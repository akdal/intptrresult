	.text
	.file	"btGImpactQuantizedBvh.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	2139095039              # float 3.40282347E+38
	.long	2139095039              # float 3.40282347E+38
	.zero	4
	.zero	4
.LCPI0_1:
	.long	4286578687              # float -3.40282347E+38
	.long	4286578687              # float -3.40282347E+38
	.zero	4
	.zero	4
.LCPI0_4:
	.long	1199570688              # float 65535
	.long	1199570688              # float 65535
	.zero	4
	.zero	4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_2:
	.long	2139095039              # float 3.40282347E+38
.LCPI0_3:
	.long	4286578687              # float -3.40282347E+38
.LCPI0_5:
	.long	1199570688              # float 65535
	.text
	.globl	_ZN18btQuantizedBvhTree17calc_quantizationER18GIM_BVH_DATA_ARRAYf
	.p2align	4, 0x90
	.type	_ZN18btQuantizedBvhTree17calc_quantizationER18GIM_BVH_DATA_ARRAYf,@function
_ZN18btQuantizedBvhTree17calc_quantizationER18GIM_BVH_DATA_ARRAYf: # @_ZN18btQuantizedBvhTree17calc_quantizationER18GIM_BVH_DATA_ARRAYf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movabsq	$9187343237679939583, %rax # imm = 0x7F7FFFFF7F7FFFFF
	movq	%rax, -32(%rsp)
	movl	$2139095039, -24(%rsp)  # imm = 0x7F7FFFFF
	movl	$-8388609, -16(%rsp)    # imm = 0xFF7FFFFF
	movabsq	$-36028797027352577, %rax # imm = 0xFF7FFFFFFF7FFFFF
	movq	%rax, -12(%rsp)
	movslq	4(%rsi), %r8
	testq	%r8, %r8
	jle	.LBB0_1
# BB#4:                                 # %.lr.ph
	leaq	-16(%rsp), %r9
	movq	16(%rsi), %rdx
	addq	$16, %rdx
	movss	.LCPI0_2(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI0_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	xorl	%esi, %esi
	leaq	-32(%rsp), %r10
	movaps	%xmm1, %xmm3
	movaps	%xmm1, %xmm4
	movaps	%xmm2, %xmm5
	movaps	%xmm2, %xmm6
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	leaq	-16(%rdx), %rcx
	ucomiss	-16(%rdx), %xmm6
	movq	%r10, %rax
	cmovaq	%rcx, %rax
	movl	(%rax), %r11d
	movl	%r11d, -32(%rsp)
	ucomiss	-12(%rdx), %xmm5
	movq	%r10, %rax
	cmovaq	%rcx, %rax
	movl	4(%rax), %eax
	movl	%eax, -28(%rsp)
	ucomiss	-8(%rdx), %xmm2
	cmovbeq	%r10, %rcx
	movl	8(%rcx), %r14d
	movl	%r14d, -24(%rsp)
	movss	(%rdx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm2
	movq	%r9, %rcx
	cmovaq	%rdx, %rcx
	movl	(%rcx), %ecx
	movl	%ecx, -16(%rsp)
	movss	4(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm2
	movq	%r9, %rbx
	cmovaq	%rdx, %rbx
	movl	4(%rbx), %ebx
	movl	%ebx, -12(%rsp)
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	movq	%r9, %rbp
	cmovaq	%rdx, %rbp
	movl	8(%rbp), %ebp
	movl	%ebp, -8(%rsp)
	incq	%rsi
	movd	%r14d, %xmm2
	movd	%ebp, %xmm1
	movd	%ecx, %xmm4
	movd	%ebx, %xmm3
	movd	%r11d, %xmm6
	movd	%eax, %xmm5
	addq	$36, %rdx
	cmpq	%r8, %rsi
	jl	.LBB0_5
# BB#2:                                 # %._crit_edge.loopexit
	movd	%ebx, %xmm3
	movd	%ecx, %xmm4
	punpckldq	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movd	%eax, %xmm5
	movd	%r11d, %xmm3
	punpckldq	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	jmp	.LBB0_3
.LBB0_1:
	movdqa	.LCPI0_0(%rip), %xmm3   # xmm3 = <3.40282347E+38,3.40282347E+38,u,u>
	movdqa	.LCPI0_1(%rip), %xmm4   # xmm4 = <-3.40282347E+38,-3.40282347E+38,u,u>
	movd	.LCPI0_2(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movd	.LCPI0_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
.LBB0_3:                                # %._crit_edge
	movaps	%xmm0, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	subps	%xmm5, %xmm3
	subss	%xmm0, %xmm2
	pxor	%xmm6, %xmm6
	xorps	%xmm7, %xmm7
	movss	%xmm2, %xmm7            # xmm7 = xmm2[0],xmm7[1,2,3]
	movlps	%xmm3, 40(%rdi)
	movlps	%xmm7, 48(%rdi)
	addps	%xmm4, %xmm5
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm5, 56(%rdi)
	movlps	%xmm0, 64(%rdi)
	subps	%xmm3, %xmm5
	subss	%xmm2, %xmm1
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = <65535,65535,u,u>
	divps	%xmm5, %xmm0
	movss	.LCPI0_5(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm2
	movss	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1,2,3]
	movlps	%xmm0, 72(%rdi)
	movlps	%xmm6, 80(%rdi)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN18btQuantizedBvhTree17calc_quantizationER18GIM_BVH_DATA_ARRAYf, .Lfunc_end0-_ZN18btQuantizedBvhTree17calc_quantizationER18GIM_BVH_DATA_ARRAYf
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_1:
	.long	1056964608              # float 0.5
.LCPI1_2:
	.long	1065353216              # float 1
.LCPI1_3:
	.long	3212836864              # float -1
	.text
	.globl	_ZN18btQuantizedBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii
	.p2align	4, 0x90
	.type	_ZN18btQuantizedBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii,@function
_ZN18btQuantizedBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii: # @_ZN18btQuantizedBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii
	.cfi_startproc
# BB#0:
	movl	%ecx, %r10d
	subl	%edx, %r10d
	xorps	%xmm3, %xmm3
	movaps	%xmm3, -24(%rsp)
	jle	.LBB1_1
# BB#2:                                 # %.lr.ph130
	movq	16(%rsi), %rax
	movslq	%edx, %r9
	movslq	%ecx, %r11
	leaq	(%r9,%r9,8), %r8
	leaq	24(%rax,%r8,4), %rdi
	movq	%r11, %rax
	subq	%r9, %rax
	xorps	%xmm4, %xmm4
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = <0.5,0.5,u,u>
	movss	.LCPI1_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movsd	-8(%rdi), %xmm2         # xmm2 = mem[0],zero
	movsd	-24(%rdi), %xmm5        # xmm5 = mem[0],zero
	addps	%xmm2, %xmm5
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	addss	-16(%rdi), %xmm2
	mulps	%xmm0, %xmm5
	mulss	%xmm1, %xmm2
	addps	%xmm5, %xmm3
	addss	%xmm2, %xmm4
	addq	$36, %rdi
	decq	%rax
	jne	.LBB1_3
# BB#4:                                 # %._crit_edge131
	cmpl	%edx, %ecx
	cvtsi2ssl	%r10d, %xmm8
	jle	.LBB1_5
# BB#6:                                 # %.lr.ph
	movss	.LCPI1_2(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	divss	%xmm8, %xmm5
	movaps	%xmm5, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm3, %xmm6
	mulss	%xmm4, %xmm5
	movq	16(%rsi), %rax
	leaq	24(%rax,%r8,4), %rcx
	subq	%r9, %r11
	xorps	%xmm4, %xmm4
	xorps	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB1_7:                                # =>This Inner Loop Header: Depth=1
	movsd	-8(%rcx), %xmm7         # xmm7 = mem[0],zero
	movsd	-24(%rcx), %xmm2        # xmm2 = mem[0],zero
	addps	%xmm7, %xmm2
	movss	(%rcx), %xmm7           # xmm7 = mem[0],zero,zero,zero
	addss	-16(%rcx), %xmm7
	mulps	%xmm0, %xmm2
	mulss	%xmm1, %xmm7
	subps	%xmm6, %xmm2
	subss	%xmm5, %xmm7
	mulps	%xmm2, %xmm2
	mulss	%xmm7, %xmm7
	addps	%xmm2, %xmm4
	addss	%xmm7, %xmm3
	addq	$36, %rcx
	decq	%r11
	jne	.LBB1_7
# BB#8:                                 # %._crit_edge
	movss	%xmm4, -24(%rsp)
	movaps	%xmm4, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm0, -20(%rsp)
	movss	%xmm3, -16(%rsp)
	jmp	.LBB1_9
.LBB1_1:                                # %._crit_edge131.thread
	cvtsi2ssl	%r10d, %xmm8
.LBB1_5:                                # %._crit_edge131._crit_edge
	xorps	%xmm4, %xmm4
	xorps	%xmm3, %xmm3
.LBB1_9:
	addss	.LCPI1_3(%rip), %xmm8
	movss	.LCPI1_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm8, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	movss	%xmm1, -24(%rsp)
	movaps	%xmm1, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movss	%xmm2, -20(%rsp)
	mulss	%xmm3, %xmm0
	movss	%xmm0, -16(%rsp)
	xorl	%ecx, %ecx
	ucomiss	%xmm1, %xmm2
	seta	%cl
	ucomiss	-24(%rsp,%rcx,4), %xmm0
	movl	$2, %eax
	cmovbel	%ecx, %eax
	retq
.Lfunc_end1:
	.size	_ZN18btQuantizedBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii, .Lfunc_end1-_ZN18btQuantizedBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	1056964608              # float 0.5
.LCPI2_1:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_2:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.text
	.globl	_ZN18btQuantizedBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii
	.p2align	4, 0x90
	.type	_ZN18btQuantizedBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii,@function
_ZN18btQuantizedBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii: # @_ZN18btQuantizedBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 40
.Lcfi10:
	.cfi_offset %rbx, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%ecx, %r9d
	subl	%edx, %r9d
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -72(%rsp)
	jle	.LBB2_1
# BB#2:                                 # %.lr.ph94
	movq	16(%rsi), %r10
	movslq	%edx, %rbx
	movslq	%ecx, %rax
	leaq	(%rbx,%rbx,8), %rdi
	leaq	24(%r10,%rdi,4), %rdi
	subq	%rbx, %rax
	xorps	%xmm0, %xmm0
	movss	.LCPI2_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movss	-8(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	-4(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	addss	-24(%rdi), %xmm4
	addss	-20(%rdi), %xmm5
	movss	(%rdi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	addss	-16(%rdi), %xmm6
	mulss	%xmm3, %xmm4
	mulss	%xmm3, %xmm5
	mulss	%xmm3, %xmm6
	addss	%xmm4, %xmm2
	addss	%xmm5, %xmm1
	addss	%xmm6, %xmm0
	addq	$36, %rdi
	decq	%rax
	jne	.LBB2_3
# BB#4:                                 # %._crit_edge95
	movss	%xmm2, -72(%rsp)
	movss	%xmm1, -68(%rsp)
	movss	%xmm0, -64(%rsp)
	jmp	.LBB2_5
.LBB2_1:                                # %._crit_edge106
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
.LBB2_5:
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%r9d, %xmm3
	movss	.LCPI2_1(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm4
	mulss	%xmm4, %xmm2
	movss	%xmm2, -72(%rsp)
	mulss	%xmm4, %xmm1
	movss	%xmm1, -68(%rsp)
	mulss	%xmm0, %xmm4
	movss	%xmm4, -64(%rsp)
	cmpl	%edx, %ecx
	movl	%edx, %eax
	jle	.LBB2_10
# BB#6:                                 # %.lr.ph
	movslq	%r8d, %r10
	movss	-72(%rsp,%r10,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	leaq	-24(%rsp), %r8
	movslq	%edx, %rax
	movslq	%ecx, %r11
	leaq	(%rax,%rax,8), %rdi
	leaq	16(,%rdi,4), %rdi
	subq	%rax, %r11
	movaps	.LCPI2_2(%rip), %xmm1   # xmm1 = <0.5,0.5,u,u>
	movss	.LCPI2_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movl	%edx, %eax
	.p2align	4, 0x90
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rsi), %rbx
	movsd	(%rbx,%rdi), %xmm3      # xmm3 = mem[0],zero
	movsd	-16(%rbx,%rdi), %xmm4   # xmm4 = mem[0],zero
	addps	%xmm3, %xmm4
	movss	8(%rbx,%rdi), %xmm3     # xmm3 = mem[0],zero,zero,zero
	addss	-8(%rbx,%rdi), %xmm3
	mulps	%xmm1, %xmm4
	mulss	%xmm2, %xmm3
	xorps	%xmm5, %xmm5
	movss	%xmm3, %xmm5            # xmm5 = xmm3[0],xmm5[1,2,3]
	movlps	%xmm4, -56(%rsp)
	movlps	%xmm5, -48(%rsp)
	movss	-56(%rsp,%r10,4), %xmm3 # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm3
	jbe	.LBB2_9
# BB#8:                                 #   in Loop: Header=BB2_7 Depth=1
	movups	-16(%rbx,%rdi), %xmm3
	movaps	%xmm3, -40(%rsp)
	movups	(%rbx,%rdi), %xmm3
	movups	%xmm3, (%r8)
	movl	16(%rbx,%rdi), %r14d
	cltq
	leaq	(,%rax,4), %rbp
	leaq	(%rbp,%rbp,8), %r15
	movl	32(%rbx,%r15), %ebp
	movl	%ebp, 16(%rbx,%rdi)
	movups	(%rbx,%r15), %xmm3
	movups	16(%rbx,%r15), %xmm4
	movups	%xmm4, (%rbx,%rdi)
	movups	%xmm3, -16(%rbx,%rdi)
	movq	16(%rsi), %rbx
	movaps	-40(%rsp), %xmm3
	movaps	-24(%rsp), %xmm4
	movups	%xmm4, 16(%rbx,%r15)
	movups	%xmm3, (%rbx,%r15)
	movl	%r14d, 32(%rbx,%r15)
	incl	%eax
.LBB2_9:                                #   in Loop: Header=BB2_7 Depth=1
	addq	$36, %rdi
	decq	%r11
	jne	.LBB2_7
.LBB2_10:                               # %._crit_edge
	movslq	%r9d, %rsi
	imulq	$1431655766, %rsi, %rsi # imm = 0x55555556
	movq	%rsi, %rdi
	shrq	$63, %rdi
	shrq	$32, %rsi
	addl	%edi, %esi
	leal	(%rsi,%rdx), %edi
	cmpl	%edi, %eax
	jle	.LBB2_12
# BB#11:
	decl	%ecx
	subl	%esi, %ecx
	cmpl	%ecx, %eax
	jl	.LBB2_13
.LBB2_12:                               # %.critedge
	sarl	%r9d
	addl	%edx, %r9d
	movl	%r9d, %eax
.LBB2_13:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN18btQuantizedBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii, .Lfunc_end2-_ZN18btQuantizedBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	2139095039              # float 3.40282347E+38
.LCPI3_1:
	.long	4286578687              # float -3.40282347E+38
.LCPI3_2:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZN18btQuantizedBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii
	.p2align	4, 0x90
	.type	_ZN18btQuantizedBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii,@function
_ZN18btQuantizedBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii: # @_ZN18btQuantizedBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 96
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movl	%edx, %r13d
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movslq	(%r14), %r15
	leal	1(%r15), %eax
	movl	%eax, (%r14)
	movl	%ebp, %eax
	subl	%r13d, %eax
	cmpl	$1, %eax
	jne	.LBB3_2
# BB#1:
	movq	16(%rbx), %rax
	movslq	%r13d, %rdx
	movq	24(%r14), %rcx
	shlq	$4, %r15
	leaq	(%rdx,%rdx,8), %rdx
	movss	40(%r14), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm0
	maxss	(%rax,%rdx,4), %xmm0
	movss	44(%r14), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm6
	maxss	4(%rax,%rdx,4), %xmm6
	movss	48(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm7
	maxss	8(%rax,%rdx,4), %xmm7
	movss	56(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	minss	%xmm0, %xmm2
	movss	60(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	minss	%xmm6, %xmm1
	movss	64(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	minss	%xmm7, %xmm0
	subss	%xmm10, %xmm2
	subss	%xmm11, %xmm1
	subss	%xmm5, %xmm0
	movss	72(%r14), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	movss	76(%r14), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm1
	movss	80(%r14), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	movss	.LCPI3_2(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm2
	cvttss2si	%xmm2, %esi
	movw	%si, (%rcx,%r15)
	addss	%xmm7, %xmm1
	cvttss2si	%xmm1, %esi
	movw	%si, 2(%rcx,%r15)
	addss	%xmm7, %xmm0
	cvttss2si	%xmm0, %esi
	movw	%si, 4(%rcx,%r15)
	movaps	%xmm10, %xmm0
	maxss	16(%rax,%rdx,4), %xmm0
	movaps	%xmm11, %xmm1
	maxss	20(%rax,%rdx,4), %xmm1
	movaps	%xmm5, %xmm2
	maxss	24(%rax,%rdx,4), %xmm2
	minss	%xmm0, %xmm3
	minss	%xmm1, %xmm4
	minss	%xmm2, %xmm6
	subss	%xmm10, %xmm3
	subss	%xmm11, %xmm4
	subss	%xmm5, %xmm6
	mulss	%xmm8, %xmm3
	mulss	%xmm9, %xmm4
	mulss	%xmm12, %xmm6
	addss	%xmm7, %xmm3
	cvttss2si	%xmm3, %esi
	movw	%si, 6(%rcx,%r15)
	addss	%xmm7, %xmm4
	cvttss2si	%xmm4, %esi
	movw	%si, 8(%rcx,%r15)
	addss	%xmm7, %xmm6
	cvttss2si	%xmm6, %esi
	movw	%si, 10(%rcx,%r15)
	movl	32(%rax,%rdx,4), %eax
	movl	%eax, 12(%rcx,%r15)
	jmp	.LBB3_5
.LBB3_2:
	movq	%rbx, %rsi
	movl	%r13d, %edx
	movl	%ebp, %ecx
	callq	_ZN18btQuantizedBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii
	movq	%rbx, %rsi
	movl	%r13d, %edx
	movl	%ebp, %ecx
	movl	%eax, %r8d
	callq	_ZN18btQuantizedBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movabsq	$9187343237679939583, %rax # imm = 0x7F7FFFFF7F7FFFFF
	movq	%rax, 8(%rsp)
	movl	$2139095039, 16(%rsp)   # imm = 0x7F7FFFFF
	movl	$-8388609, 24(%rsp)     # imm = 0xFF7FFFFF
	movabsq	$-36028797027352577, %rax # imm = 0xFF7FFFFFFF7FFFFF
	movq	%rax, 28(%rsp)
	cmpl	%r13d, %ebp
	movl	%ebp, (%rsp)            # 4-byte Spill
	jle	.LBB3_3
# BB#6:                                 # %.lr.ph
	leaq	24(%rsp), %r8
	movq	%rbx, %r11
	movq	16(%rbx), %rdx
	movslq	%r13d, %rsi
	movslq	%ebp, %rcx
	leaq	(%rsi,%rsi,8), %rdi
	leaq	16(%rdx,%rdi,4), %rdx
	subq	%rsi, %rcx
	movss	.LCPI3_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI3_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	leaq	8(%rsp), %r9
	movaps	%xmm0, %xmm1
	movaps	%xmm0, %xmm2
	movaps	%xmm3, %xmm4
	movaps	%xmm3, %xmm5
	.p2align	4, 0x90
.LBB3_7:                                # =>This Inner Loop Header: Depth=1
	leaq	-16(%rdx), %rdi
	ucomiss	-16(%rdx), %xmm5
	movq	%r9, %rsi
	cmovaq	%rdi, %rsi
	movl	(%rsi), %r10d
	movl	%r10d, 8(%rsp)
	ucomiss	-12(%rdx), %xmm4
	movq	%r9, %rbp
	cmovaq	%rdi, %rbp
	movl	4(%rbp), %ebp
	movl	%ebp, 12(%rsp)
	ucomiss	-8(%rdx), %xmm3
	cmovbeq	%r9, %rdi
	movl	8(%rdi), %edi
	movl	%edi, 16(%rsp)
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm3
	movq	%r8, %rax
	cmovaq	%rdx, %rax
	movl	(%rax), %eax
	movl	%eax, 24(%rsp)
	movss	4(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	movq	%r8, %rbx
	cmovaq	%rdx, %rbx
	movl	4(%rbx), %ebx
	movl	%ebx, 28(%rsp)
	movss	8(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	movq	%r8, %rsi
	cmovaq	%rdx, %rsi
	movl	8(%rsi), %esi
	movl	%esi, 32(%rsp)
	movd	%r10d, %xmm5
	movd	%ebp, %xmm4
	movd	%edi, %xmm3
	movd	%eax, %xmm2
	movd	%ebx, %xmm1
	movd	%esi, %xmm0
	addq	$36, %rdx
	decq	%rcx
	jne	.LBB3_7
	jmp	.LBB3_4
.LBB3_3:
	movq	%rbx, %r11
	movd	.LCPI3_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movd	.LCPI3_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movdqa	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	movdqa	%xmm3, %xmm4
	movdqa	%xmm3, %xmm5
.LBB3_4:                                # %._crit_edge
	movq	24(%r14), %rax
	movq	%r15, %r12
	shlq	$4, %r12
	movss	40(%r14), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm7
	maxss	%xmm5, %xmm7
	movss	44(%r14), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm10
	maxss	%xmm4, %xmm10
	movss	48(%r14), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm9
	maxss	%xmm3, %xmm9
	movss	56(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm5
	minss	%xmm7, %xmm5
	movss	60(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm7
	minss	%xmm10, %xmm7
	movss	64(%r14), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm3
	minss	%xmm9, %xmm3
	subss	%xmm8, %xmm5
	subss	%xmm13, %xmm7
	subss	%xmm14, %xmm3
	movss	72(%r14), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm5
	movss	76(%r14), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm7
	movss	80(%r14), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm3
	movss	.LCPI3_2(%rip), %xmm9   # xmm9 = mem[0],zero,zero,zero
	addss	%xmm9, %xmm5
	cvttss2si	%xmm5, %ecx
	movw	%cx, (%rax,%r12)
	addss	%xmm9, %xmm7
	cvttss2si	%xmm7, %ecx
	movw	%cx, 2(%rax,%r12)
	addss	%xmm9, %xmm3
	cvttss2si	%xmm3, %ecx
	movw	%cx, 4(%rax,%r12)
	movaps	%xmm8, %xmm3
	maxss	%xmm2, %xmm3
	movaps	%xmm13, %xmm2
	maxss	%xmm1, %xmm2
	movaps	%xmm14, %xmm1
	maxss	%xmm0, %xmm1
	minss	%xmm3, %xmm6
	minss	%xmm2, %xmm4
	minss	%xmm1, %xmm15
	subss	%xmm8, %xmm6
	subss	%xmm13, %xmm4
	subss	%xmm14, %xmm15
	mulss	%xmm10, %xmm6
	mulss	%xmm11, %xmm4
	mulss	%xmm12, %xmm15
	addss	%xmm9, %xmm6
	cvttss2si	%xmm6, %ecx
	movw	%cx, 6(%rax,%r12)
	addss	%xmm9, %xmm4
	cvttss2si	%xmm4, %ecx
	movw	%cx, 8(%rax,%r12)
	addss	%xmm9, %xmm15
	cvttss2si	%xmm15, %ecx
	movw	%cx, 10(%rax,%r12)
	movq	%r14, %rdi
	movq	%r11, %rbx
	movq	%rbx, %rsi
	movl	%r13d, %edx
	movl	4(%rsp), %ebp           # 4-byte Reload
	movl	%ebp, %ecx
	callq	_ZN18btQuantizedBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movl	%ebp, %edx
	movl	(%rsp), %ecx            # 4-byte Reload
	callq	_ZN18btQuantizedBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii
	movq	24(%r14), %rax
	movl	(%r14), %ecx
	subl	%ecx, %r15d
	movl	%r15d, 12(%rax,%r12)
.LBB3_5:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN18btQuantizedBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii, .Lfunc_end3-_ZN18btQuantizedBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN18btQuantizedBvhTree10build_treeER18GIM_BVH_DATA_ARRAY
	.p2align	4, 0x90
	.type	_ZN18btQuantizedBvhTree10build_treeER18GIM_BVH_DATA_ARRAY,@function
_ZN18btQuantizedBvhTree10build_treeER18GIM_BVH_DATA_ARRAY: # @_ZN18btQuantizedBvhTree10build_treeER18GIM_BVH_DATA_ARRAY
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 80
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r15
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	_ZN18btQuantizedBvhTree17calc_quantizationER18GIM_BVH_DATA_ARRAYf
	movl	$0, (%r15)
	movslq	4(%r13), %rcx
	movq	%rcx, %r12
	addq	%r12, %r12
	movl	12(%r15), %r14d
	cmpl	%r12d, %r14d
	jge	.LBB4_26
# BB#1:
	movslq	%r14d, %rbx
	cmpl	%r12d, 16(%r15)
	jge	.LBB4_2
# BB#3:
	testl	%ecx, %ecx
	movq	%r13, 16(%rsp)          # 8-byte Spill
	je	.LBB4_4
# BB#5:
	movq	%r12, %rdi
	shlq	$4, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r13
	movl	12(%r15), %eax
	jmp	.LBB4_6
.LBB4_2:                                # %..lr.ph.i_crit_edge
	leaq	24(%r15), %rbp
	jmp	.LBB4_19
.LBB4_4:
	xorl	%r13d, %r13d
	movl	%r14d, %eax
.LBB4_6:                                # %_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE8allocateEi.exit.i.i
	leaq	24(%r15), %rbp
	testl	%eax, %eax
	jle	.LBB4_14
# BB#7:                                 # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB4_8
# BB#9:                                 # %.prol.preheader
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_10:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%r13,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB4_10
	jmp	.LBB4_11
.LBB4_8:
	xorl	%ecx, %ecx
.LBB4_11:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB4_14
# BB#12:                                # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB4_13:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%r13,%rcx)
	movq	(%rbp), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%r13,%rcx)
	movq	(%rbp), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%r13,%rcx)
	movq	(%rbp), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%r13,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB4_13
.LBB4_14:                               # %_ZNK20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE4copyEiiPS0_.exit.i.i
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_18
# BB#15:
	cmpb	$0, 32(%r15)
	je	.LBB4_17
# BB#16:
	callq	_Z21btAlignedFreeInternalPv
.LBB4_17:
	movq	$0, (%rbp)
.LBB4_18:                               # %_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE7reserveEi.exit.preheader.i
	movb	$1, 32(%r15)
	movq	%r13, 24(%r15)
	movl	%r12d, 16(%r15)
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB4_19:                               # %.lr.ph.i
	movslq	%r12d, %rax
	movl	%r12d, %edx
	subl	%r14d, %edx
	leaq	-1(%rax), %r8
	subq	%rbx, %r8
	andq	$3, %rdx
	je	.LBB4_22
# BB#20:                                # %_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE7reserveEi.exit.i.prol.preheader
	movq	%rbx, %rsi
	shlq	$4, %rsi
	negq	%rdx
	.p2align	4, 0x90
.LBB4_21:                               # %_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movl	8(%rsp), %ecx
	movl	%ecx, 8(%rdi,%rsi)
	movq	(%rsp), %rcx
	movq	%rcx, (%rdi,%rsi)
	movl	$0, 12(%rdi,%rsi)
	incq	%rbx
	addq	$16, %rsi
	incq	%rdx
	jne	.LBB4_21
.LBB4_22:                               # %_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE7reserveEi.exit.i.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB4_25
# BB#23:                                # %.lr.ph.i.new
	subq	%rbx, %rax
	shlq	$4, %rbx
	.p2align	4, 0x90
.LBB4_24:                               # %_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rcx
	movl	8(%rsp), %edx
	movl	%edx, 8(%rcx,%rbx)
	movq	(%rsp), %rdx
	movq	%rdx, (%rcx,%rbx)
	movl	$0, 12(%rcx,%rbx)
	movq	(%rbp), %rcx
	movl	8(%rsp), %edx
	movl	%edx, 24(%rcx,%rbx)
	movq	(%rsp), %rdx
	movq	%rdx, 16(%rcx,%rbx)
	movl	$0, 28(%rcx,%rbx)
	movq	(%rbp), %rcx
	movl	8(%rsp), %edx
	movl	%edx, 40(%rcx,%rbx)
	movq	(%rsp), %rdx
	movq	%rdx, 32(%rcx,%rbx)
	movl	$0, 44(%rcx,%rbx)
	movq	(%rbp), %rcx
	movl	8(%rsp), %edx
	movl	%edx, 56(%rcx,%rbx)
	movq	(%rsp), %rdx
	movq	%rdx, 48(%rcx,%rbx)
	movl	$0, 60(%rcx,%rbx)
	addq	$64, %rbx
	addq	$-4, %rax
	jne	.LBB4_24
.LBB4_25:                               # %_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE6resizeEiRKS0_.exit.loopexit
	movl	4(%r13), %ecx
.LBB4_26:                               # %_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE6resizeEiRKS0_.exit
	movl	%r12d, 12(%r15)
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r13, %rsi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN18btQuantizedBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii # TAILCALL
.Lfunc_end4:
	.size	_ZN18btQuantizedBvhTree10build_treeER18GIM_BVH_DATA_ARRAY, .Lfunc_end4-_ZN18btQuantizedBvhTree10build_treeER18GIM_BVH_DATA_ARRAY
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	2139095039              # float 3.40282347E+38
.LCPI5_1:
	.long	4286578687              # float -3.40282347E+38
.LCPI5_2:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZN21btGImpactQuantizedBvh5refitEv
	.p2align	4, 0x90
	.type	_ZN21btGImpactQuantizedBvh5refitEv,@function
_ZN21btGImpactQuantizedBvh5refitEv:     # @_ZN21btGImpactQuantizedBvh5refitEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 128
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movslq	(%r13), %rcx
	testq	%rcx, %rcx
	je	.LBB5_9
# BB#1:                                 # %.lr.ph
	leaq	56(%rsp), %rdi
	movq	24(%r13), %rax
	movq	%rcx, %r14
	shlq	$4, %r14
	leal	1(%rcx), %r15d
	movq	%rsp, %r12
	leaq	40(%rsp), %r11
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movl	-4(%rax,%r14), %esi
	testl	%esi, %esi
	js	.LBB5_4
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	%rdi, %rbx
	movq	88(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rdx
	movq	%r11, %rbp
	callq	*32(%rax)
	movq	%rbp, %r11
	movq	%rbx, %rdi
	movq	24(%r13), %rax
	movss	40(%r13), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	maxss	(%rsp), %xmm0
	movss	44(%r13), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm6
	maxss	4(%rsp), %xmm6
	movss	48(%r13), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm7
	maxss	8(%rsp), %xmm7
	movss	56(%r13), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	minss	%xmm0, %xmm2
	movss	60(%r13), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	minss	%xmm6, %xmm1
	movss	64(%r13), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	minss	%xmm7, %xmm0
	subss	%xmm9, %xmm2
	subss	%xmm10, %xmm1
	subss	%xmm5, %xmm0
	movss	72(%r13), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	movss	76(%r13), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	movss	80(%r13), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	movss	.LCPI5_2(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm2
	cvttss2si	%xmm2, %ecx
	movw	%cx, -16(%rax,%r14)
	addss	%xmm7, %xmm1
	cvttss2si	%xmm1, %ecx
	movw	%cx, -14(%rax,%r14)
	addss	%xmm7, %xmm0
	cvttss2si	%xmm0, %ecx
	movw	%cx, -12(%rax,%r14)
	movaps	%xmm9, %xmm0
	maxss	16(%rsp), %xmm0
	movaps	%xmm10, %xmm1
	maxss	20(%rsp), %xmm1
	movaps	%xmm5, %xmm2
	maxss	24(%rsp), %xmm2
	minss	%xmm0, %xmm3
	minss	%xmm1, %xmm4
	minss	%xmm2, %xmm6
	subss	%xmm9, %xmm3
	subss	%xmm10, %xmm4
	subss	%xmm5, %xmm6
	mulss	%xmm8, %xmm3
	mulss	%xmm11, %xmm4
	mulss	%xmm12, %xmm6
	addss	%xmm7, %xmm3
	cvttss2si	%xmm3, %ecx
	movw	%cx, -10(%rax,%r14)
	addss	%xmm7, %xmm4
	cvttss2si	%xmm4, %ecx
	movw	%cx, -8(%rax,%r14)
	addss	%xmm7, %xmm6
	jmp	.LBB5_8
	.p2align	4, 0x90
.LBB5_4:                                #   in Loop: Header=BB5_2 Depth=1
	movabsq	$9187343237679939583, %rcx # imm = 0x7F7FFFFF7F7FFFFF
	movq	%rcx, (%rsp)
	movl	$2139095039, 8(%rsp)    # imm = 0x7F7FFFFF
	movl	$-8388609, 16(%rsp)     # imm = 0xFF7FFFFF
	movabsq	$-36028797027352577, %rcx # imm = 0xFF7FFFFFFF7FFFFF
	movq	%rcx, 20(%rsp)
	movzwl	(%rax,%r14), %ecx
	cvtsi2ssl	%ecx, %xmm3
	movss	72(%r13), %xmm6         # xmm6 = mem[0],zero,zero,zero
	divss	%xmm6, %xmm3
	movzwl	2(%rax,%r14), %ecx
	cvtsi2ssl	%ecx, %xmm4
	movss	76(%r13), %xmm7         # xmm7 = mem[0],zero,zero,zero
	divss	%xmm7, %xmm4
	movzwl	4(%rax,%r14), %ecx
	cvtsi2ssl	%ecx, %xmm5
	movss	80(%r13), %xmm13        # xmm13 = mem[0],zero,zero,zero
	divss	%xmm13, %xmm5
	movss	40(%r13), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movss	44(%r13), %xmm14        # xmm14 = mem[0],zero,zero,zero
	addss	%xmm15, %xmm3
	addss	%xmm14, %xmm4
	movss	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm0
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	movss	48(%r13), %xmm8         # xmm8 = mem[0],zero,zero,zero
	addss	%xmm8, %xmm5
	xorps	%xmm1, %xmm1
	shufps	$1, %xmm5, %xmm1        # xmm1 = xmm1[1,0],xmm5[0,0]
	shufps	$226, %xmm5, %xmm1      # xmm1 = xmm1[2,0],xmm5[2,3]
	movlps	%xmm3, 40(%rsp)
	movlps	%xmm1, 48(%rsp)
	movzwl	6(%rax,%r14), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ecx, %xmm1
	divss	%xmm6, %xmm1
	movzwl	8(%rax,%r14), %ecx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ecx, %xmm3
	divss	%xmm7, %xmm3
	movzwl	10(%rax,%r14), %eax
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	divss	%xmm13, %xmm6
	addss	%xmm15, %xmm1
	addss	%xmm14, %xmm3
	movq	%r12, %rax
	cmovaq	%r11, %rax
	ucomiss	%xmm4, %xmm0
	movq	%r12, %rcx
	cmovaq	%r11, %rcx
	orq	$4, %rcx
	ucomiss	%xmm5, %xmm0
	movq	%r12, %rdx
	cmovaq	%r11, %rdx
	movss	.LCPI5_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	unpcklps	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	addss	%xmm8, %xmm6
	xorps	%xmm4, %xmm4
	shufps	$1, %xmm6, %xmm4        # xmm4 = xmm4[1,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm4      # xmm4 = xmm4[2,0],xmm6[2,3]
	movlps	%xmm1, 56(%rsp)
	movlps	%xmm4, 64(%rsp)
	movl	(%rax), %r9d
	movl	%r9d, (%rsp)
	movl	(%rcx), %r10d
	movl	%r10d, 4(%rsp)
	movl	8(%rdx), %r8d
	movl	%r8d, 8(%rsp)
	leaq	16(%rsp), %rax
	movq	%rax, %rcx
	cmovaq	%rdi, %rax
	movl	(%rax), %ebp
	movl	%ebp, 16(%rsp)
	ucomiss	%xmm0, %xmm3
	movq	%rcx, %rax
	cmovaq	%rdi, %rax
	movl	4(%rax), %edx
	movl	%edx, 20(%rsp)
	ucomiss	%xmm0, %xmm6
	movq	%rcx, %rax
	cmovaq	%rdi, %rax
	movl	8(%rax), %esi
	movl	%esi, 24(%rsp)
	movq	24(%r13), %rax
	movq	%r12, %rbx
	movq	%rdi, %r12
	movl	12(%rax,%r14), %edi
	cmpl	$-2, %edi
	movl	$-1, %ecx
	cmovgl	%edi, %ecx
	subl	%edi, %ecx
	movd	%r9d, %xmm7
	movd	%r10d, %xmm6
	movd	%r8d, %xmm12
	movd	%ebp, %xmm11
	movd	%edx, %xmm9
	movd	%esi, %xmm10
	addl	%r15d, %ecx
	je	.LBB5_5
# BB#6:                                 #   in Loop: Header=BB5_2 Depth=1
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	movzwl	(%rax,%rcx), %edx
	cvtsi2ssl	%edx, %xmm5
	movss	72(%r13), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	76(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	divss	%xmm4, %xmm5
	movzwl	2(%rax,%rcx), %edx
	addss	%xmm15, %xmm5
	ucomiss	%xmm5, %xmm7
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%edx, %xmm1
	divss	%xmm0, %xmm1
	addss	%xmm14, %xmm1
	movq	%rbx, %rdx
	cmovaq	%r11, %rdx
	ucomiss	%xmm1, %xmm6
	movzwl	4(%rax,%rcx), %esi
	unpcklps	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%esi, %xmm1
	divss	%xmm13, %xmm1
	addss	%xmm8, %xmm1
	movlps	%xmm5, 40(%rsp)
	xorps	%xmm3, %xmm3
	shufps	$1, %xmm1, %xmm3        # xmm3 = xmm3[1,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm3      # xmm3 = xmm3[2,0],xmm1[2,3]
	movlps	%xmm3, 48(%rsp)
	movzwl	6(%rax,%rcx), %esi
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%esi, %xmm3
	divss	%xmm4, %xmm3
	movzwl	10(%rax,%rcx), %esi
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%esi, %xmm4
	divss	%xmm13, %xmm4
	movzwl	8(%rax,%rcx), %ecx
	addss	%xmm15, %xmm3
	cvtsi2ssl	%ecx, %xmm2
	movss	%xmm0, 36(%rsp)         # 4-byte Spill
	divss	%xmm0, %xmm2
	addss	%xmm14, %xmm2
	movq	%rbx, %rcx
	cmovaq	%r11, %rcx
	orq	$4, %rcx
	ucomiss	%xmm1, %xmm12
	movq	%rbx, %rsi
	cmovaq	%r11, %rsi
	ucomiss	%xmm11, %xmm3
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	addss	%xmm8, %xmm4
	xorps	%xmm0, %xmm0
	shufps	$1, %xmm4, %xmm0        # xmm0 = xmm0[1,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm0      # xmm0 = xmm0[2,0],xmm4[2,3]
	movlps	%xmm3, 56(%rsp)
	movlps	%xmm0, 64(%rsp)
	movl	(%rdx), %r8d
	movl	%r8d, (%rsp)
	movl	(%rcx), %ecx
	movl	%ecx, 4(%rsp)
	movl	8(%rsi), %esi
	movl	%esi, 8(%rsp)
	leaq	16(%rsp), %rdx
	movq	%rdx, %rdi
	cmovaq	%r12, %rdi
	movl	(%rdi), %edi
	movl	%edi, 16(%rsp)
	ucomiss	%xmm9, %xmm2
	movq	%rdx, %rbp
	cmovaq	%r12, %rbp
	movl	4(%rbp), %ebp
	movl	%ebp, 20(%rsp)
	ucomiss	%xmm10, %xmm4
	cmovaq	%r12, %rdx
	movl	8(%rdx), %edx
	movl	%edx, 24(%rsp)
	movd	%r8d, %xmm7
	movd	%ecx, %xmm6
	movd	%esi, %xmm12
	movss	40(%r13), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movss	44(%r13), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movss	48(%r13), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	80(%r13), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movd	%edi, %xmm11
	movd	%ebp, %xmm9
	movd	%edx, %xmm10
	jmp	.LBB5_7
.LBB5_5:                                # %._crit_edge64
                                        #   in Loop: Header=BB5_2 Depth=1
	movss	76(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 36(%rsp)         # 4-byte Spill
.LBB5_7:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%r12, %rdi
	movq	%rbx, %r12
	movaps	%xmm15, %xmm1
	maxss	%xmm7, %xmm1
	movaps	%xmm14, %xmm3
	maxss	%xmm6, %xmm3
	movaps	%xmm8, %xmm7
	maxss	%xmm12, %xmm7
	movss	56(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	60(%r13), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	minss	%xmm1, %xmm4
	movaps	%xmm5, %xmm1
	minss	%xmm3, %xmm1
	movss	64(%r13), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm3
	minss	%xmm7, %xmm3
	subss	%xmm15, %xmm4
	subss	%xmm14, %xmm1
	subss	%xmm8, %xmm3
	movss	72(%r13), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm4
	movss	36(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	mulss	%xmm13, %xmm3
	movss	.LCPI5_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	cvttss2si	%xmm4, %ecx
	movw	%cx, -16(%rax,%r14)
	addss	%xmm0, %xmm1
	cvttss2si	%xmm1, %ecx
	movw	%cx, -14(%rax,%r14)
	addss	%xmm0, %xmm3
	cvttss2si	%xmm3, %ecx
	movw	%cx, -12(%rax,%r14)
	movaps	%xmm15, %xmm1
	maxss	%xmm11, %xmm1
	movaps	%xmm14, %xmm3
	maxss	%xmm9, %xmm3
	movaps	%xmm8, %xmm4
	maxss	%xmm10, %xmm4
	minss	%xmm1, %xmm2
	minss	%xmm3, %xmm5
	minss	%xmm4, %xmm6
	subss	%xmm15, %xmm2
	subss	%xmm14, %xmm5
	subss	%xmm8, %xmm6
	mulss	%xmm12, %xmm2
	mulss	%xmm7, %xmm5
	mulss	%xmm13, %xmm6
	addss	%xmm0, %xmm2
	cvttss2si	%xmm2, %ecx
	movw	%cx, -10(%rax,%r14)
	addss	%xmm0, %xmm5
	cvttss2si	%xmm5, %ecx
	movw	%cx, -8(%rax,%r14)
	addss	%xmm0, %xmm6
.LBB5_8:                                # %.backedge
                                        #   in Loop: Header=BB5_2 Depth=1
	cvttss2si	%xmm6, %ecx
	movw	%cx, -6(%rax,%r14)
	addq	$-16, %r14
	decl	%r15d
	cmpl	$1, %r15d
	jne	.LBB5_2
.LBB5_9:                                # %._crit_edge
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN21btGImpactQuantizedBvh5refitEv, .Lfunc_end5-_ZN21btGImpactQuantizedBvh5refitEv
	.cfi_endproc

	.globl	_ZN21btGImpactQuantizedBvh8buildSetEv
	.p2align	4, 0x90
	.type	_ZN21btGImpactQuantizedBvh8buildSetEv,@function
_ZN21btGImpactQuantizedBvh8buildSetEv:  # @_ZN21btGImpactQuantizedBvh8buildSetEv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 48
	subq	$64, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 112
.Lcfi59:
	.cfi_offset %rbx, -48
.Lcfi60:
	.cfi_offset %r12, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movb	$1, 24(%rsp)
	movq	$0, 16(%rsp)
	movq	$0, 4(%rsp)
	movq	88(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp0:
	callq	*24(%rax)
	movl	%eax, %r14d
.Ltmp1:
# BB#1:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	testl	%r14d, %r14d
	jle	.LBB6_2
# BB#9:
	movslq	%r14d, %rbp
	leaq	(,%rbp,4), %rax
	leaq	(%rax,%rax,8), %rdi
.Ltmp2:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
.Ltmp3:
# BB#10:                                # %_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE8allocateEi.exit.i.i
	movslq	4(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB6_17
# BB#11:                                # %.lr.ph.i.i.i
	movq	16(%rsp), %rdi
	testb	$1, %al
	jne	.LBB6_13
# BB#12:
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	jne	.LBB6_15
	jmp	.LBB6_18
.LBB6_2:                                # %.loopexit.thread
	movl	%r14d, 4(%rsp)
	jmp	.LBB6_3
.LBB6_17:                               # %_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4copyEiiPS0_.exit.i.i
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB6_18
	jmp	.LBB6_21
.LBB6_13:
	movups	(%rdi), %xmm0
	movups	%xmm0, (%r15)
	movups	16(%rdi), %xmm0
	movups	%xmm0, 16(%r15)
	movl	32(%rdi), %ecx
	movl	%ecx, 32(%r15)
	movl	$1, %ecx
	cmpl	$1, %eax
	je	.LBB6_18
.LBB6_15:                               # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$2, %rcx
	leaq	(%rcx,%rcx,8), %rsi
	leaq	(%rdi,%rsi), %rcx
	movq	%r15, %rdx
	addq	%rsi, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_16:                               # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rsi), %xmm0
	movups	%xmm0, (%rdx,%rsi)
	movups	16(%rcx,%rsi), %xmm0
	movups	%xmm0, 16(%rdx,%rsi)
	movl	32(%rcx,%rsi), %ebx
	movl	%ebx, 32(%rdx,%rsi)
	movups	36(%rcx,%rsi), %xmm0
	movups	%xmm0, 36(%rdx,%rsi)
	movups	52(%rcx,%rsi), %xmm0
	movups	%xmm0, 52(%rdx,%rsi)
	movl	68(%rcx,%rsi), %ebx
	movl	%ebx, 68(%rdx,%rsi)
	addq	$72, %rsi
	addq	$-2, %rax
	jne	.LBB6_16
.LBB6_18:                               # %_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4copyEiiPS0_.exit.i.i.thread
	cmpb	$0, 24(%rsp)
	je	.LBB6_20
# BB#19:
.Ltmp4:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp5:
.LBB6_20:                               # %.noexc13
	movq	$0, 16(%rsp)
.LBB6_21:                               # %.lr.ph.i
	movb	$1, 24(%rsp)
	movq	%r15, 16(%rsp)
	movl	%r14d, 8(%rsp)
	movaps	32(%rsp), %xmm0
	movups	%xmm0, (%r15)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 16(%r15)
	movl	$0, 32(%r15)
	cmpl	$1, %r14d
	je	.LBB6_28
# BB#22:                                # %_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i._ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i_crit_edge.preheader
	leaq	48(%rsp), %rax
	testb	$1, %r14b
	jne	.LBB6_23
# BB#24:                                # %_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i._ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i_crit_edge.prol
	movq	16(%rsp), %rcx
	movaps	32(%rsp), %xmm0
	movups	%xmm0, 36(%rcx)
	movups	(%rax), %xmm0
	movups	%xmm0, 52(%rcx)
	movl	$0, 68(%rcx)
	movl	$2, %ecx
	cmpl	$2, %r14d
	jne	.LBB6_26
	jmp	.LBB6_28
.LBB6_23:
	movl	$1, %ecx
	cmpl	$2, %r14d
	je	.LBB6_28
.LBB6_26:                               # %_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i._ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i_crit_edge.preheader.new
	subq	%rcx, %rbp
	shlq	$2, %rcx
	leaq	(%rcx,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB6_27:                               # %_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i._ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi.exit.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rdx
	movaps	32(%rsp), %xmm0
	movups	%xmm0, (%rdx,%rcx)
	movups	(%rax), %xmm0
	movups	%xmm0, 16(%rdx,%rcx)
	movl	$0, 32(%rdx,%rcx)
	movq	16(%rsp), %rdx
	movaps	32(%rsp), %xmm0
	movups	%xmm0, 36(%rdx,%rcx)
	movups	(%rax), %xmm0
	movups	%xmm0, 52(%rdx,%rcx)
	movl	$0, 68(%rdx,%rcx)
	addq	$72, %rcx
	addq	$-2, %rbp
	jne	.LBB6_27
.LBB6_28:                               # %.loopexit
	movl	%r14d, 4(%rsp)
	testl	%r14d, %r14d
	jle	.LBB6_3
# BB#29:                                # %.lr.ph
	movq	16(%rsp), %rdx
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_30:                               # =>This Inner Loop Header: Depth=1
	movq	88(%r12), %rdi
	movq	(%rdi), %rax
	addq	%rbx, %rdx
.Ltmp7:
	movl	%ebp, %esi
	callq	*32(%rax)
.Ltmp8:
# BB#31:                                #   in Loop: Header=BB6_30 Depth=1
	movq	16(%rsp), %rdx
	movl	%ebp, 32(%rdx,%rbx)
	incq	%rbp
	movslq	4(%rsp), %rax
	addq	$36, %rbx
	cmpq	%rax, %rbp
	jl	.LBB6_30
.LBB6_3:                                # %._crit_edge
.Ltmp10:
	movq	%rsp, %rsi
	movq	%r12, %rdi
	callq	_ZN18btQuantizedBvhTree10build_treeER18GIM_BVH_DATA_ARRAY
.Ltmp11:
# BB#4:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_8
# BB#5:
	cmpb	$0, 24(%rsp)
	je	.LBB6_7
# BB#6:
	callq	_Z21btAlignedFreeInternalPv
.LBB6_7:
	movq	$0, 16(%rsp)
.LBB6_8:                                # %_ZN20btAlignedObjectArrayI12GIM_BVH_DATAED2Ev.exit15
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_32:
.Ltmp6:
	jmp	.LBB6_35
.LBB6_34:
.Ltmp12:
	jmp	.LBB6_35
.LBB6_33:
.Ltmp9:
.LBB6_35:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_39
# BB#36:
	cmpb	$0, 24(%rsp)
	je	.LBB6_38
# BB#37:
.Ltmp13:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp14:
.LBB6_38:                               # %.noexc
	movq	$0, 16(%rsp)
.LBB6_39:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_40:
.Ltmp15:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN21btGImpactQuantizedBvh8buildSetEv, .Lfunc_end6-_ZN21btGImpactQuantizedBvh8buildSetEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp5-.Ltmp2           #   Call between .Ltmp2 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp13-.Ltmp11         #   Call between .Ltmp11 and .Ltmp13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	1                       #   On action: 1
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Lfunc_end6-.Ltmp14     #   Call between .Ltmp14 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end7:
	.size	__clang_call_terminate, .Lfunc_end7-__clang_call_terminate

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_0:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZNK21btGImpactQuantizedBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE
	.p2align	4, 0x90
	.type	_ZNK21btGImpactQuantizedBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE,@function
_ZNK21btGImpactQuantizedBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE: # @_ZNK21btGImpactQuantizedBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi70:
	.cfi_def_cfa_offset 128
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.LBB8_47
# BB#1:                                 # %.lr.ph
	movss	40(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm0
	maxss	(%rsi), %xmm0
	movss	44(%rbx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm6
	maxss	4(%rsi), %xmm6
	movss	48(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm7
	maxss	8(%rsi), %xmm7
	movss	56(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	minss	%xmm0, %xmm2
	movss	60(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	minss	%xmm6, %xmm1
	movss	64(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	minss	%xmm7, %xmm0
	subss	%xmm10, %xmm2
	subss	%xmm11, %xmm1
	subss	%xmm5, %xmm0
	movss	72(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	movss	76(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm1
	movss	80(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	movss	.LCPI8_0(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm2
	cvttss2si	%xmm2, %r8d
	addss	%xmm7, %xmm1
	cvttss2si	%xmm1, %r9d
	addss	%xmm7, %xmm0
	cvttss2si	%xmm0, %r10d
	movaps	%xmm10, %xmm0
	maxss	16(%rsi), %xmm0
	movaps	%xmm11, %xmm1
	maxss	20(%rsi), %xmm1
	movaps	%xmm5, %xmm2
	maxss	24(%rsi), %xmm2
	minss	%xmm0, %xmm3
	minss	%xmm1, %xmm4
	minss	%xmm2, %xmm6
	subss	%xmm10, %xmm3
	subss	%xmm11, %xmm4
	subss	%xmm5, %xmm6
	mulss	%xmm8, %xmm3
	mulss	%xmm9, %xmm4
	mulss	%xmm12, %xmm6
	addss	%xmm7, %xmm3
	cvttss2si	%xmm3, %r15d
	addss	%xmm7, %xmm4
	cvttss2si	%xmm4, %esi
	addss	%xmm7, %xmm6
	cvttss2si	%xmm6, %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	leaq	4(%rdx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	jmp	.LBB8_37
.LBB8_2:                                # %_ZNK18btQuantizedBvhTree24testQuantizedBoxOverlappEiPtS0_.exit
                                        #   in Loop: Header=BB8_37 Depth=1
	movl	12(%rax,%r13), %ecx
	testl	%ecx, %ecx
	setns	%r12b
	cmpw	%r10w, 10(%rax,%r13)
	setae	%bpl
	jb	.LBB8_43
# BB#3:                                 # %_ZNK18btQuantizedBvhTree24testQuantizedBoxOverlappEiPtS0_.exit
                                        #   in Loop: Header=BB8_37 Depth=1
	testl	%ecx, %ecx
	js	.LBB8_43
# BB#4:                                 #   in Loop: Header=BB8_37 Depth=1
	movl	4(%rdx), %eax
	cmpl	8(%rdx), %eax
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	jne	.LBB8_36
# BB#5:                                 #   in Loop: Header=BB8_37 Depth=1
	leal	(%rax,%rax), %r11d
	testl	%eax, %eax
	movl	$1, %ecx
	cmovel	%ecx, %r11d
	cmpl	%r11d, %eax
	jge	.LBB8_36
# BB#6:                                 #   in Loop: Header=BB8_37 Depth=1
	testl	%r11d, %r11d
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movl	%edi, 44(%rsp)          # 4-byte Spill
	movl	%r8d, 40(%rsp)          # 4-byte Spill
	movl	%r9d, 36(%rsp)          # 4-byte Spill
	movl	%r10d, 32(%rsp)         # 4-byte Spill
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movl	%r11d, 4(%rsp)          # 4-byte Spill
	je	.LBB8_8
# BB#7:                                 #   in Loop: Header=BB8_37 Depth=1
	movslq	%r11d, %rdi
	shlq	$2, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	%rax, %r8
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	jmp	.LBB8_9
.LBB8_8:                                #   in Loop: Header=BB8_37 Depth=1
	xorl	%r8d, %r8d
.LBB8_9:                                # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i
                                        #   in Loop: Header=BB8_37 Depth=1
	movq	16(%rdx), %rdi
	testl	%eax, %eax
	jle	.LBB8_12
# BB#10:                                # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB8_37 Depth=1
	movslq	%eax, %r9
	cmpl	$8, %eax
	jae	.LBB8_13
# BB#11:                                #   in Loop: Header=BB8_37 Depth=1
	xorl	%r10d, %r10d
	movl	4(%rsp), %r11d          # 4-byte Reload
	jmp	.LBB8_26
.LBB8_12:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i
                                        #   in Loop: Header=BB8_37 Depth=1
	testq	%rdi, %rdi
	movl	4(%rsp), %r11d          # 4-byte Reload
	jne	.LBB8_32
	jmp	.LBB8_35
.LBB8_13:                               # %min.iters.checked
                                        #   in Loop: Header=BB8_37 Depth=1
	movq	%r9, %r10
	andq	$-8, %r10
	movl	4(%rsp), %r11d          # 4-byte Reload
	je	.LBB8_17
# BB#14:                                # %vector.memcheck
                                        #   in Loop: Header=BB8_37 Depth=1
	leaq	(%rdi,%r9,4), %rcx
	cmpq	%rcx, %r8
	jae	.LBB8_18
# BB#15:                                # %vector.memcheck
                                        #   in Loop: Header=BB8_37 Depth=1
	leaq	(%r8,%r9,4), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB8_18
.LBB8_17:                               #   in Loop: Header=BB8_37 Depth=1
	xorl	%r10d, %r10d
.LBB8_26:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB8_37 Depth=1
	subl	%r10d, %eax
	leaq	-1(%r9), %rcx
	subq	%r10, %rcx
	andq	$7, %rax
	je	.LBB8_29
# BB#27:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB8_37 Depth=1
	negq	%rax
	.p2align	4, 0x90
.LBB8_28:                               # %scalar.ph.prol
                                        #   Parent Loop BB8_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%r10,4), %esi
	movl	%esi, (%r8,%r10,4)
	incq	%r10
	incq	%rax
	jne	.LBB8_28
.LBB8_29:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB8_37 Depth=1
	cmpq	$7, %rcx
	jb	.LBB8_32
# BB#30:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB8_37 Depth=1
	subq	%r10, %r9
	leaq	28(%rdi,%r10,4), %rax
	leaq	28(%r8,%r10,4), %rcx
	.p2align	4, 0x90
.LBB8_31:                               # %scalar.ph
                                        #   Parent Loop BB8_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rax), %esi
	movl	%esi, -28(%rcx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rcx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rcx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rcx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rcx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rcx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rcx)
	movl	(%rax), %esi
	movl	%esi, (%rcx)
	addq	$32, %rax
	addq	$32, %rcx
	addq	$-8, %r9
	jne	.LBB8_31
.LBB8_32:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i
                                        #   in Loop: Header=BB8_37 Depth=1
	cmpb	$0, 24(%rdx)
	je	.LBB8_34
# BB#33:                                #   in Loop: Header=BB8_37 Depth=1
	movq	%r8, 8(%rsp)            # 8-byte Spill
	callq	_Z21btAlignedFreeInternalPv
	movq	8(%rsp), %r8            # 8-byte Reload
	movl	4(%rsp), %r11d          # 4-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
.LBB8_34:                               #   in Loop: Header=BB8_37 Depth=1
	movq	$0, 16(%rdx)
	movl	4(%rdx), %eax
.LBB8_35:                               # %_ZN20btAlignedObjectArrayIiE10deallocateEv.exit.i.i
                                        #   in Loop: Header=BB8_37 Depth=1
	movb	$1, 24(%rdx)
	movq	%r8, 16(%rdx)
	movl	%r11d, 8(%rdx)
	movl	44(%rsp), %edi          # 4-byte Reload
	movl	40(%rsp), %r8d          # 4-byte Reload
	movl	36(%rsp), %r9d          # 4-byte Reload
	movl	32(%rsp), %r10d         # 4-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
.LBB8_36:                               # %_ZN20btAlignedObjectArrayIiE9push_backERKi.exit
                                        #   in Loop: Header=BB8_37 Depth=1
	movq	16(%rdx), %rcx
	cltq
	movl	48(%rsp), %r11d         # 4-byte Reload
	movl	%r11d, (%rcx,%rax,4)
	incl	4(%rdx)
	jmp	.LBB8_43
.LBB8_18:                               # %vector.body.preheader
                                        #   in Loop: Header=BB8_37 Depth=1
	movq	%r8, 8(%rsp)            # 8-byte Spill
	leaq	-8(%r10), %rsi
	movq	%rsi, 64(%rsp)          # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB8_21
# BB#19:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB8_37 Depth=1
	negq	%rsi
	xorl	%ecx, %ecx
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB8_20:                               # %vector.body.prol
                                        #   Parent Loop BB8_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rcx,4), %xmm0
	movups	16(%rdi,%rcx,4), %xmm1
	movups	%xmm0, (%r8,%rcx,4)
	movups	%xmm1, 16(%r8,%rcx,4)
	addq	$8, %rcx
	incq	%rsi
	jne	.LBB8_20
	jmp	.LBB8_22
.LBB8_21:                               #   in Loop: Header=BB8_37 Depth=1
	xorl	%ecx, %ecx
.LBB8_22:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB8_37 Depth=1
	cmpq	$24, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB8_25
# BB#23:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB8_37 Depth=1
	movq	%r10, %r8
	subq	%rcx, %r8
	leaq	112(%rdi,%rcx,4), %rsi
	movq	8(%rsp), %r11           # 8-byte Reload
	leaq	112(%r11,%rcx,4), %rcx
	movl	4(%rsp), %r11d          # 4-byte Reload
.LBB8_24:                               # %vector.body
                                        #   Parent Loop BB8_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	subq	$-128, %rsi
	subq	$-128, %rcx
	addq	$-32, %r8
	jne	.LBB8_24
.LBB8_25:                               # %middle.block
                                        #   in Loop: Header=BB8_37 Depth=1
	cmpq	%r10, %r9
	movq	8(%rsp), %r8            # 8-byte Reload
	je	.LBB8_32
	jmp	.LBB8_26
	.p2align	4, 0x90
.LBB8_37:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_20 Depth 2
                                        #     Child Loop BB8_24 Depth 2
                                        #     Child Loop BB8_28 Depth 2
                                        #     Child Loop BB8_31 Depth 2
	movq	24(%rbx), %rax
	movslq	%r14d, %r13
	shlq	$4, %r13
	cmpw	%r15w, (%rax,%r13)
	ja	.LBB8_42
# BB#38:                                #   in Loop: Header=BB8_37 Depth=1
	cmpw	%r8w, 6(%rax,%r13)
	jb	.LBB8_42
# BB#39:                                #   in Loop: Header=BB8_37 Depth=1
	cmpw	%si, 2(%rax,%r13)
	ja	.LBB8_42
# BB#40:                                #   in Loop: Header=BB8_37 Depth=1
	cmpw	%r9w, 8(%rax,%r13)
	jb	.LBB8_42
# BB#41:                                #   in Loop: Header=BB8_37 Depth=1
	movl	52(%rsp), %ecx          # 4-byte Reload
	cmpw	%cx, 4(%rax,%r13)
	jbe	.LBB8_2
	.p2align	4, 0x90
.LBB8_42:                               # %_ZNK18btQuantizedBvhTree24testQuantizedBoxOverlappEiPtS0_.exit.thread
                                        #   in Loop: Header=BB8_37 Depth=1
	cmpl	$0, 12(%rax,%r13)
	setns	%r12b
	xorl	%ebp, %ebp
.LBB8_43:                               #   in Loop: Header=BB8_37 Depth=1
	movl	$1, %eax
	testb	%bpl, %bpl
	jne	.LBB8_46
# BB#44:                                #   in Loop: Header=BB8_37 Depth=1
	testb	%r12b, %r12b
	jne	.LBB8_46
# BB#45:                                #   in Loop: Header=BB8_37 Depth=1
	movq	24(%rbx), %rcx
	xorl	%eax, %eax
	subl	12(%rcx,%r13), %eax
.LBB8_46:                               #   in Loop: Header=BB8_37 Depth=1
	addl	%eax, %r14d
	cmpl	%edi, %r14d
	jl	.LBB8_37
	jmp	.LBB8_48
.LBB8_47:                               # %.._crit_edge_crit_edge
	addq	$4, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
.LBB8_48:                               # %._crit_edge
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	$0, (%rax)
	setg	%al
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZNK21btGImpactQuantizedBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE, .Lfunc_end8-_ZNK21btGImpactQuantizedBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	1056964608              # float 0.5
.LCPI9_3:
	.long	0                       # float 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_1:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
.LCPI9_2:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZNK21btGImpactQuantizedBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE
	.p2align	4, 0x90
	.type	_ZNK21btGImpactQuantizedBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE,@function
_ZNK21btGImpactQuantizedBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE: # @_ZNK21btGImpactQuantizedBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi83:
	.cfi_def_cfa_offset 112
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movq	%rcx, %r10
	movq	%rdx, %r12
	movq	%rdi, %rbx
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.LBB9_50
# BB#1:                                 # %.lr.ph
	leaq	4(%r10), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	pxor	%xmm13, %xmm13
	movss	.LCPI9_0(%rip), %xmm14  # xmm14 = mem[0],zero,zero,zero
	movaps	.LCPI9_1(%rip), %xmm15  # xmm15 = <0.5,0.5,u,u>
	movl	$1, %r8d
	jmp	.LBB9_37
.LBB9_2:                                # %_ZNK6btAABB11collide_rayERK9btVector3S2_.exit
                                        #   in Loop: Header=BB9_37 Depth=1
	mulss	%xmm9, %xmm12
	mulss	%xmm8, %xmm10
	subss	%xmm10, %xmm12
	andps	.LCPI9_2(%rip), %xmm12
	shufps	$229, %xmm11, %xmm11    # xmm11 = xmm11[1,1,2,3]
	mulss	%xmm11, %xmm0
	mulss	%xmm2, %xmm7
	addss	%xmm0, %xmm7
	movl	12(%rax,%r13), %edi
	testl	%edi, %edi
	setns	%bpl
	ucomiss	%xmm7, %xmm12
	setbe	%r15b
	ja	.LBB9_46
# BB#3:                                 # %_ZNK6btAABB11collide_rayERK9btVector3S2_.exit
                                        #   in Loop: Header=BB9_37 Depth=1
	testl	%edi, %edi
	js	.LBB9_46
# BB#4:                                 #   in Loop: Header=BB9_37 Depth=1
	movl	4(%r10), %r11d
	cmpl	8(%r10), %r11d
	jne	.LBB9_36
# BB#5:                                 #   in Loop: Header=BB9_37 Depth=1
	leal	(%r11,%r11), %ecx
	testl	%r11d, %r11d
	cmovel	%r8d, %ecx
	cmpl	%ecx, %r11d
	jge	.LBB9_36
# BB#6:                                 #   in Loop: Header=BB9_37 Depth=1
	testl	%ecx, %ecx
	movq	%r10, 40(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movl	%edi, 32(%rsp)          # 4-byte Spill
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	je	.LBB9_8
# BB#7:                                 #   in Loop: Header=BB9_37 Depth=1
	movslq	%ecx, %rdi
	shlq	$2, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movaps	.LCPI9_1(%rip), %xmm15  # xmm15 = <0.5,0.5,u,u>
	movss	.LCPI9_0(%rip), %xmm14  # xmm14 = mem[0],zero,zero,zero
	pxor	%xmm13, %xmm13
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %r11d
	jmp	.LBB9_9
.LBB9_8:                                #   in Loop: Header=BB9_37 Depth=1
	xorl	%eax, %eax
.LBB9_9:                                # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i
                                        #   in Loop: Header=BB9_37 Depth=1
	movq	16(%r10), %rdi
	testl	%r11d, %r11d
	jle	.LBB9_12
# BB#10:                                # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB9_37 Depth=1
	movslq	%r11d, %r9
	cmpl	$8, %r11d
	jae	.LBB9_13
# BB#11:                                #   in Loop: Header=BB9_37 Depth=1
	xorl	%edx, %edx
	jmp	.LBB9_26
.LBB9_12:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i
                                        #   in Loop: Header=BB9_37 Depth=1
	testq	%rdi, %rdi
	jne	.LBB9_32
	jmp	.LBB9_35
.LBB9_13:                               # %min.iters.checked
                                        #   in Loop: Header=BB9_37 Depth=1
	movq	%r9, %rdx
	andq	$-8, %rdx
	je	.LBB9_17
# BB#14:                                # %vector.memcheck
                                        #   in Loop: Header=BB9_37 Depth=1
	leaq	(%rdi,%r9,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB9_18
# BB#15:                                # %vector.memcheck
                                        #   in Loop: Header=BB9_37 Depth=1
	leaq	(%rax,%r9,4), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB9_18
.LBB9_17:                               #   in Loop: Header=BB9_37 Depth=1
	xorl	%edx, %edx
.LBB9_26:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB9_37 Depth=1
	subl	%edx, %r11d
	leaq	-1(%r9), %rcx
	subq	%rdx, %rcx
	andq	$7, %r11
	je	.LBB9_29
# BB#27:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB9_37 Depth=1
	negq	%r11
	.p2align	4, 0x90
.LBB9_28:                               # %scalar.ph.prol
                                        #   Parent Loop BB9_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rdx,4), %esi
	movl	%esi, (%rax,%rdx,4)
	incq	%rdx
	incq	%r11
	jne	.LBB9_28
.LBB9_29:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB9_37 Depth=1
	cmpq	$7, %rcx
	jb	.LBB9_32
# BB#30:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB9_37 Depth=1
	subq	%rdx, %r9
	leaq	28(%rdi,%rdx,4), %rsi
	leaq	28(%rax,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB9_31:                               # %scalar.ph
                                        #   Parent Loop BB9_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rsi), %edx
	movl	%edx, -28(%rcx)
	movl	-24(%rsi), %edx
	movl	%edx, -24(%rcx)
	movl	-20(%rsi), %edx
	movl	%edx, -20(%rcx)
	movl	-16(%rsi), %edx
	movl	%edx, -16(%rcx)
	movl	-12(%rsi), %edx
	movl	%edx, -12(%rcx)
	movl	-8(%rsi), %edx
	movl	%edx, -8(%rcx)
	movl	-4(%rsi), %edx
	movl	%edx, -4(%rcx)
	movl	(%rsi), %edx
	movl	%edx, (%rcx)
	addq	$32, %rsi
	addq	$32, %rcx
	addq	$-8, %r9
	jne	.LBB9_31
.LBB9_32:                               # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i
                                        #   in Loop: Header=BB9_37 Depth=1
	cmpb	$0, 24(%r10)
	je	.LBB9_34
# BB#33:                                #   in Loop: Header=BB9_37 Depth=1
	movq	%rax, 8(%rsp)           # 8-byte Spill
	callq	_Z21btAlignedFreeInternalPv
	movq	8(%rsp), %rax           # 8-byte Reload
	movaps	.LCPI9_1(%rip), %xmm15  # xmm15 = <0.5,0.5,u,u>
	movss	.LCPI9_0(%rip), %xmm14  # xmm14 = mem[0],zero,zero,zero
	pxor	%xmm13, %xmm13
	movq	40(%rsp), %r10          # 8-byte Reload
.LBB9_34:                               #   in Loop: Header=BB9_37 Depth=1
	movq	$0, 16(%r10)
	movl	4(%r10), %r11d
.LBB9_35:                               # %_ZN20btAlignedObjectArrayIiE10deallocateEv.exit.i.i
                                        #   in Loop: Header=BB9_37 Depth=1
	movb	$1, 24(%r10)
	movq	%rax, 16(%r10)
	movl	28(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 8(%r10)
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	$1, %r8d
	movl	32(%rsp), %edi          # 4-byte Reload
.LBB9_36:                               # %_ZN20btAlignedObjectArrayIiE9push_backERKi.exit
                                        #   in Loop: Header=BB9_37 Depth=1
	movq	16(%r10), %rcx
	movslq	%r11d, %rax
	movl	%edi, (%rcx,%rax,4)
	incl	4(%r10)
	jmp	.LBB9_46
.LBB9_18:                               # %vector.body.preheader
                                        #   in Loop: Header=BB9_37 Depth=1
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	-8(%rdx), %r8
	movl	%r8d, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB9_21
# BB#19:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB9_37 Depth=1
	negq	%rsi
	xorl	%ecx, %ecx
	movq	8(%rsp), %rax           # 8-byte Reload
	.p2align	4, 0x90
.LBB9_20:                               # %vector.body.prol
                                        #   Parent Loop BB9_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rcx,4), %xmm0
	movups	16(%rdi,%rcx,4), %xmm1
	movups	%xmm0, (%rax,%rcx,4)
	movups	%xmm1, 16(%rax,%rcx,4)
	addq	$8, %rcx
	incq	%rsi
	jne	.LBB9_20
	jmp	.LBB9_22
.LBB9_21:                               #   in Loop: Header=BB9_37 Depth=1
	xorl	%ecx, %ecx
.LBB9_22:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB9_37 Depth=1
	cmpq	$24, %r8
	jb	.LBB9_25
# BB#23:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB9_37 Depth=1
	movq	%rdx, %r8
	subq	%rcx, %r8
	leaq	112(%rdi,%rcx,4), %rsi
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	112(%rax,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB9_24:                               # %vector.body
                                        #   Parent Loop BB9_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	subq	$-128, %rsi
	subq	$-128, %rcx
	addq	$-32, %r8
	jne	.LBB9_24
.LBB9_25:                               # %middle.block
                                        #   in Loop: Header=BB9_37 Depth=1
	cmpq	%rdx, %r9
	movq	8(%rsp), %rax           # 8-byte Reload
	je	.LBB9_32
	jmp	.LBB9_26
	.p2align	4, 0x90
.LBB9_37:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_20 Depth 2
                                        #     Child Loop BB9_24 Depth 2
                                        #     Child Loop BB9_28 Depth 2
                                        #     Child Loop BB9_31 Depth 2
	movq	24(%rbx), %rax
	movslq	%r14d, %r13
	shlq	$4, %r13
	movzwl	(%rax,%r13), %ecx
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%ecx, %xmm5
	movss	72(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm5
	movd	2(%rax,%r13), %xmm2     # xmm2 = mem[0],zero,zero,zero
	movsd	76(%rbx), %xmm3         # xmm3 = mem[0],zero
	movss	40(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm5
	movsd	44(%rbx), %xmm4         # xmm4 = mem[0],zero
	movzwl	6(%rax,%r13), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	divss	%xmm1, %xmm0
	movd	8(%rax,%r13), %xmm6     # xmm6 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm0
	addss	%xmm0, %xmm5
	mulss	%xmm14, %xmm5
	subss	%xmm5, %xmm0
	movss	(%r12), %xmm10          # xmm10 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm10
	movaps	%xmm10, %xmm5
	andps	.LCPI9_2(%rip), %xmm5
	ucomiss	%xmm0, %xmm5
	jbe	.LBB9_39
# BB#38:                                #   in Loop: Header=BB9_37 Depth=1
	movss	(%rsi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm5
	ucomiss	.LCPI9_3, %xmm5
	jae	.LBB9_45
.LBB9_39:                               #   in Loop: Header=BB9_37 Depth=1
	punpcklwd	%xmm13, %xmm2   # xmm2 = xmm2[0],xmm13[0],xmm2[1],xmm13[1],xmm2[2],xmm13[2],xmm2[3],xmm13[3]
	cvtdq2ps	%xmm2, %xmm5
	divps	%xmm3, %xmm5
	addps	%xmm4, %xmm5
	punpcklwd	%xmm13, %xmm6   # xmm6 = xmm6[0],xmm13[0],xmm6[1],xmm13[1],xmm6[2],xmm13[2],xmm6[3],xmm13[3]
	cvtdq2ps	%xmm6, %xmm2
	divps	%xmm3, %xmm2
	addps	%xmm4, %xmm2
	addps	%xmm2, %xmm5
	mulps	%xmm15, %xmm5
	subps	%xmm5, %xmm2
	movss	4(%r12), %xmm12         # xmm12 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm12
	movaps	%xmm12, %xmm4
	andps	.LCPI9_2(%rip), %xmm4
	ucomiss	%xmm2, %xmm4
	jbe	.LBB9_41
# BB#40:                                #   in Loop: Header=BB9_37 Depth=1
	movss	4(%rsi), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm4
	ucomiss	.LCPI9_3, %xmm4
	jae	.LBB9_45
.LBB9_41:                               #   in Loop: Header=BB9_37 Depth=1
	movss	8(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	subss	%xmm5, %xmm1
	movaps	%xmm1, %xmm5
	andps	.LCPI9_2(%rip), %xmm5
	movaps	%xmm2, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	ucomiss	%xmm4, %xmm5
	movss	8(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	jbe	.LBB9_43
# BB#42:                                #   in Loop: Header=BB9_37 Depth=1
	movaps	%xmm1, %xmm6
	mulss	%xmm5, %xmm6
	ucomiss	.LCPI9_3, %xmm6
	jae	.LBB9_45
.LBB9_43:                               # %._crit_edge.i
                                        #   in Loop: Header=BB9_37 Depth=1
	movss	4(%rsi), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm7
	mulss	%xmm8, %xmm7
	movaps	%xmm12, %xmm6
	mulss	%xmm5, %xmm6
	subss	%xmm6, %xmm7
	movaps	.LCPI9_2(%rip), %xmm3   # xmm3 = [nan,nan,nan,nan]
	andps	%xmm3, %xmm7
	movaps	%xmm5, %xmm11
	unpcklps	%xmm8, %xmm11   # xmm11 = xmm11[0],xmm8[0],xmm11[1],xmm8[1]
	andps	%xmm3, %xmm11
	movaps	%xmm2, %xmm3
	mulps	%xmm11, %xmm3
	movaps	%xmm3, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	addss	%xmm3, %xmm6
	ucomiss	%xmm6, %xmm7
	ja	.LBB9_45
# BB#44:                                #   in Loop: Header=BB9_37 Depth=1
	mulss	%xmm10, %xmm5
	movss	(%rsi), %xmm9           # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm1
	subss	%xmm1, %xmm5
	movaps	.LCPI9_2(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	movaps	%xmm1, %xmm3
	andps	%xmm3, %xmm5
	movaps	%xmm0, %xmm1
	mulss	%xmm11, %xmm1
	movaps	%xmm9, %xmm7
	andps	%xmm3, %xmm7
	mulss	%xmm7, %xmm4
	addss	%xmm1, %xmm4
	ucomiss	%xmm4, %xmm5
	jbe	.LBB9_2
	.p2align	4, 0x90
.LBB9_45:                               # %_ZNK6btAABB11collide_rayERK9btVector3S2_.exit.thread
                                        #   in Loop: Header=BB9_37 Depth=1
	cmpl	$0, 12(%rax,%r13)
	setns	%bpl
	xorl	%r15d, %r15d
.LBB9_46:                               #   in Loop: Header=BB9_37 Depth=1
	movl	$1, %eax
	testb	%r15b, %r15b
	jne	.LBB9_49
# BB#47:                                #   in Loop: Header=BB9_37 Depth=1
	testb	%bpl, %bpl
	jne	.LBB9_49
# BB#48:                                #   in Loop: Header=BB9_37 Depth=1
	movq	24(%rbx), %rcx
	xorl	%eax, %eax
	subl	12(%rcx,%r13), %eax
.LBB9_49:                               #   in Loop: Header=BB9_37 Depth=1
	addl	%eax, %r14d
	cmpl	%edx, %r14d
	jl	.LBB9_37
	jmp	.LBB9_51
.LBB9_50:                               # %.._crit_edge_crit_edge
	addq	$4, %r10
	movq	%r10, 16(%rsp)          # 8-byte Spill
.LBB9_51:                               # %._crit_edge
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	$0, (%rax)
	setg	%al
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_ZNK21btGImpactQuantizedBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE, .Lfunc_end9-_ZNK21btGImpactQuantizedBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE
	.cfi_endproc

	.globl	_ZN21btGImpactQuantizedBvh14find_collisionEPS_RK11btTransformS0_S3_R9btPairSet
	.p2align	4, 0x90
	.type	_ZN21btGImpactQuantizedBvh14find_collisionEPS_RK11btTransformS0_S3_R9btPairSet,@function
_ZN21btGImpactQuantizedBvh14find_collisionEPS_RK11btTransformS0_S3_R9btPairSet: # @_ZN21btGImpactQuantizedBvh14find_collisionEPS_RK11btTransformS0_S3_R9btPairSet
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi92:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 40
	subq	$120, %rsp
.Lcfi94:
	.cfi_def_cfa_offset 160
.Lcfi95:
	.cfi_offset %rbx, -40
.Lcfi96:
	.cfi_offset %r12, -32
.Lcfi97:
	.cfi_offset %r14, -24
.Lcfi98:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	cmpl	$0, (%rbx)
	je	.LBB10_3
# BB#1:
	cmpl	$0, (%r15)
	je	.LBB10_3
# BB#2:
	leaq	8(%rsp), %r12
	movq	%r12, %rdi
	movq	%rcx, %rdx
	callq	_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_
	movl	$1, (%rsp)
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r12, %rcx
	callq	_ZL41_find_quantized_collision_pairs_recursiveP21btGImpactQuantizedBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
.LBB10_3:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	_ZN21btGImpactQuantizedBvh14find_collisionEPS_RK11btTransformS0_S3_R9btPairSet, .Lfunc_end10-_ZN21btGImpactQuantizedBvh14find_collisionEPS_RK11btTransformS0_S3_R9btPairSet
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI11_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_2:
	.long	897988541               # float 9.99999997E-7
	.section	.text._ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_,"axG",@progbits,_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_,comdat
	.weak	_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_
	.p2align	4, 0x90
	.type	_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_,@function
_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_: # @_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 16
	movsd	(%rsi), %xmm11          # xmm11 = mem[0],zero
	movsd	16(%rsi), %xmm9         # xmm9 = mem[0],zero
	movsd	32(%rsi), %xmm12        # xmm12 = mem[0],zero
	movss	8(%rsi), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movss	%xmm7, -104(%rsp)       # 4-byte Spill
	movss	24(%rsi), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	40(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, -100(%rsp)       # 4-byte Spill
	movd	48(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movdqa	.LCPI11_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm0, %xmm5
	movd	52(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movdqa	%xmm1, %xmm2
	pxor	%xmm0, %xmm2
	movd	56(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	pxor	%xmm3, %xmm0
	pshufd	$224, %xmm5, %xmm4      # xmm4 = xmm5[0,0,2,3]
	mulps	%xmm11, %xmm4
	pshufd	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm9, %xmm2
	addps	%xmm4, %xmm2
	pshufd	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm12, %xmm0
	addps	%xmm2, %xmm0
	movaps	%xmm0, -16(%rsp)        # 16-byte Spill
	mulss	%xmm7, %xmm5
	mulss	%xmm10, %xmm1
	subss	%xmm1, %xmm5
	mulss	%xmm6, %xmm3
	subss	%xmm3, %xmm5
	movaps	%xmm5, -32(%rsp)        # 16-byte Spill
	movss	(%rdx), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, -112(%rsp)       # 4-byte Spill
	movaps	%xmm11, %xmm3
	movaps	%xmm3, %xmm0
	mulss	%xmm8, %xmm0
	movss	%xmm8, -108(%rsp)       # 4-byte Spill
	movss	16(%rdx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm4
	movaps	%xmm4, %xmm1
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	movss	32(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, -96(%rsp)        # 16-byte Spill
	movaps	%xmm12, %xmm2
	movaps	%xmm2, %xmm15
	mulss	%xmm0, %xmm15
	addss	%xmm1, %xmm15
	movaps	%xmm3, %xmm0
	movaps	%xmm3, %xmm5
	mulss	%xmm6, %xmm0
	movss	20(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm3
	mulss	%xmm6, %xmm3
	addss	%xmm0, %xmm3
	movss	36(%rdx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm13
	movaps	%xmm2, %xmm1
	movaps	%xmm1, -48(%rsp)        # 16-byte Spill
	mulss	%xmm14, %xmm13
	addss	%xmm3, %xmm13
	movss	8(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, -116(%rsp)       # 4-byte Spill
	movaps	%xmm5, %xmm2
	movaps	%xmm2, -80(%rsp)        # 16-byte Spill
	movaps	%xmm2, %xmm3
	mulss	%xmm0, %xmm3
	movss	24(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm0
	movaps	%xmm0, -64(%rsp)        # 16-byte Spill
	mulss	%xmm5, %xmm4
	addss	%xmm3, %xmm4
	movss	40(%rdx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm11
	mulss	%xmm12, %xmm11
	addss	%xmm4, %xmm11
	pshufd	$229, %xmm2, %xmm4      # xmm4 = xmm2[1,1,2,3]
	movdqa	%xmm4, %xmm3
	mulss	%xmm8, %xmm3
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	movdqa	%xmm2, %xmm0
	mulss	%xmm7, %xmm0
	addss	%xmm3, %xmm0
	pshufd	$229, %xmm1, %xmm8      # xmm8 = xmm1[1,1,2,3]
	movdqa	%xmm8, %xmm9
	mulss	-96(%rsp), %xmm9        # 16-byte Folded Reload
	addss	%xmm0, %xmm9
	movdqa	%xmm4, %xmm0
	mulss	-112(%rsp), %xmm0       # 4-byte Folded Reload
	movdqa	%xmm2, %xmm1
	mulss	%xmm6, %xmm1
	addss	%xmm0, %xmm1
	movdqa	%xmm8, %xmm3
	mulss	%xmm14, %xmm3
	addss	%xmm1, %xmm3
	mulss	-116(%rsp), %xmm4       # 4-byte Folded Reload
	mulss	%xmm5, %xmm2
	addss	%xmm4, %xmm2
	mulss	%xmm12, %xmm8
	addss	%xmm2, %xmm8
	movss	-104(%rsp), %xmm1       # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	-108(%rsp), %xmm0       # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	mulss	%xmm10, %xmm7
	addss	%xmm0, %xmm7
	movaps	-96(%rsp), %xmm0        # 16-byte Reload
	movss	-100(%rsp), %xmm4       # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	addss	%xmm7, %xmm0
	movaps	%xmm0, %xmm2
	movss	-112(%rsp), %xmm0       # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	mulss	%xmm10, %xmm6
	addss	%xmm0, %xmm6
	mulss	%xmm4, %xmm14
	movaps	%xmm4, %xmm7
	addss	%xmm6, %xmm14
	movss	-116(%rsp), %xmm0       # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm4
	mulss	%xmm10, %xmm5
	addss	%xmm0, %xmm5
	mulss	%xmm7, %xmm12
	addss	%xmm5, %xmm12
	movss	48(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	-80(%rsp), %xmm0        # 16-byte Folded Reload
	movss	52(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm10
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	-64(%rsp), %xmm1        # 16-byte Folded Reload
	addps	%xmm0, %xmm1
	movss	56(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	-48(%rsp), %xmm0        # 16-byte Folded Reload
	addps	%xmm1, %xmm0
	addps	-16(%rsp), %xmm0        # 16-byte Folded Reload
	addss	%xmm4, %xmm10
	addss	%xmm7, %xmm10
	addss	-32(%rsp), %xmm10       # 16-byte Folded Reload
	xorps	%xmm1, %xmm1
	movss	%xmm10, %xmm1           # xmm1 = xmm10[0],xmm1[1,2,3]
	movlps	%xmm0, (%rdi)
	movlps	%xmm1, 8(%rdi)
	movss	%xmm15, 16(%rdi)
	movss	%xmm13, 20(%rdi)
	movss	%xmm11, 24(%rdi)
	movl	$0, 28(%rdi)
	movss	%xmm9, 32(%rdi)
	movss	%xmm3, 36(%rdi)
	movss	%xmm8, 40(%rdi)
	movl	$0, 44(%rdi)
	movss	%xmm2, 48(%rdi)
	movss	%xmm14, 52(%rdi)
	movss	%xmm12, 56(%rdi)
	movl	$0, 60(%rdi)
	movaps	.LCPI11_1(%rip), %xmm0  # xmm0 = [nan,nan,nan,nan]
	andps	%xmm0, %xmm15
	movss	.LCPI11_2(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm15
	movss	%xmm15, 64(%rdi)
	andps	%xmm0, %xmm13
	addss	%xmm1, %xmm13
	movss	%xmm13, 68(%rdi)
	andps	%xmm0, %xmm11
	addss	%xmm1, %xmm11
	movss	%xmm11, 72(%rdi)
	andps	%xmm0, %xmm9
	addss	%xmm1, %xmm9
	movss	%xmm9, 80(%rdi)
	andps	%xmm0, %xmm3
	addss	%xmm1, %xmm3
	movss	%xmm3, 84(%rdi)
	andps	%xmm0, %xmm8
	addss	%xmm1, %xmm8
	movss	%xmm8, 88(%rdi)
	andps	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	movss	%xmm2, 96(%rdi)
	andps	%xmm0, %xmm14
	addss	%xmm1, %xmm14
	movss	%xmm14, 100(%rdi)
	andps	%xmm0, %xmm12
	addss	%xmm1, %xmm12
	movss	%xmm12, 104(%rdi)
	popq	%rax
	retq
.Lfunc_end11:
	.size	_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_, .Lfunc_end11-_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZL41_find_quantized_collision_pairs_recursiveP21btGImpactQuantizedBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib,@function
_ZL41_find_quantized_collision_pairs_recursiveP21btGImpactQuantizedBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib: # @_ZL41_find_quantized_collision_pairs_recursiveP21btGImpactQuantizedBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi102:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi103:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi104:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi106:
	.cfi_def_cfa_offset 176
.Lcfi107:
	.cfi_offset %rbx, -56
.Lcfi108:
	.cfi_offset %r12, -48
.Lcfi109:
	.cfi_offset %r13, -40
.Lcfi110:
	.cfi_offset %r14, -32
.Lcfi111:
	.cfi_offset %r15, -24
.Lcfi112:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movl	%r8d, %r14d
	movq	%rcx, %rax
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movb	176(%rsp), %sil
	movq	24(%rbx), %rcx
	movslq	%r14d, %rbp
	shlq	$4, %rbp
	movzwl	(%rcx,%rbp), %edx
	cvtsi2ssl	%edx, %xmm3
	movss	72(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	divss	%xmm9, %xmm3
	movzwl	2(%rcx,%rbp), %edx
	cvtsi2ssl	%edx, %xmm0
	movss	76(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	divss	%xmm2, %xmm0
	movzwl	4(%rcx,%rbp), %edx
	cvtsi2ssl	%edx, %xmm4
	movss	80(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	divss	%xmm5, %xmm4
	movss	40(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm3
	movss	44(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm0
	unpcklps	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	movss	48(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	xorps	%xmm8, %xmm8
	xorps	%xmm1, %xmm1
	shufps	$1, %xmm4, %xmm1        # xmm1 = xmm1[1,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm1      # xmm1 = xmm1[2,0],xmm4[2,3]
	movlps	%xmm3, 88(%rsp)
	movlps	%xmm1, 96(%rsp)
	movzwl	6(%rcx,%rbp), %edx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%edx, %xmm1
	divss	%xmm9, %xmm1
	movzwl	8(%rcx,%rbp), %edx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%edx, %xmm3
	divss	%xmm2, %xmm3
	movzwl	10(%rcx,%rbp), %ecx
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ecx, %xmm2
	divss	%xmm5, %xmm2
	addss	%xmm6, %xmm1
	addss	%xmm7, %xmm3
	unpcklps	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	addss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	shufps	$1, %xmm2, %xmm0        # xmm0 = xmm0[1,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm0      # xmm0 = xmm0[2,0],xmm2[2,3]
	movlps	%xmm1, 104(%rsp)
	movlps	%xmm0, 112(%rsp)
	movq	24(%r13), %rcx
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movslq	%r9d, %r12
	shlq	$4, %r12
	movzwl	(%rcx,%r12), %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	movss	72(%r13), %xmm9         # xmm9 = mem[0],zero,zero,zero
	divss	%xmm9, %xmm0
	movzwl	2(%rcx,%r12), %edx
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%edx, %xmm2
	movss	76(%r13), %xmm3         # xmm3 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm2
	movzwl	4(%rcx,%r12), %edx
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%edx, %xmm4
	movss	80(%r13), %xmm5         # xmm5 = mem[0],zero,zero,zero
	divss	%xmm5, %xmm4
	movss	40(%r13), %xmm6         # xmm6 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm0
	movss	44(%r13), %xmm7         # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm2
	unpcklps	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	movss	48(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm4
	xorps	%xmm1, %xmm1
	shufps	$1, %xmm4, %xmm1        # xmm1 = xmm1[1,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm1      # xmm1 = xmm1[2,0],xmm4[2,3]
	movlps	%xmm0, 56(%rsp)
	movlps	%xmm1, 64(%rsp)
	movzwl	6(%rcx,%r12), %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	divss	%xmm9, %xmm0
	movzwl	8(%rcx,%r12), %edx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%edx, %xmm1
	divss	%xmm3, %xmm1
	movzwl	10(%rcx,%r12), %ecx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ecx, %xmm3
	divss	%xmm5, %xmm3
	addss	%xmm6, %xmm0
	addss	%xmm7, %xmm1
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	addss	%xmm2, %xmm3
	shufps	$1, %xmm3, %xmm8        # xmm8 = xmm8[1,0],xmm3[0,0]
	shufps	$226, %xmm3, %xmm8      # xmm8 = xmm8[2,0],xmm3[2,3]
	movlps	%xmm0, 72(%rsp)
	movlps	%xmm8, 80(%rsp)
	movzbl	%sil, %ecx
	leaq	88(%rsp), %rdi
	leaq	56(%rsp), %rsi
	movq	%rax, %r15
	movq	%rax, %rdx
	callq	_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb
	testb	%al, %al
	je	.LBB12_26
# BB#1:
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	24(%rbx), %rax
	movl	12(%rax,%rbp), %ebp
	movq	24(%r13), %rax
	movl	12(%rax,%r12), %edi
	testl	%ebp, %ebp
	js	.LBB12_22
# BB#2:
	testl	%edi, %edi
	movq	%rdx, %rax
	js	.LBB12_21
# BB#3:
	movq	%rcx, %rdx
	movl	4(%rdx), %r9d
	cmpl	8(%rdx), %r9d
	jne	.LBB12_20
# BB#4:
	movq	%rdx, %r15
	leal	(%r9,%r9), %ecx
	testl	%r9d, %r9d
	movl	$1, %r14d
	cmovnel	%ecx, %r14d
	cmpl	%r14d, %r9d
	jge	.LBB12_5
# BB#6:
	movl	%edi, %r12d
	testl	%r14d, %r14d
	je	.LBB12_7
# BB#8:
	movslq	%r14d, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movq	%r15, %rax
	movl	4(%rax), %r9d
	jmp	.LBB12_9
.LBB12_22:
	movq	%rcx, %rax
	movq	%r14, %rsi
	leal	1(%rsi), %r14d
	testl	%edi, %edi
	movq	%r15, %rcx
	movq	%rdx, %rbp
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	js	.LBB12_24
# BB#23:
	movl	$0, (%rsp)
	movq	%rbx, %rdi
	movq	%rsi, %r15
	movq	%r13, %rsi
	movq	%rax, %rdx
	movl	%r14d, %r8d
	movq	%rbp, %r12
	movl	%r12d, %r9d
	callq	_ZL41_find_quantized_collision_pairs_recursiveP21btGImpactQuantizedBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	movq	24(%rbx), %rax
	movslq	%r14d, %rcx
	shlq	$4, %rcx
	movl	12(%rax,%rcx), %eax
	addl	$2, %r15d
	subl	%eax, %r14d
	testl	%eax, %eax
	cmovnsl	%r15d, %r14d
	movl	$0, (%rsp)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%r14d, %r8d
	movl	%r12d, %r9d
	jmp	.LBB12_25
.LBB12_21:
	leal	1(%rax), %ebp
	movl	$0, (%rsp)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%rcx, %rdx
	movq	%r15, %rcx
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movl	%r14d, %r8d
	movq	%rax, %r14
	movl	%ebp, %r9d
	callq	_ZL41_find_quantized_collision_pairs_recursiveP21btGImpactQuantizedBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	movq	24(%r13), %rax
	movl	28(%rax,%r12), %eax
	addl	$2, %r14d
	subl	%eax, %ebp
	testl	%eax, %eax
	cmovnsl	%r14d, %ebp
	movl	$0, (%rsp)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%ebp, %r9d
	jmp	.LBB12_25
.LBB12_24:
	leal	1(%rbp), %r9d
	movl	$0, (%rsp)
	movq	%rbx, %rdi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%r13, %rsi
	movq	%rax, %rdx
	movl	%r14d, %r8d
	movq	%rbp, %r15
	movl	%r9d, %ebp
	movl	%ebp, 52(%rsp)          # 4-byte Spill
	callq	_ZL41_find_quantized_collision_pairs_recursiveP21btGImpactQuantizedBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	movq	24(%r13), %rax
	movl	28(%rax,%r12), %eax
	addl	$2, %r15d
	movq	%r15, 40(%rsp)          # 8-byte Spill
	subl	%eax, %ebp
	testl	%eax, %eax
	cmovnsl	%r15d, %ebp
	movl	$0, (%rsp)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%r14d, %r8d
	movl	%ebp, %r9d
	callq	_ZL41_find_quantized_collision_pairs_recursiveP21btGImpactQuantizedBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	movq	24(%rbx), %rax
	movslq	%r14d, %r14
	movq	%r14, %rbp
	shlq	$4, %rbp
	movl	12(%rax,%rbp), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	addl	$2, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%r14d, %r8d
	subl	%eax, %r8d
	testl	%eax, %eax
	cmovnsl	%ecx, %r8d
	movl	$0, (%rsp)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	52(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %r9d
	callq	_ZL41_find_quantized_collision_pairs_recursiveP21btGImpactQuantizedBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	movq	24(%rbx), %rax
	movl	12(%rax,%rbp), %eax
	subl	%eax, %r14d
	testl	%eax, %eax
	cmovnsl	16(%rsp), %r14d         # 4-byte Folded Reload
	movq	24(%r13), %rax
	movl	28(%rax,%r12), %eax
	subl	%eax, %r15d
	testl	%eax, %eax
	cmovnsl	40(%rsp), %r15d         # 4-byte Folded Reload
	movl	$0, (%rsp)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%r14d, %r8d
	movl	%r15d, %r9d
.LBB12_25:
	callq	_ZL41_find_quantized_collision_pairs_recursiveP21btGImpactQuantizedBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	jmp	.LBB12_26
.LBB12_5:
	movq	%r15, %rdx
	jmp	.LBB12_20
.LBB12_7:
	xorl	%ebx, %ebx
	movq	%r15, %rax
.LBB12_9:                               # %_ZN20btAlignedObjectArrayI8GIM_PAIRE8allocateEi.exit.i.i.i
	movq	16(%rax), %rdi
	testl	%r9d, %r9d
	jle	.LBB12_15
# BB#10:                                # %.lr.ph.i.i.i.i
	movslq	%r9d, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdx
	xorl	%esi, %esi
	andq	$3, %rdx
	je	.LBB12_12
	.p2align	4, 0x90
.LBB12_11:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rsi,8), %eax
	movl	%eax, (%rbx,%rsi,8)
	movl	4(%rdi,%rsi,8), %eax
	movl	%eax, 4(%rbx,%rsi,8)
	incq	%rsi
	cmpq	%rsi, %rdx
	jne	.LBB12_11
.LBB12_12:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB12_16
# BB#13:                                # %.lr.ph.i.i.i.i.new
	subq	%rsi, %rcx
	leaq	28(%rbx,%rsi,8), %rdx
	leaq	28(%rdi,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB12_14:                              # =>This Inner Loop Header: Depth=1
	movl	-28(%rsi), %eax
	movl	%eax, -28(%rdx)
	movl	-24(%rsi), %eax
	movl	%eax, -24(%rdx)
	movl	-20(%rsi), %eax
	movl	%eax, -20(%rdx)
	movl	-16(%rsi), %eax
	movl	%eax, -16(%rdx)
	movl	-12(%rsi), %eax
	movl	%eax, -12(%rdx)
	movl	-8(%rsi), %eax
	movl	%eax, -8(%rdx)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdx)
	movl	(%rsi), %eax
	movl	%eax, (%rdx)
	addq	$32, %rdx
	addq	$32, %rsi
	addq	$-4, %rcx
	jne	.LBB12_14
	jmp	.LBB12_16
.LBB12_15:                              # %_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_.exit.i.i.i
	testq	%rdi, %rdi
	je	.LBB12_19
.LBB12_16:                              # %_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_.exit.thread.i.i.i
	cmpb	$0, 24(%r15)
	je	.LBB12_18
# BB#17:
	callq	_Z21btAlignedFreeInternalPv
	movl	4(%r15), %r9d
.LBB12_18:
	movq	$0, 16(%r15)
.LBB12_19:                              # %_ZN20btAlignedObjectArrayI8GIM_PAIRE10deallocateEv.exit.i.i.i
	movb	$1, 24(%r15)
	movq	%rbx, 16(%r15)
	movl	%r14d, 8(%r15)
	movq	%r15, %rdx
	movl	%r12d, %edi
.LBB12_20:                              # %_ZN9btPairSet9push_pairEii.exit
	movq	16(%rdx), %rax
	movslq	%r9d, %rcx
	movl	%ebp, (%rax,%rcx,8)
	movl	%edi, 4(%rax,%rcx,8)
	incl	%ecx
	movl	%ecx, 4(%rdx)
.LBB12_26:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZL41_find_quantized_collision_pairs_recursiveP21btGImpactQuantizedBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib, .Lfunc_end12-_ZL41_find_quantized_collision_pairs_recursiveP21btGImpactQuantizedBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
.LCPI13_2:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI13_1:
	.long	1056964608              # float 0.5
	.section	.text._ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb,"axG",@progbits,_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb,comdat
	.weak	_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb
	.p2align	4, 0x90
	.type	_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb,@function
_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb: # @_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb
	.cfi_startproc
# BB#0:
	subq	$56, %rsp
.Lcfi113:
	.cfi_def_cfa_offset 64
	movsd	16(%rdi), %xmm6         # xmm6 = mem[0],zero
	movsd	(%rdi), %xmm3           # xmm3 = mem[0],zero
	addps	%xmm6, %xmm3
	movss	24(%rdi), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	%xmm10, %xmm0
	movaps	.LCPI13_0(%rip), %xmm4  # xmm4 = <0.5,0.5,u,u>
	mulps	%xmm4, %xmm3
	movss	.LCPI13_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	xorps	%xmm7, %xmm7
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm3, -48(%rsp)
	movlps	%xmm2, -40(%rsp)
	subps	%xmm3, %xmm6
	subss	%xmm0, %xmm10
	xorps	%xmm0, %xmm0
	movss	%xmm10, %xmm0           # xmm0 = xmm10[0],xmm0[1,2,3]
	movlps	%xmm6, -32(%rsp)
	movlps	%xmm0, -24(%rsp)
	movsd	16(%rsi), %xmm9         # xmm9 = mem[0],zero
	movsd	(%rsi), %xmm14          # xmm14 = mem[0],zero
	addps	%xmm9, %xmm14
	movss	24(%rsi), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	addss	%xmm13, %xmm2
	mulps	%xmm4, %xmm14
	mulss	%xmm1, %xmm2
	subps	%xmm14, %xmm9
	subss	%xmm2, %xmm13
	movaps	%xmm9, %xmm11
	shufps	$229, %xmm11, %xmm11    # xmm11 = xmm11[1,1,2,3]
	movss	%xmm13, %xmm7           # xmm7 = xmm13[0],xmm7[1,2,3]
	movlps	%xmm9, (%rsp)
	movlps	%xmm7, 8(%rsp)
	movss	16(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm1
	movss	%xmm0, -68(%rsp)        # 4-byte Spill
	mulss	%xmm0, %xmm1
	movaps	%xmm14, %xmm8
	shufps	$229, %xmm8, %xmm8      # xmm8 = xmm8[1,1,2,3]
	movaps	%xmm8, %xmm4
	mulss	%xmm5, %xmm4
	addss	%xmm1, %xmm4
	movss	24(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm7
	mulss	%xmm0, %xmm7
	addss	%xmm4, %xmm7
	addss	(%rdx), %xmm7
	subss	%xmm3, %xmm7
	movss	%xmm7, -64(%rsp)
	movss	64(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	68(%rdx), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	mulss	%xmm9, %xmm1
	movaps	%xmm15, %xmm3
	mulss	%xmm11, %xmm3
	addss	%xmm1, %xmm3
	movss	72(%rdx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm1
	mulss	%xmm13, %xmm1
	addss	%xmm3, %xmm1
	addss	%xmm6, %xmm1
	movaps	.LCPI13_2(%rip), %xmm3  # xmm3 = [nan,nan,nan,nan]
	andps	%xmm7, %xmm3
	ucomiss	%xmm1, %xmm3
	ja	.LBB13_11
# BB#1:
	movss	%xmm4, -72(%rsp)        # 4-byte Spill
	movss	%xmm10, -116(%rsp)      # 4-byte Spill
	movss	%xmm5, -76(%rsp)        # 4-byte Spill
	movss	%xmm0, -88(%rsp)        # 4-byte Spill
	movaps	%xmm6, 16(%rsp)         # 16-byte Spill
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movss	32(%rdx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	36(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm3
	mulss	%xmm10, %xmm3
	movaps	%xmm8, %xmm1
	movss	%xmm0, -80(%rsp)        # 4-byte Spill
	mulss	%xmm0, %xmm1
	addss	%xmm3, %xmm1
	movss	40(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	movss	%xmm0, -92(%rsp)        # 4-byte Spill
	mulss	%xmm0, %xmm3
	addss	%xmm1, %xmm3
	addss	4(%rdx), %xmm3
	subss	-44(%rsp), %xmm3
	movss	%xmm3, -60(%rsp)
	movss	80(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	84(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	mulss	%xmm9, %xmm1
	movss	%xmm4, -84(%rsp)        # 4-byte Spill
	mulss	%xmm11, %xmm4
	addss	%xmm1, %xmm4
	movss	88(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, -96(%rsp)        # 4-byte Spill
	mulss	%xmm13, %xmm1
	addss	%xmm4, %xmm1
	movaps	%xmm6, 32(%rsp)         # 16-byte Spill
	addss	%xmm6, %xmm1
	movaps	.LCPI13_2(%rip), %xmm4  # xmm4 = [nan,nan,nan,nan]
	andps	%xmm3, %xmm4
	ucomiss	%xmm1, %xmm4
	movaps	%xmm11, %xmm6
	ja	.LBB13_11
# BB#2:
	movss	%xmm10, -100(%rsp)      # 4-byte Spill
	movss	%xmm12, -108(%rsp)      # 4-byte Spill
	movss	%xmm15, -104(%rsp)      # 4-byte Spill
	movss	48(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	52(%rdx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm14
	mulss	%xmm11, %xmm8
	addss	%xmm14, %xmm8
	movss	56(%rdx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	addss	%xmm8, %xmm2
	addss	8(%rdx), %xmm2
	subss	-40(%rsp), %xmm2
	movss	%xmm2, -56(%rsp)
	movss	96(%rdx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	100(%rdx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm1
	mulss	%xmm9, %xmm1
	movaps	%xmm14, %xmm4
	mulss	%xmm6, %xmm4
	addss	%xmm1, %xmm4
	movss	104(%rdx), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm1
	mulss	%xmm13, %xmm1
	addss	%xmm4, %xmm1
	addss	-116(%rsp), %xmm1       # 4-byte Folded Reload
	movaps	.LCPI13_2(%rip), %xmm4  # xmm4 = [nan,nan,nan,nan]
	andps	%xmm2, %xmm4
	ucomiss	%xmm1, %xmm4
	ja	.LBB13_11
# BB#3:                                 # %.preheader102117
	movaps	%xmm6, -16(%rsp)        # 16-byte Spill
	movss	%xmm13, -112(%rsp)      # 4-byte Spill
	movss	-68(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	movss	-100(%rsp), %xmm13      # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm13
	addss	%xmm1, %xmm13
	mulss	%xmm2, %xmm5
	addss	%xmm13, %xmm5
	movss	-72(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm15        # 16-byte Reload
	mulss	%xmm15, %xmm1
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	mulss	%xmm4, %xmm0
	addss	%xmm1, %xmm0
	movss	-116(%rsp), %xmm6       # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm8
	addss	%xmm0, %xmm8
	addss	%xmm9, %xmm8
	andps	.LCPI13_2(%rip), %xmm5
	ucomiss	%xmm8, %xmm5
	ja	.LBB13_11
# BB#4:
	movss	-76(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	movss	-80(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm2, %xmm11
	addss	%xmm1, %xmm11
	movaps	%xmm15, %xmm1
	movss	-104(%rsp), %xmm13      # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm1
	movss	-84(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	addss	%xmm1, %xmm5
	mulss	%xmm6, %xmm14
	addss	%xmm5, %xmm14
	addss	-16(%rsp), %xmm14       # 16-byte Folded Reload
	andps	.LCPI13_2(%rip), %xmm11
	ucomiss	%xmm14, %xmm11
	jbe	.LBB13_5
.LBB13_11:
	xorl	%eax, %eax
.LBB13_12:                              # %.critedge
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addq	$56, %rsp
	retq
.LBB13_5:
	mulss	-88(%rsp), %xmm7        # 4-byte Folded Reload
	mulss	-92(%rsp), %xmm3        # 4-byte Folded Reload
	addss	%xmm7, %xmm3
	mulss	%xmm10, %xmm2
	addss	%xmm3, %xmm2
	movss	-108(%rsp), %xmm14      # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm15
	mulss	-96(%rsp), %xmm4        # 4-byte Folded Reload
	addss	%xmm15, %xmm4
	mulss	%xmm12, %xmm6
	addss	%xmm4, %xmm6
	movss	-112(%rsp), %xmm11      # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	addss	%xmm11, %xmm6
	andps	.LCPI13_2(%rip), %xmm2
	ucomiss	%xmm6, %xmm2
	setbe	%al
	ja	.LBB13_12
# BB#6:
	xorb	$1, %cl
	jne	.LBB13_12
# BB#7:                                 # %.preheader.preheader
	movss	4(%rsp), %xmm10         # xmm10 = mem[0],zero,zero,zero
	leaq	88(%rdx), %r8
	movl	$1, %r10d
	movaps	.LCPI13_2(%rip), %xmm8  # xmm8 = [nan,nan,nan,nan]
	movaps	-16(%rsp), %xmm0        # 16-byte Reload
	jmp	.LBB13_9
.LBB13_8:                               # %.loopexit112..preheader_crit_edge
                                        #   in Loop: Header=BB13_9 Depth=1
	movss	-4(%r8), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movss	(%r8), %xmm14           # xmm14 = mem[0],zero,zero,zero
	addq	$16, %r8
	movq	%r9, %r10
	movaps	%xmm10, %xmm0
.LBB13_9:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%r10), %r9
	movslq	%r9d, %rsi
	imulq	$1431655766, %rsi, %rax # imm = 0x55555556
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	addl	%ecx, %eax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %esi
	xorl	%ecx, %ecx
	cmpq	$1, %r10
	sete	%cl
	xorl	%eax, %eax
	xorl	%edi, %edi
	cmpq	$3, %r10
	movslq	%esi, %r11
	movss	-64(%rsp,%r11,4), %xmm5 # xmm5 = mem[0],zero,zero,zero
	movl	$0, %esi
	cmovneq	%r10, %rsi
	movss	-64(%rsp,%rsi,4), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movss	-32(%rsp,%rcx,4), %xmm6 # xmm6 = mem[0],zero,zero,zero
	setne	%dil
	movss	-28(%rsp,%rdi,4), %xmm7 # xmm7 = mem[0],zero,zero,zero
	incq	%rdi
	shlq	$4, %rsi
	movss	16(%rdx,%rsi), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm1
	shlq	$4, %r11
	movss	16(%rdx,%r11), %xmm4    # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	subss	%xmm4, %xmm1
	shlq	$4, %rdi
	movss	64(%rdx,%rdi), %xmm4    # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm4
	shlq	$4, %rcx
	movss	64(%rdx,%rcx), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	addss	%xmm4, %xmm2
	mulss	%xmm14, %xmm0
	addss	%xmm2, %xmm0
	movaps	%xmm11, %xmm2
	mulss	%xmm13, %xmm2
	addss	%xmm0, %xmm2
	andps	%xmm8, %xmm1
	ucomiss	%xmm2, %xmm1
	ja	.LBB13_12
# BB#10:                                #   in Loop: Header=BB13_9 Depth=1
	movss	20(%rdx,%rsi), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm1
	movss	20(%rdx,%r11), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	subss	%xmm2, %xmm1
	movss	68(%rdx,%rdi), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	movss	68(%rdx,%rcx), %xmm4    # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm4
	addss	%xmm2, %xmm4
	mulss	%xmm9, %xmm14
	addss	%xmm4, %xmm14
	movss	-24(%r8), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm2
	mulss	%xmm4, %xmm2
	addss	%xmm14, %xmm2
	andps	%xmm8, %xmm1
	ucomiss	%xmm2, %xmm1
	ja	.LBB13_11
# BB#13:                                #   in Loop: Header=BB13_9 Depth=1
	mulss	24(%rdx,%rsi), %xmm5
	mulss	24(%rdx,%r11), %xmm3
	subss	%xmm3, %xmm5
	mulss	72(%rdx,%rdi), %xmm6
	mulss	72(%rdx,%rcx), %xmm7
	addss	%xmm6, %xmm7
	mulss	%xmm9, %xmm13
	addss	%xmm7, %xmm13
	mulss	%xmm10, %xmm4
	addss	%xmm13, %xmm4
	andps	%xmm8, %xmm5
	ucomiss	%xmm4, %xmm5
	ja	.LBB13_11
# BB#14:                                # %.loopexit112
                                        #   in Loop: Header=BB13_9 Depth=1
	cmpq	$2, %r10
	jle	.LBB13_8
# BB#15:
	movb	$1, %al
	jmp	.LBB13_12
.Lfunc_end13:
	.size	_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb, .Lfunc_end13-_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
