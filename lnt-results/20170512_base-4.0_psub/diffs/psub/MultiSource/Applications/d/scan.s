	.text
	.file	"scan.bc"
	.globl	scan_buffer
	.p2align	4, 0x90
	.type	scan_buffer,@function
scan_buffer:                            # @scan_buffer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	(%rdi), %r10
	movl	24(%rdi), %ecx
	cmpb	$1, 90(%rsi)
	jne	.LBB0_9
# BB#1:
	movb	88(%rsi), %al
	cmpb	$1, %al
	je	.LBB0_21
# BB#2:
	cmpb	$2, %al
	je	.LBB0_27
# BB#3:
	cmpb	$4, %al
	jne	.LBB0_33
# BB#4:
	movq	80(%rsi), %r9
	movzbl	(%r10), %r11d
	incq	%r10
	movq	%r11, %rax
	shrq	$6, %rax
	movq	8(%r9,%rax,8), %rax
	movl	%r11d, %esi
	andl	$63, %esi
	movl	(%rax,%rsi,4), %ebx
	testl	%ebx, %ebx
	je	.LBB0_63
# BB#5:                                 # %.lr.ph345.preheader
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph345
                                        # =>This Inner Loop Header: Depth=1
	decl	%ebx
	leaq	(%rbx,%rbx,4), %rax
	cmpq	$0, (%r9,%rax,8)
	je	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=1
	movq	%r10, (%rdi)
	movl	%ecx, 24(%rdi)
	movl	%ebx, %r8d
.LBB0_8:                                #   in Loop: Header=BB0_6 Depth=1
	xorl	%ebx, %ebx
	cmpb	$10, %r11b
	sete	%bl
	cmoveq	%r10, %rsi
	addl	%ebx, %ecx
	movzbl	(%r10), %r11d
	incq	%r10
	movq	%r11, %rbp
	shrq	$6, %rbp
	leaq	(%r9,%rax,8), %rax
	movq	8(%rax,%rbp,8), %rax
	movl	%r11d, %ebx
	andl	$63, %ebx
	movl	(%rax,%rbx,4), %ebx
	testl	%ebx, %ebx
	jne	.LBB0_6
	jmp	.LBB0_64
.LBB0_9:
	movq	80(%rsi), %r8
	movzbl	(%r10), %r14d
	movq	%r14, %r11
	shrq	$6, %r11
	movq	8(%r8,%r11,8), %rax
	movl	%r14d, %r15d
	andl	$63, %r15d
	movb	(%rax,%r15), %al
	movl	$-1, %r9d
	testb	%al, %al
	je	.LBB0_20
# BB#10:                                # %.lr.ph390
	movq	96(%rsi), %rbp
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	incq	%r10
	leaq	8(%rdx), %rbp
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_14 Depth 2
	movl	%ecx, %r13d
	decb	%al
	movzbl	%al, %ecx
	leaq	(%rcx,%rcx,4), %r9
	cmpq	$0, (%r8,%r9,8)
	je	.LBB0_17
# BB#12:                                #   in Loop: Header=BB0_11 Depth=1
	movq	%r10, (%rdi)
	movl	%r13d, 24(%rdi)
	movq	104(%rsi), %rbp
	shlq	$5, %rcx
	addq	16(%rsp), %rcx          # 8-byte Folded Reload
	movq	(%rcx,%r11,8), %rcx
	movzbl	(%rcx,%r15), %ecx
	movq	(%rbp,%rcx,8), %rcx
	cmpq	$0, (%rcx)
	je	.LBB0_16
# BB#13:                                # %.lr.ph378.preheader
                                        #   in Loop: Header=BB0_11 Depth=1
	movq	%rsi, %r11
	movslq	%ebx, %rsi
	leaq	(%rsi,%rsi,4), %rsi
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	(%rbp,%rsi,8), %rsi
	addq	$8, %rcx
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph378
                                        #   Parent Loop BB0_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	%xmm1, 16(%rsi)
	movups	%xmm0, (%rsi)
	movq	-8(%rcx), %rbp
	movq	%rbp, -8(%rsi)
	addq	$40, %rsi
	incl	%ebx
	cmpq	$0, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB0_14
# BB#15:                                #   in Loop: Header=BB0_11 Depth=1
	movb	%al, %bpl
	movq	%r11, %rsi
	jmp	.LBB0_17
	.p2align	4, 0x90
.LBB0_16:                               #   in Loop: Header=BB0_11 Depth=1
	movb	%al, %bpl
.LBB0_17:                               # %.loopexit305
                                        #   in Loop: Header=BB0_11 Depth=1
	xorl	%ecx, %ecx
	cmpb	$10, %r14b
	sete	%cl
	cmoveq	%r10, %r12
	addl	%r13d, %ecx
	movzbl	(%r10), %r14d
	incq	%r10
	movq	%r14, %r11
	shrq	$6, %r11
	leaq	(%r8,%r9,8), %rax
	movq	8(%rax,%r11,8), %rax
	movl	%r14d, %r15d
	andl	$63, %r15d
	movb	(%rax,%r15), %al
	testb	%al, %al
	jne	.LBB0_11
# BB#18:                                # %._crit_edge391
	testq	%r12, %r12
	je	.LBB0_34
# BB#19:
	subl	%r12d, %r10d
	movl	%r10d, %r9d
	jmp	.LBB0_35
.LBB0_20:
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	jmp	.LBB0_35
.LBB0_21:
	movq	80(%rsi), %r9
	movzbl	(%r10), %r11d
	incq	%r10
	movq	%r11, %rax
	shrq	$6, %rax
	movq	8(%r9,%rax,8), %rax
	movl	%r11d, %esi
	andl	$63, %esi
	movb	(%rax,%rsi), %bl
	testb	%bl, %bl
	je	.LBB0_65
# BB#22:                                # %.lr.ph316.preheader
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph316
                                        # =>This Inner Loop Header: Depth=1
	decb	%bl
	movzbl	%bl, %eax
	leaq	(%rax,%rax,4), %rax
	cmpq	$0, (%r9,%rax,8)
	je	.LBB0_25
# BB#24:                                #   in Loop: Header=BB0_23 Depth=1
	movq	%r10, (%rdi)
	movl	%ecx, 24(%rdi)
	movb	%bl, %r8b
.LBB0_25:                               #   in Loop: Header=BB0_23 Depth=1
	xorl	%ebx, %ebx
	cmpb	$10, %r11b
	sete	%bl
	cmoveq	%r10, %rsi
	addl	%ebx, %ecx
	movzbl	(%r10), %r11d
	incq	%r10
	movq	%r11, %rbp
	shrq	$6, %rbp
	leaq	(%r9,%rax,8), %rax
	movq	8(%rax,%rbp,8), %rax
	movl	%r11d, %ebx
	andl	$63, %ebx
	movzbl	(%rax,%rbx), %ebx
	testb	%bl, %bl
	jne	.LBB0_23
# BB#26:                                # %._crit_edge
	movzbl	%r8b, %eax
	jmp	.LBB0_68
.LBB0_27:
	movq	80(%rsi), %r9
	movzbl	(%r10), %r11d
	incq	%r10
	movq	%r11, %rax
	shrq	$6, %rax
	movq	8(%r9,%rax,8), %rax
	movl	%r11d, %esi
	andl	$63, %esi
	movw	(%rax,%rsi,2), %bx
	testw	%bx, %bx
	je	.LBB0_66
# BB#28:                                # %.lr.ph330.preheader
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_29:                               # %.lr.ph330
                                        # =>This Inner Loop Header: Depth=1
	decl	%ebx
	movzwl	%bx, %eax
	leaq	(%rax,%rax,4), %rax
	cmpq	$0, (%r9,%rax,8)
	je	.LBB0_31
# BB#30:                                #   in Loop: Header=BB0_29 Depth=1
	movq	%r10, (%rdi)
	movl	%ecx, 24(%rdi)
	movw	%bx, %r8w
.LBB0_31:                               #   in Loop: Header=BB0_29 Depth=1
	xorl	%ebx, %ebx
	cmpb	$10, %r11b
	sete	%bl
	cmoveq	%r10, %rsi
	addl	%ebx, %ecx
	movzbl	(%r10), %r11d
	incq	%r10
	movq	%r11, %rbp
	shrq	$6, %rbp
	leaq	(%r9,%rax,8), %rax
	movq	8(%rax,%rbp,8), %rax
	movl	%r11d, %ebx
	andl	$63, %ebx
	movw	(%rax,%rbx,2), %bx
	testw	%bx, %bx
	jne	.LBB0_29
	jmp	.LBB0_67
.LBB0_33:                               # %.thread300
	movl	$-1, 20(%rdi)
	xorl	%ebx, %ebx
	jmp	.LBB0_75
.LBB0_34:
	movl	$-1, %r9d
.LBB0_35:                               # %._crit_edge391.thread
	movl	%r9d, 20(%rdi)
	movzbl	%bpl, %eax
	leaq	(%rax,%rax,4), %rax
	movq	(%r8,%rax,8), %rax
	testq	%rax, %rax
	je	.LBB0_39
# BB#36:
	movl	%ecx, 24(%rdi)
	cmpq	$0, (%rax)
	je	.LBB0_39
# BB#37:                                # %.lr.ph373
	movslq	%ebx, %rcx
	leaq	(%rcx,%rcx,4), %rcx
	leaq	8(%rdx,%rcx,8), %rcx
	addq	$8, %rax
	.p2align	4, 0x90
.LBB0_38:                               # =>This Inner Loop Header: Depth=1
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	%xmm1, 16(%rcx)
	movups	%xmm0, (%rcx)
	movq	-8(%rax), %rbp
	movq	%rbp, -8(%rcx)
	addq	$40, %rcx
	incl	%ebx
	cmpq	$0, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB0_38
.LBB0_39:                               # %.loopexit304
	testl	%ebx, %ebx
	je	.LBB0_53
# BB#40:
	cmpb	$2, 90(%rsi)
	jne	.LBB0_75
# BB#41:
	leal	-1(%rbx), %eax
	testl	%ebx, %ebx
	jle	.LBB0_54
# BB#42:                                # %.lr.ph363.preheader
	cltq
	leaq	(%rax,%rax,4), %rax
	movq	8(%rdx,%rax,8), %rcx
	movslq	%ebx, %rax
	leaq	(%rax,%rax,4), %rsi
	leaq	-32(%rdx,%rsi,8), %rdi
	xorl	%esi, %esi
	movl	$1, %r8d
	.p2align	4, 0x90
.LBB0_43:                               # %.lr.ph363
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rdi), %rbp
	cmpb	$1, 2(%rbp)
	cmovel	%r8d, %esi
	cmpq	%rcx, (%rdi)
	jb	.LBB0_45
# BB#44:                                #   in Loop: Header=BB0_43 Depth=1
	decq	%rax
	addq	$-40, %rdi
	testq	%rax, %rax
	jg	.LBB0_43
.LBB0_45:                               # %.lr.ph363.._crit_edge364_crit_edge
	decl	%eax
	testl	%esi, %esi
	je	.LBB0_54
# BB#46:                                # %.preheader302
	testl	%ebx, %ebx
	jle	.LBB0_76
# BB#47:                                # %.lr.ph358.preheader
	movl	%ebx, %r8d
	xorl	%esi, %esi
	movq	%rdx, %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_48:                               # %.lr.ph358
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rcx, 8(%rdi)
	jne	.LBB0_52
# BB#49:                                #   in Loop: Header=BB0_48 Depth=1
	movl	%ebx, %ebp
	cmpq	%rbp, %rsi
	je	.LBB0_51
# BB#50:                                #   in Loop: Header=BB0_48 Depth=1
	movslq	%ebx, %rbp
	leaq	(%rbp,%rbp,4), %rbp
	movq	32(%rdi), %rax
	movq	%rax, 32(%rdx,%rbp,8)
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	%xmm1, 16(%rdx,%rbp,8)
	movups	%xmm0, (%rdx,%rbp,8)
.LBB0_51:                               #   in Loop: Header=BB0_48 Depth=1
	incl	%ebx
.LBB0_52:                               #   in Loop: Header=BB0_48 Depth=1
	incq	%rsi
	addq	$40, %rdi
	cmpq	%rsi, %r8
	jne	.LBB0_48
	jmp	.LBB0_75
.LBB0_53:
	xorl	%ebx, %ebx
	jmp	.LBB0_75
.LBB0_54:                               # %.preheader
	testl	%eax, %eax
	js	.LBB0_61
# BB#55:                                # %.lr.ph353.preheader
	movslq	%eax, %rcx
	leaq	(%rcx,%rcx,4), %rsi
	incq	%rcx
	leaq	(%rdx,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB0_56:                               # %.lr.ph353
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rdi
	cmpb	$1, 2(%rdi)
	je	.LBB0_60
# BB#57:                                #   in Loop: Header=BB0_56 Depth=1
	leaq	-1(%rcx), %rdi
	movl	%eax, %ebp
	cmpq	%rbp, %rdi
	je	.LBB0_59
# BB#58:                                #   in Loop: Header=BB0_56 Depth=1
	movslq	%eax, %rdi
	leaq	(%rdi,%rdi,4), %rdi
	movq	32(%rsi), %rbp
	movq	%rbp, 32(%rdx,%rdi,8)
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movups	%xmm1, 16(%rdx,%rdi,8)
	movups	%xmm0, (%rdx,%rdi,8)
.LBB0_59:                               #   in Loop: Header=BB0_56 Depth=1
	decl	%eax
.LBB0_60:                               #   in Loop: Header=BB0_56 Depth=1
	addq	$-40, %rsi
	decq	%rcx
	jg	.LBB0_56
.LBB0_61:                               # %._crit_edge354
	subl	%eax, %ebx
	decl	%ebx
	cmpl	$-1, %eax
	je	.LBB0_75
# BB#62:
	incl	%eax
	cltq
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rsi
	movslq	%ebx, %rax
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rax
	movq	%rdx, %rdi
	movq	%rax, %rdx
	callq	memmove
	jmp	.LBB0_75
.LBB0_63:
	xorl	%esi, %esi
	xorl	%r8d, %r8d
.LBB0_64:                               # %._crit_edge346
	movl	%r8d, %eax
	jmp	.LBB0_68
.LBB0_65:
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movzbl	%r8b, %eax
	jmp	.LBB0_68
.LBB0_66:
	xorl	%esi, %esi
	xorl	%r8d, %r8d
.LBB0_67:
	movzwl	%r8w, %eax
.LBB0_68:
	leaq	(%rax,%rax,4), %rax
	leaq	(%r9,%rax,8), %rax
	movq	(%rax), %rax
	testq	%rsi, %rsi
	je	.LBB0_70
# BB#69:
	subl	%esi, %r10d
	jmp	.LBB0_71
.LBB0_70:
	movl	$-1, %r10d
.LBB0_71:
	movl	%r10d, 20(%rdi)
	xorl	%ebx, %ebx
	testq	%rax, %rax
	je	.LBB0_75
# BB#72:
	movl	%ecx, 24(%rdi)
	cmpq	$0, (%rax)
	je	.LBB0_75
# BB#73:                                # %.lr.ph
	addq	$8, %rdx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_74:                               # =>This Inner Loop Header: Depth=1
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	%xmm1, 16(%rdx)
	movups	%xmm0, (%rdx)
	movq	(%rax,%rbx,8), %rcx
	movq	%rcx, -8(%rdx)
	addq	$40, %rdx
	cmpq	$0, 8(%rax,%rbx,8)
	leaq	1(%rbx), %rbx
	jne	.LBB0_74
.LBB0_75:                               # %.loopexit
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_76:
	xorl	%ebx, %ebx
	jmp	.LBB0_75
.Lfunc_end0:
	.size	scan_buffer, .Lfunc_end0-scan_buffer
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
