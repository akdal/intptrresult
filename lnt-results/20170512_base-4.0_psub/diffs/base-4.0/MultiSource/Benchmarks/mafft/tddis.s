	.text
	.file	"tddis.bc"
	.globl	mdfymtx
	.p2align	4, 0x90
	.type	mdfymtx,@function
mdfymtx:                                # @mdfymtx
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movslq	njob(%rip), %r15
	cmpq	$2, %r15
	jl	.LBB0_9
# BB#1:                                 # %.lr.ph
	movslq	%esi, %rsi
	movq	(%rdi,%rsi,8), %rsi
	leaq	-1(%r15), %r8
	xorl	%r14d, %r14d
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
                                        #       Child Loop BB0_5 Depth 3
	cmpb	$0, (%rsi,%r14)
	je	.LBB0_8
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movslq	%r9d, %r10
	incl	%r9d
	movslq	%r9d, %r11
	movq	%r14, %rdi
	jmp	.LBB0_4
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_4 Depth=2
	movq	(%rcx,%r14,8), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	(%rdx,%r10,8), %rbx
	movq	%rax, (%rbx,%r11,8)
	incq	%r11
.LBB0_4:                                # %.outer
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_5 Depth 3
	movslq	%edi, %rdi
	.p2align	4, 0x90
.LBB0_5:                                #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdi, %rbx
	leaq	1(%rbx), %rdi
	cmpq	%r15, %rdi
	jge	.LBB0_8
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=3
	cmpb	$0, 1(%rsi,%rbx)
	je	.LBB0_5
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_8:                                # %.loopexit
                                        #   in Loop: Header=BB0_2 Depth=1
	incq	%r14
	cmpq	%r8, %r14
	jl	.LBB0_2
.LBB0_9:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	mdfymtx, .Lfunc_end0-mdfymtx
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	2143289344              # float NaN
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_1:
	.quad	-4616189618054758400    # double -1
.LCPI1_2:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	score_calc
	.p2align	4, 0x90
	.type	score_calc,@function
score_calc:                             # @score_calc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 128
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	(%rdi), %rdi
	callq	strlen
	xorps	%xmm0, %xmm0
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	cmpl	$2, %ebx
	jl	.LBB1_24
# BB#1:                                 # %.lr.ph93
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %ebx
	leal	-2(%rax), %edx
	movl	penalty(%rip), %esi
	movslq	%ecx, %rbp
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movl	%ecx, %ebp
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	leal	-2(%rcx), %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	movl	$1, %edi
	xorl	%ecx, %ecx
	movss	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movq	%rbx, 24(%rsp)          # 8-byte Spill
                                        # kill: %BL<def> %BL<kill> %RBX<kill>
	.p2align	4, 0x90
.LBB1_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_14 Depth 2
                                        #     Child Loop BB1_17 Depth 2
                                        #     Child Loop BB1_6 Depth 2
                                        #       Child Loop BB1_7 Depth 3
                                        #         Child Loop BB1_20 Depth 4
                                        #         Child Loop BB1_10 Depth 4
	movq	%rcx, %rbp
	leaq	1(%rbp), %r8
	cmpq	40(%rsp), %r8           # 8-byte Folded Reload
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movb	%bl, 7(%rsp)            # 1-byte Spill
	jge	.LBB1_2
# BB#4:                                 # %.lr.ph88
                                        #   in Loop: Header=BB1_3 Depth=1
	testl	%eax, %eax
	jle	.LBB1_11
# BB#5:                                 # %.lr.ph88.split.us.preheader
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rbp,8), %rbp
	leaq	1(%rbp), %rcx
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph88.split.us
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_7 Depth 3
                                        #         Child Loop BB1_20 Depth 4
                                        #         Child Loop BB1_10 Depth 4
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi,%rbx,8), %r14
	leaq	1(%r14), %r10
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB1_7:                                #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_20 Depth 4
                                        #         Child Loop BB1_10 Depth 4
	movslq	%r11d, %r15
	movsbq	(%rbp,%r15), %r12
	cmpq	$45, %r12
	movsbq	(%r14,%r15), %r13
	jne	.LBB1_18
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=3
	cmpb	$45, %r13b
	je	.LBB1_23
# BB#9:                                 #   in Loop: Header=BB1_7 Depth=3
	incl	%r8d
	shlq	$9, %r12
	addl	amino_dis(%r12,%r13,4), %r9d
	leal	-1(%r15), %r11d
	addq	%rcx, %r15
	.p2align	4, 0x90
.LBB1_10:                               #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_6 Depth=2
                                        #       Parent Loop BB1_7 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%r11d
	cmpb	$45, (%r15)
	leaq	1(%r15), %r15
	je	.LBB1_10
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_18:                               #   in Loop: Header=BB1_7 Depth=3
	incl	%r8d
	shlq	$9, %r12
	addl	amino_dis(%r12,%r13,4), %r9d
	cmpb	$45, %r13b
	jne	.LBB1_23
# BB#19:                                # %.preheader.preheader
                                        #   in Loop: Header=BB1_7 Depth=3
	leal	-1(%r15), %r11d
	addq	%r10, %r15
	.p2align	4, 0x90
.LBB1_20:                               # %.preheader
                                        #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_6 Depth=2
                                        #       Parent Loop BB1_7 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%r11d
	cmpb	$45, (%r15)
	leaq	1(%r15), %r15
	je	.LBB1_20
.LBB1_21:                               #   in Loop: Header=BB1_7 Depth=3
	addl	%esi, %r9d
	cmpl	%edx, %r11d
	jg	.LBB1_22
.LBB1_23:                               #   in Loop: Header=BB1_7 Depth=3
	incl	%r11d
	cmpl	%eax, %r11d
	jl	.LBB1_7
.LBB1_22:                               # %._crit_edge.us
                                        #   in Loop: Header=BB1_6 Depth=2
	cvtsi2sdl	%r9d, %xmm2
	cvtsi2sdl	%r8d, %xmm3
	divsd	%xmm3, %xmm2
	cvtss2sd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	incq	%rbx
	cmpq	64(%rsp), %rbx          # 8-byte Folded Reload
	jne	.LBB1_6
	jmp	.LBB1_2
.LBB1_11:                               # %.lr.ph88.split.preheader
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r9d
	subl	%ebp, %r9d
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%ebp, %ecx
	testb	$7, %r9b
	je	.LBB1_12
# BB#13:                                # %.lr.ph88.split.prol.preheader
                                        #   in Loop: Header=BB1_3 Depth=1
	andb	$7, %bl
	movzbl	%bl, %ebx
	negl	%ebx
	movl	%r8d, %ebp
	.p2align	4, 0x90
.LBB1_14:                               # %.lr.ph88.split.prol
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addss	%xmm1, %xmm0
	incl	%ebp
	incl	%ebx
	jne	.LBB1_14
	jmp	.LBB1_15
.LBB1_12:                               #   in Loop: Header=BB1_3 Depth=1
	movl	%r8d, %ebp
.LBB1_15:                               # %.lr.ph88.split.prol.loopexit
                                        #   in Loop: Header=BB1_3 Depth=1
	cmpl	$7, %ecx
	jb	.LBB1_2
# BB#16:                                # %.lr.ph88.split.preheader.new
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%ebp, %ecx
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph88.split
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addss	%xmm1, %xmm0
	addss	%xmm1, %xmm0
	addss	%xmm1, %xmm0
	addss	%xmm1, %xmm0
	addss	%xmm1, %xmm0
	addss	%xmm1, %xmm0
	addss	%xmm1, %xmm0
	addss	%xmm1, %xmm0
	addl	$-8, %ecx
	jne	.LBB1_17
	.p2align	4, 0x90
.LBB1_2:                                # %.loopexit
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	56(%rsp), %rdi          # 8-byte Reload
	incq	%rdi
	movb	7(%rsp), %bl            # 1-byte Reload
	addb	$7, %bl
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmpq	24(%rsp), %rcx          # 8-byte Folded Reload
	jne	.LBB1_3
.LBB1_24:                               # %._crit_edge94
	cvtss2sd	%xmm0, %xmm0
	movq	16(%rsp), %rax          # 8-byte Reload
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movsd	.LCPI1_1(%rip), %xmm2   # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	.LCPI1_2(%rip), %xmm2
	divsd	%xmm2, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movq	stderr(%rip), %rdi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %esi
	movb	$1, %al
	callq	fprintf
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	score_calc, .Lfunc_end1-score_calc
	.cfi_endproc

	.globl	cpmx_calc
	.p2align	4, 0x90
	.type	cpmx_calc,@function
cpmx_calc:                              # @cpmx_calc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 64
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movl	%ecx, %r12d
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testl	%r13d, %r13d
	jle	.LBB2_1
# BB#2:                                 # %.lr.ph51.preheader
	movl	%r13d, %eax
	leaq	-1(%rax), %rcx
	movq	%rax, %rsi
	andq	$7, %rsi
	je	.LBB2_3
# BB#4:                                 # %.lr.ph51.prol.preheader
	xorpd	%xmm0, %xmm0
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph51.prol
                                        # =>This Inner Loop Header: Depth=1
	addsd	(%r14,%rdx,8), %xmm0
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB2_5
	jmp	.LBB2_6
.LBB2_1:
	xorpd	%xmm0, %xmm0
	testl	%r12d, %r12d
	jg	.LBB2_10
	jmp	.LBB2_15
.LBB2_3:
	xorl	%edx, %edx
	xorpd	%xmm0, %xmm0
.LBB2_6:                                # %.lr.ph51.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB2_9
# BB#7:                                 # %.lr.ph51.preheader.new
	subq	%rdx, %rax
	leaq	56(%r14,%rdx,8), %rcx
	.p2align	4, 0x90
.LBB2_8:                                # %.lr.ph51
                                        # =>This Inner Loop Header: Depth=1
	addsd	-56(%rcx), %xmm0
	addsd	-48(%rcx), %xmm0
	addsd	-40(%rcx), %xmm0
	addsd	-32(%rcx), %xmm0
	addsd	-24(%rcx), %xmm0
	addsd	-16(%rcx), %xmm0
	addsd	-8(%rcx), %xmm0
	addsd	(%rcx), %xmm0
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB2_8
.LBB2_9:                                # %.preheader40
	testl	%r12d, %r12d
	jle	.LBB2_15
.LBB2_10:                               # %.preheader.lr.ph
	leal	-1(%r12), %eax
	leaq	4(,%rax,4), %rbp
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	callq	memset
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	56(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	80(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	88(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	96(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	104(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	112(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	120(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	128(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	136(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	144(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	152(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	160(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	168(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	176(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	184(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	192(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	200(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
	testl	%r13d, %r13d
	jle	.LBB2_15
# BB#11:                                # %.preheader.us.preheader
	movl	%r13d, %r8d
	movl	%r12d, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_12:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_13 Depth 2
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB2_13:                               #   Parent Loop BB2_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	divsd	%xmm2, %xmm0
	movq	(%rdi), %rax
	movsbq	(%rax,%rdx), %rax
	movslq	amino_n(,%rax,4), %rax
	movq	(%rbx,%rax,8), %rax
	movss	(%rax,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, (%rax,%rdx,4)
	addq	$8, %rbp
	addq	$8, %rdi
	decq	%rsi
	jne	.LBB2_13
# BB#14:                                # %._crit_edge.us
                                        #   in Loop: Header=BB2_12 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jne	.LBB2_12
.LBB2_15:                               # %._crit_edge43
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	cpmx_calc, .Lfunc_end2-cpmx_calc
	.cfi_endproc

	.globl	cpmx_calc_new_bk
	.p2align	4, 0x90
	.type	cpmx_calc_new_bk,@function
cpmx_calc_new_bk:                       # @cpmx_calc_new_bk
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movl	%ecx, %r15d
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r12
	testl	%r15d, %r15d
	jle	.LBB3_2
# BB#1:                                 # %.preheader30.us.preheader
	leal	-1(%r15), %eax
	leaq	4(,%rax,4), %rbp
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	56(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	80(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	88(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	96(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	104(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	112(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	120(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	128(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	136(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	144(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	152(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	160(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	168(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	176(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	184(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	192(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	200(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
.LBB3_2:                                # %.preheader
	testl	%r15d, %r15d
	setle	%al
	testl	%r13d, %r13d
	jle	.LBB3_11
# BB#3:                                 # %.preheader
	testb	%al, %al
	jne	.LBB3_11
# BB#4:                                 # %.lr.ph33.split.us.preheader
	movl	%r15d, %eax
	movl	%r13d, %r8d
	movl	%eax, %edx
	andl	$1, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph33.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_9 Depth 2
	testq	%rdx, %rdx
	movsd	(%r14,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r12,%rsi,8), %rdi
	jne	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=1
	xorl	%ebp, %ebp
	cmpl	$1, %r15d
	jne	.LBB3_9
	jmp	.LBB3_10
	.p2align	4, 0x90
.LBB3_7:                                #   in Loop: Header=BB3_5 Depth=1
	movsbq	(%rdi), %rbp
	movslq	amino_n(,%rbp,4), %rbp
	movq	(%rbx,%rbp,8), %rbp
	movss	(%rbp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rbp)
	movl	$1, %ebp
	cmpl	$1, %r15d
	je	.LBB3_10
	.p2align	4, 0x90
.LBB3_9:                                #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%rdi,%rbp), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	movq	(%rbx,%rcx,8), %rcx
	movss	(%rcx,%rbp,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rcx,%rbp,4)
	movsbq	1(%rdi,%rbp), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	movq	(%rbx,%rcx,8), %rcx
	movss	4(%rcx,%rbp,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%rcx,%rbp,4)
	addq	$2, %rbp
	cmpq	%rbp, %rax
	jne	.LBB3_9
.LBB3_10:                               # %._crit_edge.us
                                        #   in Loop: Header=BB3_5 Depth=1
	incq	%rsi
	cmpq	%r8, %rsi
	jne	.LBB3_5
.LBB3_11:                               # %._crit_edge34
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	cpmx_calc_new_bk, .Lfunc_end3-cpmx_calc_new_bk
	.cfi_endproc

	.globl	cpmx_calc_new
	.p2align	4, 0x90
	.type	cpmx_calc_new,@function
cpmx_calc_new:                          # @cpmx_calc_new
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 64
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movl	%ecx, %r15d
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r12
	testl	%r15d, %r15d
	je	.LBB4_2
# BB#1:                                 # %.split.preheader
	leal	-1(%r15), %eax
	leaq	4(,%rax,4), %rbp
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	56(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	80(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	88(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	96(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	104(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	112(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	120(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	128(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	136(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	144(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	152(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	160(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	168(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	176(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	184(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	192(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	200(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
.LBB4_2:                                # %.preheader
	testl	%r13d, %r13d
	jle	.LBB4_11
# BB#3:                                 # %.preheader
	testl	%r15d, %r15d
	jle	.LBB4_11
# BB#4:                                 # %.lr.ph40.split.us.preheader
	movl	%r15d, %eax
	movl	%r13d, %r8d
	movl	%eax, %edx
	andl	$1, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph40.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_9 Depth 2
	testq	%rdx, %rdx
	movsd	(%r14,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r12,%rsi,8), %rdi
	jne	.LBB4_7
# BB#6:                                 #   in Loop: Header=BB4_5 Depth=1
	xorl	%ebp, %ebp
	cmpl	$1, %r15d
	jne	.LBB4_9
	jmp	.LBB4_10
	.p2align	4, 0x90
.LBB4_7:                                #   in Loop: Header=BB4_5 Depth=1
	movsbq	(%rdi), %rbp
	incq	%rdi
	movslq	amino_n(,%rbp,4), %rbp
	movq	(%rbx,%rbp,8), %rbp
	movss	(%rbp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rbp)
	movl	$1, %ebp
	cmpl	$1, %r15d
	je	.LBB4_10
	.p2align	4, 0x90
.LBB4_9:                                #   Parent Loop BB4_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%rdi), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	movq	(%rbx,%rcx,8), %rcx
	movss	(%rcx,%rbp,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rcx,%rbp,4)
	movsbq	1(%rdi), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	movq	(%rbx,%rcx,8), %rcx
	movss	4(%rcx,%rbp,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%rcx,%rbp,4)
	addq	$2, %rbp
	addq	$2, %rdi
	cmpq	%rbp, %rax
	jne	.LBB4_9
.LBB4_10:                               # %._crit_edge.us
                                        #   in Loop: Header=BB4_5 Depth=1
	incq	%rsi
	cmpq	%r8, %rsi
	jne	.LBB4_5
.LBB4_11:                               # %._crit_edge41
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	cpmx_calc_new, .Lfunc_end4-cpmx_calc_new
	.cfi_endproc

	.globl	MScpmx_calc_new
	.p2align	4, 0x90
	.type	MScpmx_calc_new,@function
MScpmx_calc_new:                        # @MScpmx_calc_new
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 56
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	testl	%ecx, %ecx
	je	.LBB5_7
# BB#1:                                 # %.lr.ph47.preheader
	leal	-1(%rcx), %r10d
	movl	%ecx, %ebp
	andl	$7, %ebp
	movl	%ecx, %r9d
	movq	%rsi, %rax
	je	.LBB5_4
# BB#2:                                 # %.lr.ph47.prol.preheader
	negl	%ebp
	xorps	%xmm0, %xmm0
	movl	%ecx, %r9d
	movq	%rsi, %rax
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph47.prol
                                        # =>This Inner Loop Header: Depth=1
	decl	%r9d
	movq	(%rax), %rbx
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 96(%rbx)
	addq	$8, %rax
	incl	%ebp
	jne	.LBB5_3
.LBB5_4:                                # %.lr.ph47.prol.loopexit
	cmpl	$7, %r10d
	jb	.LBB5_7
# BB#5:
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB5_6:                                # %.lr.ph47
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rbp
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movq	$0, 96(%rbp)
	movq	8(%rax), %rbp
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movq	$0, 96(%rbp)
	movq	16(%rax), %rbp
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movq	$0, 96(%rbp)
	movq	24(%rax), %rbp
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movq	$0, 96(%rbp)
	movq	32(%rax), %rbp
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movq	$0, 96(%rbp)
	movq	40(%rax), %rbp
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movq	$0, 96(%rbp)
	movq	48(%rax), %rbp
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movq	$0, 96(%rbp)
	addl	$-8, %r9d
	movq	56(%rax), %rbp
	leaq	64(%rax), %rax
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movq	$0, 96(%rbp)
	jne	.LBB5_6
.LBB5_7:                                # %.preheader
	testl	%r8d, %r8d
	jle	.LBB5_16
# BB#8:                                 # %.preheader
	testl	%ecx, %ecx
	je	.LBB5_16
# BB#9:                                 # %.lr.ph41.split.preheader
	movl	%r8d, %r8d
	movl	%ecx, %r11d
	andl	$1, %r11d
	leal	-1(%rcx), %r9d
	leaq	8(%rsi), %r10
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_10:                               # %.lr.ph41.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_14 Depth 2
	testl	%r11d, %r11d
	movsd	(%rdx,%r14,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%rdi,%r14,8), %rbx
	jne	.LBB5_12
# BB#11:                                #   in Loop: Header=BB5_10 Depth=1
	movq	%rsi, %rax
	movl	%ecx, %r15d
	cmpl	$1, %ecx
	jne	.LBB5_14
	jmp	.LBB5_15
	.p2align	4, 0x90
.LBB5_12:                               #   in Loop: Header=BB5_10 Depth=1
	movq	(%rsi), %rax
	movsbq	(%rbx), %rbp
	incq	%rbx
	movslq	amino_n(,%rbp,4), %rbp
	movss	(%rax,%rbp,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rax,%rbp,4)
	movq	%r10, %rax
	movl	%r9d, %r15d
	cmpl	$1, %ecx
	je	.LBB5_15
	.p2align	4, 0x90
.LBB5_14:                               #   Parent Loop BB5_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %r12
	movsbq	(%rbx), %rbp
	movslq	amino_n(,%rbp,4), %rbp
	movss	(%r12,%rbp,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r12,%rbp,4)
	addl	$-2, %r15d
	movq	8(%rax), %r13
	leaq	16(%rax), %rax
	movsbq	1(%rbx), %r12
	leaq	2(%rbx), %rbx
	movslq	amino_n(,%r12,4), %rbp
	movss	(%r13,%rbp,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r13,%rbp,4)
	jne	.LBB5_14
.LBB5_15:                               # %._crit_edge
                                        #   in Loop: Header=BB5_10 Depth=1
	incq	%r14
	cmpq	%r8, %r14
	jne	.LBB5_10
.LBB5_16:                               # %._crit_edge42
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	MScpmx_calc_new, .Lfunc_end5-MScpmx_calc_new
	.cfi_endproc

	.globl	cpmx_ribosum
	.p2align	4, 0x90
	.type	cpmx_ribosum,@function
cpmx_ribosum:                           # @cpmx_ribosum
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
.Lcfi76:
	.cfi_offset %rbx, -56
.Lcfi77:
	.cfi_offset %r12, -48
.Lcfi78:
	.cfi_offset %r13, -40
.Lcfi79:
	.cfi_offset %r14, -32
.Lcfi80:
	.cfi_offset %r15, -24
.Lcfi81:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movq	%r8, -8(%rsp)           # 8-byte Spill
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	movq	%rsi, -24(%rsp)         # 8-byte Spill
	movl	56(%rsp), %r10d
	testl	%r9d, %r9d
	je	.LBB6_7
# BB#1:                                 # %.lr.ph79.preheader
	leal	-1(%r9), %r8d
	movl	%r9d, %ebp
	andl	$7, %ebp
	movl	%r9d, %r11d
	movq	%rcx, %rax
	je	.LBB6_4
# BB#2:                                 # %.lr.ph79.prol.preheader
	negl	%ebp
	xorps	%xmm0, %xmm0
	movl	%r9d, %r11d
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph79.prol
                                        # =>This Inner Loop Header: Depth=1
	decl	%r11d
	movq	(%rax), %rbx
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movl	$0, 144(%rbx)
	addq	$8, %rax
	incl	%ebp
	jne	.LBB6_3
.LBB6_4:                                # %.lr.ph79.prol.loopexit
	cmpl	$7, %r8d
	jb	.LBB6_7
# BB#5:
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph79
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rbp
	movups	%xmm0, 128(%rbp)
	movups	%xmm0, 112(%rbp)
	movups	%xmm0, 96(%rbp)
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movl	$0, 144(%rbp)
	movq	8(%rax), %rbp
	movups	%xmm0, 128(%rbp)
	movups	%xmm0, 112(%rbp)
	movups	%xmm0, 96(%rbp)
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movl	$0, 144(%rbp)
	movq	16(%rax), %rbp
	movups	%xmm0, 128(%rbp)
	movups	%xmm0, 112(%rbp)
	movups	%xmm0, 96(%rbp)
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movl	$0, 144(%rbp)
	movq	24(%rax), %rbp
	movups	%xmm0, 128(%rbp)
	movups	%xmm0, 112(%rbp)
	movups	%xmm0, 96(%rbp)
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movl	$0, 144(%rbp)
	movq	32(%rax), %rbp
	movups	%xmm0, 128(%rbp)
	movups	%xmm0, 112(%rbp)
	movups	%xmm0, 96(%rbp)
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movl	$0, 144(%rbp)
	movq	40(%rax), %rbp
	movups	%xmm0, 128(%rbp)
	movups	%xmm0, 112(%rbp)
	movups	%xmm0, 96(%rbp)
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movl	$0, 144(%rbp)
	movq	48(%rax), %rbp
	movups	%xmm0, 128(%rbp)
	movups	%xmm0, 112(%rbp)
	movups	%xmm0, 96(%rbp)
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movl	$0, 144(%rbp)
	addl	$-8, %r11d
	movq	56(%rax), %rbp
	leaq	64(%rax), %rax
	movups	%xmm0, 128(%rbp)
	movups	%xmm0, 112(%rbp)
	movups	%xmm0, 96(%rbp)
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movl	$0, 144(%rbp)
	jne	.LBB6_6
.LBB6_7:                                # %.preheader
	testl	%r10d, %r10d
	jle	.LBB6_21
# BB#8:                                 # %.preheader
	testl	%r9d, %r9d
	je	.LBB6_21
# BB#9:                                 # %.lr.ph73.split.preheader
	movl	%r10d, %esi
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB6_10:                               # %.lr.ph73.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_11 Depth 2
	movq	-8(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%r11,8), %xmm0    # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movq	(%rdi,%r11,8), %rax
	movq	-24(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%r11,8), %r8
	movl	%r9d, %r14d
	movq	-16(%rsp), %r15         # 8-byte Reload
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB6_11:                               #   Parent Loop BB6_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%r14d
	movsbq	(%rax), %rbx
	movl	amino_n(,%rbx,4), %r12d
	movl	$36, %r13d
	cmpl	$3, %r12d
	jg	.LBB6_19
# BB#12:                                #   in Loop: Header=BB6_11 Depth=2
	movsbq	(%r8), %rbx
	movl	amino_n(,%rbx,4), %r13d
	cmpl	$3, %r13d
	jg	.LBB6_18
# BB#14:                                #   in Loop: Header=BB6_11 Depth=2
	movzbl	(%r15), %r10d
	cmpb	$51, %r10b
	je	.LBB6_17
# BB#15:                                #   in Loop: Header=BB6_11 Depth=2
	cmpb	$53, %r10b
	jne	.LBB6_18
# BB#16:                                #   in Loop: Header=BB6_11 Depth=2
	leal	4(%r12,%r13,4), %r13d
	jmp	.LBB6_19
	.p2align	4, 0x90
.LBB6_18:                               #   in Loop: Header=BB6_11 Depth=2
	movl	%r12d, %r13d
	jmp	.LBB6_19
.LBB6_17:                               #   in Loop: Header=BB6_11 Depth=2
	leal	20(%r12,%r13,4), %r13d
	.p2align	4, 0x90
.LBB6_19:                               #   in Loop: Header=BB6_11 Depth=2
	incq	%rax
	incq	%r8
	incq	%r15
	movq	(%rbp), %rbx
	addq	$8, %rbp
	movslq	%r13d, %rdx
	movss	(%rbx,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rbx,%rdx,4)
	testl	%r14d, %r14d
	jne	.LBB6_11
# BB#20:                                # %._crit_edge
                                        #   in Loop: Header=BB6_10 Depth=1
	incq	%r11
	cmpq	%rsi, %r11
	jne	.LBB6_10
.LBB6_21:                               # %._crit_edge74
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	cpmx_ribosum, .Lfunc_end6-cpmx_ribosum
	.cfi_endproc

	.globl	mseqcat
	.p2align	4, 0x90
	.type	mseqcat,@function
mseqcat:                                # @mseqcat
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi88:
	.cfi_def_cfa_offset 128
.Lcfi89:
	.cfi_offset %rbx, -56
.Lcfi90:
	.cfi_offset %r12, -48
.Lcfi91:
	.cfi_offset %r13, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, (%rsp)             # 8-byte Spill
	movq	%rcx, %rbx
	movq	%rdx, %r15
	movl	144(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB7_15
# BB#1:                                 # %.lr.ph112.preheader
	movl	136(%rsp), %eax
	movslq	%eax, %r8
	movl	144(%rsp), %r10d
	movl	%r10d, %r14d
	cmpl	$3, %r10d
	jbe	.LBB7_5
# BB#2:                                 # %min.iters.checked
	movl	%r10d, %r9d
	andl	$3, %r9d
	movq	%r14, %rcx
	subq	%r9, %rcx
	je	.LBB7_5
# BB#3:                                 # %vector.memcheck
	leaq	(%rdi,%r8,8), %rax
	leaq	(%rsi,%r14,8), %rdx
	cmpq	%rdx, %rax
	jae	.LBB7_84
# BB#4:                                 # %vector.memcheck
	leaq	(%r8,%r14), %rax
	leaq	(%rdi,%rax,8), %rax
	cmpq	%rsi, %rax
	jbe	.LBB7_84
.LBB7_5:
	xorl	%ecx, %ecx
.LBB7_6:                                # %.lr.ph112.preheader302
	movl	%r14d, %ebp
	subl	%ecx, %ebp
	leaq	-1(%r14), %r9
	subq	%rcx, %r9
	andq	$3, %rbp
	je	.LBB7_9
# BB#7:                                 # %.lr.ph112.prol.preheader
	leaq	(%rdi,%r8,8), %rax
	negq	%rbp
	.p2align	4, 0x90
.LBB7_8:                                # %.lr.ph112.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rcx,8), %rdx
	movq	%rdx, (%rax,%rcx,8)
	incq	%rcx
	incq	%rbp
	jne	.LBB7_8
.LBB7_9:                                # %.lr.ph112.prol.loopexit
	cmpq	$3, %r9
	jb	.LBB7_12
# BB#10:                                # %.lr.ph112.preheader302.new
	movq	%r14, %rdx
	subq	%rcx, %rdx
	leaq	(%rcx,%r8), %rax
	leaq	24(%rdi,%rax,8), %rdi
	leaq	24(%rsi,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB7_11:                               # %.lr.ph112
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rcx), %rax
	movq	%rax, -24(%rdi)
	movq	-16(%rcx), %rax
	movq	%rax, -16(%rdi)
	movq	-8(%rcx), %rax
	movq	%rax, -8(%rdi)
	movq	(%rcx), %rax
	movq	%rax, (%rdi)
	addq	$32, %rdi
	addq	$32, %rcx
	addq	$-4, %rdx
	jne	.LBB7_11
.LBB7_12:                               # %.preheader92
	testl	%r10d, %r10d
	jle	.LBB7_15
# BB#13:                                # %.lr.ph109.preheader
	movq	128(%rsp), %r13
	shlq	$8, %r8
	addq	%r8, %r12
	.p2align	4, 0x90
.LBB7_14:                               # %.lr.ph109
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	strcpy
	addq	$256, %r13              # imm = 0x100
	addq	$256, %r12              # imm = 0x100
	decq	%r14
	jne	.LBB7_14
.LBB7_15:                               # %.preheader91
	movl	136(%rsp), %r13d
	testl	%r13d, %r13d
	movq	%r15, 48(%rsp)          # 8-byte Spill
	jle	.LBB7_49
# BB#16:                                # %.preheader90.us.preheader
	movl	%r13d, %r12d
	leaq	(%rbx,%r12,8), %r10
	leaq	-1(%r12), %r14
	movl	%r13d, %r9d
	andl	$3, %r9d
	movq	%r12, %r11
	subq	%r9, %r11
	leaq	16(%rbx), %r8
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_17:                               # %.preheader90.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_23 Depth 2
                                        #     Child Loop BB7_29 Depth 2
                                        #     Child Loop BB7_31 Depth 2
	cmpl	$4, %r13d
	leaq	(%rbx,%rbp,8), %rsi
	movq	(%r15,%rbp,8), %rax
	jb	.LBB7_26
# BB#19:                                # %min.iters.checked159
                                        #   in Loop: Header=BB7_17 Depth=1
	testq	%r11, %r11
	je	.LBB7_26
# BB#20:                                # %vector.memcheck176
                                        #   in Loop: Header=BB7_17 Depth=1
	leaq	(%rax,%r12,8), %rdi
	cmpq	%r10, %rax
	sbbb	%cl, %cl
	cmpq	%rdi, %rbx
	sbbb	%dl, %dl
	andb	%cl, %dl
	cmpq	%rsi, %rax
	sbbb	%cl, %cl
	cmpq	%rdi, %rsi
	sbbb	%dil, %dil
	testb	$1, %dl
	jne	.LBB7_26
# BB#21:                                # %vector.memcheck176
                                        #   in Loop: Header=BB7_17 Depth=1
	andb	%dil, %cl
	andb	$1, %cl
	movl	$0, %edi
	jne	.LBB7_27
# BB#22:                                # %vector.body155.preheader
                                        #   in Loop: Header=BB7_17 Depth=1
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	leaq	16(%rax), %rcx
	movq	%r11, %rdx
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB7_23:                               # %vector.body155
                                        #   Parent Loop BB7_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-16(%rdi), %xmm1
	movupd	(%rdi), %xmm2
	mulpd	%xmm0, %xmm1
	mulpd	%xmm0, %xmm2
	movupd	%xmm1, -16(%rcx)
	movupd	%xmm2, (%rcx)
	addq	$32, %rdi
	addq	$32, %rcx
	addq	$-4, %rdx
	jne	.LBB7_23
# BB#24:                                # %middle.block156
                                        #   in Loop: Header=BB7_17 Depth=1
	testl	%r9d, %r9d
	movq	%r11, %rdi
	jne	.LBB7_27
	jmp	.LBB7_32
	.p2align	4, 0x90
.LBB7_26:                               #   in Loop: Header=BB7_17 Depth=1
	xorl	%edi, %edi
.LBB7_27:                               # %scalar.ph157.preheader
                                        #   in Loop: Header=BB7_17 Depth=1
	movl	%r12d, %ecx
	subl	%edi, %ecx
	movq	%r14, %rdx
	subq	%rdi, %rdx
	andq	$3, %rcx
	je	.LBB7_30
# BB#28:                                # %scalar.ph157.prol.preheader
                                        #   in Loop: Header=BB7_17 Depth=1
	negq	%rcx
	.p2align	4, 0x90
.LBB7_29:                               # %scalar.ph157.prol
                                        #   Parent Loop BB7_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rbx,%rdi,8), %xmm0
	movsd	%xmm0, (%rax,%rdi,8)
	incq	%rdi
	incq	%rcx
	jne	.LBB7_29
.LBB7_30:                               # %scalar.ph157.prol.loopexit
                                        #   in Loop: Header=BB7_17 Depth=1
	cmpq	$3, %rdx
	jb	.LBB7_32
	.p2align	4, 0x90
.LBB7_31:                               # %scalar.ph157
                                        #   Parent Loop BB7_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rbx,%rdi,8), %xmm0
	movsd	%xmm0, (%rax,%rdi,8)
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	mulsd	8(%rbx,%rdi,8), %xmm0
	movsd	%xmm0, 8(%rax,%rdi,8)
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	mulsd	16(%rbx,%rdi,8), %xmm0
	movsd	%xmm0, 16(%rax,%rdi,8)
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	mulsd	24(%rbx,%rdi,8), %xmm0
	movsd	%xmm0, 24(%rax,%rdi,8)
	addq	$4, %rdi
	cmpq	%rdi, %r12
	jne	.LBB7_31
.LBB7_32:                               # %._crit_edge106.us
                                        #   in Loop: Header=BB7_17 Depth=1
	incq	%rbp
	cmpq	%rdi, %rbp
	jne	.LBB7_17
# BB#33:                                # %.preheader89
	testl	%r13d, %r13d
	jle	.LBB7_49
# BB#34:                                # %.preheader88.lr.ph
	movl	144(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB7_83
# BB#35:                                # %.preheader88.us.preheader
	leal	(%rax,%r13), %eax
	movslq	%r13d, %r15
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movslq	%eax, %rdx
	leaq	1(%r15), %r10
	cmpq	%rdx, %r10
	cmovlq	%rdx, %r10
	movq	%r10, %rsi
	subq	%r15, %rsi
	movq	(%rsp), %r14            # 8-byte Reload
	leaq	(%r14,%rsi,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	-4(%rsi), %rax
	shrq	$2, %rax
	movq	%rsi, %rdi
	andq	$-4, %rdi
	leaq	(%r15,%rdi), %rbp
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	48(%r14), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	(,%r15,8), %rax
	subq	%rax, %r14
	xorl	%r8d, %r8d
	movq	%r10, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB7_36:                               # %.preheader88.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_44 Depth 2
                                        #     Child Loop BB7_46 Depth 2
	cmpq	$4, %rsi
	leaq	(%rbx,%r8,8), %r13
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r8,8), %r11
	movq	%r15, %rax
	jb	.LBB7_46
# BB#37:                                # %min.iters.checked192
                                        #   in Loop: Header=BB7_36 Depth=1
	testq	%rdi, %rdi
	movq	%r15, %rax
	je	.LBB7_46
# BB#38:                                # %vector.memcheck214
                                        #   in Loop: Header=BB7_36 Depth=1
	movq	%rsi, %r9
	movq	%rdi, %rsi
	leaq	(%r11,%r15,8), %rdi
	leaq	(%r11,%r10,8), %rbp
	cmpq	%r13, %rdi
	sbbb	%cl, %cl
	cmpq	%rbp, %r13
	sbbb	%al, %al
	andb	%cl, %al
	cmpq	32(%rsp), %rdi          # 8-byte Folded Reload
	movq	%rsi, %rdi
	movq	%r9, %rsi
	sbbb	%cl, %cl
	cmpq	%rbp, (%rsp)            # 8-byte Folded Reload
	sbbb	%r9b, %r9b
	testb	$1, %al
	movq	%r15, %rax
	jne	.LBB7_46
# BB#39:                                # %vector.memcheck214
                                        #   in Loop: Header=BB7_36 Depth=1
	andb	%r9b, %cl
	andb	$1, %cl
	movq	%r15, %rax
	jne	.LBB7_46
# BB#40:                                # %vector.body188.preheader
                                        #   in Loop: Header=BB7_36 Depth=1
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB7_42
# BB#41:                                # %vector.body188.prol
                                        #   in Loop: Header=BB7_36 Depth=1
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movq	(%rsp), %rax            # 8-byte Reload
	movupd	(%rax), %xmm1
	movupd	16(%rax), %xmm2
	mulpd	%xmm0, %xmm1
	mulpd	%xmm0, %xmm2
	movupd	%xmm1, (%r11,%r15,8)
	movupd	%xmm2, 16(%r11,%r15,8)
	movl	$4, %eax
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	jne	.LBB7_43
	jmp	.LBB7_45
.LBB7_42:                               #   in Loop: Header=BB7_36 Depth=1
	xorl	%eax, %eax
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB7_45
.LBB7_43:                               # %vector.body188.preheader.new
                                        #   in Loop: Header=BB7_36 Depth=1
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movq	%rdi, %rbp
	subq	%rax, %rbp
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %r9
	addq	%r15, %rax
	leaq	48(%r11,%rax,8), %r10
	.p2align	4, 0x90
.LBB7_44:                               # %vector.body188
                                        #   Parent Loop BB7_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%r9), %xmm1
	movupd	-32(%r9), %xmm2
	mulpd	%xmm0, %xmm1
	mulpd	%xmm0, %xmm2
	movupd	%xmm1, -48(%r10)
	movupd	%xmm2, -32(%r10)
	movupd	-16(%r9), %xmm1
	movupd	(%r9), %xmm2
	mulpd	%xmm0, %xmm1
	mulpd	%xmm0, %xmm2
	movupd	%xmm1, -16(%r10)
	movupd	%xmm2, (%r10)
	addq	$64, %r9
	addq	$64, %r10
	addq	$-8, %rbp
	jne	.LBB7_44
.LBB7_45:                               # %middle.block189
                                        #   in Loop: Header=BB7_36 Depth=1
	cmpq	%rdi, %rsi
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %r10          # 8-byte Reload
	je	.LBB7_47
	.p2align	4, 0x90
.LBB7_46:                               # %scalar.ph190
                                        #   Parent Loop BB7_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%r14,%rax,8), %xmm0
	movsd	%xmm0, (%r11,%rax,8)
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB7_46
.LBB7_47:                               # %._crit_edge102.us
                                        #   in Loop: Header=BB7_36 Depth=1
	incq	%r8
	cmpq	%r12, %r8
	jne	.LBB7_36
# BB#48:
	movl	136(%rsp), %r13d
	movl	144(%rsp), %eax
	movl	12(%rsp), %ecx          # 4-byte Reload
	testl	%eax, %eax
	jg	.LBB7_50
	jmp	.LBB7_83
.LBB7_49:                               # %.preheader89..preheader87_crit_edge
	movl	144(%rsp), %eax
	leal	(%rax,%r13), %ecx
	testl	%eax, %eax
	jle	.LBB7_83
.LBB7_50:                               # %.preheader86.lr.ph
	testl	%r13d, %r13d
	jle	.LBB7_70
# BB#51:                                # %.preheader86.us.preheader
	movslq	%r13d, %r11
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movslq	%ecx, %r14
	movl	%r13d, %r9d
	leaq	(%rbx,%r9,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	-1(%r9), %r8
	movl	%r13d, %eax
	andl	$3, %eax
	movq	%r9, %r13
	movq	%rax, 24(%rsp)          # 8-byte Spill
	subq	%rax, %r13
	leaq	16(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	24(%rbx), %r12
	movq	%r11, %rdi
	movq	48(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_52:                               # %.preheader86.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_58 Depth 2
                                        #     Child Loop BB7_64 Depth 2
                                        #     Child Loop BB7_67 Depth 2
	movq	%rdi, %rax
	subq	%r11, %rax
	movl	136(%rsp), %ecx
	cmpl	$4, %ecx
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rcx,%rax,8), %rbp
	movq	(%r15,%rdi,8), %rax
	jb	.LBB7_61
# BB#54:                                # %min.iters.checked230
                                        #   in Loop: Header=BB7_52 Depth=1
	testq	%r13, %r13
	je	.LBB7_61
# BB#55:                                # %vector.memcheck249
                                        #   in Loop: Header=BB7_52 Depth=1
	leaq	(%rax,%r9,8), %rsi
	cmpq	%rbp, %rax
	sbbb	%cl, %cl
	cmpq	%rsi, %rbp
	sbbb	%dl, %dl
	andb	%cl, %dl
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	sbbb	%cl, %cl
	cmpq	%rsi, %rbx
	sbbb	%sil, %sil
	testb	$1, %dl
	jne	.LBB7_61
# BB#56:                                # %vector.memcheck249
                                        #   in Loop: Header=BB7_52 Depth=1
	andb	%sil, %cl
	andb	$1, %cl
	movl	$0, %edx
	jne	.LBB7_62
# BB#57:                                # %vector.body226.preheader
                                        #   in Loop: Header=BB7_52 Depth=1
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	leaq	16(%rax), %rdx
	movq	%r13, %rsi
	movq	16(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_58:                               # %vector.body226
                                        #   Parent Loop BB7_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-16(%rcx), %xmm1
	movupd	(%rcx), %xmm2
	mulpd	%xmm0, %xmm1
	mulpd	%xmm0, %xmm2
	movupd	%xmm1, -16(%rdx)
	movupd	%xmm2, (%rdx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-4, %rsi
	jne	.LBB7_58
# BB#59:                                # %middle.block227
                                        #   in Loop: Header=BB7_52 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movq	%r13, %rdx
	jne	.LBB7_62
	jmp	.LBB7_68
	.p2align	4, 0x90
.LBB7_61:                               #   in Loop: Header=BB7_52 Depth=1
	xorl	%edx, %edx
.LBB7_62:                               # %scalar.ph228.preheader
                                        #   in Loop: Header=BB7_52 Depth=1
	movl	%r9d, %r10d
	subl	%edx, %r10d
	movq	%r8, %rsi
	subq	%rdx, %rsi
	andq	$3, %r10
	je	.LBB7_65
# BB#63:                                # %scalar.ph228.prol.preheader
                                        #   in Loop: Header=BB7_52 Depth=1
	negq	%r10
	.p2align	4, 0x90
.LBB7_64:                               # %scalar.ph228.prol
                                        #   Parent Loop BB7_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rbx,%rdx,8), %xmm0
	movsd	%xmm0, (%rax,%rdx,8)
	incq	%rdx
	incq	%r10
	jne	.LBB7_64
.LBB7_65:                               # %scalar.ph228.prol.loopexit
                                        #   in Loop: Header=BB7_52 Depth=1
	cmpq	$3, %rsi
	jb	.LBB7_68
# BB#66:                                # %scalar.ph228.preheader.new
                                        #   in Loop: Header=BB7_52 Depth=1
	movq	%r9, %rsi
	subq	%rdx, %rsi
	leaq	24(%rax,%rdx,8), %rax
	leaq	(%r12,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB7_67:                               # %scalar.ph228
                                        #   Parent Loop BB7_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	mulsd	-24(%rdx), %xmm0
	movsd	%xmm0, -24(%rax)
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	mulsd	-16(%rdx), %xmm0
	movsd	%xmm0, -16(%rax)
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	mulsd	-8(%rdx), %xmm0
	movsd	%xmm0, -8(%rax)
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rdx), %xmm0
	movsd	%xmm0, (%rax)
	addq	$32, %rax
	addq	$32, %rdx
	addq	$-4, %rsi
	jne	.LBB7_67
.LBB7_68:                               # %._crit_edge98.us
                                        #   in Loop: Header=BB7_52 Depth=1
	incq	%rdi
	cmpq	%r14, %rdi
	jl	.LBB7_52
# BB#69:                                # %.preheader85
	movl	144(%rsp), %eax
	testl	%eax, %eax
	movl	136(%rsp), %r13d
	movl	12(%rsp), %ecx          # 4-byte Reload
	jle	.LBB7_83
.LBB7_70:                               # %.preheader.us.preheader
	movslq	%r13d, %r11
	movslq	%ecx, %rcx
	leaq	1(%r11), %r12
	cmpq	%rcx, %r12
	cmovlq	%rcx, %r12
	movq	%r12, %r15
	subq	%r11, %r15
	movq	(%rsp), %rdx            # 8-byte Reload
	leaq	(%rdx,%r15,8), %r14
	leaq	-4(%r15), %rax
	shrq	$2, %rax
	movq	%r15, %r10
	andq	$-4, %r10
	leaq	(%r11,%r10), %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	48(%rdx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	(,%r11,8), %rax
	movq	%rdx, %rbp
	subq	%rax, %rbp
	movq	%r11, %rdi
	.p2align	4, 0x90
.LBB7_71:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_79 Depth 2
                                        #     Child Loop BB7_81 Depth 2
	movq	%rdi, %rax
	subq	%r11, %rax
	cmpq	$4, %r15
	movq	(%rsp), %rdx            # 8-byte Reload
	leaq	(%rdx,%rax,8), %rdx
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdi,8), %rbx
	movq	%r11, %rax
	jb	.LBB7_81
# BB#72:                                # %min.iters.checked266
                                        #   in Loop: Header=BB7_71 Depth=1
	testq	%r10, %r10
	movq	%r11, %rax
	je	.LBB7_81
# BB#73:                                # %vector.memcheck288
                                        #   in Loop: Header=BB7_71 Depth=1
	leaq	(%rbx,%r11,8), %rsi
	leaq	(%rbx,%r12,8), %r9
	cmpq	%r14, %rsi
	sbbb	%r8b, %r8b
	cmpq	%r9, (%rsp)             # 8-byte Folded Reload
	sbbb	%al, %al
	andb	%r8b, %al
	cmpq	%rdx, %rsi
	sbbb	%r8b, %r8b
	cmpq	%r9, %rdx
	sbbb	%r9b, %r9b
	testb	$1, %al
	movq	%r11, %rax
	jne	.LBB7_81
# BB#74:                                # %vector.memcheck288
                                        #   in Loop: Header=BB7_71 Depth=1
	andb	%r9b, %r8b
	andb	$1, %r8b
	movq	%r11, %rax
	jne	.LBB7_81
# BB#75:                                # %vector.body261.preheader
                                        #   in Loop: Header=BB7_71 Depth=1
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	jne	.LBB7_77
# BB#76:                                # %vector.body261.prol
                                        #   in Loop: Header=BB7_71 Depth=1
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movq	(%rsp), %rax            # 8-byte Reload
	movupd	(%rax), %xmm1
	movupd	16(%rax), %xmm2
	mulpd	%xmm0, %xmm1
	mulpd	%xmm0, %xmm2
	movupd	%xmm1, (%rbx,%r11,8)
	movupd	%xmm2, 16(%rbx,%r11,8)
	movl	$4, %eax
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	jne	.LBB7_78
	jmp	.LBB7_80
.LBB7_77:                               #   in Loop: Header=BB7_71 Depth=1
	xorl	%eax, %eax
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB7_80
.LBB7_78:                               # %vector.body261.preheader.new
                                        #   in Loop: Header=BB7_71 Depth=1
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movq	%r10, %r13
	subq	%rax, %r13
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax,8), %r8
	addq	%r11, %rax
	leaq	48(%rbx,%rax,8), %r9
	.p2align	4, 0x90
.LBB7_79:                               # %vector.body261
                                        #   Parent Loop BB7_71 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%r8), %xmm1
	movupd	-32(%r8), %xmm2
	mulpd	%xmm0, %xmm1
	mulpd	%xmm0, %xmm2
	movupd	%xmm1, -48(%r9)
	movupd	%xmm2, -32(%r9)
	movupd	-16(%r8), %xmm1
	movupd	(%r8), %xmm2
	mulpd	%xmm0, %xmm1
	mulpd	%xmm0, %xmm2
	movupd	%xmm1, -16(%r9)
	movupd	%xmm2, (%r9)
	addq	$64, %r8
	addq	$64, %r9
	addq	$-8, %r13
	jne	.LBB7_79
.LBB7_80:                               # %middle.block262
                                        #   in Loop: Header=BB7_71 Depth=1
	cmpq	%r10, %r15
	movq	24(%rsp), %rax          # 8-byte Reload
	je	.LBB7_82
	.p2align	4, 0x90
.LBB7_81:                               # %scalar.ph263
                                        #   Parent Loop BB7_71 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rbp,%rax,8), %xmm0
	movsd	%xmm0, (%rbx,%rax,8)
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB7_81
.LBB7_82:                               # %._crit_edge.us
                                        #   in Loop: Header=BB7_71 Depth=1
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB7_71
.LBB7_83:                               # %._crit_edge95
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_84:                               # %vector.body.preheader
	leaq	16(%rsi), %rbp
	leaq	16(%rdi,%r8,8), %rax
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB7_85:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbp), %xmm0
	movupd	(%rbp), %xmm1
	movups	%xmm0, -16(%rax)
	movupd	%xmm1, (%rax)
	addq	$32, %rbp
	addq	$32, %rax
	addq	$-4, %rdx
	jne	.LBB7_85
# BB#86:                                # %middle.block
	testl	%r9d, %r9d
	jne	.LBB7_6
	jmp	.LBB7_12
.Lfunc_end7:
	.size	mseqcat, .Lfunc_end7-mseqcat
	.cfi_endproc

	.globl	strnbcat
	.p2align	4, 0x90
	.type	strnbcat,@function
strnbcat:                               # @strnbcat
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi97:
	.cfi_def_cfa_offset 32
.Lcfi98:
	.cfi_offset %rbx, -24
.Lcfi99:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rax
	movslq	%edx, %rbx
	movl	$strnbcat.b, %edi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	callq	strncpy
	movb	$0, strnbcat.b(%rbx)
	movl	$strnbcat.b, %edi
	movq	%r14, %rsi
	callq	strcat
	movl	$strnbcat.b, %esi
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	strcpy                  # TAILCALL
.Lfunc_end8:
	.size	strnbcat, .Lfunc_end8-strnbcat
	.cfi_endproc

	.globl	conjuctionforgaln
	.p2align	4, 0x90
	.type	conjuctionforgaln,@function
conjuctionforgaln:                      # @conjuctionforgaln
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi102:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi103:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi104:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi106:
	.cfi_def_cfa_offset 352
.Lcfi107:
	.cfi_offset %rbx, -56
.Lcfi108:
	.cfi_offset %r12, -48
.Lcfi109:
	.cfi_offset %r13, -40
.Lcfi110:
	.cfi_offset %r14, -32
.Lcfi111:
	.cfi_offset %r15, -24
.Lcfi112:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movl	%esi, %r15d
	movl	%edi, %ebp
	movq	368(%rsp), %rax
	movb	$0, (%rax)
	xorl	%eax, %eax
	cmpl	%ebp, %r15d
	jle	.LBB9_15
# BB#1:                                 # %.lr.ph38.preheader
	movslq	%ebp, %rax
	leal	-1(%r15), %ecx
	subl	%ebp, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	leaq	(%rdx,%rax,8), %r14
	leaq	(%r9,%rax,8), %r12
	xorpd	%xmm2, %xmm2
	movq	%r8, (%rsp)             # 8-byte Spill
	movq	%r8, %r13
	.p2align	4, 0x90
.LBB9_2:                                # %.lr.ph38
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm2, 16(%rsp)         # 16-byte Spill
	incl	%ebp
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	leaq	32(%rsp), %rdi
	movl	%ebp, %edx
	callq	sprintf
	movq	368(%rsp), %rdi
	callq	strlen
	cmpq	$99, %rax
	ja	.LBB9_4
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	368(%rsp), %rdi
	leaq	32(%rsp), %rsi
	callq	strcat
.LBB9_4:                                #   in Loop: Header=BB9_2 Depth=1
	movq	(%r14), %rax
	movq	%rax, (%rbx)
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, (%r13)
	movapd	16(%rsp), %xmm2         # 16-byte Reload
	addsd	%xmm0, %xmm2
	addq	$8, %r14
	addq	$8, %r12
	addq	$8, %rbx
	addq	$8, %r13
	cmpl	%ebp, %r15d
	jne	.LBB9_2
# BB#5:                                 # %.preheader
	movq	8(%rsp), %rdx           # 8-byte Reload
	leal	1(%rdx), %eax
	testl	%eax, %eax
	jle	.LBB9_15
# BB#6:                                 # %.lr.ph.preheader
	movl	%eax, %ecx
	testl	%edx, %edx
	je	.LBB9_7
# BB#10:                                # %min.iters.checked
	movl	%eax, %esi
	andl	$1, %esi
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	movq	(%rsp), %rbx            # 8-byte Reload
	je	.LBB9_11
# BB#12:                                # %vector.ph
	movapd	%xmm2, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movq	%rdx, %rdi
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB9_13:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rbp), %xmm1
	divpd	%xmm0, %xmm1
	movupd	%xmm1, (%rbp)
	addq	$16, %rbp
	addq	$-2, %rdi
	jne	.LBB9_13
# BB#14:                                # %middle.block
	testl	%esi, %esi
	jne	.LBB9_8
	jmp	.LBB9_15
.LBB9_7:
	xorl	%edx, %edx
	movq	(%rsp), %rbx            # 8-byte Reload
	jmp	.LBB9_8
.LBB9_11:
	xorl	%edx, %edx
.LBB9_8:                                # %.lr.ph.preheader49
	leaq	(%rbx,%rdx,8), %rsi
	subq	%rdx, %rcx
	.p2align	4, 0x90
.LBB9_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	divsd	%xmm2, %xmm0
	movsd	%xmm0, (%rsi)
	addq	$8, %rsi
	decq	%rcx
	jne	.LBB9_9
.LBB9_15:                               # %._crit_edge
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	conjuctionforgaln, .Lfunc_end9-conjuctionforgaln
	.cfi_endproc

	.globl	makegrouprna
	.p2align	4, 0x90
	.type	makegrouprna,@function
makegrouprna:                           # @makegrouprna
	.cfi_startproc
# BB#0:
	movl	(%rdx), %eax
	cmpl	$-1, %eax
	je	.LBB10_3
# BB#1:                                 # %.lr.ph.preheader
	addq	$4, %rdx
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cltq
	movq	(%rsi,%rax,8), %rax
	movq	%rax, (%rdi)
	movl	(%rdx), %eax
	addq	$8, %rdi
	addq	$4, %rdx
	cmpl	$-1, %eax
	jne	.LBB10_2
.LBB10_3:                               # %._crit_edge
	retq
.Lfunc_end10:
	.size	makegrouprna, .Lfunc_end10-makegrouprna
	.cfi_endproc

	.globl	makegrouprnait
	.p2align	4, 0x90
	.type	makegrouprnait,@function
makegrouprnait:                         # @makegrouprnait
	.cfi_startproc
# BB#0:
	movslq	njob(%rip), %r8
	cmpl	%ecx, %r8d
	jle	.LBB11_12
# BB#1:                                 # %.lr.ph
	movslq	%ecx, %r9
	movl	%r8d, %eax
	subl	%ecx, %eax
	leaq	-1(%r8), %r10
	xorl	%r11d, %r11d
	testb	$1, %al
	movq	%r9, %rax
	je	.LBB11_6
# BB#2:
	movq	(%rdx,%r9,8), %rax
	cmpb	$0, (%rax,%r9)
	je	.LBB11_3
# BB#4:
	movq	(%rsi,%r9,8), %rax
	movq	%rax, (%rdi)
	movl	$1, %r11d
	jmp	.LBB11_5
.LBB11_3:
	xorl	%r11d, %r11d
.LBB11_5:
	leaq	1(%r9), %rax
.LBB11_6:                               # %.prol.loopexit
	cmpq	%r9, %r10
	je	.LBB11_12
	.p2align	4, 0x90
.LBB11_7:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%r9,8), %rcx
	cmpb	$0, (%rcx,%rax)
	je	.LBB11_9
# BB#8:                                 #   in Loop: Header=BB11_7 Depth=1
	movq	(%rsi,%rax,8), %r10
	movslq	%r11d, %rcx
	incl	%r11d
	movq	%r10, (%rdi,%rcx,8)
.LBB11_9:                               #   in Loop: Header=BB11_7 Depth=1
	movq	(%rdx,%r9,8), %rcx
	cmpb	$0, 1(%rcx,%rax)
	je	.LBB11_11
# BB#10:                                #   in Loop: Header=BB11_7 Depth=1
	movq	8(%rsi,%rax,8), %r10
	movslq	%r11d, %rcx
	incl	%r11d
	movq	%r10, (%rdi,%rcx,8)
.LBB11_11:                              #   in Loop: Header=BB11_7 Depth=1
	addq	$2, %rax
	cmpq	%r8, %rax
	jl	.LBB11_7
.LBB11_12:                              # %._crit_edge
	retq
.Lfunc_end11:
	.size	makegrouprnait, .Lfunc_end11-makegrouprnait
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI12_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	fastconjuction_noweight
	.p2align	4, 0x90
	.type	fastconjuction_noweight,@function
fastconjuction_noweight:                # @fastconjuction_noweight
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi116:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi117:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi119:
	.cfi_def_cfa_offset 368
.Lcfi120:
	.cfi_offset %rbx, -56
.Lcfi121:
	.cfi_offset %r12, -48
.Lcfi122:
	.cfi_offset %r13, -40
.Lcfi123:
	.cfi_offset %r14, -32
.Lcfi124:
	.cfi_offset %r15, -24
.Lcfi125:
	.cfi_offset %rbp, -16
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movb	$0, (%r8)
	movl	(%rbp), %r15d
	cmpl	$-1, %r15d
	je	.LBB12_1
# BB#12:                                # %.lr.ph41.preheader
	xorpd	%xmm2, %xmm2
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB12_13:                              # %.lr.ph41
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm2, 32(%rsp)         # 16-byte Spill
	movl	%r14d, %r12d
	leal	1(%r15), %edx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	leaq	48(%rsp), %rdi
	callq	sprintf
	movl	%eax, %r14d
	addl	%r12d, %r14d
	cmpl	$99, %r14d
	jg	.LBB12_15
# BB#14:                                #   in Loop: Header=BB12_13 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	48(%rsp), %rsi
	callq	strcat
.LBB12_15:                              #   in Loop: Header=BB12_13 Depth=1
	movslq	%r15d, %rax
	movq	(%r13,%rax,8), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx,%rbx,8)
	movq	8(%rsp), %rax           # 8-byte Reload
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	movq	%rcx, (%rax,%rbx,8)
	movapd	32(%rsp), %xmm2         # 16-byte Reload
	addsd	.LCPI12_0(%rip), %xmm2
	movl	4(%rbp,%rbx,4), %r15d
	incq	%rbx
	cmpl	$-1, %r15d
	jne	.LBB12_13
# BB#3:                                 # %.preheader
	testl	%ebx, %ebx
	jle	.LBB12_2
# BB#4:                                 # %.lr.ph.preheader
	movl	%ebx, %eax
	cmpq	$1, %rax
	jbe	.LBB12_5
# BB#8:                                 # %min.iters.checked
	movl	%ebx, %ecx
	andl	$1, %ecx
	subq	%rcx, %rax
	je	.LBB12_5
# BB#9:                                 # %vector.ph
	movapd	%xmm2, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movl	%ebx, %edx
	movl	%ebx, %esi
	andl	$1, %esi
	subq	%rsi, %rdx
	movq	8(%rsp), %rsi           # 8-byte Reload
	.p2align	4, 0x90
.LBB12_10:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rsi), %xmm1
	divpd	%xmm0, %xmm1
	movupd	%xmm1, (%rsi)
	addq	$16, %rsi
	addq	$-2, %rdx
	jne	.LBB12_10
# BB#11:                                # %middle.block
	testq	%rcx, %rcx
	jne	.LBB12_6
	jmp	.LBB12_2
.LBB12_1:
	xorl	%ebx, %ebx
	jmp	.LBB12_2
.LBB12_5:
	xorl	%eax, %eax
.LBB12_6:                               # %.lr.ph.preheader49
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movl	%ebx, %edx
	subq	%rax, %rdx
	.p2align	4, 0x90
.LBB12_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	divsd	%xmm2, %xmm0
	movsd	%xmm0, (%rcx)
	addq	$8, %rcx
	decq	%rdx
	jne	.LBB12_7
.LBB12_2:                               # %._crit_edge
	movl	%ebx, %eax
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	fastconjuction_noweight, .Lfunc_end12-fastconjuction_noweight
	.cfi_endproc

	.globl	fastconjuction_noname
	.p2align	4, 0x90
	.type	fastconjuction_noname,@function
fastconjuction_noname:                  # @fastconjuction_noname
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi126:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi127:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi128:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi129:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi130:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi132:
	.cfi_def_cfa_offset 368
.Lcfi133:
	.cfi_offset %rbx, -56
.Lcfi134:
	.cfi_offset %r12, -48
.Lcfi135:
	.cfi_offset %r13, -40
.Lcfi136:
	.cfi_offset %r14, -32
.Lcfi137:
	.cfi_offset %r15, -24
.Lcfi138:
	.cfi_offset %rbp, -16
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, %r15
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movb	$0, (%r9)
	movl	(%r15), %r12d
	cmpl	$-1, %r12d
	je	.LBB13_1
# BB#12:                                # %.lr.ph43.preheader
	xorpd	%xmm2, %xmm2
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB13_13:                              # %.lr.ph43
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm2, 32(%rsp)         # 16-byte Spill
	movl	%r14d, %r13d
	leal	1(%r12), %edx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	leaq	48(%rsp), %rdi
	callq	sprintf
	movl	%eax, %r14d
	addl	%r13d, %r14d
	cmpl	$99, %r14d
	jg	.LBB13_15
# BB#14:                                #   in Loop: Header=BB13_13 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	leaq	48(%rsp), %rsi
	callq	strcat
.LBB13_15:                              #   in Loop: Header=BB13_13 Depth=1
	movslq	%r12d, %rax
	movq	(%rbp,%rax,8), %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rcx, (%rdx,%rbx,8)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movq	(%rsp), %rax            # 8-byte Reload
	movsd	%xmm0, (%rax,%rbx,8)
	movapd	32(%rsp), %xmm2         # 16-byte Reload
	addsd	%xmm0, %xmm2
	movl	4(%r15,%rbx,4), %r12d
	incq	%rbx
	cmpl	$-1, %r12d
	jne	.LBB13_13
# BB#3:                                 # %.preheader
	testl	%ebx, %ebx
	jle	.LBB13_2
# BB#4:                                 # %.lr.ph.preheader
	movl	%ebx, %eax
	cmpq	$1, %rax
	jbe	.LBB13_5
# BB#8:                                 # %min.iters.checked
	movl	%ebx, %ecx
	andl	$1, %ecx
	subq	%rcx, %rax
	je	.LBB13_5
# BB#9:                                 # %vector.ph
	movapd	%xmm2, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movl	%ebx, %edx
	movl	%ebx, %esi
	andl	$1, %esi
	subq	%rsi, %rdx
	movq	(%rsp), %rsi            # 8-byte Reload
	.p2align	4, 0x90
.LBB13_10:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rsi), %xmm1
	divpd	%xmm0, %xmm1
	movupd	%xmm1, (%rsi)
	addq	$16, %rsi
	addq	$-2, %rdx
	jne	.LBB13_10
# BB#11:                                # %middle.block
	testq	%rcx, %rcx
	jne	.LBB13_6
	jmp	.LBB13_2
.LBB13_1:
	xorl	%ebx, %ebx
	jmp	.LBB13_2
.LBB13_5:
	xorl	%eax, %eax
.LBB13_6:                               # %.lr.ph.preheader51
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movl	%ebx, %edx
	subq	%rax, %rdx
	.p2align	4, 0x90
.LBB13_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	divsd	%xmm2, %xmm0
	movsd	%xmm0, (%rcx)
	addq	$8, %rcx
	decq	%rdx
	jne	.LBB13_7
.LBB13_2:                               # %._crit_edge
	movl	%ebx, %eax
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	fastconjuction_noname, .Lfunc_end13-fastconjuction_noname
	.cfi_endproc

	.globl	fastconjuction
	.p2align	4, 0x90
	.type	fastconjuction,@function
fastconjuction:                         # @fastconjuction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi139:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi140:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi141:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi142:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi143:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi145:
	.cfi_def_cfa_offset 368
.Lcfi146:
	.cfi_offset %rbx, -56
.Lcfi147:
	.cfi_offset %r12, -48
.Lcfi148:
	.cfi_offset %r13, -40
.Lcfi149:
	.cfi_offset %r14, -32
.Lcfi150:
	.cfi_offset %r15, -24
.Lcfi151:
	.cfi_offset %rbp, -16
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movq	376(%rsp), %rax
	movb	$0, (%rax)
	movl	(%rbp), %r15d
	cmpl	$-1, %r15d
	je	.LBB14_1
# BB#12:                                # %.lr.ph43.preheader
	xorpd	%xmm2, %xmm2
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB14_13:                              # %.lr.ph43
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm2, 32(%rsp)         # 16-byte Spill
	movl	%r14d, %r12d
	leal	1(%r15), %edx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	leaq	48(%rsp), %rdi
	callq	sprintf
	movl	%eax, %r14d
	addl	%r12d, %r14d
	cmpl	$99, %r14d
	jg	.LBB14_15
# BB#14:                                #   in Loop: Header=BB14_13 Depth=1
	movq	376(%rsp), %rdi
	leaq	48(%rsp), %rsi
	callq	strcat
.LBB14_15:                              #   in Loop: Header=BB14_13 Depth=1
	movslq	%r15d, %rax
	movq	(%r13,%rax,8), %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rcx, (%rdx,%rbx,8)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movq	8(%rsp), %rax           # 8-byte Reload
	movsd	%xmm0, (%rax,%rbx,8)
	movapd	32(%rsp), %xmm2         # 16-byte Reload
	addsd	%xmm0, %xmm2
	movl	4(%rbp,%rbx,4), %r15d
	incq	%rbx
	cmpl	$-1, %r15d
	jne	.LBB14_13
# BB#3:                                 # %.preheader
	testl	%ebx, %ebx
	jle	.LBB14_2
# BB#4:                                 # %.lr.ph.preheader
	movl	%ebx, %eax
	cmpq	$1, %rax
	jbe	.LBB14_5
# BB#8:                                 # %min.iters.checked
	movl	%ebx, %ecx
	andl	$1, %ecx
	subq	%rcx, %rax
	je	.LBB14_5
# BB#9:                                 # %vector.ph
	movapd	%xmm2, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movl	%ebx, %edx
	movl	%ebx, %esi
	andl	$1, %esi
	subq	%rsi, %rdx
	movq	8(%rsp), %rsi           # 8-byte Reload
	.p2align	4, 0x90
.LBB14_10:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rsi), %xmm1
	divpd	%xmm0, %xmm1
	movupd	%xmm1, (%rsi)
	addq	$16, %rsi
	addq	$-2, %rdx
	jne	.LBB14_10
# BB#11:                                # %middle.block
	testq	%rcx, %rcx
	jne	.LBB14_6
	jmp	.LBB14_2
.LBB14_1:
	xorl	%ebx, %ebx
	jmp	.LBB14_2
.LBB14_5:
	xorl	%eax, %eax
.LBB14_6:                               # %.lr.ph.preheader51
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movl	%ebx, %edx
	subq	%rax, %rdx
	.p2align	4, 0x90
.LBB14_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	divsd	%xmm2, %xmm0
	movsd	%xmm0, (%rcx)
	addq	$8, %rcx
	decq	%rdx
	jne	.LBB14_7
.LBB14_2:                               # %._crit_edge
	movl	%ebx, %eax
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	fastconjuction, .Lfunc_end14-fastconjuction
	.cfi_endproc

	.globl	conjuctionfortbfast
	.p2align	4, 0x90
	.type	conjuctionfortbfast,@function
conjuctionfortbfast:                    # @conjuctionfortbfast
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi152:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi155:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi156:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi158:
	.cfi_def_cfa_offset 368
.Lcfi159:
	.cfi_offset %rbx, -56
.Lcfi160:
	.cfi_offset %r12, -48
.Lcfi161:
	.cfi_offset %r13, -40
.Lcfi162:
	.cfi_offset %r14, -32
.Lcfi163:
	.cfi_offset %r15, -24
.Lcfi164:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %ebp
	movq	368(%rsp), %rax
	movb	$0, (%rax)
	movl	njob(%rip), %esi
	xorl	%r14d, %r14d
	cmpl	%ebp, %esi
	jle	.LBB15_16
# BB#1:                                 # %.lr.ph42
	movslq	%ebp, %r12
	incl	%ebp
	xorpd	%xmm2, %xmm2
	movq	%r12, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movq	%rdi, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB15_2:                               # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	(%rdi,%rbx,8), %rbx
	leaq	1(%r12), %r13
	cmpb	$0, (%rbx,%r12)
	je	.LBB15_6
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	%rcx, %rbx
	movapd	%xmm2, 32(%rsp)         # 16-byte Spill
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	leaq	48(%rsp), %rdi
	movl	%ebp, %edx
	callq	sprintf
	movq	368(%rsp), %rdi
	callq	strlen
	cmpq	$99, %rax
	ja	.LBB15_5
# BB#4:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	368(%rsp), %rdi
	leaq	48(%rsp), %rsi
	callq	strcat
.LBB15_5:                               #   in Loop: Header=BB15_2 Depth=1
	movq	(%r15,%r12,8), %rsi
	movslq	%r14d, %r14
	movq	%rbx, %rcx
	movq	%rsi, (%rcx,%r14,8)
	movq	8(%rsp), %r9            # 8-byte Reload
	movsd	(%r9,%r12,8), %xmm0     # xmm0 = mem[0],zero
	movq	16(%rsp), %r8           # 8-byte Reload
	movsd	%xmm0, (%r8,%r14,8)
	movapd	32(%rsp), %xmm2         # 16-byte Reload
	addsd	%xmm0, %xmm2
	incl	%r14d
	movl	njob(%rip), %esi
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB15_6:                               # %._crit_edge46
                                        #   in Loop: Header=BB15_2 Depth=1
	movslq	%esi, %rbx
	incl	%ebp
	cmpq	%rbx, %r13
	movq	%r13, %r12
	jl	.LBB15_2
# BB#7:                                 # %.preheader
	testl	%r14d, %r14d
	jle	.LBB15_16
# BB#8:                                 # %.lr.ph.preheader
	movl	%r14d, %ecx
	cmpl	$1, %r14d
	je	.LBB15_9
# BB#12:                                # %min.iters.checked
	movl	%r14d, %edx
	andl	$1, %edx
	movq	%rcx, %rbp
	subq	%rdx, %rbp
	je	.LBB15_9
# BB#13:                                # %vector.ph
	movapd	%xmm2, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movq	%rbp, %rsi
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB15_14:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rdi), %xmm1
	divpd	%xmm0, %xmm1
	movupd	%xmm1, (%rdi)
	addq	$16, %rdi
	addq	$-2, %rsi
	jne	.LBB15_14
# BB#15:                                # %middle.block
	testl	%edx, %edx
	jne	.LBB15_10
	jmp	.LBB15_16
.LBB15_9:
	xorl	%ebp, %ebp
.LBB15_10:                              # %.lr.ph.preheader51
	leaq	(%r8,%rbp,8), %rdx
	subq	%rbp, %rcx
	.p2align	4, 0x90
.LBB15_11:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	divsd	%xmm2, %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	decq	%rcx
	jne	.LBB15_11
.LBB15_16:                              # %._crit_edge
	movl	%r14d, %eax
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	conjuctionfortbfast, .Lfunc_end15-conjuctionfortbfast
	.cfi_endproc

	.globl	conjuction
	.p2align	4, 0x90
	.type	conjuction,@function
conjuction:                             # @conjuction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi165:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi166:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi167:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi168:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi169:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi170:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi171:
	.cfi_def_cfa_offset 352
.Lcfi172:
	.cfi_offset %rbx, -56
.Lcfi173:
	.cfi_offset %r12, -48
.Lcfi174:
	.cfi_offset %r13, -40
.Lcfi175:
	.cfi_offset %r14, -32
.Lcfi176:
	.cfi_offset %r15, -24
.Lcfi177:
	.cfi_offset %rbp, -16
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, (%rsp)            # 8-byte Spill
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	368(%rsp), %rax
	movb	$0, (%rax)
	movl	njob(%rip), %eax
	xorl	%r14d, %r14d
	cmpl	%ebp, %eax
	jle	.LBB16_7
# BB#1:                                 # %.lr.ph
	movslq	%ebp, %r15
	incl	%ebp
	movq	%r15, %r12
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB16_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%r15,8), %rcx
	leaq	1(%r12), %r13
	cmpb	$0, (%rcx,%r12)
	je	.LBB16_6
# BB#3:                                 #   in Loop: Header=BB16_2 Depth=1
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	leaq	32(%rsp), %rdi
	movl	%ebp, %edx
	callq	sprintf
	movq	368(%rsp), %rdi
	callq	strlen
	cmpq	$99, %rax
	ja	.LBB16_5
# BB#4:                                 #   in Loop: Header=BB16_2 Depth=1
	movq	368(%rsp), %rdi
	leaq	32(%rsp), %rsi
	callq	strcat
.LBB16_5:                               #   in Loop: Header=BB16_2 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax,%r12,8), %rax
	movslq	%r14d, %r14
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, (%rcx,%r14,8)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx,%r14,8)
	incl	%r14d
	movl	njob(%rip), %eax
.LBB16_6:                               # %._crit_edge33
                                        #   in Loop: Header=BB16_2 Depth=1
	movslq	%eax, %rcx
	incl	%ebp
	cmpq	%rcx, %r13
	movq	%r13, %r12
	jl	.LBB16_2
.LBB16_7:                               # %._crit_edge
	movl	%r14d, %eax
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	conjuction, .Lfunc_end16-conjuction
	.cfi_endproc

	.globl	floatdelete
	.p2align	4, 0x90
	.type	floatdelete,@function
floatdelete:                            # @floatdelete
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi178:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi179:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi180:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi181:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi182:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi183:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi184:
	.cfi_def_cfa_offset 144
.Lcfi185:
	.cfi_offset %rbx, -56
.Lcfi186:
	.cfi_offset %r12, -48
.Lcfi187:
	.cfi_offset %r13, -40
.Lcfi188:
	.cfi_offset %r14, -32
.Lcfi189:
	.cfi_offset %r15, -24
.Lcfi190:
	.cfi_offset %rbp, -16
	decl	%edx
	cmpl	%esi, %edx
	jle	.LBB17_3
# BB#1:                                 # %.preheader.preheader
	movslq	%esi, %rcx
	movq	(%rdi), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movq	8(%rdi), %rsi
	movq	16(%rdi), %rbp
	movq	24(%rdi), %rbx
	movq	32(%rdi), %r8
	movq	40(%rdi), %r9
	movq	48(%rdi), %r10
	movq	56(%rdi), %r11
	movq	64(%rdi), %r14
	movq	72(%rdi), %r15
	movq	80(%rdi), %r12
	movq	88(%rdi), %r13
	movq	96(%rdi), %rax
	movq	%rax, -112(%rsp)        # 8-byte Spill
	movq	104(%rdi), %rax
	movq	%rax, -128(%rsp)        # 8-byte Spill
	movq	112(%rdi), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movq	120(%rdi), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movq	128(%rdi), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movq	136(%rdi), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movq	144(%rdi), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movq	152(%rdi), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movq	160(%rdi), %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	movq	168(%rdi), %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	movq	176(%rdi), %rax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	movq	184(%rdi), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	movq	192(%rdi), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	movq	200(%rdi), %rax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	movslq	%edx, %rdx
	movq	-120(%rsp), %rax        # 8-byte Reload
	leaq	4(%rax,%rcx,4), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	leaq	4(%rsi,%rcx,4), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	4(%rbp,%rcx,4), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	4(%rbx,%rcx,4), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	4(%r8,%rcx,4), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	4(%r9,%rcx,4), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	4(%r10,%rcx,4), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	4(%r11,%rcx,4), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	4(%r14,%rcx,4), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	4(%r15,%rcx,4), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	4(%r12,%rcx,4), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	4(%r13,%rcx,4), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	-112(%rsp), %rax        # 8-byte Reload
	leaq	4(%rax,%rcx,4), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movq	-128(%rsp), %rax        # 8-byte Reload
	leaq	4(%rax,%rcx,4), %rax
	movq	%rax, -128(%rsp)        # 8-byte Spill
	movq	-16(%rsp), %rax         # 8-byte Reload
	leaq	4(%rax,%rcx,4), %r8
	movq	-24(%rsp), %rax         # 8-byte Reload
	leaq	4(%rax,%rcx,4), %r9
	movq	-32(%rsp), %rax         # 8-byte Reload
	leaq	4(%rax,%rcx,4), %r10
	movq	-40(%rsp), %rax         # 8-byte Reload
	leaq	4(%rax,%rcx,4), %r11
	movq	-48(%rsp), %rax         # 8-byte Reload
	leaq	4(%rax,%rcx,4), %r14
	movq	-56(%rsp), %rax         # 8-byte Reload
	leaq	4(%rax,%rcx,4), %r15
	movq	-64(%rsp), %rax         # 8-byte Reload
	leaq	4(%rax,%rcx,4), %r12
	movq	-72(%rsp), %rax         # 8-byte Reload
	leaq	4(%rax,%rcx,4), %r13
	movq	-80(%rsp), %rax         # 8-byte Reload
	leaq	4(%rax,%rcx,4), %rsi
	movq	-88(%rsp), %rax         # 8-byte Reload
	leaq	4(%rax,%rcx,4), %rbx
	movq	-96(%rsp), %rax         # 8-byte Reload
	leaq	4(%rax,%rcx,4), %rdi
	movq	-104(%rsp), %rax        # 8-byte Reload
	leaq	4(%rax,%rcx,4), %rax
	subq	%rcx, %rdx
	movq	%rdx, -112(%rsp)        # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB17_2:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	-120(%rsp), %rbp        # 8-byte Reload
	movl	(%rbp,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	movq	80(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	movq	72(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	movq	64(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	movq	56(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	movq	48(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	movq	40(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	movq	32(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	movq	24(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	(%rbp,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	(%rbp,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	movq	-8(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	movq	-128(%rsp), %rbp        # 8-byte Reload
	movl	(%rbp,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	movl	(%r8,%rcx,4), %edx
	movl	%edx, -4(%r8,%rcx,4)
	movl	(%r9,%rcx,4), %edx
	movl	%edx, -4(%r9,%rcx,4)
	movl	(%r10,%rcx,4), %edx
	movl	%edx, -4(%r10,%rcx,4)
	movl	(%r11,%rcx,4), %edx
	movl	%edx, -4(%r11,%rcx,4)
	movl	(%r14,%rcx,4), %edx
	movl	%edx, -4(%r14,%rcx,4)
	movl	(%r15,%rcx,4), %edx
	movl	%edx, -4(%r15,%rcx,4)
	movl	(%r12,%rcx,4), %edx
	movl	%edx, -4(%r12,%rcx,4)
	movl	(%r13,%rcx,4), %edx
	movl	%edx, -4(%r13,%rcx,4)
	movl	(%rsi,%rcx,4), %edx
	movl	%edx, -4(%rsi,%rcx,4)
	movl	(%rbx,%rcx,4), %edx
	movl	%edx, -4(%rbx,%rcx,4)
	movl	(%rdi,%rcx,4), %edx
	movl	%edx, -4(%rdi,%rcx,4)
	movl	(%rax,%rcx,4), %edx
	movl	%edx, -4(%rax,%rcx,4)
	incq	%rcx
	cmpq	%rcx, -112(%rsp)        # 8-byte Folded Reload
	jne	.LBB17_2
.LBB17_3:                               # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	floatdelete, .Lfunc_end17-floatdelete
	.cfi_endproc

	.globl	chardelete
	.p2align	4, 0x90
	.type	chardelete,@function
chardelete:                             # @chardelete
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi191:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi192:
	.cfi_def_cfa_offset 24
	subq	$5000008, %rsp          # imm = 0x4C4B48
.Lcfi193:
	.cfi_def_cfa_offset 5000032
.Lcfi194:
	.cfi_offset %rbx, -24
.Lcfi195:
	.cfi_offset %r14, -16
	movslq	%esi, %rax
	leaq	(%rdi,%rax), %rbx
	leaq	1(%rdi,%rax), %rsi
	movq	%rsp, %r14
	movq	%r14, %rdi
	callq	strcpy
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	strcpy
	addq	$5000008, %rsp          # imm = 0x4C4B48
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	chardelete, .Lfunc_end18-chardelete
	.cfi_endproc

	.globl	RootBranchNode
	.p2align	4, 0x90
	.type	RootBranchNode,@function
RootBranchNode:                         # @RootBranchNode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi196:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi197:
	.cfi_def_cfa_offset 24
.Lcfi198:
	.cfi_offset %rbx, -24
.Lcfi199:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leal	1(%rdx), %ebx
	addl	$-2, %edi
	movl	$1, %eax
	cmpl	%edi, %ebx
	jge	.LBB19_9
# BB#1:                                 # %.preheader34.lr.ph
	movslq	%edx, %r8
	movslq	%ecx, %r9
	movslq	%ebx, %r11
	movl	$1, %eax
	.p2align	4, 0x90
.LBB19_3:                               # %.preheader34
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_5 Depth 2
                                        #     Child Loop BB19_8 Depth 2
	movq	(%rsi,%r11,8), %r10
	movq	(%r10), %rcx
	movl	(%rcx), %ebx
	testl	%ebx, %ebx
	js	.LBB19_6
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB19_3 Depth=1
	movq	(%rsi,%r8,8), %rdx
	movq	(%rdx,%r9,8), %rdx
	movl	(%rdx), %edx
	addq	$4, %rcx
	.p2align	4, 0x90
.LBB19_5:                               #   Parent Loop BB19_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%ebp, %ebp
	cmpl	%edx, %ebx
	sete	%bpl
	addl	%ebp, %eax
	movl	(%rcx), %ebx
	addq	$4, %rcx
	testl	%ebx, %ebx
	jns	.LBB19_5
.LBB19_6:                               # %.preheader
                                        #   in Loop: Header=BB19_3 Depth=1
	movq	8(%r10), %rcx
	movl	(%rcx), %ebx
	testl	%ebx, %ebx
	js	.LBB19_2
# BB#7:                                 # %.lr.ph40
                                        #   in Loop: Header=BB19_3 Depth=1
	movq	(%rsi,%r8,8), %rdx
	movq	(%rdx,%r9,8), %rdx
	movl	(%rdx), %edx
	addq	$4, %rcx
	.p2align	4, 0x90
.LBB19_8:                               #   Parent Loop BB19_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%ebp, %ebp
	cmpl	%edx, %ebx
	sete	%bpl
	addl	%ebp, %eax
	movl	(%rcx), %ebx
	addq	$4, %rcx
	testl	%ebx, %ebx
	jns	.LBB19_8
.LBB19_2:                               # %.loopexit
                                        #   in Loop: Header=BB19_3 Depth=1
	incq	%r11
	cmpl	%edi, %r11d
	jne	.LBB19_3
.LBB19_9:                               # %._crit_edge
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end19:
	.size	RootBranchNode, .Lfunc_end19-RootBranchNode
	.cfi_endproc

	.globl	BranchLeafNode
	.p2align	4, 0x90
	.type	BranchLeafNode,@function
BranchLeafNode:                         # @BranchLeafNode
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi200:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi201:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi202:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi203:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi204:
	.cfi_def_cfa_offset 48
.Lcfi205:
	.cfi_offset %rbx, -40
.Lcfi206:
	.cfi_offset %r12, -32
.Lcfi207:
	.cfi_offset %r14, -24
.Lcfi208:
	.cfi_offset %r15, -16
	movl	%r8d, %r14d
	movl	%ecx, %r15d
	movq	%rdx, %rbx
	movq	%rsi, %r12
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB20_2
# BB#1:                                 # %.lr.ph51.preheader
	decl	%edi
	leaq	4(,%rdi,4), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	memset
.LBB20_2:                               # %.preheader40
	cmpl	$2, %r15d
	jl	.LBB20_11
# BB#3:                                 # %.preheader39.preheader
	leal	-1(%r15), %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB20_4:                               # %.preheader39
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_6 Depth 2
                                        #     Child Loop BB20_9 Depth 2
	movq	(%r12,%rcx,8), %rdx
	movq	(%rdx), %rsi
	movl	(%rsi), %edi
	testl	%edi, %edi
	js	.LBB20_7
# BB#5:                                 # %.lr.ph45.preheader
                                        #   in Loop: Header=BB20_4 Depth=1
	addq	$4, %rsi
	.p2align	4, 0x90
.LBB20_6:                               # %.lr.ph45
                                        #   Parent Loop BB20_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edi, %rdi
	incl	(%rbx,%rdi,4)
	movl	(%rsi), %edi
	addq	$4, %rsi
	testl	%edi, %edi
	jns	.LBB20_6
.LBB20_7:                               # %._crit_edge46
                                        #   in Loop: Header=BB20_4 Depth=1
	movq	8(%rdx), %rdx
	movl	(%rdx), %esi
	testl	%esi, %esi
	js	.LBB20_10
# BB#8:                                 # %.lr.ph45.1.preheader
                                        #   in Loop: Header=BB20_4 Depth=1
	addq	$4, %rdx
	.p2align	4, 0x90
.LBB20_9:                               # %.lr.ph45.1
                                        #   Parent Loop BB20_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %rsi
	incl	(%rbx,%rsi,4)
	movl	(%rdx), %esi
	addq	$4, %rdx
	testl	%esi, %esi
	jns	.LBB20_9
.LBB20_10:                              # %._crit_edge46.1
                                        #   in Loop: Header=BB20_4 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jne	.LBB20_4
.LBB20_11:                              # %.preheader37
	testl	%r14d, %r14d
	js	.LBB20_17
# BB#12:                                # %.preheader.lr.ph
	movslq	%r15d, %rax
	movq	(%r12,%rax,8), %rax
	incl	%r14d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB20_13:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_15 Depth 2
	movq	(%rax,%rcx,8), %rdx
	movl	(%rdx), %esi
	testl	%esi, %esi
	js	.LBB20_16
# BB#14:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB20_13 Depth=1
	addq	$4, %rdx
	.p2align	4, 0x90
.LBB20_15:                              # %.lr.ph
                                        #   Parent Loop BB20_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %rsi
	incl	(%rbx,%rsi,4)
	movl	(%rdx), %esi
	addq	$4, %rdx
	testl	%esi, %esi
	jns	.LBB20_15
.LBB20_16:                              # %._crit_edge
                                        #   in Loop: Header=BB20_13 Depth=1
	incq	%rcx
	cmpq	%r14, %rcx
	jne	.LBB20_13
.LBB20_17:                              # %._crit_edge43
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end20:
	.size	BranchLeafNode, .Lfunc_end20-BranchLeafNode
	.cfi_endproc

	.globl	RootLeafNode
	.p2align	4, 0x90
	.type	RootLeafNode,@function
RootLeafNode:                           # @RootLeafNode
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi209:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi210:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi211:
	.cfi_def_cfa_offset 32
.Lcfi212:
	.cfi_offset %rbx, -32
.Lcfi213:
	.cfi_offset %r14, -24
.Lcfi214:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movl	%edi, %r15d
	testl	%r15d, %r15d
	jle	.LBB21_10
# BB#1:                                 # %.preheader26
	leal	-1(%r15), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	memset
	cmpl	$3, %r15d
	jl	.LBB21_10
# BB#2:                                 # %.preheader25.preheader
	addl	$-2, %r15d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB21_3:                               # %.preheader25
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_5 Depth 2
                                        #     Child Loop BB21_8 Depth 2
	movq	(%r14,%rax,8), %rcx
	movq	(%rcx), %rdx
	movl	(%rdx), %esi
	testl	%esi, %esi
	js	.LBB21_6
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB21_3 Depth=1
	addq	$4, %rdx
	.p2align	4, 0x90
.LBB21_5:                               # %.lr.ph
                                        #   Parent Loop BB21_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %rsi
	incl	(%rbx,%rsi,4)
	movl	(%rdx), %esi
	addq	$4, %rdx
	testl	%esi, %esi
	jns	.LBB21_5
.LBB21_6:                               # %._crit_edge
                                        #   in Loop: Header=BB21_3 Depth=1
	movq	8(%rcx), %rcx
	movl	(%rcx), %edx
	testl	%edx, %edx
	js	.LBB21_9
# BB#7:                                 # %.lr.ph.1.preheader
                                        #   in Loop: Header=BB21_3 Depth=1
	addq	$4, %rcx
	.p2align	4, 0x90
.LBB21_8:                               # %.lr.ph.1
                                        #   Parent Loop BB21_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	incl	(%rbx,%rdx,4)
	movl	(%rcx), %edx
	addq	$4, %rcx
	testl	%edx, %edx
	jns	.LBB21_8
.LBB21_9:                               # %._crit_edge.1
                                        #   in Loop: Header=BB21_3 Depth=1
	incq	%rax
	cmpq	%r15, %rax
	jne	.LBB21_3
.LBB21_10:                              # %._crit_edge30
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	RootLeafNode, .Lfunc_end21-RootLeafNode
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI22_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	nodeFromABranch
	.p2align	4, 0x90
	.type	nodeFromABranch,@function
nodeFromABranch:                        # @nodeFromABranch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi215:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi216:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi217:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi218:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi219:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi220:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi221:
	.cfi_def_cfa_offset 64
.Lcfi222:
	.cfi_offset %rbx, -56
.Lcfi223:
	.cfi_offset %r12, -48
.Lcfi224:
	.cfi_offset %r13, -40
.Lcfi225:
	.cfi_offset %r14, -32
.Lcfi226:
	.cfi_offset %r15, -24
.Lcfi227:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movq	%rcx, %rbp
	movq	%rdx, %r13
	movq	%rsi, %r14
	movl	%edi, %r15d
	movl	64(%rsp), %ebx
	cmpq	$0, nodeFromABranch.outergroup2(%rip)
	jne	.LBB22_2
# BB#1:
	movl	%r15d, %edi
	callq	AllocateIntVec
	movq	%rax, nodeFromABranch.outergroup2(%rip)
	movl	%r15d, %edi
	callq	AllocateIntVec
	movq	%rax, nodeFromABranch.table(%rip)
.LBB22_2:
	movslq	%r12d, %rax
	movq	(%rbp,%rax,8), %rcx
	movslq	%ebx, %rdx
	movq	(%rcx,%rdx,8), %r10
	xorl	%esi, %esi
	testl	%edx, %edx
	sete	%sil
	movq	(%rcx,%rsi,8), %r11
	testl	%r15d, %r15d
	jle	.LBB22_11
# BB#3:                                 # %.lr.ph103
	movq	nodeFromABranch.table(%rip), %r8
	movl	%r15d, %edx
	cmpl	$7, %r15d
	jbe	.LBB22_8
# BB#4:                                 # %min.iters.checked
	movl	%r15d, %r9d
	andl	$7, %r9d
	movq	%rdx, %rbp
	subq	%r9, %rbp
	je	.LBB22_8
# BB#5:                                 # %vector.body.preheader
	leaq	16(%r8), %rsi
	movaps	.LCPI22_0(%rip), %xmm0  # xmm0 = [1,1,1,1]
	movq	%rbp, %rdi
	.p2align	4, 0x90
.LBB22_6:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -16(%rsi)
	movups	%xmm0, (%rsi)
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB22_6
# BB#7:                                 # %middle.block
	testl	%r9d, %r9d
	jne	.LBB22_9
	jmp	.LBB22_11
.LBB22_8:
	xorl	%ebp, %ebp
.LBB22_9:                               # %scalar.ph.preheader
	leaq	(%r8,%rbp,4), %rsi
	subq	%rbp, %rdx
	.p2align	4, 0x90
.LBB22_10:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, (%rsi)
	addq	$4, %rsi
	decq	%rdx
	jne	.LBB22_10
.LBB22_11:                              # %.preheader86
	movl	(%r10), %edx
	testl	%edx, %edx
	js	.LBB22_14
# BB#12:                                # %.lr.ph100
	movq	nodeFromABranch.table(%rip), %rsi
	leaq	4(%r10), %rdi
	.p2align	4, 0x90
.LBB22_13:                              # =>This Inner Loop Header: Depth=1
	movslq	%edx, %rdx
	movl	$0, (%rsi,%rdx,4)
	movl	(%rdi), %edx
	addq	$4, %rdi
	testl	%edx, %edx
	jns	.LBB22_13
.LBB22_14:                              # %.preheader85
	movl	(%r11), %edx
	testl	%edx, %edx
	js	.LBB22_17
# BB#15:                                # %.lr.ph98
	movq	nodeFromABranch.table(%rip), %rsi
	leaq	4(%r11), %rdi
	.p2align	4, 0x90
.LBB22_16:                              # =>This Inner Loop Header: Depth=1
	movslq	%edx, %rdx
	movl	$0, (%rsi,%rdx,4)
	movl	(%rdi), %edx
	addq	$4, %rdi
	testl	%edx, %edx
	jns	.LBB22_16
.LBB22_17:                              # %.preheader84
	testl	%r15d, %r15d
	jle	.LBB22_21
# BB#18:                                # %.lr.ph95
	movq	nodeFromABranch.table(%rip), %rsi
	movq	nodeFromABranch.outergroup2(%rip), %rdx
	movl	%r15d, %r8d
	testb	$1, %r8b
	jne	.LBB22_22
# BB#19:
	xorl	%ebp, %ebp
                                        # implicit-def: %R9D
	jmp	.LBB22_20
.LBB22_21:                              # %.preheader84.._crit_edge96_crit_edge
	xorl	%r9d, %r9d
	movq	nodeFromABranch.outergroup2(%rip), %rdx
	jmp	.LBB22_31
.LBB22_22:
	cmpl	$0, (%rsi)
	je	.LBB22_24
# BB#23:
	movl	$0, (%rdx)
	movl	$1, %r9d
	movl	$1, %ebp
	movl	$1, %edi
	cmpl	$1, %r15d
	je	.LBB22_31
	jmp	.LBB22_25
.LBB22_24:
	xorl	%r9d, %r9d
	movl	$1, %ebp
.LBB22_20:
	xorl	%edi, %edi
	cmpl	$1, %r15d
	je	.LBB22_31
	.p2align	4, 0x90
.LBB22_25:                              # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rsi,%rbp,4)
	je	.LBB22_27
# BB#26:                                #   in Loop: Header=BB22_25 Depth=1
	movslq	%edi, %rdi
	movl	%ebp, (%rdx,%rdi,4)
	incl	%edi
.LBB22_27:                              #   in Loop: Header=BB22_25 Depth=1
	cmpl	$0, 4(%rsi,%rbp,4)
	je	.LBB22_29
# BB#28:                                #   in Loop: Header=BB22_25 Depth=1
	movslq	%edi, %rdi
	leal	1(%rbp), %eax
	movl	%eax, (%rdx,%rdi,4)
	incl	%edi
.LBB22_29:                              #   in Loop: Header=BB22_25 Depth=1
	addq	$2, %rbp
	cmpq	%rbp, %r8
	jne	.LBB22_25
# BB#30:
	movl	%edi, %r9d
.LBB22_31:                              # %._crit_edge96
	movslq	%r9d, %rsi
	movl	$-1, (%rdx,%rsi,4)
	movl	(%r10), %edi
	testl	%edi, %edi
	js	.LBB22_34
# BB#32:                                # %.lr.ph92.preheader
	leaq	4(%r10), %rsi
	.p2align	4, 0x90
.LBB22_33:                              # %.lr.ph92
                                        # =>This Inner Loop Header: Depth=1
	movslq	%edi, %rdi
	movq	(%r13,%rdi,8), %rbp
	movslq	(%r11), %rax
	movl	(%rbp,%rax,4), %ecx
	movslq	(%rdx), %rbx
	movl	(%rbp,%rbx,4), %ebp
	movq	(%r13,%rax,8), %rax
	leal	-1(%rcx,%rbp), %ecx
	subl	(%rax,%rbx,4), %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	movl	%eax, (%r14,%rdi,4)
	movl	(%rsi), %edi
	addq	$4, %rsi
	testl	%edi, %edi
	jns	.LBB22_33
.LBB22_34:                              # %.preheader83
	movl	(%r11), %edi
	testl	%edi, %edi
	js	.LBB22_37
# BB#35:                                # %.lr.ph89.preheader
	leaq	4(%r11), %rsi
	.p2align	4, 0x90
.LBB22_36:                              # %.lr.ph89
                                        # =>This Inner Loop Header: Depth=1
	movslq	%edi, %rax
	movq	(%r13,%rax,8), %rcx
	movslq	(%rdx), %rdi
	movl	(%rcx,%rdi,4), %ebp
	movslq	(%r10), %rbx
	movl	(%rcx,%rbx,4), %ecx
	movq	(%r13,%rbx,8), %rbx
	leal	1(%rbp,%rcx), %ecx
	subl	(%rbx,%rdi,4), %ecx
	movl	%ecx, %edi
	shrl	$31, %edi
	addl	%ecx, %edi
	sarl	%edi
	movl	%edi, (%r14,%rax,4)
	movl	(%rsi), %edi
	addq	$4, %rsi
	testl	%edi, %edi
	jns	.LBB22_36
.LBB22_37:                              # %.preheader
	movl	(%rdx), %esi
	testl	%esi, %esi
	js	.LBB22_40
# BB#38:                                # %.lr.ph.preheader
	addq	$4, %rdx
	.p2align	4, 0x90
.LBB22_39:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%esi, %rax
	movq	(%r13,%rax,8), %rcx
	movslq	(%r11), %rsi
	movl	(%rcx,%rsi,4), %edi
	movslq	(%r10), %rbp
	movl	(%rcx,%rbp,4), %ecx
	movq	(%r13,%rbp,8), %rbp
	leal	1(%rdi,%rcx), %ecx
	subl	(%rbp,%rsi,4), %ecx
	movl	%ecx, %esi
	shrl	$31, %esi
	addl	%ecx, %esi
	sarl	%esi
	movl	%esi, (%r14,%rax,4)
	movl	(%rdx), %esi
	addq	$4, %rdx
	testl	%esi, %esi
	jns	.LBB22_39
.LBB22_40:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	nodeFromABranch, .Lfunc_end22-nodeFromABranch
	.cfi_endproc

	.globl	OneClusterAndTheOther
	.p2align	4, 0x90
	.type	OneClusterAndTheOther,@function
OneClusterAndTheOther:                  # @OneClusterAndTheOther
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi228:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi229:
	.cfi_def_cfa_offset 24
.Lcfi230:
	.cfi_offset %rbx, -24
.Lcfi231:
	.cfi_offset %r14, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movslq	%r9d, %r9
	movq	(%r8,%r9,8), %rax
	movslq	24(%rsp), %r10
	movq	(%rax,%r10,8), %rax
	movslq	(%rax), %r11
	movl	%r11d, (%rdx)
	movslq	(%rax), %rbx
	testq	%rbx, %rbx
	js	.LBB23_4
# BB#1:                                 # %.lr.ph37.preheader
	movq	(%rsi,%r11,8), %rax
	movb	$1, (%rax,%rbx)
	movq	(%r8,%r9,8), %rax
	movq	(%rax,%r10,8), %rax
	movl	4(%rax), %r11d
	testl	%r11d, %r11d
	js	.LBB23_4
# BB#2:                                 # %.lr.ph37..lr.ph37_crit_edge.preheader
	movl	$8, %eax
	.p2align	4, 0x90
.LBB23_3:                               # %.lr.ph37..lr.ph37_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rdx), %rbx
	movq	(%rsi,%rbx,8), %r14
	movslq	%r11d, %rbx
	movb	$1, (%r14,%rbx)
	movq	(%r8,%r9,8), %rbx
	movq	(%rbx,%r10,8), %rbx
	movl	(%rbx,%rax), %r11d
	addq	$4, %rax
	testl	%r11d, %r11d
	jns	.LBB23_3
.LBB23_4:                               # %.preheader
	testl	%edi, %edi
	jle	.LBB23_9
# BB#5:                                 # %.lr.ph34
	movslq	(%rdx), %rax
	movq	(%rsi,%rax,8), %r8
	movslq	%edi, %r9
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB23_6:                               # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%r8,%rax)
	je	.LBB23_7
# BB#8:                                 #   in Loop: Header=BB23_6 Depth=1
	incq	%rax
	cmpq	%r9, %rax
	jl	.LBB23_6
	jmp	.LBB23_9
.LBB23_7:
	movl	%eax, (%rcx)
.LBB23_9:                               # %.loopexit
	movslq	(%rcx), %r8
	cmpl	%edi, %r8d
	jge	.LBB23_20
# BB#10:                                # %.lr.ph.preheader
	movslq	%edi, %rax
	subl	%r8d, %edi
	leaq	-1(%rax), %r9
	testb	$1, %dil
	movq	%r8, %rdi
	je	.LBB23_14
# BB#11:                                # %.lr.ph.prol
	movslq	(%rdx), %rdi
	movq	(%rsi,%rdi,8), %rdi
	cmpb	$0, (%rdi,%r8)
	jne	.LBB23_13
# BB#12:
	movslq	(%rcx), %rdi
	movq	(%rsi,%rdi,8), %rdi
	movb	$1, (%rdi,%r8)
.LBB23_13:
	leaq	1(%r8), %rdi
.LBB23_14:                              # %.lr.ph.prol.loopexit
	cmpq	%r8, %r9
	je	.LBB23_20
	.p2align	4, 0x90
.LBB23_15:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rdx), %rbx
	movq	(%rsi,%rbx,8), %rbx
	cmpb	$0, (%rbx,%rdi)
	jne	.LBB23_17
# BB#16:                                #   in Loop: Header=BB23_15 Depth=1
	movslq	(%rcx), %rbx
	movq	(%rsi,%rbx,8), %rbx
	movb	$1, (%rbx,%rdi)
.LBB23_17:                              # %.lr.ph.147
                                        #   in Loop: Header=BB23_15 Depth=1
	movslq	(%rdx), %rbx
	movq	(%rsi,%rbx,8), %rbx
	cmpb	$0, 1(%rbx,%rdi)
	jne	.LBB23_19
# BB#18:                                #   in Loop: Header=BB23_15 Depth=1
	movslq	(%rcx), %rbx
	movq	(%rsi,%rbx,8), %rbx
	movb	$1, 1(%rbx,%rdi)
.LBB23_19:                              #   in Loop: Header=BB23_15 Depth=1
	addq	$2, %rdi
	cmpq	%rdi, %rax
	jne	.LBB23_15
.LBB23_20:                              # %._crit_edge
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end23:
	.size	OneClusterAndTheOther, .Lfunc_end23-OneClusterAndTheOther
	.cfi_endproc

	.globl	makeEffMtx
	.p2align	4, 0x90
	.type	makeEffMtx,@function
makeEffMtx:                             # @makeEffMtx
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi232:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi233:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi234:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi235:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi236:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi237:
	.cfi_def_cfa_offset 56
.Lcfi238:
	.cfi_offset %rbx, -56
.Lcfi239:
	.cfi_offset %r12, -48
.Lcfi240:
	.cfi_offset %r13, -40
.Lcfi241:
	.cfi_offset %r14, -32
.Lcfi242:
	.cfi_offset %r15, -24
.Lcfi243:
	.cfi_offset %rbp, -16
	testl	%edi, %edi
	jle	.LBB24_18
# BB#1:                                 # %.preheader.us.preheader
	movl	%edi, %r12d
	leaq	(%rdx,%r12,8), %r8
	leaq	-1(%r12), %r14
	movl	%edi, %r9d
	andl	$3, %r9d
	movq	%r12, %r10
	subq	%r9, %r10
	leaq	16(%rdx), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB24_2:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_8 Depth 2
                                        #     Child Loop BB24_14 Depth 2
                                        #     Child Loop BB24_16 Depth 2
	cmpl	$4, %edi
	leaq	(%rdx,%r15,8), %r11
	movq	(%rsi,%r15,8), %rbx
	jb	.LBB24_11
# BB#4:                                 # %min.iters.checked
                                        #   in Loop: Header=BB24_2 Depth=1
	testq	%r10, %r10
	je	.LBB24_11
# BB#5:                                 # %vector.memcheck
                                        #   in Loop: Header=BB24_2 Depth=1
	leaq	(%rbx,%r12,8), %rbp
	cmpq	%r8, %rbx
	sbbb	%al, %al
	cmpq	%rbp, %rdx
	sbbb	%cl, %cl
	andb	%al, %cl
	cmpq	%r11, %rbx
	sbbb	%al, %al
	cmpq	%rbp, %r11
	sbbb	%bpl, %bpl
	testb	$1, %cl
	jne	.LBB24_11
# BB#6:                                 # %vector.memcheck
                                        #   in Loop: Header=BB24_2 Depth=1
	andb	%bpl, %al
	andb	$1, %al
	movl	$0, %eax
	jne	.LBB24_12
# BB#7:                                 # %vector.body.preheader
                                        #   in Loop: Header=BB24_2 Depth=1
	movsd	(%r11), %xmm0           # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	leaq	16(%rbx), %rax
	movq	%r10, %rbp
	movq	-8(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB24_8:                               # %vector.body
                                        #   Parent Loop BB24_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-16(%rcx), %xmm1
	movupd	(%rcx), %xmm2
	mulpd	%xmm0, %xmm1
	mulpd	%xmm0, %xmm2
	movupd	%xmm1, -16(%rax)
	movupd	%xmm2, (%rax)
	addq	$32, %rcx
	addq	$32, %rax
	addq	$-4, %rbp
	jne	.LBB24_8
# BB#9:                                 # %middle.block
                                        #   in Loop: Header=BB24_2 Depth=1
	testl	%r9d, %r9d
	movq	%r10, %rax
	jne	.LBB24_12
	jmp	.LBB24_17
	.p2align	4, 0x90
.LBB24_11:                              #   in Loop: Header=BB24_2 Depth=1
	xorl	%eax, %eax
.LBB24_12:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB24_2 Depth=1
	movl	%r12d, %ebp
	subl	%eax, %ebp
	movq	%r14, %r13
	subq	%rax, %r13
	andq	$3, %rbp
	je	.LBB24_15
# BB#13:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB24_2 Depth=1
	negq	%rbp
	.p2align	4, 0x90
.LBB24_14:                              # %scalar.ph.prol
                                        #   Parent Loop BB24_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r11), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rdx,%rax,8), %xmm0
	movsd	%xmm0, (%rbx,%rax,8)
	incq	%rax
	incq	%rbp
	jne	.LBB24_14
.LBB24_15:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB24_2 Depth=1
	cmpq	$3, %r13
	jb	.LBB24_17
	.p2align	4, 0x90
.LBB24_16:                              # %scalar.ph
                                        #   Parent Loop BB24_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r11), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rdx,%rax,8), %xmm0
	movsd	%xmm0, (%rbx,%rax,8)
	movsd	(%r11), %xmm0           # xmm0 = mem[0],zero
	mulsd	8(%rdx,%rax,8), %xmm0
	movsd	%xmm0, 8(%rbx,%rax,8)
	movsd	(%r11), %xmm0           # xmm0 = mem[0],zero
	mulsd	16(%rdx,%rax,8), %xmm0
	movsd	%xmm0, 16(%rbx,%rax,8)
	movsd	(%r11), %xmm0           # xmm0 = mem[0],zero
	mulsd	24(%rdx,%rax,8), %xmm0
	movsd	%xmm0, 24(%rbx,%rax,8)
	addq	$4, %rax
	cmpq	%rax, %r12
	jne	.LBB24_16
.LBB24_17:                              # %._crit_edge.us
                                        #   in Loop: Header=BB24_2 Depth=1
	incq	%r15
	cmpq	%rax, %r15
	jne	.LBB24_2
.LBB24_18:                              # %._crit_edge18
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	makeEffMtx, .Lfunc_end24-makeEffMtx
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI25_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	node_eff
	.p2align	4, 0x90
	.type	node_eff,@function
node_eff:                               # @node_eff
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi244:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi245:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi246:
	.cfi_def_cfa_offset 32
.Lcfi247:
	.cfi_offset %rbx, -32
.Lcfi248:
	.cfi_offset %r14, -24
.Lcfi249:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	testl	%edi, %edi
	jle	.LBB25_3
# BB#1:                                 # %.lr.ph.preheader
	movl	%edi, %r15d
	.p2align	4, 0x90
.LBB25_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14), %edi
	movsd	.LCPI25_0(%rip), %xmm0  # xmm0 = mem[0],zero
	callq	ipower
	movss	geta2(%rip), %xmm1      # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rbx)
	addq	$4, %r14
	addq	$8, %rbx
	decq	%r15
	jne	.LBB25_2
.LBB25_3:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end25:
	.size	node_eff, .Lfunc_end25-node_eff
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI26_0:
	.quad	-4616189618054758400    # double -1
	.text
	.globl	shrinklocalhom
	.p2align	4, 0x90
	.type	shrinklocalhom,@function
shrinklocalhom:                         # @shrinklocalhom
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi250:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi251:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi252:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi253:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi254:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi255:
	.cfi_def_cfa_offset 56
.Lcfi256:
	.cfi_offset %rbx, -56
.Lcfi257:
	.cfi_offset %r12, -48
.Lcfi258:
	.cfi_offset %r13, -40
.Lcfi259:
	.cfi_offset %r14, -32
.Lcfi260:
	.cfi_offset %r15, -24
.Lcfi261:
	.cfi_offset %rbp, -16
	movslq	njob(%rip), %r12
	cmpl	%esi, %r12d
	jle	.LBB26_10
# BB#1:                                 # %.lr.ph45
	movslq	%esi, %r9
	movslq	%edx, %r15
	movq	%r15, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,4), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	movsd	.LCPI26_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movq	%r9, %r13
	.p2align	4, 0x90
.LBB26_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_5 Depth 2
	movq	(%rdi,%r9,8), %rax
	cmpb	$0, (%rax,%r13)
	je	.LBB26_9
# BB#3:                                 # %.preheader
                                        #   in Loop: Header=BB26_2 Depth=1
	cmpl	%edx, %r12d
	jle	.LBB26_8
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB26_2 Depth=1
	movslq	%r11d, %r14
	xorl	%esi, %esi
	movq	-8(%rsp), %rax          # 8-byte Reload
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB26_5:                               #   Parent Loop BB26_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%r15,8), %rbp
	cmpb	$0, (%rbp,%rbx)
	je	.LBB26_7
# BB#6:                                 #   in Loop: Header=BB26_5 Depth=2
	movq	(%rcx,%r13,8), %rbp
	movsd	40(%rbp,%rax), %xmm1    # xmm1 = mem[0],zero
	addq	%rax, %rbp
	ucomisd	%xmm0, %xmm1
	movl	$0, %r10d
	cmovneq	%rbp, %r10
	cmovpq	%rbp, %r10
	movq	(%r8,%r14,8), %rbp
	movslq	%esi, %rsi
	movq	%r10, (%rbp,%rsi,8)
	incl	%esi
.LBB26_7:                               #   in Loop: Header=BB26_5 Depth=2
	incq	%rbx
	addq	$80, %rax
	cmpq	%r12, %rbx
	jl	.LBB26_5
.LBB26_8:                               # %._crit_edge
                                        #   in Loop: Header=BB26_2 Depth=1
	incl	%r11d
.LBB26_9:                               #   in Loop: Header=BB26_2 Depth=1
	incq	%r13
	cmpq	%r12, %r13
	jl	.LBB26_2
.LBB26_10:                              # %._crit_edge46
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	shrinklocalhom, .Lfunc_end26-shrinklocalhom
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI27_0:
	.quad	-4616189618054758400    # double -1
	.text
	.globl	msshrinklocalhom
	.p2align	4, 0x90
	.type	msshrinklocalhom,@function
msshrinklocalhom:                       # @msshrinklocalhom
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi262:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi263:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi264:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi265:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi266:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi267:
	.cfi_def_cfa_offset 56
.Lcfi268:
	.cfi_offset %rbx, -56
.Lcfi269:
	.cfi_offset %r12, -48
.Lcfi270:
	.cfi_offset %r13, -40
.Lcfi271:
	.cfi_offset %r14, -32
.Lcfi272:
	.cfi_offset %r15, -24
.Lcfi273:
	.cfi_offset %rbp, -16
	movslq	njob(%rip), %r12
	cmpl	%esi, %r12d
	jle	.LBB27_13
# BB#1:                                 # %.lr.ph57
	movslq	%esi, %r9
	movslq	%edx, %r15
	leaq	(%r15,%r15,4), %rax
	shlq	$4, %rax
	addq	$40, %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	movsd	.LCPI27_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movq	%r9, %r13
	.p2align	4, 0x90
.LBB27_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_5 Depth 2
	movq	(%rdi,%r9,8), %rax
	cmpb	$0, (%rax,%r13)
	je	.LBB27_12
# BB#3:                                 # %.preheader
                                        #   in Loop: Header=BB27_2 Depth=1
	cmpl	%edx, %r12d
	jle	.LBB27_11
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB27_2 Depth=1
	movslq	%r11d, %r14
	xorl	%esi, %esi
	movq	-8(%rsp), %rax          # 8-byte Reload
	movq	%r15, %r10
	.p2align	4, 0x90
.LBB27_5:                               #   Parent Loop BB27_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%r15,8), %rbp
	cmpb	$0, (%rbp,%r10)
	je	.LBB27_10
# BB#6:                                 #   in Loop: Header=BB27_5 Depth=2
	movq	(%rcx,%r13,8), %rbp
	movsd	(%rbp,%rax), %xmm1      # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB27_8
	jp	.LBB27_8
# BB#7:                                 #   in Loop: Header=BB27_5 Depth=2
	xorl	%ebp, %ebp
	jmp	.LBB27_9
	.p2align	4, 0x90
.LBB27_8:                               #   in Loop: Header=BB27_5 Depth=2
	cmpq	%r10, %r13
	movq	%r10, %rbp
	cmovgeq	%r13, %rbp
	movq	%r10, %rbx
	cmovleq	%r13, %rbx
	movslq	%ebx, %rbx
	movslq	%ebp, %rbp
	leaq	(%rbp,%rbp,4), %rbp
	shlq	$4, %rbp
	addq	(%rcx,%rbx,8), %rbp
.LBB27_9:                               #   in Loop: Header=BB27_5 Depth=2
	movq	(%r8,%r14,8), %rbx
	movslq	%esi, %rsi
	movq	%rbp, (%rbx,%rsi,8)
	incl	%esi
.LBB27_10:                              #   in Loop: Header=BB27_5 Depth=2
	incq	%r10
	addq	$80, %rax
	cmpq	%r12, %r10
	jl	.LBB27_5
.LBB27_11:                              # %._crit_edge
                                        #   in Loop: Header=BB27_2 Depth=1
	incl	%r11d
.LBB27_12:                              #   in Loop: Header=BB27_2 Depth=1
	incq	%r13
	cmpq	%r12, %r13
	jl	.LBB27_2
.LBB27_13:                              # %._crit_edge58
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	msshrinklocalhom, .Lfunc_end27-msshrinklocalhom
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI28_0:
	.quad	-4616189618054758400    # double -1
	.text
	.globl	fastshrinklocalhom
	.p2align	4, 0x90
	.type	fastshrinklocalhom,@function
fastshrinklocalhom:                     # @fastshrinklocalhom
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi274:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi275:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi276:
	.cfi_def_cfa_offset 32
.Lcfi277:
	.cfi_offset %rbx, -32
.Lcfi278:
	.cfi_offset %r14, -24
.Lcfi279:
	.cfi_offset %r15, -16
	movl	(%rdi), %eax
	cmpl	$-1, %eax
	je	.LBB28_8
# BB#1:                                 # %.preheader.lr.ph
	movl	(%rsi), %r8d
	cmpl	$-1, %r8d
	je	.LBB28_6
# BB#2:                                 # %.preheader.preheader
	xorl	%r9d, %r9d
	movsd	.LCPI28_0(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB28_3:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_4 Depth 2
	movslq	%eax, %r10
	xorl	%r15d, %r15d
	movl	%r8d, %r11d
	.p2align	4, 0x90
.LBB28_4:                               #   Parent Loop BB28_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%r10,8), %r14
	movslq	%r11d, %rbx
	leaq	(%rbx,%rbx,4), %rbx
	shlq	$4, %rbx
	leaq	(%r14,%rbx), %r11
	movsd	40(%r14,%rbx), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movl	$0, %ebx
	cmovneq	%r11, %rbx
	cmovpq	%r11, %rbx
	movq	(%rcx,%r9,8), %rax
	movq	%rbx, (%rax,%r15,2)
	movl	4(%rsi,%r15), %r11d
	addq	$4, %r15
	cmpl	$-1, %r11d
	jne	.LBB28_4
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB28_3 Depth=1
	incq	%r9
	movl	4(%rdi), %eax
	addq	$4, %rdi
	cmpl	$-1, %eax
	jne	.LBB28_3
	jmp	.LBB28_8
.LBB28_6:                               # %.preheader.us.preheader
	addq	$4, %rdi
	.p2align	4, 0x90
.LBB28_7:                               # %.preheader.us
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$-1, (%rdi)
	leaq	4(%rdi), %rdi
	jne	.LBB28_7
.LBB28_8:                               # %._crit_edge37
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end28:
	.size	fastshrinklocalhom, .Lfunc_end28-fastshrinklocalhom
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI29_0:
	.quad	-4616189618054758400    # double -1
	.text
	.globl	msfastshrinklocalhom
	.p2align	4, 0x90
	.type	msfastshrinklocalhom,@function
msfastshrinklocalhom:                   # @msfastshrinklocalhom
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi280:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi281:
	.cfi_def_cfa_offset 24
.Lcfi282:
	.cfi_offset %rbx, -24
.Lcfi283:
	.cfi_offset %r14, -16
	movl	(%rdi), %r9d
	cmpl	$-1, %r9d
	je	.LBB29_8
# BB#1:                                 # %.preheader.lr.ph
	movl	(%rsi), %r8d
	cmpl	$-1, %r8d
	je	.LBB29_6
# BB#2:                                 # %.preheader.preheader
	xorl	%r10d, %r10d
	movsd	.LCPI29_0(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB29_3:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_4 Depth 2
	xorl	%r14d, %r14d
	movl	%r8d, %r11d
	.p2align	4, 0x90
.LBB29_4:                               #   Parent Loop BB29_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rsi,%r14), %rbx
	cmpl	%r11d, %r9d
	movq	%rbx, %r11
	cmovlq	%rdi, %r11
	cmovgq	%rdi, %rbx
	movslq	(%r11), %rax
	movq	(%rdx,%rax,8), %rax
	movslq	(%rbx), %rbx
	leaq	(%rbx,%rbx,4), %rbx
	shlq	$4, %rbx
	leaq	(%rax,%rbx), %r11
	movsd	40(%rax,%rbx), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movl	$0, %eax
	cmovneq	%r11, %rax
	cmovpq	%r11, %rax
	movq	(%rcx,%r10,8), %rbx
	movq	%rax, (%rbx,%r14,2)
	movl	4(%rsi,%r14), %r11d
	addq	$4, %r14
	cmpl	$-1, %r11d
	jne	.LBB29_4
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB29_3 Depth=1
	incq	%r10
	movl	4(%rdi), %r9d
	addq	$4, %rdi
	cmpl	$-1, %r9d
	jne	.LBB29_3
	jmp	.LBB29_8
.LBB29_6:                               # %.preheader.us.preheader
	addq	$4, %rdi
	.p2align	4, 0x90
.LBB29_7:                               # %.preheader.us
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$-1, (%rdi)
	leaq	4(%rdi), %rdi
	jne	.LBB29_7
.LBB29_8:                               # %._crit_edge51
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end29:
	.size	msfastshrinklocalhom, .Lfunc_end29-msfastshrinklocalhom
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"score in score_calc = %f\n"
	.size	.L.str, 26

	.type	strnbcat.b,@object      # @strnbcat.b
	.local	strnbcat.b
	.comm	strnbcat.b,5000000,16
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" %d"
	.size	.L.str.1, 4

	.type	nodeFromABranch.outergroup2,@object # @nodeFromABranch.outergroup2
	.local	nodeFromABranch.outergroup2
	.comm	nodeFromABranch.outergroup2,8,8
	.type	nodeFromABranch.table,@object # @nodeFromABranch.table
	.local	nodeFromABranch.table
	.comm	nodeFromABranch.table,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
