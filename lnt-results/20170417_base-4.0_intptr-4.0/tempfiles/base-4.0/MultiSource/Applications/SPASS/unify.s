	.text
	.file	"unify.bc"
	.globl	unify_Init
	.p2align	4, 0x90
	.type	unify_Init,@function
unify_Init:                             # @unify_Init
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	unify_Init, .Lfunc_end0-unify_Init
	.cfi_endproc

	.globl	unify_Free
	.p2align	4, 0x90
	.type	unify_Free,@function
unify_Free:                             # @unify_Free
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	unify_Free, .Lfunc_end1-unify_Free
	.cfi_endproc

	.globl	unify_OccurCheckCom
	.p2align	4, 0x90
	.type	unify_OccurCheckCom,@function
unify_OccurCheckCom:                    # @unify_OccurCheckCom
	.cfi_startproc
# BB#0:
	movl	stack_POINTER(%rip), %r8d
	movl	%r8d, %r9d
	jmp	.LBB2_1
	.p2align	4, 0x90
.LBB2_14:                               #   in Loop: Header=BB2_1 Depth=1
	movl	%ecx, %r9d
.LBB2_1:                                # %.backedge.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_2 Depth 2
	leal	-1(%r9), %ecx
	jmp	.LBB2_2
	.p2align	4, 0x90
.LBB2_15:                               #   in Loop: Header=BB2_2 Depth=2
	movl	%r9d, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rcx,8)
.LBB2_2:                                # %.backedge
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rdx), %rax
	testq	%rax, %rax
	jle	.LBB2_6
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=2
	cmpl	%edi, %eax
	je	.LBB2_4
# BB#10:                                #   in Loop: Header=BB2_2 Depth=2
	shlq	$5, %rax
	movq	8(%rsi,%rax), %rdx
	testq	%rdx, %rdx
	jne	.LBB2_2
	jmp	.LBB2_11
	.p2align	4, 0x90
.LBB2_6:                                #   in Loop: Header=BB2_2 Depth=2
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB2_7
.LBB2_11:                               #   in Loop: Header=BB2_2 Depth=2
	cmpl	%r8d, %r9d
	je	.LBB2_12
# BB#13:                                #   in Loop: Header=BB2_2 Depth=2
	movl	%ecx, stack_POINTER(%rip)
	movq	stack_STACK(,%rcx,8), %rdx
	movq	(%rdx), %rax
	movq	8(%rdx), %rdx
	testq	%rax, %rax
	jne	.LBB2_15
	jmp	.LBB2_14
.LBB2_7:                                #   in Loop: Header=BB2_1 Depth=1
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.LBB2_9
# BB#8:                                 #   in Loop: Header=BB2_1 Depth=1
	movl	%r9d, %r10d
	leal	1(%r9), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%r10,8)
	movl	%ecx, %r9d
.LBB2_9:                                #   in Loop: Header=BB2_1 Depth=1
	movq	8(%rdx), %rdx
	jmp	.LBB2_1
.LBB2_12:
	xorl	%eax, %eax
	retq
.LBB2_4:
	movl	%r8d, stack_POINTER(%rip)
	movl	$1, %eax
	retq
.Lfunc_end2:
	.size	unify_OccurCheckCom, .Lfunc_end2-unify_OccurCheckCom
	.cfi_endproc

	.globl	unify_OccurCheck
	.p2align	4, 0x90
	.type	unify_OccurCheck,@function
unify_OccurCheck:                       # @unify_OccurCheck
	.cfi_startproc
# BB#0:
	movl	stack_POINTER(%rip), %r8d
	movl	%r8d, %r10d
	jmp	.LBB3_1
	.p2align	4, 0x90
.LBB3_13:                               #   in Loop: Header=BB3_1 Depth=1
	movq	8(%rcx), %rcx
.LBB3_1:                                # %.outer
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.LBB3_2
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB3_9
# BB#11:                                #   in Loop: Header=BB3_1 Depth=1
	cmpq	$0, (%rcx)
	je	.LBB3_13
# BB#12:                                #   in Loop: Header=BB3_1 Depth=1
	leal	1(%r10), %r9d
	movl	%r9d, stack_POINTER(%rip)
	movl	%r10d, %eax
	movq	%rdx, stack_STACK(,%rax,8)
	movq	(%rcx), %rax
	addl	$2, %r10d
	movl	%r10d, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%r9,8)
	jmp	.LBB3_13
.LBB3_2:                                # %._crit_edge
                                        #   in Loop: Header=BB3_1 Depth=1
	cmpq	%rdi, %rdx
	jne	.LBB3_6
# BB#3:                                 # %._crit_edge
                                        #   in Loop: Header=BB3_1 Depth=1
	cmpl	%esi, %eax
	je	.LBB3_4
.LBB3_6:                                #   in Loop: Header=BB3_1 Depth=1
	cltq
	shlq	$5, %rax
	movq	8(%rdx,%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB3_9
# BB#7:                                 #   in Loop: Header=BB3_1 Depth=1
	movq	16(%rdx,%rax), %rdx
	jmp	.LBB3_1
.LBB3_9:                                #   in Loop: Header=BB3_1 Depth=1
	movl	%r10d, %r9d
	cmpl	%r8d, %r9d
	je	.LBB3_10
# BB#14:                                #   in Loop: Header=BB3_1 Depth=1
	leal	-1(%r9), %eax
	movl	%eax, stack_POINTER(%rip)
	movq	stack_STACK(,%rax,8), %rcx
	movq	(%rcx), %r11
	movq	8(%rcx), %rcx
	leal	-2(%r9), %r10d
	movq	stack_STACK(,%r10,8), %rdx
	testq	%r11, %r11
	je	.LBB3_15
# BB#16:                                #   in Loop: Header=BB3_1 Depth=1
	movl	%r9d, stack_POINTER(%rip)
	movq	%r11, stack_STACK(,%rax,8)
	movl	%r9d, %r10d
	jmp	.LBB3_1
.LBB3_15:                               #   in Loop: Header=BB3_1 Depth=1
	movl	%r10d, stack_POINTER(%rip)
	jmp	.LBB3_1
.LBB3_4:
	movl	%r8d, stack_POINTER(%rip)
	movl	$1, %eax
	retq
.LBB3_10:
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	unify_OccurCheck, .Lfunc_end3-unify_OccurCheck
	.cfi_endproc

	.globl	unify_Unify
	.p2align	4, 0x90
	.type	unify_Unify,@function
unify_Unify:                            # @unify_Unify
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	xorl	%r9d, %r9d
	movl	stack_POINTER(%rip), %r8d
	movl	%r8d, %eax
	jmp	.LBB4_1
	.p2align	4, 0x90
.LBB4_12:                               #   in Loop: Header=BB4_1 Depth=1
	testl	%ebx, %ebx
	jle	.LBB4_19
# BB#13:                                #   in Loop: Header=BB4_1 Depth=1
	cmpq	%rdx, %rdi
	jne	.LBB4_15
# BB#14:                                #   in Loop: Header=BB4_1 Depth=1
	cmpl	%ebx, %r12d
	je	.LBB4_17
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_37:                               #   in Loop: Header=BB4_1 Depth=1
	testl	%r9d, %r9d
	je	.LBB4_15
# BB#38:                                # %.outer.i.preheader
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	%r10d, %r12d
	movq	%rsi, %rcx
	movq	%rdi, %r9
	jmp	.LBB4_39
	.p2align	4, 0x90
.LBB4_50:                               #   in Loop: Header=BB4_39 Depth=2
	movq	8(%rcx), %rcx
.LBB4_39:                               # %.outer.i
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %ebp
	testl	%ebp, %ebp
	jg	.LBB4_40
# BB#44:                                # %.lr.ph.i
                                        #   in Loop: Header=BB4_39 Depth=2
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB4_45
# BB#48:                                #   in Loop: Header=BB4_39 Depth=2
	cmpq	$0, (%rcx)
	je	.LBB4_50
# BB#49:                                #   in Loop: Header=BB4_39 Depth=2
	leal	1(%r12), %ebp
	movl	%ebp, stack_POINTER(%rip)
	movl	%r12d, %eax
	movq	%r9, stack_STACK(,%rax,8)
	movq	(%rcx), %rax
	addl	$2, %r12d
	movl	%r12d, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rbp,8)
	jmp	.LBB4_50
.LBB4_40:                               # %._crit_edge.i
                                        #   in Loop: Header=BB4_39 Depth=2
	cmpq	%rdx, %r9
	jne	.LBB4_42
# BB#41:                                # %._crit_edge.i
                                        #   in Loop: Header=BB4_39 Depth=2
	cmpl	%ebx, %ebp
	je	.LBB4_62
.LBB4_42:                               #   in Loop: Header=BB4_39 Depth=2
	movslq	%ebp, %rbp
	shlq	$5, %rbp
	movq	8(%r9,%rbp), %rcx
	testq	%rcx, %rcx
	je	.LBB4_45
# BB#43:                                #   in Loop: Header=BB4_39 Depth=2
	movq	16(%r9,%rbp), %r9
	jmp	.LBB4_39
.LBB4_45:                               #   in Loop: Header=BB4_39 Depth=2
	movl	%r12d, %r11d
	cmpl	%r10d, %r11d
	je	.LBB4_52
# BB#46:                                #   in Loop: Header=BB4_39 Depth=2
	leal	-1(%r11), %ebp
	movl	%ebp, stack_POINTER(%rip)
	movq	stack_STACK(,%rbp,8), %rax
	movq	(%rax), %r15
	movq	8(%rax), %rcx
	leal	-2(%r11), %r12d
	movq	stack_STACK(,%r12,8), %r9
	testq	%r15, %r15
	je	.LBB4_47
# BB#51:                                #   in Loop: Header=BB4_39 Depth=2
	movl	%r11d, stack_POINTER(%rip)
	movq	%r15, stack_STACK(,%rbp,8)
	movl	%r11d, %r12d
	jmp	.LBB4_39
.LBB4_47:                               #   in Loop: Header=BB4_39 Depth=2
	movl	%r12d, stack_POINTER(%rip)
	jmp	.LBB4_39
.LBB4_19:                               #   in Loop: Header=BB4_1 Depth=1
	testl	%r9d, %r9d
	je	.LBB4_35
# BB#20:                                # %.outer.i119.preheader
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	%r10d, %esi
	movq	%rcx, %rbx
	movq	%rdx, %r9
	jmp	.LBB4_21
	.p2align	4, 0x90
.LBB4_32:                               #   in Loop: Header=BB4_21 Depth=2
	movq	8(%rbx), %rbx
.LBB4_21:                               # %.outer.i119
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %ebp
	testl	%ebp, %ebp
	jg	.LBB4_22
# BB#26:                                # %.lr.ph.i126
                                        #   in Loop: Header=BB4_21 Depth=2
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB4_27
# BB#30:                                #   in Loop: Header=BB4_21 Depth=2
	cmpq	$0, (%rbx)
	je	.LBB4_32
# BB#31:                                #   in Loop: Header=BB4_21 Depth=2
	leal	1(%rsi), %eax
	movl	%eax, stack_POINTER(%rip)
	movl	%esi, %ebp
	movq	%r9, stack_STACK(,%rbp,8)
	movq	(%rbx), %rbp
	addl	$2, %esi
	movl	%esi, stack_POINTER(%rip)
	movq	%rbp, stack_STACK(,%rax,8)
	jmp	.LBB4_32
.LBB4_22:                               # %._crit_edge.i122
                                        #   in Loop: Header=BB4_21 Depth=2
	cmpq	%rdi, %r9
	jne	.LBB4_24
# BB#23:                                # %._crit_edge.i122
                                        #   in Loop: Header=BB4_21 Depth=2
	cmpl	%r12d, %ebp
	je	.LBB4_62
.LBB4_24:                               #   in Loop: Header=BB4_21 Depth=2
	movslq	%ebp, %rax
	shlq	$5, %rax
	movq	8(%r9,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB4_27
# BB#25:                                #   in Loop: Header=BB4_21 Depth=2
	movq	16(%r9,%rax), %r9
	jmp	.LBB4_21
.LBB4_27:                               #   in Loop: Header=BB4_21 Depth=2
	movl	%esi, %r14d
	cmpl	%r10d, %r14d
	je	.LBB4_34
# BB#28:                                #   in Loop: Header=BB4_21 Depth=2
	leal	-1(%r14), %ebp
	movl	%ebp, stack_POINTER(%rip)
	movq	stack_STACK(,%rbp,8), %rax
	movq	(%rax), %r15
	movq	8(%rax), %rbx
	leal	-2(%r14), %esi
	movq	stack_STACK(,%rsi,8), %r9
	testq	%r15, %r15
	je	.LBB4_29
# BB#33:                                #   in Loop: Header=BB4_21 Depth=2
	movl	%r14d, stack_POINTER(%rip)
	movq	%r15, stack_STACK(,%rbp,8)
	movl	%r14d, %esi
	jmp	.LBB4_21
.LBB4_29:                               #   in Loop: Header=BB4_21 Depth=2
	movl	%esi, stack_POINTER(%rip)
	jmp	.LBB4_21
.LBB4_52:                               # %unify_OccurCheck.exit.thread.loopexit
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	(%r14), %ebx
.LBB4_15:                               #   in Loop: Header=BB4_1 Depth=1
	movslq	%ebx, %rax
	shlq	$5, %rax
	leaq	(%rdx,%rax), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	%rsi, 8(%rdx,%rax)
	movq	%rdi, 16(%rdx,%rax)
	movq	cont_LASTBINDING(%rip), %rsi
	movq	%rsi, 24(%rdx,%rax)
	movq	%rcx, cont_LASTBINDING(%rip)
	jmp	.LBB4_16
.LBB4_34:                               # %unify_OccurCheck.exit140.thread.loopexit
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	(%r11), %r12d
.LBB4_35:                               # %unify_OccurCheck.exit140.thread
                                        #   in Loop: Header=BB4_1 Depth=1
	movslq	%r12d, %rax
	shlq	$5, %rax
	leaq	(%rdi,%rax), %rsi
	movq	%rsi, cont_CURRENTBINDING(%rip)
	movq	%rcx, 8(%rdi,%rax)
	movq	%rdx, 16(%rdi,%rax)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 24(%rdi,%rax)
	movq	%rsi, cont_LASTBINDING(%rip)
	.p2align	4, 0x90
.LBB4_16:                               # %.loopexit154
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	cont_BINDINGS(%rip), %r9d
	incl	%r9d
	movl	%r9d, cont_BINDINGS(%rip)
.LBB4_17:                               # %.loopexit154
                                        #   in Loop: Header=BB4_1 Depth=1
	cmpl	%r8d, %r10d
	je	.LBB4_18
# BB#59:                                #   in Loop: Header=BB4_1 Depth=1
	leal	-1(%r10), %r14d
	movq	stack_STACK(,%r14,8), %r11
	leal	-2(%r10), %ebp
	movl	%ebp, stack_POINTER(%rip)
	movq	stack_STACK(,%rbp,8), %rax
	movq	8(%r11), %rcx
	movq	(%rax), %rbx
	movq	8(%rax), %rsi
	leal	-3(%r10), %eax
	movq	stack_STACK(,%rax,8), %rdx
	leal	-4(%r10), %eax
	movq	stack_STACK(,%rax,8), %rdi
	testq	%rbx, %rbx
	jne	.LBB4_61
# BB#60:                                #   in Loop: Header=BB4_1 Depth=1
	movl	%eax, stack_POINTER(%rip)
	jmp	.LBB4_1
	.p2align	4, 0x90
.LBB4_61:                               #   in Loop: Header=BB4_1 Depth=1
	movl	%r14d, stack_POINTER(%rip)
	movq	%rbx, stack_STACK(,%rbp,8)
	movq	(%r11), %rax
	movl	%r10d, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%r14,8)
	movl	%r10d, %eax
.LBB4_1:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_2 Depth 2
                                        #       Child Loop BB4_4 Depth 3
                                        #       Child Loop BB4_8 Depth 3
                                        #     Child Loop BB4_39 Depth 2
                                        #     Child Loop BB4_21 Depth 2
	movl	%eax, %r10d
	jmp	.LBB4_2
	.p2align	4, 0x90
.LBB4_9:                                #   in Loop: Header=BB4_2 Depth=2
	movq	%rbp, %rcx
.LBB4_11:                               # %.critedge1
                                        #   in Loop: Header=BB4_2 Depth=2
	testl	%r12d, %r12d
	jg	.LBB4_12
# BB#36:                                #   in Loop: Header=BB4_2 Depth=2
	testl	%ebx, %ebx
	jg	.LBB4_37
# BB#53:                                #   in Loop: Header=BB4_2 Depth=2
	cmpl	%ebx, %r12d
	jne	.LBB4_63
# BB#54:                                #   in Loop: Header=BB4_2 Depth=2
	cmpq	%rcx, %rsi
	je	.LBB4_17
# BB#55:                                #   in Loop: Header=BB4_2 Depth=2
	movq	16(%rsi), %rax
	testq	%rax, %rax
	je	.LBB4_17
# BB#56:                                #   in Loop: Header=BB4_2 Depth=2
	movq	16(%rcx), %rcx
	cmpq	$0, (%rax)
	je	.LBB4_58
# BB#57:                                #   in Loop: Header=BB4_2 Depth=2
	leal	1(%r10), %esi
	movl	%r10d, %ebp
	movq	%rdi, stack_STACK(,%rbp,8)
	leal	2(%r10), %ebp
	movl	%ebp, stack_POINTER(%rip)
	movq	%rdx, stack_STACK(,%rsi,8)
	movq	(%rax), %rsi
	leal	3(%r10), %ebx
	movl	%ebx, stack_POINTER(%rip)
	movq	%rsi, stack_STACK(,%rbp,8)
	movq	(%rcx), %rsi
	addl	$4, %r10d
	movl	%r10d, stack_POINTER(%rip)
	movq	%rsi, stack_STACK(,%rbx,8)
.LBB4_58:                               #   in Loop: Header=BB4_2 Depth=2
	movq	8(%rax), %rsi
	movq	8(%rcx), %rcx
.LBB4_2:                                #   Parent Loop BB4_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_4 Depth 3
                                        #       Child Loop BB4_8 Depth 3
	movl	(%rsi), %r12d
	movq	%rsi, %r11
	testl	%r12d, %r12d
	jle	.LBB4_6
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_2 Depth=2
	movq	%rsi, %rbx
	.p2align	4, 0x90
.LBB4_4:                                # %.lr.ph
                                        #   Parent Loop BB4_1 Depth=1
                                        #     Parent Loop BB4_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%r12d, %rbp
	shlq	$5, %rbp
	movq	8(%rdi,%rbp), %rsi
	testq	%rsi, %rsi
	je	.LBB4_5
# BB#65:                                #   in Loop: Header=BB4_4 Depth=3
	movq	16(%rdi,%rbp), %rdi
	movl	(%rsi), %r12d
	testl	%r12d, %r12d
	movq	%rsi, %r11
	movq	%rsi, %rbx
	jg	.LBB4_4
	jmp	.LBB4_6
	.p2align	4, 0x90
.LBB4_5:                                #   in Loop: Header=BB4_2 Depth=2
	movq	%rbx, %rsi
.LBB4_6:                                # %.critedge.preheader
                                        #   in Loop: Header=BB4_2 Depth=2
	movl	(%rcx), %ebx
	movq	%rcx, %r14
	testl	%ebx, %ebx
	jle	.LBB4_11
# BB#7:                                 # %.lr.ph217.preheader
                                        #   in Loop: Header=BB4_2 Depth=2
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB4_8:                                # %.lr.ph217
                                        #   Parent Loop BB4_1 Depth=1
                                        #     Parent Loop BB4_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ebx, %rax
	shlq	$5, %rax
	movq	8(%rdx,%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB4_9
# BB#10:                                # %.critedge
                                        #   in Loop: Header=BB4_8 Depth=3
	movq	16(%rdx,%rax), %rdx
	movl	(%rcx), %ebx
	testl	%ebx, %ebx
	movq	%rcx, %r14
	movq	%rcx, %rbp
	jg	.LBB4_8
	jmp	.LBB4_11
.LBB4_62:                               # %.sink.split.sink.split
	movl	%r10d, stack_POINTER(%rip)
.LBB4_63:                               # %.sink.split
	movl	%r8d, stack_POINTER(%rip)
	xorl	%eax, %eax
.LBB4_64:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_18:
	movl	$1, %eax
	jmp	.LBB4_64
.Lfunc_end4:
	.size	unify_Unify, .Lfunc_end4-unify_Unify
	.cfi_endproc

	.globl	unify_UnifyCom
	.p2align	4, 0x90
	.type	unify_UnifyCom,@function
unify_UnifyCom:                         # @unify_UnifyCom
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movl	stack_POINTER(%rip), %r8d
	movl	%r8d, %r9d
	jmp	.LBB5_1
.LBB5_23:                               #   in Loop: Header=BB5_1 Depth=1
	movq	%rax, %rcx
.LBB5_24:                               # %.sink.split.sink.split
                                        #   in Loop: Header=BB5_1 Depth=1
	movl	(%rcx), %ecx
	movl	%r11d, %r9d
.LBB5_25:                               # %.sink.split
                                        #   in Loop: Header=BB5_1 Depth=1
	movslq	%ecx, %rax
	shlq	$5, %rax
	leaq	(%rdi,%rax), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	%r10, 8(%rdi,%rax)
	movq	%rdi, 16(%rdi,%rax)
	movq	cont_LASTBINDING(%rip), %rdx
	movq	%rdx, 24(%rdi,%rax)
	movq	%rcx, cont_LASTBINDING(%rip)
	incl	cont_BINDINGS(%rip)
.LBB5_26:                               #   in Loop: Header=BB5_1 Depth=1
	movl	%r9d, %eax
	cmpl	%r8d, %eax
	je	.LBB5_27
# BB#54:                                #   in Loop: Header=BB5_1 Depth=1
	leal	-1(%rax), %ecx
	movq	stack_STACK(,%rcx,8), %r10
	leal	-2(%rax), %r9d
	movl	%r9d, stack_POINTER(%rip)
	movq	stack_STACK(,%r9,8), %rsi
	movq	8(%r10), %rdx
	movq	(%rsi), %rbx
	movq	8(%rsi), %rsi
	testq	%rbx, %rbx
	je	.LBB5_1
# BB#55:                                #   in Loop: Header=BB5_1 Depth=1
	movl	%ecx, stack_POINTER(%rip)
	movq	%rbx, stack_STACK(,%r9,8)
	movq	(%r10), %rbx
	movl	%eax, stack_POINTER(%rip)
	movq	%rbx, stack_STACK(,%rcx,8)
	movl	%eax, %r9d
	jmp	.LBB5_1
.LBB5_10:                               #   in Loop: Header=BB5_1 Depth=1
	movq	8(%rax), %rsi
	movq	8(%rcx), %rdx
	.p2align	4, 0x90
.LBB5_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_3 Depth 2
                                        #     Child Loop BB5_44 Depth 2
                                        #     Child Loop BB5_38 Depth 2
                                        #       Child Loop BB5_39 Depth 3
                                        #     Child Loop BB5_21 Depth 2
                                        #     Child Loop BB5_15 Depth 2
                                        #       Child Loop BB5_16 Depth 3
	movq	%rsi, %rax
	movslq	(%rax), %rcx
	testq	%rcx, %rcx
	jle	.LBB5_3
# BB#2:                                 #   in Loop: Header=BB5_1 Depth=1
	movq	%rcx, %rsi
	shlq	$5, %rsi
	movq	8(%rdi,%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB5_1
	.p2align	4, 0x90
.LBB5_3:                                # %.critedge
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %r10
	movslq	(%r10), %rsi
	testq	%rsi, %rsi
	jle	.LBB5_5
# BB#4:                                 #   in Loop: Header=BB5_3 Depth=2
	movq	%rsi, %rdx
	shlq	$5, %rdx
	movq	8(%rdi,%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB5_3
.LBB5_5:                                # %.critedge1
                                        #   in Loop: Header=BB5_1 Depth=1
	cmpl	%esi, %ecx
	jne	.LBB5_11
# BB#6:                                 #   in Loop: Header=BB5_1 Depth=1
	cmpq	%r10, %rax
	je	.LBB5_26
# BB#7:                                 #   in Loop: Header=BB5_1 Depth=1
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB5_26
# BB#8:                                 #   in Loop: Header=BB5_1 Depth=1
	movq	16(%r10), %rcx
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB5_10
# BB#9:                                 #   in Loop: Header=BB5_1 Depth=1
	leal	1(%r9), %esi
	movl	%esi, stack_POINTER(%rip)
	movl	%r9d, %ebx
	movq	%rdx, stack_STACK(,%rbx,8)
	movq	(%rcx), %rdx
	addl	$2, %r9d
	movl	%r9d, stack_POINTER(%rip)
	movq	%rdx, stack_STACK(,%rsi,8)
	jmp	.LBB5_10
.LBB5_11:                               #   in Loop: Header=BB5_1 Depth=1
	testl	%ecx, %ecx
	jle	.LBB5_35
# BB#12:                                #   in Loop: Header=BB5_1 Depth=1
	testl	%esi, %esi
	jg	.LBB5_25
# BB#13:                                # %.backedge.outer.i85.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movl	%r9d, %r11d
	movl	%r9d, %r14d
	movq	%r10, %rdx
	cmpl	%r9d, %r14d
	je	.LBB5_21
	jmp	.LBB5_15
.LBB5_35:                               #   in Loop: Header=BB5_1 Depth=1
	testl	%esi, %esi
	jle	.LBB5_57
# BB#36:                                # %.backedge.outer.i.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movl	%r9d, %r11d
	movl	%r9d, %r14d
	movq	%rax, %rdx
	cmpl	%r9d, %r14d
	je	.LBB5_44
	jmp	.LBB5_38
.LBB5_45:                               #   in Loop: Header=BB5_1 Depth=1
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB5_46
.LBB5_50:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB5_52
# BB#51:                                #   in Loop: Header=BB5_1 Depth=1
	leal	1(%r14), %r11d
	movl	%r11d, stack_POINTER(%rip)
	movl	%r14d, %ebx
	movq	%rcx, stack_STACK(,%rbx,8)
	movl	%r11d, %r14d
	jmp	.LBB5_52
	.p2align	4, 0x90
.LBB5_38:                               #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_39 Depth 3
	leal	-1(%r14), %ebx
	jmp	.LBB5_39
.LBB5_53:                               #   in Loop: Header=BB5_39 Depth=3
	movl	%r14d, stack_POINTER(%rip)
	movq	%rcx, stack_STACK(,%rbx,8)
	movl	%r14d, %r11d
	.p2align	4, 0x90
.LBB5_39:                               # %.backedge.i
                                        #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	(%rdx), %rcx
	testq	%rcx, %rcx
	jle	.LBB5_49
# BB#40:                                #   in Loop: Header=BB5_39 Depth=3
	cmpl	%esi, %ecx
	je	.LBB5_56
# BB#41:                                #   in Loop: Header=BB5_39 Depth=3
	shlq	$5, %rcx
	movq	8(%rdi,%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB5_39
	jmp	.LBB5_42
.LBB5_49:                               #   in Loop: Header=BB5_39 Depth=3
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB5_50
.LBB5_42:                               # %.loopexit154
                                        #   in Loop: Header=BB5_39 Depth=3
	movl	%ebx, stack_POINTER(%rip)
	movq	stack_STACK(,%rbx,8), %rdx
	movq	(%rdx), %rcx
	movq	8(%rdx), %rdx
	testq	%rcx, %rcx
	jne	.LBB5_53
# BB#43:                                #   in Loop: Header=BB5_38 Depth=2
	movl	%ebx, %r11d
	movl	%ebx, %r14d
	cmpl	%r9d, %r14d
	je	.LBB5_44
	jmp	.LBB5_38
.LBB5_52:                               #   in Loop: Header=BB5_1 Depth=1
	movq	8(%rdx), %rdx
	cmpl	%r9d, %r14d
	jne	.LBB5_38
	.p2align	4, 0x90
.LBB5_44:                               # %.backedge.i.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rdx), %rcx
	testq	%rcx, %rcx
	jle	.LBB5_45
# BB#47:                                #   in Loop: Header=BB5_44 Depth=2
	cmpl	%esi, %ecx
	je	.LBB5_56
# BB#48:                                #   in Loop: Header=BB5_44 Depth=2
	shlq	$5, %rcx
	movq	8(%rdi,%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB5_44
.LBB5_46:                               #   in Loop: Header=BB5_1 Depth=1
	movq	%r10, %rcx
	movq	%rax, %r10
	jmp	.LBB5_24
.LBB5_22:                               #   in Loop: Header=BB5_1 Depth=1
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB5_23
.LBB5_31:                               # %.us-lcssa127.us
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	je	.LBB5_33
# BB#32:                                #   in Loop: Header=BB5_1 Depth=1
	leal	1(%r14), %r11d
	movl	%r11d, stack_POINTER(%rip)
	movl	%r14d, %ebx
	movq	%rsi, stack_STACK(,%rbx,8)
	movl	%r11d, %r14d
	jmp	.LBB5_33
	.p2align	4, 0x90
.LBB5_15:                               #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_16 Depth 3
	leal	-1(%r14), %ebx
	jmp	.LBB5_16
.LBB5_34:                               #   in Loop: Header=BB5_16 Depth=3
	movl	%r14d, stack_POINTER(%rip)
	movq	%rsi, stack_STACK(,%rbx,8)
	movl	%r14d, %r11d
	.p2align	4, 0x90
.LBB5_16:                               # %.backedge.i89
                                        #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	(%rdx), %rsi
	testq	%rsi, %rsi
	jle	.LBB5_30
# BB#17:                                #   in Loop: Header=BB5_16 Depth=3
	cmpl	%ecx, %esi
	je	.LBB5_56
# BB#18:                                #   in Loop: Header=BB5_16 Depth=3
	shlq	$5, %rsi
	movq	8(%rdi,%rsi), %rdx
	testq	%rdx, %rdx
	jne	.LBB5_16
	jmp	.LBB5_19
.LBB5_30:                               #   in Loop: Header=BB5_16 Depth=3
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB5_31
.LBB5_19:                               # %.loopexit149
                                        #   in Loop: Header=BB5_16 Depth=3
	movl	%ebx, stack_POINTER(%rip)
	movq	stack_STACK(,%rbx,8), %rdx
	movq	(%rdx), %rsi
	movq	8(%rdx), %rdx
	testq	%rsi, %rsi
	jne	.LBB5_34
# BB#20:                                #   in Loop: Header=BB5_15 Depth=2
	movl	%ebx, %r11d
	movl	%ebx, %r14d
	cmpl	%r9d, %r14d
	je	.LBB5_21
	jmp	.LBB5_15
.LBB5_33:                               #   in Loop: Header=BB5_1 Depth=1
	movq	8(%rdx), %rdx
	cmpl	%r9d, %r14d
	jne	.LBB5_15
	.p2align	4, 0x90
.LBB5_21:                               # %.backedge.i89.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rdx), %rsi
	testq	%rsi, %rsi
	jle	.LBB5_22
# BB#28:                                #   in Loop: Header=BB5_21 Depth=2
	cmpl	%ecx, %esi
	je	.LBB5_56
# BB#29:                                #   in Loop: Header=BB5_21 Depth=2
	shlq	$5, %rsi
	movq	8(%rdi,%rsi), %rdx
	testq	%rdx, %rdx
	jne	.LBB5_21
	jmp	.LBB5_23
.LBB5_56:                               # %.sink.split109.sink.split
	movl	%r9d, stack_POINTER(%rip)
.LBB5_57:                               # %.sink.split109
	movl	%r8d, stack_POINTER(%rip)
	xorl	%eax, %eax
.LBB5_58:                               # %.loopexit
	popq	%rbx
	popq	%r14
	retq
.LBB5_27:
	movl	$1, %eax
	jmp	.LBB5_58
.Lfunc_end5:
	.size	unify_UnifyCom, .Lfunc_end5-unify_UnifyCom
	.cfi_endproc

	.globl	unify_UnifyNoOC
	.p2align	4, 0x90
	.type	unify_UnifyNoOC,@function
unify_UnifyNoOC:                        # @unify_UnifyNoOC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 48
.Lcfi19:
	.cfi_offset %rbx, -48
.Lcfi20:
	.cfi_offset %r12, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movl	stack_POINTER(%rip), %r8d
	movl	%r8d, %r9d
	jmp	.LBB6_1
.LBB6_18:                               #   in Loop: Header=BB6_1 Depth=1
	movq	16(%rax), %rax
	cmpq	$0, (%rcx)
	je	.LBB6_20
# BB#19:                                #   in Loop: Header=BB6_1 Depth=1
	leal	1(%r9), %esi
	movl	%r9d, %ebp
	movq	%rdi, stack_STACK(,%rbp,8)
	leal	2(%r9), %ebp
	movl	%ebp, stack_POINTER(%rip)
	movq	%rdx, stack_STACK(,%rsi,8)
	movq	(%rcx), %rsi
	leal	3(%r9), %ebx
	movl	%ebx, stack_POINTER(%rip)
	movq	%rsi, stack_STACK(,%rbp,8)
	movq	(%rax), %rsi
	addl	$4, %r9d
	movl	%r9d, stack_POINTER(%rip)
	movq	%rsi, stack_STACK(,%rbx,8)
.LBB6_20:                               #   in Loop: Header=BB6_1 Depth=1
	movq	8(%rcx), %rsi
	movq	8(%rax), %rcx
	jmp	.LBB6_1
	.p2align	4, 0x90
.LBB6_26:                               #   in Loop: Header=BB6_1 Depth=1
	movl	%r14d, stack_POINTER(%rip)
	movl	%r14d, %r9d
.LBB6_1:                                # %.backedge.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_2 Depth 2
                                        #       Child Loop BB6_3 Depth 3
                                        #       Child Loop BB6_6 Depth 3
	leal	-1(%r9), %r10d
	leal	-2(%r9), %r15d
	leal	-3(%r9), %r11d
	leal	-4(%r9), %r14d
	jmp	.LBB6_2
	.p2align	4, 0x90
.LBB6_7:                                #   in Loop: Header=BB6_2 Depth=2
	movq	%rcx, %rax
.LBB6_9:                                # %.critedge1
                                        #   in Loop: Header=BB6_2 Depth=2
	testl	%r12d, %r12d
	jle	.LBB6_14
# BB#10:                                #   in Loop: Header=BB6_2 Depth=2
	testl	%esi, %esi
	jle	.LBB6_21
# BB#11:                                #   in Loop: Header=BB6_2 Depth=2
	cmpq	%rdx, %rdi
	jne	.LBB6_13
# BB#12:                                #   in Loop: Header=BB6_2 Depth=2
	cmpl	%esi, %r12d
	je	.LBB6_23
	jmp	.LBB6_13
	.p2align	4, 0x90
.LBB6_14:                               #   in Loop: Header=BB6_2 Depth=2
	testl	%esi, %esi
	jle	.LBB6_15
.LBB6_13:                               #   in Loop: Header=BB6_2 Depth=2
	movslq	%esi, %rax
	shlq	$5, %rax
	leaq	(%rdx,%rax), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	%rbx, 8(%rdx,%rax)
	movq	%rdi, 16(%rdx,%rax)
	movq	cont_LASTBINDING(%rip), %rsi
	movq	%rsi, 24(%rdx,%rax)
	movq	%rcx, cont_LASTBINDING(%rip)
	jmp	.LBB6_22
	.p2align	4, 0x90
.LBB6_21:                               #   in Loop: Header=BB6_2 Depth=2
	movslq	%r12d, %rcx
	shlq	$5, %rcx
	leaq	(%rdi,%rcx), %rsi
	movq	%rsi, cont_CURRENTBINDING(%rip)
	movq	%rax, 8(%rdi,%rcx)
	movq	%rdx, 16(%rdi,%rcx)
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, 24(%rdi,%rcx)
	movq	%rsi, cont_LASTBINDING(%rip)
.LBB6_22:                               #   in Loop: Header=BB6_2 Depth=2
	incl	cont_BINDINGS(%rip)
.LBB6_23:                               #   in Loop: Header=BB6_2 Depth=2
	cmpl	%r8d, %r9d
	je	.LBB6_24
# BB#25:                                #   in Loop: Header=BB6_2 Depth=2
	movq	stack_STACK(,%r10,8), %rax
	movl	%r15d, stack_POINTER(%rip)
	movq	stack_STACK(,%r15,8), %rdx
	movq	8(%rax), %rcx
	movq	(%rdx), %rbx
	movq	8(%rdx), %rsi
	movq	stack_STACK(,%r11,8), %rdx
	movq	stack_STACK(,%r14,8), %rdi
	testq	%rbx, %rbx
	jne	.LBB6_27
	jmp	.LBB6_26
.LBB6_15:                               #   in Loop: Header=BB6_2 Depth=2
	cmpl	%esi, %r12d
	jne	.LBB6_28
# BB#16:                                #   in Loop: Header=BB6_2 Depth=2
	cmpq	%rax, %rbx
	je	.LBB6_23
# BB#17:                                #   in Loop: Header=BB6_2 Depth=2
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB6_23
	jmp	.LBB6_18
	.p2align	4, 0x90
.LBB6_27:                               #   in Loop: Header=BB6_2 Depth=2
	movl	%r10d, stack_POINTER(%rip)
	movq	%rbx, stack_STACK(,%r15,8)
	movq	(%rax), %rax
	movl	%r9d, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%r10,8)
.LBB6_2:                                # %.backedge
                                        #   Parent Loop BB6_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_3 Depth 3
                                        #       Child Loop BB6_6 Depth 3
	movl	(%rsi), %r12d
	testl	%r12d, %r12d
	jle	.LBB6_4
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph
                                        #   Parent Loop BB6_1 Depth=1
                                        #     Parent Loop BB6_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%r12d, %rax
	shlq	$5, %rax
	movq	8(%rdi,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB6_4
# BB#30:                                #   in Loop: Header=BB6_3 Depth=3
	movq	16(%rdi,%rax), %rdi
	movl	(%rbx), %r12d
	testl	%r12d, %r12d
	movq	%rbx, %rsi
	jg	.LBB6_3
	jmp	.LBB6_5
	.p2align	4, 0x90
.LBB6_4:                                #   in Loop: Header=BB6_2 Depth=2
	movq	%rsi, %rbx
.LBB6_5:                                # %.critedge.preheader
                                        #   in Loop: Header=BB6_2 Depth=2
	movl	(%rcx), %esi
	testl	%esi, %esi
	movq	%rcx, %rax
	jle	.LBB6_9
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph132
                                        #   Parent Loop BB6_1 Depth=1
                                        #     Parent Loop BB6_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%esi, %rbp
	shlq	$5, %rbp
	movq	8(%rdx,%rbp), %rax
	testq	%rax, %rax
	je	.LBB6_7
# BB#8:                                 # %.critedge
                                        #   in Loop: Header=BB6_6 Depth=3
	movq	16(%rdx,%rbp), %rdx
	movl	(%rax), %esi
	testl	%esi, %esi
	movq	%rax, %rcx
	jg	.LBB6_6
	jmp	.LBB6_9
.LBB6_24:
	movl	$1, %eax
.LBB6_29:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_28:
	movl	%r8d, stack_POINTER(%rip)
	xorl	%eax, %eax
	jmp	.LBB6_29
.Lfunc_end6:
	.size	unify_UnifyNoOC, .Lfunc_end6-unify_UnifyNoOC
	.cfi_endproc

	.globl	unify_UnifyAllOC
	.p2align	4, 0x90
	.type	unify_UnifyAllOC,@function
unify_UnifyAllOC:                       # @unify_UnifyAllOC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 64
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.LBB7_3
	.p2align	4, 0x90
.LBB7_1:                                # %.lr.ph179
                                        # =>This Inner Loop Header: Depth=1
	cltq
	shlq	$5, %rax
	movq	8(%r13,%rax), %r9
	testq	%r9, %r9
	je	.LBB7_3
# BB#2:                                 # %.thread
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	16(%r13,%rax), %r13
	movl	(%r9), %eax
	testl	%eax, %eax
	movq	%r9, %rdx
	jg	.LBB7_1
	jmp	.LBB7_4
.LBB7_3:
	movq	%rdx, %r9
.LBB7_4:                                # %.preheader
	movl	(%r8), %ecx
	testl	%ecx, %ecx
	jle	.LBB7_8
	.p2align	4, 0x90
.LBB7_5:                                # %.lr.ph167
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ecx, %rax
	shlq	$5, %rax
	movq	8(%r14,%rax), %r15
	testq	%r15, %r15
	je	.LBB7_8
# BB#6:                                 # %.thread147
                                        #   in Loop: Header=BB7_5 Depth=1
	movq	16(%r14,%rax), %r14
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	movq	%r15, %r8
	jg	.LBB7_5
	jmp	.LBB7_9
.LBB7_8:
	movq	%r8, %r15
.LBB7_9:                                # %._crit_edge
	movslq	(%r9), %rdx
	testq	%rdx, %rdx
	jle	.LBB7_17
# BB#10:
	testl	%ecx, %ecx
	jle	.LBB7_32
# BB#11:
	movl	$1, %r12d
	cmpq	%r14, %r13
	jne	.LBB7_13
# BB#12:
	cmpl	%ecx, %edx
	je	.LBB7_56
.LBB7_13:
	leal	-2001(%rdx), %eax
	cmpl	$999, %eax              # imm = 0x3E7
	jbe	.LBB7_16
# BB#14:
	cmpq	%rbx, %r14
	je	.LBB7_57
# BB#15:
	leal	-2001(%rcx), %eax
	cmpl	$999, %eax              # imm = 0x3E7
	jbe	.LBB7_57
.LBB7_16:
	shlq	$5, %rdx
	leaq	(%r13,%rdx), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	%r15, 8(%r13,%rdx)
	movq	%r14, 16(%r13,%rdx)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 24(%r13,%rdx)
	movq	%rax, cont_LASTBINDING(%rip)
	incl	cont_BINDINGS(%rip)
	jmp	.LBB7_56
.LBB7_17:
	testl	%ecx, %ecx
	jle	.LBB7_47
# BB#18:
	movl	stack_POINTER(%rip), %r8d
	movl	%r8d, %edi
	movq	%r9, %rax
	movq	%r13, %rbx
	testl	%edx, %edx
	jle	.LBB7_20
	jmp	.LBB7_23
.LBB7_25:
	movslq	%edx, %rdx
	shlq	$5, %rdx
	movq	8(%rbx,%rdx), %rax
	testq	%rax, %rax
	je	.LBB7_28
# BB#26:
	movq	16(%rbx,%rdx), %rbx
	jmp	.LBB7_27
.LBB7_28:                               # %.loopexit41.i
	cmpl	%r8d, %edi
	je	.LBB7_52
# BB#29:
	leal	-1(%rdi), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	stack_STACK(,%rdx,8), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	leal	-2(%rdi), %ebp
	movq	stack_STACK(,%rbp,8), %rbx
	testq	%rsi, %rsi
	je	.LBB7_31
# BB#30:
	movl	%edi, stack_POINTER(%rip)
	movq	%rsi, stack_STACK(,%rdx,8)
	jmp	.LBB7_27
.LBB7_31:
	movl	%ebp, stack_POINTER(%rip)
	movl	(%rax), %edx
	movl	%ebp, %edi
	testl	%edx, %edx
	jg	.LBB7_23
	.p2align	4, 0x90
.LBB7_20:                               # %.lr.ph.i
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB7_28
# BB#21:
	cmpq	$0, (%rax)
	je	.LBB7_19
# BB#22:
	leal	1(%rdi), %edx
	movl	%edx, stack_POINTER(%rip)
	movl	%edi, %esi
	movq	%rbx, stack_STACK(,%rsi,8)
	movq	(%rax), %rsi
	addl	$2, %edi
	movl	%edi, stack_POINTER(%rip)
	movq	%rsi, stack_STACK(,%rdx,8)
.LBB7_19:
	movq	8(%rax), %rax
.LBB7_27:
	movl	(%rax), %edx
	testl	%edx, %edx
	jle	.LBB7_20
.LBB7_23:                               # %._crit_edge.i
	cmpq	%r14, %rbx
	jne	.LBB7_25
# BB#24:                                # %._crit_edge.i
	cmpl	%ecx, %edx
	jne	.LBB7_25
	jmp	.LBB7_46
.LBB7_32:
	movl	stack_POINTER(%rip), %r8d
	movl	%r8d, %edi
	movq	%r15, %rbp
	movq	%r14, %rsi
	testl	%ecx, %ecx
	jle	.LBB7_34
	jmp	.LBB7_37
.LBB7_39:
	movslq	%ecx, %rax
	shlq	$5, %rax
	movq	8(%rsi,%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB7_42
# BB#40:
	movq	16(%rsi,%rax), %rsi
	jmp	.LBB7_41
.LBB7_42:                               # %.loopexit41.i139
	cmpl	%r8d, %edi
	je	.LBB7_53
# BB#43:
	leal	-1(%rdi), %eax
	movl	%eax, stack_POINTER(%rip)
	movq	stack_STACK(,%rax,8), %rsi
	movq	(%rsi), %rcx
	movq	8(%rsi), %rbp
	leal	-2(%rdi), %ebx
	movq	stack_STACK(,%rbx,8), %rsi
	testq	%rcx, %rcx
	je	.LBB7_45
# BB#44:
	movl	%edi, stack_POINTER(%rip)
	movq	%rcx, stack_STACK(,%rax,8)
	jmp	.LBB7_41
.LBB7_45:
	movl	%ebx, stack_POINTER(%rip)
	movl	(%rbp), %ecx
	movl	%ebx, %edi
	testl	%ecx, %ecx
	jg	.LBB7_37
	.p2align	4, 0x90
.LBB7_34:                               # %.lr.ph.i131
	movq	16(%rbp), %rax
	testq	%rax, %rax
	je	.LBB7_42
# BB#35:
	cmpq	$0, (%rax)
	je	.LBB7_33
# BB#36:
	leal	1(%rdi), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movl	%edi, %ebp
	movq	%rsi, stack_STACK(,%rbp,8)
	movq	(%rax), %rbp
	addl	$2, %edi
	movl	%edi, stack_POINTER(%rip)
	movq	%rbp, stack_STACK(,%rcx,8)
.LBB7_33:
	movq	8(%rax), %rbp
.LBB7_41:
	movl	(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.LBB7_34
.LBB7_37:                               # %._crit_edge.i127
	cmpq	%r13, %rsi
	jne	.LBB7_39
# BB#38:                                # %._crit_edge.i127
	cmpl	%edx, %ecx
	jne	.LBB7_39
.LBB7_46:                               # %unify_OccurCheck.exit145
	movl	%r8d, stack_POINTER(%rip)
	xorl	%r12d, %r12d
	jmp	.LBB7_56
.LBB7_47:
	xorl	%r12d, %r12d
	cmpl	%ecx, %edx
	jne	.LBB7_56
# BB#48:
	movq	16(%r9), %rbp
	testq	%rbp, %rbp
	je	.LBB7_55
# BB#49:                                # %.lr.ph
	addq	$16, %r15
	.p2align	4, 0x90
.LBB7_50:                               # =>This Inner Loop Header: Depth=1
	movq	(%r15), %r15
	movq	8(%rbp), %rdx
	movq	8(%r15), %r8
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%r14, %rcx
	callq	unify_UnifyAllOC
	testl	%eax, %eax
	je	.LBB7_56
# BB#51:                                #   in Loop: Header=BB7_50 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB7_50
	jmp	.LBB7_55
.LBB7_52:
	movslq	(%r15), %rax
	shlq	$5, %rax
	leaq	(%r14,%rax), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	%r9, 8(%r14,%rax)
	movq	%r13, 16(%r14,%rax)
	movq	cont_LASTBINDING(%rip), %rdx
	movq	%rdx, 24(%r14,%rax)
	jmp	.LBB7_54
.LBB7_53:
	movslq	(%r9), %rax
	shlq	$5, %rax
	leaq	(%r13,%rax), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	%r15, 8(%r13,%rax)
	movq	%r14, 16(%r13,%rax)
	movq	cont_LASTBINDING(%rip), %rdx
	movq	%rdx, 24(%r13,%rax)
.LBB7_54:                               # %.critedge
	movq	%rcx, cont_LASTBINDING(%rip)
	incl	cont_BINDINGS(%rip)
.LBB7_55:
	movl	$1, %r12d
.LBB7_56:                               # %.critedge
	movl	%r12d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_57:
	movslq	%ecx, %rax
	shlq	$5, %rax
	leaq	(%r14,%rax), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	%r9, 8(%r14,%rax)
	movq	%r13, 16(%r14,%rax)
	movq	cont_LASTBINDING(%rip), %rdx
	movq	%rdx, 24(%r14,%rax)
	movq	%rcx, cont_LASTBINDING(%rip)
	incl	cont_BINDINGS(%rip)
	jmp	.LBB7_56
.Lfunc_end7:
	.size	unify_UnifyAllOC, .Lfunc_end7-unify_UnifyAllOC
	.cfi_endproc

	.globl	unify_Match
	.p2align	4, 0x90
	.type	unify_Match,@function
unify_Match:                            # @unify_Match
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 48
.Lcfi42:
	.cfi_offset %rbx, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movslq	(%rsi), %rax
	testq	%rax, %rax
	jle	.LBB8_3
# BB#1:
	shlq	$5, %rax
	movq	8(%r14,%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB8_2
# BB#10:
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	term_Equal              # TAILCALL
.LBB8_3:
	xorl	%r15d, %r15d
	cmpl	(%rbx), %eax
	jne	.LBB8_9
# BB#4:
	movq	16(%rsi), %rbp
	testq	%rbp, %rbp
	je	.LBB8_8
# BB#5:                                 # %.lr.ph
	addq	$16, %rbx
	.p2align	4, 0x90
.LBB8_6:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movq	8(%rbp), %rsi
	movq	8(%rbx), %rdx
	movq	%r14, %rdi
	callq	unify_Match
	testl	%eax, %eax
	je	.LBB8_9
# BB#7:                                 #   in Loop: Header=BB8_6 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB8_6
	jmp	.LBB8_8
.LBB8_2:
	leaq	8(%r14,%rax), %rcx
	movq	cont_INSTANCECONTEXT(%rip), %rdx
	leaq	(%r14,%rax), %rsi
	movq	%rsi, cont_CURRENTBINDING(%rip)
	movq	%rbx, (%rcx)
	movq	%rdx, 16(%r14,%rax)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 24(%r14,%rax)
	movq	%rsi, cont_LASTBINDING(%rip)
	incl	cont_BINDINGS(%rip)
.LBB8_8:
	movl	$1, %r15d
.LBB8_9:                                # %.critedge
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	unify_Match, .Lfunc_end8-unify_Match
	.cfi_endproc

	.globl	unify_MatchFlexible
	.p2align	4, 0x90
	.type	unify_MatchFlexible,@function
unify_MatchFlexible:                    # @unify_MatchFlexible
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 48
.Lcfi51:
	.cfi_offset %rbx, -48
.Lcfi52:
	.cfi_offset %r12, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movslq	(%rbp), %rax
	testq	%rax, %rax
	jle	.LBB9_3
# BB#1:
	shlq	$5, %rax
	movq	8(%r14,%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB9_2
# BB#11:
	movq	%rbx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	term_Equal              # TAILCALL
.LBB9_3:
	xorl	%r15d, %r15d
	cmpl	(%rbx), %eax
	jne	.LBB9_10
# BB#4:
	movq	16(%rbp), %rdi
	callq	list_Length
	movl	%eax, %r12d
	movq	16(%rbx), %rdi
	callq	list_Length
	cmpl	%eax, %r12d
	jne	.LBB9_10
# BB#5:
	movq	16(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB9_9
# BB#6:                                 # %.preheader.preheader
	addq	$16, %rbx
	.p2align	4, 0x90
.LBB9_7:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movq	8(%rbp), %rsi
	movq	8(%rbx), %rdx
	movq	%r14, %rdi
	callq	unify_MatchFlexible
	testl	%eax, %eax
	je	.LBB9_10
# BB#8:                                 #   in Loop: Header=BB9_7 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB9_7
	jmp	.LBB9_9
.LBB9_2:
	leaq	8(%r14,%rax), %rcx
	movq	cont_INSTANCECONTEXT(%rip), %rdx
	leaq	(%r14,%rax), %rsi
	movq	%rsi, cont_CURRENTBINDING(%rip)
	movq	%rbx, (%rcx)
	movq	%rdx, 16(%r14,%rax)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 24(%r14,%rax)
	movq	%rsi, cont_LASTBINDING(%rip)
	incl	cont_BINDINGS(%rip)
.LBB9_9:
	movl	$1, %r15d
.LBB9_10:                               # %.critedge
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	unify_MatchFlexible, .Lfunc_end9-unify_MatchFlexible
	.cfi_endproc

	.globl	unify_EstablishMatcher
	.p2align	4, 0x90
	.type	unify_EstablishMatcher,@function
unify_EstablishMatcher:                 # @unify_EstablishMatcher
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB10_3
# BB#1:                                 # %.lr.ph.preheader
	movq	cont_INSTANCECONTEXT(%rip), %r8
	movq	cont_LASTBINDING(%rip), %r10
	movl	cont_BINDINGS(%rip), %ecx
	incl	%ecx
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rsi), %r9
	movslq	8(%rsi), %rax
	shlq	$5, %rax
	leaq	(%rdi,%rax), %rdx
	movq	%rdx, cont_CURRENTBINDING(%rip)
	movq	%r9, 8(%rdi,%rax)
	movq	%r8, 16(%rdi,%rax)
	movq	%r10, 24(%rdi,%rax)
	movq	%rdx, cont_LASTBINDING(%rip)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	(%rsi), %rsi
	incl	%ecx
	testq	%rsi, %rsi
	movq	%rdx, %r10
	jne	.LBB10_2
.LBB10_3:                               # %._crit_edge
	retq
.Lfunc_end10:
	.size	unify_EstablishMatcher, .Lfunc_end10-unify_EstablishMatcher
	.cfi_endproc

	.globl	unify_MatchBindingsHelp
	.p2align	4, 0x90
	.type	unify_MatchBindingsHelp,@function
unify_MatchBindingsHelp:                # @unify_MatchBindingsHelp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 48
.Lcfi61:
	.cfi_offset %rbx, -48
.Lcfi62:
	.cfi_offset %r12, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rdi, %r14
	movl	(%rcx), %edx
	testl	%edx, %edx
	jle	.LBB11_1
# BB#2:                                 # %.lr.ph78
	movq	cont_INSTANCECONTEXT(%rip), %rax
	.p2align	4, 0x90
.LBB11_3:                               # =>This Inner Loop Header: Depth=1
	leal	-2001(%rdx), %edi
	cmpl	$1000, %edi             # imm = 0x3E8
	movq	%r14, %rdi
	jb	.LBB11_6
# BB#4:                                 #   in Loop: Header=BB11_3 Depth=1
	cmpq	%rax, %r15
	movq	%r15, %rdi
	je	.LBB11_5
.LBB11_6:                               #   in Loop: Header=BB11_3 Depth=1
	movslq	%edx, %rdx
	shlq	$5, %rdx
	movq	8(%rdi,%rdx), %rbx
	testq	%rbx, %rbx
	je	.LBB11_7
# BB#8:                                 #   in Loop: Header=BB11_3 Depth=1
	movq	16(%rdi,%rdx), %r15
	movl	(%rbx), %edx
	testl	%edx, %edx
	movq	%rbx, %rcx
	jg	.LBB11_3
	jmp	.LBB11_9
.LBB11_1:
	movq	%rcx, %rbx
	jmp	.LBB11_9
.LBB11_7:
	movq	%rcx, %rbx
	movq	%rdi, %r15
	jmp	.LBB11_9
.LBB11_5:
	movq	%rcx, %rbx
	movq	%rax, %r15
.LBB11_9:                               # %.thread
	movslq	(%rsi), %rax
	testq	%rax, %rax
	jle	.LBB11_12
# BB#10:
	shlq	$5, %rax
	movq	8(%r14,%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB11_11
# BB#19:
	movq	16(%r14,%rax), %rsi
	movq	%r14, %rdi
	movq	%r15, %rcx
	movq	%rbx, %r8
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	cont_TermEqualModuloBindings # TAILCALL
.LBB11_12:
	xorl	%r12d, %r12d
	cmpl	(%rbx), %eax
	jne	.LBB11_18
# BB#13:
	movq	16(%rsi), %rbp
	testq	%rbp, %rbp
	je	.LBB11_17
# BB#14:                                # %.lr.ph
	addq	$16, %rbx
	.p2align	4, 0x90
.LBB11_15:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movq	8(%rbp), %rsi
	movq	8(%rbx), %rcx
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	unify_MatchBindingsHelp
	testl	%eax, %eax
	je	.LBB11_18
# BB#16:                                #   in Loop: Header=BB11_15 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB11_15
	jmp	.LBB11_17
.LBB11_11:
	leaq	8(%r14,%rax), %rcx
	leaq	(%r14,%rax), %rdx
	movq	%rdx, cont_CURRENTBINDING(%rip)
	movq	%rbx, (%rcx)
	movq	%r15, 16(%r14,%rax)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 24(%r14,%rax)
	movq	%rdx, cont_LASTBINDING(%rip)
	incl	cont_BINDINGS(%rip)
.LBB11_17:
	movl	$1, %r12d
.LBB11_18:                              # %.critedge
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	unify_MatchBindingsHelp, .Lfunc_end11-unify_MatchBindingsHelp
	.cfi_endproc

	.globl	unify_MatchBindings
	.p2align	4, 0x90
	.type	unify_MatchBindings,@function
unify_MatchBindings:                    # @unify_MatchBindings
	.cfi_startproc
# BB#0:
	movq	%rdx, %rax
	movq	cont_INSTANCECONTEXT(%rip), %rdx
	movq	%rax, %rcx
	jmp	unify_MatchBindingsHelp # TAILCALL
.Lfunc_end12:
	.size	unify_MatchBindings, .Lfunc_end12-unify_MatchBindings
	.cfi_endproc

	.globl	unify_MatchReverse
	.p2align	4, 0x90
	.type	unify_MatchReverse,@function
unify_MatchReverse:                     # @unify_MatchReverse
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 48
.Lcfi71:
	.cfi_offset %rbx, -48
.Lcfi72:
	.cfi_offset %r12, -40
.Lcfi73:
	.cfi_offset %r14, -32
.Lcfi74:
	.cfi_offset %r15, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movl	(%rcx), %edx
	testl	%edx, %edx
	jle	.LBB13_6
# BB#1:                                 # %.lr.ph98
	movq	cont_INSTANCECONTEXT(%rip), %rax
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	leal	-2001(%rdx), %edi
	cmpl	$1000, %edi             # imm = 0x3E8
	movq	%r14, %rdi
	jb	.LBB13_4
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=1
	cmpq	%rax, %rbx
	movq	%rbx, %rdi
	je	.LBB13_8
.LBB13_4:                               #   in Loop: Header=BB13_2 Depth=1
	movslq	%edx, %rdx
	shlq	$5, %rdx
	movq	8(%rdi,%rdx), %rbp
	testq	%rbp, %rbp
	je	.LBB13_7
# BB#5:                                 #   in Loop: Header=BB13_2 Depth=1
	movq	16(%rdi,%rdx), %rbx
	movl	(%rbp), %edx
	testl	%edx, %edx
	movq	%rbp, %rcx
	jg	.LBB13_2
	jmp	.LBB13_9
.LBB13_6:
	movq	%rcx, %rbp
	jmp	.LBB13_9
.LBB13_7:
	movq	%rcx, %rbp
	movq	%rdi, %rbx
	jmp	.LBB13_9
.LBB13_8:
	movq	%rcx, %rbp
	movq	%rax, %rbx
.LBB13_9:                               # %.thread
	movslq	(%rsi), %rcx
	testq	%rcx, %rcx
	jle	.LBB13_14
# BB#10:
	movq	cont_INSTANCECONTEXT(%rip), %rax
	cmpq	%rax, %rbx
	jne	.LBB13_12
# BB#11:
	movl	$1, %r15d
	cmpl	(%rbp), %ecx
	je	.LBB13_25
.LBB13_12:
	leal	-2001(%rcx), %edx
	cmpl	$999, %edx              # imm = 0x3E7
	ja	.LBB13_18
# BB#13:
	shlq	$5, %rcx
	leaq	(%r14,%rcx), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	%rbp, 8(%r14,%rcx)
	movq	%rbx, 16(%r14,%rcx)
	movq	cont_LASTBINDING(%rip), %rdx
	movq	%rdx, 24(%r14,%rcx)
	movq	%rax, cont_LASTBINDING(%rip)
	jmp	.LBB13_23
.LBB13_14:
	movslq	(%rbp), %rax
	testq	%rax, %rax
	jle	.LBB13_26
# BB#15:
	cmpq	%r14, %rbx
	je	.LBB13_17
# BB#16:
	leal	-2001(%rax), %ecx
	xorl	%r15d, %r15d
	cmpl	$999, %ecx              # imm = 0x3E7
	ja	.LBB13_25
.LBB13_17:
	movq	cont_INSTANCECONTEXT(%rip), %rcx
	shlq	$5, %rax
	leaq	(%r14,%rax), %rdx
	movq	%rdx, cont_CURRENTBINDING(%rip)
	movq	%rsi, 8(%r14,%rax)
	movq	%rcx, 16(%r14,%rax)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 24(%r14,%rax)
	jmp	.LBB13_22
.LBB13_18:
	movslq	(%rbp), %rcx
	xorl	%r15d, %r15d
	testq	%rcx, %rcx
	jle	.LBB13_25
# BB#19:
	cmpq	%r14, %rbx
	je	.LBB13_21
# BB#20:
	leal	-2001(%rcx), %edx
	cmpl	$999, %edx              # imm = 0x3E7
	ja	.LBB13_25
.LBB13_21:
	shlq	$5, %rcx
	leaq	(%r14,%rcx), %rdx
	movq	%rdx, cont_CURRENTBINDING(%rip)
	movq	%rsi, 8(%r14,%rcx)
	movq	%rax, 16(%r14,%rcx)
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, 24(%r14,%rcx)
.LBB13_22:                              # %.critedge
	movq	%rdx, cont_LASTBINDING(%rip)
.LBB13_23:
	incl	cont_BINDINGS(%rip)
.LBB13_24:
	movl	$1, %r15d
.LBB13_25:                              # %.critedge
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_26:
	xorl	%r15d, %r15d
	cmpl	%eax, %ecx
	jne	.LBB13_25
# BB#27:
	movq	16(%rsi), %r12
	testq	%r12, %r12
	je	.LBB13_24
# BB#28:                                # %.lr.ph
	addq	$16, %rbp
	.p2align	4, 0x90
.LBB13_29:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbp
	movq	8(%r12), %rsi
	movq	8(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	unify_MatchReverse
	testl	%eax, %eax
	je	.LBB13_25
# BB#30:                                #   in Loop: Header=BB13_29 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB13_29
	jmp	.LBB13_24
.Lfunc_end13:
	.size	unify_MatchReverse, .Lfunc_end13-unify_MatchReverse
	.cfi_endproc

	.globl	unify_Variation
	.p2align	4, 0x90
	.type	unify_Variation,@function
unify_Variation:                        # @unify_Variation
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi80:
	.cfi_def_cfa_offset 48
.Lcfi81:
	.cfi_offset %rbx, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movslq	(%rsi), %rax
	testq	%rax, %rax
	movl	(%rbx), %ecx
	jle	.LBB14_5
# BB#1:
	movl	$1, %r15d
	cmpl	%ecx, %eax
	je	.LBB14_11
# BB#2:
	leal	-2001(%rax), %ecx
	xorl	%r15d, %r15d
	cmpl	$999, %ecx              # imm = 0x3E7
	ja	.LBB14_11
# BB#3:
	shlq	$5, %rax
	movq	8(%r14,%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB14_4
# BB#12:
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	term_Equal              # TAILCALL
.LBB14_5:
	xorl	%r15d, %r15d
	cmpl	%ecx, %eax
	jne	.LBB14_11
# BB#6:
	movq	16(%rsi), %rbp
	testq	%rbp, %rbp
	je	.LBB14_10
# BB#7:                                 # %.lr.ph
	addq	$16, %rbx
	.p2align	4, 0x90
.LBB14_8:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movq	8(%rbp), %rsi
	movq	8(%rbx), %rdx
	movq	%r14, %rdi
	callq	unify_Variation
	testl	%eax, %eax
	je	.LBB14_11
# BB#9:                                 #   in Loop: Header=BB14_8 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB14_8
	jmp	.LBB14_10
.LBB14_4:
	leaq	8(%r14,%rax), %rcx
	leaq	(%r14,%rax), %rdx
	movq	%rdx, cont_CURRENTBINDING(%rip)
	movq	%rbx, (%rcx)
	movq	%r14, 16(%r14,%rax)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 24(%r14,%rax)
	movq	%rdx, cont_LASTBINDING(%rip)
	incl	cont_BINDINGS(%rip)
.LBB14_10:
	movl	$1, %r15d
.LBB14_11:                              # %.critedge
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	unify_Variation, .Lfunc_end14-unify_Variation
	.cfi_endproc

	.globl	unify_ComGenLinear
	.p2align	4, 0x90
	.type	unify_ComGenLinear,@function
unify_ComGenLinear:                     # @unify_ComGenLinear
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi91:
	.cfi_def_cfa_offset 80
.Lcfi92:
	.cfi_offset %rbx, -56
.Lcfi93:
	.cfi_offset %r12, -48
.Lcfi94:
	.cfi_offset %r13, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
	movq	%rsi, %r13
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	(%rbx), %r12d
	leal	-2001(%r12), %eax
	cmpl	$999, %eax              # imm = 0x3E7
	ja	.LBB15_1
# BB#23:
	movq	%rdx, %rdi
	callq	term_Copy
	movq	(%r13), %rdx
	movl	%r12d, %edi
	movq	%rax, %rsi
	callq	subst_Add
	movq	%rax, (%r13)
	movq	%rbx, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	term_Copy               # TAILCALL
.LBB15_1:
	cmpl	%r12d, (%rdx)
	jne	.LBB15_15
# BB#2:
	movq	16(%rdx), %r15
	testq	%r15, %r15
	je	.LBB15_3
# BB#4:                                 # %.lr.ph.preheader
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	addq	$16, %rbx
	xorl	%r14d, %r14d
	movq	%rcx, %r12
	.p2align	4, 0x90
.LBB15_5:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_9 Depth 2
	movq	(%rbx), %rbx
	movq	8(%r15), %rdx
	movq	8(%rbx), %r8
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r13, %rsi
	movq	%r12, %rcx
	callq	unify_ComGenLinear
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	testq	%r14, %r14
	je	.LBB15_6
# BB#7:                                 #   in Loop: Header=BB15_5 Depth=1
	testq	%rax, %rax
	je	.LBB15_11
# BB#8:                                 # %.preheader.i49.preheader
                                        #   in Loop: Header=BB15_5 Depth=1
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB15_9:                               # %.preheader.i49
                                        #   Parent Loop BB15_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB15_9
# BB#10:                                #   in Loop: Header=BB15_5 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB15_11
	.p2align	4, 0x90
.LBB15_6:                               #   in Loop: Header=BB15_5 Depth=1
	movq	%rax, %r14
.LBB15_11:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB15_5 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB15_5
# BB#12:                                # %._crit_edge.loopexit
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r12d
	jmp	.LBB15_13
.LBB15_15:
	movq	%rdx, %rdi
	movl	cont_INDEXVARSCANNER(%rip), %eax
	movl	symbol_INDEXVARCOUNTER(%rip), %ebp
	cmpl	%ebp, %eax
	movq	%rcx, %r14
	je	.LBB15_20
# BB#16:                                # %.preheader.i
	movslq	%eax, %rcx
	shlq	$5, %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	48(%rdx,%rcx), %rcx
	.p2align	4, 0x90
.LBB15_17:                              # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rcx)
	je	.LBB15_21
# BB#18:                                #   in Loop: Header=BB15_17 Depth=1
	incl	%eax
	addq	$32, %rcx
	cmpl	%eax, %ebp
	jne	.LBB15_17
# BB#19:                                # %.sink.split.loopexit.i
	movl	%ebp, cont_INDEXVARSCANNER(%rip)
.LBB15_20:                              # %.sink.split.i
	incl	%ebp
	movl	%ebp, symbol_INDEXVARCOUNTER(%rip)
	jmp	.LBB15_22
.LBB15_3:
	xorl	%r14d, %r14d
.LBB15_13:                              # %._crit_edge
	movl	%r12d, %edi
	movq	%r14, %rsi
	jmp	.LBB15_14
.LBB15_21:                              # %cont_NextIndexVariable.exit.loopexit
	incl	%eax
	movl	%eax, %ebp
.LBB15_22:                              # %cont_NextIndexVariable.exit
	movl	%ebp, cont_INDEXVARSCANNER(%rip)
	callq	term_Copy
	movq	(%r13), %rdx
	movl	%ebp, %edi
	movq	%rax, %rsi
	callq	subst_Add
	movq	%rax, (%r13)
	movq	%rbx, %rdi
	callq	term_Copy
	movq	(%r14), %rdx
	movl	%ebp, %edi
	movq	%rax, %rsi
	callq	subst_Add
	movq	%rax, (%r14)
	xorl	%esi, %esi
	movl	%ebp, %edi
.LBB15_14:                              # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	term_Create             # TAILCALL
.Lfunc_end15:
	.size	unify_ComGenLinear, .Lfunc_end15-unify_ComGenLinear
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
