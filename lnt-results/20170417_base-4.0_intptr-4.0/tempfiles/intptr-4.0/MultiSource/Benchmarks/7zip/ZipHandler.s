	.text
	.file	"ZipHandler.bc"
	.globl	_ZN8NArchive4NZip8CHandlerC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandlerC2Ev,@function
_ZN8NArchive4NZip8CHandlerC2Ev:         # @_ZN8NArchive4NZip8CHandlerC2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$0, 24(%rbx)
	movl	$_ZTVN8NArchive4NZip8CHandlerE+176, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NZip8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN8NArchive4NZip8CHandlerE+248, 16(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 40(%rbx)
	movq	$8, 56(%rbx)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip7CItemExEE+16, 32(%rbx)
	movq	$0, 64(%rbx)
	leaq	104(%rbx), %r15
.Ltmp0:
	movq	%r15, %rdi
	callq	_ZN9CInBufferC1Ev
.Ltmp1:
# BB#1:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 152(%rbx)
	movq	$_ZTV7CBufferIhE+16, 176(%rbx)
	movdqu	%xmm0, 184(%rbx)
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 224(%rbx)
	movdqu	%xmm0, 208(%rbx)
	movl	$-1, 240(%rbx)
	movl	$50331648, 244(%rbx)    # imm = 0x3000000
	movw	$0, 248(%rbx)
	movb	$0, 250(%rbx)
.Ltmp6:
	callq	_ZN8NWindows7NSystem21GetNumberOfProcessorsEv
.Ltmp7:
# BB#2:
	movl	%eax, 252(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB0_6:
.Ltmp8:
	movq	%rax, %r14
	movq	$_ZTV7CBufferIhE+16, 176(%rbx)
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_8
# BB#7:
	callq	_ZdaPv
.LBB0_8:                                # %_ZN8NArchive4NZip14CInArchiveInfoD2Ev.exit.i
.Ltmp9:
	movq	%r15, %rdi
	callq	_ZN9CInBuffer4FreeEv
.Ltmp10:
# BB#9:
	movq	128(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_11
# BB#10:
	movq	(%rdi), %rax
.Ltmp15:
	callq	*16(%rax)
.Ltmp16:
.LBB0_11:                               # %_ZN9CInBufferD2Ev.exit.i
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_13
# BB#12:
	movq	(%rdi), %rax
.Ltmp21:
	callq	*16(%rax)
.Ltmp22:
	jmp	.LBB0_13
.LBB0_19:
.Ltmp17:
	movq	%rax, %r14
	jmp	.LBB0_20
.LBB0_16:
.Ltmp11:
	movq	%rax, %r14
	movq	128(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_20
# BB#17:
	movq	(%rdi), %rax
.Ltmp12:
	callq	*16(%rax)
.Ltmp13:
.LBB0_20:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_26
# BB#21:
	movq	(%rdi), %rax
.Ltmp18:
	callq	*16(%rax)
.Ltmp19:
	jmp	.LBB0_26
.LBB0_18:
.Ltmp14:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_22:                               # %.body5.i
.Ltmp20:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_3:
.Ltmp2:
	movq	%rax, %r14
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_13
# BB#4:
	movq	(%rdi), %rax
.Ltmp3:
	callq	*16(%rax)
.Ltmp4:
.LBB0_13:                               # %_ZN8NArchive4NZip10CInArchiveD2Ev.exit
	addq	$32, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip7CItemExEE+16, (%rbx)
.Ltmp23:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp24:
# BB#14:
.Ltmp29:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp30:
# BB#15:                                # %_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_5:                                # %.body.i
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_23:
.Ltmp25:
	movq	%rax, %r14
.Ltmp26:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp27:
	jmp	.LBB0_26
.LBB0_24:
.Ltmp28:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_25:
.Ltmp31:
	movq	%rax, %r14
.LBB0_26:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN8NArchive4NZip8CHandlerC2Ev, .Lfunc_end0-_ZN8NArchive4NZip8CHandlerC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\245\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp31-.Lfunc_begin0   #     jumps to .Ltmp31
	.byte	1                       #   On action: 1
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	1                       #   On action: 1
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 8 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	1                       #   On action: 1
	.long	.Ltmp29-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin0   #     jumps to .Ltmp31
	.byte	1                       #   On action: 1
	.long	.Ltmp30-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp26-.Ltmp30         #   Call between .Ltmp30 and .Ltmp26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip7CItemExEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip7CItemExEE+16, (%rbx)
.Ltmp32:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp33:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB2_2:
.Ltmp34:
	movq	%rax, %r14
.Ltmp35:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp36:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_4:
.Ltmp37:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED2Ev, .Lfunc_end2-_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp32-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin1   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp35-.Ltmp33         #   Call between .Ltmp33 and .Ltmp35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin1   #     jumps to .Ltmp37
	.byte	1                       #   On action: 1
	.long	.Ltmp36-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp36     #   Call between .Ltmp36 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip8CHandler21GetNumberOfPropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler21GetNumberOfPropertiesEPj,@function
_ZN8NArchive4NZip8CHandler21GetNumberOfPropertiesEPj: # @_ZN8NArchive4NZip8CHandler21GetNumberOfPropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$14, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	_ZN8NArchive4NZip8CHandler21GetNumberOfPropertiesEPj, .Lfunc_end3-_ZN8NArchive4NZip8CHandler21GetNumberOfPropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive4NZip8CHandler15GetPropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler15GetPropertyInfoEjPPwPjPt,@function
_ZN8NArchive4NZip8CHandler15GetPropertyInfoEjPPwPjPt: # @_ZN8NArchive4NZip8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$13, %esi
	ja	.LBB4_2
# BB#1:
	movl	%esi, %eax
	shlq	$4, %rax
	movl	_ZN8NArchive4NZipL6kPropsE+8(%rax), %esi
	movl	%esi, (%rcx)
	movzwl	_ZN8NArchive4NZipL6kPropsE+12(%rax), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB4_2:
	retq
.Lfunc_end4:
	.size	_ZN8NArchive4NZip8CHandler15GetPropertyInfoEjPPwPjPt, .Lfunc_end4-_ZN8NArchive4NZip8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive4NZip8CHandler28GetNumberOfArchivePropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler28GetNumberOfArchivePropertiesEPj,@function
_ZN8NArchive4NZip8CHandler28GetNumberOfArchivePropertiesEPj: # @_ZN8NArchive4NZip8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$4, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	_ZN8NArchive4NZip8CHandler28GetNumberOfArchivePropertiesEPj, .Lfunc_end5-_ZN8NArchive4NZip8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive4NZip8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler22GetArchivePropertyInfoEjPPwPjPt,@function
_ZN8NArchive4NZip8CHandler22GetArchivePropertyInfoEjPPwPjPt: # @_ZN8NArchive4NZip8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$3, %esi
	ja	.LBB6_2
# BB#1:
	movl	%esi, %eax
	shlq	$4, %rax
	movl	_ZN8NArchive4NZipL9kArcPropsE+8(%rax), %esi
	movl	%esi, (%rcx)
	movzwl	_ZN8NArchive4NZipL9kArcPropsE+12(%rax), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB6_2:
	retq
.Lfunc_end6:
	.size	_ZN8NArchive4NZip8CHandler22GetArchivePropertyInfoEjPPwPjPt, .Lfunc_end6-_ZN8NArchive4NZip8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive4NZip8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler18GetArchivePropertyEjP14tagPROPVARIANT,@function
_ZN8NArchive4NZip8CHandler18GetArchivePropertyEjP14tagPROPVARIANT: # @_ZN8NArchive4NZip8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 80
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movq	%rdx, %rbx
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %rax
	movl	$0, 8(%rsp)
	addl	$-28, %esi
	cmpl	$16, %esi
	ja	.LBB7_20
# BB#1:
	jmpq	*.LJTI7_0(,%rsi,8)
.LBB7_4:
	addq	$176, %rax
.Ltmp42:
	leaq	24(%rsp), %rdi
	movq	%rax, %rsi
	callq	_ZN8NArchive4NZipL13BytesToStringERK7CBufferIhE
.Ltmp43:
# BB#5:
.Ltmp45:
	leaq	40(%rsp), %rdi
	leaq	24(%rsp), %rsi
	xorl	%edx, %edx
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp46:
# BB#6:
	movq	40(%rsp), %rsi
.Ltmp48:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp49:
# BB#7:
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_9
# BB#8:
	callq	_ZdaPv
.LBB7_9:                                # %_ZN11CStringBaseIwED2Ev.exit
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_20
# BB#10:
	callq	_ZdaPv
	jmp	.LBB7_20
.LBB7_2:
	cmpb	$0, 200(%rax)
	je	.LBB7_20
# BB#3:
.Ltmp51:
	leaq	8(%rsp), %rdi
	movl	$1, %esi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp52:
	jmp	.LBB7_20
.LBB7_18:
	movq	160(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB7_20
# BB#19:
.Ltmp38:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp39:
	jmp	.LBB7_20
.LBB7_17:
	movq	168(%rax), %rsi
	subq	160(%rax), %rsi
.Ltmp40:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp41:
.LBB7_20:
.Ltmp53:
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp54:
# BB#21:
.Ltmp59:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp60:
# BB#22:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit17
	xorl	%eax, %eax
.LBB7_31:
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB7_13:
.Ltmp50:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_15
# BB#14:
	callq	_ZdaPv
	jmp	.LBB7_15
.LBB7_12:
.Ltmp47:
	movq	%rdx, %r14
	movq	%rax, %rbx
.LBB7_15:                               # %_ZN11CStringBaseIwED2Ev.exit15
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_26
# BB#16:
	callq	_ZdaPv
	jmp	.LBB7_26
.LBB7_11:
.Ltmp44:
	jmp	.LBB7_25
.LBB7_23:
.Ltmp61:
	movq	%rdx, %r14
	movq	%rax, %rbx
	jmp	.LBB7_27
.LBB7_24:
.Ltmp55:
.LBB7_25:
	movq	%rdx, %r14
	movq	%rax, %rbx
.LBB7_26:
.Ltmp56:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp57:
.LBB7_27:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB7_28
# BB#30:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB7_31
.LBB7_28:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp62:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp63:
# BB#33:
.LBB7_29:
.Ltmp64:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB7_32:
.Ltmp58:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN8NArchive4NZip8CHandler18GetArchivePropertyEjP14tagPROPVARIANT, .Lfunc_end7-_ZN8NArchive4NZip8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_4
	.quad	.LBB7_20
	.quad	.LBB7_20
	.quad	.LBB7_20
	.quad	.LBB7_20
	.quad	.LBB7_20
	.quad	.LBB7_20
	.quad	.LBB7_20
	.quad	.LBB7_18
	.quad	.LBB7_20
	.quad	.LBB7_20
	.quad	.LBB7_20
	.quad	.LBB7_20
	.quad	.LBB7_2
	.quad	.LBB7_20
	.quad	.LBB7_20
	.quad	.LBB7_17
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\203\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Ltmp42-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin2   #     jumps to .Ltmp44
	.byte	3                       #   On action: 2
	.long	.Ltmp45-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin2   #     jumps to .Ltmp47
	.byte	3                       #   On action: 2
	.long	.Ltmp48-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin2   #     jumps to .Ltmp50
	.byte	3                       #   On action: 2
	.long	.Ltmp51-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp54-.Ltmp51         #   Call between .Ltmp51 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin2   #     jumps to .Ltmp55
	.byte	3                       #   On action: 2
	.long	.Ltmp59-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin2   #     jumps to .Ltmp61
	.byte	3                       #   On action: 2
	.long	.Ltmp56-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin2   #     jumps to .Ltmp58
	.byte	1                       #   On action: 1
	.long	.Ltmp57-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp62-.Ltmp57         #   Call between .Ltmp57 and .Ltmp62
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin2   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Lfunc_end7-.Ltmp63     #   Call between .Ltmp63 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZipL13BytesToStringERK7CBufferIhE,@function
_ZN8NArchive4NZipL13BytesToStringERK7CBufferIhE: # @_ZN8NArchive4NZipL13BytesToStringERK7CBufferIhE
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 64
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r13
	movq	%r13, (%r14)
	movb	$0, (%r13)
	movl	$4, 12(%r14)
	movq	8(%r15), %rbx
	testl	%ebx, %ebx
	jle	.LBB8_8
# BB#1:
	leal	1(%rbx), %eax
	cmpl	$4, %eax
	jl	.LBB8_5
# BB#2:
	leal	2(%rbx), %ebp
	cmpl	$4, %ebp
	je	.LBB8_5
# BB#3:
	movslq	%ebp, %rdi
.Ltmp65:
	callq	_Znam
	movq	%rax, %r12
.Ltmp66:
# BB#4:
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%r12, (%r14)
	movb	$0, (%r12)
	movl	%ebp, 12(%r14)
	movq	%r12, %r13
.LBB8_5:
	movq	16(%r15), %rsi
	movslq	%ebx, %rbx
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	movb	$0, (%r13,%rbx)
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	movl	$-1, %eax
	movabsq	$4294967296, %rdx       # imm = 0x100000000
	movq	%r13, %rsi
	.p2align	4, 0x90
.LBB8_6:                                # =>This Inner Loop Header: Depth=1
	addq	%rdx, %rcx
	incl	%eax
	cmpb	$0, (%rsi)
	leaq	1(%rsi), %rsi
	jne	.LBB8_6
# BB#7:                                 # %_ZN11CStringBaseIcE13ReleaseBufferEv.exit
	sarq	$32, %rcx
	movb	$0, (%r13,%rcx)
	movl	%eax, 8(%r14)
.LBB8_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_9:                                # %_ZN11CStringBaseIcED2Ev.exit
.Ltmp67:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN8NArchive4NZipL13BytesToStringERK7CBufferIhE, .Lfunc_end8-_ZN8NArchive4NZipL13BytesToStringERK7CBufferIhE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp65-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp65
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin3   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Lfunc_end8-.Ltmp66     #   Call between .Ltmp66 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip8CHandler16GetNumberOfItemsEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler16GetNumberOfItemsEPj,@function
_ZN8NArchive4NZip8CHandler16GetNumberOfItemsEPj: # @_ZN8NArchive4NZip8CHandler16GetNumberOfItemsEPj
	.cfi_startproc
# BB#0:
	movl	44(%rdi), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	_ZN8NArchive4NZip8CHandler16GetNumberOfItemsEPj, .Lfunc_end9-_ZN8NArchive4NZip8CHandler16GetNumberOfItemsEPj
	.cfi_endproc

	.globl	_ZN8NArchive4NZip8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler11GetPropertyEjjP14tagPROPVARIANT,@function
_ZN8NArchive4NZip8CHandler11GetPropertyEjjP14tagPROPVARIANT: # @_ZN8NArchive4NZip8CHandler11GetPropertyEjjP14tagPROPVARIANT
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 48
	subq	$112, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 160
.Lcfi35:
	.cfi_offset %rbx, -48
.Lcfi36:
	.cfi_offset %r12, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	$0, 16(%rsp)
	addl	$-3, %edx
	cmpl	$37, %edx
	ja	.LBB10_122
# BB#1:
	movq	48(%rdi), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rbx
	jmpq	*.LJTI10_0(,%rdx,8)
.LBB10_2:
	leaq	32(%rbx), %rdx
.Ltmp180:
	leaq	32(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZNK8NArchive4NZip10CLocalItem16GetUnicodeStringERK11CStringBaseIcE
.Ltmp181:
# BB#3:
.Ltmp183:
	leaq	96(%rsp), %rdi
	leaq	32(%rsp), %rsi
	callq	_ZN8NArchive9NItemName10GetOSName2ERK11CStringBaseIwE
.Ltmp184:
# BB#4:
	movq	96(%rsp), %rsi
.Ltmp186:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp187:
# BB#5:
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB10_6
	jmp	.LBB10_7
.LBB10_8:
	movzbl	(%rbx), %esi
.Ltmp68:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp69:
	jmp	.LBB10_122
.LBB10_9:
	movw	4(%rbx), %r12w
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp72:
	movl	$4, %edi
	callq	_Znam
.Ltmp73:
# BB#10:
	movq	%rax, (%rsp)
	movb	$0, (%rax)
	movl	$4, 12(%rsp)
	movzwl	2(%rbx), %ecx
	testb	$1, %cl
	je	.LBB10_104
# BB#11:
	movzwl	%r12w, %edx
	cmpl	$99, %edx
	jne	.LBB10_81
# BB#12:                                # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.preheader
	movb	$65, (%rax)
	movb	$69, 1(%rax)
	movb	$83, 2(%rax)
	movb	$0, 3(%rax)
	movl	$3, 8(%rsp)
	movslq	132(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB10_116
# BB#13:                                # %.lr.ph.i139
	movq	136(%rbx), %rcx
	xorl	%edx, %edx
	movl	$99, %edi
	.p2align	4, 0x90
.LBB10_14:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	movzwl	(%rsi), %ebp
	cmpl	$39169, %ebp            # imm = 0x9901
	jne	.LBB10_18
# BB#15:                                #   in Loop: Header=BB10_14 Depth=1
	cmpq	$7, 16(%rsi)
	jb	.LBB10_18
# BB#16:                                #   in Loop: Header=BB10_14 Depth=1
	movq	24(%rsi), %rsi
	cmpb	$65, 2(%rsi)
	jne	.LBB10_18
# BB#17:                                #   in Loop: Header=BB10_14 Depth=1
	cmpb	$69, 3(%rsi)
	je	.LBB10_131
	.p2align	4, 0x90
.LBB10_18:                              #   in Loop: Header=BB10_14 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB10_14
	jmp	.LBB10_117
.LBB10_19:
	leaq	152(%rbx), %rsi
.Ltmp121:
	leaq	32(%rsp), %rdi
	callq	_ZN8NArchive4NZipL13BytesToStringERK7CBufferIhE
.Ltmp122:
# BB#20:
.Ltmp124:
	leaq	80(%rsp), %rdi
	leaq	32(%rsp), %rdx
	movq	%rbx, %rsi
	callq	_ZNK8NArchive4NZip10CLocalItem16GetUnicodeStringERK11CStringBaseIcE
.Ltmp125:
# BB#21:
	movq	80(%rsp), %rsi
.Ltmp127:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp128:
# BB#22:
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_7
.LBB10_6:
	callq	_ZdaPv
.LBB10_7:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB10_121
	jmp	.LBB10_122
.LBB10_24:
	movq	24(%rbx), %rsi
.Ltmp174:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp175:
	jmp	.LBB10_122
.LBB10_25:
	movslq	132(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB10_122
# BB#26:                                # %.lr.ph.i95
	movq	136(%rbx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_27:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rdi
	movzwl	(%rdi), %esi
	cmpl	$10, %esi
	je	.LBB10_60
# BB#28:                                #   in Loop: Header=BB10_27 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB10_27
	jmp	.LBB10_122
.LBB10_29:
	movzbl	81(%rbx), %eax
	cmpq	$20, %rax
	leaq	_ZN8NArchive4NZipL7kHostOSE(,%rax,8), %rax
	movl	$_ZN8NArchive4NZipL10kUnknownOSE, %ecx
	cmovbq	%rax, %rcx
	movq	(%rcx), %rsi
.Ltmp70:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKc
.Ltmp71:
	jmp	.LBB10_122
.LBB10_30:
.Ltmp176:
	movq	%rbx, %rdi
	callq	_ZNK8NArchive4NZip5CItem5IsDirEv
.Ltmp177:
# BB#31:
.Ltmp178:
	movzbl	%al, %esi
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp179:
	jmp	.LBB10_122
.LBB10_32:
	movslq	132(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB10_122
# BB#33:                                # %.lr.ph.i103
	movq	136(%rbx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_34:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rdi
	movzwl	(%rdi), %esi
	cmpl	$10, %esi
	je	.LBB10_63
# BB#35:                                #   in Loop: Header=BB10_34 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB10_34
	jmp	.LBB10_122
.LBB10_36:
	movslq	132(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB10_73
# BB#37:                                # %.lr.ph.i111
	movq	136(%rbx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_38:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rdi
	movzwl	(%rdi), %esi
	cmpl	$10, %esi
	je	.LBB10_66
# BB#39:                                #   in Loop: Header=BB10_38 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB10_38
	jmp	.LBB10_69
.LBB10_40:
	movslq	132(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB10_94
# BB#41:                                # %.lr.ph.i
	movq	136(%rbx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_42:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rdi
	movzwl	(%rdi), %esi
	cmpl	$10, %esi
	je	.LBB10_78
# BB#43:                                #   in Loop: Header=BB10_42 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB10_42
	jmp	.LBB10_90
.LBB10_44:
	movq	16(%rbx), %rsi
.Ltmp172:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp173:
	jmp	.LBB10_122
.LBB10_45:
.Ltmp132:
	movq	%rbx, %rdi
	callq	_ZNK8NArchive4NZip5CItem16GetWinAttributesEv
.Ltmp133:
# BB#46:
.Ltmp134:
	leaq	16(%rsp), %rdi
	movl	%eax, %esi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp135:
	jmp	.LBB10_122
.LBB10_47:
	movzwl	4(%rbx), %eax
	cmpl	$99, %eax
	jne	.LBB10_55
# BB#48:
	movslq	132(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB10_55
# BB#49:                                # %.lr.ph.i.i
	movq	136(%rbx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_50:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	movzwl	(%rsi), %edi
	cmpl	$39169, %edi            # imm = 0x9901
	jne	.LBB10_54
# BB#51:                                #   in Loop: Header=BB10_50 Depth=1
	cmpq	$7, 16(%rsi)
	jb	.LBB10_54
# BB#52:                                #   in Loop: Header=BB10_50 Depth=1
	movq	24(%rsi), %rsi
	cmpb	$65, 2(%rsi)
	jne	.LBB10_54
# BB#53:                                #   in Loop: Header=BB10_50 Depth=1
	cmpb	$69, 3(%rsi)
	je	.LBB10_129
	.p2align	4, 0x90
.LBB10_54:                              #   in Loop: Header=BB10_50 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB10_50
.LBB10_55:                              # %.thread.i
	leaq	12(%rbx), %rbp
	cmpl	$0, 12(%rbx)
	jne	.LBB10_58
# BB#56:
.Ltmp117:
	movq	%rbx, %rdi
	callq	_ZNK8NArchive4NZip5CItem5IsDirEv
.Ltmp118:
# BB#57:                                # %_ZNK8NArchive4NZip5CItem10IsThereCrcEv.exit
	testb	%al, %al
	jne	.LBB10_122
.LBB10_58:                              # %_ZNK8NArchive4NZip5CItem10IsThereCrcEv.exit.thread
	movl	(%rbp), %esi
.Ltmp119:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp120:
	jmp	.LBB10_122
.LBB10_59:
	movzwl	2(%rbx), %esi
.Ltmp130:
	andl	$1, %esi
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp131:
	jmp	.LBB10_122
.LBB10_60:
.Ltmp156:
	leaq	32(%rsp), %rdx
	movl	$2, %esi
	callq	_ZNK8NArchive4NZip14CExtraSubBlock15ExtractNtfsTimeEiR9_FILETIME
.Ltmp157:
# BB#61:
	testb	%al, %al
	je	.LBB10_122
# BB#62:
.Ltmp158:
	leaq	16(%rsp), %rdi
	leaq	32(%rsp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp159:
	jmp	.LBB10_122
.LBB10_63:
.Ltmp151:
	leaq	32(%rsp), %rdx
	movl	$1, %esi
	callq	_ZNK8NArchive4NZip14CExtraSubBlock15ExtractNtfsTimeEiR9_FILETIME
.Ltmp152:
# BB#64:
	testb	%al, %al
	je	.LBB10_122
# BB#65:
.Ltmp153:
	leaq	16(%rsp), %rdi
	leaq	32(%rsp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp154:
	jmp	.LBB10_122
.LBB10_66:
.Ltmp136:
	leaq	32(%rsp), %rdx
	xorl	%esi, %esi
	callq	_ZNK8NArchive4NZip14CExtraSubBlock15ExtractNtfsTimeEiR9_FILETIME
.Ltmp137:
# BB#67:
	testb	%al, %al
	jne	.LBB10_98
# BB#68:                                # %..thread171_crit_edge
	movl	132(%rbx), %eax
.LBB10_69:                              # %.thread171
	testl	%eax, %eax
	jle	.LBB10_73
# BB#70:                                # %.lr.ph.i119
	movq	136(%rbx), %rcx
	cltq
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_71:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rdi
	movzwl	(%rdi), %esi
	cmpl	$21589, %esi            # imm = 0x5455
	je	.LBB10_95
# BB#72:                                #   in Loop: Header=BB10_71 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB10_71
	jmp	.LBB10_73
.LBB10_78:
.Ltmp161:
	leaq	32(%rsp), %rdx
	xorl	%esi, %esi
	callq	_ZNK8NArchive4NZip14CExtraSubBlock15ExtractNtfsTimeEiR9_FILETIME
.Ltmp162:
# BB#79:
	testb	%al, %al
	je	.LBB10_89
# BB#80:
.Ltmp169:
	leaq	16(%rsp), %rdi
	xorl	%esi, %esi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp170:
	jmp	.LBB10_122
.LBB10_81:
	andl	$65, %ecx
	cmpl	$65, %ecx
	jne	.LBB10_102
# BB#82:
	movslq	132(%rbx), %rcx
	testq	%rcx, %rcx
	jle	.LBB10_88
# BB#83:                                # %.lr.ph.i143
	movq	136(%rbx), %rsi
	xorl	%edi, %edi
.LBB10_84:                              # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rdi,8), %rax
	movzwl	(%rax), %edx
	cmpl	$23, %edx
	jne	.LBB10_87
# BB#85:                                #   in Loop: Header=BB10_84 Depth=1
	cmpq	$8, 16(%rax)
	jb	.LBB10_87
# BB#86:                                # %_ZN8NArchive4NZip18CStrongCryptoField17ParseFromSubBlockERKNS0_14CExtraSubBlockE.exit.i
                                        #   in Loop: Header=BB10_84 Depth=1
	movq	24(%rax), %rax
	movzbl	1(%rax), %edx
	shll	$8, %edx
	movzbl	(%rax), %ebp
	orl	%edx, %ebp
	movzwl	%bp, %edx
	cmpl	$2, %edx
	je	.LBB10_135
.LBB10_87:                              # %_ZN8NArchive4NZip18CStrongCryptoField17ParseFromSubBlockERKNS0_14CExtraSubBlockE.exit.thread.i
                                        #   in Loop: Header=BB10_84 Depth=1
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB10_84
.LBB10_88:                              # %.thread178
.Ltmp80:
	movq	%rsp, %rdi
	movl	$.L.str.13, %esi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp81:
	jmp	.LBB10_103
.LBB10_89:                              # %..thread_crit_edge
	movl	132(%rbx), %eax
.LBB10_90:                              # %.thread
	testl	%eax, %eax
	jle	.LBB10_94
# BB#91:                                # %.lr.ph.i88
	movq	136(%rbx), %rcx
	cltq
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_92:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rdi
	movzwl	(%rdi), %esi
	cmpl	$21589, %esi            # imm = 0x5455
	je	.LBB10_99
# BB#93:                                #   in Loop: Header=BB10_92 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB10_92
	jmp	.LBB10_94
.LBB10_95:
.Ltmp138:
	leaq	76(%rsp), %rdx
	xorl	%esi, %esi
	callq	_ZNK8NArchive4NZip14CExtraSubBlock15ExtractUnixTimeEiRj
.Ltmp139:
# BB#96:
	testb	%al, %al
	je	.LBB10_73
# BB#97:
	movl	76(%rsp), %edi
.Ltmp145:
	leaq	32(%rsp), %rsi
	callq	_ZN8NWindows5NTime18UnixTimeToFileTimeEjR9_FILETIME
.Ltmp146:
	jmp	.LBB10_98
.LBB10_73:                              # %.thread174
	movl	8(%rbx), %edi
.Ltmp140:
	movq	%rsp, %rsi
	callq	_ZN8NWindows5NTime17DosTimeToFileTimeEjR9_FILETIME
.Ltmp141:
# BB#74:
	testb	%al, %al
	je	.LBB10_77
# BB#75:
.Ltmp142:
	movq	%rsp, %rdi
	leaq	32(%rsp), %rsi
	callq	LocalFileTimeToFileTime
.Ltmp143:
# BB#76:
	testl	%eax, %eax
	jne	.LBB10_98
.LBB10_77:
	movq	$0, 32(%rsp)
.LBB10_98:
.Ltmp148:
	leaq	16(%rsp), %rdi
	leaq	32(%rsp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp149:
	jmp	.LBB10_122
.LBB10_99:
.Ltmp163:
	movq	%rsp, %rdx
	xorl	%esi, %esi
	callq	_ZNK8NArchive4NZip14CExtraSubBlock15ExtractUnixTimeEiRj
.Ltmp164:
# BB#100:
	testb	%al, %al
	je	.LBB10_94
# BB#101:
.Ltmp167:
	leaq	16(%rsp), %rdi
	movl	$1, %esi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp168:
	jmp	.LBB10_122
.LBB10_94:                              # %.thread162
.Ltmp165:
	leaq	16(%rsp), %rdi
	movl	$2, %esi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp166:
	jmp	.LBB10_122
.LBB10_102:
.Ltmp75:
	movq	%rsp, %rdi
	movl	$.L.str.14, %esi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp76:
.LBB10_103:                             # %.thread179
.Ltmp83:
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp84:
.LBB10_104:                             # %_ZNK8NArchive4NZip11CExtraBlock13GetWzAesFieldERNS0_16CWzAesExtraFieldE.exit.thread
	movzwl	%r12w, %edi
	cmpl	$10, %edi
	ja	.LBB10_106
# BB#105:
	movq	_ZN8NArchive4NZipL8kMethodsE(,%rdi,8), %rsi
.Ltmp112:
	movq	%rsp, %rdi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp113:
	jmp	.LBB10_119
.LBB10_106:
	movswl	%di, %eax
	cmpl	$95, %eax
	jle	.LBB10_111
# BB#107:
	movzwl	%di, %eax
	cmpl	$96, %eax
	je	.LBB10_126
# BB#108:
	cmpl	$97, %eax
	je	.LBB10_127
# BB#109:
	cmpl	$98, %eax
	jne	.LBB10_117
# BB#110:
.Ltmp95:
	movq	%rsp, %rdi
	movl	$.L.str.30, %esi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp96:
	jmp	.LBB10_119
.LBB10_111:
	movzwl	%di, %eax
	cmpl	$12, %eax
	je	.LBB10_128
# BB#112:
	cmpl	$14, %eax
	jne	.LBB10_117
# BB#113:
.Ltmp103:
	movq	%rsp, %rdi
	movl	$.L.str.26, %esi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp104:
# BB#114:
	testb	$2, 2(%rbx)
	je	.LBB10_119
# BB#115:
.Ltmp105:
	movq	%rsp, %rdi
	movl	$.L.str, %esi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp106:
	jmp	.LBB10_119
.LBB10_116:
	movl	$99, %edi
.LBB10_117:                             # %.thread217
.Ltmp107:
	leaq	32(%rsp), %rsi
	movl	$10, %edx
	callq	_Z21ConvertUInt64ToStringyPcj
.Ltmp108:
# BB#118:
.Ltmp109:
	movq	%rsp, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp110:
.LBB10_119:
	movq	(%rsp), %rsi
.Ltmp114:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKc
.Ltmp115:
# BB#120:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_122
.LBB10_121:
	callq	_ZdaPv
.LBB10_122:
.Ltmp189:
	leaq	16(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp190:
# BB#123:
.Ltmp195:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp196:
# BB#124:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit84
	xorl	%eax, %eax
.LBB10_125:
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_126:
.Ltmp99:
	movq	%rsp, %rdi
	movl	$.L.str.28, %esi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp100:
	jmp	.LBB10_119
.LBB10_127:
.Ltmp97:
	movq	%rsp, %rdi
	movl	$.L.str.29, %esi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp98:
	jmp	.LBB10_119
.LBB10_128:
.Ltmp101:
	movq	%rsp, %rdi
	movl	$.L.str.27, %esi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp102:
	jmp	.LBB10_119
.LBB10_129:
	movzbl	1(%rsi), %eax
	shll	$8, %eax
	movzbl	(%rsi), %ecx
	orl	%eax, %ecx
	movzwl	%cx, %eax
	cmpl	$1, %eax
	jne	.LBB10_122
# BB#130:                               # %._ZNK8NArchive4NZip5CItem10IsThereCrcEv.exit.thread_crit_edge
	addq	$12, %rbx
	movq	%rbx, %rbp
	jmp	.LBB10_58
.LBB10_131:
	movzbl	4(%rsi), %r15d
	movzbl	5(%rsi), %r12d
.Ltmp85:
	movq	%rsp, %rdi
	movl	$45, %esi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp86:
# BB#132:
	shlq	$6, %r15
	addq	$64, %r15
.Ltmp88:
	leaq	32(%rsp), %rsi
	movl	$10, %edx
	movq	%r15, %rdi
	callq	_Z21ConvertUInt64ToStringyPcj
.Ltmp89:
# BB#133:
.Ltmp90:
	movq	%rsp, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp91:
# BB#134:
.Ltmp92:
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp93:
	jmp	.LBB10_104
.LBB10_135:                             # %_ZNK8NArchive4NZip11CExtraBlock20GetStrongCryptoFieldERNS0_18CStrongCryptoFieldE.exit.preheader
	movzbl	3(%rax), %ecx
	shll	$8, %ecx
	movzbl	2(%rax), %eax
	orl	%ecx, %eax
	movswl	%ax, %ecx
	cmpl	$26369, %ecx            # imm = 0x6701
	jg	.LBB10_139
# BB#136:                               # %_ZNK8NArchive4NZip11CExtraBlock20GetStrongCryptoFieldERNS0_18CStrongCryptoFieldE.exit.preheader
	addl	$-26113, %eax           # imm = 0x99FF
	movzwl	%ax, %ecx
	cmpl	$15, %ecx
	ja	.LBB10_88
# BB#137:                               # %_ZNK8NArchive4NZip11CExtraBlock20GetStrongCryptoFieldERNS0_18CStrongCryptoFieldE.exit.preheader
	xorl	%eax, %eax
	jmpq	*.LJTI10_1(,%rcx,8)
.LBB10_138:                             # %.fold.split
	movl	$1, %eax
	jmp	.LBB10_153
.LBB10_139:                             # %_ZNK8NArchive4NZip11CExtraBlock20GetStrongCryptoFieldERNS0_18CStrongCryptoFieldE.exit.preheader
	movzwl	%ax, %eax
	cmpl	$26400, %ecx            # imm = 0x6720
	jg	.LBB10_143
# BB#140:                               # %_ZNK8NArchive4NZip11CExtraBlock20GetStrongCryptoFieldERNS0_18CStrongCryptoFieldE.exit.preheader
	cmpl	$26370, %eax            # imm = 0x6702
	je	.LBB10_151
# BB#141:                               # %_ZNK8NArchive4NZip11CExtraBlock20GetStrongCryptoFieldERNS0_18CStrongCryptoFieldE.exit.preheader
	cmpl	$26400, %eax            # imm = 0x6720
	jne	.LBB10_88
# BB#142:                               # %.fold.split247
	movl	$8, %eax
	jmp	.LBB10_153
.LBB10_143:                             # %_ZNK8NArchive4NZip11CExtraBlock20GetStrongCryptoFieldERNS0_18CStrongCryptoFieldE.exit.preheader
	cmpl	$26401, %eax            # imm = 0x6721
	je	.LBB10_152
# BB#144:                               # %_ZNK8NArchive4NZip11CExtraBlock20GetStrongCryptoFieldERNS0_18CStrongCryptoFieldE.exit.preheader
	cmpl	$26625, %eax            # imm = 0x6801
	jne	.LBB10_88
# BB#145:                               # %.fold.split249
	movl	$10, %eax
	jmp	.LBB10_153
.LBB10_146:                             # %.fold.split244
	movl	$5, %eax
	jmp	.LBB10_153
.LBB10_147:                             # %.fold.split243
	movl	$4, %eax
	jmp	.LBB10_153
.LBB10_148:                             # %.fold.split241
	movl	$2, %eax
	jmp	.LBB10_153
.LBB10_149:                             # %.fold.split242
	movl	$3, %eax
	jmp	.LBB10_153
.LBB10_150:                             # %.fold.split245
	movl	$6, %eax
	jmp	.LBB10_153
.LBB10_151:                             # %.fold.split246
	movl	$7, %eax
	jmp	.LBB10_153
.LBB10_152:                             # %.fold.split248
	movl	$9, %eax
.LBB10_153:
	shlq	$4, %rax
	movq	_ZN8NArchive4NZipL19g_StrongCryptoPairsE+8(%rax), %rsi
.Ltmp77:
	movq	%rsp, %rdi
	callq	_ZN11CStringBaseIcEpLEPKc
.Ltmp78:
	jmp	.LBB10_103
.LBB10_154:
.Ltmp79:
	jmp	.LBB10_179
.LBB10_155:
.Ltmp87:
	jmp	.LBB10_179
.LBB10_156:
.Ltmp94:
	jmp	.LBB10_179
.LBB10_157:
.Ltmp82:
	jmp	.LBB10_179
.LBB10_158:
.Ltmp147:
	jmp	.LBB10_183
.LBB10_159:
.Ltmp155:
	jmp	.LBB10_183
.LBB10_160:
.Ltmp160:
	jmp	.LBB10_183
.LBB10_161:
.Ltmp111:
	jmp	.LBB10_179
.LBB10_162:
.Ltmp188:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_167
# BB#163:
	callq	_ZdaPv
	jmp	.LBB10_167
.LBB10_164:
.Ltmp129:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_170
# BB#165:
	callq	_ZdaPv
	jmp	.LBB10_170
.LBB10_166:
.Ltmp185:
	movq	%rdx, %r14
	movq	%rax, %rbx
.LBB10_167:                             # %_ZN11CStringBaseIwED2Ev.exit86
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_184
# BB#168:
	callq	_ZdaPv
	jmp	.LBB10_184
.LBB10_169:
.Ltmp126:
	movq	%rdx, %r14
	movq	%rax, %rbx
.LBB10_170:                             # %_ZN11CStringBaseIwED2Ev.exit130
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_184
# BB#171:
	callq	_ZdaPv
	jmp	.LBB10_184
.LBB10_172:
.Ltmp182:
	jmp	.LBB10_183
.LBB10_173:
.Ltmp123:
	jmp	.LBB10_183
.LBB10_174:
.Ltmp74:
	jmp	.LBB10_183
.LBB10_175:
.Ltmp144:
	jmp	.LBB10_183
.LBB10_176:
.Ltmp150:
	jmp	.LBB10_183
.LBB10_177:
.Ltmp171:
	jmp	.LBB10_183
.LBB10_178:
.Ltmp116:
.LBB10_179:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_184
# BB#180:
	callq	_ZdaPv
	jmp	.LBB10_184
.LBB10_181:
.Ltmp197:
	movq	%rdx, %r14
	movq	%rax, %rbx
	jmp	.LBB10_185
.LBB10_182:
.Ltmp191:
.LBB10_183:
	movq	%rdx, %r14
	movq	%rax, %rbx
.LBB10_184:
.Ltmp192:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp193:
.LBB10_185:                             # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB10_187
# BB#186:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB10_125
.LBB10_187:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp198:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp199:
# BB#188:
.LBB10_189:
.Ltmp200:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB10_190:
.Ltmp194:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end10:
	.size	_ZN8NArchive4NZip8CHandler11GetPropertyEjjP14tagPROPVARIANT, .Lfunc_end10-_ZN8NArchive4NZip8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI10_0:
	.quad	.LBB10_2
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_30
	.quad	.LBB10_24
	.quad	.LBB10_44
	.quad	.LBB10_45
	.quad	.LBB10_25
	.quad	.LBB10_32
	.quad	.LBB10_36
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_59
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_47
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_9
	.quad	.LBB10_29
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_19
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_8
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_122
	.quad	.LBB10_40
.LJTI10_1:
	.quad	.LBB10_153
	.quad	.LBB10_138
	.quad	.LBB10_148
	.quad	.LBB10_88
	.quad	.LBB10_88
	.quad	.LBB10_88
	.quad	.LBB10_88
	.quad	.LBB10_88
	.quad	.LBB10_149
	.quad	.LBB10_88
	.quad	.LBB10_88
	.quad	.LBB10_88
	.quad	.LBB10_88
	.quad	.LBB10_147
	.quad	.LBB10_146
	.quad	.LBB10_150
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\242\203\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\223\003"              # Call site table length
	.long	.Ltmp180-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp181-.Ltmp180       #   Call between .Ltmp180 and .Ltmp181
	.long	.Ltmp182-.Lfunc_begin4  #     jumps to .Ltmp182
	.byte	3                       #   On action: 2
	.long	.Ltmp183-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp184-.Ltmp183       #   Call between .Ltmp183 and .Ltmp184
	.long	.Ltmp185-.Lfunc_begin4  #     jumps to .Ltmp185
	.byte	3                       #   On action: 2
	.long	.Ltmp186-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp187-.Ltmp186       #   Call between .Ltmp186 and .Ltmp187
	.long	.Ltmp188-.Lfunc_begin4  #     jumps to .Ltmp188
	.byte	3                       #   On action: 2
	.long	.Ltmp68-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp191-.Lfunc_begin4  #     jumps to .Ltmp191
	.byte	3                       #   On action: 2
	.long	.Ltmp72-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin4   #     jumps to .Ltmp74
	.byte	3                       #   On action: 2
	.long	.Ltmp121-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp123-.Lfunc_begin4  #     jumps to .Ltmp123
	.byte	3                       #   On action: 2
	.long	.Ltmp124-.Lfunc_begin4  # >> Call Site 7 <<
	.long	.Ltmp125-.Ltmp124       #   Call between .Ltmp124 and .Ltmp125
	.long	.Ltmp126-.Lfunc_begin4  #     jumps to .Ltmp126
	.byte	3                       #   On action: 2
	.long	.Ltmp127-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Ltmp128-.Ltmp127       #   Call between .Ltmp127 and .Ltmp128
	.long	.Ltmp129-.Lfunc_begin4  #     jumps to .Ltmp129
	.byte	3                       #   On action: 2
	.long	.Ltmp174-.Lfunc_begin4  # >> Call Site 9 <<
	.long	.Ltmp131-.Ltmp174       #   Call between .Ltmp174 and .Ltmp131
	.long	.Ltmp191-.Lfunc_begin4  #     jumps to .Ltmp191
	.byte	3                       #   On action: 2
	.long	.Ltmp156-.Lfunc_begin4  # >> Call Site 10 <<
	.long	.Ltmp159-.Ltmp156       #   Call between .Ltmp156 and .Ltmp159
	.long	.Ltmp160-.Lfunc_begin4  #     jumps to .Ltmp160
	.byte	3                       #   On action: 2
	.long	.Ltmp151-.Lfunc_begin4  # >> Call Site 11 <<
	.long	.Ltmp154-.Ltmp151       #   Call between .Ltmp151 and .Ltmp154
	.long	.Ltmp155-.Lfunc_begin4  #     jumps to .Ltmp155
	.byte	3                       #   On action: 2
	.long	.Ltmp136-.Lfunc_begin4  # >> Call Site 12 <<
	.long	.Ltmp137-.Ltmp136       #   Call between .Ltmp136 and .Ltmp137
	.long	.Ltmp150-.Lfunc_begin4  #     jumps to .Ltmp150
	.byte	3                       #   On action: 2
	.long	.Ltmp161-.Lfunc_begin4  # >> Call Site 13 <<
	.long	.Ltmp170-.Ltmp161       #   Call between .Ltmp161 and .Ltmp170
	.long	.Ltmp171-.Lfunc_begin4  #     jumps to .Ltmp171
	.byte	3                       #   On action: 2
	.long	.Ltmp80-.Lfunc_begin4   # >> Call Site 14 <<
	.long	.Ltmp81-.Ltmp80         #   Call between .Ltmp80 and .Ltmp81
	.long	.Ltmp82-.Lfunc_begin4   #     jumps to .Ltmp82
	.byte	3                       #   On action: 2
	.long	.Ltmp138-.Lfunc_begin4  # >> Call Site 15 <<
	.long	.Ltmp146-.Ltmp138       #   Call between .Ltmp138 and .Ltmp146
	.long	.Ltmp147-.Lfunc_begin4  #     jumps to .Ltmp147
	.byte	3                       #   On action: 2
	.long	.Ltmp140-.Lfunc_begin4  # >> Call Site 16 <<
	.long	.Ltmp143-.Ltmp140       #   Call between .Ltmp140 and .Ltmp143
	.long	.Ltmp144-.Lfunc_begin4  #     jumps to .Ltmp144
	.byte	3                       #   On action: 2
	.long	.Ltmp148-.Lfunc_begin4  # >> Call Site 17 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin4  #     jumps to .Ltmp150
	.byte	3                       #   On action: 2
	.long	.Ltmp163-.Lfunc_begin4  # >> Call Site 18 <<
	.long	.Ltmp166-.Ltmp163       #   Call between .Ltmp163 and .Ltmp166
	.long	.Ltmp171-.Lfunc_begin4  #     jumps to .Ltmp171
	.byte	3                       #   On action: 2
	.long	.Ltmp75-.Lfunc_begin4   # >> Call Site 19 <<
	.long	.Ltmp106-.Ltmp75        #   Call between .Ltmp75 and .Ltmp106
	.long	.Ltmp116-.Lfunc_begin4  #     jumps to .Ltmp116
	.byte	3                       #   On action: 2
	.long	.Ltmp107-.Lfunc_begin4  # >> Call Site 20 <<
	.long	.Ltmp110-.Ltmp107       #   Call between .Ltmp107 and .Ltmp110
	.long	.Ltmp111-.Lfunc_begin4  #     jumps to .Ltmp111
	.byte	3                       #   On action: 2
	.long	.Ltmp114-.Lfunc_begin4  # >> Call Site 21 <<
	.long	.Ltmp115-.Ltmp114       #   Call between .Ltmp114 and .Ltmp115
	.long	.Ltmp116-.Lfunc_begin4  #     jumps to .Ltmp116
	.byte	3                       #   On action: 2
	.long	.Ltmp189-.Lfunc_begin4  # >> Call Site 22 <<
	.long	.Ltmp190-.Ltmp189       #   Call between .Ltmp189 and .Ltmp190
	.long	.Ltmp191-.Lfunc_begin4  #     jumps to .Ltmp191
	.byte	3                       #   On action: 2
	.long	.Ltmp195-.Lfunc_begin4  # >> Call Site 23 <<
	.long	.Ltmp196-.Ltmp195       #   Call between .Ltmp195 and .Ltmp196
	.long	.Ltmp197-.Lfunc_begin4  #     jumps to .Ltmp197
	.byte	3                       #   On action: 2
	.long	.Ltmp99-.Lfunc_begin4   # >> Call Site 24 <<
	.long	.Ltmp102-.Ltmp99        #   Call between .Ltmp99 and .Ltmp102
	.long	.Ltmp116-.Lfunc_begin4  #     jumps to .Ltmp116
	.byte	3                       #   On action: 2
	.long	.Ltmp85-.Lfunc_begin4   # >> Call Site 25 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin4   #     jumps to .Ltmp87
	.byte	3                       #   On action: 2
	.long	.Ltmp88-.Lfunc_begin4   # >> Call Site 26 <<
	.long	.Ltmp93-.Ltmp88         #   Call between .Ltmp88 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin4   #     jumps to .Ltmp94
	.byte	3                       #   On action: 2
	.long	.Ltmp77-.Lfunc_begin4   # >> Call Site 27 <<
	.long	.Ltmp78-.Ltmp77         #   Call between .Ltmp77 and .Ltmp78
	.long	.Ltmp79-.Lfunc_begin4   #     jumps to .Ltmp79
	.byte	3                       #   On action: 2
	.long	.Ltmp192-.Lfunc_begin4  # >> Call Site 28 <<
	.long	.Ltmp193-.Ltmp192       #   Call between .Ltmp192 and .Ltmp193
	.long	.Ltmp194-.Lfunc_begin4  #     jumps to .Ltmp194
	.byte	1                       #   On action: 1
	.long	.Ltmp193-.Lfunc_begin4  # >> Call Site 29 <<
	.long	.Ltmp198-.Ltmp193       #   Call between .Ltmp193 and .Ltmp198
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp198-.Lfunc_begin4  # >> Call Site 30 <<
	.long	.Ltmp199-.Ltmp198       #   Call between .Ltmp198 and .Ltmp199
	.long	.Ltmp200-.Lfunc_begin4  #     jumps to .Ltmp200
	.byte	0                       #   On action: cleanup
	.long	.Ltmp199-.Lfunc_begin4  # >> Call Site 31 <<
	.long	.Lfunc_end10-.Ltmp199   #   Call between .Ltmp199 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK8NArchive4NZip10CLocalItem16GetUnicodeStringERK11CStringBaseIcE,"axG",@progbits,_ZNK8NArchive4NZip10CLocalItem16GetUnicodeStringERK11CStringBaseIcE,comdat
	.weak	_ZNK8NArchive4NZip10CLocalItem16GetUnicodeStringERK11CStringBaseIcE
	.p2align	4, 0x90
	.type	_ZNK8NArchive4NZip10CLocalItem16GetUnicodeStringERK11CStringBaseIcE,@function
_ZNK8NArchive4NZip10CLocalItem16GetUnicodeStringERK11CStringBaseIcE: # @_ZNK8NArchive4NZip10CLocalItem16GetUnicodeStringERK11CStringBaseIcE
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 80
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	$4, 12(%r14)
	testb	$8, 3(%rbx)
	jne	.LBB11_2
# BB#1:                                 # %._crit_edge12
	leaq	8(%r14), %r13
	jmp	.LBB11_6
.LBB11_2:
.Ltmp201:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	_Z20ConvertUTF8ToUnicodeRK11CStringBaseIcERS_IwE
.Ltmp202:
# BB#3:
	leaq	8(%r14), %r13
	testb	%al, %al
	je	.LBB11_4
# BB#5:
	cmpl	$0, (%r13)
	jne	.LBB11_20
	jmp	.LBB11_6
.LBB11_4:
	movl	$0, 8(%r14)
	movq	(%r14), %rax
	movl	$0, (%rax)
.LBB11_6:                               # %.thread
.Ltmp204:
	leaq	8(%rsp), %rbx
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp205:
# BB#7:
	cmpq	%r14, %rbx
	je	.LBB11_8
# BB#9:
	movl	$0, (%r13)
	movq	(%r14), %rbx
	movl	$0, (%rbx)
	movslq	16(%rsp), %r15
	incq	%r15
	movl	12(%r14), %ebp
	cmpl	%ebp, %r15d
	je	.LBB11_15
# BB#10:
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp207:
	callq	_Znam
	movq	%rax, %r12
.Ltmp208:
# BB#11:                                # %.noexc
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB11_14
# BB#12:                                # %.noexc
	testl	%ebp, %ebp
	jle	.LBB11_14
# BB#13:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	(%r13), %rax
.LBB11_14:                              # %._crit_edge16.i.i
	movq	%r12, (%r14)
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
	movq	%r12, %rbx
.LBB11_15:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_16:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB11_16
# BB#17:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	16(%rsp), %eax
	movl	%eax, (%r13)
	testq	%rdi, %rdi
	jne	.LBB11_19
	jmp	.LBB11_20
.LBB11_8:                               # %._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB11_20
.LBB11_19:
	callq	_ZdaPv
.LBB11_20:
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_22:
.Ltmp209:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_26
# BB#23:
	callq	_ZdaPv
	jmp	.LBB11_26
.LBB11_24:
.Ltmp203:
	jmp	.LBB11_25
.LBB11_21:
.Ltmp206:
.LBB11_25:
	movq	%rax, %rbx
.LBB11_26:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB11_28
# BB#27:
	callq	_ZdaPv
.LBB11_28:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZNK8NArchive4NZip10CLocalItem16GetUnicodeStringERK11CStringBaseIcE, .Lfunc_end11-_ZNK8NArchive4NZip10CLocalItem16GetUnicodeStringERK11CStringBaseIcE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp201-.Lfunc_begin5  #   Call between .Lfunc_begin5 and .Ltmp201
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp201-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp202-.Ltmp201       #   Call between .Ltmp201 and .Ltmp202
	.long	.Ltmp203-.Lfunc_begin5  #     jumps to .Ltmp203
	.byte	0                       #   On action: cleanup
	.long	.Ltmp204-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp205-.Ltmp204       #   Call between .Ltmp204 and .Ltmp205
	.long	.Ltmp206-.Lfunc_begin5  #     jumps to .Ltmp206
	.byte	0                       #   On action: cleanup
	.long	.Ltmp207-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp208-.Ltmp207       #   Call between .Ltmp207 and .Ltmp208
	.long	.Ltmp209-.Lfunc_begin5  #     jumps to .Ltmp209
	.byte	0                       #   On action: cleanup
	.long	.Ltmp208-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Lfunc_end11-.Ltmp208   #   Call between .Ltmp208 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIcEpLEc,"axG",@progbits,_ZN11CStringBaseIcEpLEc,comdat
	.weak	_ZN11CStringBaseIcEpLEc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEc,@function
_ZN11CStringBaseIcEpLEc:                # @_ZN11CStringBaseIcEpLEc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi59:
	.cfi_def_cfa_offset 64
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB12_28
# BB#1:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB12_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB12_3:                               # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rsi,%rbx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB12_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB12_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB12_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB12_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB12_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB12_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB12_17
.LBB12_7:
	xorl	%ecx, %ecx
.LBB12_8:                               # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB12_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB12_10:                              # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB12_10
.LBB12_11:                              # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB12_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB12_13:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB12_13
	jmp	.LBB12_26
.LBB12_25:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB12_27
.LBB12_26:                              # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB12_27:
	movq	%r12, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%r13)
.LBB12_28:                              # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	movb	%r14b, (%rax,%rcx)
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movb	$0, 1(%rax,%rcx)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_17:                              # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB12_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r12,%rbx)
	movups	%xmm1, 16(%r12,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB12_20
	jmp	.LBB12_21
.LBB12_18:
	xorl	%ebx, %ebx
.LBB12_21:                              # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB12_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB12_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB12_23
.LBB12_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB12_8
	jmp	.LBB12_26
.Lfunc_end12:
	.size	_ZN11CStringBaseIcEpLEc, .Lfunc_end12-_ZN11CStringBaseIcEpLEc
	.cfi_endproc

	.section	.text._ZN11CStringBaseIcEpLEPKc,"axG",@progbits,_ZN11CStringBaseIcEpLEPKc,comdat
	.weak	_ZN11CStringBaseIcEpLEPKc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEPKc,@function
_ZN11CStringBaseIcEpLEPKc:              # @_ZN11CStringBaseIcEpLEPKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi72:
	.cfi_def_cfa_offset 64
.Lcfi73:
	.cfi_offset %rbx, -56
.Lcfi74:
	.cfi_offset %r12, -48
.Lcfi75:
	.cfi_offset %r13, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$-1, %r12d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB13_1:                               # =>This Inner Loop Header: Depth=1
	incl	%r12d
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB13_1
# BB#2:                                 # %_Z11MyStringLenIcEiPKT_.exit
	movl	8(%r14), %ebp
	movl	12(%r14), %r13d
	movl	%r13d, %eax
	subl	%ebp, %eax
	cmpl	%r12d, %eax
	jg	.LBB13_30
# BB#3:
	decl	%eax
	cmpl	$8, %r13d
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %r13d
	jl	.LBB13_5
# BB#4:                                 # %select.true.sink
	movl	%r13d, %ecx
	shrl	$31, %ecx
	addl	%r13d, %ecx
	sarl	%ecx
.LBB13_5:                               # %select.end
	addl	%ecx, %eax
	movl	%ebp, %edx
	subl	%r13d, %edx
	cmpl	%r12d, %eax
	leal	1(%rdx,%r12), %eax
	cmovgel	%ecx, %eax
	leal	1(%rax,%r13), %ecx
	cmpl	%r13d, %ecx
	je	.LBB13_30
# BB#6:
	addl	%r13d, %eax
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movslq	%ecx, %rcx
	cmpl	$-1, %eax
	movq	$-1, %rdi
	cmovgeq	%rcx, %rdi
	callq	_Znam
	movq	%rax, %r15
	testl	%r13d, %r13d
	jle	.LBB13_29
# BB#7:                                 # %.preheader.i.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB13_27
# BB#8:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %r9
	cmpl	$31, %ebp
	jbe	.LBB13_9
# BB#16:                                # %min.iters.checked
	movq	%r9, %rcx
	andq	$-32, %rcx
	je	.LBB13_9
# BB#17:                                # %vector.memcheck
	leaq	(%rdi,%r9), %rdx
	cmpq	%rdx, %r15
	jae	.LBB13_19
# BB#18:                                # %vector.memcheck
	leaq	(%r15,%r9), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB13_19
.LBB13_9:
	xorl	%ecx, %ecx
.LBB13_10:                              # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%r9), %rsi
	subq	%rcx, %rsi
	andq	$7, %rbp
	je	.LBB13_13
# BB#11:                                # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB13_12:                              # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r15,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB13_12
.LBB13_13:                              # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB13_28
# BB#14:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %r9
	leaq	7(%r15,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB13_15:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r9
	jne	.LBB13_15
	jmp	.LBB13_28
.LBB13_27:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB13_29
.LBB13_28:                              # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB13_29:
	movq	%r15, (%r14)
	movslq	%ebp, %rax
	movb	$0, (%r15,%rax)
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 12(%r14)
.LBB13_30:                              # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movslq	%ebp, %rax
	addq	(%r14), %rax
	.p2align	4, 0x90
.LBB13_31:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %ecx
	incq	%rbx
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB13_31
# BB#32:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit
	addl	%r12d, 8(%r14)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_19:                              # %vector.body.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB13_20
# BB#21:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB13_22:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%r15,%rdx)
	movups	%xmm1, 16(%r15,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB13_22
	jmp	.LBB13_23
.LBB13_20:
	xorl	%edx, %edx
.LBB13_23:                              # %vector.body.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB13_26
# BB#24:                                # %vector.body.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%r15,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB13_25:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB13_25
.LBB13_26:                              # %middle.block
	cmpq	%rcx, %r9
	jne	.LBB13_10
	jmp	.LBB13_28
.Lfunc_end13:
	.size	_ZN11CStringBaseIcEpLEPKc, .Lfunc_end13-_ZN11CStringBaseIcEpLEPKc
	.cfi_endproc

	.text
	.globl	_ZN8NArchive4NZip12CProgressImp8SetTotalEy
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CProgressImp8SetTotalEy,@function
_ZN8NArchive4NZip12CProgressImp8SetTotalEy: # @_ZN8NArchive4NZip12CProgressImp8SetTotalEy
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi79:
	.cfi_def_cfa_offset 16
	movq	%rsi, (%rsp)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB14_1
# BB#2:
	movq	(%rdi), %rax
	movq	%rsp, %rsi
	xorl	%edx, %edx
	callq	*40(%rax)
	popq	%rcx
	retq
.LBB14_1:
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end14:
	.size	_ZN8NArchive4NZip12CProgressImp8SetTotalEy, .Lfunc_end14-_ZN8NArchive4NZip12CProgressImp8SetTotalEy
	.cfi_endproc

	.globl	_ZN8NArchive4NZip12CProgressImp12SetCompletedEy
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CProgressImp12SetCompletedEy,@function
_ZN8NArchive4NZip12CProgressImp12SetCompletedEy: # @_ZN8NArchive4NZip12CProgressImp12SetCompletedEy
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi80:
	.cfi_def_cfa_offset 16
	movq	%rsi, (%rsp)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB15_1
# BB#2:
	movq	(%rdi), %rax
	movq	%rsp, %rsi
	xorl	%edx, %edx
	callq	*48(%rax)
	popq	%rcx
	retq
.LBB15_1:
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end15:
	.size	_ZN8NArchive4NZip12CProgressImp12SetCompletedEy, .Lfunc_end15-_ZN8NArchive4NZip12CProgressImp12SetCompletedEy
	.cfi_endproc

	.globl	_ZN8NArchive4NZip8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback,@function
_ZN8NArchive4NZip8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback: # @_ZN8NArchive4NZip8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi84:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi85:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi87:
	.cfi_def_cfa_offset 80
.Lcfi88:
	.cfi_offset %rbx, -56
.Lcfi89:
	.cfi_offset %r12, -48
.Lcfi90:
	.cfi_offset %r13, -40
.Lcfi91:
	.cfi_offset %r14, -32
.Lcfi92:
	.cfi_offset %r15, -24
.Lcfi93:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	(%r15), %rax
.Ltmp210:
	callq	*48(%rax)
.Ltmp211:
# BB#1:
	movq	(%rbx), %rax
.Ltmp212:
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp213:
# BB#2:
	testl	%ebp, %ebp
	jne	.LBB16_26
# BB#3:
	leaq	64(%r15), %r12
.Ltmp214:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	_ZN8NArchive4NZip10CInArchive4OpenEP9IInStreamPKy
	movl	%eax, %ebp
.Ltmp215:
# BB#4:
	testl	%ebp, %ebp
	jne	.LBB16_26
# BB#5:
	movq	$_ZTVN8NArchive4NZip12CProgressImpE+16, 8(%rsp)
	movq	%r14, 16(%rsp)
	testq	%r14, %r14
	je	.LBB16_7
# BB#6:
	movq	(%r14), %rax
.Ltmp216:
	movq	%r14, %rdi
	callq	*8(%rax)
.Ltmp217:
.LBB16_7:                               # %_ZN8NArchive4NZip12CProgressImpC2EP20IArchiveOpenCallback.exit
	leaq	32(%r15), %rsi
.Ltmp219:
	leaq	8(%rsp), %rdx
	movq	%r12, %rdi
	callq	_ZN8NArchive4NZip10CInArchive11ReadHeadersER13CObjectVectorINS0_7CItemExEEPNS0_13CProgressVirtE
	movl	%eax, %ebp
.Ltmp220:
# BB#8:
	movq	$_ZTVN8NArchive4NZip12CProgressImpE+16, 8(%rsp)
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_26
# BB#9:
	movq	(%rdi), %rax
.Ltmp224:
	callq	*16(%rax)
.Ltmp225:
.LBB16_26:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_12:
.Ltmp226:
	jmp	.LBB16_13
.LBB16_10:
.Ltmp221:
	movq	%rdx, %r14
	movq	%rax, %rbp
	movq	$_ZTVN8NArchive4NZip12CProgressImpE+16, 8(%rsp)
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_14
# BB#11:
	movq	(%rdi), %rax
.Ltmp222:
	callq	*16(%rax)
.Ltmp223:
	jmp	.LBB16_14
.LBB16_29:
.Ltmp218:
.LBB16_13:
	movq	%rdx, %r14
	movq	%rax, %rbp
.LBB16_14:
	movq	%rbp, %rdi
	callq	__cxa_begin_catch
	movq	(%r15), %rax
	movq	48(%rax), %rax
	cmpl	$2, %r14d
	jne	.LBB16_17
# BB#15:
.Ltmp234:
	movq	%r15, %rdi
	callq	*%rax
.Ltmp235:
# BB#16:
	movl	$1, %ebp
.Ltmp240:
	callq	__cxa_end_catch
.Ltmp241:
	jmp	.LBB16_26
.LBB16_17:
.Ltmp227:
	movq	%r15, %rdi
	callq	*%rax
.Ltmp228:
# BB#18:
.Ltmp229:
	callq	__cxa_rethrow
.Ltmp230:
.LBB16_28:
.LBB16_19:
.Ltmp231:
	movq	%rdx, %rbx
	movq	%rax, %rbp
.Ltmp232:
	callq	__cxa_end_catch
.Ltmp233:
	jmp	.LBB16_22
.LBB16_21:
.Ltmp242:
	movq	%rdx, %rbx
	movq	%rax, %rbp
	jmp	.LBB16_22
.LBB16_20:
.Ltmp236:
	movq	%rdx, %rbx
	movq	%rax, %rbp
.Ltmp237:
	callq	__cxa_end_catch
.Ltmp238:
.LBB16_22:
	movq	%rbp, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbp
	cmpl	$3, %ebx
	je	.LBB16_23
# BB#25:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB16_26
.LBB16_23:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbp, (%rax)
.Ltmp243:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp244:
	jmp	.LBB16_28
.LBB16_24:
.Ltmp245:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB16_27:
.Ltmp239:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN8NArchive4NZip8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback, .Lfunc_end16-_ZN8NArchive4NZip8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\261\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Ltmp210-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp217-.Ltmp210       #   Call between .Ltmp210 and .Ltmp217
	.long	.Ltmp218-.Lfunc_begin6  #     jumps to .Ltmp218
	.byte	3                       #   On action: 2
	.long	.Ltmp219-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp220-.Ltmp219       #   Call between .Ltmp219 and .Ltmp220
	.long	.Ltmp221-.Lfunc_begin6  #     jumps to .Ltmp221
	.byte	3                       #   On action: 2
	.long	.Ltmp224-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp225-.Ltmp224       #   Call between .Ltmp224 and .Ltmp225
	.long	.Ltmp226-.Lfunc_begin6  #     jumps to .Ltmp226
	.byte	3                       #   On action: 2
	.long	.Ltmp222-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp223-.Ltmp222       #   Call between .Ltmp222 and .Ltmp223
	.long	.Ltmp239-.Lfunc_begin6  #     jumps to .Ltmp239
	.byte	1                       #   On action: 1
	.long	.Ltmp223-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp234-.Ltmp223       #   Call between .Ltmp223 and .Ltmp234
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp234-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp235-.Ltmp234       #   Call between .Ltmp234 and .Ltmp235
	.long	.Ltmp236-.Lfunc_begin6  #     jumps to .Ltmp236
	.byte	5                       #   On action: 3
	.long	.Ltmp240-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp241-.Ltmp240       #   Call between .Ltmp240 and .Ltmp241
	.long	.Ltmp242-.Lfunc_begin6  #     jumps to .Ltmp242
	.byte	5                       #   On action: 3
	.long	.Ltmp227-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp230-.Ltmp227       #   Call between .Ltmp227 and .Ltmp230
	.long	.Ltmp231-.Lfunc_begin6  #     jumps to .Ltmp231
	.byte	5                       #   On action: 3
	.long	.Ltmp232-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp238-.Ltmp232       #   Call between .Ltmp232 and .Ltmp238
	.long	.Ltmp239-.Lfunc_begin6  #     jumps to .Ltmp239
	.byte	1                       #   On action: 1
	.long	.Ltmp238-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp243-.Ltmp238       #   Call between .Ltmp238 and .Ltmp243
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp243-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp244-.Ltmp243       #   Call between .Ltmp243 and .Ltmp244
	.long	.Ltmp245-.Lfunc_begin6  #     jumps to .Ltmp245
	.byte	0                       #   On action: cleanup
	.long	.Ltmp244-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Lfunc_end16-.Ltmp244   #   Call between .Ltmp244 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
	.byte	3                       # >> Action Record 3 <<
                                        #   Catch TypeInfo 3
	.byte	123                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 3
	.long	_ZTIN8NArchive4NZip19CInArchiveExceptionE # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip8CHandler5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler5CloseEv,@function
_ZN8NArchive4NZip8CHandler5CloseEv:     # @_ZN8NArchive4NZip8CHandler5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 16
.Lcfi95:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	32(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	addq	$64, %rbx
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NZip10CInArchive5CloseEv
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end17:
	.size	_ZN8NArchive4NZip8CHandler5CloseEv, .Lfunc_end17-_ZN8NArchive4NZip8CHandler5CloseEv
	.cfi_endproc

	.globl	_ZN8NArchive4NZip12CLzmaDecoderC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CLzmaDecoderC2Ev,@function
_ZN8NArchive4NZip12CLzmaDecoderC2Ev:    # @_ZN8NArchive4NZip12CLzmaDecoderC2Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 32
.Lcfi99:
	.cfi_offset %rbx, -32
.Lcfi100:
	.cfi_offset %r14, -24
.Lcfi101:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	$0, 8(%r14)
	movq	$_ZTVN8NArchive4NZip12CLzmaDecoderE+16, (%r14)
	movq	$0, 24(%r14)
.Ltmp246:
	movl	$280, %edi              # imm = 0x118
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp247:
# BB#1:
.Ltmp248:
	movq	%rbx, %rdi
	callq	_ZN9NCompress5NLzma8CDecoderC1Ev
.Ltmp249:
# BB#2:
	movq	%rbx, 16(%r14)
	movq	(%rbx), %rax
.Ltmp251:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp252:
# BB#3:                                 # %.noexc3
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB18_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp253:
	callq	*16(%rax)
.Ltmp254:
.LBB18_5:
	movq	%rbx, 24(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB18_7:
.Ltmp250:
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB18_8
.LBB18_6:
.Ltmp255:
	movq	%rax, %r15
.LBB18_8:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB18_10
# BB#9:
	movq	(%rdi), %rax
.Ltmp256:
	callq	*16(%rax)
.Ltmp257:
.LBB18_10:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB18_11:
.Ltmp258:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN8NArchive4NZip12CLzmaDecoderC2Ev, .Lfunc_end18-_ZN8NArchive4NZip12CLzmaDecoderC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp246-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp247-.Ltmp246       #   Call between .Ltmp246 and .Ltmp247
	.long	.Ltmp255-.Lfunc_begin7  #     jumps to .Ltmp255
	.byte	0                       #   On action: cleanup
	.long	.Ltmp248-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp249-.Ltmp248       #   Call between .Ltmp248 and .Ltmp249
	.long	.Ltmp250-.Lfunc_begin7  #     jumps to .Ltmp250
	.byte	0                       #   On action: cleanup
	.long	.Ltmp251-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp254-.Ltmp251       #   Call between .Ltmp251 and .Ltmp254
	.long	.Ltmp255-.Lfunc_begin7  #     jumps to .Ltmp255
	.byte	0                       #   On action: cleanup
	.long	.Ltmp256-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp257-.Ltmp256       #   Call between .Ltmp256 and .Ltmp257
	.long	.Ltmp258-.Lfunc_begin7  #     jumps to .Ltmp258
	.byte	1                       #   On action: 1
	.long	.Ltmp257-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Lfunc_end18-.Ltmp257   #   Call between .Ltmp257 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip12CLzmaDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CLzmaDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN8NArchive4NZip12CLzmaDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN8NArchive4NZip12CLzmaDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi104:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi107:
	.cfi_def_cfa_offset 64
.Lcfi108:
	.cfi_offset %rbx, -48
.Lcfi109:
	.cfi_offset %r12, -40
.Lcfi110:
	.cfi_offset %r13, -32
.Lcfi111:
	.cfi_offset %r14, -24
.Lcfi112:
	.cfi_offset %r15, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	%rdi, %r13
	leaq	7(%rsp), %rsi
	movl	$9, %edx
	movq	%rbx, %rdi
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	testl	%eax, %eax
	jne	.LBB19_5
# BB#1:
	movl	$-2147467263, %eax      # imm = 0x80004001
	cmpb	$5, 9(%rsp)
	jne	.LBB19_5
# BB#2:
	cmpb	$0, 10(%rsp)
	jne	.LBB19_5
# BB#3:
	movq	16(%r13), %rdi
	movq	(%rdi), %rax
	leaq	11(%rsp), %rsi
	movl	$5, %edx
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB19_5
# BB#4:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movq	%r15, %r8
	movq	%r14, %r9
	callq	*40(%rax)
.LBB19_5:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end19:
	.size	_ZN8NArchive4NZip12CLzmaDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end19-_ZN8NArchive4NZip12CLzmaDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc

	.globl	_ZN8NArchive4NZip11CZipDecoder6DecodeERNS0_10CInArchiveERKNS0_7CItemExEP20ISequentialOutStreamP23IArchiveExtractCallbackP21ICompressProgressInfojRi
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11CZipDecoder6DecodeERNS0_10CInArchiveERKNS0_7CItemExEP20ISequentialOutStreamP23IArchiveExtractCallbackP21ICompressProgressInfojRi,@function
_ZN8NArchive4NZip11CZipDecoder6DecodeERNS0_10CInArchiveERKNS0_7CItemExEP20ISequentialOutStreamP23IArchiveExtractCallbackP21ICompressProgressInfojRi: # @_ZN8NArchive4NZip11CZipDecoder6DecodeERNS0_10CInArchiveERKNS0_7CItemExEP20ISequentialOutStreamP23IArchiveExtractCallbackP21ICompressProgressInfojRi
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%rbp
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi116:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi117:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi119:
	.cfi_def_cfa_offset 224
.Lcfi120:
	.cfi_offset %rbx, -56
.Lcfi121:
	.cfi_offset %r12, -48
.Lcfi122:
	.cfi_offset %r13, -40
.Lcfi123:
	.cfi_offset %r14, -32
.Lcfi124:
	.cfi_offset %r15, -24
.Lcfi125:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movq	%rdx, %r14
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	movq	%rdi, %r15
	movq	232(%rsp), %rbx
	movl	$2, (%rbx)
	movw	4(%r14), %r13w
	movzwl	2(%r14), %eax
	movb	$1, 15(%rsp)            # 1-byte Folded Spill
	testb	$1, %al
	jne	.LBB20_2
# BB#1:
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	%r9, 136(%rsp)          # 8-byte Spill
	movl	$0, 72(%rsp)            # 4-byte Folded Spill
	jmp	.LBB20_21
.LBB20_2:
	andl	$65, %eax
	cmpl	$65, %eax
	jne	.LBB20_10
# BB#3:
	movslq	132(%r14), %rax
	testq	%rax, %rax
	jle	.LBB20_9
# BB#4:                                 # %.lr.ph.i
	movq	136(%r14), %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB20_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rdx
	movzwl	(%rdx), %edi
	cmpl	$23, %edi
	jne	.LBB20_8
# BB#6:                                 #   in Loop: Header=BB20_5 Depth=1
	cmpq	$8, 16(%rdx)
	jb	.LBB20_8
# BB#7:                                 # %_ZN8NArchive4NZip18CStrongCryptoField17ParseFromSubBlockERKNS0_14CExtraSubBlockE.exit.i
                                        #   in Loop: Header=BB20_5 Depth=1
	movq	24(%rdx), %rdx
	movzbl	1(%rdx), %edi
	shll	$8, %edi
	movzbl	(%rdx), %edx
	orl	%edi, %edx
	movzwl	%dx, %edx
	cmpl	$2, %edx
	je	.LBB20_85
.LBB20_8:                               # %_ZN8NArchive4NZip18CStrongCryptoField17ParseFromSubBlockERKNS0_14CExtraSubBlockE.exit.thread.i
                                        #   in Loop: Header=BB20_5 Depth=1
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB20_5
.LBB20_9:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit307.thread
	movl	$1, (%rbx)
	xorl	%r15d, %r15d
	jmp	.LBB20_199
.LBB20_10:
	movl	$0, 72(%rsp)            # 4-byte Folded Spill
.LBB20_11:                              # %_ZNK8NArchive4NZip11CExtraBlock20GetStrongCryptoFieldERNS0_18CStrongCryptoFieldE.exit
	movzwl	%r13w, %eax
	cmpl	$99, %eax
	movq	%r9, 136(%rsp)          # 8-byte Spill
	movq	%r8, 64(%rsp)           # 8-byte Spill
	jne	.LBB20_21
# BB#12:
	movslq	132(%r14), %rax
	testq	%rax, %rax
	jle	.LBB20_21
# BB#13:                                # %.lr.ph.i303
	movq	136(%r14), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB20_14:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	movzwl	(%rsi), %edi
	cmpl	$39169, %edi            # imm = 0x9901
	jne	.LBB20_18
# BB#15:                                #   in Loop: Header=BB20_14 Depth=1
	cmpq	$7, 16(%rsi)
	jb	.LBB20_18
# BB#16:                                #   in Loop: Header=BB20_14 Depth=1
	movq	24(%rsi), %rsi
	cmpb	$65, 2(%rsi)
	jne	.LBB20_18
# BB#17:                                #   in Loop: Header=BB20_14 Depth=1
	cmpb	$69, 3(%rsi)
	je	.LBB20_166
	.p2align	4, 0x90
.LBB20_18:                              #   in Loop: Header=BB20_14 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB20_14
.LBB20_21:
	xorl	%ebx, %ebx
.LBB20_22:                              # %_ZNK8NArchive4NZip11CExtraBlock13GetWzAesFieldERNS0_16CWzAesExtraFieldE.exit.thread
.Ltmp259:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp260:
# BB#23:
	movl	$0, 8(%r12)
	movq	$_ZTV17COutStreamWithCRC+16, (%r12)
	movq	$0, 16(%r12)
.Ltmp262:
	movq	%r12, %rdi
	callq	*_ZTV17COutStreamWithCRC+24(%rip)
.Ltmp263:
# BB#24:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit
	testq	%rbp, %rbp
	je	.LBB20_26
# BB#25:
	movq	(%rbp), %rax
.Ltmp265:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp266:
.LBB20_26:                              # %.noexc312
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB20_28
# BB#27:
	movq	(%rdi), %rax
.Ltmp267:
	callq	*16(%rax)
.Ltmp268:
.LBB20_28:
	movq	%rbp, 16(%r12)
	movq	$0, 24(%r12)
	movb	15(%rsp), %al           # 1-byte Reload
	movb	%al, 36(%r12)
	movl	$-1, 32(%r12)
	movq	16(%r14), %rdx
	testb	%bl, %bl
	je	.LBB20_31
# BB#29:
	cmpq	$10, %rdx
	jae	.LBB20_32
# BB#30:
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.LBB20_196
.LBB20_31:
	movl	%ebx, 60(%rsp)          # 4-byte Spill
	jmp	.LBB20_33
.LBB20_32:
	movl	%ebx, 60(%rsp)          # 4-byte Spill
	addq	$-10, %rdx
.LBB20_33:
	movl	180(%r14), %eax
	addq	88(%r14), %rax
	movzwl	184(%r14), %esi
	addq	%rax, %rsi
.Ltmp270:
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	callq	_ZN8NArchive4NZip10CInArchive19CreateLimitedStreamEyy
	movq	%rax, %rbx
.Ltmp271:
# BB#34:
	xorl	%ebp, %ebp
	testb	$1, 2(%r14)
	movq	%r15, 32(%rsp)          # 8-byte Spill
	jne	.LBB20_47
# BB#35:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	xorl	%r15d, %r15d
.LBB20_36:
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	84(%rax), %rbx
	testq	%rbx, %rbx
	jle	.LBB20_40
# BB#37:                                # %.lr.ph
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	88(%rax), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB20_38:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rbp,8), %rcx
	cmpw	%r13w, (%rcx)
	je	.LBB20_40
# BB#39:                                #   in Loop: Header=BB20_38 Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB20_38
.LBB20_40:                              # %._crit_edge
	cmpl	%ebx, %ebp
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	jne	.LBB20_112
# BB#41:
	movq	$0, 48(%rsp)
	movw	%r13w, 40(%rsp)
	movzwl	%r13w, %eax
	cmpl	$14, %eax
	ja	.LBB20_79
# BB#42:
	movl	$262658, %edi           # imm = 0x40202
	jmpq	*.LJTI20_0(,%rax,8)
.LBB20_43:
.Ltmp382:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp383:
# BB#44:
	movl	$0, 16(%r13)
	movl	$_ZTVN9NCompress10CCopyCoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress10CCopyCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r13)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%r13)
.Ltmp384:
	movq	%r13, %rdi
	callq	*_ZTVN9NCompress10CCopyCoderE+24(%rip)
.Ltmp385:
# BB#45:                                # %.noexc408
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_102
# BB#46:
	movq	(%rdi), %rax
.Ltmp386:
	callq	*16(%rax)
.Ltmp387:
	jmp	.LBB20_102
.LBB20_47:
	cmpb	$0, 60(%rsp)            # 1-byte Folded Reload
	je	.LBB20_56
# BB#48:
	movslq	132(%r14), %rax
	testq	%rax, %rax
	jle	.LBB20_55
# BB#49:                                # %.lr.ph.i324
	movq	136(%r14), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB20_50:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	movzwl	(%rsi), %edi
	cmpl	$39169, %edi            # imm = 0x9901
	jne	.LBB20_54
# BB#51:                                #   in Loop: Header=BB20_50 Depth=1
	cmpq	$7, 16(%rsi)
	jb	.LBB20_54
# BB#52:                                #   in Loop: Header=BB20_50 Depth=1
	movq	24(%rsi), %rsi
	cmpb	$65, 2(%rsi)
	jne	.LBB20_54
# BB#53:                                #   in Loop: Header=BB20_50 Depth=1
	cmpb	$69, 3(%rsi)
	je	.LBB20_155
	.p2align	4, 0x90
.LBB20_54:                              #   in Loop: Header=BB20_50 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB20_50
.LBB20_55:
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	testq	%rbx, %rbx
	jne	.LBB20_195
	jmp	.LBB20_196
.LBB20_56:
	cmpb	$0, 72(%rsp)            # 1-byte Folded Reload
	jne	.LBB20_64
# BB#57:
	movq	24(%r15), %rcx
	testq	%rcx, %rcx
	jne	.LBB20_63
# BB#58:
.Ltmp282:
	movl	$48, %edi
	callq	_Znwm
.Ltmp283:
# BB#59:
	movl	$0, 16(%rax)
	movl	$_ZTVN7NCrypto4NZip8CDecoderE+96, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN7NCrypto4NZip8CDecoderE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rax)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
.Ltmp284:
	movq	%rax, %rdi
	movq	%rax, %r15
	callq	*_ZTVN7NCrypto4NZip8CDecoderE+24(%rip)
	movq	%r15, %rcx
.Ltmp285:
# BB#60:                                # %.noexc341
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB20_62
# BB#61:
	movq	(%rdi), %rax
.Ltmp286:
	callq	*16(%rax)
	movq	%r15, %rcx
.Ltmp287:
.LBB20_62:                              # %_ZN9CMyComPtrI15ICompressFilterEaSEPS0_.exit343
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 24(%rax)
.LBB20_63:
	movq	(%rcx), %rax
.Ltmp288:
	movq	%rcx, %rdi
	movq	%rcx, %r15
	callq	*8(%rax)
	movq	%r15, %rdi
.Ltmp289:
	jmp	.LBB20_71
.LBB20_64:
	movq	32(%r15), %r15
	testq	%r15, %r15
	jne	.LBB20_70
# BB#65:
.Ltmp273:
	movl	$432, %edi              # imm = 0x1B0
	callq	_Znwm
.Ltmp274:
# BB#66:
.Ltmp275:
	movq	%rax, %rdi
	movq	%rax, %r15
	callq	_ZN7NCrypto14CAesCbcDecoderC2Ev
.Ltmp276:
# BB#67:                                # %.noexc335
	movq	$_ZTV7CBufferIhE+16, 376(%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 384(%r15)
	movl	$_ZTVN7NCrypto10NZipStrong8CDecoderE+112, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto10NZipStrong8CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r15)
	movq	$_ZTVN7NCrypto10NZipStrong8CDecoderE+184, 328(%r15)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%r15, 8(%rax)
	incl	16(%r15)
	movq	32(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB20_69
# BB#68:
	movq	(%rdi), %rax
.Ltmp278:
	callq	*16(%rax)
.Ltmp279:
.LBB20_69:                              # %_ZN9CMyComPtrI15ICompressFilterEaSEPS0_.exit337
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%r15, 32(%rax)
.LBB20_70:
	movq	(%r15), %rax
.Ltmp280:
	movq	%r15, %rdi
	callq	*8(%rax)
	movq	%r15, %rdi
.Ltmp281:
.LBB20_71:                              # %_ZN9CMyComPtrI15ICompressFilterEaSERKS1_.exit340
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	$0, 120(%rsp)
	movq	(%rdi), %rax
.Ltmp306:
	leaq	120(%rsp), %rdx
	movl	$IID_ICryptoSetPassword, %esi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	callq	*(%rax)
	movl	%eax, %r15d
.Ltmp307:
# BB#72:                                # %_ZNK9CMyComPtrI15ICompressFilterE14QueryInterfaceI18ICryptoSetPasswordEEiRK4GUIDPPT_.exit
	movl	$1, %ebx
	testl	%r15d, %r15d
	jne	.LBB20_187
# BB#73:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	64(%rax), %rdi
	testq	%rdi, %rdi
	jne	.LBB20_76
# BB#74:
	leaq	64(%rax), %r15
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp308:
	movl	$IID_ICryptoGetTextPassword, %esi
	movq	%r15, %rdx
	callq	*(%rax)
.Ltmp309:
# BB#75:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB20_239
.LBB20_76:                              # %.thread543
	movq	$0, 112(%rsp)
	movq	(%rdi), %rax
.Ltmp310:
	leaq	112(%rsp), %rsi
	callq	*40(%rax)
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
.Ltmp311:
	movl	60(%rsp), %ebx          # 4-byte Reload
# BB#77:
	testl	%eax, %eax
	movl	%ebx, 60(%rsp)          # 4-byte Spill
	je	.LBB20_141
# BB#78:
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	$1, %r15d
	jmp	.LBB20_184
.LBB20_79:
	cmpl	$98, %eax
	jne	.LBB20_86
# BB#80:
.Ltmp349:
	movl	$7488, %edi             # imm = 0x1D40
	callq	_Znwm
	movq	%rax, %r13
.Ltmp350:
# BB#81:
.Ltmp351:
	movl	$1, %esi
	movq	%r13, %rdi
	callq	_ZN9NCompress8NPpmdZip8CDecoderC1Eb
.Ltmp352:
# BB#82:
	movq	(%r13), %rax
.Ltmp354:
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp355:
# BB#83:                                # %.noexc419
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_102
# BB#84:
	movq	(%rdi), %rax
.Ltmp356:
	callq	*16(%rax)
.Ltmp357:
	jmp	.LBB20_102
.LBB20_85:
	movb	$1, %al
	movl	%eax, 72(%rsp)          # 4-byte Spill
	jmp	.LBB20_11
.LBB20_86:
	cmpl	$256, %eax              # imm = 0x100
	jb	.LBB20_135
# BB#87:
	movq	232(%rsp), %rax
	movl	$1, (%rax)
	xorl	%r15d, %r15d
	movl	$1, %ebp
	jmp	.LBB20_108
.LBB20_88:
.Ltmp358:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp359:
# BB#89:
.Ltmp360:
	movq	%r13, %rdi
	callq	_ZN8NArchive4NZip12CLzmaDecoderC2Ev
.Ltmp361:
# BB#90:
	movq	(%r13), %rax
.Ltmp363:
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp364:
# BB#91:                                # %.noexc416
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_102
# BB#92:
	movq	(%rdi), %rax
.Ltmp365:
	callq	*16(%rax)
.Ltmp366:
	jmp	.LBB20_102
.LBB20_93:
.Ltmp376:
	movl	$49168, %edi            # imm = 0xC010
	callq	_Znwm
	movq	%rax, %r13
.Ltmp377:
# BB#94:
	movl	$0, 8(%r13)
	movq	$_ZTVN9NCompress7NShrink8CDecoderE+16, (%r13)
.Ltmp378:
	movq	%r13, %rdi
	callq	*_ZTVN9NCompress7NShrink8CDecoderE+24(%rip)
.Ltmp379:
# BB#95:                                # %.noexc410
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_102
# BB#96:
	movq	(%rdi), %rax
.Ltmp380:
	callq	*16(%rax)
.Ltmp381:
	jmp	.LBB20_102
.LBB20_97:
.Ltmp367:
	movl	$640, %edi              # imm = 0x280
	callq	_Znwm
	movq	%rax, %r13
.Ltmp368:
# BB#98:
.Ltmp369:
	movq	%r13, %rdi
	callq	_ZN9NCompress8NImplode8NDecoder6CCoderC1Ev
.Ltmp370:
# BB#99:
	movq	(%r13), %rax
.Ltmp372:
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp373:
# BB#100:                               # %.noexc413
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_102
# BB#101:
	movq	(%rdi), %rax
.Ltmp374:
	callq	*16(%rax)
.Ltmp375:
.LBB20_102:                             # %.thread546.sink.split
	movq	%r13, 48(%rsp)
.LBB20_103:                             # %.thread546
.Ltmp391:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp392:
# BB#104:                               # %.noexc422
	movzwl	40(%rsp), %eax
	movw	%ax, (%rbp)
	movq	%r13, 8(%rbp)
	testq	%r13, %r13
	je	.LBB20_106
# BB#105:
	movq	(%r13), %rax
.Ltmp393:
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp394:
.LBB20_106:                             # %_ZN8NArchive4NZip11CMethodItemC2ERKS1_.exit.i
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	72(%rax), %rdi
.Ltmp396:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp397:
# BB#107:                               # %_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEE3AddERKS2_.exit
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	88(%rcx), %rax
	movslq	84(%rcx), %rbx
	movq	%rbp, (%rax,%rbx,8)
	leal	1(%rbx), %eax
	movl	%eax, 84(%rcx)
	xorl	%ebp, %ebp
.LBB20_108:                             # %.thread544
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_110
# BB#109:
	movq	(%rdi), %rax
.Ltmp401:
	callq	*16(%rax)
.Ltmp402:
.LBB20_110:                             # %_ZN8NArchive4NZip11CMethodItemD2Ev.exit
	testl	%ebp, %ebp
	jne	.LBB20_190
# BB#111:
	movl	%ebx, %ebp
.LBB20_112:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	88(%rax), %rax
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	8(%rax), %rbx
	movq	$0, 80(%rsp)
	movq	(%rbx), %rax
.Ltmp404:
	leaq	80(%rsp), %rdx
	movl	$IID_ICompressSetDecoderProperties2, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
.Ltmp405:
# BB#113:
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_117
# BB#114:
	movb	2(%r14), %al
	movb	%al, 40(%rsp)
	movq	(%rdi), %rax
.Ltmp407:
	leaq	40(%rsp), %rsi
	movl	$1, %edx
	callq	*40(%rax)
.Ltmp408:
# BB#115:
	xorl	%ebp, %ebp
	testl	%eax, %eax
	setne	%cl
	je	.LBB20_117
# BB#116:
	movb	%cl, %bpl
	movl	%eax, %r15d
	jmp	.LBB20_118
.LBB20_117:
	xorl	%ebp, %ebp
.LBB20_118:
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_120
# BB#119:
	movq	(%rdi), %rax
.Ltmp412:
	callq	*16(%rax)
.Ltmp413:
.LBB20_120:                             # %_ZN9CMyComPtrI30ICompressSetDecoderProperties2ED2Ev.exit407
	testl	%ebp, %ebp
	jne	.LBB20_190
# BB#121:
	movq	$0, 128(%rsp)
	movq	(%rbx), %rax
.Ltmp415:
	leaq	128(%rsp), %rdx
	movl	$IID_ICompressSetCoderMt, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
.Ltmp416:
# BB#122:
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_126
# BB#123:
	movl	224(%rsp), %esi
	movq	(%rdi), %rax
.Ltmp417:
	callq	*40(%rax)
.Ltmp418:
# BB#124:
	testl	%eax, %eax
	je	.LBB20_126
# BB#125:
	movl	$1, %ebp
	movl	%eax, %r15d
	jmp	.LBB20_127
.LBB20_126:
	xorl	%ebp, %ebp
.LBB20_127:
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_129
# BB#128:
	movq	(%rdi), %rax
.Ltmp422:
	callq	*16(%rax)
.Ltmp423:
.LBB20_129:                             # %_ZN9CMyComPtrI19ICompressSetCoderMtED2Ev.exit
	testl	%ebp, %ebp
	jne	.LBB20_190
# BB#130:
	testb	$1, 2(%r14)
	movq	24(%rsp), %rax          # 8-byte Reload
	jne	.LBB20_133
# BB#131:
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB20_201
# BB#132:
	movq	(%rbp), %rax
	xorl	%r13d, %r13d
.Ltmp453:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp454:
	movq	%rbp, %rsi
	jmp	.LBB20_202
.LBB20_133:
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpq	$0, 56(%rcx)
	je	.LBB20_204
# BB#134:                               # %._crit_edge606
	leaq	48(%rcx), %rcx
	jmp	.LBB20_210
.LBB20_135:
	movzbl	%r13b, %edi
	orq	$262400, %rdi           # imm = 0x40100
.LBB20_136:
	leaq	48(%rsp), %rsi
.Ltmp388:
	xorl	%edx, %edx
	callq	_Z11CreateCoderyR9CMyComPtrI14ICompressCoderEb
.Ltmp389:
# BB#137:
	movl	$1, %ebp
	testl	%eax, %eax
	je	.LBB20_139
# BB#138:
	movl	%eax, %r15d
	jmp	.LBB20_108
.LBB20_139:
	movq	48(%rsp), %r13
	testq	%r13, %r13
	jne	.LBB20_103
# BB#140:
	movq	232(%rsp), %rax
	movl	$1, (%rax)
	xorl	%r15d, %r15d
	jmp	.LBB20_108
.LBB20_141:
.Ltmp312:
	movl	$4, %edi
	callq	_Znam
.Ltmp313:
# BB#142:
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movb	$0, (%rax)
	testb	%bl, %bl
	sete	%al
	cmpb	$0, 72(%rsp)            # 1-byte Folded Reload
	jne	.LBB20_167
# BB#143:
	testb	%al, %al
	je	.LBB20_167
# BB#144:
	movq	112(%rsp), %rbx
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 80(%rsp)
	movl	$-1, %r15d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB20_145:                             # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB20_145
# BB#146:                               # %_Z11MyStringLenIwEiPKT_.exit.i366
	leal	1(%r15), %eax
	movl	%eax, 108(%rsp)         # 4-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp324:
	callq	_Znam
.Ltmp325:
# BB#147:                               # %.noexc370
	movq	%rax, 80(%rsp)
	movl	$0, (%rax)
	movl	108(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, 92(%rsp)
	.p2align	4, 0x90
.LBB20_148:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i369
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB20_148
# BB#149:
	movl	%r15d, 88(%rsp)
.Ltmp327:
	leaq	40(%rsp), %rdi
	leaq	80(%rsp), %rsi
	movl	$1, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp328:
# BB#150:
	movq	64(%rsp), %rbx          # 8-byte Reload
	movb	$0, (%rbx)
	movslq	48(%rsp), %rax
	leaq	1(%rax), %rdi
	cmpl	$4, %edi
	je	.LBB20_153
# BB#151:
	cmpl	$-1, %eax
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp330:
	callq	_Znam
	movq	%rax, %r15
.Ltmp331:
# BB#152:                               # %._crit_edge17.i.i382
	movq	%rbx, %rdi
	callq	_ZdaPv
	movb	$0, (%r15)
	movq	%r15, %rbx
.LBB20_153:                             # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i383
	movq	40(%rsp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB20_154:                             # =>This Inner Loop Header: Depth=1
	movzbl	(%rax,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB20_154
	jmp	.LBB20_178
.LBB20_155:
	movb	4(%rsi), %al
	movb	%al, 16(%rsp)           # 1-byte Spill
	movzbl	5(%rsi), %r13d
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	jne	.LBB20_162
# BB#156:
.Ltmp291:
	movl	$600, %edi              # imm = 0x258
	callq	_Znwm
	movq	%rax, 24(%rsp)          # 8-byte Spill
.Ltmp292:
# BB#157:
	movq	24(%rsp), %r15          # 8-byte Reload
	movl	$0, 16(%r15)
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r15)
	movq	$_ZTV7CBufferIhE+16, 48(%r15)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 56(%r15)
	movl	$3, 24(%r15)
	movq	%r15, %rdi
	addq	$284, %rdi              # imm = 0x11C
.Ltmp293:
	callq	_ZN7NCrypto6NWzAes8CAesCtr2C1Ev
.Ltmp294:
# BB#158:
	movl	$_ZTVN7NCrypto6NWzAes8CDecoderE+104, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto6NWzAes8CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r15)
	movq	$_ZTVN7NCrypto6NWzAes8CDecoderE+168, 592(%r15)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%r15, 16(%rax)
.Ltmp296:
	movq	%r15, %rdi
	movq	%rax, %r15
	callq	*_ZTVN7NCrypto6NWzAes8CDecoderE+24(%rip)
.Ltmp297:
# BB#159:                               # %.noexc330
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB20_161
# BB#160:
	movq	(%rdi), %rax
.Ltmp298:
	callq	*16(%rax)
.Ltmp299:
.LBB20_161:                             # %_ZN9CMyComPtrI15ICompressFilterEaSEPS0_.exit
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, 40(%r15)
.LBB20_162:
	movq	(%rdi), %rax
.Ltmp300:
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	callq	*8(%rax)
.Ltmp301:
# BB#163:                               # %_ZN9CMyComPtrI15ICompressFilterEaSERKS1_.exit
	movb	16(%rsp), %al           # 1-byte Reload
	movb	%al, 40(%rsp)
	movq	16(%r15), %rdi
	movq	(%rdi), %rax
.Ltmp303:
	leaq	40(%rsp), %rsi
	movl	$1, %edx
	callq	*64(%rax)
	movl	%eax, %r15d
.Ltmp304:
# BB#164:
	testl	%r15d, %r15d
	je	.LBB20_244
# BB#165:
	xorl	%r13d, %r13d
	jmp	.LBB20_192
.LBB20_166:
	movzbl	1(%rsi), %eax
	shll	$8, %eax
	movzbl	(%rsi), %ecx
	orl	%eax, %ecx
	movzwl	%cx, %eax
	cmpl	$1, %eax
	sete	15(%rsp)                # 1-byte Folded Spill
	movb	$1, %bl
	jmp	.LBB20_22
.LBB20_167:
	movq	112(%rsp), %rbx
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 80(%rsp)
	movl	$-1, %r15d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB20_168:                             # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB20_168
# BB#169:                               # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%r15), %eax
	movl	%eax, 108(%rsp)         # 4-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp315:
	callq	_Znam
.Ltmp316:
# BB#170:                               # %.noexc355
	movq	%rax, 80(%rsp)
	movl	$0, (%rax)
	movl	108(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, 92(%rsp)
	.p2align	4, 0x90
.LBB20_171:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB20_171
# BB#172:
	movl	%r15d, 88(%rsp)
.Ltmp318:
	leaq	40(%rsp), %rdi
	leaq	80(%rsp), %rsi
	xorl	%edx, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp319:
# BB#173:
	movq	64(%rsp), %rbx          # 8-byte Reload
	movb	$0, (%rbx)
	movslq	48(%rsp), %rax
	leaq	1(%rax), %rdi
	cmpl	$4, %edi
	je	.LBB20_176
# BB#174:
	cmpl	$-1, %eax
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp321:
	callq	_Znam
	movq	%rax, %r15
.Ltmp322:
# BB#175:                               # %._crit_edge17.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movb	$0, (%r15)
	movq	%r15, %rbx
.LBB20_176:                             # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	40(%rsp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB20_177:                             # =>This Inner Loop Header: Depth=1
	movzbl	(%rax,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB20_177
.LBB20_178:
	movl	48(%rsp), %r15d
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_180
# BB#179:
	callq	_ZdaPv
.LBB20_180:                             # %_ZN11CStringBaseIcED2Ev.exit
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_182
# BB#181:
	callq	_ZdaPv
.LBB20_182:
	movq	120(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp333:
	movq	%rbx, %rsi
	movl	%r15d, %edx
	callq	*40(%rax)
.Ltmp334:
# BB#183:                               # %_ZN11CStringBaseIcED2Ev.exit394
	xorl	%r15d, %r15d
	testl	%eax, %eax
	setne	%r15b
	movq	%rbx, %rdi
	callq	_ZdaPv
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
.LBB20_184:
	movq	112(%rsp), %rdi
.Ltmp338:
	callq	SysFreeString
.Ltmp339:
# BB#185:                               # %_ZN10CMyComBSTRD2Ev.exit
	movl	$1, %ebx
	testl	%r15d, %r15d
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r15d
	jne	.LBB20_187
.LBB20_186:
	xorl	%ebx, %ebx
	movl	%eax, %r15d
.LBB20_187:
	movq	120(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_189
# BB#188:
	movq	(%rdi), %rax
.Ltmp346:
	callq	*16(%rax)
.Ltmp347:
.LBB20_189:                             # %_ZN9CMyComPtrI18ICryptoSetPasswordED2Ev.exit
	testl	%ebx, %ebx
	je	.LBB20_200
.LBB20_190:
	xorl	%r13d, %r13d
.LBB20_191:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit322.thread
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB20_192:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit322.thread
	movq	24(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB20_194
.LBB20_193:
	movq	(%rdi), %rax
.Ltmp473:
	callq	*16(%rax)
.Ltmp474:
.LBB20_194:                             # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit315
	testq	%rbx, %rbx
	je	.LBB20_196
.LBB20_195:
	movq	(%rbx), %rax
.Ltmp478:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp479:
.LBB20_196:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit309
	movq	(%r12), %rax
.Ltmp483:
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp484:
# BB#197:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit307
	testq	%r13, %r13
	je	.LBB20_199
# BB#198:
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*48(%rax)
.LBB20_199:                             # %_ZN17CInStreamReleaserD2Ev.exit299
	movl	%r15d, %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB20_200:
	movq	24(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB20_36
.LBB20_201:
	xorl	%r13d, %r13d
	xorl	%esi, %esi
.LBB20_202:                             # %select.unfold551
	movq	(%rbx), %rax
	leaq	24(%r14), %r8
.Ltmp456:
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movq	%r12, %rdx
	movq	136(%rsp), %r9          # 8-byte Reload
	callq	*40(%rax)
.Ltmp457:
# BB#203:
	movq	72(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB20_219
.LBB20_204:
	xorl	%r13d, %r13d
.Ltmp425:
	movl	$200, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp426:
# BB#205:
.Ltmp427:
	movq	%rbp, %rdi
	callq	_ZN12CFilterCoderC1Ev
.Ltmp428:
# BB#206:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rbp, 48(%rax)
	movq	%rbp, %rcx
	addq	$16, %rcx
	movq	16(%rbp), %rax
	movq	%rcx, %rbp
	xorl	%r13d, %r13d
.Ltmp430:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp431:
# BB#207:                               # %.noexc392
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB20_209
# BB#208:
	movq	(%rdi), %rax
	xorl	%r13d, %r13d
.Ltmp432:
	callq	*16(%rax)
.Ltmp433:
.LBB20_209:                             # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	48(%rax), %rcx
	movq	%rbp, 56(%rax)
	movq	24(%rsp), %rax          # 8-byte Reload
.LBB20_210:
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	(%rcx), %rbp
	testq	%rax, %rax
	je	.LBB20_212
# BB#211:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	xorl	%r13d, %r13d
.Ltmp434:
	callq	*8(%rax)
.Ltmp435:
.LBB20_212:                             # %.noexc351
	movq	192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_214
# BB#213:
	movq	(%rdi), %rax
	xorl	%r13d, %r13d
.Ltmp436:
	callq	*16(%rax)
.Ltmp437:
.LBB20_214:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 192(%rbp)
	cmpb	$0, 60(%rsp)            # 1-byte Folded Reload
	je	.LBB20_216
# BB#215:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rdi
	xorl	%r13d, %r13d
.Ltmp445:
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	_ZN7NCrypto6NWzAes8CDecoder10ReadHeaderEP19ISequentialInStream
.Ltmp446:
	jmp	.LBB20_218
.LBB20_216:
	cmpb	$0, 72(%rsp)            # 1-byte Folded Reload
	jne	.LBB20_246
# BB#217:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	xorl	%r13d, %r13d
.Ltmp443:
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	_ZN7NCrypto4NZip8CDecoder10ReadHeaderEP19ISequentialInStream
.Ltmp444:
.LBB20_218:
	xorl	%edi, %edi
	testl	%eax, %eax
	movl	$0, %r13d
	je	.LBB20_241
.LBB20_219:                             # %_ZN9CMyComPtrI19ISequentialInStreamEaSERKS1_.exit
	xorl	%ebp, %ebp
	movl	$1, %ebx
	cmpl	$1, %eax
	je	.LBB20_226
# BB#220:                               # %_ZN9CMyComPtrI19ISequentialInStreamEaSERKS1_.exit
	testl	%eax, %eax
	je	.LBB20_223
# BB#221:                               # %_ZN9CMyComPtrI19ISequentialInStreamEaSERKS1_.exit
	cmpl	$-2147467263, %eax      # imm = 0x80004001
	jne	.LBB20_224
# BB#222:
	movq	232(%rsp), %rax
	movl	$1, (%rax)
	testq	%rdi, %rdi
	jne	.LBB20_227
	jmp	.LBB20_228
.LBB20_223:
	xorl	%ebx, %ebx
	jmp	.LBB20_225
.LBB20_224:
	movl	%eax, %r15d
.LBB20_225:
	movl	%r15d, %ebp
.LBB20_226:                             # %_ZN9CMyComPtrI19ISequentialInStreamEaSERKS1_.exit.thread
	testq	%rdi, %rdi
	je	.LBB20_228
.LBB20_227:
	movq	(%rdi), %rax
.Ltmp461:
	callq	*16(%rax)
.Ltmp462:
.LBB20_228:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit322
	testl	%ebx, %ebx
	je	.LBB20_230
# BB#229:
	movl	%ebp, %r15d
	jmp	.LBB20_191
.LBB20_230:
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	movb	$1, 40(%rsp)
	movb	$1, %al
	movb	$1, %r15b
	movq	24(%rsp), %rdi          # 8-byte Reload
	je	.LBB20_232
# BB#231:
	movl	32(%r12), %ecx
	notl	%ecx
	cmpl	%ecx, 12(%r14)
	sete	%r15b
.LBB20_232:
	cmpb	$0, 60(%rsp)            # 1-byte Folded Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	je	.LBB20_245
# BB#233:
	movq	152(%rsp), %rsi         # 8-byte Reload
	addq	%rcx, %rsi
.Ltmp464:
	movl	$10, %edx
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	callq	_ZN8NArchive4NZip10CInArchive19CreateLimitedStreamEyy
	movq	%rax, %rbp
.Ltmp465:
# BB#234:
	testq	%rbx, %rbx
	je	.LBB20_236
# BB#235:
	movq	(%rbx), %rax
.Ltmp466:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp467:
.LBB20_236:                             # %_ZN9CMyComPtrI19ISequentialInStreamE6AttachEPS0_.exit
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rdi
.Ltmp468:
	leaq	40(%rsp), %rdx
	movq	%rbp, %rbx
	movq	%rbp, %rsi
	callq	_ZN7NCrypto6NWzAes8CDecoder8CheckMacEP19ISequentialInStreamRb
.Ltmp469:
# BB#237:
	testl	%eax, %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	je	.LBB20_249
# BB#238:
	movb	$0, 40(%rsp)
	xorl	%eax, %eax
	jmp	.LBB20_250
.LBB20_239:
	movq	120(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp341:
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	*40(%rax)
	movl	%eax, %r15d
.Ltmp342:
# BB#240:
	testl	%r15d, %r15d
	movl	$0, %eax
	jne	.LBB20_187
	jmp	.LBB20_186
.LBB20_241:
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	xorl	%r13d, %r13d
.Ltmp447:
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	*56(%rax)
.Ltmp448:
# BB#242:
	testl	%eax, %eax
	je	.LBB20_251
# BB#243:
	xorl	%r13d, %r13d
	movl	%eax, %r15d
	jmp	.LBB20_191
.LBB20_244:
	movq	24(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB20_71
.LBB20_245:
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB20_250
.LBB20_246:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rdi
	movl	12(%r14), %edx
	movq	24(%r14), %rcx
	xorl	%r13d, %r13d
.Ltmp438:
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	_ZN7NCrypto10NZipStrong8CDecoder10ReadHeaderEP19ISequentialInStreamjy
.Ltmp439:
# BB#247:
	testl	%eax, %eax
	je	.LBB20_257
# BB#248:
	xorl	%edi, %edi
	xorl	%r13d, %r13d
	jmp	.LBB20_219
.LBB20_249:                             # %._crit_edge605
	movb	40(%rsp), %al
	andb	$1, %al
.LBB20_250:
	xorl	%ecx, %ecx
	testb	%al, %al
	sete	%cl
	leal	(%rcx,%rcx,2), %eax
	testb	%r15b, %r15b
	movl	$3, %ecx
	cmovnel	%eax, %ecx
	movq	232(%rsp), %rax
	movl	%ecx, (%rax)
	xorl	%r15d, %r15d
	movq	%rbp, %rbx
	testq	%rdi, %rdi
	jne	.LBB20_193
	jmp	.LBB20_194
.LBB20_251:
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r13
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB20_253
# BB#252:
	movq	(%rsi), %rax
.Ltmp449:
	movq	%rsi, %rdi
	movq	%rsi, %rbp
	callq	*8(%rax)
	movq	%rbp, %rsi
.Ltmp450:
.LBB20_253:                             # %_ZN9CMyComPtrI19ISequentialInStreamEaSERKS1_.exit350
	cmpb	$0, 60(%rsp)            # 1-byte Folded Reload
	je	.LBB20_202
# BB#254:
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rdi
.Ltmp451:
	callq	_ZN7NCrypto6NWzAes8CDecoder23CheckPasswordVerifyCodeEv
.Ltmp452:
# BB#255:
	testb	%al, %al
	je	.LBB20_261
# BB#256:
	movq	72(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB20_202
.LBB20_257:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rdi
.Ltmp440:
	leaq	40(%rsp), %rsi
	callq	_ZN7NCrypto10NZipStrong8CDecoder13CheckPasswordERb
.Ltmp441:
# BB#258:
	testl	%eax, %eax
	jne	.LBB20_218
# BB#259:
	movb	40(%rsp), %cl
	testb	%cl, %cl
	movq	24(%rsp), %rdi          # 8-byte Reload
	jne	.LBB20_218
# BB#260:
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rdi, %rdi
	jne	.LBB20_193
	jmp	.LBB20_194
.LBB20_261:
	xorl	%ebp, %ebp
	movl	$1, %ebx
	movq	72(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	jne	.LBB20_227
	jmp	.LBB20_228
.LBB20_262:
.Ltmp442:
	movq	%rax, %r14
	jmp	.LBB20_324
.LBB20_263:
.Ltmp323:
	movq	%rax, %r14
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_272
# BB#264:
	callq	_ZdaPv
	jmp	.LBB20_272
.LBB20_265:
.Ltmp332:
	movq	%rax, %r14
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_276
# BB#266:
	callq	_ZdaPv
	jmp	.LBB20_276
.LBB20_267:
.Ltmp295:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r14
	movq	$_ZTV7CBufferIhE+16, 48(%r15)
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB20_269
# BB#268:
	callq	_ZdaPv
.LBB20_269:                             # %.body
	movq	24(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB20_290
.LBB20_270:
.Ltmp429:
	movq	%rax, %r14
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB20_324
.LBB20_271:
.Ltmp320:
	movq	%rax, %r14
.LBB20_272:                             # %_ZN11CStringBaseIcED2Ev.exit362
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_282
# BB#273:
	callq	_ZdaPv
	jmp	.LBB20_282
.LBB20_274:
.Ltmp317:
	jmp	.LBB20_281
.LBB20_275:
.Ltmp329:
	movq	%rax, %r14
.LBB20_276:                             # %_ZN11CStringBaseIcED2Ev.exit390
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_282
# BB#277:
	callq	_ZdaPv
	jmp	.LBB20_282
.LBB20_278:
.Ltmp326:
	jmp	.LBB20_281
.LBB20_279:
.Ltmp305:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r14
	jmp	.LBB20_324
.LBB20_280:
.Ltmp335:
	movq	%rbx, 64(%rsp)          # 8-byte Spill
.LBB20_281:                             # %_ZN11CStringBaseIcED2Ev.exit396
	movq	%rax, %r14
.LBB20_282:                             # %_ZN11CStringBaseIcED2Ev.exit396
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	jmp	.LBB20_298
.LBB20_283:
.Ltmp470:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	jmp	.LBB20_285
.LBB20_284:
.Ltmp463:
	jmp	.LBB20_285
.LBB20_286:
.Ltmp353:
	jmp	.LBB20_293
.LBB20_287:
.Ltmp458:
	movq	%rax, %r14
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	je	.LBB20_325
# BB#288:
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp459:
	callq	*16(%rax)
.Ltmp460:
	jmp	.LBB20_325
.LBB20_289:
.Ltmp277:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r14
	movq	%r15, %rdi
.LBB20_290:                             # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit
	callq	_ZdlPv
	jmp	.LBB20_314
.LBB20_291:
.Ltmp371:
	jmp	.LBB20_293
.LBB20_292:
.Ltmp362:
.LBB20_293:
	movq	%rax, %r14
	movq	%r13, %rdi
	callq	_ZdlPv
	jmp	.LBB20_322
.LBB20_294:
.Ltmp302:
	jmp	.LBB20_313
.LBB20_295:
.Ltmp340:
	jmp	.LBB20_305
.LBB20_296:
.Ltmp390:
	jmp	.LBB20_321
.LBB20_297:
.Ltmp314:
	movq	%rax, %r14
.LBB20_298:
	movq	112(%rsp), %rdi
.Ltmp336:
	callq	SysFreeString
.Ltmp337:
	jmp	.LBB20_306
.LBB20_299:
.Ltmp424:
	movq	%rax, %r14
	jmp	.LBB20_324
.LBB20_300:                             # %.thread574
.Ltmp455:
.LBB20_285:
	movq	%rax, %r14
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	jne	.LBB20_326
	jmp	.LBB20_327
.LBB20_301:
.Ltmp348:
	movq	%rax, %r14
	jmp	.LBB20_324
.LBB20_302:
.Ltmp395:
	movq	%rax, %r14
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB20_322
.LBB20_303:
.Ltmp403:
	movq	%rax, %r14
	jmp	.LBB20_324
.LBB20_304:
.Ltmp343:
.LBB20_305:
	movq	%rax, %r14
.LBB20_306:
	movq	120(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_324
# BB#307:
	movq	(%rdi), %rax
.Ltmp344:
	callq	*16(%rax)
.Ltmp345:
	jmp	.LBB20_324
.LBB20_308:
.Ltmp419:
	movq	%rax, %r14
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_324
# BB#309:
	movq	(%rdi), %rax
.Ltmp420:
	callq	*16(%rax)
.Ltmp421:
	jmp	.LBB20_324
.LBB20_310:
.Ltmp414:
	movq	%rax, %r14
	jmp	.LBB20_324
.LBB20_311:
.Ltmp409:
	jmp	.LBB20_316
.LBB20_312:
.Ltmp290:
.LBB20_313:                             # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r14
.LBB20_314:                             # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit
	xorl	%r13d, %r13d
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	jne	.LBB20_328
	jmp	.LBB20_335
.LBB20_315:
.Ltmp406:
.LBB20_316:
	movq	%rax, %r14
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_324
# BB#317:
	movq	(%rdi), %rax
.Ltmp410:
	callq	*16(%rax)
.Ltmp411:
	jmp	.LBB20_324
.LBB20_318:
.Ltmp475:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r14
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	jne	.LBB20_328
	jmp	.LBB20_335
.LBB20_319:
.Ltmp480:
	movq	%rax, %r14
	jmp	.LBB20_335
.LBB20_320:
.Ltmp398:
.LBB20_321:
	movq	%rax, %r14
.LBB20_322:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_324
# BB#323:
	movq	(%rdi), %rax
.Ltmp399:
	callq	*16(%rax)
.Ltmp400:
.LBB20_324:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit320
	xorl	%r13d, %r13d
.LBB20_325:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit320
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB20_327
.LBB20_326:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp471:
	callq	*16(%rax)
.Ltmp472:
.LBB20_327:                             # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB20_335
.LBB20_328:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp476:
	callq	*16(%rax)
.Ltmp477:
	jmp	.LBB20_335
.LBB20_329:                             # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit.thread
.Ltmp272:
	jmp	.LBB20_334
.LBB20_330:
.Ltmp485:
	movq	%rax, %r14
	testq	%r13, %r13
	jne	.LBB20_337
	jmp	.LBB20_338
.LBB20_331:
.Ltmp264:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB20_332:
.Ltmp261:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB20_333:
.Ltmp269:
.LBB20_334:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	%rax, %r14
	xorl	%r13d, %r13d
.LBB20_335:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	(%r12), %rax
.Ltmp481:
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp482:
# BB#336:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	testq	%r13, %r13
	je	.LBB20_338
.LBB20_337:
	movq	(%r13), %rax
.Ltmp486:
	movq	%r13, %rdi
	callq	*48(%rax)
.Ltmp487:
.LBB20_338:                             # %_ZN17CInStreamReleaserD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB20_339:
.Ltmp488:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end20:
	.size	_ZN8NArchive4NZip11CZipDecoder6DecodeERNS0_10CInArchiveERKNS0_7CItemExEP20ISequentialOutStreamP23IArchiveExtractCallbackP21ICompressProgressInfojRi, .Lfunc_end20-_ZN8NArchive4NZip11CZipDecoder6DecodeERNS0_10CInArchiveERKNS0_7CItemExEP20ISequentialOutStreamP23IArchiveExtractCallbackP21ICompressProgressInfojRi
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI20_0:
	.quad	.LBB20_43
	.quad	.LBB20_93
	.quad	.LBB20_86
	.quad	.LBB20_86
	.quad	.LBB20_86
	.quad	.LBB20_86
	.quad	.LBB20_97
	.quad	.LBB20_86
	.quad	.LBB20_86
	.quad	.LBB20_86
	.quad	.LBB20_86
	.quad	.LBB20_86
	.quad	.LBB20_136
	.quad	.LBB20_86
	.quad	.LBB20_88
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\210\006"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\377\005"              # Call site table length
	.long	.Ltmp259-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp260-.Ltmp259       #   Call between .Ltmp259 and .Ltmp260
	.long	.Ltmp261-.Lfunc_begin8  #     jumps to .Ltmp261
	.byte	0                       #   On action: cleanup
	.long	.Ltmp262-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp263-.Ltmp262       #   Call between .Ltmp262 and .Ltmp263
	.long	.Ltmp264-.Lfunc_begin8  #     jumps to .Ltmp264
	.byte	0                       #   On action: cleanup
	.long	.Ltmp265-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp268-.Ltmp265       #   Call between .Ltmp265 and .Ltmp268
	.long	.Ltmp269-.Lfunc_begin8  #     jumps to .Ltmp269
	.byte	0                       #   On action: cleanup
	.long	.Ltmp270-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp271-.Ltmp270       #   Call between .Ltmp270 and .Ltmp271
	.long	.Ltmp272-.Lfunc_begin8  #     jumps to .Ltmp272
	.byte	0                       #   On action: cleanup
	.long	.Ltmp382-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp387-.Ltmp382       #   Call between .Ltmp382 and .Ltmp387
	.long	.Ltmp398-.Lfunc_begin8  #     jumps to .Ltmp398
	.byte	0                       #   On action: cleanup
	.long	.Ltmp282-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp274-.Ltmp282       #   Call between .Ltmp282 and .Ltmp274
	.long	.Ltmp290-.Lfunc_begin8  #     jumps to .Ltmp290
	.byte	0                       #   On action: cleanup
	.long	.Ltmp275-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Ltmp276-.Ltmp275       #   Call between .Ltmp275 and .Ltmp276
	.long	.Ltmp277-.Lfunc_begin8  #     jumps to .Ltmp277
	.byte	0                       #   On action: cleanup
	.long	.Ltmp278-.Lfunc_begin8  # >> Call Site 8 <<
	.long	.Ltmp281-.Ltmp278       #   Call between .Ltmp278 and .Ltmp281
	.long	.Ltmp290-.Lfunc_begin8  #     jumps to .Ltmp290
	.byte	0                       #   On action: cleanup
	.long	.Ltmp306-.Lfunc_begin8  # >> Call Site 9 <<
	.long	.Ltmp309-.Ltmp306       #   Call between .Ltmp306 and .Ltmp309
	.long	.Ltmp343-.Lfunc_begin8  #     jumps to .Ltmp343
	.byte	0                       #   On action: cleanup
	.long	.Ltmp310-.Lfunc_begin8  # >> Call Site 10 <<
	.long	.Ltmp311-.Ltmp310       #   Call between .Ltmp310 and .Ltmp311
	.long	.Ltmp314-.Lfunc_begin8  #     jumps to .Ltmp314
	.byte	0                       #   On action: cleanup
	.long	.Ltmp349-.Lfunc_begin8  # >> Call Site 11 <<
	.long	.Ltmp350-.Ltmp349       #   Call between .Ltmp349 and .Ltmp350
	.long	.Ltmp398-.Lfunc_begin8  #     jumps to .Ltmp398
	.byte	0                       #   On action: cleanup
	.long	.Ltmp351-.Lfunc_begin8  # >> Call Site 12 <<
	.long	.Ltmp352-.Ltmp351       #   Call between .Ltmp351 and .Ltmp352
	.long	.Ltmp353-.Lfunc_begin8  #     jumps to .Ltmp353
	.byte	0                       #   On action: cleanup
	.long	.Ltmp354-.Lfunc_begin8  # >> Call Site 13 <<
	.long	.Ltmp359-.Ltmp354       #   Call between .Ltmp354 and .Ltmp359
	.long	.Ltmp398-.Lfunc_begin8  #     jumps to .Ltmp398
	.byte	0                       #   On action: cleanup
	.long	.Ltmp360-.Lfunc_begin8  # >> Call Site 14 <<
	.long	.Ltmp361-.Ltmp360       #   Call between .Ltmp360 and .Ltmp361
	.long	.Ltmp362-.Lfunc_begin8  #     jumps to .Ltmp362
	.byte	0                       #   On action: cleanup
	.long	.Ltmp363-.Lfunc_begin8  # >> Call Site 15 <<
	.long	.Ltmp368-.Ltmp363       #   Call between .Ltmp363 and .Ltmp368
	.long	.Ltmp398-.Lfunc_begin8  #     jumps to .Ltmp398
	.byte	0                       #   On action: cleanup
	.long	.Ltmp369-.Lfunc_begin8  # >> Call Site 16 <<
	.long	.Ltmp370-.Ltmp369       #   Call between .Ltmp369 and .Ltmp370
	.long	.Ltmp371-.Lfunc_begin8  #     jumps to .Ltmp371
	.byte	0                       #   On action: cleanup
	.long	.Ltmp372-.Lfunc_begin8  # >> Call Site 17 <<
	.long	.Ltmp392-.Ltmp372       #   Call between .Ltmp372 and .Ltmp392
	.long	.Ltmp398-.Lfunc_begin8  #     jumps to .Ltmp398
	.byte	0                       #   On action: cleanup
	.long	.Ltmp393-.Lfunc_begin8  # >> Call Site 18 <<
	.long	.Ltmp394-.Ltmp393       #   Call between .Ltmp393 and .Ltmp394
	.long	.Ltmp395-.Lfunc_begin8  #     jumps to .Ltmp395
	.byte	0                       #   On action: cleanup
	.long	.Ltmp396-.Lfunc_begin8  # >> Call Site 19 <<
	.long	.Ltmp397-.Ltmp396       #   Call between .Ltmp396 and .Ltmp397
	.long	.Ltmp398-.Lfunc_begin8  #     jumps to .Ltmp398
	.byte	0                       #   On action: cleanup
	.long	.Ltmp401-.Lfunc_begin8  # >> Call Site 20 <<
	.long	.Ltmp402-.Ltmp401       #   Call between .Ltmp401 and .Ltmp402
	.long	.Ltmp403-.Lfunc_begin8  #     jumps to .Ltmp403
	.byte	0                       #   On action: cleanup
	.long	.Ltmp404-.Lfunc_begin8  # >> Call Site 21 <<
	.long	.Ltmp405-.Ltmp404       #   Call between .Ltmp404 and .Ltmp405
	.long	.Ltmp406-.Lfunc_begin8  #     jumps to .Ltmp406
	.byte	0                       #   On action: cleanup
	.long	.Ltmp407-.Lfunc_begin8  # >> Call Site 22 <<
	.long	.Ltmp408-.Ltmp407       #   Call between .Ltmp407 and .Ltmp408
	.long	.Ltmp409-.Lfunc_begin8  #     jumps to .Ltmp409
	.byte	0                       #   On action: cleanup
	.long	.Ltmp412-.Lfunc_begin8  # >> Call Site 23 <<
	.long	.Ltmp413-.Ltmp412       #   Call between .Ltmp412 and .Ltmp413
	.long	.Ltmp414-.Lfunc_begin8  #     jumps to .Ltmp414
	.byte	0                       #   On action: cleanup
	.long	.Ltmp415-.Lfunc_begin8  # >> Call Site 24 <<
	.long	.Ltmp418-.Ltmp415       #   Call between .Ltmp415 and .Ltmp418
	.long	.Ltmp419-.Lfunc_begin8  #     jumps to .Ltmp419
	.byte	0                       #   On action: cleanup
	.long	.Ltmp422-.Lfunc_begin8  # >> Call Site 25 <<
	.long	.Ltmp423-.Ltmp422       #   Call between .Ltmp422 and .Ltmp423
	.long	.Ltmp424-.Lfunc_begin8  #     jumps to .Ltmp424
	.byte	0                       #   On action: cleanup
	.long	.Ltmp453-.Lfunc_begin8  # >> Call Site 26 <<
	.long	.Ltmp454-.Ltmp453       #   Call between .Ltmp453 and .Ltmp454
	.long	.Ltmp455-.Lfunc_begin8  #     jumps to .Ltmp455
	.byte	0                       #   On action: cleanup
	.long	.Ltmp388-.Lfunc_begin8  # >> Call Site 27 <<
	.long	.Ltmp389-.Ltmp388       #   Call between .Ltmp388 and .Ltmp389
	.long	.Ltmp390-.Lfunc_begin8  #     jumps to .Ltmp390
	.byte	0                       #   On action: cleanup
	.long	.Ltmp312-.Lfunc_begin8  # >> Call Site 28 <<
	.long	.Ltmp313-.Ltmp312       #   Call between .Ltmp312 and .Ltmp313
	.long	.Ltmp314-.Lfunc_begin8  #     jumps to .Ltmp314
	.byte	0                       #   On action: cleanup
	.long	.Ltmp324-.Lfunc_begin8  # >> Call Site 29 <<
	.long	.Ltmp325-.Ltmp324       #   Call between .Ltmp324 and .Ltmp325
	.long	.Ltmp326-.Lfunc_begin8  #     jumps to .Ltmp326
	.byte	0                       #   On action: cleanup
	.long	.Ltmp327-.Lfunc_begin8  # >> Call Site 30 <<
	.long	.Ltmp328-.Ltmp327       #   Call between .Ltmp327 and .Ltmp328
	.long	.Ltmp329-.Lfunc_begin8  #     jumps to .Ltmp329
	.byte	0                       #   On action: cleanup
	.long	.Ltmp330-.Lfunc_begin8  # >> Call Site 31 <<
	.long	.Ltmp331-.Ltmp330       #   Call between .Ltmp330 and .Ltmp331
	.long	.Ltmp332-.Lfunc_begin8  #     jumps to .Ltmp332
	.byte	0                       #   On action: cleanup
	.long	.Ltmp291-.Lfunc_begin8  # >> Call Site 32 <<
	.long	.Ltmp292-.Ltmp291       #   Call between .Ltmp291 and .Ltmp292
	.long	.Ltmp302-.Lfunc_begin8  #     jumps to .Ltmp302
	.byte	0                       #   On action: cleanup
	.long	.Ltmp293-.Lfunc_begin8  # >> Call Site 33 <<
	.long	.Ltmp294-.Ltmp293       #   Call between .Ltmp293 and .Ltmp294
	.long	.Ltmp295-.Lfunc_begin8  #     jumps to .Ltmp295
	.byte	0                       #   On action: cleanup
	.long	.Ltmp296-.Lfunc_begin8  # >> Call Site 34 <<
	.long	.Ltmp301-.Ltmp296       #   Call between .Ltmp296 and .Ltmp301
	.long	.Ltmp302-.Lfunc_begin8  #     jumps to .Ltmp302
	.byte	0                       #   On action: cleanup
	.long	.Ltmp303-.Lfunc_begin8  # >> Call Site 35 <<
	.long	.Ltmp304-.Ltmp303       #   Call between .Ltmp303 and .Ltmp304
	.long	.Ltmp305-.Lfunc_begin8  #     jumps to .Ltmp305
	.byte	0                       #   On action: cleanup
	.long	.Ltmp315-.Lfunc_begin8  # >> Call Site 36 <<
	.long	.Ltmp316-.Ltmp315       #   Call between .Ltmp315 and .Ltmp316
	.long	.Ltmp317-.Lfunc_begin8  #     jumps to .Ltmp317
	.byte	0                       #   On action: cleanup
	.long	.Ltmp318-.Lfunc_begin8  # >> Call Site 37 <<
	.long	.Ltmp319-.Ltmp318       #   Call between .Ltmp318 and .Ltmp319
	.long	.Ltmp320-.Lfunc_begin8  #     jumps to .Ltmp320
	.byte	0                       #   On action: cleanup
	.long	.Ltmp321-.Lfunc_begin8  # >> Call Site 38 <<
	.long	.Ltmp322-.Ltmp321       #   Call between .Ltmp321 and .Ltmp322
	.long	.Ltmp323-.Lfunc_begin8  #     jumps to .Ltmp323
	.byte	0                       #   On action: cleanup
	.long	.Ltmp333-.Lfunc_begin8  # >> Call Site 39 <<
	.long	.Ltmp334-.Ltmp333       #   Call between .Ltmp333 and .Ltmp334
	.long	.Ltmp335-.Lfunc_begin8  #     jumps to .Ltmp335
	.byte	0                       #   On action: cleanup
	.long	.Ltmp338-.Lfunc_begin8  # >> Call Site 40 <<
	.long	.Ltmp339-.Ltmp338       #   Call between .Ltmp338 and .Ltmp339
	.long	.Ltmp340-.Lfunc_begin8  #     jumps to .Ltmp340
	.byte	0                       #   On action: cleanup
	.long	.Ltmp346-.Lfunc_begin8  # >> Call Site 41 <<
	.long	.Ltmp347-.Ltmp346       #   Call between .Ltmp346 and .Ltmp347
	.long	.Ltmp348-.Lfunc_begin8  #     jumps to .Ltmp348
	.byte	0                       #   On action: cleanup
	.long	.Ltmp473-.Lfunc_begin8  # >> Call Site 42 <<
	.long	.Ltmp474-.Ltmp473       #   Call between .Ltmp473 and .Ltmp474
	.long	.Ltmp475-.Lfunc_begin8  #     jumps to .Ltmp475
	.byte	0                       #   On action: cleanup
	.long	.Ltmp478-.Lfunc_begin8  # >> Call Site 43 <<
	.long	.Ltmp479-.Ltmp478       #   Call between .Ltmp478 and .Ltmp479
	.long	.Ltmp480-.Lfunc_begin8  #     jumps to .Ltmp480
	.byte	0                       #   On action: cleanup
	.long	.Ltmp483-.Lfunc_begin8  # >> Call Site 44 <<
	.long	.Ltmp484-.Ltmp483       #   Call between .Ltmp483 and .Ltmp484
	.long	.Ltmp485-.Lfunc_begin8  #     jumps to .Ltmp485
	.byte	0                       #   On action: cleanup
	.long	.Ltmp484-.Lfunc_begin8  # >> Call Site 45 <<
	.long	.Ltmp456-.Ltmp484       #   Call between .Ltmp484 and .Ltmp456
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp456-.Lfunc_begin8  # >> Call Site 46 <<
	.long	.Ltmp457-.Ltmp456       #   Call between .Ltmp456 and .Ltmp457
	.long	.Ltmp458-.Lfunc_begin8  #     jumps to .Ltmp458
	.byte	0                       #   On action: cleanup
	.long	.Ltmp425-.Lfunc_begin8  # >> Call Site 47 <<
	.long	.Ltmp426-.Ltmp425       #   Call between .Ltmp425 and .Ltmp426
	.long	.Ltmp455-.Lfunc_begin8  #     jumps to .Ltmp455
	.byte	0                       #   On action: cleanup
	.long	.Ltmp427-.Lfunc_begin8  # >> Call Site 48 <<
	.long	.Ltmp428-.Ltmp427       #   Call between .Ltmp427 and .Ltmp428
	.long	.Ltmp429-.Lfunc_begin8  #     jumps to .Ltmp429
	.byte	0                       #   On action: cleanup
	.long	.Ltmp430-.Lfunc_begin8  # >> Call Site 49 <<
	.long	.Ltmp444-.Ltmp430       #   Call between .Ltmp430 and .Ltmp444
	.long	.Ltmp455-.Lfunc_begin8  #     jumps to .Ltmp455
	.byte	0                       #   On action: cleanup
	.long	.Ltmp461-.Lfunc_begin8  # >> Call Site 50 <<
	.long	.Ltmp462-.Ltmp461       #   Call between .Ltmp461 and .Ltmp462
	.long	.Ltmp463-.Lfunc_begin8  #     jumps to .Ltmp463
	.byte	0                       #   On action: cleanup
	.long	.Ltmp464-.Lfunc_begin8  # >> Call Site 51 <<
	.long	.Ltmp469-.Ltmp464       #   Call between .Ltmp464 and .Ltmp469
	.long	.Ltmp470-.Lfunc_begin8  #     jumps to .Ltmp470
	.byte	0                       #   On action: cleanup
	.long	.Ltmp341-.Lfunc_begin8  # >> Call Site 52 <<
	.long	.Ltmp342-.Ltmp341       #   Call between .Ltmp341 and .Ltmp342
	.long	.Ltmp343-.Lfunc_begin8  #     jumps to .Ltmp343
	.byte	0                       #   On action: cleanup
	.long	.Ltmp447-.Lfunc_begin8  # >> Call Site 53 <<
	.long	.Ltmp450-.Ltmp447       #   Call between .Ltmp447 and .Ltmp450
	.long	.Ltmp455-.Lfunc_begin8  #     jumps to .Ltmp455
	.byte	0                       #   On action: cleanup
	.long	.Ltmp451-.Lfunc_begin8  # >> Call Site 54 <<
	.long	.Ltmp452-.Ltmp451       #   Call between .Ltmp451 and .Ltmp452
	.long	.Ltmp458-.Lfunc_begin8  #     jumps to .Ltmp458
	.byte	0                       #   On action: cleanup
	.long	.Ltmp440-.Lfunc_begin8  # >> Call Site 55 <<
	.long	.Ltmp441-.Ltmp440       #   Call between .Ltmp440 and .Ltmp441
	.long	.Ltmp442-.Lfunc_begin8  #     jumps to .Ltmp442
	.byte	0                       #   On action: cleanup
	.long	.Ltmp459-.Lfunc_begin8  # >> Call Site 56 <<
	.long	.Ltmp477-.Ltmp459       #   Call between .Ltmp459 and .Ltmp477
	.long	.Ltmp488-.Lfunc_begin8  #     jumps to .Ltmp488
	.byte	1                       #   On action: 1
	.long	.Ltmp477-.Lfunc_begin8  # >> Call Site 57 <<
	.long	.Ltmp481-.Ltmp477       #   Call between .Ltmp477 and .Ltmp481
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp481-.Lfunc_begin8  # >> Call Site 58 <<
	.long	.Ltmp487-.Ltmp481       #   Call between .Ltmp481 and .Ltmp487
	.long	.Ltmp488-.Lfunc_begin8  #     jumps to .Ltmp488
	.byte	1                       #   On action: 1
	.long	.Ltmp487-.Lfunc_begin8  # >> Call Site 59 <<
	.long	.Lfunc_end20-.Ltmp487   #   Call between .Ltmp487 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NZip8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler7ExtractEPKjjiP23IArchiveExtractCallback,@function
_ZN8NArchive4NZip8CHandler7ExtractEPKjjiP23IArchiveExtractCallback: # @_ZN8NArchive4NZip8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%rbp
.Lcfi126:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi127:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi128:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi129:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi130:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 56
	subq	$424, %rsp              # imm = 0x1A8
.Lcfi132:
	.cfi_def_cfa_offset 480
.Lcfi133:
	.cfi_offset %rbx, -56
.Lcfi134:
	.cfi_offset %r12, -48
.Lcfi135:
	.cfi_offset %r13, -40
.Lcfi136:
	.cfi_offset %r14, -32
.Lcfi137:
	.cfi_offset %r15, -24
.Lcfi138:
	.cfi_offset %rbp, -16
	movq	%r8, %rbp
	movl	%ecx, %r14d
	movl	%edx, %r13d
	movq	%rsi, %rbx
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 208(%rsp)
	movdqa	%xmm0, 176(%rsp)
	movdqa	%xmm0, 160(%rsp)
	movdqa	%xmm0, 144(%rsp)
	movdqa	%xmm0, 128(%rsp)
	movq	$0, 192(%rsp)
	movq	$8, 224(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CMethodItemEE+16, 200(%rsp)
	cmpl	$-1, %r13d
	movl	%r13d, %r15d
	jne	.LBB21_2
# BB#1:
	movl	44(%rdi), %r15d
.LBB21_2:
	testl	%r15d, %r15d
	je	.LBB21_9
# BB#3:                                 # %.lr.ph274
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	movq	48(%rdi), %rax
	cmpl	$-1, %r13d
	je	.LBB21_10
# BB#4:                                 # %.lr.ph274.split.preheader
	movq	%rbp, %r9
	movl	%r15d, %ecx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	andq	$3, %rdi
	je	.LBB21_6
	.p2align	4, 0x90
.LBB21_5:                               # %.lr.ph274.split.prol
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rbx,%rdx,4), %rbp
	movq	(%rax,%rbp,8), %rbp
	addq	24(%rbp), %rsi
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB21_5
.LBB21_6:                               # %.lr.ph274.split.prol.loopexit
	cmpq	$3, %r8
	movq	%r9, %rbp
	jb	.LBB21_16
# BB#7:                                 # %.lr.ph274.split.preheader.new
	subq	%rdx, %rcx
	leaq	12(%rbx,%rdx,4), %rdi
	.p2align	4, 0x90
.LBB21_8:                               # %.lr.ph274.split
                                        # =>This Inner Loop Header: Depth=1
	movslq	-12(%rdi), %rdx
	movq	(%rax,%rdx,8), %rdx
	addq	24(%rdx), %rsi
	movslq	-8(%rdi), %rdx
	movq	(%rax,%rdx,8), %rdx
	addq	24(%rdx), %rsi
	movslq	-4(%rdi), %rdx
	movq	(%rax,%rdx,8), %rdx
	addq	24(%rdx), %rsi
	movslq	(%rdi), %rdx
	movq	(%rax,%rdx,8), %rdx
	addq	24(%rdx), %rsi
	addq	$16, %rdi
	addq	$-4, %rcx
	jne	.LBB21_8
	jmp	.LBB21_16
.LBB21_9:
	xorl	%r12d, %r12d
	jmp	.LBB21_18
.LBB21_10:                              # %.lr.ph274.split.us.preheader
	leal	-1(%r15), %r8d
	testb	$3, %r15b
	je	.LBB21_13
# BB#11:                                # %.lr.ph274.split.us.prol.preheader
	movl	%r15d, %edi
	andl	$3, %edi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB21_12:                              # %.lr.ph274.split.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rcx,8), %rdx
	addq	24(%rdx), %rsi
	incq	%rcx
	cmpl	%ecx, %edi
	jne	.LBB21_12
	jmp	.LBB21_14
.LBB21_13:
	xorl	%esi, %esi
	xorl	%ecx, %ecx
.LBB21_14:                              # %.lr.ph274.split.us.prol.loopexit
	cmpl	$3, %r8d
	jb	.LBB21_16
	.p2align	4, 0x90
.LBB21_15:                              # %.lr.ph274.split.us
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rdx
	addq	24(%rdx), %rsi
	leal	1(%rcx), %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rdx
	addq	24(%rdx), %rsi
	leal	2(%rcx), %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rdx
	addq	24(%rdx), %rsi
	leal	3(%rcx), %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rdx
	addq	24(%rdx), %rsi
	addl	$4, %ecx
	cmpl	%ecx, %r15d
	jne	.LBB21_15
.LBB21_16:                              # %._crit_edge275
	movq	(%rbp), %rax
.Ltmp489:
.Lcfi139:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	*40(%rax)
	movl	%eax, %r12d
.Ltmp490:
# BB#17:
	testl	%r12d, %r12d
	je	.LBB21_20
.LBB21_18:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit225
.Ltmp559:
.Lcfi140:
	.cfi_escape 0x2e, 0x00
	leaq	128(%rsp), %rdi
	callq	_ZN8NArchive4NZip11CZipDecoderD2Ev
.Ltmp560:
.LBB21_19:
	movl	%r12d, %eax
	addq	$424, %rsp              # imm = 0x1A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_20:
.Ltmp492:
	movl	%r14d, 44(%rsp)         # 4-byte Spill
	movq	%rbp, %r14
.Lcfi141:
	.cfi_escape 0x2e, 0x00
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp493:
# BB#21:
.Ltmp495:
.Lcfi142:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp496:
# BB#22:
	movq	(%rbp), %rax
.Ltmp498:
.Lcfi143:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp499:
# BB#23:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit
.Ltmp501:
	movq	%rbx, 96(%rsp)          # 8-byte Spill
.Lcfi144:
	.cfi_escape 0x2e, 0x00
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	movq	%r14, %rbp
	movq	%rbp, %rsi
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp502:
# BB#24:                                # %.lr.ph
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	64(%rax), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	setne	%al
	movl	%eax, 28(%rsp)          # 4-byte Spill
	sete	15(%rsp)                # 1-byte Folded Spill
	movl	%r15d, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	jmp	.LBB21_80
	.p2align	4, 0x90
.LBB21_25:                              #   in Loop: Header=BB21_80 Depth=1
	movq	$0, 16(%rsp)
	cmpl	$-1, %r13d
	je	.LBB21_27
# BB#26:                                #   in Loop: Header=BB21_80 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r15,4), %r14d
	jmp	.LBB21_28
	.p2align	4, 0x90
.LBB21_27:                              # %._crit_edge
                                        #   in Loop: Header=BB21_80 Depth=1
	movl	%r15d, %r14d
.LBB21_28:                              #   in Loop: Header=BB21_80 Depth=1
	movq	(%rbp), %rax
.Ltmp507:
.Lcfi145:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	movl	%r14d, %esi
	leaq	16(%rsp), %rdx
	movl	28(%rsp), %ecx          # 4-byte Reload
	callq	*56(%rax)
.Ltmp508:
# BB#29:                                #   in Loop: Header=BB21_80 Depth=1
	testl	%eax, %eax
	movl	%eax, %r13d
	cmovel	%r12d, %r13d
	je	.LBB21_31
# BB#30:                                #   in Loop: Header=BB21_80 Depth=1
	pxor	%xmm1, %xmm1
	movl	$1, %ebx
	movl	%eax, %r12d
	movl	8(%rsp), %r13d          # 4-byte Reload
	jmp	.LBB21_63
	.p2align	4, 0x90
.LBB21_31:                              #   in Loop: Header=BB21_80 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	48(%rax), %rax
	movslq	%r14d, %rcx
	movq	(%rax,%rcx,8), %rbx
.Ltmp509:
.Lcfi146:
	.cfi_escape 0x2e, 0x00
	leaq	232(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN8NArchive4NZip5CItemC2ERKS1_
.Ltmp510:
# BB#32:                                #   in Loop: Header=BB21_80 Depth=1
	movzwl	184(%rbx), %eax
	leaq	412(%rsp), %rcx
	movw	%ax, 4(%rcx)
	movl	180(%rbx), %eax
	movl	%eax, (%rcx)
	cmpb	$0, 408(%rsp)
	je	.LBB21_42
.LBB21_33:                              #   in Loop: Header=BB21_80 Depth=1
.Ltmp514:
.Lcfi147:
	.cfi_escape 0x2e, 0x00
	leaq	232(%rsp), %rdi
	callq	_ZNK8NArchive4NZip5CItem5IsDirEv
.Ltmp515:
# BB#34:                                #   in Loop: Header=BB21_80 Depth=1
	testb	%al, %al
	je	.LBB21_55
# BB#35:                                #   in Loop: Header=BB21_80 Depth=1
	movq	(%rbp), %rax
.Ltmp525:
.Lcfi148:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	callq	*64(%rax)
.Ltmp526:
# BB#36:                                #   in Loop: Header=BB21_80 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r12d
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 48(%rsp)         # 16-byte Spill
	jne	.LBB21_59
# BB#37:                                #   in Loop: Header=BB21_80 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_40
# BB#38:                                #   in Loop: Header=BB21_80 Depth=1
	movq	(%rdi), %rax
.Ltmp527:
.Lcfi149:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp528:
# BB#39:                                # %.noexc231
                                        #   in Loop: Header=BB21_80 Depth=1
	movq	$0, 16(%rsp)
.LBB21_40:                              # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit232
                                        #   in Loop: Header=BB21_80 Depth=1
	movq	(%rbp), %rax
.Ltmp529:
.Lcfi150:
	.cfi_escape 0x2e, 0x00
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	*72(%rax)
.Ltmp530:
# BB#41:                                #   in Loop: Header=BB21_80 Depth=1
	xorl	%ebx, %ebx
	testl	%eax, %eax
	setne	%bl
	cmovnel	%eax, %r12d
	movl	$7, %eax
	cmovel	%eax, %ebx
	jmp	.LBB21_60
.LBB21_42:                              #   in Loop: Header=BB21_80 Depth=1
.Ltmp512:
.Lcfi151:
	.cfi_escape 0x2e, 0x00
	movq	88(%rsp), %rdi          # 8-byte Reload
	leaq	232(%rsp), %rsi
	callq	_ZN8NArchive4NZip10CInArchive24ReadLocalItemAfterCdItemERNS0_7CItemExE
.Ltmp513:
# BB#43:                                #   in Loop: Header=BB21_80 Depth=1
	testl	%eax, %eax
	je	.LBB21_33
# BB#44:                                #   in Loop: Header=BB21_80 Depth=1
	cmpl	$1, %eax
	jne	.LBB21_54
# BB#45:                                #   in Loop: Header=BB21_80 Depth=1
.Ltmp532:
.Lcfi152:
	.cfi_escape 0x2e, 0x00
	leaq	232(%rsp), %rdi
	callq	_ZNK8NArchive4NZip5CItem5IsDirEv
.Ltmp533:
# BB#46:                                #   in Loop: Header=BB21_80 Depth=1
	testb	%al, %al
	je	.LBB21_71
.LBB21_47:                              #   in Loop: Header=BB21_80 Depth=1
	movq	(%rbp), %rax
.Ltmp534:
.Lcfi153:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	callq	*64(%rax)
.Ltmp535:
# BB#48:                                #   in Loop: Header=BB21_80 Depth=1
	testl	%eax, %eax
	jne	.LBB21_54
# BB#49:                                #   in Loop: Header=BB21_80 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_52
# BB#50:                                #   in Loop: Header=BB21_80 Depth=1
	movq	(%rdi), %rax
.Ltmp536:
.Lcfi154:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp537:
# BB#51:                                # %.noexc228
                                        #   in Loop: Header=BB21_80 Depth=1
	movq	$0, 16(%rsp)
.LBB21_52:                              # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
                                        #   in Loop: Header=BB21_80 Depth=1
	movq	(%rbp), %rax
.Ltmp538:
.Lcfi155:
	.cfi_escape 0x2e, 0x00
	movl	$1, %esi
	movq	%rbp, %rdi
	callq	*72(%rax)
.Ltmp539:
# BB#53:                                #   in Loop: Header=BB21_80 Depth=1
	testl	%eax, %eax
	je	.LBB21_79
.LBB21_54:                              # %.thread282
                                        #   in Loop: Header=BB21_80 Depth=1
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 48(%rsp)         # 16-byte Spill
	jmp	.LBB21_59
.LBB21_55:                              #   in Loop: Header=BB21_80 Depth=1
	cmpq	$0, 16(%rsp)
	movdqu	248(%rsp), %xmm0
	movdqa	%xmm0, 48(%rsp)         # 16-byte Spill
	sete	%al
	movl	$7, %ebx
	testb	15(%rsp), %al           # 1-byte Folded Reload
	jne	.LBB21_60
# BB#57:                                #   in Loop: Header=BB21_80 Depth=1
	movq	(%rbp), %rax
.Ltmp516:
.Lcfi156:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	callq	*64(%rax)
.Ltmp517:
# BB#58:                                #   in Loop: Header=BB21_80 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r12d
	je	.LBB21_68
.LBB21_59:                              #   in Loop: Header=BB21_80 Depth=1
	movl	$1, %ebx
	movl	%eax, %r12d
.LBB21_60:                              # %.thread
                                        #   in Loop: Header=BB21_80 Depth=1
	movl	8(%rsp), %r13d          # 4-byte Reload
.LBB21_61:                              # %.thread
                                        #   in Loop: Header=BB21_80 Depth=1
.Ltmp543:
.Lcfi157:
	.cfi_escape 0x2e, 0x00
	leaq	232(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp544:
# BB#62:                                #   in Loop: Header=BB21_80 Depth=1
	movdqa	48(%rsp), %xmm1         # 16-byte Reload
.LBB21_63:                              #   in Loop: Header=BB21_80 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_65
# BB#64:                                #   in Loop: Header=BB21_80 Depth=1
	movq	(%rdi), %rax
.Ltmp548:
.Lcfi158:
	.cfi_escape 0x2e, 0x00
	movdqa	%xmm1, 48(%rsp)         # 16-byte Spill
	callq	*16(%rax)
	movdqa	48(%rsp), %xmm1         # 16-byte Reload
.Ltmp549:
.LBB21_65:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit230
                                        #   in Loop: Header=BB21_80 Depth=1
	cmpl	$7, %ebx
	je	.LBB21_67
# BB#66:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit230
                                        #   in Loop: Header=BB21_80 Depth=1
	testl	%ebx, %ebx
	jne	.LBB21_84
.LBB21_67:                              #   in Loop: Header=BB21_80 Depth=1
	incq	%r15
	movdqa	112(%rsp), %xmm0        # 16-byte Reload
	paddq	%xmm1, %xmm0
	cmpq	104(%rsp), %r15         # 8-byte Folded Reload
	jb	.LBB21_80
	jmp	.LBB21_83
.LBB21_68:                              #   in Loop: Header=BB21_80 Depth=1
	movq	16(%rsp), %rcx
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	252(%rax), %eax
.Ltmp518:
.Lcfi159:
	.cfi_escape 0x2e, 0x10
	leaq	128(%rsp), %rdi
	movq	88(%rsp), %rsi          # 8-byte Reload
	leaq	232(%rsp), %rdx
	movq	%rbp, %r8
	movq	32(%rsp), %r9           # 8-byte Reload
	leaq	84(%rsp), %rbx
	pushq	%rbx
.Lcfi160:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi161:
	.cfi_adjust_cfa_offset 8
	callq	_ZN8NArchive4NZip11CZipDecoder6DecodeERNS0_10CInArchiveERKNS0_7CItemExEP20ISequentialOutStreamP23IArchiveExtractCallbackP21ICompressProgressInfojRi
	addq	$16, %rsp
.Lcfi162:
	.cfi_adjust_cfa_offset -16
.Ltmp519:
	movl	8(%rsp), %r13d          # 4-byte Reload
# BB#69:                                #   in Loop: Header=BB21_80 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r12d
	je	.LBB21_74
# BB#70:                                #   in Loop: Header=BB21_80 Depth=1
	movl	$1, %ebx
	movl	%eax, %r12d
	jmp	.LBB21_61
.LBB21_71:                              #   in Loop: Header=BB21_80 Depth=1
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	jne	.LBB21_47
# BB#72:                                #   in Loop: Header=BB21_80 Depth=1
	movq	16(%rsp), %rax
	testq	%rax, %rax
	jne	.LBB21_47
# BB#73:                                #   in Loop: Header=BB21_80 Depth=1
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 48(%rsp)         # 16-byte Spill
	movl	$7, %ebx
	jmp	.LBB21_60
.LBB21_74:                              #   in Loop: Header=BB21_80 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_77
# BB#75:                                #   in Loop: Header=BB21_80 Depth=1
	movq	(%rdi), %rax
.Ltmp520:
.Lcfi163:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp521:
# BB#76:                                # %.noexc233
                                        #   in Loop: Header=BB21_80 Depth=1
	movq	$0, 16(%rsp)
.LBB21_77:                              # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit234
                                        #   in Loop: Header=BB21_80 Depth=1
	movq	(%rbp), %rax
	movl	84(%rsp), %esi
.Ltmp522:
.Lcfi164:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	*72(%rax)
.Ltmp523:
# BB#78:                                #   in Loop: Header=BB21_80 Depth=1
	xorl	%ebx, %ebx
	testl	%eax, %eax
	setne	%bl
	cmovnel	%eax, %r12d
	jmp	.LBB21_61
.LBB21_79:                              #   in Loop: Header=BB21_80 Depth=1
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 48(%rsp)         # 16-byte Spill
	movl	$7, %ebx
	movl	%r13d, %r12d
	jmp	.LBB21_60
	.p2align	4, 0x90
.LBB21_80:                              # =>This Inner Loop Header: Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movdqa	%xmm0, 112(%rsp)        # 16-byte Spill
	movdqu	%xmm0, 48(%rdi)
.Ltmp504:
.Lcfi165:
	.cfi_escape 0x2e, 0x00
	callq	_ZN14CLocalProgress6SetCurEv
.Ltmp505:
# BB#81:                                #   in Loop: Header=BB21_80 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r12d
	je	.LBB21_25
# BB#82:
	movl	%eax, %r12d
	jmp	.LBB21_84
.LBB21_83:
	xorl	%r12d, %r12d
.LBB21_84:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit230._crit_edge
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp553:
.Lcfi166:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp554:
	jmp	.LBB21_18
.LBB21_85:
.Ltmp555:
	jmp	.LBB21_91
.LBB21_86:                              # %.loopexit.split-lp
.Ltmp503:
	jmp	.LBB21_100
.LBB21_87:
.Ltmp500:
	jmp	.LBB21_91
.LBB21_88:
.Ltmp497:
	movq	%rdx, %r14
	movq	%rax, %r15
.Lcfi167:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB21_106
.LBB21_89:
.Ltmp494:
	jmp	.LBB21_91
.LBB21_90:
.Ltmp491:
.LBB21_91:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	movq	%rdx, %r14
	movq	%rax, %r15
	jmp	.LBB21_106
.LBB21_92:
.Ltmp561:
	movq	%rdx, %r14
	movq	%rax, %r15
	jmp	.LBB21_107
.LBB21_93:
.Ltmp524:
	jmp	.LBB21_98
.LBB21_94:
.Ltmp540:
	jmp	.LBB21_98
.LBB21_95:
.Ltmp545:
	jmp	.LBB21_102
.LBB21_96:
.Ltmp550:
	jmp	.LBB21_100
.LBB21_97:
.Ltmp531:
.LBB21_98:
	movq	%rdx, %r14
	movq	%rax, %r15
.Ltmp541:
.Lcfi168:
	.cfi_escape 0x2e, 0x00
	leaq	232(%rsp), %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp542:
	movq	32(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB21_103
.LBB21_99:                              # %.loopexit
.Ltmp506:
.LBB21_100:
	movq	%rdx, %r14
	movq	%rax, %r15
	movq	32(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB21_105
.LBB21_101:
.Ltmp511:
.LBB21_102:
	movq	%rdx, %r14
	movq	%rax, %r15
	movq	32(%rsp), %rbx          # 8-byte Reload
.LBB21_103:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_105
# BB#104:
	movq	(%rdi), %rax
.Ltmp546:
.Lcfi169:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp547:
.LBB21_105:
	movq	(%rbx), %rax
.Ltmp551:
.Lcfi170:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp552:
.LBB21_106:                             # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
.Ltmp556:
.Lcfi171:
	.cfi_escape 0x2e, 0x00
	leaq	128(%rsp), %rdi
	callq	_ZN8NArchive4NZip11CZipDecoderD2Ev
.Ltmp557:
.LBB21_107:
.Lcfi172:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbp
	cmpl	$2, %r14d
	je	.LBB21_109
# BB#108:
.Lcfi173:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
	movl	$-2147024882, %r12d     # imm = 0x8007000E
	jmp	.LBB21_19
.LBB21_109:
.Lcfi174:
	.cfi_escape 0x2e, 0x00
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbp, (%rax)
.Ltmp562:
.Lcfi175:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp563:
# BB#110:
.LBB21_111:
.Ltmp564:
	movq	%rax, %rbx
.Lcfi176:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
.Lcfi177:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB21_112:
.Ltmp558:
.Lcfi178:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end21:
	.size	_ZN8NArchive4NZip8CHandler7ExtractEPKjjiP23IArchiveExtractCallback, .Lfunc_end21-_ZN8NArchive4NZip8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\206\202\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\367\001"              # Call site table length
	.long	.Ltmp489-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp490-.Ltmp489       #   Call between .Ltmp489 and .Ltmp490
	.long	.Ltmp491-.Lfunc_begin9  #     jumps to .Ltmp491
	.byte	3                       #   On action: 2
	.long	.Ltmp559-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp560-.Ltmp559       #   Call between .Ltmp559 and .Ltmp560
	.long	.Ltmp561-.Lfunc_begin9  #     jumps to .Ltmp561
	.byte	3                       #   On action: 2
	.long	.Ltmp492-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp493-.Ltmp492       #   Call between .Ltmp492 and .Ltmp493
	.long	.Ltmp494-.Lfunc_begin9  #     jumps to .Ltmp494
	.byte	3                       #   On action: 2
	.long	.Ltmp495-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Ltmp496-.Ltmp495       #   Call between .Ltmp495 and .Ltmp496
	.long	.Ltmp497-.Lfunc_begin9  #     jumps to .Ltmp497
	.byte	3                       #   On action: 2
	.long	.Ltmp498-.Lfunc_begin9  # >> Call Site 5 <<
	.long	.Ltmp499-.Ltmp498       #   Call between .Ltmp498 and .Ltmp499
	.long	.Ltmp500-.Lfunc_begin9  #     jumps to .Ltmp500
	.byte	3                       #   On action: 2
	.long	.Ltmp501-.Lfunc_begin9  # >> Call Site 6 <<
	.long	.Ltmp502-.Ltmp501       #   Call between .Ltmp501 and .Ltmp502
	.long	.Ltmp503-.Lfunc_begin9  #     jumps to .Ltmp503
	.byte	3                       #   On action: 2
	.long	.Ltmp507-.Lfunc_begin9  # >> Call Site 7 <<
	.long	.Ltmp510-.Ltmp507       #   Call between .Ltmp507 and .Ltmp510
	.long	.Ltmp511-.Lfunc_begin9  #     jumps to .Ltmp511
	.byte	3                       #   On action: 2
	.long	.Ltmp514-.Lfunc_begin9  # >> Call Site 8 <<
	.long	.Ltmp513-.Ltmp514       #   Call between .Ltmp514 and .Ltmp513
	.long	.Ltmp531-.Lfunc_begin9  #     jumps to .Ltmp531
	.byte	3                       #   On action: 2
	.long	.Ltmp532-.Lfunc_begin9  # >> Call Site 9 <<
	.long	.Ltmp539-.Ltmp532       #   Call between .Ltmp532 and .Ltmp539
	.long	.Ltmp540-.Lfunc_begin9  #     jumps to .Ltmp540
	.byte	3                       #   On action: 2
	.long	.Ltmp516-.Lfunc_begin9  # >> Call Site 10 <<
	.long	.Ltmp517-.Ltmp516       #   Call between .Ltmp516 and .Ltmp517
	.long	.Ltmp531-.Lfunc_begin9  #     jumps to .Ltmp531
	.byte	3                       #   On action: 2
	.long	.Ltmp543-.Lfunc_begin9  # >> Call Site 11 <<
	.long	.Ltmp544-.Ltmp543       #   Call between .Ltmp543 and .Ltmp544
	.long	.Ltmp545-.Lfunc_begin9  #     jumps to .Ltmp545
	.byte	3                       #   On action: 2
	.long	.Ltmp548-.Lfunc_begin9  # >> Call Site 12 <<
	.long	.Ltmp549-.Ltmp548       #   Call between .Ltmp548 and .Ltmp549
	.long	.Ltmp550-.Lfunc_begin9  #     jumps to .Ltmp550
	.byte	3                       #   On action: 2
	.long	.Ltmp518-.Lfunc_begin9  # >> Call Site 13 <<
	.long	.Ltmp523-.Ltmp518       #   Call between .Ltmp518 and .Ltmp523
	.long	.Ltmp524-.Lfunc_begin9  #     jumps to .Ltmp524
	.byte	3                       #   On action: 2
	.long	.Ltmp504-.Lfunc_begin9  # >> Call Site 14 <<
	.long	.Ltmp505-.Ltmp504       #   Call between .Ltmp504 and .Ltmp505
	.long	.Ltmp506-.Lfunc_begin9  #     jumps to .Ltmp506
	.byte	3                       #   On action: 2
	.long	.Ltmp553-.Lfunc_begin9  # >> Call Site 15 <<
	.long	.Ltmp554-.Ltmp553       #   Call between .Ltmp553 and .Ltmp554
	.long	.Ltmp555-.Lfunc_begin9  #     jumps to .Ltmp555
	.byte	3                       #   On action: 2
	.long	.Ltmp541-.Lfunc_begin9  # >> Call Site 16 <<
	.long	.Ltmp557-.Ltmp541       #   Call between .Ltmp541 and .Ltmp557
	.long	.Ltmp558-.Lfunc_begin9  #     jumps to .Ltmp558
	.byte	1                       #   On action: 1
	.long	.Ltmp557-.Lfunc_begin9  # >> Call Site 17 <<
	.long	.Ltmp562-.Ltmp557       #   Call between .Ltmp557 and .Ltmp562
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp562-.Lfunc_begin9  # >> Call Site 18 <<
	.long	.Ltmp563-.Ltmp562       #   Call between .Ltmp562 and .Ltmp563
	.long	.Ltmp564-.Lfunc_begin9  #     jumps to .Ltmp564
	.byte	0                       #   On action: cleanup
	.long	.Ltmp563-.Lfunc_begin9  # >> Call Site 19 <<
	.long	.Lfunc_end21-.Ltmp563   #   Call between .Ltmp563 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NZip5CItemD2Ev,"axG",@progbits,_ZN8NArchive4NZip5CItemD2Ev,comdat
	.weak	_ZN8NArchive4NZip5CItemD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip5CItemD2Ev,@function
_ZN8NArchive4NZip5CItemD2Ev:            # @_ZN8NArchive4NZip5CItemD2Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r15
.Lcfi179:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi180:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi181:
	.cfi_def_cfa_offset 32
.Lcfi182:
	.cfi_offset %rbx, -32
.Lcfi183:
	.cfi_offset %r14, -24
.Lcfi184:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	$_ZTV7CBufferIhE+16, 152(%r15)
	movq	168(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB22_2
# BB#1:
	callq	_ZdaPv
.LBB22_2:                               # %_ZN7CBufferIhED2Ev.exit
	leaq	120(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 120(%r15)
.Ltmp565:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp566:
# BB#3:                                 # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit.i
.Ltmp571:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp572:
# BB#4:                                 # %_ZN8NArchive4NZip11CExtraBlockD2Ev.exit
	leaq	48(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 48(%r15)
.Ltmp583:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp584:
# BB#5:                                 # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit.i.i
.Ltmp589:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp590:
# BB#6:                                 # %_ZN8NArchive4NZip11CExtraBlockD2Ev.exit.i
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB22_16
# BB#7:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdaPv                  # TAILCALL
.LBB22_16:                              # %_ZN8NArchive4NZip10CLocalItemD2Ev.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB22_12:
.Ltmp591:
	movq	%rax, %r14
	jmp	.LBB22_13
.LBB22_10:
.Ltmp585:
	movq	%rax, %r14
.Ltmp586:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp587:
	jmp	.LBB22_13
.LBB22_11:
.Ltmp588:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB22_17:
.Ltmp573:
	movq	%rax, %r14
	jmp	.LBB22_18
.LBB22_8:
.Ltmp567:
	movq	%rax, %r14
.Ltmp568:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp569:
.LBB22_18:                              # %.body
	leaq	48(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 48(%r15)
.Ltmp574:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp575:
# BB#19:                                # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit.i.i2
.Ltmp580:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp581:
.LBB22_13:                              # %.body.i
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB22_15
# BB#14:
	callq	_ZdaPv
.LBB22_15:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_9:
.Ltmp570:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB22_22:
.Ltmp582:
	movq	%rax, %r14
	jmp	.LBB22_23
.LBB22_20:
.Ltmp576:
	movq	%rax, %r14
.Ltmp577:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp578:
.LBB22_23:                              # %.body.i5
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB22_25
# BB#24:
	callq	_ZdaPv
.LBB22_25:                              # %.body7
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB22_21:
.Ltmp579:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZN8NArchive4NZip5CItemD2Ev, .Lfunc_end22-_ZN8NArchive4NZip5CItemD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp565-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp566-.Ltmp565       #   Call between .Ltmp565 and .Ltmp566
	.long	.Ltmp567-.Lfunc_begin10 #     jumps to .Ltmp567
	.byte	0                       #   On action: cleanup
	.long	.Ltmp571-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp572-.Ltmp571       #   Call between .Ltmp571 and .Ltmp572
	.long	.Ltmp573-.Lfunc_begin10 #     jumps to .Ltmp573
	.byte	0                       #   On action: cleanup
	.long	.Ltmp583-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp584-.Ltmp583       #   Call between .Ltmp583 and .Ltmp584
	.long	.Ltmp585-.Lfunc_begin10 #     jumps to .Ltmp585
	.byte	0                       #   On action: cleanup
	.long	.Ltmp589-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Ltmp590-.Ltmp589       #   Call between .Ltmp589 and .Ltmp590
	.long	.Ltmp591-.Lfunc_begin10 #     jumps to .Ltmp591
	.byte	0                       #   On action: cleanup
	.long	.Ltmp586-.Lfunc_begin10 # >> Call Site 5 <<
	.long	.Ltmp587-.Ltmp586       #   Call between .Ltmp586 and .Ltmp587
	.long	.Ltmp588-.Lfunc_begin10 #     jumps to .Ltmp588
	.byte	1                       #   On action: 1
	.long	.Ltmp568-.Lfunc_begin10 # >> Call Site 6 <<
	.long	.Ltmp569-.Ltmp568       #   Call between .Ltmp568 and .Ltmp569
	.long	.Ltmp570-.Lfunc_begin10 #     jumps to .Ltmp570
	.byte	1                       #   On action: 1
	.long	.Ltmp574-.Lfunc_begin10 # >> Call Site 7 <<
	.long	.Ltmp575-.Ltmp574       #   Call between .Ltmp574 and .Ltmp575
	.long	.Ltmp576-.Lfunc_begin10 #     jumps to .Ltmp576
	.byte	1                       #   On action: 1
	.long	.Ltmp580-.Lfunc_begin10 # >> Call Site 8 <<
	.long	.Ltmp581-.Ltmp580       #   Call between .Ltmp580 and .Ltmp581
	.long	.Ltmp582-.Lfunc_begin10 #     jumps to .Ltmp582
	.byte	1                       #   On action: 1
	.long	.Ltmp581-.Lfunc_begin10 # >> Call Site 9 <<
	.long	.Ltmp577-.Ltmp581       #   Call between .Ltmp581 and .Ltmp577
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp577-.Lfunc_begin10 # >> Call Site 10 <<
	.long	.Ltmp578-.Ltmp577       #   Call between .Ltmp577 and .Ltmp578
	.long	.Ltmp579-.Lfunc_begin10 #     jumps to .Ltmp579
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NZip11CZipDecoderD2Ev,"axG",@progbits,_ZN8NArchive4NZip11CZipDecoderD2Ev,comdat
	.weak	_ZN8NArchive4NZip11CZipDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip11CZipDecoderD2Ev,@function
_ZN8NArchive4NZip11CZipDecoderD2Ev:     # @_ZN8NArchive4NZip11CZipDecoderD2Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r15
.Lcfi185:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi186:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi187:
	.cfi_def_cfa_offset 32
.Lcfi188:
	.cfi_offset %rbx, -32
.Lcfi189:
	.cfi_offset %r14, -24
.Lcfi190:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	72(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CMethodItemEE+16, 72(%r15)
.Ltmp592:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp593:
# BB#1:
.Ltmp598:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp599:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED2Ev.exit
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB23_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp603:
	callq	*16(%rax)
.Ltmp604:
.LBB23_4:                               # %_ZN9CMyComPtrI22ICryptoGetTextPasswordED2Ev.exit
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB23_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp608:
	callq	*16(%rax)
.Ltmp609:
.LBB23_6:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB23_8
# BB#7:
	movq	(%rdi), %rax
.Ltmp613:
	callq	*16(%rax)
.Ltmp614:
.LBB23_8:                               # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB23_10
# BB#9:
	movq	(%rdi), %rax
.Ltmp618:
	callq	*16(%rax)
.Ltmp619:
.LBB23_10:                              # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit9
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB23_14
# BB#11:
	movq	(%rdi), %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*16(%rax)               # TAILCALL
.LBB23_14:                              # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit10
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB23_24:
.Ltmp620:
	movq	%rax, %r14
	jmp	.LBB23_28
.LBB23_25:
.Ltmp615:
	movq	%rax, %r14
	jmp	.LBB23_26
.LBB23_21:
.Ltmp610:
	movq	%rax, %r14
	jmp	.LBB23_22
.LBB23_18:
.Ltmp605:
	movq	%rax, %r14
	jmp	.LBB23_19
.LBB23_15:
.Ltmp600:
	movq	%rax, %r14
	jmp	.LBB23_16
.LBB23_12:
.Ltmp594:
	movq	%rax, %r14
.Ltmp595:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp596:
.LBB23_16:                              # %.body
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB23_19
# BB#17:
	movq	(%rdi), %rax
.Ltmp601:
	callq	*16(%rax)
.Ltmp602:
.LBB23_19:                              # %_ZN9CMyComPtrI22ICryptoGetTextPasswordED2Ev.exit12
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB23_22
# BB#20:
	movq	(%rdi), %rax
.Ltmp606:
	callq	*16(%rax)
.Ltmp607:
.LBB23_22:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit14
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB23_26
# BB#23:
	movq	(%rdi), %rax
.Ltmp611:
	callq	*16(%rax)
.Ltmp612:
.LBB23_26:                              # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit16
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB23_28
# BB#27:
	movq	(%rdi), %rax
.Ltmp616:
	callq	*16(%rax)
.Ltmp617:
.LBB23_28:                              # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit18
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB23_30
# BB#29:
	movq	(%rdi), %rax
.Ltmp621:
	callq	*16(%rax)
.Ltmp622:
.LBB23_30:                              # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit20
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB23_13:
.Ltmp597:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB23_31:
.Ltmp623:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end23:
	.size	_ZN8NArchive4NZip11CZipDecoderD2Ev, .Lfunc_end23-_ZN8NArchive4NZip11CZipDecoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp592-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp593-.Ltmp592       #   Call between .Ltmp592 and .Ltmp593
	.long	.Ltmp594-.Lfunc_begin11 #     jumps to .Ltmp594
	.byte	0                       #   On action: cleanup
	.long	.Ltmp598-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp599-.Ltmp598       #   Call between .Ltmp598 and .Ltmp599
	.long	.Ltmp600-.Lfunc_begin11 #     jumps to .Ltmp600
	.byte	0                       #   On action: cleanup
	.long	.Ltmp603-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp604-.Ltmp603       #   Call between .Ltmp603 and .Ltmp604
	.long	.Ltmp605-.Lfunc_begin11 #     jumps to .Ltmp605
	.byte	0                       #   On action: cleanup
	.long	.Ltmp608-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Ltmp609-.Ltmp608       #   Call between .Ltmp608 and .Ltmp609
	.long	.Ltmp610-.Lfunc_begin11 #     jumps to .Ltmp610
	.byte	0                       #   On action: cleanup
	.long	.Ltmp613-.Lfunc_begin11 # >> Call Site 5 <<
	.long	.Ltmp614-.Ltmp613       #   Call between .Ltmp613 and .Ltmp614
	.long	.Ltmp615-.Lfunc_begin11 #     jumps to .Ltmp615
	.byte	0                       #   On action: cleanup
	.long	.Ltmp618-.Lfunc_begin11 # >> Call Site 6 <<
	.long	.Ltmp619-.Ltmp618       #   Call between .Ltmp618 and .Ltmp619
	.long	.Ltmp620-.Lfunc_begin11 #     jumps to .Ltmp620
	.byte	0                       #   On action: cleanup
	.long	.Ltmp619-.Lfunc_begin11 # >> Call Site 7 <<
	.long	.Ltmp595-.Ltmp619       #   Call between .Ltmp619 and .Ltmp595
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp595-.Lfunc_begin11 # >> Call Site 8 <<
	.long	.Ltmp596-.Ltmp595       #   Call between .Ltmp595 and .Ltmp596
	.long	.Ltmp597-.Lfunc_begin11 #     jumps to .Ltmp597
	.byte	1                       #   On action: 1
	.long	.Ltmp601-.Lfunc_begin11 # >> Call Site 9 <<
	.long	.Ltmp622-.Ltmp601       #   Call between .Ltmp601 and .Ltmp622
	.long	.Ltmp623-.Lfunc_begin11 #     jumps to .Ltmp623
	.byte	1                       #   On action: 1
	.long	.Ltmp622-.Lfunc_begin11 # >> Call Site 10 <<
	.long	.Lfunc_end23-.Ltmp622   #   Call between .Ltmp622 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi191:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB24_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB24_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB24_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB24_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB24_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB24_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB24_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB24_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB24_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB24_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB24_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB24_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB24_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB24_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB24_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB24_16
.LBB24_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_IInArchive(%rip), %cl
	jne	.LBB24_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_IInArchive+1(%rip), %al
	jne	.LBB24_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_IInArchive+2(%rip), %al
	jne	.LBB24_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_IInArchive+3(%rip), %al
	jne	.LBB24_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_IInArchive+4(%rip), %al
	jne	.LBB24_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_IInArchive+5(%rip), %al
	jne	.LBB24_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_IInArchive+6(%rip), %al
	jne	.LBB24_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_IInArchive+7(%rip), %al
	jne	.LBB24_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_IInArchive+8(%rip), %al
	jne	.LBB24_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_IInArchive+9(%rip), %al
	jne	.LBB24_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_IInArchive+10(%rip), %al
	jne	.LBB24_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_IInArchive+11(%rip), %al
	jne	.LBB24_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_IInArchive+12(%rip), %al
	jne	.LBB24_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_IInArchive+13(%rip), %al
	jne	.LBB24_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_IInArchive+14(%rip), %al
	jne	.LBB24_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit10
	movb	15(%rsi), %al
	cmpb	IID_IInArchive+15(%rip), %al
	jne	.LBB24_33
.LBB24_16:
	movq	%rdi, (%rdx)
	jmp	.LBB24_68
.LBB24_33:                              # %_ZeqRK4GUIDS1_.exit10.thread
	cmpb	IID_IOutArchive(%rip), %cl
	jne	.LBB24_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_IOutArchive+1(%rip), %al
	jne	.LBB24_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_IOutArchive+2(%rip), %al
	jne	.LBB24_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_IOutArchive+3(%rip), %al
	jne	.LBB24_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_IOutArchive+4(%rip), %al
	jne	.LBB24_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_IOutArchive+5(%rip), %al
	jne	.LBB24_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_IOutArchive+6(%rip), %al
	jne	.LBB24_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_IOutArchive+7(%rip), %al
	jne	.LBB24_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_IOutArchive+8(%rip), %al
	jne	.LBB24_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_IOutArchive+9(%rip), %al
	jne	.LBB24_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_IOutArchive+10(%rip), %al
	jne	.LBB24_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_IOutArchive+11(%rip), %al
	jne	.LBB24_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_IOutArchive+12(%rip), %al
	jne	.LBB24_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_IOutArchive+13(%rip), %al
	jne	.LBB24_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_IOutArchive+14(%rip), %al
	jne	.LBB24_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %al
	cmpb	IID_IOutArchive+15(%rip), %al
	jne	.LBB24_50
# BB#49:
	leaq	8(%rdi), %rax
	jmp	.LBB24_67
.LBB24_50:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ISetProperties(%rip), %cl
	jne	.LBB24_69
# BB#51:
	movb	1(%rsi), %cl
	cmpb	IID_ISetProperties+1(%rip), %cl
	jne	.LBB24_69
# BB#52:
	movb	2(%rsi), %cl
	cmpb	IID_ISetProperties+2(%rip), %cl
	jne	.LBB24_69
# BB#53:
	movb	3(%rsi), %cl
	cmpb	IID_ISetProperties+3(%rip), %cl
	jne	.LBB24_69
# BB#54:
	movb	4(%rsi), %cl
	cmpb	IID_ISetProperties+4(%rip), %cl
	jne	.LBB24_69
# BB#55:
	movb	5(%rsi), %cl
	cmpb	IID_ISetProperties+5(%rip), %cl
	jne	.LBB24_69
# BB#56:
	movb	6(%rsi), %cl
	cmpb	IID_ISetProperties+6(%rip), %cl
	jne	.LBB24_69
# BB#57:
	movb	7(%rsi), %cl
	cmpb	IID_ISetProperties+7(%rip), %cl
	jne	.LBB24_69
# BB#58:
	movb	8(%rsi), %cl
	cmpb	IID_ISetProperties+8(%rip), %cl
	jne	.LBB24_69
# BB#59:
	movb	9(%rsi), %cl
	cmpb	IID_ISetProperties+9(%rip), %cl
	jne	.LBB24_69
# BB#60:
	movb	10(%rsi), %cl
	cmpb	IID_ISetProperties+10(%rip), %cl
	jne	.LBB24_69
# BB#61:
	movb	11(%rsi), %cl
	cmpb	IID_ISetProperties+11(%rip), %cl
	jne	.LBB24_69
# BB#62:
	movb	12(%rsi), %cl
	cmpb	IID_ISetProperties+12(%rip), %cl
	jne	.LBB24_69
# BB#63:
	movb	13(%rsi), %cl
	cmpb	IID_ISetProperties+13(%rip), %cl
	jne	.LBB24_69
# BB#64:
	movb	14(%rsi), %cl
	cmpb	IID_ISetProperties+14(%rip), %cl
	jne	.LBB24_69
# BB#65:                                # %_ZeqRK4GUIDS1_.exit12
	movb	15(%rsi), %cl
	cmpb	IID_ISetProperties+15(%rip), %cl
	jne	.LBB24_69
# BB#66:
	leaq	16(%rdi), %rax
.LBB24_67:                              # %_ZeqRK4GUIDS1_.exit12.thread
	movq	%rax, (%rdx)
.LBB24_68:                              # %_ZeqRK4GUIDS1_.exit12.thread
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB24_69:                              # %_ZeqRK4GUIDS1_.exit12.thread
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZN8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end24-_ZN8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip8CHandler6AddRefEv,"axG",@progbits,_ZN8NArchive4NZip8CHandler6AddRefEv,comdat
	.weak	_ZN8NArchive4NZip8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler6AddRefEv,@function
_ZN8NArchive4NZip8CHandler6AddRefEv:    # @_ZN8NArchive4NZip8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end25:
	.size	_ZN8NArchive4NZip8CHandler6AddRefEv, .Lfunc_end25-_ZN8NArchive4NZip8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip8CHandler7ReleaseEv,"axG",@progbits,_ZN8NArchive4NZip8CHandler7ReleaseEv,comdat
	.weak	_ZN8NArchive4NZip8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler7ReleaseEv,@function
_ZN8NArchive4NZip8CHandler7ReleaseEv:   # @_ZN8NArchive4NZip8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi192:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB26_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB26_2:
	popq	%rcx
	retq
.Lfunc_end26:
	.size	_ZN8NArchive4NZip8CHandler7ReleaseEv, .Lfunc_end26-_ZN8NArchive4NZip8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip8CHandlerD2Ev,"axG",@progbits,_ZN8NArchive4NZip8CHandlerD2Ev,comdat
	.weak	_ZN8NArchive4NZip8CHandlerD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandlerD2Ev,@function
_ZN8NArchive4NZip8CHandlerD2Ev:         # @_ZN8NArchive4NZip8CHandlerD2Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi193:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi194:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi195:
	.cfi_def_cfa_offset 32
.Lcfi196:
	.cfi_offset %rbx, -24
.Lcfi197:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive4NZip8CHandlerE+176, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NZip8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN8NArchive4NZip8CHandlerE+248, 16(%rbx)
	movq	$_ZTV7CBufferIhE+16, 176(%rbx)
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_2
# BB#1:
	callq	_ZdaPv
.LBB27_2:                               # %_ZN8NArchive4NZip14CInArchiveInfoD2Ev.exit.i
	leaq	104(%rbx), %rdi
.Ltmp624:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp625:
# BB#3:
	movq	128(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp630:
	callq	*16(%rax)
.Ltmp631:
.LBB27_5:                               # %_ZN9CInBufferD2Ev.exit.i
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_7
# BB#6:
	movq	(%rdi), %rax
.Ltmp636:
	callq	*16(%rax)
.Ltmp637:
.LBB27_7:                               # %_ZN8NArchive4NZip10CInArchiveD2Ev.exit
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip7CItemExEE+16, 32(%rbx)
	addq	$32, %rbx
.Ltmp648:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp649:
# BB#8:
.Ltmp654:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp655:
# BB#9:                                 # %_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB27_19:
.Ltmp638:
	movq	%rax, %r14
	jmp	.LBB27_20
.LBB27_13:
.Ltmp632:
	movq	%rax, %r14
	jmp	.LBB27_14
.LBB27_24:
.Ltmp656:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB27_17:
.Ltmp650:
	movq	%rax, %r14
.Ltmp651:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp652:
	jmp	.LBB27_25
.LBB27_18:
.Ltmp653:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB27_10:
.Ltmp626:
	movq	%rax, %r14
	movq	128(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_14
# BB#11:
	movq	(%rdi), %rax
.Ltmp627:
	callq	*16(%rax)
.Ltmp628:
.LBB27_14:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_20
# BB#15:
	movq	(%rdi), %rax
.Ltmp633:
	callq	*16(%rax)
.Ltmp634:
.LBB27_20:                              # %.body
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip7CItemExEE+16, 32(%rbx)
	addq	$32, %rbx
.Ltmp639:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp640:
# BB#21:
.Ltmp645:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp646:
.LBB27_25:                              # %_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED2Ev.exit11
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB27_12:
.Ltmp629:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB27_16:                              # %.body5.i
.Ltmp635:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB27_26:
.Ltmp647:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB27_22:
.Ltmp641:
	movq	%rax, %r14
.Ltmp642:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp643:
# BB#27:                                # %.body9
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB27_23:
.Ltmp644:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end27:
	.size	_ZN8NArchive4NZip8CHandlerD2Ev, .Lfunc_end27-_ZN8NArchive4NZip8CHandlerD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\262\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\251\001"              # Call site table length
	.long	.Ltmp624-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp625-.Ltmp624       #   Call between .Ltmp624 and .Ltmp625
	.long	.Ltmp626-.Lfunc_begin12 #     jumps to .Ltmp626
	.byte	0                       #   On action: cleanup
	.long	.Ltmp630-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp631-.Ltmp630       #   Call between .Ltmp630 and .Ltmp631
	.long	.Ltmp632-.Lfunc_begin12 #     jumps to .Ltmp632
	.byte	0                       #   On action: cleanup
	.long	.Ltmp636-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp637-.Ltmp636       #   Call between .Ltmp636 and .Ltmp637
	.long	.Ltmp638-.Lfunc_begin12 #     jumps to .Ltmp638
	.byte	0                       #   On action: cleanup
	.long	.Ltmp648-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Ltmp649-.Ltmp648       #   Call between .Ltmp648 and .Ltmp649
	.long	.Ltmp650-.Lfunc_begin12 #     jumps to .Ltmp650
	.byte	0                       #   On action: cleanup
	.long	.Ltmp654-.Lfunc_begin12 # >> Call Site 5 <<
	.long	.Ltmp655-.Ltmp654       #   Call between .Ltmp654 and .Ltmp655
	.long	.Ltmp656-.Lfunc_begin12 #     jumps to .Ltmp656
	.byte	0                       #   On action: cleanup
	.long	.Ltmp655-.Lfunc_begin12 # >> Call Site 6 <<
	.long	.Ltmp651-.Ltmp655       #   Call between .Ltmp655 and .Ltmp651
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp651-.Lfunc_begin12 # >> Call Site 7 <<
	.long	.Ltmp652-.Ltmp651       #   Call between .Ltmp651 and .Ltmp652
	.long	.Ltmp653-.Lfunc_begin12 #     jumps to .Ltmp653
	.byte	1                       #   On action: 1
	.long	.Ltmp627-.Lfunc_begin12 # >> Call Site 8 <<
	.long	.Ltmp628-.Ltmp627       #   Call between .Ltmp627 and .Ltmp628
	.long	.Ltmp629-.Lfunc_begin12 #     jumps to .Ltmp629
	.byte	1                       #   On action: 1
	.long	.Ltmp633-.Lfunc_begin12 # >> Call Site 9 <<
	.long	.Ltmp634-.Ltmp633       #   Call between .Ltmp633 and .Ltmp634
	.long	.Ltmp635-.Lfunc_begin12 #     jumps to .Ltmp635
	.byte	1                       #   On action: 1
	.long	.Ltmp639-.Lfunc_begin12 # >> Call Site 10 <<
	.long	.Ltmp640-.Ltmp639       #   Call between .Ltmp639 and .Ltmp640
	.long	.Ltmp641-.Lfunc_begin12 #     jumps to .Ltmp641
	.byte	1                       #   On action: 1
	.long	.Ltmp645-.Lfunc_begin12 # >> Call Site 11 <<
	.long	.Ltmp646-.Ltmp645       #   Call between .Ltmp645 and .Ltmp646
	.long	.Ltmp647-.Lfunc_begin12 #     jumps to .Ltmp647
	.byte	1                       #   On action: 1
	.long	.Ltmp646-.Lfunc_begin12 # >> Call Site 12 <<
	.long	.Ltmp642-.Ltmp646       #   Call between .Ltmp646 and .Ltmp642
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp642-.Lfunc_begin12 # >> Call Site 13 <<
	.long	.Ltmp643-.Ltmp642       #   Call between .Ltmp642 and .Ltmp643
	.long	.Ltmp644-.Lfunc_begin12 #     jumps to .Ltmp644
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NZip8CHandlerD0Ev,"axG",@progbits,_ZN8NArchive4NZip8CHandlerD0Ev,comdat
	.weak	_ZN8NArchive4NZip8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandlerD0Ev,@function
_ZN8NArchive4NZip8CHandlerD0Ev:         # @_ZN8NArchive4NZip8CHandlerD0Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi198:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi199:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi200:
	.cfi_def_cfa_offset 32
.Lcfi201:
	.cfi_offset %rbx, -24
.Lcfi202:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp657:
	callq	_ZN8NArchive4NZip8CHandlerD2Ev
.Ltmp658:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB28_2:
.Ltmp659:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end28:
	.size	_ZN8NArchive4NZip8CHandlerD0Ev, .Lfunc_end28-_ZN8NArchive4NZip8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp657-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp658-.Ltmp657       #   Call between .Ltmp657 and .Ltmp658
	.long	.Ltmp659-.Lfunc_begin13 #     jumps to .Ltmp659
	.byte	0                       #   On action: cleanup
	.long	.Ltmp658-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Lfunc_end28-.Ltmp658   #   Call between .Ltmp658 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end29:
	.size	_ZThn8_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end29-_ZThn8_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive4NZip8CHandler6AddRefEv,"axG",@progbits,_ZThn8_N8NArchive4NZip8CHandler6AddRefEv,comdat
	.weak	_ZThn8_N8NArchive4NZip8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NZip8CHandler6AddRefEv,@function
_ZThn8_N8NArchive4NZip8CHandler6AddRefEv: # @_ZThn8_N8NArchive4NZip8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end30:
	.size	_ZThn8_N8NArchive4NZip8CHandler6AddRefEv, .Lfunc_end30-_ZThn8_N8NArchive4NZip8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive4NZip8CHandler7ReleaseEv,"axG",@progbits,_ZThn8_N8NArchive4NZip8CHandler7ReleaseEv,comdat
	.weak	_ZThn8_N8NArchive4NZip8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NZip8CHandler7ReleaseEv,@function
_ZThn8_N8NArchive4NZip8CHandler7ReleaseEv: # @_ZThn8_N8NArchive4NZip8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi203:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB31_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB31_2:                               # %_ZN8NArchive4NZip8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end31:
	.size	_ZThn8_N8NArchive4NZip8CHandler7ReleaseEv, .Lfunc_end31-_ZThn8_N8NArchive4NZip8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive4NZip8CHandlerD1Ev,"axG",@progbits,_ZThn8_N8NArchive4NZip8CHandlerD1Ev,comdat
	.weak	_ZThn8_N8NArchive4NZip8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NZip8CHandlerD1Ev,@function
_ZThn8_N8NArchive4NZip8CHandlerD1Ev:    # @_ZThn8_N8NArchive4NZip8CHandlerD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN8NArchive4NZip8CHandlerD2Ev # TAILCALL
.Lfunc_end32:
	.size	_ZThn8_N8NArchive4NZip8CHandlerD1Ev, .Lfunc_end32-_ZThn8_N8NArchive4NZip8CHandlerD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive4NZip8CHandlerD0Ev,"axG",@progbits,_ZThn8_N8NArchive4NZip8CHandlerD0Ev,comdat
	.weak	_ZThn8_N8NArchive4NZip8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NZip8CHandlerD0Ev,@function
_ZThn8_N8NArchive4NZip8CHandlerD0Ev:    # @_ZThn8_N8NArchive4NZip8CHandlerD0Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi204:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi205:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi206:
	.cfi_def_cfa_offset 32
.Lcfi207:
	.cfi_offset %rbx, -24
.Lcfi208:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp660:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NZip8CHandlerD2Ev
.Ltmp661:
# BB#1:                                 # %_ZN8NArchive4NZip8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB33_2:
.Ltmp662:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end33:
	.size	_ZThn8_N8NArchive4NZip8CHandlerD0Ev, .Lfunc_end33-_ZThn8_N8NArchive4NZip8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp660-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp661-.Ltmp660       #   Call between .Ltmp660 and .Ltmp661
	.long	.Ltmp662-.Lfunc_begin14 #     jumps to .Ltmp662
	.byte	0                       #   On action: cleanup
	.long	.Ltmp661-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Lfunc_end33-.Ltmp661   #   Call between .Ltmp661 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn16_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end34:
	.size	_ZThn16_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end34-_ZThn16_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive4NZip8CHandler6AddRefEv,"axG",@progbits,_ZThn16_N8NArchive4NZip8CHandler6AddRefEv,comdat
	.weak	_ZThn16_N8NArchive4NZip8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NZip8CHandler6AddRefEv,@function
_ZThn16_N8NArchive4NZip8CHandler6AddRefEv: # @_ZThn16_N8NArchive4NZip8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end35:
	.size	_ZThn16_N8NArchive4NZip8CHandler6AddRefEv, .Lfunc_end35-_ZThn16_N8NArchive4NZip8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive4NZip8CHandler7ReleaseEv,"axG",@progbits,_ZThn16_N8NArchive4NZip8CHandler7ReleaseEv,comdat
	.weak	_ZThn16_N8NArchive4NZip8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NZip8CHandler7ReleaseEv,@function
_ZThn16_N8NArchive4NZip8CHandler7ReleaseEv: # @_ZThn16_N8NArchive4NZip8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi209:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB36_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB36_2:                               # %_ZN8NArchive4NZip8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end36:
	.size	_ZThn16_N8NArchive4NZip8CHandler7ReleaseEv, .Lfunc_end36-_ZThn16_N8NArchive4NZip8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive4NZip8CHandlerD1Ev,"axG",@progbits,_ZThn16_N8NArchive4NZip8CHandlerD1Ev,comdat
	.weak	_ZThn16_N8NArchive4NZip8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NZip8CHandlerD1Ev,@function
_ZThn16_N8NArchive4NZip8CHandlerD1Ev:   # @_ZThn16_N8NArchive4NZip8CHandlerD1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN8NArchive4NZip8CHandlerD2Ev # TAILCALL
.Lfunc_end37:
	.size	_ZThn16_N8NArchive4NZip8CHandlerD1Ev, .Lfunc_end37-_ZThn16_N8NArchive4NZip8CHandlerD1Ev
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive4NZip8CHandlerD0Ev,"axG",@progbits,_ZThn16_N8NArchive4NZip8CHandlerD0Ev,comdat
	.weak	_ZThn16_N8NArchive4NZip8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NZip8CHandlerD0Ev,@function
_ZThn16_N8NArchive4NZip8CHandlerD0Ev:   # @_ZThn16_N8NArchive4NZip8CHandlerD0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi210:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi211:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi212:
	.cfi_def_cfa_offset 32
.Lcfi213:
	.cfi_offset %rbx, -24
.Lcfi214:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp663:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NZip8CHandlerD2Ev
.Ltmp664:
# BB#1:                                 # %_ZN8NArchive4NZip8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB38_2:
.Ltmp665:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end38:
	.size	_ZThn16_N8NArchive4NZip8CHandlerD0Ev, .Lfunc_end38-_ZThn16_N8NArchive4NZip8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table38:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp663-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp664-.Ltmp663       #   Call between .Ltmp663 and .Ltmp664
	.long	.Ltmp665-.Lfunc_begin15 #     jumps to .Ltmp665
	.byte	0                       #   On action: cleanup
	.long	.Ltmp664-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Lfunc_end38-.Ltmp664   #   Call between .Ltmp664 and .Lfunc_end38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive4NZip12CLzmaDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive4NZip12CLzmaDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive4NZip12CLzmaDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CLzmaDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive4NZip12CLzmaDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive4NZip12CLzmaDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi215:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB39_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB39_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB39_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB39_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB39_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB39_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB39_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB39_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB39_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB39_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB39_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB39_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB39_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB39_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB39_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB39_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB39_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end39:
	.size	_ZN8NArchive4NZip12CLzmaDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end39-_ZN8NArchive4NZip12CLzmaDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip12CLzmaDecoder6AddRefEv,"axG",@progbits,_ZN8NArchive4NZip12CLzmaDecoder6AddRefEv,comdat
	.weak	_ZN8NArchive4NZip12CLzmaDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CLzmaDecoder6AddRefEv,@function
_ZN8NArchive4NZip12CLzmaDecoder6AddRefEv: # @_ZN8NArchive4NZip12CLzmaDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end40:
	.size	_ZN8NArchive4NZip12CLzmaDecoder6AddRefEv, .Lfunc_end40-_ZN8NArchive4NZip12CLzmaDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip12CLzmaDecoder7ReleaseEv,"axG",@progbits,_ZN8NArchive4NZip12CLzmaDecoder7ReleaseEv,comdat
	.weak	_ZN8NArchive4NZip12CLzmaDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CLzmaDecoder7ReleaseEv,@function
_ZN8NArchive4NZip12CLzmaDecoder7ReleaseEv: # @_ZN8NArchive4NZip12CLzmaDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi216:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB41_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB41_2:
	popq	%rcx
	retq
.Lfunc_end41:
	.size	_ZN8NArchive4NZip12CLzmaDecoder7ReleaseEv, .Lfunc_end41-_ZN8NArchive4NZip12CLzmaDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip12CLzmaDecoderD2Ev,"axG",@progbits,_ZN8NArchive4NZip12CLzmaDecoderD2Ev,comdat
	.weak	_ZN8NArchive4NZip12CLzmaDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CLzmaDecoderD2Ev,@function
_ZN8NArchive4NZip12CLzmaDecoderD2Ev:    # @_ZN8NArchive4NZip12CLzmaDecoderD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTVN8NArchive4NZip12CLzmaDecoderE+16, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB42_1
# BB#2:
	movq	(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.LBB42_1:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	retq
.Lfunc_end42:
	.size	_ZN8NArchive4NZip12CLzmaDecoderD2Ev, .Lfunc_end42-_ZN8NArchive4NZip12CLzmaDecoderD2Ev
	.cfi_endproc

	.section	.text._ZN8NArchive4NZip12CLzmaDecoderD0Ev,"axG",@progbits,_ZN8NArchive4NZip12CLzmaDecoderD0Ev,comdat
	.weak	_ZN8NArchive4NZip12CLzmaDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip12CLzmaDecoderD0Ev,@function
_ZN8NArchive4NZip12CLzmaDecoderD0Ev:    # @_ZN8NArchive4NZip12CLzmaDecoderD0Ev
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%r14
.Lcfi217:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi218:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi219:
	.cfi_def_cfa_offset 32
.Lcfi220:
	.cfi_offset %rbx, -24
.Lcfi221:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive4NZip12CLzmaDecoderE+16, (%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB43_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp666:
	callq	*16(%rax)
.Ltmp667:
.LBB43_2:                               # %_ZN8NArchive4NZip12CLzmaDecoderD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB43_3:
.Ltmp668:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end43:
	.size	_ZN8NArchive4NZip12CLzmaDecoderD0Ev, .Lfunc_end43-_ZN8NArchive4NZip12CLzmaDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table43:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp666-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp667-.Ltmp666       #   Call between .Ltmp666 and .Ltmp667
	.long	.Ltmp668-.Lfunc_begin16 #     jumps to .Ltmp668
	.byte	0                       #   On action: cleanup
	.long	.Ltmp667-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Lfunc_end43-.Ltmp667   #   Call between .Ltmp667 and .Lfunc_end43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN7CBufferIhED2Ev,"axG",@progbits,_ZN7CBufferIhED2Ev,comdat
	.weak	_ZN7CBufferIhED2Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED2Ev,@function
_ZN7CBufferIhED2Ev:                     # @_ZN7CBufferIhED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV7CBufferIhE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB44_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB44_1:
	retq
.Lfunc_end44:
	.size	_ZN7CBufferIhED2Ev, .Lfunc_end44-_ZN7CBufferIhED2Ev
	.cfi_endproc

	.section	.text._ZN7CBufferIhED0Ev,"axG",@progbits,_ZN7CBufferIhED0Ev,comdat
	.weak	_ZN7CBufferIhED0Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED0Ev,@function
_ZN7CBufferIhED0Ev:                     # @_ZN7CBufferIhED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi222:
	.cfi_def_cfa_offset 16
.Lcfi223:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB45_2
# BB#1:
	callq	_ZdaPv
.LBB45_2:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end45:
	.size	_ZN7CBufferIhED0Ev, .Lfunc_end45-_ZN7CBufferIhED0Ev
	.cfi_endproc

	.section	.text._ZN7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi224:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi225:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi226:
	.cfi_def_cfa_offset 32
.Lcfi227:
	.cfi_offset %rbx, -32
.Lcfi228:
	.cfi_offset %r14, -24
.Lcfi229:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB46_3
# BB#1:
	movl	$IID_ICryptoSetPassword, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB46_2
.LBB46_3:
	leaq	328(%rbx), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB46_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB46_2:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB46_4
.Lfunc_end46:
	.size	_ZN7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end46-_ZN7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN7NCrypto10NZipStrong8CDecoder6AddRefEv,"axG",@progbits,_ZN7NCrypto10NZipStrong8CDecoder6AddRefEv,comdat
	.weak	_ZN7NCrypto10NZipStrong8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto10NZipStrong8CDecoder6AddRefEv,@function
_ZN7NCrypto10NZipStrong8CDecoder6AddRefEv: # @_ZN7NCrypto10NZipStrong8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end47:
	.size	_ZN7NCrypto10NZipStrong8CDecoder6AddRefEv, .Lfunc_end47-_ZN7NCrypto10NZipStrong8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN7NCrypto10NZipStrong8CDecoder7ReleaseEv,"axG",@progbits,_ZN7NCrypto10NZipStrong8CDecoder7ReleaseEv,comdat
	.weak	_ZN7NCrypto10NZipStrong8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto10NZipStrong8CDecoder7ReleaseEv,@function
_ZN7NCrypto10NZipStrong8CDecoder7ReleaseEv: # @_ZN7NCrypto10NZipStrong8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi230:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB48_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB48_2:
	popq	%rcx
	retq
.Lfunc_end48:
	.size	_ZN7NCrypto10NZipStrong8CDecoder7ReleaseEv, .Lfunc_end48-_ZN7NCrypto10NZipStrong8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN7NCrypto10NZipStrong10CBaseCoderD2Ev,"axG",@progbits,_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev,comdat
	.weak	_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev,@function
_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev: # @_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+112, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rdi)
	movq	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+184, 328(%rdi)
	movq	$_ZTV7CBufferIhE+16, 376(%rdi)
	movq	392(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB49_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB49_1:                               # %_ZN7CBufferIhED2Ev.exit
	retq
.Lfunc_end49:
	.size	_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev, .Lfunc_end49-_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev
	.cfi_endproc

	.section	.text._ZN7NCrypto10NZipStrong8CDecoderD0Ev,"axG",@progbits,_ZN7NCrypto10NZipStrong8CDecoderD0Ev,comdat
	.weak	_ZN7NCrypto10NZipStrong8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto10NZipStrong8CDecoderD0Ev,@function
_ZN7NCrypto10NZipStrong8CDecoderD0Ev:   # @_ZN7NCrypto10NZipStrong8CDecoderD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi231:
	.cfi_def_cfa_offset 16
.Lcfi232:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+112, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+184, 328(%rbx)
	movq	$_ZTV7CBufferIhE+16, 376(%rbx)
	movq	392(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB50_2
# BB#1:
	callq	_ZdaPv
.LBB50_2:                               # %_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end50:
	.size	_ZN7NCrypto10NZipStrong8CDecoderD0Ev, .Lfunc_end50-_ZN7NCrypto10NZipStrong8CDecoderD0Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi233:
	.cfi_def_cfa_offset 16
	movq	%rdi, %r8
	leaq	-8(%r8), %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB51_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB51_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB51_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB51_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB51_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB51_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB51_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB51_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB51_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB51_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB51_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB51_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB51_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB51_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB51_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB51_32
.LBB51_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICryptoSetPassword(%rip), %cl
	jne	.LBB51_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+1(%rip), %cl
	jne	.LBB51_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+2(%rip), %cl
	jne	.LBB51_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+3(%rip), %cl
	jne	.LBB51_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+4(%rip), %cl
	jne	.LBB51_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+5(%rip), %cl
	jne	.LBB51_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+6(%rip), %cl
	jne	.LBB51_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+7(%rip), %cl
	jne	.LBB51_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+8(%rip), %cl
	jne	.LBB51_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+9(%rip), %cl
	jne	.LBB51_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+10(%rip), %cl
	jne	.LBB51_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+11(%rip), %cl
	jne	.LBB51_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+12(%rip), %cl
	jne	.LBB51_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+13(%rip), %cl
	jne	.LBB51_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+14(%rip), %cl
	jne	.LBB51_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+15(%rip), %cl
	jne	.LBB51_33
.LBB51_32:
	leaq	320(%r8), %rax
	movq	%rax, (%rdx)
	movq	-8(%r8), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB51_33:                              # %_ZN7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end51:
	.size	_ZThn8_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end51-_ZThn8_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto10NZipStrong8CDecoder6AddRefEv,"axG",@progbits,_ZThn8_N7NCrypto10NZipStrong8CDecoder6AddRefEv,comdat
	.weak	_ZThn8_N7NCrypto10NZipStrong8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto10NZipStrong8CDecoder6AddRefEv,@function
_ZThn8_N7NCrypto10NZipStrong8CDecoder6AddRefEv: # @_ZThn8_N7NCrypto10NZipStrong8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end52:
	.size	_ZThn8_N7NCrypto10NZipStrong8CDecoder6AddRefEv, .Lfunc_end52-_ZThn8_N7NCrypto10NZipStrong8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto10NZipStrong8CDecoder7ReleaseEv,"axG",@progbits,_ZThn8_N7NCrypto10NZipStrong8CDecoder7ReleaseEv,comdat
	.weak	_ZThn8_N7NCrypto10NZipStrong8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto10NZipStrong8CDecoder7ReleaseEv,@function
_ZThn8_N7NCrypto10NZipStrong8CDecoder7ReleaseEv: # @_ZThn8_N7NCrypto10NZipStrong8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi234:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB53_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB53_2:                               # %_ZN7NCrypto10NZipStrong8CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end53:
	.size	_ZThn8_N7NCrypto10NZipStrong8CDecoder7ReleaseEv, .Lfunc_end53-_ZThn8_N7NCrypto10NZipStrong8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto10NZipStrong8CDecoderD1Ev,"axG",@progbits,_ZThn8_N7NCrypto10NZipStrong8CDecoderD1Ev,comdat
	.weak	_ZThn8_N7NCrypto10NZipStrong8CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto10NZipStrong8CDecoderD1Ev,@function
_ZThn8_N7NCrypto10NZipStrong8CDecoderD1Ev: # @_ZThn8_N7NCrypto10NZipStrong8CDecoderD1Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+112, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	movq	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+184, 320(%rdi)
	movq	$_ZTV7CBufferIhE+16, 368(%rdi)
	movq	384(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB54_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB54_1:                               # %_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev.exit
	retq
.Lfunc_end54:
	.size	_ZThn8_N7NCrypto10NZipStrong8CDecoderD1Ev, .Lfunc_end54-_ZThn8_N7NCrypto10NZipStrong8CDecoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto10NZipStrong8CDecoderD0Ev,"axG",@progbits,_ZThn8_N7NCrypto10NZipStrong8CDecoderD0Ev,comdat
	.weak	_ZThn8_N7NCrypto10NZipStrong8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto10NZipStrong8CDecoderD0Ev,@function
_ZThn8_N7NCrypto10NZipStrong8CDecoderD0Ev: # @_ZThn8_N7NCrypto10NZipStrong8CDecoderD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi235:
	.cfi_def_cfa_offset 16
.Lcfi236:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+112, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rax)
	movq	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+184, 320(%rax)
	movq	$_ZTV7CBufferIhE+16, 368(%rax)
	movq	384(%rax), %rdi
	leaq	-8(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB55_2
# BB#1:
	callq	_ZdaPv
.LBB55_2:                               # %_ZN7NCrypto10NZipStrong8CDecoderD0Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end55:
	.size	_ZThn8_N7NCrypto10NZipStrong8CDecoderD0Ev, .Lfunc_end55-_ZThn8_N7NCrypto10NZipStrong8CDecoderD0Ev
	.cfi_endproc

	.section	.text._ZThn328_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn328_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn328_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn328_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn328_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn328_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi237:
	.cfi_def_cfa_offset 16
	movq	%rdi, %r8
	leaq	-328(%r8), %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB56_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB56_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB56_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB56_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB56_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB56_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB56_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB56_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB56_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB56_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB56_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB56_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB56_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB56_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB56_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB56_32
.LBB56_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICryptoSetPassword(%rip), %cl
	jne	.LBB56_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+1(%rip), %cl
	jne	.LBB56_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+2(%rip), %cl
	jne	.LBB56_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+3(%rip), %cl
	jne	.LBB56_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+4(%rip), %cl
	jne	.LBB56_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+5(%rip), %cl
	jne	.LBB56_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+6(%rip), %cl
	jne	.LBB56_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+7(%rip), %cl
	jne	.LBB56_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+8(%rip), %cl
	jne	.LBB56_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+9(%rip), %cl
	jne	.LBB56_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+10(%rip), %cl
	jne	.LBB56_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+11(%rip), %cl
	jne	.LBB56_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+12(%rip), %cl
	jne	.LBB56_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+13(%rip), %cl
	jne	.LBB56_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+14(%rip), %cl
	jne	.LBB56_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+15(%rip), %cl
	jne	.LBB56_33
.LBB56_32:
	movq	%r8, (%rdx)
	movq	-328(%r8), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB56_33:                              # %_ZN7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end56:
	.size	_ZThn328_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end56-_ZThn328_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn328_N7NCrypto10NZipStrong8CDecoder6AddRefEv,"axG",@progbits,_ZThn328_N7NCrypto10NZipStrong8CDecoder6AddRefEv,comdat
	.weak	_ZThn328_N7NCrypto10NZipStrong8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn328_N7NCrypto10NZipStrong8CDecoder6AddRefEv,@function
_ZThn328_N7NCrypto10NZipStrong8CDecoder6AddRefEv: # @_ZThn328_N7NCrypto10NZipStrong8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	-312(%rdi), %eax
	incl	%eax
	movl	%eax, -312(%rdi)
	retq
.Lfunc_end57:
	.size	_ZThn328_N7NCrypto10NZipStrong8CDecoder6AddRefEv, .Lfunc_end57-_ZThn328_N7NCrypto10NZipStrong8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn328_N7NCrypto10NZipStrong8CDecoder7ReleaseEv,"axG",@progbits,_ZThn328_N7NCrypto10NZipStrong8CDecoder7ReleaseEv,comdat
	.weak	_ZThn328_N7NCrypto10NZipStrong8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn328_N7NCrypto10NZipStrong8CDecoder7ReleaseEv,@function
_ZThn328_N7NCrypto10NZipStrong8CDecoder7ReleaseEv: # @_ZThn328_N7NCrypto10NZipStrong8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi238:
	.cfi_def_cfa_offset 16
	movl	-312(%rdi), %eax
	decl	%eax
	movl	%eax, -312(%rdi)
	jne	.LBB58_2
# BB#1:
	addq	$-328, %rdi             # imm = 0xFEB8
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB58_2:                               # %_ZN7NCrypto10NZipStrong8CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end58:
	.size	_ZThn328_N7NCrypto10NZipStrong8CDecoder7ReleaseEv, .Lfunc_end58-_ZThn328_N7NCrypto10NZipStrong8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn328_N7NCrypto10NZipStrong8CDecoderD1Ev,"axG",@progbits,_ZThn328_N7NCrypto10NZipStrong8CDecoderD1Ev,comdat
	.weak	_ZThn328_N7NCrypto10NZipStrong8CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn328_N7NCrypto10NZipStrong8CDecoderD1Ev,@function
_ZThn328_N7NCrypto10NZipStrong8CDecoderD1Ev: # @_ZThn328_N7NCrypto10NZipStrong8CDecoderD1Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+112, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -328(%rdi)
	movq	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+184, (%rdi)
	movq	$_ZTV7CBufferIhE+16, 48(%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB59_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB59_1:                               # %_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev.exit
	retq
.Lfunc_end59:
	.size	_ZThn328_N7NCrypto10NZipStrong8CDecoderD1Ev, .Lfunc_end59-_ZThn328_N7NCrypto10NZipStrong8CDecoderD1Ev
	.cfi_endproc

	.section	.text._ZThn328_N7NCrypto10NZipStrong8CDecoderD0Ev,"axG",@progbits,_ZThn328_N7NCrypto10NZipStrong8CDecoderD0Ev,comdat
	.weak	_ZThn328_N7NCrypto10NZipStrong8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn328_N7NCrypto10NZipStrong8CDecoderD0Ev,@function
_ZThn328_N7NCrypto10NZipStrong8CDecoderD0Ev: # @_ZThn328_N7NCrypto10NZipStrong8CDecoderD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi239:
	.cfi_def_cfa_offset 16
.Lcfi240:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+112, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -328(%rax)
	movq	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+184, (%rax)
	movq	$_ZTV7CBufferIhE+16, 48(%rax)
	movq	64(%rax), %rdi
	leaq	-328(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB60_2
# BB#1:
	callq	_ZdaPv
.LBB60_2:                               # %_ZN7NCrypto10NZipStrong8CDecoderD0Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end60:
	.size	_ZThn328_N7NCrypto10NZipStrong8CDecoderD0Ev, .Lfunc_end60-_ZThn328_N7NCrypto10NZipStrong8CDecoderD0Ev
	.cfi_endproc

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB61_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB61_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB61_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB61_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB61_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB61_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB61_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB61_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB61_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB61_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB61_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB61_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB61_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB61_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB61_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB61_16:
	xorl	%eax, %eax
	retq
.Lfunc_end61:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end61-_ZeqRK4GUIDS1_
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED2Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi241:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi242:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi243:
	.cfi_def_cfa_offset 32
.Lcfi244:
	.cfi_offset %rbx, -24
.Lcfi245:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CMethodItemEE+16, (%rbx)
.Ltmp669:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp670:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB62_2:
.Ltmp671:
	movq	%rax, %r14
.Ltmp672:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp673:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB62_4:
.Ltmp674:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end62:
	.size	_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED2Ev, .Lfunc_end62-_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table62:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp669-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp670-.Ltmp669       #   Call between .Ltmp669 and .Ltmp670
	.long	.Ltmp671-.Lfunc_begin17 #     jumps to .Ltmp671
	.byte	0                       #   On action: cleanup
	.long	.Ltmp670-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Ltmp672-.Ltmp670       #   Call between .Ltmp670 and .Ltmp672
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp672-.Lfunc_begin17 # >> Call Site 3 <<
	.long	.Ltmp673-.Ltmp672       #   Call between .Ltmp672 and .Ltmp673
	.long	.Ltmp674-.Lfunc_begin17 #     jumps to .Ltmp674
	.byte	1                       #   On action: 1
	.long	.Ltmp673-.Lfunc_begin17 # >> Call Site 4 <<
	.long	.Lfunc_end62-.Ltmp673   #   Call between .Ltmp673 and .Lfunc_end62
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED0Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi246:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi247:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi248:
	.cfi_def_cfa_offset 32
.Lcfi249:
	.cfi_offset %rbx, -24
.Lcfi250:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CMethodItemEE+16, (%rbx)
.Ltmp675:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp676:
# BB#1:
.Ltmp681:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp682:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB63_5:
.Ltmp683:
	movq	%rax, %r14
	jmp	.LBB63_6
.LBB63_3:
.Ltmp677:
	movq	%rax, %r14
.Ltmp678:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp679:
.LBB63_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB63_4:
.Ltmp680:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end63:
	.size	_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED0Ev, .Lfunc_end63-_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table63:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp675-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp676-.Ltmp675       #   Call between .Ltmp675 and .Ltmp676
	.long	.Ltmp677-.Lfunc_begin18 #     jumps to .Ltmp677
	.byte	0                       #   On action: cleanup
	.long	.Ltmp681-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp682-.Ltmp681       #   Call between .Ltmp681 and .Ltmp682
	.long	.Ltmp683-.Lfunc_begin18 #     jumps to .Ltmp683
	.byte	0                       #   On action: cleanup
	.long	.Ltmp678-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Ltmp679-.Ltmp678       #   Call between .Ltmp678 and .Ltmp679
	.long	.Ltmp680-.Lfunc_begin18 #     jumps to .Ltmp680
	.byte	1                       #   On action: 1
	.long	.Ltmp679-.Lfunc_begin18 # >> Call Site 4 <<
	.long	.Lfunc_end63-.Ltmp679   #   Call between .Ltmp679 and .Lfunc_end63
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEE6DeleteEii
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi251:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi252:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi253:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi254:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi255:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi256:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi257:
	.cfi_def_cfa_offset 64
.Lcfi258:
	.cfi_offset %rbx, -56
.Lcfi259:
	.cfi_offset %r12, -48
.Lcfi260:
	.cfi_offset %r13, -40
.Lcfi261:
	.cfi_offset %r14, -32
.Lcfi262:
	.cfi_offset %r15, -24
.Lcfi263:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB64_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB64_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB64_6
# BB#3:                                 #   in Loop: Header=BB64_2 Depth=1
	movq	8(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB64_5
# BB#4:                                 #   in Loop: Header=BB64_2 Depth=1
	movq	(%rdi), %rax
.Ltmp684:
	callq	*16(%rax)
.Ltmp685:
.LBB64_5:                               # %_ZN8NArchive4NZip11CMethodItemD2Ev.exit
                                        #   in Loop: Header=BB64_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB64_6:                               #   in Loop: Header=BB64_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB64_2
.LBB64_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB64_8:
.Ltmp686:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end64:
	.size	_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEE6DeleteEii, .Lfunc_end64-_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table64:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp684-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp685-.Ltmp684       #   Call between .Ltmp684 and .Ltmp685
	.long	.Ltmp686-.Lfunc_begin19 #     jumps to .Ltmp686
	.byte	0                       #   On action: cleanup
	.long	.Ltmp685-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Lfunc_end64-.Ltmp685   #   Call between .Ltmp685 and .Lfunc_end64
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive4NZip5CItemC2ERKS1_,"axG",@progbits,_ZN8NArchive4NZip5CItemC2ERKS1_,comdat
	.weak	_ZN8NArchive4NZip5CItemC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip5CItemC2ERKS1_,@function
_ZN8NArchive4NZip5CItemC2ERKS1_:        # @_ZN8NArchive4NZip5CItemC2ERKS1_
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%r15
.Lcfi264:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi265:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi266:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi267:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi268:
	.cfi_def_cfa_offset 48
.Lcfi269:
	.cfi_offset %rbx, -48
.Lcfi270:
	.cfi_offset %r12, -40
.Lcfi271:
	.cfi_offset %r13, -32
.Lcfi272:
	.cfi_offset %r14, -24
.Lcfi273:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	%xmm1, 16(%r14)
	movups	%xmm0, (%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r14)
	movslq	40(%rbx), %rax
	leaq	1(%rax), %r15
	testl	%r15d, %r15d
	je	.LBB65_1
# BB#2:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r15, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, 32(%r14)
	movb	$0, (%rax)
	movl	%r15d, 44(%r14)
	jmp	.LBB65_3
.LBB65_1:
	xorl	%eax, %eax
.LBB65_3:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i
	leaq	32(%r14), %r12
	movq	32(%rbx), %rcx
	.p2align	4, 0x90
.LBB65_4:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB65_4
# BB#5:                                 # %_ZN11CStringBaseIcEC2ERKS0_.exit.i
	movl	40(%rbx), %eax
	movl	%eax, 40(%r14)
	leaq	48(%r14), %r15
	leaq	48(%rbx), %rsi
.Ltmp687:
	movq	%r15, %rdi
	callq	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEC2ERKS3_
.Ltmp688:
# BB#6:                                 # %_ZN8NArchive4NZip10CLocalItemC2ERKS1_.exit
	movq	112(%rbx), %rax
	movq	%rax, 112(%r14)
	movups	80(%rbx), %xmm0
	movups	96(%rbx), %xmm1
	movups	%xmm1, 96(%r14)
	movups	%xmm0, 80(%r14)
	leaq	120(%r14), %r12
	leaq	120(%rbx), %rsi
.Ltmp690:
	movq	%r12, %rdi
	callq	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEC2ERKS3_
.Ltmp691:
# BB#7:                                 # %_ZN8NArchive4NZip11CExtraBlockC2ERKS1_.exit
	movq	$_ZTV7CBufferIhE+16, 152(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 160(%r14)
	movq	160(%rbx), %r13
	testq	%r13, %r13
	je	.LBB65_10
# BB#8:                                 # %_ZN7CBufferIhE11SetCapacityEm.exit.i.i
.Ltmp693:
	movq	%r13, %rdi
	callq	_Znam
.Ltmp694:
# BB#9:                                 # %.noexc
	movq	%rax, 168(%r14)
	movq	%r13, 160(%r14)
	movq	160(%rbx), %rdx
	movq	168(%rbx), %rsi
	movq	%rax, %rdi
	callq	memmove
.LBB65_10:                              # %_ZN7CBufferIhEC2ERKS0_.exit
	movb	178(%rbx), %al
	movb	%al, 178(%r14)
	movzwl	176(%rbx), %eax
	movw	%ax, 176(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB65_15:
.Ltmp695:
	movq	%rax, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, (%r12)
.Ltmp696:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp697:
# BB#16:                                # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit.i
.Ltmp702:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp703:
	jmp	.LBB65_17
.LBB65_28:
.Ltmp704:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB65_13:
.Ltmp698:
	movq	%rax, %rbx
.Ltmp699:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp700:
	jmp	.LBB65_29
.LBB65_14:
.Ltmp701:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB65_12:
.Ltmp692:
	movq	%rax, %rbx
.LBB65_17:                              # %_ZN8NArchive4NZip11CExtraBlockD2Ev.exit
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, 48(%r14)
.Ltmp705:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp706:
# BB#18:                                # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit.i.i
.Ltmp711:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp712:
# BB#19:                                # %_ZN8NArchive4NZip11CExtraBlockD2Ev.exit.i
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	jne	.LBB65_21
	jmp	.LBB65_22
.LBB65_25:
.Ltmp713:
	movq	%rax, %rbx
	jmp	.LBB65_26
.LBB65_23:
.Ltmp707:
	movq	%rax, %rbx
.Ltmp708:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp709:
.LBB65_26:                              # %.body.i
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	jne	.LBB65_27
.LBB65_29:                              # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB65_27:
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB65_24:
.Ltmp710:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB65_11:
.Ltmp689:
	movq	%rax, %rbx
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB65_22
.LBB65_21:
	callq	_ZdaPv
.LBB65_22:                              # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end65:
	.size	_ZN8NArchive4NZip5CItemC2ERKS1_, .Lfunc_end65-_ZN8NArchive4NZip5CItemC2ERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table65:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\245\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Lfunc_begin20-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp687-.Lfunc_begin20 #   Call between .Lfunc_begin20 and .Ltmp687
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp687-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Ltmp688-.Ltmp687       #   Call between .Ltmp687 and .Ltmp688
	.long	.Ltmp689-.Lfunc_begin20 #     jumps to .Ltmp689
	.byte	0                       #   On action: cleanup
	.long	.Ltmp690-.Lfunc_begin20 # >> Call Site 3 <<
	.long	.Ltmp691-.Ltmp690       #   Call between .Ltmp690 and .Ltmp691
	.long	.Ltmp692-.Lfunc_begin20 #     jumps to .Ltmp692
	.byte	0                       #   On action: cleanup
	.long	.Ltmp693-.Lfunc_begin20 # >> Call Site 4 <<
	.long	.Ltmp694-.Ltmp693       #   Call between .Ltmp693 and .Ltmp694
	.long	.Ltmp695-.Lfunc_begin20 #     jumps to .Ltmp695
	.byte	0                       #   On action: cleanup
	.long	.Ltmp694-.Lfunc_begin20 # >> Call Site 5 <<
	.long	.Ltmp696-.Ltmp694       #   Call between .Ltmp694 and .Ltmp696
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp696-.Lfunc_begin20 # >> Call Site 6 <<
	.long	.Ltmp697-.Ltmp696       #   Call between .Ltmp696 and .Ltmp697
	.long	.Ltmp698-.Lfunc_begin20 #     jumps to .Ltmp698
	.byte	1                       #   On action: 1
	.long	.Ltmp702-.Lfunc_begin20 # >> Call Site 7 <<
	.long	.Ltmp703-.Ltmp702       #   Call between .Ltmp702 and .Ltmp703
	.long	.Ltmp704-.Lfunc_begin20 #     jumps to .Ltmp704
	.byte	1                       #   On action: 1
	.long	.Ltmp699-.Lfunc_begin20 # >> Call Site 8 <<
	.long	.Ltmp700-.Ltmp699       #   Call between .Ltmp699 and .Ltmp700
	.long	.Ltmp701-.Lfunc_begin20 #     jumps to .Ltmp701
	.byte	1                       #   On action: 1
	.long	.Ltmp705-.Lfunc_begin20 # >> Call Site 9 <<
	.long	.Ltmp706-.Ltmp705       #   Call between .Ltmp705 and .Ltmp706
	.long	.Ltmp707-.Lfunc_begin20 #     jumps to .Ltmp707
	.byte	1                       #   On action: 1
	.long	.Ltmp711-.Lfunc_begin20 # >> Call Site 10 <<
	.long	.Ltmp712-.Ltmp711       #   Call between .Ltmp711 and .Ltmp712
	.long	.Ltmp713-.Lfunc_begin20 #     jumps to .Ltmp713
	.byte	1                       #   On action: 1
	.long	.Ltmp708-.Lfunc_begin20 # >> Call Site 11 <<
	.long	.Ltmp709-.Ltmp708       #   Call between .Ltmp708 and .Ltmp709
	.long	.Ltmp710-.Lfunc_begin20 #     jumps to .Ltmp710
	.byte	1                       #   On action: 1
	.long	.Ltmp709-.Lfunc_begin20 # >> Call Site 12 <<
	.long	.Lfunc_end65-.Ltmp709   #   Call between .Ltmp709 and .Lfunc_end65
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI66_0:
	.zero	16
	.section	.text._ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEC2ERKS3_,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEC2ERKS3_,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEC2ERKS3_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEC2ERKS3_,@function
_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEC2ERKS3_: # @_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEC2ERKS3_
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%rbp
.Lcfi274:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi275:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi276:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi277:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi278:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi279:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi280:
	.cfi_def_cfa_offset 80
.Lcfi281:
	.cfi_offset %rbx, -56
.Lcfi282:
	.cfi_offset %r12, -48
.Lcfi283:
	.cfi_offset %r13, -40
.Lcfi284:
	.cfi_offset %r14, -32
.Lcfi285:
	.cfi_offset %r15, -24
.Lcfi286:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbp)
	movq	$8, 24(%rbp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, (%rbp)
.Ltmp714:
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp715:
# BB#1:                                 # %.noexc
	movl	12(%rbx), %r15d
	movl	12(%rbp), %esi
	addl	%r15d, %esi
.Ltmp716:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp717:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB66_10
# BB#3:                                 # %.lr.ph.i.i
	xorl	%r13d, %r13d
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB66_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rax
	movq	(%rax,%r13,8), %r14
.Ltmp719:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp720:
# BB#5:                                 # %.noexc5
                                        #   in Loop: Header=BB66_4 Depth=1
	movzwl	(%r14), %eax
	movw	%ax, (%rbx)
	movq	$_ZTV7CBufferIhE+16, 8(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movq	16(%r14), %r12
	testq	%r12, %r12
	je	.LBB66_8
# BB#6:                                 # %_ZN7CBufferIhE11SetCapacityEm.exit.i.i.i.i
                                        #   in Loop: Header=BB66_4 Depth=1
.Ltmp721:
	movq	%r12, %rdi
	callq	_Znam
.Ltmp722:
# BB#7:                                 # %.noexc.i
                                        #   in Loop: Header=BB66_4 Depth=1
	movq	%rax, 24(%rbx)
	movq	%r12, 16(%rbx)
	movq	24(%r14), %rsi
	movq	%rax, %rdi
	movq	%r12, %rdx
	callq	memmove
.LBB66_8:                               # %_ZN8NArchive4NZip14CExtraSubBlockC2ERKS1_.exit.i
                                        #   in Loop: Header=BB66_4 Depth=1
.Ltmp724:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp725:
# BB#9:                                 # %.noexc4
                                        #   in Loop: Header=BB66_4 Depth=1
	movq	16(%rbp), %rax
	movslq	12(%rbp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rbp)
	incq	%r13
	cmpq	%r13, %r15
	movq	16(%rsp), %rbx          # 8-byte Reload
	jne	.LBB66_4
.LBB66_10:                              # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEaSERKS3_.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB66_12:                              # %.loopexit.split-lp
.Ltmp718:
	jmp	.LBB66_13
.LBB66_17:
.Ltmp723:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB66_14
.LBB66_11:                              # %.loopexit
.Ltmp726:
.LBB66_13:                              # %.body
	movq	%rax, %r14
.LBB66_14:                              # %.body
.Ltmp727:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp728:
# BB#15:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB66_16:
.Ltmp729:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end66:
	.size	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEC2ERKS3_, .Lfunc_end66-_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEEC2ERKS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table66:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp714-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp717-.Ltmp714       #   Call between .Ltmp714 and .Ltmp717
	.long	.Ltmp718-.Lfunc_begin21 #     jumps to .Ltmp718
	.byte	0                       #   On action: cleanup
	.long	.Ltmp719-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Ltmp720-.Ltmp719       #   Call between .Ltmp719 and .Ltmp720
	.long	.Ltmp726-.Lfunc_begin21 #     jumps to .Ltmp726
	.byte	0                       #   On action: cleanup
	.long	.Ltmp721-.Lfunc_begin21 # >> Call Site 3 <<
	.long	.Ltmp722-.Ltmp721       #   Call between .Ltmp721 and .Ltmp722
	.long	.Ltmp723-.Lfunc_begin21 #     jumps to .Ltmp723
	.byte	0                       #   On action: cleanup
	.long	.Ltmp722-.Lfunc_begin21 # >> Call Site 4 <<
	.long	.Ltmp724-.Ltmp722       #   Call between .Ltmp722 and .Ltmp724
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp724-.Lfunc_begin21 # >> Call Site 5 <<
	.long	.Ltmp725-.Ltmp724       #   Call between .Ltmp724 and .Ltmp725
	.long	.Ltmp726-.Lfunc_begin21 #     jumps to .Ltmp726
	.byte	0                       #   On action: cleanup
	.long	.Ltmp727-.Lfunc_begin21 # >> Call Site 6 <<
	.long	.Ltmp728-.Ltmp727       #   Call between .Ltmp727 and .Ltmp728
	.long	.Ltmp729-.Lfunc_begin21 #     jumps to .Ltmp729
	.byte	1                       #   On action: 1
	.long	.Ltmp728-.Lfunc_begin21 # >> Call Site 7 <<
	.long	.Lfunc_end66-.Ltmp728   #   Call between .Ltmp728 and .Lfunc_end66
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:
	pushq	%r14
.Lcfi287:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi288:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi289:
	.cfi_def_cfa_offset 32
.Lcfi290:
	.cfi_offset %rbx, -24
.Lcfi291:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, (%rbx)
.Ltmp730:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp731:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB67_2:
.Ltmp732:
	movq	%rax, %r14
.Ltmp733:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp734:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB67_4:
.Ltmp735:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end67:
	.size	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev, .Lfunc_end67-_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table67:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp730-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp731-.Ltmp730       #   Call between .Ltmp730 and .Ltmp731
	.long	.Ltmp732-.Lfunc_begin22 #     jumps to .Ltmp732
	.byte	0                       #   On action: cleanup
	.long	.Ltmp731-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Ltmp733-.Ltmp731       #   Call between .Ltmp731 and .Ltmp733
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp733-.Lfunc_begin22 # >> Call Site 3 <<
	.long	.Ltmp734-.Ltmp733       #   Call between .Ltmp733 and .Ltmp734
	.long	.Ltmp735-.Lfunc_begin22 #     jumps to .Ltmp735
	.byte	1                       #   On action: 1
	.long	.Ltmp734-.Lfunc_begin22 # >> Call Site 4 <<
	.long	.Lfunc_end67-.Ltmp734   #   Call between .Ltmp734 and .Lfunc_end67
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev
.Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception23
# BB#0:
	pushq	%r14
.Lcfi292:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi293:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi294:
	.cfi_def_cfa_offset 32
.Lcfi295:
	.cfi_offset %rbx, -24
.Lcfi296:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE+16, (%rbx)
.Ltmp736:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp737:
# BB#1:
.Ltmp742:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp743:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB68_5:
.Ltmp744:
	movq	%rax, %r14
	jmp	.LBB68_6
.LBB68_3:
.Ltmp738:
	movq	%rax, %r14
.Ltmp739:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp740:
.LBB68_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB68_4:
.Ltmp741:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end68:
	.size	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev, .Lfunc_end68-_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table68:
.Lexception23:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp736-.Lfunc_begin23 # >> Call Site 1 <<
	.long	.Ltmp737-.Ltmp736       #   Call between .Ltmp736 and .Ltmp737
	.long	.Ltmp738-.Lfunc_begin23 #     jumps to .Ltmp738
	.byte	0                       #   On action: cleanup
	.long	.Ltmp742-.Lfunc_begin23 # >> Call Site 2 <<
	.long	.Ltmp743-.Ltmp742       #   Call between .Ltmp742 and .Ltmp743
	.long	.Ltmp744-.Lfunc_begin23 #     jumps to .Ltmp744
	.byte	0                       #   On action: cleanup
	.long	.Ltmp739-.Lfunc_begin23 # >> Call Site 3 <<
	.long	.Ltmp740-.Ltmp739       #   Call between .Ltmp739 and .Ltmp740
	.long	.Ltmp741-.Lfunc_begin23 #     jumps to .Ltmp741
	.byte	1                       #   On action: 1
	.long	.Ltmp740-.Lfunc_begin23 # >> Call Site 4 <<
	.long	.Lfunc_end68-.Ltmp740   #   Call between .Ltmp740 and .Lfunc_end68
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi297:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi298:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi299:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi300:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi301:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi302:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi303:
	.cfi_def_cfa_offset 64
.Lcfi304:
	.cfi_offset %rbx, -56
.Lcfi305:
	.cfi_offset %r12, -48
.Lcfi306:
	.cfi_offset %r13, -40
.Lcfi307:
	.cfi_offset %r14, -32
.Lcfi308:
	.cfi_offset %r15, -24
.Lcfi309:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB69_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB69_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB69_6
# BB#3:                                 #   in Loop: Header=BB69_2 Depth=1
	movq	$_ZTV7CBufferIhE+16, 8(%rbp)
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB69_5
# BB#4:                                 #   in Loop: Header=BB69_2 Depth=1
	callq	_ZdaPv
.LBB69_5:                               # %_ZN8NArchive4NZip14CExtraSubBlockD2Ev.exit
                                        #   in Loop: Header=BB69_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB69_6:                               #   in Loop: Header=BB69_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB69_2
.LBB69_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end69:
	.size	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii, .Lfunc_end69-_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip7CItemExEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED0Ev
.Lfunc_begin24:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception24
# BB#0:
	pushq	%r14
.Lcfi310:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi311:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi312:
	.cfi_def_cfa_offset 32
.Lcfi313:
	.cfi_offset %rbx, -24
.Lcfi314:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip7CItemExEE+16, (%rbx)
.Ltmp745:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp746:
# BB#1:
.Ltmp751:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp752:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB70_5:
.Ltmp753:
	movq	%rax, %r14
	jmp	.LBB70_6
.LBB70_3:
.Ltmp747:
	movq	%rax, %r14
.Ltmp748:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp749:
.LBB70_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB70_4:
.Ltmp750:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end70:
	.size	_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED0Ev, .Lfunc_end70-_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table70:
.Lexception24:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp745-.Lfunc_begin24 # >> Call Site 1 <<
	.long	.Ltmp746-.Ltmp745       #   Call between .Ltmp745 and .Ltmp746
	.long	.Ltmp747-.Lfunc_begin24 #     jumps to .Ltmp747
	.byte	0                       #   On action: cleanup
	.long	.Ltmp751-.Lfunc_begin24 # >> Call Site 2 <<
	.long	.Ltmp752-.Ltmp751       #   Call between .Ltmp751 and .Ltmp752
	.long	.Ltmp753-.Lfunc_begin24 #     jumps to .Ltmp753
	.byte	0                       #   On action: cleanup
	.long	.Ltmp748-.Lfunc_begin24 # >> Call Site 3 <<
	.long	.Ltmp749-.Ltmp748       #   Call between .Ltmp748 and .Ltmp749
	.long	.Ltmp750-.Lfunc_begin24 #     jumps to .Ltmp750
	.byte	1                       #   On action: 1
	.long	.Ltmp749-.Lfunc_begin24 # >> Call Site 4 <<
	.long	.Lfunc_end70-.Ltmp749   #   Call between .Ltmp749 and .Lfunc_end70
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip7CItemExEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip7CItemExEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip7CItemExEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip7CItemExEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NZip7CItemExEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NZip7CItemExEE6DeleteEii
.Lfunc_begin25:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception25
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi315:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi316:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi317:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi318:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi319:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi320:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi321:
	.cfi_def_cfa_offset 64
.Lcfi322:
	.cfi_offset %rbx, -56
.Lcfi323:
	.cfi_offset %r12, -48
.Lcfi324:
	.cfi_offset %r13, -40
.Lcfi325:
	.cfi_offset %r14, -32
.Lcfi326:
	.cfi_offset %r15, -24
.Lcfi327:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB71_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB71_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB71_5
# BB#3:                                 #   in Loop: Header=BB71_2 Depth=1
.Ltmp754:
	movq	%rbp, %rdi
	callq	_ZN8NArchive4NZip5CItemD2Ev
.Ltmp755:
# BB#4:                                 #   in Loop: Header=BB71_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB71_5:                               #   in Loop: Header=BB71_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB71_2
.LBB71_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB71_7:
.Ltmp756:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end71:
	.size	_ZN13CObjectVectorIN8NArchive4NZip7CItemExEE6DeleteEii, .Lfunc_end71-_ZN13CObjectVectorIN8NArchive4NZip7CItemExEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table71:
.Lexception25:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp754-.Lfunc_begin25 # >> Call Site 1 <<
	.long	.Ltmp755-.Ltmp754       #   Call between .Ltmp754 and .Ltmp755
	.long	.Ltmp756-.Lfunc_begin25 #     jumps to .Ltmp756
	.byte	0                       #   On action: cleanup
	.long	.Ltmp755-.Lfunc_begin25 # >> Call Site 2 <<
	.long	.Lfunc_end71-.Ltmp755   #   Call between .Ltmp755 and .Lfunc_end71
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTVN8NArchive4NZip8CHandlerE,@object # @_ZTVN8NArchive4NZip8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NArchive4NZip8CHandlerE
	.p2align	3
_ZTVN8NArchive4NZip8CHandlerE:
	.quad	0
	.quad	_ZTIN8NArchive4NZip8CHandlerE
	.quad	_ZN8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive4NZip8CHandler6AddRefEv
	.quad	_ZN8NArchive4NZip8CHandler7ReleaseEv
	.quad	_ZN8NArchive4NZip8CHandlerD2Ev
	.quad	_ZN8NArchive4NZip8CHandlerD0Ev
	.quad	_ZN8NArchive4NZip8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.quad	_ZN8NArchive4NZip8CHandler5CloseEv
	.quad	_ZN8NArchive4NZip8CHandler16GetNumberOfItemsEPj
	.quad	_ZN8NArchive4NZip8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.quad	_ZN8NArchive4NZip8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.quad	_ZN8NArchive4NZip8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.quad	_ZN8NArchive4NZip8CHandler21GetNumberOfPropertiesEPj
	.quad	_ZN8NArchive4NZip8CHandler15GetPropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive4NZip8CHandler28GetNumberOfArchivePropertiesEPj
	.quad	_ZN8NArchive4NZip8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive4NZip8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.quad	_ZN8NArchive4NZip8CHandler15GetFileTimeTypeEPj
	.quad	_ZN8NArchive4NZip8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.quad	-8
	.quad	_ZTIN8NArchive4NZip8CHandlerE
	.quad	_ZThn8_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N8NArchive4NZip8CHandler6AddRefEv
	.quad	_ZThn8_N8NArchive4NZip8CHandler7ReleaseEv
	.quad	_ZThn8_N8NArchive4NZip8CHandlerD1Ev
	.quad	_ZThn8_N8NArchive4NZip8CHandlerD0Ev
	.quad	_ZThn8_N8NArchive4NZip8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.quad	_ZThn8_N8NArchive4NZip8CHandler15GetFileTimeTypeEPj
	.quad	-16
	.quad	_ZTIN8NArchive4NZip8CHandlerE
	.quad	_ZThn16_N8NArchive4NZip8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N8NArchive4NZip8CHandler6AddRefEv
	.quad	_ZThn16_N8NArchive4NZip8CHandler7ReleaseEv
	.quad	_ZThn16_N8NArchive4NZip8CHandlerD1Ev
	.quad	_ZThn16_N8NArchive4NZip8CHandlerD0Ev
	.quad	_ZThn16_N8NArchive4NZip8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.size	_ZTVN8NArchive4NZip8CHandlerE, 296

	.type	_ZN8NArchive4NZipL6kPropsE,@object # @_ZN8NArchive4NZipL6kPropsE
	.p2align	4
_ZN8NArchive4NZipL6kPropsE:
	.quad	0
	.long	3                       # 0x3
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	6                       # 0x6
	.short	11                      # 0xb
	.zero	2
	.quad	0
	.long	7                       # 0x7
	.short	21                      # 0x15
	.zero	2
	.quad	0
	.long	8                       # 0x8
	.short	21                      # 0x15
	.zero	2
	.quad	0
	.long	12                      # 0xc
	.short	64                      # 0x40
	.zero	2
	.quad	0
	.long	10                      # 0xa
	.short	64                      # 0x40
	.zero	2
	.quad	0
	.long	11                      # 0xb
	.short	64                      # 0x40
	.zero	2
	.quad	0
	.long	9                       # 0x9
	.short	19                      # 0x13
	.zero	2
	.quad	0
	.long	15                      # 0xf
	.short	11                      # 0xb
	.zero	2
	.quad	0
	.long	28                      # 0x1c
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	19                      # 0x13
	.short	19                      # 0x13
	.zero	2
	.quad	0
	.long	22                      # 0x16
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	23                      # 0x17
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	33                      # 0x21
	.short	19                      # 0x13
	.zero	2
	.size	_ZN8NArchive4NZipL6kPropsE, 224

	.type	_ZN8NArchive4NZipL9kArcPropsE,@object # @_ZN8NArchive4NZipL9kArcPropsE
	.p2align	4
_ZN8NArchive4NZipL9kArcPropsE:
	.quad	0
	.long	41                      # 0x29
	.short	11                      # 0xb
	.zero	2
	.quad	0
	.long	28                      # 0x1c
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	44                      # 0x2c
	.short	21                      # 0x15
	.zero	2
	.quad	0
	.long	36                      # 0x24
	.short	21                      # 0x15
	.zero	2
	.size	_ZN8NArchive4NZipL9kArcPropsE, 64

	.type	_ZN8NArchive4NZipL19g_StrongCryptoPairsE,@object # @_ZN8NArchive4NZipL19g_StrongCryptoPairsE
	.p2align	4
_ZN8NArchive4NZipL19g_StrongCryptoPairsE:
	.short	26113                   # 0x6601
	.zero	6
	.quad	.L.str.2
	.short	26114                   # 0x6602
	.zero	6
	.quad	.L.str.3
	.short	26115                   # 0x6603
	.zero	6
	.quad	.L.str.4
	.short	26121                   # 0x6609
	.zero	6
	.quad	.L.str.5
	.short	26126                   # 0x660e
	.zero	6
	.quad	.L.str.6
	.short	26127                   # 0x660f
	.zero	6
	.quad	.L.str.7
	.short	26128                   # 0x6610
	.zero	6
	.quad	.L.str.8
	.short	26370                   # 0x6702
	.zero	6
	.quad	.L.str.9
	.short	26400                   # 0x6720
	.zero	6
	.quad	.L.str.10
	.short	26401                   # 0x6721
	.zero	6
	.quad	.L.str.11
	.short	26625                   # 0x6801
	.zero	6
	.quad	.L.str.12
	.size	_ZN8NArchive4NZipL19g_StrongCryptoPairsE, 176

	.type	_ZN8NArchive4NZipL8kMethodsE,@object # @_ZN8NArchive4NZipL8kMethodsE
	.p2align	4
_ZN8NArchive4NZipL8kMethodsE:
	.quad	.L.str.15
	.quad	.L.str.16
	.quad	.L.str.17
	.quad	.L.str.18
	.quad	.L.str.19
	.quad	.L.str.20
	.quad	.L.str.21
	.quad	.L.str.22
	.quad	.L.str.23
	.quad	.L.str.24
	.quad	.L.str.25
	.size	_ZN8NArchive4NZipL8kMethodsE, 88

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	":EOS"
	.size	.L.str, 5

	.type	_ZN8NArchive4NZipL7kHostOSE,@object # @_ZN8NArchive4NZipL7kHostOSE
	.section	.rodata,"a",@progbits
	.p2align	4
_ZN8NArchive4NZipL7kHostOSE:
	.quad	.L.str.31
	.quad	.L.str.32
	.quad	.L.str.33
	.quad	.L.str.34
	.quad	.L.str.35
	.quad	.L.str.36
	.quad	.L.str.37
	.quad	.L.str.38
	.quad	.L.str.39
	.quad	.L.str.40
	.quad	.L.str.41
	.quad	.L.str.42
	.quad	.L.str.43
	.quad	.L.str.44
	.quad	.L.str.45
	.quad	.L.str.46
	.quad	.L.str.47
	.quad	.L.str.48
	.quad	.L.str.49
	.quad	.L.str.50
	.size	_ZN8NArchive4NZipL7kHostOSE, 160

	.type	_ZN8NArchive4NZipL10kUnknownOSE,@object # @_ZN8NArchive4NZipL10kUnknownOSE
	.p2align	3
_ZN8NArchive4NZipL10kUnknownOSE:
	.quad	.L.str.51
	.size	_ZN8NArchive4NZipL10kUnknownOSE, 8

	.type	_ZTSN8NArchive4NZip19CInArchiveExceptionE,@object # @_ZTSN8NArchive4NZip19CInArchiveExceptionE
	.section	.rodata._ZTSN8NArchive4NZip19CInArchiveExceptionE,"aG",@progbits,_ZTSN8NArchive4NZip19CInArchiveExceptionE,comdat
	.weak	_ZTSN8NArchive4NZip19CInArchiveExceptionE
	.p2align	4
_ZTSN8NArchive4NZip19CInArchiveExceptionE:
	.asciz	"N8NArchive4NZip19CInArchiveExceptionE"
	.size	_ZTSN8NArchive4NZip19CInArchiveExceptionE, 38

	.type	_ZTIN8NArchive4NZip19CInArchiveExceptionE,@object # @_ZTIN8NArchive4NZip19CInArchiveExceptionE
	.section	.rodata._ZTIN8NArchive4NZip19CInArchiveExceptionE,"aG",@progbits,_ZTIN8NArchive4NZip19CInArchiveExceptionE,comdat
	.weak	_ZTIN8NArchive4NZip19CInArchiveExceptionE
	.p2align	3
_ZTIN8NArchive4NZip19CInArchiveExceptionE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN8NArchive4NZip19CInArchiveExceptionE
	.size	_ZTIN8NArchive4NZip19CInArchiveExceptionE, 16

	.type	_ZTVN8NArchive4NZip12CLzmaDecoderE,@object # @_ZTVN8NArchive4NZip12CLzmaDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NArchive4NZip12CLzmaDecoderE
	.p2align	3
_ZTVN8NArchive4NZip12CLzmaDecoderE:
	.quad	0
	.quad	_ZTIN8NArchive4NZip12CLzmaDecoderE
	.quad	_ZN8NArchive4NZip12CLzmaDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive4NZip12CLzmaDecoder6AddRefEv
	.quad	_ZN8NArchive4NZip12CLzmaDecoder7ReleaseEv
	.quad	_ZN8NArchive4NZip12CLzmaDecoderD2Ev
	.quad	_ZN8NArchive4NZip12CLzmaDecoderD0Ev
	.quad	_ZN8NArchive4NZip12CLzmaDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.size	_ZTVN8NArchive4NZip12CLzmaDecoderE, 64

	.type	_ZTSN8NArchive4NZip8CHandlerE,@object # @_ZTSN8NArchive4NZip8CHandlerE
	.globl	_ZTSN8NArchive4NZip8CHandlerE
	.p2align	4
_ZTSN8NArchive4NZip8CHandlerE:
	.asciz	"N8NArchive4NZip8CHandlerE"
	.size	_ZTSN8NArchive4NZip8CHandlerE, 26

	.type	_ZTS10IInArchive,@object # @_ZTS10IInArchive
	.section	.rodata._ZTS10IInArchive,"aG",@progbits,_ZTS10IInArchive,comdat
	.weak	_ZTS10IInArchive
_ZTS10IInArchive:
	.asciz	"10IInArchive"
	.size	_ZTS10IInArchive, 13

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI10IInArchive,@object # @_ZTI10IInArchive
	.section	.rodata._ZTI10IInArchive,"aG",@progbits,_ZTI10IInArchive,comdat
	.weak	_ZTI10IInArchive
	.p2align	4
_ZTI10IInArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IInArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI10IInArchive, 24

	.type	_ZTS11IOutArchive,@object # @_ZTS11IOutArchive
	.section	.rodata._ZTS11IOutArchive,"aG",@progbits,_ZTS11IOutArchive,comdat
	.weak	_ZTS11IOutArchive
_ZTS11IOutArchive:
	.asciz	"11IOutArchive"
	.size	_ZTS11IOutArchive, 14

	.type	_ZTI11IOutArchive,@object # @_ZTI11IOutArchive
	.section	.rodata._ZTI11IOutArchive,"aG",@progbits,_ZTI11IOutArchive,comdat
	.weak	_ZTI11IOutArchive
	.p2align	4
_ZTI11IOutArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS11IOutArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI11IOutArchive, 24

	.type	_ZTS14ISetProperties,@object # @_ZTS14ISetProperties
	.section	.rodata._ZTS14ISetProperties,"aG",@progbits,_ZTS14ISetProperties,comdat
	.weak	_ZTS14ISetProperties
	.p2align	4
_ZTS14ISetProperties:
	.asciz	"14ISetProperties"
	.size	_ZTS14ISetProperties, 17

	.type	_ZTI14ISetProperties,@object # @_ZTI14ISetProperties
	.section	.rodata._ZTI14ISetProperties,"aG",@progbits,_ZTI14ISetProperties,comdat
	.weak	_ZTI14ISetProperties
	.p2align	4
_ZTI14ISetProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ISetProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI14ISetProperties, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive4NZip8CHandlerE,@object # @_ZTIN8NArchive4NZip8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive4NZip8CHandlerE
	.p2align	4
_ZTIN8NArchive4NZip8CHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive4NZip8CHandlerE
	.long	1                       # 0x1
	.long	4                       # 0x4
	.quad	_ZTI10IInArchive
	.quad	2                       # 0x2
	.quad	_ZTI11IOutArchive
	.quad	2050                    # 0x802
	.quad	_ZTI14ISetProperties
	.quad	4098                    # 0x1002
	.quad	_ZTI13CMyUnknownImp
	.quad	6146                    # 0x1802
	.size	_ZTIN8NArchive4NZip8CHandlerE, 88

	.type	_ZTVN8NArchive4NZip12CProgressImpE,@object # @_ZTVN8NArchive4NZip12CProgressImpE
	.globl	_ZTVN8NArchive4NZip12CProgressImpE
	.p2align	3
_ZTVN8NArchive4NZip12CProgressImpE:
	.quad	0
	.quad	_ZTIN8NArchive4NZip12CProgressImpE
	.quad	_ZN8NArchive4NZip12CProgressImp8SetTotalEy
	.quad	_ZN8NArchive4NZip12CProgressImp12SetCompletedEy
	.size	_ZTVN8NArchive4NZip12CProgressImpE, 32

	.type	_ZTSN8NArchive4NZip12CProgressImpE,@object # @_ZTSN8NArchive4NZip12CProgressImpE
	.globl	_ZTSN8NArchive4NZip12CProgressImpE
	.p2align	4
_ZTSN8NArchive4NZip12CProgressImpE:
	.asciz	"N8NArchive4NZip12CProgressImpE"
	.size	_ZTSN8NArchive4NZip12CProgressImpE, 31

	.type	_ZTSN8NArchive4NZip13CProgressVirtE,@object # @_ZTSN8NArchive4NZip13CProgressVirtE
	.section	.rodata._ZTSN8NArchive4NZip13CProgressVirtE,"aG",@progbits,_ZTSN8NArchive4NZip13CProgressVirtE,comdat
	.weak	_ZTSN8NArchive4NZip13CProgressVirtE
	.p2align	4
_ZTSN8NArchive4NZip13CProgressVirtE:
	.asciz	"N8NArchive4NZip13CProgressVirtE"
	.size	_ZTSN8NArchive4NZip13CProgressVirtE, 32

	.type	_ZTIN8NArchive4NZip13CProgressVirtE,@object # @_ZTIN8NArchive4NZip13CProgressVirtE
	.section	.rodata._ZTIN8NArchive4NZip13CProgressVirtE,"aG",@progbits,_ZTIN8NArchive4NZip13CProgressVirtE,comdat
	.weak	_ZTIN8NArchive4NZip13CProgressVirtE
	.p2align	3
_ZTIN8NArchive4NZip13CProgressVirtE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN8NArchive4NZip13CProgressVirtE
	.size	_ZTIN8NArchive4NZip13CProgressVirtE, 16

	.type	_ZTIN8NArchive4NZip12CProgressImpE,@object # @_ZTIN8NArchive4NZip12CProgressImpE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive4NZip12CProgressImpE
	.p2align	4
_ZTIN8NArchive4NZip12CProgressImpE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NArchive4NZip12CProgressImpE
	.quad	_ZTIN8NArchive4NZip13CProgressVirtE
	.size	_ZTIN8NArchive4NZip12CProgressImpE, 24

	.type	_ZTSN8NArchive4NZip12CLzmaDecoderE,@object # @_ZTSN8NArchive4NZip12CLzmaDecoderE
	.globl	_ZTSN8NArchive4NZip12CLzmaDecoderE
	.p2align	4
_ZTSN8NArchive4NZip12CLzmaDecoderE:
	.asciz	"N8NArchive4NZip12CLzmaDecoderE"
	.size	_ZTSN8NArchive4NZip12CLzmaDecoderE, 31

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTIN8NArchive4NZip12CLzmaDecoderE,@object # @_ZTIN8NArchive4NZip12CLzmaDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive4NZip12CLzmaDecoderE
	.p2align	4
_ZTIN8NArchive4NZip12CLzmaDecoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive4NZip12CLzmaDecoderE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN8NArchive4NZip12CLzmaDecoderE, 56

	.type	_ZTV7CBufferIhE,@object # @_ZTV7CBufferIhE
	.section	.rodata._ZTV7CBufferIhE,"aG",@progbits,_ZTV7CBufferIhE,comdat
	.weak	_ZTV7CBufferIhE
	.p2align	3
_ZTV7CBufferIhE:
	.quad	0
	.quad	_ZTI7CBufferIhE
	.quad	_ZN7CBufferIhED2Ev
	.quad	_ZN7CBufferIhED0Ev
	.size	_ZTV7CBufferIhE, 32

	.type	_ZTS7CBufferIhE,@object # @_ZTS7CBufferIhE
	.section	.rodata._ZTS7CBufferIhE,"aG",@progbits,_ZTS7CBufferIhE,comdat
	.weak	_ZTS7CBufferIhE
_ZTS7CBufferIhE:
	.asciz	"7CBufferIhE"
	.size	_ZTS7CBufferIhE, 12

	.type	_ZTI7CBufferIhE,@object # @_ZTI7CBufferIhE
	.section	.rodata._ZTI7CBufferIhE,"aG",@progbits,_ZTI7CBufferIhE,comdat
	.weak	_ZTI7CBufferIhE
	.p2align	3
_ZTI7CBufferIhE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS7CBufferIhE
	.size	_ZTI7CBufferIhE, 16

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"DES"
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"RC2a"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"3DES-168"
	.size	.L.str.4, 9

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"3DES-112"
	.size	.L.str.5, 9

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"pkAES-128"
	.size	.L.str.6, 10

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"pkAES-192"
	.size	.L.str.7, 10

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"pkAES-256"
	.size	.L.str.8, 10

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"RC2"
	.size	.L.str.9, 4

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Blowfish"
	.size	.L.str.10, 9

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Twofish"
	.size	.L.str.11, 8

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"RC4"
	.size	.L.str.12, 4

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"StrongCrypto"
	.size	.L.str.13, 13

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"ZipCrypto"
	.size	.L.str.14, 10

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Store"
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Shrink"
	.size	.L.str.16, 7

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Reduced1"
	.size	.L.str.17, 9

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Reduced2"
	.size	.L.str.18, 9

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Reduced3"
	.size	.L.str.19, 9

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Reduced4"
	.size	.L.str.20, 9

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Implode"
	.size	.L.str.21, 8

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Tokenizing"
	.size	.L.str.22, 11

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"Deflate"
	.size	.L.str.23, 8

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Deflate64"
	.size	.L.str.24, 10

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"PKImploding"
	.size	.L.str.25, 12

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"LZMA"
	.size	.L.str.26, 5

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"BZip2"
	.size	.L.str.27, 6

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Jpeg"
	.size	.L.str.28, 5

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"WavPack"
	.size	.L.str.29, 8

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"PPMd"
	.size	.L.str.30, 5

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"FAT"
	.size	.L.str.31, 4

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"AMIGA"
	.size	.L.str.32, 6

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"VMS"
	.size	.L.str.33, 4

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"Unix"
	.size	.L.str.34, 5

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"VM/CMS"
	.size	.L.str.35, 7

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"Atari"
	.size	.L.str.36, 6

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"HPFS"
	.size	.L.str.37, 5

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"Macintosh"
	.size	.L.str.38, 10

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"Z-System"
	.size	.L.str.39, 9

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"CP/M"
	.size	.L.str.40, 5

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"TOPS-20"
	.size	.L.str.41, 8

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"NTFS"
	.size	.L.str.42, 5

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"SMS/QDOS"
	.size	.L.str.43, 9

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"Acorn"
	.size	.L.str.44, 6

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"VFAT"
	.size	.L.str.45, 5

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"MVS"
	.size	.L.str.46, 4

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"BeOS"
	.size	.L.str.47, 5

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"Tandem"
	.size	.L.str.48, 7

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"OS/400"
	.size	.L.str.49, 7

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"OS/X"
	.size	.L.str.50, 5

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"Unknown"
	.size	.L.str.51, 8

	.type	_ZTVN7NCrypto10NZipStrong8CDecoderE,@object # @_ZTVN7NCrypto10NZipStrong8CDecoderE
	.section	.rodata._ZTVN7NCrypto10NZipStrong8CDecoderE,"aG",@progbits,_ZTVN7NCrypto10NZipStrong8CDecoderE,comdat
	.weak	_ZTVN7NCrypto10NZipStrong8CDecoderE
	.p2align	3
_ZTVN7NCrypto10NZipStrong8CDecoderE:
	.quad	0
	.quad	_ZTIN7NCrypto10NZipStrong8CDecoderE
	.quad	_ZN7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN7NCrypto10NZipStrong8CDecoder6AddRefEv
	.quad	_ZN7NCrypto10NZipStrong8CDecoder7ReleaseEv
	.quad	_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev
	.quad	_ZN7NCrypto10NZipStrong8CDecoderD0Ev
	.quad	_ZN7NCrypto12CAesCbcCoder4InitEv
	.quad	_ZN7NCrypto12CAesCbcCoder6FilterEPhj
	.quad	_ZN7NCrypto12CAesCbcCoder6SetKeyEPKhj
	.quad	_ZN7NCrypto12CAesCbcCoder13SetInitVectorEPKhj
	.quad	_ZN7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj
	.quad	-8
	.quad	_ZTIN7NCrypto10NZipStrong8CDecoderE
	.quad	_ZThn8_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N7NCrypto10NZipStrong8CDecoder6AddRefEv
	.quad	_ZThn8_N7NCrypto10NZipStrong8CDecoder7ReleaseEv
	.quad	_ZThn8_N7NCrypto10NZipStrong8CDecoderD1Ev
	.quad	_ZThn8_N7NCrypto10NZipStrong8CDecoderD0Ev
	.quad	_ZThn8_N7NCrypto12CAesCbcCoder6SetKeyEPKhj
	.quad	_ZThn8_N7NCrypto12CAesCbcCoder13SetInitVectorEPKhj
	.quad	-328
	.quad	_ZTIN7NCrypto10NZipStrong8CDecoderE
	.quad	_ZThn328_N7NCrypto10NZipStrong8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn328_N7NCrypto10NZipStrong8CDecoder6AddRefEv
	.quad	_ZThn328_N7NCrypto10NZipStrong8CDecoder7ReleaseEv
	.quad	_ZThn328_N7NCrypto10NZipStrong8CDecoderD1Ev
	.quad	_ZThn328_N7NCrypto10NZipStrong8CDecoderD0Ev
	.quad	_ZThn328_N7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj
	.size	_ZTVN7NCrypto10NZipStrong8CDecoderE, 232

	.type	_ZTSN7NCrypto10NZipStrong8CDecoderE,@object # @_ZTSN7NCrypto10NZipStrong8CDecoderE
	.section	.rodata._ZTSN7NCrypto10NZipStrong8CDecoderE,"aG",@progbits,_ZTSN7NCrypto10NZipStrong8CDecoderE,comdat
	.weak	_ZTSN7NCrypto10NZipStrong8CDecoderE
	.p2align	4
_ZTSN7NCrypto10NZipStrong8CDecoderE:
	.asciz	"N7NCrypto10NZipStrong8CDecoderE"
	.size	_ZTSN7NCrypto10NZipStrong8CDecoderE, 32

	.type	_ZTIN7NCrypto10NZipStrong8CDecoderE,@object # @_ZTIN7NCrypto10NZipStrong8CDecoderE
	.section	.rodata._ZTIN7NCrypto10NZipStrong8CDecoderE,"aG",@progbits,_ZTIN7NCrypto10NZipStrong8CDecoderE,comdat
	.weak	_ZTIN7NCrypto10NZipStrong8CDecoderE
	.p2align	4
_ZTIN7NCrypto10NZipStrong8CDecoderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7NCrypto10NZipStrong8CDecoderE
	.quad	_ZTIN7NCrypto10NZipStrong10CBaseCoderE
	.size	_ZTIN7NCrypto10NZipStrong8CDecoderE, 24

	.type	_ZTV13CObjectVectorIN8NArchive4NZip11CMethodItemEE,@object # @_ZTV13CObjectVectorIN8NArchive4NZip11CMethodItemEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NZip11CMethodItemEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NZip11CMethodItemEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NZip11CMethodItemEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NZip11CMethodItemEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NZip11CMethodItemEE
	.quad	_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip11CMethodItemEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NZip11CMethodItemEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NZip11CMethodItemEE,@object # @_ZTS13CObjectVectorIN8NArchive4NZip11CMethodItemEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NZip11CMethodItemEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NZip11CMethodItemEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NZip11CMethodItemEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NZip11CMethodItemEE:
	.asciz	"13CObjectVectorIN8NArchive4NZip11CMethodItemEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NZip11CMethodItemEE, 47

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN8NArchive4NZip11CMethodItemEE,@object # @_ZTI13CObjectVectorIN8NArchive4NZip11CMethodItemEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NZip11CMethodItemEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NZip11CMethodItemEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NZip11CMethodItemEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NZip11CMethodItemEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NZip11CMethodItemEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NZip11CMethodItemEE, 24

	.type	_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,@object # @_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.quad	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,@object # @_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE:
	.asciz	"13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE, 50

	.type	_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,@object # @_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NZip14CExtraSubBlockEE, 24

	.type	_ZTV13CObjectVectorIN8NArchive4NZip7CItemExEE,@object # @_ZTV13CObjectVectorIN8NArchive4NZip7CItemExEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NZip7CItemExEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NZip7CItemExEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NZip7CItemExEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NZip7CItemExEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NZip7CItemExEE
	.quad	_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip7CItemExEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip7CItemExEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NZip7CItemExEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NZip7CItemExEE,@object # @_ZTS13CObjectVectorIN8NArchive4NZip7CItemExEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NZip7CItemExEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NZip7CItemExEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NZip7CItemExEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NZip7CItemExEE:
	.asciz	"13CObjectVectorIN8NArchive4NZip7CItemExEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NZip7CItemExEE, 42

	.type	_ZTI13CObjectVectorIN8NArchive4NZip7CItemExEE,@object # @_ZTI13CObjectVectorIN8NArchive4NZip7CItemExEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NZip7CItemExEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NZip7CItemExEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NZip7CItemExEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NZip7CItemExEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NZip7CItemExEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NZip7CItemExEE, 24


	.globl	_ZN8NArchive4NZip8CHandlerC1Ev
	.type	_ZN8NArchive4NZip8CHandlerC1Ev,@function
_ZN8NArchive4NZip8CHandlerC1Ev = _ZN8NArchive4NZip8CHandlerC2Ev
	.globl	_ZN8NArchive4NZip12CLzmaDecoderC1Ev
	.type	_ZN8NArchive4NZip12CLzmaDecoderC1Ev,@function
_ZN8NArchive4NZip12CLzmaDecoderC1Ev = _ZN8NArchive4NZip12CLzmaDecoderC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
