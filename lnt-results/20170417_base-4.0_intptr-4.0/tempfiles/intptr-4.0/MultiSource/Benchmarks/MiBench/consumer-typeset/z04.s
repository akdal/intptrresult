	.text
	.file	"z04.bc"
	.globl	NewToken
	.p2align	4, 0x90
	.type	NewToken,@function
NewToken:                               # @NewToken
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movl	%r8d, %r15d
	movl	%ecx, %r12d
	movl	%edx, %r13d
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movzbl	zz_lengths(%rbp), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_1
# BB#2:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_3
.LBB0_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_3:
	movb	%bpl, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movzwl	2(%rbx), %ecx
	movw	%cx, 34(%rax)
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	4(%rbx), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	movl	36(%rax), %esi
	andl	%edx, %esi
	orl	%ecx, %esi
	movl	%esi, 36(%rax)
	andl	4(%rbx), %edx
	orl	%ecx, %edx
	movl	%edx, 36(%rax)
	movb	%r13b, 42(%rax)
	movb	%r12b, 41(%rax)
	movb	%r15b, 40(%rax)
	movq	%r14, 80(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	NewToken, .Lfunc_end0-NewToken
	.cfi_endproc

	.globl	CopyTokenList
	.p2align	4, 0x90
	.type	CopyTokenList,@function
CopyTokenList:                          # @CopyTokenList
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorl	%ebx, %ebx
	testq	%r12, %r12
	je	.LBB1_13
# BB#1:                                 # %.preheader
	movq	%r12, %rbp
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r14, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movzbl	32(%rbp), %r15d
	movzbl	%r15b, %edi
	movl	%edi, %eax
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	leaq	64(%rbp), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r14, %rdx
	callq	MakeWord
	movzbl	42(%rbp), %ecx
	movb	%cl, 42(%rax)
	movzbl	41(%rbp), %ecx
	movb	%cl, 41(%rax)
	jmp	.LBB1_8
	.p2align	4, 0x90
.LBB1_4:                                #   in Loop: Header=BB1_2 Depth=1
	movzbl	42(%rbp), %r12d
	movzbl	40(%rbp), %r13d
	movzbl	41(%rbp), %r11d
	movq	80(%rbp), %r10
	movzbl	zz_lengths(%rdi), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_5
# BB#6:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_7
.LBB1_5:                                #   in Loop: Header=BB1_2 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r10, 8(%rsp)           # 8-byte Spill
	movl	%r11d, %r14d
	callq	GetMemory
	movl	%r14d, %r11d
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	(%rsp), %r14            # 8-byte Reload
	movq	%rax, zz_hold(%rip)
.LBB1_7:                                # %NewToken.exit
                                        #   in Loop: Header=BB1_2 Depth=1
	movb	%r15b, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movzwl	2(%r14), %ecx
	movw	%cx, 34(%rax)
	movl	4(%r14), %ecx
	movl	$1048575, %edx          # imm = 0xFFFFF
	andl	%edx, %ecx
	movl	36(%rax), %edx
	movl	$-1048576, %esi         # imm = 0xFFF00000
	andl	%esi, %edx
	orl	%ecx, %edx
	movl	%edx, 36(%rax)
	movl	4(%r14), %edx
	andl	%esi, %edx
	orl	%ecx, %edx
	movl	%edx, 36(%rax)
	movb	%r12b, 42(%rax)
	movb	%r11b, 41(%rax)
	movb	%r13b, 40(%rax)
	movq	%r10, 80(%rax)
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB1_8:                                #   in Loop: Header=BB1_2 Depth=1
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_12
# BB#9:                                 #   in Loop: Header=BB1_2 Depth=1
	testq	%rbx, %rbx
	je	.LBB1_10
# BB#11:                                #   in Loop: Header=BB1_2 Depth=1
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB1_12
	.p2align	4, 0x90
.LBB1_10:                               #   in Loop: Header=BB1_2 Depth=1
	movq	%rax, %rbx
.LBB1_12:                               #   in Loop: Header=BB1_2 Depth=1
	movq	24(%rbp), %rbp
	cmpq	%r12, %rbp
	jne	.LBB1_2
.LBB1_13:                               # %.loopexit
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	CopyTokenList, .Lfunc_end1-CopyTokenList
	.cfi_endproc

	.globl	EchoCatOp
	.p2align	4, 0x90
	.type	EchoCatOp,@function
EchoCatOp:                              # @EchoCatOp
	.cfi_startproc
# BB#0:
	cmpl	$17, %edi
	je	.LBB2_6
# BB#1:
	cmpl	$18, %edi
	je	.LBB2_5
# BB#2:
	cmpl	$19, %edi
	jne	.LBB2_7
# BB#3:
	testl	%edx, %edx
	movl	$.L.str.1, %eax
	movl	$.L.str.2, %ecx
	cmovneq	%rax, %rcx
	movl	$.L.str.3, %edx
	movl	$.L.str.4, %eax
	jmp	.LBB2_4
.LBB2_6:
	testl	%esi, %esi
	movl	$.L.str.9, %eax
	movl	$.L.str.11, %ecx
	cmovneq	%rax, %rcx
	testl	%edx, %edx
	movl	$.L.str.10, %eax
	cmovneq	%rcx, %rax
	retq
.LBB2_5:
	testl	%edx, %edx
	movl	$.L.str.5, %eax
	movl	$.L.str.6, %ecx
	cmovneq	%rax, %rcx
	movl	$.L.str.7, %edx
	movl	$.L.str.8, %eax
.LBB2_4:
	cmovneq	%rdx, %rax
	testl	%esi, %esi
	cmovneq	%rcx, %rax
	retq
.LBB2_7:
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 16
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	$.L.str.14, %eax
	addq	$8, %rsp
	retq
.Lfunc_end2:
	.size	EchoCatOp, .Lfunc_end2-EchoCatOp
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"^/"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"^//"
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"/"
	.size	.L.str.3, 2

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"//"
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"^|"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"^||"
	.size	.L.str.6, 4

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"|"
	.size	.L.str.7, 2

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"||"
	.size	.L.str.8, 3

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"^&"
	.size	.L.str.9, 3

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"??"
	.size	.L.str.10, 3

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"&"
	.size	.L.str.11, 2

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"assert failed in %s"
	.size	.L.str.12, 20

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"EchoCatOp"
	.size	.L.str.13, 10

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.zero	1
	.size	.L.str.14, 1


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
