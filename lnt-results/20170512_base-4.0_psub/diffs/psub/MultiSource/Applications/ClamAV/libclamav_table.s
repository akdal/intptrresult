	.text
	.file	"libclamav_table.bc"
	.globl	tableCreate
	.p2align	4, 0x90
	.type	tableCreate,@function
tableCreate:                            # @tableCreate
	.cfi_startproc
# BB#0:
	movl	$1, %edi
	movl	$24, %esi
	jmp	cli_calloc              # TAILCALL
.Lfunc_end0:
	.size	tableCreate, .Lfunc_end0-tableCreate
	.cfi_endproc

	.globl	tableDestroy
	.p2align	4, 0x90
	.type	tableDestroy,@function
tableDestroy:                           # @tableDestroy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB1_4
	.p2align	4, 0x90
.LBB1_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	16(%rbx), %r15
	testq	%rdi, %rdi
	je	.LBB1_3
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	callq	free
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	movq	%rbx, %rdi
	callq	free
	testq	%r15, %r15
	movq	%r15, %rbx
	jne	.LBB1_1
.LBB1_4:                                # %._crit_edge
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	tableDestroy, .Lfunc_end1-tableDestroy
	.cfi_endproc

	.globl	tableInsert
	.p2align	4, 0x90
	.type	tableInsert,@function
tableInsert:                            # @tableInsert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 48
.Lcfi11:
	.cfi_offset %rbx, -48
.Lcfi12:
	.cfi_offset %r12, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r12
	movq	%rdi, %r15
	testq	%r12, %r12
	movq	(%r15), %rbp
	je	.LBB2_6
# BB#1:
	testq	%rbp, %rbp
	je	.LBB2_7
# BB#2:                                 # %.lr.ph.i.preheader
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_5
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=1
	movq	%r12, %rsi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB2_8
.LBB2_5:                                #   in Loop: Header=BB2_3 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_3
.LBB2_6:                                # %tableFind.exit.thread
	testq	%rbp, %rbp
	je	.LBB2_7
# BB#10:
	movl	16(%r15), %eax
	testb	$1, %al
	je	.LBB2_15
	.p2align	4, 0x90
.LBB2_11:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rbp)
	je	.LBB2_12
# BB#13:                                #   in Loop: Header=BB2_11 Depth=1
	movq	16(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB2_11
# BB#14:                                # %._crit_edge
	andl	$-2, %eax
	movl	%eax, 16(%r15)
.LBB2_15:
	movl	$24, %edi
	callq	cli_malloc
	movq	8(%r15), %rcx
	addq	$8, %r15
	movq	%rax, 16(%rcx)
	jmp	.LBB2_16
.LBB2_7:                                # %tableFind.exit.thread.thread
	movl	$24, %edi
	callq	cli_malloc
	movq	%rax, (%r15)
	addq	$8, %r15
.LBB2_16:
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.LBB2_17
# BB#18:
	movq	$0, 16(%rax)
	movq	%r12, %rdi
	callq	cli_strdup
	movq	(%r15), %rcx
	movq	%rax, (%rcx)
	movl	%r14d, 8(%rcx)
	jmp	.LBB2_19
.LBB2_17:
	movl	$-1, %r14d
	jmp	.LBB2_19
.LBB2_8:                                # %tableFind.exit
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB2_6
# BB#9:
	cmpl	%r14d, %eax
	movl	$-1, %eax
	cmovnel	%eax, %r14d
	jmp	.LBB2_19
.LBB2_12:                               # %.critedge
	movq	%r12, %rdi
	callq	cli_strdup
	movq	%rax, (%rbp)
	movl	%r14d, 8(%rbp)
.LBB2_19:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	tableInsert, .Lfunc_end2-tableInsert
	.cfi_endproc

	.globl	tableFind
	.p2align	4, 0x90
	.type	tableFind,@function
tableFind:                              # @tableFind
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	$-1, %r14d
	testq	%rbx, %rbx
	je	.LBB3_6
# BB#1:
	movq	(%rdi), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_3
	jmp	.LBB3_6
	.p2align	4, 0x90
.LBB3_7:                                #   in Loop: Header=BB3_3 Depth=1
	movq	16(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB3_6
.LBB3_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_7
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	%rbx, %rsi
	callq	strcasecmp
	testl	%eax, %eax
	jne	.LBB3_7
# BB#5:
	movl	8(%rbp), %r14d
.LBB3_6:                                # %.loopexit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	tableFind, .Lfunc_end3-tableFind
	.cfi_endproc

	.globl	tableUpdate
	.p2align	4, 0x90
	.type	tableUpdate,@function
tableUpdate:                            # @tableUpdate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%rbx, %rbx
	je	.LBB4_1
# BB#2:
	movq	(%r15), %rbp
	testq	%rbp, %rbp
	jne	.LBB4_4
	jmp	.LBB4_9
	.p2align	4, 0x90
.LBB4_8:                                #   in Loop: Header=BB4_4 Depth=1
	movq	16(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB4_9
.LBB4_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_8
# BB#5:                                 #   in Loop: Header=BB4_4 Depth=1
	movq	%rbx, %rsi
	callq	strcasecmp
	testl	%eax, %eax
	jne	.LBB4_8
# BB#6:
	movl	%r14d, 8(%rbp)
	jmp	.LBB4_7
.LBB4_9:                                # %._crit_edge
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movl	%r14d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	tableInsert             # TAILCALL
.LBB4_1:
	movl	$-1, %r14d
.LBB4_7:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	tableUpdate, .Lfunc_end4-tableUpdate
	.cfi_endproc

	.globl	tableRemove
	.p2align	4, 0x90
	.type	tableRemove,@function
tableRemove:                            # @tableRemove
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 48
.Lcfi36:
	.cfi_offset %rbx, -40
.Lcfi37:
	.cfi_offset %r12, -32
.Lcfi38:
	.cfi_offset %r14, -24
.Lcfi39:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r15, %r15
	je	.LBB5_7
# BB#1:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_3
	jmp	.LBB5_7
	.p2align	4, 0x90
.LBB5_6:                                #   in Loop: Header=BB5_3 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB5_7
.LBB5_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.LBB5_6
# BB#4:                                 #   in Loop: Header=BB5_3 Depth=1
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	strcasecmp
	testl	%eax, %eax
	jne	.LBB5_6
# BB#5:                                 #   in Loop: Header=BB5_3 Depth=1
	movq	%r12, %rdi
	callq	free
	movq	$0, (%rbx)
	orb	$1, 16(%r14)
	jmp	.LBB5_6
.LBB5_7:                                # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	tableRemove, .Lfunc_end5-tableRemove
	.cfi_endproc

	.globl	tableIterate
	.p2align	4, 0x90
	.type	tableIterate,@function
tableIterate:                           # @tableIterate
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -32
.Lcfi44:
	.cfi_offset %r14, -24
.Lcfi45:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	testq	%rdi, %rdi
	je	.LBB6_6
# BB#1:
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.LBB6_3
	jmp	.LBB6_6
	.p2align	4, 0x90
.LBB6_5:                                #   in Loop: Header=BB6_3 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB6_6
.LBB6_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_5
# BB#4:                                 #   in Loop: Header=BB6_3 Depth=1
	movl	8(%rbx), %esi
	movq	%r14, %rdx
	callq	*%r15
	jmp	.LBB6_5
.LBB6_6:                                # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	tableIterate, .Lfunc_end6-tableIterate
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
