	.text
	.file	"spectral-norm.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	eval_A
	.p2align	4, 0x90
	.type	eval_A,@function
eval_A:                                 # @eval_A
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	(%rsi,%rdi), %eax
	leal	1(%rsi,%rdi), %ecx
	imull	%eax, %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	leal	1(%rdi,%rax), %eax
	cvtsi2sdl	%eax, %xmm1
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	retq
.Lfunc_end0:
	.size	eval_A, .Lfunc_end0-eval_A
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	eval_A_times_u
	.p2align	4, 0x90
	.type	eval_A_times_u,@function
eval_A_times_u:                         # @eval_A_times_u
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	jle	.LBB1_5
# BB#1:                                 # %.lr.ph20.split.us.preheader
	movl	%edi, %r8d
	xorl	%r10d, %r10d
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph20.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
	movq	$0, (%rdx,%r10,8)
	leaq	1(%r10), %r9
	xorpd	%xmm1, %xmm1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_3:                                #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r10,%rax), %edi
	leal	1(%r10,%rax), %ecx
	imull	%edi, %ecx
	movl	%ecx, %edi
	shrl	$31, %edi
	addl	%ecx, %edi
	sarl	%edi
	addl	%r9d, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	movapd	%xmm0, %xmm3
	divsd	%xmm2, %xmm3
	mulsd	(%rsi,%rax,8), %xmm3
	addsd	%xmm3, %xmm1
	movsd	%xmm1, (%rdx,%r10,8)
	incq	%rax
	cmpq	%rax, %r8
	jne	.LBB1_3
# BB#4:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB1_2 Depth=1
	incq	%r10
	cmpq	%rax, %r10
	movq	%r9, %r10
	jne	.LBB1_2
.LBB1_5:                                # %._crit_edge21
	retq
.Lfunc_end1:
	.size	eval_A_times_u, .Lfunc_end1-eval_A_times_u
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	eval_At_times_u
	.p2align	4, 0x90
	.type	eval_At_times_u,@function
eval_At_times_u:                        # @eval_At_times_u
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	jle	.LBB2_5
# BB#1:                                 # %.lr.ph21.split.us.preheader
	movl	%edi, %r8d
	xorl	%r9d, %r9d
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph21.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
	movq	$0, (%rdx,%r9,8)
	xorpd	%xmm1, %xmm1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_3:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r9,%rdi), %eax
	leal	1(%r9,%rdi), %ecx
	imull	%eax, %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	leal	1(%rdi,%rax), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm3
	divsd	%xmm2, %xmm3
	mulsd	(%rsi,%rdi,8), %xmm3
	incq	%rdi
	addsd	%xmm3, %xmm1
	movsd	%xmm1, (%rdx,%r9,8)
	cmpq	%rdi, %r8
	jne	.LBB2_3
# BB#4:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB2_2 Depth=1
	incq	%r9
	cmpq	%rdi, %r9
	jne	.LBB2_2
.LBB2_5:                                # %._crit_edge22
	retq
.Lfunc_end2:
	.size	eval_At_times_u, .Lfunc_end2-eval_At_times_u
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	eval_AtA_times_u
	.p2align	4, 0x90
	.type	eval_AtA_times_u,@function
eval_AtA_times_u:                       # @eval_AtA_times_u
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi2:
	.cfi_def_cfa_register %rbp
	movl	%edi, %r9d
	leaq	15(,%r9,8), %rax
	movabsq	$68719476720, %rcx      # imm = 0xFFFFFFFF0
	andq	%rax, %rcx
	movq	%rsp, %r8
	subq	%rcx, %r8
	movq	%r8, %rsp
	testl	%edi, %edi
	jle	.LBB3_9
# BB#1:                                 # %.lr.ph20.split.us.i.preheader
	xorl	%r11d, %r11d
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph20.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_3 Depth 2
	movq	$0, (%r8,%r11,8)
	leaq	1(%r11), %r10
	xorpd	%xmm1, %xmm1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_3:                                #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r11,%rax), %ecx
	leal	1(%r11,%rax), %edi
	imull	%ecx, %edi
	movl	%edi, %ecx
	shrl	$31, %ecx
	addl	%edi, %ecx
	sarl	%ecx
	addl	%r10d, %ecx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%ecx, %xmm2
	movapd	%xmm0, %xmm3
	divsd	%xmm2, %xmm3
	mulsd	(%rsi,%rax,8), %xmm3
	addsd	%xmm3, %xmm1
	incq	%rax
	cmpq	%rax, %r9
	jne	.LBB3_3
# BB#4:                                 # %._crit_edge.us.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movsd	%xmm1, (%r8,%r11,8)
	cmpq	%r9, %r10
	movq	%r10, %r11
	jne	.LBB3_2
# BB#5:                                 # %.lr.ph21.split.us.i.preheader
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph21.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_7 Depth 2
	movq	$0, (%rdx,%rsi,8)
	xorpd	%xmm1, %xmm1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_7:                                #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rsi,%rax), %ecx
	leal	1(%rsi,%rax), %edi
	imull	%ecx, %edi
	movl	%edi, %ecx
	shrl	$31, %ecx
	addl	%edi, %ecx
	sarl	%ecx
	leal	1(%rax,%rcx), %ecx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%ecx, %xmm2
	movapd	%xmm0, %xmm3
	divsd	%xmm2, %xmm3
	mulsd	(%r8,%rax,8), %xmm3
	incq	%rax
	addsd	%xmm3, %xmm1
	cmpq	%rax, %r9
	jne	.LBB3_7
# BB#8:                                 # %._crit_edge.us.i9
                                        #   in Loop: Header=BB3_6 Depth=1
	movsd	%xmm1, (%rdx,%rsi,8)
	incq	%rsi
	cmpq	%r9, %rsi
	jne	.LBB3_6
.LBB3_9:                                # %eval_At_times_u.exit
	movq	%rbp, %rsp
	popq	%rbp
	retq
.Lfunc_end3:
	.size	eval_AtA_times_u, .Lfunc_end3-eval_AtA_times_u
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi5:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	pushq	%rax
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
	cmpl	$2, %edi
	jne	.LBB4_1
# BB#2:
	movq	8(%rsi), %rdi
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r15
	movl	%r15d, %ebx
	leaq	15(,%rbx,8), %rax
	movabsq	$68719476720, %rcx      # imm = 0xFFFFFFFF0
	andq	%rax, %rcx
	movq	%rsp, %r12
	subq	%rcx, %r12
	movq	%r12, %rsp
	movq	%rsp, %r14
	subq	%rcx, %r14
	movq	%r14, %rsp
	testl	%r15d, %r15d
	jg	.LBB4_3
	jmp	.LBB4_17
.LBB4_1:                                # %.thread
	movq	%rsp, %r12
	addq	$-16000, %r12           # imm = 0xC180
	movq	%r12, %rsp
	movq	%rsp, %r14
	addq	$-16000, %r14           # imm = 0xC180
	movq	%r14, %rsp
	movl	$2000, %r15d            # imm = 0x7D0
	movl	$2000, %ebx             # imm = 0x7D0
.LBB4_3:                                # %.lr.ph41.preheader
	cmpq	$3, %rbx
	jbe	.LBB4_4
# BB#8:                                 # %min.iters.checked
	movl	$4294967292, %eax       # imm = 0xFFFFFFFC
	andq	%rbx, %rax
	je	.LBB4_4
# BB#9:                                 # %vector.body.preheader
	leaq	-4(%rax), %rcx
	movl	%ecx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB4_10
# BB#11:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	movapd	.LCPI4_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB4_12:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, (%r12,%rdx,8)
	movupd	%xmm0, 16(%r12,%rdx,8)
	addq	$4, %rdx
	incq	%rsi
	jne	.LBB4_12
	jmp	.LBB4_13
.LBB4_4:
	xorl	%eax, %eax
.LBB4_5:                                # %.lr.ph41.preheader52
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph41
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%r12,%rax,8)
	incq	%rax
	cmpq	%rax, %rbx
	jne	.LBB4_6
# BB#7:
	movb	$1, %r13b
.LBB4_17:                               # %.preheader33.preheader
	movl	%r15d, %edi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	eval_AtA_times_u
	movl	%r15d, %edi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	eval_AtA_times_u
	xorpd	%xmm0, %xmm0
	testb	%r13b, %r13b
	je	.LBB4_24
# BB#18:                                # %.lr.ph.preheader
	testb	$1, %bl
	jne	.LBB4_20
# BB#19:
	xorpd	%xmm1, %xmm1
	xorl	%ecx, %ecx
                                        # implicit-def: %XMM0
	cmpq	$1, %rbx
	jne	.LBB4_22
	jmp	.LBB4_24
.LBB4_20:                               # %.lr.ph.prol
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	movsd	(%r14), %xmm1           # xmm1 = mem[0],zero
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm0, %xmm1
	xorpd	%xmm0, %xmm0
	addpd	%xmm1, %xmm0
	movl	$1, %ecx
	movapd	%xmm0, %xmm1
	cmpq	$1, %rbx
	je	.LBB4_24
.LBB4_22:                               # %.lr.ph.preheader.new
	subq	%rcx, %rbx
	leaq	8(%r12,%rcx,8), %rax
	leaq	8(%r14,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB4_23:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	-8(%rax), %xmm2         # xmm2 = mem[0],zero
	movsd	(%rax), %xmm3           # xmm3 = mem[0],zero
	movsd	-8(%rcx), %xmm4         # xmm4 = mem[0],zero
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	unpcklpd	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0]
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm2, %xmm4
	addpd	%xmm1, %xmm4
	unpcklpd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0]
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	%xmm3, %xmm0
	addpd	%xmm4, %xmm0
	addq	$16, %rax
	addq	$16, %rcx
	addq	$-2, %rbx
	movapd	%xmm0, %xmm1
	jne	.LBB4_23
.LBB4_24:                               # %._crit_edge
	movapd	%xmm0, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	divsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB4_26
# BB#25:                                # %call.sqrt
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB4_26:                               # %._crit_edge.split
	movl	$.L.str, %edi
	movb	$1, %al
	movapd	%xmm1, %xmm0
	callq	printf
	xorl	%eax, %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_10:
	xorl	%edx, %edx
.LBB4_13:                               # %vector.body.prol.loopexit
	cmpq	$28, %rcx
	jb	.LBB4_16
# BB#14:                                # %vector.body.preheader.new
	movq	%rax, %rcx
	subq	%rdx, %rcx
	leaq	240(%r12,%rdx,8), %rdx
	movapd	.LCPI4_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB4_15:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -240(%rdx)
	movupd	%xmm0, -224(%rdx)
	movupd	%xmm0, -208(%rdx)
	movupd	%xmm0, -192(%rdx)
	movupd	%xmm0, -176(%rdx)
	movupd	%xmm0, -160(%rdx)
	movupd	%xmm0, -144(%rdx)
	movupd	%xmm0, -128(%rdx)
	movupd	%xmm0, -112(%rdx)
	movupd	%xmm0, -96(%rdx)
	movupd	%xmm0, -80(%rdx)
	movupd	%xmm0, -64(%rdx)
	movupd	%xmm0, -48(%rdx)
	movupd	%xmm0, -32(%rdx)
	movupd	%xmm0, -16(%rdx)
	movupd	%xmm0, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-32, %rcx
	jne	.LBB4_15
.LBB4_16:                               # %middle.block
	movb	$1, %r13b
	cmpq	%rax, %rbx
	jne	.LBB4_5
	jmp	.LBB4_17
.Lfunc_end4:
	.size	main, .Lfunc_end4-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.9f\n"
	.size	.L.str, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
