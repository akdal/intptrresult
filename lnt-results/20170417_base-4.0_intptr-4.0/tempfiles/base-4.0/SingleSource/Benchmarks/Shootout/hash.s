	.text
	.file	"hash.bc"
	.globl	ht_node_create
	.p2align	4, 0x90
	.type	ht_node_create,@function
ht_node_create:                         # @ht_node_create
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_1
# BB#3:
	movq	%r14, %rdi
	callq	__strdup
	testq	%rax, %rax
	je	.LBB0_4
# BB#5:
	movq	%rax, (%rbx)
	movl	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_1:
	movl	$.L.str, %edi
	jmp	.LBB0_2
.LBB0_4:
	movl	$.L.str.1, %edi
.LBB0_2:
	callq	perror
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	ht_node_create, .Lfunc_end0-ht_node_create
	.cfi_endproc

	.globl	ht_create
	.p2align	4, 0x90
	.type	ht_create,@function
ht_create:                              # @ht_create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movl	$40, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	$ht_prime_list, %eax
	movslq	%ebp, %rcx
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdx
	addq	$8, %rax
	cmpq	%rcx, %rdx
	jb	.LBB1_1
# BB#2:
	movl	%edx, (%rbx)
	movslq	%edx, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, 8(%rbx)
	movl	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movl	$0, 32(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	ht_create, .Lfunc_end1-ht_create
	.cfi_endproc

	.globl	ht_destroy
	.p2align	4, 0x90
	.type	ht_destroy,@function
ht_destroy:                             # @ht_destroy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r12, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	(%r14), %eax
	movq	8(%r14), %rdi
	testl	%eax, %eax
	jle	.LBB2_6
# BB#1:                                 # %.lr.ph20.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph20
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
	movq	(%rdi,%r15,8), %rbx
	testq	%rbx, %rbx
	je	.LBB2_5
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rdi
	movq	16(%rbx), %r12
	callq	free
	movq	%rbx, %rdi
	callq	free
	testq	%r12, %r12
	movq	%r12, %rbx
	jne	.LBB2_3
# BB#4:                                 # %._crit_edge.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	(%r14), %eax
	movq	8(%r14), %rdi
.LBB2_5:                                # %._crit_edge
                                        #   in Loop: Header=BB2_2 Depth=1
	incq	%r15
	movslq	%eax, %rcx
	cmpq	%rcx, %r15
	jl	.LBB2_2
.LBB2_6:                                # %._crit_edge21
	callq	free
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end2:
	.size	ht_destroy, .Lfunc_end2-ht_destroy
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 144
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movl	$3500000, %r14d         # imm = 0x3567E0
	cmpl	$2, %edi
	jne	.LBB3_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
.LBB3_2:
	movl	$40, %edi
	callq	malloc
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$ht_prime_list, %eax
	movslq	%r14d, %rcx
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdx
	addq	$8, %rax
	cmpq	%rcx, %rdx
	jb	.LBB3_3
# BB#4:                                 # %ht_create.exit
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	%edx, (%rbx)
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movslq	%edx, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, 8(%rbx)
	movl	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movl	$0, 32(%rbx)
	testl	%r14d, %r14d
	jle	.LBB3_9
# BB#5:                                 # %.lr.ph46
	movslq	24(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$1, %ebp
	leaq	48(%rsp), %r12
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_11 Depth 2
                                        #     Child Loop BB3_14 Depth 2
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%ebp, %edx
	callq	sprintf
	movb	48(%rsp), %cl
	testb	%cl, %cl
	je	.LBB3_7
# BB#10:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB3_6 Depth=1
	leaq	49(%rsp), %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_11:                               # %.lr.ph.i.i
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rax,4), %rsi
	movsbq	%cl, %rax
	addq	%rsi, %rax
	movzbl	(%rdx), %ecx
	incq	%rdx
	testb	%cl, %cl
	jne	.LBB3_11
	jmp	.LBB3_12
	.p2align	4, 0x90
.LBB3_7:                                #   in Loop: Header=BB3_6 Depth=1
	xorl	%eax, %eax
.LBB3_12:                               # %ht_hashcode.exit.i
                                        #   in Loop: Header=BB3_6 Depth=1
	xorl	%edx, %edx
	divq	40(%rsp)                # 8-byte Folded Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %r13
	movslq	%edx, %r14
	movq	(%r13,%r14,8), %rax
	testq	%rax, %rax
	je	.LBB3_13
	.p2align	4, 0x90
.LBB3_14:                               # %.lr.ph.i
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %r15
	movq	(%r15), %rsi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_22
# BB#15:                                #   in Loop: Header=BB3_14 Depth=2
	movq	16(%r15), %rax
	testq	%rax, %rax
	jne	.LBB3_14
	jmp	.LBB3_16
	.p2align	4, 0x90
.LBB3_13:                               #   in Loop: Header=BB3_6 Depth=1
	xorl	%r15d, %r15d
.LBB3_16:                               # %._crit_edge.i
                                        #   in Loop: Header=BB3_6 Depth=1
	movl	20(%rsp), %ecx          # 4-byte Reload
	incl	%ecx
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movl	%ecx, 32(%rax)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_17
# BB#19:                                #   in Loop: Header=BB3_6 Depth=1
	movq	%r12, %rdi
	callq	__strdup
	testq	%rax, %rax
	je	.LBB3_20
# BB#21:                                # %ht_node_create.exit.i
                                        #   in Loop: Header=BB3_6 Depth=1
	leaq	(%r13,%r14,8), %rcx
	testq	%r15, %r15
	leaq	16(%r15), %rdx
	movq	%rax, (%rbx)
	movl	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	cmoveq	%rcx, %rdx
	movq	%rbx, (%rdx)
	movq	%rbx, %r15
.LBB3_22:                               # %ht_find_new.exit
                                        #   in Loop: Header=BB3_6 Depth=1
	movl	%ebp, 8(%r15)
	movq	32(%rsp), %rbx          # 8-byte Reload
	cmpl	%ebx, %ebp
	leal	1(%rbp), %eax
	movl	%eax, %ebp
	jl	.LBB3_6
# BB#8:                                 # %.preheader
	testl	%ebx, %ebx
	jle	.LBB3_9
# BB#23:                                # %.lr.ph
	movslq	24(%rsp), %r14          # 4-byte Folded Reload
	leaq	48(%rsp), %r13
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_24:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_27 Depth 2
                                        #     Child Loop BB3_30 Depth 2
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%ebx, %edx
	callq	sprintf
	movb	48(%rsp), %cl
	testb	%cl, %cl
	je	.LBB3_25
# BB#26:                                # %.lr.ph.i.i28.preheader
                                        #   in Loop: Header=BB3_24 Depth=1
	leaq	49(%rsp), %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_27:                               # %.lr.ph.i.i28
                                        #   Parent Loop BB3_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rax,4), %rsi
	movsbq	%cl, %rax
	addq	%rsi, %rax
	movzbl	(%rdx), %ecx
	incq	%rdx
	testb	%cl, %cl
	jne	.LBB3_27
	jmp	.LBB3_28
	.p2align	4, 0x90
.LBB3_25:                               #   in Loop: Header=BB3_24 Depth=1
	xorl	%eax, %eax
.LBB3_28:                               # %ht_hashcode.exit.i31
                                        #   in Loop: Header=BB3_24 Depth=1
	xorl	%edx, %edx
	divq	%r14
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %r12
	movslq	%edx, %rax
	movq	(%r12,%rax,8), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_30
	jmp	.LBB3_32
	.p2align	4, 0x90
.LBB3_31:                               #   in Loop: Header=BB3_30 Depth=2
	movq	16(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB3_32
.LBB3_30:                               # %.lr.ph.i32
                                        #   Parent Loop BB3_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rsi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB3_31
	jmp	.LBB3_33
	.p2align	4, 0x90
.LBB3_32:                               #   in Loop: Header=BB3_24 Depth=1
	xorl	%ebp, %ebp
.LBB3_33:                               # %ht_find.exit
                                        #   in Loop: Header=BB3_24 Depth=1
	cmpq	$1, %rbp
	sbbl	$-1, %r15d
	cmpl	$1, %ebx
	leal	-1(%rbx), %eax
	movl	%eax, %ebx
	jg	.LBB3_24
	jmp	.LBB3_34
.LBB3_9:                                # %.preheader.._crit_edge_crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %r12
	xorl	%r15d, %r15d
.LBB3_34:                               # %._crit_edge
	movq	24(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB3_39
# BB#35:                                # %.lr.ph20.i.preheader
	movslq	%eax, %r14
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_36:                               # %.lr.ph20.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_37 Depth 2
	movq	(%r12,%r13,8), %rbx
	testq	%rbx, %rbx
	je	.LBB3_38
	.p2align	4, 0x90
.LBB3_37:                               # %.lr.ph.i36
                                        #   Parent Loop BB3_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rdi
	movq	16(%rbx), %rbp
	callq	free
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB3_37
.LBB3_38:                               # %._crit_edge.i38
                                        #   in Loop: Header=BB3_36 Depth=1
	incq	%r13
	cmpq	%r14, %r13
	jl	.LBB3_36
.LBB3_39:                               # %ht_destroy.exit
	movq	%r12, %rdi
	callq	free
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	free
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	printf
	xorl	%eax, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_17:
	movl	$.L.str, %edi
	jmp	.LBB3_18
.LBB3_20:
	movl	$.L.str.1, %edi
.LBB3_18:
	callq	perror
	movl	$1, %edi
	callq	exit
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"malloc ht_node"
	.size	.L.str, 15

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"strdup newkey"
	.size	.L.str.1, 14

	.type	ht_prime_list,@object   # @ht_prime_list
	.section	.rodata,"a",@progbits
	.p2align	4
ht_prime_list:
	.quad	53                      # 0x35
	.quad	97                      # 0x61
	.quad	193                     # 0xc1
	.quad	389                     # 0x185
	.quad	769                     # 0x301
	.quad	1543                    # 0x607
	.quad	3079                    # 0xc07
	.quad	6151                    # 0x1807
	.quad	12289                   # 0x3001
	.quad	24593                   # 0x6011
	.quad	49157                   # 0xc005
	.quad	98317                   # 0x1800d
	.quad	196613                  # 0x30005
	.quad	393241                  # 0x60019
	.quad	786433                  # 0xc0001
	.quad	1572869                 # 0x180005
	.quad	3145739                 # 0x30000b
	.quad	6291469                 # 0x60000d
	.quad	12582917                # 0xc00005
	.quad	25165843                # 0x1800013
	.quad	50331653                # 0x3000005
	.quad	100663319               # 0x6000017
	.quad	201326611               # 0xc000013
	.quad	402653189               # 0x18000005
	.quad	805306457               # 0x30000059
	.quad	1610612741              # 0x60000005
	.quad	3221225473              # 0xc0000001
	.quad	4294967291              # 0xfffffffb
	.size	ht_prime_list, 224

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"%x"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%d"
	.size	.L.str.3, 3

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%d\n"
	.size	.L.str.4, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
