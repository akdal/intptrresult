	.text
	.file	"main.bc"
	.globl	CountTree
	.p2align	4, 0x90
	.type	CountTree,@function
CountTree:                              # @CountTree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rax
	movq	8(%rax), %rdi
	movq	16(%rax), %r15
	movq	24(%rax), %r14
	movq	32(%rax), %r13
	testq	%rdi, %rdi
	jne	.LBB0_4
# BB#1:
	testq	%r15, %r15
	jne	.LBB0_4
# BB#2:
	testq	%r14, %r14
	jne	.LBB0_4
# BB#3:
	movl	$1, %r12d
	testq	%r13, %r13
	je	.LBB0_9
.LBB0_4:                                # %tailrecurse.preheader
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB0_5:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	callq	CountTree
	movl	%eax, %ebx
	movq	%r15, %rdi
	callq	CountTree
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	CountTree
	addl	%r12d, %ebx
	addl	%ebp, %ebx
	movl	%ebx, %r12d
	addl	%eax, %r12d
	movq	8(%r13), %rdi
	movq	16(%r13), %r15
	movq	24(%r13), %r14
	movq	32(%r13), %r13
	testq	%rdi, %rdi
	jne	.LBB0_5
# BB#6:                                 # %tailrecurse
                                        #   in Loop: Header=BB0_5 Depth=1
	testq	%r15, %r15
	jne	.LBB0_5
# BB#7:                                 # %tailrecurse
                                        #   in Loop: Header=BB0_5 Depth=1
	testq	%r14, %r14
	jne	.LBB0_5
# BB#8:                                 # %tailrecurse
                                        #   in Loop: Header=BB0_5 Depth=1
	testq	%r13, %r13
	jne	.LBB0_5
.LBB0_9:                                # %tailrecurse._crit_edge
	movl	%r12d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	CountTree, .Lfunc_end0-CountTree
	.cfi_endproc

	.globl	perimeter
	.p2align	4, 0x90
	.type	perimeter,@function
perimeter:                              # @perimeter
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	movl	(%r15), %eax
	testl	%eax, %eax
	je	.LBB1_3
# BB#1:
	xorl	%ebp, %ebp
	cmpl	$2, %eax
	jne	.LBB1_23
# BB#2:
	movq	24(%r15), %rdi
	movl	%r14d, %ebp
	shrl	$31, %ebp
	addl	%r14d, %ebp
	sarl	%ebp
	movl	%ebp, %esi
	callq	perimeter
	movl	%eax, %r14d
	movq	32(%r15), %rdi
	movl	%ebp, %esi
	callq	perimeter
	movl	%eax, %ebx
	addl	%r14d, %ebx
	movq	16(%r15), %rdi
	movl	%ebp, %esi
	callq	perimeter
	movl	%eax, %r14d
	addl	%ebx, %r14d
	movq	8(%r15), %rdi
	movl	%ebp, %esi
	callq	perimeter
	addl	%r14d, %eax
	jmp	.LBB1_24
.LBB1_3:
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	gtequal_adj_neighbor
	testq	%rax, %rax
	movl	%r14d, %ebp
	je	.LBB1_8
# BB#4:
	movl	(%rax), %ecx
	cmpl	$1, %ecx
	movl	%r14d, %ebp
	je	.LBB1_8
# BB#5:
	cmpl	$2, %ecx
	jne	.LBB1_7
# BB#6:
	movl	$3, %esi
	movl	$2, %edx
	movq	%rax, %rdi
	movl	%r14d, %ecx
	callq	sum_adjacent
	movl	%eax, %ebp
	jmp	.LBB1_8
.LBB1_7:                                # %.fold.split
	xorl	%ebp, %ebp
.LBB1_8:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	gtequal_adj_neighbor
	testq	%rax, %rax
	je	.LBB1_11
# BB#9:
	movl	(%rax), %ecx
	cmpl	$2, %ecx
	je	.LBB1_12
# BB#10:
	cmpl	$1, %ecx
	jne	.LBB1_13
.LBB1_11:
	addl	%r14d, %ebp
	jmp	.LBB1_13
.LBB1_12:
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	%r14d, %ecx
	callq	sum_adjacent
	addl	%eax, %ebp
.LBB1_13:
	movl	$2, %esi
	movq	%r15, %rdi
	callq	gtequal_adj_neighbor
	testq	%rax, %rax
	je	.LBB1_16
# BB#14:
	movl	(%rax), %ecx
	cmpl	$2, %ecx
	je	.LBB1_17
# BB#15:
	cmpl	$1, %ecx
	jne	.LBB1_18
.LBB1_16:
	addl	%r14d, %ebp
	jmp	.LBB1_18
.LBB1_17:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, %rdi
	movl	%r14d, %ecx
	callq	sum_adjacent
	addl	%eax, %ebp
.LBB1_18:
	movl	$3, %esi
	movq	%r15, %rdi
	callq	gtequal_adj_neighbor
	testq	%rax, %rax
	je	.LBB1_21
# BB#19:
	movl	(%rax), %ecx
	cmpl	$2, %ecx
	je	.LBB1_22
# BB#20:
	cmpl	$1, %ecx
	jne	.LBB1_23
.LBB1_21:
	addl	%r14d, %ebp
	jmp	.LBB1_23
.LBB1_22:
	movl	$1, %esi
	movl	$3, %edx
	movq	%rax, %rdi
	movl	%r14d, %ecx
	callq	sum_adjacent
	addl	%eax, %ebp
.LBB1_23:
	movl	%ebp, %eax
.LBB1_24:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	perimeter, .Lfunc_end1-perimeter
	.cfi_endproc

	.p2align	4, 0x90
	.type	gtequal_adj_neighbor,@function
gtequal_adj_neighbor:                   # @gtequal_adj_neighbor
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.LBB2_10
# BB#1:
	movslq	4(%rdi), %r14
	cmpl	$3, %ebx
	ja	.LBB2_3
# BB#2:                                 # %switch.lookup
	movslq	%ebx, %rcx
	movl	.Lswitch.table.2(,%rcx,4), %edx
	orl	%r14d, %edx
	cmpl	.Lswitch.table.3(,%rcx,4), %edx
	jne	.LBB2_4
.LBB2_3:                                # %adj.exit.thread
	movq	%rax, %rdi
	movl	%ebx, %esi
	callq	gtequal_adj_neighbor
	testq	%rax, %rax
	je	.LBB2_10
.LBB2_4:                                # %.thread21
	cmpl	$2, (%rax)
	jne	.LBB2_13
# BB#5:
	cmpl	$3, %r14d
	ja	.LBB2_11
# BB#6:                                 # %reflect.exit
	orl	$2, %ebx
	cmpl	$3, %ebx
	movl	$.Lswitch.table, %ecx
	movl	$.Lswitch.table.1, %edx
	cmoveq	%rcx, %rdx
	movl	(%rdx,%r14,4), %ecx
	cmpq	$3, %rcx
	ja	.LBB2_14
# BB#7:                                 # %reflect.exit
	jmpq	*.LJTI2_0(,%rcx,8)
.LBB2_8:
	addq	$8, %rax
	jmp	.LBB2_12
.LBB2_10:
	xorl	%eax, %eax
	jmp	.LBB2_13
.LBB2_11:                               # %reflect.exit.thread
	addq	$16, %rax
.LBB2_12:                               # %.sink.split.i
	movq	(%rax), %rax
.LBB2_13:                               # %child.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_14:
	xorl	%eax, %eax
	jmp	.LBB2_13
.LBB2_15:
	addq	$24, %rax
	jmp	.LBB2_12
.LBB2_16:
	addq	$32, %rax
	jmp	.LBB2_12
.Lfunc_end2:
	.size	gtequal_adj_neighbor, .Lfunc_end2-gtequal_adj_neighbor
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_8
	.quad	.LBB2_11
	.quad	.LBB2_15
	.quad	.LBB2_16

	.text
	.p2align	4, 0x90
	.type	sum_adjacent,@function
sum_adjacent:                           # @sum_adjacent
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 48
.Lcfi32:
	.cfi_offset %rbx, -48
.Lcfi33:
	.cfi_offset %r12, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB3_20
# BB#1:
	cmpl	$2, %eax
	jne	.LBB3_19
# BB#2:
	cmpl	$3, %r15d
	ja	.LBB3_3
# BB#4:
	movl	%r15d, %eax
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_6:
	leaq	8(%rbx), %rax
	jmp	.LBB3_9
.LBB3_19:
	xorl	%ecx, %ecx
.LBB3_20:
	movl	%ecx, %eax
	jmp	.LBB3_21
.LBB3_3:
	xorl	%edi, %edi
	jmp	.LBB3_10
.LBB3_5:
	leaq	16(%rbx), %rax
	jmp	.LBB3_9
.LBB3_8:
	leaq	24(%rbx), %rax
	jmp	.LBB3_9
.LBB3_7:
	leaq	32(%rbx), %rax
.LBB3_9:                                # %.sink.split.i
	movq	(%rax), %rdi
.LBB3_10:                               # %child.exit
	movl	%ecx, %ebp
	shrl	$31, %ebp
	addl	%ecx, %ebp
	sarl	%ebp
	movl	%r15d, %esi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	sum_adjacent
	movl	%eax, %r12d
	cmpl	$3, %r14d
	ja	.LBB3_11
# BB#12:                                # %child.exit
	movl	%r14d, %eax
	jmpq	*.LJTI3_1(,%rax,8)
.LBB3_14:
	addq	$8, %rbx
	jmp	.LBB3_17
.LBB3_11:
	xorl	%edi, %edi
	jmp	.LBB3_18
.LBB3_13:
	addq	$16, %rbx
	jmp	.LBB3_17
.LBB3_16:
	addq	$24, %rbx
	jmp	.LBB3_17
.LBB3_15:
	addq	$32, %rbx
.LBB3_17:                               # %.sink.split.i13
	movq	(%rbx), %rdi
.LBB3_18:                               # %child.exit15
	movl	%r15d, %esi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	sum_adjacent
	addl	%r12d, %eax
.LBB3_21:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	sum_adjacent, .Lfunc_end3-sum_adjacent
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_6
	.quad	.LBB3_5
	.quad	.LBB3_8
	.quad	.LBB3_7
.LJTI3_1:
	.quad	.LBB3_14
	.quad	.LBB3_13
	.quad	.LBB3_16
	.quad	.LBB3_15

	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 16
.Lcfi38:
	.cfi_offset %rbx, -16
	callq	dealwithargs
	movl	%eax, %ebx
	movl	NumNodes(%rip), %edx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movl	NumNodes(%rip), %r8d
	decl	%r8d
	movl	$2097152, %edi          # imm = 0x200000
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	pushq	%rbx
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	$3
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	callq	MakeTree
	addq	$16, %rsp
.Lcfi41:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	CountTree
	movl	%eax, %ecx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	movl	$4096, %esi             # imm = 0x1000
	movq	%rbx, %rdi
	callq	perimeter
	movl	%eax, %ecx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	xorl	%edi, %edi
	callq	exit
.Lfunc_end4:
	.size	main, .Lfunc_end4-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Perimeter with %d levels on %d processors\n"
	.size	.L.str, 43

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"# of leaves is %d\n"
	.size	.L.str.1, 19

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"perimeter is %d\n"
	.size	.L.str.2, 17

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	2
.Lswitch.table:
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	2                       # 0x2
	.size	.Lswitch.table, 16

	.type	.Lswitch.table.1,@object # @switch.table.1
	.p2align	2
.Lswitch.table.1:
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	1                       # 0x1
	.size	.Lswitch.table.1, 16

	.type	.Lswitch.table.2,@object # @switch.table.2
	.p2align	2
.Lswitch.table.2:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	2                       # 0x2
	.size	.Lswitch.table.2, 16

	.type	.Lswitch.table.3,@object # @switch.table.3
	.p2align	2
.Lswitch.table.3:
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.size	.Lswitch.table.3, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
