	.text
	.file	"btSimulationIslandManager.bc"
	.globl	_ZN25btSimulationIslandManagerC2Ev
	.p2align	4, 0x90
	.type	_ZN25btSimulationIslandManagerC2Ev,@function
_ZN25btSimulationIslandManagerC2Ev:     # @_ZN25btSimulationIslandManagerC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV25btSimulationIslandManager+16, (%rbx)
	leaq	8(%rbx), %rdi
	callq	_ZN11btUnionFindC1Ev
	movb	$1, 64(%rbx)
	movq	$0, 56(%rbx)
	movl	$0, 44(%rbx)
	movl	$0, 48(%rbx)
	movb	$1, 96(%rbx)
	movq	$0, 88(%rbx)
	movl	$0, 76(%rbx)
	movl	$0, 80(%rbx)
	movb	$1, 104(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN25btSimulationIslandManagerC2Ev, .Lfunc_end0-_ZN25btSimulationIslandManagerC2Ev
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN25btSimulationIslandManagerD2Ev
	.p2align	4, 0x90
	.type	_ZN25btSimulationIslandManagerD2Ev,@function
_ZN25btSimulationIslandManagerD2Ev:     # @_ZN25btSimulationIslandManagerD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV25btSimulationIslandManager+16, (%rbx)
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#1:
	cmpb	$0, 96(%rbx)
	je	.LBB2_3
# BB#2:
.Ltmp0:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp1:
.LBB2_3:                                # %.noexc
	movq	$0, 88(%rbx)
.LBB2_4:
	movb	$1, 96(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 76(%rbx)
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_8
# BB#5:
	cmpb	$0, 64(%rbx)
	je	.LBB2_7
# BB#6:
.Ltmp5:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp6:
.LBB2_7:                                # %.noexc4
	movq	$0, 56(%rbx)
.LBB2_8:
	movb	$1, 64(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 44(%rbx)
	addq	$8, %rbx
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN11btUnionFindD1Ev    # TAILCALL
.LBB2_14:
.Ltmp7:
	movq	%rax, %r14
	jmp	.LBB2_15
.LBB2_9:
.Ltmp2:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_13
# BB#10:
	cmpb	$0, 64(%rbx)
	je	.LBB2_12
# BB#11:
.Ltmp3:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
.LBB2_12:                               # %.noexc6
	movq	$0, 56(%rbx)
.LBB2_13:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev.exit7
	movb	$1, 64(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 44(%rbx)
.LBB2_15:
	addq	$8, %rbx
.Ltmp8:
	movq	%rbx, %rdi
	callq	_ZN11btUnionFindD1Ev
.Ltmp9:
# BB#16:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_17:
.Ltmp10:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN25btSimulationIslandManagerD2Ev, .Lfunc_end2-_ZN25btSimulationIslandManagerD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp6           #   Call between .Ltmp6 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp9-.Ltmp3           #   Call between .Ltmp3 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	1                       #   On action: 1
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end2-.Ltmp9      #   Call between .Ltmp9 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN25btSimulationIslandManagerD0Ev
	.p2align	4, 0x90
	.type	_ZN25btSimulationIslandManagerD0Ev,@function
_ZN25btSimulationIslandManagerD0Ev:     # @_ZN25btSimulationIslandManagerD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp11:
	callq	_ZN25btSimulationIslandManagerD2Ev
.Ltmp12:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_2:
.Ltmp13:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN25btSimulationIslandManagerD0Ev, .Lfunc_end3-_ZN25btSimulationIslandManagerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end3-.Ltmp12     #   Call between .Ltmp12 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN25btSimulationIslandManager13initUnionFindEi
	.p2align	4, 0x90
	.type	_ZN25btSimulationIslandManager13initUnionFindEi,@function
_ZN25btSimulationIslandManager13initUnionFindEi: # @_ZN25btSimulationIslandManager13initUnionFindEi
	.cfi_startproc
# BB#0:
	addq	$8, %rdi
	jmp	_ZN11btUnionFind5resetEi # TAILCALL
.Lfunc_end4:
	.size	_ZN25btSimulationIslandManager13initUnionFindEi, .Lfunc_end4-_ZN25btSimulationIslandManager13initUnionFindEi
	.cfi_endproc

	.globl	_ZN25btSimulationIslandManager10findUnionsEP12btDispatcherP16btCollisionWorld
	.p2align	4, 0x90
	.type	_ZN25btSimulationIslandManager10findUnionsEP12btDispatcherP16btCollisionWorld,@function
_ZN25btSimulationIslandManager10findUnionsEP12btDispatcherP16btCollisionWorld: # @_ZN25btSimulationIslandManager10findUnionsEP12btDispatcherP16btCollisionWorld
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -40
.Lcfi18:
	.cfi_offset %r12, -32
.Lcfi19:
	.cfi_offset %r14, -24
.Lcfi20:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rdi, %r14
	movq	112(%r15), %rdi
	movq	(%rdi), %rax
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*72(%rcx)
	testl	%eax, %eax
	jle	.LBB5_15
# BB#1:                                 # %.lr.ph
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB5_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_8 Depth 2
                                        #     Child Loop BB5_11 Depth 2
	movq	112(%r15), %rdi
	movq	(%rdi), %rax
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*40(%rcx)
	movq	%r12, %rdx
	shlq	$5, %rdx
	movq	(%rax,%rdx), %rcx
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB5_14
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	8(%rax,%rdx), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB5_14
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=1
	movl	216(%rcx), %edx
	andl	$7, %edx
	jne	.LBB5_14
# BB#5:                                 #   in Loop: Header=BB5_2 Depth=1
	testb	$7, 216(%rax)
	jne	.LBB5_14
# BB#6:                                 #   in Loop: Header=BB5_2 Depth=1
	movl	220(%rcx), %esi
	movl	220(%rax), %r8d
	movslq	%r8d, %rcx
	movq	24(%r14), %rax
	movslq	%esi, %rdi
	movl	(%rax,%rdi,8), %ebx
	cmpl	%edi, %ebx
	je	.LBB5_9
# BB#7:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	leaq	(%rax,%rdi,8), %rdx
	.p2align	4, 0x90
.LBB5_8:                                # %.lr.ph.i.i
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebx, %rsi
	movl	(%rax,%rsi,8), %esi
	movl	%esi, (%rdx)
	movl	(%rax,%rdi,8), %esi
	movslq	%esi, %rdi
	leaq	(%rax,%rdi,8), %rdx
	movl	(%rax,%rdi,8), %ebx
	cmpl	%ebx, %esi
	jne	.LBB5_8
.LBB5_9:                                # %_ZN11btUnionFind4findEi.exit.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	(%rax,%rcx,8), %edi
	cmpl	%ecx, %edi
	je	.LBB5_12
# BB#10:                                # %.lr.ph.i11.i.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	leaq	(%rax,%rcx,8), %rdx
	.p2align	4, 0x90
.LBB5_11:                               # %.lr.ph.i11.i
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edi, %rdi
	movl	(%rax,%rdi,8), %edi
	movl	%edi, (%rdx)
	movl	(%rax,%rcx,8), %r8d
	movslq	%r8d, %rcx
	leaq	(%rax,%rcx,8), %rdx
	movl	(%rax,%rcx,8), %edi
	cmpl	%edi, %r8d
	jne	.LBB5_11
.LBB5_12:                               # %_ZN11btUnionFind4findEi.exit13.i
                                        #   in Loop: Header=BB5_2 Depth=1
	cmpl	%r8d, %esi
	je	.LBB5_14
# BB#13:                                #   in Loop: Header=BB5_2 Depth=1
	movslq	%esi, %rcx
	movl	%r8d, (%rax,%rcx,8)
	movl	4(%rax,%rcx,8), %ecx
	movslq	%r8d, %rdx
	addl	%ecx, 4(%rax,%rdx,8)
	.p2align	4, 0x90
.LBB5_14:                               # %_ZN11btUnionFind5uniteEii.exit
                                        #   in Loop: Header=BB5_2 Depth=1
	incq	%r12
	movq	112(%r15), %rdi
	movq	(%rdi), %rax
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*72(%rcx)
	cltq
	cmpq	%rax, %r12
	jl	.LBB5_2
.LBB5_15:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	_ZN25btSimulationIslandManager10findUnionsEP12btDispatcherP16btCollisionWorld, .Lfunc_end5-_ZN25btSimulationIslandManager10findUnionsEP12btDispatcherP16btCollisionWorld
	.cfi_endproc

	.globl	_ZN25btSimulationIslandManager21updateActivationStateEP16btCollisionWorldP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN25btSimulationIslandManager21updateActivationStateEP16btCollisionWorldP12btDispatcher,@function
_ZN25btSimulationIslandManager21updateActivationStateEP16btCollisionWorldP12btDispatcher: # @_ZN25btSimulationIslandManager21updateActivationStateEP16btCollisionWorldP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	12(%r15), %esi
	leaq	8(%r14), %rdi
	callq	_ZN11btUnionFind5resetEi
	movslq	12(%r15), %rax
	testq	%rax, %rax
	jle	.LBB6_7
# BB#1:                                 # %.lr.ph
	movq	24(%r15), %rcx
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	xorl	%edx, %edx
	andq	$3, %rsi
	je	.LBB6_2
	.p2align	4, 0x90
.LBB6_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rdi
	movl	%edx, 220(%rdi)
	movl	$-1, 224(%rdi)
	movl	$1065353216, 260(%rdi)  # imm = 0x3F800000
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB6_3
# BB#4:                                 # %.prol.loopexit.unr-lcssa
	movl	%edx, %esi
	cmpq	$3, %r8
	jae	.LBB6_6
	jmp	.LBB6_7
.LBB6_2:
	xorl	%esi, %esi
	cmpq	$3, %r8
	jb	.LBB6_7
	.p2align	4, 0x90
.LBB6_6:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rdi
	movl	%esi, 220(%rdi)
	movl	$-1, 224(%rdi)
	movl	$1065353216, 260(%rdi)  # imm = 0x3F800000
	movq	8(%rcx,%rdx,8), %rdi
	leal	1(%rsi), %ebx
	movl	%ebx, 220(%rdi)
	movl	$-1, 224(%rdi)
	movl	$1065353216, 260(%rdi)  # imm = 0x3F800000
	movq	16(%rcx,%rdx,8), %rdi
	leal	2(%rsi), %ebx
	movl	%ebx, 220(%rdi)
	movl	$-1, 224(%rdi)
	movl	$1065353216, 260(%rdi)  # imm = 0x3F800000
	movq	24(%rcx,%rdx,8), %rdi
	leal	3(%rsi), %ebx
	movl	%ebx, 220(%rdi)
	movl	$-1, 224(%rdi)
	movl	$1065353216, 260(%rdi)  # imm = 0x3F800000
	addq	$4, %rdx
	addl	$4, %esi
	cmpq	%rax, %rdx
	jl	.LBB6_6
.LBB6_7:                                # %._crit_edge
	movq	%r14, %rdi
	movq	%r15, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN25btSimulationIslandManager10findUnionsEP12btDispatcherP16btCollisionWorld # TAILCALL
.Lfunc_end6:
	.size	_ZN25btSimulationIslandManager21updateActivationStateEP16btCollisionWorldP12btDispatcher, .Lfunc_end6-_ZN25btSimulationIslandManager21updateActivationStateEP16btCollisionWorldP12btDispatcher
	.cfi_endproc

	.globl	_ZN25btSimulationIslandManager26storeIslandActivationStateEP16btCollisionWorld
	.p2align	4, 0x90
	.type	_ZN25btSimulationIslandManager26storeIslandActivationStateEP16btCollisionWorld,@function
_ZN25btSimulationIslandManager26storeIslandActivationStateEP16btCollisionWorld: # @_ZN25btSimulationIslandManager26storeIslandActivationStateEP16btCollisionWorld
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movslq	12(%rsi), %r9
	testq	%r9, %r9
	jle	.LBB7_9
# BB#1:                                 # %.lr.ph
	movq	24(%rsi), %r8
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_7 Depth 2
	movq	(%r8,%r14,8), %r11
	testb	$3, 216(%r11)
	je	.LBB7_4
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	$-2, %r10d
	movl	$-1, %esi
	jmp	.LBB7_8
	.p2align	4, 0x90
.LBB7_4:                                #   in Loop: Header=BB7_2 Depth=1
	movq	24(%rdi), %rax
	movl	(%rax,%r14,8), %ecx
	cmpq	%r14, %rcx
	movl	$-1, %r10d
	jne	.LBB7_6
# BB#5:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	%r14d, %esi
	jmp	.LBB7_8
.LBB7_6:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	leaq	(%rax,%r14,8), %rbx
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB7_7:                                # %.lr.ph.i
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ecx, %rcx
	movl	(%rax,%rcx,8), %ecx
	movl	%ecx, (%rbx)
	movl	(%rax,%rdx,8), %esi
	movslq	%esi, %rdx
	leaq	(%rax,%rdx,8), %rbx
	movl	(%rax,%rdx,8), %ecx
	cmpl	%ecx, %esi
	jne	.LBB7_7
	.p2align	4, 0x90
.LBB7_8:                                # %_ZN11btUnionFind4findEi.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	%esi, 220(%r11)
	movl	%r10d, 224(%r11)
	incq	%r14
	cmpq	%r9, %r14
	jl	.LBB7_2
.LBB7_9:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	_ZN25btSimulationIslandManager26storeIslandActivationStateEP16btCollisionWorld, .Lfunc_end7-_ZN25btSimulationIslandManager26storeIslandActivationStateEP16btCollisionWorld
	.cfi_endproc

	.globl	_ZN25btSimulationIslandManager12buildIslandsEP12btDispatcherP16btCollisionWorld
	.p2align	4, 0x90
	.type	_ZN25btSimulationIslandManager12buildIslandsEP12btDispatcherP16btCollisionWorld,@function
_ZN25btSimulationIslandManager12buildIslandsEP12btDispatcherP16btCollisionWorld: # @_ZN25btSimulationIslandManager12buildIslandsEP12btDispatcherP16btCollisionWorld
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 96
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movl	$.L.str, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	movq	%rbx, %rsi
	movslq	44(%rsi), %rbx
	testq	%rbx, %rbx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	jns	.LBB8_12
# BB#1:
	cmpl	$0, 48(%rsi)
	jns	.LBB8_7
# BB#2:                                 # %_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_.exit.i.i
	movq	56(%rsi), %rdi
	testq	%rdi, %rdi
	je	.LBB8_6
# BB#3:
	cmpb	$0, 64(%rsi)
	je	.LBB8_5
# BB#4:
.Ltmp14:
	callq	_Z21btAlignedFreeInternalPv
	movq	8(%rsp), %rsi           # 8-byte Reload
.Ltmp15:
.LBB8_5:                                # %.noexc
	movq	$0, 56(%rsi)
.LBB8_6:                                # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi.exit.preheader.i
	movb	$1, 64(%rsi)
	movq	$0, 56(%rsi)
	movl	$0, 48(%rsi)
.LBB8_7:                                # %.lr.ph.i
	movl	%ebx, %ecx
	negl	%ecx
	andq	$7, %rcx
	movq	%rbx, %rax
	je	.LBB8_10
# BB#8:                                 # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi.exit.i.prol.preheader
	negq	%rcx
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB8_9:                                # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%rsi), %rdx
	movq	$0, (%rdx,%rax,8)
	incq	%rax
	incq	%rcx
	jne	.LBB8_9
.LBB8_10:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi.exit.i.prol.loopexit
	cmpl	$-8, %ebx
	ja	.LBB8_12
	.p2align	4, 0x90
.LBB8_11:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%rsi), %rcx
	movq	$0, (%rcx,%rax,8)
	movq	56(%rsi), %rcx
	movq	$0, 8(%rcx,%rax,8)
	movq	56(%rsi), %rcx
	movq	$0, 16(%rcx,%rax,8)
	movq	56(%rsi), %rcx
	movq	$0, 24(%rcx,%rax,8)
	movq	56(%rsi), %rcx
	movq	$0, 32(%rcx,%rax,8)
	movq	56(%rsi), %rcx
	movq	$0, 40(%rcx,%rax,8)
	movq	56(%rsi), %rcx
	movq	$0, 48(%rcx,%rax,8)
	movq	56(%rsi), %rcx
	movq	$0, 56(%rcx,%rax,8)
	addq	$8, %rax
	jne	.LBB8_11
.LBB8_12:                               # %.loopexit162
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movl	$0, 44(%rsi)
	leaq	8(%rsi), %rdi
.Ltmp17:
	movq	%rsi, %rbx
	callq	_ZN11btUnionFind11sortIslandsEv
.Ltmp18:
# BB#13:
	movslq	12(%rbx), %rcx
	testq	%rcx, %rcx
	movq	%rbx, %r9
	movq	16(%rsp), %r10          # 8-byte Reload
	jle	.LBB8_43
# BB#14:                                # %.lr.ph173
	xorl	%r8d, %r8d
	movabsq	$4294967296, %r11       # imm = 0x100000000
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	jmp	.LBB8_16
	.p2align	4, 0x90
.LBB8_20:                               # %.lr.ph166
                                        #   in Loop: Header=BB8_16 Depth=1
	movq	24(%r10), %rdx
	sarq	$32, %r12
	leaq	-1(%r13), %rsi
	movb	$1, %bl
	.p2align	4, 0x90
.LBB8_21:                               #   Parent Loop BB8_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	12(%rax,%rsi,8), %rcx
	movq	(%rdx,%rcx,8), %rcx
	cmpl	%r15d, 220(%rcx)
	jne	.LBB8_27
# BB#22:                                #   in Loop: Header=BB8_21 Depth=2
	movl	228(%rcx), %ecx
	xorl	%edi, %edi
	cmpl	$1, %ecx
	movl	$0, %ebp
	je	.LBB8_24
# BB#23:                                #   in Loop: Header=BB8_21 Depth=2
	movb	%bl, %bpl
.LBB8_24:                               #   in Loop: Header=BB8_21 Depth=2
	cmpl	$4, %ecx
	je	.LBB8_26
# BB#25:                                #   in Loop: Header=BB8_21 Depth=2
	movb	%bpl, %dil
.LBB8_26:                               #   in Loop: Header=BB8_21 Depth=2
	movl	%edi, %ebx
.LBB8_27:                               # %.critedge
                                        #   in Loop: Header=BB8_21 Depth=2
	incq	%rsi
	cmpq	%r12, %rsi
	jl	.LBB8_21
# BB#28:                                # %.critedge._crit_edge
                                        #   in Loop: Header=BB8_16 Depth=1
	testb	$1, %bl
	jne	.LBB8_29
# BB#34:                                # %.preheader159
                                        #   in Loop: Header=BB8_16 Depth=1
	cmpl	%r14d, %r8d
	jle	.LBB8_35
	jmp	.LBB8_15
	.p2align	4, 0x90
.LBB8_40:                               # %..lr.ph170_crit_edge
                                        #   in Loop: Header=BB8_35 Depth=2
	incq	%r13
	movq	24(%r9), %rax
.LBB8_35:                               # %.lr.ph170
                                        #   Parent Loop BB8_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%r10), %rcx
	movslq	4(%rax,%r13,8), %rax
	movq	(%rcx,%rax,8), %rbx
	cmpl	%r15d, 220(%rbx)
	jne	.LBB8_39
# BB#36:                                #   in Loop: Header=BB8_35 Depth=2
	cmpl	$2, 228(%rbx)
	jne	.LBB8_39
# BB#37:                                #   in Loop: Header=BB8_35 Depth=2
.Ltmp23:
	movl	$3, %esi
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObject18setActivationStateEi
.Ltmp24:
# BB#38:                                #   in Loop: Header=BB8_35 Depth=2
	movl	$0, 232(%rbx)
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	movabsq	$4294967296, %r11       # imm = 0x100000000
.LBB8_39:                               #   in Loop: Header=BB8_35 Depth=2
	cmpq	%r12, %r13
	jl	.LBB8_40
	jmp	.LBB8_15
	.p2align	4, 0x90
.LBB8_29:                               # %.preheader160
                                        #   in Loop: Header=BB8_16 Depth=1
	cmpl	%r14d, %r8d
	jle	.LBB8_30
	jmp	.LBB8_15
	.p2align	4, 0x90
.LBB8_33:                               # %..lr.ph168_crit_edge
                                        #   in Loop: Header=BB8_30 Depth=2
	incq	%r13
	movq	24(%r9), %rax
.LBB8_30:                               # %.lr.ph168
                                        #   Parent Loop BB8_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%r10), %rcx
	movslq	4(%rax,%r13,8), %rax
	movq	(%rcx,%rax,8), %rdi
	cmpl	%r15d, 220(%rdi)
	jne	.LBB8_32
# BB#31:                                #   in Loop: Header=BB8_30 Depth=2
.Ltmp20:
	movl	$2, %esi
	callq	_ZN17btCollisionObject18setActivationStateEi
	movabsq	$4294967296, %r11       # imm = 0x100000000
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
.Ltmp21:
.LBB8_32:                               #   in Loop: Header=BB8_30 Depth=2
	cmpq	%r12, %r13
	jl	.LBB8_33
	jmp	.LBB8_15
	.p2align	4, 0x90
.LBB8_16:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_17 Depth 2
                                        #     Child Loop BB8_21 Depth 2
                                        #     Child Loop BB8_30 Depth 2
                                        #     Child Loop BB8_35 Depth 2
	movq	24(%r9), %rax
	movslq	%r8d, %r13
	movl	(%rax,%r13,8), %r15d
	movq	%r13, %rsi
	shlq	$32, %rsi
	leaq	1(%r13), %rdx
	movl	%r8d, %edi
	.p2align	4, 0x90
.LBB8_17:                               #   Parent Loop BB8_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %r14d
	movq	%rsi, %r12
	cmpq	%rcx, %rdx
	jge	.LBB8_19
# BB#18:                                #   in Loop: Header=BB8_17 Depth=2
	leal	1(%r14), %edi
	leaq	(%r12,%r11), %rsi
	cmpl	%r15d, (%rax,%rdx,8)
	leaq	1(%rdx), %rdx
	je	.LBB8_17
.LBB8_19:                               # %.critedge.preheader
                                        #   in Loop: Header=BB8_16 Depth=1
	cmpl	%r14d, %r8d
	jle	.LBB8_20
.LBB8_15:                               # %.backedge
                                        #   in Loop: Header=BB8_16 Depth=1
	incl	%r14d
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	%ecx, %r14d
	movl	%r14d, %r8d
	jl	.LBB8_16
.LBB8_43:                               # %._crit_edge174
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	(%r14), %rax
.Ltmp26:
	movq	%r14, %rdi
	callq	*72(%rax)
	movl	%eax, %r13d
.Ltmp27:
# BB#44:                                # %.preheader
	testl	%r13d, %r13d
	jle	.LBB8_82
# BB#45:                                # %.lr.ph
	xorl	%r15d, %r15d
	movl	%r13d, 16(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB8_46:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_71 Depth 2
                                        #     Child Loop BB8_73 Depth 2
	movq	(%r14), %rax
.Ltmp29:
	movq	%r14, %rdi
	movl	%r15d, %esi
	callq	*80(%rax)
	movq	%rax, %r12
.Ltmp30:
# BB#47:                                #   in Loop: Header=BB8_46 Depth=1
	movq	712(%r12), %rbx
	movq	720(%r12), %rbp
	testq	%rbx, %rbx
	movq	8(%rsp), %rcx           # 8-byte Reload
	je	.LBB8_49
# BB#48:                                #   in Loop: Header=BB8_46 Depth=1
	cmpl	$2, 228(%rbx)
	jne	.LBB8_51
.LBB8_49:                               #   in Loop: Header=BB8_46 Depth=1
	testq	%rbp, %rbp
	je	.LBB8_81
# BB#50:                                #   in Loop: Header=BB8_46 Depth=1
	cmpl	$2, 228(%rbp)
	je	.LBB8_81
.LBB8_51:                               #   in Loop: Header=BB8_46 Depth=1
	testb	$2, 216(%rbx)
	je	.LBB8_54
# BB#52:                                #   in Loop: Header=BB8_46 Depth=1
	cmpl	$2, 228(%rbx)
	je	.LBB8_54
# BB#53:                                #   in Loop: Header=BB8_46 Depth=1
.Ltmp32:
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	_ZN17btCollisionObject8activateEb
	movq	8(%rsp), %rcx           # 8-byte Reload
.Ltmp33:
.LBB8_54:                               #   in Loop: Header=BB8_46 Depth=1
	testb	$2, 216(%rbp)
	je	.LBB8_57
# BB#55:                                #   in Loop: Header=BB8_46 Depth=1
	cmpl	$2, 228(%rbp)
	je	.LBB8_57
# BB#56:                                #   in Loop: Header=BB8_46 Depth=1
.Ltmp34:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObject8activateEb
	movq	8(%rsp), %rcx           # 8-byte Reload
.Ltmp35:
.LBB8_57:                               #   in Loop: Header=BB8_46 Depth=1
	cmpb	$0, 104(%rcx)
	movl	16(%rsp), %r13d         # 4-byte Reload
	je	.LBB8_81
# BB#58:                                #   in Loop: Header=BB8_46 Depth=1
	movq	(%r14), %rax
.Ltmp36:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	*56(%rax)
	movq	8(%rsp), %rbx           # 8-byte Reload
.Ltmp37:
# BB#59:                                #   in Loop: Header=BB8_46 Depth=1
	testb	%al, %al
	movl	16(%rsp), %r13d         # 4-byte Reload
	je	.LBB8_81
# BB#60:                                #   in Loop: Header=BB8_46 Depth=1
	movl	44(%rbx), %eax
	cmpl	48(%rbx), %eax
	jne	.LBB8_80
# BB#61:                                #   in Loop: Header=BB8_46 Depth=1
	leal	(%rax,%rax), %ebp
	testl	%eax, %eax
	movl	$1, %ecx
	cmovel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB8_80
# BB#62:                                #   in Loop: Header=BB8_46 Depth=1
	testl	%ebp, %ebp
	je	.LBB8_63
# BB#67:                                #   in Loop: Header=BB8_46 Depth=1
	movslq	%ebp, %rdi
	shlq	$3, %rdi
.Ltmp38:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rax, %r13
.Ltmp39:
# BB#68:                                # %.noexc150
                                        #   in Loop: Header=BB8_46 Depth=1
	movl	44(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB8_70
	jmp	.LBB8_74
.LBB8_63:                               #   in Loop: Header=BB8_46 Depth=1
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jle	.LBB8_74
.LBB8_70:                               # %.lr.ph.i.i.i143
                                        #   in Loop: Header=BB8_46 Depth=1
	movl	%ebp, %r8d
	movslq	%eax, %rcx
	leaq	-1(%rcx), %rsi
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB8_72
	.p2align	4, 0x90
.LBB8_71:                               #   Parent Loop BB8_46 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%rbx), %rbp
	movq	(%rbp,%rdx,8), %rbp
	movq	%rbp, (%r13,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB8_71
.LBB8_72:                               # %.prol.loopexit
                                        #   in Loop: Header=BB8_46 Depth=1
	cmpq	$3, %rsi
	movl	%r8d, %ebp
	jb	.LBB8_74
	.p2align	4, 0x90
.LBB8_73:                               #   Parent Loop BB8_46 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r13,%rdx,8)
	movq	56(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r13,%rdx,8)
	movq	56(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r13,%rdx,8)
	movq	56(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r13,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB8_73
.LBB8_74:                               # %_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_.exit.i.i148
                                        #   in Loop: Header=BB8_46 Depth=1
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_79
# BB#75:                                #   in Loop: Header=BB8_46 Depth=1
	cmpb	$0, 64(%rbx)
	je	.LBB8_78
# BB#76:                                #   in Loop: Header=BB8_46 Depth=1
.Ltmp40:
	callq	_Z21btAlignedFreeInternalPv
	movq	8(%rsp), %rbx           # 8-byte Reload
.Ltmp41:
# BB#77:                                # %.noexc151
                                        #   in Loop: Header=BB8_46 Depth=1
	movl	44(%rbx), %eax
.LBB8_78:                               #   in Loop: Header=BB8_46 Depth=1
	movq	$0, 56(%rbx)
.LBB8_79:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv.exit.i.i
                                        #   in Loop: Header=BB8_46 Depth=1
	movb	$1, 64(%rbx)
	movq	%r13, 56(%rbx)
	movl	%ebp, 48(%rbx)
	movl	16(%rsp), %r13d         # 4-byte Reload
.LBB8_80:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_.exit
                                        #   in Loop: Header=BB8_46 Depth=1
	movq	56(%rbx), %rcx
	cltq
	movq	%r12, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 44(%rbx)
	.p2align	4, 0x90
.LBB8_81:                               #   in Loop: Header=BB8_46 Depth=1
	incl	%r15d
	cmpl	%r13d, %r15d
	jl	.LBB8_46
.LBB8_82:                               # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN15CProfileManager12Stop_ProfileEv # TAILCALL
.LBB8_83:
.Ltmp16:
	jmp	.LBB8_84
.LBB8_64:
.Ltmp28:
	jmp	.LBB8_84
.LBB8_87:
.Ltmp19:
	jmp	.LBB8_84
.LBB8_66:
.Ltmp42:
	jmp	.LBB8_84
.LBB8_65:
.Ltmp31:
	jmp	.LBB8_84
.LBB8_42:
.Ltmp25:
	jmp	.LBB8_84
.LBB8_41:
.Ltmp22:
.LBB8_84:
	movq	%rax, %rbx
.Ltmp43:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp44:
# BB#85:                                # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB8_86:
.Ltmp45:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN25btSimulationIslandManager12buildIslandsEP12btDispatcherP16btCollisionWorld, .Lfunc_end8-_ZN25btSimulationIslandManager12buildIslandsEP12btDispatcherP16btCollisionWorld
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp14-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin2   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin2   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin2   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin2   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin2   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin2   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp41-.Ltmp32         #   Call between .Ltmp32 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin2   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp43-.Ltmp41         #   Call between .Ltmp41 and .Ltmp43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin2   #     jumps to .Ltmp45
	.byte	1                       #   On action: 1
	.long	.Ltmp44-.Lfunc_begin2   # >> Call Site 11 <<
	.long	.Lfunc_end8-.Ltmp44     #   Call between .Ltmp44 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN25btSimulationIslandManager22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldPNS_14IslandCallbackE
	.p2align	4, 0x90
	.type	_ZN25btSimulationIslandManager22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldPNS_14IslandCallbackE,@function
_ZN25btSimulationIslandManager22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldPNS_14IslandCallbackE: # @_ZN25btSimulationIslandManager22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldPNS_14IslandCallbackE
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi50:
	.cfi_def_cfa_offset 112
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	%rdx, (%rsp)            # 8-byte Spill
	callq	_ZN25btSimulationIslandManager12buildIslandsEP12btDispatcherP16btCollisionWorld
	movslq	12(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$.L.str.1, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	cmpb	$0, 104(%rbx)
	je	.LBB9_1
# BB#6:
	movslq	44(%rbx), %r14
	cmpq	$2, %r14
	jl	.LBB9_8
# BB#7:
	leaq	40(%rbx), %rdi
	leal	-1(%r14), %edx
.Ltmp46:
	xorl	%esi, %esi
	callq	_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvT_ii
.Ltmp47:
.LBB9_8:                                # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9quickSortI33btPersistentManifoldSortPredicateEEvT_.exit.preheader
	movq	8(%rsp), %r10           # 8-byte Reload
	testl	%r10d, %r10d
	movq	(%rsp), %rdi            # 8-byte Reload
	jle	.LBB9_61
# BB#9:                                 # %.lr.ph139
	xorl	%r15d, %r15d
	movl	$1, 24(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r14, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB9_10:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_12 Depth 2
                                        #       Child Loop BB9_20 Depth 3
                                        #       Child Loop BB9_22 Depth 3
                                        #     Child Loop BB9_43 Depth 2
                                        #     Child Loop BB9_57 Depth 2
                                        #     Child Loop BB9_59 Depth 2
	movq	24(%rbx), %rax
	movslq	%r15d, %r14
	movl	(%rax,%r14,8), %r9d
	xorl	%ebp, %ebp
	xorl	%r11d, %r11d
	cmpl	%r10d, %r14d
	jge	.LBB9_36
# BB#11:                                # %.lr.ph.preheader.preheader
                                        #   in Loop: Header=BB9_10 Depth=1
	movl	%r9d, 28(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB9_12:                               # %.lr.ph.preheader
                                        #   Parent Loop BB9_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_20 Depth 3
                                        #       Child Loop BB9_22 Depth 3
	movq	24(%rdi), %rcx
	movslq	4(%rax,%r14,8), %rax
	movq	(%rcx,%rax,8), %r12
	movl	76(%rbx), %eax
	cmpl	80(%rbx), %eax
	jne	.LBB9_29
# BB#13:                                #   in Loop: Header=BB9_12 Depth=2
	leal	(%rax,%rax), %r13d
	testl	%eax, %eax
	movl	$1, %ecx
	cmovel	%ecx, %r13d
	cmpl	%r13d, %eax
	jge	.LBB9_29
# BB#14:                                #   in Loop: Header=BB9_12 Depth=2
	testl	%r13d, %r13d
	movq	%r11, 40(%rsp)          # 8-byte Spill
	je	.LBB9_15
# BB#16:                                #   in Loop: Header=BB9_12 Depth=2
	movslq	%r13d, %rdi
	shlq	$3, %rdi
.Ltmp49:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
.Ltmp50:
# BB#17:                                # %.noexc117
                                        #   in Loop: Header=BB9_12 Depth=2
	movl	76(%rbx), %eax
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	40(%rsp), %r11          # 8-byte Reload
	testl	%eax, %eax
	jg	.LBB9_19
	jmp	.LBB9_23
.LBB9_15:                               #   in Loop: Header=BB9_12 Depth=2
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jle	.LBB9_23
.LBB9_19:                               # %.lr.ph.i.i.i110
                                        #   in Loop: Header=BB9_12 Depth=2
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB9_21
	.p2align	4, 0x90
.LBB9_20:                               #   Parent Loop BB9_10 Depth=1
                                        #     Parent Loop BB9_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	88(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbp,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB9_20
.LBB9_21:                               # %.prol.loopexit
                                        #   in Loop: Header=BB9_12 Depth=2
	cmpq	$3, %r8
	jb	.LBB9_23
	.p2align	4, 0x90
.LBB9_22:                               #   Parent Loop BB9_10 Depth=1
                                        #     Parent Loop BB9_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	88(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbp,%rdx,8)
	movq	88(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%rbp,%rdx,8)
	movq	88(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%rbp,%rdx,8)
	movq	88(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%rbp,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB9_22
.LBB9_23:                               # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_.exit.i.i115
                                        #   in Loop: Header=BB9_12 Depth=2
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_28
# BB#24:                                #   in Loop: Header=BB9_12 Depth=2
	cmpb	$0, 96(%rbx)
	je	.LBB9_27
# BB#25:                                #   in Loop: Header=BB9_12 Depth=2
.Ltmp51:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp52:
# BB#26:                                # %.noexc118
                                        #   in Loop: Header=BB9_12 Depth=2
	movl	76(%rbx), %eax
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	40(%rsp), %r11          # 8-byte Reload
.LBB9_27:                               #   in Loop: Header=BB9_12 Depth=2
	movq	$0, 88(%rbx)
.LBB9_28:                               # %_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv.exit.i.i
                                        #   in Loop: Header=BB9_12 Depth=2
	movb	$1, 96(%rbx)
	movq	%rbp, 88(%rbx)
	movl	%r13d, 80(%rbx)
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	28(%rsp), %r9d          # 4-byte Reload
	xorl	%ebp, %ebp
.LBB9_29:                               #   in Loop: Header=BB9_12 Depth=2
	movq	88(%rbx), %rcx
	cltq
	movq	%r12, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 76(%rbx)
	movl	228(%r12), %eax
	cmpl	$2, %eax
	jne	.LBB9_31
# BB#30:                                #   in Loop: Header=BB9_12 Depth=2
	movb	$1, %r11b
.LBB9_31:                               #   in Loop: Header=BB9_12 Depth=2
	cmpl	$5, %eax
	jne	.LBB9_33
# BB#32:                                #   in Loop: Header=BB9_12 Depth=2
	movb	$1, %r11b
.LBB9_33:                               #   in Loop: Header=BB9_12 Depth=2
	leaq	1(%r14), %rcx
	incl	%r15d
	cmpq	%r10, %rcx
	jge	.LBB9_36
# BB#34:                                # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB9_12 Depth=2
	movq	24(%rbx), %rax
	cmpl	%r9d, 8(%rax,%r14,8)
	movq	%rcx, %r14
	je	.LBB9_12
# BB#35:                                #   in Loop: Header=BB9_10 Depth=1
	movl	%ecx, %r15d
.LBB9_36:                               # %.critedge
                                        #   in Loop: Header=BB9_10 Depth=1
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	%r14d, %eax
	jge	.LBB9_37
# BB#40:                                #   in Loop: Header=BB9_10 Depth=1
	movq	%rax, %rcx
	movq	56(%rbx), %rax
	movslq	%ecx, %rsi
	movq	(%rax,%rsi,8), %rcx
	movq	712(%rcx), %rdx
	cmpl	$0, 220(%rdx)
	cmovsq	720(%rcx), %rdx
	cmpl	%r9d, 220(%rdx)
	jne	.LBB9_41
# BB#42:                                # %.preheader.preheader
                                        #   in Loop: Header=BB9_10 Depth=1
	leaq	(%rax,%rsi,8), %rcx
	movl	$1, %edi
	.p2align	4, 0x90
.LBB9_43:                               # %.preheader
                                        #   Parent Loop BB9_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, %rbp
	leaq	(%rsi,%rbp), %rax
	cmpq	%r14, %rax
	jge	.LBB9_45
# BB#44:                                #   in Loop: Header=BB9_43 Depth=2
	movq	(%rcx,%rbp,8), %rax
	movq	712(%rax), %rdx
	cmpl	$0, 220(%rdx)
	cmovsq	720(%rax), %rdx
	leaq	1(%rbp), %rdi
	cmpl	220(%rdx), %r9d
	je	.LBB9_43
.LBB9_45:                               # %.critedge1
                                        #   in Loop: Header=BB9_10 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rbp), %edx
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movq	(%rsp), %rdi            # 8-byte Reload
	testb	$1, %r11b
	je	.LBB9_47
	jmp	.LBB9_48
	.p2align	4, 0x90
.LBB9_37:                               #   in Loop: Header=BB9_10 Depth=1
	xorl	%ecx, %ecx
	testb	$1, %r11b
	je	.LBB9_47
	jmp	.LBB9_48
	.p2align	4, 0x90
.LBB9_41:                               #   in Loop: Header=BB9_10 Depth=1
	xorl	%ebp, %ebp
	xorl	%ecx, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	testb	$1, %r11b
	jne	.LBB9_48
.LBB9_47:                               #   in Loop: Header=BB9_10 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	88(%rbx), %rsi
	movl	76(%rbx), %edx
.Ltmp54:
	movl	%ebp, %r8d
	callq	*16(%rax)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
.Ltmp55:
.LBB9_48:                               #   in Loop: Header=BB9_10 Depth=1
	testl	%ebp, %ebp
	cmovnel	24(%rsp), %eax          # 4-byte Folded Reload
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movslq	76(%rbx), %rbp
	testq	%rbp, %rbp
	jns	.LBB9_60
# BB#49:                                #   in Loop: Header=BB9_10 Depth=1
	cmpl	$0, 80(%rbx)
	jns	.LBB9_55
# BB#50:                                # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_.exit.i.i
                                        #   in Loop: Header=BB9_10 Depth=1
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_54
# BB#51:                                #   in Loop: Header=BB9_10 Depth=1
	cmpb	$0, 96(%rbx)
	je	.LBB9_53
# BB#52:                                #   in Loop: Header=BB9_10 Depth=1
.Ltmp57:
	callq	_Z21btAlignedFreeInternalPv
	movq	8(%rsp), %r10           # 8-byte Reload
.Ltmp58:
.LBB9_53:                               # %.noexc
                                        #   in Loop: Header=BB9_10 Depth=1
	movq	$0, 88(%rbx)
.LBB9_54:                               # %_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi.exit.preheader.i
                                        #   in Loop: Header=BB9_10 Depth=1
	movb	$1, 96(%rbx)
	movq	$0, 88(%rbx)
	movl	$0, 80(%rbx)
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB9_55:                               # %.lr.ph.i
                                        #   in Loop: Header=BB9_10 Depth=1
	movl	%ebp, %ecx
	negl	%ecx
	andq	$7, %rcx
	movq	%rbp, %rax
	je	.LBB9_58
# BB#56:                                # %_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi.exit.i.prol.preheader
                                        #   in Loop: Header=BB9_10 Depth=1
	negq	%rcx
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB9_57:                               # %_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi.exit.i.prol
                                        #   Parent Loop BB9_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	88(%rbx), %rdx
	movq	$0, (%rdx,%rax,8)
	incq	%rax
	incq	%rcx
	jne	.LBB9_57
.LBB9_58:                               # %_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi.exit.i.prol.loopexit
                                        #   in Loop: Header=BB9_10 Depth=1
	cmpl	$-8, %ebp
	ja	.LBB9_60
	.p2align	4, 0x90
.LBB9_59:                               # %_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi.exit.i
                                        #   Parent Loop BB9_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	88(%rbx), %rcx
	movq	$0, (%rcx,%rax,8)
	movq	88(%rbx), %rcx
	movq	$0, 8(%rcx,%rax,8)
	movq	88(%rbx), %rcx
	movq	$0, 16(%rcx,%rax,8)
	movq	88(%rbx), %rcx
	movq	$0, 24(%rcx,%rax,8)
	movq	88(%rbx), %rcx
	movq	$0, 32(%rcx,%rax,8)
	movq	88(%rbx), %rcx
	movq	$0, 40(%rcx,%rax,8)
	movq	88(%rbx), %rcx
	movq	$0, 48(%rcx,%rax,8)
	movq	88(%rbx), %rcx
	movq	$0, 56(%rcx,%rax,8)
	addq	$8, %rax
	jne	.LBB9_59
	.p2align	4, 0x90
.LBB9_60:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9quickSortI33btPersistentManifoldSortPredicateEEvT_.exit
                                        #   in Loop: Header=BB9_10 Depth=1
	movl	$0, 76(%rbx)
	cmpl	%r10d, %r15d
	jl	.LBB9_10
	jmp	.LBB9_61
.LBB9_1:
	movq	(%rbp), %rax
.Ltmp60:
	movq	%rbp, %rdi
	callq	*88(%rax)
	movq	%rax, %rbx
.Ltmp61:
# BB#2:
	movq	(%rbp), %rax
.Ltmp63:
	movq	%rbp, %rdi
	callq	*72(%rax)
.Ltmp64:
# BB#3:
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbp
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	24(%rcx), %rsi
	movl	12(%rcx), %edx
.Ltmp66:
	movl	$-1, %r9d
	movq	%rbx, %rcx
	movl	%eax, %r8d
	callq	*16(%rbp)
.Ltmp67:
.LBB9_61:                               # %.loopexit127
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN15CProfileManager12Stop_ProfileEv # TAILCALL
.LBB9_38:
.Ltmp48:
	jmp	.LBB9_63
.LBB9_5:
.Ltmp68:
	jmp	.LBB9_63
.LBB9_4:
.Ltmp65:
	jmp	.LBB9_63
.LBB9_62:
.Ltmp62:
	jmp	.LBB9_63
.LBB9_67:
.Ltmp59:
	jmp	.LBB9_63
.LBB9_66:
.Ltmp56:
	jmp	.LBB9_63
.LBB9_39:
.Ltmp53:
.LBB9_63:
	movq	%rax, %rbx
.Ltmp69:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp70:
# BB#64:                                # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB9_65:
.Ltmp71:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN25btSimulationIslandManager22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldPNS_14IslandCallbackE, .Lfunc_end9-_ZN25btSimulationIslandManager22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldPNS_14IslandCallbackE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp46-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin3   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp52-.Ltmp49         #   Call between .Ltmp49 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin3   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin3   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin3   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin3   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin3   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin3   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp69-.Ltmp67         #   Call between .Ltmp67 and .Ltmp69
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin3   #     jumps to .Ltmp71
	.byte	1                       #   On action: 1
	.long	.Ltmp70-.Lfunc_begin3   # >> Call Site 11 <<
	.long	.Lfunc_end9-.Ltmp70     #   Call between .Ltmp70 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvT_ii,"axG",@progbits,_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvT_ii,comdat
	.weak	_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvT_ii
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvT_ii,@function
_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvT_ii: # @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvT_ii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 48
.Lcfi62:
	.cfi_offset %rbx, -48
.Lcfi63:
	.cfi_offset %r12, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r12d
	movq	%rdi, %r15
	.p2align	4, 0x90
.LBB10_1:                               # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_2 Depth 2
                                        #       Child Loop BB10_3 Depth 3
                                        #       Child Loop BB10_5 Depth 3
	movl	%r12d, %esi
	movq	16(%r15), %r9
	leal	(%rsi,%r14), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	movq	(%r9,%rax,8), %r8
	movl	%r14d, %r10d
	jmp	.LBB10_2
	.p2align	4, 0x90
.LBB10_9:                               # %._crit_edge
                                        #   in Loop: Header=BB10_2 Depth=2
	movq	16(%r15), %r9
	movl	%edx, %r10d
.LBB10_2:                               #   Parent Loop BB10_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_3 Depth 3
                                        #       Child Loop BB10_5 Depth 3
	movq	712(%r8), %rax
	cmpl	$0, 220(%rax)
	cmovsq	720(%r8), %rax
	movl	220(%rax), %r11d
	movslq	%r12d, %rax
	leaq	(%r9,%rax,8), %rax
	movl	%r12d, %ebx
	.p2align	4, 0x90
.LBB10_3:                               #   Parent Loop BB10_1 Depth=1
                                        #     Parent Loop BB10_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rbp
	movq	712(%rbp), %rcx
	cmpl	$0, 220(%rcx)
	cmovsq	720(%rbp), %rcx
	incl	%ebx
	addq	$8, %rax
	cmpl	%r11d, 220(%rcx)
	jl	.LBB10_3
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB10_2 Depth=2
	movslq	%r10d, %rcx
	shlq	$3, %rcx
	.p2align	4, 0x90
.LBB10_5:                               #   Parent Loop BB10_1 Depth=1
                                        #     Parent Loop BB10_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r9,%rcx), %rdi
	movq	712(%rdi), %rdx
	cmpl	$0, 220(%rdx)
	cmovsq	720(%rdi), %rdx
	addq	$-8, %rcx
	decl	%r10d
	cmpl	220(%rdx), %r11d
	jl	.LBB10_5
# BB#6:                                 #   in Loop: Header=BB10_2 Depth=2
	leal	-1(%rbx), %r12d
	leal	1(%r10), %edx
	cmpl	%edx, %r12d
	jg	.LBB10_8
# BB#7:                                 #   in Loop: Header=BB10_2 Depth=2
	movq	%rdi, -8(%rax)
	movq	16(%r15), %rax
	movq	%rbp, 8(%rax,%rcx)
	movl	%r10d, %edx
	movl	%ebx, %r12d
.LBB10_8:                               #   in Loop: Header=BB10_2 Depth=2
	cmpl	%edx, %r12d
	jle	.LBB10_9
# BB#10:                                #   in Loop: Header=BB10_1 Depth=1
	cmpl	%esi, %edx
	jle	.LBB10_12
# BB#11:                                #   in Loop: Header=BB10_1 Depth=1
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvT_ii
.LBB10_12:                              #   in Loop: Header=BB10_1 Depth=1
	cmpl	%r14d, %r12d
	jl	.LBB10_1
# BB#13:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvT_ii, .Lfunc_end10-_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvT_ii
	.cfi_endproc

	.type	_ZTV25btSimulationIslandManager,@object # @_ZTV25btSimulationIslandManager
	.section	.rodata,"a",@progbits
	.globl	_ZTV25btSimulationIslandManager
	.p2align	3
_ZTV25btSimulationIslandManager:
	.quad	0
	.quad	_ZTI25btSimulationIslandManager
	.quad	_ZN25btSimulationIslandManagerD2Ev
	.quad	_ZN25btSimulationIslandManagerD0Ev
	.quad	_ZN25btSimulationIslandManager21updateActivationStateEP16btCollisionWorldP12btDispatcher
	.quad	_ZN25btSimulationIslandManager26storeIslandActivationStateEP16btCollisionWorld
	.size	_ZTV25btSimulationIslandManager, 48

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"islandUnionFindAndQuickSort"
	.size	.L.str, 28

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"processIslands"
	.size	.L.str.1, 15

	.type	_ZTS25btSimulationIslandManager,@object # @_ZTS25btSimulationIslandManager
	.section	.rodata,"a",@progbits
	.globl	_ZTS25btSimulationIslandManager
	.p2align	4
_ZTS25btSimulationIslandManager:
	.asciz	"25btSimulationIslandManager"
	.size	_ZTS25btSimulationIslandManager, 28

	.type	_ZTI25btSimulationIslandManager,@object # @_ZTI25btSimulationIslandManager
	.globl	_ZTI25btSimulationIslandManager
	.p2align	3
_ZTI25btSimulationIslandManager:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS25btSimulationIslandManager
	.size	_ZTI25btSimulationIslandManager, 16


	.globl	_ZN25btSimulationIslandManagerC1Ev
	.type	_ZN25btSimulationIslandManagerC1Ev,@function
_ZN25btSimulationIslandManagerC1Ev = _ZN25btSimulationIslandManagerC2Ev
	.globl	_ZN25btSimulationIslandManagerD1Ev
	.type	_ZN25btSimulationIslandManagerD1Ev,@function
_ZN25btSimulationIslandManagerD1Ev = _ZN25btSimulationIslandManagerD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
