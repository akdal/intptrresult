	.text
	.file	"outputQuery.bc"
	.globl	outputQuery
	.p2align	4, 0x90
	.type	outputQuery,@function
outputQuery:                            # @outputQuery
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	(%r15), %eax
	decl	%eax
	cmpl	$2, %eax
	ja	.LBB0_1
# BB#2:                                 # %switch.lookup
	cltq
	movq	.Lswitch.table(,%rax,8), %r14
	jmp	.LBB0_3
.LBB0_1:
	xorl	%r14d, %r14d
.LBB0_3:                                # %.fold.split
	movb	$0, outputQuery.dataObjectString(%rip)
	movq	8(%r15), %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$outputQuery.temp, %edi
	movl	$.L.str, %esi
	movb	$1, %al
	callq	sprintf
	movl	$outputQuery.dataObjectString, %edi
	movl	$outputQuery.temp, %esi
	callq	strcat
	movl	$outputQuery.dataObjectString, %edi
	callq	strlen
	movw	$32, outputQuery.dataObjectString(%rax)
	movq	8(%r15), %rax
	movss	8(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$outputQuery.temp, %edi
	movl	$.L.str, %esi
	movb	$1, %al
	callq	sprintf
	movl	$outputQuery.dataObjectString, %edi
	movl	$outputQuery.temp, %esi
	callq	strcat
	movl	$outputQuery.dataObjectString, %edi
	callq	strlen
	movw	$32, outputQuery.dataObjectString(%rax)
	movq	8(%r15), %rax
	movss	16(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$outputQuery.temp, %edi
	movl	$.L.str, %esi
	movb	$1, %al
	callq	sprintf
	movl	$outputQuery.dataObjectString, %edi
	movl	$outputQuery.temp, %esi
	callq	strcat
	movl	$outputQuery.dataObjectString, %edi
	callq	strlen
	movw	$32, outputQuery.dataObjectString(%rax)
	movq	8(%r15), %rax
	movss	24(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$outputQuery.temp, %edi
	movl	$.L.str, %esi
	movb	$1, %al
	callq	sprintf
	movl	$outputQuery.dataObjectString, %edi
	movl	$outputQuery.temp, %esi
	callq	strcat
	movl	$outputQuery.dataObjectString, %edi
	callq	strlen
	movw	$32, outputQuery.dataObjectString(%rax)
	movq	8(%r15), %rax
	movss	32(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$outputQuery.temp, %edi
	movl	$.L.str, %esi
	movb	$1, %al
	callq	sprintf
	movl	$outputQuery.dataObjectString, %edi
	movl	$outputQuery.temp, %esi
	callq	strcat
	movl	$outputQuery.dataObjectString, %edi
	callq	strlen
	movw	$32, outputQuery.dataObjectString(%rax)
	movq	8(%r15), %rax
	movss	40(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$outputQuery.temp, %edi
	movl	$.L.str, %esi
	movb	$1, %al
	callq	sprintf
	movl	$outputQuery.dataObjectString, %edi
	movl	$outputQuery.temp, %esi
	callq	strcat
	movl	$outputQuery.dataObjectString, %edi
	callq	strlen
	movw	$32, outputQuery.dataObjectString(%rax)
	movq	8(%r15), %rax
	movss	48(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$outputQuery.temp, %edi
	movl	$.L.str, %esi
	movb	$1, %al
	callq	sprintf
	movl	$outputQuery.dataObjectString, %edi
	movl	$outputQuery.temp, %esi
	callq	strcat
	movl	$outputQuery.dataObjectString, %edi
	callq	strlen
	movw	$32, outputQuery.dataObjectString(%rax)
	movq	8(%r15), %rax
	movss	56(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$outputQuery.temp, %edi
	movl	$.L.str, %esi
	movb	$1, %al
	callq	sprintf
	movl	$outputQuery.dataObjectString, %edi
	movl	$outputQuery.temp, %esi
	callq	strcat
	movl	$outputQuery.dataObjectString, %edi
	callq	strlen
	movw	$32, outputQuery.dataObjectString(%rax)
	movq	8(%r15), %rax
	cmpq	$9, %r14
	jl	.LBB0_6
# BB#4:                                 # %.lr.ph.preheader
	movl	$8, %ebx
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rbx,8), %rsi
	movl	$outputQuery.dataObjectString, %edi
	callq	strcat
	movl	$outputQuery.dataObjectString, %edi
	callq	strlen
	movw	$32, outputQuery.dataObjectString(%rax)
	incq	%rbx
	movq	8(%r15), %rax
	cmpq	%rbx, %r14
	jne	.LBB0_5
.LBB0_6:                                # %._crit_edge
	movq	(%rax,%r14,8), %rsi
	movl	$outputQuery.dataObjectString, %edi
	callq	strcat
	movl	$outputQuery.dataObjectString, %edi
	callq	strlen
	movw	$10, outputQuery.dataObjectString(%rax)
	movl	$outputQuery.dataObjectString, %edi
	callq	strlen
	movq	%rax, %rbx
	movl	$outputBuffer, %edi
	callq	strlen
	addq	%rax, %rbx
	cmpq	$88967, %rbx            # imm = 0x15B87
	jb	.LBB0_10
# BB#7:
	testq	%rax, %rax
	je	.LBB0_9
# BB#8:
	movq	outputFile(%rip), %rsi
	movl	$outputBuffer, %edi
	callq	fputs
	movq	outputFile(%rip), %rdi
	callq	fflush
.LBB0_9:                                # %flushOutputBuffer.exit
	movb	$0, outputBuffer(%rip)
.LBB0_10:
	movl	$outputBuffer, %edi
	movl	$outputQuery.dataObjectString, %esi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	strcat                  # TAILCALL
.Lfunc_end0:
	.size	outputQuery, .Lfunc_end0-outputQuery
	.cfi_endproc

	.globl	flushOutputBuffer
	.p2align	4, 0x90
	.type	flushOutputBuffer,@function
flushOutputBuffer:                      # @flushOutputBuffer
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 16
	movl	$outputBuffer, %edi
	callq	strlen
	testq	%rax, %rax
	je	.LBB1_2
# BB#1:
	movq	outputFile(%rip), %rsi
	movl	$outputBuffer, %edi
	callq	fputs
	movq	outputFile(%rip), %rdi
	callq	fflush
.LBB1_2:
	movb	$0, outputBuffer(%rip)
	popq	%rax
	retq
.Lfunc_end1:
	.size	flushOutputBuffer, .Lfunc_end1-flushOutputBuffer
	.cfi_endproc

	.globl	initOutputBuffer
	.p2align	4, 0x90
	.type	initOutputBuffer,@function
initOutputBuffer:                       # @initOutputBuffer
	.cfi_startproc
# BB#0:
	movq	%rdi, outputFile(%rip)
	retq
.Lfunc_end2:
	.size	initOutputBuffer, .Lfunc_end2-initOutputBuffer
	.cfi_endproc

	.type	outputQuery.temp,@object # @outputQuery.temp
	.local	outputQuery.temp
	.comm	outputQuery.temp,51,16
	.type	outputQuery.dataObjectString,@object # @outputQuery.dataObjectString
	.local	outputQuery.dataObjectString
	.comm	outputQuery.dataObjectString,44484,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%.8e"
	.size	.L.str, 5

	.type	outputBuffer,@object    # @outputBuffer
	.local	outputBuffer
	.comm	outputBuffer,88967,16
	.type	outputFile,@object      # @outputFile
	.local	outputFile
	.comm	outputFile,8,8
	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	17                      # 0x11
	.quad	24                      # 0x18
	.quad	50                      # 0x32
	.size	.Lswitch.table, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
