	.text
	.file	"fp-convert.bc"
	.globl	loop
	.p2align	4, 0x90
	.type	loop,@function
loop:                                   # @loop
	.cfi_startproc
# BB#0:
	testq	%rdx, %rdx
	jle	.LBB0_1
# BB#2:                                 # %.lr.ph.preheader
	testb	$1, %dl
	jne	.LBB0_4
# BB#3:
	xorpd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	cmpq	$1, %rdx
	jne	.LBB0_6
	jmp	.LBB0_8
.LBB0_1:
	xorps	%xmm0, %xmm0
	retq
.LBB0_4:                                # %.lr.ph.prol
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm1
	xorpd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	movl	$1, %ecx
	cmpq	$1, %rdx
	je	.LBB0_8
.LBB0_6:                                # %.lr.ph.preheader.new
	subq	%rcx, %rdx
	leaq	4(%rdi,%rcx,4), %rax
	leaq	4(%rsi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	movss	-4(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm1, %xmm3
	addsd	%xmm0, %xmm3
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm4, %xmm0
	mulsd	%xmm1, %xmm0
	addsd	%xmm3, %xmm0
	addq	$8, %rax
	addq	$8, %rcx
	addq	$-2, %rdx
	jne	.LBB0_7
.LBB0_8:                                # %._crit_edge
	retq
.Lfunc_end0:
	.size	loop, .Lfunc_end0-loop
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_1:
	.long	1045220557              # float 0.200000003
	.long	1036831949              # float 0.100000001
	.zero	4
	.zero	4
.LCPI1_2:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI1_3:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI1_4:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI1_5:
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	12                      # 0xc
.LCPI1_6:
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	subq	$16392, %rsp            # imm = 0x4008
.Lcfi0:
	.cfi_def_cfa_offset 16400
	movss	.LCPI1_0(%rip), %xmm9   # xmm9 = mem[0],zero,zero,zero
	xorl	%eax, %eax
	xorpd	%xmm0, %xmm0
	movaps	.LCPI1_1(%rip), %xmm8   # xmm8 = <0.200000003,0.100000001,u,u>
	movdqa	.LCPI1_2(%rip), %xmm10  # xmm10 = [0,1,2,3]
	movdqa	.LCPI1_3(%rip), %xmm12  # xmm12 = [4,4,4,4]
	movdqa	.LCPI1_4(%rip), %xmm13  # xmm13 = [8,8,8,8]
	movdqa	.LCPI1_5(%rip), %xmm14  # xmm14 = [12,12,12,12]
	movdqa	.LCPI1_6(%rip), %xmm15  # xmm15 = [16,16,16,16]
	movaps	%xmm9, %xmm11
	.p2align	4, 0x90
.LBB1_1:                                # %min.iters.checked
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_2 Depth 2
                                        #     Child Loop BB1_4 Depth 2
	movslq	%eax, %rcx
	imulq	$1717986919, %rcx, %rdx # imm = 0x66666667
	movq	%rdx, %rsi
	shrq	$63, %rsi
	sarq	$34, %rdx
	addl	%esi, %edx
	addl	%edx, %edx
	leal	(%rdx,%rdx,4), %edx
	xorl	%esi, %esi
	cmpl	%edx, %ecx
	sete	%sil
	addps	%xmm8, %xmm11
	movd	%esi, %xmm1
	movaps	%xmm11, %xmm2
	pshufd	$80, %xmm1, %xmm11      # xmm11 = xmm1[0,0,1,1]
	pslld	$31, %xmm11
	psrad	$31, %xmm11
	andps	%xmm11, %xmm2
	andnps	%xmm9, %xmm11
	orps	%xmm2, %xmm11
	pshufd	$85, %xmm11, %xmm7      # xmm7 = xmm11[1,1,1,1]
	pshufd	$0, %xmm11, %xmm3       # xmm3 = xmm11[0,0,0,0]
	xorl	%ecx, %ecx
	movdqa	%xmm10, %xmm4
	.p2align	4, 0x90
.LBB1_2:                                # %vector.body
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqa	%xmm4, %xmm2
	paddd	%xmm12, %xmm2
	cvtdq2ps	%xmm4, %xmm5
	cvtdq2ps	%xmm2, %xmm2
	movdqa	%xmm7, %xmm1
	addps	%xmm5, %xmm1
	movdqa	%xmm7, %xmm6
	addps	%xmm2, %xmm6
	movaps	%xmm1, 8192(%rsp,%rcx,4)
	movaps	%xmm6, 8208(%rsp,%rcx,4)
	addps	%xmm3, %xmm5
	addps	%xmm3, %xmm2
	movaps	%xmm5, (%rsp,%rcx,4)
	movaps	%xmm2, 16(%rsp,%rcx,4)
	movdqa	%xmm4, %xmm1
	paddd	%xmm13, %xmm1
	movdqa	%xmm4, %xmm2
	paddd	%xmm14, %xmm2
	cvtdq2ps	%xmm1, %xmm1
	cvtdq2ps	%xmm2, %xmm2
	movdqa	%xmm7, %xmm5
	addps	%xmm1, %xmm5
	movdqa	%xmm7, %xmm6
	addps	%xmm2, %xmm6
	movaps	%xmm5, 8224(%rsp,%rcx,4)
	movaps	%xmm6, 8240(%rsp,%rcx,4)
	addps	%xmm3, %xmm1
	addps	%xmm3, %xmm2
	movaps	%xmm1, 32(%rsp,%rcx,4)
	movaps	%xmm2, 48(%rsp,%rcx,4)
	addq	$16, %rcx
	paddd	%xmm15, %xmm4
	cmpq	$2048, %rcx             # imm = 0x800
	jne	.LBB1_2
# BB#3:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB1_1 Depth=1
	xorps	%xmm1, %xmm1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph.i
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	8192(%rsp,%rcx,4), %xmm2 # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movss	(%rsp,%rcx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm2, %xmm3
	addsd	%xmm1, %xmm3
	movss	8196(%rsp,%rcx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm1, %xmm2
	movss	4(%rsp,%rcx,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm2, %xmm1
	addsd	%xmm3, %xmm1
	addq	$2, %rcx
	cmpq	$2048, %rcx             # imm = 0x800
	jne	.LBB1_4
# BB#5:                                 # %loop.exit
                                        #   in Loop: Header=BB1_1 Depth=1
	addsd	%xmm1, %xmm0
	incl	%eax
	cmpl	$500000, %eax           # imm = 0x7A120
	jne	.LBB1_1
# BB#6:
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	addq	$16392, %rsp            # imm = 0x4008
	retq
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Total is %g\n"
	.size	.L.str, 13


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
