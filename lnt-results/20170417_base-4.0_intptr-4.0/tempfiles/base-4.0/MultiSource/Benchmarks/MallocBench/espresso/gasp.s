	.text
	.file	"gasp.bc"
	.globl	expand_gasp
	.p2align	4, 0x90
	.type	expand_gasp,@function
expand_gasp:                            # @expand_gasp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movl	cube(%rip), %esi
	xorl	%ebp, %ebp
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rcx
	movq	%rcx, (%rsp)
	cmpl	$0, 12(%rbx)
	jle	.LBB0_4
# BB#1:                                 # %.lr.ph.preheader
	movq	%rsp, %r13
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movl	%ebp, %r8d
	movq	%r13, %r9
	callq	expand1_gasp
	incl	%ebp
	cmpl	12(%rbx), %ebp
	jl	.LBB0_2
# BB#3:                                 # %._crit_edge.loopexit
	movq	(%rsp), %rcx
.LBB0_4:                                # %._crit_edge
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_dupl
	movq	%rax, %rcx
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r14, %rsi
	callq	expand
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	expand_gasp, .Lfunc_end0-expand_gasp
	.cfi_endproc

	.globl	expand1_gasp
	.p2align	4, 0x90
	.type	expand1_gasp,@function
expand1_gasp:                           # @expand1_gasp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 128
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movl	%r8d, %r12d
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rdx, %rbp
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	testb	$8, debug(%rip)
	je	.LBB1_2
# BB#1:
	movq	24(%rbx), %rax
	movslq	(%rbx), %rcx
	movslq	%r12d, %rdx
	imulq	%rcx, %rdx
	leaq	(%rax,%rdx,4), %rdi
	xorl	%eax, %eax
	callq	pc1
	movq	%rax, %rcx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
.LBB1_2:
	movl	cube(%rip), %r14d
	cmpl	$33, %r14d
	jge	.LBB1_4
# BB#3:
	movl	$8, %edi
	jmp	.LBB1_5
.LBB1_4:
	leal	-1(%r14), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB1_5:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r14d, %esi
	callq	set_clear
	movq	%rax, %r15
	movl	cube(%rip), %r14d
	cmpl	$33, %r14d
	jge	.LBB1_7
# BB#6:
	movl	$8, %edi
	jmp	.LBB1_8
.LBB1_7:
	leal	-1(%r14), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB1_8:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r14d, %esi
	callq	set_clear
	movq	%rax, %r13
	movl	cube(%rip), %r14d
	cmpl	$33, %r14d
	jge	.LBB1_10
# BB#9:
	movl	$8, %edi
	jmp	.LBB1_11
.LBB1_10:
	leal	-1(%r14), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB1_11:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r14d, %esi
	callq	set_clear
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movslq	12(%rbp), %rax
	movl	%eax, 16(%rbp)
	movslq	(%rbp), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB1_14
# BB#12:                                # %.lr.ph124.preheader
	movq	24(%rbp), %rax
	leaq	(%rax,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB1_13:                               # %.lr.ph124
                                        # =>This Inner Loop Header: Depth=1
	orb	$32, 1(%rax)
	movslq	(%rbp), %rdx
	leaq	(%rax,%rdx,4), %rax
	cmpq	%rcx, %rax
	jb	.LBB1_13
.LBB1_14:                               # %._crit_edge125
	movl	12(%rbx), %ecx
	movl	%ecx, 16(%rbx)
	movq	24(%rbx), %rax
	testl	%ecx, %ecx
	jle	.LBB1_41
# BB#15:                                # %.lr.ph120.preheader
	xorl	%ecx, %ecx
	movl	$-8193, %edx            # imm = 0xDFFF
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB1_16:                               # %.lr.ph120
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%ecx, %r12d
	je	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_16 Depth=1
	movl	(%rsi), %edi
	testw	%di, %di
	js	.LBB1_18
# BB#19:                                #   in Loop: Header=BB1_16 Depth=1
	orl	$8192, %edi             # imm = 0x2000
	jmp	.LBB1_20
	.p2align	4, 0x90
.LBB1_18:                               #   in Loop: Header=BB1_16 Depth=1
	decl	16(%rbx)
	movl	(%rsi), %edi
	andl	%edx, %edi
.LBB1_20:                               #   in Loop: Header=BB1_16 Depth=1
	movl	%edi, (%rsi)
	movslq	(%rbx), %rdi
	leaq	(%rsi,%rdi,4), %rsi
	incl	%ecx
	cmpl	12(%rbx), %ecx
	jl	.LBB1_16
	jmp	.LBB1_21
.LBB1_41:                               # %._crit_edge125.._crit_edge121_crit_edge
	movl	(%rbx), %edi
.LBB1_21:                               # %._crit_edge121
	imull	%r12d, %edi
	movslq	%edi, %rcx
	leaq	(%rax,%rcx,4), %rsi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	set_copy
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r15, %rdx
	callq	set_diff
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r13, %rcx
	callq	essen_parts
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, (%rsp)            # 8-byte Spill
	movq	%r15, %rsi
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%r13, %rdx
	callq	essen_raising
	cmpl	$0, 12(%rbx)
	jle	.LBB1_35
# BB#22:                                # %.lr.ph.preheader
	movq	24(%rbx), %r15
	movslq	%r12d, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_23:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testb	$32, 1(%r15)
	je	.LBB1_34
# BB#24:                                #   in Loop: Header=BB1_23 Depth=1
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	setp_implies
	testl	%eax, %eax
	jne	.LBB1_26
# BB#25:                                #   in Loop: Header=BB1_23 Depth=1
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	feasibly_covered
	testl	%eax, %eax
	je	.LBB1_34
.LBB1_26:                               #   in Loop: Header=BB1_23 Depth=1
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	sf_save
	movq	%rax, %rbp
	movslq	(%rbp), %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	imulq	%rax, %rdi
	shlq	$2, %rdi
	addq	24(%rbp), %rdi
	movslq	(%rbx), %rsi
	imulq	%rax, %rsi
	shlq	$2, %rsi
	addq	24(%rbx), %rsi
	xorl	%eax, %eax
	callq	set_copy
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	cube2list
	movq	%rax, %r12
	movq	24(%rbp), %rax
	movslq	(%rbp), %rcx
	movslq	%r14d, %rdx
	imulq	%rcx, %rdx
	leaq	(%rax,%rdx,4), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	reduce_cube
	movq	%rax, %r13
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB1_28
# BB#27:                                # %.thread
                                        #   in Loop: Header=BB1_23 Depth=1
	callq	free
	jmp	.LBB1_29
.LBB1_28:                               #   in Loop: Header=BB1_23 Depth=1
	testq	%r12, %r12
	je	.LBB1_30
.LBB1_29:                               #   in Loop: Header=BB1_23 Depth=1
	movq	%r12, %rdi
	callq	free
.LBB1_30:                               #   in Loop: Header=BB1_23 Depth=1
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, %rbx
	movq	%r13, %rsi
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%rbp, %rdx
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	%r12, %rcx
	callq	feasibly_covered
	testl	%eax, %eax
	je	.LBB1_32
# BB#31:                                #   in Loop: Header=BB1_23 Depth=1
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	callq	set_or
	andb	$127, 1(%r12)
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rdi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	sf_addset
	movq	%rax, (%rbp)
.LBB1_32:                               #   in Loop: Header=BB1_23 Depth=1
	testq	%r13, %r13
	movq	%rbx, %rbp
	movq	40(%rsp), %rbx          # 8-byte Reload
	je	.LBB1_34
# BB#33:                                #   in Loop: Header=BB1_23 Depth=1
	movq	%r13, %rdi
	callq	free
.LBB1_34:                               #   in Loop: Header=BB1_23 Depth=1
	movslq	(%rbx), %rax
	leaq	(%r15,%rax,4), %r15
	incl	%r14d
	cmpl	12(%rbx), %r14d
	jl	.LBB1_23
.LBB1_35:                               # %._crit_edge
	movq	(%rsp), %rdi            # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB1_37
# BB#36:
	callq	free
.LBB1_37:
	movq	16(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB1_39
# BB#38:
	callq	free
.LBB1_39:
	movq	8(%rsp), %rdi           # 8-byte Reload
	addq	$72, %rsp
	testq	%rdi, %rdi
	je	.LBB1_40
# BB#42:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB1_40:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	expand1_gasp, .Lfunc_end1-expand1_gasp
	.cfi_endproc

	.globl	irred_gasp
	.p2align	4, 0x90
	.type	irred_gasp,@function
irred_gasp:                             # @irred_gasp
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpl	$0, 12(%rdx)
	je	.LBB2_1
# BB#2:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rdx, %rsi
	callq	sf_append
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	irredundant             # TAILCALL
.LBB2_1:
	xorl	%eax, %eax
	movq	%rdx, %rdi
	callq	sf_free
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	irred_gasp, .Lfunc_end2-irred_gasp
	.cfi_endproc

	.globl	last_gasp
	.p2align	4, 0x90
	.type	last_gasp,@function
last_gasp:                              # @last_gasp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 80
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	%rsi, %r12
	movq	%rdi, %r15
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	reduce_gasp
	movq	%rax, %rbx
	movl	$9, %esi
	xorl	%eax, %eax
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r14, (%rsp)            # 8-byte Spill
	movq	%r14, %rcx
	callq	totals
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r14
	movl	cube(%rip), %esi
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rcx
	movq	%rcx, 8(%rsp)
	cmpl	$0, 12(%rbx)
	jle	.LBB3_4
# BB#1:                                 # %.lr.ph.i.preheader
	movq	%r14, 16(%rsp)          # 8-byte Spill
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%r15, %rcx
	movl	%ebp, %r8d
	movq	%r14, %r9
	callq	expand1_gasp
	incl	%ebp
	cmpl	12(%rbx), %ebp
	jl	.LBB3_2
# BB#3:                                 # %._crit_edge.loopexit.i
	movq	8(%rsp), %rcx
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB3_4:                                # %expand_gasp.exit
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_dupl
	movq	%rax, %rcx
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r13, %rsi
	callq	expand
	movq	%rax, %rbp
	movl	$7, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbp, %rdx
	movq	(%rsp), %r14            # 8-byte Reload
	movq	%r14, %rcx
	callq	totals
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	cmpl	$0, 12(%rbp)
	je	.LBB3_6
# BB#5:
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	sf_append
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r12, %rsi
	callq	irredundant
	movq	%rax, %r15
	jmp	.LBB3_7
.LBB3_6:
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sf_free
.LBB3_7:                                # %irred_gasp.exit
	movl	$8, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	totals
	movq	%r15, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	last_gasp, .Lfunc_end3-last_gasp
	.cfi_endproc

	.p2align	4, 0x90
	.type	reduce_gasp,@function
reduce_gasp:                            # @reduce_gasp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 64
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movl	12(%r12), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	cube2list
	movq	%rax, %r15
	movslq	(%r12), %rcx
	movslq	12(%r12), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB4_13
# BB#1:                                 # %.lr.ph.preheader
	movq	24(%r12), %rbx
	leaq	(%rbx,%rax,4), %r14
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	reduce_cube
	movq	%rax, %r13
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	setp_empty
	testl	%eax, %eax
	je	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	fatal
	testb	$16, debug(%rip)
	jne	.LBB4_9
	jmp	.LBB4_10
	.p2align	4, 0x90
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	setp_equal
	testl	%eax, %eax
	movl	(%r13), %eax
	je	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_2 Depth=1
	orl	$32768, %eax            # imm = 0x8000
	movl	%eax, (%r13)
	xorl	%eax, %eax
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbx, %rsi
	jmp	.LBB4_7
.LBB4_6:                                #   in Loop: Header=BB4_2 Depth=1
	andl	$-32769, %eax           # imm = 0xFFFF7FFF
	movl	%eax, (%r13)
	xorl	%eax, %eax
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r13, %rsi
.LBB4_7:                                #   in Loop: Header=BB4_2 Depth=1
	callq	sf_addset
	movq	%rax, (%rsp)            # 8-byte Spill
	testb	$16, debug(%rip)
	je	.LBB4_10
.LBB4_9:                                #   in Loop: Header=BB4_2 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pc1
	movq	%rax, %rbp
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	pc2
	movq	%rax, %rcx
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	movq	%rcx, %rdx
	callq	printf
.LBB4_10:                               #   in Loop: Header=BB4_2 Depth=1
	testq	%r13, %r13
	je	.LBB4_12
# BB#11:                                #   in Loop: Header=BB4_2 Depth=1
	movq	%r13, %rdi
	callq	free
.LBB4_12:                               #   in Loop: Header=BB4_2 Depth=1
	movslq	(%r12), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r14, %rbx
	jb	.LBB4_2
.LBB4_13:                               # %._crit_edge
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_15
# BB#14:
	callq	free
.LBB4_15:
	movq	%r15, %rdi
	callq	free
	movq	(%rsp), %rax            # 8-byte Reload
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	reduce_gasp, .Lfunc_end4-reduce_gasp
	.cfi_endproc

	.globl	super_gasp
	.p2align	4, 0x90
	.type	super_gasp,@function
super_gasp:                             # @super_gasp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi63:
	.cfi_def_cfa_offset 64
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %rbp
	movq	%rdi, %r13
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	movq	%r13, %rdi
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rbp, %rsi
	callq	reduce_gasp
	movq	%rax, %rbp
	movl	$9, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	movq	%r15, %rcx
	callq	totals
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	all_primes
	movq	%rax, %r14
	movl	$7, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	totals
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sf_free
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	sf_append
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_dupl
	movq	%rax, %rbx
	cmpl	$0, trace(%rip)
	je	.LBB5_2
# BB#1:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r12, %rcx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB5_2:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbp
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	irredundant
	movq	%rax, %rbx
	movl	$5, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	totals
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	super_gasp, .Lfunc_end5-super_gasp
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\nEXPAND1_GASP:    \t%s\n"
	.size	.L.str, 23

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"NEWPRIMES"
	.size	.L.str.1, 10

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"empty reduction in reduce_gasp, shouldn't happen"
	.size	.L.str.2, 49

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"REDUCE_GASP: %s reduced to %s\n"
	.size	.L.str.3, 31


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
