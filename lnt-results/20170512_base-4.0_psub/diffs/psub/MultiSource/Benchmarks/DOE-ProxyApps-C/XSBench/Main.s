	.text
	.file	"Main.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$440, %rsp              # imm = 0x1B8
.Lcfi6:
	.cfi_def_cfa_offset 496
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$26, %edi
	callq	srand
	leaq	48(%rsp), %rdi
	movl	%ebp, %esi
	movq	%rbx, %rdx
	callq	read_CLI
	movq	80(%rsp), %rax
	movq	%rax, 32(%rsp)
	movupd	48(%rsp), %xmm0
	movups	64(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movl	$13, %esi
	callq	print_inputs
	movl	$.Lstr, %edi
	callq	puts
	movq	56(%rsp), %rdi
	movq	64(%rsp), %rsi
	callq	gpmatrix
	movq	%rax, %rbx
	movq	56(%rsp), %rsi
	movq	64(%rsp), %rdx
	movq	%rbx, %rdi
	callq	generate_grids_v
	movl	$.Lstr.1, %edi
	callq	puts
	movq	56(%rsp), %rsi
	movq	64(%rsp), %rdx
	movq	%rbx, %rdi
	callq	sort_nuclide_grids
	movq	56(%rsp), %rdi
	movq	64(%rsp), %rsi
	movq	%rbx, %rdx
	callq	generate_energy_grid
	movq	%rax, %r14
	movq	56(%rsp), %rdx
	movq	64(%rsp), %rcx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	set_grid_ptrs
	movl	$.Lstr.2, %edi
	callq	puts
	movq	56(%rsp), %rdi
	callq	load_num_nucs
	movq	%rax, %rbp
	movq	56(%rsp), %rsi
	movq	%rbp, %rdi
	callq	load_mats
	movq	%rax, %r15
	movq	%rbp, %rdi
	callq	load_concs_v
	movq	%rax, %r13
	movl	$10, %edi
	callq	putchar
	callq	border_print
	movl	$.L.str.4, %edi
	movl	$79, %esi
	callq	center_print
	callq	border_print
	movq	$36, 120(%rsp)
	xorl	%r12d, %r12d
	cmpl	$0, 72(%rsp)
	jle	.LBB0_1
# BB#2:                                 # %.lr.ph
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movq	%r14, 104(%rsp)         # 8-byte Spill
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	movq	%r13, %rbx
	movq	%r15, %r13
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	callq	rn_v
	movsd	%xmm0, 88(%rsp)         # 8-byte Spill
	leaq	120(%rsp), %rdi
	callq	pick_mat
	movl	%eax, %r15d
	movq	56(%rsp), %rsi
	movq	64(%rsp), %rdx
	leaq	128(%rsp), %rax
	movq	%rax, 16(%rsp)
	movq	%r13, 8(%rsp)
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movsd	88(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movl	%r15d, %edi
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	%rbx, %r8
	movq	104(%rsp), %r9          # 8-byte Reload
	callq	calculate_macro_xs
	movsd	128(%rsp), %xmm1        # xmm1 = mem[0],zero
	movsd	136(%rsp), %xmm2        # xmm2 = mem[0],zero
	movsd	144(%rsp), %xmm3        # xmm3 = mem[0],zero
	movsd	152(%rsp), %xmm4        # xmm4 = mem[0],zero
	movsd	160(%rsp), %xmm5        # xmm5 = mem[0],zero
	movl	$.L.str.5, %esi
	movb	$6, %al
	leaq	176(%rsp), %r14
	movq	%r14, %rdi
	movsd	88(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movl	%r15d, %edx
	callq	sprintf
	movl	$10000, %esi            # imm = 0x2710
	movq	%r14, %rdi
	callq	hash
	movl	%eax, %eax
	addq	%rax, %rbp
	incl	%r12d
	cmpl	72(%rsp), %r12d
	jl	.LBB0_3
	jmp	.LBB0_4
.LBB0_1:
	xorl	%ebp, %ebp
.LBB0_4:                                # %._crit_edge
	movl	$10, %edi
	callq	putchar
	movl	$.Lstr.3, %edi
	callq	puts
	movq	80(%rsp), %rax
	movq	%rax, 32(%rsp)
	movups	48(%rsp), %xmm0
	movups	64(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	xorl	%edi, %edi
	xorps	%xmm0, %xmm0
	movq	%rbp, %rdx
	callq	print_results
	xorl	%eax, %eax
	addq	$440, %rsp              # imm = 0x1B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"SIMULATION"
	.size	.L.str.4, 11

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%.5lf %d %.5lf %.5lf %.5lf %.5lf %.5lf"
	.size	.L.str.5, 39

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Generating Nuclide Energy Grids..."
	.size	.Lstr, 35

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"Sorting Nuclide Energy Grids..."
	.size	.Lstr.1, 32

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.2:
	.asciz	"Loading Mats..."
	.size	.Lstr.2, 16

	.type	.Lstr.3,@object         # @str.3
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.3:
	.asciz	"Simulation complete."
	.size	.Lstr.3, 21


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
