	.text
	.file	"ptou.bc"
	.globl	ptou
	.p2align	4, 0x90
	.type	ptou,@function
ptou:                                   # @ptou
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB0_2
# BB#1:
	incw	(%r14)
.LBB0_2:
	cmpb	$0, 6(%r14)
	je	.LBB0_5
# BB#3:
	movl	$4, %edi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	jmp	.LBB0_4
.LBB0_5:
	leaq	8(%r14), %rax
	movzwl	4(%r14), %ecx
	leaq	8(%r14,%rcx,2), %rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_6:                                # =>This Inner Loop Header: Depth=1
	cmpl	$65536, %ebx            # imm = 0x10000
	jae	.LBB0_7
# BB#8:                                 #   in Loop: Header=BB0_6 Depth=1
	movzwl	-2(%rcx), %edx
	addq	$-2, %rcx
	shll	$16, %ebx
	orl	%edx, %ebx
	cmpq	%rax, %rcx
	ja	.LBB0_6
	jmp	.LBB0_9
.LBB0_7:
	movl	$5, %edi
	movl	$.L.str, %esi
	movl	$.L.str.2, %edx
.LBB0_4:                                # %.loopexit
	callq	errorp
	movq	%rax, %rbx
.LBB0_9:                                # %.loopexit
	testq	%r14, %r14
	je	.LBB0_12
# BB#10:
	decw	(%r14)
	jne	.LBB0_12
# BB#11:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	pfree
.LBB0_12:
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	ptou, .Lfunc_end0-ptou
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ptou"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"negative argument"
	.size	.L.str.1, 18

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"overflow"
	.size	.L.str.2, 9


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
