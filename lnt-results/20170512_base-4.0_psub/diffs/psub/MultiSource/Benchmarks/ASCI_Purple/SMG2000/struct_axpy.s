	.text
	.file	"struct_axpy.bc"
	.globl	hypre_StructAxpy
	.p2align	4, 0x90
	.type	hypre_StructAxpy,@function
hypre_StructAxpy:                       # @hypre_StructAxpy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 272
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	movaps	%xmm0, 176(%rsp)        # 16-byte Spill
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	movq	8(%rsi), %rax
	movq	8(%rax), %rcx
	cmpl	$0, 8(%rcx)
	jle	.LBB0_20
# BB#1:                                 # %.lr.ph317
	movaps	176(%rsp), %xmm0        # 16-byte Reload
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	xorl	%esi, %esi
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_29 Depth 4
                                        #         Child Loop BB0_15 Depth 4
	movq	(%rcx), %rcx
	leaq	(,%rsi,8), %rax
	leaq	(%rax,%rax,2), %rbp
	movq	%rcx, %r15
	leaq	(%rcx,%rbp), %rdi
	movq	136(%rsp), %rdx         # 8-byte Reload
	movq	16(%rdx), %rax
	movq	24(%rdx), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	(%rax), %r14
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	16(%rcx), %rax
	movq	24(%rcx), %rbx
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movq	(%rax), %r12
	movq	40(%rdx), %rax
	movslq	(%rax,%rsi,4), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	40(%rcx), %rax
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	movslq	(%rax,%rsi,4), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	116(%rsp), %rsi
	callq	hypre_BoxGetSize
	movq	%r14, %r11
	movl	(%r12,%rbp), %edi
	movl	4(%r12,%rbp), %ebx
	movl	12(%r12,%rbp), %eax
	movl	%eax, %r13d
	subl	%edi, %r13d
	incl	%r13d
	cmpl	%edi, %eax
	movl	16(%r12,%rbp), %eax
	movl	$0, %ecx
	cmovsl	%ecx, %r13d
	movl	%eax, %r9d
	subl	%ebx, %r9d
	incl	%r9d
	cmpl	%ebx, %eax
	movl	(%r11,%rbp), %r8d
	movl	4(%r11,%rbp), %esi
	movl	12(%r11,%rbp), %eax
	cmovsl	%ecx, %r9d
	movl	%eax, %edx
	subl	%r8d, %edx
	incl	%edx
	cmpl	%r8d, %eax
	movl	16(%r11,%rbp), %eax
	cmovsl	%ecx, %edx
	movl	%edx, 20(%rsp)          # 4-byte Spill
	movl	%eax, %r14d
	subl	%esi, %r14d
	incl	%r14d
	cmpl	%esi, %eax
	cmovsl	%ecx, %r14d
	movl	116(%rsp), %eax
	movl	120(%rsp), %ecx
	movl	124(%rsp), %edx
	cmpl	%eax, %ecx
	movq	%rax, 56(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmovgel	%ecx, %eax
	cmpl	%eax, %edx
	movl	%edx, 36(%rsp)          # 4-byte Spill
	cmovgel	%edx, %eax
	testl	%eax, %eax
	jle	.LBB0_19
# BB#3:                                 # %.lr.ph313
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_19
# BB#4:                                 # %.lr.ph313
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_19
# BB#5:                                 # %.preheader279.us.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, %rax
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movl	%esi, (%rsp)            # 4-byte Spill
	movl	%edi, %esi
	movl	8(%rax,%rbp), %r10d
	movl	%r10d, %r15d
	subl	8(%r12,%rbp), %r15d
	subl	8(%r11,%rbp), %r10d
	movl	(%rax,%rbp), %r12d
	movl	4(%rax,%rbp), %ecx
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	movq	56(%rsp), %rbx          # 8-byte Reload
	subl	%ebx, %ebp
	movl	%r13d, %r11d
	subl	%ebx, %r11d
	movq	40(%rsp), %rdi          # 8-byte Reload
	leal	-1(%rdi), %edx
	imull	%edx, %ebp
	imull	%edx, %r11d
	movl	%r12d, %edx
	subl	%esi, %edx
	movl	%ecx, %esi
	subl	8(%rsp), %esi           # 4-byte Folded Reload
	imull	%r9d, %r15d
	addl	%esi, %r15d
	imull	%r13d, %r15d
	addl	%edx, %r15d
	movl	%r15d, 4(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	(%rdx,%rsi,8), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	subl	%r8d, %r12d
	movq	72(%rsp), %r15          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	leaq	(%r15,%rsi,8), %r8
	subl	(%rsp), %ecx            # 4-byte Folded Reload
	movl	%r14d, %edx
	subl	%edi, %edx
	imull	%eax, %edx
	movl	%edx, 96(%rsp)          # 4-byte Spill
	subl	%edi, %r9d
	imull	%r13d, %r9d
	movl	%r9d, 92(%rsp)          # 4-byte Spill
	addl	%eax, %ebp
	subl	%ebx, %ebp
	movl	%ebp, 104(%rsp)         # 4-byte Spill
	addl	%r13d, %r11d
	subl	%ebx, %r11d
	movl	%r11d, 100(%rsp)        # 4-byte Spill
	imull	%r14d, %r10d
	leal	-1(%rbx), %edx
	addl	%ecx, %r10d
	movl	%eax, %ecx
	imull	%edi, %ecx
	movl	%ecx, 88(%rsp)          # 4-byte Spill
	imull	%eax, %r10d
	addl	%r12d, %r10d
	movl	%r10d, (%rsp)           # 4-byte Spill
	movl	%r13d, %eax
	imull	%edi, %eax
	movl	%eax, 84(%rsp)          # 4-byte Spill
	addq	%rdx, %rsi
	leaq	8(%r15,%rsi,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rcx          # 8-byte Reload
	addq	%rdx, %rcx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	leaq	1(%rdx), %r14
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax,%rcx,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %r14
	leaq	-4(%r14), %rax
	shrq	$2, %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movl	%r13d, 112(%rsp)        # 4-byte Spill
	.p2align	4, 0x90
.LBB0_6:                                # %.preheader279.us
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_8 Depth 3
                                        #         Child Loop BB0_29 Depth 4
                                        #         Child Loop BB0_15 Depth 4
	movq	56(%rsp), %rbx          # 8-byte Reload
	testl	%ebx, %ebx
	movl	100(%rsp), %eax         # 4-byte Reload
	movl	104(%rsp), %ecx         # 4-byte Reload
	jle	.LBB0_18
# BB#7:                                 # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB0_6 Depth=2
	movl	%edx, 108(%rsp)         # 4-byte Spill
	xorl	%r9d, %r9d
	movl	4(%rsp), %ebp           # 4-byte Reload
	movl	(%rsp), %r11d           # 4-byte Reload
	movapd	176(%rsp), %xmm4        # 16-byte Reload
	movapd	192(%rsp), %xmm5        # 16-byte Reload
	movl	20(%rsp), %edi          # 4-byte Reload
	movq	40(%rsp), %r15          # 8-byte Reload
	jmp	.LBB0_8
.LBB0_24:                               # %vector.ph
                                        #   in Loop: Header=BB0_8 Depth=3
	cmpq	$0, 160(%rsp)           # 8-byte Folded Reload
	jne	.LBB0_25
# BB#26:                                # %vector.body.prol
                                        #   in Loop: Header=BB0_8 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	movupd	(%rax,%r10,8), %xmm0
	movupd	16(%rax,%r10,8), %xmm1
	mulpd	%xmm5, %xmm0
	mulpd	%xmm5, %xmm1
	movupd	(%r8,%r12,8), %xmm2
	movupd	16(%r8,%r12,8), %xmm3
	addpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm3
	movupd	%xmm2, (%r8,%r12,8)
	movupd	%xmm3, 16(%r8,%r12,8)
	movl	$4, %r13d
	cmpq	$0, 168(%rsp)           # 8-byte Folded Reload
	jne	.LBB0_28
	jmp	.LBB0_30
.LBB0_25:                               #   in Loop: Header=BB0_8 Depth=3
	xorl	%r13d, %r13d
	cmpq	$0, 168(%rsp)           # 8-byte Folded Reload
	je	.LBB0_30
.LBB0_28:                               # %vector.ph.new
                                        #   in Loop: Header=BB0_8 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rax,%r10,8), %rsi
	leaq	(%r8,%r12,8), %r15
	.p2align	4, 0x90
.LBB0_29:                               # %vector.body
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movupd	(%rsi,%r13,8), %xmm0
	movupd	16(%rsi,%r13,8), %xmm1
	mulpd	%xmm5, %xmm0
	mulpd	%xmm5, %xmm1
	movupd	(%r15,%r13,8), %xmm2
	movupd	16(%r15,%r13,8), %xmm3
	addpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm3
	movupd	%xmm2, (%r15,%r13,8)
	movupd	%xmm3, 16(%r15,%r13,8)
	movupd	32(%rsi,%r13,8), %xmm0
	movupd	48(%rsi,%r13,8), %xmm1
	mulpd	%xmm5, %xmm0
	mulpd	%xmm5, %xmm1
	movupd	32(%r15,%r13,8), %xmm2
	movupd	48(%r15,%r13,8), %xmm3
	addpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm3
	movupd	%xmm2, 32(%r15,%r13,8)
	movupd	%xmm3, 48(%r15,%r13,8)
	addq	$8, %r13
	cmpq	%r13, %r14
	jne	.LBB0_29
.LBB0_30:                               # %middle.block
                                        #   in Loop: Header=BB0_8 Depth=3
	cmpq	%r14, 24(%rsp)          # 8-byte Folded Reload
	movl	112(%rsp), %r13d        # 4-byte Reload
	movl	20(%rsp), %edi          # 4-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	40(%rsp), %r15          # 8-byte Reload
	je	.LBB0_16
# BB#31:                                #   in Loop: Header=BB0_8 Depth=3
	addq	%r14, %r12
	addq	%r14, %r10
	movl	%r14d, %esi
	jmp	.LBB0_10
	.p2align	4, 0x90
.LBB0_8:                                # %.preheader.us.us
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_29 Depth 4
                                        #         Child Loop BB0_15 Depth 4
	movslq	%r11d, %r10
	movslq	%ebp, %r12
	cmpq	$3, 24(%rsp)            # 8-byte Folded Reload
	jbe	.LBB0_9
# BB#21:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_8 Depth=3
	testq	%r14, %r14
	je	.LBB0_9
# BB#22:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_8 Depth=3
	movl	%r13d, %eax
	imull	%r9d, %eax
	addl	4(%rsp), %eax           # 4-byte Folded Reload
	cltq
	leaq	(%r8,%rax,8), %rdx
	movl	%edi, %ecx
	imull	%r9d, %ecx
	addl	(%rsp), %ecx            # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movq	72(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB0_24
# BB#23:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_8 Depth=3
	movq	64(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	cmpq	%rax, %rcx
	jae	.LBB0_24
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_8 Depth=3
	xorl	%esi, %esi
.LBB0_10:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_8 Depth=3
	movl	%ebx, %eax
	subl	%esi, %eax
	testb	$1, %al
	jne	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_8 Depth=3
	movl	%esi, %edx
	cmpl	%esi, 48(%rsp)          # 4-byte Folded Reload
	jne	.LBB0_14
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_12:                               # %scalar.ph.prol
                                        #   in Loop: Header=BB0_8 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	movsd	(%rax,%r10,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	addsd	(%r8,%r12,8), %xmm0
	movsd	%xmm0, (%r8,%r12,8)
	incq	%r10
	incq	%r12
	leal	1(%rsi), %edx
	cmpl	%esi, 48(%rsp)          # 4-byte Folded Reload
	je	.LBB0_16
.LBB0_14:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB0_8 Depth=3
	leaq	8(%r8,%r12,8), %rsi
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	8(%rax,%r10,8), %rax
	movl	%ebx, %ecx
	subl	%edx, %ecx
	.p2align	4, 0x90
.LBB0_15:                               # %scalar.ph
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	-8(%rax), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	addsd	-8(%rsi), %xmm0
	movsd	%xmm0, -8(%rsi)
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	addsd	(%rsi), %xmm0
	movsd	%xmm0, (%rsi)
	addq	$16, %rsi
	addq	$16, %rax
	addl	$-2, %ecx
	jne	.LBB0_15
.LBB0_16:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB0_8 Depth=3
	addl	%edi, %r11d
	addl	%r13d, %ebp
	incl	%r9d
	cmpl	%r15d, %r9d
	jne	.LBB0_8
# BB#17:                                #   in Loop: Header=BB0_6 Depth=2
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	88(%rsp), %ecx          # 4-byte Reload
	movl	108(%rsp), %edx         # 4-byte Reload
.LBB0_18:                               # %._crit_edge287.us
                                        #   in Loop: Header=BB0_6 Depth=2
	addl	(%rsp), %ecx            # 4-byte Folded Reload
	addl	4(%rsp), %eax           # 4-byte Folded Reload
	addl	96(%rsp), %ecx          # 4-byte Folded Reload
	addl	92(%rsp), %eax          # 4-byte Folded Reload
	incl	%edx
	cmpl	36(%rsp), %edx          # 4-byte Folded Reload
	movl	%ecx, (%rsp)            # 4-byte Spill
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jne	.LBB0_6
.LBB0_19:                               # %._crit_edge314
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	152(%rsp), %rsi         # 8-byte Reload
	incq	%rsi
	movq	128(%rsp), %rcx         # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %rsi
	jl	.LBB0_2
.LBB0_20:                               # %._crit_edge318
	xorl	%eax, %eax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_StructAxpy, .Lfunc_end0-hypre_StructAxpy
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
