	.text
	.file	"hash2.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 192
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	$2000, %r14d            # imm = 0x7D0
	cmpl	$2, %edi
	jne	.LBB0_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
.LBB0_2:
.Ltmp0:
	leaq	8(%rsp), %rdi
	leaq	48(%rsp), %rdx
	movq	%rsp, %rcx
	leaq	88(%rsp), %r8
	movl	$100, %esi
	callq	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E
.Ltmp1:
# BB#3:                                 # %_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEC2Ev.exit
.Ltmp3:
	leaq	48(%rsp), %rdi
	movq	%rsp, %rdx
	leaq	88(%rsp), %rcx
	leaq	96(%rsp), %r8
	movl	$100, %esi
	callq	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E
.Ltmp4:
# BB#4:                                 # %_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEC2Ev.exit29
	leaq	112(%rsp), %rbx
	leaq	8(%rsp), %r15
	movq	%rsp, %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	sprintf
	movq	%rbx, %rdi
	callq	strdup
	movq	%rax, (%rsp)
.Ltmp6:
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_
.Ltmp7:
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=1
	movl	%ebp, (%rax)
	incl	%ebp
	cmpl	$10000, %ebp            # imm = 0x2710
	jl	.LBB0_5
# BB#7:                                 # %.preheader
	testl	%r14d, %r14d
	jle	.LBB0_20
# BB#8:                                 # %.lr.ph107
	xorl	%r13d, %r13d
	leaq	8(%rsp), %r12
.LBB0_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_14 Depth 2
                                        #     Child Loop BB0_15 Depth 2
                                        #       Child Loop BB0_36 Depth 3
                                        #       Child Loop BB0_38 Depth 3
	movq	%r14, %r15
	movq	16(%rsp), %rax
	movq	24(%rsp), %rcx
	subq	%rax, %rcx
	je	.LBB0_19
# BB#10:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB0_9 Depth=1
	sarq	$3, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph.i.i
                                        #   Parent Loop BB0_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rdx,8), %r14
	testq	%r14, %r14
	jne	.LBB0_15
# BB#13:                                #   in Loop: Header=BB0_14 Depth=2
	incq	%rdx
	cmpq	%rcx, %rdx
	jb	.LBB0_14
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph
                                        #   Parent Loop BB0_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_36 Depth 3
                                        #       Child Loop BB0_38 Depth 3
	leaq	8(%r14), %rbx
.Ltmp9:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_
.Ltmp10:
# BB#16:                                #   in Loop: Header=BB0_15 Depth=2
	movl	(%rax), %ebp
.Ltmp11:
	leaq	48(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_
.Ltmp12:
# BB#17:                                #   in Loop: Header=BB0_15 Depth=2
	addl	%ebp, (%rax)
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	jne	.LBB0_18
# BB#33:                                #   in Loop: Header=BB0_15 Depth=2
	movq	16(%rsp), %rdi
	movq	24(%rsp), %rbp
	subq	%rdi, %rbp
	sarq	$3, %rbp
	movq	(%rbx), %rcx
	movb	(%rcx), %dl
	testb	%dl, %dl
	je	.LBB0_34
# BB#35:                                # %.lr.ph.i.i.i.i.i.i.preheader
                                        #   in Loop: Header=BB0_15 Depth=2
	incq	%rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_36:                               # %.lr.ph.i.i.i.i.i.i
                                        #   Parent Loop BB0_9 Depth=1
                                        #     Parent Loop BB0_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%rax,%rax,4), %rbx
	movsbq	%dl, %rax
	addq	%rbx, %rax
	movzbl	(%rcx), %edx
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB0_36
	jmp	.LBB0_37
.LBB0_34:                               #   in Loop: Header=BB0_15 Depth=2
	xorl	%eax, %eax
.LBB0_37:                               # %.lr.ph.i
                                        #   in Loop: Header=BB0_15 Depth=2
	xorl	%edx, %edx
	divq	%rbp
	movq	%rdx, %rcx
	incq	%rcx
	.p2align	4, 0x90
.LBB0_38:                               #   Parent Loop BB0_9 Depth=1
                                        #     Parent Loop BB0_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, %rcx
	jae	.LBB0_18
# BB#39:                                #   in Loop: Header=BB0_38 Depth=3
	movq	(%rdi,%rcx,8), %r14
	incq	%rcx
	testq	%r14, %r14
	movl	$0, %esi
	je	.LBB0_38
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_18:                               # %_ZN9__gnu_cxx19_Hashtable_iteratorISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEppEv.exit.backedge
                                        #   in Loop: Header=BB0_15 Depth=2
	testq	%rsi, %rsi
	movq	%rsi, %r14
	jne	.LBB0_15
	.p2align	4, 0x90
.LBB0_19:                               # %_ZN9__gnu_cxx19_Hashtable_iteratorISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEppEv.exit._crit_edge
                                        #   in Loop: Header=BB0_9 Depth=1
	incl	%r13d
	movq	%r15, %r14
	cmpl	%r14d, %r13d
	jl	.LBB0_9
.LBB0_20:                               # %._crit_edge
	movq	$.L.str.1, (%rsp)
.Ltmp14:
	leaq	8(%rsp), %rdi
	movq	%rsp, %rsi
	callq	_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_
.Ltmp15:
# BB#21:
	movl	(%rax), %esi
.Ltmp16:
	movl	$_ZSt4cout, %edi
	callq	_ZNSolsEi
	movq	%rax, %rbx
.Ltmp17:
# BB#22:
.Ltmp18:
	movl	$.L.str.2, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp19:
# BB#23:                                # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
	movq	$.L.str.3, 88(%rsp)
.Ltmp21:
	leaq	8(%rsp), %rdi
	leaq	88(%rsp), %rsi
	callq	_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_
.Ltmp22:
# BB#24:
	movl	(%rax), %esi
.Ltmp23:
	movq	%rbx, %rdi
	callq	_ZNSolsEi
	movq	%rax, %rbx
.Ltmp24:
# BB#25:
.Ltmp25:
	movl	$.L.str.2, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp26:
# BB#26:                                # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit63
	movq	$.L.str.1, 96(%rsp)
.Ltmp28:
	leaq	48(%rsp), %rdi
	leaq	96(%rsp), %rsi
	callq	_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_
.Ltmp29:
# BB#27:
	movl	(%rax), %esi
.Ltmp30:
	movq	%rbx, %rdi
	callq	_ZNSolsEi
	movq	%rax, %rbx
.Ltmp31:
# BB#28:
.Ltmp32:
	movl	$.L.str.2, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp33:
# BB#29:                                # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit64
	movq	$.L.str.3, 104(%rsp)
.Ltmp35:
	leaq	48(%rsp), %rdi
	leaq	104(%rsp), %rsi
	callq	_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_
.Ltmp36:
# BB#30:
	movl	(%rax), %esi
.Ltmp37:
	movq	%rbx, %rdi
	callq	_ZNSolsEi
	movq	%rax, %rbx
.Ltmp38:
# BB#31:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	movq	240(%rbx,%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB0_32
# BB#42:                                # %.noexc84
	cmpb	$0, 56(%rbp)
	je	.LBB0_44
# BB#43:
	movb	67(%rbp), %al
	jmp	.LBB0_46
.LBB0_44:
.Ltmp39:
	movq	%rbp, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp40:
# BB#45:                                # %.noexc86
	movq	(%rbp), %rax
.Ltmp41:
	movl	$10, %esi
	movq	%rbp, %rdi
	callq	*48(%rax)
.Ltmp42:
.LBB0_46:                               # %.noexc
.Ltmp43:
	movsbl	%al, %esi
	movq	%rbx, %rdi
	callq	_ZNSo3putEc
.Ltmp44:
# BB#47:                                # %.noexc65
.Ltmp45:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp46:
# BB#48:                                # %_ZNSolsEPFRSoS_E.exit
	cmpq	$0, 80(%rsp)
	je	.LBB0_49
# BB#50:                                # %.preheader.i.i.i71
	movq	56(%rsp), %rax
	movq	64(%rsp), %rdi
	cmpq	%rax, %rdi
	je	.LBB0_56
# BB#51:                                # %.lr.ph16.i.i.i72
	xorl	%ebx, %ebx
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB0_52:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_53 Depth 2
	movq	(%rdi,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB0_55
	.p2align	4, 0x90
.LBB0_53:                               # %.lr.ph.i.i.i78
                                        #   Parent Loop BB0_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbp
	movq	%rax, %rdi
	callq	_ZdlPv
	testq	%rbp, %rbp
	movq	%rbp, %rax
	jne	.LBB0_53
# BB#54:                                # %._crit_edge.loopexit.i.i.i80
                                        #   in Loop: Header=BB0_52 Depth=1
	movq	56(%rsp), %rdi
.LBB0_55:                               # %._crit_edge.i.i.i81
                                        #   in Loop: Header=BB0_52 Depth=1
	movq	$0, (%rdi,%rbx,8)
	incq	%rbx
	movq	56(%rsp), %rdi
	movq	64(%rsp), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.LBB0_52
.LBB0_56:                               # %._crit_edge17.i.i.i74
	movq	$0, 80(%rsp)
	testq	%rdi, %rdi
	jne	.LBB0_58
	jmp	.LBB0_59
.LBB0_49:                               # %._ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE5clearEv.exit_crit_edge.i.i70
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_59
.LBB0_58:
	callq	_ZdlPv
.LBB0_59:                               # %_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEED2Ev.exit83
	cmpq	$0, 40(%rsp)
	je	.LBB0_60
# BB#61:                                # %.preheader.i.i.i50
	movq	16(%rsp), %rax
	movq	24(%rsp), %rdi
	cmpq	%rax, %rdi
	je	.LBB0_67
# BB#62:                                # %.lr.ph16.i.i.i51
	xorl	%ebx, %ebx
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB0_63:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_64 Depth 2
	movq	(%rdi,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB0_66
	.p2align	4, 0x90
.LBB0_64:                               # %.lr.ph.i.i.i57
                                        #   Parent Loop BB0_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbp
	movq	%rax, %rdi
	callq	_ZdlPv
	testq	%rbp, %rbp
	movq	%rbp, %rax
	jne	.LBB0_64
# BB#65:                                # %._crit_edge.loopexit.i.i.i59
                                        #   in Loop: Header=BB0_63 Depth=1
	movq	16(%rsp), %rdi
.LBB0_66:                               # %._crit_edge.i.i.i60
                                        #   in Loop: Header=BB0_63 Depth=1
	movq	$0, (%rdi,%rbx,8)
	incq	%rbx
	movq	16(%rsp), %rdi
	movq	24(%rsp), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.LBB0_63
.LBB0_67:                               # %._crit_edge17.i.i.i53
	movq	$0, 40(%rsp)
	testq	%rdi, %rdi
	jne	.LBB0_69
	jmp	.LBB0_70
.LBB0_60:                               # %._ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE5clearEv.exit_crit_edge.i.i49
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_70
.LBB0_69:
	callq	_ZdlPv
.LBB0_70:                               # %_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEED2Ev.exit62
	xorl	%eax, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_32:
.Ltmp47:
	callq	_ZSt16__throw_bad_castv
.Ltmp48:
# BB#41:                                # %.noexc88
.LBB0_73:
.Ltmp34:
	jmp	.LBB0_75
.LBB0_72:
.Ltmp27:
	jmp	.LBB0_75
.LBB0_71:
.Ltmp20:
	jmp	.LBB0_75
.LBB0_11:                               # %.body
.Ltmp5:
	movq	%rax, %r14
	cmpq	$0, 40(%rsp)
	jne	.LBB0_88
	jmp	.LBB0_87
.LBB0_98:
.Ltmp2:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_74:
.Ltmp49:
	jmp	.LBB0_75
.LBB0_40:
.Ltmp13:
	jmp	.LBB0_75
.LBB0_12:
.Ltmp8:
.LBB0_75:
	movq	%rax, %r14
	cmpq	$0, 80(%rsp)
	je	.LBB0_76
# BB#77:                                # %.preheader.i.i.i34
	movq	56(%rsp), %rax
	movq	64(%rsp), %rdi
	cmpq	%rax, %rdi
	je	.LBB0_83
# BB#78:                                # %.lr.ph16.i.i.i35
	xorl	%ebp, %ebp
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB0_79:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_80 Depth 2
	movq	(%rdi,%rbp,8), %rax
	testq	%rax, %rax
	je	.LBB0_82
	.p2align	4, 0x90
.LBB0_80:                               # %.lr.ph.i.i.i41
                                        #   Parent Loop BB0_79 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbx
	movq	%rax, %rdi
	callq	_ZdlPv
	testq	%rbx, %rbx
	movq	%rbx, %rax
	jne	.LBB0_80
# BB#81:                                # %._crit_edge.loopexit.i.i.i43
                                        #   in Loop: Header=BB0_79 Depth=1
	movq	56(%rsp), %rdi
.LBB0_82:                               # %._crit_edge.i.i.i44
                                        #   in Loop: Header=BB0_79 Depth=1
	movq	$0, (%rdi,%rbp,8)
	incq	%rbp
	movq	56(%rsp), %rdi
	movq	64(%rsp), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbp
	jb	.LBB0_79
.LBB0_83:                               # %._crit_edge17.i.i.i37
	movq	$0, 80(%rsp)
	testq	%rdi, %rdi
	jne	.LBB0_85
	jmp	.LBB0_86
.LBB0_76:                               # %._ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE5clearEv.exit_crit_edge.i.i33
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_86
.LBB0_85:
	callq	_ZdlPv
.LBB0_86:
	cmpq	$0, 40(%rsp)
	je	.LBB0_87
.LBB0_88:                               # %.preheader.i.i.i
	movq	16(%rsp), %rax
	movq	24(%rsp), %rdi
	cmpq	%rax, %rdi
	je	.LBB0_94
# BB#89:                                # %.lr.ph16.i.i.i
	xorl	%ebp, %ebp
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB0_90:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_91 Depth 2
	movq	(%rdi,%rbp,8), %rax
	testq	%rax, %rax
	je	.LBB0_93
	.p2align	4, 0x90
.LBB0_91:                               # %.lr.ph.i.i.i
                                        #   Parent Loop BB0_90 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbx
	movq	%rax, %rdi
	callq	_ZdlPv
	testq	%rbx, %rbx
	movq	%rbx, %rax
	jne	.LBB0_91
# BB#92:                                # %._crit_edge.loopexit.i.i.i
                                        #   in Loop: Header=BB0_90 Depth=1
	movq	16(%rsp), %rdi
.LBB0_93:                               # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB0_90 Depth=1
	movq	$0, (%rdi,%rbp,8)
	incq	%rbp
	movq	16(%rsp), %rdi
	movq	24(%rsp), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbp
	jb	.LBB0_90
.LBB0_94:                               # %._crit_edge17.i.i.i
	movq	$0, 40(%rsp)
	testq	%rdi, %rdi
	je	.LBB0_97
.LBB0_96:
	callq	_ZdlPv
.LBB0_97:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_87:                               # %._ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE5clearEv.exit_crit_edge.i.i
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_96
	jmp	.LBB0_97
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\367\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp12-.Ltmp9          #   Call between .Ltmp9 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp19-.Ltmp14         #   Call between .Ltmp14 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp26-.Ltmp21         #   Call between .Ltmp21 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin0   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp33-.Ltmp28         #   Call between .Ltmp28 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin0   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp48-.Ltmp35         #   Call between .Ltmp35 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin0   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Lfunc_end0-.Ltmp48     #   Call between .Ltmp48 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_,"axG",@progbits,_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_,comdat
	.weak	_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_
	.p2align	4, 0x90
	.type	_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_,@function
_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_: # @_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -48
.Lcfi19:
	.cfi_offset %r12, -40
.Lcfi20:
	.cfi_offset %r13, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%rsi), %r12
	movq	32(%r14), %rsi
	incq	%rsi
	callq	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm
	movq	8(%r14), %rcx
	movq	16(%r14), %rsi
	subq	%rcx, %rsi
	sarq	$3, %rsi
	movb	(%r12), %dl
	testb	%dl, %dl
	je	.LBB1_1
# BB#2:                                 # %.lr.ph.i.i.i.i.i.i.preheader
	leaq	1(%r12), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph.i.i.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rax,%rax,4), %rbx
	movsbq	%dl, %rax
	addq	%rbx, %rax
	movzbl	(%rdi), %edx
	incq	%rdi
	testb	%dl, %dl
	jne	.LBB1_3
	jmp	.LBB1_4
.LBB1_1:
	xorl	%eax, %eax
.LBB1_4:                                # %_ZNK9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE10_M_bkt_numERKS5_.exit.i
	xorl	%edx, %edx
	divq	%rsi
	movq	%rdx, %r15
	movq	(%rcx,%r15,8), %r13
	testq	%r13, %r13
	je	.LBB1_9
# BB#5:                                 # %.lr.ph.i.preheader
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_7
# BB#8:                                 #   in Loop: Header=BB1_6 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_6
.LBB1_9:                                # %._crit_edge.i
	movl	$24, %edi
	callq	_Znwm
	movq	%r12, 8(%rax)
	movl	$0, 16(%rax)
	movq	%r13, (%rax)
	movq	8(%r14), %rcx
	movq	%rax, (%rcx,%r15,8)
	leaq	8(%rax), %rbx
	incq	32(%r14)
	jmp	.LBB1_10
.LBB1_7:
	addq	$8, %rbx
.LBB1_10:                               # %_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE14find_or_insertERKS5_.exit
	addq	$8, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_, .Lfunc_end1-_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_
	.cfi_endproc

	.section	.text._ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E,"axG",@progbits,_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E,comdat
	.weak	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E
	.p2align	4, 0x90
	.type	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E,@function
_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E: # @_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 48
.Lcfi28:
	.cfi_offset %rbx, -40
.Lcfi29:
	.cfi_offset %r12, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	8(%r15), %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%r15)
	movups	%xmm0, 8(%r15)
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE, %eax
	movl	$29, %ecx
	jmp	.LBB2_1
.LBB2_3:                                #   in Loop: Header=BB2_1 Depth=1
	leaq	(%rax,%rcx,8), %rax
	addq	$8, %rax
	decq	%rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	testq	%rdx, %rdx
	jle	.LBB2_4
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	movq	%rdx, %rcx
	shrq	%rcx
	cmpq	%rsi, (%rax,%rcx,8)
	jae	.LBB2_1
	jmp	.LBB2_3
.LBB2_4:                                # %_ZNK9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE12_M_next_sizeEm.exit.i
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE+232, %ecx
	cmpq	%rcx, %rax
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE+224, %ecx
	cmovneq	%rax, %rcx
	movq	(%rcx), %rbx
	movq	%rbx, %rax
	shrq	$61, %rax
	jne	.LBB2_5
# BB#7:
	testq	%rbx, %rbx
	je	.LBB2_8
# BB#9:                                 # %_ZNSt12_Vector_baseIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE11_M_allocateEm.exit.i.i
	leaq	(,%rbx,8), %rdi
.Ltmp50:
	callq	_Znwm
	movq	%rax, %r12
.Ltmp51:
# BB#10:                                # %_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE20_M_allocate_and_copyIPS8_EESC_mT_SD_.exit.i
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_12
# BB#11:
	callq	_ZdlPv
.LBB2_12:                               # %_ZNSt12_Vector_baseIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE13_M_deallocateEPS8_m.exit.i
	movq	%r12, 8(%r15)
	movq	%r12, 16(%r15)
	leaq	(%r12,%rbx,8), %rax
	movq	%rax, 24(%r15)
	jmp	.LBB2_13
.LBB2_8:
	xorl	%r12d, %r12d
.LBB2_13:                               # %.noexc
	movq	$0, (%rsp)
.Ltmp52:
	movq	%rsp, %rcx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_
.Ltmp53:
# BB#14:
	movq	$0, 32(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB2_5:
.Ltmp54:
	movl	$.L.str.4, %edi
	callq	_ZSt20__throw_length_errorPKc
.Ltmp55:
# BB#6:                                 # %.noexc9
.LBB2_15:
.Ltmp56:
	movq	%rax, %rbx
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_17
# BB#16:
	callq	_ZdlPv
.LBB2_17:                               # %_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E, .Lfunc_end2-_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp50-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp55-.Ltmp50         #   Call between .Ltmp50 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin1   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end2-.Ltmp55     #   Call between .Ltmp55 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_,"axG",@progbits,_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_,comdat
	.weak	_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_
	.p2align	4, 0x90
	.type	_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_,@function
_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_: # @_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB3_78
# BB#1:
	movq	8(%r15), %r12
	movq	16(%r15), %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%r14, %rax
	jae	.LBB3_2
# BB#52:
	movabsq	$2305843009213693951, %rax # imm = 0x1FFFFFFFFFFFFFFF
	movq	%r15, %rbp
	movq	(%r15), %r15
	subq	%r15, %r12
	sarq	$3, %r12
	movq	%rax, %rcx
	subq	%r12, %rcx
	cmpq	%r14, %rcx
	jb	.LBB3_79
# BB#53:                                # %_ZNKSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE12_M_check_lenEmS4_.exit
	cmpq	%r14, %r12
	movq	%r12, %rcx
	cmovbq	%r14, %rcx
	leaq	(%rcx,%r12), %rdx
	cmpq	%rax, %rdx
	cmovaq	%rax, %rdx
	addq	%r12, %rcx
	cmovbq	%rax, %rdx
	testq	%rdx, %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	je	.LBB3_54
# BB#55:
	cmpq	%rax, %rdx
	ja	.LBB3_80
# BB#56:                                # %_ZN9__gnu_cxx14__alloc_traitsISaIPNS_15_Hashtable_nodeISt4pairIKPKciEEEEE8allocateERS9_m.exit.i
	leaq	(,%rdx,8), %rdi
	callq	_Znwm
	movq	%rax, %r12
	jmp	.LBB3_57
.LBB3_2:
	movabsq	$4611686018427387900, %r9 # imm = 0x3FFFFFFFFFFFFFFC
	movq	(%r13), %rbp
	movq	%r12, %rdx
	subq	%rbx, %rdx
	movq	%rdx, %r13
	sarq	$3, %r13
	cmpq	%r14, %r13
	jbe	.LBB3_21
# BB#3:
	leaq	(,%r14,8), %rdx
	movq	%r12, %r13
	subq	%rdx, %r13
	testq	%rdx, %rdx
	je	.LBB3_4
# BB#5:
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	memmove
	movabsq	$4611686018427387900, %r9 # imm = 0x3FFFFFFFFFFFFFFC
	movq	8(%r15), %rax
	jmp	.LBB3_6
.LBB3_21:
	subq	%r13, %r14
	je	.LBB3_22
# BB#23:                                # %.lr.ph.i.i.i.i.i64.preheader
	cmpq	$3, %r14
	movq	%r14, %rsi
	movq	%r12, %rax
	jbe	.LBB3_35
# BB#24:                                # %min.iters.checked
	movq	%r14, %r8
	andq	$-4, %r8
	movq	%r14, %rcx
	andq	$-4, %rcx
	je	.LBB3_25
# BB#26:                                # %vector.ph
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%rcx), %rax
	movl	%eax, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB3_27
# BB#28:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_29:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%r12,%rdi,8)
	movdqu	%xmm0, 16(%r12,%rdi,8)
	addq	$4, %rdi
	incq	%rsi
	jne	.LBB3_29
	jmp	.LBB3_30
.LBB3_54:
	xorl	%r12d, %r12d
.LBB3_57:                               # %_ZNSt12_Vector_baseIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE11_M_allocateEm.exit
	movq	%rbx, %rdi
	subq	%r15, %rdi
	sarq	$3, %rdi
	leaq	(%r12,%rdi,8), %rax
	movq	(%r13), %rcx
	cmpq	$4, %r14
	jae	.LBB3_59
# BB#58:
	movq	%r14, %rdx
	movq	%rbp, %r15
	jmp	.LBB3_70
.LBB3_59:                               # %min.iters.checked138
	movq	%r14, %r8
	andq	$-4, %r8
	movq	%r14, %r9
	andq	$-4, %r9
	movq	%rbp, %r15
	je	.LBB3_60
# BB#61:                                # %vector.ph142
	movd	%rcx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %r10
	movl	%r10d, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$7, %rdx
	je	.LBB3_62
# BB#63:                                # %vector.body134.prol.preheader
	leaq	16(%r12,%rdi,8), %rbp
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_64:                               # %vector.body134.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbp,%rsi,8)
	movdqu	%xmm0, (%rbp,%rsi,8)
	addq	$4, %rsi
	incq	%rdx
	jne	.LBB3_64
	jmp	.LBB3_65
.LBB3_4:
	movq	%r12, %rax
.LBB3_6:                                # %_ZSt22__uninitialized_move_aIPPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEES9_SaIS8_EET0_T_SC_SB_RT1_.exit
	leaq	(%rax,%r14,8), %rax
	movq	%rax, 8(%r15)
	subq	%rbx, %r13
	movq	%r13, %rax
	sarq	$3, %rax
	je	.LBB3_8
# BB#7:
	shlq	$3, %rax
	subq	%rax, %r12
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	%r9, %r15
	callq	memmove
	movq	%r15, %r9
.LBB3_8:                                # %.lr.ph.i.i68.preheader
	leaq	-8(,%r14,8), %rax
	shrq	$3, %rax
	incq	%rax
	cmpq	$4, %rax
	movq	%rbx, %rcx
	jb	.LBB3_19
# BB#9:                                 # %min.iters.checked120
	andq	%rax, %r9
	movq	%rbx, %rcx
	je	.LBB3_19
# BB#10:                                # %vector.ph124
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %rcx
	movl	%ecx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB3_11
# BB#12:                                # %vector.body116.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_13:                               # %vector.body116.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rbx,%rdx,8)
	movdqu	%xmm0, 16(%rbx,%rdx,8)
	addq	$4, %rdx
	incq	%rsi
	jne	.LBB3_13
	jmp	.LBB3_14
.LBB3_22:
	movq	%r12, %rdi
	jmp	.LBB3_37
.LBB3_60:
	movq	%r14, %rdx
	jmp	.LBB3_70
.LBB3_62:
	xorl	%esi, %esi
.LBB3_65:                               # %vector.body134.prol.loopexit
	cmpq	$28, %r10
	jb	.LBB3_68
# BB#66:                                # %vector.ph142.new
	movq	%r9, %rdx
	subq	%rsi, %rdx
	addq	%rsi, %rdi
	leaq	240(%r12,%rdi,8), %rsi
	.p2align	4, 0x90
.LBB3_67:                               # %vector.body134
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rsi)
	movdqu	%xmm0, -224(%rsi)
	movdqu	%xmm0, -208(%rsi)
	movdqu	%xmm0, -192(%rsi)
	movdqu	%xmm0, -176(%rsi)
	movdqu	%xmm0, -160(%rsi)
	movdqu	%xmm0, -144(%rsi)
	movdqu	%xmm0, -128(%rsi)
	movdqu	%xmm0, -112(%rsi)
	movdqu	%xmm0, -96(%rsi)
	movdqu	%xmm0, -80(%rsi)
	movdqu	%xmm0, -64(%rsi)
	movdqu	%xmm0, -48(%rsi)
	movdqu	%xmm0, -32(%rsi)
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	addq	$256, %rsi              # imm = 0x100
	addq	$-32, %rdx
	jne	.LBB3_67
.LBB3_68:                               # %middle.block135
	cmpq	%r14, %r9
	je	.LBB3_71
# BB#69:
	movq	%r14, %rdx
	subq	%r8, %rdx
	leaq	(%rax,%r9,8), %rax
	.p2align	4, 0x90
.LBB3_70:                               # %.lr.ph.i.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rax)
	addq	$8, %rax
	decq	%rdx
	jne	.LBB3_70
.LBB3_71:                               # %_ZSt24__uninitialized_fill_n_aIPPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEEmS8_S8_ET_SA_T0_RKT1_RSaIT2_E.exit
	movq	(%r15), %r13
	movq	%rbx, %rdx
	subq	%r13, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB3_73
# BB#72:
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	memmove
.LBB3_73:
	leaq	(%r12,%rbp,8), %rax
	leaq	(%rax,%r14,8), %r14
	movq	8(%r15), %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB3_75
# BB#74:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	memmove
.LBB3_75:
	leaq	(%r14,%rbp,8), %rbx
	testq	%r13, %r13
	je	.LBB3_77
# BB#76:
	movq	%r13, %rdi
	callq	_ZdlPv
.LBB3_77:                               # %_ZNSt12_Vector_baseIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE13_M_deallocateEPS8_m.exit57
	movq	%r12, (%r15)
	movq	%rbx, 8(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%r12,%rax,8), %rax
	movq	%rax, 16(%r15)
	jmp	.LBB3_78
.LBB3_25:
	movq	%r14, %rsi
	movq	%r12, %rax
	jmp	.LBB3_35
.LBB3_11:
	xorl	%edx, %edx
.LBB3_14:                               # %vector.body116.prol.loopexit
	cmpq	$28, %rcx
	jb	.LBB3_17
# BB#15:                                # %vector.ph124.new
	movq	%r9, %rcx
	subq	%rdx, %rcx
	leaq	240(%rbx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB3_16:                               # %vector.body116
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdx)
	movdqu	%xmm0, -224(%rdx)
	movdqu	%xmm0, -208(%rdx)
	movdqu	%xmm0, -192(%rdx)
	movdqu	%xmm0, -176(%rdx)
	movdqu	%xmm0, -160(%rdx)
	movdqu	%xmm0, -144(%rdx)
	movdqu	%xmm0, -128(%rdx)
	movdqu	%xmm0, -112(%rdx)
	movdqu	%xmm0, -96(%rdx)
	movdqu	%xmm0, -80(%rdx)
	movdqu	%xmm0, -64(%rdx)
	movdqu	%xmm0, -48(%rdx)
	movdqu	%xmm0, -32(%rdx)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-32, %rcx
	jne	.LBB3_16
.LBB3_17:                               # %middle.block117
	cmpq	%r9, %rax
	je	.LBB3_78
# BB#18:
	leaq	(%rbx,%r9,8), %rcx
.LBB3_19:                               # %.lr.ph.i.i68.preheader159
	leaq	(%rbx,%r14,8), %rax
	.p2align	4, 0x90
.LBB3_20:                               # %.lr.ph.i.i68
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB3_20
	jmp	.LBB3_78
.LBB3_27:
	xorl	%edi, %edi
.LBB3_30:                               # %vector.body.prol.loopexit
	cmpq	$28, %rax
	jb	.LBB3_33
# BB#31:                                # %vector.ph.new
	movq	%rcx, %rsi
	subq	%rdi, %rsi
	leaq	240(%r12,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB3_32:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdi)
	movdqu	%xmm0, -224(%rdi)
	movdqu	%xmm0, -208(%rdi)
	movdqu	%xmm0, -192(%rdi)
	movdqu	%xmm0, -176(%rdi)
	movdqu	%xmm0, -160(%rdi)
	movdqu	%xmm0, -144(%rdi)
	movdqu	%xmm0, -128(%rdi)
	movdqu	%xmm0, -112(%rdi)
	movdqu	%xmm0, -96(%rdi)
	movdqu	%xmm0, -80(%rdi)
	movdqu	%xmm0, -64(%rdi)
	movdqu	%xmm0, -48(%rdi)
	movdqu	%xmm0, -32(%rdi)
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	addq	$256, %rdi              # imm = 0x100
	addq	$-32, %rsi
	jne	.LBB3_32
.LBB3_33:                               # %middle.block
	cmpq	%rcx, %r14
	je	.LBB3_36
# BB#34:
	movq	%r14, %rsi
	subq	%r8, %rsi
	leaq	(%r12,%rcx,8), %rax
	.p2align	4, 0x90
.LBB3_35:                               # %.lr.ph.i.i.i.i.i64
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rax)
	addq	$8, %rax
	decq	%rsi
	jne	.LBB3_35
.LBB3_36:                               # %._crit_edge.loopexit.i.i.i.i.i61
	leaq	(%r12,%r14,8), %rdi
.LBB3_37:                               # %_ZSt24__uninitialized_fill_n_aIPPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEEmS8_S8_ET_SA_T0_RKT1_RSaIT2_E.exit66
	movq	%rdi, 8(%r15)
	testq	%r13, %r13
	je	.LBB3_39
# BB#38:
	movq	%rbx, %rsi
	movq	%r9, %r14
	callq	memmove
	movq	%r14, %r9
	movq	8(%r15), %rdi
.LBB3_39:                               # %_ZSt22__uninitialized_move_aIPPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEES9_SaIS8_EET0_T_SC_SB_RT1_.exit59
	leaq	(%rdi,%r13,8), %rax
	movq	%rax, 8(%r15)
	cmpq	%rbx, %r12
	je	.LBB3_78
# BB#40:                                # %.lr.ph.i.i.preheader
	leaq	-8(%r12), %rax
	subq	%rbx, %rax
	shrq	$3, %rax
	incq	%rax
	cmpq	$4, %rax
	jb	.LBB3_51
# BB#41:                                # %min.iters.checked102
	andq	%rax, %r9
	je	.LBB3_51
# BB#42:                                # %vector.ph106
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %rcx
	movl	%ecx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB3_43
# BB#44:                                # %vector.body96.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_45:                               # %vector.body96.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rbx,%rdx,8)
	movdqu	%xmm0, 16(%rbx,%rdx,8)
	addq	$4, %rdx
	incq	%rsi
	jne	.LBB3_45
	jmp	.LBB3_46
.LBB3_43:
	xorl	%edx, %edx
.LBB3_46:                               # %vector.body96.prol.loopexit
	cmpq	$28, %rcx
	jb	.LBB3_49
# BB#47:                                # %vector.ph106.new
	movq	%r9, %rcx
	subq	%rdx, %rcx
	leaq	240(%rbx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB3_48:                               # %vector.body96
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdx)
	movdqu	%xmm0, -224(%rdx)
	movdqu	%xmm0, -208(%rdx)
	movdqu	%xmm0, -192(%rdx)
	movdqu	%xmm0, -176(%rdx)
	movdqu	%xmm0, -160(%rdx)
	movdqu	%xmm0, -144(%rdx)
	movdqu	%xmm0, -128(%rdx)
	movdqu	%xmm0, -112(%rdx)
	movdqu	%xmm0, -96(%rdx)
	movdqu	%xmm0, -80(%rdx)
	movdqu	%xmm0, -64(%rdx)
	movdqu	%xmm0, -48(%rdx)
	movdqu	%xmm0, -32(%rdx)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-32, %rcx
	jne	.LBB3_48
.LBB3_49:                               # %middle.block97
	cmpq	%r9, %rax
	je	.LBB3_78
# BB#50:
	leaq	(%rbx,%r9,8), %rbx
	.p2align	4, 0x90
.LBB3_51:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.LBB3_51
.LBB3_78:                               # %_ZSt4fillIPPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEES8_EvT_SA_RKT0_.exit69
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_79:
	movl	$.L.str.5, %edi
	callq	_ZSt20__throw_length_errorPKc
.LBB3_80:
	callq	_ZSt17__throw_bad_allocv
.Lfunc_end3:
	.size	_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_, .Lfunc_end3-_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_
	.cfi_endproc

	.section	.text._ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm,"axG",@progbits,_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm,comdat
	.weak	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm
	.p2align	4, 0x90
	.type	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm,@function
_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm: # @_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 64
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	16(%r14), %r12
	subq	8(%r14), %r12
	sarq	$3, %r12
	cmpq	%rsi, %r12
	jae	.LBB4_18
# BB#1:                                 # %.outer.i.i.i.i.preheader
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE, %eax
	movl	$29, %ecx
	jmp	.LBB4_2
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	leaq	(%rax,%rcx,8), %rax
	addq	$8, %rax
	decq	%rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	testq	%rdx, %rdx
	jle	.LBB4_5
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	%rdx, %rcx
	shrq	%rcx
	cmpq	%rsi, (%rax,%rcx,8)
	jae	.LBB4_2
	jmp	.LBB4_4
.LBB4_5:                                # %_ZNK9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE12_M_next_sizeEm.exit
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE+232, %ecx
	cmpq	%rcx, %rax
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE+224, %ecx
	cmovneq	%rax, %rcx
	movq	(%rcx), %r13
	cmpq	%r12, %r13
	jbe	.LBB4_18
# BB#6:
	movq	%r13, %rax
	shrq	$61, %rax
	jne	.LBB4_19
# BB#7:                                 # %_ZN9__gnu_cxx14__alloc_traitsISaIPNS_15_Hashtable_nodeISt4pairIKPKciEEEEE8allocateERS9_m.exit.i.i.i.i
	leaq	(,%r13,8), %rbx
	movq	%rbx, %rdi
	callq	_Znwm
	movq	%rax, %r15
	leaq	(%r15,%r13,8), %rbp
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	memset
	testq	%r12, %r12
	movq	8(%r14), %rdi
	je	.LBB4_8
# BB#9:                                 # %.preheader.preheader
	xorl	%ecx, %ecx
	movq	%rbp, %r8
	jmp	.LBB4_10
	.p2align	4, 0x90
.LBB4_15:                               # %.loopexit
                                        #   in Loop: Header=BB4_10 Depth=1
	xorl	%edx, %edx
	divq	%r13
	movq	(%rsi), %rax
	movq	%rax, (%rdi,%rcx,8)
	movq	(%r15,%rdx,8), %rax
	movq	%rax, (%rsi)
	movq	%rsi, (%r15,%rdx,8)
	movq	8(%r14), %rdi
.LBB4_10:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_14 Depth 2
	movq	(%rdi,%rcx,8), %rsi
	testq	%rsi, %rsi
	je	.LBB4_16
# BB#11:                                # %.lr.ph
                                        #   in Loop: Header=BB4_10 Depth=1
	movq	8(%rsi), %rdx
	movb	(%rdx), %bl
	testb	%bl, %bl
	je	.LBB4_12
# BB#13:                                # %.lr.ph.i.i.i.i.preheader
                                        #   in Loop: Header=BB4_10 Depth=1
	incq	%rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_14:                               # %.lr.ph.i.i.i.i
                                        #   Parent Loop BB4_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rax,4), %rbp
	movsbq	%bl, %rax
	addq	%rbp, %rax
	movzbl	(%rdx), %ebx
	incq	%rdx
	testb	%bl, %bl
	jne	.LBB4_14
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_12:                               #   in Loop: Header=BB4_10 Depth=1
	xorl	%eax, %eax
	jmp	.LBB4_15
.LBB4_16:                               # %._crit_edge
                                        #   in Loop: Header=BB4_10 Depth=1
	incq	%rcx
	cmpq	%r12, %rcx
	jne	.LBB4_10
	jmp	.LBB4_17
.LBB4_8:
	movq	%rbp, %r8
.LBB4_17:                               # %._crit_edge85
	movq	%r15, 8(%r14)
	movq	%r8, 16(%r14)
	movq	%r8, 24(%r14)
	testq	%rdi, %rdi
	je	.LBB4_18
# BB#20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZdlPv                  # TAILCALL
.LBB4_18:                               # %_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_19:                               # %.noexc.i.i
	callq	_ZSt17__throw_bad_allocv
.Lfunc_end4:
	.size	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm, .Lfunc_end4-_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_hash2.ii,@function
_GLOBAL__sub_I_hash2.ii:                # @_GLOBAL__sub_I_hash2.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi58:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end5:
	.size	_GLOBAL__sub_I_hash2.ii, .Lfunc_end5-_GLOBAL__sub_I_hash2.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"foo_%d"
	.size	.L.str, 7

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"foo_1"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" "
	.size	.L.str.2, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"foo_9999"
	.size	.L.str.3, 9

	.type	_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE,@object # @_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE
	.section	.rodata._ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE,"aG",@progbits,_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE,comdat
	.weak	_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE
	.p2align	4
_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE:
	.quad	5                       # 0x5
	.quad	53                      # 0x35
	.quad	97                      # 0x61
	.quad	193                     # 0xc1
	.quad	389                     # 0x185
	.quad	769                     # 0x301
	.quad	1543                    # 0x607
	.quad	3079                    # 0xc07
	.quad	6151                    # 0x1807
	.quad	12289                   # 0x3001
	.quad	24593                   # 0x6011
	.quad	49157                   # 0xc005
	.quad	98317                   # 0x1800d
	.quad	196613                  # 0x30005
	.quad	393241                  # 0x60019
	.quad	786433                  # 0xc0001
	.quad	1572869                 # 0x180005
	.quad	3145739                 # 0x30000b
	.quad	6291469                 # 0x60000d
	.quad	12582917                # 0xc00005
	.quad	25165843                # 0x1800013
	.quad	50331653                # 0x3000005
	.quad	100663319               # 0x6000017
	.quad	201326611               # 0xc000013
	.quad	402653189               # 0x18000005
	.quad	805306457               # 0x30000059
	.quad	1610612741              # 0x60000005
	.quad	3221225473              # 0xc0000001
	.quad	4294967291              # 0xfffffffb
	.size	_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE, 232

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"vector::reserve"
	.size	.L.str.4, 16

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"vector::_M_fill_insert"
	.size	.L.str.5, 23

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_hash2.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
