	.text
	.file	"parse_settings.bc"
	.globl	parse_settings
	.p2align	4, 0x90
	.type	parse_settings,@function
parse_settings:                         # @parse_settings
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	$100, %edi
	callq	malloc
	movq	%rax, %r12
	pcmpeqd	%xmm0, %xmm0
	movdqa	%xmm0, settings+48(%rip)
	movdqa	%xmm0, settings+32(%rip)
	movdqa	%xmm0, settings+16(%rip)
	movdqa	%xmm0, settings(%rip)
	movl	$-1, settings+64(%rip)
	movl	$.L.str.17, %esi
	movq	%r15, %rdi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB0_1
# BB#7:
	movq	stderr(%rip), %rdi
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	fprintf
	movl	$1, %edi
	callq	exit
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	$100, %esi
	movq	%r12, %rdi
	movq	%r14, %rdx
	callq	fgets
	cmpb	$35, (%r12)
	je	.LBB0_1
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	movl	$.L.str.19, %esi
	movq	%r12, %rdi
	callq	strtok
	movq	$-68, %rbx
	.p2align	4, 0x90
.LBB0_4:                                #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	pattern+136(%rbx,%rbx), %rdi
	movq	%r12, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=2
	movq	%r12, %rdi
	callq	strlen
	leaq	1(%r12,%rax), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, settings+68(%rbx)
.LBB0_6:                                #   in Loop: Header=BB0_4 Depth=2
	addq	$4, %rbx
	jne	.LBB0_4
.LBB0_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
	movq	%r14, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB0_2
# BB#8:                                 # %._crit_edge
	movq	%r12, %rdi
	callq	free
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	parse_settings, .Lfunc_end0-parse_settings
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"LOAD_PENALTY"
	.size	.L.str, 13

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"STORE_PENALTY"
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"DATA"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"NAME"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"SPEED"
	.size	.L.str.4, 6

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"EARLY_SPEED"
	.size	.L.str.5, 12

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"NO_FORWARDING"
	.size	.L.str.6, 14

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"INST_MIX"
	.size	.L.str.7, 9

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"QUIET"
	.size	.L.str.8, 6

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"EPOCH_LENGTH"
	.size	.L.str.9, 13

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"KERNEL"
	.size	.L.str.10, 7

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"CPULIMIT"
	.size	.L.str.11, 9

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"CONFIGS"
	.size	.L.str.12, 8

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"SHOW_SPEEDUP"
	.size	.L.str.13, 13

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"THREAD_PEN"
	.size	.L.str.14, 11

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"COMMIT_PEN"
	.size	.L.str.15, 11

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"EPOCH_LENGTH_AS_NUM_EPOCHS"
	.size	.L.str.16, 27

	.type	pattern,@object         # @pattern
	.data
	.globl	pattern
	.p2align	4
pattern:
	.quad	.L.str
	.quad	.L.str.1
	.quad	.L.str.2
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	.L.str.6
	.quad	.L.str.7
	.quad	.L.str.8
	.quad	.L.str.9
	.quad	.L.str.10
	.quad	.L.str.11
	.quad	.L.str.12
	.quad	.L.str.13
	.quad	.L.str.14
	.quad	.L.str.15
	.quad	.L.str.16
	.size	pattern, 136

	.type	settings,@object        # @settings
	.comm	settings,68,16
	.type	.L.str.17,@object       # @.str.17
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.17:
	.asciz	"r"
	.size	.L.str.17, 2

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Could not find file %s\n"
	.size	.L.str.18, 24

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	" "
	.size	.L.str.19, 2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
