	.text
	.file	"GzHandler.bc"
	.globl	_ZN8NArchive3NGz5CItem10ReadHeaderEPN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz5CItem10ReadHeaderEPN9NCompress8NDeflate8NDecoder9CCOMCoderE,@function
_ZN8NArchive3NGz5CItem10ReadHeaderEPN9NCompress8NDeflate8NDecoder9CCOMCoderE: # @_ZN8NArchive3NGz5CItem10ReadHeaderEPN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movl	$0, 24(%r15)
	movq	16(%r15), %rax
	movb	$0, (%rax)
	movl	$0, 40(%r15)
	movq	32(%r15), %rax
	movb	$0, (%rax)
	leaq	112(%rbx), %r14
	movl	104(%rbx), %eax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_2 Depth 2
	cmpl	$8, %eax
	jb	.LBB0_8
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph.i.i.i.i
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB0_5
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=2
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB0_6
# BB#4:                                 # %._crit_edge.i.i.i.i.i
                                        #   in Loop: Header=BB0_2 Depth=2
	movq	(%r14), %rax
.LBB0_5:                                # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i.i
                                        #   in Loop: Header=BB0_2 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_6:                                # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i.i
                                        #   in Loop: Header=BB0_2 Depth=2
	incl	160(%rbx)
	movb	$-1, %al
.LBB0_7:                                #   in Loop: Header=BB0_2 Depth=2
	movzbl	%al, %eax
	movl	104(%rbx), %edx
	movl	$32, %ecx
	subl	%edx, %ecx
	movl	%eax, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	164(%rbx), %esi
	movl	%esi, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rax), %eax
	orl	%ecx, %eax
	movl	%eax, 108(%rbx)
	addl	$-8, %edx
	movl	%edx, 104(%rbx)
	cmpl	$7, %edx
	ja	.LBB0_2
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_8:                                # %._ZN5NBitl8CDecoderI9CInBufferE9NormalizeEv.exit_crit_edge.i.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	164(%rbx), %esi
	movl	%eax, %edx
.LBB0_9:                                # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadByteEv.exit.i
                                        #   in Loop: Header=BB0_1 Depth=1
	leal	8(%rdx), %eax
	movl	%eax, 104(%rbx)
	movl	%esi, %ecx
	shrl	$8, %ecx
	movl	%ecx, 164(%rbx)
	movb	%sil, 14(%rsp,%rbp)
	incq	%rbp
	cmpq	$10, %rbp
	jne	.LBB0_1
# BB#10:                                # %._crit_edge.i
	movl	160(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB0_12
# BB#11:                                # %_ZN8NArchive3NGzL9ReadBytesEPN9NCompress8NDeflate8NDecoder9CCOMCoderEPhj.exit
	movl	$24, %esi
	subl	%edx, %esi
	leal	(,%rcx,8), %edx
	movb	$1, %r13b
	cmpl	%edx, %esi
	jb	.LBB0_96
.LBB0_12:                               # %_ZN8NArchive3NGzL9ReadBytesEPN9NCompress8NDeflate8NDecoder9CCOMCoderEPhj.exit.thread
	movb	$1, %r13b
	movzwl	14(%rsp), %edx
	cmpl	$35615, %edx            # imm = 0x8B1F
	jne	.LBB0_96
# BB#13:
	movb	16(%rsp), %dl
	movb	%dl, (%r15)
	cmpb	$8, %dl
	jne	.LBB0_96
# BB#14:
	movb	17(%rsp), %dil
	movb	%dil, 1(%r15)
	movl	18(%rsp), %esi
	movl	%esi, 4(%r15)
	movb	22(%rsp), %dl
	movb	%dl, 2(%r15)
	movb	23(%rsp), %dl
	movb	%dl, 3(%r15)
	testb	$4, %dil
	jne	.LBB0_29
.LBB0_15:                               # %.thread
	movb	1(%r15), %dl
	testb	$8, %dl
	je	.LBB0_59
# BB#16:
	leaq	16(%r15), %r12
	movl	$0, 24(%r15)
	movq	16(%r15), %rax
	movb	$0, (%rax)
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_17:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_18 Depth 2
	movl	104(%rbx), %esi
	cmpl	$8, %esi
	jb	.LBB0_24
	.p2align	4, 0x90
.LBB0_18:                               # %.lr.ph.i.i.i119
                                        #   Parent Loop BB0_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB0_21
# BB#19:                                #   in Loop: Header=BB0_18 Depth=2
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB0_22
# BB#20:                                # %._crit_edge.i.i.i.i121
                                        #   in Loop: Header=BB0_18 Depth=2
	movq	(%r14), %rax
.LBB0_21:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i122
                                        #   in Loop: Header=BB0_18 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB0_23
	.p2align	4, 0x90
.LBB0_22:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i123
                                        #   in Loop: Header=BB0_18 Depth=2
	incl	160(%rbx)
	movb	$-1, %al
.LBB0_23:                               #   in Loop: Header=BB0_18 Depth=2
	movzbl	%al, %eax
	movl	104(%rbx), %esi
	movl	$32, %ecx
	subl	%esi, %ecx
	movl	%eax, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	164(%rbx), %edx
	movl	%edx, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rax), %eax
	orl	%ecx, %eax
	movl	%eax, 108(%rbx)
	addl	$-8, %esi
	movl	%esi, 104(%rbx)
	cmpl	$7, %esi
	ja	.LBB0_18
	jmp	.LBB0_25
	.p2align	4, 0x90
.LBB0_24:                               # %._ZN5NBitl8CDecoderI9CInBufferE9NormalizeEv.exit_crit_edge.i.i118
                                        #   in Loop: Header=BB0_17 Depth=1
	movl	164(%rbx), %edx
.LBB0_25:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadByteEv.exit126
                                        #   in Loop: Header=BB0_17 Depth=1
	leal	8(%rsi), %eax
	movl	%eax, 104(%rbx)
	movl	%edx, %ecx
	shrl	$8, %ecx
	movl	%ecx, 164(%rbx)
	movl	160(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB0_27
# BB#26:                                # %_ZNK9NCompress8NDeflate8NDecoder6CCoder13InputEofErrorEv.exit.i114
                                        #   in Loop: Header=BB0_17 Depth=1
	movl	$24, %edi
	subl	%esi, %edi
	leal	(,%rcx,8), %esi
	cmpl	%esi, %edi
	jb	.LBB0_96
.LBB0_27:                               # %_ZNK9NCompress8NDeflate8NDecoder6CCoder13InputEofErrorEv.exit.thread.i115
                                        #   in Loop: Header=BB0_17 Depth=1
	testb	%dl, %dl
	je	.LBB0_58
# BB#28:                                #   in Loop: Header=BB0_17 Depth=1
	movsbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN11CStringBaseIcEpLEc
	incl	%ebp
	cmpl	$1024, %ebp             # imm = 0x400
	jb	.LBB0_17
	jmp	.LBB0_96
	.p2align	4, 0x90
.LBB0_29:                               # %.lr.ph.i.i.i105
                                        # =>This Inner Loop Header: Depth=1
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB0_32
# BB#30:                                #   in Loop: Header=BB0_29 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB0_33
# BB#31:                                # %._crit_edge.i.i.i.i107
                                        #   in Loop: Header=BB0_29 Depth=1
	movq	(%r14), %rax
.LBB0_32:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i108
                                        #   in Loop: Header=BB0_29 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB0_34
	.p2align	4, 0x90
.LBB0_33:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i109
                                        #   in Loop: Header=BB0_29 Depth=1
	incl	160(%rbx)
	movb	$-1, %al
.LBB0_34:                               #   in Loop: Header=BB0_29 Depth=1
	movzbl	%al, %esi
	movl	104(%rbx), %edx
	movl	$32, %ecx
	subl	%edx, %ecx
	movl	%esi, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	164(%rbx), %eax
	movl	%eax, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%rbx)
	leal	-8(%rdx), %ecx
	movl	%ecx, 104(%rbx)
	cmpl	$7, %ecx
	ja	.LBB0_29
# BB#35:                                # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadByteEv.exit112
	movl	160(%rbx), %ecx
	movl	%edx, 104(%rbx)
	movl	%eax, %esi
	shrl	$8, %esi
	movl	%esi, 164(%rbx)
	testl	%ecx, %ecx
	je	.LBB0_37
# BB#36:                                # %_ZNK9NCompress8NDeflate8NDecoder6CCoder13InputEofErrorEv.exit.i72
	movl	$32, %esi
	subl	%edx, %esi
	shll	$3, %ecx
	cmpl	%ecx, %esi
	jb	.LBB0_96
.LBB0_37:                               # %.lr.ph.i.i.i81
	movzbl	%al, %ebp
	.p2align	4, 0x90
.LBB0_38:                               # =>This Inner Loop Header: Depth=1
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB0_41
# BB#39:                                #   in Loop: Header=BB0_38 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB0_42
# BB#40:                                # %._crit_edge.i.i.i.i83
                                        #   in Loop: Header=BB0_38 Depth=1
	movq	(%r14), %rax
.LBB0_41:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i84
                                        #   in Loop: Header=BB0_38 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB0_43
	.p2align	4, 0x90
.LBB0_42:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i85
                                        #   in Loop: Header=BB0_38 Depth=1
	incl	160(%rbx)
	movb	$-1, %al
.LBB0_43:                               #   in Loop: Header=BB0_38 Depth=1
	movzbl	%al, %esi
	movl	104(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	164(%rbx), %edx
	movl	%edx, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%rbx)
	leal	-8(%rax), %ecx
	movl	%ecx, 104(%rbx)
	cmpl	$7, %ecx
	ja	.LBB0_38
# BB#44:                                # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadByteEv.exit88
	movl	160(%rbx), %ecx
	movl	%eax, 104(%rbx)
	movl	%edx, %esi
	shrl	$8, %esi
	movl	%esi, 164(%rbx)
	testl	%ecx, %ecx
	je	.LBB0_46
# BB#45:                                # %_ZNK9NCompress8NDeflate8NDecoder6CCoder13InputEofErrorEv.exit.1.i75
	movl	$32, %esi
	subl	%eax, %esi
	leal	(,%rcx,8), %edi
	cmpl	%edi, %esi
	jb	.LBB0_96
.LBB0_46:
	shll	$8, %edx
	movzwl	%dx, %r12d
	orl	%ebp, %r12d
	je	.LBB0_56
# BB#47:                                # %.lr.ph.i.i.i.i94.preheader.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_48:                               # %.lr.ph.i.i.i.i94
                                        # =>This Inner Loop Header: Depth=1
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB0_51
# BB#49:                                #   in Loop: Header=BB0_48 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB0_52
# BB#50:                                # %._crit_edge.i.i.i.i.i96
                                        #   in Loop: Header=BB0_48 Depth=1
	movq	(%r14), %rax
.LBB0_51:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i.i97
                                        #   in Loop: Header=BB0_48 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB0_53
	.p2align	4, 0x90
.LBB0_52:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i.i98
                                        #   in Loop: Header=BB0_48 Depth=1
	incl	160(%rbx)
	movb	$-1, %al
.LBB0_53:                               #   in Loop: Header=BB0_48 Depth=1
	movzbl	%al, %esi
	movl	104(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	164(%rbx), %edx
	movl	%edx, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%rbx)
	leal	-8(%rax), %ecx
	movl	%ecx, 104(%rbx)
	cmpl	$7, %ecx
	ja	.LBB0_48
# BB#54:                                # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadByteEv.exit.i101
                                        #   in Loop: Header=BB0_48 Depth=1
	movl	%eax, 104(%rbx)
	shrl	$8, %edx
	movl	%edx, 164(%rbx)
	incl	%ebp
	cmpl	%r12d, %ebp
	jne	.LBB0_48
# BB#55:                                # %._crit_edge.i89.loopexit
	movl	160(%rbx), %ecx
.LBB0_56:                               # %._crit_edge.i89
	testl	%ecx, %ecx
	je	.LBB0_73
# BB#57:                                # %_ZN8NArchive3NGzL9SkipBytesEPN9NCompress8NDeflate8NDecoder9CCOMCoderEj.exit
	movl	$32, %edx
	subl	%eax, %edx
	leal	(,%rcx,8), %esi
	cmpl	%esi, %edx
	jae	.LBB0_15
	jmp	.LBB0_96
.LBB0_58:                               # %_ZN8NArchive3NGzL10ReadStringEPN9NCompress8NDeflate8NDecoder9CCOMCoderER11CStringBaseIcEj.exit.thread159.loopexit
	movb	1(%r15), %dl
.LBB0_59:                               # %_ZN8NArchive3NGzL10ReadStringEPN9NCompress8NDeflate8NDecoder9CCOMCoderER11CStringBaseIcEj.exit.thread159
	testb	$16, %dl
	je	.LBB0_75
# BB#60:
	leaq	32(%r15), %r12
	movl	$0, 40(%r15)
	movq	32(%r15), %rax
	movb	$0, (%rax)
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_61:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_62 Depth 2
	movl	104(%rbx), %esi
	cmpl	$8, %esi
	jb	.LBB0_68
	.p2align	4, 0x90
.LBB0_62:                               # %.lr.ph.i.i.i136
                                        #   Parent Loop BB0_61 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB0_65
# BB#63:                                #   in Loop: Header=BB0_62 Depth=2
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB0_66
# BB#64:                                # %._crit_edge.i.i.i.i138
                                        #   in Loop: Header=BB0_62 Depth=2
	movq	(%r14), %rax
.LBB0_65:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i139
                                        #   in Loop: Header=BB0_62 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB0_67
	.p2align	4, 0x90
.LBB0_66:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i140
                                        #   in Loop: Header=BB0_62 Depth=2
	incl	160(%rbx)
	movb	$-1, %al
.LBB0_67:                               #   in Loop: Header=BB0_62 Depth=2
	movzbl	%al, %eax
	movl	104(%rbx), %esi
	movl	$32, %ecx
	subl	%esi, %ecx
	movl	%eax, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	164(%rbx), %edx
	movl	%edx, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rax), %eax
	orl	%ecx, %eax
	movl	%eax, 108(%rbx)
	addl	$-8, %esi
	movl	%esi, 104(%rbx)
	cmpl	$7, %esi
	ja	.LBB0_62
	jmp	.LBB0_69
	.p2align	4, 0x90
.LBB0_68:                               # %._ZN5NBitl8CDecoderI9CInBufferE9NormalizeEv.exit_crit_edge.i.i135
                                        #   in Loop: Header=BB0_61 Depth=1
	movl	164(%rbx), %edx
.LBB0_69:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadByteEv.exit143
                                        #   in Loop: Header=BB0_61 Depth=1
	leal	8(%rsi), %eax
	movl	%eax, 104(%rbx)
	movl	%edx, %ecx
	shrl	$8, %ecx
	movl	%ecx, 164(%rbx)
	movl	160(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB0_71
# BB#70:                                # %_ZNK9NCompress8NDeflate8NDecoder6CCoder13InputEofErrorEv.exit.i129
                                        #   in Loop: Header=BB0_61 Depth=1
	movl	$24, %edi
	subl	%esi, %edi
	leal	(,%rcx,8), %esi
	cmpl	%esi, %edi
	jb	.LBB0_96
.LBB0_71:                               # %_ZNK9NCompress8NDeflate8NDecoder6CCoder13InputEofErrorEv.exit.thread.i130
                                        #   in Loop: Header=BB0_61 Depth=1
	testb	%dl, %dl
	je	.LBB0_74
# BB#72:                                #   in Loop: Header=BB0_61 Depth=1
	movsbl	%dl, %esi
	movq	%r12, %rdi
	callq	_ZN11CStringBaseIcEpLEc
	incl	%ebp
	cmpl	$65536, %ebp            # imm = 0x10000
	jb	.LBB0_61
	jmp	.LBB0_96
.LBB0_73:
	xorl	%ecx, %ecx
	jmp	.LBB0_15
.LBB0_74:                               # %_ZN8NArchive3NGzL10ReadStringEPN9NCompress8NDeflate8NDecoder9CCOMCoderER11CStringBaseIcEj.exit132.thread164.loopexit
	movb	1(%r15), %dl
.LBB0_75:                               # %_ZN8NArchive3NGzL10ReadStringEPN9NCompress8NDeflate8NDecoder9CCOMCoderER11CStringBaseIcEj.exit132.thread164
	testb	$2, %dl
	jne	.LBB0_78
# BB#76:                                # %_ZN8NArchive3NGzL10ReadUInt16EPN9NCompress8NDeflate8NDecoder9CCOMCoderERt.exit
	testl	%ecx, %ecx
	jne	.LBB0_94
	jmp	.LBB0_95
	.p2align	4, 0x90
.LBB0_78:                               # %.lr.ph.i.i.i64
                                        # =>This Inner Loop Header: Depth=1
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB0_81
# BB#79:                                #   in Loop: Header=BB0_78 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB0_82
# BB#80:                                # %._crit_edge.i.i.i.i66
                                        #   in Loop: Header=BB0_78 Depth=1
	movq	(%r14), %rax
.LBB0_81:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i67
                                        #   in Loop: Header=BB0_78 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB0_83
	.p2align	4, 0x90
.LBB0_82:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i68
                                        #   in Loop: Header=BB0_78 Depth=1
	incl	160(%rbx)
	movb	$-1, %al
.LBB0_83:                               #   in Loop: Header=BB0_78 Depth=1
	movzbl	%al, %esi
	movl	104(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	164(%rbx), %edx
	movl	%edx, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%rbx)
	leal	-8(%rax), %ecx
	movl	%ecx, 104(%rbx)
	cmpl	$7, %ecx
	ja	.LBB0_78
# BB#84:                                # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadByteEv.exit71
	movl	160(%rbx), %ecx
	movl	%eax, 104(%rbx)
	shrl	$8, %edx
	movl	%edx, 164(%rbx)
	testl	%ecx, %ecx
	je	.LBB0_86
# BB#85:                                # %_ZNK9NCompress8NDeflate8NDecoder6CCoder13InputEofErrorEv.exit.i
	movl	$32, %edx
	subl	%eax, %edx
	shll	$3, %ecx
	cmpl	%ecx, %edx
	jb	.LBB0_96
	.p2align	4, 0x90
.LBB0_86:                               # %.lr.ph.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB0_89
# BB#87:                                #   in Loop: Header=BB0_86 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB0_90
# BB#88:                                # %._crit_edge.i.i.i.i
                                        #   in Loop: Header=BB0_86 Depth=1
	movq	(%r14), %rax
.LBB0_89:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i
                                        #   in Loop: Header=BB0_86 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB0_91
.LBB0_90:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i
                                        #   in Loop: Header=BB0_86 Depth=1
	incl	160(%rbx)
	movb	$-1, %al
.LBB0_91:                               #   in Loop: Header=BB0_86 Depth=1
	movzbl	%al, %esi
	movl	104(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	164(%rbx), %edx
	movl	%edx, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%rbx)
	leal	-8(%rax), %ecx
	movl	%ecx, 104(%rbx)
	cmpl	$7, %ecx
	ja	.LBB0_86
# BB#92:                                # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadByteEv.exit
	movl	160(%rbx), %ecx
	movl	%eax, 104(%rbx)
	shrl	$8, %edx
	movl	%edx, 164(%rbx)
	testl	%ecx, %ecx
	je	.LBB0_95
# BB#93:                                # %_ZNK9NCompress8NDeflate8NDecoder6CCoder13InputEofErrorEv.exit.1.i
	movl	$32, %edx
	subl	%eax, %edx
	leal	(,%rcx,8), %esi
	cmpl	%esi, %edx
	jb	.LBB0_96
.LBB0_94:                               # %_ZN8NArchive3NGzL10ReadUInt16EPN9NCompress8NDeflate8NDecoder9CCOMCoderERt.exit.thread217
	movl	$32, %edx
	subl	%eax, %edx
	shll	$3, %ecx
	cmpl	%ecx, %edx
	setb	%r13b
	jmp	.LBB0_96
.LBB0_95:
	xorl	%r13d, %r13d
.LBB0_96:                               # %.thread155
	movzbl	%r13b, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN8NArchive3NGz5CItem10ReadHeaderEPN9NCompress8NDeflate8NDecoder9CCOMCoderE, .Lfunc_end0-_ZN8NArchive3NGz5CItem10ReadHeaderEPN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.cfi_endproc

	.globl	_ZN8NArchive3NGz5CItem11ReadFooter1EPN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz5CItem11ReadFooter1EPN9NCompress8NDeflate8NDecoder9CCOMCoderE,@function
_ZN8NArchive3NGz5CItem11ReadFooter1EPN9NCompress8NDeflate8NDecoder9CCOMCoderE: # @_ZN8NArchive3NGz5CItem11ReadFooter1EPN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r12, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	leaq	112(%rbx), %r15
	movl	104(%rbx), %ecx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
	cmpl	$8, %ecx
	jb	.LBB1_2
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph.i.i.i.i
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rbx), %rax
	cmpq	120(%rbx), %rax
	jb	.LBB1_6
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=2
	movq	%r15, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB1_7
# BB#5:                                 # %._crit_edge.i.i.i.i.i
                                        #   in Loop: Header=BB1_3 Depth=2
	movq	(%r15), %rax
.LBB1_6:                                # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i.i.i
                                        #   in Loop: Header=BB1_3 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r15)
	movzbl	(%rax), %eax
	jmp	.LBB1_8
	.p2align	4, 0x90
.LBB1_7:                                # %_ZN9CInBuffer8ReadByteERh.exit.i.i.i.i
                                        #   in Loop: Header=BB1_3 Depth=2
	incl	160(%rbx)
	movb	$-1, %al
.LBB1_8:                                #   in Loop: Header=BB1_3 Depth=2
	movzbl	%al, %esi
	movl	104(%rbx), %edx
	movl	$32, %ecx
	subl	%edx, %ecx
	movl	%esi, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	164(%rbx), %eax
	movl	%eax, 164(%rbx)
	movl	108(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 108(%rbx)
	addl	$-8, %edx
	movl	%edx, 104(%rbx)
	cmpl	$7, %edx
	ja	.LBB1_3
	jmp	.LBB1_9
	.p2align	4, 0x90
.LBB1_2:                                # %._ZN5NBitl8CDecoderI9CInBufferE9NormalizeEv.exit_crit_edge.i.i.i
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	164(%rbx), %eax
	movl	%ecx, %edx
.LBB1_9:                                # %_ZN9NCompress8NDeflate8NDecoder6CCoder8ReadByteEv.exit.i
                                        #   in Loop: Header=BB1_1 Depth=1
	leal	8(%rdx), %ecx
	movl	%ecx, 104(%rbx)
	movl	%eax, %esi
	shrl	$8, %esi
	movl	%esi, 164(%rbx)
	movb	%al, (%rsp,%r12)
	incq	%r12
	cmpq	$8, %r12
	jne	.LBB1_1
# BB#10:                                # %._crit_edge.i
	movl	160(%rbx), %esi
	testl	%esi, %esi
	je	.LBB1_15
# BB#11:                                # %_ZN8NArchive3NGzL9ReadBytesEPN9NCompress8NDeflate8NDecoder9CCOMCoderEPhj.exit
	movl	$24, %edi
	subl	%edx, %edi
	leal	(,%rsi,8), %ecx
	cmpl	%ecx, %edi
	sbbl	%eax, %eax
	cmpl	%ecx, %edi
	jae	.LBB1_13
# BB#12:
	andl	$1, %eax
	jmp	.LBB1_18
.LBB1_15:                               # %_ZNK9NCompress8NDeflate8NDecoder6CCoder13InputEofErrorEv.exit.critedge
	movl	(%rsp), %eax
	movl	%eax, 8(%r14)
	movl	4(%rsp), %eax
	movl	%eax, 12(%r14)
	jmp	.LBB1_16
.LBB1_13:                               # %_ZN8NArchive3NGzL9ReadBytesEPN9NCompress8NDeflate8NDecoder9CCOMCoderEPhj.exit.thread
	testl	%esi, %esi
	movl	(%rsp), %eax
	movl	%eax, 8(%r14)
	movl	4(%rsp), %eax
	movl	%eax, 12(%r14)
	je	.LBB1_16
# BB#14:
	movl	$24, %eax
	subl	%edx, %eax
	cmpl	%ecx, %eax
	setb	%al
	jmp	.LBB1_17
.LBB1_16:                               # %_ZNK9NCompress8NDeflate8NDecoder6CCoder13InputEofErrorEv.exit
	xorl	%eax, %eax
.LBB1_17:                               # %_ZNK9NCompress8NDeflate8NDecoder6CCoder13InputEofErrorEv.exit
	movzbl	%al, %eax
.LBB1_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZN8NArchive3NGz5CItem11ReadFooter1EPN9NCompress8NDeflate8NDecoder9CCOMCoderE, .Lfunc_end1-_ZN8NArchive3NGz5CItem11ReadFooter1EPN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.cfi_endproc

	.globl	_ZN8NArchive3NGz5CItem11ReadFooter2EP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz5CItem11ReadFooter2EP19ISequentialInStream,@function
_ZN8NArchive3NGz5CItem11ReadFooter2EP19ISequentialInStream: # @_ZN8NArchive3NGz5CItem11ReadFooter2EP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	8(%rsp), %rax
	movl	$8, %edx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	testl	%eax, %eax
	jne	.LBB2_2
# BB#1:
	movl	8(%rsp), %eax
	movl	%eax, 8(%rbx)
	movl	12(%rsp), %eax
	movl	%eax, 12(%rbx)
	xorl	%eax, %eax
.LBB2_2:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZN8NArchive3NGz5CItem11ReadFooter2EP19ISequentialInStream, .Lfunc_end2-_ZN8NArchive3NGz5CItem11ReadFooter2EP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN8NArchive3NGz5CItem11WriteHeaderEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz5CItem11WriteHeaderEP20ISequentialOutStream,@function
_ZN8NArchive3NGz5CItem11WriteHeaderEP20ISequentialOutStream: # @_ZN8NArchive3NGz5CItem11WriteHeaderEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 48
.Lcfi28:
	.cfi_offset %rbx, -24
.Lcfi29:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movw	$-29921, 14(%rsp)       # imm = 0x8B1F
	movb	(%rbx), %al
	movb	%al, 16(%rsp)
	movb	1(%rbx), %al
	andb	$8, %al
	movb	%al, 17(%rsp)
	movl	4(%rbx), %eax
	movl	%eax, 18(%rsp)
	movb	2(%rbx), %al
	movb	%al, 22(%rsp)
	movb	3(%rbx), %al
	movb	%al, 23(%rsp)
	leaq	14(%rsp), %rsi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB3_4
# BB#1:
	testb	$8, 1(%rbx)
	je	.LBB3_3
# BB#2:
	movq	16(%rbx), %rsi
	movslq	24(%rbx), %rdx
	incq	%rdx
	movq	%r14, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB3_4
.LBB3_3:
	xorl	%eax, %eax
.LBB3_4:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	_ZN8NArchive3NGz5CItem11WriteHeaderEP20ISequentialOutStream, .Lfunc_end3-_ZN8NArchive3NGz5CItem11WriteHeaderEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN8NArchive3NGz5CItem11WriteFooterEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz5CItem11WriteFooterEP20ISequentialOutStream,@function
_ZN8NArchive3NGz5CItem11WriteFooterEP20ISequentialOutStream: # @_ZN8NArchive3NGz5CItem11WriteFooterEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	movl	%eax, (%rsp)
	movl	12(%rdi), %eax
	movl	%eax, 4(%rsp)
	movq	%rsp, %rax
	movl	$8, %edx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	popq	%rcx
	retq
.Lfunc_end4:
	.size	_ZN8NArchive3NGz5CItem11WriteFooterEP20ISequentialOutStream, .Lfunc_end4-_ZN8NArchive3NGz5CItem11WriteFooterEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN8NArchive3NGz8CHandler21GetNumberOfPropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler21GetNumberOfPropertiesEPj,@function
_ZN8NArchive3NGz8CHandler21GetNumberOfPropertiesEPj: # @_ZN8NArchive3NGz8CHandler21GetNumberOfPropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$6, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	_ZN8NArchive3NGz8CHandler21GetNumberOfPropertiesEPj, .Lfunc_end5-_ZN8NArchive3NGz8CHandler21GetNumberOfPropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive3NGz8CHandler15GetPropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler15GetPropertyInfoEjPPwPjPt,@function
_ZN8NArchive3NGz8CHandler15GetPropertyInfoEjPPwPjPt: # @_ZN8NArchive3NGz8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$5, %esi
	ja	.LBB6_2
# BB#1:
	movl	%esi, %eax
	shlq	$4, %rax
	movl	_ZN8NArchive3NGz6kPropsE+8(%rax), %esi
	movl	%esi, (%rcx)
	movzwl	_ZN8NArchive3NGz6kPropsE+12(%rax), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB6_2:
	retq
.Lfunc_end6:
	.size	_ZN8NArchive3NGz8CHandler15GetPropertyInfoEjPPwPjPt, .Lfunc_end6-_ZN8NArchive3NGz8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive3NGz8CHandler28GetNumberOfArchivePropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler28GetNumberOfArchivePropertiesEPj,@function
_ZN8NArchive3NGz8CHandler28GetNumberOfArchivePropertiesEPj: # @_ZN8NArchive3NGz8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$0, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end7:
	.size	_ZN8NArchive3NGz8CHandler28GetNumberOfArchivePropertiesEPj, .Lfunc_end7-_ZN8NArchive3NGz8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive3NGz8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler22GetArchivePropertyInfoEjPPwPjPt,@function
_ZN8NArchive3NGz8CHandler22GetArchivePropertyInfoEjPPwPjPt: # @_ZN8NArchive3NGz8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147467263, %eax      # imm = 0x80004001
	retq
.Lfunc_end8:
	.size	_ZN8NArchive3NGz8CHandler22GetArchivePropertyInfoEjPPwPjPt, .Lfunc_end8-_ZN8NArchive3NGz8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive3NGz8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler18GetArchivePropertyEjP14tagPROPVARIANT,@function
_ZN8NArchive3NGz8CHandler18GetArchivePropertyEjP14tagPROPVARIANT: # @_ZN8NArchive3NGz8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -16
	movq	%rdx, %rbx
	movl	$0, (%rsp)
	cmpl	$44, %esi
	jne	.LBB9_3
# BB#1:
	cmpb	$0, 112(%rdi)
	je	.LBB9_3
# BB#2:
	movq	104(%rdi), %rsi
.Ltmp0:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp1:
.LBB9_3:
.Ltmp2:
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp3:
# BB#4:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB9_5:
.Ltmp4:
	movq	%rax, %rbx
.Ltmp5:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp6:
# BB#6:                                 # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB9_7:
.Ltmp7:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN8NArchive3NGz8CHandler18GetArchivePropertyEjP14tagPROPVARIANT, .Lfunc_end9-_ZN8NArchive3NGz8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp5-.Ltmp3           #   Call between .Ltmp3 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	1                       #   On action: 1
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end9-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end10:
	.size	__clang_call_terminate, .Lfunc_end10-__clang_call_terminate

	.text
	.globl	_ZN8NArchive3NGz8CHandler16GetNumberOfItemsEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler16GetNumberOfItemsEPj,@function
_ZN8NArchive3NGz8CHandler16GetNumberOfItemsEPj: # @_ZN8NArchive3NGz8CHandler16GetNumberOfItemsEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end11:
	.size	_ZN8NArchive3NGz8CHandler16GetNumberOfItemsEPj, .Lfunc_end11-_ZN8NArchive3NGz8CHandler16GetNumberOfItemsEPj
	.cfi_endproc

	.globl	_ZN8NArchive3NGz8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler11GetPropertyEjjP14tagPROPVARIANT,@function
_ZN8NArchive3NGz8CHandler11GetPropertyEjjP14tagPROPVARIANT: # @_ZN8NArchive3NGz8CHandler11GetPropertyEjjP14tagPROPVARIANT
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 64
.Lcfi37:
	.cfi_offset %rbx, -24
.Lcfi38:
	.cfi_offset %r14, -16
	movq	%rcx, %rbx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %rax
	movl	$0, (%rsp)
	addl	$-3, %edx
	cmpl	$20, %edx
	ja	.LBB12_23
# BB#1:
	jmpq	*.LJTI12_0(,%rdx,8)
.LBB12_2:
	testb	$8, 41(%rax)
	je	.LBB12_23
# BB#3:
	addq	$56, %rax
.Ltmp21:
	leaq	24(%rsp), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp22:
# BB#4:
	movq	24(%rsp), %rsi
.Ltmp23:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp24:
# BB#5:
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_23
# BB#6:
	callq	_ZdaPv
	jmp	.LBB12_23
.LBB12_18:
	cmpb	$0, 112(%rax)
	je	.LBB12_23
# BB#19:
	movq	104(%rax), %rsi
.Ltmp12:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp13:
	jmp	.LBB12_23
.LBB12_20:
	movzbl	43(%rax), %eax
	cmpq	$20, %rax
	leaq	_ZN8NArchive3NGzL9kHostOSesE(,%rax,8), %rax
	movl	$_ZN8NArchive3NGzL10kUnknownOSE, %ecx
	cmovbq	%rax, %rcx
	movq	(%rcx), %rsi
.Ltmp10:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKc
.Ltmp11:
	jmp	.LBB12_23
.LBB12_16:
	cmpq	$0, 120(%rax)
	je	.LBB12_23
# BB#17:
	movl	52(%rax), %esi
.Ltmp14:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp15:
	jmp	.LBB12_23
.LBB12_12:
	movl	44(%rax), %edi
	testl	%edi, %edi
	je	.LBB12_23
# BB#13:
.Ltmp16:
	leaq	16(%rsp), %rsi
	callq	_ZN8NWindows5NTime18UnixTimeToFileTimeEjR9_FILETIME
.Ltmp17:
# BB#14:
.Ltmp18:
	movq	%rsp, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp19:
	jmp	.LBB12_23
.LBB12_21:
	cmpq	$0, 120(%rax)
	je	.LBB12_23
# BB#22:
	movl	48(%rax), %esi
.Ltmp8:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp9:
.LBB12_23:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp26:
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp27:
# BB#24:
.Ltmp32:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp33:
# BB#25:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit13
	xorl	%eax, %eax
.LBB12_31:
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB12_9:
.Ltmp25:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_11
# BB#10:
	callq	_ZdaPv
	jmp	.LBB12_11
.LBB12_15:
.Ltmp20:
	jmp	.LBB12_8
.LBB12_26:
.Ltmp34:
	movq	%rdx, %r14
	movq	%rax, %rbx
	jmp	.LBB12_27
.LBB12_7:
.Ltmp28:
.LBB12_8:                               # %_ZN11CStringBaseIwED2Ev.exit14
	movq	%rdx, %r14
	movq	%rax, %rbx
.LBB12_11:                              # %_ZN11CStringBaseIwED2Ev.exit14
.Ltmp29:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp30:
.LBB12_27:                              # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB12_28
# BB#30:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB12_31
.LBB12_28:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp35:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp36:
# BB#33:
.LBB12_29:
.Ltmp37:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB12_32:
.Ltmp31:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN8NArchive3NGz8CHandler11GetPropertyEjjP14tagPROPVARIANT, .Lfunc_end12-_ZN8NArchive3NGz8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI12_0:
	.quad	.LBB12_2
	.quad	.LBB12_23
	.quad	.LBB12_23
	.quad	.LBB12_23
	.quad	.LBB12_16
	.quad	.LBB12_18
	.quad	.LBB12_23
	.quad	.LBB12_23
	.quad	.LBB12_23
	.quad	.LBB12_12
	.quad	.LBB12_23
	.quad	.LBB12_23
	.quad	.LBB12_23
	.quad	.LBB12_23
	.quad	.LBB12_23
	.quad	.LBB12_23
	.quad	.LBB12_21
	.quad	.LBB12_23
	.quad	.LBB12_23
	.quad	.LBB12_23
	.quad	.LBB12_20
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\221\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp28-.Lfunc_begin1   #     jumps to .Ltmp28
	.byte	3                       #   On action: 2
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin1   #     jumps to .Ltmp25
	.byte	3                       #   On action: 2
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp15-.Ltmp12         #   Call between .Ltmp12 and .Ltmp15
	.long	.Ltmp28-.Lfunc_begin1   #     jumps to .Ltmp28
	.byte	3                       #   On action: 2
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp19-.Ltmp16         #   Call between .Ltmp16 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin1   #     jumps to .Ltmp20
	.byte	3                       #   On action: 2
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 5 <<
	.long	.Ltmp27-.Ltmp8          #   Call between .Ltmp8 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin1   #     jumps to .Ltmp28
	.byte	3                       #   On action: 2
	.long	.Ltmp32-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin1   #     jumps to .Ltmp34
	.byte	3                       #   On action: 2
	.long	.Ltmp29-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin1   #     jumps to .Ltmp31
	.byte	1                       #   On action: 1
	.long	.Ltmp30-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp35-.Ltmp30         #   Call between .Ltmp30 and .Ltmp35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin1   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Lfunc_end12-.Ltmp36    #   Call between .Ltmp36 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3NGz8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback,@function
_ZN8NArchive3NGz8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback: # @_ZN8NArchive3NGz8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 48
.Lcfi43:
	.cfi_offset %rbx, -32
.Lcfi44:
	.cfi_offset %r14, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%rbx), %rax
	leaq	88(%r14), %rcx
.Ltmp38:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp39:
# BB#1:
	testl	%ebp, %ebp
	jne	.LBB13_21
# BB#2:
	movq	(%r14), %rax
.Ltmp40:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*136(%rax)
	movl	%eax, %ebp
.Ltmp41:
# BB#3:
	testl	%ebp, %ebp
	jne	.LBB13_16
# BB#4:
	movq	(%rbx), %rax
.Ltmp43:
	leaq	8(%rsp), %rcx
	movq	$-8, %rsi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp44:
# BB#5:
	movq	8(%rsp), %rax
	addq	$8, %rax
	subq	88(%r14), %rax
	movq	%rax, 104(%r14)
	movb	$1, 112(%r14)
	testl	%ebp, %ebp
	jne	.LBB13_16
# BB#6:
.Ltmp45:
	movq	%rsp, %rsi
	movl	$8, %edx
	movq	%rbx, %rdi
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	movl	%eax, %ebp
.Ltmp46:
# BB#7:                                 # %.noexc
	testl	%ebp, %ebp
	jne	.LBB13_9
# BB#8:
	movl	(%rsp), %eax
	movl	%eax, 48(%r14)
	movl	4(%rsp), %eax
	movl	%eax, 52(%r14)
	xorl	%ebp, %ebp
.LBB13_9:
	movq	(%rbx), %rax
.Ltmp47:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp48:
# BB#10:                                # %.noexc30
	movq	120(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB13_12
# BB#11:
	movq	(%rdi), %rax
.Ltmp49:
	callq	*16(%rax)
.Ltmp50:
.LBB13_12:
	movq	%rbx, 120(%r14)
	testl	%ebp, %ebp
	je	.LBB13_13
.LBB13_16:                              # %.thread
	movq	(%r14), %rax
.Ltmp54:
	movq	%r14, %rdi
	callq	*48(%rax)
.Ltmp55:
.LBB13_21:
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB13_13:
	xorl	%ebp, %ebp
	jmp	.LBB13_21
.LBB13_14:
.Ltmp51:
	jmp	.LBB13_15
.LBB13_23:
.Ltmp42:
.LBB13_15:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movl	$1, %ebp
.Ltmp52:
	callq	__cxa_end_catch
.Ltmp53:
	jmp	.LBB13_16
.LBB13_17:
.Ltmp56:
	movq	%rdx, %rbp
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %ebp
	je	.LBB13_18
# BB#20:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB13_21
.LBB13_18:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp57:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp58:
# BB#22:
.LBB13_19:
.Ltmp59:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZN8NArchive3NGz8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback, .Lfunc_end13-_ZN8NArchive3NGz8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\366\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp38-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp41-.Ltmp38         #   Call between .Ltmp38 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin2   #     jumps to .Ltmp42
	.byte	1                       #   On action: 1
	.long	.Ltmp43-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp50-.Ltmp43         #   Call between .Ltmp43 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin2   #     jumps to .Ltmp51
	.byte	1                       #   On action: 1
	.long	.Ltmp54-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin2   #     jumps to .Ltmp56
	.byte	3                       #   On action: 2
	.long	.Ltmp55-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp52-.Ltmp55         #   Call between .Ltmp55 and .Ltmp52
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp56-.Lfunc_begin2   #     jumps to .Ltmp56
	.byte	3                       #   On action: 2
	.long	.Ltmp53-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp57-.Ltmp53         #   Call between .Ltmp53 and .Ltmp57
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin2   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Lfunc_end13-.Ltmp58    #   Call between .Ltmp58 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3NGz8CHandler7OpenSeqEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler7OpenSeqEP19ISequentialInStream,@function
_ZN8NArchive3NGz8CHandler7OpenSeqEP19ISequentialInStream: # @_ZN8NArchive3NGz8CHandler7OpenSeqEP19ISequentialInStream
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 32
.Lcfi49:
	.cfi_offset %rbx, -32
.Lcfi50:
	.cfi_offset %r14, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	(%rbx), %rax
.Ltmp60:
	callq	*48(%rax)
.Ltmp61:
# BB#1:
	movq	136(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp62:
	movq	%rbp, %rsi
	callq	*48(%rax)
.Ltmp63:
# BB#2:
	movq	136(%rbx), %rbp
	leaq	112(%rbp), %r14
.Ltmp64:
	movl	$131072, %esi           # imm = 0x20000
	movq	%r14, %rdi
	callq	_ZN9CInBuffer6CreateEj
.Ltmp65:
# BB#3:                                 # %.noexc
	testb	%al, %al
	je	.LBB14_6
# BB#4:
.Ltmp66:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer4InitEv
.Ltmp67:
# BB#5:                                 # %.noexc16
	movl	$32, 104(%rbp)
	movl	$0, 108(%rbp)
	movl	$0, 160(%rbp)
	movl	$0, 164(%rbp)
	movb	$0, 3459(%rbp)
.LBB14_6:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder12InitInStreamEb.exit
	leaq	40(%rbx), %rdi
	movq	136(%rbx), %rsi
.Ltmp68:
	callq	_ZN8NArchive3NGz5CItem10ReadHeaderEPN9NCompress8NDeflate8NDecoder9CCOMCoderE
	movl	%eax, %ebp
.Ltmp69:
# BB#7:
	movq	136(%rbx), %rax
	movq	112(%rax), %rcx
	addq	144(%rax), %rcx
	subq	128(%rax), %rcx
	movl	160(%rax), %edx
	addq	%rcx, %rdx
	movl	$32, %ecx
	subl	104(%rax), %ecx
	shrl	$3, %ecx
	subq	%rcx, %rdx
	movq	%rdx, 96(%rbx)
	testl	%ebp, %ebp
	je	.LBB14_8
.LBB14_10:                              # %.thread
	movq	(%rbx), %rax
.Ltmp73:
	movq	%rbx, %rdi
	callq	*48(%rax)
.Ltmp74:
	jmp	.LBB14_15
.LBB14_8:
	xorl	%ebp, %ebp
.LBB14_15:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB14_9:
.Ltmp70:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movl	$1, %ebp
.Ltmp71:
	callq	__cxa_end_catch
.Ltmp72:
	jmp	.LBB14_10
.LBB14_11:
.Ltmp75:
	movq	%rdx, %rbp
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %ebp
	je	.LBB14_12
# BB#14:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB14_15
.LBB14_12:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp76:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp77:
# BB#16:
.LBB14_13:
.Ltmp78:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN8NArchive3NGz8CHandler7OpenSeqEP19ISequentialInStream, .Lfunc_end14-_ZN8NArchive3NGz8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	105                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp60-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp69-.Ltmp60         #   Call between .Ltmp60 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin3   #     jumps to .Ltmp70
	.byte	1                       #   On action: 1
	.long	.Ltmp73-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin3   #     jumps to .Ltmp75
	.byte	3                       #   On action: 2
	.long	.Ltmp74-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp71-.Ltmp74         #   Call between .Ltmp74 and .Ltmp71
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp75-.Lfunc_begin3   #     jumps to .Ltmp75
	.byte	3                       #   On action: 2
	.long	.Ltmp72-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp76-.Ltmp72         #   Call between .Ltmp72 and .Ltmp76
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin3   #     jumps to .Ltmp78
	.byte	0                       #   On action: cleanup
	.long	.Ltmp77-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Lfunc_end14-.Ltmp77    #   Call between .Ltmp77 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn8_N8NArchive3NGz8CHandler7OpenSeqEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3NGz8CHandler7OpenSeqEP19ISequentialInStream,@function
_ZThn8_N8NArchive3NGz8CHandler7OpenSeqEP19ISequentialInStream: # @_ZThn8_N8NArchive3NGz8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN8NArchive3NGz8CHandler7OpenSeqEP19ISequentialInStream # TAILCALL
.Lfunc_end15:
	.size	_ZThn8_N8NArchive3NGz8CHandler7OpenSeqEP19ISequentialInStream, .Lfunc_end15-_ZThn8_N8NArchive3NGz8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN8NArchive3NGz8CHandler5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler5CloseEv,@function
_ZN8NArchive3NGz8CHandler5CloseEv:      # @_ZN8NArchive3NGz8CHandler5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 16
.Lcfi53:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	$0, 112(%rbx)
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB16_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 120(%rbx)
.LBB16_2:                               # %_ZN9CMyComPtrI9IInStreamE7ReleaseEv.exit
	movq	136(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*56(%rax)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end16:
	.size	_ZN8NArchive3NGz8CHandler5CloseEv, .Lfunc_end16-_ZN8NArchive3NGz8CHandler5CloseEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_0:
	.zero	16
	.text
	.globl	_ZN8NArchive3NGz8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler7ExtractEPKjjiP23IArchiveExtractCallback,@function
_ZN8NArchive3NGz8CHandler7ExtractEPKjjiP23IArchiveExtractCallback: # @_ZN8NArchive3NGz8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi60:
	.cfi_def_cfa_offset 144
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movl	%ecx, %ebp
	movq	%rdi, %r12
	cmpl	$-1, %edx
	je	.LBB17_6
# BB#1:
	testl	%edx, %edx
	je	.LBB17_2
# BB#3:
	cmpl	$1, %edx
	jne	.LBB17_5
# BB#4:
	cmpl	$0, (%rsi)
	je	.LBB17_6
.LBB17_5:
	movl	$-2147024809, %r14d     # imm = 0x80070057
	jmp	.LBB17_105
.LBB17_6:
	cmpq	$0, 120(%r12)
	je	.LBB17_8
# BB#7:
	movq	(%r13), %rax
	movq	104(%r12), %rsi
.Ltmp79:
	movq	%r13, %rdi
	callq	*40(%rax)
.Ltmp80:
.LBB17_8:
	movq	$0, 80(%rsp)
	movq	(%r13), %rax
.Ltmp82:
	leaq	80(%rsp), %rsi
	movq	%r13, %rdi
	callq	*48(%rax)
	movl	%eax, %r14d
.Ltmp83:
# BB#9:
	testl	%r14d, %r14d
	jne	.LBB17_105
# BB#10:
	movq	$0, 16(%rsp)
	xorl	%ebx, %ebx
	testl	%ebp, %ebp
	setne	%bl
	movq	(%r13), %rax
.Ltmp85:
	leaq	16(%rsp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	%ebx, %ecx
	callq	*56(%rax)
	movl	%eax, %r14d
.Ltmp86:
# BB#11:
	testl	%r14d, %r14d
	je	.LBB17_12
.LBB17_88:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit147
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_105
# BB#89:
	movq	(%rdi), %rax
.Ltmp154:
	callq	*16(%rax)
.Ltmp155:
	jmp	.LBB17_105
.LBB17_2:
	xorl	%r14d, %r14d
.LBB17_105:
	movl	%r14d, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_12:
	testl	%ebp, %ebp
	sete	%al
	cmpq	$0, 16(%rsp)
	jne	.LBB17_14
# BB#13:
	xorl	%r14d, %r14d
	testb	%al, %al
	jne	.LBB17_105
.LBB17_14:
	movq	(%r13), %rax
.Ltmp87:
	movq	%r13, %rdi
	movl	%ebx, %esi
	callq	*64(%rax)
.Ltmp88:
# BB#15:
.Ltmp89:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp90:
# BB#16:
	movl	$0, 8(%rbx)
	movq	$_ZTV17COutStreamWithCRC+16, (%rbx)
	movq	$0, 16(%rbx)
.Ltmp92:
	movq	%rbx, %rdi
	callq	*_ZTV17COutStreamWithCRC+24(%rip)
.Ltmp93:
# BB#17:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit
	movq	16(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB17_19
# BB#18:
	movq	(%rbp), %rax
.Ltmp95:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp96:
.LBB17_19:                              # %.noexc148
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_21
# BB#20:
	movq	(%rdi), %rax
.Ltmp97:
	callq	*16(%rax)
.Ltmp98:
.LBB17_21:
	movq	%rbp, 16(%rbx)
	movq	$0, 24(%rbx)
	movb	$1, 36(%rbx)
	movl	$-1, 32(%rbx)
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_24
# BB#22:
	movq	(%rdi), %rax
.Ltmp99:
	callq	*16(%rax)
.Ltmp100:
# BB#23:                                # %.noexc150
	movq	$0, 16(%rsp)
.LBB17_24:                              # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
.Ltmp101:
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp102:
# BB#25:
.Ltmp104:
	movq	%r15, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp105:
# BB#26:
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	(%r15), %rax
.Ltmp107:
	movq	%r15, %rdi
	callq	*8(%rax)
.Ltmp108:
# BB#27:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit
.Ltmp110:
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp111:
# BB#28:
	movq	120(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB17_29
# BB#32:
	movq	(%rdi), %rax
	movq	88(%r12), %rsi
.Ltmp112:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %r14d
.Ltmp113:
# BB#33:
	testl	%r14d, %r14d
	movq	8(%rsp), %rbx           # 8-byte Reload
	jne	.LBB17_85
# BB#34:
	movq	136(%r12), %rbx
	leaq	112(%rbx), %rbp
.Ltmp114:
	movl	$131072, %esi           # imm = 0x20000
	movq	%rbp, %rdi
	callq	_ZN9CInBuffer6CreateEj
.Ltmp115:
# BB#35:                                # %.noexc154
	leaq	136(%r12), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	testb	%al, %al
	je	.LBB17_38
# BB#36:
.Ltmp116:
	movq	%rbp, %rdi
	callq	_ZN9CInBuffer4InitEv
.Ltmp117:
# BB#37:                                # %.noexc155
	movl	$32, 104(%rbx)
	movl	$0, 108(%rbx)
	movl	$0, 160(%rbx)
	movl	$0, 164(%rbx)
	movb	$0, 3459(%rbx)
	jmp	.LBB17_38
.LBB17_29:                              # %..preheader_crit_edge
	leaq	136(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB17_38:                              # %.preheader
	xorl	%r14d, %r14d
	movb	$1, %al
	movl	%eax, 4(%rsp)           # 4-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, (%rsp)            # 4-byte Spill
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB17_39:                              # =>This Inner Loop Header: Depth=1
	movq	136(%r12), %rax
	movq	112(%rax), %rcx
	addq	144(%rax), %rcx
	subq	128(%rax), %rcx
	movl	160(%rax), %edx
	addq	%rcx, %rdx
	movl	$32, %ecx
	subl	104(%rax), %ecx
	shrl	$3, %ecx
	subq	%rcx, %rdx
	movq	%rdx, 104(%r12)
	movq	%rdx, 48(%r15)
	movb	$1, 112(%r12)
	movq	24(%rbx), %rax
	movq	%rax, 56(%r15)
.Ltmp119:
	movq	%r15, %rdi
	callq	_ZN14CLocalProgress6SetCurEv
.Ltmp120:
# BB#40:                                #   in Loop: Header=BB17_39 Depth=1
	testl	%eax, %eax
	jne	.LBB17_41
# BB#49:                                #   in Loop: Header=BB17_39 Depth=1
	leaq	48(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
.Ltmp121:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp122:
# BB#50:                                # %.noexc158
                                        #   in Loop: Header=BB17_39 Depth=1
	movq	%rbp, 48(%rsp)
	movb	$0, (%rbp)
	movl	$4, 60(%rsp)
	leaq	48(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
.Ltmp124:
	movl	$4, %edi
	callq	_Znam
.Ltmp125:
# BB#51:                                #   in Loop: Header=BB17_39 Depth=1
	movq	%rax, 64(%rsp)
	movb	$0, (%rax)
	movl	$4, 76(%rsp)
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, %ebx
	andb	$1, %bl
	je	.LBB17_53
# BB#52:                                #   in Loop: Header=BB17_39 Depth=1
	cmpq	$0, 120(%r12)
	je	.LBB17_63
.LBB17_53:                              #   in Loop: Header=BB17_39 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
.Ltmp127:
	leaq	32(%rsp), %rdi
	callq	_ZN8NArchive3NGz5CItem10ReadHeaderEPN9NCompress8NDeflate8NDecoder9CCOMCoderE
.Ltmp128:
# BB#54:                                #   in Loop: Header=BB17_39 Depth=1
	testl	%eax, %eax
	je	.LBB17_63
# BB#55:                                #   in Loop: Header=BB17_39 Depth=1
	cmpl	$1, %eax
	jne	.LBB17_56
# BB#62:                                #   in Loop: Header=BB17_39 Depth=1
	addb	%bl, %bl
	movzbl	%bl, %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movl	$2, %ebp
	jmp	.LBB17_77
.LBB17_63:                              #   in Loop: Header=BB17_39 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	24(%rsi), %rbx
	movl	$-1, 32(%rsi)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	(%rdi), %rax
.Ltmp130:
	xorl	%edx, %edx
	movq	%r15, %rcx
	callq	*80(%rax)
.Ltmp131:
# BB#64:                                #   in Loop: Header=BB17_39 Depth=1
	testl	%eax, %eax
	je	.LBB17_69
# BB#65:                                #   in Loop: Header=BB17_39 Depth=1
	cmpl	$1, %eax
	jne	.LBB17_66
# BB#68:                                #   in Loop: Header=BB17_39 Depth=1
	movl	$2, (%rsp)              # 4-byte Folded Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	$2, %ebp
	jmp	.LBB17_77
.LBB17_56:                              #   in Loop: Header=BB17_39 Depth=1
	movl	$1, %ebp
	movl	%eax, %r14d
	jmp	.LBB17_77
.LBB17_69:                              #   in Loop: Header=BB17_39 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movl	104(%rsi), %eax
	movl	%eax, %ecx
	negl	%ecx
	andl	$7, %ecx
	addl	%ecx, %eax
	movl	%eax, 104(%rsi)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, 164(%rsi)
.Ltmp133:
	leaq	32(%rsp), %rdi
	callq	_ZN8NArchive3NGz5CItem11ReadFooter1EPN9NCompress8NDeflate8NDecoder9CCOMCoderE
.Ltmp134:
# BB#70:                                #   in Loop: Header=BB17_39 Depth=1
	movl	$2, %ebp
	testl	%eax, %eax
	je	.LBB17_73
# BB#71:                                #   in Loop: Header=BB17_39 Depth=1
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	$2, (%rsp)              # 4-byte Folded Spill
	jmp	.LBB17_77
.LBB17_66:                              #   in Loop: Header=BB17_39 Depth=1
	movl	$1, %ebp
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	%eax, %r14d
	jmp	.LBB17_77
.LBB17_73:                              #   in Loop: Header=BB17_39 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	32(%rcx), %eax
	notl	%eax
	cmpl	%eax, 40(%rsp)
	jne	.LBB17_74
# BB#75:                                #   in Loop: Header=BB17_39 Depth=1
	movl	24(%rcx), %eax
	subl	%ebx, %eax
	xorl	%ebp, %ebp
	cmpl	%eax, 44(%rsp)
	movl	(%rsp), %eax            # 4-byte Reload
	movl	$3, %ecx
	cmovnel	%ecx, %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	setne	%bpl
	addl	%ebp, %ebp
	jmp	.LBB17_76
.LBB17_74:                              #   in Loop: Header=BB17_39 Depth=1
	movl	$3, (%rsp)              # 4-byte Folded Spill
.LBB17_76:                              # %.thread
                                        #   in Loop: Header=BB17_39 Depth=1
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
.LBB17_77:                              # %.thread
                                        #   in Loop: Header=BB17_39 Depth=1
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_79
# BB#78:                                #   in Loop: Header=BB17_39 Depth=1
	callq	_ZdaPv
.LBB17_79:                              # %_ZN11CStringBaseIcED2Ev.exit.i160
                                        #   in Loop: Header=BB17_39 Depth=1
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB17_81
# BB#80:                                #   in Loop: Header=BB17_39 Depth=1
	callq	_ZdaPv
.LBB17_81:                              # %_ZN8NArchive3NGz5CItemD2Ev.exit161
                                        #   in Loop: Header=BB17_39 Depth=1
	andb	$3, %bpl
	je	.LBB17_39
# BB#82:                                # %_ZN8NArchive3NGz5CItemD2Ev.exit161
	cmpb	$2, %bpl
	jne	.LBB17_85
# BB#83:
	movq	(%rbx), %rax
.Ltmp136:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp137:
# BB#84:                                # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit157
	movq	(%r13), %rax
	xorl	%ebx, %ebx
.Ltmp138:
	movq	%r13, %rdi
	movl	(%rsp), %esi            # 4-byte Reload
	callq	*72(%rax)
	movl	%eax, %r14d
.Ltmp139:
	jmp	.LBB17_85
.LBB17_41:
	movl	%eax, %r14d
.LBB17_85:                              # %.loopexit181
	movq	(%r15), %rax
.Ltmp143:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp144:
# BB#86:                                # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit153
	testq	%rbx, %rbx
	je	.LBB17_88
# BB#87:
	movq	(%rbx), %rax
.Ltmp148:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp149:
	jmp	.LBB17_88
.LBB17_47:                              # %.loopexit.split-lp
.Ltmp140:
	jmp	.LBB17_48
.LBB17_92:
.Ltmp150:
	jmp	.LBB17_31
.LBB17_93:
.Ltmp145:
	movq	%rdx, %r14
	movq	%rax, %r12
	testq	%rbx, %rbx
	jne	.LBB17_95
	jmp	.LBB17_96
.LBB17_45:
.Ltmp109:
	movq	%rdx, %r14
	movq	%rax, %r12
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB17_95
.LBB17_44:
.Ltmp106:
	movq	%rdx, %r14
	movq	%rax, %r12
	movq	%r15, %rdi
	callq	_ZdlPv
	jmp	.LBB17_95
.LBB17_42:
.Ltmp94:
	jmp	.LBB17_31
.LBB17_90:
.Ltmp118:
	movq	%rdx, %r14
	movq	%rax, %r12
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB17_91
.LBB17_98:
.Ltmp156:
	jmp	.LBB17_100
.LBB17_43:
.Ltmp103:
	movq	%rdx, %r14
	movq	%rax, %r12
	jmp	.LBB17_95
.LBB17_30:
.Ltmp91:
.LBB17_31:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit143
	movq	%rdx, %r14
	movq	%rax, %r12
	jmp	.LBB17_96
.LBB17_108:
.Ltmp81:
	jmp	.LBB17_100
.LBB17_99:
.Ltmp84:
.LBB17_100:
	movq	%rdx, %r14
	movq	%rax, %r12
	jmp	.LBB17_101
.LBB17_72:
.Ltmp135:
	jmp	.LBB17_58
.LBB17_67:
.Ltmp132:
	jmp	.LBB17_58
.LBB17_57:
.Ltmp129:
.LBB17_58:
	movq	%rdx, %r14
	movq	%rax, %r12
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB17_60
# BB#59:
	callq	_ZdaPv
.LBB17_60:                              # %_ZN11CStringBaseIcED2Ev.exit.i159
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_91
# BB#61:
	callq	_ZdaPv
	jmp	.LBB17_91
.LBB17_109:                             # %_ZN11CStringBaseIcED2Ev.exit.i
.Ltmp126:
	movq	%rdx, %r14
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	_ZdaPv
	jmp	.LBB17_91
.LBB17_46:                              # %.loopexit
.Ltmp123:
.LBB17_48:                              # %.body
	movq	%rdx, %r14
	movq	%rax, %r12
.LBB17_91:
	movq	(%r15), %rax
.Ltmp141:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp142:
# BB#94:                                # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	testq	%rbx, %rbx
	je	.LBB17_96
.LBB17_95:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.thread
	movq	(%rbx), %rax
.Ltmp146:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp147:
.LBB17_96:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit143
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_101
# BB#97:
	movq	(%rdi), %rax
.Ltmp151:
	callq	*16(%rax)
.Ltmp152:
.LBB17_101:
	movq	%r12, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB17_102
# BB#104:
	callq	__cxa_end_catch
	movl	$-2147024882, %r14d     # imm = 0x8007000E
	jmp	.LBB17_105
.LBB17_102:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp157:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp158:
# BB#107:
.LBB17_103:
.Ltmp159:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB17_106:
.Ltmp153:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end17:
	.size	_ZN8NArchive3NGz8CHandler7ExtractEPKjjiP23IArchiveExtractCallback, .Lfunc_end17-_ZN8NArchive3NGz8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\255\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\236\002"              # Call site table length
	.long	.Ltmp79-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin4   #     jumps to .Ltmp81
	.byte	3                       #   On action: 2
	.long	.Ltmp82-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin4   #     jumps to .Ltmp84
	.byte	3                       #   On action: 2
	.long	.Ltmp85-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp91-.Lfunc_begin4   #     jumps to .Ltmp91
	.byte	3                       #   On action: 2
	.long	.Ltmp154-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp155-.Ltmp154       #   Call between .Ltmp154 and .Ltmp155
	.long	.Ltmp156-.Lfunc_begin4  #     jumps to .Ltmp156
	.byte	3                       #   On action: 2
	.long	.Ltmp87-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Ltmp90-.Ltmp87         #   Call between .Ltmp87 and .Ltmp90
	.long	.Ltmp91-.Lfunc_begin4   #     jumps to .Ltmp91
	.byte	3                       #   On action: 2
	.long	.Ltmp92-.Lfunc_begin4   # >> Call Site 6 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin4   #     jumps to .Ltmp94
	.byte	3                       #   On action: 2
	.long	.Ltmp95-.Lfunc_begin4   # >> Call Site 7 <<
	.long	.Ltmp102-.Ltmp95        #   Call between .Ltmp95 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin4  #     jumps to .Ltmp103
	.byte	3                       #   On action: 2
	.long	.Ltmp104-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin4  #     jumps to .Ltmp106
	.byte	3                       #   On action: 2
	.long	.Ltmp107-.Lfunc_begin4  # >> Call Site 9 <<
	.long	.Ltmp108-.Ltmp107       #   Call between .Ltmp107 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin4  #     jumps to .Ltmp109
	.byte	3                       #   On action: 2
	.long	.Ltmp110-.Lfunc_begin4  # >> Call Site 10 <<
	.long	.Ltmp117-.Ltmp110       #   Call between .Ltmp110 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin4  #     jumps to .Ltmp118
	.byte	3                       #   On action: 2
	.long	.Ltmp119-.Lfunc_begin4  # >> Call Site 11 <<
	.long	.Ltmp122-.Ltmp119       #   Call between .Ltmp119 and .Ltmp122
	.long	.Ltmp123-.Lfunc_begin4  #     jumps to .Ltmp123
	.byte	3                       #   On action: 2
	.long	.Ltmp124-.Lfunc_begin4  # >> Call Site 12 <<
	.long	.Ltmp125-.Ltmp124       #   Call between .Ltmp124 and .Ltmp125
	.long	.Ltmp126-.Lfunc_begin4  #     jumps to .Ltmp126
	.byte	3                       #   On action: 2
	.long	.Ltmp127-.Lfunc_begin4  # >> Call Site 13 <<
	.long	.Ltmp128-.Ltmp127       #   Call between .Ltmp127 and .Ltmp128
	.long	.Ltmp129-.Lfunc_begin4  #     jumps to .Ltmp129
	.byte	3                       #   On action: 2
	.long	.Ltmp130-.Lfunc_begin4  # >> Call Site 14 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin4  #     jumps to .Ltmp132
	.byte	3                       #   On action: 2
	.long	.Ltmp133-.Lfunc_begin4  # >> Call Site 15 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp135-.Lfunc_begin4  #     jumps to .Ltmp135
	.byte	3                       #   On action: 2
	.long	.Ltmp136-.Lfunc_begin4  # >> Call Site 16 <<
	.long	.Ltmp139-.Ltmp136       #   Call between .Ltmp136 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin4  #     jumps to .Ltmp140
	.byte	3                       #   On action: 2
	.long	.Ltmp143-.Lfunc_begin4  # >> Call Site 17 <<
	.long	.Ltmp144-.Ltmp143       #   Call between .Ltmp143 and .Ltmp144
	.long	.Ltmp145-.Lfunc_begin4  #     jumps to .Ltmp145
	.byte	3                       #   On action: 2
	.long	.Ltmp148-.Lfunc_begin4  # >> Call Site 18 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin4  #     jumps to .Ltmp150
	.byte	3                       #   On action: 2
	.long	.Ltmp141-.Lfunc_begin4  # >> Call Site 19 <<
	.long	.Ltmp152-.Ltmp141       #   Call between .Ltmp141 and .Ltmp152
	.long	.Ltmp153-.Lfunc_begin4  #     jumps to .Ltmp153
	.byte	1                       #   On action: 1
	.long	.Ltmp152-.Lfunc_begin4  # >> Call Site 20 <<
	.long	.Ltmp157-.Ltmp152       #   Call between .Ltmp152 and .Ltmp157
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp157-.Lfunc_begin4  # >> Call Site 21 <<
	.long	.Ltmp158-.Ltmp157       #   Call between .Ltmp157 and .Ltmp158
	.long	.Ltmp159-.Lfunc_begin4  #     jumps to .Ltmp159
	.byte	0                       #   On action: cleanup
	.long	.Ltmp158-.Lfunc_begin4  # >> Call Site 22 <<
	.long	.Lfunc_end17-.Ltmp158   #   Call between .Ltmp158 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3NGz8CHandler15GetFileTimeTypeEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler15GetFileTimeTypeEPj,@function
_ZN8NArchive3NGz8CHandler15GetFileTimeTypeEPj: # @_ZN8NArchive3NGz8CHandler15GetFileTimeTypeEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end18:
	.size	_ZN8NArchive3NGz8CHandler15GetFileTimeTypeEPj, .Lfunc_end18-_ZN8NArchive3NGz8CHandler15GetFileTimeTypeEPj
	.cfi_endproc

	.globl	_ZThn16_N8NArchive3NGz8CHandler15GetFileTimeTypeEPj
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive3NGz8CHandler15GetFileTimeTypeEPj,@function
_ZThn16_N8NArchive3NGz8CHandler15GetFileTimeTypeEPj: # @_ZThn16_N8NArchive3NGz8CHandler15GetFileTimeTypeEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end19:
	.size	_ZThn16_N8NArchive3NGz8CHandler15GetFileTimeTypeEPj, .Lfunc_end19-_ZThn16_N8NArchive3NGz8CHandler15GetFileTimeTypeEPj
	.cfi_endproc

	.globl	_ZN8NArchive3NGz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback,@function
_ZN8NArchive3NGz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback: # @_ZN8NArchive3NGz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi73:
	.cfi_def_cfa_offset 256
.Lcfi74:
	.cfi_offset %rbx, -56
.Lcfi75:
	.cfi_offset %r12, -48
.Lcfi76:
	.cfi_offset %r13, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$-2147024809, %r12d     # imm = 0x80070057
	cmpl	$1, %edx
	jne	.LBB20_133
# BB#1:
	testq	%r13, %r13
	je	.LBB20_5
# BB#2:
	movq	(%r13), %rax
	leaq	172(%rsp), %rdx
	leaq	108(%rsp), %rcx
	leaq	168(%rsp), %r8
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	*56(%rax)
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jne	.LBB20_133
# BB#3:
	movdqu	40(%r15), %xmm0
	movdqa	%xmm0, 48(%rsp)
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 64(%rsp)
	movslq	64(%r15), %rax
	leaq	1(%rax), %rbx
	testl	%ebx, %ebx
	je	.LBB20_6
# BB#4:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rbx, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, 64(%rsp)
	movb	$0, (%rax)
	movl	%ebx, 76(%rsp)
	jmp	.LBB20_7
.LBB20_5:
	movl	$-2147467259, %r12d     # imm = 0x80004005
	jmp	.LBB20_133
.LBB20_6:
	xorl	%eax, %eax
.LBB20_7:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i
	movq	56(%r15), %rcx
	.p2align	4, 0x90
.LBB20_8:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB20_8
# BB#9:                                 # %_ZN11CStringBaseIcEC2ERKS0_.exit.i
	movl	64(%r15), %eax
	movl	%eax, 72(%rsp)
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 80(%rsp)
	movslq	80(%r15), %rax
	leaq	1(%rax), %rbx
	testl	%ebx, %ebx
	je	.LBB20_12
# BB#10:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rbx, %rdi
	cmovlq	%rax, %rdi
.Ltmp160:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp161:
# BB#11:                                # %.noexc.i
	movq	%rcx, 80(%rsp)
	movb	$0, (%rcx)
	movl	%ebx, 92(%rsp)
	jmp	.LBB20_13
.LBB20_12:
	xorl	%ecx, %ecx
.LBB20_13:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i5.i
	movq	72(%r15), %rsi
	.p2align	4, 0x90
.LBB20_14:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %eax
	incq	%rsi
	movb	%al, (%rcx)
	incq	%rcx
	testb	%al, %al
	jne	.LBB20_14
# BB#15:                                # %_ZN8NArchive3NGz5CItemC2ERKS1_.exit
	movl	80(%r15), %eax
	movl	%eax, 88(%rsp)
	movw	$0, 49(%rsp)
	cmpl	$0, 108(%rsp)
	je	.LBB20_36
# BB#16:
	movl	$0, 8(%rsp)
	movq	(%r13), %rax
.Ltmp163:
	leaq	8(%rsp), %rcx
	xorl	%esi, %esi
	movl	$12, %edx
	movq	%r13, %rdi
	callq	*64(%rax)
	movl	%eax, %r12d
.Ltmp164:
# BB#17:
	movl	$1, %ebx
	testl	%r12d, %r12d
	jne	.LBB20_21
# BB#18:
	movl	$-2147024809, %r12d     # imm = 0x80070057
	movzwl	8(%rsp), %eax
	cmpl	$64, %eax
	jne	.LBB20_21
# BB#19:
	movq	16(%rsp), %rax
	movq	%rax, 192(%rsp)
	leaq	52(%rsp), %rsi
.Ltmp165:
	leaq	192(%rsp), %rdi
	callq	_ZN8NWindows5NTime18FileTimeToUnixTimeERK9_FILETIMERj
.Ltmp166:
# BB#20:
	movl	%eax, %ecx
	xorb	$1, %cl
	movzbl	%cl, %ebx
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147024809, %r12d     # imm = 0x80070057
	cmovnel	%ecx, %r12d
.LBB20_21:
.Ltmp170:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp171:
# BB#22:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit112
	testl	%ebx, %ebx
	jne	.LBB20_129
# BB#23:
	movl	$0, 128(%rsp)
	movq	(%r13), %rax
.Ltmp173:
	leaq	128(%rsp), %rcx
	xorl	%esi, %esi
	movl	$3, %edx
	movq	%r13, %rdi
	callq	*64(%rax)
	movl	%eax, %ebx
.Ltmp174:
# BB#24:
	testl	%ebx, %ebx
	cmovnel	%ebx, %r12d
	movl	$1, %ebp
	jne	.LBB20_118
# BB#25:
	movzwl	128(%rsp), %eax
	testw	%ax, %ax
	je	.LBB20_117
# BB#26:
	movl	$-2147024809, %ebx      # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$8, %eax
	jne	.LBB20_118
# BB#27:
	movq	136(%rsp), %rbp
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 32(%rsp)
	movl	$-1, %ebx
	movq	%rbp, %rax
.LBB20_28:                              # =>This Inner Loop Header: Depth=1
	incl	%ebx
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB20_28
# BB#29:                                # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%rbx), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp175:
	callq	_Znam
.Ltmp176:
# BB#30:                                # %.noexc
	movq	%rax, 32(%rsp)
	movl	$0, (%rax)
	movl	24(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 44(%rsp)
	movq	%rax, %rcx
.LBB20_31:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %edx
	addq	$4, %rbp
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB20_31
# BB#32:
	movl	%ebx, 40(%rsp)
	testl	%ebx, %ebx
	je	.LBB20_89
# BB#33:
	movslq	%ebx, %rcx
	shlq	$2, %rcx
.LBB20_34:                              # =>This Inner Loop Header: Depth=1
	cmpl	$47, -4(%rax,%rcx)
	je	.LBB20_77
# BB#35:                                #   in Loop: Header=BB20_34 Depth=1
	addq	$-4, %rcx
	jne	.LBB20_34
	jmp	.LBB20_89
.LBB20_36:
	xorl	%r12d, %r12d
.LBB20_37:
	cmpl	$0, 172(%rsp)
	je	.LBB20_41
# BB#38:
	movl	$0, 112(%rsp)
	movq	(%r13), %rax
.Ltmp203:
	leaq	112(%rsp), %rcx
	xorl	%esi, %esi
	movl	$7, %edx
	movq	%r13, %rdi
	callq	*64(%rax)
.Ltmp204:
# BB#39:
	movl	$1, %ebp
	testl	%eax, %eax
	je	.LBB20_52
# BB#40:
                                        # implicit-def: %RBX
	movl	%eax, %r12d
	jmp	.LBB20_55
.LBB20_41:
	movl	$-2147024809, %r12d     # imm = 0x80070057
	cmpl	$0, 168(%rsp)
	jne	.LBB20_129
# BB#42:
	movq	120(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB20_76
# BB#43:
	movq	88(%r15), %rbx
	cmpl	$0, 108(%rsp)
	je	.LBB20_49
# BB#44:
	movw	$-29921, 8(%rsp)        # imm = 0x8B1F
	movb	48(%rsp), %al
	movb	%al, 10(%rsp)
	movb	49(%rsp), %al
	andb	$8, %al
	movb	%al, 11(%rsp)
	movl	52(%rsp), %eax
	movl	%eax, 12(%rsp)
	movb	50(%rsp), %al
	movb	%al, 16(%rsp)
	movb	51(%rsp), %al
	movb	%al, 17(%rsp)
.Ltmp288:
	leaq	8(%rsp), %rsi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
.Ltmp289:
# BB#45:                                # %.noexc147
	testl	%eax, %eax
	jne	.LBB20_48
# BB#46:
	testb	$8, 49(%rsp)
	je	.LBB20_48
# BB#47:
	movq	64(%rsp), %rsi
	movslq	72(%rsp), %rdx
	incq	%rdx
.Ltmp290:
	movq	%r14, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
.Ltmp291:
.LBB20_48:                              # %.noexc148
	addq	96(%r15), %rbx
	movq	120(%r15), %rdi
.LBB20_49:
	movq	(%rdi), %rax
.Ltmp292:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	callq	*48(%rax)
	movl	%eax, %r12d
.Ltmp293:
# BB#50:
	testl	%r12d, %r12d
	jne	.LBB20_129
# BB#51:
	movq	120(%r15), %rdi
.Ltmp294:
	xorl	%edx, %edx
	movq	%r14, %rsi
	callq	_ZN9NCompress10CopyStreamEP19ISequentialInStreamP20ISequentialOutStreamP21ICompressProgressInfo
	movl	%eax, %r12d
.Ltmp295:
	jmp	.LBB20_129
.LBB20_52:
	movzwl	112(%rsp), %eax
	cmpl	$21, %eax
	jne	.LBB20_54
# BB#53:
	xorl	%ebp, %ebp
	movq	120(%rsp), %rbx
	jmp	.LBB20_55
.LBB20_54:
	movl	$-2147024809, %r12d     # imm = 0x80070057
                                        # implicit-def: %RBX
.LBB20_55:
.Ltmp209:
	leaq	112(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp210:
# BB#56:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit143
	testl	%ebp, %ebp
	jne	.LBB20_129
# BB#57:
	movq	$0, 152(%rsp)
	movq	(%r13), %rax
.Ltmp212:
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	*40(%rax)
	movl	%eax, %r12d
.Ltmp213:
# BB#58:                                # %.noexc144
	testl	%r12d, %r12d
	jne	.LBB20_129
# BB#59:
	movq	(%r13), %rax
.Ltmp214:
	leaq	152(%rsp), %rsi
	movq	%r13, %rdi
	callq	*48(%rax)
	movl	%eax, %r12d
.Ltmp215:
# BB#60:                                # %.noexc145
	testl	%r12d, %r12d
	jne	.LBB20_129
# BB#61:
	movq	$0, 112(%rsp)
	movq	(%r13), %rax
.Ltmp216:
	leaq	112(%rsp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	*72(%rax)
	movl	%eax, %r12d
.Ltmp217:
# BB#62:
	testl	%r12d, %r12d
	jne	.LBB20_167
# BB#63:
.Ltmp218:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp219:
# BB#64:
	movl	$0, 8(%rbp)
	movq	$_ZTV26CSequentialInStreamWithCRC+16, (%rbp)
	movq	$0, 16(%rbp)
.Ltmp221:
	movq	%rbp, %rdi
	callq	*_ZTV26CSequentialInStreamWithCRC+24(%rip)
.Ltmp222:
# BB#65:                                # %_ZN9CMyComPtrI19ISequentialInStreamEC2EPS0_.exit.i
	movq	112(%rsp), %rbx
	testq	%rbx, %rbx
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	je	.LBB20_67
# BB#66:
	movq	(%rbx), %rax
.Ltmp224:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp225:
.LBB20_67:                              # %.noexc112.i
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_69
# BB#68:
	movq	(%rdi), %rax
.Ltmp226:
	callq	*16(%rax)
.Ltmp227:
.LBB20_69:
	movq	%rbx, 16(%rbp)
	movq	$0, 24(%rbp)
	movb	$0, 36(%rbp)
	movl	$-1, 32(%rbp)
.Ltmp228:
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp229:
# BB#70:
.Ltmp231:
	movq	%rbx, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp232:
# BB#71:
	movq	(%rbx), %rax
.Ltmp234:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp235:
# BB#72:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit.i
.Ltmp237:
	movl	$1, %edx
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp238:
# BB#73:
	movb	49(%rsp), %al
	movb	%al, 7(%rsp)            # 1-byte Spill
	movl	52(%rsp), %r12d
	movslq	72(%rsp), %rax
	leaq	1(%rax), %rdi
	testl	%edi, %edi
	je	.LBB20_134
# BB#74:
	cmpl	$-1, %eax
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp239:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp240:
# BB#75:                                # %.noexc115.i
	movb	$0, (%rbx)
	jmp	.LBB20_135
.LBB20_76:
	movl	$-2147467263, %r12d     # imm = 0x80004001
	jmp	.LBB20_129
.LBB20_77:                              # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
	leaq	-4(%rax,%rcx), %rdx
	subq	%rax, %rdx
	shrq	$2, %rdx
	testl	%edx, %edx
	js	.LBB20_89
# BB#78:
	subl	%edx, %ebx
	incl	%edx
	decl	%ebx
.Ltmp178:
	leaq	8(%rsp), %rdi
	leaq	32(%rsp), %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%ebx, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp179:
# BB#79:                                # %_ZNK11CStringBaseIwE3MidEi.exit
	movl	$0, 40(%rsp)
	movq	32(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	16(%rsp), %rbp
	incq	%rbp
	movl	44(%rsp), %eax
	cmpl	%eax, %ebp
	je	.LBB20_85
# BB#80:
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp181:
	callq	_Znam
.Ltmp182:
# BB#81:                                # %.noexc118
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB20_84
# BB#82:                                # %.noexc118
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB20_84
# BB#83:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	40(%rsp), %rcx
.LBB20_84:                              # %._crit_edge16.i.i
	movq	%rax, 32(%rsp)
	movl	$0, (%rax,%rcx,4)
	movl	%ebp, 44(%rsp)
	movq	%rax, %rbx
.LBB20_85:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i115
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
.LBB20_86:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB20_86
# BB#87:
	movl	16(%rsp), %eax
	movl	%eax, 40(%rsp)
	testq	%rdi, %rdi
	je	.LBB20_89
# BB#88:
	callq	_ZdaPv
.LBB20_89:                              # %_ZNK11CStringBaseIwE11ReverseFindEw.exit.thread
.Ltmp184:
	leaq	8(%rsp), %rdi
	leaq	32(%rsp), %rsi
	xorl	%edx, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp185:
# BB#90:
	movl	$0, 72(%rsp)
	movq	64(%rsp), %rax
	movb	$0, (%rax)
	movslq	16(%rsp), %rax
	leaq	1(%rax), %rcx
	movl	76(%rsp), %ebp
	cmpl	%ebp, %ecx
	jne	.LBB20_92
# BB#91:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	64(%rsp), %rbx
	jmp	.LBB20_109
.LBB20_92:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	cmovlq	%rax, %rdi
.Ltmp187:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp188:
# BB#93:                                # %.noexc126
	testl	%ebp, %ebp
	jle	.LBB20_108
# BB#94:                                # %.preheader.i.i
	movslq	72(%rsp), %r8
	testq	%r8, %r8
	movq	64(%rsp), %rdi
	jle	.LBB20_106
# BB#95:                                # %.lr.ph.preheader.i.i
	cmpl	$31, %r8d
	jbe	.LBB20_99
# BB#96:                                # %min.iters.checked
	movq	%r8, %rcx
	andq	$-32, %rcx
	je	.LBB20_99
# BB#97:                                # %vector.memcheck
	leaq	(%rdi,%r8), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB20_169
# BB#98:                                # %vector.memcheck
	leaq	(%rbx,%r8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB20_169
.LBB20_99:
	xorl	%ecx, %ecx
.LBB20_100:                             # %.lr.ph.i.i.preheader
	movl	%r8d, %esi
	subl	%ecx, %esi
	leaq	-1(%r8), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB20_103
# BB#101:                               # %.lr.ph.i.i.prol.preheader
	negq	%rsi
.LBB20_102:                             # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB20_102
.LBB20_103:                             # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB20_107
# BB#104:                               # %.lr.ph.i.i.preheader.new
	subq	%rcx, %r8
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
.LBB20_105:                             # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r8
	jne	.LBB20_105
	jmp	.LBB20_107
.LBB20_106:                             # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB20_108
.LBB20_107:                             # %._crit_edge.thread.i.i123
	callq	_ZdaPv
.LBB20_108:                             # %._crit_edge17.i.i
	movq	%rbx, 64(%rsp)
	movslq	72(%rsp), %rax
	movb	$0, (%rbx,%rax)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 76(%rsp)
.LBB20_109:                             # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	8(%rsp), %rax
.LBB20_110:                             # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB20_110
# BB#111:
	movl	16(%rsp), %eax
	movl	%eax, 72(%rsp)
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_113
# BB#112:
	callq	_ZdaPv
	movl	72(%rsp), %eax
.LBB20_113:                             # %_ZN11CStringBaseIcED2Ev.exit
	testl	%eax, %eax
	je	.LBB20_115
# BB#114:
	orb	$8, 49(%rsp)
.LBB20_115:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_117
# BB#116:
	callq	_ZdaPv
.LBB20_117:
	xorl	%ebp, %ebp
	movl	%r12d, %ebx
.LBB20_118:
.Ltmp192:
	leaq	128(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp193:
# BB#119:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit131
	testl	%ebp, %ebp
	je	.LBB20_121
# BB#120:
	movl	%ebx, %r12d
	jmp	.LBB20_129
.LBB20_121:
	movl	$0, 152(%rsp)
	movq	(%r13), %rax
.Ltmp195:
	leaq	152(%rsp), %rcx
	xorl	%esi, %esi
	movl	$6, %edx
	movq	%r13, %rdi
	callq	*64(%rax)
	movl	%eax, %r12d
.Ltmp196:
# BB#122:
	testl	%r12d, %r12d
	cmovnel	%r12d, %ebx
	movl	$1, %ebp
	jne	.LBB20_127
# BB#123:
	movzwl	152(%rsp), %eax
	testw	%ax, %ax
	je	.LBB20_126
# BB#124:
	movl	$-2147024809, %r12d     # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$11, %eax
	jne	.LBB20_127
# BB#125:
	cmpw	$0, 160(%rsp)
	jne	.LBB20_127
.LBB20_126:
	xorl	%ebp, %ebp
	movl	%ebx, %r12d
.LBB20_127:
.Ltmp200:
	leaq	152(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp201:
# BB#128:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit137
	testl	%ebp, %ebp
	je	.LBB20_37
.LBB20_129:
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_131
# BB#130:
	callq	_ZdaPv
.LBB20_131:                             # %_ZN11CStringBaseIcED2Ev.exit.i138
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_133
# BB#132:
	callq	_ZdaPv
.LBB20_133:
	movl	%r12d, %eax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB20_134:
	xorl	%ebx, %ebx
.LBB20_135:                             # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i.i
	leaq	144(%r15), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	64(%rsp), %rax
	xorl	%ecx, %ecx
.LBB20_136:                             # =>This Inner Loop Header: Depth=1
	movzbl	(%rax,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB20_136
# BB#137:                               # %_ZN11CStringBaseIcEC2ERKS0_.exit.i.i
	movslq	72(%rsp), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movslq	88(%rsp), %rax
	leaq	1(%rax), %rdi
	testl	%edi, %edi
	je	.LBB20_140
# BB#138:
	cmpl	$-1, %eax
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp242:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp243:
# BB#139:                               # %.noexc.i.i
	movb	$0, (%rbp)
	jmp	.LBB20_141
.LBB20_140:
	xorl	%ebp, %ebp
.LBB20_141:                             # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i5.i.i
	movq	80(%rsp), %rax
	xorl	%ecx, %ecx
.LBB20_142:                             # =>This Inner Loop Header: Depth=1
	movzbl	(%rax,%rcx), %edx
	movb	%dl, (%rbp,%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB20_142
# BB#143:                               # %_ZN8NArchive3NGz5CItemC2ERKS1_.exit.i
	cmpl	$0, 156(%r15)
	movb	$2, %al
	jne	.LBB20_145
# BB#144:                               # %_ZN8NArchive3NGz5CItemC2ERKS1_.exit.i
	movb	$4, %al
.LBB20_145:                             # %_ZN8NArchive3NGz5CItemC2ERKS1_.exit.i
	movw	$-29921, 8(%rsp)        # imm = 0x8B1F
	movb	$8, 10(%rsp)
	movb	7(%rsp), %r15b          # 1-byte Reload
	andb	$8, %r15b
	movb	%r15b, 11(%rsp)
	movl	%r12d, 12(%rsp)
	movb	%al, 16(%rsp)
	movb	$3, 17(%rsp)
.Ltmp245:
	leaq	8(%rsp), %rsi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	%eax, %r12d
.Ltmp246:
# BB#146:                               # %.noexc116.i
	testl	%r12d, %r12d
	jne	.LBB20_161
# BB#147:
	testb	%r15b, %r15b
	je	.LBB20_150
# BB#148:
	movq	176(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
.Ltmp247:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	%eax, %r12d
.Ltmp248:
# BB#149:                               # %.noexc117.i
	testl	%r12d, %r12d
	jne	.LBB20_161
.LBB20_150:
.Ltmp249:
	movl	$39792, %edi            # imm = 0x9B70
	callq	_Znwm
	movq	%rax, %r15
.Ltmp250:
# BB#151:
	movl	$_ZTV27ICompressSetCoderProperties+16, %eax
	movd	%rax, %xmm0
	movl	$_ZTV14ICompressCoder+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r15)
	movl	$0, 16(%r15)
	leaq	24(%r15), %rdi
.Ltmp252:
	xorl	%esi, %esi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoderC2Eb
.Ltmp253:
# BB#152:
	movl	$_ZTVN9NCompress8NDeflate8NEncoder9CCOMCoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NEncoder9CCOMCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r15)
.Ltmp255:
	movq	%r15, %rdi
	callq	*_ZTVN9NCompress8NDeflate8NEncoder9CCOMCoderE+24(%rip)
.Ltmp256:
# BB#153:                               # %_ZN9CMyComPtrI14ICompressCoderEC2EPS0_.exit.i
	movq	%r15, %rsi
	addq	$8, %rsi
.Ltmp258:
	movq	184(%rsp), %rdi         # 8-byte Reload
	callq	_ZN8NArchive13CDeflateProps18SetCoderPropertiesEP27ICompressSetCoderProperties
	movl	%eax, %r12d
.Ltmp259:
# BB#154:
	testl	%r12d, %r12d
	jne	.LBB20_160
# BB#155:
	movq	(%r15), %rax
.Ltmp260:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	movq	144(%rsp), %r9          # 8-byte Reload
	callq	*40(%rax)
	movl	%eax, %r12d
.Ltmp261:
# BB#156:
	testl	%r12d, %r12d
	jne	.LBB20_160
# BB#157:
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	24(%rcx), %eax
	movl	32(%rcx), %ecx
	notl	%ecx
	movl	%ecx, 128(%rsp)
	movl	%eax, 132(%rsp)
.Ltmp262:
	leaq	128(%rsp), %rsi
	movl	$8, %edx
	movq	%r14, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	%eax, %r12d
.Ltmp263:
# BB#158:
	testl	%r12d, %r12d
	jne	.LBB20_160
# BB#159:
	movq	(%r13), %rax
.Ltmp264:
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	*80(%rax)
	movl	%eax, %r12d
.Ltmp265:
.LBB20_160:
	movq	(%r15), %rax
.Ltmp269:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp270:
.LBB20_161:                             # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit127.i
	testq	%rbp, %rbp
	je	.LBB20_163
# BB#162:
	movq	%rbp, %rdi
	callq	_ZdaPv
.LBB20_163:                             # %_ZN11CStringBaseIcED2Ev.exit.i128.i
	testq	%rbx, %rbx
	je	.LBB20_165
# BB#164:
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB20_165:                             # %_ZN8NArchive3NGz5CItemD2Ev.exit129.i
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp274:
	callq	*16(%rax)
.Ltmp275:
# BB#166:                               # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit131.i
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp279:
	callq	*16(%rax)
.Ltmp280:
.LBB20_167:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit123.i
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_129
# BB#168:
	movq	(%rdi), %rax
.Ltmp285:
	callq	*16(%rax)
.Ltmp286:
	jmp	.LBB20_129
.LBB20_169:                             # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB20_172
# BB#170:                               # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB20_171:                             # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rdi,%rbp), %xmm0
	movdqu	16(%rdi,%rbp), %xmm1
	movdqu	%xmm0, (%rbx,%rbp)
	movdqu	%xmm1, 16(%rbx,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB20_171
	jmp	.LBB20_173
.LBB20_172:
	xorl	%ebp, %ebp
.LBB20_173:                             # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB20_176
# BB#174:                               # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%rbx,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB20_175:                             # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbp), %xmm0
	movdqu	(%rbp), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB20_175
.LBB20_176:                             # %middle.block
	cmpq	%rcx, %r8
	jne	.LBB20_100
	jmp	.LBB20_107
.LBB20_177:
.Ltmp271:
	jmp	.LBB20_190
.LBB20_178:
.Ltmp257:
	jmp	.LBB20_190
.LBB20_179:
.Ltmp254:
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	_ZdlPv
	testq	%rbp, %rbp
	jne	.LBB20_192
	jmp	.LBB20_193
.LBB20_180:
.Ltmp266:
	movq	%rax, %r14
	movq	(%r15), %rax
.Ltmp267:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp268:
	jmp	.LBB20_191
.LBB20_181:
.Ltmp183:
	jmp	.LBB20_201
.LBB20_182:
.Ltmp244:
	movq	%rax, %r14
	testq	%rbx, %rbx
	jne	.LBB20_194
	jmp	.LBB20_196
.LBB20_183:
.Ltmp281:
	jmp	.LBB20_204
.LBB20_184:
.Ltmp276:
	jmp	.LBB20_198
.LBB20_185:
.Ltmp236:
	jmp	.LBB20_198
.LBB20_186:
.Ltmp233:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB20_199
.LBB20_187:
.Ltmp223:
	jmp	.LBB20_204
.LBB20_188:
.Ltmp180:
	jmp	.LBB20_209
.LBB20_189:
.Ltmp251:
.LBB20_190:                             # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit.i
	movq	%rax, %r14
.LBB20_191:                             # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit.i
	testq	%rbp, %rbp
	je	.LBB20_193
.LBB20_192:
	movq	%rbp, %rdi
	callq	_ZdaPv
.LBB20_193:                             # %_ZN11CStringBaseIcED2Ev.exit.i124.i
	testq	%rbx, %rbx
	je	.LBB20_196
.LBB20_194:
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB20_196
.LBB20_195:
.Ltmp241:
	movq	%rax, %r14
.LBB20_196:                             # %_ZN8NArchive3NGz5CItemD2Ev.exit.i
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp272:
	callq	*16(%rax)
.Ltmp273:
	jmp	.LBB20_199
.LBB20_197:
.Ltmp230:
.LBB20_198:                             # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.i
	movq	%rax, %r14
.LBB20_199:                             # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.i
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp277:
	callq	*16(%rax)
.Ltmp278:
	jmp	.LBB20_205
.LBB20_200:
.Ltmp189:
.LBB20_201:
	movq	%rax, %r14
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_210
# BB#202:
	callq	_ZdaPv
	jmp	.LBB20_210
.LBB20_203:
.Ltmp220:
.LBB20_204:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit111.i
	movq	%rax, %r14
.LBB20_205:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit111.i
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_225
# BB#206:
	movq	(%rdi), %rax
.Ltmp282:
	callq	*16(%rax)
.Ltmp283:
	jmp	.LBB20_225
.LBB20_207:
.Ltmp284:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB20_208:
.Ltmp186:
.LBB20_209:
	movq	%rax, %r14
.LBB20_210:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_220
# BB#211:
	callq	_ZdaPv
	jmp	.LBB20_220
.LBB20_212:
.Ltmp296:
	jmp	.LBB20_222
.LBB20_213:
.Ltmp202:
	jmp	.LBB20_222
.LBB20_214:
.Ltmp197:
	movq	%rax, %r14
.Ltmp198:
	leaq	152(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp199:
	jmp	.LBB20_225
.LBB20_215:
.Ltmp287:
	jmp	.LBB20_222
.LBB20_216:
.Ltmp194:
	jmp	.LBB20_222
.LBB20_217:
.Ltmp211:
	jmp	.LBB20_222
.LBB20_218:
.Ltmp205:
	movq	%rax, %r14
.Ltmp206:
	leaq	112(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp207:
	jmp	.LBB20_225
.LBB20_219:
.Ltmp177:
	movq	%rax, %r14
.LBB20_220:
.Ltmp190:
	leaq	128(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp191:
	jmp	.LBB20_225
.LBB20_221:
.Ltmp172:
.LBB20_222:
	movq	%rax, %r14
	jmp	.LBB20_225
.LBB20_223:
.Ltmp162:
	movq	%rax, %r14
	jmp	.LBB20_227
.LBB20_224:
.Ltmp167:
	movq	%rax, %r14
.Ltmp168:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp169:
.LBB20_225:
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_227
# BB#226:
	callq	_ZdaPv
.LBB20_227:                             # %_ZN11CStringBaseIcED2Ev.exit.i113
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_229
# BB#228:
	callq	_ZdaPv
.LBB20_229:                             # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB20_230:
.Ltmp208:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end20:
	.size	_ZN8NArchive3NGz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback, .Lfunc_end20-_ZN8NArchive3NGz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\203"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\272\003"              # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp160-.Lfunc_begin5  #   Call between .Lfunc_begin5 and .Ltmp160
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp160-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp162-.Lfunc_begin5  #     jumps to .Ltmp162
	.byte	0                       #   On action: cleanup
	.long	.Ltmp163-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp166-.Ltmp163       #   Call between .Ltmp163 and .Ltmp166
	.long	.Ltmp167-.Lfunc_begin5  #     jumps to .Ltmp167
	.byte	0                       #   On action: cleanup
	.long	.Ltmp170-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp171-.Ltmp170       #   Call between .Ltmp170 and .Ltmp171
	.long	.Ltmp172-.Lfunc_begin5  #     jumps to .Ltmp172
	.byte	0                       #   On action: cleanup
	.long	.Ltmp173-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp176-.Ltmp173       #   Call between .Ltmp173 and .Ltmp176
	.long	.Ltmp177-.Lfunc_begin5  #     jumps to .Ltmp177
	.byte	0                       #   On action: cleanup
	.long	.Ltmp203-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp204-.Ltmp203       #   Call between .Ltmp203 and .Ltmp204
	.long	.Ltmp205-.Lfunc_begin5  #     jumps to .Ltmp205
	.byte	0                       #   On action: cleanup
	.long	.Ltmp288-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp295-.Ltmp288       #   Call between .Ltmp288 and .Ltmp295
	.long	.Ltmp296-.Lfunc_begin5  #     jumps to .Ltmp296
	.byte	0                       #   On action: cleanup
	.long	.Ltmp209-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp210-.Ltmp209       #   Call between .Ltmp209 and .Ltmp210
	.long	.Ltmp211-.Lfunc_begin5  #     jumps to .Ltmp211
	.byte	0                       #   On action: cleanup
	.long	.Ltmp212-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp215-.Ltmp212       #   Call between .Ltmp212 and .Ltmp215
	.long	.Ltmp287-.Lfunc_begin5  #     jumps to .Ltmp287
	.byte	0                       #   On action: cleanup
	.long	.Ltmp216-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp219-.Ltmp216       #   Call between .Ltmp216 and .Ltmp219
	.long	.Ltmp220-.Lfunc_begin5  #     jumps to .Ltmp220
	.byte	0                       #   On action: cleanup
	.long	.Ltmp221-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Ltmp222-.Ltmp221       #   Call between .Ltmp221 and .Ltmp222
	.long	.Ltmp223-.Lfunc_begin5  #     jumps to .Ltmp223
	.byte	0                       #   On action: cleanup
	.long	.Ltmp224-.Lfunc_begin5  # >> Call Site 12 <<
	.long	.Ltmp229-.Ltmp224       #   Call between .Ltmp224 and .Ltmp229
	.long	.Ltmp230-.Lfunc_begin5  #     jumps to .Ltmp230
	.byte	0                       #   On action: cleanup
	.long	.Ltmp231-.Lfunc_begin5  # >> Call Site 13 <<
	.long	.Ltmp232-.Ltmp231       #   Call between .Ltmp231 and .Ltmp232
	.long	.Ltmp233-.Lfunc_begin5  #     jumps to .Ltmp233
	.byte	0                       #   On action: cleanup
	.long	.Ltmp234-.Lfunc_begin5  # >> Call Site 14 <<
	.long	.Ltmp235-.Ltmp234       #   Call between .Ltmp234 and .Ltmp235
	.long	.Ltmp236-.Lfunc_begin5  #     jumps to .Ltmp236
	.byte	0                       #   On action: cleanup
	.long	.Ltmp237-.Lfunc_begin5  # >> Call Site 15 <<
	.long	.Ltmp240-.Ltmp237       #   Call between .Ltmp237 and .Ltmp240
	.long	.Ltmp241-.Lfunc_begin5  #     jumps to .Ltmp241
	.byte	0                       #   On action: cleanup
	.long	.Ltmp178-.Lfunc_begin5  # >> Call Site 16 <<
	.long	.Ltmp179-.Ltmp178       #   Call between .Ltmp178 and .Ltmp179
	.long	.Ltmp180-.Lfunc_begin5  #     jumps to .Ltmp180
	.byte	0                       #   On action: cleanup
	.long	.Ltmp181-.Lfunc_begin5  # >> Call Site 17 <<
	.long	.Ltmp182-.Ltmp181       #   Call between .Ltmp181 and .Ltmp182
	.long	.Ltmp183-.Lfunc_begin5  #     jumps to .Ltmp183
	.byte	0                       #   On action: cleanup
	.long	.Ltmp184-.Lfunc_begin5  # >> Call Site 18 <<
	.long	.Ltmp185-.Ltmp184       #   Call between .Ltmp184 and .Ltmp185
	.long	.Ltmp186-.Lfunc_begin5  #     jumps to .Ltmp186
	.byte	0                       #   On action: cleanup
	.long	.Ltmp187-.Lfunc_begin5  # >> Call Site 19 <<
	.long	.Ltmp188-.Ltmp187       #   Call between .Ltmp187 and .Ltmp188
	.long	.Ltmp189-.Lfunc_begin5  #     jumps to .Ltmp189
	.byte	0                       #   On action: cleanup
	.long	.Ltmp192-.Lfunc_begin5  # >> Call Site 20 <<
	.long	.Ltmp193-.Ltmp192       #   Call between .Ltmp192 and .Ltmp193
	.long	.Ltmp194-.Lfunc_begin5  #     jumps to .Ltmp194
	.byte	0                       #   On action: cleanup
	.long	.Ltmp195-.Lfunc_begin5  # >> Call Site 21 <<
	.long	.Ltmp196-.Ltmp195       #   Call between .Ltmp195 and .Ltmp196
	.long	.Ltmp197-.Lfunc_begin5  #     jumps to .Ltmp197
	.byte	0                       #   On action: cleanup
	.long	.Ltmp200-.Lfunc_begin5  # >> Call Site 22 <<
	.long	.Ltmp201-.Ltmp200       #   Call between .Ltmp200 and .Ltmp201
	.long	.Ltmp202-.Lfunc_begin5  #     jumps to .Ltmp202
	.byte	0                       #   On action: cleanup
	.long	.Ltmp242-.Lfunc_begin5  # >> Call Site 23 <<
	.long	.Ltmp243-.Ltmp242       #   Call between .Ltmp242 and .Ltmp243
	.long	.Ltmp244-.Lfunc_begin5  #     jumps to .Ltmp244
	.byte	0                       #   On action: cleanup
	.long	.Ltmp245-.Lfunc_begin5  # >> Call Site 24 <<
	.long	.Ltmp250-.Ltmp245       #   Call between .Ltmp245 and .Ltmp250
	.long	.Ltmp251-.Lfunc_begin5  #     jumps to .Ltmp251
	.byte	0                       #   On action: cleanup
	.long	.Ltmp252-.Lfunc_begin5  # >> Call Site 25 <<
	.long	.Ltmp253-.Ltmp252       #   Call between .Ltmp252 and .Ltmp253
	.long	.Ltmp254-.Lfunc_begin5  #     jumps to .Ltmp254
	.byte	0                       #   On action: cleanup
	.long	.Ltmp255-.Lfunc_begin5  # >> Call Site 26 <<
	.long	.Ltmp256-.Ltmp255       #   Call between .Ltmp255 and .Ltmp256
	.long	.Ltmp257-.Lfunc_begin5  #     jumps to .Ltmp257
	.byte	0                       #   On action: cleanup
	.long	.Ltmp258-.Lfunc_begin5  # >> Call Site 27 <<
	.long	.Ltmp265-.Ltmp258       #   Call between .Ltmp258 and .Ltmp265
	.long	.Ltmp266-.Lfunc_begin5  #     jumps to .Ltmp266
	.byte	0                       #   On action: cleanup
	.long	.Ltmp269-.Lfunc_begin5  # >> Call Site 28 <<
	.long	.Ltmp270-.Ltmp269       #   Call between .Ltmp269 and .Ltmp270
	.long	.Ltmp271-.Lfunc_begin5  #     jumps to .Ltmp271
	.byte	0                       #   On action: cleanup
	.long	.Ltmp274-.Lfunc_begin5  # >> Call Site 29 <<
	.long	.Ltmp275-.Ltmp274       #   Call between .Ltmp274 and .Ltmp275
	.long	.Ltmp276-.Lfunc_begin5  #     jumps to .Ltmp276
	.byte	0                       #   On action: cleanup
	.long	.Ltmp279-.Lfunc_begin5  # >> Call Site 30 <<
	.long	.Ltmp280-.Ltmp279       #   Call between .Ltmp279 and .Ltmp280
	.long	.Ltmp281-.Lfunc_begin5  #     jumps to .Ltmp281
	.byte	0                       #   On action: cleanup
	.long	.Ltmp285-.Lfunc_begin5  # >> Call Site 31 <<
	.long	.Ltmp286-.Ltmp285       #   Call between .Ltmp285 and .Ltmp286
	.long	.Ltmp287-.Lfunc_begin5  #     jumps to .Ltmp287
	.byte	0                       #   On action: cleanup
	.long	.Ltmp267-.Lfunc_begin5  # >> Call Site 32 <<
	.long	.Ltmp283-.Ltmp267       #   Call between .Ltmp267 and .Ltmp283
	.long	.Ltmp284-.Lfunc_begin5  #     jumps to .Ltmp284
	.byte	1                       #   On action: 1
	.long	.Ltmp198-.Lfunc_begin5  # >> Call Site 33 <<
	.long	.Ltmp169-.Ltmp198       #   Call between .Ltmp198 and .Ltmp169
	.long	.Ltmp208-.Lfunc_begin5  #     jumps to .Ltmp208
	.byte	1                       #   On action: 1
	.long	.Ltmp169-.Lfunc_begin5  # >> Call Site 34 <<
	.long	.Lfunc_end20-.Ltmp169   #   Call between .Ltmp169 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn16_N8NArchive3NGz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive3NGz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback,@function
_ZThn16_N8NArchive3NGz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback: # @_ZThn16_N8NArchive3NGz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN8NArchive3NGz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback # TAILCALL
.Lfunc_end21:
	.size	_ZThn16_N8NArchive3NGz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback, .Lfunc_end21-_ZThn16_N8NArchive3NGz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_endproc

	.globl	_ZN8NArchive3NGz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi,@function
_ZN8NArchive3NGz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi: # @_ZN8NArchive3NGz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_startproc
# BB#0:
	addq	$144, %rdi
	jmp	_ZN8NArchive13CDeflateProps13SetPropertiesEPPKwPK14tagPROPVARIANTi # TAILCALL
.Lfunc_end22:
	.size	_ZN8NArchive3NGz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi, .Lfunc_end22-_ZN8NArchive3NGz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_endproc

	.globl	_ZThn24_N8NArchive3NGz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive3NGz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi,@function
_ZThn24_N8NArchive3NGz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi: # @_ZThn24_N8NArchive3NGz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_startproc
# BB#0:
	addq	$120, %rdi
	jmp	_ZN8NArchive13CDeflateProps13SetPropertiesEPPKwPK14tagPROPVARIANTi # TAILCALL
.Lfunc_end23:
	.size	_ZThn24_N8NArchive3NGz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi, .Lfunc_end23-_ZThn24_N8NArchive3NGz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_endproc

	.section	.text._ZN8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi80:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB24_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB24_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB24_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB24_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB24_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB24_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB24_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB24_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB24_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB24_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB24_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB24_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB24_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB24_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB24_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB24_16
.LBB24_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_IInArchive(%rip), %cl
	jne	.LBB24_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_IInArchive+1(%rip), %al
	jne	.LBB24_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_IInArchive+2(%rip), %al
	jne	.LBB24_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_IInArchive+3(%rip), %al
	jne	.LBB24_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_IInArchive+4(%rip), %al
	jne	.LBB24_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_IInArchive+5(%rip), %al
	jne	.LBB24_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_IInArchive+6(%rip), %al
	jne	.LBB24_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_IInArchive+7(%rip), %al
	jne	.LBB24_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_IInArchive+8(%rip), %al
	jne	.LBB24_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_IInArchive+9(%rip), %al
	jne	.LBB24_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_IInArchive+10(%rip), %al
	jne	.LBB24_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_IInArchive+11(%rip), %al
	jne	.LBB24_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_IInArchive+12(%rip), %al
	jne	.LBB24_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_IInArchive+13(%rip), %al
	jne	.LBB24_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_IInArchive+14(%rip), %al
	jne	.LBB24_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit12
	movb	15(%rsi), %al
	cmpb	IID_IInArchive+15(%rip), %al
	jne	.LBB24_33
.LBB24_16:
	movq	%rdi, (%rdx)
	jmp	.LBB24_85
.LBB24_33:                              # %_ZeqRK4GUIDS1_.exit12.thread
	cmpb	IID_IArchiveOpenSeq(%rip), %cl
	jne	.LBB24_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+1(%rip), %al
	jne	.LBB24_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+2(%rip), %al
	jne	.LBB24_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+3(%rip), %al
	jne	.LBB24_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+4(%rip), %al
	jne	.LBB24_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+5(%rip), %al
	jne	.LBB24_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+6(%rip), %al
	jne	.LBB24_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+7(%rip), %al
	jne	.LBB24_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+8(%rip), %al
	jne	.LBB24_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+9(%rip), %al
	jne	.LBB24_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+10(%rip), %al
	jne	.LBB24_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+11(%rip), %al
	jne	.LBB24_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+12(%rip), %al
	jne	.LBB24_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+13(%rip), %al
	jne	.LBB24_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+14(%rip), %al
	jne	.LBB24_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit16
	movb	15(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+15(%rip), %al
	jne	.LBB24_50
# BB#49:
	leaq	8(%rdi), %rax
	jmp	.LBB24_84
.LBB24_50:                              # %_ZeqRK4GUIDS1_.exit16.thread
	cmpb	IID_IOutArchive(%rip), %cl
	jne	.LBB24_67
# BB#51:
	movb	1(%rsi), %al
	cmpb	IID_IOutArchive+1(%rip), %al
	jne	.LBB24_67
# BB#52:
	movb	2(%rsi), %al
	cmpb	IID_IOutArchive+2(%rip), %al
	jne	.LBB24_67
# BB#53:
	movb	3(%rsi), %al
	cmpb	IID_IOutArchive+3(%rip), %al
	jne	.LBB24_67
# BB#54:
	movb	4(%rsi), %al
	cmpb	IID_IOutArchive+4(%rip), %al
	jne	.LBB24_67
# BB#55:
	movb	5(%rsi), %al
	cmpb	IID_IOutArchive+5(%rip), %al
	jne	.LBB24_67
# BB#56:
	movb	6(%rsi), %al
	cmpb	IID_IOutArchive+6(%rip), %al
	jne	.LBB24_67
# BB#57:
	movb	7(%rsi), %al
	cmpb	IID_IOutArchive+7(%rip), %al
	jne	.LBB24_67
# BB#58:
	movb	8(%rsi), %al
	cmpb	IID_IOutArchive+8(%rip), %al
	jne	.LBB24_67
# BB#59:
	movb	9(%rsi), %al
	cmpb	IID_IOutArchive+9(%rip), %al
	jne	.LBB24_67
# BB#60:
	movb	10(%rsi), %al
	cmpb	IID_IOutArchive+10(%rip), %al
	jne	.LBB24_67
# BB#61:
	movb	11(%rsi), %al
	cmpb	IID_IOutArchive+11(%rip), %al
	jne	.LBB24_67
# BB#62:
	movb	12(%rsi), %al
	cmpb	IID_IOutArchive+12(%rip), %al
	jne	.LBB24_67
# BB#63:
	movb	13(%rsi), %al
	cmpb	IID_IOutArchive+13(%rip), %al
	jne	.LBB24_67
# BB#64:
	movb	14(%rsi), %al
	cmpb	IID_IOutArchive+14(%rip), %al
	jne	.LBB24_67
# BB#65:                                # %_ZeqRK4GUIDS1_.exit18
	movb	15(%rsi), %al
	cmpb	IID_IOutArchive+15(%rip), %al
	jne	.LBB24_67
# BB#66:
	leaq	16(%rdi), %rax
	jmp	.LBB24_84
.LBB24_67:                              # %_ZeqRK4GUIDS1_.exit18.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ISetProperties(%rip), %cl
	jne	.LBB24_86
# BB#68:
	movb	1(%rsi), %cl
	cmpb	IID_ISetProperties+1(%rip), %cl
	jne	.LBB24_86
# BB#69:
	movb	2(%rsi), %cl
	cmpb	IID_ISetProperties+2(%rip), %cl
	jne	.LBB24_86
# BB#70:
	movb	3(%rsi), %cl
	cmpb	IID_ISetProperties+3(%rip), %cl
	jne	.LBB24_86
# BB#71:
	movb	4(%rsi), %cl
	cmpb	IID_ISetProperties+4(%rip), %cl
	jne	.LBB24_86
# BB#72:
	movb	5(%rsi), %cl
	cmpb	IID_ISetProperties+5(%rip), %cl
	jne	.LBB24_86
# BB#73:
	movb	6(%rsi), %cl
	cmpb	IID_ISetProperties+6(%rip), %cl
	jne	.LBB24_86
# BB#74:
	movb	7(%rsi), %cl
	cmpb	IID_ISetProperties+7(%rip), %cl
	jne	.LBB24_86
# BB#75:
	movb	8(%rsi), %cl
	cmpb	IID_ISetProperties+8(%rip), %cl
	jne	.LBB24_86
# BB#76:
	movb	9(%rsi), %cl
	cmpb	IID_ISetProperties+9(%rip), %cl
	jne	.LBB24_86
# BB#77:
	movb	10(%rsi), %cl
	cmpb	IID_ISetProperties+10(%rip), %cl
	jne	.LBB24_86
# BB#78:
	movb	11(%rsi), %cl
	cmpb	IID_ISetProperties+11(%rip), %cl
	jne	.LBB24_86
# BB#79:
	movb	12(%rsi), %cl
	cmpb	IID_ISetProperties+12(%rip), %cl
	jne	.LBB24_86
# BB#80:
	movb	13(%rsi), %cl
	cmpb	IID_ISetProperties+13(%rip), %cl
	jne	.LBB24_86
# BB#81:
	movb	14(%rsi), %cl
	cmpb	IID_ISetProperties+14(%rip), %cl
	jne	.LBB24_86
# BB#82:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %cl
	cmpb	IID_ISetProperties+15(%rip), %cl
	jne	.LBB24_86
# BB#83:
	leaq	24(%rdi), %rax
.LBB24_84:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movq	%rax, (%rdx)
.LBB24_85:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB24_86:                              # %_ZeqRK4GUIDS1_.exit14.thread
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZN8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end24-_ZN8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive3NGz8CHandler6AddRefEv,"axG",@progbits,_ZN8NArchive3NGz8CHandler6AddRefEv,comdat
	.weak	_ZN8NArchive3NGz8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler6AddRefEv,@function
_ZN8NArchive3NGz8CHandler6AddRefEv:     # @_ZN8NArchive3NGz8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	32(%rdi), %eax
	incl	%eax
	movl	%eax, 32(%rdi)
	retq
.Lfunc_end25:
	.size	_ZN8NArchive3NGz8CHandler6AddRefEv, .Lfunc_end25-_ZN8NArchive3NGz8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive3NGz8CHandler7ReleaseEv,"axG",@progbits,_ZN8NArchive3NGz8CHandler7ReleaseEv,comdat
	.weak	_ZN8NArchive3NGz8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandler7ReleaseEv,@function
_ZN8NArchive3NGz8CHandler7ReleaseEv:    # @_ZN8NArchive3NGz8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi81:
	.cfi_def_cfa_offset 16
	movl	32(%rdi), %eax
	decl	%eax
	movl	%eax, 32(%rdi)
	jne	.LBB26_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB26_2:
	popq	%rcx
	retq
.Lfunc_end26:
	.size	_ZN8NArchive3NGz8CHandler7ReleaseEv, .Lfunc_end26-_ZN8NArchive3NGz8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive3NGz8CHandlerD2Ev,"axG",@progbits,_ZN8NArchive3NGz8CHandlerD2Ev,comdat
	.weak	_ZN8NArchive3NGz8CHandlerD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandlerD2Ev,@function
_ZN8NArchive3NGz8CHandlerD2Ev:          # @_ZN8NArchive3NGz8CHandlerD2Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 32
.Lcfi85:
	.cfi_offset %rbx, -24
.Lcfi86:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive3NGz8CHandlerE+184, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3NGz8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN8NArchive3NGz8CHandlerE+320, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3NGz8CHandlerE+248, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movq	128(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp297:
	callq	*16(%rax)
.Ltmp298:
.LBB27_2:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp303:
	callq	*16(%rax)
.Ltmp304:
.LBB27_4:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_6
# BB#5:
	callq	_ZdaPv
.LBB27_6:                               # %_ZN11CStringBaseIcED2Ev.exit.i
	movq	56(%rbx), %rdi
	addq	$8, %rsp
	testq	%rdi, %rdi
	je	.LBB27_7
# BB#17:
	popq	%rbx
	popq	%r14
	jmp	_ZdaPv                  # TAILCALL
.LBB27_7:                               # %_ZN8NArchive3NGz5CItemD2Ev.exit
	popq	%rbx
	popq	%r14
	retq
.LBB27_10:
.Ltmp305:
	movq	%rax, %r14
	jmp	.LBB27_11
.LBB27_8:
.Ltmp299:
	movq	%rax, %r14
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_11
# BB#9:
	movq	(%rdi), %rax
.Ltmp300:
	callq	*16(%rax)
.Ltmp301:
.LBB27_11:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit11
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_13
# BB#12:
	callq	_ZdaPv
.LBB27_13:                              # %_ZN11CStringBaseIcED2Ev.exit.i8
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_15
# BB#14:
	callq	_ZdaPv
.LBB27_15:                              # %_ZN8NArchive3NGz5CItemD2Ev.exit9
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB27_16:
.Ltmp302:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end27:
	.size	_ZN8NArchive3NGz8CHandlerD2Ev, .Lfunc_end27-_ZN8NArchive3NGz8CHandlerD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp297-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp298-.Ltmp297       #   Call between .Ltmp297 and .Ltmp298
	.long	.Ltmp299-.Lfunc_begin6  #     jumps to .Ltmp299
	.byte	0                       #   On action: cleanup
	.long	.Ltmp303-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp304-.Ltmp303       #   Call between .Ltmp303 and .Ltmp304
	.long	.Ltmp305-.Lfunc_begin6  #     jumps to .Ltmp305
	.byte	0                       #   On action: cleanup
	.long	.Ltmp300-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp301-.Ltmp300       #   Call between .Ltmp300 and .Ltmp301
	.long	.Ltmp302-.Lfunc_begin6  #     jumps to .Ltmp302
	.byte	1                       #   On action: 1
	.long	.Ltmp301-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Lfunc_end27-.Ltmp301   #   Call between .Ltmp301 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3NGz8CHandlerD0Ev,"axG",@progbits,_ZN8NArchive3NGz8CHandlerD0Ev,comdat
	.weak	_ZN8NArchive3NGz8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandlerD0Ev,@function
_ZN8NArchive3NGz8CHandlerD0Ev:          # @_ZN8NArchive3NGz8CHandlerD0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 32
.Lcfi90:
	.cfi_offset %rbx, -24
.Lcfi91:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp306:
	callq	_ZN8NArchive3NGz8CHandlerD2Ev
.Ltmp307:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB28_2:
.Ltmp308:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end28:
	.size	_ZN8NArchive3NGz8CHandlerD0Ev, .Lfunc_end28-_ZN8NArchive3NGz8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp306-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp307-.Ltmp306       #   Call between .Ltmp306 and .Ltmp307
	.long	.Ltmp308-.Lfunc_begin7  #     jumps to .Ltmp308
	.byte	0                       #   On action: cleanup
	.long	.Ltmp307-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Lfunc_end28-.Ltmp307   #   Call between .Ltmp307 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end29:
	.size	_ZThn8_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end29-_ZThn8_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive3NGz8CHandler6AddRefEv,"axG",@progbits,_ZThn8_N8NArchive3NGz8CHandler6AddRefEv,comdat
	.weak	_ZThn8_N8NArchive3NGz8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3NGz8CHandler6AddRefEv,@function
_ZThn8_N8NArchive3NGz8CHandler6AddRefEv: # @_ZThn8_N8NArchive3NGz8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end30:
	.size	_ZThn8_N8NArchive3NGz8CHandler6AddRefEv, .Lfunc_end30-_ZThn8_N8NArchive3NGz8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive3NGz8CHandler7ReleaseEv,"axG",@progbits,_ZThn8_N8NArchive3NGz8CHandler7ReleaseEv,comdat
	.weak	_ZThn8_N8NArchive3NGz8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3NGz8CHandler7ReleaseEv,@function
_ZThn8_N8NArchive3NGz8CHandler7ReleaseEv: # @_ZThn8_N8NArchive3NGz8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi92:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB31_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB31_2:                               # %_ZN8NArchive3NGz8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end31:
	.size	_ZThn8_N8NArchive3NGz8CHandler7ReleaseEv, .Lfunc_end31-_ZThn8_N8NArchive3NGz8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive3NGz8CHandlerD1Ev,"axG",@progbits,_ZThn8_N8NArchive3NGz8CHandlerD1Ev,comdat
	.weak	_ZThn8_N8NArchive3NGz8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3NGz8CHandlerD1Ev,@function
_ZThn8_N8NArchive3NGz8CHandlerD1Ev:     # @_ZThn8_N8NArchive3NGz8CHandlerD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN8NArchive3NGz8CHandlerD2Ev # TAILCALL
.Lfunc_end32:
	.size	_ZThn8_N8NArchive3NGz8CHandlerD1Ev, .Lfunc_end32-_ZThn8_N8NArchive3NGz8CHandlerD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive3NGz8CHandlerD0Ev,"axG",@progbits,_ZThn8_N8NArchive3NGz8CHandlerD0Ev,comdat
	.weak	_ZThn8_N8NArchive3NGz8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3NGz8CHandlerD0Ev,@function
_ZThn8_N8NArchive3NGz8CHandlerD0Ev:     # @_ZThn8_N8NArchive3NGz8CHandlerD0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi95:
	.cfi_def_cfa_offset 32
.Lcfi96:
	.cfi_offset %rbx, -24
.Lcfi97:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp309:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3NGz8CHandlerD2Ev
.Ltmp310:
# BB#1:                                 # %_ZN8NArchive3NGz8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB33_2:
.Ltmp311:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end33:
	.size	_ZThn8_N8NArchive3NGz8CHandlerD0Ev, .Lfunc_end33-_ZThn8_N8NArchive3NGz8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp309-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp310-.Ltmp309       #   Call between .Ltmp309 and .Ltmp310
	.long	.Ltmp311-.Lfunc_begin8  #     jumps to .Ltmp311
	.byte	0                       #   On action: cleanup
	.long	.Ltmp310-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Lfunc_end33-.Ltmp310   #   Call between .Ltmp310 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn16_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end34:
	.size	_ZThn16_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end34-_ZThn16_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive3NGz8CHandler6AddRefEv,"axG",@progbits,_ZThn16_N8NArchive3NGz8CHandler6AddRefEv,comdat
	.weak	_ZThn16_N8NArchive3NGz8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive3NGz8CHandler6AddRefEv,@function
_ZThn16_N8NArchive3NGz8CHandler6AddRefEv: # @_ZThn16_N8NArchive3NGz8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end35:
	.size	_ZThn16_N8NArchive3NGz8CHandler6AddRefEv, .Lfunc_end35-_ZThn16_N8NArchive3NGz8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive3NGz8CHandler7ReleaseEv,"axG",@progbits,_ZThn16_N8NArchive3NGz8CHandler7ReleaseEv,comdat
	.weak	_ZThn16_N8NArchive3NGz8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive3NGz8CHandler7ReleaseEv,@function
_ZThn16_N8NArchive3NGz8CHandler7ReleaseEv: # @_ZThn16_N8NArchive3NGz8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi98:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB36_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB36_2:                               # %_ZN8NArchive3NGz8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end36:
	.size	_ZThn16_N8NArchive3NGz8CHandler7ReleaseEv, .Lfunc_end36-_ZThn16_N8NArchive3NGz8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive3NGz8CHandlerD1Ev,"axG",@progbits,_ZThn16_N8NArchive3NGz8CHandlerD1Ev,comdat
	.weak	_ZThn16_N8NArchive3NGz8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive3NGz8CHandlerD1Ev,@function
_ZThn16_N8NArchive3NGz8CHandlerD1Ev:    # @_ZThn16_N8NArchive3NGz8CHandlerD1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN8NArchive3NGz8CHandlerD2Ev # TAILCALL
.Lfunc_end37:
	.size	_ZThn16_N8NArchive3NGz8CHandlerD1Ev, .Lfunc_end37-_ZThn16_N8NArchive3NGz8CHandlerD1Ev
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive3NGz8CHandlerD0Ev,"axG",@progbits,_ZThn16_N8NArchive3NGz8CHandlerD0Ev,comdat
	.weak	_ZThn16_N8NArchive3NGz8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive3NGz8CHandlerD0Ev,@function
_ZThn16_N8NArchive3NGz8CHandlerD0Ev:    # @_ZThn16_N8NArchive3NGz8CHandlerD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 32
.Lcfi102:
	.cfi_offset %rbx, -24
.Lcfi103:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp312:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3NGz8CHandlerD2Ev
.Ltmp313:
# BB#1:                                 # %_ZN8NArchive3NGz8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB38_2:
.Ltmp314:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end38:
	.size	_ZThn16_N8NArchive3NGz8CHandlerD0Ev, .Lfunc_end38-_ZThn16_N8NArchive3NGz8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table38:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp312-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp313-.Ltmp312       #   Call between .Ltmp312 and .Ltmp313
	.long	.Ltmp314-.Lfunc_begin9  #     jumps to .Ltmp314
	.byte	0                       #   On action: cleanup
	.long	.Ltmp313-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Lfunc_end38-.Ltmp313   #   Call between .Ltmp313 and .Lfunc_end38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn24_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn24_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn24_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn24_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn24_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end39:
	.size	_ZThn24_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end39-_ZThn24_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn24_N8NArchive3NGz8CHandler6AddRefEv,"axG",@progbits,_ZThn24_N8NArchive3NGz8CHandler6AddRefEv,comdat
	.weak	_ZThn24_N8NArchive3NGz8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive3NGz8CHandler6AddRefEv,@function
_ZThn24_N8NArchive3NGz8CHandler6AddRefEv: # @_ZThn24_N8NArchive3NGz8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end40:
	.size	_ZThn24_N8NArchive3NGz8CHandler6AddRefEv, .Lfunc_end40-_ZThn24_N8NArchive3NGz8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn24_N8NArchive3NGz8CHandler7ReleaseEv,"axG",@progbits,_ZThn24_N8NArchive3NGz8CHandler7ReleaseEv,comdat
	.weak	_ZThn24_N8NArchive3NGz8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive3NGz8CHandler7ReleaseEv,@function
_ZThn24_N8NArchive3NGz8CHandler7ReleaseEv: # @_ZThn24_N8NArchive3NGz8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi104:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB41_2
# BB#1:
	addq	$-24, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB41_2:                               # %_ZN8NArchive3NGz8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end41:
	.size	_ZThn24_N8NArchive3NGz8CHandler7ReleaseEv, .Lfunc_end41-_ZThn24_N8NArchive3NGz8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn24_N8NArchive3NGz8CHandlerD1Ev,"axG",@progbits,_ZThn24_N8NArchive3NGz8CHandlerD1Ev,comdat
	.weak	_ZThn24_N8NArchive3NGz8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive3NGz8CHandlerD1Ev,@function
_ZThn24_N8NArchive3NGz8CHandlerD1Ev:    # @_ZThn24_N8NArchive3NGz8CHandlerD1Ev
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN8NArchive3NGz8CHandlerD2Ev # TAILCALL
.Lfunc_end42:
	.size	_ZThn24_N8NArchive3NGz8CHandlerD1Ev, .Lfunc_end42-_ZThn24_N8NArchive3NGz8CHandlerD1Ev
	.cfi_endproc

	.section	.text._ZThn24_N8NArchive3NGz8CHandlerD0Ev,"axG",@progbits,_ZThn24_N8NArchive3NGz8CHandlerD0Ev,comdat
	.weak	_ZThn24_N8NArchive3NGz8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive3NGz8CHandlerD0Ev,@function
_ZThn24_N8NArchive3NGz8CHandlerD0Ev:    # @_ZThn24_N8NArchive3NGz8CHandlerD0Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi107:
	.cfi_def_cfa_offset 32
.Lcfi108:
	.cfi_offset %rbx, -24
.Lcfi109:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-24, %rbx
.Ltmp315:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3NGz8CHandlerD2Ev
.Ltmp316:
# BB#1:                                 # %_ZN8NArchive3NGz8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB43_2:
.Ltmp317:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end43:
	.size	_ZThn24_N8NArchive3NGz8CHandlerD0Ev, .Lfunc_end43-_ZThn24_N8NArchive3NGz8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table43:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp315-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp316-.Ltmp315       #   Call between .Ltmp315 and .Ltmp316
	.long	.Ltmp317-.Lfunc_begin10 #     jumps to .Ltmp317
	.byte	0                       #   On action: cleanup
	.long	.Ltmp316-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Lfunc_end43-.Ltmp316   #   Call between .Ltmp316 and .Lfunc_end43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIcEpLEc,"axG",@progbits,_ZN11CStringBaseIcEpLEc,comdat
	.weak	_ZN11CStringBaseIcEpLEc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEc,@function
_ZN11CStringBaseIcEpLEc:                # @_ZN11CStringBaseIcEpLEc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi114:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi116:
	.cfi_def_cfa_offset 64
.Lcfi117:
	.cfi_offset %rbx, -56
.Lcfi118:
	.cfi_offset %r12, -48
.Lcfi119:
	.cfi_offset %r13, -40
.Lcfi120:
	.cfi_offset %r14, -32
.Lcfi121:
	.cfi_offset %r15, -24
.Lcfi122:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB44_28
# BB#1:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB44_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB44_3:                               # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rsi,%rbx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB44_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB44_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB44_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB44_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB44_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB44_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB44_17
.LBB44_7:
	xorl	%ecx, %ecx
.LBB44_8:                               # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB44_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB44_10:                              # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB44_10
.LBB44_11:                              # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB44_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB44_13:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB44_13
	jmp	.LBB44_26
.LBB44_25:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB44_27
.LBB44_26:                              # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB44_27:
	movq	%r12, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%r13)
.LBB44_28:                              # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	movb	%r14b, (%rax,%rcx)
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movb	$0, 1(%rax,%rcx)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB44_17:                              # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB44_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB44_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r12,%rbx)
	movups	%xmm1, 16(%r12,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB44_20
	jmp	.LBB44_21
.LBB44_18:
	xorl	%ebx, %ebx
.LBB44_21:                              # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB44_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB44_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB44_23
.LBB44_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB44_8
	jmp	.LBB44_26
.Lfunc_end44:
	.size	_ZN11CStringBaseIcEpLEc, .Lfunc_end44-_ZN11CStringBaseIcEpLEc
	.cfi_endproc

	.section	.text._ZN14ICompressCoderD0Ev,"axG",@progbits,_ZN14ICompressCoderD0Ev,comdat
	.weak	_ZN14ICompressCoderD0Ev
	.p2align	4, 0x90
	.type	_ZN14ICompressCoderD0Ev,@function
_ZN14ICompressCoderD0Ev:                # @_ZN14ICompressCoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end45:
	.size	_ZN14ICompressCoderD0Ev, .Lfunc_end45-_ZN14ICompressCoderD0Ev
	.cfi_endproc

	.section	.text._ZN8IUnknownD2Ev,"axG",@progbits,_ZN8IUnknownD2Ev,comdat
	.weak	_ZN8IUnknownD2Ev
	.p2align	4, 0x90
	.type	_ZN8IUnknownD2Ev,@function
_ZN8IUnknownD2Ev:                       # @_ZN8IUnknownD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end46:
	.size	_ZN8IUnknownD2Ev, .Lfunc_end46-_ZN8IUnknownD2Ev
	.cfi_endproc

	.section	.text._ZN27ICompressSetCoderPropertiesD0Ev,"axG",@progbits,_ZN27ICompressSetCoderPropertiesD0Ev,comdat
	.weak	_ZN27ICompressSetCoderPropertiesD0Ev
	.p2align	4, 0x90
	.type	_ZN27ICompressSetCoderPropertiesD0Ev,@function
_ZN27ICompressSetCoderPropertiesD0Ev:   # @_ZN27ICompressSetCoderPropertiesD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end47:
	.size	_ZN27ICompressSetCoderPropertiesD0Ev, .Lfunc_end47-_ZN27ICompressSetCoderPropertiesD0Ev
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGzL9CreateArcEv,@function
_ZN8NArchive3NGzL9CreateArcEv:          # @_ZN8NArchive3NGzL9CreateArcEv
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi125:
	.cfi_def_cfa_offset 32
.Lcfi126:
	.cfi_offset %rbx, -24
.Lcfi127:
	.cfi_offset %r14, -16
	movl	$168, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp318:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3NGz8CHandlerC2Ev
.Ltmp319:
# BB#1:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB48_2:
.Ltmp320:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end48:
	.size	_ZN8NArchive3NGzL9CreateArcEv, .Lfunc_end48-_ZN8NArchive3NGzL9CreateArcEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table48:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin11-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp318-.Lfunc_begin11 #   Call between .Lfunc_begin11 and .Ltmp318
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp318-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp319-.Ltmp318       #   Call between .Ltmp318 and .Ltmp319
	.long	.Ltmp320-.Lfunc_begin11 #     jumps to .Ltmp320
	.byte	0                       #   On action: cleanup
	.long	.Ltmp319-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Lfunc_end48-.Ltmp319   #   Call between .Ltmp319 and .Lfunc_end48
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGzL12CreateArcOutEv,@function
_ZN8NArchive3NGzL12CreateArcOutEv:      # @_ZN8NArchive3NGzL12CreateArcOutEv
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi130:
	.cfi_def_cfa_offset 32
.Lcfi131:
	.cfi_offset %rbx, -24
.Lcfi132:
	.cfi_offset %r14, -16
	movl	$168, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp321:
	movq	%rbx, %rdi
	callq	_ZN8NArchive3NGz8CHandlerC2Ev
.Ltmp322:
# BB#1:
	addq	$16, %rbx
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB49_2:
.Ltmp323:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end49:
	.size	_ZN8NArchive3NGzL12CreateArcOutEv, .Lfunc_end49-_ZN8NArchive3NGzL12CreateArcOutEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table49:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin12-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp321-.Lfunc_begin12 #   Call between .Lfunc_begin12 and .Ltmp321
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp321-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp322-.Ltmp321       #   Call between .Ltmp321 and .Ltmp322
	.long	.Ltmp323-.Lfunc_begin12 #     jumps to .Ltmp323
	.byte	0                       #   On action: cleanup
	.long	.Ltmp322-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Lfunc_end49-.Ltmp322   #   Call between .Ltmp322 and .Lfunc_end49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI50_0:
	.zero	16
	.section	.text._ZN8NArchive3NGz8CHandlerC2Ev,"axG",@progbits,_ZN8NArchive3NGz8CHandlerC2Ev,comdat
	.weak	_ZN8NArchive3NGz8CHandlerC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3NGz8CHandlerC2Ev,@function
_ZN8NArchive3NGz8CHandlerC2Ev:          # @_ZN8NArchive3NGz8CHandlerC2Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r15
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 32
.Lcfi136:
	.cfi_offset %rbx, -32
.Lcfi137:
	.cfi_offset %r14, -24
.Lcfi138:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	$0, 32(%r15)
	movl	$_ZTVN8NArchive3NGz8CHandlerE+184, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3NGz8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r15)
	movl	$_ZTVN8NArchive3NGz8CHandlerE+320, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3NGz8CHandlerE+248, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%r15)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 56(%r15)
.Ltmp324:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp325:
# BB#1:                                 # %.noexc6
	movq	%rbx, 56(%r15)
	movb	$0, (%rbx)
	movl	$4, 68(%r15)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 72(%r15)
.Ltmp327:
	movl	$4, %edi
	callq	_Znam
.Ltmp328:
# BB#2:
	movq	%rax, 72(%r15)
	movb	$0, (%rax)
	movl	$4, 84(%r15)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 120(%r15)
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 144(%r15)
	movl	$-1, 160(%r15)
	movb	$0, 164(%r15)
.Ltmp330:
	movl	$3480, %edi             # imm = 0xD98
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp331:
# BB#3:
.Ltmp332:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderC2Ebb
.Ltmp333:
# BB#4:                                 # %.noexc7
	movl	$_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE+264, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movq	$_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE+328, 32(%rbx)
	movq	%rbx, 136(%r15)
	incl	40(%rbx)
	movq	128(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB50_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp335:
	callq	*16(%rax)
.Ltmp336:
.LBB50_6:
	movq	%rbx, 128(%r15)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB50_9:
.Ltmp334:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB50_10
.LBB50_20:                              # %_ZN11CStringBaseIcED2Ev.exit.i5
.Ltmp329:
	movq	%rax, %r14
	movq	%rbx, %rdi
	jmp	.LBB50_17
.LBB50_7:
.Ltmp326:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB50_8:
.Ltmp337:
	movq	%rax, %r14
.LBB50_10:
	movq	128(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB50_12
# BB#11:
	movq	(%rdi), %rax
.Ltmp338:
	callq	*16(%rax)
.Ltmp339:
.LBB50_12:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	leaq	120(%r15), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB50_14
# BB#13:
	movq	(%rdi), %rax
.Ltmp340:
	callq	*16(%rax)
.Ltmp341:
.LBB50_14:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB50_16
# BB#15:
	callq	_ZdaPv
.LBB50_16:                              # %_ZN11CStringBaseIcED2Ev.exit.i
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB50_18
.LBB50_17:
	callq	_ZdaPv
.LBB50_18:                              # %_ZN8NArchive3NGz5CItemD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB50_19:
.Ltmp342:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end50:
	.size	_ZN8NArchive3NGz8CHandlerC2Ev, .Lfunc_end50-_ZN8NArchive3NGz8CHandlerC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table50:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp324-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp325-.Ltmp324       #   Call between .Ltmp324 and .Ltmp325
	.long	.Ltmp326-.Lfunc_begin13 #     jumps to .Ltmp326
	.byte	0                       #   On action: cleanup
	.long	.Ltmp327-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp328-.Ltmp327       #   Call between .Ltmp327 and .Ltmp328
	.long	.Ltmp329-.Lfunc_begin13 #     jumps to .Ltmp329
	.byte	0                       #   On action: cleanup
	.long	.Ltmp330-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp331-.Ltmp330       #   Call between .Ltmp330 and .Ltmp331
	.long	.Ltmp337-.Lfunc_begin13 #     jumps to .Ltmp337
	.byte	0                       #   On action: cleanup
	.long	.Ltmp332-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Ltmp333-.Ltmp332       #   Call between .Ltmp332 and .Ltmp333
	.long	.Ltmp334-.Lfunc_begin13 #     jumps to .Ltmp334
	.byte	0                       #   On action: cleanup
	.long	.Ltmp335-.Lfunc_begin13 # >> Call Site 5 <<
	.long	.Ltmp336-.Ltmp335       #   Call between .Ltmp335 and .Ltmp336
	.long	.Ltmp337-.Lfunc_begin13 #     jumps to .Ltmp337
	.byte	0                       #   On action: cleanup
	.long	.Ltmp336-.Lfunc_begin13 # >> Call Site 6 <<
	.long	.Ltmp338-.Ltmp336       #   Call between .Ltmp336 and .Ltmp338
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp338-.Lfunc_begin13 # >> Call Site 7 <<
	.long	.Ltmp341-.Ltmp338       #   Call between .Ltmp338 and .Ltmp341
	.long	.Ltmp342-.Lfunc_begin13 #     jumps to .Ltmp342
	.byte	1                       #   On action: 1
	.long	.Ltmp341-.Lfunc_begin13 # >> Call Site 8 <<
	.long	.Lfunc_end50-.Ltmp341   #   Call between .Ltmp341 and .Lfunc_end50
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi139:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB51_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB51_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB51_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB51_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB51_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB51_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB51_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB51_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB51_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB51_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB51_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB51_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB51_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB51_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB51_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB51_16
.LBB51_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICompressGetInStreamProcessedSize(%rip), %cl
	jne	.LBB51_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+1(%rip), %al
	jne	.LBB51_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+2(%rip), %al
	jne	.LBB51_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+3(%rip), %al
	jne	.LBB51_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+4(%rip), %al
	jne	.LBB51_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+5(%rip), %al
	jne	.LBB51_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+6(%rip), %al
	jne	.LBB51_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+7(%rip), %al
	jne	.LBB51_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+8(%rip), %al
	jne	.LBB51_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+9(%rip), %al
	jne	.LBB51_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+10(%rip), %al
	jne	.LBB51_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+11(%rip), %al
	jne	.LBB51_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+12(%rip), %al
	jne	.LBB51_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+13(%rip), %al
	jne	.LBB51_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+14(%rip), %al
	jne	.LBB51_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit12
	movb	15(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+15(%rip), %al
	jne	.LBB51_33
.LBB51_16:
	leaq	8(%rdi), %rax
	jmp	.LBB51_84
.LBB51_33:                              # %_ZeqRK4GUIDS1_.exit12.thread
	cmpb	IID_ICompressSetInStream(%rip), %cl
	jne	.LBB51_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetInStream+1(%rip), %al
	jne	.LBB51_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetInStream+2(%rip), %al
	jne	.LBB51_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetInStream+3(%rip), %al
	jne	.LBB51_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetInStream+4(%rip), %al
	jne	.LBB51_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetInStream+5(%rip), %al
	jne	.LBB51_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetInStream+6(%rip), %al
	jne	.LBB51_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetInStream+7(%rip), %al
	jne	.LBB51_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetInStream+8(%rip), %al
	jne	.LBB51_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetInStream+9(%rip), %al
	jne	.LBB51_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetInStream+10(%rip), %al
	jne	.LBB51_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetInStream+11(%rip), %al
	jne	.LBB51_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetInStream+12(%rip), %al
	jne	.LBB51_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetInStream+13(%rip), %al
	jne	.LBB51_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetInStream+14(%rip), %al
	jne	.LBB51_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit16
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetInStream+15(%rip), %al
	jne	.LBB51_50
# BB#49:
	leaq	16(%rdi), %rax
	jmp	.LBB51_84
.LBB51_50:                              # %_ZeqRK4GUIDS1_.exit16.thread
	cmpb	IID_ICompressSetOutStreamSize(%rip), %cl
	jne	.LBB51_67
# BB#51:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+1(%rip), %al
	jne	.LBB51_67
# BB#52:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+2(%rip), %al
	jne	.LBB51_67
# BB#53:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+3(%rip), %al
	jne	.LBB51_67
# BB#54:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+4(%rip), %al
	jne	.LBB51_67
# BB#55:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+5(%rip), %al
	jne	.LBB51_67
# BB#56:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+6(%rip), %al
	jne	.LBB51_67
# BB#57:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+7(%rip), %al
	jne	.LBB51_67
# BB#58:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+8(%rip), %al
	jne	.LBB51_67
# BB#59:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+9(%rip), %al
	jne	.LBB51_67
# BB#60:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+10(%rip), %al
	jne	.LBB51_67
# BB#61:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+11(%rip), %al
	jne	.LBB51_67
# BB#62:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+12(%rip), %al
	jne	.LBB51_67
# BB#63:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+13(%rip), %al
	jne	.LBB51_67
# BB#64:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+14(%rip), %al
	jne	.LBB51_67
# BB#65:                                # %_ZeqRK4GUIDS1_.exit18
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+15(%rip), %al
	jne	.LBB51_67
# BB#66:
	leaq	24(%rdi), %rax
	jmp	.LBB51_84
.LBB51_67:                              # %_ZeqRK4GUIDS1_.exit18.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ISequentialInStream(%rip), %cl
	jne	.LBB51_85
# BB#68:
	movb	1(%rsi), %cl
	cmpb	IID_ISequentialInStream+1(%rip), %cl
	jne	.LBB51_85
# BB#69:
	movb	2(%rsi), %cl
	cmpb	IID_ISequentialInStream+2(%rip), %cl
	jne	.LBB51_85
# BB#70:
	movb	3(%rsi), %cl
	cmpb	IID_ISequentialInStream+3(%rip), %cl
	jne	.LBB51_85
# BB#71:
	movb	4(%rsi), %cl
	cmpb	IID_ISequentialInStream+4(%rip), %cl
	jne	.LBB51_85
# BB#72:
	movb	5(%rsi), %cl
	cmpb	IID_ISequentialInStream+5(%rip), %cl
	jne	.LBB51_85
# BB#73:
	movb	6(%rsi), %cl
	cmpb	IID_ISequentialInStream+6(%rip), %cl
	jne	.LBB51_85
# BB#74:
	movb	7(%rsi), %cl
	cmpb	IID_ISequentialInStream+7(%rip), %cl
	jne	.LBB51_85
# BB#75:
	movb	8(%rsi), %cl
	cmpb	IID_ISequentialInStream+8(%rip), %cl
	jne	.LBB51_85
# BB#76:
	movb	9(%rsi), %cl
	cmpb	IID_ISequentialInStream+9(%rip), %cl
	jne	.LBB51_85
# BB#77:
	movb	10(%rsi), %cl
	cmpb	IID_ISequentialInStream+10(%rip), %cl
	jne	.LBB51_85
# BB#78:
	movb	11(%rsi), %cl
	cmpb	IID_ISequentialInStream+11(%rip), %cl
	jne	.LBB51_85
# BB#79:
	movb	12(%rsi), %cl
	cmpb	IID_ISequentialInStream+12(%rip), %cl
	jne	.LBB51_85
# BB#80:
	movb	13(%rsi), %cl
	cmpb	IID_ISequentialInStream+13(%rip), %cl
	jne	.LBB51_85
# BB#81:
	movb	14(%rsi), %cl
	cmpb	IID_ISequentialInStream+14(%rip), %cl
	jne	.LBB51_85
# BB#82:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %cl
	cmpb	IID_ISequentialInStream+15(%rip), %cl
	jne	.LBB51_85
# BB#83:
	leaq	32(%rdi), %rax
.LBB51_84:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB51_85:                              # %_ZeqRK4GUIDS1_.exit14.thread
	popq	%rcx
	retq
.Lfunc_end51:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end51-_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	40(%rdi), %eax
	incl	%eax
	movl	%eax, 40(%rdi)
	retq
.Lfunc_end52:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end52-_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi140:
	.cfi_def_cfa_offset 16
	movl	40(%rdi), %eax
	decl	%eax
	movl	%eax, 40(%rdi)
	jne	.LBB53_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB53_2:
	popq	%rcx
	retq
.Lfunc_end53:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end53-_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev,@function
_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev: # @_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi143:
	.cfi_def_cfa_offset 32
.Lcfi144:
	.cfi_offset %rbx, -24
.Lcfi145:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+264, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movq	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+328, 32(%rbx)
	leaq	112(%rbx), %rdi
.Ltmp343:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp344:
# BB#1:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB54_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp349:
	callq	*16(%rax)
.Ltmp350:
.LBB54_3:                               # %_ZN5NBitl12CBaseDecoderI9CInBufferED2Ev.exit
	leaq	48(%rbx), %rdi
.Ltmp361:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp362:
# BB#4:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB54_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp367:
	callq	*16(%rax)
.Ltmp368:
.LBB54_6:                               # %_ZN10COutBufferD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB54_20:
.Ltmp369:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB54_13:
.Ltmp351:
	movq	%rax, %r14
	jmp	.LBB54_14
.LBB54_10:
.Ltmp363:
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB54_21
# BB#11:
	movq	(%rdi), %rax
.Ltmp364:
	callq	*16(%rax)
.Ltmp365:
	jmp	.LBB54_21
.LBB54_12:
.Ltmp366:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB54_7:
.Ltmp345:
	movq	%rax, %r14
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB54_14
# BB#8:
	movq	(%rdi), %rax
.Ltmp346:
	callq	*16(%rax)
.Ltmp347:
.LBB54_14:                              # %.body
	leaq	48(%rbx), %rdi
.Ltmp352:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp353:
# BB#15:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB54_21
# BB#16:
	movq	(%rdi), %rax
.Ltmp358:
	callq	*16(%rax)
.Ltmp359:
.LBB54_21:                              # %_ZN10COutBufferD2Ev.exit14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB54_9:
.Ltmp348:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB54_22:
.Ltmp360:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB54_17:
.Ltmp354:
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB54_23
# BB#18:
	movq	(%rdi), %rax
.Ltmp355:
	callq	*16(%rax)
.Ltmp356:
.LBB54_23:                              # %.body12
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB54_19:
.Ltmp357:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end54:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev, .Lfunc_end54-_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table54:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp343-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp344-.Ltmp343       #   Call between .Ltmp343 and .Ltmp344
	.long	.Ltmp345-.Lfunc_begin14 #     jumps to .Ltmp345
	.byte	0                       #   On action: cleanup
	.long	.Ltmp349-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp350-.Ltmp349       #   Call between .Ltmp349 and .Ltmp350
	.long	.Ltmp351-.Lfunc_begin14 #     jumps to .Ltmp351
	.byte	0                       #   On action: cleanup
	.long	.Ltmp361-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp362-.Ltmp361       #   Call between .Ltmp361 and .Ltmp362
	.long	.Ltmp363-.Lfunc_begin14 #     jumps to .Ltmp363
	.byte	0                       #   On action: cleanup
	.long	.Ltmp367-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Ltmp368-.Ltmp367       #   Call between .Ltmp367 and .Ltmp368
	.long	.Ltmp369-.Lfunc_begin14 #     jumps to .Ltmp369
	.byte	0                       #   On action: cleanup
	.long	.Ltmp368-.Lfunc_begin14 # >> Call Site 5 <<
	.long	.Ltmp364-.Ltmp368       #   Call between .Ltmp368 and .Ltmp364
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp364-.Lfunc_begin14 # >> Call Site 6 <<
	.long	.Ltmp365-.Ltmp364       #   Call between .Ltmp364 and .Ltmp365
	.long	.Ltmp366-.Lfunc_begin14 #     jumps to .Ltmp366
	.byte	1                       #   On action: 1
	.long	.Ltmp346-.Lfunc_begin14 # >> Call Site 7 <<
	.long	.Ltmp347-.Ltmp346       #   Call between .Ltmp346 and .Ltmp347
	.long	.Ltmp348-.Lfunc_begin14 #     jumps to .Ltmp348
	.byte	1                       #   On action: 1
	.long	.Ltmp352-.Lfunc_begin14 # >> Call Site 8 <<
	.long	.Ltmp353-.Ltmp352       #   Call between .Ltmp352 and .Ltmp353
	.long	.Ltmp354-.Lfunc_begin14 #     jumps to .Ltmp354
	.byte	1                       #   On action: 1
	.long	.Ltmp358-.Lfunc_begin14 # >> Call Site 9 <<
	.long	.Ltmp359-.Ltmp358       #   Call between .Ltmp358 and .Ltmp359
	.long	.Ltmp360-.Lfunc_begin14 #     jumps to .Ltmp360
	.byte	1                       #   On action: 1
	.long	.Ltmp359-.Lfunc_begin14 # >> Call Site 10 <<
	.long	.Ltmp355-.Ltmp359       #   Call between .Ltmp359 and .Ltmp355
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp355-.Lfunc_begin14 # >> Call Site 11 <<
	.long	.Ltmp356-.Ltmp355       #   Call between .Ltmp355 and .Ltmp356
	.long	.Ltmp357-.Lfunc_begin14 #     jumps to .Ltmp357
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,@function
_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev: # @_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi146:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi148:
	.cfi_def_cfa_offset 32
.Lcfi149:
	.cfi_offset %rbx, -24
.Lcfi150:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp370:
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp371:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB55_2:
.Ltmp372:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end55:
	.size	_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev, .Lfunc_end55-_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table55:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp370-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp371-.Ltmp370       #   Call between .Ltmp370 and .Ltmp371
	.long	.Ltmp372-.Lfunc_begin15 #     jumps to .Ltmp372
	.byte	0                       #   On action: cleanup
	.long	.Ltmp371-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Lfunc_end55-.Ltmp371   #   Call between .Ltmp371 and .Lfunc_end55
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end56:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end56-_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	32(%rdi), %eax
	incl	%eax
	movl	%eax, 32(%rdi)
	retq
.Lfunc_end57:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end57-_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi151:
	.cfi_def_cfa_offset 16
	movl	32(%rdi), %eax
	decl	%eax
	movl	%eax, 32(%rdi)
	jne	.LBB58_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB58_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end58:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end58-_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,@function
_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev: # @_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end59:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev, .Lfunc_end59-_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,@function
_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev: # @_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%r14
.Lcfi152:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi154:
	.cfi_def_cfa_offset 32
.Lcfi155:
	.cfi_offset %rbx, -24
.Lcfi156:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp373:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp374:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB60_2:
.Ltmp375:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end60:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev, .Lfunc_end60-_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table60:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp373-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp374-.Ltmp373       #   Call between .Ltmp373 and .Ltmp374
	.long	.Ltmp375-.Lfunc_begin16 #     jumps to .Ltmp375
	.byte	0                       #   On action: cleanup
	.long	.Ltmp374-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Lfunc_end60-.Ltmp374   #   Call between .Ltmp374 and .Lfunc_end60
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end61:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end61-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end62:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end62-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi157:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB63_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB63_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end63:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end63-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,@function
_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev: # @_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end64:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev, .Lfunc_end64-_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,@function
_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev: # @_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi158:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi159:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi160:
	.cfi_def_cfa_offset 32
.Lcfi161:
	.cfi_offset %rbx, -24
.Lcfi162:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp376:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp377:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB65_2:
.Ltmp378:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end65:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev, .Lfunc_end65-_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table65:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp376-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp377-.Ltmp376       #   Call between .Ltmp376 and .Ltmp377
	.long	.Ltmp378-.Lfunc_begin17 #     jumps to .Ltmp378
	.byte	0                       #   On action: cleanup
	.long	.Ltmp377-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Lfunc_end65-.Ltmp377   #   Call between .Ltmp377 and .Lfunc_end65
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end66:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end66-_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end67:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end67-_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi163:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB68_2
# BB#1:
	addq	$-24, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB68_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end68:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end68-_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,@function
_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev: # @_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end69:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev, .Lfunc_end69-_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,@function
_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev: # @_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi164:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi166:
	.cfi_def_cfa_offset 32
.Lcfi167:
	.cfi_offset %rbx, -24
.Lcfi168:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-24, %rbx
.Ltmp379:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp380:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB70_2:
.Ltmp381:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end70:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev, .Lfunc_end70-_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table70:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp379-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp380-.Ltmp379       #   Call between .Ltmp379 and .Ltmp380
	.long	.Ltmp381-.Lfunc_begin18 #     jumps to .Ltmp381
	.byte	0                       #   On action: cleanup
	.long	.Ltmp380-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Lfunc_end70-.Ltmp380   #   Call between .Ltmp380 and .Lfunc_end70
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end71:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end71-_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end72:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end72-_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi169:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB73_2
# BB#1:
	addq	$-32, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB73_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end73:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end73-_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,@function
_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev: # @_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end74:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev, .Lfunc_end74-_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,@function
_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev: # @_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:
	pushq	%r14
.Lcfi170:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi171:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi172:
	.cfi_def_cfa_offset 32
.Lcfi173:
	.cfi_offset %rbx, -24
.Lcfi174:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-32, %rbx
.Ltmp382:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp383:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB75_2:
.Ltmp384:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end75:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev, .Lfunc_end75-_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table75:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp382-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp383-.Ltmp382       #   Call between .Ltmp382 and .Ltmp383
	.long	.Ltmp384-.Lfunc_begin19 #     jumps to .Ltmp384
	.byte	0                       #   On action: cleanup
	.long	.Ltmp383-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Lfunc_end75-.Ltmp383   #   Call between .Ltmp383 and .Lfunc_end75
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%rbp
.Lcfi175:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi176:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi177:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi178:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi179:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi180:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi181:
	.cfi_def_cfa_offset 64
.Lcfi182:
	.cfi_offset %rbx, -56
.Lcfi183:
	.cfi_offset %r12, -48
.Lcfi184:
	.cfi_offset %r13, -40
.Lcfi185:
	.cfi_offset %r14, -32
.Lcfi186:
	.cfi_offset %r15, -24
.Lcfi187:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB76_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB76_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB76_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB76_5
.LBB76_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB76_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp385:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp386:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB76_35
# BB#12:
	movq	%rbx, %r13
.LBB76_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB76_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB76_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB76_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB76_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB76_15
.LBB76_14:
	xorl	%esi, %esi
.LBB76_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB76_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB76_16
.LBB76_29:
	movq	%r13, %rbx
.LBB76_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp387:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp388:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB76_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB76_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB76_8
.LBB76_3:
	xorl	%eax, %eax
.LBB76_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB76_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB76_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB76_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB76_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB76_30
.LBB76_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB76_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB76_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB76_24
	jmp	.LBB76_25
.LBB76_22:
	xorl	%ecx, %ecx
.LBB76_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB76_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB76_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB76_27
.LBB76_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB76_15
	jmp	.LBB76_29
.LBB76_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp389:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end76:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end76-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table76:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin20-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp385-.Lfunc_begin20 #   Call between .Lfunc_begin20 and .Ltmp385
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp385-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Ltmp388-.Ltmp385       #   Call between .Ltmp385 and .Ltmp388
	.long	.Ltmp389-.Lfunc_begin20 #     jumps to .Ltmp389
	.byte	0                       #   On action: cleanup
	.long	.Ltmp388-.Lfunc_begin20 # >> Call Site 3 <<
	.long	.Lfunc_end76-.Ltmp388   #   Call between .Ltmp388 and .Lfunc_end76
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_GzHandler.ii,@function
_GLOBAL__sub_I_GzHandler.ii:            # @_GLOBAL__sub_I_GzHandler.ii
	.cfi_startproc
# BB#0:
	movl	$_ZN8NArchive3NGzL9g_ArcInfoE, %edi
	jmp	_Z11RegisterArcPK8CArcInfo # TAILCALL
.Lfunc_end77:
	.size	_GLOBAL__sub_I_GzHandler.ii, .Lfunc_end77-_GLOBAL__sub_I_GzHandler.ii
	.cfi_endproc

	.type	_ZN8NArchive3NGz6kPropsE,@object # @_ZN8NArchive3NGz6kPropsE
	.data
	.globl	_ZN8NArchive3NGz6kPropsE
	.p2align	4
_ZN8NArchive3NGz6kPropsE:
	.quad	0
	.long	3                       # 0x3
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	7                       # 0x7
	.short	21                      # 0x15
	.zero	2
	.quad	0
	.long	8                       # 0x8
	.short	21                      # 0x15
	.zero	2
	.quad	0
	.long	12                      # 0xc
	.short	64                      # 0x40
	.zero	2
	.quad	0
	.long	23                      # 0x17
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	19                      # 0x13
	.short	19                      # 0x13
	.zero	2
	.size	_ZN8NArchive3NGz6kPropsE, 96

	.type	_ZN8NArchive3NGzL9kHostOSesE,@object # @_ZN8NArchive3NGzL9kHostOSesE
	.section	.rodata,"a",@progbits
	.p2align	4
_ZN8NArchive3NGzL9kHostOSesE:
	.quad	.L.str
	.quad	.L.str.1
	.quad	.L.str.2
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	.L.str.6
	.quad	.L.str.7
	.quad	.L.str.8
	.quad	.L.str.9
	.quad	.L.str.10
	.quad	.L.str.11
	.quad	.L.str.12
	.quad	.L.str.13
	.quad	.L.str.14
	.quad	.L.str.15
	.quad	.L.str.16
	.quad	.L.str.17
	.quad	.L.str.18
	.quad	.L.str.19
	.size	_ZN8NArchive3NGzL9kHostOSesE, 160

	.type	_ZN8NArchive3NGzL10kUnknownOSE,@object # @_ZN8NArchive3NGzL10kUnknownOSE
	.p2align	3
_ZN8NArchive3NGzL10kUnknownOSE:
	.quad	.L.str.20
	.size	_ZN8NArchive3NGzL10kUnknownOSE, 8

	.type	_ZTVN8NArchive3NGz8CHandlerE,@object # @_ZTVN8NArchive3NGz8CHandlerE
	.globl	_ZTVN8NArchive3NGz8CHandlerE
	.p2align	3
_ZTVN8NArchive3NGz8CHandlerE:
	.quad	0
	.quad	_ZTIN8NArchive3NGz8CHandlerE
	.quad	_ZN8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive3NGz8CHandler6AddRefEv
	.quad	_ZN8NArchive3NGz8CHandler7ReleaseEv
	.quad	_ZN8NArchive3NGz8CHandlerD2Ev
	.quad	_ZN8NArchive3NGz8CHandlerD0Ev
	.quad	_ZN8NArchive3NGz8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.quad	_ZN8NArchive3NGz8CHandler5CloseEv
	.quad	_ZN8NArchive3NGz8CHandler16GetNumberOfItemsEPj
	.quad	_ZN8NArchive3NGz8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.quad	_ZN8NArchive3NGz8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.quad	_ZN8NArchive3NGz8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.quad	_ZN8NArchive3NGz8CHandler21GetNumberOfPropertiesEPj
	.quad	_ZN8NArchive3NGz8CHandler15GetPropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive3NGz8CHandler28GetNumberOfArchivePropertiesEPj
	.quad	_ZN8NArchive3NGz8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive3NGz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.quad	_ZN8NArchive3NGz8CHandler15GetFileTimeTypeEPj
	.quad	_ZN8NArchive3NGz8CHandler7OpenSeqEP19ISequentialInStream
	.quad	_ZN8NArchive3NGz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.quad	-8
	.quad	_ZTIN8NArchive3NGz8CHandlerE
	.quad	_ZThn8_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N8NArchive3NGz8CHandler6AddRefEv
	.quad	_ZThn8_N8NArchive3NGz8CHandler7ReleaseEv
	.quad	_ZThn8_N8NArchive3NGz8CHandlerD1Ev
	.quad	_ZThn8_N8NArchive3NGz8CHandlerD0Ev
	.quad	_ZThn8_N8NArchive3NGz8CHandler7OpenSeqEP19ISequentialInStream
	.quad	-16
	.quad	_ZTIN8NArchive3NGz8CHandlerE
	.quad	_ZThn16_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N8NArchive3NGz8CHandler6AddRefEv
	.quad	_ZThn16_N8NArchive3NGz8CHandler7ReleaseEv
	.quad	_ZThn16_N8NArchive3NGz8CHandlerD1Ev
	.quad	_ZThn16_N8NArchive3NGz8CHandlerD0Ev
	.quad	_ZThn16_N8NArchive3NGz8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.quad	_ZThn16_N8NArchive3NGz8CHandler15GetFileTimeTypeEPj
	.quad	-24
	.quad	_ZTIN8NArchive3NGz8CHandlerE
	.quad	_ZThn24_N8NArchive3NGz8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn24_N8NArchive3NGz8CHandler6AddRefEv
	.quad	_ZThn24_N8NArchive3NGz8CHandler7ReleaseEv
	.quad	_ZThn24_N8NArchive3NGz8CHandlerD1Ev
	.quad	_ZThn24_N8NArchive3NGz8CHandlerD0Ev
	.quad	_ZThn24_N8NArchive3NGz8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.size	_ZTVN8NArchive3NGz8CHandlerE, 368

	.type	_ZTSN8NArchive3NGz8CHandlerE,@object # @_ZTSN8NArchive3NGz8CHandlerE
	.globl	_ZTSN8NArchive3NGz8CHandlerE
	.p2align	4
_ZTSN8NArchive3NGz8CHandlerE:
	.asciz	"N8NArchive3NGz8CHandlerE"
	.size	_ZTSN8NArchive3NGz8CHandlerE, 25

	.type	_ZTS10IInArchive,@object # @_ZTS10IInArchive
	.section	.rodata._ZTS10IInArchive,"aG",@progbits,_ZTS10IInArchive,comdat
	.weak	_ZTS10IInArchive
_ZTS10IInArchive:
	.asciz	"10IInArchive"
	.size	_ZTS10IInArchive, 13

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI10IInArchive,@object # @_ZTI10IInArchive
	.section	.rodata._ZTI10IInArchive,"aG",@progbits,_ZTI10IInArchive,comdat
	.weak	_ZTI10IInArchive
	.p2align	4
_ZTI10IInArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IInArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI10IInArchive, 24

	.type	_ZTS15IArchiveOpenSeq,@object # @_ZTS15IArchiveOpenSeq
	.section	.rodata._ZTS15IArchiveOpenSeq,"aG",@progbits,_ZTS15IArchiveOpenSeq,comdat
	.weak	_ZTS15IArchiveOpenSeq
	.p2align	4
_ZTS15IArchiveOpenSeq:
	.asciz	"15IArchiveOpenSeq"
	.size	_ZTS15IArchiveOpenSeq, 18

	.type	_ZTI15IArchiveOpenSeq,@object # @_ZTI15IArchiveOpenSeq
	.section	.rodata._ZTI15IArchiveOpenSeq,"aG",@progbits,_ZTI15IArchiveOpenSeq,comdat
	.weak	_ZTI15IArchiveOpenSeq
	.p2align	4
_ZTI15IArchiveOpenSeq:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15IArchiveOpenSeq
	.quad	_ZTI8IUnknown
	.size	_ZTI15IArchiveOpenSeq, 24

	.type	_ZTS11IOutArchive,@object # @_ZTS11IOutArchive
	.section	.rodata._ZTS11IOutArchive,"aG",@progbits,_ZTS11IOutArchive,comdat
	.weak	_ZTS11IOutArchive
_ZTS11IOutArchive:
	.asciz	"11IOutArchive"
	.size	_ZTS11IOutArchive, 14

	.type	_ZTI11IOutArchive,@object # @_ZTI11IOutArchive
	.section	.rodata._ZTI11IOutArchive,"aG",@progbits,_ZTI11IOutArchive,comdat
	.weak	_ZTI11IOutArchive
	.p2align	4
_ZTI11IOutArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS11IOutArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI11IOutArchive, 24

	.type	_ZTS14ISetProperties,@object # @_ZTS14ISetProperties
	.section	.rodata._ZTS14ISetProperties,"aG",@progbits,_ZTS14ISetProperties,comdat
	.weak	_ZTS14ISetProperties
	.p2align	4
_ZTS14ISetProperties:
	.asciz	"14ISetProperties"
	.size	_ZTS14ISetProperties, 17

	.type	_ZTI14ISetProperties,@object # @_ZTI14ISetProperties
	.section	.rodata._ZTI14ISetProperties,"aG",@progbits,_ZTI14ISetProperties,comdat
	.weak	_ZTI14ISetProperties
	.p2align	4
_ZTI14ISetProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ISetProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI14ISetProperties, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive3NGz8CHandlerE,@object # @_ZTIN8NArchive3NGz8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive3NGz8CHandlerE
	.p2align	4
_ZTIN8NArchive3NGz8CHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive3NGz8CHandlerE
	.long	1                       # 0x1
	.long	5                       # 0x5
	.quad	_ZTI10IInArchive
	.quad	2                       # 0x2
	.quad	_ZTI15IArchiveOpenSeq
	.quad	2050                    # 0x802
	.quad	_ZTI11IOutArchive
	.quad	4098                    # 0x1002
	.quad	_ZTI14ISetProperties
	.quad	6146                    # 0x1802
	.quad	_ZTI13CMyUnknownImp
	.quad	8194                    # 0x2002
	.size	_ZTIN8NArchive3NGz8CHandlerE, 104

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"FAT"
	.size	.L.str, 4

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"AMIGA"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"VMS"
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Unix"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"VM/CMS"
	.size	.L.str.4, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Atari"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"HPFS"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Macintosh"
	.size	.L.str.7, 10

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Z-System"
	.size	.L.str.8, 9

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"CP/M"
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"TOPS-20"
	.size	.L.str.10, 8

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"NTFS"
	.size	.L.str.11, 5

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"SMS/QDOS"
	.size	.L.str.12, 9

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Acorn"
	.size	.L.str.13, 6

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"VFAT"
	.size	.L.str.14, 5

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"MVS"
	.size	.L.str.15, 4

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"BeOS"
	.size	.L.str.16, 5

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Tandem"
	.size	.L.str.17, 7

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"OS/400"
	.size	.L.str.18, 7

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"OS/X"
	.size	.L.str.19, 5

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Unknown"
	.size	.L.str.20, 8

	.type	_ZTV14ICompressCoder,@object # @_ZTV14ICompressCoder
	.section	.rodata._ZTV14ICompressCoder,"aG",@progbits,_ZTV14ICompressCoder,comdat
	.weak	_ZTV14ICompressCoder
	.p2align	3
_ZTV14ICompressCoder:
	.quad	0
	.quad	_ZTI14ICompressCoder
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN14ICompressCoderD0Ev
	.quad	__cxa_pure_virtual
	.size	_ZTV14ICompressCoder, 64

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTV27ICompressSetCoderProperties,@object # @_ZTV27ICompressSetCoderProperties
	.section	.rodata._ZTV27ICompressSetCoderProperties,"aG",@progbits,_ZTV27ICompressSetCoderProperties,comdat
	.weak	_ZTV27ICompressSetCoderProperties
	.p2align	3
_ZTV27ICompressSetCoderProperties:
	.quad	0
	.quad	_ZTI27ICompressSetCoderProperties
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN27ICompressSetCoderPropertiesD0Ev
	.quad	__cxa_pure_virtual
	.size	_ZTV27ICompressSetCoderProperties, 64

	.type	_ZTS27ICompressSetCoderProperties,@object # @_ZTS27ICompressSetCoderProperties
	.section	.rodata._ZTS27ICompressSetCoderProperties,"aG",@progbits,_ZTS27ICompressSetCoderProperties,comdat
	.weak	_ZTS27ICompressSetCoderProperties
	.p2align	4
_ZTS27ICompressSetCoderProperties:
	.asciz	"27ICompressSetCoderProperties"
	.size	_ZTS27ICompressSetCoderProperties, 30

	.type	_ZTI27ICompressSetCoderProperties,@object # @_ZTI27ICompressSetCoderProperties
	.section	.rodata._ZTI27ICompressSetCoderProperties,"aG",@progbits,_ZTI27ICompressSetCoderProperties,comdat
	.weak	_ZTI27ICompressSetCoderProperties
	.p2align	4
_ZTI27ICompressSetCoderProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS27ICompressSetCoderProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI27ICompressSetCoderProperties, 24

	.type	_ZN8NArchive3NGzL9g_ArcInfoE,@object # @_ZN8NArchive3NGzL9g_ArcInfoE
	.data
	.p2align	3
_ZN8NArchive3NGzL9g_ArcInfoE:
	.quad	.L.str.21
	.quad	.L.str.22
	.quad	.L.str.23
	.byte	239                     # 0xef
	.asciz	"\037\213\b\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.zero	3
	.long	3                       # 0x3
	.byte	1                       # 0x1
	.zero	3
	.quad	_ZN8NArchive3NGzL9CreateArcEv
	.quad	_ZN8NArchive3NGzL12CreateArcOutEv
	.size	_ZN8NArchive3NGzL9g_ArcInfoE, 80

	.type	.L.str.21,@object       # @.str.21
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.21:
	.long	103                     # 0x67
	.long	122                     # 0x7a
	.long	105                     # 0x69
	.long	112                     # 0x70
	.long	0                       # 0x0
	.size	.L.str.21, 20

	.type	.L.str.22,@object       # @.str.22
	.p2align	2
.L.str.22:
	.long	103                     # 0x67
	.long	122                     # 0x7a
	.long	32                      # 0x20
	.long	103                     # 0x67
	.long	122                     # 0x7a
	.long	105                     # 0x69
	.long	112                     # 0x70
	.long	32                      # 0x20
	.long	116                     # 0x74
	.long	103                     # 0x67
	.long	122                     # 0x7a
	.long	32                      # 0x20
	.long	116                     # 0x74
	.long	112                     # 0x70
	.long	122                     # 0x7a
	.long	0                       # 0x0
	.size	.L.str.22, 64

	.type	.L.str.23,@object       # @.str.23
	.p2align	2
.L.str.23:
	.long	42                      # 0x2a
	.long	32                      # 0x20
	.long	42                      # 0x2a
	.long	32                      # 0x20
	.long	46                      # 0x2e
	.long	116                     # 0x74
	.long	97                      # 0x61
	.long	114                     # 0x72
	.long	32                      # 0x20
	.long	46                      # 0x2e
	.long	116                     # 0x74
	.long	97                      # 0x61
	.long	114                     # 0x72
	.long	0                       # 0x0
	.size	.L.str.23, 56

	.type	_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE,@object # @_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.section	.rodata._ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE,"aG",@progbits,_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE,comdat
	.weak	_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.p2align	3
_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE:
	.quad	0
	.quad	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
	.quad	_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder10CodeResumeEP20ISequentialOutStreamPKyP21ICompressProgressInfo
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy
	.quad	-8
	.quad	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy
	.quad	-16
	.quad	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv
	.quad	-24
	.quad	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy
	.quad	-32
	.quad	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj
	.size	_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE, 376

	.type	_ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE,@object # @_ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.section	.rodata._ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE,"aG",@progbits,_ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE,comdat
	.weak	_ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.p2align	4
_ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE:
	.asciz	"N9NCompress8NDeflate8NDecoder9CCOMCoderE"
	.size	_ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE, 41

	.type	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE,@object # @_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.section	.rodata._ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE,"aG",@progbits,_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE,comdat
	.weak	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.p2align	4
_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.quad	_ZTIN9NCompress8NDeflate8NDecoder6CCoderE
	.size	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_GzHandler.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
