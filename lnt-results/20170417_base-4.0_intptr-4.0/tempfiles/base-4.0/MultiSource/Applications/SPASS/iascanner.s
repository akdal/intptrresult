	.text
	.file	"iascanner.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	ia_lex
	.p2align	4, 0x90
	.type	ia_lex,@function
ia_lex:                                 # @ia_lex
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movb	yy_init(%rip), %al
	testb	%al, %al
	jne	.LBB0_12
# BB#1:
	movb	$1, yy_init(%rip)
	movb	yy_start(%rip), %al
	testb	%al, %al
	jne	.LBB0_3
# BB#2:
	movb	$1, yy_start(%rip)
.LBB0_3:
	movq	ia_in(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_5
# BB#4:
	movq	stdin(%rip), %rbp
	movq	%rbp, ia_in(%rip)
.LBB0_5:
	cmpq	$0, ia_out(%rip)
	jne	.LBB0_7
# BB#6:
	movq	stdout(%rip), %rax
	movq	%rax, ia_out(%rip)
.LBB0_7:
	movq	yy_current_buffer(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_11
# BB#8:
	movl	$56, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_132
# BB#9:
	movl	$16384, 24(%rbx)        # imm = 0x4000
	movl	$16386, %edi            # imm = 0x4002
	callq	malloc
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.LBB0_132
# BB#10:                                # %ia__create_buffer.exit
	movb	$0, (%rax)
	movb	$0, 1(%rax)
	movq	%rax, 16(%rbx)
	movl	$0, 48(%rbx)
	movq	%rbp, (%rbx)
	movl	$1, 44(%rbx)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,1,1]
	movups	%xmm0, 28(%rbx)
	movq	%rbx, yy_current_buffer(%rip)
.LBB0_11:
	movl	28(%rbx), %eax
	movl	%eax, yy_n_chars(%rip)
	movq	16(%rbx), %rax
	movq	%rax, yy_c_buf_p(%rip)
	movq	%rax, ia_text(%rip)
	movq	(%rbx), %rcx
	movq	%rcx, ia_in(%rip)
	movb	(%rax), %al
	movb	%al, yy_hold_char(%rip)
.LBB0_12:                               # %.preheader.preheader
	movabsq	$-4294967296, %rbx      # imm = 0xFFFFFFFF00000000
	movl	$8192, %r12d            # imm = 0x2000
	jmp	.LBB0_13
.LBB0_39:                               #   in Loop: Header=BB0_13 Depth=1
	movq	ia_text(%rip), %rdi
	movslq	ia_leng(%rip), %rsi
	movq	ia_out(%rip), %rcx
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB0_13
.LBB0_32:                               #   in Loop: Header=BB0_13 Depth=1
	incl	dfg_LINENUMBER(%rip)
	.p2align	4, 0x90
.LBB0_13:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_14 Depth 2
                                        #       Child Loop BB0_15 Depth 3
                                        #       Child Loop BB0_17 Depth 3
                                        #         Child Loop BB0_18 Depth 4
                                        #           Child Loop BB0_71 Depth 5
                                        #           Child Loop BB0_75 Depth 5
                                        #           Child Loop BB0_78 Depth 5
                                        #           Child Loop BB0_81 Depth 5
                                        #           Child Loop BB0_88 Depth 5
                                        #         Child Loop BB0_53 Depth 4
                                        #         Child Loop BB0_113 Depth 4
                                        #       Child Loop BB0_118 Depth 3
	movq	yy_c_buf_p(%rip), %rax
	movb	yy_hold_char(%rip), %cl
	movb	%cl, (%rax)
	movzbl	yy_start(%rip), %edx
	movq	%rax, %rbp
	jmp	.LBB0_14
.LBB0_60:                               #   in Loop: Header=BB0_14 Depth=2
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$4, %rcx
	movswl	yy_nxt+2(%rcx), %edx
	incq	%rbp
	movq	%rbp, yy_c_buf_p(%rip)
	.p2align	4, 0x90
.LBB0_14:                               # %.thread
                                        #   Parent Loop BB0_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_15 Depth 3
                                        #       Child Loop BB0_17 Depth 3
                                        #         Child Loop BB0_18 Depth 4
                                        #           Child Loop BB0_71 Depth 5
                                        #           Child Loop BB0_75 Depth 5
                                        #           Child Loop BB0_78 Depth 5
                                        #           Child Loop BB0_81 Depth 5
                                        #           Child Loop BB0_88 Depth 5
                                        #         Child Loop BB0_53 Depth 4
                                        #         Child Loop BB0_113 Depth 4
                                        #       Child Loop BB0_118 Depth 3
	decq	%rbp
	.p2align	4, 0x90
.LBB0_15:                               #   Parent Loop BB0_13 Depth=1
                                        #     Parent Loop BB0_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%edx, %rcx
	movzbl	1(%rbp), %edx
	incq	%rbp
	movslq	yy_ec(,%rdx,4), %rdx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$4, %rcx
	movswl	yy_nxt(%rcx,%rdx,2), %edx
	testl	%edx, %edx
	jg	.LBB0_15
# BB#16:                                #   in Loop: Header=BB0_14 Depth=2
	negl	%edx
	jmp	.LBB0_17
.LBB0_48:                               # %.lr.ph.i.prol
                                        #   in Loop: Header=BB0_17 Depth=3
	movzbl	(%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB0_49
# BB#50:                                #   in Loop: Header=BB0_17 Depth=3
	movl	yy_ec(,%rcx,4), %ecx
	jmp	.LBB0_51
.LBB0_49:                               #   in Loop: Header=BB0_17 Depth=3
	movl	$1, %ecx
.LBB0_51:                               #   in Loop: Header=BB0_17 Depth=3
	movslq	%ecx, %rcx
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$4, %rdx
	movswl	yy_nxt(%rdx,%rcx,2), %edx
	leaq	1(%rax), %rcx
	cmpq	$1, %rsi
	je	.LBB0_58
.LBB0_53:                               # %.lr.ph.i
                                        #   Parent Loop BB0_13 Depth=1
                                        #     Parent Loop BB0_14 Depth=2
                                        #       Parent Loop BB0_17 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movslq	%edx, %r8
	movzbl	(%rcx), %edi
	testq	%rdi, %rdi
	movl	$1, %edx
	movl	$1, %esi
	je	.LBB0_55
# BB#54:                                #   in Loop: Header=BB0_53 Depth=4
	movl	yy_ec(,%rdi,4), %esi
.LBB0_55:                               # %.lr.ph.i.1378
                                        #   in Loop: Header=BB0_53 Depth=4
	movslq	%esi, %rdi
	leaq	(%r8,%r8,2), %rsi
	shlq	$4, %rsi
	movswq	yy_nxt(%rsi,%rdi,2), %rsi
	movzbl	1(%rcx), %edi
	testq	%rdi, %rdi
	je	.LBB0_57
# BB#56:                                #   in Loop: Header=BB0_53 Depth=4
	movl	yy_ec(,%rdi,4), %edx
.LBB0_57:                               #   in Loop: Header=BB0_53 Depth=4
	movslq	%edx, %rdx
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$4, %rsi
	movswl	yy_nxt(%rsi,%rdx,2), %edx
	addq	$2, %rcx
	cmpq	%rbp, %rcx
	jne	.LBB0_53
.LBB0_58:                               # %yy_get_previous_state.exit
                                        #   in Loop: Header=BB0_17 Depth=3
	movslq	%edx, %rcx
	leaq	-1(%rcx), %rsi
	cmpq	$1, %rsi
	ja	.LBB0_17
# BB#59:                                # %yy_get_previous_state.exit
                                        #   in Loop: Header=BB0_17 Depth=3
	testl	%edx, %edx
	je	.LBB0_17
	jmp	.LBB0_60
.LBB0_124:                              #   in Loop: Header=BB0_17 Depth=3
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB0_17:                               # %.thread76
                                        #   Parent Loop BB0_13 Depth=1
                                        #     Parent Loop BB0_14 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_18 Depth 4
                                        #           Child Loop BB0_71 Depth 5
                                        #           Child Loop BB0_75 Depth 5
                                        #           Child Loop BB0_78 Depth 5
                                        #           Child Loop BB0_81 Depth 5
                                        #           Child Loop BB0_88 Depth 5
                                        #         Child Loop BB0_53 Depth 4
                                        #         Child Loop BB0_113 Depth 4
	movslq	%edx, %rcx
	movswl	yy_accept(%rcx,%rcx), %ecx
	movq	%rax, ia_text(%rip)
	movl	%ebp, %edx
	subl	%eax, %edx
	movl	%edx, ia_leng(%rip)
	movb	(%rbp), %al
	movb	%al, yy_hold_char(%rip)
	movb	$0, (%rbp)
	movq	%rbp, yy_c_buf_p(%rip)
	jmp	.LBB0_18
.LBB0_91:                               # %.thread74.i
                                        #   in Loop: Header=BB0_18 Depth=4
	cmpl	$-1, %eax
	je	.LBB0_95
# BB#92:                                # %.thread74.i
                                        #   in Loop: Header=BB0_18 Depth=4
	cmpl	$10, %eax
	jne	.LBB0_94
# BB#93:                                # %.thread76.i
                                        #   in Loop: Header=BB0_18 Depth=4
	movq	yy_current_buffer(%rip), %rax
	movq	%r13, %rcx
	addq	8(%rax), %rcx
	movslq	%ebx, %rax
	incl	%ebx
	movb	$10, (%rax,%rcx)
	jmp	.LBB0_94
.LBB0_105:                              #   in Loop: Header=BB0_18 Depth=4
	movq	ia_in(%rip), %rdi
	callq	ia_restart
	movl	yy_n_chars(%rip), %ebx
	movl	$1, %esi
	movq	yy_current_buffer(%rip), %r14
	jmp	.LBB0_107
.LBB0_97:                               #   in Loop: Header=BB0_18 Depth=4
	movslq	%r13d, %rdi
	addq	8(%r14), %rdi
	movslq	%edx, %rdx
	movq	ia_in(%rip), %rcx
	movl	$1, %esi
	callq	fread
	movq	%rax, %rbx
	movl	%ebx, yy_n_chars(%rip)
	testl	%ebx, %ebx
	je	.LBB0_99
# BB#98:                                # %.thread111.i
                                        #   in Loop: Header=BB0_18 Depth=4
	movq	yy_current_buffer(%rip), %r14
	movl	%ebx, 28(%r14)
	xorl	%esi, %esi
	jmp	.LBB0_107
.LBB0_95:                               #   in Loop: Header=BB0_18 Depth=4
	movq	ia_in(%rip), %rdi
	callq	ferror
	testl	%eax, %eax
	jne	.LBB0_96
.LBB0_94:                               #   in Loop: Header=BB0_18 Depth=4
	movl	%ebx, yy_n_chars(%rip)
	movq	yy_current_buffer(%rip), %r14
	movl	%ebx, 28(%r14)
	xorl	%esi, %esi
	testl	%ebx, %ebx
	je	.LBB0_104
.LBB0_107:                              #   in Loop: Header=BB0_18 Depth=4
	addl	%r13d, %ebx
	movl	%ebx, yy_n_chars(%rip)
	movq	8(%r14), %rax
	movslq	%ebx, %rcx
	movb	$0, (%rax,%rcx)
	movq	8(%r14), %rax
	movslq	yy_n_chars(%rip), %rcx
	movb	$0, 1(%rax,%rcx)
	movq	yy_current_buffer(%rip), %rax
	movq	8(%rax), %rax
	movq	%rax, ia_text(%rip)
	movq	%rax, %rcx
	movabsq	$-4294967296, %rbx      # imm = 0xFFFFFFFF00000000
	cmpl	$1, %esi
	je	.LBB0_115
	jmp	.LBB0_109
.LBB0_70:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_18 Depth=4
	leaq	(%rax,%rbx), %rdi
	addq	$16, %rax
	leaq	(%rcx,%rbx), %rsi
	addq	$16, %rcx
	movq	%rbx, %rdx
.LBB0_71:                               # %vector.body
                                        #   Parent Loop BB0_13 Depth=1
                                        #     Parent Loop BB0_14 Depth=2
                                        #       Parent Loop BB0_17 Depth=3
                                        #         Parent Loop BB0_18 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movups	-16(%rax), %xmm0
	movups	(%rax), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	addq	$32, %rax
	addq	$32, %rcx
	addq	$-32, %rdx
	jne	.LBB0_71
# BB#72:                                # %middle.block
                                        #   in Loop: Header=BB0_18 Depth=4
	testq	%r8, %r8
	jne	.LBB0_73
	jmp	.LBB0_79
.LBB0_99:                               #   in Loop: Header=BB0_18 Depth=4
	movq	ia_in(%rip), %rdi
	callq	ferror
	testl	%eax, %eax
	jne	.LBB0_101
# BB#100:                               # %.thread109.i
                                        #   in Loop: Header=BB0_18 Depth=4
	movq	yy_current_buffer(%rip), %r14
	jmp	.LBB0_103
.LBB0_115:                              # %.thread77
                                        #   in Loop: Header=BB0_18 Depth=4
	movq	%rax, yy_c_buf_p(%rip)
	movzbl	yy_start(%rip), %eax
	movl	%eax, %ecx
	notb	%cl
	movzbl	%cl, %ecx
	xorl	$1, %eax
	andl	$1, %ecx
	subl	%eax, %ecx
	sarl	%ecx
	addl	$21, %ecx
	.p2align	4, 0x90
.LBB0_18:                               #   Parent Loop BB0_13 Depth=1
                                        #     Parent Loop BB0_14 Depth=2
                                        #       Parent Loop BB0_17 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_71 Depth 5
                                        #           Child Loop BB0_75 Depth 5
                                        #           Child Loop BB0_78 Depth 5
                                        #           Child Loop BB0_81 Depth 5
                                        #           Child Loop BB0_88 Depth 5
	decl	%ecx
	cmpl	$20, %ecx
	ja	.LBB0_125
# BB#19:                                #   in Loop: Header=BB0_18 Depth=4
	jmpq	*.LJTI0_0(,%rcx,8)
.LBB0_41:                               #   in Loop: Header=BB0_18 Depth=4
	movq	ia_text(%rip), %rsi
	movb	yy_hold_char(%rip), %al
	movb	%al, (%rbp)
	movq	yy_current_buffer(%rip), %r14
	cmpl	$0, 48(%r14)
	je	.LBB0_43
# BB#42:                                # %._crit_edge
                                        #   in Loop: Header=BB0_18 Depth=4
	movl	yy_n_chars(%rip), %eax
	jmp	.LBB0_44
.LBB0_43:                               #   in Loop: Header=BB0_18 Depth=4
	movl	28(%r14), %eax
	movl	%eax, yy_n_chars(%rip)
	movq	ia_in(%rip), %rcx
	movq	%rcx, (%r14)
	movl	$1, 48(%r14)
	movq	yy_current_buffer(%rip), %r14
.LBB0_44:                               #   in Loop: Header=BB0_18 Depth=4
	movq	yy_c_buf_p(%rip), %r15
	movq	8(%r14), %rcx
	movslq	%eax, %rdx
	leaq	(%rcx,%rdx), %rax
	cmpq	%rax, %r15
	movq	ia_text(%rip), %rax
	jbe	.LBB0_45
# BB#61:                                #   in Loop: Header=BB0_18 Depth=4
	leaq	1(%rcx,%rdx), %rdx
	cmpq	%rdx, %r15
	ja	.LBB0_133
# BB#62:                                #   in Loop: Header=BB0_18 Depth=4
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%r15, %r9
	subq	%rax, %r9
	cmpl	$0, 44(%r14)
	je	.LBB0_63
# BB#64:                                #   in Loop: Header=BB0_18 Depth=4
	movl	$4294967294, %esi       # imm = 0xFFFFFFFE
	movslq	%r9d, %rdx
	leaq	-1(%rdx), %r13
	cmpl	$2, %edx
	jl	.LBB0_79
# BB#65:                                # %.lr.ph100.i.preheader
                                        #   in Loop: Header=BB0_18 Depth=4
	leal	-2(%r9), %ebx
	incq	%rbx
	cmpq	$32, %rbx
	jb	.LBB0_66
# BB#67:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_18 Depth=4
	leal	31(%r9), %r8d
	andl	$31, %r8d
	subq	%r8, %rbx
	je	.LBB0_66
# BB#68:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_18 Depth=4
	movl	%r9d, %edx
	addl	%esi, %edx
	leaq	1(%rax,%rdx), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB0_70
# BB#69:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_18 Depth=4
	leaq	1(%rcx,%rdx), %rdx
	cmpq	%rdx, %rax
	jae	.LBB0_70
.LBB0_66:                               #   in Loop: Header=BB0_18 Depth=4
	movq	%rcx, %rsi
	movq	%rax, %rdi
	xorl	%ebx, %ebx
.LBB0_73:                               # %.lr.ph100.i.preheader316
                                        #   in Loop: Header=BB0_18 Depth=4
	leal	7(%r9), %ecx
	subl	%ebx, %ecx
	leal	-2(%r9), %eax
	subl	%ebx, %eax
	andl	$7, %ecx
	je	.LBB0_76
# BB#74:                                # %.lr.ph100.i.prol.preheader
                                        #   in Loop: Header=BB0_18 Depth=4
	negl	%ecx
	.p2align	4, 0x90
.LBB0_75:                               # %.lr.ph100.i.prol
                                        #   Parent Loop BB0_13 Depth=1
                                        #     Parent Loop BB0_14 Depth=2
                                        #       Parent Loop BB0_17 Depth=3
                                        #         Parent Loop BB0_18 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzbl	(%rdi), %edx
	incq	%rdi
	movb	%dl, (%rsi)
	incq	%rsi
	incl	%ebx
	incl	%ecx
	jne	.LBB0_75
.LBB0_76:                               # %.lr.ph100.i.prol.loopexit
                                        #   in Loop: Header=BB0_18 Depth=4
	cmpl	$7, %eax
	jb	.LBB0_79
# BB#77:                                # %.lr.ph100.i.preheader316.new
                                        #   in Loop: Header=BB0_18 Depth=4
	leal	-1(%r9), %eax
	subl	%ebx, %eax
	.p2align	4, 0x90
.LBB0_78:                               # %.lr.ph100.i
                                        #   Parent Loop BB0_13 Depth=1
                                        #     Parent Loop BB0_14 Depth=2
                                        #       Parent Loop BB0_17 Depth=3
                                        #         Parent Loop BB0_18 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzbl	(%rdi), %ecx
	movb	%cl, (%rsi)
	movzbl	1(%rdi), %ecx
	movb	%cl, 1(%rsi)
	movzbl	2(%rdi), %ecx
	movb	%cl, 2(%rsi)
	movzbl	3(%rdi), %ecx
	movb	%cl, 3(%rsi)
	movzbl	4(%rdi), %ecx
	movb	%cl, 4(%rsi)
	movzbl	5(%rdi), %ecx
	movb	%cl, 5(%rsi)
	movzbl	6(%rdi), %ecx
	movb	%cl, 6(%rsi)
	movzbl	7(%rdi), %ecx
	movb	%cl, 7(%rsi)
	addq	$8, %rdi
	addq	$8, %rsi
	addl	$-8, %eax
	jne	.LBB0_78
.LBB0_79:                               # %._crit_edge101.i
                                        #   in Loop: Header=BB0_18 Depth=4
	cmpl	$2, 48(%r14)
	jne	.LBB0_80
# BB#102:                               # %.thread108.i
                                        #   in Loop: Header=BB0_18 Depth=4
	movl	$0, yy_n_chars(%rip)
.LBB0_103:                              # %.sink.split.i
                                        #   in Loop: Header=BB0_18 Depth=4
	movl	$0, 28(%r14)
.LBB0_104:                              #   in Loop: Header=BB0_18 Depth=4
	testl	%r13d, %r13d
	je	.LBB0_105
# BB#106:                               #   in Loop: Header=BB0_18 Depth=4
	movl	$2, 48(%r14)
	movl	$2, %esi
	xorl	%ebx, %ebx
	jmp	.LBB0_107
.LBB0_63:                               #   in Loop: Header=BB0_18 Depth=4
	xorl	%esi, %esi
	cmpq	$1, %r9
	setne	%sil
	incl	%esi
	cmpl	$1, %esi
	je	.LBB0_115
	jmp	.LBB0_109
.LBB0_80:                               # %.preheader78.i
                                        #   in Loop: Header=BB0_18 Depth=4
	movl	24(%r14), %ecx
	movl	%ecx, %edx
	subl	%r13d, %edx
	decl	%edx
	testl	%edx, %edx
	jg	.LBB0_86
	.p2align	4, 0x90
.LBB0_81:                               # %.lr.ph94.i
                                        #   Parent Loop BB0_13 Depth=1
                                        #     Parent Loop BB0_14 Depth=2
                                        #       Parent Loop BB0_17 Depth=3
                                        #         Parent Loop BB0_18 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpl	$0, 32(%r14)
	je	.LBB0_82
# BB#84:                                #   in Loop: Header=BB0_81 Depth=5
	movq	8(%r14), %rbx
	leal	(%rcx,%rcx), %eax
	movl	%ecx, %esi
	shrl	$3, %esi
	addl	%ecx, %esi
	testl	%eax, %eax
	cmovgl	%eax, %esi
	movl	%esi, 24(%r14)
	addl	$2, %esi
	movq	%rbx, %rdi
	callq	realloc
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.LBB0_83
# BB#85:                                #   in Loop: Header=BB0_81 Depth=5
	subq	%rbx, %r15
	movslq	%r15d, %rcx
	addq	%rcx, %rax
	movq	%rax, yy_c_buf_p(%rip)
	movq	yy_current_buffer(%rip), %r14
	movl	24(%r14), %ecx
	movl	%ecx, %edx
	subl	%r13d, %edx
	decl	%edx
	testl	%edx, %edx
	movq	%rax, %r15
	jle	.LBB0_81
.LBB0_86:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_18 Depth=4
	cmpl	$8193, %edx             # imm = 0x2001
	cmovgel	%r12d, %edx
	cmpl	$0, 36(%r14)
	je	.LBB0_97
# BB#87:                                # %.lr.ph.i57
                                        #   in Loop: Header=BB0_18 Depth=4
	movslq	%edx, %r14
	xorl	%ebx, %ebx
.LBB0_88:                               #   Parent Loop BB0_13 Depth=1
                                        #     Parent Loop BB0_14 Depth=2
                                        #       Parent Loop BB0_17 Depth=3
                                        #         Parent Loop BB0_18 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	ia_in(%rip), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB0_91
# BB#89:                                #   in Loop: Header=BB0_88 Depth=5
	cmpl	$10, %eax
	je	.LBB0_91
# BB#90:                                #   in Loop: Header=BB0_88 Depth=5
	movq	yy_current_buffer(%rip), %rcx
	movq	8(%rcx), %rcx
	addq	%r13, %rcx
	movb	%al, (%rbx,%rcx)
	incq	%rbx
	cmpq	%r14, %rbx
	jl	.LBB0_88
	jmp	.LBB0_91
.LBB0_45:                               #   in Loop: Header=BB0_17 Depth=3
	subl	%esi, %ebp
	shlq	$32, %rbp
	addq	%rbx, %rbp
	movq	%rbp, %rsi
	sarq	$32, %rsi
	testq	%rbp, %rbp
	leaq	(%rax,%rsi), %rbp
	movq	%rbp, yy_c_buf_p(%rip)
	movzbl	yy_start(%rip), %edx
	jle	.LBB0_58
# BB#46:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_17 Depth=3
	testb	$1, %sil
	jne	.LBB0_48
# BB#47:                                #   in Loop: Header=BB0_17 Depth=3
	movq	%rax, %rcx
	cmpq	$1, %rsi
	jne	.LBB0_53
	jmp	.LBB0_58
.LBB0_109:                              # %yy_get_next_buffer.exit
                                        #   in Loop: Header=BB0_17 Depth=3
	testl	%esi, %esi
	je	.LBB0_116
# BB#110:                               # %yy_get_next_buffer.exit
                                        #   in Loop: Header=BB0_17 Depth=3
	cmpl	$2, %esi
	jne	.LBB0_13
# BB#111:                               #   in Loop: Header=BB0_17 Depth=3
	movslq	yy_n_chars(%rip), %rdx
	addq	%rdx, %rcx
	movq	%rcx, yy_c_buf_p(%rip)
	movzbl	yy_start(%rip), %edx
	cmpq	%rcx, %rax
	movq	%rcx, %rbp
	jae	.LBB0_17
# BB#112:                               # %.lr.ph.i68.preheader
                                        #   in Loop: Header=BB0_17 Depth=3
	movq	%rax, %rsi
.LBB0_113:                              # %.lr.ph.i68
                                        #   Parent Loop BB0_13 Depth=1
                                        #     Parent Loop BB0_14 Depth=2
                                        #       Parent Loop BB0_17 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movslq	%edx, %rdx
	movzbl	(%rsi), %edi
	testq	%rdi, %rdi
	je	.LBB0_114
# BB#122:                               #   in Loop: Header=BB0_113 Depth=4
	movl	yy_ec(,%rdi,4), %edi
	jmp	.LBB0_123
.LBB0_114:                              #   in Loop: Header=BB0_113 Depth=4
	movl	$1, %edi
.LBB0_123:                              #   in Loop: Header=BB0_113 Depth=4
	movslq	%edi, %rdi
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$4, %rdx
	movswl	yy_nxt(%rdx,%rdi,2), %edx
	incq	%rsi
	cmpq	%rsi, %rcx
	jne	.LBB0_113
	jmp	.LBB0_124
.LBB0_116:                              #   in Loop: Header=BB0_14 Depth=2
	subq	(%rsp), %rbp            # 8-byte Folded Reload
	movl	%ebp, %ecx
	shlq	$32, %rbp
	addq	%rbx, %rbp
	sarq	$32, %rbp
	addq	%rax, %rbp
	movq	%rbp, yy_c_buf_p(%rip)
	movzbl	yy_start(%rip), %edx
	cmpl	$2, %ecx
	jl	.LBB0_14
# BB#117:                               # %.lr.ph.i61.preheader
                                        #   in Loop: Header=BB0_14 Depth=2
	movq	%rax, %rcx
.LBB0_118:                              # %.lr.ph.i61
                                        #   Parent Loop BB0_13 Depth=1
                                        #     Parent Loop BB0_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%edx, %rdx
	movzbl	(%rcx), %esi
	testq	%rsi, %rsi
	je	.LBB0_119
# BB#120:                               #   in Loop: Header=BB0_118 Depth=3
	movl	yy_ec(,%rsi,4), %esi
	jmp	.LBB0_121
.LBB0_119:                              #   in Loop: Header=BB0_118 Depth=3
	movl	$1, %esi
.LBB0_121:                              #   in Loop: Header=BB0_118 Depth=3
	movslq	%esi, %rsi
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$4, %rdx
	movswl	yy_nxt(%rdx,%rsi,2), %edx
	incq	%rcx
	cmpq	%rbp, %rcx
	jb	.LBB0_118
	jmp	.LBB0_14
.LBB0_127:                              # %.loopexit123
	movl	$259, %eax              # imm = 0x103
	jmp	.LBB0_131
.LBB0_128:                              # %.loopexit180
	movl	$260, %eax              # imm = 0x104
	jmp	.LBB0_131
.LBB0_130:                              # %.loopexit317
	movl	$262, %eax              # imm = 0x106
	jmp	.LBB0_131
.LBB0_20:
	movl	$263, %eax              # imm = 0x107
	jmp	.LBB0_131
.LBB0_27:
	callq	__errno_location
	movq	%rax, %rbx
	movl	$0, (%rbx)
	movq	ia_text(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtoul
	cmpq	$2147483647, %rax       # imm = 0x7FFFFFFF
	ja	.LBB0_29
# BB#28:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.LBB0_29
# BB#30:
	movl	%eax, ia_lval(%rip)
	movl	$270, %eax              # imm = 0x10E
	jmp	.LBB0_131
.LBB0_31:
	movl	ia_leng(%rip), %edi
	incl	%edi
	callq	memory_Malloc
	movq	%rax, ia_lval(%rip)
	movq	ia_text(%rip), %rsi
	movq	%rax, %rdi
	callq	strcpy
	movl	$271, %eax              # imm = 0x10F
	jmp	.LBB0_131
.LBB0_33:
	movq	ia_text(%rip), %rax
	movsbl	(%rax), %eax
	jmp	.LBB0_131
.LBB0_40:
	xorl	%eax, %eax
	jmp	.LBB0_131
.LBB0_126:                              # %.loopexit
	movl	$258, %eax              # imm = 0x102
	jmp	.LBB0_131
.LBB0_129:                              # %.loopexit245
	movl	$261, %eax              # imm = 0x105
	jmp	.LBB0_131
.LBB0_21:
	movl	$264, %eax              # imm = 0x108
	jmp	.LBB0_131
.LBB0_22:
	movl	$265, %eax              # imm = 0x109
	jmp	.LBB0_131
.LBB0_23:
	movl	$266, %eax              # imm = 0x10A
	jmp	.LBB0_131
.LBB0_24:
	movl	$267, %eax              # imm = 0x10B
	jmp	.LBB0_131
.LBB0_25:
	movl	$269, %eax              # imm = 0x10D
	jmp	.LBB0_131
.LBB0_26:
	movl	$268, %eax              # imm = 0x10C
.LBB0_131:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_125:
	movl	$.L.str.5, %edi
	callq	yy_fatal_error
.LBB0_34:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movq	ia_text(%rip), %rcx
	movsbq	(%rcx), %rsi
	testb	$64, 1(%rax,%rsi,2)
	jne	.LBB0_35
# BB#36:
	movl	$.L.str.3, %edi
	jmp	.LBB0_37
.LBB0_132:
	movl	$.L.str.6, %edi
	callq	yy_fatal_error
.LBB0_35:
	movl	$.L.str.2, %edi
.LBB0_37:
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	misc_UserErrorReport
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str.4, %edi
.LBB0_38:
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB0_82:                               # %.thread.i
	movq	$0, 8(%r14)
.LBB0_83:                               # %.loopexit.i
	movl	$.L.str.11, %edi
	callq	yy_fatal_error
.LBB0_29:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str, %edi
	jmp	.LBB0_38
.LBB0_133:
	movl	$.L.str.10, %edi
	callq	yy_fatal_error
.LBB0_96:
	movl	$.L.str.12, %edi
	callq	yy_fatal_error
.LBB0_101:
	movl	$.L.str.12, %edi
	callq	yy_fatal_error
.Lfunc_end0:
	.size	ia_lex, .Lfunc_end0-ia_lex
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_126
	.quad	.LBB0_127
	.quad	.LBB0_128
	.quad	.LBB0_129
	.quad	.LBB0_130
	.quad	.LBB0_20
	.quad	.LBB0_21
	.quad	.LBB0_22
	.quad	.LBB0_23
	.quad	.LBB0_24
	.quad	.LBB0_25
	.quad	.LBB0_26
	.quad	.LBB0_27
	.quad	.LBB0_31
	.quad	.LBB0_13
	.quad	.LBB0_32
	.quad	.LBB0_33
	.quad	.LBB0_34
	.quad	.LBB0_39
	.quad	.LBB0_41
	.quad	.LBB0_40

	.text
	.globl	ia__create_buffer
	.p2align	4, 0x90
	.type	ia__create_buffer,@function
ia__create_buffer:                      # @ia__create_buffer
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	movl	$56, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_5
# BB#1:
	movl	%r15d, 24(%rbx)
	addl	$2, %r15d
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.LBB1_5
# BB#2:
	movl	$1, 32(%rbx)
	movl	$0, 28(%rbx)
	movb	$0, (%rax)
	movb	$0, 1(%rax)
	movq	%rax, 16(%rbx)
	movl	$1, 40(%rbx)
	movl	$0, 48(%rbx)
	cmpq	%rbx, yy_current_buffer(%rip)
	jne	.LBB1_4
# BB#3:
	movl	$0, yy_n_chars(%rip)
	movq	%rax, yy_c_buf_p(%rip)
	movq	%rax, ia_text(%rip)
	movb	$0, yy_hold_char(%rip)
.LBB1_4:                                # %ia__init_buffer.exit
	movq	%r14, (%rbx)
	movl	$1, 44(%rbx)
	movl	$1, 36(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB1_5:
	movl	$.L.str.6, %edi
	callq	yy_fatal_error
.Lfunc_end1:
	.size	ia__create_buffer, .Lfunc_end1-ia__create_buffer
	.cfi_endproc

	.globl	ia__load_buffer_state
	.p2align	4, 0x90
	.type	ia__load_buffer_state,@function
ia__load_buffer_state:                  # @ia__load_buffer_state
	.cfi_startproc
# BB#0:
	movq	yy_current_buffer(%rip), %rax
	movl	28(%rax), %ecx
	movl	%ecx, yy_n_chars(%rip)
	movq	16(%rax), %rcx
	movq	%rcx, yy_c_buf_p(%rip)
	movq	%rcx, ia_text(%rip)
	movq	(%rax), %rax
	movq	%rax, ia_in(%rip)
	movb	(%rcx), %al
	movb	%al, yy_hold_char(%rip)
	retq
.Lfunc_end2:
	.size	ia__load_buffer_state, .Lfunc_end2-ia__load_buffer_state
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end3:
	.size	misc_Error, .Lfunc_end3-misc_Error
	.cfi_endproc

	.p2align	4, 0x90
	.type	yy_fatal_error,@function
yy_fatal_error:                         # @yy_fatal_error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rcx
	movq	stderr(%rip), %rdi
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	fprintf
	movl	$2, %edi
	callq	exit
.Lfunc_end4:
	.size	yy_fatal_error, .Lfunc_end4-yy_fatal_error
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	ia_restart
	.p2align	4, 0x90
	.type	ia_restart,@function
ia_restart:                             # @ia_restart
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	yy_current_buffer(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_4
# BB#1:
	movq	ia_in(%rip), %r15
	movl	$56, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_7
# BB#2:
	movl	$16384, 24(%rbx)        # imm = 0x4000
	movl	$16386, %edi            # imm = 0x4002
	callq	malloc
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.LBB5_7
# BB#3:                                 # %ia__create_buffer.exit
	movb	$0, (%rax)
	movb	$0, 1(%rax)
	movq	%rax, 16(%rbx)
	movl	$0, 48(%rbx)
	movq	%r15, (%rbx)
	movl	$1, 44(%rbx)
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [0,1,1,1]
	movups	%xmm0, 28(%rbx)
	movq	%rbx, yy_current_buffer(%rip)
.LBB5_4:
	movl	$0, 28(%rbx)
	movq	8(%rbx), %rax
	movb	$0, (%rax)
	movq	8(%rbx), %rax
	movb	$0, 1(%rax)
	movq	8(%rbx), %rax
	movq	%rax, 16(%rbx)
	movl	$1, 40(%rbx)
	movl	$0, 48(%rbx)
	cmpq	%rbx, %rbx
	jne	.LBB5_6
# BB#5:
	movl	28(%rbx), %ecx
	movl	%ecx, yy_n_chars(%rip)
	movq	%rax, yy_c_buf_p(%rip)
	movq	%rax, ia_text(%rip)
	movq	(%rbx), %rcx
	movq	%rcx, ia_in(%rip)
	movb	(%rax), %al
	movb	%al, yy_hold_char(%rip)
.LBB5_6:                                # %ia__init_buffer.exit
	movq	%r14, (%rbx)
	movl	$1, 44(%rbx)
	movl	$1, 36(%rbx)
	movl	28(%rbx), %eax
	movl	%eax, yy_n_chars(%rip)
	movq	16(%rbx), %rax
	movq	%rax, yy_c_buf_p(%rip)
	movq	%rax, ia_text(%rip)
	movq	(%rbx), %rcx
	movq	%rcx, ia_in(%rip)
	movb	(%rax), %al
	movb	%al, yy_hold_char(%rip)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB5_7:
	movl	$.L.str.6, %edi
	callq	yy_fatal_error
.Lfunc_end5:
	.size	ia_restart, .Lfunc_end5-ia_restart
	.cfi_endproc

	.globl	ia__init_buffer
	.p2align	4, 0x90
	.type	ia__init_buffer,@function
ia__init_buffer:                        # @ia__init_buffer
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB6_3
# BB#1:
	movl	$0, 28(%rdi)
	movq	8(%rdi), %rax
	movb	$0, (%rax)
	movq	8(%rdi), %rax
	movb	$0, 1(%rax)
	movq	8(%rdi), %rax
	movq	%rax, 16(%rdi)
	movl	$1, 40(%rdi)
	movl	$0, 48(%rdi)
	cmpq	%rdi, yy_current_buffer(%rip)
	jne	.LBB6_3
# BB#2:
	movl	28(%rdi), %ecx
	movl	%ecx, yy_n_chars(%rip)
	movq	%rax, yy_c_buf_p(%rip)
	movq	%rax, ia_text(%rip)
	movq	(%rdi), %rcx
	movq	%rcx, ia_in(%rip)
	movb	(%rax), %al
	movb	%al, yy_hold_char(%rip)
.LBB6_3:                                # %ia__flush_buffer.exit
	movq	%rsi, (%rdi)
	movl	$1, 44(%rdi)
	movl	$1, 36(%rdi)
	retq
.Lfunc_end6:
	.size	ia__init_buffer, .Lfunc_end6-ia__init_buffer
	.cfi_endproc

	.globl	ia__switch_to_buffer
	.p2align	4, 0x90
	.type	ia__switch_to_buffer,@function
ia__switch_to_buffer:                   # @ia__switch_to_buffer
	.cfi_startproc
# BB#0:
	movq	yy_current_buffer(%rip), %rax
	cmpq	%rdi, %rax
	je	.LBB7_4
# BB#1:
	testq	%rax, %rax
	je	.LBB7_3
# BB#2:
	movb	yy_hold_char(%rip), %cl
	movq	yy_c_buf_p(%rip), %rdx
	movb	%cl, (%rdx)
	movq	%rdx, 16(%rax)
	movl	yy_n_chars(%rip), %ecx
	movl	%ecx, 28(%rax)
.LBB7_3:
	movq	%rdi, yy_current_buffer(%rip)
	movl	28(%rdi), %eax
	movl	%eax, yy_n_chars(%rip)
	movq	16(%rdi), %rax
	movq	%rax, yy_c_buf_p(%rip)
	movq	%rax, ia_text(%rip)
	movq	(%rdi), %rcx
	movq	%rcx, ia_in(%rip)
	movb	(%rax), %al
	movb	%al, yy_hold_char(%rip)
.LBB7_4:
	retq
.Lfunc_end7:
	.size	ia__switch_to_buffer, .Lfunc_end7-ia__switch_to_buffer
	.cfi_endproc

	.globl	ia__delete_buffer
	.p2align	4, 0x90
	.type	ia__delete_buffer,@function
ia__delete_buffer:                      # @ia__delete_buffer
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 16
.Lcfi28:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB8_6
# BB#1:
	cmpq	%rbx, yy_current_buffer(%rip)
	jne	.LBB8_3
# BB#2:
	movq	$0, yy_current_buffer(%rip)
.LBB8_3:
	cmpl	$0, 32(%rbx)
	je	.LBB8_5
# BB#4:
	movq	8(%rbx), %rdi
	callq	free
.LBB8_5:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB8_6:
	popq	%rbx
	retq
.Lfunc_end8:
	.size	ia__delete_buffer, .Lfunc_end8-ia__delete_buffer
	.cfi_endproc

	.globl	ia__flush_buffer
	.p2align	4, 0x90
	.type	ia__flush_buffer,@function
ia__flush_buffer:                       # @ia__flush_buffer
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB9_3
# BB#1:
	movl	$0, 28(%rdi)
	movq	8(%rdi), %rax
	movb	$0, (%rax)
	movq	8(%rdi), %rax
	movb	$0, 1(%rax)
	movq	8(%rdi), %rax
	movq	%rax, 16(%rdi)
	movl	$1, 40(%rdi)
	movl	$0, 48(%rdi)
	cmpq	%rdi, yy_current_buffer(%rip)
	jne	.LBB9_3
# BB#2:
	movl	28(%rdi), %ecx
	movl	%ecx, yy_n_chars(%rip)
	movq	%rax, yy_c_buf_p(%rip)
	movq	%rax, ia_text(%rip)
	movq	(%rdi), %rcx
	movq	%rcx, ia_in(%rip)
	movb	(%rax), %al
	movb	%al, yy_hold_char(%rip)
.LBB9_3:
	retq
.Lfunc_end9:
	.size	ia__flush_buffer, .Lfunc_end9-ia__flush_buffer
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.text
	.globl	ia__scan_buffer
	.p2align	4, 0x90
	.type	ia__scan_buffer,@function
ia__scan_buffer:                        # @ia__scan_buffer
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -24
.Lcfi33:
	.cfi_offset %r14, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %rbx
	cmpl	$2, %esi
	jae	.LBB10_2
# BB#1:
	xorl	%eax, %eax
	jmp	.LBB10_12
.LBB10_2:
	leal	-2(%rsi), %r14d
	cmpb	$0, (%rbx,%r14)
	je	.LBB10_4
# BB#3:
	xorl	%eax, %eax
	jmp	.LBB10_12
.LBB10_4:
	decl	%esi
	cmpb	$0, (%rbx,%rsi)
	je	.LBB10_6
# BB#5:
	xorl	%eax, %eax
	jmp	.LBB10_12
.LBB10_6:
	movl	$56, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB10_13
# BB#7:
	movl	%r14d, 24(%rax)
	movq	%rbx, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	$0, (%rax)
	movl	%r14d, 28(%rax)
	movaps	.LCPI10_0(%rip), %xmm0  # xmm0 = [0,0,1,0]
	movups	%xmm0, 32(%rax)
	movl	$0, 48(%rax)
	movq	yy_current_buffer(%rip), %rcx
	cmpq	%rax, %rcx
	je	.LBB10_12
# BB#8:
	testq	%rcx, %rcx
	je	.LBB10_9
# BB#10:
	movb	yy_hold_char(%rip), %dl
	movq	yy_c_buf_p(%rip), %rsi
	movb	%dl, (%rsi)
	movq	%rsi, 16(%rcx)
	movl	yy_n_chars(%rip), %edx
	movl	%edx, 28(%rcx)
	movl	28(%rax), %r14d
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	jmp	.LBB10_11
.LBB10_9:
	xorl	%ecx, %ecx
.LBB10_11:                              # %._crit_edge
	movq	%rax, yy_current_buffer(%rip)
	movl	%r14d, yy_n_chars(%rip)
	movq	%rbx, yy_c_buf_p(%rip)
	movq	%rbx, ia_text(%rip)
	movq	%rcx, ia_in(%rip)
	movb	(%rbx), %cl
	movb	%cl, yy_hold_char(%rip)
.LBB10_12:                              # %ia__switch_to_buffer.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB10_13:
	movl	$.L.str.7, %edi
	callq	yy_fatal_error
.Lfunc_end10:
	.size	ia__scan_buffer, .Lfunc_end10-ia__scan_buffer
	.cfi_endproc

	.globl	ia__scan_string
	.p2align	4, 0x90
	.type	ia__scan_string,@function
ia__scan_string:                        # @ia__scan_string
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 64
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	$-1, %rbx
	xorl	%r13d, %r13d
	movl	$4294967294, %ebp       # imm = 0xFFFFFFFE
	.p2align	4, 0x90
.LBB11_1:                               # =>This Inner Loop Header: Depth=1
	leaq	2(%r13,%rbp), %r13
	cmpb	$0, 1(%r12,%rbx)
	leaq	1(%rbx), %rbx
	jne	.LBB11_1
# BB#2:
	leaq	2(%rbx), %r14
	movl	%r14d, %edi
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB11_7
# BB#3:                                 # %.preheader.i
	testl	%ebx, %ebx
	jle	.LBB11_5
# BB#4:                                 # %.lr.ph.preheader.i
	leal	1(%rbx,%rbp), %edx
	incq	%rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	memcpy
.LBB11_5:                               # %._crit_edge.i
	sarq	$32, %r13
	movb	$0, (%r15,%r13)
	movb	$0, (%r15,%rbx)
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	ia__scan_buffer
	testq	%rax, %rax
	je	.LBB11_8
# BB#6:                                 # %ia__scan_bytes.exit
	movl	$1, 32(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_7:
	movl	$.L.str.8, %edi
	callq	yy_fatal_error
.LBB11_8:
	movl	$.L.str.9, %edi
	callq	yy_fatal_error
.Lfunc_end11:
	.size	ia__scan_string, .Lfunc_end11-ia__scan_string
	.cfi_endproc

	.globl	ia__scan_bytes
	.p2align	4, 0x90
	.type	ia__scan_bytes,@function
ia__scan_bytes:                         # @ia__scan_bytes
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 48
.Lcfi52:
	.cfi_offset %rbx, -40
.Lcfi53:
	.cfi_offset %r12, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %r15, -16
	movl	%esi, %r15d
	movq	%rdi, %r12
	leal	2(%r15), %r14d
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB12_5
# BB#1:                                 # %.preheader
	testl	%r15d, %r15d
	jle	.LBB12_3
# BB#2:                                 # %.lr.ph.preheader
	leal	-1(%r15), %edx
	incq	%rdx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcpy
.LBB12_3:                               # %._crit_edge
	movslq	%r15d, %rax
	movw	$0, (%rbx,%rax)
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	ia__scan_buffer
	testq	%rax, %rax
	je	.LBB12_6
# BB#4:
	movl	$1, 32(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB12_5:
	movl	$.L.str.8, %edi
	callq	yy_fatal_error
.LBB12_6:
	movl	$.L.str.9, %edi
	callq	yy_fatal_error
.Lfunc_end12:
	.size	ia__scan_bytes, .Lfunc_end12-ia__scan_bytes
	.cfi_endproc

	.type	ia_in,@object           # @ia_in
	.bss
	.globl	ia_in
	.p2align	3
ia_in:
	.quad	0
	.size	ia_in, 8

	.type	ia_out,@object          # @ia_out
	.globl	ia_out
	.p2align	3
ia_out:
	.quad	0
	.size	ia_out, 8

	.type	yy_init,@object         # @yy_init
	.local	yy_init
	.comm	yy_init,1,4
	.type	yy_start,@object        # @yy_start
	.local	yy_start
	.comm	yy_start,1,4
	.type	yy_current_buffer,@object # @yy_current_buffer
	.local	yy_current_buffer
	.comm	yy_current_buffer,8,8
	.type	yy_c_buf_p,@object      # @yy_c_buf_p
	.local	yy_c_buf_p
	.comm	yy_c_buf_p,8,8
	.type	yy_hold_char,@object    # @yy_hold_char
	.local	yy_hold_char
	.comm	yy_hold_char,1,1
	.type	yy_nxt,@object          # @yy_nxt
	.section	.rodata,"a",@progbits
	.p2align	4
yy_nxt:
	.zero	48
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	9                       # 0x9
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	14                      # 0xe
	.short	15                      # 0xf
	.short	16                      # 0x10
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	17                      # 0x11
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	9                       # 0x9
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	14                      # 0xe
	.short	15                      # 0xf
	.short	16                      # 0x10
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	17                      # 0x11
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	65533                   # 0xfffd
	.short	3                       # 0x3
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.short	3                       # 0x3
	.short	65531                   # 0xfffb
	.short	18                      # 0x12
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	65531                   # 0xfffb
	.short	3                       # 0x3
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	65530                   # 0xfffa
	.short	3                       # 0x3
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	65529                   # 0xfff9
	.short	3                       # 0x3
	.short	65528                   # 0xfff8
	.short	65528                   # 0xfff8
	.short	65528                   # 0xfff8
	.short	65528                   # 0xfff8
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65527                   # 0xfff7
	.short	65527                   # 0xfff7
	.short	65527                   # 0xfff7
	.short	65527                   # 0xfff7
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65526                   # 0xfff6
	.short	65526                   # 0xfff6
	.short	65526                   # 0xfff6
	.short	65526                   # 0xfff6
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	21                      # 0x15
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65525                   # 0xfff5
	.short	65525                   # 0xfff5
	.short	65525                   # 0xfff5
	.short	65525                   # 0xfff5
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	22                      # 0x16
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	23                      # 0x17
	.short	3                       # 0x3
	.short	65524                   # 0xfff4
	.short	65524                   # 0xfff4
	.short	65524                   # 0xfff4
	.short	65524                   # 0xfff4
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	24                      # 0x18
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	25                      # 0x19
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65523                   # 0xfff3
	.short	65523                   # 0xfff3
	.short	65523                   # 0xfff3
	.short	65523                   # 0xfff3
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	26                      # 0x1a
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65522                   # 0xfff2
	.short	65522                   # 0xfff2
	.short	65522                   # 0xfff2
	.short	65522                   # 0xfff2
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	27                      # 0x1b
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65521                   # 0xfff1
	.short	65521                   # 0xfff1
	.short	65521                   # 0xfff1
	.short	65521                   # 0xfff1
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	28                      # 0x1c
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65520                   # 0xfff0
	.short	65520                   # 0xfff0
	.short	65520                   # 0xfff0
	.short	65520                   # 0xfff0
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	29                      # 0x1d
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65519                   # 0xffef
	.short	65519                   # 0xffef
	.short	65519                   # 0xffef
	.short	65519                   # 0xffef
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	30                      # 0x1e
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65518                   # 0xffee
	.short	18                      # 0x12
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	65518                   # 0xffee
	.short	3                       # 0x3
	.short	65517                   # 0xffed
	.short	65517                   # 0xffed
	.short	65517                   # 0xffed
	.short	65517                   # 0xffed
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65516                   # 0xffec
	.short	65516                   # 0xffec
	.short	65516                   # 0xffec
	.short	65516                   # 0xffec
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65515                   # 0xffeb
	.short	65515                   # 0xffeb
	.short	65515                   # 0xffeb
	.short	65515                   # 0xffeb
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	31                      # 0x1f
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65514                   # 0xffea
	.short	65514                   # 0xffea
	.short	65514                   # 0xffea
	.short	65514                   # 0xffea
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	32                      # 0x20
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65513                   # 0xffe9
	.short	65513                   # 0xffe9
	.short	65513                   # 0xffe9
	.short	65513                   # 0xffe9
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	33                      # 0x21
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65512                   # 0xffe8
	.short	65512                   # 0xffe8
	.short	65512                   # 0xffe8
	.short	65512                   # 0xffe8
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	34                      # 0x22
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65511                   # 0xffe7
	.short	65511                   # 0xffe7
	.short	65511                   # 0xffe7
	.short	65511                   # 0xffe7
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	35                      # 0x23
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65510                   # 0xffe6
	.short	65510                   # 0xffe6
	.short	65510                   # 0xffe6
	.short	65510                   # 0xffe6
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	36                      # 0x24
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65509                   # 0xffe5
	.short	65509                   # 0xffe5
	.short	65509                   # 0xffe5
	.short	65509                   # 0xffe5
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	37                      # 0x25
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65508                   # 0xffe4
	.short	65508                   # 0xffe4
	.short	65508                   # 0xffe4
	.short	65508                   # 0xffe4
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65507                   # 0xffe3
	.short	65507                   # 0xffe3
	.short	65507                   # 0xffe3
	.short	65507                   # 0xffe3
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	38                      # 0x26
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65506                   # 0xffe2
	.short	65506                   # 0xffe2
	.short	65506                   # 0xffe2
	.short	65506                   # 0xffe2
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	39                      # 0x27
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65505                   # 0xffe1
	.short	65505                   # 0xffe1
	.short	65505                   # 0xffe1
	.short	65505                   # 0xffe1
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65504                   # 0xffe0
	.short	65504                   # 0xffe0
	.short	65504                   # 0xffe0
	.short	65504                   # 0xffe0
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	40                      # 0x28
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	41                      # 0x29
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65503                   # 0xffdf
	.short	65503                   # 0xffdf
	.short	65503                   # 0xffdf
	.short	65503                   # 0xffdf
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	42                      # 0x2a
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65502                   # 0xffde
	.short	65502                   # 0xffde
	.short	65502                   # 0xffde
	.short	65502                   # 0xffde
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	43                      # 0x2b
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65501                   # 0xffdd
	.short	65501                   # 0xffdd
	.short	65501                   # 0xffdd
	.short	65501                   # 0xffdd
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	44                      # 0x2c
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65500                   # 0xffdc
	.short	65500                   # 0xffdc
	.short	65500                   # 0xffdc
	.short	65500                   # 0xffdc
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	45                      # 0x2d
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65499                   # 0xffdb
	.short	65499                   # 0xffdb
	.short	65499                   # 0xffdb
	.short	65499                   # 0xffdb
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65498                   # 0xffda
	.short	65498                   # 0xffda
	.short	65498                   # 0xffda
	.short	65498                   # 0xffda
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	46                      # 0x2e
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65497                   # 0xffd9
	.short	65497                   # 0xffd9
	.short	65497                   # 0xffd9
	.short	65497                   # 0xffd9
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	47                      # 0x2f
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65496                   # 0xffd8
	.short	65496                   # 0xffd8
	.short	65496                   # 0xffd8
	.short	65496                   # 0xffd8
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	48                      # 0x30
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65495                   # 0xffd7
	.short	65495                   # 0xffd7
	.short	65495                   # 0xffd7
	.short	65495                   # 0xffd7
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	49                      # 0x31
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65494                   # 0xffd6
	.short	65494                   # 0xffd6
	.short	65494                   # 0xffd6
	.short	65494                   # 0xffd6
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	50                      # 0x32
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65493                   # 0xffd5
	.short	65493                   # 0xffd5
	.short	65493                   # 0xffd5
	.short	65493                   # 0xffd5
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	51                      # 0x33
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65492                   # 0xffd4
	.short	65492                   # 0xffd4
	.short	65492                   # 0xffd4
	.short	65492                   # 0xffd4
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	52                      # 0x34
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65491                   # 0xffd3
	.short	65491                   # 0xffd3
	.short	65491                   # 0xffd3
	.short	65491                   # 0xffd3
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	53                      # 0x35
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65490                   # 0xffd2
	.short	65490                   # 0xffd2
	.short	65490                   # 0xffd2
	.short	65490                   # 0xffd2
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	54                      # 0x36
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65489                   # 0xffd1
	.short	65489                   # 0xffd1
	.short	65489                   # 0xffd1
	.short	65489                   # 0xffd1
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65488                   # 0xffd0
	.short	65488                   # 0xffd0
	.short	65488                   # 0xffd0
	.short	65488                   # 0xffd0
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65487                   # 0xffcf
	.short	65487                   # 0xffcf
	.short	65487                   # 0xffcf
	.short	65487                   # 0xffcf
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65486                   # 0xffce
	.short	65486                   # 0xffce
	.short	65486                   # 0xffce
	.short	65486                   # 0xffce
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	55                      # 0x37
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65485                   # 0xffcd
	.short	65485                   # 0xffcd
	.short	65485                   # 0xffcd
	.short	65485                   # 0xffcd
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65484                   # 0xffcc
	.short	65484                   # 0xffcc
	.short	65484                   # 0xffcc
	.short	65484                   # 0xffcc
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	56                      # 0x38
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65483                   # 0xffcb
	.short	65483                   # 0xffcb
	.short	65483                   # 0xffcb
	.short	65483                   # 0xffcb
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	57                      # 0x39
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65482                   # 0xffca
	.short	65482                   # 0xffca
	.short	65482                   # 0xffca
	.short	65482                   # 0xffca
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65481                   # 0xffc9
	.short	65481                   # 0xffc9
	.short	65481                   # 0xffc9
	.short	65481                   # 0xffc9
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65480                   # 0xffc8
	.short	65480                   # 0xffc8
	.short	65480                   # 0xffc8
	.short	65480                   # 0xffc8
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65479                   # 0xffc7
	.short	65479                   # 0xffc7
	.short	65479                   # 0xffc7
	.short	65479                   # 0xffc7
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	58                      # 0x3a
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	59                      # 0x3b
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65478                   # 0xffc6
	.short	65478                   # 0xffc6
	.short	65478                   # 0xffc6
	.short	65478                   # 0xffc6
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	3                       # 0x3
	.short	65477                   # 0xffc5
	.short	65477                   # 0xffc5
	.short	65477                   # 0xffc5
	.short	65477                   # 0xffc5
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.short	20                      # 0x14
	.size	yy_nxt, 2880

	.type	yy_ec,@object           # @yy_ec
	.p2align	4
yy_ec:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	6                       # 0x6
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	11                      # 0xb
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	17                      # 0x11
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	20                      # 0x14
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	6                       # 0x6
	.long	23                      # 0x17
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.size	yy_ec, 1024

	.type	yy_accept,@object       # @yy_accept
	.p2align	4
yy_accept:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	20                      # 0x14
	.short	18                      # 0x12
	.short	15                      # 0xf
	.short	16                      # 0x10
	.short	17                      # 0x11
	.short	13                      # 0xd
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	15                      # 0xf
	.short	13                      # 0xd
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	10                      # 0xa
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	1                       # 0x1
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	9                       # 0x9
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	11                      # 0xb
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	14                      # 0xe
	.short	5                       # 0x5
	.short	14                      # 0xe
	.short	14                      # 0xe
	.short	12                      # 0xc
	.short	4                       # 0x4
	.short	6                       # 0x6
	.short	14                      # 0xe
	.short	7                       # 0x7
	.short	8                       # 0x8
	.size	yy_accept, 120

	.type	ia_text,@object         # @ia_text
	.comm	ia_text,8,8
	.type	ia_leng,@object         # @ia_leng
	.comm	ia_leng,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n Number too big in line %d.\n"
	.size	.L.str, 30

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n Illegal character '"
	.size	.L.str.1, 22

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%c"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\\x%x"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"' in line %d.\n"
	.size	.L.str.4, 15

	.type	yy_n_chars,@object      # @yy_n_chars
	.local	yy_n_chars
	.comm	yy_n_chars,4,4
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"fatal flex scanner internal error--no action found"
	.size	.L.str.5, 51

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"out of dynamic memory in yy_create_buffer()"
	.size	.L.str.6, 44

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"out of dynamic memory in yy_scan_buffer()"
	.size	.L.str.7, 42

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"out of dynamic memory in yy_scan_bytes()"
	.size	.L.str.8, 41

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"bad buffer in yy_scan_bytes()"
	.size	.L.str.9, 30

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"fatal flex scanner internal error--end of buffer missed"
	.size	.L.str.10, 56

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"fatal error - scanner input buffer overflow"
	.size	.L.str.11, 44

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"input in flex scanner failed"
	.size	.L.str.12, 29

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"%s\n"
	.size	.L.str.13, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
