	.text
	.file	"parse.bc"
	.globl	lame_usage
	.p2align	4, 0x90
	.type	lame_usage,@function
lame_usage:                             # @lame_usage
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	stderr(%rip), %rdi
	callq	lame_print_version
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$65, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	lame_usage, .Lfunc_end0-lame_usage
	.cfi_endproc

	.globl	lame_help
	.p2align	4, 0x90
	.type	lame_help,@function
lame_help:                              # @lame_help
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	stdout(%rip), %rdi
	callq	lame_print_version
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rdi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	movq	stdout(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$65, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$37, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$49, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$76, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$45, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.10, %edi
	movl	$18, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$64, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$74, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rcx
	movl	$.L.str.14, %edi
	movl	$75, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rcx
	movl	$.L.str.16, %edi
	movl	$80, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$72, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.18, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.19, %edi
	movl	$70, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.20, %edi
	movl	$66, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.21, %edi
	movl	$71, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.22, %edi
	movl	$64, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.23, %edi
	movl	$64, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.24, %edi
	movl	$49, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.25, %edi
	movl	$48, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.26, %edi
	movl	$44, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.27, %edi
	movl	$44, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.28, %edi
	movl	$76, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.29, %edi
	movl	$56, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.31, %edi
	movl	$71, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.32, %edi
	movl	$49, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.33, %edi
	movl	$53, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.34, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.35, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	movl	92(%r14), %edx
	movl	$.L.str.36, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rcx
	movl	$.L.str.37, %edi
	movl	$65, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.38, %edi
	movl	$67, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.39, %edi
	movl	$68, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.40, %edi
	movl	$55, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.41, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.42, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.43, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.44, %edi
	movl	$38, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.45, %edi
	movl	$41, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.46, %edi
	movl	$74, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.47, %edi
	movl	$57, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.48, %edi
	movl	$46, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.49, %edi
	movl	$63, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.50, %edi
	movl	$51, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.51, %edi
	movl	$61, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.52, %edi
	movl	$62, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.53, %edi
	movl	$74, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.54, %edi
	movl	$53, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.55, %edi
	movl	$68, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.56, %edi
	movl	$67, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.57, %edi
	movl	$66, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.58, %edi
	movl	$53, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rdi
	callq	display_bitrates
	xorl	%edi, %edi
	callq	exit
.Lfunc_end1:
	.size	lame_help, .Lfunc_end1-lame_help
	.cfi_endproc

	.globl	lame_presets_info
	.p2align	4, 0x90
	.type	lame_presets_info,@function
lame_presets_info:                      # @lame_presets_info
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 16
	movq	stdout(%rip), %rdi
	callq	lame_print_version
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.59, %edi
	movl	$48, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.60, %edi
	movl	$51, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.61, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.62, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.63, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.64, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.65, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.66, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.67, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.68, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.69, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.70, %edi
	movl	$58, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.71, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.72, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.73, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.74, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.75, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.76, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.66, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.67, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.77, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.69, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.78, %edi
	movl	$58, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.79, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.80, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.81, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.82, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.83, %edi
	movl	$45, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.84, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.85, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.86, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.87, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.88, %edi
	movl	$58, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.79, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.89, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.76, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.90, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.91, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.85, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.92, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.87, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.88, %edi
	movl	$58, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.79, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.93, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.94, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.90, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.91, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.95, %edi
	movl	$27, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.85, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.96, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.87, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.97, %edi
	movl	$58, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.98, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.99, %edi
	movl	$27, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.95, %edi
	movl	$27, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.100, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.101, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.102, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.103, %edi
	movl	$58, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.104, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.105, %edi
	movl	$27, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.95, %edi
	movl	$27, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.100, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.106, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.107, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.108, %edi
	movl	$58, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.109, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	xorl	%edi, %edi
	callq	exit
.Lfunc_end2:
	.size	lame_presets_info, .Lfunc_end2-lame_presets_info
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4652007308841189376     # double 1000
.LCPI3_1:
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_2:
	.long	0                       # 0x0
	.long	112                     # 0x70
	.long	320                     # 0x140
	.long	4294967295              # 0xffffffff
.LCPI3_3:
	.long	2                       # 0x2
	.long	80                      # 0x50
	.long	256                     # 0x100
	.long	4294967295              # 0xffffffff
.LCPI3_4:
	.long	3                       # 0x3
	.long	32                      # 0x20
	.long	224                     # 0xe0
	.long	20000                   # 0x4e20
.LCPI3_5:
	.long	4                       # 0x4
	.long	32                      # 0x20
	.long	192                     # 0xc0
	.long	17000                   # 0x4268
.LCPI3_6:
	.long	4                       # 0x4
	.long	32                      # 0x20
	.long	192                     # 0xc0
	.long	15000                   # 0x3a98
.LCPI3_7:
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	96                      # 0x60
	.long	11000                   # 0x2af8
.LCPI3_8:
	.long	5                       # 0x5
	.long	8                       # 0x8
	.long	56                      # 0x38
	.long	3700                    # 0xe74
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_9:
	.long	1065353216              # float 1
.LCPI3_10:
	.long	0                       # float 0
	.text
	.globl	lame_parse_args
	.p2align	4, 0x90
	.type	lame_parse_args,@function
lame_parse_args:                        # @lame_parse_args
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 112
.Lcfi15:
	.cfi_offset %rbx, -56
.Lcfi16:
	.cfi_offset %r12, -48
.Lcfi17:
	.cfi_offset %r13, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movl	%esi, %r13d
	movq	(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movb	$0, inPath(%rip)
	movl	$inPath, %eax
	movb	$0, outPath(%rip)
	movl	$outPath, %ecx
	movd	%rcx, %xmm0
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movdqu	%xmm1, 128(%rdi)
	movl	$id3tag, %edi
	callq	id3_inittag
	movl	$0, id3tag(%rip)
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
	cmpl	$2, %r13d
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	movl	$0, %ebp
	jl	.LBB3_169
# BB#1:                                 # %.lr.ph1301
	movl	$1, %r15d
	xorl	%ebx, %ebx
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	jmp	.LBB3_27
	.p2align	4, 0x90
.LBB3_2:                                # %.loopexit1244
                                        #   in Loop: Header=BB3_27 Depth=1
	testl	%ebp, %ebp
	je	.LBB3_27
	jmp	.LBB3_169
.LBB3_149:                              #   in Loop: Header=BB3_27 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.154, %esi
	xorl	%eax, %eax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	callq	fprintf
	movl	$1, %ebp
	jmp	.LBB3_40
.LBB3_150:                              #   in Loop: Header=BB3_27 Depth=1
	movl	$1, 148(%rbx)
	xorl	%ebx, %ebx
	jmp	.LBB3_166
.LBB3_151:                              #   in Loop: Header=BB3_27 Depth=1
	movl	$1, 72(%rbx)
	movl	$0, 196(%rbx)
	xorl	%ebx, %ebx
	jmp	.LBB3_166
.LBB3_152:                              #   in Loop: Header=BB3_27 Depth=1
	movl	$1, 144(%rbx)
.LBB3_153:                              #   in Loop: Header=BB3_27 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB3_166
.LBB3_154:                              #   in Loop: Header=BB3_27 Depth=1
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	strtod
	mulsd	.LCPI3_0(%rip), %xmm0
	addsd	.LCPI3_1(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, 104(%rbx)
	movl	$1, %ebx
	testl	%eax, %eax
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	8(%rsp), %r13d          # 4-byte Reload
	jg	.LBB3_167
	jmp	.LBB3_192
.LBB3_155:                              #   in Loop: Header=BB3_27 Depth=1
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	strtod
	mulsd	.LCPI3_0(%rip), %xmm0
	addsd	.LCPI3_1(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, 112(%rbx)
	movl	$1, %ebx
	testl	%eax, %eax
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	8(%rsp), %r13d          # 4-byte Reload
	jns	.LBB3_167
	jmp	.LBB3_193
.LBB3_156:                              #   in Loop: Header=BB3_27 Depth=1
	movl	$1, id3tag(%rip)
	movl	$id3tag+8, %edi
	jmp	.LBB3_163
.LBB3_157:                              #   in Loop: Header=BB3_27 Depth=1
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	strtod
	mulsd	.LCPI3_0(%rip), %xmm0
	addsd	.LCPI3_1(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, 108(%rbx)
	movl	$1, %ebx
	testl	%eax, %eax
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	8(%rsp), %r13d          # 4-byte Reload
	jg	.LBB3_167
	jmp	.LBB3_194
.LBB3_158:                              #   in Loop: Header=BB3_27 Depth=1
	movl	$1, id3tag(%rip)
	movl	$id3tag+39, %edi
	jmp	.LBB3_163
.LBB3_159:                              #   in Loop: Header=BB3_27 Depth=1
	movl	$1, id3tag(%rip)
	movl	$id3tag+70, %edi
	jmp	.LBB3_163
.LBB3_160:                              #   in Loop: Header=BB3_27 Depth=1
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	strtod
	mulsd	.LCPI3_0(%rip), %xmm0
	addsd	.LCPI3_1(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, 116(%rbx)
	movl	$1, %ebx
	testl	%eax, %eax
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	8(%rsp), %r13d          # 4-byte Reload
	jns	.LBB3_167
	jmp	.LBB3_195
.LBB3_161:                              #   in Loop: Header=BB3_27 Depth=1
	movl	$1, id3tag(%rip)
	movl	$id3tag+101, %edi
	movl	$4, %edx
	jmp	.LBB3_164
.LBB3_162:                              #   in Loop: Header=BB3_27 Depth=1
	movl	$1, id3tag(%rip)
	movl	$id3tag+106, %edi
.LBB3_163:                              #   in Loop: Header=BB3_27 Depth=1
	movl	$30, %edx
.LBB3_164:                              #   in Loop: Header=BB3_27 Depth=1
	movq	%r12, %rsi
	callq	strncpy
.LBB3_165:                              #   in Loop: Header=BB3_27 Depth=1
	movl	$1, %ebx
	jmp	.LBB3_166
.LBB3_3:                                #   in Loop: Header=BB3_27 Depth=1
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	strtod
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 152(%rbx)
	movl	$1, %ebx
	pxor	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	8(%rsp), %r13d          # 4-byte Reload
	jb	.LBB3_167
	jmp	.LBB3_196
.LBB3_4:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$1, id3tag(%rip)
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r12, %rdi
	callq	strtol
	testl	%eax, %eax
	movl	$1, %ebx
	cmovlel	%ebx, %eax
	cmpl	$99, %eax
	movl	8(%rsp), %r13d          # 4-byte Reload
	jl	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_27 Depth=1
	movb	$99, %al
.LBB3_6:                                #   in Loop: Header=BB3_27 Depth=1
	movb	%al, id3tag+266(%rip)
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB3_167
.LBB3_7:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$10, %edx
	movq	%r12, %rdi
	leaq	48(%rsp), %rsi
	callq	strtol
	movq	%rax, %rbp
	cmpq	48(%rsp), %r12
	je	.LBB3_13
# BB#8:                                 # %..loopexit_crit_edge
                                        #   in Loop: Header=BB3_27 Depth=1
	movl	genre_last(%rip), %ebx
	movl	8(%rsp), %r13d          # 4-byte Reload
.LBB3_9:                                # %.loopexit
                                        #   in Loop: Header=BB3_27 Depth=1
	cmpl	%ebx, %ebp
	jg	.LBB3_21
.LBB3_10:                               #   in Loop: Header=BB3_27 Depth=1
	movb	%bpl, 15(%rsp)
	movl	$1, id3tag(%rip)
	movl	$id3tag+265, %edi
	movl	$1, %edx
	leaq	15(%rsp), %rsi
	callq	strncpy
	jmp	.LBB3_25
.LBB3_11:                               #   in Loop: Header=BB3_27 Depth=1
	movl	$16, 48(%rbx)
	movl	$260, 108(%rbx)         # imm = 0x104
	movl	$40, 116(%rbx)
	movl	$300, 112(%rbx)         # imm = 0x12C
	movapd	.LCPI3_8(%rip), %xmm0   # xmm0 = [5,8,56,3700]
	movupd	%xmm0, 92(%rbx)
	movl	$1, 160(%rbx)
	movl	$16000, 16(%rbx)        # imm = 0x3E80
	movl	$3, 36(%rbx)
	movl	$1, 40(%rbx)
	movl	$5, 28(%rbx)
	jmp	.LBB3_165
.LBB3_12:                               #   in Loop: Header=BB3_27 Depth=1
	movl	$56, 48(%rbx)
	movl	$100, 108(%rbx)
	movl	$20, 116(%rbx)
	movl	$2000, 112(%rbx)        # imm = 0x7D0
	movapd	.LCPI3_7(%rip), %xmm0   # xmm0 = [4,8,96,11000]
	movupd	%xmm0, 92(%rbx)
	movl	$1, 160(%rbx)
	movl	$3, 36(%rbx)
	movl	$1, 40(%rbx)
	movl	$24000, 16(%rbx)        # imm = 0x5DC0
	movl	$5, 28(%rbx)
	jmp	.LBB3_25
.LBB3_13:                               # %.preheader
                                        #   in Loop: Header=BB3_27 Depth=1
	movslq	genre_last(%rip), %rbx
	testq	%rbx, %rbx
	movl	8(%rsp), %r13d          # 4-byte Reload
	js	.LBB3_20
# BB#14:                                # %.lr.ph1293
                                        #   in Loop: Header=BB3_27 Depth=1
	xorl	%ebp, %ebp
.LBB3_15:                               #   Parent Loop BB3_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	genre_list(,%rbp,8), %rdi
	movq	%r12, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_9
# BB#16:                                #   in Loop: Header=BB3_15 Depth=2
	cmpq	%rbx, %rbp
	leaq	1(%rbp), %rbp
	jl	.LBB3_15
	jmp	.LBB3_9
.LBB3_17:                               #   in Loop: Header=BB3_27 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	$128, 48(%rax)
	movl	$15, 108(%rax)
	movl	$15, 116(%rax)
	movl	$2000, 112(%rax)        # imm = 0x7D0
	movapd	.LCPI3_5(%rip), %xmm0   # xmm0 = [4,32,192,17000]
	jmp	.LBB3_24
.LBB3_18:                               #   in Loop: Header=BB3_27 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	$160, 48(%rax)
	movl	$15, 108(%rax)
	movl	$15, 116(%rax)
	movl	$3000, 112(%rax)        # imm = 0xBB8
	movapd	.LCPI3_4(%rip), %xmm0   # xmm0 = [3,32,224,20000]
	movupd	%xmm0, 92(%rax)
	movl	$1, 36(%rax)
.LBB3_19:                               #   in Loop: Header=BB3_27 Depth=1
	movl	$1, 40(%rax)
	movl	$2, 28(%rax)
	jmp	.LBB3_25
.LBB3_20:                               #   in Loop: Header=BB3_27 Depth=1
	xorl	%ebp, %ebp
	cmpl	%ebx, %ebp
	jle	.LBB3_10
.LBB3_21:                               #   in Loop: Header=BB3_27 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.127, %esi
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	fprintf
	movl	$255, %ebp
	jmp	.LBB3_10
.LBB3_22:                               #   in Loop: Header=BB3_27 Depth=1
	movq	stderr(%rip), %rdi
	xorl	%ebx, %ebx
	movl	$.L.str.148, %esi
	xorl	%eax, %eax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r13, %rcx
	callq	fprintf
	jmp	.LBB3_166
.LBB3_23:                               #   in Loop: Header=BB3_27 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	$96, 48(%rax)
	movl	$30, 108(%rax)
	movl	$0, 116(%rax)
	movl	$0, 112(%rax)
	movapd	.LCPI3_6(%rip), %xmm0   # xmm0 = [4,32,192,15000]
.LBB3_24:                               #   in Loop: Header=BB3_27 Depth=1
	movupd	%xmm0, 92(%rax)
	movl	$1, 36(%rax)
	movl	$1, 40(%rax)
	movl	$5, 28(%rax)
.LBB3_25:                               #   in Loop: Header=BB3_27 Depth=1
	movl	$1, %ebx
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB3_167
.LBB3_26:                               #   in Loop: Header=BB3_27 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	$192, 48(%rax)
	movl	$-1, 108(%rax)
	movapd	.LCPI3_3(%rip), %xmm0   # xmm0 = [2,80,256,4294967295]
	jmp	.LBB3_88
	.p2align	4, 0x90
.LBB3_27:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_90 Depth 2
                                        #     Child Loop BB3_15 Depth 2
	movslq	%r15d, %rax
	movq	(%r12,%rax,8), %r14
	leaq	1(%r14), %rbp
	movq	%rbp, 48(%rsp)
	cmpb	$45, (%r14)
	jne	.LBB3_30
# BB#28:                                #   in Loop: Header=BB3_27 Depth=1
	addl	$2, %ebx
	cmpl	%r13d, %ebx
	jge	.LBB3_33
# BB#29:                                #   in Loop: Header=BB3_27 Depth=1
	movslq	%ebx, %rax
	movq	(%r12,%rax,8), %r12
	jmp	.LBB3_34
	.p2align	4, 0x90
.LBB3_30:                               #   in Loop: Header=BB3_27 Depth=1
	cmpb	$0, inPath(%rip)
	je	.LBB3_38
# BB#31:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$0, outPath(%rip)
	jne	.LBB3_149
# BB#32:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$outPath, %edi
	jmp	.LBB3_39
	.p2align	4, 0x90
.LBB3_33:                               #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.110, %r12d
.LBB3_34:                               #   in Loop: Header=BB3_27 Depth=1
	movq	32(%rsp), %rbx          # 8-byte Reload
	movb	(%rbp), %al
	testb	%al, %al
	jne	.LBB3_44
# BB#35:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$0, inPath(%rip)
	je	.LBB3_41
# BB#36:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$0, outPath(%rip)
	jne	.LBB3_43
# BB#37:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$outPath, %edi
	jmp	.LBB3_42
.LBB3_38:                               #   in Loop: Header=BB3_27 Depth=1
	movl	$inPath, %edi
.LBB3_39:                               # %.loopexit1244
                                        #   in Loop: Header=BB3_27 Depth=1
	movl	$300, %edx              # imm = 0x12C
	movq	%r14, %rsi
	callq	strncpy
	xorl	%ebp, %ebp
.LBB3_40:                               # %.loopexit1244
                                        #   in Loop: Header=BB3_27 Depth=1
	movl	%r15d, %ebx
	jmp	.LBB3_168
.LBB3_41:                               #   in Loop: Header=BB3_27 Depth=1
	movl	$inPath, %edi
.LBB3_42:                               # %thread-pre-split1317
                                        #   in Loop: Header=BB3_27 Depth=1
	movl	$300, %edx              # imm = 0x12C
	movq	%r14, %rsi
	callq	strncpy
.LBB3_43:                               # %thread-pre-split1317
                                        #   in Loop: Header=BB3_27 Depth=1
	movb	(%rbp), %al
.LBB3_44:                               #   in Loop: Header=BB3_27 Depth=1
	leaq	2(%r14), %r13
	cmpb	$45, %al
	movq	%r13, 48(%rsp)
	jne	.LBB3_89
# BB#45:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.111, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_145
# BB#46:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.113, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_146
# BB#47:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.114, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_147
# BB#48:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.115, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_148
# BB#49:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.116, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_150
# BB#50:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.117, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_151
# BB#51:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.118, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_152
# BB#52:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.119, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_153
# BB#53:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$116, (%r13)
	jne	.LBB3_68
# BB#54:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$116, 3(%r14)
	jne	.LBB3_56
# BB#55:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$0, 4(%r14)
	je	.LBB3_156
.LBB3_56:                               #   in Loop: Header=BB3_27 Depth=1
	cmpb	$97, 3(%r14)
	jne	.LBB3_58
# BB#57:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$0, 4(%r14)
	je	.LBB3_158
.LBB3_58:                               #   in Loop: Header=BB3_27 Depth=1
	cmpb	$108, 3(%r14)
	jne	.LBB3_60
# BB#59:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$0, 4(%r14)
	je	.LBB3_159
.LBB3_60:                               #   in Loop: Header=BB3_27 Depth=1
	cmpb	$121, 3(%r14)
	jne	.LBB3_62
# BB#61:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$0, 4(%r14)
	je	.LBB3_161
.LBB3_62:                               #   in Loop: Header=BB3_27 Depth=1
	cmpb	$99, 3(%r14)
	jne	.LBB3_64
# BB#63:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$0, 4(%r14)
	je	.LBB3_162
.LBB3_64:                               #   in Loop: Header=BB3_27 Depth=1
	cmpb	$110, 3(%r14)
	jne	.LBB3_66
# BB#65:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$0, 4(%r14)
	je	.LBB3_4
.LBB3_66:                               #   in Loop: Header=BB3_27 Depth=1
	cmpb	$103, 3(%r14)
	jne	.LBB3_68
# BB#67:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$0, 4(%r14)
	je	.LBB3_7
.LBB3_68:                               # %.thread1335
                                        #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.128, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_154
# BB#69:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.130, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_155
# BB#70:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.132, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_157
# BB#71:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.134, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_160
# BB#72:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.136, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_3
# BB#73:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.138, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_188
# BB#74:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.139, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_188
# BB#75:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.140, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB3_22
# BB#76:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.141, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_11
# BB#77:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.114, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	movl	8(%rsp), %r13d          # 4-byte Reload
	je	.LBB3_12
# BB#78:                                #   in Loop: Header=BB3_27 Depth=1
	movb	(%r12), %bl
	cmpb	$102, %bl
	jne	.LBB3_81
# BB#79:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$109, 1(%r12)
	jne	.LBB3_81
# BB#80:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$0, 2(%r12)
	je	.LBB3_23
.LBB3_81:                               # %.thread1337
                                        #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.143, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_17
# BB#82:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.144, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_18
# BB#83:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$99, %bl
	jne	.LBB3_86
# BB#84:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$100, 1(%r12)
	jne	.LBB3_86
# BB#85:                                #   in Loop: Header=BB3_27 Depth=1
	cmpb	$0, 2(%r12)
	je	.LBB3_26
.LBB3_86:                               # %.thread1339
                                        #   in Loop: Header=BB3_27 Depth=1
	movl	$.L.str.146, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	movq	32(%rsp), %rax          # 8-byte Reload
	jne	.LBB3_197
# BB#87:                                #   in Loop: Header=BB3_27 Depth=1
	movl	$256, 48(%rax)          # imm = 0x100
	movl	$-1, 108(%rax)
	movapd	.LCPI3_2(%rip), %xmm0   # xmm0 = [0,112,320,4294967295]
.LBB3_88:                               #   in Loop: Header=BB3_27 Depth=1
	movupd	%xmm0, 92(%rax)
	movl	$0, 36(%rax)
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_89:                               # %.preheader1243
                                        #   in Loop: Header=BB3_27 Depth=1
	movb	%al, 15(%rsp)
	xorl	%ebp, %ebp
	testb	%al, %al
	je	.LBB3_144
	.p2align	4, 0x90
.LBB3_90:                               # %.lr.ph
                                        #   Parent Loop BB3_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, (%r13)
	movq	%r13, %r14
	cmoveq	%r12, %r14
	movsbl	%al, %ecx
	leal	-63(%rcx), %eax
	cmpl	$57, %eax
	ja	.LBB3_108
# BB#91:                                # %.lr.ph
                                        #   in Loop: Header=BB3_90 Depth=2
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_92:                               #   in Loop: Header=BB3_90 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	strtol
	movl	%eax, 100(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_93:                               #   in Loop: Header=BB3_90 Depth=2
	movl	$0, 24(%rbx)
	jmp	.LBB3_142
.LBB3_94:                               #   in Loop: Header=BB3_90 Depth=2
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	strtod
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI3_0(%rip), %xmm0
	addsd	.LCPI3_1(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, 12(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_95:                               #   in Loop: Header=BB3_90 Depth=2
	movl	$1, 60(%rbx)
	jmp	.LBB3_142
.LBB3_96:                               #   in Loop: Header=BB3_90 Depth=2
	movl	$1, 156(%rbx)
	jmp	.LBB3_142
.LBB3_97:                               #   in Loop: Header=BB3_90 Depth=2
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	strtol
	testl	%eax, %eax
	cmovnsl	%eax, %ebx
	cmpl	$10, %ebx
	movl	$9, %eax
	cmovll	%ebx, %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movq	32(%rsp), %rbx          # 8-byte Reload
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_98:                               #   in Loop: Header=BB3_90 Depth=2
	movabsq	$4294967299, %rax       # imm = 0x100000003
	movq	%rax, 36(%rbx)
	movl	$1, 40(%rsp)            # 4-byte Folded Spill
	jmp	.LBB3_142
.LBB3_99:                               #   in Loop: Header=BB3_90 Depth=2
	movq	stderr(%rip), %rdi
	movl	$.L.str.151, %esi
	xorl	%eax, %eax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	callq	fprintf
	jmp	.LBB3_142
.LBB3_100:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$1, 80(%rbx)
	jmp	.LBB3_142
.LBB3_101:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$1, 84(%rbx)
	jmp	.LBB3_142
.LBB3_102:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$1, 88(%rbx)
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	strtol
	testl	%eax, %eax
	cmovnsl	%eax, %ebx
	cmpl	$10, %ebx
	movl	$9, %eax
	cmovll	%ebx, %eax
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	%eax, 92(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_103:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$1, 52(%rbx)
	jmp	.LBB3_142
.LBB3_104:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$0, 76(%rbx)
	movzbl	(%r14), %eax
	addb	$-48, %al
	cmpb	$6, %al
	ja	.LBB3_136
# BB#105:                               #   in Loop: Header=BB3_90 Depth=2
	movzbl	%al, %eax
	jmpq	*.LJTI3_1(,%rax,8)
.LBB3_106:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$0, 76(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_107:                              #   in Loop: Header=BB3_90 Depth=2
	movq	$-1, 104(%rbx)
	jmp	.LBB3_142
.LBB3_108:                              #   in Loop: Header=BB3_90 Depth=2
	movq	stderr(%rip), %rdi
	movl	$.L.str.153, %esi
	xorl	%eax, %eax
	movq	24(%rsp), %rdx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$1, %ebp
	jmp	.LBB3_142
.LBB3_109:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$2, 28(%rbx)
	jmp	.LBB3_142
.LBB3_110:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$1, 40(%rbx)
	movzbl	(%r14), %eax
	addb	$-100, %al
	cmpb	$15, %al
	ja	.LBB3_138
# BB#111:                               #   in Loop: Header=BB3_90 Depth=2
	movzbl	%al, %eax
	jmpq	*.LJTI3_2(,%rax,8)
.LBB3_112:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$2, 36(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_113:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$4, 120(%rbx)
	jmp	.LBB3_142
.LBB3_114:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$9, 28(%rbx)
	jmp	.LBB3_142
.LBB3_115:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$0, 56(%rbx)
	jmp	.LBB3_142
.LBB3_116:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$1, 32(%rbx)
	jmp	.LBB3_142
.LBB3_117:                              #   in Loop: Header=BB3_90 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	strtol
	movl	%eax, 48(%rbx)
	movl	%eax, 96(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_118:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$1, 88(%rbx)
	jmp	.LBB3_142
.LBB3_119:                              #   in Loop: Header=BB3_90 Depth=2
	movzbl	(%r14), %eax
	cmpb	$53, %al
	je	.LBB3_124
# BB#120:                               #   in Loop: Header=BB3_90 Depth=2
	cmpb	$99, %al
	je	.LBB3_125
# BB#121:                               #   in Loop: Header=BB3_90 Depth=2
	cmpb	$110, %al
	jne	.LBB3_137
# BB#122:                               #   in Loop: Header=BB3_90 Depth=2
	movl	$0, 164(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_123:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$1, 124(%rbx)
	jmp	.LBB3_142
.LBB3_124:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$1, 164(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_125:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$3, 164(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_126:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$1, 36(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_127:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$3, 36(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_128:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$0, 36(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_129:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$1, 36(%rbx)
	movl	$1, 44(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_130:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$4, 76(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_131:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$2, 76(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_132:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$3, 76(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_133:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$1, 76(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_134:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$5, 76(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_135:                              #   in Loop: Header=BB3_90 Depth=2
	movl	$6, 76(%rbx)
	cmpq	%r13, %r14
	jne	.LBB3_140
	jmp	.LBB3_141
.LBB3_136:                              #   in Loop: Header=BB3_90 Depth=2
	movq	stderr(%rip), %rdi
	movl	$.L.str.150, %esi
	jmp	.LBB3_139
.LBB3_137:                              #   in Loop: Header=BB3_90 Depth=2
	movq	stderr(%rip), %rdi
	movl	$.L.str.152, %esi
	jmp	.LBB3_139
.LBB3_138:                              #   in Loop: Header=BB3_90 Depth=2
	movq	stderr(%rip), %rdi
	movl	$.L.str.149, %esi
.LBB3_139:                              #   in Loop: Header=BB3_90 Depth=2
	xorl	%eax, %eax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	callq	fprintf
	movl	$1, %ebp
	cmpq	%r13, %r14
	je	.LBB3_141
.LBB3_140:                              #   in Loop: Header=BB3_90 Depth=2
	incl	%r15d
	jmp	.LBB3_142
.LBB3_141:                              #   in Loop: Header=BB3_90 Depth=2
	movq	$.L.str.110, 48(%rsp)
	movl	$.L.str.110, %r13d
	.p2align	4, 0x90
.LBB3_142:                              # %.backedge
                                        #   in Loop: Header=BB3_90 Depth=2
	leaq	1(%r13), %rcx
	movq	%rcx, 48(%rsp)
	movzbl	(%r13), %eax
	testb	%al, %al
	movq	%rcx, %r13
	jne	.LBB3_90
# BB#143:                               # %..loopexit1244_crit_edge
                                        #   in Loop: Header=BB3_27 Depth=1
	movb	$0, 15(%rsp)
.LBB3_144:                              # %.loopexit1244
                                        #   in Loop: Header=BB3_27 Depth=1
	movl	%r15d, %ebx
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	8(%rsp), %r13d          # 4-byte Reload
	jmp	.LBB3_168
.LBB3_145:                              #   in Loop: Header=BB3_27 Depth=1
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	strtod
	cvtsd2ss	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm0, %xmm1
	mulsd	.LCPI3_0(%rip), %xmm1
	addsd	.LCPI3_1(%rip), %xmm1
	cvttsd2si	%xmm1, %eax
	movl	%eax, 16(%rbx)
	movl	$1, %ebx
	movd	.LCPI3_9(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	8(%rsp), %r13d          # 4-byte Reload
	jbe	.LBB3_167
	jmp	.LBB3_189
.LBB3_146:                              #   in Loop: Header=BB3_27 Depth=1
	movl	$3, 120(%rbx)
	xorl	%ebx, %ebx
	jmp	.LBB3_166
.LBB3_147:                              #   in Loop: Header=BB3_27 Depth=1
	movabsq	$51539607552160, %rax   # imm = 0x2EE0000000A0
	movq	%rax, 100(%rbx)
.LBB3_148:                              #   in Loop: Header=BB3_27 Depth=1
	movl	$1, 160(%rbx)
	xorl	%ebx, %ebx
.LBB3_166:                              #   in Loop: Header=BB3_27 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	8(%rsp), %r13d          # 4-byte Reload
.LBB3_167:                              #   in Loop: Header=BB3_27 Depth=1
	addl	%r15d, %ebx
	xorl	%ebp, %ebp
.LBB3_168:                              # %.loopexit1244
                                        #   in Loop: Header=BB3_27 Depth=1
	leal	1(%rbx), %r15d
	cmpl	%r13d, %r15d
	jl	.LBB3_2
.LBB3_169:                              # %._crit_edge
	testl	%ebp, %ebp
	jne	.LBB3_191
# BB#170:                               # %._crit_edge
	movb	inPath(%rip), %al
	testb	%al, %al
	je	.LBB3_191
# BB#171:
	cmpb	$45, %al
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	44(%rsp), %ebp          # 4-byte Reload
	jne	.LBB3_173
# BB#172:
	movl	$1, 32(%rbx)
.LBB3_173:
	movb	outPath(%rip), %cl
	testb	%cl, %cl
	jne	.LBB3_177
# BB#174:
	cmpb	$45, %al
	jne	.LBB3_176
# BB#175:                               # %thread-pre-split.thread
	movw	$45, outPath(%rip)
	jmp	.LBB3_178
.LBB3_176:
	movl	$outPath, %edi
	movl	$inPath, %esi
	movl	$296, %edx              # imm = 0x128
	callq	strncpy
	movl	$outPath, %edi
	callq	strlen
	movb	$0, outPath+4(%rax)
	movl	$863005998, outPath(%rax) # imm = 0x33706D2E
	movb	outPath(%rip), %cl
.LBB3_177:                              # %thread-pre-split
	cmpb	$45, %cl
	jne	.LBB3_179
.LBB3_178:
	movl	$0, 24(%rbx)
	cmpl	$0, id3tag(%rip)
	jne	.LBB3_187
.LBB3_179:
	cmpl	$3, 120(%rbx)
	je	.LBB3_182
.LBB3_180:
	movl	$inPath, %edi
	callq	strlen
	leaq	inPath-4(%rax), %rdi
	movl	$.L.str.156, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB3_182
# BB#181:
	movl	$3, 120(%rbx)
.LBB3_182:
	movl	$2, %eax
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_184
# BB#183:
	xorl	%eax, %eax
	cmpl	$3, 36(%rbx)
	setne	%al
	incl	%eax
.LBB3_184:
	movl	%eax, 8(%rbx)
	testl	%ebp, %ebp
	je	.LBB3_186
# BB#185:
	movl	%ebp, 28(%rbx)
.LBB3_186:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_187:
	movl	$0, id3tag(%rip)
	movq	stderr(%rip), %rcx
	movl	$.L.str.157, %edi
	movl	$54, %esi
	movl	$1, %edx
	callq	fwrite
	cmpl	$3, 120(%rbx)
	jne	.LBB3_180
	jmp	.LBB3_182
.LBB3_188:
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	lame_help
.LBB3_189:
	movq	stderr(%rip), %rcx
	movl	$.L.str.112, %edi
	movl	$42, %esi
.LBB3_190:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB3_191:
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	lame_usage
.LBB3_192:
	movq	stderr(%rip), %rcx
	movl	$.L.str.129, %edi
	movl	$60, %esi
	jmp	.LBB3_190
.LBB3_193:
	movq	stderr(%rip), %rcx
	movl	$.L.str.131, %edi
	movl	$68, %esi
	jmp	.LBB3_190
.LBB3_194:
	movq	stderr(%rip), %rcx
	movl	$.L.str.133, %edi
	movl	$62, %esi
	jmp	.LBB3_190
.LBB3_195:
	movq	stderr(%rip), %rcx
	movl	$.L.str.135, %edi
	movl	$70, %esi
	jmp	.LBB3_190
.LBB3_196:
	movq	stderr(%rip), %rcx
	movl	$.L.str.137, %edi
	movl	$28, %esi
	jmp	.LBB3_190
.LBB3_197:
	movl	$.L.str.138, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB3_199
# BB#198:
	callq	lame_presets_info
.LBB3_199:
	movq	stderr(%rip), %rdi
	movl	$.L.str.147, %esi
	xorl	%eax, %eax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r12, %rcx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end3:
	.size	lame_parse_args, .Lfunc_end3-lame_parse_args
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_188
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_92
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_116
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_102
	.quad	.LBB3_108
	.quad	.LBB3_104
	.quad	.LBB3_100
	.quad	.LBB3_101
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_98
	.quad	.LBB3_117
	.quad	.LBB3_103
	.quad	.LBB3_96
	.quad	.LBB3_119
	.quad	.LBB3_114
	.quad	.LBB3_99
	.quad	.LBB3_109
	.quad	.LBB3_108
	.quad	.LBB3_108
	.quad	.LBB3_107
	.quad	.LBB3_108
	.quad	.LBB3_110
	.quad	.LBB3_108
	.quad	.LBB3_115
	.quad	.LBB3_95
	.quad	.LBB3_97
	.quad	.LBB3_113
	.quad	.LBB3_94
	.quad	.LBB3_93
	.quad	.LBB3_108
	.quad	.LBB3_118
	.quad	.LBB3_108
	.quad	.LBB3_123
.LJTI3_1:
	.quad	.LBB3_106
	.quad	.LBB3_133
	.quad	.LBB3_131
	.quad	.LBB3_132
	.quad	.LBB3_130
	.quad	.LBB3_134
	.quad	.LBB3_135
.LJTI3_2:
	.quad	.LBB3_112
	.quad	.LBB3_138
	.quad	.LBB3_129
	.quad	.LBB3_138
	.quad	.LBB3_138
	.quad	.LBB3_138
	.quad	.LBB3_126
	.quad	.LBB3_138
	.quad	.LBB3_138
	.quad	.LBB3_127
	.quad	.LBB3_138
	.quad	.LBB3_138
	.quad	.LBB3_138
	.quad	.LBB3_138
	.quad	.LBB3_138
	.quad	.LBB3_128

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"USAGE   :  %s [options] <infile> [outfile]\n"
	.size	.L.str.1, 44

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n<infile> and/or <outfile> can be \"-\", which means stdin/stdout.\n"
	.size	.L.str.2, 66

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Try \"%s --help\" for more information\n"
	.size	.L.str.3, 38

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"OPTIONS :\n"
	.size	.L.str.4, 11

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"  Input options:\n"
	.size	.L.str.5, 18

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"    -r              input is raw pcm\n"
	.size	.L.str.6, 38

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"    -x              force byte-swapping of input\n"
	.size	.L.str.7, 50

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"    -s sfreq        sampling frequency of input file(kHz) - default 44.1kHz\n"
	.size	.L.str.8, 77

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"    --mp3input      input file is a MP3 file\n"
	.size	.L.str.9, 46

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"  Filter options:\n"
	.size	.L.str.10, 19

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"    -k              keep ALL frequencies (disables all filters)\n"
	.size	.L.str.11, 65

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"  --lowpass freq         frequency(kHz), lowpass filter cutoff above freq\n"
	.size	.L.str.12, 75

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"  --lowpass-width freq   frequency(kHz) - default 15%% of lowpass freq\n"
	.size	.L.str.13, 72

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"  --highpass freq        frequency(kHz), highpass filter cutoff below freq\n"
	.size	.L.str.14, 76

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"  --highpass-width freq  frequency(kHz) - default 15%% of highpass freq\n"
	.size	.L.str.15, 73

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"  --resample sfreq  sampling frequency of output file(kHz)- default=input sfreq\n"
	.size	.L.str.16, 81

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"  --cwlimit freq    compute tonality up to freq (in kHz) default 8.8717\n"
	.size	.L.str.17, 73

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"  Operational options:\n"
	.size	.L.str.18, 24

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"    -m mode         (s)tereo, (j)oint, (f)orce or (m)ono  (default j)\n"
	.size	.L.str.19, 71

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"                    force = force ms_stereo on all frames. Faster\n"
	.size	.L.str.20, 67

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"    -a              downmix from stereo to mono file for mono encoding\n"
	.size	.L.str.21, 72

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"    -d              allow channels to have different blocktypes\n"
	.size	.L.str.22, 65

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"    -S              don't print progress report, VBR histograms\n"
	.size	.L.str.23, 65

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"    --athonly       only use the ATH for masking\n"
	.size	.L.str.24, 50

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"    --noath         disable the ATH for masking\n"
	.size	.L.str.25, 49

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"    --noshort       do not use short blocks\n"
	.size	.L.str.26, 45

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"    --voice         experimental voice mode\n"
	.size	.L.str.27, 45

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"    --preset type   type must be phone, voice, fm, tape, hifi, cd or studio\n"
	.size	.L.str.28, 77

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"                    help gives some more infos on these\n"
	.size	.L.str.29, 57

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"  CBR (constant bitrate, the default) options:\n"
	.size	.L.str.30, 48

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"    -h              higher quality, but a little slower.  Recommended.\n"
	.size	.L.str.31, 72

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"    -f              fast mode (very low quality)\n"
	.size	.L.str.32, 50

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"    -b bitrate      set the bitrate, default 128kbps\n"
	.size	.L.str.33, 54

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"  VBR options:\n"
	.size	.L.str.34, 16

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"    -v              use variable bitrate (VBR)\n"
	.size	.L.str.35, 48

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"    -V n            quality setting for VBR.  default n=%i\n"
	.size	.L.str.36, 60

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"                    0=high quality,bigger files. 9=smaller files\n"
	.size	.L.str.37, 66

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"    -b bitrate      specify minimum allowed bitrate, default 32kbs\n"
	.size	.L.str.38, 68

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"    -B bitrate      specify maximum allowed bitrate, default 256kbs\n"
	.size	.L.str.39, 69

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"    -t              disable Xing VBR informational tag\n"
	.size	.L.str.40, 56

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"    --nohist        disable VBR histogram display\n"
	.size	.L.str.41, 51

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"  MP3 header/stream options:\n"
	.size	.L.str.42, 30

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"    -e emp          de-emphasis n/5/c  (obsolete)\n"
	.size	.L.str.43, 51

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"    -c              mark as copyright\n"
	.size	.L.str.44, 39

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"    -o              mark as non-original\n"
	.size	.L.str.45, 42

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"    -p              error protection.  adds 16bit checksum to every frame\n"
	.size	.L.str.46, 75

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"                    (the checksum is computed correctly)\n"
	.size	.L.str.47, 58

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"    --nores         disable the bit reservoir\n"
	.size	.L.str.48, 47

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"  Specifying any of the following options will add an ID3 tag:\n"
	.size	.L.str.49, 64

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"     --tt \"title\"     title of song (max 30 chars)\n"
	.size	.L.str.50, 52

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"     --ta \"artist\"    artist who did the song (max 30 chars)\n"
	.size	.L.str.51, 62

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"     --tl \"album\"     album where it came from (max 30 chars)\n"
	.size	.L.str.52, 63

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"     --ty \"year\"      year in which the song/album was made (max 4 chars)\n"
	.size	.L.str.53, 75

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"     --tc \"comment\"   additional info (max 30 chars)\n"
	.size	.L.str.54, 54

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"                      (or max 28 chars if using the \"track\" option)\n"
	.size	.L.str.55, 69

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"     --tn \"track\"     track number of the song on the CD (1 to 99)\n"
	.size	.L.str.56, 68

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"                      (using this option will add an ID3v1.1 tag)\n"
	.size	.L.str.57, 67

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"     --tg \"genre\"     genre of song (name or number)\n"
	.size	.L.str.58, 54

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"Presets are some shortcuts for common settings.\n"
	.size	.L.str.59, 49

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"They can be combined with -v if you want VBR MP3s.\n"
	.size	.L.str.60, 52

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"  --preset phone    =>  --resample      16\n"
	.size	.L.str.61, 44

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"                        --highpass       0.260\n"
	.size	.L.str.62, 48

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"                        --highpasswidth  0.040\n"
	.size	.L.str.63, 48

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"                        --lowpass        3.700\n"
	.size	.L.str.64, 48

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"                        --lowpasswidth   0.300\n"
	.size	.L.str.65, 48

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"                        --noshort\n"
	.size	.L.str.66, 35

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"                        -m   m\n"
	.size	.L.str.67, 32

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"                        -b  16\n"
	.size	.L.str.68, 32

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"                  plus  -b   8  \\\n"
	.size	.L.str.69, 35

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"                        -B  56   > in combination with -v\n"
	.size	.L.str.70, 59

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"                        -V   5  /\n"
	.size	.L.str.71, 35

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"  --preset voice:   =>  --resample      24\n"
	.size	.L.str.72, 44

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"                        --highpass       0.100\n"
	.size	.L.str.73, 48

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"                        --highpasswidth  0.020\n"
	.size	.L.str.74, 48

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"                        --lowpass       11\n"
	.size	.L.str.75, 44

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"                        --lowpasswidth   2\n"
	.size	.L.str.76, 44

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"                        -b  32\n"
	.size	.L.str.77, 32

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"                        -B  96   > in combination with -v\n"
	.size	.L.str.78, 59

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"                        -V   4  /\n"
	.size	.L.str.79, 35

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"  --preset fm:      =>  --resample      32\n"
	.size	.L.str.80, 44

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"                        --highpass       0.030\n"
	.size	.L.str.81, 48

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"                        --highpasswidth  0\n"
	.size	.L.str.82, 44

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"                        --lowpass       11.4\n"
	.size	.L.str.83, 46

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"                        --lowpasswidth   0\n"
	.size	.L.str.84, 44

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"                        -m   j\n"
	.size	.L.str.85, 32

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"                        -b  96\n"
	.size	.L.str.86, 32

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"                  plus  -b  32  \\\n"
	.size	.L.str.87, 35

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"                        -B 192   > in combination with -v\n"
	.size	.L.str.88, 59

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"  --preset tape:    =>  --lowpass       17\n"
	.size	.L.str.89, 44

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"                        --highpass       0.015\n"
	.size	.L.str.90, 48

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"                        --highpasswidth  0.015\n"
	.size	.L.str.91, 48

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"                        -b 128\n"
	.size	.L.str.92, 32

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"  --preset hifi:    =>  --lowpass       20\n"
	.size	.L.str.93, 44

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"                        --lowpasswidth   3\n"
	.size	.L.str.94, 44

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"                        -h\n"
	.size	.L.str.95, 28

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"                        -b 160\n"
	.size	.L.str.96, 32

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"                        -B 224   > in combination with -v\n"
	.size	.L.str.97, 59

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"                        -V   3  /\n"
	.size	.L.str.98, 35

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"  --preset cd:      =>  -k\n"
	.size	.L.str.99, 28

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"                        -m   s\n"
	.size	.L.str.100, 32

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"                        -b 192\n"
	.size	.L.str.101, 32

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"                  plus  -b  80  \\\n"
	.size	.L.str.102, 35

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"                        -B 256   > in combination with -v\n"
	.size	.L.str.103, 59

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"                        -V   2  /\n"
	.size	.L.str.104, 35

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"  --preset studio:  =>  -k\n"
	.size	.L.str.105, 28

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"                        -b 256\n"
	.size	.L.str.106, 32

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"                  plus  -b 112  \\\n"
	.size	.L.str.107, 35

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"                        -B 320   > in combination with -v\n"
	.size	.L.str.108, 59

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"                        -V   0  /\n"
	.size	.L.str.109, 35

	.type	inPath,@object          # @inPath
	.comm	inPath,300,16
	.type	outPath,@object         # @outPath
	.comm	outPath,300,16
	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.zero	1
	.size	.L.str.110, 1

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"resample"
	.size	.L.str.111, 9

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"Must specify a samplerate with --resample\n"
	.size	.L.str.112, 43

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"mp3input"
	.size	.L.str.113, 9

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"voice"
	.size	.L.str.114, 6

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"noshort"
	.size	.L.str.115, 8

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"noath"
	.size	.L.str.116, 6

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"nores"
	.size	.L.str.117, 6

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"athonly"
	.size	.L.str.118, 8

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"nohist"
	.size	.L.str.119, 7

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"tt"
	.size	.L.str.120, 3

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"ta"
	.size	.L.str.121, 3

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"tl"
	.size	.L.str.122, 3

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"ty"
	.size	.L.str.123, 3

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"tc"
	.size	.L.str.124, 3

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"tn"
	.size	.L.str.125, 3

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"tg"
	.size	.L.str.126, 3

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"Unknown genre: %s.  Specifiy genre number \n"
	.size	.L.str.127, 44

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"lowpass"
	.size	.L.str.128, 8

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"Must specify lowpass with --lowpass freq, freq >= 0.001 kHz\n"
	.size	.L.str.129, 61

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"lowpass-width"
	.size	.L.str.130, 14

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"Must specify lowpass width with --lowpass-width freq, freq >= 0 kHz\n"
	.size	.L.str.131, 69

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"highpass"
	.size	.L.str.132, 9

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"Must specify highpass with --highpass freq, freq >= 0.001 kHz\n"
	.size	.L.str.133, 63

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"highpass-width"
	.size	.L.str.134, 15

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"Must specify highpass width with --highpass-width freq, freq >= 0 kHz\n"
	.size	.L.str.135, 71

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"cwlimit"
	.size	.L.str.136, 8

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"Must specify cwlimit in kHz\n"
	.size	.L.str.137, 29

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"help"
	.size	.L.str.138, 5

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"usage"
	.size	.L.str.139, 6

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"preset"
	.size	.L.str.140, 7

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"phone"
	.size	.L.str.141, 6

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"fm"
	.size	.L.str.142, 3

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"tape"
	.size	.L.str.143, 5

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"hifi"
	.size	.L.str.144, 5

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"cd"
	.size	.L.str.145, 3

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"studio"
	.size	.L.str.146, 7

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"%s: --preset type, type must be phone, voice, fm, tape, hifi, cd or studio, not %s\n"
	.size	.L.str.147, 84

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"%s: unrec option --%s\n"
	.size	.L.str.148, 23

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"%s: -m mode must be s/d/j/f/m not %s\n"
	.size	.L.str.149, 38

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"%s: -X n must be 0-6, not %s\n"
	.size	.L.str.150, 30

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"LAME not compiled with GTK support, -g not supported.\n"
	.size	.L.str.151, 55

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"%s: -e emp must be n/5/c not %s\n"
	.size	.L.str.152, 33

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"%s: unrec option %c\n"
	.size	.L.str.153, 21

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"%s: excess arg %s\n"
	.size	.L.str.154, 19

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	".mp3"
	.size	.L.str.156, 5

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	"id3tag ignored: id3 tagging not supported for stdout.\n"
	.size	.L.str.157, 55


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
