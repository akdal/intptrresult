	.text
	.file	"md_highfast.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	9218868437227405311     # double 1.7976931348623157E+308
	.text
	.globl	encode_one_macroblock_highfast
	.p2align	4, 0x90
	.type	encode_one_macroblock_highfast,@function
encode_one_macroblock_highfast:         # @encode_one_macroblock_highfast
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$440, %rsp              # imm = 0x1B8
.Lcfi6:
	.cfi_def_cfa_offset 496
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movw	$-256, 138(%rsp)
	movl	.Lencode_one_macroblock_highfast.bmcost+16(%rip), %eax
	movl	%eax, 432(%rsp)
	movaps	.Lencode_one_macroblock_highfast.bmcost(%rip), %xmm0
	movaps	%xmm0, 416(%rsp)
	movl	$0, 284(%rsp)
	movl	$0, 164(%rsp)
	movl	$0, 356(%rsp)
	movl	$0, 352(%rsp)
	movq	img(%rip), %rax
	movl	20(%rax), %esi
	movl	$0, 132(%rsp)           # 4-byte Folded Spill
	cmpl	$1, %esi
	sete	%bl
	testl	%esi, %esi
	sete	%cl
	cmpl	$3, %esi
	setne	%dl
	movl	%esi, 156(%rsp)         # 4-byte Spill
	cmpl	$2, %esi
	sete	%r14b
	je	.LBB0_5
# BB#1:
	xorb	%dl, %cl
	jne	.LBB0_5
# BB#2:
	movl	164(%rax), %ecx
	cmpl	112(%rax), %ecx
	jne	.LBB0_4
# BB#3:
	cmpl	116(%rax), %ecx
	setne	%r14b
	jmp	.LBB0_5
.LBB0_4:
	xorl	%r14d, %r14d
.LBB0_5:
	movq	14224(%rax), %rbp
	movl	12(%rax), %eax
	movslq	%eax, %rdi
	imulq	$536, %rdi, %rax        # imm = 0x218
	movq	%rax, 336(%rsp)         # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	FmoGetPreviousMBNr
	movq	img(%rip), %rcx
	testl	%eax, %eax
	js	.LBB0_7
# BB#6:
	cltq
	imulq	$536, %rax, %rax        # imm = 0x218
	addq	14224(%rcx), %rax
	jmp	.LBB0_8
.LBB0_7:
	xorl	%eax, %eax
.LBB0_8:                                # %._crit_edge583
	movq	%rax, 368(%rsp)         # 8-byte Spill
	movl	132(%rsp), %eax         # 4-byte Reload
	movb	%bl, %al
	movl	%eax, 132(%rsp)         # 4-byte Spill
	movq	336(%rsp), %rax         # 8-byte Reload
	leaq	(%rbp,%rax), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	14384(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 408(%rsp)         # 8-byte Spill
	movw	$0, 130(%rsp)
	movw	$0, 310(%rsp)
	movq	$0, 344(%rsp)
	movq	input(%rip), %rax
	movl	5244(%rax), %eax
	cmpl	$2, %eax
	je	.LBB0_11
# BB#9:                                 # %._crit_edge583
	cmpl	$1, %eax
	jne	.LBB0_12
# BB#10:
	callq	UMHEX_decide_intrabk_SAD
	jmp	.LBB0_12
.LBB0_11:
	callq	smpUMHEX_decide_intrabk_SAD
.LBB0_12:
	xorl	%r12d, %r12d
	movl	156(%rsp), %ebx         # 4-byte Reload
	cmpl	$1, %ebx
	sete	%r12b
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	callq	RandomIntra
	movzbl	%r14b, %ecx
	orl	%eax, %ecx
	movl	%ecx, 140(%rsp)         # 4-byte Spill
	movswl	%cx, %r13d
	leaq	184(%rsp), %rsi
	movq	168(%rsp), %rdi         # 8-byte Reload
	movl	%r13d, %edx
	movl	%r12d, %ecx
	callq	init_enc_mb_params
	movq	336(%rsp), %rax         # 8-byte Reload
	leaq	416(%rbp,%rax), %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	cmpl	$2, %ebx
	setne	163(%rsp)               # 1-byte Folded Spill
	movl	$0, 416(%rbp,%rax)
	movq	cs_cm(%rip), %rdi
	callq	store_coding_state
	testw	%r13w, %r13w
	movq	%rbp, 360(%rsp)         # 8-byte Spill
	je	.LBB0_14
# BB#13:
	xorl	%ecx, %ecx
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	%xmm0, 296(%rsp)        # 8-byte Spill
	jmp	.LBB0_82
.LBB0_14:
	movw	$1, best_mode(%rip)
	cmpl	$1, %ebx
	jne	.LBB0_17
# BB#15:
	callq	Get_Direct_Motion_Vectors
	cmpw	$0, 228(%rsp)
	je	.LBB0_17
# BB#16:
	movw	$0, best_mode(%rip)
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax)
	movabsq	$5055640609639927018, %rax # imm = 0x46293E5939A08CEA
	movq	%rax, 312(%rsp)
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	130(%rsp), %rax
	movq	%rax, 96(%rsp)
	movzwl	132(%rsp), %r9d         # 2-byte Folded Reload
	leaq	312(%rsp), %rdx
	leaq	344(%rsp), %rcx
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	movq	168(%rsp), %rsi         # 8-byte Reload
	callq	compute_mode_RD_cost
.LBB0_17:
	movl	%r13d, 320(%rsp)        # 4-byte Spill
	movq	input(%rip), %rax
	cmpl	$1, 4172(%rax)
	jne	.LBB0_19
# BB#18:
	callq	get_initial_mb16x16_cost
.LBB0_19:                               # %.preheader503.preheader
	movq	336(%rsp), %rax         # 8-byte Reload
	leaq	468(%rbp,%rax), %rax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	movl	$2147483647, %ebx       # imm = 0x7FFFFFFF
	xorl	%eax, %eax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	%xmm0, 296(%rsp)        # 8-byte Spill
	movl	$1, %ebp
	leaq	416(%rsp), %r14
	leaq	138(%rsp), %r13
	.p2align	4, 0x90
.LBB0_20:                               # %.preheader503
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_23 Depth 2
	movw	$0, bi_pred_me(%rip)
	movq	img(%rip), %rax
	movw	$0, 14408(%rax,%rbp,2)
	cmpw	$0, 228(%rsp,%rbp,2)
	je	.LBB0_71
# BB#21:                                # %.preheader503
                                        #   in Loop: Header=BB0_20 Depth=1
	movzwl	130(%rsp), %eax
	testw	%ax, %ax
	jne	.LBB0_71
# BB#22:                                #   in Loop: Header=BB0_20 Depth=1
	movl	%ebx, 376(%rsp)         # 4-byte Spill
	movl	$0, 284(%rsp)
	xorl	%ebx, %ebx
	cmpq	$1, %rbp
	setne	%bl
	incq	%rbx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_23:                               # %.preheader497
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	input(%rip), %rcx
	movl	216(%rsp), %eax
	cmpl	$0, 4172(%rcx)
	je	.LBB0_33
# BB#24:                                #   in Loop: Header=BB0_23 Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movsd	lambda_mf_factor(%rip), %xmm1 # xmm1 = mem[0],zero
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_26
# BB#25:                                # %call.sqrt
                                        #   in Loop: Header=BB0_23 Depth=2
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 176(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	176(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_26:                               # %.split
                                        #   in Loop: Header=BB0_23 Depth=2
	mulsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %eax
	movq	input(%rip), %rcx
	cmpl	$0, 4172(%rcx)
	movl	%eax, 388(%rsp)
	movl	220(%rsp), %eax
	je	.LBB0_34
# BB#27:                                #   in Loop: Header=BB0_23 Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movsd	lambda_mf_factor(%rip), %xmm1 # xmm1 = mem[0],zero
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_29
# BB#28:                                # %call.sqrt698
                                        #   in Loop: Header=BB0_23 Depth=2
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 176(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	176(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_29:                               # %.split697
                                        #   in Loop: Header=BB0_23 Depth=2
	mulsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %eax
	movq	input(%rip), %rcx
	cmpl	$0, 4172(%rcx)
	movl	%eax, 392(%rsp)
	movl	224(%rsp), %eax
	je	.LBB0_35
# BB#30:                                #   in Loop: Header=BB0_23 Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movsd	lambda_mf_factor(%rip), %xmm1 # xmm1 = mem[0],zero
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_32
# BB#31:                                # %call.sqrt700
                                        #   in Loop: Header=BB0_23 Depth=2
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 176(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	176(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_32:                               # %.split699
                                        #   in Loop: Header=BB0_23 Depth=2
	mulsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %eax
	jmp	.LBB0_35
	.p2align	4, 0x90
.LBB0_33:                               # %.thread
                                        #   in Loop: Header=BB0_23 Depth=2
	movl	%eax, 388(%rsp)
	movl	220(%rsp), %eax
.LBB0_34:                               # %.thread636
                                        #   in Loop: Header=BB0_23 Depth=2
	movl	%eax, 392(%rsp)
	movl	224(%rsp), %eax
.LBB0_35:                               #   in Loop: Header=BB0_23 Depth=2
	movl	%eax, 396(%rsp)
	movl	%ebp, %edi
	movl	%r15d, %esi
	leaq	388(%rsp), %rdx
	callq	PartitionMotionSearch
	movl	$2147483647, 416(%rsp)  # imm = 0x7FFFFFFF
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movl	$0, %edi
	movl	%r15d, %esi
	movl	%ebp, %edx
	movq	%r14, %rcx
	movq	%r13, %r8
	callq	list_prediction_cost
	cmpl	$1, 156(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_39
# BB#36:                                #   in Loop: Header=BB0_23 Depth=2
	movl	$2147483647, 420(%rsp)  # imm = 0x7FFFFFFF
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	184(%rsp), %xmm0
	movups	200(%rsp), %xmm1
	movups	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$1, %edi
	movl	%r15d, %esi
	movl	%ebp, %edx
	movq	%r14, %rcx
	movq	%r13, %r8
	callq	list_prediction_cost
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movl	$2, %edi
	movl	%r15d, %esi
	movl	%ebp, %edx
	movq	%r14, %rcx
	movq	%r13, %r8
	callq	list_prediction_cost
	cmpq	$1, %rbp
	jne	.LBB0_40
# BB#37:                                #   in Loop: Header=BB0_23 Depth=2
	movq	input(%rip), %rax
	movl	2120(%rax), %eax
	testl	%eax, %eax
	je	.LBB0_40
# BB#38:                                #   in Loop: Header=BB0_23 Depth=2
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	184(%rsp), %xmm0
	movups	200(%rsp), %xmm1
	movups	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$3, %edi
	movl	$1, %edx
	xorl	%r8d, %r8d
	movl	%r15d, %esi
	movq	%r14, %rcx
	callq	list_prediction_cost
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movl	$4, %edi
	movl	$1, %edx
	xorl	%r8d, %r8d
	movl	%r15d, %esi
	movq	%r14, %rcx
	callq	list_prediction_cost
	jmp	.LBB0_41
	.p2align	4, 0x90
.LBB0_39:                               #   in Loop: Header=BB0_23 Depth=2
	movb	$0, 129(%rsp)
	movl	416(%rsp), %eax
	addl	%eax, 284(%rsp)
	xorl	%eax, %eax
	jmp	.LBB0_42
	.p2align	4, 0x90
.LBB0_40:                               #   in Loop: Header=BB0_23 Depth=2
	movabsq	$9223372034707292159, %rax # imm = 0x7FFFFFFF7FFFFFFF
	movq	%rax, 428(%rsp)
.LBB0_41:                               #   in Loop: Header=BB0_23 Depth=2
	movl	$bi_pred_me, %r9d
	movl	%ebp, %edi
	movq	%r14, %rsi
	movq	%r13, %rdx
	leaq	129(%rsp), %rcx
	leaq	284(%rsp), %r8
	callq	determine_prediction_list
	movb	129(%rsp), %al
.LBB0_42:                               #   in Loop: Header=BB0_23 Depth=2
	movswl	258(%rsp), %ecx
	movsbl	138(%rsp), %r8d
	movsbl	139(%rsp), %r9d
	movl	%r12d, (%rsp)
	movsbl	%al, %esi
	movl	%ebp, %edi
	movl	%r15d, %edx
	callq	assign_enc_picture_params
	movzbl	138(%rsp), %eax
	cmpl	$2, %ebp
	je	.LBB0_45
# BB#43:                                #   in Loop: Header=BB0_23 Depth=2
	cmpl	$3, %ebp
	jne	.LBB0_46
# BB#44:                                #   in Loop: Header=BB0_23 Depth=2
	movb	%al, best8x8fwref+14(%r15)
	movb	%al, best8x8fwref+12(%r15)
	movb	129(%rsp), %dl
	movb	%dl, best8x8pdir+14(%r15)
	movb	%dl, best8x8pdir+12(%r15)
	movb	139(%rsp), %sil
	movb	%sil, best8x8bwref+14(%r15)
	movb	%sil, best8x8bwref+12(%r15)
	cmpq	$2, %rbp
	jge	.LBB0_47
	jmp	.LBB0_49
	.p2align	4, 0x90
.LBB0_45:                               #   in Loop: Header=BB0_23 Depth=2
	movb	%al, best8x8fwref+9(%r15,%r15)
	movb	%al, best8x8fwref+8(%r15,%r15)
	movb	129(%rsp), %dl
	movb	%dl, best8x8pdir+9(%r15,%r15)
	movb	%dl, best8x8pdir+8(%r15,%r15)
	movb	139(%rsp), %sil
	movb	%sil, best8x8bwref+9(%r15,%r15)
	movb	%sil, best8x8bwref+8(%r15,%r15)
	cmpq	$2, %rbp
	jge	.LBB0_47
	jmp	.LBB0_49
	.p2align	4, 0x90
.LBB0_46:                               #   in Loop: Header=BB0_23 Depth=2
	movzbl	%al, %ecx
	imull	$16843009, %ecx, %ecx   # imm = 0x1010101
	movl	%ecx, best8x8fwref+4(%rip)
	movzbl	139(%rsp), %esi
	imull	$16843009, %esi, %ecx   # imm = 0x1010101
	movl	%ecx, best8x8bwref+4(%rip)
	movzbl	129(%rsp), %edx
	imull	$16843009, %edx, %ecx   # imm = 0x1010101
	movl	%ecx, best8x8pdir+4(%rip)
	cmpq	$2, %rbp
	jl	.LBB0_49
.LBB0_47:                               #   in Loop: Header=BB0_23 Depth=2
	testq	%r15, %r15
	jne	.LBB0_49
# BB#48:                                #   in Loop: Header=BB0_23 Depth=2
	movsbl	%al, %ecx
	movsbl	%dl, %edx
	movsbl	%sil, %r8d
	xorl	%edi, %edi
	movl	%ebp, %esi
	callq	SetRefAndMotionVectors
.LBB0_49:                               #   in Loop: Header=BB0_23 Depth=2
	incq	%r15
	cmpq	%rbx, %r15
	jl	.LBB0_23
# BB#50:                                #   in Loop: Header=BB0_20 Depth=1
	cmpq	$1, %rbp
	movl	376(%rsp), %r15d        # 4-byte Reload
	jne	.LBB0_65
# BB#51:                                #   in Loop: Header=BB0_20 Depth=1
	movl	156(%rsp), %ebx         # 4-byte Reload
	cmpl	$3, %ebx
	je	.LBB0_53
# BB#52:                                #   in Loop: Header=BB0_20 Depth=1
	testl	%ebx, %ebx
	jne	.LBB0_54
.LBB0_53:                               #   in Loop: Header=BB0_20 Depth=1
	movabsq	$5055640609639927018, %rax # imm = 0x46293E5939A08CEA
	movq	%rax, 312(%rsp)
.LBB0_54:                               # %.preheader500
                                        #   in Loop: Header=BB0_20 Depth=1
	cmpl	$1, %ebx
	jne	.LBB0_56
# BB#55:                                # %.us-lcssa.us.thread
                                        #   in Loop: Header=BB0_20 Depth=1
	movl	$0, best8x8pdir+4(%rip)
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax)
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	130(%rsp), %rax
	movq	%rax, 96(%rsp)
	movzwl	132(%rsp), %r9d         # 2-byte Folded Reload
	movl	$1, %edi
	xorl	%r8d, %r8d
	movq	168(%rsp), %rsi         # 8-byte Reload
	leaq	312(%rsp), %rdx
	leaq	344(%rsp), %rcx
	callq	compute_mode_RD_cost
	jmp	.LBB0_64
.LBB0_56:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB0_20 Depth=1
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax)
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	130(%rsp), %rax
	movq	%rax, 96(%rsp)
	movzwl	132(%rsp), %r9d         # 2-byte Folded Reload
	movl	$1, %edi
	xorl	%r8d, %r8d
	movq	168(%rsp), %rsi         # 8-byte Reload
	leaq	312(%rsp), %rdx
	leaq	344(%rsp), %rcx
	callq	compute_mode_RD_cost
	cmpl	$3, %ebx
	je	.LBB0_58
# BB#57:                                # %.us-lcssa.us
                                        #   in Loop: Header=BB0_20 Depth=1
	cmpl	$0, 156(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_64
.LBB0_58:                               #   in Loop: Header=BB0_20 Depth=1
	callq	FindSkipModeMotionVector
	movq	input(%rip), %rax
	cmpl	$0, 5744(%rax)
	je	.LBB0_64
# BB#59:                                #   in Loop: Header=BB0_20 Depth=1
	movq	144(%rsp), %rax         # 8-byte Reload
	cmpl	$0, -52(%rax)
	jne	.LBB0_64
# BB#60:                                #   in Loop: Header=BB0_20 Depth=1
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rcx
	movq	(%rcx), %rdx
	movq	img(%rip), %rsi
	movslq	172(%rsi), %rcx
	movq	(%rdx,%rcx,8), %rdi
	movslq	168(%rsi), %rdx
	cmpb	$0, (%rdi,%rdx)
	jne	.LBB0_64
# BB#61:                                #   in Loop: Header=BB0_20 Depth=1
	movq	6512(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%rdx,8), %rax
	movzwl	(%rax), %ecx
	movq	408(%rsp), %rdx         # 8-byte Reload
	cmpw	(%rdx), %cx
	jne	.LBB0_64
# BB#62:                                #   in Loop: Header=BB0_20 Depth=1
	movzwl	2(%rax), %eax
	movq	408(%rsp), %rcx         # 8-byte Reload
	cmpw	2(%rcx), %ax
	jne	.LBB0_64
# BB#63:                                #   in Loop: Header=BB0_20 Depth=1
	movw	$1, 130(%rsp)
	movw	$0, best_mode(%rip)
	.p2align	4, 0x90
.LBB0_64:                               #   in Loop: Header=BB0_20 Depth=1
	movsd	312(%rsp), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 296(%rsp)        # 8-byte Spill
	movswl	best_mode(%rip), %eax
	movq	%rax, 288(%rsp)         # 8-byte Spill
.LBB0_65:                               #   in Loop: Header=BB0_20 Depth=1
	cmpw	$0, 130(%rsp)
	jne	.LBB0_70
# BB#66:                                #   in Loop: Header=BB0_20 Depth=1
	movl	284(%rsp), %ebx
	cmpl	%r15d, %ebx
	jge	.LBB0_70
# BB#67:                                #   in Loop: Header=BB0_20 Depth=1
	movw	%bp, best_mode(%rip)
	movq	input(%rip), %rax
	cmpl	$1, 4172(%rax)
	jne	.LBB0_71
# BB#68:                                #   in Loop: Header=BB0_20 Depth=1
	movl	%ebx, %edi
	callq	adjust_mb16x16_cost
	jmp	.LBB0_71
	.p2align	4, 0x90
.LBB0_70:                               #   in Loop: Header=BB0_20 Depth=1
	movl	%r15d, %ebx
.LBB0_71:                               #   in Loop: Header=BB0_20 Depth=1
	incq	%rbp
	cmpq	$4, %rbp
	jne	.LBB0_20
# BB#72:
	cmpw	$0, 130(%rsp)
	movl	320(%rsp), %r13d        # 4-byte Reload
	jne	.LBB0_80
# BB#73:
	movzwl	244(%rsp), %eax
	testw	%ax, %ax
	je	.LBB0_80
# BB#74:
	movl	$1, giRDOpt_B8OnlyFlag(%rip)
	movl	$2147483647, tr8x8(%rip) # imm = 0x7FFFFFFF
	movl	$2147483647, tr4x4(%rip) # imm = 0x7FFFFFFF
	movq	cs_mb(%rip), %rdi
	callq	store_coding_state
	movq	400(%rsp), %rcx         # 8-byte Reload
	movl	$-1, (%rcx)
	movq	input(%rip), %rax
	cmpl	$0, 5100(%rax)
	je	.LBB0_76
# BB#75:
	movl	$0, tr8x8(%rip)
	movl	$0, cnt_nonz_8x8(%rip)
	movl	$0, cbp_blk8x8(%rip)
	movl	$0, cbp8x8(%rip)
	movl	$0, 164(%rsp)
	movq	cofAC_8x8ts(%rip), %rax
	movq	(%rax), %rdx
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	184(%rsp), %xmm0
	movups	200(%rsp), %xmm1
	movups	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	352(%rsp), %r12
	movq	%r12, 112(%rsp)
	leaq	284(%rsp), %r14
	movq	%r14, 104(%rsp)
	leaq	164(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$1, 120(%rsp)
	movzwl	132(%rsp), %ebx         # 2-byte Folded Reload
	leaq	356(%rsp), %r15
	movl	$tr8x8, %edi
	xorl	%r9d, %r9d
	movq	168(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rsi
	movq	%r15, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr8x8+6148(%rip), %eax
	movw	%ax, best8x8mode(%rip)
	movb	tr8x8+6156(%rip), %al
	movb	%al, best8x8pdir+32(%rip)
	movb	tr8x8+6160(%rip), %al
	movb	%al, best8x8fwref+32(%rip)
	movb	tr8x8+6164(%rip), %al
	movb	%al, best8x8bwref+32(%rip)
	movq	cofAC_8x8ts(%rip), %rax
	movq	8(%rax), %rdx
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	184(%rsp), %xmm0
	movups	200(%rsp), %xmm1
	movups	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r12, 112(%rsp)
	movq	%r14, 104(%rsp)
	leaq	164(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$1, 120(%rsp)
	movl	$tr8x8, %edi
	movl	$1, %r9d
	movq	%rbp, %rsi
	movq	%r15, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr8x8+6150(%rip), %eax
	movw	%ax, best8x8mode+2(%rip)
	movb	tr8x8+6157(%rip), %al
	movb	%al, best8x8pdir+33(%rip)
	movb	tr8x8+6161(%rip), %al
	movb	%al, best8x8fwref+33(%rip)
	movb	tr8x8+6165(%rip), %al
	movb	%al, best8x8bwref+33(%rip)
	movq	cofAC_8x8ts(%rip), %rax
	movq	16(%rax), %rdx
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	184(%rsp), %xmm0
	movups	200(%rsp), %xmm1
	movups	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r12, 112(%rsp)
	movq	%r14, 104(%rsp)
	leaq	164(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$1, 120(%rsp)
	movl	$tr8x8, %edi
	movl	$2, %r9d
	movq	%rbp, %rsi
	movq	%r15, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr8x8+6152(%rip), %eax
	movw	%ax, best8x8mode+4(%rip)
	movb	tr8x8+6158(%rip), %al
	movb	%al, best8x8pdir+34(%rip)
	movb	tr8x8+6162(%rip), %al
	movb	%al, best8x8fwref+34(%rip)
	movb	tr8x8+6166(%rip), %al
	movb	%al, best8x8bwref+34(%rip)
	movq	cofAC_8x8ts(%rip), %rax
	movq	24(%rax), %rdx
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movq	%r12, 112(%rsp)
	movq	%r14, 104(%rsp)
	leaq	164(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$1, 120(%rsp)
	movl	$tr8x8, %edi
	movl	$3, %r9d
	movq	%rbp, %rsi
	movq	%r15, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr8x8+6154(%rip), %eax
	movw	%ax, best8x8mode+6(%rip)
	movb	tr8x8+6159(%rip), %al
	movb	%al, best8x8pdir+35(%rip)
	movb	tr8x8+6163(%rip), %al
	movb	%al, best8x8fwref+35(%rip)
	movb	tr8x8+6167(%rip), %al
	movb	%al, best8x8bwref+35(%rip)
	movl	cbp8x8(%rip), %eax
	movl	%eax, cbp8_8x8ts(%rip)
	movslq	cbp_blk8x8(%rip), %rax
	movq	%rax, cbp_blk8_8x8ts(%rip)
	movl	cnt_nonz_8x8(%rip), %eax
	movl	%eax, cnt_nonz8_8x8ts(%rip)
	movq	400(%rsp), %rax         # 8-byte Reload
	movl	$0, 4(%rax)
	movq	input(%rip), %rax
	cmpl	$2, 5100(%rax)
	je	.LBB0_77
.LBB0_76:                               # %.loopexit502.loopexit566
	movl	$0, tr4x4(%rip)
	movl	$0, cnt_nonz_8x8(%rip)
	movl	$0, cbp_blk8x8(%rip)
	movl	$0, cbp8x8(%rip)
	movl	$0, 164(%rsp)
	movq	cofAC8x8(%rip), %rax
	movq	(%rax), %rdx
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	184(%rsp), %xmm0
	movups	200(%rsp), %xmm1
	movups	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	352(%rsp), %r12
	movq	%r12, 112(%rsp)
	leaq	284(%rsp), %r14
	movq	%r14, 104(%rsp)
	leaq	164(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$0, 120(%rsp)
	movzwl	132(%rsp), %ebx         # 2-byte Folded Reload
	leaq	356(%rsp), %r15
	movl	$tr4x4, %edi
	xorl	%r9d, %r9d
	movq	168(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rsi
	movq	%r15, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6148(%rip), %eax
	movw	%ax, best8x8mode(%rip)
	movb	tr4x4+6156(%rip), %al
	movb	%al, best8x8pdir+32(%rip)
	movb	tr4x4+6160(%rip), %al
	movb	%al, best8x8fwref+32(%rip)
	movb	tr4x4+6164(%rip), %al
	movb	%al, best8x8bwref+32(%rip)
	movq	cofAC8x8(%rip), %rax
	movq	8(%rax), %rdx
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	184(%rsp), %xmm0
	movups	200(%rsp), %xmm1
	movups	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r12, 112(%rsp)
	movq	%r14, 104(%rsp)
	leaq	164(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$0, 120(%rsp)
	movl	$tr4x4, %edi
	movl	$1, %r9d
	movq	%rbp, %rsi
	movq	%r15, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6150(%rip), %eax
	movw	%ax, best8x8mode+2(%rip)
	movb	tr4x4+6157(%rip), %al
	movb	%al, best8x8pdir+33(%rip)
	movb	tr4x4+6161(%rip), %al
	movb	%al, best8x8fwref+33(%rip)
	movb	tr4x4+6165(%rip), %al
	movb	%al, best8x8bwref+33(%rip)
	movq	cofAC8x8(%rip), %rax
	movq	16(%rax), %rdx
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	184(%rsp), %xmm0
	movups	200(%rsp), %xmm1
	movups	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r12, 112(%rsp)
	movq	%r14, 104(%rsp)
	leaq	164(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$0, 120(%rsp)
	movl	$tr4x4, %edi
	movl	$2, %r9d
	movq	%rbp, %rsi
	movq	%r15, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6152(%rip), %eax
	movw	%ax, best8x8mode+4(%rip)
	movb	tr4x4+6158(%rip), %al
	movb	%al, best8x8pdir+34(%rip)
	movb	tr4x4+6162(%rip), %al
	movb	%al, best8x8fwref+34(%rip)
	movb	tr4x4+6166(%rip), %al
	movb	%al, best8x8bwref+34(%rip)
	movq	cofAC8x8(%rip), %rax
	movq	24(%rax), %rdx
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movq	%r12, 112(%rsp)
	movq	%r14, 104(%rsp)
	leaq	164(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	$0, 120(%rsp)
	movl	$tr4x4, %edi
	movl	$3, %r9d
	movq	%rbp, %rsi
	movq	%r15, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6154(%rip), %eax
	movw	%ax, best8x8mode+6(%rip)
	movb	tr4x4+6159(%rip), %al
	movb	%al, best8x8pdir+35(%rip)
	movb	tr4x4+6163(%rip), %al
	movb	%al, best8x8fwref+35(%rip)
	movb	tr4x4+6167(%rip), %al
	movb	%al, best8x8bwref+35(%rip)
.LBB0_77:                               # %.loopexit502
	movq	cs_mb(%rip), %rdi
	callq	reset_coding_state
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB0_79
# BB#78:
	movq	img(%rip), %rdx
	movl	192(%rdx), %edi
	movl	196(%rdx), %esi
	addq	$12624, %rdx            # imm = 0x3150
	callq	rc_store_diff
.LBB0_79:
	movl	$0, giRDOpt_B8OnlyFlag(%rip)
	jmp	.LBB0_81
.LBB0_80:
	movl	$2147483647, tr4x4(%rip) # imm = 0x7FFFFFFF
.LBB0_81:
	movq	360(%rsp), %rbp         # 8-byte Reload
	movq	288(%rsp), %rcx         # 8-byte Reload
.LBB0_82:
	movq	336(%rsp), %rax         # 8-byte Reload
	leaq	72(%rbp,%rax), %r12
	cmpw	$0, 130(%rsp)
	jne	.LBB0_260
# BB#83:
	movq	img(%rip), %rax
	cmpl	$2, 20(%rax)
	jne	.LBB0_85
# BB#84:
	movabsq	$5055640609639927018, %rcx # imm = 0x46293E5939A08CEA
	movq	%rcx, 312(%rsp)
	jmp	.LBB0_86
.LBB0_85:
	movsd	296(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 312(%rsp)
	movw	%cx, best_mode(%rip)
.LBB0_86:
	cmpw	$0, 140(%rsp)           # 2-byte Folded Reload
	movq	input(%rip), %rdx
	je	.LBB0_88
# BB#87:
	xorl	%ecx, %ecx
	jmp	.LBB0_89
.LBB0_88:
	cmpl	$0, 5748(%rdx)
	setne	%cl
.LBB0_89:                               # %._crit_edge593
	xorl	%ebx, %ebx
	testb	%cl, %cl
	sete	%r14b
	cmpl	$0, 2120(%rdx)
	je	.LBB0_91
# BB#90:
	movw	$0, 14410(%rax)
.LBB0_91:
	xorl	%ebp, %ebp
	cmpl	$0, 15536(%rax)
	je	.LBB0_96
# BB#92:
	xorb	$1, %cl
	je	.LBB0_96
# BB#93:
	leaq	332(%rsp), %rdi
	leaq	328(%rsp), %rsi
	leaq	324(%rsp), %rdx
	callq	IntraChromaPrediction
	movq	input(%rip), %rax
	cmpl	$0, 4176(%rax)
	je	.LBB0_97
# BB#94:
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	callq	IntraChromaRDDecision
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %ebp
	movl	%ebp, %eax
	jmp	.LBB0_98
.LBB0_96:
	xorl	%eax, %eax
	jmp	.LBB0_98
.LBB0_97:
	movl	$3, %eax
.LBB0_98:
	movq	%r12, 376(%rsp)         # 8-byte Spill
	movswl	%bp, %ecx
	movq	144(%rsp), %rdx         # 8-byte Reload
	movl	%ecx, (%rdx)
	movswl	%ax, %edi
	cmpl	%edi, %ecx
	movq	img(%rip), %r10
	movl	%r13d, 320(%rsp)        # 4-byte Spill
	jg	.LBB0_166
# BB#99:                                # %.lr.ph.preheader
	movb	%r14b, %bl
	leal	5(,%rbx,4), %r14d
	leaq	344(%rsp), %r12
	movl	156(%rsp), %ebp         # 4-byte Reload
	movq	144(%rsp), %rbx         # 8-byte Reload
	movl	%edi, 176(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB0_100:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_132 Depth 2
                                        #       Child Loop BB0_150 Depth 3
                                        #       Child Loop BB0_154 Depth 3
                                        #       Child Loop BB0_158 Depth 3
                                        #       Child Loop BB0_162 Depth 3
	movl	15536(%r10), %edx
	testl	%edx, %edx
	movl	$0, %esi
	je	.LBB0_114
# BB#101:                               #   in Loop: Header=BB0_100 Depth=1
	movq	input(%rip), %rsi
	cmpw	$0, 140(%rsp)           # 2-byte Folded Reload
	je	.LBB0_103
# BB#102:                               #   in Loop: Header=BB0_100 Depth=1
	cmpl	$0, 4048(%rsi)
	jne	.LBB0_105
.LBB0_103:                              # %._crit_edge595
                                        #   in Loop: Header=BB0_100 Depth=1
	cmpl	$1, 4072(%rsi)
	jne	.LBB0_105
# BB#104:                               #   in Loop: Header=BB0_100 Depth=1
	testl	%ecx, %ecx
	movl	%edx, %esi
	jne	.LBB0_165
	jmp	.LBB0_114
	.p2align	4, 0x90
.LBB0_105:                              #   in Loop: Header=BB0_100 Depth=1
	cmpl	$2, %ecx
	movl	332(%rsp), %esi
	jne	.LBB0_107
# BB#106:                               #   in Loop: Header=BB0_100 Depth=1
	testl	%esi, %esi
	je	.LBB0_165
.LBB0_107:                              #   in Loop: Header=BB0_100 Depth=1
	cmpl	$1, %ecx
	movl	328(%rsp), %eax
	jne	.LBB0_109
# BB#108:                               #   in Loop: Header=BB0_100 Depth=1
	testl	%eax, %eax
	je	.LBB0_165
.LBB0_109:                              #   in Loop: Header=BB0_100 Depth=1
	cmpl	$3, %ecx
	jne	.LBB0_113
# BB#110:                               #   in Loop: Header=BB0_100 Depth=1
	testl	%esi, %esi
	je	.LBB0_165
# BB#111:                               #   in Loop: Header=BB0_100 Depth=1
	testl	%eax, %eax
	je	.LBB0_165
# BB#112:                               #   in Loop: Header=BB0_100 Depth=1
	movl	324(%rsp), %ecx
	testl	%ecx, %ecx
	movl	%edx, %esi
	jne	.LBB0_114
	jmp	.LBB0_165
.LBB0_113:                              #   in Loop: Header=BB0_100 Depth=1
	movl	%edx, %esi
	.p2align	4, 0x90
.LBB0_114:                              # %.thread489.preheader.preheader
                                        #   in Loop: Header=BB0_100 Depth=1
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.LBB0_132
	.p2align	4, 0x90
.LBB0_115:                              # %.loopexit496
                                        #   in Loop: Header=BB0_132 Depth=2
	cmpw	$0, 228(%rsp,%rbx,2)
	je	.LBB0_117
# BB#116:                               #   in Loop: Header=BB0_132 Depth=2
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	leaq	130(%rsp), %rax
	movq	%rax, 96(%rsp)
	movzwl	132(%rsp), %r9d         # 2-byte Folded Reload
	xorl	%r8d, %r8d
	movl	%ebx, %edi
	movq	168(%rsp), %rsi         # 8-byte Reload
	leaq	312(%rsp), %rdx
	movq	%r12, %rcx
	callq	compute_mode_RD_cost
	movq	input(%rip), %r9
.LBB0_117:                              #   in Loop: Header=BB0_132 Depth=2
	movl	156(%rsp), %ebp         # 4-byte Reload
	cmpl	$1, %ebp
	jne	.LBB0_124
# BB#118:                               #   in Loop: Header=BB0_132 Depth=2
	cmpl	$2, %r13d
	jne	.LBB0_124
# BB#119:                               #   in Loop: Header=BB0_132 Depth=2
	movl	2120(%r9), %eax
	testl	%eax, %eax
	je	.LBB0_124
# BB#120:                               #   in Loop: Header=BB0_132 Depth=2
	movl	$2, %r13d
	cmpl	$1, %ebx
	jne	.LBB0_124
# BB#121:                               #   in Loop: Header=BB0_132 Depth=2
	movq	img(%rip), %rax
	movw	14408(%rax,%rbx,2), %cx
	movswl	%cx, %edx
	cmpl	$1, %edx
	jg	.LBB0_124
# BB#122:                               #   in Loop: Header=BB0_132 Depth=2
	cmpb	$2, best8x8pdir+4(%rip)
	jne	.LBB0_124
# BB#123:                               #   in Loop: Header=BB0_132 Depth=2
	incl	%ecx
	movw	%cx, 14408(%rax,%rbx,2)
	.p2align	4, 0x90
.LBB0_124:                              # %.critedge462
                                        #   in Loop: Header=BB0_132 Depth=2
	incl	%r15d
	movq	img(%rip), %r10
	cmpl	%r14d, %r15d
	jl	.LBB0_131
	jmp	.LBB0_164
.LBB0_125:                              # %.thread482
                                        #   in Loop: Header=BB0_132 Depth=2
	cmpl	$2, %r13d
	jne	.LBB0_130
# BB#126:                               # %.thread482
                                        #   in Loop: Header=BB0_132 Depth=2
	movl	2120(%r9), %eax
	testl	%eax, %eax
	movl	156(%rsp), %ebp         # 4-byte Reload
	je	.LBB0_124
# BB#127:                               #   in Loop: Header=BB0_132 Depth=2
	movl	$2, %r13d
	cmpl	$1, %ebx
	jne	.LBB0_124
# BB#128:                               #   in Loop: Header=BB0_132 Depth=2
	movw	14408(%r10,%rbx,2), %cx
	movswl	%cx, %eax
	cmpl	$1, %eax
	jg	.LBB0_124
# BB#129:                               # %.critedge
                                        #   in Loop: Header=BB0_132 Depth=2
	incl	%ecx
	movw	%cx, 14408(%r10,%rbx,2)
	jmp	.LBB0_124
.LBB0_130:                              #   in Loop: Header=BB0_132 Depth=2
	movl	156(%rsp), %ebp         # 4-byte Reload
	jmp	.LBB0_124
	.p2align	4, 0x90
.LBB0_131:                              # %.critedge462..thread489.preheader_crit_edge
                                        #   in Loop: Header=BB0_132 Depth=2
	movl	15536(%r10), %esi
.LBB0_132:                              # %.thread489.preheader
                                        #   Parent Loop BB0_100 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_150 Depth 3
                                        #       Child Loop BB0_154 Depth 3
                                        #       Child Loop BB0_158 Depth 3
                                        #       Child Loop BB0_162 Depth 3
	movslq	%r15d, %rcx
	movslq	mb_mode_table(,%rcx,4), %rbx
	testl	%esi, %esi
	je	.LBB0_135
# BB#133:                               #   in Loop: Header=BB0_132 Depth=2
	cmpl	$1, %ebp
	sete	%dl
	testl	%ebx, %ebx
	sete	%al
	cmpl	$1, %ebx
	sete	%cl
	testb	%al, %dl
	jne	.LBB0_124
# BB#134:                               #   in Loop: Header=BB0_132 Depth=2
	andb	163(%rsp), %cl          # 1-byte Folded Reload
	jne	.LBB0_124
.LBB0_135:                              #   in Loop: Header=BB0_132 Depth=2
	cmpl	$1, %ebp
	jne	.LBB0_141
# BB#136:                               #   in Loop: Header=BB0_132 Depth=2
	cmpl	$1, %ebx
	jne	.LBB0_141
# BB#137:                               #   in Loop: Header=BB0_132 Depth=2
	movzbl	%r13b, %eax
	imull	$16843009, %eax, %eax   # imm = 0x1010101
	movl	%eax, best8x8pdir+4(%rip)
	cmpl	$2, %r13d
	jne	.LBB0_140
# BB#138:                               #   in Loop: Header=BB0_132 Depth=2
	movq	input(%rip), %rax
	movl	2120(%rax), %eax
	testl	%eax, %eax
	je	.LBB0_140
# BB#139:                               #   in Loop: Header=BB0_132 Depth=2
	movswl	14410(%r10), %eax
	xorl	%ecx, %ecx
	cmpl	$2, %eax
	setl	%cl
	movl	$2, %r13d
	subl	%ecx, %r13d
.LBB0_140:                              #   in Loop: Header=BB0_132 Depth=2
	xorl	%eax, %eax
	cmpl	$2, %r13d
	setl	%al
	subl	%eax, %r15d
	incl	%r13d
.LBB0_141:                              #   in Loop: Header=BB0_132 Depth=2
	cmpw	$0, 140(%rsp)           # 2-byte Folded Reload
	sete	%dl
	movq	input(%rip), %r9
	cmpl	$0, 2960(%r9)
	setne	%cl
	movswl	best_mode(%rip), %eax
	cmpl	$3, %eax
	jg	.LBB0_145
# BB#142:                               #   in Loop: Header=BB0_132 Depth=2
	cmpl	$10, %ebx
	jl	.LBB0_145
# BB#143:                               #   in Loop: Header=BB0_132 Depth=2
	andb	%cl, %dl
	je	.LBB0_145
# BB#144:                               #   in Loop: Header=BB0_132 Depth=2
	movq	144(%rsp), %rax         # 8-byte Reload
	cmpl	$0, -52(%rax)
	je	.LBB0_124
.LBB0_145:                              #   in Loop: Header=BB0_132 Depth=2
	cmpl	$1, %ebp
	jne	.LBB0_115
# BB#146:                               #   in Loop: Header=BB0_132 Depth=2
	cmpl	$7, %ebx
	jg	.LBB0_115
# BB#147:                               #   in Loop: Header=BB0_132 Depth=2
	movq	active_pps(%rip), %rax
	cmpl	$1, 196(%rax)
	jne	.LBB0_115
# BB#148:                               # %.preheader495
                                        #   in Loop: Header=BB0_132 Depth=2
	movq	wbp_weight(%rip), %r8
	movq	active_sps(%rip), %rdx
	cmpb	$2, best8x8pdir(,%rbx,4)
	jne	.LBB0_152
# BB#149:                               # %.preheader
                                        #   in Loop: Header=BB0_132 Depth=2
	movsbq	best8x8fwref(,%rbx,4), %rax
	movq	(%r8), %rcx
	movq	8(%r8), %rsi
	movq	(%rcx,%rax,8), %rcx
	movsbq	best8x8bwref(,%rbx,4), %rbp
	movq	(%rcx,%rbp,8), %rdi
	movq	(%rsi,%rax,8), %rax
	movq	(%rax,%rbp,8), %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_150:                              #   Parent Loop BB0_100 Depth=1
                                        #     Parent Loop BB0_132 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdi,%rsi,4), %eax
	movl	(%rbp,%rsi,4), %ecx
	leal	128(%rax,%rcx), %eax
	cmpl	$255, %eax
	ja	.LBB0_125
# BB#151:                               #   in Loop: Header=BB0_150 Depth=3
	incq	%rsi
	xorl	%eax, %eax
	cmpl	$0, 32(%rdx)
	setne	%al
	leaq	1(%rax,%rax), %rax
	cmpq	%rax, %rsi
	jl	.LBB0_150
.LBB0_152:                              # %.loopexit
                                        #   in Loop: Header=BB0_132 Depth=2
	cmpb	$2, best8x8pdir+1(,%rbx,4)
	jne	.LBB0_156
# BB#153:                               # %.preheader.1
                                        #   in Loop: Header=BB0_132 Depth=2
	movsbq	best8x8fwref+1(,%rbx,4), %rax
	movq	(%r8), %rcx
	movq	8(%r8), %rsi
	movq	(%rcx,%rax,8), %rcx
	movsbq	best8x8bwref+1(,%rbx,4), %rbp
	movq	(%rcx,%rbp,8), %rdi
	movq	(%rsi,%rax,8), %rax
	movq	(%rax,%rbp,8), %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_154:                              #   Parent Loop BB0_100 Depth=1
                                        #     Parent Loop BB0_132 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdi,%rsi,4), %eax
	movl	(%rbp,%rsi,4), %ecx
	leal	128(%rax,%rcx), %eax
	cmpl	$255, %eax
	ja	.LBB0_125
# BB#155:                               #   in Loop: Header=BB0_154 Depth=3
	incq	%rsi
	xorl	%eax, %eax
	cmpl	$0, 32(%rdx)
	setne	%al
	leaq	1(%rax,%rax), %rax
	cmpq	%rax, %rsi
	jl	.LBB0_154
.LBB0_156:                              # %.loopexit.1
                                        #   in Loop: Header=BB0_132 Depth=2
	cmpb	$2, best8x8pdir+2(,%rbx,4)
	jne	.LBB0_160
# BB#157:                               # %.preheader.2
                                        #   in Loop: Header=BB0_132 Depth=2
	movsbq	best8x8fwref+2(,%rbx,4), %rax
	movq	(%r8), %rcx
	movq	8(%r8), %rsi
	movq	(%rcx,%rax,8), %rcx
	movsbq	best8x8bwref+2(,%rbx,4), %rbp
	movq	(%rcx,%rbp,8), %rdi
	movq	(%rsi,%rax,8), %rax
	movq	(%rax,%rbp,8), %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_158:                              #   Parent Loop BB0_100 Depth=1
                                        #     Parent Loop BB0_132 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdi,%rsi,4), %eax
	movl	(%rbp,%rsi,4), %ecx
	leal	128(%rax,%rcx), %eax
	cmpl	$255, %eax
	ja	.LBB0_125
# BB#159:                               #   in Loop: Header=BB0_158 Depth=3
	incq	%rsi
	xorl	%eax, %eax
	cmpl	$0, 32(%rdx)
	setne	%al
	leaq	1(%rax,%rax), %rax
	cmpq	%rax, %rsi
	jl	.LBB0_158
.LBB0_160:                              # %.loopexit.2
                                        #   in Loop: Header=BB0_132 Depth=2
	cmpb	$2, best8x8pdir+3(,%rbx,4)
	jne	.LBB0_115
# BB#161:                               # %.preheader.3
                                        #   in Loop: Header=BB0_132 Depth=2
	movsbq	best8x8fwref+3(,%rbx,4), %rax
	movq	(%r8), %rcx
	movq	8(%r8), %rdi
	movq	(%rcx,%rax,8), %rcx
	movsbq	best8x8bwref+3(,%rbx,4), %rbp
	movq	(%rcx,%rbp,8), %rsi
	movq	(%rdi,%rax,8), %rax
	movq	(%rax,%rbp,8), %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_162:                              #   Parent Loop BB0_100 Depth=1
                                        #     Parent Loop BB0_132 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rsi,%rbp,4), %eax
	movl	(%rdi,%rbp,4), %ecx
	leal	128(%rax,%rcx), %eax
	cmpl	$255, %eax
	ja	.LBB0_125
# BB#163:                               #   in Loop: Header=BB0_162 Depth=3
	incq	%rbp
	xorl	%eax, %eax
	cmpl	$0, 32(%rdx)
	setne	%al
	leaq	1(%rax,%rax), %rax
	cmpq	%rax, %rbp
	jl	.LBB0_162
	jmp	.LBB0_115
	.p2align	4, 0x90
.LBB0_164:                              #   in Loop: Header=BB0_100 Depth=1
	movq	144(%rsp), %rbx         # 8-byte Reload
	movl	176(%rsp), %edi         # 4-byte Reload
.LBB0_165:                              # %.loopexit499
                                        #   in Loop: Header=BB0_100 Depth=1
	movl	(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rbx)
	cmpl	%edi, %eax
	jl	.LBB0_100
.LBB0_166:                              # %._crit_edge
	cmpl	$2, 20(%r10)
	je	.LBB0_259
# BB#167:
	movq	input(%rip), %rax
	cmpl	$0, 5748(%rax)
	je	.LBB0_259
# BB#168:
	cmpl	$99, (%rax)
	jg	.LBB0_259
# BB#169:
	movsd	344(%rsp), %xmm0        # xmm0 = mem[0],zero
	leaq	310(%rsp), %rdi
	callq	fast_mode_intra_decision
	cmpw	$0, 310(%rsp)
	jne	.LBB0_259
# BB#170:
	movq	img(%rip), %rax
	xorl	%ebx, %ebx
	cmpl	$0, 15536(%rax)
	je	.LBB0_173
# BB#171:
	leaq	332(%rsp), %rdi
	leaq	328(%rsp), %rsi
	leaq	324(%rsp), %rdx
	callq	IntraChromaPrediction
	movq	input(%rip), %rax
	cmpl	$0, 4176(%rax)
	je	.LBB0_174
# BB#172:
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	callq	IntraChromaRDDecision
	movq	144(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %ebx
	movl	%ebx, %ecx
	jmp	.LBB0_176
.LBB0_173:
	xorl	%ecx, %ecx
	jmp	.LBB0_175
.LBB0_174:
	movl	$3, %ecx
.LBB0_175:
	movq	144(%rsp), %rdx         # 8-byte Reload
.LBB0_176:
	movswl	%bx, %eax
	movl	%eax, (%rdx)
	movswl	%cx, %r13d
	cmpl	%r13d, %eax
	jg	.LBB0_259
# BB#177:                               # %.lr.ph538.preheader
	movslq	mb_mode_table+32(%rip), %rbx
	movslq	mb_mode_table+28(%rip), %rcx
	movq	%rcx, 296(%rsp)         # 8-byte Spill
	movslq	mb_mode_table+24(%rip), %rcx
	movq	%rcx, 288(%rsp)         # 8-byte Spill
	movslq	mb_mode_table+20(%rip), %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	leaq	130(%rsp), %r14
	leaq	312(%rsp), %r15
	leaq	344(%rsp), %r12
	.p2align	4, 0x90
.LBB0_178:                              # %.lr.ph538
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_193 Depth 2
	movq	img(%rip), %rcx
	movl	15536(%rcx), %edx
	testl	%edx, %edx
	je	.LBB0_191
# BB#179:                               #   in Loop: Header=BB0_178 Depth=1
	movq	input(%rip), %rcx
	cmpw	$0, 140(%rsp)           # 2-byte Folded Reload
	je	.LBB0_181
# BB#180:                               #   in Loop: Header=BB0_178 Depth=1
	cmpl	$0, 4048(%rcx)
	jne	.LBB0_183
.LBB0_181:                              # %._crit_edge603
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpl	$1, 4072(%rcx)
	jne	.LBB0_183
# BB#182:                               #   in Loop: Header=BB0_178 Depth=1
	testl	%eax, %eax
	jne	.LBB0_258
	jmp	.LBB0_191
.LBB0_183:                              #   in Loop: Header=BB0_178 Depth=1
	cmpl	$2, %eax
	movl	332(%rsp), %ecx
	jne	.LBB0_185
# BB#184:                               #   in Loop: Header=BB0_178 Depth=1
	testl	%ecx, %ecx
	je	.LBB0_258
.LBB0_185:                              #   in Loop: Header=BB0_178 Depth=1
	cmpl	$1, %eax
	movl	328(%rsp), %esi
	jne	.LBB0_187
# BB#186:                               #   in Loop: Header=BB0_178 Depth=1
	testl	%esi, %esi
	je	.LBB0_258
.LBB0_187:                              #   in Loop: Header=BB0_178 Depth=1
	cmpl	$3, %eax
	jne	.LBB0_191
# BB#188:                               #   in Loop: Header=BB0_178 Depth=1
	testl	%ecx, %ecx
	je	.LBB0_258
# BB#189:                               #   in Loop: Header=BB0_178 Depth=1
	testl	%esi, %esi
	je	.LBB0_258
# BB#190:                               #   in Loop: Header=BB0_178 Depth=1
	movl	324(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_258
.LBB0_191:                              # %.thread493.preheader
                                        #   in Loop: Header=BB0_178 Depth=1
	movl	156(%rsp), %esi         # 4-byte Reload
	cmpl	$1, %esi
	jne	.LBB0_203
# BB#192:                               # %.thread493.preheader.split.us.preheader
                                        #   in Loop: Header=BB0_178 Depth=1
	movq	$-16, %rbp
	.p2align	4, 0x90
.LBB0_193:                              # %.thread493.preheader.split.us
                                        #   Parent Loop BB0_178 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpw	$0, 140(%rsp)           # 2-byte Folded Reload
	sete	%al
	movq	input(%rip), %rcx
	cmpl	$0, 2960(%rcx)
	movl	mb_mode_table+36(%rbp), %edi
	setne	%cl
	movswl	best_mode(%rip), %edx
	cmpl	$3, %edx
	jg	.LBB0_197
# BB#194:                               # %.thread493.preheader.split.us
                                        #   in Loop: Header=BB0_193 Depth=2
	cmpl	$10, %edi
	jl	.LBB0_197
# BB#195:                               # %.thread493.preheader.split.us
                                        #   in Loop: Header=BB0_193 Depth=2
	andb	%cl, %al
	je	.LBB0_197
# BB#196:                               #   in Loop: Header=BB0_193 Depth=2
	movq	144(%rsp), %rax         # 8-byte Reload
	cmpl	$0, -52(%rax)
	je	.LBB0_202
.LBB0_197:                              #   in Loop: Header=BB0_193 Depth=2
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB0_200
# BB#198:                               #   in Loop: Header=BB0_193 Depth=2
	cmpl	$1, %edi
	sete	%al
	testl	%edi, %edi
	je	.LBB0_202
# BB#199:                               #   in Loop: Header=BB0_193 Depth=2
	andb	163(%rsp), %al          # 1-byte Folded Reload
	jne	.LBB0_202
.LBB0_200:                              #   in Loop: Header=BB0_193 Depth=2
	movslq	%edi, %rax
	cmpw	$0, 228(%rsp,%rax,2)
	je	.LBB0_202
# BB#201:                               #   in Loop: Header=BB0_193 Depth=2
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movq	%r14, 96(%rsp)
	movzwl	132(%rsp), %r9d         # 2-byte Folded Reload
	xorl	%r8d, %r8d
	movq	168(%rsp), %rsi         # 8-byte Reload
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	compute_mode_RD_cost
.LBB0_202:                              # %.thread493.us
                                        #   in Loop: Header=BB0_193 Depth=2
	addq	$4, %rbp
	jne	.LBB0_193
	jmp	.LBB0_258
.LBB0_203:                              # %.thread493.preheader.split
                                        #   in Loop: Header=BB0_178 Depth=1
	movq	%rbx, %rbp
	cmpw	$0, 140(%rsp)           # 2-byte Folded Reload
	sete	%cl
	movq	input(%rip), %rax
	movl	2960(%rax), %eax
	testl	%eax, %eax
	setne	%bl
	andb	%cl, %bl
	cmpl	$2, %esi
	jne	.LBB0_221
# BB#204:                               # %.thread493.preheader.split.split.preheader
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpl	$9, 176(%rsp)           # 4-byte Folded Reload
	setg	%dl
	movswl	best_mode(%rip), %ecx
	cmpl	$3, %ecx
	jg	.LBB0_207
# BB#205:                               # %.thread493.preheader.split.split.preheader
                                        #   in Loop: Header=BB0_178 Depth=1
	andb	%bl, %dl
	je	.LBB0_207
# BB#206:                               #   in Loop: Header=BB0_178 Depth=1
	movq	144(%rsp), %rdx         # 8-byte Reload
	cmpl	$0, -52(%rdx)
	je	.LBB0_209
.LBB0_207:                              #   in Loop: Header=BB0_178 Depth=1
	movq	176(%rsp), %rdx         # 8-byte Reload
	cmpw	$0, 228(%rsp,%rdx,2)
	je	.LBB0_209
# BB#208:                               #   in Loop: Header=BB0_178 Depth=1
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movq	%r14, 96(%rsp)
	movzwl	132(%rsp), %r9d         # 2-byte Folded Reload
	xorl	%r8d, %r8d
	movq	176(%rsp), %rdi         # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	168(%rsp), %rsi         # 8-byte Reload
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	compute_mode_RD_cost
	movq	input(%rip), %rax
	movl	2960(%rax), %eax
	movw	best_mode(%rip), %cx
.LBB0_209:                              # %.thread493
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpw	$0, 140(%rsp)           # 2-byte Folded Reload
	sete	%dl
	testl	%eax, %eax
	setne	%bl
	movswl	%cx, %esi
	cmpl	$3, %esi
	jg	.LBB0_213
# BB#210:                               # %.thread493
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpl	$10, 288(%rsp)          # 4-byte Folded Reload
	jl	.LBB0_213
# BB#211:                               # %.thread493
                                        #   in Loop: Header=BB0_178 Depth=1
	andb	%bl, %dl
	je	.LBB0_213
# BB#212:                               #   in Loop: Header=BB0_178 Depth=1
	movq	144(%rsp), %rdx         # 8-byte Reload
	cmpl	$0, -52(%rdx)
	je	.LBB0_215
.LBB0_213:                              #   in Loop: Header=BB0_178 Depth=1
	movq	288(%rsp), %rdx         # 8-byte Reload
	cmpw	$0, 228(%rsp,%rdx,2)
	je	.LBB0_215
# BB#214:                               #   in Loop: Header=BB0_178 Depth=1
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movq	%r14, 96(%rsp)
	movzwl	132(%rsp), %r9d         # 2-byte Folded Reload
	xorl	%r8d, %r8d
	movq	288(%rsp), %rdi         # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	168(%rsp), %rsi         # 8-byte Reload
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	compute_mode_RD_cost
	movq	input(%rip), %rax
	movl	2960(%rax), %eax
	movw	best_mode(%rip), %cx
.LBB0_215:                              # %.thread493.1
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpw	$0, 140(%rsp)           # 2-byte Folded Reload
	sete	%dl
	testl	%eax, %eax
	setne	%bl
	movswl	%cx, %esi
	cmpl	$3, %esi
	jg	.LBB0_219
# BB#216:                               # %.thread493.1
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpl	$10, 296(%rsp)          # 4-byte Folded Reload
	jl	.LBB0_219
# BB#217:                               # %.thread493.1
                                        #   in Loop: Header=BB0_178 Depth=1
	andb	%bl, %dl
	je	.LBB0_219
# BB#218:                               #   in Loop: Header=BB0_178 Depth=1
	movq	144(%rsp), %rdx         # 8-byte Reload
	cmpl	$0, -52(%rdx)
	je	.LBB0_251
.LBB0_219:                              #   in Loop: Header=BB0_178 Depth=1
	movq	296(%rsp), %rdx         # 8-byte Reload
	cmpw	$0, 228(%rsp,%rdx,2)
	movq	%rbp, %rbx
	je	.LBB0_252
# BB#220:                               #   in Loop: Header=BB0_178 Depth=1
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movq	%r14, 96(%rsp)
	movzwl	132(%rsp), %r9d         # 2-byte Folded Reload
	xorl	%r8d, %r8d
	movq	296(%rsp), %rdi         # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	168(%rsp), %rsi         # 8-byte Reload
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	compute_mode_RD_cost
	movq	input(%rip), %rax
	movl	2960(%rax), %eax
	movw	best_mode(%rip), %cx
	jmp	.LBB0_252
.LBB0_221:                              # %.thread493.preheader.split.split.us.preheader
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpl	$9, 176(%rsp)           # 4-byte Folded Reload
	setg	%cl
	movswl	best_mode(%rip), %esi
	cmpl	$3, %esi
	jg	.LBB0_224
# BB#222:                               # %.thread493.preheader.split.split.us.preheader
                                        #   in Loop: Header=BB0_178 Depth=1
	andb	%bl, %cl
	je	.LBB0_224
# BB#223:                               #   in Loop: Header=BB0_178 Depth=1
	movq	144(%rsp), %rcx         # 8-byte Reload
	cmpl	$0, -52(%rcx)
	jne	.LBB0_226
	jmp	.LBB0_228
.LBB0_224:                              #   in Loop: Header=BB0_178 Depth=1
	testl	%edx, %edx
	je	.LBB0_226
# BB#225:                               #   in Loop: Header=BB0_178 Depth=1
	cmpl	$1, 176(%rsp)           # 4-byte Folded Reload
	je	.LBB0_228
.LBB0_226:                              # %.thread634
                                        #   in Loop: Header=BB0_178 Depth=1
	movq	176(%rsp), %rcx         # 8-byte Reload
	cmpw	$0, 228(%rsp,%rcx,2)
	je	.LBB0_228
# BB#227:                               #   in Loop: Header=BB0_178 Depth=1
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movq	%r14, 96(%rsp)
	movzwl	132(%rsp), %r9d         # 2-byte Folded Reload
	xorl	%r8d, %r8d
	movq	176(%rsp), %rdi         # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	168(%rsp), %rsi         # 8-byte Reload
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	compute_mode_RD_cost
	movq	input(%rip), %rax
	movl	2960(%rax), %eax
	movw	best_mode(%rip), %si
.LBB0_228:                              # %.thread493.us534
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpw	$0, 140(%rsp)           # 2-byte Folded Reload
	sete	%dl
	testl	%eax, %eax
	setne	%bl
	movswl	%si, %ecx
	cmpl	$3, %ecx
	jg	.LBB0_232
# BB#229:                               # %.thread493.us534
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpl	$10, 288(%rsp)          # 4-byte Folded Reload
	jl	.LBB0_232
# BB#230:                               # %.thread493.us534
                                        #   in Loop: Header=BB0_178 Depth=1
	andb	%bl, %dl
	je	.LBB0_232
# BB#231:                               #   in Loop: Header=BB0_178 Depth=1
	movq	144(%rsp), %rcx         # 8-byte Reload
	cmpl	$0, -52(%rcx)
	jne	.LBB0_234
	jmp	.LBB0_236
.LBB0_232:                              #   in Loop: Header=BB0_178 Depth=1
	movq	img(%rip), %rcx
	cmpl	$0, 15536(%rcx)
	je	.LBB0_234
# BB#233:                               #   in Loop: Header=BB0_178 Depth=1
	cmpl	$1, 288(%rsp)           # 4-byte Folded Reload
	je	.LBB0_236
.LBB0_234:                              # %.thread637
                                        #   in Loop: Header=BB0_178 Depth=1
	movq	288(%rsp), %rcx         # 8-byte Reload
	cmpw	$0, 228(%rsp,%rcx,2)
	je	.LBB0_236
# BB#235:                               #   in Loop: Header=BB0_178 Depth=1
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movq	%r14, 96(%rsp)
	movzwl	132(%rsp), %r9d         # 2-byte Folded Reload
	xorl	%r8d, %r8d
	movq	288(%rsp), %rdi         # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	168(%rsp), %rsi         # 8-byte Reload
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	compute_mode_RD_cost
	movq	input(%rip), %rax
	movl	2960(%rax), %eax
	movw	best_mode(%rip), %si
.LBB0_236:                              # %.thread493.us534.1
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpw	$0, 140(%rsp)           # 2-byte Folded Reload
	sete	%dl
	testl	%eax, %eax
	setne	%bl
	movswl	%si, %ecx
	cmpl	$3, %ecx
	jg	.LBB0_241
# BB#237:                               # %.thread493.us534.1
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpl	$10, 296(%rsp)          # 4-byte Folded Reload
	jl	.LBB0_241
# BB#238:                               # %.thread493.us534.1
                                        #   in Loop: Header=BB0_178 Depth=1
	andb	%bl, %dl
	je	.LBB0_241
# BB#239:                               #   in Loop: Header=BB0_178 Depth=1
	movq	144(%rsp), %rcx         # 8-byte Reload
	cmpl	$0, -52(%rcx)
	jne	.LBB0_244
# BB#240:                               #   in Loop: Header=BB0_178 Depth=1
	movq	%rbp, %rbx
	jmp	.LBB0_246
.LBB0_241:                              #   in Loop: Header=BB0_178 Depth=1
	movq	img(%rip), %rcx
	cmpl	$0, 15536(%rcx)
	je	.LBB0_244
# BB#242:                               #   in Loop: Header=BB0_178 Depth=1
	cmpl	$1, 296(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_244
# BB#243:                               #   in Loop: Header=BB0_178 Depth=1
	movq	%rbp, %rbx
	jmp	.LBB0_246
.LBB0_244:                              # %.thread639
                                        #   in Loop: Header=BB0_178 Depth=1
	movq	296(%rsp), %rcx         # 8-byte Reload
	cmpw	$0, 228(%rsp,%rcx,2)
	movq	%rbp, %rbx
	je	.LBB0_246
# BB#245:                               #   in Loop: Header=BB0_178 Depth=1
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movq	%r14, 96(%rsp)
	movzwl	132(%rsp), %r9d         # 2-byte Folded Reload
	xorl	%r8d, %r8d
	movq	296(%rsp), %rdi         # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	168(%rsp), %rsi         # 8-byte Reload
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	compute_mode_RD_cost
	movq	input(%rip), %rax
	movl	2960(%rax), %eax
	movw	best_mode(%rip), %si
.LBB0_246:                              # %.thread493.us534.2
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpw	$0, 140(%rsp)           # 2-byte Folded Reload
	sete	%dl
	testl	%eax, %eax
	setne	%al
	movswl	%si, %ecx
	cmpl	$3, %ecx
	jg	.LBB0_249
# BB#247:                               # %.thread493.us534.2
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpl	$10, %ebx
	jl	.LBB0_249
# BB#248:                               # %.thread493.us534.2
                                        #   in Loop: Header=BB0_178 Depth=1
	andb	%al, %dl
	jne	.LBB0_255
.LBB0_249:                              #   in Loop: Header=BB0_178 Depth=1
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB0_256
# BB#250:                               #   in Loop: Header=BB0_178 Depth=1
	cmpl	$1, %ebx
	je	.LBB0_258
	jmp	.LBB0_256
.LBB0_251:                              #   in Loop: Header=BB0_178 Depth=1
	movq	%rbp, %rbx
.LBB0_252:                              # %.thread493.2
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpw	$0, 140(%rsp)           # 2-byte Folded Reload
	sete	%dl
	testl	%eax, %eax
	setne	%al
	movswl	%cx, %ecx
	cmpl	$3, %ecx
	jg	.LBB0_256
# BB#253:                               # %.thread493.2
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpl	$10, %ebx
	jl	.LBB0_256
# BB#254:                               # %.thread493.2
                                        #   in Loop: Header=BB0_178 Depth=1
	andb	%al, %dl
	je	.LBB0_256
.LBB0_255:                              #   in Loop: Header=BB0_178 Depth=1
	movq	144(%rsp), %rax         # 8-byte Reload
	cmpl	$0, -52(%rax)
	je	.LBB0_258
.LBB0_256:                              # %.thread641
                                        #   in Loop: Header=BB0_178 Depth=1
	cmpw	$0, 228(%rsp,%rbx,2)
	je	.LBB0_258
# BB#257:                               #   in Loop: Header=BB0_178 Depth=1
	movups	264(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	248(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	184(%rsp), %xmm0
	movupd	200(%rsp), %xmm1
	movupd	216(%rsp), %xmm2
	movups	232(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movupd	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movq	%r14, 96(%rsp)
	movzwl	132(%rsp), %r9d         # 2-byte Folded Reload
	xorl	%r8d, %r8d
	movl	%ebx, %edi
	movq	168(%rsp), %rsi         # 8-byte Reload
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	compute_mode_RD_cost
	.p2align	4, 0x90
.LBB0_258:                              # %.loopexit498
                                        #   in Loop: Header=BB0_178 Depth=1
	movq	144(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %ecx
	leal	1(%rcx), %eax
	movl	%eax, (%rdx)
	cmpl	%r13d, %ecx
	jl	.LBB0_178
.LBB0_259:                              # %.loopexit501
	movl	320(%rsp), %r13d        # 4-byte Reload
	movq	376(%rsp), %r12         # 8-byte Reload
.LBB0_260:
	movl	(%r12), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	sbbb	%r14b, %r14b
	movb	$51, %r15b
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %r15b
	movl	cbp(%rip), %ecx
	movzwl	best_mode(%rip), %eax
	testl	%ecx, %ecx
	sete	%dl
	cmpl	$10, %eax
	setne	%bl
	cmpl	$14, %eax
	je	.LBB0_262
# BB#261:
	andb	%bl, %dl
	movl	$1, %edx
	je	.LBB0_267
.LBB0_262:
	movzwl	%ax, %eax
	testl	%ecx, %ecx
	je	.LBB0_264
# BB#263:
	cmpl	$14, %eax
	je	.LBB0_266
	jmp	.LBB0_268
.LBB0_264:
	cmpl	$14, %eax
	je	.LBB0_266
# BB#265:
	movq	input(%rip), %rax
	movl	5116(%rax), %eax
	testl	%eax, %eax
	jne	.LBB0_268
.LBB0_266:
	movq	360(%rsp), %rbx         # 8-byte Reload
	movq	336(%rsp), %rbp         # 8-byte Reload
	movl	$0, 4(%rbx,%rbp)
	movl	496(%rbx,%rbp), %eax
	movl	%eax, 8(%rbx,%rbp)
	movq	168(%rsp), %rdi         # 8-byte Reload
	callq	set_chroma_qp
	movl	8(%rbx,%rbp), %eax
	movq	img(%rip), %rcx
	movl	%eax, 36(%rcx)
	xorl	%edx, %edx
.LBB0_267:                              # %.sink.split
	movq	360(%rsp), %rax         # 8-byte Reload
	movq	336(%rsp), %rcx         # 8-byte Reload
	movl	%edx, 504(%rax,%rcx)
.LBB0_268:
	callq	set_stored_macroblock_parameters
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB0_270
# BB#269:
	movswl	best_mode(%rip), %esi
	movq	168(%rsp), %rdi         # 8-byte Reload
	callq	update_rc
.LBB0_270:
	movq	312(%rsp), %rax
	movq	rdopt(%rip), %rcx
	movq	%rax, (%rcx)
	movq	img(%rip), %rax
	cmpl	$0, 15268(%rax)
	je	.LBB0_276
# BB#271:
	testb	$1, 12(%rax)
	je	.LBB0_276
# BB#272:
	cmpl	$0, (%r12)
	jne	.LBB0_276
# BB#273:
	cmpl	$1, 156(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_275
# BB#274:
	movq	144(%rsp), %rax         # 8-byte Reload
	cmpl	$0, -52(%rax)
	jne	.LBB0_276
.LBB0_275:
	movq	368(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 72(%rax)
	je	.LBB0_286
.LBB0_276:
	movq	input(%rip), %rax
	cmpl	$0, 4732(%rax)
	je	.LBB0_278
# BB#277:
	andb	%r15b, %r14b
	andb	$1, %r14b
	movzbl	%r14b, %esi
	movl	%r13d, %edi
	movq	168(%rsp), %rdx         # 8-byte Reload
	callq	update_refresh_map
	movq	input(%rip), %rax
.LBB0_278:
	movl	5244(%rax), %eax
	cmpl	$2, %eax
	je	.LBB0_281
# BB#279:
	cmpl	$1, %eax
	jne	.LBB0_282
# BB#280:
	movswl	best_mode(%rip), %edi
	movswq	258(%rsp), %rax
	movl	listXsize(,%rax,4), %esi
	callq	UMHEX_skip_intrabk_SAD
	jmp	.LBB0_282
.LBB0_281:
	movswl	best_mode(%rip), %edi
	movswq	258(%rsp), %rax
	movl	listXsize(,%rax,4), %esi
	callq	smpUMHEX_skip_intrabk_SAD
.LBB0_282:
	movq	input(%rip), %rax
	cmpl	$0, 272(%rax)
	je	.LBB0_285
# BB#283:
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	ja	.LBB0_285
# BB#284:
	movl	(%r12), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	sbbb	%dl, %dl
	movb	$51, %bl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %bl
	andb	%dl, %bl
	andb	$1, %bl
	movzbl	%bl, %ecx
	movq	14240(%rax), %rdx
	movslq	12(%rax), %rax
	movl	%ecx, (%rdx,%rax,4)
.LBB0_285:
	addq	$440, %rsp              # imm = 0x1B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_286:
	cmpl	$1, 156(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_288
# BB#287:
	movq	368(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 364(%rax)
	jne	.LBB0_276
.LBB0_288:
	callq	field_flag_inference
	movswl	262(%rsp), %ecx
	cmpl	%ecx, %eax
	je	.LBB0_276
# BB#289:
	movq	rdopt(%rip), %rax
	movabsq	$5055640609639927018, %rcx # imm = 0x46293E5939A08CEA
	movq	%rcx, (%rax)
	jmp	.LBB0_276
.Lfunc_end0:
	.size	encode_one_macroblock_highfast, .Lfunc_end0-encode_one_macroblock_highfast
	.cfi_endproc

	.type	.Lencode_one_macroblock_highfast.bmcost,@object # @encode_one_macroblock_highfast.bmcost
	.section	.rodata,"a",@progbits
	.p2align	4
.Lencode_one_macroblock_highfast.bmcost:
	.long	2147483647              # 0x7fffffff
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	.Lencode_one_macroblock_highfast.bmcost, 20

	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	diffy,@object           # @diffy
	.comm	diffy,1024,16
	.type	qp_mbaff,@object        # @qp_mbaff
	.comm	qp_mbaff,16,16
	.type	delta_qp_mbaff,@object  # @delta_qp_mbaff
	.comm	delta_qp_mbaff,16,16
	.type	generic_RC,@object      # @generic_RC
	.comm	generic_RC,8,8
	.type	generic_RC_init,@object # @generic_RC_init
	.comm	generic_RC_init,8,8
	.type	generic_RC_best,@object # @generic_RC_best
	.comm	generic_RC_best,8,8
	.type	McostState,@object      # @McostState
	.comm	McostState,8,8
	.type	SearchState,@object     # @SearchState
	.comm	SearchState,8,8
	.type	fastme_ref_cost,@object # @fastme_ref_cost
	.comm	fastme_ref_cost,8,8
	.type	fastme_l0_cost,@object  # @fastme_l0_cost
	.comm	fastme_l0_cost,8,8
	.type	fastme_l1_cost,@object  # @fastme_l1_cost
	.comm	fastme_l1_cost,8,8
	.type	fastme_l0_cost_bipred,@object # @fastme_l0_cost_bipred
	.comm	fastme_l0_cost_bipred,8,8
	.type	fastme_l1_cost_bipred,@object # @fastme_l1_cost_bipred
	.comm	fastme_l1_cost_bipred,8,8
	.type	bipred_flag,@object     # @bipred_flag
	.comm	bipred_flag,4,4
	.type	fastme_best_cost,@object # @fastme_best_cost
	.comm	fastme_best_cost,8,8
	.type	pred_SAD,@object        # @pred_SAD
	.comm	pred_SAD,4,4
	.type	pred_MV_ref,@object     # @pred_MV_ref
	.comm	pred_MV_ref,8,4
	.type	pred_MV_uplayer,@object # @pred_MV_uplayer
	.comm	pred_MV_uplayer,8,4
	.type	UMHEX_blocktype,@object # @UMHEX_blocktype
	.comm	UMHEX_blocktype,4,4
	.type	predict_point,@object   # @predict_point
	.comm	predict_point,40,16
	.type	SAD_a,@object           # @SAD_a
	.comm	SAD_a,4,4
	.type	SAD_b,@object           # @SAD_b
	.comm	SAD_b,4,4
	.type	SAD_c,@object           # @SAD_c
	.comm	SAD_c,4,4
	.type	SAD_d,@object           # @SAD_d
	.comm	SAD_d,4,4
	.type	Threshold_DSR_MB,@object # @Threshold_DSR_MB
	.comm	Threshold_DSR_MB,32,16
	.type	Bsize,@object           # @Bsize
	.comm	Bsize,32,16
	.type	AlphaFourth_1,@object   # @AlphaFourth_1
	.comm	AlphaFourth_1,32,16
	.type	AlphaFourth_2,@object   # @AlphaFourth_2
	.comm	AlphaFourth_2,32,16
	.type	flag_intra,@object      # @flag_intra
	.comm	flag_intra,8,8
	.type	flag_intra_SAD,@object  # @flag_intra_SAD
	.comm	flag_intra_SAD,4,4
	.type	SymmetricalCrossSearchThreshold1,@object # @SymmetricalCrossSearchThreshold1
	.comm	SymmetricalCrossSearchThreshold1,2,2
	.type	SymmetricalCrossSearchThreshold2,@object # @SymmetricalCrossSearchThreshold2
	.comm	SymmetricalCrossSearchThreshold2,2,2
	.type	ConvergeThreshold,@object # @ConvergeThreshold
	.comm	ConvergeThreshold,2,2
	.type	SubPelThreshold1,@object # @SubPelThreshold1
	.comm	SubPelThreshold1,2,2
	.type	SubPelThreshold3,@object # @SubPelThreshold3
	.comm	SubPelThreshold3,2,2
	.type	smpUMHEX_SearchState,@object # @smpUMHEX_SearchState
	.comm	smpUMHEX_SearchState,8,8
	.type	smpUMHEX_l0_cost,@object # @smpUMHEX_l0_cost
	.comm	smpUMHEX_l0_cost,8,8
	.type	smpUMHEX_l1_cost,@object # @smpUMHEX_l1_cost
	.comm	smpUMHEX_l1_cost,8,8
	.type	smpUMHEX_flag_intra,@object # @smpUMHEX_flag_intra
	.comm	smpUMHEX_flag_intra,8,8
	.type	smpUMHEX_flag_intra_SAD,@object # @smpUMHEX_flag_intra_SAD
	.comm	smpUMHEX_flag_intra_SAD,4,4
	.type	smpUMHEX_pred_SAD_uplayer,@object # @smpUMHEX_pred_SAD_uplayer
	.comm	smpUMHEX_pred_SAD_uplayer,4,4
	.type	smpUMHEX_pred_MV_uplayer_X,@object # @smpUMHEX_pred_MV_uplayer_X
	.comm	smpUMHEX_pred_MV_uplayer_X,2,2
	.type	smpUMHEX_pred_MV_uplayer_Y,@object # @smpUMHEX_pred_MV_uplayer_Y
	.comm	smpUMHEX_pred_MV_uplayer_Y,2,2

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
