	.text
	.file	"id.bc"
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"null"
	.size	.L.str, 5

	.type	idnull,@object          # @idnull
	.data
	.globl	idnull
	.p2align	3
idnull:
	.quad	.L.str
	.long	8                       # 0x8
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	idnull, 40

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"-1"
	.size	.L.str.1, 3

	.type	idneg1,@object          # @idneg1
	.data
	.globl	idneg1
	.p2align	3
idneg1:
	.quad	.L.str.1
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	idneg1, 40

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"0"
	.size	.L.str.2, 2

	.type	id0i,@object            # @id0i
	.data
	.globl	id0i
	.p2align	3
id0i:
	.quad	.L.str.2
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	id0i, 40

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"1"
	.size	.L.str.3, 2

	.type	id1i,@object            # @id1i
	.data
	.globl	id1i
	.p2align	3
id1i:
	.quad	.L.str.3
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	id1i, 40

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"2"
	.size	.L.str.4, 2

	.type	id2i,@object            # @id2i
	.data
	.globl	id2i
	.p2align	3
id2i:
	.quad	.L.str.4
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	id2i, 40

	.type	.L.str.5,@object        # @.str.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.5:
	.asciz	"3"
	.size	.L.str.5, 2

	.type	id3i,@object            # @id3i
	.data
	.globl	id3i
	.p2align	3
id3i:
	.quad	.L.str.5
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	id3i, 40

	.type	.L.str.6,@object        # @.str.6
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.6:
	.asciz	"4"
	.size	.L.str.6, 2

	.type	id4i,@object            # @id4i
	.data
	.globl	id4i
	.p2align	3
id4i:
	.quad	.L.str.6
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	id4i, 40

	.type	.L.str.7,@object        # @.str.7
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.7:
	.asciz	"5"
	.size	.L.str.7, 2

	.type	id5i,@object            # @id5i
	.data
	.globl	id5i
	.p2align	3
id5i:
	.quad	.L.str.7
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	id5i, 40

	.type	.L.str.8,@object        # @.str.8
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.8:
	.asciz	"0L"
	.size	.L.str.8, 3

	.type	id0L,@object            # @id0L
	.data
	.globl	id0L
	.p2align	3
id0L:
	.quad	.L.str.8
	.long	5                       # 0x5
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	id0L, 40

	.type	.L.str.9,@object        # @.str.9
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.9:
	.asciz	"1L"
	.size	.L.str.9, 3

	.type	id1L,@object            # @id1L
	.data
	.globl	id1L
	.p2align	3
id1L:
	.quad	.L.str.9
	.long	5                       # 0x5
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	id1L, 40

	.type	.L.str.10,@object       # @.str.10
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.10:
	.asciz	"0.0f"
	.size	.L.str.10, 5

	.type	id0f,@object            # @id0f
	.data
	.globl	id0f
	.p2align	3
id0f:
	.quad	.L.str.10
	.long	6                       # 0x6
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	id0f, 40

	.type	.L.str.11,@object       # @.str.11
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.11:
	.asciz	"1.0f"
	.size	.L.str.11, 5

	.type	id1f,@object            # @id1f
	.data
	.globl	id1f
	.p2align	3
id1f:
	.quad	.L.str.11
	.long	6                       # 0x6
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	id1f, 40

	.type	.L.str.12,@object       # @.str.12
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.12:
	.asciz	"2.0f"
	.size	.L.str.12, 5

	.type	id2f,@object            # @id2f
	.data
	.globl	id2f
	.p2align	3
id2f:
	.quad	.L.str.12
	.long	6                       # 0x6
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	id2f, 40

	.type	.L.str.13,@object       # @.str.13
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.13:
	.asciz	"0.0d"
	.size	.L.str.13, 5

	.type	id0d,@object            # @id0d
	.data
	.globl	id0d
	.p2align	3
id0d:
	.quad	.L.str.13
	.long	7                       # 0x7
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	id0d, 40

	.type	.L.str.14,@object       # @.str.14
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.14:
	.asciz	"1.0d"
	.size	.L.str.14, 5

	.type	id1d,@object            # @id1d
	.data
	.globl	id1d
	.p2align	3
id1d:
	.quad	.L.str.14
	.long	7                       # 0x7
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	id1d, 40

	.type	.L.str.15,@object       # @.str.15
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.15:
	.asciz	"false"
	.size	.L.str.15, 6

	.type	idfalse,@object         # @idfalse
	.data
	.globl	idfalse
	.p2align	3
idfalse:
	.quad	.L.str.15
	.long	10                      # 0xa
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	idfalse, 40

	.type	.L.str.16,@object       # @.str.16
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.16:
	.asciz	"true"
	.size	.L.str.16, 5

	.type	idtrue,@object          # @idtrue
	.data
	.globl	idtrue
	.p2align	3
idtrue:
	.quad	.L.str.16
	.long	10                      # 0xa
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.zero	16
	.size	idtrue, 40


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
