	.text
	.file	"gutil.bc"
	.globl	_ZN2kc25f_something_to_initializeEPNS_17impl_Ccode_optionE
	.p2align	4, 0x90
	.type	_ZN2kc25f_something_to_initializeEPNS_17impl_Ccode_optionE,@function
_ZN2kc25f_something_to_initializeEPNS_17impl_Ccode_optionE: # @_ZN2kc25f_something_to_initializeEPNS_17impl_Ccode_optionE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$31, %eax
	jne	.LBB0_12
# BB#1:
	movq	8(%rbx), %r15
	movq	16(%rbx), %r14
	jmp	.LBB0_2
	.p2align	4, 0x90
.LBB0_15:                               # %.thread31.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	16(%r15), %r15
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$33, %eax
	jne	.LBB0_9
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	8(%r15), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$34, %eax
	jne	.LBB0_6
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$36, %eax
	je	.LBB0_5
.LBB0_6:                                #   in Loop: Header=BB0_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$34, %eax
	jne	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$35, %eax
	je	.LBB0_15
.LBB0_8:
	movl	$.L.str.28, %edi
	movl	$148, %esi
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB0_9:                                # %_ZN2kcL26f_attributes_to_initializeEPNS_15impl_attributesE.exit.thread
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	movb	$1, %bl
	cmpl	$58, %eax
	je	.LBB0_14
# BB#10:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$57, %eax
	je	.LBB0_13
# BB#11:
	movl	$.L.str.2, %edi
	movl	$118, %esi
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	jmp	.LBB0_14
.LBB0_12:
	movl	$.L.str, %edi
	movl	$96, %esi
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB0_13:                               # %_ZN2kcL26f_attributes_to_initializeEPNS_15impl_attributesE.exit
	xorl	%ebx, %ebx
.LBB0_14:                               # %_ZN2kcL26f_attributes_to_initializeEPNS_15impl_attributesE.exit
	movl	%ebx, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB0_5:
	movb	$1, %bl
	jmp	.LBB0_14
.Lfunc_end0:
	.size	_ZN2kc25f_something_to_initializeEPNS_17impl_Ccode_optionE, .Lfunc_end0-_ZN2kc25f_something_to_initializeEPNS_17impl_Ccode_optionE
	.cfi_endproc

	.globl	_ZN2kc11f_NilCtextsEPNS_11impl_CtextsE
	.p2align	4, 0x90
	.type	_ZN2kc11f_NilCtextsEPNS_11impl_CtextsE,@function
_ZN2kc11f_NilCtextsEPNS_11impl_CtextsE: # @_ZN2kc11f_NilCtextsEPNS_11impl_CtextsE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$58, %eax
	je	.LBB1_3
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movl	%eax, %ecx
	movb	$1, %al
	cmpl	$57, %ecx
	je	.LBB1_4
# BB#2:
	movl	$.L.str.2, %edi
	movl	$118, %esi
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB1_3:
	xorl	%eax, %eax
.LBB1_4:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN2kc11f_NilCtextsEPNS_11impl_CtextsE, .Lfunc_end1-_ZN2kc11f_NilCtextsEPNS_11impl_CtextsE
	.cfi_endproc

	.globl	_ZN2kc25f_something_to_initializeEPNS_16impl_alternativeE
	.p2align	4, 0x90
	.type	_ZN2kc25f_something_to_initializeEPNS_16impl_alternativeE,@function
_ZN2kc25f_something_to_initializeEPNS_16impl_alternativeE: # @_ZN2kc25f_something_to_initializeEPNS_16impl_alternativeE
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	_ZN2kc25f_something_to_initializeEPNS_16impl_alternativeE, .Lfunc_end2-_ZN2kc25f_something_to_initializeEPNS_16impl_alternativeE
	.cfi_endproc

	.globl	_ZN2kc30f_constructors_in_operatordeclEPNS_16impl_alternativeE
	.p2align	4, 0x90
	.type	_ZN2kc30f_constructors_in_operatordeclEPNS_16impl_alternativeE,@function
_ZN2kc30f_constructors_in_operatordeclEPNS_16impl_alternativeE: # @_ZN2kc30f_constructors_in_operatordeclEPNS_16impl_alternativeE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	addq	$24, %rbx
	.p2align	4, 0x90
.LBB3_1:                                # %tailrecurse.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$108, %eax
	jne	.LBB3_5
# BB#2:                                 #   in Loop: Header=BB3_1 Depth=1
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$109, %eax
	jne	.LBB3_5
# BB#3:                                 #   in Loop: Header=BB3_1 Depth=1
	movq	8(%rbx), %rax
	movq	88(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$114, %eax
	je	.LBB3_4
.LBB3_5:                                #   in Loop: Header=BB3_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	addq	$16, %rbx
	cmpl	$108, %eax
	je	.LBB3_1
# BB#6:
	xorl	%eax, %eax
	jmp	.LBB3_7
.LBB3_4:
	movb	$1, %al
.LBB3_7:                                # %_ZN2kcL25f_constructors_in_membersEPNS_19impl_fndeclarationsE.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end3:
	.size	_ZN2kc30f_constructors_in_operatordeclEPNS_16impl_alternativeE, .Lfunc_end3-_ZN2kc30f_constructors_in_operatordeclEPNS_16impl_alternativeE
	.cfi_endproc

	.globl	_ZN2kc28f_constructors_in_phylumdeclEPNS_22impl_phylumdeclarationE
	.p2align	4, 0x90
	.type	_ZN2kc28f_constructors_in_phylumdeclEPNS_22impl_phylumdeclarationE,@function
_ZN2kc28f_constructors_in_phylumdeclEPNS_22impl_phylumdeclarationE: # @_ZN2kc28f_constructors_in_phylumdeclEPNS_22impl_phylumdeclarationE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	addq	$16, %rbx
	.p2align	4, 0x90
.LBB4_1:                                # %tailrecurse.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$108, %eax
	jne	.LBB4_5
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$109, %eax
	jne	.LBB4_5
# BB#3:                                 #   in Loop: Header=BB4_1 Depth=1
	movq	8(%rbx), %rax
	movq	88(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$114, %eax
	je	.LBB4_4
.LBB4_5:                                #   in Loop: Header=BB4_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	addq	$16, %rbx
	cmpl	$108, %eax
	je	.LBB4_1
# BB#6:
	xorl	%eax, %eax
	jmp	.LBB4_7
.LBB4_4:
	movb	$1, %al
.LBB4_7:                                # %_ZN2kcL25f_constructors_in_membersEPNS_19impl_fndeclarationsE.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZN2kc28f_constructors_in_phylumdeclEPNS_22impl_phylumdeclarationE, .Lfunc_end4-_ZN2kc28f_constructors_in_phylumdeclEPNS_22impl_phylumdeclarationE
	.cfi_endproc

	.globl	_ZN2kc29f_destructors_in_operatordeclEPNS_16impl_alternativeE
	.p2align	4, 0x90
	.type	_ZN2kc29f_destructors_in_operatordeclEPNS_16impl_alternativeE,@function
_ZN2kc29f_destructors_in_operatordeclEPNS_16impl_alternativeE: # @_ZN2kc29f_destructors_in_operatordeclEPNS_16impl_alternativeE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	addq	$24, %rbx
	.p2align	4, 0x90
.LBB5_1:                                # %tailrecurse.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$108, %eax
	jne	.LBB5_5
# BB#2:                                 #   in Loop: Header=BB5_1 Depth=1
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$109, %eax
	jne	.LBB5_5
# BB#3:                                 #   in Loop: Header=BB5_1 Depth=1
	movq	8(%rbx), %rax
	movq	88(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$115, %eax
	je	.LBB5_4
.LBB5_5:                                #   in Loop: Header=BB5_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	addq	$16, %rbx
	cmpl	$108, %eax
	je	.LBB5_1
# BB#6:
	xorl	%eax, %eax
	jmp	.LBB5_7
.LBB5_4:
	movb	$1, %al
.LBB5_7:                                # %_ZN2kcL24f_destructors_in_membersEPNS_19impl_fndeclarationsE.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_ZN2kc29f_destructors_in_operatordeclEPNS_16impl_alternativeE, .Lfunc_end5-_ZN2kc29f_destructors_in_operatordeclEPNS_16impl_alternativeE
	.cfi_endproc

	.globl	_ZN2kc27f_destructors_in_phylumdeclEPNS_22impl_phylumdeclarationE
	.p2align	4, 0x90
	.type	_ZN2kc27f_destructors_in_phylumdeclEPNS_22impl_phylumdeclarationE,@function
_ZN2kc27f_destructors_in_phylumdeclEPNS_22impl_phylumdeclarationE: # @_ZN2kc27f_destructors_in_phylumdeclEPNS_22impl_phylumdeclarationE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
.Lcfi15:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	addq	$16, %rbx
	.p2align	4, 0x90
.LBB6_1:                                # %tailrecurse.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$108, %eax
	jne	.LBB6_5
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$109, %eax
	jne	.LBB6_5
# BB#3:                                 #   in Loop: Header=BB6_1 Depth=1
	movq	8(%rbx), %rax
	movq	88(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$115, %eax
	je	.LBB6_4
.LBB6_5:                                #   in Loop: Header=BB6_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	addq	$16, %rbx
	cmpl	$108, %eax
	je	.LBB6_1
# BB#6:
	xorl	%eax, %eax
	jmp	.LBB6_7
.LBB6_4:
	movb	$1, %al
.LBB6_7:                                # %_ZN2kcL24f_destructors_in_membersEPNS_19impl_fndeclarationsE.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end6:
	.size	_ZN2kc27f_destructors_in_phylumdeclEPNS_22impl_phylumdeclarationE, .Lfunc_end6-_ZN2kc27f_destructors_in_phylumdeclEPNS_22impl_phylumdeclarationE
	.cfi_endproc

	.globl	_ZN2kc11f_no_paramsEPNS_27impl_ac_parameter_type_listE
	.p2align	4, 0x90
	.type	_ZN2kc11f_no_paramsEPNS_27impl_ac_parameter_type_listE,@function
_ZN2kc11f_no_paramsEPNS_27impl_ac_parameter_type_listE: # @_ZN2kc11f_no_paramsEPNS_27impl_ac_parameter_type_listE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$276, %eax              # imm = 0x114
	jne	.LBB7_2
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movl	%eax, %ecx
	movb	$1, %al
	cmpl	$278, %ecx              # imm = 0x116
	je	.LBB7_3
.LBB7_2:
	xorl	%eax, %eax
.LBB7_3:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end7:
	.size	_ZN2kc11f_no_paramsEPNS_27impl_ac_parameter_type_listE, .Lfunc_end7-_ZN2kc11f_no_paramsEPNS_27impl_ac_parameter_type_listE
	.cfi_endproc

	.globl	_ZN2kc29f_post_create_in_operatordeclEPNS_16impl_alternativeE
	.p2align	4, 0x90
	.type	_ZN2kc29f_post_create_in_operatordeclEPNS_16impl_alternativeE,@function
_ZN2kc29f_post_create_in_operatordeclEPNS_16impl_alternativeE: # @_ZN2kc29f_post_create_in_operatordeclEPNS_16impl_alternativeE
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	jmp	_ZN2kcL24f_post_create_in_membersEPNS_19impl_fndeclarationsE # TAILCALL
.Lfunc_end8:
	.size	_ZN2kc29f_post_create_in_operatordeclEPNS_16impl_alternativeE, .Lfunc_end8-_ZN2kc29f_post_create_in_operatordeclEPNS_16impl_alternativeE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL24f_post_create_in_membersEPNS_19impl_fndeclarationsE,@function
_ZN2kcL24f_post_create_in_membersEPNS_19impl_fndeclarationsE: # @_ZN2kcL24f_post_create_in_membersEPNS_19impl_fndeclarationsE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	jmp	.LBB9_1
	.p2align	4, 0x90
.LBB9_14:                               #   in Loop: Header=BB9_1 Depth=1
	movq	16(%rbx), %rbx
.LBB9_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$108, %eax
	jne	.LBB9_11
# BB#2:                                 #   in Loop: Header=BB9_1 Depth=1
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$109, %eax
	jne	.LBB9_11
# BB#3:                                 #   in Loop: Header=BB9_1 Depth=1
	movq	8(%rbx), %rax
	movq	48(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$254, %eax
	jne	.LBB9_11
# BB#4:                                 #   in Loop: Header=BB9_1 Depth=1
	movq	8(%rbx), %rax
	movq	48(%rax), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$259, %eax              # imm = 0x103
	jne	.LBB9_11
# BB#5:                                 #   in Loop: Header=BB9_1 Depth=1
	movq	8(%rbx), %rax
	movq	48(%rax), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$255, %eax
	jne	.LBB9_11
# BB#6:                                 #   in Loop: Header=BB9_1 Depth=1
	movq	8(%rbx), %rax
	movq	48(%rax), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB9_11
# BB#7:                                 #   in Loop: Header=BB9_1 Depth=1
	movq	8(%rbx), %rax
	movq	48(%rax), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movq	40(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$6, %eax
	jne	.LBB9_11
# BB#8:                                 #   in Loop: Header=BB9_1 Depth=1
	movq	8(%rbx), %r14
	movq	48(%r14), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movq	40(%rax), %rax
	movq	40(%rax), %rax
	movq	8(%rax), %rdi
	movl	$.L.str.29, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB9_11
# BB#9:                                 #   in Loop: Header=BB9_1 Depth=1
	movq	88(%r14), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$113, %eax
	je	.LBB9_10
	.p2align	4, 0x90
.LBB9_11:                               #   in Loop: Header=BB9_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$108, %eax
	je	.LBB9_14
# BB#12:
	xorl	%eax, %eax
.LBB9_13:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB9_10:
	movb	$1, %al
	jmp	.LBB9_13
.Lfunc_end9:
	.size	_ZN2kcL24f_post_create_in_membersEPNS_19impl_fndeclarationsE, .Lfunc_end9-_ZN2kcL24f_post_create_in_membersEPNS_19impl_fndeclarationsE
	.cfi_endproc

	.globl	_ZN2kc27f_post_create_in_phylumdeclEPNS_22impl_phylumdeclarationE
	.p2align	4, 0x90
	.type	_ZN2kc27f_post_create_in_phylumdeclEPNS_22impl_phylumdeclarationE,@function
_ZN2kc27f_post_create_in_phylumdeclEPNS_22impl_phylumdeclarationE: # @_ZN2kc27f_post_create_in_phylumdeclEPNS_22impl_phylumdeclarationE
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rdi
	jmp	_ZN2kcL24f_post_create_in_membersEPNS_19impl_fndeclarationsE # TAILCALL
.Lfunc_end10:
	.size	_ZN2kc27f_post_create_in_phylumdeclEPNS_22impl_phylumdeclarationE, .Lfunc_end10-_ZN2kc27f_post_create_in_phylumdeclEPNS_22impl_phylumdeclarationE
	.cfi_endproc

	.globl	_ZN2kc23f_rewrite_in_phylumdeclEPNS_22impl_phylumdeclarationE
	.p2align	4, 0x90
	.type	_ZN2kc23f_rewrite_in_phylumdeclEPNS_22impl_phylumdeclarationE,@function
_ZN2kc23f_rewrite_in_phylumdeclEPNS_22impl_phylumdeclarationE: # @_ZN2kc23f_rewrite_in_phylumdeclEPNS_22impl_phylumdeclarationE
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rdi
	jmp	_ZN2kcL20f_rewrite_in_membersEPNS_19impl_fndeclarationsE # TAILCALL
.Lfunc_end11:
	.size	_ZN2kc23f_rewrite_in_phylumdeclEPNS_22impl_phylumdeclarationE, .Lfunc_end11-_ZN2kc23f_rewrite_in_phylumdeclEPNS_22impl_phylumdeclarationE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL20f_rewrite_in_membersEPNS_19impl_fndeclarationsE,@function
_ZN2kcL20f_rewrite_in_membersEPNS_19impl_fndeclarationsE: # @_ZN2kcL20f_rewrite_in_membersEPNS_19impl_fndeclarationsE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	jmp	.LBB12_1
	.p2align	4, 0x90
.LBB12_14:                              #   in Loop: Header=BB12_1 Depth=1
	movq	16(%rbx), %rbx
.LBB12_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$108, %eax
	jne	.LBB12_11
# BB#2:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$109, %eax
	jne	.LBB12_11
# BB#3:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	8(%rbx), %rax
	movq	48(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$254, %eax
	jne	.LBB12_11
# BB#4:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	8(%rbx), %rax
	movq	48(%rax), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$259, %eax              # imm = 0x103
	jne	.LBB12_11
# BB#5:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	8(%rbx), %rax
	movq	48(%rax), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$255, %eax
	jne	.LBB12_11
# BB#6:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	8(%rbx), %rax
	movq	48(%rax), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB12_11
# BB#7:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	8(%rbx), %rax
	movq	48(%rax), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movq	40(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$6, %eax
	jne	.LBB12_11
# BB#8:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	8(%rbx), %r14
	movq	48(%r14), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movq	40(%rax), %rax
	movq	40(%rax), %rax
	movq	8(%rax), %rdi
	movl	$.L.str.30, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB12_11
# BB#9:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	88(%r14), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$113, %eax
	je	.LBB12_10
	.p2align	4, 0x90
.LBB12_11:                              #   in Loop: Header=BB12_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$108, %eax
	je	.LBB12_14
# BB#12:
	xorl	%eax, %eax
.LBB12_13:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB12_10:
	movb	$1, %al
	jmp	.LBB12_13
.Lfunc_end12:
	.size	_ZN2kcL20f_rewrite_in_membersEPNS_19impl_fndeclarationsE, .Lfunc_end12-_ZN2kcL20f_rewrite_in_membersEPNS_19impl_fndeclarationsE
	.cfi_endproc

	.globl	_ZN2kc25f_rewrite_in_operatordeclEPNS_16impl_alternativeE
	.p2align	4, 0x90
	.type	_ZN2kc25f_rewrite_in_operatordeclEPNS_16impl_alternativeE,@function
_ZN2kc25f_rewrite_in_operatordeclEPNS_16impl_alternativeE: # @_ZN2kc25f_rewrite_in_operatordeclEPNS_16impl_alternativeE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
.Lcfi29:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$27, %eax
	jne	.LBB13_1
# BB#2:
	movq	24(%rbx), %rdi
	movq	40(%rbx), %rbx
	callq	_ZN2kcL20f_rewrite_in_membersEPNS_19impl_fndeclarationsE
	movl	%eax, %ecx
	movb	$1, %al
	testb	%cl, %cl
	jne	.LBB13_3
# BB#4:
	movq	%rbx, %rdi
	callq	_ZN2kc18f_phylumofoperatorEPNS_7impl_IDE
	movq	%rax, %rdi
	callq	_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE
	movq	16(%rax), %rdi
	popq	%rbx
	jmp	_ZN2kcL20f_rewrite_in_membersEPNS_19impl_fndeclarationsE # TAILCALL
.LBB13_1:
	xorl	%eax, %eax
.LBB13_3:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end13:
	.size	_ZN2kc25f_rewrite_in_operatordeclEPNS_16impl_alternativeE, .Lfunc_end13-_ZN2kc25f_rewrite_in_operatordeclEPNS_16impl_alternativeE
	.cfi_endproc

	.globl	_ZN2kc23f_phylumofwithcasesinfoEPNS_18impl_withcasesinfoE
	.p2align	4, 0x90
	.type	_ZN2kc23f_phylumofwithcasesinfoEPNS_18impl_withcasesinfoE,@function
_ZN2kc23f_phylumofwithcasesinfoEPNS_18impl_withcasesinfoE: # @_ZN2kc23f_phylumofwithcasesinfoEPNS_18impl_withcasesinfoE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -32
.Lcfi34:
	.cfi_offset %r14, -24
.Lcfi35:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	.p2align	4, 0x90
.LBB14_1:                               # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_4 Depth 2
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$221, %eax
	jne	.LBB14_12
# BB#2:                                 #   in Loop: Header=BB14_1 Depth=1
	movq	8(%r15), %rbx
	movq	16(%r15), %r15
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$222, %eax
	jne	.LBB14_11
# BB#3:                                 #   in Loop: Header=BB14_1 Depth=1
	movq	8(%rbx), %rbx
	.p2align	4, 0x90
.LBB14_4:                               # %tailrecurse.i
                                        #   Parent Loop BB14_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$201, %eax
	jne	.LBB14_7
# BB#5:                                 #   in Loop: Header=BB14_4 Depth=2
	movq	8(%rbx), %r14
	movq	16(%rbx), %rbx
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	jne	.LBB14_4
# BB#6:                                 #   in Loop: Header=BB14_1 Depth=1
	movq	40(%r14), %rdi
	callq	_ZN2kc18f_phylumofoperatorEPNS_7impl_IDE
	movq	%rax, %r14
	jmp	.LBB14_10
	.p2align	4, 0x90
.LBB14_7:                               #   in Loop: Header=BB14_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$200, %eax
	jne	.LBB14_9
# BB#8:                                 #   in Loop: Header=BB14_1 Depth=1
	callq	_ZN2kc9f_emptyIdEv
	movq	%rax, %r14
	jmp	.LBB14_10
	.p2align	4, 0x90
.LBB14_9:                               #   in Loop: Header=BB14_1 Depth=1
	movl	$.L.str.6, %edi
	movl	$371, %esi              # imm = 0x173
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%r14d, %r14d
.LBB14_10:                              # %_ZN2kc31f_phylumofpatternrepresentationEPNS_26impl_patternrepresentationE.exit
                                        #   in Loop: Header=BB14_1 Depth=1
	callq	_ZN2kc9f_emptyIdEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	jne	.LBB14_1
	jmp	.LBB14_16
.LBB14_12:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$220, %eax
	jne	.LBB14_14
# BB#13:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, %rbx
	movl	$.L.str.4, %edi
	callq	_ZN2kc9Problem1SEPKc
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	movl	$.L.str.5, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r14
	jmp	.LBB14_16
.LBB14_11:
	movl	$.L.str.3, %edi
	movl	$325, %esi              # imm = 0x145
	jmp	.LBB14_15
.LBB14_14:
	movl	$.L.str.3, %edi
	movl	$336, %esi              # imm = 0x150
.LBB14_15:                              # %.loopexit
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%r14d, %r14d
.LBB14_16:                              # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	_ZN2kc23f_phylumofwithcasesinfoEPNS_18impl_withcasesinfoE, .Lfunc_end14-_ZN2kc23f_phylumofwithcasesinfoEPNS_18impl_withcasesinfoE
	.cfi_endproc

	.globl	_ZN2kc31f_phylumofpatternrepresentationEPNS_26impl_patternrepresentationE
	.p2align	4, 0x90
	.type	_ZN2kc31f_phylumofpatternrepresentationEPNS_26impl_patternrepresentationE,@function
_ZN2kc31f_phylumofpatternrepresentationEPNS_26impl_patternrepresentationE: # @_ZN2kc31f_phylumofpatternrepresentationEPNS_26impl_patternrepresentationE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 32
.Lcfi39:
	.cfi_offset %rbx, -24
.Lcfi40:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB15_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$201, %eax
	jne	.LBB15_4
# BB#2:                                 #   in Loop: Header=BB15_1 Depth=1
	movq	8(%rbx), %r14
	movq	16(%rbx), %rbx
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	jne	.LBB15_1
# BB#3:
	movq	40(%r14), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc18f_phylumofoperatorEPNS_7impl_IDE # TAILCALL
.LBB15_4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$200, %eax
	jne	.LBB15_5
# BB#6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc9f_emptyIdEv      # TAILCALL
.LBB15_5:
	movl	$.L.str.6, %edi
	movl	$371, %esi              # imm = 0x173
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end15:
	.size	_ZN2kc31f_phylumofpatternrepresentationEPNS_26impl_patternrepresentationE, .Lfunc_end15-_ZN2kc31f_phylumofpatternrepresentationEPNS_26impl_patternrepresentationE
	.cfi_endproc

	.globl	_ZN2kc31sort_extend_parameter_type_listEPNS_24impl_ac_declaration_listEPNS_18impl_ac_declaratorE
	.p2align	4, 0x90
	.type	_ZN2kc31sort_extend_parameter_type_listEPNS_24impl_ac_declaration_listEPNS_18impl_ac_declaratorE,@function
_ZN2kc31sort_extend_parameter_type_listEPNS_24impl_ac_declaration_listEPNS_18impl_ac_declaratorE: # @_ZN2kc31sort_extend_parameter_type_listEPNS_24impl_ac_declaration_listEPNS_18impl_ac_declaratorE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 16
.Lcfi42:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$254, %eax
	jne	.LBB16_10
# BB#1:
	movq	24(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$259, %eax              # imm = 0x103
	jne	.LBB16_3
# BB#2:
	movq	24(%rbx), %rax
	popq	%rbx
	retq
.LBB16_10:
	movl	$.L.str.7, %edi
	movl	$407, %esi              # imm = 0x197
.LBB16_11:
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB16_3:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$258, %eax              # imm = 0x102
	jne	.LBB16_5
# BB#4:
	movq	16(%rbx), %rax
	popq	%rbx
	retq
.LBB16_5:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$257, %eax              # imm = 0x101
	je	.LBB16_6
# BB#7:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$256, %eax              # imm = 0x100
	je	.LBB16_6
# BB#8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$255, %eax
	jne	.LBB16_9
.LBB16_6:
	callq	_ZN2kc20Nilac_parameter_listEv
	movq	%rax, %rdi
	callq	_ZN2kc9AcParListEPNS_22impl_ac_parameter_listE
	popq	%rbx
	retq
.LBB16_9:
	movl	$.L.str.7, %edi
	movl	$402, %esi              # imm = 0x192
	jmp	.LBB16_11
.Lfunc_end16:
	.size	_ZN2kc31sort_extend_parameter_type_listEPNS_24impl_ac_declaration_listEPNS_18impl_ac_declaratorE, .Lfunc_end16-_ZN2kc31sort_extend_parameter_type_listEPNS_24impl_ac_declaration_listEPNS_18impl_ac_declaratorE
	.cfi_endproc

	.globl	_ZN2kc28t_sort_extend_parameter_listEPNS_24impl_ac_declaration_listEPNS_23impl_ac_identifier_listEPNS_22impl_ac_parameter_listE
	.p2align	4, 0x90
	.type	_ZN2kc28t_sort_extend_parameter_listEPNS_24impl_ac_declaration_listEPNS_23impl_ac_identifier_listEPNS_22impl_ac_parameter_listE,@function
_ZN2kc28t_sort_extend_parameter_listEPNS_24impl_ac_declaration_listEPNS_23impl_ac_identifier_listEPNS_22impl_ac_parameter_listE: # @_ZN2kc28t_sort_extend_parameter_listEPNS_24impl_ac_declaration_listEPNS_23impl_ac_identifier_listEPNS_22impl_ac_parameter_listE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 80
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbp
	movq	%rdi, %r12
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$283, %eax              # imm = 0x11B
	jne	.LBB17_16
# BB#1:
	movq	8(%rbp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	callq	_ZN2kc28t_sort_extend_parameter_listEPNS_24impl_ac_declaration_listEPNS_23impl_ac_identifier_listEPNS_22impl_ac_parameter_listE
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	xorl	%r15d, %r15d
	cmpl	$234, %eax
	jne	.LBB17_11
# BB#2:                                 # %.lr.ph61.i
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB17_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_5 Depth 2
	movq	8(%r12), %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$232, %eax
	jne	.LBB17_9
# BB#4:                                 #   in Loop: Header=BB17_3 Depth=1
	movq	8(%rbp), %r13
	jmp	.LBB17_5
	.p2align	4, 0x90
.LBB17_8:                               #   in Loop: Header=BB17_5 Depth=2
	callq	_ZN2kc24Noac_constant_expressionEv
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc13AcParDeclDeclEPNS_30impl_ac_declaration_specifiersEPNS_18impl_ac_declaratorEPNS_34impl_ac_constant_expression_optionE
	movq	%rax, (%rsp)            # 8-byte Spill
	incl	%r15d
.LBB17_5:                               #   Parent Loop BB17_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$252, %eax
	jne	.LBB17_9
# BB#6:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB17_5 Depth=2
	movq	8(%rbp), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$253, %eax
	jne	.LBB17_5
# BB#7:                                 #   in Loop: Header=BB17_5 Depth=2
	movq	8(%rbx), %r14
	movq	%r14, %rdi
	callq	_ZN2kc18f_ID_of_declaratorEPNS_18impl_ac_declaratorE
	movq	%rax, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB17_5
	jmp	.LBB17_8
	.p2align	4, 0x90
.LBB17_9:                               # %.loopexit.i
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	16(%r12), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$234, %eax
	je	.LBB17_3
# BB#10:                                # %._crit_edge.i
	cmpl	$1, %r15d
	movq	(%rsp), %rdi            # 8-byte Reload
	je	.LBB17_15
.LBB17_11:                              # %._crit_edge.thread.i
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	32(%r12), %rdi
	movl	24(%r12), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	cmpl	$2, %r15d
	jl	.LBB17_13
# BB#12:
	movl	$.L.str.31, %edi
	jmp	.LBB17_14
.LBB17_16:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$282, %eax              # imm = 0x11A
	je	.LBB17_18
# BB#17:
	movl	$.L.str.8, %edi
	movl	$428, %esi              # imm = 0x1AC
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%r14d, %r14d
.LBB17_18:
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_13:
	movl	$.L.str.32, %edi
.LBB17_14:
	movq	%r12, %rsi
	callq	_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc7WarningEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	movl	$.L.str.5, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rdi
	callq	_ZN2kc10AcTypeSpecEPNS_7impl_IDE
	movq	%rax, %rdi
	callq	_ZN2kc18AcDeclSpecTypeSpecEPNS_22impl_ac_type_specifierE
	movq	%rax, %rbx
	callq	_ZN2kc28Nilac_declaration_specifiersEv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc29Consac_declaration_specifiersEPNS_29impl_ac_declaration_specifierEPNS_30impl_ac_declaration_specifiersE
	movq	%rax, %r14
	callq	_ZN2kc9NopointerEv
	movq	%rax, %rbp
	callq	_ZN2kc7AcNoRefEv
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	_ZN2kc14AcDirectDeclIdEPNS_7impl_IDE
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc12AcDeclaratorEPNS_22impl_ac_pointer_optionEPNS_18impl_ac_ref_optionEPNS_25impl_ac_direct_declaratorE
	movq	%rax, %rbx
	callq	_ZN2kc24Noac_constant_expressionEv
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZN2kc13AcParDeclDeclEPNS_30impl_ac_declaration_specifiersEPNS_18impl_ac_declaratorEPNS_34impl_ac_constant_expression_optionE
	movq	%rax, %rdi
.LBB17_15:                              # %_ZN2kcL42lookup_and_create_ac_parameter_declarationEPNS_7impl_IDEPNS_24impl_ac_declaration_listE.exit
	movq	16(%rsp), %rsi          # 8-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc21Consac_parameter_listEPNS_29impl_ac_parameter_declarationEPNS_22impl_ac_parameter_listE # TAILCALL
.Lfunc_end17:
	.size	_ZN2kc28t_sort_extend_parameter_listEPNS_24impl_ac_declaration_listEPNS_23impl_ac_identifier_listEPNS_22impl_ac_parameter_listE, .Lfunc_end17-_ZN2kc28t_sort_extend_parameter_listEPNS_24impl_ac_declaration_listEPNS_23impl_ac_identifier_listEPNS_22impl_ac_parameter_listE
	.cfi_endproc

	.globl	_ZN2kc7unparseEPKcRNS_21printer_functor_classERNS_11uview_classE
	.p2align	4, 0x90
	.type	_ZN2kc7unparseEPKcRNS_21printer_functor_classERNS_11uview_classE,@function
_ZN2kc7unparseEPKcRNS_21printer_functor_classERNS_11uview_classE: # @_ZN2kc7unparseEPKcRNS_21printer_functor_classERNS_11uview_classE
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movq	(%rsi), %rcx
	movq	(%rcx), %rcx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmpq	*%rcx                   # TAILCALL
.Lfunc_end18:
	.size	_ZN2kc7unparseEPKcRNS_21printer_functor_classERNS_11uview_classE, .Lfunc_end18-_ZN2kc7unparseEPKcRNS_21printer_functor_classERNS_11uview_classE
	.cfi_endproc

	.globl	_ZN2kc13impl_charruns3setEi
	.p2align	4, 0x90
	.type	_ZN2kc13impl_charruns3setEi,@function
_ZN2kc13impl_charruns3setEi:            # @_ZN2kc13impl_charruns3setEi
	.cfi_startproc
# BB#0:
	movl	%esi, 8(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end19:
	.size	_ZN2kc13impl_charruns3setEi, .Lfunc_end19-_ZN2kc13impl_charruns3setEi
	.cfi_endproc

	.globl	_ZN2kc10f_mkselvarEPKci
	.p2align	4, 0x90
	.type	_ZN2kc10f_mkselvarEPKci,@function
_ZN2kc10f_mkselvarEPKci:                # @_ZN2kc10f_mkselvarEPKci
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 40
	subq	$8200, %rsp             # imm = 0x2008
.Lcfi60:
	.cfi_def_cfa_offset 8240
.Lcfi61:
	.cfi_offset %rbx, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	callq	strlen
	addq	$31, %rax
	cmpq	$8193, %rax             # imm = 0x2001
	jb	.LBB20_1
# BB#2:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
	movq	%rbp, %r15
	jmp	.LBB20_3
.LBB20_1:
	movq	%rsp, %rbp
	xorl	%r15d, %r15d
.LBB20_3:
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	callq	strlen
	leaq	(%rax,%rbp), %rdi
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	sprintf
	movl	$-1, %esi
	movq	%rbp, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rbx
	testq	%r15, %r15
	je	.LBB20_5
# BB#4:
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB20_5:
	movq	%rbx, %rax
	addq	$8200, %rsp             # imm = 0x2008
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	_ZN2kc10f_mkselvarEPKci, .Lfunc_end20-_ZN2kc10f_mkselvarEPKci
	.cfi_endproc

	.globl	_ZN2kc11f_mkselvar2EPKcii
	.p2align	4, 0x90
	.type	_ZN2kc11f_mkselvar2EPKcii,@function
_ZN2kc11f_mkselvar2EPKcii:              # @_ZN2kc11f_mkselvar2EPKcii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 48
	subq	$8192, %rsp             # imm = 0x2000
.Lcfi70:
	.cfi_def_cfa_offset 8240
.Lcfi71:
	.cfi_offset %rbx, -48
.Lcfi72:
	.cfi_offset %r12, -40
.Lcfi73:
	.cfi_offset %r14, -32
.Lcfi74:
	.cfi_offset %r15, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	callq	strlen
	addq	$62, %rax
	cmpq	$8193, %rax             # imm = 0x2001
	jb	.LBB21_1
# BB#2:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
	movq	%rbp, %r12
	jmp	.LBB21_3
.LBB21_1:
	movq	%rsp, %rbp
	xorl	%r12d, %r12d
.LBB21_3:
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	callq	strlen
	leaq	(%rax,%rbp), %rdi
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	movl	%r14d, %ecx
	callq	sprintf
	movl	$-1, %esi
	movq	%rbp, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.LBB21_5
# BB#4:
	movq	%r12, %rdi
	callq	_ZdaPv
.LBB21_5:
	movq	%rbx, %rax
	addq	$8192, %rsp             # imm = 0x2000
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	_ZN2kc11f_mkselvar2EPKcii, .Lfunc_end21-_ZN2kc11f_mkselvar2EPKcii
	.cfi_endproc

	.globl	_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrEPKc
	.p2align	4, 0x90
	.type	_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrEPKc,@function
_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrEPKc: # @_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrEPKc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi78:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi80:
	.cfi_def_cfa_offset 48
.Lcfi81:
	.cfi_offset %rbx, -40
.Lcfi82:
	.cfi_offset %r12, -32
.Lcfi83:
	.cfi_offset %r14, -24
.Lcfi84:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	8(%rdi), %r14
	movl	$47, %esi
	movq	%r14, %rdi
	callq	strrchr
	movq	%rax, %r12
	testq	%r12, %r12
	cmoveq	%r14, %r12
	movb	(%r12), %al
	cmpb	$34, %al
	je	.LBB22_2
# BB#1:
	cmpb	$47, %al
	jne	.LBB22_3
.LBB22_2:
	incq	%r12
.LBB22_3:
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%r15,%rax), %rdi
	callq	_Znam
	movq	%rax, %r14
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	strcpy
	testq	%r15, %r15
	je	.LBB22_4
# BB#5:
	cmpb	$34, -1(%r14,%r15)
	jne	.LBB22_7
# BB#6:
	decq	%r15
	movb	$0, (%r14,%r15)
.LBB22_7:
	cmpq	$2, %r15
	jb	.LBB22_10
# BB#8:
	cmpb	$107, -1(%r14,%r15)
	jne	.LBB22_10
# BB#9:
	leaq	-2(%r15), %rax
	cmpb	$46, -2(%r14,%r15)
	cmoveq	%rax, %r15
	jmp	.LBB22_10
.LBB22_4:
	xorl	%r15d, %r15d
.LBB22_10:                              # %.thread
	movb	(%rbx), %al
	testb	%al, %al
	je	.LBB22_11
# BB#12:                                # %._crit_edge.lr.ph
	movb	$46, (%r14,%r15)
	movb	%al, 1(%r14,%r15)
	incq	%rbx
	leaq	2(%r14,%r15), %rax
	.p2align	4, 0x90
.LBB22_13:                              # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %ecx
	movb	%cl, (%rax)
	incq	%rbx
	incq	%rax
	testb	%cl, %cl
	jne	.LBB22_13
	jmp	.LBB22_14
.LBB22_11:
	movb	$0, (%r14,%r15)
.LBB22_14:                              # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end22:
	.size	_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrEPKc, .Lfunc_end22-_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrEPKc
	.cfi_endproc

	.globl	_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align	4, 0x90
	.type	_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,@function
_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE: # @_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi85:
	.cfi_def_cfa_offset 16
	movq	(%rsi), %rsi
	callq	_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrEPKc
	popq	%rcx
	retq
.Lfunc_end23:
	.size	_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .Lfunc_end23-_ZN2kc13f_mk_filenameEPNS_20impl_casestring__StrERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc

	.globl	_ZN2kc26f_make_identifier_basenameEPKc
	.p2align	4, 0x90
	.type	_ZN2kc26f_make_identifier_basenameEPKc,@function
_ZN2kc26f_make_identifier_basenameEPKc: # @_ZN2kc26f_make_identifier_basenameEPKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi89:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi90:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi92:
	.cfi_def_cfa_offset 64
.Lcfi93:
	.cfi_offset %rbx, -56
.Lcfi94:
	.cfi_offset %r12, -48
.Lcfi95:
	.cfi_offset %r13, -40
.Lcfi96:
	.cfi_offset %r14, -32
.Lcfi97:
	.cfi_offset %r15, -24
.Lcfi98:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	callq	strlen
	movq	%rax, %r12
	leaq	-2(%r12), %r14
	leaq	-1(%r12), %rdi
	callq	_Znam
	movq	%rax, %r13
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	strncpy
	movb	$0, -2(%r13,%r12)
	testq	%r14, %r14
	je	.LBB24_5
# BB#1:                                 # %.lr.ph.preheader
	movl	$1, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB24_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%r13,%rbx), %edi
	callq	isalnum
	testl	%eax, %eax
	jne	.LBB24_4
# BB#3:                                 #   in Loop: Header=BB24_2 Depth=1
	movb	$95, (%r13,%rbx)
.LBB24_4:                               #   in Loop: Header=BB24_2 Depth=1
	movl	%ebp, %ebx
	incl	%ebp
	cmpq	%r14, %rbx
	jb	.LBB24_2
.LBB24_5:                               # %._crit_edge
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	_ZN2kc26f_make_identifier_basenameEPKc, .Lfunc_end24-_ZN2kc26f_make_identifier_basenameEPKc
	.cfi_endproc

	.globl	_ZN2kc37f_rewriterulesinfoofalternativeinviewEPNS_16impl_alternativeEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc37f_rewriterulesinfoofalternativeinviewEPNS_16impl_alternativeEPNS_7impl_IDE,@function
_ZN2kc37f_rewriterulesinfoofalternativeinviewEPNS_16impl_alternativeEPNS_7impl_IDE: # @_ZN2kc37f_rewriterulesinfoofalternativeinviewEPNS_16impl_alternativeEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	movq	%rsi, _ZL17global_filterview(%rip)
	movq	8(%rdi), %rdi
	movl	$_ZN2kcL27filterrewriteruleinfoonviewEPNS_20impl_rewriteruleinfoE, %esi
	jmp	_ZN2kc21impl_rewriterulesinfo6filterEPFbPNS_20impl_rewriteruleinfoEE # TAILCALL
.Lfunc_end25:
	.size	_ZN2kc37f_rewriterulesinfoofalternativeinviewEPNS_16impl_alternativeEPNS_7impl_IDE, .Lfunc_end25-_ZN2kc37f_rewriterulesinfoofalternativeinviewEPNS_16impl_alternativeEPNS_7impl_IDE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL27filterrewriteruleinfoonviewEPNS_20impl_rewriteruleinfoE,@function
_ZN2kcL27filterrewriteruleinfoonviewEPNS_20impl_rewriteruleinfoE: # @_ZN2kcL27filterrewriteruleinfoonviewEPNS_20impl_rewriteruleinfoE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 32
.Lcfi102:
	.cfi_offset %rbx, -24
.Lcfi103:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	_ZL17global_filterview(%rip), %r14
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$219, %eax
	jne	.LBB26_7
# BB#1:
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$70, %eax
	jne	.LBB26_7
# BB#2:
	movq	24(%rbx), %rax
	movq	8(%rax), %rbx
	jmp	.LBB26_3
	.p2align	4, 0x90
.LBB26_6:                               # %.thread.i.i
                                        #   in Loop: Header=BB26_3 Depth=1
	movq	24(%rbx), %rbx
.LBB26_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$145, %eax
	jne	.LBB26_8
# BB#4:                                 #   in Loop: Header=BB26_3 Depth=1
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB26_6
# BB#5:
	movb	$1, %al
	jmp	.LBB26_9
.LBB26_7:
	movl	$.L.str.33, %edi
	movl	$719, %esi              # imm = 0x2CF
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB26_8:                               # %_ZN2kcL30is_viewname_in_rewriteruleinfoEPNS_7impl_IDEPNS_20impl_rewriteruleinfoE.exit
	xorl	%eax, %eax
.LBB26_9:                               # %_ZN2kcL30is_viewname_in_rewriteruleinfoEPNS_7impl_IDEPNS_20impl_rewriteruleinfoE.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end26:
	.size	_ZN2kcL27filterrewriteruleinfoonviewEPNS_20impl_rewriteruleinfoE, .Lfunc_end26-_ZN2kcL27filterrewriteruleinfoonviewEPNS_20impl_rewriteruleinfoE
	.cfi_endproc

	.globl	_ZN2kc33f_rewriteviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE
	.p2align	4, 0x90
	.type	_ZN2kc33f_rewriteviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE,@function
_ZN2kc33f_rewriteviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE: # @_ZN2kc33f_rewriteviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi106:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi108:
	.cfi_def_cfa_offset 48
.Lcfi109:
	.cfi_offset %rbx, -40
.Lcfi110:
	.cfi_offset %r12, -32
.Lcfi111:
	.cfi_offset %r14, -24
.Lcfi112:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	8(%rdi), %r14
	callq	_ZN2kc19NilrewriteviewsinfoEv
	movq	%rax, %r15
	jmp	.LBB27_2
	.p2align	4, 0x90
.LBB27_1:                               # %.lr.ph
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	16(%rbx), %r12
	movq	%r12, _ZL17global_filterview(%rip)
	movl	$_ZN2kcL27filterrewriteruleinfoonviewEPNS_20impl_rewriteruleinfoE, %esi
	movq	%r14, %rdi
	callq	_ZN2kc21impl_rewriterulesinfo6filterEPFbPNS_20impl_rewriteruleinfoEE
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc15RewriteviewinfoEPNS_7impl_IDEPNS_21impl_rewriterulesinfoE
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc20ConsrewriteviewsinfoEPNS_20impl_rewriteviewinfoEPNS_21impl_rewriteviewsinfoE
	movq	%rax, %r15
	movq	24(%rbx), %rbx
.LBB27_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$145, %eax
	je	.LBB27_1
# BB#3:                                 # %._crit_edge
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end27:
	.size	_ZN2kc33f_rewriteviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE, .Lfunc_end27-_ZN2kc33f_rewriteviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE
	.cfi_endproc

	.globl	_ZN2kc33f_unparseviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE
	.p2align	4, 0x90
	.type	_ZN2kc33f_unparseviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE,@function
_ZN2kc33f_unparseviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE: # @_ZN2kc33f_unparseviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi114:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi115:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi117:
	.cfi_def_cfa_offset 48
.Lcfi118:
	.cfi_offset %rbx, -40
.Lcfi119:
	.cfi_offset %r12, -32
.Lcfi120:
	.cfi_offset %r14, -24
.Lcfi121:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	16(%rdi), %r14
	callq	_ZN2kc19NilunparseviewsinfoEv
	movq	%rax, %r15
	jmp	.LBB28_2
	.p2align	4, 0x90
.LBB28_1:                               # %.lr.ph
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	16(%rbx), %r12
	movq	%r12, _ZL17global_filterview(%rip)
	movl	$_ZN2kcL27filterunparsedeclinfoonviewEPNS_20impl_unparsedeclinfoE, %esi
	movq	%r14, %rdi
	callq	_ZN2kc21impl_unparsedeclsinfo6filterEPFbPNS_20impl_unparsedeclinfoEE
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc15UnparseviewinfoEPNS_7impl_IDEPNS_21impl_unparsedeclsinfoE
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc20ConsunparseviewsinfoEPNS_20impl_unparseviewinfoEPNS_21impl_unparseviewsinfoE
	movq	%rax, %r15
	movq	24(%rbx), %rbx
.LBB28_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$145, %eax
	je	.LBB28_1
# BB#3:                                 # %._crit_edge
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end28:
	.size	_ZN2kc33f_unparseviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE, .Lfunc_end28-_ZN2kc33f_unparseviewsinfo_of_alternativeEPNS_16impl_alternativeEPNS_14impl_viewnamesE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL27filterunparsedeclinfoonviewEPNS_20impl_unparsedeclinfoE,@function
_ZN2kcL27filterunparsedeclinfoonviewEPNS_20impl_unparsedeclinfoE: # @_ZN2kcL27filterunparsedeclinfoonviewEPNS_20impl_unparsedeclinfoE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi124:
	.cfi_def_cfa_offset 32
.Lcfi125:
	.cfi_offset %rbx, -24
.Lcfi126:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	_ZL17global_filterview(%rip), %r14
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$231, %eax
	jne	.LBB29_7
# BB#1:
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$143, %eax
	jne	.LBB29_7
# BB#2:
	movq	24(%rbx), %rax
	movq	8(%rax), %rbx
	jmp	.LBB29_3
	.p2align	4, 0x90
.LBB29_6:                               # %.thread.i.i
                                        #   in Loop: Header=BB29_3 Depth=1
	movq	24(%rbx), %rbx
.LBB29_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$145, %eax
	jne	.LBB29_8
# BB#4:                                 #   in Loop: Header=BB29_3 Depth=1
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB29_6
# BB#5:
	movb	$1, %al
	jmp	.LBB29_9
.LBB29_7:
	movl	$.L.str.34, %edi
	movl	$735, %esi              # imm = 0x2DF
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB29_8:                               # %_ZN2kcL30is_viewname_in_unparsedeclinfoEPNS_7impl_IDEPNS_20impl_unparsedeclinfoE.exit
	xorl	%eax, %eax
.LBB29_9:                               # %_ZN2kcL30is_viewname_in_unparsedeclinfoEPNS_7impl_IDEPNS_20impl_unparsedeclinfoE.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end29:
	.size	_ZN2kcL27filterunparsedeclinfoonviewEPNS_20impl_unparsedeclinfoE, .Lfunc_end29-_ZN2kcL27filterunparsedeclinfoonviewEPNS_20impl_unparsedeclinfoE
	.cfi_endproc

	.globl	_ZN2kc8f_typeofEPNS_9impl_pathE
	.p2align	4, 0x90
	.type	_ZN2kc8f_typeofEPNS_9impl_pathE,@function
_ZN2kc8f_typeofEPNS_9impl_pathE:        # @_ZN2kc8f_typeofEPNS_9impl_pathE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi129:
	.cfi_def_cfa_offset 32
.Lcfi130:
	.cfi_offset %rbx, -24
.Lcfi131:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %r14
	callq	_ZN2kc9f_emptyIdEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB30_4
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$212, %eax
	jne	.LBB30_2
# BB#6:
	movq	24(%rbx), %rdi
	movq	32(%rbx), %rax
	movq	8(%rax), %rbx
	callq	_ZN2kc3IntEPNS_17impl_integer__IntE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc21f_subphylumofoperatorEPNS_7impl_IDEPNS_8impl_INTE # TAILCALL
.LBB30_4:
	movq	16(%rbx), %rax
	jmp	.LBB30_5
.LBB30_2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$211, %eax
	jne	.LBB30_3
# BB#7:
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc18f_phylumofoperatorEPNS_7impl_IDE # TAILCALL
.LBB30_3:
	movl	$.L.str.11, %edi
	movl	$787, %esi              # imm = 0x313
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
.LBB30_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end30:
	.size	_ZN2kc8f_typeofEPNS_9impl_pathE, .Lfunc_end30-_ZN2kc8f_typeofEPNS_9impl_pathE
	.cfi_endproc

	.globl	_ZN2kc33f_operatorofpatternrepresentationEPNS_26impl_patternrepresentationE
	.p2align	4, 0x90
	.type	_ZN2kc33f_operatorofpatternrepresentationEPNS_26impl_patternrepresentationE,@function
_ZN2kc33f_operatorofpatternrepresentationEPNS_26impl_patternrepresentationE: # @_ZN2kc33f_operatorofpatternrepresentationEPNS_26impl_patternrepresentationE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi132:
	.cfi_def_cfa_offset 16
.Lcfi133:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$201, %eax
	jne	.LBB31_37
# BB#1:
	movq	8(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$208, %eax
	je	.LBB31_41
# BB#2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$205, %eax
	je	.LBB31_41
# BB#3:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$210, %eax
	jne	.LBB31_8
# BB#4:
	movq	32(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$212, %eax
	je	.LBB31_5
# BB#6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$211, %eax
	jne	.LBB31_33
# BB#7:
	movq	8(%rbx), %rax
	popq	%rbx
	retq
.LBB31_37:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$200, %eax
	jne	.LBB31_38
.LBB31_41:
	popq	%rbx
	jmp	_ZN2kc9f_emptyIdEv      # TAILCALL
.LBB31_38:
	movl	$.L.str.12, %edi
	movl	$811, %esi              # imm = 0x32B
.LBB31_39:                              # %_ZN2kcL38f_operatorofelem_patternrepresentationEPNS_31impl_elem_patternrepresentationE.exit
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB31_8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$209, %eax
	jne	.LBB31_12
# BB#9:
	movq	32(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$212, %eax
	je	.LBB31_5
# BB#10:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$211, %eax
	jne	.LBB31_33
# BB#11:
	movq	8(%rbx), %rax
	popq	%rbx
	retq
.LBB31_12:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$207, %eax
	jne	.LBB31_16
# BB#13:
	movq	32(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$212, %eax
	je	.LBB31_5
# BB#14:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$211, %eax
	jne	.LBB31_33
# BB#15:
	movq	8(%rbx), %rax
	popq	%rbx
	retq
.LBB31_16:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$206, %eax
	jne	.LBB31_20
# BB#17:
	movq	32(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$212, %eax
	je	.LBB31_5
# BB#18:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$211, %eax
	jne	.LBB31_33
# BB#19:
	movq	8(%rbx), %rax
	popq	%rbx
	retq
.LBB31_20:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$204, %eax
	jne	.LBB31_24
# BB#21:
	movq	32(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$212, %eax
	je	.LBB31_5
# BB#22:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$211, %eax
	jne	.LBB31_33
# BB#23:
	movq	8(%rbx), %rax
	popq	%rbx
	retq
.LBB31_24:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$202, %eax
	jne	.LBB31_28
# BB#25:
	movq	32(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$212, %eax
	jne	.LBB31_26
.LBB31_5:
	movq	32(%rbx), %rax
	movq	8(%rax), %rax
	popq	%rbx
	retq
.LBB31_28:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$203, %eax
	jne	.LBB31_36
# BB#29:
	movq	32(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$214, %eax
	jne	.LBB31_34
# BB#30:
	movq	8(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$212, %eax
	je	.LBB31_5
# BB#31:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$211, %eax
	jne	.LBB31_33
# BB#32:
	movq	8(%rbx), %rax
	popq	%rbx
	retq
.LBB31_26:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$211, %eax
	jne	.LBB31_33
# BB#27:
	movq	8(%rbx), %rax
	popq	%rbx
	retq
.LBB31_33:
	movl	$.L.str.36, %edi
	movl	$897, %esi              # imm = 0x381
	jmp	.LBB31_39
.LBB31_36:
	movl	$.L.str.35, %edi
	movl	$855, %esi              # imm = 0x357
	jmp	.LBB31_39
.LBB31_34:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$213, %eax
	je	.LBB31_41
# BB#35:
	movl	$.L.str.37, %edi
	movl	$876, %esi              # imm = 0x36C
	jmp	.LBB31_39
.Lfunc_end31:
	.size	_ZN2kc33f_operatorofpatternrepresentationEPNS_26impl_patternrepresentationE, .Lfunc_end31-_ZN2kc33f_operatorofpatternrepresentationEPNS_26impl_patternrepresentationE
	.cfi_endproc

	.globl	_ZN2kc18f_typeofunpsubtermEPNS_15impl_unpsubtermEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc18f_typeofunpsubtermEPNS_15impl_unpsubtermEPNS_7impl_IDE,@function
_ZN2kc18f_typeofunpsubtermEPNS_15impl_unpsubtermEPNS_7impl_IDE: # @_ZN2kc18f_typeofunpsubtermEPNS_15impl_unpsubtermEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi136:
	.cfi_def_cfa_offset 32
.Lcfi137:
	.cfi_offset %rbx, -24
.Lcfi138:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$157, %eax
	jne	.LBB32_2
# BB#1:
	movq	8(%rbx), %rax
.LBB32_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB32_2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$156, %eax
	jne	.LBB32_4
# BB#3:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rbx
	movq	%r14, %rdi
	callq	_ZN2kc21f_subphylumofoperatorEPNS_7impl_IDEPNS_8impl_INTE
	jmp	.LBB32_6
.LBB32_4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$155, %eax
	jne	.LBB32_7
# BB#5:
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rbx
	callq	_ZN2kc19f_phylumofpatternIDEPNS_7impl_IDE
.LBB32_6:
	movq	%rbx, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc31f_check_unpattributes_in_phylumEPNS_18impl_unpattributesEPNS_7impl_IDE # TAILCALL
.LBB32_7:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$154, %eax
	jne	.LBB32_8
# BB#11:
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc21f_subphylumofoperatorEPNS_7impl_IDEPNS_8impl_INTE # TAILCALL
.LBB32_8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$153, %eax
	jne	.LBB32_9
# BB#12:
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc19f_phylumofpatternIDEPNS_7impl_IDE # TAILCALL
.LBB32_9:
	movl	$.L.str.13, %edi
	movl	$929, %esi              # imm = 0x3A1
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	jmp	.LBB32_10
.Lfunc_end32:
	.size	_ZN2kc18f_typeofunpsubtermEPNS_15impl_unpsubtermEPNS_7impl_IDE, .Lfunc_end32-_ZN2kc18f_typeofunpsubtermEPNS_15impl_unpsubtermEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc38f_outmost_nl_preds_in_rewriterulesinfoEPNS_21impl_rewriterulesinfoE
	.p2align	4, 0x90
	.type	_ZN2kc38f_outmost_nl_preds_in_rewriterulesinfoEPNS_21impl_rewriterulesinfoE,@function
_ZN2kc38f_outmost_nl_preds_in_rewriterulesinfoEPNS_21impl_rewriterulesinfoE: # @_ZN2kc38f_outmost_nl_preds_in_rewriterulesinfoEPNS_21impl_rewriterulesinfoE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 16
.Lcfi140:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB33_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$218, %eax
	jne	.LBB33_4
# BB#2:                                 #   in Loop: Header=BB33_1 Depth=1
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$219, %eax
	jne	.LBB33_4
# BB#3:                                 #   in Loop: Header=BB33_1 Depth=1
	movq	8(%rbx), %rax
	movq	16(%rbx), %rbx
	movq	8(%rax), %rdi
	callq	_ZN2kcL43f_outmost_nl_preds_in_patternrepresentationEPNS_26impl_patternrepresentationE
	testq	%rax, %rax
	je	.LBB33_1
	jmp	.LBB33_7
.LBB33_4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$217, %eax
	je	.LBB33_6
# BB#5:
	movl	$.L.str.14, %edi
	movl	$952, %esi              # imm = 0x3B8
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB33_6:                               # %.loopexit
	xorl	%eax, %eax
.LBB33_7:                               # %.loopexit
	popq	%rbx
	retq
.Lfunc_end33:
	.size	_ZN2kc38f_outmost_nl_preds_in_rewriterulesinfoEPNS_21impl_rewriterulesinfoE, .Lfunc_end33-_ZN2kc38f_outmost_nl_preds_in_rewriterulesinfoEPNS_21impl_rewriterulesinfoE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL43f_outmost_nl_preds_in_patternrepresentationEPNS_26impl_patternrepresentationE,@function
_ZN2kcL43f_outmost_nl_preds_in_patternrepresentationEPNS_26impl_patternrepresentationE: # @_ZN2kcL43f_outmost_nl_preds_in_patternrepresentationEPNS_26impl_patternrepresentationE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 32
.Lcfi144:
	.cfi_offset %rbx, -32
.Lcfi145:
	.cfi_offset %r14, -24
.Lcfi146:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	jmp	.LBB34_1
	.p2align	4, 0x90
.LBB34_14:                              #   in Loop: Header=BB34_1 Depth=1
	movl	$.L.str.39, %edi
	movl	$1035, %esi             # imm = 0x40B
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB34_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB34_4 Depth 2
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$201, %eax
	jne	.LBB34_7
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB34_1 Depth=1
	movq	8(%r15), %r14
	movq	16(%r15), %r15
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$203, %eax
	jne	.LBB34_1
# BB#3:                                 #   in Loop: Header=BB34_1 Depth=1
	movq	32(%r14), %rbx
	jmp	.LBB34_4
	.p2align	4, 0x90
.LBB34_6:                               # %tailrecurse.i.i
                                        #   in Loop: Header=BB34_4 Depth=2
	movq	16(%rbx), %rbx
.LBB34_4:                               #   Parent Loop BB34_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$214, %eax
	jne	.LBB34_11
# BB#5:                                 # %.lr.ph.i.i
                                        #   in Loop: Header=BB34_4 Depth=2
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$212, %eax
	je	.LBB34_6
.LBB34_11:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB34_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$214, %eax
	jne	.LBB34_13
# BB#12:                                #   in Loop: Header=BB34_1 Depth=1
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$211, %eax
	je	.LBB34_10
.LBB34_13:                              #   in Loop: Header=BB34_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$213, %eax
	je	.LBB34_1
	jmp	.LBB34_14
.LBB34_7:                               # %tailrecurse._crit_edge
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$200, %eax
	je	.LBB34_9
# BB#8:
	movl	$.L.str.38, %edi
	movl	$996, %esi              # imm = 0x3E4
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB34_9:                               # %_ZN2kcL48f_outmost_nl_preds_in_elem_patternrepresentationEPNS_31impl_elem_patternrepresentationE.exit
	xorl	%r14d, %r14d
.LBB34_10:                              # %_ZN2kcL48f_outmost_nl_preds_in_elem_patternrepresentationEPNS_31impl_elem_patternrepresentationE.exit
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end34:
	.size	_ZN2kcL43f_outmost_nl_preds_in_patternrepresentationEPNS_26impl_patternrepresentationE, .Lfunc_end34-_ZN2kcL43f_outmost_nl_preds_in_patternrepresentationEPNS_26impl_patternrepresentationE
	.cfi_endproc

	.globl	_ZN2kc38f_outmost_nl_preds_in_unparsedeclsinfoEPNS_21impl_unparsedeclsinfoE
	.p2align	4, 0x90
	.type	_ZN2kc38f_outmost_nl_preds_in_unparsedeclsinfoEPNS_21impl_unparsedeclsinfoE,@function
_ZN2kc38f_outmost_nl_preds_in_unparsedeclsinfoEPNS_21impl_unparsedeclsinfoE: # @_ZN2kc38f_outmost_nl_preds_in_unparsedeclsinfoEPNS_21impl_unparsedeclsinfoE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 16
.Lcfi148:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB35_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$230, %eax
	jne	.LBB35_4
# BB#2:                                 #   in Loop: Header=BB35_1 Depth=1
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$231, %eax
	jne	.LBB35_4
# BB#3:                                 #   in Loop: Header=BB35_1 Depth=1
	movq	8(%rbx), %rax
	movq	16(%rbx), %rbx
	movq	8(%rax), %rdi
	callq	_ZN2kcL43f_outmost_nl_preds_in_patternrepresentationEPNS_26impl_patternrepresentationE
	testq	%rax, %rax
	je	.LBB35_1
	jmp	.LBB35_7
.LBB35_4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$229, %eax
	je	.LBB35_6
# BB#5:
	movl	$.L.str.15, %edi
	movl	$975, %esi              # imm = 0x3CF
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB35_6:                               # %.loopexit
	xorl	%eax, %eax
.LBB35_7:                               # %.loopexit
	popq	%rbx
	retq
.Lfunc_end35:
	.size	_ZN2kc38f_outmost_nl_preds_in_unparsedeclsinfoEPNS_21impl_unparsedeclsinfoE, .Lfunc_end35-_ZN2kc38f_outmost_nl_preds_in_unparsedeclsinfoEPNS_21impl_unparsedeclsinfoE
	.cfi_endproc

	.globl	_ZN2kc19f_is_known_ptr_typeEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc19f_is_known_ptr_typeEPNS_7impl_IDE,@function
_ZN2kc19f_is_known_ptr_typeEPNS_7impl_IDE: # @_ZN2kc19f_is_known_ptr_typeEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi151:
	.cfi_def_cfa_offset 32
.Lcfi152:
	.cfi_offset %rbx, -24
.Lcfi153:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	_ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB36_2
# BB#1:
	callq	_ZN2kc14NilphylumnamesEv
	movq	%rax, _ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip)
	movl	$.L.str.16, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	_ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip), %rsi
	movq	%rax, %rdi
	callq	_ZN2kc15ConsphylumnamesEPNS_7impl_IDEPNS_16impl_phylumnamesE
	movq	%rax, _ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip)
	movl	$.L.str.17, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	_ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip), %rsi
	movq	%rax, %rdi
	callq	_ZN2kc15ConsphylumnamesEPNS_7impl_IDEPNS_16impl_phylumnamesE
	movq	%rax, _ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip)
	movl	$.L.str.18, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	_ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip), %rsi
	movq	%rax, %rdi
	callq	_ZN2kc15ConsphylumnamesEPNS_7impl_IDEPNS_16impl_phylumnamesE
	movq	%rax, _ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip)
	movl	$.L.str.19, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	_ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip), %rsi
	movq	%rax, %rdi
	callq	_ZN2kc15ConsphylumnamesEPNS_7impl_IDEPNS_16impl_phylumnamesE
	movq	%rax, _ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip)
	movl	$.L.str.20, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	_ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip), %rsi
	movq	%rax, %rdi
	callq	_ZN2kc15ConsphylumnamesEPNS_7impl_IDEPNS_16impl_phylumnamesE
	movq	%rax, _ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip)
	movl	$.L.str.21, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	_ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip), %rsi
	movq	%rax, %rdi
	callq	_ZN2kc15ConsphylumnamesEPNS_7impl_IDEPNS_16impl_phylumnamesE
	movq	%rax, _ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip)
	movl	$.L.str.22, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	_ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip), %rsi
	movq	%rax, %rdi
	callq	_ZN2kc15ConsphylumnamesEPNS_7impl_IDEPNS_16impl_phylumnamesE
	movq	%rax, _ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip)
	movl	$.L.str.23, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	_ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip), %rsi
	movq	%rax, %rdi
	callq	_ZN2kc15ConsphylumnamesEPNS_7impl_IDEPNS_16impl_phylumnamesE
	movq	%rax, _ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip)
	movl	$.L.str.24, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	_ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip), %rsi
	movq	%rax, %rdi
	callq	_ZN2kc15ConsphylumnamesEPNS_7impl_IDEPNS_16impl_phylumnamesE
	movq	%rax, _ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip)
	movl	$.L.str.25, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	_ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip), %rsi
	movq	%rax, %rdi
	callq	_ZN2kc15ConsphylumnamesEPNS_7impl_IDEPNS_16impl_phylumnamesE
	movq	%rax, %rbx
	movq	%rbx, _ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known(%rip)
.LBB36_2:                               # %.preheader
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$14, %eax
	jne	.LBB36_6
	.p2align	4, 0x90
.LBB36_3:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	jne	.LBB36_7
# BB#4:                                 #   in Loop: Header=BB36_3 Depth=1
	movq	16(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$14, %eax
	je	.LBB36_3
.LBB36_6:
	xorl	%eax, %eax
	jmp	.LBB36_8
.LBB36_7:
	movb	$1, %al
.LBB36_8:                               # %._crit_edge
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end36:
	.size	_ZN2kc19f_is_known_ptr_typeEPNS_7impl_IDE, .Lfunc_end36-_ZN2kc19f_is_known_ptr_typeEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc21rewrite_withcasesinfoEPNS_18impl_withcasesinfoE
	.p2align	4, 0x90
	.type	_ZN2kc21rewrite_withcasesinfoEPNS_18impl_withcasesinfoE,@function
_ZN2kc21rewrite_withcasesinfoEPNS_18impl_withcasesinfoE: # @_ZN2kc21rewrite_withcasesinfoEPNS_18impl_withcasesinfoE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi154:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi155:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 32
.Lcfi157:
	.cfi_offset %rbx, -32
.Lcfi158:
	.cfi_offset %r14, -24
.Lcfi159:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$221, %eax
	jne	.LBB37_2
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$222, %eax
	jne	.LBB37_2
# BB#5:
	movq	8(%rbx), %rax
	movq	16(%rbx), %r14
	movq	8(%rax), %rdi
	movq	16(%rax), %rbx
	movq	24(%rax), %r15
	movq	(%rdi), %rax
	movl	$_ZN2kc10base_rviewE, %esi
	callq	*24(%rax)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	_ZN2kc12WithcaseinfoEPNS_26impl_patternrepresentationES1_PNS_10impl_CtextE
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZN2kc21rewrite_withcasesinfoEPNS_18impl_withcasesinfoE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN2kc17ConswithcasesinfoEPNS_17impl_withcaseinfoEPNS_18impl_withcasesinfoE # TAILCALL
.LBB37_2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$220, %eax
	je	.LBB37_4
# BB#3:
	movl	$.L.str.26, %edi
	movl	$1102, %esi             # imm = 0x44E
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%ebx, %ebx
.LBB37_4:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end37:
	.size	_ZN2kc21rewrite_withcasesinfoEPNS_18impl_withcasesinfoE, .Lfunc_end37-_ZN2kc21rewrite_withcasesinfoEPNS_18impl_withcasesinfoE
	.cfi_endproc

	.globl	_ZN2kc45pos_of_sole_dollar_or_pattern_in_patternchainEPNS_17impl_patternchainE
	.p2align	4, 0x90
	.type	_ZN2kc45pos_of_sole_dollar_or_pattern_in_patternchainEPNS_17impl_patternchainE,@function
_ZN2kc45pos_of_sole_dollar_or_pattern_in_patternchainEPNS_17impl_patternchainE: # @_ZN2kc45pos_of_sole_dollar_or_pattern_in_patternchainEPNS_17impl_patternchainE
	.cfi_startproc
# BB#0:
	movl	$-2, %esi
	movl	$1, %edx
	jmp	_ZN2kc47t_pos_of_sole_dollar_or_pattern_in_patternchainEPNS_17impl_patternchainEii # TAILCALL
.Lfunc_end38:
	.size	_ZN2kc45pos_of_sole_dollar_or_pattern_in_patternchainEPNS_17impl_patternchainE, .Lfunc_end38-_ZN2kc45pos_of_sole_dollar_or_pattern_in_patternchainEPNS_17impl_patternchainE
	.cfi_endproc

	.globl	_ZN2kc47t_pos_of_sole_dollar_or_pattern_in_patternchainEPNS_17impl_patternchainEii
	.p2align	4, 0x90
	.type	_ZN2kc47t_pos_of_sole_dollar_or_pattern_in_patternchainEPNS_17impl_patternchainEii,@function
_ZN2kc47t_pos_of_sole_dollar_or_pattern_in_patternchainEPNS_17impl_patternchainEii: # @_ZN2kc47t_pos_of_sole_dollar_or_pattern_in_patternchainEPNS_17impl_patternchainEii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi162:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi163:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi164:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi166:
	.cfi_def_cfa_offset 64
.Lcfi167:
	.cfi_offset %rbx, -56
.Lcfi168:
	.cfi_offset %r12, -48
.Lcfi169:
	.cfi_offset %r13, -40
.Lcfi170:
	.cfi_offset %r14, -32
.Lcfi171:
	.cfi_offset %r15, -24
.Lcfi172:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebx
	movq	%rdi, %rbp
	movl	$-1, %r13d
	cmpl	$-1, %ebx
	je	.LBB39_12
# BB#1:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB39_2
# BB#3:
	movq	24(%rbp), %r12
	movq	32(%rbp), %rdi
	leal	1(%r15), %edx
	movl	%ebx, %esi
	callq	_ZN2kc47t_pos_of_sole_dollar_or_pattern_in_patternchainEPNS_17impl_patternchainEii
	movl	%eax, %r14d
	cmpl	$-1, %r14d
	je	.LBB39_12
# BB#4:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$79, %eax
	jne	.LBB39_5
.LBB39_11:
	testl	%r14d, %r14d
	movl	$-1, %eax
	cmovnsl	%eax, %r15d
	movl	%r15d, %r13d
	jmp	.LBB39_12
.LBB39_2:
	movl	%ebx, %r13d
.LBB39_12:
	movl	%r13d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB39_5:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$77, %eax
	jne	.LBB39_12
# BB#6:
	movq	32(%r12), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$80, %eax
	jne	.LBB39_11
# BB#7:
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB39_11
# BB#8:
	movq	32(%rbx), %rax
	movq	40(%rax), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$172, %eax
	jne	.LBB39_10
# BB#9:
	movl	%r14d, %r13d
	jmp	.LBB39_12
.LBB39_10:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$185, %eax
	movl	%r14d, %r13d
	jne	.LBB39_11
	jmp	.LBB39_12
.Lfunc_end39:
	.size	_ZN2kc47t_pos_of_sole_dollar_or_pattern_in_patternchainEPNS_17impl_patternchainEii, .Lfunc_end39-_ZN2kc47t_pos_of_sole_dollar_or_pattern_in_patternchainEPNS_17impl_patternchainEii
	.cfi_endproc

	.globl	_ZN2kc20f_getidentfromstringB5cxx11EPPKc
	.p2align	4, 0x90
	.type	_ZN2kc20f_getidentfromstringB5cxx11EPPKc,@function
_ZN2kc20f_getidentfromstringB5cxx11EPPKc: # @_ZN2kc20f_getidentfromstringB5cxx11EPPKc
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:                                 # %._crit_edge.i.i.i.i
	pushq	%rbp
.Lcfi173:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi174:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi175:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi176:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi177:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi178:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi179:
	.cfi_def_cfa_offset 64
.Lcfi180:
	.cfi_offset %rbx, -56
.Lcfi181:
	.cfi_offset %r12, -48
.Lcfi182:
	.cfi_offset %r13, -40
.Lcfi183:
	.cfi_offset %r14, -32
.Lcfi184:
	.cfi_offset %r15, -24
.Lcfi185:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	leaq	16(%r15), %r12
	movq	%r12, (%r15)
	movq	$0, 8(%r15)
	movb	$0, 16(%r15)
	movq	(%r14), %r13
	movsbl	(%r13), %ebx
	movl	%ebx, %edi
	callq	isalnum
	cmpl	$95, %ebx
	je	.LBB40_2
# BB#1:                                 # %._crit_edge.i.i.i.i
	testl	%eax, %eax
	jne	.LBB40_2
	.p2align	4, 0x90
.LBB40_17:                              # %switch.early.test
                                        # =>This Inner Loop Header: Depth=1
	testb	%bl, %bl
	je	.LBB40_26
# BB#18:                                # %switch.early.test
                                        #   in Loop: Header=BB40_17 Depth=1
	cmpb	$95, %bl
	je	.LBB40_26
# BB#19:                                #   in Loop: Header=BB40_17 Depth=1
	leaq	1(%r13), %rax
	movq	%rax, (%r14)
	movzbl	(%r13), %r13d
	movq	(%r15), %rax
	movq	8(%r15), %rbx
	leaq	1(%rbx), %rbp
	cmpq	%r12, %rax
	je	.LBB40_20
# BB#21:                                #   in Loop: Header=BB40_17 Depth=1
	movq	16(%r15), %rcx
	cmpq	%rcx, %rbp
	ja	.LBB40_23
	jmp	.LBB40_25
	.p2align	4, 0x90
.LBB40_20:                              #   in Loop: Header=BB40_17 Depth=1
	movl	$15, %ecx
	cmpq	%rcx, %rbp
	jbe	.LBB40_25
.LBB40_23:                              #   in Loop: Header=BB40_17 Depth=1
.Ltmp0:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm
.Ltmp1:
# BB#24:                                # %.noexc19
                                        #   in Loop: Header=BB40_17 Depth=1
	movq	(%r15), %rax
.LBB40_25:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEpLEc.exit20
                                        #   in Loop: Header=BB40_17 Depth=1
	movb	%r13b, (%rax,%rbx)
	movq	%rbp, 8(%r15)
	movq	(%r15), %rax
	movb	$0, 1(%rax,%rbx)
	movq	(%r14), %r13
	movb	(%r13), %bl
	movsbl	%bl, %edi
	callq	isalnum
	testl	%eax, %eax
	je	.LBB40_17
	jmp	.LBB40_26
.LBB40_2:                               # %.critedge.preheader
	xorl	%ebx, %ebx
	movq	%r12, %rax
	jmp	.LBB40_3
	.p2align	4, 0x90
.LBB40_11:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEpLEc.exit..critedge_crit_edge
                                        #   in Loop: Header=BB40_3 Depth=1
	movq	(%r15), %rax
	movq	8(%r15), %rbx
.LBB40_3:                               # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%r13), %rcx
	movq	%rcx, (%r14)
	movzbl	(%r13), %r13d
	leaq	1(%rbx), %rbp
	cmpq	%r12, %rax
	je	.LBB40_4
# BB#5:                                 #   in Loop: Header=BB40_3 Depth=1
	movq	16(%r15), %rcx
	cmpq	%rcx, %rbp
	ja	.LBB40_7
	jmp	.LBB40_9
	.p2align	4, 0x90
.LBB40_4:                               #   in Loop: Header=BB40_3 Depth=1
	movl	$15, %ecx
	cmpq	%rcx, %rbp
	jbe	.LBB40_9
.LBB40_7:                               #   in Loop: Header=BB40_3 Depth=1
.Ltmp3:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm
.Ltmp4:
# BB#8:                                 # %.noexc16
                                        #   in Loop: Header=BB40_3 Depth=1
	movq	(%r15), %rax
.LBB40_9:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEpLEc.exit
                                        #   in Loop: Header=BB40_3 Depth=1
	movb	%r13b, (%rax,%rbx)
	movq	%rbp, 8(%r15)
	movq	(%r15), %rax
	movb	$0, 1(%rax,%rbx)
	movq	(%r14), %r13
	movsbl	(%r13), %ebx
	movl	%ebx, %edi
	callq	isalnum
	cmpl	$95, %ebx
	je	.LBB40_11
# BB#10:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEpLEc.exit
                                        #   in Loop: Header=BB40_3 Depth=1
	testl	%eax, %eax
	jne	.LBB40_11
.LBB40_26:                              # %.critedge14
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB40_13:                              # %.loopexit.split-lp
.Ltmp2:
	jmp	.LBB40_14
.LBB40_12:                              # %.loopexit
.Ltmp5:
.LBB40_14:
	movq	%rax, %rbx
	movq	(%r15), %rdi
	cmpq	%r12, %rdi
	je	.LBB40_16
# BB#15:
	callq	_ZdlPv
.LBB40_16:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end40:
	.size	_ZN2kc20f_getidentfromstringB5cxx11EPPKc, .Lfunc_end40-_ZN2kc20f_getidentfromstringB5cxx11EPPKc
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end40-.Ltmp4     #   Call between .Ltmp4 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	The_abstract_phylum_decl,@object # @The_abstract_phylum_decl
	.bss
	.globl	The_abstract_phylum_decl
	.p2align	3
The_abstract_phylum_decl:
	.quad	0
	.size	The_abstract_phylum_decl, 8

	.type	The_abstract_phylum_ref_decl,@object # @The_abstract_phylum_ref_decl
	.globl	The_abstract_phylum_ref_decl
	.p2align	3
The_abstract_phylum_ref_decl:
	.quad	0
	.size	The_abstract_phylum_ref_decl, 8

	.type	The_abstract_list_decl,@object # @The_abstract_list_decl
	.globl	The_abstract_list_decl
	.p2align	3
The_abstract_list_decl:
	.quad	0
	.size	The_abstract_list_decl, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"f_something_to_initialize"
	.size	.L.str, 26

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/kimwitu++/gutil.cc"
	.size	.L.str.1, 82

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"f_NilCtexts"
	.size	.L.str.2, 12

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"f_phylumofwithcasesinfo"
	.size	.L.str.3, 24

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Error: can not find type of with expression"
	.size	.L.str.4, 44

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"KC_ERRORunknownTYPE"
	.size	.L.str.5, 20

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"f_phylumofpatternrepresentation"
	.size	.L.str.6, 32

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"sort_extend_parameter_type_list"
	.size	.L.str.7, 32

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"t_sort_extend_parameter_list"
	.size	.L.str.8, 29

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%d"
	.size	.L.str.9, 3

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%d_%d"
	.size	.L.str.10, 6

	.type	_ZL17global_filterview,@object # @_ZL17global_filterview
	.local	_ZL17global_filterview
	.comm	_ZL17global_filterview,8,8
	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"f_typeof"
	.size	.L.str.11, 9

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"f_operatorofpatternrepresentation"
	.size	.L.str.12, 34

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"f_typeofunpsubterm"
	.size	.L.str.13, 19

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"f_outmost_nl_preds_in_rewriterulesinfo"
	.size	.L.str.14, 39

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"f_outmost_nl_preds_in_unparsedeclsinfo"
	.size	.L.str.15, 39

	.type	_ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known,@object # @_ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known
	.local	_ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known
	.comm	_ZZN2kc19f_is_known_ptr_typeEPNS_7impl_IDEE5known,8,8
	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"size_t"
	.size	.L.str.16, 7

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"unsigned"
	.size	.L.str.17, 9

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"enum_phyla"
	.size	.L.str.18, 11

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"enum_operators"
	.size	.L.str.19, 15

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"KC_UNIQ_INFO"
	.size	.L.str.20, 13

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"bool"
	.size	.L.str.21, 5

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"hashtable_t"
	.size	.L.str.22, 12

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"KC_IO_STATUS"
	.size	.L.str.23, 13

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"uview"
	.size	.L.str.24, 6

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"rview"
	.size	.L.str.25, 6

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"rewrite_withcasesinfo"
	.size	.L.str.26, 22

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"f_attributes_to_initialize"
	.size	.L.str.28, 27

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"post_create"
	.size	.L.str.29, 12

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"rewrite"
	.size	.L.str.30, 8

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"more than one type defined for function argument:"
	.size	.L.str.31, 50

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"can not find type of function argument:"
	.size	.L.str.32, 40

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"is_viewname_in_rewriteruleinfo"
	.size	.L.str.33, 31

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"is_viewname_in_unparsedeclinfo"
	.size	.L.str.34, 31

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"f_operatorofelem_patternrepresentation"
	.size	.L.str.35, 39

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"f_operatorofpath"
	.size	.L.str.36, 17

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"f_operatorofpaths"
	.size	.L.str.37, 18

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"f_outmost_nl_preds_in_patternrepresentation"
	.size	.L.str.38, 44

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"f_outmost_nl_preds_in_paths"
	.size	.L.str.39, 28


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
