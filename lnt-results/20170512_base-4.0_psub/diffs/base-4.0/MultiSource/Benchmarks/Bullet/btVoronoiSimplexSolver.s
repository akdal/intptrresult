	.text
	.file	"btVoronoiSimplexSolver.bc"
	.globl	_ZN22btVoronoiSimplexSolver12removeVertexEi
	.p2align	4, 0x90
	.type	_ZN22btVoronoiSimplexSolver12removeVertexEi,@function
_ZN22btVoronoiSimplexSolver12removeVertexEi: # @_ZN22btVoronoiSimplexSolver12removeVertexEi
	.cfi_startproc
# BB#0:
	movslq	(%rdi), %rax
	decq	%rax
	movl	%eax, (%rdi)
	shlq	$4, %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	movups	4(%rdi,%rax), %xmm0
	movups	%xmm0, 4(%rdi,%rcx)
	movslq	(%rdi), %rax
	shlq	$4, %rax
	movups	84(%rdi,%rax), %xmm0
	movups	%xmm0, 84(%rdi,%rcx)
	movslq	(%rdi), %rax
	shlq	$4, %rax
	movups	164(%rdi,%rax), %xmm0
	movups	%xmm0, 164(%rdi,%rcx)
	retq
.Lfunc_end0:
	.size	_ZN22btVoronoiSimplexSolver12removeVertexEi, .Lfunc_end0-_ZN22btVoronoiSimplexSolver12removeVertexEi
	.cfi_endproc

	.globl	_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield
	.p2align	4, 0x90
	.type	_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield,@function
_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield: # @_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield
	.cfi_startproc
# BB#0:
	movslq	(%rdi), %rcx
	cmpq	$4, %rcx
	jl	.LBB1_3
# BB#1:
	testb	$8, (%rsi)
	movl	%ecx, %eax
	jne	.LBB1_4
# BB#2:
	decq	%rcx
	movl	%ecx, (%rdi)
	movq	%rcx, %rax
	shlq	$4, %rax
	movups	4(%rdi,%rax), %xmm0
	movups	%xmm0, 52(%rdi)
	movups	84(%rdi,%rax), %xmm0
	movups	%xmm0, 132(%rdi)
	movups	164(%rdi,%rax), %xmm0
	movups	%xmm0, 212(%rdi)
	movl	%ecx, %eax
	testb	$4, (%rsi)
	je	.LBB1_5
	jmp	.LBB1_7
.LBB1_3:                                # %thread-pre-split
	movl	$3, %eax
	cmpl	$3, %ecx
	jne	.LBB1_6
.LBB1_4:                                # %thread-pre-split.thread
	testb	$4, (%rsi)
	jne	.LBB1_7
.LBB1_5:
	cltq
	movq	%rax, %rcx
	shlq	$4, %rcx
	decl	%eax
	movl	%eax, (%rdi)
	movups	-12(%rdi,%rcx), %xmm0
	movups	%xmm0, 36(%rdi)
	movups	68(%rdi,%rcx), %xmm0
	movups	%xmm0, 116(%rdi)
	movups	148(%rdi,%rcx), %xmm0
	movups	%xmm0, 196(%rdi)
	testb	$2, (%rsi)
	je	.LBB1_8
	jmp	.LBB1_10
.LBB1_6:
	movl	$2, %eax
	cmpl	$2, %ecx
	jl	.LBB1_9
.LBB1_7:                                # %.thread
	testb	$2, (%rsi)
	jne	.LBB1_10
.LBB1_8:
	cltq
	movq	%rax, %rcx
	shlq	$4, %rcx
	decl	%eax
	movl	%eax, (%rdi)
	movups	-12(%rdi,%rcx), %xmm0
	movups	%xmm0, 20(%rdi)
	movups	68(%rdi,%rcx), %xmm0
	movups	%xmm0, 100(%rdi)
	movups	148(%rdi,%rcx), %xmm0
	movups	%xmm0, 180(%rdi)
.LBB1_10:                               # %thread-pre-split5.thread
	testb	$1, (%rsi)
	jne	.LBB1_12
# BB#11:
	cltq
	movq	%rax, %rcx
	shlq	$4, %rcx
	decl	%eax
	movl	%eax, (%rdi)
	movups	-12(%rdi,%rcx), %xmm0
	movups	%xmm0, 4(%rdi)
	movups	68(%rdi,%rcx), %xmm0
	movups	%xmm0, 84(%rdi)
	movups	148(%rdi,%rcx), %xmm0
	movups	%xmm0, 164(%rdi)
.LBB1_12:
	retq
.LBB1_9:                                # %thread-pre-split5
	movl	$1, %eax
	cmpl	$1, %ecx
	je	.LBB1_10
	jmp	.LBB1_12
.Lfunc_end1:
	.size	_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield, .Lfunc_end1-_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield
	.cfi_endproc

	.globl	_ZN22btVoronoiSimplexSolver5resetEv
	.p2align	4, 0x90
	.type	_ZN22btVoronoiSimplexSolver5resetEv,@function
_ZN22btVoronoiSimplexSolver5resetEv:    # @_ZN22btVoronoiSimplexSolver5resetEv
	.cfi_startproc
# BB#0:
	movb	$0, 308(%rdi)
	movl	$0, (%rdi)
	movb	$1, 352(%rdi)
	movabsq	$6727827449093950315, %rax # imm = 0x5D5E0B6B5D5E0B6B
	movq	%rax, 292(%rdi)
	movq	$1566444395, 300(%rdi)  # imm = 0x5D5E0B6B
	xorps	%xmm0, %xmm0
	movups	%xmm0, 332(%rdi)
	movb	$0, 348(%rdi)
	andb	$-16, 328(%rdi)
	retq
.Lfunc_end2:
	.size	_ZN22btVoronoiSimplexSolver5resetEv, .Lfunc_end2-_ZN22btVoronoiSimplexSolver5resetEv
	.cfi_endproc

	.globl	_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_
	.p2align	4, 0x90
	.type	_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_,@function
_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_: # @_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_
	.cfi_startproc
# BB#0:
	movups	(%rsi), %xmm0
	movups	%xmm0, 292(%rdi)
	movb	$1, 352(%rdi)
	movslq	(%rdi), %rax
	shlq	$4, %rax
	movups	(%rsi), %xmm0
	movups	%xmm0, 4(%rdi,%rax)
	movslq	(%rdi), %rax
	shlq	$4, %rax
	movups	(%rdx), %xmm0
	movups	%xmm0, 84(%rdi,%rax)
	movslq	(%rdi), %rax
	shlq	$4, %rax
	movups	(%rcx), %xmm0
	movups	%xmm0, 164(%rdi,%rax)
	incl	(%rdi)
	retq
.Lfunc_end3:
	.size	_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_, .Lfunc_end3-_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1065353216              # float 1
.LCPI4_1:
	.long	0                       # float 0
	.text
	.globl	_ZN22btVoronoiSimplexSolver28updateClosestVectorAndPointsEv
	.p2align	4, 0x90
	.type	_ZN22btVoronoiSimplexSolver28updateClosestVectorAndPointsEv,@function
_ZN22btVoronoiSimplexSolver28updateClosestVectorAndPointsEv: # @_ZN22btVoronoiSimplexSolver28updateClosestVectorAndPointsEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 48
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpb	$0, 352(%rbx)
	je	.LBB4_4
# BB#1:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 332(%rbx)
	movb	$0, 348(%rbx)
	movb	328(%rbx), %al
	andb	$-16, %al
	movb	%al, 328(%rbx)
	movb	$0, 352(%rbx)
	movl	(%rbx), %ecx
	cmpq	$4, %rcx
	ja	.LBB4_18
# BB#2:
	leaq	312(%rbx), %r9
	jmpq	*.LJTI4_0(,%rcx,8)
.LBB4_3:                                # %_ZN25btSubSimplexClosestResult7isValidEv.exit278
	movups	84(%rbx), %xmm0
	movups	%xmm0, 244(%rbx)
	movups	164(%rbx), %xmm0
	movups	%xmm0, 260(%rbx)
	movsd	244(%rbx), %xmm0        # xmm0 = mem[0],zero
	movsd	260(%rbx), %xmm1        # xmm1 = mem[0],zero
	subps	%xmm1, %xmm0
	movss	252(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	268(%rbx), %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 276(%rbx)
	movlps	%xmm2, 284(%rbx)
	movb	$0, 348(%rbx)
	movb	%al, 328(%rbx)
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movups	%xmm0, 332(%rbx)
	movb	$1, 308(%rbx)
	movb	$1, %al
	jmp	.LBB4_19
.LBB4_4:                                # %._crit_edge
	movb	308(%rbx), %al
	jmp	.LBB4_19
.LBB4_5:
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	xorps	%xmm8, %xmm8
	xorps	%xmm5, %xmm5
	subss	%xmm1, %xmm5
	xorps	%xmm6, %xmm6
	subss	%xmm4, %xmm6
	movss	12(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	subss	%xmm7, %xmm0
	movss	20(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm3
	movss	24(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm2
	movss	28(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm1
	mulss	%xmm3, %xmm5
	mulss	%xmm2, %xmm6
	addss	%xmm5, %xmm6
	mulss	%xmm1, %xmm0
	addss	%xmm6, %xmm0
	movb	$1, %cl
	ucomiss	%xmm8, %xmm0
	jbe	.LBB4_16
# BB#6:
	mulss	%xmm3, %xmm3
	mulss	%xmm2, %xmm2
	addss	%xmm3, %xmm2
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	movb	$2, %cl
	ucomiss	%xmm0, %xmm1
	jbe	.LBB4_20
# BB#7:
	divss	%xmm1, %xmm0
	orb	$1, %al
	movb	%al, 328(%rbx)
	jmp	.LBB4_21
.LBB4_8:
	leaq	4(%rbx), %rdx
	leaq	20(%rbx), %rcx
	leaq	36(%rbx), %r8
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	leaq	16(%rsp), %rsi
	callq	_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult
	movsd	84(%rbx), %xmm2         # xmm2 = mem[0],zero
	movss	332(%rbx), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movss	336(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm9
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	mulps	%xmm9, %xmm2
	movss	92(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	movsd	100(%rbx), %xmm7        # xmm7 = mem[0],zero
	movaps	%xmm0, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm6, %xmm7
	movss	108(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addps	%xmm2, %xmm7
	addss	%xmm3, %xmm1
	movss	340(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movsd	116(%rbx), %xmm2        # xmm2 = mem[0],zero
	movaps	%xmm3, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm5, %xmm2
	movss	124(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	addps	%xmm7, %xmm2
	addss	%xmm1, %xmm4
	xorps	%xmm1, %xmm1
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	movlps	%xmm2, 244(%rbx)
	movlps	%xmm1, 252(%rbx)
	movsd	164(%rbx), %xmm1        # xmm1 = mem[0],zero
	mulps	%xmm9, %xmm1
	mulss	172(%rbx), %xmm8
	movsd	180(%rbx), %xmm7        # xmm7 = mem[0],zero
	mulps	%xmm6, %xmm7
	mulss	188(%rbx), %xmm0
	addps	%xmm1, %xmm7
	addss	%xmm8, %xmm0
	movsd	196(%rbx), %xmm1        # xmm1 = mem[0],zero
	mulps	%xmm5, %xmm1
	mulss	204(%rbx), %xmm3
	addps	%xmm7, %xmm1
	addss	%xmm0, %xmm3
	xorps	%xmm0, %xmm0
	movss	%xmm3, %xmm0            # xmm0 = xmm3[0],xmm0[1,2,3]
	movlps	%xmm1, 260(%rbx)
	movlps	%xmm0, 268(%rbx)
	subps	%xmm1, %xmm2
	subss	%xmm3, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm2, 276(%rbx)
	jmp	.LBB4_9
.LBB4_14:
	leaq	4(%rbx), %rdx
	leaq	20(%rbx), %rcx
	leaq	36(%rbx), %r8
	leaq	52(%rbx), %rax
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movq	%r9, (%rsp)
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %r9
	callq	_ZN22btVoronoiSimplexSolver25closestPtPointTetrahedronERK9btVector3S2_S2_S2_S2_R25btSubSimplexClosestResult
	testb	%al, %al
	je	.LBB4_17
# BB#15:
	movsd	84(%rbx), %xmm1         # xmm1 = mem[0],zero
	movss	332(%rbx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movss	336(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm8
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	mulps	%xmm8, %xmm1
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm2
	movsd	100(%rbx), %xmm3        # xmm3 = mem[0],zero
	movaps	%xmm0, %xmm10
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	mulps	%xmm10, %xmm3
	movss	108(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addps	%xmm1, %xmm3
	addss	%xmm2, %xmm4
	movss	340(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movsd	116(%rbx), %xmm7        # xmm7 = mem[0],zero
	movaps	%xmm2, %xmm11
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm11, %xmm7
	movss	124(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	addps	%xmm3, %xmm7
	addss	%xmm4, %xmm6
	movss	344(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movsd	132(%rbx), %xmm3        # xmm3 = mem[0],zero
	movaps	%xmm4, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm1, %xmm3
	movss	140(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	addps	%xmm7, %xmm3
	addss	%xmm6, %xmm5
	xorps	%xmm6, %xmm6
	movss	%xmm5, %xmm6            # xmm6 = xmm5[0],xmm6[1,2,3]
	movlps	%xmm3, 244(%rbx)
	movlps	%xmm6, 252(%rbx)
	movsd	164(%rbx), %xmm6        # xmm6 = mem[0],zero
	mulps	%xmm8, %xmm6
	mulss	172(%rbx), %xmm9
	movsd	180(%rbx), %xmm7        # xmm7 = mem[0],zero
	mulps	%xmm10, %xmm7
	mulss	188(%rbx), %xmm0
	addps	%xmm6, %xmm7
	addss	%xmm9, %xmm0
	movsd	196(%rbx), %xmm6        # xmm6 = mem[0],zero
	mulps	%xmm11, %xmm6
	mulss	204(%rbx), %xmm2
	addps	%xmm7, %xmm6
	addss	%xmm0, %xmm2
	movsd	212(%rbx), %xmm0        # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	mulss	220(%rbx), %xmm4
	addps	%xmm6, %xmm0
	addss	%xmm2, %xmm4
	xorps	%xmm1, %xmm1
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	movlps	%xmm0, 260(%rbx)
	movlps	%xmm1, 268(%rbx)
	subps	%xmm0, %xmm3
	subss	%xmm4, %xmm5
	xorps	%xmm0, %xmm0
	movss	%xmm5, %xmm0            # xmm0 = xmm5[0],xmm0[1,2,3]
	movlps	%xmm3, 276(%rbx)
.LBB4_9:
	movlps	%xmm0, 284(%rbx)
	leaq	328(%rbx), %rsi
	movq	%rbx, %rdi
	callq	_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield
	movss	332(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm1
	jb	.LBB4_22
# BB#10:
	movss	336(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB4_22
# BB#11:
	movss	340(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm1
	jb	.LBB4_22
# BB#12:
	movss	344(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jmp	.LBB4_13
.LBB4_16:
	xorps	%xmm0, %xmm0
	jmp	.LBB4_21
.LBB4_17:
	cmpb	$0, 348(%rbx)
	je	.LBB4_26
.LBB4_18:
	movb	$0, 308(%rbx)
	xorl	%eax, %eax
	jmp	.LBB4_19
.LBB4_20:
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB4_21:
	orb	%cl, %al
	leaq	328(%rbx), %rsi
	movb	%al, 328(%rbx)
	movss	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 332(%rbx)
	movss	%xmm0, 336(%rbx)
	movq	$0, 340(%rbx)
	movss	100(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	84(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	88(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movss	104(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm2
	movss	108(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	92(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm3
	mulss	%xmm0, %xmm1
	mulss	%xmm0, %xmm2
	mulss	%xmm0, %xmm3
	addss	%xmm4, %xmm1
	addss	%xmm5, %xmm2
	addss	%xmm6, %xmm3
	movaps	%xmm1, %xmm5
	unpcklps	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1]
	xorps	%xmm8, %xmm8
	xorps	%xmm6, %xmm6
	movss	%xmm3, %xmm6            # xmm6 = xmm3[0],xmm6[1,2,3]
	movlps	%xmm5, 244(%rbx)
	movlps	%xmm6, 252(%rbx)
	movss	180(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	164(%rbx), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movss	168(%rbx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm5
	movss	184(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	subss	%xmm9, %xmm4
	movss	188(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	172(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm7
	mulss	%xmm0, %xmm5
	mulss	%xmm0, %xmm4
	mulss	%xmm0, %xmm7
	addss	%xmm10, %xmm5
	addss	%xmm9, %xmm4
	addss	%xmm6, %xmm7
	subss	%xmm5, %xmm1
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	xorps	%xmm0, %xmm0
	movss	%xmm7, %xmm0            # xmm0 = xmm7[0],xmm0[1,2,3]
	movlps	%xmm5, 260(%rbx)
	movlps	%xmm0, 268(%rbx)
	subss	%xmm4, %xmm2
	subss	%xmm7, %xmm3
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movss	%xmm3, %xmm8            # xmm8 = xmm3[0],xmm8[1,2,3]
	movlps	%xmm1, 276(%rbx)
	movlps	%xmm8, 284(%rbx)
	movq	%rbx, %rdi
	callq	_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield
	movss	332(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI4_1, %xmm0
	jb	.LBB4_22
# BB#23:
	movss	336(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm1
	jb	.LBB4_22
# BB#24:
	movss	340(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB4_25
.LBB4_22:
	xorl	%eax, %eax
	movb	%al, 308(%rbx)
.LBB4_19:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$32, %rsp
	popq	%rbx
	retq
.LBB4_26:
	movb	$1, 308(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 276(%rbx)
	movb	$1, %al
	jmp	.LBB4_19
.LBB4_25:
	movss	344(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
.LBB4_13:
	setae	%al
	movb	%al, 308(%rbx)
	jmp	.LBB4_19
.Lfunc_end4:
	.size	_ZN22btVoronoiSimplexSolver28updateClosestVectorAndPointsEv, .Lfunc_end4-_ZN22btVoronoiSimplexSolver28updateClosestVectorAndPointsEv
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_18
	.quad	.LBB4_3
	.quad	.LBB4_5
	.quad	.LBB4_8
	.quad	.LBB4_14

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult
	.p2align	4, 0x90
	.type	_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult,@function
_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult: # @_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult
	.cfi_startproc
# BB#0:
	movb	16(%r9), %al
	andb	$-16, %al
	movb	%al, 16(%r9)
	movsd	(%rcx), %xmm8           # xmm8 = mem[0],zero
	movq	(%rdx), %xmm4           # xmm4 = mem[0],zero
	pshufd	$229, %xmm4, %xmm0      # xmm0 = xmm4[1,1,2,3]
	movaps	%xmm8, %xmm11
	subps	%xmm4, %xmm11
	movss	8(%rcx), %xmm14         # xmm14 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movsd	(%r8), %xmm15           # xmm15 = mem[0],zero
	movaps	%xmm15, -88(%rsp)       # 16-byte Spill
	subps	%xmm4, %xmm15
	movss	8(%r8), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, -24(%rsp)       # 16-byte Spill
	movaps	%xmm1, -40(%rsp)        # 16-byte Spill
	unpcklps	%xmm1, %xmm14   # xmm14 = xmm14[0],xmm1[0],xmm14[1],xmm1[1]
	movaps	%xmm6, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	subps	%xmm1, %xmm14
	movss	(%rsi), %xmm10          # xmm10 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm1
	subss	%xmm4, %xmm1
	movaps	%xmm12, %xmm5
	subss	%xmm0, %xmm5
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm2
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movaps	%xmm11, %xmm3
	shufps	$17, %xmm15, %xmm3      # xmm3 = xmm3[1,0],xmm15[1,0]
	shufps	$226, %xmm15, %xmm3     # xmm3 = xmm3[2,0],xmm15[2,3]
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm1, %xmm5
	movaps	%xmm0, %xmm7
	subss	%xmm6, %xmm7
	movaps	%xmm14, %xmm13
	shufps	$225, %xmm13, %xmm13    # xmm13 = xmm13[1,0,2,3]
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm13, %xmm7
	addps	%xmm5, %xmm7
	movaps	%xmm7, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm9, %xmm9
	ucomiss	%xmm1, %xmm9
	jb	.LBB5_3
# BB#1:
	ucomiss	%xmm7, %xmm9
	jb	.LBB5_3
# BB#2:
	movups	(%rdx), %xmm0
	movups	%xmm0, (%r9)
	orb	$1, %al
	movb	%al, 16(%r9)
	xorps	%xmm1, %xmm1
	movss	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm9, %xmm9
	jmp	.LBB5_22
.LBB5_3:
	movaps	%xmm4, -120(%rsp)       # 16-byte Spill
	movaps	%xmm6, -104(%rsp)       # 16-byte Spill
	movaps	%xmm10, %xmm5
	subss	%xmm8, %xmm5
	movaps	%xmm8, -72(%rsp)        # 16-byte Spill
	movaps	%xmm8, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm0, %xmm4
	movaps	%xmm12, %xmm0
	subss	%xmm6, %xmm0
	movaps	%xmm4, -56(%rsp)        # 16-byte Spill
	movaps	%xmm4, %xmm6
	movaps	-24(%rsp), %xmm4        # 16-byte Reload
	subss	%xmm4, %xmm6
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm5, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm3
	addps	%xmm2, %xmm3
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm6, %xmm13
	addps	%xmm3, %xmm13
	movaps	%xmm13, %xmm8
	shufps	$229, %xmm8, %xmm8      # xmm8 = xmm8[1,1,2,3]
	ucomiss	%xmm9, %xmm8
	jb	.LBB5_6
# BB#4:
	ucomiss	%xmm13, %xmm8
	jb	.LBB5_6
# BB#5:
	movups	(%rcx), %xmm0
	movups	%xmm0, (%r9)
	orb	$2, %al
	movb	%al, 16(%r9)
	movss	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	xorps	%xmm9, %xmm9
	jmp	.LBB5_22
.LBB5_6:
	movaps	%xmm1, %xmm2
	mulss	%xmm13, %xmm2
	movaps	%xmm8, %xmm0
	mulss	%xmm7, %xmm0
	subss	%xmm0, %xmm2
	xorps	%xmm3, %xmm3
	ucomiss	%xmm8, %xmm3
	jb	.LBB5_10
# BB#7:
	ucomiss	%xmm3, %xmm1
	jb	.LBB5_10
# BB#8:
	xorps	%xmm9, %xmm9
	ucomiss	%xmm2, %xmm9
	jb	.LBB5_10
# BB#9:
	movaps	%xmm1, %xmm0
	subss	%xmm8, %xmm0
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm11
	mulss	%xmm1, %xmm14
	movaps	-120(%rsp), %xmm3       # 16-byte Reload
	addps	%xmm11, %xmm3
	movaps	-104(%rsp), %xmm2       # 16-byte Reload
	addss	%xmm14, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm3, (%r9)
	movlps	%xmm0, 8(%r9)
	orb	$3, %al
	movb	%al, 16(%r9)
	movss	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	jmp	.LBB5_22
.LBB5_10:
	movaps	-88(%rsp), %xmm0        # 16-byte Reload
	subss	%xmm0, %xmm10
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm0, %xmm12
	movaps	-40(%rsp), %xmm9        # 16-byte Reload
	movaps	-56(%rsp), %xmm1        # 16-byte Reload
	subss	%xmm9, %xmm1
	movaps	%xmm11, %xmm0
	unpcklps	%xmm15, %xmm0   # xmm0 = xmm0[0],xmm15[0],xmm0[1],xmm15[1]
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	mulps	%xmm0, %xmm10
	movaps	%xmm15, %xmm0
	shufps	$17, %xmm11, %xmm0      # xmm0 = xmm0[1,0],xmm11[1,0]
	shufps	$226, %xmm11, %xmm0     # xmm0 = xmm0[2,0],xmm11[2,3]
	shufps	$224, %xmm12, %xmm12    # xmm12 = xmm12[0,0,2,3]
	mulps	%xmm0, %xmm12
	addps	%xmm10, %xmm12
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm14, %xmm1
	addps	%xmm12, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	ucomiss	%xmm3, %xmm0
	jb	.LBB5_13
# BB#11:
	ucomiss	%xmm1, %xmm0
	jb	.LBB5_13
# BB#12:
	movups	(%r8), %xmm0
	movups	%xmm0, (%r9)
	orb	$4, %al
	movb	%al, 16(%r9)
	movss	.LCPI5_0(%rip), %xmm9   # xmm9 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	jmp	.LBB5_22
.LBB5_13:
	movaps	%xmm4, %xmm10
	movaps	%xmm14, %xmm12
	shufps	$229, %xmm12, %xmm12    # xmm12 = xmm12[1,1,2,3]
	movaps	%xmm1, %xmm4
	movaps	%xmm1, %xmm5
	mulps	%xmm7, %xmm5
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	subss	%xmm1, %xmm5
	xorps	%xmm3, %xmm3
	ucomiss	%xmm0, %xmm3
	jb	.LBB5_17
# BB#14:
	ucomiss	%xmm3, %xmm7
	jb	.LBB5_17
# BB#15:
	xorps	%xmm1, %xmm1
	ucomiss	%xmm5, %xmm1
	jb	.LBB5_17
# BB#16:
	movaps	%xmm7, %xmm2
	subss	%xmm0, %xmm2
	divss	%xmm2, %xmm7
	movaps	%xmm7, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm15
	mulss	%xmm7, %xmm12
	movaps	-120(%rsp), %xmm3       # 16-byte Reload
	addps	%xmm15, %xmm3
	movaps	-104(%rsp), %xmm2       # 16-byte Reload
	addss	%xmm12, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm3, (%r9)
	movlps	%xmm0, 8(%r9)
	orb	$5, %al
	movb	%al, 16(%r9)
	movss	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm0
	movaps	%xmm7, %xmm9
	jmp	.LBB5_22
.LBB5_17:
	movaps	%xmm8, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm4, %xmm6
	mulss	%xmm13, %xmm6
	subss	%xmm6, %xmm1
	ucomiss	%xmm1, %xmm3
	jb	.LBB5_21
# BB#18:
	subss	%xmm8, %xmm13
	xorps	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm13
	jb	.LBB5_21
# BB#19:
	subss	%xmm0, %xmm4
	movaps	%xmm4, %xmm0
	ucomiss	%xmm3, %xmm4
	jae	.LBB5_20
.LBB5_21:
	addss	%xmm5, %xmm1
	addss	%xmm2, %xmm1
	movss	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm9
	divss	%xmm1, %xmm9
	mulss	%xmm9, %xmm5
	mulss	%xmm2, %xmm9
	movaps	%xmm5, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm1, %xmm11
	mulss	%xmm5, %xmm14
	addps	-120(%rsp), %xmm11      # 16-byte Folded Reload
	addss	-104(%rsp), %xmm14      # 16-byte Folded Reload
	movaps	%xmm9, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm1, %xmm15
	mulss	%xmm9, %xmm12
	addps	%xmm11, %xmm15
	addss	%xmm14, %xmm12
	xorps	%xmm1, %xmm1
	movss	%xmm12, %xmm1           # xmm1 = xmm12[0],xmm1[1,2,3]
	movlps	%xmm15, (%r9)
	movlps	%xmm1, 8(%r9)
	orb	$7, %al
	movb	%al, 16(%r9)
	subss	%xmm5, %xmm0
	subss	%xmm9, %xmm0
	movaps	%xmm5, %xmm1
.LBB5_22:
	movss	%xmm0, 20(%r9)
	movss	%xmm1, 24(%r9)
	movss	%xmm9, 28(%r9)
	movl	$0, 32(%r9)
	movb	$1, %al
	retq
.LBB5_20:
	addss	%xmm13, %xmm0
	divss	%xmm0, %xmm13
	movaps	-72(%rsp), %xmm1        # 16-byte Reload
	movaps	-88(%rsp), %xmm2        # 16-byte Reload
	subps	%xmm1, %xmm2
	subss	%xmm10, %xmm9
	movaps	%xmm13, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	mulss	%xmm13, %xmm9
	addps	%xmm1, %xmm0
	addss	%xmm10, %xmm9
	xorps	%xmm1, %xmm1
	movss	%xmm9, %xmm1            # xmm1 = xmm9[0],xmm1[1,2,3]
	movlps	%xmm0, (%r9)
	movlps	%xmm1, 8(%r9)
	orb	$6, %al
	movb	%al, 16(%r9)
	movss	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	subss	%xmm13, %xmm1
	xorps	%xmm0, %xmm0
	movaps	%xmm13, %xmm9
	jmp	.LBB5_22
.Lfunc_end5:
	.size	_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult, .Lfunc_end5-_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	841731190               # float 9.99999905E-9
.LCPI6_1:
	.long	2139095039              # float 3.40282347E+38
	.text
	.globl	_ZN22btVoronoiSimplexSolver25closestPtPointTetrahedronERK9btVector3S2_S2_S2_S2_R25btSubSimplexClosestResult
	.p2align	4, 0x90
	.type	_ZN22btVoronoiSimplexSolver25closestPtPointTetrahedronERK9btVector3S2_S2_S2_S2_R25btSubSimplexClosestResult,@function
_ZN22btVoronoiSimplexSolver25closestPtPointTetrahedronERK9btVector3S2_S2_S2_S2_R25btSubSimplexClosestResult: # @_ZN22btVoronoiSimplexSolver25closestPtPointTetrahedronERK9btVector3S2_S2_S2_S2_R25btSubSimplexClosestResult
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi6:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi9:
	.cfi_def_cfa_offset 368
.Lcfi10:
	.cfi_offset %rbx, -56
.Lcfi11:
	.cfi_offset %r12, -48
.Lcfi12:
	.cfi_offset %r13, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	368(%rsp), %rdx
	movb	$0, 24(%rsp)
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rdx)
	orb	$15, 16(%rdx)
	movss	(%rcx), %xmm15          # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm15, 288(%rsp)       # 16-byte Spill
	movss	4(%rcx), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, 272(%rsp)       # 16-byte Spill
	movss	(%r13), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm8          # xmm8 = mem[0],zero,zero,zero
	subss	%xmm9, %xmm15
	subss	%xmm8, %xmm12
	movss	8(%rcx), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, 256(%rsp)       # 16-byte Spill
	movss	8(%r13), %xmm7          # xmm7 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm10
	movaps	%xmm10, 48(%rsp)        # 16-byte Spill
	movss	(%r8), %xmm1            # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 72(%rsp)         # 4-byte Spill
	movss	4(%r8), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 240(%rsp)        # 16-byte Spill
	movaps	%xmm0, %xmm3
	subss	%xmm8, %xmm3
	movss	8(%r8), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 76(%rsp)         # 4-byte Spill
	subss	%xmm7, %xmm0
	movaps	%xmm12, %xmm11
	mulss	%xmm0, %xmm11
	movaps	%xmm0, %xmm13
	movaps	%xmm10, %xmm0
	mulss	%xmm3, %xmm0
	movaps	%xmm3, %xmm4
	subss	%xmm0, %xmm11
	subss	%xmm9, %xmm1
	mulss	%xmm1, %xmm10
	movaps	%xmm15, 208(%rsp)       # 16-byte Spill
	movaps	%xmm15, %xmm0
	mulss	%xmm13, %xmm0
	subss	%xmm0, %xmm10
	mulss	%xmm4, %xmm15
	movaps	%xmm12, %xmm0
	mulss	%xmm1, %xmm0
	subss	%xmm0, %xmm15
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	(%r9), %xmm3            # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, 224(%rsp)        # 16-byte Spill
	movaps	%xmm0, %xmm2
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	unpcklps	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1]
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	subps	%xmm9, %xmm2
	movaps	%xmm2, 96(%rsp)         # 16-byte Spill
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	4(%r9), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, 160(%rsp)        # 16-byte Spill
	movaps	%xmm0, %xmm9
	unpcklps	%xmm3, %xmm9    # xmm9 = xmm9[0],xmm3[0],xmm9[1],xmm3[1]
	unpcklps	%xmm8, %xmm0    # xmm0 = xmm0[0],xmm8[0],xmm0[1],xmm8[1]
	movaps	%xmm0, 176(%rsp)        # 16-byte Spill
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	subps	%xmm8, %xmm9
	movq	%r9, %r12
	movss	8(%r9), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm14         # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm5
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	unpcklps	%xmm7, %xmm14   # xmm14 = xmm14[0],xmm7[0],xmm14[1],xmm7[1]
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	subps	%xmm7, %xmm5
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm2, %xmm11
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	mulps	%xmm9, %xmm10
	addps	%xmm11, %xmm10
	shufps	$224, %xmm15, %xmm15    # xmm15 = xmm15[0,0,2,3]
	mulps	%xmm5, %xmm15
	addps	%xmm10, %xmm15
	movaps	%xmm15, %xmm11
	shufps	$229, %xmm11, %xmm11    # xmm11 = xmm11[1,1,2,3]
	mulss	%xmm11, %xmm15
	xorl	%eax, %eax
	xorps	%xmm0, %xmm0
	ucomiss	%xmm15, %xmm0
	mulss	%xmm11, %xmm11
	seta	%al
	movss	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm11, %xmm0
	movaps	%xmm5, %xmm15
	shufps	$229, %xmm15, %xmm15    # xmm15 = xmm15[1,1,2,3]
	movaps	%xmm4, %xmm10
	movaps	%xmm10, 128(%rsp)       # 16-byte Spill
	movaps	%xmm10, %xmm8
	mulss	%xmm15, %xmm8
	movaps	%xmm9, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	movaps	%xmm13, 144(%rsp)       # 16-byte Spill
	movaps	%xmm13, %xmm7
	mulss	%xmm4, %xmm7
	subss	%xmm7, %xmm8
	movaps	%xmm2, %xmm11
	shufps	$229, %xmm11, %xmm11    # xmm11 = xmm11[1,1,2,3]
	movaps	%xmm13, %xmm7
	mulss	%xmm11, %xmm7
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	movaps	%xmm1, %xmm13
	mulss	%xmm15, %xmm13
	subss	%xmm13, %xmm7
	movaps	%xmm1, %xmm13
	mulss	%xmm4, %xmm13
	mulss	%xmm11, %xmm10
	subss	%xmm10, %xmm13
	movaps	%xmm12, %xmm10
	mulss	%xmm15, %xmm10
	movaps	208(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm0, %xmm15
	movaps	48(%rsp), %xmm6         # 16-byte Reload
	movaps	%xmm6, %xmm1
	mulss	%xmm4, %xmm1
	mulss	%xmm0, %xmm4
	shufps	$0, %xmm2, %xmm0        # xmm0 = xmm0[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm0      # xmm0 = xmm0[2,0],xmm2[2,3]
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	mulps	%xmm0, %xmm8
	movaps	%xmm6, %xmm2
	mulss	%xmm11, %xmm6
	movaps	%xmm6, 48(%rsp)         # 16-byte Spill
	mulss	%xmm12, %xmm11
	shufps	$0, %xmm9, %xmm12       # xmm12 = xmm12[0,0],xmm9[0,0]
	shufps	$226, %xmm9, %xmm12     # xmm12 = xmm12[2,0],xmm9[2,3]
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm12, %xmm7
	addps	%xmm8, %xmm7
	shufps	$0, %xmm5, %xmm2        # xmm2 = xmm2[0,0],xmm5[0,0]
	shufps	$226, %xmm5, %xmm2      # xmm2 = xmm2[2,0],xmm5[2,3]
	shufps	$224, %xmm13, %xmm13    # xmm13 = xmm13[0,0,2,3]
	mulps	%xmm2, %xmm13
	addps	%xmm7, %xmm13
	movl	$-1, %esi
	cmoval	%esi, %eax
	movaps	%xmm13, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm0, %xmm13
	xorl	%r15d, %r15d
	xorps	%xmm6, %xmm6
	ucomiss	%xmm13, %xmm6
	mulss	%xmm0, %xmm0
	seta	%r15b
	movss	.LCPI6_0(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm7
	subss	%xmm10, %xmm1
	subss	48(%rsp), %xmm15        # 16-byte Folded Reload
	subss	%xmm4, %xmm11
	movaps	112(%rsp), %xmm0        # 16-byte Reload
	movaps	96(%rsp), %xmm2         # 16-byte Reload
	shufps	$0, %xmm2, %xmm0        # xmm0 = xmm0[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm0      # xmm0 = xmm0[2,0],xmm2[2,3]
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm0, %xmm1
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	shufps	$0, %xmm9, %xmm0        # xmm0 = xmm0[0,0],xmm9[0,0]
	shufps	$226, %xmm9, %xmm0      # xmm0 = xmm0[2,0],xmm9[2,3]
	shufps	$224, %xmm15, %xmm15    # xmm15 = xmm15[0,0,2,3]
	mulps	%xmm0, %xmm15
	addps	%xmm1, %xmm15
	movaps	144(%rsp), %xmm0        # 16-byte Reload
	shufps	$0, %xmm5, %xmm0        # xmm0 = xmm0[0,0],xmm5[0,0]
	shufps	$226, %xmm5, %xmm0      # xmm0 = xmm0[2,0],xmm5[2,3]
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm0, %xmm11
	cmoval	%esi, %r15d
	addps	%xmm15, %xmm11
	movaps	%xmm11, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm0, %xmm11
	mulss	%xmm0, %xmm0
	xorl	%ebp, %ebp
	ucomiss	%xmm11, %xmm6
	xorps	%xmm10, %xmm10
	seta	%bpl
	ucomiss	%xmm0, %xmm7
	movaps	%xmm7, %xmm11
	movaps	272(%rsp), %xmm0        # 16-byte Reload
	movaps	160(%rsp), %xmm4        # 16-byte Reload
	subss	%xmm0, %xmm4
	movaps	256(%rsp), %xmm1        # 16-byte Reload
	subss	%xmm1, %xmm3
	movaps	240(%rsp), %xmm2        # 16-byte Reload
	subss	%xmm0, %xmm2
	movaps	%xmm0, %xmm9
	movss	76(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm5
	movaps	%xmm1, %xmm8
	movaps	%xmm5, %xmm0
	mulss	%xmm4, %xmm0
	movaps	%xmm4, %xmm6
	movaps	%xmm2, %xmm1
	mulss	%xmm3, %xmm1
	subss	%xmm1, %xmm0
	movaps	288(%rsp), %xmm1        # 16-byte Reload
	movaps	224(%rsp), %xmm4        # 16-byte Reload
	subss	%xmm1, %xmm4
	movss	72(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm7
	mulss	%xmm7, %xmm3
	mulss	%xmm4, %xmm5
	subss	%xmm5, %xmm3
	mulss	%xmm4, %xmm2
	mulss	%xmm6, %xmm7
	subss	%xmm7, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movaps	192(%rsp), %xmm5        # 16-byte Reload
	subps	%xmm1, %xmm5
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	movaps	176(%rsp), %xmm1        # 16-byte Reload
	subps	%xmm9, %xmm1
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	subps	%xmm8, %xmm14
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm5, %xmm0
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	addps	%xmm0, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm14, %xmm2
	cmoval	%esi, %ebp
	addps	%xmm3, %xmm2
	movaps	%xmm2, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm0, %xmm2
	xorl	%r14d, %r14d
	ucomiss	%xmm2, %xmm10
	mulss	%xmm0, %xmm0
	seta	%r14b
	ucomiss	%xmm0, %xmm11
	cmoval	%esi, %r14d
	movl	%r15d, %esi
	orl	%eax, %esi
	orl	%ebp, %esi
	orl	%r14d, %esi
	js	.LBB6_5
# BB#1:
	testl	%esi, %esi
	je	.LBB6_6
# BB#2:
	testl	%eax, %eax
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	je	.LBB6_7
# BB#3:
	leaq	8(%rsp), %r9
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult
	movq	368(%rsp), %rdi
	movss	8(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	subss	(%rbx), %xmm4
	movaps	%xmm2, %xmm5
	subss	4(%rbx), %xmm5
	movaps	%xmm3, %xmm0
	subss	8(%rbx), %xmm0
	mulss	%xmm4, %xmm4
	mulss	%xmm5, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm0, %xmm0
	addss	%xmm5, %xmm0
	movss	.LCPI6_1(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movss	%xmm4, 48(%rsp)         # 4-byte Spill
	ucomiss	%xmm0, %xmm4
	jbe	.LBB6_9
# BB#4:
	movl	20(%rsp), %eax
	movss	%xmm1, (%rdi)
	movss	%xmm2, 4(%rdi)
	movss	%xmm3, 8(%rdi)
	movl	%eax, 12(%rdi)
	movb	16(%rdi), %sil
	andb	$-16, %sil
	movb	24(%rsp), %cl
	movl	%ecx, %edx
	andb	$1, %dl
	movl	%ecx, %eax
	andb	$2, %al
	orb	%dl, %al
	andb	$4, %cl
	orb	%al, %cl
	orb	%sil, %cl
	movb	%cl, 16(%rdi)
	movl	28(%rsp), %eax
	movl	32(%rsp), %ecx
	movl	36(%rsp), %edx
	movl	%eax, 20(%rdi)
	movl	%ecx, 24(%rdi)
	movl	%edx, 28(%rdi)
	movl	$0, 32(%rdi)
	jmp	.LBB6_8
.LBB6_5:
	movb	$1, 36(%rdx)
.LBB6_6:
	xorl	%ebp, %ebp
	jmp	.LBB6_18
.LBB6_7:
	movss	.LCPI6_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB6_8:
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
.LBB6_9:
	testl	%r15d, %r15d
	je	.LBB6_12
# BB#10:
	leaq	8(%rsp), %r9
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	%r12, %r8
	callq	_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult
	movq	368(%rsp), %rdi
	movss	8(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	subss	(%rbx), %xmm4
	movaps	%xmm2, %xmm5
	subss	4(%rbx), %xmm5
	movaps	%xmm3, %xmm0
	subss	8(%rbx), %xmm0
	mulss	%xmm4, %xmm4
	mulss	%xmm5, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm0, %xmm0
	addss	%xmm5, %xmm0
	movss	48(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm4
	jbe	.LBB6_12
# BB#11:
	movl	20(%rsp), %eax
	movss	%xmm1, (%rdi)
	movss	%xmm2, 4(%rdi)
	movss	%xmm3, 8(%rdi)
	movl	%eax, 12(%rdi)
	movb	16(%rdi), %sil
	andb	$-16, %sil
	movb	24(%rsp), %cl
	movl	%ecx, %edx
	andb	$1, %dl
	addb	%cl, %cl
	movl	%ecx, %eax
	andb	$4, %al
	orb	%dl, %al
	andb	$8, %cl
	orb	%al, %cl
	orb	%sil, %cl
	movb	%cl, 16(%rdi)
	movl	28(%rsp), %eax
	movq	32(%rsp), %rcx
	movl	%eax, 20(%rdi)
	movl	$0, 24(%rdi)
	movq	%rcx, 28(%rdi)
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
.LBB6_12:
	testl	%ebp, %ebp
	je	.LBB6_15
# BB#13:
	leaq	8(%rsp), %r9
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	80(%rsp), %r8           # 8-byte Reload
	callq	_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult
	movq	368(%rsp), %rdi
	movss	8(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	subss	(%rbx), %xmm4
	movaps	%xmm2, %xmm5
	subss	4(%rbx), %xmm5
	movaps	%xmm3, %xmm0
	subss	8(%rbx), %xmm0
	mulss	%xmm4, %xmm4
	mulss	%xmm5, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm0, %xmm0
	addss	%xmm5, %xmm0
	movss	48(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm4
	jbe	.LBB6_15
# BB#14:
	movl	20(%rsp), %eax
	movss	%xmm1, (%rdi)
	movss	%xmm2, 4(%rdi)
	movss	%xmm3, 8(%rdi)
	movl	%eax, 12(%rdi)
	movb	16(%rdi), %sil
	andb	$-16, %sil
	movb	24(%rsp), %cl
	movl	%ecx, %edx
	andb	$1, %dl
	movl	%ecx, %eax
	shrb	%al
	andb	$2, %al
	orb	%dl, %al
	shlb	$2, %cl
	andb	$8, %cl
	orb	%al, %cl
	orb	%sil, %cl
	movb	%cl, 16(%rdi)
	movl	36(%rsp), %eax
	movl	28(%rsp), %ecx
	movl	32(%rsp), %edx
	movl	%ecx, 20(%rdi)
	movl	%eax, 24(%rdi)
	movl	$0, 28(%rdi)
	movl	%edx, 32(%rdi)
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
.LBB6_15:
	movb	$1, %bpl
	testl	%r14d, %r14d
	je	.LBB6_18
# BB#16:
	leaq	8(%rsp), %r9
	movq	%rbx, %rsi
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	%r12, %rcx
	movq	88(%rsp), %r8           # 8-byte Reload
	callq	_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	subss	(%rbx), %xmm3
	movaps	%xmm1, %xmm4
	subss	4(%rbx), %xmm4
	movaps	%xmm2, %xmm5
	subss	8(%rbx), %xmm5
	mulss	%xmm3, %xmm3
	mulss	%xmm4, %xmm4
	addss	%xmm3, %xmm4
	mulss	%xmm5, %xmm5
	addss	%xmm4, %xmm5
	movss	48(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm5, %xmm3
	jbe	.LBB6_18
# BB#17:
	movl	20(%rsp), %eax
	movq	368(%rsp), %rcx
	movq	%rcx, %rsi
	movss	%xmm0, (%rsi)
	movss	%xmm1, 4(%rsi)
	movss	%xmm2, 8(%rsi)
	movl	%eax, 12(%rsi)
	movb	16(%rsi), %al
	andb	$-16, %al
	movb	24(%rsp), %dl
	movl	%edx, %ebx
	addb	%bl, %bl
	andb	$2, %bl
	movl	%edx, %ecx
	andb	$4, %cl
	shlb	$2, %dl
	andb	$8, %dl
	orb	%bl, %cl
	orb	%dl, %cl
	orb	%al, %cl
	movb	%cl, 16(%rsi)
	movl	36(%rsp), %eax
	movl	28(%rsp), %ecx
	movl	32(%rsp), %edx
	movl	$0, 20(%rsi)
	movl	%ecx, 24(%rsi)
	movl	%eax, 28(%rsi)
	movl	%edx, 32(%rsi)
.LBB6_18:
	movl	%ebp, %eax
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN22btVoronoiSimplexSolver25closestPtPointTetrahedronERK9btVector3S2_S2_S2_S2_R25btSubSimplexClosestResult, .Lfunc_end6-_ZN22btVoronoiSimplexSolver25closestPtPointTetrahedronERK9btVector3S2_S2_S2_S2_R25btSubSimplexClosestResult
	.cfi_endproc

	.globl	_ZN22btVoronoiSimplexSolver7closestER9btVector3
	.p2align	4, 0x90
	.type	_ZN22btVoronoiSimplexSolver7closestER9btVector3,@function
_ZN22btVoronoiSimplexSolver7closestER9btVector3: # @_ZN22btVoronoiSimplexSolver7closestER9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN22btVoronoiSimplexSolver28updateClosestVectorAndPointsEv
	movups	276(%rbx), %xmm0
	movups	%xmm0, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	_ZN22btVoronoiSimplexSolver7closestER9btVector3, .Lfunc_end7-_ZN22btVoronoiSimplexSolver7closestER9btVector3
	.cfi_endproc

	.globl	_ZN22btVoronoiSimplexSolver9maxVertexEv
	.p2align	4, 0x90
	.type	_ZN22btVoronoiSimplexSolver9maxVertexEv,@function
_ZN22btVoronoiSimplexSolver9maxVertexEv: # @_ZN22btVoronoiSimplexSolver9maxVertexEv
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB8_1
# BB#2:                                 # %.lr.ph.preheader
	addq	$12, %rdi
	xorps	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB8_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	-8(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	-4(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	maxss	%xmm1, %xmm0
	addq	$16, %rdi
	decq	%rax
	movaps	%xmm0, %xmm1
	jne	.LBB8_3
# BB#4:                                 # %._crit_edge
	retq
.LBB8_1:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end8:
	.size	_ZN22btVoronoiSimplexSolver9maxVertexEv, .Lfunc_end8-_ZN22btVoronoiSimplexSolver9maxVertexEv
	.cfi_endproc

	.globl	_ZNK22btVoronoiSimplexSolver10getSimplexEP9btVector3S1_S1_
	.p2align	4, 0x90
	.type	_ZNK22btVoronoiSimplexSolver10getSimplexEP9btVector3S1_S1_,@function
_ZNK22btVoronoiSimplexSolver10getSimplexEP9btVector3S1_S1_: # @_ZNK22btVoronoiSimplexSolver10getSimplexEP9btVector3S1_S1_
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB9_3
# BB#1:                                 # %.lr.ph.preheader
	leaq	164(%rdi), %r8
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB9_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movups	-160(%r8), %xmm0
	movups	%xmm0, (%rcx)
	movups	-80(%r8), %xmm0
	movups	%xmm0, (%rsi)
	movups	(%r8), %xmm0
	movups	%xmm0, (%rdx)
	incq	%r9
	movslq	(%rdi), %rax
	addq	$16, %rcx
	addq	$16, %rsi
	addq	$16, %rdx
	addq	$16, %r8
	cmpq	%rax, %r9
	jl	.LBB9_2
.LBB9_3:                                # %._crit_edge
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end9:
	.size	_ZNK22btVoronoiSimplexSolver10getSimplexEP9btVector3S1_S1_, .Lfunc_end9-_ZNK22btVoronoiSimplexSolver10getSimplexEP9btVector3S1_S1_
	.cfi_endproc

	.globl	_ZN22btVoronoiSimplexSolver9inSimplexERK9btVector3
	.p2align	4, 0x90
	.type	_ZN22btVoronoiSimplexSolver9inSimplexERK9btVector3,@function
_ZN22btVoronoiSimplexSolver9inSimplexERK9btVector3: # @_ZN22btVoronoiSimplexSolver9inSimplexERK9btVector3
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	movss	12(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	testl	%ecx, %ecx
	jle	.LBB10_1
# BB#2:                                 # %.lr.ph
	movss	8(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	leaq	16(%rdi), %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB10_3:                               # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm4
	jne	.LBB10_8
	jp	.LBB10_8
# BB#4:                                 #   in Loop: Header=BB10_3 Depth=1
	movss	-4(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm4
	jne	.LBB10_8
	jp	.LBB10_8
# BB#5:                                 #   in Loop: Header=BB10_3 Depth=1
	movss	-8(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm4
	jne	.LBB10_8
	jp	.LBB10_8
# BB#6:                                 # %_ZNK9btVector3eqERKS_.exit13
                                        #   in Loop: Header=BB10_3 Depth=1
	movss	-12(%rdx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm4
	jne	.LBB10_8
	jp	.LBB10_8
# BB#7:                                 #   in Loop: Header=BB10_3 Depth=1
	movb	$1, %al
	.p2align	4, 0x90
.LBB10_8:                               # %_ZNK9btVector3eqERKS_.exit13.thread
                                        #   in Loop: Header=BB10_3 Depth=1
	addq	$16, %rdx
	decq	%rcx
	jne	.LBB10_3
	jmp	.LBB10_9
.LBB10_1:
	xorl	%eax, %eax
.LBB10_9:                               # %._crit_edge
	ucomiss	304(%rdi), %xmm0
	jne	.LBB10_12
	jp	.LBB10_12
# BB#10:
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	300(%rdi), %xmm0
	jne	.LBB10_12
	jp	.LBB10_12
# BB#11:
	movss	4(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	296(%rdi), %xmm0
	jne	.LBB10_12
	jp	.LBB10_12
# BB#13:                                # %_ZNK9btVector3eqERKS_.exit
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	292(%rdi), %xmm0
	jne	.LBB10_14
	jp	.LBB10_14
# BB#15:
	movb	$1, %al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB10_12:                              # %_ZNK9btVector3eqERKS_.exit.thread
	andb	$1, %al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB10_14:
	andb	$1, %al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end10:
	.size	_ZN22btVoronoiSimplexSolver9inSimplexERK9btVector3, .Lfunc_end10-_ZN22btVoronoiSimplexSolver9inSimplexERK9btVector3
	.cfi_endproc

	.globl	_ZN22btVoronoiSimplexSolver14backup_closestER9btVector3
	.p2align	4, 0x90
	.type	_ZN22btVoronoiSimplexSolver14backup_closestER9btVector3,@function
_ZN22btVoronoiSimplexSolver14backup_closestER9btVector3: # @_ZN22btVoronoiSimplexSolver14backup_closestER9btVector3
	.cfi_startproc
# BB#0:
	movups	276(%rdi), %xmm0
	movups	%xmm0, (%rsi)
	retq
.Lfunc_end11:
	.size	_ZN22btVoronoiSimplexSolver14backup_closestER9btVector3, .Lfunc_end11-_ZN22btVoronoiSimplexSolver14backup_closestER9btVector3
	.cfi_endproc

	.globl	_ZNK22btVoronoiSimplexSolver12emptySimplexEv
	.p2align	4, 0x90
	.type	_ZNK22btVoronoiSimplexSolver12emptySimplexEv,@function
_ZNK22btVoronoiSimplexSolver12emptySimplexEv: # @_ZNK22btVoronoiSimplexSolver12emptySimplexEv
	.cfi_startproc
# BB#0:
	cmpl	$0, (%rdi)
	sete	%al
	retq
.Lfunc_end12:
	.size	_ZNK22btVoronoiSimplexSolver12emptySimplexEv, .Lfunc_end12-_ZNK22btVoronoiSimplexSolver12emptySimplexEv
	.cfi_endproc

	.globl	_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_
	.p2align	4, 0x90
	.type	_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_,@function
_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_: # @_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	callq	_ZN22btVoronoiSimplexSolver28updateClosestVectorAndPointsEv
	movups	244(%rbx), %xmm0
	movups	%xmm0, (%r15)
	movups	260(%rbx), %xmm0
	movups	%xmm0, (%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_, .Lfunc_end13-_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_0:
	.long	841731190               # float 9.99999905E-9
	.text
	.globl	_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_
	.p2align	4, 0x90
	.type	_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_,@function
_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_: # @_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_
	.cfi_startproc
# BB#0:
	movss	(%rcx), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm9
	subss	%xmm3, %xmm6
	movss	8(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm8          # xmm8 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm1
	movss	(%r8), %xmm7            # xmm7 = mem[0],zero,zero,zero
	movss	4(%r8), %xmm0           # xmm0 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm7
	subss	%xmm3, %xmm0
	movss	8(%r8), %xmm2           # xmm2 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm2
	movaps	%xmm1, %xmm5
	mulss	%xmm7, %xmm1
	mulss	%xmm6, %xmm7
	mulss	%xmm2, %xmm6
	mulss	%xmm0, %xmm5
	subss	%xmm5, %xmm6
	mulss	%xmm9, %xmm2
	subss	%xmm2, %xmm1
	mulss	%xmm9, %xmm0
	subss	%xmm7, %xmm0
	movss	(%r9), %xmm2            # xmm2 = mem[0],zero,zero,zero
	movss	4(%r9), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm7    # xmm7 = xmm7[0],xmm2[0],xmm7[1],xmm2[1]
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	subps	%xmm4, %xmm7
	unpcklps	%xmm9, %xmm5    # xmm5 = xmm5[0],xmm9[0],xmm5[1],xmm9[1]
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	subps	%xmm3, %xmm5
	movss	8(%r9), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	subps	%xmm8, %xmm3
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm7, %xmm6
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm5, %xmm1
	addps	%xmm6, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	addps	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
	xorl	%ecx, %ecx
	ucomiss	%xmm0, %xmm2
	seta	%cl
	movss	.LCPI14_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	movl	$-1, %eax
	cmovbel	%ecx, %eax
	retq
.Lfunc_end14:
	.size	_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_, .Lfunc_end14-_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
