	.text
	.file	"reduce.bc"
	.globl	reduce
	.p2align	4, 0x90
	.type	reduce,@function
reduce:                                 # @reduce
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	cmpl	$0, use_random_order(%rip)
	je	.LBB0_2
# BB#1:
	xorl	%eax, %eax
	callq	random_order
	movq	%rax, %r13
	jmp	.LBB0_6
.LBB0_2:
	cmpl	$0, toggle(%rip)
	je	.LBB0_4
# BB#3:
	xorl	%eax, %eax
	callq	sort_reduce
	jmp	.LBB0_5
.LBB0_4:
	movl	$descend, %esi
	xorl	%eax, %eax
	callq	mini_sort
.LBB0_5:
	movq	%rax, %r13
	xorl	%eax, %eax
	cmpl	$0, toggle(%rip)
	sete	%al
	movl	%eax, toggle(%rip)
.LBB0_6:
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	cube2list
	movq	%rax, %rbp
	movslq	(%r13), %rcx
	movslq	12(%r13), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB0_16
# BB#7:                                 # %.lr.ph.preheader
	movq	24(%r13), %rbx
	leaq	(%rbx,%rax,4), %r14
	movl	$40960, %r15d           # imm = 0xA000
	movq	%rbp, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	cofactor
	movq	%rax, %rdi
	callq	sccc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rcx, %rsi
	movq	%rbx, %rdx
	callq	set_and
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	setp_equal
	testl	%eax, %eax
	je	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=1
	movl	(%rbx), %ecx
	orl	%r15d, %ecx
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_8 Depth=1
	testb	$64, debug(%rip)
	je	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_8 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pc1
	movq	%r13, %rbp
	movq	%rax, %r13
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	pc2
	movq	%r14, %r15
	movq	%rax, %r14
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rdi
	callq	util_print_time
	movq	%rax, %rcx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	movq	%rbp, %r13
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%r14, %rdx
	movq	%r15, %r14
	movl	$40960, %r15d           # imm = 0xA000
	callq	printf
.LBB0_12:                               #   in Loop: Header=BB0_8 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	set_copy
	andb	$127, 1(%rbx)
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	setp_empty
	movl	(%rbx), %ecx
	movl	%ecx, %edx
	orl	$8192, %edx             # imm = 0x2000
	andl	$-8193, %ecx            # imm = 0xDFFF
	testl	%eax, %eax
	cmovel	%edx, %ecx
.LBB0_13:                               #   in Loop: Header=BB0_8 Depth=1
	movl	%ecx, (%rbx)
	testq	%r12, %r12
	je	.LBB0_15
# BB#14:                                #   in Loop: Header=BB0_8 Depth=1
	movq	%r12, %rdi
	callq	free
.LBB0_15:                               #   in Loop: Header=BB0_8 Depth=1
	movslq	(%r13), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r14, %rbx
	jb	.LBB0_8
.LBB0_16:                               # %._crit_edge
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_18
# BB#17:
	callq	free
.LBB0_18:
	movq	%rbp, %rdi
	callq	free
	xorl	%eax, %eax
	movq	%r13, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sf_inactive             # TAILCALL
.Lfunc_end0:
	.size	reduce, .Lfunc_end0-reduce
	.cfi_endproc

	.globl	reduce_cube
	.p2align	4, 0x90
	.type	reduce_cube,@function
reduce_cube:                            # @reduce_cube
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	xorl	%eax, %eax
	callq	cofactor
	movq	%rax, %rdi
	callq	sccc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rcx, %rsi
	movq	%rbx, %rdx
	popq	%rbx
	jmp	set_and                 # TAILCALL
.Lfunc_end1:
	.size	reduce_cube, .Lfunc_end1-reduce_cube
	.cfi_endproc

	.globl	sccc
	.p2align	4, 0x90
	.type	sccc,@function
sccc:                                   # @sccc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 64
.Lcfi21:
	.cfi_offset %rbx, -48
.Lcfi22:
	.cfi_offset %r12, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpb	$0, debug(%rip)
	jns	.LBB2_2
# BB#1:
	movl	sccc.sccc_level(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, sccc.sccc_level(%rip)
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	debug_print
.LBB2_2:
	leaq	8(%rsp), %rsi
	movq	%r14, %rdi
	callq	sccc_special_cases
	cmpl	$2, %eax
	jne	.LBB2_58
# BB#3:
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB2_5
# BB#4:
	movl	$8, %edi
	jmp	.LBB2_6
.LBB2_5:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB2_6:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %r15
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB2_8
# BB#7:
	movl	$8, %edi
	jmp	.LBB2_9
.LBB2_8:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB2_9:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %r12
	movl	$128, %ecx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	binate_split_select
	movl	%eax, %ebp
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	scofactor
	movq	%rax, %rdi
	callq	sccc
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movl	%ebp, %edx
	callq	scofactor
	movq	%rax, %rdi
	callq	sccc
	movl	(%rbx), %edx
	movl	%edx, %esi
	notl	%esi
	movl	%edx, %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	orq	$-1024, %rsi            # imm = 0xFC00
	testw	$1023, %dx              # imm = 0x3FF
	movq	$-2, %rdx
	cmoveq	%rsi, %rdx
	leaq	2(%rdx,%rcx), %r9
	cmpq	$8, %r9
	jb	.LBB2_21
# BB#10:                                # %min.iters.checked
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB2_21
# BB#11:                                # %vector.memcheck
	testl	%ecx, %ecx
	movl	$1, %edx
	cmoveq	%rcx, %rdx
	leaq	(%rbx,%rdx,4), %rsi
	leaq	4(%r15,%rcx,4), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB2_13
# BB#12:                                # %vector.memcheck
	leaq	4(%rbx,%rcx,4), %rsi
	leaq	(%r15,%rdx,4), %rdx
	cmpq	%rsi, %rdx
	jb	.LBB2_21
.LBB2_13:                               # %vector.body.preheader
	leaq	-8(%r8), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB2_14
# BB#15:                                # %vector.body.prol
	movups	-12(%rbx,%rcx,4), %xmm0
	movups	-28(%rbx,%rcx,4), %xmm1
	movups	-12(%r15,%rcx,4), %xmm2
	movups	-28(%r15,%rcx,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rcx,4)
	movups	%xmm3, -28(%rbx,%rcx,4)
	movl	$8, %edi
	testq	%rdx, %rdx
	jne	.LBB2_17
	jmp	.LBB2_19
.LBB2_14:
	xorl	%edi, %edi
	testq	%rdx, %rdx
	je	.LBB2_19
.LBB2_17:                               # %vector.body.preheader.new
	movq	%r8, %rsi
	negq	%rsi
	negq	%rdi
	leaq	-12(%rbx,%rcx,4), %rbp
	leaq	-12(%r15,%rcx,4), %rdx
	.p2align	4, 0x90
.LBB2_18:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rdi,4), %xmm0
	movups	-16(%rbp,%rdi,4), %xmm1
	movups	(%rdx,%rdi,4), %xmm2
	movups	-16(%rdx,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rbp,%rdi,4)
	movups	%xmm3, -16(%rbp,%rdi,4)
	movups	-32(%rbp,%rdi,4), %xmm0
	movups	-48(%rbp,%rdi,4), %xmm1
	movups	-32(%rdx,%rdi,4), %xmm2
	movups	-48(%rdx,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbp,%rdi,4)
	movups	%xmm3, -48(%rbp,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %rsi
	jne	.LBB2_18
.LBB2_19:                               # %middle.block
	cmpq	%r8, %r9
	je	.LBB2_23
# BB#20:
	subq	%r8, %rcx
.LBB2_21:                               # %scalar.ph.preheader
	incq	%rcx
	.p2align	4, 0x90
.LBB2_22:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r15,%rcx,4), %edx
	andl	%edx, -4(%rbx,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB2_22
.LBB2_23:                               # %.loopexit133
	movl	(%rax), %edx
	movl	%edx, %esi
	notl	%esi
	movl	%edx, %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	orq	$-1024, %rsi            # imm = 0xFC00
	testw	$1023, %dx              # imm = 0x3FF
	movq	$-2, %rdx
	cmoveq	%rsi, %rdx
	leaq	2(%rdx,%rcx), %r9
	cmpq	$8, %r9
	jb	.LBB2_35
# BB#24:                                # %min.iters.checked47
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB2_35
# BB#25:                                # %vector.memcheck65
	testl	%ecx, %ecx
	movl	$1, %edx
	cmoveq	%rcx, %rdx
	leaq	(%rax,%rdx,4), %rsi
	leaq	4(%r12,%rcx,4), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB2_27
# BB#26:                                # %vector.memcheck65
	leaq	4(%rax,%rcx,4), %rsi
	leaq	(%r12,%rdx,4), %rdx
	cmpq	%rsi, %rdx
	jb	.LBB2_35
.LBB2_27:                               # %vector.body42.preheader
	leaq	-8(%r8), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB2_28
# BB#29:                                # %vector.body42.prol
	movups	-12(%rax,%rcx,4), %xmm0
	movups	-28(%rax,%rcx,4), %xmm1
	movups	-12(%r12,%rcx,4), %xmm2
	movups	-28(%r12,%rcx,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rax,%rcx,4)
	movups	%xmm3, -28(%rax,%rcx,4)
	movl	$8, %edi
	testq	%rdx, %rdx
	jne	.LBB2_31
	jmp	.LBB2_33
.LBB2_28:
	xorl	%edi, %edi
	testq	%rdx, %rdx
	je	.LBB2_33
.LBB2_31:                               # %vector.body42.preheader.new
	movq	%r8, %rsi
	negq	%rsi
	negq	%rdi
	leaq	-12(%rax,%rcx,4), %rbp
	leaq	-12(%r12,%rcx,4), %rdx
	.p2align	4, 0x90
.LBB2_32:                               # %vector.body42
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rdi,4), %xmm0
	movups	-16(%rbp,%rdi,4), %xmm1
	movups	(%rdx,%rdi,4), %xmm2
	movups	-16(%rdx,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rbp,%rdi,4)
	movups	%xmm3, -16(%rbp,%rdi,4)
	movups	-32(%rbp,%rdi,4), %xmm0
	movups	-48(%rbp,%rdi,4), %xmm1
	movups	-32(%rdx,%rdi,4), %xmm2
	movups	-48(%rdx,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbp,%rdi,4)
	movups	%xmm3, -48(%rbp,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %rsi
	jne	.LBB2_32
.LBB2_33:                               # %middle.block43
	cmpq	%r8, %r9
	je	.LBB2_37
# BB#34:
	subq	%r8, %rcx
.LBB2_35:                               # %scalar.ph44.preheader
	incq	%rcx
	.p2align	4, 0x90
.LBB2_36:                               # %scalar.ph44
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r12,%rcx,4), %edx
	andl	%edx, -4(%rax,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB2_36
.LBB2_37:                               # %.loopexit132
	movl	(%rbx), %edx
	movl	%edx, %esi
	notl	%esi
	movl	%edx, %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	orq	$-1024, %rsi            # imm = 0xFC00
	testw	$1023, %dx              # imm = 0x3FF
	movq	$-2, %rdx
	cmoveq	%rsi, %rdx
	leaq	2(%rdx,%rcx), %r9
	cmpq	$8, %r9
	jb	.LBB2_49
# BB#38:                                # %min.iters.checked92
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB2_49
# BB#39:                                # %vector.memcheck110
	testl	%ecx, %ecx
	movl	$1, %edx
	cmoveq	%rcx, %rdx
	leaq	(%rbx,%rdx,4), %rsi
	leaq	4(%rax,%rcx,4), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB2_41
# BB#40:                                # %vector.memcheck110
	leaq	4(%rbx,%rcx,4), %rsi
	leaq	(%rax,%rdx,4), %rdx
	cmpq	%rsi, %rdx
	jb	.LBB2_49
.LBB2_41:                               # %vector.body87.preheader
	leaq	-8(%r8), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB2_42
# BB#43:                                # %vector.body87.prol
	movups	-12(%rbx,%rcx,4), %xmm0
	movups	-28(%rbx,%rcx,4), %xmm1
	movups	-12(%rax,%rcx,4), %xmm2
	movups	-28(%rax,%rcx,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rcx,4)
	movups	%xmm3, -28(%rbx,%rcx,4)
	movl	$8, %edi
	testq	%rdx, %rdx
	jne	.LBB2_45
	jmp	.LBB2_47
.LBB2_42:
	xorl	%edi, %edi
	testq	%rdx, %rdx
	je	.LBB2_47
.LBB2_45:                               # %vector.body87.preheader.new
	movq	%r8, %rsi
	negq	%rsi
	negq	%rdi
	leaq	-12(%rbx,%rcx,4), %rbp
	leaq	-12(%rax,%rcx,4), %rdx
	.p2align	4, 0x90
.LBB2_46:                               # %vector.body87
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rdi,4), %xmm0
	movups	-16(%rbp,%rdi,4), %xmm1
	movups	(%rdx,%rdi,4), %xmm2
	movups	-16(%rdx,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rbp,%rdi,4)
	movups	%xmm3, -16(%rbp,%rdi,4)
	movups	-32(%rbp,%rdi,4), %xmm0
	movups	-48(%rbp,%rdi,4), %xmm1
	movups	-32(%rdx,%rdi,4), %xmm2
	movups	-48(%rdx,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbp,%rdi,4)
	movups	%xmm3, -48(%rbp,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %rsi
	jne	.LBB2_46
.LBB2_47:                               # %middle.block88
	cmpq	%r8, %r9
	je	.LBB2_51
# BB#48:
	subq	%r8, %rcx
.LBB2_49:                               # %scalar.ph89.preheader
	incq	%rcx
	.p2align	4, 0x90
.LBB2_50:                               # %scalar.ph89
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rax,%rcx,4), %edx
	orl	%edx, -4(%rbx,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB2_50
.LBB2_51:                               # %.loopexit
	movq	%rax, %rdi
	callq	free
	testq	%r15, %r15
	je	.LBB2_53
# BB#52:
	movq	%r15, %rdi
	callq	free
.LBB2_53:
	testq	%r12, %r12
	je	.LBB2_55
# BB#54:
	movq	%r12, %rdi
	callq	free
.LBB2_55:                               # %sccc_merge.exit
	movq	%rbx, 8(%rsp)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_57
# BB#56:
	callq	free
.LBB2_57:
	movq	%r14, %rdi
	callq	free
.LBB2_58:
	cmpb	$0, debug(%rip)
	js	.LBB2_60
# BB#59:                                # %._crit_edge
	movq	8(%rsp), %rbx
	jmp	.LBB2_61
.LBB2_60:
	movl	sccc.sccc_level(%rip), %ebp
	decl	%ebp
	movl	%ebp, sccc.sccc_level(%rip)
	movq	8(%rsp), %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pc1
	movq	%rax, %rcx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	movq	%rcx, %rdx
	callq	printf
.LBB2_61:
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	sccc, .Lfunc_end2-sccc
	.cfi_endproc

	.globl	sccc_merge
	.p2align	4, 0x90
	.type	sccc_merge,@function
sccc_merge:                             # @sccc_merge
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movl	(%rbx), %ecx
	movl	%ecx, %edx
	notl	%edx
	movl	%ecx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	orq	$-1024, %rdx            # imm = 0xFC00
	testw	$1023, %cx              # imm = 0x3FF
	movq	$-2, %rcx
	cmoveq	%rdx, %rcx
	leaq	2(%rcx,%rax), %r9
	cmpq	$8, %r9
	jb	.LBB3_12
# BB#1:                                 # %min.iters.checked
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB3_12
# BB#2:                                 # %vector.memcheck
	testl	%eax, %eax
	movl	$1, %ecx
	cmoveq	%rax, %rcx
	leaq	(%rbx,%rcx,4), %rdx
	leaq	4(%r15,%rax,4), %rdi
	cmpq	%rdi, %rdx
	jae	.LBB3_4
# BB#3:                                 # %vector.memcheck
	leaq	4(%rbx,%rax,4), %rdx
	leaq	(%r15,%rcx,4), %rcx
	cmpq	%rdx, %rcx
	jb	.LBB3_12
.LBB3_4:                                # %vector.body.preheader
	leaq	-8(%r8), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB3_5
# BB#6:                                 # %vector.body.prol
	movups	-12(%rbx,%rax,4), %xmm0
	movups	-28(%rbx,%rax,4), %xmm1
	movups	-12(%r15,%rax,4), %xmm2
	movups	-28(%r15,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rax,4)
	movups	%xmm3, -28(%rbx,%rax,4)
	movl	$8, %edi
	testq	%rcx, %rcx
	jne	.LBB3_8
	jmp	.LBB3_10
.LBB3_5:
	xorl	%edi, %edi
	testq	%rcx, %rcx
	je	.LBB3_10
.LBB3_8:                                # %vector.body.preheader.new
	movq	%r8, %r10
	negq	%r10
	negq	%rdi
	leaq	-12(%rbx,%rax,4), %rcx
	leaq	-12(%r15,%rax,4), %rdx
	.p2align	4, 0x90
.LBB3_9:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rdi,4), %xmm0
	movups	-16(%rcx,%rdi,4), %xmm1
	movups	(%rdx,%rdi,4), %xmm2
	movups	-16(%rdx,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rcx,%rdi,4)
	movups	%xmm3, -16(%rcx,%rdi,4)
	movups	-32(%rcx,%rdi,4), %xmm0
	movups	-48(%rcx,%rdi,4), %xmm1
	movups	-32(%rdx,%rdi,4), %xmm2
	movups	-48(%rdx,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rcx,%rdi,4)
	movups	%xmm3, -48(%rcx,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %r10
	jne	.LBB3_9
.LBB3_10:                               # %middle.block
	cmpq	%r8, %r9
	je	.LBB3_14
# BB#11:
	subq	%r8, %rax
.LBB3_12:                               # %scalar.ph.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB3_13:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r15,%rax,4), %ecx
	andl	%ecx, -4(%rbx,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB3_13
.LBB3_14:                               # %.loopexit158
	movl	(%rsi), %ecx
	movl	%ecx, %edx
	notl	%edx
	movl	%ecx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	orq	$-1024, %rdx            # imm = 0xFC00
	testw	$1023, %cx              # imm = 0x3FF
	movq	$-2, %rcx
	cmoveq	%rdx, %rcx
	leaq	2(%rcx,%rax), %r9
	cmpq	$8, %r9
	jb	.LBB3_26
# BB#15:                                # %min.iters.checked72
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB3_26
# BB#16:                                # %vector.memcheck90
	testl	%eax, %eax
	movl	$1, %ecx
	cmoveq	%rax, %rcx
	leaq	(%rsi,%rcx,4), %rdx
	leaq	4(%r14,%rax,4), %rdi
	cmpq	%rdi, %rdx
	jae	.LBB3_18
# BB#17:                                # %vector.memcheck90
	leaq	4(%rsi,%rax,4), %rdx
	leaq	(%r14,%rcx,4), %rcx
	cmpq	%rdx, %rcx
	jb	.LBB3_26
.LBB3_18:                               # %vector.body67.preheader
	leaq	-8(%r8), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB3_19
# BB#20:                                # %vector.body67.prol
	movups	-12(%rsi,%rax,4), %xmm0
	movups	-28(%rsi,%rax,4), %xmm1
	movups	-12(%r14,%rax,4), %xmm2
	movups	-28(%r14,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rsi,%rax,4)
	movups	%xmm3, -28(%rsi,%rax,4)
	movl	$8, %edi
	testq	%rcx, %rcx
	jne	.LBB3_22
	jmp	.LBB3_24
.LBB3_19:
	xorl	%edi, %edi
	testq	%rcx, %rcx
	je	.LBB3_24
.LBB3_22:                               # %vector.body67.preheader.new
	movq	%r8, %r10
	negq	%r10
	negq	%rdi
	leaq	-12(%rsi,%rax,4), %rcx
	leaq	-12(%r14,%rax,4), %rdx
	.p2align	4, 0x90
.LBB3_23:                               # %vector.body67
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rdi,4), %xmm0
	movups	-16(%rcx,%rdi,4), %xmm1
	movups	(%rdx,%rdi,4), %xmm2
	movups	-16(%rdx,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rcx,%rdi,4)
	movups	%xmm3, -16(%rcx,%rdi,4)
	movups	-32(%rcx,%rdi,4), %xmm0
	movups	-48(%rcx,%rdi,4), %xmm1
	movups	-32(%rdx,%rdi,4), %xmm2
	movups	-48(%rdx,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rcx,%rdi,4)
	movups	%xmm3, -48(%rcx,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %r10
	jne	.LBB3_23
.LBB3_24:                               # %middle.block68
	cmpq	%r8, %r9
	je	.LBB3_28
# BB#25:
	subq	%r8, %rax
.LBB3_26:                               # %scalar.ph69.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB3_27:                               # %scalar.ph69
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r14,%rax,4), %ecx
	andl	%ecx, -4(%rsi,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB3_27
.LBB3_28:                               # %.loopexit157
	movl	(%rbx), %ecx
	movl	%ecx, %edx
	notl	%edx
	movl	%ecx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	orq	$-1024, %rdx            # imm = 0xFC00
	testw	$1023, %cx              # imm = 0x3FF
	movq	$-2, %rcx
	cmoveq	%rdx, %rcx
	leaq	2(%rcx,%rax), %r9
	cmpq	$8, %r9
	jb	.LBB3_40
# BB#29:                                # %min.iters.checked117
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB3_40
# BB#30:                                # %vector.memcheck135
	testl	%eax, %eax
	movl	$1, %ecx
	cmoveq	%rax, %rcx
	leaq	(%rbx,%rcx,4), %rdx
	leaq	4(%rsi,%rax,4), %rdi
	cmpq	%rdi, %rdx
	jae	.LBB3_32
# BB#31:                                # %vector.memcheck135
	leaq	4(%rbx,%rax,4), %rdx
	leaq	(%rsi,%rcx,4), %rcx
	cmpq	%rdx, %rcx
	jb	.LBB3_40
.LBB3_32:                               # %vector.body112.preheader
	leaq	-8(%r8), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB3_33
# BB#34:                                # %vector.body112.prol
	movups	-12(%rbx,%rax,4), %xmm0
	movups	-28(%rbx,%rax,4), %xmm1
	movups	-12(%rsi,%rax,4), %xmm2
	movups	-28(%rsi,%rax,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rax,4)
	movups	%xmm3, -28(%rbx,%rax,4)
	movl	$8, %edi
	testq	%rcx, %rcx
	jne	.LBB3_36
	jmp	.LBB3_38
.LBB3_33:
	xorl	%edi, %edi
	testq	%rcx, %rcx
	je	.LBB3_38
.LBB3_36:                               # %vector.body112.preheader.new
	movq	%r8, %r10
	negq	%r10
	negq	%rdi
	leaq	-12(%rbx,%rax,4), %rcx
	leaq	-12(%rsi,%rax,4), %rdx
	.p2align	4, 0x90
.LBB3_37:                               # %vector.body112
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rdi,4), %xmm0
	movups	-16(%rcx,%rdi,4), %xmm1
	movups	(%rdx,%rdi,4), %xmm2
	movups	-16(%rdx,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rcx,%rdi,4)
	movups	%xmm3, -16(%rcx,%rdi,4)
	movups	-32(%rcx,%rdi,4), %xmm0
	movups	-48(%rcx,%rdi,4), %xmm1
	movups	-32(%rdx,%rdi,4), %xmm2
	movups	-48(%rdx,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rcx,%rdi,4)
	movups	%xmm3, -48(%rcx,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %r10
	jne	.LBB3_37
.LBB3_38:                               # %middle.block113
	cmpq	%r8, %r9
	je	.LBB3_42
# BB#39:
	subq	%r8, %rax
.LBB3_40:                               # %scalar.ph114.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB3_41:                               # %scalar.ph114
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rsi,%rax,4), %ecx
	orl	%ecx, -4(%rbx,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB3_41
.LBB3_42:                               # %.loopexit
	movq	%rsi, %rdi
	callq	free
	testq	%r15, %r15
	je	.LBB3_44
# BB#43:
	movq	%r15, %rdi
	callq	free
.LBB3_44:
	testq	%r14, %r14
	je	.LBB3_46
# BB#45:
	movq	%r14, %rdi
	callq	free
.LBB3_46:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	sccc_merge, .Lfunc_end3-sccc_merge
	.cfi_endproc

	.globl	sccc_cube
	.p2align	4, 0x90
	.type	sccc_cube,@function
sccc_cube:                              # @sccc_cube
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	cube+80(%rip), %rax
	movq	(%rax), %rbx
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	cactive
	testl	%eax, %eax
	js	.LBB4_29
# BB#1:
	movq	cube+72(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %r11
	movl	(%r15), %ecx
	movl	%ecx, %eax
	notl	%eax
	movl	$-1024, %edx            # imm = 0xFC00
	andl	(%rbx), %edx
	orq	$-1024, %rax            # imm = 0xFC00
	andl	$1023, %ecx             # imm = 0x3FF
	movq	$-2, %rsi
	cmoveq	%rax, %rsi
	orl	%ecx, %edx
	movl	%edx, (%rbx)
	leaq	2(%rsi,%rcx), %r9
	cmpq	$8, %r9
	jb	.LBB4_13
# BB#2:                                 # %min.iters.checked
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB4_13
# BB#3:                                 # %vector.memcheck
	testl	%ecx, %ecx
	movl	$1, %eax
	cmoveq	%rcx, %rax
	leaq	(%rbx,%rax,4), %rbp
	leaq	4(%rbx,%rcx,4), %rsi
	leaq	(%r15,%rax,4), %r10
	leaq	4(%r15,%rcx,4), %rdi
	leaq	(%r11,%rax,4), %r12
	leaq	4(%r11,%rcx,4), %r13
	cmpq	%rdi, %rbp
	sbbb	%al, %al
	cmpq	%rsi, %r10
	sbbb	%dl, %dl
	andb	%al, %dl
	cmpq	%r13, %rbp
	sbbb	%al, %al
	cmpq	%rsi, %r12
	sbbb	%sil, %sil
	testb	$1, %dl
	jne	.LBB4_13
# BB#4:                                 # %vector.memcheck
	andb	%sil, %al
	andb	$1, %al
	jne	.LBB4_13
# BB#5:                                 # %vector.body.preheader
	leaq	-8(%r8), %rdx
	movq	%rdx, %rax
	shrq	$3, %rax
	btl	$3, %edx
	jb	.LBB4_6
# BB#7:                                 # %vector.body.prol
	movups	-12(%r15,%rcx,4), %xmm0
	movups	-28(%r15,%rcx,4), %xmm1
	movups	-12(%r11,%rcx,4), %xmm2
	movups	-28(%r11,%rcx,4), %xmm3
	xorps	%xmm0, %xmm2
	xorps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rcx,4)
	movups	%xmm3, -28(%rbx,%rcx,4)
	movl	$8, %edi
	testq	%rax, %rax
	jne	.LBB4_9
	jmp	.LBB4_11
.LBB4_6:
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.LBB4_11
.LBB4_9:                                # %vector.body.preheader.new
	movq	%r8, %r10
	negq	%r10
	negq	%rdi
	leaq	-12(%r15,%rcx,4), %rdx
	leaq	-12(%r11,%rcx,4), %rsi
	leaq	-12(%rbx,%rcx,4), %rax
	.p2align	4, 0x90
.LBB4_10:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdx,%rdi,4), %xmm0
	movups	-16(%rdx,%rdi,4), %xmm1
	movups	(%rsi,%rdi,4), %xmm2
	movups	-16(%rsi,%rdi,4), %xmm3
	xorps	%xmm0, %xmm2
	xorps	%xmm1, %xmm3
	movups	%xmm2, (%rax,%rdi,4)
	movups	%xmm3, -16(%rax,%rdi,4)
	movups	-32(%rdx,%rdi,4), %xmm0
	movups	-48(%rdx,%rdi,4), %xmm1
	movups	-32(%rsi,%rdi,4), %xmm2
	movups	-48(%rsi,%rdi,4), %xmm3
	xorps	%xmm0, %xmm2
	xorps	%xmm1, %xmm3
	movups	%xmm2, -32(%rax,%rdi,4)
	movups	%xmm3, -48(%rax,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %r10
	jne	.LBB4_10
.LBB4_11:                               # %middle.block
	cmpq	%r8, %r9
	je	.LBB4_15
# BB#12:
	subq	%r8, %rcx
.LBB4_13:                               # %scalar.ph.preheader
	incq	%rcx
	.p2align	4, 0x90
.LBB4_14:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r11,%rcx,4), %eax
	xorl	-4(%r15,%rcx,4), %eax
	movl	%eax, -4(%rbx,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB4_14
.LBB4_15:                               # %.loopexit105
	movl	(%r14), %ecx
	movl	%ecx, %edx
	notl	%edx
	movl	%ecx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	orq	$-1024, %rdx            # imm = 0xFC00
	testw	$1023, %cx              # imm = 0x3FF
	movq	$-2, %rcx
	cmoveq	%rdx, %rcx
	leaq	2(%rcx,%rax), %r9
	cmpq	$8, %r9
	jb	.LBB4_27
# BB#16:                                # %min.iters.checked65
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB4_27
# BB#17:                                # %vector.memcheck83
	testl	%eax, %eax
	movl	$1, %ecx
	cmoveq	%rax, %rcx
	leaq	(%r14,%rcx,4), %rdx
	leaq	4(%rbx,%rax,4), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB4_19
# BB#18:                                # %vector.memcheck83
	leaq	4(%r14,%rax,4), %rdx
	leaq	(%rbx,%rcx,4), %rcx
	cmpq	%rdx, %rcx
	jb	.LBB4_27
.LBB4_19:                               # %vector.body60.preheader
	leaq	-8(%r8), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB4_20
# BB#21:                                # %vector.body60.prol
	movups	-12(%r14,%rax,4), %xmm0
	movups	-28(%r14,%rax,4), %xmm1
	movups	-12(%rbx,%rax,4), %xmm2
	movups	-28(%rbx,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%r14,%rax,4)
	movups	%xmm3, -28(%r14,%rax,4)
	movl	$8, %esi
	testq	%rcx, %rcx
	jne	.LBB4_23
	jmp	.LBB4_25
.LBB4_20:
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.LBB4_25
.LBB4_23:                               # %vector.body60.preheader.new
	movq	%r8, %rdi
	negq	%rdi
	negq	%rsi
	leaq	-12(%r14,%rax,4), %rdx
	leaq	-12(%rbx,%rax,4), %rcx
	.p2align	4, 0x90
.LBB4_24:                               # %vector.body60
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdx,%rsi,4), %xmm0
	movups	-16(%rdx,%rsi,4), %xmm1
	movups	(%rcx,%rsi,4), %xmm2
	movups	-16(%rcx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rdx,%rsi,4)
	movups	%xmm3, -16(%rdx,%rsi,4)
	movups	-32(%rdx,%rsi,4), %xmm0
	movups	-48(%rdx,%rsi,4), %xmm1
	movups	-32(%rcx,%rsi,4), %xmm2
	movups	-48(%rcx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rdx,%rsi,4)
	movups	%xmm3, -48(%rdx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB4_24
.LBB4_25:                               # %middle.block61
	cmpq	%r8, %r9
	je	.LBB4_29
# BB#26:
	subq	%r8, %rax
.LBB4_27:                               # %scalar.ph62.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB4_28:                               # %scalar.ph62
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rbx,%rax,4), %ecx
	andl	%ecx, -4(%r14,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB4_28
.LBB4_29:                               # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	sccc_cube, .Lfunc_end4-sccc_cube
	.cfi_endproc

	.globl	sccc_special_cases
	.p2align	4, 0x90
	.type	sccc_special_cases,@function
sccc_special_cases:                     # @sccc_special_cases
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 96
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %rbp
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_31
# BB#1:                                 # %.lr.ph119.preheader
	movq	cube+80(%rip), %rax
	movq	8(%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	(%rbp), %r13
	leaq	16(%rbp), %r14
	leaq	24(%rbp), %rbx
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph119
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	full_row
	testl	%eax, %eax
	jne	.LBB5_33
# BB#3:                                 # %..preheader_crit_edge
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB5_2
# BB#4:                                 # %.preheader._crit_edge
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	massive_count
	movl	cdata+36(%rip), %eax
	cmpl	cdata+32(%rip), %eax
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	je	.LBB5_35
# BB#5:
	cmpq	$0, 24(%rbp)
	je	.LBB5_35
# BB#6:
	movl	(%r13), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	movl	$2, %ecx
	cmpl	$33, %eax
	jb	.LBB5_8
# BB#7:
	decl	%eax
	shrl	$5, %eax
	addl	$2, %eax
	movl	%eax, %ecx
.LBB5_8:
	movl	%ecx, %edi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r13, %rsi
	callq	set_copy
	movq	%rax, %r13
	movq	(%r14), %rax
	testq	%rax, %rax
	movq	(%rsp), %rbx            # 8-byte Reload
	je	.LBB5_25
# BB#9:                                 # %.lr.ph116.preheader
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	24(%rcx), %r12
	leaq	-4(%r13), %r11
	leaq	4(%r13), %r10
	leaq	-12(%r13), %r8
	movl	$1, %r14d
	movl	$2, %r9d
	.p2align	4, 0x90
.LBB5_10:                               # %.lr.ph116
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_19 Depth 2
                                        #     Child Loop BB5_23 Depth 2
	movl	(%r13), %ecx
	movl	%ecx, %edi
	andl	$1023, %edi             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rdi, %rdx
	cmoveq	%r14, %rdx
	cmpq	$8, %rdx
	jb	.LBB5_22
# BB#11:                                # %min.iters.checked
                                        #   in Loop: Header=BB5_10 Depth=1
	movq	%rdx, %r15
	andq	$1016, %r15             # imm = 0x3F8
	je	.LBB5_22
# BB#12:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_10 Depth=1
	leaq	1(%rdi), %rcx
	testl	%edi, %edi
	cmovneq	%r9, %rcx
	leaq	(%r11,%rcx,4), %rsi
	leaq	4(%rax,%rdi,4), %rbx
	cmpq	%rbx, %rsi
	jae	.LBB5_15
# BB#13:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_10 Depth=1
	leaq	(%r10,%rdi,4), %rsi
	leaq	-4(%rax,%rcx,4), %rcx
	cmpq	%rsi, %rcx
	jae	.LBB5_15
# BB#14:                                #   in Loop: Header=BB5_10 Depth=1
	movq	(%rsp), %rbx            # 8-byte Reload
	jmp	.LBB5_22
.LBB5_15:                               # %vector.body.preheader
                                        #   in Loop: Header=BB5_10 Depth=1
	leaq	-8(%r15), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	btl	$3, %esi
	jb	.LBB5_17
# BB#16:                                # %vector.body.prol
                                        #   in Loop: Header=BB5_10 Depth=1
	movups	-12(%r13,%rdi,4), %xmm0
	movups	-28(%r13,%rdi,4), %xmm1
	movups	-12(%rax,%rdi,4), %xmm2
	movups	-28(%rax,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%r13,%rdi,4)
	movups	%xmm3, -28(%r13,%rdi,4)
	movl	$8, %ebp
	testq	%rcx, %rcx
	jne	.LBB5_18
	jmp	.LBB5_20
.LBB5_17:                               #   in Loop: Header=BB5_10 Depth=1
	xorl	%ebp, %ebp
	testq	%rcx, %rcx
	je	.LBB5_20
.LBB5_18:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB5_10 Depth=1
	movq	%rdx, %rsi
	andq	$-8, %rsi
	negq	%rsi
	negq	%rbp
	leaq	(%r8,%rdi,4), %rbx
	leaq	-12(%rax,%rdi,4), %rcx
	.p2align	4, 0x90
.LBB5_19:                               # %vector.body
                                        #   Parent Loop BB5_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbx,%rbp,4), %xmm0
	movups	-16(%rbx,%rbp,4), %xmm1
	movups	(%rcx,%rbp,4), %xmm2
	movups	-16(%rcx,%rbp,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rbx,%rbp,4)
	movups	%xmm3, -16(%rbx,%rbp,4)
	movups	-32(%rbx,%rbp,4), %xmm0
	movups	-48(%rbx,%rbp,4), %xmm1
	movups	-32(%rcx,%rbp,4), %xmm2
	movups	-48(%rcx,%rbp,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbx,%rbp,4)
	movups	%xmm3, -48(%rbx,%rbp,4)
	addq	$-16, %rbp
	cmpq	%rbp, %rsi
	jne	.LBB5_19
.LBB5_20:                               # %middle.block
                                        #   in Loop: Header=BB5_10 Depth=1
	cmpq	%r15, %rdx
	movq	(%rsp), %rbx            # 8-byte Reload
	je	.LBB5_24
# BB#21:                                #   in Loop: Header=BB5_10 Depth=1
	subq	%r15, %rdi
	.p2align	4, 0x90
.LBB5_22:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB5_10 Depth=1
	incq	%rdi
	.p2align	4, 0x90
.LBB5_23:                               # %scalar.ph
                                        #   Parent Loop BB5_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rdi,4), %ecx
	orl	%ecx, -4(%r13,%rdi,4)
	decq	%rdi
	cmpq	$1, %rdi
	jg	.LBB5_23
.LBB5_24:                               # %.loopexit
                                        #   in Loop: Header=BB5_10 Depth=1
	movq	(%r12), %rax
	addq	$8, %r12
	testq	%rax, %rax
	jne	.LBB5_10
.LBB5_25:                               # %._crit_edge
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	setp_equal
	testl	%eax, %eax
	je	.LBB5_83
# BB#26:
	testq	%r13, %r13
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB5_28
# BB#27:
	movq	%r13, %rdi
	callq	free
.LBB5_28:
	cmpl	$1, cdata+32(%rip)
	jne	.LBB5_85
# BB#29:
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB5_39
# BB#30:
	movl	$8, %edi
	jmp	.LBB5_40
.LBB5_31:
	movq	cube+88(%rip), %rbx
	movl	(%rbx), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB5_37
# BB#32:
	movl	$8, %edi
	jmp	.LBB5_38
.LBB5_33:
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB5_39
# BB#34:
	movl	$8, %edi
	jmp	.LBB5_40
.LBB5_35:
	movq	cube+88(%rip), %rbx
	movl	(%rbx), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB5_44
# BB#36:
	movl	$8, %edi
	jmp	.LBB5_45
.LBB5_37:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB5_38:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbx, %rsi
	callq	set_copy
	jmp	.LBB5_41
.LBB5_39:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB5_40:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
.LBB5_41:
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rax, (%rcx)
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_43
# BB#42:
	callq	free
.LBB5_43:
	movq	%rbp, %rdi
	jmp	.LBB5_81
.LBB5_44:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB5_45:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbx, %rsi
	callq	set_copy
	movq	%rax, %r12
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%r12, (%rbx)
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	jne	.LBB5_47
	jmp	.LBB5_77
	.p2align	4, 0x90
.LBB5_46:                               # %sccc_cube.exit.backedge..lr.ph_crit_edge
                                        #   in Loop: Header=BB5_47 Depth=1
	movq	(%rbx), %r12
.LBB5_47:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_58 Depth 2
                                        #     Child Loop BB5_62 Depth 2
                                        #     Child Loop BB5_71 Depth 2
                                        #     Child Loop BB5_75 Depth 2
	addq	$8, %r14
	xorl	%eax, %eax
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rdx
	callq	set_or
	movq	%rax, %rbp
	movq	cube+80(%rip), %rax
	movq	(%rax), %r15
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	cactive
	testl	%eax, %eax
	js	.LBB5_76
# BB#48:                                #   in Loop: Header=BB5_47 Depth=1
	movq	cube+72(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movl	(%rbp), %ecx
	movl	(%r15), %edx
	movl	$-1024, %esi            # imm = 0xFC00
	andl	%esi, %edx
	andl	$1023, %ecx             # imm = 0x3FF
	movq	%rcx, %r9
	movl	$1, %esi
	cmoveq	%rsi, %r9
	movl	%ecx, %esi
	orl	%edx, %esi
	movl	%esi, (%r15)
	cmpq	$8, %r9
	jb	.LBB5_61
# BB#49:                                # %min.iters.checked427
                                        #   in Loop: Header=BB5_47 Depth=1
	movq	%r9, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB5_61
# BB#50:                                # %vector.memcheck459
                                        #   in Loop: Header=BB5_47 Depth=1
	leaq	1(%rcx), %rdx
	testl	%ecx, %ecx
	movl	$2, %esi
	cmovneq	%rsi, %rdx
	leaq	-4(%r15,%rdx,4), %rsi
	leaq	4(%r15,%rcx,4), %rdi
	leaq	-4(%rbp,%rdx,4), %r10
	leaq	4(%rbp,%rcx,4), %rbx
	leaq	-4(%rax,%rdx,4), %r11
	leaq	4(%rax,%rcx,4), %rdx
	cmpq	%rbx, %rsi
	sbbb	%bl, %bl
	cmpq	%rdi, %r10
	sbbb	%r10b, %r10b
	andb	%bl, %r10b
	cmpq	%rdx, %rsi
	sbbb	%dl, %dl
	cmpq	%rdi, %r11
	sbbb	%bl, %bl
	testb	$1, %r10b
	jne	.LBB5_55
# BB#51:                                # %vector.memcheck459
                                        #   in Loop: Header=BB5_47 Depth=1
	andb	%bl, %dl
	andb	$1, %dl
	jne	.LBB5_55
# BB#52:                                # %vector.body422.preheader
                                        #   in Loop: Header=BB5_47 Depth=1
	leaq	-8(%r8), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB5_56
# BB#53:                                # %vector.body422.prol
                                        #   in Loop: Header=BB5_47 Depth=1
	movups	-12(%rbp,%rcx,4), %xmm0
	movups	-28(%rbp,%rcx,4), %xmm1
	movups	-12(%rax,%rcx,4), %xmm2
	movups	-28(%rax,%rcx,4), %xmm3
	xorps	%xmm0, %xmm2
	xorps	%xmm1, %xmm3
	movups	%xmm2, -12(%r15,%rcx,4)
	movups	%xmm3, -28(%r15,%rcx,4)
	movl	$8, %edi
	testq	%rdx, %rdx
	jne	.LBB5_57
	jmp	.LBB5_59
.LBB5_55:                               #   in Loop: Header=BB5_47 Depth=1
	movq	(%rsp), %rbx            # 8-byte Reload
	jmp	.LBB5_61
.LBB5_56:                               #   in Loop: Header=BB5_47 Depth=1
	xorl	%edi, %edi
	testq	%rdx, %rdx
	je	.LBB5_59
.LBB5_57:                               # %vector.body422.preheader.new
                                        #   in Loop: Header=BB5_47 Depth=1
	movq	%r9, %r10
	andq	$-8, %r10
	negq	%r10
	negq	%rdi
	leaq	-12(%rbp,%rcx,4), %rsi
	leaq	-12(%rax,%rcx,4), %rdx
	leaq	-12(%r15,%rcx,4), %rbx
	.p2align	4, 0x90
.LBB5_58:                               # %vector.body422
                                        #   Parent Loop BB5_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rsi,%rdi,4), %xmm0
	movups	-16(%rsi,%rdi,4), %xmm1
	movups	(%rdx,%rdi,4), %xmm2
	movups	-16(%rdx,%rdi,4), %xmm3
	xorps	%xmm0, %xmm2
	xorps	%xmm1, %xmm3
	movups	%xmm2, (%rbx,%rdi,4)
	movups	%xmm3, -16(%rbx,%rdi,4)
	movups	-32(%rsi,%rdi,4), %xmm0
	movups	-48(%rsi,%rdi,4), %xmm1
	movups	-32(%rdx,%rdi,4), %xmm2
	movups	-48(%rdx,%rdi,4), %xmm3
	xorps	%xmm0, %xmm2
	xorps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbx,%rdi,4)
	movups	%xmm3, -48(%rbx,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %r10
	jne	.LBB5_58
.LBB5_59:                               # %middle.block423
                                        #   in Loop: Header=BB5_47 Depth=1
	cmpq	%r8, %r9
	movq	(%rsp), %rbx            # 8-byte Reload
	je	.LBB5_63
# BB#60:                                #   in Loop: Header=BB5_47 Depth=1
	subq	%r8, %rcx
	.p2align	4, 0x90
.LBB5_61:                               # %scalar.ph424.preheader
                                        #   in Loop: Header=BB5_47 Depth=1
	incq	%rcx
	.p2align	4, 0x90
.LBB5_62:                               # %scalar.ph424
                                        #   Parent Loop BB5_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rcx,4), %edx
	xorl	-4(%rbp,%rcx,4), %edx
	movl	%edx, -4(%r15,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB5_62
.LBB5_63:                               # %.loopexit481
                                        #   in Loop: Header=BB5_47 Depth=1
	movl	(%r12), %ecx
	movl	%ecx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movl	$1, %ecx
	cmovneq	%rax, %rcx
	cmpq	$8, %rcx
	jb	.LBB5_74
# BB#64:                                # %min.iters.checked378
                                        #   in Loop: Header=BB5_47 Depth=1
	movq	%rcx, %rdx
	andq	$1016, %rdx             # imm = 0x3F8
	je	.LBB5_74
# BB#65:                                # %vector.memcheck400
                                        #   in Loop: Header=BB5_47 Depth=1
	leaq	1(%rax), %rsi
	testl	%eax, %eax
	movl	$2, %edi
	cmovneq	%rdi, %rsi
	leaq	-4(%r12,%rsi,4), %rdi
	leaq	4(%r15,%rax,4), %rbp
	cmpq	%rbp, %rdi
	jae	.LBB5_67
# BB#66:                                # %vector.memcheck400
                                        #   in Loop: Header=BB5_47 Depth=1
	leaq	4(%r12,%rax,4), %rdi
	leaq	-4(%r15,%rsi,4), %rsi
	cmpq	%rdi, %rsi
	jb	.LBB5_74
.LBB5_67:                               # %vector.body373.preheader
                                        #   in Loop: Header=BB5_47 Depth=1
	leaq	-8(%rdx), %rsi
	movq	%rsi, %rdi
	shrq	$3, %rdi
	btl	$3, %esi
	jb	.LBB5_69
# BB#68:                                # %vector.body373.prol
                                        #   in Loop: Header=BB5_47 Depth=1
	movups	-12(%r12,%rax,4), %xmm0
	movups	-28(%r12,%rax,4), %xmm1
	movups	-12(%r15,%rax,4), %xmm2
	movups	-28(%r15,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%r12,%rax,4)
	movups	%xmm3, -28(%r12,%rax,4)
	movl	$8, %esi
	testq	%rdi, %rdi
	jne	.LBB5_70
	jmp	.LBB5_72
.LBB5_69:                               #   in Loop: Header=BB5_47 Depth=1
	xorl	%esi, %esi
	testq	%rdi, %rdi
	je	.LBB5_72
.LBB5_70:                               # %vector.body373.preheader.new
                                        #   in Loop: Header=BB5_47 Depth=1
	movq	%rcx, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rsi
	leaq	-12(%r12,%rax,4), %rbp
	leaq	-12(%r15,%rax,4), %rbx
	.p2align	4, 0x90
.LBB5_71:                               # %vector.body373
                                        #   Parent Loop BB5_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbp,%rsi,4), %xmm0
	movups	-16(%rbp,%rsi,4), %xmm1
	movups	(%rbx,%rsi,4), %xmm2
	movups	-16(%rbx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rbp,%rsi,4)
	movups	%xmm3, -16(%rbp,%rsi,4)
	movups	-32(%rbp,%rsi,4), %xmm0
	movups	-48(%rbp,%rsi,4), %xmm1
	movups	-32(%rbx,%rsi,4), %xmm2
	movups	-48(%rbx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbp,%rsi,4)
	movups	%xmm3, -48(%rbp,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB5_71
.LBB5_72:                               # %middle.block374
                                        #   in Loop: Header=BB5_47 Depth=1
	cmpq	%rdx, %rcx
	movq	(%rsp), %rbx            # 8-byte Reload
	je	.LBB5_76
# BB#73:                                #   in Loop: Header=BB5_47 Depth=1
	subq	%rdx, %rax
	.p2align	4, 0x90
.LBB5_74:                               # %scalar.ph375.preheader
                                        #   in Loop: Header=BB5_47 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB5_75:                               # %scalar.ph375
                                        #   Parent Loop BB5_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%r15,%rax,4), %ecx
	andl	%ecx, -4(%r12,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB5_75
.LBB5_76:                               # %sccc_cube.exit.backedge
                                        #   in Loop: Header=BB5_47 Depth=1
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	jne	.LBB5_46
.LBB5_77:                               # %sccc_cube.exit._crit_edge
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB5_78:                               # %sccc_cube.exit._crit_edge
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_80
# BB#79:
	callq	free
.LBB5_80:
	movq	%rbx, %rdi
.LBB5_81:
	callq	free
	movl	$1, %ebx
.LBB5_82:
	movl	%ebx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_83:
	movq	cube+88(%rip), %rbp
	movl	(%rbp), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB5_91
# BB#84:
	movl	$8, %edi
	jmp	.LBB5_92
.LBB5_85:
	movq	cdata+8(%rip), %rax
	movslq	cdata+40(%rip), %rcx
	movslq	(%rax,%rcx,4), %rax
	movq	8(%rbp), %rcx
	subq	%rbp, %rcx
	sarq	$3, %rcx
	leaq	-3(%rcx), %rdx
	shrq	$63, %rdx
	leaq	-3(%rcx,%rdx), %rcx
	sarq	%rcx
	movl	$2, %ebx
	cmpq	%rcx, %rax
	jge	.LBB5_82
# BB#86:
	movl	debug(%rip), %ecx
	andl	$128, %ecx
	leaq	32(%rsp), %rsi
	leaq	24(%rsp), %rdx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	cubelist_partition
	testl	%eax, %eax
	je	.LBB5_82
# BB#87:
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_89
# BB#88:
	callq	free
.LBB5_89:
	movq	%rbp, %rdi
	callq	free
	movq	32(%rsp), %rdi
	callq	sccc
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rax, (%rbx)
	movq	24(%rsp), %rdi
	callq	sccc
	movq	%rax, %rbp
	movq	(%rbx), %rdi
	xorl	%eax, %eax
	movq	%rdi, %rsi
	movq	%rbp, %rdx
	callq	set_and
	movl	$1, %ebx
	testq	%rbp, %rbp
	je	.LBB5_82
# BB#90:
	movq	%rbp, %rdi
	callq	free
	jmp	.LBB5_82
.LBB5_91:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB5_92:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbp, %rsi
	callq	set_copy
	movq	%rax, %r12
	movq	cube+80(%rip), %rax
	movq	(%rax), %rbp
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	cactive
	testl	%eax, %eax
	js	.LBB5_121
# BB#93:
	movq	cube+72(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movl	(%r13), %ecx
	movl	%ecx, %edx
	notl	%edx
	movl	$-1024, %esi            # imm = 0xFC00
	andl	(%rbp), %esi
	orq	$-1024, %rdx            # imm = 0xFC00
	andl	$1023, %ecx             # imm = 0x3FF
	movq	$-2, %rdi
	cmoveq	%rdx, %rdi
	orl	%ecx, %esi
	movl	%esi, (%rbp)
	leaq	2(%rdi,%rcx), %r9
	cmpq	$8, %r9
	jb	.LBB5_106
# BB#94:                                # %min.iters.checked146
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB5_106
# BB#95:                                # %vector.memcheck171
	testl	%ecx, %ecx
	movl	$1, %edx
	cmoveq	%rcx, %rdx
	leaq	(%rbp,%rdx,4), %rsi
	leaq	4(%rbp,%rcx,4), %rdi
	leaq	(%r13,%rdx,4), %r10
	leaq	4(%r13,%rcx,4), %rbx
	leaq	(%rax,%rdx,4), %r11
	leaq	4(%rax,%rcx,4), %r14
	cmpq	%rbx, %rsi
	sbbb	%dl, %dl
	cmpq	%rdi, %r10
	sbbb	%bl, %bl
	andb	%dl, %bl
	cmpq	%r14, %rsi
	sbbb	%dl, %dl
	cmpq	%rdi, %r11
	sbbb	%sil, %sil
	testb	$1, %bl
	jne	.LBB5_100
# BB#96:                                # %vector.memcheck171
	andb	%sil, %dl
	andb	$1, %dl
	jne	.LBB5_100
# BB#97:                                # %vector.body141.preheader
	leaq	-8(%r8), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB5_101
# BB#98:                                # %vector.body141.prol
	movups	-12(%r13,%rcx,4), %xmm0
	movups	-28(%r13,%rcx,4), %xmm1
	movups	-12(%rax,%rcx,4), %xmm2
	movups	-28(%rax,%rcx,4), %xmm3
	xorps	%xmm0, %xmm2
	xorps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbp,%rcx,4)
	movups	%xmm3, -28(%rbp,%rcx,4)
	movl	$8, %edi
	testq	%rdx, %rdx
	jne	.LBB5_102
	jmp	.LBB5_104
.LBB5_100:
	movq	(%rsp), %rbx            # 8-byte Reload
	jmp	.LBB5_106
.LBB5_101:
	xorl	%edi, %edi
	testq	%rdx, %rdx
	je	.LBB5_104
.LBB5_102:                              # %vector.body141.preheader.new
	movq	%r8, %r10
	negq	%r10
	negq	%rdi
	leaq	-12(%r13,%rcx,4), %rdx
	leaq	-12(%rax,%rcx,4), %rbx
	leaq	-12(%rbp,%rcx,4), %rsi
.LBB5_103:                              # %vector.body141
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdx,%rdi,4), %xmm0
	movups	-16(%rdx,%rdi,4), %xmm1
	movups	(%rbx,%rdi,4), %xmm2
	movups	-16(%rbx,%rdi,4), %xmm3
	xorps	%xmm0, %xmm2
	xorps	%xmm1, %xmm3
	movups	%xmm2, (%rsi,%rdi,4)
	movups	%xmm3, -16(%rsi,%rdi,4)
	movups	-32(%rdx,%rdi,4), %xmm0
	movups	-48(%rdx,%rdi,4), %xmm1
	movups	-32(%rbx,%rdi,4), %xmm2
	movups	-48(%rbx,%rdi,4), %xmm3
	xorps	%xmm0, %xmm2
	xorps	%xmm1, %xmm3
	movups	%xmm2, -32(%rsi,%rdi,4)
	movups	%xmm3, -48(%rsi,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %r10
	jne	.LBB5_103
.LBB5_104:                              # %middle.block142
	cmpq	%r8, %r9
	movq	(%rsp), %rbx            # 8-byte Reload
	je	.LBB5_108
# BB#105:
	subq	%r8, %rcx
.LBB5_106:                              # %scalar.ph143.preheader
	incq	%rcx
	.p2align	4, 0x90
.LBB5_107:                              # %scalar.ph143
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rax,%rcx,4), %edx
	xorl	-4(%r13,%rcx,4), %edx
	movl	%edx, -4(%rbp,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB5_107
.LBB5_108:                              # %.loopexit485
	movl	(%r12), %ecx
	movl	%ecx, %edx
	notl	%edx
	movl	%ecx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	orq	$-1024, %rdx            # imm = 0xFC00
	testw	$1023, %cx              # imm = 0x3FF
	movq	$-2, %rcx
	cmoveq	%rdx, %rcx
	leaq	2(%rcx,%rax), %rcx
	cmpq	$8, %rcx
	jb	.LBB5_119
# BB#109:                               # %min.iters.checked198
	movq	%rcx, %r8
	andq	$-8, %r8
	je	.LBB5_119
# BB#110:                               # %vector.memcheck216
	testl	%eax, %eax
	movl	$1, %edx
	cmoveq	%rax, %rdx
	leaq	(%r12,%rdx,4), %rsi
	leaq	4(%rbp,%rax,4), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB5_112
# BB#111:                               # %vector.memcheck216
	leaq	4(%r12,%rax,4), %rsi
	leaq	(%rbp,%rdx,4), %rdx
	cmpq	%rsi, %rdx
	jb	.LBB5_119
.LBB5_112:                              # %vector.body193.preheader
	leaq	-8(%r8), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB5_114
# BB#113:                               # %vector.body193.prol
	movups	-12(%r12,%rax,4), %xmm0
	movups	-28(%r12,%rax,4), %xmm1
	movups	-12(%rbp,%rax,4), %xmm2
	movups	-28(%rbp,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%r12,%rax,4)
	movups	%xmm3, -28(%r12,%rax,4)
	movl	$8, %esi
	testq	%rdx, %rdx
	jne	.LBB5_115
	jmp	.LBB5_117
.LBB5_114:
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.LBB5_117
.LBB5_115:                              # %vector.body193.preheader.new
	movq	%r8, %rdi
	negq	%rdi
	negq	%rsi
	leaq	-12(%r12,%rax,4), %rbx
	leaq	-12(%rbp,%rax,4), %rdx
.LBB5_116:                              # %vector.body193
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbx,%rsi,4), %xmm0
	movups	-16(%rbx,%rsi,4), %xmm1
	movups	(%rdx,%rsi,4), %xmm2
	movups	-16(%rdx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rbx,%rsi,4)
	movups	%xmm3, -16(%rbx,%rsi,4)
	movups	-32(%rbx,%rsi,4), %xmm0
	movups	-48(%rbx,%rsi,4), %xmm1
	movups	-32(%rdx,%rsi,4), %xmm2
	movups	-48(%rdx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbx,%rsi,4)
	movups	%xmm3, -48(%rbx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB5_116
.LBB5_117:                              # %middle.block194
	cmpq	%r8, %rcx
	movq	(%rsp), %rbx            # 8-byte Reload
	je	.LBB5_121
# BB#118:
	subq	%r8, %rax
.LBB5_119:                              # %scalar.ph195.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB5_120:                              # %scalar.ph195
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rbp,%rax,4), %ecx
	andl	%ecx, -4(%r12,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB5_120
.LBB5_121:                              # %sccc_cube.exit112
	movq	%r12, (%rbx)
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	setp_equal
	testl	%eax, %eax
	je	.LBB5_124
# BB#122:
	testq	%r13, %r13
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB5_78
# BB#123:
	movq	%r13, %rdi
	callq	free
	jmp	.LBB5_78
.LBB5_124:
	xorl	%eax, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r13, %rsi
	callq	cofactor
	movq	%rax, %rdi
	callq	sccc
	movq	%rax, %r12
	movq	cube+88(%rip), %rbp
	movl	(%rbp), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB5_126
# BB#125:
	movl	$8, %edi
	jmp	.LBB5_127
.LBB5_126:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB5_127:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbp, %rsi
	callq	set_copy
	movq	(%rbx), %rbx
	movl	(%r12), %edx
	movl	%edx, %esi
	notl	%esi
	movl	%edx, %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	orq	$-1024, %rsi            # imm = 0xFC00
	testw	$1023, %dx              # imm = 0x3FF
	movq	$-2, %rdx
	cmoveq	%rsi, %rdx
	leaq	2(%rdx,%rcx), %r9
	cmpq	$8, %r9
	jb	.LBB5_138
# BB#128:                               # %min.iters.checked243
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB5_138
# BB#129:                               # %vector.memcheck261
	testl	%ecx, %ecx
	movl	$1, %edx
	cmoveq	%rcx, %rdx
	leaq	(%r12,%rdx,4), %rsi
	leaq	4(%r13,%rcx,4), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB5_131
# BB#130:                               # %vector.memcheck261
	leaq	4(%r12,%rcx,4), %rsi
	leaq	(%r13,%rdx,4), %rdx
	cmpq	%rsi, %rdx
	jb	.LBB5_138
.LBB5_131:                              # %vector.body238.preheader
	leaq	-8(%r8), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB5_133
# BB#132:                               # %vector.body238.prol
	movups	-12(%r12,%rcx,4), %xmm0
	movups	-28(%r12,%rcx,4), %xmm1
	movups	-12(%r13,%rcx,4), %xmm2
	movups	-28(%r13,%rcx,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%r12,%rcx,4)
	movups	%xmm3, -28(%r12,%rcx,4)
	movl	$8, %edi
	testq	%rdx, %rdx
	jne	.LBB5_134
	jmp	.LBB5_136
.LBB5_133:
	xorl	%edi, %edi
	testq	%rdx, %rdx
	je	.LBB5_136
.LBB5_134:                              # %vector.body238.preheader.new
	movq	%r8, %rsi
	negq	%rsi
	negq	%rdi
	leaq	-12(%r12,%rcx,4), %rbp
	leaq	-12(%r13,%rcx,4), %rdx
.LBB5_135:                              # %vector.body238
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rdi,4), %xmm0
	movups	-16(%rbp,%rdi,4), %xmm1
	movups	(%rdx,%rdi,4), %xmm2
	movups	-16(%rdx,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rbp,%rdi,4)
	movups	%xmm3, -16(%rbp,%rdi,4)
	movups	-32(%rbp,%rdi,4), %xmm0
	movups	-48(%rbp,%rdi,4), %xmm1
	movups	-32(%rdx,%rdi,4), %xmm2
	movups	-48(%rdx,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbp,%rdi,4)
	movups	%xmm3, -48(%rbp,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %rsi
	jne	.LBB5_135
.LBB5_136:                              # %middle.block239
	cmpq	%r8, %r9
	je	.LBB5_140
# BB#137:
	subq	%r8, %rcx
.LBB5_138:                              # %scalar.ph240.preheader
	incq	%rcx
	.p2align	4, 0x90
.LBB5_139:                              # %scalar.ph240
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r13,%rcx,4), %edx
	andl	%edx, -4(%r12,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB5_139
.LBB5_140:                              # %.loopexit484
	movl	(%rax), %edx
	movl	%edx, %esi
	notl	%esi
	movl	%edx, %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	orq	$-1024, %rsi            # imm = 0xFC00
	testw	$1023, %dx              # imm = 0x3FF
	movq	$-2, %rdx
	cmoveq	%rsi, %rdx
	leaq	2(%rdx,%rcx), %r9
	cmpq	$8, %r9
	jb	.LBB5_151
# BB#141:                               # %min.iters.checked288
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB5_151
# BB#142:                               # %vector.memcheck306
	testl	%ecx, %ecx
	movl	$1, %edx
	cmoveq	%rcx, %rdx
	leaq	(%rax,%rdx,4), %rsi
	leaq	4(%rbx,%rcx,4), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB5_144
# BB#143:                               # %vector.memcheck306
	leaq	4(%rax,%rcx,4), %rsi
	leaq	(%rbx,%rdx,4), %rdx
	cmpq	%rsi, %rdx
	jb	.LBB5_151
.LBB5_144:                              # %vector.body283.preheader
	leaq	-8(%r8), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB5_146
# BB#145:                               # %vector.body283.prol
	movups	-12(%rax,%rcx,4), %xmm0
	movups	-28(%rax,%rcx,4), %xmm1
	movups	-12(%rbx,%rcx,4), %xmm2
	movups	-28(%rbx,%rcx,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rax,%rcx,4)
	movups	%xmm3, -28(%rax,%rcx,4)
	movl	$8, %edi
	testq	%rdx, %rdx
	jne	.LBB5_147
	jmp	.LBB5_149
.LBB5_146:
	xorl	%edi, %edi
	testq	%rdx, %rdx
	je	.LBB5_149
.LBB5_147:                              # %vector.body283.preheader.new
	movq	%r8, %rsi
	negq	%rsi
	negq	%rdi
	leaq	-12(%rax,%rcx,4), %rbp
	leaq	-12(%rbx,%rcx,4), %rdx
.LBB5_148:                              # %vector.body283
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rdi,4), %xmm0
	movups	-16(%rbp,%rdi,4), %xmm1
	movups	(%rdx,%rdi,4), %xmm2
	movups	-16(%rdx,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rbp,%rdi,4)
	movups	%xmm3, -16(%rbp,%rdi,4)
	movups	-32(%rbp,%rdi,4), %xmm0
	movups	-48(%rbp,%rdi,4), %xmm1
	movups	-32(%rdx,%rdi,4), %xmm2
	movups	-48(%rdx,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbp,%rdi,4)
	movups	%xmm3, -48(%rbp,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %rsi
	jne	.LBB5_148
.LBB5_149:                              # %middle.block284
	cmpq	%r8, %r9
	je	.LBB5_153
# BB#150:
	subq	%r8, %rcx
.LBB5_151:                              # %scalar.ph285.preheader
	incq	%rcx
	.p2align	4, 0x90
.LBB5_152:                              # %scalar.ph285
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rbx,%rcx,4), %edx
	andl	%edx, -4(%rax,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB5_152
.LBB5_153:                              # %.loopexit483
	movl	(%r12), %edx
	movl	%edx, %esi
	notl	%esi
	movl	%edx, %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	orq	$-1024, %rsi            # imm = 0xFC00
	testw	$1023, %dx              # imm = 0x3FF
	movq	$-2, %rdx
	cmoveq	%rsi, %rdx
	leaq	2(%rdx,%rcx), %r9
	cmpq	$8, %r9
	jb	.LBB5_164
# BB#154:                               # %min.iters.checked333
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB5_164
# BB#155:                               # %vector.memcheck351
	testl	%ecx, %ecx
	movl	$1, %edx
	cmoveq	%rcx, %rdx
	leaq	(%r12,%rdx,4), %rsi
	leaq	4(%rax,%rcx,4), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB5_157
# BB#156:                               # %vector.memcheck351
	leaq	4(%r12,%rcx,4), %rsi
	leaq	(%rax,%rdx,4), %rdx
	cmpq	%rsi, %rdx
	jb	.LBB5_164
.LBB5_157:                              # %vector.body328.preheader
	leaq	-8(%r8), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB5_159
# BB#158:                               # %vector.body328.prol
	movups	-12(%r12,%rcx,4), %xmm0
	movups	-28(%r12,%rcx,4), %xmm1
	movups	-12(%rax,%rcx,4), %xmm2
	movups	-28(%rax,%rcx,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%r12,%rcx,4)
	movups	%xmm3, -28(%r12,%rcx,4)
	movl	$8, %edi
	testq	%rdx, %rdx
	jne	.LBB5_160
	jmp	.LBB5_162
.LBB5_159:
	xorl	%edi, %edi
	testq	%rdx, %rdx
	je	.LBB5_162
.LBB5_160:                              # %vector.body328.preheader.new
	movq	%r8, %rsi
	negq	%rsi
	negq	%rdi
	leaq	-12(%r12,%rcx,4), %rbp
	leaq	-12(%rax,%rcx,4), %rdx
.LBB5_161:                              # %vector.body328
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rdi,4), %xmm0
	movups	-16(%rbp,%rdi,4), %xmm1
	movups	(%rdx,%rdi,4), %xmm2
	movups	-16(%rdx,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rbp,%rdi,4)
	movups	%xmm3, -16(%rbp,%rdi,4)
	movups	-32(%rbp,%rdi,4), %xmm0
	movups	-48(%rbp,%rdi,4), %xmm1
	movups	-32(%rdx,%rdi,4), %xmm2
	movups	-48(%rdx,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbp,%rdi,4)
	movups	%xmm3, -48(%rbp,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %rsi
	jne	.LBB5_161
.LBB5_162:                              # %middle.block329
	cmpq	%r8, %r9
	je	.LBB5_166
# BB#163:
	subq	%r8, %rcx
.LBB5_164:                              # %scalar.ph330.preheader
	incq	%rcx
	.p2align	4, 0x90
.LBB5_165:                              # %scalar.ph330
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rax,%rcx,4), %edx
	orl	%edx, -4(%r12,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB5_165
.LBB5_166:                              # %.loopexit482
	movq	%rax, %rdi
	callq	free
	testq	%r13, %r13
	je	.LBB5_168
# BB#167:
	movq	%r13, %rdi
	callq	free
.LBB5_168:
	testq	%rbx, %rbx
	je	.LBB5_170
# BB#169:
	movq	%rbx, %rdi
	callq	free
.LBB5_170:                              # %sccc_merge.exit
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%r12, (%rax)
	jmp	.LBB5_77
.Lfunc_end5:
	.size	sccc_special_cases, .Lfunc_end5-sccc_special_cases
	.cfi_endproc

	.type	toggle,@object          # @toggle
	.data
	.p2align	2
toggle:
	.long	1                       # 0x1
	.size	toggle, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"REDUCE: %s to %s %s\n"
	.size	.L.str, 21

	.type	sccc.sccc_level,@object # @sccc.sccc_level
	.local	sccc.sccc_level
	.comm	sccc.sccc_level,4,4
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SCCC"
	.size	.L.str.1, 5

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"SCCC[%d]: result is %s\n"
	.size	.L.str.2, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
