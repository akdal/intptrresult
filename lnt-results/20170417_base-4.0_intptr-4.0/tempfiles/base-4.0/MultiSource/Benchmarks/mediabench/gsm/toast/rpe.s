	.text
	.file	"rpe.bc"
	.globl	Gsm_RPE_Encoding
	.p2align	4, 0x90
	.type	Gsm_RPE_Encoding,@function
Gsm_RPE_Encoding:                       # @Gsm_RPE_Encoding
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 272
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, 168(%rsp)          # 8-byte Spill
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movq	%rsi, %r12
	movw	-10(%r12), %r10w
	movzwl	-4(%r12), %ebp
	movzwl	-2(%r12), %ecx
	movzwl	(%r12), %r11d
	movw	2(%r12), %r15w
	movw	8(%r12), %r14w
	xorl	%esi, %esi
	movw	$-32768, %r8w           # imm = 0x8000
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movswq	%bp, %r13
	movl	%ecx, %ebp
	movswq	%bp, %r9
	movl	%r11d, %ecx
	movl	%r15d, %r11d
	movswq	%r10w, %rax
	movswq	-8(%r12,%rsi,2), %r10
	movswq	%cx, %rdx
	shlq	$13, %rdx
	movswq	%r11w, %rdi
	movswq	4(%r12,%rsi,2), %r15
	movswq	%r14w, %rbx
	movswq	10(%r12,%rsi,2), %r14
	addq	%r9, %rdi
	imulq	$5741, %rdi, %r9        # imm = 0x166D
	addq	%r15, %r13
	imulq	$2054, %r13, %rdi       # imm = 0x806
	addq	%r10, %rbx
	imulq	$-374, %rbx, %rbx       # imm = 0xFE8A
	addq	%r14, %rax
	imulq	$-134, %rax, %rax
	leaq	4096(%r9,%rdx), %rdx
	addq	%rdi, %rdx
	addq	%rbx, %rdx
	addq	%rax, %rdx
	sarq	$13, %rdx
	cmpq	$32767, %rdx            # imm = 0x7FFF
	movw	$32767, %ax             # imm = 0x7FFF
	cmovlw	%dx, %ax
	cmpq	$-32768, %rdx           # imm = 0x8000
	cmovlw	%r8w, %ax
	movw	%ax, 32(%rsp,%rsi,2)
	incq	%rsi
	cmpq	$40, %rsi
	jne	.LBB0_1
# BB#2:                                 # %Weighting_filter.exit
	movswl	38(%rsp), %eax
	sarl	$2, %eax
	cltq
	imulq	%rax, %rax
	movswl	44(%rsp), %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	imulq	%rcx, %rcx
	addq	%rax, %rcx
	movswl	50(%rsp), %eax
	sarl	$2, %eax
	cltq
	imulq	%rax, %rax
	addq	%rcx, %rax
	movswl	56(%rsp), %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	imulq	%rcx, %rcx
	addq	%rax, %rcx
	movswl	62(%rsp), %eax
	sarl	$2, %eax
	cltq
	imulq	%rax, %rax
	addq	%rcx, %rax
	movswl	68(%rsp), %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	imulq	%rcx, %rcx
	addq	%rax, %rcx
	movswl	74(%rsp), %eax
	sarl	$2, %eax
	cltq
	imulq	%rax, %rax
	addq	%rcx, %rax
	movswl	80(%rsp), %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	imulq	%rcx, %rcx
	addq	%rax, %rcx
	movswl	86(%rsp), %eax
	sarl	$2, %eax
	cltq
	imulq	%rax, %rax
	addq	%rcx, %rax
	movswl	92(%rsp), %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	imulq	%rcx, %rcx
	addq	%rax, %rcx
	movswl	98(%rsp), %eax
	sarl	$2, %eax
	movslq	%eax, %rdx
	imulq	%rdx, %rdx
	addq	%rcx, %rdx
	movswl	104(%rsp), %eax
	sarl	$2, %eax
	cltq
	imulq	%rax, %rax
	addq	%rdx, %rax
	movswl	32(%rsp), %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rsi
	imulq	%rsi, %rsi
	addq	%rax, %rsi
	addq	%rsi, %rsi
	movswl	34(%rsp), %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	imulq	%rcx, %rcx
	movswl	40(%rsp), %edx
	sarl	$2, %edx
	movslq	%edx, %rdx
	imulq	%rdx, %rdx
	addq	%rcx, %rdx
	movswl	46(%rsp), %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	imulq	%rcx, %rcx
	addq	%rdx, %rcx
	movswl	52(%rsp), %edx
	sarl	$2, %edx
	movslq	%edx, %rdx
	imulq	%rdx, %rdx
	addq	%rcx, %rdx
	movswl	58(%rsp), %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	imulq	%rcx, %rcx
	addq	%rdx, %rcx
	movswl	64(%rsp), %edx
	sarl	$2, %edx
	movslq	%edx, %rdx
	imulq	%rdx, %rdx
	addq	%rcx, %rdx
	movswl	70(%rsp), %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	imulq	%rcx, %rcx
	addq	%rdx, %rcx
	movswl	76(%rsp), %edx
	sarl	$2, %edx
	movslq	%edx, %rdx
	imulq	%rdx, %rdx
	addq	%rcx, %rdx
	movswl	82(%rsp), %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	imulq	%rcx, %rcx
	addq	%rdx, %rcx
	movswl	88(%rsp), %edx
	sarl	$2, %edx
	movslq	%edx, %rdx
	imulq	%rdx, %rdx
	addq	%rcx, %rdx
	movswl	94(%rsp), %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	imulq	%rcx, %rcx
	addq	%rdx, %rcx
	movswl	100(%rsp), %edx
	sarl	$2, %edx
	movslq	%edx, %rdx
	imulq	%rdx, %rdx
	addq	%rcx, %rdx
	movswl	106(%rsp), %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	imulq	%rcx, %rcx
	addq	%rdx, %rcx
	addq	%rcx, %rcx
	xorl	%edx, %edx
	cmpq	%rsi, %rcx
	setg	%dl
	cmovlq	%rsi, %rcx
	movswl	36(%rsp), %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	imulq	%rsi, %rsi
	movswl	42(%rsp), %edi
	sarl	$2, %edi
	movslq	%edi, %rdi
	imulq	%rdi, %rdi
	addq	%rsi, %rdi
	movswl	48(%rsp), %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	imulq	%rsi, %rsi
	addq	%rdi, %rsi
	movswl	54(%rsp), %edi
	sarl	$2, %edi
	movslq	%edi, %rdi
	imulq	%rdi, %rdi
	addq	%rsi, %rdi
	movswl	60(%rsp), %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	imulq	%rsi, %rsi
	addq	%rdi, %rsi
	movswl	66(%rsp), %edi
	sarl	$2, %edi
	movslq	%edi, %rdi
	imulq	%rdi, %rdi
	addq	%rsi, %rdi
	movswl	72(%rsp), %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	imulq	%rsi, %rsi
	addq	%rdi, %rsi
	movswl	78(%rsp), %edi
	sarl	$2, %edi
	movslq	%edi, %rdi
	imulq	%rdi, %rdi
	addq	%rsi, %rdi
	movswl	84(%rsp), %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	imulq	%rsi, %rsi
	addq	%rdi, %rsi
	movswl	90(%rsp), %edi
	sarl	$2, %edi
	movslq	%edi, %rdi
	imulq	%rdi, %rdi
	addq	%rsi, %rdi
	movswl	96(%rsp), %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	imulq	%rsi, %rsi
	addq	%rdi, %rsi
	movswl	102(%rsp), %edi
	sarl	$2, %edi
	movslq	%edi, %rdi
	imulq	%rdi, %rdi
	addq	%rsi, %rdi
	movswl	108(%rsp), %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	imulq	%rsi, %rsi
	addq	%rdi, %rsi
	addq	%rsi, %rsi
	cmpq	%rcx, %rsi
	cmovlq	%rcx, %rsi
	movw	$2, %cx
	cmovlew	%dx, %cx
	movswl	110(%rsp), %edx
	sarl	$2, %edx
	movslq	%edx, %rdx
	imulq	%rdx, %rdx
	addq	%rax, %rdx
	addq	%rdx, %rdx
	cmpq	%rsi, %rdx
	movw	$3, %ax
	cmovlew	%cx, %ax
	movzwl	%ax, %eax
	leaq	(%rax,%rax), %rcx
	leaq	32(%rsp), %rdx
	orq	%rdx, %rcx
	movswl	(%rcx), %r8d
	leaq	6(%rax,%rax), %rsi
	orq	%rdx, %rsi
	movq	%rax, %rdx
	orq	$12, %rdx
	movq	%rax, %rdi
	orq	$24, %rdi
	movq	%rax, %rbp
	orq	$36, %rbp
	testl	%r8d, %r8d
	movw	(%rsi), %r9w
	movw	12(%rcx), %r10w
	movw	18(%rcx), %r11w
	movw	32(%rsp,%rdx,2), %r13w
	movw	30(%rcx), %r14w
	movw	36(%rcx), %si
	movw	42(%rcx), %bx
	movw	32(%rsp,%rdi,2), %r15w
	movw	54(%rcx), %di
	movw	60(%rcx), %dx
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movw	66(%rcx), %cx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movw	32(%rsp,%rbp,2), %cx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movq	144(%rsp), %rcx         # 8-byte Reload
	movw	%ax, (%rcx)
	js	.LBB0_4
# BB#3:
	movl	%r8d, %ecx
	jmp	.LBB0_5
.LBB0_4:
	movzwl	%r8w, %eax
	movl	%r8d, %edx
	negl	%edx
	cmpl	$32768, %eax            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	cmovnew	%dx, %cx
.LBB0_5:
	xorl	%eax, %eax
	testw	%cx, %cx
	cmovnsw	%cx, %ax
	testw	%r9w, %r9w
	movl	%r9d, %ecx
	jns	.LBB0_7
# BB#6:
	movzwl	%r9w, %ecx
	movl	%r9d, %edx
	negl	%edx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	cmovnew	%dx, %cx
.LBB0_7:
	cmpw	%ax, %cx
	cmovgew	%cx, %ax
	testw	%r10w, %r10w
	movl	%r10d, %ecx
	jns	.LBB0_9
# BB#8:
	movzwl	%r10w, %ecx
	movl	%r10d, %edx
	negl	%edx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	cmovnew	%dx, %cx
.LBB0_9:
	cmpw	%ax, %cx
	cmovgew	%cx, %ax
	testw	%r11w, %r11w
	movl	%r11d, %ecx
	jns	.LBB0_11
# BB#10:
	movzwl	%r11w, %ecx
	movl	%r11d, %edx
	negl	%edx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	cmovnew	%dx, %cx
.LBB0_11:
	cmpw	%ax, %cx
	cmovgew	%cx, %ax
	testw	%r13w, %r13w
	movl	%r13d, %ecx
	jns	.LBB0_13
# BB#12:
	movzwl	%r13w, %ecx
	movl	%r13d, %edx
	negl	%edx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	cmovnew	%dx, %cx
.LBB0_13:
	cmpw	%ax, %cx
	cmovgew	%cx, %ax
	testw	%r14w, %r14w
	movl	%r14d, %ecx
	jns	.LBB0_15
# BB#14:
	movzwl	%r14w, %ecx
	movl	%r14d, %edx
	negl	%edx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	cmovnew	%dx, %cx
.LBB0_15:
	cmpw	%ax, %cx
	cmovgew	%cx, %ax
	testw	%si, %si
	movl	%esi, %ecx
	jns	.LBB0_17
# BB#16:
	movzwl	%si, %ecx
	movl	%esi, %edx
	negl	%edx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	cmovnew	%dx, %cx
.LBB0_17:
	cmpw	%ax, %cx
	cmovgew	%cx, %ax
	testw	%bx, %bx
	movl	%ebx, %ecx
	jns	.LBB0_19
# BB#18:
	movzwl	%bx, %ecx
	movl	%ebx, %edx
	negl	%edx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	cmovnew	%dx, %cx
.LBB0_19:
	cmpw	%ax, %cx
	cmovgew	%cx, %ax
	testw	%r15w, %r15w
	movl	%r15d, %ecx
	jns	.LBB0_21
# BB#20:
	movzwl	%r15w, %ecx
	movl	%r15d, %edx
	negl	%edx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	cmovnew	%dx, %cx
.LBB0_21:
	cmpw	%ax, %cx
	cmovgew	%cx, %ax
	testw	%di, %di
	movl	%edi, %ecx
	jns	.LBB0_23
# BB#22:
	movzwl	%di, %ecx
	movl	%edi, %edx
	negl	%edx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	cmovnew	%dx, %cx
.LBB0_23:
	cmpw	%ax, %cx
	cmovgew	%cx, %ax
	movl	16(%rsp), %ecx          # 4-byte Reload
	testw	%cx, %cx
                                        # kill: %CX<def> %CX<kill> %ECX<kill>
	jns	.LBB0_25
# BB#24:
	movl	16(%rsp), %edx          # 4-byte Reload
	movzwl	%dx, %ecx
	negl	%edx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	cmovnew	%dx, %cx
.LBB0_25:
	movl	%edi, 124(%rsp)         # 4-byte Spill
	movl	%ebx, 128(%rsp)         # 4-byte Spill
	movl	%esi, 132(%rsp)         # 4-byte Spill
	cmpw	%ax, %cx
	cmovgew	%cx, %ax
	movl	8(%rsp), %ecx           # 4-byte Reload
	testw	%cx, %cx
                                        # kill: %CX<def> %CX<kill> %ECX<kill>
	jns	.LBB0_27
# BB#26:
	movl	8(%rsp), %edx           # 4-byte Reload
	movzwl	%dx, %ecx
	negl	%edx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	cmovnew	%dx, %cx
.LBB0_27:
	movl	%r15d, 28(%rsp)         # 4-byte Spill
	movl	%r11d, (%rsp)           # 4-byte Spill
	movl	%r10d, 112(%rsp)        # 4-byte Spill
	movl	%r9d, %r15d
	movq	%r8, 152(%rsp)          # 8-byte Spill
	cmpw	%ax, %cx
	cmovgew	%cx, %ax
	movl	4(%rsp), %ecx           # 4-byte Reload
	testw	%cx, %cx
                                        # kill: %CX<def> %CX<kill> %ECX<kill>
	jns	.LBB0_29
# BB#28:
	movl	4(%rsp), %edx           # 4-byte Reload
	movzwl	%dx, %ecx
	negl	%edx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	cmovnew	%dx, %cx
.LBB0_29:
	cmpw	%ax, %cx
	cmovgew	%cx, %ax
	movswl	%ax, %edi
	movl	%edi, %ecx
	shll	$7, %ecx
	andl	$-65536, %ecx           # imm = 0xFFFF0000
	movl	%ecx, %edx
	sarl	$17, %edx
	xorl	%eax, %eax
	cmpl	$65535, %ecx            # imm = 0xFFFF
	setg	%al
	testw	%dx, %dx
	setg	%dl
	movl	%ecx, %esi
	sarl	$18, %esi
	andb	%al, %dl
	movzbl	%dl, %edx
	addl	%edx, %eax
	testw	%si, %si
	setg	%bl
	movl	%ecx, %esi
	sarl	$19, %esi
	andb	%bl, %dl
	movzbl	%dl, %edx
	addl	%edx, %eax
	testw	%si, %si
	setg	%bl
	movl	%ecx, %esi
	sarl	$20, %esi
	andb	%bl, %dl
	movzbl	%dl, %edx
	addl	%edx, %eax
	testw	%si, %si
	setg	%bl
	sarl	$21, %ecx
	andb	%bl, %dl
	movzbl	%dl, %edx
	addl	%edx, %eax
	testw	%cx, %cx
	setg	%cl
	andb	%cl, %dl
	movzbl	%dl, %ecx
	leal	(%rax,%rcx), %edx
	leal	5(%rcx,%rax), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edi
	shll	$3, %edx
	movswl	%dx, %esi
	callq	gsm_add
	movswl	%ax, %edx
	cmpl	$15, %edx
	jle	.LBB0_49
# BB#30:
	shrl	$3, %edx
	addl	$65535, %edx            # imm = 0xFFFF
	jmp	.LBB0_31
.LBB0_49:
	xorl	%edx, %edx
.LBB0_31:
	movl	%r13d, %ebp
	movl	(%rsp), %r13d           # 4-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	leal	(,%rdx,8), %edi
	movw	%ax, (%rsp)             # 2-byte Spill
	movw	%ax, %cx
	subw	%di, %cx
	movswl	%r15w, %r10d
	movswl	112(%rsp), %eax         # 2-byte Folded Reload
	movswl	%r13w, %r9d
	movswl	%bp, %r13d
	movswl	%r14w, %r14d
	movswl	132(%rsp), %ebx         # 2-byte Folded Reload
	movswl	128(%rsp), %r8d         # 2-byte Folded Reload
	movswl	%si, %r11d
	movswl	124(%rsp), %r15d        # 2-byte Folded Reload
	movswl	16(%rsp), %esi          # 2-byte Folded Reload
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movswl	8(%rsp), %esi           # 2-byte Folded Reload
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movswl	4(%rsp), %esi           # 2-byte Folded Reload
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%r12, 136(%rsp)         # 8-byte Spill
	je	.LBB0_32
# BB#33:                                # %.preheader.i.i
	movswl	%cx, %edi
	cmpl	$7, %edi
	jg	.LBB0_35
	.p2align	4, 0x90
.LBB0_34:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rcx,%rcx), %ecx
	decl	%edx
	movswl	%cx, %edi
	cmpl	$8, %edi
	jl	.LBB0_34
.LBB0_35:                               # %._crit_edge.i.i
	addl	$-8, %ecx
	movswq	%cx, %rdi
	jmp	.LBB0_36
.LBB0_32:
	movw	$-4, %dx
	movl	$7, %edi
.LBB0_36:                               # %APCM_quantization.exit
	movl	$6, %ecx
	subl	%edx, %ecx
	movq	152(%rsp), %rsi         # 8-byte Reload
	shll	%cl, %esi
	movswq	%si, %rsi
	movq	%rdi, %rbp
	movswl	gsm_NRFAC(%rbp,%rbp), %edi
	imull	%edi, %esi
	addl	%esi, %esi
	sarl	$28, %esi
	addl	$4, %esi
	movq	168(%rsp), %r12         # 8-byte Reload
	movw	%si, (%r12)
	shll	%cl, %r10d
	movswq	%r10w, %rsi
	imull	%edi, %esi
	addl	%esi, %esi
	sarl	$28, %esi
	addl	$4, %esi
	movw	%si, 2(%r12)
	shll	%cl, %eax
	movswq	%ax, %rax
	imull	%edi, %eax
	addl	%eax, %eax
	sarl	$28, %eax
	addl	$4, %eax
	movw	%ax, 4(%r12)
	shll	%cl, %r9d
	movswq	%r9w, %rax
	imull	%edi, %eax
	addl	%eax, %eax
	sarl	$28, %eax
	addl	$4, %eax
	movw	%ax, 6(%r12)
	shll	%cl, %r13d
	movswq	%r13w, %rax
	imull	%edi, %eax
	addl	%eax, %eax
	sarl	$28, %eax
	addl	$4, %eax
	movw	%ax, 8(%r12)
	shll	%cl, %r14d
	movswq	%r14w, %rax
	imull	%edi, %eax
	addl	%eax, %eax
	sarl	$28, %eax
	addl	$4, %eax
	movw	%ax, 10(%r12)
	shll	%cl, %ebx
	movswq	%bx, %rax
	imull	%edi, %eax
	addl	%eax, %eax
	sarl	$28, %eax
	addl	$4, %eax
	movw	%ax, 12(%r12)
	shll	%cl, %r8d
	movswq	%r8w, %rax
	imull	%edi, %eax
	addl	%eax, %eax
	sarl	$28, %eax
	addl	$4, %eax
	movw	%ax, 14(%r12)
	shll	%cl, %r11d
	movswq	%r11w, %rax
	imull	%edi, %eax
	addl	%eax, %eax
	sarl	$28, %eax
	addl	$4, %eax
	movw	%ax, 16(%r12)
	shll	%cl, %r15d
	movswq	%r15w, %rax
	imull	%edi, %eax
	addl	%eax, %eax
	sarl	$28, %eax
	addl	$4, %eax
	movw	%ax, 18(%r12)
	movq	112(%rsp), %rax         # 8-byte Reload
	shll	%cl, %eax
	movswq	%ax, %rax
	imull	%edi, %eax
	addl	%eax, %eax
	sarl	$28, %eax
	addl	$4, %eax
	movw	%ax, 20(%r12)
	movq	8(%rsp), %rax           # 8-byte Reload
	shll	%cl, %eax
	movswq	%ax, %rax
	imull	%edi, %eax
	addl	%eax, %eax
	sarl	$28, %eax
	addl	$4, %eax
	movw	%ax, 22(%r12)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	movq	16(%rsp), %rax          # 8-byte Reload
	shll	%cl, %eax
	movswq	%ax, %rax
	imull	%edi, %eax
	addl	%eax, %eax
	sarl	$28, %eax
	addl	$4, %eax
	movw	%ax, 24(%r12)
	movq	160(%rsp), %rax         # 8-byte Reload
	movzwl	(%rsp), %ecx            # 2-byte Folded Reload
	movw	%cx, (%rax)
	movswq	gsm_FAC(%rbp,%rbp), %r14
	movswl	%dx, %esi
	movl	$6, %edi
	callq	gsm_sub
	movswl	%ax, %r13d
	movl	$1, %esi
	movl	%r13d, %edi
	callq	gsm_sub
	movswl	%ax, %esi
	movl	$1, %edi
	callq	gsm_asl
	shlq	$33, %r14
	movswq	%ax, %rbx
	xorl	%ebp, %ebp
	movabsq	$140737488355328, %r15  # imm = 0x800000000000
	.p2align	4, 0x90
.LBB0_37:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%r12,%rbp,2), %eax
	shll	$13, %eax
	addl	$-28672, %eax           # imm = 0x9000
	movswq	%ax, %rax
	imulq	%r14, %rax
	addq	%r15, %rax
	sarq	$48, %rax
	leaq	32768(%rax,%rbx), %rcx
	addq	%rbx, %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmoval	%edx, %eax
	movswl	%ax, %edi
	movl	%r13d, %esi
	callq	gsm_asr
	movw	%ax, 176(%rsp,%rbp,2)
	incq	%rbp
	cmpl	$13, %ebp
	jne	.LBB0_37
# BB#38:                                # %APCM_inverse_quantization.exit
	movq	144(%rsp), %rax         # 8-byte Reload
	movswl	(%rax), %eax
	cmpl	$3, %eax
	ja	.LBB0_39
# BB#43:                                # %APCM_inverse_quantization.exit
	leaq	176(%rsp), %rcx
	movl	$13, %edx
	movq	136(%rsp), %rdi         # 8-byte Reload
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_44:                               # %.thread.i
	leaq	178(%rsp), %rcx
	movzwl	176(%rsp), %edx
	movw	%dx, (%rdi)
	addq	$2, %rdi
	movl	$12, %edx
	jmp	.LBB0_46
.LBB0_39:
	movq	136(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB0_40
.LBB0_45:
	movw	$0, (%rdi)
	addq	$2, %rdi
	jmp	.LBB0_46
.LBB0_40:                               # %.preheader.i
	leal	1(%rax), %ecx
	movswl	%cx, %ecx
	cmpl	$3, %ecx
	jg	.LBB0_42
# BB#41:                                # %.lr.ph.preheader.i
	movl	$2, %ecx
	subl	%eax, %ecx
	movzwl	%cx, %eax
	leaq	2(%rax,%rax), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB0_42:                               # %RPE_grid_positioning.exit
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_46:
	movw	$0, (%rdi)
	addq	$2, %rdi
.LBB0_47:
	movw	$0, (%rdi)
	movzwl	(%rcx), %esi
	movw	%si, 2(%rdi)
	addq	$4, %rdi
	decl	%edx
	je	.LBB0_40
# BB#48:
	addq	$2, %rcx
	jmp	.LBB0_46
.Lfunc_end0:
	.size	Gsm_RPE_Encoding, .Lfunc_end0-Gsm_RPE_Encoding
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_44
	.quad	.LBB0_47
	.quad	.LBB0_46
	.quad	.LBB0_45

	.text
	.globl	Gsm_RPE_Decoding
	.p2align	4, 0x90
	.type	Gsm_RPE_Decoding,@function
Gsm_RPE_Decoding:                       # @Gsm_RPE_Decoding
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 112
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 8(%rsp)           # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	cmpl	$16, %esi
	jl	.LBB1_1
# BB#2:
	movswl	%si, %eax
	shrl	$3, %eax
	addl	$65535, %eax            # imm = 0xFFFF
	jmp	.LBB1_3
.LBB1_1:
	xorl	%eax, %eax
.LBB1_3:
	leal	(,%rax,8), %ecx
	subw	%cx, %si
	movq	%r8, (%rsp)             # 8-byte Spill
	je	.LBB1_4
# BB#5:                                 # %.preheader.i
	movswl	%si, %ecx
	cmpl	$7, %ecx
	jg	.LBB1_7
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rsi,%rsi), %esi
	decl	%eax
	movswl	%si, %ecx
	cmpl	$8, %ecx
	jl	.LBB1_6
.LBB1_7:                                # %._crit_edge.i
	addl	$-8, %esi
	movswq	%si, %rcx
	jmp	.LBB1_8
.LBB1_4:
	movw	$-4, %ax
	movl	$7, %ecx
.LBB1_8:                                # %APCM_quantization_xmaxc_to_exp_mant.exit
	movswq	gsm_FAC(%rcx,%rcx), %r13
	movswl	%ax, %esi
	movl	$6, %edi
	callq	gsm_sub
	movswl	%ax, %ebp
	movl	$1, %esi
	movl	%ebp, %edi
	callq	gsm_sub
	movswl	%ax, %esi
	movl	$1, %edi
	callq	gsm_asl
	shlq	$33, %r13
	movswq	%ax, %rbx
	xorl	%r14d, %r14d
	movabsq	$140737488355328, %r15  # imm = 0x800000000000
	.p2align	4, 0x90
.LBB1_9:                                # =>This Inner Loop Header: Depth=1
	movzwl	(%r12,%r14,2), %eax
	shll	$13, %eax
	addl	$-28672, %eax           # imm = 0x9000
	movswq	%ax, %rax
	imulq	%r13, %rax
	addq	%r15, %rax
	sarq	$48, %rax
	leaq	32768(%rax,%rbx), %rcx
	addq	%rbx, %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmoval	%edx, %eax
	movswl	%ax, %edi
	movl	%ebp, %esi
	callq	gsm_asr
	movw	%ax, 16(%rsp,%r14,2)
	incq	%r14
	cmpl	$13, %r14d
	jne	.LBB1_9
# BB#10:                                # %APCM_inverse_quantization.exit
	movq	8(%rsp), %rsi           # 8-byte Reload
	movswl	%si, %edx
	cmpl	$3, %edx
	ja	.LBB1_11
# BB#15:                                # %APCM_inverse_quantization.exit
	leaq	16(%rsp), %rax
	movl	$13, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	jmpq	*.LJTI1_0(,%rdx,8)
.LBB1_16:                               # %.thread.i
	leaq	18(%rsp), %rax
	movzwl	16(%rsp), %ecx
	movw	%cx, (%rdi)
	addq	$2, %rdi
	movl	$12, %ecx
	jmp	.LBB1_18
.LBB1_11:
	movq	(%rsp), %rdi            # 8-byte Reload
	jmp	.LBB1_12
.LBB1_17:
	movw	$0, (%rdi)
	addq	$2, %rdi
	jmp	.LBB1_18
.LBB1_12:                               # %.preheader.i4
	leal	1(%rsi), %eax
	cwtl
	cmpl	$3, %eax
	jg	.LBB1_14
# BB#13:                                # %.lr.ph.preheader.i
	movl	$2, %eax
	subl	%esi, %eax
	movzwl	%ax, %eax
	leaq	2(%rax,%rax), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB1_14:                               # %RPE_grid_positioning.exit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_18:
	movw	$0, (%rdi)
	addq	$2, %rdi
.LBB1_19:
	movw	$0, (%rdi)
	movzwl	(%rax), %edx
	movw	%dx, 2(%rdi)
	addq	$4, %rdi
	decl	%ecx
	je	.LBB1_12
# BB#20:
	addq	$2, %rax
	jmp	.LBB1_18
.Lfunc_end1:
	.size	Gsm_RPE_Decoding, .Lfunc_end1-Gsm_RPE_Decoding
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_16
	.quad	.LBB1_19
	.quad	.LBB1_18
	.quad	.LBB1_17


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
