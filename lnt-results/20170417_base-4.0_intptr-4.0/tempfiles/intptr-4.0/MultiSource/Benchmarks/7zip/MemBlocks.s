	.text
	.file	"MemBlocks.bc"
	.globl	_ZN16CMemBlockManager13AllocateSpaceEm
	.p2align	4, 0x90
	.type	_ZN16CMemBlockManager13AllocateSpaceEm,@function
_ZN16CMemBlockManager13AllocateSpaceEm: # @_ZN16CMemBlockManager13AllocateSpaceEm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	(%r14), %rdi
	callq	MidFree
	movq	$0, (%r14)
	movq	$0, 16(%r14)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.LBB0_17
# BB#1:
	movq	8(%r14), %rsi
	cmpq	$8, %rsi
	jb	.LBB0_17
# BB#2:
	movq	%rsi, %rcx
	imulq	%r13, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%r13, %rax
	jne	.LBB0_10
# BB#3:
	movq	%rcx, %rdi
	callq	MidAlloc
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.LBB0_10
# BB#4:                                 # %.preheader
	cmpq	$2, %r13
	jb	.LBB0_16
# BB#5:                                 # %.lr.ph
	movq	8(%r14), %rdx
	leaq	-1(%r13), %r8
	leaq	-2(%r13), %r9
	movq	%r8, %r10
	andq	$7, %r10
	je	.LBB0_11
# BB#6:                                 # %.prol.preheader
	xorl	%edi, %edi
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rcx
	leaq	(%rcx,%rdx), %rbx
	movq	%rbx, (%rcx)
	incq	%rdi
	cmpq	%rdi, %r10
	jne	.LBB0_7
# BB#8:                                 # %.prol.loopexit.unr-lcssa
	incq	%rdi
	jmp	.LBB0_12
.LBB0_10:
	xorl	%eax, %eax
	jmp	.LBB0_17
.LBB0_11:
	movl	$1, %edi
	movq	%rax, %rbx
.LBB0_12:                               # %.prol.loopexit
	imulq	%rdx, %r8
	cmpq	$7, %r9
	jb	.LBB0_15
# BB#13:                                # %.lr.ph.new
	subq	%rdi, %r13
	leaq	(%rdx,%rdx), %r10
	leaq	(%rdx,%rdx,2), %r11
	leaq	(,%rdx,4), %r9
	leaq	(%rdx,%rdx,4), %r12
	leaq	(%r10,%r10,2), %rcx
	leaq	(,%rdx,8), %r15
	movq	%r15, %rdi
	subq	%rdx, %rdi
	.p2align	4, 0x90
.LBB0_14:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rbx,%rdx), %rsi
	movq	%rsi, (%rbx)
	addq	%rdx, %rsi
	leaq	(%rbx,%r10), %rbp
	movq	%rbp, (%rbx,%rdx)
	addq	%rdx, %rsi
	leaq	(%rbx,%r11), %rbp
	movq	%rbp, (%rbx,%rdx,2)
	addq	%rdx, %rsi
	leaq	(%rbx,%r9), %rbp
	movq	%rbp, (%rbx,%r11)
	addq	%rdx, %rsi
	leaq	(%rbx,%r12), %rbp
	movq	%rbp, (%rbx,%rdx,4)
	addq	%rdx, %rsi
	leaq	(%rbx,%rcx), %rbp
	movq	%rbp, (%rbx,%r12)
	addq	%rdx, %rsi
	leaq	(%rbx,%rdi), %rbp
	movq	%rbp, (%rbx,%rcx)
	addq	%rdx, %rsi
	leaq	(%rbx,%r15), %rbp
	movq	%rbp, (%rbx,%rdi)
	addq	$-8, %r13
	movq	%rsi, %rbx
	jne	.LBB0_14
.LBB0_15:                               # %._crit_edge.loopexit
	addq	%r8, %rax
.LBB0_16:                               # %._crit_edge
	movq	$0, (%rax)
	movq	(%r14), %rax
	movq	%rax, 16(%r14)
	movb	$1, %al
.LBB0_17:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN16CMemBlockManager13AllocateSpaceEm, .Lfunc_end0-_ZN16CMemBlockManager13AllocateSpaceEm
	.cfi_endproc

	.globl	_ZN16CMemBlockManager9FreeSpaceEv
	.p2align	4, 0x90
	.type	_ZN16CMemBlockManager9FreeSpaceEv,@function
_ZN16CMemBlockManager9FreeSpaceEv:      # @_ZN16CMemBlockManager9FreeSpaceEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	callq	MidFree
	movq	$0, (%rbx)
	movq	$0, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN16CMemBlockManager9FreeSpaceEv, .Lfunc_end1-_ZN16CMemBlockManager9FreeSpaceEv
	.cfi_endproc

	.globl	_ZN16CMemBlockManager13AllocateBlockEv
	.p2align	4, 0x90
	.type	_ZN16CMemBlockManager13AllocateBlockEv,@function
_ZN16CMemBlockManager13AllocateBlockEv: # @_ZN16CMemBlockManager13AllocateBlockEv
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB2_1
# BB#2:
	movq	(%rax), %rcx
	movq	%rcx, 16(%rdi)
	retq
.LBB2_1:
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	_ZN16CMemBlockManager13AllocateBlockEv, .Lfunc_end2-_ZN16CMemBlockManager13AllocateBlockEv
	.cfi_endproc

	.globl	_ZN16CMemBlockManager9FreeBlockEPv
	.p2align	4, 0x90
	.type	_ZN16CMemBlockManager9FreeBlockEPv,@function
_ZN16CMemBlockManager9FreeBlockEPv:     # @_ZN16CMemBlockManager9FreeBlockEPv
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB3_2
# BB#1:
	movq	16(%rdi), %rax
	movq	%rax, (%rsi)
	movq	%rsi, 16(%rdi)
.LBB3_2:
	retq
.Lfunc_end3:
	.size	_ZN16CMemBlockManager9FreeBlockEPv, .Lfunc_end3-_ZN16CMemBlockManager9FreeBlockEPv
	.cfi_endproc

	.globl	_ZN18CMemBlockManagerMt13AllocateSpaceEPN8NWindows16NSynchronization8CSynchroEmm
	.p2align	4, 0x90
	.type	_ZN18CMemBlockManagerMt13AllocateSpaceEPN8NWindows16NSynchronization8CSynchroEmm,@function
_ZN18CMemBlockManagerMt13AllocateSpaceEPN8NWindows16NSynchronization8CSynchroEmm: # @_ZN18CMemBlockManagerMt13AllocateSpaceEPN8NWindows16NSynchronization8CSynchroEmm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 96
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$-2147024809, %ebp      # imm = 0x80070057
	cmpq	%r12, %rbx
	jb	.LBB4_18
# BB#1:
	movq	(%r14), %rdi
	callq	MidFree
	movq	$0, (%r14)
	movq	$0, 16(%r14)
	testq	%rbx, %rbx
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	je	.LBB4_18
# BB#2:
	movq	8(%r14), %rsi
	cmpq	$8, %rsi
	jb	.LBB4_18
# BB#3:
	movq	%rsi, %rcx
	imulq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rbx, %rax
	jne	.LBB4_18
# BB#4:
	movq	%rcx, %rdi
	callq	MidAlloc
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.LBB4_18
# BB#5:                                 # %.preheader.i
	cmpq	$2, %rbx
	jb	.LBB4_15
# BB#6:                                 # %.lr.ph.i
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	8(%r14), %rcx
	leaq	-1(%rbx), %rdi
	leaq	-2(%rbx), %r8
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	andq	$7, %rdi
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB4_7
# BB#8:                                 # %.prol.preheader
	xorl	%ebp, %ebp
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB4_9:                                # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdx
	leaq	(%rdx,%rcx), %r15
	movq	%r15, (%rdx)
	incq	%rbp
	cmpq	%rbp, %rdi
	jne	.LBB4_9
# BB#10:                                # %.prol.loopexit.unr-lcssa
	incq	%rbp
	cmpq	$7, %r8
	jae	.LBB4_12
	jmp	.LBB4_14
.LBB4_7:
	movl	$1, %ebp
	movq	%rax, %r15
	cmpq	$7, %r8
	jb	.LBB4_14
.LBB4_12:                               # %.lr.ph.i.new
	movq	%rbx, %rdi
	subq	%rbp, %rdi
	leaq	(%rcx,%rcx), %r10
	leaq	(%rcx,%rcx,2), %r11
	leaq	(,%rcx,4), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rcx,4), %rbp
	leaq	(%r10,%r10,2), %rdx
	leaq	(,%rcx,8), %r8
	movq	%r8, %r13
	subq	%rcx, %r13
	.p2align	4, 0x90
.LBB4_13:                               # =>This Inner Loop Header: Depth=1
	leaq	(%r15,%rcx), %rsi
	movq	%rsi, (%r15)
	addq	%rcx, %rsi
	leaq	(%r15,%r10), %r9
	movq	%r9, (%r15,%rcx)
	addq	%rcx, %rsi
	leaq	(%r15,%r11), %rax
	movq	%rax, (%r15,%rcx,2)
	addq	%rcx, %rsi
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%r15,%rax), %rax
	movq	%rax, (%r15,%r11)
	addq	%rcx, %rsi
	leaq	(%r15,%rbp), %rax
	movq	%rax, (%r15,%rcx,4)
	addq	%rcx, %rsi
	leaq	(%r15,%rdx), %rax
	movq	%rax, (%r15,%rbp)
	addq	%rcx, %rsi
	leaq	(%r15,%r13), %rax
	movq	%rax, (%r15,%rdx)
	addq	%rcx, %rsi
	leaq	(%r15,%r8), %rax
	movq	%rax, (%r15,%r13)
	addq	$-8, %rdi
	movq	%rsi, %r15
	jne	.LBB4_13
.LBB4_14:                               # %._crit_edge.loopexit.i
	imulq	8(%rsp), %rcx           # 8-byte Folded Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	addq	%rcx, %rax
	movq	24(%rsp), %r15          # 8-byte Reload
.LBB4_15:
	movq	$0, (%rax)
	movq	(%r14), %rax
	movq	%rax, 16(%r14)
	subl	%r12d, %ebx
	movq	$0, 72(%r14)
	testl	%ebx, %ebx
	jle	.LBB4_16
# BB#17:
	movq	%r15, 72(%r14)
	movl	%ebx, 80(%r14)
	movl	%ebx, 84(%r14)
	xorl	%ebp, %ebp
	jmp	.LBB4_18
.LBB4_16:
	movl	$1, %ebp
.LBB4_18:                               # %_ZN8NWindows16NSynchronization14CSemaphoreWFMO6CreateEPNS0_8CSynchroEii.exit
	movl	%ebp, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN18CMemBlockManagerMt13AllocateSpaceEPN8NWindows16NSynchronization8CSynchroEmm, .Lfunc_end4-_ZN18CMemBlockManagerMt13AllocateSpaceEPN8NWindows16NSynchronization8CSynchroEmm
	.cfi_endproc

	.globl	_ZN18CMemBlockManagerMt19AllocateSpaceAlwaysEPN8NWindows16NSynchronization8CSynchroEmm
	.p2align	4, 0x90
	.type	_ZN18CMemBlockManagerMt19AllocateSpaceAlwaysEPN8NWindows16NSynchronization8CSynchroEmm,@function
_ZN18CMemBlockManagerMt19AllocateSpaceAlwaysEPN8NWindows16NSynchronization8CSynchroEmm: # @_ZN18CMemBlockManagerMt19AllocateSpaceAlwaysEPN8NWindows16NSynchronization8CSynchroEmm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 80
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpq	%rbx, %r12
	ja	.LBB5_21
# BB#1:                                 # %.preheader
	movq	%rsi, (%rsp)            # 8-byte Spill
	cmpq	%r12, %rbx
	jb	.LBB5_19
	jmp	.LBB5_3
	.p2align	4, 0x90
.LBB5_22:                               #   in Loop: Header=BB5_19 Depth=1
	shrq	%rbx
	addq	%r12, %rbx
	cmpq	%r12, %rbx
	jb	.LBB5_19
.LBB5_3:
	movq	(%r14), %rdi
	callq	MidFree
	movq	$0, (%r14)
	movq	$0, 16(%r14)
	testq	%rbx, %rbx
	je	.LBB5_19
# BB#4:
	movq	8(%r14), %rsi
	cmpq	$8, %rsi
	jb	.LBB5_19
# BB#5:
	movq	%rsi, %rcx
	imulq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rbx, %rax
	jne	.LBB5_19
# BB#6:
	movq	%rcx, %rdi
	callq	MidAlloc
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.LBB5_19
# BB#7:                                 # %.preheader.i.i
	cmpq	$2, %rbx
	jb	.LBB5_17
# BB#8:                                 # %.lr.ph.i.i
	movq	8(%r14), %rcx
	leal	7(%rbx), %ebp
	leaq	-2(%rbx), %r8
	andq	$7, %rbp
	movq	%rax, 8(%rsp)           # 8-byte Spill
	je	.LBB5_9
# BB#10:                                # %.prol.preheader
	xorl	%edi, %edi
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB5_11:                               # =>This Inner Loop Header: Depth=1
	movq	%r15, %rsi
	leaq	(%rsi,%rcx), %r15
	movq	%r15, (%rsi)
	incq	%rdi
	cmpq	%rdi, %rbp
	jne	.LBB5_11
# BB#12:                                # %.prol.loopexit.unr-lcssa
	incq	%rdi
	cmpq	$7, %r8
	jae	.LBB5_14
	jmp	.LBB5_16
.LBB5_9:
	movl	$1, %edi
	movq	%rax, %r15
	cmpq	$7, %r8
	jb	.LBB5_16
.LBB5_14:                               # %.lr.ph.i.i.new
	movq	%rbx, %rsi
	subq	%rdi, %rsi
	leaq	(%rcx,%rcx), %r8
	leaq	(%rcx,%rcx,2), %r10
	leaq	(,%rcx,4), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rcx,4), %r11
	leaq	(%r8,%r8,2), %rdi
	leaq	(,%rcx,8), %r13
	movq	%r13, %rbp
	subq	%rcx, %rbp
	.p2align	4, 0x90
.LBB5_15:                               # =>This Inner Loop Header: Depth=1
	leaq	(%r15,%rcx), %rdx
	movq	%rdx, (%r15)
	addq	%rcx, %rdx
	leaq	(%r15,%r8), %r9
	movq	%r9, (%r15,%rcx)
	addq	%rcx, %rdx
	leaq	(%r15,%r10), %rax
	movq	%rax, (%r15,%rcx,2)
	addq	%rcx, %rdx
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%r15,%rax), %rax
	movq	%rax, (%r15,%r10)
	addq	%rcx, %rdx
	leaq	(%r15,%r11), %rax
	movq	%rax, (%r15,%rcx,4)
	addq	%rcx, %rdx
	leaq	(%r15,%rdi), %rax
	movq	%rax, (%r15,%r11)
	addq	%rcx, %rdx
	leaq	(%r15,%rbp), %rax
	movq	%rax, (%r15,%rdi)
	addq	%rcx, %rdx
	leaq	(%r15,%r13), %rax
	movq	%rax, (%r15,%rbp)
	addq	$-8, %rsi
	movq	%rdx, %r15
	jne	.LBB5_15
.LBB5_16:                               # %._crit_edge.loopexit.i.i
	leaq	-1(%rbx), %rdx
	imulq	%rdx, %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	%rcx, %rax
.LBB5_17:
	movq	$0, (%rax)
	movq	(%r14), %rax
	movq	%rax, 16(%r14)
	movl	%ebx, %eax
	subl	%r12d, %eax
	movq	$0, 72(%r14)
	testl	%eax, %eax
	jg	.LBB5_18
	.p2align	4, 0x90
.LBB5_19:                               # =>This Inner Loop Header: Depth=1
	subq	%r12, %rbx
	jne	.LBB5_22
# BB#20:
	movl	$-2147024882, %eax      # imm = 0x8007000E
.LBB5_21:                               # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_18:                               # %_ZN18CMemBlockManagerMt13AllocateSpaceEPN8NWindows16NSynchronization8CSynchroEmm.exit
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, 72(%r14)
	movl	%eax, 80(%r14)
	movl	%eax, 84(%r14)
	xorl	%eax, %eax
	jmp	.LBB5_21
.Lfunc_end5:
	.size	_ZN18CMemBlockManagerMt19AllocateSpaceAlwaysEPN8NWindows16NSynchronization8CSynchroEmm, .Lfunc_end5-_ZN18CMemBlockManagerMt19AllocateSpaceAlwaysEPN8NWindows16NSynchronization8CSynchroEmm
	.cfi_endproc

	.globl	_ZN18CMemBlockManagerMt9FreeSpaceEv
	.p2align	4, 0x90
	.type	_ZN18CMemBlockManagerMt9FreeSpaceEv,@function
_ZN18CMemBlockManagerMt9FreeSpaceEv:    # @_ZN18CMemBlockManagerMt9FreeSpaceEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 16
.Lcfi42:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$0, 72(%rbx)
	movq	(%rbx), %rdi
	callq	MidFree
	movq	$0, (%rbx)
	movq	$0, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end6:
	.size	_ZN18CMemBlockManagerMt9FreeSpaceEv, .Lfunc_end6-_ZN18CMemBlockManagerMt9FreeSpaceEv
	.cfi_endproc

	.globl	_ZN18CMemBlockManagerMt13AllocateBlockEv
	.p2align	4, 0x90
	.type	_ZN18CMemBlockManagerMt13AllocateBlockEv,@function
_ZN18CMemBlockManagerMt13AllocateBlockEv: # @_ZN18CMemBlockManagerMt13AllocateBlockEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 32
.Lcfi46:
	.cfi_offset %rbx, -32
.Lcfi47:
	.cfi_offset %r14, -24
.Lcfi48:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	leaq	24(%rbx), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	movq	16(%rbx), %r15
	testq	%r15, %r15
	je	.LBB7_1
# BB#2:
	movq	(%r15), %rax
	movq	%rax, 16(%rbx)
	jmp	.LBB7_3
.LBB7_1:
	xorl	%r15d, %r15d
.LBB7_3:                                # %_ZN16CMemBlockManager13AllocateBlockEv.exit
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	movq	%r15, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	_ZN18CMemBlockManagerMt13AllocateBlockEv, .Lfunc_end7-_ZN18CMemBlockManagerMt13AllocateBlockEv
	.cfi_endproc

	.globl	_ZN18CMemBlockManagerMt9FreeBlockEPvb
	.p2align	4, 0x90
	.type	_ZN18CMemBlockManagerMt9FreeBlockEPvb,@function
_ZN18CMemBlockManagerMt9FreeBlockEPvb:  # @_ZN18CMemBlockManagerMt9FreeBlockEPvb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 48
.Lcfi54:
	.cfi_offset %rbx, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	testq	%rbp, %rbp
	je	.LBB8_6
# BB#1:                                 # %_ZN16CMemBlockManager9FreeBlockEPv.exit
	leaq	24(%rbx), %r15
	movq	%r15, %rdi
	callq	pthread_mutex_lock
	movq	16(%rbx), %rax
	movq	%rax, (%rbp)
	movq	%rbp, 16(%rbx)
	movq	%r15, %rdi
	callq	pthread_mutex_unlock
	testb	%r14b, %r14b
	je	.LBB8_6
# BB#2:
	movq	72(%rbx), %rdi
	callq	pthread_mutex_lock
	movl	80(%rbx), %eax
	cmpl	84(%rbx), %eax
	jge	.LBB8_3
# BB#5:
	incl	%eax
	movl	%eax, 80(%rbx)
	movq	72(%rbx), %rbx
	leaq	40(%rbx), %rdi
	callq	pthread_cond_broadcast
	movq	%rbx, %rdi
	jmp	.LBB8_4
.LBB8_6:                                # %_ZN8NWindows16NSynchronization14CSemaphoreWFMO7ReleaseEi.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_3:
	movq	72(%rbx), %rdi
.LBB8_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	pthread_mutex_unlock    # TAILCALL
.Lfunc_end8:
	.size	_ZN18CMemBlockManagerMt9FreeBlockEPvb, .Lfunc_end8-_ZN18CMemBlockManagerMt9FreeBlockEPvb
	.cfi_endproc

	.globl	_ZN10CMemBlocks4FreeEP18CMemBlockManagerMt
	.p2align	4, 0x90
	.type	_ZN10CMemBlocks4FreeEP18CMemBlockManagerMt,@function
_ZN10CMemBlocks4FreeEP18CMemBlockManagerMt: # @_ZN10CMemBlocks4FreeEP18CMemBlockManagerMt
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 48
.Lcfi63:
	.cfi_offset %rbx, -48
.Lcfi64:
	.cfi_offset %r12, -40
.Lcfi65:
	.cfi_offset %r13, -32
.Lcfi66:
	.cfi_offset %r14, -24
.Lcfi67:
	.cfi_offset %r15, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.LBB9_8
# BB#1:                                 # %.lr.ph
	leaq	24(%r13), %r15
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rcx
	cltq
	movq	-8(%rcx,%rax,8), %rbx
	testq	%rbx, %rbx
	je	.LBB9_7
# BB#3:                                 # %_ZN16CMemBlockManager9FreeBlockEPv.exit.i
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%r15, %rdi
	callq	pthread_mutex_lock
	movq	16(%r13), %rax
	movq	%rax, (%rbx)
	movq	%rbx, 16(%r13)
	movq	%r15, %rdi
	callq	pthread_mutex_unlock
	movq	72(%r13), %rdi
	callq	pthread_mutex_lock
	movl	80(%r13), %eax
	cmpl	84(%r13), %eax
	jge	.LBB9_4
# BB#5:                                 #   in Loop: Header=BB9_2 Depth=1
	incl	%eax
	movl	%eax, 80(%r13)
	movq	72(%r13), %r12
	leaq	40(%r12), %rdi
	callq	pthread_cond_broadcast
	movq	%r12, %rdi
	jmp	.LBB9_6
	.p2align	4, 0x90
.LBB9_4:                                #   in Loop: Header=BB9_2 Depth=1
	movq	72(%r13), %rdi
.LBB9_6:                                # %_ZN18CMemBlockManagerMt9FreeBlockEPvb.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	callq	pthread_mutex_unlock
.LBB9_7:                                # %_ZN18CMemBlockManagerMt9FreeBlockEPvb.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
	movl	12(%r14), %eax
	testl	%eax, %eax
	jg	.LBB9_2
.LBB9_8:                                # %._crit_edge
	movq	$0, 32(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	_ZN10CMemBlocks4FreeEP18CMemBlockManagerMt, .Lfunc_end9-_ZN10CMemBlocks4FreeEP18CMemBlockManagerMt
	.cfi_endproc

	.globl	_ZN10CMemBlocks7FreeOptEP18CMemBlockManagerMt
	.p2align	4, 0x90
	.type	_ZN10CMemBlocks7FreeOptEP18CMemBlockManagerMt,@function
_ZN10CMemBlocks7FreeOptEP18CMemBlockManagerMt: # @_ZN10CMemBlocks7FreeOptEP18CMemBlockManagerMt
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 16
.Lcfi69:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN10CMemBlocks4FreeEP18CMemBlockManagerMt
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZN17CBaseRecordVector12ClearAndFreeEv # TAILCALL
.Lfunc_end10:
	.size	_ZN10CMemBlocks7FreeOptEP18CMemBlockManagerMt, .Lfunc_end10-_ZN10CMemBlocks7FreeOptEP18CMemBlockManagerMt
	.cfi_endproc

	.globl	_ZNK10CMemBlocks13WriteToStreamEmP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZNK10CMemBlocks13WriteToStreamEmP20ISequentialOutStream,@function
_ZNK10CMemBlocks13WriteToStreamEmP20ISequentialOutStream: # @_ZNK10CMemBlocks13WriteToStreamEmP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi76:
	.cfi_def_cfa_offset 64
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	32(%r12), %r13
	testq	%r13, %r13
	je	.LBB11_6
# BB#1:                                 # %.lr.ph
	movl	%r15d, %ebp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	movslq	12(%r12), %rax
	cmpq	%rax, %r14
	jge	.LBB11_3
# BB#4:                                 #   in Loop: Header=BB11_2 Depth=1
	cmpq	%rbp, %r13
	movq	%r15, %rax
	cmovbq	%r13, %rax
	movq	16(%r12), %rcx
	movq	(%rcx,%r14,8), %rsi
	movl	%eax, %ebx
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbx, %rdx
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB11_7
# BB#5:                                 # %.thread
                                        #   in Loop: Header=BB11_2 Depth=1
	incq	%r14
	subq	%rbx, %r13
	jne	.LBB11_2
.LBB11_6:
	xorl	%eax, %eax
	jmp	.LBB11_7
.LBB11_3:
	movl	$-2147467259, %eax      # imm = 0x80004005
.LBB11_7:                               # %.thread37
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZNK10CMemBlocks13WriteToStreamEmP20ISequentialOutStream, .Lfunc_end11-_ZNK10CMemBlocks13WriteToStreamEmP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN14CMemLockBlocks9FreeBlockEiP18CMemBlockManagerMt
	.p2align	4, 0x90
	.type	_ZN14CMemLockBlocks9FreeBlockEiP18CMemBlockManagerMt,@function
_ZN14CMemLockBlocks9FreeBlockEiP18CMemBlockManagerMt: # @_ZN14CMemLockBlocks9FreeBlockEiP18CMemBlockManagerMt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi87:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 64
.Lcfi90:
	.cfi_offset %rbx, -56
.Lcfi91:
	.cfi_offset %r12, -48
.Lcfi92:
	.cfi_offset %r13, -40
.Lcfi93:
	.cfi_offset %r14, -32
.Lcfi94:
	.cfi_offset %r15, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rdi, %r14
	movq	16(%r14), %rax
	movslq	%esi, %r12
	movq	(%rax,%r12,8), %rbx
	testq	%rbx, %rbx
	je	.LBB12_6
# BB#1:                                 # %_ZN16CMemBlockManager9FreeBlockEPv.exit.i
	movb	40(%r14), %bpl
	leaq	24(%r13), %r15
	movq	%r15, %rdi
	callq	pthread_mutex_lock
	movq	16(%r13), %rax
	movq	%rax, (%rbx)
	movq	%rbx, 16(%r13)
	movq	%r15, %rdi
	callq	pthread_mutex_unlock
	cmpb	$0, %bpl
	je	.LBB12_6
# BB#2:
	movq	72(%r13), %rdi
	callq	pthread_mutex_lock
	movl	80(%r13), %eax
	cmpl	84(%r13), %eax
	jge	.LBB12_3
# BB#4:
	incl	%eax
	movl	%eax, 80(%r13)
	movq	72(%r13), %rbx
	leaq	40(%rbx), %rdi
	callq	pthread_cond_broadcast
	movq	%rbx, %rdi
	jmp	.LBB12_5
.LBB12_3:
	movq	72(%r13), %rdi
.LBB12_5:                               # %_ZN18CMemBlockManagerMt9FreeBlockEPvb.exit
	callq	pthread_mutex_unlock
.LBB12_6:                               # %_ZN18CMemBlockManagerMt9FreeBlockEPvb.exit
	movq	16(%r14), %rax
	movq	$0, (%rax,%r12,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN14CMemLockBlocks9FreeBlockEiP18CMemBlockManagerMt, .Lfunc_end12-_ZN14CMemLockBlocks9FreeBlockEiP18CMemBlockManagerMt
	.cfi_endproc

	.globl	_ZN14CMemLockBlocks4FreeEP18CMemBlockManagerMt
	.p2align	4, 0x90
	.type	_ZN14CMemLockBlocks4FreeEP18CMemBlockManagerMt,@function
_ZN14CMemLockBlocks4FreeEP18CMemBlockManagerMt: # @_ZN14CMemLockBlocks4FreeEP18CMemBlockManagerMt
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi98:
	.cfi_def_cfa_offset 32
.Lcfi99:
	.cfi_offset %rbx, -24
.Lcfi100:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	jmp	.LBB13_2
	.p2align	4, 0x90
.LBB13_1:                               #   in Loop: Header=BB13_2 Depth=1
	decl	%esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	_ZN14CMemLockBlocks9FreeBlockEiP18CMemBlockManagerMt
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	movl	12(%rbx), %esi
	testl	%esi, %esi
	jg	.LBB13_1
# BB#3:                                 # %._crit_edge
	movq	$0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end13:
	.size	_ZN14CMemLockBlocks4FreeEP18CMemBlockManagerMt, .Lfunc_end13-_ZN14CMemLockBlocks4FreeEP18CMemBlockManagerMt
	.cfi_endproc

	.globl	_ZN14CMemLockBlocks18SwitchToNoLockModeEP18CMemBlockManagerMt
	.p2align	4, 0x90
	.type	_ZN14CMemLockBlocks18SwitchToNoLockModeEP18CMemBlockManagerMt,@function
_ZN14CMemLockBlocks18SwitchToNoLockModeEP18CMemBlockManagerMt: # @_ZN14CMemLockBlocks18SwitchToNoLockModeEP18CMemBlockManagerMt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi105:
	.cfi_def_cfa_offset 48
.Lcfi106:
	.cfi_offset %rbx, -40
.Lcfi107:
	.cfi_offset %r14, -32
.Lcfi108:
	.cfi_offset %r15, -24
.Lcfi109:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	cmpb	$0, 40(%r15)
	je	.LBB14_5
# BB#1:
	movl	12(%r15), %ebx
	testl	%ebx, %ebx
	jle	.LBB14_4
# BB#2:
	movq	72(%rbp), %rdi
	callq	pthread_mutex_lock
	addl	80(%rbp), %ebx
	cmpl	84(%rbp), %ebx
	jle	.LBB14_3
# BB#6:                                 # %_ZN18CMemBlockManagerMt19ReleaseLockedBlocksEi.exit
	movq	72(%rbp), %rdi
	callq	pthread_mutex_unlock
	movl	$1, %r14d
	jmp	.LBB14_5
.LBB14_3:                               # %_ZN18CMemBlockManagerMt19ReleaseLockedBlocksEi.exit.thread
	movl	%ebx, 80(%rbp)
	movq	72(%rbp), %rbp
	leaq	40(%rbp), %rdi
	callq	pthread_cond_broadcast
	movq	%rbp, %rdi
	callq	pthread_mutex_unlock
.LBB14_4:
	movb	$0, 40(%r15)
.LBB14_5:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_ZN14CMemLockBlocks18SwitchToNoLockModeEP18CMemBlockManagerMt, .Lfunc_end14-_ZN14CMemLockBlocks18SwitchToNoLockModeEP18CMemBlockManagerMt
	.cfi_endproc

	.globl	_ZN14CMemLockBlocks6DetachERS_P18CMemBlockManagerMt
	.p2align	4, 0x90
	.type	_ZN14CMemLockBlocks6DetachERS_P18CMemBlockManagerMt,@function
_ZN14CMemLockBlocks6DetachERS_P18CMemBlockManagerMt: # @_ZN14CMemLockBlocks6DetachERS_P18CMemBlockManagerMt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi114:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi116:
	.cfi_def_cfa_offset 64
.Lcfi117:
	.cfi_offset %rbx, -56
.Lcfi118:
	.cfi_offset %r12, -48
.Lcfi119:
	.cfi_offset %r13, -40
.Lcfi120:
	.cfi_offset %r14, -32
.Lcfi121:
	.cfi_offset %r15, -24
.Lcfi122:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r15
	jmp	.LBB15_2
	.p2align	4, 0x90
.LBB15_1:                               #   in Loop: Header=BB15_2 Depth=1
	decl	%esi
	movq	%r12, %rdi
	movq	%r14, %rdx
	callq	_ZN14CMemLockBlocks9FreeBlockEiP18CMemBlockManagerMt
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.LBB15_2:                               # =>This Inner Loop Header: Depth=1
	movl	12(%r12), %esi
	testl	%esi, %esi
	jg	.LBB15_1
# BB#3:                                 # %_ZN14CMemLockBlocks4FreeEP18CMemBlockManagerMt.exit
	movq	$0, 32(%r12)
	movb	40(%r15), %al
	movb	%al, 40(%r12)
	cmpl	$0, 12(%r15)
	movq	32(%r15), %rax
	jle	.LBB15_4
# BB#8:                                 # %.lr.ph
	movq	8(%r14), %r13
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	movq	%r14, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB15_9:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %rbp
	jae	.LBB15_11
# BB#10:                                #   in Loop: Header=BB15_9 Depth=1
	movq	16(%r15), %rax
	movq	%r13, %r14
	movq	(%rax,%rbx,8), %r13
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	movq	%r14, %r13
	movq	(%rsp), %r14            # 8-byte Reload
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	jmp	.LBB15_12
	.p2align	4, 0x90
.LBB15_11:                              #   in Loop: Header=BB15_9 Depth=1
	movq	%r15, %rdi
	movl	%ebx, %esi
	movq	%r14, %rdx
	callq	_ZN14CMemLockBlocks9FreeBlockEiP18CMemBlockManagerMt
.LBB15_12:                              #   in Loop: Header=BB15_9 Depth=1
	movq	16(%r15), %rax
	movq	$0, (%rax,%rbx,8)
	addq	%r13, %rbp
	incq	%rbx
	movslq	12(%r15), %rsi
	movq	32(%r15), %rax
	cmpq	%rsi, %rbx
	jl	.LBB15_9
# BB#13:                                # %._crit_edge
	movq	%rax, 32(%r12)
	testl	%esi, %esi
	jle	.LBB15_7
	.p2align	4, 0x90
.LBB15_5:                               # %.lr.ph.i22
                                        # =>This Inner Loop Header: Depth=1
	decl	%esi
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r14, %rdx
	callq	_ZN14CMemLockBlocks9FreeBlockEiP18CMemBlockManagerMt
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
	movl	12(%r15), %esi
	testl	%esi, %esi
	jg	.LBB15_5
	jmp	.LBB15_7
.LBB15_4:                               # %._crit_edge.thread
	movq	%rax, 32(%r12)
.LBB15_7:                               # %_ZN14CMemLockBlocks4FreeEP18CMemBlockManagerMt.exit23
	movq	$0, 32(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	_ZN14CMemLockBlocks6DetachERS_P18CMemBlockManagerMt, .Lfunc_end15-_ZN14CMemLockBlocks6DetachERS_P18CMemBlockManagerMt
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
