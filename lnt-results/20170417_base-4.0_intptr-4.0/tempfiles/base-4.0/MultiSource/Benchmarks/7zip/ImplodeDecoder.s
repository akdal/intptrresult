	.text
	.file	"ImplodeDecoder.bc"
	.globl	_ZN9NCompress8NImplode8NDecoder6CCoderC2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NDecoder6CCoderC2Ev,@function
_ZN9NCompress8NImplode8NDecoder6CCoderC2Ev: # @_ZN9NCompress8NImplode8NDecoder6CCoderC2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$0, 16(%rbx)
	movl	$_ZTVN9NCompress8NImplode8NDecoder6CCoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NImplode8NDecoder6CCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$0, 24(%rbx)
	movl	$0, 32(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 64(%rbx)
	leaq	88(%rbx), %r15
.Ltmp0:
	movq	%r15, %rdi
	callq	_ZN9CInBufferC1Ev
.Ltmp1:
# BB#1:                                 # %_ZN5NBitl8CDecoderI9CInBufferEC2Ev.exit
	leaq	144(%rbx), %r12
.Ltmp3:
	movl	$256, %esi              # imm = 0x100
	movq	%r12, %rdi
	callq	_ZN9NCompress8NImplode8NHuffman8CDecoderC1Ej
.Ltmp4:
# BB#2:
	leaq	304(%rbx), %r13
.Ltmp6:
	movl	$64, %esi
	movq	%r13, %rdi
	callq	_ZN9NCompress8NImplode8NHuffman8CDecoderC1Ej
.Ltmp7:
# BB#3:
	leaq	464(%rbx), %rdi
.Ltmp9:
	movl	$64, %esi
	callq	_ZN9NCompress8NImplode8NHuffman8CDecoderC1Ej
.Ltmp10:
# BB#4:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB0_7:
.Ltmp11:
	movq	%rax, %r14
.Ltmp12:
	movq	%r13, %rdi
	callq	_ZN9NCompress8NImplode8NHuffman8CDecoderD1Ev
.Ltmp13:
	jmp	.LBB0_8
.LBB0_6:
.Ltmp8:
	movq	%rax, %r14
.LBB0_8:
.Ltmp14:
	movq	%r12, %rdi
	callq	_ZN9NCompress8NImplode8NHuffman8CDecoderD1Ev
.Ltmp15:
	jmp	.LBB0_9
.LBB0_5:
.Ltmp5:
	movq	%rax, %r14
.LBB0_9:
.Ltmp16:
	movq	%r15, %rdi
	callq	_ZN9CInBuffer4FreeEv
.Ltmp17:
# BB#10:
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_16
# BB#11:
	movq	(%rdi), %rax
.Ltmp22:
	callq	*16(%rax)
.Ltmp23:
	jmp	.LBB0_16
.LBB0_12:
.Ltmp18:
	movq	%rax, %r14
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_24
# BB#13:
	movq	(%rdi), %rax
.Ltmp19:
	callq	*16(%rax)
.Ltmp20:
	jmp	.LBB0_24
.LBB0_14:
.Ltmp21:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_15:
.Ltmp2:
	movq	%rax, %r14
.LBB0_16:                               # %_ZN5NBitl12CBaseDecoderI9CInBufferED2Ev.exit
	leaq	24(%rbx), %rdi
.Ltmp24:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp25:
# BB#17:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_19
# BB#18:
	movq	(%rdi), %rax
.Ltmp30:
	callq	*16(%rax)
.Ltmp31:
.LBB0_19:                               # %_ZN10COutBufferD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_20:
.Ltmp26:
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_24
# BB#21:
	movq	(%rdi), %rax
.Ltmp27:
	callq	*16(%rax)
.Ltmp28:
	jmp	.LBB0_24
.LBB0_22:
.Ltmp29:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_23:
.Ltmp32:
	movq	%rax, %r14
.LBB0_24:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN9NCompress8NImplode8NDecoder6CCoderC2Ev, .Lfunc_end0-_ZN9NCompress8NImplode8NDecoder6CCoderC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\245\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp15-.Ltmp12         #   Call between .Ltmp12 and .Ltmp15
	.long	.Ltmp32-.Lfunc_begin0   #     jumps to .Ltmp32
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin0   #     jumps to .Ltmp18
	.byte	1                       #   On action: 1
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp32-.Lfunc_begin0   #     jumps to .Ltmp32
	.byte	1                       #   On action: 1
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin0   #     jumps to .Ltmp21
	.byte	1                       #   On action: 1
	.long	.Ltmp24-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin0   #     jumps to .Ltmp26
	.byte	1                       #   On action: 1
	.long	.Ltmp30-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin0   #     jumps to .Ltmp32
	.byte	1                       #   On action: 1
	.long	.Ltmp31-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp27-.Ltmp31         #   Call between .Ltmp31 and .Ltmp27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin0   #     jumps to .Ltmp29
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN9NCompress8NImplode8NDecoder6CCoder14ReleaseStreamsEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NDecoder6CCoder14ReleaseStreamsEv,@function
_ZN9NCompress8NImplode8NDecoder6CCoder14ReleaseStreamsEv: # @_ZN9NCompress8NImplode8NDecoder6CCoder14ReleaseStreamsEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 48(%rbx)
.LBB2_2:                                # %_ZN10COutBuffer13ReleaseStreamEv.exit
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 112(%rbx)
.LBB2_4:                                # %_ZN5NBitl12CBaseDecoderI9CInBufferE13ReleaseStreamEv.exit
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZN9NCompress8NImplode8NDecoder6CCoder14ReleaseStreamsEv, .Lfunc_end2-_ZN9NCompress8NImplode8NDecoder6CCoder14ReleaseStreamsEv
	.cfi_endproc

	.globl	_ZN9NCompress8NImplode8NDecoder6CCoder14ReadLevelItemsERNS0_8NHuffman8CDecoderEPhi
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NDecoder6CCoder14ReadLevelItemsERNS0_8NHuffman8CDecoderEPhi,@function
_ZN9NCompress8NImplode8NDecoder6CCoder14ReadLevelItemsERNS0_8NHuffman8CDecoderEPhi: # @_ZN9NCompress8NImplode8NDecoder6CCoder14ReadLevelItemsERNS0_8NHuffman8CDecoderEPhi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 112
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movl	80(%rbx), %eax
	cmpl	$7, %eax
	jbe	.LBB3_1
# BB#2:                                 # %.lr.ph.i.i
	leaq	88(%rbx), %r13
	leaq	96(%rbx), %r14
	leaq	140(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	136(%rbx), %r12
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	(%r14), %rax
	jb	.LBB3_6
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	%r13, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB3_7
# BB#5:                                 # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	(%r13), %rax
.LBB3_6:                                # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i
                                        #   in Loop: Header=BB3_3 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r13)
	movzbl	(%rax), %eax
	jmp	.LBB3_8
	.p2align	4, 0x90
.LBB3_7:                                # %_ZN9CInBuffer8ReadByteERh.exit.i.i
                                        #   in Loop: Header=BB3_3 Depth=1
	incl	(%r12)
	movb	$-1, %al
.LBB3_8:                                #   in Loop: Header=BB3_3 Depth=1
	movzbl	%al, %esi
	movl	80(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	140(%rbx), %edx
	movl	%edx, 140(%rbx)
	movl	84(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 84(%rbx)
	addl	$-8, %eax
	movl	%eax, 80(%rbx)
	cmpl	$7, %eax
	ja	.LBB3_3
	jmp	.LBB3_9
.LBB3_1:                                # %._ZN5NBitl8CDecoderI9CInBufferE9NormalizeEv.exit_crit_edge.i
	leaq	140(%rbx), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	140(%rbx), %edx
	leaq	88(%rbx), %r13
	leaq	96(%rbx), %r14
	leaq	136(%rbx), %r12
.LBB3_9:                                # %_ZN5NBitl8CDecoderI9CInBufferE8ReadBitsEj.exit
	movzbl	%dl, %ecx
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	addl	$8, %eax
	movl	%eax, 80(%rbx)
	shrl	$8, %edx
	movl	%edx, 140(%rbx)
	xorl	%ecx, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	cmpl	$8, %eax
	jae	.LBB3_12
	jmp	.LBB3_11
	.p2align	4, 0x90
.LBB3_25:                               # %_ZN5NBitl8CDecoderI9CInBufferE8ReadBitsEj.exit52
	movl	%edx, %r15d
	andl	$15, %r15d
	addl	$4, %eax
	movl	%eax, 80(%rbx)
	shrl	$4, %edx
	movl	%edx, 140(%rbx)
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	1(%rcx,%r15), %eax
	cmpl	8(%rsp), %eax           # 4-byte Folded Reload
	jg	.LBB3_28
# BB#26:                                # %.preheader
	andl	$15, %ebp
	incl	%ebp
	movslq	%ecx, %rdi
	addq	40(%rsp), %rdi          # 8-byte Folded Reload
	leal	1(%r15), %edx
	movl	%ebp, %esi
	callq	memset
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	1(%rax,%r15), %eax
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmpl	28(%rsp), %ecx          # 4-byte Folded Reload
	jge	.LBB3_29
# BB#27:                                # %.preheader._crit_edge
	movq	%rax, 16(%rsp)          # 8-byte Spill
	incl	%ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	80(%rbx), %eax
	cmpl	$8, %eax
	jb	.LBB3_11
	.p2align	4, 0x90
.LBB3_12:                               # %.lr.ph.i.i34
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	(%r14), %rax
	jb	.LBB3_15
# BB#13:                                #   in Loop: Header=BB3_12 Depth=1
	movq	%r13, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB3_16
# BB#14:                                # %._crit_edge.i.i.i36
                                        #   in Loop: Header=BB3_12 Depth=1
	movq	(%r13), %rax
.LBB3_15:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i37
                                        #   in Loop: Header=BB3_12 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r13)
	movzbl	(%rax), %eax
	jmp	.LBB3_17
	.p2align	4, 0x90
.LBB3_16:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i38
                                        #   in Loop: Header=BB3_12 Depth=1
	incl	(%r12)
	movb	$-1, %al
.LBB3_17:                               #   in Loop: Header=BB3_12 Depth=1
	movzbl	%al, %edx
	movl	80(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%edx, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	orl	140(%rbx), %ebp
	movl	%ebp, 140(%rbx)
	movl	84(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rdx), %edx
	orl	%ecx, %edx
	movl	%edx, 84(%rbx)
	addl	$-8, %eax
	movl	%eax, 80(%rbx)
	cmpl	$7, %eax
	ja	.LBB3_12
	jmp	.LBB3_18
	.p2align	4, 0x90
.LBB3_11:                               # %._ZN5NBitl8CDecoderI9CInBufferE9NormalizeEv.exit_crit_edge.i33
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ebp
.LBB3_18:                               # %_ZN5NBitl8CDecoderI9CInBufferE8ReadBitsEj.exit41
	addl	$4, %eax
	movl	%eax, 80(%rbx)
	movl	%ebp, %edx
	shrl	$4, %edx
	movl	%edx, 140(%rbx)
	cmpl	$8, %eax
	jb	.LBB3_25
	.p2align	4, 0x90
.LBB3_19:                               # %.lr.ph.i.i45
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	(%r14), %rax
	jb	.LBB3_22
# BB#20:                                #   in Loop: Header=BB3_19 Depth=1
	movq	%r13, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB3_23
# BB#21:                                # %._crit_edge.i.i.i47
                                        #   in Loop: Header=BB3_19 Depth=1
	movq	(%r13), %rax
.LBB3_22:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i48
                                        #   in Loop: Header=BB3_19 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r13)
	movzbl	(%rax), %eax
	jmp	.LBB3_24
	.p2align	4, 0x90
.LBB3_23:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i49
                                        #   in Loop: Header=BB3_19 Depth=1
	incl	(%r12)
	movb	$-1, %al
.LBB3_24:                               #   in Loop: Header=BB3_19 Depth=1
	movzbl	%al, %esi
	movl	80(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	140(%rbx), %edx
	movl	%edx, 140(%rbx)
	movl	84(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 84(%rbx)
	addl	$-8, %eax
	movl	%eax, 80(%rbx)
	cmpl	$7, %eax
	ja	.LBB3_19
	jmp	.LBB3_25
.LBB3_29:
	cmpl	8(%rsp), %eax           # 4-byte Folded Reload
	jne	.LBB3_30
# BB#31:
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN9NCompress8NImplode8NHuffman8CDecoder14SetCodeLengthsEPKh # TAILCALL
.LBB3_30:
	xorl	%eax, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_28:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$0, (%rax)
	movl	$_ZTIN9NCompress8NImplode8NDecoder10CExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end3:
	.size	_ZN9NCompress8NImplode8NDecoder6CCoder14ReadLevelItemsERNS0_8NHuffman8CDecoderEPhi, .Lfunc_end3-_ZN9NCompress8NImplode8NDecoder6CCoder14ReadLevelItemsERNS0_8NHuffman8CDecoderEPhi
	.cfi_endproc

	.globl	_ZN9NCompress8NImplode8NDecoder6CCoder10ReadTablesEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NDecoder6CCoder10ReadTablesEv,@function
_ZN9NCompress8NImplode8NDecoder6CCoder10ReadTablesEv: # @_ZN9NCompress8NImplode8NDecoder6CCoder10ReadTablesEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 16
	subq	$320, %rsp              # imm = 0x140
.Lcfi26:
	.cfi_def_cfa_offset 336
.Lcfi27:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpb	$0, 625(%rbx)
	je	.LBB4_2
# BB#1:
	leaq	144(%rbx), %rsi
	leaq	64(%rsp), %rdx
	movl	$256, %ecx              # imm = 0x100
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NImplode8NDecoder6CCoder14ReadLevelItemsERNS0_8NHuffman8CDecoderEPhi
	testb	%al, %al
	je	.LBB4_5
.LBB4_2:
	leaq	304(%rbx), %rsi
	leaq	64(%rsp), %rdx
	movl	$64, %ecx
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NImplode8NDecoder6CCoder14ReadLevelItemsERNS0_8NHuffman8CDecoderEPhi
	testb	%al, %al
	je	.LBB4_5
# BB#3:
	movq	%rsp, %rdx
	leaq	464(%rbx), %rsi
	movl	$64, %ecx
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NImplode8NDecoder6CCoder14ReadLevelItemsERNS0_8NHuffman8CDecoderEPhi
                                        # kill: %AL<def> %AL<kill> %EAX<def>
	jmp	.LBB4_6
.LBB4_5:
	xorl	%eax, %eax
.LBB4_6:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$320, %rsp              # imm = 0x140
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZN9NCompress8NImplode8NDecoder6CCoder10ReadTablesEv, .Lfunc_end4-_ZN9NCompress8NImplode8NDecoder6CCoder10ReadTablesEv
	.cfi_endproc

	.globl	_ZN9NCompress8NImplode8NDecoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NDecoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo,@function
_ZN9NCompress8NImplode8NDecoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo: # @_ZN9NCompress8NImplode8NDecoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$472, %rsp              # imm = 0x1D8
.Lcfi34:
	.cfi_def_cfa_offset 528
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	%r8, %r15
	movq	%rdx, %rbp
	movq	%rsi, %r13
	movq	%rdi, %rbx
	leaq	88(%rbx), %r12
	movl	$1048576, %esi          # imm = 0x100000
	movq	%r12, %rdi
	callq	_ZN9CInBuffer6CreateEj
	movl	$-2147024882, %r14d     # imm = 0x8007000E
	testb	%al, %al
	je	.LBB5_4
# BB#1:
	leaq	24(%rbx), %rdi
	movl	$8192, %esi             # imm = 0x2000
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	callq	_ZN10COutBuffer6CreateEj
	testb	%al, %al
	je	.LBB5_4
# BB#2:
	testq	%r15, %r15
	je	.LBB5_3
# BB#5:
	movq	$0, 8(%rsp)
	movq	(%r15), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	_ZN12CLzOutWindow4InitEb
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	movq	%r12, %rdi
	callq	_ZN9CInBuffer4InitEv
	movq	$32, 80(%rbx)
	movq	$0, 136(%rbx)
	cmpb	$0, 625(%rbx)
	je	.LBB5_8
# BB#6:
	leaq	144(%rbx), %rsi
.Ltmp33:
	leaq	208(%rsp), %rdx
	movl	$256, %ecx              # imm = 0x100
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NImplode8NDecoder6CCoder14ReadLevelItemsERNS0_8NHuffman8CDecoderEPhi
.Ltmp34:
# BB#7:                                 # %.noexc
	movl	$1, %r14d
	testb	%al, %al
	je	.LBB5_11
.LBB5_8:
	leaq	304(%rbx), %rsi
.Ltmp35:
	leaq	144(%rsp), %rdx
	movl	$64, %ecx
	movq	%rbx, %rdi
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	callq	_ZN9NCompress8NImplode8NDecoder6CCoder14ReadLevelItemsERNS0_8NHuffman8CDecoderEPhi
.Ltmp36:
# BB#9:                                 # %.noexc77
	testb	%al, %al
	je	.LBB5_10
# BB#15:
	leaq	464(%rbx), %rsi
.Ltmp37:
	leaq	80(%rsp), %rdx
	movl	$64, %ecx
	movq	%rbx, %rdi
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	callq	_ZN9NCompress8NImplode8NDecoder6CCoder14ReadLevelItemsERNS0_8NHuffman8CDecoderEPhi
.Ltmp38:
# BB#16:                                # %_ZN9NCompress8NImplode8NDecoder6CCoder10ReadTablesEv.exit
	movl	$1, %r14d
	testb	%al, %al
	je	.LBB5_11
# BB#17:                                # %.preheader155
	movq	8(%rsp), %rax
	cmpq	40(%rsp), %rax          # 8-byte Folded Reload
	jae	.LBB5_104
# BB#18:                                # %.lr.ph182.lr.ph
	leaq	80(%rbx), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	144(%rbx), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	jmp	.LBB5_19
.LBB5_46:                               #   in Loop: Header=BB5_19 Depth=1
	movb	628(%rbx), %cl
	shll	%cl, %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
.Ltmp59:
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	_ZN9NCompress8NImplode8NHuffman8CDecoder12DecodeSymbolEPN5NBitl8CDecoderI9CInBufferEE
.Ltmp60:
# BB#47:                                #   in Loop: Header=BB5_19 Depth=1
	cmpl	$63, %eax
	ja	.LBB5_11
# BB#48:                                #   in Loop: Header=BB5_19 Depth=1
	movl	632(%rbx), %ebp
	addl	%eax, %ebp
	cmpl	$63, %eax
	jne	.LBB5_49
# BB#51:                                #   in Loop: Header=BB5_19 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpl	$8, %eax
	jb	.LBB5_52
	.p2align	4, 0x90
.LBB5_53:                               # %.lr.ph.i.i95
                                        #   Parent Loop BB5_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	88(%rbx), %rax
	cmpq	96(%rbx), %rax
	jb	.LBB5_57
# BB#54:                                #   in Loop: Header=BB5_53 Depth=2
.Ltmp62:
	movq	%r12, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp63:
# BB#55:                                # %.noexc102
                                        #   in Loop: Header=BB5_53 Depth=2
	testb	%al, %al
	je	.LBB5_58
# BB#56:                                # %._crit_edge.i.i.i97
                                        #   in Loop: Header=BB5_53 Depth=2
	movq	(%r12), %rax
.LBB5_57:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i98
                                        #   in Loop: Header=BB5_53 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	movzbl	(%rax), %eax
	jmp	.LBB5_59
	.p2align	4, 0x90
.LBB5_58:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i99
                                        #   in Loop: Header=BB5_53 Depth=2
	incl	136(%rbx)
	movb	$-1, %al
.LBB5_59:                               #   in Loop: Header=BB5_53 Depth=2
	movzbl	%al, %esi
	movl	80(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	140(%rbx), %edx
	movl	%edx, 140(%rbx)
	movl	84(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 84(%rbx)
	addl	$-8, %eax
	movl	%eax, 80(%rbx)
	cmpl	$7, %eax
	ja	.LBB5_53
	jmp	.LBB5_60
.LBB5_52:                               # %._ZN5NBitl8CDecoderI9CInBufferE9NormalizeEv.exit_crit_edge.i94
                                        #   in Loop: Header=BB5_19 Depth=1
	movl	140(%rbx), %edx
.LBB5_60:                               # %.loopexit139
                                        #   in Loop: Header=BB5_19 Depth=1
	movzbl	%dl, %ecx
	addl	$8, %eax
	movl	%eax, 80(%rbx)
	shrl	$8, %edx
	movl	%edx, 140(%rbx)
	addl	%ebp, %ecx
	movl	%ecx, %ebp
.LBB5_49:                               #   in Loop: Header=BB5_19 Depth=1
	movl	20(%rsp), %ecx          # 4-byte Reload
	decl	%r15d
	andl	%r15d, %r13d
	addl	%r13d, %ecx
	testl	%ebp, %ebp
	setne	%al
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	je	.LBB5_50
# BB#64:                                # %.preheader
                                        #   in Loop: Header=BB5_19 Depth=1
	movl	%ecx, %r15d
	cmpq	8(%rsp), %r15
	movl	%ebp, %r14d
	movq	32(%rsp), %r13          # 8-byte Reload
	jb	.LBB5_69
	.p2align	4, 0x90
.LBB5_65:                               # %.lr.ph
                                        #   Parent Loop BB5_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rbx), %rax
	movl	32(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 32(%rbx)
	movb	$0, (%rax,%rcx)
	movl	32(%rbx), %eax
	cmpl	36(%rbx), %eax
	jne	.LBB5_67
# BB#66:                                #   in Loop: Header=BB5_65 Depth=2
.Ltmp65:
	movq	%r13, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp66:
.LBB5_67:                               # %_ZN12CLzOutWindow7PutByteEh.exit
                                        #   in Loop: Header=BB5_65 Depth=2
	movq	8(%rsp), %rcx
	incq	%rcx
	movq	%rcx, 8(%rsp)
	leal	-1(%rbp), %r14d
	cmpl	$1, %ebp
	setne	%al
	je	.LBB5_69
# BB#68:                                # %_ZN12CLzOutWindow7PutByteEh.exit
                                        #   in Loop: Header=BB5_65 Depth=2
	cmpq	%rcx, %r15
	movl	%r14d, %ebp
	jae	.LBB5_65
	jmp	.LBB5_69
.LBB5_50:                               #   in Loop: Header=BB5_19 Depth=1
	movl	%ebp, %r14d
	movq	32(%rsp), %r13          # 8-byte Reload
.LBB5_69:                               # %._crit_edge
                                        #   in Loop: Header=BB5_19 Depth=1
	testb	%al, %al
	je	.LBB5_102
# BB#70:                                #   in Loop: Header=BB5_19 Depth=1
	movl	32(%rbx), %edx
	movl	%edx, %ebp
	movl	20(%rsp), %eax          # 4-byte Reload
	subl	%eax, %ebp
	decl	%ebp
	movl	%eax, %ecx
	cmpl	%eax, %edx
	ja	.LBB5_74
# BB#71:                                #   in Loop: Header=BB5_19 Depth=1
	cmpb	$0, 72(%rbx)
	je	.LBB5_102
# BB#72:                                #   in Loop: Header=BB5_19 Depth=1
	movl	44(%rbx), %eax
	cmpl	%ecx, %eax
	jbe	.LBB5_102
# BB#73:                                #   in Loop: Header=BB5_19 Depth=1
	addl	%ebp, %eax
	movl	%eax, %ebp
.LBB5_74:                               #   in Loop: Header=BB5_19 Depth=1
	movl	36(%rbx), %eax
	subl	%edx, %eax
	cmpl	%r14d, %eax
	jbe	.LBB5_75
# BB#80:                                #   in Loop: Header=BB5_19 Depth=1
	movl	44(%rbx), %eax
	subl	%ebp, %eax
	cmpl	%r14d, %eax
	jbe	.LBB5_75
# BB#81:                                #   in Loop: Header=BB5_19 Depth=1
	movq	24(%rbx), %r15
	movl	%ebp, %r11d
	leaq	(%r15,%r11), %r10
	leaq	(%r15,%rdx), %rcx
	movl	%edx, %esi
	addl	%r14d, %esi
	movl	%esi, 32(%rbx)
	leal	-1(%r14), %esi
	leaq	1(%rsi), %r9
	cmpq	$32, %r9
	jae	.LBB5_83
# BB#82:                                #   in Loop: Header=BB5_19 Depth=1
	movl	%r14d, %edx
	jmp	.LBB5_97
.LBB5_75:                               # %.preheader.i.preheader
                                        #   in Loop: Header=BB5_19 Depth=1
	movl	$1, %r15d
	subl	%r14d, %r15d
	jmp	.LBB5_76
	.p2align	4, 0x90
.LBB5_79:                               # %._crit_edge.i
                                        #   in Loop: Header=BB5_76 Depth=2
	incl	%ebp
	movl	32(%rbx), %edx
	incl	%r15d
.LBB5_76:                               # %.preheader.i
                                        #   Parent Loop BB5_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	44(%rbx), %ebp
	movl	$0, %eax
	cmovel	%eax, %ebp
	movq	24(%rbx), %rax
	movzbl	(%rax,%rbp), %ecx
	leal	1(%rdx), %esi
	movl	%esi, 32(%rbx)
	movl	%edx, %edx
	movb	%cl, (%rax,%rdx)
	movl	32(%rbx), %eax
	cmpl	36(%rbx), %eax
	jne	.LBB5_78
# BB#77:                                #   in Loop: Header=BB5_76 Depth=2
.Ltmp68:
	movq	%r13, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp69:
.LBB5_78:                               # %.noexc107
                                        #   in Loop: Header=BB5_76 Depth=2
	testl	%r15d, %r15d
	jne	.LBB5_79
.LBB5_102:                              # %.loopexit
                                        #   in Loop: Header=BB5_19 Depth=1
	movl	%r14d, %eax
	addq	8(%rsp), %rax
.LBB5_103:                              # %.loopexit
                                        #   in Loop: Header=BB5_19 Depth=1
	movq	%rax, 8(%rsp)
	cmpq	40(%rsp), %rax          # 8-byte Folded Reload
	jb	.LBB5_19
	jmp	.LBB5_104
.LBB5_83:                               # %min.iters.checked
                                        #   in Loop: Header=BB5_19 Depth=1
	movq	%r9, %r8
	movabsq	$8589934560, %rax       # imm = 0x1FFFFFFE0
	andq	%rax, %r8
	je	.LBB5_84
# BB#85:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_19 Depth=1
	leaq	(%r11,%rsi), %rdi
	leaq	1(%r15,%rdi), %rdi
	cmpq	%rdi, %rcx
	jae	.LBB5_88
# BB#86:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_19 Depth=1
	addq	%rdx, %rsi
	leaq	1(%r15,%rsi), %rsi
	cmpq	%rsi, %r10
	jae	.LBB5_88
# BB#87:                                #   in Loop: Header=BB5_19 Depth=1
	movl	%r14d, %edx
	jmp	.LBB5_97
.LBB5_107:                              #   in Loop: Header=BB5_19 Depth=1
	cmpb	$0, 625(%rbx)
	je	.LBB5_112
# BB#108:                               #   in Loop: Header=BB5_19 Depth=1
.Ltmp45:
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	_ZN9NCompress8NImplode8NHuffman8CDecoder12DecodeSymbolEPN5NBitl8CDecoderI9CInBufferEE
	movl	%eax, %r13d
.Ltmp46:
# BB#109:                               #   in Loop: Header=BB5_19 Depth=1
	cmpl	$255, %r13d
	jbe	.LBB5_121
	jmp	.LBB5_10
.LBB5_84:                               #   in Loop: Header=BB5_19 Depth=1
	movl	%r14d, %edx
	jmp	.LBB5_97
.LBB5_112:                              #   in Loop: Header=BB5_19 Depth=1
	cmpl	$8, %eax
	jb	.LBB5_120
.LBB5_113:                              # %.lr.ph.i.i111
                                        #   Parent Loop BB5_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	88(%rbx), %rax
	cmpq	96(%rbx), %rax
	jb	.LBB5_117
# BB#114:                               #   in Loop: Header=BB5_113 Depth=2
.Ltmp47:
	movq	%r12, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp48:
# BB#115:                               # %.noexc118
                                        #   in Loop: Header=BB5_113 Depth=2
	testb	%al, %al
	je	.LBB5_118
# BB#116:                               # %._crit_edge.i.i.i113
                                        #   in Loop: Header=BB5_113 Depth=2
	movq	(%r12), %rax
.LBB5_117:                              # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i114
                                        #   in Loop: Header=BB5_113 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	movzbl	(%rax), %eax
	jmp	.LBB5_119
.LBB5_118:                              # %_ZN9CInBuffer8ReadByteERh.exit.i.i115
                                        #   in Loop: Header=BB5_113 Depth=2
	incl	136(%rbx)
	movb	$-1, %al
.LBB5_119:                              #   in Loop: Header=BB5_113 Depth=2
	movzbl	%al, %edx
	movl	80(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%edx, %r13d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r13d
	orl	140(%rbx), %r13d
	movl	%r13d, 140(%rbx)
	movl	84(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rdx), %edx
	orl	%ecx, %edx
	movl	%edx, 84(%rbx)
	addl	$-8, %eax
	movl	%eax, 80(%rbx)
	cmpl	$7, %eax
	ja	.LBB5_113
.LBB5_120:                              # %.loopexit150
                                        #   in Loop: Header=BB5_19 Depth=1
	addl	$8, %eax
	movl	%eax, 80(%rbx)
	movl	%r13d, %eax
	shrl	$8, %eax
	movl	%eax, 140(%rbx)
.LBB5_121:                              #   in Loop: Header=BB5_19 Depth=1
	movq	24(%rbx), %rax
	movl	32(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 32(%rbx)
	movb	%r13b, (%rax,%rcx)
	movl	32(%rbx), %eax
	cmpl	36(%rbx), %eax
	jne	.LBB5_123
# BB#122:                               #   in Loop: Header=BB5_19 Depth=1
.Ltmp50:
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp51:
.LBB5_123:                              # %.thread131
                                        #   in Loop: Header=BB5_19 Depth=1
	movq	8(%rsp), %rax
	incq	%rax
	jmp	.LBB5_103
.LBB5_88:                               # %vector.body.preheader
                                        #   in Loop: Header=BB5_19 Depth=1
	leaq	-32(%r8), %rax
	movl	%eax, %edi
	shrl	$5, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB5_89
# BB#90:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB5_19 Depth=1
	leaq	16(%r15,%rdx), %r13
	leaq	16(%r15,%r11), %rsi
	negq	%rdi
	xorl	%ebp, %ebp
.LBB5_91:                               # %vector.body.prol
                                        #   Parent Loop BB5_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rsi,%rbp), %xmm0
	movups	(%rsi,%rbp), %xmm1
	movups	%xmm0, -16(%r13,%rbp)
	movups	%xmm1, (%r13,%rbp)
	addq	$32, %rbp
	incq	%rdi
	jne	.LBB5_91
	jmp	.LBB5_92
.LBB5_89:                               #   in Loop: Header=BB5_19 Depth=1
	xorl	%ebp, %ebp
.LBB5_92:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB5_19 Depth=1
	cmpq	$96, %rax
	jb	.LBB5_95
# BB#93:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB5_19 Depth=1
	movq	%r8, %rdi
	subq	%rbp, %rdi
	leaq	(%rdx,%rbp), %rdx
	leaq	112(%r15,%rdx), %rdx
	addq	%rbp, %r11
	leaq	112(%r15,%r11), %rsi
.LBB5_94:                               # %vector.body
                                        #   Parent Loop BB5_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-128, %rdi
	jne	.LBB5_94
.LBB5_95:                               # %middle.block
                                        #   in Loop: Header=BB5_19 Depth=1
	cmpq	%r8, %r9
	je	.LBB5_102
# BB#96:                                #   in Loop: Header=BB5_19 Depth=1
	movl	%r14d, %edx
	subl	%r8d, %edx
	addq	%r8, %r10
	addq	%r8, %rcx
.LBB5_97:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB5_19 Depth=1
	leal	-1(%rdx), %esi
	movl	%edx, %edi
	andl	$7, %edi
	je	.LBB5_100
# BB#98:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB5_19 Depth=1
	negl	%edi
.LBB5_99:                               # %scalar.ph.prol
                                        #   Parent Loop BB5_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r10), %r8d
	incq	%r10
	movb	%r8b, (%rcx)
	incq	%rcx
	decl	%edx
	incl	%edi
	jne	.LBB5_99
.LBB5_100:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB5_19 Depth=1
	cmpl	$7, %esi
	jb	.LBB5_102
.LBB5_101:                              # %scalar.ph
                                        #   Parent Loop BB5_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r10), %eax
	movb	%al, (%rcx)
	movzbl	1(%r10), %eax
	movb	%al, 1(%rcx)
	movzbl	2(%r10), %eax
	movb	%al, 2(%rcx)
	movzbl	3(%r10), %eax
	movb	%al, 3(%rcx)
	movzbl	4(%r10), %eax
	movb	%al, 4(%rcx)
	movzbl	5(%r10), %eax
	movb	%al, 5(%rcx)
	movzbl	6(%r10), %eax
	movb	%al, 6(%rcx)
	movzbl	7(%r10), %eax
	movb	%al, 7(%rcx)
	addq	$8, %r10
	addq	$8, %rcx
	addl	$-8, %edx
	jne	.LBB5_101
	jmp	.LBB5_102
.LBB5_19:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_28 Depth 2
                                        #     Child Loop BB5_113 Depth 2
                                        #     Child Loop BB5_37 Depth 2
                                        #     Child Loop BB5_53 Depth 2
                                        #     Child Loop BB5_65 Depth 2
                                        #     Child Loop BB5_76 Depth 2
                                        #     Child Loop BB5_91 Depth 2
                                        #     Child Loop BB5_94 Depth 2
                                        #     Child Loop BB5_99 Depth 2
                                        #     Child Loop BB5_101 Depth 2
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB5_23
# BB#20:                                #   in Loop: Header=BB5_19 Depth=1
	movzwl	%ax, %eax
	testq	%rax, %rax
	jne	.LBB5_23
# BB#21:                                #   in Loop: Header=BB5_19 Depth=1
	movq	88(%rbx), %rax
	addq	120(%rbx), %rax
	subq	104(%rbx), %rax
	movl	136(%rbx), %ecx
	addq	%rax, %rcx
	movl	$32, %eax
	subl	80(%rbx), %eax
	shrl	$3, %eax
	subq	%rax, %rcx
	movq	%rcx, 208(%rsp)
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp39:
	leaq	208(%rsp), %rsi
	leaq	8(%rsp), %rdx
	callq	*40(%rax)
	movl	%eax, %r14d
.Ltmp40:
# BB#22:                                #   in Loop: Header=BB5_19 Depth=1
	testl	%r14d, %r14d
	jne	.LBB5_11
.LBB5_23:                               #   in Loop: Header=BB5_19 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpl	$8, %eax
	jb	.LBB5_24
	.p2align	4, 0x90
.LBB5_28:                               # %.lr.ph.i.i
                                        #   Parent Loop BB5_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	88(%rbx), %rax
	cmpq	96(%rbx), %rax
	jb	.LBB5_32
# BB#29:                                #   in Loop: Header=BB5_28 Depth=2
.Ltmp42:
	movq	%r12, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp43:
# BB#30:                                # %.noexc79
                                        #   in Loop: Header=BB5_28 Depth=2
	testb	%al, %al
	je	.LBB5_33
# BB#31:                                # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB5_28 Depth=2
	movq	(%r12), %rax
.LBB5_32:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i
                                        #   in Loop: Header=BB5_28 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	movzbl	(%rax), %eax
	jmp	.LBB5_34
	.p2align	4, 0x90
.LBB5_33:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i
                                        #   in Loop: Header=BB5_28 Depth=2
	incl	136(%rbx)
	movb	$-1, %al
.LBB5_34:                               #   in Loop: Header=BB5_28 Depth=2
	movzbl	%al, %esi
	movl	80(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%esi, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	140(%rbx), %edx
	movl	%edx, 140(%rbx)
	movl	84(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rsi), %esi
	orl	%ecx, %esi
	movl	%esi, 84(%rbx)
	addl	$-8, %eax
	movl	%eax, 80(%rbx)
	cmpl	$7, %eax
	ja	.LBB5_28
	jmp	.LBB5_35
.LBB5_24:                               # %._ZN5NBitl8CDecoderI9CInBufferE9NormalizeEv.exit_crit_edge.i
                                        #   in Loop: Header=BB5_19 Depth=1
	movl	140(%rbx), %edx
.LBB5_35:                               # %.loopexit154
                                        #   in Loop: Header=BB5_19 Depth=1
	incl	%eax
	movl	%eax, 80(%rbx)
	movl	%edx, %r13d
	shrl	%r13d
	movl	%r13d, 140(%rbx)
	testb	$1, %dl
	jne	.LBB5_107
# BB#36:                                #   in Loop: Header=BB5_19 Depth=1
	movl	628(%rbx), %ebp
	cmpl	$8, %eax
	jb	.LBB5_44
	.p2align	4, 0x90
.LBB5_37:                               # %.lr.ph.i.i83
                                        #   Parent Loop BB5_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	88(%rbx), %rax
	cmpq	96(%rbx), %rax
	jb	.LBB5_41
# BB#38:                                #   in Loop: Header=BB5_37 Depth=2
.Ltmp53:
	movq	%r12, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp54:
# BB#39:                                # %.noexc90
                                        #   in Loop: Header=BB5_37 Depth=2
	testb	%al, %al
	je	.LBB5_42
# BB#40:                                # %._crit_edge.i.i.i85
                                        #   in Loop: Header=BB5_37 Depth=2
	movq	(%r12), %rax
.LBB5_41:                               # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i86
                                        #   in Loop: Header=BB5_37 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	movzbl	(%rax), %eax
	jmp	.LBB5_43
	.p2align	4, 0x90
.LBB5_42:                               # %_ZN9CInBuffer8ReadByteERh.exit.i.i87
                                        #   in Loop: Header=BB5_37 Depth=2
	incl	136(%rbx)
	movb	$-1, %al
.LBB5_43:                               #   in Loop: Header=BB5_37 Depth=2
	movzbl	%al, %edx
	movl	80(%rbx), %eax
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	%edx, %r13d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r13d
	orl	140(%rbx), %r13d
	movl	%r13d, 140(%rbx)
	movl	84(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rdx), %edx
	orl	%ecx, %edx
	movl	%edx, 84(%rbx)
	addl	$-8, %eax
	movl	%eax, 80(%rbx)
	cmpl	$7, %eax
	ja	.LBB5_37
.LBB5_44:                               # %.loopexit144
                                        #   in Loop: Header=BB5_19 Depth=1
	movl	$1, %r15d
	movl	%ebp, %ecx
	shll	%cl, %r15d
	addl	%ebp, %eax
	movl	%eax, 80(%rbx)
	movl	%r13d, %eax
	shrl	%cl, %eax
	movl	%eax, 140(%rbx)
.Ltmp56:
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	_ZN9NCompress8NImplode8NHuffman8CDecoder12DecodeSymbolEPN5NBitl8CDecoderI9CInBufferEE
.Ltmp57:
# BB#45:                                #   in Loop: Header=BB5_19 Depth=1
	movl	$1, %r14d
	cmpl	$63, %eax
	jbe	.LBB5_46
	jmp	.LBB5_11
.LBB5_3:
	movl	$-2147024809, %r14d     # imm = 0x80070057
	jmp	.LBB5_4
.LBB5_10:                               # %_ZN9NCompress8NImplode8NDecoder6CCoder10ReadTablesEv.exit.thread129
	movl	$1, %r14d
.LBB5_11:                               # %_ZN9NCompress8NImplode8NDecoder6CCoder10ReadTablesEv.exit.thread
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_13
# BB#12:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 48(%rbx)
.LBB5_13:                               # %_ZN10COutBuffer13ReleaseStreamEv.exit.i.i
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_4
# BB#14:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 112(%rbx)
.LBB5_4:
	movl	%r14d, %eax
	addq	$472, %rsp              # imm = 0x1D8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_104:                              # %.outer._crit_edge
	cmpq	40(%rsp), %rax          # 8-byte Folded Reload
	movl	$1, %r14d
	ja	.LBB5_11
# BB#105:
.Ltmp71:
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZN10COutBuffer5FlushEv
	movl	%eax, %r14d
.Ltmp72:
	jmp	.LBB5_11
.LBB5_111:                              # %.loopexit.split-lp146
.Ltmp52:
	jmp	.LBB5_125
.LBB5_110:                              # %.loopexit145
.Ltmp49:
	jmp	.LBB5_125
.LBB5_124:
.Ltmp41:
	jmp	.LBB5_125
.LBB5_133:
.Ltmp61:
	jmp	.LBB5_125
.LBB5_106:
.Ltmp58:
	jmp	.LBB5_125
.LBB5_27:                               # %.loopexit.split-lp141.loopexit.split-lp
.Ltmp73:
	jmp	.LBB5_125
.LBB5_61:                               # %.loopexit134
.Ltmp70:
	jmp	.LBB5_125
.LBB5_63:                               # %.loopexit.split-lp.loopexit.split-lp
.Ltmp64:
	jmp	.LBB5_125
.LBB5_62:                               # %.loopexit.split-lp.loopexit
.Ltmp67:
	jmp	.LBB5_125
.LBB5_25:                               # %.loopexit140
.Ltmp55:
	jmp	.LBB5_125
.LBB5_26:                               # %.loopexit.split-lp141.loopexit
.Ltmp44:
.LBB5_125:
	movq	%rax, %rbp
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_128
# BB#126:
	movq	(%rdi), %rax
.Ltmp74:
	callq	*16(%rax)
.Ltmp75:
# BB#127:                               # %.noexc123
	movq	$0, 48(%rbx)
.LBB5_128:                              # %_ZN10COutBuffer13ReleaseStreamEv.exit.i.i122
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_131
# BB#129:
	movq	(%rdi), %rax
.Ltmp76:
	callq	*16(%rax)
.Ltmp77:
# BB#130:                               # %.noexc124
	movq	$0, 112(%rbx)
.LBB5_131:                              # %_ZN9NCompress8NImplode8NDecoder14CCoderReleaserD2Ev.exit125
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB5_132:
.Ltmp78:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN9NCompress8NImplode8NDecoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo, .Lfunc_end5-_ZN9NCompress8NImplode8NDecoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\346\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\335\001"              # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp33-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp38-.Ltmp33         #   Call between .Ltmp33 and .Ltmp38
	.long	.Ltmp73-.Lfunc_begin1   #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin1   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin1   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin1   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin1   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp52-.Lfunc_begin1   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin1   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin1   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin1   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin1   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Ltmp54-.Ltmp53         #   Call between .Ltmp53 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin1   #     jumps to .Ltmp55
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin1   # >> Call Site 13 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin1   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin1   # >> Call Site 14 <<
	.long	.Ltmp71-.Ltmp57         #   Call between .Ltmp57 and .Ltmp71
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin1   # >> Call Site 15 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin1   #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin1   # >> Call Site 16 <<
	.long	.Ltmp77-.Ltmp74         #   Call between .Ltmp74 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin1   #     jumps to .Ltmp78
	.byte	1                       #   On action: 1
	.long	.Ltmp77-.Lfunc_begin1   # >> Call Site 17 <<
	.long	.Lfunc_end5-.Ltmp77     #   Call between .Ltmp77 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress8NImplode8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo,@function
_ZN9NCompress8NImplode8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo: # @_ZN9NCompress8NImplode8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 16
.Lcfi42:
	.cfi_offset %rbx, -16
.Ltmp79:
	callq	_ZN9NCompress8NImplode8NDecoder6CCoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	movl	%eax, %ebx
.Ltmp80:
.LBB6_4:
	movl	%ebx, %eax
	popq	%rbx
	retq
.LBB6_1:
.Ltmp81:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	cmpl	$2, %ebx
	jne	.LBB6_3
# BB#2:
	movl	(%rax), %ebx
	callq	__cxa_end_catch
	jmp	.LBB6_4
.LBB6_3:
	callq	__cxa_end_catch
	movl	$1, %ebx
	jmp	.LBB6_4
.Lfunc_end6:
	.size	_ZN9NCompress8NImplode8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo, .Lfunc_end6-_ZN9NCompress8NImplode8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\250"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp79-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin2   #     jumps to .Ltmp81
	.byte	3                       #   On action: 2
	.long	.Ltmp80-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end6-.Ltmp80     #   Call between .Ltmp80 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTI19COutBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress8NImplode8NDecoder6CCoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NDecoder6CCoder21SetDecoderProperties2EPKhj,@function
_ZN9NCompress8NImplode8NDecoder6CCoder21SetDecoderProperties2EPKhj: # @_ZN9NCompress8NImplode8NDecoder6CCoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	testl	%edx, %edx
	je	.LBB7_1
# BB#2:
	movzbl	(%rsi), %eax
	movl	%eax, %ecx
	shrl	%ecx
	andl	$1, %ecx
	movb	%cl, 624(%rdi)
	orl	$6, %ecx
	movl	%ecx, 628(%rdi)
	shrl	$2, %eax
	andl	$1, %eax
	movb	%al, 625(%rdi)
	orl	$2, %eax
	movl	%eax, 632(%rdi)
	xorl	%eax, %eax
	retq
.LBB7_1:
	movl	$-2147024809, %eax      # imm = 0x80070057
	retq
.Lfunc_end7:
	.size	_ZN9NCompress8NImplode8NDecoder6CCoder21SetDecoderProperties2EPKhj, .Lfunc_end7-_ZN9NCompress8NImplode8NDecoder6CCoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.globl	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder21SetDecoderProperties2EPKhj,@function
_ZThn8_N9NCompress8NImplode8NDecoder6CCoder21SetDecoderProperties2EPKhj: # @_ZThn8_N9NCompress8NImplode8NDecoder6CCoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	testl	%edx, %edx
	je	.LBB8_1
# BB#2:
	movzbl	(%rsi), %eax
	movl	%eax, %ecx
	shrl	%ecx
	andl	$1, %ecx
	movb	%cl, 616(%rdi)
	orl	$6, %ecx
	movl	%ecx, 620(%rdi)
	shrl	$2, %eax
	andl	$1, %eax
	movb	%al, 617(%rdi)
	orl	$2, %eax
	movl	%eax, 624(%rdi)
	xorl	%eax, %eax
	retq
.LBB8_1:
	movl	$-2147024809, %eax      # imm = 0x80070057
	retq
.Lfunc_end8:
	.size	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder21SetDecoderProperties2EPKhj, .Lfunc_end8-_ZThn8_N9NCompress8NImplode8NDecoder6CCoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.section	.text._ZN9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 32
.Lcfi46:
	.cfi_offset %rbx, -32
.Lcfi47:
	.cfi_offset %r14, -24
.Lcfi48:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB9_3
# BB#1:
	movl	$IID_ICompressSetDecoderProperties2, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB9_2
.LBB9_3:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB9_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB9_2:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB9_4
.Lfunc_end9:
	.size	_ZN9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end9-_ZN9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress8NImplode8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZN9NCompress8NImplode8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZN9NCompress8NImplode8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NDecoder6CCoder6AddRefEv,@function
_ZN9NCompress8NImplode8NDecoder6CCoder6AddRefEv: # @_ZN9NCompress8NImplode8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end10:
	.size	_ZN9NCompress8NImplode8NDecoder6CCoder6AddRefEv, .Lfunc_end10-_ZN9NCompress8NImplode8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NImplode8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZN9NCompress8NImplode8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZN9NCompress8NImplode8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NDecoder6CCoder7ReleaseEv,@function
_ZN9NCompress8NImplode8NDecoder6CCoder7ReleaseEv: # @_ZN9NCompress8NImplode8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB11_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB11_2:
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZN9NCompress8NImplode8NDecoder6CCoder7ReleaseEv, .Lfunc_end11-_ZN9NCompress8NImplode8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NImplode8NDecoder6CCoderD2Ev,"axG",@progbits,_ZN9NCompress8NImplode8NDecoder6CCoderD2Ev,comdat
	.weak	_ZN9NCompress8NImplode8NDecoder6CCoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NDecoder6CCoderD2Ev,@function
_ZN9NCompress8NImplode8NDecoder6CCoderD2Ev: # @_ZN9NCompress8NImplode8NDecoder6CCoderD2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 32
.Lcfi53:
	.cfi_offset %rbx, -24
.Lcfi54:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress8NImplode8NDecoder6CCoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NImplode8NDecoder6CCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	leaq	464(%rbx), %rdi
.Ltmp82:
	callq	_ZN9NCompress8NImplode8NHuffman8CDecoderD1Ev
.Ltmp83:
# BB#1:
	leaq	304(%rbx), %rdi
.Ltmp87:
	callq	_ZN9NCompress8NImplode8NHuffman8CDecoderD1Ev
.Ltmp88:
# BB#2:
	leaq	144(%rbx), %rdi
.Ltmp92:
	callq	_ZN9NCompress8NImplode8NHuffman8CDecoderD1Ev
.Ltmp93:
# BB#3:
	leaq	88(%rbx), %rdi
.Ltmp103:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp104:
# BB#4:
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp109:
	callq	*16(%rax)
.Ltmp110:
.LBB12_6:                               # %_ZN5NBitl12CBaseDecoderI9CInBufferED2Ev.exit
	leaq	24(%rbx), %rdi
.Ltmp121:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp122:
# BB#7:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_9
# BB#8:
	movq	(%rdi), %rax
.Ltmp127:
	callq	*16(%rax)
.Ltmp128:
.LBB12_9:                               # %_ZN10COutBufferD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB12_31:
.Ltmp129:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_19:
.Ltmp111:
	movq	%rax, %r14
	jmp	.LBB12_24
.LBB12_13:
.Ltmp123:
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_27
# BB#14:
	movq	(%rdi), %rax
.Ltmp124:
	callq	*16(%rax)
.Ltmp125:
	jmp	.LBB12_27
.LBB12_15:
.Ltmp126:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_10:
.Ltmp105:
	movq	%rax, %r14
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_24
# BB#11:
	movq	(%rdi), %rax
.Ltmp106:
	callq	*16(%rax)
.Ltmp107:
	jmp	.LBB12_24
.LBB12_12:
.Ltmp108:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_20:
.Ltmp94:
	movq	%rax, %r14
	jmp	.LBB12_21
.LBB12_17:
.Ltmp89:
	movq	%rax, %r14
	jmp	.LBB12_18
.LBB12_16:
.Ltmp84:
	movq	%rax, %r14
	leaq	304(%rbx), %rdi
.Ltmp85:
	callq	_ZN9NCompress8NImplode8NHuffman8CDecoderD1Ev
.Ltmp86:
.LBB12_18:
	leaq	144(%rbx), %rdi
.Ltmp90:
	callq	_ZN9NCompress8NImplode8NHuffman8CDecoderD1Ev
.Ltmp91:
.LBB12_21:
	leaq	88(%rbx), %rdi
.Ltmp95:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp96:
# BB#22:
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_24
# BB#23:
	movq	(%rdi), %rax
.Ltmp101:
	callq	*16(%rax)
.Ltmp102:
.LBB12_24:                              # %_ZN5NBitl12CBaseDecoderI9CInBufferED2Ev.exit14
	leaq	24(%rbx), %rdi
.Ltmp112:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp113:
# BB#25:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_27
# BB#26:
	movq	(%rdi), %rax
.Ltmp118:
	callq	*16(%rax)
.Ltmp119:
.LBB12_27:                              # %_ZN10COutBufferD2Ev.exit19
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_28:
.Ltmp97:
	movq	%rax, %r14
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_36
# BB#29:
	movq	(%rdi), %rax
.Ltmp98:
	callq	*16(%rax)
.Ltmp99:
	jmp	.LBB12_36
.LBB12_30:
.Ltmp100:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_32:
.Ltmp114:
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_36
# BB#33:
	movq	(%rdi), %rax
.Ltmp115:
	callq	*16(%rax)
.Ltmp116:
	jmp	.LBB12_36
.LBB12_34:
.Ltmp117:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_35:
.Ltmp120:
	movq	%rax, %r14
.LBB12_36:                              # %.body12
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN9NCompress8NImplode8NDecoder6CCoderD2Ev, .Lfunc_end12-_ZN9NCompress8NImplode8NDecoder6CCoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\363\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\352\001"              # Call site table length
	.long	.Ltmp82-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin3   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp87-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin3   #     jumps to .Ltmp89
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin3   #     jumps to .Ltmp94
	.byte	0                       #   On action: cleanup
	.long	.Ltmp103-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin3  #     jumps to .Ltmp105
	.byte	0                       #   On action: cleanup
	.long	.Ltmp109-.Lfunc_begin3  # >> Call Site 5 <<
	.long	.Ltmp110-.Ltmp109       #   Call between .Ltmp109 and .Ltmp110
	.long	.Ltmp111-.Lfunc_begin3  #     jumps to .Ltmp111
	.byte	0                       #   On action: cleanup
	.long	.Ltmp121-.Lfunc_begin3  # >> Call Site 6 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp123-.Lfunc_begin3  #     jumps to .Ltmp123
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin3  # >> Call Site 7 <<
	.long	.Ltmp128-.Ltmp127       #   Call between .Ltmp127 and .Ltmp128
	.long	.Ltmp129-.Lfunc_begin3  #     jumps to .Ltmp129
	.byte	0                       #   On action: cleanup
	.long	.Ltmp128-.Lfunc_begin3  # >> Call Site 8 <<
	.long	.Ltmp124-.Ltmp128       #   Call between .Ltmp128 and .Ltmp124
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp124-.Lfunc_begin3  # >> Call Site 9 <<
	.long	.Ltmp125-.Ltmp124       #   Call between .Ltmp124 and .Ltmp125
	.long	.Ltmp126-.Lfunc_begin3  #     jumps to .Ltmp126
	.byte	1                       #   On action: 1
	.long	.Ltmp106-.Lfunc_begin3  # >> Call Site 10 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin3  #     jumps to .Ltmp108
	.byte	1                       #   On action: 1
	.long	.Ltmp85-.Lfunc_begin3   # >> Call Site 11 <<
	.long	.Ltmp91-.Ltmp85         #   Call between .Ltmp85 and .Ltmp91
	.long	.Ltmp120-.Lfunc_begin3  #     jumps to .Ltmp120
	.byte	1                       #   On action: 1
	.long	.Ltmp95-.Lfunc_begin3   # >> Call Site 12 <<
	.long	.Ltmp96-.Ltmp95         #   Call between .Ltmp95 and .Ltmp96
	.long	.Ltmp97-.Lfunc_begin3   #     jumps to .Ltmp97
	.byte	1                       #   On action: 1
	.long	.Ltmp101-.Lfunc_begin3  # >> Call Site 13 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp120-.Lfunc_begin3  #     jumps to .Ltmp120
	.byte	1                       #   On action: 1
	.long	.Ltmp112-.Lfunc_begin3  # >> Call Site 14 <<
	.long	.Ltmp113-.Ltmp112       #   Call between .Ltmp112 and .Ltmp113
	.long	.Ltmp114-.Lfunc_begin3  #     jumps to .Ltmp114
	.byte	1                       #   On action: 1
	.long	.Ltmp118-.Lfunc_begin3  # >> Call Site 15 <<
	.long	.Ltmp119-.Ltmp118       #   Call between .Ltmp118 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin3  #     jumps to .Ltmp120
	.byte	1                       #   On action: 1
	.long	.Ltmp119-.Lfunc_begin3  # >> Call Site 16 <<
	.long	.Ltmp98-.Ltmp119        #   Call between .Ltmp119 and .Ltmp98
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin3   # >> Call Site 17 <<
	.long	.Ltmp99-.Ltmp98         #   Call between .Ltmp98 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin3  #     jumps to .Ltmp100
	.byte	1                       #   On action: 1
	.long	.Ltmp115-.Lfunc_begin3  # >> Call Site 18 <<
	.long	.Ltmp116-.Ltmp115       #   Call between .Ltmp115 and .Ltmp116
	.long	.Ltmp117-.Lfunc_begin3  #     jumps to .Ltmp117
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress8NImplode8NDecoder6CCoderD0Ev,"axG",@progbits,_ZN9NCompress8NImplode8NDecoder6CCoderD0Ev,comdat
	.weak	_ZN9NCompress8NImplode8NDecoder6CCoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NDecoder6CCoderD0Ev,@function
_ZN9NCompress8NImplode8NDecoder6CCoderD0Ev: # @_ZN9NCompress8NImplode8NDecoder6CCoderD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 32
.Lcfi58:
	.cfi_offset %rbx, -24
.Lcfi59:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp130:
	callq	_ZN9NCompress8NImplode8NDecoder6CCoderD2Ev
.Ltmp131:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB13_2:
.Ltmp132:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZN9NCompress8NImplode8NDecoder6CCoderD0Ev, .Lfunc_end13-_ZN9NCompress8NImplode8NDecoder6CCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp130-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin4  #     jumps to .Ltmp132
	.byte	0                       #   On action: cleanup
	.long	.Ltmp131-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Lfunc_end13-.Ltmp131   #   Call between .Ltmp131 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB14_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB14_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB14_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB14_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB14_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB14_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB14_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB14_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB14_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB14_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB14_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB14_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB14_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB14_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB14_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB14_32
.LBB14_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressSetDecoderProperties2(%rip), %cl
	jne	.LBB14_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+1(%rip), %cl
	jne	.LBB14_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+2(%rip), %cl
	jne	.LBB14_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+3(%rip), %cl
	jne	.LBB14_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+4(%rip), %cl
	jne	.LBB14_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+5(%rip), %cl
	jne	.LBB14_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+6(%rip), %cl
	jne	.LBB14_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+7(%rip), %cl
	jne	.LBB14_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+8(%rip), %cl
	jne	.LBB14_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+9(%rip), %cl
	jne	.LBB14_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+10(%rip), %cl
	jne	.LBB14_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+11(%rip), %cl
	jne	.LBB14_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+12(%rip), %cl
	jne	.LBB14_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+13(%rip), %cl
	jne	.LBB14_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+14(%rip), %cl
	jne	.LBB14_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+15(%rip), %cl
	jne	.LBB14_33
.LBB14_32:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB14_33:                              # %_ZN9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end14:
	.size	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end14-_ZThn8_N9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NImplode8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress8NImplode8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder6AddRefEv,@function
_ZThn8_N9NCompress8NImplode8NDecoder6CCoder6AddRefEv: # @_ZThn8_N9NCompress8NImplode8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end15:
	.size	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder6AddRefEv, .Lfunc_end15-_ZThn8_N9NCompress8NImplode8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NImplode8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress8NImplode8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder7ReleaseEv,@function
_ZThn8_N9NCompress8NImplode8NDecoder6CCoder7ReleaseEv: # @_ZThn8_N9NCompress8NImplode8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB16_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB16_2:                               # %_ZN9NCompress8NImplode8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end16:
	.size	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder7ReleaseEv, .Lfunc_end16-_ZThn8_N9NCompress8NImplode8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NImplode8NDecoder6CCoderD1Ev,"axG",@progbits,_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD1Ev,comdat
	.weak	_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD1Ev,@function
_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD1Ev: # @_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress8NImplode8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end17:
	.size	_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD1Ev, .Lfunc_end17-_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NImplode8NDecoder6CCoderD0Ev,"axG",@progbits,_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD0Ev,comdat
	.weak	_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD0Ev,@function
_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD0Ev: # @_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 32
.Lcfi65:
	.cfi_offset %rbx, -24
.Lcfi66:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp133:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NImplode8NDecoder6CCoderD2Ev
.Ltmp134:
# BB#1:                                 # %_ZN9NCompress8NImplode8NDecoder6CCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB18_2:
.Ltmp135:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end18:
	.size	_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD0Ev, .Lfunc_end18-_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp133-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp135-.Lfunc_begin5  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp134-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Lfunc_end18-.Ltmp134   #   Call between .Ltmp134 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB19_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB19_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB19_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB19_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB19_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB19_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB19_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB19_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB19_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB19_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB19_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB19_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB19_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB19_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB19_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB19_16:
	xorl	%eax, %eax
	retq
.Lfunc_end19:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end19-_ZeqRK4GUIDS1_
	.cfi_endproc

	.type	_ZTVN9NCompress8NImplode8NDecoder6CCoderE,@object # @_ZTVN9NCompress8NImplode8NDecoder6CCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress8NImplode8NDecoder6CCoderE
	.p2align	3
_ZTVN9NCompress8NImplode8NDecoder6CCoderE:
	.quad	0
	.quad	_ZTIN9NCompress8NImplode8NDecoder6CCoderE
	.quad	_ZN9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress8NImplode8NDecoder6CCoder6AddRefEv
	.quad	_ZN9NCompress8NImplode8NDecoder6CCoder7ReleaseEv
	.quad	_ZN9NCompress8NImplode8NDecoder6CCoderD2Ev
	.quad	_ZN9NCompress8NImplode8NDecoder6CCoderD0Ev
	.quad	_ZN9NCompress8NImplode8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.quad	_ZN9NCompress8NImplode8NDecoder6CCoder21SetDecoderProperties2EPKhj
	.quad	-8
	.quad	_ZTIN9NCompress8NImplode8NDecoder6CCoderE
	.quad	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder6AddRefEv
	.quad	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD1Ev
	.quad	_ZThn8_N9NCompress8NImplode8NDecoder6CCoderD0Ev
	.quad	_ZThn8_N9NCompress8NImplode8NDecoder6CCoder21SetDecoderProperties2EPKhj
	.size	_ZTVN9NCompress8NImplode8NDecoder6CCoderE, 136

	.type	_ZTSN9NCompress8NImplode8NDecoder10CExceptionE,@object # @_ZTSN9NCompress8NImplode8NDecoder10CExceptionE
	.section	.rodata._ZTSN9NCompress8NImplode8NDecoder10CExceptionE,"aG",@progbits,_ZTSN9NCompress8NImplode8NDecoder10CExceptionE,comdat
	.weak	_ZTSN9NCompress8NImplode8NDecoder10CExceptionE
	.p2align	4
_ZTSN9NCompress8NImplode8NDecoder10CExceptionE:
	.asciz	"N9NCompress8NImplode8NDecoder10CExceptionE"
	.size	_ZTSN9NCompress8NImplode8NDecoder10CExceptionE, 43

	.type	_ZTIN9NCompress8NImplode8NDecoder10CExceptionE,@object # @_ZTIN9NCompress8NImplode8NDecoder10CExceptionE
	.section	.rodata._ZTIN9NCompress8NImplode8NDecoder10CExceptionE,"aG",@progbits,_ZTIN9NCompress8NImplode8NDecoder10CExceptionE,comdat
	.weak	_ZTIN9NCompress8NImplode8NDecoder10CExceptionE
	.p2align	3
_ZTIN9NCompress8NImplode8NDecoder10CExceptionE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN9NCompress8NImplode8NDecoder10CExceptionE
	.size	_ZTIN9NCompress8NImplode8NDecoder10CExceptionE, 16

	.type	_ZTS19COutBufferException,@object # @_ZTS19COutBufferException
	.section	.rodata._ZTS19COutBufferException,"aG",@progbits,_ZTS19COutBufferException,comdat
	.weak	_ZTS19COutBufferException
	.p2align	4
_ZTS19COutBufferException:
	.asciz	"19COutBufferException"
	.size	_ZTS19COutBufferException, 22

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	_ZTI19COutBufferException,@object # @_ZTI19COutBufferException
	.section	.rodata._ZTI19COutBufferException,"aG",@progbits,_ZTI19COutBufferException,comdat
	.weak	_ZTI19COutBufferException
	.p2align	4
_ZTI19COutBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19COutBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI19COutBufferException, 24

	.type	_ZTSN9NCompress8NImplode8NDecoder6CCoderE,@object # @_ZTSN9NCompress8NImplode8NDecoder6CCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTSN9NCompress8NImplode8NDecoder6CCoderE
	.p2align	4
_ZTSN9NCompress8NImplode8NDecoder6CCoderE:
	.asciz	"N9NCompress8NImplode8NDecoder6CCoderE"
	.size	_ZTSN9NCompress8NImplode8NDecoder6CCoderE, 38

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS30ICompressSetDecoderProperties2,@object # @_ZTS30ICompressSetDecoderProperties2
	.section	.rodata._ZTS30ICompressSetDecoderProperties2,"aG",@progbits,_ZTS30ICompressSetDecoderProperties2,comdat
	.weak	_ZTS30ICompressSetDecoderProperties2
	.p2align	4
_ZTS30ICompressSetDecoderProperties2:
	.asciz	"30ICompressSetDecoderProperties2"
	.size	_ZTS30ICompressSetDecoderProperties2, 33

	.type	_ZTI30ICompressSetDecoderProperties2,@object # @_ZTI30ICompressSetDecoderProperties2
	.section	.rodata._ZTI30ICompressSetDecoderProperties2,"aG",@progbits,_ZTI30ICompressSetDecoderProperties2,comdat
	.weak	_ZTI30ICompressSetDecoderProperties2
	.p2align	4
_ZTI30ICompressSetDecoderProperties2:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS30ICompressSetDecoderProperties2
	.quad	_ZTI8IUnknown
	.size	_ZTI30ICompressSetDecoderProperties2, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress8NImplode8NDecoder6CCoderE,@object # @_ZTIN9NCompress8NImplode8NDecoder6CCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress8NImplode8NDecoder6CCoderE
	.p2align	4
_ZTIN9NCompress8NImplode8NDecoder6CCoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress8NImplode8NDecoder6CCoderE
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI30ICompressSetDecoderProperties2
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTIN9NCompress8NImplode8NDecoder6CCoderE, 72


	.globl	_ZN9NCompress8NImplode8NDecoder6CCoderC1Ev
	.type	_ZN9NCompress8NImplode8NDecoder6CCoderC1Ev,@function
_ZN9NCompress8NImplode8NDecoder6CCoderC1Ev = _ZN9NCompress8NImplode8NDecoder6CCoderC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
