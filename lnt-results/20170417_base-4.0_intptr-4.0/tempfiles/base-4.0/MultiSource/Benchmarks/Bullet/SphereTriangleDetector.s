	.text
	.file	"SphereTriangleDetector.bc"
	.globl	_ZN22SphereTriangleDetectorC2EP13btSphereShapeP15btTriangleShapef
	.p2align	4, 0x90
	.type	_ZN22SphereTriangleDetectorC2EP13btSphereShapeP15btTriangleShapef,@function
_ZN22SphereTriangleDetectorC2EP13btSphereShapeP15btTriangleShapef: # @_ZN22SphereTriangleDetectorC2EP13btSphereShapeP15btTriangleShapef
	.cfi_startproc
# BB#0:
	movq	$_ZTV22SphereTriangleDetector+16, (%rdi)
	movq	%rsi, 8(%rdi)
	movq	%rdx, 16(%rdi)
	movss	%xmm0, 24(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN22SphereTriangleDetectorC2EP13btSphereShapeP15btTriangleShapef, .Lfunc_end0-_ZN22SphereTriangleDetectorC2EP13btSphereShapeP15btTriangleShapef
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN22SphereTriangleDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
	.p2align	4, 0x90
	.type	_ZN22SphereTriangleDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb,@function
_ZN22SphereTriangleDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb: # @_ZN22SphereTriangleDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$240, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 272
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movl	$1065353216, 44(%rsp)   # imm = 0x3F800000
	movl	$0, 8(%rsp)
	movsd	64(%rbx), %xmm10        # xmm10 = mem[0],zero
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 40(%rsp)         # 4-byte Spill
	movss	4(%rbx), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm0
	movaps	%xmm10, %xmm4
	movaps	%xmm10, %xmm2
	mulss	%xmm1, %xmm2
	movss	16(%rbx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movsd	80(%rbx), %xmm11        # xmm11 = mem[0],zero
	movaps	%xmm11, %xmm7
	movaps	%xmm11, %xmm5
	movaps	%xmm11, %xmm3
	mulss	%xmm13, %xmm3
	addss	%xmm2, %xmm3
	movss	32(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 36(%rsp)         # 4-byte Spill
	movsd	96(%rbx), %xmm14        # xmm14 = mem[0],zero
	movaps	%xmm14, %xmm6
	movaps	%xmm14, %xmm15
	movaps	%xmm14, %xmm1
	mulss	%xmm2, %xmm1
	addss	%xmm3, %xmm1
	movaps	%xmm1, 224(%rsp)        # 16-byte Spill
	mulss	%xmm8, %xmm0
	movaps	%xmm8, %xmm12
	movss	%xmm12, 24(%rsp)        # 4-byte Spill
	movss	20(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm7
	movss	%xmm3, 20(%rsp)         # 4-byte Spill
	addss	%xmm0, %xmm7
	movss	36(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm6
	addss	%xmm7, %xmm6
	movaps	%xmm6, 208(%rsp)        # 16-byte Spill
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 28(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm4
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm5
	addss	%xmm4, %xmm5
	movss	40(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm15
	addss	%xmm5, %xmm15
	movaps	%xmm15, 192(%rsp)       # 16-byte Spill
	movss	48(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	subss	112(%rbx), %xmm9
	movaps	%xmm9, 176(%rsp)        # 16-byte Spill
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	mulps	%xmm10, %xmm9
	pshufd	$229, %xmm10, %xmm1     # xmm1 = xmm10[1,1,2,3]
	movss	52(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	subss	116(%rbx), %xmm10
	movaps	%xmm10, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm11, %xmm5
	pshufd	$229, %xmm11, %xmm0     # xmm0 = xmm11[1,1,2,3]
	movss	40(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm11
	mulss	%xmm1, %xmm11
	movaps	%xmm13, %xmm4
	movaps	%xmm4, %xmm6
	mulss	%xmm0, %xmm6
	addss	%xmm11, %xmm6
	movss	56(%rbx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	subss	120(%rbx), %xmm11
	movaps	%xmm11, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm14, %xmm2
	pshufd	$229, %xmm14, %xmm15    # xmm15 = xmm14[1,1,2,3]
	movss	36(%rsp), %xmm14        # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm8
	mulss	%xmm15, %xmm8
	addss	%xmm6, %xmm8
	movaps	%xmm12, %xmm6
	mulss	%xmm1, %xmm6
	mulss	%xmm0, %xmm3
	addss	%xmm6, %xmm3
	movss	32(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm13
	mulss	%xmm15, %xmm13
	addss	%xmm3, %xmm13
	movss	28(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm1
	mulss	16(%rsp), %xmm0         # 4-byte Folded Reload
	addss	%xmm1, %xmm0
	mulss	12(%rsp), %xmm15        # 4-byte Folded Reload
	addss	%xmm0, %xmm15
	movss	72(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm3
	mulss	%xmm0, %xmm3
	movss	88(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	movss	104(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm14
	addss	%xmm4, %xmm14
	movss	24(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	20(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	addss	%xmm4, %xmm7
	movaps	%xmm7, %xmm4
	mulss	%xmm3, %xmm12
	addss	%xmm4, %xmm12
	movaps	%xmm6, %xmm4
	mulss	%xmm0, %xmm4
	movss	16(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	addss	%xmm4, %xmm6
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	addss	%xmm6, %xmm4
	addps	%xmm9, %xmm5
	addps	%xmm5, %xmm2
	movaps	176(%rsp), %xmm5        # 16-byte Reload
	mulss	%xmm0, %xmm5
	mulss	%xmm1, %xmm10
	addss	%xmm5, %xmm10
	mulss	%xmm3, %xmm11
	addss	%xmm10, %xmm11
	xorps	%xmm0, %xmm0
	movss	%xmm11, %xmm0           # xmm0 = xmm11[0],xmm0[1,2,3]
	movaps	224(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 112(%rsp)
	movaps	208(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 116(%rsp)
	movaps	192(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 120(%rsp)
	movl	$0, 124(%rsp)
	movss	%xmm8, 128(%rsp)
	movss	%xmm13, 132(%rsp)
	movss	%xmm15, 136(%rsp)
	movl	$0, 140(%rsp)
	movss	%xmm14, 144(%rsp)
	movss	%xmm12, 148(%rsp)
	movss	%xmm4, 152(%rsp)
	movl	$0, 156(%rsp)
	movlps	%xmm2, 160(%rsp)
	leaq	160(%rsp), %rsi
	movlps	%xmm0, 168(%rsp)
	movss	24(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	leaq	48(%rsp), %rdx
	leaq	96(%rsp), %rcx
	leaq	8(%rsp), %r8
	leaq	44(%rsp), %r9
	callq	_ZN22SphereTriangleDetector7collideERK9btVector3RS0_S3_RfS4_f
	testb	%al, %al
	je	.LBB1_4
# BB#1:
	movss	64(%rbx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	testb	%bpl, %bpl
	je	.LBB1_3
# BB#2:
	movss	100(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movss	80(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm14   # xmm14 = xmm14[0],xmm3[0],xmm14[1],xmm3[1]
	movaps	%xmm0, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm14, %xmm5
	movss	84(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm12   # xmm12 = xmm12[0],xmm7[0],xmm12[1],xmm7[1]
	movaps	%xmm4, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm12, %xmm6
	addps	%xmm5, %xmm6
	movss	88(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm10   # xmm10 = xmm10[0],xmm5[0],xmm10[1],xmm5[1]
	movaps	%xmm13, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm10, %xmm7
	addps	%xmm6, %xmm7
	movss	96(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm0
	movss	100(%rbx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm4
	addss	%xmm0, %xmm4
	movss	104(%rbx), %xmm11       # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm13
	addss	%xmm4, %xmm13
	movaps	.LCPI1_0(%rip), %xmm6   # xmm6 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm7, %xmm4
	xorps	%xmm6, %xmm7
	xorps	%xmm13, %xmm6
	xorps	%xmm5, %xmm5
	xorps	%xmm3, %xmm3
	movss	%xmm6, %xmm3            # xmm3 = xmm6[0],xmm3[1,2,3]
	movlps	%xmm7, 80(%rsp)
	movlps	%xmm3, 88(%rsp)
	movss	48(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	52(%rsp), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm14, %xmm7
	movaps	%xmm6, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm12, %xmm1
	movss	56(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	addps	%xmm7, %xmm1
	movaps	%xmm2, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm10, %xmm7
	addps	%xmm1, %xmm7
	movsd	112(%rbx), %xmm1        # xmm1 = mem[0],zero
	addps	%xmm7, %xmm1
	mulss	%xmm8, %xmm3
	mulss	%xmm9, %xmm6
	addss	%xmm3, %xmm6
	mulss	%xmm11, %xmm2
	addss	%xmm6, %xmm2
	addss	120(%rbx), %xmm2
	mulss	%xmm0, %xmm13
	addps	%xmm1, %xmm4
	addss	%xmm2, %xmm13
	movss	%xmm13, %xmm5           # xmm5 = xmm13[0],xmm5[1,2,3]
	movlps	%xmm4, 64(%rsp)
	movlps	%xmm5, 72(%rsp)
	movq	(%r14), %rax
	leaq	80(%rsp), %rsi
	leaq	64(%rsp), %rdx
	movq	%r14, %rdi
	callq	*32(%rax)
	jmp	.LBB1_4
.LBB1_3:
	movq	(%r14), %rax
	movq	32(%rax), %rax
	movss	100(%rsp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	80(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm14   # xmm14 = xmm14[0],xmm2[0],xmm14[1],xmm2[1]
	movaps	%xmm0, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm14, %xmm5
	movss	84(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0],xmm3[1],xmm6[1]
	movaps	%xmm7, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm3, %xmm6
	addps	%xmm5, %xmm6
	movss	88(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm2, %xmm4
	addps	%xmm6, %xmm4
	movss	96(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm0
	movss	100(%rbx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm7
	addss	%xmm0, %xmm7
	movss	104(%rbx), %xmm10       # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm1
	addss	%xmm7, %xmm1
	xorps	%xmm0, %xmm0
	xorps	%xmm7, %xmm7
	movss	%xmm1, %xmm7            # xmm7 = xmm1[0],xmm7[1,2,3]
	movlps	%xmm4, 80(%rsp)
	movlps	%xmm7, 88(%rsp)
	movss	48(%rsp), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	52(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	56(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm14, %xmm1
	movaps	%xmm5, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm3, %xmm7
	addps	%xmm1, %xmm7
	movaps	%xmm4, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	addps	%xmm7, %xmm1
	movsd	112(%rbx), %xmm2        # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	mulss	%xmm8, %xmm6
	mulss	%xmm9, %xmm5
	addss	%xmm6, %xmm5
	mulss	%xmm10, %xmm4
	addss	%xmm5, %xmm4
	addss	120(%rbx), %xmm4
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm2, 64(%rsp)
	movlps	%xmm0, 72(%rsp)
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	leaq	80(%rsp), %rsi
	leaq	64(%rsp), %rdx
	movq	%r14, %rdi
	callq	*%rax
.LBB1_4:
	addq	$240, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN22SphereTriangleDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb, .Lfunc_end1-_ZN22SphereTriangleDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN22SphereTriangleDetector7collideERK9btVector3RS0_S3_RfS4_f
	.p2align	4, 0x90
	.type	_ZN22SphereTriangleDetector7collideERK9btVector3RS0_S3_RfS4_f,@function
_ZN22SphereTriangleDetector7collideERK9btVector3RS0_S3_RfS4_f: # @_ZN22SphereTriangleDetector7collideERK9btVector3RS0_S3_RfS4_f
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 240
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movq	%r8, %r13
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movq	8(%rbp), %rax
	movq	16(%rbp), %r12
	movss	40(%rax), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	24(%rax), %xmm9
	movss	64(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	80(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm6
	movsd	84(%r12), %xmm0         # xmm0 = mem[0],zero
	movsd	68(%r12), %xmm4         # xmm4 = mem[0],zero
	subps	%xmm4, %xmm0
	movss	100(%r12), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movss	96(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	104(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	pshufd	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	shufps	$0, %xmm4, %xmm3        # xmm3 = xmm3[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	subps	%xmm3, %xmm2
	movaps	%xmm0, %xmm4
	mulps	%xmm2, %xmm4
	movaps	%xmm6, %xmm3
	mulss	%xmm1, %xmm6
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	mulss	%xmm0, %xmm2
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	shufps	$0, %xmm0, %xmm3        # xmm3 = xmm3[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm3      # xmm3 = xmm3[2,0],xmm0[2,3]
	mulps	%xmm3, %xmm1
	subps	%xmm1, %xmm4
	movaps	%xmm4, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm2, %xmm6
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB2_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movaps	%xmm9, 96(%rsp)         # 16-byte Spill
	movaps	%xmm6, 16(%rsp)         # 16-byte Spill
	movaps	%xmm4, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movaps	(%rsp), %xmm4           # 16-byte Reload
	movaps	16(%rsp), %xmm6         # 16-byte Reload
	movaps	96(%rsp), %xmm9         # 16-byte Reload
.LBB2_2:                                # %.split
	movss	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm4, %xmm7
	movaps	%xmm7, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm6, %xmm1
	xorps	%xmm6, %xmm6
	movss	%xmm1, %xmm6            # xmm6 = xmm1[0],xmm6[1,2,3]
	movq	(%rbx), %xmm10          # xmm10 = mem[0],zero
	pshufd	$229, %xmm10, %xmm2     # xmm2 = xmm10[1,1,2,3]
	movss	64(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movdqa	%xmm10, %xmm4
	subss	%xmm3, %xmm4
	subss	68(%r12), %xmm2
	movss	8(%rbx), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm5
	subss	72(%r12), %xmm5
	mulss	%xmm7, %xmm4
	mulss	%xmm0, %xmm2
	addss	%xmm4, %xmm2
	mulss	%xmm1, %xmm5
	addss	%xmm2, %xmm5
	xorps	%xmm0, %xmm0
	ucomiss	%xmm5, %xmm0
	jbe	.LBB2_4
# BB#3:
	movaps	.LCPI2_1(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm5
	xorps	%xmm2, %xmm7
	xorps	%xmm2, %xmm1
	movss	%xmm1, %xmm6            # xmm6 = xmm1[0],xmm6[1,2,3]
.LBB2_4:
	movss	32(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	addss	%xmm9, %xmm3
	movaps	%xmm7, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm7, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	mulss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm6, %xmm1
	mulss	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	ucomiss	%xmm5, %xmm9
	seta	%al
	ucomiss	%xmm0, %xmm1
	setb	%cl
	xorl	%r14d, %r14d
	movss	%xmm3, 32(%rsp)         # 4-byte Spill
	ucomiss	%xmm5, %xmm3
	jbe	.LBB2_32
# BB#5:
	orb	%cl, %al
	je	.LBB2_32
# BB#6:
	movaps	%xmm5, (%rsp)           # 16-byte Spill
	movss	%xmm8, 16(%rsp)         # 4-byte Spill
	movdqa	%xmm10, 80(%rsp)        # 16-byte Spill
	movaps	%xmm9, 96(%rsp)         # 16-byte Spill
	addq	$64, %r12
	movups	(%rbx), %xmm0
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm7, %xmm0
	unpcklpd	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0]
	movupd	%xmm0, 136(%rsp)
	leaq	136(%rsp), %rdx
	leaq	112(%rsp), %rcx
	movq	%r12, %rsi
	movaps	%xmm7, 160(%rsp)        # 16-byte Spill
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	callq	_ZN22SphereTriangleDetector15pointInTriangleEPK9btVector3RS1_PS0_
	testb	%al, %al
	je	.LBB2_8
# BB#7:                                 # %.thread212
	movaps	(%rsp), %xmm1           # 16-byte Reload
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	160(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	mulss	%xmm1, %xmm2
	movaps	80(%rsp), %xmm5         # 16-byte Reload
	movaps	%xmm5, %xmm8
	subps	%xmm0, %xmm8
	movss	16(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	subss	%xmm2, %xmm0
	xorps	%xmm3, %xmm3
	movss	%xmm0, %xmm3            # xmm3 = xmm0[0],xmm3[1,2,3]
	jmp	.LBB2_22
.LBB2_8:
	movq	16(%rbp), %rdi
	movq	(%rdi), %rax
	callq	*152(%rax)
	xorl	%r14d, %r14d
	testl	%eax, %eax
	jle	.LBB2_32
# BB#9:                                 # %.lr.ph
	movq	%r13, 64(%rsp)          # 8-byte Spill
	movq	%r15, 80(%rsp)          # 8-byte Spill
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	leaq	136(%rsp), %r15
	xorl	%r13d, %r13d
                                        # implicit-def: %XMM0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
                                        # implicit-def: %XMM0
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	.p2align	4, 0x90
.LBB2_10:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rdi
	movq	(%rdi), %rax
	movl	%r14d, %esi
	leaq	112(%rsp), %rdx
	movq	%r15, %rcx
	callq	*160(%rax)
	movsd	(%rbx), %xmm4           # xmm4 = mem[0],zero
	movsd	112(%rsp), %xmm9        # xmm9 = mem[0],zero
	subps	%xmm9, %xmm4
	movaps	%xmm4, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	120(%rsp), %xmm8        # xmm8 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm0
	xorps	%xmm6, %xmm6
	movss	%xmm0, %xmm6            # xmm6 = xmm0[0],xmm6[1,2,3]
	movsd	136(%rsp), %xmm2        # xmm2 = mem[0],zero
	subps	%xmm9, %xmm2
	movaps	%xmm2, %xmm10
	shufps	$229, %xmm10, %xmm10    # xmm10 = xmm10[1,1,2,3]
	movss	144(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm5
	movaps	%xmm4, %xmm7
	mulss	%xmm2, %xmm7
	movaps	%xmm10, %xmm3
	mulss	%xmm1, %xmm3
	addss	%xmm7, %xmm3
	movaps	%xmm0, %xmm7
	mulss	%xmm5, %xmm7
	addss	%xmm3, %xmm7
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm7
	jbe	.LBB2_14
# BB#11:                                #   in Loop: Header=BB2_10 Depth=1
	movaps	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	movaps	%xmm10, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm1, %xmm3
	movaps	%xmm5, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	ucomiss	%xmm7, %xmm1
	jbe	.LBB2_13
# BB#12:                                #   in Loop: Header=BB2_10 Depth=1
	divss	%xmm1, %xmm7
	movaps	%xmm7, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movaps	%xmm2, %xmm3
	mulps	%xmm1, %xmm3
	movaps	%xmm5, %xmm1
	mulss	%xmm7, %xmm1
	subps	%xmm3, %xmm4
	subss	%xmm1, %xmm0
	movss	%xmm0, %xmm6            # xmm6 = xmm0[0],xmm6[1,2,3]
	movaps	%xmm7, %xmm1
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_13:                               #   in Loop: Header=BB2_10 Depth=1
	subps	%xmm2, %xmm4
	subss	%xmm5, %xmm0
	movss	%xmm0, %xmm6            # xmm6 = xmm0[0],xmm6[1,2,3]
	movss	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
.LBB2_14:                               # %_Z18SegmentSqrDistanceRK9btVector3S1_S1_RS_.exit
                                        #   in Loop: Header=BB2_10 Depth=1
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	mulss	%xmm4, %xmm4
	addss	%xmm0, %xmm4
	mulss	%xmm6, %xmm6
	addss	%xmm4, %xmm6
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm0
	jbe	.LBB2_16
# BB#15:                                #   in Loop: Header=BB2_10 Depth=1
	mulss	%xmm1, %xmm2
	mulss	%xmm1, %xmm10
	mulss	%xmm1, %xmm5
	addss	%xmm9, %xmm2
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	addss	%xmm10, %xmm9
	addss	%xmm5, %xmm8
	unpcklps	%xmm9, %xmm2    # xmm2 = xmm2[0],xmm9[0],xmm2[1],xmm9[1]
	xorps	%xmm0, %xmm0
	movss	%xmm8, %xmm0            # xmm0 = xmm8[0],xmm0[1,2,3]
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movaps	%xmm2, (%rsp)           # 16-byte Spill
.LBB2_16:                               # %_Z18SegmentSqrDistanceRK9btVector3S1_S1_RS_.exit
                                        #   in Loop: Header=BB2_10 Depth=1
	movb	$1, %r12b
	ja	.LBB2_18
# BB#17:                                # %_Z18SegmentSqrDistanceRK9btVector3S1_S1_RS_.exit
                                        #   in Loop: Header=BB2_10 Depth=1
	movl	%r13d, %r12d
.LBB2_18:                               # %_Z18SegmentSqrDistanceRK9btVector3S1_S1_RS_.exit
                                        #   in Loop: Header=BB2_10 Depth=1
	incl	%r14d
	movq	16(%rbp), %rdi
	movq	(%rdi), %rax
	callq	*152(%rax)
	cmpl	%eax, %r14d
	movb	%r12b, %r13b
	jl	.LBB2_10
# BB#19:                                # %._crit_edge
	testb	$1, %r12b
	je	.LBB2_31
# BB#21:                                # %._crit_edge._crit_edge
	movsd	(%rbx), %xmm5           # xmm5 = mem[0],zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movq	80(%rsp), %r15          # 8-byte Reload
	movq	64(%rsp), %r13          # 8-byte Reload
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	movaps	(%rsp), %xmm8           # 16-byte Reload
.LBB2_22:
	subps	%xmm8, %xmm5
	movaps	%xmm5, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	subss	%xmm3, %xmm6
	movaps	%xmm5, %xmm1
	mulss	%xmm1, %xmm1
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm6, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm4, %xmm0
	movaps	96(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	movq	152(%rsp), %rbx         # 8-byte Reload
	jbe	.LBB2_28
# BB#23:
	xorps	%xmm1, %xmm1
	movss	%xmm6, %xmm1            # xmm1 = xmm6[0],xmm1[1,2,3]
	xorps	%xmm6, %xmm6
	sqrtss	%xmm0, %xmm6
	ucomiss	%xmm6, %xmm6
	jnp	.LBB2_25
# BB#24:                                # %call.sqrt226
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	movaps	%xmm8, (%rsp)           # 16-byte Spill
	movaps	%xmm5, 80(%rsp)         # 16-byte Spill
	movaps	%xmm7, 32(%rsp)         # 16-byte Spill
	movss	%xmm4, 56(%rsp)         # 4-byte Spill
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	movss	56(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm7         # 16-byte Reload
	movaps	80(%rsp), %xmm5         # 16-byte Reload
	movaps	(%rsp), %xmm8           # 16-byte Reload
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	movaps	96(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm0, %xmm6
.LBB2_25:                               # %.split225
	movlps	%xmm5, (%rbx)
	movlps	%xmm1, 8(%rbx)
	mulss	%xmm5, %xmm5
	mulss	%xmm7, %xmm7
	addss	%xmm5, %xmm7
	addss	%xmm4, %xmm7
	xorps	%xmm0, %xmm0
	sqrtss	%xmm7, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB2_27
# BB#26:                                # %call.sqrt227
	movaps	%xmm7, %xmm0
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	movaps	%xmm8, (%rsp)           # 16-byte Spill
	movss	%xmm6, 32(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	32(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	(%rsp), %xmm8           # 16-byte Reload
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	movaps	96(%rsp), %xmm2         # 16-byte Reload
.LBB2_27:                               # %.split225.split
	movss	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movss	%xmm0, (%rbx)
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movss	%xmm0, 4(%rbx)
	mulss	8(%rbx), %xmm1
	movss	%xmm1, 8(%rbx)
	movlps	%xmm8, (%r15)
	movlps	%xmm3, 8(%r15)
	subss	%xmm6, %xmm2
	xorps	.LCPI2_1(%rip), %xmm2
	movss	%xmm2, (%r13)
	jmp	.LBB2_30
.LBB2_28:
	xorps	%xmm0, %xmm0
	mulss	%xmm0, %xmm5
	mulss	%xmm0, %xmm7
	addss	%xmm5, %xmm7
	mulss	%xmm0, %xmm6
	addss	%xmm7, %xmm6
	ucomiss	%xmm0, %xmm6
	jae	.LBB2_31
# BB#29:
	movlps	%xmm8, (%r15)
	movlps	%xmm3, 8(%r15)
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax)
.LBB2_30:                               # %.thread
	movb	$1, %r14b
	jmp	.LBB2_32
.LBB2_31:
	xorl	%r14d, %r14d
.LBB2_32:                               # %.thread
	movl	%r14d, %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN22SphereTriangleDetector7collideERK9btVector3RS0_S3_RfS4_f, .Lfunc_end2-_ZN22SphereTriangleDetector7collideERK9btVector3RS0_S3_RfS4_f
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	1065353216              # float 1
	.text
	.globl	_Z18SegmentSqrDistanceRK9btVector3S1_S1_RS_
	.p2align	4, 0x90
	.type	_Z18SegmentSqrDistanceRK9btVector3S1_S1_RS_,@function
_Z18SegmentSqrDistanceRK9btVector3S1_S1_RS_: # @_Z18SegmentSqrDistanceRK9btVector3S1_S1_RS_
	.cfi_startproc
# BB#0:
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	movsd	(%rdi), %xmm11          # xmm11 = mem[0],zero
	subps	%xmm11, %xmm1
	movaps	%xmm1, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movss	8(%rdx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm9          # xmm9 = mem[0],zero,zero,zero
	subss	%xmm9, %xmm4
	xorps	%xmm8, %xmm8
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movsd	(%rsi), %xmm5           # xmm5 = mem[0],zero
	subps	%xmm11, %xmm5
	movaps	%xmm5, %xmm10
	shufps	$229, %xmm10, %xmm10    # xmm10 = xmm10[1,1,2,3]
	movss	8(%rsi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	subss	%xmm9, %xmm6
	movaps	%xmm1, %xmm7
	mulss	%xmm5, %xmm7
	movaps	%xmm10, %xmm2
	mulss	%xmm3, %xmm2
	addss	%xmm7, %xmm2
	movaps	%xmm4, %xmm3
	mulss	%xmm6, %xmm3
	addss	%xmm2, %xmm3
	xorps	%xmm7, %xmm7
	ucomiss	%xmm7, %xmm3
	jbe	.LBB3_4
# BB#1:
	movaps	%xmm5, %xmm7
	mulss	%xmm7, %xmm7
	movaps	%xmm10, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm7, %xmm2
	movaps	%xmm6, %xmm7
	mulss	%xmm7, %xmm7
	addss	%xmm2, %xmm7
	ucomiss	%xmm3, %xmm7
	jbe	.LBB3_3
# BB#2:
	divss	%xmm7, %xmm3
	movaps	%xmm3, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movaps	%xmm5, %xmm7
	mulps	%xmm2, %xmm7
	movaps	%xmm6, %xmm2
	mulss	%xmm3, %xmm2
	subps	%xmm7, %xmm1
	subss	%xmm2, %xmm4
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movaps	%xmm3, %xmm7
	jmp	.LBB3_4
.LBB3_3:
	subps	%xmm5, %xmm1
	subss	%xmm6, %xmm4
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movss	.LCPI3_0(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
.LBB3_4:
	mulss	%xmm7, %xmm5
	mulss	%xmm7, %xmm10
	mulss	%xmm7, %xmm6
	addss	%xmm11, %xmm5
	shufps	$229, %xmm11, %xmm11    # xmm11 = xmm11[1,1,2,3]
	addss	%xmm10, %xmm11
	addss	%xmm9, %xmm6
	unpcklps	%xmm11, %xmm5   # xmm5 = xmm5[0],xmm11[0],xmm5[1],xmm11[1]
	movss	%xmm6, %xmm8            # xmm8 = xmm6[0],xmm8[1,2,3]
	movlps	%xmm5, (%rcx)
	movlps	%xmm8, 8(%rcx)
	movaps	%xmm1, %xmm2
	mulss	%xmm2, %xmm2
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	retq
.Lfunc_end3:
	.size	_Z18SegmentSqrDistanceRK9btVector3S1_S1_RS_, .Lfunc_end3-_Z18SegmentSqrDistanceRK9btVector3S1_S1_RS_
	.cfi_endproc

	.globl	_ZN22SphereTriangleDetector12facecontainsERK9btVector3PS1_RS0_
	.p2align	4, 0x90
	.type	_ZN22SphereTriangleDetector12facecontainsERK9btVector3PS1_RS0_,@function
_ZN22SphereTriangleDetector12facecontainsERK9btVector3PS1_RS0_: # @_ZN22SphereTriangleDetector12facecontainsERK9btVector3PS1_RS0_
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 48
	movups	(%rsi), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	(%rcx), %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rax
	leaq	16(%rsp), %rcx
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	_ZN22SphereTriangleDetector15pointInTriangleEPK9btVector3RS1_PS0_
	addq	$40, %rsp
	retq
.Lfunc_end4:
	.size	_ZN22SphereTriangleDetector12facecontainsERK9btVector3PS1_RS0_, .Lfunc_end4-_ZN22SphereTriangleDetector12facecontainsERK9btVector3PS1_RS0_
	.cfi_endproc

	.globl	_ZN22SphereTriangleDetector15pointInTriangleEPK9btVector3RS1_PS0_
	.p2align	4, 0x90
	.type	_ZN22SphereTriangleDetector15pointInTriangleEPK9btVector3RS1_PS0_,@function
_ZN22SphereTriangleDetector15pointInTriangleEPK9btVector3RS1_PS0_: # @_ZN22SphereTriangleDetector15pointInTriangleEPK9btVector3RS1_PS0_
	.cfi_startproc
# BB#0:
	movss	16(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	32(%rsi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm12
	subss	%xmm2, %xmm12
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	subss	%xmm2, %xmm0
	movss	%xmm0, -4(%rsp)         # 4-byte Spill
	subss	%xmm9, %xmm2
	movss	20(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	36(%rsi), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm10
	subss	%xmm1, %xmm10
	movss	4(%rcx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	subss	%xmm1, %xmm0
	movss	%xmm0, -16(%rsp)        # 4-byte Spill
	movaps	%xmm1, %xmm0
	subss	%xmm4, %xmm0
	movss	24(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	40(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	subss	%xmm1, %xmm3
	movaps	%xmm5, %xmm7
	subss	%xmm9, %xmm7
	movss	%xmm7, -24(%rsp)        # 4-byte Spill
	subss	%xmm11, %xmm9
	subss	%xmm11, %xmm5
	movss	%xmm5, -8(%rsp)         # 4-byte Spill
	movaps	%xmm6, %xmm5
	subss	%xmm4, %xmm5
	movss	%xmm5, -28(%rsp)        # 4-byte Spill
	subss	%xmm13, %xmm4
	subss	%xmm13, %xmm6
	movss	%xmm6, -12(%rsp)        # 4-byte Spill
	movss	8(%rcx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm5
	subss	%xmm1, %xmm5
	movss	%xmm5, -20(%rsp)        # 4-byte Spill
	movss	8(%rsi), %xmm7          # xmm7 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm1
	movaps	%xmm6, %xmm5
	subss	%xmm7, %xmm5
	movss	%xmm5, -32(%rsp)        # 4-byte Spill
	subss	%xmm8, %xmm7
	subss	%xmm8, %xmm6
	movss	8(%rdx), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm15
	mulss	%xmm13, %xmm15
	movss	4(%rdx), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm5
	mulss	%xmm11, %xmm5
	subss	%xmm5, %xmm15
	movss	(%rdx), %xmm14          # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm1
	movaps	%xmm2, %xmm5
	mulss	%xmm13, %xmm5
	subss	%xmm5, %xmm1
	mulss	%xmm11, %xmm2
	mulss	%xmm14, %xmm0
	subss	%xmm0, %xmm2
	movaps	%xmm10, %xmm8
	mulss	%xmm13, %xmm8
	movaps	%xmm3, %xmm0
	mulss	%xmm11, %xmm0
	subss	%xmm0, %xmm8
	mulss	%xmm14, %xmm3
	movaps	%xmm12, %xmm0
	mulss	%xmm13, %xmm0
	subss	%xmm0, %xmm3
	mulss	%xmm11, %xmm12
	mulss	%xmm14, %xmm10
	subss	%xmm10, %xmm12
	movaps	%xmm4, %xmm0
	mulss	%xmm13, %xmm0
	movaps	%xmm7, %xmm5
	mulss	%xmm11, %xmm5
	subss	%xmm5, %xmm0
	mulss	%xmm14, %xmm7
	mulss	%xmm9, %xmm13
	subss	%xmm13, %xmm7
	mulss	%xmm11, %xmm9
	mulss	%xmm14, %xmm4
	subss	%xmm4, %xmm9
	mulss	-24(%rsp), %xmm15       # 4-byte Folded Reload
	mulss	-28(%rsp), %xmm1        # 4-byte Folded Reload
	addss	%xmm15, %xmm1
	mulss	-32(%rsp), %xmm2        # 4-byte Folded Reload
	addss	%xmm1, %xmm2
	mulss	-4(%rsp), %xmm8         # 4-byte Folded Reload
	mulss	-16(%rsp), %xmm3        # 4-byte Folded Reload
	addss	%xmm8, %xmm3
	mulss	-20(%rsp), %xmm12       # 4-byte Folded Reload
	addss	%xmm3, %xmm12
	mulss	-8(%rsp), %xmm0         # 4-byte Folded Reload
	mulss	-12(%rsp), %xmm7        # 4-byte Folded Reload
	addss	%xmm0, %xmm7
	mulss	%xmm6, %xmm9
	addss	%xmm7, %xmm9
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm9
	jbe	.LBB5_3
# BB#1:
	ucomiss	%xmm0, %xmm2
	jbe	.LBB5_3
# BB#2:
	movb	$1, %al
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm12
	ja	.LBB5_4
.LBB5_3:
	ucomiss	%xmm2, %xmm0
	setae	%al
	ucomiss	%xmm12, %xmm0
	setae	%cl
	andb	%al, %cl
	ucomiss	%xmm9, %xmm0
	setae	%al
	andb	%cl, %al
.LBB5_4:
	retq
.Lfunc_end5:
	.size	_ZN22SphereTriangleDetector15pointInTriangleEPK9btVector3RS1_PS0_, .Lfunc_end5-_ZN22SphereTriangleDetector15pointInTriangleEPK9btVector3RS1_PS0_
	.cfi_endproc

	.section	.text._ZN36btDiscreteCollisionDetectorInterfaceD2Ev,"axG",@progbits,_ZN36btDiscreteCollisionDetectorInterfaceD2Ev,comdat
	.weak	_ZN36btDiscreteCollisionDetectorInterfaceD2Ev
	.p2align	4, 0x90
	.type	_ZN36btDiscreteCollisionDetectorInterfaceD2Ev,@function
_ZN36btDiscreteCollisionDetectorInterfaceD2Ev: # @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end6:
	.size	_ZN36btDiscreteCollisionDetectorInterfaceD2Ev, .Lfunc_end6-_ZN36btDiscreteCollisionDetectorInterfaceD2Ev
	.cfi_endproc

	.section	.text._ZN22SphereTriangleDetectorD0Ev,"axG",@progbits,_ZN22SphereTriangleDetectorD0Ev,comdat
	.weak	_ZN22SphereTriangleDetectorD0Ev
	.p2align	4, 0x90
	.type	_ZN22SphereTriangleDetectorD0Ev,@function
_ZN22SphereTriangleDetectorD0Ev:        # @_ZN22SphereTriangleDetectorD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end7:
	.size	_ZN22SphereTriangleDetectorD0Ev, .Lfunc_end7-_ZN22SphereTriangleDetectorD0Ev
	.cfi_endproc

	.type	_ZTV22SphereTriangleDetector,@object # @_ZTV22SphereTriangleDetector
	.section	.rodata,"a",@progbits
	.globl	_ZTV22SphereTriangleDetector
	.p2align	3
_ZTV22SphereTriangleDetector:
	.quad	0
	.quad	_ZTI22SphereTriangleDetector
	.quad	_ZN36btDiscreteCollisionDetectorInterfaceD2Ev
	.quad	_ZN22SphereTriangleDetectorD0Ev
	.quad	_ZN22SphereTriangleDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
	.size	_ZTV22SphereTriangleDetector, 40

	.type	_ZTS22SphereTriangleDetector,@object # @_ZTS22SphereTriangleDetector
	.globl	_ZTS22SphereTriangleDetector
	.p2align	4
_ZTS22SphereTriangleDetector:
	.asciz	"22SphereTriangleDetector"
	.size	_ZTS22SphereTriangleDetector, 25

	.type	_ZTS36btDiscreteCollisionDetectorInterface,@object # @_ZTS36btDiscreteCollisionDetectorInterface
	.section	.rodata._ZTS36btDiscreteCollisionDetectorInterface,"aG",@progbits,_ZTS36btDiscreteCollisionDetectorInterface,comdat
	.weak	_ZTS36btDiscreteCollisionDetectorInterface
	.p2align	4
_ZTS36btDiscreteCollisionDetectorInterface:
	.asciz	"36btDiscreteCollisionDetectorInterface"
	.size	_ZTS36btDiscreteCollisionDetectorInterface, 39

	.type	_ZTI36btDiscreteCollisionDetectorInterface,@object # @_ZTI36btDiscreteCollisionDetectorInterface
	.section	.rodata._ZTI36btDiscreteCollisionDetectorInterface,"aG",@progbits,_ZTI36btDiscreteCollisionDetectorInterface,comdat
	.weak	_ZTI36btDiscreteCollisionDetectorInterface
	.p2align	3
_ZTI36btDiscreteCollisionDetectorInterface:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS36btDiscreteCollisionDetectorInterface
	.size	_ZTI36btDiscreteCollisionDetectorInterface, 16

	.type	_ZTI22SphereTriangleDetector,@object # @_ZTI22SphereTriangleDetector
	.section	.rodata,"a",@progbits
	.globl	_ZTI22SphereTriangleDetector
	.p2align	4
_ZTI22SphereTriangleDetector:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22SphereTriangleDetector
	.quad	_ZTI36btDiscreteCollisionDetectorInterface
	.size	_ZTI22SphereTriangleDetector, 24


	.globl	_ZN22SphereTriangleDetectorC1EP13btSphereShapeP15btTriangleShapef
	.type	_ZN22SphereTriangleDetectorC1EP13btSphereShapeP15btTriangleShapef,@function
_ZN22SphereTriangleDetectorC1EP13btSphereShapeP15btTriangleShapef = _ZN22SphereTriangleDetectorC2EP13btSphereShapeP15btTriangleShapef
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
