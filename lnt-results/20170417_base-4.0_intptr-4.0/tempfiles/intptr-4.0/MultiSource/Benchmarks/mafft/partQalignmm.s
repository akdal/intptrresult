	.text
	.file	"partQalignmm.bc"
	.globl	part_imp_match_out_scQ
	.p2align	4, 0x90
	.type	part_imp_match_out_scQ,@function
part_imp_match_out_scQ:                 # @part_imp_match_out_scQ
	.cfi_startproc
# BB#0:
	movq	impmtx(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	%esi, %rcx
	movss	(%rax,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end0:
	.size	part_imp_match_out_scQ, .Lfunc_end0-part_imp_match_out_scQ
	.cfi_endproc

	.globl	part_imp_match_init_strictQ
	.p2align	4, 0x90
	.type	part_imp_match_init_strictQ,@function
part_imp_match_init_strictQ:            # @part_imp_match_init_strictQ
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movl	%r8d, %ebp
	movl	%ecx, %r12d
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%esi, %r14d
	movl	impalloclen(%rip), %eax
	leal	2(%r12), %ecx
	cmpl	%ecx, %eax
	jle	.LBB1_2
# BB#1:
	leal	2(%rbp), %ecx
	cmpl	%ecx, %eax
	jg	.LBB1_5
.LBB1_2:
	movq	impmtx(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	callq	FreeFloatMtx
.LBB1_4:
	cmpl	%ebp, %r12d
	movl	%ebp, %edi
	cmovgel	%r12d, %edi
	leal	2(%rdi), %eax
	movl	%eax, impalloclen(%rip)
	addl	$102, %edi
	movl	%edi, %esi
	callq	AllocateFloatMtx
	movq	%rax, impmtx(%rip)
.LBB1_5:                                # %.preheader174
	testl	%r12d, %r12d
	jle	.LBB1_12
# BB#6:                                 # %.preheader173.lr.ph
	testl	%ebp, %ebp
	jle	.LBB1_12
# BB#7:                                 # %.preheader173.us.preheader
	movl	%r14d, 24(%rsp)         # 4-byte Spill
	movq	impmtx(%rip), %r13
	decl	%ebp
	leaq	4(,%rbp,4), %rbp
	movl	%r12d, %r15d
	leaq	-1(%r15), %r12
	movq	%r15, %r14
	xorl	%ebx, %ebx
	andq	$7, %r14
	je	.LBB1_9
	.p2align	4, 0x90
.LBB1_8:                                # %.preheader173.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB1_8
.LBB1_9:                                # %.preheader173.us.prol.loopexit
	cmpq	$7, %r12
	movl	24(%rsp), %r14d         # 4-byte Reload
	jb	.LBB1_12
# BB#10:                                # %.preheader173.us.preheader.new
	subq	%rbx, %r15
	leaq	56(%r13,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_11:                               # %.preheader173.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-16(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	addq	$64, %rbx
	addq	$-8, %r15
	jne	.LBB1_11
.LBB1_12:                               # %._crit_edge186
	testl	%r14d, %r14d
	jle	.LBB1_50
# BB#13:                                # %.preheader172.lr.ph
	movsd	fastathreshold(%rip), %xmm0 # xmm0 = mem[0],zero
	movq	impmtx(%rip), %rdi
	movl	12(%rsp), %eax          # 4-byte Reload
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%r14d, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_14:                               # %.preheader172
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_16 Depth 2
                                        #       Child Loop BB1_18 Depth 3
                                        #         Child Loop BB1_19 Depth 4
                                        #         Child Loop BB1_23 Depth 4
                                        #         Child Loop BB1_27 Depth 4
                                        #         Child Loop BB1_30 Depth 4
                                        #         Child Loop BB1_34 Depth 4
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_49
# BB#15:                                # %.lr.ph178
                                        #   in Loop: Header=BB1_14 Depth=1
	movq	120(%rsp), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movsd	(%rax,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movq	136(%rsp), %rax
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_16:                               #   Parent Loop BB1_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_18 Depth 3
                                        #         Child Loop BB1_19 Depth 4
                                        #         Child Loop BB1_23 Depth 4
                                        #         Child Loop BB1_27 Depth 4
                                        #         Child Loop BB1_30 Depth 4
                                        #         Child Loop BB1_34 Depth 4
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %r14
	testq	%r14, %r14
	je	.LBB1_48
# BB#17:                                # %.lr.ph
                                        #   in Loop: Header=BB1_16 Depth=2
	movq	128(%rsp), %rax
	movsd	(%rax,%r12,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %r13
	.p2align	4, 0x90
.LBB1_18:                               #   Parent Loop BB1_14 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_19 Depth 4
                                        #         Child Loop BB1_23 Depth 4
                                        #         Child Loop BB1_27 Depth 4
                                        #         Child Loop BB1_30 Depth 4
                                        #         Child Loop BB1_34 Depth 4
	movl	$-1, %eax
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB1_19:                               #   Parent Loop BB1_14 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        #       Parent Loop BB1_18 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB1_20
# BB#21:                                #   in Loop: Header=BB1_19 Depth=4
	incq	%rbp
	xorl	%edx, %edx
	cmpb	$45, %cl
	setne	%dl
	addl	%edx, %eax
	cmpl	24(%r14), %eax
	movl	%eax, %ecx
	jne	.LBB1_19
	jmp	.LBB1_22
	.p2align	4, 0x90
.LBB1_20:                               # %._crit_edge203
                                        #   in Loop: Header=BB1_18 Depth=3
	movl	24(%r14), %ecx
.LBB1_22:                               # %.loopexit
                                        #   in Loop: Header=BB1_18 Depth=3
	movl	%ebp, %edx
	subl	%r13d, %edx
	decl	%edx
	movl	28(%r14), %esi
	cmpl	%esi, %ecx
	movl	%edx, %r8d
	je	.LBB1_26
	.p2align	4, 0x90
.LBB1_23:                               # %.preheader171
                                        #   Parent Loop BB1_14 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        #       Parent Loop BB1_18 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB1_25
# BB#24:                                #   in Loop: Header=BB1_23 Depth=4
	incq	%rbp
	xorl	%ebx, %ebx
	cmpb	$45, %cl
	setne	%bl
	addl	%ebx, %eax
	cmpl	%esi, %eax
	jne	.LBB1_23
.LBB1_25:                               #   in Loop: Header=BB1_18 Depth=3
	subl	%r13d, %ebp
	decl	%ebp
	movl	%ebp, %r8d
.LBB1_26:                               #   in Loop: Header=BB1_18 Depth=3
	movq	112(%rsp), %rax
	movq	(%rax,%r12,8), %r9
	movl	$-1, %eax
	movq	%r9, %rbp
	.p2align	4, 0x90
.LBB1_27:                               #   Parent Loop BB1_14 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        #       Parent Loop BB1_18 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB1_29
# BB#28:                                #   in Loop: Header=BB1_27 Depth=4
	incq	%rbp
	xorl	%esi, %esi
	cmpb	$45, %cl
	setne	%sil
	addl	%esi, %eax
	cmpl	32(%r14), %eax
	jne	.LBB1_27
.LBB1_29:                               #   in Loop: Header=BB1_18 Depth=3
	movl	%ebp, %r15d
	subl	%r9d, %r15d
	decl	%r15d
	movl	36(%r14), %esi
	cmpl	%esi, 32(%r14)
	movl	%r15d, %r10d
	je	.LBB1_33
	.p2align	4, 0x90
.LBB1_30:                               # %.preheader
                                        #   Parent Loop BB1_14 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        #       Parent Loop BB1_18 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB1_32
# BB#31:                                #   in Loop: Header=BB1_30 Depth=4
	incq	%rbp
	xorl	%ebx, %ebx
	cmpb	$45, %cl
	setne	%bl
	addl	%ebx, %eax
	cmpl	%esi, %eax
	jne	.LBB1_30
.LBB1_32:                               #   in Loop: Header=BB1_18 Depth=3
	subl	%r9d, %ebp
	decl	%ebp
	movl	%ebp, %r10d
.LBB1_33:                               #   in Loop: Header=BB1_18 Depth=3
	movslq	%edx, %r11
	addq	%r13, %r11
	movslq	%r15d, %rax
	addq	%rax, %r9
	jmp	.LBB1_34
	.p2align	4, 0x90
.LBB1_41:                               #   in Loop: Header=BB1_34 Depth=4
	cmpb	$45, %cl
	jne	.LBB1_45
# BB#42:                                #   in Loop: Header=BB1_34 Depth=4
	incl	%edx
	incq	%r11
	cmpb	$45, %al
	je	.LBB1_43
	jmp	.LBB1_45
	.p2align	4, 0x90
.LBB1_34:                               #   Parent Loop BB1_14 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        #       Parent Loop BB1_18 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%r11), %ecx
	testb	%cl, %cl
	je	.LBB1_47
# BB#35:                                #   in Loop: Header=BB1_34 Depth=4
	movzbl	(%r9), %eax
	testb	%al, %al
	je	.LBB1_47
# BB#36:                                #   in Loop: Header=BB1_34 Depth=4
	cmpb	$45, %cl
	je	.LBB1_39
# BB#37:                                #   in Loop: Header=BB1_34 Depth=4
	cmpb	$45, %al
	je	.LBB1_39
# BB#38:                                #   in Loop: Header=BB1_34 Depth=4
	movss	64(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm2, %xmm3
	movslq	%edx, %rdx
	movq	(%rdi,%rdx,8), %rax
	movslq	%r15d, %r15
	movss	(%rax,%r15,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm3, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rax,%r15,4)
	incl	%edx
	incl	%r15d
	incq	%r11
	jmp	.LBB1_44
	.p2align	4, 0x90
.LBB1_39:                               #   in Loop: Header=BB1_34 Depth=4
	cmpb	$45, %al
	setne	%bl
	cmpb	$45, %cl
	je	.LBB1_41
# BB#40:                                #   in Loop: Header=BB1_34 Depth=4
	testb	%bl, %bl
	jne	.LBB1_41
.LBB1_43:                               #   in Loop: Header=BB1_34 Depth=4
	incl	%r15d
.LBB1_44:                               # %.thread
                                        #   in Loop: Header=BB1_34 Depth=4
	incq	%r9
.LBB1_45:                               # %.thread
                                        #   in Loop: Header=BB1_34 Depth=4
	cmpl	%r10d, %r15d
	jg	.LBB1_47
# BB#46:                                # %.thread
                                        #   in Loop: Header=BB1_34 Depth=4
	cmpl	%r8d, %edx
	jle	.LBB1_34
	.p2align	4, 0x90
.LBB1_47:                               # %.critedge
                                        #   in Loop: Header=BB1_18 Depth=3
	movq	8(%r14), %r14
	testq	%r14, %r14
	jne	.LBB1_18
.LBB1_48:                               # %._crit_edge
                                        #   in Loop: Header=BB1_16 Depth=2
	incq	%r12
	cmpq	48(%rsp), %r12          # 8-byte Folded Reload
	jne	.LBB1_16
.LBB1_49:                               # %._crit_edge179
                                        #   in Loop: Header=BB1_14 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	32(%rsp), %rcx          # 8-byte Folded Reload
	jne	.LBB1_14
.LBB1_50:                               # %._crit_edge181
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	part_imp_match_init_strictQ, .Lfunc_end1-part_imp_match_init_strictQ
	.cfi_endproc

	.globl	part_imp_rnaQ
	.p2align	4, 0x90
	.type	part_imp_rnaQ,@function
part_imp_rnaQ:                          # @part_imp_rnaQ
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	48(%rsp)
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	impmtx(%rip)
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	callq	foldrna
	addq	$48, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset -48
	popq	%rax
	retq
.Lfunc_end2:
	.size	part_imp_rnaQ, .Lfunc_end2-part_imp_rnaQ
	.cfi_endproc

	.globl	part_imp_match_initQ
	.p2align	4, 0x90
	.type	part_imp_match_initQ,@function
part_imp_match_initQ:                   # @part_imp_match_initQ
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 128
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movl	%r8d, %r14d
	movl	%ecx, %r12d
	movl	%esi, %r15d
	movl	part_imp_match_initQ.impalloclen(%rip), %eax
	cmpl	%r12d, %eax
	movl	%edx, 12(%rsp)          # 4-byte Spill
	jl	.LBB3_2
# BB#1:
	cmpl	%r14d, %eax
	jge	.LBB3_9
.LBB3_2:
	movq	impmtx(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#3:
	callq	FreeFloatMtx
.LBB3_4:
	movq	part_imp_match_initQ.nocount1(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_6
# BB#5:
	callq	free
.LBB3_6:
	movq	part_imp_match_initQ.nocount2(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_8
# BB#7:
	callq	free
.LBB3_8:
	cmpl	%r14d, %r12d
	movl	%r14d, %edi
	cmovgel	%r12d, %edi
	addl	$2, %edi
	movl	%edi, part_imp_match_initQ.impalloclen(%rip)
	movl	%edi, %esi
	callq	AllocateFloatMtx
	movq	%rax, impmtx(%rip)
	movl	part_imp_match_initQ.impalloclen(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, part_imp_match_initQ.nocount1(%rip)
	movl	part_imp_match_initQ.impalloclen(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, part_imp_match_initQ.nocount2(%rip)
	addl	$-2, part_imp_match_initQ.impalloclen(%rip)
	movl	12(%rsp), %edx          # 4-byte Reload
.LBB3_9:                                # %.preheader236
	testl	%r12d, %r12d
	jle	.LBB3_19
# BB#10:                                # %.preheader235.lr.ph
	testl	%r15d, %r15d
	movq	part_imp_match_initQ.nocount1(%rip), %rax
	jle	.LBB3_11
# BB#23:                                # %.preheader235.us.preheader
	movslq	%r15d, %rcx
	movl	%r12d, %r8d
	xorl	%esi, %esi
	testb	$1, %r8b
	je	.LBB3_27
	.p2align	4, 0x90
.LBB3_24:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rsi,8), %rdi
	cmpb	$45, (%rdi)
	je	.LBB3_26
# BB#25:                                #   in Loop: Header=BB3_24 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB3_24
.LBB3_26:                               # %._crit_edge267.us.prol
	cmpl	%r15d, %esi
	setne	(%rax)
	movl	$1, %esi
.LBB3_27:                               # %.preheader235.us.prol.loopexit
	cmpl	$1, %r12d
	je	.LBB3_19
	.p2align	4, 0x90
.LBB3_28:                               # %.preheader235.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_29 Depth 2
                                        #     Child Loop BB3_32 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_29:                               #   Parent Loop BB3_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rdi,8), %rbp
	cmpb	$45, (%rbp,%rsi)
	je	.LBB3_31
# BB#30:                                #   in Loop: Header=BB3_29 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB3_29
.LBB3_31:                               # %._crit_edge267.us
                                        #   in Loop: Header=BB3_28 Depth=1
	cmpl	%r15d, %edi
	setne	(%rax,%rsi)
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_32:                               #   Parent Loop BB3_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rdi,8), %rbp
	cmpb	$45, 1(%rbp,%rsi)
	je	.LBB3_34
# BB#33:                                #   in Loop: Header=BB3_32 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB3_32
.LBB3_34:                               # %._crit_edge267.us.1
                                        #   in Loop: Header=BB3_28 Depth=1
	cmpl	%r15d, %edi
	setne	1(%rax,%rsi)
	addq	$2, %rsi
	cmpq	%r8, %rsi
	jne	.LBB3_28
	jmp	.LBB3_19
.LBB3_11:                               # %.preheader235.preheader
	setne	%cl
	movl	%r12d, %edi
	cmpl	$31, %r12d
	jbe	.LBB3_12
# BB#15:                                # %min.iters.checked
	movl	%r12d, %r8d
	andl	$31, %r8d
	movq	%rdi, %r9
	subq	%r8, %r9
	je	.LBB3_12
# BB#16:                                # %vector.ph
	movzbl	%cl, %esi
	movd	%esi, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	16(%rax), %rbp
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB3_17:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbp)
	movdqu	%xmm0, (%rbp)
	addq	$32, %rbp
	addq	$-32, %rsi
	jne	.LBB3_17
# BB#18:                                # %middle.block
	testl	%r8d, %r8d
	jne	.LBB3_13
	jmp	.LBB3_19
.LBB3_12:
	xorl	%r9d, %r9d
.LBB3_13:                               # %.preheader235.preheader370
	addq	%r9, %rax
	subq	%r9, %rdi
	.p2align	4, 0x90
.LBB3_14:                               # %.preheader235
                                        # =>This Inner Loop Header: Depth=1
	movb	%cl, (%rax)
	incq	%rax
	decq	%rdi
	jne	.LBB3_14
.LBB3_19:                               # %.preheader234
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	128(%rsp), %rbx
	testl	%r14d, %r14d
	jle	.LBB3_41
# BB#20:                                # %.preheader233.lr.ph
	testl	%edx, %edx
	movq	part_imp_match_initQ.nocount2(%rip), %rax
	jle	.LBB3_21
# BB#45:                                # %.preheader233.us.preheader
	movslq	%edx, %rcx
	movl	%r14d, %r8d
	xorl	%esi, %esi
	testb	$1, %r8b
	je	.LBB3_49
	.p2align	4, 0x90
.LBB3_46:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rsi,8), %rdi
	cmpb	$45, (%rdi)
	je	.LBB3_48
# BB#47:                                #   in Loop: Header=BB3_46 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB3_46
.LBB3_48:                               # %._crit_edge261.us.prol
	cmpl	%edx, %esi
	setne	(%rax)
	movl	$1, %esi
.LBB3_49:                               # %.preheader233.us.prol.loopexit
	cmpl	$1, %r14d
	je	.LBB3_41
	.p2align	4, 0x90
.LBB3_50:                               # %.preheader233.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_51 Depth 2
                                        #     Child Loop BB3_54 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_51:                               #   Parent Loop BB3_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rdi,8), %rbp
	cmpb	$45, (%rbp,%rsi)
	je	.LBB3_53
# BB#52:                                #   in Loop: Header=BB3_51 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB3_51
.LBB3_53:                               # %._crit_edge261.us
                                        #   in Loop: Header=BB3_50 Depth=1
	cmpl	%edx, %edi
	setne	(%rax,%rsi)
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_54:                               #   Parent Loop BB3_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rdi,8), %rbp
	cmpb	$45, 1(%rbp,%rsi)
	je	.LBB3_56
# BB#55:                                #   in Loop: Header=BB3_54 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB3_54
.LBB3_56:                               # %._crit_edge261.us.1
                                        #   in Loop: Header=BB3_50 Depth=1
	cmpl	%edx, %edi
	setne	1(%rax,%rsi)
	addq	$2, %rsi
	cmpq	%r8, %rsi
	jne	.LBB3_50
	jmp	.LBB3_41
.LBB3_21:                               # %.preheader233.preheader
	setne	%cl
	movl	%r14d, %edx
	cmpl	$31, %r14d
	jbe	.LBB3_22
# BB#35:                                # %min.iters.checked351
	movl	%r14d, %r8d
	andl	$31, %r8d
	movq	%rdx, %rdi
	subq	%r8, %rdi
	je	.LBB3_22
# BB#36:                                # %vector.ph355
	movzbl	%cl, %esi
	movd	%esi, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	16(%rax), %rbp
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB3_37:                               # %vector.body347
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbp)
	movdqu	%xmm0, (%rbp)
	addq	$32, %rbp
	addq	$-32, %rsi
	jne	.LBB3_37
# BB#38:                                # %middle.block348
	testl	%r8d, %r8d
	jne	.LBB3_39
	jmp	.LBB3_41
.LBB3_22:
	xorl	%edi, %edi
.LBB3_39:                               # %.preheader233.preheader368
	addq	%rdi, %rax
	subq	%rdi, %rdx
	.p2align	4, 0x90
.LBB3_40:                               # %.preheader233
                                        # =>This Inner Loop Header: Depth=1
	movb	%cl, (%rax)
	incq	%rax
	decq	%rdx
	jne	.LBB3_40
.LBB3_41:                               # %.preheader232
	testl	%r12d, %r12d
	jle	.LBB3_62
# BB#42:                                # %.preheader231.lr.ph
	testl	%r14d, %r14d
	jle	.LBB3_62
# BB#43:                                # %.preheader231.us.preheader
	movq	impmtx(%rip), %r13
	decl	%r14d
	leaq	4(,%r14,4), %rbx
	movl	%r12d, %r14d
	leaq	-1(%r14), %rax
	movq	%r14, %r12
	andq	$7, %r12
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB3_44
# BB#57:                                # %.preheader231.us.prol.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_58:                               # %.preheader231.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB3_58
	jmp	.LBB3_59
.LBB3_44:
	xorl	%ebp, %ebp
.LBB3_59:                               # %.preheader231.us.prol.loopexit
	cmpq	$7, 16(%rsp)            # 8-byte Folded Reload
	jb	.LBB3_62
# BB#60:                                # %.preheader231.us.preheader.new
	subq	%rbp, %r14
	leaq	56(%r13,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB3_61:                               # %.preheader231.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-48(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-40(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-32(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-24(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-16(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-8(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	addq	$64, %rbp
	addq	$-8, %r14
	jne	.LBB3_61
.LBB3_62:                               # %.preheader230
	testl	%r15d, %r15d
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	128(%rsp), %rbp
	movq	%rbp, %r13
	jg	.LBB3_63
.LBB3_111:                              # %._crit_edge254
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_63:                               # %.lr.ph253
	movl	12(%rsp), %eax          # 4-byte Reload
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	jmp	.LBB3_64
.LBB3_65:                               # %.lr.ph250
                                        #   in Loop: Header=BB3_64 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB3_66
.LBB3_97:                               # %.preheader
                                        #   in Loop: Header=BB3_66 Depth=2
	cmpl	%ebx, %ebp
	movq	%rbx, %rax
	jge	.LBB3_98
	.p2align	4, 0x90
.LBB3_112:                              # %.preheader.split.us
                                        #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	callq	fprintf
	movq	152(%rsp), %r11
	decl	%r13d
	cmpl	$-1, %r13d
	jne	.LBB3_112
	jmp	.LBB3_109
.LBB3_98:                               # %.preheader.split.preheader
                                        #   in Loop: Header=BB3_66 Depth=2
	movslq	%eax, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movslq	%ebp, %rbx
	movq	32(%rsp), %rcx          # 8-byte Reload
	leal	(%rax,%rcx), %edx
	subl	%ebp, %edx
	movslq	24(%rsp), %rbp          # 4-byte Folded Reload
	movslq	%ecx, %r15
	.p2align	4, 0x90
.LBB3_99:                               # %.preheader.split
                                        #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_101 Depth 4
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	callq	fprintf
	movl	16(%rsp), %edx          # 4-byte Reload
	movq	152(%rsp), %r11
	movq	24(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%r13d, %eax
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	jg	.LBB3_108
# BB#100:                               # %.lr.ph247
                                        #   in Loop: Header=BB3_99 Depth=3
	movslq	%edx, %rax
	movq	part_imp_match_initQ.nocount1(%rip), %rcx
	movq	part_imp_match_initQ.nocount2(%rip), %r9
	movq	impmtx(%rip), %r8
	movq	48(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_101:                              #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        #       Parent Loop BB3_99 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testq	%rax, %rax
	js	.LBB3_106
# BB#102:                               #   in Loop: Header=BB3_101 Depth=4
	cmpq	%rax, %rbp
	jg	.LBB3_106
# BB#103:                               #   in Loop: Header=BB3_101 Depth=4
	cmpb	$0, (%rcx,%rdi)
	jne	.LBB3_106
# BB#104:                               #   in Loop: Header=BB3_101 Depth=4
	cmpb	$0, (%r9,%rax)
	jne	.LBB3_106
# BB#105:                               #   in Loop: Header=BB3_101 Depth=4
	movq	(%r11,%r12,8), %rsi
	movq	(%rsi,%r14,8), %rsi
	movsd	72(%rsi), %xmm0         # xmm0 = mem[0],zero
	movq	136(%rsp), %rsi
	mulsd	(%rsi,%r12,8), %xmm0
	movq	144(%rsp), %rsi
	mulsd	(%rsi,%r14,8), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r8,%rdi,8), %rsi
	movd	%xmm0, (%rsi,%rax,4)
	.p2align	4, 0x90
.LBB3_106:                              #   in Loop: Header=BB3_101 Depth=4
	cmpq	%r15, %rax
	jge	.LBB3_108
# BB#107:                               #   in Loop: Header=BB3_101 Depth=4
	incq	%rax
	cmpq	%rbx, %rdi
	leaq	1(%rdi), %rdi
	jl	.LBB3_101
.LBB3_108:                              # %.critedge2
                                        #   in Loop: Header=BB3_99 Depth=3
	incl	%edx
	testl	%r13d, %r13d
	leal	-1(%r13), %eax
	movl	%eax, %r13d
	jne	.LBB3_99
	jmp	.LBB3_109
	.p2align	4, 0x90
.LBB3_66:                               #   Parent Loop BB3_64 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_67 Depth 3
                                        #       Child Loop BB3_70 Depth 3
                                        #       Child Loop BB3_73 Depth 3
                                        #       Child Loop BB3_76 Depth 3
                                        #       Child Loop BB3_81 Depth 3
                                        #       Child Loop BB3_88 Depth 3
                                        #         Child Loop BB3_90 Depth 4
                                        #       Child Loop BB3_99 Depth 3
                                        #         Child Loop BB3_101 Depth 4
                                        #       Child Loop BB3_112 Depth 3
	movq	stderr(%rip), %rdi
	movq	(%r11,%r12,8), %rax
	movq	(%rax,%r14,8), %rax
	movl	24(%rax), %edx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	152(%rsp), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%r14,8), %rax
	movl	28(%rax), %edx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	(%r13,%r14,8), %rcx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	movq	152(%rsp), %rdi
	movq	(%rbx,%r12,8), %rdx
	movl	$-1, %ecx
	movq	%rdx, %rax
	.p2align	4, 0x90
.LBB3_67:                               #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ebx
	testb	%bl, %bl
	je	.LBB3_69
# BB#68:                                #   in Loop: Header=BB3_67 Depth=3
	incq	%rax
	xorl	%esi, %esi
	cmpb	$45, %bl
	setne	%sil
	addl	%esi, %ecx
	movq	(%rdi,%r12,8), %rsi
	movq	(%rsi,%r14,8), %rsi
	cmpl	24(%rsi), %ecx
	jne	.LBB3_67
.LBB3_69:                               #   in Loop: Header=BB3_66 Depth=2
	movl	$4294967295, %ebp       # imm = 0xFFFFFFFF
	subq	%rdx, %rbp
	leaq	(%rbp,%rax), %r15
	.p2align	4, 0x90
.LBB3_70:                               #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.LBB3_72
# BB#71:                                #   in Loop: Header=BB3_70 Depth=3
	incq	%rax
	xorl	%esi, %esi
	cmpb	$45, %dl
	setne	%sil
	addl	%esi, %ecx
	movq	(%rdi,%r12,8), %rdx
	movq	(%rdx,%r14,8), %rdx
	cmpl	28(%rdx), %ecx
	jne	.LBB3_70
.LBB3_72:                               #   in Loop: Header=BB3_66 Depth=2
	addq	%rax, %rbp
	movq	(%r13,%r14,8), %rdx
	movl	$-1, %ecx
	movq	%rdx, %rax
	.p2align	4, 0x90
.LBB3_73:                               #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ebx
	testb	%bl, %bl
	je	.LBB3_75
# BB#74:                                #   in Loop: Header=BB3_73 Depth=3
	incq	%rax
	xorl	%esi, %esi
	cmpb	$45, %bl
	setne	%sil
	addl	%esi, %ecx
	movq	(%rdi,%r12,8), %rsi
	movq	(%rsi,%r14,8), %rsi
	cmpl	32(%rsi), %ecx
	jne	.LBB3_73
.LBB3_75:                               #   in Loop: Header=BB3_66 Depth=2
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	$4294967295, %ebx       # imm = 0xFFFFFFFF
	subq	%rdx, %rbx
	leaq	(%rbx,%rax), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_76:                               #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.LBB3_78
# BB#77:                                #   in Loop: Header=BB3_76 Depth=3
	incq	%rax
	xorl	%esi, %esi
	cmpb	$45, %dl
	setne	%sil
	addl	%esi, %ecx
	movq	(%rdi,%r12,8), %rdx
	movq	(%rdx,%r14,8), %rdx
	cmpl	36(%rdx), %ecx
	jne	.LBB3_76
.LBB3_78:                               #   in Loop: Header=BB3_66 Depth=2
	addq	%rax, %rbx
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	152(%rsp), %r9
	movq	24(%rsp), %r13          # 8-byte Reload
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	subl	%ebx, %r13d
	movq	%r15, %r10
	movq	16(%rsp), %r11          # 8-byte Reload
	jg	.LBB3_86
# BB#79:                                #   in Loop: Header=BB3_66 Depth=2
	cmpl	%r10d, %r11d
	jl	.LBB3_86
# BB#80:                                # %.lr.ph
                                        #   in Loop: Header=BB3_66 Depth=2
	movq	part_imp_match_initQ.nocount1(%rip), %rax
	movq	part_imp_match_initQ.nocount2(%rip), %rcx
	movq	impmtx(%rip), %r8
	movslq	%r10d, %rsi
	movslq	%r11d, %rdi
	movslq	24(%rsp), %rbx          # 4-byte Folded Reload
	movslq	32(%rsp), %rdx          # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB3_81:                               #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$0, (%rax,%rsi)
	jne	.LBB3_84
# BB#82:                                #   in Loop: Header=BB3_81 Depth=3
	cmpb	$0, (%rcx,%rbx)
	jne	.LBB3_84
# BB#83:                                #   in Loop: Header=BB3_81 Depth=3
	movq	(%r9,%r12,8), %rbp
	movq	(%rbp,%r14,8), %rbp
	movsd	72(%rbp), %xmm0         # xmm0 = mem[0],zero
	movq	136(%rsp), %rbp
	mulsd	(%rbp,%r12,8), %xmm0
	movq	144(%rsp), %rbp
	mulsd	(%rbp,%r14,8), %xmm0
	movq	(%r8,%rsi,8), %rbp
	movss	(%rbp,%rbx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movd	%xmm0, (%rbp,%rbx,4)
	.p2align	4, 0x90
.LBB3_84:                               #   in Loop: Header=BB3_81 Depth=3
	cmpq	%rdx, %rbx
	jge	.LBB3_86
# BB#85:                                #   in Loop: Header=BB3_81 Depth=3
	incq	%rbx
	cmpq	%rdi, %rsi
	leaq	1(%rsi), %rsi
	jl	.LBB3_81
.LBB3_86:                               # %.critedge
                                        #   in Loop: Header=BB3_66 Depth=2
	movl	%r11d, %eax
	subl	%r10d, %eax
	addl	%eax, %r13d
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	movq	%r10, %rbx
	movq	%r11, %rbp
	callq	fprintf
	testl	%r13d, %r13d
	jg	.LBB3_97
# BB#87:                                # %.preheader228
                                        #   in Loop: Header=BB3_66 Depth=2
	movq	part_imp_match_initQ.nocount1(%rip), %rax
	movq	part_imp_match_initQ.nocount2(%rip), %r10
	movq	impmtx(%rip), %r9
	movq	24(%rsp), %rcx          # 8-byte Reload
	leal	(%rbp,%rcx), %esi
	subl	32(%rsp), %esi          # 4-byte Folded Reload
	movslq	%ebx, %rdi
	movslq	%ebp, %rbx
	movslq	%ecx, %r8
	movq	152(%rsp), %r11
	.p2align	4, 0x90
.LBB3_88:                               #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_90 Depth 4
	leal	(%r13,%r15), %edx
	cmpl	16(%rsp), %edx          # 4-byte Folded Reload
	jg	.LBB3_96
# BB#89:                                # %.lr.ph242.preheader
                                        #   in Loop: Header=BB3_88 Depth=3
	movslq	%esi, %rdx
	movq	%r8, %rbp
	.p2align	4, 0x90
.LBB3_90:                               # %.lr.ph242
                                        #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        #       Parent Loop BB3_88 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testq	%rdx, %rdx
	js	.LBB3_95
# BB#91:                                # %.lr.ph242
                                        #   in Loop: Header=BB3_90 Depth=4
	cmpq	%rdi, %rdx
	jl	.LBB3_95
# BB#92:                                #   in Loop: Header=BB3_90 Depth=4
	cmpb	$0, (%rax,%rdx)
	jne	.LBB3_95
# BB#93:                                #   in Loop: Header=BB3_90 Depth=4
	cmpb	$0, (%r10,%rbp)
	jne	.LBB3_95
# BB#94:                                #   in Loop: Header=BB3_90 Depth=4
	movq	(%r11,%r12,8), %rcx
	movq	(%rcx,%r14,8), %rcx
	movsd	72(%rcx), %xmm0         # xmm0 = mem[0],zero
	movq	136(%rsp), %rcx
	mulsd	(%rcx,%r12,8), %xmm0
	movq	144(%rsp), %rcx
	mulsd	(%rcx,%r14,8), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r9,%rdx,8), %rcx
	movd	%xmm0, (%rcx,%rbp,4)
	.p2align	4, 0x90
.LBB3_95:                               #   in Loop: Header=BB3_90 Depth=4
	incq	%rbp
	cmpq	%rbx, %rdx
	leaq	1(%rdx), %rdx
	jl	.LBB3_90
.LBB3_96:                               # %._crit_edge
                                        #   in Loop: Header=BB3_88 Depth=3
	incl	%esi
	testl	%r13d, %r13d
	leal	1(%r13), %ecx
	movl	%ecx, %r13d
	jne	.LBB3_88
.LBB3_109:                              # %.loopexit
                                        #   in Loop: Header=BB3_66 Depth=2
	incq	%r14
	cmpq	64(%rsp), %r14          # 8-byte Folded Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	128(%rsp), %rbp
	movq	%rbp, %r13
	jne	.LBB3_66
	jmp	.LBB3_110
	.p2align	4, 0x90
.LBB3_64:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_66 Depth 2
                                        #       Child Loop BB3_67 Depth 3
                                        #       Child Loop BB3_70 Depth 3
                                        #       Child Loop BB3_73 Depth 3
                                        #       Child Loop BB3_76 Depth 3
                                        #       Child Loop BB3_81 Depth 3
                                        #       Child Loop BB3_88 Depth 3
                                        #         Child Loop BB3_90 Depth 4
                                        #       Child Loop BB3_99 Depth 3
                                        #         Child Loop BB3_101 Depth 4
                                        #       Child Loop BB3_112 Depth 3
	movq	stderr(%rip), %rdi
	movq	(%rbx,%r12,8), %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%r12d, %edx
	callq	fprintf
	movq	152(%rsp), %r11
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jg	.LBB3_65
.LBB3_110:                              # %._crit_edge251
                                        #   in Loop: Header=BB3_64 Depth=1
	incq	%r12
	cmpq	56(%rsp), %r12          # 8-byte Folded Reload
	jne	.LBB3_64
	jmp	.LBB3_111
.Lfunc_end3:
	.size	part_imp_match_initQ, .Lfunc_end3-part_imp_match_initQ
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4608533498688228557     # double 1.3
.LCPI4_1:
	.quad	4607182418800017408     # double 1
.LCPI4_2:
	.quad	4602678819172646912     # double 0.5
.LCPI4_4:
	.quad	-4620693217682128896    # double -0.5
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_3:
	.long	1065353216              # float 1
.LCPI4_5:
	.long	1176256512              # float 1.0E+4
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_6:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI4_7:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI4_8:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.text
	.globl	partQ__align
	.p2align	4, 0x90
	.type	partQ__align,@function
partQ__align:                           # @partQ__align
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	subq	$408, %rsp              # imm = 0x198
.Lcfi40:
	.cfi_def_cfa_offset 464
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movl	%r8d, %r12d
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rdx, %r14
	movq	%rsi, %rbx
	cvtsi2ssl	penalty(%rip), %xmm0
	movss	%xmm0, 56(%rsp)         # 4-byte Spill
	movl	partQ__align.orlgth1(%rip), %r15d
	testl	%r15d, %r15d
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	jne	.LBB4_2
# BB#1:
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, partQ__align.mseq1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%rax, partQ__align.mseq2(%rip)
	movl	partQ__align.orlgth1(%rip), %r15d
.LBB4_2:
	movq	(%rdi), %rdi
	callq	strlen
	movq	%rax, %r13
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%r15, %rdx
	cmpl	%edx, %r13d
	movl	partQ__align.orlgth2(%rip), %r10d
	movl	%ebp, 24(%rsp)          # 4-byte Spill
	movl	%r12d, 28(%rsp)         # 4-byte Spill
	movq	%r13, %r15
	movq	%r13, 112(%rsp)         # 8-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%r12d, %r8d
	movq	%r14, 72(%rsp)          # 8-byte Spill
	jg	.LBB4_5
# BB#3:
	cmpl	%r10d, %eax
	jg	.LBB4_5
# BB#4:
	movl	%ebp, %r13d
	jmp	.LBB4_10
.LBB4_5:
	testl	%edx, %edx
	jle	.LBB4_6
# BB#7:
	testl	%r10d, %r10d
	movq	64(%rsp), %rbx          # 8-byte Reload
	jle	.LBB4_9
# BB#8:
	movq	partQ__align.w1(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.w2(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.match(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.m(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.mp(%rip), %rdi
	callq	FreeIntVec
	movq	partQ__align.mseq(%rip), %rdi
	callq	FreeCharMtx
	movq	partQ__align.digf1(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.digf2(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.diaf1(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.diaf2(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.gapz1(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.gapz2(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.gapf1(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.gapf2(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.ogcp1g(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.ogcp2g(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.fgcp1g(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.fgcp2g(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.og_h_dg_n1_p(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.og_h_dg_n2_p(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.fg_h_dg_n1_p(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.fg_h_dg_n2_p(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.og_t_fg_h_dg_n1_p(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.og_t_fg_h_dg_n2_p(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.fg_t_og_h_dg_n1_p(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.fg_t_og_h_dg_n2_p(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.gapz_n1(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.gapz_n2(%rip), %rdi
	callq	FreeFloatVec
	movq	partQ__align.cpmx1(%rip), %rdi
	callq	FreeFloatMtx
	movq	partQ__align.cpmx2(%rip), %rdi
	callq	FreeFloatMtx
	movq	partQ__align.floatwork(%rip), %rdi
	callq	FreeFloatMtx
	movq	partQ__align.intwork(%rip), %rdi
	callq	FreeIntMtx
	movl	partQ__align.orlgth1(%rip), %edx
	movl	partQ__align.orlgth2(%rip), %r10d
	jmp	.LBB4_9
.LBB4_6:
	movq	64(%rsp), %rbx          # 8-byte Reload
.LBB4_9:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r15d, %xmm0
	movsd	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%edx, %eax
	cmovgel	%eax, %edx
	leal	100(%rdx), %r13d
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	leal	100(%r10), %r15d
	leal	102(%r10), %r12d
	movl	%r12d, %edi
	movq	%r10, %r14
	movq	%rdx, %rbp
	callq	AllocateFloatVec
	movq	%rax, partQ__align.w1(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.w2(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.match(%rip)
	leal	102(%rbp), %ebx
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.initverticalw(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.lastverticalw(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.m(%rip)
	movl	%r12d, %edi
	callq	AllocateIntVec
	movq	%rax, partQ__align.mp(%rip)
	movl	njob(%rip), %edi
	leal	200(%r14,%rbp), %esi
	callq	AllocateCharMtx
	movq	%rax, partQ__align.mseq(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.digf1(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.digf2(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.diaf1(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.diaf2(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.gapz1(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.gapz2(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.gapf1(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.gapf2(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.ogcp1g(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.ogcp2g(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.fgcp1g(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.fgcp2g(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.og_h_dg_n1_p(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.og_h_dg_n2_p(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.fg_h_dg_n1_p(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.fg_h_dg_n2_p(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.og_t_fg_h_dg_n1_p(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.og_t_fg_h_dg_n2_p(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.fg_t_og_h_dg_n1_p(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.fg_t_og_h_dg_n2_p(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.gapz_n1(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, partQ__align.gapz_n2(%rip)
	movl	$26, %edi
	movl	%ebx, %esi
	callq	AllocateFloatMtx
	movq	%rax, partQ__align.cpmx1(%rip)
	movl	$26, %edi
	movl	%r12d, %esi
	callq	AllocateFloatMtx
	movq	%rax, partQ__align.cpmx2(%rip)
	cmpl	%r15d, %r13d
	cmovgel	%r13d, %r15d
	addl	$2, %r15d
	movl	$26, %esi
	movl	%r15d, %edi
	callq	AllocateFloatMtx
	movq	%rax, partQ__align.floatwork(%rip)
	movl	$26, %esi
	movl	%r15d, %edi
	movq	112(%rsp), %r15         # 8-byte Reload
	callq	AllocateIntMtx
	movq	%rbp, %rdx
	movq	%r14, %r10
	movq	%rax, partQ__align.intwork(%rip)
	movl	%edx, partQ__align.orlgth1(%rip)
	movl	%r10d, partQ__align.orlgth2(%rip)
	movl	24(%rsp), %r13d         # 4-byte Reload
	movl	28(%rsp), %r8d          # 4-byte Reload
.LBB4_10:                               # %.preheader632
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%rdx, %r14
	testl	%r8d, %r8d
	jle	.LBB4_25
# BB#11:                                # %.lr.ph703
	movq	partQ__align.mseq(%rip), %rcx
	movq	partQ__align.mseq1(%rip), %rdx
	movl	%r8d, %ebp
	cmpl	$3, %r8d
	jbe	.LBB4_12
# BB#19:                                # %min.iters.checked
	movl	%r8d, %r9d
	andl	$3, %r9d
	movq	%rbp, %rsi
	subq	%r9, %rsi
	je	.LBB4_12
# BB#20:                                # %vector.memcheck
	leaq	(%rcx,%rbp,8), %rax
	cmpq	%rax, %rdx
	jae	.LBB4_22
# BB#21:                                # %vector.memcheck
	leaq	(%rdx,%rbp,8), %rax
	cmpq	%rax, %rcx
	jae	.LBB4_22
.LBB4_12:
	xorl	%esi, %esi
.LBB4_13:                               # %scalar.ph.preheader
	movl	%ebp, %eax
	subl	%esi, %eax
	leaq	-1(%rbp), %rdi
	subq	%rsi, %rdi
	andq	$7, %rax
	je	.LBB4_16
# BB#14:                                # %scalar.ph.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB4_15:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rbx
	movq	%rbx, (%rdx,%rsi,8)
	incq	%rsi
	incq	%rax
	jne	.LBB4_15
.LBB4_16:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rdi
	jb	.LBB4_25
# BB#17:                                # %scalar.ph.preheader.new
	subq	%rsi, %rbp
	leaq	56(%rdx,%rsi,8), %rdx
	leaq	56(%rcx,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB4_18:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rax
	movq	%rax, -56(%rdx)
	movq	-48(%rcx), %rax
	movq	%rax, -48(%rdx)
	movq	-40(%rcx), %rax
	movq	%rax, -40(%rdx)
	movq	-32(%rcx), %rax
	movq	%rax, -32(%rdx)
	movq	-24(%rcx), %rax
	movq	%rax, -24(%rdx)
	movq	-16(%rcx), %rax
	movq	%rax, -16(%rdx)
	movq	-8(%rcx), %rax
	movq	%rax, -8(%rdx)
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
	addq	$64, %rdx
	addq	$64, %rcx
	addq	$-8, %rbp
	jne	.LBB4_18
.LBB4_25:                               # %.preheader631
	testl	%r13d, %r13d
	jle	.LBB4_40
# BB#26:                                # %.lr.ph700
	movq	partQ__align.mseq(%rip), %r11
	movq	partQ__align.mseq2(%rip), %rsi
	movslq	%r8d, %r9
	movl	%r13d, %eax
	movl	%eax, %ebx
	cmpl	$3, %eax
	jbe	.LBB4_27
# BB#34:                                # %min.iters.checked805
	movl	%eax, %ebp
	andl	$3, %ebp
	movq	%rbx, %rdi
	subq	%rbp, %rdi
	je	.LBB4_27
# BB#35:                                # %vector.memcheck820
	leaq	(%r9,%rbx), %rax
	leaq	(%r11,%rax,8), %rax
	cmpq	%rax, %rsi
	jae	.LBB4_37
# BB#36:                                # %vector.memcheck820
	leaq	(%rsi,%rbx,8), %rax
	leaq	(%r11,%r9,8), %rcx
	cmpq	%rax, %rcx
	jae	.LBB4_37
.LBB4_27:
	xorl	%edi, %edi
.LBB4_28:                               # %scalar.ph803.preheader
	movl	%ebx, %eax
	subl	%edi, %eax
	leaq	-1(%rbx), %rbp
	subq	%rdi, %rbp
	andq	$3, %rax
	je	.LBB4_31
# BB#29:                                # %scalar.ph803.prol.preheader
	leaq	(%r11,%r9,8), %rcx
	negq	%rax
	.p2align	4, 0x90
.LBB4_30:                               # %scalar.ph803.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rdx
	movq	%rdx, (%rsi,%rdi,8)
	incq	%rdi
	incq	%rax
	jne	.LBB4_30
.LBB4_31:                               # %scalar.ph803.prol.loopexit
	cmpq	$3, %rbp
	jb	.LBB4_40
# BB#32:                                # %scalar.ph803.preheader.new
	subq	%rdi, %rbx
	leaq	24(%rsi,%rdi,8), %rsi
	addq	%rdi, %r9
	leaq	24(%r11,%r9,8), %rax
	.p2align	4, 0x90
.LBB4_33:                               # %scalar.ph803
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rax), %rcx
	movq	%rcx, -24(%rsi)
	movq	-16(%rax), %rcx
	movq	%rcx, -16(%rsi)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rsi)
	movq	(%rax), %rcx
	movq	%rcx, (%rsi)
	addq	$32, %rsi
	addq	$32, %rax
	addq	$-4, %rbx
	jne	.LBB4_33
.LBB4_40:                               # %._crit_edge701
	movl	commonAlloc1(%rip), %ebx
	movq	%r14, %rax
	cmpl	%ebx, %eax
	movl	commonAlloc2(%rip), %ebp
	jg	.LBB4_43
# BB#41:                                # %._crit_edge701
	cmpl	%ebp, %r10d
	jg	.LBB4_43
# BB#42:                                # %._crit_edge775
	movq	commonIP(%rip), %rax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	72(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB4_47
.LBB4_43:                               # %._crit_edge701._crit_edge
	movq	%r12, %r14
	movl	%r8d, %r12d
	testl	%ebx, %ebx
	je	.LBB4_46
# BB#44:                                # %._crit_edge701._crit_edge
	testl	%ebp, %ebp
	je	.LBB4_46
# BB#45:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movl	partQ__align.orlgth1(%rip), %eax
	movl	commonAlloc1(%rip), %ebx
	movl	partQ__align.orlgth2(%rip), %r10d
	movl	commonAlloc2(%rip), %ebp
.LBB4_46:
	cmpl	%ebx, %eax
	cmovgel	%eax, %ebx
	cmpl	%ebp, %r10d
	cmovgel	%r10d, %ebp
	leal	10(%rbx), %edi
	leal	10(%rbp), %esi
	callq	AllocateIntMtx
	movq	%rax, commonIP(%rip)
	movl	%ebx, commonAlloc1(%rip)
	movl	%ebp, commonAlloc2(%rip)
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	72(%rsp), %rbp          # 8-byte Reload
	movl	%r12d, %r8d
	movq	%r14, %r12
.LBB4_47:
	movq	%rax, partQ__align.ijp(%rip)
	movq	partQ__align.cpmx1(%rip), %rsi
	movq	%rbp, %rdx
	movl	%r15d, %ecx
	movq	%rdi, %r14
	callq	cpmx_calc_new
	movq	partQ__align.cpmx2(%rip), %rsi
	movq	144(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	64(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r13d, %r8d
	callq	cpmx_calc_new
	movq	536(%rsp), %rax
	testq	%rax, %rax
	movq	partQ__align.ogcp1g(%rip), %rdi
	je	.LBB4_49
# BB#48:
	movq	%r14, %r13
	movq	%r12, %rbx
	movq	560(%rsp), %r14
	movq	552(%rsp), %rcx
	movq	%rcx, (%rsp)
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	%r13, %rdx
	movq	%rbp, %r12
	movq	%r12, %rcx
	movl	%r15d, %r8d
	movq	%rax, %r9
	callq	new_OpeningGapCount_zure
	movq	partQ__align.ogcp2g(%rip), %rdi
	movq	%r14, (%rsp)
	movl	24(%rsp), %esi          # 4-byte Reload
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	movq	64(%rsp), %r14          # 8-byte Reload
	movl	%r14d, %r8d
	movq	544(%rsp), %r9
	callq	new_OpeningGapCount_zure
	movq	partQ__align.fgcp1g(%rip), %rdi
	movq	552(%rsp), %rax
	movq	%rax, (%rsp)
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	%r13, %rdx
	movq	%r12, %rcx
	movl	%r15d, %r8d
	movq	536(%rsp), %r9
	callq	new_FinalGapCount_zure
	movq	partQ__align.fgcp2g(%rip), %rdi
	movq	560(%rsp), %rax
	movq	%rax, (%rsp)
	movl	24(%rsp), %esi          # 4-byte Reload
	movq	%rbp, %rdx
	movq	%rbp, %r15
	movq	%rbx, %rcx
	movl	%r14d, %r8d
	movq	%r14, %rbp
	movq	544(%rsp), %rax
	movq	%rax, %r9
	callq	new_FinalGapCount_zure
	movq	partQ__align.digf1(%rip), %rdi
	movq	552(%rsp), %rax
	movq	%rax, (%rsp)
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	112(%rsp), %rbx         # 8-byte Reload
	movl	%ebx, %r8d
	movq	536(%rsp), %r9
	callq	getdigapfreq_part
	movq	partQ__align.digf2(%rip), %rdi
	movq	560(%rsp), %rax
	movq	%rax, (%rsp)
	movl	24(%rsp), %esi          # 4-byte Reload
	movq	%r15, %rdx
	movq	%r15, %r14
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rcx
	movl	%ebp, %r8d
	movq	544(%rsp), %r9
	callq	getdigapfreq_part
	movq	partQ__align.diaf1(%rip), %rdi
	movq	552(%rsp), %rax
	movq	%rax, (%rsp)
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%r12, %r13
	movl	%ebx, %r8d
	movq	536(%rsp), %r9
	callq	getdiaminofreq_part
	movq	partQ__align.diaf2(%rip), %rdi
	movq	560(%rsp), %rax
	movq	%rax, (%rsp)
	movl	24(%rsp), %r12d         # 4-byte Reload
	movl	%r12d, %esi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	64(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r8d
	movq	544(%rsp), %r9
	callq	getdiaminofreq_part
	movq	partQ__align.gapf1(%rip), %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	movq	%r13, %rcx
	movq	112(%rsp), %r8          # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	getgapfreq
	movq	partQ__align.gapf2(%rip), %rdi
	movl	%r12d, %esi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movl	%ebx, %r8d
	callq	getgapfreq
	movq	partQ__align.gapz1(%rip), %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	%rbp, %rdx
	movq	%r13, %rcx
	movq	112(%rsp), %r8          # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	536(%rsp), %rbp
	movq	%rbp, %r9
	callq	getgapfreq_zure_part
	movq	partQ__align.gapz2(%rip), %rdi
	movl	%r12d, %esi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movl	%ebx, %r8d
	movq	%rbp, %r9
	callq	getgapfreq_zure_part
	movq	%rbx, %r12
	jmp	.LBB4_50
.LBB4_49:
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	%r14, %rdx
	movq	%rbp, %rcx
	movl	%r15d, %r8d
	movq	%r15, %r14
	callq	st_OpeningGapCount
	movq	partQ__align.ogcp2g(%rip), %rdi
	movl	%r13d, %esi
	movq	%rbx, %rdx
	movq	%r12, %r15
	movq	%r15, %rcx
	movq	64(%rsp), %r12          # 8-byte Reload
	movl	%r12d, %r8d
	callq	st_OpeningGapCount
	movq	partQ__align.fgcp1g(%rip), %rdi
	movl	28(%rsp), %r13d         # 4-byte Reload
	movl	%r13d, %esi
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%rbp, %rcx
	movl	%r14d, %r8d
	callq	st_FinalGapCount_zure
	movq	partQ__align.fgcp2g(%rip), %rdi
	movl	24(%rsp), %esi          # 4-byte Reload
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	%r15, %rcx
	movl	%r12d, %r8d
	callq	st_FinalGapCount_zure
	movq	partQ__align.digf1(%rip), %rdi
	movl	%r13d, %esi
	movq	%rbx, %rdx
	movq	%rbx, %r14
	movq	%rbp, %rcx
	movq	%rbp, %r15
	movq	112(%rsp), %rbx         # 8-byte Reload
	movl	%ebx, %r8d
	callq	getdigapfreq_st
	movq	partQ__align.digf2(%rip), %rdi
	movl	24(%rsp), %r12d         # 4-byte Reload
	movl	%r12d, %esi
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	getdigapfreq_st
	movq	partQ__align.diaf1(%rip), %rdi
	movl	%r13d, %esi
	movq	%r14, %rdx
	movq	%r14, %r13
	movq	%r15, %r14
	movq	%r14, %rcx
	movl	%ebx, %r8d
	callq	getdiaminofreq_x
	movq	partQ__align.diaf2(%rip), %rdi
	movl	%r12d, %esi
	movl	%r12d, %ebx
	movq	%rbp, %rdx
	movq	%rbp, %r15
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	movq	64(%rsp), %r12          # 8-byte Reload
	movl	%r12d, %r8d
	callq	getdiaminofreq_x
	movq	partQ__align.gapf1(%rip), %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	112(%rsp), %r14         # 8-byte Reload
	movl	%r14d, %r8d
	callq	getgapfreq
	movq	partQ__align.gapf2(%rip), %rdi
	movl	%ebx, %r13d
	movl	%r13d, %esi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	movl	%r12d, %r8d
	callq	getgapfreq
	movq	partQ__align.gapz1(%rip), %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	movl	%r14d, %r8d
	callq	getgapfreq_zure
	movq	partQ__align.gapz2(%rip), %rdi
	movl	%r13d, %esi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	movl	%r12d, %r8d
	callq	getgapfreq_zure
.LBB4_50:
	movss	56(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	cmpl	$-1, %r12d
	jl	.LBB4_53
# BB#51:                                # %.lr.ph697
	movq	partQ__align.ogcp2g(%rip), %rax
	movq	partQ__align.digf2(%rip), %rcx
	cvtss2sd	%xmm6, %xmm0
	movq	partQ__align.og_h_dg_n2_p(%rip), %r8
	movq	partQ__align.fgcp2g(%rip), %rsi
	movq	partQ__align.fg_h_dg_n2_p(%rip), %r9
	movq	partQ__align.og_t_fg_h_dg_n2_p(%rip), %rbx
	movq	partQ__align.fg_t_og_h_dg_n2_p(%rip), %rbp
	movq	partQ__align.gapz2(%rip), %rdx
	movq	partQ__align.gapz_n2(%rip), %rdi
	leal	2(%r12), %r10d
	movsd	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI4_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movss	.LCPI4_3(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB4_52:                               # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	subsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	%xmm2, %xmm5
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm5, %xmm4
	movss	%xmm4, (%r8)
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	subsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	%xmm2, %xmm5
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm5, %xmm4
	movss	%xmm4, (%r9)
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm5, %xmm4
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movss	%xmm4, (%rbx)
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm5, %xmm4
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movss	%xmm4, (%rbp)
	movaps	%xmm3, %xmm4
	subss	(%rdx), %xmm4
	movss	%xmm4, (%rdi)
	addq	$4, %rax
	addq	$4, %rcx
	addq	$4, %r8
	addq	$4, %rsi
	addq	$4, %r9
	addq	$4, %rbx
	addq	$4, %rbp
	addq	$4, %rdx
	addq	$4, %rdi
	decq	%r10
	jne	.LBB4_52
.LBB4_53:                               # %._crit_edge698
	movq	472(%rsp), %r13
	movq	112(%rsp), %r15         # 8-byte Reload
	cmpl	$-1, %r15d
	jl	.LBB4_56
# BB#54:                                # %.lr.ph693
	movq	partQ__align.ogcp1g(%rip), %rax
	movq	partQ__align.digf1(%rip), %rcx
	cvtss2sd	%xmm6, %xmm0
	movq	partQ__align.og_h_dg_n1_p(%rip), %r8
	movq	partQ__align.fgcp1g(%rip), %rsi
	movq	partQ__align.fg_h_dg_n1_p(%rip), %r9
	movq	partQ__align.og_t_fg_h_dg_n1_p(%rip), %rbx
	movq	partQ__align.fg_t_og_h_dg_n1_p(%rip), %rbp
	movq	partQ__align.gapz1(%rip), %rdx
	movq	partQ__align.gapz_n1(%rip), %rdi
	leal	2(%r15), %r10d
	movsd	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI4_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movss	.LCPI4_3(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB4_55:                               # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	subsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	%xmm2, %xmm5
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm5, %xmm4
	movss	%xmm4, (%r8)
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	subsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	%xmm2, %xmm5
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm5, %xmm4
	movss	%xmm4, (%r9)
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm5, %xmm4
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movss	%xmm4, (%rbx)
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm5, %xmm4
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movss	%xmm4, (%rbp)
	movaps	%xmm3, %xmm4
	subss	(%rdx), %xmm4
	movss	%xmm4, (%rdi)
	addq	$4, %rax
	addq	$4, %rcx
	addq	$4, %r8
	addq	$4, %rsi
	addq	$4, %r9
	addq	$4, %rbx
	addq	$4, %rbp
	addq	$4, %rdx
	addq	$4, %rdi
	decq	%r10
	jne	.LBB4_55
.LBB4_56:                               # %._crit_edge694
	movl	504(%rsp), %r14d
	movq	partQ__align.w1(%rip), %rbx
	movq	partQ__align.w2(%rip), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	partQ__align.initverticalw(%rip), %rdi
	movq	partQ__align.cpmx2(%rip), %rsi
	movq	partQ__align.cpmx1(%rip), %rdx
	movq	partQ__align.floatwork(%rip), %r9
	movq	partQ__align.intwork(%rip), %rax
	movq	%rax, (%rsp)
	movl	$1, 8(%rsp)
	xorl	%ecx, %ecx
	movl	%r15d, %r8d
	callq	match_calc
	testq	%r13, %r13
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	je	.LBB4_71
# BB#57:
	testl	%r15d, %r15d
	jle	.LBB4_64
# BB#58:                                # %.lr.ph.i
	movq	partQ__align.initverticalw(%rip), %rsi
	movq	528(%rsp), %rax
	movslq	(%rax), %rcx
	movslq	%r14d, %rax
	addq	%rcx, %rax
	movq	impmtx(%rip), %rcx
	movl	%r15d, %edx
	testb	$1, %r15b
	jne	.LBB4_60
# BB#59:
	xorl	%edi, %edi
	cmpq	$1, %rdx
	jne	.LBB4_62
	jmp	.LBB4_64
.LBB4_71:                               # %.critedge
	movq	partQ__align.cpmx1(%rip), %rsi
	movq	partQ__align.cpmx2(%rip), %rdx
	movq	partQ__align.floatwork(%rip), %r9
	movq	partQ__align.intwork(%rip), %rax
	movq	%rax, (%rsp)
	movl	$1, 8(%rsp)
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movl	%r12d, %r8d
	callq	match_calc
	movq	48(%rsp), %r8           # 8-byte Reload
	movss	56(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	cmpl	$1, outgap(%rip)
	je	.LBB4_83
	jmp	.LBB4_73
.LBB4_60:
	movq	520(%rsp), %rdi
	movslq	(%rdi), %rdi
	movslq	488(%rsp), %rbp
	addq	%rdi, %rbp
	movq	(%rcx,%rbp,8), %rdi
	movss	(%rdi,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	movl	$1, %edi
	cmpq	$1, %rdx
	je	.LBB4_64
.LBB4_62:                               # %.lr.ph.i.new
	subq	%rdi, %rdx
	leaq	4(%rsi,%rdi,4), %rsi
	movq	520(%rsp), %rbp
	leaq	4(%rbp,%rdi,4), %rdi
	movslq	488(%rsp), %rbp
	.p2align	4, 0x90
.LBB4_63:                               # =>This Inner Loop Header: Depth=1
	movslq	-4(%rdi), %rbx
	addq	%rbp, %rbx
	movq	(%rcx,%rbx,8), %rbx
	movss	(%rbx,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movslq	(%rdi), %rbx
	addq	%rbp, %rbx
	movq	(%rcx,%rbx,8), %rbx
	movss	(%rbx,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB4_63
.LBB4_64:                               # %part_imp_match_out_vead_tate_gapmapQ.exit
	movq	partQ__align.cpmx1(%rip), %rsi
	movq	partQ__align.cpmx2(%rip), %rdx
	movq	partQ__align.floatwork(%rip), %r9
	movq	partQ__align.intwork(%rip), %rax
	movq	%rax, (%rsp)
	movl	$1, 8(%rsp)
	xorl	%ecx, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%r12d, %r8d
	callq	match_calc
	testl	%r12d, %r12d
	movq	48(%rsp), %r8           # 8-byte Reload
	movss	56(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	jle	.LBB4_72
# BB#65:                                # %.lr.ph.i577
	movq	520(%rsp), %rax
	movslq	(%rax), %rax
	movslq	488(%rsp), %rcx
	addq	%rax, %rcx
	movq	impmtx(%rip), %rax
	movq	(%rax,%rcx,8), %rax
	movl	%r12d, %ecx
	testb	$1, %r12b
	jne	.LBB4_67
# BB#66:
	xorl	%esi, %esi
	cmpq	$1, %rcx
	jne	.LBB4_69
	jmp	.LBB4_72
.LBB4_67:
	movq	528(%rsp), %rdx
	movslq	(%rdx), %rdx
	movslq	%r14d, %rsi
	addq	%rdx, %rsi
	movss	(%rax,%rsi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	32(%rsp), %rdx          # 8-byte Reload
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	movl	$1, %esi
	cmpq	$1, %rcx
	je	.LBB4_72
.LBB4_69:                               # %.lr.ph.i577.new
	subq	%rsi, %rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	4(%rdx,%rsi,4), %rdx
	movq	528(%rsp), %rdi
	leaq	4(%rdi,%rsi,4), %rsi
	movslq	%r14d, %rdi
	.p2align	4, 0x90
.LBB4_70:                               # =>This Inner Loop Header: Depth=1
	movslq	-4(%rsi), %rbp
	addq	%rdi, %rbp
	movss	(%rax,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	movss	%xmm0, -4(%rdx)
	movslq	(%rsi), %rbp
	addq	%rdi, %rbp
	movss	(%rax,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB4_70
.LBB4_72:                               # %part_imp_match_out_vead_gapmapQ.exit
	cmpl	$1, outgap(%rip)
	jne	.LBB4_73
.LBB4_83:
	movq	partQ__align.ogcp1g(%rip), %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	partQ__align.og_h_dg_n2_p(%rip), %rax
	mulss	(%rax), %xmm0
	xorpd	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movq	partQ__align.ogcp2g(%rip), %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	partQ__align.og_h_dg_n1_p(%rip), %rax
	mulss	(%rax), %xmm0
	addss	%xmm1, %xmm0
	movq	partQ__align.fgcp1g(%rip), %rax
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movq	partQ__align.fg_h_dg_n2_p(%rip), %rax
	mulss	(%rax), %xmm1
	addss	%xmm0, %xmm1
	movq	partQ__align.fgcp2g(%rip), %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	partQ__align.fg_h_dg_n1_p(%rip), %rax
	mulss	(%rax), %xmm0
	addss	%xmm1, %xmm0
	movq	partQ__align.initverticalw(%rip), %r14
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r14)
	movq	32(%rsp), %rax          # 8-byte Reload
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	testl	%r15d, %r15d
	jle	.LBB4_100
# BB#84:                                # %.lr.ph686
	movq	partQ__align.gapz_n2(%rip), %rax
	movq	partQ__align.og_t_fg_h_dg_n1_p(%rip), %r9
	movq	partQ__align.fg_t_og_h_dg_n1_p(%rip), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	leaq	1(%r15), %r11
	movl	%r11d, %edx
	leaq	-1(%rdx), %r10
	cmpq	$3, %r10
	jbe	.LBB4_85
# BB#91:                                # %min.iters.checked836
	movl	%r15d, %ecx
	andl	$3, %ecx
	movq	%r10, %rbx
	subq	%rcx, %rbx
	je	.LBB4_85
# BB#92:                                # %vector.memcheck863
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	leaq	4(%rax), %r8
	leaq	4(%r14), %rbp
	leaq	(%r14,%rdx,4), %r13
	movq	56(%rsp), %rdi          # 8-byte Reload
	leaq	4(%rdi), %rsi
	leaq	(%rdi,%rdx,4), %r15
	cmpq	%rax, %r14
	sbbb	%r12b, %r12b
	cmpq	%r13, %r8
	sbbb	%r8b, %r8b
	andb	%r12b, %r8b
	cmpq	%r9, %rbp
	sbbb	%r12b, %r12b
	cmpq	%r13, %r9
	sbbb	%cl, %cl
	movb	%cl, 72(%rsp)           # 1-byte Spill
	cmpq	%r15, %rbp
	sbbb	%r15b, %r15b
	cmpq	%r13, %rsi
	sbbb	%dil, %dil
	movl	$1, %r13d
	testb	$1, %r8b
	jne	.LBB4_93
# BB#94:                                # %vector.memcheck863
	andb	72(%rsp), %r12b         # 1-byte Folded Reload
	andb	$1, %r12b
	movq	48(%rsp), %r8           # 8-byte Reload
	jne	.LBB4_95
# BB#96:                                # %vector.memcheck863
	andb	%dil, %r15b
	andb	$1, %r15b
	movq	112(%rsp), %r15         # 8-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	jne	.LBB4_86
# BB#97:                                # %vector.body832.preheader
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	(%r9), %xmm1            # xmm1 = mem[0],zero,zero,zero
	mulps	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	1(%rbx), %r13
	.p2align	4, 0x90
.LBB4_98:                               # %vector.body832
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp), %xmm2
	addps	%xmm1, %xmm2
	movups	%xmm2, (%rbp)
	movups	(%rsi), %xmm3
	mulps	%xmm0, %xmm3
	addps	%xmm2, %xmm3
	movups	%xmm3, (%rbp)
	addq	$16, %rbp
	addq	$16, %rsi
	addq	$-4, %rbx
	jne	.LBB4_98
# BB#99:                                # %middle.block833
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	jne	.LBB4_86
	jmp	.LBB4_100
.LBB4_73:                               # %.preheader630
	testl	%r12d, %r12d
	jle	.LBB4_80
# BB#74:                                # %.lr.ph690
	movslq	offset(%rip), %rax
	leaq	1(%r12), %rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB4_75
# BB#76:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI4_4(%rip), %xmm0
	movq	32(%rsp), %rdx          # 8-byte Reload
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rdx)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB4_78
	jmp	.LBB4_80
.LBB4_85:
	movl	$1, %r13d
.LBB4_86:                               # %scalar.ph834.preheader
	subl	%r13d, %r11d
	testb	$1, %r11b
	movq	%r13, %rdi
	je	.LBB4_88
# BB#87:                                # %scalar.ph834.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%r9), %xmm0
	addss	(%r14,%r13,4), %xmm0
	movss	%xmm0, (%r14,%r13,4)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movq	56(%rsp), %rsi          # 8-byte Reload
	mulss	(%rsi,%r13,4), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r14,%r13,4)
	leaq	1(%r13), %rdi
.LBB4_88:                               # %scalar.ph834.prol.loopexit
	cmpq	%r13, %r10
	je	.LBB4_100
# BB#89:                                # %scalar.ph834.preheader.new
	subq	%rdi, %rdx
	leaq	4(%r14,%rdi,4), %rsi
	movq	56(%rsp), %rbp          # 8-byte Reload
	leaq	4(%rbp,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB4_90:                               # %scalar.ph834
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%r9), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	-4(%rdi), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%r9), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB4_90
.LBB4_100:                              # %.preheader626
	testl	%r12d, %r12d
	jle	.LBB4_101
# BB#102:                               # %.lr.ph683
	movq	partQ__align.gapz_n1(%rip), %rax
	movq	partQ__align.og_t_fg_h_dg_n2_p(%rip), %rcx
	movq	partQ__align.fg_t_og_h_dg_n2_p(%rip), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	leaq	1(%r12), %r11
	movl	%r11d, %edx
	leaq	-1(%rdx), %r10
	cmpq	$3, %r10
	jbe	.LBB4_103
# BB#110:                               # %min.iters.checked878
	movl	%r12d, %r9d
	andl	$3, %r9d
	movq	%r10, %rdi
	subq	%r9, %rdi
	je	.LBB4_103
# BB#111:                               # %vector.memcheck907
	leaq	4(%rax), %r14
	movq	32(%rsp), %r8           # 8-byte Reload
	leaq	4(%r8), %rsi
	leaq	(%r8,%rdx,4), %rbx
	movq	56(%rsp), %r15          # 8-byte Reload
	leaq	4(%r15), %rbp
	leaq	(%r15,%rdx,4), %r12
	cmpq	%rax, %r8
	sbbb	%r15b, %r15b
	cmpq	%rbx, %r14
	sbbb	%r8b, %r8b
	andb	%r15b, %r8b
	cmpq	%rcx, %rsi
	sbbb	%r15b, %r15b
	cmpq	%rbx, %rcx
	sbbb	%r13b, %r13b
	cmpq	%r12, %rsi
	sbbb	%r14b, %r14b
	cmpq	%rbx, %rbp
	sbbb	%r12b, %r12b
	movl	$1, %ebx
	testb	$1, %r8b
	jne	.LBB4_112
# BB#113:                               # %vector.memcheck907
	andb	%r13b, %r15b
	andb	$1, %r15b
	movq	48(%rsp), %r8           # 8-byte Reload
	jne	.LBB4_114
# BB#115:                               # %vector.memcheck907
	andb	%r12b, %r14b
	andb	$1, %r14b
	movl	504(%rsp), %r14d
	movq	112(%rsp), %r15         # 8-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	472(%rsp), %r13
	jne	.LBB4_105
# BB#116:                               # %vector.body874.preheader
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulps	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	1(%rdi), %rbx
	.p2align	4, 0x90
.LBB4_117:                              # %vector.body874
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rsi), %xmm2
	addps	%xmm1, %xmm2
	movups	%xmm2, (%rsi)
	movups	(%rbp), %xmm3
	mulps	%xmm0, %xmm3
	addps	%xmm2, %xmm3
	movups	%xmm3, (%rsi)
	addq	$16, %rsi
	addq	$16, %rbp
	addq	$-4, %rdi
	jne	.LBB4_117
# BB#118:                               # %middle.block875
	testq	%r9, %r9
	jne	.LBB4_105
	jmp	.LBB4_123
.LBB4_103:
	movl	$1, %ebx
	movl	504(%rsp), %r14d
.LBB4_104:                              # %scalar.ph876.preheader
	movq	472(%rsp), %r13
.LBB4_105:                              # %scalar.ph876.preheader
	subl	%ebx, %r11d
	testb	$1, %r11b
	movq	%rbx, %rdi
	je	.LBB4_107
# BB#106:                               # %scalar.ph876.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	movq	32(%rsp), %rsi          # 8-byte Reload
	addss	(%rsi,%rbx,4), %xmm0
	movss	%xmm0, (%rsi,%rbx,4)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movq	56(%rsp), %rdi          # 8-byte Reload
	mulss	(%rdi,%rbx,4), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi,%rbx,4)
	leaq	1(%rbx), %rdi
.LBB4_107:                              # %scalar.ph876.prol.loopexit
	cmpq	%rbx, %r10
	je	.LBB4_123
# BB#108:                               # %scalar.ph876.preheader.new
	subq	%rdi, %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	4(%rsi,%rdi,4), %rsi
	movq	56(%rsp), %rbp          # 8-byte Reload
	leaq	4(%rbp,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB4_109:                              # %scalar.ph876
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	-4(%rdi), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB4_109
	jmp	.LBB4_123
.LBB4_101:                              # %.loopexit627.thread
	movq	partQ__align.m(%rip), %r9
	movl	$0, (%r9)
	movb	$1, 128(%rsp)           # 1-byte Folded Spill
	movl	504(%rsp), %r14d
	movq	472(%rsp), %r13
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB4_140
.LBB4_75:
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB4_80
.LBB4_78:                               # %.lr.ph690.new
	subq	%rdx, %rcx
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	4(%rsi,%rdx,4), %rsi
	leaq	1(%rdx), %rdi
	imull	%eax, %edx
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI4_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB4_79:                               # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%eax, %ebp
	addq	$-2, %rcx
	jne	.LBB4_79
.LBB4_80:                               # %.preheader628
	testl	%r15d, %r15d
	jle	.LBB4_123
# BB#81:                                # %.lr.ph688
	movq	partQ__align.initverticalw(%rip), %rsi
	movslq	offset(%rip), %rax
	leaq	1(%r15), %rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB4_82
# BB#119:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI4_4(%rip), %xmm0
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rsi)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB4_121
	jmp	.LBB4_123
.LBB4_82:
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB4_123
.LBB4_121:                              # %.lr.ph688.new
	subq	%rdx, %rcx
	leaq	4(%rsi,%rdx,4), %rsi
	leaq	1(%rdx), %rdi
	imull	%eax, %edx
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI4_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB4_122:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%eax, %ebp
	addq	$-2, %rcx
	jne	.LBB4_122
.LBB4_123:                              # %.loopexit627
	movq	partQ__align.m(%rip), %r9
	movl	$0, (%r9)
	testl	%r12d, %r12d
	setle	128(%rsp)               # 1-byte Folded Spill
	jle	.LBB4_124
# BB#125:                               # %.lr.ph680
	movq	partQ__align.mp(%rip), %rax
	movss	.LCPI4_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	leaq	1(%r12), %rbp
	movl	%ebp, %edx
	leaq	-1(%rdx), %r10
	cmpq	$8, %r10
	jae	.LBB4_127
# BB#126:
	movl	$1, %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB4_135
.LBB4_124:
	movb	$1, 128(%rsp)           # 1-byte Folded Spill
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB4_140
.LBB4_127:                              # %min.iters.checked924
	movl	%r12d, %r8d
	andl	$7, %r8d
	movq	%r10, %rdi
	subq	%r8, %rdi
	je	.LBB4_128
# BB#129:                               # %vector.memcheck939
	leaq	4(%r9), %rcx
	movq	32(%rsp), %rbx          # 8-byte Reload
	leaq	-4(%rbx,%rdx,4), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB4_132
# BB#130:                               # %vector.memcheck939
	leaq	(%r9,%rdx,4), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB4_132
# BB#131:
	movl	$1, %esi
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %rdi
	jmp	.LBB4_135
.LBB4_22:                               # %vector.body.preheader
	leaq	16(%rcx), %rax
	leaq	16(%rdx), %rbx
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB4_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rax), %xmm0
	movupd	(%rax), %xmm1
	movupd	%xmm0, -16(%rbx)
	movupd	%xmm1, (%rbx)
	addq	$32, %rax
	addq	$32, %rbx
	addq	$-4, %rdi
	jne	.LBB4_23
# BB#24:                                # %middle.block
	testl	%r9d, %r9d
	jne	.LBB4_13
	jmp	.LBB4_25
.LBB4_37:                               # %vector.body801.preheader
	leaq	16(%r11,%r9,8), %rax
	leaq	16(%rsi), %rcx
	movq	%rdi, %rdx
	.p2align	4, 0x90
.LBB4_38:                               # %vector.body801
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rax), %xmm0
	movupd	(%rax), %xmm1
	movupd	%xmm0, -16(%rcx)
	movupd	%xmm1, (%rcx)
	addq	$32, %rax
	addq	$32, %rcx
	addq	$-4, %rdx
	jne	.LBB4_38
# BB#39:                                # %middle.block802
	testl	%ebp, %ebp
	jne	.LBB4_28
	jmp	.LBB4_40
.LBB4_128:
	movl	$1, %esi
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB4_135
.LBB4_132:                              # %vector.ph940
	leaq	1(%rdi), %rsi
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	xorl	%ecx, %ecx
	xorpd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB4_133:                              # %vector.body920
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm2, 4(%rax,%rcx,4)
	movupd	%xmm2, 20(%rax,%rcx,4)
	movups	(%rbx,%rcx,4), %xmm3
	movups	16(%rbx,%rcx,4), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm1, %xmm4
	movups	%xmm3, 4(%r9,%rcx,4)
	movups	%xmm4, 20(%r9,%rcx,4)
	addq	$8, %rcx
	cmpq	%rcx, %rdi
	jne	.LBB4_133
# BB#134:                               # %middle.block921
	testq	%r8, %r8
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %rdi
	je	.LBB4_140
.LBB4_135:                              # %scalar.ph922.preheader
	subl	%esi, %ebp
	subq	%rsi, %r10
	andq	$3, %rbp
	je	.LBB4_138
# BB#136:                               # %scalar.ph922.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB4_137:                              # %scalar.ph922.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rax,%rsi,4)
	movss	-4(%rdi,%rsi,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r9,%rsi,4)
	incq	%rsi
	incq	%rbp
	jne	.LBB4_137
.LBB4_138:                              # %scalar.ph922.prol.loopexit
	cmpq	$3, %r10
	jb	.LBB4_140
	.p2align	4, 0x90
.LBB4_139:                              # %scalar.ph922
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rax,%rsi,4)
	movss	-4(%rdi,%rsi,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r9,%rsi,4)
	movl	$0, 4(%rax,%rsi,4)
	movss	(%rdi,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%r9,%rsi,4)
	movl	$0, 8(%rax,%rsi,4)
	movss	4(%rdi,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 8(%r9,%rsi,4)
	movl	$0, 12(%rax,%rsi,4)
	movss	8(%rdi,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 12(%r9,%rsi,4)
	addq	$4, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB4_139
.LBB4_140:                              # %._crit_edge681
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	testl	%r12d, %r12d
	xorps	%xmm0, %xmm0
	je	.LBB4_142
# BB#141:
	movq	%r12, %rcx
	shlq	$32, %rcx
	addq	%rax, %rcx
	sarq	$30, %rcx
	movss	(%rdi,%rcx), %xmm0      # xmm0 = mem[0],zero,zero,zero
.LBB4_142:
	movq	partQ__align.lastverticalw(%rip), %rbx
	movss	%xmm0, (%rbx)
	movl	outgap(%rip), %ebp
	cmpl	$1, %ebp
	movl	%r15d, %edx
	sbbl	$-1, %edx
	cmpl	$2, %edx
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	jl	.LBB4_143
# BB#144:                               # %.lr.ph672
	movl	%ebp, 236(%rsp)         # 4-byte Spill
	testq	%r13, %r13
	setne	%cl
	movq	partQ__align.initverticalw(%rip), %rsi
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	movq	partQ__align.cpmx1(%rip), %rsi
	movq	partQ__align.floatwork(%rip), %rbp
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movq	partQ__align.intwork(%rip), %rbp
	movq	%rbp, 160(%rsp)         # 8-byte Spill
	testl	%r12d, %r12d
	setg	%bl
	andb	%cl, %bl
	movb	%bl, 40(%rsp)           # 1-byte Spill
	movq	impmtx(%rip), %rcx
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	movl	%r12d, %ecx
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	mulss	.LCPI4_5(%rip), %xmm12
	movq	partQ__align.ijp(%rip), %rcx
	movq	%rcx, 216(%rsp)         # 8-byte Spill
	movq	partQ__align.mp(%rip), %rcx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movq	partQ__align.fg_t_og_h_dg_n2_p(%rip), %rcx
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	movq	partQ__align.og_t_fg_h_dg_n2_p(%rip), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	partQ__align.og_h_dg_n2_p(%rip), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%r12, %rcx
	shlq	$32, %rcx
	addq	%rax, %rcx
	sarq	$32, %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movl	%edx, %eax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movl	%r12d, %eax
	andl	$1, %eax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	xorps	%xmm4, %xmm4
	movq	partQ__align.fg_h_dg_n2_p(%rip), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	partQ__align.gapz_n2(%rip), %r11
	movq	partQ__align.fgcp2g(%rip), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	partQ__align.ogcp2g(%rip), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	partQ__align.fg_t_og_h_dg_n1_p(%rip), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	partQ__align.og_t_fg_h_dg_n1_p(%rip), %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movq	partQ__align.og_h_dg_n1_p(%rip), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movq	partQ__align.fg_h_dg_n1_p(%rip), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movq	partQ__align.gapz_n1(%rip), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	partQ__align.fgcp1g(%rip), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movq	partQ__align.ogcp1g(%rip), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movq	528(%rsp), %rax
	leaq	4(%rax), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	leal	-1(%r12), %r12d
	movl	$1, %edx
	movq	136(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_145:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_146 Depth 2
                                        #       Child Loop BB4_147 Depth 3
                                        #     Child Loop BB4_150 Depth 2
                                        #       Child Loop BB4_152 Depth 3
                                        #     Child Loop BB4_160 Depth 2
                                        #     Child Loop BB4_163 Depth 2
	movq	%rcx, %r13
	movq	%rdi, %rcx
	movq	%rdx, %r15
	leaq	-1(%r15), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	-4(%rax,%r15,4), %eax
	movq	%rcx, %r10
	movl	%eax, (%rcx)
	movl	$n_dis_consweight_multi, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_146:                              #   Parent Loop BB4_145 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_147 Depth 3
	movl	$0, 304(%rsp,%rdx,4)
	xorps	%xmm0, %xmm0
	movl	$1, %ecx
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB4_147:                              #   Parent Loop BB4_145 Depth=1
                                        #     Parent Loop BB4_146 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-8(%rsi,%rcx,8), %rbp
	movss	(%rdi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rbp,%r15,4), %xmm1
	addss	%xmm0, %xmm1
	movss	104(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movq	(%rsi,%rcx,8), %rbp
	mulss	(%rbp,%r15,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rdi
	addq	$2, %rcx
	cmpq	$27, %rcx
	jne	.LBB4_147
# BB#148:                               #   in Loop: Header=BB4_146 Depth=2
	movss	%xmm0, 304(%rsp,%rdx,4)
	incq	%rdx
	addq	$4, %rax
	cmpq	$26, %rdx
	jne	.LBB4_146
# BB#149:                               # %.preheader.i
                                        #   in Loop: Header=BB4_145 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	movl	%eax, %r8d
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	%r13, %rbx
	je	.LBB4_154
	.p2align	4, 0x90
.LBB4_150:                              # %.lr.ph84.i
                                        #   Parent Loop BB4_145 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_152 Depth 3
	decl	%r8d
	movl	$0, (%rbx)
	movq	(%rax), %rcx
	movl	(%rcx), %ebp
	testl	%ebp, %ebp
	js	.LBB4_153
# BB#151:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB4_150 Depth=2
	movq	(%rdx), %rdi
	addq	$4, %rcx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_152:                              # %.lr.ph.i585
                                        #   Parent Loop BB4_145 Depth=1
                                        #     Parent Loop BB4_150 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ebp, %rbp
	movss	304(%rsp,%rbp,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi), %xmm1
	addq	$4, %rdi
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rbx)
	movl	(%rcx), %ebp
	addq	$4, %rcx
	testl	%ebp, %ebp
	jns	.LBB4_152
.LBB4_153:                              # %._crit_edge.i586
                                        #   in Loop: Header=BB4_150 Depth=2
	addq	$8, %rax
	addq	$8, %rdx
	addq	$4, %rbx
	testl	%r8d, %r8d
	jne	.LBB4_150
.LBB4_154:                              # %match_calc.exit
                                        #   in Loop: Header=BB4_145 Depth=1
	cmpb	$0, 40(%rsp)            # 1-byte Folded Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	je	.LBB4_161
# BB#155:                               # %.lr.ph.i615
                                        #   in Loop: Header=BB4_145 Depth=1
	movq	520(%rsp), %rax
	movslq	(%rax,%r15,4), %rax
	movslq	488(%rsp), %rcx
	addq	%rax, %rcx
	cmpq	$0, 192(%rsp)           # 8-byte Folded Reload
	movq	224(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	jne	.LBB4_157
# BB#156:                               #   in Loop: Header=BB4_145 Depth=1
	xorl	%edi, %edi
	cmpq	$1, 184(%rsp)           # 8-byte Folded Reload
	jne	.LBB4_159
	jmp	.LBB4_161
	.p2align	4, 0x90
.LBB4_157:                              #   in Loop: Header=BB4_145 Depth=1
	movq	528(%rsp), %rcx
	movslq	(%rcx), %rcx
	movslq	%r14d, %rdx
	addq	%rcx, %rdx
	movss	(%rax,%rdx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%r13), %xmm0
	movss	%xmm0, (%r13)
	movl	$1, %edi
	cmpq	$1, 184(%rsp)           # 8-byte Folded Reload
	je	.LBB4_161
.LBB4_159:                              # %.lr.ph.i615.new
                                        #   in Loop: Header=BB4_145 Depth=1
	movq	184(%rsp), %rcx         # 8-byte Reload
	subq	%rdi, %rcx
	leaq	4(%r13,%rdi,4), %rdx
	movq	240(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rdi,4), %rbp
	.p2align	4, 0x90
.LBB4_160:                              #   Parent Loop BB4_145 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	-4(%rbp), %rdi
	movslq	%r14d, %rbx
	addq	%rbx, %rdi
	movss	(%rax,%rdi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	movss	%xmm0, -4(%rdx)
	movslq	(%rbp), %rdi
	addq	%rbx, %rdi
	movss	(%rax,%rdi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rdx
	addq	$8, %rbp
	addq	$-2, %rcx
	jne	.LBB4_160
.LBB4_161:                              # %part_imp_match_out_vead_gapmapQ.exit620
                                        #   in Loop: Header=BB4_145 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r15,4), %eax
	movl	%eax, (%r13)
	movq	%r10, %rcx
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm2
	addss	%xmm0, %xmm2
	movss	%xmm2, partQ__align.mi(%rip)
	leaq	1(%r15), %rdx
	xorl	%r10d, %r10d
	cmpb	$0, 128(%rsp)           # 1-byte Folded Reload
	movq	88(%rsp), %rbx          # 8-byte Reload
	jne	.LBB4_173
# BB#162:                               # %.lr.ph665.preheader
                                        #   in Loop: Header=BB4_145 Depth=1
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	movq	296(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r15,4), %xmm8    # xmm8 = mem[0],zero,zero,zero
	movq	288(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r15,4), %xmm9    # xmm9 = mem[0],zero,zero,zero
	movq	280(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r15,4), %xmm10   # xmm10 = mem[0],zero,zero,zero
	movq	272(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r15,4), %xmm11   # xmm11 = mem[0],zero,zero,zero
	movq	264(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r15,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	movss	4(%rax,%r15,4), %xmm6   # xmm6 = mem[0],zero,zero,zero
	movq	256(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r15,4), %xmm7    # xmm7 = mem[0],zero,zero,zero
	movq	248(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r15,8), %r14
	xorl	%ebp, %ebp
	movl	$-1, %r8d
	xorl	%r10d, %r10d
	movq	208(%rsp), %rdx         # 8-byte Reload
	movq	200(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB4_163
	.p2align	4, 0x90
.LBB4_477:                              # %..lr.ph665_crit_edge
                                        #   in Loop: Header=BB4_163 Depth=2
	movss	4(%rcx,%rbp,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	incq	%rbp
	decl	%r8d
.LBB4_163:                              # %.lr.ph665
                                        #   Parent Loop BB4_145 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	movss	4(%rax,%rbp,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm0, %xmm3
	movq	120(%rsp), %rax         # 8-byte Reload
	movss	4(%rax,%rbp,4), %xmm4   # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm3, %xmm4
	movq	72(%rsp), %rax          # 8-byte Reload
	movss	4(%rax,%rbp,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm3
	addss	%xmm4, %xmm3
	movq	104(%rsp), %rax         # 8-byte Reload
	movss	4(%rax,%rbp,4), %xmm4   # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm4
	addss	%xmm3, %xmm4
	movl	$0, 4(%r14,%rbp,4)
	movss	4(%rdi,%rbp,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	addss	%xmm2, %xmm3
	ucomiss	%xmm4, %xmm3
	jbe	.LBB4_165
# BB#164:                               #   in Loop: Header=BB4_163 Depth=2
	leal	(%r10,%r8), %eax
	movl	%eax, 4(%r14,%rbp,4)
	movaps	%xmm3, %xmm4
.LBB4_165:                              #   in Loop: Header=BB4_163 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	movss	4(%rax,%rbp,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	addss	%xmm3, %xmm0
	ucomiss	%xmm2, %xmm0
	jb	.LBB4_167
# BB#166:                               #   in Loop: Header=BB4_163 Depth=2
	movss	%xmm0, partQ__align.mi(%rip)
	movaps	%xmm0, %xmm2
	movl	%ebp, %r10d
.LBB4_167:                              #   in Loop: Header=BB4_163 Depth=2
	movss	8(%r11,%rbp,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	movss	4(%r9,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm3
	ucomiss	%xmm4, %xmm3
	jbe	.LBB4_169
# BB#168:                               #   in Loop: Header=BB4_163 Depth=2
	movl	%r15d, %eax
	subl	4(%rdx,%rbp,4), %eax
	movl	%eax, 4(%r14,%rbp,4)
	movaps	%xmm3, %xmm4
.LBB4_169:                              #   in Loop: Header=BB4_163 Depth=2
	movss	4(%r11,%rbp,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	addss	(%rcx,%rbp,4), %xmm3
	ucomiss	%xmm0, %xmm3
	jb	.LBB4_171
# BB#170:                               #   in Loop: Header=BB4_163 Depth=2
	movss	%xmm3, 4(%r9,%rbp,4)
	movq	136(%rsp), %rax         # 8-byte Reload
	movl	%eax, 4(%rdx,%rbp,4)
.LBB4_171:                              #   in Loop: Header=BB4_163 Depth=2
	movss	4(%r13,%rbp,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm0
	movss	%xmm0, 4(%r13,%rbp,4)
	cmpl	%ebp, %r12d
	jne	.LBB4_477
# BB#172:                               #   in Loop: Header=BB4_145 Depth=1
	movq	48(%rsp), %r8           # 8-byte Reload
	movl	504(%rsp), %r14d
	movq	168(%rsp), %rdx         # 8-byte Reload
.LBB4_173:                              # %._crit_edge666
                                        #   in Loop: Header=BB4_145 Depth=1
	movq	152(%rsp), %rax         # 8-byte Reload
	movl	(%r13,%rax,4), %eax
	movl	%eax, (%rbx,%r15,4)
	cmpq	176(%rsp), %rdx         # 8-byte Folded Reload
	movq	%r13, %rdi
	jne	.LBB4_145
# BB#174:                               # %._crit_edge673
	movss	%xmm4, 192(%rsp)        # 4-byte Spill
	movl	%r10d, partQ__align.mpi(%rip)
	movq	112(%rsp), %r15         # 8-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	movl	236(%rsp), %ebp         # 4-byte Reload
	testl	%ebp, %ebp
	jne	.LBB4_190
	jmp	.LBB4_176
.LBB4_143:
	xorps	%xmm0, %xmm0
	movss	%xmm0, 192(%rsp)        # 4-byte Spill
	movq	%rdi, %r13
	testl	%ebp, %ebp
	jne	.LBB4_190
.LBB4_176:                              # %.preheader625
	movl	%ebp, %r9d
	cmpb	$0, 128(%rsp)           # 1-byte Folded Reload
	jne	.LBB4_183
# BB#177:                               # %.lr.ph639
	movslq	offset(%rip), %rax
	movq	%r12, %rdx
	incq	%rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB4_178
# BB#179:
	leal	-1(%r12), %edx
	imull	%eax, %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	mulsd	.LCPI4_4(%rip), %xmm0
	movss	4(%r13), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%r13)
	movl	$2, %ebp
	cmpq	$2, %rcx
	jne	.LBB4_181
	jmp	.LBB4_183
.LBB4_178:
	movl	$1, %ebp
	cmpq	$2, %rcx
	je	.LBB4_183
.LBB4_181:                              # %.lr.ph639.new
	subq	%rbp, %rcx
	movl	%r12d, %edx
	subl	%ebp, %edx
	imull	%eax, %edx
	leaq	4(%r13,%rbp,4), %rsi
	leal	-1(%r12), %edi
	subl	%ebp, %edi
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI4_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB4_182:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	subl	%eax, %ebp
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB4_182
.LBB4_183:                              # %.preheader624
	testl	%r15d, %r15d
	movq	88(%rsp), %rbx          # 8-byte Reload
	movl	%r9d, %ebp
	jle	.LBB4_190
# BB#184:                               # %.lr.ph637
	xorps	%xmm0, %xmm0
	cvtsi2sdl	offset(%rip), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r15d, %xmm1
	incq	%r15
	movl	%r15d, %eax
	testb	$1, %r15b
	jne	.LBB4_185
# BB#186:
	movsd	.LCPI4_4(%rip), %xmm2   # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movss	4(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm2, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, 4(%rbx)
	movl	$2, %ecx
	cmpq	$2, %rax
	jne	.LBB4_188
	jmp	.LBB4_190
.LBB4_185:
	movl	$1, %ecx
	cmpq	$2, %rax
	je	.LBB4_190
.LBB4_188:
	movsd	.LCPI4_2(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB4_189:                              # =>This Inner Loop Header: Depth=1
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%ecx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	(%rbx,%rcx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, (%rbx,%rcx,4)
	leal	1(%rcx), %edx
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%edx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	4(%rbx,%rcx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, 4(%rbx,%rcx,4)
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jne	.LBB4_189
.LBB4_190:                              # %.loopexit
	movq	partQ__align.mseq1(%rip), %r12
	movq	partQ__align.mseq2(%rip), %r15
	movq	partQ__align.ijp(%rip), %r14
	movq	(%r8), %rdi
	callq	strlen
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpq	$0, 472(%rsp)
	movq	%r14, 184(%rsp)         # 8-byte Spill
	je	.LBB4_331
# BB#191:
	cmpl	$1, %ebp
	movq	144(%rsp), %r11         # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	je	.LBB4_192
# BB#203:
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	80(%rsp), %rsi          # 8-byte Reload
	testl	%esi, %esi
	jle	.LBB4_209
# BB#204:                               # %.lr.ph75.i
	movslq	%esi, %rax
	movslq	40(%rsp), %rcx          # 4-byte Folded Reload
	movl	%eax, %edx
	addq	$4, %rbx
	decq	%rdx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movaps	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB4_207
	jmp	.LBB4_206
	.p2align	4, 0x90
.LBB4_478:                              # %._crit_edge.i596
                                        #   in Loop: Header=BB4_207 Depth=1
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rbx
	decq	%rdx
	decl	%esi
	ucomiss	%xmm1, %xmm0
	jb	.LBB4_207
.LBB4_206:
	movq	(%r14,%rax,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movaps	%xmm0, %xmm1
.LBB4_207:                              # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	jne	.LBB4_478
# BB#208:
	movaps	%xmm1, %xmm0
.LBB4_209:                              # %.preheader16.i
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_192
# BB#210:                               # %.lr.ph71.i
	movslq	80(%rsp), %rax          # 4-byte Folded Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	movslq	%esi, %rcx
	movl	%ecx, %edx
	testb	$1, %sil
	jne	.LBB4_212
# BB#211:
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB4_216
	jmp	.LBB4_192
.LBB4_331:
	cmpl	$1, %ebp
	movq	144(%rsp), %r9          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	80(%rsp), %r10          # 8-byte Reload
	je	.LBB4_332
# BB#343:
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	testl	%r10d, %r10d
	jle	.LBB4_349
# BB#344:                               # %.lr.ph74.i
	movslq	%r10d, %rax
	movslq	40(%rsp), %rcx          # 4-byte Folded Reload
	movl	%eax, %edx
	addq	$4, %rbx
	decq	%rdx
	movl	%r10d, %esi
	movaps	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB4_347
	jmp	.LBB4_346
	.p2align	4, 0x90
.LBB4_479:                              # %._crit_edge.i
                                        #   in Loop: Header=BB4_347 Depth=1
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rbx
	decq	%rdx
	decl	%esi
	ucomiss	%xmm1, %xmm0
	jb	.LBB4_347
.LBB4_346:
	movq	(%r14,%rax,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movaps	%xmm0, %xmm1
.LBB4_347:                              # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	jne	.LBB4_479
# BB#348:
	movaps	%xmm1, %xmm0
.LBB4_349:                              # %.preheader15.i
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_332
# BB#350:                               # %.lr.ph70.i
	movslq	%r10d, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	movslq	%esi, %rcx
	movl	%ecx, %edx
	testb	$1, %sil
	jne	.LBB4_352
# BB#351:
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB4_356
	jmp	.LBB4_332
.LBB4_212:
	movss	(%r13), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB4_214
# BB#213:
	movl	$1, %esi
	cmpq	$1, %rdx
	jne	.LBB4_216
	jmp	.LBB4_192
.LBB4_352:
	movss	(%r13), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB4_354
# BB#353:
	movl	$1, %esi
	cmpq	$1, %rdx
	jne	.LBB4_356
	jmp	.LBB4_332
.LBB4_214:
	movq	40(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	negl	%esi
	movq	(%r14,%rax,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movl	$1, %esi
	movaps	%xmm1, %xmm0
	cmpq	$1, %rdx
	je	.LBB4_192
.LBB4_216:                              # %.lr.ph71.i.new
	movq	40(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	negl	%edi
	.p2align	4, 0x90
.LBB4_217:                              # =>This Inner Loop Header: Depth=1
	movss	(%r13,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB4_219
# BB#218:                               #   in Loop: Header=BB4_217 Depth=1
	leal	(%rdi,%rsi), %ebp
	movq	(%r14,%rax,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB4_219:                              #   in Loop: Header=BB4_217 Depth=1
	movss	4(%r13,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB4_221
# BB#220:                               #   in Loop: Header=BB4_217 Depth=1
	leal	1(%rdi,%rsi), %ebp
	movq	(%r14,%rax,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB4_221:                              #   in Loop: Header=BB4_217 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB4_217
.LBB4_192:                              # %.preheader15.i587
	cmpl	$0, 80(%rsp)            # 4-byte Folded Reload
	js	.LBB4_198
# BB#193:                               # %.lr.ph68.preheader.i
	movq	80(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	movl	%esi, %eax
	leaq	-1(%rax), %rcx
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB4_195
	.p2align	4, 0x90
.LBB4_194:                              # %.lr.ph68.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB4_194
.LBB4_195:                              # %.lr.ph68.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB4_198
# BB#196:                               # %.lr.ph68.preheader.i.new
	negq	%rax
	leaq	4(%rdx,%rax), %rax
	leaq	56(%r14,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_197:                              # %.lr.ph68.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %ebp
	leal	-3(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-48(%rcx,%rsi,8), %rdi
	leal	-2(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-40(%rcx,%rsi,8), %rdi
	leal	-1(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-32(%rcx,%rsi,8), %rdi
	movl	%ebp, (%rdi)
	movq	-24(%rcx,%rsi,8), %rdi
	leal	1(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-16(%rcx,%rsi,8), %rdi
	leal	2(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-8(%rcx,%rsi,8), %rdi
	leal	3(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	(%rcx,%rsi,8), %rdi
	leal	4(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	leaq	8(%rax,%rsi), %rdi
	addq	$8, %rsi
	cmpq	$4, %rdi
	jne	.LBB4_197
.LBB4_198:                              # %.preheader14.i598
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	js	.LBB4_226
# BB#199:                               # %.lr.ph66.i
	movq	(%r14), %rcx
	movq	40(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	movl	%esi, %eax
	cmpq	$7, %rax
	jbe	.LBB4_200
# BB#222:                               # %min.iters.checked977
	andl	$7, %esi
	movq	%rax, %rbp
	subq	%rsi, %rbp
	je	.LBB4_200
# BB#223:                               # %vector.body973.preheader
	leaq	16(%rcx), %rdi
	movdqa	.LCPI4_6(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI4_7(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI4_8(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB4_224:                              # %vector.body973
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdi)
	movdqu	%xmm4, (%rdi)
	paddd	%xmm3, %xmm0
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB4_224
# BB#225:                               # %middle.block974
	testq	%rsi, %rsi
	jne	.LBB4_201
	jmp	.LBB4_226
.LBB4_200:
	xorl	%ebp, %ebp
.LBB4_201:                              # %scalar.ph975.preheader
	movl	%ebp, %edx
	notl	%edx
	leaq	(%rcx,%rbp,4), %rcx
	subq	%rbp, %rax
	.p2align	4, 0x90
.LBB4_202:                              # %scalar.ph975
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rcx)
	decl	%edx
	addq	$4, %rcx
	decq	%rax
	jne	.LBB4_202
.LBB4_226:                              # %.preheader13.i599
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_232
# BB#227:                               # %.lr.ph64.i
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rax), %eax
	cltq
	movl	28(%rsp), %ecx          # 4-byte Reload
	leaq	-1(%rcx), %rdx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	andq	$3, %rdi
	je	.LBB4_229
	.p2align	4, 0x90
.LBB4_228:                              # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rsi,8), %rbp
	leaq	(%rbp,%rax), %rbx
	movq	%rbx, (%r12,%rsi,8)
	movb	$0, (%rbp,%rax)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB4_228
.LBB4_229:                              # %.prol.loopexit1051
	cmpq	$3, %rdx
	jb	.LBB4_232
# BB#230:                               # %.lr.ph64.i.new
	subq	%rsi, %rcx
	leaq	24(%r12,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB4_231:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -24(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-16(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -16(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-8(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -8(%rdx)
	movb	$0, (%rsi,%rax)
	movq	(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, (%rdx)
	movb	$0, (%rsi,%rax)
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB4_231
.LBB4_232:                              # %.preheader12.i601
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_238
# BB#233:                               # %.lr.ph61.i
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rax), %eax
	cltq
	movl	24(%rsp), %ecx          # 4-byte Reload
	leaq	-1(%rcx), %rdx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	andq	$3, %rdi
	je	.LBB4_235
	.p2align	4, 0x90
.LBB4_234:                              # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rsi,8), %rbp
	leaq	(%rbp,%rax), %rbx
	movq	%rbx, (%r15,%rsi,8)
	movb	$0, (%rbp,%rax)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB4_234
.LBB4_235:                              # %.prol.loopexit1046
	cmpq	$3, %rdx
	jb	.LBB4_238
# BB#236:                               # %.lr.ph61.i.new
	subq	%rsi, %rcx
	leaq	24(%r15,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB4_237:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -24(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-16(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -16(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-8(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -8(%rdx)
	movb	$0, (%rsi,%rax)
	movq	(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, (%rdx)
	movb	$0, (%rsi,%rax)
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB4_237
.LBB4_238:                              # %._crit_edge62.i
	movq	480(%rsp), %rax
	movl	$0, (%rax)
	movq	40(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addl	80(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 208(%rsp)         # 4-byte Spill
	js	.LBB4_467
# BB#239:                               # %.lr.ph57.i
	movq	impmtx(%rip), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movl	28(%rsp), %r14d         # 4-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
	leaq	-1(%r9), %r13
	leaq	-1(%r14), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r14, 56(%rsp)          # 8-byte Spill
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	andl	$3, %r14d
	movq	%r9, 72(%rsp)           # 8-byte Spill
                                        # kill: %R9D<def> %R9D<kill> %R9<kill> %R9<def>
	andl	$3, %r9d
	leaq	24(%r15), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	24(%r12), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leaq	24(%r8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	24(%r11), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	xorl	%r10d, %r10d
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	movq	40(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 176(%rsp)         # 4-byte Spill
	movq	%r13, 136(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_240:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_270 Depth 2
                                        #       Child Loop BB4_271 Depth 3
                                        #       Child Loop BB4_274 Depth 3
                                        #     Child Loop BB4_262 Depth 2
                                        #       Child Loop BB4_263 Depth 3
                                        #       Child Loop BB4_266 Depth 3
                                        #     Child Loop BB4_249 Depth 2
                                        #       Child Loop BB4_250 Depth 3
                                        #       Child Loop BB4_253 Depth 3
                                        #       Child Loop BB4_257 Depth 3
                                        #       Child Loop BB4_260 Depth 3
                                        #     Child Loop BB4_302 Depth 2
                                        #       Child Loop BB4_303 Depth 3
                                        #       Child Loop BB4_306 Depth 3
                                        #     Child Loop BB4_292 Depth 2
                                        #       Child Loop BB4_293 Depth 3
                                        #       Child Loop BB4_296 Depth 3
                                        #     Child Loop BB4_281 Depth 2
                                        #       Child Loop BB4_282 Depth 3
                                        #       Child Loop BB4_285 Depth 3
                                        #       Child Loop BB4_287 Depth 3
                                        #       Child Loop BB4_290 Depth 3
                                        #     Child Loop BB4_318 Depth 2
                                        #     Child Loop BB4_321 Depth 2
                                        #     Child Loop BB4_326 Depth 2
                                        #     Child Loop BB4_329 Depth 2
	movl	176(%rsp), %eax         # 4-byte Reload
	movl	%eax, %edx
	movl	%ecx, %esi
	movslq	%esi, %rcx
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 216(%rsp)         # 8-byte Spill
	movq	(%rax,%rcx,8), %rax
	movq	%rdx, %rcx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movslq	%edx, %rcx
	movl	(%rax,%rcx,4), %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	testl	%eax, %eax
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	js	.LBB4_241
# BB#242:                               #   in Loop: Header=BB4_240 Depth=1
	je	.LBB4_244
# BB#243:                               #   in Loop: Header=BB4_240 Depth=1
	movl	%esi, %ecx
	subl	88(%rsp), %ecx          # 4-byte Folded Reload
	jmp	.LBB4_245
	.p2align	4, 0x90
.LBB4_241:                              #   in Loop: Header=BB4_240 Depth=1
	leal	-1(%rsi), %ecx
	jmp	.LBB4_246
	.p2align	4, 0x90
.LBB4_244:                              #   in Loop: Header=BB4_240 Depth=1
	leal	-1(%rsi), %ecx
.LBB4_245:                              #   in Loop: Header=BB4_240 Depth=1
	movl	$-1, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
.LBB4_246:                              #   in Loop: Header=BB4_240 Depth=1
	movl	%esi, %eax
	subl	%ecx, %eax
	decl	%eax
	movl	%ecx, 152(%rsp)         # 4-byte Spill
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	je	.LBB4_277
# BB#247:                               # %.preheader9.lr.ph.i
                                        #   in Loop: Header=BB4_240 Depth=1
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_269
# BB#248:                               # %.preheader9.us.preheader.i
                                        #   in Loop: Header=BB4_240 Depth=1
	movq	%r10, 168(%rsp)         # 8-byte Spill
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movslq	%eax, %r11
	movslq	%ecx, %r10
	jle	.LBB4_262
	.p2align	4, 0x90
.LBB4_249:                              # %.preheader9.us.i.us
                                        #   Parent Loop BB4_240 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_250 Depth 3
                                        #       Child Loop BB4_253 Depth 3
                                        #       Child Loop BB4_257 Depth 3
                                        #       Child Loop BB4_260 Depth 3
	leaq	(%r11,%r10), %rbp
	xorl	%edx, %edx
	testq	%r14, %r14
	je	.LBB4_251
	.p2align	4, 0x90
.LBB4_250:                              #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_249 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r8,%rdx,8), %rsi
	movzbl	(%rsi,%rbp), %ebx
	movq	(%r12,%rdx,8), %rsi
	leaq	-1(%rsi), %rax
	movq	%rax, (%r12,%rdx,8)
	movb	%bl, -1(%rsi)
	incq	%rdx
	cmpq	%rdx, %r14
	jne	.LBB4_250
.LBB4_251:                              # %.prol.loopexit1008
                                        #   in Loop: Header=BB4_249 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_254
# BB#252:                               # %.preheader9.us.i.us.new
                                        #   in Loop: Header=BB4_249 Depth=2
	movq	56(%rsp), %rsi          # 8-byte Reload
	subq	%rdx, %rsi
	movq	120(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdx,8), %r13
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB4_253:                              #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_249 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	-24(%r13), %rbx
	leaq	-1(%rbx), %r8
	movq	%r8, -24(%r13)
	movb	%al, -1(%rbx)
	movq	-16(%rdx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	-16(%r13), %rbx
	leaq	-1(%rbx), %rdi
	movq	%rdi, -16(%r13)
	movb	%al, -1(%rbx)
	movq	-8(%rdx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	-8(%r13), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, -8(%r13)
	movb	%al, -1(%rdi)
	movq	(%rdx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	(%r13), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, (%r13)
	movb	%al, -1(%rdi)
	addq	$32, %r13
	addq	$32, %rdx
	addq	$-4, %rsi
	jne	.LBB4_253
.LBB4_254:                              # %.lr.ph19.us.i.us.preheader
                                        #   in Loop: Header=BB4_249 Depth=2
	xorl	%esi, %esi
	testq	%r9, %r9
	je	.LBB4_255
# BB#256:                               # %.lr.ph19.us.i.us.prol.preheader
                                        #   in Loop: Header=BB4_249 Depth=2
	movq	136(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_257:                              # %.lr.ph19.us.i.us.prol
                                        #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_249 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15,%rsi,8), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, (%r15,%rsi,8)
	movb	$45, -1(%rax)
	incq	%rsi
	cmpq	%rsi, %r9
	jne	.LBB4_257
	jmp	.LBB4_258
	.p2align	4, 0x90
.LBB4_255:                              #   in Loop: Header=BB4_249 Depth=2
	movq	136(%rsp), %r13         # 8-byte Reload
.LBB4_258:                              # %.lr.ph19.us.i.us.prol.loopexit
                                        #   in Loop: Header=BB4_249 Depth=2
	cmpq	$3, %r13
	jb	.LBB4_261
# BB#259:                               # %.lr.ph19.us.i.us.preheader.new
                                        #   in Loop: Header=BB4_249 Depth=2
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rsi, %rdx
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB4_260:                              # %.lr.ph19.us.i.us
                                        #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_249 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rsi), %rax
	leaq	-1(%rax), %rdi
	movq	%rdi, -24(%rsi)
	movb	$45, -1(%rax)
	movq	-16(%rsi), %rax
	leaq	-1(%rax), %rdi
	movq	%rdi, -16(%rsi)
	movb	$45, -1(%rax)
	movq	-8(%rsi), %rax
	leaq	-1(%rax), %rdi
	movq	%rdi, -8(%rsi)
	movb	$45, -1(%rax)
	movq	(%rsi), %rax
	leaq	-1(%rax), %rdi
	movq	%rdi, (%rsi)
	movb	$45, -1(%rax)
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB4_260
.LBB4_261:                              # %._crit_edge.us.i605.loopexit.us
                                        #   in Loop: Header=BB4_249 Depth=2
	decq	%r11
	testl	%r11d, %r11d
	movq	48(%rsp), %r8           # 8-byte Reload
	jne	.LBB4_249
	jmp	.LBB4_268
	.p2align	4, 0x90
.LBB4_262:                              # %.preheader9.us.i
                                        #   Parent Loop BB4_240 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_263 Depth 3
                                        #       Child Loop BB4_266 Depth 3
	leaq	(%r11,%r10), %rbp
	xorl	%ebx, %ebx
	testq	%r14, %r14
	je	.LBB4_264
	.p2align	4, 0x90
.LBB4_263:                              #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_262 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r8,%rbx,8), %rax
	movzbl	(%rax,%rbp), %eax
	movq	(%r12,%rbx,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r12,%rbx,8)
	movb	%al, -1(%rdx)
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB4_263
.LBB4_264:                              # %.prol.loopexit1003
                                        #   in Loop: Header=BB4_262 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_267
# BB#265:                               # %.preheader9.us.i.new
                                        #   in Loop: Header=BB4_262 Depth=2
	movq	56(%rsp), %rsi          # 8-byte Reload
	subq	%rbx, %rsi
	movq	120(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rdx
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB4_266:                              #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_262 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rbx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	-24(%rdx), %rdi
	leaq	-1(%rdi), %rcx
	movq	%rcx, -24(%rdx)
	movb	%al, -1(%rdi)
	movq	-16(%rbx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	-16(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -16(%rdx)
	movb	%al, -1(%rcx)
	movq	-8(%rbx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	-8(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -8(%rdx)
	movb	%al, -1(%rcx)
	movq	(%rbx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, (%rdx)
	movb	%al, -1(%rcx)
	addq	$32, %rdx
	addq	$32, %rbx
	addq	$-4, %rsi
	jne	.LBB4_266
.LBB4_267:                              # %..preheader8_crit_edge.us.i
                                        #   in Loop: Header=BB4_262 Depth=2
	decq	%r11
	testl	%r11d, %r11d
	jne	.LBB4_262
.LBB4_268:                              # %._crit_edge21.loopexit.i
                                        #   in Loop: Header=BB4_240 Depth=1
	movq	168(%rsp), %r10         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	leal	-1(%r10,%rsi), %r10d
	subl	152(%rsp), %r10d        # 4-byte Folded Reload
	movq	144(%rsp), %r11         # 8-byte Reload
	jmp	.LBB4_277
	.p2align	4, 0x90
.LBB4_269:                              # %.preheader9.lr.ph.split.i
                                        #   in Loop: Header=BB4_240 Depth=1
	leal	-1(%r10,%rsi), %r10d
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_276
	.p2align	4, 0x90
.LBB4_270:                              # %.preheader9.us22.i
                                        #   Parent Loop BB4_240 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_271 Depth 3
                                        #       Child Loop BB4_274 Depth 3
	xorl	%esi, %esi
	testq	%r9, %r9
	je	.LBB4_272
	.p2align	4, 0x90
.LBB4_271:                              #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_270 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15,%rsi,8), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%r15,%rsi,8)
	movb	$45, -1(%rcx)
	incq	%rsi
	cmpq	%rsi, %r9
	jne	.LBB4_271
.LBB4_272:                              # %.prol.loopexit
                                        #   in Loop: Header=BB4_270 Depth=2
	cmpq	$3, %r13
	jb	.LBB4_275
# BB#273:                               # %.preheader9.us22.i.new
                                        #   in Loop: Header=BB4_270 Depth=2
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rsi, %rdx
	movq	104(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB4_274:                              #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_270 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -24(%rsi)
	movb	$45, -1(%rcx)
	movq	-16(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -16(%rsi)
	movb	$45, -1(%rcx)
	movq	-8(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -8(%rsi)
	movb	$45, -1(%rcx)
	movq	(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, (%rsi)
	movb	$45, -1(%rcx)
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB4_274
.LBB4_275:                              # %._crit_edge.us30.i
                                        #   in Loop: Header=BB4_270 Depth=2
	decl	%eax
	jne	.LBB4_270
.LBB4_276:                              # %._crit_edge21.loopexit79.i
                                        #   in Loop: Header=BB4_240 Depth=1
	subl	152(%rsp), %r10d        # 4-byte Folded Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
.LBB4_277:                              # %._crit_edge21.i
                                        #   in Loop: Header=BB4_240 Depth=1
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rax), %eax
	movl	%eax, 176(%rsp)         # 4-byte Spill
	cmpl	$-1, %ecx
	je	.LBB4_309
# BB#278:                               # %.preheader7.lr.ph.i
                                        #   in Loop: Header=BB4_240 Depth=1
	notl	%ecx
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	jle	.LBB4_299
# BB#279:                               # %.preheader7.us.preheader.i
                                        #   in Loop: Header=BB4_240 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movslq	%ecx, %r8
	movq	%r10, 168(%rsp)         # 8-byte Spill
	jle	.LBB4_292
# BB#280:                               # %.preheader7.us.i.us.preheader
                                        #   in Loop: Header=BB4_240 Depth=1
	movslq	176(%rsp), %r10         # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB4_281:                              # %.preheader7.us.i.us
                                        #   Parent Loop BB4_240 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_282 Depth 3
                                        #       Child Loop BB4_285 Depth 3
                                        #       Child Loop BB4_287 Depth 3
                                        #       Child Loop BB4_290 Depth 3
	xorl	%esi, %esi
	testq	%r14, %r14
	je	.LBB4_283
	.p2align	4, 0x90
.LBB4_282:                              #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_281 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r12,%rsi,8), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%r12,%rsi,8)
	movb	$45, -1(%rcx)
	incq	%rsi
	cmpq	%rsi, %r14
	jne	.LBB4_282
.LBB4_283:                              # %.prol.loopexit1026
                                        #   in Loop: Header=BB4_281 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_286
# BB#284:                               # %.preheader7.us.i.us.new
                                        #   in Loop: Header=BB4_281 Depth=2
	movq	56(%rsp), %rdx          # 8-byte Reload
	subq	%rsi, %rdx
	movq	120(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB4_285:                              #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_281 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -24(%rsi)
	movb	$45, -1(%rcx)
	movq	-16(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -16(%rsi)
	movb	$45, -1(%rcx)
	movq	-8(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -8(%rsi)
	movb	$45, -1(%rcx)
	movq	(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, (%rsi)
	movb	$45, -1(%rcx)
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB4_285
.LBB4_286:                              # %.lr.ph34.us.i.us
                                        #   in Loop: Header=BB4_281 Depth=2
	leaq	(%r8,%r10), %rbp
	xorl	%ebx, %ebx
	testq	%r9, %r9
	je	.LBB4_288
	.p2align	4, 0x90
.LBB4_287:                              #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_281 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r11,%rbx,8), %rcx
	movzbl	(%rcx,%rbp), %ecx
	movq	(%r15,%rbx,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15,%rbx,8)
	movb	%cl, -1(%rdx)
	incq	%rbx
	cmpq	%rbx, %r9
	jne	.LBB4_287
.LBB4_288:                              # %.prol.loopexit1031
                                        #   in Loop: Header=BB4_281 Depth=2
	cmpq	$3, %r13
	jb	.LBB4_291
# BB#289:                               # %.lr.ph34.us.i.us.new
                                        #   in Loop: Header=BB4_281 Depth=2
	movq	72(%rsp), %rsi          # 8-byte Reload
	subq	%rbx, %rsi
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rdx
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB4_290:                              #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_281 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rbx), %rcx
	movzbl	(%rcx,%rbp), %ecx
	movq	-24(%rdx), %rdi
	leaq	-1(%rdi), %rax
	movq	%rax, -24(%rdx)
	movb	%cl, -1(%rdi)
	movq	-16(%rbx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	-16(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -16(%rdx)
	movb	%al, -1(%rcx)
	movq	-8(%rbx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	-8(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -8(%rdx)
	movb	%al, -1(%rcx)
	movq	(%rbx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, (%rdx)
	movb	%al, -1(%rcx)
	addq	$32, %rdx
	addq	$32, %rbx
	addq	$-4, %rsi
	jne	.LBB4_290
.LBB4_291:                              # %._crit_edge35.us.i.loopexit.us
                                        #   in Loop: Header=BB4_281 Depth=2
	decq	%r8
	testl	%r8d, %r8d
	jne	.LBB4_281
	jmp	.LBB4_298
	.p2align	4, 0x90
.LBB4_292:                              # %.preheader7.us.i
                                        #   Parent Loop BB4_240 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_293 Depth 3
                                        #       Child Loop BB4_296 Depth 3
	xorl	%esi, %esi
	testq	%r14, %r14
	je	.LBB4_294
	.p2align	4, 0x90
.LBB4_293:                              #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_292 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r12,%rsi,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%r12,%rsi,8)
	movb	$45, -1(%rax)
	incq	%rsi
	cmpq	%rsi, %r14
	jne	.LBB4_293
.LBB4_294:                              # %.prol.loopexit1021
                                        #   in Loop: Header=BB4_292 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_297
# BB#295:                               # %.preheader7.us.i.new
                                        #   in Loop: Header=BB4_292 Depth=2
	movq	56(%rsp), %rdx          # 8-byte Reload
	subq	%rsi, %rdx
	movq	120(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB4_296:                              #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_292 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rsi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, -24(%rsi)
	movb	$45, -1(%rax)
	movq	-16(%rsi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, -16(%rsi)
	movb	$45, -1(%rax)
	movq	-8(%rsi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, -8(%rsi)
	movb	$45, -1(%rax)
	movq	(%rsi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rsi)
	movb	$45, -1(%rax)
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB4_296
.LBB4_297:                              # %..preheader_crit_edge.us.i610
                                        #   in Loop: Header=BB4_292 Depth=2
	decq	%r8
	testl	%r8d, %r8d
	jne	.LBB4_292
.LBB4_298:                              # %._crit_edge37.loopexit.i
                                        #   in Loop: Header=BB4_240 Depth=1
	movq	168(%rsp), %r10         # 8-byte Reload
	addl	88(%rsp), %r10d         # 4-byte Folded Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	cmpl	80(%rsp), %esi          # 4-byte Folded Reload
	jne	.LBB4_310
	jmp	.LBB4_312
	.p2align	4, 0x90
.LBB4_299:                              # %.preheader7.lr.ph.split.i
                                        #   in Loop: Header=BB4_240 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_300
# BB#301:                               # %.preheader7.us39.preheader.i
                                        #   in Loop: Header=BB4_240 Depth=1
	movq	%r10, 168(%rsp)         # 8-byte Spill
	movslq	88(%rsp), %r8           # 4-byte Folded Reload
	movslq	176(%rsp), %r10         # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB4_302:                              # %.preheader7.us39.i
                                        #   Parent Loop BB4_240 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_303 Depth 3
                                        #       Child Loop BB4_306 Depth 3
	leaq	(%r8,%r10), %rbp
	xorl	%ebx, %ebx
	testq	%r9, %r9
	je	.LBB4_304
	.p2align	4, 0x90
.LBB4_303:                              #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_302 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r11,%rbx,8), %rcx
	movzbl	(%rcx,%rbp), %ecx
	movq	(%r15,%rbx,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15,%rbx,8)
	movb	%cl, -1(%rdx)
	incq	%rbx
	cmpq	%rbx, %r9
	jne	.LBB4_303
.LBB4_304:                              # %.prol.loopexit1016
                                        #   in Loop: Header=BB4_302 Depth=2
	cmpq	$3, %r13
	jb	.LBB4_307
# BB#305:                               # %.preheader7.us39.i.new
                                        #   in Loop: Header=BB4_302 Depth=2
	movq	72(%rsp), %rsi          # 8-byte Reload
	subq	%rbx, %rsi
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rdx
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB4_306:                              #   Parent Loop BB4_240 Depth=1
                                        #     Parent Loop BB4_302 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rbx), %rcx
	movzbl	(%rcx,%rbp), %ecx
	movq	-24(%rdx), %rdi
	leaq	-1(%rdi), %rax
	movq	%rax, -24(%rdx)
	movb	%cl, -1(%rdi)
	movq	-16(%rbx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	-16(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -16(%rdx)
	movb	%al, -1(%rcx)
	movq	-8(%rbx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	-8(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -8(%rdx)
	movb	%al, -1(%rcx)
	movq	(%rbx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, (%rdx)
	movb	%al, -1(%rcx)
	addq	$32, %rdx
	addq	$32, %rbx
	addq	$-4, %rsi
	jne	.LBB4_306
.LBB4_307:                              # %._crit_edge35.us47.i
                                        #   in Loop: Header=BB4_302 Depth=2
	decq	%r8
	testl	%r8d, %r8d
	movq	96(%rsp), %rsi          # 8-byte Reload
	jne	.LBB4_302
# BB#308:                               # %._crit_edge37.loopexit77.i
                                        #   in Loop: Header=BB4_240 Depth=1
	movq	168(%rsp), %r10         # 8-byte Reload
	addl	88(%rsp), %r10d         # 4-byte Folded Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	cmpl	80(%rsp), %esi          # 4-byte Folded Reload
	jne	.LBB4_310
	jmp	.LBB4_312
.LBB4_300:                              # %.preheader7.preheader.i
                                        #   in Loop: Header=BB4_240 Depth=1
	addl	88(%rsp), %r10d         # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB4_309:                              # %._crit_edge37.i
                                        #   in Loop: Header=BB4_240 Depth=1
	cmpl	80(%rsp), %esi          # 4-byte Folded Reload
	je	.LBB4_312
.LBB4_310:                              # %._crit_edge37.i
                                        #   in Loop: Header=BB4_240 Depth=1
	movq	160(%rsp), %rax         # 8-byte Reload
	cmpl	40(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB4_312
# BB#311:                               #   in Loop: Header=BB4_240 Depth=1
	movq	520(%rsp), %rax
	movq	216(%rsp), %rcx         # 8-byte Reload
	movslq	(%rax,%rcx,4), %rax
	movslq	488(%rsp), %rcx
	addq	%rax, %rcx
	movq	528(%rsp), %rax
	movq	224(%rsp), %rdx         # 8-byte Reload
	movslq	(%rax,%rdx,4), %rax
	movslq	504(%rsp), %rdx
	addq	%rax, %rdx
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movss	(%rax,%rdx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	480(%rsp), %rax
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
.LBB4_312:                              #   in Loop: Header=BB4_240 Depth=1
	testl	%esi, %esi
	jle	.LBB4_467
# BB#313:                               #   in Loop: Header=BB4_240 Depth=1
	cmpl	$0, 160(%rsp)           # 4-byte Folded Reload
	jle	.LBB4_467
# BB#314:                               # %.preheader11.i612
                                        #   in Loop: Header=BB4_240 Depth=1
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_322
# BB#315:                               # %.lr.ph49.i
                                        #   in Loop: Header=BB4_240 Depth=1
	testq	%r14, %r14
	movslq	152(%rsp), %rax         # 4-byte Folded Reload
	je	.LBB4_316
# BB#317:                               # %.prol.preheader1035
                                        #   in Loop: Header=BB4_240 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_318:                              #   Parent Loop BB4_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r8,%rdx,8), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%r12,%rdx,8), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r12,%rdx,8)
	movb	%cl, -1(%rsi)
	incq	%rdx
	cmpq	%rdx, %r14
	jne	.LBB4_318
	jmp	.LBB4_319
	.p2align	4, 0x90
.LBB4_316:                              #   in Loop: Header=BB4_240 Depth=1
	xorl	%edx, %edx
.LBB4_319:                              # %.prol.loopexit1036
                                        #   in Loop: Header=BB4_240 Depth=1
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_322
# BB#320:                               # %.lr.ph49.i.new
                                        #   in Loop: Header=BB4_240 Depth=1
	movq	56(%rsp), %rbx          # 8-byte Reload
	subq	%rdx, %rbx
	movq	120(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rsi
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB4_321:                              #   Parent Loop BB4_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-24(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -24(%rsi)
	movb	%cl, -1(%rdi)
	movq	-16(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-16(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -16(%rsi)
	movb	%cl, -1(%rdi)
	movq	-8(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-8(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -8(%rsi)
	movb	%cl, -1(%rdi)
	movq	(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, (%rsi)
	movb	%cl, -1(%rdi)
	addq	$32, %rsi
	addq	$32, %rdx
	addq	$-4, %rbx
	jne	.LBB4_321
.LBB4_322:                              # %.preheader10.i613
                                        #   in Loop: Header=BB4_240 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_330
# BB#323:                               # %.lr.ph51.i
                                        #   in Loop: Header=BB4_240 Depth=1
	testq	%r9, %r9
	movslq	176(%rsp), %rax         # 4-byte Folded Reload
	je	.LBB4_324
# BB#325:                               # %.prol.preheader1040
                                        #   in Loop: Header=BB4_240 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_326:                              #   Parent Loop BB4_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r11,%rdx,8), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%r15,%rdx,8), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r15,%rdx,8)
	movb	%cl, -1(%rsi)
	incq	%rdx
	cmpq	%rdx, %r9
	jne	.LBB4_326
	jmp	.LBB4_327
	.p2align	4, 0x90
.LBB4_324:                              #   in Loop: Header=BB4_240 Depth=1
	xorl	%edx, %edx
.LBB4_327:                              # %.prol.loopexit1041
                                        #   in Loop: Header=BB4_240 Depth=1
	cmpq	$3, %r13
	jb	.LBB4_330
# BB#328:                               # %.lr.ph51.i.new
                                        #   in Loop: Header=BB4_240 Depth=1
	movq	72(%rsp), %rbx          # 8-byte Reload
	subq	%rdx, %rbx
	movq	104(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rsi
	movq	128(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB4_329:                              #   Parent Loop BB4_240 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-24(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -24(%rsi)
	movb	%cl, -1(%rdi)
	movq	-16(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-16(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -16(%rsi)
	movb	%cl, -1(%rdi)
	movq	-8(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-8(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -8(%rsi)
	movb	%cl, -1(%rdi)
	movq	(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, (%rsi)
	movb	%cl, -1(%rdi)
	addq	$32, %rsi
	addq	$32, %rdx
	addq	$-4, %rbx
	jne	.LBB4_329
.LBB4_330:                              # %._crit_edge52.i
                                        #   in Loop: Header=BB4_240 Depth=1
	addl	$2, %r10d
	cmpl	208(%rsp), %r10d        # 4-byte Folded Reload
	movl	152(%rsp), %ecx         # 4-byte Reload
	jle	.LBB4_240
	jmp	.LBB4_467
.LBB4_354:
	movq	40(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	negl	%esi
	movq	(%r14,%rax,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movl	$1, %esi
	movaps	%xmm1, %xmm0
	cmpq	$1, %rdx
	je	.LBB4_332
.LBB4_356:                              # %.lr.ph70.i.new
	movq	40(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	negl	%edi
	.p2align	4, 0x90
.LBB4_357:                              # =>This Inner Loop Header: Depth=1
	movss	(%r13,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB4_359
# BB#358:                               #   in Loop: Header=BB4_357 Depth=1
	leal	(%rdi,%rsi), %ebp
	movq	(%r14,%rax,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB4_359:                              #   in Loop: Header=BB4_357 Depth=1
	movss	4(%r13,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB4_361
# BB#360:                               #   in Loop: Header=BB4_357 Depth=1
	leal	1(%rdi,%rsi), %ebp
	movq	(%r14,%rax,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB4_361:                              #   in Loop: Header=BB4_357 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB4_357
.LBB4_332:                              # %.preheader14.i
	testl	%r10d, %r10d
	js	.LBB4_338
# BB#333:                               # %.lr.ph67.preheader.i
	movq	%r10, %rsi
	incq	%rsi
	movl	%esi, %eax
	leaq	-1(%rax), %rcx
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB4_335
	.p2align	4, 0x90
.LBB4_334:                              # %.lr.ph67.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB4_334
.LBB4_335:                              # %.lr.ph67.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB4_338
# BB#336:                               # %.lr.ph67.preheader.i.new
	negq	%rax
	leaq	4(%rdx,%rax), %rax
	leaq	56(%r14,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_337:                              # %.lr.ph67.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %ebp
	leal	-3(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-48(%rcx,%rsi,8), %rdi
	leal	-2(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-40(%rcx,%rsi,8), %rdi
	leal	-1(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-32(%rcx,%rsi,8), %rdi
	movl	%ebp, (%rdi)
	movq	-24(%rcx,%rsi,8), %rdi
	leal	1(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-16(%rcx,%rsi,8), %rdi
	leal	2(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-8(%rcx,%rsi,8), %rdi
	leal	3(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	(%rcx,%rsi,8), %rdi
	leal	4(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	leaq	8(%rax,%rsi), %rdi
	addq	$8, %rsi
	cmpq	$4, %rdi
	jne	.LBB4_337
.LBB4_338:                              # %.preheader13.i
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	js	.LBB4_366
# BB#339:                               # %.lr.ph65.i
	movq	(%r14), %rcx
	movq	40(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	movl	%esi, %eax
	cmpq	$7, %rax
	jbe	.LBB4_340
# BB#362:                               # %min.iters.checked959
	andl	$7, %esi
	movq	%rax, %rbp
	subq	%rsi, %rbp
	je	.LBB4_340
# BB#363:                               # %vector.body955.preheader
	leaq	16(%rcx), %rdi
	movdqa	.LCPI4_6(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI4_7(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI4_8(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB4_364:                              # %vector.body955
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdi)
	movdqu	%xmm4, (%rdi)
	paddd	%xmm3, %xmm0
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB4_364
# BB#365:                               # %middle.block956
	testq	%rsi, %rsi
	jne	.LBB4_341
	jmp	.LBB4_366
.LBB4_340:
	xorl	%ebp, %ebp
.LBB4_341:                              # %scalar.ph957.preheader
	movl	%ebp, %edx
	notl	%edx
	leaq	(%rcx,%rbp,4), %rcx
	subq	%rbp, %rax
	.p2align	4, 0x90
.LBB4_342:                              # %scalar.ph957
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rcx)
	decl	%edx
	addq	$4, %rcx
	decq	%rax
	jne	.LBB4_342
.LBB4_366:                              # %.preheader12.i
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_372
# BB#367:                               # %.lr.ph63.i
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r10), %eax
	cltq
	movl	28(%rsp), %ecx          # 4-byte Reload
	leaq	-1(%rcx), %rdx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	andq	$3, %rdi
	je	.LBB4_369
	.p2align	4, 0x90
.LBB4_368:                              # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rsi,8), %rbp
	leaq	(%rbp,%rax), %rbx
	movq	%rbx, (%r12,%rsi,8)
	movb	$0, (%rbp,%rax)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB4_368
.LBB4_369:                              # %.prol.loopexit1116
	cmpq	$3, %rdx
	jb	.LBB4_372
# BB#370:                               # %.lr.ph63.i.new
	subq	%rsi, %rcx
	leaq	24(%r12,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB4_371:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -24(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-16(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -16(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-8(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -8(%rdx)
	movb	$0, (%rsi,%rax)
	movq	(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, (%rdx)
	movb	$0, (%rsi,%rax)
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB4_371
.LBB4_372:                              # %.preheader11.i
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_378
# BB#373:                               # %.lr.ph60.i
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r10), %eax
	cltq
	movl	24(%rsp), %ecx          # 4-byte Reload
	leaq	-1(%rcx), %rdx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	andq	$3, %rdi
	je	.LBB4_375
	.p2align	4, 0x90
.LBB4_374:                              # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rsi,8), %rbp
	leaq	(%rbp,%rax), %rbx
	movq	%rbx, (%r15,%rsi,8)
	movb	$0, (%rbp,%rax)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB4_374
.LBB4_375:                              # %.prol.loopexit1111
	cmpq	$3, %rdx
	jb	.LBB4_378
# BB#376:                               # %.lr.ph60.i.new
	subq	%rsi, %rcx
	leaq	24(%r15,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB4_377:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -24(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-16(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -16(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-8(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -8(%rdx)
	movb	$0, (%rsi,%rax)
	movq	(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, (%rdx)
	movb	$0, (%rsi,%rax)
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB4_377
.LBB4_378:                              # %._crit_edge61.i
	movq	40(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addl	%r10d, %eax
	movl	%eax, 152(%rsp)         # 4-byte Spill
	js	.LBB4_467
# BB#379:                               # %.lr.ph56.preheader.i
	movl	28(%rsp), %r14d         # 4-byte Reload
	movl	24(%rsp), %r10d         # 4-byte Reload
	leaq	-1(%r10), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	-1(%r14), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%r14, 72(%rsp)          # 8-byte Spill
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	andl	$3, %r14d
	movq	%r10, 104(%rsp)         # 8-byte Spill
                                        # kill: %R10D<def> %R10D<kill> %R10<kill> %R10<def>
	andl	$3, %r10d
	leaq	24(%r15), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leaq	24(%r12), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leaq	24(%r8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	24(%r9), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_380:                              # %.lr.ph56.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_408 Depth 2
                                        #       Child Loop BB4_409 Depth 3
                                        #       Child Loop BB4_412 Depth 3
                                        #     Child Loop BB4_400 Depth 2
                                        #       Child Loop BB4_401 Depth 3
                                        #       Child Loop BB4_404 Depth 3
                                        #     Child Loop BB4_389 Depth 2
                                        #       Child Loop BB4_390 Depth 3
                                        #       Child Loop BB4_393 Depth 3
                                        #       Child Loop BB4_395 Depth 3
                                        #       Child Loop BB4_398 Depth 3
                                        #     Child Loop BB4_440 Depth 2
                                        #       Child Loop BB4_441 Depth 3
                                        #       Child Loop BB4_444 Depth 3
                                        #     Child Loop BB4_418 Depth 2
                                        #       Child Loop BB4_419 Depth 3
                                        #       Child Loop BB4_422 Depth 3
                                        #     Child Loop BB4_425 Depth 2
                                        #       Child Loop BB4_426 Depth 3
                                        #       Child Loop BB4_429 Depth 3
                                        #       Child Loop BB4_431 Depth 3
                                        #       Child Loop BB4_434 Depth 3
                                        #     Child Loop BB4_454 Depth 2
                                        #     Child Loop BB4_457 Depth 2
                                        #     Child Loop BB4_462 Depth 2
                                        #     Child Loop BB4_465 Depth 2
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r11d
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ebx
	movslq	%ebx, %rax
	movq	184(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movslq	%r11d, %rsi
	movl	(%rax,%rsi,4), %eax
	testl	%eax, %eax
	js	.LBB4_381
# BB#382:                               #   in Loop: Header=BB4_380 Depth=1
	je	.LBB4_384
# BB#383:                               #   in Loop: Header=BB4_380 Depth=1
	movl	%ebx, %ebp
	subl	%eax, %ebp
	jmp	.LBB4_385
	.p2align	4, 0x90
.LBB4_381:                              #   in Loop: Header=BB4_380 Depth=1
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leal	-1(%rbx), %ebp
	jmp	.LBB4_386
	.p2align	4, 0x90
.LBB4_384:                              #   in Loop: Header=BB4_380 Depth=1
	leal	-1(%rbx), %ebp
.LBB4_385:                              #   in Loop: Header=BB4_380 Depth=1
	movl	$-1, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
.LBB4_386:                              #   in Loop: Header=BB4_380 Depth=1
	movl	%ebx, %eax
	subl	%ebp, %eax
	decl	%eax
	movq	%rbp, %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%r11, 88(%rsp)          # 8-byte Spill
	movq	%rbx, 160(%rsp)         # 8-byte Spill
	je	.LBB4_415
# BB#387:                               # %.preheader8.lr.ph.i
                                        #   in Loop: Header=BB4_380 Depth=1
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_407
# BB#388:                               # %.preheader8.us.preheader.i
                                        #   in Loop: Header=BB4_380 Depth=1
	movq	%r13, 40(%rsp)          # 8-byte Spill
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movslq	%eax, %r11
	movslq	%ebp, %r13
	jle	.LBB4_400
	.p2align	4, 0x90
.LBB4_389:                              # %.preheader8.us.i.us
                                        #   Parent Loop BB4_380 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_390 Depth 3
                                        #       Child Loop BB4_393 Depth 3
                                        #       Child Loop BB4_395 Depth 3
                                        #       Child Loop BB4_398 Depth 3
	leaq	(%r11,%r13), %rax
	xorl	%edi, %edi
	testq	%r14, %r14
	je	.LBB4_391
	.p2align	4, 0x90
.LBB4_390:                              #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_389 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r8,%rdi,8), %rsi
	movzbl	(%rsi,%rax), %ebx
	movq	(%r12,%rdi,8), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, (%r12,%rdi,8)
	movb	%bl, -1(%rsi)
	incq	%rdi
	cmpq	%rdi, %r14
	jne	.LBB4_390
.LBB4_391:                              # %.prol.loopexit1073
                                        #   in Loop: Header=BB4_389 Depth=2
	cmpq	$3, 56(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_394
# BB#392:                               # %.preheader8.us.i.us.new
                                        #   in Loop: Header=BB4_389 Depth=2
	movq	72(%rsp), %rbp          # 8-byte Reload
	subq	%rdi, %rbp
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rsi
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB4_393:                              #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_389 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdi), %rbx
	movzbl	(%rbx,%rax), %ebx
	movq	-24(%rsi), %r9
	leaq	-1(%r9), %r8
	movq	%r8, -24(%rsi)
	movb	%bl, -1(%r9)
	movq	-16(%rdi), %rbx
	movzbl	(%rbx,%rax), %ebx
	movq	-16(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -16(%rsi)
	movb	%bl, -1(%rcx)
	movq	-8(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-8(%rsi), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, -8(%rsi)
	movb	%cl, -1(%rdx)
	movq	(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%rsi), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, (%rsi)
	movb	%cl, -1(%rdx)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-4, %rbp
	jne	.LBB4_393
.LBB4_394:                              # %.lr.ph18.us.i.us.preheader
                                        #   in Loop: Header=BB4_389 Depth=2
	xorl	%esi, %esi
	testq	%r10, %r10
	je	.LBB4_396
	.p2align	4, 0x90
.LBB4_395:                              # %.lr.ph18.us.i.us.prol
                                        #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_389 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15,%rsi,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%r15,%rsi,8)
	movb	$45, -1(%rax)
	incq	%rsi
	cmpq	%rsi, %r10
	jne	.LBB4_395
.LBB4_396:                              # %.lr.ph18.us.i.us.prol.loopexit
                                        #   in Loop: Header=BB4_389 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_399
# BB#397:                               # %.lr.ph18.us.i.us.preheader.new
                                        #   in Loop: Header=BB4_389 Depth=2
	movq	104(%rsp), %rax         # 8-byte Reload
	subq	%rsi, %rax
	movq	120(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB4_398:                              # %.lr.ph18.us.i.us
                                        #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_389 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -24(%rsi)
	movb	$45, -1(%rcx)
	movq	-16(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -16(%rsi)
	movb	$45, -1(%rcx)
	movq	-8(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -8(%rsi)
	movb	$45, -1(%rcx)
	movq	(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%rsi)
	movb	$45, -1(%rcx)
	addq	$32, %rsi
	addq	$-4, %rax
	jne	.LBB4_398
.LBB4_399:                              # %._crit_edge.us.i.loopexit.us
                                        #   in Loop: Header=BB4_389 Depth=2
	decq	%r11
	testl	%r11d, %r11d
	movq	48(%rsp), %r8           # 8-byte Reload
	jne	.LBB4_389
	jmp	.LBB4_406
	.p2align	4, 0x90
.LBB4_400:                              # %.preheader8.us.i
                                        #   Parent Loop BB4_380 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_401 Depth 3
                                        #       Child Loop BB4_404 Depth 3
	leaq	(%r11,%r13), %rax
	xorl	%edi, %edi
	testq	%r14, %r14
	je	.LBB4_402
	.p2align	4, 0x90
.LBB4_401:                              #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_400 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r8,%rdi,8), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%r12,%rdi,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r12,%rdi,8)
	movb	%cl, -1(%rdx)
	incq	%rdi
	cmpq	%rdi, %r14
	jne	.LBB4_401
.LBB4_402:                              # %.prol.loopexit1068
                                        #   in Loop: Header=BB4_400 Depth=2
	cmpq	$3, 56(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_405
# BB#403:                               # %.preheader8.us.i.new
                                        #   in Loop: Header=BB4_400 Depth=2
	movq	72(%rsp), %rbp          # 8-byte Reload
	subq	%rdi, %rbp
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rsi
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB4_404:                              #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_400 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-24(%rsi), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, -24(%rsi)
	movb	%cl, -1(%rdx)
	movq	-16(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-16(%rsi), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, -16(%rsi)
	movb	%cl, -1(%rdx)
	movq	-8(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-8(%rsi), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, -8(%rsi)
	movb	%cl, -1(%rdx)
	movq	(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%rsi), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, (%rsi)
	movb	%cl, -1(%rdx)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-4, %rbp
	jne	.LBB4_404
.LBB4_405:                              # %..preheader7_crit_edge.us.i
                                        #   in Loop: Header=BB4_400 Depth=2
	decq	%r11
	testl	%r11d, %r11d
	jne	.LBB4_400
.LBB4_406:                              # %._crit_edge20.loopexit.i
                                        #   in Loop: Header=BB4_380 Depth=1
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	160(%rsp), %rbx         # 8-byte Reload
	leal	-1(%r13,%rbx), %r13d
	subl	80(%rsp), %r13d         # 4-byte Folded Reload
	movq	144(%rsp), %r9          # 8-byte Reload
	movq	88(%rsp), %r11          # 8-byte Reload
	jmp	.LBB4_415
	.p2align	4, 0x90
.LBB4_407:                              # %.preheader8.lr.ph.split.i
                                        #   in Loop: Header=BB4_380 Depth=1
	leal	-1(%r13,%rbx), %r13d
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_414
	.p2align	4, 0x90
.LBB4_408:                              # %.preheader8.us21.i
                                        #   Parent Loop BB4_380 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_409 Depth 3
                                        #       Child Loop BB4_412 Depth 3
	xorl	%edi, %edi
	testq	%r10, %r10
	je	.LBB4_410
	.p2align	4, 0x90
.LBB4_409:                              #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_408 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15,%rdi,8), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%r15,%rdi,8)
	movb	$45, -1(%rcx)
	incq	%rdi
	cmpq	%rdi, %r10
	jne	.LBB4_409
.LBB4_410:                              # %.prol.loopexit1063
                                        #   in Loop: Header=BB4_408 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_413
# BB#411:                               # %.preheader8.us21.i.new
                                        #   in Loop: Header=BB4_408 Depth=2
	movq	104(%rsp), %rsi         # 8-byte Reload
	subq	%rdi, %rsi
	movq	120(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB4_412:                              #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_408 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -24(%rdi)
	movb	$45, -1(%rcx)
	movq	-16(%rdi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -16(%rdi)
	movb	$45, -1(%rcx)
	movq	-8(%rdi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -8(%rdi)
	movb	$45, -1(%rcx)
	movq	(%rdi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%rdi)
	movb	$45, -1(%rcx)
	addq	$32, %rdi
	addq	$-4, %rsi
	jne	.LBB4_412
.LBB4_413:                              # %._crit_edge.us29.i
                                        #   in Loop: Header=BB4_408 Depth=2
	decl	%eax
	jne	.LBB4_408
.LBB4_414:                              # %._crit_edge20.loopexit78.i
                                        #   in Loop: Header=BB4_380 Depth=1
	subl	%ebp, %r13d
.LBB4_415:                              # %._crit_edge20.i
                                        #   in Loop: Header=BB4_380 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r11), %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	cmpl	$-1, %eax
	je	.LBB4_448
# BB#416:                               # %.preheader6.lr.ph.i
                                        #   in Loop: Header=BB4_380 Depth=1
	notl	%eax
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	movq	%rax, 96(%rsp)          # 8-byte Spill
	jle	.LBB4_437
# BB#417:                               # %.preheader6.us.preheader.i
                                        #   in Loop: Header=BB4_380 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movslq	%eax, %r8
	movq	%r13, %r11
	jle	.LBB4_418
# BB#424:                               # %.preheader6.us.i.us.preheader
                                        #   in Loop: Header=BB4_380 Depth=1
	movslq	40(%rsp), %r13          # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB4_425:                              # %.preheader6.us.i.us
                                        #   Parent Loop BB4_380 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_426 Depth 3
                                        #       Child Loop BB4_429 Depth 3
                                        #       Child Loop BB4_431 Depth 3
                                        #       Child Loop BB4_434 Depth 3
	xorl	%esi, %esi
	testq	%r14, %r14
	je	.LBB4_427
	.p2align	4, 0x90
.LBB4_426:                              #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_425 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r12,%rsi,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%r12,%rsi,8)
	movb	$45, -1(%rax)
	incq	%rsi
	cmpq	%rsi, %r14
	jne	.LBB4_426
.LBB4_427:                              # %.prol.loopexit1091
                                        #   in Loop: Header=BB4_425 Depth=2
	cmpq	$3, 56(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_430
# BB#428:                               # %.preheader6.us.i.us.new
                                        #   in Loop: Header=BB4_425 Depth=2
	movq	72(%rsp), %rax          # 8-byte Reload
	subq	%rsi, %rax
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB4_429:                              #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_425 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -24(%rsi)
	movb	$45, -1(%rcx)
	movq	-16(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -16(%rsi)
	movb	$45, -1(%rcx)
	movq	-8(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -8(%rsi)
	movb	$45, -1(%rcx)
	movq	(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%rsi)
	movb	$45, -1(%rcx)
	addq	$32, %rsi
	addq	$-4, %rax
	jne	.LBB4_429
.LBB4_430:                              # %.lr.ph33.us.i.us
                                        #   in Loop: Header=BB4_425 Depth=2
	leaq	(%r8,%r13), %rbp
	xorl	%eax, %eax
	testq	%r10, %r10
	je	.LBB4_432
	.p2align	4, 0x90
.LBB4_431:                              #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_425 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r9,%rax,8), %rcx
	movzbl	(%rcx,%rbp), %ecx
	movq	(%r15,%rax,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15,%rax,8)
	movb	%cl, -1(%rdx)
	incq	%rax
	cmpq	%rax, %r10
	jne	.LBB4_431
.LBB4_432:                              # %.prol.loopexit1096
                                        #   in Loop: Header=BB4_425 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_435
# BB#433:                               # %.lr.ph33.us.i.us.new
                                        #   in Loop: Header=BB4_425 Depth=2
	movq	104(%rsp), %rsi         # 8-byte Reload
	subq	%rax, %rsi
	movq	120(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rdi
	movq	128(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	.p2align	4, 0x90
.LBB4_434:                              #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_425 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rax), %rcx
	movzbl	(%rcx,%rbp), %ecx
	movq	-24(%rdi), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, -24(%rdi)
	movb	%cl, -1(%rdx)
	movq	-16(%rax), %rcx
	movzbl	(%rcx,%rbp), %ecx
	movq	-16(%rdi), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, -16(%rdi)
	movb	%cl, -1(%rdx)
	movq	-8(%rax), %rcx
	movzbl	(%rcx,%rbp), %ecx
	movq	-8(%rdi), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, -8(%rdi)
	movb	%cl, -1(%rdx)
	movq	(%rax), %rcx
	movzbl	(%rcx,%rbp), %ecx
	movq	(%rdi), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, (%rdi)
	movb	%cl, -1(%rdx)
	addq	$32, %rdi
	addq	$32, %rax
	addq	$-4, %rsi
	jne	.LBB4_434
.LBB4_435:                              # %._crit_edge34.us.i.loopexit.us
                                        #   in Loop: Header=BB4_425 Depth=2
	decq	%r8
	testl	%r8d, %r8d
	jne	.LBB4_425
	jmp	.LBB4_436
	.p2align	4, 0x90
.LBB4_418:                              # %.preheader6.us.i
                                        #   Parent Loop BB4_380 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_419 Depth 3
                                        #       Child Loop BB4_422 Depth 3
	xorl	%esi, %esi
	testq	%r14, %r14
	je	.LBB4_420
	.p2align	4, 0x90
.LBB4_419:                              #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_418 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r12,%rsi,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%r12,%rsi,8)
	movb	$45, -1(%rax)
	incq	%rsi
	cmpq	%rsi, %r14
	jne	.LBB4_419
.LBB4_420:                              # %.prol.loopexit1086
                                        #   in Loop: Header=BB4_418 Depth=2
	cmpq	$3, 56(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_423
# BB#421:                               # %.preheader6.us.i.new
                                        #   in Loop: Header=BB4_418 Depth=2
	movq	72(%rsp), %rax          # 8-byte Reload
	subq	%rsi, %rax
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB4_422:                              #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_418 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -24(%rsi)
	movb	$45, -1(%rcx)
	movq	-16(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -16(%rsi)
	movb	$45, -1(%rcx)
	movq	-8(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -8(%rsi)
	movb	$45, -1(%rcx)
	movq	(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%rsi)
	movb	$45, -1(%rcx)
	addq	$32, %rsi
	addq	$-4, %rax
	jne	.LBB4_422
.LBB4_423:                              # %..preheader_crit_edge.us.i
                                        #   in Loop: Header=BB4_418 Depth=2
	decq	%r8
	testl	%r8d, %r8d
	jne	.LBB4_418
.LBB4_436:                              # %._crit_edge36.loopexit.i
                                        #   in Loop: Header=BB4_380 Depth=1
	movq	%r11, %r13
	addl	96(%rsp), %r13d         # 4-byte Folded Reload
	jmp	.LBB4_447
	.p2align	4, 0x90
.LBB4_437:                              # %.preheader6.lr.ph.split.i
                                        #   in Loop: Header=BB4_380 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_438
# BB#439:                               # %.preheader6.us38.preheader.i
                                        #   in Loop: Header=BB4_380 Depth=1
	movq	%r13, %r11
	movslq	%eax, %r8
	movslq	40(%rsp), %r13          # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB4_440:                              # %.preheader6.us38.i
                                        #   Parent Loop BB4_380 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_441 Depth 3
                                        #       Child Loop BB4_444 Depth 3
	leaq	(%r8,%r13), %rbp
	xorl	%ebx, %ebx
	testq	%r10, %r10
	je	.LBB4_442
	.p2align	4, 0x90
.LBB4_441:                              #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_440 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r9,%rbx,8), %rcx
	movzbl	(%rcx,%rbp), %ecx
	movq	(%r15,%rbx,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15,%rbx,8)
	movb	%cl, -1(%rdx)
	incq	%rbx
	cmpq	%rbx, %r10
	jne	.LBB4_441
.LBB4_442:                              # %.prol.loopexit1081
                                        #   in Loop: Header=BB4_440 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_445
# BB#443:                               # %.preheader6.us38.i.new
                                        #   in Loop: Header=BB4_440 Depth=2
	movq	104(%rsp), %rsi         # 8-byte Reload
	subq	%rbx, %rsi
	movq	120(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rdi
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB4_444:                              #   Parent Loop BB4_380 Depth=1
                                        #     Parent Loop BB4_440 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rbx), %rcx
	movzbl	(%rcx,%rbp), %ecx
	movq	-24(%rdi), %rdx
	leaq	-1(%rdx), %rax
	movq	%rax, -24(%rdi)
	movb	%cl, -1(%rdx)
	movq	-16(%rbx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	-16(%rdi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -16(%rdi)
	movb	%al, -1(%rcx)
	movq	-8(%rbx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	-8(%rdi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -8(%rdi)
	movb	%al, -1(%rcx)
	movq	(%rbx), %rax
	movzbl	(%rax,%rbp), %eax
	movq	(%rdi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%rdi)
	movb	%al, -1(%rcx)
	addq	$32, %rdi
	addq	$32, %rbx
	addq	$-4, %rsi
	jne	.LBB4_444
.LBB4_445:                              # %._crit_edge34.us46.i
                                        #   in Loop: Header=BB4_440 Depth=2
	decq	%r8
	testl	%r8d, %r8d
	movq	96(%rsp), %rax          # 8-byte Reload
	jne	.LBB4_440
# BB#446:                               # %._crit_edge36.loopexit76.i
                                        #   in Loop: Header=BB4_380 Depth=1
	movq	%r11, %r13
	addl	%eax, %r13d
.LBB4_447:                              # %._crit_edge36.i
                                        #   in Loop: Header=BB4_380 Depth=1
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	88(%rsp), %r11          # 8-byte Reload
	movq	160(%rsp), %rbx         # 8-byte Reload
.LBB4_448:                              # %._crit_edge36.i
                                        #   in Loop: Header=BB4_380 Depth=1
	testl	%ebx, %ebx
	jle	.LBB4_467
.LBB4_449:                              # %._crit_edge36.i
                                        #   in Loop: Header=BB4_380 Depth=1
	testl	%r11d, %r11d
	jle	.LBB4_467
# BB#450:                               # %.preheader10.i
                                        #   in Loop: Header=BB4_380 Depth=1
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	jle	.LBB4_458
# BB#451:                               # %.lr.ph48.i
                                        #   in Loop: Header=BB4_380 Depth=1
	testq	%r14, %r14
	cltq
	je	.LBB4_452
# BB#453:                               # %.prol.preheader1100
                                        #   in Loop: Header=BB4_380 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_454:                              #   Parent Loop BB4_380 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r8,%rdi,8), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%r12,%rdi,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r12,%rdi,8)
	movb	%cl, -1(%rdx)
	incq	%rdi
	cmpq	%rdi, %r14
	jne	.LBB4_454
	jmp	.LBB4_455
	.p2align	4, 0x90
.LBB4_452:                              #   in Loop: Header=BB4_380 Depth=1
	xorl	%edi, %edi
.LBB4_455:                              # %.prol.loopexit1101
                                        #   in Loop: Header=BB4_380 Depth=1
	cmpq	$3, 56(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_458
# BB#456:                               # %.lr.ph48.i.new
                                        #   in Loop: Header=BB4_380 Depth=1
	movq	72(%rsp), %rbx          # 8-byte Reload
	subq	%rdi, %rbx
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rsi
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB4_457:                              #   Parent Loop BB4_380 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-24(%rsi), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, -24(%rsi)
	movb	%cl, -1(%rdx)
	movq	-16(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-16(%rsi), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, -16(%rsi)
	movb	%cl, -1(%rdx)
	movq	-8(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-8(%rsi), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, -8(%rsi)
	movb	%cl, -1(%rdx)
	movq	(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%rsi), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, (%rsi)
	movb	%cl, -1(%rdx)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-4, %rbx
	jne	.LBB4_457
.LBB4_458:                              # %.preheader9.i
                                        #   in Loop: Header=BB4_380 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_466
# BB#459:                               # %.lr.ph50.i
                                        #   in Loop: Header=BB4_380 Depth=1
	testq	%r10, %r10
	movslq	40(%rsp), %rax          # 4-byte Folded Reload
	je	.LBB4_460
# BB#461:                               # %.prol.preheader1105
                                        #   in Loop: Header=BB4_380 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_462:                              #   Parent Loop BB4_380 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r9,%rdi,8), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%r15,%rdi,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15,%rdi,8)
	movb	%cl, -1(%rdx)
	incq	%rdi
	cmpq	%rdi, %r10
	jne	.LBB4_462
	jmp	.LBB4_463
	.p2align	4, 0x90
.LBB4_460:                              #   in Loop: Header=BB4_380 Depth=1
	xorl	%edi, %edi
.LBB4_463:                              # %.prol.loopexit1106
                                        #   in Loop: Header=BB4_380 Depth=1
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_466
# BB#464:                               # %.lr.ph50.i.new
                                        #   in Loop: Header=BB4_380 Depth=1
	movq	104(%rsp), %rbx         # 8-byte Reload
	subq	%rdi, %rbx
	movq	120(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rsi
	movq	128(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB4_465:                              #   Parent Loop BB4_380 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-24(%rsi), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, -24(%rsi)
	movb	%cl, -1(%rdx)
	movq	-16(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-16(%rsi), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, -16(%rsi)
	movb	%cl, -1(%rdx)
	movq	-8(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-8(%rsi), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, -8(%rsi)
	movb	%cl, -1(%rdx)
	movq	(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%rsi), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, (%rsi)
	movb	%cl, -1(%rdx)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-4, %rbx
	jne	.LBB4_465
.LBB4_466:                              # %._crit_edge51.i
                                        #   in Loop: Header=BB4_380 Depth=1
	addl	$2, %r13d
	cmpl	152(%rsp), %r13d        # 4-byte Folded Reload
	jle	.LBB4_380
	jmp	.LBB4_467
.LBB4_438:                              # %.preheader6.preheader.i
                                        #   in Loop: Header=BB4_380 Depth=1
	addl	%eax, %r13d
	testl	%ebx, %ebx
	jg	.LBB4_449
.LBB4_467:                              # %Atracking_localhom.exit
	movq	(%r12), %rdi
	callq	strlen
	movq	%rax, %rcx
	movl	464(%rsp), %edx
	cmpl	%edx, %ecx
	jg	.LBB4_469
# BB#468:                               # %Atracking_localhom.exit
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB4_469
.LBB4_470:                              # %.preheader622
	movl	28(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movl	24(%rsp), %r15d         # 4-byte Reload
	movq	48(%rsp), %r14          # 8-byte Reload
	jle	.LBB4_473
# BB#471:                               # %.lr.ph635.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_472:                              # %.lr.ph635
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbp,8), %rdi
	movq	partQ__align.mseq1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB4_472
.LBB4_473:                              # %.preheader
	testl	%r15d, %r15d
	movq	144(%rsp), %r14         # 8-byte Reload
	jle	.LBB4_476
# BB#474:                               # %.lr.ph.preheader
	movl	%r15d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_475:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbp,8), %rdi
	movq	partQ__align.mseq2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB4_475
.LBB4_476:                              # %._crit_edge
	movss	192(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$408, %rsp              # imm = 0x198
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_469:
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.7, %edi
	callq	ErrorExit
	jmp	.LBB4_470
.LBB4_93:
	movq	48(%rsp), %r8           # 8-byte Reload
.LBB4_95:
	movq	112(%rsp), %r15         # 8-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	jmp	.LBB4_86
.LBB4_112:
	movq	48(%rsp), %r8           # 8-byte Reload
.LBB4_114:
	movl	504(%rsp), %r14d
	movq	112(%rsp), %r15         # 8-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	jmp	.LBB4_104
.Lfunc_end4:
	.size	partQ__align, .Lfunc_end4-partQ__align
	.cfi_endproc

	.p2align	4, 0x90
	.type	match_calc,@function
match_calc:                             # @match_calc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 40
.Lcfi51:
	.cfi_offset %rbx, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	40(%rsp), %r11
	cmpl	$0, 48(%rsp)
	je	.LBB5_10
# BB#1:
	testl	%r8d, %r8d
	jle	.LBB5_10
# BB#2:                                 # %.preheader77.preheader
	movl	%r8d, %r10d
	xorl	%r14d, %r14d
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB5_3:                                # %.preheader77
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_4 Depth 2
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB5_4:                                #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rax,8), %rbx
	movss	(%rbx,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB5_5
	jnp	.LBB5_6
.LBB5_5:                                #   in Loop: Header=BB5_4 Depth=2
	movq	(%r9,%r14,8), %rbx
	movslq	%r15d, %r15
	movss	%xmm1, (%rbx,%r15,4)
	movq	(%r11,%r14,8), %rbx
	movl	%eax, (%rbx,%r15,4)
	incl	%r15d
.LBB5_6:                                #   in Loop: Header=BB5_4 Depth=2
	movq	8(%rdx,%rax,8), %rbx
	movss	(%rbx,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB5_7
	jnp	.LBB5_8
.LBB5_7:                                #   in Loop: Header=BB5_4 Depth=2
	movq	(%r9,%r14,8), %rbx
	movslq	%r15d, %r15
	movss	%xmm1, (%rbx,%r15,4)
	movq	(%r11,%r14,8), %rbx
	leal	1(%rax), %ebp
	movl	%ebp, (%rbx,%r15,4)
	incl	%r15d
.LBB5_8:                                #   in Loop: Header=BB5_4 Depth=2
	addq	$2, %rax
	cmpq	$26, %rax
	jne	.LBB5_4
# BB#9:                                 #   in Loop: Header=BB5_3 Depth=1
	movq	(%r11,%r14,8), %rax
	movslq	%r15d, %rbx
	movl	$-1, (%rax,%rbx,4)
	incq	%r14
	cmpq	%r10, %r14
	jne	.LBB5_3
.LBB5_10:                               # %.preheader76
	movl	$n_dis_consweight_multi, %r10d
	movslq	%ecx, %rcx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_12 Depth 2
	movl	$0, -104(%rsp,%r14,4)
	xorps	%xmm0, %xmm0
	movl	$1, %ebx
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB5_12:                               #   Parent Loop BB5_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rsi,%rbx,8), %rdx
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdx,%rcx,4), %xmm1
	addss	%xmm0, %xmm1
	movss	104(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movq	(%rsi,%rbx,8), %rdx
	mulss	(%rdx,%rcx,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rax
	addq	$2, %rbx
	cmpq	$27, %rbx
	jne	.LBB5_12
# BB#13:                                #   in Loop: Header=BB5_11 Depth=1
	movss	%xmm0, -104(%rsp,%r14,4)
	incq	%r14
	addq	$4, %r10
	cmpq	$26, %r14
	jne	.LBB5_11
	jmp	.LBB5_14
	.p2align	4, 0x90
.LBB5_18:                               # %._crit_edge
                                        #   in Loop: Header=BB5_14 Depth=1
	addq	$8, %r11
	addq	$8, %r9
	addq	$4, %rdi
.LBB5_14:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_17 Depth 2
	testl	%r8d, %r8d
	je	.LBB5_19
# BB#15:                                # %.lr.ph84
                                        #   in Loop: Header=BB5_14 Depth=1
	decl	%r8d
	movl	$0, (%rdi)
	movq	(%r11), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	js	.LBB5_18
# BB#16:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB5_14 Depth=1
	movq	(%r9), %rcx
	addq	$4, %rax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB5_17:                               # %.lr.ph
                                        #   Parent Loop BB5_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	movss	-104(%rsp,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addq	$4, %rcx
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdi)
	movl	(%rax), %edx
	addq	$4, %rax
	testl	%edx, %edx
	jns	.LBB5_17
	jmp	.LBB5_18
.LBB5_19:                               # %._crit_edge85
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	match_calc, .Lfunc_end5-match_calc
	.cfi_endproc

	.type	impmtx,@object          # @impmtx
	.local	impmtx
	.comm	impmtx,8,8
	.type	impalloclen,@object     # @impalloclen
	.local	impalloclen
	.comm	impalloclen,4,4
	.type	part_imp_match_initQ.impalloclen,@object # @part_imp_match_initQ.impalloclen
	.local	part_imp_match_initQ.impalloclen
	.comm	part_imp_match_initQ.impalloclen,4,4
	.type	part_imp_match_initQ.nocount1,@object # @part_imp_match_initQ.nocount1
	.local	part_imp_match_initQ.nocount1
	.comm	part_imp_match_initQ.nocount1,8,8
	.type	part_imp_match_initQ.nocount2,@object # @part_imp_match_initQ.nocount2
	.local	part_imp_match_initQ.nocount2
	.comm	part_imp_match_initQ.nocount2,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"i = %d, seq1 = %s\n"
	.size	.L.str, 19

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"start1 = %d\n"
	.size	.L.str.1, 13

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"end1   = %d\n"
	.size	.L.str.2, 13

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"j = %d, seq2 = %s\n"
	.size	.L.str.3, 19

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"step 0\n"
	.size	.L.str.4, 8

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"dif = %d\n"
	.size	.L.str.5, 10

	.type	partQ__align.mi,@object # @partQ__align.mi
	.local	partQ__align.mi
	.comm	partQ__align.mi,4,4
	.type	partQ__align.m,@object  # @partQ__align.m
	.local	partQ__align.m
	.comm	partQ__align.m,8,8
	.type	partQ__align.ijp,@object # @partQ__align.ijp
	.local	partQ__align.ijp
	.comm	partQ__align.ijp,8,8
	.type	partQ__align.mpi,@object # @partQ__align.mpi
	.local	partQ__align.mpi
	.comm	partQ__align.mpi,4,4
	.type	partQ__align.mp,@object # @partQ__align.mp
	.local	partQ__align.mp
	.comm	partQ__align.mp,8,8
	.type	partQ__align.w1,@object # @partQ__align.w1
	.local	partQ__align.w1
	.comm	partQ__align.w1,8,8
	.type	partQ__align.w2,@object # @partQ__align.w2
	.local	partQ__align.w2
	.comm	partQ__align.w2,8,8
	.type	partQ__align.match,@object # @partQ__align.match
	.local	partQ__align.match
	.comm	partQ__align.match,8,8
	.type	partQ__align.initverticalw,@object # @partQ__align.initverticalw
	.local	partQ__align.initverticalw
	.comm	partQ__align.initverticalw,8,8
	.type	partQ__align.lastverticalw,@object # @partQ__align.lastverticalw
	.local	partQ__align.lastverticalw
	.comm	partQ__align.lastverticalw,8,8
	.type	partQ__align.mseq1,@object # @partQ__align.mseq1
	.local	partQ__align.mseq1
	.comm	partQ__align.mseq1,8,8
	.type	partQ__align.mseq2,@object # @partQ__align.mseq2
	.local	partQ__align.mseq2
	.comm	partQ__align.mseq2,8,8
	.type	partQ__align.mseq,@object # @partQ__align.mseq
	.local	partQ__align.mseq
	.comm	partQ__align.mseq,8,8
	.type	partQ__align.digf1,@object # @partQ__align.digf1
	.local	partQ__align.digf1
	.comm	partQ__align.digf1,8,8
	.type	partQ__align.digf2,@object # @partQ__align.digf2
	.local	partQ__align.digf2
	.comm	partQ__align.digf2,8,8
	.type	partQ__align.diaf1,@object # @partQ__align.diaf1
	.local	partQ__align.diaf1
	.comm	partQ__align.diaf1,8,8
	.type	partQ__align.diaf2,@object # @partQ__align.diaf2
	.local	partQ__align.diaf2
	.comm	partQ__align.diaf2,8,8
	.type	partQ__align.gapz1,@object # @partQ__align.gapz1
	.local	partQ__align.gapz1
	.comm	partQ__align.gapz1,8,8
	.type	partQ__align.gapz2,@object # @partQ__align.gapz2
	.local	partQ__align.gapz2
	.comm	partQ__align.gapz2,8,8
	.type	partQ__align.gapf1,@object # @partQ__align.gapf1
	.local	partQ__align.gapf1
	.comm	partQ__align.gapf1,8,8
	.type	partQ__align.gapf2,@object # @partQ__align.gapf2
	.local	partQ__align.gapf2
	.comm	partQ__align.gapf2,8,8
	.type	partQ__align.ogcp1g,@object # @partQ__align.ogcp1g
	.local	partQ__align.ogcp1g
	.comm	partQ__align.ogcp1g,8,8
	.type	partQ__align.ogcp2g,@object # @partQ__align.ogcp2g
	.local	partQ__align.ogcp2g
	.comm	partQ__align.ogcp2g,8,8
	.type	partQ__align.fgcp1g,@object # @partQ__align.fgcp1g
	.local	partQ__align.fgcp1g
	.comm	partQ__align.fgcp1g,8,8
	.type	partQ__align.fgcp2g,@object # @partQ__align.fgcp2g
	.local	partQ__align.fgcp2g
	.comm	partQ__align.fgcp2g,8,8
	.type	partQ__align.og_h_dg_n1_p,@object # @partQ__align.og_h_dg_n1_p
	.local	partQ__align.og_h_dg_n1_p
	.comm	partQ__align.og_h_dg_n1_p,8,8
	.type	partQ__align.og_h_dg_n2_p,@object # @partQ__align.og_h_dg_n2_p
	.local	partQ__align.og_h_dg_n2_p
	.comm	partQ__align.og_h_dg_n2_p,8,8
	.type	partQ__align.fg_h_dg_n1_p,@object # @partQ__align.fg_h_dg_n1_p
	.local	partQ__align.fg_h_dg_n1_p
	.comm	partQ__align.fg_h_dg_n1_p,8,8
	.type	partQ__align.fg_h_dg_n2_p,@object # @partQ__align.fg_h_dg_n2_p
	.local	partQ__align.fg_h_dg_n2_p
	.comm	partQ__align.fg_h_dg_n2_p,8,8
	.type	partQ__align.og_t_fg_h_dg_n1_p,@object # @partQ__align.og_t_fg_h_dg_n1_p
	.local	partQ__align.og_t_fg_h_dg_n1_p
	.comm	partQ__align.og_t_fg_h_dg_n1_p,8,8
	.type	partQ__align.og_t_fg_h_dg_n2_p,@object # @partQ__align.og_t_fg_h_dg_n2_p
	.local	partQ__align.og_t_fg_h_dg_n2_p
	.comm	partQ__align.og_t_fg_h_dg_n2_p,8,8
	.type	partQ__align.fg_t_og_h_dg_n1_p,@object # @partQ__align.fg_t_og_h_dg_n1_p
	.local	partQ__align.fg_t_og_h_dg_n1_p
	.comm	partQ__align.fg_t_og_h_dg_n1_p,8,8
	.type	partQ__align.fg_t_og_h_dg_n2_p,@object # @partQ__align.fg_t_og_h_dg_n2_p
	.local	partQ__align.fg_t_og_h_dg_n2_p
	.comm	partQ__align.fg_t_og_h_dg_n2_p,8,8
	.type	partQ__align.gapz_n1,@object # @partQ__align.gapz_n1
	.local	partQ__align.gapz_n1
	.comm	partQ__align.gapz_n1,8,8
	.type	partQ__align.gapz_n2,@object # @partQ__align.gapz_n2
	.local	partQ__align.gapz_n2
	.comm	partQ__align.gapz_n2,8,8
	.type	partQ__align.cpmx1,@object # @partQ__align.cpmx1
	.local	partQ__align.cpmx1
	.comm	partQ__align.cpmx1,8,8
	.type	partQ__align.cpmx2,@object # @partQ__align.cpmx2
	.local	partQ__align.cpmx2
	.comm	partQ__align.cpmx2,8,8
	.type	partQ__align.intwork,@object # @partQ__align.intwork
	.local	partQ__align.intwork
	.comm	partQ__align.intwork,8,8
	.type	partQ__align.floatwork,@object # @partQ__align.floatwork
	.local	partQ__align.floatwork
	.comm	partQ__align.floatwork,8,8
	.type	partQ__align.orlgth1,@object # @partQ__align.orlgth1
	.local	partQ__align.orlgth1
	.comm	partQ__align.orlgth1,4,4
	.type	partQ__align.orlgth2,@object # @partQ__align.orlgth2
	.local	partQ__align.orlgth2
	.comm	partQ__align.orlgth2,4,4
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"alloclen=%d, resultlen=%d, N=%d\n"
	.size	.L.str.6, 33

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"LENGTH OVER!\n"
	.size	.L.str.7, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
