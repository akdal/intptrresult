	.text
	.file	"smg_solve.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
.LCPI0_1:
	.quad	0                       # double 0
	.text
	.globl	hypre_SMGSolve
	.p2align	4, 0x90
	.type	hypre_SMGSolve,@function
hypre_SMGSolve:                         # @hypre_SMGSolve
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi6:
	.cfi_def_cfa_offset 320
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	movsd	8(%r14), %xmm0          # xmm0 = mem[0],zero
	movsd	%xmm0, 80(%rsp)         # 8-byte Spill
	movslq	16(%r14), %rbx
	movl	20(%r14), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movl	24(%r14), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movslq	32(%r14), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	36(%r14), %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movl	40(%r14), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	movq	96(%r14), %r15
	movq	104(%r14), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movq	112(%r14), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	120(%r14), %rbp
	movq	128(%r14), %r13
	movq	152(%r14), %r12
	movq	160(%r14), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	168(%r14), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	176(%r14), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	184(%r14), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	192(%r14), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movl	208(%r14), %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movq	216(%r14), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	224(%r14), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	204(%r14), %edi
	callq	hypre_BeginTiming
	movq	(%r15), %rdi
	callq	hypre_StructMatrixDestroy
	movq	(%rbp), %rdi
	callq	hypre_StructVectorDestroy
	movq	(%r13), %rdi
	callq	hypre_StructVectorDestroy
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	hypre_StructMatrixRef
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%rax, (%r15)
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	hypre_StructVectorRef
	movq	%rax, (%rbp)
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	hypre_StructVectorRef
	movq	%rbx, %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	testq	%rbx, %rbx
	movq	%rax, (%r13)
	movl	$0, 200(%r14)
	je	.LBB0_1
# BB#3:
	xorpd	%xmm0, %xmm0
	movsd	80(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
                                        # implicit-def: %XMM0
                                        # implicit-def: %XMM1
	movsd	%xmm1, 72(%rsp)         # 8-byte Spill
	movapd	%xmm2, %xmm1
	jbe	.LBB0_6
# BB#4:
	movq	(%rbp), %rdi
	movq	%rdi, %rsi
	callq	hypre_StructInnerProd
	ucomisd	.LCPI0_1, %xmm0
	jne	.LBB0_5
	jp	.LBB0_5
# BB#25:
	xorpd	%xmm0, %xmm0
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	hypre_StructVectorSetConstantValues
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_43
# BB#26:
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	$0, (%rax)
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	$0, (%rax)
	jmp	.LBB0_43
.LBB0_1:
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB0_43
# BB#2:
	xorpd	%xmm0, %xmm0
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	hypre_StructVectorSetConstantValues
	jmp	.LBB0_43
.LBB0_5:
	movsd	80(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm2
	movsd	%xmm2, 72(%rsp)         # 8-byte Spill
.LBB0_6:
	movsd	%xmm0, 136(%rsp)        # 8-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	cmpl	$0, 144(%rsp)           # 4-byte Folded Reload
	movq	88(%rsp), %r15          # 8-byte Reload
	movl	24(%rsp), %ebp          # 4-byte Reload
	jle	.LBB0_43
# BB#7:                                 # %.lr.ph301
	leaq	48(%r14), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	leaq	60(%r14), %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	seta	%al
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpl	$1, %ecx
	setg	%dl
	leal	-2(%rcx), %esi
	movl	%esi, 116(%rsp)         # 4-byte Spill
	leaq	8(%r15), %rsi
	movq	%rsi, 208(%rsp)         # 8-byte Spill
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	setne	%sil
	andb	%al, %sil
	andb	%sil, %dl
	movb	%dl, 7(%rsp)            # 1-byte Spill
	leal	-3(%rcx), %eax
	leaq	16(%r15,%rax,8), %rdx
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	addq	$2, %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	leal	-1(%rcx), %eax
	decq	%rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	-1(%rcx), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
                                        # implicit-def: %XMM0
	movsd	%xmm0, 96(%rsp)         # 8-byte Spill
                                        # implicit-def: %XMM0
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	movq	%r14, 232(%rsp)         # 8-byte Spill
	movb	%sil, 6(%rsp)           # 1-byte Spill
	movq	%r12, 256(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_28 Depth 2
                                        #     Child Loop BB0_32 Depth 2
	cmpl	$2, 40(%rsp)            # 4-byte Folded Reload
	jl	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=1
	movq	(%r15), %rdi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	hypre_SMGRelaxSetRegSpaceRank
	movq	(%r15), %rdi
	movl	$1, %esi
	movl	$1, %edx
	callq	hypre_SMGRelaxSetRegSpaceRank
.LBB0_10:                               #   in Loop: Header=BB0_8 Depth=1
	movq	(%r15), %rdi
	movl	60(%rsp), %esi          # 4-byte Reload
	callq	hypre_SMGRelaxSetMaxIter
	movq	(%r15), %rdi
	movl	%ebp, %esi
	callq	hypre_SMGRelaxSetZeroGuess
	movq	(%r15), %rdi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rsi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	(%rbx), %rdx
	movq	(%r13), %rcx
	callq	hypre_SMGRelax
	movq	176(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rdi
	movq	(%rbp), %rsi
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	(%r13), %rdx
	movq	(%rbx), %rcx
	movq	256(%rsp), %r12         # 8-byte Reload
	movq	(%r12), %r8
	callq	hypre_SMGResidual
	movsd	80(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	.LCPI0_1, %xmm0
	movq	152(%rsp), %rbx         # 8-byte Reload
	jbe	.LBB0_22
# BB#11:                                #   in Loop: Header=BB0_8 Depth=1
	movq	(%r12), %rdi
	movq	%rdi, %rsi
	callq	hypre_StructInnerProd
	movapd	%xmm0, %xmm3
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_18
# BB#12:                                #   in Loop: Header=BB0_8 Depth=1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm3, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_14
# BB#13:                                # %call.sqrt
                                        #   in Loop: Header=BB0_8 Depth=1
	movapd	%xmm3, %xmm0
	movsd	%xmm3, 24(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	24(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
.LBB0_14:                               # %.split
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movsd	%xmm0, (%rax,%rcx,8)
	xorpd	%xmm0, %xmm0
	movsd	136(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	jbe	.LBB0_17
# BB#15:                                #   in Loop: Header=BB0_8 Depth=1
	movapd	%xmm3, %xmm1
	divsd	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_17
# BB#16:                                # %call.sqrt352
                                        #   in Loop: Header=BB0_8 Depth=1
	movapd	%xmm1, %xmm0
	movsd	%xmm3, 24(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	24(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB0_17:                               #   in Loop: Header=BB0_8 Depth=1
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movsd	%xmm0, (%rax,%rcx,8)
.LBB0_18:                               #   in Loop: Header=BB0_8 Depth=1
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	jle	.LBB0_22
# BB#19:                                #   in Loop: Header=BB0_8 Depth=1
	divsd	136(%rsp), %xmm3        # 8-byte Folded Reload
	movsd	72(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	%xmm3, %xmm0
	jbe	.LBB0_22
# BB#20:                                #   in Loop: Header=BB0_8 Depth=1
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	je	.LBB0_43
# BB#21:                                #   in Loop: Header=BB0_8 Depth=1
	movsd	64(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	96(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	72(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB0_43
.LBB0_22:                               #   in Loop: Header=BB0_8 Depth=1
	cmpl	$2, 40(%rsp)            # 4-byte Folded Reload
	jl	.LBB0_34
# BB#23:                                #   in Loop: Header=BB0_8 Depth=1
	movq	160(%rsp), %rbp         # 8-byte Reload
	movq	(%rbp), %rdi
	movq	168(%rsp), %rbx         # 8-byte Reload
	movq	(%rbx), %rsi
	movq	(%r12), %rdx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %rcx
	callq	hypre_SemiRestrict
	cmpl	$0, 116(%rsp)           # 4-byte Folded Reload
	movq	208(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rdi
	jle	.LBB0_24
# BB#27:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	%rax, %rbp
	xorl	%r14d, %r14d
	movq	%r12, %rbx
	movq	%r13, %r15
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %r12           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_28:                               # %.lr.ph
                                        #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	hypre_SMGRelaxSetRegSpaceRank
	movq	(%rbp,%r14,8), %rdi
	movl	$1, %esi
	movl	$1, %edx
	callq	hypre_SMGRelaxSetRegSpaceRank
	movq	(%rbp,%r14,8), %rdi
	movl	60(%rsp), %esi          # 4-byte Reload
	callq	hypre_SMGRelaxSetMaxIter
	movq	(%rbp,%r14,8), %rdi
	movl	$1, %esi
	callq	hypre_SMGRelaxSetZeroGuess
	movq	(%rbp,%r14,8), %rdi
	movq	8(%r13,%r14,8), %rsi
	movq	8(%r12,%r14,8), %rdx
	movq	8(%r15,%r14,8), %rcx
	callq	hypre_SMGRelax
	movq	176(%rsp), %rax         # 8-byte Reload
	movq	8(%rax,%r14,8), %rdi
	movq	8(%r13,%r14,8), %rsi
	movq	8(%r15,%r14,8), %rdx
	movq	8(%r12,%r14,8), %rcx
	movq	8(%rbx,%r14,8), %r8
	callq	hypre_SMGResidual
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	8(%rax,%r14,8), %rdi
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	8(%rax,%r14,8), %rsi
	movq	8(%rbx,%r14,8), %rdx
	movq	16(%r12,%r14,8), %rcx
	callq	hypre_SemiRestrict
	movq	8(%rbp,%r14,8), %rdi
	incq	%r14
	cmpq	%r14, 104(%rsp)         # 8-byte Folded Reload
	jne	.LBB0_28
# BB#29:                                #   in Loop: Header=BB0_8 Depth=1
	movq	200(%rsp), %rbx         # 8-byte Reload
	movq	192(%rsp), %r14         # 8-byte Reload
	movq	%r15, %r13
	jmp	.LBB0_30
.LBB0_24:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$1, %ebx
	movq	%rax, %r14
.LBB0_30:                               # %._crit_edge
                                        #   in Loop: Header=BB0_8 Depth=1
	movl	$1, %esi
	callq	hypre_SMGRelaxSetZeroGuess
	movq	(%r14), %rdi
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rsi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rbx,8), %rdx
	movq	(%r13,%rbx,8), %rcx
	callq	hypre_SMGRelax
	cmpl	$3, 40(%rsp)            # 4-byte Folded Reload
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	240(%rsp), %r12         # 8-byte Reload
	movq	152(%rsp), %rbp         # 8-byte Reload
	movq	248(%rsp), %r14         # 8-byte Reload
	jl	.LBB0_33
# BB#31:                                # %.lr.ph294.preheader
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	184(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_32:                               # %.lr.ph294
                                        #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%r12,%rbx,8), %rdi
	movq	-8(%r14,%rbx,8), %rsi
	movq	(%r13,%rbx,8), %rdx
	movq	-8(%rbp,%rbx,8), %rcx
	callq	hypre_SemiInterp
	movq	-8(%rbp,%rbx,8), %rdi
	movq	-8(%r13,%rbx,8), %rsi
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	hypre_StructAxpy
	movq	-8(%r15,%rbx,8), %rdi
	xorl	%esi, %esi
	movl	$1, %edx
	callq	hypre_SMGRelaxSetRegSpaceRank
	movq	-8(%r15,%rbx,8), %rdi
	movl	$1, %esi
	xorl	%edx, %edx
	callq	hypre_SMGRelaxSetRegSpaceRank
	movq	-8(%r15,%rbx,8), %rdi
	movl	56(%rsp), %esi          # 4-byte Reload
	callq	hypre_SMGRelaxSetMaxIter
	movq	-8(%r15,%rbx,8), %rdi
	xorl	%esi, %esi
	callq	hypre_SMGRelaxSetZeroGuess
	movq	-8(%r15,%rbx,8), %rdi
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	-8(%rax,%rbx,8), %rsi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	-8(%rax,%rbx,8), %rdx
	movq	-8(%r13,%rbx,8), %rcx
	callq	hypre_SMGRelax
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB0_32
.LBB0_33:                               # %._crit_edge295
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	(%r12), %rdi
	movq	(%r14), %rsi
	movq	8(%r13), %rdx
	movq	(%rbp), %rcx
	callq	hypre_SemiInterp
	movq	(%rbp), %rdi
	movq	(%r13), %rsi
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	224(%rsp), %rdx         # 8-byte Reload
	movq	216(%rsp), %rcx         # 8-byte Reload
	callq	hypre_SMGAxpy
	movq	232(%rsp), %r14         # 8-byte Reload
	movq	%rbp, %rbx
.LBB0_34:                               #   in Loop: Header=BB0_8 Depth=1
	cmpb	$0, 6(%rsp)             # 1-byte Folded Reload
	xorpd	%xmm0, %xmm0
	jne	.LBB0_36
# BB#35:                                #   in Loop: Header=BB0_8 Depth=1
	movsd	64(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB0_36:                               #   in Loop: Header=BB0_8 Depth=1
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	jne	.LBB0_38
# BB#37:                                #   in Loop: Header=BB0_8 Depth=1
	movsd	96(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB0_38:                               #   in Loop: Header=BB0_8 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	je	.LBB0_40
# BB#39:                                #   in Loop: Header=BB0_8 Depth=1
	movq	(%rbx), %rdi
	movq	%rdi, %rsi
	callq	hypre_StructInnerProd
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	movq	(%r13), %rdi
	movq	%rdi, %rsi
	callq	hypre_StructInnerProd
.LBB0_40:                               #   in Loop: Header=BB0_8 Depth=1
	movsd	%xmm0, 96(%rsp)         # 8-byte Spill
	cmpl	$2, 40(%rsp)            # 4-byte Folded Reload
	jl	.LBB0_42
# BB#41:                                #   in Loop: Header=BB0_8 Depth=1
	movq	(%r15), %rdi
	xorl	%esi, %esi
	movl	$1, %edx
	callq	hypre_SMGRelaxSetRegSpaceRank
	movq	(%r15), %rdi
	movl	$1, %esi
	xorl	%edx, %edx
	callq	hypre_SMGRelaxSetRegSpaceRank
.LBB0_42:                               #   in Loop: Header=BB0_8 Depth=1
	movq	(%r15), %rdi
	movl	56(%rsp), %esi          # 4-byte Reload
	callq	hypre_SMGRelaxSetMaxIter
	movq	(%r15), %rdi
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	callq	hypre_SMGRelaxSetZeroGuess
	movq	(%r15), %rdi
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rdx
	movq	(%r13), %rcx
	callq	hypre_SMGRelax
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movl	%ecx, 200(%r14)
	movq	%rcx, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	cmpq	144(%rsp), %rcx         # 8-byte Folded Reload
	jl	.LBB0_8
.LBB0_43:                               # %._crit_edge302
	movl	204(%r14), %edi
	callq	hypre_EndTiming
	xorl	%eax, %eax
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_SMGSolve, .Lfunc_end0-hypre_SMGSolve
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
