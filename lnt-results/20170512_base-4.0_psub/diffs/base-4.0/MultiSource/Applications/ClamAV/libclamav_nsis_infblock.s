	.text
	.file	"libclamav_nsis_infblock.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.text
	.globl	nsis_inflate
	.p2align	4, 0x90
	.type	nsis_inflate,@function
nsis_inflate:                           # @nsis_inflate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	(%r15), %rbp
	movl	8(%r15), %eax
	movq	1360(%r15), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	1356(%r15), %ecx
	movq	39912(%r15), %r13
	movq	39904(%r15), %rbx
	cmpq	%rbx, %r13
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	jae	.LBB0_2
# BB#1:
	movq	%r13, %rax
	notq	%rax
	addq	%rax, %rbx
	leaq	39896(%r15), %r12
	jmp	.LBB0_3
.LBB0_2:
	leaq	39896(%r15), %r12
	movq	39896(%r15), %rbx
	subq	%r13, %rbx
.LBB0_3:
	leaq	39912(%r15), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	7128(%r15), %rax
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	movq	%rax, 40(%rsp)          # 8-byte Spill
	subq	%rax, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	56(%r15), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	1344(%r15), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	1368(%r15), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	1336(%r15), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	jmp	.LBB0_4
.LBB0_226:                              #   in Loop: Header=BB0_4 Depth=1
	testb	$64, %cl
	jne	.LBB0_308
.LBB0_200:                              #   in Loop: Header=BB0_4 Depth=1
	leaq	(%rdx,%rax,4), %rsi
	movl	%ecx, 64(%r15)
	movzwl	2(%rdx,%rax,4), %eax
	leaq	(%rsi,%rax,4), %rax
	movq	%rax, 56(%r15)
	jmp	.LBB0_4
.LBB0_74:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, %rbp
	jmp	.LBB0_4
.LBB0_108:                              # %.critedge
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	$17, 40(%r15)
	movq	32(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_4:                                # %.thread958thread-pre-split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
                                        #       Child Loop BB0_101 Depth 3
                                        #       Child Loop BB0_105 Depth 3
                                        #       Child Loop BB0_121 Depth 3
                                        #         Child Loop BB0_124 Depth 4
                                        #         Child Loop BB0_145 Depth 4
                                        #         Child Loop BB0_168 Depth 4
                                        #         Child Loop BB0_171 Depth 4
                                        #         Child Loop BB0_176 Depth 4
                                        #         Child Loop BB0_179 Depth 4
                                        #       Child Loop BB0_65 Depth 3
                                        #       Child Loop BB0_31 Depth 3
                                        #       Child Loop BB0_289 Depth 3
                                        #       Child Loop BB0_270 Depth 3
                                        #       Child Loop BB0_230 Depth 3
                                        #       Child Loop BB0_243 Depth 3
                                        #         Child Loop BB0_250 Depth 4
                                        #       Child Loop BB0_206 Depth 3
                                        #       Child Loop BB0_217 Depth 3
                                        #       Child Loop BB0_188 Depth 3
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movl	40(%r15), %r14d
	cmpl	$15, %r14d
	jbe	.LBB0_6
	jmp	.LBB0_307
.LBB0_186:                              #   in Loop: Header=BB0_6 Depth=2
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
.LBB0_195:                              # %._crit_edge1483
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	56(%r15), %rdx
	movl	%eax, %eax
	movzwl	inflate_mask(%rax,%rax), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rsi
	andq	%rsi, %rax
	movzbl	1(%rdx,%rax,4), %ecx
	shrq	%cl, %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	subl	%ecx, %edi
	movzbl	(%rdx,%rax,4), %ecx
	testl	%ecx, %ecx
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	je	.LBB0_196
# BB#197:                               #   in Loop: Header=BB0_6 Depth=2
	testb	$16, %cl
	jne	.LBB0_198
# BB#199:                               #   in Loop: Header=BB0_6 Depth=2
	testb	$64, %cl
	je	.LBB0_200
# BB#201:                               #   in Loop: Header=BB0_6 Depth=2
	movl	$7, %r14d
	testb	$32, %cl
	jne	.LBB0_298
	jmp	.LBB0_202
.LBB0_196:                              #   in Loop: Header=BB0_6 Depth=2
	movzwl	2(%rdx,%rax,4), %eax
	movl	%eax, 56(%r15)
	movl	$6, %r14d
	jmp	.LBB0_298
.LBB0_198:                              #   in Loop: Header=BB0_6 Depth=2
	andl	$15, %ecx
	movl	%ecx, 56(%r15)
	movzwl	2(%rdx,%rax,4), %eax
	movl	%eax, 48(%r15)
	movl	$2, %r14d
	jmp	.LBB0_298
.LBB0_266:                              #   in Loop: Header=BB0_6 Depth=2
	movq	39904(%r15), %rax
	movq	40(%rsp), %rdx          # 8-byte Reload
	cmpq	%rdx, %rax
	je	.LBB0_268
# BB#267:                               #   in Loop: Header=BB0_6 Depth=2
	movq	64(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rax), %ecx
	subl	%edx, %r13d
	cmpq	%rax, %rdx
	movl	%r13d, %ebx
	cmovbl	%ecx, %ebx
	testl	%ebx, %ebx
	movq	%rdx, %r13
	jne	.LBB0_284
	jmp	.LBB0_268
.LBB0_60:                               #   in Loop: Header=BB0_6 Depth=2
	movq	39904(%r15), %rax
	movq	40(%rsp), %rdx          # 8-byte Reload
	cmpq	%rdx, %rax
	je	.LBB0_62
# BB#61:                                #   in Loop: Header=BB0_6 Depth=2
	movq	64(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rax), %ecx
	subl	%edx, %r13d
	cmpq	%rax, %rdx
	movl	%r13d, %ebx
	cmovbl	%ecx, %ebx
	testl	%ebx, %ebx
	movq	%rdx, %r13
	jne	.LBB0_73
	jmp	.LBB0_62
.LBB0_274:                              #   in Loop: Header=BB0_6 Depth=2
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %rdx
	je	.LBB0_276
# BB#275:                               #   in Loop: Header=BB0_6 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rdx), %eax
	subl	%ecx, %r13d
	cmpq	%rdx, %rcx
	movl	%r13d, %ebx
	cmovbl	%eax, %ebx
	movq	%rcx, %r13
.LBB0_276:                              #   in Loop: Header=BB0_6 Depth=2
	testl	%ebx, %ebx
	je	.LBB0_277
.LBB0_284:                              #   in Loop: Header=BB0_6 Depth=2
	movq	104(%rsp), %rax         # 8-byte Reload
	movb	(%rax), %al
	movb	%al, (%r13)
	incq	%r13
	decl	%ebx
	xorl	%r14d, %r14d
	movq	32(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_298
.LBB0_64:                               # %.lr.ph1490.preheader
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	48(%rsp), %r14          # 8-byte Reload
.LBB0_65:                               # %.lr.ph1490
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14), %rax
	cmpq	%r12, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_66
# BB#320:                               #   in Loop: Header=BB0_65 Depth=3
	movq	%rsi, (%r14)
	movq	%rsi, %rax
.LBB0_66:                               # %.backedge.i926.backedge
                                        #   in Loop: Header=BB0_65 Depth=3
	cmpq	%rax, %rsi
	movq	56(%rsp), %rax          # 8-byte Reload
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r12
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	movq	39896(%r15), %r13
	cmpq	%r13, %r12
	movq	%rbp, %rbx
	je	.LBB0_65
.LBB0_67:                               # %inflate_flush.exit927
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%r12, 39904(%r15)
	movq	39912(%r15), %rax
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	leal	(%r12,%rcx), %ecx
	subl	%eax, %ecx
	movl	%r13d, %ebx
	subl	%eax, %ebx
	cmpq	%r12, %rax
	cmovbl	%ecx, %ebx
	cmpq	%r13, %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	je	.LBB0_69
# BB#68:                                #   in Loop: Header=BB0_6 Depth=2
	movq	%rax, %r13
	movq	24(%rsp), %rsi          # 8-byte Reload
	testl	%ebx, %ebx
	jne	.LBB0_72
	jmp	.LBB0_75
.LBB0_69:                               #   in Loop: Header=BB0_6 Depth=2
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %r12
	movq	24(%rsp), %rsi          # 8-byte Reload
	je	.LBB0_71
# BB#70:                                #   in Loop: Header=BB0_6 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r12), %eax
	subl	%ecx, %r13d
	cmpq	%r12, %rcx
	movl	%r13d, %ebx
	cmovbl	%eax, %ebx
	movq	%rcx, %r13
.LBB0_71:                               #   in Loop: Header=BB0_6 Depth=2
	testl	%ebx, %ebx
	je	.LBB0_75
.LBB0_72:                               #   in Loop: Header=BB0_6 Depth=2
	movq	56(%rsp), %r12          # 8-byte Reload
.LBB0_73:                               #   in Loop: Header=BB0_6 Depth=2
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	cmpl	%esi, %ebx
	movl	%esi, %ebp
	cmovbl	%ebx, %ebp
	movl	48(%r15), %eax
	cmpl	%ebp, %eax
	cmovbl	%eax, %ebp
	movq	%r13, %rdi
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	addq	%rbp, %r14
	movq	%r14, %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	subl	%ebp, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	addq	%rbp, %r13
	subl	%ebp, %ebx
	movl	48(%r15), %eax
	subl	%ebp, %eax
	movl	%eax, 48(%r15)
	jne	.LBB0_74
# BB#82:                                #   in Loop: Header=BB0_6 Depth=2
	movl	1352(%r15), %r14d
	movq	%rcx, %rbp
.LBB0_298:                              # %.thread958.backedge.sink.split
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movl	%r14d, 40(%r15)
	.p2align	4, 0x90
.LBB0_5:                                # %.thread958
                                        #   in Loop: Header=BB0_6 Depth=2
	cmpl	$15, %r14d
	ja	.LBB0_307
.LBB0_6:                                # %.thread958
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_101 Depth 3
                                        #       Child Loop BB0_105 Depth 3
                                        #       Child Loop BB0_121 Depth 3
                                        #         Child Loop BB0_124 Depth 4
                                        #         Child Loop BB0_145 Depth 4
                                        #         Child Loop BB0_168 Depth 4
                                        #         Child Loop BB0_171 Depth 4
                                        #         Child Loop BB0_176 Depth 4
                                        #         Child Loop BB0_179 Depth 4
                                        #       Child Loop BB0_65 Depth 3
                                        #       Child Loop BB0_31 Depth 3
                                        #       Child Loop BB0_289 Depth 3
                                        #       Child Loop BB0_270 Depth 3
                                        #       Child Loop BB0_230 Depth 3
                                        #       Child Loop BB0_243 Depth 3
                                        #         Child Loop BB0_250 Depth 4
                                        #       Child Loop BB0_206 Depth 3
                                        #       Child Loop BB0_217 Depth 3
                                        #       Child Loop BB0_188 Depth 3
	movl	%r14d, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_285:                              #   in Loop: Header=BB0_6 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$8, %eax
	jb	.LBB0_287
# BB#286:                               #   in Loop: Header=BB0_6 Depth=2
	addl	$-8, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	decq	32(%rsp)                # 8-byte Folded Spill
.LBB0_287:                              #   in Loop: Header=BB0_6 Depth=2
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r13, %rbp
	movq	%r12, %rax
	movq	48(%rsp), %r14          # 8-byte Reload
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r13
	addq	%rbx, %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	movq	39896(%r15), %rdx
	cmpq	%rdx, %r13
	je	.LBB0_289
# BB#288:                               #   in Loop: Header=BB0_6 Depth=2
	movq	%rbx, %rbp
	jmp	.LBB0_291
	.p2align	4, 0x90
.LBB0_289:                              # %.lr.ph1360
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_290
# BB#323:                               #   in Loop: Header=BB0_289 Depth=3
	movq	%rsi, (%r14)
	movq	%rsi, %rax
.LBB0_290:                              # %.backedge.i821.backedge
                                        #   in Loop: Header=BB0_289 Depth=3
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	movq	39896(%r15), %rdx
	cmpq	%rdx, %r13
	movq	%rbp, %rbx
	je	.LBB0_289
.LBB0_291:                              # %inflate_flush.exit822
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%r13, 39904(%r15)
	movq	39912(%r15), %rcx
	cmpq	%r13, %rcx
	jne	.LBB0_292
# BB#299:                               #   in Loop: Header=BB0_6 Depth=2
	movl	%edx, %ebx
	subl	%ecx, %ebx
	movl	1352(%r15), %ecx
	movl	%ecx, 40(%r15)
	movl	$8, %r14d
	cmpl	$8, %ecx
	je	.LBB0_5
	jmp	.LBB0_300
.LBB0_183:                              # %.loopexit2093.loopexit
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%r12, 56(%rsp)          # 8-byte Spill
	jmp	.LBB0_184
.LBB0_7:                                # %.thread958._crit_edge1765
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movl	64(%r15), %eax
	jmp	.LBB0_185
.LBB0_203:                              #   in Loop: Header=BB0_6 Depth=2
	movl	56(%r15), %eax
	cmpl	%eax, 8(%rsp)           # 4-byte Folded Reload
	jae	.LBB0_204
# BB#205:                               # %.lr.ph1392.preheader
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_206:                              # %.lr.ph1392
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%edi, %edi
	je	.LBB0_207
# BB#212:                               #   in Loop: Header=BB0_206 Depth=3
	decl	%edi
	movzbl	(%rsi), %edx
	incq	%rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	shlq	%cl, %rdx
	orq	%rdx, %rbp
	addl	$8, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmpl	%eax, %ecx
	jb	.LBB0_206
	jmp	.LBB0_213
.LBB0_9:                                # %.thread958._crit_edge1760
                                        #   in Loop: Header=BB0_6 Depth=2
	movl	64(%r15), %eax
	movq	8(%rsp), %rdx           # 8-byte Reload
	jmp	.LBB0_214
.LBB0_227:                              #   in Loop: Header=BB0_6 Depth=2
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	cmpl	%eax, 8(%rsp)           # 4-byte Folded Reload
	jae	.LBB0_228
# BB#229:                               # %.lr.ph1370.preheader
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_230:                              # %.lr.ph1370
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%edi, %edi
	je	.LBB0_231
# BB#236:                               #   in Loop: Header=BB0_230 Depth=3
	decl	%edi
	movzbl	(%rsi), %edx
	incq	%rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	shlq	%cl, %rdx
	orq	%rdx, %rbp
	addl	$8, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmpl	%eax, %ecx
	jb	.LBB0_230
	jmp	.LBB0_237
.LBB0_10:                               # %.thread958._crit_edge
                                        #   in Loop: Header=BB0_6 Depth=2
	movl	60(%r15), %edx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB0_238
.LBB0_264:                              #   in Loop: Header=BB0_6 Depth=2
	testl	%ebx, %ebx
	jne	.LBB0_284
# BB#265:                               #   in Loop: Header=BB0_6 Depth=2
	cmpq	(%r12), %r13
	je	.LBB0_266
.LBB0_268:                              # %.thread966
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r13, %rbp
	movq	%r12, %rax
	movq	48(%rsp), %r14          # 8-byte Reload
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %rdx
	addq	%rbx, %rdx
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	movq	39896(%r15), %r13
	cmpq	%r13, %rdx
	je	.LBB0_270
# BB#269:                               #   in Loop: Header=BB0_6 Depth=2
	movq	%rbx, %rbp
	jmp	.LBB0_272
.LBB0_270:                              # %.lr.ph1364
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14), %rax
	cmpq	%rdx, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_271
# BB#322:                               #   in Loop: Header=BB0_270 Depth=3
	movq	%rsi, (%r14)
	movq	%rsi, %rax
.LBB0_271:                              # %.backedge.i835.backedge
                                        #   in Loop: Header=BB0_270 Depth=3
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %rdx
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	movq	39896(%r15), %r13
	cmpq	%r13, %rdx
	movq	%rbp, %rbx
	je	.LBB0_270
.LBB0_272:                              # %inflate_flush.exit836
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%rdx, 39904(%r15)
	movq	39912(%r15), %rax
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	leal	(%rdx,%rcx), %ecx
	subl	%eax, %ecx
	movl	%r13d, %ebx
	subl	%eax, %ebx
	cmpq	%rdx, %rax
	cmovbl	%ecx, %ebx
	cmpq	%r13, %rax
	je	.LBB0_274
# BB#273:                               #   in Loop: Header=BB0_6 Depth=2
	movq	%rax, %r13
	testl	%ebx, %ebx
	jne	.LBB0_284
	jmp	.LBB0_277
.LBB0_15:                               # %.preheader1034
                                        #   in Loop: Header=BB0_6 Depth=2
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	ja	.LBB0_16
# BB#17:                                # %.lr.ph1505
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	24(%rsp), %r14          # 8-byte Reload
	testl	%r14d, %r14d
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_18
# BB#23:                                # %._crit_edge1506.loopexit
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	leal	8(%rcx), %eax
	movzbl	(%rbp), %edx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rdx
	movq	16(%rsp), %rcx          # 8-byte Reload
	orq	%rdx, %rcx
	incq	%rbp
	decl	%r14d
	movl	%eax, %edx
	jmp	.LBB0_24
.LBB0_13:                               # %.preheader1035
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$16, %eax
	jae	.LBB0_14
# BB#40:                                # %.lr.ph1496
                                        #   in Loop: Header=BB0_6 Depth=2
	movl	%eax, %ecx
	movq	24(%rsp), %rdi          # 8-byte Reload
	testl	%edi, %edi
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_41
# BB#46:                                #   in Loop: Header=BB0_6 Depth=2
	leal	-1(%rdi), %edx
	leaq	1(%rbp), %rax
	movzbl	(%rbp), %esi
	shlq	%cl, %rsi
	orq	%rsi, 16(%rsp)          # 8-byte Folded Spill
	addq	$8, %rcx
	cmpq	$16, %rcx
	jae	.LBB0_47
# BB#317:                               # %.lr.ph1496.1
                                        #   in Loop: Header=BB0_6 Depth=2
	testl	%edx, %edx
	je	.LBB0_318
# BB#319:                               #   in Loop: Header=BB0_6 Depth=2
	addl	$-2, %edi
	movzbl	1(%rbp), %eax
	addq	$2, %rbp
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rax
	orq	%rax, 16(%rsp)          # 8-byte Folded Spill
	jmp	.LBB0_48
.LBB0_52:                               #   in Loop: Header=BB0_6 Depth=2
	movq	24(%rsp), %rsi          # 8-byte Reload
	testl	%esi, %esi
	je	.LBB0_53
# BB#58:                                #   in Loop: Header=BB0_6 Depth=2
	testl	%ebx, %ebx
	jne	.LBB0_73
# BB#59:                                #   in Loop: Header=BB0_6 Depth=2
	cmpq	(%r12), %r13
	je	.LBB0_60
.LBB0_62:                               # %.thread
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%r13, 39912(%r15)
	movq	%r12, %rax
	movq	39904(%r15), %r12
	movq	24(%r15), %rdi
	cmpq	%r13, %r12
	movq	%rax, 56(%rsp)          # 8-byte Spill
	cmovbeq	48(%rsp), %rax          # 8-byte Folded Reload
	movl	(%rax), %ebx
	subl	%r12d, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	addq	%rbx, %r12
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	movq	39896(%r15), %r13
	cmpq	%r13, %r12
	je	.LBB0_64
# BB#63:                                #   in Loop: Header=BB0_6 Depth=2
	movq	%rbx, %rbp
	jmp	.LBB0_67
.LBB0_11:                               # %.preheader1043
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$14, %eax
	jae	.LBB0_12
# BB#83:                                # %.lr.ph1413
                                        #   in Loop: Header=BB0_6 Depth=2
	movl	%eax, %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_84
# BB#90:                                #   in Loop: Header=BB0_6 Depth=2
	leal	-1(%rcx), %esi
	leaq	1(%rbp), %r8
	movzbl	(%rbp), %edx
	movl	%eax, %ecx
	shlq	%cl, %rdx
	orq	16(%rsp), %rdx          # 8-byte Folded Reload
	leaq	8(%rax), %rdi
	movq	%rdi, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmpq	$14, %rdi
	jae	.LBB0_91
# BB#314:                               # %.lr.ph1413.1
                                        #   in Loop: Header=BB0_6 Depth=2
	testl	%esi, %esi
	movq	48(%rsp), %r14          # 8-byte Reload
	je	.LBB0_315
# BB#316:                               #   in Loop: Header=BB0_6 Depth=2
	movq	24(%rsp), %rdi          # 8-byte Reload
	addl	$-2, %edi
	movzbl	1(%rbp), %esi
	addq	$2, %rbp
	movq	8(%rsp), %rcx           # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rsi
	orq	%rsi, %rdx
	addq	$16, %rax
	movl	%edi, %esi
	movq	%rbp, %r8
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB0_92
.LBB0_103:                              # %.preheader1042
                                        #   in Loop: Header=BB0_6 Depth=2
	movl	48(%r15), %ecx
	movl	52(%r15), %eax
	shrl	$10, %ecx
	addl	$4, %ecx
	cmpl	%ecx, %eax
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	jb	.LBB0_101
	jmp	.LBB0_104
.LBB0_8:                                # %.thread958..preheader1040_crit_edge
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movl	52(%r15), %esi
	movq	120(%rsp), %r14         # 8-byte Reload
	jmp	.LBB0_118
.LBB0_204:                              #   in Loop: Header=BB0_6 Depth=2
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB0_213:                              # %._crit_edge1393
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movzwl	inflate_mask(%rax,%rax), %ecx
	andl	%ebp, %ecx
	addl	%ecx, 48(%r15)
	movl	%eax, %ecx
	shrq	%cl, %rbp
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rdx           # 8-byte Reload
	subl	%eax, %edx
	movzbl	73(%r15), %eax
	movl	%eax, 64(%r15)
	movq	88(%r15), %rcx
	movq	%rcx, 56(%r15)
	movl	$3, 40(%r15)
.LBB0_214:                              #   in Loop: Header=BB0_6 Depth=2
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	cmpl	%eax, %edx
	jae	.LBB0_215
# BB#216:                               # %.lr.ph1403.preheader
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_217:                              # %.lr.ph1403
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%esi, %esi
	je	.LBB0_218
# BB#223:                               #   in Loop: Header=BB0_217 Depth=3
	decl	%esi
	movzbl	(%rbp), %edx
	incq	%rbp
	movq	8(%rsp), %rcx           # 8-byte Reload
	shlq	%cl, %rdx
	orq	%rdx, 16(%rsp)          # 8-byte Folded Spill
	addl	$8, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmpl	%eax, %ecx
	jb	.LBB0_217
	jmp	.LBB0_224
.LBB0_215:                              #   in Loop: Header=BB0_6 Depth=2
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
.LBB0_224:                              # %._crit_edge1404
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	56(%r15), %rdx
	movl	%eax, %eax
	movzwl	inflate_mask(%rax,%rax), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rsi
	andq	%rsi, %rax
	movzbl	1(%rdx,%rax,4), %ecx
	shrq	%cl, %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rsi           # 8-byte Reload
	subl	%ecx, %esi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movzbl	(%rdx,%rax,4), %ecx
	testb	$16, %cl
	je	.LBB0_226
# BB#225:                               #   in Loop: Header=BB0_6 Depth=2
	andl	$15, %ecx
	movl	%ecx, 56(%r15)
	movzwl	2(%rdx,%rax,4), %eax
	movl	%eax, 60(%r15)
	movl	$4, %r14d
	jmp	.LBB0_298
.LBB0_228:                              #   in Loop: Header=BB0_6 Depth=2
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB0_237:                              # %._crit_edge
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movzwl	inflate_mask(%rax,%rax), %edx
	andl	%ebp, %edx
	addl	60(%r15), %edx
	movl	%edx, 60(%r15)
	movl	%eax, %ecx
	shrq	%cl, %rbp
	movq	8(%rsp), %rsi           # 8-byte Reload
	subl	%eax, %esi
	movl	$5, 40(%r15)
.LBB0_238:                              #   in Loop: Header=BB0_6 Depth=2
	movq	%r13, %rcx
	subq	40(%rsp), %rcx          # 8-byte Folded Reload
	cmpl	%edx, %ecx
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	jae	.LBB0_240
# BB#239:                               #   in Loop: Header=BB0_6 Depth=2
	movq	(%r12), %rdi
	movl	%edx, %eax
	subq	%rcx, %rax
	jmp	.LBB0_241
.LBB0_240:                              #   in Loop: Header=BB0_6 Depth=2
	movl	%edx, %eax
	movq	%r13, %rdi
.LBB0_241:                              #   in Loop: Header=BB0_6 Depth=2
	xorl	%r14d, %r14d
	cmpl	$0, 48(%r15)
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_298
# BB#242:                               # %.lr.ph1383.preheader
                                        #   in Loop: Header=BB0_6 Depth=2
	subq	%rax, %rdi
	movq	%r12, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_243:                              # %.lr.ph1383
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_250 Depth 4
	testl	%ebx, %ebx
	jne	.LBB0_258
# BB#244:                               #   in Loop: Header=BB0_243 Depth=3
	cmpq	(%r12), %r13
	je	.LBB0_245
.LBB0_247:                              # %.thread963
                                        #   in Loop: Header=BB0_243 Depth=3
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r13, %rbp
	movq	%r12, %rax
	cmovbeq	48(%rsp), %rax          # 8-byte Folded Reload
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %rsi
	addq	%rbx, %rsi
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	movq	39896(%r15), %r13
	cmpq	%r13, %rsi
	je	.LBB0_249
# BB#248:                               #   in Loop: Header=BB0_243 Depth=3
	movq	%rbx, %r12
	jmp	.LBB0_252
.LBB0_245:                              #   in Loop: Header=BB0_243 Depth=3
	movq	39904(%r15), %rax
	movq	40(%rsp), %rdx          # 8-byte Reload
	cmpq	%rdx, %rax
	je	.LBB0_247
# BB#246:                               #   in Loop: Header=BB0_243 Depth=3
	movq	64(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rax), %ecx
	subl	%edx, %r13d
	cmpq	%rax, %rdx
	movl	%r13d, %ebx
	cmovbl	%ecx, %ebx
	testl	%ebx, %ebx
	movq	%rdx, %r13
	jne	.LBB0_258
	jmp	.LBB0_247
.LBB0_249:                              # %.lr.ph1377.preheader
                                        #   in Loop: Header=BB0_243 Depth=3
	movq	48(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_250:                              # %.lr.ph1377
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_243 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rbp), %rax
	cmpq	%rsi, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_251
# BB#321:                               #   in Loop: Header=BB0_250 Depth=4
	movq	%rsi, (%rbp)
	movq	%rsi, %rax
.LBB0_251:                              # %.backedge.i849.backedge
                                        #   in Loop: Header=BB0_250 Depth=4
	cmpq	%rax, %rsi
	movq	56(%rsp), %rax          # 8-byte Reload
	cmovbeq	%rbp, %rax
	movl	(%rax), %r12d
	subl	%esi, %r12d
	movl	32(%r15), %eax
	cmpl	%eax, %r12d
	cmovael	%eax, %r12d
	subl	%r12d, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%r12, %rdx
	callq	memcpy
	leaq	7128(%r15,%r12), %rsi
	addq	24(%r15), %r12
	movq	%r12, 24(%r15)
	movq	39896(%r15), %r13
	cmpq	%r13, %rsi
	movq	%r12, %rbx
	je	.LBB0_250
.LBB0_252:                              # %inflate_flush.exit850
                                        #   in Loop: Header=BB0_243 Depth=3
	movq	%rsi, 39904(%r15)
	movq	39912(%r15), %rax
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	leal	(%rsi,%rcx), %ecx
	subl	%eax, %ecx
	movl	%r13d, %ebx
	subl	%eax, %ebx
	cmpq	%rsi, %rax
	cmovbl	%ecx, %ebx
	cmpq	%r13, %rax
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_254
# BB#253:                               #   in Loop: Header=BB0_243 Depth=3
	movq	%rax, %r13
	movq	96(%rsp), %rdi          # 8-byte Reload
	testl	%ebx, %ebx
	jne	.LBB0_257
	jmp	.LBB0_259
.LBB0_254:                              #   in Loop: Header=BB0_243 Depth=3
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %rsi
	movq	96(%rsp), %rdi          # 8-byte Reload
	je	.LBB0_256
# BB#255:                               #   in Loop: Header=BB0_243 Depth=3
	movq	64(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rsi), %eax
	subl	%ecx, %r13d
	cmpq	%rsi, %rcx
	movl	%r13d, %ebx
	cmovbl	%eax, %ebx
	movq	%rcx, %r13
.LBB0_256:                              #   in Loop: Header=BB0_243 Depth=3
	testl	%ebx, %ebx
	je	.LBB0_259
.LBB0_257:                              #   in Loop: Header=BB0_243 Depth=3
	movq	56(%rsp), %r12          # 8-byte Reload
.LBB0_258:                              #   in Loop: Header=BB0_243 Depth=3
	movb	(%rdi), %al
	incq	%rdi
	movb	%al, (%r13)
	incq	%r13
	decl	%ebx
	cmpq	39896(%r15), %rdi
	cmoveq	40(%rsp), %rdi          # 8-byte Folded Reload
	movl	48(%r15), %eax
	decl	%eax
	movl	%eax, 48(%r15)
	jne	.LBB0_243
	jmp	.LBB0_298
.LBB0_16:                               #   in Loop: Header=BB0_6 Depth=2
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB0_24:                               # %._crit_edge1506
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%rcx, %rax
	shrq	$3, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	addl	$-3, %edx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	testb	$1, %al
	movl	$8, %ecx
	movl	$15, %edx
	cmovnel	%edx, %ecx
	movl	%ecx, 1352(%r15)
	shrb	%al
	andb	$3, %al
	cmpb	$1, %al
	movq	%r14, 24(%rsp)          # 8-byte Spill
	je	.LBB0_29
# BB#25:                                # %._crit_edge1506
                                        #   in Loop: Header=BB0_6 Depth=2
	cmpb	$2, %al
	je	.LBB0_26
# BB#27:                                # %._crit_edge1506
                                        #   in Loop: Header=BB0_6 Depth=2
	cmpb	$3, %al
	je	.LBB0_28
# BB#297:                               #   in Loop: Header=BB0_6 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %ecx
	andl	$7, %ecx
	shrq	%cl, 16(%rsp)           # 8-byte Folded Spill
	subl	%ecx, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$9, %r14d
	jmp	.LBB0_298
.LBB0_14:                               #   in Loop: Header=BB0_6 Depth=2
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_48
.LBB0_12:                               #   in Loop: Header=BB0_6 Depth=2
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_93
.LBB0_29:                               #   in Loop: Header=BB0_6 Depth=2
	cmpb	$0, fixed_built(%rip)
	jne	.LBB0_39
# BB#30:                                #   in Loop: Header=BB0_6 Depth=2
	movl	$0, 72(%rsp)
	movl	$1, %eax
.LBB0_31:                               #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	-1(%rax), %rdx
	movl	$8, %ecx
	cmpq	$144, %rdx
	movl	$8, %esi
	jl	.LBB0_34
# BB#32:                                #   in Loop: Header=BB0_31 Depth=3
	movl	$9, %esi
	cmpq	$256, %rdx              # imm = 0x100
	jl	.LBB0_34
# BB#33:                                #   in Loop: Header=BB0_31 Depth=3
	xorl	%esi, %esi
	cmpq	$279, %rdx              # imm = 0x117
	setg	%sil
	addl	$7, %esi
.LBB0_34:                               #   in Loop: Header=BB0_31 Depth=3
	movl	%esi, nsis_inflate.lc-4(,%rax,4)
	cmpq	$142, %rdx
	jle	.LBB0_37
# BB#35:                                #   in Loop: Header=BB0_31 Depth=3
	movl	$9, %ecx
	cmpq	$256, %rax              # imm = 0x100
	jl	.LBB0_37
# BB#36:                                #   in Loop: Header=BB0_31 Depth=3
	xorl	%ecx, %ecx
	cmpq	$279, %rax              # imm = 0x117
	setg	%cl
	addl	$7, %ecx
.LBB0_37:                               #   in Loop: Header=BB0_31 Depth=3
	movl	%ecx, nsis_inflate.lc(,%rax,4)
	addq	$2, %rax
	cmpq	$289, %rax              # imm = 0x121
	jne	.LBB0_31
# BB#38:                                #   in Loop: Header=BB0_6 Depth=2
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movl	$nsis_inflate.lc, %edi
	movl	$288, %esi              # imm = 0x120
	movl	$257, %edx              # imm = 0x101
	movl	$cplens, %ecx
	movl	$cplext, %r8d
	movl	$fixed_tl, %r9d
	leaq	80(%rsp), %rax
	pushq	%rax
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	$fixed_mem
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	$fixed_bl
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	huft_build
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [5,5,5,5]
	movdqa	%xmm0, nsis_inflate.lc(%rip)
	movdqa	%xmm0, nsis_inflate.lc+16(%rip)
	movdqa	%xmm0, nsis_inflate.lc+32(%rip)
	movdqa	%xmm0, nsis_inflate.lc+48(%rip)
	movdqa	%xmm0, nsis_inflate.lc+64(%rip)
	movdqa	%xmm0, nsis_inflate.lc+80(%rip)
	movdqa	%xmm0, nsis_inflate.lc+96(%rip)
	movabsq	$21474836485, %rax      # imm = 0x500000005
	movq	%rax, nsis_inflate.lc+112(%rip)
	subq	$8, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	movl	$nsis_inflate.lc, %edi
	movl	$30, %esi
	xorl	%edx, %edx
	movl	$cpdist, %ecx
	movl	$cpdext, %r8d
	movl	$fixed_td, %r9d
	leaq	80(%rsp), %rax
	pushq	%rax
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	$fixed_mem
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	$fixed_bd
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	callq	huft_build
	addq	$32, %rsp
.Lcfi22:
	.cfi_adjust_cfa_offset -32
	incb	fixed_built(%rip)
.LBB0_39:                               #   in Loop: Header=BB0_6 Depth=2
	movb	fixed_bl(%rip), %al
	movb	%al, 72(%r15)
	movb	fixed_bd(%rip), %al
	movb	%al, 73(%r15)
	movq	fixed_tl(%rip), %rax
	movq	%rax, 80(%r15)
	movq	fixed_td(%rip), %rax
	movq	%rax, 88(%r15)
	xorl	%r14d, %r14d
	jmp	.LBB0_298
.LBB0_26:                               #   in Loop: Header=BB0_6 Depth=2
	movl	$11, %r14d
	jmp	.LBB0_298
.LBB0_47:                               #   in Loop: Header=BB0_6 Depth=2
	movq	%rax, %rbp
	movl	%edx, %edi
.LBB0_48:                               # %._crit_edge1497
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movzwl	16(%rsp), %eax          # 2-byte Folded Reload
	movl	%eax, 48(%r15)
	testl	%eax, %eax
	je	.LBB0_51
# BB#49:                                #   in Loop: Header=BB0_6 Depth=2
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$10, %r14d
	jmp	.LBB0_50
.LBB0_51:                               #   in Loop: Header=BB0_6 Depth=2
	movl	1352(%r15), %r14d
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB0_50:                               # %.thread958.backedge.sink.split
                                        #   in Loop: Header=BB0_6 Depth=2
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB0_298
.LBB0_91:                               #   in Loop: Header=BB0_6 Depth=2
	movq	48(%rsp), %r14          # 8-byte Reload
.LBB0_92:                               # %._crit_edge1414.loopexit
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%rdx, %rbp
	movl	%esi, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB0_93:                               # %._crit_edge1414
                                        #   in Loop: Header=BB0_6 Depth=2
	movl	%ebp, %eax
	andl	$16383, %eax            # imm = 0x3FFF
	movl	%eax, 48(%r15)
	movl	%ebp, %eax
	andl	$30, %eax
	cmpl	$29, %eax
	ja	.LBB0_95
# BB#94:                                # %._crit_edge1414
                                        #   in Loop: Header=BB0_6 Depth=2
	movl	%ebp, %eax
	andl	$960, %eax              # imm = 0x3C0
	cmpl	$929, %eax              # imm = 0x3A1
	jae	.LBB0_95
# BB#100:                               # %.preheader1042.thread
                                        #   in Loop: Header=BB0_6 Depth=2
	shrq	$14, %rbp
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	$-14, %esi
	movl	$0, 52(%r15)
	movl	$12, 40(%r15)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_101:                              # %.preheader
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rsi, %rcx
	cmpl	$2, %esi
	ja	.LBB0_102
# BB#109:                               # %.lr.ph1423
                                        #   in Loop: Header=BB0_101 Depth=3
	movq	%rbp, %rdi
	movq	24(%rsp), %rbp          # 8-byte Reload
	testl	%ebp, %ebp
	je	.LBB0_110
# BB#115:                               # %._crit_edge1424.loopexit
                                        #   in Loop: Header=BB0_101 Depth=3
	leal	8(%rcx), %edx
	movzbl	(%r8), %esi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rsi
	orq	%rsi, %rdi
	incq	%r8
	decl	%ebp
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	movl	%edx, %esi
	jmp	.LBB0_116
	.p2align	4, 0x90
.LBB0_102:                              #   in Loop: Header=BB0_101 Depth=3
	movq	%rcx, %rsi
.LBB0_116:                              # %._crit_edge1424
                                        #   in Loop: Header=BB0_101 Depth=3
	movl	%ebp, %ecx
	andl	$7, %ecx
	leal	1(%rax), %edx
	movl	%edx, 52(%r15)
	movl	%eax, %eax
	movsbq	border(%rax), %rax
	movl	%ecx, 56(%r15,%rax,4)
	shrq	$3, %rbp
	addl	$-3, %esi
	movl	48(%r15), %ecx
	movl	52(%r15), %eax
	shrl	$10, %ecx
	addl	$4, %ecx
	cmpl	%ecx, %eax
	jb	.LBB0_101
.LBB0_104:                              # %.preheader1041
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%r8, 32(%rsp)           # 8-byte Spill
	cmpl	$18, %eax
	ja	.LBB0_106
	.p2align	4, 0x90
.LBB0_105:                              # %.lr.ph1438
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 52(%r15)
	movl	%eax, %eax
	movsbq	border(%rax), %rax
	movl	$0, 56(%r15,%rax,4)
	movl	52(%r15), %eax
	cmpl	$19, %eax
	jb	.LBB0_105
.LBB0_106:                              # %._crit_edge1439
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	120(%rsp), %r14         # 8-byte Reload
	movl	$7, (%r14)
	movl	$0, 72(%rsp)
	subq	$8, %rsp
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	movl	$19, %esi
	movl	$19, %edx
	movl	$0, %ecx
	movl	$0, %r8d
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	136(%rsp), %r9          # 8-byte Reload
	leaq	80(%rsp), %rax
	pushq	%rax
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)               # 8-byte Folded Reload
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	callq	huft_build
	addq	$32, %rsp
.Lcfi27:
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB0_108
# BB#107:                               #   in Loop: Header=BB0_6 Depth=2
	cmpl	$0, (%r14)
	je	.LBB0_108
# BB#117:                               #   in Loop: Header=BB0_6 Depth=2
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movl	$0, 52(%r15)
	movl	$13, 40(%r15)
	xorl	%esi, %esi
.LBB0_118:                              # %.preheader1040
                                        #   in Loop: Header=BB0_6 Depth=2
	movl	48(%r15), %eax
	movl	%eax, %r12d
	andl	$31, %r12d
	shrl	$5, %eax
	andl	$31, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leal	258(%r12,%rax), %r8d
	cmpl	%r8d, %esi
	jae	.LBB0_119
# BB#120:                               # %.lr.ph1466.preheader
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%r12, 136(%rsp)         # 8-byte Spill
.LBB0_121:                              # %.lr.ph1466
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_124 Depth 4
                                        #         Child Loop BB0_145 Depth 4
                                        #         Child Loop BB0_168 Depth 4
                                        #         Child Loop BB0_171 Depth 4
                                        #         Child Loop BB0_176 Depth 4
                                        #         Child Loop BB0_179 Depth 4
	movl	(%r14), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	%eax, %ecx
	jae	.LBB0_122
# BB#123:                               # %.lr.ph1445.preheader
                                        #   in Loop: Header=BB0_121 Depth=3
	movq	24(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_124:                              # %.lr.ph1445
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_121 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testl	%edi, %edi
	je	.LBB0_125
# BB#130:                               #   in Loop: Header=BB0_124 Depth=4
	decl	%edi
	movzbl	(%rbp), %edx
	incq	%rbp
	shlq	%cl, %rdx
	orq	%rdx, 16(%rsp)          # 8-byte Folded Spill
	addl	$8, %ecx
	cmpl	%eax, %ecx
	jb	.LBB0_124
	jmp	.LBB0_131
.LBB0_122:                              #   in Loop: Header=BB0_121 Depth=3
	movq	24(%rsp), %rdi          # 8-byte Reload
.LBB0_131:                              # %._crit_edge1446
                                        #   in Loop: Header=BB0_121 Depth=3
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %rcx
	movzwl	inflate_mask(%rax,%rax), %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
	andq	%rdx, %rax
	movzbl	1(%rcx,%rax,4), %r12d
	movzwl	2(%rcx,%rax,4), %r14d
	cmpl	$15, %r14d
	ja	.LBB0_144
# BB#132:                               #   in Loop: Header=BB0_121 Depth=3
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movl	%r12d, %ecx
	shrq	%cl, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	subl	%r12d, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%esi, %eax
	leal	1(%rsi), %ecx
	movl	%ecx, 52(%r15)
	movl	%r14d, 56(%r15,%rax,4)
	movl	%ecx, %esi
	movq	136(%rsp), %r12         # 8-byte Reload
	jmp	.LBB0_133
.LBB0_144:                              #   in Loop: Header=BB0_121 Depth=3
	movl	%r8d, 32(%rsp)          # 4-byte Spill
	movq	%rbp, %r8
	leal	-14(%r14), %r11d
	xorl	%r10d, %r10d
	cmpl	$18, %r14d
	sete	%r9b
	movl	$7, %eax
	cmovel	%eax, %r11d
	leal	(%r11,%r12), %ebp
	cmpl	%ebp, 8(%rsp)           # 4-byte Folded Reload
	jae	.LBB0_152
	.p2align	4, 0x90
.LBB0_145:                              # %.lr.ph1456
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_121 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testl	%edi, %edi
	je	.LBB0_146
# BB#151:                               #   in Loop: Header=BB0_145 Depth=4
	decl	%edi
	movzbl	(%r8), %eax
	incq	%r8
	movq	8(%rsp), %rcx           # 8-byte Reload
	shlq	%cl, %rax
	orq	%rax, 16(%rsp)          # 8-byte Folded Spill
	addl	$8, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmpl	%ebp, %ecx
	jb	.LBB0_145
.LBB0_152:                              # %._crit_edge1457
                                        #   in Loop: Header=BB0_121 Depth=3
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movb	%r9b, %r10b
	leal	3(,%r10,8), %r9d
	movl	%r12d, %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
	shrq	%cl, %rdx
	movq	8(%rsp), %rdi           # 8-byte Reload
	subl	%r12d, %edi
	movl	%r11d, %eax
	movzwl	inflate_mask(%rax,%rax), %r10d
	andl	%edx, %r10d
	leal	(%r10,%r9), %ebp
	movl	%r11d, %ecx
	shrq	%cl, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	subl	%r11d, %edi
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	leal	(%rbp,%rsi), %eax
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	ja	.LBB0_155
# BB#153:                               #   in Loop: Header=BB0_121 Depth=3
	cmpl	$16, %r14d
	jne	.LBB0_160
# BB#154:                               #   in Loop: Header=BB0_121 Depth=3
	testl	%esi, %esi
	je	.LBB0_155
.LBB0_160:                              #   in Loop: Header=BB0_121 Depth=3
	xorl	%eax, %eax
	movzwl	%r14w, %ecx
	cmpl	$16, %ecx
	jne	.LBB0_162
# BB#161:                               #   in Loop: Header=BB0_121 Depth=3
	leal	-1(%rsi), %eax
	movl	56(%r15,%rax,4), %eax
.LBB0_162:                              #   in Loop: Header=BB0_121 Depth=3
	leal	(%r9,%rsi), %r11d
	cmpl	$8, %ebp
	movq	136(%rsp), %r12         # 8-byte Reload
	jb	.LBB0_174
# BB#163:                               # %min.iters.checked
                                        #   in Loop: Header=BB0_121 Depth=3
	movl	%ebp, %r14d
	andl	$131064, %r14d          # imm = 0x1FFF8
	je	.LBB0_174
# BB#164:                               # %vector.scevcheck
                                        #   in Loop: Header=BB0_121 Depth=3
	leal	-1(%r9,%r10), %ecx
	addl	%esi, %ecx
	jb	.LBB0_174
# BB#165:                               # %vector.ph
                                        #   in Loop: Header=BB0_121 Depth=3
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leal	-8(%r14), %r9d
	movl	%r9d, %ecx
	shrl	$3, %ecx
	incl	%ecx
	andl	$3, %ecx
	je	.LBB0_166
# BB#167:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_121 Depth=3
	negl	%ecx
	xorl	%edi, %edi
.LBB0_168:                              # %vector.body.prol
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_121 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%rsi,%rdi), %edx
	movdqu	%xmm0, 56(%r15,%rdx,4)
	movdqu	%xmm0, 72(%r15,%rdx,4)
	addl	$8, %edi
	incl	%ecx
	jne	.LBB0_168
	jmp	.LBB0_169
.LBB0_166:                              #   in Loop: Header=BB0_121 Depth=3
	xorl	%edi, %edi
.LBB0_169:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_121 Depth=3
	cmpl	$24, %r9d
	jb	.LBB0_172
# BB#170:                               # %vector.ph.new
                                        #   in Loop: Header=BB0_121 Depth=3
	movl	%esi, %ecx
	movl	%r14d, %edx
.LBB0_171:                              # %vector.body
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_121 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%rdi,%rcx), %r9d
	movdqu	%xmm0, 56(%r15,%r9,4)
	movdqu	%xmm0, 72(%r15,%r9,4)
	leal	8(%rdi,%rcx), %r9d
	movdqu	%xmm0, 56(%r15,%r9,4)
	movdqu	%xmm0, 72(%r15,%r9,4)
	leal	16(%rdi,%rcx), %r9d
	movdqu	%xmm0, 56(%r15,%r9,4)
	movdqu	%xmm0, 72(%r15,%r9,4)
	leal	24(%rdi,%rcx), %r9d
	movdqu	%xmm0, 56(%r15,%r9,4)
	movdqu	%xmm0, 72(%r15,%r9,4)
	addl	$-32, %edx
	addl	$32, %ecx
	cmpl	%edx, %edi
	jne	.LBB0_171
.LBB0_172:                              # %middle.block
                                        #   in Loop: Header=BB0_121 Depth=3
	cmpl	%r14d, %ebp
	je	.LBB0_180
# BB#173:                               #   in Loop: Header=BB0_121 Depth=3
	addl	%r14d, %esi
	subl	%r14d, %ebp
.LBB0_174:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_121 Depth=3
	leal	-1(%rbp), %edi
	movl	%ebp, %ecx
	andl	$7, %ecx
	je	.LBB0_177
# BB#175:                               # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB0_121 Depth=3
	negl	%ecx
	.p2align	4, 0x90
.LBB0_176:                              # %scalar.ph.prol
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_121 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%esi, %edx
	incl	%esi
	movl	%eax, 56(%r15,%rdx,4)
	decl	%ebp
	incl	%ecx
	jne	.LBB0_176
.LBB0_177:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB0_121 Depth=3
	cmpl	$7, %edi
	jb	.LBB0_180
# BB#178:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB0_121 Depth=3
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_179:                              # %scalar.ph
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_121 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%rsi,%rdi), %ecx
	leal	1(%rsi,%rdi), %edx
	movl	%eax, 56(%r15,%rcx,4)
	leal	2(%rsi,%rdi), %ecx
	movl	%eax, 56(%r15,%rdx,4)
	leal	3(%rsi,%rdi), %edx
	movl	%eax, 56(%r15,%rcx,4)
	leal	4(%rsi,%rdi), %ecx
	movl	%eax, 56(%r15,%rdx,4)
	leal	5(%rsi,%rdi), %edx
	movl	%eax, 56(%r15,%rcx,4)
	leal	6(%rsi,%rdi), %ecx
	movl	%eax, 56(%r15,%rdx,4)
	leal	7(%rsi,%rdi), %edx
	movl	%eax, 56(%r15,%rcx,4)
	movl	%eax, 56(%r15,%rdx,4)
	addl	$8, %edi
	cmpl	%edi, %ebp
	jne	.LBB0_179
.LBB0_180:                              # %.loopexit2373
                                        #   in Loop: Header=BB0_121 Depth=3
	addl	%r10d, %r11d
	movl	%r11d, 52(%r15)
	movl	%r11d, %esi
	movq	%r8, %rbp
	movl	32(%rsp), %r8d          # 4-byte Reload
.LBB0_133:                              # %.backedge
                                        #   in Loop: Header=BB0_121 Depth=3
	cmpl	%r8d, %esi
	movq	120(%rsp), %r14         # 8-byte Reload
	jb	.LBB0_121
	jmp	.LBB0_134
.LBB0_119:                              #   in Loop: Header=BB0_6 Depth=2
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB0_134:                              # %._crit_edge1467
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	$0, (%rax)
	movl	$0, 92(%rsp)
	leal	257(%r12), %ebp
	movl	$9, 88(%rsp)
	movl	$6, 84(%rsp)
	subq	$8, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	movl	$257, %edx              # imm = 0x101
	movl	$cplens, %ecx
	movl	$cplext, %r8d
	movq	112(%rsp), %rdi         # 8-byte Reload
	movl	%ebp, %esi
	leaq	80(%rsp), %r9
	leaq	100(%rsp), %rax
	pushq	%rax
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)               # 8-byte Folded Reload
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	leaq	112(%rsp), %rax
	pushq	%rax
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	callq	huft_build
	addq	$32, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB0_139
# BB#135:                               # %._crit_edge1467
                                        #   in Loop: Header=BB0_6 Depth=2
	movl	88(%rsp), %r14d
	testl	%r14d, %r14d
	je	.LBB0_139
# BB#136:                               #   in Loop: Header=BB0_6 Depth=2
	movq	96(%rsp), %rsi          # 8-byte Reload
	incl	%esi
	movl	%ebp, %eax
	leaq	56(%r15,%rax,4), %rdi
	subq	$8, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	xorl	%edx, %edx
	movl	$cpdist, %ecx
	movl	$cpdext, %r8d
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	leaq	152(%rsp), %r9
	leaq	100(%rsp), %rax
	pushq	%rax
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)               # 8-byte Folded Reload
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	leaq	108(%rsp), %rax
	pushq	%rax
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	callq	huft_build
	addq	$32, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB0_139
# BB#137:                               #   in Loop: Header=BB0_6 Depth=2
	movl	84(%rsp), %eax
	testl	%r12d, %r12d
	je	.LBB0_182
# BB#138:                               #   in Loop: Header=BB0_6 Depth=2
	testl	%eax, %eax
	je	.LBB0_139
.LBB0_182:                              #   in Loop: Header=BB0_6 Depth=2
	movb	%r14b, 72(%r15)
	movb	%al, 73(%r15)
	movq	72(%rsp), %rax
	movq	%rax, 80(%r15)
	movq	144(%rsp), %rax
	movq	%rax, 88(%r15)
	movl	$0, 40(%r15)
.LBB0_184:                              # %.loopexit2093
                                        #   in Loop: Header=BB0_6 Depth=2
	movzbl	72(%r15), %eax
	movl	%eax, 64(%r15)
	movq	80(%r15), %rcx
	movq	%rcx, 56(%r15)
	movl	$1, 40(%r15)
.LBB0_185:                              #   in Loop: Header=BB0_6 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, %rdi
	cmpl	%eax, %ecx
	jae	.LBB0_186
# BB#187:                               # %.lr.ph1482.preheader
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_188:                              # %.lr.ph1482
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%esi, %esi
	je	.LBB0_189
# BB#194:                               #   in Loop: Header=BB0_188 Depth=3
	decl	%esi
	movzbl	(%rbp), %edx
	incq	%rbp
	movq	%rdi, %rcx
	shlq	%cl, %rdx
	orq	%rdx, 16(%rsp)          # 8-byte Folded Spill
	addl	$8, %ecx
	movq	%rcx, %rdi
	cmpl	%eax, %ecx
	jb	.LBB0_188
	jmp	.LBB0_195
.LBB0_292:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 1356(%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 8(%r15)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	cmpq	%rcx, %r13
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%r13d, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	addq	%rbx, %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r13
	jne	.LBB0_312
	.p2align	4, 0x90
.LBB0_293:                              # %.lr.ph1356
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_294
# BB#296:                               #   in Loop: Header=BB0_293 Depth=1
	movq	%rsi, (%r14)
	movq	%rsi, %rax
.LBB0_294:                              # %.backedge.i814.backedge
                                        #   in Loop: Header=BB0_293 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r13
	movq	%rbp, %rbx
	je	.LBB0_293
# BB#295:
	xorl	%eax, %eax
	jmp	.LBB0_312
.LBB0_300:
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 1360(%r15)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, 1356(%r15)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 8(%r15)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%r15)
	movq	%r13, 39912(%r15)
	movq	%rbp, 24(%r15)
	movl	$1, %eax
	cmpq	%rdx, %r13
	jne	.LBB0_312
# BB#301:                               # %.lr.ph1352.preheader
	movq	%rdx, %r13
	movq	48(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_302
	.p2align	4, 0x90
.LBB0_305:                              # %.backedge.i807.backedge._crit_edge
                                        #   in Loop: Header=BB0_302 Depth=1
	movq	(%r14), %rdx
.LBB0_302:                              # %.lr.ph1352
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r13, %rdx
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_303
# BB#306:                               #   in Loop: Header=BB0_302 Depth=1
	movq	%rsi, (%r14)
	movq	%rsi, %rdx
.LBB0_303:                              # %.backedge.i807.backedge
                                        #   in Loop: Header=BB0_302 Depth=1
	cmpq	%rdx, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r13
	movq	%rbx, %rbp
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r13
	je	.LBB0_305
# BB#304:
	movl	$1, %eax
.LBB0_312:                              # %.thread960.sink.split
	movq	%r13, 39904(%r15)
.LBB0_313:                              # %.thread960
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_307:                              # %.loopexit.loopexit
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB0_308:                              # %.loopexit
	movl	$17, 40(%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 1356(%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r13, %rbp
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r13
	addq	%rbx, %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	movl	$-2, %eax
	cmpq	39896(%r15), %r13
	jne	.LBB0_312
	.p2align	4, 0x90
.LBB0_309:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_310
# BB#324:                               #   in Loop: Header=BB0_309 Depth=1
	movq	%rsi, (%r14)
	movq	%rsi, %rax
.LBB0_310:                              # %.backedge.i800.backedge
                                        #   in Loop: Header=BB0_309 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r13
	movq	%rbp, %rbx
	je	.LBB0_309
# BB#311:
	movl	$-2, %eax
	jmp	.LBB0_312
.LBB0_189:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1360(%r15)
	movl	%edi, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbx
	movq	24(%r15), %rdi
	cmpq	%r13, %rbx
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%ebx, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%rbx, %r13
	addq	%rbp, %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r13
	jne	.LBB0_312
	.p2align	4, 0x90
.LBB0_190:                              # %.lr.ph1328
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_191
# BB#193:                               #   in Loop: Header=BB0_190 Depth=1
	movq	%rsi, (%r14)
	movq	%rsi, %rax
.LBB0_191:                              # %.backedge.i877.backedge
                                        #   in Loop: Header=BB0_190 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %r13
	movq	%rbx, %rbp
	je	.LBB0_190
# BB#192:
	xorl	%eax, %eax
	jmp	.LBB0_312
.LBB0_125:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1360(%r15)
	movl	%ecx, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbx
	movq	24(%r15), %rdi
	cmpq	%r13, %rbx
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rax
	movq	48(%rsp), %r12          # 8-byte Reload
	cmovbeq	%r12, %rax
	movl	(%rax), %ebp
	subl	%ebx, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%rbx, %r13
	addq	%rbp, %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r13
	jne	.LBB0_312
	.p2align	4, 0x90
.LBB0_126:                              # %.lr.ph1316
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_127
# BB#129:                               #   in Loop: Header=BB0_126 Depth=1
	movq	%rsi, (%r12)
	movq	%rsi, %rax
.LBB0_127:                              # %.backedge.i919.backedge
                                        #   in Loop: Header=BB0_126 Depth=1
	cmpq	%rax, %rsi
	movq	%r14, %rax
	cmovbeq	%r12, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %r13
	movq	%rbx, %rbp
	je	.LBB0_126
# BB#128:
	xorl	%eax, %eax
	jmp	.LBB0_312
.LBB0_110:
	movq	%rdi, 1360(%r15)
	movl	%ecx, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%r8, (%r15)
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbx
	movq	24(%r15), %rdi
	cmpq	%r13, %rbx
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%ebx, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%rbx, %r13
	addq	%rbp, %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r13
	jne	.LBB0_312
.LBB0_111:                              # %.lr.ph1312
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_112
# BB#114:                               #   in Loop: Header=BB0_111 Depth=1
	movq	%rsi, (%r14)
	movq	%rsi, %rax
.LBB0_112:                              # %.backedge.i933.backedge
                                        #   in Loop: Header=BB0_111 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %r13
	movq	%rbx, %rbp
	je	.LBB0_111
# BB#113:
	xorl	%eax, %eax
	jmp	.LBB0_312
.LBB0_218:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbx
	movq	24(%r15), %rdi
	cmpq	%r13, %rbx
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%ebx, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%rbx, %r13
	addq	%rbp, %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r13
	jne	.LBB0_312
.LBB0_219:                              # %.lr.ph1336
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_220
# BB#222:                               #   in Loop: Header=BB0_219 Depth=1
	movq	%rsi, (%r14)
	movq	%rsi, %rax
.LBB0_220:                              # %.backedge.i863.backedge
                                        #   in Loop: Header=BB0_219 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %r13
	movq	%rbx, %rbp
	je	.LBB0_219
# BB#221:
	xorl	%eax, %eax
	jmp	.LBB0_312
.LBB0_146:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%r8, (%r15)
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbx
	movq	24(%r15), %rdi
	cmpq	%r13, %rbx
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rax
	movq	48(%rsp), %r12          # 8-byte Reload
	cmovbeq	%r12, %rax
	movl	(%rax), %ebp
	subl	%ebx, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%rbx, %r13
	addq	%rbp, %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r13
	jne	.LBB0_312
.LBB0_147:                              # %.lr.ph1320
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_148
# BB#150:                               #   in Loop: Header=BB0_147 Depth=1
	movq	%rsi, (%r12)
	movq	%rsi, %rax
.LBB0_148:                              # %.backedge.i912.backedge
                                        #   in Loop: Header=BB0_147 Depth=1
	cmpq	%rax, %rsi
	movq	%r14, %rax
	cmovbeq	%r12, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %r13
	movq	%rbx, %rbp
	je	.LBB0_147
# BB#149:
	xorl	%eax, %eax
	jmp	.LBB0_312
.LBB0_259:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 1356(%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r13, 39912(%r15)
	cmpq	%r13, %rsi
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rax
	movq	48(%rsp), %rbp          # 8-byte Reload
	cmovbeq	%rbp, %rax
	movl	(%rax), %r13d
	subl	%esi, %r13d
	movl	32(%r15), %eax
	cmpl	%eax, %r13d
	cmovael	%eax, %r13d
	subl	%r13d, %eax
	movl	%eax, 32(%r15)
	movq	%r12, %rdi
	movq	%rsi, %rbx
	movq	%r13, %rdx
	callq	memcpy
	movq	%rbx, %rcx
	addq	%r13, %rcx
	addq	24(%r15), %r13
	movq	%r13, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %rcx
	je	.LBB0_261
# BB#260:
	movq	%rcx, %r13
	jmp	.LBB0_312
.LBB0_261:                              # %.lr.ph1344
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	cmpq	%rcx, %rax
	jne	.LBB0_262
# BB#263:                               #   in Loop: Header=BB0_261 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbp)
.LBB0_262:                              # %.backedge.i842.backedge
                                        #   in Loop: Header=BB0_261 Depth=1
	movq	40(%rsp), %rsi          # 8-byte Reload
	cmpq	%rax, %rsi
	movq	%r14, %rax
	cmovbeq	%rbp, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %rcx
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %rcx
	movq	%rbx, %r13
	je	.LBB0_261
.LBB0_282:
	movq	%rcx, %r13
	xorl	%eax, %eax
	jmp	.LBB0_312
.LBB0_207:
	movq	%rbp, 1360(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rsi, (%r15)
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbx
	movq	24(%r15), %rdi
	cmpq	%r13, %rbx
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%ebx, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%rbx, %r13
	addq	%rbp, %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r13
	jne	.LBB0_312
.LBB0_208:                              # %.lr.ph1332
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_209
# BB#211:                               #   in Loop: Header=BB0_208 Depth=1
	movq	%rsi, (%r14)
	movq	%rsi, %rax
.LBB0_209:                              # %.backedge.i870.backedge
                                        #   in Loop: Header=BB0_208 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %r13
	movq	%rbx, %rbp
	je	.LBB0_208
# BB#210:
	xorl	%eax, %eax
	jmp	.LBB0_312
.LBB0_231:
	movq	%rbp, 1360(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rsi, (%r15)
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbx
	movq	24(%r15), %rdi
	cmpq	%r13, %rbx
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%ebx, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%rbx, %r13
	addq	%rbp, %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r13
	jne	.LBB0_312
.LBB0_232:                              # %.lr.ph1340
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_233
# BB#235:                               #   in Loop: Header=BB0_232 Depth=1
	movq	%rsi, (%r14)
	movq	%rsi, %rax
.LBB0_233:                              # %.backedge.i856.backedge
                                        #   in Loop: Header=BB0_232 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %r13
	movq	%rbx, %rbp
	je	.LBB0_232
# BB#234:
	xorl	%eax, %eax
	jmp	.LBB0_312
.LBB0_28:
	movq	48(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_308
.LBB0_139:                              # %.thread961
	movl	$17, 40(%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 1356(%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 8(%r15)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r13, %rbp
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rax
	movq	48(%rsp), %rbx          # 8-byte Reload
	cmovbeq	%rbx, %rax
	movl	(%rax), %r12d
	subl	%ebp, %r12d
	movl	32(%r15), %eax
	cmpl	%eax, %r12d
	cmovael	%eax, %r12d
	subl	%r12d, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	memcpy
	addq	%r12, %rbp
	addq	24(%r15), %r12
	movq	%r12, 24(%r15)
	cmpq	39896(%r15), %rbp
	jne	.LBB0_143
# BB#140:                               # %.lr.ph1475.preheader
	movq	%rbx, %r13
.LBB0_141:                              # %.lr.ph1475
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%rbp, %rax
	jne	.LBB0_142
# BB#181:                               #   in Loop: Header=BB0_141 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
.LBB0_142:                              # %.backedge.i884.backedge
                                        #   in Loop: Header=BB0_141 Depth=1
	movq	40(%rsp), %rsi          # 8-byte Reload
	cmpq	%rax, %rsi
	movq	%r14, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %rbp
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %rbp
	movq	%rbx, %r12
	je	.LBB0_141
.LBB0_143:                              # %inflate_flush.exit885
	movq	%rbp, 39904(%r15)
	movl	$-3, %eax
	jmp	.LBB0_313
.LBB0_155:
	movl	$17, 40(%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 1356(%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 8(%r15)
	movq	%r8, (%r15)
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbx
	movq	24(%r15), %rdi
	cmpq	%r13, %rbx
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rax
	movq	48(%rsp), %r12          # 8-byte Reload
	cmovbeq	%r12, %rax
	movl	(%rax), %ebp
	subl	%ebx, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%rbx, %r13
	addq	%rbp, %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	movl	$-3, %eax
	cmpq	39896(%r15), %r13
	jne	.LBB0_312
.LBB0_156:                              # %.lr.ph1324
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_157
# BB#159:                               #   in Loop: Header=BB0_156 Depth=1
	movq	%rsi, (%r12)
	movq	%rsi, %rax
.LBB0_157:                              # %.backedge.i905.backedge
                                        #   in Loop: Header=BB0_156 Depth=1
	cmpq	%rax, %rsi
	movq	%r14, %rax
	cmovbeq	%r12, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %r13
	movq	%rbx, %rbp
	je	.LBB0_156
# BB#158:
	movl	$-3, %eax
	jmp	.LBB0_312
.LBB0_53:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbx
	movq	24(%r15), %rdi
	cmpq	%r13, %rbx
	movq	%r12, %rax
	movq	48(%rsp), %r14          # 8-byte Reload
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%ebx, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%rbx, %r13
	addq	%rbp, %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r13
	jne	.LBB0_312
.LBB0_54:                               # %.lr.ph1296
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_55
# BB#57:                                #   in Loop: Header=BB0_54 Depth=1
	movq	%rsi, (%r14)
	movq	%rsi, %rax
.LBB0_55:                               # %.backedge.i898.backedge
                                        #   in Loop: Header=BB0_54 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %r13
	movq	%rbx, %rbp
	je	.LBB0_54
# BB#56:
	xorl	%eax, %eax
	jmp	.LBB0_312
.LBB0_95:
	movl	$17, 40(%r15)
	movq	%rbp, 1360(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 1356(%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 8(%r15)
	movq	%r8, (%r15)
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbx
	movq	24(%r15), %rdi
	cmpq	%r13, %rbx
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%ebx, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%rbx, %r13
	addq	%rbp, %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	movl	$-3, %eax
	cmpq	39896(%r15), %r13
	jne	.LBB0_312
.LBB0_96:                               # %.lr.ph1308
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_97
# BB#99:                                #   in Loop: Header=BB0_96 Depth=1
	movq	%rsi, (%r14)
	movq	%rsi, %rax
.LBB0_97:                               # %.backedge.i940.backedge
                                        #   in Loop: Header=BB0_96 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %r13
	movq	%rbx, %rbp
	je	.LBB0_96
# BB#98:
	movl	$-3, %eax
	jmp	.LBB0_312
.LBB0_202:
	movq	48(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_308
.LBB0_18:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbx
	movq	24(%r15), %rdi
	cmpq	%r13, %rbx
	movq	%r12, %rax
	movq	48(%rsp), %r14          # 8-byte Reload
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%ebx, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%rbx, %r13
	addq	%rbp, %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r13
	jne	.LBB0_312
.LBB0_19:                               # %.lr.ph1288
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_20
# BB#22:                                #   in Loop: Header=BB0_19 Depth=1
	movq	%rsi, (%r14)
	movq	%rsi, %rax
.LBB0_20:                               # %.backedge.i.backedge
                                        #   in Loop: Header=BB0_19 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %r13
	movq	%rbx, %rbp
	je	.LBB0_19
# BB#21:
	xorl	%eax, %eax
	jmp	.LBB0_312
.LBB0_84:
	movq	48(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_85
.LBB0_277:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 1356(%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 8(%r15)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	movq	%r13, 39912(%r15)
	cmpq	%r13, %rdx
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%edx, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rdx, %rbp
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %rcx
	addq	%rbx, %rcx
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %rcx
	je	.LBB0_279
# BB#278:
	movq	%rcx, %r13
	jmp	.LBB0_312
.LBB0_75:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1360(%r15)
	movl	%edx, 1356(%r15)
	movl	%esi, 8(%r15)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	movq	%r13, 39912(%r15)
	cmpq	%r13, %r12
	movq	56(%rsp), %rax          # 8-byte Reload
	cmovbeq	48(%rsp), %rax          # 8-byte Folded Reload
	movl	(%rax), %r13d
	subl	%r12d, %r13d
	movl	32(%r15), %eax
	cmpl	%eax, %r13d
	cmovael	%eax, %r13d
	subl	%r13d, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	memcpy
	addq	%r13, %r12
	addq	24(%r15), %r13
	movq	%r13, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r12
	je	.LBB0_77
# BB#76:
	movq	%r12, %r13
	jmp	.LBB0_312
.LBB0_318:
	movq	%rax, %rbp
.LBB0_41:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 1360(%r15)
	movl	%ecx, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbx
	movq	24(%r15), %rdi
	cmpq	%r13, %rbx
	movq	%r12, %rax
	movq	48(%rsp), %r14          # 8-byte Reload
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%ebx, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%rbx, %r13
	addq	%rbp, %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r13
	jne	.LBB0_312
.LBB0_42:                               # %.lr.ph1292
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_43
# BB#45:                                #   in Loop: Header=BB0_42 Depth=1
	movq	%rsi, (%r14)
	movq	%rsi, %rax
.LBB0_43:                               # %.backedge.i891.backedge
                                        #   in Loop: Header=BB0_42 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %r13
	movq	%rbx, %rbp
	je	.LBB0_42
# BB#44:
	xorl	%eax, %eax
	jmp	.LBB0_312
.LBB0_315:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%r8, %rbp
.LBB0_85:
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 1360(%r15)
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r13, 39912(%r15)
	movq	39904(%r15), %rbx
	movq	24(%r15), %rdi
	cmpq	%r13, %rbx
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%ebx, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%rbx, %r13
	addq	%rbp, %r13
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r13
	jne	.LBB0_312
.LBB0_86:                               # %.lr.ph1304
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	%r13, %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_87
# BB#89:                                #   in Loop: Header=BB0_86 Depth=1
	movq	%rsi, (%r14)
	movq	%rsi, %rax
.LBB0_87:                               # %.backedge.i947.backedge
                                        #   in Loop: Header=BB0_86 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r13
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %r13
	movq	%rbx, %rbp
	je	.LBB0_86
# BB#88:
	xorl	%eax, %eax
	jmp	.LBB0_312
.LBB0_279:                              # %.lr.ph1348.preheader
	movq	40(%rsp), %r13          # 8-byte Reload
.LBB0_280:                              # %.lr.ph1348
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	%rcx, %rax
	jne	.LBB0_281
# BB#283:                               #   in Loop: Header=BB0_280 Depth=1
	movq	%r13, (%r14)
	movq	%r13, %rax
.LBB0_281:                              # %.backedge.i828.backedge
                                        #   in Loop: Header=BB0_280 Depth=1
	cmpq	%rax, %r13
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%r13d, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %rcx
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %rcx
	movq	%rbp, %rbx
	je	.LBB0_280
	jmp	.LBB0_282
.LBB0_77:                               # %.lr.ph1300.preheader
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB0_78:                               # %.lr.ph1300
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	cmpq	%r12, %rax
	jne	.LBB0_79
# BB#81:                                #   in Loop: Header=BB0_78 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbp)
.LBB0_79:                               # %.backedge.i954.backedge
                                        #   in Loop: Header=BB0_78 Depth=1
	movq	40(%rsp), %rsi          # 8-byte Reload
	cmpq	%rax, %rsi
	movq	56(%rsp), %rax          # 8-byte Reload
	cmovbeq	%rbp, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r12
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %r12
	movq	%rbx, %r13
	je	.LBB0_78
# BB#80:
	movq	%r12, %r13
	xorl	%eax, %eax
	jmp	.LBB0_312
.Lfunc_end0:
	.size	nsis_inflate, .Lfunc_end0-nsis_inflate
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_183
	.quad	.LBB0_7
	.quad	.LBB0_203
	.quad	.LBB0_9
	.quad	.LBB0_227
	.quad	.LBB0_10
	.quad	.LBB0_264
	.quad	.LBB0_285
	.quad	.LBB0_15
	.quad	.LBB0_13
	.quad	.LBB0_52
	.quad	.LBB0_11
	.quad	.LBB0_103
	.quad	.LBB0_8
	.quad	.LBB0_307
	.quad	.LBB0_287

	.text
	.p2align	4, 0x90
	.type	huft_build,@function
huft_build:                             # @huft_build
	.cfi_startproc
# BB#0:                                 # %.preheader229.preheader
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 272
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%r9, -8(%rsp)           # 8-byte Spill
	movq	%rcx, %r15
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -64(%rsp)
	movaps	%xmm0, -80(%rsp)
	movaps	%xmm0, -96(%rsp)
	movaps	%xmm0, -112(%rsp)
	leal	-1(%rsi), %r9d
	movl	%esi, %ebp
	andl	$3, %ebp
	movl	%esi, %eax
	movq	%rdi, %rcx
	je	.LBB1_3
# BB#1:                                 # %.preheader229.prol.preheader
	negl	%ebp
	movl	%esi, %eax
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader229.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %ebx
	addq	$4, %rcx
	incl	-112(%rsp,%rbx,4)
	decl	%eax
	incl	%ebp
	jne	.LBB1_2
.LBB1_3:                                # %.preheader229.prol.loopexit
	cmpl	$3, %r9d
	jb	.LBB1_5
	.p2align	4, 0x90
.LBB1_4:                                # %.preheader229
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %ebp
	incl	-112(%rsp,%rbp,4)
	movl	4(%rcx), %ebp
	incl	-112(%rsp,%rbp,4)
	movl	8(%rcx), %ebp
	incl	-112(%rsp,%rbp,4)
	movl	12(%rcx), %ebp
	incl	-112(%rsp,%rbp,4)
	addq	$16, %rcx
	addl	$-4, %eax
	jne	.LBB1_4
.LBB1_5:
	movq	272(%rsp), %rax
	cmpl	%esi, -112(%rsp)
	jne	.LBB1_7
# BB#6:
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movq	$0, (%rcx)
	movl	$0, (%rax)
	xorl	%eax, %eax
	jmp	.LBB1_92
.LBB1_7:
	movl	(%rax), %ebx
	movl	-108(%rsp), %ebp
	movl	$1, %ecx
	testl	%ebp, %ebp
	jne	.LBB1_22
# BB#8:
	movl	$2, %ecx
	cmpl	$0, -104(%rsp)
	jne	.LBB1_22
# BB#9:
	movl	$3, %ecx
	cmpl	$0, -100(%rsp)
	jne	.LBB1_22
# BB#10:
	movl	$4, %ecx
	cmpl	$0, -96(%rsp)
	jne	.LBB1_22
# BB#11:
	movl	$5, %ecx
	cmpl	$0, -92(%rsp)
	jne	.LBB1_22
# BB#12:
	movl	$6, %ecx
	cmpl	$0, -88(%rsp)
	jne	.LBB1_22
# BB#13:
	movl	$7, %ecx
	cmpl	$0, -84(%rsp)
	jne	.LBB1_22
# BB#14:
	movl	$8, %ecx
	cmpl	$0, -80(%rsp)
	jne	.LBB1_22
# BB#15:
	movl	$9, %ecx
	cmpl	$0, -76(%rsp)
	jne	.LBB1_22
# BB#16:
	movl	$10, %ecx
	cmpl	$0, -72(%rsp)
	jne	.LBB1_22
# BB#17:
	movl	$11, %ecx
	cmpl	$0, -68(%rsp)
	jne	.LBB1_22
# BB#18:
	movl	$12, %ecx
	cmpl	$0, -64(%rsp)
	jne	.LBB1_22
# BB#19:
	movl	$13, %ecx
	cmpl	$0, -60(%rsp)
	jne	.LBB1_22
# BB#20:
	movl	$14, %ecx
	cmpl	$0, -56(%rsp)
	jne	.LBB1_22
# BB#21:
	cmpl	$1, -52(%rsp)
	movl	$15, %ecx
	adcl	$0, %ecx
.LBB1_22:
	cmpl	%ecx, %ebx
	cmovbl	%ecx, %ebx
	movl	$15, %r14d
	cmpl	$0, -52(%rsp)
	jne	.LBB1_36
# BB#23:
	movl	$14, %r14d
	cmpl	$0, -56(%rsp)
	jne	.LBB1_36
# BB#24:
	movl	$13, %r14d
	cmpl	$0, -60(%rsp)
	jne	.LBB1_36
# BB#25:
	movl	$12, %r14d
	cmpl	$0, -64(%rsp)
	jne	.LBB1_36
# BB#26:
	movl	$11, %r14d
	cmpl	$0, -68(%rsp)
	jne	.LBB1_36
# BB#27:
	movl	$10, %r14d
	cmpl	$0, -72(%rsp)
	jne	.LBB1_36
# BB#28:
	movl	$9, %r14d
	cmpl	$0, -76(%rsp)
	jne	.LBB1_36
# BB#29:
	movl	$8, %r14d
	cmpl	$0, -80(%rsp)
	jne	.LBB1_36
# BB#30:
	movl	$7, %r14d
	cmpl	$0, -84(%rsp)
	jne	.LBB1_36
# BB#31:
	movl	$6, %r14d
	cmpl	$0, -88(%rsp)
	jne	.LBB1_36
# BB#32:
	movl	$5, %r14d
	cmpl	$0, -92(%rsp)
	jne	.LBB1_36
# BB#33:
	movl	$4, %r14d
	cmpl	$0, -96(%rsp)
	jne	.LBB1_36
# BB#34:
	movl	$3, %r14d
	cmpl	$0, -100(%rsp)
	jne	.LBB1_36
# BB#35:
	xorl	%r9d, %r9d
	testl	%ebp, %ebp
	setne	%r9b
	cmpl	$0, -104(%rsp)
	movl	$2, %r14d
	cmovel	%r9d, %r14d
.LBB1_36:
	cmpl	%r14d, %ebx
	movl	%ebx, %r11d
	cmoval	%r14d, %r11d
	movl	%r11d, (%rax)
	movl	$1, %eax
	shll	%cl, %eax
	cmpl	%r14d, %ecx
	jae	.LBB1_40
# BB#37:                                # %.lr.ph327.preheader
	movl	%ecx, %ebp
	.p2align	4, 0x90
.LBB1_38:                               # %.lr.ph327
                                        # =>This Inner Loop Header: Depth=1
	subl	-112(%rsp,%rbp,4), %eax
	js	.LBB1_47
# BB#39:                                #   in Loop: Header=BB1_38 Depth=1
	addl	%eax, %eax
	incq	%rbp
	cmpl	%r14d, %ebp
	jb	.LBB1_38
.LBB1_40:                               # %._crit_edge328
	movl	%r14d, %r13d
	movl	%eax, %ebp
	subl	-112(%rsp,%r13,4), %ebp
	js	.LBB1_47
# BB#41:
	movl	%ebp, -36(%rsp)         # 4-byte Spill
	movq	%rbx, %r12
	movl	%eax, -112(%rsp,%r13,4)
	movl	$0, 36(%rsp)
	movl	%r14d, %r9d
	decl	%r9d
	je	.LBB1_50
# BB#42:                                # %.lr.ph323.preheader
	leal	-2(%r14), %r10d
	movl	%r9d, %eax
	andl	$3, %eax
	je	.LBB1_48
# BB#43:                                # %.lr.ph323.prol.preheader
	negl	%eax
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_44:                               # %.lr.ph323.prol
                                        # =>This Inner Loop Header: Depth=1
	addl	-108(%rsp,%rbx), %ebp
	movl	%ebp, 40(%rsp,%rbx)
	decl	%r9d
	addq	$4, %rbx
	incl	%eax
	jne	.LBB1_44
# BB#45:                                # %.lr.ph323.prol.loopexit.unr-lcssa
	leaq	40(%rsp,%rbx), %rax
	leaq	-108(%rsp,%rbx), %rbx
	cmpl	$3, %r10d
	jae	.LBB1_49
	jmp	.LBB1_50
.LBB1_47:
	movl	$-3, %eax
	jmp	.LBB1_92
.LBB1_48:
	leaq	-108(%rsp), %rbx
	leaq	40(%rsp), %rax
	xorl	%ebp, %ebp
	cmpl	$3, %r10d
	jb	.LBB1_50
	.p2align	4, 0x90
.LBB1_49:                               # %.lr.ph323
                                        # =>This Inner Loop Header: Depth=1
	addl	(%rbx), %ebp
	movl	%ebp, (%rax)
	addl	4(%rbx), %ebp
	movl	%ebp, 4(%rax)
	addl	8(%rbx), %ebp
	movl	%ebp, 8(%rax)
	addl	12(%rbx), %ebp
	movl	%ebp, 12(%rax)
	addq	$16, %rbx
	addq	$16, %rax
	addl	$-4, %r9d
	jne	.LBB1_49
.LBB1_50:                               # %.preheader227.preheader
	xorl	%eax, %eax
	movq	%r12, %r9
	.p2align	4, 0x90
.LBB1_51:                               # %.preheader227
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax,4), %ebp
	testq	%rbp, %rbp
	je	.LBB1_53
# BB#52:                                #   in Loop: Header=BB1_51 Depth=1
	movl	32(%rsp,%rbp,4), %r9d
	leal	1(%r9), %ebx
	movl	%ebx, 32(%rsp,%rbp,4)
	movl	%eax, huft_build.v(,%r9,4)
	movq	%r12, %r9
.LBB1_53:                               #   in Loop: Header=BB1_51 Depth=1
	incq	%rax
	cmpl	%esi, %eax
	jb	.LBB1_51
# BB#54:
	movl	%edx, -32(%rsp)         # 4-byte Spill
	movq	%r15, (%rsp)            # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movl	32(%rsp,%r13,4), %eax
	movl	$0, 32(%rsp)
	movq	$0, 96(%rsp)
	cmpl	%r14d, %ecx
	movq	288(%rsp), %rdx
	jbe	.LBB1_56
.LBB1_55:                               # %._crit_edge318
	movl	-36(%rsp), %eax         # 4-byte Reload
	testl	%eax, %eax
	movl	$-5, %ecx
	cmovel	%eax, %ecx
	xorl	%eax, %eax
	cmpl	$1, %r14d
	cmovnel	%ecx, %eax
	jmp	.LBB1_92
.LBB1_56:                               # %.lr.ph317
	movl	%r11d, %esi
	negl	%esi
	leaq	huft_build.v(,%rax,4), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$huft_build.v, %eax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movl	%ecx, %r8d
	leaq	-108(%rsp,%r8,4), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movl	%r14d, %eax
	notl	%eax
	notl	%r9d
	cmpl	%r9d, %eax
	cmoval	%eax, %r9d
	movl	$-1, %r13d
	xorl	%r10d, %r10d
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	movq	%r9, -24(%rsp)          # 8-byte Spill
.LBB1_57:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_59 Depth 2
                                        #       Child Loop BB1_60 Depth 3
                                        #         Child Loop BB1_61 Depth 4
                                        #           Child Loop BB1_65 Depth 5
                                        #       Child Loop BB1_81 Depth 3
                                        #       Child Loop BB1_83 Depth 3
                                        #       Child Loop BB1_87 Depth 3
	movl	-112(%rsp,%r8,4), %eax
	movl	%eax, -116(%rsp)        # 4-byte Spill
	testl	%eax, %eax
	je	.LBB1_89
# BB#58:                                # %.preheader226.lr.ph
                                        #   in Loop: Header=BB1_57 Depth=1
	leal	-1(%r8), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movl	%eax, -28(%rsp)         # 4-byte Spill
.LBB1_59:                               # %.preheader226
                                        #   Parent Loop BB1_57 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_60 Depth 3
                                        #         Child Loop BB1_61 Depth 4
                                        #           Child Loop BB1_65 Depth 5
                                        #       Child Loop BB1_81 Depth 3
                                        #       Child Loop BB1_83 Depth 3
                                        #       Child Loop BB1_87 Depth 3
	leal	(%rsi,%r11), %eax
	movslq	%eax, %rcx
	cmpq	%rcx, %r8
	jle	.LBB1_73
.LBB1_60:                               # %.lr.ph264
                                        #   Parent Loop BB1_57 Depth=1
                                        #     Parent Loop BB1_59 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_61 Depth 4
                                        #           Child Loop BB1_65 Depth 5
	movl	%esi, %edi
	movl	%r13d, %r9d
	.p2align	4, 0x90
.LBB1_61:                               #   Parent Loop BB1_57 Depth=1
                                        #     Parent Loop BB1_59 Depth=2
                                        #       Parent Loop BB1_60 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB1_65 Depth 5
	movl	%eax, %esi
	movl	%r14d, %ebp
	subl	%esi, %ebp
	cmpl	%r11d, %ebp
	cmoval	%r11d, %ebp
	movl	%r8d, %eax
	subl	%esi, %eax
	movl	$1, %ebx
	movl	%eax, %ecx
	shll	%cl, %ebx
	subl	-116(%rsp), %ebx        # 4-byte Folded Reload
	jbe	.LBB1_67
# BB#62:                                #   in Loop: Header=BB1_61 Depth=4
	cmpl	%ebp, %eax
	jae	.LBB1_67
# BB#63:                                # %.preheader
                                        #   in Loop: Header=BB1_61 Depth=4
	incl	%eax
	cmpl	%ebp, %eax
	jae	.LBB1_67
# BB#64:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_61 Depth=4
	movq	-16(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_65:                               # %.lr.ph
                                        #   Parent Loop BB1_57 Depth=1
                                        #     Parent Loop BB1_59 Depth=2
                                        #       Parent Loop BB1_60 Depth=3
                                        #         Parent Loop BB1_61 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	addl	%ebx, %ebx
	subl	(%rcx), %ebx
	jbe	.LBB1_67
# BB#66:                                #   in Loop: Header=BB1_65 Depth=5
	incl	%eax
	addq	$4, %rcx
	cmpl	%ebp, %eax
	jb	.LBB1_65
	.p2align	4, 0x90
.LBB1_67:                               # %.critedge
                                        #   in Loop: Header=BB1_61 Depth=4
	movl	$1, %r10d
	movl	%eax, %ecx
	shll	%cl, %r10d
	movl	(%rdx), %ecx
	movq	%rdx, %rbx
	leal	(%rcx,%r10), %edx
	cmpl	$1440, %edx             # imm = 0x5A0
	ja	.LBB1_91
# BB#68:                                #   in Loop: Header=BB1_61 Depth=4
	leal	1(%r9), %r13d
	movq	280(%rsp), %rbp
	leaq	(%rbp,%rcx,4), %rbp
	movslq	%r13d, %rcx
	movq	%rbp, 96(%rsp,%rcx,8)
	movl	%edx, (%rbx)
	testl	%ecx, %ecx
	movq	%rbx, %rdx
	jne	.LBB1_70
# BB#69:                                #   in Loop: Header=BB1_61 Depth=4
	movq	-8(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
	leal	(%rsi,%r11), %eax
	movslq	%eax, %rcx
	xorl	%r9d, %r9d
	cmpq	%rcx, %r8
	movl	%esi, %edi
	jg	.LBB1_61
	jmp	.LBB1_71
.LBB1_70:                               # %.outer
                                        #   in Loop: Header=BB1_60 Depth=3
	movl	%r12d, 32(%rsp,%rcx,4)
	movl	%r12d, %edx
	movl	%edi, %ecx
	shrl	%cl, %edx
	movslq	%r9d, %rcx
	movq	96(%rsp,%rcx,8), %rcx
	movl	%ebp, %r15d
	subl	%ecx, %r15d
	shrl	$2, %r15d
	subl	%edx, %r15d
	movb	%al, (%rcx,%rdx,4)
	movb	%r11b, 1(%rcx,%rdx,4)
	movw	%r15w, 2(%rcx,%rdx,4)
	movq	288(%rsp), %rdx
	leal	(%rsi,%r11), %eax
	movslq	%eax, %rcx
	cmpq	%rcx, %r8
	jg	.LBB1_60
	jmp	.LBB1_72
.LBB1_71:                               #   in Loop: Header=BB1_59 Depth=2
	xorl	%r13d, %r13d
.LBB1_72:                               # %.outer._crit_edge
                                        #   in Loop: Header=BB1_59 Depth=2
	movq	-24(%rsp), %r9          # 8-byte Reload
.LBB1_73:                               # %.outer._crit_edge
                                        #   in Loop: Header=BB1_59 Depth=2
	movl	%r8d, %eax
	subl	%esi, %eax
	movb	$-64, %dil
	movq	-48(%rsp), %rdx         # 8-byte Reload
	cmpq	24(%rsp), %rdx          # 8-byte Folded Reload
	jae	.LBB1_80
# BB#74:                                #   in Loop: Header=BB1_59 Depth=2
	movl	(%rdx), %r15d
	movl	%r15d, %ecx
	subl	-32(%rsp), %ecx         # 4-byte Folded Reload
	jae	.LBB1_77
# BB#75:                                #   in Loop: Header=BB1_59 Depth=2
	cmpl	$256, %r15d             # imm = 0x100
	jb	.LBB1_78
# BB#76:                                #   in Loop: Header=BB1_59 Depth=2
	movb	$96, %dil
	jmp	.LBB1_79
.LBB1_77:                               #   in Loop: Header=BB1_59 Depth=2
	movq	8(%rsp), %rdx           # 8-byte Reload
	movb	(%rdx,%rcx,2), %dil
	addb	$80, %dil
	movq	(%rsp), %rdx            # 8-byte Reload
	movw	(%rdx,%rcx,2), %r15w
	movq	-48(%rsp), %rdx         # 8-byte Reload
	jmp	.LBB1_79
.LBB1_78:                               #   in Loop: Header=BB1_59 Depth=2
	xorl	%edi, %edi
.LBB1_79:                               #   in Loop: Header=BB1_59 Depth=2
	addq	$4, %rdx
	movq	%rdx, -48(%rsp)         # 8-byte Spill
.LBB1_80:                               #   in Loop: Header=BB1_59 Depth=2
	movl	$1, %ebx
	movl	%eax, %ecx
	shll	%cl, %ebx
	movl	%r12d, %edx
	movl	%esi, %ecx
	shrl	%cl, %edx
	cmpl	%r10d, %edx
	jae	.LBB1_82
	.p2align	4, 0x90
.LBB1_81:                               # %.lr.ph285
                                        #   Parent Loop BB1_57 Depth=1
                                        #     Parent Loop BB1_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edx, %ecx
	movb	%dil, (%rbp,%rcx,4)
	movb	%al, 1(%rbp,%rcx,4)
	movw	%r15w, 2(%rbp,%rcx,4)
	addl	%ebx, %edx
	cmpl	%r10d, %edx
	jb	.LBB1_81
.LBB1_82:                               # %._crit_edge286.preheader
                                        #   in Loop: Header=BB1_59 Depth=2
	decl	-116(%rsp)              # 4-byte Folded Spill
	movl	-28(%rsp), %eax         # 4-byte Reload
	.p2align	4, 0x90
.LBB1_83:                               # %._crit_edge286
                                        #   Parent Loop BB1_57 Depth=1
                                        #     Parent Loop BB1_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r12d, %ecx
	movl	%eax, %r12d
	xorl	%ecx, %r12d
	movl	%eax, %edx
	shrl	%edx
	testl	%ecx, %eax
	movl	%edx, %eax
	jne	.LBB1_83
# BB#84:                                # %.preheader225
                                        #   in Loop: Header=BB1_59 Depth=2
	movl	$1, %edx
	movl	%esi, %ecx
	shll	%cl, %edx
	decl	%edx
	andl	%r12d, %edx
	movslq	%r13d, %rax
	cmpl	32(%rsp,%rax,4), %edx
	jne	.LBB1_86
# BB#85:                                #   in Loop: Header=BB1_59 Depth=2
	movl	%esi, %eax
	jmp	.LBB1_88
.LBB1_86:                               # %.lr.ph289.preheader
                                        #   in Loop: Header=BB1_59 Depth=2
	leaq	28(%rsp), %rcx
	leaq	(%rcx,%rax,4), %rdi
	.p2align	4, 0x90
.LBB1_87:                               # %.lr.ph289
                                        #   Parent Loop BB1_57 Depth=1
                                        #     Parent Loop BB1_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%esi, %eax
	subl	%r11d, %eax
	leal	1(%r9,%rsi), %ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	decl	%edx
	andl	%r12d, %edx
	decl	%r13d
	cmpl	(%rdi), %edx
	leaq	-4(%rdi), %rdi
	movl	%eax, %esi
	jne	.LBB1_87
.LBB1_88:                               # %.thread.loopexit
                                        #   in Loop: Header=BB1_59 Depth=2
	cmpl	$0, -116(%rsp)          # 4-byte Folded Reload
	movl	%eax, %esi
	movq	288(%rsp), %rdx
	jne	.LBB1_59
	jmp	.LBB1_90
.LBB1_89:                               #   in Loop: Header=BB1_57 Depth=1
	movl	%esi, %eax
.LBB1_90:                               # %.thread._crit_edge
                                        #   in Loop: Header=BB1_57 Depth=1
	addq	$4, -16(%rsp)           # 8-byte Folded Spill
	cmpq	16(%rsp), %r8           # 8-byte Folded Reload
	leaq	1(%r8), %r8
	movl	%eax, %esi
	jl	.LBB1_57
	jmp	.LBB1_55
.LBB1_91:
	movl	$-4, %eax
.LBB1_92:                               # %.loopexit
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	huft_build, .Lfunc_end1-huft_build
	.cfi_endproc

	.type	fixed_built,@object     # @fixed_built
	.local	fixed_built
	.comm	fixed_built,1,1
	.type	nsis_inflate.lc,@object # @nsis_inflate.lc
	.local	nsis_inflate.lc
	.comm	nsis_inflate.lc,1152,16
	.type	cplens,@object          # @cplens
	.section	.rodata,"a",@progbits
	.p2align	4
cplens:
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	13                      # 0xd
	.short	15                      # 0xf
	.short	17                      # 0x11
	.short	19                      # 0x13
	.short	23                      # 0x17
	.short	27                      # 0x1b
	.short	31                      # 0x1f
	.short	35                      # 0x23
	.short	43                      # 0x2b
	.short	51                      # 0x33
	.short	59                      # 0x3b
	.short	67                      # 0x43
	.short	83                      # 0x53
	.short	99                      # 0x63
	.short	115                     # 0x73
	.short	131                     # 0x83
	.short	163                     # 0xa3
	.short	195                     # 0xc3
	.short	227                     # 0xe3
	.short	258                     # 0x102
	.short	0                       # 0x0
	.short	0                       # 0x0
	.size	cplens, 62

	.type	cplext,@object          # @cplext
	.p2align	4
cplext:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	0                       # 0x0
	.short	112                     # 0x70
	.short	112                     # 0x70
	.size	cplext, 62

	.type	fixed_tl,@object        # @fixed_tl
	.local	fixed_tl
	.comm	fixed_tl,8,8
	.type	fixed_bl,@object        # @fixed_bl
	.data
	.p2align	2
fixed_bl:
	.long	9                       # 0x9
	.size	fixed_bl, 4

	.type	fixed_mem,@object       # @fixed_mem
	.local	fixed_mem
	.comm	fixed_mem,2176,16
	.type	cpdist,@object          # @cpdist
	.section	.rodata,"a",@progbits
	.p2align	4
cpdist:
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	7                       # 0x7
	.short	9                       # 0x9
	.short	13                      # 0xd
	.short	17                      # 0x11
	.short	25                      # 0x19
	.short	33                      # 0x21
	.short	49                      # 0x31
	.short	65                      # 0x41
	.short	97                      # 0x61
	.short	129                     # 0x81
	.short	193                     # 0xc1
	.short	257                     # 0x101
	.short	385                     # 0x181
	.short	513                     # 0x201
	.short	769                     # 0x301
	.short	1025                    # 0x401
	.short	1537                    # 0x601
	.short	2049                    # 0x801
	.short	3073                    # 0xc01
	.short	4097                    # 0x1001
	.short	6145                    # 0x1801
	.short	8193                    # 0x2001
	.short	12289                   # 0x3001
	.short	16385                   # 0x4001
	.short	24577                   # 0x6001
	.size	cpdist, 60

	.type	cpdext,@object          # @cpdext
	.p2align	4
cpdext:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	13                      # 0xd
	.size	cpdext, 60

	.type	fixed_td,@object        # @fixed_td
	.local	fixed_td
	.comm	fixed_td,8,8
	.type	fixed_bd,@object        # @fixed_bd
	.data
	.p2align	2
fixed_bd:
	.long	5                       # 0x5
	.size	fixed_bd, 4

	.type	border,@object          # @border
	.section	.rodata,"a",@progbits
	.p2align	4
border:
	.ascii	"\020\021\022\000\b\007\t\006\n\005\013\004\f\003\r\002\016\001\017"
	.size	border, 19

	.type	inflate_mask,@object    # @inflate_mask
	.p2align	4
inflate_mask:
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	3                       # 0x3
	.short	7                       # 0x7
	.short	15                      # 0xf
	.short	31                      # 0x1f
	.short	63                      # 0x3f
	.short	127                     # 0x7f
	.short	255                     # 0xff
	.short	511                     # 0x1ff
	.short	1023                    # 0x3ff
	.short	2047                    # 0x7ff
	.short	4095                    # 0xfff
	.short	8191                    # 0x1fff
	.short	16383                   # 0x3fff
	.short	32767                   # 0x7fff
	.short	65535                   # 0xffff
	.size	inflate_mask, 34

	.type	huft_build.v,@object    # @huft_build.v
	.local	huft_build.v
	.comm	huft_build.v,1152,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
