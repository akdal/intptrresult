	.text
	.file	"llex.bc"
	.hidden	luaX_init
	.globl	luaX_init
	.p2align	4, 0x90
	.type	luaX_init,@function
luaX_init:                              # @luaX_init
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	$-21, %rbx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movq	luaX_tokens+168(,%rbx,8), %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	luaS_newlstr
	orb	$32, 9(%rax)
	leal	22(%rbx), %ecx
	movb	%cl, 10(%rax)
	incq	%rbx
	jne	.LBB0_1
# BB#2:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	luaX_init, .Lfunc_end0-luaX_init
	.cfi_endproc

	.hidden	luaX_token2str
	.globl	luaX_token2str
	.p2align	4, 0x90
	.type	luaX_token2str,@function
luaX_token2str:                         # @luaX_token2str
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	cmpl	$256, %ebx              # imm = 0x100
	jg	.LBB1_1
# BB#2:
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movzwl	(%rax,%rcx,2), %eax
	movq	56(%r14), %rdi
	movl	$.L.str.31, %ecx
	movl	$.L.str.32, %esi
	testb	$2, %al
	cmovneq	%rcx, %rsi
	xorl	%eax, %eax
	movl	%ebx, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	luaO_pushfstring        # TAILCALL
.LBB1_1:
	movslq	%ebx, %rax
	movq	luaX_tokens-2056(,%rax,8), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	luaX_token2str, .Lfunc_end1-luaX_token2str
	.cfi_endproc

	.hidden	luaX_lexerror
	.globl	luaX_lexerror
	.p2align	4, 0x90
	.type	luaX_lexerror,@function
luaX_lexerror:                          # @luaX_lexerror
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 48
	subq	$160, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 208
.Lcfi17:
	.cfi_offset %rbx, -48
.Lcfi18:
	.cfi_offset %r12, -40
.Lcfi19:
	.cfi_offset %r13, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movl	%edx, %r13d
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	80(%r12), %rsi
	addq	$24, %rsi
	movq	%rsp, %r14
	movl	$80, %edx
	movq	%r14, %rdi
	callq	luaO_chunkid
	movq	56(%r12), %rdi
	movl	4(%r12), %ecx
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	movq	%rbx, %r8
	callq	luaO_pushfstring
	movq	%rax, %r14
	testl	%r13d, %r13d
	je	.LBB2_15
# BB#1:
	movq	56(%r12), %r15
	leal	-284(%r13), %eax
	cmpl	$2, %eax
	ja	.LBB2_11
# BB#2:
	movq	72(%r12), %rbx
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB2_3
# BB#4:
	movabsq	$9223372036854775806, %rax # imm = 0x7FFFFFFFFFFFFFFE
	cmpq	%rax, %rdx
	movq	%r15, %rdi
	jb	.LBB2_6
# BB#5:
	movq	80(%r12), %rsi
	addq	$24, %rsi
	leaq	80(%rsp), %r13
	movl	$80, %edx
	movq	%r13, %rdi
	callq	luaO_chunkid
	movq	56(%r12), %rdi
	movl	4(%r12), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%r13, %rdx
	callq	luaO_pushfstring
	movq	56(%r12), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbx), %rdx
	movq	56(%r12), %rdi
.LBB2_6:
	leaq	(%rdx,%rdx), %r13
	cmpq	$-2, %r13
	jne	.LBB2_7
# BB#8:
	callq	luaM_toobig
	jmp	.LBB2_9
.LBB2_11:
	cmpl	$256, %r13d             # imm = 0x100
	jg	.LBB2_13
# BB#12:
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%r13d, %rcx
	movzwl	(%rax,%rcx,2), %eax
	movl	$.L.str.31, %ecx
	movl	$.L.str.32, %esi
	testb	$2, %al
	cmovneq	%rcx, %rsi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movl	%r13d, %edx
	callq	luaO_pushfstring
	movq	%rax, %rcx
	jmp	.LBB2_14
.LBB2_3:                                # %.save.exit.i_crit_edge
	movq	(%rbx), %rax
	jmp	.LBB2_10
.LBB2_13:
	movslq	%r13d, %rax
	movq	luaX_tokens-2056(,%rax,8), %rcx
	jmp	.LBB2_14
.LBB2_7:
	movq	(%rbx), %rsi
	movq	%r13, %rcx
	callq	luaM_realloc_
.LBB2_9:
	movq	%rax, (%rbx)
	movq	%r13, 16(%rbx)
	movq	8(%rbx), %rcx
.LBB2_10:                               # %save.exit.i
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbx)
	movb	$0, (%rax,%rcx)
	movq	72(%r12), %rax
	movq	(%rax), %rcx
.LBB2_14:                               # %txtToken.exit
	movl	$.L.str.34, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	luaO_pushfstring
.LBB2_15:
	movq	56(%r12), %rdi
	movl	$3, %esi
	callq	luaD_throw
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	luaX_lexerror, .Lfunc_end2-luaX_lexerror
	.cfi_endproc

	.hidden	luaX_syntaxerror
	.globl	luaX_syntaxerror
	.p2align	4, 0x90
	.type	luaX_syntaxerror,@function
luaX_syntaxerror:                       # @luaX_syntaxerror
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %edx
	jmp	luaX_lexerror           # TAILCALL
.Lfunc_end3:
	.size	luaX_syntaxerror, .Lfunc_end3-luaX_syntaxerror
	.cfi_endproc

	.hidden	luaX_newstring
	.globl	luaX_newstring
	.p2align	4, 0x90
	.type	luaX_newstring,@function
luaX_newstring:                         # @luaX_newstring
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	56(%rbx), %r15
	movq	%r15, %rdi
	callq	luaS_newlstr
	movq	%rax, %r14
	movq	48(%rbx), %rax
	movq	8(%rax), %rsi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	luaH_setstr
	cmpl	$0, 8(%rax)
	jne	.LBB4_2
# BB#1:
	movl	$1, (%rax)
	movl	$1, 8(%rax)
.LBB4_2:
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	luaX_newstring, .Lfunc_end4-luaX_newstring
	.cfi_endproc

	.hidden	luaX_setinput
	.globl	luaX_setinput
	.p2align	4, 0x90
	.type	luaX_setinput,@function
luaX_setinput:                          # @luaX_setinput
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
.Lcfi29:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movb	$46, 88(%rbx)
	movq	%rdi, 56(%rbx)
	movl	$287, 32(%rbx)          # imm = 0x11F
	movq	%rdx, 64(%rbx)
	movq	$0, 48(%rbx)
	movl	$1, 4(%rbx)
	movl	$1, 8(%rbx)
	movq	%rcx, 80(%rbx)
	movq	72(%rbx), %rax
	movq	(%rax), %rsi
	movq	16(%rax), %rdx
	movl	$32, %ecx
	callq	luaM_realloc_
	movq	72(%rbx), %rcx
	movq	%rax, (%rcx)
	movq	$32, 16(%rcx)
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB5_2
# BB#1:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB5_3
.LBB5_2:
	callq	luaZ_fill
.LBB5_3:
	movl	%eax, (%rbx)
	popq	%rbx
	retq
.Lfunc_end5:
	.size	luaX_setinput, .Lfunc_end5-luaX_setinput
	.cfi_endproc

	.hidden	luaX_next
	.globl	luaX_next
	.p2align	4, 0x90
	.type	luaX_next,@function
luaX_next:                              # @luaX_next
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 16
.Lcfi31:
	.cfi_offset %rbx, -16
	movl	4(%rdi), %eax
	movl	%eax, 8(%rdi)
	leaq	16(%rdi), %rbx
	cmpl	$287, 32(%rdi)          # imm = 0x11F
	jne	.LBB6_1
# BB#2:
	leaq	24(%rdi), %rsi
	callq	llex
	jmp	.LBB6_3
.LBB6_1:
	addq	$32, %rdi
	movups	(%rdi), %xmm0
	movups	%xmm0, (%rbx)
	movl	$287, %eax              # imm = 0x11F
	movq	%rdi, %rbx
.LBB6_3:
	movl	%eax, (%rbx)
	popq	%rbx
	retq
.Lfunc_end6:
	.size	luaX_next, .Lfunc_end6-luaX_next
	.cfi_endproc

	.p2align	4, 0x90
	.type	llex,@function
llex:                                   # @llex
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 160
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	72(%r15), %rax
	movq	$0, 8(%rax)
	movl	$18433, %ebx            # imm = 0x4801
	jmp	.LBB7_1
.LBB7_18:                               #   in Loop: Header=BB7_1 Depth=1
	movl	16(%r15), %edx
	movl	$.L.str.38, %esi
	movq	%r15, %rdi
	callq	luaX_lexerror
	.p2align	4, 0x90
.LBB7_1:                                # %.critedgethread-pre-split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_2 Depth 2
                                        #     Child Loop BB7_31 Depth 2
	movl	(%r15), %r14d
	jmp	.LBB7_2
	.p2align	4, 0x90
.LBB7_237:                              #   in Loop: Header=BB7_2 Depth=2
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %r14d
	movl	%r14d, (%r15)
.LBB7_2:                                # %.critedge
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1(%r14), %eax
	cmpl	$92, %eax
	ja	.LBB7_3
# BB#6:                                 # %.critedge
                                        #   in Loop: Header=BB7_2 Depth=2
	jmpq	*.LJTI7_0(,%rax,8)
	.p2align	4, 0x90
.LBB7_3:                                # %.critedge
                                        #   in Loop: Header=BB7_2 Depth=2
	cmpl	$126, %r14d
	je	.LBB7_4
.LBB7_235:                              #   in Loop: Header=BB7_2 Depth=2
	callq	__ctype_b_loc
	movq	%rax, %rbp
	movq	(%rbp), %rax
	movslq	%r14d, %rcx
	movzwl	(%rax,%rcx,2), %eax
	testb	$32, %ah
	je	.LBB7_239
# BB#236:                               #   in Loop: Header=BB7_2 Depth=2
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	jne	.LBB7_237
# BB#238:                               #   in Loop: Header=BB7_2 Depth=2
	callq	luaZ_fill
	movl	%eax, %r14d
	movl	%r14d, (%r15)
	jmp	.LBB7_2
.LBB7_7:                                #   in Loop: Header=BB7_1 Depth=1
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_9
# BB#8:                                 #   in Loop: Header=BB7_1 Depth=1
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_10
.LBB7_19:                               #   in Loop: Header=BB7_1 Depth=1
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_21
# BB#20:                                #   in Loop: Header=BB7_1 Depth=1
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_22
.LBB7_9:                                #   in Loop: Header=BB7_1 Depth=1
	callq	luaZ_fill
.LBB7_10:                               #   in Loop: Header=BB7_1 Depth=1
	movl	%eax, (%r15)
	cmpl	$13, %eax
	je	.LBB7_12
# BB#11:                                #   in Loop: Header=BB7_1 Depth=1
	cmpl	$10, %eax
	jne	.LBB7_17
.LBB7_12:                               #   in Loop: Header=BB7_1 Depth=1
	cmpl	%r14d, %eax
	je	.LBB7_17
# BB#13:                                #   in Loop: Header=BB7_1 Depth=1
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_15
# BB#14:                                #   in Loop: Header=BB7_1 Depth=1
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_16
.LBB7_21:                               #   in Loop: Header=BB7_1 Depth=1
	callq	luaZ_fill
.LBB7_22:                               #   in Loop: Header=BB7_1 Depth=1
	movl	%eax, (%r15)
	cmpl	$45, %eax
	jne	.LBB7_23
# BB#24:                                #   in Loop: Header=BB7_1 Depth=1
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_26
# BB#25:                                #   in Loop: Header=BB7_1 Depth=1
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_27
.LBB7_26:                               #   in Loop: Header=BB7_1 Depth=1
	callq	luaZ_fill
.LBB7_27:                               #   in Loop: Header=BB7_1 Depth=1
	movl	%eax, (%r15)
	cmpl	$91, %eax
	jne	.LBB7_31
# BB#28:                                #   in Loop: Header=BB7_1 Depth=1
	movq	%r15, %rdi
	callq	skip_sep
	movq	72(%r15), %rcx
	movq	$0, 8(%rcx)
	testl	%eax, %eax
	js	.LBB7_30
# BB#29:                                # %.critedge.critedge
                                        #   in Loop: Header=BB7_1 Depth=1
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%eax, %edx
	callq	read_long_string
	movq	72(%r15), %rax
	movq	$0, 8(%rax)
	jmp	.LBB7_1
.LBB7_15:                               #   in Loop: Header=BB7_1 Depth=1
	callq	luaZ_fill
.LBB7_16:                               #   in Loop: Header=BB7_1 Depth=1
	movl	%eax, (%r15)
.LBB7_17:                               #   in Loop: Header=BB7_1 Depth=1
	movl	4(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 4(%r15)
	cmpl	$2147483644, %eax       # imm = 0x7FFFFFFC
	jl	.LBB7_1
	jmp	.LBB7_18
.LBB7_30:                               # %thread-pre-split
                                        #   in Loop: Header=BB7_1 Depth=1
	movl	(%r15), %eax
	jmp	.LBB7_31
	.p2align	4, 0x90
.LBB7_34:                               #   in Loop: Header=BB7_31 Depth=2
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	movl	%eax, (%r15)
.LBB7_31:                               # %.preheader
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%eax
	cmpl	$14, %eax
	ja	.LBB7_33
# BB#32:                                # %.preheader
                                        #   in Loop: Header=BB7_31 Depth=2
	btl	%eax, %ebx
	jb	.LBB7_1
.LBB7_33:                               #   in Loop: Header=BB7_31 Depth=2
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	jne	.LBB7_34
# BB#35:                                #   in Loop: Header=BB7_31 Depth=2
	callq	luaZ_fill
	movl	%eax, (%r15)
	jmp	.LBB7_31
.LBB7_239:
	testb	$8, %ah
	jne	.LBB7_240
# BB#241:
	cmpl	$95, %r14d
	je	.LBB7_243
# BB#242:
	andl	$1024, %eax             # imm = 0x400
	jne	.LBB7_243
# BB#262:
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_264
# BB#263:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	movl	%eax, (%r15)
	jmp	.LBB7_266
.LBB7_240:
	movq	%r15, %rdi
	movq	%r12, %rsi
.LBB7_234:                              # %.thread
	callq	read_numeral
	movl	$284, %r14d             # imm = 0x11C
	jmp	.LBB7_266
.LBB7_243:                              # %.critedge3.preheader
	movq	%r12, 8(%rsp)           # 8-byte Spill
	leaq	16(%rsp), %r12
	.p2align	4, 0x90
.LBB7_244:                              # %.critedge3
                                        # =>This Inner Loop Header: Depth=1
	movq	72(%r15), %rbx
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB7_245
# BB#246:                               #   in Loop: Header=BB7_244 Depth=1
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	cmpq	%rax, %rdx
	jbe	.LBB7_248
# BB#247:                               #   in Loop: Header=BB7_244 Depth=1
	movq	80(%r15), %rsi
	addq	$24, %rsi
	movl	$80, %edx
	movq	%r12, %rdi
	callq	luaO_chunkid
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbx), %rdx
.LBB7_248:                              #   in Loop: Header=BB7_244 Depth=1
	leaq	(%rdx,%rdx), %r13
	cmpq	$-2, %r13
	movq	56(%r15), %rdi
	jne	.LBB7_249
# BB#250:                               #   in Loop: Header=BB7_244 Depth=1
	callq	luaM_toobig
	jmp	.LBB7_251
	.p2align	4, 0x90
.LBB7_245:                              # %._crit_edge.i121
                                        #   in Loop: Header=BB7_244 Depth=1
	movq	(%rbx), %rax
	jmp	.LBB7_252
	.p2align	4, 0x90
.LBB7_249:                              #   in Loop: Header=BB7_244 Depth=1
	movq	(%rbx), %rsi
	movq	%r13, %rcx
	callq	luaM_realloc_
.LBB7_251:                              #   in Loop: Header=BB7_244 Depth=1
	movq	%rax, (%rbx)
	movq	%r13, 16(%rbx)
	movq	8(%rbx), %rcx
.LBB7_252:                              # %save.exit126
                                        #   in Loop: Header=BB7_244 Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbx)
	movb	%r14b, (%rax,%rcx)
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_254
# BB#253:                               #   in Loop: Header=BB7_244 Depth=1
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %r14d
	jmp	.LBB7_255
	.p2align	4, 0x90
.LBB7_254:                              #   in Loop: Header=BB7_244 Depth=1
	callq	luaZ_fill
	movl	%eax, %r14d
.LBB7_255:                              #   in Loop: Header=BB7_244 Depth=1
	movl	%r14d, (%r15)
	movq	(%rbp), %rax
	movslq	%r14d, %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$8, %eax
	cmpl	$95, %ecx
	je	.LBB7_244
# BB#256:                               #   in Loop: Header=BB7_244 Depth=1
	testw	%ax, %ax
	jne	.LBB7_244
# BB#257:
	movq	56(%r15), %rbp
	movq	72(%r15), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	movq	%rbp, %rdi
	callq	luaS_newlstr
	movq	%rax, %rbx
	movq	48(%r15), %rax
	movq	8(%rax), %rsi
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	luaH_setstr
	cmpl	$0, 8(%rax)
	movq	8(%rsp), %rcx           # 8-byte Reload
	jne	.LBB7_259
# BB#258:
	movl	$1, (%rax)
	movl	$1, 8(%rax)
.LBB7_259:                              # %luaX_newstring.exit
	movzbl	10(%rbx), %r14d
	testl	%r14d, %r14d
	je	.LBB7_261
# BB#260:
	orl	$256, %r14d             # imm = 0x100
	jmp	.LBB7_266
.LBB7_71:
	movq	72(%r15), %rbx
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB7_72
# BB#73:
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	movq	%r12, %r13
	cmpq	%rax, %rdx
	jbe	.LBB7_74
# BB#75:
	movq	80(%r15), %rsi
	addq	$24, %rsi
	leaq	16(%rsp), %rbp
	movl	$80, %edx
	movq	%rbp, %rdi
	callq	luaO_chunkid
	leaq	56(%r15), %r12
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbx), %rdx
	jmp	.LBB7_76
.LBB7_264:
	callq	luaZ_fill
	movl	%eax, (%r15)
	jmp	.LBB7_266
.LBB7_261:
	movq	%rbx, (%rcx)
	movl	$285, %r14d             # imm = 0x11D
	jmp	.LBB7_266
.LBB7_265:                              # %.thread.loopexit
	movl	$287, %r14d             # imm = 0x11F
	jmp	.LBB7_266
.LBB7_191:
	movq	72(%r15), %rbp
	movq	8(%rbp), %rcx
	movq	16(%rbp), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB7_192
# BB#193:
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	cmpq	%rax, %rdx
	jbe	.LBB7_194
# BB#195:
	movq	80(%r15), %rsi
	addq	$24, %rsi
	leaq	16(%rsp), %rbx
	movl	$80, %edx
	movq	%rbx, %rdi
	callq	luaO_chunkid
	leaq	56(%r15), %r14
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbp), %rdx
	jmp	.LBB7_196
.LBB7_49:
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_51
# BB#50:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_52
.LBB7_57:
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_59
# BB#58:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_60
.LBB7_36:
	movq	%r15, %rdi
	callq	skip_sep
	testl	%eax, %eax
	js	.LBB7_38
# BB#37:
	movq	%r15, %rdi
	movq	%r12, %rsi
	movl	%eax, %edx
	callq	read_long_string
	movl	$286, %r14d             # imm = 0x11E
	jmp	.LBB7_266
.LBB7_72:                               # %._crit_edge.i.i
	movq	(%rbx), %rax
	jmp	.LBB7_80
.LBB7_4:
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_65
# BB#5:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_66
.LBB7_74:                               # %._crit_edge104.i
	leaq	56(%r15), %r12
.LBB7_76:
	leaq	(%rdx,%rdx), %rbp
	cmpq	$-2, %rbp
	movq	(%r12), %rdi
	jne	.LBB7_77
# BB#78:
	callq	luaM_toobig
	jmp	.LBB7_79
.LBB7_192:                              # %._crit_edge.i113
	movq	(%rbp), %rax
	jmp	.LBB7_200
.LBB7_77:
	movq	(%rbx), %rsi
	movq	%rbp, %rcx
	callq	luaM_realloc_
.LBB7_79:
	movq	%r13, %r12
	movq	%rax, (%rbx)
	movq	%rbp, 16(%rbx)
	movq	8(%rbx), %rcx
.LBB7_80:                               # %save.exit.i
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbx)
	movb	%r14b, (%rax,%rcx)
	jmp	.LBB7_81
	.p2align	4, 0x90
.LBB7_167:                              #   in Loop: Header=BB7_81 Depth=1
	movq	72(%r15), %rbx
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB7_168
# BB#169:                               #   in Loop: Header=BB7_81 Depth=1
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	cmpq	%rax, %rdx
	jbe	.LBB7_171
# BB#170:                               #   in Loop: Header=BB7_81 Depth=1
	movq	80(%r15), %rsi
	addq	$24, %rsi
	movl	$80, %edx
	leaq	16(%rsp), %r13
	movq	%r13, %rdi
	callq	luaO_chunkid
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%r13, %rdx
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbx), %rdx
.LBB7_171:                              #   in Loop: Header=BB7_81 Depth=1
	leaq	(%rdx,%rdx), %r13
	cmpq	$-2, %r13
	movq	56(%r15), %rdi
	jne	.LBB7_172
# BB#173:                               #   in Loop: Header=BB7_81 Depth=1
	callq	luaM_toobig
	jmp	.LBB7_174
.LBB7_168:                              # %._crit_edge.i88.i
                                        #   in Loop: Header=BB7_81 Depth=1
	movq	(%rbx), %rax
	jmp	.LBB7_175
.LBB7_172:                              #   in Loop: Header=BB7_81 Depth=1
	movq	(%rbx), %rsi
	movq	%r13, %rcx
	callq	luaM_realloc_
.LBB7_174:                              #   in Loop: Header=BB7_81 Depth=1
	movq	%rax, (%rbx)
	movq	%r13, 16(%rbx)
	movq	8(%rbx), %rcx
	jmp	.LBB7_175
.LBB7_157:                              #   in Loop: Header=BB7_81 Depth=1
	movb	$8, %r13b
	jmp	.LBB7_158
.LBB7_106:                              #   in Loop: Header=BB7_81 Depth=1
	movb	$12, %r13b
	jmp	.LBB7_158
.LBB7_107:                              #   in Loop: Header=BB7_81 Depth=1
	movb	$10, %r13b
	jmp	.LBB7_158
.LBB7_108:                              #   in Loop: Header=BB7_81 Depth=1
	movb	$13, %r13b
	jmp	.LBB7_158
.LBB7_109:                              #   in Loop: Header=BB7_81 Depth=1
	movb	$9, %r13b
	jmp	.LBB7_158
.LBB7_110:                              #   in Loop: Header=BB7_81 Depth=1
	movb	$11, %r13b
	.p2align	4, 0x90
.LBB7_158:                              #   in Loop: Header=BB7_81 Depth=1
	movq	72(%r15), %rbx
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB7_159
# BB#160:                               #   in Loop: Header=BB7_81 Depth=1
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	cmpq	%rax, %rdx
	jbe	.LBB7_162
# BB#161:                               #   in Loop: Header=BB7_81 Depth=1
	movq	80(%r15), %rsi
	addq	$24, %rsi
	movl	$80, %edx
	leaq	16(%rsp), %rbp
	movq	%rbp, %rdi
	callq	luaO_chunkid
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbx), %rdx
.LBB7_162:                              #   in Loop: Header=BB7_81 Depth=1
	leaq	(%rdx,%rdx), %rbp
	cmpq	$-2, %rbp
	movq	56(%r15), %rdi
	jne	.LBB7_163
# BB#164:                               #   in Loop: Header=BB7_81 Depth=1
	callq	luaM_toobig
	jmp	.LBB7_165
.LBB7_159:                              # %._crit_edge.i80.i
                                        #   in Loop: Header=BB7_81 Depth=1
	movq	(%rbx), %rax
	jmp	.LBB7_166
.LBB7_163:                              #   in Loop: Header=BB7_81 Depth=1
	movq	(%rbx), %rsi
	movq	%rbp, %rcx
	callq	luaM_realloc_
.LBB7_165:                              #   in Loop: Header=BB7_81 Depth=1
	movq	%rax, (%rbx)
	movq	%rbp, 16(%rbx)
	movq	8(%rbx), %rcx
.LBB7_166:                              # %save.exit85.i
                                        #   in Loop: Header=BB7_81 Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbx)
	movb	%r13b, (%rax,%rcx)
	jmp	.LBB7_81
.LBB7_133:                              #   in Loop: Header=BB7_81 Depth=1
	movq	72(%r15), %rbx
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB7_134
# BB#135:                               #   in Loop: Header=BB7_81 Depth=1
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	cmpq	%rax, %rdx
	jbe	.LBB7_137
# BB#136:                               #   in Loop: Header=BB7_81 Depth=1
	movq	80(%r15), %rsi
	addq	$24, %rsi
	movl	$80, %edx
	leaq	16(%rsp), %r13
	movq	%r13, %rdi
	callq	luaO_chunkid
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%r13, %rdx
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbx), %rdx
.LBB7_137:                              #   in Loop: Header=BB7_81 Depth=1
	leaq	(%rdx,%rdx), %r13
	cmpq	$-2, %r13
	movq	56(%r15), %rdi
	jne	.LBB7_138
# BB#139:                               #   in Loop: Header=BB7_81 Depth=1
	callq	luaM_toobig
	jmp	.LBB7_140
.LBB7_134:                              # %._crit_edge.i64.i
                                        #   in Loop: Header=BB7_81 Depth=1
	movq	(%rbx), %rax
	jmp	.LBB7_141
.LBB7_138:                              #   in Loop: Header=BB7_81 Depth=1
	movq	(%rbx), %rsi
	movq	%r13, %rcx
	callq	luaM_realloc_
.LBB7_140:                              #   in Loop: Header=BB7_81 Depth=1
	movq	%rax, (%rbx)
	movq	%r13, 16(%rbx)
	movq	8(%rbx), %rcx
.LBB7_141:                              # %save.exit69.i
                                        #   in Loop: Header=BB7_81 Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbx)
	movb	%bpl, (%rax,%rcx)
	jmp	.LBB7_81
	.p2align	4, 0x90
.LBB7_175:                              # %save.exit93.i
                                        #   in Loop: Header=BB7_81 Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbx)
	movb	%bpl, (%rax,%rcx)
.LBB7_81:                               # %save.exit.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_86 Depth 2
                                        #       Child Loop BB7_131 Depth 3
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_83
# BB#82:                                #   in Loop: Header=BB7_81 Depth=1
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %ebp
	jmp	.LBB7_84
	.p2align	4, 0x90
.LBB7_83:                               #   in Loop: Header=BB7_81 Depth=1
	callq	luaZ_fill
	movl	%eax, %ebp
.LBB7_84:                               #   in Loop: Header=BB7_81 Depth=1
	movl	%ebp, (%r15)
	cmpl	%r14d, %ebp
	jne	.LBB7_86
	jmp	.LBB7_176
.LBB7_145:                              # %.critedge.i
                                        #   in Loop: Header=BB7_86 Depth=2
	cmpl	$256, %ebx              # imm = 0x100
	jl	.LBB7_147
# BB#146:                               #   in Loop: Header=BB7_86 Depth=2
	movl	$.L.str.43, %esi
	movl	$286, %edx              # imm = 0x11E
	movq	%r15, %rdi
	callq	luaX_lexerror
.LBB7_147:                              #   in Loop: Header=BB7_86 Depth=2
	movq	72(%r15), %rbp
	movq	8(%rbp), %rcx
	movq	16(%rbp), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	movq	8(%rsp), %r12           # 8-byte Reload
	jbe	.LBB7_148
# BB#149:                               #   in Loop: Header=BB7_86 Depth=2
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	cmpq	%rax, %rdx
	jbe	.LBB7_151
# BB#150:                               #   in Loop: Header=BB7_86 Depth=2
	movq	80(%r15), %rsi
	addq	$24, %rsi
	movl	$80, %edx
	leaq	16(%rsp), %r13
	movq	%r13, %rdi
	callq	luaO_chunkid
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%r13, %rdx
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbp), %rdx
.LBB7_151:                              #   in Loop: Header=BB7_86 Depth=2
	leaq	(%rdx,%rdx), %r13
	cmpq	$-2, %r13
	movq	56(%r15), %rdi
	jne	.LBB7_152
# BB#153:                               #   in Loop: Header=BB7_86 Depth=2
	callq	luaM_toobig
	jmp	.LBB7_154
.LBB7_114:                              #   in Loop: Header=BB7_86 Depth=2
	movq	(%rbx), %rsi
	movq	%rbp, %rcx
	callq	luaM_realloc_
.LBB7_116:                              #   in Loop: Header=BB7_86 Depth=2
	movq	%rax, (%rbx)
	movq	%rbp, 16(%rbx)
	movq	8(%rbx), %rcx
.LBB7_117:                              # %save.exit61.i
                                        #   in Loop: Header=BB7_86 Depth=2
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbx)
	movb	$10, (%rax,%rcx)
	movl	(%r15), %ebx
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_119
# BB#118:                               #   in Loop: Header=BB7_86 Depth=2
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_120
.LBB7_119:                              #   in Loop: Header=BB7_86 Depth=2
	callq	luaZ_fill
.LBB7_120:                              #   in Loop: Header=BB7_86 Depth=2
	movl	%eax, (%r15)
	cmpl	$13, %eax
	je	.LBB7_122
# BB#121:                               #   in Loop: Header=BB7_86 Depth=2
	cmpl	$10, %eax
	jne	.LBB7_127
.LBB7_122:                              #   in Loop: Header=BB7_86 Depth=2
	cmpl	%ebx, %eax
	je	.LBB7_127
# BB#123:                               #   in Loop: Header=BB7_86 Depth=2
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_125
# BB#124:                               #   in Loop: Header=BB7_86 Depth=2
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_126
.LBB7_125:                              #   in Loop: Header=BB7_86 Depth=2
	callq	luaZ_fill
.LBB7_126:                              #   in Loop: Header=BB7_86 Depth=2
	movl	%eax, (%r15)
.LBB7_127:                              #   in Loop: Header=BB7_86 Depth=2
	movl	4(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 4(%r15)
	cmpl	$2147483644, %eax       # imm = 0x7FFFFFFC
	jl	.LBB7_156
# BB#128:                               #   in Loop: Header=BB7_86 Depth=2
	movl	16(%r15), %edx
	movl	$.L.str.38, %esi
	jmp	.LBB7_90
.LBB7_148:                              # %._crit_edge.i72.i
                                        #   in Loop: Header=BB7_86 Depth=2
	movq	(%rbp), %rax
	jmp	.LBB7_155
.LBB7_152:                              #   in Loop: Header=BB7_86 Depth=2
	movq	(%rbp), %rsi
	movq	%r13, %rcx
	callq	luaM_realloc_
.LBB7_154:                              #   in Loop: Header=BB7_86 Depth=2
	movq	%rax, (%rbp)
	movq	%r13, 16(%rbp)
	movq	8(%rbp), %rcx
.LBB7_155:                              # %save.exit77.i
                                        #   in Loop: Header=BB7_86 Depth=2
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbp)
	movb	%bl, (%rax,%rcx)
.LBB7_156:                              # %thread-pre-split.i
                                        #   in Loop: Header=BB7_86 Depth=2
	movl	(%r15), %ebp
	cmpl	%r14d, %ebp
	jne	.LBB7_86
	jmp	.LBB7_176
	.p2align	4, 0x90
.LBB7_90:                               # %thread-pre-split.i
                                        #   in Loop: Header=BB7_86 Depth=2
	movq	%r15, %rdi
	callq	luaX_lexerror
	movl	(%r15), %ebp
	cmpl	%r14d, %ebp
	je	.LBB7_176
.LBB7_86:                               #   Parent Loop BB7_81 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_131 Depth 3
	cmpl	$12, %ebp
	jg	.LBB7_91
# BB#87:                                #   in Loop: Header=BB7_86 Depth=2
	cmpl	$-1, %ebp
	je	.LBB7_95
# BB#88:                                #   in Loop: Header=BB7_86 Depth=2
	cmpl	$10, %ebp
	je	.LBB7_89
	jmp	.LBB7_167
	.p2align	4, 0x90
.LBB7_91:                               #   in Loop: Header=BB7_86 Depth=2
	cmpl	$13, %ebp
	je	.LBB7_89
# BB#92:                                #   in Loop: Header=BB7_86 Depth=2
	cmpl	$92, %ebp
	jne	.LBB7_167
# BB#93:                                #   in Loop: Header=BB7_86 Depth=2
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_96
# BB#94:                                #   in Loop: Header=BB7_86 Depth=2
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %ebp
	jmp	.LBB7_97
	.p2align	4, 0x90
.LBB7_89:                               #   in Loop: Header=BB7_86 Depth=2
	movl	$.L.str.42, %esi
	movl	$286, %edx              # imm = 0x11E
	jmp	.LBB7_90
	.p2align	4, 0x90
.LBB7_95:                               #   in Loop: Header=BB7_86 Depth=2
	movq	80(%r15), %rsi
	addq	$24, %rsi
	movl	$80, %edx
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdi
	callq	luaO_chunkid
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.42, %r8d
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	luaO_pushfstring
	movq	%rax, %rdx
	movq	56(%r15), %rdi
	movl	$.L.str.34, %esi
	movl	$.L.str.30, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movl	(%r15), %ebp
	cmpl	%r14d, %ebp
	jne	.LBB7_86
	jmp	.LBB7_176
.LBB7_96:                               #   in Loop: Header=BB7_86 Depth=2
	callq	luaZ_fill
	movl	%eax, %ebp
.LBB7_97:                               #   in Loop: Header=BB7_86 Depth=2
	movl	%ebp, (%r15)
	cmpl	$96, %ebp
	jle	.LBB7_98
# BB#104:                               #   in Loop: Header=BB7_86 Depth=2
	leal	-97(%rbp), %eax
	cmpl	$21, %eax
	ja	.LBB7_129
# BB#105:                               #   in Loop: Header=BB7_86 Depth=2
	movb	$7, %r13b
	jmpq	*.LJTI7_1(,%rax,8)
.LBB7_98:                               #   in Loop: Header=BB7_86 Depth=2
	cmpl	$-1, %ebp
	je	.LBB7_99
# BB#100:                               #   in Loop: Header=BB7_86 Depth=2
	cmpl	$10, %ebp
	je	.LBB7_102
# BB#101:                               #   in Loop: Header=BB7_86 Depth=2
	cmpl	$13, %ebp
	jne	.LBB7_129
.LBB7_102:                              #   in Loop: Header=BB7_86 Depth=2
	movq	72(%r15), %rbx
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB7_103
# BB#111:                               #   in Loop: Header=BB7_86 Depth=2
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	cmpq	%rax, %rdx
	jbe	.LBB7_113
# BB#112:                               #   in Loop: Header=BB7_86 Depth=2
	movq	80(%r15), %rsi
	addq	$24, %rsi
	movl	$80, %edx
	leaq	16(%rsp), %rbp
	movq	%rbp, %rdi
	callq	luaO_chunkid
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbx), %rdx
.LBB7_113:                              #   in Loop: Header=BB7_86 Depth=2
	leaq	(%rdx,%rdx), %rbp
	cmpq	$-2, %rbp
	movq	56(%r15), %rdi
	jne	.LBB7_114
# BB#115:                               #   in Loop: Header=BB7_86 Depth=2
	callq	luaM_toobig
	jmp	.LBB7_116
.LBB7_99:                               #   in Loop: Header=BB7_86 Depth=2
	movl	$-1, %ebp
	cmpl	%r14d, %ebp
	jne	.LBB7_86
	jmp	.LBB7_176
.LBB7_103:                              # %._crit_edge.i56.i
                                        #   in Loop: Header=BB7_86 Depth=2
	movq	(%rbx), %rax
	jmp	.LBB7_117
.LBB7_129:                              #   in Loop: Header=BB7_86 Depth=2
	callq	__ctype_b_loc
	movq	%rax, %rdx
	movq	(%rdx), %rax
	movslq	%ebp, %rcx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB7_133
# BB#130:                               # %.preheader.i.preheader
                                        #   in Loop: Header=BB7_86 Depth=2
	movq	%r12, 8(%rsp)           # 8-byte Spill
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB7_131:                              # %.preheader.i
                                        #   Parent Loop BB7_81 Depth=1
                                        #     Parent Loop BB7_86 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebp, %r13d
	leal	(%rbx,%rbx,4), %ebx
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_142
# BB#132:                               #   in Loop: Header=BB7_131 Depth=3
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %ebp
	jmp	.LBB7_143
.LBB7_142:                              #   in Loop: Header=BB7_131 Depth=3
	movq	%rdx, %rbp
	callq	luaZ_fill
	movq	%rbp, %rdx
	movl	%eax, %ebp
.LBB7_143:                              #   in Loop: Header=BB7_131 Depth=3
	leal	-48(%r13,%rbx,2), %ebx
	movl	%ebp, (%r15)
	incl	%r12d
	cmpl	$2, %r12d
	jg	.LBB7_145
# BB#144:                               #   in Loop: Header=BB7_131 Depth=3
	movq	(%rdx), %rax
	movslq	%ebp, %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB7_131
	jmp	.LBB7_145
.LBB7_176:                              # %._crit_edge.i
	movq	72(%r15), %rbx
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB7_177
# BB#178:
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	movq	%r12, %r13
	cmpq	%rax, %rdx
	jbe	.LBB7_179
# BB#180:
	movq	80(%r15), %rsi
	addq	$24, %rsi
	leaq	16(%rsp), %rbp
	movl	$80, %edx
	movq	%rbp, %rdi
	callq	luaO_chunkid
	leaq	56(%r15), %r12
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbx), %rdx
	jmp	.LBB7_181
.LBB7_177:                              # %._crit_edge.i96.i
	movq	(%rbx), %rax
	jmp	.LBB7_185
.LBB7_179:                              # %._crit_edge103.i
	leaq	56(%r15), %r12
.LBB7_181:
	leaq	(%rdx,%rdx), %rbp
	cmpq	$-2, %rbp
	movq	(%r12), %rdi
	jne	.LBB7_182
# BB#183:
	callq	luaM_toobig
	jmp	.LBB7_184
.LBB7_182:
	movq	(%rbx), %rsi
	movq	%rbp, %rcx
	callq	luaM_realloc_
.LBB7_184:
	movq	%r13, %r12
	movq	%rax, (%rbx)
	movq	%rbp, 16(%rbx)
	movq	8(%rbx), %rcx
.LBB7_185:                              # %save.exit101.i
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbx)
	movb	%r14b, (%rax,%rcx)
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_187
# BB#186:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_188
.LBB7_187:
	callq	luaZ_fill
.LBB7_188:
	movl	%eax, (%r15)
	movq	56(%r15), %rbp
	movq	72(%r15), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	incq	%rsi
	addq	$-2, %rdx
	movq	%rbp, %rdi
	callq	luaS_newlstr
	movq	%rax, %rbx
	movq	48(%r15), %rax
	movq	8(%rax), %rsi
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	luaH_setstr
	cmpl	$0, 8(%rax)
	jne	.LBB7_190
# BB#189:
	movl	$1, (%rax)
	movl	$1, 8(%rax)
.LBB7_190:                              # %read_string.exit
	movq	%rbx, (%r12)
	movl	$286, %r14d             # imm = 0x11E
	jmp	.LBB7_266
.LBB7_51:
	callq	luaZ_fill
.LBB7_52:
	movl	%eax, (%r15)
	movl	$60, %r14d
	cmpl	$61, %eax
	jne	.LBB7_266
# BB#53:
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_55
# BB#54:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_56
.LBB7_59:
	callq	luaZ_fill
.LBB7_60:
	movl	%eax, (%r15)
	movl	$62, %r14d
	cmpl	$61, %eax
	jne	.LBB7_266
# BB#61:
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_63
# BB#62:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_64
.LBB7_38:
	cmpl	$-1, %eax
	je	.LBB7_39
# BB#40:
	movl	$.L.str.36, %esi
	movl	$286, %edx              # imm = 0x11E
	movq	%r15, %rdi
	callq	luaX_lexerror
.LBB7_41:                               # %.loopexit
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_43
# BB#42:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_44
.LBB7_43:
	callq	luaZ_fill
.LBB7_44:
	movl	%eax, (%r15)
	movl	$61, %r14d
	cmpl	$61, %eax
	jne	.LBB7_266
# BB#45:
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_47
# BB#46:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_48
.LBB7_194:                              # %._crit_edge180
	leaq	56(%r15), %r14
.LBB7_196:
	leaq	(%rdx,%rdx), %rbx
	cmpq	$-2, %rbx
	movq	(%r14), %rdi
	jne	.LBB7_197
# BB#198:
	callq	luaM_toobig
	jmp	.LBB7_199
.LBB7_197:
	movq	(%rbp), %rsi
	movq	%rbx, %rcx
	callq	luaM_realloc_
.LBB7_199:
	movq	%rax, (%rbp)
	movq	%rbx, 16(%rbp)
	movq	8(%rbp), %rcx
.LBB7_200:                              # %save.exit
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbp)
	movb	$46, (%rax,%rcx)
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	movq	%r12, %rbp
	testq	%rax, %rax
	je	.LBB7_202
# BB#201:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %ebx
	jmp	.LBB7_203
.LBB7_202:
	callq	luaZ_fill
	movl	%eax, %ebx
.LBB7_203:
	movl	%ebx, (%r15)
	movl	$1, %eax
	movl	%ebx, %ecx
	shlq	%cl, %rax
	cmpl	$63, %ebx
	ja	.LBB7_232
# BB#204:
	movabsq	$70368744177665, %r12   # imm = 0x400000000001
	andq	%r12, %rax
	je	.LBB7_232
# BB#205:
	movq	72(%r15), %rbp
	movq	8(%rbp), %rcx
	movq	16(%rbp), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB7_206
# BB#207:
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	cmpq	%rax, %rdx
	jbe	.LBB7_208
# BB#209:
	movq	80(%r15), %rsi
	addq	$24, %rsi
	leaq	16(%rsp), %r14
	movl	$80, %edx
	movq	%r14, %rdi
	callq	luaO_chunkid
	leaq	56(%r15), %r13
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbp), %rdx
	jmp	.LBB7_210
.LBB7_232:
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movl	$46, %r14d
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB7_266
# BB#233:
	movq	%r15, %rdi
	movq	%rbp, %rsi
	jmp	.LBB7_234
.LBB7_206:                              # %._crit_edge.i129
	movq	(%rbp), %rax
	jmp	.LBB7_214
.LBB7_47:
	callq	luaZ_fill
.LBB7_48:
	movl	%eax, (%r15)
	movl	$280, %r14d             # imm = 0x118
	jmp	.LBB7_266
.LBB7_65:
	callq	luaZ_fill
.LBB7_66:
	movl	%eax, (%r15)
	movl	$126, %r14d
	cmpl	$61, %eax
	jne	.LBB7_266
# BB#67:
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_69
# BB#68:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_70
.LBB7_55:
	callq	luaZ_fill
.LBB7_56:
	movl	%eax, (%r15)
	movl	$282, %r14d             # imm = 0x11A
	jmp	.LBB7_266
.LBB7_63:
	callq	luaZ_fill
.LBB7_64:
	movl	%eax, (%r15)
	movl	$281, %r14d             # imm = 0x119
	jmp	.LBB7_266
.LBB7_39:
	movl	$91, %r14d
	jmp	.LBB7_266
.LBB7_208:                              # %._crit_edge179
	leaq	56(%r15), %r13
.LBB7_210:
	leaq	(%rdx,%rdx), %r14
	cmpq	$-2, %r14
	movq	(%r13), %rdi
	jne	.LBB7_211
# BB#212:
	callq	luaM_toobig
	jmp	.LBB7_213
.LBB7_211:
	movq	(%rbp), %rsi
	movq	%r14, %rcx
	callq	luaM_realloc_
.LBB7_213:
	movq	%rax, (%rbp)
	movq	%r14, 16(%rbp)
	movq	8(%rbp), %rcx
.LBB7_214:                              # %save.exit134
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbp)
	movb	%bl, (%rax,%rcx)
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_216
# BB#215:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %ebp
	jmp	.LBB7_217
.LBB7_216:
	callq	luaZ_fill
	movl	%eax, %ebp
.LBB7_217:
	movl	%ebp, (%r15)
	movl	$1, %eax
	movl	%ebp, %ecx
	shlq	%cl, %rax
	movl	$278, %r14d             # imm = 0x116
	cmpl	$63, %ebp
	ja	.LBB7_266
# BB#218:
	andq	%r12, %rax
	je	.LBB7_266
# BB#219:
	movq	72(%r15), %rbx
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB7_220
# BB#221:
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	cmpq	%rax, %rdx
	jbe	.LBB7_222
# BB#223:
	movq	80(%r15), %rsi
	addq	$24, %rsi
	leaq	16(%rsp), %r14
	movl	$80, %edx
	movq	%r14, %rdi
	callq	luaO_chunkid
	leaq	56(%r15), %r12
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbx), %rdx
	jmp	.LBB7_224
.LBB7_220:                              # %._crit_edge.i137
	movq	(%rbx), %rax
	jmp	.LBB7_228
.LBB7_69:
	callq	luaZ_fill
.LBB7_70:
	movl	%eax, (%r15)
	movl	$283, %r14d             # imm = 0x11B
	jmp	.LBB7_266
.LBB7_222:                              # %._crit_edge
	leaq	56(%r15), %r12
.LBB7_224:
	leaq	(%rdx,%rdx), %r14
	cmpq	$-2, %r14
	movq	(%r12), %rdi
	jne	.LBB7_225
# BB#226:
	callq	luaM_toobig
	jmp	.LBB7_227
.LBB7_225:
	movq	(%rbx), %rsi
	movq	%r14, %rcx
	callq	luaM_realloc_
.LBB7_227:
	movq	%rax, (%rbx)
	movq	%r14, 16(%rbx)
	movq	8(%rbx), %rcx
.LBB7_228:                              # %save.exit142
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbx)
	movb	%bpl, (%rax,%rcx)
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB7_230
# BB#229:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB7_231
.LBB7_230:
	callq	luaZ_fill
.LBB7_231:
	movl	%eax, (%r15)
	movl	$279, %r14d             # imm = 0x117
.LBB7_266:                              # %.thread
	movl	%r14d, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_23:
	movl	$45, %r14d
	jmp	.LBB7_266
.Lfunc_end7:
	.size	llex, .Lfunc_end7-llex
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_265
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_7
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_7
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_71
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_71
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_19
	.quad	.LBB7_191
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_49
	.quad	.LBB7_41
	.quad	.LBB7_57
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_235
	.quad	.LBB7_36
.LJTI7_1:
	.quad	.LBB7_158
	.quad	.LBB7_157
	.quad	.LBB7_129
	.quad	.LBB7_129
	.quad	.LBB7_129
	.quad	.LBB7_106
	.quad	.LBB7_129
	.quad	.LBB7_129
	.quad	.LBB7_129
	.quad	.LBB7_129
	.quad	.LBB7_129
	.quad	.LBB7_129
	.quad	.LBB7_129
	.quad	.LBB7_107
	.quad	.LBB7_129
	.quad	.LBB7_129
	.quad	.LBB7_129
	.quad	.LBB7_108
	.quad	.LBB7_129
	.quad	.LBB7_109
	.quad	.LBB7_129
	.quad	.LBB7_110

	.text
	.hidden	luaX_lookahead
	.globl	luaX_lookahead
	.p2align	4, 0x90
	.type	luaX_lookahead,@function
luaX_lookahead:                         # @luaX_lookahead
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 16
.Lcfi46:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	40(%rbx), %rsi
	callq	llex
	movl	%eax, 32(%rbx)
	popq	%rbx
	retq
.Lfunc_end8:
	.size	luaX_lookahead, .Lfunc_end8-luaX_lookahead
	.cfi_endproc

	.p2align	4, 0x90
	.type	skip_sep,@function
skip_sep:                               # @skip_sep
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi53:
	.cfi_def_cfa_offset 160
.Lcfi54:
	.cfi_offset %rbx, -56
.Lcfi55:
	.cfi_offset %r12, -48
.Lcfi56:
	.cfi_offset %r13, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movabsq	$9223372036854775805, %r15 # imm = 0x7FFFFFFFFFFFFFFD
	movl	(%rbx), %r12d
	movq	72(%rbx), %rbp
	movq	8(%rbp), %rcx
	movq	16(%rbp), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB9_1
# BB#2:
	movq	%r15, %r13
	cmpq	%r15, %rdx
	jbe	.LBB9_3
# BB#4:
	movq	80(%rbx), %rsi
	addq	$24, %rsi
	leaq	16(%rsp), %r14
	movl	$80, %edx
	movq	%r14, %rdi
	callq	luaO_chunkid
	leaq	56(%rbx), %r15
	movq	56(%rbx), %rdi
	movl	4(%rbx), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	luaO_pushfstring
	movq	56(%rbx), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbp), %rdx
	jmp	.LBB9_5
.LBB9_1:                                # %._crit_edge.i
	movq	(%rbp), %rax
	jmp	.LBB9_9
.LBB9_3:                                # %._crit_edge29
	leaq	56(%rbx), %r15
.LBB9_5:
	leaq	(%rdx,%rdx), %r14
	cmpq	$-2, %r14
	movq	(%r15), %rdi
	movq	%r13, %r15
	jne	.LBB9_6
# BB#7:
	callq	luaM_toobig
	jmp	.LBB9_8
.LBB9_6:
	movq	(%rbp), %rsi
	movq	%r14, %rcx
	callq	luaM_realloc_
.LBB9_8:
	movq	%rax, (%rbp)
	movq	%r14, 16(%rbp)
	movq	8(%rbp), %rcx
.LBB9_9:                                # %save.exit
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbp)
	movb	%r12b, (%rax,%rcx)
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	movq	%r15, %r14
	je	.LBB9_11
# BB#10:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB9_12
.LBB9_11:
	callq	luaZ_fill
.LBB9_12:
	movl	%eax, (%rbx)
	xorl	%r15d, %r15d
	cmpl	$61, %eax
	movl	$0, %r12d
	jne	.LBB9_26
# BB#13:                                # %.lr.ph
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB9_14:                               # =>This Inner Loop Header: Depth=1
	movq	72(%rbx), %r13
	movq	8(%r13), %rcx
	movq	16(%r13), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB9_15
# BB#16:                                #   in Loop: Header=BB9_14 Depth=1
	cmpq	%r14, %rdx
	jbe	.LBB9_18
# BB#17:                                #   in Loop: Header=BB9_14 Depth=1
	movq	80(%rbx), %rsi
	addq	$24, %rsi
	movl	$80, %edx
	leaq	16(%rsp), %rbp
	movq	%rbp, %rdi
	callq	luaO_chunkid
	movq	56(%rbx), %rdi
	movl	4(%rbx), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	luaO_pushfstring
	movq	56(%rbx), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%r13), %rdx
.LBB9_18:                               #   in Loop: Header=BB9_14 Depth=1
	leaq	(%rdx,%rdx), %rbp
	cmpq	$-2, %rbp
	movq	56(%rbx), %rdi
	jne	.LBB9_19
# BB#20:                                #   in Loop: Header=BB9_14 Depth=1
	callq	luaM_toobig
	jmp	.LBB9_21
	.p2align	4, 0x90
.LBB9_15:                               # %._crit_edge.i19
                                        #   in Loop: Header=BB9_14 Depth=1
	movq	(%r13), %rax
	jmp	.LBB9_22
	.p2align	4, 0x90
.LBB9_19:                               #   in Loop: Header=BB9_14 Depth=1
	movq	(%r13), %rsi
	movq	%rbp, %rcx
	callq	luaM_realloc_
.LBB9_21:                               #   in Loop: Header=BB9_14 Depth=1
	movq	%rax, (%r13)
	movq	%rbp, 16(%r13)
	movq	8(%r13), %rcx
.LBB9_22:                               # %save.exit24
                                        #   in Loop: Header=BB9_14 Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%r13)
	movb	$61, (%rax,%rcx)
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB9_24
# BB#23:                                #   in Loop: Header=BB9_14 Depth=1
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB9_25
	.p2align	4, 0x90
.LBB9_24:                               #   in Loop: Header=BB9_14 Depth=1
	callq	luaZ_fill
.LBB9_25:                               #   in Loop: Header=BB9_14 Depth=1
	movl	%eax, (%rbx)
	incl	%r12d
	cmpl	$61, %eax
	je	.LBB9_14
.LBB9_26:                               # %._crit_edge
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	movl	$-1, %eax
	cmovnel	%eax, %r15d
	xorl	%r12d, %r15d
	movl	%r15d, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	skip_sep, .Lfunc_end9-skip_sep
	.cfi_endproc

	.p2align	4, 0x90
	.type	read_long_string,@function
read_long_string:                       # @read_long_string
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi64:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi66:
	.cfi_def_cfa_offset 160
.Lcfi67:
	.cfi_offset %rbx, -56
.Lcfi68:
	.cfi_offset %r12, -48
.Lcfi69:
	.cfi_offset %r13, -40
.Lcfi70:
	.cfi_offset %r14, -32
.Lcfi71:
	.cfi_offset %r15, -24
.Lcfi72:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %r13
	movabsq	$9223372036854775805, %r15 # imm = 0x7FFFFFFFFFFFFFFD
	movl	(%r13), %r12d
	movq	72(%r13), %rbx
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB10_1
# BB#2:
	cmpq	%r15, %rdx
	jbe	.LBB10_3
# BB#4:
	movq	80(%r13), %rsi
	addq	$24, %rsi
	leaq	16(%rsp), %rbp
	movl	$80, %edx
	movq	%rbp, %rdi
	callq	luaO_chunkid
	leaq	56(%r13), %r14
	movq	56(%r13), %rdi
	movl	4(%r13), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	luaO_pushfstring
	movq	56(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbx), %rdx
	jmp	.LBB10_5
.LBB10_1:                               # %._crit_edge.i
	movq	(%rbx), %rax
	jmp	.LBB10_9
.LBB10_3:                               # %._crit_edge
	leaq	56(%r13), %r14
.LBB10_5:
	leaq	(%rdx,%rdx), %rbp
	cmpq	$-2, %rbp
	movq	(%r14), %rdi
	jne	.LBB10_6
# BB#7:
	callq	luaM_toobig
	jmp	.LBB10_8
.LBB10_6:
	movq	(%rbx), %rsi
	movq	%rbp, %rcx
	callq	luaM_realloc_
.LBB10_8:
	movq	%rax, (%rbx)
	movq	%rbp, 16(%rbx)
	movq	8(%rbx), %rcx
.LBB10_9:                               # %save.exit
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbx)
	movb	%r12b, (%rax,%rcx)
	movq	64(%r13), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB10_11
# BB#10:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %ebp
	jmp	.LBB10_12
.LBB10_11:
	callq	luaZ_fill
	movl	%eax, %ebp
.LBB10_12:
	movl	%ebp, (%r13)
	cmpl	$13, %ebp
	je	.LBB10_14
# BB#13:
	cmpl	$10, %ebp
	jne	.LBB10_27
.LBB10_14:
	movq	64(%r13), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB10_16
# BB#15:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB10_17
.LBB10_16:
	callq	luaZ_fill
.LBB10_17:
	movl	%eax, (%r13)
	cmpl	$13, %eax
	je	.LBB10_19
# BB#18:
	cmpl	$10, %eax
	jne	.LBB10_24
.LBB10_19:
	cmpl	%ebp, %eax
	je	.LBB10_24
# BB#20:
	movq	64(%r13), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB10_22
# BB#21:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB10_23
.LBB10_22:
	callq	luaZ_fill
.LBB10_23:
	movl	%eax, (%r13)
.LBB10_24:
	movl	4(%r13), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 4(%r13)
	cmpl	$2147483644, %eax       # imm = 0x7FFFFFFC
	jl	.LBB10_26
# BB#25:
	movl	16(%r13), %edx
	movl	$.L.str.38, %esi
	movq	%r13, %rdi
	callq	luaX_lexerror
.LBB10_26:                              # %inclinenumber.exitthread-pre-split
	movl	(%r13), %ebp
.LBB10_27:                              # %inclinenumber.exit.preheader
	leaq	16(%rsp), %r12
.LBB10_28:                              # %inclinenumber.exit.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_40 Depth 2
                                        #     Child Loop BB10_29 Depth 2
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	jne	.LBB10_40
	jmp	.LBB10_29
	.p2align	4, 0x90
.LBB10_39:                              # %inclinenumber.exit
                                        #   in Loop: Header=BB10_40 Depth=2
	movl	%ebp, (%r13)
.LBB10_40:                              # %inclinenumber.exit
                                        #   Parent Loop BB10_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$12, %ebp
	jle	.LBB10_41
# BB#45:                                # %inclinenumber.exit
                                        #   in Loop: Header=BB10_40 Depth=2
	cmpl	$13, %ebp
	je	.LBB10_43
# BB#46:                                # %inclinenumber.exit
                                        #   in Loop: Header=BB10_40 Depth=2
	cmpl	$91, %ebp
	je	.LBB10_53
# BB#47:                                # %inclinenumber.exit
                                        #   in Loop: Header=BB10_40 Depth=2
	cmpl	$93, %ebp
	jne	.LBB10_101
	jmp	.LBB10_48
	.p2align	4, 0x90
.LBB10_41:                              # %inclinenumber.exit
                                        #   in Loop: Header=BB10_40 Depth=2
	cmpl	$-1, %ebp
	je	.LBB10_51
# BB#42:                                # %inclinenumber.exit
                                        #   in Loop: Header=BB10_40 Depth=2
	cmpl	$10, %ebp
	je	.LBB10_43
.LBB10_101:                             #   in Loop: Header=BB10_40 Depth=2
	movq	72(%r13), %r14
	movq	8(%r14), %rcx
	movq	16(%r14), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB10_102
# BB#103:                               #   in Loop: Header=BB10_40 Depth=2
	cmpq	%r15, %rdx
	jbe	.LBB10_105
# BB#104:                               #   in Loop: Header=BB10_40 Depth=2
	movq	80(%r13), %rsi
	addq	$24, %rsi
	movl	$80, %edx
	movq	%r12, %rdi
	callq	luaO_chunkid
	movq	56(%r13), %rdi
	movl	4(%r13), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	luaO_pushfstring
	movq	56(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%r14), %rdx
.LBB10_105:                             #   in Loop: Header=BB10_40 Depth=2
	leaq	(%rdx,%rdx), %rbx
	cmpq	$-2, %rbx
	movq	56(%r13), %rdi
	jne	.LBB10_106
# BB#107:                               #   in Loop: Header=BB10_40 Depth=2
	callq	luaM_toobig
	jmp	.LBB10_108
	.p2align	4, 0x90
.LBB10_102:                             # %._crit_edge.i77
                                        #   in Loop: Header=BB10_40 Depth=2
	movq	(%r14), %rax
	jmp	.LBB10_109
	.p2align	4, 0x90
.LBB10_106:                             #   in Loop: Header=BB10_40 Depth=2
	movq	(%r14), %rsi
	movq	%rbx, %rcx
	callq	luaM_realloc_
.LBB10_108:                             #   in Loop: Header=BB10_40 Depth=2
	movq	%rax, (%r14)
	movq	%rbx, 16(%r14)
	movq	8(%r14), %rcx
.LBB10_109:                             # %save.exit82
                                        #   in Loop: Header=BB10_40 Depth=2
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%r14)
	movb	%bpl, (%rax,%rcx)
	movq	64(%r13), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB10_38
# BB#110:                               #   in Loop: Header=BB10_40 Depth=2
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %ebp
	jmp	.LBB10_39
	.p2align	4, 0x90
.LBB10_38:                              #   in Loop: Header=BB10_40 Depth=2
	callq	luaZ_fill
	movl	%eax, %ebp
	jmp	.LBB10_39
	.p2align	4, 0x90
.LBB10_36:                              #   in Loop: Header=BB10_29 Depth=2
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %ebp
	movl	%ebp, (%r13)
.LBB10_29:                              # %inclinenumber.exit.us
                                        #   Parent Loop BB10_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$12, %ebp
	jg	.LBB10_32
.LBB10_30:                              # %inclinenumber.exit.us
                                        #   in Loop: Header=BB10_29 Depth=2
	cmpl	$-1, %ebp
	je	.LBB10_51
# BB#31:                                # %inclinenumber.exit.us
                                        #   in Loop: Header=BB10_29 Depth=2
	cmpl	$10, %ebp
	jne	.LBB10_35
	jmp	.LBB10_43
	.p2align	4, 0x90
.LBB10_32:                              # %inclinenumber.exit.us
                                        #   in Loop: Header=BB10_29 Depth=2
	cmpl	$13, %ebp
	je	.LBB10_43
# BB#33:                                # %inclinenumber.exit.us
                                        #   in Loop: Header=BB10_29 Depth=2
	cmpl	$91, %ebp
	je	.LBB10_53
# BB#34:                                # %inclinenumber.exit.us
                                        #   in Loop: Header=BB10_29 Depth=2
	cmpl	$93, %ebp
	je	.LBB10_48
.LBB10_35:                              #   in Loop: Header=BB10_29 Depth=2
	movq	64(%r13), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	jne	.LBB10_36
# BB#37:                                #   in Loop: Header=BB10_29 Depth=2
	callq	luaZ_fill
	movl	%eax, %ebp
	movl	%ebp, (%r13)
	cmpl	$12, %ebp
	jg	.LBB10_32
	jmp	.LBB10_30
	.p2align	4, 0x90
.LBB10_53:                              # %.us-lcssa90.us
                                        #   in Loop: Header=BB10_28 Depth=1
	movq	%r13, %rdi
	callq	skip_sep
	cmpl	8(%rsp), %eax           # 4-byte Folded Reload
	jne	.LBB10_26
# BB#54:                                #   in Loop: Header=BB10_28 Depth=1
	movl	(%r13), %r14d
	movq	72(%r13), %rbx
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB10_55
# BB#56:                                #   in Loop: Header=BB10_28 Depth=1
	cmpq	%r15, %rdx
	jbe	.LBB10_58
# BB#57:                                #   in Loop: Header=BB10_28 Depth=1
	movq	80(%r13), %rsi
	addq	$24, %rsi
	movl	$80, %edx
	movq	%r12, %rdi
	callq	luaO_chunkid
	movq	56(%r13), %rdi
	movl	4(%r13), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	luaO_pushfstring
	movq	56(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbx), %rdx
.LBB10_58:                              #   in Loop: Header=BB10_28 Depth=1
	leaq	(%rdx,%rdx), %rbp
	cmpq	$-2, %rbp
	movq	56(%r13), %rdi
	jne	.LBB10_59
# BB#60:                                #   in Loop: Header=BB10_28 Depth=1
	callq	luaM_toobig
	jmp	.LBB10_61
.LBB10_55:                              # %._crit_edge.i52
                                        #   in Loop: Header=BB10_28 Depth=1
	movq	(%rbx), %rax
	jmp	.LBB10_62
.LBB10_59:                              #   in Loop: Header=BB10_28 Depth=1
	movq	(%rbx), %rsi
	movq	%rbp, %rcx
	callq	luaM_realloc_
.LBB10_61:                              #   in Loop: Header=BB10_28 Depth=1
	movq	%rax, (%rbx)
	movq	%rbp, 16(%rbx)
	movq	8(%rbx), %rcx
.LBB10_62:                              # %save.exit57
                                        #   in Loop: Header=BB10_28 Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbx)
	movb	%r14b, (%rax,%rcx)
	movq	64(%r13), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB10_64
# BB#63:                                #   in Loop: Header=BB10_28 Depth=1
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %ebp
	jmp	.LBB10_65
.LBB10_64:                              #   in Loop: Header=BB10_28 Depth=1
	callq	luaZ_fill
	movl	%eax, %ebp
.LBB10_65:                              #   in Loop: Header=BB10_28 Depth=1
	movl	%ebp, (%r13)
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB10_28
# BB#66:
	movq	80(%r13), %rsi
	addq	$24, %rsi
	leaq	16(%rsp), %rbx
	movl	$80, %edx
	movq	%rbx, %rdi
	callq	luaO_chunkid
	movq	56(%r13), %rdi
	movl	4(%r13), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.41, %r8d
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	luaO_pushfstring
	movq	%rax, %rbx
	movq	56(%r13), %rbp
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzwl	182(%rax), %eax
	movl	$.L.str.31, %ecx
	movl	$.L.str.32, %esi
	testb	$2, %al
	cmovneq	%rcx, %rsi
	movl	$91, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	luaO_pushfstring
	movq	%rax, %rcx
	movl	$.L.str.34, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	jmp	.LBB10_52
.LBB10_43:                              # %.us-lcssa92.us
	movq	72(%r13), %rbx
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB10_44
# BB#81:
	cmpq	%r15, %rdx
	jbe	.LBB10_83
# BB#82:
	movq	80(%r13), %rsi
	addq	$24, %rsi
	leaq	16(%rsp), %rbp
	movl	$80, %edx
	movq	%rbp, %rdi
	callq	luaO_chunkid
	movq	56(%r13), %rdi
	movl	4(%r13), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	luaO_pushfstring
	movq	56(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbx), %rdx
.LBB10_83:
	leaq	(%rdx,%rdx), %rbp
	cmpq	$-2, %rbp
	movq	56(%r13), %rdi
	jne	.LBB10_84
# BB#85:
	callq	luaM_toobig
	jmp	.LBB10_86
.LBB10_51:                              # %.us-lcssa.us
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	movl	$.L.str.39, %eax
	movl	$.L.str.40, %ebx
	cmovneq	%rax, %rbx
	movq	80(%r13), %rsi
	addq	$24, %rsi
	leaq	16(%rsp), %rbp
	movl	$80, %edx
	movq	%rbp, %rdi
	callq	luaO_chunkid
	movq	56(%r13), %rdi
	movl	4(%r13), %ecx
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	movq	%rbx, %r8
	callq	luaO_pushfstring
	movq	%rax, %rdx
	movq	56(%r13), %rdi
	movl	$.L.str.34, %esi
	movl	$.L.str.30, %ecx
	xorl	%eax, %eax
.LBB10_52:                              # %inclinenumber.exitthread-pre-split
	callq	luaO_pushfstring
	movq	56(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
	jmp	.LBB10_26
.LBB10_48:                              # %.us-lcssa91.us
	movq	%r13, %rdi
	callq	skip_sep
	cmpl	8(%rsp), %eax           # 4-byte Folded Reload
	jne	.LBB10_26
# BB#49:
	movl	(%r13), %r12d
	movq	72(%r13), %rbx
	movq	8(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB10_50
# BB#67:
	cmpq	%r15, %rdx
	jbe	.LBB10_69
# BB#68:
	movq	80(%r13), %rsi
	addq	$24, %rsi
	leaq	16(%rsp), %rbp
	movl	$80, %edx
	movq	%rbp, %rdi
	callq	luaO_chunkid
	movq	56(%r13), %rdi
	movl	4(%r13), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	luaO_pushfstring
	movq	56(%r13), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbx), %rdx
.LBB10_69:
	leaq	(%rdx,%rdx), %rbp
	cmpq	$-2, %rbp
	movq	56(%r13), %rdi
	jne	.LBB10_70
# BB#71:
	callq	luaM_toobig
	jmp	.LBB10_72
.LBB10_44:                              # %._crit_edge.i68
	movq	(%rbx), %rax
	jmp	.LBB10_87
.LBB10_84:
	movq	(%rbx), %rsi
	movq	%rbp, %rcx
	callq	luaM_realloc_
.LBB10_86:
	movq	%rax, (%rbx)
	movq	%rbp, 16(%rbx)
	movq	8(%rbx), %rcx
.LBB10_87:                              # %save.exit73
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbx)
	movb	$10, (%rax,%rcx)
	movl	(%r13), %ebx
	movq	64(%r13), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB10_89
# BB#88:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB10_90
.LBB10_89:
	callq	luaZ_fill
.LBB10_90:
	movl	%eax, (%r13)
	cmpl	$13, %eax
	je	.LBB10_92
# BB#91:
	cmpl	$10, %eax
	jne	.LBB10_97
.LBB10_92:
	cmpl	%ebx, %eax
	je	.LBB10_97
# BB#93:
	movq	64(%r13), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB10_95
# BB#94:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB10_96
.LBB10_50:                              # %._crit_edge.i60
	movq	(%rbx), %rax
	jmp	.LBB10_73
.LBB10_95:
	callq	luaZ_fill
.LBB10_96:
	movl	%eax, (%r13)
.LBB10_97:
	movl	4(%r13), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 4(%r13)
	cmpl	$2147483644, %eax       # imm = 0x7FFFFFFC
	jl	.LBB10_99
# BB#98:
	movl	16(%r13), %edx
	movl	$.L.str.38, %esi
	movq	%r13, %rdi
	callq	luaX_lexerror
.LBB10_99:                              # %inclinenumber.exit74
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	jne	.LBB10_26
# BB#100:
	movq	72(%r13), %rax
	movq	$0, 8(%rax)
	jmp	.LBB10_26
.LBB10_70:
	movq	(%rbx), %rsi
	movq	%rbp, %rcx
	callq	luaM_realloc_
.LBB10_72:
	movq	%rax, (%rbx)
	movq	%rbp, 16(%rbx)
	movq	8(%rbx), %rcx
.LBB10_73:                              # %save.exit65
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbx)
	movb	%r12b, (%rax,%rcx)
	movq	64(%r13), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB10_75
# BB#74:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.LBB10_76
.LBB10_75:
	callq	luaZ_fill
.LBB10_76:
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	movl	%eax, (%r13)
	je	.LBB10_80
# BB#77:
	movq	56(%r13), %rbp
	movq	72(%r13), %rax
	movq	8(%rsp), %rdi           # 8-byte Reload
	movslq	%edi, %rcx
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	leaq	2(%rsi,%rcx), %rsi
	leal	4(%rdi,%rdi), %eax
	cltq
	subq	%rax, %rdx
	movq	%rbp, %rdi
	callq	luaS_newlstr
	movq	%rax, %rbx
	movq	48(%r13), %rax
	movq	8(%rax), %rsi
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	luaH_setstr
	cmpl	$0, 8(%rax)
	jne	.LBB10_79
# BB#78:
	movl	$1, (%rax)
	movl	$1, 8(%rax)
.LBB10_79:                              # %luaX_newstring.exit
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rbx, (%rax)
.LBB10_80:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	read_long_string, .Lfunc_end10-read_long_string
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
.LCPI11_1:
	.short	46                      # 0x2e
	.short	46                      # 0x2e
	.short	46                      # 0x2e
	.short	46                      # 0x2e
	.short	46                      # 0x2e
	.short	46                      # 0x2e
	.short	46                      # 0x2e
	.short	46                      # 0x2e
	.text
	.p2align	4, 0x90
	.type	read_numeral,@function
read_numeral:                           # @read_numeral
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 56
	subq	$888, %rsp              # imm = 0x378
.Lcfi79:
	.cfi_def_cfa_offset 944
.Lcfi80:
	.cfi_offset %rbx, -56
.Lcfi81:
	.cfi_offset %r12, -48
.Lcfi82:
	.cfi_offset %r13, -40
.Lcfi83:
	.cfi_offset %r14, -32
.Lcfi84:
	.cfi_offset %r15, -24
.Lcfi85:
	.cfi_offset %rbp, -16
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r15
	movl	(%r15), %r14d
	leaq	800(%rsp), %r12
	.p2align	4, 0x90
.LBB11_1:                               # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	movq	72(%r15), %rbp
	movq	8(%rbp), %rcx
	movq	16(%rbp), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB11_6
# BB#2:                                 #   in Loop: Header=BB11_1 Depth=1
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	cmpq	%rax, %rdx
	jbe	.LBB11_4
# BB#3:                                 #   in Loop: Header=BB11_1 Depth=1
	movq	80(%r15), %rsi
	addq	$24, %rsi
	movl	$80, %edx
	movq	%r12, %rdi
	callq	luaO_chunkid
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbp), %rdx
.LBB11_4:                               #   in Loop: Header=BB11_1 Depth=1
	leaq	(%rdx,%rdx), %rbx
	cmpq	$-2, %rbx
	movq	56(%r15), %rdi
	jne	.LBB11_7
# BB#5:                                 #   in Loop: Header=BB11_1 Depth=1
	callq	luaM_toobig
	jmp	.LBB11_8
	.p2align	4, 0x90
.LBB11_6:                               # %._crit_edge.i
                                        #   in Loop: Header=BB11_1 Depth=1
	movq	(%rbp), %rax
	jmp	.LBB11_9
	.p2align	4, 0x90
.LBB11_7:                               #   in Loop: Header=BB11_1 Depth=1
	movq	(%rbp), %rsi
	movq	%rbx, %rcx
	callq	luaM_realloc_
.LBB11_8:                               #   in Loop: Header=BB11_1 Depth=1
	movq	%rax, (%rbp)
	movq	%rbx, 16(%rbp)
	movq	8(%rbp), %rcx
.LBB11_9:                               # %save.exit
                                        #   in Loop: Header=BB11_1 Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbp)
	movb	%r14b, (%rax,%rcx)
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB11_11
# BB#10:                                #   in Loop: Header=BB11_1 Depth=1
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %r14d
	jmp	.LBB11_12
	.p2align	4, 0x90
.LBB11_11:                              #   in Loop: Header=BB11_1 Depth=1
	callq	luaZ_fill
	movl	%eax, %r14d
.LBB11_12:                              #   in Loop: Header=BB11_1 Depth=1
	movl	%r14d, (%r15)
	callq	__ctype_b_loc
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movslq	%r14d, %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$2048, %eax             # imm = 0x800
	cmpl	$46, %ecx
	je	.LBB11_1
# BB#13:                                #   in Loop: Header=BB11_1 Depth=1
	testw	%ax, %ax
	jne	.LBB11_1
# BB#14:
	movl	$.L.str.44, %edi
	movl	$3, %edx
	movl	%r14d, %esi
	callq	memchr
	testq	%rax, %rax
	je	.LBB11_40
# BB#15:
	movq	72(%r15), %rbp
	movq	8(%rbp), %rcx
	movq	16(%rbp), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB11_20
# BB#16:
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	cmpq	%rax, %rdx
	jbe	.LBB11_18
# BB#17:
	movq	80(%r15), %rsi
	addq	$24, %rsi
	leaq	800(%rsp), %r12
	movl	$80, %edx
	movq	%r12, %rdi
	callq	luaO_chunkid
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbp), %rdx
.LBB11_18:
	leaq	(%rdx,%rdx), %r12
	cmpq	$-2, %r12
	movq	56(%r15), %rdi
	jne	.LBB11_21
# BB#19:
	callq	luaM_toobig
	jmp	.LBB11_22
.LBB11_20:                              # %._crit_edge.i47
	movq	(%rbp), %rax
	jmp	.LBB11_23
.LBB11_21:
	movq	(%rbp), %rsi
	movq	%r12, %rcx
	callq	luaM_realloc_
.LBB11_22:
	movq	%rax, (%rbp)
	movq	%r12, 16(%rbp)
	movq	8(%rbp), %rcx
.LBB11_23:                              # %save.exit52
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbp)
	movb	%r14b, (%rax,%rcx)
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB11_25
# BB#24:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %r14d
	jmp	.LBB11_26
.LBB11_25:
	callq	luaZ_fill
	movl	%eax, %r14d
.LBB11_26:
	movl	%r14d, (%r15)
	movl	$1, %eax
	movl	%r14d, %ecx
	shlq	%cl, %rax
	cmpl	$63, %r14d
	ja	.LBB11_40
# BB#27:
	movabsq	$43980465111041, %rcx   # imm = 0x280000000001
	andq	%rcx, %rax
	je	.LBB11_40
# BB#28:
	movq	72(%r15), %rbp
	movq	8(%rbp), %rcx
	movq	16(%rbp), %rdx
	leaq	1(%rcx), %rax
	cmpq	%rdx, %rax
	jbe	.LBB11_33
# BB#29:
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	cmpq	%rax, %rdx
	jbe	.LBB11_31
# BB#30:
	movq	80(%r15), %rsi
	addq	$24, %rsi
	leaq	800(%rsp), %r12
	movl	$80, %edx
	movq	%r12, %rdi
	callq	luaO_chunkid
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	16(%rbp), %rdx
.LBB11_31:
	leaq	(%rdx,%rdx), %r12
	cmpq	$-2, %r12
	movq	56(%r15), %rdi
	jne	.LBB11_34
# BB#32:
	callq	luaM_toobig
	jmp	.LBB11_35
.LBB11_33:                              # %._crit_edge.i55
	movq	(%rbp), %rax
	jmp	.LBB11_36
.LBB11_34:
	movq	(%rbp), %rsi
	movq	%r12, %rcx
	callq	luaM_realloc_
.LBB11_35:
	movq	%rax, (%rbp)
	movq	%r12, 16(%rbp)
	movq	8(%rbp), %rcx
.LBB11_36:                              # %save.exit60
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rbp)
	movb	%r14b, (%rax,%rcx)
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB11_38
# BB#37:
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %r14d
	jmp	.LBB11_39
.LBB11_38:
	callq	luaZ_fill
	movl	%eax, %r14d
.LBB11_39:
	movl	%r14d, (%r15)
.LBB11_40:                              # %check_next.exit24.preheader
	movq	72(%r15), %r12
	leaq	8(%r12), %rbp
	movq	8(%r12), %rcx
	movq	16(%r12), %rdx
	leaq	1(%rcx), %rax
	leaq	16(%r12), %r13
	cmpq	%rax, %rdx
	setb	%al
	cmpl	$95, %r14d
	je	.LBB11_42
# BB#41:                                # %check_next.exit24.preheader
	movq	(%rbx), %rsi
	movslq	%r14d, %rdi
	movzwl	(%rsi,%rdi,2), %esi
	andl	$8, %esi
	testw	%si, %si
	je	.LBB11_56
.LBB11_42:                              # %.lr.ph
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB11_43:                              # =>This Inner Loop Header: Depth=1
	testb	$1, %al
	je	.LBB11_48
# BB#44:                                #   in Loop: Header=BB11_43 Depth=1
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	cmpq	%rax, %rdx
	jbe	.LBB11_46
# BB#45:                                #   in Loop: Header=BB11_43 Depth=1
	movq	%r15, %rbp
	movq	80(%rbp), %rsi
	addq	$24, %rsi
	movl	$80, %edx
	leaq	800(%rsp), %rbx
	movq	%rbx, %rdi
	callq	luaO_chunkid
	movq	56(%rbp), %rdi
	movl	4(%rbp), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	luaO_pushfstring
	movq	56(%rbp), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	(%r13), %rdx
.LBB11_46:                              #   in Loop: Header=BB11_43 Depth=1
	leaq	(%rdx,%rdx), %rbx
	cmpq	$-2, %rbx
	movq	56(%r15), %rdi
	jne	.LBB11_49
# BB#47:                                #   in Loop: Header=BB11_43 Depth=1
	callq	luaM_toobig
	jmp	.LBB11_50
	.p2align	4, 0x90
.LBB11_48:                              # %._crit_edge.i27
                                        #   in Loop: Header=BB11_43 Depth=1
	movq	(%r12), %rax
	jmp	.LBB11_51
	.p2align	4, 0x90
.LBB11_49:                              #   in Loop: Header=BB11_43 Depth=1
	movq	(%r12), %rsi
	movq	%rbx, %rcx
	callq	luaM_realloc_
.LBB11_50:                              #   in Loop: Header=BB11_43 Depth=1
	movq	%rax, (%r12)
	movq	%rbx, (%r13)
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rcx
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB11_51:                              # %save.exit32
                                        #   in Loop: Header=BB11_43 Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rbp)
	movb	%r14b, (%rax,%rcx)
	movq	64(%r15), %rdi
	movq	(%rdi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	je	.LBB11_53
# BB#52:                                #   in Loop: Header=BB11_43 Depth=1
	movq	8(%rdi), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movzbl	(%rax), %r14d
	jmp	.LBB11_54
	.p2align	4, 0x90
.LBB11_53:                              #   in Loop: Header=BB11_43 Depth=1
	callq	luaZ_fill
	movl	%eax, %r14d
.LBB11_54:                              # %check_next.exit24
                                        #   in Loop: Header=BB11_43 Depth=1
	movl	%r14d, (%r15)
	movq	(%rbx), %rax
	movslq	%r14d, %rdi
	movzwl	(%rax,%rdi,2), %esi
	andl	$8, %esi
	movq	72(%r15), %r12
	leaq	8(%r12), %rbp
	movq	8(%r12), %rcx
	movq	16(%r12), %rdx
	leaq	1(%rcx), %rax
	leaq	16(%r12), %r13
	cmpq	%rax, %rdx
	setb	%al
	cmpl	$95, %edi
	je	.LBB11_43
# BB#55:                                # %check_next.exit24
                                        #   in Loop: Header=BB11_43 Depth=1
	testw	%si, %si
	jne	.LBB11_43
.LBB11_56:                              # %check_next.exit24._crit_edge
	testb	%al, %al
	je	.LBB11_61
# BB#57:
	movabsq	$9223372036854775805, %rax # imm = 0x7FFFFFFFFFFFFFFD
	cmpq	%rax, %rdx
	jbe	.LBB11_59
# BB#58:
	movq	80(%r15), %rsi
	addq	$24, %rsi
	leaq	800(%rsp), %rbx
	movl	$80, %edx
	movq	%rbx, %rdi
	callq	luaO_chunkid
	movq	56(%r15), %rdi
	movl	4(%r15), %ecx
	movl	$.L.str.33, %esi
	movl	$.L.str.35, %r8d
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	luaO_pushfstring
	movq	56(%r15), %rdi
	movl	$3, %esi
	callq	luaD_throw
	movq	(%r13), %rdx
.LBB11_59:
	leaq	(%rdx,%rdx), %rbx
	cmpq	$-2, %rbx
	movq	56(%r15), %rdi
	movq	8(%rsp), %r14           # 8-byte Reload
	jne	.LBB11_62
# BB#60:
	callq	luaM_toobig
	jmp	.LBB11_63
.LBB11_61:                              # %._crit_edge.i35
	movq	(%r12), %rax
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB11_64
.LBB11_62:
	movq	(%r12), %rsi
	movq	%rbx, %rcx
	callq	luaM_realloc_
.LBB11_63:
	movq	%rax, (%r12)
	movq	%rbx, (%r13)
	movq	(%rbp), %rcx
.LBB11_64:                              # %save.exit40
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rbp)
	movb	$0, (%rax,%rcx)
	movq	72(%r15), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB11_120
# BB#65:                                # %.lr.ph.i.preheader
	movb	88(%r15), %al
	cmpq	$16, %rdx
	jb	.LBB11_103
# BB#67:                                # %min.iters.checked
	movq	%rdx, %rsi
	andq	$-16, %rsi
	movq	%rdx, %rbx
	andq	$-16, %rbx
	je	.LBB11_103
# BB#68:                                # %vector.body.preheader
	movq	%rdx, %rcx
	subq	%rsi, %rcx
	leaq	-16(%rdi,%rdx), %rbp
	movdqa	.LCPI11_0(%rip), %xmm0  # xmm0 = [255,0,255,0,255,0,255,0,255,0,255,0,255,0,255,0]
	movdqa	.LCPI11_1(%rip), %xmm1  # xmm1 = [46,46,46,46,46,46,46,46]
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB11_69:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %xmm2          # xmm2 = mem[0],zero
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm3       # xmm3 = xmm2[0,1,2,3,7,6,5,4]
	pand	%xmm0, %xmm3
	movq	(%rbp), %xmm2           # xmm2 = mem[0],zero
	pcmpeqw	%xmm1, %xmm3
	movdqa	%xmm3, 784(%rsp)
	testb	$1, 784(%rsp)
	je	.LBB11_71
# BB#70:                                # %pred.store.if
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, 15(%rbp)
.LBB11_71:                              # %pred.store.continue
                                        #   in Loop: Header=BB11_69 Depth=1
	movdqa	%xmm3, 768(%rsp)
	testb	$1, 770(%rsp)
	je	.LBB11_73
# BB#72:                                # %pred.store.if109
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, 14(%rbp)
.LBB11_73:                              # %pred.store.continue110
                                        #   in Loop: Header=BB11_69 Depth=1
	movdqa	%xmm3, 752(%rsp)
	testb	$1, 756(%rsp)
	je	.LBB11_75
# BB#74:                                # %pred.store.if111
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, 13(%rbp)
.LBB11_75:                              # %pred.store.continue112
                                        #   in Loop: Header=BB11_69 Depth=1
	movdqa	%xmm3, 736(%rsp)
	testb	$1, 742(%rsp)
	je	.LBB11_77
# BB#76:                                # %pred.store.if113
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, 12(%rbp)
.LBB11_77:                              # %pred.store.continue114
                                        #   in Loop: Header=BB11_69 Depth=1
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	movdqa	%xmm3, 720(%rsp)
	testb	$1, 728(%rsp)
	je	.LBB11_79
# BB#78:                                # %pred.store.if115
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, 11(%rbp)
.LBB11_79:                              # %pred.store.continue116
                                        #   in Loop: Header=BB11_69 Depth=1
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	movdqa	%xmm3, 704(%rsp)
	testb	$1, 714(%rsp)
	je	.LBB11_81
# BB#80:                                # %pred.store.if117
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, 10(%rbp)
.LBB11_81:                              # %pred.store.continue118
                                        #   in Loop: Header=BB11_69 Depth=1
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	movdqa	%xmm3, 688(%rsp)
	testb	$1, 700(%rsp)
	je	.LBB11_83
# BB#82:                                # %pred.store.if119
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, 9(%rbp)
.LBB11_83:                              # %pred.store.continue120
                                        #   in Loop: Header=BB11_69 Depth=1
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	movdqa	%xmm3, 672(%rsp)
	testb	$1, 686(%rsp)
	je	.LBB11_85
# BB#84:                                # %pred.store.if121
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, 8(%rbp)
.LBB11_85:                              # %pred.store.continue122
                                        #   in Loop: Header=BB11_69 Depth=1
	pand	%xmm0, %xmm2
	pcmpeqw	%xmm1, %xmm2
	movdqa	%xmm2, 656(%rsp)
	testb	$1, 656(%rsp)
	je	.LBB11_87
# BB#86:                                # %pred.store.if123
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, 7(%rbp)
.LBB11_87:                              # %pred.store.continue124
                                        #   in Loop: Header=BB11_69 Depth=1
	movdqa	%xmm2, 640(%rsp)
	testb	$1, 642(%rsp)
	je	.LBB11_89
# BB#88:                                # %pred.store.if125
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, 6(%rbp)
.LBB11_89:                              # %pred.store.continue126
                                        #   in Loop: Header=BB11_69 Depth=1
	movdqa	%xmm2, 624(%rsp)
	testb	$1, 628(%rsp)
	je	.LBB11_91
# BB#90:                                # %pred.store.if127
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, 5(%rbp)
.LBB11_91:                              # %pred.store.continue128
                                        #   in Loop: Header=BB11_69 Depth=1
	movdqa	%xmm2, 608(%rsp)
	testb	$1, 614(%rsp)
	je	.LBB11_93
# BB#92:                                # %pred.store.if129
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, 4(%rbp)
.LBB11_93:                              # %pred.store.continue130
                                        #   in Loop: Header=BB11_69 Depth=1
	movdqa	%xmm2, 592(%rsp)
	testb	$1, 600(%rsp)
	je	.LBB11_95
# BB#94:                                # %pred.store.if131
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, 3(%rbp)
.LBB11_95:                              # %pred.store.continue132
                                        #   in Loop: Header=BB11_69 Depth=1
	movdqa	%xmm2, 576(%rsp)
	testb	$1, 586(%rsp)
	je	.LBB11_97
# BB#96:                                # %pred.store.if133
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, 2(%rbp)
.LBB11_97:                              # %pred.store.continue134
                                        #   in Loop: Header=BB11_69 Depth=1
	movdqa	%xmm2, 560(%rsp)
	testb	$1, 572(%rsp)
	je	.LBB11_99
# BB#98:                                # %pred.store.if135
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, 1(%rbp)
.LBB11_99:                              # %pred.store.continue136
                                        #   in Loop: Header=BB11_69 Depth=1
	movdqa	%xmm2, 544(%rsp)
	testb	$1, 558(%rsp)
	je	.LBB11_101
# BB#100:                               # %pred.store.if137
                                        #   in Loop: Header=BB11_69 Depth=1
	movb	%al, (%rbp)
.LBB11_101:                             # %pred.store.continue138
                                        #   in Loop: Header=BB11_69 Depth=1
	addq	$-16, %rbp
	addq	$-16, %rsi
	jne	.LBB11_69
# BB#102:                               # %middle.block
	cmpq	%rbx, %rdx
	jne	.LBB11_104
	jmp	.LBB11_119
.LBB11_103:
	movq	%rdx, %rcx
.LBB11_104:                             # %.lr.ph.i.preheader255
	leaq	-1(%rcx), %rdx
	movq	%rcx, %rsi
	andq	$3, %rsi
	je	.LBB11_109
# BB#105:                               # %.lr.ph.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB11_106:                             # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$46, -1(%rdi,%rcx)
	jne	.LBB11_108
# BB#107:                               #   in Loop: Header=BB11_106 Depth=1
	movb	%al, -1(%rdi,%rcx)
.LBB11_108:                             # %.backedge.i.prol
                                        #   in Loop: Header=BB11_106 Depth=1
	decq	%rcx
	incq	%rsi
	jne	.LBB11_106
.LBB11_109:                             # %.lr.ph.i.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB11_119
	.p2align	4, 0x90
.LBB11_110:                             # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$46, -1(%rdi,%rcx)
	jne	.LBB11_112
# BB#111:                               #   in Loop: Header=BB11_110 Depth=1
	movb	%al, -1(%rdi,%rcx)
.LBB11_112:                             # %.backedge.i
                                        #   in Loop: Header=BB11_110 Depth=1
	cmpb	$46, -2(%rdi,%rcx)
	jne	.LBB11_114
# BB#113:                               #   in Loop: Header=BB11_110 Depth=1
	movb	%al, -2(%rdi,%rcx)
.LBB11_114:                             # %.backedge.i.1
                                        #   in Loop: Header=BB11_110 Depth=1
	cmpb	$46, -3(%rdi,%rcx)
	jne	.LBB11_116
# BB#115:                               #   in Loop: Header=BB11_110 Depth=1
	movb	%al, -3(%rdi,%rcx)
.LBB11_116:                             # %.backedge.i.2
                                        #   in Loop: Header=BB11_110 Depth=1
	leaq	-4(%rcx), %rdx
	cmpb	$46, -4(%rdi,%rcx)
	jne	.LBB11_118
# BB#117:                               #   in Loop: Header=BB11_110 Depth=1
	movb	%al, -4(%rdi,%rcx)
.LBB11_118:                             # %.backedge.i.3
                                        #   in Loop: Header=BB11_110 Depth=1
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB11_110
.LBB11_119:                             # %buffreplace.exit.loopexit
	movq	72(%r15), %rax
	movq	(%rax), %rdi
.LBB11_120:                             # %buffreplace.exit
	movq	%r14, %rsi
	callq	luaO_str2d
	testl	%eax, %eax
	jne	.LBB11_237
# BB#121:
	callq	localeconv
	movb	88(%r15), %cl
	testq	%rax, %rax
	je	.LBB11_123
# BB#122:
	movq	(%rax), %rax
	movb	(%rax), %al
	jmp	.LBB11_124
.LBB11_123:
	movb	$46, %al
.LBB11_124:
	movb	%al, 88(%r15)
	movq	72(%r15), %rdx
	movq	(%rdx), %rdi
	movq	8(%rdx), %r9
	testq	%r9, %r9
	je	.LBB11_180
# BB#125:                               # %.lr.ph.i.i.preheader
	cmpq	$16, %r9
	jb	.LBB11_163
# BB#127:                               # %min.iters.checked143
	movq	%r9, %rdx
	andq	$-16, %rdx
	movq	%r9, %r8
	andq	$-16, %r8
	je	.LBB11_163
# BB#128:                               # %vector.ph147
	movq	%r9, %rsi
	subq	%rdx, %rsi
	movzbl	%cl, %edx
	movd	%edx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	-16(%rdi,%r9), %rbx
	movdqa	.LCPI11_0(%rip), %xmm1  # xmm1 = [255,0,255,0,255,0,255,0,255,0,255,0,255,0,255,0]
	pand	%xmm1, %xmm0
	movq	%r8, %rbp
	.p2align	4, 0x90
.LBB11_129:                             # %vector.body139
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %xmm2          # xmm2 = mem[0],zero
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm3       # xmm3 = xmm2[0,1,2,3,7,6,5,4]
	pand	%xmm1, %xmm3
	movq	(%rbx), %xmm2           # xmm2 = mem[0],zero
	pcmpeqw	%xmm0, %xmm3
	movdqa	%xmm3, 528(%rsp)
	testb	$1, 528(%rsp)
	je	.LBB11_131
# BB#130:                               # %pred.store.if164
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, 15(%rbx)
.LBB11_131:                             # %pred.store.continue165
                                        #   in Loop: Header=BB11_129 Depth=1
	movdqa	%xmm3, 512(%rsp)
	testb	$1, 514(%rsp)
	je	.LBB11_133
# BB#132:                               # %pred.store.if166
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, 14(%rbx)
.LBB11_133:                             # %pred.store.continue167
                                        #   in Loop: Header=BB11_129 Depth=1
	movdqa	%xmm3, 496(%rsp)
	testb	$1, 500(%rsp)
	je	.LBB11_135
# BB#134:                               # %pred.store.if168
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, 13(%rbx)
.LBB11_135:                             # %pred.store.continue169
                                        #   in Loop: Header=BB11_129 Depth=1
	movdqa	%xmm3, 480(%rsp)
	testb	$1, 486(%rsp)
	je	.LBB11_137
# BB#136:                               # %pred.store.if170
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, 12(%rbx)
.LBB11_137:                             # %pred.store.continue171
                                        #   in Loop: Header=BB11_129 Depth=1
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	movdqa	%xmm3, 464(%rsp)
	testb	$1, 472(%rsp)
	je	.LBB11_139
# BB#138:                               # %pred.store.if172
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, 11(%rbx)
.LBB11_139:                             # %pred.store.continue173
                                        #   in Loop: Header=BB11_129 Depth=1
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	movdqa	%xmm3, 448(%rsp)
	testb	$1, 458(%rsp)
	je	.LBB11_141
# BB#140:                               # %pred.store.if174
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, 10(%rbx)
.LBB11_141:                             # %pred.store.continue175
                                        #   in Loop: Header=BB11_129 Depth=1
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	movdqa	%xmm3, 432(%rsp)
	testb	$1, 444(%rsp)
	je	.LBB11_143
# BB#142:                               # %pred.store.if176
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, 9(%rbx)
.LBB11_143:                             # %pred.store.continue177
                                        #   in Loop: Header=BB11_129 Depth=1
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	movdqa	%xmm3, 416(%rsp)
	testb	$1, 430(%rsp)
	je	.LBB11_145
# BB#144:                               # %pred.store.if178
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, 8(%rbx)
.LBB11_145:                             # %pred.store.continue179
                                        #   in Loop: Header=BB11_129 Depth=1
	pand	%xmm1, %xmm2
	pcmpeqw	%xmm0, %xmm2
	movdqa	%xmm2, 400(%rsp)
	testb	$1, 400(%rsp)
	je	.LBB11_147
# BB#146:                               # %pred.store.if180
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, 7(%rbx)
.LBB11_147:                             # %pred.store.continue181
                                        #   in Loop: Header=BB11_129 Depth=1
	movdqa	%xmm2, 384(%rsp)
	testb	$1, 386(%rsp)
	je	.LBB11_149
# BB#148:                               # %pred.store.if182
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, 6(%rbx)
.LBB11_149:                             # %pred.store.continue183
                                        #   in Loop: Header=BB11_129 Depth=1
	movdqa	%xmm2, 368(%rsp)
	testb	$1, 372(%rsp)
	je	.LBB11_151
# BB#150:                               # %pred.store.if184
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, 5(%rbx)
.LBB11_151:                             # %pred.store.continue185
                                        #   in Loop: Header=BB11_129 Depth=1
	movdqa	%xmm2, 352(%rsp)
	testb	$1, 358(%rsp)
	je	.LBB11_153
# BB#152:                               # %pred.store.if186
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, 4(%rbx)
.LBB11_153:                             # %pred.store.continue187
                                        #   in Loop: Header=BB11_129 Depth=1
	movdqa	%xmm2, 336(%rsp)
	testb	$1, 344(%rsp)
	je	.LBB11_155
# BB#154:                               # %pred.store.if188
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, 3(%rbx)
.LBB11_155:                             # %pred.store.continue189
                                        #   in Loop: Header=BB11_129 Depth=1
	movdqa	%xmm2, 320(%rsp)
	testb	$1, 330(%rsp)
	je	.LBB11_157
# BB#156:                               # %pred.store.if190
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, 2(%rbx)
.LBB11_157:                             # %pred.store.continue191
                                        #   in Loop: Header=BB11_129 Depth=1
	movdqa	%xmm2, 304(%rsp)
	testb	$1, 316(%rsp)
	je	.LBB11_159
# BB#158:                               # %pred.store.if192
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, 1(%rbx)
.LBB11_159:                             # %pred.store.continue193
                                        #   in Loop: Header=BB11_129 Depth=1
	movdqa	%xmm2, 288(%rsp)
	testb	$1, 302(%rsp)
	je	.LBB11_161
# BB#160:                               # %pred.store.if194
                                        #   in Loop: Header=BB11_129 Depth=1
	movb	%al, (%rbx)
.LBB11_161:                             # %pred.store.continue195
                                        #   in Loop: Header=BB11_129 Depth=1
	addq	$-16, %rbx
	addq	$-16, %rbp
	jne	.LBB11_129
# BB#162:                               # %middle.block140
	cmpq	%r8, %r9
	jne	.LBB11_164
	jmp	.LBB11_179
.LBB11_163:
	movq	%r9, %rsi
.LBB11_164:                             # %.lr.ph.i.i.preheader254
	leaq	-1(%rsi), %rbp
	movq	%rsi, %rdx
	andq	$3, %rdx
	je	.LBB11_169
# BB#165:                               # %.lr.ph.i.i.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB11_166:                             # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	cmpb	%cl, -1(%rdi,%rsi)
	jne	.LBB11_168
# BB#167:                               #   in Loop: Header=BB11_166 Depth=1
	movb	%al, -1(%rdi,%rsi)
.LBB11_168:                             # %.backedge.i.i.prol
                                        #   in Loop: Header=BB11_166 Depth=1
	decq	%rsi
	incq	%rdx
	jne	.LBB11_166
.LBB11_169:                             # %.lr.ph.i.i.prol.loopexit
	cmpq	$3, %rbp
	jb	.LBB11_179
	.p2align	4, 0x90
.LBB11_170:                             # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	%cl, -1(%rdi,%rsi)
	jne	.LBB11_172
# BB#171:                               #   in Loop: Header=BB11_170 Depth=1
	movb	%al, -1(%rdi,%rsi)
.LBB11_172:                             # %.backedge.i.i
                                        #   in Loop: Header=BB11_170 Depth=1
	cmpb	%cl, -2(%rdi,%rsi)
	jne	.LBB11_174
# BB#173:                               #   in Loop: Header=BB11_170 Depth=1
	movb	%al, -2(%rdi,%rsi)
.LBB11_174:                             # %.backedge.i.i.1
                                        #   in Loop: Header=BB11_170 Depth=1
	cmpb	%cl, -3(%rdi,%rsi)
	jne	.LBB11_176
# BB#175:                               #   in Loop: Header=BB11_170 Depth=1
	movb	%al, -3(%rdi,%rsi)
.LBB11_176:                             # %.backedge.i.i.2
                                        #   in Loop: Header=BB11_170 Depth=1
	leaq	-4(%rsi), %rdx
	cmpb	%cl, -4(%rdi,%rsi)
	jne	.LBB11_178
# BB#177:                               #   in Loop: Header=BB11_170 Depth=1
	movb	%al, -4(%rdi,%rsi)
.LBB11_178:                             # %.backedge.i.i.3
                                        #   in Loop: Header=BB11_170 Depth=1
	testq	%rdx, %rdx
	movq	%rdx, %rsi
	jne	.LBB11_170
.LBB11_179:                             # %buffreplace.exit.loopexit.i
	movq	72(%r15), %rax
	movq	(%rax), %rdi
.LBB11_180:                             # %buffreplace.exit.i
	movq	%r14, %rsi
	callq	luaO_str2d
	testl	%eax, %eax
	jne	.LBB11_237
# BB#181:
	movq	72(%r15), %rcx
	movq	8(%rcx), %rsi
	testq	%rsi, %rsi
	je	.LBB11_236
# BB#182:                               # %.lr.ph.i16.i.preheader
	movb	88(%r15), %al
	movq	(%rcx), %rcx
	cmpq	$16, %rsi
	jb	.LBB11_220
# BB#184:                               # %min.iters.checked200
	movq	%rsi, %rdi
	andq	$-16, %rdi
	movq	%rsi, %rbx
	andq	$-16, %rbx
	je	.LBB11_220
# BB#185:                               # %vector.ph204
	movq	%rsi, %rdx
	subq	%rdi, %rdx
	movzbl	%al, %edi
	movd	%edi, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	-16(%rcx,%rsi), %rbp
	movdqa	.LCPI11_0(%rip), %xmm1  # xmm1 = [255,0,255,0,255,0,255,0,255,0,255,0,255,0,255,0]
	pand	%xmm1, %xmm0
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB11_186:                             # %vector.body196
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %xmm2          # xmm2 = mem[0],zero
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm3       # xmm3 = xmm2[0,1,2,3,7,6,5,4]
	pand	%xmm1, %xmm3
	movq	(%rbp), %xmm2           # xmm2 = mem[0],zero
	pcmpeqw	%xmm0, %xmm3
	movdqa	%xmm3, 272(%rsp)
	testb	$1, 272(%rsp)
	je	.LBB11_188
# BB#187:                               # %pred.store.if221
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, 15(%rbp)
.LBB11_188:                             # %pred.store.continue222
                                        #   in Loop: Header=BB11_186 Depth=1
	movdqa	%xmm3, 256(%rsp)
	testb	$1, 258(%rsp)
	je	.LBB11_190
# BB#189:                               # %pred.store.if223
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, 14(%rbp)
.LBB11_190:                             # %pred.store.continue224
                                        #   in Loop: Header=BB11_186 Depth=1
	movdqa	%xmm3, 240(%rsp)
	testb	$1, 244(%rsp)
	je	.LBB11_192
# BB#191:                               # %pred.store.if225
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, 13(%rbp)
.LBB11_192:                             # %pred.store.continue226
                                        #   in Loop: Header=BB11_186 Depth=1
	movdqa	%xmm3, 224(%rsp)
	testb	$1, 230(%rsp)
	je	.LBB11_194
# BB#193:                               # %pred.store.if227
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, 12(%rbp)
.LBB11_194:                             # %pred.store.continue228
                                        #   in Loop: Header=BB11_186 Depth=1
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	movdqa	%xmm3, 208(%rsp)
	testb	$1, 216(%rsp)
	je	.LBB11_196
# BB#195:                               # %pred.store.if229
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, 11(%rbp)
.LBB11_196:                             # %pred.store.continue230
                                        #   in Loop: Header=BB11_186 Depth=1
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	movdqa	%xmm3, 192(%rsp)
	testb	$1, 202(%rsp)
	je	.LBB11_198
# BB#197:                               # %pred.store.if231
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, 10(%rbp)
.LBB11_198:                             # %pred.store.continue232
                                        #   in Loop: Header=BB11_186 Depth=1
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	movdqa	%xmm3, 176(%rsp)
	testb	$1, 188(%rsp)
	je	.LBB11_200
# BB#199:                               # %pred.store.if233
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, 9(%rbp)
.LBB11_200:                             # %pred.store.continue234
                                        #   in Loop: Header=BB11_186 Depth=1
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	movdqa	%xmm3, 160(%rsp)
	testb	$1, 174(%rsp)
	je	.LBB11_202
# BB#201:                               # %pred.store.if235
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, 8(%rbp)
.LBB11_202:                             # %pred.store.continue236
                                        #   in Loop: Header=BB11_186 Depth=1
	pand	%xmm1, %xmm2
	pcmpeqw	%xmm0, %xmm2
	movdqa	%xmm2, 144(%rsp)
	testb	$1, 144(%rsp)
	je	.LBB11_204
# BB#203:                               # %pred.store.if237
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, 7(%rbp)
.LBB11_204:                             # %pred.store.continue238
                                        #   in Loop: Header=BB11_186 Depth=1
	movdqa	%xmm2, 128(%rsp)
	testb	$1, 130(%rsp)
	je	.LBB11_206
# BB#205:                               # %pred.store.if239
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, 6(%rbp)
.LBB11_206:                             # %pred.store.continue240
                                        #   in Loop: Header=BB11_186 Depth=1
	movdqa	%xmm2, 112(%rsp)
	testb	$1, 116(%rsp)
	je	.LBB11_208
# BB#207:                               # %pred.store.if241
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, 5(%rbp)
.LBB11_208:                             # %pred.store.continue242
                                        #   in Loop: Header=BB11_186 Depth=1
	movdqa	%xmm2, 96(%rsp)
	testb	$1, 102(%rsp)
	je	.LBB11_210
# BB#209:                               # %pred.store.if243
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, 4(%rbp)
.LBB11_210:                             # %pred.store.continue244
                                        #   in Loop: Header=BB11_186 Depth=1
	movdqa	%xmm2, 80(%rsp)
	testb	$1, 88(%rsp)
	je	.LBB11_212
# BB#211:                               # %pred.store.if245
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, 3(%rbp)
.LBB11_212:                             # %pred.store.continue246
                                        #   in Loop: Header=BB11_186 Depth=1
	movdqa	%xmm2, 64(%rsp)
	testb	$1, 74(%rsp)
	je	.LBB11_214
# BB#213:                               # %pred.store.if247
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, 2(%rbp)
.LBB11_214:                             # %pred.store.continue248
                                        #   in Loop: Header=BB11_186 Depth=1
	movdqa	%xmm2, 48(%rsp)
	testb	$1, 60(%rsp)
	je	.LBB11_216
# BB#215:                               # %pred.store.if249
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, 1(%rbp)
.LBB11_216:                             # %pred.store.continue250
                                        #   in Loop: Header=BB11_186 Depth=1
	movdqa	%xmm2, 32(%rsp)
	testb	$1, 46(%rsp)
	je	.LBB11_218
# BB#217:                               # %pred.store.if251
                                        #   in Loop: Header=BB11_186 Depth=1
	movb	$46, (%rbp)
.LBB11_218:                             # %pred.store.continue252
                                        #   in Loop: Header=BB11_186 Depth=1
	addq	$-16, %rbp
	addq	$-16, %rdi
	jne	.LBB11_186
# BB#219:                               # %middle.block197
	cmpq	%rbx, %rsi
	jne	.LBB11_221
	jmp	.LBB11_236
.LBB11_220:
	movq	%rsi, %rdx
.LBB11_221:                             # %.lr.ph.i16.i.preheader253
	leaq	-1(%rdx), %rsi
	movq	%rdx, %rdi
	andq	$3, %rdi
	je	.LBB11_226
# BB#222:                               # %.lr.ph.i16.i.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB11_223:                             # %.lr.ph.i16.i.prol
                                        # =>This Inner Loop Header: Depth=1
	cmpb	%al, -1(%rcx,%rdx)
	jne	.LBB11_225
# BB#224:                               #   in Loop: Header=BB11_223 Depth=1
	movb	$46, -1(%rcx,%rdx)
.LBB11_225:                             # %.backedge.i17.i.prol
                                        #   in Loop: Header=BB11_223 Depth=1
	decq	%rdx
	incq	%rdi
	jne	.LBB11_223
.LBB11_226:                             # %.lr.ph.i16.i.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB11_236
	.p2align	4, 0x90
.LBB11_227:                             # %.lr.ph.i16.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	%al, -1(%rcx,%rdx)
	jne	.LBB11_229
# BB#228:                               #   in Loop: Header=BB11_227 Depth=1
	movb	$46, -1(%rcx,%rdx)
.LBB11_229:                             # %.backedge.i17.i
                                        #   in Loop: Header=BB11_227 Depth=1
	cmpb	%al, -2(%rcx,%rdx)
	jne	.LBB11_231
# BB#230:                               #   in Loop: Header=BB11_227 Depth=1
	movb	$46, -2(%rcx,%rdx)
.LBB11_231:                             # %.backedge.i17.i.1
                                        #   in Loop: Header=BB11_227 Depth=1
	cmpb	%al, -3(%rcx,%rdx)
	jne	.LBB11_233
# BB#232:                               #   in Loop: Header=BB11_227 Depth=1
	movb	$46, -3(%rcx,%rdx)
.LBB11_233:                             # %.backedge.i17.i.2
                                        #   in Loop: Header=BB11_227 Depth=1
	leaq	-4(%rdx), %rsi
	cmpb	%al, -4(%rcx,%rdx)
	jne	.LBB11_235
# BB#234:                               #   in Loop: Header=BB11_227 Depth=1
	movb	$46, -4(%rcx,%rdx)
.LBB11_235:                             # %.backedge.i17.i.3
                                        #   in Loop: Header=BB11_227 Depth=1
	testq	%rsi, %rsi
	movq	%rsi, %rdx
	jne	.LBB11_227
.LBB11_236:                             # %buffreplace.exit18.i
	movl	$.L.str.46, %esi
	movl	$284, %edx              # imm = 0x11C
	movq	%r15, %rdi
	callq	luaX_lexerror
.LBB11_237:                             # %trydecpoint.exit
	addq	$888, %rsp              # imm = 0x378
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	read_numeral, .Lfunc_end11-read_numeral
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"and"
	.size	.L.str, 4

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"break"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"do"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"else"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"elseif"
	.size	.L.str.4, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"end"
	.size	.L.str.5, 4

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"false"
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"for"
	.size	.L.str.7, 4

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"function"
	.size	.L.str.8, 9

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"if"
	.size	.L.str.9, 3

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"in"
	.size	.L.str.10, 3

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"local"
	.size	.L.str.11, 6

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"nil"
	.size	.L.str.12, 4

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"not"
	.size	.L.str.13, 4

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"or"
	.size	.L.str.14, 3

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"repeat"
	.size	.L.str.15, 7

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"return"
	.size	.L.str.16, 7

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"then"
	.size	.L.str.17, 5

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"true"
	.size	.L.str.18, 5

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"until"
	.size	.L.str.19, 6

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"while"
	.size	.L.str.20, 6

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	".."
	.size	.L.str.21, 3

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"..."
	.size	.L.str.22, 4

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"=="
	.size	.L.str.23, 3

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	">="
	.size	.L.str.24, 3

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"<="
	.size	.L.str.25, 3

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"~="
	.size	.L.str.26, 3

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"<number>"
	.size	.L.str.27, 9

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"<name>"
	.size	.L.str.28, 7

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"<string>"
	.size	.L.str.29, 9

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"<eof>"
	.size	.L.str.30, 6

	.hidden	luaX_tokens             # @luaX_tokens
	.type	luaX_tokens,@object
	.section	.rodata,"a",@progbits
	.globl	luaX_tokens
	.p2align	4
luaX_tokens:
	.quad	.L.str
	.quad	.L.str.1
	.quad	.L.str.2
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	.L.str.6
	.quad	.L.str.7
	.quad	.L.str.8
	.quad	.L.str.9
	.quad	.L.str.10
	.quad	.L.str.11
	.quad	.L.str.12
	.quad	.L.str.13
	.quad	.L.str.14
	.quad	.L.str.15
	.quad	.L.str.16
	.quad	.L.str.17
	.quad	.L.str.18
	.quad	.L.str.19
	.quad	.L.str.20
	.quad	.L.str.21
	.quad	.L.str.22
	.quad	.L.str.23
	.quad	.L.str.24
	.quad	.L.str.25
	.quad	.L.str.26
	.quad	.L.str.27
	.quad	.L.str.28
	.quad	.L.str.29
	.quad	.L.str.30
	.quad	0
	.size	luaX_tokens, 256

	.type	.L.str.31,@object       # @.str.31
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.31:
	.asciz	"char(%d)"
	.size	.L.str.31, 9

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"%c"
	.size	.L.str.32, 3

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"%s:%d: %s"
	.size	.L.str.33, 10

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"%s near '%s'"
	.size	.L.str.34, 13

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"lexical element too long"
	.size	.L.str.35, 25

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"invalid long string delimiter"
	.size	.L.str.36, 30

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"chunk has too many lines"
	.size	.L.str.38, 25

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"unfinished long string"
	.size	.L.str.39, 23

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"unfinished long comment"
	.size	.L.str.40, 24

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"nesting of [[...]] is deprecated"
	.size	.L.str.41, 33

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"unfinished string"
	.size	.L.str.42, 18

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"escape sequence too large"
	.size	.L.str.43, 26

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"Ee"
	.size	.L.str.44, 3

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"malformed number"
	.size	.L.str.46, 17

	.hidden	luaS_newlstr
	.hidden	luaO_pushfstring
	.hidden	luaO_chunkid
	.hidden	luaD_throw
	.hidden	luaH_setstr
	.hidden	luaM_realloc_
	.hidden	luaZ_fill
	.hidden	luaM_toobig
	.hidden	luaO_str2d

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
