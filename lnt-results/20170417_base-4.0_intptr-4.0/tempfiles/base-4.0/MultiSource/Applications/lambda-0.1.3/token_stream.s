	.text
	.file	"token_stream.bc"
	.globl	_ZN12token_streamC2EPKc
	.p2align	4, 0x90
	.type	_ZN12token_streamC2EPKc,@function
_ZN12token_streamC2EPKc:                # @_ZN12token_streamC2EPKc
	.cfi_startproc
# BB#0:
	movq	$0, 8(%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8736(%rdi)
	movl	$0, 8752(%rdi)
	jmp	_ZN12token_stream4openEPKc # TAILCALL
.Lfunc_end0:
	.size	_ZN12token_streamC2EPKc, .Lfunc_end0-_ZN12token_streamC2EPKc
	.cfi_endproc

	.globl	_ZN12token_stream4openEPKc
	.p2align	4, 0x90
	.type	_ZN12token_stream4openEPKc,@function
_ZN12token_stream4openEPKc:             # @_ZN12token_stream4openEPKc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movw	$0, (%rbx)
	testq	%r14, %r14
	je	.LBB1_6
# BB#1:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_3
# BB#2:
	callq	fclose
.LBB1_3:
	movl	$.L.str, %esi
	movq	%r14, %rdi
	callq	fopen
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	jne	.LBB1_5
# BB#4:
	movw	$1, (%rbx)
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	_ZN12token_stream8dderrmsgEPcz
.LBB1_5:
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 8736(%rbx)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcpy
	jmp	.LBB1_7
.LBB1_6:
	movq	stdin(%rip), %rax
	movq	%rax, 8(%rbx)
	movq	$0, 8736(%rbx)
.LBB1_7:
	movw	$0, 2(%rbx)
	movl	$0, 24(%rbx)
	leaq	28(%rbx), %rdi
	movl	$.L.str.2, %eax
	cmpq	%rax, %rdi
	je	.LBB1_9
# BB#8:
	xorl	%esi, %esi
	movl	$8191, %edx             # imm = 0x1FFF
	callq	memset
.LBB1_9:                                # %_ZN12token_stream10push_tokenENS_10token_typeEPc.exit
	movb	$0, 8219(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 8744(%rbx)
	movl	$1, 8752(%rbx)
	xorl	%eax, %eax
	cmpq	$0, 8(%rbx)
	setne	%al
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	_ZN12token_stream4openEPKc, .Lfunc_end1-_ZN12token_stream4openEPKc
	.cfi_endproc

	.globl	_ZN12token_stream8dderrmsgEPcz
	.p2align	4, 0x90
	.type	_ZN12token_stream8dderrmsgEPcz,@function
_ZN12token_stream8dderrmsgEPcz:         # @_ZN12token_stream8dderrmsgEPcz
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
	subq	$208, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 224
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	testb	%al, %al
	je	.LBB2_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB2_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	224(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$16, (%rsp)
	movl	8744(%rdi), %edx
	testl	%edx, %edx
	jns	.LBB2_3
.LBB2_4:
	movq	stderr(%rip), %rdi
	movq	%rsp, %rdx
	movq	%rbx, %rsi
	callq	vfprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	addq	$208, %rsp
	popq	%rbx
	retq
.LBB2_3:
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB2_4
.Lfunc_end2:
	.size	_ZN12token_stream8dderrmsgEPcz, .Lfunc_end2-_ZN12token_stream8dderrmsgEPcz
	.cfi_endproc

	.globl	_ZN12token_stream10push_tokenENS_10token_typeEPc
	.p2align	4, 0x90
	.type	_ZN12token_stream10push_tokenENS_10token_typeEPc,@function
_ZN12token_stream10push_tokenENS_10token_typeEPc: # @_ZN12token_stream10push_tokenENS_10token_typeEPc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
	movq	%rdx, %rax
	movq	%rdi, %rbx
	movl	%esi, 24(%rbx)
	testq	%rax, %rax
	je	.LBB3_3
# BB#1:
	leaq	28(%rbx), %rdi
	cmpq	%rax, %rdi
	je	.LBB3_3
# BB#2:
	movl	$8191, %edx             # imm = 0x1FFF
	movq	%rax, %rsi
	callq	strncpy
.LBB3_3:
	movb	$0, 8219(%rbx)
	popq	%rbx
	retq
.Lfunc_end3:
	.size	_ZN12token_stream10push_tokenENS_10token_typeEPc, .Lfunc_end3-_ZN12token_stream10push_tokenENS_10token_typeEPc
	.cfi_endproc

	.globl	_ZN12token_streamD2Ev
	.p2align	4, 0x90
	.type	_ZN12token_streamD2Ev,@function
_ZN12token_streamD2Ev:                  # @_ZN12token_streamD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_2
# BB#1:
	callq	fclose
.LBB4_2:
	movq	8736(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_3
# BB#4:
	popq	%rbx
	jmp	_ZdaPv                  # TAILCALL
.LBB4_3:
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZN12token_streamD2Ev, .Lfunc_end4-_ZN12token_streamD2Ev
	.cfi_endproc

	.globl	_ZN12token_stream11reset_tokenEv
	.p2align	4, 0x90
	.type	_ZN12token_stream11reset_tokenEv,@function
_ZN12token_stream11reset_tokenEv:       # @_ZN12token_stream11reset_tokenEv
	.cfi_startproc
# BB#0:
	movl	$0, 8748(%rdi)
	movl	$0, 24(%rdi)
	movl	$1, 8752(%rdi)
	leaq	8220(%rdi), %rax
	movq	%rax, 16(%rdi)
	movb	$0, 8220(%rdi)
	retq
.Lfunc_end5:
	.size	_ZN12token_stream11reset_tokenEv, .Lfunc_end5-_ZN12token_stream11reset_tokenEv
	.cfi_endproc

	.globl	_ZN12token_stream5closeEv
	.p2align	4, 0x90
	.type	_ZN12token_stream5closeEv,@function
_ZN12token_stream5closeEv:              # @_ZN12token_stream5closeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_3
# BB#1:
	cmpq	$0, 8736(%rbx)
	je	.LBB6_3
# BB#2:
	callq	fclose
.LBB6_3:
	movq	$0, 8(%rbx)
	popq	%rbx
	retq
.Lfunc_end6:
	.size	_ZN12token_stream5closeEv, .Lfunc_end6-_ZN12token_stream5closeEv
	.cfi_endproc

	.globl	_ZN12token_stream9read_lineEv
	.p2align	4, 0x90
	.type	_ZN12token_stream9read_lineEv,@function
_ZN12token_stream9read_lineEv:          # @_ZN12token_stream9read_lineEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	8220(%r15), %rbx
	movq	8(%r15), %rdx
	movl	$512, %esi              # imm = 0x200
	movq	%rbx, %rdi
	callq	fgets
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB7_4
# BB#1:
	incl	8744(%r15)
	movq	%rbx, %rdi
	callq	strlen
	testl	%eax, %eax
	jle	.LBB7_8
# BB#2:
	shlq	$32, %rax
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	addq	%rax, %rcx
	sarq	$32, %rcx
	cmpb	$10, 8220(%r15,%rcx)
	jne	.LBB7_8
# BB#3:
	movb	$0, 8220(%r15,%rcx)
	jmp	.LBB7_8
.LBB7_4:
	movq	8(%r15), %rbx
	movq	%rbx, %rdi
	callq	ferror
	testl	%eax, %eax
	je	.LBB7_6
# BB#5:
	callq	__errno_location
	movl	8744(%r15), %ebx
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movq	%rcx, %rdx
	callq	printf
	movw	$1, (%r15)
	jmp	.LBB7_8
.LBB7_6:
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB7_8
# BB#7:
	movw	$1, 2(%r15)
.LBB7_8:
	movl	$0, 8752(%r15)
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	_ZN12token_stream9read_lineEv, .Lfunc_end7-_ZN12token_stream9read_lineEv
	.cfi_endproc

	.globl	_ZN12token_stream9is_headerENS_10token_typeEPc
	.p2align	4, 0x90
	.type	_ZN12token_stream9is_headerENS_10token_typeEPc,@function
_ZN12token_stream9is_headerENS_10token_typeEPc: # @_ZN12token_stream9is_headerENS_10token_typeEPc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 16
.Lcfi21:
	.cfi_offset %rbx, -16
	movq	%rdx, %rbx
	xorl	%eax, %eax
	cmpl	$8, %esi
	jne	.LBB8_5
# BB#1:                                 # %.preheader.preheader
	movl	$.L.str.8, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB8_2
# BB#3:                                 # %.preheader.111
	movl	$.L.str.9, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB8_4
# BB#6:                                 # %.preheader.212
	movl	$.L.str.10, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB8_7
# BB#8:                                 # %.preheader.313
	movl	$.L.str.11, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB8_9
# BB#10:                                # %.preheader.414
	movl	$.L.str.12, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB8_11
# BB#12:                                # %.preheader.515
	movl	$.L.str.13, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB8_13
# BB#14:                                # %.preheader.616
	movl	$.L.str.14, %esi
	movq	%rbx, %rdi
	callq	strcmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	movl	$7, %eax
	cmovnel	%ecx, %eax
	popq	%rbx
	retq
.LBB8_2:
	movl	$1, %eax
	popq	%rbx
	retq
.LBB8_4:
	movl	$2, %eax
.LBB8_5:                                # %.loopexit
	popq	%rbx
	retq
.LBB8_7:
	movl	$3, %eax
	popq	%rbx
	retq
.LBB8_9:
	movl	$4, %eax
	popq	%rbx
	retq
.LBB8_11:
	movl	$5, %eax
	popq	%rbx
	retq
.LBB8_13:
	movl	$6, %eax
	popq	%rbx
	retq
.Lfunc_end8:
	.size	_ZN12token_stream9is_headerENS_10token_typeEPc, .Lfunc_end8-_ZN12token_stream9is_headerENS_10token_typeEPc
	.cfi_endproc

	.globl	_ZN12token_stream9get_tokenEPPc
	.p2align	4, 0x90
	.type	_ZN12token_stream9get_tokenEPPc,@function
_ZN12token_stream9get_tokenEPPc:        # @_ZN12token_stream9get_tokenEPPc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 64
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	$0, 8(%rbx)
	je	.LBB9_1
# BB#2:
	movl	24(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB9_4
# BB#3:
	leaq	28(%rbx), %rax
	movq	%rax, (%r14)
	movl	$0, 24(%rbx)
	jmp	.LBB9_52
.LBB9_1:
	xorl	%ecx, %ecx
	jmp	.LBB9_52
.LBB9_4:
	movq	$0, (%r14)
	leaq	28(%rbx), %r15
	leaq	8219(%rbx), %r12
	movq	16(%rbx), %rax
	movl	$.L.str.6, %r13d
	testq	%rax, %rax
	jne	.LBB9_6
	jmp	.LBB9_7
	.p2align	4, 0x90
.LBB9_19:                               # %._crit_edge.loopexit
	decq	%rax
.LBB9_20:                               # %._crit_edge
	movb	%cl, 28(%rbx)
	movb	$0, 29(%rbx)
	movb	(%rax), %bpl
	movsbl	%bpl, %ecx
	leal	-34(%rcx), %edx
	cmpl	$91, %edx
	ja	.LBB9_21
# BB#53:                                # %._crit_edge
	jmpq	*.LJTI9_0(,%rdx,8)
	.p2align	4, 0x90
.LBB9_21:                               # %._crit_edge
	testl	%ecx, %ecx
	jne	.LBB9_22
.LBB9_49:                               # %.backedge
	movq	$0, 16(%rbx)
	xorl	%eax, %eax
	testq	%rax, %rax
	je	.LBB9_7
.LBB9_6:
	cmpb	$0, (%rax)
	jne	.LBB9_14
.LBB9_7:
	movl	8748(%rbx), %esi
	testl	%esi, %esi
	je	.LBB9_8
# BB#10:                                # %thread-pre-split
	jle	.LBB9_12
# BB#11:
	cmpl	$1, %esi
	movl	$.L.str.2, %edx
	cmovneq	%r13, %rdx
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB9_12
	.p2align	4, 0x90
.LBB9_8:
	cmpl	$0, 8752(%rbx)
	je	.LBB9_9
.LBB9_12:                               # %thread-pre-split41
	movq	%rbx, %rdi
	callq	_ZN12token_stream9read_lineEv
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB9_13
.LBB9_14:                               # %.preheader47
	movb	(%rax), %cl
	cmpb	$32, %cl
	jg	.LBB9_20
# BB#15:                                # %.preheader47
	testb	%cl, %cl
	je	.LBB9_20
# BB#16:                                # %.lr.ph.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB9_17:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, 16(%rbx)
	movzbl	(%rax), %ecx
	incq	%rax
	cmpb	$32, %cl
	jg	.LBB9_19
# BB#18:                                # %.lr.ph
                                        #   in Loop: Header=BB9_17 Depth=1
	testb	%cl, %cl
	jne	.LBB9_17
	jmp	.LBB9_19
.LBB9_13:
	xorl	%ecx, %ecx
	jmp	.LBB9_52
.LBB9_9:                                # %.thread
	movl	$1, 8752(%rbx)
	movb	$0, 28(%rbx)
	movl	$11, %ecx
	jmp	.LBB9_51
.LBB9_22:                               # %.preheader45.preheader
	testb	%bpl, %bpl
	movq	%r15, %r13
	je	.LBB9_28
# BB#23:
	movq	%r15, %rax
.LBB9_24:                               # %.lr.ph129
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %r13
	movsbl	%bpl, %esi
	movl	$.L.str.7, %edi
	movl	$10, %edx
	callq	memchr
	testq	%rax, %rax
	jne	.LBB9_28
# BB#25:                                #   in Loop: Header=BB9_24 Depth=1
	movb	%bpl, (%r13)
	leaq	1(%r13), %rax
	movq	16(%rbx), %rcx
	incq	%rcx
	movq	%rcx, 16(%rbx)
	cmpq	%r12, %rax
	jae	.LBB9_27
# BB#26:                                # %..preheader45_crit_edge
                                        #   in Loop: Header=BB9_24 Depth=1
	movzbl	(%rcx), %ebp
	testb	%bpl, %bpl
	jne	.LBB9_24
.LBB9_27:                               # %.critedge3.loopexitsplit
	incq	%r13
.LBB9_28:                               # %.critedge3
	movb	$0, (%r13)
	movl	$8, %ecx
	jmp	.LBB9_51
.LBB9_36:                               # %.preheader46
	incq	%rax
	movq	%rax, 16(%rbx)
	movq	%r15, %r13
	.p2align	4, 0x90
.LBB9_37:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	cmpb	$92, %cl
	jne	.LBB9_38
# BB#41:                                #   in Loop: Header=BB9_37 Depth=1
	cmpb	$0, 1(%rax)
	jne	.LBB9_43
# BB#42:                                #   in Loop: Header=BB9_37 Depth=1
	movq	%rbx, %rdi
	callq	_ZN12token_stream9read_lineEv
	movq	%rax, 16(%rbx)
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.LBB9_37
	jmp	.LBB9_52
.LBB9_38:                               #   in Loop: Header=BB9_37 Depth=1
	testb	%cl, %cl
	je	.LBB9_48
# BB#39:                                #   in Loop: Header=BB9_37 Depth=1
	cmpb	$34, %cl
	je	.LBB9_40
	jmp	.LBB9_47
.LBB9_43:                               # %.critedge.thread
                                        #   in Loop: Header=BB9_37 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, 16(%rbx)
	movzbl	1(%rax), %ecx
	cmpb	$110, %cl
	je	.LBB9_44
# BB#45:                                # %.critedge.thread
                                        #   in Loop: Header=BB9_37 Depth=1
	cmpb	$116, %cl
	jne	.LBB9_47
# BB#46:                                #   in Loop: Header=BB9_37 Depth=1
	movb	$9, %cl
	jmp	.LBB9_47
.LBB9_44:                               #   in Loop: Header=BB9_37 Depth=1
	movb	$10, %cl
.LBB9_47:                               #   in Loop: Header=BB9_37 Depth=1
	movb	%cl, (%r13)
	incq	%r13
	movq	16(%rbx), %rax
	incq	%rax
	movq	%rax, 16(%rbx)
	cmpq	%r12, %r13
	jb	.LBB9_37
	jmp	.LBB9_48
.LBB9_30:
	incl	8748(%rbx)
	incq	%rax
	movq	%rax, 16(%rbx)
	movl	$1, %ecx
	jmp	.LBB9_51
.LBB9_31:
	decl	8748(%rbx)
	incq	%rax
	movq	%rax, 16(%rbx)
	movl	$2, %ecx
	jmp	.LBB9_51
.LBB9_34:
	incq	%rax
	movq	%rax, 16(%rbx)
	movl	$5, %ecx
	jmp	.LBB9_51
.LBB9_29:
	incq	%rax
	movq	%rax, 16(%rbx)
	movl	$10, %ecx
	jmp	.LBB9_51
.LBB9_35:
	incq	%rax
	movq	%rax, 16(%rbx)
	movl	$6, %ecx
	jmp	.LBB9_51
.LBB9_50:
	incq	%rax
	movq	%rax, 16(%rbx)
	movl	$9, %ecx
	jmp	.LBB9_51
.LBB9_32:
	incq	%rax
	movq	%rax, 16(%rbx)
	movl	$3, %ecx
	jmp	.LBB9_51
.LBB9_33:
	incq	%rax
	movq	%rax, 16(%rbx)
	movl	$4, %ecx
	jmp	.LBB9_51
.LBB9_40:
	incq	%rax
	movq	%rax, 16(%rbx)
.LBB9_48:                               # %.loopexit
	movb	$0, (%r13)
	movl	$7, %ecx
.LBB9_51:
	movq	%r15, (%r14)
.LBB9_52:                               # %.critedge40
	movl	%ecx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_ZN12token_stream9get_tokenEPPc, .Lfunc_end9-_ZN12token_stream9get_tokenEPPc
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI9_0:
	.quad	.LBB9_36
	.quad	.LBB9_49
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_30
	.quad	.LBB9_31
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_34
	.quad	.LBB9_22
	.quad	.LBB9_29
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_35
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_50
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_22
	.quad	.LBB9_32
	.quad	.LBB9_22
	.quad	.LBB9_33

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"r"
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Cannot Open \"%s\":%s\n"
	.size	.L.str.1, 21

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.zero	1
	.size	.L.str.2, 1

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n*** line %d: "
	.size	.L.str.3, 15

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"*error reading line %d of DataDesc input file: %s\n"
	.size	.L.str.4, 51

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"*need %d )%s*<< "
	.size	.L.str.5, 17

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"'s"
	.size	.L.str.6, 3

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	" .(){},;\""
	.size	.L.str.7, 10

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"def"
	.size	.L.str.8, 4

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"load"
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"save"
	.size	.L.str.10, 5

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"quit"
	.size	.L.str.11, 5

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"list"
	.size	.L.str.12, 5

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"set"
	.size	.L.str.13, 4

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"ext"
	.size	.L.str.14, 4


	.globl	_ZN12token_streamC1EPKc
	.type	_ZN12token_streamC1EPKc,@function
_ZN12token_streamC1EPKc = _ZN12token_streamC2EPKc
	.globl	_ZN12token_streamD1Ev
	.type	_ZN12token_streamD1Ev,@function
_ZN12token_streamD1Ev = _ZN12token_streamD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
