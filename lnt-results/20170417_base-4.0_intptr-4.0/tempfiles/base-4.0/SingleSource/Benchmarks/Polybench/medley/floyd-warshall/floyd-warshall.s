	.text
	.file	"floyd-warshall.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4562146422526312448     # double 9.765625E-4
.LCPI6_2:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_1:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 96
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_46
# BB#1:
	movq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_46
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_46
# BB#3:                                 # %polybench_alloc_data.exit
	movq	8(%rsp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_46
# BB#4:                                 # %polybench_alloc_data.exit21
	leaq	24(%rbx), %rax
	xorl	%ecx, %ecx
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_5:                                # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_6 Depth 2
	incq	%rcx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movq	$-1024, %rdx            # imm = 0xFC00
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_6:                                #   Parent Loop BB6_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1025(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -24(%rsi)
	leal	1026(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -16(%rsi)
	leal	1027(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -8(%rsi)
	leal	1028(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, (%rsi)
	addq	$32, %rsi
	addq	$4, %rdx
	jne	.LBB6_6
# BB#7:                                 # %.loopexit.i
                                        #   in Loop: Header=BB6_5 Depth=1
	addq	$8192, %rax             # imm = 0x2000
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB6_5
# BB#8:                                 # %.preheader1.i.preheader
	leaq	16(%rbx), %r11
	leaq	8(%rbx), %r10
	xorl	%r9d, %r9d
	movq	%r10, 16(%rsp)          # 8-byte Spill
	movq	%r11, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_9:                                # %.preheader1.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_10 Depth 2
                                        #       Child Loop BB6_13 Depth 3
                                        #       Child Loop BB6_15 Depth 3
	movq	%r9, %rax
	shlq	$13, %rax
	leaq	(%rbx,%rax), %r15
	leaq	8192(%rbx,%rax), %r13
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_10:                               # %.preheader.i22
                                        #   Parent Loop BB6_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_13 Depth 3
                                        #       Child Loop BB6_15 Depth 3
	movq	%rdi, %rax
	shlq	$13, %rax
	leaq	(%rbx,%rax), %rcx
	movq	%rbx, %r12
	leaq	8192(%rbx,%rax), %rdx
	leaq	(%rcx,%r9,8), %rax
	cmpq	%rax, %rcx
	sbbb	%sil, %sil
	cmpq	%rdx, %rax
	sbbb	%bl, %bl
	andb	%sil, %bl
	cmpq	%r13, %rcx
	sbbb	%cl, %cl
	cmpq	%rdx, %r15
	sbbb	%dl, %dl
	testb	$1, %bl
	jne	.LBB6_14
# BB#11:                                # %.preheader.i22
                                        #   in Loop: Header=BB6_10 Depth=2
	andb	%dl, %cl
	andb	$1, %cl
	jne	.LBB6_14
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB6_10 Depth=2
	movl	$1024, %ecx             # imm = 0x400
	movq	%r11, %rbp
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB6_13:                               # %vector.body
                                        #   Parent Loop BB6_9 Depth=1
                                        #     Parent Loop BB6_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-16(%rsi), %xmm1
	movupd	(%rsi), %xmm2
	movsd	(%rax), %xmm3           # xmm3 = mem[0],zero
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	movupd	-16(%rbp), %xmm4
	movupd	(%rbp), %xmm5
	addpd	%xmm3, %xmm4
	addpd	%xmm3, %xmm5
	minpd	%xmm4, %xmm1
	minpd	%xmm5, %xmm2
	movupd	%xmm1, -16(%rsi)
	movupd	%xmm2, (%rsi)
	addq	$32, %rsi
	addq	$32, %rbp
	addq	$-4, %rcx
	jne	.LBB6_13
	jmp	.LBB6_16
	.p2align	4, 0x90
.LBB6_14:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB6_10 Depth=2
	movl	$1024, %ecx             # imm = 0x400
	movq	%r10, %rsi
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB6_15:                               # %scalar.ph
                                        #   Parent Loop BB6_9 Depth=1
                                        #     Parent Loop BB6_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rbp), %xmm1         # xmm1 = mem[0],zero
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	addsd	-8(%rsi), %xmm2
	minsd	%xmm2, %xmm1
	movsd	%xmm1, -8(%rbp)
	movsd	(%rbp), %xmm1           # xmm1 = mem[0],zero
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	addsd	(%rsi), %xmm2
	minsd	%xmm2, %xmm1
	movsd	%xmm1, (%rbp)
	addq	$16, %rbp
	addq	$16, %rsi
	addq	$-2, %rcx
	jne	.LBB6_15
.LBB6_16:                               # %middle.block
                                        #   in Loop: Header=BB6_10 Depth=2
	incq	%rdi
	addq	$8192, %r8              # imm = 0x2000
	addq	$8192, %r14             # imm = 0x2000
	cmpq	$1024, %rdi             # imm = 0x400
	movq	%r12, %rbx
	jne	.LBB6_10
# BB#17:                                #   in Loop: Header=BB6_9 Depth=1
	incq	%r9
	addq	$8192, %r11             # imm = 0x2000
	addq	$8192, %r10             # imm = 0x2000
	cmpq	$1024, %r9              # imm = 0x400
	jne	.LBB6_9
# BB#18:                                # %kernel_floyd_warshall.exit
	movq	32(%rsp), %rbx          # 8-byte Reload
	leaq	24(%rbx), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_19:                               # %.preheader.i30
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_20 Depth 2
	incq	%rcx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movq	$-1024, %rdx            # imm = 0xFC00
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_20:                               #   Parent Loop BB6_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1025(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -24(%rsi)
	leal	1026(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -16(%rsi)
	leal	1027(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -8(%rsi)
	leal	1028(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, (%rsi)
	addq	$32, %rsi
	addq	$4, %rdx
	jne	.LBB6_20
# BB#21:                                # %.loopexit.i27
                                        #   in Loop: Header=BB6_19 Depth=1
	addq	$8192, %rax             # imm = 0x2000
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB6_19
# BB#22:                                # %.preheader1.i36.preheader
	leaq	16(%rbx), %r11
	leaq	8(%rbx), %r10
	xorl	%r9d, %r9d
	movq	%r10, 16(%rsp)          # 8-byte Spill
	movq	%r11, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_23:                               # %.preheader1.i36
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_24 Depth 2
                                        #       Child Loop BB6_27 Depth 3
                                        #       Child Loop BB6_29 Depth 3
	movq	%r9, %rax
	shlq	$13, %rax
	leaq	(%rbx,%rax), %r15
	leaq	8192(%rbx,%rax), %r13
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_24:                               # %.preheader.i38
                                        #   Parent Loop BB6_23 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_27 Depth 3
                                        #       Child Loop BB6_29 Depth 3
	movq	%rdi, %rax
	shlq	$13, %rax
	leaq	(%rbx,%rax), %rcx
	leaq	8192(%rbx,%rax), %rdx
	leaq	(%rcx,%r9,8), %rax
	cmpq	%rax, %rcx
	sbbb	%sil, %sil
	cmpq	%rdx, %rax
	sbbb	%bl, %bl
	andb	%sil, %bl
	cmpq	%r13, %rcx
	sbbb	%cl, %cl
	cmpq	%rdx, %r15
	sbbb	%dl, %dl
	testb	$1, %bl
	jne	.LBB6_28
# BB#25:                                # %.preheader.i38
                                        #   in Loop: Header=BB6_24 Depth=2
	andb	%dl, %cl
	andb	$1, %cl
	jne	.LBB6_28
# BB#26:                                # %vector.body89.preheader
                                        #   in Loop: Header=BB6_24 Depth=2
	movl	$1024, %ecx             # imm = 0x400
	movq	%r11, %rbp
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB6_27:                               # %vector.body89
                                        #   Parent Loop BB6_23 Depth=1
                                        #     Parent Loop BB6_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-16(%rsi), %xmm0
	movupd	(%rsi), %xmm1
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movupd	-16(%rbp), %xmm3
	movupd	(%rbp), %xmm4
	addpd	%xmm2, %xmm3
	addpd	%xmm2, %xmm4
	minpd	%xmm3, %xmm0
	minpd	%xmm4, %xmm1
	movupd	%xmm0, -16(%rsi)
	movupd	%xmm1, (%rsi)
	addq	$32, %rsi
	addq	$32, %rbp
	addq	$-4, %rcx
	jne	.LBB6_27
	jmp	.LBB6_30
	.p2align	4, 0x90
.LBB6_28:                               # %scalar.ph91.preheader
                                        #   in Loop: Header=BB6_24 Depth=2
	movl	$1024, %ecx             # imm = 0x400
	movq	%r10, %rsi
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB6_29:                               # %scalar.ph91
                                        #   Parent Loop BB6_23 Depth=1
                                        #     Parent Loop BB6_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rbp), %xmm0         # xmm0 = mem[0],zero
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	addsd	-8(%rsi), %xmm1
	minsd	%xmm1, %xmm0
	movsd	%xmm0, -8(%rbp)
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	addsd	(%rsi), %xmm1
	minsd	%xmm1, %xmm0
	movsd	%xmm0, (%rbp)
	addq	$16, %rbp
	addq	$16, %rsi
	addq	$-2, %rcx
	jne	.LBB6_29
.LBB6_30:                               # %middle.block90
                                        #   in Loop: Header=BB6_24 Depth=2
	incq	%rdi
	addq	$8192, %r8              # imm = 0x2000
	addq	$8192, %r14             # imm = 0x2000
	cmpq	$1024, %rdi             # imm = 0x400
	movq	32(%rsp), %rbx          # 8-byte Reload
	jne	.LBB6_24
# BB#31:                                #   in Loop: Header=BB6_23 Depth=1
	incq	%r9
	addq	$8192, %r11             # imm = 0x2000
	addq	$8192, %r10             # imm = 0x2000
	cmpq	$1024, %r9              # imm = 0x400
	jne	.LBB6_23
# BB#32:                                # %.preheader.i47.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	movapd	.LCPI6_1(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_2(%rip), %xmm3   # xmm3 = mem[0],zero
	movq	%r12, %rbp
.LBB6_33:                               # %.preheader.i47
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_34 Depth 2
	movq	%rax, %rsi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_34:                               #   Parent Loop BB6_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rbp,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%rbx,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_35
# BB#37:                                #   in Loop: Header=BB6_34 Depth=2
	movsd	(%rbp,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rbx,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_36
# BB#38:                                #   in Loop: Header=BB6_34 Depth=2
	addq	$2, %rsi
	leaq	2(%rcx), %rdi
	incq	%rcx
	cmpq	$1024, %rcx             # imm = 0x400
	movq	%rdi, %rcx
	jl	.LBB6_34
# BB#39:                                #   in Loop: Header=BB6_33 Depth=1
	incq	%rdx
	addq	$1024, %rax             # imm = 0x400
	cmpq	$1024, %rdx             # imm = 0x400
	jl	.LBB6_33
# BB#40:                                # %check_FP.exit
	movl	$16385, %edi            # imm = 0x4001
	callq	malloc
	movq	%rax, %r15
	movb	$0, 16384(%r15)
	xorl	%r13d, %r13d
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB6_41:                               # %.preheader.i52
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_42 Depth 2
	movq	%rbp, %rax
	movl	$15, %ecx
	.p2align	4, 0x90
.LBB6_42:                               #   Parent Loop BB6_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movl	%edx, %ebx
	andb	$15, %bl
	orb	$48, %bl
	movb	%bl, -15(%r15,%rcx)
	movb	%bl, -14(%r15,%rcx)
	movq	%rdx, %rsi
	shrq	$8, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -13(%r15,%rcx)
	movb	%sil, -12(%r15,%rcx)
	movq	%rdx, %rsi
	shrq	$16, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -11(%r15,%rcx)
	movb	%sil, -10(%r15,%rcx)
	movl	%edx, %esi
	shrl	$24, %esi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -9(%r15,%rcx)
	movb	%sil, -8(%r15,%rcx)
	movq	%rdx, %rsi
	shrq	$32, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -7(%r15,%rcx)
	movb	%sil, -6(%r15,%rcx)
	movq	%rdx, %rsi
	shrq	$40, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -5(%r15,%rcx)
	movb	%sil, -4(%r15,%rcx)
	movq	%rdx, %rsi
	shrq	$48, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -3(%r15,%rcx)
	movb	%sil, -2(%r15,%rcx)
	shrq	$56, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, -1(%r15,%rcx)
	movb	%dl, (%r15,%rcx)
	addq	$16, %rcx
	addq	$8, %rax
	cmpq	$16399, %rcx            # imm = 0x400F
	jne	.LBB6_42
# BB#43:                                #   in Loop: Header=BB6_41 Depth=1
	movq	stderr(%rip), %rsi
	movq	%r15, %rdi
	callq	fputs
	incq	%r13
	addq	$8192, %rbp             # imm = 0x2000
	cmpq	$1024, %r13             # imm = 0x400
	jne	.LBB6_41
# BB#44:                                # %print_array.exit
	movq	%r15, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_45
.LBB6_35:                               # %check_FP.exit.threadsplit
	decq	%rcx
.LBB6_36:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r8d
	movl	%ecx, %r9d
	callq	fprintf
	movl	$1, %eax
.LBB6_45:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_46:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d][%d] = %lf and B[%d][%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 76


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
