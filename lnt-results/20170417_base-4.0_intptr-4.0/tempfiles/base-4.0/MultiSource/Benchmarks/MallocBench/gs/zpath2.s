	.text
	.file	"zpath2.bc"
	.globl	zflattenpath
	.p2align	4, 0x90
	.type	zflattenpath,@function
zflattenpath:                           # @zflattenpath
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_flattenpath          # TAILCALL
.Lfunc_end0:
	.size	zflattenpath, .Lfunc_end0-zflattenpath
	.cfi_endproc

	.globl	zreversepath
	.p2align	4, 0x90
	.type	zreversepath,@function
zreversepath:                           # @zreversepath
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_reversepath          # TAILCALL
.Lfunc_end1:
	.size	zreversepath, .Lfunc_end1-zreversepath
	.cfi_endproc

	.globl	zstrokepath
	.p2align	4, 0x90
	.type	zstrokepath,@function
zstrokepath:                            # @zstrokepath
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_strokepath           # TAILCALL
.Lfunc_end2:
	.size	zstrokepath, .Lfunc_end2-zstrokepath
	.cfi_endproc

	.globl	zclippath
	.p2align	4, 0x90
	.type	zclippath,@function
zclippath:                              # @zclippath
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_clippath             # TAILCALL
.Lfunc_end3:
	.size	zclippath, .Lfunc_end3-zclippath
	.cfi_endproc

	.globl	zpathbbox
	.p2align	4, 0x90
	.type	zpathbbox,@function
zpathbbox:                              # @zpathbbox
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	igs(%rip), %rdi
	movq	%rsp, %rsi
	callq	gs_pathbbox
	testl	%eax, %eax
	js	.LBB4_4
# BB#1:
	leaq	64(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB4_3
# BB#2:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB4_4
.LBB4_3:
	movl	(%rsp), %eax
	movl	%eax, 16(%rbx)
	movw	$44, 24(%rbx)
	movl	4(%rsp), %eax
	movl	%eax, 32(%rbx)
	movw	$44, 40(%rbx)
	movl	8(%rsp), %eax
	movl	%eax, 48(%rbx)
	movw	$44, 56(%rbx)
	movl	12(%rsp), %eax
	movl	%eax, 64(%rbx)
	movw	$44, 72(%rbx)
	xorl	%eax, %eax
.LBB4_4:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end4:
	.size	zpathbbox, .Lfunc_end4-zpathbbox
	.cfi_endproc

	.globl	zpathforall
	.p2align	4, 0x90
	.type	zpathforall,@function
zpathforall:                            # @zpathforall
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -24
.Lcfi7:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$-17, %eax
	cmpq	%rbx, osp_nargs+24(%rip)
	ja	.LBB5_4
# BB#1:
	movl	$128, %ecx
	addq	esp(%rip), %rcx
	movl	$-5, %eax
	cmpq	estop(%rip), %rcx
	ja	.LBB5_4
# BB#2:
	movl	gs_path_enum_sizeof(%rip), %esi
	movl	$1, %edi
	movl	$.L.str, %edx
	callq	alloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB5_3
# BB#5:
	movq	igs(%rip), %rsi
	movq	%r14, %rdi
	callq	gs_path_enum_init
	movq	esp(%rip), %rax
	movw	$2, 16(%rax)
	movw	$33, 24(%rax)
	leaq	32(%rax), %rcx
	movq	%rcx, esp(%rip)
	movups	-48(%rbx), %xmm0
	movups	%xmm0, 32(%rax)
	leaq	48(%rax), %rcx
	movq	%rcx, esp(%rip)
	movups	-32(%rbx), %xmm0
	movups	%xmm0, 48(%rax)
	leaq	64(%rax), %rcx
	movq	%rcx, esp(%rip)
	movups	-16(%rbx), %xmm0
	movups	%xmm0, 64(%rax)
	leaq	80(%rax), %rcx
	movq	%rcx, esp(%rip)
	movups	(%rbx), %xmm0
	movups	%xmm0, 80(%rax)
	leaq	96(%rax), %rcx
	movq	%rcx, esp(%rip)
	movq	%r14, 96(%rax)
	movw	$20, 104(%rax)
	addq	$-64, osp(%rip)
	addq	$-64, %rbx
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	path_continue           # TAILCALL
.LBB5_3:
	movl	$-25, %eax
.LBB5_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	zpathforall, .Lfunc_end5-zpathforall
	.cfi_endproc

	.globl	path_continue
	.p2align	4, 0x90
	.type	path_continue,@function
path_continue:                          # @path_continue
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 64
.Lcfi13:
	.cfi_offset %rbx, -40
.Lcfi14:
	.cfi_offset %r14, -32
.Lcfi15:
	.cfi_offset %r15, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	esp(%rip), %rax
	movq	(%rax), %rbx
	movq	%rsp, %r15
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	gs_path_enum_next
	movl	%eax, %ebp
	cmpl	$4, %ebp
	ja	.LBB6_15
# BB#1:
	movl	%ebp, %eax
	jmpq	*.LJTI6_0(,%rax,8)
.LBB6_2:
	movl	gs_path_enum_sizeof(%rip), %edx
	movl	$1, %ebp
	movl	$1, %esi
	movl	$.L.str, %ecx
	movq	%rbx, %rdi
	callq	alloc_free
	addq	$-96, esp(%rip)
	jmp	.LBB6_15
.LBB6_5:
	movq	esp(%rip), %rax
	movups	-48(%rax), %xmm0
	movups	%xmm0, 32(%rax)
	leaq	32(%r14), %rcx
	cmpq	ostop(%rip), %rcx
	jbe	.LBB6_12
# BB#6:
	movq	%r14, %rdx
	jmp	.LBB6_11
.LBB6_9:                                # %.thread
	movq	esp(%rip), %rax
	movups	-16(%rax), %xmm0
	movups	%xmm0, 32(%rax)
	jmp	.LBB6_14
.LBB6_3:
	movq	esp(%rip), %rax
	movups	-64(%rax), %xmm0
	movups	%xmm0, 32(%rax)
	leaq	32(%r14), %rcx
	cmpq	ostop(%rip), %rcx
	jbe	.LBB6_12
# BB#4:
	movq	%r14, %rdx
	jmp	.LBB6_11
.LBB6_12:                               # %pf_push.exit24.loopexit45
	movl	$2, %edx
	xorl	%esi, %esi
	movl	$1, %edi
.LBB6_13:                               # %pf_push.exit24
	movl	(%r15), %ebp
	shlq	$4, %rdi
	movl	%ebp, (%r14,%rdi)
	movw	$44, 8(%r14,%rdi)
	movl	4(%rsp,%rsi,8), %esi
	movl	%esi, (%rcx)
	shlq	$4, %rdx
	movw	$44, 8(%r14,%rdx)
	movq	%rcx, osp(%rip)
.LBB6_14:
	movq	$path_continue, 16(%rax)
	movw	$37, 24(%rax)
	movw	$0, 26(%rax)
	addq	$32, %rax
	movq	%rax, esp(%rip)
	movl	$1, %ebp
	jmp	.LBB6_15
.LBB6_7:
	movq	esp(%rip), %rax
	movups	-32(%rax), %xmm0
	movups	%xmm0, 32(%rax)
	movq	ostop(%rip), %rsi
	leaq	32(%r14), %rdx
	cmpq	%rsi, %rdx
	jbe	.LBB6_10
# BB#8:
	movq	%r14, %rdx
	jmp	.LBB6_11
.LBB6_10:
	movl	(%rsp), %ecx
	movl	%ecx, 16(%r14)
	movw	$44, 24(%r14)
	movl	4(%rsp), %ecx
	movl	%ecx, 32(%r14)
	movw	$44, 40(%r14)
	leaq	64(%r14), %rdi
	cmpq	%rsi, %rdi
	ja	.LBB6_11
# BB#16:
	movl	8(%rsp), %ecx
	movl	%ecx, 48(%r14)
	movw	$44, 56(%r14)
	movl	12(%rsp), %ecx
	movl	%ecx, 64(%r14)
	movw	$44, 72(%r14)
	leaq	96(%r14), %rcx
	cmpq	%rsi, %rcx
	movq	%rdi, %rdx
	jbe	.LBB6_17
.LBB6_11:                               # %pf_push.exit24.thread
	movq	%rdx, osp(%rip)
	movl	$-16, %ebp
.LBB6_15:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_17:                               # %pf_push.exit24.loopexit3647
	leaq	16(%rsp), %r15
	movl	$6, %edx
	movl	$2, %esi
	movl	$5, %edi
	jmp	.LBB6_13
.Lfunc_end6:
	.size	path_continue, .Lfunc_end6-path_continue
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_2
	.quad	.LBB6_3
	.quad	.LBB6_5
	.quad	.LBB6_7
	.quad	.LBB6_9

	.text
	.globl	pf_push
	.p2align	4, 0x90
	.type	pf_push,@function
pf_push:                                # @pf_push
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	je	.LBB7_1
# BB#2:                                 # %.lr.ph
	movq	ostop(%rip), %rax
	addq	$32, %rdx
	.p2align	4, 0x90
.LBB7_3:                                # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %rdx
	ja	.LBB7_4
# BB#5:                                 #   in Loop: Header=BB7_3 Depth=1
	decl	%esi
	movl	(%rdi), %ecx
	movl	%ecx, -16(%rdx)
	movw	$44, -8(%rdx)
	movl	4(%rdi), %ecx
	movl	%ecx, (%rdx)
	movw	$44, 8(%rdx)
	addq	$32, %rdx
	addq	$8, %rdi
	testl	%esi, %esi
	jne	.LBB7_3
# BB#6:                                 # %.loopexit.sink.split.loopexit
	addq	$-32, %rdx
	xorl	%eax, %eax
	movq	%rdx, osp(%rip)
	retq
.LBB7_1:
	xorl	%eax, %eax
	retq
.LBB7_4:
	movq	%rdx, osp(%rip)
	addq	$-32, %rdx
	movl	$-16, %eax
	movq	%rdx, osp(%rip)
	retq
.Lfunc_end7:
	.size	pf_push, .Lfunc_end7-pf_push
	.cfi_endproc

	.globl	zinitclip
	.p2align	4, 0x90
	.type	zinitclip,@function
zinitclip:                              # @zinitclip
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_initclip             # TAILCALL
.Lfunc_end8:
	.size	zinitclip, .Lfunc_end8-zinitclip
	.cfi_endproc

	.globl	zclip
	.p2align	4, 0x90
	.type	zclip,@function
zclip:                                  # @zclip
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_clip                 # TAILCALL
.Lfunc_end9:
	.size	zclip, .Lfunc_end9-zclip
	.cfi_endproc

	.globl	zeoclip
	.p2align	4, 0x90
	.type	zeoclip,@function
zeoclip:                                # @zeoclip
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_eoclip               # TAILCALL
.Lfunc_end10:
	.size	zeoclip, .Lfunc_end10-zeoclip
	.cfi_endproc

	.globl	zpath2_op_init
	.p2align	4, 0x90
	.type	zpath2_op_init,@function
zpath2_op_init:                         # @zpath2_op_init
	.cfi_startproc
# BB#0:
	movl	$zpath2_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end11:
	.size	zpath2_op_init, .Lfunc_end11-zpath2_op_init
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"pathforall"
	.size	.L.str, 11

	.type	zpath2_op_init.my_defs,@object # @zpath2_op_init.my_defs
	.data
	.p2align	4
zpath2_op_init.my_defs:
	.quad	.L.str.1
	.quad	zclip
	.quad	.L.str.2
	.quad	zclippath
	.quad	.L.str.3
	.quad	zeoclip
	.quad	.L.str.4
	.quad	zflattenpath
	.quad	.L.str.5
	.quad	zinitclip
	.quad	.L.str.6
	.quad	zpathbbox
	.quad	.L.str.7
	.quad	zpathforall
	.quad	.L.str.8
	.quad	zreversepath
	.quad	.L.str.9
	.quad	zstrokepath
	.zero	16
	.size	zpath2_op_init.my_defs, 160

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"0clip"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"0clippath"
	.size	.L.str.2, 10

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"0eoclip"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"0flattenpath"
	.size	.L.str.4, 13

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"0initclip"
	.size	.L.str.5, 10

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"0pathbbox"
	.size	.L.str.6, 10

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"4pathforall"
	.size	.L.str.7, 12

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"0reversepath"
	.size	.L.str.8, 13

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"0strokepath"
	.size	.L.str.9, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
