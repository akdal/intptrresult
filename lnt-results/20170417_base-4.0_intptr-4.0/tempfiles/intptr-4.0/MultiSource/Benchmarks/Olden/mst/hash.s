	.text
	.file	"hash.bc"
	.globl	MakeHash
	.p2align	4, 0x90
	.type	MakeHash,@function
MakeHash:                               # @MakeHash
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %r15d
	movl	remaining(%rip), %ecx
	cmpl	$23, %ecx
	jg	.LBB0_4
# BB#1:
	movl	$32768, %edi            # imm = 0x8000
	callq	malloc
	movq	%rax, temp(%rip)
	testq	%rax, %rax
	jne	.LBB0_3
# BB#2:
	movl	$.Lstr, %edi
	callq	puts
.LBB0_3:
	movl	$32768, remaining(%rip) # imm = 0x8000
	movl	$32768, %ecx            # imm = 0x8000
.LBB0_4:                                # %localmalloc.exit
	movq	temp(%rip), %rbx
	leaq	24(%rbx), %rax
	movq	%rax, temp(%rip)
	addl	$-24, %ecx
	movl	%ecx, remaining(%rip)
	leal	(,%r15,8), %ebp
	cmpl	%ebp, %ecx
	jge	.LBB0_8
# BB#5:
	movl	$32768, %edi            # imm = 0x8000
	callq	malloc
	movq	%rax, temp(%rip)
	testq	%rax, %rax
	jne	.LBB0_7
# BB#6:
	movl	$.Lstr, %edi
	callq	puts
	movq	temp(%rip), %rax
.LBB0_7:
	movl	$32768, remaining(%rip) # imm = 0x8000
	movl	$32768, %ecx            # imm = 0x8000
.LBB0_8:                                # %localmalloc.exit15
	movslq	%ebp, %rdx
	addq	%rax, %rdx
	movq	%rdx, temp(%rip)
	subl	%ebp, %ecx
	movl	%ecx, remaining(%rip)
	movq	%rax, (%rbx)
	testl	%r15d, %r15d
	jle	.LBB0_17
# BB#9:                                 # %.lr.ph.preheader
	movq	$0, (%rax)
	cmpl	$1, %r15d
	je	.LBB0_17
# BB#10:                                # %.lr.ph..lr.ph_crit_edge.preheader
	movl	%r15d, %eax
	leal	7(%rax), %esi
	leaq	-2(%rax), %rdx
	andq	$7, %rsi
	je	.LBB0_11
# BB#12:                                # %.lr.ph..lr.ph_crit_edge.prol.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph..lr.ph_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	$0, 8(%rdi,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB0_13
# BB#14:                                # %.lr.ph..lr.ph_crit_edge.prol.loopexit.unr-lcssa
	incq	%rcx
	cmpq	$7, %rdx
	jae	.LBB0_16
	jmp	.LBB0_17
.LBB0_11:
	movl	$1, %ecx
	cmpq	$7, %rdx
	jb	.LBB0_17
	.p2align	4, 0x90
.LBB0_16:                               # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movq	$0, (%rdx,%rcx,8)
	movq	(%rbx), %rdx
	movq	$0, 8(%rdx,%rcx,8)
	movq	(%rbx), %rdx
	movq	$0, 16(%rdx,%rcx,8)
	movq	(%rbx), %rdx
	movq	$0, 24(%rdx,%rcx,8)
	movq	(%rbx), %rdx
	movq	$0, 32(%rdx,%rcx,8)
	movq	(%rbx), %rdx
	movq	$0, 40(%rdx,%rcx,8)
	movq	(%rbx), %rdx
	movq	$0, 48(%rdx,%rcx,8)
	movq	(%rbx), %rdx
	movq	$0, 56(%rdx,%rcx,8)
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB0_16
.LBB0_17:                               # %._crit_edge
	movq	%r14, 8(%rbx)
	movl	%r15d, 16(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	MakeHash, .Lfunc_end0-MakeHash
	.cfi_endproc

	.globl	HashLookup
	.p2align	4, 0x90
	.type	HashLookup,@function
HashLookup:                             # @HashLookup
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movl	%edi, %ebx
	callq	*8(%r14)
	testl	%eax, %eax
	js	.LBB1_1
# BB#3:
	cmpl	16(%r14), %eax
	jge	.LBB1_4
# BB#5:
	movq	(%r14), %rcx
	cltq
	movq	(%rcx,%rax,8), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jne	.LBB1_7
	jmp	.LBB1_10
	.p2align	4, 0x90
.LBB1_8:                                #   in Loop: Header=BB1_7 Depth=1
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_10
.LBB1_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%ebx, (%rcx)
	jne	.LBB1_8
# BB#9:                                 # %.critedge
	movq	8(%rcx), %rax
.LBB1_10:                               # %.critedge17
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_1:
	movl	$.L.str, %edi
	movl	$1, %esi
	jmp	.LBB1_2
.LBB1_4:
	movl	$.L.str, %edi
	movl	$2, %esi
.LBB1_2:
	xorl	%eax, %eax
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end1:
	.size	HashLookup, .Lfunc_end1-HashLookup
	.cfi_endproc

	.globl	HashInsert
	.p2align	4, 0x90
	.type	HashInsert,@function
HashInsert:                             # @HashInsert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 48
.Lcfi19:
	.cfi_offset %rbx, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	%ebp, %edi
	callq	*8(%rbx)
	testl	%eax, %eax
	js	.LBB2_1
# BB#3:
	cmpl	16(%rbx), %eax
	jge	.LBB2_4
# BB#5:
	movq	(%rbx), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	testq	%rax, %rax
	jne	.LBB2_7
	jmp	.LBB2_9
	.p2align	4, 0x90
.LBB2_8:                                #   in Loop: Header=BB2_7 Depth=1
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB2_9
.LBB2_7:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%ebp, (%rax)
	jne	.LBB2_8
# BB#14:                                # %HashLookup.exit
	cmpq	$0, 8(%rax)
	jne	.LBB2_15
.LBB2_9:                                # %HashLookup.exit.thread
	movl	%ebp, %edi
	callq	*8(%rbx)
	movl	%eax, %r15d
	movl	remaining(%rip), %eax
	cmpl	$23, %eax
	jg	.LBB2_13
# BB#10:
	movl	$32768, %edi            # imm = 0x8000
	callq	malloc
	movq	%rax, temp(%rip)
	testq	%rax, %rax
	jne	.LBB2_12
# BB#11:
	movl	$.Lstr, %edi
	callq	puts
.LBB2_12:
	movl	$32768, remaining(%rip) # imm = 0x8000
	movl	$32768, %eax            # imm = 0x8000
.LBB2_13:                               # %localmalloc.exit
	movq	temp(%rip), %rcx
	leaq	24(%rcx), %rdx
	movq	%rdx, temp(%rip)
	addl	$-24, %eax
	movl	%eax, remaining(%rip)
	movq	(%rbx), %rax
	movslq	%r15d, %rdx
	movq	(%rax,%rdx,8), %rsi
	movq	%rsi, 16(%rcx)
	movq	%rcx, (%rax,%rdx,8)
	movl	%ebp, (%rcx)
	movq	%r14, 8(%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_1:
	movl	$.L.str, %edi
	movl	$1, %esi
	jmp	.LBB2_2
.LBB2_4:
	movl	$.L.str, %edi
	movl	$2, %esi
	jmp	.LBB2_2
.LBB2_15:
	movl	$.L.str, %edi
	movl	$3, %esi
.LBB2_2:
	xorl	%eax, %eax
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end2:
	.size	HashInsert, .Lfunc_end2-HashInsert
	.cfi_endproc

	.globl	HashDelete
	.p2align	4, 0x90
	.type	HashDelete,@function
HashDelete:                             # @HashDelete
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movl	%edi, %ebx
	callq	*8(%r14)
	movq	(%r14), %rcx
	movslq	%eax, %rdx
	movq	(%rcx,%rdx,8), %rax
	testq	%rax, %rax
	je	.LBB3_4
# BB#1:                                 # %.lr.ph.preheader
	leaq	(%rcx,%rdx,8), %rcx
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%ebx, (%rax)
	leaq	16(%rax), %rdx
	je	.LBB3_5
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	(%rdx), %rax
	testq	%rax, %rax
	movq	%rdx, %rcx
	jne	.LBB3_2
.LBB3_4:                                # %.loopexit21
	movl	$.L.str, %edi
	movl	$4, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$-1, %edi
	callq	exit
.LBB3_5:                                # %.loopexit
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	HashDelete, .Lfunc_end3-HashDelete
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Assertion failure:%d in hash\n"
	.size	.L.str, 30

	.type	remaining,@object       # @remaining
	.local	remaining
	.comm	remaining,4,4
	.type	temp,@object            # @temp
	.local	temp
	.comm	temp,8,8
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Error! malloc returns null"
	.size	.Lstr, 27


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
