	.text
	.file	"rawdaudio.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_3:                                #   in Loop: Header=BB0_1 Depth=1
	leal	(%rbx,%rbx), %edx
	movl	$abuf, %edi
	movl	$sbuf, %esi
	movl	$state, %ecx
	callq	adpcm_decoder
	shll	$2, %ebx
	movslq	%ebx, %rdx
	movl	$1, %edi
	movl	$sbuf, %esi
	callq	write
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	xorl	%edi, %edi
	movl	$abuf, %esi
	movl	$500, %edx              # imm = 0x1F4
	callq	read
	movq	%rax, %rbx
	testl	%ebx, %ebx
	js	.LBB0_4
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB0_1 Depth=1
	testl	%ebx, %ebx
	jne	.LBB0_3
# BB#5:
	movq	stderr(%rip), %rdi
	movswl	state(%rip), %edx
	movsbl	state+2(%rip), %ecx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
	xorl	%edi, %edi
	callq	exit
.LBB0_4:                                # %._crit_edge
	movl	$.L.str, %edi
	callq	perror
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	abuf,@object            # @abuf
	.comm	abuf,500,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"input file"
	.size	.L.str, 11

	.type	sbuf,@object            # @sbuf
	.comm	sbuf,2000,16
	.type	state,@object           # @state
	.comm	state,4,2
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Final valprev=%d, index=%d\n"
	.size	.L.str.1, 28


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
