	.text
	.file	"btConvexConvexAlgorithm.bc"
	.globl	_ZN23btConvexConvexAlgorithm10CreateFuncC2EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.p2align	4, 0x90
	.type	_ZN23btConvexConvexAlgorithm10CreateFuncC2EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver,@function
_ZN23btConvexConvexAlgorithm10CreateFuncC2EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver: # @_ZN23btConvexConvexAlgorithm10CreateFuncC2EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.cfi_startproc
# BB#0:
	movb	$0, 8(%rdi)
	movq	$_ZTVN23btConvexConvexAlgorithm10CreateFuncE+16, (%rdi)
	movl	$0, 32(%rdi)
	movl	$3, 36(%rdi)
	movq	%rsi, 24(%rdi)
	movq	%rdx, 16(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN23btConvexConvexAlgorithm10CreateFuncC2EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver, .Lfunc_end0-_ZN23btConvexConvexAlgorithm10CreateFuncC2EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.cfi_endproc

	.globl	_ZN23btConvexConvexAlgorithm10CreateFuncD2Ev
	.p2align	4, 0x90
	.type	_ZN23btConvexConvexAlgorithm10CreateFuncD2Ev,@function
_ZN23btConvexConvexAlgorithm10CreateFuncD2Ev: # @_ZN23btConvexConvexAlgorithm10CreateFuncD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN23btConvexConvexAlgorithm10CreateFuncD2Ev, .Lfunc_end1-_ZN23btConvexConvexAlgorithm10CreateFuncD2Ev
	.cfi_endproc

	.globl	_ZN23btConvexConvexAlgorithm10CreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN23btConvexConvexAlgorithm10CreateFuncD0Ev,@function
_ZN23btConvexConvexAlgorithm10CreateFuncD0Ev: # @_ZN23btConvexConvexAlgorithm10CreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end2:
	.size	_ZN23btConvexConvexAlgorithm10CreateFuncD0Ev, .Lfunc_end2-_ZN23btConvexConvexAlgorithm10CreateFuncD0Ev
	.cfi_endproc

	.globl	_ZN23btConvexConvexAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii
	.p2align	4, 0x90
	.type	_ZN23btConvexConvexAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii,@function
_ZN23btConvexConvexAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii: # @_ZN23btConvexConvexAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 64
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r13, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%r9, %r14
	movq	%r8, %r12
	movq	%rcx, %r13
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	%rdx, %rsi
	movq	%r13, %rdx
	movq	%r12, %rcx
	callq	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	movq	$_ZTV23btConvexConvexAlgorithm+16, (%rbx)
	movq	200(%r13), %rdi
	movq	(%rdi), %rax
.Ltmp0:
	callq	*32(%rax)
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
.Ltmp1:
# BB#1:
	movq	200(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp2:
	callq	*32(%rax)
.Ltmp3:
# BB#2:
	movl	80(%rsp), %eax
	movl	72(%rsp), %ecx
	movq	64(%rsp), %rdx
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 96(%rbx)
	movss	%xmm0, 100(%rbx)
	movl	$0, 104(%rbx)
	movq	%r14, 112(%rbx)
	movq	%rdx, 120(%rbx)
	movb	$0, 128(%rbx)
	movq	%r15, 136(%rbx)
	movb	$0, 144(%rbx)
	movl	%ecx, 148(%rbx)
	movl	%eax, 152(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB3_3:
.Ltmp4:
	movq	%rax, %r14
.Ltmp5:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp6:
# BB#4:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_5:
.Ltmp7:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN23btConvexConvexAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii, .Lfunc_end3-_ZN23btConvexConvexAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	1                       #   On action: 1
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end4:
	.size	__clang_call_terminate, .Lfunc_end4-__clang_call_terminate

	.text
	.globl	_ZN23btConvexConvexAlgorithmD2Ev
	.p2align	4, 0x90
	.type	_ZN23btConvexConvexAlgorithmD2Ev,@function
_ZN23btConvexConvexAlgorithmD2Ev:       # @_ZN23btConvexConvexAlgorithmD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV23btConvexConvexAlgorithm+16, (%rbx)
	cmpb	$0, 128(%rbx)
	je	.LBB5_3
# BB#1:
	movq	136(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB5_3
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp8:
	callq	*32(%rax)
.Ltmp9:
.LBB5_3:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN30btActivatingCollisionAlgorithmD2Ev # TAILCALL
.LBB5_4:
.Ltmp10:
	movq	%rax, %r14
.Ltmp11:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp12:
# BB#5:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_6:
.Ltmp13:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN23btConvexConvexAlgorithmD2Ev, .Lfunc_end5-_ZN23btConvexConvexAlgorithmD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin1   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp11-.Ltmp9          #   Call between .Ltmp9 and .Ltmp11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	1                       #   On action: 1
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp12     #   Call between .Ltmp12 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN23btConvexConvexAlgorithmD0Ev
	.p2align	4, 0x90
	.type	_ZN23btConvexConvexAlgorithmD0Ev,@function
_ZN23btConvexConvexAlgorithmD0Ev:       # @_ZN23btConvexConvexAlgorithmD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV23btConvexConvexAlgorithm+16, (%rbx)
	cmpb	$0, 128(%rbx)
	je	.LBB6_3
# BB#1:
	movq	136(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB6_3
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp14:
	callq	*32(%rax)
.Ltmp15:
.LBB6_3:
.Ltmp20:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp21:
# BB#4:                                 # %_ZN23btConvexConvexAlgorithmD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB6_5:
.Ltmp16:
	movq	%rax, %r14
.Ltmp17:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp18:
	jmp	.LBB6_8
.LBB6_6:
.Ltmp19:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_7:
.Ltmp22:
	movq	%rax, %r14
.LBB6_8:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN23btConvexConvexAlgorithmD0Ev, .Lfunc_end6-_ZN23btConvexConvexAlgorithmD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin2   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin2   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin2   #     jumps to .Ltmp19
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp18     #   Call between .Ltmp18 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN23btConvexConvexAlgorithm19setLowLevelOfDetailEb
	.p2align	4, 0x90
	.type	_ZN23btConvexConvexAlgorithm19setLowLevelOfDetailEb,@function
_ZN23btConvexConvexAlgorithm19setLowLevelOfDetailEb: # @_ZN23btConvexConvexAlgorithm19setLowLevelOfDetailEb
	.cfi_startproc
# BB#0:
	movb	%sil, 144(%rdi)
	retq
.Lfunc_end7:
	.size	_ZN23btConvexConvexAlgorithm19setLowLevelOfDetailEb, .Lfunc_end7-_ZN23btConvexConvexAlgorithm19setLowLevelOfDetailEb
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_0:
	.long	1566444395              # float 9.99999984E+17
.LCPI8_1:
	.long	872415232               # float 1.1920929E-7
.LCPI8_2:
	.long	1065353216              # float 1
.LCPI8_4:
	.long	1060439283              # float 0.707106769
.LCPI8_6:
	.long	1053364187              # float 0.392699093
.LCPI8_7:
	.long	1056964608              # float 0.5
.LCPI8_8:
	.long	1086918619              # float 6.28318548
.LCPI8_9:
	.long	1073741824              # float 2
.LCPI8_10:
	.long	679477248               # float 1.42108547E-14
.LCPI8_11:
	.long	3212836864              # float -1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_3:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI8_5:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN23btConvexConvexAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN23btConvexConvexAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN23btConvexConvexAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN23btConvexConvexAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$1064, %rsp             # imm = 0x428
.Lcfi27:
	.cfi_def_cfa_offset 1120
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%r8, %rbp
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	136(%r12), %rax
	testq	%rax, %rax
	jne	.LBB8_2
# BB#1:
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	*24(%rax)
	movq	%rax, 136(%r12)
	movb	$1, 128(%r12)
.LBB8_2:
	movq	%rax, 8(%rbp)
	movq	200(%rbx), %r14
	movq	200(%r15), %r13
	cmpl	$10, 8(%r14)
	jne	.LBB8_11
# BB#3:
	cmpl	$10, 8(%r13)
	jne	.LBB8_11
# BB#4:
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*56(%rax)
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*56(%rax)
	movq	136(%r12), %rdi
	callq	_ZNK20btPersistentManifold27getContactBreakingThresholdEv
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movslq	64(%r14), %rsi
	leal	2(%rsi), %eax
	movslq	%eax, %rcx
	imulq	$1431655766, %rcx, %rax # imm = 0x55555556
	movq	%rax, %rdx
	shrq	$63, %rdx
	shrq	$32, %rax
	addl	%edx, %eax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %ecx
	movslq	64(%r13), %rax
	leal	2(%rax), %edx
	movslq	%edx, %rdi
	imulq	$1431655766, %rdi, %rdx # imm = 0x55555556
	movq	%rdx, %rbp
	shrq	$63, %rbp
	shrq	$32, %rdx
	addl	%ebp, %edx
	leal	(%rdx,%rdx,2), %edx
	subl	%edx, %edi
	movss	40(%r14,%rsi,4), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movslq	%ecx, %rdx
	movslq	%edi, %rcx
	movss	8(%rbx,%rsi,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	24(%rbx,%rsi,4), %xmm12 # xmm12 = mem[0],zero,zero,zero
	movss	40(%rbx,%rsi,4), %xmm14 # xmm14 = mem[0],zero,zero,zero
	movsd	56(%rbx), %xmm1         # xmm1 = mem[0],zero
	movss	8(%r15,%rax,4), %xmm15  # xmm15 = mem[0],zero,zero,zero
	movss	24(%r15,%rax,4), %xmm9  # xmm9 = mem[0],zero,zero,zero
	movss	40(%r15,%rax,4), %xmm11 # xmm11 = mem[0],zero,zero,zero
	movsd	56(%r15), %xmm10        # xmm10 = mem[0],zero
	movss	64(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subps	%xmm1, %xmm10
	movaps	%xmm10, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	subss	64(%rbx), %xmm6
	movaps	%xmm0, %xmm1
	mulss	%xmm15, %xmm1
	movaps	%xmm12, %xmm2
	mulss	%xmm9, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm14, %xmm4
	mulss	%xmm11, %xmm4
	addss	%xmm2, %xmm4
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movaps	%xmm0, %xmm1
	mulss	%xmm10, %xmm1
	movaps	%xmm12, %xmm2
	mulss	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm14, %xmm5
	mulss	%xmm6, %xmm5
	addss	%xmm2, %xmm5
	movaps	%xmm15, %xmm1
	mulss	%xmm10, %xmm1
	mulss	%xmm9, %xmm3
	addss	%xmm1, %xmm3
	movaps	%xmm11, %xmm7
	movss	%xmm6, 8(%rsp)          # 4-byte Spill
	mulss	%xmm6, %xmm7
	addss	%xmm3, %xmm7
	movaps	%xmm4, %xmm2
	mulss	%xmm2, %xmm2
	movss	.LCPI8_2(%rip), %xmm6   # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm1
	subss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm1
	movss	40(%r14,%rdx,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movss	40(%r13,%rax,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movss	40(%r13,%rcx,4), %xmm13 # xmm13 = mem[0],zero,zero,zero
	jne	.LBB8_5
	jnp	.LBB8_7
.LBB8_5:
	movaps	%xmm4, %xmm2
	mulss	%xmm7, %xmm2
	movaps	%xmm5, %xmm3
	subss	%xmm2, %xmm3
	divss	%xmm1, %xmm3
	movaps	.LCPI8_5(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm8, %xmm2
	ucomiss	%xmm3, %xmm2
	ja	.LBB8_7
# BB#6:
	ucomiss	%xmm8, %xmm3
	movaps	%xmm8, %xmm2
	jbe	.LBB8_8
.LBB8_7:                                # %.sink.split.i.i
	movaps	%xmm2, %xmm3
.LBB8_8:
	movaps	%xmm4, %xmm1
	mulss	%xmm3, %xmm1
	subss	%xmm7, %xmm1
	movaps	%xmm1, %xmm7
	movaps	.LCPI8_5(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm1
	ucomiss	%xmm7, %xmm1
	jbe	.LBB8_58
# BB#9:
	movaps	%xmm1, %xmm7
	mulss	%xmm1, %xmm4
	movaps	%xmm4, %xmm3
	addss	%xmm5, %xmm3
	movaps	.LCPI8_5(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm8, %xmm1
	ucomiss	%xmm3, %xmm1
	movq	48(%rsp), %rbx          # 8-byte Reload
	jbe	.LBB8_63
# BB#10:
	movaps	%xmm1, %xmm3
	jmp	.LBB8_65
.LBB8_11:
	leaq	16(%r12), %rdi
	leaq	8(%rbx), %rsi
	leaq	8(%r15), %rdx
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	callq	_ZN30btConvexSeparatingDistanceUtil24updateSeparatingDistanceERK11btTransformS2_
	movq	64(%rsp), %rax          # 8-byte Reload
	cmpb	$0, 40(%rax)
	je	.LBB8_13
# BB#12:
	xorps	%xmm0, %xmm0
	ucomiss	104(%r12), %xmm0
	jb	.LBB8_54
.LBB8_13:
	movl	$1566444395, 416(%rsp)  # imm = 0x5D5E0B6B
	movq	$0, 424(%rsp)
	movq	112(%r12), %rcx
	movq	120(%r12), %r8
	leaq	576(%rsp), %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	movq	%r14, 616(%rsp)
	movq	%r13, 624(%rsp)
	movq	64(%rsp), %rax          # 8-byte Reload
	cmpb	$0, 40(%rax)
	je	.LBB8_15
# BB#14:
	movss	.LCPI8_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	jmp	.LBB8_19
.LBB8_15:
	movq	(%r14), %rax
.Ltmp23:
	movq	%r14, %rdi
	callq	*88(%rax)
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
.Ltmp24:
# BB#16:
	movq	(%r13), %rax
.Ltmp25:
	movq	%r13, %rdi
	callq	*88(%rax)
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
.Ltmp26:
# BB#17:
	movq	136(%r12), %rdi
.Ltmp27:
	callq	_ZNK20btPersistentManifold27getContactBreakingThresholdEv
.Ltmp28:
# BB#18:
	movss	32(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	8(%rsp), %xmm1          # 4-byte Folded Reload
	addss	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
.LBB8_19:
	movss	%xmm1, 416(%rsp)
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	48(%rcx), %rax
	movq	%rax, 424(%rsp)
	movq	144(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm0
	movaps	%xmm0, 288(%rsp)
	movups	24(%rbx), %xmm0
	movaps	%xmm0, 304(%rsp)
	movups	40(%rbx), %xmm0
	movaps	%xmm0, 320(%rsp)
	movups	56(%rbx), %xmm0
	movaps	%xmm0, 336(%rsp)
	movq	152(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm0
	movaps	%xmm0, 352(%rsp)
	movups	24(%r15), %xmm0
	movaps	%xmm0, 368(%rsp)
	movups	40(%r15), %xmm0
	movaps	%xmm0, 384(%rsp)
	movups	56(%r15), %xmm0
	movaps	%xmm0, 400(%rsp)
	movq	24(%rcx), %rcx
.Ltmp29:
	leaq	576(%rsp), %rdi
	leaq	288(%rsp), %rsi
	xorl	%r8d, %r8d
	movq	%rbp, %rdx
	callq	_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
.Ltmp30:
# BB#20:
	movq	64(%rsp), %rax          # 8-byte Reload
	cmpb	$0, 40(%rax)
	je	.LBB8_28
# BB#21:
	movss	652(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	.LCPI8_1(%rip), %xmm2
	jbe	.LBB8_29
# BB#22:
	movq	64(%rsp), %rax          # 8-byte Reload
	movss	44(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	584(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	588(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	592(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB8_24
# BB#23:                                # %call.sqrt463
	movss	%xmm2, 20(%rsp)         # 4-byte Spill
	movss	%xmm6, 32(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	32(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB8_24:                               # %.split462
	movss	.LCPI8_2(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	divss	%xmm1, %xmm5
	movsd	584(%rsp), %xmm0        # xmm0 = mem[0],zero
	movaps	%xmm5, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm0, %xmm3
	movaps	%xmm3, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	mulss	592(%rsp), %xmm5
	xorps	%xmm0, %xmm0
	movss	%xmm5, %xmm0            # xmm0 = xmm5[0],xmm0[1,2,3]
	movaps	%xmm0, 272(%rsp)        # 16-byte Spill
	addss	%xmm6, %xmm2
	movaps	.LCPI8_3(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	andps	%xmm5, %xmm0
	ucomiss	.LCPI8_4(%rip), %xmm0
	jbe	.LBB8_72
# BB#25:
	movaps	%xmm1, %xmm6
	mulss	%xmm1, %xmm1
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB8_27
# BB#26:                                # %call.sqrt465
	movss	%xmm2, 20(%rsp)         # 4-byte Spill
	movaps	%xmm3, 192(%rsp)        # 16-byte Spill
	movaps	%xmm5, 256(%rsp)        # 16-byte Spill
	movaps	%xmm6, 176(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	176(%rsp), %xmm6        # 16-byte Reload
	movaps	256(%rsp), %xmm5        # 16-byte Reload
	movss	.LCPI8_2(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movaps	192(%rsp), %xmm3        # 16-byte Reload
	movss	20(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB8_27:                               # %.split464
	divss	%xmm1, %xmm4
	mulss	%xmm4, %xmm5
	xorps	.LCPI8_5(%rip), %xmm5
	mulss	%xmm4, %xmm6
	xorps	%xmm0, %xmm0
	movss	%xmm0, 28(%rsp)         # 4-byte Spill
	movaps	%xmm6, %xmm0
	movaps	%xmm5, %xmm1
	jmp	.LBB8_30
.LBB8_28:
	xorps	%xmm2, %xmm2
.LBB8_29:
                                        # implicit-def: %XMM3
                                        # implicit-def: %XMM0
	movaps	%xmm0, 272(%rsp)        # 16-byte Spill
                                        # implicit-def: %XMM0
	movss	%xmm0, 28(%rsp)         # 4-byte Spill
                                        # implicit-def: %XMM1
                                        # implicit-def: %XMM0
.LBB8_30:                               # %_Z13btPlaneSpace1RK9btVector3RS_S2_.exit
	leaq	56(%rbx), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	leaq	56(%r15), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movq	8(%rbp), %rax
	movl	728(%rax), %eax
	cmpl	152(%r12), %eax
	jge	.LBB8_48
# BB#31:
	movaps	%xmm1, 256(%rsp)        # 16-byte Spill
	movaps	%xmm0, 176(%rsp)        # 16-byte Spill
	movaps	%xmm3, 192(%rsp)        # 16-byte Spill
	movss	%xmm2, 20(%rsp)         # 4-byte Spill
	movq	(%r14), %rax
.Ltmp32:
	movq	%r14, %rdi
	callq	*32(%rax)
	movss	%xmm0, 60(%rsp)         # 4-byte Spill
.Ltmp33:
# BB#32:
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	(%r13), %rax
.Ltmp35:
	movq	%r13, %rdi
	callq	*32(%rax)
.Ltmp36:
# BB#33:
	leaq	304(%rsp), %rbp
	movss	gContactBreakingThreshold(%rip), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movss	%xmm0, 92(%rsp)         # 4-byte Spill
	ucomiss	60(%rsp), %xmm0         # 4-byte Folded Reload
	jbe	.LBB8_35
# BB#34:
	movaps	288(%rsp), %xmm1
	movaps	%xmm1, 464(%rsp)
	movups	(%rbp), %xmm1
	movaps	%xmm1, 480(%rsp)
	movups	16(%rbp), %xmm1
	movaps	%xmm1, 496(%rsp)
	movups	32(%rbp), %xmm1
	jmp	.LBB8_36
.LBB8_35:
	movups	48(%rbp), %xmm1
	movaps	%xmm1, 464(%rsp)
	movups	64(%rbp), %xmm1
	movaps	%xmm1, 480(%rsp)
	movups	80(%rbp), %xmm1
	movaps	%xmm1, 496(%rsp)
	movups	96(%rbp), %xmm1
.LBB8_36:                               # %.preheader
	movaps	%xmm1, 512(%rsp)
	cmpl	$0, 148(%r12)
	jle	.LBB8_47
# BB#37:                                # %.lr.ph
	movss	60(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm1
	movss	92(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	minss	%xmm0, %xmm1
	divss	%xmm1, %xmm3
	movss	.LCPI8_6(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	leaq	24(%rbx), %rax
	movq	%rax, 456(%rsp)         # 8-byte Spill
	leaq	40(%rbx), %rax
	movq	%rax, 448(%rsp)         # 8-byte Spill
	leaq	24(%r15), %rax
	movq	%rax, 440(%rsp)         # 8-byte Spill
	leaq	40(%r15), %rax
	movq	%rax, 432(%rsp)         # 8-byte Spill
	minss	%xmm3, %xmm4
	ucomiss	%xmm2, %xmm0
	seta	27(%rsp)                # 1-byte Folded Spill
	movss	28(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	movaps	256(%rsp), %xmm1        # 16-byte Reload
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	176(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm0, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	mulss	.LCPI8_7(%rip), %xmm4
	movss	%xmm4, 224(%rsp)        # 4-byte Spill
	movaps	192(%rsp), %xmm1        # 16-byte Reload
	movaps	%xmm1, %xmm0
	mulss	%xmm0, %xmm0
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm1, 560(%rsp)        # 16-byte Spill
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	272(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	leaq	856(%rsp), %r14
	xorl	%r13d, %r13d
	movss	%xmm2, 164(%rsp)        # 4-byte Spill
	xorps	%xmm1, %xmm1
	sqrtss	%xmm2, %xmm1
	movss	%xmm1, 172(%rsp)        # 4-byte Spill
	movaps	%xmm0, 544(%rsp)        # 16-byte Spill
	sqrtss	%xmm0, %xmm0
	movss	%xmm0, 168(%rsp)        # 4-byte Spill
	.p2align	4, 0x90
.LBB8_38:                               # =>This Inner Loop Header: Depth=1
	movss	172(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm0
	jnp	.LBB8_40
# BB#39:                                # %call.sqrt469
                                        #   in Loop: Header=BB8_38 Depth=1
	movss	164(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	sqrtf
.LBB8_40:                               # %.split468
                                        #   in Loop: Header=BB8_38 Depth=1
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movss	224(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	sinf
	divss	32(%rsp), %xmm0         # 4-byte Folded Reload
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movss	224(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	cosf
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r13d, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	148(%r12), %xmm1
	movss	.LCPI8_8(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
	movss	168(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm0
	jnp	.LBB8_42
# BB#41:                                # %call.sqrt470
                                        #   in Loop: Header=BB8_38 Depth=1
	movaps	544(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
.LBB8_42:                               # %.split468.split
                                        #   in Loop: Header=BB8_38 Depth=1
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movss	28(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movss	%xmm0, 128(%rsp)        # 4-byte Spill
	movaps	256(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm1, %xmm0
	movss	%xmm0, 208(%rsp)        # 4-byte Spill
	mulss	176(%rsp), %xmm1        # 16-byte Folded Reload
	movss	%xmm1, 32(%rsp)         # 4-byte Spill
	mulss	.LCPI8_7(%rip), %xmm2
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	movaps	%xmm2, %xmm0
	callq	sinf
	movaps	%xmm0, %xmm1
	divss	4(%rsp), %xmm1          # 4-byte Folded Reload
	movaps	192(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm1, %xmm0
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movaps	560(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm1, %xmm0
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	mulss	272(%rsp), %xmm1        # 16-byte Folded Reload
	movss	%xmm1, 4(%rsp)          # 4-byte Spill
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	cosf
	movss	92(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	ucomiss	60(%rsp), %xmm1         # 4-byte Folded Reload
	jbe	.LBB8_44
# BB#43:                                #   in Loop: Header=BB8_38 Depth=1
	movss	128(%rsp), %xmm6        # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm1
	mulss	%xmm0, %xmm1
	movss	96(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm2
	movss	12(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm1
	movss	32(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm2
	movss	16(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	subss	%xmm2, %xmm1
	movss	208(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm8
	movss	4(%rsp), %xmm9          # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm8
	addss	%xmm1, %xmm8
	movaps	%xmm7, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm12, %xmm2
	mulss	%xmm10, %xmm2
	subss	%xmm2, %xmm1
	movaps	%xmm6, %xmm2
	mulss	%xmm9, %xmm2
	subss	%xmm2, %xmm1
	movaps	%xmm5, %xmm4
	mulss	%xmm11, %xmm4
	addss	%xmm1, %xmm4
	movaps	%xmm5, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm12, %xmm2
	mulss	%xmm9, %xmm2
	subss	%xmm2, %xmm1
	movaps	%xmm7, %xmm2
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm1
	movaps	%xmm6, %xmm3
	mulss	%xmm10, %xmm3
	addss	%xmm1, %xmm3
	mulss	%xmm0, %xmm12
	mulss	%xmm11, %xmm6
	addss	%xmm12, %xmm6
	mulss	%xmm10, %xmm7
	addss	%xmm6, %xmm7
	mulss	%xmm9, %xmm5
	addss	%xmm7, %xmm5
	movaps	%xmm11, %xmm1
	mulss	%xmm5, %xmm1
	movaps	%xmm0, %xmm2
	mulss	%xmm8, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm9, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm2, %xmm1
	movaps	%xmm10, %xmm2
	mulss	%xmm3, %xmm2
	subss	%xmm2, %xmm1
	movaps	%xmm10, %xmm2
	mulss	%xmm5, %xmm2
	movaps	%xmm0, %xmm6
	mulss	%xmm4, %xmm6
	addss	%xmm2, %xmm6
	movaps	%xmm11, %xmm2
	mulss	%xmm3, %xmm2
	addss	%xmm6, %xmm2
	movaps	%xmm9, %xmm6
	mulss	%xmm8, %xmm6
	subss	%xmm6, %xmm2
	movaps	%xmm9, %xmm6
	mulss	%xmm5, %xmm6
	movaps	%xmm0, %xmm7
	mulss	%xmm3, %xmm7
	addss	%xmm6, %xmm7
	movaps	%xmm10, %xmm6
	mulss	%xmm8, %xmm6
	addss	%xmm7, %xmm6
	movaps	%xmm11, %xmm7
	mulss	%xmm4, %xmm7
	subss	%xmm7, %xmm6
	mulss	%xmm0, %xmm5
	mulss	%xmm11, %xmm8
	subss	%xmm8, %xmm5
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm5
	mulss	%xmm9, %xmm3
	subss	%xmm3, %xmm5
	movaps	%xmm1, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm6, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm5, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movss	.LCPI8_9(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm4
	movaps	%xmm1, %xmm8
	mulss	%xmm4, %xmm8
	movaps	%xmm2, %xmm7
	mulss	%xmm4, %xmm7
	mulss	%xmm6, %xmm4
	movaps	%xmm5, %xmm10
	mulss	%xmm8, %xmm10
	movaps	%xmm5, %xmm11
	mulss	%xmm7, %xmm11
	mulss	%xmm4, %xmm5
	mulss	%xmm1, %xmm8
	movaps	%xmm1, %xmm9
	mulss	%xmm7, %xmm9
	mulss	%xmm4, %xmm1
	mulss	%xmm2, %xmm7
	mulss	%xmm4, %xmm2
	mulss	%xmm6, %xmm4
	movaps	%xmm7, %xmm3
	addss	%xmm4, %xmm3
	movss	.LCPI8_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm14
	subss	%xmm3, %xmm0
	movaps	%xmm9, %xmm3
	subss	%xmm5, %xmm3
	movaps	%xmm1, %xmm13
	addss	%xmm11, %xmm13
	addss	%xmm5, %xmm9
	addss	%xmm8, %xmm4
	movaps	%xmm14, %xmm6
	subss	%xmm4, %xmm6
	movaps	%xmm2, %xmm12
	subss	%xmm10, %xmm12
	subss	%xmm11, %xmm1
	addss	%xmm10, %xmm2
	addss	%xmm8, %xmm7
	movaps	%xmm14, %xmm8
	subss	%xmm7, %xmm8
	movss	8(%rbx), %xmm15         # xmm15 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movss	%xmm14, 32(%rsp)        # 4-byte Spill
	movaps	%xmm0, %xmm10
	mulss	%xmm15, %xmm10
	movss	24(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm7
	mulss	%xmm4, %xmm7
	addss	%xmm10, %xmm7
	movss	40(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm5
	mulss	%xmm10, %xmm5
	addss	%xmm7, %xmm5
	movss	%xmm5, 16(%rsp)         # 4-byte Spill
	movaps	%xmm0, %xmm11
	mulss	%xmm14, %xmm11
	movss	28(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm5
	mulss	%xmm7, %xmm5
	addss	%xmm11, %xmm5
	movss	44(%rbx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm11
	mulss	%xmm14, %xmm11
	addss	%xmm5, %xmm11
	movss	16(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	%xmm5, 8(%rsp)          # 4-byte Spill
	mulss	%xmm5, %xmm0
	movss	32(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	%xmm5, 4(%rsp)          # 4-byte Spill
	mulss	%xmm5, %xmm3
	addss	%xmm0, %xmm3
	movss	48(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm13
	addss	%xmm3, %xmm13
	movaps	%xmm9, %xmm0
	mulss	%xmm15, %xmm0
	movaps	%xmm6, %xmm3
	mulss	%xmm4, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm5
	mulss	%xmm10, %xmm5
	addss	%xmm3, %xmm5
	movaps	%xmm9, %xmm0
	mulss	32(%rsp), %xmm0         # 4-byte Folded Reload
	movaps	%xmm6, %xmm3
	mulss	%xmm7, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm0
	mulss	%xmm14, %xmm0
	addss	%xmm3, %xmm0
	mulss	8(%rsp), %xmm9          # 4-byte Folded Reload
	mulss	4(%rsp), %xmm6          # 4-byte Folded Reload
	addss	%xmm9, %xmm6
	movss	12(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm12
	addss	%xmm6, %xmm12
	mulss	%xmm1, %xmm15
	mulss	%xmm2, %xmm4
	addss	%xmm15, %xmm4
	mulss	%xmm8, %xmm10
	addss	%xmm4, %xmm10
	movss	32(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	mulss	%xmm2, %xmm7
	addss	%xmm3, %xmm7
	mulss	%xmm8, %xmm14
	addss	%xmm7, %xmm14
	mulss	8(%rsp), %xmm1          # 4-byte Folded Reload
	mulss	4(%rsp), %xmm2          # 4-byte Folded Reload
	addss	%xmm1, %xmm2
	mulss	%xmm9, %xmm8
	addss	%xmm2, %xmm8
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 288(%rsp)
	movss	%xmm11, 292(%rsp)
	movss	%xmm13, 296(%rsp)
	movl	$0, 300(%rsp)
	movss	%xmm5, 304(%rsp)
	movss	%xmm0, 308(%rsp)
	movss	%xmm12, 312(%rsp)
	movl	$0, 316(%rsp)
	movss	%xmm10, 320(%rsp)
	movss	%xmm14, 324(%rsp)
	movss	%xmm8, 328(%rsp)
	movl	$0, 332(%rsp)
	movq	152(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm0
	movups	%xmm0, 48(%rbp)
	movq	440(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm0
	movups	%xmm0, 64(%rbp)
	movq	432(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm0
	movups	%xmm0, 80(%rbp)
	movq	248(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm0
	movups	%xmm0, 96(%rbp)
	jmp	.LBB8_45
	.p2align	4, 0x90
.LBB8_44:                               #   in Loop: Header=BB8_38 Depth=1
	movq	144(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm1
	movaps	%xmm1, 288(%rsp)
	movq	456(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm1
	movups	%xmm1, (%rbp)
	movq	448(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm1
	movups	%xmm1, 16(%rbp)
	movq	240(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm1
	movups	%xmm1, 32(%rbp)
	movss	128(%rsp), %xmm6        # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm1
	mulss	%xmm0, %xmm1
	movss	96(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm2
	movss	12(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm1
	movss	32(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm2
	movss	16(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	subss	%xmm2, %xmm1
	movss	208(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm8
	movss	4(%rsp), %xmm9          # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm8
	addss	%xmm1, %xmm8
	movaps	%xmm7, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm12, %xmm2
	mulss	%xmm10, %xmm2
	subss	%xmm2, %xmm1
	movaps	%xmm6, %xmm2
	mulss	%xmm9, %xmm2
	subss	%xmm2, %xmm1
	movaps	%xmm5, %xmm4
	mulss	%xmm11, %xmm4
	addss	%xmm1, %xmm4
	movaps	%xmm5, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm12, %xmm2
	mulss	%xmm9, %xmm2
	subss	%xmm2, %xmm1
	movaps	%xmm7, %xmm2
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm1
	movaps	%xmm6, %xmm3
	mulss	%xmm10, %xmm3
	addss	%xmm1, %xmm3
	mulss	%xmm0, %xmm12
	mulss	%xmm11, %xmm6
	addss	%xmm12, %xmm6
	mulss	%xmm10, %xmm7
	addss	%xmm6, %xmm7
	mulss	%xmm9, %xmm5
	addss	%xmm7, %xmm5
	movaps	%xmm11, %xmm1
	mulss	%xmm5, %xmm1
	movaps	%xmm0, %xmm2
	mulss	%xmm8, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm9, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm2, %xmm1
	movaps	%xmm10, %xmm2
	mulss	%xmm3, %xmm2
	subss	%xmm2, %xmm1
	movaps	%xmm10, %xmm2
	mulss	%xmm5, %xmm2
	movaps	%xmm0, %xmm6
	mulss	%xmm4, %xmm6
	addss	%xmm2, %xmm6
	movaps	%xmm11, %xmm2
	mulss	%xmm3, %xmm2
	addss	%xmm6, %xmm2
	movaps	%xmm9, %xmm6
	mulss	%xmm8, %xmm6
	subss	%xmm6, %xmm2
	movaps	%xmm9, %xmm6
	mulss	%xmm5, %xmm6
	movaps	%xmm0, %xmm7
	mulss	%xmm3, %xmm7
	addss	%xmm6, %xmm7
	movaps	%xmm10, %xmm6
	mulss	%xmm8, %xmm6
	addss	%xmm7, %xmm6
	movaps	%xmm11, %xmm7
	mulss	%xmm4, %xmm7
	subss	%xmm7, %xmm6
	mulss	%xmm0, %xmm5
	mulss	%xmm11, %xmm8
	subss	%xmm8, %xmm5
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm5
	mulss	%xmm9, %xmm3
	subss	%xmm3, %xmm5
	movaps	%xmm1, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm6, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm5, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movss	.LCPI8_9(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm4
	movaps	%xmm1, %xmm8
	mulss	%xmm4, %xmm8
	movaps	%xmm2, %xmm7
	mulss	%xmm4, %xmm7
	mulss	%xmm6, %xmm4
	movaps	%xmm5, %xmm10
	mulss	%xmm8, %xmm10
	movaps	%xmm5, %xmm11
	mulss	%xmm7, %xmm11
	mulss	%xmm4, %xmm5
	mulss	%xmm1, %xmm8
	movaps	%xmm1, %xmm9
	mulss	%xmm7, %xmm9
	mulss	%xmm4, %xmm1
	mulss	%xmm2, %xmm7
	mulss	%xmm4, %xmm2
	mulss	%xmm6, %xmm4
	movaps	%xmm7, %xmm3
	addss	%xmm4, %xmm3
	movss	.LCPI8_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm14
	subss	%xmm3, %xmm0
	movaps	%xmm9, %xmm3
	subss	%xmm5, %xmm3
	movaps	%xmm1, %xmm13
	addss	%xmm11, %xmm13
	addss	%xmm5, %xmm9
	addss	%xmm8, %xmm4
	movaps	%xmm14, %xmm6
	subss	%xmm4, %xmm6
	movaps	%xmm2, %xmm12
	subss	%xmm10, %xmm12
	subss	%xmm11, %xmm1
	addss	%xmm10, %xmm2
	addss	%xmm8, %xmm7
	movaps	%xmm14, %xmm8
	subss	%xmm7, %xmm8
	movss	8(%r15), %xmm15         # xmm15 = mem[0],zero,zero,zero
	movss	12(%r15), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movss	%xmm14, 32(%rsp)        # 4-byte Spill
	movaps	%xmm0, %xmm10
	mulss	%xmm15, %xmm10
	movss	24(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm7
	mulss	%xmm4, %xmm7
	addss	%xmm10, %xmm7
	movss	40(%r15), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm5
	mulss	%xmm10, %xmm5
	addss	%xmm7, %xmm5
	movss	%xmm5, 16(%rsp)         # 4-byte Spill
	movaps	%xmm0, %xmm11
	mulss	%xmm14, %xmm11
	movss	28(%r15), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm5
	mulss	%xmm7, %xmm5
	addss	%xmm11, %xmm5
	movss	44(%r15), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm11
	mulss	%xmm14, %xmm11
	addss	%xmm5, %xmm11
	movss	16(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	%xmm5, 8(%rsp)          # 4-byte Spill
	mulss	%xmm5, %xmm0
	movss	32(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	%xmm5, 4(%rsp)          # 4-byte Spill
	mulss	%xmm5, %xmm3
	addss	%xmm0, %xmm3
	movss	48(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm13
	addss	%xmm3, %xmm13
	movaps	%xmm9, %xmm0
	mulss	%xmm15, %xmm0
	movaps	%xmm6, %xmm3
	mulss	%xmm4, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm5
	mulss	%xmm10, %xmm5
	addss	%xmm3, %xmm5
	movaps	%xmm9, %xmm0
	mulss	32(%rsp), %xmm0         # 4-byte Folded Reload
	movaps	%xmm6, %xmm3
	mulss	%xmm7, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm0
	mulss	%xmm14, %xmm0
	addss	%xmm3, %xmm0
	mulss	8(%rsp), %xmm9          # 4-byte Folded Reload
	mulss	4(%rsp), %xmm6          # 4-byte Folded Reload
	addss	%xmm9, %xmm6
	movss	12(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm12
	addss	%xmm6, %xmm12
	mulss	%xmm1, %xmm15
	mulss	%xmm2, %xmm4
	addss	%xmm15, %xmm4
	mulss	%xmm8, %xmm10
	addss	%xmm4, %xmm10
	movss	32(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	mulss	%xmm2, %xmm7
	addss	%xmm3, %xmm7
	mulss	%xmm8, %xmm14
	addss	%xmm7, %xmm14
	mulss	8(%rsp), %xmm1          # 4-byte Folded Reload
	mulss	4(%rsp), %xmm2          # 4-byte Folded Reload
	addss	%xmm1, %xmm2
	mulss	%xmm9, %xmm8
	addss	%xmm2, %xmm8
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 352(%rsp)
	movss	%xmm11, 356(%rsp)
	movss	%xmm13, 360(%rsp)
	movl	$0, 364(%rsp)
	movss	%xmm5, 368(%rsp)
	movss	%xmm0, 372(%rsp)
	movss	%xmm12, 376(%rsp)
	movl	$0, 380(%rsp)
	movss	%xmm10, 384(%rsp)
	movss	%xmm14, 388(%rsp)
	movss	%xmm8, 392(%rsp)
	movl	$0, 396(%rsp)
.LBB8_45:                               #   in Loop: Header=BB8_38 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rcx
	movq	$_ZTV24btPerturbedContactResult+16, 672(%rsp)
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%rax, 848(%rsp)
	movaps	288(%rsp), %xmm0
	movups	%xmm0, (%r14)
	movups	(%rbp), %xmm0
	movups	%xmm0, 16(%r14)
	movups	16(%rbp), %xmm0
	movups	%xmm0, 32(%r14)
	movups	32(%rbp), %xmm0
	movups	%xmm0, 48(%r14)
	movups	48(%rbp), %xmm0
	movups	%xmm0, 64(%r14)
	movups	64(%rbp), %xmm0
	movups	%xmm0, 80(%r14)
	movups	80(%rbp), %xmm0
	movups	%xmm0, 96(%r14)
	movups	96(%rbp), %xmm0
	movups	%xmm0, 112(%r14)
	movaps	464(%rsp), %xmm0
	movups	%xmm0, 128(%r14)
	leaq	480(%rsp), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, 144(%r14)
	movups	16(%rax), %xmm0
	movups	%xmm0, 160(%r14)
	movups	32(%rax), %xmm0
	movups	%xmm0, 176(%r14)
	movzbl	27(%rsp), %eax          # 1-byte Folded Reload
	movb	%al, 1048(%rsp)
	movq	%rcx, 1056(%rsp)
.Ltmp38:
	xorl	%r8d, %r8d
	leaq	576(%rsp), %rdi
	leaq	288(%rsp), %rsi
	leaq	672(%rsp), %rdx
	callq	_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
.Ltmp39:
# BB#46:                                #   in Loop: Header=BB8_38 Depth=1
	incl	%r13d
	cmpl	148(%r12), %r13d
	jl	.LBB8_38
.LBB8_47:                               # %._crit_edge
	movq	48(%rsp), %rbp          # 8-byte Reload
	movss	20(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
.LBB8_48:
	ucomiss	.LCPI8_1(%rip), %xmm2
	jbe	.LBB8_54
# BB#49:
	movq	64(%rsp), %rax          # 8-byte Reload
	movb	40(%rax), %al
	testb	%al, %al
	je	.LBB8_54
# BB#50:
	movss	%xmm2, 104(%r12)
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm2
	jbe	.LBB8_54
# BB#51:
	movups	584(%rsp), %xmm0
	movups	%xmm0, 80(%r12)
.Ltmp41:
	leaq	672(%rsp), %rsi
	movq	144(%rsp), %rdi         # 8-byte Reload
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
.Ltmp42:
# BB#52:                                # %.noexc
	movaps	672(%rsp), %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
.Ltmp43:
	leaq	464(%rsp), %rsi
	movq	152(%rsp), %rdi         # 8-byte Reload
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
.Ltmp44:
# BB#53:                                # %.noexc135
	movaps	464(%rsp), %xmm0
	movq	240(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm1
	movups	%xmm1, 48(%r12)
	movq	248(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm1
	movups	%xmm1, 64(%r12)
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	movups	%xmm1, 16(%r12)
	movups	%xmm0, 32(%r12)
.LBB8_54:
	cmpb	$0, 128(%r12)
	je	.LBB8_91
# BB#55:
	movq	8(%rbp), %rdi
	cmpl	$0, 728(%rdi)
	je	.LBB8_91
# BB#56:
	movq	712(%rdi), %rax
	cmpq	144(%rbp), %rax
	je	.LBB8_61
# BB#57:
	leaq	80(%rbp), %rsi
	addq	$16, %rbp
	jmp	.LBB8_62
.LBB8_58:
	ucomiss	%xmm0, %xmm7
	movq	48(%rsp), %rbx          # 8-byte Reload
	jbe	.LBB8_65
# BB#59:
	mulss	%xmm0, %xmm4
	movaps	%xmm4, %xmm3
	addss	%xmm5, %xmm3
	movaps	.LCPI8_5(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm8, %xmm1
	movaps	%xmm0, %xmm7
	ucomiss	%xmm3, %xmm1
	jbe	.LBB8_63
# BB#60:
	movaps	%xmm1, %xmm3
	jmp	.LBB8_65
.LBB8_61:
	leaq	16(%rbp), %rsi
	addq	$80, %rbp
.LBB8_62:                               # %_ZN16btManifoldResult20refreshContactPointsEv.exit166
	movq	%rbp, %rdx
	jmp	.LBB8_90
.LBB8_63:
	ucomiss	%xmm8, %xmm3
	jbe	.LBB8_65
# BB#64:                                # %_ZL21segmentsClosestPointsR9btVector3S0_S0_RfS1_RKS_S3_fS3_f.exit.i
	movaps	%xmm8, %xmm3
.LBB8_65:                               # %_ZL21segmentsClosestPointsR9btVector3S0_S0_RfS1_RKS_S3_fS3_f.exit.i
	movaps	224(%rsp), %xmm4        # 16-byte Reload
	movaps	%xmm4, %xmm1
	unpcklps	%xmm12, %xmm1   # xmm1 = xmm1[0],xmm12[0],xmm1[1],xmm12[1]
	movaps	%xmm14, %xmm2
	mulss	%xmm3, %xmm2
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	unpcklps	%xmm9, %xmm15   # xmm15 = xmm15[0],xmm9[0],xmm15[1],xmm9[1]
	mulss	%xmm7, %xmm11
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm15, %xmm7
	subps	%xmm3, %xmm10
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm0
	movaps	%xmm0, %xmm2
	addps	%xmm7, %xmm10
	movaps	%xmm10, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	addss	%xmm11, %xmm2
	movaps	%xmm10, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm3, %xmm3
	sqrtss	%xmm1, %xmm3
	ucomiss	%xmm3, %xmm3
	jnp	.LBB8_67
# BB#66:                                # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm11, 16(%rsp)        # 4-byte Spill
	movaps	%xmm12, 128(%rsp)       # 16-byte Spill
	movss	%xmm13, 12(%rsp)        # 4-byte Spill
	movaps	%xmm14, 64(%rsp)        # 16-byte Spill
	movaps	%xmm10, 208(%rsp)       # 16-byte Spill
	movaps	%xmm7, 96(%rsp)         # 16-byte Spill
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	movss	%xmm1, 48(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	48(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI8_2(%rip), %xmm6   # xmm6 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	224(%rsp), %xmm4        # 16-byte Reload
	movaps	96(%rsp), %xmm7         # 16-byte Reload
	movaps	208(%rsp), %xmm10       # 16-byte Reload
	movaps	64(%rsp), %xmm14        # 16-byte Reload
	movss	12(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movaps	128(%rsp), %xmm12       # 16-byte Reload
	movss	16(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
.LBB8_67:                               # %_ZL21segmentsClosestPointsR9btVector3S0_S0_RfS1_RKS_S3_fS3_f.exit.i.split
	subss	4(%rsp), %xmm3          # 4-byte Folded Reload
	subss	%xmm13, %xmm3
	ucomiss	32(%rsp), %xmm3         # 4-byte Folded Reload
	ja	.LBB8_83
# BB#68:
	movss	.LCPI8_10(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jae	.LBB8_75
# BB#69:
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB8_71
# BB#70:                                # %call.sqrt461
	movaps	%xmm1, %xmm0
	movss	%xmm11, 16(%rsp)        # 4-byte Spill
	movss	%xmm13, 12(%rsp)        # 4-byte Spill
	movaps	%xmm10, 208(%rsp)       # 16-byte Spill
	movaps	%xmm7, 96(%rsp)         # 16-byte Spill
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	movss	%xmm3, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	96(%rsp), %xmm7         # 16-byte Reload
	movaps	208(%rsp), %xmm10       # 16-byte Reload
	movss	12(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
.LBB8_71:                               # %.split460
	movss	.LCPI8_11(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm1, %xmm10
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm10, 112(%rsp)
	movaps	%xmm10, %xmm14
	shufps	$229, %xmm14, %xmm14    # xmm14 = xmm14[1,1,2,3]
	movlps	%xmm0, 120(%rsp)
	movaps	%xmm2, %xmm12
	jmp	.LBB8_82
.LBB8_72:
	movaps	%xmm1, %xmm5
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB8_74
# BB#73:                                # %call.sqrt467
	movss	%xmm2, 20(%rsp)         # 4-byte Spill
	movaps	%xmm3, 192(%rsp)        # 16-byte Spill
	movaps	%xmm5, 176(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	176(%rsp), %xmm5        # 16-byte Reload
	movss	.LCPI8_2(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movaps	192(%rsp), %xmm3        # 16-byte Reload
	movss	20(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB8_74:                               # %.split466
	divss	%xmm1, %xmm4
	mulss	%xmm4, %xmm5
	xorps	.LCPI8_5(%rip), %xmm5
	mulss	%xmm3, %xmm4
	movss	%xmm5, 28(%rsp)         # 4-byte Spill
	movaps	%xmm4, %xmm1
	xorps	%xmm0, %xmm0
	jmp	.LBB8_30
.LBB8_75:
	movaps	.LCPI8_3(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	andps	%xmm14, %xmm0
	ucomiss	.LCPI8_4(%rip), %xmm0
	jbe	.LBB8_79
# BB#76:
	movaps	%xmm12, %xmm1
	mulss	%xmm1, %xmm1
	movaps	%xmm14, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB8_78
# BB#77:                                # %call.sqrt457
	movss	%xmm11, 16(%rsp)        # 4-byte Spill
	movaps	%xmm12, 128(%rsp)       # 16-byte Spill
	movss	%xmm13, 12(%rsp)        # 4-byte Spill
	movaps	%xmm14, 64(%rsp)        # 16-byte Spill
	movaps	%xmm7, 96(%rsp)         # 16-byte Spill
	movss	%xmm3, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI8_2(%rip), %xmm6   # xmm6 = mem[0],zero,zero,zero
	movaps	96(%rsp), %xmm7         # 16-byte Reload
	movaps	64(%rsp), %xmm14        # 16-byte Reload
	movss	12(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movaps	128(%rsp), %xmm12       # 16-byte Reload
	movss	16(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB8_78:                               # %.split
	divss	%xmm1, %xmm6
	mulss	%xmm6, %xmm14
	xorps	.LCPI8_5(%rip), %xmm14
	mulss	%xmm6, %xmm12
	movl	$0, 112(%rsp)
	movss	%xmm14, 116(%rsp)
	movss	%xmm12, 120(%rsp)
	movl	$0, 124(%rsp)
	xorps	%xmm10, %xmm10
	jmp	.LBB8_82
.LBB8_79:
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	movaps	%xmm12, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB8_81
# BB#80:                                # %call.sqrt459
	movss	%xmm11, 16(%rsp)        # 4-byte Spill
	movaps	%xmm12, 128(%rsp)       # 16-byte Spill
	movss	%xmm13, 12(%rsp)        # 4-byte Spill
	movaps	%xmm7, 96(%rsp)         # 16-byte Spill
	movss	%xmm3, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI8_2(%rip), %xmm6   # xmm6 = mem[0],zero,zero,zero
	movaps	224(%rsp), %xmm4        # 16-byte Reload
	movaps	96(%rsp), %xmm7         # 16-byte Reload
	movss	12(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movaps	128(%rsp), %xmm12       # 16-byte Reload
	movss	16(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB8_81:                               # %.split458
	divss	%xmm1, %xmm6
	mulss	%xmm6, %xmm12
	xorps	.LCPI8_5(%rip), %xmm12
	movaps	%xmm12, %xmm10
	mulss	%xmm6, %xmm4
	movss	%xmm12, 112(%rsp)
	movss	%xmm4, 116(%rsp)
	movq	$0, 120(%rsp)
	xorps	%xmm12, %xmm12
	movaps	%xmm4, %xmm14
.LBB8_82:                               # %_Z13btPlaneSpace1RK9btVector3RS_S2_.exit.i
	movss	56(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm0
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	addss	60(%r15), %xmm7
	addss	64(%r15), %xmm11
	mulss	%xmm13, %xmm10
	mulss	%xmm13, %xmm14
	mulss	%xmm13, %xmm12
	addss	%xmm0, %xmm10
	addss	%xmm7, %xmm14
	addss	%xmm11, %xmm12
	unpcklps	%xmm14, %xmm10  # xmm10 = xmm10[0],xmm14[0],xmm10[1],xmm14[1]
	xorps	%xmm0, %xmm0
	movss	%xmm12, %xmm0           # xmm0 = xmm12[0],xmm0[1,2,3]
	movlps	%xmm10, 528(%rsp)
	movlps	%xmm0, 536(%rsp)
.LBB8_83:                               # %_ZL22capsuleCapsuleDistanceR9btVector3S0_ffffiiRK11btTransformS3_f.exit
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm0
	jbe	.LBB8_85
# BB#84:
	movq	(%rbx), %rax
	leaq	112(%rsp), %rsi
	leaq	528(%rsp), %rdx
	movq	%rbx, %rdi
	movaps	%xmm3, %xmm0
	callq	*32(%rax)
.LBB8_85:
	movq	8(%rbx), %rdi
	cmpl	$0, 728(%rdi)
	je	.LBB8_91
# BB#86:
	movq	712(%rdi), %rax
	cmpq	144(%rbx), %rax
	je	.LBB8_88
# BB#87:
	leaq	80(%rbx), %rsi
	addq	$16, %rbx
	jmp	.LBB8_89
.LBB8_88:
	leaq	16(%rbx), %rsi
	addq	$80, %rbx
.LBB8_89:                               # %_ZN16btManifoldResult20refreshContactPointsEv.exit166
	movq	%rbx, %rdx
.LBB8_90:                               # %_ZN16btManifoldResult20refreshContactPointsEv.exit166
	callq	_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_
.LBB8_91:                               # %_ZN16btManifoldResult20refreshContactPointsEv.exit166
	addq	$1064, %rsp             # imm = 0x428
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_92:
.Ltmp45:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB8_93:
.Ltmp37:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB8_94:
.Ltmp34:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB8_95:
.Ltmp31:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB8_96:
.Ltmp40:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN23btConvexConvexAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end8-_ZN23btConvexConvexAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp23-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp30-.Ltmp23         #   Call between .Ltmp23 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin3   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin3   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin3   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin3   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp44-.Ltmp41         #   Call between .Ltmp41 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin3   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Lfunc_end8-.Ltmp44     #   Call between .Ltmp44 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN30btConvexSeparatingDistanceUtil24updateSeparatingDistanceERK11btTransformS2_,"axG",@progbits,_ZN30btConvexSeparatingDistanceUtil24updateSeparatingDistanceERK11btTransformS2_,comdat
	.weak	_ZN30btConvexSeparatingDistanceUtil24updateSeparatingDistanceERK11btTransformS2_
	.p2align	4, 0x90
	.type	_ZN30btConvexSeparatingDistanceUtil24updateSeparatingDistanceERK11btTransformS2_,@function
_ZN30btConvexSeparatingDistanceUtil24updateSeparatingDistanceERK11btTransformS2_: # @_ZN30btConvexSeparatingDistanceUtil24updateSeparatingDistanceERK11btTransformS2_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 192
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r12
	leaq	48(%rbx), %r13
	leaq	48(%r15), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movq	16(%rsp), %r14
	movq	24(%rsp), %rbp
	movq	%r14, 120(%rsp)
	movq	%rbp, 128(%rsp)
	leaq	16(%rsp), %rsi
	movq	%r15, %rdi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, 64(%rsp)
	movss	88(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm1
	jbe	.LBB9_1
# BB#2:
	movq	%rbp, %rax
	shrq	$32, %rax
	movd	%eax, %xmm1
	movss	48(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	52(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	32(%r12), %xmm3
	movss	%xmm3, 56(%rsp)         # 4-byte Spill
	subss	36(%r12), %xmm2
	movss	%xmm2, 60(%rsp)         # 4-byte Spill
	movss	56(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	40(%r12), %xmm2
	movss	%xmm2, 52(%rsp)         # 4-byte Spill
	movss	12(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jne	.LBB9_7
	jp	.LBB9_7
# BB#3:
	movd	%ebp, %xmm1
	movss	8(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jne	.LBB9_7
	jp	.LBB9_7
# BB#4:
	movq	%r14, %rax
	shrq	$32, %rax
	movd	%eax, %xmm1
	movss	4(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jne	.LBB9_7
	jp	.LBB9_7
# BB#5:                                 # %_ZNK10btQuadWordneERKS_.exit.i49
	movd	%r14d, %xmm1
	movss	(%r12), %xmm2           # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jne	.LBB9_7
	jnp	.LBB9_6
.LBB9_7:                                # %_ZNK10btQuadWordneERKS_.exit.thread.i57
	leaq	120(%rsp), %rsi
	leaq	16(%rsp), %rdx
	leaq	12(%rsp), %rcx
	movq	%r12, %rdi
	callq	_ZN15btTransformUtil32calculateDiffAxisAngleQuaternionERK12btQuaternionS2_R9btVector3Rf
	movss	12(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movsd	16(%rsp), %xmm1         # xmm1 = mem[0],zero
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	mulss	24(%rsp), %xmm2
	pxor	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movd	76(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
.LBB9_8:                                # %_ZN15btTransformUtil27calculateVelocityQuaternionERK9btVector3S2_RK12btQuaternionS5_fRS0_S6_.exit58
	leaq	16(%r12), %r14
	movss	48(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	52(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	48(%r12), %xmm2
	movss	%xmm2, 40(%rsp)         # 4-byte Spill
	subss	52(%r12), %xmm1
	movss	%xmm1, 48(%rsp)         # 4-byte Spill
	movss	56(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	56(%r12), %xmm1
	movss	%xmm1, 44(%rsp)         # 4-byte Spill
	movss	28(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm1
	jne	.LBB9_13
	jp	.LBB9_13
# BB#9:
	movss	24(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	72(%rsp), %xmm1
	jne	.LBB9_13
	jp	.LBB9_13
# BB#10:
	movss	20(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	68(%rsp), %xmm1
	jne	.LBB9_13
	jp	.LBB9_13
# BB#11:                                # %_ZNK10btQuadWordneERKS_.exit.i
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	64(%rsp), %xmm1
	jne	.LBB9_13
	jnp	.LBB9_12
.LBB9_13:                               # %_ZNK10btQuadWordneERKS_.exit.thread.i
	leaq	64(%rsp), %rsi
	leaq	16(%rsp), %rdx
	leaq	12(%rsp), %rcx
	movq	%r14, %rdi
	movaps	%xmm3, 80(%rsp)         # 16-byte Spill
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	callq	_ZN15btTransformUtil32calculateDiffAxisAngleQuaternionERK12btQuaternionS2_R9btVector3Rf
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	movaps	80(%rsp), %xmm3         # 16-byte Reload
	movss	12(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movsd	16(%rsp), %xmm1         # xmm1 = mem[0],zero
	movaps	%xmm2, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm1, %xmm5
	mulss	24(%rsp), %xmm2
	pxor	%xmm4, %xmm4
	movss	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1,2,3]
.LBB9_14:                               # %_ZN15btTransformUtil27calculateVelocityQuaternionERK9btVector3S2_RK12btQuaternionS5_fRS0_S6_.exit
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	mulss	%xmm3, %xmm3
	addss	%xmm1, %xmm3
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	xorps	%xmm3, %xmm3
	sqrtss	%xmm0, %xmm3
	ucomiss	%xmm3, %xmm3
	jnp	.LBB9_16
# BB#15:                                # %call.sqrt
	movaps	%xmm4, 80(%rsp)         # 16-byte Spill
	movaps	%xmm5, 96(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	96(%rsp), %xmm5         # 16-byte Reload
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	movaps	%xmm0, %xmm3
.LBB9_16:                               # %_ZN15btTransformUtil27calculateVelocityQuaternionERK9btVector3S2_RK12btQuaternionS5_fRS0_S6_.exit.split
	mulss	80(%r12), %xmm3
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	mulss	%xmm5, %xmm5
	addss	%xmm0, %xmm5
	mulss	%xmm4, %xmm4
	addss	%xmm5, %xmm4
	xorps	%xmm0, %xmm0
	sqrtss	%xmm4, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB9_18
# BB#17:                                # %call.sqrt94
	movaps	%xmm4, %xmm0
	movss	%xmm3, 80(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	80(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB9_18:                               # %_ZN15btTransformUtil27calculateVelocityQuaternionERK9btVector3S2_RK12btQuaternionS5_fRS0_S6_.exit.split.split
	mulss	84(%r12), %xmm0
	addss	%xmm0, %xmm3
	movss	40(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	56(%rsp), %xmm2         # 4-byte Folded Reload
	movss	48(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	60(%rsp), %xmm0         # 4-byte Folded Reload
	movss	44(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	52(%rsp), %xmm1         # 4-byte Folded Reload
	mulss	64(%r12), %xmm2
	mulss	68(%r12), %xmm0
	addss	%xmm2, %xmm0
	mulss	72(%r12), %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	maxss	%xmm1, %xmm0
	addss	%xmm3, %xmm0
	movss	88(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 88(%r12)
	jmp	.LBB9_19
.LBB9_1:                                # %._crit_edge
	leaq	16(%r12), %r14
.LBB9_19:
	movups	(%r13), %xmm0
	movups	%xmm0, 32(%r12)
	movq	112(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm0
	movups	%xmm0, 48(%r12)
	movups	120(%rsp), %xmm0
	movups	%xmm0, (%r12)
	movaps	64(%rsp), %xmm0
	movups	%xmm0, (%r14)
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_6:
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %rax
	shrq	$32, %rax
	movd	%eax, %xmm4
	xorps	%xmm3, %xmm3
	pxor	%xmm0, %xmm0
	jmp	.LBB9_8
.LBB9_12:
	pxor	%xmm4, %xmm4
	xorps	%xmm5, %xmm5
	jmp	.LBB9_14
.Lfunc_end9:
	.size	_ZN30btConvexSeparatingDistanceUtil24updateSeparatingDistanceERK11btTransformS2_, .Lfunc_end9-_ZN30btConvexSeparatingDistanceUtil24updateSeparatingDistanceERK11btTransformS2_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN23btConvexConvexAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN23btConvexConvexAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN23btConvexConvexAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN23btConvexConvexAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
	subq	$1336, %rsp             # imm = 0x538
.Lcfi53:
	.cfi_def_cfa_offset 1392
.Lcfi54:
	.cfi_offset %rbx, -56
.Lcfi55:
	.cfi_offset %r12, -48
.Lcfi56:
	.cfi_offset %r13, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movss	120(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	124(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	56(%rbx), %xmm0
	subss	60(%rbx), %xmm1
	movss	128(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	64(%rbx), %xmm2
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	268(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	ucomiss	%xmm2, %xmm0
	jbe	.LBB10_7
# BB#1:
	movss	120(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	124(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	56(%r14), %xmm0
	subss	60(%r14), %xmm1
	movss	128(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	64(%r14), %xmm2
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	268(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	movss	.LCPI10_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	ja	.LBB10_27
# BB#2:
	movb	disableCcd(%rip), %al
	testb	%al, %al
	je	.LBB10_3
	jmp	.LBB10_27
.LBB10_7:
	cmpb	$0, disableCcd(%rip)
	je	.LBB10_3
# BB#8:
	movss	.LCPI10_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB10_27
.LBB10_3:
	movq	200(%rbx), %r12
	movl	264(%r14), %ebp
	leaq	168(%rsp), %r15
	movq	%r15, %rdi
	callq	_ZN21btConvexInternalShapeC2Ev
	movq	$_ZTV13btSphereShape+16, 168(%rsp)
	movl	$8, 176(%rsp)
	movl	%ebp, 208(%rsp)
	movl	%ebp, 224(%rsp)
	movq	$_ZTVN12btConvexCast10CastResultE+16, 424(%rsp)
	movl	$1566444395, 592(%rsp)  # imm = 0x5D5E0B6B
	movq	$0, 600(%rsp)
	movl	$0, 608(%rsp)
	andb	$-16, 1304(%rsp)
.Ltmp46:
	leaq	72(%rsp), %rdi
	leaq	976(%rsp), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	_ZN15btGjkConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
.Ltmp47:
# BB#4:
	leaq	72(%rbx), %r15
	leaq	8(%rbx), %r12
	leaq	72(%r14), %r13
	leaq	8(%r14), %rbp
.Ltmp49:
	leaq	72(%rsp), %rdi
	leaq	424(%rsp), %r9
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	movq	%r13, %r8
	callq	_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
.Ltmp50:
# BB#5:
	testb	%al, %al
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	je	.LBB10_6
# BB#9:
	movss	260(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	592(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB10_11
# BB#10:
	movss	%xmm0, 260(%rbx)
.LBB10_11:
	movss	260(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB10_13
# BB#12:
	movss	%xmm0, 260(%r14)
.LBB10_13:
	movss	.LCPI10_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB10_15
# BB#14:
	movaps	%xmm0, %xmm1
	jmp	.LBB10_15
.LBB10_6:
	movss	.LCPI10_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
.LBB10_15:
.Ltmp54:
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movss	%xmm1, 4(%rsp)          # 4-byte Spill
	leaq	72(%rsp), %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp55:
# BB#16:
	leaq	168(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
	movq	200(%r14), %r15
	movl	264(%rbx), %ebp
	leaq	104(%rsp), %r12
	movq	%r12, %rdi
	callq	_ZN21btConvexInternalShapeC2Ev
	movq	$_ZTV13btSphereShape+16, 104(%rsp)
	movl	$8, 112(%rsp)
	movl	%ebp, 144(%rsp)
	movl	%ebp, 160(%rsp)
	movq	$_ZTVN12btConvexCast10CastResultE+16, 232(%rsp)
	movl	$1566444395, 400(%rsp)  # imm = 0x5D5E0B6B
	movq	$0, 408(%rsp)
	movl	$0, 416(%rsp)
	movb	$0, 944(%rsp)
.Ltmp59:
	leaq	40(%rsp), %rdi
	leaq	616(%rsp), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	_ZN15btGjkConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
.Ltmp60:
# BB#17:
.Ltmp62:
	leaq	40(%rsp), %rdi
	leaq	232(%rsp), %r9
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	callq	_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
.Ltmp63:
# BB#18:
	testb	%al, %al
	je	.LBB10_25
# BB#19:
	movss	260(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	400(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB10_21
# BB#20:
	movss	%xmm0, 260(%rbx)
.LBB10_21:
	movss	260(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB10_23
# BB#22:
	movss	%xmm0, 260(%r14)
.LBB10_23:
	movss	4(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB10_25
# BB#24:
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
.LBB10_25:
.Ltmp67:
	leaq	40(%rsp), %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp68:
# BB#26:
	leaq	104(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
.LBB10_27:
	addq	$1336, %rsp             # imm = 0x538
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_35:
.Ltmp69:
	jmp	.LBB10_36
.LBB10_34:
.Ltmp64:
	movq	%rax, %rbx
.Ltmp65:
	leaq	40(%rsp), %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp66:
	jmp	.LBB10_37
.LBB10_33:
.Ltmp61:
.LBB10_36:
	movq	%rax, %rbx
.LBB10_37:
.Ltmp70:
	leaq	104(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp71:
	jmp	.LBB10_38
.LBB10_30:
.Ltmp56:
	jmp	.LBB10_31
.LBB10_29:
.Ltmp51:
	movq	%rax, %rbx
.Ltmp52:
	leaq	72(%rsp), %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp53:
	jmp	.LBB10_32
.LBB10_28:
.Ltmp48:
.LBB10_31:
	movq	%rax, %rbx
.LBB10_32:
.Ltmp57:
	leaq	168(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp58:
.LBB10_38:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB10_39:
.Ltmp72:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end10:
	.size	_ZN23btConvexConvexAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end10-_ZN23btConvexConvexAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp46-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin4   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin4   #     jumps to .Ltmp51
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin4   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Ltmp59-.Ltmp55         #   Call between .Ltmp55 and .Ltmp59
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin4   # >> Call Site 6 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin4   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin4   # >> Call Site 7 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin4   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin4   # >> Call Site 8 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin4   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin4   # >> Call Site 9 <<
	.long	.Ltmp65-.Ltmp68         #   Call between .Ltmp68 and .Ltmp65
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin4   # >> Call Site 10 <<
	.long	.Ltmp58-.Ltmp65         #   Call between .Ltmp65 and .Ltmp58
	.long	.Ltmp72-.Lfunc_begin4   #     jumps to .Ltmp72
	.byte	1                       #   On action: 1
	.long	.Ltmp58-.Lfunc_begin4   # >> Call Site 11 <<
	.long	.Lfunc_end10-.Ltmp58    #   Call between .Ltmp58 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN12btConvexCast10CastResultD2Ev,"axG",@progbits,_ZN12btConvexCast10CastResultD2Ev,comdat
	.weak	_ZN12btConvexCast10CastResultD2Ev
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResultD2Ev,@function
_ZN12btConvexCast10CastResultD2Ev:      # @_ZN12btConvexCast10CastResultD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	_ZN12btConvexCast10CastResultD2Ev, .Lfunc_end11-_ZN12btConvexCast10CastResultD2Ev
	.cfi_endproc

	.section	.text._ZN23btConvexConvexAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN23btConvexConvexAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN23btConvexConvexAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN23btConvexConvexAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN23btConvexConvexAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN23btConvexConvexAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi64:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi66:
	.cfi_def_cfa_offset 80
.Lcfi67:
	.cfi_offset %rbx, -56
.Lcfi68:
	.cfi_offset %r12, -48
.Lcfi69:
	.cfi_offset %r13, -40
.Lcfi70:
	.cfi_offset %r14, -32
.Lcfi71:
	.cfi_offset %r15, -24
.Lcfi72:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$160, %esi
	callq	*96(%rax)
	movq	%rax, %rbx
	movq	8(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	16(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	24(%r13), %rbp
	movl	32(%r13), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	36(%r13), %r13d
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	movq	$_ZTV23btConvexConvexAlgorithm+16, (%rbx)
	movq	200(%r15), %rdi
	movq	(%rdi), %rax
.Ltmp73:
	callq	*32(%rax)
	movss	%xmm0, (%rsp)           # 4-byte Spill
.Ltmp74:
# BB#1:
	movq	200(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp75:
	callq	*32(%rax)
.Ltmp76:
# BB#2:                                 # %_ZN23btConvexConvexAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii.exit
	movss	(%rsp), %xmm1           # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 96(%rbx)
	movss	%xmm0, 100(%rbx)
	movl	$0, 104(%rbx)
	movq	%rbp, 112(%rbx)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 120(%rbx)
	movb	$0, 128(%rbx)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 136(%rbx)
	movb	$0, 144(%rbx)
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 148(%rbx)
	movl	%r13d, 152(%rbx)
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_3:
.Ltmp77:
	movq	%rax, %r14
.Ltmp78:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp79:
# BB#4:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_5:
.Ltmp80:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN23btConvexConvexAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end12-_ZN23btConvexConvexAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp73-.Lfunc_begin5   #   Call between .Lfunc_begin5 and .Ltmp73
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp73-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp76-.Ltmp73         #   Call between .Ltmp73 and .Ltmp76
	.long	.Ltmp77-.Lfunc_begin5   #     jumps to .Ltmp77
	.byte	0                       #   On action: cleanup
	.long	.Ltmp78-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin5   #     jumps to .Ltmp80
	.byte	1                       #   On action: 1
	.long	.Ltmp79-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Lfunc_end12-.Ltmp79    #   Call between .Ltmp79 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN23btConvexConvexAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,"axG",@progbits,_ZN23btConvexConvexAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,comdat
	.weak	_ZN23btConvexConvexAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.p2align	4, 0x90
	.type	_ZN23btConvexConvexAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,@function
_ZN23btConvexConvexAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE: # @_ZN23btConvexConvexAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi77:
	.cfi_def_cfa_offset 48
.Lcfi78:
	.cfi_offset %rbx, -40
.Lcfi79:
	.cfi_offset %r14, -32
.Lcfi80:
	.cfi_offset %r15, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	136(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB13_18
# BB#1:
	cmpb	$0, 128(%r14)
	je	.LBB13_18
# BB#2:
	movl	4(%rbx), %eax
	cmpl	8(%rbx), %eax
	jne	.LBB13_17
# BB#3:
	leal	(%rax,%rax), %edx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%edx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB13_17
# BB#4:
	testl	%ebp, %ebp
	je	.LBB13_5
# BB#6:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB13_8
	jmp	.LBB13_12
.LBB13_5:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB13_12
.LBB13_8:                               # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB13_10
	.p2align	4, 0x90
.LBB13_9:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB13_9
.LBB13_10:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB13_12
	.p2align	4, 0x90
.LBB13_11:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r15,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB13_11
.LBB13_12:                              # %_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_.exit.i.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_16
# BB#13:
	cmpb	$0, 24(%rbx)
	je	.LBB13_15
# BB#14:
	callq	_Z21btAlignedFreeInternalPv
	movl	4(%rbx), %eax
.LBB13_15:
	movq	$0, 16(%rbx)
.LBB13_16:                              # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv.exit.i.i
	movb	$1, 24(%rbx)
	movq	%r15, 16(%rbx)
	movl	%ebp, 8(%rbx)
	movq	136(%r14), %rcx
.LBB13_17:                              # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_.exit
	movq	16(%rbx), %rdx
	cltq
	movq	%rcx, (%rdx,%rax,8)
	incl	%eax
	movl	%eax, 4(%rbx)
.LBB13_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZN23btConvexConvexAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE, .Lfunc_end13-_ZN23btConvexConvexAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_0:
	.long	1065353216              # float 1
.LCPI14_1:
	.long	1056964608              # float 0.5
	.section	.text._ZNK11btMatrix3x311getRotationER12btQuaternion,"axG",@progbits,_ZNK11btMatrix3x311getRotationER12btQuaternion,comdat
	.weak	_ZNK11btMatrix3x311getRotationER12btQuaternion
	.p2align	4, 0x90
	.type	_ZNK11btMatrix3x311getRotationER12btQuaternion,@function
_ZNK11btMatrix3x311getRotationER12btQuaternion: # @_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi88:
	.cfi_def_cfa_offset 80
.Lcfi89:
	.cfi_offset %rbx, -56
.Lcfi90:
	.cfi_offset %r12, -48
.Lcfi91:
	.cfi_offset %r13, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm1
	jbe	.LBB14_4
# BB#1:
	addss	.LCPI14_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB14_3
# BB#2:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movq	%rsi, %rbp
	callq	sqrtf
	movq	%rbp, %rsi
.LBB14_3:                               # %.split
	movss	.LCPI14_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	32(%rbx), %xmm2
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movaps	%xmm1, %xmm3
	divss	%xmm0, %xmm3
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	24(%rbx), %xmm1
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	4(%rbx), %xmm4
	movaps	%xmm3, %xmm5
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	unpcklps	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1]
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	mulps	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)
	jmp	.LBB14_7
.LBB14_4:
	xorl	%eax, %eax
	ucomiss	%xmm0, %xmm2
	seta	%al
	maxss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm3
	movl	$2, %r15d
	cmovbel	%eax, %r15d
	leal	1(%r15), %eax
	movl	$2863311531, %ecx       # imm = 0xAAAAAAAB
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	1(%r15,%rax), %edx
	movl	%r15d, %eax
	addl	$2, %eax
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	2(%r15,%rax), %r14d
	movq	%r15, %rbp
	shlq	$4, %rbp
	addq	%rbx, %rbp
	movss	(%rbp,%r15,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	%rdx, %r12
	shlq	$4, %r12
	addq	%rbx, %r12
	subss	(%r12,%rdx,4), %xmm0
	movq	%r14, %r13
	shlq	$4, %r13
	addq	%rbx, %r13
	subss	(%r13,%r14,4), %xmm0
	addss	.LCPI14_0(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB14_6
# BB#5:                                 # %call.sqrt72
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %rbx
	callq	sqrtf
	movq	%rbx, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movaps	%xmm0, %xmm1
.LBB14_6:                               # %.split71
	movss	.LCPI14_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, (%rsp,%r15,4)
	divss	%xmm1, %xmm0
	movss	(%r13,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	(%r12,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)
	movss	(%r12,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%rdx,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%rdx,4)
	movss	(%r13,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%r14,4)
	movaps	(%rsp), %xmm1
.LBB14_7:
	movups	%xmm1, (%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_ZNK11btMatrix3x311getRotationER12btQuaternion, .Lfunc_end14-_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI15_1:
	.long	1065353216              # float 1
.LCPI15_2:
	.long	679477248               # float 1.42108547E-14
	.section	.text._ZN15btTransformUtil32calculateDiffAxisAngleQuaternionERK12btQuaternionS2_R9btVector3Rf,"axG",@progbits,_ZN15btTransformUtil32calculateDiffAxisAngleQuaternionERK12btQuaternionS2_R9btVector3Rf,comdat
	.weak	_ZN15btTransformUtil32calculateDiffAxisAngleQuaternionERK12btQuaternionS2_R9btVector3Rf
	.p2align	4, 0x90
	.type	_ZN15btTransformUtil32calculateDiffAxisAngleQuaternionERK12btQuaternionS2_R9btVector3Rf,@function
_ZN15btTransformUtil32calculateDiffAxisAngleQuaternionERK12btQuaternionS2_R9btVector3Rf: # @_ZN15btTransformUtil32calculateDiffAxisAngleQuaternionERK12btQuaternionS2_R9btVector3Rf
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi97:
	.cfi_def_cfa_offset 96
.Lcfi98:
	.cfi_offset %rbx, -24
.Lcfi99:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movss	(%rdi), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movq	(%rsi), %xmm12          # xmm12 = mem[0],zero
	pshufd	$229, %xmm12, %xmm5     # xmm5 = xmm12[1,1,2,3]
	movaps	%xmm9, %xmm2
	subss	%xmm12, %xmm2
	movaps	%xmm8, %xmm0
	subss	%xmm5, %xmm0
	movss	8(%rdi), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movq	8(%rsi), %xmm13         # xmm13 = mem[0],zero
	pshufd	$229, %xmm13, %xmm6     # xmm6 = xmm13[1,1,2,3]
	movaps	%xmm10, %xmm3
	subss	%xmm13, %xmm3
	movss	12(%rdi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm7
	subss	%xmm6, %xmm7
	movaps	%xmm9, %xmm4
	addss	%xmm12, %xmm4
	addss	%xmm8, %xmm5
	movaps	%xmm10, %xmm1
	addss	%xmm13, %xmm1
	addss	%xmm11, %xmm6
	mulss	%xmm2, %xmm2
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	mulss	%xmm7, %xmm7
	addss	%xmm3, %xmm7
	mulss	%xmm4, %xmm4
	mulss	%xmm5, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm1, %xmm1
	addss	%xmm5, %xmm1
	mulss	%xmm6, %xmm6
	addss	%xmm1, %xmm6
	ucomiss	%xmm7, %xmm6
	jbe	.LBB15_2
# BB#1:
	movsd	(%rsi), %xmm12          # xmm12 = mem[0],zero
	movsd	8(%rsi), %xmm13         # xmm13 = mem[0],zero
	jmp	.LBB15_3
.LBB15_2:
	movaps	.LCPI15_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm12
	xorps	%xmm0, %xmm13
.LBB15_3:                               # %_ZNK12btQuaternion7nearestERKS_.exit
	movaps	%xmm13, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movaps	%xmm5, %xmm0
	mulss	%xmm9, %xmm0
	movaps	%xmm12, %xmm1
	mulss	%xmm11, %xmm1
	subss	%xmm0, %xmm1
	movaps	%xmm12, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm6, %xmm0
	mulss	%xmm10, %xmm0
	subss	%xmm0, %xmm1
	movaps	%xmm13, %xmm2
	mulss	%xmm8, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm5, %xmm0
	mulss	%xmm8, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm11, %xmm1
	subss	%xmm0, %xmm1
	movaps	%xmm13, %xmm0
	mulss	%xmm9, %xmm0
	subss	%xmm0, %xmm1
	movaps	%xmm12, %xmm3
	mulss	%xmm10, %xmm3
	addss	%xmm1, %xmm3
	movaps	%xmm5, %xmm0
	mulss	%xmm10, %xmm0
	movaps	%xmm13, %xmm1
	mulss	%xmm11, %xmm1
	subss	%xmm0, %xmm1
	movaps	%xmm12, %xmm0
	mulss	%xmm8, %xmm0
	subss	%xmm0, %xmm1
	movaps	%xmm6, %xmm4
	mulss	%xmm9, %xmm4
	addss	%xmm1, %xmm4
	mulss	%xmm11, %xmm5
	mulss	%xmm9, %xmm12
	addss	%xmm5, %xmm12
	mulss	%xmm8, %xmm6
	addss	%xmm12, %xmm6
	mulss	%xmm10, %xmm13
	addss	%xmm6, %xmm13
	movaps	%xmm2, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm13, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB15_5
# BB#4:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movaps	%xmm13, 48(%rsp)        # 16-byte Spill
	movaps	%xmm2, 32(%rsp)         # 16-byte Spill
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	movaps	%xmm4, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movaps	(%rsp), %xmm4           # 16-byte Reload
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	movaps	32(%rsp), %xmm2         # 16-byte Reload
	movaps	48(%rsp), %xmm13        # 16-byte Reload
.LBB15_5:                               # %_ZNK12btQuaternion7nearestERKS_.exit.split
	movss	.LCPI15_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm2
	movaps	%xmm2, 32(%rsp)         # 16-byte Spill
	mulss	%xmm1, %xmm3
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	mulss	%xmm1, %xmm4
	movaps	%xmm4, (%rsp)           # 16-byte Spill
	mulss	%xmm13, %xmm1
	movaps	%xmm1, %xmm0
	callq	acosf
	movaps	(%rsp), %xmm1           # 16-byte Reload
	addss	%xmm0, %xmm0
	movss	%xmm0, (%r14)
	movaps	32(%rsp), %xmm2         # 16-byte Reload
	movss	%xmm2, (%rbx)
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 4(%rbx)
	movss	%xmm1, 8(%rbx)
	movl	$0, 12(%rbx)
	mulss	%xmm2, %xmm2
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	.LCPI15_2(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB15_7
# BB#6:
	movq	$1065353216, (%rbx)     # imm = 0x3F800000
	movq	$0, 8(%rbx)
	jmp	.LBB15_10
.LBB15_7:
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB15_9
# BB#8:                                 # %call.sqrt76
	movaps	%xmm1, %xmm0
	callq	sqrtf
.LBB15_9:                               # %.split
	movss	.LCPI15_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movss	%xmm0, (%rbx)
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movss	%xmm0, 4(%rbx)
	mulss	8(%rbx), %xmm1
	movss	%xmm1, 8(%rbx)
.LBB15_10:
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end15:
	.size	_ZN15btTransformUtil32calculateDiffAxisAngleQuaternionERK12btQuaternionS2_R9btVector3Rf, .Lfunc_end15-_ZN15btTransformUtil32calculateDiffAxisAngleQuaternionERK12btQuaternionS2_R9btVector3Rf
	.cfi_endproc

	.section	.text._ZN24btPerturbedContactResultD0Ev,"axG",@progbits,_ZN24btPerturbedContactResultD0Ev,comdat
	.weak	_ZN24btPerturbedContactResultD0Ev
	.p2align	4, 0x90
	.type	_ZN24btPerturbedContactResultD0Ev,@function
_ZN24btPerturbedContactResultD0Ev:      # @_ZN24btPerturbedContactResultD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end16:
	.size	_ZN24btPerturbedContactResultD0Ev, .Lfunc_end16-_ZN24btPerturbedContactResultD0Ev
	.cfi_endproc

	.section	.text._ZN16btManifoldResult20setShapeIdentifiersAEii,"axG",@progbits,_ZN16btManifoldResult20setShapeIdentifiersAEii,comdat
	.weak	_ZN16btManifoldResult20setShapeIdentifiersAEii
	.p2align	4, 0x90
	.type	_ZN16btManifoldResult20setShapeIdentifiersAEii,@function
_ZN16btManifoldResult20setShapeIdentifiersAEii: # @_ZN16btManifoldResult20setShapeIdentifiersAEii
	.cfi_startproc
# BB#0:
	movl	%esi, 160(%rdi)
	movl	%edx, 168(%rdi)
	retq
.Lfunc_end17:
	.size	_ZN16btManifoldResult20setShapeIdentifiersAEii, .Lfunc_end17-_ZN16btManifoldResult20setShapeIdentifiersAEii
	.cfi_endproc

	.section	.text._ZN16btManifoldResult20setShapeIdentifiersBEii,"axG",@progbits,_ZN16btManifoldResult20setShapeIdentifiersBEii,comdat
	.weak	_ZN16btManifoldResult20setShapeIdentifiersBEii
	.p2align	4, 0x90
	.type	_ZN16btManifoldResult20setShapeIdentifiersBEii,@function
_ZN16btManifoldResult20setShapeIdentifiersBEii: # @_ZN16btManifoldResult20setShapeIdentifiersBEii
	.cfi_startproc
# BB#0:
	movl	%esi, 164(%rdi)
	movl	%edx, 172(%rdi)
	retq
.Lfunc_end18:
	.size	_ZN16btManifoldResult20setShapeIdentifiersBEii, .Lfunc_end18-_ZN16btManifoldResult20setShapeIdentifiersBEii
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI19_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZN24btPerturbedContactResult15addContactPointERK9btVector3S2_f,"axG",@progbits,_ZN24btPerturbedContactResult15addContactPointERK9btVector3S2_f,comdat
	.weak	_ZN24btPerturbedContactResult15addContactPointERK9btVector3S2_f
	.p2align	4, 0x90
	.type	_ZN24btPerturbedContactResult15addContactPointERK9btVector3S2_f,@function
_ZN24btPerturbedContactResult15addContactPointERK9btVector3S2_f: # @_ZN24btPerturbedContactResult15addContactPointERK9btVector3S2_f
	.cfi_startproc
# BB#0:
	subq	$248, %rsp
.Lcfi100:
	.cfi_def_cfa_offset 256
	cmpb	$0, 376(%rdi)
	movq	(%rsi), %xmm6           # xmm6 = mem[0],zero
	pshufd	$229, %xmm6, %xmm7      # xmm7 = xmm6[1,1,2,3]
	movdqa	%xmm6, %xmm2
	mulss	%xmm0, %xmm2
	movdqa	%xmm7, %xmm1
	mulss	%xmm0, %xmm1
	movss	8(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, 112(%rsp)        # 4-byte Spill
	mulss	%xmm3, %xmm0
	movss	(%rdx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm4, 128(%rsp)        # 16-byte Spill
	addss	%xmm4, %xmm2
	movaps	%xmm2, 176(%rsp)        # 16-byte Spill
	movaps	%xmm3, 144(%rsp)        # 16-byte Spill
	addss	%xmm3, %xmm1
	movaps	%xmm1, 192(%rsp)        # 16-byte Spill
	movss	8(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, 160(%rsp)        # 16-byte Spill
	addss	%xmm1, %xmm0
	movaps	%xmm0, 208(%rsp)        # 16-byte Spill
	je	.LBB19_2
# BB#1:
	movss	200(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, (%rsp)           # 16-byte Spill
	movss	216(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	movss	184(%rdi), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movss	232(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	xorps	.LCPI19_0(%rip), %xmm4
	movss	236(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movdqa	%xmm6, 96(%rsp)         # 16-byte Spill
	movaps	%xmm13, %xmm6
	mulss	%xmm4, %xmm6
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm6
	movss	240(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	mulss	%xmm2, %xmm1
	subss	%xmm1, %xmm6
	movss	188(%rdi), %xmm15       # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm1
	mulss	%xmm4, %xmm1
	movss	204(%rdi), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm3
	mulss	%xmm0, %xmm3
	subss	%xmm3, %xmm1
	movss	220(%rdi), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm3
	mulss	%xmm2, %xmm3
	subss	%xmm3, %xmm1
	movss	192(%rdi), %xmm10       # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	movaps	%xmm10, 224(%rsp)       # 16-byte Spill
	movss	208(%rdi), %xmm14       # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm0
	subss	%xmm0, %xmm4
	movss	224(%rdi), %xmm8        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	subss	%xmm2, %xmm4
	movss	328(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	312(%rdi), %xmm5        # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	movss	332(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	316(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	movaps	%xmm13, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm5, %xmm0
	movaps	%xmm15, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm3, %xmm2
	addps	%xmm0, %xmm2
	movss	336(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movdqa	%xmm7, 80(%rsp)         # 16-byte Spill
	movss	320(%rdi), %xmm7        # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1]
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	mulps	%xmm7, %xmm10
	addps	%xmm2, %xmm10
	movaps	(%rsp), %xmm0           # 16-byte Reload
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm5, %xmm0
	movaps	%xmm5, %xmm9
	movaps	%xmm9, 48(%rsp)         # 16-byte Spill
	movaps	%xmm12, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm3, %xmm2
	movaps	%xmm3, %xmm5
	movaps	%xmm5, 64(%rsp)         # 16-byte Spill
	addps	%xmm0, %xmm2
	movaps	%xmm14, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm7, %xmm3
	addps	%xmm2, %xmm3
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm9, %xmm0
	movaps	%xmm11, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	addps	%xmm0, %xmm2
	movaps	%xmm8, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm7, %xmm5
	addps	%xmm2, %xmm5
	movss	344(%rdi), %xmm9        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm13
	movss	348(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm15
	addss	%xmm13, %xmm15
	movss	352(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	224(%rsp), %xmm13       # 16-byte Reload
	mulss	%xmm0, %xmm13
	addss	%xmm15, %xmm13
	movaps	%xmm13, %xmm15
	movaps	(%rsp), %xmm13          # 16-byte Reload
	mulss	%xmm9, %xmm13
	mulss	%xmm2, %xmm12
	addss	%xmm13, %xmm12
	mulss	%xmm0, %xmm14
	addss	%xmm12, %xmm14
	movaps	16(%rsp), %xmm12        # 16-byte Reload
	mulss	%xmm9, %xmm12
	mulss	%xmm2, %xmm11
	addss	%xmm12, %xmm11
	mulss	%xmm0, %xmm8
	addss	%xmm11, %xmm8
	movaps	%xmm6, %xmm11
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	48(%rsp), %xmm11        # 16-byte Folded Reload
	movaps	%xmm1, %xmm12
	shufps	$224, %xmm12, %xmm12    # xmm12 = xmm12[0,0,2,3]
	mulps	64(%rsp), %xmm12        # 16-byte Folded Reload
	addps	%xmm11, %xmm12
	movaps	%xmm4, %xmm11
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm7, %xmm11
	addps	%xmm12, %xmm11
	movsd	360(%rdi), %xmm7        # xmm7 = mem[0],zero
	addps	%xmm11, %xmm7
	mulss	%xmm9, %xmm6
	mulss	%xmm2, %xmm1
	addss	%xmm6, %xmm1
	mulss	%xmm0, %xmm4
	addss	%xmm1, %xmm4
	movaps	176(%rsp), %xmm6        # 16-byte Reload
	movaps	%xmm6, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm10, %xmm0
	movaps	192(%rsp), %xmm2        # 16-byte Reload
	movaps	%xmm2, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm3, %xmm1
	addps	%xmm0, %xmm1
	movaps	208(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm1, %xmm3
	addps	%xmm7, %xmm3
	movaps	%xmm15, %xmm1
	mulss	%xmm6, %xmm1
	mulss	%xmm2, %xmm14
	addss	%xmm1, %xmm14
	mulss	%xmm0, %xmm8
	addss	%xmm14, %xmm8
	addss	368(%rdi), %xmm4
	addss	%xmm4, %xmm8
	movaps	%xmm3, %xmm1
	subss	128(%rsp), %xmm1        # 16-byte Folded Reload
	movaps	%xmm3, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	subss	144(%rsp), %xmm2        # 16-byte Folded Reload
	movaps	%xmm8, %xmm0
	subss	160(%rsp), %xmm0        # 16-byte Folded Reload
	mulss	80(%rsp), %xmm2         # 16-byte Folded Reload
	movaps	96(%rsp), %xmm4         # 16-byte Reload
	mulss	%xmm4, %xmm1
	addss	%xmm1, %xmm2
	movss	112(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	addss	%xmm2, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm3, %xmm1
	movaps	%xmm5, %xmm2
	mulss	%xmm0, %xmm2
	addss	%xmm8, %xmm2
	movaps	%xmm2, %xmm3
	xorps	%xmm2, %xmm2
	movss	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1,2,3]
	movlps	%xmm1, 32(%rsp)
	movlps	%xmm2, 40(%rsp)
	jmp	.LBB19_3
.LBB19_2:
	movss	264(%rdi), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movss	280(%rdi), %xmm15       # xmm15 = mem[0],zero,zero,zero
	movss	248(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	252(%rdi), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	268(%rdi), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	296(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	xorps	.LCPI19_0(%rip), %xmm3
	movss	300(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	304(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm8
	movaps	%xmm1, %xmm13
	movaps	%xmm13, 96(%rsp)        # 16-byte Spill
	mulss	%xmm3, %xmm8
	movaps	%xmm12, %xmm1
	movaps	%xmm12, 64(%rsp)        # 16-byte Spill
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm8
	movaps	%xmm15, %xmm1
	movaps	%xmm15, 48(%rsp)        # 16-byte Spill
	mulss	%xmm2, %xmm1
	subss	%xmm1, %xmm8
	movaps	%xmm6, %xmm1
	movaps	%xmm6, (%rsp)           # 16-byte Spill
	mulss	%xmm3, %xmm1
	movaps	%xmm7, %xmm4
	mulss	%xmm0, %xmm4
	subss	%xmm4, %xmm1
	movss	284(%rdi), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm4
	mulss	%xmm2, %xmm4
	subss	%xmm4, %xmm1
	movss	256(%rdi), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	movaps	%xmm5, 80(%rsp)         # 16-byte Spill
	movss	272(%rdi), %xmm11       # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	subss	%xmm0, %xmm3
	movss	288(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	subss	%xmm2, %xmm3
	movss	328(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	312(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movss	332(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	316(%rdi), %xmm10       # xmm10 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm10   # xmm10 = xmm10[0],xmm0[0],xmm10[1],xmm0[1]
	movaps	%xmm13, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	movaps	%xmm2, %xmm13
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm10, %xmm6
	addps	%xmm0, %xmm6
	movss	336(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	320(%rdi), %xmm9        # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm9    # xmm9 = xmm9[0],xmm0[0],xmm9[1],xmm0[1]
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm9, %xmm5
	addps	%xmm6, %xmm5
	movaps	%xmm5, 16(%rsp)         # 16-byte Spill
	shufps	$224, %xmm12, %xmm12    # xmm12 = xmm12[0,0,2,3]
	mulps	%xmm13, %xmm12
	movaps	%xmm13, %xmm6
	movaps	%xmm6, 112(%rsp)        # 16-byte Spill
	movaps	%xmm7, %xmm2
	movaps	%xmm7, %xmm13
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm10, %xmm2
	addps	%xmm12, %xmm2
	movaps	%xmm11, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm9, %xmm5
	addps	%xmm2, %xmm5
	shufps	$224, %xmm15, %xmm15    # xmm15 = xmm15[0,0,2,3]
	mulps	%xmm6, %xmm15
	movaps	%xmm14, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm10, %xmm2
	addps	%xmm15, %xmm2
	movaps	%xmm4, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm9, %xmm6
	addps	%xmm2, %xmm6
	movss	344(%rdi), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	mulss	%xmm12, %xmm0
	movss	348(%rdi), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	(%rsp), %xmm15          # 16-byte Reload
	mulss	%xmm7, %xmm15
	addss	%xmm0, %xmm15
	movaps	%xmm15, %xmm0
	movss	352(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	80(%rsp), %xmm15        # 16-byte Reload
	mulss	%xmm2, %xmm15
	addss	%xmm0, %xmm15
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	mulss	%xmm12, %xmm0
	mulss	%xmm7, %xmm13
	addss	%xmm0, %xmm13
	mulss	%xmm2, %xmm11
	addss	%xmm13, %xmm11
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	mulss	%xmm12, %xmm0
	mulss	%xmm7, %xmm14
	addss	%xmm0, %xmm14
	mulss	%xmm2, %xmm4
	addss	%xmm14, %xmm4
	movaps	%xmm8, %xmm14
	shufps	$224, %xmm14, %xmm14    # xmm14 = xmm14[0,0,2,3]
	mulps	112(%rsp), %xmm14       # 16-byte Folded Reload
	movaps	%xmm1, %xmm13
	shufps	$224, %xmm13, %xmm13    # xmm13 = xmm13[0,0,2,3]
	mulps	%xmm10, %xmm13
	addps	%xmm14, %xmm13
	movaps	%xmm3, %xmm10
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	mulps	%xmm9, %xmm10
	addps	%xmm13, %xmm10
	movsd	360(%rdi), %xmm9        # xmm9 = mem[0],zero
	addps	%xmm10, %xmm9
	mulss	%xmm12, %xmm8
	mulss	%xmm7, %xmm1
	addss	%xmm8, %xmm1
	mulss	%xmm2, %xmm3
	addss	%xmm1, %xmm3
	movaps	128(%rsp), %xmm8        # 16-byte Reload
	movaps	%xmm8, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	16(%rsp), %xmm1         # 16-byte Folded Reload
	movaps	144(%rsp), %xmm7        # 16-byte Reload
	movaps	%xmm7, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	addps	%xmm1, %xmm2
	movaps	160(%rsp), %xmm5        # 16-byte Reload
	movaps	%xmm5, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm6, %xmm1
	addps	%xmm2, %xmm1
	addps	%xmm9, %xmm1
	mulss	%xmm8, %xmm15
	mulss	%xmm7, %xmm11
	addss	%xmm15, %xmm11
	mulss	%xmm5, %xmm4
	addss	%xmm11, %xmm4
	addss	368(%rdi), %xmm3
	addss	%xmm3, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm1, 32(%rsp)
	movlps	%xmm0, 40(%rsp)
	movaps	176(%rsp), %xmm3        # 16-byte Reload
	subss	%xmm1, %xmm3
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	192(%rsp), %xmm2        # 16-byte Reload
	subss	%xmm1, %xmm2
	movaps	208(%rsp), %xmm0        # 16-byte Reload
	subss	%xmm4, %xmm0
	mulss	(%rsi), %xmm3
	mulss	4(%rsi), %xmm2
	addss	%xmm3, %xmm2
	mulss	8(%rsi), %xmm0
	addss	%xmm2, %xmm0
.LBB19_3:
	movq	176(%rdi), %rdi
	movq	(%rdi), %rax
	leaq	32(%rsp), %rdx
	callq	*32(%rax)
	addq	$248, %rsp
	retq
.Lfunc_end19:
	.size	_ZN24btPerturbedContactResult15addContactPointERK9btVector3S2_f, .Lfunc_end19-_ZN24btPerturbedContactResult15addContactPointERK9btVector3S2_f
	.cfi_endproc

	.section	.text._ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev,"axG",@progbits,_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev,comdat
	.weak	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.p2align	4, 0x90
	.type	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev,@function
_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev: # @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end20:
	.size	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev, .Lfunc_end20-_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResult9DebugDrawEf,"axG",@progbits,_ZN12btConvexCast10CastResult9DebugDrawEf,comdat
	.weak	_ZN12btConvexCast10CastResult9DebugDrawEf
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResult9DebugDrawEf,@function
_ZN12btConvexCast10CastResult9DebugDrawEf: # @_ZN12btConvexCast10CastResult9DebugDrawEf
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end21:
	.size	_ZN12btConvexCast10CastResult9DebugDrawEf, .Lfunc_end21-_ZN12btConvexCast10CastResult9DebugDrawEf
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,"axG",@progbits,_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,comdat
	.weak	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,@function
_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform: # @_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end22:
	.size	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform, .Lfunc_end22-_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResultD0Ev,"axG",@progbits,_ZN12btConvexCast10CastResultD0Ev,comdat
	.weak	_ZN12btConvexCast10CastResultD0Ev
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResultD0Ev,@function
_ZN12btConvexCast10CastResultD0Ev:      # @_ZN12btConvexCast10CastResultD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end23:
	.size	_ZN12btConvexCast10CastResultD0Ev, .Lfunc_end23-_ZN12btConvexCast10CastResultD0Ev
	.cfi_endproc

	.type	_ZTVN23btConvexConvexAlgorithm10CreateFuncE,@object # @_ZTVN23btConvexConvexAlgorithm10CreateFuncE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN23btConvexConvexAlgorithm10CreateFuncE
	.p2align	3
_ZTVN23btConvexConvexAlgorithm10CreateFuncE:
	.quad	0
	.quad	_ZTIN23btConvexConvexAlgorithm10CreateFuncE
	.quad	_ZN23btConvexConvexAlgorithm10CreateFuncD2Ev
	.quad	_ZN23btConvexConvexAlgorithm10CreateFuncD0Ev
	.quad	_ZN23btConvexConvexAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN23btConvexConvexAlgorithm10CreateFuncE, 40

	.type	_ZTV23btConvexConvexAlgorithm,@object # @_ZTV23btConvexConvexAlgorithm
	.globl	_ZTV23btConvexConvexAlgorithm
	.p2align	3
_ZTV23btConvexConvexAlgorithm:
	.quad	0
	.quad	_ZTI23btConvexConvexAlgorithm
	.quad	_ZN23btConvexConvexAlgorithmD2Ev
	.quad	_ZN23btConvexConvexAlgorithmD0Ev
	.quad	_ZN23btConvexConvexAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN23btConvexConvexAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN23btConvexConvexAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.size	_ZTV23btConvexConvexAlgorithm, 56

	.type	disableCcd,@object      # @disableCcd
	.bss
	.globl	disableCcd
disableCcd:
	.byte	0                       # 0x0
	.size	disableCcd, 1

	.type	_ZTSN23btConvexConvexAlgorithm10CreateFuncE,@object # @_ZTSN23btConvexConvexAlgorithm10CreateFuncE
	.section	.rodata,"a",@progbits
	.globl	_ZTSN23btConvexConvexAlgorithm10CreateFuncE
	.p2align	4
_ZTSN23btConvexConvexAlgorithm10CreateFuncE:
	.asciz	"N23btConvexConvexAlgorithm10CreateFuncE"
	.size	_ZTSN23btConvexConvexAlgorithm10CreateFuncE, 40

	.type	_ZTS30btCollisionAlgorithmCreateFunc,@object # @_ZTS30btCollisionAlgorithmCreateFunc
	.section	.rodata._ZTS30btCollisionAlgorithmCreateFunc,"aG",@progbits,_ZTS30btCollisionAlgorithmCreateFunc,comdat
	.weak	_ZTS30btCollisionAlgorithmCreateFunc
	.p2align	4
_ZTS30btCollisionAlgorithmCreateFunc:
	.asciz	"30btCollisionAlgorithmCreateFunc"
	.size	_ZTS30btCollisionAlgorithmCreateFunc, 33

	.type	_ZTI30btCollisionAlgorithmCreateFunc,@object # @_ZTI30btCollisionAlgorithmCreateFunc
	.section	.rodata._ZTI30btCollisionAlgorithmCreateFunc,"aG",@progbits,_ZTI30btCollisionAlgorithmCreateFunc,comdat
	.weak	_ZTI30btCollisionAlgorithmCreateFunc
	.p2align	3
_ZTI30btCollisionAlgorithmCreateFunc:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS30btCollisionAlgorithmCreateFunc
	.size	_ZTI30btCollisionAlgorithmCreateFunc, 16

	.type	_ZTIN23btConvexConvexAlgorithm10CreateFuncE,@object # @_ZTIN23btConvexConvexAlgorithm10CreateFuncE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN23btConvexConvexAlgorithm10CreateFuncE
	.p2align	4
_ZTIN23btConvexConvexAlgorithm10CreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN23btConvexConvexAlgorithm10CreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN23btConvexConvexAlgorithm10CreateFuncE, 24

	.type	_ZTS23btConvexConvexAlgorithm,@object # @_ZTS23btConvexConvexAlgorithm
	.globl	_ZTS23btConvexConvexAlgorithm
	.p2align	4
_ZTS23btConvexConvexAlgorithm:
	.asciz	"23btConvexConvexAlgorithm"
	.size	_ZTS23btConvexConvexAlgorithm, 26

	.type	_ZTI23btConvexConvexAlgorithm,@object # @_ZTI23btConvexConvexAlgorithm
	.globl	_ZTI23btConvexConvexAlgorithm
	.p2align	4
_ZTI23btConvexConvexAlgorithm:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS23btConvexConvexAlgorithm
	.quad	_ZTI30btActivatingCollisionAlgorithm
	.size	_ZTI23btConvexConvexAlgorithm, 24

	.type	_ZTV24btPerturbedContactResult,@object # @_ZTV24btPerturbedContactResult
	.section	.rodata._ZTV24btPerturbedContactResult,"aG",@progbits,_ZTV24btPerturbedContactResult,comdat
	.weak	_ZTV24btPerturbedContactResult
	.p2align	3
_ZTV24btPerturbedContactResult:
	.quad	0
	.quad	_ZTI24btPerturbedContactResult
	.quad	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.quad	_ZN24btPerturbedContactResultD0Ev
	.quad	_ZN16btManifoldResult20setShapeIdentifiersAEii
	.quad	_ZN16btManifoldResult20setShapeIdentifiersBEii
	.quad	_ZN24btPerturbedContactResult15addContactPointERK9btVector3S2_f
	.size	_ZTV24btPerturbedContactResult, 56

	.type	_ZTS24btPerturbedContactResult,@object # @_ZTS24btPerturbedContactResult
	.section	.rodata._ZTS24btPerturbedContactResult,"aG",@progbits,_ZTS24btPerturbedContactResult,comdat
	.weak	_ZTS24btPerturbedContactResult
	.p2align	4
_ZTS24btPerturbedContactResult:
	.asciz	"24btPerturbedContactResult"
	.size	_ZTS24btPerturbedContactResult, 27

	.type	_ZTI24btPerturbedContactResult,@object # @_ZTI24btPerturbedContactResult
	.section	.rodata._ZTI24btPerturbedContactResult,"aG",@progbits,_ZTI24btPerturbedContactResult,comdat
	.weak	_ZTI24btPerturbedContactResult
	.p2align	4
_ZTI24btPerturbedContactResult:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS24btPerturbedContactResult
	.quad	_ZTI16btManifoldResult
	.size	_ZTI24btPerturbedContactResult, 24

	.type	_ZTVN12btConvexCast10CastResultE,@object # @_ZTVN12btConvexCast10CastResultE
	.section	.rodata._ZTVN12btConvexCast10CastResultE,"aG",@progbits,_ZTVN12btConvexCast10CastResultE,comdat
	.weak	_ZTVN12btConvexCast10CastResultE
	.p2align	3
_ZTVN12btConvexCast10CastResultE:
	.quad	0
	.quad	_ZTIN12btConvexCast10CastResultE
	.quad	_ZN12btConvexCast10CastResult9DebugDrawEf
	.quad	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.quad	_ZN12btConvexCast10CastResultD2Ev
	.quad	_ZN12btConvexCast10CastResultD0Ev
	.size	_ZTVN12btConvexCast10CastResultE, 48

	.type	_ZTSN12btConvexCast10CastResultE,@object # @_ZTSN12btConvexCast10CastResultE
	.section	.rodata._ZTSN12btConvexCast10CastResultE,"aG",@progbits,_ZTSN12btConvexCast10CastResultE,comdat
	.weak	_ZTSN12btConvexCast10CastResultE
	.p2align	4
_ZTSN12btConvexCast10CastResultE:
	.asciz	"N12btConvexCast10CastResultE"
	.size	_ZTSN12btConvexCast10CastResultE, 29

	.type	_ZTIN12btConvexCast10CastResultE,@object # @_ZTIN12btConvexCast10CastResultE
	.section	.rodata._ZTIN12btConvexCast10CastResultE,"aG",@progbits,_ZTIN12btConvexCast10CastResultE,comdat
	.weak	_ZTIN12btConvexCast10CastResultE
	.p2align	3
_ZTIN12btConvexCast10CastResultE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN12btConvexCast10CastResultE
	.size	_ZTIN12btConvexCast10CastResultE, 16


	.globl	_ZN23btConvexConvexAlgorithm10CreateFuncC1EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.type	_ZN23btConvexConvexAlgorithm10CreateFuncC1EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver,@function
_ZN23btConvexConvexAlgorithm10CreateFuncC1EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver = _ZN23btConvexConvexAlgorithm10CreateFuncC2EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	.globl	_ZN23btConvexConvexAlgorithm10CreateFuncD1Ev
	.type	_ZN23btConvexConvexAlgorithm10CreateFuncD1Ev,@function
_ZN23btConvexConvexAlgorithm10CreateFuncD1Ev = _ZN23btConvexConvexAlgorithm10CreateFuncD2Ev
	.globl	_ZN23btConvexConvexAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii
	.type	_ZN23btConvexConvexAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii,@function
_ZN23btConvexConvexAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii = _ZN23btConvexConvexAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolverii
	.globl	_ZN23btConvexConvexAlgorithmD1Ev
	.type	_ZN23btConvexConvexAlgorithmD1Ev,@function
_ZN23btConvexConvexAlgorithmD1Ev = _ZN23btConvexConvexAlgorithmD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
