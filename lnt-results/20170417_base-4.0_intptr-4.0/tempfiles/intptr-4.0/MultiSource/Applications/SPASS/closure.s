	.text
	.file	"closure.bc"
	.globl	cc_Init
	.p2align	4, 0x90
	.type	cc_Init,@function
cc_Init:                                # @cc_Init
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi0:
	.cfi_def_cfa_offset 32
	movl	$64, %edi
	callq	part_Create
	movq	%rax, cc_CLOSURE.0(%rip)
	movl	$64, %edi
	movl	$64, %esi
	movl	$64, %edx
	callq	table_Create
	movq	%rax, cc_CLOSURE.1(%rip)
	movl	$528, %edi              # imm = 0x210
	callq	memory_Malloc
	movl	$64, %ecx
	movd	%rcx, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqa	%xmm0, (%rsp)           # 16-byte Spill
	movdqu	%xmm0, (%rax)
	addq	$16, %rax
	movq	%rax, cc_CLOSURE.2(%rip)
	movl	$528, %edi              # imm = 0x210
	callq	memory_Malloc
	movaps	(%rsp), %xmm0           # 16-byte Reload
	movups	%xmm0, (%rax)
	addq	$16, %rax
	movq	%rax, cc_CLOSURE.3(%rip)
	movl	$528, %edi              # imm = 0x210
	callq	memory_Malloc
	movaps	(%rsp), %xmm0           # 16-byte Reload
	movups	%xmm0, (%rax)
	addq	$16, %rax
	movq	%rax, cc_CLOSURE.4(%rip)
	movl	$528, %edi              # imm = 0x210
	callq	memory_Malloc
	movaps	(%rsp), %xmm0           # 16-byte Reload
	movups	%xmm0, (%rax)
	addq	$16, %rax
	movq	%rax, cc_CLOSURE.5(%rip)
	movl	$3088, %edi             # imm = 0xC10
	callq	memory_Malloc
	movl	$384, %ecx              # imm = 0x180
	movd	%rcx, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqu	%xmm0, (%rax)
	addq	$16, %rax
	movq	%rax, cc_CLOSURE.6(%rip)
	addq	$24, %rsp
	retq
.Lfunc_end0:
	.size	cc_Init, .Lfunc_end0-cc_Init
	.cfi_endproc

	.globl	cc_Free
	.p2align	4, 0x90
	.type	cc_Free,@function
cc_Free:                                # @cc_Free
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 32
.Lcfi4:
	.cfi_offset %rbx, -24
.Lcfi5:
	.cfi_offset %r14, -16
	movq	cc_CLOSURE.0(%rip), %rax
	testq	%rax, %rax
	je	.LBB1_8
# BB#1:
	movl	-8(%rax), %ecx
	leal	-3(%rcx), %edx
	movslq	%edx, %rdx
	imulq	$1431655766, %rdx, %rdx # imm = 0x55555556
	movq	%rdx, %rsi
	shrq	$63, %rsi
	shrq	$32, %rdx
	addl	%esi, %edx
	movslq	%edx, %rdx
	shlq	$2, %rdx
	negq	%rdx
	leaq	-12(%rax,%rdx), %rdi
	shll	$2, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB1_2
# BB#7:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB1_8
.LBB1_2:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rdx, %rbx
	movq	%r8, (%rbx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_4
# BB#3:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB1_4:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB1_6
# BB#5:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB1_6:
	addq	$-16, %rdi
	callq	free
.LBB1_8:                                # %part_Free.exit
	movabsq	$8589934592, %r14       # imm = 0x200000000
	movq	cc_CLOSURE.1(%rip), %rdi
	callq	table_Free
	movq	cc_CLOSURE.2(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_16
# BB#9:
	leaq	-16(%rdi), %rsi
	movq	-8(%rdi), %rcx
	shlq	$32, %rcx
	addq	%r14, %rcx
	shrq	$29, %rcx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB1_10
# BB#15:
	andl	$-8, %ecx
	movq	memory_ARRAY(,%rcx,8), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	memory_ARRAY(,%rcx,8), %rax
	movq	%rsi, (%rax)
	jmp	.LBB1_16
.LBB1_10:
	movl	memory_ALIGN(%rip), %ebx
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%ebx
	subl	%edx, %ebx
	testl	%edx, %edx
	cmovel	%edx, %ebx
	addl	%ecx, %ebx
	movl	memory_OFFSET(%rip), %eax
	movq	%rsi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %r9
	movl	$memory_BIGBLOCKS, %edx
	cmovneq	%r9, %rdx
	movq	%r8, (%rdx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_12
# BB#11:
	negq	%rax
	movq	-16(%rsi,%rax), %rax
	movq	%rax, (%rcx)
.LBB1_12:
	addl	memory_MARKSIZE(%rip), %ebx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rbx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB1_14
# BB#13:
	leaq	16(%rbx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB1_14:
	addq	$-32, %rdi
	callq	free
.LBB1_16:                               # %ras_Free.exit18
	movq	cc_CLOSURE.3(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_24
# BB#17:
	leaq	-16(%rdi), %rsi
	movq	-8(%rdi), %rcx
	shlq	$32, %rcx
	addq	%r14, %rcx
	shrq	$29, %rcx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB1_18
# BB#23:
	andl	$-8, %ecx
	movq	memory_ARRAY(,%rcx,8), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	memory_ARRAY(,%rcx,8), %rax
	movq	%rsi, (%rax)
	jmp	.LBB1_24
.LBB1_18:
	movl	memory_ALIGN(%rip), %ebx
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%ebx
	subl	%edx, %ebx
	testl	%edx, %edx
	cmovel	%edx, %ebx
	addl	%ecx, %ebx
	movl	memory_OFFSET(%rip), %eax
	movq	%rsi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %r9
	movl	$memory_BIGBLOCKS, %edx
	cmovneq	%r9, %rdx
	movq	%r8, (%rdx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_20
# BB#19:
	negq	%rax
	movq	-16(%rsi,%rax), %rax
	movq	%rax, (%rcx)
.LBB1_20:
	addl	memory_MARKSIZE(%rip), %ebx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rbx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB1_22
# BB#21:
	leaq	16(%rbx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB1_22:
	addq	$-32, %rdi
	callq	free
.LBB1_24:                               # %ras_Free.exit23
	movq	cc_CLOSURE.4(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_32
# BB#25:
	leaq	-16(%rdi), %rsi
	movq	-8(%rdi), %rcx
	shlq	$32, %rcx
	addq	%r14, %rcx
	shrq	$29, %rcx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB1_26
# BB#31:
	andl	$-8, %ecx
	movq	memory_ARRAY(,%rcx,8), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	memory_ARRAY(,%rcx,8), %rax
	movq	%rsi, (%rax)
	jmp	.LBB1_32
.LBB1_26:
	movl	memory_ALIGN(%rip), %ebx
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%ebx
	subl	%edx, %ebx
	testl	%edx, %edx
	cmovel	%edx, %ebx
	addl	%ecx, %ebx
	movl	memory_OFFSET(%rip), %eax
	movq	%rsi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %r9
	movl	$memory_BIGBLOCKS, %edx
	cmovneq	%r9, %rdx
	movq	%r8, (%rdx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_28
# BB#27:
	negq	%rax
	movq	-16(%rsi,%rax), %rax
	movq	%rax, (%rcx)
.LBB1_28:
	addl	memory_MARKSIZE(%rip), %ebx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rbx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB1_30
# BB#29:
	leaq	16(%rbx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB1_30:
	addq	$-32, %rdi
	callq	free
.LBB1_32:                               # %ras_Free.exit13
	movq	cc_CLOSURE.5(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_40
# BB#33:
	leaq	-16(%rdi), %rsi
	movq	-8(%rdi), %rcx
	shlq	$32, %rcx
	addq	%r14, %rcx
	shrq	$29, %rcx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB1_34
# BB#39:
	andl	$-8, %ecx
	movq	memory_ARRAY(,%rcx,8), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	memory_ARRAY(,%rcx,8), %rax
	movq	%rsi, (%rax)
	jmp	.LBB1_40
.LBB1_34:
	movl	memory_ALIGN(%rip), %ebx
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%ebx
	subl	%edx, %ebx
	testl	%edx, %edx
	cmovel	%edx, %ebx
	addl	%ecx, %ebx
	movl	memory_OFFSET(%rip), %eax
	movq	%rsi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %r9
	movl	$memory_BIGBLOCKS, %edx
	cmovneq	%r9, %rdx
	movq	%r8, (%rdx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_36
# BB#35:
	negq	%rax
	movq	-16(%rsi,%rax), %rax
	movq	%rax, (%rcx)
.LBB1_36:
	addl	memory_MARKSIZE(%rip), %ebx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rbx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB1_38
# BB#37:
	leaq	16(%rbx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB1_38:
	addq	$-32, %rdi
	callq	free
.LBB1_40:                               # %ras_Free.exit8
	movq	cc_CLOSURE.6(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_48
# BB#41:
	leaq	-16(%rdi), %rsi
	movq	-8(%rdi), %rcx
	shlq	$32, %rcx
	addq	%r14, %rcx
	shrq	$29, %rcx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB1_42
# BB#47:
	andl	$-8, %ecx
	movq	memory_ARRAY(,%rcx,8), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	memory_ARRAY(,%rcx,8), %rax
	movq	%rsi, (%rax)
.LBB1_48:                               # %ras_Free.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_42:
	movl	memory_ALIGN(%rip), %ebx
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%ebx
	subl	%edx, %ebx
	testl	%edx, %edx
	cmovel	%edx, %ebx
	addl	%ecx, %ebx
	movl	memory_OFFSET(%rip), %eax
	movq	%rsi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %r9
	movl	$memory_BIGBLOCKS, %edx
	cmovneq	%r9, %rdx
	movq	%r8, (%rdx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_44
# BB#43:
	negq	%rax
	movq	-16(%rsi,%rax), %rax
	movq	%rax, (%rcx)
.LBB1_44:
	addl	memory_MARKSIZE(%rip), %ebx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rbx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB1_46
# BB#45:
	leaq	16(%rbx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB1_46:
	addq	$-32, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	cc_Free, .Lfunc_end1-cc_Free
	.cfi_endproc

	.globl	cc_Tautology
	.p2align	4, 0x90
	.type	cc_Tautology,@function
cc_Tautology:                           # @cc_Tautology
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 64
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movabsq	$8589934592, %r12       # imm = 0x200000000
	movq	cc_CLOSURE.2(%rip), %rdi
	movq	-8(%rdi), %rsi
	leaq	-16(%rdi), %rcx
	cmpl	$63, %esi
	jg	.LBB2_9
# BB#1:
	shlq	$32, %rsi
	addq	%r12, %rsi
	shrq	$29, %rsi
	cmpl	$1024, %esi             # imm = 0x400
	jae	.LBB2_2
# BB#7:
	andl	$-8, %esi
	movq	memory_ARRAY(,%rsi,8), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	memory_ARRAY(,%rsi,8), %rax
	movq	%rcx, (%rax)
	jmp	.LBB2_8
.LBB2_2:
	movl	memory_ALIGN(%rip), %ebp
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	%ebp
	subl	%edx, %ebp
	testl	%edx, %edx
	cmovel	%edx, %ebp
	addl	%esi, %ebp
	movl	memory_OFFSET(%rip), %eax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %r8
	testq	%rsi, %rsi
	leaq	8(%rsi), %rsi
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rsi, %rbx
	movq	%r8, (%rbx)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB2_4
# BB#3:
	negq	%rax
	movq	-16(%rcx,%rax), %rax
	movq	%rax, (%rdx)
.LBB2_4:
	addl	memory_MARKSIZE(%rip), %ebp
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rbp), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB2_6
# BB#5:
	leaq	16(%rbp,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB2_6:
	addq	$-32, %rdi
	callq	free
.LBB2_8:                                # %ras_Free.exit.i.i
	movl	$528, %edi              # imm = 0x210
	callq	memory_Malloc
	movq	%rax, %rcx
	movq	$64, 8(%rcx)
	leaq	16(%rcx), %rdi
.LBB2_9:                                # %ras_InitWithSize.exit.i
	movq	$0, (%rcx)
	movq	%rdi, cc_CLOSURE.2(%rip)
	movq	cc_CLOSURE.5(%rip), %rcx
	movq	-8(%rcx), %rbp
	leaq	-16(%rcx), %rsi
	movabsq	$4294967296, %r13       # imm = 0x100000000
	cmpl	$63, %ebp
	jg	.LBB2_18
# BB#10:
	shlq	$32, %rbp
	addq	%r12, %rbp
	shrq	$29, %rbp
	cmpl	$1024, %ebp             # imm = 0x400
	jae	.LBB2_11
# BB#16:
	andl	$-8, %ebp
	movq	memory_ARRAY(,%rbp,8), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	memory_ARRAY(,%rbp,8), %rax
	movq	%rsi, (%rax)
	jmp	.LBB2_17
.LBB2_11:
	movl	memory_ALIGN(%rip), %edi
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	%edi
	subl	%edx, %edi
	testl	%edx, %edx
	cmovel	%edx, %edi
	addl	%ebp, %edi
	movl	memory_OFFSET(%rip), %eax
	movq	%rsi, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rbp
	movq	-8(%rdx), %r8
	testq	%rbp, %rbp
	leaq	8(%rbp), %rbp
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rbp, %rbx
	movq	%r8, (%rbx)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB2_13
# BB#12:
	negq	%rax
	movq	-16(%rsi,%rax), %rax
	movq	%rax, (%rdx)
.LBB2_13:
	addl	memory_MARKSIZE(%rip), %edi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rdi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB2_15
# BB#14:
	leaq	16(%rdi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB2_15:
	addq	$-32, %rcx
	movq	%rcx, %rdi
	callq	free
.LBB2_17:                               # %ras_Free.exit.i74.i
	movl	$528, %edi              # imm = 0x210
	callq	memory_Malloc
	movq	%rax, %rsi
	movq	$64, 8(%rsi)
	leaq	16(%rsi), %rcx
	movq	cc_CLOSURE.2(%rip), %rdi
.LBB2_18:                               # %ras_InitWithSize.exit77.i
	movq	$0, (%rsi)
	movq	%rcx, cc_CLOSURE.5(%rip)
	movq	-16(%rdi), %rax
	shlq	$32, %rax
	movq	%rax, %rcx
	sarq	$29, %rcx
	movq	$0, (%rdi,%rcx)
	addq	%r13, %rax
	sarq	$32, %rax
	movq	%rax, -16(%rdi)
	movl	68(%r15), %ebp
	addl	64(%r15), %ebp
	addl	72(%r15), %ebp
	movl	%ebp, %eax
	decl	%eax
	js	.LBB2_19
# BB#20:                                # %.lr.ph135.i
	movl	$1, %eax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_21:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	24(%rcx), %rbx
	movl	(%rbx), %ecx
	cmpl	%ecx, fol_NOT(%rip)
	jne	.LBB2_23
# BB#22:                                #   in Loop: Header=BB2_21 Depth=1
	movq	16(%rbx), %rcx
	movq	8(%rcx), %rbx
	movl	(%rbx), %ecx
.LBB2_23:                               # %clause_GetLiteralAtom.exit119.i
                                        #   in Loop: Header=BB2_21 Depth=1
	cmpl	%ecx, fol_EQUALITY(%rip)
	jne	.LBB2_25
# BB#24:                                #   in Loop: Header=BB2_21 Depth=1
	movq	16(%rbx), %rcx
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movl	%eax, %edi
	callq	cc_Number
	movq	16(%rbx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movl	%eax, %edi
	jmp	.LBB2_26
	.p2align	4, 0x90
.LBB2_25:                               #   in Loop: Header=BB2_21 Depth=1
	xorl	%edx, %edx
	movl	%eax, %edi
	movq	%rbx, %rsi
.LBB2_26:                               #   in Loop: Header=BB2_21 Depth=1
	callq	cc_Number
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	incq	%r14
	cmpq	%r14, %rbp
	jne	.LBB2_21
	jmp	.LBB2_27
.LBB2_19:
	movl	$1, %eax
.LBB2_27:                               # %._crit_edge136.i
	movq	cc_CLOSURE.0(%rip), %rdi
	movl	%eax, %esi
	movq	%rax, %rbx
	callq	part_Init
	movq	%rax, cc_CLOSURE.0(%rip)
	movq	cc_CLOSURE.1(%rip), %rdi
	movl	symbol_ACTINDEX(%rip), %esi
	decl	%esi
	movq	%r15, (%rsp)            # 8-byte Spill
	movl	52(%r15), %edx
	leal	-1(%rbx), %r14d
	movl	%r14d, %ecx
	callq	table_Init
	movq	%rbx, %r8
	movq	%rax, cc_CLOSURE.1(%rip)
	movq	cc_CLOSURE.3(%rip), %r15
	movq	-8(%r15), %rsi
	leaq	-16(%r15), %rcx
	cmpl	%r8d, %esi
	jge	.LBB2_36
# BB#28:
	shlq	$32, %rsi
	addq	%r12, %rsi
	shrq	$29, %rsi
	cmpl	$1024, %esi             # imm = 0x400
	jae	.LBB2_29
# BB#34:
	andl	$-8, %esi
	movq	memory_ARRAY(,%rsi,8), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	memory_ARRAY(,%rsi,8), %rax
	movq	%rcx, (%rax)
	jmp	.LBB2_35
.LBB2_29:
	movl	memory_ALIGN(%rip), %edi
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	%edi
	subl	%edx, %edi
	testl	%edx, %edx
	cmovel	%edx, %edi
	addl	%esi, %edi
	movl	memory_OFFSET(%rip), %eax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %r8
	testq	%rsi, %rsi
	leaq	8(%rsi), %r9
	movl	$memory_BIGBLOCKS, %esi
	cmovneq	%r9, %rsi
	movq	%r8, (%rsi)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB2_31
# BB#30:
	negq	%rax
	movq	-16(%rcx,%rax), %rax
	movq	%rax, (%rdx)
.LBB2_31:
	addl	memory_MARKSIZE(%rip), %edi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rdi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB2_33
# BB#32:
	leaq	16(%rdi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB2_33:
	addq	$-32, %r15
	movq	%r15, %rdi
	callq	free
	movq	%rbx, %r8
.LBB2_35:                               # %ras_Free.exit.i106.i
	leal	16(,%r8,8), %edi
	callq	memory_Malloc
	movq	%rbx, %r8
	movq	%rax, %rcx
	movslq	%r8d, %rax
	movq	%rax, 8(%rcx)
	leaq	16(%rcx), %r15
.LBB2_36:                               # %ras_InitWithSize.exit109.i
	movq	$0, (%rcx)
	movq	cc_CLOSURE.4(%rip), %rdi
	movq	-8(%rdi), %rsi
	leaq	-16(%rdi), %rcx
	cmpl	%r8d, %esi
	jge	.LBB2_45
# BB#37:
	shlq	$32, %rsi
	addq	%r12, %rsi
	shrq	$29, %rsi
	cmpl	$1024, %esi             # imm = 0x400
	jae	.LBB2_38
# BB#43:
	andl	$-8, %esi
	movq	memory_ARRAY(,%rsi,8), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	memory_ARRAY(,%rsi,8), %rax
	movq	%rcx, (%rax)
	jmp	.LBB2_44
.LBB2_38:
	movl	memory_ALIGN(%rip), %r8d
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	%r8d
	subl	%edx, %r8d
	testl	%edx, %edx
	cmovel	%edx, %r8d
	addl	%esi, %r8d
	movl	memory_OFFSET(%rip), %eax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %r9
	testq	%rsi, %rsi
	leaq	8(%rsi), %r10
	movl	$memory_BIGBLOCKS, %esi
	cmovneq	%r10, %rsi
	movq	%r9, (%rsi)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB2_40
# BB#39:
	negq	%rax
	movq	-16(%rcx,%rax), %rax
	movq	%rax, (%rdx)
.LBB2_40:
	addl	memory_MARKSIZE(%rip), %r8d
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%r8), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB2_42
# BB#41:
	leaq	16(%r8,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB2_42:
	addq	$-32, %rdi
	callq	free
	movq	%rbx, %r8
.LBB2_44:                               # %ras_Free.exit.i98.i
	leal	16(,%r8,8), %edi
	callq	memory_Malloc
	movq	%rbx, %r8
	movq	%rax, %rcx
	movslq	%r8d, %rax
	movq	%rax, 8(%rcx)
	leaq	16(%rcx), %rdi
.LBB2_45:                               # %ras_InitWithSize.exit101.i
	movq	$0, (%rcx)
	testl	%r8d, %r8d
	jle	.LBB2_46
# BB#47:                                # %.lr.ph131.i
	movq	cc_CLOSURE.2(%rip), %rax
	movl	%r8d, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_48:                               # =>This Inner Loop Header: Depth=1
	movq	-16(%r15), %rbp
	shlq	$32, %rbp
	movq	%rbp, %rsi
	sarq	$29, %rsi
	movq	%rdx, (%r15,%rsi)
	addq	%r13, %rbp
	sarq	$32, %rbp
	movq	%rbp, -16(%r15)
	xorl	%esi, %esi
	cmpq	$0, (%rax,%rdx,8)
	setne	%sil
	movq	-16(%rdi), %rbx
	shlq	$32, %rbx
	movq	%rbx, %rbp
	sarq	$29, %rbp
	movq	%rsi, (%rdi,%rbp)
	addq	%r13, %rbx
	sarq	$32, %rbx
	movq	%rbx, -16(%rdi)
	incq	%rdx
	cmpq	%rdx, %rcx
	jne	.LBB2_48
# BB#49:                                # %._crit_edge132.i
	movq	%r15, cc_CLOSURE.3(%rip)
	movq	%rdi, cc_CLOSURE.4(%rip)
	xorl	%ebx, %ebx
	cmpl	$2, %r8d
	jl	.LBB2_52
# BB#50:                                # %.lr.ph128.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_51:                               # %.lr.ph128.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebx
	shrl	%r14d
	jne	.LBB2_51
	jmp	.LBB2_52
.LBB2_46:                               # %._crit_edge132.thread.i
	movq	%r15, cc_CLOSURE.3(%rip)
	movq	%rdi, cc_CLOSURE.4(%rip)
	xorl	%ebx, %ebx
.LBB2_52:                               # %._crit_edge129.i
	movq	cc_CLOSURE.6(%rip), %rdi
	imull	%r8d, %ebx
	movq	-8(%rdi), %rsi
	leaq	-16(%rdi), %rcx
	cmpl	%ebx, %esi
	movq	(%rsp), %r15            # 8-byte Reload
	jg	.LBB2_61
# BB#53:
	incl	%ebx
	shlq	$32, %rsi
	addq	%r12, %rsi
	shrq	$29, %rsi
	cmpl	$1024, %esi             # imm = 0x400
	jae	.LBB2_54
# BB#59:
	andl	$-8, %esi
	movq	memory_ARRAY(,%rsi,8), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	memory_ARRAY(,%rsi,8), %rax
	movq	%rcx, (%rax)
	jmp	.LBB2_60
.LBB2_54:
	movl	memory_ALIGN(%rip), %ebp
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	%ebp
	subl	%edx, %ebp
	testl	%edx, %edx
	cmovel	%edx, %ebp
	addl	%esi, %ebp
	movl	memory_OFFSET(%rip), %eax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %r8
	testq	%rsi, %rsi
	leaq	8(%rsi), %r9
	movl	$memory_BIGBLOCKS, %esi
	cmovneq	%r9, %rsi
	movq	%r8, (%rsi)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB2_56
# BB#55:
	negq	%rax
	movq	-16(%rcx,%rax), %rax
	movq	%rax, (%rdx)
.LBB2_56:
	addl	memory_MARKSIZE(%rip), %ebp
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rbp), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB2_58
# BB#57:
	leaq	16(%rbp,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB2_58:
	addq	$-32, %rdi
	callq	free
.LBB2_60:                               # %ras_Free.exit.i82.i
	leal	16(,%rbx,8), %edi
	callq	memory_Malloc
	movq	%rax, %rcx
	movslq	%ebx, %rax
	movq	%rax, 8(%rcx)
	leaq	16(%rcx), %rdi
.LBB2_61:                               # %ras_InitWithSize.exit85.i
	movq	$0, (%rcx)
	movq	%rdi, cc_CLOSURE.6(%rip)
	movl	68(%r15), %r14d
	addl	64(%r15), %r14d
	movl	%r14d, %eax
	decl	%eax
	js	.LBB2_69
# BB#62:                                # %.lr.ph.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_63:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rax
	movl	(%rax), %ecx
	cmpl	%ecx, fol_NOT(%rip)
	jne	.LBB2_65
# BB#64:                                #   in Loop: Header=BB2_63 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %ecx
.LBB2_65:                               # %clause_GetLiteralAtom.exit.i
                                        #   in Loop: Header=BB2_63 Depth=1
	cmpl	%ecx, fol_EQUALITY(%rip)
	jne	.LBB2_67
# BB#66:                                #   in Loop: Header=BB2_63 Depth=1
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movl	28(%rax), %edi
	movq	8(%rcx), %rax
	movl	28(%rax), %esi
	jmp	.LBB2_68
	.p2align	4, 0x90
.LBB2_67:                               #   in Loop: Header=BB2_63 Depth=1
	movl	28(%rax), %ebx
	movq	cc_CLOSURE.0(%rip), %rdi
	xorl	%esi, %esi
	callq	part_Find
	movl	%ebx, %edi
	movl	%eax, %esi
.LBB2_68:                               #   in Loop: Header=BB2_63 Depth=1
	callq	cc_Union
	incq	%rbp
	cmpq	%rbp, %r14
	jne	.LBB2_63
.LBB2_69:                               # %cc_InitData.exit.preheader
	movq	cc_CLOSURE.5(%rip), %rax
	movq	-16(%rax), %rcx
	testl	%ecx, %ecx
	je	.LBB2_81
# BB#70:                                # %.preheader43.preheader
	movabsq	$-4294967296, %rbp      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB2_73:                               # %.preheader43
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_75 Depth 2
                                        #     Child Loop BB2_80 Depth 2
	testl	%ecx, %ecx
	je	.LBB2_78
# BB#74:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_73 Depth=1
	leaq	-16(%rax), %rdx
	.p2align	4, 0x90
.LBB2_75:                               # %.lr.ph
                                        #   Parent Loop BB2_73 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shlq	$32, %rcx
	addq	%rbp, %rcx
	movq	%rcx, %rsi
	sarq	$32, %rsi
	movq	%rsi, (%rdx)
	sarq	$29, %rcx
	movq	(%rax,%rcx), %rbx
	movq	cc_CLOSURE.1(%rip), %rdi
	movq	cc_CLOSURE.0(%rip), %rsi
	movq	%rbx, %rdx
	callq	table_QueryAndEnter
	testq	%rax, %rax
	je	.LBB2_77
# BB#76:                                #   in Loop: Header=BB2_75 Depth=2
	movq	cc_CLOSURE.6(%rip), %rcx
	movq	-16(%rcx), %rdx
	shlq	$32, %rdx
	movq	%rdx, %rsi
	sarq	$29, %rsi
	movq	%rbx, (%rcx,%rsi)
	leaq	(%rdx,%r13), %rsi
	sarq	$29, %rsi
	movq	%rax, (%rcx,%rsi)
	addq	%r12, %rdx
	sarq	$32, %rdx
	movq	%rdx, -16(%rcx)
.LBB2_77:                               # %.backedge
                                        #   in Loop: Header=BB2_75 Depth=2
	movq	cc_CLOSURE.5(%rip), %rax
	leaq	-16(%rax), %rdx
	movq	-16(%rax), %rcx
	testl	%ecx, %ecx
	jne	.LBB2_75
.LBB2_78:                               # %.preheader
                                        #   in Loop: Header=BB2_73 Depth=1
	movq	cc_CLOSURE.6(%rip), %rcx
	movq	-16(%rcx), %rdx
	testl	%edx, %edx
	je	.LBB2_72
# BB#79:                                # %.lr.ph51.preheader
                                        #   in Loop: Header=BB2_73 Depth=1
	leaq	-16(%rcx), %rax
	.p2align	4, 0x90
.LBB2_80:                               # %.lr.ph51
                                        #   Parent Loop BB2_73 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cc_CLOSURE.0(%rip), %rdi
	shlq	$32, %rdx
	addq	%rbp, %rdx
	movq	%rdx, %rsi
	sarq	$32, %rsi
	movq	%rsi, (%rax)
	sarq	$29, %rdx
	movq	(%rcx,%rdx), %rax
	movl	28(%rax), %esi
	callq	part_Find
	movl	%eax, %ebx
	movq	cc_CLOSURE.0(%rip), %rdi
	movq	cc_CLOSURE.6(%rip), %rax
	movq	-16(%rax), %rcx
	shlq	$32, %rcx
	addq	%rbp, %rcx
	movq	%rcx, %rdx
	sarq	$32, %rdx
	movq	%rdx, -16(%rax)
	sarq	$29, %rcx
	movq	(%rax,%rcx), %rax
	movl	28(%rax), %esi
	callq	part_Find
	movl	%ebx, %edi
	movl	%eax, %esi
	callq	cc_Union
	movq	cc_CLOSURE.6(%rip), %rcx
	leaq	-16(%rcx), %rax
	movq	-16(%rcx), %rdx
	testl	%edx, %edx
	jne	.LBB2_80
# BB#71:                                # %cc_InitData.exit.loopexit.loopexit
                                        #   in Loop: Header=BB2_73 Depth=1
	movq	cc_CLOSURE.5(%rip), %rax
.LBB2_72:                               # %cc_InitData.exit.loopexit
                                        #   in Loop: Header=BB2_73 Depth=1
	movq	-16(%rax), %rcx
	testl	%ecx, %ecx
	jne	.LBB2_73
.LBB2_81:                               # %cc_InitData.exit._crit_edge
	movl	68(%r15), %eax
	movl	72(%r15), %ecx
	addl	64(%r15), %eax
	leal	-1(%rcx,%rax), %edx
	xorl	%ecx, %ecx
	cmpl	%edx, %eax
	jg	.LBB2_90
# BB#82:                                # %.lr.ph.i27
	movslq	%eax, %rbx
	movslq	%edx, %r12
	.p2align	4, 0x90
.LBB2_83:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rax
	movl	(%rax), %ecx
	cmpl	%ecx, fol_NOT(%rip)
	jne	.LBB2_85
# BB#84:                                #   in Loop: Header=BB2_83 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %ecx
.LBB2_85:                               # %clause_GetLiteralAtom.exit.i38
                                        #   in Loop: Header=BB2_83 Depth=1
	xorl	%r15d, %r15d
	movq	cc_CLOSURE.0(%rip), %r14
	cmpl	%ecx, fol_EQUALITY(%rip)
	jne	.LBB2_87
# BB#86:                                #   in Loop: Header=BB2_83 Depth=1
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	8(%rcx), %rcx
	movl	28(%rcx), %r15d
.LBB2_87:                               #   in Loop: Header=BB2_83 Depth=1
	movl	28(%rax), %esi
	movq	%r14, %rdi
	callq	part_Find
	movl	%eax, %ebp
	movq	%r14, %rdi
	movl	%r15d, %esi
	callq	part_Find
	cmpl	%eax, %ebp
	setne	%cl
	cmpq	%r12, %rbx
	movq	(%rsp), %r15            # 8-byte Reload
	jge	.LBB2_89
# BB#88:                                #   in Loop: Header=BB2_83 Depth=1
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB2_83
.LBB2_89:                               # %.critedge.loopexit.i
	xorl	%ecx, %ecx
	cmpl	%eax, %ebp
	sete	%cl
.LBB2_90:                               # %cc_Outit.exit
	movl	%ecx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	cc_Tautology, .Lfunc_end2-cc_Tautology
	.cfi_endproc

	.p2align	4, 0x90
	.type	cc_Union,@function
cc_Union:                               # @cc_Union
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 64
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movl	%edi, %r12d
	cmpl	%esi, %r12d
	jne	.LBB3_1
# BB#8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_1:
	movq	cc_CLOSURE.4(%rip), %rax
	movslq	%r12d, %rcx
	movl	(%rax,%rcx,8), %ecx
	movslq	%esi, %rdx
	cmpl	(%rax,%rdx,8), %ecx
	movl	%esi, %r14d
	cmovll	%r12d, %r14d
	cmovll	%esi, %r12d
	movslq	%r14d, %r15
	movq	(%rax,%r15,8), %rcx
	testl	%ecx, %ecx
	jle	.LBB3_5
# BB#2:                                 # %.lr.ph.preheader
	leal	1(%rcx), %ebx
	movl	%r14d, %r13d
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	cc_CLOSURE.2(%rip), %rax
	movslq	%r13d, %rcx
	movq	(%rax,%rcx,8), %rbp
	movq	cc_CLOSURE.3(%rip), %rax
	movl	(%rax,%rcx,8), %r13d
	movq	cc_CLOSURE.1(%rip), %rdi
	movq	%rbp, %rsi
	callq	table_Delete
	movq	cc_CLOSURE.5(%rip), %rdi
	movq	%rbp, %rsi
	callq	ras_Push
	movq	%rax, cc_CLOSURE.5(%rip)
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB3_3
# BB#4:                                 # %._crit_edge.loopexit
	movq	cc_CLOSURE.4(%rip), %rax
	movq	(%rax,%r15,8), %rcx
.LBB3_5:                                # %._crit_edge
	testl	%ecx, %ecx
	jle	.LBB3_7
# BB#6:
	movq	cc_CLOSURE.3(%rip), %rcx
	movslq	%r12d, %rdx
	movslq	(%rcx,%rdx,8), %rsi
	movslq	(%rcx,%r15,8), %rdi
	movq	%rdi, (%rcx,%rdx,8)
	movq	%rsi, (%rcx,%r15,8)
	movl	(%rax,%r15,8), %ecx
	addl	(%rax,%rdx,8), %ecx
	movslq	%ecx, %rcx
	movq	%rcx, (%rax,%rdx,8)
.LBB3_7:
	movq	cc_CLOSURE.0(%rip), %rdi
	movl	%r12d, %esi
	movl	%r14d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	part_Union              # TAILCALL
.Lfunc_end3:
	.size	cc_Union, .Lfunc_end3-cc_Union
	.cfi_endproc

	.p2align	4, 0x90
	.type	cc_Number,@function
cc_Number:                              # @cc_Number
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %ebp
	movl	%ebp, 28(%r14)
	incl	%ebp
	movq	cc_CLOSURE.2(%rip), %rdi
	movq	%rdx, %rsi
	callq	ras_Push
	movq	%rax, cc_CLOSURE.2(%rip)
	movq	cc_CLOSURE.5(%rip), %rdi
	movq	%r14, %rsi
	callq	ras_Push
	movq	%rax, cc_CLOSURE.5(%rip)
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB4_3
	.p2align	4, 0x90
.LBB4_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movl	%ebp, %edi
	movq	%r14, %rdx
	callq	cc_Number
	movl	%eax, %ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_1
.LBB4_3:                                # %._crit_edge
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end4:
	.size	cc_Number, .Lfunc_end4-cc_Number
	.cfi_endproc

	.p2align	4, 0x90
	.type	ras_Push,@function
ras_Push:                               # @ras_Push
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 48
.Lcfi43:
	.cfi_offset %rbx, -48
.Lcfi44:
	.cfi_offset %r12, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	-16(%rbx), %edi
	movq	-8(%rbx), %r12
	cmpl	%r12d, %edi
	jne	.LBB5_13
# BB#1:
	leal	(%rdi,%rdi), %ebp
	shll	$4, %edi
	addl	$16, %edi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	memory_Malloc
	leaq	16(%rax), %r15
	movslq	%ebp, %rcx
	movq	%rcx, 8(%rax)
	movslq	%r12d, %rcx
	movq	%rcx, (%rax)
	leaq	-8(%rbx,%rcx,8), %rdx
	cmpq	%rbx, %rdx
	jb	.LBB5_5
# BB#2:                                 # %.lr.ph.preheader
	shlq	$3, %rcx
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rbx,%rcx), %rdx
	movq	%rdx, 8(%rax,%rcx)
	leaq	-16(%rbx,%rcx), %rdx
	addq	$-8, %rcx
	cmpq	%rbx, %rdx
	jae	.LBB5_3
# BB#4:                                 # %._crit_edge
	testq	%rbx, %rbx
	je	.LBB5_12
.LBB5_5:                                # %._crit_edge.thread
	leaq	-16(%rbx), %rsi
	movq	-8(%rbx), %rax
	shlq	$32, %rax
	movabsq	$8589934592, %rcx       # imm = 0x200000000
	addq	%rax, %rcx
	shrq	$29, %rcx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB5_6
# BB#11:
	andl	$-8, %ecx
	movq	memory_ARRAY(,%rcx,8), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	memory_ARRAY(,%rcx,8), %rax
	movq	%rsi, (%rax)
	jmp	.LBB5_12
.LBB5_6:
	movl	memory_ALIGN(%rip), %edi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%edi
	subl	%edx, %edi
	testl	%edx, %edx
	cmovel	%edx, %edi
	addl	%ecx, %edi
	movl	memory_OFFSET(%rip), %eax
	movq	%rsi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rdx, %rbp
	movq	%r8, (%rbp)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB5_8
# BB#7:
	negq	%rax
	movq	-16(%rsi,%rax), %rax
	movq	%rax, (%rcx)
.LBB5_8:
	addl	memory_MARKSIZE(%rip), %edi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rdi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB5_10
# BB#9:
	leaq	16(%rdi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB5_10:
	addq	$-32, %rbx
	movq	%rbx, %rdi
	callq	free
.LBB5_12:                               # %ras_Free.exit
	movq	%r15, %rbx
.LBB5_13:                               # %ras_Free.exit
	movq	-16(%rbx), %rax
	shlq	$32, %rax
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	addq	%rax, %rcx
	sarq	$29, %rax
	movq	%r14, (%rbx,%rax)
	sarq	$32, %rcx
	movq	%rcx, -16(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	ras_Push, .Lfunc_end5-ras_Push
	.cfi_endproc

	.type	cc_CLOSURE.0,@object    # @cc_CLOSURE.0
	.local	cc_CLOSURE.0
	.comm	cc_CLOSURE.0,8,8
	.type	cc_CLOSURE.1,@object    # @cc_CLOSURE.1
	.local	cc_CLOSURE.1
	.comm	cc_CLOSURE.1,8,8
	.type	cc_CLOSURE.2,@object    # @cc_CLOSURE.2
	.local	cc_CLOSURE.2
	.comm	cc_CLOSURE.2,8,8
	.type	cc_CLOSURE.3,@object    # @cc_CLOSURE.3
	.local	cc_CLOSURE.3
	.comm	cc_CLOSURE.3,8,8
	.type	cc_CLOSURE.4,@object    # @cc_CLOSURE.4
	.local	cc_CLOSURE.4
	.comm	cc_CLOSURE.4,8,8
	.type	cc_CLOSURE.5,@object    # @cc_CLOSURE.5
	.local	cc_CLOSURE.5
	.comm	cc_CLOSURE.5,8,8
	.type	cc_CLOSURE.6,@object    # @cc_CLOSURE.6
	.local	cc_CLOSURE.6
	.comm	cc_CLOSURE.6,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
