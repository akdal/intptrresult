	.text
	.file	"fannkuch.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
.LCPI0_1:
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
.LCPI0_2:
	.quad	-2                      # 0xfffffffffffffffe
	.quad	-3                      # 0xfffffffffffffffd
.LCPI0_3:
	.long	4294967292              # 0xfffffffc
	.long	4294967291              # 0xfffffffb
	.long	4294967290              # 0xfffffffa
	.long	4294967289              # 0xfffffff9
.LCPI0_4:
	.quad	-8                      # 0xfffffffffffffff8
	.quad	-9                      # 0xfffffffffffffff7
.LCPI0_5:
	.quad	-10                     # 0xfffffffffffffff6
	.quad	-11                     # 0xfffffffffffffff5
.LCPI0_6:
	.long	4294967292              # 0xfffffffc
	.long	4294967292              # 0xfffffffc
	.long	4294967292              # 0xfffffffc
	.long	4294967292              # 0xfffffffc
.LCPI0_7:
	.quad	-8                      # 0xfffffffffffffff8
	.quad	-8                      # 0xfffffffffffffff8
.LCPI0_8:
	.long	4294967284              # 0xfffffff4
	.long	4294967284              # 0xfffffff4
	.long	4294967284              # 0xfffffff4
	.long	4294967284              # 0xfffffff4
.LCPI0_9:
	.quad	-16                     # 0xfffffffffffffff0
	.quad	-16                     # 0xfffffffffffffff0
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	$11, %edi
	movl	$4, %esi
	callq	calloc
	movq	%rax, %rbx
	movl	$11, %edi
	movl	$4, %esi
	callq	calloc
	movq	%rax, %r14
	movl	$11, %edi
	movl	$4, %esi
	callq	calloc
	movq	%rax, %r15
	leaq	4(%r14), %r13
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1,2,3,4]
	movups	%xmm0, 4(%r14)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [5,6,7,8]
	movups	%xmm0, 20(%r14)
	movabsq	$42949672969, %rax      # imm = 0xA00000009
	movq	%rax, 36(%r14)
	leaq	20(%r14), %r9
	movq	%rbx, %rax
	addq	$4, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$11, %r12d
	movq	$-1, %rax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqa	%xmm0, 32(%rsp)         # 16-byte Spill
	movdqa	.LCPI0_6(%rip), %xmm5   # xmm5 = [4294967292,4294967292,4294967292,4294967292]
	movdqa	.LCPI0_7(%rip), %xmm6   # xmm6 = [18446744073709551608,18446744073709551608]
	movdqa	.LCPI0_8(%rip), %xmm7   # xmm7 = [4294967284,4294967284,4294967284,4294967284]
	movdqa	.LCPI0_9(%rip), %xmm8   # xmm8 = [18446744073709551600,18446744073709551600]
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
	movq	%r9, 24(%rsp)           # 8-byte Spill
.LBB0_1:                                # %.loopexit.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_10 Depth 2
                                        #     Child Loop BB0_13 Depth 2
                                        #     Child Loop BB0_17 Depth 2
                                        #       Child Loop BB0_19 Depth 3
                                        #     Child Loop BB0_24 Depth 2
                                        #       Child Loop BB0_28 Depth 3
                                        #       Child Loop BB0_32 Depth 3
	cmpl	$29, %r11d
	jg	.LBB0_3
# BB#2:                                 # %.preheader3.preheader.i
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	(%r14), %esi
	incl	%esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%r10, %rbp
	movl	%r11d, 12(%rsp)         # 4-byte Spill
	callq	printf
	movl	4(%r14), %esi
	incl	%esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	8(%r14), %esi
	incl	%esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	12(%r14), %esi
	incl	%esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	16(%r14), %esi
	incl	%esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	20(%r14), %esi
	incl	%esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	24(%r14), %esi
	incl	%esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	28(%r14), %esi
	incl	%esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	32(%r14), %esi
	incl	%esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	36(%r14), %esi
	incl	%esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	40(%r14), %esi
	incl	%esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	12(%rsp), %r11d         # 4-byte Reload
	movdqa	.LCPI0_9(%rip), %xmm8   # xmm8 = [18446744073709551600,18446744073709551600]
	movdqa	.LCPI0_8(%rip), %xmm7   # xmm7 = [4294967284,4294967284,4294967284,4294967284]
	movdqa	.LCPI0_7(%rip), %xmm6   # xmm6 = [18446744073709551608,18446744073709551608]
	movdqa	.LCPI0_6(%rip), %xmm5   # xmm5 = [4294967292,4294967292,4294967292,4294967292]
	movq	%rbp, %r10
	movq	24(%rsp), %r9           # 8-byte Reload
	incl	%r11d
.LBB0_3:                                # %.preheader2.i
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$1, %r12d
	je	.LBB0_14
# BB#4:                                 # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_1 Depth=1
	movslq	%r12d, %rax
	addl	$-2, %r12d
	incq	%r12
	cmpq	$8, %r12
	jb	.LBB0_13
# BB#5:                                 # %min.iters.checked13
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	%r12, %rcx
	movabsq	$8589934584, %rdx       # imm = 0x1FFFFFFF8
	movq	%rdx, %rsi
	andq	%rsi, %rcx
	movq	%r12, %rdx
	andq	%rsi, %rdx
	je	.LBB0_13
# BB#6:                                 # %vector.ph17
                                        #   in Loop: Header=BB0_1 Depth=1
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm2       # xmm2 = xmm0[0,1,0,1]
	movdqa	%xmm2, %xmm0
	paddq	32(%rsp), %xmm0         # 16-byte Folded Reload
	movdqa	%xmm2, %xmm1
	paddq	.LCPI0_2(%rip), %xmm1
	leaq	-8(%rdx), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	btl	$3, %edi
	jb	.LBB0_8
# BB#7:                                 # %vector.body9.prol
                                        #   in Loop: Header=BB0_1 Depth=1
	movdqa	%xmm2, %xmm3
	shufps	$136, %xmm3, %xmm3      # xmm3 = xmm3[0,2,0,2]
	paddd	.LCPI0_3(%rip), %xmm3
	shufps	$34, %xmm0, %xmm1       # xmm1 = xmm1[2,0],xmm0[2,0]
	movups	%xmm1, -16(%r15,%rax,4)
	pshufd	$27, %xmm3, %xmm0       # xmm0 = xmm3[3,2,1,0]
	movdqu	%xmm0, -32(%r15,%rax,4)
	movdqa	%xmm2, %xmm0
	paddq	.LCPI0_4(%rip), %xmm0
	paddq	.LCPI0_5(%rip), %xmm2
	movl	$8, %edi
	movdqa	%xmm2, %xmm1
	testq	%rsi, %rsi
	jne	.LBB0_9
	jmp	.LBB0_11
.LBB0_8:                                #   in Loop: Header=BB0_1 Depth=1
	xorl	%edi, %edi
	testq	%rsi, %rsi
	je	.LBB0_11
.LBB0_9:                                # %vector.ph17.new
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	%rdx, %rsi
	subq	%rdi, %rsi
	.p2align	4, 0x90
.LBB0_10:                               # %vector.body9
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	%xmm0, %rdi
	movdqa	%xmm0, %xmm2
	shufps	$136, %xmm1, %xmm2      # xmm2 = xmm2[0,2],xmm1[0,2]
	movd	%edi, %xmm3
	movss	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1,2,3]
	movaps	%xmm2, %xmm3
	paddd	%xmm5, %xmm3
	movaps	%xmm2, %xmm4
	shufps	$27, %xmm4, %xmm4       # xmm4 = xmm4[3,2,1,0]
	movups	%xmm4, -16(%r15,%rdi,4)
	pshufd	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0]
	movdqu	%xmm3, -32(%r15,%rdi,4)
	movdqa	%xmm1, %xmm3
	paddq	%xmm6, %xmm3
	movdqa	%xmm0, %xmm4
	paddq	%xmm6, %xmm4
	movd	%xmm4, %rdi
	shufps	$136, %xmm3, %xmm4      # xmm4 = xmm4[0,2],xmm3[0,2]
	movd	%edi, %xmm3
	movss	%xmm3, %xmm4            # xmm4 = xmm3[0],xmm4[1,2,3]
	paddd	%xmm7, %xmm2
	shufps	$27, %xmm4, %xmm4       # xmm4 = xmm4[3,2,1,0]
	movups	%xmm4, -16(%r15,%rdi,4)
	pshufd	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	movdqu	%xmm2, -32(%r15,%rdi,4)
	paddq	%xmm8, %xmm0
	paddq	%xmm8, %xmm1
	addq	$-16, %rsi
	jne	.LBB0_10
.LBB0_11:                               # %middle.block10
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpq	%rdx, %r12
	je	.LBB0_14
# BB#12:                                #   in Loop: Header=BB0_1 Depth=1
	subq	%rcx, %rax
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph.i
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, -4(%r15,%rax,4)
	decq	%rax
	cmpl	$1, %eax
	jne	.LBB0_13
.LBB0_14:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	(%r14), %r8d
	testl	%r8d, %r8d
	je	.LBB0_22
# BB#15:                                #   in Loop: Header=BB0_1 Depth=1
	cmpl	$10, 40(%r14)
	je	.LBB0_22
# BB#16:                                # %.preheader1.preheader.i
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	32(%r13), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 32(%rcx)
	movdqu	(%r13), %xmm0
	movdqu	16(%r13), %xmm1
	movdqu	%xmm1, 16(%rcx)
	movdqu	%xmm0, (%rcx)
	movl	%r8d, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_17:                               #   Parent Loop BB0_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_19 Depth 3
	movq	%rax, %rcx
	leal	-1(%rdx), %eax
	cmpl	$2, %eax
	jl	.LBB0_20
# BB#18:                                # %.lr.ph13.preheader.i
                                        #   in Loop: Header=BB0_17 Depth=2
	movslq	%eax, %rsi
	decq	%rsi
	movl	$1, %eax
	.p2align	4, 0x90
.LBB0_19:                               # %.lr.ph13.i
                                        #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbx,%rax,4), %edi
	movl	4(%rbx,%rsi,4), %ebp
	movl	%ebp, (%rbx,%rax,4)
	movl	%edi, 4(%rbx,%rsi,4)
	incq	%rax
	cmpq	%rsi, %rax
	leaq	-1(%rsi), %rsi
	jl	.LBB0_19
.LBB0_20:                               # %._crit_edge14.i
                                        #   in Loop: Header=BB0_17 Depth=2
	leaq	1(%rcx), %rax
	movslq	%edx, %rsi
	movl	(%rbx,%rsi,4), %edi
	movl	%edx, (%rbx,%rsi,4)
	testl	%edi, %edi
	movl	%edi, %edx
	jne	.LBB0_17
# BB#21:                                #   in Loop: Header=BB0_1 Depth=1
	cmpq	%rcx, %r10
	cmovleq	%rax, %r10
.LBB0_22:                               # %.lr.ph20.preheader.i
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	$1, %edi
	xorl	%ecx, %ecx
	movl	$1, %r12d
	jmp	.LBB0_24
	.p2align	4, 0x90
.LBB0_23:                               # %..lr.ph20_crit_edge.i
                                        #   in Loop: Header=BB0_24 Depth=2
	incl	%eax
	movl	(%r14), %r8d
	movl	%eax, %edi
.LBB0_24:                               # %.lr.ph17.preheader.i
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_28 Depth 3
                                        #       Child Loop BB0_32 Depth 3
	movl	%edi, %eax
	incl	%ecx
	cmpl	$8, %ecx
	jb	.LBB0_30
# BB#26:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_24 Depth=2
	movl	%ecx, %esi
	movl	%ecx, %ebp
	andl	$7, %ebp
	subq	%rbp, %rsi
	je	.LBB0_30
# BB#27:                                # %vector.body.preheader
                                        #   in Loop: Header=BB0_24 Depth=2
	andl	$7, %edi
	movq	%rax, %rdx
	subq	%rdi, %rdx
	movq	%r9, %rdi
	.p2align	4, 0x90
.LBB0_28:                               # %vector.body
                                        #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	%xmm0, -20(%rdi)
	movdqu	%xmm1, -4(%rdi)
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB0_28
# BB#29:                                # %middle.block
                                        #   in Loop: Header=BB0_24 Depth=2
	testl	%ebp, %ebp
	jne	.LBB0_31
	jmp	.LBB0_33
	.p2align	4, 0x90
.LBB0_30:                               #   in Loop: Header=BB0_24 Depth=2
	xorl	%esi, %esi
.LBB0_31:                               # %.lr.ph17.i.preheader
                                        #   in Loop: Header=BB0_24 Depth=2
	leaq	(%r13,%rsi,4), %rdx
	movq	%rax, %rdi
	subq	%rsi, %rdi
	.p2align	4, 0x90
.LBB0_32:                               # %.lr.ph17.i
                                        #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdx), %esi
	movl	%esi, -4(%rdx)
	addq	$4, %rdx
	decq	%rdi
	jne	.LBB0_32
.LBB0_33:                               # %._crit_edge18.i
                                        #   in Loop: Header=BB0_24 Depth=2
	movl	%r8d, (%r14,%r12,4)
	movl	(%r15,%r12,4), %edx
	leal	-1(%rdx), %esi
	movl	%esi, (%r15,%r12,4)
	cmpl	$1, %edx
	jg	.LBB0_1
# BB#34:                                #   in Loop: Header=BB0_24 Depth=2
	incq	%r12
	cmpl	$11, %r12d
	jne	.LBB0_23
# BB#35:                                # %fannkuch.exit
	movl	$.L.str, %edi
	movl	$11, %esi
	xorl	%eax, %eax
	movq	%r10, %rdx
	callq	printf
	xorl	%eax, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Pfannkuchen(%d) = %ld\n"
	.size	.L.str, 23

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%d"
	.size	.L.str.1, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
