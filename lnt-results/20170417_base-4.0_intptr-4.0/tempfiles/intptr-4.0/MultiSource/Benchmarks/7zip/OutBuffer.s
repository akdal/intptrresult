	.text
	.file	"OutBuffer.bc"
	.globl	_ZN10COutBuffer6CreateEj
	.p2align	4, 0x90
	.type	_ZN10COutBuffer6CreateEj,@function
_ZN10COutBuffer6CreateEj:               # @_ZN10COutBuffer6CreateEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	testl	%esi, %esi
	movl	$1, %ebp
	cmovnel	%esi, %ebp
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_2
# BB#1:
	movb	$1, %al
	cmpl	%ebp, 20(%rbx)
	je	.LBB0_3
.LBB0_2:                                # %._crit_edge
	callq	MidFree
	movq	$0, (%rbx)
	movl	%ebp, 20(%rbx)
	movl	%ebp, %edi
	callq	MidAlloc
	movq	%rax, (%rbx)
	testq	%rax, %rax
	setne	%al
.LBB0_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN10COutBuffer6CreateEj, .Lfunc_end0-_ZN10COutBuffer6CreateEj
	.cfi_endproc

	.globl	_ZN10COutBuffer4FreeEv
	.p2align	4, 0x90
	.type	_ZN10COutBuffer4FreeEv,@function
_ZN10COutBuffer4FreeEv:                 # @_ZN10COutBuffer4FreeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	callq	MidFree
	movq	$0, (%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN10COutBuffer4FreeEv, .Lfunc_end1-_ZN10COutBuffer4FreeEv
	.cfi_endproc

	.globl	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream,@function
_ZN10COutBuffer9SetStreamEP20ISequentialOutStream: # @_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB2_2
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB2_2:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB2_4:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit
	movq	%rbx, 24(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream, .Lfunc_end2-_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN10COutBuffer4InitEv
	.p2align	4, 0x90
	.type	_ZN10COutBuffer4InitEv,@function
_ZN10COutBuffer4InitEv:                 # @_ZN10COutBuffer4InitEv
	.cfi_startproc
# BB#0:
	movl	$0, 16(%rdi)
	movl	20(%rdi), %eax
	movl	%eax, 12(%rdi)
	movl	$0, 8(%rdi)
	movq	$0, 32(%rdi)
	movb	$0, 48(%rdi)
	retq
.Lfunc_end3:
	.size	_ZN10COutBuffer4InitEv, .Lfunc_end3-_ZN10COutBuffer4InitEv
	.cfi_endproc

	.globl	_ZNK10COutBuffer16GetProcessedSizeEv
	.p2align	4, 0x90
	.type	_ZNK10COutBuffer16GetProcessedSizeEv,@function
_ZNK10COutBuffer16GetProcessedSizeEv:   # @_ZNK10COutBuffer16GetProcessedSizeEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %ecx
	movl	16(%rdi), %edx
	movq	%rcx, %rax
	addq	32(%rdi), %rax
	subq	%rdx, %rax
	cmpl	%ecx, %edx
	jbe	.LBB4_2
# BB#1:
	movl	20(%rdi), %ecx
	addq	%rcx, %rax
.LBB4_2:
	retq
.Lfunc_end4:
	.size	_ZNK10COutBuffer16GetProcessedSizeEv, .Lfunc_end4-_ZNK10COutBuffer16GetProcessedSizeEv
	.cfi_endproc

	.globl	_ZN10COutBuffer9FlushPartEv
	.p2align	4, 0x90
	.type	_ZN10COutBuffer9FlushPartEv,@function
_ZN10COutBuffer9FlushPartEv:            # @_ZN10COutBuffer9FlushPartEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	8(%rbx), %ebp
	movl	16(%rbx), %esi
	cmpl	%ebp, %esi
	jb	.LBB5_2
# BB#1:
	movl	20(%rbx), %ebp
.LBB5_2:
	subl	%esi, %ebp
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_4
# BB#3:
	addq	(%rbx), %rsi
	movl	%ebp, %r14d
	movq	%r14, %rdx
	callq	memmove
	addq	%r14, 40(%rbx)
.LBB5_4:
	movq	24(%rbx), %rdi
	xorl	%r14d, %r14d
	testq	%rdi, %rdi
	je	.LBB5_5
# BB#6:
	movl	$0, 12(%rsp)
	movq	(%rdi), %rax
	movl	16(%rbx), %esi
	addq	(%rbx), %rsi
	leaq	12(%rsp), %rcx
	movl	%ebp, %edx
	callq	*40(%rax)
	movl	12(%rsp), %ebp
	jmp	.LBB5_7
.LBB5_5:
	xorl	%eax, %eax
.LBB5_7:
	movl	16(%rbx), %ecx
	addl	%ebp, %ecx
	movl	20(%rbx), %edx
	cmpl	%edx, %ecx
	cmovel	%r14d, %ecx
	movl	%ecx, 16(%rbx)
	movl	8(%rbx), %esi
	cmpl	%edx, %esi
	jne	.LBB5_9
# BB#8:
	movb	$1, 48(%rbx)
	movl	$0, 8(%rbx)
	xorl	%esi, %esi
.LBB5_9:
	cmpl	%esi, %ecx
	cmoval	%ecx, %edx
	movl	%edx, 12(%rbx)
	movl	%ebp, %ecx
	addq	%rcx, 32(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN10COutBuffer9FlushPartEv, .Lfunc_end5-_ZN10COutBuffer9FlushPartEv
	.cfi_endproc

	.globl	_ZN10COutBuffer5FlushEv
	.p2align	4, 0x90
	.type	_ZN10COutBuffer5FlushEv,@function
_ZN10COutBuffer5FlushEv:                # @_ZN10COutBuffer5FlushEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 64
.Lcfi25:
	.cfi_offset %rbx, -48
.Lcfi26:
	.cfi_offset %r12, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	8(%rbx), %ebp
	movl	16(%rbx), %ecx
	xorl	%r12d, %r12d
	leaq	12(%rsp), %r14
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	cmpl	%ebp, %ecx
	je	.LBB6_2
# BB#3:                                 #   in Loop: Header=BB6_1 Depth=1
	jb	.LBB6_5
# BB#4:                                 #   in Loop: Header=BB6_1 Depth=1
	movl	20(%rbx), %ebp
.LBB6_5:                                #   in Loop: Header=BB6_1 Depth=1
	subl	%ecx, %ebp
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_7
# BB#6:                                 #   in Loop: Header=BB6_1 Depth=1
	movl	%ecx, %esi
	addq	(%rbx), %rsi
	movl	%ebp, %r15d
	movq	%r15, %rdx
	callq	memmove
	addq	%r15, 40(%rbx)
.LBB6_7:                                #   in Loop: Header=BB6_1 Depth=1
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_8
# BB#9:                                 #   in Loop: Header=BB6_1 Depth=1
	movl	$0, 12(%rsp)
	movq	(%rdi), %rax
	movl	16(%rbx), %esi
	addq	(%rbx), %rsi
	movl	%ebp, %edx
	movq	%r14, %rcx
	callq	*40(%rax)
	movl	12(%rsp), %ebp
	jmp	.LBB6_10
	.p2align	4, 0x90
.LBB6_8:                                #   in Loop: Header=BB6_1 Depth=1
	xorl	%eax, %eax
.LBB6_10:                               #   in Loop: Header=BB6_1 Depth=1
	movl	16(%rbx), %ecx
	addl	%ebp, %ecx
	movl	20(%rbx), %edx
	cmpl	%edx, %ecx
	cmovel	%r12d, %ecx
	movl	%ecx, 16(%rbx)
	movl	8(%rbx), %esi
	cmpl	%edx, %esi
	jne	.LBB6_12
# BB#11:                                #   in Loop: Header=BB6_1 Depth=1
	movb	$1, 48(%rbx)
	movl	$0, 8(%rbx)
	xorl	%esi, %esi
.LBB6_12:                               # %_ZN10COutBuffer9FlushPartEv.exit
                                        #   in Loop: Header=BB6_1 Depth=1
	cmpl	%esi, %ecx
	cmoval	%ecx, %edx
	movl	%edx, 12(%rbx)
	movl	%ebp, %edx
	addq	%rdx, 32(%rbx)
	testl	%eax, %eax
	movl	%esi, %ebp
	je	.LBB6_1
	jmp	.LBB6_13
.LBB6_2:
	xorl	%eax, %eax
.LBB6_13:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN10COutBuffer5FlushEv, .Lfunc_end6-_ZN10COutBuffer5FlushEv
	.cfi_endproc

	.globl	_ZN10COutBuffer14FlushWithCheckEv
	.p2align	4, 0x90
	.type	_ZN10COutBuffer14FlushWithCheckEv,@function
_ZN10COutBuffer14FlushWithCheckEv:      # @_ZN10COutBuffer14FlushWithCheckEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 64
.Lcfi36:
	.cfi_offset %rbx, -48
.Lcfi37:
	.cfi_offset %r12, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	8(%rbx), %ebp
	movl	16(%rbx), %eax
	xorl	%r12d, %r12d
	leaq	12(%rsp), %r14
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	cmpl	%ebp, %eax
	je	.LBB7_13
# BB#2:                                 #   in Loop: Header=BB7_1 Depth=1
	jb	.LBB7_4
# BB#3:                                 #   in Loop: Header=BB7_1 Depth=1
	movl	20(%rbx), %ebp
.LBB7_4:                                #   in Loop: Header=BB7_1 Depth=1
	subl	%eax, %ebp
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_6
# BB#5:                                 #   in Loop: Header=BB7_1 Depth=1
	movl	%eax, %esi
	addq	(%rbx), %rsi
	movl	%ebp, %r15d
	movq	%r15, %rdx
	callq	memmove
	addq	%r15, 40(%rbx)
.LBB7_6:                                #   in Loop: Header=BB7_1 Depth=1
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_7
# BB#8:                                 #   in Loop: Header=BB7_1 Depth=1
	movl	$0, 12(%rsp)
	movq	(%rdi), %rax
	movl	16(%rbx), %esi
	addq	(%rbx), %rsi
	movl	%ebp, %edx
	movq	%r14, %rcx
	callq	*40(%rax)
	movl	%eax, %r15d
	movl	12(%rsp), %ebp
	jmp	.LBB7_9
	.p2align	4, 0x90
.LBB7_7:                                #   in Loop: Header=BB7_1 Depth=1
	xorl	%r15d, %r15d
.LBB7_9:                                #   in Loop: Header=BB7_1 Depth=1
	movl	16(%rbx), %eax
	addl	%ebp, %eax
	movl	20(%rbx), %ecx
	cmpl	%ecx, %eax
	cmovel	%r12d, %eax
	movl	%eax, 16(%rbx)
	movl	8(%rbx), %edx
	cmpl	%ecx, %edx
	jne	.LBB7_11
# BB#10:                                #   in Loop: Header=BB7_1 Depth=1
	movb	$1, 48(%rbx)
	movl	$0, 8(%rbx)
	xorl	%edx, %edx
.LBB7_11:                               # %_ZN10COutBuffer9FlushPartEv.exit.i
                                        #   in Loop: Header=BB7_1 Depth=1
	cmpl	%edx, %eax
	cmoval	%eax, %ecx
	movl	%ecx, 12(%rbx)
	movl	%ebp, %ecx
	addq	%rcx, 32(%rbx)
	testl	%r15d, %r15d
	movl	%edx, %ebp
	je	.LBB7_1
# BB#12:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	%r15d, (%rax)
	movl	$_ZTI19COutBufferException, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.LBB7_13:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN10COutBuffer14FlushWithCheckEv, .Lfunc_end7-_ZN10COutBuffer14FlushWithCheckEv
	.cfi_endproc

	.type	_ZTS19COutBufferException,@object # @_ZTS19COutBufferException
	.section	.rodata._ZTS19COutBufferException,"aG",@progbits,_ZTS19COutBufferException,comdat
	.weak	_ZTS19COutBufferException
	.p2align	4
_ZTS19COutBufferException:
	.asciz	"19COutBufferException"
	.size	_ZTS19COutBufferException, 22

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	_ZTI19COutBufferException,@object # @_ZTI19COutBufferException
	.section	.rodata._ZTI19COutBufferException,"aG",@progbits,_ZTI19COutBufferException,comdat
	.weak	_ZTI19COutBufferException
	.p2align	4
_ZTI19COutBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19COutBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI19COutBufferException, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
