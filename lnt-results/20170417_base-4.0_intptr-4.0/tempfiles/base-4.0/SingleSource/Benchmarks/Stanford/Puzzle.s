	.text
	.file	"Puzzle.bc"
	.globl	Initrand
	.p2align	4, 0x90
	.type	Initrand,@function
Initrand:                               # @Initrand
	.cfi_startproc
# BB#0:
	movq	$74755, seed(%rip)      # imm = 0x12403
	retq
.Lfunc_end0:
	.size	Initrand, .Lfunc_end0-Initrand
	.cfi_endproc

	.globl	Rand
	.p2align	4, 0x90
	.type	Rand,@function
Rand:                                   # @Rand
	.cfi_startproc
# BB#0:
	imull	$1309, seed(%rip), %eax # imm = 0x51D
	addl	$13849, %eax            # imm = 0x3619
	movzwl	%ax, %eax
	movq	%rax, seed(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end1:
	.size	Rand, .Lfunc_end1-Rand
	.cfi_endproc

	.globl	Fit
	.p2align	4, 0x90
	.type	Fit,@function
Fit:                                    # @Fit
	.cfi_startproc
# BB#0:
	movslq	%edi, %rdx
	movslq	piecemax(,%rdx,4), %rcx
	testq	%rcx, %rcx
	movl	$1, %eax
	js	.LBB2_6
# BB#1:                                 # %.lr.ph.preheader
	movslq	%esi, %rsi
	shlq	$11, %rdx
	leaq	p(%rdx), %rdx
	leaq	puzzl(,%rsi,4), %rsi
	movq	$-1, %rdi
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 4(%rdx,%rdi,4)
	je	.LBB2_5
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpl	$0, 4(%rsi,%rdi,4)
	jne	.LBB2_4
.LBB2_5:                                #   in Loop: Header=BB2_2 Depth=1
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB2_2
.LBB2_6:                                # %._crit_edge
	retq
.LBB2_4:
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	Fit, .Lfunc_end2-Fit
	.cfi_endproc

	.globl	Place
	.p2align	4, 0x90
	.type	Place,@function
Place:                                  # @Place
	.cfi_startproc
# BB#0:
	movslq	%edi, %r9
	movslq	piecemax(,%r9,4), %rcx
	testq	%rcx, %rcx
	js	.LBB3_13
# BB#1:                                 # %.lr.ph20
	movslq	%esi, %r8
	testb	$1, %cl
	jne	.LBB3_2
# BB#3:
	movq	%r9, %rax
	shlq	$11, %rax
	cmpl	$0, p(%rax)
	je	.LBB3_5
# BB#4:
	movl	$1, puzzl(,%r8,4)
.LBB3_5:                                # %.prol.loopexit
	movl	$1, %eax
	testl	%ecx, %ecx
	jne	.LBB3_7
	jmp	.LBB3_13
.LBB3_2:
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.LBB3_13
.LBB3_7:                                # %.lr.ph20.new
	movq	%r9, %rdi
	shlq	$11, %rdi
	addq	%rax, %r8
	leaq	-1(%rax), %rdx
	leaq	puzzl+4(,%r8,4), %rax
	.p2align	4, 0x90
.LBB3_8:                                # =>This Inner Loop Header: Depth=1
	cmpl	$0, p+4(%rdi,%rdx,4)
	je	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_8 Depth=1
	movl	$1, -4(%rax)
.LBB3_10:                               #   in Loop: Header=BB3_8 Depth=1
	cmpl	$0, p+8(%rdi,%rdx,4)
	je	.LBB3_12
# BB#11:                                #   in Loop: Header=BB3_8 Depth=1
	movl	$1, (%rax)
.LBB3_12:                               #   in Loop: Header=BB3_8 Depth=1
	addq	$2, %rdx
	addq	$8, %rax
	cmpq	%rcx, %rdx
	jl	.LBB3_8
.LBB3_13:                               # %._crit_edge21
	movslq	class(,%r9,4), %rax
	decl	piececount(,%rax,4)
	xorl	%eax, %eax
	cmpl	$511, %esi              # imm = 0x1FF
	jg	.LBB3_18
# BB#14:                                # %.lr.ph.preheader
	movslq	%esi, %rcx
	.p2align	4, 0x90
.LBB3_15:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, puzzl(,%rcx,4)
	je	.LBB3_16
# BB#17:                                #   in Loop: Header=BB3_15 Depth=1
	incq	%rcx
	cmpq	$512, %rcx              # imm = 0x200
	jl	.LBB3_15
.LBB3_18:                               # %._crit_edge
	retq
.LBB3_16:
	movl	%ecx, %eax
	retq
.Lfunc_end3:
	.size	Place, .Lfunc_end3-Place
	.cfi_endproc

	.globl	Remove
	.p2align	4, 0x90
	.type	Remove,@function
Remove:                                 # @Remove
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	movslq	piecemax(,%rax,4), %rcx
	testq	%rcx, %rcx
	js	.LBB4_13
# BB#1:                                 # %.lr.ph
	movslq	%esi, %rdx
	testb	$1, %cl
	jne	.LBB4_2
# BB#3:
	movq	%rax, %rsi
	shlq	$11, %rsi
	cmpl	$0, p(%rsi)
	je	.LBB4_5
# BB#4:
	movl	$0, puzzl(,%rdx,4)
.LBB4_5:                                # %.prol.loopexit
	movl	$1, %edi
	testl	%ecx, %ecx
	jne	.LBB4_7
	jmp	.LBB4_13
.LBB4_2:
	xorl	%edi, %edi
	testl	%ecx, %ecx
	je	.LBB4_13
.LBB4_7:                                # %.lr.ph.new
	movq	%rax, %rsi
	shlq	$11, %rsi
	addq	%rdi, %rdx
	decq	%rdi
	leaq	puzzl+4(,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB4_8:                                # =>This Inner Loop Header: Depth=1
	cmpl	$0, p+4(%rsi,%rdi,4)
	je	.LBB4_10
# BB#9:                                 #   in Loop: Header=BB4_8 Depth=1
	movl	$0, -4(%rdx)
.LBB4_10:                               #   in Loop: Header=BB4_8 Depth=1
	cmpl	$0, p+8(%rsi,%rdi,4)
	je	.LBB4_12
# BB#11:                                #   in Loop: Header=BB4_8 Depth=1
	movl	$0, (%rdx)
.LBB4_12:                               #   in Loop: Header=BB4_8 Depth=1
	addq	$2, %rdi
	addq	$8, %rdx
	cmpq	%rcx, %rdi
	jl	.LBB4_8
.LBB4_13:                               # %._crit_edge
	movslq	class(,%rax,4), %rax
	incl	piececount(,%rax,4)
	retq
.Lfunc_end4:
	.size	Remove, .Lfunc_end4-Remove
	.cfi_endproc

	.globl	Trial
	.p2align	4, 0x90
	.type	Trial,@function
Trial:                                  # @Trial
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	incl	kount(%rip)
	cmpl	$512, %edi              # imm = 0x200
	movslq	%edi, %r14
	jge	.LBB5_1
# BB#10:                                # %.split.us.preheader
	leaq	puzzl(,%r14,4), %r15
	movl	$p, %r12d
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_11:                               # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_14 Depth 2
                                        #     Child Loop BB5_24 Depth 2
                                        #     Child Loop BB5_30 Depth 2
                                        #     Child Loop BB5_43 Depth 2
	movslq	class(,%rbp,4), %rax
	movl	piececount(,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB5_49
# BB#12:                                #   in Loop: Header=BB5_11 Depth=1
	movslq	piecemax(,%rbp,4), %rdx
	testq	%rdx, %rdx
	js	.LBB5_29
# BB#13:                                # %.lr.ph.preheader.i.us
                                        #   in Loop: Header=BB5_11 Depth=1
	movq	$-1, %rsi
	movq	%r12, %rdi
	.p2align	4, 0x90
.LBB5_14:                               # %.lr.ph.i.us
                                        #   Parent Loop BB5_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, (%rdi)
	je	.LBB5_16
# BB#15:                                #   in Loop: Header=BB5_14 Depth=2
	cmpl	$0, 4(%r15,%rsi,4)
	jne	.LBB5_49
.LBB5_16:                               #   in Loop: Header=BB5_14 Depth=2
	incq	%rsi
	addq	$4, %rdi
	cmpq	%rdx, %rsi
	jl	.LBB5_14
# BB#17:                                # %.lr.ph20.i.us.preheader
                                        #   in Loop: Header=BB5_11 Depth=1
	testb	$1, %dl
	jne	.LBB5_18
# BB#19:                                # %.lr.ph20.i.us.prol
                                        #   in Loop: Header=BB5_11 Depth=1
	movq	%rbp, %rsi
	shlq	$11, %rsi
	cmpl	$0, p(%rsi)
	je	.LBB5_21
# BB#20:                                #   in Loop: Header=BB5_11 Depth=1
	movl	$1, puzzl(,%r14,4)
.LBB5_21:                               # %.lr.ph20.i.us.prol.loopexit
                                        #   in Loop: Header=BB5_11 Depth=1
	movl	$1, %edi
	testl	%edx, %edx
	jne	.LBB5_23
	jmp	.LBB5_29
.LBB5_18:                               #   in Loop: Header=BB5_11 Depth=1
	xorl	%edi, %edi
	testl	%edx, %edx
	je	.LBB5_29
.LBB5_23:                               # %.lr.ph20.i.us.preheader.new
                                        #   in Loop: Header=BB5_11 Depth=1
	incq	%rdx
	subq	%rdi, %rdx
	leaq	4(%r15,%rdi,4), %rsi
	leaq	(%r13,%rdi,4), %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_24:                               # %.lr.ph20.i.us
                                        #   Parent Loop BB5_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, p(%rdi,%rbx,4)
	je	.LBB5_26
# BB#25:                                #   in Loop: Header=BB5_24 Depth=2
	movl	$1, -4(%rsi,%rbx,4)
.LBB5_26:                               # %.lr.ph20.i.us.161
                                        #   in Loop: Header=BB5_24 Depth=2
	cmpl	$0, p+4(%rdi,%rbx,4)
	je	.LBB5_28
# BB#27:                                #   in Loop: Header=BB5_24 Depth=2
	movl	$1, (%rsi,%rbx,4)
.LBB5_28:                               #   in Loop: Header=BB5_24 Depth=2
	addq	$2, %rbx
	cmpq	%rbx, %rdx
	jne	.LBB5_24
.LBB5_29:                               # %._crit_edge21.i.us
                                        #   in Loop: Header=BB5_11 Depth=1
	decl	%ecx
	movl	%ecx, piececount(,%rax,4)
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB5_30:                               # %.lr.ph.i19.us
                                        #   Parent Loop BB5_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, puzzl(,%rbx,4)
	je	.LBB5_33
# BB#31:                                #   in Loop: Header=BB5_30 Depth=2
	incq	%rbx
	cmpq	$512, %rbx              # imm = 0x200
	jl	.LBB5_30
# BB#32:                                #   in Loop: Header=BB5_11 Depth=1
	xorl	%ebx, %ebx
.LBB5_33:                               # %Place.exit.loopexit.us
                                        #   in Loop: Header=BB5_11 Depth=1
	movl	%ebx, %edi
	callq	Trial
	movl	%eax, %ecx
	testl	%ebx, %ebx
	movl	$1, %eax
	je	.LBB5_63
# BB#34:                                # %Place.exit.loopexit.us
                                        #   in Loop: Header=BB5_11 Depth=1
	testl	%ecx, %ecx
	jne	.LBB5_63
# BB#35:                                #   in Loop: Header=BB5_11 Depth=1
	movslq	piecemax(,%rbp,4), %rax
	testq	%rax, %rax
	js	.LBB5_48
# BB#36:                                # %.lr.ph.i21.us
                                        #   in Loop: Header=BB5_11 Depth=1
	testb	$1, %al
	jne	.LBB5_37
# BB#38:                                #   in Loop: Header=BB5_11 Depth=1
	movq	%rbp, %rcx
	shlq	$11, %rcx
	cmpl	$0, p(%rcx)
	je	.LBB5_40
# BB#39:                                #   in Loop: Header=BB5_11 Depth=1
	movl	$0, puzzl(,%r14,4)
.LBB5_40:                               # %.prol.loopexit
                                        #   in Loop: Header=BB5_11 Depth=1
	movl	$1, %ecx
	testl	%eax, %eax
	jne	.LBB5_42
	jmp	.LBB5_48
.LBB5_37:                               #   in Loop: Header=BB5_11 Depth=1
	xorl	%ecx, %ecx
	testl	%eax, %eax
	je	.LBB5_48
.LBB5_42:                               # %.lr.ph.i21.us.new
                                        #   in Loop: Header=BB5_11 Depth=1
	leaq	(%r14,%rcx), %rdx
	decq	%rcx
	leaq	puzzl+4(,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB5_43:                               #   Parent Loop BB5_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, p+4(%r13,%rcx,4)
	je	.LBB5_45
# BB#44:                                #   in Loop: Header=BB5_43 Depth=2
	movl	$0, -4(%rdx)
.LBB5_45:                               #   in Loop: Header=BB5_43 Depth=2
	cmpl	$0, p+8(%r13,%rcx,4)
	je	.LBB5_47
# BB#46:                                #   in Loop: Header=BB5_43 Depth=2
	movl	$0, (%rdx)
.LBB5_47:                               #   in Loop: Header=BB5_43 Depth=2
	addq	$2, %rcx
	addq	$8, %rdx
	cmpq	%rcx, %rax
	jne	.LBB5_43
.LBB5_48:                               # %Remove.exit.us
                                        #   in Loop: Header=BB5_11 Depth=1
	movslq	class(,%rbp,4), %rax
	incl	piececount(,%rax,4)
.LBB5_49:                               # %Fit.exit.us
                                        #   in Loop: Header=BB5_11 Depth=1
	incq	%rbp
	addq	$2048, %r12             # imm = 0x800
	addq	$2048, %r13             # imm = 0x800
	cmpq	$13, %rbp
	jl	.LBB5_11
.LBB5_62:
	xorl	%eax, %eax
	jmp	.LBB5_63
.LBB5_1:                                # %.split.preheader
	leaq	puzzl(,%r14,4), %rbp
	xorl	%edx, %edx
	xorl	%edi, %edi
.LBB5_2:                                # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_5 Depth 2
	movslq	class(,%rdi,4), %rax
	movl	piececount(,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB5_61
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movslq	piecemax(,%rdi,4), %rsi
	testq	%rsi, %rsi
	js	.LBB5_60
# BB#4:                                 # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	$-1, %rbx
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph.i
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, p+4(%rdx,%rbx,4)
	je	.LBB5_7
# BB#6:                                 #   in Loop: Header=BB5_5 Depth=2
	cmpl	$0, 4(%rbp,%rbx,4)
	jne	.LBB5_61
.LBB5_7:                                #   in Loop: Header=BB5_5 Depth=2
	incq	%rbx
	cmpq	%rsi, %rbx
	jl	.LBB5_5
	jmp	.LBB5_8
	.p2align	4, 0x90
.LBB5_61:                               # %Fit.exit
                                        #   in Loop: Header=BB5_2 Depth=1
	incq	%rdi
	addq	$2048, %rdx             # imm = 0x800
	cmpq	$13, %rdi
	jl	.LBB5_2
	jmp	.LBB5_62
.LBB5_8:                                # %.lr.ph20.i.preheader
	testb	$1, %sil
	jne	.LBB5_9
# BB#50:                                # %.lr.ph20.i.prol
	shlq	$11, %rdi
	cmpl	$0, p(%rdi)
	je	.LBB5_52
# BB#51:
	movl	$1, puzzl(,%r14,4)
.LBB5_52:                               # %.lr.ph20.i.prol.loopexit
	movl	$1, %ebx
	testl	%esi, %esi
	jne	.LBB5_54
	jmp	.LBB5_60
.LBB5_9:
	xorl	%ebx, %ebx
	testl	%esi, %esi
	je	.LBB5_60
.LBB5_54:                               # %.lr.ph20.i.preheader.new
	addq	%rbx, %r14
	leaq	puzzl+4(,%r14,4), %rdi
	movl	$1, %ebp
	subq	%rbx, %rbp
	addq	%rsi, %rbp
	leaq	p+4(%rdx,%rbx,4), %rdx
	.p2align	4, 0x90
.LBB5_55:                               # %.lr.ph20.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, -4(%rdx)
	je	.LBB5_57
# BB#56:                                #   in Loop: Header=BB5_55 Depth=1
	movl	$1, -4(%rdi)
.LBB5_57:                               # %.lr.ph20.i.166
                                        #   in Loop: Header=BB5_55 Depth=1
	cmpl	$0, (%rdx)
	je	.LBB5_59
# BB#58:                                #   in Loop: Header=BB5_55 Depth=1
	movl	$1, (%rdi)
.LBB5_59:                               #   in Loop: Header=BB5_55 Depth=1
	addq	$8, %rdi
	addq	$8, %rdx
	addq	$-2, %rbp
	jne	.LBB5_55
.LBB5_60:                               # %._crit_edge21.i
	decl	%ecx
	movl	%ecx, piececount(,%rax,4)
	xorl	%edi, %edi
	callq	Trial
	movl	$1, %eax
.LBB5_63:                               # %.us-lcssa.us
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	Trial, .Lfunc_end5-Trial
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI6_1:
	.long	11                      # 0xb
	.long	193                     # 0xc1
	.long	88                      # 0x58
	.long	25                      # 0x19
.LCPI6_2:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI6_3:
	.long	67                      # 0x43
	.long	200                     # 0xc8
	.long	2                       # 0x2
	.long	16                      # 0x10
.LCPI6_4:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
.LCPI6_5:
	.long	128                     # 0x80
	.long	9                       # 0x9
	.long	65                      # 0x41
	.long	72                      # 0x48
.LCPI6_6:
	.long	13                      # 0xd
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI6_7:
	.zero	16
	.text
	.globl	Puzzle
	.p2align	4, 0x90
	.type	Puzzle,@function
Puzzle:                                 # @Puzzle
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movq	$-2048, %rax            # imm = 0xF800
	movaps	.LCPI6_0(%rip), %xmm0   # xmm0 = [1,1,1,1]
	.p2align	4, 0x90
.LBB6_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, puzzl+2048(%rax)
	movaps	%xmm0, puzzl+2064(%rax)
	movaps	%xmm0, puzzl+2080(%rax)
	movaps	%xmm0, puzzl+2096(%rax)
	movaps	%xmm0, puzzl+2112(%rax)
	movaps	%xmm0, puzzl+2128(%rax)
	movaps	%xmm0, puzzl+2144(%rax)
	movaps	%xmm0, puzzl+2160(%rax)
	movaps	%xmm0, puzzl+2176(%rax)
	movaps	%xmm0, puzzl+2192(%rax)
	movaps	%xmm0, puzzl+2208(%rax)
	movaps	%xmm0, puzzl+2224(%rax)
	movaps	%xmm0, puzzl+2240(%rax)
	movaps	%xmm0, puzzl+2256(%rax)
	movaps	%xmm0, puzzl+2272(%rax)
	movaps	%xmm0, puzzl+2288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB6_1
# BB#2:                                 # %.lr.ph.i
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	xorps	%xmm1, %xmm1
	movups	%xmm1, puzzl+292(%rip)
	movl	$0, puzzl+308(%rip)
	movups	%xmm1, puzzl+324(%rip)
	movl	$0, puzzl+340(%rip)
	movups	%xmm1, puzzl+356(%rip)
	movl	$0, puzzl+372(%rip)
	movups	%xmm1, puzzl+388(%rip)
	movl	$0, puzzl+404(%rip)
	movups	%xmm1, puzzl+420(%rip)
	movl	$0, puzzl+436(%rip)
	movups	%xmm1, puzzl+548(%rip)
	movl	$0, puzzl+564(%rip)
	movups	%xmm1, puzzl+580(%rip)
	movl	$0, puzzl+596(%rip)
	movups	%xmm1, puzzl+612(%rip)
	movl	$0, puzzl+628(%rip)
	movups	%xmm1, puzzl+644(%rip)
	movl	$0, puzzl+660(%rip)
	movups	%xmm1, puzzl+676(%rip)
	movl	$0, puzzl+692(%rip)
	movups	%xmm1, puzzl+804(%rip)
	movl	$0, puzzl+820(%rip)
	movups	%xmm1, puzzl+836(%rip)
	movl	$0, puzzl+852(%rip)
	movups	%xmm1, puzzl+868(%rip)
	movl	$0, puzzl+884(%rip)
	movups	%xmm1, puzzl+900(%rip)
	movl	$0, puzzl+916(%rip)
	movups	%xmm1, puzzl+932(%rip)
	movl	$0, puzzl+948(%rip)
	movups	%xmm1, puzzl+1060(%rip)
	movl	$0, puzzl+1076(%rip)
	movups	%xmm1, puzzl+1092(%rip)
	movl	$0, puzzl+1108(%rip)
	movups	%xmm1, puzzl+1124(%rip)
	movl	$0, puzzl+1140(%rip)
	movups	%xmm1, puzzl+1156(%rip)
	movl	$0, puzzl+1172(%rip)
	movups	%xmm1, puzzl+1188(%rip)
	movl	$0, puzzl+1204(%rip)
	movups	%xmm1, puzzl+1316(%rip)
	movl	$0, puzzl+1332(%rip)
	movups	%xmm1, puzzl+1348(%rip)
	movl	$0, puzzl+1364(%rip)
	movups	%xmm1, puzzl+1380(%rip)
	movl	$0, puzzl+1396(%rip)
	movups	%xmm1, puzzl+1412(%rip)
	movl	$0, puzzl+1428(%rip)
	movups	%xmm1, puzzl+1444(%rip)
	movl	$0, puzzl+1460(%rip)
	movl	$p+16, %edi
	xorl	%esi, %esi
	movl	$26608, %edx            # imm = 0x67F0
	callq	memset
	movaps	.LCPI6_0(%rip), %xmm0   # xmm0 = [1,1,1,1]
	movaps	%xmm0, p(%rip)
	movaps	%xmm0, p+32(%rip)
	movaps	%xmm0, %xmm1
	movl	$1, p+2048(%rip)
	movl	$1, p+2304(%rip)
	movl	$1, p+2560(%rip)
	movl	$1, p+2816(%rip)
	movl	$1, p+2052(%rip)
	movl	$1, p+2308(%rip)
	movl	$1, p+2564(%rip)
	movl	$1, p+2820(%rip)
	movl	$1, p+4096(%rip)
	movl	$1, p+4352(%rip)
	movl	$1, p+4128(%rip)
	movl	$1, p+4384(%rip)
	movl	$1, p+4160(%rip)
	movl	$1, p+4416(%rip)
	movl	$1, p+4192(%rip)
	movl	$1, p+4448(%rip)
	movl	$1, p+6144(%rip)
	movl	$1, p+6176(%rip)
	movl	$1, p+6208(%rip)
	movl	$1, p+6240(%rip)
	movl	$1, p+6148(%rip)
	movl	$1, p+6180(%rip)
	movl	$1, p+6212(%rip)
	movl	$1, p+6244(%rip)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, class(%rip)
	movaps	.LCPI6_1(%rip), %xmm0   # xmm0 = [11,193,88,25]
	movaps	%xmm0, piecemax(%rip)
	movaps	%xmm1, p+8192(%rip)
	movaps	%xmm1, p+8448(%rip)
	movl	$1, p+10240(%rip)
	movl	$1, p+10496(%rip)
	movl	$1, p+10752(%rip)
	movl	$1, p+11008(%rip)
	movl	$1, p+10272(%rip)
	movl	$1, p+10528(%rip)
	movl	$1, p+10784(%rip)
	movl	$1, p+11040(%rip)
	movl	$1, p+12288(%rip)
	movl	$1, p+12292(%rip)
	movl	$1, p+12296(%rip)
	movl	$1, p+14336(%rip)
	movl	$1, p+14368(%rip)
	movl	$1, p+14400(%rip)
	movaps	.LCPI6_2(%rip), %xmm0   # xmm0 = [0,0,1,1]
	movaps	%xmm0, class+16(%rip)
	movaps	.LCPI6_3(%rip), %xmm0   # xmm0 = [67,200,2,16]
	movaps	%xmm0, piecemax+16(%rip)
	movl	$1, p+16384(%rip)
	movl	$1, p+16640(%rip)
	movl	$1, p+16896(%rip)
	movl	$1, p+18432(%rip)
	movl	$1, p+18464(%rip)
	movl	$1, p+18436(%rip)
	movl	$1, p+18468(%rip)
	movl	$1, p+20480(%rip)
	movl	$1, p+20736(%rip)
	movl	$1, p+20484(%rip)
	movl	$1, p+20740(%rip)
	movl	$1, p+22528(%rip)
	movl	$1, p+22784(%rip)
	movl	$1, p+22560(%rip)
	movl	$1, p+22816(%rip)
	movaps	.LCPI6_4(%rip), %xmm0   # xmm0 = [1,2,2,2]
	movaps	%xmm0, class+32(%rip)
	movaps	.LCPI6_5(%rip), %xmm0   # xmm0 = [128,9,65,72]
	movaps	%xmm0, piecemax+32(%rip)
	movl	$1, p+24576(%rip)
	movl	$1, p+24832(%rip)
	movl	$1, p+24608(%rip)
	movl	$1, p+24864(%rip)
	movl	$1, p+24580(%rip)
	movl	$1, p+24836(%rip)
	movl	$1, p+24612(%rip)
	movl	$1, p+24868(%rip)
	movl	$3, class+48(%rip)
	movl	$73, piecemax+48(%rip)
	movaps	.LCPI6_6(%rip), %xmm0   # xmm0 = [13,3,1,1]
	movaps	%xmm0, piececount(%rip)
	movl	$0, kount(%rip)
	cmpl	$0, p(%rip)
	je	.LBB6_4
# BB#3:
	cmpl	$0, puzzl+292(%rip)
	jne	.LBB6_45
.LBB6_4:                                # %.lr.ph.i.1324
	cmpl	$0, p+4(%rip)
	je	.LBB6_6
# BB#5:
	cmpl	$0, puzzl+296(%rip)
	jne	.LBB6_45
.LBB6_6:                                # %.lr.ph.i.2325
	cmpl	$0, p+8(%rip)
	je	.LBB6_8
# BB#7:
	cmpl	$0, puzzl+300(%rip)
	jne	.LBB6_45
.LBB6_8:                                # %.lr.ph.i.3326
	cmpl	$0, p+12(%rip)
	je	.LBB6_10
# BB#9:
	cmpl	$0, puzzl+304(%rip)
	jne	.LBB6_45
.LBB6_10:                               # %.lr.ph.i.4327
	cmpl	$0, p+16(%rip)
	je	.LBB6_12
# BB#11:
	cmpl	$0, puzzl+308(%rip)
	jne	.LBB6_45
.LBB6_12:                               # %.lr.ph.i.5328
	cmpl	$0, p+20(%rip)
	je	.LBB6_14
# BB#13:
	cmpl	$0, puzzl+312(%rip)
	jne	.LBB6_45
.LBB6_14:                               # %.lr.ph.i.6329
	cmpl	$0, p+24(%rip)
	je	.LBB6_16
# BB#15:
	cmpl	$0, puzzl+316(%rip)
	jne	.LBB6_45
.LBB6_16:                               # %.lr.ph.i.7330
	cmpl	$0, p+28(%rip)
	je	.LBB6_18
# BB#17:
	cmpl	$0, puzzl+320(%rip)
	jne	.LBB6_45
.LBB6_18:                               # %.lr.ph.i.8331
	cmpl	$0, p+32(%rip)
	je	.LBB6_20
# BB#19:
	cmpl	$0, puzzl+324(%rip)
	jne	.LBB6_45
.LBB6_20:                               # %.lr.ph.i.9332
	cmpl	$0, p+36(%rip)
	je	.LBB6_22
# BB#21:
	cmpl	$0, puzzl+328(%rip)
	jne	.LBB6_45
.LBB6_22:                               # %.lr.ph.i.10333
	cmpl	$0, p+40(%rip)
	je	.LBB6_24
# BB#23:
	cmpl	$0, puzzl+332(%rip)
	jne	.LBB6_45
.LBB6_24:                               # %.lr.ph.i.11334
	cmpl	$0, p+44(%rip)
	je	.LBB6_26
# BB#25:
	cmpl	$0, puzzl+336(%rip)
	je	.LBB6_26
.LBB6_45:                               # %Fit.exit
	movl	$.Lstr, %edi
	callq	puts
	movl	n(%rip), %edi
.LBB6_46:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	Trial
	testl	%eax, %eax
	je	.LBB6_47
# BB#48:
	cmpl	$2005, kount(%rip)      # imm = 0x7D5
	je	.LBB6_51
# BB#49:
	movl	$.Lstr.2, %edi
	jmp	.LBB6_50
.LBB6_47:
	movl	$.Lstr.1, %edi
.LBB6_50:
	callq	puts
.LBB6_51:
	movl	n(%rip), %esi
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	movl	kount(%rip), %esi
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	popq	%rcx
	jmp	printf                  # TAILCALL
.LBB6_26:
	movslq	piecemax(%rip), %rax
	testq	%rax, %rax
	js	.LBB6_39
# BB#27:                                # %.lr.ph20.i
	testb	$1, %al
	jne	.LBB6_28
# BB#29:
	cmpl	$0, p(%rip)
	je	.LBB6_31
# BB#30:
	movl	$1, puzzl+292(%rip)
.LBB6_31:                               # %.prol.loopexit
	movl	$1, %ecx
	testl	%eax, %eax
	jne	.LBB6_33
	jmp	.LBB6_39
.LBB6_28:
	xorl	%ecx, %ecx
	testl	%eax, %eax
	je	.LBB6_39
.LBB6_33:                               # %.lr.ph20.i.new
	incq	%rax
.LBB6_34:                               # =>This Inner Loop Header: Depth=1
	cmpl	$0, p(,%rcx,4)
	je	.LBB6_36
# BB#35:                                #   in Loop: Header=BB6_34 Depth=1
	movl	$1, puzzl+292(,%rcx,4)
.LBB6_36:                               #   in Loop: Header=BB6_34 Depth=1
	cmpl	$0, p+4(,%rcx,4)
	je	.LBB6_38
# BB#37:                                #   in Loop: Header=BB6_34 Depth=1
	movl	$1, puzzl+296(,%rcx,4)
.LBB6_38:                               #   in Loop: Header=BB6_34 Depth=1
	addq	$2, %rcx
	cmpq	%rcx, %rax
	jne	.LBB6_34
.LBB6_39:                               # %._crit_edge21.i
	movslq	class(%rip), %rax
	decl	piececount(,%rax,4)
	movl	$73, %edi
	xorl	%eax, %eax
	cmpl	$0, puzzl+292(,%rax,4)
	jne	.LBB6_41
	jmp	.LBB6_44
.LBB6_57:                               #   in Loop: Header=BB6_41 Depth=1
	addq	$4, %rdi
	addq	$4, %rax
	cmpl	$0, puzzl+292(,%rax,4)
	je	.LBB6_44
.LBB6_41:                               # %.lr.ph.i177.1322
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, puzzl+296(,%rax,4)
	je	.LBB6_42
# BB#52:                                # %.lr.ph.i177.2323
                                        #   in Loop: Header=BB6_41 Depth=1
	cmpl	$0, puzzl+300(,%rax,4)
	je	.LBB6_53
# BB#54:                                #   in Loop: Header=BB6_41 Depth=1
	leaq	76(%rax), %rcx
	cmpq	$511, %rcx              # imm = 0x1FF
	jg	.LBB6_55
# BB#56:                                # %.lr.ph.i177.3
                                        #   in Loop: Header=BB6_41 Depth=1
	cmpl	$0, puzzl+304(,%rax,4)
	jne	.LBB6_57
# BB#43:                                # %.lr.ph.i177.3.Place.exit_crit_edge
	addl	$76, %eax
	movl	%eax, %edi
.LBB6_44:                               # %Place.exit
	movl	%edi, n(%rip)
	jmp	.LBB6_46
.LBB6_42:
	incl	%edi
	movl	%edi, n(%rip)
	jmp	.LBB6_46
.LBB6_53:
	addl	$2, %edi
	movl	%edi, n(%rip)
	jmp	.LBB6_46
.LBB6_55:
	xorl	%edi, %edi
	movl	%edi, n(%rip)
	jmp	.LBB6_46
.Lfunc_end6:
	.size	Puzzle, .Lfunc_end6-Puzzle
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
.Lcfi15:
	.cfi_offset %rbx, -16
	movl	$100, %ebx
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	callq	Puzzle
	decl	%ebx
	jne	.LBB7_1
# BB#2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	main, .Lfunc_end7-main
	.cfi_endproc

	.type	seed,@object            # @seed
	.comm	seed,8,8
	.type	piecemax,@object        # @piecemax
	.comm	piecemax,52,16
	.type	p,@object               # @p
	.comm	p,26624,16
	.type	puzzl,@object           # @puzzl
	.comm	puzzl,2048,16
	.type	piececount,@object      # @piececount
	.comm	piececount,16,16
	.type	class,@object           # @class
	.comm	class,52,16
	.type	kount,@object           # @kount
	.comm	kount,4,4
	.type	n,@object               # @n
	.comm	n,4,4
	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"%d\n"
	.size	.L.str.3, 4

	.type	value,@object           # @value
	.comm	value,4,4
	.type	fixed,@object           # @fixed
	.comm	fixed,4,4
	.type	floated,@object         # @floated
	.comm	floated,4,4
	.type	permarray,@object       # @permarray
	.comm	permarray,44,16
	.type	pctr,@object            # @pctr
	.comm	pctr,4,4
	.type	tree,@object            # @tree
	.comm	tree,8,8
	.type	stack,@object           # @stack
	.comm	stack,16,16
	.type	cellspace,@object       # @cellspace
	.comm	cellspace,152,16
	.type	freelist,@object        # @freelist
	.comm	freelist,4,4
	.type	movesdone,@object       # @movesdone
	.comm	movesdone,4,4
	.type	ima,@object             # @ima
	.comm	ima,6724,16
	.type	imb,@object             # @imb
	.comm	imb,6724,16
	.type	imr,@object             # @imr
	.comm	imr,6724,16
	.type	rma,@object             # @rma
	.comm	rma,6724,16
	.type	rmb,@object             # @rmb
	.comm	rmb,6724,16
	.type	rmr,@object             # @rmr
	.comm	rmr,6724,16
	.type	sortlist,@object        # @sortlist
	.comm	sortlist,20004,16
	.type	biggest,@object         # @biggest
	.comm	biggest,4,4
	.type	littlest,@object        # @littlest
	.comm	littlest,4,4
	.type	top,@object             # @top
	.comm	top,4,4
	.type	z,@object               # @z
	.comm	z,2056,16
	.type	w,@object               # @w
	.comm	w,2056,16
	.type	e,@object               # @e
	.comm	e,1040,16
	.type	zr,@object              # @zr
	.comm	zr,4,4
	.type	zi,@object              # @zi
	.comm	zi,4,4
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Error1 in Puzzle"
	.size	.Lstr, 17

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"Error2 in Puzzle."
	.size	.Lstr.1, 18

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"Error3 in Puzzle."
	.size	.Lstr.2, 18


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
