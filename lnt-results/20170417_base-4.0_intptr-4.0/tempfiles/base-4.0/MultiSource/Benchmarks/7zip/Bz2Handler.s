	.text
	.file	"Bz2Handler.bc"
	.globl	_ZN8NArchive4NBz28CHandler21GetNumberOfPropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler21GetNumberOfPropertiesEPj,@function
_ZN8NArchive4NBz28CHandler21GetNumberOfPropertiesEPj: # @_ZN8NArchive4NBz28CHandler21GetNumberOfPropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	_ZN8NArchive4NBz28CHandler21GetNumberOfPropertiesEPj, .Lfunc_end0-_ZN8NArchive4NBz28CHandler21GetNumberOfPropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive4NBz28CHandler15GetPropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler15GetPropertyInfoEjPPwPjPt,@function
_ZN8NArchive4NBz28CHandler15GetPropertyInfoEjPPwPjPt: # @_ZN8NArchive4NBz28CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	testl	%esi, %esi
	jne	.LBB1_2
# BB#1:
	movl	_ZN8NArchive4NBz26kPropsE+8(%rip), %eax
	movl	%eax, (%rcx)
	movzwl	_ZN8NArchive4NBz26kPropsE+12(%rip), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB1_2:
	retq
.Lfunc_end1:
	.size	_ZN8NArchive4NBz28CHandler15GetPropertyInfoEjPPwPjPt, .Lfunc_end1-_ZN8NArchive4NBz28CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive4NBz28CHandler28GetNumberOfArchivePropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler28GetNumberOfArchivePropertiesEPj,@function
_ZN8NArchive4NBz28CHandler28GetNumberOfArchivePropertiesEPj: # @_ZN8NArchive4NBz28CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$0, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	_ZN8NArchive4NBz28CHandler28GetNumberOfArchivePropertiesEPj, .Lfunc_end2-_ZN8NArchive4NBz28CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive4NBz28CHandler22GetArchivePropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler22GetArchivePropertyInfoEjPPwPjPt,@function
_ZN8NArchive4NBz28CHandler22GetArchivePropertyInfoEjPPwPjPt: # @_ZN8NArchive4NBz28CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147467263, %eax      # imm = 0x80004001
	retq
.Lfunc_end3:
	.size	_ZN8NArchive4NBz28CHandler22GetArchivePropertyInfoEjPPwPjPt, .Lfunc_end3-_ZN8NArchive4NBz28CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive4NBz28CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler18GetArchivePropertyEjP14tagPROPVARIANT,@function
_ZN8NArchive4NBz28CHandler18GetArchivePropertyEjP14tagPROPVARIANT: # @_ZN8NArchive4NBz28CHandler18GetArchivePropertyEjP14tagPROPVARIANT
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdx, %rbx
	movl	$0, (%rsp)
	cmpl	$44, %esi
	jne	.LBB4_3
# BB#1:
	cmpb	$0, 72(%rdi)
	je	.LBB4_3
# BB#2:
	movq	56(%rdi), %rsi
.Ltmp0:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp1:
.LBB4_3:
.Ltmp2:
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp3:
# BB#4:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB4_5:
.Ltmp4:
	movq	%rax, %rbx
.Ltmp5:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp6:
# BB#6:                                 # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_7:
.Ltmp7:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN8NArchive4NBz28CHandler18GetArchivePropertyEjP14tagPROPVARIANT, .Lfunc_end4-_ZN8NArchive4NBz28CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp5-.Ltmp3           #   Call between .Ltmp3 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	1                       #   On action: 1
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end4-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end5:
	.size	__clang_call_terminate, .Lfunc_end5-__clang_call_terminate

	.text
	.globl	_ZN8NArchive4NBz28CHandler16GetNumberOfItemsEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler16GetNumberOfItemsEPj,@function
_ZN8NArchive4NBz28CHandler16GetNumberOfItemsEPj: # @_ZN8NArchive4NBz28CHandler16GetNumberOfItemsEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end6:
	.size	_ZN8NArchive4NBz28CHandler16GetNumberOfItemsEPj, .Lfunc_end6-_ZN8NArchive4NBz28CHandler16GetNumberOfItemsEPj
	.cfi_endproc

	.globl	_ZN8NArchive4NBz28CHandler11GetPropertyEjjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler11GetPropertyEjjP14tagPROPVARIANT,@function
_ZN8NArchive4NBz28CHandler11GetPropertyEjjP14tagPROPVARIANT: # @_ZN8NArchive4NBz28CHandler11GetPropertyEjjP14tagPROPVARIANT
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rcx, %rbx
	movl	$0, (%rsp)
	cmpl	$8, %edx
	jne	.LBB7_3
# BB#1:
	cmpb	$0, 72(%rdi)
	je	.LBB7_3
# BB#2:
	movq	56(%rdi), %rsi
.Ltmp8:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp9:
.LBB7_3:
.Ltmp10:
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp11:
# BB#4:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB7_5:
.Ltmp12:
	movq	%rax, %rbx
.Ltmp13:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp14:
# BB#6:                                 # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB7_7:
.Ltmp15:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN8NArchive4NBz28CHandler11GetPropertyEjjP14tagPROPVARIANT, .Lfunc_end7-_ZN8NArchive4NBz28CHandler11GetPropertyEjjP14tagPROPVARIANT
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp11-.Ltmp8          #   Call between .Ltmp8 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin1   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp11         #   Call between .Ltmp11 and .Ltmp13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin1   #     jumps to .Ltmp15
	.byte	1                       #   On action: 1
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end7-.Ltmp14     #   Call between .Ltmp14 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NBz28CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback,@function
_ZN8NArchive4NBz28CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback: # @_ZN8NArchive4NBz28CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -32
.Lcfi11:
	.cfi_offset %r14, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
.Ltmp16:
	callq	*48(%rax)
.Ltmp17:
# BB#1:
	movq	(%r14), %rax
	leaq	64(%rbx), %rcx
.Ltmp18:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp19:
# BB#2:
	testl	%ebp, %ebp
	jne	.LBB8_20
# BB#3:
.Ltmp21:
	leaq	5(%rsp), %rsi
	movl	$3, %edx
	movq	%r14, %rdi
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	movl	%eax, %ebp
.Ltmp22:
# BB#4:
	movl	$1, %eax
	testl	%ebp, %ebp
	jne	.LBB8_19
# BB#5:
	cmpb	$66, 5(%rsp)
	jne	.LBB8_18
# BB#6:
	cmpb	$90, 6(%rsp)
	jne	.LBB8_18
# BB#7:
	cmpb	$104, 7(%rsp)
	movl	$1, %ebp
	jne	.LBB8_19
# BB#8:
	movq	(%r14), %rax
.Ltmp24:
	leaq	8(%rsp), %rcx
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp25:
# BB#9:
	movl	$1, %eax
	testl	%ebp, %ebp
	jne	.LBB8_19
# BB#10:
	movq	8(%rsp), %rax
	subq	64(%rbx), %rax
	movq	%rax, 56(%rbx)
	movb	$1, 72(%rbx)
	movq	(%r14), %rax
.Ltmp26:
	movq	%r14, %rdi
	callq	*8(%rax)
.Ltmp27:
# BB#11:                                # %.noexc
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_13
# BB#12:
	movq	(%rdi), %rax
.Ltmp28:
	callq	*16(%rax)
.Ltmp29:
.LBB8_13:
	movq	%r14, 40(%rbx)
	movq	(%r14), %rax
.Ltmp30:
	movq	%r14, %rdi
	callq	*8(%rax)
.Ltmp31:
# BB#14:                                # %.noexc46
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_16
# BB#15:
	movq	(%rdi), %rax
.Ltmp32:
	callq	*16(%rax)
.Ltmp33:
.LBB8_16:                               # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
	movq	%r14, 48(%rbx)
	xorl	%eax, %eax
	jmp	.LBB8_19
.LBB8_18:
	movl	$1, %ebp
.LBB8_19:
	testl	%eax, %eax
	cmovel	%eax, %ebp
.LBB8_20:
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB8_21:
.Ltmp34:
	jmp	.LBB8_24
.LBB8_22:
.Ltmp23:
	jmp	.LBB8_24
.LBB8_23:
.Ltmp20:
.LBB8_24:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movl	$1, %ebp
.Ltmp35:
	callq	__cxa_end_catch
.Ltmp36:
	jmp	.LBB8_20
.LBB8_25:
.Ltmp37:
	movq	%rdx, %rbp
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %ebp
	je	.LBB8_27
# BB#26:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB8_20
.LBB8_27:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp38:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp39:
# BB#28:
.LBB8_29:
.Ltmp40:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN8NArchive4NBz28CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback, .Lfunc_end8-_ZN8NArchive4NBz28CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\366\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp19-.Ltmp16         #   Call between .Ltmp16 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	1                       #   On action: 1
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin2   #     jumps to .Ltmp23
	.byte	1                       #   On action: 1
	.long	.Ltmp24-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp33-.Ltmp24         #   Call between .Ltmp24 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin2   #     jumps to .Ltmp34
	.byte	1                       #   On action: 1
	.long	.Ltmp33-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp35-.Ltmp33         #   Call between .Ltmp33 and .Ltmp35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin2   #     jumps to .Ltmp37
	.byte	3                       #   On action: 2
	.long	.Ltmp36-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp38-.Ltmp36         #   Call between .Ltmp36 and .Ltmp38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin2   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Lfunc_end8-.Ltmp39     #   Call between .Ltmp39 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NBz28CHandler7OpenSeqEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler7OpenSeqEP19ISequentialInStream,@function
_ZN8NArchive4NBz28CHandler7OpenSeqEP19ISequentialInStream: # @_ZN8NArchive4NBz28CHandler7OpenSeqEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*48(%rax)
	testq	%r14, %r14
	je	.LBB9_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB9_2:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB9_4:                                # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
	movq	%r14, 48(%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	_ZN8NArchive4NBz28CHandler7OpenSeqEP19ISequentialInStream, .Lfunc_end9-_ZN8NArchive4NBz28CHandler7OpenSeqEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZThn8_N8NArchive4NBz28CHandler7OpenSeqEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NBz28CHandler7OpenSeqEP19ISequentialInStream,@function
_ZThn8_N8NArchive4NBz28CHandler7OpenSeqEP19ISequentialInStream: # @_ZThn8_N8NArchive4NBz28CHandler7OpenSeqEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	-8(%rbx), %rax
	addq	$-8, %rbx
	movq	%rbx, %rdi
	callq	*48(%rax)
	testq	%r14, %r14
	je	.LBB10_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB10_2:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB10_4:                               # %_ZN8NArchive4NBz28CHandler7OpenSeqEP19ISequentialInStream.exit
	movq	%r14, 48(%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	_ZThn8_N8NArchive4NBz28CHandler7OpenSeqEP19ISequentialInStream, .Lfunc_end10-_ZThn8_N8NArchive4NBz28CHandler7OpenSeqEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN8NArchive4NBz28CHandler5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler5CloseEv,@function
_ZN8NArchive4NBz28CHandler5CloseEv:     # @_ZN8NArchive4NBz28CHandler5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 16
.Lcfi24:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	$0, 72(%rbx)
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 48(%rbx)
.LBB11_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamE7ReleaseEv.exit
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 40(%rbx)
.LBB11_4:                               # %_ZN9CMyComPtrI9IInStreamE7ReleaseEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end11:
	.size	_ZN8NArchive4NBz28CHandler5CloseEv, .Lfunc_end11-_ZN8NArchive4NBz28CHandler5CloseEv
	.cfi_endproc

	.globl	_ZN8NArchive4NBz28CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler7ExtractEPKjjiP23IArchiveExtractCallback,@function
_ZN8NArchive4NBz28CHandler7ExtractEPKjjiP23IArchiveExtractCallback: # @_ZN8NArchive4NBz28CHandler7ExtractEPKjjiP23IArchiveExtractCallback
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 80
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movl	%ecx, %ebx
	movq	%rdi, %r12
	cmpl	$-1, %edx
	je	.LBB12_6
# BB#1:
	testl	%edx, %edx
	je	.LBB12_2
# BB#3:
	cmpl	$1, %edx
	jne	.LBB12_5
# BB#4:
	cmpl	$0, (%rsi)
	je	.LBB12_6
.LBB12_5:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	jmp	.LBB12_91
.LBB12_6:
	cmpq	$0, 40(%r12)
	je	.LBB12_8
# BB#7:
	movq	(%r15), %rax
	movq	56(%r12), %rsi
.Ltmp41:
	movq	%r15, %rdi
	callq	*40(%rax)
.Ltmp42:
.LBB12_8:
	movq	$0, 16(%rsp)
	movq	(%r15), %rax
.Ltmp44:
	leaq	16(%rsp), %rsi
	movq	%r15, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp45:
# BB#9:
	testl	%ebp, %ebp
	jne	.LBB12_91
# BB#10:
	movq	$0, 8(%rsp)
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	setne	%r14b
	movq	(%r15), %rax
.Ltmp47:
	leaq	8(%rsp), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%r14d, %ecx
	callq	*56(%rax)
	movl	%eax, %ebp
.Ltmp48:
# BB#11:
	testl	%ebp, %ebp
	jne	.LBB12_45
# BB#12:
	testl	%ebx, %ebx
	sete	%al
	cmpq	$0, 8(%rsp)
	jne	.LBB12_14
# BB#13:
	xorl	%ebp, %ebp
	testb	%al, %al
	jne	.LBB12_91
.LBB12_14:
	movq	(%r15), %rax
.Ltmp49:
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	*64(%rax)
.Ltmp50:
# BB#15:
.Ltmp51:
	movl	$28992, %edi            # imm = 0x7140
	callq	_Znwm
	movq	%rax, %r14
.Ltmp52:
# BB#16:
.Ltmp54:
	movq	%r14, %rdi
	callq	_ZN9NCompress6NBZip28CDecoderC1Ev
.Ltmp55:
# BB#17:
	movq	(%r14), %rax
.Ltmp57:
	movq	%r14, %rdi
	callq	*8(%rax)
.Ltmp58:
# BB#18:                                # %_ZN9CMyComPtrI14ICompressCoderEC2EPS0_.exit
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB12_21
# BB#19:
	movq	(%rdi), %rax
	movq	64(%r12), %rsi
.Ltmp60:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp61:
# BB#20:
	testl	%ebp, %ebp
	jne	.LBB12_44
.LBB12_21:
	movq	(%r14), %rax
	movq	48(%r12), %rsi
.Ltmp62:
	movq	%r14, %rdi
	callq	*48(%rax)
.Ltmp63:
# BB#22:
	movq	(%r14), %rax
	movl	88(%r12), %esi
.Ltmp64:
	movq	%r14, %rdi
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp65:
# BB#23:
	testl	%ebp, %ebp
	je	.LBB12_24
.LBB12_44:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit145
	movq	(%r14), %rax
.Ltmp116:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp117:
.LBB12_45:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit140
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_91
# BB#46:
	movq	(%rdi), %rax
.Ltmp122:
	callq	*16(%rax)
.Ltmp123:
	jmp	.LBB12_91
.LBB12_2:
	xorl	%ebp, %ebp
.LBB12_91:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_24:
.Ltmp66:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp67:
# BB#25:
	movl	$0, 8(%rbx)
	movq	$_ZTV15CDummyOutStream+16, (%rbx)
	movq	$0, 16(%rbx)
.Ltmp69:
	movq	%rbx, %rdi
	callq	*_ZTV15CDummyOutStream+24(%rip)
.Ltmp70:
# BB#26:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit
	movq	8(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB12_28
# BB#27:
	movq	(%rbp), %rax
.Ltmp72:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp73:
.LBB12_28:                              # %.noexc149
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_30
# BB#29:
	movq	(%rdi), %rax
.Ltmp74:
	callq	*16(%rax)
.Ltmp75:
.LBB12_30:
	movq	%rbp, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_33
# BB#31:
	movq	(%rdi), %rax
.Ltmp76:
	callq	*16(%rax)
.Ltmp77:
# BB#32:                                # %.noexc151
	movq	$0, 8(%rsp)
.LBB12_33:                              # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
.Ltmp78:
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp79:
# BB#34:
.Ltmp81:
	movq	%r13, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp82:
# BB#35:
	movq	(%r13), %rax
.Ltmp84:
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp85:
# BB#36:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit
.Ltmp87:
	movl	$1, %ebp
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp88:
# BB#37:                                # %.preheader
	movq	16(%rsp), %rax
	jmp	.LBB12_38
.LBB12_73:                              #   in Loop: Header=BB12_38 Depth=1
	movq	344(%r14), %rax
	addq	376(%r14), %rax
	subq	360(%r14), %rax
	movl	$32, %ecx
	subl	336(%r14), %ecx
	shrl	$3, %ecx
	subq	%rcx, %rax
	movq	%rax, 16(%rsp)
	movq	%rax, 56(%r12)
	movb	$1, 72(%r12)
	xorl	%ebp, %ebp
.LBB12_38:                              # =>This Inner Loop Header: Depth=1
	movq	%rax, 48(%r13)
	movq	24(%rbx), %rax
	movq	%rax, 56(%r13)
.Ltmp90:
	movq	%r13, %rdi
	callq	_ZN14CLocalProgress6SetCurEv
.Ltmp91:
# BB#39:                                #   in Loop: Header=BB12_38 Depth=1
	testl	%eax, %eax
	jne	.LBB12_40
# BB#61:                                #   in Loop: Header=BB12_38 Depth=1
.Ltmp93:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	leaq	7(%rsp), %rdx
	movq	%r13, %rcx
	callq	_ZN9NCompress6NBZip28CDecoder10CodeResumeEP20ISequentialOutStreamRbP21ICompressProgressInfo
.Ltmp94:
# BB#62:                                #   in Loop: Header=BB12_38 Depth=1
	testl	%eax, %eax
	jne	.LBB12_63
# BB#72:                                #   in Loop: Header=BB12_38 Depth=1
	cmpb	$0, 7(%rsp)
	jne	.LBB12_73
	jmp	.LBB12_64
.LBB12_40:
	movl	%eax, %ebp
	jmp	.LBB12_41
.LBB12_63:
	movl	%eax, %ebp
.LBB12_64:                              # %.loopexit179
	movq	(%r14), %rax
.Ltmp96:
	movq	%r14, %rdi
	callq	*56(%rax)
.Ltmp97:
# BB#65:
	movq	(%rbx), %rax
.Ltmp98:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp99:
# BB#66:                                # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit154
	xorl	%ebx, %ebx
	testl	%ebp, %ebp
	je	.LBB12_69
# BB#67:                                # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit154
	cmpl	$1, %ebp
	jne	.LBB12_41
# BB#68:
	movl	$2, %ebx
.LBB12_69:
	movq	(%r15), %rax
.Ltmp101:
	movq	%r15, %rdi
	movl	%ebx, %esi
	callq	*72(%rax)
	movl	%eax, %ebp
.Ltmp102:
# BB#70:
	xorl	%ebx, %ebx
.LBB12_41:                              # %.loopexit178
	movq	(%r13), %rax
.Ltmp106:
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp107:
# BB#42:                                # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit148
	testq	%rbx, %rbx
	je	.LBB12_44
# BB#43:
	movq	(%rbx), %rax
.Ltmp111:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp112:
	jmp	.LBB12_44
.LBB12_74:
.Ltmp103:
	movq	%rdx, %r12
	movq	%rax, %rbp
	xorl	%ebx, %ebx
	jmp	.LBB12_75
.LBB12_76:
.Ltmp113:
	jmp	.LBB12_51
.LBB12_77:
.Ltmp108:
	movq	%rdx, %r12
	movq	%rax, %rbp
	testq	%rbx, %rbx
	jne	.LBB12_79
	jmp	.LBB12_80
.LBB12_60:                              # %.loopexit.split-lp
.Ltmp100:
	jmp	.LBB12_58
.LBB12_57:
.Ltmp89:
	jmp	.LBB12_58
.LBB12_56:
.Ltmp86:
	jmp	.LBB12_54
.LBB12_55:
.Ltmp83:
	movq	%rdx, %r12
	movq	%rax, %rbp
	movq	%r13, %rdi
	callq	_ZdlPv
	jmp	.LBB12_79
.LBB12_52:
.Ltmp71:
	jmp	.LBB12_51
.LBB12_53:
.Ltmp80:
.LBB12_54:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.thread
	movq	%rdx, %r12
	movq	%rax, %rbp
	jmp	.LBB12_79
.LBB12_95:
.Ltmp118:
	jmp	.LBB12_48
.LBB12_49:
.Ltmp59:
	jmp	.LBB12_48
.LBB12_81:
.Ltmp56:
	movq	%rdx, %r12
	movq	%rax, %rbp
	movq	%r14, %rdi
	callq	_ZdlPv
	jmp	.LBB12_82
.LBB12_50:
.Ltmp68:
.LBB12_51:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit142
	movq	%rdx, %r12
	movq	%rax, %rbp
	jmp	.LBB12_80
.LBB12_84:
.Ltmp124:
	jmp	.LBB12_86
.LBB12_47:
.Ltmp53:
.LBB12_48:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	%rdx, %r12
	movq	%rax, %rbp
	jmp	.LBB12_82
.LBB12_94:
.Ltmp43:
	jmp	.LBB12_86
.LBB12_85:
.Ltmp46:
.LBB12_86:
	movq	%rdx, %r12
	movq	%rax, %rbp
	jmp	.LBB12_87
.LBB12_71:
.Ltmp95:
	jmp	.LBB12_58
.LBB12_59:                              # %.loopexit
.Ltmp92:
.LBB12_58:
	movq	%rdx, %r12
	movq	%rax, %rbp
.LBB12_75:
	movq	(%r13), %rax
.Ltmp104:
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp105:
# BB#78:                                # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	testq	%rbx, %rbx
	je	.LBB12_80
.LBB12_79:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.thread
	movq	(%rbx), %rax
.Ltmp109:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp110:
.LBB12_80:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit142
	movq	(%r14), %rax
.Ltmp114:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp115:
.LBB12_82:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_87
# BB#83:
	movq	(%rdi), %rax
.Ltmp119:
	callq	*16(%rax)
.Ltmp120:
.LBB12_87:
	movq	%rbp, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r12d
	je	.LBB12_88
# BB#90:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB12_91
.LBB12_88:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp125:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp126:
# BB#93:
.LBB12_89:
.Ltmp127:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB12_92:
.Ltmp121:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN8NArchive4NBz28CHandler7ExtractEPKjjiP23IArchiveExtractCallback, .Lfunc_end12-_ZN8NArchive4NBz28CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\307\202"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\270\002"              # Call site table length
	.long	.Ltmp41-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin3   #     jumps to .Ltmp43
	.byte	3                       #   On action: 2
	.long	.Ltmp44-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin3   #     jumps to .Ltmp46
	.byte	3                       #   On action: 2
	.long	.Ltmp47-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp52-.Ltmp47         #   Call between .Ltmp47 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin3   #     jumps to .Ltmp53
	.byte	3                       #   On action: 2
	.long	.Ltmp54-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin3   #     jumps to .Ltmp56
	.byte	3                       #   On action: 2
	.long	.Ltmp57-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin3   #     jumps to .Ltmp59
	.byte	3                       #   On action: 2
	.long	.Ltmp60-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp65-.Ltmp60         #   Call between .Ltmp60 and .Ltmp65
	.long	.Ltmp68-.Lfunc_begin3   #     jumps to .Ltmp68
	.byte	3                       #   On action: 2
	.long	.Ltmp116-.Lfunc_begin3  # >> Call Site 7 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin3  #     jumps to .Ltmp118
	.byte	3                       #   On action: 2
	.long	.Ltmp122-.Lfunc_begin3  # >> Call Site 8 <<
	.long	.Ltmp123-.Ltmp122       #   Call between .Ltmp122 and .Ltmp123
	.long	.Ltmp124-.Lfunc_begin3  #     jumps to .Ltmp124
	.byte	3                       #   On action: 2
	.long	.Ltmp66-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin3   #     jumps to .Ltmp68
	.byte	3                       #   On action: 2
	.long	.Ltmp69-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin3   #     jumps to .Ltmp71
	.byte	3                       #   On action: 2
	.long	.Ltmp72-.Lfunc_begin3   # >> Call Site 11 <<
	.long	.Ltmp79-.Ltmp72         #   Call between .Ltmp72 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin3   #     jumps to .Ltmp80
	.byte	3                       #   On action: 2
	.long	.Ltmp81-.Lfunc_begin3   # >> Call Site 12 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin3   #     jumps to .Ltmp83
	.byte	3                       #   On action: 2
	.long	.Ltmp84-.Lfunc_begin3   # >> Call Site 13 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin3   #     jumps to .Ltmp86
	.byte	3                       #   On action: 2
	.long	.Ltmp87-.Lfunc_begin3   # >> Call Site 14 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin3   #     jumps to .Ltmp89
	.byte	3                       #   On action: 2
	.long	.Ltmp90-.Lfunc_begin3   # >> Call Site 15 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp92-.Lfunc_begin3   #     jumps to .Ltmp92
	.byte	3                       #   On action: 2
	.long	.Ltmp93-.Lfunc_begin3   # >> Call Site 16 <<
	.long	.Ltmp94-.Ltmp93         #   Call between .Ltmp93 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin3   #     jumps to .Ltmp95
	.byte	3                       #   On action: 2
	.long	.Ltmp96-.Lfunc_begin3   # >> Call Site 17 <<
	.long	.Ltmp99-.Ltmp96         #   Call between .Ltmp96 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin3  #     jumps to .Ltmp100
	.byte	3                       #   On action: 2
	.long	.Ltmp101-.Lfunc_begin3  # >> Call Site 18 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin3  #     jumps to .Ltmp103
	.byte	3                       #   On action: 2
	.long	.Ltmp106-.Lfunc_begin3  # >> Call Site 19 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin3  #     jumps to .Ltmp108
	.byte	3                       #   On action: 2
	.long	.Ltmp111-.Lfunc_begin3  # >> Call Site 20 <<
	.long	.Ltmp112-.Ltmp111       #   Call between .Ltmp111 and .Ltmp112
	.long	.Ltmp113-.Lfunc_begin3  #     jumps to .Ltmp113
	.byte	3                       #   On action: 2
	.long	.Ltmp104-.Lfunc_begin3  # >> Call Site 21 <<
	.long	.Ltmp120-.Ltmp104       #   Call between .Ltmp104 and .Ltmp120
	.long	.Ltmp121-.Lfunc_begin3  #     jumps to .Ltmp121
	.byte	1                       #   On action: 1
	.long	.Ltmp120-.Lfunc_begin3  # >> Call Site 22 <<
	.long	.Ltmp125-.Ltmp120       #   Call between .Ltmp120 and .Ltmp125
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp125-.Lfunc_begin3  # >> Call Site 23 <<
	.long	.Ltmp126-.Ltmp125       #   Call between .Ltmp125 and .Ltmp126
	.long	.Ltmp127-.Lfunc_begin3  #     jumps to .Ltmp127
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin3  # >> Call Site 24 <<
	.long	.Lfunc_end12-.Ltmp126   #   Call between .Ltmp126 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NBz28CHandler15GetFileTimeTypeEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler15GetFileTimeTypeEPj,@function
_ZN8NArchive4NBz28CHandler15GetFileTimeTypeEPj: # @_ZN8NArchive4NBz28CHandler15GetFileTimeTypeEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end13:
	.size	_ZN8NArchive4NBz28CHandler15GetFileTimeTypeEPj, .Lfunc_end13-_ZN8NArchive4NBz28CHandler15GetFileTimeTypeEPj
	.cfi_endproc

	.globl	_ZThn16_N8NArchive4NBz28CHandler15GetFileTimeTypeEPj
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NBz28CHandler15GetFileTimeTypeEPj,@function
_ZThn16_N8NArchive4NBz28CHandler15GetFileTimeTypeEPj: # @_ZThn16_N8NArchive4NBz28CHandler15GetFileTimeTypeEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end14:
	.size	_ZThn16_N8NArchive4NBz28CHandler15GetFileTimeTypeEPj, .Lfunc_end14-_ZThn16_N8NArchive4NBz28CHandler15GetFileTimeTypeEPj
	.cfi_endproc

	.globl	_ZN8NArchive4NBz28CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback,@function
_ZN8NArchive4NBz28CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback: # @_ZN8NArchive4NBz28CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 160
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$-2147024809, %ebp      # imm = 0x80070057
	cmpl	$1, %edx
	jne	.LBB15_78
# BB#1:
	testq	%rbx, %rbx
	je	.LBB15_2
# BB#3:
	movq	(%rbx), %rax
	leaq	44(%rsp), %rdx
	leaq	40(%rsp), %rcx
	leaq	36(%rsp), %r8
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	*56(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB15_78
# BB#4:
	cmpl	$0, 40(%rsp)
	je	.LBB15_13
# BB#5:
	movl	$0, 48(%rsp)
	movq	(%rbx), %rax
.Ltmp128:
	leaq	48(%rsp), %rcx
	xorl	%esi, %esi
	movl	$6, %edx
	movq	%rbx, %rdi
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp129:
# BB#6:
	testl	%ebp, %ebp
	je	.LBB15_7
.LBB15_10:                              # %.thread
	leaq	48(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	jmp	.LBB15_78
.LBB15_2:
	movl	$-2147467259, %ebp      # imm = 0x80004005
.LBB15_78:
	movl	%ebp, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_7:
	movzwl	48(%rsp), %eax
	testw	%ax, %ax
	je	.LBB15_12
# BB#8:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$11, %eax
	jne	.LBB15_10
# BB#9:
	cmpw	$0, 56(%rsp)
	jne	.LBB15_10
.LBB15_12:
	leaq	48(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.LBB15_13:
	cmpl	$0, 44(%rsp)
	je	.LBB15_72
# BB#14:
	movl	$0, (%rsp)
	movq	(%rbx), %rax
.Ltmp133:
	movq	%rsp, %rcx
	xorl	%esi, %esi
	movl	$7, %edx
	movq	%rbx, %rdi
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp134:
# BB#15:
	testl	%ebp, %ebp
	jne	.LBB15_17
# BB#16:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	movzwl	(%rsp), %eax
	cmpl	$21, %eax
	jne	.LBB15_17
# BB#18:
	movq	8(%rsp), %rbp
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movl	80(%r15), %r13d
	cmpl	$-1, %r13d
	jne	.LBB15_20
# BB#19:
	movl	76(%r15), %eax
	cmpl	$2, %eax
	movl	$500000, %ecx           # imm = 0x7A120
	movl	$100000, %edx           # imm = 0x186A0
	cmoval	%ecx, %edx
	cmpl	$4, %eax
	movl	$900000, %r13d          # imm = 0xDBBA0
	cmovbel	%edx, %r13d
.LBB15_20:
	movl	84(%r15), %r12d
	cmpl	$-1, %r12d
	jne	.LBB15_22
# BB#21:
	movl	76(%r15), %eax
	xorl	%ecx, %ecx
	cmpl	$6, %eax
	seta	%cl
	incl	%ecx
	cmpl	$8, %eax
	movl	$7, %r12d
	cmovbel	%ecx, %r12d
.LBB15_22:
	movl	88(%r15), %r15d
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	*40(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB15_78
# BB#23:
	movq	$0, 96(%rsp)
	movq	(%rbx), %rax
	leaq	96(%rsp), %rsi
	movq	%rbx, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB15_78
# BB#24:
	movq	$0, 16(%rsp)
	movq	(%rbx), %rax
.Ltmp139:
	leaq	16(%rsp), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	*72(%rax)
	movl	%eax, %ebp
.Ltmp140:
# BB#25:
	testl	%ebp, %ebp
	jne	.LBB15_43
# BB#26:
.Ltmp141:
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp142:
# BB#27:
.Ltmp144:
	movq	%rbp, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp145:
# BB#28:
	movq	(%rbp), %rax
.Ltmp147:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp148:
# BB#29:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit.i
.Ltmp150:
	movl	$1, %edx
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp151:
# BB#30:
.Ltmp152:
	movl	$712, %edi              # imm = 0x2C8
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp153:
# BB#31:
.Ltmp155:
	movq	%rbp, %rdi
	callq	_ZN9NCompress6NBZip28CEncoderC1Ev
.Ltmp156:
# BB#32:
	movq	(%rbp), %rax
.Ltmp158:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp159:
# BB#33:                                # %_ZN9CMyComPtrI14ICompressCoderEC2EPS0_.exit.i
	movw	$19, 48(%rsp)
	movw	$0, 50(%rsp)
	movl	%r13d, 56(%rsp)
	movw	$19, 64(%rsp)
	movw	$0, 66(%rsp)
	movl	%r12d, 72(%rsp)
	movw	$19, 80(%rsp)
	movw	$0, 82(%rsp)
	movl	%r15d, 88(%rsp)
	movl	.L_ZZN8NArchive4NBz2L13UpdateArchiveEyP20ISequentialOutStreamijjjP22IArchiveUpdateCallbackE7propIDs+8(%rip), %eax
	movl	%eax, 8(%rsp)
	movq	.L_ZZN8NArchive4NBz2L13UpdateArchiveEyP20ISequentialOutStreamijjjP22IArchiveUpdateCallbackE7propIDs(%rip), %rax
	movq	%rax, (%rsp)
	movq	(%rbp), %rax
.Ltmp161:
	movq	%rsp, %rsi
	leaq	48(%rsp), %r13
	movl	$3, %ecx
	movq	%rbp, %rdi
	movq	%r13, %rdx
	movq	%rbp, %r12
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp162:
# BB#34:
	leaq	80(%rsp), %r15
.Ltmp171:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp172:
# BB#35:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit.i
	leaq	64(%rsp), %r15
.Ltmp173:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp174:
# BB#36:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit.1.i
.Ltmp175:
	movq	%r13, %r15
	movq	%r13, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp176:
# BB#37:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit.2.i
	testl	%ebp, %ebp
	jne	.LBB15_41
# BB#38:
	movq	(%r12), %rax
	movq	16(%rsp), %rsi
.Ltmp181:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	24(%rsp), %r9           # 8-byte Reload
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp182:
# BB#39:
	testl	%ebp, %ebp
	jne	.LBB15_41
# BB#40:
	movq	(%rbx), %rax
.Ltmp183:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	*80(%rax)
	movl	%eax, %ebp
.Ltmp184:
.LBB15_41:
	movq	(%r12), %rax
.Ltmp188:
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp189:
# BB#42:                                # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit23.i
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp193:
	callq	*16(%rax)
.Ltmp194:
.LBB15_43:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit21.i
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_78
# BB#44:
	movq	(%rdi), %rax
	callq	*16(%rax)
	jmp	.LBB15_78
.LBB15_17:                              # %.thread67
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	jmp	.LBB15_78
.LBB15_72:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	cmpl	$0, 36(%rsp)
	jne	.LBB15_78
# BB#73:
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB15_74
# BB#75:
	movq	(%rdi), %rax
	movq	64(%r15), %rsi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB15_78
# BB#76:                                # %._crit_edge
	movq	40(%r15), %rdi
	jmp	.LBB15_77
.LBB15_74:
	xorl	%edi, %edi
.LBB15_77:
	xorl	%edx, %edx
	movq	%r14, %rsi
	callq	_ZN9NCompress10CopyStreamEP19ISequentialInStreamP20ISequentialOutStreamP21ICompressProgressInfo
	movl	%eax, %ebp
	jmp	.LBB15_78
.LBB15_57:
.Ltmp185:
	movq	%rax, %rbx
	jmp	.LBB15_58
.LBB15_59:
.Ltmp195:
	jmp	.LBB15_64
.LBB15_56:
.Ltmp190:
	jmp	.LBB15_61
.LBB15_53:
.Ltmp163:
	movq	%rax, %rbx
	leaq	80(%rsp), %rdi
.Ltmp164:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp165:
# BB#54:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit27.i
	leaq	64(%rsp), %rdi
.Ltmp166:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp167:
# BB#55:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit27.1.i
.Ltmp168:
	leaq	48(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp169:
	jmp	.LBB15_58
.LBB15_69:                              # %.loopexit.split-lp.loopexit.i
.Ltmp170:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_49:
.Ltmp160:
	jmp	.LBB15_61
.LBB15_48:
.Ltmp157:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB15_62
.LBB15_47:
.Ltmp149:
	jmp	.LBB15_64
.LBB15_46:
.Ltmp146:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB15_65
.LBB15_60:
.Ltmp154:
.LBB15_61:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit.i
	movq	%rax, %rbx
	jmp	.LBB15_62
.LBB15_50:
.Ltmp177:
	movq	%rax, %rbx
.LBB15_51:                              # =>This Inner Loop Header: Depth=1
	cmpq	%r15, %r13
	je	.LBB15_58
# BB#52:                                # %.preheader.i
                                        #   in Loop: Header=BB15_51 Depth=1
	addq	$-16, %r15
.Ltmp178:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp179:
	jmp	.LBB15_51
.LBB15_58:
	movq	(%r12), %rax
.Ltmp186:
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp187:
.LBB15_62:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit.i
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp191:
	callq	*16(%rax)
.Ltmp192:
	jmp	.LBB15_65
.LBB15_68:                              # %.loopexit.i
.Ltmp180:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_63:
.Ltmp143:
.LBB15_64:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.i
	movq	%rax, %rbx
.LBB15_65:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.i
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_67
# BB#66:
	movq	(%rdi), %rax
.Ltmp196:
	callq	*16(%rax)
.Ltmp197:
	jmp	.LBB15_67
.LBB15_70:                              # %.loopexit.split-lp.loopexit.split-lp.i
.Ltmp198:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_45:
.Ltmp135:
	movq	%rax, %rbx
.Ltmp136:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp137:
	jmp	.LBB15_67
.LBB15_11:
.Ltmp130:
	movq	%rax, %rbx
.Ltmp131:
	leaq	48(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp132:
.LBB15_67:                              # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB15_79:
.Ltmp138:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN8NArchive4NBz28CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback, .Lfunc_end15-_ZN8NArchive4NBz28CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\247\202"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\236\002"              # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp128-.Lfunc_begin4  #   Call between .Lfunc_begin4 and .Ltmp128
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp128-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp129-.Ltmp128       #   Call between .Ltmp128 and .Ltmp129
	.long	.Ltmp130-.Lfunc_begin4  #     jumps to .Ltmp130
	.byte	0                       #   On action: cleanup
	.long	.Ltmp129-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp133-.Ltmp129       #   Call between .Ltmp129 and .Ltmp133
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp135-.Lfunc_begin4  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp134-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp139-.Ltmp134       #   Call between .Ltmp134 and .Ltmp139
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp139-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp142-.Ltmp139       #   Call between .Ltmp139 and .Ltmp142
	.long	.Ltmp143-.Lfunc_begin4  #     jumps to .Ltmp143
	.byte	0                       #   On action: cleanup
	.long	.Ltmp144-.Lfunc_begin4  # >> Call Site 7 <<
	.long	.Ltmp145-.Ltmp144       #   Call between .Ltmp144 and .Ltmp145
	.long	.Ltmp146-.Lfunc_begin4  #     jumps to .Ltmp146
	.byte	0                       #   On action: cleanup
	.long	.Ltmp147-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Ltmp148-.Ltmp147       #   Call between .Ltmp147 and .Ltmp148
	.long	.Ltmp149-.Lfunc_begin4  #     jumps to .Ltmp149
	.byte	0                       #   On action: cleanup
	.long	.Ltmp150-.Lfunc_begin4  # >> Call Site 9 <<
	.long	.Ltmp153-.Ltmp150       #   Call between .Ltmp150 and .Ltmp153
	.long	.Ltmp154-.Lfunc_begin4  #     jumps to .Ltmp154
	.byte	0                       #   On action: cleanup
	.long	.Ltmp155-.Lfunc_begin4  # >> Call Site 10 <<
	.long	.Ltmp156-.Ltmp155       #   Call between .Ltmp155 and .Ltmp156
	.long	.Ltmp157-.Lfunc_begin4  #     jumps to .Ltmp157
	.byte	0                       #   On action: cleanup
	.long	.Ltmp158-.Lfunc_begin4  # >> Call Site 11 <<
	.long	.Ltmp159-.Ltmp158       #   Call between .Ltmp158 and .Ltmp159
	.long	.Ltmp160-.Lfunc_begin4  #     jumps to .Ltmp160
	.byte	0                       #   On action: cleanup
	.long	.Ltmp161-.Lfunc_begin4  # >> Call Site 12 <<
	.long	.Ltmp162-.Ltmp161       #   Call between .Ltmp161 and .Ltmp162
	.long	.Ltmp163-.Lfunc_begin4  #     jumps to .Ltmp163
	.byte	0                       #   On action: cleanup
	.long	.Ltmp171-.Lfunc_begin4  # >> Call Site 13 <<
	.long	.Ltmp176-.Ltmp171       #   Call between .Ltmp171 and .Ltmp176
	.long	.Ltmp177-.Lfunc_begin4  #     jumps to .Ltmp177
	.byte	0                       #   On action: cleanup
	.long	.Ltmp181-.Lfunc_begin4  # >> Call Site 14 <<
	.long	.Ltmp184-.Ltmp181       #   Call between .Ltmp181 and .Ltmp184
	.long	.Ltmp185-.Lfunc_begin4  #     jumps to .Ltmp185
	.byte	0                       #   On action: cleanup
	.long	.Ltmp188-.Lfunc_begin4  # >> Call Site 15 <<
	.long	.Ltmp189-.Ltmp188       #   Call between .Ltmp188 and .Ltmp189
	.long	.Ltmp190-.Lfunc_begin4  #     jumps to .Ltmp190
	.byte	0                       #   On action: cleanup
	.long	.Ltmp193-.Lfunc_begin4  # >> Call Site 16 <<
	.long	.Ltmp194-.Ltmp193       #   Call between .Ltmp193 and .Ltmp194
	.long	.Ltmp195-.Lfunc_begin4  #     jumps to .Ltmp195
	.byte	0                       #   On action: cleanup
	.long	.Ltmp194-.Lfunc_begin4  # >> Call Site 17 <<
	.long	.Ltmp164-.Ltmp194       #   Call between .Ltmp194 and .Ltmp164
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp164-.Lfunc_begin4  # >> Call Site 18 <<
	.long	.Ltmp169-.Ltmp164       #   Call between .Ltmp164 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin4  #     jumps to .Ltmp170
	.byte	1                       #   On action: 1
	.long	.Ltmp178-.Lfunc_begin4  # >> Call Site 19 <<
	.long	.Ltmp179-.Ltmp178       #   Call between .Ltmp178 and .Ltmp179
	.long	.Ltmp180-.Lfunc_begin4  #     jumps to .Ltmp180
	.byte	1                       #   On action: 1
	.long	.Ltmp186-.Lfunc_begin4  # >> Call Site 20 <<
	.long	.Ltmp197-.Ltmp186       #   Call between .Ltmp186 and .Ltmp197
	.long	.Ltmp198-.Lfunc_begin4  #     jumps to .Ltmp198
	.byte	1                       #   On action: 1
	.long	.Ltmp136-.Lfunc_begin4  # >> Call Site 21 <<
	.long	.Ltmp132-.Ltmp136       #   Call between .Ltmp136 and .Ltmp132
	.long	.Ltmp138-.Lfunc_begin4  #     jumps to .Ltmp138
	.byte	1                       #   On action: 1
	.long	.Ltmp132-.Lfunc_begin4  # >> Call Site 22 <<
	.long	.Lfunc_end15-.Ltmp132   #   Call between .Ltmp132 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn16_N8NArchive4NBz28CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NBz28CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback,@function
_ZThn16_N8NArchive4NBz28CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback: # @_ZThn16_N8NArchive4NBz28CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN8NArchive4NBz28CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback # TAILCALL
.Lfunc_end16:
	.size	_ZThn16_N8NArchive4NBz28CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback, .Lfunc_end16-_ZThn16_N8NArchive4NBz28CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_0:
	.zero	16
	.text
	.globl	_ZN8NArchive4NBz28CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi,@function
_ZN8NArchive4NBz28CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi: # @_ZN8NArchive4NBz28CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 160
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movl	$-1, 84(%rbx)
	movabsq	$-4294967291, %rax      # imm = 0xFFFFFFFF00000005
	movq	%rax, 76(%rbx)
	callq	_ZN8NWindows7NSystem21GetNumberOfProcessorsEv
	movl	%eax, 88(%rbx)
	callq	_ZN8NWindows7NSystem21GetNumberOfProcessorsEv
	movl	%eax, 68(%rsp)          # 4-byte Spill
	movl	%eax, 88(%rbx)
	testl	%ebp, %ebp
	jle	.LBB17_1
# BB#3:                                 # %.lr.ph
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	leaq	88(%rbx), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movslq	%ebp, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
                                        # implicit-def: %R13D
	.p2align	4, 0x90
.LBB17_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_5 Depth 2
                                        #     Child Loop BB17_7 Depth 2
	movl	%r13d, %r15d
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movl	$-1, %ebp
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB17_5:                               #   Parent Loop BB17_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB17_5
# BB#6:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
                                        #   in Loop: Header=BB17_4 Depth=1
	leal	1(%rbp), %eax
	movslq	%eax, %r14
	movq	%r14, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, 32(%rsp)
	movl	$0, (%rax)
	movl	%r14d, 44(%rsp)
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB17_7:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   Parent Loop BB17_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %edx
	addq	$4, %rbx
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB17_7
# BB#8:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
                                        #   in Loop: Header=BB17_4 Depth=1
	movl	%ebp, 40(%rsp)
.Ltmp199:
	movq	%rax, %rdi
	callq	_Z13MyStringUpperPw
.Ltmp200:
# BB#9:                                 # %_ZN11CStringBaseIwE9MakeUpperEv.exit
                                        #   in Loop: Header=BB17_4 Depth=1
	movl	40(%rsp), %ecx
	movl	$-2147024809, %r13d     # imm = 0x80070057
	movl	$1, %ebx
	testl	%ecx, %ecx
	je	.LBB17_58
# BB#10:                                #   in Loop: Header=BB17_4 Depth=1
	movl	%r15d, 28(%rsp)         # 4-byte Spill
	movq	%r12, %r15
	shlq	$4, %r15
	addq	80(%rsp), %r15          # 8-byte Folded Reload
	movq	32(%rsp), %rax
	movl	(%rax), %eax
	cmpl	$68, %eax
	je	.LBB17_21
# BB#11:                                #   in Loop: Header=BB17_4 Depth=1
	cmpl	$88, %eax
	jne	.LBB17_31
# BB#12:                                #   in Loop: Header=BB17_4 Depth=1
	movl	$9, 4(%rsp)
	decl	%ecx
.Ltmp208:
	movl	$1, %edx
	leaq	8(%rsp), %rbp
	movq	%rbp, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp209:
# BB#13:                                # %_ZNK11CStringBaseIwE3MidEi.exit
                                        #   in Loop: Header=BB17_4 Depth=1
.Ltmp211:
	movq	%rbp, %rdi
	movq	%r15, %rsi
	leaq	4(%rsp), %rdx
	callq	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %r13d
.Ltmp212:
# BB#14:                                #   in Loop: Header=BB17_4 Depth=1
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	movl	28(%rsp), %ebp          # 4-byte Reload
	je	.LBB17_16
# BB#15:                                #   in Loop: Header=BB17_4 Depth=1
	callq	_ZdaPv
.LBB17_16:                              # %_ZN11CStringBaseIwED2Ev.exit91
                                        #   in Loop: Header=BB17_4 Depth=1
	testl	%r13d, %r13d
	jne	.LBB17_58
# BB#17:                                # %.thread
                                        #   in Loop: Header=BB17_4 Depth=1
	movl	4(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 76(%rcx)
	jmp	.LBB17_27
	.p2align	4, 0x90
.LBB17_21:                              #   in Loop: Header=BB17_4 Depth=1
	movl	$900000, 4(%rsp)        # imm = 0xDBBA0
	decl	%ecx
.Ltmp202:
	movl	$1, %edx
	leaq	8(%rsp), %rbp
	movq	%rbp, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp203:
# BB#22:                                # %_ZNK11CStringBaseIwE3MidEi.exit94
                                        #   in Loop: Header=BB17_4 Depth=1
.Ltmp205:
	movq	%rbp, %rdi
	movq	%r15, %rsi
	leaq	4(%rsp), %rdx
	callq	_Z24ParsePropDictionaryValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %r13d
.Ltmp206:
# BB#23:                                #   in Loop: Header=BB17_4 Depth=1
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	movl	28(%rsp), %ebp          # 4-byte Reload
	je	.LBB17_25
# BB#24:                                #   in Loop: Header=BB17_4 Depth=1
	callq	_ZdaPv
.LBB17_25:                              # %_ZN11CStringBaseIwED2Ev.exit95
                                        #   in Loop: Header=BB17_4 Depth=1
	testl	%r13d, %r13d
	jne	.LBB17_58
# BB#26:                                # %.thread109
                                        #   in Loop: Header=BB17_4 Depth=1
	movl	4(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 80(%rcx)
	jmp	.LBB17_27
	.p2align	4, 0x90
.LBB17_31:                              #   in Loop: Header=BB17_4 Depth=1
.Ltmp214:
	xorl	%edx, %edx
	movl	$4, %ecx
	leaq	8(%rsp), %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp215:
# BB#32:                                # %_ZNK11CStringBaseIwE4LeftEi.exit
                                        #   in Loop: Header=BB17_4 Depth=1
	movq	8(%rsp), %rdi
.Ltmp217:
	movl	$.L.str, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %r14d
.Ltmp218:
# BB#33:                                #   in Loop: Header=BB17_4 Depth=1
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_35
# BB#34:                                #   in Loop: Header=BB17_4 Depth=1
	callq	_ZdaPv
.LBB17_35:                              # %_ZN11CStringBaseIwED2Ev.exit97
                                        #   in Loop: Header=BB17_4 Depth=1
	testl	%r14d, %r14d
	je	.LBB17_36
# BB#48:                                #   in Loop: Header=BB17_4 Depth=1
.Ltmp220:
	xorl	%edx, %edx
	movl	$2, %ecx
	leaq	8(%rsp), %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp221:
# BB#49:                                # %_ZNK11CStringBaseIwE4LeftEi.exit102
                                        #   in Loop: Header=BB17_4 Depth=1
	movq	8(%rsp), %rdi
.Ltmp223:
	movl	$.L.str.1, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %ebp
.Ltmp224:
# BB#50:                                #   in Loop: Header=BB17_4 Depth=1
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_52
# BB#51:                                #   in Loop: Header=BB17_4 Depth=1
	callq	_ZdaPv
.LBB17_52:                              # %_ZN11CStringBaseIwED2Ev.exit104
                                        #   in Loop: Header=BB17_4 Depth=1
	testl	%ebp, %ebp
	jne	.LBB17_58
# BB#53:                                #   in Loop: Header=BB17_4 Depth=1
	movl	40(%rsp), %ecx
	addl	$-2, %ecx
.Ltmp226:
	movl	$2, %edx
	leaq	8(%rsp), %rbp
	movq	%rbp, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp227:
# BB#54:                                # %_ZNK11CStringBaseIwE3MidEi.exit105
                                        #   in Loop: Header=BB17_4 Depth=1
.Ltmp229:
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movl	68(%rsp), %edx          # 4-byte Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	callq	_Z11ParseMtPropRK11CStringBaseIwERK14tagPROPVARIANTjRj
	movl	%eax, %r13d
.Ltmp230:
# BB#55:                                #   in Loop: Header=BB17_4 Depth=1
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	movl	28(%rsp), %ebp          # 4-byte Reload
	je	.LBB17_57
# BB#56:                                #   in Loop: Header=BB17_4 Depth=1
	callq	_ZdaPv
.LBB17_57:                              # %_ZN11CStringBaseIwED2Ev.exit106
                                        #   in Loop: Header=BB17_4 Depth=1
	testl	%r13d, %r13d
	jne	.LBB17_58
	jmp	.LBB17_27
.LBB17_36:                              #   in Loop: Header=BB17_4 Depth=1
	movl	$7, 4(%rsp)
	movl	40(%rsp), %ecx
	addl	$-4, %ecx
.Ltmp232:
	movl	$4, %edx
	leaq	8(%rsp), %rbp
	movq	%rbp, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp233:
# BB#37:                                # %_ZNK11CStringBaseIwE3MidEi.exit98
                                        #   in Loop: Header=BB17_4 Depth=1
.Ltmp235:
	movq	%rbp, %rdi
	movq	%r15, %rsi
	leaq	4(%rsp), %rdx
	callq	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %r13d
.Ltmp236:
# BB#38:                                #   in Loop: Header=BB17_4 Depth=1
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	movl	28(%rsp), %ebp          # 4-byte Reload
	je	.LBB17_40
# BB#39:                                #   in Loop: Header=BB17_4 Depth=1
	callq	_ZdaPv
.LBB17_40:                              # %_ZN11CStringBaseIwED2Ev.exit99
                                        #   in Loop: Header=BB17_4 Depth=1
	testl	%r13d, %r13d
	jne	.LBB17_58
# BB#41:                                # %.thread111
                                        #   in Loop: Header=BB17_4 Depth=1
	movl	4(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 84(%rcx)
	.p2align	4, 0x90
.LBB17_27:                              #   in Loop: Header=BB17_4 Depth=1
	xorl	%ebx, %ebx
	movl	%ebp, %r13d
.LBB17_58:                              #   in Loop: Header=BB17_4 Depth=1
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_60
# BB#59:                                #   in Loop: Header=BB17_4 Depth=1
	callq	_ZdaPv
.LBB17_60:                              # %_ZN11CStringBaseIwED2Ev.exit90
                                        #   in Loop: Header=BB17_4 Depth=1
	testl	%ebx, %ebx
	jne	.LBB17_2
# BB#61:                                #   in Loop: Header=BB17_4 Depth=1
	incq	%r12
	cmpq	88(%rsp), %r12          # 8-byte Folded Reload
	jl	.LBB17_4
.LBB17_1:
	xorl	%r13d, %r13d
.LBB17_2:                               # %._crit_edge
	movl	%r13d, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_66:
.Ltmp231:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_70
# BB#67:
	callq	_ZdaPv
	jmp	.LBB17_70
.LBB17_65:
.Ltmp228:
	jmp	.LBB17_69
.LBB17_46:
.Ltmp237:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_70
# BB#47:
	callq	_ZdaPv
	jmp	.LBB17_70
.LBB17_45:
.Ltmp234:
	jmp	.LBB17_69
.LBB17_63:
.Ltmp225:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_70
# BB#64:
	callq	_ZdaPv
	jmp	.LBB17_70
.LBB17_62:
.Ltmp222:
	jmp	.LBB17_69
.LBB17_43:
.Ltmp219:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_70
# BB#44:
	callq	_ZdaPv
	jmp	.LBB17_70
.LBB17_42:
.Ltmp216:
	jmp	.LBB17_69
.LBB17_29:
.Ltmp207:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_70
# BB#30:
	callq	_ZdaPv
	jmp	.LBB17_70
.LBB17_28:
.Ltmp204:
	jmp	.LBB17_69
.LBB17_19:
.Ltmp213:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_70
# BB#20:
	callq	_ZdaPv
	jmp	.LBB17_70
.LBB17_18:
.Ltmp210:
	jmp	.LBB17_69
.LBB17_68:
.Ltmp201:
.LBB17_69:
	movq	%rax, %rbx
.LBB17_70:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_72
# BB#71:
	callq	_ZdaPv
.LBB17_72:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end17:
	.size	_ZN8NArchive4NBz28CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi, .Lfunc_end17-_ZN8NArchive4NBz28CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\306\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\303\001"              # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp199-.Lfunc_begin5  #   Call between .Lfunc_begin5 and .Ltmp199
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp199-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp200-.Ltmp199       #   Call between .Ltmp199 and .Ltmp200
	.long	.Ltmp201-.Lfunc_begin5  #     jumps to .Ltmp201
	.byte	0                       #   On action: cleanup
	.long	.Ltmp208-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp209-.Ltmp208       #   Call between .Ltmp208 and .Ltmp209
	.long	.Ltmp210-.Lfunc_begin5  #     jumps to .Ltmp210
	.byte	0                       #   On action: cleanup
	.long	.Ltmp211-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp212-.Ltmp211       #   Call between .Ltmp211 and .Ltmp212
	.long	.Ltmp213-.Lfunc_begin5  #     jumps to .Ltmp213
	.byte	0                       #   On action: cleanup
	.long	.Ltmp202-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp203-.Ltmp202       #   Call between .Ltmp202 and .Ltmp203
	.long	.Ltmp204-.Lfunc_begin5  #     jumps to .Ltmp204
	.byte	0                       #   On action: cleanup
	.long	.Ltmp205-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp206-.Ltmp205       #   Call between .Ltmp205 and .Ltmp206
	.long	.Ltmp207-.Lfunc_begin5  #     jumps to .Ltmp207
	.byte	0                       #   On action: cleanup
	.long	.Ltmp214-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp215-.Ltmp214       #   Call between .Ltmp214 and .Ltmp215
	.long	.Ltmp216-.Lfunc_begin5  #     jumps to .Ltmp216
	.byte	0                       #   On action: cleanup
	.long	.Ltmp217-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp218-.Ltmp217       #   Call between .Ltmp217 and .Ltmp218
	.long	.Ltmp219-.Lfunc_begin5  #     jumps to .Ltmp219
	.byte	0                       #   On action: cleanup
	.long	.Ltmp220-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp221-.Ltmp220       #   Call between .Ltmp220 and .Ltmp221
	.long	.Ltmp222-.Lfunc_begin5  #     jumps to .Ltmp222
	.byte	0                       #   On action: cleanup
	.long	.Ltmp223-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp224-.Ltmp223       #   Call between .Ltmp223 and .Ltmp224
	.long	.Ltmp225-.Lfunc_begin5  #     jumps to .Ltmp225
	.byte	0                       #   On action: cleanup
	.long	.Ltmp226-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Ltmp227-.Ltmp226       #   Call between .Ltmp226 and .Ltmp227
	.long	.Ltmp228-.Lfunc_begin5  #     jumps to .Ltmp228
	.byte	0                       #   On action: cleanup
	.long	.Ltmp229-.Lfunc_begin5  # >> Call Site 12 <<
	.long	.Ltmp230-.Ltmp229       #   Call between .Ltmp229 and .Ltmp230
	.long	.Ltmp231-.Lfunc_begin5  #     jumps to .Ltmp231
	.byte	0                       #   On action: cleanup
	.long	.Ltmp232-.Lfunc_begin5  # >> Call Site 13 <<
	.long	.Ltmp233-.Ltmp232       #   Call between .Ltmp232 and .Ltmp233
	.long	.Ltmp234-.Lfunc_begin5  #     jumps to .Ltmp234
	.byte	0                       #   On action: cleanup
	.long	.Ltmp235-.Lfunc_begin5  # >> Call Site 14 <<
	.long	.Ltmp236-.Ltmp235       #   Call between .Ltmp235 and .Ltmp236
	.long	.Ltmp237-.Lfunc_begin5  #     jumps to .Ltmp237
	.byte	0                       #   On action: cleanup
	.long	.Ltmp236-.Lfunc_begin5  # >> Call Site 15 <<
	.long	.Lfunc_end17-.Ltmp236   #   Call between .Ltmp236 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn24_N8NArchive4NBz28CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive4NBz28CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi,@function
_ZThn24_N8NArchive4NBz28CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi: # @_ZThn24_N8NArchive4NBz28CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN8NArchive4NBz28CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi # TAILCALL
.Lfunc_end18:
	.size	_ZThn24_N8NArchive4NBz28CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi, .Lfunc_end18-_ZThn24_N8NArchive4NBz28CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_endproc

	.section	.text._ZN8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB19_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB19_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB19_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB19_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB19_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB19_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB19_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB19_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB19_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB19_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB19_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB19_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB19_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB19_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB19_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB19_16
.LBB19_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_IInArchive(%rip), %cl
	jne	.LBB19_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_IInArchive+1(%rip), %al
	jne	.LBB19_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_IInArchive+2(%rip), %al
	jne	.LBB19_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_IInArchive+3(%rip), %al
	jne	.LBB19_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_IInArchive+4(%rip), %al
	jne	.LBB19_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_IInArchive+5(%rip), %al
	jne	.LBB19_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_IInArchive+6(%rip), %al
	jne	.LBB19_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_IInArchive+7(%rip), %al
	jne	.LBB19_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_IInArchive+8(%rip), %al
	jne	.LBB19_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_IInArchive+9(%rip), %al
	jne	.LBB19_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_IInArchive+10(%rip), %al
	jne	.LBB19_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_IInArchive+11(%rip), %al
	jne	.LBB19_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_IInArchive+12(%rip), %al
	jne	.LBB19_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_IInArchive+13(%rip), %al
	jne	.LBB19_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_IInArchive+14(%rip), %al
	jne	.LBB19_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit12
	movb	15(%rsi), %al
	cmpb	IID_IInArchive+15(%rip), %al
	jne	.LBB19_33
.LBB19_16:
	movq	%rdi, (%rdx)
	jmp	.LBB19_85
.LBB19_33:                              # %_ZeqRK4GUIDS1_.exit12.thread
	cmpb	IID_IArchiveOpenSeq(%rip), %cl
	jne	.LBB19_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+1(%rip), %al
	jne	.LBB19_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+2(%rip), %al
	jne	.LBB19_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+3(%rip), %al
	jne	.LBB19_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+4(%rip), %al
	jne	.LBB19_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+5(%rip), %al
	jne	.LBB19_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+6(%rip), %al
	jne	.LBB19_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+7(%rip), %al
	jne	.LBB19_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+8(%rip), %al
	jne	.LBB19_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+9(%rip), %al
	jne	.LBB19_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+10(%rip), %al
	jne	.LBB19_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+11(%rip), %al
	jne	.LBB19_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+12(%rip), %al
	jne	.LBB19_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+13(%rip), %al
	jne	.LBB19_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+14(%rip), %al
	jne	.LBB19_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit16
	movb	15(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+15(%rip), %al
	jne	.LBB19_50
# BB#49:
	leaq	8(%rdi), %rax
	jmp	.LBB19_84
.LBB19_50:                              # %_ZeqRK4GUIDS1_.exit16.thread
	cmpb	IID_IOutArchive(%rip), %cl
	jne	.LBB19_67
# BB#51:
	movb	1(%rsi), %al
	cmpb	IID_IOutArchive+1(%rip), %al
	jne	.LBB19_67
# BB#52:
	movb	2(%rsi), %al
	cmpb	IID_IOutArchive+2(%rip), %al
	jne	.LBB19_67
# BB#53:
	movb	3(%rsi), %al
	cmpb	IID_IOutArchive+3(%rip), %al
	jne	.LBB19_67
# BB#54:
	movb	4(%rsi), %al
	cmpb	IID_IOutArchive+4(%rip), %al
	jne	.LBB19_67
# BB#55:
	movb	5(%rsi), %al
	cmpb	IID_IOutArchive+5(%rip), %al
	jne	.LBB19_67
# BB#56:
	movb	6(%rsi), %al
	cmpb	IID_IOutArchive+6(%rip), %al
	jne	.LBB19_67
# BB#57:
	movb	7(%rsi), %al
	cmpb	IID_IOutArchive+7(%rip), %al
	jne	.LBB19_67
# BB#58:
	movb	8(%rsi), %al
	cmpb	IID_IOutArchive+8(%rip), %al
	jne	.LBB19_67
# BB#59:
	movb	9(%rsi), %al
	cmpb	IID_IOutArchive+9(%rip), %al
	jne	.LBB19_67
# BB#60:
	movb	10(%rsi), %al
	cmpb	IID_IOutArchive+10(%rip), %al
	jne	.LBB19_67
# BB#61:
	movb	11(%rsi), %al
	cmpb	IID_IOutArchive+11(%rip), %al
	jne	.LBB19_67
# BB#62:
	movb	12(%rsi), %al
	cmpb	IID_IOutArchive+12(%rip), %al
	jne	.LBB19_67
# BB#63:
	movb	13(%rsi), %al
	cmpb	IID_IOutArchive+13(%rip), %al
	jne	.LBB19_67
# BB#64:
	movb	14(%rsi), %al
	cmpb	IID_IOutArchive+14(%rip), %al
	jne	.LBB19_67
# BB#65:                                # %_ZeqRK4GUIDS1_.exit18
	movb	15(%rsi), %al
	cmpb	IID_IOutArchive+15(%rip), %al
	jne	.LBB19_67
# BB#66:
	leaq	16(%rdi), %rax
	jmp	.LBB19_84
.LBB19_67:                              # %_ZeqRK4GUIDS1_.exit18.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ISetProperties(%rip), %cl
	jne	.LBB19_86
# BB#68:
	movb	1(%rsi), %cl
	cmpb	IID_ISetProperties+1(%rip), %cl
	jne	.LBB19_86
# BB#69:
	movb	2(%rsi), %cl
	cmpb	IID_ISetProperties+2(%rip), %cl
	jne	.LBB19_86
# BB#70:
	movb	3(%rsi), %cl
	cmpb	IID_ISetProperties+3(%rip), %cl
	jne	.LBB19_86
# BB#71:
	movb	4(%rsi), %cl
	cmpb	IID_ISetProperties+4(%rip), %cl
	jne	.LBB19_86
# BB#72:
	movb	5(%rsi), %cl
	cmpb	IID_ISetProperties+5(%rip), %cl
	jne	.LBB19_86
# BB#73:
	movb	6(%rsi), %cl
	cmpb	IID_ISetProperties+6(%rip), %cl
	jne	.LBB19_86
# BB#74:
	movb	7(%rsi), %cl
	cmpb	IID_ISetProperties+7(%rip), %cl
	jne	.LBB19_86
# BB#75:
	movb	8(%rsi), %cl
	cmpb	IID_ISetProperties+8(%rip), %cl
	jne	.LBB19_86
# BB#76:
	movb	9(%rsi), %cl
	cmpb	IID_ISetProperties+9(%rip), %cl
	jne	.LBB19_86
# BB#77:
	movb	10(%rsi), %cl
	cmpb	IID_ISetProperties+10(%rip), %cl
	jne	.LBB19_86
# BB#78:
	movb	11(%rsi), %cl
	cmpb	IID_ISetProperties+11(%rip), %cl
	jne	.LBB19_86
# BB#79:
	movb	12(%rsi), %cl
	cmpb	IID_ISetProperties+12(%rip), %cl
	jne	.LBB19_86
# BB#80:
	movb	13(%rsi), %cl
	cmpb	IID_ISetProperties+13(%rip), %cl
	jne	.LBB19_86
# BB#81:
	movb	14(%rsi), %cl
	cmpb	IID_ISetProperties+14(%rip), %cl
	jne	.LBB19_86
# BB#82:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %cl
	cmpb	IID_ISetProperties+15(%rip), %cl
	jne	.LBB19_86
# BB#83:
	leaq	24(%rdi), %rax
.LBB19_84:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movq	%rax, (%rdx)
.LBB19_85:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB19_86:                              # %_ZeqRK4GUIDS1_.exit14.thread
	popq	%rcx
	retq
.Lfunc_end19:
	.size	_ZN8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end19-_ZN8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive4NBz28CHandler6AddRefEv,"axG",@progbits,_ZN8NArchive4NBz28CHandler6AddRefEv,comdat
	.weak	_ZN8NArchive4NBz28CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler6AddRefEv,@function
_ZN8NArchive4NBz28CHandler6AddRefEv:    # @_ZN8NArchive4NBz28CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	32(%rdi), %eax
	incl	%eax
	movl	%eax, 32(%rdi)
	retq
.Lfunc_end20:
	.size	_ZN8NArchive4NBz28CHandler6AddRefEv, .Lfunc_end20-_ZN8NArchive4NBz28CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NBz28CHandler7ReleaseEv,"axG",@progbits,_ZN8NArchive4NBz28CHandler7ReleaseEv,comdat
	.weak	_ZN8NArchive4NBz28CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandler7ReleaseEv,@function
_ZN8NArchive4NBz28CHandler7ReleaseEv:   # @_ZN8NArchive4NBz28CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 16
	movl	32(%rdi), %eax
	decl	%eax
	movl	%eax, 32(%rdi)
	jne	.LBB21_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB21_2:
	popq	%rcx
	retq
.Lfunc_end21:
	.size	_ZN8NArchive4NBz28CHandler7ReleaseEv, .Lfunc_end21-_ZN8NArchive4NBz28CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NBz28CHandlerD2Ev,"axG",@progbits,_ZN8NArchive4NBz28CHandlerD2Ev,comdat
	.weak	_ZN8NArchive4NBz28CHandlerD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandlerD2Ev,@function
_ZN8NArchive4NBz28CHandlerD2Ev:         # @_ZN8NArchive4NBz28CHandlerD2Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 32
.Lcfi69:
	.cfi_offset %rbx, -24
.Lcfi70:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive4NBz28CHandlerE+184, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN8NArchive4NBz28CHandlerE+320, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+248, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp238:
	callq	*16(%rax)
.Ltmp239:
.LBB22_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp244:
	callq	*16(%rax)
.Ltmp245:
.LBB22_4:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB22_7:
.Ltmp246:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_5:
.Ltmp240:
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp241:
	callq	*16(%rax)
.Ltmp242:
.LBB22_8:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit8
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_9:
.Ltmp243:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZN8NArchive4NBz28CHandlerD2Ev, .Lfunc_end22-_ZN8NArchive4NBz28CHandlerD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp238-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp239-.Ltmp238       #   Call between .Ltmp238 and .Ltmp239
	.long	.Ltmp240-.Lfunc_begin6  #     jumps to .Ltmp240
	.byte	0                       #   On action: cleanup
	.long	.Ltmp244-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp245-.Ltmp244       #   Call between .Ltmp244 and .Ltmp245
	.long	.Ltmp246-.Lfunc_begin6  #     jumps to .Ltmp246
	.byte	0                       #   On action: cleanup
	.long	.Ltmp245-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp241-.Ltmp245       #   Call between .Ltmp245 and .Ltmp241
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp241-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp242-.Ltmp241       #   Call between .Ltmp241 and .Ltmp242
	.long	.Ltmp243-.Lfunc_begin6  #     jumps to .Ltmp243
	.byte	1                       #   On action: 1
	.long	.Ltmp242-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Lfunc_end22-.Ltmp242   #   Call between .Ltmp242 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NBz28CHandlerD0Ev,"axG",@progbits,_ZN8NArchive4NBz28CHandlerD0Ev,comdat
	.weak	_ZN8NArchive4NBz28CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz28CHandlerD0Ev,@function
_ZN8NArchive4NBz28CHandlerD0Ev:         # @_ZN8NArchive4NBz28CHandlerD0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 32
.Lcfi74:
	.cfi_offset %rbx, -24
.Lcfi75:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive4NBz28CHandlerE+184, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN8NArchive4NBz28CHandlerE+320, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+248, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp247:
	callq	*16(%rax)
.Ltmp248:
.LBB23_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp253:
	callq	*16(%rax)
.Ltmp254:
.LBB23_4:                               # %_ZN8NArchive4NBz28CHandlerD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB23_8:
.Ltmp255:
	movq	%rax, %r14
	jmp	.LBB23_9
.LBB23_5:
.Ltmp249:
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_9
# BB#6:
	movq	(%rdi), %rax
.Ltmp250:
	callq	*16(%rax)
.Ltmp251:
.LBB23_9:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB23_7:
.Ltmp252:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end23:
	.size	_ZN8NArchive4NBz28CHandlerD0Ev, .Lfunc_end23-_ZN8NArchive4NBz28CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp247-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp248-.Ltmp247       #   Call between .Ltmp247 and .Ltmp248
	.long	.Ltmp249-.Lfunc_begin7  #     jumps to .Ltmp249
	.byte	0                       #   On action: cleanup
	.long	.Ltmp253-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp254-.Ltmp253       #   Call between .Ltmp253 and .Ltmp254
	.long	.Ltmp255-.Lfunc_begin7  #     jumps to .Ltmp255
	.byte	0                       #   On action: cleanup
	.long	.Ltmp250-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp251-.Ltmp250       #   Call between .Ltmp250 and .Ltmp251
	.long	.Ltmp252-.Lfunc_begin7  #     jumps to .Ltmp252
	.byte	1                       #   On action: 1
	.long	.Ltmp251-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Lfunc_end23-.Ltmp251   #   Call between .Ltmp251 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZThn8_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end24:
	.size	_ZThn8_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end24-_ZThn8_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive4NBz28CHandler6AddRefEv,"axG",@progbits,_ZThn8_N8NArchive4NBz28CHandler6AddRefEv,comdat
	.weak	_ZThn8_N8NArchive4NBz28CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NBz28CHandler6AddRefEv,@function
_ZThn8_N8NArchive4NBz28CHandler6AddRefEv: # @_ZThn8_N8NArchive4NBz28CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end25:
	.size	_ZThn8_N8NArchive4NBz28CHandler6AddRefEv, .Lfunc_end25-_ZThn8_N8NArchive4NBz28CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive4NBz28CHandler7ReleaseEv,"axG",@progbits,_ZThn8_N8NArchive4NBz28CHandler7ReleaseEv,comdat
	.weak	_ZThn8_N8NArchive4NBz28CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NBz28CHandler7ReleaseEv,@function
_ZThn8_N8NArchive4NBz28CHandler7ReleaseEv: # @_ZThn8_N8NArchive4NBz28CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi76:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB26_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB26_2:                               # %_ZN8NArchive4NBz28CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end26:
	.size	_ZThn8_N8NArchive4NBz28CHandler7ReleaseEv, .Lfunc_end26-_ZThn8_N8NArchive4NBz28CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive4NBz28CHandlerD1Ev,"axG",@progbits,_ZThn8_N8NArchive4NBz28CHandlerD1Ev,comdat
	.weak	_ZThn8_N8NArchive4NBz28CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NBz28CHandlerD1Ev,@function
_ZThn8_N8NArchive4NBz28CHandlerD1Ev:    # @_ZThn8_N8NArchive4NBz28CHandlerD1Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi79:
	.cfi_def_cfa_offset 32
.Lcfi80:
	.cfi_offset %rbx, -24
.Lcfi81:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive4NBz28CHandlerE+184, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rbx)
	movl	$_ZTVN8NArchive4NBz28CHandlerE+320, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+248, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 8(%rbx)
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp256:
	callq	*16(%rax)
.Ltmp257:
.LBB27_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp262:
	callq	*16(%rax)
.Ltmp263:
.LBB27_4:                               # %_ZN8NArchive4NBz28CHandlerD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB27_7:
.Ltmp264:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB27_5:
.Ltmp258:
	movq	%rax, %r14
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp259:
	callq	*16(%rax)
.Ltmp260:
.LBB27_8:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit8.i
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB27_9:
.Ltmp261:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end27:
	.size	_ZThn8_N8NArchive4NBz28CHandlerD1Ev, .Lfunc_end27-_ZThn8_N8NArchive4NBz28CHandlerD1Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp256-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp257-.Ltmp256       #   Call between .Ltmp256 and .Ltmp257
	.long	.Ltmp258-.Lfunc_begin8  #     jumps to .Ltmp258
	.byte	0                       #   On action: cleanup
	.long	.Ltmp262-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp263-.Ltmp262       #   Call between .Ltmp262 and .Ltmp263
	.long	.Ltmp264-.Lfunc_begin8  #     jumps to .Ltmp264
	.byte	0                       #   On action: cleanup
	.long	.Ltmp263-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp259-.Ltmp263       #   Call between .Ltmp263 and .Ltmp259
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp259-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp260-.Ltmp259       #   Call between .Ltmp259 and .Ltmp260
	.long	.Ltmp261-.Lfunc_begin8  #     jumps to .Ltmp261
	.byte	1                       #   On action: 1
	.long	.Ltmp260-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Lfunc_end27-.Ltmp260   #   Call between .Ltmp260 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZThn8_N8NArchive4NBz28CHandlerD0Ev,"axG",@progbits,_ZThn8_N8NArchive4NBz28CHandlerD0Ev,comdat
	.weak	_ZThn8_N8NArchive4NBz28CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NBz28CHandlerD0Ev,@function
_ZThn8_N8NArchive4NBz28CHandlerD0Ev:    # @_ZThn8_N8NArchive4NBz28CHandlerD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 32
.Lcfi85:
	.cfi_offset %rbx, -24
.Lcfi86:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	movl	$_ZTVN8NArchive4NBz28CHandlerE+184, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rax)
	movl	$_ZTVN8NArchive4NBz28CHandlerE+320, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+248, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 8(%rax)
	movq	40(%rax), %rdi
	leaq	-8(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB28_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp265:
	callq	*16(%rax)
.Ltmp266:
.LBB28_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i.i
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB28_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp271:
	callq	*16(%rax)
.Ltmp272:
.LBB28_4:                               # %_ZN8NArchive4NBz28CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB28_8:
.Ltmp273:
	movq	%rax, %r14
	jmp	.LBB28_9
.LBB28_5:
.Ltmp267:
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB28_9
# BB#6:
	movq	(%rdi), %rax
.Ltmp268:
	callq	*16(%rax)
.Ltmp269:
.LBB28_9:                               # %.body.i
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB28_7:
.Ltmp270:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end28:
	.size	_ZThn8_N8NArchive4NBz28CHandlerD0Ev, .Lfunc_end28-_ZThn8_N8NArchive4NBz28CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp265-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp266-.Ltmp265       #   Call between .Ltmp265 and .Ltmp266
	.long	.Ltmp267-.Lfunc_begin9  #     jumps to .Ltmp267
	.byte	0                       #   On action: cleanup
	.long	.Ltmp271-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp272-.Ltmp271       #   Call between .Ltmp271 and .Ltmp272
	.long	.Ltmp273-.Lfunc_begin9  #     jumps to .Ltmp273
	.byte	0                       #   On action: cleanup
	.long	.Ltmp268-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp269-.Ltmp268       #   Call between .Ltmp268 and .Ltmp269
	.long	.Ltmp270-.Lfunc_begin9  #     jumps to .Ltmp270
	.byte	1                       #   On action: 1
	.long	.Ltmp269-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Lfunc_end28-.Ltmp269   #   Call between .Ltmp269 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZThn16_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end29:
	.size	_ZThn16_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end29-_ZThn16_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive4NBz28CHandler6AddRefEv,"axG",@progbits,_ZThn16_N8NArchive4NBz28CHandler6AddRefEv,comdat
	.weak	_ZThn16_N8NArchive4NBz28CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NBz28CHandler6AddRefEv,@function
_ZThn16_N8NArchive4NBz28CHandler6AddRefEv: # @_ZThn16_N8NArchive4NBz28CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end30:
	.size	_ZThn16_N8NArchive4NBz28CHandler6AddRefEv, .Lfunc_end30-_ZThn16_N8NArchive4NBz28CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive4NBz28CHandler7ReleaseEv,"axG",@progbits,_ZThn16_N8NArchive4NBz28CHandler7ReleaseEv,comdat
	.weak	_ZThn16_N8NArchive4NBz28CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NBz28CHandler7ReleaseEv,@function
_ZThn16_N8NArchive4NBz28CHandler7ReleaseEv: # @_ZThn16_N8NArchive4NBz28CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi87:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB31_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB31_2:                               # %_ZN8NArchive4NBz28CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end31:
	.size	_ZThn16_N8NArchive4NBz28CHandler7ReleaseEv, .Lfunc_end31-_ZThn16_N8NArchive4NBz28CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive4NBz28CHandlerD1Ev,"axG",@progbits,_ZThn16_N8NArchive4NBz28CHandlerD1Ev,comdat
	.weak	_ZThn16_N8NArchive4NBz28CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NBz28CHandlerD1Ev,@function
_ZThn16_N8NArchive4NBz28CHandlerD1Ev:   # @_ZThn16_N8NArchive4NBz28CHandlerD1Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi90:
	.cfi_def_cfa_offset 32
.Lcfi91:
	.cfi_offset %rbx, -24
.Lcfi92:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive4NBz28CHandlerE+184, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -16(%rbx)
	movl	$_ZTVN8NArchive4NBz28CHandlerE+320, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+248, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp274:
	callq	*16(%rax)
.Ltmp275:
.LBB32_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp280:
	callq	*16(%rax)
.Ltmp281:
.LBB32_4:                               # %_ZN8NArchive4NBz28CHandlerD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB32_7:
.Ltmp282:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB32_5:
.Ltmp276:
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp277:
	callq	*16(%rax)
.Ltmp278:
.LBB32_8:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit8.i
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB32_9:
.Ltmp279:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end32:
	.size	_ZThn16_N8NArchive4NBz28CHandlerD1Ev, .Lfunc_end32-_ZThn16_N8NArchive4NBz28CHandlerD1Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp274-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp275-.Ltmp274       #   Call between .Ltmp274 and .Ltmp275
	.long	.Ltmp276-.Lfunc_begin10 #     jumps to .Ltmp276
	.byte	0                       #   On action: cleanup
	.long	.Ltmp280-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp281-.Ltmp280       #   Call between .Ltmp280 and .Ltmp281
	.long	.Ltmp282-.Lfunc_begin10 #     jumps to .Ltmp282
	.byte	0                       #   On action: cleanup
	.long	.Ltmp281-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp277-.Ltmp281       #   Call between .Ltmp281 and .Ltmp277
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp277-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Ltmp278-.Ltmp277       #   Call between .Ltmp277 and .Ltmp278
	.long	.Ltmp279-.Lfunc_begin10 #     jumps to .Ltmp279
	.byte	1                       #   On action: 1
	.long	.Ltmp278-.Lfunc_begin10 # >> Call Site 5 <<
	.long	.Lfunc_end32-.Ltmp278   #   Call between .Ltmp278 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZThn16_N8NArchive4NBz28CHandlerD0Ev,"axG",@progbits,_ZThn16_N8NArchive4NBz28CHandlerD0Ev,comdat
	.weak	_ZThn16_N8NArchive4NBz28CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NBz28CHandlerD0Ev,@function
_ZThn16_N8NArchive4NBz28CHandlerD0Ev:   # @_ZThn16_N8NArchive4NBz28CHandlerD0Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi95:
	.cfi_def_cfa_offset 32
.Lcfi96:
	.cfi_offset %rbx, -24
.Lcfi97:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	movl	$_ZTVN8NArchive4NBz28CHandlerE+184, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -16(%rax)
	movl	$_ZTVN8NArchive4NBz28CHandlerE+320, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+248, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rax)
	movq	32(%rax), %rdi
	leaq	-16(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB33_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp283:
	callq	*16(%rax)
.Ltmp284:
.LBB33_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i.i
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB33_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp289:
	callq	*16(%rax)
.Ltmp290:
.LBB33_4:                               # %_ZN8NArchive4NBz28CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB33_8:
.Ltmp291:
	movq	%rax, %r14
	jmp	.LBB33_9
.LBB33_5:
.Ltmp285:
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB33_9
# BB#6:
	movq	(%rdi), %rax
.Ltmp286:
	callq	*16(%rax)
.Ltmp287:
.LBB33_9:                               # %.body.i
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB33_7:
.Ltmp288:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end33:
	.size	_ZThn16_N8NArchive4NBz28CHandlerD0Ev, .Lfunc_end33-_ZThn16_N8NArchive4NBz28CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp283-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp284-.Ltmp283       #   Call between .Ltmp283 and .Ltmp284
	.long	.Ltmp285-.Lfunc_begin11 #     jumps to .Ltmp285
	.byte	0                       #   On action: cleanup
	.long	.Ltmp289-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp290-.Ltmp289       #   Call between .Ltmp289 and .Ltmp290
	.long	.Ltmp291-.Lfunc_begin11 #     jumps to .Ltmp291
	.byte	0                       #   On action: cleanup
	.long	.Ltmp286-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp287-.Ltmp286       #   Call between .Ltmp286 and .Ltmp287
	.long	.Ltmp288-.Lfunc_begin11 #     jumps to .Ltmp288
	.byte	1                       #   On action: 1
	.long	.Ltmp287-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Lfunc_end33-.Ltmp287   #   Call between .Ltmp287 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZThn24_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn24_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn24_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn24_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn24_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end34:
	.size	_ZThn24_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end34-_ZThn24_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn24_N8NArchive4NBz28CHandler6AddRefEv,"axG",@progbits,_ZThn24_N8NArchive4NBz28CHandler6AddRefEv,comdat
	.weak	_ZThn24_N8NArchive4NBz28CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive4NBz28CHandler6AddRefEv,@function
_ZThn24_N8NArchive4NBz28CHandler6AddRefEv: # @_ZThn24_N8NArchive4NBz28CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end35:
	.size	_ZThn24_N8NArchive4NBz28CHandler6AddRefEv, .Lfunc_end35-_ZThn24_N8NArchive4NBz28CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn24_N8NArchive4NBz28CHandler7ReleaseEv,"axG",@progbits,_ZThn24_N8NArchive4NBz28CHandler7ReleaseEv,comdat
	.weak	_ZThn24_N8NArchive4NBz28CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive4NBz28CHandler7ReleaseEv,@function
_ZThn24_N8NArchive4NBz28CHandler7ReleaseEv: # @_ZThn24_N8NArchive4NBz28CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi98:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB36_2
# BB#1:
	addq	$-24, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB36_2:                               # %_ZN8NArchive4NBz28CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end36:
	.size	_ZThn24_N8NArchive4NBz28CHandler7ReleaseEv, .Lfunc_end36-_ZThn24_N8NArchive4NBz28CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn24_N8NArchive4NBz28CHandlerD1Ev,"axG",@progbits,_ZThn24_N8NArchive4NBz28CHandlerD1Ev,comdat
	.weak	_ZThn24_N8NArchive4NBz28CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive4NBz28CHandlerD1Ev,@function
_ZThn24_N8NArchive4NBz28CHandlerD1Ev:   # @_ZThn24_N8NArchive4NBz28CHandlerD1Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 32
.Lcfi102:
	.cfi_offset %rbx, -24
.Lcfi103:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive4NBz28CHandlerE+184, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -24(%rbx)
	movl	$_ZTVN8NArchive4NBz28CHandlerE+320, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+248, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB37_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp292:
	callq	*16(%rax)
.Ltmp293:
.LBB37_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB37_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp298:
	callq	*16(%rax)
.Ltmp299:
.LBB37_4:                               # %_ZN8NArchive4NBz28CHandlerD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB37_7:
.Ltmp300:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB37_5:
.Ltmp294:
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB37_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp295:
	callq	*16(%rax)
.Ltmp296:
.LBB37_8:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit8.i
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB37_9:
.Ltmp297:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end37:
	.size	_ZThn24_N8NArchive4NBz28CHandlerD1Ev, .Lfunc_end37-_ZThn24_N8NArchive4NBz28CHandlerD1Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table37:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp292-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp293-.Ltmp292       #   Call between .Ltmp292 and .Ltmp293
	.long	.Ltmp294-.Lfunc_begin12 #     jumps to .Ltmp294
	.byte	0                       #   On action: cleanup
	.long	.Ltmp298-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp299-.Ltmp298       #   Call between .Ltmp298 and .Ltmp299
	.long	.Ltmp300-.Lfunc_begin12 #     jumps to .Ltmp300
	.byte	0                       #   On action: cleanup
	.long	.Ltmp299-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp295-.Ltmp299       #   Call between .Ltmp299 and .Ltmp295
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp295-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Ltmp296-.Ltmp295       #   Call between .Ltmp295 and .Ltmp296
	.long	.Ltmp297-.Lfunc_begin12 #     jumps to .Ltmp297
	.byte	1                       #   On action: 1
	.long	.Ltmp296-.Lfunc_begin12 # >> Call Site 5 <<
	.long	.Lfunc_end37-.Ltmp296   #   Call between .Ltmp296 and .Lfunc_end37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZThn24_N8NArchive4NBz28CHandlerD0Ev,"axG",@progbits,_ZThn24_N8NArchive4NBz28CHandlerD0Ev,comdat
	.weak	_ZThn24_N8NArchive4NBz28CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive4NBz28CHandlerD0Ev,@function
_ZThn24_N8NArchive4NBz28CHandlerD0Ev:   # @_ZThn24_N8NArchive4NBz28CHandlerD0Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -24
.Lcfi108:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	movl	$_ZTVN8NArchive4NBz28CHandlerE+184, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -24(%rax)
	movl	$_ZTVN8NArchive4NBz28CHandlerE+320, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+248, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rax)
	movq	24(%rax), %rdi
	leaq	-24(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB38_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp301:
	callq	*16(%rax)
.Ltmp302:
.LBB38_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i.i
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB38_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp307:
	callq	*16(%rax)
.Ltmp308:
.LBB38_4:                               # %_ZN8NArchive4NBz28CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB38_8:
.Ltmp309:
	movq	%rax, %r14
	jmp	.LBB38_9
.LBB38_5:
.Ltmp303:
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB38_9
# BB#6:
	movq	(%rdi), %rax
.Ltmp304:
	callq	*16(%rax)
.Ltmp305:
.LBB38_9:                               # %.body.i
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB38_7:
.Ltmp306:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end38:
	.size	_ZThn24_N8NArchive4NBz28CHandlerD0Ev, .Lfunc_end38-_ZThn24_N8NArchive4NBz28CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table38:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp301-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp302-.Ltmp301       #   Call between .Ltmp301 and .Ltmp302
	.long	.Ltmp303-.Lfunc_begin13 #     jumps to .Ltmp303
	.byte	0                       #   On action: cleanup
	.long	.Ltmp307-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp308-.Ltmp307       #   Call between .Ltmp307 and .Ltmp308
	.long	.Ltmp309-.Lfunc_begin13 #     jumps to .Ltmp309
	.byte	0                       #   On action: cleanup
	.long	.Ltmp304-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp305-.Ltmp304       #   Call between .Ltmp304 and .Ltmp305
	.long	.Ltmp306-.Lfunc_begin13 #     jumps to .Ltmp306
	.byte	1                       #   On action: 1
	.long	.Ltmp305-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Lfunc_end38-.Ltmp305   #   Call between .Ltmp305 and .Lfunc_end38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz2L9CreateArcEv,@function
_ZN8NArchive4NBz2L9CreateArcEv:         # @_ZN8NArchive4NBz2L9CreateArcEv
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 32
.Lcfi112:
	.cfi_offset %rbx, -32
.Lcfi113:
	.cfi_offset %r14, -24
.Lcfi114:
	.cfi_offset %r15, -16
	movl	$96, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	$0, 32(%rbx)
	movl	$_ZTVN8NArchive4NBz28CHandlerE+184, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN8NArchive4NBz28CHandlerE+320, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+248, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 40(%rbx)
	movl	$-1, 84(%rbx)
	movabsq	$-4294967291, %rax      # imm = 0xFFFFFFFF00000005
	movq	%rax, 76(%rbx)
	leaq	40(%rbx), %r15
.Ltmp310:
	callq	_ZN8NWindows7NSystem21GetNumberOfProcessorsEv
.Ltmp311:
# BB#1:
	movl	%eax, 88(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB39_2:
.Ltmp312:
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB39_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp313:
	callq	*16(%rax)
.Ltmp314:
.LBB39_4:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB39_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp315:
	callq	*16(%rax)
.Ltmp316:
.LBB39_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB39_7:
.Ltmp317:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end39:
	.size	_ZN8NArchive4NBz2L9CreateArcEv, .Lfunc_end39-_ZN8NArchive4NBz2L9CreateArcEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table39:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin14-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp310-.Lfunc_begin14 #   Call between .Lfunc_begin14 and .Ltmp310
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp310-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp311-.Ltmp310       #   Call between .Ltmp310 and .Ltmp311
	.long	.Ltmp312-.Lfunc_begin14 #     jumps to .Ltmp312
	.byte	0                       #   On action: cleanup
	.long	.Ltmp313-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp316-.Ltmp313       #   Call between .Ltmp313 and .Ltmp316
	.long	.Ltmp317-.Lfunc_begin14 #     jumps to .Ltmp317
	.byte	1                       #   On action: 1
	.long	.Ltmp316-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end39-.Ltmp316   #   Call between .Ltmp316 and .Lfunc_end39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive4NBz2L12CreateArcOutEv,@function
_ZN8NArchive4NBz2L12CreateArcOutEv:     # @_ZN8NArchive4NBz2L12CreateArcOutEv
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r15
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 32
.Lcfi118:
	.cfi_offset %rbx, -32
.Lcfi119:
	.cfi_offset %r14, -24
.Lcfi120:
	.cfi_offset %r15, -16
	movl	$96, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	$0, 32(%rbx)
	movl	$_ZTVN8NArchive4NBz28CHandlerE+184, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN8NArchive4NBz28CHandlerE+320, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NBz28CHandlerE+248, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	leaq	40(%rbx), %r15
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 40(%rbx)
	movl	$-1, 84(%rbx)
	movabsq	$-4294967291, %rax      # imm = 0xFFFFFFFF00000005
	movq	%rax, 76(%rbx)
.Ltmp318:
	callq	_ZN8NWindows7NSystem21GetNumberOfProcessorsEv
.Ltmp319:
# BB#1:
	movq	%rbx, %rcx
	addq	$16, %rcx
	movl	%eax, 88(%rbx)
	movq	%rcx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB40_2:
.Ltmp320:
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB40_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp321:
	callq	*16(%rax)
.Ltmp322:
.LBB40_4:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB40_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp323:
	callq	*16(%rax)
.Ltmp324:
.LBB40_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB40_7:
.Ltmp325:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end40:
	.size	_ZN8NArchive4NBz2L12CreateArcOutEv, .Lfunc_end40-_ZN8NArchive4NBz2L12CreateArcOutEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin15-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp318-.Lfunc_begin15 #   Call between .Lfunc_begin15 and .Ltmp318
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp318-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp319-.Ltmp318       #   Call between .Ltmp318 and .Ltmp319
	.long	.Ltmp320-.Lfunc_begin15 #     jumps to .Ltmp320
	.byte	0                       #   On action: cleanup
	.long	.Ltmp321-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp324-.Ltmp321       #   Call between .Ltmp321 and .Ltmp324
	.long	.Ltmp325-.Lfunc_begin15 #     jumps to .Ltmp325
	.byte	1                       #   On action: 1
	.long	.Ltmp324-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Lfunc_end40-.Ltmp324   #   Call between .Ltmp324 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%rbp
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi124:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi125:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi127:
	.cfi_def_cfa_offset 64
.Lcfi128:
	.cfi_offset %rbx, -56
.Lcfi129:
	.cfi_offset %r12, -48
.Lcfi130:
	.cfi_offset %r13, -40
.Lcfi131:
	.cfi_offset %r14, -32
.Lcfi132:
	.cfi_offset %r15, -24
.Lcfi133:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB41_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB41_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB41_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB41_5
.LBB41_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB41_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp326:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp327:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB41_35
# BB#12:
	movq	%rbx, %r13
.LBB41_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB41_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB41_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB41_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB41_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB41_15
.LBB41_14:
	xorl	%esi, %esi
.LBB41_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB41_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB41_16
.LBB41_29:
	movq	%r13, %rbx
.LBB41_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp328:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp329:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB41_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB41_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB41_8
.LBB41_3:
	xorl	%eax, %eax
.LBB41_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB41_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB41_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB41_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB41_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB41_30
.LBB41_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB41_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB41_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB41_24
	jmp	.LBB41_25
.LBB41_22:
	xorl	%ecx, %ecx
.LBB41_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB41_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB41_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB41_27
.LBB41_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB41_15
	jmp	.LBB41_29
.LBB41_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp330:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end41:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end41-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table41:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin16-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp326-.Lfunc_begin16 #   Call between .Lfunc_begin16 and .Ltmp326
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp326-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp329-.Ltmp326       #   Call between .Ltmp326 and .Ltmp329
	.long	.Ltmp330-.Lfunc_begin16 #     jumps to .Ltmp330
	.byte	0                       #   On action: cleanup
	.long	.Ltmp329-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Lfunc_end41-.Ltmp329   #   Call between .Ltmp329 and .Lfunc_end41
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_Bz2Handler.ii,@function
_GLOBAL__sub_I_Bz2Handler.ii:           # @_GLOBAL__sub_I_Bz2Handler.ii
	.cfi_startproc
# BB#0:
	movl	$_ZN8NArchive4NBz2L9g_ArcInfoE, %edi
	jmp	_Z11RegisterArcPK8CArcInfo # TAILCALL
.Lfunc_end42:
	.size	_GLOBAL__sub_I_Bz2Handler.ii, .Lfunc_end42-_GLOBAL__sub_I_Bz2Handler.ii
	.cfi_endproc

	.type	_ZN8NArchive4NBz26kPropsE,@object # @_ZN8NArchive4NBz26kPropsE
	.data
	.globl	_ZN8NArchive4NBz26kPropsE
	.p2align	4
_ZN8NArchive4NBz26kPropsE:
	.quad	0
	.long	8                       # 0x8
	.short	21                      # 0x15
	.zero	2
	.size	_ZN8NArchive4NBz26kPropsE, 16

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	80                      # 0x50
	.long	65                      # 0x41
	.long	83                      # 0x53
	.long	83                      # 0x53
	.long	0                       # 0x0
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
	.p2align	2
.L.str.1:
	.long	77                      # 0x4d
	.long	84                      # 0x54
	.long	0                       # 0x0
	.size	.L.str.1, 12

	.type	_ZTVN8NArchive4NBz28CHandlerE,@object # @_ZTVN8NArchive4NBz28CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NArchive4NBz28CHandlerE
	.p2align	3
_ZTVN8NArchive4NBz28CHandlerE:
	.quad	0
	.quad	_ZTIN8NArchive4NBz28CHandlerE
	.quad	_ZN8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive4NBz28CHandler6AddRefEv
	.quad	_ZN8NArchive4NBz28CHandler7ReleaseEv
	.quad	_ZN8NArchive4NBz28CHandlerD2Ev
	.quad	_ZN8NArchive4NBz28CHandlerD0Ev
	.quad	_ZN8NArchive4NBz28CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.quad	_ZN8NArchive4NBz28CHandler5CloseEv
	.quad	_ZN8NArchive4NBz28CHandler16GetNumberOfItemsEPj
	.quad	_ZN8NArchive4NBz28CHandler11GetPropertyEjjP14tagPROPVARIANT
	.quad	_ZN8NArchive4NBz28CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.quad	_ZN8NArchive4NBz28CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.quad	_ZN8NArchive4NBz28CHandler21GetNumberOfPropertiesEPj
	.quad	_ZN8NArchive4NBz28CHandler15GetPropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive4NBz28CHandler28GetNumberOfArchivePropertiesEPj
	.quad	_ZN8NArchive4NBz28CHandler22GetArchivePropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive4NBz28CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.quad	_ZN8NArchive4NBz28CHandler15GetFileTimeTypeEPj
	.quad	_ZN8NArchive4NBz28CHandler7OpenSeqEP19ISequentialInStream
	.quad	_ZN8NArchive4NBz28CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.quad	-8
	.quad	_ZTIN8NArchive4NBz28CHandlerE
	.quad	_ZThn8_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N8NArchive4NBz28CHandler6AddRefEv
	.quad	_ZThn8_N8NArchive4NBz28CHandler7ReleaseEv
	.quad	_ZThn8_N8NArchive4NBz28CHandlerD1Ev
	.quad	_ZThn8_N8NArchive4NBz28CHandlerD0Ev
	.quad	_ZThn8_N8NArchive4NBz28CHandler7OpenSeqEP19ISequentialInStream
	.quad	-16
	.quad	_ZTIN8NArchive4NBz28CHandlerE
	.quad	_ZThn16_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N8NArchive4NBz28CHandler6AddRefEv
	.quad	_ZThn16_N8NArchive4NBz28CHandler7ReleaseEv
	.quad	_ZThn16_N8NArchive4NBz28CHandlerD1Ev
	.quad	_ZThn16_N8NArchive4NBz28CHandlerD0Ev
	.quad	_ZThn16_N8NArchive4NBz28CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.quad	_ZThn16_N8NArchive4NBz28CHandler15GetFileTimeTypeEPj
	.quad	-24
	.quad	_ZTIN8NArchive4NBz28CHandlerE
	.quad	_ZThn24_N8NArchive4NBz28CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn24_N8NArchive4NBz28CHandler6AddRefEv
	.quad	_ZThn24_N8NArchive4NBz28CHandler7ReleaseEv
	.quad	_ZThn24_N8NArchive4NBz28CHandlerD1Ev
	.quad	_ZThn24_N8NArchive4NBz28CHandlerD0Ev
	.quad	_ZThn24_N8NArchive4NBz28CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.size	_ZTVN8NArchive4NBz28CHandlerE, 368

	.type	_ZTSN8NArchive4NBz28CHandlerE,@object # @_ZTSN8NArchive4NBz28CHandlerE
	.globl	_ZTSN8NArchive4NBz28CHandlerE
	.p2align	4
_ZTSN8NArchive4NBz28CHandlerE:
	.asciz	"N8NArchive4NBz28CHandlerE"
	.size	_ZTSN8NArchive4NBz28CHandlerE, 26

	.type	_ZTS10IInArchive,@object # @_ZTS10IInArchive
	.section	.rodata._ZTS10IInArchive,"aG",@progbits,_ZTS10IInArchive,comdat
	.weak	_ZTS10IInArchive
_ZTS10IInArchive:
	.asciz	"10IInArchive"
	.size	_ZTS10IInArchive, 13

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI10IInArchive,@object # @_ZTI10IInArchive
	.section	.rodata._ZTI10IInArchive,"aG",@progbits,_ZTI10IInArchive,comdat
	.weak	_ZTI10IInArchive
	.p2align	4
_ZTI10IInArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IInArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI10IInArchive, 24

	.type	_ZTS15IArchiveOpenSeq,@object # @_ZTS15IArchiveOpenSeq
	.section	.rodata._ZTS15IArchiveOpenSeq,"aG",@progbits,_ZTS15IArchiveOpenSeq,comdat
	.weak	_ZTS15IArchiveOpenSeq
	.p2align	4
_ZTS15IArchiveOpenSeq:
	.asciz	"15IArchiveOpenSeq"
	.size	_ZTS15IArchiveOpenSeq, 18

	.type	_ZTI15IArchiveOpenSeq,@object # @_ZTI15IArchiveOpenSeq
	.section	.rodata._ZTI15IArchiveOpenSeq,"aG",@progbits,_ZTI15IArchiveOpenSeq,comdat
	.weak	_ZTI15IArchiveOpenSeq
	.p2align	4
_ZTI15IArchiveOpenSeq:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15IArchiveOpenSeq
	.quad	_ZTI8IUnknown
	.size	_ZTI15IArchiveOpenSeq, 24

	.type	_ZTS11IOutArchive,@object # @_ZTS11IOutArchive
	.section	.rodata._ZTS11IOutArchive,"aG",@progbits,_ZTS11IOutArchive,comdat
	.weak	_ZTS11IOutArchive
_ZTS11IOutArchive:
	.asciz	"11IOutArchive"
	.size	_ZTS11IOutArchive, 14

	.type	_ZTI11IOutArchive,@object # @_ZTI11IOutArchive
	.section	.rodata._ZTI11IOutArchive,"aG",@progbits,_ZTI11IOutArchive,comdat
	.weak	_ZTI11IOutArchive
	.p2align	4
_ZTI11IOutArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS11IOutArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI11IOutArchive, 24

	.type	_ZTS14ISetProperties,@object # @_ZTS14ISetProperties
	.section	.rodata._ZTS14ISetProperties,"aG",@progbits,_ZTS14ISetProperties,comdat
	.weak	_ZTS14ISetProperties
	.p2align	4
_ZTS14ISetProperties:
	.asciz	"14ISetProperties"
	.size	_ZTS14ISetProperties, 17

	.type	_ZTI14ISetProperties,@object # @_ZTI14ISetProperties
	.section	.rodata._ZTI14ISetProperties,"aG",@progbits,_ZTI14ISetProperties,comdat
	.weak	_ZTI14ISetProperties
	.p2align	4
_ZTI14ISetProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ISetProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI14ISetProperties, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive4NBz28CHandlerE,@object # @_ZTIN8NArchive4NBz28CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive4NBz28CHandlerE
	.p2align	4
_ZTIN8NArchive4NBz28CHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive4NBz28CHandlerE
	.long	1                       # 0x1
	.long	5                       # 0x5
	.quad	_ZTI10IInArchive
	.quad	2                       # 0x2
	.quad	_ZTI15IArchiveOpenSeq
	.quad	2050                    # 0x802
	.quad	_ZTI11IOutArchive
	.quad	4098                    # 0x1002
	.quad	_ZTI14ISetProperties
	.quad	6146                    # 0x1802
	.quad	_ZTI13CMyUnknownImp
	.quad	8194                    # 0x2002
	.size	_ZTIN8NArchive4NBz28CHandlerE, 104

	.type	.L_ZZN8NArchive4NBz2L13UpdateArchiveEyP20ISequentialOutStreamijjjP22IArchiveUpdateCallbackE7propIDs,@object # @_ZZN8NArchive4NBz2L13UpdateArchiveEyP20ISequentialOutStreamijjjP22IArchiveUpdateCallbackE7propIDs
	.p2align	2
.L_ZZN8NArchive4NBz2L13UpdateArchiveEyP20ISequentialOutStreamijjjP22IArchiveUpdateCallbackE7propIDs:
	.long	1                       # 0x1
	.long	11                      # 0xb
	.long	13                      # 0xd
	.size	.L_ZZN8NArchive4NBz2L13UpdateArchiveEyP20ISequentialOutStreamijjjP22IArchiveUpdateCallbackE7propIDs, 12

	.type	_ZN8NArchive4NBz2L9g_ArcInfoE,@object # @_ZN8NArchive4NBz2L9g_ArcInfoE
	.data
	.p2align	3
_ZN8NArchive4NBz2L9g_ArcInfoE:
	.quad	.L.str.2
	.quad	.L.str.3
	.quad	.L.str.4
	.byte	2                       # 0x2
	.asciz	"BZh\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.zero	3
	.long	3                       # 0x3
	.byte	1                       # 0x1
	.zero	3
	.quad	_ZN8NArchive4NBz2L9CreateArcEv
	.quad	_ZN8NArchive4NBz2L12CreateArcOutEv
	.size	_ZN8NArchive4NBz2L9g_ArcInfoE, 80

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.2:
	.long	98                      # 0x62
	.long	122                     # 0x7a
	.long	105                     # 0x69
	.long	112                     # 0x70
	.long	50                      # 0x32
	.long	0                       # 0x0
	.size	.L.str.2, 24

	.type	.L.str.3,@object        # @.str.3
	.p2align	2
.L.str.3:
	.long	98                      # 0x62
	.long	122                     # 0x7a
	.long	50                      # 0x32
	.long	32                      # 0x20
	.long	98                      # 0x62
	.long	122                     # 0x7a
	.long	105                     # 0x69
	.long	112                     # 0x70
	.long	50                      # 0x32
	.long	32                      # 0x20
	.long	116                     # 0x74
	.long	98                      # 0x62
	.long	122                     # 0x7a
	.long	50                      # 0x32
	.long	32                      # 0x20
	.long	116                     # 0x74
	.long	98                      # 0x62
	.long	122                     # 0x7a
	.long	0                       # 0x0
	.size	.L.str.3, 76

	.type	.L.str.4,@object        # @.str.4
	.p2align	2
.L.str.4:
	.long	42                      # 0x2a
	.long	32                      # 0x20
	.long	42                      # 0x2a
	.long	32                      # 0x20
	.long	46                      # 0x2e
	.long	116                     # 0x74
	.long	97                      # 0x61
	.long	114                     # 0x72
	.long	32                      # 0x20
	.long	46                      # 0x2e
	.long	116                     # 0x74
	.long	97                      # 0x61
	.long	114                     # 0x72
	.long	0                       # 0x0
	.size	.L.str.4, 56

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_Bz2Handler.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
