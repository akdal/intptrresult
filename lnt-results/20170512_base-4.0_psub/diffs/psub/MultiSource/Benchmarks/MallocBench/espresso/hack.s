	.text
	.file	"hack.bc"
	.globl	map_dcset
	.p2align	4, 0x90
	.type	map_dcset,@function
map_dcset:                              # @map_dcset
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	56(%r12), %r14
	testq	%r14, %r14
	je	.LBB0_47
# BB#1:
	movq	(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB0_47
# BB#2:                                 # %.preheader121
	movslq	cube+8(%rip), %r15
	testq	%r15, %r15
	jle	.LBB0_47
# BB#3:                                 # %.lr.ph134
	movl	$.L.str, %esi
	movl	$9, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_4
# BB#5:                                 # %.lr.ph148.preheader
	addq	%r15, %r15
	xorl	%r13d, %r13d
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph148
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.1, %esi
	movl	$8, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_12
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=1
	movl	$.L.str.2, %esi
	movl	$9, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_12
# BB#8:                                 #   in Loop: Header=BB0_6 Depth=1
	movl	$.L.str.3, %esi
	movl	$8, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_11
# BB#9:                                 #   in Loop: Header=BB0_6 Depth=1
	cmpq	%r15, %rbx
	jge	.LBB0_47
# BB#10:                                # %._crit_edge142
                                        #   in Loop: Header=BB0_6 Depth=1
	incq	%r13
	movq	(%r14,%rbx,8), %rbp
	movl	$.L.str, %esi
	movl	$9, %edx
	movq	%rbp, %rdi
	callq	strncmp
	incq	%rbx
	testl	%eax, %eax
	jne	.LBB0_6
	jmp	.LBB0_12
.LBB0_4:
	xorl	%r13d, %r13d
	jmp	.LBB0_12
.LBB0_11:                               # %.._crit_edge149.loopexit_crit_edge
	decq	%rbx
	movq	%rbx, %r13
.LBB0_12:                               # %._crit_edge149
	leal	3(%r13), %eax
	cmpl	$2, %eax
	jb	.LBB0_47
# BB#13:
	movl	%r13d, %ebx
	shrl	$31, %ebx
	addl	%r13d, %ebx
	movq	cube+88(%rip), %rbp
	movl	(%rbp), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB0_15
# BB#14:
	movl	$8, %edi
	jmp	.LBB0_16
.LBB0_15:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB0_16:
	sarl	%ebx
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbp, %rsi
	callq	set_copy
	movq	%rax, %r15
	movq	cube+88(%rip), %rbp
	movl	(%rbp), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB0_18
# BB#17:
	movl	$8, %edi
	jmp	.LBB0_19
.LBB0_18:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB0_19:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbp, %rsi
	callq	set_copy
	movq	%rax, %rbp
	leal	(%rbx,%rbx), %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%eax, %ecx
	andl	$30, %ecx
	movl	$1, %r14d
	movl	$1, %eax
	shll	%cl, %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	$-2, %eax
	movl	$-2, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	roll	%cl, %edx
	movl	%ebx, %ecx
	sarl	$4, %ecx
	movslq	%ecx, %r13
	andl	%edx, 4(%r15,%r13,4)
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	leal	1(%rbx,%rbx), %ecx
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	andl	$31, %ecx
	shll	%cl, %r14d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	roll	%cl, %eax
	andl	%eax, 4(%rbp,%r13,4)
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	movq	(%r12), %rdi
	xorl	%eax, %eax
	callq	cube1list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r15, %rsi
	callq	cofactor
	movq	%rax, %rcx
	leaq	16(%rsp), %rsi
	leaq	48(%rsp), %rdx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	simp_comp
	cmpl	$0, trace(%rip)
	je	.LBB0_21
# BB#20:
	movq	%rbp, %r15
	movq	16(%rsp), %rbp
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%rbx, %rcx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rbp
	movq	%rcx, %rdx
	callq	print_trace
.LBB0_21:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r15
	movq	(%r12), %rdi
	xorl	%eax, %eax
	callq	cube1list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbp, %rsi
	callq	cofactor
	movq	%rax, %rcx
	leaq	8(%rsp), %rsi
	leaq	40(%rsp), %rdx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	simp_comp
	cmpl	$0, trace(%rip)
	je	.LBB0_23
# BB#22:
	movq	8(%rsp), %rbp
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r15, %rcx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB0_23:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbp
	movq	16(%rsp), %rdi
	movq	40(%rsp), %rsi
	xorl	%eax, %eax
	callq	cv_intersect
	movq	%rax, %rbx
	cmpl	$0, trace(%rip)
	je	.LBB0_25
# BB#24:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%rbp, %rcx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB0_25:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r15
	movq	8(%rsp), %rdi
	movq	48(%rsp), %rsi
	xorl	%eax, %eax
	callq	cv_intersect
	movq	%rax, %rbp
	cmpl	$0, trace(%rip)
	je	.LBB0_27
# BB#26:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r15, %rcx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB0_27:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r15
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	sf_union
	movq	%rax, %rbp
	cmpl	$0, trace(%rip)
	je	.LBB0_29
# BB#28:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r15, %rcx
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB0_29:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r15
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	cube1list
	movq	%rax, %rcx
	leaq	8(%r12), %rbp
	leaq	32(%rsp), %rdx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbp, %rsi
	callq	simp_comp
	cmpl	$0, trace(%rip)
	je	.LBB0_31
# BB#30:
	movq	(%rbp), %rbp
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r15, %rcx
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB0_31:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r15
	movq	(%r12), %rdi
	movq	32(%rsp), %rsi
	xorl	%eax, %eax
	callq	cv_intersect
	movq	%rax, %rbp
	cmpl	$0, trace(%rip)
	je	.LBB0_33
# BB#32:
	movq	(%r12), %rbx
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r15, %rcx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB0_33:
	movq	(%r12), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	%rbp, (%r12)
	movq	16(%rsp), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	48(%rsp), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	40(%rsp), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	32(%rsp), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	(%r12), %rdi
	xorl	%eax, %eax
	callq	sf_active
	movq	(%r12), %rdi
	movl	(%rdi), %eax
	movl	12(%rdi), %edx
	imull	%eax, %edx
	testl	%edx, %edx
	jle	.LBB0_38
# BB#34:                                # %.lr.ph131
	incq	%r13
	movq	24(%rdi), %rcx
	movslq	%edx, %rdx
	leaq	(%rcx,%rdx,4), %rdx
	orl	24(%rsp), %r14d         # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB0_35:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%r13,4), %esi
	andl	%r14d, %esi
	cmpl	%r14d, %esi
	je	.LBB0_37
# BB#36:                                #   in Loop: Header=BB0_35 Depth=1
	andb	$-33, 1(%rcx)
	movl	(%rdi), %eax
.LBB0_37:                               #   in Loop: Header=BB0_35 Depth=1
	movslq	%eax, %rsi
	leaq	(%rcx,%rsi,4), %rcx
	cmpq	%rdx, %rcx
	jb	.LBB0_35
.LBB0_38:                               # %._crit_edge132
	xorl	%eax, %eax
	callq	sf_inactive
	movq	%rax, (%r12)
	xorl	%eax, %eax
	callq	setdown_cube
	movq	56(%rsp), %rbx          # 8-byte Reload
	leal	2(%rbx), %edx
	movslq	cube(%rip), %rax
	cmpl	%eax, %edx
	movq	64(%rsp), %r8           # 8-byte Reload
	jge	.LBB0_43
# BB#39:                                # %.lr.ph128
	movslq	%edx, %rcx
	movl	%eax, %esi
	subl	%edx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$3, %rsi
	je	.LBB0_42
# BB#40:                                # %.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB0_41:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rdi
	movq	(%rdi,%rcx,8), %rbp
	movq	%rbp, -16(%rdi,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_41
.LBB0_42:                               # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB0_43
	.p2align	4, 0x90
.LBB0_48:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rdx
	movq	(%rdx,%rcx,8), %rsi
	movq	%rsi, -16(%rdx,%rcx,8)
	movq	56(%r12), %rdx
	movq	8(%rdx,%rcx,8), %rsi
	movq	%rsi, -8(%rdx,%rcx,8)
	movq	56(%r12), %rdx
	movq	16(%rdx,%rcx,8), %rsi
	movq	%rsi, (%rdx,%rcx,8)
	movq	56(%r12), %rdx
	movq	24(%rdx,%rcx,8), %rsi
	movq	%rsi, 8(%rdx,%rcx,8)
	addq	$4, %rcx
	cmpq	%rax, %rcx
	jl	.LBB0_48
.LBB0_43:                               # %.preheader
	leal	1(%r8), %ecx
	movl	cube+4(%rip), %eax
	cmpl	%eax, %ecx
	jge	.LBB0_46
# BB#44:                                # %.lr.ph
	movq	cube+32(%rip), %rcx
	movslq	%r8d, %rdx
	incq	%rdx
	.p2align	4, 0x90
.LBB0_45:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rdx,4), %eax
	movl	%eax, -4(%rcx,%rdx,4)
	incq	%rdx
	movslq	cube+4(%rip), %rax
	cmpq	%rax, %rdx
	jl	.LBB0_45
.LBB0_46:                               # %._crit_edge
	decl	cube+8(%rip)
	decl	%eax
	movl	%eax, cube+4(%rip)
	xorl	%eax, %eax
	callq	cube_setup
	movq	(%r12), %rdi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movl	28(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %edx
	callq	sf_delc
	movq	%rax, (%r12)
	movq	8(%r12), %rdi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movl	%ebp, %edx
	callq	sf_delc
	movq	%rax, 8(%r12)
.LBB0_47:                               # %.thread
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	map_dcset, .Lfunc_end0-map_dcset
	.cfi_endproc

	.globl	map_output_symbolic
	.p2align	4, 0x90
	.type	map_output_symbolic,@function
map_output_symbolic:                    # @map_output_symbolic
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 96
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	8(%r15), %rax
	cmpl	$0, 12(%rax)
	jle	.LBB1_2
# BB#1:
	movq	(%r15), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	8(%r15), %rdi
	movq	16(%r15), %rsi
	xorl	%eax, %eax
	callq	cube2list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	complement
	movq	%rax, (%r15)
.LBB1_2:
	movq	72(%r15), %rbx
	xorl	%r13d, %r13d
	testq	%rbx, %rbx
	je	.LBB1_3
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph145
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_14 Depth 2
	movq	(%rbx), %rbp
	testq	%rbp, %rbp
	jne	.LBB1_14
	jmp	.LBB1_5
	.p2align	4, 0x90
.LBB1_17:                               #   in Loop: Header=BB1_14 Depth=2
	movq	8(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB1_5
.LBB1_14:                               # %.lr.ph139
                                        #   Parent Loop BB1_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rbp), %eax
	testl	%eax, %eax
	js	.LBB1_16
# BB#15:                                #   in Loop: Header=BB1_14 Depth=2
	movq	cube+32(%rip), %rcx
	movslq	cube+124(%rip), %rdx
	cmpl	(%rcx,%rdx,4), %eax
	jl	.LBB1_17
.LBB1_16:                               #   in Loop: Header=BB1_14 Depth=2
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	fatal
	jmp	.LBB1_17
	.p2align	4, 0x90
.LBB1_5:                                # %._crit_edge140
                                        #   in Loop: Header=BB1_12 Depth=1
	movb	8(%rbx), %cl
	movl	$1, %eax
	shll	%cl, %eax
	addl	%eax, %r13d
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_12
# BB#6:                                 # %.preheader
	movq	72(%r15), %rax
	testq	%rax, %rax
	jne	.LBB1_8
	jmp	.LBB1_3
	.p2align	4, 0x90
.LBB1_11:                               # %._crit_edge131
                                        #   in Loop: Header=BB1_8 Depth=1
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.LBB1_3
.LBB1_8:                                # %.lr.ph134
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_9 Depth 2
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB1_11
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph130
                                        #   Parent Loop BB1_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	%r13d, 4(%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_9
	jmp	.LBB1_11
.LBB1_3:                                # %._crit_edge135
	movl	cube(%rip), %r14d
	movq	cube+32(%rip), %rax
	movslq	cube+124(%rip), %rcx
	addl	%r13d, (%rax,%rcx,4)
	xorl	%eax, %eax
	callq	setdown_cube
	xorl	%eax, %eax
	callq	cube_setup
	movq	cube+16(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	(%rax,%rcx,4), %ebx
	movq	(%r15), %rdi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movl	%r13d, %edx
	callq	sf_addcol
	movq	%rax, (%r15)
	movq	8(%r15), %rdi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movl	%r13d, %edx
	callq	sf_addcol
	movq	%rax, 8(%r15)
	movq	16(%r15), %rdi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movl	%r13d, %edx
	callq	sf_addcol
	movq	%rax, 16(%r15)
	movq	72(%r15), %rbp
	testq	%rbp, %rbp
	je	.LBB1_4
# BB#18:                                # %.lr.ph125
	movl	%r14d, 28(%rsp)         # 4-byte Spill
	leaq	32(%rsp), %r14
	leaq	16(%rsp), %r12
	.p2align	4, 0x90
.LBB1_19:                               # =>This Inner Loop Header: Depth=1
	movl	cube(%rip), %esi
	movl	$100, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, 16(%rsp)
	movl	cube(%rip), %esi
	movl	$100, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, 32(%rsp)
	movq	(%rbp), %rdx
	movq	%r14, (%rsp)
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movl	%ebx, %ecx
	movq	%r12, %r9
	callq	find_inputs
	movq	(%r15), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	16(%rsp), %rax
	movq	%rax, (%r15)
	movq	32(%rsp), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movzbl	8(%rbp), %ecx
	movl	$1, %eax
	shll	%cl, %eax
	addl	%eax, %ebx
	movq	32(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB1_19
# BB#20:                                # %._crit_edge126.loopexit
	movq	16(%rsp), %rbx
	movl	28(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB1_21
.LBB1_4:
                                        # implicit-def: %RBX
.LBB1_21:                               # %._crit_edge126
	movl	4(%rbx), %eax
	cmpl	$33, %eax
	jge	.LBB1_23
# BB#22:
	movl	$8, %edi
	jmp	.LBB1_24
.LBB1_23:
	decl	%eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB1_24:
	callq	malloc
	movq	%rax, %rcx
	movl	4(%rbx), %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	set_fill
	movq	%rax, %rbx
	movq	72(%r15), %rax
	testq	%rax, %rax
	je	.LBB1_30
# BB#25:                                # %.lr.ph119
	movq	cube+16(%rip), %rdx
	.p2align	4, 0x90
.LBB1_26:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_27 Depth 2
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB1_29
	.p2align	4, 0x90
.LBB1_27:                               # %.lr.ph114
                                        #   Parent Loop BB1_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	cube+124(%rip), %rdi
	movl	4(%rsi), %ecx
	addl	(%rdx,%rdi,4), %ecx
	movl	$-2, %edi
	roll	%cl, %edi
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	andl	%edi, 4(%rbx,%rcx,4)
	movq	8(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB1_27
.LBB1_29:                               # %._crit_edge115
                                        #   in Loop: Header=BB1_26 Depth=1
	movq	32(%rax), %rax
	testq	%rax, %rax
	jne	.LBB1_26
.LBB1_30:                               # %._crit_edge120
	movq	16(%rsp), %rax
	movl	4(%rax), %ebp
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_ord
	movq	cube+32(%rip), %rcx
	movslq	cube+124(%rip), %rdx
	subl	%ebp, %eax
	addl	%eax, (%rcx,%rdx,4)
	xorl	%eax, %eax
	callq	setdown_cube
	xorl	%eax, %eax
	callq	cube_setup
	movq	(%r15), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	sf_compress
	movq	%rax, (%r15)
	movq	8(%r15), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	sf_compress
	movq	%rax, 8(%r15)
	movl	cube(%rip), %eax
	movq	(%r15), %rdi
	cmpl	4(%rdi), %eax
	je	.LBB1_32
# BB#31:
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	fatal
	movq	(%r15), %rdi
.LBB1_32:
	xorl	%eax, %eax
	callq	sf_contain
	movq	%rax, (%r15)
	movq	8(%r15), %rdi
	xorl	%eax, %eax
	callq	sf_contain
	movq	%rax, 8(%r15)
	movq	(%r15), %rdi
	cmpl	$0, cube+4(%rip)
	jle	.LBB1_35
# BB#33:                                # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_34:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	d1merge
	movq	%rax, (%r15)
	movq	8(%r15), %rdi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	d1merge
	movq	%rax, 8(%r15)
	incl	%ebp
	movq	(%r15), %rdi
	cmpl	cube+4(%rip), %ebp
	jl	.LBB1_34
.LBB1_35:                               # %._crit_edge
	xorl	%eax, %eax
	callq	sf_contain
	movq	%rax, (%r15)
	movq	8(%r15), %rdi
	xorl	%eax, %eax
	callq	sf_contain
	movq	%rax, 8(%r15)
	movq	16(%r15), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movl	cube(%rip), %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, 16(%r15)
	movq	72(%r15), %rsi
	movl	cube(%rip), %ecx
	movq	%r15, %rdi
	movq	%rbx, %rdx
	movl	%r14d, %r8d
	movl	%r13d, %r9d
	callq	symbolic_hack_labels
	testq	%rbx, %rbx
	je	.LBB1_37
# BB#36:
	movq	%rbx, %rdi
	callq	free
.LBB1_37:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	map_output_symbolic, .Lfunc_end1-map_output_symbolic
	.cfi_endproc

	.globl	find_inputs
	.p2align	4, 0x90
	.type	find_inputs,@function
find_inputs:                            # @find_inputs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 96
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movl	%r8d, %ebp
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r13
	testq	%r15, %r15
	je	.LBB2_1
# BB#5:
	movq	96(%rsp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	16(%r12), %rdi
	movq	cube+16(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	4(%r15), %esi
	addl	(%rax,%rcx,4), %esi
	xorl	%eax, %eax
	callq	cof_output
	movq	%rax, %r14
	testq	%r13, %r13
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	je	.LBB2_7
# BB#6:
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	cv_intersect
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_free
	movq	%rbx, %r14
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB2_7:
	movq	8(%r15), %rdx
	addl	%ebp, %ebp
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%r14, %rdi
	movq	%r12, %rsi
	movl	20(%rsp), %ecx          # 4-byte Reload
	movl	%ebp, %r8d
	movq	%rbx, %r9
	callq	find_inputs
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_free
	movq	(%r12), %rdi
	movq	cube+16(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	4(%r15), %esi
	addl	(%rax,%rcx,4), %esi
	xorl	%eax, %eax
	callq	cof_output
	movq	%rax, %rbx
	testq	%r13, %r13
	je	.LBB2_9
# BB#8:
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	cv_intersect
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
	movq	%r14, %rbx
.LBB2_9:
	movq	8(%r15), %rdx
	orl	$1, %ebp
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	20(%rsp), %ecx          # 4-byte Reload
	movl	%ebp, %r8d
	movq	24(%rsp), %r9           # 8-byte Reload
	callq	find_inputs
	xorl	%eax, %eax
	movq	%rbx, %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sf_free                 # TAILCALL
.LBB2_1:
	movq	(%r12), %rsi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	cv_intersect
	movq	%rax, %rdx
	movslq	(%rdx), %rax
	movslq	12(%rdx), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB2_4
# BB#2:                                 # %.lr.ph
	movq	24(%rdx), %rax
	leaq	(%rax,%rcx,4), %rsi
	addl	20(%rsp), %ebp          # 4-byte Folded Reload
	movl	$1, %edi
	movl	%ebp, %ecx
	shll	%cl, %edi
	sarl	$5, %ebp
	incl	%ebp
	movslq	%ebp, %rcx
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	orl	%edi, (%rax,%rcx,4)
	movslq	(%rdx), %rbp
	leaq	(%rax,%rbp,4), %rax
	cmpq	%rsi, %rax
	jb	.LBB2_3
.LBB2_4:                                # %._crit_edge
	movq	(%rbx), %rdi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	callq	sf_append
	movq	%rax, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	find_inputs, .Lfunc_end2-find_inputs
	.cfi_endproc

	.globl	map_symbolic
	.p2align	4, 0x90
	.type	map_symbolic,@function
map_symbolic:                           # @map_symbolic
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 112
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rdi, (%rsp)            # 8-byte Spill
	movq	64(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB3_10
	.p2align	4, 0x90
.LBB3_1:                                # %.lr.ph176
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_3 Depth 2
	movq	(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB3_6
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph171
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp), %eax
	testl	%eax, %eax
	js	.LBB3_5
# BB#4:                                 # %.lr.ph171
                                        #   in Loop: Header=BB3_3 Depth=2
	cmpl	cube+8(%rip), %eax
	jl	.LBB3_2
.LBB3_5:                                #   in Loop: Header=BB3_3 Depth=2
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB3_2:                                #   in Loop: Header=BB3_3 Depth=2
	movq	8(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_3
.LBB3_6:                                # %._crit_edge172
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_1
# BB#7:                                 # %.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.LBB3_10
# BB#8:                                 # %.lr.ph164.preheader
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_9:                                # %.lr.ph164
                                        # =>This Inner Loop Header: Depth=1
	movzbl	8(%rax), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	addl	%edx, %ebp
	incl	%ebx
	movq	32(%rax), %rax
	testq	%rax, %rax
	jne	.LBB3_9
	jmp	.LBB3_11
.LBB3_10:
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
.LBB3_11:                               # %._crit_edge165
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %r14
	movl	4(%r14), %eax
	addl	%ebp, %eax
	cmpl	$33, %eax
	jge	.LBB3_13
# BB#12:
	movl	$8, %edi
	jmp	.LBB3_14
.LBB3_13:
	decl	%eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB3_14:
	callq	malloc
	movq	%rax, %rcx
	movl	4(%r14), %esi
	addl	%ebp, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	set_fill
	movq	%rax, %r13
	movq	(%rsp), %rax            # 8-byte Reload
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.LBB3_18
	.p2align	4, 0x90
.LBB3_16:                               # %.lr.ph158
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_17 Depth 2
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB3_15
	.p2align	4, 0x90
.LBB3_17:                               # %.lr.ph153
                                        #   Parent Loop BB3_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %esi
	movl	%esi, %ecx
	addb	%cl, %cl
	andb	$30, %cl
	movl	$-2, %edi
	roll	%cl, %edi
	sarl	$4, %esi
	movslq	%esi, %rcx
	andl	%edi, 4(%r13,%rcx,4)
	movl	(%rdx), %esi
	movl	%esi, %ecx
	andl	$15, %ecx
	leal	1(%rcx,%rcx), %ecx
	movl	$-2, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	roll	%cl, %edi
	sarl	$4, %esi
	movslq	%esi, %rcx
	andl	%edi, 4(%r13,%rcx,4)
	movq	8(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_17
.LBB3_15:                               # %._crit_edge154
                                        #   in Loop: Header=BB3_16 Depth=1
	movq	32(%rax), %rax
	testq	%rax, %rax
	jne	.LBB3_16
.LBB3_18:                               # %._crit_edge159
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rax
	movl	4(%rax), %r14d
	addl	%ebp, %r14d
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	set_ord
	subl	%eax, %r14d
	movl	%r14d, %eax
	shrl	$31, %eax
	addl	%r14d, %eax
	movl	%eax, %edx
	sarl	%edx
	movslq	cube+4(%rip), %r15
	movl	%r15d, %ecx
	movl	%edx, 8(%rsp)           # 4-byte Spill
	subl	%edx, %ecx
	addl	%ebx, %ecx
	movslq	cube+8(%rip), %r12
	movl	cube(%rip), %r14d
	andl	$-2, %eax
	movl	%ebp, %edx
	subl	%eax, %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	movslq	%ecx, %rbx
	leaq	(,%rbx,4), %rdi
	callq	malloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	cube+32(%rip), %rcx
	movl	-4(%rcx,%r15,4), %edx
	decq	%r15
	movl	%edx, -4(%rax,%rbx,4)
	cmpl	%r15d, %r12d
	jge	.LBB3_35
# BB#19:                                # %.lr.ph148
	movslq	8(%rsp), %r8            # 4-byte Folded Reload
	movq	%r15, %r10
	subq	%r12, %r10
	cmpq	$8, %r10
	jae	.LBB3_21
# BB#20:
	movq	%r12, %rdx
	jmp	.LBB3_33
.LBB3_21:                               # %min.iters.checked
	movq	%r10, %rdi
	andq	$-8, %rdi
	je	.LBB3_25
# BB#22:                                # %vector.memcheck
	leaq	(,%r12,4), %rbx
	leaq	(,%r8,4), %rdx
	subq	%rdx, %rbx
	addq	%rax, %rbx
	leaq	(%rcx,%r15,4), %rsi
	cmpq	%rsi, %rbx
	jae	.LBB3_26
# BB#23:                                # %vector.memcheck
	leaq	(,%r15,4), %rsi
	subq	%rdx, %rsi
	addq	%rax, %rsi
	leaq	(%rcx,%r12,4), %rdx
	cmpq	%rsi, %rdx
	jae	.LBB3_26
.LBB3_25:
	movq	%r12, %rdx
	jmp	.LBB3_33
.LBB3_26:                               # %vector.body.preheader
	leaq	-8(%rdi), %rdx
	movq	%rdx, %r9
	shrq	$3, %r9
	btl	$3, %edx
	jb	.LBB3_28
# BB#27:                                # %vector.body.prol
	movups	(%rcx,%r12,4), %xmm0
	movups	16(%rcx,%r12,4), %xmm1
	movq	%r12, %rdx
	subq	%r8, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movups	%xmm0, (%rsi,%rdx,4)
	movups	%xmm1, 16(%rsi,%rdx,4)
	movl	$8, %edx
	testq	%r9, %r9
	jne	.LBB3_29
	jmp	.LBB3_31
.LBB3_28:
	xorl	%edx, %edx
	testq	%r9, %r9
	je	.LBB3_31
.LBB3_29:                               # %vector.body.preheader.new
	leaq	48(%rcx,%r12,4), %rsi
	.p2align	4, 0x90
.LBB3_30:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rsi,%rdx,4), %xmm0
	movups	-32(%rsi,%rdx,4), %xmm1
	movups	%xmm0, (%rbx,%rdx,4)
	movups	%xmm1, 16(%rbx,%rdx,4)
	movups	-16(%rsi,%rdx,4), %xmm0
	movups	(%rsi,%rdx,4), %xmm1
	movups	%xmm0, 32(%rbx,%rdx,4)
	movups	%xmm1, 48(%rbx,%rdx,4)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.LBB3_30
.LBB3_31:                               # %middle.block
	cmpq	%rdi, %r10
	je	.LBB3_35
# BB#32:
	movq	%r12, %rdx
	addq	%rdi, %rdx
.LBB3_33:                               # %scalar.ph.preheader
	shlq	$2, %r8
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB3_34:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rdx,4), %esi
	movl	%esi, (%rax,%rdx,4)
	incq	%rdx
	cmpq	%r15, %rdx
	jl	.LBB3_34
.LBB3_35:                               # %._crit_edge149
	movq	%r12, 40(%rsp)          # 8-byte Spill
	addl	%r14d, 12(%rsp)         # 4-byte Folded Spill
	movq	cube+16(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	(%rax,%rcx,4), %r12d
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	(%rbx), %rdi
	xorl	%eax, %eax
	movl	%r12d, %esi
	movl	%ebp, %edx
	callq	sf_addcol
	movq	%rax, (%rbx)
	movq	8(%rbx), %rdi
	xorl	%eax, %eax
	movl	%r12d, %esi
	movl	%ebp, %edx
	callq	sf_addcol
	movq	%rax, 8(%rbx)
	movq	16(%rbx), %rdi
	xorl	%eax, %eax
	movl	%r12d, %esi
	movl	%ebp, 36(%rsp)          # 4-byte Spill
	movl	%ebp, %edx
	callq	sf_addcol
	movq	%rax, 16(%rbx)
	movq	(%rbx), %r15
	movq	64(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB3_49
# BB#36:                                # %.lr.ph.preheader
	movl	8(%rsp), %eax           # 4-byte Reload
	notl	%eax
	addl	cube+4(%rip), %eax
	movslq	%eax, %rdi
	.p2align	4, 0x90
.LBB3_37:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_39 Depth 2
                                        #     Child Loop BB3_43 Depth 2
                                        #     Child Loop BB3_47 Depth 2
	movq	(%rsi), %rbp
	movslq	(%r15), %rcx
	movslq	12(%r15), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	jle	.LBB3_41
# BB#38:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB3_37 Depth=1
	movq	24(%r15), %r14
	leaq	(%r14,%rax,4), %rbx
	.p2align	4, 0x90
.LBB3_39:                               # %.lr.ph.i
                                        #   Parent Loop BB3_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	%r12d, %esi
	movq	%rbp, %rcx
	callq	form_bitvector
	movslq	(%r15), %rax
	leaq	(%r14,%rax,4), %r14
	cmpq	%rbx, %r14
	jb	.LBB3_39
# BB#40:                                # %map_symbolic_cover.exit.loopexit
                                        #   in Loop: Header=BB3_37 Depth=1
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi), %rbp
.LBB3_41:                               # %map_symbolic_cover.exit
                                        #   in Loop: Header=BB3_37 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%r15, (%rax)
	movq	8(%rax), %r14
	movslq	(%r14), %rcx
	movslq	12(%r14), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB3_45
# BB#42:                                # %.lr.ph.i135.preheader
                                        #   in Loop: Header=BB3_37 Depth=1
	movq	24(%r14), %rbx
	leaq	(%rbx,%rax,4), %r15
	.p2align	4, 0x90
.LBB3_43:                               # %.lr.ph.i135
                                        #   Parent Loop BB3_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movq	%rbp, %rcx
	callq	form_bitvector
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r15, %rbx
	jb	.LBB3_43
# BB#44:                                # %map_symbolic_cover.exit136.loopexit
                                        #   in Loop: Header=BB3_37 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rbp
.LBB3_45:                               # %map_symbolic_cover.exit136
                                        #   in Loop: Header=BB3_37 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%r14, 8(%rax)
	movq	16(%rax), %r14
	movslq	(%r14), %rcx
	movslq	12(%r14), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB3_48
# BB#46:                                # %.lr.ph.i138.preheader
                                        #   in Loop: Header=BB3_37 Depth=1
	movq	24(%r14), %rbx
	leaq	(%rbx,%rax,4), %r15
	.p2align	4, 0x90
.LBB3_47:                               # %.lr.ph.i138
                                        #   Parent Loop BB3_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movq	%rbp, %rcx
	callq	form_bitvector
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r15, %rbx
	jb	.LBB3_47
.LBB3_48:                               # %map_symbolic_cover.exit139
                                        #   in Loop: Header=BB3_37 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%r14, 16(%rdx)
	movq	24(%rsp), %rsi          # 8-byte Reload
	movb	8(%rsi), %cl
	movl	$1, %eax
	shll	%cl, %eax
	addl	%eax, %r12d
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%eax, (%rcx,%rdi,4)
	incq	%rdi
	movq	32(%rsi), %rsi
	movq	(%rdx), %r15
	testq	%rsi, %rsi
	jne	.LBB3_37
.LBB3_49:                               # %._crit_edge
	movq	40(%rsp), %rbp          # 8-byte Reload
	subl	8(%rsp), %ebp           # 4-byte Folded Reload
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	sf_compress
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rax, (%rbx)
	movq	8(%rbx), %rdi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	sf_compress
	movq	%rax, 8(%rbx)
	movq	16(%rbx), %rdi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	sf_compress
	movq	%rax, 16(%rbx)
	movq	64(%rbx), %rsi
	movl	cube(%rip), %r8d
	movq	%rbx, %rdi
	movq	%r13, %rdx
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	36(%rsp), %r9d          # 4-byte Reload
	callq	symbolic_hack_labels
	xorl	%eax, %eax
	callq	setdown_cube
	movq	cube+32(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_51
# BB#50:
	callq	free
	movq	$0, cube+32(%rip)
.LBB3_51:
	movl	32(%rsp), %eax          # 4-byte Reload
	movl	%eax, cube+4(%rip)
	movl	%ebp, cube+8(%rip)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, cube+32(%rip)
	xorl	%eax, %eax
	callq	cube_setup
	testq	%r13, %r13
	je	.LBB3_53
# BB#52:
	movq	%r13, %rdi
	callq	free
.LBB3_53:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	map_symbolic, .Lfunc_end3-map_symbolic
	.cfi_endproc

	.globl	map_symbolic_cover
	.p2align	4, 0x90
	.type	map_symbolic_cover,@function
map_symbolic_cover:                     # @map_symbolic_cover
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 48
.Lcfi57:
	.cfi_offset %rbx, -48
.Lcfi58:
	.cfi_offset %r12, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %r12
	movslq	(%r12), %rcx
	movslq	12(%r12), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB4_3
# BB#1:                                 # %.lr.ph.preheader
	movq	24(%r12), %rbp
	leaq	(%rbp,%rax,4), %rbx
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	movq	%rbp, %rdi
	movl	%r14d, %esi
	movq	%r15, %rcx
	callq	form_bitvector
	movslq	(%r12), %rax
	leaq	(%rbp,%rax,4), %rbp
	cmpq	%rbx, %rbp
	jb	.LBB4_2
.LBB4_3:                                # %._crit_edge
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	map_symbolic_cover, .Lfunc_end4-map_symbolic_cover
	.cfi_endproc

	.globl	form_bitvector
	.p2align	4, 0x90
	.type	form_bitvector,@function
form_bitvector:                         # @form_bitvector
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi66:
	.cfi_def_cfa_offset 48
.Lcfi67:
	.cfi_offset %rbx, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movl	%edx, %ebx
	movl	%esi, %r15d
	movq	%rdi, %r14
	testq	%rbp, %rbp
	jne	.LBB5_3
	jmp	.LBB5_2
	.p2align	4, 0x90
.LBB5_7:                                #   in Loop: Header=BB5_3 Depth=1
	leal	1(%rbx,%rbx), %ebx
	addq	$8, %rbp
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB5_2
.LBB5_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %ecx
	movl	%ecx, %eax
	sarl	$4, %eax
	cltq
	movl	4(%r14,%rax,4), %eax
	addb	%cl, %cl
	andb	$30, %cl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	andb	$3, %al
	cmpb	$3, %al
	je	.LBB5_8
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpb	$2, %al
	je	.LBB5_7
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpb	$1, %al
	jne	.LBB5_10
# BB#6:                                 #   in Loop: Header=BB5_3 Depth=1
	addl	%ebx, %ebx
	addq	$8, %rbp
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB5_3
	jmp	.LBB5_2
	.p2align	4, 0x90
.LBB5_8:                                #   in Loop: Header=BB5_3 Depth=1
	leal	(%rbx,%rbx), %edx
	movq	8(%rbp), %rcx
	addq	$8, %rbp
	movq	%r14, %rdi
	movl	%r15d, %esi
	callq	form_bitvector
	leal	1(%rbx,%rbx), %ebx
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB5_3
.LBB5_2:                                # %tailrecurse._crit_edge
	addl	%r15d, %ebx
	movl	$1, %eax
	movl	%ebx, %ecx
	shll	%cl, %eax
	sarl	$5, %ebx
	movslq	%ebx, %rcx
	orl	%eax, 4(%r14,%rcx,4)
	jmp	.LBB5_11
.LBB5_10:
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB5_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	form_bitvector, .Lfunc_end5-form_bitvector
	.cfi_endproc

	.globl	symbolic_hack_labels
	.p2align	4, 0x90
	.type	symbolic_hack_labels,@function
symbolic_hack_labels:                   # @symbolic_hack_labels
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi74:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi77:
	.cfi_def_cfa_offset 96
.Lcfi78:
	.cfi_offset %rbx, -56
.Lcfi79:
	.cfi_offset %r12, -48
.Lcfi80:
	.cfi_offset %r13, -40
.Lcfi81:
	.cfi_offset %r14, -32
.Lcfi82:
	.cfi_offset %r15, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movl	%r8d, %r13d
	movl	%ecx, %r15d
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %rbp
	movq	56(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_35
# BB#1:
	movslq	%r15d, %rbx
	leaq	(,%rbx,8), %rdi
	callq	malloc
	movq	%rax, 56(%rbp)
	testl	%ebx, %ebx
	jle	.LBB6_9
# BB#2:                                 # %.lr.ph101.preheader
	movq	$0, (%rax)
	cmpl	$1, %r15d
	je	.LBB6_9
# BB#3:                                 # %.lr.ph101..lr.ph101_crit_edge.preheader
	movl	%r15d, %eax
	leal	7(%rax), %esi
	leaq	-2(%rax), %rdx
	andq	$7, %rsi
	je	.LBB6_4
# BB#5:                                 # %.lr.ph101..lr.ph101_crit_edge.prol.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph101..lr.ph101_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%rbp), %rdi
	movq	$0, 8(%rdi,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB6_6
# BB#7:                                 # %.lr.ph101..lr.ph101_crit_edge.prol.loopexit.unr-lcssa
	incq	%rcx
	cmpq	$7, %rdx
	jae	.LBB6_36
	jmp	.LBB6_9
.LBB6_4:
	movl	$1, %ecx
	cmpq	$7, %rdx
	jb	.LBB6_9
	.p2align	4, 0x90
.LBB6_36:                               # %.lr.ph101..lr.ph101_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movq	56(%rbp), %rdx
	movq	$0, 8(%rdx,%rcx,8)
	movq	56(%rbp), %rdx
	movq	$0, 16(%rdx,%rcx,8)
	movq	56(%rbp), %rdx
	movq	$0, 24(%rdx,%rcx,8)
	movq	56(%rbp), %rdx
	movq	$0, 32(%rdx,%rcx,8)
	movq	56(%rbp), %rdx
	movq	$0, 40(%rdx,%rcx,8)
	movq	56(%rbp), %rdx
	movq	$0, 48(%rdx,%rcx,8)
	movq	56(%rbp), %rdx
	movq	$0, 56(%rdx,%rcx,8)
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB6_36
.LBB6_9:                                # %.preheader81
	movq	cube+16(%rip), %rcx
	movslq	cube+124(%rip), %rax
	cmpl	$0, (%rcx,%rax,4)
	jle	.LBB6_10
# BB#17:                                # %.lr.ph97.preheader
	xorl	%ebx, %ebx
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movq	24(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_18:                               # %.lr.ph97
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%r15,%rcx,4), %ecx
	btl	%ebx, %ecx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rbx,8), %rdi
	jae	.LBB6_20
# BB#19:                                #   in Loop: Header=BB6_18 Depth=1
	movq	56(%rbp), %rcx
	movl	12(%rsp), %esi          # 4-byte Reload
	movslq	%esi, %rdx
	incl	%esi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movq	%rdi, (%rcx,%rdx,8)
	jmp	.LBB6_22
	.p2align	4, 0x90
.LBB6_20:                               #   in Loop: Header=BB6_18 Depth=1
	testq	%rdi, %rdi
	je	.LBB6_22
# BB#21:                                #   in Loop: Header=BB6_18 Depth=1
	callq	free
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax,%rbx,8)
	movl	cube+124(%rip), %eax
.LBB6_22:                               #   in Loop: Header=BB6_18 Depth=1
	incq	%rbx
	movq	cube+16(%rip), %rcx
	movslq	%eax, %rdx
	movslq	(%rcx,%rdx,4), %rdx
	cmpq	%rdx, %rbx
	jl	.LBB6_18
	jmp	.LBB6_11
.LBB6_10:
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movq	24(%rsp), %r15          # 8-byte Reload
.LBB6_11:                               # %.preheader
	testq	%r14, %r14
	je	.LBB6_27
# BB#12:
	movl	%r13d, 36(%rsp)         # 4-byte Spill
	movl	%r12d, %ebx
	.p2align	4, 0x90
.LBB6_13:                               # %.lr.ph92
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_15 Depth 2
	movl	8(%r14), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	cmpl	$31, %ecx
	je	.LBB6_25
# BB#14:                                # %.lr.ph87.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	16(%r14), %rax
	movslq	12(%rsp), %r12          # 4-byte Folded Reload
	shlq	$3, %r12
	xorl	%r15d, %r15d
	movq	%rax, %r13
	.p2align	4, 0x90
.LBB6_15:                               # %.lr.ph87
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	je	.LBB6_16
# BB#23:                                #   in Loop: Header=BB6_15 Depth=2
	movq	(%r13), %rdx
	movq	56(%rbp), %rsi
	addq	%r12, %rsi
	movq	%rdx, (%rsi,%r15,8)
	movq	8(%rax), %r13
	movq	%r13, %rax
	jmp	.LBB6_24
	.p2align	4, 0x90
.LBB6_16:                               #   in Loop: Header=BB6_15 Depth=2
	movl	$10, %edi
	callq	malloc
	movq	56(%rbp), %rcx
	addq	%r12, %rcx
	movq	%rax, (%rcx,%r15,8)
	movq	56(%rbp), %rax
	addq	%r12, %rax
	movq	(%rax,%r15,8), %rdi
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	sprintf
	movl	8(%r14), %ecx
	xorl	%eax, %eax
.LBB6_24:                               #   in Loop: Header=BB6_15 Depth=2
	incq	%r15
	movl	$1, %edx
	shll	%cl, %edx
	movslq	%edx, %rsi
	cmpq	%rsi, %r15
	jl	.LBB6_15
.LBB6_25:                               # %._crit_edge88
                                        #   in Loop: Header=BB6_13 Depth=1
	addl	%edx, 12(%rsp)          # 4-byte Folded Spill
	movq	32(%r14), %r14
	testq	%r14, %r14
	jne	.LBB6_13
# BB#26:                                # %._crit_edge93.loopexit
	movq	cube+16(%rip), %rcx
	movl	cube+124(%rip), %eax
	movl	%ebx, %r12d
	movl	36(%rsp), %r13d         # 4-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
.LBB6_27:                               # %._crit_edge93
	movl	12(%rsp), %r14d         # 4-byte Reload
	cltq
	movslq	(%rcx,%rax,4), %rax
	cmpl	%r13d, %eax
	jge	.LBB6_34
# BB#28:                                # %.lr.ph.preheader
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rbx
	addl	%eax, %r12d
	subl	%eax, %r13d
	.p2align	4, 0x90
.LBB6_29:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%r12d, %eax
	sarl	$5, %eax
	cltq
	movl	4(%r15,%rax,4), %eax
	btl	%r12d, %eax
	movq	(%rbx), %rdi
	jae	.LBB6_31
# BB#30:                                #   in Loop: Header=BB6_29 Depth=1
	movq	56(%rbp), %rax
	movslq	%r14d, %rcx
	incl	%r14d
	movq	%rdi, (%rax,%rcx,8)
	jmp	.LBB6_33
	.p2align	4, 0x90
.LBB6_31:                               #   in Loop: Header=BB6_29 Depth=1
	testq	%rdi, %rdi
	je	.LBB6_33
# BB#32:                                #   in Loop: Header=BB6_29 Depth=1
	callq	free
	movq	$0, (%rbx)
.LBB6_33:                               #   in Loop: Header=BB6_29 Depth=1
	addq	$8, %rbx
	incl	%r12d
	decl	%r13d
	jne	.LBB6_29
.LBB6_34:                               # %._crit_edge
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
.LBB6_35:
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	symbolic_hack_labels, .Lfunc_end6-symbolic_hack_labels
	.cfi_endproc

	.globl	disassemble_fsm
	.p2align	4, 0x90
	.type	disassemble_fsm,@function
disassemble_fsm:                        # @disassemble_fsm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi90:
	.cfi_def_cfa_offset 176
.Lcfi91:
	.cfi_offset %rbx, -56
.Lcfi92:
	.cfi_offset %r12, -48
.Lcfi93:
	.cfi_offset %r13, -40
.Lcfi94:
	.cfi_offset %r14, -32
.Lcfi95:
	.cfi_offset %r15, -24
.Lcfi96:
	.cfi_offset %rbp, -16
	movl	%esi, 52(%rsp)          # 4-byte Spill
	movq	%rdi, %rbx
	movl	cube+4(%rip), %ecx
	movl	cube+8(%rip), %eax
	movl	%ecx, %edx
	subl	%eax, %edx
	cmpl	$2, %edx
	jne	.LBB7_1
.LBB7_2:
	movq	cube+32(%rip), %rdx
	movslq	%eax, %rsi
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movl	(%rdx,%rsi,4), %esi
	movslq	%ecx, %rcx
	movl	-4(%rdx,%rcx,4), %ecx
	movl	%esi, 4(%rsp)           # 4-byte Spill
	subl	%esi, %ecx
	movl	%ecx, 92(%rsp)          # 4-byte Spill
	jl	.LBB7_3
.LBB7_4:
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	cube+16(%rip), %rcx
	cltq
	movl	(%rcx,%rax,4), %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	cube(%rip), %ebx
	movl	$2, %eax
	cmpl	$33, %ebx
	jl	.LBB7_6
# BB#5:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	addl	$2, %eax
.LBB7_6:
	movslq	%eax, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB7_12
# BB#7:                                 # %.lr.ph284.preheader
	testb	$1, 4(%rsp)             # 1-byte Folded Reload
	je	.LBB7_9
# BB#8:                                 # %.lr.ph284.prol
	movl	$1, %ebp
	movl	$1, %eax
	movq	64(%rsp), %rcx          # 8-byte Reload
	shll	%cl, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	orl	%eax, 4(%rdx,%rcx,4)
.LBB7_9:                                # %.lr.ph284.prol.loopexit
	cmpl	$1, 4(%rsp)             # 4-byte Folded Reload
	movq	40(%rsp), %rdi          # 8-byte Reload
	je	.LBB7_12
# BB#10:                                # %.lr.ph284.preheader.new
	movl	4(%rsp), %edx           # 4-byte Reload
	subl	%ebp, %edx
	movq	64(%rsp), %rax          # 8-byte Reload
	leal	1(%rbp,%rax), %eax
	.p2align	4, 0x90
.LBB7_11:                               # %.lr.ph284
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%esi, 4(%rdi,%rcx,4)
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	movl	%eax, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%esi, 4(%rdi,%rcx,4)
	addl	$2, %eax
	addl	$-2, %edx
	jne	.LBB7_11
.LBB7_12:                               # %._crit_edge285
	movq	cube+16(%rip), %rax
	movslq	cube+8(%rip), %rcx
	movl	4(%rax,%rcx,4), %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	cube(%rip), %ebx
	movl	$2, %eax
	cmpl	$33, %ebx
	jl	.LBB7_14
# BB#13:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	addl	$2, %eax
.LBB7_14:
	movslq	%eax, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %rdi
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB7_20
# BB#15:                                # %.lr.ph280.preheader
	testb	$1, 4(%rsp)             # 1-byte Folded Reload
	je	.LBB7_17
# BB#16:                                # %.lr.ph280.prol
	movl	$1, %ebp
	movl	$1, %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	shll	%cl, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%eax, 4(%rdi,%rcx,4)
.LBB7_17:                               # %.lr.ph280.prol.loopexit
	cmpl	$1, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB7_20
# BB#18:                                # %.lr.ph280.preheader.new
	movl	4(%rsp), %edx           # 4-byte Reload
	subl	%ebp, %edx
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	1(%rbp,%rax), %eax
	.p2align	4, 0x90
.LBB7_19:                               # %.lr.ph280
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%esi, 4(%rdi,%rcx,4)
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	movl	%eax, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%esi, 4(%rdi,%rcx,4)
	addl	$2, %eax
	addl	$-2, %edx
	jne	.LBB7_19
.LBB7_20:                               # %._crit_edge281
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	jge	.LBB7_22
# BB#21:
	movl	$8, %edi
	jmp	.LBB7_23
.LBB7_22:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB7_23:
	movq	8(%rsp), %rbp           # 8-byte Reload
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	callq	set_or
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	cube(%rip), %esi
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r15
	movl	cube(%rip), %esi
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r13
	movq	(%rbp), %rax
	movq	24(%rax), %rbx
	movslq	(%rax), %rcx
	movslq	12(%rax), %rax
	imulq	%rcx, %rax
	leaq	(%rbx,%rax,4), %r12
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB7_35
# BB#24:                                # %.preheader233.lr.ph
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	je	.LBB7_28
# BB#25:                                # %.preheader233.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_26:                               # %.preheader233
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_51 Depth 2
                                        #     Child Loop BB7_60 Depth 2
	cmpq	%r12, %rbx
	movq	%r15, 16(%rsp)          # 8-byte Spill
	jae	.LBB7_27
# BB#50:                                # %.lr.ph264
                                        #   in Loop: Header=BB7_26 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%r14, %r15
	leal	(%r14,%rax), %ecx
	movl	%ecx, %eax
	sarl	$5, %eax
	incl	%eax
	movslq	%eax, %r14
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	.p2align	4, 0x90
.LBB7_51:                               #   Parent Loop BB7_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%eax, %eax
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	setp_implies
	testl	%eax, %eax
	je	.LBB7_54
# BB#52:                                #   in Loop: Header=BB7_51 Depth=2
	testl	(%rbx,%r14,4), %ebp
	je	.LBB7_54
# BB#53:                                #   in Loop: Header=BB7_51 Depth=2
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	sf_addset
	movq	%rax, %r13
.LBB7_54:                               #   in Loop: Header=BB7_51 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r12, %rbx
	jb	.LBB7_51
	jmp	.LBB7_55
	.p2align	4, 0x90
.LBB7_27:                               #   in Loop: Header=BB7_26 Depth=1
	movq	%r14, %r15
.LBB7_55:                               # %._crit_edge265
                                        #   in Loop: Header=BB7_26 Depth=1
	movl	12(%r13), %eax
	testl	%eax, %eax
	jle	.LBB7_56
# BB#57:                                #   in Loop: Header=BB7_26 Depth=1
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	cube(%rip), %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	cube1list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	complement
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	espresso
	movq	%rax, %rbp
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sf_free
	movslq	12(%rbp), %rbx
	movslq	(%rbp), %rcx
	imulq	%rbx, %rcx
	testl	%ecx, %ecx
	movq	%r15, %r14
	jle	.LBB7_58
# BB#59:                                # %.lr.ph269
                                        #   in Loop: Header=BB7_26 Depth=1
	movq	24(%rbp), %rax
	leaq	(%rax,%rcx,4), %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	leal	(%r14,%rcx), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	sarl	$5, %ecx
	incl	%ecx
	movslq	%ecx, %rcx
	movq	16(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_60:                               #   Parent Loop BB7_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	orl	%esi, (%rax,%rcx,4)
	movslq	(%rbp), %rbx
	leaq	(%rax,%rbx,4), %rax
	cmpq	%rdx, %rax
	jb	.LBB7_60
# BB#61:                                # %._crit_edge270.loopexit
                                        #   in Loop: Header=BB7_26 Depth=1
	movl	12(%rbp), %ebx
	jmp	.LBB7_62
	.p2align	4, 0x90
.LBB7_56:                               #   in Loop: Header=BB7_26 Depth=1
	movq	%r15, %r14
	movq	16(%rsp), %r15          # 8-byte Reload
	jmp	.LBB7_63
	.p2align	4, 0x90
.LBB7_58:                               #   in Loop: Header=BB7_26 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
.LBB7_62:                               # %._crit_edge270
                                        #   in Loop: Header=BB7_26 Depth=1
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	sf_append
	movq	%rax, %r15
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movl	28(%rsp), %edx          # 4-byte Reload
	movl	%ebx, %ecx
	callq	printf
.LBB7_63:                               #   in Loop: Header=BB7_26 Depth=1
	incl	%r14d
	movl	cube(%rip), %esi
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r13
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%rbp), %rax
	movq	24(%rax), %rbx
	movslq	(%rax), %rcx
	movslq	12(%rax), %rax
	imulq	%rcx, %rax
	leaq	(%rbx,%rax,4), %r12
	cmpl	4(%rsp), %r14d          # 4-byte Folded Reload
	jne	.LBB7_26
	jmp	.LBB7_35
.LBB7_28:                               # %.preheader233.us.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_29:                               # %.preheader233.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_44 Depth 2
                                        #     Child Loop BB7_49 Depth 2
	cmpq	%r12, %rbx
	movq	%r15, 16(%rsp)          # 8-byte Spill
	jae	.LBB7_30
# BB#43:                                # %.lr.ph264.us
                                        #   in Loop: Header=BB7_29 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	(%r14,%rax), %ecx
	movl	%ecx, %eax
	sarl	$5, %eax
	incl	%eax
	movslq	%eax, %r15
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	.p2align	4, 0x90
.LBB7_44:                               #   Parent Loop BB7_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%eax, %eax
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	setp_implies
	testl	%eax, %eax
	je	.LBB7_47
# BB#45:                                #   in Loop: Header=BB7_44 Depth=2
	testl	(%rbx,%r15,4), %ebp
	je	.LBB7_47
# BB#46:                                #   in Loop: Header=BB7_44 Depth=2
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	sf_addset
	movq	%rax, %r13
.LBB7_47:                               #   in Loop: Header=BB7_44 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r12, %rbx
	jb	.LBB7_44
.LBB7_30:                               # %._crit_edge265.us
                                        #   in Loop: Header=BB7_29 Depth=1
	cmpl	$0, 12(%r13)
	jle	.LBB7_31
# BB#32:                                #   in Loop: Header=BB7_29 Depth=1
	movl	cube(%rip), %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	cube1list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	complement
	movq	%rax, %rbp
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	espresso
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sf_free
	movslq	(%rbx), %rax
	movslq	12(%rbx), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	movq	16(%rsp), %rdi          # 8-byte Reload
	jle	.LBB7_33
# BB#48:                                # %.lr.ph269.us
                                        #   in Loop: Header=BB7_29 Depth=1
	movq	24(%rbx), %rax
	leaq	(%rax,%rcx,4), %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	leal	(%r14,%rcx), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	sarl	$5, %ecx
	incl	%ecx
	movslq	%ecx, %rcx
	.p2align	4, 0x90
.LBB7_49:                               #   Parent Loop BB7_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	orl	%esi, (%rax,%rcx,4)
	movslq	(%rbx), %rbp
	leaq	(%rax,%rbp,4), %rax
	cmpq	%rdx, %rax
	jb	.LBB7_49
.LBB7_33:                               # %._crit_edge270.us
                                        #   in Loop: Header=BB7_29 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	sf_append
	movq	%rax, %r15
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB7_34
	.p2align	4, 0x90
.LBB7_31:                               #   in Loop: Header=BB7_29 Depth=1
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB7_34:                               #   in Loop: Header=BB7_29 Depth=1
	incl	%r14d
	movl	cube(%rip), %esi
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r13
	movq	(%rbp), %rax
	movq	24(%rax), %rbx
	movslq	(%rax), %rcx
	movslq	12(%rax), %rax
	imulq	%rcx, %rax
	leaq	(%rbx,%rax,4), %r12
	cmpl	4(%rsp), %r14d          # 4-byte Folded Reload
	jne	.LBB7_29
	jmp	.LBB7_35
	.p2align	4, 0x90
.LBB7_38:                               #   in Loop: Header=BB7_35 Depth=1
	movq	(%rbp), %rax
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
.LBB7_35:                               # %.preheader232
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r12, %rbx
	jae	.LBB7_39
# BB#36:                                # %.lr.ph259
                                        #   in Loop: Header=BB7_35 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	setp_disjoint
	testl	%eax, %eax
	je	.LBB7_38
# BB#37:                                #   in Loop: Header=BB7_35 Depth=1
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	sf_addset
	movq	%rax, %r13
	jmp	.LBB7_38
.LBB7_39:                               # %._crit_edge260
	movl	12(%r13), %ebx
	movl	cube+8(%rip), %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%esi, %edx
	callq	unravel_range
	movq	%rax, %rcx
	movl	12(%rcx), %ebp
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rsi
	callq	sf_append
	movq	%rax, 72(%rsp)          # 8-byte Spill
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	je	.LBB7_41
# BB#40:
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movl	%ebp, %edx
	callq	printf
.LBB7_41:                               # %.preheader231
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB7_42
# BB#64:                                # %.lr.ph250.preheader
	xorl	%ecx, %ecx
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB7_65:                               # %.lr.ph250
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_66 Depth 2
                                        #       Child Loop BB7_68 Depth 3
                                        #       Child Loop BB7_82 Depth 3
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leal	(%rcx,%rax), %ecx
	movl	%ecx, %eax
	sarl	$5, %eax
	incl	%eax
	cltq
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB7_66:                               #   Parent Loop BB7_65 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_68 Depth 3
                                        #       Child Loop BB7_82 Depth 3
	movl	cube(%rip), %esi
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r14
	movq	(%rbp), %rax
	movslq	(%rax), %rdx
	movslq	12(%rax), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	movq	%r15, 80(%rsp)          # 8-byte Spill
	jle	.LBB7_77
# BB#67:                                # %.lr.ph242
                                        #   in Loop: Header=BB7_66 Depth=2
	movq	24(%rax), %r13
	leaq	(%r13,%rcx,4), %rbx
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	(%r15,%rax), %ecx
	movl	%ecx, %eax
	sarl	$5, %eax
	incl	%eax
	movslq	%eax, %r15
	movl	$1, %r12d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r12d
	jmp	.LBB7_68
	.p2align	4, 0x90
.LBB7_69:                               #   in Loop: Header=BB7_68 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	28(%rsp), %ecx          # 4-byte Reload
	testl	(%r13,%rax,4), %ecx
	je	.LBB7_76
# BB#70:                                #   in Loop: Header=BB7_68 Depth=3
	testl	(%r13,%r15,4), %r12d
	je	.LBB7_76
# BB#71:                                #   in Loop: Header=BB7_68 Depth=3
	movl	(%r13), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	movl	$2, %ecx
	cmpl	$33, %eax
	jb	.LBB7_73
# BB#72:                                #   in Loop: Header=BB7_68 Depth=3
	decl	%eax
	shrl	$5, %eax
	addl	$2, %eax
	movl	%eax, %ecx
.LBB7_73:                               #   in Loop: Header=BB7_68 Depth=3
	movl	%ecx, %edi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r13, %rsi
	callq	set_copy
	movq	%rax, %rbp
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbp, %rsi
	movq	104(%rsp), %rdx         # 8-byte Reload
	callq	set_diff
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	28(%rsp), %ecx          # 4-byte Reload
	orl	%ecx, (%rbp,%rax,4)
	orl	%r12d, (%rbp,%r15,4)
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	sf_addset
	movq	%rax, %r14
	testq	%rbp, %rbp
	je	.LBB7_75
# BB#74:                                #   in Loop: Header=BB7_68 Depth=3
	movq	%rbp, %rdi
	callq	free
.LBB7_75:                               #   in Loop: Header=BB7_68 Depth=3
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB7_76
	.p2align	4, 0x90
.LBB7_68:                               #   Parent Loop BB7_65 Depth=1
                                        #     Parent Loop BB7_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorl	%eax, %eax
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	callq	setp_implies
	testl	%eax, %eax
	je	.LBB7_69
.LBB7_76:                               #   in Loop: Header=BB7_68 Depth=3
	movq	(%rbp), %rax
	movslq	(%rax), %rax
	leaq	(%r13,%rax,4), %r13
	cmpq	%rbx, %r13
	jb	.LBB7_68
.LBB7_77:                               # %._crit_edge243
                                        #   in Loop: Header=BB7_66 Depth=2
	movl	12(%r14), %r13d
	testl	%r13d, %r13d
	jle	.LBB7_78
# BB#79:                                #   in Loop: Header=BB7_66 Depth=2
	movl	cube(%rip), %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rbp
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	cube1list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	complement
	movq	%rax, %r15
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	callq	espresso
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sf_free
	movslq	12(%rbx), %rbp
	movslq	(%rbx), %rcx
	imulq	%rbp, %rcx
	testl	%ecx, %ecx
	jle	.LBB7_80
# BB#81:                                # %.lr.ph246
                                        #   in Loop: Header=BB7_66 Depth=2
	movq	24(%rbx), %rax
	leaq	(%rax,%rcx,4), %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	80(%rsp), %r15          # 8-byte Reload
	leal	(%r15,%rcx), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	sarl	$5, %ecx
	incl	%ecx
	movslq	%ecx, %rcx
	.p2align	4, 0x90
.LBB7_82:                               #   Parent Loop BB7_65 Depth=1
                                        #     Parent Loop BB7_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	orl	%esi, (%rax,%rcx,4)
	movslq	(%rbx), %rdi
	leaq	(%rax,%rdi,4), %rax
	cmpq	%rdx, %rax
	jb	.LBB7_82
# BB#83:                                # %._crit_edge247.loopexit
                                        #   in Loop: Header=BB7_66 Depth=2
	movl	12(%rbx), %ebp
	jmp	.LBB7_84
	.p2align	4, 0x90
.LBB7_78:                               #   in Loop: Header=BB7_66 Depth=2
	movq	80(%rsp), %r15          # 8-byte Reload
	jmp	.LBB7_87
	.p2align	4, 0x90
.LBB7_80:                               #   in Loop: Header=BB7_66 Depth=2
	movq	80(%rsp), %r15          # 8-byte Reload
.LBB7_84:                               # %._crit_edge247
                                        #   in Loop: Header=BB7_66 Depth=2
	xorl	%eax, %eax
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	sf_append
	movq	%rax, 72(%rsp)          # 8-byte Spill
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	je	.LBB7_86
# BB#85:                                #   in Loop: Header=BB7_66 Depth=2
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	movq	96(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	movl	%r13d, %ecx
	movl	%ebp, %r8d
	callq	printf
.LBB7_86:                               #   in Loop: Header=BB7_66 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB7_87:                               #   in Loop: Header=BB7_66 Depth=2
	incl	%r15d
	cmpl	4(%rsp), %r15d          # 4-byte Folded Reload
	jne	.LBB7_66
# BB#88:                                # %._crit_edge251
                                        #   in Loop: Header=BB7_65 Depth=1
	movq	96(%rsp), %rcx          # 8-byte Reload
	incl	%ecx
	cmpl	4(%rsp), %ecx           # 4-byte Folded Reload
	jne	.LBB7_65
	jmp	.LBB7_89
.LBB7_42:
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB7_89:                               # %._crit_edge255
	movq	104(%rsp), %rdi         # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB7_91
# BB#90:
	callq	free
.LBB7_91:
	movq	40(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB7_93
# BB#92:
	callq	free
.LBB7_93:
	movq	56(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB7_95
# BB#94:
	callq	free
.LBB7_95:
	movq	(%rbp), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbp)
	movq	8(%rbp), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movl	cube(%rip), %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, 8(%rbp)
	xorl	%eax, %eax
	callq	setdown_cube
	movq	cube+32(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB7_97
# BB#96:
	callq	free
	movq	$0, cube+32(%rip)
.LBB7_97:
	movq	112(%rsp), %rbx         # 8-byte Reload
	movl	%ebx, cube+8(%rip)
	leal	3(%rbx), %eax
	movl	%eax, cube+4(%rip)
	leaq	12(,%rbx,4), %rdi
	callq	malloc
	movq	%rax, cube+32(%rip)
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, (%rax,%rbx,4)
	movl	%ecx, 4(%rax,%rbx,4)
	movl	92(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 8(%rax,%rbx,4)
	xorl	%eax, %eax
	callq	cube_setup
	movq	(%rbp), %rax
	movslq	(%rax), %rdx
	movslq	12(%rax), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB7_100
# BB#98:                                # %.lr.ph.preheader
	movq	24(%rax), %rbx
	leaq	(%rbx,%rcx,4), %r14
	.p2align	4, 0x90
.LBB7_99:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rdi
	movl	$.L.str.23, %ecx
	xorl	%eax, %eax
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	kiss_print_cube
	movq	(%rbp), %rax
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r14, %rbx
	jb	.LBB7_99
.LBB7_100:                              # %._crit_edge
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_1:
	movq	stderr(%rip), %rcx
	movl	$.L.str.16, %edi
	movl	$46, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$51, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	fatal
	movl	cube+8(%rip), %eax
	movl	cube+4(%rip), %ecx
	jmp	.LBB7_2
.LBB7_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.16, %edi
	movl	$46, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$51, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	fatal
	movl	cube+8(%rip), %eax
	jmp	.LBB7_4
.Lfunc_end7:
	.size	disassemble_fsm, .Lfunc_end7-disassemble_fsm
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"DONT_CARE"
	.size	.L.str, 10

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"DONTCARE"
	.size	.L.str.1, 9

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"dont_care"
	.size	.L.str.2, 10

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"dontcare"
	.size	.L.str.3, 9

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"simpcomp+"
	.size	.L.str.4, 10

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"simpcomp-"
	.size	.L.str.5, 10

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"term1    "
	.size	.L.str.6, 10

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"term2    "
	.size	.L.str.7, 10

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"union     "
	.size	.L.str.8, 11

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"simplify"
	.size	.L.str.9, 9

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"separate  "
	.size	.L.str.10, 11

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"symbolic-output index out of range"
	.size	.L.str.11, 35

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"error"
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	".symbolic requires binary variables"
	.size	.L.str.13, 36

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"bad cube in form_bitvector"
	.size	.L.str.14, 27

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"X%d"
	.size	.L.str.15, 4

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"use .symbolic and .symbolic-output to specify\n"
	.size	.L.str.16, 47

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"the present state and next state field information\n"
	.size	.L.str.17, 52

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"disassemble_pla: need two multiple-valued variables\n"
	.size	.L.str.18, 53

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"disassemble_pla: # outputs < # states\n"
	.size	.L.str.19, 39

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"# state EVERY to %d, before=%d after=%d\n"
	.size	.L.str.20, 41

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"# state ANY to NOWHERE, before=%d after=%d\n"
	.size	.L.str.21, 44

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"# state %d to %d, before=%d after=%d\n"
	.size	.L.str.22, 38

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"~1"
	.size	.L.str.23, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
