	.text
	.file	"btManifoldResult.bc"
	.globl	_ZN16btManifoldResultC2EP17btCollisionObjectS1_
	.p2align	4, 0x90
	.type	_ZN16btManifoldResultC2EP17btCollisionObjectS1_,@function
_ZN16btManifoldResultC2EP17btCollisionObjectS1_: # @_ZN16btManifoldResultC2EP17btCollisionObjectS1_
	.cfi_startproc
# BB#0:
	movq	$_ZTV16btManifoldResult+16, (%rdi)
	movq	$0, 8(%rdi)
	movq	%rsi, 144(%rdi)
	movq	%rdx, 152(%rdi)
	movups	8(%rsi), %xmm0
	movups	%xmm0, 16(%rdi)
	movups	24(%rsi), %xmm0
	movups	%xmm0, 32(%rdi)
	movups	40(%rsi), %xmm0
	movups	%xmm0, 48(%rdi)
	movups	56(%rsi), %xmm0
	movups	%xmm0, 64(%rdi)
	movups	8(%rdx), %xmm0
	movups	%xmm0, 80(%rdi)
	movups	24(%rdx), %xmm0
	movups	%xmm0, 96(%rdi)
	movups	40(%rdx), %xmm0
	movups	%xmm0, 112(%rdi)
	movups	56(%rdx), %xmm0
	movups	%xmm0, 128(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN16btManifoldResultC2EP17btCollisionObjectS1_, .Lfunc_end0-_ZN16btManifoldResultC2EP17btCollisionObjectS1_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	3240099840              # float -10
.LCPI1_1:
	.long	1092616192              # float 10
	.text
	.globl	_ZN16btManifoldResult15addContactPointERK9btVector3S2_f
	.p2align	4, 0x90
	.type	_ZN16btManifoldResult15addContactPointERK9btVector3S2_f,@function
_ZN16btManifoldResult15addContactPointERK9btVector3S2_f: # @_ZN16btManifoldResult15addContactPointERK9btVector3S2_f
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 288
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	8(%r12), %rdi
	callq	_ZNK20btPersistentManifold27getContactBreakingThresholdEv
	movaps	16(%rsp), %xmm13        # 16-byte Reload
	ucomiss	%xmm0, %xmm13
	ja	.LBB1_14
# BB#1:
	movq	8(%r12), %rdi
	movq	144(%r12), %rbx
	movq	712(%rdi), %r13
	movsd	(%r15), %xmm1           # xmm1 = mem[0],zero
	movaps	%xmm13, %xmm12
	shufps	$224, %xmm12, %xmm12    # xmm12 = xmm12[0,0,2,3]
	mulps	%xmm1, %xmm12
	movss	8(%r15), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm5
	movsd	(%r14), %xmm2           # xmm2 = mem[0],zero
	addps	%xmm2, %xmm12
	movaps	%xmm12, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm5
	xorps	%xmm9, %xmm9
	xorps	%xmm8, %xmm8
	movss	%xmm5, %xmm8            # xmm8 = xmm5[0],xmm8[1,2,3]
	cmpq	%rbx, %r13
	je	.LBB1_3
# BB#2:
	movss	128(%r12), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm4
	subss	%xmm6, %xmm4
	subss	132(%r12), %xmm3
	subss	136(%r12), %xmm5
	movsd	80(%r12), %xmm6         # xmm6 = mem[0],zero
	movsd	96(%r12), %xmm11        # xmm11 = mem[0],zero
	movsd	112(%r12), %xmm10       # xmm10 = mem[0],zero
	movaps	%xmm4, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm6, %xmm0
	movaps	%xmm3, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm11, %xmm7
	addps	%xmm0, %xmm7
	movaps	%xmm5, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm10, %xmm6
	addps	%xmm7, %xmm6
	mulss	88(%r12), %xmm4
	mulss	104(%r12), %xmm3
	addss	%xmm4, %xmm3
	mulss	120(%r12), %xmm5
	addss	%xmm3, %xmm5
	movss	%xmm5, %xmm9            # xmm9 = xmm5[0],xmm9[1,2,3]
	leaq	16(%r12), %rax
	jmp	.LBB1_4
.LBB1_3:
	movss	64(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm7
	subss	%xmm0, %xmm7
	subss	68(%r12), %xmm3
	subss	72(%r12), %xmm5
	movsd	16(%r12), %xmm0         # xmm0 = mem[0],zero
	movsd	32(%r12), %xmm6         # xmm6 = mem[0],zero
	movsd	48(%r12), %xmm10        # xmm10 = mem[0],zero
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm0, %xmm4
	movaps	%xmm3, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm6, %xmm0
	addps	%xmm4, %xmm0
	movaps	%xmm5, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm10, %xmm6
	addps	%xmm0, %xmm6
	mulss	24(%r12), %xmm7
	mulss	40(%r12), %xmm3
	addss	%xmm7, %xmm3
	mulss	56(%r12), %xmm5
	addss	%xmm3, %xmm5
	movss	%xmm5, %xmm9            # xmm9 = xmm5[0],xmm9[1,2,3]
	leaq	80(%r12), %rax
.LBB1_4:
	movss	48(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	subss	%xmm0, %xmm5
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	subss	52(%rax), %xmm2
	subss	56(%rax), %xmm1
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	16(%rax), %xmm3         # xmm3 = mem[0],zero
	movsd	32(%rax), %xmm4         # xmm4 = mem[0],zero
	movaps	%xmm5, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm0, %xmm7
	movaps	%xmm2, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	addps	%xmm7, %xmm0
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	addps	%xmm0, %xmm3
	mulss	8(%rax), %xmm5
	mulss	24(%rax), %xmm2
	addss	%xmm5, %xmm2
	mulss	40(%rax), %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm6, 56(%rsp)
	movlps	%xmm9, 64(%rsp)
	movlps	%xmm3, 72(%rsp)
	movlps	%xmm0, 80(%rsp)
	movups	(%r15), %xmm0
	movups	%xmm0, 120(%rsp)
	movss	%xmm13, 136(%rsp)
	movl	$0, 140(%rsp)
	movl	$0, 144(%rsp)
	movq	$0, 168(%rsp)
	movl	$0, 176(%rsp)
	movb	$0, 180(%rsp)
	movl	$0, 184(%rsp)
	movl	$0, 188(%rsp)
	movl	$0, 192(%rsp)
	unpcklpd	%xmm8, %xmm12   # xmm12 = xmm12[0],xmm8[0]
	movupd	%xmm12, 104(%rsp)
	movups	(%r14), %xmm0
	movups	%xmm0, 88(%rsp)
	leaq	56(%rsp), %rsi
	callq	_ZNK20btPersistentManifold13getCacheEntryERK15btManifoldPoint
	movq	144(%r12), %rdx
	movq	152(%r12), %rcx
	movss	236(%rdx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	236(%rcx), %xmm0
	movss	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	maxss	%xmm0, %xmm1
	movss	.LCPI1_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	minss	%xmm1, %xmm0
	movss	%xmm0, 140(%rsp)
	movss	240(%rdx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	240(%rcx), %xmm0
	movss	%xmm0, 144(%rsp)
	movl	160(%r12), %ebp
	movl	164(%r12), %edi
	cmpq	%rbx, %r13
	je	.LBB1_6
# BB#5:
	leaq	172(%r12), %rdx
	leaq	168(%r12), %rcx
	movl	%edi, %esi
	jmp	.LBB1_7
.LBB1_6:
	leaq	168(%r12), %rdx
	leaq	172(%r12), %rcx
	movl	%ebp, %esi
	movl	%edi, %ebp
.LBB1_7:
	movl	(%rdx), %edx
	movl	%esi, 148(%rsp)
	movl	%ebp, 152(%rsp)
	movl	%edx, 156(%rsp)
	movl	(%rcx), %ecx
	movl	%ecx, 160(%rsp)
	movq	8(%r12), %rbp
	testl	%eax, %eax
	js	.LBB1_9
# BB#8:
	movl	%eax, 16(%rsp)          # 4-byte Spill
	cltq
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	imulq	$176, %rax, %rbx
	leaq	8(%rbp,%rbx), %rdi
	movl	144(%rbp,%rbx), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movl	128(%rbp,%rbx), %r13d
	movl	136(%rbp,%rbx), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	140(%rbp,%rbx), %r15d
	movq	120(%rbp,%rbx), %r14
	leaq	56(%rsp), %rsi
	movl	$172, %edx
	callq	memcpy
	movq	%r14, 120(%rbp,%rbx)
	movl	%r13d, 128(%rbp,%rbx)
	movq	40(%rsp), %r13          # 8-byte Reload
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, 136(%rbp,%rbx)
	movl	%r15d, 140(%rbp,%rbx)
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 144(%rbp,%rbx)
	movq	48(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_10
.LBB1_9:
	leaq	56(%rsp), %rsi
	movq	%rbp, %rdi
	callq	_ZN20btPersistentManifold16addManifoldPointERK15btManifoldPoint
	movl	%eax, 16(%rsp)          # 4-byte Spill
.LBB1_10:
	movq	gContactAddedCallback(%rip), %rax
	testq	%rax, %rax
	je	.LBB1_14
# BB#11:
	leaq	144(%r12), %rcx
	leaq	152(%r12), %rdx
	movq	(%rcx), %rsi
	testb	$8, 216(%rsi)
	jne	.LBB1_13
# BB#12:
	movq	(%rdx), %rsi
	testb	$8, 216(%rsi)
	je	.LBB1_14
.LBB1_13:
	cmpq	%rbx, %r13
	movq	%rcx, %rsi
	cmovneq	%rdx, %rsi
	movq	(%rsi), %rsi
	cmovneq	%rcx, %rdx
	movq	(%rdx), %r8
	movq	8(%r12), %rcx
	movslq	16(%rsp), %rdx          # 4-byte Folded Reload
	imulq	$176, %rdx, %rdx
	leaq	8(%rcx,%rdx), %rdi
	movl	156(%rsp), %ecx
	movl	148(%rsp), %edx
	movl	152(%rsp), %r9d
	movl	160(%rsp), %ebp
	movl	%ebp, (%rsp)
	callq	*%rax
.LBB1_14:
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN16btManifoldResult15addContactPointERK9btVector3S2_f, .Lfunc_end1-_ZN16btManifoldResult15addContactPointERK9btVector3S2_f
	.cfi_endproc

	.section	.text._ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev,"axG",@progbits,_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev,comdat
	.weak	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.p2align	4, 0x90
	.type	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev,@function
_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev: # @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end2:
	.size	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev, .Lfunc_end2-_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.cfi_endproc

	.section	.text._ZN16btManifoldResultD0Ev,"axG",@progbits,_ZN16btManifoldResultD0Ev,comdat
	.weak	_ZN16btManifoldResultD0Ev
	.p2align	4, 0x90
	.type	_ZN16btManifoldResultD0Ev,@function
_ZN16btManifoldResultD0Ev:              # @_ZN16btManifoldResultD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end3:
	.size	_ZN16btManifoldResultD0Ev, .Lfunc_end3-_ZN16btManifoldResultD0Ev
	.cfi_endproc

	.section	.text._ZN16btManifoldResult20setShapeIdentifiersAEii,"axG",@progbits,_ZN16btManifoldResult20setShapeIdentifiersAEii,comdat
	.weak	_ZN16btManifoldResult20setShapeIdentifiersAEii
	.p2align	4, 0x90
	.type	_ZN16btManifoldResult20setShapeIdentifiersAEii,@function
_ZN16btManifoldResult20setShapeIdentifiersAEii: # @_ZN16btManifoldResult20setShapeIdentifiersAEii
	.cfi_startproc
# BB#0:
	movl	%esi, 160(%rdi)
	movl	%edx, 168(%rdi)
	retq
.Lfunc_end4:
	.size	_ZN16btManifoldResult20setShapeIdentifiersAEii, .Lfunc_end4-_ZN16btManifoldResult20setShapeIdentifiersAEii
	.cfi_endproc

	.section	.text._ZN16btManifoldResult20setShapeIdentifiersBEii,"axG",@progbits,_ZN16btManifoldResult20setShapeIdentifiersBEii,comdat
	.weak	_ZN16btManifoldResult20setShapeIdentifiersBEii
	.p2align	4, 0x90
	.type	_ZN16btManifoldResult20setShapeIdentifiersBEii,@function
_ZN16btManifoldResult20setShapeIdentifiersBEii: # @_ZN16btManifoldResult20setShapeIdentifiersBEii
	.cfi_startproc
# BB#0:
	movl	%esi, 164(%rdi)
	movl	%edx, 172(%rdi)
	retq
.Lfunc_end5:
	.size	_ZN16btManifoldResult20setShapeIdentifiersBEii, .Lfunc_end5-_ZN16btManifoldResult20setShapeIdentifiersBEii
	.cfi_endproc

	.type	gContactAddedCallback,@object # @gContactAddedCallback
	.bss
	.globl	gContactAddedCallback
	.p2align	3
gContactAddedCallback:
	.quad	0
	.size	gContactAddedCallback, 8

	.type	_ZTV16btManifoldResult,@object # @_ZTV16btManifoldResult
	.section	.rodata,"a",@progbits
	.globl	_ZTV16btManifoldResult
	.p2align	3
_ZTV16btManifoldResult:
	.quad	0
	.quad	_ZTI16btManifoldResult
	.quad	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.quad	_ZN16btManifoldResultD0Ev
	.quad	_ZN16btManifoldResult20setShapeIdentifiersAEii
	.quad	_ZN16btManifoldResult20setShapeIdentifiersBEii
	.quad	_ZN16btManifoldResult15addContactPointERK9btVector3S2_f
	.size	_ZTV16btManifoldResult, 56

	.type	_ZTS16btManifoldResult,@object # @_ZTS16btManifoldResult
	.globl	_ZTS16btManifoldResult
	.p2align	4
_ZTS16btManifoldResult:
	.asciz	"16btManifoldResult"
	.size	_ZTS16btManifoldResult, 19

	.type	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE,@object # @_ZTSN36btDiscreteCollisionDetectorInterface6ResultE
	.section	.rodata._ZTSN36btDiscreteCollisionDetectorInterface6ResultE,"aG",@progbits,_ZTSN36btDiscreteCollisionDetectorInterface6ResultE,comdat
	.weak	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE
	.p2align	4
_ZTSN36btDiscreteCollisionDetectorInterface6ResultE:
	.asciz	"N36btDiscreteCollisionDetectorInterface6ResultE"
	.size	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE, 48

	.type	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE,@object # @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE
	.section	.rodata._ZTIN36btDiscreteCollisionDetectorInterface6ResultE,"aG",@progbits,_ZTIN36btDiscreteCollisionDetectorInterface6ResultE,comdat
	.weak	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE
	.p2align	3
_ZTIN36btDiscreteCollisionDetectorInterface6ResultE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE
	.size	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE, 16

	.type	_ZTI16btManifoldResult,@object # @_ZTI16btManifoldResult
	.section	.rodata,"a",@progbits
	.globl	_ZTI16btManifoldResult
	.p2align	4
_ZTI16btManifoldResult:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16btManifoldResult
	.quad	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE
	.size	_ZTI16btManifoldResult, 24


	.globl	_ZN16btManifoldResultC1EP17btCollisionObjectS1_
	.type	_ZN16btManifoldResultC1EP17btCollisionObjectS1_,@function
_ZN16btManifoldResultC1EP17btCollisionObjectS1_ = _ZN16btManifoldResultC2EP17btCollisionObjectS1_
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
