	.text
	.file	"Oscar.bc"
	.globl	Initrand
	.p2align	4, 0x90
	.type	Initrand,@function
Initrand:                               # @Initrand
	.cfi_startproc
# BB#0:
	movq	$74755, seed(%rip)      # imm = 0x12403
	retq
.Lfunc_end0:
	.size	Initrand, .Lfunc_end0-Initrand
	.cfi_endproc

	.globl	Rand
	.p2align	4, 0x90
	.type	Rand,@function
Rand:                                   # @Rand
	.cfi_startproc
# BB#0:
	imull	$1309, seed(%rip), %eax # imm = 0x51D
	addl	$13849, %eax            # imm = 0x3619
	movzwl	%ax, %eax
	movq	%rax, seed(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end1:
	.size	Rand, .Lfunc_end1-Rand
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	3204448256              # float -0.5
.LCPI2_1:
	.long	1065353216              # float 1
.LCPI2_2:
	.long	1103101952              # float 24
.LCPI2_3:
	.long	3291742208              # float -720
.LCPI2_4:
	.long	1193115648              # float 40320
.LCPI2_5:
	.long	3395124224              # float -3628800
	.text
	.globl	Cos
	.p2align	4, 0x90
	.type	Cos,@function
Cos:                                    # @Cos
	.cfi_startproc
# BB#0:
	movaps	%xmm0, %xmm1
	mulss	%xmm0, %xmm0
	movss	.LCPI2_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	.LCPI2_1(%rip), %xmm2
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm0, %xmm3
	divss	.LCPI2_2(%rip), %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm0, %xmm2
	divss	.LCPI2_3(%rip), %xmm2
	addss	%xmm3, %xmm2
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm0, %xmm3
	divss	.LCPI2_4(%rip), %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm0
	divss	.LCPI2_5(%rip), %xmm0
	addss	%xmm3, %xmm0
	retq
.Lfunc_end2:
	.size	Cos, .Lfunc_end2-Cos
	.cfi_endproc

	.globl	Min0
	.p2align	4, 0x90
	.type	Min0,@function
Min0:                                   # @Min0
	.cfi_startproc
# BB#0:
	cmpl	%esi, %edi
	cmovlel	%edi, %esi
	movl	%esi, %eax
	retq
.Lfunc_end3:
	.size	Min0, .Lfunc_end3-Min0
	.cfi_endproc

	.globl	Printcomplex
	.p2align	4, 0x90
	.type	Printcomplex,@function
Printcomplex:                           # @Printcomplex
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r14d
	movl	%esi, %ebx
	movq	%rdi, %rbp
	movl	$10, %edi
	callq	putchar
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rbx
	movss	(%rbp,%rbx,8), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movss	4(%rbp,%rbx,8), %xmm1   # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.1, %edi
	movb	$2, %al
	callq	printf
	addl	%r15d, %ebx
	movslq	%ebx, %rbx
	movss	(%rbp,%rbx,8), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movss	4(%rbp,%rbx,8), %xmm1   # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.1, %edi
	movb	$2, %al
	callq	printf
	movl	$10, %edi
	callq	putchar
	addl	%r15d, %ebx
	cmpl	%r14d, %ebx
	jle	.LBB4_1
# BB#2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	Printcomplex, .Lfunc_end4-Printcomplex
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	956301312               # float 1.22070313E-4
	.text
	.globl	Uniform11
	.p2align	4, 0x90
	.type	Uniform11,@function
Uniform11:                              # @Uniform11
	.cfi_startproc
# BB#0:
	imull	$4855, (%rdi), %eax     # imm = 0x12F7
	addl	$1731, %eax             # imm = 0x6C3
	andl	$8191, %eax             # imm = 0x1FFF
	movl	%eax, (%rdi)
	cvtsi2ssl	%eax, %xmm0
	mulss	.LCPI5_0(%rip), %xmm0
	movss	%xmm0, (%rsi)
	retq
.Lfunc_end5:
	.size	Uniform11, .Lfunc_end5-Uniform11
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	1082130432              # float 4
.LCPI6_1:
	.long	1078530011              # float 3.14159274
.LCPI6_2:
	.long	3204448256              # float -0.5
.LCPI6_3:
	.long	1065353216              # float 1
.LCPI6_4:
	.long	1103101952              # float 24
.LCPI6_5:
	.long	3291742208              # float -720
.LCPI6_6:
	.long	1193115648              # float 40320
.LCPI6_7:
	.long	3395124224              # float -3628800
	.text
	.globl	Exptab
	.p2align	4, 0x90
	.type	Exptab,@function
Exptab:                                 # @Exptab
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movss	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movl	$1, %eax
	movss	.LCPI6_1(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movss	.LCPI6_2(%rip), %xmm9   # xmm9 = mem[0],zero,zero,zero
	movss	.LCPI6_3(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI6_4(%rip), %xmm10  # xmm10 = mem[0],zero,zero,zero
	movss	.LCPI6_5(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movss	.LCPI6_6(%rip), %xmm12  # xmm12 = mem[0],zero,zero,zero
	movss	.LCPI6_7(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	movaps	%xmm8, %xmm6
	divss	%xmm0, %xmm6
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	movaps	%xmm6, %xmm2
	mulss	%xmm1, %xmm2
	mulss	%xmm9, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm6, %xmm2
	movaps	%xmm6, %xmm4
	mulss	%xmm2, %xmm4
	divss	%xmm10, %xmm2
	mulss	%xmm6, %xmm4
	movaps	%xmm6, %xmm5
	addss	%xmm1, %xmm2
	mulss	%xmm4, %xmm5
	divss	%xmm11, %xmm4
	mulss	%xmm6, %xmm5
	movaps	%xmm6, %xmm1
	addss	%xmm2, %xmm4
	mulss	%xmm5, %xmm1
	divss	%xmm12, %xmm5
	mulss	%xmm6, %xmm1
	addss	%xmm4, %xmm5
	divss	%xmm7, %xmm1
	addss	%xmm5, %xmm1
	addss	%xmm1, %xmm1
	movaps	%xmm3, %xmm2
	divss	%xmm1, %xmm2
	movss	%xmm2, -104(%rsp,%rax,4)
	addss	%xmm0, %xmm0
	incq	%rax
	cmpq	$26, %rax
	jne	.LBB6_1
# BB#2:
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	sarl	%eax
	movl	%edi, %r10d
	sarl	$31, %r10d
	shrl	$30, %r10d
	addl	%edi, %r10d
	sarl	$2, %r10d
	movl	$1065353216, 8(%rsi)    # imm = 0x3F800000
	movl	$0, 12(%rsi)
	movslq	%r10d, %rcx
	movl	$0, 8(%rsi,%rcx,8)
	movl	$1065353216, 12(%rsi,%rcx,8) # imm = 0x3F800000
	movslq	%eax, %rcx
	movl	$3212836864, %eax       # imm = 0xBF800000
	movq	%rax, 8(%rsi,%rcx,8)
	movl	$1, %r11d
	movl	$25, %r8d
	.p2align	4, 0x90
.LBB6_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_4 Depth 2
	movl	%r10d, %edi
	shrl	$31, %edi
	addl	%r10d, %edi
	movl	%edi, %r9d
	sarl	%r9d
	movslq	%r11d, %rax
	movss	-104(%rsp,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movslq	%r9d, %rax
	movslq	%r10d, %rdx
	andl	$-2, %edi
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB6_4:                                #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rdi,%rbx), %ebp
	movslq	%ebp, %rbp
	movss	(%rsi,%rbp,8), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movslq	%ebx, %rbx
	addss	(%rsi,%rbx,8), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 8(%rsi,%rax,8)
	movss	4(%rsi,%rbp,8), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	4(%rsi,%rbx,8), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsi,%rax,8)
	addq	%rdx, %rax
	addl	%edx, %ebx
	cmpq	%rcx, %rax
	jle	.LBB6_4
# BB#5:                                 #   in Loop: Header=BB6_3 Depth=1
	incl	%r11d
	cmpl	$26, %r11d
	cmovgel	%r8d, %r11d
	cmpl	$3, %r10d
	movl	%r9d, %r10d
	jg	.LBB6_3
# BB#6:
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end6:
	.size	Exptab, .Lfunc_end6-Exptab
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	Fft
	.p2align	4, 0x90
	.type	Fft,@function
Fft:                                    # @Fft
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%rsi, %r9
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	sarl	%eax
	movl	%eax, -84(%rsp)         # 4-byte Spill
	movslq	%eax, %r8
	movq	%rdi, -64(%rsp)         # 8-byte Spill
	movslq	%edi, %rdi
	testq	%rdi, %rdi
	movl	$1, %esi
	cmovgq	%rdi, %rsi
	leaq	8(%r9), %rax
	leaq	8(%r9,%rsi,8), %r10
	leaq	8(%rdx), %r11
	leaq	8(%rdx,%rsi,8), %rbx
	movabsq	$9223372036854775804, %rbp # imm = 0x7FFFFFFFFFFFFFFC
	movq	%rsi, -80(%rsp)         # 8-byte Spill
	andq	%rsi, %rbp
	leaq	-4(%rbp), %rsi
	movq	%rsi, -32(%rsp)         # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	shrl	$2, %esi
	incl	%esi
	cmpq	%rbx, %rax
	sbbb	%al, %al
	cmpq	%r10, %r11
	sbbb	%bl, %bl
	andb	%al, %bl
	andb	$1, %bl
	movb	%bl, -109(%rsp)         # 1-byte Spill
	movq	%rbp, -24(%rsp)         # 8-byte Spill
	leaq	1(%rbp), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	andl	$3, %esi
	leaq	4(%rdx), %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	movq	%rsi, -40(%rsp)         # 8-byte Spill
	negq	%rsi
	movq	%rsi, -56(%rsp)         # 8-byte Spill
	movq	%r8, -8(%rsp)           # 8-byte Spill
	leaq	(%r9,%r8,8), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movl	$1, %ebx
	movq	%rdx, -96(%rsp)         # 8-byte Spill
	movq	%rdi, -104(%rsp)        # 8-byte Spill
	jmp	.LBB7_1
.LBB7_10:                               # %vector.body.preheader
                                        #   in Loop: Header=BB7_1 Depth=1
	cmpq	$0, -40(%rsp)           # 8-byte Folded Reload
	je	.LBB7_11
# BB#12:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	-56(%rsp), %rax         # 8-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_13:                               # %vector.body.prol
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	8(%rdx,%rbp,8), %xmm1
	movups	24(%rdx,%rbp,8), %xmm2
	movups	%xmm1, 8(%r9,%rbp,8)
	movups	%xmm2, 24(%r9,%rbp,8)
	addq	$4, %rbp
	incq	%rax
	jne	.LBB7_13
	jmp	.LBB7_14
.LBB7_11:                               #   in Loop: Header=BB7_1 Depth=1
	xorl	%ebp, %ebp
.LBB7_14:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB7_1 Depth=1
	cmpq	$12, -32(%rsp)          # 8-byte Folded Reload
	jb	.LBB7_16
	.p2align	4, 0x90
.LBB7_15:                               # %vector.body
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(,%rbp,8), %rax
	orq	$8, %rax
	movups	(%rdx,%rax), %xmm1
	movups	16(%rdx,%rax), %xmm2
	movups	%xmm1, (%r9,%rax)
	movups	%xmm2, 16(%r9,%rax)
	leaq	4(%rbp), %rax
	orq	$1, %rax
	movups	(%rdx,%rax,8), %xmm1
	movups	16(%rdx,%rax,8), %xmm2
	movups	%xmm1, (%r9,%rax,8)
	movups	%xmm2, 16(%r9,%rax,8)
	leaq	8(%rbp), %rax
	orq	$1, %rax
	movups	(%rdx,%rax,8), %xmm1
	movups	16(%rdx,%rax,8), %xmm2
	movups	%xmm1, (%r9,%rax,8)
	movups	%xmm2, 16(%r9,%rax,8)
	leaq	12(%rbp), %rax
	orq	$1, %rax
	movups	(%rdx,%rax,8), %xmm1
	movups	16(%rdx,%rax,8), %xmm2
	movups	%xmm1, (%r9,%rax,8)
	movups	%xmm2, 16(%r9,%rax,8)
	addq	$16, %rbp
	cmpq	%rsi, %rbp
	jne	.LBB7_15
.LBB7_16:                               # %middle.block
                                        #   in Loop: Header=BB7_1 Depth=1
	cmpq	%rsi, -80(%rsp)         # 8-byte Folded Reload
	movq	-48(%rsp), %rax         # 8-byte Reload
	jne	.LBB7_17
	jmp	.LBB7_19
	.p2align	4, 0x90
.LBB7_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_2 Depth 2
                                        #       Child Loop BB7_3 Depth 3
                                        #     Child Loop BB7_13 Depth 2
                                        #     Child Loop BB7_15 Depth 2
                                        #     Child Loop BB7_18 Depth 2
	movl	%ebx, -108(%rsp)        # 4-byte Spill
	movslq	%ebx, %r10
	movq	-72(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r10,8), %r15
	leaq	(,%r10,8), %rdx
	xorl	%r8d, %r8d
	movq	%r10, %r14
	movl	$1, %r11d
	.p2align	4, 0x90
.LBB7_2:                                #   Parent Loop BB7_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_3 Depth 3
	movslq	%r8d, %rbx
	leal	1(%rbx), %eax
	movslq	%eax, %rdi
	movq	%r11, %rbp
	shlq	$32, %rbp
	movslq	%r11d, %r11
	movq	%rbp, %r13
	sarq	$29, %r13
	leaq	(%r15,%r13), %rax
	sarq	$31, %rbp
	leaq	(%rbp,%rbx,2), %rbp
	movq	-72(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rbp,4), %r12
	movq	-16(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r13), %rbx
	movq	%r9, %rsi
	addq	%r9, %r13
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB7_3:                                #   Parent Loop BB7_1 Depth=1
                                        #     Parent Loop BB7_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%r13,%r9,8), %xmm1     # xmm1 = mem[0],zero,zero,zero
	addss	(%rbx,%r9,8), %xmm1
	movss	%xmm1, -4(%r12,%r9,8)
	movss	4(%r13,%r9,8), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	4(%rbx,%r9,8), %xmm1
	movss	%xmm1, (%r12,%r9,8)
	movss	(%r13,%r9,8), %xmm1     # xmm1 = mem[0],zero,zero,zero
	subss	(%rbx,%r9,8), %xmm1
	mulss	(%rcx,%rdi,8), %xmm1
	movss	4(%rcx,%rdi,8), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	4(%r13,%r9,8), %xmm3    # xmm3 = mem[0],zero,zero,zero
	subss	4(%rbx,%r9,8), %xmm3
	movaps	%xmm2, %xmm4
	mulss	%xmm3, %xmm4
	subss	%xmm4, %xmm1
	movss	%xmm1, -4(%rax,%r9,8)
	mulss	(%rcx,%rdi,8), %xmm3
	movss	(%r13,%r9,8), %xmm1     # xmm1 = mem[0],zero,zero,zero
	subss	(%rbx,%r9,8), %xmm1
	mulss	%xmm2, %xmm1
	addss	%xmm3, %xmm1
	movss	%xmm1, (%rax,%r9,8)
	leaq	1(%r11,%r9), %rbp
	incq	%r9
	decq	%rbp
	cmpq	%r14, %rbp
	jl	.LBB7_3
# BB#4:                                 #   in Loop: Header=BB7_2 Depth=2
	addq	%r10, %r14
	addl	%r10d, %r8d
	addq	%rdx, %r15
	addq	%r9, %r11
	cmpq	-8(%rsp), %r14          # 8-byte Folded Reload
	movq	%rsi, %r9
	jle	.LBB7_2
# BB#5:                                 # %.preheader98.preheader
                                        #   in Loop: Header=BB7_1 Depth=1
	cmpq	$4, -80(%rsp)           # 8-byte Folded Reload
	jb	.LBB7_6
# BB#7:                                 # %min.iters.checked
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	-24(%rsp), %rsi         # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB7_6
# BB#8:                                 # %vector.memcheck
                                        #   in Loop: Header=BB7_1 Depth=1
	cmpb	$0, -109(%rsp)          # 1-byte Folded Reload
	movq	-96(%rsp), %rdx         # 8-byte Reload
	movq	-104(%rsp), %rdi        # 8-byte Reload
	movl	-108(%rsp), %ebx        # 4-byte Reload
	je	.LBB7_10
# BB#9:                                 #   in Loop: Header=BB7_1 Depth=1
	movl	$1, %eax
	jmp	.LBB7_17
	.p2align	4, 0x90
.LBB7_6:                                #   in Loop: Header=BB7_1 Depth=1
	movl	$1, %eax
	movq	-96(%rsp), %rdx         # 8-byte Reload
	movq	-104(%rsp), %rdi        # 8-byte Reload
	movl	-108(%rsp), %ebx        # 4-byte Reload
.LBB7_17:                               # %.preheader98.preheader141
                                        #   in Loop: Header=BB7_1 Depth=1
	decq	%rax
	.p2align	4, 0x90
.LBB7_18:                               # %.preheader98
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdx,%rax,8), %rsi
	movq	%rsi, 8(%r9,%rax,8)
	incq	%rax
	cmpq	%rdi, %rax
	jl	.LBB7_18
.LBB7_19:                               # %.loopexit
                                        #   in Loop: Header=BB7_1 Depth=1
	addl	%ebx, %ebx
	cmpl	-84(%rsp), %ebx         # 4-byte Folded Reload
	jle	.LBB7_1
# BB#20:                                # %.preheader
	movq	-64(%rsp), %rdi         # 8-byte Reload
	testl	%edi, %edi
	jle	.LBB7_35
# BB#21:                                # %.lr.ph.preheader
	incl	%edi
	leaq	-1(%rdi), %rcx
	cmpq	$4, %rcx
	jae	.LBB7_23
# BB#22:
	movl	$1, %eax
	jmp	.LBB7_33
.LBB7_23:                               # %min.iters.checked123
	movq	%rcx, %rax
	andq	$-4, %rax
	movq	%rcx, %rdx
	andq	$-4, %rdx
	je	.LBB7_24
# BB#25:                                # %vector.ph127
	movq	%rdi, %r8
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	leaq	-4(%rdx), %rdi
	movq	%rdi, %rbx
	shrq	$2, %rbx
	btl	$2, %edi
	jb	.LBB7_26
# BB#27:                                # %vector.body119.prol
	movups	8(%r9), %xmm2
	movups	24(%r9), %xmm3
	movaps	%xmm2, %xmm4
	shufps	$136, %xmm3, %xmm4      # xmm4 = xmm4[0,2],xmm3[0,2]
	shufps	$221, %xmm3, %xmm2      # xmm2 = xmm2[1,3],xmm3[1,3]
	mulps	%xmm1, %xmm4
	mulps	%xmm1, %xmm2
	xorps	.LCPI7_0(%rip), %xmm2
	movaps	%xmm4, %xmm3
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	unpckhps	%xmm2, %xmm4    # xmm4 = xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	movups	%xmm4, 24(%r9)
	movups	%xmm3, 8(%r9)
	movl	$4, %ebp
	testq	%rbx, %rbx
	jne	.LBB7_29
	jmp	.LBB7_31
.LBB7_24:
	movl	$1, %eax
	jmp	.LBB7_33
.LBB7_26:
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	je	.LBB7_31
.LBB7_29:                               # %vector.ph127.new
	movq	%rdx, %rdi
	subq	%rbp, %rdi
	leaq	40(%r9,%rbp,8), %rbp
	movaps	.LCPI7_0(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB7_30:                               # %vector.body119
                                        # =>This Inner Loop Header: Depth=1
	movups	-32(%rbp), %xmm3
	movups	-16(%rbp), %xmm4
	movaps	%xmm3, %xmm5
	shufps	$136, %xmm4, %xmm5      # xmm5 = xmm5[0,2],xmm4[0,2]
	shufps	$221, %xmm4, %xmm3      # xmm3 = xmm3[1,3],xmm4[1,3]
	mulps	%xmm1, %xmm5
	mulps	%xmm1, %xmm3
	xorps	%xmm2, %xmm3
	movaps	%xmm5, %xmm4
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	unpckhps	%xmm3, %xmm5    # xmm5 = xmm5[2],xmm3[2],xmm5[3],xmm3[3]
	movups	%xmm5, -16(%rbp)
	movups	%xmm4, -32(%rbp)
	movups	(%rbp), %xmm3
	movups	16(%rbp), %xmm4
	movaps	%xmm3, %xmm5
	shufps	$136, %xmm4, %xmm5      # xmm5 = xmm5[0,2],xmm4[0,2]
	shufps	$221, %xmm4, %xmm3      # xmm3 = xmm3[1,3],xmm4[1,3]
	mulps	%xmm1, %xmm5
	mulps	%xmm1, %xmm3
	xorps	%xmm2, %xmm3
	movaps	%xmm5, %xmm4
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	unpckhps	%xmm3, %xmm5    # xmm5 = xmm5[2],xmm3[2],xmm5[3],xmm3[3]
	movups	%xmm5, 16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$64, %rbp
	addq	$-8, %rdi
	jne	.LBB7_30
.LBB7_31:                               # %middle.block120
	cmpq	%rdx, %rcx
	movq	%r8, %rdi
	je	.LBB7_35
# BB#32:
	orq	$1, %rax
.LBB7_33:                               # %.lr.ph.preheader140
	leaq	4(%r9,%rax,8), %rcx
	subq	%rax, %rdi
	movaps	.LCPI7_0(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB7_34:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	%xmm2, -4(%rcx)
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	xorps	%xmm1, %xmm2
	movss	%xmm2, (%rcx)
	addq	$8, %rcx
	decq	%rdi
	jne	.LBB7_34
.LBB7_35:                               # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	Fft, .Lfunc_end7-Fft
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_0:
	.long	956301312               # float 1.22070313E-4
.LCPI8_1:
	.long	1101004800              # float 20
.LCPI8_2:
	.long	3240099840              # float -10
.LCPI8_3:
	.long	1031798784              # float 0.0625
	.text
	.globl	Oscar
	.p2align	4, 0x90
	.type	Oscar,@function
Oscar:                                  # @Oscar
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 16
.Lcfi26:
	.cfi_offset %rbx, -16
	movl	$256, %edi              # imm = 0x100
	movl	$e, %esi
	callq	Exptab
	movq	$5767, seed(%rip)       # imm = 0x1687
	movl	$5767, %ecx             # imm = 0x1687
	movq	$-2048, %rax            # imm = 0xF800
	movss	.LCPI8_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI8_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI8_2(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB8_1:                                # =>This Inner Loop Header: Depth=1
	imull	$4855, %ecx, %ecx       # imm = 0x12F7
	addl	$1731, %ecx             # imm = 0x6C3
	andl	$8191, %ecx             # imm = 0x1FFF
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ecx, %xmm3
	mulss	%xmm0, %xmm3
	imull	$4855, %ecx, %ecx       # imm = 0x12F7
	addl	$1731, %ecx             # imm = 0x6C3
	andl	$8191, %ecx             # imm = 0x1FFF
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ecx, %xmm4
	mulss	%xmm0, %xmm4
	movaps	%xmm3, %xmm5
	mulss	%xmm1, %xmm5
	addss	%xmm2, %xmm5
	movss	%xmm5, z+2056(%rax)
	movaps	%xmm4, %xmm5
	mulss	%xmm1, %xmm5
	addss	%xmm2, %xmm5
	movss	%xmm5, z+2060(%rax)
	addq	$8, %rax
	jne	.LBB8_1
# BB#2:                                 # %.preheader
	movl	%ecx, %eax
	movq	%rax, seed(%rip)
	movss	%xmm3, zr(%rip)
	movss	%xmm4, zi(%rip)
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$256, %edi              # imm = 0x100
	movl	$z, %esi
	movl	$w, %edx
	movl	$e, %ecx
	movss	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	callq	Fft
	movl	$10, %edi
	callq	putchar
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB8_3:                                # =>This Inner Loop Header: Depth=1
	movss	z(,%rbx,8), %xmm0       # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movss	z+4(,%rbx,8), %xmm1     # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.1, %edi
	movb	$2, %al
	callq	printf
	movss	z+136(,%rbx,8), %xmm0   # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movss	z+140(,%rbx,8), %xmm1   # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.1, %edi
	movb	$2, %al
	callq	printf
	movl	$10, %edi
	callq	putchar
	addq	$34, %rbx
	cmpq	$257, %rbx              # imm = 0x101
	jl	.LBB8_3
# BB#4:                                 # %Printcomplex.exit
	popq	%rbx
	retq
.Lfunc_end8:
	.size	Oscar, .Lfunc_end8-Oscar
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 16
	callq	Oscar
	callq	Oscar
	callq	Oscar
	callq	Oscar
	callq	Oscar
	callq	Oscar
	callq	Oscar
	callq	Oscar
	callq	Oscar
	callq	Oscar
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end9:
	.size	main, .Lfunc_end9-main
	.cfi_endproc

	.type	seed,@object            # @seed
	.comm	seed,8,8
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"  %15.3f%15.3f"
	.size	.L.str.1, 15

	.type	e,@object               # @e
	.comm	e,1040,16
	.type	zr,@object              # @zr
	.comm	zr,4,4
	.type	zi,@object              # @zi
	.comm	zi,4,4
	.type	z,@object               # @z
	.comm	z,2056,16
	.type	w,@object               # @w
	.comm	w,2056,16
	.type	value,@object           # @value
	.comm	value,4,4
	.type	fixed,@object           # @fixed
	.comm	fixed,4,4
	.type	floated,@object         # @floated
	.comm	floated,4,4
	.type	permarray,@object       # @permarray
	.comm	permarray,44,16
	.type	pctr,@object            # @pctr
	.comm	pctr,4,4
	.type	tree,@object            # @tree
	.comm	tree,8,8
	.type	stack,@object           # @stack
	.comm	stack,16,16
	.type	cellspace,@object       # @cellspace
	.comm	cellspace,152,16
	.type	freelist,@object        # @freelist
	.comm	freelist,4,4
	.type	movesdone,@object       # @movesdone
	.comm	movesdone,4,4
	.type	ima,@object             # @ima
	.comm	ima,6724,16
	.type	imb,@object             # @imb
	.comm	imb,6724,16
	.type	imr,@object             # @imr
	.comm	imr,6724,16
	.type	rma,@object             # @rma
	.comm	rma,6724,16
	.type	rmb,@object             # @rmb
	.comm	rmb,6724,16
	.type	rmr,@object             # @rmr
	.comm	rmr,6724,16
	.type	piececount,@object      # @piececount
	.comm	piececount,16,16
	.type	class,@object           # @class
	.comm	class,52,16
	.type	piecemax,@object        # @piecemax
	.comm	piecemax,52,16
	.type	puzzl,@object           # @puzzl
	.comm	puzzl,2048,16
	.type	p,@object               # @p
	.comm	p,26624,16
	.type	n,@object               # @n
	.comm	n,4,4
	.type	kount,@object           # @kount
	.comm	kount,4,4
	.type	sortlist,@object        # @sortlist
	.comm	sortlist,20004,16
	.type	biggest,@object         # @biggest
	.comm	biggest,4,4
	.type	littlest,@object        # @littlest
	.comm	littlest,4,4
	.type	top,@object             # @top
	.comm	top,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
