	.text
	.file	"sort.bc"
	.globl	sort_Delete
	.p2align	4, 0x90
	.type	sort_Delete,@function
sort_Delete:                            # @sort_Delete
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB0_2
	.p2align	4, 0x90
.LBB0_1:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rdi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rdi
	jne	.LBB0_1
.LBB0_2:                                # %list_Delete.exit
	retq
.Lfunc_end0:
	.size	sort_Delete, .Lfunc_end0-sort_Delete
	.cfi_endproc

	.globl	sort_Eq
	.p2align	4, 0x90
	.type	sort_Eq,@function
sort_Eq:                                # @sort_Eq
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbp
	callq	list_Length
	movl	%eax, %ebx
	movq	%r14, %rdi
	callq	list_Length
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	%ecx, %ebx
	jne	.LBB1_9
# BB#1:                                 # %.preheader24
	testq	%rbp, %rbp
	je	.LBB1_8
# BB#2:                                 # %.preheader.lr.ph
	testq	%r14, %r14
	je	.LBB1_9
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
	movq	8(%rbp), %rcx
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB1_4:                                #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdx), %rsi
	movq	(%rdx), %rdx
	cmpq	%rsi, %rcx
	setne	%bl
	testq	%rdx, %rdx
	je	.LBB1_6
# BB#5:                                 #   in Loop: Header=BB1_4 Depth=2
	testb	%bl, %bl
	jne	.LBB1_4
.LBB1_6:                                # %..critedge_crit_edge.us
                                        #   in Loop: Header=BB1_3 Depth=1
	cmpq	%rsi, %rcx
	jne	.LBB1_9
# BB#7:                                 #   in Loop: Header=BB1_3 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB1_3
.LBB1_8:
	movl	$1, %eax
.LBB1_9:                                # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	sort_Eq, .Lfunc_end1-sort_Eq
	.cfi_endproc

	.globl	sort_GetSymbolsFromSort
	.p2align	4, 0x90
	.type	sort_GetSymbolsFromSort,@function
sort_GetSymbolsFromSort:                # @sort_GetSymbolsFromSort
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB2_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movslq	32(%rax), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r14, (%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r14
	jne	.LBB2_3
	jmp	.LBB2_4
.LBB2_1:
	xorl	%eax, %eax
.LBB2_4:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	sort_GetSymbolsFromSort, .Lfunc_end2-sort_GetSymbolsFromSort
	.cfi_endproc

	.globl	sort_ContainsSymbol
	.p2align	4, 0x90
	.type	sort_ContainsSymbol,@function
sort_ContainsSymbol:                    # @sort_ContainsSymbol
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	jne	.LBB3_2
	jmp	.LBB3_4
	.p2align	4, 0x90
.LBB3_5:                                #   in Loop: Header=BB3_2 Depth=1
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdi), %rcx
	cmpl	%esi, 32(%rcx)
	jne	.LBB3_5
# BB#3:
	movl	$1, %eax
.LBB3_4:                                # %._crit_edge
	retq
.Lfunc_end3:
	.size	sort_ContainsSymbol, .Lfunc_end3-sort_ContainsSymbol
	.cfi_endproc

	.globl	sort_IsSort
	.p2align	4, 0x90
	.type	sort_IsSort,@function
sort_IsSort:                            # @sort_IsSort
	.cfi_startproc
# BB#0:
	movl	$1, %eax
	testq	%rdi, %rdi
	jne	.LBB4_2
	jmp	.LBB4_6
	.p2align	4, 0x90
.LBB4_7:                                #   in Loop: Header=BB4_2 Depth=1
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB4_6
.LBB4_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_3 Depth 2
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB4_3:                                #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB4_7
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=2
	movq	8(%rdi), %rdx
	cmpq	8(%rcx), %rdx
	jne	.LBB4_3
# BB#5:
	xorl	%eax, %eax
.LBB4_6:                                # %.loopexit
	retq
.Lfunc_end4:
	.size	sort_IsSort, .Lfunc_end4-sort_IsSort
	.cfi_endproc

	.globl	sort_TheorySortEqual
	.p2align	4, 0x90
	.type	sort_TheorySortEqual,@function
sort_TheorySortEqual:                   # @sort_TheorySortEqual
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	%r15, %rdi
	callq	list_Length
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	list_Length
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	%ecx, %ebp
	jne	.LBB5_13
# BB#1:
	movl	32024(%rbx), %ecx
	cmpl	$-1, %ecx
	jne	.LBB5_5
# BB#2:                                 # %.preheader.i.preheader
	movl	$5, %ecx
	.p2align	4, 0x90
.LBB5_3:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-32(%rbx,%rcx,8), %rdx
	movl	$0, 8(%rdx)
	movl	$0, 16(%rdx)
	movl	$0, 12(%rdx)
	movq	-24(%rbx,%rcx,8), %rdx
	movl	$0, 8(%rdx)
	movl	$0, 16(%rdx)
	movl	$0, 12(%rdx)
	movq	-16(%rbx,%rcx,8), %rdx
	movl	$0, 8(%rdx)
	movl	$0, 16(%rdx)
	movl	$0, 12(%rdx)
	movq	-8(%rbx,%rcx,8), %rdx
	movl	$0, 8(%rdx)
	movl	$0, 16(%rdx)
	movl	$0, 12(%rdx)
	movq	(%rbx,%rcx,8), %rdx
	movl	$0, 8(%rdx)
	movl	$0, 16(%rdx)
	movl	$0, 12(%rdx)
	addq	$5, %rcx
	cmpq	$4005, %rcx             # imm = 0xFA5
	jne	.LBB5_3
# BB#4:
	movl	$0, 32024(%rbx)
	xorl	%ecx, %ecx
.LBB5_5:                                # %sort_TheoryIncrementMark.exit
	incl	%ecx
	movl	%ecx, 32024(%rbx)
	testq	%r15, %r15
	je	.LBB5_8
# BB#6:                                 # %.lr.ph25.preheader
	movq	8(%r15), %rdx
	movl	%ecx, 16(%rdx)
	movq	(%r15), %rcx
	testq	%rcx, %rcx
	je	.LBB5_8
	.p2align	4, 0x90
.LBB5_14:                               # %.lr.ph25..lr.ph25_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movl	32024(%rbx), %edx
	movq	8(%rcx), %rsi
	movl	%edx, 16(%rsi)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_14
.LBB5_8:                                # %.preheader
	testq	%r14, %r14
	je	.LBB5_12
# BB#9:                                 # %.lr.ph
	movl	32024(%rbx), %ecx
	.p2align	4, 0x90
.LBB5_10:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rdx
	cmpl	%ecx, 16(%rdx)
	jne	.LBB5_13
# BB#11:                                #   in Loop: Header=BB5_10 Depth=1
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB5_10
.LBB5_12:
	movl	$1, %eax
.LBB5_13:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	sort_TheorySortEqual, .Lfunc_end5-sort_TheorySortEqual
	.cfi_endproc

	.globl	sort_DeleteSortPair
	.p2align	4, 0x90
	.type	sort_DeleteSortPair,@function
sort_DeleteSortPair:                    # @sort_DeleteSortPair
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.LBB6_2
	.p2align	4, 0x90
.LBB6_1:                                # %.lr.ph.i.i7
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB6_1
.LBB6_2:                                # %sort_DeleteOne.exit
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB6_6
# BB#3:
	movq	8(%rbx), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	16(%rbx), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	24(%rbx), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB6_5
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB6_4
.LBB6_5:                                # %list_Delete.exit.i
	movq	memory_ARRAY+320(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+320(%rip), %rax
	movq	%rbx, (%rax)
.LBB6_6:                                # %sort_ConditionDelete.exit
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	sort_DeleteSortPair, .Lfunc_end6-sort_DeleteSortPair
	.cfi_endproc

	.globl	sort_ConditionDelete
	.p2align	4, 0x90
	.type	sort_ConditionDelete,@function
sort_ConditionDelete:                   # @sort_ConditionDelete
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 16
.Lcfi27:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB7_4
# BB#1:
	movq	8(%rbx), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	16(%rbx), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	24(%rbx), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB7_3
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB7_2
.LBB7_3:                                # %list_Delete.exit
	movq	memory_ARRAY+320(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+320(%rip), %rax
	movq	%rbx, (%rax)
.LBB7_4:
	popq	%rbx
	retq
.Lfunc_end7:
	.size	sort_ConditionDelete, .Lfunc_end7-sort_ConditionDelete
	.cfi_endproc

	.globl	sort_PairPrint
	.p2align	4, 0x90
	.type	sort_PairPrint,@function
sort_PairPrint:                         # @sort_PairPrint
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	8(%r14), %rbx
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	testq	%rbx, %rbx
	jne	.LBB8_2
	jmp	.LBB8_3
	.p2align	4, 0x90
.LBB8_1:                                # %.backedge.i
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
.LBB8_2:                                # %.backedge.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movl	32(%rax), %edi
	callq	symbol_Print
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_1
.LBB8_3:                                # %sort_Print.exit
	movq	stdout(%rip), %rsi
	movl	$41, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rcx
	movl	$.L.str, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%r14), %r14
	movl	(%r14), %edi
	callq	symbol_Print
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB8_6
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph46.i
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movq	8(%rbx), %rdi
	callq	term_PrintPrefix
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_4
.LBB8_6:                                # %._crit_edge47.i
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB8_9
	.p2align	4, 0x90
.LBB8_7:                                # %.lr.ph41.i
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movq	8(%rbx), %rdi
	callq	term_PrintPrefix
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_7
.LBB8_9:                                # %._crit_edge42.i
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB8_12
	.p2align	4, 0x90
.LBB8_10:                               # %.lr.ph36.i
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movq	8(%rbx), %rdi
	callq	term_PrintPrefix
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_10
.LBB8_12:                               # %._crit_edge37.i
	movq	32(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB8_15
	.p2align	4, 0x90
.LBB8_13:                               # %.lr.ph.i6
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movl	(%rax), %esi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_13
.LBB8_15:                               # %sort_ConditionPrint.exit
	movq	stdout(%rip), %rsi
	movl	$93, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_IO_putc                # TAILCALL
.Lfunc_end8:
	.size	sort_PairPrint, .Lfunc_end8-sort_PairPrint
	.cfi_endproc

	.globl	sort_Print
	.p2align	4, 0x90
	.type	sort_Print,@function
sort_Print:                             # @sort_Print
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 16
.Lcfi34:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	testq	%rbx, %rbx
	jne	.LBB9_2
	jmp	.LBB9_3
	.p2align	4, 0x90
.LBB9_1:                                # %.backedge
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
.LBB9_2:                                # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movl	32(%rax), %edi
	callq	symbol_Print
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_1
.LBB9_3:                                # %._crit_edge
	movq	stdout(%rip), %rsi
	movl	$41, %edi
	popq	%rbx
	jmp	_IO_putc                # TAILCALL
.Lfunc_end9:
	.size	sort_Print, .Lfunc_end9-sort_Print
	.cfi_endproc

	.globl	sort_NodeIsTop
	.p2align	4, 0x90
	.type	sort_NodeIsTop,@function
sort_NodeIsTop:                         # @sort_NodeIsTop
	.cfi_startproc
# BB#0:
	movq	32008(%rdi), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jne	.LBB10_2
	jmp	.LBB10_5
	.p2align	4, 0x90
.LBB10_6:                               #   in Loop: Header=BB10_2 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB10_5
.LBB10_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	8(%rdx), %rdx
	cmpq	$0, (%rdx)
	jne	.LBB10_6
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	cmpq	%rsi, 8(%rdx)
	jne	.LBB10_6
# BB#4:
	movl	$1, %eax
.LBB10_5:                               # %._crit_edge
	retq
.Lfunc_end10:
	.size	sort_NodeIsTop, .Lfunc_end10-sort_NodeIsTop
	.cfi_endproc

	.globl	sort_Init
	.p2align	4, 0x90
	.type	sort_Init,@function
sort_Init:                              # @sort_Init
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	sort_Init, .Lfunc_end11-sort_Init
	.cfi_endproc

	.globl	sort_Free
	.p2align	4, 0x90
	.type	sort_Free,@function
sort_Free:                              # @sort_Free
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end12:
	.size	sort_Free, .Lfunc_end12-sort_Free
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.zero	16
	.text
	.globl	sort_TheoryCreate
	.p2align	4, 0x90
	.type	sort_TheoryCreate,@function
sort_TheoryCreate:                      # @sort_TheoryCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 48
.Lcfi40:
	.cfi_offset %rbx, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movl	$32032, %edi            # imm = 0x7D20
	callq	memory_Malloc
	movq	%rax, %r15
	cmpl	$2, symbol_ACTINDEX(%rip)
	jl	.LBB13_8
# BB#1:                                 # %.lr.ph
	movl	symbol_TYPEMASK(%rip), %r14d
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	movq	$0, 8(%r15,%rbp,8)
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%rbp,8), %rax
	testq	%rax, %rax
	je	.LBB13_7
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=1
	movl	24(%rax), %ebx
	testl	%ebx, %ebx
	jns	.LBB13_7
# BB#4:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB13_2 Depth=1
	movl	%ebx, %ecx
	negl	%ecx
	andl	%r14d, %ecx
	cmpl	$2, %ecx
	jne	.LBB13_7
# BB#5:                                 #   in Loop: Header=BB13_2 Depth=1
	cmpl	$1, 16(%rax)
	jne	.LBB13_7
# BB#6:                                 #   in Loop: Header=BB13_2 Depth=1
	movl	$40, %edi
	callq	memory_Malloc
	movq	$0, 24(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movl	$0, 16(%rax)
	movl	%ebx, 32(%rax)
	movq	%rax, 8(%r15,%rbp,8)
	.p2align	4, 0x90
.LBB13_7:                               # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB13_2 Depth=1
	incq	%rbp
	movslq	symbol_ACTINDEX(%rip), %rax
	cmpq	%rax, %rbp
	jl	.LBB13_2
.LBB13_8:                               # %._crit_edge
	callq	st_IndexCreate
	movq	%rax, (%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32008(%r15)
	movl	$0, 32024(%r15)
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	sort_TheoryCreate, .Lfunc_end13-sort_TheoryCreate
	.cfi_endproc

	.globl	sort_TheoryPrint
	.p2align	4, 0x90
	.type	sort_TheoryPrint,@function
sort_TheoryPrint:                       # @sort_TheoryPrint
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 48
.Lcfi49:
	.cfi_offset %rbx, -40
.Lcfi50:
	.cfi_offset %r12, -32
.Lcfi51:
	.cfi_offset %r14, -24
.Lcfi52:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	stdout(%rip), %rcx
	testq	%r14, %r14
	je	.LBB14_23
# BB#1:
	movl	$.L.str.2, %edi
	movl	$18, %esi
	movl	$1, %edx
	callq	fwrite
	movq	32008(%r14), %r15
	testq	%r15, %r15
	movq	stdout(%rip), %rcx
	je	.LBB14_19
	.p2align	4, 0x90
.LBB14_2:                               # %.lr.ph28
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_4 Depth 2
                                        #     Child Loop BB14_8 Depth 2
                                        #     Child Loop BB14_12 Depth 2
                                        #     Child Loop BB14_16 Depth 2
	movl	$.L.str.3, %edi
	movl	$11, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%r15), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	clause_Print
	movq	stdout(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r12
	movq	stdout(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$8, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	jne	.LBB14_4
	jmp	.LBB14_6
	.p2align	4, 0x90
.LBB14_5:                               # %.backedge51.i
                                        #   in Loop: Header=BB14_4 Depth=2
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB14_6
.LBB14_4:                               # %.lr.ph67.i
                                        #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rax
	movl	32(%rax), %edi
	callq	symbol_Print
	cmpq	$0, (%rbx)
	jne	.LBB14_5
.LBB14_6:                               # %._crit_edge68.i
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%r12), %rax
	movl	32(%rax), %edi
	callq	symbol_Print
	movq	stdout(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	jne	.LBB14_8
	jmp	.LBB14_10
	.p2align	4, 0x90
.LBB14_9:                               # %.backedge50.i
                                        #   in Loop: Header=BB14_8 Depth=2
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB14_10
.LBB14_8:                               # %.lr.ph62.i
                                        #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	callq	term_PrintPrefix
	cmpq	$0, (%rbx)
	jne	.LBB14_9
.LBB14_10:                              # %._crit_edge63.i
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.14, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movq	32(%r12), %rbx
	testq	%rbx, %rbx
	jne	.LBB14_12
	jmp	.LBB14_14
	.p2align	4, 0x90
.LBB14_13:                              # %.backedge49.i
                                        #   in Loop: Header=BB14_12 Depth=2
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB14_14
.LBB14_12:                              # %.lr.ph57.i
                                        #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	callq	term_PrintPrefix
	cmpq	$0, (%rbx)
	jne	.LBB14_13
.LBB14_14:                              # %._crit_edge58.i
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.15, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movq	40(%r12), %rbx
	testq	%rbx, %rbx
	jne	.LBB14_16
	jmp	.LBB14_18
	.p2align	4, 0x90
.LBB14_17:                              # %.backedge.i
                                        #   in Loop: Header=BB14_16 Depth=2
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB14_18
.LBB14_16:                              # %.lr.ph.i
                                        #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	callq	term_PrintPrefix
	cmpq	$0, (%rbx)
	jne	.LBB14_17
.LBB14_18:                              # %sort_LinkPrint.exit
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	56(%r12), %rax
	movl	(%rax), %esi
	movl	16(%r12), %edx
	movl	20(%r12), %ecx
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	printf
	movl	48(%r12), %edi
	callq	symbol_Print
	movq	(%r15), %r15
	movq	stdout(%rip), %rcx
	testq	%r15, %r15
	jne	.LBB14_2
.LBB14_19:                              # %._crit_edge
	movl	$.L.str.5, %edi
	movl	$27, %esi
	movl	$1, %edx
	callq	fwrite
	movq	32016(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB14_22
	.p2align	4, 0x90
.LBB14_20:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$11, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	clause_Print
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB14_20
.LBB14_22:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB14_23:
	movl	$.L.str.1, %edi
	movl	$13, %esi
	movl	$1, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	fwrite                  # TAILCALL
.Lfunc_end14:
	.size	sort_TheoryPrint, .Lfunc_end14-sort_TheoryPrint
	.cfi_endproc

	.globl	sort_TheoryDelete
	.p2align	4, 0x90
	.type	sort_TheoryDelete,@function
sort_TheoryDelete:                      # @sort_TheoryDelete
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi59:
	.cfi_def_cfa_offset 64
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB15_33
# BB#1:
	movq	32008(%r14), %r15
	testq	%r15, %r15
	je	.LBB15_11
	.p2align	4, 0x90
.LBB15_2:                               # %.lr.ph95
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_3 Depth 2
                                        #     Child Loop BB15_5 Depth 2
                                        #     Child Loop BB15_7 Depth 2
	movq	8(%r15), %r13
	movq	(%r13), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r12
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB15_6
	.p2align	4, 0x90
.LBB15_3:                               # %.lr.ph.i78
                                        #   Parent Loop BB15_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rbp
	movq	(%rbp), %rdi
	movq	%r12, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, (%rbp)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB15_3
# BB#4:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.LBB15_6
	.p2align	4, 0x90
.LBB15_5:                               # %.lr.ph.i.i82
                                        #   Parent Loop BB15_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB15_5
.LBB15_6:                               # %sort_LinkDelete.exit
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	24(%r12), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	32(%r12), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	40(%r12), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	memory_ARRAY+512(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	memory_ARRAY+512(%rip), %rax
	movq	%r12, (%rax)
	movq	(%r13), %rax
	movq	8(%rax), %rdi
	callq	clause_Delete
	testq	%r13, %r13
	je	.LBB15_8
	.p2align	4, 0x90
.LBB15_7:                               # %.lr.ph.i76
                                        #   Parent Loop BB15_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB15_7
.LBB15_8:                               # %list_Delete.exit77
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB15_2
# BB#9:                                 # %._crit_edge96
	movq	32008(%r14), %rax
	testq	%rax, %rax
	je	.LBB15_11
	.p2align	4, 0x90
.LBB15_10:                              # %.lr.ph.i71
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB15_10
.LBB15_11:                              # %list_Delete.exit72
	movq	32016(%r14), %r12
	testq	%r12, %r12
	je	.LBB15_21
	.p2align	4, 0x90
.LBB15_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_13 Depth 2
                                        #     Child Loop BB15_15 Depth 2
                                        #     Child Loop BB15_17 Depth 2
	movq	8(%r12), %rbx
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r13
	movq	8(%r13), %rax
	movq	8(%rax), %r15
	movq	(%r14), %rdi
	movq	cont_LEFTCONTEXT(%rip), %rcx
	movq	%r13, %rsi
	movq	%r13, %rdx
	callq	st_EntryDelete
	movq	(%r14), %rdi
	movq	cont_LEFTCONTEXT(%rip), %rcx
	movq	%r15, %rsi
	movq	%r15, %rdx
	callq	st_EntryDelete
	movq	8(%r15), %rax
	testq	%rax, %rax
	je	.LBB15_14
	.p2align	4, 0x90
.LBB15_13:                              # %.lr.ph.i66
                                        #   Parent Loop BB15_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB15_13
.LBB15_14:                              # %list_Delete.exit67
                                        #   in Loop: Header=BB15_12 Depth=1
	movq	8(%r13), %rax
	testq	%rax, %rax
	je	.LBB15_16
	.p2align	4, 0x90
.LBB15_15:                              # %.lr.ph.i61
                                        #   Parent Loop BB15_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB15_15
.LBB15_16:                              # %list_Delete.exit62
                                        #   in Loop: Header=BB15_12 Depth=1
	movq	$0, 8(%r13)
	movq	$0, 8(%r15)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	callq	clause_Delete
	testq	%rbx, %rbx
	je	.LBB15_18
	.p2align	4, 0x90
.LBB15_17:                              # %.lr.ph.i56
                                        #   Parent Loop BB15_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB15_17
.LBB15_18:                              # %list_Delete.exit57
                                        #   in Loop: Header=BB15_12 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB15_12
# BB#19:                                # %._crit_edge91
	movq	32016(%r14), %rax
	testq	%rax, %rax
	je	.LBB15_21
	.p2align	4, 0x90
.LBB15_20:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB15_20
.LBB15_21:                              # %list_Delete.exit
	movq	(%r14), %rdi
	callq	st_IndexDelete
	movl	symbol_ACTINDEX(%rip), %eax
	cmpl	$2, %eax
	jl	.LBB15_28
# BB#22:                                # %.lr.ph.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB15_23:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_25 Depth 2
	movq	8(%r14,%rbx,8), %r15
	testq	%r15, %r15
	je	.LBB15_27
# BB#24:                                #   in Loop: Header=BB15_23 Depth=1
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.LBB15_26
	.p2align	4, 0x90
.LBB15_25:                              # %.lr.ph.i.i
                                        #   Parent Loop BB15_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB15_25
.LBB15_26:                              # %sort_NodeDelete.exit
                                        #   in Loop: Header=BB15_23 Depth=1
	movq	$0, (%r15)
	movq	24(%r15), %rdi
	movl	$sort_ConditionDelete, %esi
	callq	list_DeleteWithElement
	movq	$0, 24(%r15)
	movq	memory_ARRAY+320(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	memory_ARRAY+320(%rip), %rax
	movq	%r15, (%rax)
	movl	symbol_ACTINDEX(%rip), %eax
.LBB15_27:                              #   in Loop: Header=BB15_23 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB15_23
.LBB15_28:                              # %._crit_edge
	movl	memory_ALIGN(%rip), %ecx
	movl	$32032, %esi            # imm = 0x7D20
	movl	$32032, %eax            # imm = 0x7D20
	xorl	%edx, %edx
	divl	%ecx
	addl	$32032, %ecx            # imm = 0x7D20
	subl	%edx, %ecx
	testl	%edx, %edx
	cmovel	%esi, %ecx
	movl	memory_OFFSET(%rip), %eax
	movq	%r14, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %rdi
	testq	%rsi, %rsi
	leaq	8(%rsi), %rsi
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rsi, %rbp
	movq	%rdi, (%rbp)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB15_30
# BB#29:
	negq	%rax
	movq	-16(%r14,%rax), %rax
	movq	%rax, (%rdx)
.LBB15_30:
	addl	memory_MARKSIZE(%rip), %ecx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB15_32
# BB#31:
	leaq	16(%rcx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB15_32:                              # %memory_Free.exit
	addq	$-16, %r14
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB15_33:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	sort_TheoryDelete, .Lfunc_end15-sort_TheoryDelete
	.cfi_endproc

	.globl	sort_TheoryInsertClause
	.p2align	4, 0x90
	.type	sort_TheoryInsertClause,@function
sort_TheoryInsertClause:                # @sort_TheoryInsertClause
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi72:
	.cfi_def_cfa_offset 128
.Lcfi73:
	.cfi_offset %rbx, -56
.Lcfi74:
	.cfi_offset %r12, -48
.Lcfi75:
	.cfi_offset %r13, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %r15
	movq	24(%rbp), %r14
	movq	16(%r14), %rax
	movq	8(%rax), %rbx
	cmpl	$0, (%rbx)
	jle	.LBB16_26
# BB#1:
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movl	$64, %edi
	callq	memory_Malloc
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	24(%rbp), %rax
	movl	(%rax), %ebx
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %r13d
	movl	52(%r12), %edi
	callq	term_StartMaxRenaming
	movl	symbol_STANDARDVARCOUNTER(%rip), %esi
	incl	%esi
	movl	%esi, symbol_STANDARDVARCOUNTER(%rip)
	movl	64(%r12), %ecx
	testl	%ecx, %ecx
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	%r13d, 12(%rsp)         # 4-byte Spill
	jle	.LBB16_2
# BB#5:                                 # %.lr.ph168.i
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB16_6:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	(%rdi), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	movq	%rdi, %rdx
	jne	.LBB16_8
# BB#7:                                 #   in Loop: Header=BB16_6 Depth=1
	movq	16(%rdi), %rdx
	movq	8(%rdx), %rdx
.LBB16_8:                               # %clause_GetLiteralAtom.exit135.i
                                        #   in Loop: Header=BB16_6 Depth=1
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rdx
	cmpl	(%rdx), %r13d
	jne	.LBB16_12
# BB#9:                                 #   in Loop: Header=BB16_6 Depth=1
	cmpl	%eax, %ecx
	jne	.LBB16_11
# BB#10:                                #   in Loop: Header=BB16_6 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB16_11:                              # %clause_GetLiteralAtom.exit.i
                                        #   in Loop: Header=BB16_6 Depth=1
	negl	%eax
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	cltq
	movq	8(%r15,%rax,8), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, %rcx
	movq	%rax, %r14
	jmp	.LBB16_13
	.p2align	4, 0x90
.LBB16_12:                              #   in Loop: Header=BB16_6 Depth=1
	callq	term_Copy
	movl	%r13d, %esi
	movq	%rax, %r13
	movq	%r13, %rdi
	movl	8(%rsp), %edx           # 4-byte Reload
	callq	term_ExchangeVariable
	movl	$16, %edi
	callq	memory_Malloc
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
.LBB16_13:                              #   in Loop: Header=BB16_6 Depth=1
	movq	%r13, 8(%rax)
	movq	%rcx, (%rax)
	incq	%rbp
	movslq	64(%r12), %rcx
	cmpq	%rcx, %rbp
	movl	12(%rsp), %r13d         # 4-byte Reload
	jl	.LBB16_6
	jmp	.LBB16_3
.LBB16_2:
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB16_3:                               # %.preheader.i
	movl	68(%r12), %edx
	leal	-1(%rcx,%rdx), %eax
	cmpl	%eax, %ecx
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movl	%ebx, 36(%rsp)          # 4-byte Spill
	jle	.LBB16_14
# BB#4:
	xorl	%r13d, %r13d
	jmp	.LBB16_16
.LBB16_14:                              # %.lr.ph157.i
	movslq	%ecx, %rbx
	decq	%rbx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB16_15:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%rax, %rbp
	movq	%rbp, %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	8(%rsp), %edx           # 4-byte Reload
	callq	term_ExchangeVariable
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%rbp, 8(%r13)
	movq	%r15, (%r13)
	movl	64(%r12), %ecx
	movl	68(%r12), %edx
	leal	-1(%rcx,%rdx), %eax
	cltq
	incq	%rbx
	cmpq	%rax, %rbx
	movq	%r13, %r15
	jl	.LBB16_15
.LBB16_16:                              # %._crit_edge158.i
	leal	(%rdx,%rcx), %eax
	movl	72(%r12), %esi
	leal	-1(%rsi,%rax), %edi
	cmpl	%edi, %eax
	jle	.LBB16_18
# BB#17:
	xorl	%ebx, %ebx
	jmp	.LBB16_22
.LBB16_18:                              # %.lr.ph151.i
	movslq	%eax, %r15
	decq	%r15
	xorl	%ebx, %ebx
	movq	24(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB16_19:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	8(%rax,%r15,8), %rax
	cmpq	%rdi, %rax
	je	.LBB16_21
# BB#20:                                #   in Loop: Header=BB16_19 Depth=1
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%rax, %rbp
	movq	%rbp, %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	8(%rsp), %edx           # 4-byte Reload
	callq	term_ExchangeVariable
	movl	$16, %edi
	callq	memory_Malloc
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, 8(%rax)
	movq	%rbx, (%rax)
	movl	64(%r12), %ecx
	movl	68(%r12), %edx
	movl	72(%r12), %esi
	movq	%rax, %rbx
.LBB16_21:                              #   in Loop: Header=BB16_19 Depth=1
	leal	(%rsi,%rdx), %eax
	leal	-1(%rcx,%rax), %eax
	cltq
	incq	%r15
	cmpq	%rax, %r15
	jl	.LBB16_19
.LBB16_22:                              # %._crit_edge152.i
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	%r14, (%r15)
	movl	36(%rsp), %eax          # 4-byte Reload
	negl	%eax
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %eax
	cltq
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	8(%rcx,%rax,8), %rax
	movq	%rax, 8(%r15)
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, 48(%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 24(%r15)
	movq	%r13, 32(%r15)
	movq	%rbx, 40(%r15)
	movq	%r14, %rdi
	callq	list_Length
	movl	%eax, 16(%r15)
	movl	%eax, 20(%r15)
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 56(%r15)
	testq	%r14, %r14
	je	.LBB16_25
	.p2align	4, 0x90
.LBB16_23:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rbx
	movq	(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, (%rbx)
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB16_23
.LBB16_25:                              # %sort_TheoryLinkCreate.exit
	movq	%r12, %rdi
	callq	clause_Copy
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r15, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%r14, 8(%r15)
	movq	%rbp, (%r15)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	%r13, 8(%rbp)
	movq	%r15, (%rbp)
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	32008(%r15), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 32008(%r15)
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	24(%rbp), %r14
	movq	64(%rsp), %rbx          # 8-byte Reload
.LBB16_26:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 8(%r14)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 8(%rbx)
	movq	(%r15), %rdi
	movq	cont_LEFTCONTEXT(%rip), %rcx
	movq	%rbx, %rsi
	movq	%rbx, %rdx
	callq	st_EntryCreate
	movq	(%r15), %rdi
	movq	cont_LEFTCONTEXT(%rip), %rcx
	movq	%r14, %rsi
	movq	%r14, %rdx
	callq	st_EntryCreate
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%r12, 8(%r14)
	movq	%rbp, (%r14)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r13, 8(%rbx)
	movq	%r14, (%rbx)
	movq	32016(%r15), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 32016(%r15)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	sort_TheoryInsertClause, .Lfunc_end16-sort_TheoryInsertClause
	.cfi_endproc

	.globl	sort_TheoryDeleteClause
	.p2align	4, 0x90
	.type	sort_TheoryDeleteClause,@function
sort_TheoryDeleteClause:                # @sort_TheoryDeleteClause
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi82:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi83:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi85:
	.cfi_def_cfa_offset 64
.Lcfi86:
	.cfi_offset %rbx, -56
.Lcfi87:
	.cfi_offset %r12, -48
.Lcfi88:
	.cfi_offset %r13, -40
.Lcfi89:
	.cfi_offset %r14, -32
.Lcfi90:
	.cfi_offset %r15, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	32008(%r14), %r12
	testq	%r12, %r12
	je	.LBB17_1
# BB#2:                                 # %.lr.ph80.preheader
	movq	%r15, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB17_3:                               # %.lr.ph80
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_6 Depth 2
                                        #     Child Loop BB17_8 Depth 2
                                        #     Child Loop BB17_10 Depth 2
	movq	8(%r12), %rbx
	cmpq	%r15, 8(%rbx)
	jne	.LBB17_11
# BB#4:                                 #   in Loop: Header=BB17_3 Depth=1
	movq	$0, 8(%r12)
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r13
	movq	(%r13), %rbp
	testq	%rbp, %rbp
	je	.LBB17_9
# BB#5:                                 # %.lr.ph.i68.preheader
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	%r14, %r15
	.p2align	4, 0x90
.LBB17_6:                               # %.lr.ph.i68
                                        #   Parent Loop BB17_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %r14
	movq	(%r14), %rdi
	movq	%r13, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, (%r14)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB17_6
# BB#7:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	(%r13), %rax
	testq	%rax, %rax
	movq	%r15, %r14
	movq	(%rsp), %r15            # 8-byte Reload
	je	.LBB17_9
	.p2align	4, 0x90
.LBB17_8:                               # %.lr.ph.i.i
                                        #   Parent Loop BB17_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB17_8
.LBB17_9:                               # %sort_LinkDelete.exit
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	24(%r13), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	32(%r13), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	40(%r13), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	memory_ARRAY+512(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	memory_ARRAY+512(%rip), %rax
	movq	%r13, (%rax)
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	callq	clause_Delete
	testq	%rbx, %rbx
	je	.LBB17_11
	.p2align	4, 0x90
.LBB17_10:                              # %.lr.ph.i66
                                        #   Parent Loop BB17_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB17_10
.LBB17_11:                              # %list_Delete.exit67
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB17_3
# BB#12:                                # %._crit_edge81.loopexit
	movq	32008(%r14), %rdi
	jmp	.LBB17_13
.LBB17_1:
	xorl	%edi, %edi
.LBB17_13:                              # %._crit_edge81
	xorl	%r12d, %r12d
	xorl	%esi, %esi
	callq	list_PointerDeleteElement
	movq	%rax, 32008(%r14)
	movq	32016(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB17_23
	.p2align	4, 0x90
.LBB17_14:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_16 Depth 2
                                        #     Child Loop BB17_18 Depth 2
                                        #     Child Loop BB17_20 Depth 2
	movq	8(%rbx), %rbp
	cmpq	%r15, 8(%rbp)
	jne	.LBB17_21
# BB#15:                                #   in Loop: Header=BB17_14 Depth=1
	movq	$0, 8(%rbx)
	movq	(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r13
	movq	8(%r13), %rax
	movq	8(%rax), %r12
	movq	(%r14), %rdi
	movq	cont_LEFTCONTEXT(%rip), %rcx
	movq	%r13, %rsi
	movq	%r13, %rdx
	callq	st_EntryDelete
	movq	(%r14), %rdi
	movq	cont_LEFTCONTEXT(%rip), %rcx
	movq	%r12, %rsi
	movq	%r12, %rdx
	callq	st_EntryDelete
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.LBB17_17
	.p2align	4, 0x90
.LBB17_16:                              # %.lr.ph.i61
                                        #   Parent Loop BB17_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB17_16
.LBB17_17:                              # %list_Delete.exit62
                                        #   in Loop: Header=BB17_14 Depth=1
	movq	8(%r13), %rax
	testq	%rax, %rax
	je	.LBB17_19
	.p2align	4, 0x90
.LBB17_18:                              # %.lr.ph.i56
                                        #   Parent Loop BB17_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB17_18
.LBB17_19:                              # %list_Delete.exit57
                                        #   in Loop: Header=BB17_14 Depth=1
	movq	$0, 8(%r13)
	movq	$0, 8(%r12)
	movq	(%rbp), %rax
	movq	8(%rax), %rdi
	callq	clause_Delete
	testq	%rbp, %rbp
	je	.LBB17_21
	.p2align	4, 0x90
.LBB17_20:                              # %.lr.ph.i
                                        #   Parent Loop BB17_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB17_20
.LBB17_21:                              # %list_Delete.exit
                                        #   in Loop: Header=BB17_14 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB17_14
# BB#22:                                # %._crit_edge.loopexit
	movq	32016(%r14), %r12
.LBB17_23:                              # %._crit_edge
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	list_PointerDeleteElement
	movq	%rax, 32016(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	sort_TheoryDeleteClause, .Lfunc_end17-sort_TheoryDeleteClause
	.cfi_endproc

	.globl	sort_ConditionCreate
	.p2align	4, 0x90
	.type	sort_ConditionCreate,@function
sort_ConditionCreate:                   # @sort_ConditionCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 48
.Lcfi97:
	.cfi_offset %rbx, -48
.Lcfi98:
	.cfi_offset %r12, -40
.Lcfi99:
	.cfi_offset %r14, -32
.Lcfi100:
	.cfi_offset %r15, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$40, %edi
	callq	memory_Malloc
	movl	%ebp, (%rax)
	movq	%rbx, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%r15, 24(%rax)
	movq	%r14, 32(%rax)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	sort_ConditionCreate, .Lfunc_end18-sort_ConditionCreate
	.cfi_endproc

	.globl	sort_ConditionNormalize
	.p2align	4, 0x90
	.type	sort_ConditionNormalize,@function
sort_ConditionNormalize:                # @sort_ConditionNormalize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi106:
	.cfi_def_cfa_offset 48
.Lcfi107:
	.cfi_offset %rbx, -40
.Lcfi108:
	.cfi_offset %r14, -32
.Lcfi109:
	.cfi_offset %r15, -24
.Lcfi110:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	term_StartMinRenaming
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB19_3
	.p2align	4, 0x90
.LBB19_1:                               # %.lr.ph79
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Rename
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB19_1
.LBB19_3:                               # %._crit_edge80
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB19_6
	.p2align	4, 0x90
.LBB19_4:                               # %.lr.ph74
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Rename
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB19_4
.LBB19_6:                               # %._crit_edge75
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB19_9
	.p2align	4, 0x90
.LBB19_7:                               # %.lr.ph69
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Rename
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB19_7
.LBB19_9:                               # %._crit_edge70
	movl	symbol_STANDARDVARCOUNTER(%rip), %r15d
	incl	%r15d
	movl	%r15d, symbol_STANDARDVARCOUNTER(%rip)
	movl	(%r14), %edi
	callq	term_GetRenamedVarSymbol
	movl	%eax, %ebx
	movq	8(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB19_11
	.p2align	4, 0x90
.LBB19_17:                              # %.lr.ph65
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movl	%ebx, %esi
	movl	%r15d, %edx
	callq	term_ExchangeVariable
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB19_17
.LBB19_11:                              # %.preheader56
	movq	16(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB19_13
	.p2align	4, 0x90
.LBB19_18:                              # %.lr.ph61
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movl	%ebx, %esi
	movl	%r15d, %edx
	callq	term_ExchangeVariable
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB19_18
.LBB19_13:                              # %.preheader
	movq	24(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB19_16
	.p2align	4, 0x90
.LBB19_14:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movl	%ebx, %esi
	movl	%r15d, %edx
	callq	term_ExchangeVariable
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB19_14
.LBB19_16:                              # %._crit_edge
	movl	%r15d, (%r14)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	sort_ConditionNormalize, .Lfunc_end19-sort_ConditionNormalize
	.cfi_endproc

	.globl	sort_ConditionCreateNoResidues
	.p2align	4, 0x90
	.type	sort_ConditionCreateNoResidues,@function
sort_ConditionCreateNoResidues:         # @sort_ConditionCreateNoResidues
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 16
.Lcfi112:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$40, %edi
	callq	memory_Malloc
	movl	$1, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	movq	$0, 24(%rax)
	movq	%rbx, 32(%rax)
	popq	%rbx
	retq
.Lfunc_end20:
	.size	sort_ConditionCreateNoResidues, .Lfunc_end20-sort_ConditionCreateNoResidues
	.cfi_endproc

	.globl	sort_ExtendConditionByLink
	.p2align	4, 0x90
	.type	sort_ExtendConditionByLink,@function
sort_ExtendConditionByLink:             # @sort_ExtendConditionByLink
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi116:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi117:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi119:
	.cfi_def_cfa_offset 80
.Lcfi120:
	.cfi_offset %rbx, -56
.Lcfi121:
	.cfi_offset %r12, -48
.Lcfi122:
	.cfi_offset %r13, -40
.Lcfi123:
	.cfi_offset %r14, -32
.Lcfi124:
	.cfi_offset %r15, -24
.Lcfi125:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r15
	movl	(%r15), %edi
	callq	term_StartMaxRenaming
	movq	24(%rbp), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r13
	movq	32(%rbp), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r14
	movq	40(%rbp), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, (%rsp)            # 8-byte Spill
	testq	%r13, %r13
	je	.LBB21_3
# BB#1:
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB21_2:                               # %.lr.ph156
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Rename
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB21_2
.LBB21_3:                               # %.preheader128
	testq	%r14, %r14
	je	.LBB21_6
# BB#4:
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB21_5:                               # %.lr.ph153
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Rename
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB21_5
.LBB21_6:                               # %.preheader127
	movq	%r14, 8(%rsp)           # 8-byte Spill
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB21_9
# BB#7:
	movq	(%rsp), %rbx            # 8-byte Reload
	.p2align	4, 0x90
.LBB21_8:                               # %.lr.ph150
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Rename
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB21_8
.LBB21_9:                               # %._crit_edge151
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	48(%rbp), %edi
	callq	term_GetRenamedVarSymbol
	movl	%eax, %r14d
	movl	symbol_STANDARDVARCOUNTER(%rip), %r12d
	incl	%r12d
	testq	%r13, %r13
	movl	%r12d, symbol_STANDARDVARCOUNTER(%rip)
	je	.LBB21_12
# BB#10:
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB21_11:                              # %.lr.ph148
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	%r14d, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB21_11
.LBB21_12:                              # %.preheader126
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	je	.LBB21_15
# BB#13:
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB21_14:                              # %.lr.ph145
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	%r14d, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB21_14
.LBB21_15:                              # %.preheader
	testq	%rbp, %rbp
	je	.LBB21_18
# BB#16:
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB21_17:                              # %.lr.ph142
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	%r14d, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB21_17
.LBB21_18:                              # %._crit_edge143
	movl	(%r15), %ebx
	movq	8(%r15), %rbp
	testq	%rbp, %rbp
	je	.LBB21_21
	.p2align	4, 0x90
.LBB21_19:                              # %.lr.ph139
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movl	%ebx, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB21_19
.LBB21_21:                              # %._crit_edge140
	movq	16(%r15), %rbp
	testq	%rbp, %rbp
	movq	16(%rsp), %r14          # 8-byte Reload
	je	.LBB21_23
	.p2align	4, 0x90
.LBB21_22:                              # %.lr.ph134
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movl	%ebx, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB21_22
.LBB21_23:                              # %._crit_edge135
	movq	24(%r15), %rbp
	testq	%rbp, %rbp
	je	.LBB21_26
	.p2align	4, 0x90
.LBB21_24:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movl	%ebx, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB21_24
.LBB21_26:                              # %._crit_edge
	movq	8(%r15), %rax
	testq	%rax, %rax
	movq	%rax, %rcx
	cmoveq	%r13, %rcx
	je	.LBB21_27
# BB#28:                                # %._crit_edge
	testq	%r13, %r13
	movq	(%rsp), %rsi            # 8-byte Reload
	je	.LBB21_32
# BB#29:                                # %.preheader.i111.preheader
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB21_30:                              # %.preheader.i111
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB21_30
# BB#31:
	movq	%r13, (%rcx)
	movq	%rax, %rcx
	jmp	.LBB21_32
.LBB21_27:
	movq	(%rsp), %rsi            # 8-byte Reload
.LBB21_32:                              # %list_Nconc.exit113
	movq	%rcx, 8(%r15)
	movq	16(%r15), %rax
	testq	%rax, %rax
	movq	%rax, %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	cmoveq	%rdx, %rcx
	je	.LBB21_37
# BB#33:                                # %list_Nconc.exit113
	testq	%rdx, %rdx
	je	.LBB21_37
# BB#34:                                # %.preheader.i105.preheader
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB21_35:                              # %.preheader.i105
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB21_35
# BB#36:
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rdx, (%rcx)
	movq	%rax, %rcx
.LBB21_37:                              # %list_Nconc.exit107
	movq	%rcx, 16(%r15)
	movq	24(%r15), %rax
	testq	%rax, %rax
	movq	%rax, %rcx
	cmoveq	%rsi, %rcx
	je	.LBB21_42
# BB#38:                                # %list_Nconc.exit107
	testq	%rsi, %rsi
	je	.LBB21_42
# BB#39:                                # %.preheader.i.preheader
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB21_40:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB21_40
# BB#41:
	movq	%rsi, (%rcx)
	movq	%rax, %rcx
.LBB21_42:                              # %list_Nconc.exit
	movq	%rcx, 24(%r15)
	movq	56(%r14), %rbx
	movq	32(%r15), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 32(%r15)
	movl	%r12d, (%r15)
	movq	%r15, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sort_ConditionNormalize # TAILCALL
.Lfunc_end21:
	.size	sort_ExtendConditionByLink, .Lfunc_end21-sort_ExtendConditionByLink
	.cfi_endproc

	.globl	sort_ExtendConditionByCondition
	.p2align	4, 0x90
	.type	sort_ExtendConditionByCondition,@function
sort_ExtendConditionByCondition:        # @sort_ExtendConditionByCondition
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi126:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi127:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi128:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi129:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi130:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi132:
	.cfi_def_cfa_offset 96
.Lcfi133:
	.cfi_offset %rbx, -56
.Lcfi134:
	.cfi_offset %r12, -48
.Lcfi135:
	.cfi_offset %r13, -40
.Lcfi136:
	.cfi_offset %r14, -32
.Lcfi137:
	.cfi_offset %r15, -24
.Lcfi138:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r15
	movl	(%r15), %edi
	callq	term_StartMaxRenaming
	movq	8(%r13), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %rbx
	movq	16(%r13), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	24(%r13), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %rbp
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	testq	%rbx, %rbx
	je	.LBB22_3
# BB#1:
	movq	16(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB22_2:                               # %.lr.ph159
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Rename
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_2
.LBB22_3:                               # %.preheader131
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB22_6
# BB#4:
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB22_5:                               # %.lr.ph156
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Rename
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_5
.LBB22_6:                               # %.preheader130
	testq	%rbp, %rbp
	je	.LBB22_9
# BB#7:
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB22_8:                               # %.lr.ph153
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Rename
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_8
.LBB22_9:                               # %._crit_edge154
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movl	(%r13), %edi
	callq	term_GetRenamedVarSymbol
	movl	%eax, %r14d
	movl	symbol_STANDARDVARCOUNTER(%rip), %r12d
	incl	%r12d
	movq	16(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	movl	%r12d, symbol_STANDARDVARCOUNTER(%rip)
	je	.LBB22_12
# BB#10:
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB22_11:                              # %.lr.ph151
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	%r14d, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_11
.LBB22_12:                              # %.preheader129
	movq	8(%rsp), %rax           # 8-byte Reload
	testq	%rax, %rax
	je	.LBB22_15
# BB#13:
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB22_14:                              # %.lr.ph148
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	%r14d, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_14
.LBB22_15:                              # %.preheader
	movq	%r13, 32(%rsp)          # 8-byte Spill
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB22_18
# BB#16:
	movq	24(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB22_17:                              # %.lr.ph145
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	%r14d, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_17
.LBB22_18:                              # %._crit_edge146
	movl	(%r15), %ebx
	movq	8(%r15), %rbp
	testq	%rbp, %rbp
	je	.LBB22_21
	.p2align	4, 0x90
.LBB22_19:                              # %.lr.ph142
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movl	%ebx, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB22_19
.LBB22_21:                              # %._crit_edge143
	movq	16(%r15), %rbp
	testq	%rbp, %rbp
	movq	24(%rsp), %r14          # 8-byte Reload
	je	.LBB22_23
	.p2align	4, 0x90
.LBB22_22:                              # %.lr.ph137
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movl	%ebx, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB22_22
.LBB22_23:                              # %._crit_edge138
	movq	24(%r15), %rbp
	testq	%rbp, %rbp
	je	.LBB22_26
	.p2align	4, 0x90
.LBB22_24:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movl	%ebx, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB22_24
.LBB22_26:                              # %._crit_edge
	movq	8(%r15), %rax
	testq	%rax, %rax
	movq	%rax, %rcx
	movq	16(%rsp), %rbp          # 8-byte Reload
	cmoveq	%rbp, %rcx
	je	.LBB22_27
# BB#28:                                # %._crit_edge
	testq	%rbp, %rbp
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	je	.LBB22_32
# BB#29:                                # %.preheader.i117.preheader
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB22_30:                              # %.preheader.i117
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB22_30
# BB#31:
	movq	%rbp, (%rcx)
	movq	%rax, %rcx
	jmp	.LBB22_32
.LBB22_27:
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB22_32:                              # %list_Nconc.exit119
	movq	%rcx, 8(%r15)
	movq	16(%r15), %rax
	testq	%rax, %rax
	movq	%rax, %rcx
	cmoveq	%rdi, %rcx
	je	.LBB22_37
# BB#33:                                # %list_Nconc.exit119
	testq	%rdi, %rdi
	je	.LBB22_37
# BB#34:                                # %.preheader.i111.preheader
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB22_35:                              # %.preheader.i111
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB22_35
# BB#36:
	movq	%rdi, (%rcx)
	movq	%rax, %rcx
.LBB22_37:                              # %list_Nconc.exit113
	movq	%rcx, 16(%r15)
	movq	24(%r15), %rax
	testq	%rax, %rax
	movq	%rax, %rcx
	cmoveq	%r14, %rcx
	je	.LBB22_42
# BB#38:                                # %list_Nconc.exit113
	testq	%r14, %r14
	je	.LBB22_42
# BB#39:                                # %.preheader.i105.preheader
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB22_40:                              # %.preheader.i105
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB22_40
# BB#41:
	movq	%r14, (%rcx)
	movq	%rax, %rcx
.LBB22_42:                              # %list_Nconc.exit107
	movq	%rcx, 24(%r15)
	movq	32(%rsi), %rdi
	callq	list_Copy
	movq	32(%r15), %rcx
	testq	%rax, %rax
	je	.LBB22_43
# BB#44:
	testq	%rcx, %rcx
	je	.LBB22_48
# BB#45:                                # %.preheader.i.preheader
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB22_46:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB22_46
# BB#47:
	movq	%rcx, (%rdx)
	jmp	.LBB22_48
.LBB22_43:
	movq	%rcx, %rax
.LBB22_48:                              # %list_Nconc.exit
	movq	%rax, 32(%r15)
	movl	%r12d, (%r15)
	movq	%r15, %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sort_ConditionNormalize # TAILCALL
.Lfunc_end22:
	.size	sort_ExtendConditionByCondition, .Lfunc_end22-sort_ExtendConditionByCondition
	.cfi_endproc

	.globl	sort_ExtendConditions
	.p2align	4, 0x90
	.type	sort_ExtendConditions,@function
sort_ExtendConditions:                  # @sort_ExtendConditions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi139:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi140:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi141:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi142:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi143:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi145:
	.cfi_def_cfa_offset 144
.Lcfi146:
	.cfi_offset %rbx, -56
.Lcfi147:
	.cfi_offset %r12, -48
.Lcfi148:
	.cfi_offset %r13, -40
.Lcfi149:
	.cfi_offset %r14, -32
.Lcfi150:
	.cfi_offset %r15, -24
.Lcfi151:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB23_1
# BB#2:                                 # %.lr.ph187
	xorl	%r14d, %r14d
	movq	%r15, 64(%rsp)          # 8-byte Spill
	jmp	.LBB23_3
.LBB23_6:                               # %sort_LinkNoResidues.exit
                                        #   in Loop: Header=BB23_3 Depth=1
	movl	(%rbp), %r13d
	cmpq	$0, 40(%r15)
	jne	.LBB23_7
# BB#39:                                #   in Loop: Header=BB23_3 Depth=1
	movq	56(%r15), %r12
	movq	32(%rbp), %rdi
	callq	list_Copy
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%r12, 8(%r14)
	movq	%rbp, (%r14)
	movl	$40, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movl	%r13d, (%rbp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 8(%rbp)
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rbp)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 24(%rbp)
	movq	%r14, 32(%rbp)
	jmp	.LBB23_37
	.p2align	4, 0x90
.LBB23_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_40 Depth 2
                                        #     Child Loop BB23_41 Depth 2
                                        #     Child Loop BB23_10 Depth 2
                                        #     Child Loop BB23_42 Depth 2
                                        #     Child Loop BB23_43 Depth 2
                                        #     Child Loop BB23_14 Depth 2
                                        #     Child Loop BB23_44 Depth 2
                                        #     Child Loop BB23_45 Depth 2
                                        #     Child Loop BB23_19 Depth 2
                                        #     Child Loop BB23_23 Depth 2
                                        #     Child Loop BB23_29 Depth 2
                                        #     Child Loop BB23_34 Depth 2
	movq	8(%rbx), %rbp
	movq	8(%rbp), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rbp), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r12
	movq	24(%rbp), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpq	$0, 24(%r15)
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	%r12, 40(%rsp)          # 8-byte Spill
	jne	.LBB23_5
# BB#4:                                 #   in Loop: Header=BB23_3 Depth=1
	cmpq	$0, 32(%r15)
	je	.LBB23_6
.LBB23_5:                               # %sort_LinkNoResidues.exit.thread
                                        #   in Loop: Header=BB23_3 Depth=1
	movl	(%rbp), %r13d
.LBB23_7:                               #   in Loop: Header=BB23_3 Depth=1
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%r13d, %edi
	callq	term_StartMaxRenaming
	movq	24(%r15), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r13
	movq	32(%r15), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r14
	movq	40(%r15), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testq	%r13, %r13
	movq	%r13, %rbx
	je	.LBB23_8
	.p2align	4, 0x90
.LBB23_40:                              # %.lr.ph
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	callq	term_Rename
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB23_40
.LBB23_8:                               # %.preheader162
                                        #   in Loop: Header=BB23_3 Depth=1
	testq	%r14, %r14
	movq	%r14, %rbx
	je	.LBB23_9
	.p2align	4, 0x90
.LBB23_41:                              # %.lr.ph165
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	callq	term_Rename
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB23_41
.LBB23_9:                               # %.preheader161
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.LBB23_11
	.p2align	4, 0x90
.LBB23_10:                              # %.lr.ph167
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	callq	term_Rename
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB23_10
.LBB23_11:                              # %._crit_edge
                                        #   in Loop: Header=BB23_3 Depth=1
	movl	48(%r15), %edi
	callq	term_GetRenamedVarSymbol
	movl	%eax, %r12d
	movl	symbol_STANDARDVARCOUNTER(%rip), %r15d
	incl	%r15d
	testq	%r13, %r13
	movl	%r15d, symbol_STANDARDVARCOUNTER(%rip)
	movq	%r13, %rbx
	je	.LBB23_12
	.p2align	4, 0x90
.LBB23_42:                              # %.lr.ph170
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB23_42
.LBB23_12:                              # %.preheader160
                                        #   in Loop: Header=BB23_3 Depth=1
	testq	%r14, %r14
	movq	%r14, %rbx
	je	.LBB23_13
	.p2align	4, 0x90
.LBB23_43:                              # %.lr.ph172
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB23_43
.LBB23_13:                              # %.preheader159
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.LBB23_15
	.p2align	4, 0x90
.LBB23_14:                              # %.lr.ph174
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB23_14
.LBB23_15:                              # %._crit_edge175
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ebx
	testq	%rbp, %rbp
	je	.LBB23_17
	.p2align	4, 0x90
.LBB23_44:                              # %.lr.ph178
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rdi
	movl	%ebx, %esi
	movl	%r15d, %edx
	callq	term_ExchangeVariable
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB23_44
.LBB23_17:                              # %.preheader158
                                        #   in Loop: Header=BB23_3 Depth=1
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	movq	40(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rbp
	je	.LBB23_18
	.p2align	4, 0x90
.LBB23_45:                              # %.lr.ph180
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rdi
	movl	%ebx, %esi
	movl	%r15d, %edx
	callq	term_ExchangeVariable
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB23_45
.LBB23_18:                              # %.preheader
                                        #   in Loop: Header=BB23_3 Depth=1
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB23_20
	.p2align	4, 0x90
.LBB23_19:                              # %.lr.ph182
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rdi
	movl	%ebx, %esi
	movl	%r15d, %edx
	callq	term_ExchangeVariable
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB23_19
.LBB23_20:                              # %._crit_edge183
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	testq	%rdx, %rdx
	movq	%rdx, %rax
	cmoveq	%r13, %rax
	je	.LBB23_25
# BB#21:                                # %._crit_edge183
                                        #   in Loop: Header=BB23_3 Depth=1
	testq	%r13, %r13
	je	.LBB23_25
# BB#22:                                # %.preheader.i137.preheader
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB23_23:                              # %.preheader.i137
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB23_23
# BB#24:                                #   in Loop: Header=BB23_3 Depth=1
	movq	%r13, (%rax)
	movq	%rdx, %rax
.LBB23_25:                              # %list_Nconc.exit139
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	cmoveq	%r14, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB23_26
# BB#27:                                # %list_Nconc.exit139
                                        #   in Loop: Header=BB23_3 Depth=1
	testq	%r14, %r14
	movq	32(%rsp), %rdx          # 8-byte Reload
	je	.LBB23_31
# BB#28:                                # %.preheader.i131.preheader
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB23_29:                              # %.preheader.i131
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB23_29
# BB#30:                                #   in Loop: Header=BB23_3 Depth=1
	movq	%r14, (%rax)
	movq	%r12, 16(%rsp)          # 8-byte Spill
	jmp	.LBB23_31
	.p2align	4, 0x90
.LBB23_26:                              #   in Loop: Header=BB23_3 Depth=1
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB23_31:                              # %list_Nconc.exit133
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	48(%rsp), %r12          # 8-byte Reload
	testq	%r12, %r12
	cmoveq	%rdx, %r12
	je	.LBB23_36
# BB#32:                                # %list_Nconc.exit133
                                        #   in Loop: Header=BB23_3 Depth=1
	testq	%rdx, %rdx
	je	.LBB23_36
# BB#33:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB23_34:                              # %.preheader.i
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB23_34
# BB#35:                                #   in Loop: Header=BB23_3 Depth=1
	movq	%rdx, (%rax)
	movq	24(%rsp), %r12          # 8-byte Reload
.LBB23_36:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	56(%rbx), %r14
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	32(%rax), %rdi
	callq	list_Copy
	movq	%rax, %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r14, 8(%rbp)
	movq	%r13, (%rbp)
	movl	$40, %edi
	callq	memory_Malloc
	movl	%r15d, (%rax)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%rbx, %r15
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 16(%rax)
	movq	%r12, 24(%rax)
	movq	%rbp, 32(%rax)
	movq	%rax, %rdi
	callq	sort_ConditionNormalize
	movq	%rax, %rbp
	movq	72(%rsp), %rbx          # 8-byte Reload
.LBB23_37:                              #   in Loop: Header=BB23_3 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r14
	jne	.LBB23_3
	jmp	.LBB23_38
.LBB23_1:
	xorl	%eax, %eax
.LBB23_38:                              # %._crit_edge188
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	sort_ExtendConditions, .Lfunc_end23-sort_ExtendConditions
	.cfi_endproc

	.globl	sort_ConditionsUnion
	.p2align	4, 0x90
	.type	sort_ConditionsUnion,@function
sort_ConditionsUnion:                   # @sort_ConditionsUnion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi152:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi155:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi156:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi158:
	.cfi_def_cfa_offset 128
.Lcfi159:
	.cfi_offset %rbx, -56
.Lcfi160:
	.cfi_offset %r12, -48
.Lcfi161:
	.cfi_offset %r13, -40
.Lcfi162:
	.cfi_offset %r14, -32
.Lcfi163:
	.cfi_offset %r15, -24
.Lcfi164:
	.cfi_offset %rbp, -16
	testq	%rdi, %rdi
	je	.LBB24_44
# BB#1:
	movq	(%rdi), %r15
	movq	8(%rdi), %rbx
	movl	(%rbx), %ebp
	movq	8(%rbx), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	16(%rbx), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	24(%rbx), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r13
	movq	32(%rbx), %rdi
	callq	list_Copy
	movq	%rax, %r12
	testq	%r15, %r15
	je	.LBB24_2
	.p2align	4, 0x90
.LBB24_3:                               # %.lr.ph198
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_45 Depth 2
                                        #     Child Loop BB24_46 Depth 2
                                        #     Child Loop BB24_9 Depth 2
                                        #     Child Loop BB24_47 Depth 2
                                        #     Child Loop BB24_48 Depth 2
                                        #     Child Loop BB24_49 Depth 2
                                        #     Child Loop BB24_50 Depth 2
                                        #     Child Loop BB24_51 Depth 2
                                        #     Child Loop BB24_16 Depth 2
                                        #     Child Loop BB24_21 Depth 2
                                        #     Child Loop BB24_26 Depth 2
                                        #     Child Loop BB24_32 Depth 2
                                        #     Child Loop BB24_40 Depth 2
	movq	8(%r15), %rbx
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB24_6
# BB#4:                                 #   in Loop: Header=BB24_3 Depth=1
	cmpq	$0, 16(%rbx)
	jne	.LBB24_6
# BB#5:                                 # %sort_ConditionNoResidues.exit
                                        #   in Loop: Header=BB24_3 Depth=1
	cmpq	$0, 24(%rbx)
	je	.LBB24_36
	.p2align	4, 0x90
.LBB24_6:                               # %sort_ConditionNoResidues.exit.thread
                                        #   in Loop: Header=BB24_3 Depth=1
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	24(%rbx), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%ebp, %edi
	callq	term_StartMaxRenaming
	testq	%r14, %r14
	movq	%r14, %rbx
	je	.LBB24_7
	.p2align	4, 0x90
.LBB24_45:                              # %.lr.ph
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	callq	term_Rename
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB24_45
.LBB24_7:                               # %.preheader171
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	testq	%rax, %rax
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rcx, %rbx
	je	.LBB24_8
	.p2align	4, 0x90
.LBB24_46:                              # %.lr.ph174
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	callq	term_Rename
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB24_46
.LBB24_8:                               # %.preheader170
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	%r13, 64(%rsp)          # 8-byte Spill
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.LBB24_10
	.p2align	4, 0x90
.LBB24_9:                               # %.lr.ph176
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	callq	term_Rename
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB24_9
.LBB24_10:                              # %._crit_edge
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edi
	callq	term_GetRenamedVarSymbol
	movl	%eax, %r13d
	movl	symbol_STANDARDVARCOUNTER(%rip), %r12d
	incl	%r12d
	testq	%r14, %r14
	movl	%r12d, symbol_STANDARDVARCOUNTER(%rip)
	movq	%r14, %r15
	je	.LBB24_11
	.p2align	4, 0x90
.LBB24_47:                              # %.lr.ph179
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r15), %rdi
	movl	%r13d, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB24_47
.LBB24_11:                              # %.preheader169
                                        #   in Loop: Header=BB24_3 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movq	48(%rsp), %rbx          # 8-byte Reload
	je	.LBB24_12
	.p2align	4, 0x90
.LBB24_48:                              # %.lr.ph181
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movl	%r13d, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB24_48
.LBB24_12:                              # %.preheader168
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	testq	%rax, %rax
	movq	%rax, %rbx
	movq	16(%rsp), %r15          # 8-byte Reload
	je	.LBB24_13
	.p2align	4, 0x90
.LBB24_49:                              # %.lr.ph183
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movl	%r13d, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB24_49
.LBB24_13:                              # %.preheader167
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.LBB24_14
	.p2align	4, 0x90
.LBB24_50:                              # %.lr.ph185
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movl	%ebp, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB24_50
.LBB24_14:                              # %.preheader166
                                        #   in Loop: Header=BB24_3 Depth=1
	testq	%r15, %r15
	movq	%r15, %rbx
	movq	64(%rsp), %r13          # 8-byte Reload
	je	.LBB24_15
	.p2align	4, 0x90
.LBB24_51:                              # %.lr.ph187
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movl	%ebp, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB24_51
.LBB24_15:                              # %.preheader
                                        #   in Loop: Header=BB24_3 Depth=1
	testq	%r13, %r13
	movq	%r13, %rbx
	je	.LBB24_17
	.p2align	4, 0x90
.LBB24_16:                              # %.lr.ph189
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movl	%ebp, %esi
	movl	%r12d, %edx
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB24_16
.LBB24_17:                              # %._crit_edge190
                                        #   in Loop: Header=BB24_3 Depth=1
	testq	%r14, %r14
	movq	%r14, %rax
	movq	(%rsp), %rdx            # 8-byte Reload
	cmoveq	%rdx, %rax
	je	.LBB24_18
# BB#19:                                # %._crit_edge190
                                        #   in Loop: Header=BB24_3 Depth=1
	testq	%rdx, %rdx
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	je	.LBB24_23
# BB#20:                                # %.preheader.i152.preheader
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB24_21:                              # %.preheader.i152
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB24_21
# BB#22:                                #   in Loop: Header=BB24_3 Depth=1
	movq	%rdx, (%rax)
	movq	%r14, %rax
	jmp	.LBB24_23
	.p2align	4, 0x90
.LBB24_18:                              #   in Loop: Header=BB24_3 Depth=1
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
.LBB24_23:                              # %list_Nconc.exit154
                                        #   in Loop: Header=BB24_3 Depth=1
	testq	%rsi, %rsi
	cmoveq	%r15, %rsi
	je	.LBB24_28
# BB#24:                                # %list_Nconc.exit154
                                        #   in Loop: Header=BB24_3 Depth=1
	testq	%r15, %r15
	je	.LBB24_28
# BB#25:                                # %.preheader.i146.preheader
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB24_26:                              # %.preheader.i146
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB24_26
# BB#27:                                #   in Loop: Header=BB24_3 Depth=1
	movq	%r15, (%rcx)
	movq	48(%rsp), %rsi          # 8-byte Reload
.LBB24_28:                              # %list_Nconc.exit148
                                        #   in Loop: Header=BB24_3 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB24_29
# BB#30:                                #   in Loop: Header=BB24_3 Depth=1
	testq	%r13, %r13
	je	.LBB24_34
# BB#31:                                # %.preheader.i140.preheader
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	.p2align	4, 0x90
.LBB24_32:                              # %.preheader.i140
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB24_32
# BB#33:                                #   in Loop: Header=BB24_3 Depth=1
	movq	%r13, (%rcx)
.LBB24_34:                              # %list_Nconc.exit142
                                        #   in Loop: Header=BB24_3 Depth=1
	movl	%r12d, %ebp
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB24_35
	.p2align	4, 0x90
.LBB24_29:                              #   in Loop: Header=BB24_3 Depth=1
	movl	%r12d, %ebp
	movq	%rsi, 16(%rsp)          # 8-byte Spill
.LBB24_35:                              # %list_Nconc.exit142
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB24_36:                              # %list_Nconc.exit142
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	32(%rbx), %rdi
	callq	list_Copy
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB24_37
# BB#38:                                #   in Loop: Header=BB24_3 Depth=1
	testq	%r12, %r12
	je	.LBB24_42
# BB#39:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB24_40:                              # %.preheader.i
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB24_40
# BB#41:                                #   in Loop: Header=BB24_3 Depth=1
	movq	%r12, (%rax)
	jmp	.LBB24_42
	.p2align	4, 0x90
.LBB24_37:                              #   in Loop: Header=BB24_3 Depth=1
	movq	%r12, %rbx
.LBB24_42:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	movq	%rbx, %r12
	jne	.LBB24_3
	jmp	.LBB24_43
.LBB24_44:
	movl	$40, %edi
	callq	memory_Malloc
	movl	$1, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rax)
	movups	%xmm0, 8(%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_2:
	movq	%r12, %rbx
.LBB24_43:                              # %._crit_edge199
	movl	$40, %edi
	callq	memory_Malloc
	movl	%ebp, (%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 16(%rax)
	movq	%r13, 24(%rax)
	movq	%rbx, 32(%rax)
	movq	%rax, %rdi
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sort_ConditionNormalize # TAILCALL
.Lfunc_end24:
	.size	sort_ConditionsUnion, .Lfunc_end24-sort_ConditionsUnion
	.cfi_endproc

	.globl	sort_ConditionCopy
	.p2align	4, 0x90
	.type	sort_ConditionCopy,@function
sort_ConditionCopy:                     # @sort_ConditionCopy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi165:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi166:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi167:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi168:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi169:
	.cfi_def_cfa_offset 48
.Lcfi170:
	.cfi_offset %rbx, -48
.Lcfi171:
	.cfi_offset %r12, -40
.Lcfi172:
	.cfi_offset %r14, -32
.Lcfi173:
	.cfi_offset %r15, -24
.Lcfi174:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	(%rbx), %ebp
	movq	8(%rbx), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r15
	movq	24(%rbx), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r12
	movq	32(%rbx), %rdi
	callq	list_Copy
	movq	%rax, %rbx
	movl	$40, %edi
	callq	memory_Malloc
	movl	%ebp, (%rax)
	movq	%r14, 8(%rax)
	movq	%r15, 16(%rax)
	movq	%r12, 24(%rax)
	movq	%rbx, 32(%rax)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	sort_ConditionCopy, .Lfunc_end25-sort_ConditionCopy
	.cfi_endproc

	.globl	sort_IsBaseSortSymbol
	.p2align	4, 0x90
	.type	sort_IsBaseSortSymbol,@function
sort_IsBaseSortSymbol:                  # @sort_IsBaseSortSymbol
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	js	.LBB26_2
# BB#1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB26_2:                               # %symbol_IsPredicate.exit
	negl	%edi
	movl	symbol_TYPEMASK(%rip), %eax
	andl	%edi, %eax
	cmpl	$2, %eax
	jne	.LBB26_3
# BB#4:
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %edi
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	cmpl	$1, 16(%rax)
	sete	%al
	movzbl	%al, %eax
	retq
.LBB26_3:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.Lfunc_end26:
	.size	sort_IsBaseSortSymbol, .Lfunc_end26-sort_IsBaseSortSymbol
	.cfi_endproc

	.globl	sort_TheorySortOfSymbol
	.p2align	4, 0x90
	.type	sort_TheorySortOfSymbol,@function
sort_TheorySortOfSymbol:                # @sort_TheorySortOfSymbol
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi175:
	.cfi_def_cfa_offset 16
.Lcfi176:
	.cfi_offset %rbx, -16
	negl	%esi
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %esi
	movslq	%esi, %rax
	movq	8(%rdi,%rax,8), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	popq	%rbx
	retq
.Lfunc_end27:
	.size	sort_TheorySortOfSymbol, .Lfunc_end27-sort_TheorySortOfSymbol
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI28_0:
	.zero	16
	.text
	.globl	sort_TheoryIsSubsortOfNoResidues
	.p2align	4, 0x90
	.type	sort_TheoryIsSubsortOfNoResidues,@function
sort_TheoryIsSubsortOfNoResidues:       # @sort_TheoryIsSubsortOfNoResidues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi177:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi178:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi179:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi180:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi181:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi182:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi183:
	.cfi_def_cfa_offset 80
.Lcfi184:
	.cfi_offset %rbx, -56
.Lcfi185:
	.cfi_offset %r12, -48
.Lcfi186:
	.cfi_offset %r13, -40
.Lcfi187:
	.cfi_offset %r14, -32
.Lcfi188:
	.cfi_offset %r15, -24
.Lcfi189:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r13
	movq	%rdi, %r12
	movl	32024(%r12), %eax
	cmpl	$-1, %eax
	jne	.LBB28_4
# BB#1:                                 # %.preheader.i.preheader
	movl	$5, %eax
	.p2align	4, 0x90
.LBB28_2:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-32(%r12,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-24(%r12,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-16(%r12,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-8(%r12,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	(%r12,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	addq	$5, %rax
	cmpq	$4005, %rax             # imm = 0xFA5
	jne	.LBB28_2
# BB#3:
	movl	$0, 32024(%r12)
	xorl	%eax, %eax
.LBB28_4:                               # %sort_TheoryIncrementMark.exit
	incl	%eax
	movl	%eax, 32024(%r12)
	movq	32008(%r12), %rax
	testq	%rax, %rax
	je	.LBB28_6
	.p2align	4, 0x90
.LBB28_5:                               # %.lr.ph108
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rcx
	movl	16(%rcx), %edx
	movl	%edx, 20(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB28_5
.LBB28_6:                               # %.preheader95
	testq	%r13, %r13
	je	.LBB28_9
# BB#7:
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB28_8:                               # %.lr.ph105
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %r14
	movq	24(%r14), %rdi
	movl	$sort_ConditionDelete, %esi
	callq	list_DeleteWithElement
	movl	$40, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movl	$1, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movups	%xmm0, 8(%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 24(%r14)
	movl	32024(%r12), %eax
	movl	%eax, 8(%r14)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB28_8
.LBB28_9:                               # %.preheader
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	32008(%r12), %rbx
	testq	%rbx, %rbx
	movq	%r12, (%rsp)            # 8-byte Spill
	je	.LBB28_22
# BB#10:                                # %.lr.ph102.preheader
	movq	%r13, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB28_11:                              # %.lr.ph102
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rbp
	cmpq	$0, (%rbp)
	jne	.LBB28_16
# BB#12:                                #   in Loop: Header=BB28_11 Depth=1
	cmpq	$0, 24(%rbp)
	jne	.LBB28_16
# BB#13:                                #   in Loop: Header=BB28_11 Depth=1
	cmpq	$0, 32(%rbp)
	jne	.LBB28_16
# BB#14:                                # %sort_LinkNoResidues.exit
                                        #   in Loop: Header=BB28_11 Depth=1
	cmpq	$0, 40(%rbp)
	jne	.LBB28_16
# BB#15:                                #   in Loop: Header=BB28_11 Depth=1
	movq	8(%rbp), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%r15, 8(%r12)
	movq	%r13, (%r12)
	movq	24(%r15), %rdi
	movl	$sort_ConditionDelete, %esi
	callq	list_DeleteWithElement
	movq	56(%rbp), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbp, 8(%r14)
	movq	$0, (%r14)
	movl	$40, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movl	$1, (%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbp)
	movq	$0, 24(%rbp)
	movq	%r14, 32(%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 24(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	32024(%rax), %eax
	movl	%eax, 8(%r15)
	movq	%r12, %r13
	movq	(%rsp), %r12            # 8-byte Reload
	.p2align	4, 0x90
.LBB28_16:                              # %sort_LinkNoResidues.exit.thread
                                        #   in Loop: Header=BB28_11 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB28_11
# BB#17:                                # %._crit_edge
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	list_Copy
	testq	%r13, %r13
	je	.LBB28_23
# BB#18:
	testq	%rax, %rax
	je	.LBB28_25
# BB#19:                                # %.preheader.i.i82.preheader
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB28_20:                              # %.preheader.i.i82
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB28_20
# BB#21:
	movq	%rax, (%rcx)
	testq	%r13, %r13
	jne	.LBB28_25
	jmp	.LBB28_44
.LBB28_22:                              # %._crit_edge.thread
	movq	%r13, %rdi
	callq	list_Copy
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB28_25
	jmp	.LBB28_44
.LBB28_23:
	movq	%rax, %r13
.LBB28_24:                              # %sort_Intersect.exit
	testq	%r13, %r13
	je	.LBB28_44
.LBB28_25:                              # %.lr.ph75.i
	movq	%r13, %rax
	movq	(%rax), %r13
	movq	8(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	movq	(%rcx), %rbp
	testq	%rbp, %rbp
	je	.LBB28_24
	.p2align	4, 0x90
.LBB28_28:                              # %.lr.ph71.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_35 Depth 2
                                        #       Child Loop BB28_39 Depth 3
	movq	8(%rbp), %rbx
	cmpq	$0, 24(%rbx)
	jne	.LBB28_27
# BB#29:                                #   in Loop: Header=BB28_28 Depth=1
	cmpq	$0, 32(%rbx)
	jne	.LBB28_27
# BB#30:                                # %sort_LinkNoResidues.exit.i
                                        #   in Loop: Header=BB28_28 Depth=1
	cmpq	$0, 40(%rbx)
	jne	.LBB28_27
# BB#31:                                #   in Loop: Header=BB28_28 Depth=1
	decl	20(%rbx)
	jne	.LBB28_27
# BB#32:                                #   in Loop: Header=BB28_28 Depth=1
	movq	8(%rbx), %r14
	movl	8(%r14), %eax
	cmpl	32024(%r12), %eax
	je	.LBB28_27
# BB#33:                                #   in Loop: Header=BB28_28 Depth=1
	movq	56(%rbx), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%r12, 8(%r15)
	movq	$0, (%r15)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB28_26
# BB#34:                                # %.lr.ph.i78.preheader
                                        #   in Loop: Header=BB28_28 Depth=1
	movq	%r15, %r12
	.p2align	4, 0x90
.LBB28_35:                              # %.lr.ph.i78
                                        #   Parent Loop BB28_28 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB28_39 Depth 3
	movq	8(%rbx), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.LBB28_42
# BB#36:                                #   in Loop: Header=BB28_35 Depth=2
	movq	8(%rax), %rax
	movq	32(%rax), %rdi
	callq	list_Copy
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB28_42
# BB#37:                                #   in Loop: Header=BB28_35 Depth=2
	testq	%r12, %r12
	je	.LBB28_43
# BB#38:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB28_35 Depth=2
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB28_39:                              # %.preheader.i.i
                                        #   Parent Loop BB28_28 Depth=1
                                        #     Parent Loop BB28_35 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB28_39
# BB#40:                                #   in Loop: Header=BB28_35 Depth=2
	movq	%r12, (%rax)
	jmp	.LBB28_43
	.p2align	4, 0x90
.LBB28_42:                              #   in Loop: Header=BB28_35 Depth=2
	movq	%r12, %r15
.LBB28_43:                              # %list_Nconc.exit.i
                                        #   in Loop: Header=BB28_35 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%r15, %r12
	jne	.LBB28_35
.LBB28_26:                              # %._crit_edge.i
                                        #   in Loop: Header=BB28_28 Depth=1
	movq	24(%r14), %rdi
	movl	$sort_ConditionDelete, %esi
	callq	list_DeleteWithElement
	movl	$40, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movl	$1, (%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$0, 24(%r12)
	movq	%r15, 32(%r12)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 24(%r14)
	movq	(%rsp), %r12            # 8-byte Reload
	movl	32024(%r12), %eax
	movl	%eax, 8(%r14)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, %r13
	.p2align	4, 0x90
.LBB28_27:                              # %sort_LinkNoResidues.exit.thread.i
                                        #   in Loop: Header=BB28_28 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB28_28
	jmp	.LBB28_24
.LBB28_44:                              # %sort_EvalSubsortNoResidues.exit.preheader
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB28_56
# BB#45:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB28_46:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_51 Depth 2
	movq	8(%rbp), %rax
	movl	8(%rax), %ecx
	cmpl	32024(%r12), %ecx
	jne	.LBB28_58
# BB#47:                                #   in Loop: Header=BB28_46 Depth=1
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.LBB28_54
# BB#48:                                #   in Loop: Header=BB28_46 Depth=1
	movq	8(%rax), %rax
	movq	32(%rax), %rdi
	callq	list_Copy
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB28_54
# BB#49:                                #   in Loop: Header=BB28_46 Depth=1
	testq	%rbx, %rbx
	je	.LBB28_55
# BB#50:                                # %.preheader.i77.preheader
                                        #   in Loop: Header=BB28_46 Depth=1
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB28_51:                              # %.preheader.i77
                                        #   Parent Loop BB28_46 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB28_51
# BB#52:                                #   in Loop: Header=BB28_46 Depth=1
	movq	%rbx, (%rax)
	jmp	.LBB28_55
	.p2align	4, 0x90
.LBB28_54:                              #   in Loop: Header=BB28_46 Depth=1
	movq	%rbx, %r14
.LBB28_55:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB28_46 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%r14, %rbx
	jne	.LBB28_46
	jmp	.LBB28_57
.LBB28_56:
	xorl	%r14d, %r14d
.LBB28_57:                              # %sort_EvalSubsortNoResidues.exit._crit_edge
	movl	$40, %edi
	callq	memory_Malloc
	movl	$1, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	movq	$0, 24(%rax)
	movq	%r14, 32(%rax)
	jmp	.LBB28_60
.LBB28_58:
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB28_60
	.p2align	4, 0x90
.LBB28_59:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB28_59
.LBB28_60:                              # %list_Delete.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	sort_TheoryIsSubsortOfNoResidues, .Lfunc_end28-sort_TheoryIsSubsortOfNoResidues
	.cfi_endproc

	.globl	sort_TheoryIsSubsortOf
	.p2align	4, 0x90
	.type	sort_TheoryIsSubsortOf,@function
sort_TheoryIsSubsortOf:                 # @sort_TheoryIsSubsortOf
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi190:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi191:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi192:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi193:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi194:
	.cfi_def_cfa_offset 48
.Lcfi195:
	.cfi_offset %rbx, -48
.Lcfi196:
	.cfi_offset %r12, -40
.Lcfi197:
	.cfi_offset %r13, -32
.Lcfi198:
	.cfi_offset %r14, -24
.Lcfi199:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rdi, %r15
	movl	32024(%r15), %eax
	cmpl	$-1, %eax
	jne	.LBB29_4
# BB#1:                                 # %.preheader.i.preheader
	movl	$5, %eax
	.p2align	4, 0x90
.LBB29_2:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-32(%r15,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-24(%r15,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-16(%r15,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-8(%r15,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	(%r15,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	addq	$5, %rax
	cmpq	$4005, %rax             # imm = 0xFA5
	jne	.LBB29_2
# BB#3:
	movl	$0, 32024(%r15)
	xorl	%eax, %eax
.LBB29_4:                               # %sort_TheoryIncrementMark.exit
	incl	%eax
	movl	%eax, 32024(%r15)
	movq	32008(%r15), %rax
	testq	%rax, %rax
	je	.LBB29_6
	.p2align	4, 0x90
.LBB29_23:                              # %.lr.ph44
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rcx
	movl	16(%rcx), %edx
	movl	%edx, 20(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB29_23
.LBB29_6:                               # %.preheader
	testq	%rsi, %rsi
	je	.LBB29_9
# BB#7:
	movq	%rsi, %rax
	.p2align	4, 0x90
.LBB29_8:                               # %.lr.ph40
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movl	32024(%r15), %edx
	movl	%edx, 8(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB29_8
.LBB29_9:                               # %._crit_edge41
	movq	%rsi, %rdi
	callq	list_Copy
	movq	%rax, %r12
	jmp	.LBB29_10
	.p2align	4, 0x90
.LBB29_11:                              #   in Loop: Header=BB29_10 Depth=1
	movq	%r12, %rax
	movq	%r12, %rcx
	movq	8(%rcx), %rdx
	movq	(%rax), %r12
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rax)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rcx, (%rax)
	movq	(%rdx), %rbx
	testq	%rbx, %rbx
	jne	.LBB29_13
	jmp	.LBB29_10
	.p2align	4, 0x90
.LBB29_16:                              #   in Loop: Header=BB29_13 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB29_10
.LBB29_13:                              # %.lr.ph.i
                                        #   Parent Loop BB29_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rax
	decl	20(%rax)
	jne	.LBB29_16
# BB#14:                                #   in Loop: Header=BB29_13 Depth=2
	movq	8(%rax), %r13
	movl	32024(%r15), %eax
	cmpl	%eax, 8(%r13)
	je	.LBB29_16
# BB#15:                                #   in Loop: Header=BB29_13 Depth=2
	movl	%eax, 8(%r13)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, %r12
	jmp	.LBB29_16
	.p2align	4, 0x90
.LBB29_10:                              # %._crit_edge41
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_13 Depth 2
	testq	%r12, %r12
	jne	.LBB29_11
# BB#17:                                # %sort_EvalSubsortSpecial.exit.preheader
	movl	$1, %eax
	testq	%r14, %r14
	je	.LBB29_22
# BB#18:                                # %.lr.ph
	movl	32024(%r15), %ecx
	.p2align	4, 0x90
.LBB29_19:                              # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rdx
	cmpl	%ecx, 8(%rdx)
	jne	.LBB29_20
# BB#21:                                # %sort_EvalSubsortSpecial.exit
                                        #   in Loop: Header=BB29_19 Depth=1
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB29_19
	jmp	.LBB29_22
.LBB29_20:
	xorl	%eax, %eax
.LBB29_22:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end29:
	.size	sort_TheoryIsSubsortOf, .Lfunc_end29-sort_TheoryIsSubsortOf
	.cfi_endproc

	.globl	sort_TheoryIsSubsortOfExtra
	.p2align	4, 0x90
	.type	sort_TheoryIsSubsortOfExtra,@function
sort_TheoryIsSubsortOfExtra:            # @sort_TheoryIsSubsortOfExtra
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi200:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi201:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi202:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi203:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi204:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi205:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi206:
	.cfi_def_cfa_offset 64
.Lcfi207:
	.cfi_offset %rbx, -56
.Lcfi208:
	.cfi_offset %r12, -48
.Lcfi209:
	.cfi_offset %r13, -40
.Lcfi210:
	.cfi_offset %r14, -32
.Lcfi211:
	.cfi_offset %r15, -24
.Lcfi212:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	movl	32024(%r12), %eax
	cmpl	$-1, %eax
	jne	.LBB30_4
# BB#1:                                 # %.preheader.i.preheader
	movl	$5, %eax
	.p2align	4, 0x90
.LBB30_2:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-32(%r12,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-24(%r12,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-16(%r12,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-8(%r12,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	(%r12,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	addq	$5, %rax
	cmpq	$4005, %rax             # imm = 0xFA5
	jne	.LBB30_2
# BB#3:
	movl	$0, 32024(%r12)
	xorl	%eax, %eax
.LBB30_4:                               # %sort_TheoryIncrementMark.exit
	incl	%eax
	movl	%eax, 32024(%r12)
	movq	32008(%r12), %rax
	testq	%rax, %rax
	je	.LBB30_6
	.p2align	4, 0x90
.LBB30_42:                              # %.lr.ph54
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rcx
	movl	16(%rcx), %esi
	movl	%esi, 20(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB30_42
.LBB30_6:                               # %.preheader
	testq	%rdx, %rdx
	je	.LBB30_9
# BB#7:
	movq	%rdx, %rax
	.p2align	4, 0x90
.LBB30_8:                               # %.lr.ph51
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movl	32024(%r12), %esi
	movl	%esi, 8(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB30_8
.LBB30_9:                               # %._crit_edge
	movq	%rdx, %rdi
	callq	list_Copy
	movq	%rax, %r13
	jmp	.LBB30_10
	.p2align	4, 0x90
.LBB30_11:                              #   in Loop: Header=BB30_10 Depth=1
	movq	%r13, %rax
	movq	%r13, %rcx
	movq	8(%rcx), %rdx
	movq	(%rax), %r13
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rax)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rcx, (%rax)
	movq	(%rdx), %rbp
	testq	%rbp, %rbp
	jne	.LBB30_13
	jmp	.LBB30_10
	.p2align	4, 0x90
.LBB30_16:                              #   in Loop: Header=BB30_13 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB30_10
.LBB30_13:                              # %.lr.ph.i38
                                        #   Parent Loop BB30_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rax
	decl	20(%rax)
	jne	.LBB30_16
# BB#14:                                #   in Loop: Header=BB30_13 Depth=2
	movq	8(%rax), %rbx
	movl	32024(%r12), %eax
	cmpl	%eax, 8(%rbx)
	je	.LBB30_16
# BB#15:                                #   in Loop: Header=BB30_13 Depth=2
	movl	%eax, 8(%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, %r13
	jmp	.LBB30_16
	.p2align	4, 0x90
.LBB30_10:                              # %._crit_edge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_13 Depth 2
	testq	%r13, %r13
	jne	.LBB30_11
# BB#17:                                # %sort_EvalSubsortSpecial.exit.preheader
	testq	%r15, %r15
	je	.LBB30_22
# BB#18:                                # %.lr.ph
	movl	32024(%r12), %eax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB30_19:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdx
	cmpl	%eax, 8(%rdx)
	jne	.LBB30_20
# BB#21:                                # %sort_EvalSubsortSpecial.exit
                                        #   in Loop: Header=BB30_19 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB30_19
.LBB30_22:                              # %sort_EvalSubsortSpecial.exit._crit_edge
	movq	%r14, %rdi
	callq	list_Copy
	movq	%rax, %rbp
	xorl	%r14d, %r14d
	testq	%rbp, %rbp
	je	.LBB30_41
# BB#23:                                # %.lr.ph17.i
	testq	%r15, %r15
	je	.LBB30_24
.LBB30_31:                              # %.lr.ph17.split.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_32 Depth 2
                                        #     Child Loop BB30_36 Depth 2
	movq	%rbp, %rcx
	movq	%rbp, %rdx
	movq	8(%rdx), %rax
	movq	(%rcx), %rbp
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rdx, (%rcx)
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB30_32:                              # %.lr.ph.i4.i
                                        #   Parent Loop BB30_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, 8(%rcx)
	je	.LBB30_39
# BB#33:                                #   in Loop: Header=BB30_32 Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB30_32
# BB#34:                                # %.loopexit11.i
                                        #   in Loop: Header=BB30_31 Depth=1
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB30_36
	jmp	.LBB30_30
	.p2align	4, 0x90
.LBB30_38:                              #   in Loop: Header=BB30_36 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB30_30
.LBB30_36:                              # %.lr.ph.i
                                        #   Parent Loop BB30_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rax
	cmpl	$0, 20(%rax)
	jne	.LBB30_38
# BB#37:                                #   in Loop: Header=BB30_36 Depth=2
	movq	8(%rax), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, %rbp
	jmp	.LBB30_38
	.p2align	4, 0x90
.LBB30_30:                              # %.loopexit.i
                                        #   in Loop: Header=BB30_31 Depth=1
	testq	%rbp, %rbp
	jne	.LBB30_31
	jmp	.LBB30_41
	.p2align	4, 0x90
.LBB30_24:                              # %.lr.ph17.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_26 Depth 2
	movq	8(%rbp), %rax
	movq	(%rbp), %r15
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	movq	(%rax), %rbp
	testq	%rbp, %rbp
	jne	.LBB30_26
	jmp	.LBB30_29
	.p2align	4, 0x90
.LBB30_28:                              #   in Loop: Header=BB30_26 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB30_29
.LBB30_26:                              # %.lr.ph.us.i
                                        #   Parent Loop BB30_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rax
	cmpl	$0, 20(%rax)
	jne	.LBB30_28
# BB#27:                                #   in Loop: Header=BB30_26 Depth=2
	movq	8(%rax), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
	jmp	.LBB30_28
	.p2align	4, 0x90
.LBB30_29:                              # %.loopexit.us.i
                                        #   in Loop: Header=BB30_24 Depth=1
	testq	%r15, %r15
	movq	%r15, %rbp
	jne	.LBB30_24
	jmp	.LBB30_41
.LBB30_20:
	xorl	%r14d, %r14d
	jmp	.LBB30_41
.LBB30_39:                              # %list_PointerMember.exit.i
	movl	$1, %r14d
	testq	%rbp, %rbp
	je	.LBB30_41
	.p2align	4, 0x90
.LBB30_40:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB30_40
.LBB30_41:                              # %sort_TestSubsortSpecial.exit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	sort_TheoryIsSubsortOfExtra, .Lfunc_end30-sort_TheoryIsSubsortOfExtra
	.cfi_endproc

	.globl	sort_TheoryComputeAllSubsortHits
	.p2align	4, 0x90
	.type	sort_TheoryComputeAllSubsortHits,@function
sort_TheoryComputeAllSubsortHits:       # @sort_TheoryComputeAllSubsortHits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi213:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi214:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi215:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi216:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi217:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi218:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi219:
	.cfi_def_cfa_offset 144
.Lcfi220:
	.cfi_offset %rbx, -56
.Lcfi221:
	.cfi_offset %r12, -48
.Lcfi222:
	.cfi_offset %r13, -40
.Lcfi223:
	.cfi_offset %r14, -32
.Lcfi224:
	.cfi_offset %r15, -24
.Lcfi225:
	.cfi_offset %rbp, -16
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	32024(%r13), %eax
	cmpl	$-1, %eax
	jne	.LBB31_4
# BB#1:                                 # %.preheader.i.preheader
	movl	$5, %eax
	.p2align	4, 0x90
.LBB31_2:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-32(%r13,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-24(%r13,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-16(%r13,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-8(%r13,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	(%r13,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	addq	$5, %rax
	cmpq	$4005, %rax             # imm = 0xFA5
	jne	.LBB31_2
# BB#3:
	movl	$0, 32024(%r13)
	xorl	%eax, %eax
.LBB31_4:                               # %sort_TheoryIncrementMark.exit
	incl	%eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movl	%eax, 32024(%r13)
	movq	32008(%r13), %rax
	testq	%rax, %rax
	je	.LBB31_6
	.p2align	4, 0x90
.LBB31_85:                              # %.lr.ph206
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rcx
	movl	16(%rcx), %edx
	movl	%edx, 20(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB31_85
.LBB31_6:                               # %.preheader
	testq	%r14, %r14
	je	.LBB31_9
# BB#7:
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB31_8:                               # %.lr.ph202
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %r15
	movq	24(%r15), %rdi
	movl	$sort_ConditionDelete, %esi
	callq	list_DeleteWithElement
	movl	$40, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movl	$1, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movups	%xmm0, 8(%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 24(%r15)
	movl	32024(%r13), %eax
	movl	%eax, 8(%r15)
	movl	32024(%r13), %eax
	movl	%eax, 12(%r15)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB31_8
.LBB31_9:                               # %._crit_edge203
	movq	%r14, %rdi
	callq	list_Copy
	movq	%rax, %r14
	testq	%r14, %r14
	movq	%r13, 32(%rsp)          # 8-byte Spill
	je	.LBB31_13
	.p2align	4, 0x90
.LBB31_10:                              # %.lr.ph71.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_17 Depth 2
                                        #       Child Loop BB31_22 Depth 3
	movq	%r14, %rax
	movq	(%rax), %r14
	movq	8(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	movq	(%rcx), %rbx
	testq	%rbx, %rbx
	jne	.LBB31_17
	jmp	.LBB31_12
	.p2align	4, 0x90
.LBB31_26:                              #   in Loop: Header=BB31_17 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB31_12
.LBB31_17:                              # %.lr.ph67.i
                                        #   Parent Loop BB31_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB31_22 Depth 3
	movq	8(%rbx), %r12
	decl	20(%r12)
	jne	.LBB31_26
# BB#18:                                #   in Loop: Header=BB31_17 Depth=2
	movq	%r13, %rcx
	movq	8(%r12), %r13
	movl	8(%r13), %eax
	cmpl	32024(%rcx), %eax
	jne	.LBB31_20
# BB#19:                                #   in Loop: Header=BB31_17 Depth=2
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB31_17
	jmp	.LBB31_12
.LBB31_20:                              #   in Loop: Header=BB31_17 Depth=2
	movl	$40, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movl	$1, (%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbp)
	movups	%xmm0, 8(%rbp)
	movq	(%r12), %r15
	testq	%r15, %r15
	jne	.LBB31_22
	jmp	.LBB31_25
	.p2align	4, 0x90
.LBB31_24:                              #   in Loop: Header=BB31_22 Depth=3
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB31_25
.LBB31_22:                              # %.lr.ph.i
                                        #   Parent Loop BB31_10 Depth=1
                                        #     Parent Loop BB31_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%r15), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.LBB31_24
# BB#23:                                #   in Loop: Header=BB31_22 Depth=3
	movq	8(%rax), %rsi
	movq	%rbp, %rdi
	callq	sort_ExtendConditionByCondition
	jmp	.LBB31_24
.LBB31_25:                              # %._crit_edge.i
                                        #   in Loop: Header=BB31_17 Depth=2
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	sort_ExtendConditionByLink
	movq	24(%r13), %rdi
	movl	$sort_ConditionDelete, %esi
	callq	list_DeleteWithElement
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 24(%r13)
	movq	32(%rsp), %rbp          # 8-byte Reload
	movl	32024(%rbp), %eax
	movl	%eax, 8(%r13)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%rbp, %r13
	movq	%r14, (%rax)
	movq	%rax, %r14
	jmp	.LBB31_26
	.p2align	4, 0x90
.LBB31_12:                              # %.loopexit.i
                                        #   in Loop: Header=BB31_10 Depth=1
	testq	%r14, %r14
	jne	.LBB31_10
.LBB31_13:                              # %sort_EvalSubsort.exit.preheader
	movq	(%rsp), %rdi            # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB31_28
# BB#14:                                # %.lr.ph200
	movl	32024(%r13), %eax
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB31_15:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdx
	cmpl	%eax, 8(%rdx)
	jne	.LBB31_16
# BB#27:                                # %sort_EvalSubsort.exit
                                        #   in Loop: Header=BB31_15 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB31_15
.LBB31_28:                              # %sort_EvalSubsort.exit._crit_edge
	callq	list_Copy
	movq	%rax, %r14
	movl	$40, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movl	$1, (%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbp)
	movups	%xmm0, 8(%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r14, 8(%rbx)
	movq	%rbp, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	$0, (%rbp)
	movq	stdout(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	testq	%rbp, %rbp
	je	.LBB31_29
# BB#30:                                # %.lr.ph.preheader
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB31_31:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_46 Depth 2
                                        #       Child Loop BB31_49 Depth 3
                                        #       Child Loop BB31_86 Depth 3
                                        #       Child Loop BB31_55 Depth 3
                                        #     Child Loop BB31_34 Depth 2
                                        #     Child Loop BB31_37 Depth 2
                                        #     Child Loop BB31_45 Depth 2
                                        #     Child Loop BB31_60 Depth 2
                                        #     Child Loop BB31_69 Depth 2
                                        #       Child Loop BB31_71 Depth 3
                                        #         Child Loop BB31_77 Depth 4
	movq	(%rbp), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	8(%rbp), %rbx
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	%rbx, %rdi
	callq	sort_PairPrint
	testq	%r15, %r15
	je	.LBB31_58
# BB#32:                                # %.lr.ph.i137
                                        #   in Loop: Header=BB31_31 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rax), %r14
	testq	%r14, %r14
	movq	%r15, %rbp
	je	.LBB31_33
.LBB31_46:                              # %.lr.ph.split.i
                                        #   Parent Loop BB31_31 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB31_49 Depth 3
                                        #       Child Loop BB31_86 Depth 3
                                        #       Child Loop BB31_55 Depth 3
	movq	8(%rbp), %r12
	movq	%r12, %rdi
	callq	list_Length
	movl	%eax, %ebx
	movq	%r14, %rdi
	callq	list_Length
	cmpl	%eax, %ebx
	jne	.LBB31_57
# BB#47:                                #   in Loop: Header=BB31_46 Depth=2
	movl	32024(%r13), %eax
	cmpl	$-1, %eax
	jne	.LBB31_51
# BB#48:                                # %.preheader.i.i.i.preheader
                                        #   in Loop: Header=BB31_46 Depth=2
	movl	$5, %eax
	.p2align	4, 0x90
.LBB31_49:                              # %.preheader.i.i.i
                                        #   Parent Loop BB31_31 Depth=1
                                        #     Parent Loop BB31_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-32(%r13,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-24(%r13,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-16(%r13,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-8(%r13,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	(%r13,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	addq	$5, %rax
	cmpq	$4005, %rax             # imm = 0xFA5
	jne	.LBB31_49
# BB#50:                                #   in Loop: Header=BB31_46 Depth=2
	movl	$0, 32024(%r13)
	xorl	%eax, %eax
.LBB31_51:                              # %sort_TheoryIncrementMark.exit.i.i
                                        #   in Loop: Header=BB31_46 Depth=2
	incl	%eax
	movl	%eax, 32024(%r13)
	testq	%r12, %r12
	je	.LBB31_54
# BB#52:                                # %.lr.ph25.i.preheader.i
                                        #   in Loop: Header=BB31_46 Depth=2
	movq	8(%r12), %rcx
	movl	%eax, 16(%rcx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.LBB31_54
	.p2align	4, 0x90
.LBB31_86:                              # %.lr.ph25..lr.ph25_crit_edge.i.i
                                        #   Parent Loop BB31_31 Depth=1
                                        #     Parent Loop BB31_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	32024(%r13), %ecx
	movq	8(%rax), %rdx
	movl	%ecx, 16(%rdx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB31_86
.LBB31_54:                              # %.lr.ph.i.i
                                        #   in Loop: Header=BB31_46 Depth=2
	movl	32024(%r13), %eax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB31_55:                              #   Parent Loop BB31_31 Depth=1
                                        #     Parent Loop BB31_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rcx), %rdx
	cmpl	%eax, 16(%rdx)
	jne	.LBB31_57
# BB#56:                                #   in Loop: Header=BB31_55 Depth=3
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB31_55
	jmp	.LBB31_40
	.p2align	4, 0x90
.LBB31_57:                              # %.loopexit.i138
                                        #   in Loop: Header=BB31_46 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB31_46
	jmp	.LBB31_58
	.p2align	4, 0x90
.LBB31_33:                              # %.lr.ph.split.us.i.preheader
                                        #   in Loop: Header=BB31_31 Depth=1
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB31_34:                              # %.lr.ph.split.us.i
                                        #   Parent Loop BB31_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %r14
	movq	%r14, %rdi
	callq	list_Length
	movl	%eax, %ebx
	xorl	%edi, %edi
	callq	list_Length
	cmpl	%eax, %ebx
	je	.LBB31_35
# BB#42:                                #   in Loop: Header=BB31_34 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB31_34
.LBB31_58:                              # %sort_TheorySortMember.exit
                                        #   in Loop: Header=BB31_31 Depth=1
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	8(%rbp), %rdi
	callq	list_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movq	8(%rbp), %rdi
	testq	%rdi, %rdi
	movq	%rax, 56(%rsp)          # 8-byte Spill
	je	.LBB31_67
# BB#59:                                # %.lr.ph178.preheader
                                        #   in Loop: Header=BB31_31 Depth=1
	movl	$1, %ecx
	movq	%rdi, %r10
	movq	%rdi, %rbx
	movl	$1, %edi
	movl	52(%rsp), %r9d          # 4-byte Reload
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB31_60:                              # %.lr.ph178
                                        #   Parent Loop BB31_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rbp
	xorl	%edx, %edx
	cmpl	%r9d, 8(%rbp)
	movl	$0, %esi
	jne	.LBB31_62
# BB#61:                                #   in Loop: Header=BB31_60 Depth=2
	cmpl	%r9d, 12(%rbp)
	cmovnel	%r8d, %ecx
	movl	%edi, %edx
	movl	%ecx, %esi
.LBB31_62:                              #   in Loop: Header=BB31_60 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB31_64
# BB#63:                                #   in Loop: Header=BB31_60 Depth=2
	movl	%esi, %ecx
	orl	%edx, %ecx
	movl	%esi, %ecx
	movl	%edx, %edi
	jne	.LBB31_60
.LBB31_64:                              # %.critedge
                                        #   in Loop: Header=BB31_31 Depth=1
	testl	%edx, %edx
	je	.LBB31_65
# BB#66:                                #   in Loop: Header=BB31_31 Depth=1
	testl	%esi, %esi
	movq	%r10, %rdi
	je	.LBB31_68
.LBB31_67:                              # %.thread
                                        #   in Loop: Header=BB31_31 Depth=1
	callq	list_Copy
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	(%rsp), %r12            # 8-byte Reload
	movq	(%r12), %rbp
	movl	(%rbp), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	8(%rbp), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r15
	movq	16(%rbp), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r13
	movq	24(%rbp), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %rbx
	movq	32(%rbp), %rdi
	callq	list_Copy
	movq	%rax, %rbp
	movl	$40, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%r14)
	movq	%r15, 8(%r14)
	movq	%r13, 16(%r14)
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	%rbx, 24(%r14)
	movq	%rbp, 32(%r14)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rbx)
	movq	%r14, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	8(%r12), %rdi
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB31_68:                              #   in Loop: Header=BB31_31 Depth=1
	testq	%rdi, %rdi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rdi, %rax
	je	.LBB31_82
	.p2align	4, 0x90
.LBB31_69:                              #   Parent Loop BB31_31 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB31_71 Depth 3
                                        #         Child Loop BB31_77 Depth 4
	movq	32008(%r13), %r14
	testq	%r14, %r14
	movq	%rax, 64(%rsp)          # 8-byte Spill
	je	.LBB31_81
# BB#70:                                # %.lr.ph184.preheader
                                        #   in Loop: Header=BB31_69 Depth=2
	movq	8(%rax), %rbx
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB31_71:                              # %.lr.ph184
                                        #   Parent Loop BB31_31 Depth=1
                                        #     Parent Loop BB31_69 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB31_77 Depth 4
	movq	8(%r14), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r13
	cmpq	%rbx, 8(%r13)
	jne	.LBB31_80
# BB#72:                                #   in Loop: Header=BB31_71 Depth=3
	cmpq	$0, (%r13)
	je	.LBB31_80
# BB#73:                                #   in Loop: Header=BB31_71 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rax), %rdi
	callq	list_Copy
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, %rbx
	movq	(%r13), %rdi
	callq	list_Copy
	testq	%rbx, %rbx
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	je	.LBB31_74
# BB#75:                                #   in Loop: Header=BB31_71 Depth=3
	testq	%rax, %rax
	je	.LBB31_79
# BB#76:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB31_71 Depth=3
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB31_77:                              # %.preheader.i.i
                                        #   Parent Loop BB31_31 Depth=1
                                        #     Parent Loop BB31_69 Depth=2
                                        #       Parent Loop BB31_71 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB31_77
# BB#78:                                #   in Loop: Header=BB31_71 Depth=3
	movq	%rax, (%rcx)
	jmp	.LBB31_79
.LBB31_74:                              #   in Loop: Header=BB31_71 Depth=3
	movq	%rax, %rbx
.LBB31_79:                              # %sort_Intersect.exit
                                        #   in Loop: Header=BB31_71 Depth=3
	movq	%rbx, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rbx
	movl	(%rbx), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	8(%rbx), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	16(%rbx), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %rbp
	movq	24(%rbx), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r12
	movq	32(%rbx), %rdi
	callq	list_Copy
	movq	%rax, %r15
	movl	$40, %edi
	callq	memory_Malloc
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, (%rax)
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%rbp, 16(%rax)
	movq	%r12, 24(%rax)
	movq	%r15, 32(%rax)
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	sort_ExtendConditionByLink
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rbp)
	movq	%rbx, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, %rbp
	movq	72(%rsp), %rbx          # 8-byte Reload
.LBB31_80:                              #   in Loop: Header=BB31_71 Depth=3
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB31_71
.LBB31_81:                              # %._crit_edge
                                        #   in Loop: Header=BB31_69 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	movq	32(%rsp), %r13          # 8-byte Reload
	jne	.LBB31_69
	jmp	.LBB31_82
.LBB31_35:                              #   in Loop: Header=BB31_31 Depth=1
	movl	32024(%r13), %eax
	cmpl	$-1, %eax
	jne	.LBB31_39
# BB#36:                                # %.preheader.i.i.us.i.preheader
                                        #   in Loop: Header=BB31_31 Depth=1
	movl	$5, %eax
	.p2align	4, 0x90
.LBB31_37:                              # %.preheader.i.i.us.i
                                        #   Parent Loop BB31_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-32(%r13,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-24(%r13,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-16(%r13,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	-8(%r13,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	movq	(%r13,%rax,8), %rcx
	movl	$0, 8(%rcx)
	movl	$0, 16(%rcx)
	movl	$0, 12(%rcx)
	addq	$5, %rax
	cmpq	$4005, %rax             # imm = 0xFA5
	jne	.LBB31_37
# BB#38:                                #   in Loop: Header=BB31_31 Depth=1
	movl	$0, 32024(%r13)
	xorl	%eax, %eax
.LBB31_39:                              # %sort_TheoryIncrementMark.exit.i.us.i
                                        #   in Loop: Header=BB31_31 Depth=1
	incl	%eax
	movl	%eax, 32024(%r13)
	testq	%r14, %r14
	je	.LBB31_40
# BB#43:                                # %.lr.ph25.i.preheader.us.i
                                        #   in Loop: Header=BB31_31 Depth=1
	movq	8(%r14), %rcx
	movl	%eax, 16(%rcx)
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.LBB31_40
	.p2align	4, 0x90
.LBB31_45:                              # %.lr.ph25..lr.ph25_crit_edge.i.us.i
                                        #   Parent Loop BB31_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	32024(%r13), %ecx
	movq	8(%rax), %rdx
	movl	%ecx, 16(%rdx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB31_45
	.p2align	4, 0x90
.LBB31_40:                              # %sort_TheorySortMember.exit.thread.backedge
                                        #   in Loop: Header=BB31_31 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	jne	.LBB31_31
	jmp	.LBB31_41
.LBB31_65:                              #   in Loop: Header=BB31_31 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB31_82:                              # %sort_TheorySortMember.exit.thread.outer
                                        #   in Loop: Header=BB31_31 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	sort_PairDelete
	testq	%rbp, %rbp
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, %r15
	jne	.LBB31_31
	jmp	.LBB31_83
.LBB31_16:
	xorl	%eax, %eax
	jmp	.LBB31_84
.LBB31_41:
	movq	%r15, %rdi
	jmp	.LBB31_83
.LBB31_29:
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%edi, %edi
.LBB31_83:                              # %sort_TheorySortMember.exit.thread.outer._crit_edge
	movl	$sort_Delete, %esi
	callq	list_DeleteWithElement
	movq	24(%rsp), %rax          # 8-byte Reload
.LBB31_84:                              # %.loopexit157
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	sort_TheoryComputeAllSubsortHits, .Lfunc_end31-sort_TheoryComputeAllSubsortHits
	.cfi_endproc

	.p2align	4, 0x90
	.type	sort_PairDelete,@function
sort_PairDelete:                        # @sort_PairDelete
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi226:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi227:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi228:
	.cfi_def_cfa_offset 32
.Lcfi229:
	.cfi_offset %rbx, -24
.Lcfi230:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.LBB32_2
	.p2align	4, 0x90
.LBB32_1:                               # %.lr.ph.i.i7
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB32_1
.LBB32_2:                               # %sort_DeleteOne.exit
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB32_6
# BB#3:
	movq	8(%rbx), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	16(%rbx), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	24(%rbx), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB32_5
	.p2align	4, 0x90
.LBB32_4:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB32_4
.LBB32_5:                               # %list_Delete.exit.i
	movq	memory_ARRAY+320(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+320(%rip), %rax
	movq	%rbx, (%rax)
.LBB32_6:                               # %sort_ConditionDelete.exit
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end32:
	.size	sort_PairDelete, .Lfunc_end32-sort_PairDelete
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI33_0:
	.zero	16
	.text
	.globl	sort_ComputeSortNoResidues
	.p2align	4, 0x90
	.type	sort_ComputeSortNoResidues,@function
sort_ComputeSortNoResidues:             # @sort_ComputeSortNoResidues
	.cfi_startproc
# BB#0:                                 # %.lr.ph
	pushq	%rbp
.Lcfi231:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi232:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi233:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi234:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi235:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi236:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi237:
	.cfi_def_cfa_offset 160
.Lcfi238:
	.cfi_offset %rbx, -56
.Lcfi239:
	.cfi_offset %r12, -48
.Lcfi240:
	.cfi_offset %r13, -40
.Lcfi241:
	.cfi_offset %r14, -32
.Lcfi242:
	.cfi_offset %r15, -24
.Lcfi243:
	.cfi_offset %rbp, -16
	movq	%r9, 96(%rsp)           # 8-byte Spill
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movl	%ecx, %r12d
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movl	stack_POINTER(%rip), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movq	%rsi, %rdi
	callq	sharing_PushOnStack
	movl	stack_POINTER(%rip), %eax
	movl	%r12d, 60(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB33_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_7 Depth 2
                                        #       Child Loop BB33_12 Depth 3
                                        #       Child Loop BB33_17 Depth 3
                                        #         Child Loop BB33_27 Depth 4
                                        #         Child Loop BB33_32 Depth 4
                                        #         Child Loop BB33_36 Depth 4
                                        #         Child Loop BB33_38 Depth 4
                                        #       Child Loop BB33_45 Depth 3
                                        #       Child Loop BB33_19 Depth 3
                                        #       Child Loop BB33_21 Depth 3
                                        #       Child Loop BB33_51 Depth 3
                                        #       Child Loop BB33_58 Depth 3
                                        #     Child Loop BB33_63 Depth 2
	decl	%eax
	movl	%eax, stack_POINTER(%rip)
	movq	stack_STACK(,%rax,8), %r15
	movq	%r15, %rdi
	callq	hash_Get
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB33_66
# BB#2:                                 #   in Loop: Header=BB33_1 Depth=1
	movl	(%r15), %esi
	testl	%esi, %esi
	jle	.LBB33_4
# BB#3:                                 #   in Loop: Header=BB33_1 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movl	%r12d, %ecx
	callq	sort_VarSort
	movq	%rax, %r14
	xorl	%ebx, %ebx
	jmp	.LBB33_65
.LBB33_4:                               #   in Loop: Header=BB33_1 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movq	%r15, %rdx
	callq	st_GetGen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB33_5
# BB#6:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB33_1 Depth=1
	xorl	%r14d, %r14d
	movq	%rbp, %r12
	xorl	%eax, %eax
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	jmp	.LBB33_7
	.p2align	4, 0x90
.LBB33_8:                               #   in Loop: Header=BB33_7 Depth=2
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	8(%rbx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	8(%rax), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %r13
	movq	%r13, %rdi
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	96(%rsp), %rdx          # 8-byte Reload
	callq	clause_IsSortTheoryClause
	testl	%eax, %eax
	je	.LBB33_9
# BB#10:                                #   in Loop: Header=BB33_7 Depth=2
	movl	68(%r13), %eax
	addl	64(%r13), %eax
	movq	%r13, 16(%rsp)          # 8-byte Spill
	addl	72(%r13), %eax
	movl	%eax, 68(%rsp)          # 4-byte Spill
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	unify_Match
	callq	subst_ExtractMatcher
	movl	cont_BINDINGS(%rip), %esi
	testl	%esi, %esi
	xorps	%xmm0, %xmm0
	jle	.LBB33_13
# BB#11:                                # %.lr.ph.i73.i.i.preheader
                                        #   in Loop: Header=BB33_7 Depth=2
	incl	%esi
	.p2align	4, 0x90
.LBB33_12:                              # %.lr.ph.i73.i.i
                                        #   Parent Loop BB33_1 Depth=1
                                        #     Parent Loop BB33_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rsi), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%esi
	cmpl	$1, %esi
	jg	.LBB33_12
.LBB33_13:                              # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB33_7 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB33_15
# BB#14:                                #   in Loop: Header=BB33_7 Depth=2
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
.LBB33_15:                              # %cont_BackTrack.exit.preheader.i.i
                                        #   in Loop: Header=BB33_7 Depth=2
	testq	%rax, %rax
	je	.LBB33_47
# BB#16:                                # %.lr.ph.i58.i.preheader
                                        #   in Loop: Header=BB33_7 Depth=2
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rax, %r15
	xorl	%r14d, %r14d
	movq	16(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB33_17:                              # %.lr.ph.i58.i
                                        #   Parent Loop BB33_1 Depth=1
                                        #     Parent Loop BB33_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB33_27 Depth 4
                                        #         Child Loop BB33_32 Depth 4
                                        #         Child Loop BB33_36 Depth 4
                                        #         Child Loop BB33_38 Depth 4
	movl	8(%r15), %esi
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdi
	movl	68(%rsp), %ecx          # 4-byte Reload
	callq	sort_VarSort
	movq	%rax, %rbx
	movq	16(%r15), %rdi
	callq	hash_Get
	movq	%rax, %rbp
	movq	8(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	sort_TheoryIsSubsortOfNoResidues
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB33_18
# BB#23:                                #   in Loop: Header=BB33_17 Depth=3
	movq	(%rbp), %rax
	movq	32(%rax), %rdi
	callq	list_Copy
	testq	%r14, %r14
	je	.LBB33_24
# BB#25:                                #   in Loop: Header=BB33_17 Depth=3
	testq	%rax, %rax
	movq	%r14, %rcx
	je	.LBB33_26
	.p2align	4, 0x90
.LBB33_27:                              # %.preheader.i64.i.i
                                        #   Parent Loop BB33_1 Depth=1
                                        #     Parent Loop BB33_7 Depth=2
                                        #       Parent Loop BB33_17 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB33_27
# BB#28:                                #   in Loop: Header=BB33_17 Depth=3
	movq	%rax, (%rdx)
	jmp	.LBB33_29
	.p2align	4, 0x90
.LBB33_24:                              #   in Loop: Header=BB33_17 Depth=3
	movq	%rax, %r14
.LBB33_29:                              # %list_Nconc.exit66.i.i
                                        #   in Loop: Header=BB33_17 Depth=3
	movq	%r13, %rbp
	addq	$32, %rbp
	movq	32(%r13), %rax
	testq	%r14, %r14
	movq	%r14, %rcx
	movq	%rax, %r14
	jne	.LBB33_30
	jmp	.LBB33_35
	.p2align	4, 0x90
.LBB33_26:                              # %list_Nconc.exit66.thread.i.i
                                        #   in Loop: Header=BB33_17 Depth=3
	movq	%r13, %rbp
	addq	$32, %rbp
	movq	32(%r13), %rax
	movq	%r14, %rcx
.LBB33_30:                              #   in Loop: Header=BB33_17 Depth=3
	testq	%rax, %rax
	je	.LBB33_34
# BB#31:                                # %.preheader.i58.i.i.preheader
                                        #   in Loop: Header=BB33_17 Depth=3
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB33_32:                              # %.preheader.i58.i.i
                                        #   Parent Loop BB33_1 Depth=1
                                        #     Parent Loop BB33_7 Depth=2
                                        #       Parent Loop BB33_17 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB33_32
# BB#33:                                #   in Loop: Header=BB33_17 Depth=3
	movq	%rax, (%rdx)
.LBB33_34:                              #   in Loop: Header=BB33_17 Depth=3
	movq	%rcx, %r14
.LBB33_35:                              #   in Loop: Header=BB33_17 Depth=3
	movq	$0, (%rbp)
	movq	8(%r13), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	16(%r13), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	24(%r13), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	(%rbp), %rax
	testq	%rax, %rax
	je	.LBB33_37
	.p2align	4, 0x90
.LBB33_36:                              # %.lr.ph.i.i54.i.i
                                        #   Parent Loop BB33_1 Depth=1
                                        #     Parent Loop BB33_7 Depth=2
                                        #       Parent Loop BB33_17 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB33_36
.LBB33_37:                              # %sort_ConditionDelete.exit.i.i
                                        #   in Loop: Header=BB33_17 Depth=3
	movq	memory_ARRAY+320(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	memory_ARRAY+320(%rip), %rax
	movq	%r13, (%rax)
	testq	%rbx, %rbx
	movq	16(%rsp), %rdx          # 8-byte Reload
	je	.LBB33_39
	.p2align	4, 0x90
.LBB33_38:                              # %.lr.ph.i.i.i.i
                                        #   Parent Loop BB33_1 Depth=1
                                        #     Parent Loop BB33_7 Depth=2
                                        #       Parent Loop BB33_17 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB33_38
.LBB33_39:                              # %sort_DeleteOne.exit.i.i
                                        #   in Loop: Header=BB33_17 Depth=3
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB33_17
# BB#40:                                # %cont_BackTrack.exit._crit_edge.i.i
                                        #   in Loop: Header=BB33_7 Depth=2
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	subst_Free
	testq	%r14, %r14
	je	.LBB33_41
# BB#42:                                #   in Loop: Header=BB33_7 Depth=2
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	je	.LBB33_43
# BB#44:                                # %.preheader.i.i62.i.preheader
                                        #   in Loop: Header=BB33_7 Depth=2
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB33_45:                              # %.preheader.i.i62.i
                                        #   Parent Loop BB33_1 Depth=1
                                        #     Parent Loop BB33_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB33_45
# BB#46:                                #   in Loop: Header=BB33_7 Depth=2
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%rax)
.LBB33_43:                              #   in Loop: Header=BB33_7 Depth=2
	movq	%r14, (%rsp)            # 8-byte Spill
	movq	24(%rsp), %r14          # 8-byte Reload
	testq	%r14, %r14
	jne	.LBB33_50
	jmp	.LBB33_54
.LBB33_9:                               #   in Loop: Header=BB33_7 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	jmp	.LBB33_61
.LBB33_47:                              # %cont_BackTrack.exit._crit_edge.thread.i.i
                                        #   in Loop: Header=BB33_7 Depth=2
	xorl	%edi, %edi
	callq	subst_Free
	jmp	.LBB33_48
.LBB33_18:                              #   in Loop: Header=BB33_7 Depth=2
	testq	%rbx, %rbx
	je	.LBB33_20
	.p2align	4, 0x90
.LBB33_19:                              # %.lr.ph.i.i70.i.i
                                        #   Parent Loop BB33_1 Depth=1
                                        #     Parent Loop BB33_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB33_19
.LBB33_20:                              # %sort_DeleteOne.exit72.i.i
                                        #   in Loop: Header=BB33_7 Depth=2
	testq	%r14, %r14
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	72(%rsp), %rdi          # 8-byte Reload
	je	.LBB33_22
	.p2align	4, 0x90
.LBB33_21:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB33_1 Depth=1
                                        #     Parent Loop BB33_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB33_21
.LBB33_22:                              # %sort_MatchNoResidues.exit.i
                                        #   in Loop: Header=BB33_7 Depth=2
	callq	subst_Free
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	(%rsp), %rax            # 8-byte Reload
	jmp	.LBB33_61
.LBB33_41:                              #   in Loop: Header=BB33_7 Depth=2
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
.LBB33_48:                              #   in Loop: Header=BB33_7 Depth=2
	movl	12(%rsp), %edx          # 4-byte Reload
	testq	%r14, %r14
	je	.LBB33_54
.LBB33_50:                              # %.lr.ph.i56.i.preheader
                                        #   in Loop: Header=BB33_7 Depth=2
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB33_51:                              # %.lr.ph.i56.i
                                        #   Parent Loop BB33_1 Depth=1
                                        #     Parent Loop BB33_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rax), %rcx
	cmpl	%edx, 32(%rcx)
	je	.LBB33_52
# BB#53:                                #   in Loop: Header=BB33_51 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB33_51
.LBB33_54:                              # %.loopexit.i
                                        #   in Loop: Header=BB33_7 Depth=2
	negl	%edx
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	movslq	%edx, %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	8(%rcx,%rax,8), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	testq	%r14, %r14
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	je	.LBB33_55
# BB#56:                                #   in Loop: Header=BB33_7 Depth=2
	testq	%rax, %rax
	je	.LBB33_60
# BB#57:                                # %.preheader.i.i.i.preheader
                                        #   in Loop: Header=BB33_7 Depth=2
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB33_58:                              # %.preheader.i.i.i
                                        #   Parent Loop BB33_1 Depth=1
                                        #     Parent Loop BB33_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB33_58
# BB#59:                                #   in Loop: Header=BB33_7 Depth=2
	movq	%rax, (%rcx)
	jmp	.LBB33_60
.LBB33_52:                              #   in Loop: Header=BB33_7 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	jmp	.LBB33_61
.LBB33_55:                              #   in Loop: Header=BB33_7 Depth=2
	movq	%rax, %r14
.LBB33_60:                              # %sort_Intersect.exit.i
                                        #   in Loop: Header=BB33_7 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%rax)
	jmp	.LBB33_61
	.p2align	4, 0x90
.LBB33_7:                               # %.lr.ph.i
                                        #   Parent Loop BB33_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB33_12 Depth 3
                                        #       Child Loop BB33_17 Depth 3
                                        #         Child Loop BB33_27 Depth 4
                                        #         Child Loop BB33_32 Depth 4
                                        #         Child Loop BB33_36 Depth 4
                                        #         Child Loop BB33_38 Depth 4
                                        #       Child Loop BB33_45 Depth 3
                                        #       Child Loop BB33_19 Depth 3
                                        #       Child Loop BB33_21 Depth 3
                                        #       Child Loop BB33_51 Depth 3
                                        #       Child Loop BB33_58 Depth 3
	movq	8(%r12), %rbx
	cmpl	$0, (%rbx)
	jle	.LBB33_8
.LBB33_61:                              # %sort_ContainsSymbol.exit.i
                                        #   in Loop: Header=BB33_7 Depth=2
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB33_7
# BB#62:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB33_1 Depth=1
	movq	%rax, %rsi
	movl	60(%rsp), %r12d         # 4-byte Reload
	.p2align	4, 0x90
.LBB33_63:                              # %.lr.ph.i.i
                                        #   Parent Loop BB33_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB33_63
# BB#64:                                #   in Loop: Header=BB33_1 Depth=1
	movq	%rsi, %rbx
	jmp	.LBB33_65
.LBB33_5:                               #   in Loop: Header=BB33_1 Depth=1
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB33_65:                              # %sort_ComputeSortInternNoResidues.exit
                                        #   in Loop: Header=BB33_1 Depth=1
	movl	$40, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movl	$1, (%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbp)
	movq	$0, 24(%rbp)
	movq	%rbx, 32(%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r14, 8(%rbx)
	movq	%rbp, (%rbx)
	movl	%r15d, %eax
	movl	$2369637129, %ecx       # imm = 0x8D3DCB09
	imulq	%rcx, %rax
	shrq	$36, %rax
	imull	$29, %eax, %eax
	movl	%r15d, %ebp
	subl	%eax, %ebp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%r15, 8(%r14)
	movq	%rbx, (%r14)
	movq	hash_TABLE(,%rbp,8), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, hash_TABLE(,%rbp,8)
.LBB33_66:                              # %.backedge
                                        #   in Loop: Header=BB33_1 Depth=1
	movl	stack_POINTER(%rip), %eax
	cmpl	64(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB33_1
# BB#67:                                # %._crit_edge
	movq	8(%rbx), %rdi
	callq	list_Copy
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	(%rbx), %rbx
	movl	(%rbx), %r14d
	movq	8(%rbx), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r15
	movq	16(%rbx), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r12
	movq	24(%rbx), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %r13
	movq	32(%rbx), %rdi
	callq	list_Copy
	movq	%rax, %rbp
	movl	$40, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movl	%r14d, (%rbx)
	movq	%r15, 8(%rbx)
	movq	%r12, 16(%rbx)
	movq	%r13, 24(%rbx)
	movq	%rbp, 32(%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rbp)
	movq	%rbx, (%rbp)
	movl	$sort_DeleteSortPair, %edi
	callq	hash_ResetWithValue
	movq	%rbp, %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end33:
	.size	sort_ComputeSortNoResidues, .Lfunc_end33-sort_ComputeSortNoResidues
	.cfi_endproc

	.globl	sort_ApproxMaxDeclClauses
	.p2align	4, 0x90
	.type	sort_ApproxMaxDeclClauses,@function
sort_ApproxMaxDeclClauses:              # @sort_ApproxMaxDeclClauses
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi244:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi245:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi246:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi247:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi248:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi249:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi250:
	.cfi_def_cfa_offset 112
.Lcfi251:
	.cfi_offset %rbx, -56
.Lcfi252:
	.cfi_offset %r12, -48
.Lcfi253:
	.cfi_offset %r13, -40
.Lcfi254:
	.cfi_offset %r14, -32
.Lcfi255:
	.cfi_offset %r15, -24
.Lcfi256:
	.cfi_offset %rbp, -16
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movl	72(%rbx), %eax
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB34_22
# BB#1:                                 # %.lr.ph89
	movslq	68(%rbx), %rcx
	movslq	64(%rbx), %r12
	addq	%rcx, %r12
	addl	%r12d, %eax
	movl	symbol_TYPESTATBITS(%rip), %r14d
	movslq	%eax, %rdx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB34_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB34_7 Depth 2
                                        #     Child Loop BB34_17 Depth 2
                                        #     Child Loop BB34_19 Depth 2
	movq	56(%rbx), %rax
	movq	(%rax,%r12,8), %r13
	testb	$1, (%r13)
	je	.LBB34_21
# BB#3:                                 #   in Loop: Header=BB34_2 Depth=1
	movq	24(%r13), %rdi
	xorl	%eax, %eax
	subl	(%rdi), %eax
	movl	%r14d, %ecx
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	cmpl	$1, 16(%rax)
	jne	.LBB34_21
# BB#4:                                 #   in Loop: Header=BB34_2 Depth=1
	movq	%r12, 24(%rsp)          # 8-byte Spill
	callq	term_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	$0, (%rbp)
	movl	64(%rbx), %r12d
	testl	%r12d, %r12d
	jle	.LBB34_5
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB34_2 Depth=1
	movq	%r15, 16(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB34_7:                               # %.lr.ph
                                        #   Parent Loop BB34_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%rbx), %rax
	movq	(%rax,%r15,8), %rsi
	movq	%r13, %rdi
	callq	clause_LitsHaveCommonVar
	testl	%eax, %eax
	je	.LBB34_11
# BB#8:                                 #   in Loop: Header=BB34_7 Depth=2
	movq	56(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB34_10
# BB#9:                                 #   in Loop: Header=BB34_7 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB34_10:                              # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB34_7 Depth=2
	callq	term_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB34_11:                              #   in Loop: Header=BB34_7 Depth=2
	incq	%r15
	cmpq	%r15, %r12
	jne	.LBB34_7
# BB#12:                                # %._crit_edge
                                        #   in Loop: Header=BB34_2 Depth=1
	testq	%r14, %r14
	movq	24(%rsp), %r12          # 8-byte Reload
	je	.LBB34_13
# BB#14:                                #   in Loop: Header=BB34_2 Depth=1
	movq	8(%rbp), %rsi
	movl	52(%rbx), %edx
	movq	%r14, %rdi
	callq	sort_ApproxPseudoLinear
	movq	%rbx, %r14
	movq	%rax, %rbx
	jmp	.LBB34_15
.LBB34_5:                               #   in Loop: Header=BB34_2 Depth=1
	movq	%rbx, %r14
	xorl	%ebx, %ebx
	movq	24(%rsp), %r12          # 8-byte Reload
	jmp	.LBB34_16
.LBB34_13:                              #   in Loop: Header=BB34_2 Depth=1
	movq	%rbx, %r14
	xorl	%ebx, %ebx
.LBB34_15:                              # %._crit_edge.thread
                                        #   in Loop: Header=BB34_2 Depth=1
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB34_16:                              # %._crit_edge.thread
                                        #   in Loop: Header=BB34_2 Depth=1
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	callq	clause_Create
	movq	%rax, %r13
	movl	$0, 12(%r13)
	orb	$1, 48(%r13)
	movl	$0, 76(%r13)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 32(%r13)
	movq	40(%r13), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 40(%r13)
	testq	%rbx, %rbx
	je	.LBB34_18
	.p2align	4, 0x90
.LBB34_17:                              # %.lr.ph.i81
                                        #   Parent Loop BB34_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB34_17
.LBB34_18:                              # %list_Delete.exit82
                                        #   in Loop: Header=BB34_2 Depth=1
	testq	%rbp, %rbp
	movl	symbol_TYPESTATBITS(%rip), %r14d
	je	.LBB34_20
	.p2align	4, 0x90
.LBB34_19:                              # %.lr.ph.i
                                        #   Parent Loop BB34_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB34_19
.LBB34_20:                              # %list_Delete.exit
                                        #   in Loop: Header=BB34_2 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB34_21:                              #   in Loop: Header=BB34_2 Depth=1
	incq	%r12
	cmpq	%rdx, %r12
	jl	.LBB34_2
.LBB34_22:                              # %._crit_edge90
	movq	%r15, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end34:
	.size	sort_ApproxMaxDeclClauses, .Lfunc_end34-sort_ApproxMaxDeclClauses
	.cfi_endproc

	.p2align	4, 0x90
	.type	sort_ApproxPseudoLinear,@function
sort_ApproxPseudoLinear:                # @sort_ApproxPseudoLinear
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi257:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi258:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi259:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi260:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi261:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi262:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi263:
	.cfi_def_cfa_offset 80
.Lcfi264:
	.cfi_offset %rbx, -56
.Lcfi265:
	.cfi_offset %r12, -48
.Lcfi266:
	.cfi_offset %r13, -40
.Lcfi267:
	.cfi_offset %r14, -32
.Lcfi268:
	.cfi_offset %r15, -24
.Lcfi269:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movl	%edx, %esi
	callq	term_RenamePseudoLinear
	testq	%rax, %rax
	je	.LBB35_1
# BB#2:                                 # %.lr.ph52
	testq	%r13, %r13
	movq	%rax, 8(%rsp)           # 8-byte Spill
	je	.LBB35_9
# BB#3:                                 # %.lr.ph52.split.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB35_4:                               # %.lr.ph52.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_5 Depth 2
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rax), %rax
	movl	(%rax), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movl	8(%rax), %r14d
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	%r13, %r15
	.p2align	4, 0x90
.LBB35_5:                               #   Parent Loop BB35_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rax
	movq	16(%rax), %rcx
	movq	8(%rcx), %rcx
	cmpl	%r14d, (%rcx)
	jne	.LBB35_7
# BB#6:                                 #   in Loop: Header=BB35_5 Depth=2
	movl	(%rax), %ebx
	xorl	%esi, %esi
	movl	4(%rsp), %edi           # 4-byte Reload
	callq	term_Create
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movl	%ebx, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, %r12
.LBB35_7:                               #   in Loop: Header=BB35_5 Depth=2
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB35_5
# BB#8:                                 # %._crit_edge
                                        #   in Loop: Header=BB35_4 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB35_4
	jmp	.LBB35_11
.LBB35_1:
	xorl	%r12d, %r12d
	testq	%r13, %r13
	jne	.LBB35_15
	jmp	.LBB35_14
.LBB35_9:                               # %.lr.ph52.split.us.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB35_10:                              # %.lr.ph52.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rcx, (%rdx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB35_10
.LBB35_11:                              # %._crit_edge53
	movq	8(%rsp), %rsi           # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB35_13
	.p2align	4, 0x90
.LBB35_12:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB35_12
.LBB35_13:                              # %list_Delete.exit
	testq	%r13, %r13
	je	.LBB35_14
.LBB35_15:
	testq	%r12, %r12
	je	.LBB35_19
# BB#16:                                # %.preheader.i.preheader
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB35_17:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB35_17
# BB#18:
	movq	%r12, (%rax)
	jmp	.LBB35_19
.LBB35_14:
	movq	%r12, %r13
.LBB35_19:                              # %list_Nconc.exit
	movq	%r13, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end35:
	.size	sort_ApproxPseudoLinear, .Lfunc_end35-sort_ApproxPseudoLinear
	.cfi_endproc

	.globl	sort_EliminateSubsumedClauses
	.p2align	4, 0x90
	.type	sort_EliminateSubsumedClauses,@function
sort_EliminateSubsumedClauses:          # @sort_EliminateSubsumedClauses
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi270:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi271:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi272:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi273:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi274:
	.cfi_def_cfa_offset 48
.Lcfi275:
	.cfi_offset %rbx, -40
.Lcfi276:
	.cfi_offset %r12, -32
.Lcfi277:
	.cfi_offset %r14, -24
.Lcfi278:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	xorl	%r15d, %r15d
	testq	%r14, %r14
	je	.LBB36_12
# BB#1:                                 # %.preheader.us.preheader
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB36_2:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB36_3 Depth 2
                                        #       Child Loop BB36_6 Depth 3
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB36_3:                               #   Parent Loop BB36_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB36_6 Depth 3
	movq	8(%rbx), %rdi
	movq	8(%r12), %rsi
	cmpq	%rsi, %rdi
	je	.LBB36_10
# BB#4:                                 #   in Loop: Header=BB36_3 Depth=2
	testq	%r15, %r15
	je	.LBB36_8
# BB#5:                                 # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB36_3 Depth=2
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB36_6:                               # %.lr.ph.i.us
                                        #   Parent Loop BB36_2 Depth=1
                                        #     Parent Loop BB36_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rdi, 8(%rax)
	je	.LBB36_10
# BB#7:                                 #   in Loop: Header=BB36_6 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB36_6
.LBB36_8:                               # %.loopexit.us
                                        #   in Loop: Header=BB36_3 Depth=2
	movl	$-1, %edx
	movl	$-1, %ecx
	callq	subs_Subsumes
	testl	%eax, %eax
	jne	.LBB36_9
.LBB36_10:                              # %list_PointerMember.exit.us
                                        #   in Loop: Header=BB36_3 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB36_3
	jmp	.LBB36_11
	.p2align	4, 0x90
.LBB36_9:                               # %list_PointerMember.exit.us.thread
                                        #   in Loop: Header=BB36_2 Depth=1
	movq	8(%r12), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
.LBB36_11:                              # %._crit_edge.us
                                        #   in Loop: Header=BB36_2 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB36_2
.LBB36_12:                              # %._crit_edge46
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	list_NPointerDifference
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	clause_DeleteClauseList
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end36:
	.size	sort_EliminateSubsumedClauses, .Lfunc_end36-sort_EliminateSubsumedClauses
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI37_0:
	.zero	16
	.text
	.globl	sort_ApproxStaticSortTheory
	.p2align	4, 0x90
	.type	sort_ApproxStaticSortTheory,@function
sort_ApproxStaticSortTheory:            # @sort_ApproxStaticSortTheory
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi279:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi280:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi281:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi282:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi283:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi284:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi285:
	.cfi_def_cfa_offset 128
.Lcfi286:
	.cfi_offset %rbx, -56
.Lcfi287:
	.cfi_offset %r12, -48
.Lcfi288:
	.cfi_offset %r13, -40
.Lcfi289:
	.cfi_offset %r14, -32
.Lcfi290:
	.cfi_offset %r15, -24
.Lcfi291:
	.cfi_offset %rbp, -16
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	movl	$32032, %edi            # imm = 0x7D20
	callq	memory_Malloc
	movq	%rax, %rdx
	cmpl	$2, symbol_ACTINDEX(%rip)
	movq	%rdx, (%rsp)            # 8-byte Spill
	jl	.LBB37_8
# BB#1:                                 # %.lr.ph.i
	movl	symbol_TYPEMASK(%rip), %r14d
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB37_2:                               # =>This Inner Loop Header: Depth=1
	movq	$0, 8(%rdx,%rbp,8)
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%rbp,8), %rax
	testq	%rax, %rax
	je	.LBB37_7
# BB#3:                                 #   in Loop: Header=BB37_2 Depth=1
	movl	24(%rax), %ebx
	testl	%ebx, %ebx
	jns	.LBB37_7
# BB#4:                                 # %symbol_IsPredicate.exit.i
                                        #   in Loop: Header=BB37_2 Depth=1
	movl	%ebx, %ecx
	negl	%ecx
	andl	%r14d, %ecx
	cmpl	$2, %ecx
	jne	.LBB37_7
# BB#5:                                 #   in Loop: Header=BB37_2 Depth=1
	cmpl	$1, 16(%rax)
	jne	.LBB37_7
# BB#6:                                 #   in Loop: Header=BB37_2 Depth=1
	movl	$40, %edi
	callq	memory_Malloc
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	$0, 24(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movl	$0, 16(%rax)
	movl	%ebx, 32(%rax)
	movq	%rax, 8(%rdx,%rbp,8)
	.p2align	4, 0x90
.LBB37_7:                               # %symbol_IsPredicate.exit.thread.i
                                        #   in Loop: Header=BB37_2 Depth=1
	incq	%rbp
	movslq	symbol_ACTINDEX(%rip), %rax
	cmpq	%rax, %rbp
	jl	.LBB37_2
.LBB37_8:                               # %sort_TheoryCreate.exit
	callq	st_IndexCreate
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rax, (%rcx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32008(%rcx)
	movl	$0, 32024(%rcx)
	xorl	%ebp, %ebp
	movq	%r15, %rsi
	testq	%rsi, %rsi
	je	.LBB37_43
# BB#9:                                 # %.lr.ph.i33
	movl	symbol_TYPESTATBITS(%rip), %r14d
	xorl	%eax, %eax
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB37_11
.LBB37_10:                              #   in Loop: Header=BB37_11 Depth=1
	movq	%rdi, %rbp
	jmp	.LBB37_42
	.p2align	4, 0x90
.LBB37_11:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_15 Depth 2
                                        #       Child Loop BB37_18 Depth 3
                                        #       Child Loop BB37_29 Depth 3
                                        #       Child Loop BB37_31 Depth 3
                                        #     Child Loop BB37_37 Depth 2
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	8(%rsi), %rbp
	movq	%rbp, %rdi
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	clause_HasOnlyVarsInConstraint
	testl	%eax, %eax
	je	.LBB37_40
# BB#12:                                #   in Loop: Header=BB37_11 Depth=1
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	clause_HasSortInSuccedent
	testl	%eax, %eax
	je	.LBB37_39
# BB#13:                                #   in Loop: Header=BB37_11 Depth=1
	movl	72(%rbp), %eax
	testl	%eax, %eax
	jle	.LBB37_39
# BB#14:                                # %.lr.ph95.i.i
                                        #   in Loop: Header=BB37_11 Depth=1
	movslq	68(%rbp), %rcx
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movslq	64(%rbp), %rdx
	addq	%rcx, %rdx
	addl	%edx, %eax
	movslq	%eax, %rsi
	xorl	%ebp, %ebp
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB37_15:                              #   Parent Loop BB37_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB37_18 Depth 3
                                        #       Child Loop BB37_29 Depth 3
                                        #       Child Loop BB37_31 Depth 3
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	56(%r12), %rax
	movq	(%rax,%rdx,8), %r13
	movq	24(%r13), %rdi
	xorl	%eax, %eax
	subl	(%rdi), %eax
	movl	%r14d, %ecx
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	cmpl	$1, 16(%rax)
	jne	.LBB37_33
# BB#16:                                #   in Loop: Header=BB37_15 Depth=2
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	callq	term_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	movq	$0, (%r15)
	movl	64(%r12), %r12d
	testl	%r12d, %r12d
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	jle	.LBB37_25
# BB#17:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB37_15 Depth=2
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB37_18:                              # %.lr.ph.i.i
                                        #   Parent Loop BB37_11 Depth=1
                                        #     Parent Loop BB37_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	56(%rbp), %rax
	movq	(%rax,%r14,8), %rsi
	movq	%r13, %rdi
	callq	clause_LitsHaveCommonVar
	testl	%eax, %eax
	je	.LBB37_22
# BB#19:                                #   in Loop: Header=BB37_18 Depth=3
	movq	56(%rbp), %rax
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB37_21
# BB#20:                                #   in Loop: Header=BB37_18 Depth=3
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB37_21:                              # %clause_LiteralAtom.exit.i.i
                                        #   in Loop: Header=BB37_18 Depth=3
	callq	term_Copy
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, %rbx
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB37_22:                              #   in Loop: Header=BB37_18 Depth=3
	incq	%r14
	cmpq	%r14, %r12
	jne	.LBB37_18
# BB#23:                                # %._crit_edge.i.i
                                        #   in Loop: Header=BB37_15 Depth=2
	testq	%rbx, %rbx
	movq	24(%rsp), %rcx          # 8-byte Reload
	je	.LBB37_26
# BB#24:                                #   in Loop: Header=BB37_15 Depth=2
	movq	8(%r15), %rsi
	movl	52(%rbp), %edx
	movq	%rbx, %rdi
	movq	%rcx, %rbx
	callq	sort_ApproxPseudoLinear
	movq	%rbp, %r14
	movq	%rax, %rbp
	movq	%rbx, %rcx
	jmp	.LBB37_27
.LBB37_25:                              #   in Loop: Header=BB37_15 Depth=2
	xorl	%ebp, %ebp
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB37_28
.LBB37_26:                              #   in Loop: Header=BB37_15 Depth=2
	movq	%rbp, %r14
	xorl	%ebp, %ebp
.LBB37_27:                              # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB37_15 Depth=2
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB37_28:                              # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB37_15 Depth=2
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movq	%r15, %rdx
	callq	clause_Create
	movq	%rax, %rbx
	movl	$0, 12(%rbx)
	orb	$1, 48(%rbx)
	movl	$0, 76(%rbx)
	movslq	(%r14), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 32(%rbx)
	movq	40(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 40(%rbx)
	testq	%rbp, %rbp
	je	.LBB37_30
	.p2align	4, 0x90
.LBB37_29:                              # %.lr.ph.i87.i.i
                                        #   Parent Loop BB37_11 Depth=1
                                        #     Parent Loop BB37_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB37_29
.LBB37_30:                              # %list_Delete.exit88.i.i
                                        #   in Loop: Header=BB37_15 Depth=2
	testq	%r15, %r15
	movl	symbol_TYPESTATBITS(%rip), %r14d
	movq	64(%rsp), %rbp          # 8-byte Reload
	je	.LBB37_32
	.p2align	4, 0x90
.LBB37_31:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB37_11 Depth=1
                                        #     Parent Loop BB37_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB37_31
.LBB37_32:                              # %list_Delete.exit.i.i
                                        #   in Loop: Header=BB37_15 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, %rbp
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
.LBB37_33:                              #   in Loop: Header=BB37_15 Depth=2
	incq	%rdx
	cmpq	%rsi, %rdx
	jl	.LBB37_15
# BB#34:                                # %sort_ApproxHornClauses.exit.i
                                        #   in Loop: Header=BB37_11 Depth=1
	testq	%rbp, %rbp
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	je	.LBB37_10
# BB#35:                                #   in Loop: Header=BB37_11 Depth=1
	testq	%rdi, %rdi
	je	.LBB37_42
# BB#36:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB37_11 Depth=1
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB37_37:                              # %.preheader.i.i
                                        #   Parent Loop BB37_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB37_37
# BB#38:                                #   in Loop: Header=BB37_11 Depth=1
	movq	%rdi, (%rax)
	jmp	.LBB37_42
	.p2align	4, 0x90
.LBB37_39:                              #   in Loop: Header=BB37_11 Depth=1
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB37_41
	.p2align	4, 0x90
.LBB37_40:                              #   in Loop: Header=BB37_11 Depth=1
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB37_41:                              # %list_Nconc.exit.i
                                        #   in Loop: Header=BB37_11 Depth=1
	movq	40(%rsp), %rsi          # 8-byte Reload
.LBB37_42:                              # %list_Nconc.exit.i
                                        #   in Loop: Header=BB37_11 Depth=1
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	movq	%rbp, %rax
	jne	.LBB37_11
.LBB37_43:                              # %sort_ApproxClauses.exit
	movq	%rbp, %rdi
	callq	sort_EliminateSubsumedClauses
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB37_68
# BB#44:                                # %.lr.ph.preheader
	movq	%r13, %rbx
	movq	(%rsp), %rbp            # 8-byte Reload
	.p2align	4, 0x90
.LBB37_45:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movslq	68(%rsi), %rax
	movslq	64(%rsi), %rcx
	addq	%rax, %rcx
	movq	56(%rsi), %rax
	movq	(%rax,%rcx,8), %rcx
	movq	%rbp, %rdi
	movq	%rsi, %rdx
	callq	sort_TheoryInsertClause
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB37_45
# BB#46:                                # %._crit_edge
	testq	%r13, %r13
	je	.LBB37_68
# BB#47:                                # %.lr.ph73.i.preheader
	xorl	%ebp, %ebp
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%r13, %r14
	.p2align	4, 0x90
.LBB37_48:                              # %.lr.ph73.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_50 Depth 2
	movq	8(%r14), %r13
	movq	(%r14), %r14
	movl	68(%r13), %eax
	addl	64(%r13), %eax
	addl	72(%r13), %eax
	jle	.LBB37_53
# BB#49:                                # %.lr.ph67.i
                                        #   in Loop: Header=BB37_48 Depth=1
	movl	%eax, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB37_50:                              #   Parent Loop BB37_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rcx
	movl	(%rcx), %eax
	cmpl	%eax, fol_NOT(%rip)
	jne	.LBB37_52
# BB#51:                                #   in Loop: Header=BB37_50 Depth=2
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB37_52:                              # %clause_LiteralAtom.exit.i
                                        #   in Loop: Header=BB37_50 Depth=2
	movslq	%eax, %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%rbp, (%rax)
	incq	%rbx
	cmpq	%rbx, %r12
	movq	%rax, %rbp
	jne	.LBB37_50
	jmp	.LBB37_54
	.p2align	4, 0x90
.LBB37_53:                              #   in Loop: Header=BB37_48 Depth=1
	movq	%rbp, %rax
.LBB37_54:                              # %._crit_edge68.i
                                        #   in Loop: Header=BB37_48 Depth=1
	movq	%rax, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%rax, %rbp
	testq	%r14, %r14
	jne	.LBB37_48
# BB#55:                                # %.preheader.i
	testq	%rbp, %rbp
	movq	16(%rsp), %r13          # 8-byte Reload
	je	.LBB37_68
# BB#56:                                # %.lr.ph62.i
	movl	symbol_TYPESTATBITS(%rip), %r14d
	movq	%rbp, %rbx
	movq	(%rsp), %r12            # 8-byte Reload
	.p2align	4, 0x90
.LBB37_57:                              # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	subl	8(%rbx), %eax
	movl	%r14d, %ecx
	sarl	%cl, %eax
	cltq
	movq	8(%r12,%rax,8), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB37_57
# BB#58:                                # %._crit_edge63.i
	movq	%rbp, %rdi
	callq	list_Length
	movl	%eax, %r14d
	xorl	%r15d, %r15d
	movq	%rbp, %r13
	movq	%r12, %rdi
	movq	24(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB37_59:                              # %.lr.ph.i38
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_61 Depth 2
                                        #     Child Loop BB37_64 Depth 2
	movq	8(%r13), %rdx
	xorl	%esi, %esi
	callq	sort_TheoryIsSubsortOfNoResidues
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB37_63
# BB#60:                                #   in Loop: Header=BB37_59 Depth=1
	movq	8(%rbx), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	16(%rbx), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	24(%rbx), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB37_62
	.p2align	4, 0x90
.LBB37_61:                              # %.lr.ph.i.i55.i
                                        #   Parent Loop BB37_59 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB37_61
.LBB37_62:                              # %sort_ConditionDelete.exit.i
                                        #   in Loop: Header=BB37_59 Depth=1
	movq	memory_ARRAY+320(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+320(%rip), %rax
	movq	%rbx, (%rax)
	incl	%r15d
.LBB37_63:                              #   in Loop: Header=BB37_59 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%r13), %rax
	testq	%rax, %rax
	je	.LBB37_65
	.p2align	4, 0x90
.LBB37_64:                              # %.lr.ph.i.i.i42
                                        #   Parent Loop BB37_59 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB37_64
.LBB37_65:                              # %sort_DeleteOne.exit.i
                                        #   in Loop: Header=BB37_59 Depth=1
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB37_59
	.p2align	4, 0x90
.LBB37_66:                              # %.lr.ph.i.i43
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB37_66
# BB#67:
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	%r12, %rax
	cmpl	%r14d, %r15d
	je	.LBB37_69
	jmp	.LBB37_70
.LBB37_68:                              # %._crit_edge63.thread.i
	xorl	%r15d, %r15d
	xorl	%edi, %edi
	callq	list_Length
	movl	%eax, %r14d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	%r14d, %r15d
	jne	.LBB37_70
.LBB37_69:
	movq	%rax, %rbx
	callq	sort_TheoryDelete
	movq	%rbx, %rax
	xorl	%edi, %edi
.LBB37_70:
	movq	%rdi, %rbx
	cmpl	$0, 32(%rax)
	je	.LBB37_74
# BB#71:
	movq	stdout(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$22, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, %rbp
	testq	%rbp, %rbp
	je	.LBB37_73
# BB#72:
	movl	$.L.str.8, %edi
	callq	puts
	movq	%rbp, %rdi
	callq	sort_TheoryPrint
	testq	%r13, %r13
	jne	.LBB37_75
	jmp	.LBB37_76
.LBB37_73:
	movq	stdout(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$9, %esi
	movl	$1, %edx
	callq	fwrite
.LBB37_74:
	testq	%r13, %r13
	je	.LBB37_76
	.p2align	4, 0x90
.LBB37_75:                              # %.lr.ph.i44
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB37_75
.LBB37_76:                              # %list_Delete.exit
	movq	%rbx, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end37:
	.size	sort_ApproxStaticSortTheory, .Lfunc_end37-sort_ApproxStaticSortTheory
	.cfi_endproc

	.globl	sort_ApproxDynamicSortTheory
	.p2align	4, 0x90
	.type	sort_ApproxDynamicSortTheory,@function
sort_ApproxDynamicSortTheory:           # @sort_ApproxDynamicSortTheory
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end38:
	.size	sort_ApproxDynamicSortTheory, .Lfunc_end38-sort_ApproxDynamicSortTheory
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI39_0:
	.zero	16
	.text
	.globl	sort_AnalyzeSortStructure
	.p2align	4, 0x90
	.type	sort_AnalyzeSortStructure,@function
sort_AnalyzeSortStructure:              # @sort_AnalyzeSortStructure
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi292:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi293:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi294:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi295:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi296:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi297:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi298:
	.cfi_def_cfa_offset 144
.Lcfi299:
	.cfi_offset %rbx, -56
.Lcfi300:
	.cfi_offset %r12, -48
.Lcfi301:
	.cfi_offset %r13, -40
.Lcfi302:
	.cfi_offset %r14, -32
.Lcfi303:
	.cfi_offset %r15, -24
.Lcfi304:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%rdi, %r12
	movl	$32032, %edi            # imm = 0x7D20
	callq	memory_Malloc
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpl	$2, symbol_ACTINDEX(%rip)
	jl	.LBB39_8
# BB#1:                                 # %.lr.ph.i
	movl	symbol_TYPEMASK(%rip), %r14d
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB39_2:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	$0, 8(%rax,%rbp,8)
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%rbp,8), %rax
	testq	%rax, %rax
	je	.LBB39_7
# BB#3:                                 #   in Loop: Header=BB39_2 Depth=1
	movl	24(%rax), %ebx
	testl	%ebx, %ebx
	jns	.LBB39_7
# BB#4:                                 # %symbol_IsPredicate.exit.i
                                        #   in Loop: Header=BB39_2 Depth=1
	movl	%ebx, %ecx
	negl	%ecx
	andl	%r14d, %ecx
	cmpl	$2, %ecx
	jne	.LBB39_7
# BB#5:                                 #   in Loop: Header=BB39_2 Depth=1
	cmpl	$1, 16(%rax)
	jne	.LBB39_7
# BB#6:                                 #   in Loop: Header=BB39_2 Depth=1
	movl	$40, %edi
	callq	memory_Malloc
	movq	$0, 24(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movl	$0, 16(%rax)
	movl	%ebx, 32(%rax)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, 8(%rcx,%rbp,8)
	.p2align	4, 0x90
.LBB39_7:                               # %symbol_IsPredicate.exit.thread.i
                                        #   in Loop: Header=BB39_2 Depth=1
	incq	%rbp
	movslq	symbol_ACTINDEX(%rip), %rax
	cmpq	%rax, %rbp
	jl	.LBB39_2
.LBB39_8:                               # %sort_TheoryCreate.exit
	callq	st_IndexCreate
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, (%rcx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32008(%rcx)
	movl	$0, 32024(%rcx)
	testq	%r12, %r12
	je	.LBB39_9
# BB#10:                                # %.lr.ph191
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movl	symbol_TYPESTATBITS(%rip), %r14d
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB39_11:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_13 Depth 2
                                        #     Child Loop BB39_15 Depth 2
	movq	8(%rbp), %r15
	movq	%r15, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	clause_IsPotentialSortTheoryClause
	testl	%eax, %eax
	je	.LBB39_17
# BB#12:                                #   in Loop: Header=BB39_11 Depth=1
	movq	%r15, %rdi
	callq	clause_Copy
	movq	%rax, %rbx
	movslq	68(%rbx), %rax
	movslq	64(%rbx), %rcx
	addq	%rax, %rcx
	movq	56(%rbx), %rax
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rax
	xorl	%edx, %edx
	subl	(%rax), %edx
	movl	%r14d, %ecx
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rax
	orl	$32, 20(%rax)
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB39_14
	.p2align	4, 0x90
.LBB39_13:                              # %.lr.ph.i141
                                        #   Parent Loop BB39_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB39_13
.LBB39_14:                              # %list_Delete.exit
                                        #   in Loop: Header=BB39_11 Depth=1
	movq	$0, 32(%rbx)
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.LBB39_16
	.p2align	4, 0x90
.LBB39_15:                              # %.lr.ph.i159
                                        #   Parent Loop BB39_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB39_15
.LBB39_16:                              # %list_Delete.exit160
                                        #   in Loop: Header=BB39_11 Depth=1
	movq	$0, 40(%rbx)
	movl	(%r15), %eax
	movl	%eax, (%rbx)
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	clause_SetSortConstraint
	movslq	68(%rbx), %rax
	movslq	64(%rbx), %rcx
	addq	%rax, %rcx
	movq	56(%rbx), %rax
	movq	(%rax,%rcx,8), %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	sort_TheoryInsertClause
.LBB39_17:                              #   in Loop: Header=BB39_11 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB39_11
# BB#18:                                # %.lr.ph186.preheader
	movl	$1, %r14d
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB39_19:                              # %.lr.ph186
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_22 Depth 2
                                        #       Child Loop BB39_29 Depth 3
                                        #         Child Loop BB39_30 Depth 4
                                        #       Child Loop BB39_40 Depth 3
                                        #       Child Loop BB39_43 Depth 3
	movq	8(%r12), %r13
	movl	72(%r13), %eax
	testl	%eax, %eax
	movq	%r12, 72(%rsp)          # 8-byte Spill
	jle	.LBB39_49
# BB#20:                                # %.lr.ph186
                                        #   in Loop: Header=BB39_19 Depth=1
	testl	%r14d, %r14d
	je	.LBB39_49
# BB#21:                                # %.lr.ph
                                        #   in Loop: Header=BB39_19 Depth=1
	movslq	68(%r13), %rcx
	movslq	64(%r13), %r12
	addq	%rcx, %r12
	addl	%r12d, %eax
	movslq	%eax, %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB39_22:                              #   Parent Loop BB39_19 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB39_29 Depth 3
                                        #         Child Loop BB39_30 Depth 4
                                        #       Child Loop BB39_40 Depth 3
                                        #       Child Loop BB39_43 Depth 3
	movq	56(%r13), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rax
	movl	fol_EQUALITY(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB39_47
# BB#23:                                #   in Loop: Header=BB39_22 Depth=2
	movl	%ebp, 68(%rsp)          # 4-byte Spill
	movl	%r14d, 28(%rsp)         # 4-byte Spill
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	8(%rcx), %r14
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	movq	%r13, %rdx
	movl	%r12d, %ecx
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r8
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	%r15, %r9
	callq	sort_ComputeSortNoResidues
	movq	%r13, %rdx
	movq	%rax, %r13
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movl	%r12d, %ecx
	movq	%rbx, %r8
	movq	%r15, %r9
	callq	sort_ComputeSortNoResidues
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	8(%rax), %r14
	testq	%r14, %r14
	je	.LBB39_24
# BB#26:                                #   in Loop: Header=BB39_22 Depth=2
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	8(%r13), %rbp
	testq	%rbp, %rbp
	je	.LBB39_27
# BB#28:                                #   in Loop: Header=BB39_22 Depth=2
	movq	%r14, %rdi
	callq	list_Length
	movl	%eax, %ebx
	movq	%rbp, %rdi
	callq	list_Length
	cmpl	%eax, %ebx
	movq	40(%rsp), %r13          # 8-byte Reload
	jne	.LBB39_35
	.p2align	4, 0x90
.LBB39_29:                              # %.preheader.us.i
                                        #   Parent Loop BB39_19 Depth=1
                                        #     Parent Loop BB39_22 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB39_30 Depth 4
	movq	8(%r14), %rax
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB39_30:                              #   Parent Loop BB39_19 Depth=1
                                        #     Parent Loop BB39_22 Depth=2
                                        #       Parent Loop BB39_29 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rcx), %rdx
	movq	(%rcx), %rcx
	cmpq	%rdx, %rax
	setne	%bl
	testq	%rcx, %rcx
	je	.LBB39_32
# BB#31:                                #   in Loop: Header=BB39_30 Depth=4
	testb	%bl, %bl
	jne	.LBB39_30
.LBB39_32:                              # %..critedge_crit_edge.us.i
                                        #   in Loop: Header=BB39_29 Depth=3
	cmpq	%rdx, %rax
	jne	.LBB39_35
# BB#33:                                #   in Loop: Header=BB39_29 Depth=3
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB39_29
# BB#34:                                #   in Loop: Header=BB39_22 Depth=2
	movl	68(%rsp), %ebp          # 4-byte Reload
	movl	28(%rsp), %r14d         # 4-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB39_46
.LBB39_24:                              #   in Loop: Header=BB39_22 Depth=2
	xorl	%r14d, %r14d
	movq	%r13, %rdi
	jmp	.LBB39_25
.LBB39_35:                              # %.loopexit175
                                        #   in Loop: Header=BB39_22 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rbx
	movq	8(%rax), %rsi
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rdx
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	sort_TheoryIsSubsortOfNoResidues
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB39_36
# BB#37:                                #   in Loop: Header=BB39_22 Depth=2
	movq	56(%r13), %rax
	movq	(%rax,%r12,8), %rax
	cmpl	$0, 8(%rax)
	je	.LBB39_39
# BB#38:                                #   in Loop: Header=BB39_22 Depth=2
	movl	28(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB39_42
.LBB39_27:                              #   in Loop: Header=BB39_22 Depth=2
	xorl	%r14d, %r14d
	movq	16(%rsp), %rdi          # 8-byte Reload
.LBB39_25:                              # %sort_Eq.exit
                                        #   in Loop: Header=BB39_22 Depth=2
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	xorl	%ebp, %ebp
	jmp	.LBB39_46
.LBB39_39:                              #   in Loop: Header=BB39_22 Depth=2
	movq	8(%r15), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	16(%r15), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	24(%r15), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB39_41
	.p2align	4, 0x90
.LBB39_40:                              # %.lr.ph.i.i153
                                        #   Parent Loop BB39_19 Depth=1
                                        #     Parent Loop BB39_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB39_40
.LBB39_41:                              # %.loopexit
                                        #   in Loop: Header=BB39_22 Depth=2
	movq	memory_ARRAY+320(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	memory_ARRAY+320(%rip), %rax
	movq	%r15, (%rax)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rsi
	movq	8(%rbx), %rdx
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	sort_TheoryIsSubsortOfNoResidues
	movq	%rax, %r15
	testq	%r15, %r15
	movl	28(%rsp), %r14d         # 4-byte Reload
	je	.LBB39_36
.LBB39_42:                              # %.thread163
                                        #   in Loop: Header=BB39_22 Depth=2
	movq	8(%r15), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	16(%r15), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	24(%r15), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB39_44
	.p2align	4, 0x90
.LBB39_43:                              # %.lr.ph.i.i
                                        #   Parent Loop BB39_19 Depth=1
                                        #     Parent Loop BB39_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB39_43
.LBB39_44:                              # %sort_ConditionDelete.exit
                                        #   in Loop: Header=BB39_22 Depth=2
	movq	memory_ARRAY+320(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	memory_ARRAY+320(%rip), %rax
	movq	%r15, (%rax)
	jmp	.LBB39_45
.LBB39_36:                              #   in Loop: Header=BB39_22 Depth=2
	xorl	%r14d, %r14d
.LBB39_45:                              # %sort_Eq.exit
                                        #   in Loop: Header=BB39_22 Depth=2
	xorl	%ebp, %ebp
	movq	16(%rsp), %rdi          # 8-byte Reload
.LBB39_46:                              # %sort_Eq.exit
                                        #   in Loop: Header=BB39_22 Depth=2
	callq	sort_PairDelete
	movq	%rbx, %rdi
	callq	sort_PairDelete
	movq	80(%rsp), %rdx          # 8-byte Reload
.LBB39_47:                              #   in Loop: Header=BB39_22 Depth=2
	incq	%r12
	cmpq	%rdx, %r12
	jge	.LBB39_49
# BB#48:                                #   in Loop: Header=BB39_22 Depth=2
	testl	%r14d, %r14d
	jne	.LBB39_22
.LBB39_49:                              # %._crit_edge
                                        #   in Loop: Header=BB39_19 Depth=1
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.LBB39_51
# BB#50:                                # %._crit_edge
                                        #   in Loop: Header=BB39_19 Depth=1
	testl	%r14d, %r14d
	jne	.LBB39_19
.LBB39_51:                              # %._crit_edge187.loopexit
	cmpl	$1, %r14d
	movl	$1, %ebx
	sbbl	$-1, %ebx
	jmp	.LBB39_52
.LBB39_9:
	movl	$2, %ebx
	movl	$1, %ebp
.LBB39_52:                              # %._crit_edge187
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	sort_TheoryDelete
	testl	%ebp, %ebp
	movl	$3, %eax
	cmovel	%ebx, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end39:
	.size	sort_AnalyzeSortStructure, .Lfunc_end39-sort_AnalyzeSortStructure
	.cfi_endproc

	.p2align	4, 0x90
	.type	sort_VarSort,@function
sort_VarSort:                           # @sort_VarSort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi305:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi306:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi307:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi308:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi309:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi310:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi311:
	.cfi_def_cfa_offset 80
.Lcfi312:
	.cfi_offset %rbx, -56
.Lcfi313:
	.cfi_offset %r12, -48
.Lcfi314:
	.cfi_offset %r13, -40
.Lcfi315:
	.cfi_offset %r14, -32
.Lcfi316:
	.cfi_offset %r15, -24
.Lcfi317:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	64(%rbx), %eax
	addl	68(%rbx), %eax
	decl	%eax
	js	.LBB40_1
# BB#2:                                 # %.lr.ph
	movl	symbol_TYPESTATBITS(%rip), %r12d
	movl	%ecx, %ebp
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	%esi, 12(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB40_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_12 Depth 2
	cmpq	%rbp, %r14
	je	.LBB40_15
# BB#4:                                 #   in Loop: Header=BB40_3 Depth=1
	movq	56(%rbx), %rax
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rax
	movl	(%rax), %edx
	cmpl	%edx, fol_NOT(%rip)
	jne	.LBB40_6
# BB#5:                                 #   in Loop: Header=BB40_3 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %edx
.LBB40_6:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB40_3 Depth=1
	negl	%edx
	movl	%r12d, %ecx
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rdi
	movslq	%edx, %rcx
	movq	(%rdi,%rcx,8), %rdx
	cmpl	$1, 16(%rdx)
	jne	.LBB40_15
# BB#7:                                 #   in Loop: Header=BB40_3 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	cmpl	%esi, (%rax)
	jne	.LBB40_15
# BB#8:                                 #   in Loop: Header=BB40_3 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax,%rcx,8), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	testq	%r13, %r13
	je	.LBB40_9
# BB#10:                                #   in Loop: Header=BB40_3 Depth=1
	testq	%rax, %rax
	je	.LBB40_14
# BB#11:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB40_3 Depth=1
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB40_12:                              # %.preheader.i.i
                                        #   Parent Loop BB40_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB40_12
# BB#13:                                #   in Loop: Header=BB40_3 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB40_14
.LBB40_9:                               #   in Loop: Header=BB40_3 Depth=1
	movq	%rax, %r13
.LBB40_14:                              # %sort_Intersect.exit
                                        #   in Loop: Header=BB40_3 Depth=1
	movl	12(%rsp), %esi          # 4-byte Reload
	.p2align	4, 0x90
.LBB40_15:                              # %sort_Intersect.exit
                                        #   in Loop: Header=BB40_3 Depth=1
	movl	64(%rbx), %eax
	movl	68(%rbx), %ecx
	leal	-1(%rax,%rcx), %eax
	cltq
	cmpq	%rax, %r14
	leaq	1(%r14), %r14
	jl	.LBB40_3
	jmp	.LBB40_16
.LBB40_1:
	xorl	%r13d, %r13d
.LBB40_16:                              # %._crit_edge
	movq	%r13, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end40:
	.size	sort_VarSort, .Lfunc_end40-sort_VarSort
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	":["
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" Empty Theory"
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n Subsort Clauses:"
	.size	.L.str.2, 19

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n\t\t Clause:"
	.size	.L.str.3, 12

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n\t\t Link: "
	.size	.L.str.4, 11

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\n Term Declaration Clauses:"
	.size	.L.str.5, 28

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n\n Starting Soju Search:"
	.size	.L.str.6, 25

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\n\n Approx Sort Theory:"
	.size	.L.str.7, 23

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\n"
	.size	.L.str.8, 2

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	" trivial."
	.size	.L.str.9, 10

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	",%d"
	.size	.L.str.10, 4

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Input: ("
	.size	.L.str.11, 9

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	") Output: "
	.size	.L.str.12, 11

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"  C: ("
	.size	.L.str.13, 7

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	") A: ("
	.size	.L.str.14, 7

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	") S: ("
	.size	.L.str.15, 7

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	") Clause: %d Card: %d Fire: %d Var: "
	.size	.L.str.16, 37


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
