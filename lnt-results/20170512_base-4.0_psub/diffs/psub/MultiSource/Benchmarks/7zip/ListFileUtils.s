	.text
	.file	"ListFileUtils.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	_Z21ReadNamesFromListFilePKwR13CObjectVectorI11CStringBaseIwEEj
	.p2align	4, 0x90
	.type	_Z21ReadNamesFromListFilePKwR13CObjectVectorI11CStringBaseIwEEj,@function
_Z21ReadNamesFromListFilePKwR13CObjectVectorI11CStringBaseIwEEj: # @_Z21ReadNamesFromListFilePKwR13CObjectVectorI11CStringBaseIwEEj
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1176, %rsp             # imm = 0x498
.Lcfi6:
	.cfi_def_cfa_offset 1232
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movl	$-1, 96(%rsp)
	movq	$0, 112(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, 104(%rsp)
	movb	$0, (%rax)
	movl	$4, 116(%rsp)
	movq	$_ZTVN8NWindows5NFile3NIO7CInFileE+16, 88(%rsp)
.Ltmp0:
	leaq	88(%rsp), %rbp
	movl	$1, %edx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwb
.Ltmp1:
# BB#1:
	testb	%al, %al
	je	.LBB0_20
# BB#2:
.Ltmp3:
	leaq	88(%rsp), %r14
	leaq	72(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZNK8NWindows5NFile3NIO9CFileBase9GetLengthERy
.Ltmp4:
# BB#3:
	movq	72(%rsp), %rbp
	movl	$2147483648, %ecx       # imm = 0x80000000
	xorl	%ebx, %ebx
	cmpq	%rcx, %rbp
	ja	.LBB0_106
# BB#4:
	xorb	$1, %al
	jne	.LBB0_106
# BB#5:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
.Ltmp5:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp6:
# BB#6:
	movq	%rbx, 32(%rsp)
	movb	$0, (%rbx)
	movl	$4, 44(%rsp)
	leal	1(%rbp), %eax
	cmpl	$4, %eax
	movq	%r14, 64(%rsp)          # 8-byte Spill
	jl	.LBB0_10
# BB#7:
	leal	2(%rbp), %r14d
	cmpl	$4, %r14d
	je	.LBB0_10
# BB#8:
	movslq	%r14d, %rdi
.Ltmp8:
	callq	_Znam
	movq	%rax, %r13
.Ltmp9:
# BB#9:
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	72(%rsp), %rbp
	movslq	40(%rsp), %rax
	movq	%r13, 32(%rsp)
	movb	$0, (%r13,%rax)
	movl	%r14d, 44(%rsp)
	movq	%r13, %rbx
.LBB0_10:
.Ltmp11:
	leaq	88(%rsp), %rdi
	leaq	84(%rsp), %rcx
	movq	%rbx, %rsi
	movl	%ebp, %edx
	callq	_ZN8NWindows5NFile3NIO7CInFile4ReadEPvjRj
.Ltmp12:
# BB#11:
	testb	%al, %al
	je	.LBB0_21
# BB#12:
	movl	72(%rsp), %eax
	movb	$0, (%rbx,%rax)
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	movl	$-1, %eax
	movq	32(%rsp), %rdx
	movabsq	$4294967296, %rsi       # imm = 0x100000000
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB0_13:                               # =>This Inner Loop Header: Depth=1
	addq	%rsi, %rcx
	incl	%eax
	cmpb	$0, (%rdi)
	leaq	1(%rdi), %rdi
	jne	.LBB0_13
# BB#14:
	sarq	$32, %rcx
	movb	$0, (%rdx,%rcx)
	movl	%eax, 40(%rsp)
.Ltmp13:
	leaq	88(%rsp), %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
.Ltmp14:
# BB#15:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
.Ltmp15:
	movl	$16, %edi
	callq	_Znam
.Ltmp16:
# BB#16:
	movq	%rax, 16(%rsp)
	movl	$0, (%rax)
	movl	$4, 28(%rsp)
	cmpl	$65001, %r15d           # imm = 0xFDE9
	jne	.LBB0_22
# BB#17:
.Ltmp24:
	leaq	32(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	_Z20ConvertUTF8ToUnicodeRK11CStringBaseIcERS_IwE
.Ltmp25:
# BB#18:
	testb	%al, %al
	jne	.LBB0_33
# BB#19:
	xorl	%ebx, %ebx
	jmp	.LBB0_102
.LBB0_20:                               # %._crit_edge84
	leaq	88(%rsp), %rdi
	xorl	%ebx, %ebx
	jmp	.LBB0_107
.LBB0_21:
	xorl	%ebx, %ebx
	jmp	.LBB0_104
.LBB0_22:
.Ltmp18:
	movq	%rsp, %rdi
	leaq	32(%rsp), %rsi
	movl	%r15d, %edx
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp19:
# BB#23:
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	8(%rsp), %rbp
	incq	%rbp
	movl	28(%rsp), %r14d
	cmpl	%r14d, %ebp
	je	.LBB0_29
# BB#24:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp21:
	callq	_Znam
	movq	%rax, %r15
.Ltmp22:
# BB#25:                                # %.noexc42
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB0_28
# BB#26:                                # %.noexc42
	testl	%r14d, %r14d
	jle	.LBB0_28
# BB#27:                                # %._crit_edge.thread.i.i41
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	24(%rsp), %rax
.LBB0_28:                               # %._crit_edge16.i.i
	movq	%r15, 16(%rsp)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 28(%rsp)
	movq	%r15, %rbx
.LBB0_29:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_30:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB0_30
# BB#31:
	movl	8(%rsp), %eax
	movl	%eax, 24(%rsp)
	testq	%rdi, %rdi
	je	.LBB0_33
# BB#32:
	callq	_ZdaPv
.LBB0_33:
	movl	24(%rsp), %ebp
	testl	%ebp, %ebp
	je	.LBB0_37
# BB#34:
	movq	16(%rsp), %rdi
	cmpl	$65279, (%rdi)          # imm = 0xFEFF
	jne	.LBB0_38
# BB#35:
	cmpl	$2, %ebp
	movl	$1, %eax
	cmovll	%ebp, %eax
	testl	%ebp, %ebp
	jle	.LBB0_38
# BB#36:
	movslq	%eax, %rbx
	leaq	(%rdi,%rbx,4), %rsi
	incl	%ebp
	subl	%ebx, %ebp
	movslq	%ebp, %rdx
	shlq	$2, %rdx
	callq	memmove
	movl	24(%rsp), %ebp
	subl	%ebx, %ebp
	movl	%ebp, 24(%rsp)
	jmp	.LBB0_38
.LBB0_37:
	xorl	%ebp, %ebp
.LBB0_38:                               # %_ZN11CStringBaseIwE6DeleteEii.exit
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp26:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp27:
# BB#39:                                # %_ZN11CStringBaseIwEC2Ev.exit46
	movq	%rbx, (%rsp)
	movl	$0, (%rbx)
	movl	$4, 12(%rsp)
	testl	%ebp, %ebp
	jle	.LBB0_87
# BB#40:                                # %.lr.ph
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.LBB0_41
.LBB0_50:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_41 Depth=1
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_53
# BB#51:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_41 Depth=1
	negq	%rsi
	xorl	%edi, %edi
.LBB0_52:                               # %vector.body.prol
                                        #   Parent Loop BB0_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbx,%rdi,4), %xmm0
	movups	16(%rbx,%rdi,4), %xmm1
	movups	%xmm0, (%r13,%rdi,4)
	movups	%xmm1, 16(%r13,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB0_52
	jmp	.LBB0_54
.LBB0_53:                               #   in Loop: Header=BB0_41 Depth=1
	xorl	%edi, %edi
.LBB0_54:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_41 Depth=1
	cmpq	$24, %rdx
	jb	.LBB0_57
# BB#55:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_41 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%r13,%rdi,4), %rsi
	leaq	112(%rbx,%rdi,4), %rdi
.LBB0_56:                               # %vector.body
                                        #   Parent Loop BB0_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB0_56
.LBB0_57:                               # %middle.block
                                        #   in Loop: Header=BB0_41 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB0_70
	jmp	.LBB0_83
	.p2align	4, 0x90
.LBB0_41:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_52 Depth 2
                                        #     Child Loop BB0_56 Depth 2
                                        #     Child Loop BB0_72 Depth 2
                                        #     Child Loop BB0_75 Depth 2
                                        #     Child Loop BB0_78 Depth 2
	movq	16(%rsp), %rax
	movl	(%rax,%r15,4), %ebp
	cmpl	$13, %ebp
	je	.LBB0_43
# BB#42:                                #   in Loop: Header=BB0_41 Depth=1
	cmpl	$10, %ebp
	jne	.LBB0_58
.LBB0_43:                               #   in Loop: Header=BB0_41 Depth=1
.Ltmp29:
	movq	%rsp, %rdi
	callq	_ZN11CStringBaseIwE4TrimEv
.Ltmp30:
# BB#44:                                #   in Loop: Header=BB0_41 Depth=1
.Ltmp31:
	movq	%rsp, %rdi
	callq	_ZL11RemoveQuoteR11CStringBaseIwE
.Ltmp32:
# BB#45:                                #   in Loop: Header=BB0_41 Depth=1
	movslq	8(%rsp), %r14
	testq	%r14, %r14
	je	.LBB0_81
# BB#46:                                #   in Loop: Header=BB0_41 Depth=1
.Ltmp33:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp34:
# BB#47:                                # %.noexc50
                                        #   in Loop: Header=BB0_41 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	leaq	1(%r14), %rbp
	testl	%ebp, %ebp
	je	.LBB0_76
# BB#48:                                # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB0_41 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp35:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp36:
# BB#49:                                # %.noexc.i
                                        #   in Loop: Header=BB0_41 Depth=1
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%ebp, 12(%rbx)
	jmp	.LBB0_77
	.p2align	4, 0x90
.LBB0_58:                               #   in Loop: Header=BB0_41 Depth=1
	movl	12(%rsp), %r12d
	movl	%r12d, %eax
	subl	%r14d, %eax
	cmpl	$1, %eax
	jg	.LBB0_85
# BB#59:                                #   in Loop: Header=BB0_41 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$8, %r12d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r12d
	jl	.LBB0_61
# BB#60:                                # %select.true.sink
                                        #   in Loop: Header=BB0_41 Depth=1
	movl	%r12d, %edx
	shrl	$31, %edx
	addl	%r12d, %edx
	sarl	%edx
.LBB0_61:                               # %select.end
                                        #   in Loop: Header=BB0_41 Depth=1
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%r12,%rsi), %eax
	cmpl	%r12d, %eax
	je	.LBB0_85
# BB#62:                                #   in Loop: Header=BB0_41 Depth=1
	movl	%eax, 80(%rsp)          # 4-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp40:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp41:
# BB#63:                                # %.noexc64
                                        #   in Loop: Header=BB0_41 Depth=1
	testl	%r12d, %r12d
	jle	.LBB0_84
# BB#64:                                # %.preheader.i.i54
                                        #   in Loop: Header=BB0_41 Depth=1
	testl	%r14d, %r14d
	jle	.LBB0_82
# BB#65:                                # %.lr.ph.i.i55
                                        #   in Loop: Header=BB0_41 Depth=1
	movslq	%r14d, %rax
	cmpl	$7, %r14d
	jbe	.LBB0_69
# BB#66:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_41 Depth=1
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB0_69
# BB#67:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_41 Depth=1
	leaq	(%rbx,%rax,4), %rdx
	cmpq	%rdx, %r13
	jae	.LBB0_50
# BB#68:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_41 Depth=1
	leaq	(%r13,%rax,4), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB0_50
.LBB0_69:                               #   in Loop: Header=BB0_41 Depth=1
	xorl	%ecx, %ecx
.LBB0_70:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_41 Depth=1
	subl	%ecx, %r14d
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %r14
	je	.LBB0_73
# BB#71:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB0_41 Depth=1
	negq	%r14
	.p2align	4, 0x90
.LBB0_72:                               # %scalar.ph.prol
                                        #   Parent Loop BB0_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rcx,4), %esi
	movl	%esi, (%r13,%rcx,4)
	incq	%rcx
	incq	%r14
	jne	.LBB0_72
.LBB0_73:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB0_41 Depth=1
	cmpq	$7, %rdx
	jb	.LBB0_83
# BB#74:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB0_41 Depth=1
	subq	%rcx, %rax
	leaq	28(%r13,%rcx,4), %rdx
	leaq	28(%rbx,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB0_75:                               # %scalar.ph
                                        #   Parent Loop BB0_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB0_75
	jmp	.LBB0_83
.LBB0_76:                               #   in Loop: Header=BB0_41 Depth=1
	xorl	%eax, %eax
.LBB0_77:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB0_41 Depth=1
	movq	(%rsp), %rcx
	.p2align	4, 0x90
.LBB0_78:                               #   Parent Loop BB0_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB0_78
# BB#79:                                #   in Loop: Header=BB0_41 Depth=1
	movl	%r14d, 8(%rbx)
.Ltmp38:
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp39:
# BB#80:                                # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit
                                        #   in Loop: Header=BB0_41 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rdx)
.LBB0_81:                               #   in Loop: Header=BB0_41 Depth=1
	movl	$0, 8(%rsp)
	xorl	%r14d, %r14d
	movq	(%rsp), %rbx
	movq	%rbx, %rax
	jmp	.LBB0_86
.LBB0_82:                               # %._crit_edge.i.i56
                                        #   in Loop: Header=BB0_41 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_84
.LBB0_83:                               # %._crit_edge.thread.i.i61
                                        #   in Loop: Header=BB0_41 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movl	8(%rsp), %r14d
.LBB0_84:                               # %._crit_edge16.i.i62
                                        #   in Loop: Header=BB0_41 Depth=1
	movq	%r13, (%rsp)
	movslq	%r14d, %rax
	movl	$0, (%r13,%rax,4)
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	%eax, 12(%rsp)
	movq	%r13, %rbx
.LBB0_85:                               # %_ZN11CStringBaseIwEpLEw.exit
                                        #   in Loop: Header=BB0_41 Depth=1
	movslq	%r14d, %rax
	movl	%ebp, (%rbx,%rax,4)
	leal	1(%rax), %r14d
	movl	%r14d, 8(%rsp)
	leaq	4(%rbx,%rax,4), %rax
.LBB0_86:                               #   in Loop: Header=BB0_41 Depth=1
	movl	$0, (%rax)
	incq	%r15
	movslq	24(%rsp), %rax
	cmpq	%rax, %r15
	jl	.LBB0_41
.LBB0_87:                               # %._crit_edge
.Ltmp43:
	movq	%rsp, %rdi
	callq	_ZN11CStringBaseIwE4TrimEv
.Ltmp44:
# BB#88:
.Ltmp45:
	movq	%rsp, %rdi
	callq	_ZL11RemoveQuoteR11CStringBaseIwE
.Ltmp46:
# BB#89:
	movslq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB0_99
# BB#90:
.Ltmp47:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp48:
# BB#91:                                # %.noexc70
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	leaq	1(%rbx), %rbp
	testl	%ebp, %ebp
	je	.LBB0_94
# BB#92:                                # %._crit_edge16.i.i.i65
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp49:
	callq	_Znam
.Ltmp50:
# BB#93:                                # %.noexc.i66
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%ebp, 12(%r14)
	jmp	.LBB0_95
.LBB0_94:
	xorl	%eax, %eax
.LBB0_95:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i67
	movq	(%rsp), %rcx
	.p2align	4, 0x90
.LBB0_96:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB0_96
# BB#97:
	movl	%ebx, 8(%r14)
.Ltmp52:
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp53:
# BB#98:                                # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit74
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movq	%r14, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rdx)
.LBB0_99:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_101
# BB#100:
	callq	_ZdaPv
.LBB0_101:                              # %_ZN11CStringBaseIwED2Ev.exit75
	movb	$1, %bl
.LBB0_102:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_104
# BB#103:
	callq	_ZdaPv
.LBB0_104:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_106
# BB#105:
	callq	_ZdaPv
.LBB0_106:
	leaq	88(%rsp), %rdi
.LBB0_107:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	movl	%ebx, %eax
	addq	$1176, %rsp             # imm = 0x498
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_108:
.Ltmp23:
	jmp	.LBB0_122
.LBB0_109:
.Ltmp51:
	movq	%rax, %rbp
	movq	%r14, %rdi
	jmp	.LBB0_116
.LBB0_110:
.Ltmp20:
	jmp	.LBB0_113
.LBB0_111:                              # %.thread
.Ltmp10:
	movq	%rax, %rbp
	jmp	.LBB0_128
.LBB0_112:
.Ltmp28:
.LBB0_113:
	movq	%rax, %rbp
	jmp	.LBB0_125
.LBB0_114:
.Ltmp54:
	jmp	.LBB0_122
.LBB0_115:
.Ltmp37:
	movq	%rax, %rbp
	movq	%rbx, %rdi
.LBB0_116:                              # %.body
	callq	_ZdlPv
	jmp	.LBB0_123
.LBB0_117:
.Ltmp17:
	movq	%rax, %rbp
	jmp	.LBB0_127
.LBB0_118:
.Ltmp7:
	movq	%r14, 64(%rsp)          # 8-byte Spill
	jmp	.LBB0_120
.LBB0_119:
.Ltmp2:
	movq	%rbp, 64(%rsp)          # 8-byte Spill
.LBB0_120:
	movq	%rax, %rbp
	jmp	.LBB0_129
.LBB0_121:
.Ltmp42:
.LBB0_122:                              # %.body
	movq	%rax, %rbp
.LBB0_123:                              # %.body
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_125
# BB#124:
	callq	_ZdaPv
.LBB0_125:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_127
# BB#126:
	callq	_ZdaPv
.LBB0_127:
	movq	32(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB0_129
.LBB0_128:
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB0_129:
.Ltmp55:
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp56:
# BB#130:
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB0_131:
.Ltmp57:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_Z21ReadNamesFromListFilePKwR13CObjectVectorI11CStringBaseIwEEj, .Lfunc_end0-_Z21ReadNamesFromListFilePKwR13CObjectVectorI11CStringBaseIwEEj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\200\002"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\367\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp16-.Ltmp11         #   Call between .Ltmp11 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin0   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp26-.Ltmp22         #   Call between .Ltmp22 and .Ltmp26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp34-.Ltmp29         #   Call between .Ltmp29 and .Ltmp34
	.long	.Ltmp42-.Lfunc_begin0   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin0   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp39-.Ltmp40         #   Call between .Ltmp40 and .Ltmp39
	.long	.Ltmp42-.Lfunc_begin0   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp48-.Ltmp43         #   Call between .Ltmp43 and .Ltmp48
	.long	.Ltmp54-.Lfunc_begin0   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin0   #     jumps to .Ltmp51
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin0   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp55-.Ltmp53         #   Call between .Ltmp53 and .Ltmp55
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin0   #     jumps to .Ltmp57
	.byte	1                       #   On action: 1
	.long	.Ltmp56-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Lfunc_end0-.Ltmp56     #   Call between .Ltmp56 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.text._ZN11CStringBaseIwE4TrimEv,"axG",@progbits,_ZN11CStringBaseIwE4TrimEv,comdat
	.weak	_ZN11CStringBaseIwE4TrimEv
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE4TrimEv,@function
_ZN11CStringBaseIwE4TrimEv:             # @_ZN11CStringBaseIwE4TrimEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	callq	_ZN11CStringBaseIwE9TrimRightEv
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv
	movq	(%r14), %rdi
	movq	8(%rsp), %rax
	movl	(%rax), %r8d
	movq	%rdi, %rbx
	jmp	.LBB2_1
	.p2align	4, 0x90
.LBB2_5:                                # %_ZNK11CStringBaseIwE4FindEw.exit.i.i
                                        #   in Loop: Header=BB2_1 Depth=1
	testl	%esi, %esi
	je	.LBB2_8
# BB#6:                                 # %_ZNK11CStringBaseIwE4FindEw.exit.i.i
                                        #   in Loop: Header=BB2_1 Depth=1
	subq	%rax, %rdx
	shrq	$2, %rdx
	testl	%edx, %edx
	js	.LBB2_8
# BB#7:                                 #   in Loop: Header=BB2_1 Depth=1
	addq	$4, %rbx
.LBB2_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
	movl	(%rbx), %esi
	cmpl	%esi, %r8d
	movq	%rax, %rdx
	je	.LBB2_5
# BB#2:                                 # %.lr.ph.i.i.i.i.preheader
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	%r8d, %ecx
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph.i.i.i.i
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ecx, %ecx
	je	.LBB2_8
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=2
	movl	4(%rdx), %ecx
	addq	$4, %rdx
	cmpl	%esi, %ecx
	jne	.LBB2_3
	jmp	.LBB2_5
.LBB2_8:                                # %.critedge.i.i
	subq	%rdi, %rbx
	shrq	$2, %rbx
	movl	8(%r14), %ecx
	cmpl	%ecx, %ebx
	cmovgl	%ecx, %ebx
	testl	%ebx, %ebx
	jle	.LBB2_10
# BB#9:
	movslq	%ebx, %rax
	leaq	(%rdi,%rax,4), %rsi
	incl	%ecx
	subl	%ebx, %ecx
	movslq	%ecx, %rdx
	shlq	$2, %rdx
	callq	memmove
	subl	%ebx, 8(%r14)
	movq	8(%rsp), %rax
.LBB2_10:                               # %_ZN11CStringBaseIwE19TrimLeftWithCharSetERKS0_.exit.i
	testq	%rax, %rax
	je	.LBB2_12
# BB#11:
	movq	%rax, %rdi
	callq	_ZdaPv
.LBB2_12:                               # %_ZN11CStringBaseIwE8TrimLeftEv.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	_ZN11CStringBaseIwE4TrimEv, .Lfunc_end2-_ZN11CStringBaseIwE4TrimEv
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZL11RemoveQuoteR11CStringBaseIwE,@function
_ZL11RemoveQuoteR11CStringBaseIwE:      # @_ZL11RemoveQuoteR11CStringBaseIwE
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 64
.Lcfi24:
	.cfi_offset %rbx, -48
.Lcfi25:
	.cfi_offset %r12, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movslq	8(%r14), %rcx
	cmpq	$2, %rcx
	jl	.LBB3_16
# BB#1:
	movq	(%r14), %rax
	cmpl	$34, (%rax)
	jne	.LBB3_16
# BB#2:
	cmpl	$34, -4(%rax,%rcx,4)
	jne	.LBB3_16
# BB#3:
	addl	$-2, %ecx
	movq	%rsp, %rbx
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZNK11CStringBaseIwE3MidEii
	cmpq	%r14, %rbx
	je	.LBB3_4
# BB#5:
	movl	$0, 8(%r14)
	movq	(%r14), %rbx
	movl	$0, (%rbx)
	movslq	8(%rsp), %r15
	incq	%r15
	movl	12(%r14), %ebp
	cmpl	%ebp, %r15d
	je	.LBB3_11
# BB#6:
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp58:
	callq	_Znam
	movq	%rax, %r12
.Ltmp59:
# BB#7:                                 # %.noexc
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB3_10
# BB#8:                                 # %.noexc
	testl	%ebp, %ebp
	jle	.LBB3_10
# BB#9:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r14), %rax
.LBB3_10:                               # %._crit_edge16.i.i
	movq	%r12, (%r14)
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
	movq	%r12, %rbx
.LBB3_11:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_12:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB3_12
# BB#13:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%rsp), %eax
	movl	%eax, 8(%r14)
	testq	%rdi, %rdi
	jne	.LBB3_15
	jmp	.LBB3_16
.LBB3_4:                                # %._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB3_16
.LBB3_15:
	callq	_ZdaPv
.LBB3_16:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_17:
.Ltmp60:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_19
# BB#18:
	callq	_ZdaPv
.LBB3_19:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZL11RemoveQuoteR11CStringBaseIwE, .Lfunc_end3-_ZL11RemoveQuoteR11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp58-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp58
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin1   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Lfunc_end3-.Ltmp59     #   Call between .Ltmp59 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NWindows5NFile3NIO7CInFileD0Ev,"axG",@progbits,_ZN8NWindows5NFile3NIO7CInFileD0Ev,comdat
	.weak	_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO7CInFileD0Ev,@function
_ZN8NWindows5NFile3NIO7CInFileD0Ev:     # @_ZN8NWindows5NFile3NIO7CInFileD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -24
.Lcfi33:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp61:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp62:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB4_2:
.Ltmp63:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN8NWindows5NFile3NIO7CInFileD0Ev, .Lfunc_end4-_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp61-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin2   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end4-.Ltmp62     #   Call between .Ltmp62 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 64
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB5_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB5_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB5_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB5_5
.LBB5_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB5_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp64:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp65:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB5_35
# BB#12:
	movq	%rbx, %r13
.LBB5_13:                               # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB5_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB5_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB5_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB5_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB5_15
.LBB5_14:
	xorl	%esi, %esi
.LBB5_15:                               # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB5_16:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB5_16
.LBB5_29:
	movq	%r13, %rbx
.LBB5_30:                               # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp66:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp67:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB5_32:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB5_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB5_8
.LBB5_3:
	xorl	%eax, %eax
.LBB5_5:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB5_6:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB5_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB5_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_35:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB5_30
.LBB5_21:                               # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB5_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_24:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB5_24
	jmp	.LBB5_25
.LBB5_22:
	xorl	%ecx, %ecx
.LBB5_25:                               # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB5_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB5_27:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB5_27
.LBB5_28:                               # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB5_15
	jmp	.LBB5_29
.LBB5_34:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp68:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end5-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp64-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp64
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp67-.Ltmp64         #   Call between .Ltmp64 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin3   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Lfunc_end5-.Ltmp67     #   Call between .Ltmp67 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIwE9TrimRightEv,"axG",@progbits,_ZN11CStringBaseIwE9TrimRightEv,comdat
	.weak	_ZN11CStringBaseIwE9TrimRightEv
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE9TrimRightEv,@function
_ZN11CStringBaseIwE9TrimRightEv:        # @_ZN11CStringBaseIwE9TrimRightEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 48
.Lcfi50:
	.cfi_offset %rbx, -24
.Lcfi51:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv
	movq	(%r14), %r8
	movl	(%r8), %ebx
	testl	%ebx, %ebx
	je	.LBB6_11
# BB#1:                                 # %.lr.ph.i
	movq	8(%rsp), %r10
	movl	(%r10), %edi
	xorl	%r9d, %r9d
	movq	%r8, %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_5 Depth 2
	cmpl	%ebx, %edi
	movq	%r10, %rdx
	movl	%edi, %esi
	je	.LBB6_3
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph.i.i.i
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%esi, %esi
	je	.LBB6_6
# BB#4:                                 #   in Loop: Header=BB6_5 Depth=2
	movl	4(%rdx), %esi
	addq	$4, %rdx
	cmpl	%ebx, %esi
	jne	.LBB6_5
.LBB6_3:                                # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB6_2 Depth=1
	subq	%r10, %rdx
	shrq	$2, %rdx
	jmp	.LBB6_7
	.p2align	4, 0x90
.LBB6_6:                                #   in Loop: Header=BB6_2 Depth=1
	movl	$-1, %edx
.LBB6_7:                                # %_ZNK11CStringBaseIwE4FindEw.exit.i
                                        #   in Loop: Header=BB6_2 Depth=1
	testq	%rcx, %rcx
	cmoveq	%rax, %rcx
	testl	%edx, %edx
	cmovsq	%r9, %rcx
	movl	4(%rax), %ebx
	addq	$4, %rax
	testl	%ebx, %ebx
	jne	.LBB6_2
# BB#8:                                 # %._crit_edge.i
	testq	%rcx, %rcx
	je	.LBB6_11
# BB#9:
	subq	%r8, %rcx
	movq	%rcx, %rsi
	shrq	$2, %rsi
	movslq	8(%r14), %rax
	movl	%eax, %edx
	subl	%esi, %edx
	jle	.LBB6_11
# BB#10:
	shlq	$30, %rcx
	sarq	$30, %rcx
	movl	(%r8,%rax,4), %eax
	movl	%eax, (%r8,%rcx)
	subl	%edx, 8(%r14)
.LBB6_11:                               # %_ZN11CStringBaseIwE20TrimRightWithCharSetERKS0_.exit
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_13
# BB#12:
	callq	_ZdaPv
.LBB6_13:                               # %_ZN11CStringBaseIwED2Ev.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	_ZN11CStringBaseIwE9TrimRightEv, .Lfunc_end6-_ZN11CStringBaseIwE9TrimRightEv
	.cfi_endproc

	.section	.text._ZN11CStringBaseIwE21GetTrimDefaultCharSetEv,"axG",@progbits,_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv,comdat
	.weak	_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv,@function
_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv: # @_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 32
.Lcfi55:
	.cfi_offset %rbx, -24
.Lcfi56:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	$4, 12(%rbx)
.Ltmp69:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp70:
# BB#1:
	movq	(%rbx), %rax
	movslq	8(%rbx), %rcx
	movl	$32, (%rax,%rcx,4)
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%rbx)
	movl	$0, 4(%rax,%rcx,4)
.Ltmp71:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp72:
# BB#2:
	movq	(%rbx), %rax
	movslq	8(%rbx), %rcx
	movl	$10, (%rax,%rcx,4)
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%rbx)
	movl	$0, 4(%rax,%rcx,4)
.Ltmp73:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp74:
# BB#3:
	movq	(%rbx), %rax
	movslq	8(%rbx), %rcx
	movl	$9, (%rax,%rcx,4)
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%rbx)
	movl	$0, 4(%rax,%rcx,4)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB7_4:
.Ltmp75:
	movq	%rax, %r14
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_6
# BB#5:
	callq	_ZdaPv
.LBB7_6:                                # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv, .Lfunc_end7-_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp69-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp69
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp74-.Ltmp69         #   Call between .Ltmp69 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin4   #     jumps to .Ltmp75
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Lfunc_end7-.Ltmp74     #   Call between .Ltmp74 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIwE10GrowLengthEi,"axG",@progbits,_ZN11CStringBaseIwE10GrowLengthEi,comdat
	.weak	_ZN11CStringBaseIwE10GrowLengthEi
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE10GrowLengthEi,@function
_ZN11CStringBaseIwE10GrowLengthEi:      # @_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 48
.Lcfi62:
	.cfi_offset %rbx, -48
.Lcfi63:
	.cfi_offset %r12, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	8(%r14), %ebp
	movl	12(%r14), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	%esi, %eax
	jg	.LBB8_28
# BB#1:
	decl	%eax
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB8_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB8_3:                                # %select.end
	movl	%esi, %edx
	subl	%eax, %edx
	addl	%ecx, %eax
	cmpl	%esi, %eax
	cmovgel	%ecx, %edx
	leal	1(%rbx,%rdx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB8_28
# BB#4:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB8_27
# BB#5:                                 # %.preheader.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB8_25
# BB#6:                                 # %.lr.ph.i
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB8_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB8_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB8_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB8_17
.LBB8_7:
	xorl	%ecx, %ecx
.LBB8_8:                                # %scalar.ph.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB8_11
# BB#9:                                 # %scalar.ph.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB8_10:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rbp
	jne	.LBB8_10
.LBB8_11:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB8_26
# BB#12:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB8_13:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB8_13
	jmp	.LBB8_26
.LBB8_25:                               # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB8_27
.LBB8_26:                               # %._crit_edge.thread.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB8_27:                               # %._crit_edge16.i
	movq	%r12, (%r14)
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
.LBB8_28:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_17:                               # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB8_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB8_20
	jmp	.LBB8_21
.LBB8_18:
	xorl	%ebx, %ebx
.LBB8_21:                               # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB8_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB8_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB8_23
.LBB8_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB8_8
	jmp	.LBB8_26
.Lfunc_end8:
	.size	_ZN11CStringBaseIwE10GrowLengthEi, .Lfunc_end8-_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_endproc

	.type	_ZTVN8NWindows5NFile3NIO7CInFileE,@object # @_ZTVN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTVN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTVN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTVN8NWindows5NFile3NIO7CInFileE
	.p2align	3
_ZTVN8NWindows5NFile3NIO7CInFileE:
	.quad	0
	.quad	_ZTIN8NWindows5NFile3NIO7CInFileE
	.quad	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.quad	_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.quad	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.size	_ZTVN8NWindows5NFile3NIO7CInFileE, 40

	.type	_ZTSN8NWindows5NFile3NIO7CInFileE,@object # @_ZTSN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTSN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTSN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTSN8NWindows5NFile3NIO7CInFileE
	.p2align	4
_ZTSN8NWindows5NFile3NIO7CInFileE:
	.asciz	"N8NWindows5NFile3NIO7CInFileE"
	.size	_ZTSN8NWindows5NFile3NIO7CInFileE, 30

	.type	_ZTIN8NWindows5NFile3NIO7CInFileE,@object # @_ZTIN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTIN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTIN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTIN8NWindows5NFile3NIO7CInFileE
	.p2align	4
_ZTIN8NWindows5NFile3NIO7CInFileE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows5NFile3NIO7CInFileE
	.quad	_ZTIN8NWindows5NFile3NIO9CFileBaseE
	.size	_ZTIN8NWindows5NFile3NIO7CInFileE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
