	.text
	.file	"play.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	reset
	.p2align	4, 0x90
	.type	reset,@function
reset:                                  # @reset
	.cfi_startproc
# BB#0:                                 # %.preheader.preheader
	movl	$0, plycnt(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, dias+60(%rip)
	movaps	%xmm0, dias+48(%rip)
	movaps	%xmm0, dias+32(%rip)
	movaps	%xmm0, dias+16(%rip)
	movaps	%xmm0, dias(%rip)
	movaps	%xmm0, rows+16(%rip)
	movaps	%xmm0, rows(%rip)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1,1,1,1]
	movaps	%xmm0, columns(%rip)
	movaps	%xmm0, height(%rip)
	movaps	%xmm0, columns+16(%rip)
	movaps	%xmm0, height+16(%rip)
	retq
.Lfunc_end0:
	.size	reset, .Lfunc_end0-reset
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	play_init
	.p2align	4, 0x90
	.type	play_init,@function
play_init:                              # @play_init
	.cfi_startproc
# BB#0:                                 # %.preheader.preheader11
	movl	$1, colthr+32(%rip)
	movl	$2, colthr+60(%rip)
	movl	$1, colthr+64(%rip)
	movl	$2, colthr+92(%rip)
	movl	$1, colthr+96(%rip)
	movl	$2, colthr+124(%rip)
	movl	$1, colthr+128(%rip)
	movl	$2, colthr+156(%rip)
	movl	$1, colthr+160(%rip)
	movl	$2, colthr+188(%rip)
	movl	$1, colthr+192(%rip)
	movl	$2, colthr+220(%rip)
	movl	$1, colthr+224(%rip)
	movl	$2, colthr+252(%rip)
	movl	$1, colthr+256(%rip)
	movl	$2, colthr+284(%rip)
	movl	$1, colthr+288(%rip)
	movl	$2, colthr+316(%rip)
	movl	$1, colthr+320(%rip)
	movl	$2, colthr+348(%rip)
	movl	$1, colthr+352(%rip)
	movl	$2, colthr+380(%rip)
	movl	$1, colthr+384(%rip)
	movl	$2, colthr+412(%rip)
	movl	$1, colthr+416(%rip)
	movl	$2, colthr+444(%rip)
	movl	$1, colthr+448(%rip)
	movl	$2, colthr+476(%rip)
	movl	$1, colthr+480(%rip)
	movl	$2, colthr+508(%rip)
	movl	$1, colwon+124(%rip)
	movl	$1, colwon+64(%rip)
	movl	$1, colwon+188(%rip)
	movl	$1, colwon+128(%rip)
	movl	$1, colwon+252(%rip)
	movl	$1, colwon+192(%rip)
	movl	$1, colwon+316(%rip)
	movl	$1, colwon+256(%rip)
	movl	$1, colwon+380(%rip)
	movl	$1, colwon+320(%rip)
	movl	$1, colwon+444(%rip)
	movl	$1, colwon+384(%rip)
	movl	$1, colwon+508(%rip)
	movl	$1, colwon+448(%rip)
	movl	$0, plycnt(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, dias+60(%rip)
	movaps	%xmm0, dias+48(%rip)
	movaps	%xmm0, dias+32(%rip)
	movaps	%xmm0, dias+16(%rip)
	movaps	%xmm0, dias(%rip)
	movaps	%xmm0, rows+16(%rip)
	movaps	%xmm0, rows(%rip)
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [1,1,1,1]
	movaps	%xmm0, columns(%rip)
	movaps	%xmm0, height(%rip)
	movaps	%xmm0, columns+16(%rip)
	movaps	%xmm0, height+16(%rip)
	retq
.Lfunc_end1:
	.size	play_init, .Lfunc_end1-play_init
	.cfi_endproc

	.globl	printMoves
	.p2align	4, 0x90
	.type	printMoves,@function
printMoves:                             # @printMoves
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	cmpl	$0, plycnt(%rip)
	jle	.LBB2_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	moves+4(,%rbx,4), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movslq	plycnt(%rip), %rax
	incq	%rbx
	cmpq	%rax, %rbx
	jl	.LBB2_2
.LBB2_3:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end2:
	.size	printMoves, .Lfunc_end2-printMoves
	.cfi_endproc

	.globl	wins
	.p2align	4, 0x90
	.type	wins,@function
wins:                                   # @wins
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	%edi, %ecx
	addb	%cl, %cl
	shll	%cl, %edx
	movslq	%esi, %rax
	movl	rows(,%rax,4), %eax
	orl	%edx, %eax
	leal	(,%rax,4), %ecx
	andl	%eax, %ecx
	movl	%ecx, %r8d
	shll	$4, %r8d
	movl	$1, %eax
	testl	%ecx, %r8d
	jne	.LBB3_3
# BB#1:
	leal	5(%rdi,%rsi), %ecx
	movslq	%ecx, %rcx
	movl	dias(,%rcx,4), %r8d
	orl	%edx, %r8d
	leal	(,%r8,4), %r9d
	andl	%r8d, %r9d
	movl	%r9d, %ecx
	shll	$4, %ecx
	testl	%r9d, %ecx
	jne	.LBB3_3
# BB#2:
	addl	$5, %edi
	subl	%esi, %edi
	movslq	%edi, %rax
	orl	dias(,%rax,4), %edx
	leal	(,%rdx,4), %ecx
	andl	%edx, %ecx
	movl	%ecx, %edx
	shll	$4, %edx
	xorl	%eax, %eax
	testl	%ecx, %edx
	setne	%al
.LBB3_3:
	retq
.Lfunc_end3:
	.size	wins, .Lfunc_end3-wins
	.cfi_endproc

	.globl	backmove
	.p2align	4, 0x90
	.type	backmove,@function
backmove:                               # @backmove
	.cfi_startproc
# BB#0:
	movslq	plycnt(%rip), %rax
	leal	-1(%rax), %ecx
	movslq	moves(,%rax,4), %rdx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movl	%ecx, plycnt(%rip)
	movslq	height(,%rdx,4), %rsi
	leaq	-1(%rsi), %rdi
	movl	%edi, height(,%rdx,4)
	sarl	columns(,%rdx,4)
	leal	(%rax,%rdx,2), %ecx
	movl	$-2, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	roll	%cl, %eax
	andl	%eax, rows-4(,%rsi,4)
	leal	5(%rdx), %ecx
	leal	5(%rdi,%rdx), %edx
	movslq	%edx, %rdx
	andl	%eax, dias(,%rdx,4)
	subl	%edi, %ecx
	movslq	%ecx, %rcx
	andl	%eax, dias(,%rcx,4)
	retq
.Lfunc_end4:
	.size	backmove, .Lfunc_end4-backmove
	.cfi_endproc

	.globl	makemove
	.p2align	4, 0x90
	.type	makemove,@function
makemove:                               # @makemove
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movslq	plycnt(%rip), %rax
	leaq	1(%rax), %rcx
	movl	%ecx, plycnt(%rip)
	movl	%edi, moves+4(,%rax,4)
	andl	$1, %ecx
	movslq	%edi, %rax
	movslq	height(,%rax,4), %rdx
	leal	1(%rdx), %esi
	movl	%esi, height(,%rax,4)
	movl	columns(,%rax,4), %esi
	leal	(%rcx,%rsi,2), %esi
	movl	%esi, columns(,%rax,4)
	leal	(%rcx,%rdi,2), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	orl	%esi, rows(,%rdx,4)
	addl	$5, %eax
	leal	5(%rdx,%rdi), %ecx
	movslq	%ecx, %rcx
	orl	%esi, dias(,%rcx,4)
	subl	%edx, %eax
	cltq
	orl	%esi, dias(,%rax,4)
	retq
.Lfunc_end5:
	.size	makemove, .Lfunc_end5-makemove
	.cfi_endproc

	.type	plycnt,@object          # @plycnt
	.comm	plycnt,4,4
	.type	dias,@object            # @dias
	.comm	dias,76,16
	.type	columns,@object         # @columns
	.comm	columns,512,16
	.type	height,@object          # @height
	.comm	height,512,16
	.type	rows,@object            # @rows
	.comm	rows,32,16
	.type	colthr,@object          # @colthr
	.comm	colthr,512,16
	.type	colwon,@object          # @colwon
	.comm	colwon,512,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d"
	.size	.L.str, 3

	.type	moves,@object           # @moves
	.comm	moves,176,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
