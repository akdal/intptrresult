	.text
	.file	"sgesl.bc"
	.globl	sgesl
	.p2align	4, 0x90
	.type	sgesl,@function
sgesl:                                  # @sgesl
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movl	(%rdi), %r13d
	movslq	%r13d, %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	testl	%ecx, %ecx
	je	.LBB0_25
# BB#1:                                 # %.preheader148
	testl	%r13d, %r13d
	jle	.LBB0_12
# BB#2:                                 # %.lr.ph185.preheader
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph185
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
                                        #     Child Loop BB0_10 Depth 2
	movq	8(%rdi,%r10,8), %r9
	testq	%r10, %r10
	jle	.LBB0_7
# BB#4:                                 # %.lr.ph181.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	leal	-1(%r10), %r11d
	testb	$3, %r10b
	je	.LBB0_8
# BB#5:                                 # %.lr.ph181.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%r8d, %ecx
	andl	$3, %ecx
	xorps	%xmm0, %xmm0
	xorl	%ebx, %ebx
	movq	%rdx, %rbp
	movq	%r9, %rax
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph181.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rbp), %xmm1
	addss	%xmm1, %xmm0
	incl	%ebx
	addq	$4, %rax
	addq	$4, %rbp
	cmpl	%ebx, %ecx
	jne	.LBB0_6
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_3 Depth=1
	xorps	%xmm0, %xmm0
	jmp	.LBB0_11
	.p2align	4, 0x90
.LBB0_8:                                #   in Loop: Header=BB0_3 Depth=1
	xorps	%xmm0, %xmm0
	xorl	%ebx, %ebx
	movq	%rdx, %rbp
	movq	%r9, %rax
.LBB0_9:                                # %.lr.ph181.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	$3, %r11d
	jb	.LBB0_11
	.p2align	4, 0x90
.LBB0_10:                               # %.lr.ph181
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	(%rbp), %xmm1
	addss	%xmm0, %xmm1
	mulss	4(%rbp), %xmm2
	addss	%xmm1, %xmm2
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	8(%rbp), %xmm1
	addss	%xmm2, %xmm1
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	12(%rbp), %xmm0
	addss	%xmm1, %xmm0
	addl	$4, %ebx
	addq	$16, %rax
	addq	$16, %rbp
	cmpl	%ebx, %r8d
	jne	.LBB0_10
.LBB0_11:                               # %._crit_edge182
                                        #   in Loop: Header=BB0_3 Depth=1
	movss	(%rdx,%r10,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	divss	(%r9,%r10,4), %xmm1
	movss	%xmm1, (%rdx,%r10,4)
	incq	%r10
	incl	%r8d
	cmpq	%r13, %r10
	jne	.LBB0_3
.LBB0_12:                               # %._crit_edge186
	movq	-64(%rsp), %rax         # 8-byte Reload
	leaq	-2(%rax), %r8
	testl	%r8d, %r8d
	js	.LBB0_61
# BB#13:                                # %.lr.ph175.preheader
	leaq	(%rsi,%r8,4), %r15
	xorl	%r9d, %r9d
	movb	$1, %r10b
	movq	%rdx, %r14
	xorl	%r12d, %r12d
	leaq	(,%r8,4), %rbx
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph175
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_17 Depth 2
                                        #     Child Loop BB0_21 Depth 2
	leaq	(%rdx,%r8,4), %r11
	leaq	1(%r8), %rsi
	xorps	%xmm0, %xmm0
	cmpq	-64(%rsp), %rsi         # 8-byte Folded Reload
	jge	.LBB0_22
# BB#15:                                # %.lr.ph170.preheader
                                        #   in Loop: Header=BB0_14 Depth=1
	leal	1(%r12), %eax
	movq	8(%rdi,%r8,8), %rcx
	testb	$3, %al
	je	.LBB0_19
# BB#16:                                # %.lr.ph170.prol.preheader
                                        #   in Loop: Header=BB0_14 Depth=1
	movl	%r10d, %eax
	andb	$3, %al
	movzbl	%al, %eax
	negl	%eax
	leaq	(%rcx,%r9,4), %rbp
	xorps	%xmm0, %xmm0
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph170.prol
                                        #   Parent Loop BB0_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	4(%rbx,%rbp), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	4(%rbx,%rcx), %xmm1
	addss	%xmm1, %xmm0
	incl	%esi
	addq	$4, %rbp
	addq	$4, %rcx
	incl	%eax
	jne	.LBB0_17
# BB#18:                                # %.lr.ph170.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB0_14 Depth=1
	addq	%rbx, %rbp
	addq	%rbx, %rcx
	cmpl	$3, %r12d
	jae	.LBB0_20
	jmp	.LBB0_22
.LBB0_19:                               #   in Loop: Header=BB0_14 Depth=1
	leaq	(%rcx,%r8,4), %rbp
	xorps	%xmm0, %xmm0
	movq	%r11, %rcx
	cmpl	$3, %r12d
	jb	.LBB0_22
.LBB0_20:                               # %.lr.ph170.preheader.new
                                        #   in Loop: Header=BB0_14 Depth=1
	movl	%r13d, %eax
	subl	%esi, %eax
	addq	$16, %rbp
	addq	$16, %rcx
	.p2align	4, 0x90
.LBB0_21:                               # %.lr.ph170
                                        #   Parent Loop BB0_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-12(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	-8(%rbp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	-12(%rcx), %xmm1
	addss	%xmm0, %xmm1
	mulss	-8(%rcx), %xmm2
	addss	%xmm1, %xmm2
	movss	-4(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	-4(%rcx), %xmm1
	addss	%xmm2, %xmm1
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	%xmm1, %xmm0
	addq	$16, %rbp
	addq	$16, %rcx
	addl	$-4, %eax
	jne	.LBB0_21
.LBB0_22:                               # %._crit_edge171
                                        #   in Loop: Header=BB0_14 Depth=1
	addss	(%r11), %xmm0
	movss	%xmm0, (%r11)
	movslq	(%r15), %rax
	cmpl	%r8d, %eax
	je	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_14 Depth=1
	movl	(%rdx,%rax,4), %ecx
	movss	%xmm0, (%rdx,%rax,4)
	movl	%ecx, (%r11)
.LBB0_24:                               #   in Loop: Header=BB0_14 Depth=1
	addq	$-4, %r15
	incl	%r12d
	decq	%r9
	addq	$-4, %r14
	incb	%r10b
	testq	%r8, %r8
	leaq	-1(%r8), %r8
	jg	.LBB0_14
	jmp	.LBB0_61
.LBB0_25:                               # %.preheader146
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	leal	-1(%r13), %eax
	movl	%eax, -68(%rsp)         # 4-byte Spill
	movq	%r13, %rax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	cmpl	$2, %r13d
	jl	.LBB0_51
# BB#26:                                # %.lr.ph161.preheader
	movl	-68(%rsp), %r8d         # 4-byte Reload
	movq	-80(%rsp), %rax         # 8-byte Reload
	leaq	-1(%rax), %r14
	leaq	(%rdx,%rax,4), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	leaq	16(%rdx), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	leaq	12(%rdx), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movl	$1, %r11d
	xorl	%r13d, %r13d
	movl	$9, %r15d
	movl	$13, %eax
	movq	%r14, -56(%rsp)         # 8-byte Spill
	movq	%r8, -24(%rsp)          # 8-byte Spill
	jmp	.LBB0_27
.LBB0_37:                               # %vector.ph
                                        #   in Loop: Header=BB0_27 Depth=1
	leaq	-8(%r12), %rbp
	shrq	$3, %rbp
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	testb	$1, %bpl
	jne	.LBB0_39
# BB#38:                                # %vector.body.prol
                                        #   in Loop: Header=BB0_27 Depth=1
	movups	4(%r9), %xmm2
	movups	20(%r9), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	(%rdx,%r11,4), %xmm4
	movups	16(%rdx,%r11,4), %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, (%rdx,%r11,4)
	movups	%xmm5, 16(%rdx,%r11,4)
	movl	$8, %ecx
	testq	%rbp, %rbp
	jne	.LBB0_40
	jmp	.LBB0_42
.LBB0_39:                               #   in Loop: Header=BB0_27 Depth=1
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	je	.LBB0_42
.LBB0_40:                               # %vector.ph.new
                                        #   in Loop: Header=BB0_27 Depth=1
	movq	%r14, %rbx
	andq	$-8, %rbx
	subq	%rcx, %rbx
	leaq	(%rcx,%r15), %rbp
	movq	%rdi, -32(%rsp)         # 8-byte Spill
	movq	-40(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rbp,4), %r8
	movq	-32(%rsp), %rdi         # 8-byte Reload
	addq	%rax, %rcx
	leaq	(%r10,%rcx,4), %rbp
	.p2align	4, 0x90
.LBB0_41:                               # %vector.body
                                        #   Parent Loop BB0_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rbp), %xmm2
	movups	-32(%rbp), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	-48(%r8), %xmm4
	movups	-32(%r8), %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, -48(%r8)
	movups	%xmm5, -32(%r8)
	movups	-16(%rbp), %xmm2
	movups	(%rbp), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	-16(%r8), %xmm4
	movups	(%r8), %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, -16(%r8)
	movups	%xmm5, (%r8)
	addq	$64, %r8
	addq	$64, %rbp
	addq	$-16, %rbx
	jne	.LBB0_41
.LBB0_42:                               # %middle.block
                                        #   in Loop: Header=BB0_27 Depth=1
	cmpq	%rdi, %r12
	movq	-24(%rsp), %r8          # 8-byte Reload
	je	.LBB0_50
# BB#43:                                #   in Loop: Header=BB0_27 Depth=1
	leaq	(%r11,%rdi), %rbx
	leaq	(%r9,%rdi,4), %r9
	jmp	.LBB0_44
	.p2align	4, 0x90
.LBB0_27:                               # %.lr.ph161
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_41 Depth 2
                                        #     Child Loop BB0_46 Depth 2
                                        #     Child Loop BB0_49 Depth 2
	movq	%r13, %rbp
	leaq	1(%rbp), %r13
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movq	8(%rcx,%rbp,8), %r10
	movl	(%rsi), %edi
	movslq	%edi, %rcx
	movss	(%rdx,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cmpq	%rbp, %rdi
	je	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_27 Depth=1
	movl	(%rdx,%rbp,4), %edi
	movl	%edi, (%rdx,%rcx,4)
	movss	%xmm0, (%rdx,%rbp,4)
.LBB0_29:                               #   in Loop: Header=BB0_27 Depth=1
	cmpq	-64(%rsp), %r13         # 8-byte Folded Reload
	jge	.LBB0_50
# BB#30:                                # %.lr.ph157.preheader
                                        #   in Loop: Header=BB0_27 Depth=1
	movq	-56(%rsp), %r12         # 8-byte Reload
	subq	%rbp, %r12
	leaq	(%r10,%rbp,4), %r9
	cmpq	$8, %r12
	jae	.LBB0_32
# BB#31:                                #   in Loop: Header=BB0_27 Depth=1
	movq	%r11, %rbx
	jmp	.LBB0_44
	.p2align	4, 0x90
.LBB0_32:                               # %min.iters.checked
                                        #   in Loop: Header=BB0_27 Depth=1
	movq	%r12, %rdi
	andq	$-8, %rdi
	je	.LBB0_36
# BB#33:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_27 Depth=1
	leaq	4(%rdx,%rbp,4), %rcx
	movq	-80(%rsp), %rbp         # 8-byte Reload
	leaq	(%r10,%rbp,4), %rbp
	cmpq	%rbp, %rcx
	jae	.LBB0_37
# BB#34:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_27 Depth=1
	leaq	(%r10,%r13,4), %rcx
	cmpq	-16(%rsp), %rcx         # 8-byte Folded Reload
	jae	.LBB0_37
.LBB0_36:                               #   in Loop: Header=BB0_27 Depth=1
	movq	%r11, %rbx
.LBB0_44:                               # %.lr.ph157.preheader239
                                        #   in Loop: Header=BB0_27 Depth=1
	movq	-80(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	subl	%ebx, %ecx
	movq	-56(%rsp), %rbp         # 8-byte Reload
	subq	%rbx, %rbp
	andq	$3, %rcx
	je	.LBB0_47
# BB#45:                                # %.lr.ph157.prol.preheader
                                        #   in Loop: Header=BB0_27 Depth=1
	negq	%rcx
	.p2align	4, 0x90
.LBB0_46:                               # %.lr.ph157.prol
                                        #   Parent Loop BB0_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	4(%r9), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addq	$4, %r9
	addss	(%rdx,%rbx,4), %xmm1
	movss	%xmm1, (%rdx,%rbx,4)
	incq	%rbx
	incq	%rcx
	jne	.LBB0_46
.LBB0_47:                               # %.lr.ph157.prol.loopexit
                                        #   in Loop: Header=BB0_27 Depth=1
	cmpq	$3, %rbp
	jb	.LBB0_50
# BB#48:                                # %.lr.ph157.preheader239.new
                                        #   in Loop: Header=BB0_27 Depth=1
	movq	-80(%rsp), %rbp         # 8-byte Reload
	subq	%rbx, %rbp
	movq	-8(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rbx,4), %rbx
	addq	$16, %r9
	.p2align	4, 0x90
.LBB0_49:                               # %.lr.ph157
                                        #   Parent Loop BB0_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-12(%r9), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	-12(%rbx), %xmm1
	movss	%xmm1, -12(%rbx)
	movss	-8(%r9), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	-8(%rbx), %xmm1
	movss	%xmm1, -8(%rbx)
	movss	-4(%r9), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	-4(%rbx), %xmm1
	movss	%xmm1, -4(%rbx)
	movss	(%r9), %xmm1            # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rbx), %xmm1
	movss	%xmm1, (%rbx)
	addq	$16, %rbx
	addq	$16, %r9
	addq	$-4, %rbp
	jne	.LBB0_49
.LBB0_50:                               # %._crit_edge158
                                        #   in Loop: Header=BB0_27 Depth=1
	addq	$4, %rsi
	incq	%r11
	decq	%r14
	incq	%r15
	incq	%rax
	cmpq	%r8, %r13
	jne	.LBB0_27
.LBB0_51:                               # %.preheader
	movq	-80(%rsp), %rcx         # 8-byte Reload
	testl	%ecx, %ecx
	movq	-48(%rsp), %r11         # 8-byte Reload
	jle	.LBB0_61
# BB#52:                                # %.lr.ph152.preheader
	movl	-68(%rsp), %eax         # 4-byte Reload
	movslq	%ecx, %r10
	xorl	%ecx, %ecx
	leaq	1(%rax), %r9
	leaq	-2(%rax), %r8
	.p2align	4, 0x90
.LBB0_53:                               # %.lr.ph152
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_59 Depth 2
	movq	%r10, %rsi
	leaq	-1(%rsi), %r10
	movq	(%r11,%rsi,8), %rbp
	movss	-4(%rdx,%rsi,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	-4(%rbp,%rsi,4), %xmm0
	movss	%xmm0, -4(%rdx,%rsi,4)
	testq	%r10, %r10
	jle	.LBB0_61
# BB#54:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_53 Depth=1
	mulss	(%rbp), %xmm0
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, (%rdx)
	cmpq	$1, %rax
	je	.LBB0_60
# BB#55:                                # %.lr.ph..lr.ph_crit_edge.preheader
                                        #   in Loop: Header=BB0_53 Depth=1
	movq	%r9, %rdi
	subq	%rcx, %rdi
	testb	$1, %dil
	jne	.LBB0_57
# BB#56:                                #   in Loop: Header=BB0_53 Depth=1
	movl	$1, %ebx
	cmpq	%rcx, %r8
	jne	.LBB0_58
	jmp	.LBB0_60
	.p2align	4, 0x90
.LBB0_57:                               # %.lr.ph..lr.ph_crit_edge.prol
                                        #   in Loop: Header=BB0_53 Depth=1
	movss	4(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rbp
	mulss	-4(%rdx,%rsi,4), %xmm0
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 4(%rdx)
	movl	$2, %ebx
	cmpq	%rcx, %r8
	je	.LBB0_60
.LBB0_58:                               # %.lr.ph..lr.ph_crit_edge.preheader.new
                                        #   in Loop: Header=BB0_53 Depth=1
	addq	$8, %rbp
	.p2align	4, 0x90
.LBB0_59:                               # %.lr.ph..lr.ph_crit_edge
                                        #   Parent Loop BB0_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-4(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	-4(%rdx,%rsi,4), %xmm0
	movss	(%rdx,%rbx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, (%rdx,%rbx,4)
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	-4(%rdx,%rsi,4), %xmm0
	movss	4(%rdx,%rbx,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 4(%rdx,%rbx,4)
	addq	$2, %rbx
	addq	$8, %rbp
	cmpq	%rbx, %rax
	jne	.LBB0_59
.LBB0_60:                               # %._crit_edge
                                        #   in Loop: Header=BB0_53 Depth=1
	decq	%rax
	incq	%rcx
	testq	%r10, %r10
	jg	.LBB0_53
.LBB0_61:                               # %.loopexit
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	sgesl, .Lfunc_end0-sgesl
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
