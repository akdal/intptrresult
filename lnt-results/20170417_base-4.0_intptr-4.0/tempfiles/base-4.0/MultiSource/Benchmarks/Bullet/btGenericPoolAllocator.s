	.text
	.file	"btGenericPoolAllocator.bc"
	.globl	_ZN19btGenericMemoryPool24allocate_from_free_nodesEm
	.p2align	4, 0x90
	.type	_ZN19btGenericMemoryPool24allocate_from_free_nodesEm,@function
_ZN19btGenericMemoryPool24allocate_from_free_nodesEm: # @_ZN19btGenericMemoryPool24allocate_from_free_nodesEm
	.cfi_startproc
# BB#0:
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	32(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB0_8
# BB#1:                                 # %.preheader
	movq	8(%rdi), %r8
	movq	16(%rdi), %r9
	decq	%rcx
	movl	$4294967295, %r10d      # imm = 0xFFFFFFFF
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rcx,8), %rdx
	cmpq	%rsi, (%r9,%rdx,8)
	movq	%rcx, %rdx
	cmovbq	%rax, %rdx
	cmpq	%r10, %rdx
	jne	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	testq	%rcx, %rcx
	leaq	-1(%rcx), %rcx
	jne	.LBB0_2
.LBB0_4:                                # %._crit_edge
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	cmpq	%rcx, %rdx
	je	.LBB0_8
# BB#5:
	movq	(%r8,%rdx,8), %rax
	movq	(%r9,%rax,8), %rcx
	subq	%rsi, %rcx
	movq	%rsi, (%r9,%rax,8)
	je	.LBB0_7
# BB#6:
	addq	%rax, %rsi
	movq	%rsi, (%r8,%rdx,8)
	movq	%rcx, (%r9,%rsi,8)
	retq
.LBB0_7:
	movq	32(%rdi), %rcx
	movq	-8(%r8,%rcx,8), %rcx
	movq	%rcx, (%r8,%rdx,8)
	decq	32(%rdi)
.LBB0_8:
	retq
.Lfunc_end0:
	.size	_ZN19btGenericMemoryPool24allocate_from_free_nodesEm, .Lfunc_end0-_ZN19btGenericMemoryPool24allocate_from_free_nodesEm
	.cfi_endproc

	.globl	_ZN19btGenericMemoryPool18allocate_from_poolEm
	.p2align	4, 0x90
	.type	_ZN19btGenericMemoryPool18allocate_from_poolEm,@function
_ZN19btGenericMemoryPool18allocate_from_poolEm: # @_ZN19btGenericMemoryPool18allocate_from_poolEm
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	leaq	(%rax,%rsi), %rcx
	cmpq	48(%rdi), %rcx
	jbe	.LBB1_2
# BB#1:
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	retq
.LBB1_2:
	movq	16(%rdi), %rcx
	movq	%rsi, (%rcx,%rax,8)
	addq	%rsi, 24(%rdi)
	retq
.Lfunc_end1:
	.size	_ZN19btGenericMemoryPool18allocate_from_poolEm, .Lfunc_end1-_ZN19btGenericMemoryPool18allocate_from_poolEm
	.cfi_endproc

	.globl	_ZN19btGenericMemoryPool9init_poolEmm
	.p2align	4, 0x90
	.type	_ZN19btGenericMemoryPool9init_poolEmm,@function
_ZN19btGenericMemoryPool9init_poolEmm:  # @_ZN19btGenericMemoryPool9init_poolEmm
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	%rsi, 40(%rbx)
	movq	%rdx, 48(%rbx)
	imulq	%rsi, %rdx
	movl	$16, %esi
	movq	%rdx, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, (%rbx)
	movq	48(%rbx), %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, 8(%rbx)
	movq	48(%rbx), %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, 16(%rbx)
	cmpq	$0, 48(%rbx)
	je	.LBB2_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, (%rax,%rcx,8)
	incq	%rcx
	cmpq	48(%rbx), %rcx
	jb	.LBB2_2
.LBB2_3:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZN19btGenericMemoryPool9init_poolEmm, .Lfunc_end2-_ZN19btGenericMemoryPool9init_poolEmm
	.cfi_endproc

	.globl	_ZN19btGenericMemoryPool8end_poolEv
	.p2align	4, 0x90
	.type	_ZN19btGenericMemoryPool8end_poolEv,@function
_ZN19btGenericMemoryPool8end_poolEv:    # @_ZN19btGenericMemoryPool8end_poolEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	8(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	16(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	popq	%rbx
	retq
.Lfunc_end3:
	.size	_ZN19btGenericMemoryPool8end_poolEv, .Lfunc_end3-_ZN19btGenericMemoryPool8end_poolEv
	.cfi_endproc

	.globl	_ZN19btGenericMemoryPool8allocateEm
	.p2align	4, 0x90
	.type	_ZN19btGenericMemoryPool8allocateEm,@function
_ZN19btGenericMemoryPool8allocateEm:    # @_ZN19btGenericMemoryPool8allocateEm
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %r8
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	40(%rdi)
	cmpq	$1, %rdx
	sbbq	$-1, %rax
	testq	%r8, %r8
	je	.LBB4_9
# BB#1:                                 # %.preheader.i
	movq	8(%rdi), %r9
	movq	16(%rdi), %rsi
	decq	%r8
	movl	$4294967295, %edx       # imm = 0xFFFFFFFF
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r9,%r8,8), %rcx
	cmpq	%rax, (%rsi,%rcx,8)
	movq	%r8, %rcx
	cmovbq	%rdx, %rcx
	testq	%r8, %r8
	je	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	decq	%r8
	cmpq	%rdx, %rcx
	je	.LBB4_2
.LBB4_4:                                # %._crit_edge.i
	movl	$4294967295, %edx       # imm = 0xFFFFFFFF
	cmpq	%rdx, %rcx
	je	.LBB4_9
# BB#5:
	movq	(%r9,%rcx,8), %r8
	movq	(%rsi,%r8,8), %rdx
	subq	%rax, %rdx
	movq	%rax, (%rsi,%r8,8)
	je	.LBB4_7
# BB#6:
	leaq	(%r8,%rax), %r10
	movq	%r10, (%r9,%rcx,8)
	movq	%rdx, (%rsi,%r10,8)
	jmp	.LBB4_8
.LBB4_7:
	movq	32(%rdi), %rdx
	movq	-8(%r9,%rdx,8), %rdx
	movq	%rdx, (%r9,%rcx,8)
	decq	32(%rdi)
.LBB4_8:                                # %_ZN19btGenericMemoryPool24allocate_from_free_nodesEm.exit
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	cmpq	%rcx, %r8
	jne	.LBB4_13
.LBB4_9:                                # %_ZN19btGenericMemoryPool24allocate_from_free_nodesEm.exit.thread
	movq	24(%rdi), %r8
	leaq	(%r8,%rax), %rcx
	cmpq	48(%rdi), %rcx
	jbe	.LBB4_11
# BB#10:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	retq
.LBB4_11:                               # %_ZN19btGenericMemoryPool18allocate_from_poolEm.exit
	movq	16(%rdi), %rcx
	movq	%rax, (%rcx,%r8,8)
	addq	%rax, 24(%rdi)
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	cmpq	%rax, %r8
	jne	.LBB4_13
# BB#12:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	retq
.LBB4_13:                               # %.sink.split
	imulq	40(%rdi), %r8
	addq	(%rdi), %r8
	movq	%r8, %rax
	retq
.Lfunc_end4:
	.size	_ZN19btGenericMemoryPool8allocateEm, .Lfunc_end4-_ZN19btGenericMemoryPool8allocateEm
	.cfi_endproc

	.globl	_ZN19btGenericMemoryPool10freeMemoryEPv
	.p2align	4, 0x90
	.type	_ZN19btGenericMemoryPool10freeMemoryEPv,@function
_ZN19btGenericMemoryPool10freeMemoryEPv: # @_ZN19btGenericMemoryPool10freeMemoryEPv
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	cmpq	%rsi, %rax
	jbe	.LBB5_2
# BB#1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB5_2:
	subq	%rax, %rsi
	movq	40(%rdi), %rcx
	movq	48(%rdi), %rax
	imulq	%rcx, %rax
	cmpq	%rax, %rsi
	jae	.LBB5_3
# BB#4:
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	movq	8(%rdi), %rcx
	movq	32(%rdi), %rdx
	movq	%rax, (%rcx,%rdx,8)
	incq	32(%rdi)
	movb	$1, %al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB5_3:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end5:
	.size	_ZN19btGenericMemoryPool10freeMemoryEPv, .Lfunc_end5-_ZN19btGenericMemoryPool10freeMemoryEPv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.zero	16
	.text
	.globl	_ZN22btGenericPoolAllocatorD0Ev
	.p2align	4, 0x90
	.type	_ZN22btGenericPoolAllocatorD0Ev,@function
_ZN22btGenericPoolAllocatorD0Ev:        # @_ZN22btGenericPoolAllocatorD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 32
.Lcfi7:
	.cfi_offset %rbx, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	$_ZTV22btGenericPoolAllocator+16, (%r15)
	cmpq	$0, 152(%r15)
	je	.LBB6_7
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%r15,%r14,8), %rbx
	movq	(%rbx), %rdi
.Ltmp0:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp1:
# BB#3:                                 # %.noexc
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	8(%rbx), %rdi
.Ltmp2:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp3:
# BB#4:                                 # %.noexc2
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	16(%rbx), %rdi
.Ltmp4:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp5:
# BB#5:                                 # %.noexc3
                                        #   in Loop: Header=BB6_2 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	24(%r15,%r14,8), %rdi
.Ltmp6:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp7:
# BB#6:                                 # %.noexc4
                                        #   in Loop: Header=BB6_2 Depth=1
	incq	%r14
	cmpq	152(%r15), %r14
	jb	.LBB6_2
.LBB6_7:                                # %_ZN22btGenericPoolAllocatorD2Ev.exit
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdlPv                  # TAILCALL
.LBB6_8:
.Ltmp8:
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN22btGenericPoolAllocatorD0Ev, .Lfunc_end6-_ZN22btGenericPoolAllocatorD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp0           #   Call between .Ltmp0 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end6-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN22btGenericPoolAllocator13push_new_poolEv
	.p2align	4, 0x90
	.type	_ZN22btGenericPoolAllocator13push_new_poolEv,@function
_ZN22btGenericPoolAllocator13push_new_poolEv: # @_ZN22btGenericPoolAllocator13push_new_poolEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpq	$15, 152(%r15)
	jbe	.LBB7_2
# BB#1:
	xorl	%r14d, %r14d
	jmp	.LBB7_6
.LBB7_2:
	movl	$56, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r14
	movq	152(%r15), %rax
	movq	%r14, 24(%r15,%rax,8)
	movq	152(%r15), %rax
	movq	24(%r15,%rax,8), %rbx
	movq	8(%r15), %rax
	movq	16(%r15), %rdi
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	%rax, 40(%rbx)
	movq	%rdi, 48(%rbx)
	imulq	%rax, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, (%rbx)
	movq	48(%rbx), %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, 8(%rbx)
	movq	48(%rbx), %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, 16(%rbx)
	cmpq	$0, 48(%rbx)
	je	.LBB7_5
# BB#3:                                 # %.lr.ph.i.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_4:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, (%rax,%rcx,8)
	incq	%rcx
	cmpq	48(%rbx), %rcx
	jb	.LBB7_4
.LBB7_5:                                # %_ZN19btGenericMemoryPool9init_poolEmm.exit
	incq	152(%r15)
.LBB7_6:
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	_ZN22btGenericPoolAllocator13push_new_poolEv, .Lfunc_end7-_ZN22btGenericPoolAllocator13push_new_poolEv
	.cfi_endproc

	.globl	_ZN22btGenericPoolAllocator14failback_allocEm
	.p2align	4, 0x90
	.type	_ZN22btGenericPoolAllocator14failback_allocEm,@function
_ZN22btGenericPoolAllocator14failback_allocEm: # @_ZN22btGenericPoolAllocator14failback_allocEm
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	16(%rdi), %rax
	imulq	8(%rdi), %rax
	cmpq	%rbx, %rax
	jb	.LBB8_2
# BB#1:
	callq	_ZN22btGenericPoolAllocator13push_new_poolEv
	testq	%rax, %rax
	je	.LBB8_2
# BB#3:
	movq	%rax, %rdi
	movq	%rbx, %rsi
	popq	%rbx
	jmp	_ZN19btGenericMemoryPool8allocateEm # TAILCALL
.LBB8_2:                                # %.thread
	movl	$16, %esi
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_Z22btAlignedAllocInternalmi # TAILCALL
.Lfunc_end8:
	.size	_ZN22btGenericPoolAllocator14failback_allocEm, .Lfunc_end8-_ZN22btGenericPoolAllocator14failback_allocEm
	.cfi_endproc

	.globl	_ZN22btGenericPoolAllocator13failback_freeEPv
	.p2align	4, 0x90
	.type	_ZN22btGenericPoolAllocator13failback_freeEPv,@function
_ZN22btGenericPoolAllocator13failback_freeEPv: # @_ZN22btGenericPoolAllocator13failback_freeEPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 16
	movq	%rsi, %rdi
	callq	_Z21btAlignedFreeInternalPv
	movb	$1, %al
	popq	%rcx
	retq
.Lfunc_end9:
	.size	_ZN22btGenericPoolAllocator13failback_freeEPv, .Lfunc_end9-_ZN22btGenericPoolAllocator13failback_freeEPv
	.cfi_endproc

	.globl	_ZN22btGenericPoolAllocator8allocateEm
	.p2align	4, 0x90
	.type	_ZN22btGenericPoolAllocator8allocateEm,@function
_ZN22btGenericPoolAllocator8allocateEm: # @_ZN22btGenericPoolAllocator8allocateEm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpq	$0, 152(%r15)
	je	.LBB10_5
# BB#1:                                 # %.lr.ph.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r15,%rbx,8), %rdi
	movq	%r14, %rsi
	callq	_ZN19btGenericMemoryPool8allocateEm
	testq	%rax, %rax
	jne	.LBB10_4
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB10_2 Depth=1
	cmpq	152(%r15), %rbx
	leaq	1(%rbx), %rbx
	jb	.LBB10_2
.LBB10_4:                               # %._crit_edge
	testq	%rax, %rax
	je	.LBB10_5
# BB#8:                                 # %_ZN22btGenericPoolAllocator14failback_allocEm.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB10_5:                               # %._crit_edge.thread
	movq	16(%r15), %rax
	imulq	8(%r15), %rax
	cmpq	%r14, %rax
	jb	.LBB10_9
# BB#6:
	movq	%r15, %rdi
	callq	_ZN22btGenericPoolAllocator13push_new_poolEv
	testq	%rax, %rax
	je	.LBB10_9
# BB#7:
	movq	%rax, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN19btGenericMemoryPool8allocateEm # TAILCALL
.LBB10_9:                               # %.thread.i
	movl	$16, %esi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_Z22btAlignedAllocInternalmi # TAILCALL
.Lfunc_end10:
	.size	_ZN22btGenericPoolAllocator8allocateEm, .Lfunc_end10-_ZN22btGenericPoolAllocator8allocateEm
	.cfi_endproc

	.globl	_ZN22btGenericPoolAllocator10freeMemoryEPv
	.p2align	4, 0x90
	.type	_ZN22btGenericPoolAllocator10freeMemoryEPv,@function
_ZN22btGenericPoolAllocator10freeMemoryEPv: # @_ZN22btGenericPoolAllocator10freeMemoryEPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 16
	movq	152(%rdi), %r8
	testq	%r8, %r8
	je	.LBB11_6
# BB#1:                                 # %.lr.ph
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	movq	24(%rdi,%rdx,8), %r10
	movq	(%r10), %rcx
	cmpq	%rsi, %rcx
	ja	.LBB11_5
# BB#3:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	%rsi, %rax
	subq	%rcx, %rax
	movq	40(%r10), %r9
	movq	48(%r10), %rcx
	imulq	%r9, %rcx
	cmpq	%rcx, %rax
	jb	.LBB11_4
.LBB11_5:                               # %_ZN19btGenericMemoryPool10freeMemoryEPv.exit
                                        #   in Loop: Header=BB11_2 Depth=1
	incq	%rdx
	cmpq	%r8, %rdx
	jb	.LBB11_2
.LBB11_6:                               # %.critedge13
	movq	%rsi, %rdi
	callq	_Z21btAlignedFreeInternalPv
	jmp	.LBB11_7
.LBB11_4:                               # %.critedge.thread
	xorl	%edx, %edx
	divq	%r9
	movq	8(%r10), %rcx
	movq	32(%r10), %rdx
	movq	%rax, (%rcx,%rdx,8)
	incq	32(%r10)
.LBB11_7:
	movb	$1, %al
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZN22btGenericPoolAllocator10freeMemoryEPv, .Lfunc_end11-_ZN22btGenericPoolAllocator10freeMemoryEPv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.zero	16
	.text
	.globl	_ZN22btGenericPoolAllocatorD2Ev
	.p2align	4, 0x90
	.type	_ZN22btGenericPoolAllocatorD2Ev,@function
_ZN22btGenericPoolAllocatorD2Ev:        # @_ZN22btGenericPoolAllocatorD2Ev
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	$_ZTV22btGenericPoolAllocator+16, (%r14)
	cmpq	$0, 152(%r14)
	je	.LBB12_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%r14,%rbx,8), %r15
	movq	(%r15), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	8(%r15), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	16(%r15), %rdi
	callq	_Z21btAlignedFreeInternalPv
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%r15)
	movq	24(%r14,%rbx,8), %rdi
	callq	_Z21btAlignedFreeInternalPv
	incq	%rbx
	cmpq	152(%r14), %rbx
	jb	.LBB12_2
.LBB12_3:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	_ZN22btGenericPoolAllocatorD2Ev, .Lfunc_end12-_ZN22btGenericPoolAllocatorD2Ev
	.cfi_endproc

	.globl	_Z11btPoolAllocm
	.p2align	4, 0x90
	.type	_Z11btPoolAllocm,@function
_Z11btPoolAllocm:                       # @_Z11btPoolAllocm
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -24
.Lcfi36:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpq	$0, g_main_allocator+152(%rip)
	je	.LBB13_5
# BB#1:                                 # %.lr.ph.i.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB13_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	g_main_allocator+16(,%rbx,8), %rdi
	movq	%r14, %rsi
	callq	_ZN19btGenericMemoryPool8allocateEm
	testq	%rax, %rax
	jne	.LBB13_4
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB13_2 Depth=1
	cmpq	g_main_allocator+152(%rip), %rbx
	leaq	1(%rbx), %rbx
	jb	.LBB13_2
.LBB13_4:                               # %._crit_edge.i
	testq	%rax, %rax
	je	.LBB13_5
# BB#8:                                 # %_ZN22btGenericPoolAllocator8allocateEm.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB13_5:                               # %._crit_edge.thread.i
	movq	g_main_allocator+16(%rip), %rax
	imulq	g_main_allocator+8(%rip), %rax
	cmpq	%r14, %rax
	jb	.LBB13_9
# BB#6:
	movl	$g_main_allocator, %edi
	callq	_ZN22btGenericPoolAllocator13push_new_poolEv
	testq	%rax, %rax
	je	.LBB13_9
# BB#7:
	movq	%rax, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN19btGenericMemoryPool8allocateEm # TAILCALL
.LBB13_9:                               # %.thread.i.i
	movl	$16, %esi
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z22btAlignedAllocInternalmi # TAILCALL
.Lfunc_end13:
	.size	_Z11btPoolAllocm, .Lfunc_end13-_Z11btPoolAllocm
	.cfi_endproc

	.globl	_Z13btPoolReallocPvmm
	.p2align	4, 0x90
	.type	_Z13btPoolReallocPvmm,@function
_Z13btPoolReallocPvmm:                  # @_Z13btPoolReallocPvmm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 48
.Lcfi42:
	.cfi_offset %rbx, -48
.Lcfi43:
	.cfi_offset %r12, -40
.Lcfi44:
	.cfi_offset %r13, -32
.Lcfi45:
	.cfi_offset %r14, -24
.Lcfi46:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r14
	cmpq	$0, g_main_allocator+152(%rip)
	je	.LBB14_5
# BB#1:                                 # %.lr.ph.i.i.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB14_2:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	g_main_allocator+16(,%rbx,8), %rdi
	movq	%r15, %rsi
	callq	_ZN19btGenericMemoryPool8allocateEm
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB14_4
# BB#3:                                 # %.lr.ph.i.i
                                        #   in Loop: Header=BB14_2 Depth=1
	cmpq	g_main_allocator+152(%rip), %rbx
	leaq	1(%rbx), %rbx
	jb	.LBB14_2
.LBB14_4:                               # %._crit_edge.i.i
	testq	%r13, %r13
	jne	.LBB14_10
.LBB14_5:                               # %._crit_edge.thread.i.i
	movq	g_main_allocator+16(%rip), %rax
	imulq	g_main_allocator+8(%rip), %rax
	cmpq	%r15, %rax
	jb	.LBB14_7
# BB#6:
	movl	$g_main_allocator, %edi
	callq	_ZN22btGenericPoolAllocator13push_new_poolEv
	testq	%rax, %rax
	je	.LBB14_7
# BB#8:
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	_ZN19btGenericMemoryPool8allocateEm
	jmp	.LBB14_9
.LBB14_7:                               # %.thread.i.i.i
	movl	$16, %esi
	movq	%r15, %rdi
	callq	_Z22btAlignedAllocInternalmi
.LBB14_9:                               # %_Z11btPoolAllocm.exit
	movq	%rax, %r13
.LBB14_10:                              # %_Z11btPoolAllocm.exit
	cmpq	%r15, %r12
	cmovbq	%r12, %r15
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	memcpy
	movq	g_main_allocator+152(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB14_16
# BB#11:                                # %.lr.ph.i.i12
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB14_12:                              # =>This Inner Loop Header: Depth=1
	movq	g_main_allocator+24(,%rdx,8), %rsi
	movq	(%rsi), %rdi
	cmpq	%r14, %rdi
	ja	.LBB14_15
# BB#13:                                #   in Loop: Header=BB14_12 Depth=1
	movq	%r14, %rax
	subq	%rdi, %rax
	movq	40(%rsi), %rdi
	movq	48(%rsi), %rbx
	imulq	%rdi, %rbx
	cmpq	%rbx, %rax
	jb	.LBB14_14
.LBB14_15:                              # %_ZN19btGenericMemoryPool10freeMemoryEPv.exit.i.i
                                        #   in Loop: Header=BB14_12 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jb	.LBB14_12
.LBB14_16:                              # %.critedge13.i.i
	movq	%r14, %rdi
	callq	_Z21btAlignedFreeInternalPv
	jmp	.LBB14_17
.LBB14_14:                              # %.critedge.thread.i.i
	xorl	%edx, %edx
	divq	%rdi
	movq	8(%rsi), %rcx
	movq	32(%rsi), %rdx
	movq	%rax, (%rcx,%rdx,8)
	incq	32(%rsi)
.LBB14_17:                              # %_Z10btPoolFreePv.exit
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	_Z13btPoolReallocPvmm, .Lfunc_end14-_Z13btPoolReallocPvmm
	.cfi_endproc

	.globl	_Z10btPoolFreePv
	.p2align	4, 0x90
	.type	_Z10btPoolFreePv,@function
_Z10btPoolFreePv:                       # @_Z10btPoolFreePv
	.cfi_startproc
# BB#0:
	movq	g_main_allocator+152(%rip), %r8
	testq	%r8, %r8
	je	.LBB15_5
# BB#1:                                 # %.lr.ph.i
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB15_2:                               # =>This Inner Loop Header: Depth=1
	movq	g_main_allocator+24(,%rdx,8), %rsi
	movq	(%rsi), %rcx
	cmpq	%rdi, %rcx
	ja	.LBB15_4
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	%rdi, %rax
	subq	%rcx, %rax
	movq	40(%rsi), %r9
	movq	48(%rsi), %rcx
	imulq	%r9, %rcx
	cmpq	%rcx, %rax
	jb	.LBB15_6
.LBB15_4:                               # %_ZN19btGenericMemoryPool10freeMemoryEPv.exit.i
                                        #   in Loop: Header=BB15_2 Depth=1
	incq	%rdx
	cmpq	%r8, %rdx
	jb	.LBB15_2
.LBB15_5:                               # %.critedge13.i
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB15_6:                               # %_ZN22btGenericPoolAllocator10freeMemoryEPv.exit
	xorl	%edx, %edx
	divq	%r9
	movq	8(%rsi), %rcx
	movq	32(%rsi), %rdx
	movq	%rax, (%rcx,%rdx,8)
	incq	32(%rsi)
	retq
.Lfunc_end15:
	.size	_Z10btPoolFreePv, .Lfunc_end15-_Z10btPoolFreePv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI16_0:
	.zero	16
	.section	.text._ZN22GIM_STANDARD_ALLOCATORD0Ev,"axG",@progbits,_ZN22GIM_STANDARD_ALLOCATORD0Ev,comdat
	.weak	_ZN22GIM_STANDARD_ALLOCATORD0Ev
	.p2align	4, 0x90
	.type	_ZN22GIM_STANDARD_ALLOCATORD0Ev,@function
_ZN22GIM_STANDARD_ALLOCATORD0Ev:        # @_ZN22GIM_STANDARD_ALLOCATORD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 32
.Lcfi50:
	.cfi_offset %rbx, -32
.Lcfi51:
	.cfi_offset %r14, -24
.Lcfi52:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	$_ZTV22btGenericPoolAllocator+16, (%r15)
	cmpq	$0, 152(%r15)
	je	.LBB16_7
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB16_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%r15,%r14,8), %rbx
	movq	(%rbx), %rdi
.Ltmp9:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp10:
# BB#3:                                 # %.noexc
                                        #   in Loop: Header=BB16_2 Depth=1
	movq	8(%rbx), %rdi
.Ltmp11:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp12:
# BB#4:                                 # %.noexc2
                                        #   in Loop: Header=BB16_2 Depth=1
	movq	16(%rbx), %rdi
.Ltmp13:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp14:
# BB#5:                                 # %.noexc3
                                        #   in Loop: Header=BB16_2 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	24(%r15,%r14,8), %rdi
.Ltmp15:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp16:
# BB#6:                                 # %.noexc4
                                        #   in Loop: Header=BB16_2 Depth=1
	incq	%r14
	cmpq	152(%r15), %r14
	jb	.LBB16_2
.LBB16_7:                               # %_ZN22btGenericPoolAllocatorD2Ev.exit
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdlPv                  # TAILCALL
.LBB16_8:
.Ltmp17:
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end16:
	.size	_ZN22GIM_STANDARD_ALLOCATORD0Ev, .Lfunc_end16-_ZN22GIM_STANDARD_ALLOCATORD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp16-.Ltmp9          #   Call between .Ltmp9 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end16-.Ltmp16    #   Call between .Ltmp16 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_0:
	.quad	8                       # 0x8
	.quad	32768                   # 0x8000
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_btGenericPoolAllocator.ii,@function
_GLOBAL__sub_I_btGenericPoolAllocator.ii: # @_GLOBAL__sub_I_btGenericPoolAllocator.ii
	.cfi_startproc
# BB#0:
	movq	$0, g_main_allocator+152(%rip)
	movaps	.LCPI17_0(%rip), %xmm0  # xmm0 = [8,32768]
	movups	%xmm0, g_main_allocator+8(%rip)
	movq	$_ZTV22GIM_STANDARD_ALLOCATOR+16, g_main_allocator(%rip)
	movl	$_ZN22btGenericPoolAllocatorD2Ev, %edi
	movl	$g_main_allocator, %esi
	movl	$__dso_handle, %edx
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end17:
	.size	_GLOBAL__sub_I_btGenericPoolAllocator.ii, .Lfunc_end17-_GLOBAL__sub_I_btGenericPoolAllocator.ii
	.cfi_endproc

	.type	_ZTV22btGenericPoolAllocator,@object # @_ZTV22btGenericPoolAllocator
	.section	.rodata,"a",@progbits
	.globl	_ZTV22btGenericPoolAllocator
	.p2align	3
_ZTV22btGenericPoolAllocator:
	.quad	0
	.quad	_ZTI22btGenericPoolAllocator
	.quad	_ZN22btGenericPoolAllocatorD2Ev
	.quad	_ZN22btGenericPoolAllocatorD0Ev
	.size	_ZTV22btGenericPoolAllocator, 32

	.type	g_main_allocator,@object # @g_main_allocator
	.bss
	.globl	g_main_allocator
	.p2align	3
g_main_allocator:
	.zero	160
	.size	g_main_allocator, 160

	.type	_ZTS22btGenericPoolAllocator,@object # @_ZTS22btGenericPoolAllocator
	.section	.rodata,"a",@progbits
	.globl	_ZTS22btGenericPoolAllocator
	.p2align	4
_ZTS22btGenericPoolAllocator:
	.asciz	"22btGenericPoolAllocator"
	.size	_ZTS22btGenericPoolAllocator, 25

	.type	_ZTI22btGenericPoolAllocator,@object # @_ZTI22btGenericPoolAllocator
	.globl	_ZTI22btGenericPoolAllocator
	.p2align	3
_ZTI22btGenericPoolAllocator:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS22btGenericPoolAllocator
	.size	_ZTI22btGenericPoolAllocator, 16

	.type	_ZTV22GIM_STANDARD_ALLOCATOR,@object # @_ZTV22GIM_STANDARD_ALLOCATOR
	.section	.rodata._ZTV22GIM_STANDARD_ALLOCATOR,"aG",@progbits,_ZTV22GIM_STANDARD_ALLOCATOR,comdat
	.weak	_ZTV22GIM_STANDARD_ALLOCATOR
	.p2align	3
_ZTV22GIM_STANDARD_ALLOCATOR:
	.quad	0
	.quad	_ZTI22GIM_STANDARD_ALLOCATOR
	.quad	_ZN22btGenericPoolAllocatorD2Ev
	.quad	_ZN22GIM_STANDARD_ALLOCATORD0Ev
	.size	_ZTV22GIM_STANDARD_ALLOCATOR, 32

	.type	_ZTS22GIM_STANDARD_ALLOCATOR,@object # @_ZTS22GIM_STANDARD_ALLOCATOR
	.section	.rodata._ZTS22GIM_STANDARD_ALLOCATOR,"aG",@progbits,_ZTS22GIM_STANDARD_ALLOCATOR,comdat
	.weak	_ZTS22GIM_STANDARD_ALLOCATOR
	.p2align	4
_ZTS22GIM_STANDARD_ALLOCATOR:
	.asciz	"22GIM_STANDARD_ALLOCATOR"
	.size	_ZTS22GIM_STANDARD_ALLOCATOR, 25

	.type	_ZTI22GIM_STANDARD_ALLOCATOR,@object # @_ZTI22GIM_STANDARD_ALLOCATOR
	.section	.rodata._ZTI22GIM_STANDARD_ALLOCATOR,"aG",@progbits,_ZTI22GIM_STANDARD_ALLOCATOR,comdat
	.weak	_ZTI22GIM_STANDARD_ALLOCATOR
	.p2align	4
_ZTI22GIM_STANDARD_ALLOCATOR:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22GIM_STANDARD_ALLOCATOR
	.quad	_ZTI22btGenericPoolAllocator
	.size	_ZTI22GIM_STANDARD_ALLOCATOR, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_btGenericPoolAllocator.ii

	.globl	_ZN22btGenericPoolAllocatorD1Ev
	.type	_ZN22btGenericPoolAllocatorD1Ev,@function
_ZN22btGenericPoolAllocatorD1Ev = _ZN22btGenericPoolAllocatorD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
