	.text
	.file	"btUniversalConstraint.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN21btUniversalConstraintC2ER11btRigidBodyS1_R9btVector3S3_S3_
	.p2align	4, 0x90
	.type	_ZN21btUniversalConstraintC2ER11btRigidBodyS1_R9btVector3S3_S3_,@function
_ZN21btUniversalConstraintC2ER11btRigidBodyS1_R9btVector3S3_S3_: # @_ZN21btUniversalConstraintC2ER11btRigidBodyS1_R9btVector3S3_S3_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 256
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movq	%r8, %rbp
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movb	_ZGVZN11btTransform11getIdentityEvE17identityTransform(%rip), %al
	testb	%al, %al
	jne	.LBB0_6
# BB#1:
	movl	$_ZGVZN11btTransform11getIdentityEvE17identityTransform, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB0_6
# BB#2:
	movb	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip), %al
	testb	%al, %al
	jne	.LBB0_5
# BB#3:
	movl	$_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB0_5
# BB#4:
	movl	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip) # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+4(%rip)
	movl	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+20(%rip) # imm = 0x3F800000
	movups	%xmm0, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+24(%rip)
	movq	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+40(%rip) # imm = 0x3F800000
	movl	$_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, %edi
	callq	__cxa_guard_release
.LBB0_5:
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform(%rip)
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix+16(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+16(%rip)
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix+32(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+32(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+48(%rip)
	movl	$_ZGVZN11btTransform11getIdentityEvE17identityTransform, %edi
	callq	__cxa_guard_release
.LBB0_6:                                # %_ZN11btTransform11getIdentityEv.exit
	movb	_ZGVZN11btTransform11getIdentityEvE17identityTransform(%rip), %al
	testb	%al, %al
	jne	.LBB0_12
# BB#7:
	movl	$_ZGVZN11btTransform11getIdentityEvE17identityTransform, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB0_12
# BB#8:
	movb	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip), %al
	testb	%al, %al
	jne	.LBB0_11
# BB#9:
	movl	$_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB0_11
# BB#10:
	movl	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip) # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+4(%rip)
	movl	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+20(%rip) # imm = 0x3F800000
	movups	%xmm0, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+24(%rip)
	movq	$1065353216, _ZZN11btMatrix3x311getIdentityEvE14identityMatrix+40(%rip) # imm = 0x3F800000
	movl	$_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, %edi
	callq	__cxa_guard_release
.LBB0_11:
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform(%rip)
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix+16(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+16(%rip)
	movups	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix+32(%rip), %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+32(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZN11btTransform11getIdentityEvE17identityTransform+48(%rip)
	movl	$_ZGVZN11btTransform11getIdentityEvE17identityTransform, %edi
	callq	__cxa_guard_release
.LBB0_12:
	movl	$_ZZN11btTransform11getIdentityEvE17identityTransform, %ecx
	movl	$_ZZN11btTransform11getIdentityEvE17identityTransform, %r8d
	movl	$1, %r9d
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	_ZN23btGeneric6DofConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	movq	$_ZTV21btUniversalConstraint+16, (%rbx)
	movups	(%r15), %xmm0
	movups	%xmm0, 1268(%rbx)
	movups	(%rbp), %xmm0
	movups	%xmm0, 1284(%rbx)
	movups	(%r13), %xmm0
	movups	%xmm0, 1300(%rbx)
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB0_14
# BB#13:                                # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB0_14:                               # %.split
	movss	.LCPI0_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	divss	%xmm1, %xmm2
	movss	(%rbp), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	movss	%xmm4, (%rbp)
	movss	4(%rbp), %xmm9          # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm9
	movss	%xmm9, 4(%rbp)
	mulss	8(%rbp), %xmm2
	movss	%xmm2, 8(%rbp)
	movss	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	movss	%xmm2, 16(%rsp)         # 4-byte Spill
	movss	%xmm4, 12(%rsp)         # 4-byte Spill
	movss	%xmm9, 44(%rsp)         # 4-byte Spill
	jnp	.LBB0_16
# BB#15:                                # %call.sqrt365
	callq	sqrtf
	movss	44(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	.LCPI0_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB0_16:                               # %.split.split
	divss	%xmm1, %xmm3
	movss	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movss	%xmm0, (%r13)
	movaps	%xmm0, %xmm1
	movss	4(%r13), %xmm12         # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm12
	movss	%xmm12, 4(%r13)
	mulss	8(%r13), %xmm3
	movss	%xmm3, 8(%r13)
	movaps	%xmm2, %xmm7
	mulss	%xmm12, %xmm7
	movaps	%xmm9, %xmm0
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm7
	movaps	%xmm7, %xmm15
	movaps	%xmm4, %xmm5
	mulss	%xmm3, %xmm5
	movaps	%xmm2, %xmm0
	mulss	%xmm1, %xmm0
	subss	%xmm0, %xmm5
	movss	%xmm5, 20(%rsp)         # 4-byte Spill
	movaps	%xmm9, %xmm7
	mulss	%xmm1, %xmm7
	movaps	%xmm1, %xmm8
	movss	%xmm8, 48(%rsp)         # 4-byte Spill
	movaps	%xmm4, %xmm0
	mulss	%xmm12, %xmm0
	subss	%xmm0, %xmm7
	movaps	%xmm7, %xmm14
	movss	%xmm14, 8(%rsp)         # 4-byte Spill
	movsd	8(%r12), %xmm7          # xmm7 = mem[0],zero
	movaps	%xmm7, 80(%rsp)         # 16-byte Spill
	movsd	24(%r12), %xmm11        # xmm11 = mem[0],zero
	movd	56(%r12), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movdqa	.LCPI0_1(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm4, %xmm13
	movd	60(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movdqa	%xmm0, %xmm1
	pxor	%xmm4, %xmm1
	movdqa	%xmm4, %xmm5
	pshufd	$224, %xmm13, %xmm2     # xmm2 = xmm13[0,0,2,3]
	mulps	%xmm7, %xmm2
	pshufd	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm11, %xmm1
	addps	%xmm2, %xmm1
	movd	64(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movdqa	%xmm4, %xmm2
	pxor	%xmm5, %xmm2
	pshufd	$224, %xmm2, %xmm5      # xmm5 = xmm2[0,0,2,3]
	movsd	40(%r12), %xmm2         # xmm2 = mem[0],zero
	mulps	%xmm2, %xmm5
	addps	%xmm1, %xmm5
	movaps	%xmm5, 64(%rsp)         # 16-byte Spill
	movss	16(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 24(%rsp)         # 4-byte Spill
	mulss	%xmm1, %xmm13
	movss	32(%r12), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm0
	subss	%xmm0, %xmm13
	movaps	%xmm3, %xmm5
	movss	48(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	subss	%xmm4, %xmm13
	movaps	%xmm15, %xmm4
	movss	%xmm4, 56(%rsp)         # 4-byte Spill
	movaps	%xmm4, %xmm0
	mulss	%xmm7, %xmm0
	movss	20(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm1
	mulss	%xmm11, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm14, %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, 40(%rsp)         # 4-byte Spill
	movaps	%xmm8, %xmm0
	mulss	%xmm7, %xmm0
	movaps	%xmm12, %xmm1
	movss	%xmm12, 52(%rsp)        # 4-byte Spill
	mulss	%xmm11, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm5, %xmm0
	movaps	%xmm5, %xmm15
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, 36(%rsp)         # 4-byte Spill
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	movaps	%xmm1, %xmm5
	mulss	%xmm7, %xmm0
	mulss	%xmm11, %xmm9
	addss	%xmm0, %xmm9
	movss	16(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm9, %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	pshufd	$229, %xmm7, %xmm14     # xmm14 = xmm7[1,1,2,3]
	movaps	%xmm4, %xmm1
	mulss	%xmm14, %xmm1
	pshufd	$229, %xmm11, %xmm4     # xmm4 = xmm11[1,1,2,3]
	movaps	%xmm6, %xmm0
	mulss	%xmm4, %xmm0
	addss	%xmm1, %xmm0
	pshufd	$229, %xmm2, %xmm9      # xmm9 = xmm2[1,1,2,3]
	movss	8(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm1
	mulss	%xmm9, %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, 28(%rsp)         # 4-byte Spill
	movss	48(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm14, %xmm0
	movaps	%xmm12, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm15, %xmm12
	movss	%xmm12, 60(%rsp)        # 4-byte Spill
	mulss	%xmm9, %xmm15
	addss	%xmm1, %xmm15
	mulss	%xmm5, %xmm14
	movss	44(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm4
	addss	%xmm14, %xmm4
	mulss	%xmm8, %xmm9
	addss	%xmm4, %xmm9
	movss	56(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	movss	20(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm6, %xmm8
	mulss	%xmm3, %xmm8
	addss	%xmm1, %xmm8
	movaps	%xmm7, %xmm0
	movaps	%xmm0, %xmm1
	mulss	%xmm4, %xmm1
	movss	52(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm12, %xmm1
	mulss	%xmm3, %xmm1
	addss	%xmm0, %xmm1
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	movaps	%xmm4, %xmm7
	mulss	%xmm10, %xmm5
	addss	%xmm0, %xmm5
	movss	16(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm12
	addss	%xmm5, %xmm12
	movss	(%r15), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	movaps	%xmm5, 160(%rsp)        # 16-byte Spill
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movaps	%xmm4, 176(%rsp)        # 16-byte Spill
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	mulps	%xmm4, %xmm0
	movss	4(%r15), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm4
	movaps	%xmm6, 144(%rsp)        # 16-byte Spill
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movaps	%xmm4, 80(%rsp)         # 16-byte Spill
	mulps	%xmm4, %xmm11
	addps	%xmm0, %xmm11
	movss	8(%r15), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm0
	movaps	%xmm4, 128(%rsp)        # 16-byte Spill
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movaps	%xmm0, 112(%rsp)        # 16-byte Spill
	mulps	%xmm0, %xmm2
	addps	%xmm11, %xmm2
	addps	64(%rsp), %xmm2         # 16-byte Folded Reload
	mulss	%xmm5, %xmm7
	mulss	%xmm6, %xmm10
	addss	%xmm7, %xmm10
	mulss	%xmm4, %xmm3
	addss	%xmm10, %xmm3
	addss	%xmm13, %xmm3
	xorps	%xmm6, %xmm6
	movss	%xmm3, %xmm6            # xmm6 = xmm3[0],xmm6[1,2,3]
	movss	40(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 96(%rbx)
	movss	36(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 100(%rbx)
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 104(%rbx)
	movl	$0, 108(%rbx)
	movss	28(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 112(%rbx)
	movss	%xmm15, 116(%rbx)
	movss	%xmm9, 120(%rbx)
	movl	$0, 124(%rbx)
	movss	%xmm8, 128(%rbx)
	movss	%xmm1, 132(%rbx)
	movss	%xmm12, 136(%rbx)
	movl	$0, 140(%rbx)
	movlps	%xmm2, 144(%rbx)
	movlps	%xmm6, 152(%rbx)
	movd	56(%r14), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movdqa	.LCPI0_1(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm1, %xmm10
	movd	60(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movdqa	%xmm0, %xmm2
	pxor	%xmm1, %xmm2
	movsd	8(%r14), %xmm4          # xmm4 = mem[0],zero
	pshufd	$224, %xmm10, %xmm3     # xmm3 = xmm10[0,0,2,3]
	mulps	%xmm4, %xmm3
	movaps	%xmm4, %xmm5
	movaps	%xmm5, 96(%rsp)         # 16-byte Spill
	pshufd	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movsd	24(%r14), %xmm15        # xmm15 = mem[0],zero
	mulps	%xmm15, %xmm2
	addps	%xmm3, %xmm2
	movd	64(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	pxor	%xmm3, %xmm1
	pshufd	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movsd	40(%r14), %xmm14        # xmm14 = mem[0],zero
	mulps	%xmm14, %xmm1
	addps	%xmm2, %xmm1
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	movss	16(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 28(%rsp)         # 4-byte Spill
	mulss	%xmm1, %xmm10
	movss	32(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 24(%rsp)         # 4-byte Spill
	mulss	%xmm1, %xmm0
	subss	%xmm0, %xmm10
	movss	48(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm3
	subss	%xmm3, %xmm10
	movss	56(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	mulss	%xmm5, %xmm0
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	mulss	%xmm15, %xmm2
	addss	%xmm0, %xmm2
	movss	8(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	mulss	%xmm14, %xmm0
	addss	%xmm2, %xmm0
	movss	%xmm0, 40(%rsp)         # 4-byte Spill
	movss	48(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	movss	52(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm2
	mulss	%xmm15, %xmm2
	addss	%xmm0, %xmm2
	movss	60(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm0
	mulss	%xmm14, %xmm0
	addss	%xmm2, %xmm0
	movss	%xmm0, 36(%rsp)         # 4-byte Spill
	movss	12(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	mulss	%xmm5, %xmm0
	movss	44(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm2
	mulss	%xmm15, %xmm2
	addss	%xmm0, %xmm2
	movss	16(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm13
	mulss	%xmm14, %xmm13
	addss	%xmm2, %xmm13
	pshufd	$229, %xmm5, %xmm6      # xmm6 = xmm5[1,1,2,3]
	movaps	%xmm1, %xmm0
	mulss	%xmm6, %xmm0
	pshufd	$229, %xmm15, %xmm5     # xmm5 = xmm15[1,1,2,3]
	movaps	%xmm3, %xmm2
	mulss	%xmm5, %xmm2
	addss	%xmm0, %xmm2
	pshufd	$229, %xmm14, %xmm1     # xmm1 = xmm14[1,1,2,3]
	movaps	%xmm8, %xmm3
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	movss	48(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	mulss	%xmm6, %xmm2
	mulss	%xmm5, %xmm7
	addss	%xmm2, %xmm7
	movaps	%xmm4, %xmm2
	mulss	%xmm1, %xmm2
	addss	%xmm7, %xmm2
	mulss	%xmm9, %xmm6
	movaps	%xmm11, %xmm7
	mulss	%xmm7, %xmm5
	addss	%xmm6, %xmm5
	movaps	%xmm12, %xmm11
	mulss	%xmm11, %xmm1
	addss	%xmm5, %xmm1
	movss	56(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	movss	20(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm5
	addss	%xmm0, %xmm5
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	addss	%xmm5, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	mulss	%xmm4, %xmm8
	movss	52(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	addss	%xmm8, %xmm0
	movss	60(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm5
	addss	%xmm0, %xmm5
	mulss	%xmm4, %xmm9
	movaps	%xmm4, %xmm8
	movaps	%xmm7, %xmm4
	mulss	%xmm6, %xmm4
	addss	%xmm9, %xmm4
	movaps	%xmm11, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm4, %xmm0
	movaps	%xmm0, %xmm9
	movaps	176(%rsp), %xmm0        # 16-byte Reload
	mulps	96(%rsp), %xmm0         # 16-byte Folded Reload
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	mulps	%xmm15, %xmm4
	addps	%xmm0, %xmm4
	movaps	112(%rsp), %xmm0        # 16-byte Reload
	mulps	%xmm14, %xmm0
	addps	%xmm4, %xmm0
	addps	64(%rsp), %xmm0         # 16-byte Folded Reload
	movaps	%xmm0, %xmm7
	movaps	160(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm8, %xmm4
	movaps	144(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm6, %xmm0
	addss	%xmm4, %xmm0
	movaps	128(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm12, %xmm4
	addss	%xmm0, %xmm4
	addss	%xmm10, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movss	40(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	%xmm4, 160(%rbx)
	movss	36(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	%xmm4, 164(%rbx)
	movss	%xmm13, 168(%rbx)
	movl	$0, 172(%rbx)
	movss	%xmm3, 176(%rbx)
	movss	%xmm2, 180(%rbx)
	movss	%xmm1, 184(%rbx)
	movl	$0, 188(%rbx)
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 192(%rbx)
	movss	%xmm5, 196(%rbx)
	movss	%xmm9, 200(%rbx)
	movl	$0, 204(%rbx)
	movlps	%xmm7, 208(%rbx)
	movlps	%xmm0, 216(%rbx)
	xorps	%xmm0, %xmm0
	movl	$0, 868(%rbx)
	movups	%xmm0, 744(%rbx)
	movups	%xmm0, 728(%rbx)
	movl	$-1077426131, 924(%rbx) # imm = 0xBFC7C82D
	movl	$-1068995580, 980(%rbx) # imm = 0xC0486C04
	movl	$0, 872(%rbx)
	movl	$1070057517, 928(%rbx)  # imm = 0x3FC7C82D
	movl	$1078488068, 984(%rbx)  # imm = 0x40486C04
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN21btUniversalConstraintC2ER11btRigidBodyS1_R9btVector3S3_S3_, .Lfunc_end0-_ZN21btUniversalConstraintC2ER11btRigidBodyS1_R9btVector3S3_S3_
	.cfi_endproc

	.section	.text._ZN17btTypedConstraintD2Ev,"axG",@progbits,_ZN17btTypedConstraintD2Ev,comdat
	.weak	_ZN17btTypedConstraintD2Ev
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraintD2Ev,@function
_ZN17btTypedConstraintD2Ev:             # @_ZN17btTypedConstraintD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN17btTypedConstraintD2Ev, .Lfunc_end1-_ZN17btTypedConstraintD2Ev
	.cfi_endproc

	.section	.text._ZN21btUniversalConstraintD0Ev,"axG",@progbits,_ZN21btUniversalConstraintD0Ev,comdat
	.weak	_ZN21btUniversalConstraintD0Ev
	.p2align	4, 0x90
	.type	_ZN21btUniversalConstraintD0Ev,@function
_ZN21btUniversalConstraintD0Ev:         # @_ZN21btUniversalConstraintD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end2:
	.size	_ZN21btUniversalConstraintD0Ev, .Lfunc_end2-_ZN21btUniversalConstraintD0Ev
	.cfi_endproc

	.section	.text._ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,"axG",@progbits,_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,comdat
	.weak	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,@function
_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif: # @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end3:
	.size	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif, .Lfunc_end3-_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_endproc

	.type	_ZTV21btUniversalConstraint,@object # @_ZTV21btUniversalConstraint
	.section	.rodata._ZTV21btUniversalConstraint,"aG",@progbits,_ZTV21btUniversalConstraint,comdat
	.weak	_ZTV21btUniversalConstraint
	.p2align	3
_ZTV21btUniversalConstraint:
	.quad	0
	.quad	_ZTI21btUniversalConstraint
	.quad	_ZN17btTypedConstraintD2Ev
	.quad	_ZN21btUniversalConstraintD0Ev
	.quad	_ZN23btGeneric6DofConstraint13buildJacobianEv
	.quad	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.quad	_ZN23btGeneric6DofConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.quad	_ZN23btGeneric6DofConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.quad	_ZN23btGeneric6DofConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.quad	_ZN23btGeneric6DofConstraint13calcAnchorPosEv
	.size	_ZTV21btUniversalConstraint, 80

	.type	_ZTS21btUniversalConstraint,@object # @_ZTS21btUniversalConstraint
	.section	.rodata._ZTS21btUniversalConstraint,"aG",@progbits,_ZTS21btUniversalConstraint,comdat
	.weak	_ZTS21btUniversalConstraint
	.p2align	4
_ZTS21btUniversalConstraint:
	.asciz	"21btUniversalConstraint"
	.size	_ZTS21btUniversalConstraint, 24

	.type	_ZTI21btUniversalConstraint,@object # @_ZTI21btUniversalConstraint
	.section	.rodata._ZTI21btUniversalConstraint,"aG",@progbits,_ZTI21btUniversalConstraint,comdat
	.weak	_ZTI21btUniversalConstraint
	.p2align	4
_ZTI21btUniversalConstraint:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21btUniversalConstraint
	.quad	_ZTI23btGeneric6DofConstraint
	.size	_ZTI21btUniversalConstraint, 24

	.type	_ZZN11btTransform11getIdentityEvE17identityTransform,@object # @_ZZN11btTransform11getIdentityEvE17identityTransform
	.section	.bss._ZZN11btTransform11getIdentityEvE17identityTransform,"aGw",@nobits,_ZZN11btTransform11getIdentityEvE17identityTransform,comdat
	.weak	_ZZN11btTransform11getIdentityEvE17identityTransform
	.p2align	2
_ZZN11btTransform11getIdentityEvE17identityTransform:
	.zero	64
	.size	_ZZN11btTransform11getIdentityEvE17identityTransform, 64

	.type	_ZGVZN11btTransform11getIdentityEvE17identityTransform,@object # @_ZGVZN11btTransform11getIdentityEvE17identityTransform
	.section	.bss._ZGVZN11btTransform11getIdentityEvE17identityTransform,"aGw",@nobits,_ZGVZN11btTransform11getIdentityEvE17identityTransform,comdat
	.weak	_ZGVZN11btTransform11getIdentityEvE17identityTransform
	.p2align	3
_ZGVZN11btTransform11getIdentityEvE17identityTransform:
	.quad	0                       # 0x0
	.size	_ZGVZN11btTransform11getIdentityEvE17identityTransform, 8

	.type	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix,@object # @_ZZN11btMatrix3x311getIdentityEvE14identityMatrix
	.section	.bss._ZZN11btMatrix3x311getIdentityEvE14identityMatrix,"aGw",@nobits,_ZZN11btMatrix3x311getIdentityEvE14identityMatrix,comdat
	.weak	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix
	.p2align	2
_ZZN11btMatrix3x311getIdentityEvE14identityMatrix:
	.zero	48
	.size	_ZZN11btMatrix3x311getIdentityEvE14identityMatrix, 48

	.type	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix,@object # @_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix
	.section	.bss._ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix,"aGw",@nobits,_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix,comdat
	.weak	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix
	.p2align	3
_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix:
	.quad	0                       # 0x0
	.size	_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix, 8


	.globl	_ZN21btUniversalConstraintC1ER11btRigidBodyS1_R9btVector3S3_S3_
	.type	_ZN21btUniversalConstraintC1ER11btRigidBodyS1_R9btVector3S3_S3_,@function
_ZN21btUniversalConstraintC1ER11btRigidBodyS1_R9btVector3S3_S3_ = _ZN21btUniversalConstraintC2ER11btRigidBodyS1_R9btVector3S3_S3_
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
