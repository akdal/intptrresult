	.text
	.file	"main.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:                                 # %.preheader
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$3960, %rsp             # imm = 0xF78
.Lcfi6:
	.cfi_def_cfa_offset 4016
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	$.Lstr, %edi
	callq	puts
	leaq	2800(%rsp), %rbx
	leaq	1648(%rsp), %rax
	leaq	16(%rsp), %rdi
	leaq	400(%rsp), %r14
	leaq	304(%rsp), %r15
	leaq	208(%rsp), %r12
	leaq	112(%rsp), %r13
	leaq	496(%rsp), %rbp
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	movq	%r13, %r8
	movq	%rbp, %r9
	pushq	%rbx
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	init
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	leaq	16(%rsp), %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	movq	%r13, %r8
	movq	%rbp, %r9
	pushq	%rbx
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	leaq	1656(%rsp), %rax
	pushq	%rax
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	callq	SPEdriver
	addq	$16, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset -16
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.L.str.2, %edi
	movb	$1, %al
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	printf
	movsd	496(%rsp), %xmm0        # xmm0 = mem[0],zero
	movl	$.L.str.3, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movb	$1, %al
	callq	printf
	movsd	528(%rsp), %xmm0        # xmm0 = mem[0],zero
	movl	$.L.str.3, %edi
	xorl	%esi, %esi
	movl	$4, %edx
	movb	$1, %al
	callq	printf
	movsd	560(%rsp), %xmm0        # xmm0 = mem[0],zero
	movl	$.L.str.3, %edi
	xorl	%esi, %esi
	movl	$8, %edx
	movb	$1, %al
	callq	printf
	movsd	880(%rsp), %xmm0        # xmm0 = mem[0],zero
	movl	$.L.str.3, %edi
	movl	$4, %esi
	xorl	%edx, %edx
	movb	$1, %al
	callq	printf
	movsd	912(%rsp), %xmm0        # xmm0 = mem[0],zero
	movl	$.L.str.3, %edi
	movl	$4, %esi
	movl	$4, %edx
	movb	$1, %al
	callq	printf
	movsd	944(%rsp), %xmm0        # xmm0 = mem[0],zero
	movl	$.L.str.3, %edi
	movl	$4, %esi
	movl	$8, %edx
	movb	$1, %al
	callq	printf
	movsd	1264(%rsp), %xmm0       # xmm0 = mem[0],zero
	movl	$.L.str.3, %edi
	movl	$8, %esi
	xorl	%edx, %edx
	movb	$1, %al
	callq	printf
	movsd	1296(%rsp), %xmm0       # xmm0 = mem[0],zero
	movl	$.L.str.3, %edi
	movl	$8, %esi
	movl	$4, %edx
	movb	$1, %al
	callq	printf
	movsd	1328(%rsp), %xmm0       # xmm0 = mem[0],zero
	movl	$.L.str.3, %edi
	movl	$8, %esi
	movl	$8, %edx
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	addq	$3960, %rsp             # imm = 0xF78
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"returnVal = %f \n"
	.size	.L.str.2, 17

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"i = %5d j = %5d    dtcdgd[i][j]   = %e \n"
	.size	.L.str.3, 41

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"\nSequoia benchmark version 1.0"
	.size	.Lstr, 31

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.1:
	.asciz	"\n***** results "
	.size	.Lstr.1, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
