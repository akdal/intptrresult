	.text
	.file	"getopt.bc"
	.globl	espresso_getopt
	.p2align	4, 0x90
	.type	espresso_getopt,@function
espresso_getopt:                        # @espresso_getopt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	$0, optarg(%rip)
	movq	espresso_getopt.scan(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:
	movb	(%rbx), %al
	testb	%al, %al
	je	.LBB0_2
.LBB0_10:
	incq	%rbx
	movq	%rbx, espresso_getopt.scan(%rip)
	movsbl	%al, %ebp
	movq	%rdx, %rdi
	movl	%ebp, %esi
	callq	strchr
	cmpb	$58, %bpl
	je	.LBB0_12
# BB#11:
	testq	%rax, %rax
	je	.LBB0_12
# BB#13:
	cmpb	$58, 1(%rax)
	jne	.LBB0_17
# BB#14:
	cmpb	$0, (%rbx)
	je	.LBB0_16
# BB#15:
	movq	%rbx, optarg(%rip)
	movq	$0, espresso_getopt.scan(%rip)
	jmp	.LBB0_17
.LBB0_2:
	movl	optind(%rip), %eax
	testl	%eax, %eax
	jne	.LBB0_4
# BB#3:
	movl	$1, optind(%rip)
	movl	$1, %eax
.LBB0_4:
	movl	$-1, %ebp
	cmpl	%edi, %eax
	jge	.LBB0_17
# BB#5:
	movslq	%eax, %rcx
	movq	(%r14,%rcx,8), %rcx
	cmpb	$45, (%rcx)
	jne	.LBB0_17
# BB#6:
	cmpb	$0, 1(%rcx)
	je	.LBB0_17
# BB#7:
	leaq	1(%rcx), %rbx
	incl	%eax
	movl	%eax, optind(%rip)
	movb	(%rbx), %al
	cmpb	$45, %al
	jne	.LBB0_9
# BB#8:
	cmpb	$0, 2(%rcx)
	je	.LBB0_17
.LBB0_9:
	movq	%rbx, espresso_getopt.scan(%rip)
	jmp	.LBB0_10
.LBB0_16:
	movslq	optind(%rip), %rax
	movq	(%r14,%rax,8), %rcx
	movq	%rcx, optarg(%rip)
	leal	1(%rax), %eax
	movl	%eax, optind(%rip)
	jmp	.LBB0_17
.LBB0_12:
	movq	stderr(%rip), %rdi
	movq	(%r14), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ebp, %ecx
	callq	fprintf
	movl	$63, %ebp
.LBB0_17:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	espresso_getopt, .Lfunc_end0-espresso_getopt
	.cfi_endproc

	.type	optind,@object          # @optind
	.bss
	.globl	optind
	.p2align	2
optind:
	.long	0                       # 0x0
	.size	optind, 4

	.type	espresso_getopt.scan,@object # @espresso_getopt.scan
	.local	espresso_getopt.scan
	.comm	espresso_getopt.scan,8,8
	.type	optarg,@object          # @optarg
	.comm	optarg,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s: unknown option %c\n"
	.size	.L.str, 23


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
