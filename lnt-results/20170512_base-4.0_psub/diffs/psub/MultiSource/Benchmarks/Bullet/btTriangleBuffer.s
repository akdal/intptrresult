	.text
	.file	"btTriangleBuffer.bc"
	.globl	_ZN16btTriangleBuffer15processTriangleEP9btVector3ii
	.p2align	4, 0x90
	.type	_ZN16btTriangleBuffer15processTriangleEP9btVector3ii,@function
_ZN16btTriangleBuffer15processTriangleEP9btVector3ii: # @_ZN16btTriangleBuffer15processTriangleEP9btVector3ii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 96
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %r15d
	movq	%rdi, %rbx
	movups	(%rsi), %xmm0
	movaps	%xmm0, (%rsp)
	movups	16(%rsi), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	32(%rsi), %xmm0
	movaps	%xmm0, 32(%rsp)
	movl	12(%rbx), %eax
	cmpl	16(%rbx), %eax
	jne	.LBB0_18
# BB#1:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %r12d
	cmovnel	%ecx, %r12d
	cmpl	%r12d, %eax
	jge	.LBB0_18
# BB#2:
	testl	%r12d, %r12d
	je	.LBB0_3
# BB#4:
	movslq	%r12d, %rax
	imulq	$56, %rax, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB0_6
	jmp	.LBB0_13
.LBB0_3:
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jle	.LBB0_13
.LBB0_6:                                # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %r9
	andq	$3, %r9
	je	.LBB0_7
# BB#8:                                 # %.prol.preheader
	xorl	%edi, %edi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	movq	24(%rbx), %rcx
	movq	48(%rcx,%rdi), %rsi
	movq	%rsi, 48(%rbp,%rdi)
	movups	(%rcx,%rdi), %xmm0
	movups	16(%rcx,%rdi), %xmm1
	movups	32(%rcx,%rdi), %xmm2
	movups	%xmm2, 32(%rbp,%rdi)
	movups	%xmm1, 16(%rbp,%rdi)
	movups	%xmm0, (%rbp,%rdi)
	incq	%rdx
	addq	$56, %rdi
	cmpq	%rdx, %r9
	jne	.LBB0_9
	jmp	.LBB0_10
.LBB0_7:
	xorl	%edx, %edx
.LBB0_10:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB0_13
# BB#11:                                # %.lr.ph.i.i.i.new
	subq	%rdx, %rax
	imulq	$56, %rdx, %rcx
	addq	$168, %rcx
	.p2align	4, 0x90
.LBB0_12:                               # =>This Inner Loop Header: Depth=1
	movq	24(%rbx), %rdx
	movq	-120(%rdx,%rcx), %rsi
	movq	%rsi, -120(%rbp,%rcx)
	movups	-168(%rdx,%rcx), %xmm0
	movups	-152(%rdx,%rcx), %xmm1
	movups	-136(%rdx,%rcx), %xmm2
	movups	%xmm2, -136(%rbp,%rcx)
	movups	%xmm1, -152(%rbp,%rcx)
	movups	%xmm0, -168(%rbp,%rcx)
	movq	24(%rbx), %rdx
	movq	-64(%rdx,%rcx), %rsi
	movq	%rsi, -64(%rbp,%rcx)
	movups	-112(%rdx,%rcx), %xmm0
	movups	-96(%rdx,%rcx), %xmm1
	movups	-80(%rdx,%rcx), %xmm2
	movups	%xmm2, -80(%rbp,%rcx)
	movups	%xmm1, -96(%rbp,%rcx)
	movups	%xmm0, -112(%rbp,%rcx)
	movq	24(%rbx), %rdx
	movq	-8(%rdx,%rcx), %rsi
	movq	%rsi, -8(%rbp,%rcx)
	movups	-56(%rdx,%rcx), %xmm0
	movups	-40(%rdx,%rcx), %xmm1
	movups	-24(%rdx,%rcx), %xmm2
	movups	%xmm2, -24(%rbp,%rcx)
	movups	%xmm1, -40(%rbp,%rcx)
	movups	%xmm0, -56(%rbp,%rcx)
	movq	24(%rbx), %rdx
	movq	48(%rdx,%rcx), %rsi
	movq	%rsi, 48(%rbp,%rcx)
	movups	(%rdx,%rcx), %xmm0
	movups	16(%rdx,%rcx), %xmm1
	movups	32(%rdx,%rcx), %xmm2
	movups	%xmm2, 32(%rbp,%rcx)
	movups	%xmm1, 16(%rbp,%rcx)
	movups	%xmm0, (%rbp,%rcx)
	addq	$224, %rcx
	addq	$-4, %rax
	jne	.LBB0_12
.LBB0_13:                               # %_ZNK20btAlignedObjectArrayI10btTriangleE4copyEiiPS0_.exit.i.i
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_17
# BB#14:
	cmpb	$0, 32(%rbx)
	je	.LBB0_16
# BB#15:
	callq	_Z21btAlignedFreeInternalPv
.LBB0_16:
	movq	$0, 24(%rbx)
.LBB0_17:                               # %_ZN20btAlignedObjectArrayI10btTriangleE10deallocateEv.exit.i.i
	movb	$1, 32(%rbx)
	movq	%rbp, 24(%rbx)
	movl	%r12d, 16(%rbx)
	movl	12(%rbx), %eax
.LBB0_18:                               # %_ZN20btAlignedObjectArrayI10btTriangleE9push_backERKS0_.exit
	movq	24(%rbx), %rcx
	cltq
	imulq	$56, %rax, %rax
	movaps	(%rsp), %xmm0
	movaps	16(%rsp), %xmm1
	movaps	32(%rsp), %xmm2
	movups	%xmm2, 32(%rcx,%rax)
	movups	%xmm1, 16(%rcx,%rax)
	movups	%xmm0, (%rcx,%rax)
	movl	%r15d, 48(%rcx,%rax)
	movl	%r14d, 52(%rcx,%rax)
	incl	12(%rbx)
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN16btTriangleBuffer15processTriangleEP9btVector3ii, .Lfunc_end0-_ZN16btTriangleBuffer15processTriangleEP9btVector3ii
	.cfi_endproc

	.section	.text._ZN16btTriangleBufferD2Ev,"axG",@progbits,_ZN16btTriangleBufferD2Ev,comdat
	.weak	_ZN16btTriangleBufferD2Ev
	.p2align	4, 0x90
	.type	_ZN16btTriangleBufferD2Ev,@function
_ZN16btTriangleBufferD2Ev:              # @_ZN16btTriangleBufferD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV16btTriangleBuffer+16, (%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#1:
	cmpb	$0, 32(%rbx)
	je	.LBB1_3
# BB#2:
.Ltmp0:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp1:
.LBB1_3:                                # %.noexc
	movq	$0, 24(%rbx)
.LBB1_4:
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN18btTriangleCallbackD2Ev # TAILCALL
.LBB1_5:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp4:
# BB#6:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB1_7:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end1:
	.size	_ZN16btTriangleBufferD2Ev, .Lfunc_end1-_ZN16btTriangleBufferD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end1-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN16btTriangleBufferD0Ev,"axG",@progbits,_ZN16btTriangleBufferD0Ev,comdat
	.weak	_ZN16btTriangleBufferD0Ev
	.p2align	4, 0x90
	.type	_ZN16btTriangleBufferD0Ev,@function
_ZN16btTriangleBufferD0Ev:              # @_ZN16btTriangleBufferD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV16btTriangleBuffer+16, (%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#1:
	cmpb	$0, 32(%rbx)
	je	.LBB2_3
# BB#2:
.Ltmp6:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp7:
.LBB2_3:                                # %.noexc.i
	movq	$0, 24(%rbx)
.LBB2_4:
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
.Ltmp12:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp13:
# BB#5:                                 # %_ZN16btTriangleBufferD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB2_6:
.Ltmp8:
	movq	%rax, %r14
.Ltmp9:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp10:
	jmp	.LBB2_9
.LBB2_7:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_8:
.Ltmp14:
	movq	%rax, %r14
.LBB2_9:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN16btTriangleBufferD0Ev, .Lfunc_end2-_ZN16btTriangleBufferD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate

	.type	_ZTV16btTriangleBuffer,@object # @_ZTV16btTriangleBuffer
	.section	.rodata,"a",@progbits
	.globl	_ZTV16btTriangleBuffer
	.p2align	3
_ZTV16btTriangleBuffer:
	.quad	0
	.quad	_ZTI16btTriangleBuffer
	.quad	_ZN16btTriangleBufferD2Ev
	.quad	_ZN16btTriangleBufferD0Ev
	.quad	_ZN16btTriangleBuffer15processTriangleEP9btVector3ii
	.size	_ZTV16btTriangleBuffer, 40

	.type	_ZTS16btTriangleBuffer,@object # @_ZTS16btTriangleBuffer
	.globl	_ZTS16btTriangleBuffer
	.p2align	4
_ZTS16btTriangleBuffer:
	.asciz	"16btTriangleBuffer"
	.size	_ZTS16btTriangleBuffer, 19

	.type	_ZTI16btTriangleBuffer,@object # @_ZTI16btTriangleBuffer
	.globl	_ZTI16btTriangleBuffer
	.p2align	4
_ZTI16btTriangleBuffer:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16btTriangleBuffer
	.quad	_ZTI18btTriangleCallback
	.size	_ZTI16btTriangleBuffer, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
