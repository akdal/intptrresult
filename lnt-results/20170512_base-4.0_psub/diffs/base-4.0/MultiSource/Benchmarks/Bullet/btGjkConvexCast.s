	.text
	.file	"btGjkConvexCast.bc"
	.globl	_ZN15btGjkConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
	.p2align	4, 0x90
	.type	_ZN15btGjkConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver,@function
_ZN15btGjkConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver: # @_ZN15btGjkConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
	.cfi_startproc
# BB#0:
	movq	$_ZTV15btGjkConvexCast+16, (%rdi)
	movq	%rcx, 8(%rdi)
	movq	%rsi, 16(%rdi)
	movq	%rdx, 24(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN15btGjkConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver, .Lfunc_end0-_ZN15btGjkConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	981668463               # float 0.00100000005
.LCPI1_1:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
	.p2align	4, 0x90
	.type	_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE,@function
_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE: # @_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$344, %rsp              # imm = 0x158
.Lcfi6:
	.cfi_def_cfa_offset 400
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rcx, %r13
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movq	8(%rbp), %rdi
	callq	_ZN22btVoronoiSimplexSolver5resetEv
	movss	48(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	52(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	48(%rbx), %xmm1
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	subss	52(%rbx), %xmm0
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movss	56(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	56(%rbx), %xmm0
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movss	48(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	52(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	48(%r13), %xmm1
	movss	%xmm1, 28(%rsp)         # 4-byte Spill
	subss	52(%r13), %xmm0
	movss	%xmm0, 20(%rsp)         # 4-byte Spill
	movss	56(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	56(%r13), %xmm0
	movss	%xmm0, 24(%rsp)         # 4-byte Spill
	movq	$_ZTV16btPointCollector+16, 32(%rsp)
	movl	$1566444395, 72(%rsp)   # imm = 0x5D5E0B6B
	movb	$0, 76(%rsp)
	movq	8(%rbp), %rcx
	movq	16(%rbp), %rsi
	movq	24(%rbp), %rdx
.Ltmp0:
	leaq	248(%rsp), %rdi
	xorl	%r8d, %r8d
	callq	_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
.Ltmp1:
# BB#1:
	leaq	48(%rbx), %rax
	leaq	48(%r13), %rcx
	movl	$1566444395, 224(%rsp)  # imm = 0x5D5E0B6B
	movq	$0, 232(%rsp)
	movups	(%rbx), %xmm0
	movaps	%xmm0, 96(%rsp)
	movups	16(%rbx), %xmm0
	movaps	%xmm0, 112(%rsp)
	movups	32(%rbx), %xmm0
	movaps	%xmm0, 128(%rsp)
	movups	(%rax), %xmm0
	movaps	%xmm0, 144(%rsp)
	movups	(%r13), %xmm0
	movaps	%xmm0, 160(%rsp)
	movups	16(%r13), %xmm0
	movaps	%xmm0, 176(%rsp)
	movups	32(%r13), %xmm0
	movaps	%xmm0, 192(%rsp)
	movups	(%rcx), %xmm0
	movaps	%xmm0, 208(%rsp)
.Ltmp3:
	leaq	248(%rsp), %rdi
	leaq	96(%rsp), %rsi
	leaq	32(%rsp), %rdx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	callq	_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
.Ltmp4:
# BB#2:
	movb	76(%rsp), %al
	testb	%al, %al
	movups	56(%rsp), %xmm0
	movaps	%xmm0, 80(%rsp)
	je	.LBB1_15
# BB#3:
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	28(%rsp), %xmm0         # 4-byte Folded Reload
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	20(%rsp), %xmm0         # 4-byte Folded Reload
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	24(%rsp), %xmm0         # 4-byte Folded Reload
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movss	72(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movl	48(%rsp), %eax
	movl	40(%rsp), %edx
	movl	44(%rsp), %ecx
	ucomiss	.LCPI1_0(%rip), %xmm5
	jbe	.LBB1_16
# BB#4:                                 # %.lr.ph
	xorps	%xmm4, %xmm4
	movl	$-1, %ebp
	xorps	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB1_5:                                # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$31, %ebp
	jg	.LBB1_21
# BB#6:                                 #   in Loop: Header=BB1_5 Depth=1
	movd	%edx, %xmm2
	mulss	8(%rsp), %xmm2          # 4-byte Folded Reload
	movd	%ecx, %xmm3
	mulss	16(%rsp), %xmm3         # 4-byte Folded Reload
	addss	%xmm2, %xmm3
	movd	%eax, %xmm2
	mulss	12(%rsp), %xmm2         # 4-byte Folded Reload
	addss	%xmm3, %xmm2
	divss	%xmm2, %xmm5
	movaps	%xmm1, %xmm0
	subss	%xmm5, %xmm0
	xorl	%ecx, %ecx
	ucomiss	%xmm0, %xmm1
	jae	.LBB1_24
# BB#7:                                 #   in Loop: Header=BB1_5 Depth=1
	ucomiss	%xmm0, %xmm4
	ja	.LBB1_24
# BB#8:                                 #   in Loop: Header=BB1_5 Depth=1
	ucomiss	.LCPI1_1(%rip), %xmm0
	ja	.LBB1_24
# BB#9:                                 #   in Loop: Header=BB1_5 Depth=1
	movq	(%r14), %rax
.Ltmp6:
	movq	%r14, %rdi
	movss	%xmm0, 20(%rsp)         # 4-byte Spill
	callq	*(%rax)
.Ltmp7:
# BB#10:                                #   in Loop: Header=BB1_5 Depth=1
	movss	.LCPI1_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	movss	48(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	48(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	movss	%xmm2, 144(%rsp)
	movss	52(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	52(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	movss	%xmm2, 148(%rsp)
	movss	56(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	56(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	movss	%xmm2, 152(%rsp)
	movss	48(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	48(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	movss	%xmm2, 208(%rsp)
	movss	52(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	52(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	movss	%xmm2, 212(%rsp)
	mulss	56(%r13), %xmm0
	movss	56(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, 216(%rsp)
.Ltmp8:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	248(%rsp), %rdi
	leaq	96(%rsp), %rsi
	leaq	32(%rsp), %rdx
	callq	_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
.Ltmp9:
# BB#11:                                #   in Loop: Header=BB1_5 Depth=1
	movss	20(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cmpb	$0, 76(%rsp)
	je	.LBB1_21
# BB#12:                                #   in Loop: Header=BB1_5 Depth=1
	movss	72(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	xorps	%xmm4, %xmm4
	ucomiss	%xmm5, %xmm4
	ja	.LBB1_22
# BB#13:                                #   in Loop: Header=BB1_5 Depth=1
	leaq	56(%rsp), %rax
	movups	(%rax), %xmm1
	movaps	%xmm1, 80(%rsp)
	movl	48(%rsp), %eax
	movl	40(%rsp), %edx
	movl	44(%rsp), %ecx
	ucomiss	.LCPI1_0(%rip), %xmm5
	movaps	%xmm0, %xmm6
	movaps	%xmm6, %xmm1
	ja	.LBB1_5
# BB#14:                                # %._crit_edge.loopexit
	movss	52(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB1_17
.LBB1_15:
	xorl	%ecx, %ecx
	jmp	.LBB1_24
.LBB1_16:
	movss	52(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm6, %xmm6
.LBB1_17:                               # %._crit_edge
	movd	%edx, %xmm1
	movss	8(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movd	%ecx, %xmm1
	movss	16(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movd	%eax, %xmm1
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	movss	184(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	.LCPI1_2(%rip), %xmm1
	ucomiss	%xmm1, %xmm3
	jae	.LBB1_21
# BB#18:
	movss	%xmm6, 168(%r14)
	movl	%edx, 136(%r14)
	movl	%ecx, 140(%r14)
	movl	%eax, 144(%r14)
	movss	%xmm0, 148(%r14)
	movaps	80(%rsp), %xmm0
	jmp	.LBB1_23
.LBB1_21:
	xorl	%ecx, %ecx
	jmp	.LBB1_24
.LBB1_22:
	movss	%xmm0, 168(%r14)
	movups	40(%rsp), %xmm0
	movups	%xmm0, 136(%r14)
	leaq	56(%rsp), %rax
	movups	(%rax), %xmm0
.LBB1_23:                               # %.thread
	movups	%xmm0, 152(%r14)
	movb	$1, %cl
.LBB1_24:                               # %.thread
	movl	%ecx, %eax
	addq	$344, %rsp              # imm = 0x158
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_25:
.Ltmp5:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB1_26:
.Ltmp2:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB1_27:
.Ltmp10:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE, .Lfunc_end1-_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp9-.Ltmp6           #   Call between .Ltmp6 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end1-.Ltmp9      #   Call between .Ltmp9 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN15btGjkConvexCastD0Ev,"axG",@progbits,_ZN15btGjkConvexCastD0Ev,comdat
	.weak	_ZN15btGjkConvexCastD0Ev
	.p2align	4, 0x90
	.type	_ZN15btGjkConvexCastD0Ev,@function
_ZN15btGjkConvexCastD0Ev:               # @_ZN15btGjkConvexCastD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp11:
	callq	_ZN12btConvexCastD2Ev
.Ltmp12:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB2_2:
.Ltmp13:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN15btGjkConvexCastD0Ev, .Lfunc_end2-_ZN15btGjkConvexCastD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end2-.Ltmp12     #   Call between .Ltmp12 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN16btPointCollectorD0Ev,"axG",@progbits,_ZN16btPointCollectorD0Ev,comdat
	.weak	_ZN16btPointCollectorD0Ev
	.p2align	4, 0x90
	.type	_ZN16btPointCollectorD0Ev,@function
_ZN16btPointCollectorD0Ev:              # @_ZN16btPointCollectorD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end3:
	.size	_ZN16btPointCollectorD0Ev, .Lfunc_end3-_ZN16btPointCollectorD0Ev
	.cfi_endproc

	.section	.text._ZN16btPointCollector20setShapeIdentifiersAEii,"axG",@progbits,_ZN16btPointCollector20setShapeIdentifiersAEii,comdat
	.weak	_ZN16btPointCollector20setShapeIdentifiersAEii
	.p2align	4, 0x90
	.type	_ZN16btPointCollector20setShapeIdentifiersAEii,@function
_ZN16btPointCollector20setShapeIdentifiersAEii: # @_ZN16btPointCollector20setShapeIdentifiersAEii
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end4:
	.size	_ZN16btPointCollector20setShapeIdentifiersAEii, .Lfunc_end4-_ZN16btPointCollector20setShapeIdentifiersAEii
	.cfi_endproc

	.section	.text._ZN16btPointCollector20setShapeIdentifiersBEii,"axG",@progbits,_ZN16btPointCollector20setShapeIdentifiersBEii,comdat
	.weak	_ZN16btPointCollector20setShapeIdentifiersBEii
	.p2align	4, 0x90
	.type	_ZN16btPointCollector20setShapeIdentifiersBEii,@function
_ZN16btPointCollector20setShapeIdentifiersBEii: # @_ZN16btPointCollector20setShapeIdentifiersBEii
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end5:
	.size	_ZN16btPointCollector20setShapeIdentifiersBEii, .Lfunc_end5-_ZN16btPointCollector20setShapeIdentifiersBEii
	.cfi_endproc

	.section	.text._ZN16btPointCollector15addContactPointERK9btVector3S2_f,"axG",@progbits,_ZN16btPointCollector15addContactPointERK9btVector3S2_f,comdat
	.weak	_ZN16btPointCollector15addContactPointERK9btVector3S2_f
	.p2align	4, 0x90
	.type	_ZN16btPointCollector15addContactPointERK9btVector3S2_f,@function
_ZN16btPointCollector15addContactPointERK9btVector3S2_f: # @_ZN16btPointCollector15addContactPointERK9btVector3S2_f
	.cfi_startproc
# BB#0:
	movss	40(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB6_2
# BB#1:
	movb	$1, 44(%rdi)
	movups	(%rsi), %xmm1
	movups	%xmm1, 8(%rdi)
	movups	(%rdx), %xmm1
	movups	%xmm1, 24(%rdi)
	movss	%xmm0, 40(%rdi)
.LBB6_2:
	retq
.Lfunc_end6:
	.size	_ZN16btPointCollector15addContactPointERK9btVector3S2_f, .Lfunc_end6-_ZN16btPointCollector15addContactPointERK9btVector3S2_f
	.cfi_endproc

	.section	.text._ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev,"axG",@progbits,_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev,comdat
	.weak	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.p2align	4, 0x90
	.type	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev,@function
_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev: # @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end7:
	.size	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev, .Lfunc_end7-_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.cfi_endproc

	.type	_ZTV15btGjkConvexCast,@object # @_ZTV15btGjkConvexCast
	.section	.rodata,"a",@progbits
	.globl	_ZTV15btGjkConvexCast
	.p2align	3
_ZTV15btGjkConvexCast:
	.quad	0
	.quad	_ZTI15btGjkConvexCast
	.quad	_ZN12btConvexCastD2Ev
	.quad	_ZN15btGjkConvexCastD0Ev
	.quad	_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
	.size	_ZTV15btGjkConvexCast, 40

	.type	_ZTS15btGjkConvexCast,@object # @_ZTS15btGjkConvexCast
	.globl	_ZTS15btGjkConvexCast
	.p2align	4
_ZTS15btGjkConvexCast:
	.asciz	"15btGjkConvexCast"
	.size	_ZTS15btGjkConvexCast, 18

	.type	_ZTI15btGjkConvexCast,@object # @_ZTI15btGjkConvexCast
	.globl	_ZTI15btGjkConvexCast
	.p2align	4
_ZTI15btGjkConvexCast:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15btGjkConvexCast
	.quad	_ZTI12btConvexCast
	.size	_ZTI15btGjkConvexCast, 24

	.type	_ZTV16btPointCollector,@object # @_ZTV16btPointCollector
	.section	.rodata._ZTV16btPointCollector,"aG",@progbits,_ZTV16btPointCollector,comdat
	.weak	_ZTV16btPointCollector
	.p2align	3
_ZTV16btPointCollector:
	.quad	0
	.quad	_ZTI16btPointCollector
	.quad	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.quad	_ZN16btPointCollectorD0Ev
	.quad	_ZN16btPointCollector20setShapeIdentifiersAEii
	.quad	_ZN16btPointCollector20setShapeIdentifiersBEii
	.quad	_ZN16btPointCollector15addContactPointERK9btVector3S2_f
	.size	_ZTV16btPointCollector, 56

	.type	_ZTS16btPointCollector,@object # @_ZTS16btPointCollector
	.section	.rodata._ZTS16btPointCollector,"aG",@progbits,_ZTS16btPointCollector,comdat
	.weak	_ZTS16btPointCollector
	.p2align	4
_ZTS16btPointCollector:
	.asciz	"16btPointCollector"
	.size	_ZTS16btPointCollector, 19

	.type	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE,@object # @_ZTSN36btDiscreteCollisionDetectorInterface6ResultE
	.section	.rodata._ZTSN36btDiscreteCollisionDetectorInterface6ResultE,"aG",@progbits,_ZTSN36btDiscreteCollisionDetectorInterface6ResultE,comdat
	.weak	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE
	.p2align	4
_ZTSN36btDiscreteCollisionDetectorInterface6ResultE:
	.asciz	"N36btDiscreteCollisionDetectorInterface6ResultE"
	.size	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE, 48

	.type	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE,@object # @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE
	.section	.rodata._ZTIN36btDiscreteCollisionDetectorInterface6ResultE,"aG",@progbits,_ZTIN36btDiscreteCollisionDetectorInterface6ResultE,comdat
	.weak	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE
	.p2align	3
_ZTIN36btDiscreteCollisionDetectorInterface6ResultE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE
	.size	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE, 16

	.type	_ZTI16btPointCollector,@object # @_ZTI16btPointCollector
	.section	.rodata._ZTI16btPointCollector,"aG",@progbits,_ZTI16btPointCollector,comdat
	.weak	_ZTI16btPointCollector
	.p2align	4
_ZTI16btPointCollector:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16btPointCollector
	.quad	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE
	.size	_ZTI16btPointCollector, 24


	.globl	_ZN15btGjkConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
	.type	_ZN15btGjkConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver,@function
_ZN15btGjkConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver = _ZN15btGjkConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
