	.text
	.file	"btMultiSphereShape.bc"
	.globl	_ZN18btMultiSphereShapeC2EPK9btVector3PKfi
	.p2align	4, 0x90
	.type	_ZN18btMultiSphereShapeC2EPK9btVector3PKfi,@function
_ZN18btMultiSphereShapeC2EPK9btVector3PKfi: # @_ZN18btMultiSphereShapeC2EPK9btVector3PKfi
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbp
	callq	_ZN32btConvexInternalAabbCachingShapeC2Ev
	movq	$_ZTV18btMultiSphereShape+16, (%rbp)
	movb	$1, 128(%rbp)
	movq	$0, 120(%rbp)
	movl	$0, 108(%rbp)
	movl	$0, 112(%rbp)
	movb	$1, 160(%rbp)
	movq	$0, 152(%rbp)
	movl	$0, 140(%rbp)
	movl	$0, 144(%rbp)
	movl	$9, 8(%rbp)
	testl	%r12d, %r12d
	jle	.LBB0_6
# BB#1:
	movslq	%r12d, %r13
	movq	%r13, %rdi
	shlq	$4, %rdi
.Ltmp0:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
.Ltmp1:
# BB#2:                                 # %_ZN20btAlignedObjectArrayI9btVector3E8allocateEi.exit.i.i
	movslq	108(%rbp), %rax
	testq	%rax, %rax
	jle	.LBB0_11
# BB#3:                                 # %.lr.ph.i.i.i
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB0_7
# BB#4:                                 # %.prol.preheader
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	movq	120(%rbp), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%rbx,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB0_5
	jmp	.LBB0_8
.LBB0_6:
	xorl	%ebx, %ebx
	jmp	.LBB0_24
.LBB0_7:
	xorl	%ecx, %ecx
.LBB0_8:                                # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB0_11
# BB#9:                                 # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB0_10:                               # =>This Inner Loop Header: Depth=1
	movq	120(%rbp), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbx,%rcx)
	movq	120(%rbp), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbx,%rcx)
	movq	120(%rbp), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbx,%rcx)
	movq	120(%rbp), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbx,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB0_10
.LBB0_11:                               # %_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_.exit.i.i
	movq	120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_15
# BB#12:
	cmpb	$0, 128(%rbp)
	je	.LBB0_14
# BB#13:
.Ltmp2:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp3:
.LBB0_14:                               # %.noexc21
	movq	$0, 120(%rbp)
.LBB0_15:                               # %.lr.ph.i
	movb	$1, 128(%rbp)
	movq	%rbx, 120(%rbp)
	movl	%r12d, 112(%rbp)
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%rbx)
	cmpl	$1, %r12d
	je	.LBB0_23
# BB#16:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge.preheader
	leal	3(%r12), %edx
	leaq	-2(%r13), %rcx
	andq	$3, %rdx
	je	.LBB0_20
# BB#17:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge.prol.preheader
	xorl	%eax, %eax
	movl	$16, %esi
	.p2align	4, 0x90
.LBB0_18:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	120(%rbp), %rdi
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%rdi,%rsi)
	incq	%rax
	addq	$16, %rsi
	cmpq	%rax, %rdx
	jne	.LBB0_18
# BB#19:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge.prol.loopexit.unr-lcssa
	incq	%rax
	cmpq	$3, %rcx
	jae	.LBB0_21
	jmp	.LBB0_23
.LBB0_20:
	movl	$1, %eax
	cmpq	$3, %rcx
	jb	.LBB0_23
.LBB0_21:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge.preheader.new
	subq	%rax, %r13
	shlq	$4, %rax
	addq	$48, %rax
	.p2align	4, 0x90
.LBB0_22:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	120(%rbp), %rcx
	movups	8(%rsp), %xmm0
	movups	%xmm0, -48(%rcx,%rax)
	movq	120(%rbp), %rcx
	movups	8(%rsp), %xmm0
	movups	%xmm0, -32(%rcx,%rax)
	movq	120(%rbp), %rcx
	movups	8(%rsp), %xmm0
	movups	%xmm0, -16(%rcx,%rax)
	movq	120(%rbp), %rcx
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%rcx,%rax)
	addq	$64, %rax
	addq	$-4, %r13
	jne	.LBB0_22
.LBB0_23:                               # %.loopexit47.loopexit
	movl	140(%rbp), %ebx
.LBB0_24:                               # %.loopexit47
	movl	%r12d, 108(%rbp)
	cmpl	%r12d, %ebx
	jge	.LBB0_58
# BB#25:
	cmpl	%r12d, 144(%rbp)
	jge	.LBB0_29
# BB#26:
	testl	%r12d, %r12d
	je	.LBB0_30
# BB#27:
	movslq	%r12d, %rdi
	shlq	$2, %rdi
.Ltmp5:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r13
.Ltmp6:
# BB#28:                                # %.noexc35
	movl	140(%rbp), %ecx
	jmp	.LBB0_31
.LBB0_29:                               # %..lr.ph.i31_crit_edge
	movq	152(%rbp), %r13
	jmp	.LBB0_57
.LBB0_30:
	xorl	%r13d, %r13d
	movl	%ebx, %ecx
.LBB0_31:                               # %_ZN20btAlignedObjectArrayIfE8allocateEi.exit.i.i
	movq	152(%rbp), %rdi
	testl	%ecx, %ecx
	jle	.LBB0_34
# BB#32:                                # %.lr.ph.i.i.i25
	movslq	%ecx, %rax
	cmpl	$8, %ecx
	jae	.LBB0_35
# BB#33:
	xorl	%edx, %edx
	jmp	.LBB0_48
.LBB0_34:                               # %_ZNK20btAlignedObjectArrayIfE4copyEiiPf.exit.i.i
	testq	%rdi, %rdi
	jne	.LBB0_54
	jmp	.LBB0_56
.LBB0_35:                               # %min.iters.checked
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB0_39
# BB#36:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rsi
	cmpq	%rsi, %r13
	jae	.LBB0_40
# BB#37:                                # %vector.memcheck
	leaq	(%r13,%rax,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB0_40
.LBB0_39:
	xorl	%edx, %edx
.LBB0_48:                               # %scalar.ph.preheader
	movl	%ebx, %r8d
	subl	%edx, %ecx
	leaq	-1(%rax), %rsi
	subq	%rdx, %rsi
	andq	$7, %rcx
	je	.LBB0_51
# BB#49:                                # %scalar.ph.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB0_50:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %ebx
	movl	%ebx, (%r13,%rdx,4)
	incq	%rdx
	incq	%rcx
	jne	.LBB0_50
.LBB0_51:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rsi
	movl	%r8d, %ebx
	jb	.LBB0_54
# BB#52:                                # %scalar.ph.preheader.new
	subq	%rdx, %rax
	leaq	28(%rdi,%rdx,4), %rcx
	leaq	28(%r13,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB0_53:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %rax
	jne	.LBB0_53
.LBB0_54:                               # %_ZNK20btAlignedObjectArrayIfE4copyEiiPf.exit.thread.i.i
	cmpb	$0, 160(%rbp)
	je	.LBB0_56
# BB#55:
.Ltmp7:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp8:
.LBB0_56:                               # %.lr.ph.i31.sink.split
	movb	$1, 160(%rbp)
	movq	%r13, 152(%rbp)
	movl	%r12d, 144(%rbp)
.LBB0_57:                               # %.lr.ph.i31
	movslq	%ebx, %rax
	movslq	%r12d, %rdx
	leaq	(%r13,%rax,4), %rdi
	subq	%rax, %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
.LBB0_58:                               # %.loopexit
	movl	%r12d, 140(%rbp)
	testl	%r12d, %r12d
	jle	.LBB0_64
# BB#59:                                # %.lr.ph.preheader
	movl	%r12d, %eax
	testb	$1, %al
	jne	.LBB0_61
# BB#60:
	xorl	%ecx, %ecx
	cmpl	$1, %r12d
	jne	.LBB0_62
	jmp	.LBB0_64
.LBB0_61:                               # %.lr.ph.prol
	movq	120(%rbp), %rcx
	movups	(%r15), %xmm0
	movups	%xmm0, (%rcx)
	movl	(%r14), %ecx
	movq	152(%rbp), %rdx
	movl	%ecx, (%rdx)
	movl	$1, %ecx
	cmpl	$1, %r12d
	je	.LBB0_64
.LBB0_62:                               # %.lr.ph.preheader.new
	movq	%rcx, %rdx
	shlq	$4, %rdx
	.p2align	4, 0x90
.LBB0_63:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	120(%rbp), %rsi
	movups	(%r15,%rdx), %xmm0
	movups	%xmm0, (%rsi,%rdx)
	movl	(%r14,%rcx,4), %esi
	movq	152(%rbp), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movq	120(%rbp), %rsi
	movups	16(%r15,%rdx), %xmm0
	movups	%xmm0, 16(%rsi,%rdx)
	movl	4(%r14,%rcx,4), %esi
	movq	152(%rbp), %rdi
	movl	%esi, 4(%rdi,%rcx,4)
	addq	$2, %rcx
	addq	$32, %rdx
	cmpq	%rcx, %rax
	jne	.LBB0_63
.LBB0_64:                               # %._crit_edge
.Ltmp10:
	movq	%rbp, %rdi
	callq	_ZN32btConvexInternalAabbCachingShape15recalcLocalAabbEv
.Ltmp11:
# BB#65:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_40:                               # %vector.body.preheader
	movl	%ebx, %r9d
	leaq	-8(%rdx), %r8
	movl	%r8d, %ebx
	shrl	$3, %ebx
	incl	%ebx
	andq	$3, %rbx
	je	.LBB0_43
# BB#41:                                # %vector.body.prol.preheader
	negq	%rbx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_42:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rsi,4), %xmm0
	movups	16(%rdi,%rsi,4), %xmm1
	movups	%xmm0, (%r13,%rsi,4)
	movups	%xmm1, 16(%r13,%rsi,4)
	addq	$8, %rsi
	incq	%rbx
	jne	.LBB0_42
	jmp	.LBB0_44
.LBB0_43:
	xorl	%esi, %esi
.LBB0_44:                               # %vector.body.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB0_47
# BB#45:                                # %vector.body.preheader.new
	movq	%rdx, %r8
	subq	%rsi, %r8
	leaq	112(%rdi,%rsi,4), %rbx
	leaq	112(%r13,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB0_46:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rbx
	subq	$-128, %rsi
	addq	$-32, %r8
	jne	.LBB0_46
.LBB0_47:                               # %middle.block
	cmpq	%rdx, %rax
	movl	%r9d, %ebx
	je	.LBB0_54
	jmp	.LBB0_48
.LBB0_66:
.Ltmp9:
	jmp	.LBB0_69
.LBB0_67:
.Ltmp4:
	jmp	.LBB0_69
.LBB0_68:
.Ltmp12:
.LBB0_69:
	movq	%rax, %rbx
	movq	152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_73
# BB#70:
	cmpb	$0, 160(%rbp)
	je	.LBB0_72
# BB#71:
.Ltmp13:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp14:
.LBB0_72:                               # %.noexc19
	movq	$0, 152(%rbp)
.LBB0_73:
	movb	$1, 160(%rbp)
	movq	$0, 152(%rbp)
	movq	$0, 140(%rbp)
	movq	120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_77
# BB#74:
	cmpb	$0, 128(%rbp)
	je	.LBB0_76
# BB#75:
.Ltmp15:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp16:
.LBB0_76:                               # %.noexc
	movq	$0, 120(%rbp)
.LBB0_77:
	movb	$1, 128(%rbp)
	movq	$0, 120(%rbp)
	movq	$0, 108(%rbp)
.Ltmp17:
	movq	%rbp, %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp18:
# BB#78:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_79:
.Ltmp19:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN18btMultiSphereShapeC2EPK9btVector3PKfi, .Lfunc_end0-_ZN18btMultiSphereShapeC2EPK9btVector3PKfi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp8-.Ltmp5           #   Call between .Ltmp5 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp10-.Ltmp8          #   Call between .Ltmp8 and .Ltmp10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp18-.Ltmp13         #   Call between .Ltmp13 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Lfunc_end0-.Ltmp18     #   Call between .Ltmp18 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	1065353216              # float 1
.LCPI2_1:
	.long	679477248               # float 1.42108547E-14
.LCPI2_2:
	.long	3713928043              # float -9.99999984E+17
	.text
	.globl	_ZNK18btMultiSphereShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK18btMultiSphereShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK18btMultiSphereShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK18btMultiSphereShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	subq	$104, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 144
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	(%rsi), %xmm0           # xmm0 = mem[0],zero
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movdqa	%xmm0, 16(%rsp)         # 16-byte Spill
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm2, 4(%rsp)          # 4-byte Spill
	movaps	%xmm2, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	.LCPI2_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB2_2
# BB#1:
	xorps	%xmm0, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movaps	%xmm2, 16(%rsp)         # 16-byte Spill
	jmp	.LBB2_5
.LBB2_2:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB2_4
# BB#3:                                 # %call.sqrt
	callq	sqrtf
	movss	.LCPI2_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB2_4:                                # %.split
	divss	%xmm1, %xmm2
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	mulps	%xmm2, %xmm0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
.LBB2_5:
	movl	108(%r14), %r15d
	testl	%r15d, %r15d
	jle	.LBB2_6
# BB#7:                                 # %.lr.ph
	movq	120(%r14), %rbx
	movq	152(%r14), %rbp
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movss	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB2_8:                                # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movsd	24(%r14), %xmm0         # xmm0 = mem[0],zero
	mulps	16(%rsp), %xmm0         # 16-byte Folded Reload
	movss	32(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	4(%rsp), %xmm2          # 4-byte Folded Reload
	movss	(%rbp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm0, %xmm1
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	addps	%xmm1, %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	addss	8(%rbx), %xmm2
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	movss	8(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	80(%rsp), %xmm3         # 16-byte Reload
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movaps	16(%rsp), %xmm6         # 16-byte Reload
	mulps	%xmm6, %xmm0
	subps	%xmm0, %xmm3
	subss	%xmm1, %xmm4
	movaps	%xmm3, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm6, %xmm0
	mulss	%xmm3, %xmm0
	mulss	48(%rsp), %xmm1         # 16-byte Folded Reload
	addss	%xmm0, %xmm1
	movaps	%xmm5, %xmm0
	mulss	%xmm4, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	%xmm2, %xmm0
	ja	.LBB2_9
# BB#10:                                #   in Loop: Header=BB2_8 Depth=1
	movaps	64(%rsp), %xmm3         # 16-byte Reload
	jmp	.LBB2_11
	.p2align	4, 0x90
.LBB2_9:                                #   in Loop: Header=BB2_8 Depth=1
	xorps	%xmm1, %xmm1
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
.LBB2_11:                               #   in Loop: Header=BB2_8 Depth=1
	addq	$4, %rbp
	maxss	%xmm2, %xmm0
	addq	$16, %rbx
	decl	%r15d
	movaps	%xmm3, %xmm1
	jne	.LBB2_8
	jmp	.LBB2_12
.LBB2_6:
	xorps	%xmm3, %xmm3
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
.LBB2_12:                               # %._crit_edge
	movaps	%xmm3, %xmm0
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	addq	$104, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZNK18btMultiSphereShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end2-_ZNK18btMultiSphereShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	3713928043              # float -9.99999984E+17
	.text
	.globl	_ZNK18btMultiSphereShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK18btMultiSphereShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK18btMultiSphereShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK18btMultiSphereShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi28:
	.cfi_def_cfa_offset 144
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	testl	%ecx, %ecx
	jle	.LBB3_8
# BB#1:                                 # %.lr.ph94
	movl	%ecx, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB3_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
	movl	108(%rbx), %ebp
	testl	%ebp, %ebp
	jle	.LBB3_7
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	120(%rbx), %r12
	movq	152(%rbx), %r14
	movq	8(%rsp), %rax           # 8-byte Reload
	shlq	$4, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax), %r15
	leaq	8(%rcx,%rax), %r13
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	leaq	8(%rcx,%rax), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movss	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB3_4:                                #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	movsd	24(%rbx), %xmm1         # xmm1 = mem[0],zero
	mulps	%xmm0, %xmm1
	movss	(%r13), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	32(%rbx), %xmm2
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	movsd	(%r12), %xmm1           # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	addss	8(%r12), %xmm2
	movss	%xmm2, 20(%rsp)         # 4-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	movsd	(%r15), %xmm2           # xmm2 = mem[0],zero
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm2, %xmm3
	movss	(%r13), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	subps	%xmm3, %xmm4
	movaps	%xmm4, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movss	20(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm5
	pshufd	$229, %xmm2, %xmm0      # xmm0 = xmm2[1,1,2,3]
	mulss	%xmm4, %xmm2
	mulss	%xmm3, %xmm0
	addss	%xmm2, %xmm0
	movaps	%xmm5, %xmm2
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	ucomiss	4(%rsp), %xmm1          # 4-byte Folded Reload
	jbe	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movq	56(%rsp), %rax          # 8-byte Reload
	movlps	%xmm4, (%rax)
	movq	48(%rsp), %rax          # 8-byte Reload
	movlps	%xmm0, (%rax)
	movss	%xmm1, 4(%rsp)          # 4-byte Spill
.LBB3_6:                                #   in Loop: Header=BB3_4 Depth=2
	addq	$4, %r14
	addq	$16, %r12
	decl	%ebp
	jne	.LBB3_4
.LBB3_7:                                # %._crit_edge
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	40(%rsp), %rcx          # 8-byte Folded Reload
	jne	.LBB3_2
.LBB3_8:                                # %._crit_edge95
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZNK18btMultiSphereShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end3-_ZNK18btMultiSphereShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1056964608              # float 0.5
.LCPI4_1:
	.long	1094713344              # float 12
	.text
	.globl	_ZNK18btMultiSphereShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK18btMultiSphereShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK18btMultiSphereShape21calculateLocalInertiaEfR9btVector3: # @_ZNK18btMultiSphereShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	movss	80(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	84(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	88(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	64(%rdi), %xmm1
	subss	68(%rdi), %xmm3
	subss	72(%rdi), %xmm2
	movss	.LCPI4_0(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	mulss	%xmm4, %xmm3
	mulss	%xmm4, %xmm2
	addss	%xmm1, %xmm1
	addss	%xmm3, %xmm3
	addss	%xmm2, %xmm2
	divss	.LCPI4_1(%rip), %xmm0
	mulss	%xmm3, %xmm3
	mulss	%xmm2, %xmm2
	movaps	%xmm3, %xmm4
	addss	%xmm2, %xmm4
	mulss	%xmm0, %xmm4
	mulss	%xmm1, %xmm1
	addss	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm4, (%rsi)
	movss	%xmm2, 4(%rsi)
	movss	%xmm1, 8(%rsi)
	movl	$0, 12(%rsi)
	retq
.Lfunc_end4:
	.size	_ZNK18btMultiSphereShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end4-_ZNK18btMultiSphereShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.text._ZN18btMultiSphereShapeD2Ev,"axG",@progbits,_ZN18btMultiSphereShapeD2Ev,comdat
	.weak	_ZN18btMultiSphereShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN18btMultiSphereShapeD2Ev,@function
_ZN18btMultiSphereShapeD2Ev:            # @_ZN18btMultiSphereShapeD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV18btMultiSphereShape+16, (%rbx)
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_4
# BB#1:
	cmpb	$0, 160(%rbx)
	je	.LBB5_3
# BB#2:
.Ltmp20:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp21:
.LBB5_3:                                # %.noexc
	movq	$0, 152(%rbx)
.LBB5_4:
	movb	$1, 160(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 140(%rbx)
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_8
# BB#5:
	cmpb	$0, 128(%rbx)
	je	.LBB5_7
# BB#6:
.Ltmp25:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp26:
.LBB5_7:                                # %.noexc4
	movq	$0, 120(%rbx)
.LBB5_8:
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN13btConvexShapeD2Ev  # TAILCALL
.LBB5_14:
.Ltmp27:
	movq	%rax, %r14
	jmp	.LBB5_15
.LBB5_9:
.Ltmp22:
	movq	%rax, %r14
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_13
# BB#10:
	cmpb	$0, 128(%rbx)
	je	.LBB5_12
# BB#11:
.Ltmp23:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp24:
.LBB5_12:                               # %.noexc6
	movq	$0, 120(%rbx)
.LBB5_13:                               # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit7
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
.LBB5_15:
.Ltmp28:
	movq	%rbx, %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp29:
# BB#16:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_17:
.Ltmp30:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN18btMultiSphereShapeD2Ev, .Lfunc_end5-_ZN18btMultiSphereShapeD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin1   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin1   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp23-.Ltmp26         #   Call between .Ltmp26 and .Ltmp23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp29-.Ltmp23         #   Call between .Ltmp23 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin1   #     jumps to .Ltmp30
	.byte	1                       #   On action: 1
	.long	.Ltmp29-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end5-.Ltmp29     #   Call between .Ltmp29 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN18btMultiSphereShapeD0Ev,"axG",@progbits,_ZN18btMultiSphereShapeD0Ev,comdat
	.weak	_ZN18btMultiSphereShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN18btMultiSphereShapeD0Ev,@function
_ZN18btMultiSphereShapeD0Ev:            # @_ZN18btMultiSphereShapeD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -24
.Lcfi44:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp31:
	callq	_ZN18btMultiSphereShapeD2Ev
.Ltmp32:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB6_2:
.Ltmp33:
	movq	%rax, %r14
.Ltmp34:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp35:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_4:
.Ltmp36:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN18btMultiSphereShapeD0Ev, .Lfunc_end6-_ZN18btMultiSphereShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp31-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin2   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp34-.Ltmp32         #   Call between .Ltmp32 and .Ltmp34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin2   #     jumps to .Ltmp36
	.byte	1                       #   On action: 1
	.long	.Ltmp35-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp35     #   Call between .Ltmp35 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end7:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end7-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK18btMultiSphereShape7getNameEv,"axG",@progbits,_ZNK18btMultiSphereShape7getNameEv,comdat
	.weak	_ZNK18btMultiSphereShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK18btMultiSphereShape7getNameEv,@function
_ZNK18btMultiSphereShape7getNameEv:     # @_ZNK18btMultiSphereShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end8:
	.size	_ZNK18btMultiSphereShape7getNameEv, .Lfunc_end8-_ZNK18btMultiSphereShape7getNameEv
	.cfi_endproc

	.section	.text._ZN21btConvexInternalShape9setMarginEf,"axG",@progbits,_ZN21btConvexInternalShape9setMarginEf,comdat
	.weak	_ZN21btConvexInternalShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShape9setMarginEf,@function
_ZN21btConvexInternalShape9setMarginEf: # @_ZN21btConvexInternalShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 56(%rdi)
	retq
.Lfunc_end9:
	.size	_ZN21btConvexInternalShape9setMarginEf, .Lfunc_end9-_ZN21btConvexInternalShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape9getMarginEv,"axG",@progbits,_ZNK21btConvexInternalShape9getMarginEv,comdat
	.weak	_ZNK21btConvexInternalShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape9getMarginEv,@function
_ZNK21btConvexInternalShape9getMarginEv: # @_ZNK21btConvexInternalShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end10:
	.size	_ZNK21btConvexInternalShape9getMarginEv, .Lfunc_end10-_ZNK21btConvexInternalShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end11:
	.size	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end11-_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end12:
	.size	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end12-_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.type	_ZTV18btMultiSphereShape,@object # @_ZTV18btMultiSphereShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV18btMultiSphereShape
	.p2align	3
_ZTV18btMultiSphereShape:
	.quad	0
	.quad	_ZTI18btMultiSphereShape
	.quad	_ZN18btMultiSphereShapeD2Ev
	.quad	_ZN18btMultiSphereShapeD0Ev
	.quad	_ZNK32btConvexInternalAabbCachingShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN32btConvexInternalAabbCachingShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK18btMultiSphereShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK18btMultiSphereShape7getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK18btMultiSphereShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK18btMultiSphereShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.size	_ZTV18btMultiSphereShape, 160

	.type	_ZTS18btMultiSphereShape,@object # @_ZTS18btMultiSphereShape
	.globl	_ZTS18btMultiSphereShape
	.p2align	4
_ZTS18btMultiSphereShape:
	.asciz	"18btMultiSphereShape"
	.size	_ZTS18btMultiSphereShape, 21

	.type	_ZTI18btMultiSphereShape,@object # @_ZTI18btMultiSphereShape
	.globl	_ZTI18btMultiSphereShape
	.p2align	4
_ZTI18btMultiSphereShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18btMultiSphereShape
	.quad	_ZTI32btConvexInternalAabbCachingShape
	.size	_ZTI18btMultiSphereShape, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"MultiSphere"
	.size	.L.str, 12


	.globl	_ZN18btMultiSphereShapeC1EPK9btVector3PKfi
	.type	_ZN18btMultiSphereShapeC1EPK9btVector3PKfi,@function
_ZN18btMultiSphereShapeC1EPK9btVector3PKfi = _ZN18btMultiSphereShapeC2EPK9btVector3PKfi
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
