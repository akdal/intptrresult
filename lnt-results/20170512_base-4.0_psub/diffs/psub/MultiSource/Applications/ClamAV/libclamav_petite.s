	.text
	.file	"libclamav_petite.bc"
	.globl	petite_inflate2x_1to9
	.p2align	4, 0x90
	.type	petite_inflate2x_1to9,@function
petite_inflate2x_1to9:                  # @petite_inflate2x_1to9
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 304
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rdi, %r10
	movl	320(%rsp), %edi
	movl	%esi, %eax
	movq	%r10, %rsi
	subq	%rax, %rsi
	cmpl	$2, %edi
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	jne	.LBB0_2
# BB#1:
	movl	$53, %ebp
	movl	$853, %ebx              # imm = 0x355
	movl	$440, %eax              # imm = 0x1B8
	jmp	.LBB0_4
.LBB0_2:
	cmpl	$1, %edi
	jne	.LBB0_5
# BB#3:
	movl	$52, %ebp
	movl	$803, %ebx              # imm = 0x323
	movl	$376, %eax              # imm = 0x178
.LBB0_4:                                # %.thread939.preheader.sink.split
	leal	-1(%r8), %edi
	leaq	(%rdi,%rdi,8), %rdi
	movl	(%rcx,%rdi,4), %edi
	addq	%rsi, %rdi
	addq	%rax, %rdi
	jmp	.LBB0_6
.LBB0_5:
	movl	$53, %ebp
	movl	$853, %ebx              # imm = 0x355
	xorl	%edi, %edi
.LBB0_6:                                # %.thread939.preheader
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%r9, 240(%rsp)          # 8-byte Spill
	movl	%edx, %eax
	leaq	(%r10,%rax), %r9
	cmpl	$8, %edx
	setb	39(%rsp)                # 1-byte Folded Spill
	movl	%ebx, %ecx
	negq	%rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%rbx, 168(%rsp)         # 8-byte Spill
	leal	(%rbp,%rbx), %ecx
	movl	%ecx, 156(%rsp)         # 4-byte Spill
	movq	%rbp, 224(%rsp)         # 8-byte Spill
	movl	%ebp, %ecx
	negq	%rcx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	%r8, 184(%rsp)          # 8-byte Spill
	movl	%r8d, %ecx
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	movl	%edx, 52(%rsp)          # 4-byte Spill
	testl	%edx, %edx
	sete	38(%rsp)                # 1-byte Folded Spill
	leaq	-1(%r10,%rax), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movl	$0, 124(%rsp)           # 4-byte Folded Spill
	movl	$0, %r11d
	xorl	%r14d, %r14d
	movl	$0, 128(%rsp)           # 4-byte Folded Spill
	movl	$0, 132(%rsp)           # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
                                        # implicit-def: %EAX
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%r10, (%rsp)            # 8-byte Spill
	movq	%r9, 40(%rsp)           # 8-byte Spill
	jmp	.LBB0_40
.LBB0_7:                                # %.outer977._crit_edge.loopexit1920
                                        #   in Loop: Header=BB0_40 Depth=1
	movq	216(%rsp), %rcx         # 8-byte Reload
	leal	-1(%rax,%rcx), %eax
	addq	%rax, %rbp
	leaq	1(%r13,%rbp), %rbx
	jmp	.LBB0_9
.LBB0_8:                                # %.outer977._crit_edge.loopexit
                                        #   in Loop: Header=BB0_40 Depth=1
	addq	%r13, %rbp
	movq	%rbp, %rbx
.LBB0_9:                                # %.outer977._crit_edge
                                        #   in Loop: Header=BB0_40 Depth=1
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
.LBB0_10:                               # %.outer977._crit_edge
                                        #   in Loop: Header=BB0_40 Depth=1
	testl	%esi, %esi
	movl	52(%rsp), %edi          # 4-byte Reload
	je	.LBB0_22
# BB#11:                                #   in Loop: Header=BB0_40 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	12(%rax), %eax
	cmpl	$8, %edi
	jb	.LBB0_19
# BB#12:                                #   in Loop: Header=BB0_40 Depth=1
	cmpl	168(%rsp), %eax         # 4-byte Folded Reload
	jbe	.LBB0_19
# BB#13:                                #   in Loop: Header=BB0_40 Depth=1
	movq	112(%rsp), %rcx         # 8-byte Reload
	leaq	84(%rbx,%rcx), %rcx
	cmpq	%r10, %rcx
	jb	.LBB0_19
# BB#14:                                #   in Loop: Header=BB0_40 Depth=1
	leaq	8(%rcx), %rdx
	cmpq	%r9, %rdx
	ja	.LBB0_19
# BB#15:                                #   in Loop: Header=BB0_40 Depth=1
	cmpq	%r10, %rdx
	jbe	.LBB0_19
# BB#16:                                #   in Loop: Header=BB0_40 Depth=1
	cmpl	$1683931187, (%rcx)     # imm = 0x645EC033
	jne	.LBB0_19
# BB#17:                                #   in Loop: Header=BB0_40 Depth=1
	cmpl	$462100619, 4(%rcx)     # imm = 0x1B8B188B
	jne	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_40 Depth=1
	xorl	%eax, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	$1, %eax
	cmpl	$8, %edi
	jae	.LBB0_27
	jmp	.LBB0_22
.LBB0_19:                               #   in Loop: Header=BB0_40 Depth=1
	cmpl	$8, %edi
	jb	.LBB0_22
# BB#20:                                #   in Loop: Header=BB0_40 Depth=1
	cmpl	156(%rsp), %eax         # 4-byte Folded Reload
	jbe	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_40 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	(%rbx,%rax), %rax
	movq	160(%rsp), %rcx         # 8-byte Reload
	leaq	84(%rcx,%rax), %rax
	cmpq	%r10, %rax
	jae	.LBB0_23
.LBB0_22:                               # %.thread913
                                        #   in Loop: Header=BB0_40 Depth=1
	movq	%r15, %rdi
	jmp	.LBB0_39
.LBB0_23:                               #   in Loop: Header=BB0_40 Depth=1
	leaq	8(%rax), %rcx
	cmpq	%r9, %rcx
	ja	.LBB0_22
# BB#24:                                #   in Loop: Header=BB0_40 Depth=1
	cmpq	%r10, %rcx
	jbe	.LBB0_22
# BB#25:                                #   in Loop: Header=BB0_40 Depth=1
	cmpl	$1683931187, (%rax)     # imm = 0x645EC033
	jne	.LBB0_22
# BB#26:                                #   in Loop: Header=BB0_40 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	84(%rbx,%rax), %rcx
	xorl	%eax, %eax
	movq	160(%rsp), %rdx         # 8-byte Reload
	cmpl	$462100619, 4(%rcx,%rdx) # imm = 0x1B8B188B
	sete	%al
	movq	104(%rsp), %rcx         # 8-byte Reload
	cmovel	224(%rsp), %ecx         # 4-byte Folded Reload
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	cmpl	$8, %edi
	jb	.LBB0_22
.LBB0_27:                               #   in Loop: Header=BB0_40 Depth=1
	testl	%eax, %eax
	movq	%r15, %rdi
	je	.LBB0_39
# BB#28:                                #   in Loop: Header=BB0_40 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	15(%rbx,%rax), %r13
	movl	104(%rsp), %r12d        # 4-byte Reload
	movq	%r13, %rax
	subq	%r12, %rax
	addq	$-8, %rax
	cmpq	%r10, %rax
	jb	.LBB0_39
# BB#29:                                #   in Loop: Header=BB0_40 Depth=1
	addq	$8, %rax
	cmpq	%r9, %rax
	ja	.LBB0_39
# BB#30:                                #   in Loop: Header=BB0_40 Depth=1
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	cmpq	%r10, %rax
	jbe	.LBB0_38
# BB#31:                                #   in Loop: Header=BB0_40 Depth=1
	negq	%r12
	movl	-8(%r13,%r12), %r14d
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edx
	movl	-4(%r13,%r12), %ebp
	movl	$1953408297, %eax       # imm = 0x746EA529
	xorl	%eax, %ebp
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movq	56(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	cmpl	$437, 52(%rsp)          # 4-byte Folded Reload
                                        # imm = 0x1B5
	jb	.LBB0_37
# BB#32:                                #   in Loop: Header=BB0_40 Depth=1
	movl	%r14d, %eax
	cmpl	%ebp, %r14d
	jne	.LBB0_37
# BB#33:                                #   in Loop: Header=BB0_40 Depth=1
	addq	%r12, %r13
	cmpq	(%rsp), %r13            # 8-byte Folded Reload
	jb	.LBB0_37
# BB#34:                                #   in Loop: Header=BB0_40 Depth=1
	addq	$437, %r13              # imm = 0x1B5
	cmpq	40(%rsp), %r13          # 8-byte Folded Reload
	ja	.LBB0_37
# BB#35:                                #   in Loop: Header=BB0_40 Depth=1
	cmpq	(%rsp), %r13            # 8-byte Folded Reload
	jbe	.LBB0_37
# BB#36:                                #   in Loop: Header=BB0_40 Depth=1
	addq	112(%rsp), %rbx         # 8-byte Folded Reload
	movl	%eax, %ebp
	xorl	$-1654234710, %ebp      # imm = 0x9D6661AA
	movl	289(%rbx,%r12), %edx
	xorl	15(%rbx,%r12), %ebp
	xorl	%eax, %eax
	cmpl	$-1869574000, 448(%rbx,%r12) # imm = 0x90909090
	setne	%al
	movl	%eax, 124(%rsp)         # 4-byte Spill
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	movl	%edx, 128(%rsp)         # 4-byte Spill
	callq	cli_dbgmsg
	movl	%ebp, 132(%rsp)         # 4-byte Spill
.LBB0_37:                               #   in Loop: Header=BB0_40 Depth=1
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%rax), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	subl	%eax, 12(%rcx)
.LBB0_38:                               # %.thread913
                                        #   in Loop: Header=BB0_40 Depth=1
	movq	(%rsp), %r10            # 8-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	movl	68(%rsp), %r11d         # 4-byte Reload
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB0_22
.LBB0_39:                               # %.thread913
                                        #   in Loop: Header=BB0_40 Depth=1
	incl	%r11d
	.p2align	4, 0x90
.LBB0_40:                               # %.thread939.outer.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_41 Depth 2
                                        #       Child Loop BB0_54 Depth 3
                                        #     Child Loop BB0_72 Depth 2
                                        #     Child Loop BB0_87 Depth 2
                                        #       Child Loop BB0_88 Depth 3
                                        #       Child Loop BB0_102 Depth 3
                                        #       Child Loop BB0_114 Depth 3
                                        #       Child Loop BB0_133 Depth 3
                                        #       Child Loop BB0_159 Depth 3
                                        #       Child Loop BB0_163 Depth 3
                                        #       Child Loop BB0_168 Depth 3
                                        #       Child Loop BB0_171 Depth 3
	movslq	%esi, %rsi
	leal	-1(%rsi), %ecx
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
.LBB0_41:                               # %.thread939.outer
                                        #   Parent Loop BB0_40 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_54 Depth 3
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movl	52(%rsp), %r15d         # 4-byte Reload
	cmpl	$4, %r15d
	movq	80(%rsp), %r13          # 8-byte Reload
	movq	136(%rsp), %rbp         # 8-byte Reload
	jb	.LBB0_173
# BB#42:                                # %.thread939.outer
                                        #   in Loop: Header=BB0_41 Depth=2
	cmpq	%r10, %rdi
	jb	.LBB0_173
# BB#43:                                # %.lr.ph1251
                                        #   in Loop: Header=BB0_41 Depth=2
	cmpl	$8, %r15d
	jae	.LBB0_54
# BB#44:                                # %.lr.ph1251.split.us
                                        #   in Loop: Header=BB0_41 Depth=2
	leaq	4(%rdi), %rax
	cmpq	%r9, %rax
	ja	.LBB0_173
# BB#45:                                # %.lr.ph1251.split.us
                                        #   in Loop: Header=BB0_41 Depth=2
	cmpq	%r10, %rax
	jbe	.LBB0_173
# BB#46:                                #   in Loop: Header=BB0_41 Depth=2
	movl	(%rdi), %r12d
	testl	%r12d, %r12d
	je	.LBB0_177
# BB#47:                                #   in Loop: Header=BB0_41 Depth=2
	movl	%r12d, %ecx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	cmpl	%ecx, %r12d
	jne	.LBB0_173
.LBB0_48:                               # %.us-lcssa1258.us
                                        #   in Loop: Header=BB0_41 Depth=2
	cmpq	%r10, %rax
	setb	%cl
	orb	39(%rsp), %cl           # 1-byte Folded Reload
	jne	.LBB0_188
# BB#49:                                #   in Loop: Header=BB0_41 Depth=2
	leaq	12(%rdi), %rcx
	cmpq	%r9, %rcx
	ja	.LBB0_188
# BB#50:                                #   in Loop: Header=BB0_41 Depth=2
	cmpq	%r10, %rcx
	jbe	.LBB0_188
# BB#51:                                #   in Loop: Header=BB0_41 Depth=2
	movq	56(%rsp), %rbx          # 8-byte Reload
	cmpq	$96, %rbx
	movq	16(%rsp), %r14          # 8-byte Reload
	jge	.LBB0_190
# BB#52:                                #   in Loop: Header=BB0_41 Depth=2
	movq	%rbp, 136(%rsp)         # 8-byte Spill
	movl	%r11d, 68(%rsp)         # 4-byte Spill
	movl	(%rax), %r15d
	movl	8(%rdi), %r13d
	leaq	4(,%rbx,4), %rax
	leaq	(%rax,%rax,8), %rsi
	movq	%rdi, %rbp
	movq	%r14, %rdi
	callq	cli_realloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_173
# BB#53:                                #   in Loop: Header=BB0_41 Depth=2
	addq	$16, %rbp
	leaq	1(%rbx), %rsi
	leaq	(%rbx,%rbx,8), %rax
	movl	%r13d, (%r14,%rax,4)
	movl	%r15d, 12(%r14,%rax,4)
	movq	136(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%r13d, %ecx
	testl	%ecx, %ecx
	cmovlel	%r15d, %ecx
	movl	%ecx, 4(%r14,%rax,4)
	movl	$0, 8(%r14,%rax,4)
	movl	8(%rsp), %ecx           # 4-byte Reload
	incl	%ecx
	testl	%r15d, %r15d
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	(%rsp), %r10            # 8-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	movl	68(%rsp), %r11d         # 4-byte Reload
	movq	%rbp, %rdi
	je	.LBB0_41
	jmp	.LBB0_69
	.p2align	4, 0x90
.LBB0_54:                               # %.lr.ph1251.split.split.us
                                        #   Parent Loop BB0_40 Depth=1
                                        #     Parent Loop BB0_41 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	4(%rdi), %rax
	cmpq	%r9, %rax
	ja	.LBB0_173
# BB#55:                                # %.lr.ph1251.split.split.us
                                        #   in Loop: Header=BB0_54 Depth=3
	cmpq	%r10, %rax
	jbe	.LBB0_173
# BB#56:                                #   in Loop: Header=BB0_54 Depth=3
	movl	(%rdi), %r12d
	testl	%r12d, %r12d
	je	.LBB0_177
# BB#57:                                #   in Loop: Header=BB0_54 Depth=3
	movl	%r12d, %ecx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	cmpl	%ecx, %r12d
	je	.LBB0_48
# BB#58:                                #   in Loop: Header=BB0_54 Depth=3
	cmpq	%r10, %rax
	jb	.LBB0_173
# BB#59:                                #   in Loop: Header=BB0_54 Depth=3
	leaq	12(%rdi), %rbx
	cmpq	%r9, %rbx
	ja	.LBB0_173
# BB#60:                                #   in Loop: Header=BB0_54 Depth=3
	cmpq	%r10, %rbx
	jbe	.LBB0_173
# BB#61:                                #   in Loop: Header=BB0_54 Depth=3
	leal	-1(,%r12,4), %eax
	cmpl	%r15d, %eax
	jae	.LBB0_173
# BB#62:                                #   in Loop: Header=BB0_54 Depth=3
	movslq	4(%rdi), %rsi
	addq	%r13, %rsi
	leal	-4(,%r12,4), %eax
	subq	%rax, %rsi
	cmpq	%r10, %rsi
	jb	.LBB0_173
# BB#63:                                #   in Loop: Header=BB0_54 Depth=3
	movslq	8(%rdi), %rbp
	leaq	(%r13,%rbp), %rdi
	subq	%rax, %rdi
	cmpq	%r10, %rdi
	jb	.LBB0_173
# BB#64:                                #   in Loop: Header=BB0_54 Depth=3
	shll	$2, %r12d
	movl	%r12d, %edx
	leaq	(%rsi,%rdx), %rax
	cmpq	%r10, %rax
	jbe	.LBB0_173
# BB#65:                                #   in Loop: Header=BB0_54 Depth=3
	cmpq	%r9, %rax
	ja	.LBB0_173
# BB#66:                                #   in Loop: Header=BB0_54 Depth=3
	leaq	(%rdi,%rdx), %rax
	cmpq	%r9, %rax
	ja	.LBB0_173
# BB#67:                                #   in Loop: Header=BB0_54 Depth=3
	cmpq	%r10, %rax
	jbe	.LBB0_173
# BB#68:                                # %.thread939.us1272
                                        #   in Loop: Header=BB0_54 Depth=3
	addl	$4, %ebp
	callq	memmove
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	(%rsp), %r10            # 8-byte Reload
	xorl	%r11d, %r11d
	cmpq	%r10, %rbx
	movq	%rbx, %rdi
	jae	.LBB0_54
	jmp	.LBB0_173
.LBB0_69:                               #   in Loop: Header=BB0_40 Depth=1
	movq	%r15, 16(%rsp)          # 8-byte Spill
	leaq	(%r14,%rax,4), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%r12d, %r8d
	testl	%r11d, %r11d
	movq	232(%rsp), %rbx         # 8-byte Reload
	movq	%rdi, %r15
	jne	.LBB0_76
# BB#70:                                #   in Loop: Header=BB0_40 Depth=1
	cmpl	$0, 184(%rsp)           # 4-byte Folded Reload
	je	.LBB0_76
# BB#71:                                # %.lr.ph1275.preheader
                                        #   in Loop: Header=BB0_40 Depth=1
	movq	176(%rsp), %rax         # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_72:                               # %.lr.ph1275
                                        #   Parent Loop BB0_40 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %ecx
	cmpl	%ecx, %r13d
	jbe	.LBB0_74
# BB#73:                                #   in Loop: Header=BB0_72 Depth=2
	movl	4(%rax), %edi
	addl	%ecx, %edi
	cmpl	%edi, %r13d
	jb	.LBB0_75
.LBB0_74:                               #   in Loop: Header=BB0_72 Depth=2
	incq	%rdx
	addq	$36, %rax
	cmpq	%rbx, %rdx
	jb	.LBB0_72
	jmp	.LBB0_76
.LBB0_75:                               #   in Loop: Header=BB0_40 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%ecx, (%rdi)
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	(%r13,%rcx), %edx
	subl	(%rax), %edx
	movl	%edx, 12(%rdi)
.LBB0_76:                               # %.loopexit979
                                        #   in Loop: Header=BB0_40 Depth=1
	addq	80(%rsp), %r8           # 8-byte Folded Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	cmpl	$65536, %edx            # imm = 0x10000
	jae	.LBB0_78
# BB#77:                                #   in Loop: Header=BB0_40 Depth=1
	movl	$5, %edi
	movl	$-928, %edx             # imm = 0xFC60
	movl	$-16288, %ecx           # imm = 0xC060
	jmp	.LBB0_79
.LBB0_78:                               #   in Loop: Header=BB0_40 Depth=1
	cmpl	$262144, %edx           # imm = 0x40000
	movl	$-32000, %ecx           # imm = 0x8300
	movl	$-32384, %eax           # imm = 0x8180
	cmovbl	%eax, %ecx
	movq	%rdx, %rbx
	movl	$-1280, %edx            # imm = 0xFB00
	movl	$-1664, %eax            # imm = 0xF980
	cmovbl	%eax, %edx
	xorl	%edi, %edi
	cmpl	$262143, %ebx           # imm = 0x3FFFF
	seta	%dil
	addl	$7, %edi
.LBB0_79:                               #   in Loop: Header=BB0_40 Depth=1
	cmpq	%r10, %r8
	setb	%al
	orb	38(%rsp), %al           # 1-byte Folded Reload
	jne	.LBB0_191
# BB#80:                                #   in Loop: Header=BB0_40 Depth=1
	leaq	1(%r8), %r12
	cmpq	%r10, %r12
	jbe	.LBB0_191
# BB#81:                                #   in Loop: Header=BB0_40 Depth=1
	cmpq	%r9, %r12
	ja	.LBB0_191
# BB#82:                                #   in Loop: Header=BB0_40 Depth=1
	addq	80(%rsp), %r13          # 8-byte Folded Reload
	cmpq	%r10, %r13
	jb	.LBB0_191
# BB#83:                                #   in Loop: Header=BB0_40 Depth=1
	leaq	1(%r13), %rbx
	cmpq	%r9, %rbx
	ja	.LBB0_191
# BB#84:                                #   in Loop: Header=BB0_40 Depth=1
	cmpq	%r10, %rbx
	jbe	.LBB0_191
# BB#85:                                #   in Loop: Header=BB0_40 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	decl	%eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movb	(%r8), %al
	movb	%al, (%r13)
	je	.LBB0_10
# BB#86:                                # %.lr.ph1280.preheader
                                        #   in Loop: Header=BB0_40 Depth=1
	movl	%edi, 152(%rsp)         # 4-byte Spill
	movl	%edx, 144(%rsp)         # 4-byte Spill
	movl	%ecx, 148(%rsp)         # 4-byte Spill
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
.LBB0_87:                               # %.lr.ph1280
                                        #   Parent Loop BB0_40 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_88 Depth 3
                                        #       Child Loop BB0_102 Depth 3
                                        #       Child Loop BB0_114 Depth 3
                                        #       Child Loop BB0_133 Depth 3
                                        #       Child Loop BB0_159 Depth 3
                                        #       Child Loop BB0_163 Depth 3
                                        #       Child Loop BB0_168 Depth 3
                                        #       Child Loop BB0_171 Depth 3
	movq	%rbx, %rbp
	movl	16(%rsp), %esi          # 4-byte Reload
	xorl	%edx, %edx
	xorl	%r13d, %r13d
	movq	%r12, %rdi
.LBB0_88:                               #   Parent Loop BB0_40 Depth=1
                                        #     Parent Loop BB0_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	$127, %r9b
	je	.LBB0_90
# BB#89:                                #   in Loop: Header=BB0_88 Depth=3
	movl	%r9d, %ebx
	addb	%bl, %bl
	jmp	.LBB0_93
.LBB0_90:                               #   in Loop: Header=BB0_88 Depth=3
	cmpq	%r10, %rdi
	jb	.LBB0_221
# BB#91:                                #   in Loop: Header=BB0_88 Depth=3
	cmpq	72(%rsp), %rdi          # 8-byte Folded Reload
	jae	.LBB0_221
# BB#92:                                #   in Loop: Header=BB0_88 Depth=3
	movb	(%rdi), %r9b
	movl	%r9d, %ebx
	addb	%bl, %bl
	orb	$1, %bl
	incq	%rdi
.LBB0_93:                               # %doubledl.exit
                                        #   in Loop: Header=BB0_88 Depth=3
	movq	%rdi, %r12
	testb	%r9b, %r9b
	js	.LBB0_101
# BB#94:                                #   in Loop: Header=BB0_88 Depth=3
	cmpq	%r10, %r12
	jb	.LBB0_221
# BB#95:                                #   in Loop: Header=BB0_88 Depth=3
	cmpq	%r10, %rbp
	jb	.LBB0_221
# BB#96:                                #   in Loop: Header=BB0_88 Depth=3
	leaq	1(%r12), %rdi
	cmpq	%r10, %rdi
	jbe	.LBB0_221
# BB#97:                                #   in Loop: Header=BB0_88 Depth=3
	cmpq	40(%rsp), %rdi          # 8-byte Folded Reload
	ja	.LBB0_221
# BB#98:                                #   in Loop: Header=BB0_88 Depth=3
	leaq	1(%rbp,%r13), %rax
	cmpq	40(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_221
# BB#99:                                #   in Loop: Header=BB0_88 Depth=3
	cmpq	%r10, %rax
	jbe	.LBB0_221
# BB#100:                               #   in Loop: Header=BB0_88 Depth=3
	leaq	(%rsi,%rdx), %rax
	movzbl	(%r12), %ecx
	xorl	%ecx, %eax
	movb	%al, (%rbp,%r13)
	incq	%r13
	decq	%rdx
	cmpl	%r13d, %esi
	movb	%bl, %r9b
	jne	.LBB0_88
	jmp	.LBB0_8
.LBB0_101:                              # %.preheader976.preheader
                                        #   in Loop: Header=BB0_87 Depth=2
	leaq	(%rbp,%r13), %r10
	movl	$1, %edx
	movl	152(%rsp), %edi         # 4-byte Reload
.LBB0_102:                              # %.preheader976
                                        #   Parent Loop BB0_40 Depth=1
                                        #     Parent Loop BB0_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	$127, %bl
	je	.LBB0_104
# BB#103:                               #   in Loop: Header=BB0_102 Depth=3
	movl	%ebx, %ecx
	addb	%cl, %cl
	movl	%ebx, %eax
	testb	$127, %cl
	jne	.LBB0_107
	jmp	.LBB0_108
.LBB0_104:                              #   in Loop: Header=BB0_102 Depth=3
	cmpq	(%rsp), %r12            # 8-byte Folded Reload
	jb	.LBB0_221
# BB#105:                               #   in Loop: Header=BB0_102 Depth=3
	cmpq	72(%rsp), %r12          # 8-byte Folded Reload
	jae	.LBB0_221
# BB#106:                               #   in Loop: Header=BB0_102 Depth=3
	movzbl	(%r12), %eax
	movl	%eax, %ecx
	addb	%cl, %cl
	orb	$1, %cl
	incq	%r12
	testb	$127, %cl
	je	.LBB0_108
.LBB0_107:                              #   in Loop: Header=BB0_102 Depth=3
	movl	%ecx, %ebx
	addb	%bl, %bl
	jmp	.LBB0_111
.LBB0_108:                              #   in Loop: Header=BB0_102 Depth=3
	cmpq	(%rsp), %r12            # 8-byte Folded Reload
	jb	.LBB0_221
# BB#109:                               #   in Loop: Header=BB0_102 Depth=3
	cmpq	72(%rsp), %r12          # 8-byte Folded Reload
	jae	.LBB0_221
# BB#110:                               #   in Loop: Header=BB0_102 Depth=3
	movzbl	(%r12), %ecx
	movl	%ecx, %ebx
	addb	%bl, %bl
	orb	$1, %bl
	incq	%r12
.LBB0_111:                              # %doubledl.exit860
                                        #   in Loop: Header=BB0_102 Depth=3
	movzbl	%al, %eax
	shrl	$7, %eax
	leal	(%rax,%rdx,2), %edx
	testb	%cl, %cl
	js	.LBB0_102
# BB#112:                               #   in Loop: Header=BB0_87 Depth=2
	movq	%rsi, %r11
	movl	%edx, %esi
	addl	$-3, %esi
	js	.LBB0_121
# BB#113:                               # %.preheader975.preheader
                                        #   in Loop: Header=BB0_87 Depth=2
	movl	%edi, %edx
.LBB0_114:                              # %.preheader975
                                        #   Parent Loop BB0_40 Depth=1
                                        #     Parent Loop BB0_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	$127, %bl
	je	.LBB0_116
# BB#115:                               #   in Loop: Header=BB0_114 Depth=3
	movl	%ebx, %ecx
	addb	%cl, %cl
	jmp	.LBB0_119
.LBB0_116:                              #   in Loop: Header=BB0_114 Depth=3
	cmpq	(%rsp), %r12            # 8-byte Folded Reload
	jb	.LBB0_221
# BB#117:                               #   in Loop: Header=BB0_114 Depth=3
	cmpq	72(%rsp), %r12          # 8-byte Folded Reload
	jae	.LBB0_221
# BB#118:                               #   in Loop: Header=BB0_114 Depth=3
	movzbl	(%r12), %ebx
	movl	%ebx, %ecx
	addb	%cl, %cl
	orb	$1, %cl
	incq	%r12
.LBB0_119:                              # %doubledl.exit856
                                        #   in Loop: Header=BB0_114 Depth=3
	movzbl	%bl, %eax
	shrl	$7, %eax
	leal	(%rax,%rsi,2), %esi
	decl	%edx
	movl	%ecx, %ebx
	jne	.LBB0_114
# BB#120:                               #   in Loop: Header=BB0_87 Depth=2
	notl	%esi
	xorl	%eax, %eax
	cmpl	%esi, 144(%rsp)         # 4-byte Folded Reload
	setg	%al
	xorl	%edx, %edx
	cmpl	%esi, 148(%rsp)         # 4-byte Folded Reload
	setg	%dl
	leal	1(%rax,%rdx), %edi
	xorl	%edx, %edx
	movl	%esi, %r8d
	testb	$127, %cl
	jne	.LBB0_122
	jmp	.LBB0_123
.LBB0_121:                              #   in Loop: Header=BB0_87 Depth=2
	addl	$-2, %edx
	xorl	%edi, %edi
	movl	%ebx, %ecx
	testb	$127, %cl
	je	.LBB0_123
.LBB0_122:                              #   in Loop: Header=BB0_87 Depth=2
	movl	%ecx, %eax
	addb	%al, %al
	testb	$127, %al
	jne	.LBB0_126
	jmp	.LBB0_127
.LBB0_123:                              #   in Loop: Header=BB0_87 Depth=2
	cmpq	(%rsp), %r12            # 8-byte Folded Reload
	jb	.LBB0_221
# BB#124:                               #   in Loop: Header=BB0_87 Depth=2
	cmpq	72(%rsp), %r12          # 8-byte Folded Reload
	jae	.LBB0_221
# BB#125:                               #   in Loop: Header=BB0_87 Depth=2
	movb	(%r12), %cl
	movl	%ecx, %eax
	addb	%al, %al
	orb	$1, %al
	incq	%r12
	testb	$127, %al
	je	.LBB0_127
.LBB0_126:                              #   in Loop: Header=BB0_87 Depth=2
	movb	%al, %r9b
	addb	%r9b, %r9b
	jmp	.LBB0_130
.LBB0_127:                              #   in Loop: Header=BB0_87 Depth=2
	cmpq	(%rsp), %r12            # 8-byte Folded Reload
	jb	.LBB0_221
# BB#128:                               #   in Loop: Header=BB0_87 Depth=2
	cmpq	72(%rsp), %r12          # 8-byte Folded Reload
	jae	.LBB0_221
# BB#129:                               #   in Loop: Header=BB0_87 Depth=2
	movb	(%r12), %al
	movb	%al, %r9b
	addb	%r9b, %r9b
	orb	$1, %r9b
	incq	%r12
.LBB0_130:                              # %doubledl.exit848
                                        #   in Loop: Header=BB0_87 Depth=2
	movzbl	%cl, %ecx
	shrl	$7, %ecx
	movzbl	%al, %esi
	shrl	$7, %esi
	leal	(%rcx,%rdx,2), %eax
	addl	%eax, %eax
	orl	%esi, %eax
	je	.LBB0_132
# BB#131:                               #   in Loop: Header=BB0_87 Depth=2
	movl	%r8d, 92(%rsp)          # 4-byte Spill
	jmp	.LBB0_144
.LBB0_132:                              # %.preheader974.preheader
                                        #   in Loop: Header=BB0_87 Depth=2
	movl	$1, %eax
.LBB0_133:                              # %.preheader974
                                        #   Parent Loop BB0_40 Depth=1
                                        #     Parent Loop BB0_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	$127, %r9b
	je	.LBB0_135
# BB#134:                               #   in Loop: Header=BB0_133 Depth=3
	movl	%r9d, %edx
	addb	%dl, %dl
	movl	%r9d, %ecx
	testb	$127, %dl
	jne	.LBB0_138
	jmp	.LBB0_139
.LBB0_135:                              #   in Loop: Header=BB0_133 Depth=3
	cmpq	(%rsp), %r12            # 8-byte Folded Reload
	jb	.LBB0_221
# BB#136:                               #   in Loop: Header=BB0_133 Depth=3
	cmpq	72(%rsp), %r12          # 8-byte Folded Reload
	jae	.LBB0_221
# BB#137:                               #   in Loop: Header=BB0_133 Depth=3
	movzbl	(%r12), %ecx
	movl	%ecx, %edx
	addb	%dl, %dl
	orb	$1, %dl
	incq	%r12
	testb	$127, %dl
	je	.LBB0_139
.LBB0_138:                              #   in Loop: Header=BB0_133 Depth=3
	movb	%dl, %r9b
	addb	%r9b, %r9b
	jmp	.LBB0_142
.LBB0_139:                              #   in Loop: Header=BB0_133 Depth=3
	cmpq	(%rsp), %r12            # 8-byte Folded Reload
	jb	.LBB0_221
# BB#140:                               #   in Loop: Header=BB0_133 Depth=3
	cmpq	72(%rsp), %r12          # 8-byte Folded Reload
	jae	.LBB0_221
# BB#141:                               #   in Loop: Header=BB0_133 Depth=3
	movzbl	(%r12), %edx
	movb	%dl, %r9b
	addb	%r9b, %r9b
	orb	$1, %r9b
	incq	%r12
.LBB0_142:                              # %doubledl.exit840
                                        #   in Loop: Header=BB0_133 Depth=3
	movzbl	%cl, %ecx
	shrl	$7, %ecx
	leal	(%rcx,%rax,2), %eax
	testb	%dl, %dl
	js	.LBB0_133
# BB#143:                               #   in Loop: Header=BB0_87 Depth=2
	movl	%r8d, 92(%rsp)          # 4-byte Spill
	addl	$2, %eax
.LBB0_144:                              #   in Loop: Header=BB0_87 Depth=2
	leal	(%rax,%rdi), %edx
	testl	%edx, %edx
	setle	%cl
	cmpq	(%rsp), %r10            # 8-byte Folded Reload
	jb	.LBB0_221
# BB#145:                               #   in Loop: Header=BB0_87 Depth=2
	cmpl	52(%rsp), %edx          # 4-byte Folded Reload
	ja	.LBB0_221
# BB#146:                               #   in Loop: Header=BB0_87 Depth=2
	testb	%cl, %cl
	jne	.LBB0_221
# BB#147:                               #   in Loop: Header=BB0_87 Depth=2
	movslq	%edx, %rcx
	leaq	(%rbp,%rcx), %rsi
	addq	%r13, %rsi
	cmpq	40(%rsp), %rsi          # 8-byte Folded Reload
	ja	.LBB0_221
# BB#148:                               #   in Loop: Header=BB0_87 Depth=2
	testl	%edx, %edx
	jle	.LBB0_221
# BB#149:                               #   in Loop: Header=BB0_87 Depth=2
	cmpq	(%rsp), %rsi            # 8-byte Folded Reload
	jbe	.LBB0_221
# BB#150:                               #   in Loop: Header=BB0_87 Depth=2
	movslq	92(%rsp), %r8           # 4-byte Folded Reload
	leaq	(%rbp,%r8), %rsi
	addq	%r13, %rsi
	cmpq	(%rsp), %rsi            # 8-byte Folded Reload
	jb	.LBB0_221
# BB#151:                               #   in Loop: Header=BB0_87 Depth=2
	addq	%r8, %rcx
	addq	%rbp, %rcx
	addq	%r13, %rcx
	cmpq	40(%rsp), %rcx          # 8-byte Folded Reload
	ja	.LBB0_221
# BB#152:                               #   in Loop: Header=BB0_87 Depth=2
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	jbe	.LBB0_221
# BB#153:                               # %.lr.ph1286.preheader
                                        #   in Loop: Header=BB0_87 Depth=2
	movl	%eax, %ecx
	negl	%ecx
	subl	%edi, %ecx
	addq	%r11, %rcx
	subq	%r13, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%edx, %r14d
	negl	%r14d
	addq	%r11, %r14
	movq	%rdi, 216(%rsp)         # 8-byte Spill
	leal	-1(%rax,%rdi), %edi
	leaq	1(%rdi), %rbx
	cmpq	$31, %rbx
	jbe	.LBB0_166
# BB#154:                               # %min.iters.checked
                                        #   in Loop: Header=BB0_87 Depth=2
	movq	%rbx, %r11
	movabsq	$8589934560, %rcx       # imm = 0x1FFFFFFE0
	andq	%rcx, %r11
	je	.LBB0_166
# BB#155:                               # %vector.memcheck
                                        #   in Loop: Header=BB0_87 Depth=2
	leaq	(%r8,%rdi), %rcx
	addq	%rbp, %rcx
	leaq	1(%r13,%rcx), %rcx
	cmpq	%rcx, %r10
	jae	.LBB0_157
# BB#156:                               # %vector.memcheck
                                        #   in Loop: Header=BB0_87 Depth=2
	leaq	(%rbp,%rdi), %rcx
	leaq	1(%r13,%rcx), %rcx
	cmpq	%rcx, %rsi
	jb	.LBB0_166
.LBB0_157:                              # %vector.body.preheader
                                        #   in Loop: Header=BB0_87 Depth=2
	movq	%rbx, 200(%rsp)         # 8-byte Spill
	movq	%rdi, 208(%rsp)         # 8-byte Spill
	leaq	-32(%r11), %rsi
	movq	%rsi, 192(%rsp)         # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_160
# BB#158:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_87 Depth=2
	leaq	16(%rbp,%r8), %rbx
	leaq	16(%rbp,%r13), %rdi
	negq	%rsi
	xorl	%r10d, %r10d
.LBB0_159:                              # %vector.body.prol
                                        #   Parent Loop BB0_40 Depth=1
                                        #     Parent Loop BB0_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%rbx,%r10), %rcx
	movups	-16(%r13,%rcx), %xmm0
	movups	(%r13,%rcx), %xmm1
	movups	%xmm0, -16(%rdi,%r10)
	movups	%xmm1, (%rdi,%r10)
	addq	$32, %r10
	incq	%rsi
	jne	.LBB0_159
	jmp	.LBB0_161
.LBB0_160:                              #   in Loop: Header=BB0_87 Depth=2
	xorl	%r10d, %r10d
.LBB0_161:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_87 Depth=2
	cmpq	$96, 192(%rsp)          # 8-byte Folded Reload
	jb	.LBB0_164
# BB#162:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_87 Depth=2
	leaq	(%rbp,%r10), %rcx
	leaq	112(%r13,%rcx), %rsi
	movq	%r11, %rbx
	subq	%r10, %rbx
.LBB0_163:                              # %vector.body
                                        #   Parent Loop BB0_40 Depth=1
                                        #     Parent Loop BB0_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%r8,%rsi), %xmm0
	movups	-96(%r8,%rsi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%r8,%rsi), %xmm0
	movups	-64(%r8,%rsi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%r8,%rsi), %xmm0
	movups	-32(%r8,%rsi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%r8,%rsi), %xmm0
	movups	(%r8,%rsi), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-128, %rbx
	jne	.LBB0_163
.LBB0_164:                              # %middle.block
                                        #   in Loop: Header=BB0_87 Depth=2
	cmpq	%r11, 200(%rsp)         # 8-byte Folded Reload
	movq	208(%rsp), %rdi         # 8-byte Reload
	je	.LBB0_172
# BB#165:                               #   in Loop: Header=BB0_87 Depth=2
	movq	%r11, %r10
	addq	%rbp, %r10
	subl	%r11d, %edx
	addq	%r13, %r10
.LBB0_166:                              # %.lr.ph1286.preheader1919
                                        #   in Loop: Header=BB0_87 Depth=2
	leal	-1(%rdx), %esi
	movl	%edx, %ebx
	andl	$7, %ebx
	je	.LBB0_169
# BB#167:                               # %.lr.ph1286.prol.preheader
                                        #   in Loop: Header=BB0_87 Depth=2
	negl	%ebx
.LBB0_168:                              # %.lr.ph1286.prol
                                        #   Parent Loop BB0_40 Depth=1
                                        #     Parent Loop BB0_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	decl	%edx
	movzbl	(%r8,%r10), %ecx
	movb	%cl, (%r10)
	incq	%r10
	incl	%ebx
	jne	.LBB0_168
.LBB0_169:                              # %.lr.ph1286.prol.loopexit
                                        #   in Loop: Header=BB0_87 Depth=2
	cmpl	$7, %esi
	jb	.LBB0_172
# BB#170:                               # %.lr.ph1286.preheader1919.new
                                        #   in Loop: Header=BB0_87 Depth=2
	addq	$7, %r10
.LBB0_171:                              # %.lr.ph1286
                                        #   Parent Loop BB0_40 Depth=1
                                        #     Parent Loop BB0_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	-7(%r8,%r10), %ecx
	movb	%cl, -7(%r10)
	movzbl	-6(%r8,%r10), %ecx
	movb	%cl, -6(%r10)
	movzbl	-5(%r8,%r10), %ecx
	movb	%cl, -5(%r10)
	movzbl	-4(%r8,%r10), %ecx
	movb	%cl, -4(%r10)
	movzbl	-3(%r8,%r10), %ecx
	movb	%cl, -3(%r10)
	movzbl	-2(%r8,%r10), %ecx
	movb	%cl, -2(%r10)
	movzbl	-1(%r8,%r10), %ecx
	movb	%cl, -1(%r10)
	addl	$-8, %edx
	movzbl	(%r8,%r10), %ecx
	movb	%cl, (%r10)
	leaq	8(%r10), %r10
	jne	.LBB0_171
.LBB0_172:                              # %.loopexit973
                                        #   in Loop: Header=BB0_87 Depth=2
	addq	%rbp, %rdi
	leaq	1(%r13,%rdi), %rbx
	cmpl	%r13d, %r14d
	movq	(%rsp), %r10            # 8-byte Reload
	movl	68(%rsp), %r11d         # 4-byte Reload
	movl	92(%rsp), %r8d          # 4-byte Reload
	jne	.LBB0_87
	jmp	.LBB0_7
.LBB0_173:                              # %.thread939.outer._crit_edge
	movl	$1, %r14d
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB0_176
.LBB0_174:
	movq	16(%rsp), %rdi          # 8-byte Reload
.LBB0_175:                              # %.thread925
	callq	free
.LBB0_176:                              # %.thread925
	movl	%r14d, %eax
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_177:                              # %.us-lcssa.us
	movl	$1, %r14d
	movq	56(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	movq	240(%rsp), %r13         # 8-byte Reload
	jle	.LBB0_176
# BB#178:                               # %.preheader971
	leal	-1(%rax), %r8d
	movslq	%r8d, %rcx
	movl	132(%rsp), %r15d        # 4-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_179:                              # %.preheader970
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_181 Depth 2
                                        #       Child Loop BB0_182 Depth 3
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.LBB0_181
	.p2align	4, 0x90
.LBB0_180:                              #   in Loop: Header=BB0_181 Depth=2
	movl	-32(%rdi), %esi
	movl	-24(%rdi), %eax
	movl	%ebx, -36(%rdi)
	movl	12(%rdi), %ebx
	movl	%ebx, -24(%rdi)
	movl	4(%rdi), %ebx
	movl	%ebx, -32(%rdi)
	movl	%ebp, (%rdi)
	movl	%eax, 12(%rdi)
	movl	%esi, 4(%rdi)
	incq	%rdx
	movl	$1, %esi
.LBB0_181:                              # %.outer
                                        #   Parent Loop BB0_179 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_182 Depth 3
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,8), %rax
	leaq	(%r12,%rax,4), %rdi
	decq	%rdx
	.p2align	4, 0x90
.LBB0_182:                              #   Parent Loop BB0_179 Depth=1
                                        #     Parent Loop BB0_181 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incq	%rdx
	cmpq	%rcx, %rdx
	jge	.LBB0_184
# BB#183:                               #   in Loop: Header=BB0_182 Depth=3
	movl	(%rdi), %ebp
	movl	36(%rdi), %ebx
	leaq	36(%rdi), %rdi
	cmpl	%ebx, %ebp
	jbe	.LBB0_182
	jmp	.LBB0_180
	.p2align	4, 0x90
.LBB0_184:                              # %.loopexit
                                        #   in Loop: Header=BB0_179 Depth=1
	testl	%esi, %esi
	jne	.LBB0_179
# BB#185:                               # %.preheader
	movq	56(%rsp), %rbx          # 8-byte Reload
	cmpl	$2, %ebx
	jl	.LBB0_201
# BB#186:                               # %.lr.ph1245.preheader
	movl	(%r12), %edx
	testb	$1, %cl
	jne	.LBB0_192
# BB#187:
	xorl	%ecx, %ecx
	cmpl	$1, %r8d
	jne	.LBB0_195
	jmp	.LBB0_201
.LBB0_188:
	movl	$1, %r14d
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	jne	.LBB0_175
	jmp	.LBB0_176
.LBB0_190:
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_191:
	movq	%r14, %rdi
	jmp	.LBB0_222
.LBB0_192:                              # %.lr.ph1245.prol
	movl	36(%r12), %esi
	movl	%esi, %ecx
	subl	%edx, %ecx
	cmpl	%ecx, 4(%r12)
	je	.LBB0_194
# BB#193:
	movl	%ecx, 4(%r12)
.LBB0_194:                              # %.lr.ph1245.prol.loopexit
	movl	$1, %ecx
	movl	%esi, %edx
	cmpl	$1, %r8d
	je	.LBB0_201
.LBB0_195:                              # %.lr.ph1245.preheader.new
	movl	8(%rsp), %eax           # 4-byte Reload
	subq	%rcx, %rax
	leaq	(%rcx,%rcx,8), %rcx
	leaq	4(%r12,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB0_196:                              # %.lr.ph1245
                                        # =>This Inner Loop Header: Depth=1
	movl	32(%rcx), %esi
	movl	%esi, %edi
	subl	%edx, %edi
	cmpl	%edi, (%rcx)
	je	.LBB0_198
# BB#197:                               #   in Loop: Header=BB0_196 Depth=1
	movl	%edi, (%rcx)
.LBB0_198:                              # %.backedge
                                        #   in Loop: Header=BB0_196 Depth=1
	movl	68(%rcx), %edx
	movl	%edx, %edi
	subl	%esi, %edi
	cmpl	%edi, 36(%rcx)
	je	.LBB0_200
# BB#199:                               #   in Loop: Header=BB0_196 Depth=1
	movl	%edi, 36(%rcx)
.LBB0_200:                              # %.backedge.1
                                        #   in Loop: Header=BB0_196 Depth=1
	addq	$72, %rcx
	addq	$-2, %rax
	jne	.LBB0_196
.LBB0_201:                              # %._crit_edge1246
	testl	%r15d, %r15d
	je	.LBB0_220
# BB#202:
	movl	304(%rsp), %eax
	addl	$5, %eax
	cmpl	$2, 320(%rsp)
	jne	.LBB0_235
# BB#203:
	movq	%rax, %r11
	movl	128(%rsp), %edi         # 4-byte Reload
	addq	80(%rsp), %rdi          # 8-byte Folded Reload
	cmpq	%r10, %rdi
	jb	.LBB0_233
# BB#204:                               # %.lr.ph1228
	leal	(%r11,%r13), %edx
	movq	184(%rsp), %rax         # 8-byte Reload
	decl	%eax
	cmpl	$0, 124(%rsp)           # 4-byte Folded Reload
	leaq	(%rax,%rax,8), %rax
	movq	176(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,4), %rax
	je	.LBB0_223
# BB#205:                               # %.lr.ph1228.split.us.preheader
	xorl	%ebx, %ebx
.LBB0_206:                              # %.lr.ph1228.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_210 Depth 2
	leaq	4(%rdi), %r8
	cmpq	%r9, %r8
	ja	.LBB0_233
# BB#207:                               # %.lr.ph1228.split.us
                                        #   in Loop: Header=BB0_206 Depth=1
	cmpq	%r10, %r8
	jbe	.LBB0_233
# BB#208:                               #   in Loop: Header=BB0_206 Depth=1
	movslq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB0_234
# BB#209:                               #   in Loop: Header=BB0_206 Depth=1
	addq	80(%rsp), %rdi          # 8-byte Folded Reload
	cmpq	%r10, %rdi
	jb	.LBB0_233
.LBB0_210:                              # %.lr.ph1220.split.us.us
                                        #   Parent Loop BB0_206 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	4(%rdi), %rbp
	cmpq	%r9, %rbp
	ja	.LBB0_233
# BB#211:                               # %.lr.ph1220.split.us.us
                                        #   in Loop: Header=BB0_210 Depth=2
	cmpq	%r10, %rbp
	jbe	.LBB0_233
# BB#212:                               #   in Loop: Header=BB0_210 Depth=2
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	je	.LBB0_219
# BB#213:                               #   in Loop: Header=BB0_210 Depth=2
	movl	%ecx, %esi
	orl	$-2147483648, %esi      # imm = 0x80000000
	movl	$-1074785481, %edi      # imm = 0xBFF01337
	cmpl	%esi, %ecx
	jne	.LBB0_215
# BB#214:                               #   in Loop: Header=BB0_210 Depth=2
	movl	%edx, %ecx
	jmp	.LBB0_218
.LBB0_215:                              #   in Loop: Header=BB0_210 Depth=2
	testl	%ebx, %ebx
	jle	.LBB0_217
# BB#216:                               #   in Loop: Header=BB0_210 Depth=2
	decl	%ebx
	movl	%edx, %ecx
	jmp	.LBB0_218
.LBB0_217:                              #   in Loop: Header=BB0_210 Depth=2
	leal	5(%rdx), %ecx
	movl	%ecx, %ebx
	andl	$7, %ebx
	movl	%edx, %edi
.LBB0_218:                              #   in Loop: Header=BB0_210 Depth=2
	movl	(%rax), %edx
	addl	%r13d, %edx
	cmpl	%edi, %edx
	sbbl	%edx, %edx
	cmpl	%ecx, %edi
	sbbl	$0, %r15d
	addl	%edx, %r15d
	roll	$29, %r15d
	cmpq	%r10, %rbp
	movq	%rbp, %rdi
	movl	%ecx, %edx
	jae	.LBB0_210
	jmp	.LBB0_233
.LBB0_219:                              # %.critedge9.loopexit.us-lcssa.us.us
                                        #   in Loop: Header=BB0_206 Depth=1
	cmpq	%r10, %r8
	movq	%r8, %rdi
	jae	.LBB0_206
	jmp	.LBB0_233
.LBB0_220:
	xorl	%r15d, %r15d
	testl	%ebx, %ebx
	jg	.LBB0_236
	jmp	.LBB0_248
.LBB0_221:                              # %doubledl.exit.thread
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB0_222:                              # %.thread925
	callq	free
	movl	$1, %r14d
	jmp	.LBB0_176
.LBB0_223:                              # %.lr.ph1228.split.preheader
	xorl	%esi, %esi
	cmpl	$-1074785481, %edx      # imm = 0xBFF01337
	movl	$-1, %ecx
	cmovbel	%esi, %ecx
.LBB0_224:                              # %.lr.ph1228.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_228 Depth 2
	leaq	4(%rdi), %rdx
	cmpq	%r9, %rdx
	ja	.LBB0_233
# BB#225:                               # %.lr.ph1228.split
                                        #   in Loop: Header=BB0_224 Depth=1
	cmpq	%r10, %rdx
	jbe	.LBB0_233
# BB#226:                               #   in Loop: Header=BB0_224 Depth=1
	movslq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.LBB0_234
# BB#227:                               #   in Loop: Header=BB0_224 Depth=1
	addq	80(%rsp), %rsi          # 8-byte Folded Reload
	cmpq	%r10, %rsi
	jb	.LBB0_233
.LBB0_228:                              # %.lr.ph1220.split
                                        #   Parent Loop BB0_224 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	4(%rsi), %rdi
	cmpq	%r9, %rdi
	ja	.LBB0_233
# BB#229:                               # %.lr.ph1220.split
                                        #   in Loop: Header=BB0_228 Depth=2
	cmpq	%r10, %rdi
	jbe	.LBB0_233
# BB#230:                               #   in Loop: Header=BB0_228 Depth=2
	cmpl	$0, (%rsi)
	je	.LBB0_232
# BB#231:                               #   in Loop: Header=BB0_228 Depth=2
	movl	(%rax), %esi
	addl	%r13d, %esi
	cmpl	$-1074785481, %esi      # imm = 0xBFF01337
	sbbl	%esi, %esi
	addl	%ecx, %r15d
	addl	%esi, %r15d
	roll	$29, %r15d
	cmpq	%r10, %rdi
	movq	%rdi, %rsi
	jae	.LBB0_228
	jmp	.LBB0_233
.LBB0_232:                              # %.critedge9.loopexit
                                        #   in Loop: Header=BB0_224 Depth=1
	cmpq	%r10, %rdx
	movq	%rdx, %rdi
	jae	.LBB0_224
.LBB0_233:                              # %.critedge
	movl	(%r12), %r15d
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	cli_dbgmsg
	movq	56(%rsp), %rbx          # 8-byte Reload
	testl	%ebx, %ebx
	jg	.LBB0_236
	jmp	.LBB0_248
.LBB0_234:
	movq	%r11, %rax
	movq	56(%rsp), %rbx          # 8-byte Reload
.LBB0_235:                              # %.critedge.thread
	addl	%eax, %r15d
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	cli_dbgmsg
	testl	%ebx, %ebx
	jle	.LBB0_248
.LBB0_236:                              # %.lr.ph1213.preheader
	movl	%ebx, %r12d
	xorl	%ebp, %ebp
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	(%rsp), %r9             # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_237:                              # %.lr.ph1213
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbp, %rbp
	movl	$0, %eax
	jle	.LBB0_239
# BB#238:                               #   in Loop: Header=BB0_237 Depth=1
	movl	-24(%rbx), %eax
	addl	-28(%rbx), %eax
.LBB0_239:                              #   in Loop: Header=BB0_237 Depth=1
	movl	52(%rsp), %esi          # 4-byte Reload
	testl	%esi, %esi
	sete	%cl
	movl	%eax, 8(%rbx)
	movl	12(%rbx), %edx
	testq	%rdx, %rdx
	sete	%r8b
	leal	-1(%rdx), %edi
	cmpl	%esi, %edi
	jae	.LBB0_244
# BB#240:                               #   in Loop: Header=BB0_237 Depth=1
	orb	%r8b, %cl
	jne	.LBB0_244
# BB#241:                               #   in Loop: Header=BB0_237 Depth=1
	movl	%eax, %edi
	addq	%r9, %rdi
	leaq	(%rdi,%rdx), %rax
	cmpq	%r10, %rax
	ja	.LBB0_244
# BB#242:                               #   in Loop: Header=BB0_237 Depth=1
	cmpq	%r9, %rax
	jbe	.LBB0_244
# BB#243:                               #   in Loop: Header=BB0_237 Depth=1
	movl	(%rbx), %esi
	addq	80(%rsp), %rsi          # 8-byte Folded Reload
	callq	memmove
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	(%rsp), %r9             # 8-byte Reload
.LBB0_244:                              #   in Loop: Header=BB0_237 Depth=1
	incq	%rbp
	addq	$36, %rbx
	cmpq	%rbp, %r12
	jne	.LBB0_237
# BB#245:                               # %._crit_edge1214
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_249
# BB#246:                               # %.lr.ph.preheader
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	12(%rax), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_247:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbx), %edx
	movl	-8(%rbx), %ecx
	movl	-4(%rbx), %r8d
	movl	(%rbx), %r9d
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	incq	%rbp
	addq	$36, %rbx
	cmpq	%rbp, %r12
	jne	.LBB0_247
	jmp	.LBB0_249
.LBB0_248:                              # %._crit_edge1214.thread
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_249:                              # %._crit_edge
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r13d, %ecx
	movl	%r15d, %r8d
	movl	328(%rsp), %r9d
	movl	312(%rsp), %eax
	pushq	%rax
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movl	344(%rsp), %eax
	pushq	%rax
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	cli_rebuildpe
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB0_251
# BB#250:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	xorl	%r14d, %r14d
	jmp	.LBB0_176
.LBB0_251:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_174
.Lfunc_end0:
	.size	petite_inflate2x_1to9, .Lfunc_end0-petite_inflate2x_1to9
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Petite: Old EP: %x\n"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Petite: In troubles while attempting to decrypt old EP, using bogus %x\n"
	.size	.L.str.1, 72

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Petite: Sections dump:\n"
	.size	.L.str.2, 24

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Petite: .SECT%d RVA:%x VSize:%x ROffset: %x, RSize:%x\n"
	.size	.L.str.3, 55

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Petite: Rebuilding failed\n"
	.size	.L.str.4, 27

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Petite: maximum number of sections exceeded, giving up.\n"
	.size	.L.str.5, 57

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Petite: Found petite code in sect%d(%x). Let's strip it.\n"
	.size	.L.str.6, 58

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Petite: Encrypted EP: %x | Array of imports: %x\n"
	.size	.L.str.7, 49


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
