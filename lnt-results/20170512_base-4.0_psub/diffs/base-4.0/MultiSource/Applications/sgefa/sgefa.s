	.text
	.file	"sgefa.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	3212836864              # float -1
.LCPI0_1:
	.long	0                       # float 0
	.text
	.globl	sgefa
	.p2align	4, 0x90
	.type	sgefa,@function
sgefa:                                  # @sgefa
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movl	(%r12), %r13d
	movl	$-1, %eax
	cmpl	4(%r12), %r13d
	jne	.LBB0_73
# BB#1:
	leal	-1(%r13), %eax
	movq	8(%r12), %r15
	xorl	%ecx, %ecx
	cmpl	$2, %r13d
	jl	.LBB0_72
# BB#2:                                 # %.lr.ph140.preheader
	movslq	%r13d, %rbp
	leal	-2(%r13), %ecx
	incq	%rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movl	%eax, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	leal	-1(%r13), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	leal	-1(%r13), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	leal	-1(%r13), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	$1, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	movq	%rsi, %rbx
	movl	$0, 24(%rsp)            # 4-byte Folded Spill
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	jmp	.LBB0_3
.LBB0_16:                               # %.lr.ph126.split.split.us.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	28(%rsp), %eax          # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	subl	%r8d, %eax
	incq	%rax
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	%edx, %r11d
	subl	%r8d, %r11d
	addq	%r14, %r11
	leaq	8(%r15,%r11,4), %rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movabsq	$8589934584, %rdx       # imm = 0x1FFFFFFF8
	andq	%rdx, %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leaq	-8(%rcx), %rsi
	shrq	$3, %rsi
	movq	%rax, %rbx
	andq	%rdx, %rbx
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rbx,4), %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	leal	(%rbx,%r8), %edx
	movl	%edx, 40(%rsp)          # 4-byte Spill
	movq	%rsi, 80(%rsp)          # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	andl	$1, %esi
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	leaq	52(%r15,%r14,4), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	96(%rsp), %r15          # 8-byte Reload
	jmp	.LBB0_17
.LBB0_53:                               # %vector.ph211
                                        #   in Loop: Header=BB0_17 Depth=2
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_54
# BB#55:                                # %vector.body188.prol
                                        #   in Loop: Header=BB0_17 Depth=2
	movups	(%rcx), %xmm2
	movups	16(%rcx), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	4(%r10), %xmm4
	movups	20(%r10), %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, 4(%r10)
	movups	%xmm5, 20(%r10)
	movl	$8, %esi
	cmpq	$0, 80(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_57
	jmp	.LBB0_59
.LBB0_54:                               #   in Loop: Header=BB0_17 Depth=2
	xorl	%esi, %esi
	cmpq	$0, 80(%rsp)            # 8-byte Folded Reload
	je	.LBB0_59
.LBB0_57:                               # %vector.ph211.new
                                        #   in Loop: Header=BB0_17 Depth=2
	movq	72(%rsp), %rbp          # 8-byte Reload
	subq	%rsi, %rbp
	movq	56(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rsi,4), %rdi
	leaq	(%rdx,%r14,4), %rdx
	leaq	52(%rdx,%rsi,4), %rdx
	.p2align	4, 0x90
.LBB0_58:                               # %vector.body188
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-48(%rdi), %xmm2
	movups	-32(%rdi), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	-48(%rdx), %xmm4
	movups	-32(%rdx), %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, -48(%rdx)
	movups	%xmm5, -32(%rdx)
	movups	-16(%rdi), %xmm2
	movups	(%rdi), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	-16(%rdx), %xmm4
	movups	(%rdx), %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, -16(%rdx)
	movups	%xmm5, (%rdx)
	addq	$64, %rdi
	addq	$64, %rdx
	addq	$-16, %rbp
	jne	.LBB0_58
.LBB0_59:                               # %middle.block189
                                        #   in Loop: Header=BB0_17 Depth=2
	cmpq	%rbx, %rax
	je	.LBB0_67
# BB#60:                                #   in Loop: Header=BB0_17 Depth=2
	leaq	(%r10,%rbx,4), %r10
	movq	48(%rsp), %r9           # 8-byte Reload
	movl	40(%rsp), %edx          # 4-byte Reload
	jmp	.LBB0_61
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph126.split.split.us
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_58 Depth 3
                                        #       Child Loop BB0_63 Depth 3
                                        #       Child Loop BB0_66 Depth 3
	cmpq	$8, %rax
	movq	8(%r12,%r15,8), %rdx
	leaq	(%rdx,%r14,4), %r10
	movss	(%rdx,%r14,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	jb	.LBB0_18
# BB#50:                                # %min.iters.checked192
                                        #   in Loop: Header=BB0_17 Depth=2
	testq	%rbx, %rbx
	je	.LBB0_18
# BB#51:                                # %vector.memcheck210
                                        #   in Loop: Header=BB0_17 Depth=2
	leaq	4(%rdx,%r14,4), %rsi
	cmpq	88(%rsp), %rsi          # 8-byte Folded Reload
	jae	.LBB0_53
# BB#52:                                # %vector.memcheck210
                                        #   in Loop: Header=BB0_17 Depth=2
	leaq	8(%rdx,%r11,4), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB0_53
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_17 Depth=2
	movq	%rcx, %r9
	movl	%r8d, %edx
.LBB0_61:                               # %scalar.ph190.preheader
                                        #   in Loop: Header=BB0_17 Depth=2
	movl	%r13d, %ebp
	subl	%edx, %ebp
	movl	4(%rsp), %edi           # 4-byte Reload
	subl	%edx, %edi
	andl	$3, %ebp
	je	.LBB0_64
# BB#62:                                # %scalar.ph190.prol.preheader
                                        #   in Loop: Header=BB0_17 Depth=2
	negl	%ebp
	.p2align	4, 0x90
.LBB0_63:                               # %scalar.ph190.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%r9), %xmm1            # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	4(%r10), %xmm1
	movss	%xmm1, 4(%r10)
	leaq	4(%r10), %r10
	incl	%edx
	addq	$4, %r9
	incl	%ebp
	jne	.LBB0_63
.LBB0_64:                               # %scalar.ph190.prol.loopexit
                                        #   in Loop: Header=BB0_17 Depth=2
	cmpl	$3, %edi
	jb	.LBB0_67
# BB#65:                                # %scalar.ph190.preheader.new
                                        #   in Loop: Header=BB0_17 Depth=2
	addq	$16, %r10
	movl	%r13d, %edi
	subl	%edx, %edi
	.p2align	4, 0x90
.LBB0_66:                               # %scalar.ph190
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%r9), %xmm1            # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	-12(%r10), %xmm1
	movss	%xmm1, -12(%r10)
	movss	4(%r9), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	-8(%r10), %xmm1
	movss	%xmm1, -8(%r10)
	movss	8(%r9), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	-4(%r10), %xmm1
	movss	%xmm1, -4(%r10)
	movss	12(%r9), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%r10), %xmm1
	movss	%xmm1, (%r10)
	addq	$16, %r10
	addq	$16, %r9
	addl	$-4, %edi
	jne	.LBB0_66
.LBB0_67:                               # %._crit_edge.us135
                                        #   in Loop: Header=BB0_17 Depth=2
	incq	%r15
	cmpq	%r13, %r15
	jne	.LBB0_17
.LBB0_68:                               #   in Loop: Header=BB0_3 Depth=1
	movq	112(%rsp), %rbp         # 8-byte Reload
	movq	120(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_69:                               # %.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	144(%rsp), %r8          # 8-byte Folded Reload
	jne	.LBB0_70
	jmp	.LBB0_71
.LBB0_22:                               #   in Loop: Header=BB0_3 Depth=1
	xorl	%ebp, %ebp
	testq	%r10, %r10
	je	.LBB0_27
.LBB0_25:                               # %vector.ph245.new
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%r9, %rsi
	subq	%rbp, %rsi
	addq	%r14, %rbp
	leaq	52(%r15,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB0_26:                               # %vector.body237
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rbp), %xmm2
	movups	-32(%rbp), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	%xmm2, -48(%rbp)
	movups	%xmm3, -32(%rbp)
	movups	-16(%rbp), %xmm2
	movups	(%rbp), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	%xmm2, -16(%rbp)
	movups	%xmm3, (%rbp)
	addq	$64, %rbp
	addq	$-16, %rsi
	jne	.LBB0_26
.LBB0_27:                               # %middle.block238
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpq	%r9, %rcx
	je	.LBB0_14
# BB#28:                                #   in Loop: Header=BB0_3 Depth=1
	addl	%r8d, %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%rsi,%r9,4), %rsi
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_70:                               # %.loopexit..lr.ph140_crit_edge
                                        #   in Loop: Header=BB0_3 Depth=1
	incq	96(%rsp)                # 8-byte Folded Spill
	addq	$4, %rbx
	movq	8(%r12,%r8,8), %r15
	movq	%r8, %r14
.LBB0_3:                                # %.lr.ph140
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_26 Depth 2
                                        #     Child Loop BB0_13 Depth 2
                                        #     Child Loop BB0_17 Depth 2
                                        #       Child Loop BB0_58 Depth 3
                                        #       Child Loop BB0_63 Depth 3
                                        #       Child Loop BB0_66 Depth 3
                                        #     Child Loop BB0_30 Depth 2
                                        #       Child Loop BB0_47 Depth 3
                                        #       Child Loop BB0_34 Depth 3
                                        #       Child Loop BB0_37 Depth 3
	leaq	(%r15,%r14,4), %rsi
	movl	%r13d, %edi
	subl	%r14d, %edi
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	callq	isamax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%r14), %edx
	movl	%edx, (%rbx)
	movq	8(%r12,%r14,8), %rcx
	movslq	%edx, %rdi
	movss	(%rcx,%rdi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	.LCPI0_1, %xmm1
	jne	.LBB0_5
	jp	.LBB0_5
# BB#4:                                 # %.lr.ph140..loopexit_crit_edge
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%r14, %r8
	incq	%r8
	movl	%r14d, %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	cmpq	144(%rsp), %r8          # 8-byte Folded Reload
	jne	.LBB0_70
	jmp	.LBB0_71
	.p2align	4, 0x90
.LBB0_5:                                #   in Loop: Header=BB0_3 Depth=1
	testl	%eax, %eax
	je	.LBB0_6
# BB#7:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	32(%rsp), %rsi          # 8-byte Reload
	movl	(%rsi), %edx
	movl	%edx, (%rcx,%rdi,4)
	movss	%xmm1, (%rsi)
	jmp	.LBB0_8
.LBB0_6:                                # %._crit_edge
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
.LBB0_8:                                #   in Loop: Header=BB0_3 Depth=1
	leaq	1(%r14), %r8
	cmpq	%rbp, %r8
	jge	.LBB0_69
# BB#9:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movss	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	4(%rdx), %rsi
	movl	20(%rsp), %edx          # 4-byte Reload
	movl	%edx, %ecx
	subl	%r8d, %ecx
	incq	%rcx
	cmpq	$7, %rcx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	ja	.LBB0_19
# BB#10:                                #   in Loop: Header=BB0_3 Depth=1
	movq	%rbp, %r11
	jmp	.LBB0_11
.LBB0_19:                               # %min.iters.checked241
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rcx, %rdx
	movabsq	$8589934584, %rsi       # imm = 0x1FFFFFFF8
	andq	%rsi, %rdx
	movq	%rcx, %r9
	movq	%rbp, %r11
	andq	%rsi, %r9
	je	.LBB0_20
# BB#21:                                # %vector.ph245
                                        #   in Loop: Header=BB0_3 Depth=1
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	leaq	-8(%r9), %rsi
	movq	%rsi, %r10
	shrq	$3, %r10
	btl	$3, %esi
	jb	.LBB0_22
# BB#23:                                # %vector.body237.prol
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	movups	(%rsi), %xmm2
	movups	16(%rsi), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	%xmm2, (%rsi)
	movups	%xmm3, 16(%rsi)
	movl	$8, %ebp
	testq	%r10, %r10
	jne	.LBB0_25
	jmp	.LBB0_27
.LBB0_20:                               #   in Loop: Header=BB0_3 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB0_11:                               # %.lr.ph.preheader267
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%r8d, %edx
.LBB0_12:                               # %.lr.ph.preheader267
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%r13d, %ebp
	subl	%edx, %ebp
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsi)
	addq	$4, %rsi
	decl	%ebp
	jne	.LBB0_13
.LBB0_14:                               # %.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%r11, %rbp
	cmpq	%rbp, %r8
	jge	.LBB0_69
# BB#15:                                # %.lr.ph126
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%eax, %eax
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	je	.LBB0_16
# BB#29:                                # %.lr.ph124.us.preheader.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, %r11d
	subl	%r8d, %r11d
	incq	%r11
	movl	20(%rsp), %eax          # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	subl	%r8d, %eax
	addq	%r14, %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	8(%r15,%rax,4), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movabsq	$8589934584, %rax       # imm = 0x1FFFFFFF8
	andq	%rax, %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leaq	-8(%rcx), %rdx
	shrq	$3, %rdx
	movq	%r11, %rbx
	andq	%rax, %rbx
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rbx,4), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leal	(%rbx,%r8), %eax
	movl	%eax, 108(%rsp)         # 4-byte Spill
	movq	%rdx, 64(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	andl	$1, %edx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	leaq	52(%r15,%r14,4), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	96(%rsp), %r15          # 8-byte Reload
	jmp	.LBB0_30
.LBB0_42:                               # %vector.ph
                                        #   in Loop: Header=BB0_30 Depth=2
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_43
# BB#44:                                # %vector.body.prol
                                        #   in Loop: Header=BB0_30 Depth=2
	movups	(%rcx), %xmm2
	movups	16(%rcx), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	4(%r10), %xmm4
	movups	20(%r10), %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, 4(%r10)
	movups	%xmm5, 20(%r10)
	movl	$8, %eax
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_46
	jmp	.LBB0_48
.LBB0_43:                               #   in Loop: Header=BB0_30 Depth=2
	xorl	%eax, %eax
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB0_48
.LBB0_46:                               # %vector.ph.new
                                        #   in Loop: Header=BB0_30 Depth=2
	movq	72(%rsp), %rbp          # 8-byte Reload
	subq	%rax, %rbp
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax,4), %rsi
	leaq	(%rdx,%r14,4), %rdx
	leaq	52(%rdx,%rax,4), %rdx
	.p2align	4, 0x90
.LBB0_47:                               # %vector.body
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-48(%rsi), %xmm2
	movups	-32(%rsi), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	-48(%rdx), %xmm4
	movups	-32(%rdx), %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, -48(%rdx)
	movups	%xmm5, -32(%rdx)
	movups	-16(%rsi), %xmm2
	movups	(%rsi), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	-16(%rdx), %xmm4
	movups	(%rdx), %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, -16(%rdx)
	movups	%xmm5, (%rdx)
	addq	$64, %rsi
	addq	$64, %rdx
	addq	$-16, %rbp
	jne	.LBB0_47
.LBB0_48:                               # %middle.block
                                        #   in Loop: Header=BB0_30 Depth=2
	cmpq	%rbx, %r11
	je	.LBB0_38
# BB#49:                                #   in Loop: Header=BB0_30 Depth=2
	leaq	(%r10,%rbx,4), %r10
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	108(%rsp), %r9d         # 4-byte Reload
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_30:                               # %.lr.ph124.us.preheader
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_47 Depth 3
                                        #       Child Loop BB0_34 Depth 3
                                        #       Child Loop BB0_37 Depth 3
	movq	8(%r12,%r15,8), %rdx
	leaq	(%rdx,%r14,4), %r10
	movl	(%rdx,%r14,4), %eax
	movl	(%rdx,%rdi,4), %esi
	movl	%esi, (%rdx,%r14,4)
	movl	%eax, (%rdx,%rdi,4)
	movss	(%rdx,%r14,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cmpq	$7, %r11
	jbe	.LBB0_31
# BB#39:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_30 Depth=2
	testq	%rbx, %rbx
	je	.LBB0_31
# BB#40:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_30 Depth=2
	leaq	4(%rdx,%r14,4), %rax
	cmpq	88(%rsp), %rax          # 8-byte Folded Reload
	jae	.LBB0_42
# BB#41:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_30 Depth=2
	movq	80(%rsp), %rax          # 8-byte Reload
	leaq	8(%rdx,%rax,4), %rax
	cmpq	%rax, %rcx
	jae	.LBB0_42
	.p2align	4, 0x90
.LBB0_31:                               #   in Loop: Header=BB0_30 Depth=2
	movq	%rcx, %rdx
	movl	%r8d, %r9d
.LBB0_32:                               # %.lr.ph124.us.preheader265
                                        #   in Loop: Header=BB0_30 Depth=2
	movl	%r13d, %ebp
	subl	%r9d, %ebp
	movl	28(%rsp), %esi          # 4-byte Reload
	subl	%r9d, %esi
	andl	$3, %ebp
	je	.LBB0_35
# BB#33:                                # %.lr.ph124.us.prol.preheader
                                        #   in Loop: Header=BB0_30 Depth=2
	negl	%ebp
	.p2align	4, 0x90
.LBB0_34:                               # %.lr.ph124.us.prol
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	4(%r10), %xmm1
	movss	%xmm1, 4(%r10)
	leaq	4(%r10), %r10
	incl	%r9d
	addq	$4, %rdx
	incl	%ebp
	jne	.LBB0_34
.LBB0_35:                               # %.lr.ph124.us.prol.loopexit
                                        #   in Loop: Header=BB0_30 Depth=2
	cmpl	$3, %esi
	jb	.LBB0_38
# BB#36:                                # %.lr.ph124.us.preheader265.new
                                        #   in Loop: Header=BB0_30 Depth=2
	addq	$16, %r10
	movl	%r13d, %esi
	subl	%r9d, %esi
	.p2align	4, 0x90
.LBB0_37:                               # %.lr.ph124.us
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	-12(%r10), %xmm1
	movss	%xmm1, -12(%r10)
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	-8(%r10), %xmm1
	movss	%xmm1, -8(%r10)
	movss	8(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	-4(%r10), %xmm1
	movss	%xmm1, -4(%r10)
	movss	12(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%r10), %xmm1
	movss	%xmm1, (%r10)
	addq	$16, %r10
	addq	$16, %rdx
	addl	$-4, %esi
	jne	.LBB0_37
.LBB0_38:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_30 Depth=2
	incq	%r15
	cmpq	%r13, %r15
	jne	.LBB0_30
	jmp	.LBB0_68
.LBB0_71:                               # %.loopexit117.loopexit
	movq	136(%rsp), %rsi         # 8-byte Reload
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	(%rsi,%rax,4), %rsi
	movl	104(%rsp), %eax         # 4-byte Reload
	movl	24(%rsp), %ecx          # 4-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB0_72:                               # %.loopexit117
	movl	%eax, (%rsi)
	xorps	%xmm0, %xmm0
	ucomiss	(%r15), %xmm0
	cmovnel	%ecx, %r13d
	cmovpl	%ecx, %r13d
	movl	%r13d, %eax
.LBB0_73:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	sgefa, .Lfunc_end0-sgefa
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
