	.text
	.file	"command.bc"
	.globl	C_addcmd
	.p2align	4, 0x90
	.type	C_addcmd,@function
C_addcmd:                               # @C_addcmd
	.cfi_startproc
# BB#0:
	movq	%rdi, %rcx
	movslq	_C_nextcmd(%rip), %rax
	leal	1(%rax), %edx
	movl	%edx, _C_nextcmd(%rip)
	leaq	_C_cmds(,%rax,8), %rdi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	jmp	S_savestr               # TAILCALL
.Lfunc_end0:
	.size	C_addcmd, .Lfunc_end0-C_addcmd
	.cfi_endproc

	.globl	C_docmds
	.p2align	4, 0x90
	.type	C_docmds,@function
C_docmds:                               # @C_docmds
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	cmpl	$0, _C_nextcmd(%rip)
	jle	.LBB1_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	_C_cmds(,%rbx,8), %rdi
	callq	_C_do_a_cmd
	incq	%rbx
	movslq	_C_nextcmd(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB1_2
.LBB1_3:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end1:
	.size	C_docmds, .Lfunc_end1-C_docmds
	.cfi_endproc

	.p2align	4, 0x90
	.type	_C_do_a_cmd,@function
_C_do_a_cmd:                            # @_C_do_a_cmd
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 32
.Lcfi4:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	%rbx, 8(%rsp)
	leaq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	S_skipspace
	movq	8(%rsp), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB2_1
# BB#4:
	movq	8(%rsp), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB2_5
# BB#6:
	movq	8(%rsp), %rdi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB2_7
# BB#11:
	movq	8(%rsp), %rdi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB2_12
# BB#15:
	movq	8(%rsp), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB2_16
# BB#19:
	movq	8(%rsp), %rdi
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB2_20
# BB#21:
	movq	8(%rsp), %rdi
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB2_22
# BB#23:
	movq	8(%rsp), %rdi
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB2_24
# BB#25:
	movq	8(%rsp), %rdi
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB2_26
# BB#27:
	movq	8(%rsp), %rdi
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB2_28
# BB#29:
	movq	8(%rsp), %rdi
	cmpb	$0, (%rdi)
	je	.LBB2_33
# BB#30:
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB2_33
# BB#31:
	movq	8(%rsp), %rax
	cmpb	$35, (%rax)
	je	.LBB2_33
# BB#32:
	movl	$Z_err_buf, %edi
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	sprintf
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	jmp	.LBB2_33
.LBB2_1:
	leaq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	S_nextword
	movq	8(%rsp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	cmpq	$20, %rax
	jb	.LBB2_3
# BB#2:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	movq	8(%rsp), %rbx
.LBB2_3:
	movl	$_C_cmdword, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	S_wordcpy
	jmp	.LBB2_33
.LBB2_5:
	leaq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	S_nextword
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	T_tolline
	jmp	.LBB2_33
.LBB2_7:
	leaq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	S_nextword
	movq	8(%rsp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	cmpq	$20, %rax
	jb	.LBB2_9
# BB#8:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	movq	8(%rsp), %rbx
.LBB2_9:
	xorl	%esi, %esi
	jmp	.LBB2_10
.LBB2_12:
	leaq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	S_nextword
	movq	8(%rsp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	cmpq	$20, %rax
	jb	.LBB2_14
# BB#13:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	movq	8(%rsp), %rbx
.LBB2_14:
	movl	$1, %esi
.LBB2_10:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	W_addcom
.LBB2_33:
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB2_16:
	leaq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	S_nextword
	movq	8(%rsp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	cmpq	$20, %rax
	jb	.LBB2_18
# BB#17:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	movq	8(%rsp), %rbx
.LBB2_18:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	W_addlit
	jmp	.LBB2_33
.LBB2_20:
	xorl	%eax, %eax
	callq	W_clearcoms
	jmp	.LBB2_33
.LBB2_22:
	xorl	%eax, %eax
	callq	W_clearlits
	jmp	.LBB2_33
.LBB2_24:
	leaq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	S_nextword
	movq	8(%rsp), %rax
	movb	(%rax), %al
	movb	%al, _W_bolchar(%rip)
	jmp	.LBB2_33
.LBB2_26:
	leaq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	S_nextword
	movq	8(%rsp), %rax
	movb	(%rax), %al
	movb	%al, _W_eolchar(%rip)
	jmp	.LBB2_33
.LBB2_28:
	leaq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	S_nextword
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	P_addalpha
	jmp	.LBB2_33
.Lfunc_end2:
	.size	_C_do_a_cmd, .Lfunc_end2-_C_do_a_cmd
	.cfi_endproc

	.globl	C_clear_cmd
	.p2align	4, 0x90
	.type	C_clear_cmd,@function
C_clear_cmd:                            # @C_clear_cmd
	.cfi_startproc
# BB#0:
	movb	$0, _C_cmdword(%rip)
	retq
.Lfunc_end3:
	.size	C_clear_cmd, .Lfunc_end3-C_clear_cmd
	.cfi_endproc

	.globl	C_is_cmd
	.p2align	4, 0x90
	.type	C_is_cmd,@function
C_is_cmd:                               # @C_is_cmd
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cmpb	$0, _C_cmdword(%rip)
	je	.LBB4_1
# BB#2:
	xorl	%ebp, %ebp
	movl	$_C_cmdword, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	S_wordcmp
	testl	%eax, %eax
	jne	.LBB4_4
# BB#3:
	movq	%rbx, (%rsp)
	movq	%rsp, %rdi
	xorl	%eax, %eax
	callq	S_nextword
	movq	(%rsp), %rdi
	callq	_C_do_a_cmd
	movl	$1, %ebp
	jmp	.LBB4_4
.LBB4_1:
	xorl	%ebp, %ebp
.LBB4_4:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	C_is_cmd, .Lfunc_end4-C_is_cmd
	.cfi_endproc

	.type	_C_cmds,@object         # @_C_cmds
	.local	_C_cmds
	.comm	_C_cmds,800,16
	.type	_C_nextcmd,@object      # @_C_nextcmd
	.local	_C_nextcmd
	.comm	_C_nextcmd,4,4
	.type	_C_cmdword,@object      # @_C_cmdword
	.local	_C_cmdword
	.comm	_C_cmdword,20,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"command"
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"command word is too long"
	.size	.L.str.1, 25

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"tol"
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"comment"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"nestcom"
	.size	.L.str.4, 8

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"literal"
	.size	.L.str.5, 8

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"resetcomments"
	.size	.L.str.6, 14

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"resetliterals"
	.size	.L.str.7, 14

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"beginchar"
	.size	.L.str.8, 10

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"endchar"
	.size	.L.str.9, 8

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"addalpha"
	.size	.L.str.10, 9

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"rem"
	.size	.L.str.11, 4

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"don't understand command %s\n"
	.size	.L.str.12, 29


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
