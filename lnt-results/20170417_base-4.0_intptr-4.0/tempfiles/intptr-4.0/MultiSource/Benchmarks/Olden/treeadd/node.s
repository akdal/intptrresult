	.text
	.file	"node.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	callq	dealwithargs
	movl	%eax, %ebx
	movl	NumNodes(%rip), %edx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movl	$.Lstr, %edi
	callq	puts
	movl	NumNodes(%rip), %edx
	xorl	%esi, %esi
	movl	%ebx, %edi
	callq	TreeAlloc
	movq	%rax, %rbx
	movl	$.Lstr.1, %edi
	callq	puts
	movq	%rbx, %rdi
	callq	TreeAdd
	movl	%eax, %ecx
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	xorl	%edi, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.globl	TreeAdd
	.p2align	4, 0x90
	.type	TreeAdd,@function
TreeAdd:                                # @TreeAdd
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB1_2
# BB#1:
	movq	8(%rbx), %rdi
	callq	TreeAdd
	movl	%eax, %ebp
	movq	16(%rbx), %rdi
	callq	TreeAdd
	addl	%ebp, %eax
	addl	(%rbx), %eax
	jmp	.LBB1_3
.LBB1_2:
	xorl	%eax, %eax
.LBB1_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	TreeAdd, .Lfunc_end1-TreeAdd
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Treeadd with %d levels on %d processors \n"
	.size	.L.str, 42

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Received result of %d\n"
	.size	.L.str.3, 23

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"About to enter TreeAlloc"
	.size	.Lstr, 25

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"About to enter TreeAdd"
	.size	.Lstr.1, 23


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
