	.text
	.file	"libclamav_matcher-bm.bc"
	.globl	cli_bm_addpatt
	.p2align	4, 0x90
	.type	cli_bm_addpatt,@function
cli_bm_addpatt:                         # @cli_bm_addpatt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movzwl	16(%rsi), %r8d
	cmpl	$3, %r8d
	jae	.LBB0_1
# BB#19:
	movq	24(%rsi), %rsi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-117, %eax
	jmp	.LBB0_18
.LBB0_1:                                # %.lr.ph93
	movq	(%rsi), %r11
	movq	16(%rdi), %r9
	leal	-2(%r8), %r10d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movzwl	%dx, %eax
	movzbl	(%r11,%rax), %ecx
	imull	$211, %ecx, %ecx
	movl	%edx, %ebx
	movzbl	1(%r11,%rbx), %ebp
	imull	$37, %ebp, %ebp
	addl	%ecx, %ebp
	movzbl	2(%r11,%rbx), %ecx
	addl	%ebp, %ecx
	movzwl	%cx, %ecx
	cmpq	$0, (%r9,%rcx,8)
	je	.LBB0_3
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	leal	1(%rdx), %eax
	movzwl	%ax, %edx
	cmpl	%r10d, %edx
	jl	.LBB0_2
	jmp	.LBB0_6
.LBB0_3:
	testw	%dx, %dx
	je	.LBB0_6
# BB#4:
	addq	%r11, %rax
	movq	%r11, 8(%rsi)
	movw	%dx, 18(%rsi)
	movq	%rax, (%rsi)
	subl	%edx, %r8d
	movw	%r8w, 16(%rsi)
	movq	%rax, %r11
.LBB0_6:                                # %.loopexit
	movzbl	(%r11), %eax
	imull	$211, %eax, %eax
	movzbl	1(%r11), %ecx
	imull	$37, %ecx, %ecx
	addl	%eax, %ecx
	movzbl	2(%r11), %eax
	addl	%ecx, %eax
	movq	8(%rdi), %rcx
	movzwl	%ax, %r9d
	movb	$0, (%rcx,%r9)
	movq	16(%rdi), %r8
	movq	(%r8,%r9,8), %r10
	testq	%r10, %r10
	je	.LBB0_7
# BB#8:                                 # %.lr.ph
	movb	(%r11), %r11b
	movq	%r10, %rcx
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	cmpb	(%rcx), %r11b
	jae	.LBB0_13
# BB#10:                                #   in Loop: Header=BB0_9 Depth=1
	movq	48(%rdx), %rcx
	testq	%rcx, %rcx
	movq	%rdx, %rax
	jne	.LBB0_9
# BB#11:
	movq	%rdx, %rax
	jmp	.LBB0_12
.LBB0_7:
	xorl	%eax, %eax
.LBB0_12:                               # %._crit_edge
	xorl	%edx, %edx
.LBB0_13:                               # %._crit_edge
	cmpq	%r10, %rdx
	je	.LBB0_14
# BB#16:
	movq	48(%rax), %rcx
	addq	$48, %rax
	movq	%rcx, 48(%rsi)
	jmp	.LBB0_17
.LBB0_14:
	leaq	(%r8,%r9,8), %rax
	movq	%r10, 48(%rsi)
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB0_17
# BB#15:
	movzwl	56(%rcx), %ecx
	movw	%cx, 56(%rsi)
.LBB0_17:
	movq	%rsi, (%rax)
	movq	16(%rdi), %rax
	movq	(%rax,%r9,8), %rax
	incw	56(%rax)
	xorl	%eax, %eax
.LBB0_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cli_bm_addpatt, .Lfunc_end0-cli_bm_addpatt
	.cfi_endproc

	.globl	cli_bm_init
	.p2align	4, 0x90
	.type	cli_bm_init,@function
cli_bm_init:                            # @cli_bm_init
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$63496, %edi            # imm = 0xF808
	callq	cli_malloc
	movq	%rax, 8(%rbx)
	movl	$-114, %r14d
	testq	%rax, %rax
	je	.LBB1_4
# BB#1:
	movl	$63496, %edi            # imm = 0xF808
	movl	$8, %esi
	callq	cli_calloc
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB1_5
# BB#2:                                 # %.preheader.preheader
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rcx
	movb	$1, (%rcx,%rax)
	movq	8(%rbx), %rcx
	movb	$1, 1(%rcx,%rax)
	movq	8(%rbx), %rcx
	movb	$1, 2(%rcx,%rax)
	movq	8(%rbx), %rcx
	movb	$1, 3(%rcx,%rax)
	movq	8(%rbx), %rcx
	movb	$1, 4(%rcx,%rax)
	movq	8(%rbx), %rcx
	movb	$1, 5(%rcx,%rax)
	movq	8(%rbx), %rcx
	movb	$1, 6(%rcx,%rax)
	movq	8(%rbx), %rcx
	movb	$1, 7(%rcx,%rax)
	addq	$8, %rax
	cmpq	$63496, %rax            # imm = 0xF808
	jne	.LBB1_3
	jmp	.LBB1_4
.LBB1_5:
	movq	8(%rbx), %rdi
	callq	free
.LBB1_4:                                # %.loopexit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	cli_bm_init, .Lfunc_end1-cli_bm_init
	.cfi_endproc

	.globl	cli_bm_free
	.p2align	4, 0x90
	.type	cli_bm_free,@function
cli_bm_free:                            # @cli_bm_free
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r12, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	callq	free
.LBB2_2:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_15
# BB#3:                                 # %.preheader.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB2_4:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_5 Depth 2
	movq	(%rdi,%r15,8), %rbx
	testq	%rbx, %rbx
	je	.LBB2_13
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movq	48(%rbx), %r12
	testq	%rdi, %rdi
	jne	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=2
	movq	(%rbx), %rdi
.LBB2_7:                                #   in Loop: Header=BB2_5 Depth=2
	callq	free
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_9
# BB#8:                                 #   in Loop: Header=BB2_5 Depth=2
	callq	free
.LBB2_9:                                #   in Loop: Header=BB2_5 Depth=2
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_11
# BB#10:                                #   in Loop: Header=BB2_5 Depth=2
	callq	free
.LBB2_11:                               #   in Loop: Header=BB2_5 Depth=2
	movq	%rbx, %rdi
	callq	free
	testq	%r12, %r12
	movq	%r12, %rbx
	jne	.LBB2_5
# BB#12:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	16(%r14), %rdi
.LBB2_13:                               # %._crit_edge
                                        #   in Loop: Header=BB2_4 Depth=1
	incq	%r15
	cmpq	$63496, %r15            # imm = 0xF808
	jne	.LBB2_4
# BB#14:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB2_15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	cli_bm_free, .Lfunc_end2-cli_bm_free
	.cfi_endproc

	.globl	cli_bm_scanbuff
	.p2align	4, 0x90
	.type	cli_bm_scanbuff,@function
cli_bm_scanbuff:                        # @cli_bm_scanbuff
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 160
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movl	%r9d, 24(%rsp)          # 4-byte Spill
	movl	%r8d, %r11d
	movl	%esi, %r14d
	xorl	%ebx, %ebx
	cmpl	$3, %r14d
	jb	.LBB3_61
# BB#1:
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.LBB3_61
# BB#2:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movq	$0, 96(%rsp)
	movl	%r14d, %ebp
	addl	$-2, %ebp
	je	.LBB3_61
# BB#3:                                 # %.lr.ph133
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	cmpl	$-1, 160(%rsp)
	setne	%dl
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	setne	%bl
	orb	%dl, %bl
	movb	%bl, 11(%rsp)           # 1-byte Spill
	xorl	%r12d, %r12d
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	jmp	.LBB3_8
.LBB3_4:                                #   in Loop: Header=BB3_8 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
.LBB3_5:                                #   in Loop: Header=BB3_8 Depth=1
	movl	12(%rsp), %ebp          # 4-byte Reload
.LBB3_6:                                # %.loopexit
                                        #   in Loop: Header=BB3_8 Depth=1
	movzbl	%r10b, %edx
	addl	%r12d, %edx
	cmpl	%ebp, %edx
	jae	.LBB3_59
# BB#7:                                 # %.loopexit._crit_edge
                                        #   in Loop: Header=BB3_8 Depth=1
	movq	8(%rcx), %rax
	movl	%edx, %r12d
.LBB3_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_13 Depth 2
                                        #       Child Loop BB3_24 Depth 3
                                        #     Child Loop BB3_38 Depth 2
                                        #       Child Loop BB3_49 Depth 3
	movl	%r12d, %r9d
	movzbl	(%rdi,%r9), %r15d
	imull	$211, %r15d, %esi
	leal	1(%r12), %edx
	movzbl	(%rdi,%rdx), %edx
	imull	$37, %edx, %edx
	addl	%esi, %edx
	leal	2(%r12), %esi
	movzbl	(%rdi,%rsi), %esi
	addl	%edx, %esi
	movzwl	%si, %edx
	movb	(%rax,%rdx), %r10b
	testb	%r10b, %r10b
	jne	.LBB3_6
# BB#9:                                 #   in Loop: Header=BB3_8 Depth=1
	movq	16(%rcx), %rax
	movq	(%rax,%rdx,8), %rbp
	movb	$1, %r10b
	testq	%rbp, %rbp
	je	.LBB3_5
# BB#10:                                # %.lr.ph126
                                        #   in Loop: Header=BB3_8 Depth=1
	addq	%rdi, %r9
	movl	%r14d, %eax
	subl	%r12d, %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	cmpb	$0, 11(%rsp)            # 1-byte Folded Reload
	je	.LBB3_12
# BB#11:                                # %.lr.ph126.split.us.preheader
                                        #   in Loop: Header=BB3_8 Depth=1
	leal	(%r12,%r11), %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	xorl	%r13d, %r13d
	movq	%r9, 32(%rsp)           # 8-byte Spill
	jmp	.LBB3_38
.LBB3_12:                               # %.lr.ph126.split.preheader
                                        #   in Loop: Header=BB3_8 Depth=1
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB3_13:                               # %.lr.ph126.split
                                        #   Parent Loop BB3_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_24 Depth 3
	movq	(%rbp), %rdx
	cmpb	%r15b, (%rdx)
	jne	.LBB3_30
# BB#14:                                #   in Loop: Header=BB3_13 Depth=2
	movzwl	16(%rbp), %esi
	leal	(%rsi,%r12), %eax
	movb	$1, %r8b
	cmpl	%r14d, %eax
	ja	.LBB3_32
# BB#15:                                #   in Loop: Header=BB3_13 Depth=2
	movzwl	18(%rbp), %r13d
	movl	%r12d, %eax
	subl	%r13d, %eax
	jb	.LBB3_32
# BB#16:                                #   in Loop: Header=BB3_13 Depth=2
	movl	28(%rsp), %edi          # 4-byte Reload
	cmpl	%edi, %esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	cmovbl	%esi, %edi
	addl	$65535, %edi            # imm = 0xFFFF
	testw	%di, %di
	je	.LBB3_19
# BB#17:                                #   in Loop: Header=BB3_13 Depth=2
	movq	%r9, %r10
	movzwl	%di, %r9d
	movb	(%r10,%r9), %bl
	cmpb	(%rdx,%r9), %bl
	jne	.LBB3_29
# BB#18:                                #   in Loop: Header=BB3_13 Depth=2
	shrl	%edi
	andl	$32767, %edi            # imm = 0x7FFF
	movb	(%r10,%rdi), %bl
	cmpb	(%rdx,%rdi), %bl
	movq	%r10, %r9
	movb	$1, %r10b
	jne	.LBB3_33
.LBB3_19:                               #   in Loop: Header=BB3_13 Depth=2
	testw	%r13w, %r13w
	movl	%r12d, %edi
	movq	%r9, %r10
	je	.LBB3_21
# BB#20:                                #   in Loop: Header=BB3_13 Depth=2
	movq	%r10, %r9
	subq	%r13, %r9
	movq	8(%rbp), %rdx
	movl	%eax, %edi
.LBB3_21:                               #   in Loop: Header=BB3_13 Depth=2
	addl	%esi, %r13d
	xorl	%eax, %eax
	cmpl	%r14d, %edi
	jae	.LBB3_27
# BB#22:                                #   in Loop: Header=BB3_13 Depth=2
	testl	%r13d, %r13d
	je	.LBB3_27
# BB#23:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB3_13 Depth=2
	incl	%edi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_24:                               # %.lr.ph
                                        #   Parent Loop BB3_8 Depth=1
                                        #     Parent Loop BB3_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%r9,%rax), %ebx
	cmpb	(%rdx,%rax), %bl
	jne	.LBB3_29
# BB#25:                                #   in Loop: Header=BB3_24 Depth=3
	movl	%eax, %esi
	incq	%rax
	leal	(%rdi,%rsi), %esi
	cmpl	%r14d, %esi
	jae	.LBB3_27
# BB#26:                                #   in Loop: Header=BB3_24 Depth=3
	cmpl	%r13d, %eax
	jb	.LBB3_24
.LBB3_27:                               # %.critedge113
                                        #   in Loop: Header=BB3_13 Depth=2
	cmpl	%eax, %r13d
	jne	.LBB3_29
# BB#28:                                #   in Loop: Header=BB3_13 Depth=2
	cmpb	$0, 40(%rbp)
	je	.LBB3_34
.LBB3_29:                               #   in Loop: Header=BB3_13 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r10, %r9
	movb	$1, %r10b
	jmp	.LBB3_32
	.p2align	4, 0x90
.LBB3_30:                               #   in Loop: Header=BB3_13 Depth=2
	testb	%r8b, %r8b
	jne	.LBB3_5
# BB#31:                                #   in Loop: Header=BB3_13 Depth=2
	xorl	%r8d, %r8d
.LBB3_32:                               # %.backedge
                                        #   in Loop: Header=BB3_13 Depth=2
	movq	48(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_13
	jmp	.LBB3_5
.LBB3_33:                               #   in Loop: Header=BB3_13 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB3_32
.LBB3_34:                               #   in Loop: Header=BB3_13 Depth=2
	cmpq	$0, 32(%rbp)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r10, %r9
	movb	$1, %r10b
	jne	.LBB3_32
	jmp	.LBB3_62
.LBB3_37:                               #   in Loop: Header=BB3_38 Depth=2
	movq	32(%rsp), %r9           # 8-byte Reload
	jmp	.LBB3_58
.LBB3_35:                               #   in Loop: Header=BB3_38 Depth=2
	movq	%r10, %r9
	movb	$1, %r10b
	jmp	.LBB3_58
	.p2align	4, 0x90
.LBB3_38:                               # %.lr.ph126.split.us
                                        #   Parent Loop BB3_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_49 Depth 3
	movq	(%rbp), %rdx
	cmpb	%r15b, (%rdx)
	jne	.LBB3_56
# BB#39:                                #   in Loop: Header=BB3_38 Depth=2
	movzwl	16(%rbp), %ecx
	leal	(%rcx,%r12), %eax
	movb	$1, %r13b
	cmpl	%r14d, %eax
	ja	.LBB3_58
# BB#40:                                #   in Loop: Header=BB3_38 Depth=2
	movzwl	18(%rbp), %r8d
	movl	%r12d, %ebx
	subl	%r8d, %ebx
	jb	.LBB3_58
# BB#41:                                #   in Loop: Header=BB3_38 Depth=2
	movl	28(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, %ecx
	movl	%eax, %esi
	cmovbl	%ecx, %esi
	addl	$65535, %esi            # imm = 0xFFFF
	testw	%si, %si
	je	.LBB3_44
# BB#42:                                #   in Loop: Header=BB3_38 Depth=2
	movq	%r9, %r10
	movzwl	%si, %r9d
	movb	(%r10,%r9), %al
	cmpb	(%rdx,%r9), %al
	jne	.LBB3_35
# BB#43:                                #   in Loop: Header=BB3_38 Depth=2
	shrl	%esi
	andl	$32767, %esi            # imm = 0x7FFF
	movb	(%r10,%rsi), %al
	cmpb	(%rdx,%rsi), %al
	movq	%r10, %r9
	movb	$1, %r10b
	jne	.LBB3_58
.LBB3_44:                               #   in Loop: Header=BB3_38 Depth=2
	testw	%r8w, %r8w
	movl	%r12d, %esi
	movq	%r9, %rax
	je	.LBB3_46
# BB#45:                                #   in Loop: Header=BB3_38 Depth=2
	movq	%rax, %r9
	subq	%r8, %r9
	movq	8(%rbp), %rdx
	movl	%ebx, %esi
.LBB3_46:                               #   in Loop: Header=BB3_38 Depth=2
	addl	%r8d, %ecx
	xorl	%ebx, %ebx
	cmpl	%r14d, %esi
	jae	.LBB3_52
# BB#47:                                #   in Loop: Header=BB3_38 Depth=2
	testl	%ecx, %ecx
	je	.LBB3_52
# BB#48:                                # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB3_38 Depth=2
	incl	%esi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_49:                               # %.lr.ph.us
                                        #   Parent Loop BB3_8 Depth=1
                                        #     Parent Loop BB3_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%r9,%rbx), %eax
	cmpb	(%rdx,%rbx), %al
	jne	.LBB3_37
# BB#50:                                #   in Loop: Header=BB3_49 Depth=3
	movl	%ebx, %eax
	incq	%rbx
	leal	(%rsi,%rax), %eax
	cmpl	%r14d, %eax
	jae	.LBB3_52
# BB#51:                                #   in Loop: Header=BB3_49 Depth=3
	cmpl	%ecx, %ebx
	jb	.LBB3_49
.LBB3_52:                               # %.critedge113.us
                                        #   in Loop: Header=BB3_38 Depth=2
	cmpl	%ebx, %ecx
	jne	.LBB3_37
# BB#53:                                #   in Loop: Header=BB3_38 Depth=2
	movq	32(%rbp), %rsi
	cmpb	$0, 40(%rbp)
	jne	.LBB3_55
# BB#54:                                #   in Loop: Header=BB3_38 Depth=2
	testq	%rsi, %rsi
	je	.LBB3_62
.LBB3_55:                               # %._crit_edge144
                                        #   in Loop: Header=BB3_38 Depth=2
	movl	44(%rsp), %eax          # 4-byte Reload
	movl	%eax, %edx
	subl	%r8d, %edx
	movq	24(%rbp), %r9
	movl	24(%rsp), %edi          # 4-byte Reload
	leaq	64(%rsp), %rcx
	movl	160(%rsp), %r8d
	movq	%r11, %rbx
	callq	cli_validatesig
	movb	$1, %r10b
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %r11
	testl	%eax, %eax
	movq	32(%rsp), %r9           # 8-byte Reload
	je	.LBB3_58
	jmp	.LBB3_62
	.p2align	4, 0x90
.LBB3_56:                               #   in Loop: Header=BB3_38 Depth=2
	testb	%r13b, %r13b
	jne	.LBB3_4
# BB#57:                                #   in Loop: Header=BB3_38 Depth=2
	xorl	%r13d, %r13d
.LBB3_58:                               # %.backedge.us
                                        #   in Loop: Header=BB3_38 Depth=2
	movq	48(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_38
	jmp	.LBB3_4
.LBB3_59:                               # %._crit_edge
	movq	88(%rsp), %rdi
	xorl	%ebx, %ebx
	testq	%rdi, %rdi
	je	.LBB3_61
.LBB3_60:
	callq	free
.LBB3_61:                               # %._crit_edge.thread
	movl	%ebx, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_62:                               # %.us-lcssa.us
	movq	56(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB3_64
# BB#63:
	movq	24(%rbp), %rax
	movq	%rax, (%rcx)
.LBB3_64:
	movq	88(%rsp), %rdi
	movl	$1, %ebx
	testq	%rdi, %rdi
	jne	.LBB3_60
	jmp	.LBB3_61
.Lfunc_end3:
	.size	cli_bm_scanbuff, .Lfunc_end3-cli_bm_scanbuff
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Signature for %s is too short\n"
	.size	.L.str, 31


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
