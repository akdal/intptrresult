	.text
	.file	"sieve.bc"
	.globl	_Z5sieveRNSt7__cxx114listIiSaIiEEERSt6vectorIiS1_E
	.p2align	4, 0x90
	.type	_Z5sieveRNSt7__cxx114listIiSaIiEEERSt6vectorIiS1_E,@function
_Z5sieveRNSt7__cxx114listIiSaIiEEERSt6vectorIiS1_E: # @_Z5sieveRNSt7__cxx114listIiSaIiEEERSt6vectorIiS1_E
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 64
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r13, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	(%r12), %rbx
	cmpq	%r12, %rbx
	je	.LBB0_10
# BB#1:                                 # %.lr.ph16
	leaq	12(%rsp), %r15
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_3 Depth 2
	movl	16(%rbx), %eax
	movl	%eax, 12(%rsp)
	decq	16(%r12)
	movq	%rbx, %rdi
	callq	_ZNSt8__detail15_List_node_base9_M_unhookEv
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	(%r12), %rbx
	cmpq	%r12, %rbx
	je	.LBB0_6
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	16(%rbx), %eax
	cltd
	idivl	12(%rsp)
	movq	(%rbx), %r13
	testl	%edx, %edx
	jne	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=2
	decq	16(%r12)
	movq	%rbx, %rdi
	callq	_ZNSt8__detail15_List_node_base9_M_unhookEv
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB0_5:                                # %.backedge
                                        #   in Loop: Header=BB0_3 Depth=2
	cmpq	%r12, %r13
	movq	%r13, %rbx
	jne	.LBB0_3
.LBB0_6:                                # %._crit_edge
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	je	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	12(%rsp), %eax
	movl	%eax, (%rsi)
	addq	$4, %rsi
	movq	%rsi, 8(%r14)
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_8:                                #   in Loop: Header=BB0_2 Depth=1
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	_ZNSt6vectorIiSaIiEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPiS1_EERKi
.LBB0_9:                                # %_ZNSt6vectorIiSaIiEE9push_backERKi.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	(%r12), %rbx
	cmpq	%r12, %rbx
	jne	.LBB0_2
.LBB0_10:                               # %._crit_edge17
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	_Z5sieveRNSt7__cxx114listIiSaIiEEERSt6vectorIiS1_E, .Lfunc_end0-_Z5sieveRNSt7__cxx114listIiSaIiEEERSt6vectorIiS1_E
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 96
.Lcfi16:
	.cfi_offset %rbx, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	cmpl	$2, %edi
	jne	.LBB1_1
# BB#2:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	testl	%eax, %eax
	jle	.LBB1_3
# BB#5:
	movslq	%eax, %r15
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movq	$0, 16(%rsp)
	testq	%r15, %r15
	jne	.LBB1_6
.LBB1_14:                               # %._crit_edge
.Ltmp6:
	movl	$_ZSt4cout, %edi
	movl	$.L.str, %esi
	movl	$7, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp7:
# BB#15:                                # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
	movq	8(%rsp), %rsi
	subq	(%rsp), %rsi
	sarq	$2, %rsi
.Ltmp8:
	movl	$_ZSt4cout, %edi
	callq	_ZNSo9_M_insertImEERSoT_
	movq	%rax, %r14
.Ltmp9:
# BB#16:                                # %_ZNSolsEm.exit
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB1_17
# BB#22:                                # %.noexc19
	cmpb	$0, 56(%rbx)
	je	.LBB1_24
# BB#23:
	movb	67(%rbx), %al
	jmp	.LBB1_26
.LBB1_1:
	movl	$500, %r15d             # imm = 0x1F4
	jmp	.LBB1_4
.LBB1_3:
	movl	$1, %r15d
.LBB1_4:                                # %.thread
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movq	$0, 16(%rsp)
.LBB1_6:                                # %.lr.ph
	leaq	32(%rsp), %rbx
	movq	%rsp, %r14
.LBB1_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_8 Depth 2
                                        #     Child Loop BB1_12 Depth 2
	decq	%r15
	movq	%rbx, 32(%rsp)
	movq	%rbx, 40(%rsp)
	movq	$0, 48(%rsp)
	movl	$2, %ebp
	.p2align	4, 0x90
.LBB1_8:                                #   Parent Loop BB1_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp0:
	movl	$24, %edi
	callq	_Znwm
.Ltmp1:
# BB#9:                                 #   in Loop: Header=BB1_8 Depth=2
	movl	%ebp, 16(%rax)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	48(%rsp)
	incl	%ebp
	cmpl	$8192, %ebp             # imm = 0x2000
	jl	.LBB1_8
# BB#10:                                #   in Loop: Header=BB1_7 Depth=1
	movq	(%rsp), %rax
	movq	%rax, 8(%rsp)
.Ltmp3:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_Z5sieveRNSt7__cxx114listIiSaIiEEERSt6vectorIiS1_E
.Ltmp4:
# BB#11:                                #   in Loop: Header=BB1_7 Depth=1
	movq	32(%rsp), %rdi
	cmpq	%rbx, %rdi
	je	.LBB1_13
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph.i.i
                                        #   Parent Loop BB1_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%rbx, %rbp
	movq	%rbp, %rdi
	jne	.LBB1_12
.LBB1_13:                               # %_ZNSt7__cxx1110_List_baseIiSaIiEED2Ev.exit
                                        #   in Loop: Header=BB1_7 Depth=1
	testq	%r15, %r15
	jne	.LBB1_7
	jmp	.LBB1_14
.LBB1_24:
.Ltmp10:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp11:
# BB#25:                                # %.noexc21
	movq	(%rbx), %rax
.Ltmp12:
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.Ltmp13:
.LBB1_26:                               # %.noexc
.Ltmp14:
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZNSo3putEc
.Ltmp15:
# BB#27:                                # %.noexc16
.Ltmp16:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp17:
# BB#28:                                # %_ZNSolsEPFRSoS_E.exit
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_30
# BB#29:
	callq	_ZdlPv
.LBB1_30:                               # %_ZNSt6vectorIiSaIiEED2Ev.exit12
	xorl	%eax, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_17:
.Ltmp18:
	callq	_ZSt16__throw_bad_castv
.Ltmp19:
# BB#21:                                # %.noexc23
.LBB1_31:
.Ltmp20:
	movq	%rax, %r14
	jmp	.LBB1_32
.LBB1_18:
.Ltmp5:
	jmp	.LBB1_19
.LBB1_35:
.Ltmp2:
.LBB1_19:
	movq	%rax, %r14
	movq	32(%rsp), %rdi
	cmpq	%rbx, %rdi
	je	.LBB1_32
	.p2align	4, 0x90
.LBB1_20:                               # %.lr.ph.i.i14
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%rbx, %rbp
	movq	%rbp, %rdi
	jne	.LBB1_20
.LBB1_32:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_34
# BB#33:
	callq	_ZdlPv
.LBB1_34:                               # %_ZNSt6vectorIiSaIiEED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp9-.Ltmp6           #   Call between .Ltmp6 and .Ltmp9
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp19-.Ltmp10         #   Call between .Ltmp10 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Lfunc_end1-.Ltmp19     #   Call between .Ltmp19 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNSt6vectorIiSaIiEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPiS1_EERKi,"axG",@progbits,_ZNSt6vectorIiSaIiEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPiS1_EERKi,comdat
	.weak	_ZNSt6vectorIiSaIiEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPiS1_EERKi
	.p2align	4, 0x90
	.type	_ZNSt6vectorIiSaIiEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPiS1_EERKi,@function
_ZNSt6vectorIiSaIiEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPiS1_EERKi: # @_ZNSt6vectorIiSaIiEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPiS1_EERKi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 64
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	cmpq	16(%rbx), %rdi
	je	.LBB2_4
# BB#1:
	leaq	-4(%rdi), %rdx
	movl	-4(%rdi), %eax
	movl	%eax, (%rdi)
	leaq	4(%rdi), %rax
	movq	%rax, 8(%rbx)
	movl	(%r15), %ebx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$2, %rax
	je	.LBB2_3
# BB#2:
	shlq	$2, %rax
	subq	%rax, %rdi
	movq	%r14, %rsi
	callq	memmove
.LBB2_3:                                # %_ZSt13copy_backwardIPiS0_ET0_T_S2_S1_.exit
	movl	%ebx, (%r14)
	jmp	.LBB2_15
.LBB2_4:                                # %_ZNKSt6vectorIiSaIiEE12_M_check_lenEmPKc.exit
	movq	(%rbx), %rbp
	subq	%rbp, %rdi
	sarq	$2, %rdi
	movl	$1, %ecx
	cmovneq	%rdi, %rcx
	leaq	(%rcx,%rdi), %r13
	movq	%r13, %rax
	shrq	$62, %rax
	movabsq	$4611686018427387903, %rax # imm = 0x3FFFFFFFFFFFFFFF
	cmovneq	%rax, %r13
	addq	%rdi, %rcx
	cmovbq	%rax, %r13
	testq	%r13, %r13
	je	.LBB2_5
# BB#6:
	cmpq	%rax, %r13
	ja	.LBB2_16
# BB#7:                                 # %_ZN9__gnu_cxx14__alloc_traitsISaIiEE8allocateERS1_m.exit.i
	leaq	(,%r13,4), %rdi
	callq	_Znwm
	movq	%rax, %r12
	movq	(%rbx), %rsi
	jmp	.LBB2_8
.LBB2_5:
	xorl	%r12d, %r12d
	movq	%rbp, %rsi
.LBB2_8:
	movq	%r14, %rax
	subq	%rbp, %rax
	sarq	$2, %rax
	movl	(%r15), %ecx
	movl	%ecx, (%r12,%rax,4)
	movq	%r14, %rdx
	subq	%rsi, %rdx
	movq	%rdx, %rbp
	sarq	$2, %rbp
	je	.LBB2_10
# BB#9:
	movq	%r12, %rdi
	movq	%rsi, %r15
	callq	memmove
	movq	%r15, %rsi
.LBB2_10:
	leaq	4(%r12,%rbp,4), %r15
	movq	8(%rbx), %rdx
	subq	%r14, %rdx
	movq	%rdx, %rbp
	sarq	$2, %rbp
	je	.LBB2_12
# BB#11:
	movq	%r15, %rdi
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%r14, %rsi
	callq	memmove
	movq	(%rsp), %rsi            # 8-byte Reload
.LBB2_12:
	leaq	(%r15,%rbp,4), %rbp
	testq	%rsi, %rsi
	je	.LBB2_14
# BB#13:
	movq	%rsi, %rdi
	callq	_ZdlPv
.LBB2_14:                               # %_ZNSt12_Vector_baseIiSaIiEE13_M_deallocateEPim.exit37
	movq	%r12, (%rbx)
	movq	%rbp, 8(%rbx)
	leaq	(%r12,%r13,4), %rax
	movq	%rax, 16(%rbx)
.LBB2_15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_16:
	callq	_ZSt17__throw_bad_allocv
.Lfunc_end2:
	.size	_ZNSt6vectorIiSaIiEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPiS1_EERKi, .Lfunc_end2-_ZNSt6vectorIiSaIiEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPiS1_EERKi
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_sieve.ii,@function
_GLOBAL__sub_I_sieve.ii:                # @_GLOBAL__sub_I_sieve.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end3:
	.size	_GLOBAL__sub_I_sieve.ii, .Lfunc_end3-_GLOBAL__sub_I_sieve.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Count: "
	.size	.L.str, 8

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_sieve.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
