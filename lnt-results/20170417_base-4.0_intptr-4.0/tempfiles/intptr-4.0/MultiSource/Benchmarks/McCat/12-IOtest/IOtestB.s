	.text
	.file	"IOtestB.bc"
	.globl	initminB
	.p2align	4, 0x90
	.type	initminB,@function
initminB:                               # @initminB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%edi, %edi
	callq	setac
	movb	$-1, (%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	initminB, .Lfunc_end0-initminB
	.cfi_endproc

	.globl	initmaxB
	.p2align	4, 0x90
	.type	initmaxB,@function
initmaxB:                               # @initmaxB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%edi, %edi
	callq	setac
	movb	$0, (%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	initmaxB, .Lfunc_end1-initmaxB
	.cfi_endproc

	.globl	initaddB
	.p2align	4, 0x90
	.type	initaddB,@function
initaddB:                               # @initaddB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%edi, %edi
	callq	setac
	movb	$0, (%rbx)
	popq	%rbx
	retq
.Lfunc_end2:
	.size	initaddB, .Lfunc_end2-initaddB
	.cfi_endproc

	.globl	initmultB
	.p2align	4, 0x90
	.type	initmultB,@function
initmultB:                              # @initmultB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%edi, %edi
	callq	setac
	movb	$1, (%rbx)
	popq	%rbx
	retq
.Lfunc_end3:
	.size	initmultB, .Lfunc_end3-initmultB
	.cfi_endproc

	.globl	stepminB
	.p2align	4, 0x90
	.type	stepminB,@function
stepminB:                               # @stepminB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	getac
	movq	%rax, %rdi
	callq	array
	movsbl	(%rbx), %edi
	movsbl	%al, %esi
	callq	min
	movb	%al, (%rbx)
	callq	getac
	leaq	1(%rax), %rdi
	popq	%rbx
	jmp	setac                   # TAILCALL
.Lfunc_end4:
	.size	stepminB, .Lfunc_end4-stepminB
	.cfi_endproc

	.globl	stepmaxB
	.p2align	4, 0x90
	.type	stepmaxB,@function
stepmaxB:                               # @stepmaxB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	getac
	movq	%rax, %rdi
	callq	array
	movsbl	(%rbx), %edi
	movsbl	%al, %esi
	callq	max
	movb	%al, (%rbx)
	callq	getac
	leaq	1(%rax), %rdi
	popq	%rbx
	jmp	setac                   # TAILCALL
.Lfunc_end5:
	.size	stepmaxB, .Lfunc_end5-stepmaxB
	.cfi_endproc

	.globl	stepaddB
	.p2align	4, 0x90
	.type	stepaddB,@function
stepaddB:                               # @stepaddB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	getac
	movq	%rax, %rdi
	callq	array
	movsbl	(%rbx), %edi
	movsbl	%al, %esi
	callq	add
	movb	%al, (%rbx)
	callq	getac
	leaq	1(%rax), %rdi
	popq	%rbx
	jmp	setac                   # TAILCALL
.Lfunc_end6:
	.size	stepaddB, .Lfunc_end6-stepaddB
	.cfi_endproc

	.globl	stepmultB
	.p2align	4, 0x90
	.type	stepmultB,@function
stepmultB:                              # @stepmultB
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
.Lcfi15:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	getac
	movq	%rax, %rdi
	callq	array
	movsbl	(%rbx), %edi
	movsbl	%al, %esi
	callq	mult
	movb	%al, (%rbx)
	callq	getac
	leaq	1(%rax), %rdi
	popq	%rbx
	jmp	setac                   # TAILCALL
.Lfunc_end7:
	.size	stepmultB, .Lfunc_end7-stepmultB
	.cfi_endproc

	.globl	testB
	.p2align	4, 0x90
	.type	testB,@function
testB:                                  # @testB
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 16
	callq	initarray
	movq	%rsp, %rdx
	movl	$initminB, %edi
	movl	$stepminB, %esi
	callq	loop
	leaq	1(%rsp), %rdx
	movl	$initmaxB, %edi
	movl	$stepmaxB, %esi
	callq	loop
	leaq	2(%rsp), %rdx
	movl	$initaddB, %edi
	movl	$stepaddB, %esi
	callq	loop
	leaq	3(%rsp), %rdx
	movl	$initmultB, %edi
	movl	$stepmultB, %esi
	callq	loop
	movsbl	(%rsp), %esi
	movsbl	1(%rsp), %edx
	movsbl	2(%rsp), %ecx
	movsbl	3(%rsp), %r8d
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	popq	%rax
	retq
.Lfunc_end8:
	.size	testB, .Lfunc_end8-testB
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"B %d min %d max %d add %d mult \n"
	.size	.L.str, 33


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
