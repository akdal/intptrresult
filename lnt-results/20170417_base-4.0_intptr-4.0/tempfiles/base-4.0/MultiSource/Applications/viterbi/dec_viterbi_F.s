	.text
	.file	"dec_viterbi_F.bc"
	.globl	dec_viterbi_F
	.p2align	4, 0x90
	.type	dec_viterbi_F,@function
dec_viterbi_F:                          # @dec_viterbi_F
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$36888, %rsp            # imm = 0x9018
.Lcfi6:
	.cfi_def_cfa_offset 36944
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %rbp
	movq	%r8, %rbx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rdx, %r13
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	cmpq	$0, (%r13)
	je	.LBB0_2
# BB#1:
	movq	8(%r13), %rdi
	callq	free
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, (%r13)
.LBB0_2:
	testq	%rbp, %rbp
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	je	.LBB0_5
# BB#3:                                 # %.thread
	movq	%rbp, (%r13)
	jmp	.LBB0_4
.LBB0_5:
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	movq	(%rax), %rbp
	movq	%rbp, (%r13)
	testq	%rbp, %rbp
	je	.LBB0_6
.LBB0_4:
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memset
.LBB0_7:                                # %bitvector_init.exit
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movq	%rbx, 8(%r13)
	movq	64(%rsp), %r14          # 8-byte Reload
	movq	(%r14), %r15
	leaq	(,%r15,8), %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r12
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	8(%r14), %rsi
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	144(%rsp), %rdi
	movl	$18304, %edx            # imm = 0x4780
	movq	88(%rsp), %rsi          # 8-byte Reload
	callq	memcpy
	testq	%rbp, %rbp
	je	.LBB0_8
# BB#9:                                 # %.lr.ph218
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rax
	addq	$8, %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rax
	addq	$8, %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%r13, 72(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	movq	40(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_10
	.p2align	4, 0x90
.LBB0_48:                               # %._crit_edge214._crit_edge
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movq	136(%rsp), %r13         # 8-byte Reload
	incq	%r13
	cmpq	40(%r14), %r13
	movl	$0, %eax
	cmoveq	%rax, %r13
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r15
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB0_10:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_14 Depth 2
                                        #     Child Loop BB0_19 Depth 2
                                        #     Child Loop BB0_23 Depth 2
                                        #     Child Loop BB0_33 Depth 2
                                        #     Child Loop BB0_36 Depth 2
                                        #     Child Loop BB0_43 Depth 2
                                        #     Child Loop BB0_46 Depth 2
	shlq	$3, %r15
	movq	%r12, %rdi
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	memcpy
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	memcpy
	cmpb	$1, 24(%r14,%r13)
	jne	.LBB0_11
# BB#12:                                # %.preheader193
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	8(%r14), %rax
	testq	%rax, %rax
	movq	8(%rsp), %rdi           # 8-byte Reload
	je	.LBB0_15
# BB#13:                                # %.lr.ph
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	8(%rcx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_14:                               #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	48(%r14,%rdx), %rsi
	movb	$1, %bl
	subb	%sil, %bl
	movsd	(%r12,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	shlq	$4, %rsi
	movq	8(%rcx,%rsi), %rsi
	addsd	(%rsi,%rdi,8), %xmm0
	movsd	%xmm0, (%r12,%rdx,8)
	movsd	(%rbp,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movsbq	%bl, %rsi
	shlq	$4, %rsi
	movq	8(%rcx,%rsi), %rsi
	addsd	(%rsi,%rdi,8), %xmm0
	movsd	%xmm0, (%rbp,%rdx,8)
	incq	%rdx
	cmpq	%rax, %rdx
	jb	.LBB0_14
.LBB0_15:                               # %._crit_edge
                                        #   in Loop: Header=BB0_10 Depth=1
	incq	%rdi
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_11:                               #   in Loop: Header=BB0_10 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB0_16:                               #   in Loop: Header=BB0_10 Depth=1
	movq	8(%r14), %rsi
	cmpb	$1, 31(%r14,%r13)
	movq	%r13, 136(%rsp)         # 8-byte Spill
	jne	.LBB0_21
# BB#17:                                # %.preheader192
                                        #   in Loop: Header=BB0_10 Depth=1
	testq	%rsi, %rsi
	je	.LBB0_20
# BB#18:                                # %.lr.ph197
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_19:                               #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	176(%r14,%rcx), %rdx
	movb	$1, %bl
	subb	%dl, %bl
	movsd	(%r12,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	shlq	$4, %rdx
	movq	8(%rax,%rdx), %rdx
	addsd	(%rdx,%rdi,8), %xmm0
	movsd	%xmm0, (%r12,%rcx,8)
	movsd	(%rbp,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsbq	%bl, %rdx
	shlq	$4, %rdx
	movq	8(%rax,%rdx), %rdx
	addsd	(%rdx,%rdi,8), %xmm0
	movsd	%xmm0, (%rbp,%rcx,8)
	incq	%rcx
	cmpq	%rsi, %rcx
	jb	.LBB0_19
.LBB0_20:                               # %._crit_edge198
                                        #   in Loop: Header=BB0_10 Depth=1
	incq	%rdi
.LBB0_21:                               # %._crit_edge235
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	cmpq	$2, %rsi
	jb	.LBB0_25
# BB#22:                                # %.lr.ph205
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	96(%rsp), %rcx          # 8-byte Reload
	shrq	%rcx
	movq	%rcx, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,8), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	18449(%rsp), %r12
	movq	120(%rsp), %r13         # 8-byte Reload
	movq	128(%rsp), %rbp         # 8-byte Reload
	leaq	144(%rsp), %r15
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_23:                               #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	143(%r15), %rbx
	movsd	-8(%r13), %xmm0         # xmm0 = mem[0],zero
	movsd	(%r13), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jae	.LBB0_24
# BB#37:                                # %.loopexit189.loopexit
                                        #   in Loop: Header=BB0_23 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movsd	%xmm1, (%rax,%r14,8)
	movb	$1, -1(%r12)
	movl	$143, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	jmp	.LBB0_38
	.p2align	4, 0x90
.LBB0_24:                               # %.loopexit189.loopexit222
                                        #   in Loop: Header=BB0_23 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movsd	%xmm0, (%rax,%r14,8)
	movb	$0, -1(%r12)
	movl	$143, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
.LBB0_38:                               # %.loopexit189
                                        #   in Loop: Header=BB0_23 Depth=2
	callq	memcpy
	movsd	-8(%rbp), %xmm0         # xmm0 = mem[0],zero
	movsd	(%rbp), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jae	.LBB0_39
# BB#40:                                # %.loopexit.loopexit
                                        #   in Loop: Header=BB0_23 Depth=2
	movq	104(%rsp), %rax         # 8-byte Reload
	movsd	%xmm1, (%rax,%r14,8)
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12), %rdi
	movb	$1, -1(%rax,%r12)
	movl	$143, %edx
	movq	%rbx, %rsi
	jmp	.LBB0_41
	.p2align	4, 0x90
.LBB0_39:                               # %.loopexit.loopexit221
                                        #   in Loop: Header=BB0_23 Depth=2
	movq	104(%rsp), %rax         # 8-byte Reload
	movsd	%xmm0, (%rax,%r14,8)
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12), %rdi
	movb	$0, -1(%rax,%r12)
	movl	$143, %edx
	movq	%r15, %rsi
.LBB0_41:                               # %.loopexit
                                        #   in Loop: Header=BB0_23 Depth=2
	callq	memcpy
	incq	%r14
	addq	$286, %r15              # imm = 0x11E
	addq	$16, %rbp
	addq	$16, %r13
	addq	$144, %r12
	cmpq	80(%rsp), %r14          # 8-byte Folded Reload
	jb	.LBB0_23
.LBB0_25:                               # %.preheader191
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	96(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB0_26
# BB#30:                                # %.lr.ph208.preheader
                                        #   in Loop: Header=BB0_10 Depth=1
	leaq	-1(%rbx), %rdx
	movq	%rbx, %rsi
	andq	$3, %rsi
	movq	72(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_31
# BB#32:                                # %.lr.ph208.prol.preheader
                                        #   in Loop: Header=BB0_10 Depth=1
	leaq	18591(%rsp), %rdi
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_33:                               # %.lr.ph208.prol
                                        #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$1, (%rdi)
	adcq	$0, %rax
	incq	%rcx
	addq	$144, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB0_33
	jmp	.LBB0_34
	.p2align	4, 0x90
.LBB0_26:                               #   in Loop: Header=BB0_10 Depth=1
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	movq	72(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_27
	.p2align	4, 0x90
.LBB0_31:                               #   in Loop: Header=BB0_10 Depth=1
	xorl	%ecx, %ecx
	xorl	%eax, %eax
.LBB0_34:                               # %.lr.ph208.prol.loopexit
                                        #   in Loop: Header=BB0_10 Depth=1
	cmpq	$3, %rdx
	jb	.LBB0_27
# BB#35:                                # %.lr.ph208.preheader.new
                                        #   in Loop: Header=BB0_10 Depth=1
	leaq	(%rcx,%rcx,8), %rdx
	shlq	$4, %rdx
	leaq	19023(%rsp), %rsi
	addq	%rsi, %rdx
	.p2align	4, 0x90
.LBB0_36:                               # %.lr.ph208
                                        #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$1, -432(%rdx)
	adcq	$0, %rax
	cmpb	$1, -288(%rdx)
	adcq	$0, %rax
	cmpb	$1, -144(%rdx)
	adcq	$0, %rax
	cmpb	$1, (%rdx)
	adcq	$0, %rax
	addq	$4, %rcx
	addq	$576, %rdx              # imm = 0x240
	cmpq	%rbx, %rcx
	jb	.LBB0_36
.LBB0_27:                               # %._crit_edge209
                                        #   in Loop: Header=BB0_10 Depth=1
	shrq	%rbx
	movq	8(%r13), %rcx
	cmpq	%rbx, %rax
	setb	%al
	movb	%al, (%rcx,%rbp)
	movq	8(%r14), %r15
	testq	%r15, %r15
	je	.LBB0_47
# BB#28:                                # %.preheader.preheader
                                        #   in Loop: Header=BB0_10 Depth=1
	leaq	-1(%r15), %r14
	movq	%r15, %r13
	andq	$3, %r13
	je	.LBB0_29
# BB#42:                                # %.preheader.prol.preheader
                                        #   in Loop: Header=BB0_10 Depth=1
	leaq	144(%rsp), %rbx
	leaq	18448(%rsp), %rbp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_43:                               # %.preheader.prol
                                        #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$143, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	memcpy
	incq	%r12
	addq	$144, %rbp
	addq	$143, %rbx
	cmpq	%r12, %r13
	jne	.LBB0_43
	jmp	.LBB0_44
	.p2align	4, 0x90
.LBB0_29:                               #   in Loop: Header=BB0_10 Depth=1
	xorl	%r12d, %r12d
.LBB0_44:                               # %.preheader.prol.loopexit
                                        #   in Loop: Header=BB0_10 Depth=1
	cmpq	$3, %r14
	movq	72(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	jb	.LBB0_47
# BB#45:                                # %.preheader.preheader.new
                                        #   in Loop: Header=BB0_10 Depth=1
	leaq	(%r12,%r12,8), %rbp
	shlq	$4, %rbp
	leaq	18880(%rsp), %rax
	addq	%rax, %rbp
	imulq	$143, %r12, %rbx
	leaq	573(%rsp), %rax
	addq	%rax, %rbx
	.p2align	4, 0x90
.LBB0_46:                               # %.preheader
                                        #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-429(%rbx), %rdi
	leaq	-432(%rbp), %rsi
	movl	$143, %edx
	callq	memcpy
	leaq	-286(%rbx), %rdi
	leaq	-288(%rbp), %rsi
	movl	$143, %edx
	callq	memcpy
	leaq	-143(%rbx), %rdi
	leaq	-144(%rbp), %rsi
	movl	$143, %edx
	callq	memcpy
	movl	$143, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	memcpy
	addq	$4, %r12
	addq	$576, %rbp              # imm = 0x240
	addq	$572, %rbx              # imm = 0x23C
	cmpq	%r15, %r12
	jb	.LBB0_46
.LBB0_47:                               # %._crit_edge214
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	48(%rsp), %rbx          # 8-byte Reload
	incq	%rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpq	(%r13), %rax
	jb	.LBB0_48
# BB#49:                                # %._crit_edge219.loopexit
	movq	8(%r13), %rdi
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_50
.LBB0_8:
	xorl	%ebx, %ebx
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	80(%rsp), %rdi          # 8-byte Reload
.LBB0_50:                               # %._crit_edge219
	movq	%rbx, %rsi
	callq	realloc
	movq	%rax, 8(%r13)
	movq	%rbx, (%r13)
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdx
	movq	8(%rax), %rdi
	shlq	$3, %rdx
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	callq	memcpy
	leaq	144(%rsp), %rsi
	movl	$18304, %edx            # imm = 0x4780
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	memcpy
	movq	%rbx, %rdi
	callq	free
	movq	%rbp, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	addq	$36888, %rsp            # imm = 0x9018
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_6:
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	jmp	.LBB0_7
.Lfunc_end0:
	.size	dec_viterbi_F, .Lfunc_end0-dec_viterbi_F
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
