	.text
	.file	"Error.bc"
	.globl	_ZN8NWindows6NError15MyFormatMessageEjR11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NWindows6NError15MyFormatMessageEjR11CStringBaseIwE,@function
_ZN8NWindows6NError15MyFormatMessageEjR11CStringBaseIwE: # @_ZN8NWindows6NError15MyFormatMessageEjR11CStringBaseIwE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi6:
	.cfi_def_cfa_offset 336
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %r12d
	movq	$0, 8(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r15
	movq	%r15, (%rsp)
	movb	$0, (%r15)
	movl	$4, 12(%rsp)
	cmpl	$-2147287040, %r12d     # imm = 0x80030000
	jg	.LBB0_4
# BB#1:
	leal	2147467263(%r12), %eax
	cmpl	$4, %eax
	ja	.LBB0_16
# BB#2:
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_3:
	movl	$.L.str.1, %ebx
	jmp	.LBB0_17
.LBB0_4:
	cmpl	$-2147024810, %r12d     # imm = 0x80070056
	jg	.LBB0_8
# BB#5:
	cmpl	$-2147287039, %r12d     # imm = 0x80030001
	je	.LBB0_14
# BB#6:
	cmpl	$-2147024882, %r12d     # imm = 0x8007000E
	jne	.LBB0_16
# BB#7:
	movl	$.L.str.6, %ebx
	jmp	.LBB0_17
.LBB0_8:
	cmpl	$-2147024809, %r12d     # imm = 0x80070057
	je	.LBB0_15
# BB#9:
	cmpl	$1048867, %r12d         # imm = 0x100123
	jne	.LBB0_16
# BB#10:
	movl	$.L.str, %ebx
	jmp	.LBB0_17
.LBB0_11:
	movl	$.L.str.2, %ebx
	jmp	.LBB0_17
.LBB0_12:
	movl	$.L.str.3, %ebx
	jmp	.LBB0_17
.LBB0_13:
	movl	$.L.str.4, %ebx
	jmp	.LBB0_17
.LBB0_14:
	movl	$.L.str.5, %ebx
	jmp	.LBB0_17
.LBB0_15:
	movl	$.L.str.7, %ebx
	jmp	.LBB0_17
.LBB0_16:
	movl	%r12d, %edi
	callq	strerror
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_60
.LBB0_17:                               # %.thread
	movl	$0, 8(%rsp)
	movb	$0, (%r15)
	movl	$-1, %r13d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB0_18:                               # =>This Inner Loop Header: Depth=1
	incl	%r13d
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB0_18
# BB#19:                                # %_Z11MyStringLenIcEiPKT_.exit.i
	cmpl	$3, %r13d
	je	.LBB0_22
# BB#20:
	leal	1(%r13), %ebp
	movslq	%ebp, %rax
	cmpl	$-1, %r13d
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp0:
	callq	_Znam
	movq	%rax, %r12
.Ltmp1:
# BB#21:                                # %._crit_edge17.i.i
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	%r12, (%rsp)
	movb	$0, (%r12)
	movl	%ebp, 12(%rsp)
	movq	%r12, %r15
	.p2align	4, 0x90
.LBB0_22:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %eax
	incq	%rbx
	movb	%al, (%r15)
	incq	%r15
	testb	%al, %al
	jne	.LBB0_22
.LBB0_23:
	movl	%r13d, 8(%rsp)
	movl	12(%rsp), %ebp
	movl	%ebp, %ecx
	subl	%r13d, %ecx
	cmpl	$16, %ecx
	jg	.LBB0_44
# BB#24:
	leal	-1(%rcx), %eax
	cmpl	$8, %ebp
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebp
	jl	.LBB0_26
# BB#25:                                # %select.true.sink
	movl	%ebp, %edx
	shrl	$31, %edx
	addl	%ebp, %edx
	sarl	%edx
.LBB0_26:                               # %select.end
	addl	%edx, %eax
	movl	$17, %esi
	subl	%ecx, %esi
	cmpl	$16, %eax
	cmovgel	%edx, %esi
	leal	1(%rsi,%rbp), %r15d
	cmpl	%ebp, %r15d
	je	.LBB0_44
# BB#27:
	addl	%ebp, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp5:
	callq	_Znam
	movq	%rax, %r12
.Ltmp6:
# BB#28:                                # %.noexc48
	testl	%ebp, %ebp
	jle	.LBB0_43
# BB#29:                                # %.preheader.i.i38
	movq	(%rsp), %rdi
	testl	%r13d, %r13d
	jle	.LBB0_41
# BB#30:                                # %.lr.ph.preheader.i.i39
	movslq	%r13d, %rax
	cmpl	$31, %r13d
	jbe	.LBB0_34
# BB#31:                                # %min.iters.checked67
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB0_34
# BB#32:                                # %vector.memcheck78
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB0_78
# BB#33:                                # %vector.memcheck78
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_78
.LBB0_34:
	xorl	%ecx, %ecx
.LBB0_35:                               # %.lr.ph.i.i44.preheader
	movl	%r13d, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB0_38
# BB#36:                                # %.lr.ph.i.i44.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB0_37:                               # %.lr.ph.i.i44.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r12,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_37
.LBB0_38:                               # %.lr.ph.i.i44.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB0_42
# BB#39:                                # %.lr.ph.i.i44.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_40:                               # %.lr.ph.i.i44
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB0_40
	jmp	.LBB0_42
.LBB0_41:                               # %._crit_edge.i.i40
	testq	%rdi, %rdi
	je	.LBB0_43
.LBB0_42:                               # %._crit_edge.thread.i.i46
	callq	_ZdaPv
.LBB0_43:                               # %._crit_edge17.i.i37
	movq	%r12, (%rsp)
	movslq	%r13d, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%rsp)
.LBB0_44:                               # %.noexc36
	movq	(%rsp), %rax
	movslq	%r13d, %rcx
	movb	$32, (%rax,%rcx)
	movb	$32, 1(%rax,%rcx)
	movb	$32, 2(%rax,%rcx)
	movb	$32, 3(%rax,%rcx)
	movb	$32, 4(%rax,%rcx)
	movb	$32, 5(%rax,%rcx)
	movb	$32, 6(%rax,%rcx)
	movb	$32, 7(%rax,%rcx)
	movb	$32, 8(%rax,%rcx)
	movb	$32, 9(%rax,%rcx)
	movb	$32, 10(%rax,%rcx)
	movb	$32, 11(%rax,%rcx)
	movb	$32, 12(%rax,%rcx)
	movb	$32, 13(%rax,%rcx)
	movb	$32, 14(%rax,%rcx)
	movb	$32, 15(%rax,%rcx)
	movb	$0, 16(%rax,%rcx)
	addl	$16, 8(%rsp)
.Ltmp8:
	leaq	16(%rsp), %rbx
	movq	%rsp, %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp9:
# BB#45:
	cmpq	%r14, %rbx
	je	.LBB0_55
# BB#46:
	movl	$0, 8(%r14)
	movq	(%r14), %rbx
	movl	$0, (%rbx)
	movslq	24(%rsp), %r15
	incq	%r15
	movl	12(%r14), %ebp
	cmpl	%ebp, %r15d
	je	.LBB0_52
# BB#47:
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp11:
	callq	_Znam
	movq	%rax, %r12
.Ltmp12:
# BB#48:                                # %.noexc53
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB0_51
# BB#49:                                # %.noexc53
	testl	%ebp, %ebp
	jle	.LBB0_51
# BB#50:                                # %._crit_edge.thread.i.i50
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r14), %rax
.LBB0_51:                               # %._crit_edge16.i.i
	movq	%r12, (%r14)
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
	movq	%r12, %rbx
.LBB0_52:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	16(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_53:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB0_53
# BB#54:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	24(%rsp), %eax
	movl	%eax, 8(%r14)
	testq	%rdi, %rdi
	jne	.LBB0_56
	jmp	.LBB0_57
.LBB0_55:                               # %._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_57
.LBB0_56:
	callq	_ZdaPv
.LBB0_57:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_59
# BB#58:
	callq	_ZdaPv
.LBB0_59:                               # %_ZN11CStringBaseIcED2Ev.exit
	movb	$1, %al
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_60:
	xorl	%r15d, %r15d
	leaq	16(%rsp), %rbx
	movl	$256, %esi              # imm = 0x100
	movl	$.L.str.8, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%r12d, %ecx
	callq	snprintf
	movb	$0, 271(%rsp)
	movl	$0, 8(%rsp)
	movq	(%rsp), %rax
	movb	$0, (%rax)
	.p2align	4, 0x90
.LBB0_61:                               # =>This Inner Loop Header: Depth=1
	incl	%r15d
	cmpb	$0, (%rbx)
	leaq	1(%rbx), %rbx
	jne	.LBB0_61
# BB#62:                                # %_Z11MyStringLenIcEiPKT_.exit.i14
	leal	-1(%r15), %r13d
	movl	12(%rsp), %ebp
	cmpl	%r15d, %ebp
	jne	.LBB0_64
# BB#63:                                # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i16
	movq	(%rsp), %rbx
	jmp	.LBB0_89
.LBB0_64:
	movslq	%r15d, %rax
	cmpl	$-1, %r13d
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp2:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp3:
# BB#65:                                # %.noexc29
	movslq	8(%rsp), %r12
	testl	%ebp, %ebp
	jle	.LBB0_88
# BB#66:                                # %.preheader.i.i17
	movq	(%rsp), %rdi
	testl	%r12d, %r12d
	jle	.LBB0_86
# BB#67:                                # %.lr.ph.preheader.i.i18
	cmpl	$31, %r12d
	jbe	.LBB0_71
# BB#68:                                # %min.iters.checked
	movq	%r12, %rax
	andq	$-32, %rax
	je	.LBB0_71
# BB#69:                                # %vector.memcheck
	leaq	(%rdi,%r12), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB0_91
# BB#70:                                # %vector.memcheck
	leaq	(%rbx,%r12), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB0_91
.LBB0_71:
	xorl	%eax, %eax
.LBB0_72:                               # %.lr.ph.i.i23.preheader
	movl	%r12d, %edx
	subl	%eax, %edx
	leaq	-1(%r12), %rsi
	subq	%rax, %rsi
	andq	$7, %rdx
	je	.LBB0_75
# BB#73:                                # %.lr.ph.i.i23.prol.preheader
	negq	%rdx
.LBB0_74:                               # %.lr.ph.i.i23.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, (%rbx,%rax)
	incq	%rax
	incq	%rdx
	jne	.LBB0_74
.LBB0_75:                               # %.lr.ph.i.i23.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB0_87
# BB#76:                                # %.lr.ph.i.i23.preheader.new
	movq	%r12, %rsi
	subq	%rax, %rsi
	leaq	7(%rbx,%rax), %rdx
	leaq	7(%rdi,%rax), %rax
.LBB0_77:                               # %.lr.ph.i.i23
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rax), %ecx
	movb	%cl, -7(%rdx)
	movzbl	-6(%rax), %ecx
	movb	%cl, -6(%rdx)
	movzbl	-5(%rax), %ecx
	movb	%cl, -5(%rdx)
	movzbl	-4(%rax), %ecx
	movb	%cl, -4(%rdx)
	movzbl	-3(%rax), %ecx
	movb	%cl, -3(%rdx)
	movzbl	-2(%rax), %ecx
	movb	%cl, -2(%rdx)
	movzbl	-1(%rax), %ecx
	movb	%cl, -1(%rdx)
	movzbl	(%rax), %ecx
	movb	%cl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rax
	addq	$-8, %rsi
	jne	.LBB0_77
	jmp	.LBB0_87
.LBB0_78:                               # %vector.body63.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_81
# BB#79:                                # %vector.body63.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_80:                               # %vector.body63.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%r12,%rbp)
	movups	%xmm1, 16(%r12,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB0_80
	jmp	.LBB0_82
.LBB0_81:
	xorl	%ebp, %ebp
.LBB0_82:                               # %vector.body63.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB0_85
# BB#83:                                # %vector.body63.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r12,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
	.p2align	4, 0x90
.LBB0_84:                               # %vector.body63
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB0_84
.LBB0_85:                               # %middle.block64
	cmpq	%rcx, %rax
	jne	.LBB0_35
	jmp	.LBB0_42
.LBB0_86:                               # %._crit_edge.i.i19
	testq	%rdi, %rdi
	je	.LBB0_88
.LBB0_87:                               # %._crit_edge.thread.i.i24
	callq	_ZdaPv
.LBB0_88:                               # %._crit_edge17.i.i25
	movq	%rbx, (%rsp)
	movb	$0, (%rbx,%r12)
	movl	%r15d, 12(%rsp)
.LBB0_89:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i28.preheader
	leaq	16(%rsp), %rax
	.p2align	4, 0x90
.LBB0_90:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i28
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB0_90
	jmp	.LBB0_23
.LBB0_91:                               # %vector.body.preheader
	leaq	-32(%rax), %rcx
	movl	%ecx, %edx
	shrl	$5, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB0_94
# BB#92:                                # %vector.body.prol.preheader
	negq	%rdx
	xorl	%esi, %esi
.LBB0_93:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rsi), %xmm0
	movups	16(%rdi,%rsi), %xmm1
	movups	%xmm0, (%rbx,%rsi)
	movups	%xmm1, 16(%rbx,%rsi)
	addq	$32, %rsi
	incq	%rdx
	jne	.LBB0_93
	jmp	.LBB0_95
.LBB0_94:
	xorl	%esi, %esi
.LBB0_95:                               # %vector.body.prol.loopexit
	cmpq	$96, %rcx
	jb	.LBB0_98
# BB#96:                                # %vector.body.preheader.new
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%rbx,%rsi), %rdx
	leaq	112(%rdi,%rsi), %rsi
.LBB0_97:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-128, %rcx
	jne	.LBB0_97
.LBB0_98:                               # %middle.block
	cmpq	%rax, %r12
	jne	.LBB0_72
	jmp	.LBB0_87
.LBB0_99:
.Ltmp4:
	jmp	.LBB0_104
.LBB0_100:
.Ltmp13:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_105
# BB#101:
	callq	_ZdaPv
	jmp	.LBB0_105
.LBB0_102:
.Ltmp7:
	jmp	.LBB0_104
.LBB0_103:
.Ltmp10:
.LBB0_104:
	movq	%rax, %rbx
.LBB0_105:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_107
# BB#106:
	callq	_ZdaPv
.LBB0_107:                              # %_ZN11CStringBaseIcED2Ev.exit55
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZN8NWindows6NError15MyFormatMessageEjR11CStringBaseIwE, .Lfunc_end0-_ZN8NWindows6NError15MyFormatMessageEjR11CStringBaseIwE
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_3
	.quad	.LBB0_11
	.quad	.LBB0_16
	.quad	.LBB0_12
	.quad	.LBB0_13
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\320"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp0           #   Call between .Ltmp0 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp3-.Ltmp2           #   Call between .Ltmp2 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Lfunc_end0-.Ltmp3      #   Call between .Ltmp3 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"No more files"
	.size	.L.str, 14

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"E_NOTIMPL"
	.size	.L.str.1, 10

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"E_NOINTERFACE"
	.size	.L.str.2, 14

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"E_ABORT"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"E_FAIL"
	.size	.L.str.4, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"STG_E_INVALIDFUNCTION"
	.size	.L.str.5, 22

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"E_OUTOFMEMORY"
	.size	.L.str.6, 14

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"E_INVALIDARG"
	.size	.L.str.7, 13

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"error #%x"
	.size	.L.str.8, 10


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
