	.text
	.file	"zmatrix.bc"
	.globl	zmatrix_init
	.p2align	4, 0x90
	.type	zmatrix_init,@function
zmatrix_init:                           # @zmatrix_init
	.cfi_startproc
# BB#0:
	movw	$44, gs_identity_matrix+8(%rip)
	movw	$44, gs_identity_matrix+24(%rip)
	movw	$44, gs_identity_matrix+40(%rip)
	movw	$44, gs_identity_matrix+56(%rip)
	movw	$44, gs_identity_matrix+72(%rip)
	movw	$44, gs_identity_matrix+88(%rip)
	retq
.Lfunc_end0:
	.size	zmatrix_init, .Lfunc_end0-zmatrix_init
	.cfi_endproc

	.globl	zcurrentmatrix
	.p2align	4, 0x90
	.type	zcurrentmatrix,@function
zcurrentmatrix:                         # @zcurrentmatrix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	callq	write_matrix
	testl	%eax, %eax
	js	.LBB1_1
# BB#2:
	movq	igs(%rip), %rdi
	movq	(%rbx), %rsi
	callq	gs_currentmatrix
	jmp	.LBB1_3
.LBB1_1:
	movl	%eax, %ebp
.LBB1_3:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	zcurrentmatrix, .Lfunc_end1-zcurrentmatrix
	.cfi_endproc

	.globl	zsetmatrix
	.p2align	4, 0x90
	.type	zsetmatrix,@function
zsetmatrix:                             # @zsetmatrix
	.cfi_startproc
# BB#0:
	subq	$104, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 112
	leaq	8(%rsp), %rsi
	xorl	%eax, %eax
	callq	read_matrix
	testl	%eax, %eax
	js	.LBB2_3
# BB#1:
	movq	igs(%rip), %rdi
	leaq	8(%rsp), %rsi
	callq	gs_setmatrix
	testl	%eax, %eax
	js	.LBB2_3
# BB#2:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB2_3:
	addq	$104, %rsp
	retq
.Lfunc_end2:
	.size	zsetmatrix, .Lfunc_end2-zsetmatrix
	.cfi_endproc

	.globl	ztranslate
	.p2align	4, 0x90
	.type	ztranslate,@function
ztranslate:                             # @ztranslate
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	xorl	%eax, %eax
	callq	write_matrix
	testl	%eax, %eax
	js	.LBB3_1
# BB#3:
	movq	(%rbx), %r14
	leaq	-16(%rbx), %rdi
	movq	%rsp, %rdx
	movl	$2, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB3_7
# BB#4:                                 # %.thread
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movq	%r14, %rdi
	callq	gs_make_translation
	movups	(%rbx), %xmm0
	movups	%xmm0, -32(%rbx)
	testl	%eax, %eax
	jns	.LBB3_6
	jmp	.LBB3_7
.LBB3_1:
	movq	%rsp, %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	num_params
	testl	%eax, %eax
	js	.LBB3_7
# BB#2:
	movq	igs(%rip), %rdi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	callq	gs_translate
	testl	%eax, %eax
	js	.LBB3_7
.LBB3_6:
	addq	$-32, osp(%rip)
.LBB3_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	ztranslate, .Lfunc_end3-ztranslate
	.cfi_endproc

	.globl	zscale
	.p2align	4, 0x90
	.type	zscale,@function
zscale:                                 # @zscale
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	xorl	%eax, %eax
	callq	write_matrix
	testl	%eax, %eax
	js	.LBB4_1
# BB#3:
	movq	(%rbx), %r14
	leaq	-16(%rbx), %rdi
	movq	%rsp, %rdx
	movl	$2, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB4_7
# BB#4:                                 # %.thread
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movq	%r14, %rdi
	callq	gs_make_scaling
	movups	(%rbx), %xmm0
	movups	%xmm0, -32(%rbx)
	testl	%eax, %eax
	jns	.LBB4_6
	jmp	.LBB4_7
.LBB4_1:
	movq	%rsp, %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	num_params
	testl	%eax, %eax
	js	.LBB4_7
# BB#2:
	movq	igs(%rip), %rdi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	callq	gs_scale
	testl	%eax, %eax
	js	.LBB4_7
.LBB4_6:
	addq	$-32, osp(%rip)
.LBB4_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	zscale, .Lfunc_end4-zscale
	.cfi_endproc

	.globl	zrotate
	.p2align	4, 0x90
	.type	zrotate,@function
zrotate:                                # @zrotate
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorl	%eax, %eax
	callq	write_matrix
	testl	%eax, %eax
	js	.LBB5_1
# BB#3:
	movq	(%rbx), %r15
	leaq	-16(%rbx), %r14
	leaq	12(%rsp), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	num_params
	testl	%eax, %eax
	js	.LBB5_7
# BB#4:                                 # %.thread
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movq	%r15, %rdi
	callq	gs_make_rotation
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
	testl	%eax, %eax
	jns	.LBB5_6
	jmp	.LBB5_7
.LBB5_1:
	leaq	12(%rsp), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	num_params
	testl	%eax, %eax
	js	.LBB5_7
# BB#2:
	movq	igs(%rip), %rdi
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	callq	gs_rotate
	testl	%eax, %eax
	js	.LBB5_7
.LBB5_6:
	addq	$-16, osp(%rip)
.LBB5_7:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	zrotate, .Lfunc_end5-zrotate
	.cfi_endproc

	.globl	zconcat
	.p2align	4, 0x90
	.type	zconcat,@function
zconcat:                                # @zconcat
	.cfi_startproc
# BB#0:
	subq	$104, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 112
	leaq	8(%rsp), %rsi
	xorl	%eax, %eax
	callq	read_matrix
	testl	%eax, %eax
	js	.LBB6_3
# BB#1:
	movq	igs(%rip), %rdi
	leaq	8(%rsp), %rsi
	callq	gs_concat
	testl	%eax, %eax
	js	.LBB6_3
# BB#2:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB6_3:
	addq	$104, %rsp
	retq
.Lfunc_end6:
	.size	zconcat, .Lfunc_end6-zconcat
	.cfi_endproc

	.globl	zconcatmatrix
	.p2align	4, 0x90
	.type	zconcatmatrix,@function
zconcatmatrix:                          # @zconcatmatrix
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 24
	subq	$200, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 224
.Lcfi27:
	.cfi_offset %rbx, -24
.Lcfi28:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	-32(%rbx), %r14
	leaq	104(%rsp), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	read_matrix
	testl	%eax, %eax
	js	.LBB7_5
# BB#1:
	leaq	-16(%rbx), %rdi
	leaq	8(%rsp), %rsi
	xorl	%eax, %eax
	callq	read_matrix
	testl	%eax, %eax
	js	.LBB7_5
# BB#2:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	write_matrix
	testl	%eax, %eax
	js	.LBB7_5
# BB#3:
	movq	(%rbx), %rdx
	leaq	104(%rsp), %rdi
	leaq	8(%rsp), %rsi
	callq	gs_matrix_multiply
	testl	%eax, %eax
	js	.LBB7_5
# BB#4:
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
	addq	$-32, osp(%rip)
.LBB7_5:
	addq	$200, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	zconcatmatrix, .Lfunc_end7-zconcatmatrix
	.cfi_endproc

	.globl	ztransform
	.p2align	4, 0x90
	.type	ztransform,@function
ztransform:                             # @ztransform
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 16
	subq	$112, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 128
.Lcfi31:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %ecx
	shrl	$2, %ecx
	movl	%ecx, %edx
	andb	$63, %dl
	movl	$-20, %eax
	cmpb	$11, %dl
	ja	.LBB8_14
# BB#1:
	andl	$63, %ecx
	jmpq	*.LJTI8_0(,%rcx,8)
.LBB8_3:
	leaq	16(%rsp), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	read_matrix
	testl	%eax, %eax
	js	.LBB8_14
# BB#4:
	addq	$-16, %rbx
	movq	%rsp, %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	num_params
	testl	%eax, %eax
	js	.LBB8_14
# BB#5:
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	leaq	16(%rsp), %rdi
	leaq	8(%rsp), %rsi
	callq	gs_point_transform
	testl	%eax, %eax
	js	.LBB8_14
# BB#6:
	addq	$-16, osp(%rip)
	jmp	.LBB8_13
.LBB8_2:
	cvtsi2ssq	(%rbx), %xmm1
	jmp	.LBB8_8
.LBB8_7:
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
.LBB8_8:
	movss	%xmm1, 4(%rsp)
	movb	-8(%rbx), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB8_11
# BB#9:
	cmpb	$11, %cl
	jne	.LBB8_14
# BB#10:
	movss	-16(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB8_12
.LBB8_11:
	cvtsi2ssq	-16(%rbx), %xmm0
.LBB8_12:
	movss	%xmm0, (%rsp)
	movq	igs(%rip), %rdi
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	leaq	8(%rsp), %rsi
	callq	gs_transform
	testl	%eax, %eax
	js	.LBB8_14
.LBB8_13:
	movl	8(%rsp), %eax
	movl	%eax, -16(%rbx)
	movw	$44, -8(%rbx)
	movl	12(%rsp), %eax
	movl	%eax, (%rbx)
	movw	$44, 8(%rbx)
	xorl	%eax, %eax
.LBB8_14:                               # %common_transform.exit
	addq	$112, %rsp
	popq	%rbx
	retq
.Lfunc_end8:
	.size	ztransform, .Lfunc_end8-ztransform
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI8_0:
	.quad	.LBB8_3
	.quad	.LBB8_14
	.quad	.LBB8_14
	.quad	.LBB8_14
	.quad	.LBB8_14
	.quad	.LBB8_2
	.quad	.LBB8_14
	.quad	.LBB8_14
	.quad	.LBB8_14
	.quad	.LBB8_14
	.quad	.LBB8_3
	.quad	.LBB8_7

	.text
	.globl	common_transform
	.p2align	4, 0x90
	.type	common_transform,@function
common_transform:                       # @common_transform
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 24
	subq	$120, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 144
.Lcfi35:
	.cfi_offset %rbx, -24
.Lcfi36:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %r8
	movq	%rdi, %rbx
	movzwl	8(%rbx), %edx
	shrl	$2, %edx
	movl	%edx, %ecx
	andb	$63, %cl
	movl	$-20, %eax
	cmpb	$11, %cl
	ja	.LBB9_14
# BB#1:
	andl	$63, %edx
	jmpq	*.LJTI9_0(,%rdx,8)
.LBB9_3:
	leaq	24(%rsp), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	read_matrix
	testl	%eax, %eax
	js	.LBB9_14
# BB#4:
	addq	$-16, %rbx
	leaq	8(%rsp), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	num_params
	testl	%eax, %eax
	js	.LBB9_14
# BB#5:
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	leaq	24(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	*%r14
	testl	%eax, %eax
	js	.LBB9_14
# BB#6:
	addq	$-16, osp(%rip)
	jmp	.LBB9_13
.LBB9_2:
	cvtsi2ssq	(%rbx), %xmm1
	jmp	.LBB9_8
.LBB9_7:
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
.LBB9_8:
	movss	%xmm1, 12(%rsp)
	movb	-8(%rbx), %dl
	shrb	$2, %dl
	cmpb	$5, %dl
	je	.LBB9_11
# BB#9:
	cmpb	$11, %dl
	jne	.LBB9_14
# BB#10:
	movss	-16(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB9_12
.LBB9_11:
	cvtsi2ssq	-16(%rbx), %xmm0
.LBB9_12:
	movss	%xmm0, 8(%rsp)
	movq	igs(%rip), %rdi
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	leaq	16(%rsp), %rsi
	callq	*%r8
	testl	%eax, %eax
	js	.LBB9_14
.LBB9_13:
	movl	16(%rsp), %eax
	movl	%eax, -16(%rbx)
	movw	$44, -8(%rbx)
	movl	20(%rsp), %eax
	movl	%eax, (%rbx)
	movw	$44, 8(%rbx)
	xorl	%eax, %eax
.LBB9_14:
	addq	$120, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	common_transform, .Lfunc_end9-common_transform
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI9_0:
	.quad	.LBB9_3
	.quad	.LBB9_14
	.quad	.LBB9_14
	.quad	.LBB9_14
	.quad	.LBB9_14
	.quad	.LBB9_2
	.quad	.LBB9_14
	.quad	.LBB9_14
	.quad	.LBB9_14
	.quad	.LBB9_14
	.quad	.LBB9_3
	.quad	.LBB9_7

	.text
	.globl	zdtransform
	.p2align	4, 0x90
	.type	zdtransform,@function
zdtransform:                            # @zdtransform
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 16
	subq	$112, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 128
.Lcfi39:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %ecx
	shrl	$2, %ecx
	movl	%ecx, %edx
	andb	$63, %dl
	movl	$-20, %eax
	cmpb	$11, %dl
	ja	.LBB10_14
# BB#1:
	andl	$63, %ecx
	jmpq	*.LJTI10_0(,%rcx,8)
.LBB10_3:
	leaq	16(%rsp), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	read_matrix
	testl	%eax, %eax
	js	.LBB10_14
# BB#4:
	addq	$-16, %rbx
	movq	%rsp, %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	num_params
	testl	%eax, %eax
	js	.LBB10_14
# BB#5:
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	leaq	16(%rsp), %rdi
	leaq	8(%rsp), %rsi
	callq	gs_distance_transform
	testl	%eax, %eax
	js	.LBB10_14
# BB#6:
	addq	$-16, osp(%rip)
	jmp	.LBB10_13
.LBB10_2:
	cvtsi2ssq	(%rbx), %xmm1
	jmp	.LBB10_8
.LBB10_7:
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
.LBB10_8:
	movss	%xmm1, 4(%rsp)
	movb	-8(%rbx), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB10_11
# BB#9:
	cmpb	$11, %cl
	jne	.LBB10_14
# BB#10:
	movss	-16(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB10_12
.LBB10_11:
	cvtsi2ssq	-16(%rbx), %xmm0
.LBB10_12:
	movss	%xmm0, (%rsp)
	movq	igs(%rip), %rdi
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	leaq	8(%rsp), %rsi
	callq	gs_dtransform
	testl	%eax, %eax
	js	.LBB10_14
.LBB10_13:
	movl	8(%rsp), %eax
	movl	%eax, -16(%rbx)
	movw	$44, -8(%rbx)
	movl	12(%rsp), %eax
	movl	%eax, (%rbx)
	movw	$44, 8(%rbx)
	xorl	%eax, %eax
.LBB10_14:                              # %common_transform.exit
	addq	$112, %rsp
	popq	%rbx
	retq
.Lfunc_end10:
	.size	zdtransform, .Lfunc_end10-zdtransform
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI10_0:
	.quad	.LBB10_3
	.quad	.LBB10_14
	.quad	.LBB10_14
	.quad	.LBB10_14
	.quad	.LBB10_14
	.quad	.LBB10_2
	.quad	.LBB10_14
	.quad	.LBB10_14
	.quad	.LBB10_14
	.quad	.LBB10_14
	.quad	.LBB10_3
	.quad	.LBB10_7

	.text
	.globl	zitransform
	.p2align	4, 0x90
	.type	zitransform,@function
zitransform:                            # @zitransform
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 16
	subq	$112, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 128
.Lcfi42:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %ecx
	shrl	$2, %ecx
	movl	%ecx, %edx
	andb	$63, %dl
	movl	$-20, %eax
	cmpb	$11, %dl
	ja	.LBB11_14
# BB#1:
	andl	$63, %ecx
	jmpq	*.LJTI11_0(,%rcx,8)
.LBB11_3:
	leaq	16(%rsp), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	read_matrix
	testl	%eax, %eax
	js	.LBB11_14
# BB#4:
	addq	$-16, %rbx
	movq	%rsp, %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	num_params
	testl	%eax, %eax
	js	.LBB11_14
# BB#5:
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	leaq	16(%rsp), %rdi
	leaq	8(%rsp), %rsi
	callq	gs_point_transform_inverse
	testl	%eax, %eax
	js	.LBB11_14
# BB#6:
	addq	$-16, osp(%rip)
	jmp	.LBB11_13
.LBB11_2:
	cvtsi2ssq	(%rbx), %xmm1
	jmp	.LBB11_8
.LBB11_7:
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
.LBB11_8:
	movss	%xmm1, 4(%rsp)
	movb	-8(%rbx), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB11_11
# BB#9:
	cmpb	$11, %cl
	jne	.LBB11_14
# BB#10:
	movss	-16(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB11_12
.LBB11_11:
	cvtsi2ssq	-16(%rbx), %xmm0
.LBB11_12:
	movss	%xmm0, (%rsp)
	movq	igs(%rip), %rdi
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	leaq	8(%rsp), %rsi
	callq	gs_itransform
	testl	%eax, %eax
	js	.LBB11_14
.LBB11_13:
	movl	8(%rsp), %eax
	movl	%eax, -16(%rbx)
	movw	$44, -8(%rbx)
	movl	12(%rsp), %eax
	movl	%eax, (%rbx)
	movw	$44, 8(%rbx)
	xorl	%eax, %eax
.LBB11_14:                              # %common_transform.exit
	addq	$112, %rsp
	popq	%rbx
	retq
.Lfunc_end11:
	.size	zitransform, .Lfunc_end11-zitransform
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI11_0:
	.quad	.LBB11_3
	.quad	.LBB11_14
	.quad	.LBB11_14
	.quad	.LBB11_14
	.quad	.LBB11_14
	.quad	.LBB11_2
	.quad	.LBB11_14
	.quad	.LBB11_14
	.quad	.LBB11_14
	.quad	.LBB11_14
	.quad	.LBB11_3
	.quad	.LBB11_7

	.text
	.globl	zidtransform
	.p2align	4, 0x90
	.type	zidtransform,@function
zidtransform:                           # @zidtransform
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 16
	subq	$112, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 128
.Lcfi45:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %ecx
	shrl	$2, %ecx
	movl	%ecx, %edx
	andb	$63, %dl
	movl	$-20, %eax
	cmpb	$11, %dl
	ja	.LBB12_14
# BB#1:
	andl	$63, %ecx
	jmpq	*.LJTI12_0(,%rcx,8)
.LBB12_3:
	leaq	16(%rsp), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	read_matrix
	testl	%eax, %eax
	js	.LBB12_14
# BB#4:
	addq	$-16, %rbx
	movq	%rsp, %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	num_params
	testl	%eax, %eax
	js	.LBB12_14
# BB#5:
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	leaq	16(%rsp), %rdi
	leaq	8(%rsp), %rsi
	callq	gs_distance_transform_inverse
	testl	%eax, %eax
	js	.LBB12_14
# BB#6:
	addq	$-16, osp(%rip)
	jmp	.LBB12_13
.LBB12_2:
	cvtsi2ssq	(%rbx), %xmm1
	jmp	.LBB12_8
.LBB12_7:
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
.LBB12_8:
	movss	%xmm1, 4(%rsp)
	movb	-8(%rbx), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB12_11
# BB#9:
	cmpb	$11, %cl
	jne	.LBB12_14
# BB#10:
	movss	-16(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB12_12
.LBB12_11:
	cvtsi2ssq	-16(%rbx), %xmm0
.LBB12_12:
	movss	%xmm0, (%rsp)
	movq	igs(%rip), %rdi
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	leaq	8(%rsp), %rsi
	callq	gs_idtransform
	testl	%eax, %eax
	js	.LBB12_14
.LBB12_13:
	movl	8(%rsp), %eax
	movl	%eax, -16(%rbx)
	movw	$44, -8(%rbx)
	movl	12(%rsp), %eax
	movl	%eax, (%rbx)
	movw	$44, 8(%rbx)
	xorl	%eax, %eax
.LBB12_14:                              # %common_transform.exit
	addq	$112, %rsp
	popq	%rbx
	retq
.Lfunc_end12:
	.size	zidtransform, .Lfunc_end12-zidtransform
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI12_0:
	.quad	.LBB12_3
	.quad	.LBB12_14
	.quad	.LBB12_14
	.quad	.LBB12_14
	.quad	.LBB12_14
	.quad	.LBB12_2
	.quad	.LBB12_14
	.quad	.LBB12_14
	.quad	.LBB12_14
	.quad	.LBB12_14
	.quad	.LBB12_3
	.quad	.LBB12_7

	.text
	.globl	zinvertmatrix
	.p2align	4, 0x90
	.type	zinvertmatrix,@function
zinvertmatrix:                          # @zinvertmatrix
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 24
	subq	$104, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 128
.Lcfi49:
	.cfi_offset %rbx, -24
.Lcfi50:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	-16(%rbx), %r14
	leaq	8(%rsp), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	read_matrix
	testl	%eax, %eax
	js	.LBB13_4
# BB#1:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	write_matrix
	testl	%eax, %eax
	js	.LBB13_4
# BB#2:
	movq	(%rbx), %rsi
	leaq	8(%rsp), %rdi
	callq	gs_matrix_invert
	testl	%eax, %eax
	js	.LBB13_4
# BB#3:
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
	addq	$-16, osp(%rip)
.LBB13_4:
	addq	$104, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end13:
	.size	zinvertmatrix, .Lfunc_end13-zinvertmatrix
	.cfi_endproc

	.globl	zmatrix_op_init
	.p2align	4, 0x90
	.type	zmatrix_op_init,@function
zmatrix_op_init:                        # @zmatrix_op_init
	.cfi_startproc
# BB#0:
	movl	$zmatrix_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end14:
	.size	zmatrix_op_init, .Lfunc_end14-zmatrix_op_init
	.cfi_endproc

	.type	zmatrix_op_init.my_defs,@object # @zmatrix_op_init.my_defs
	.data
	.p2align	4
zmatrix_op_init.my_defs:
	.quad	.L.str
	.quad	zconcat
	.quad	.L.str.1
	.quad	zdtransform
	.quad	.L.str.2
	.quad	zconcatmatrix
	.quad	.L.str.3
	.quad	zcurrentmatrix
	.quad	.L.str.4
	.quad	zidtransform
	.quad	.L.str.5
	.quad	zinvertmatrix
	.quad	.L.str.6
	.quad	zitransform
	.quad	.L.str.7
	.quad	zrotate
	.quad	.L.str.8
	.quad	zscale
	.quad	.L.str.9
	.quad	zsetmatrix
	.quad	.L.str.10
	.quad	ztransform
	.quad	.L.str.11
	.quad	ztranslate
	.zero	16
	.size	zmatrix_op_init.my_defs, 208

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"1concat"
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"2dtransform"
	.size	.L.str.1, 12

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"3concatmatrix"
	.size	.L.str.2, 14

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"1currentmatrix"
	.size	.L.str.3, 15

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"2idtransform"
	.size	.L.str.4, 13

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"2invertmatrix"
	.size	.L.str.5, 14

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"2itransform"
	.size	.L.str.6, 12

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"1rotate"
	.size	.L.str.7, 8

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"2scale"
	.size	.L.str.8, 7

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"1setmatrix"
	.size	.L.str.9, 11

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"2transform"
	.size	.L.str.10, 11

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"2translate"
	.size	.L.str.11, 11


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
