	.text
	.file	"sieve.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movl	$170000, %r14d          # imm = 0x29810
	cmpl	$2, %edi
	jne	.LBB0_4
# BB#1:                                 # %.preheader28
	movq	8(%rsi), %rdi
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
	testl	%r14d, %r14d
	je	.LBB0_2
	.p2align	4, 0x90
.LBB0_4:                                # %.preheader27
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_9 Depth 2
                                        #     Child Loop BB0_7 Depth 2
	movl	$main.flags+2, %edi
	movl	$1, %esi
	movl	$8191, %edx             # imm = 0x1FFF
	callq	memset
	xorl	%ebx, %ebx
	movb	$1, %cl
	movl	$2, %eax
	testb	%cl, %cl
	jne	.LBB0_6
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_10:                               # %..preheader_crit_edge
                                        #   in Loop: Header=BB0_9 Depth=2
	movzbl	main.flags(%rax), %ecx
	testb	%cl, %cl
	je	.LBB0_9
.LBB0_6:                                #   in Loop: Header=BB0_4 Depth=1
	leaq	(%rax,%rax), %rcx
	cmpq	$8192, %rcx             # imm = 0x2000
	jg	.LBB0_8
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$0, main.flags(%rcx)
	addq	%rax, %rcx
	cmpq	$8193, %rcx             # imm = 0x2001
	jl	.LBB0_7
.LBB0_8:                                # %._crit_edge
                                        #   in Loop: Header=BB0_4 Depth=1
	incl	%ebx
.LBB0_9:                                #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rax
	cmpq	$8193, %rax             # imm = 0x2001
	jne	.LBB0_10
# BB#3:                                 # %.loopexit
                                        #   in Loop: Header=BB0_4 Depth=1
	decl	%r14d
	jne	.LBB0_4
.LBB0_2:                                # %._crit_edge35
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	main.flags,@object      # @main.flags
	.local	main.flags
	.comm	main.flags,8193,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Count: %d\n"
	.size	.L.str, 11


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
