	.text
	.file	"zarray.bc"
	.globl	zarray
	.p2align	4, 0x90
	.type	zarray,@function
zarray:                                 # @zarray
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movzwl	8(%r14), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB0_12
# BB#1:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	movl	$-15, %eax
	js	.LBB0_12
# BB#2:
	cmpq	$268435454, %rbx        # imm = 0xFFFFFFE
	ja	.LBB0_12
# BB#3:
	movl	$16, %esi
	movl	$.L.str, %edx
	movl	%ebx, %edi
	callq	alloc
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB0_4
# BB#5:
	movq	%rcx, (%r14)
	movw	$770, 8(%r14)           # imm = 0x302
	movw	%bx, 10(%r14)
	xorl	%eax, %eax
	testw	%bx, %bx
	je	.LBB0_12
# BB#6:                                 # %.lr.ph.preheader
	leal	-1(%rbx), %edx
	movw	%bx, %si
	andw	$7, %si
	je	.LBB0_9
# BB#7:                                 # %.lr.ph.prol.preheader
	negl	%esi
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	decl	%ebx
	movw	$32, 8(%rcx)
	addq	$16, %rcx
	incw	%si
	jne	.LBB0_8
.LBB0_9:                                # %.lr.ph.prol.loopexit
	movzwl	%dx, %edx
	cmpl	$7, %edx
	jb	.LBB0_12
# BB#10:                                # %.lr.ph.preheader.new
	addq	$120, %rcx
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movw	$32, -112(%rcx)
	movw	$32, -96(%rcx)
	movw	$32, -80(%rcx)
	movw	$32, -64(%rcx)
	movw	$32, -48(%rcx)
	movw	$32, -32(%rcx)
	movw	$32, -16(%rcx)
	movw	$32, (%rcx)
	subq	$-128, %rcx
	addw	$-8, %bx
	jne	.LBB0_11
	jmp	.LBB0_12
.LBB0_4:
	movl	$-25, %eax
.LBB0_12:                               # %make_array.exit.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	zarray, .Lfunc_end0-zarray
	.cfi_endproc

	.globl	make_array
	.p2align	4, 0x90
	.type	make_array,@function
make_array:                             # @make_array
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -40
.Lcfi11:
	.cfi_offset %r12, -32
.Lcfi12:
	.cfi_offset %r14, -24
.Lcfi13:
	.cfi_offset %r15, -16
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movzwl	8(%rbx), %edx
	andl	$252, %edx
	movl	$-20, %eax
	cmpl	$20, %edx
	jne	.LBB1_6
# BB#1:
	movq	(%rbx), %r12
	testq	%r12, %r12
	movl	$-15, %eax
	js	.LBB1_6
# BB#2:
	cmpq	$268435454, %r12        # imm = 0xFFFFFFE
	ja	.LBB1_6
# BB#3:
	movl	$16, %esi
	movl	%r12d, %edi
	movq	%rcx, %rdx
	callq	alloc
	testq	%rax, %rax
	je	.LBB1_4
# BB#5:
	movq	%rax, (%rbx)
	leal	(%r14,%r15,4), %eax
	movw	%ax, 8(%rbx)
	movw	%r12w, 10(%rbx)
	xorl	%eax, %eax
	jmp	.LBB1_6
.LBB1_4:
	movl	$-25, %eax
.LBB1_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	make_array, .Lfunc_end1-make_array
	.cfi_endproc

	.globl	zaload
	.p2align	4, 0x90
	.type	zaload,@function
zaload:                                 # @zaload
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 64
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movw	8(%rbx), %dx
	movl	%edx, %ecx
	shrb	$2, %cl
	cmpb	$10, %cl
	je	.LBB2_2
# BB#1:
	movl	$-20, %eax
	testb	%cl, %cl
	jne	.LBB2_7
.LBB2_2:
	movl	$-7, %eax
	testb	$2, %dh
	je	.LBB2_7
# BB#3:
	movzwl	10(%rbx), %ebp
	movzwl	%bp, %r14d
	movq	ostop(%rip), %rcx
	subq	%rbx, %rcx
	sarq	$4, %rcx
	movl	$-15, %eax
	cmpq	%rcx, %r14
	jg	.LBB2_7
# BB#4:
	movl	%edx, %r13d
	movq	(%rbx), %r15
	movl	12(%rbx), %r12d
	shlq	$4, %r14
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	memcpy
	leaq	(%rbx,%r14), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB2_6
# BB#5:
	subq	%r14, %rax
	movq	%rax, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB2_7
.LBB2_6:
	movq	%r15, (%rax)
	movw	%r13w, 8(%rbx,%r14)
	movw	%bp, 10(%rbx,%r14)
	movl	%r12d, 12(%rax)
	xorl	%eax, %eax
.LBB2_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	zaload, .Lfunc_end2-zaload
	.cfi_endproc

	.globl	zastore
	.p2align	4, 0x90
	.type	zastore,@function
zastore:                                # @zastore
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 48
.Lcfi32:
	.cfi_offset %rbx, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %eax
	movl	$-20, %ebp
	testb	$-4, %al
	jne	.LBB3_4
# BB#1:
	movl	$-7, %ebp
	testb	$1, %ah
	je	.LBB3_4
# BB#2:
	movzwl	10(%rbx), %edx
	movq	%rbx, %rax
	subq	osbot(%rip), %rax
	sarq	$4, %rax
	movl	$-17, %ebp
	cmpq	%rax, %rdx
	ja	.LBB3_4
# BB#3:
	movq	(%rbx), %rdi
	movq	%rdx, %r15
	shlq	$4, %r15
	movq	%rbx, %r14
	subq	%r15, %r14
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	refcpy
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
	subq	%r15, osp(%rip)
.LBB3_4:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	zastore, .Lfunc_end3-zastore
	.cfi_endproc

	.globl	zarray_op_init
	.p2align	4, 0x90
	.type	zarray_op_init,@function
zarray_op_init:                         # @zarray_op_init
	.cfi_startproc
# BB#0:
	movl	$zarray_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end4:
	.size	zarray_op_init, .Lfunc_end4-zarray_op_init
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"array"
	.size	.L.str, 6

	.type	zarray_op_init.my_defs,@object # @zarray_op_init.my_defs
	.data
	.p2align	4
zarray_op_init.my_defs:
	.quad	.L.str.1
	.quad	zaload
	.quad	.L.str.2
	.quad	zarray
	.quad	.L.str.3
	.quad	zastore
	.zero	16
	.size	zarray_op_init.my_defs, 64

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"1aload"
	.size	.L.str.1, 7

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"1array"
	.size	.L.str.2, 7

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"1astore"
	.size	.L.str.3, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
