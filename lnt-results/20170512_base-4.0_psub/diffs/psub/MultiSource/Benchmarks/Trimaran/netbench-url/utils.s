	.text
	.file	"utils.bc"
	.globl	_fatal
	.p2align	4, 0x90
	.type	_fatal,@function
_fatal:                                 # @_fatal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	movq	%rbp, %rcx
	movl	%r14d, %r8d
	callq	fprintf
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	_fatal, .Lfunc_end0-_fatal
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"fatal: %s"
	.size	.L.str, 10

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" [%s:%s, line %d]"
	.size	.L.str.1, 18


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
