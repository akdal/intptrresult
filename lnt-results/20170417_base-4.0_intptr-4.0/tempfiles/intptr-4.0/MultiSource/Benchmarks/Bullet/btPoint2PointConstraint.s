	.text
	.file	"btPoint2PointConstraint.bc"
	.globl	_ZN23btPoint2PointConstraintC2Ev
	.p2align	4, 0x90
	.type	_ZN23btPoint2PointConstraintC2Ev,@function
_ZN23btPoint2PointConstraintC2Ev:       # @_ZN23btPoint2PointConstraintC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$3, %esi
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintType
	movq	$_ZTV23btPoint2PointConstraint+16, (%rbx)
	movb	$0, 380(%rbx)
	movabsq	$4575657222458677658, %rax # imm = 0x3F8000003E99999A
	movq	%rax, 384(%rbx)
	movl	$0, 392(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN23btPoint2PointConstraintC2Ev, .Lfunc_end0-_ZN23btPoint2PointConstraintC2Ev
	.cfi_endproc

	.globl	_ZN23btPoint2PointConstraintC2ER11btRigidBodyS1_RK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZN23btPoint2PointConstraintC2ER11btRigidBodyS1_RK9btVector3S4_,@function
_ZN23btPoint2PointConstraintC2ER11btRigidBodyS1_RK9btVector3S4_: # @_ZN23btPoint2PointConstraintC2ER11btRigidBodyS1_RK9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %rax
	movq	%rsi, %rcx
	movq	%rdi, %rbx
	movl	$3, %esi
	movq	%rcx, %rdx
	movq	%rax, %rcx
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_
	movq	$_ZTV23btPoint2PointConstraint+16, (%rbx)
	movups	(%r15), %xmm0
	movups	%xmm0, 348(%rbx)
	movups	(%r14), %xmm0
	movups	%xmm0, 364(%rbx)
	movb	$0, 380(%rbx)
	movabsq	$4575657222458677658, %rax # imm = 0x3F8000003E99999A
	movq	%rax, 384(%rbx)
	movl	$0, 392(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZN23btPoint2PointConstraintC2ER11btRigidBodyS1_RK9btVector3S4_, .Lfunc_end1-_ZN23btPoint2PointConstraintC2ER11btRigidBodyS1_RK9btVector3S4_
	.cfi_endproc

	.globl	_ZN23btPoint2PointConstraintC2ER11btRigidBodyRK9btVector3
	.p2align	4, 0x90
	.type	_ZN23btPoint2PointConstraintC2ER11btRigidBodyRK9btVector3,@function
_ZN23btPoint2PointConstraintC2ER11btRigidBodyRK9btVector3: # @_ZN23btPoint2PointConstraintC2ER11btRigidBodyRK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -32
.Lcfi12:
	.cfi_offset %r14, -24
.Lcfi13:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$3, %esi
	movq	%rbx, %rdx
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody
	movq	$_ZTV23btPoint2PointConstraint+16, (%r14)
	movups	(%r15), %xmm0
	movups	%xmm0, 348(%r14)
	movss	(%r15), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	28(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	movsd	56(%rbx), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm3, %xmm4
	mulss	40(%rbx), %xmm2
	mulss	44(%rbx), %xmm1
	addss	%xmm2, %xmm1
	mulss	48(%rbx), %xmm0
	addss	%xmm1, %xmm0
	addss	64(%rbx), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm4, 364(%r14)
	movlps	%xmm1, 372(%r14)
	movb	$0, 380(%r14)
	movabsq	$4575657222458677658, %rax # imm = 0x3F8000003E99999A
	movq	%rax, 384(%r14)
	movl	$0, 392(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	_ZN23btPoint2PointConstraintC2ER11btRigidBodyRK9btVector3, .Lfunc_end2-_ZN23btPoint2PointConstraintC2ER11btRigidBodyRK9btVector3
	.cfi_endproc

	.globl	_ZN23btPoint2PointConstraint13buildJacobianEv
	.p2align	4, 0x90
	.type	_ZN23btPoint2PointConstraint13buildJacobianEv,@function
_ZN23btPoint2PointConstraint13buildJacobianEv: # @_ZN23btPoint2PointConstraint13buildJacobianEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 208
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$0, 40(%rbx)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 96(%rsp)
	leaq	96(%rbx), %r14
	xorl	%r12d, %r12d
	leaq	120(%rsp), %r13
	leaq	96(%rsp), %r15
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movl	$1065353216, 96(%rsp,%r12,4) # imm = 0x3F800000
	movq	24(%rbx), %rdx
	movl	8(%rdx), %eax
	movl	%eax, 48(%rsp)
	movl	24(%rdx), %eax
	movl	%eax, 52(%rsp)
	movl	40(%rdx), %eax
	movl	%eax, 56(%rsp)
	movl	$0, 60(%rsp)
	movl	12(%rdx), %eax
	movl	%eax, 64(%rsp)
	movl	28(%rdx), %eax
	movl	%eax, 68(%rsp)
	movl	44(%rdx), %eax
	movl	%eax, 72(%rsp)
	movl	$0, 76(%rsp)
	movl	16(%rdx), %eax
	movl	%eax, 80(%rsp)
	movl	32(%rdx), %eax
	movl	%eax, 84(%rsp)
	movl	48(%rdx), %esi
	movl	%esi, 88(%rsp)
	movl	$0, 92(%rsp)
	movq	32(%rbx), %rax
	movl	8(%rax), %ecx
	movl	%ecx, (%rsp)
	movl	24(%rax), %ecx
	movl	%ecx, 4(%rsp)
	movl	40(%rax), %ecx
	movl	%ecx, 8(%rsp)
	movl	$0, 12(%rsp)
	movl	12(%rax), %ecx
	movl	%ecx, 16(%rsp)
	movl	28(%rax), %ecx
	movl	%ecx, 20(%rsp)
	movl	44(%rax), %ecx
	movl	%ecx, 24(%rsp)
	movl	$0, 28(%rsp)
	movl	16(%rax), %ecx
	movl	%ecx, 32(%rsp)
	movl	32(%rax), %ecx
	movl	%ecx, 36(%rsp)
	movl	48(%rax), %ecx
	movl	%ecx, 40(%rsp)
	movl	$0, 44(%rsp)
	movss	348(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	352(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	356(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	24(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	12(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	28(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	32(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movd	%esi, %xmm3
	mulss	%xmm2, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	addps	%xmm4, %xmm2
	movsd	56(%rdx), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm4, %xmm2
	mulss	40(%rdx), %xmm0
	mulss	44(%rdx), %xmm1
	addss	%xmm0, %xmm1
	addss	%xmm1, %xmm3
	movss	64(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm3
	subps	%xmm4, %xmm2
	subss	%xmm0, %xmm3
	xorps	%xmm0, %xmm0
	movss	%xmm3, %xmm0            # xmm0 = xmm3[0],xmm0[1,2,3]
	movlps	%xmm2, 136(%rsp)
	movlps	%xmm0, 144(%rsp)
	movss	364(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	368(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	372(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	24(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	28(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	32(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movd	%ecx, %xmm3
	mulss	%xmm2, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	addps	%xmm4, %xmm2
	movsd	56(%rax), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm4, %xmm2
	mulss	40(%rax), %xmm0
	mulss	44(%rax), %xmm1
	addss	%xmm0, %xmm1
	addss	%xmm1, %xmm3
	movss	64(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm3
	subps	%xmm4, %xmm2
	subss	%xmm0, %xmm3
	xorps	%xmm0, %xmm0
	movss	%xmm3, %xmm0            # xmm0 = xmm3[0],xmm0[1,2,3]
	movlps	%xmm2, 120(%rsp)
	movlps	%xmm0, 128(%rsp)
	movq	24(%rbx), %rax
	movq	32(%rbx), %rbp
	movss	360(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addq	$428, %rax              # imm = 0x1AC
	movss	360(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addq	$428, %rbp              # imm = 0x1AC
	movq	%r14, %rdi
	leaq	48(%rsp), %rsi
	movq	%rsp, %rdx
	leaq	136(%rsp), %rcx
	movq	%r13, %r8
	movq	%r15, %r9
	pushq	%rbp
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	callq	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	addq	$16, %rsp
.Lcfi29:
	.cfi_adjust_cfa_offset -16
	movl	$0, 96(%rsp,%r12,4)
	incq	%r12
	addq	$84, %r14
	cmpq	$3, %r12
	jne	.LBB3_1
# BB#2:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN23btPoint2PointConstraint13buildJacobianEv, .Lfunc_end3-_ZN23btPoint2PointConstraint13buildJacobianEv
	.cfi_endproc

	.section	.text._ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,"axG",@progbits,_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,comdat
	.weak	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.p2align	4, 0x90
	.type	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,@function
_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f: # @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.cfi_startproc
# BB#0:
	movq	16(%rsp), %r10
	movq	8(%rsp), %rax
	movups	(%r9), %xmm2
	movups	%xmm2, (%rdi)
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm7
	mulss	%xmm11, %xmm7
	movss	8(%rcx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	(%rdi), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm7
	mulss	%xmm9, %xmm5
	movaps	%xmm11, %xmm4
	mulss	%xmm2, %xmm4
	subss	%xmm4, %xmm5
	mulss	%xmm10, %xmm2
	mulss	%xmm9, %xmm6
	subss	%xmm6, %xmm2
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	16(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1]
	mulps	%xmm4, %xmm6
	movss	20(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm6, %xmm4
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm12         # xmm12 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm12   # xmm12 = xmm12[0],xmm8[0],xmm12[1],xmm8[1]
	mulps	%xmm3, %xmm12
	addps	%xmm4, %xmm12
	mulss	32(%rsi), %xmm7
	mulss	36(%rsi), %xmm5
	addss	%xmm7, %xmm5
	mulss	40(%rsi), %xmm2
	addss	%xmm5, %xmm2
	xorps	%xmm8, %xmm8
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movlps	%xmm12, 16(%rdi)
	movlps	%xmm3, 24(%rdi)
	movss	(%r8), %xmm3            # xmm3 = mem[0],zero,zero,zero
	movss	4(%r8), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	mulss	%xmm11, %xmm5
	movss	8(%r8), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm6
	mulss	%xmm10, %xmm6
	subss	%xmm5, %xmm6
	mulss	%xmm9, %xmm7
	mulss	%xmm3, %xmm11
	subss	%xmm7, %xmm11
	mulss	%xmm10, %xmm3
	mulss	%xmm9, %xmm4
	subss	%xmm3, %xmm4
	movss	16(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	movss	20(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm9    # xmm9 = xmm9[0],xmm5[0],xmm9[1],xmm5[1]
	movaps	%xmm11, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm9, %xmm7
	addps	%xmm3, %xmm7
	movaps	%xmm4, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rdx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm9, %xmm5    # xmm5 = xmm5[0],xmm9[0],xmm5[1],xmm9[1]
	mulps	%xmm3, %xmm5
	addps	%xmm7, %xmm5
	mulss	32(%rdx), %xmm6
	mulss	36(%rdx), %xmm11
	addss	%xmm6, %xmm11
	mulss	40(%rdx), %xmm4
	addss	%xmm11, %xmm4
	xorps	%xmm3, %xmm3
	movss	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1,2,3]
	movlps	%xmm5, 32(%rdi)
	movlps	%xmm3, 40(%rdi)
	movsd	(%rax), %xmm9           # xmm9 = mem[0],zero
	mulps	%xmm12, %xmm9
	mulss	8(%rax), %xmm2
	xorps	%xmm6, %xmm6
	movss	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1,2,3]
	movlps	%xmm9, 48(%rdi)
	movlps	%xmm6, 56(%rdi)
	movsd	(%r10), %xmm6           # xmm6 = mem[0],zero
	mulps	%xmm5, %xmm6
	mulss	8(%r10), %xmm4
	movaps	%xmm6, %xmm7
	movlps	%xmm6, 64(%rdi)
	mulss	%xmm5, %xmm6
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movss	%xmm4, %xmm8            # xmm8 = xmm4[0],xmm8[1,2,3]
	movlps	%xmm8, 72(%rdi)
	movss	16(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	mulss	20(%rdi), %xmm9
	addss	%xmm3, %xmm9
	mulss	24(%rdi), %xmm2
	addss	%xmm9, %xmm2
	addss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm5, %xmm7
	addss	%xmm6, %xmm7
	mulss	40(%rdi), %xmm4
	addss	%xmm7, %xmm4
	addss	%xmm2, %xmm4
	movss	%xmm4, 80(%rdi)
	retq
.Lfunc_end4:
	.size	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f, .Lfunc_end4-_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.cfi_endproc

	.text
	.globl	_ZN23btPoint2PointConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.p2align	4, 0x90
	.type	_ZN23btPoint2PointConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E,@function
_ZN23btPoint2PointConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E: # @_ZN23btPoint2PointConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpb	$0, 380(%rdi)
	sete	%al
	leal	(%rax,%rax,2), %eax
	movl	%eax, (%rsi)
	movl	%eax, 4(%rsi)
	retq
.Lfunc_end5:
	.size	_ZN23btPoint2PointConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E, .Lfunc_end5-_ZN23btPoint2PointConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.cfi_endproc

	.globl	_ZN23btPoint2PointConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E
	.p2align	4, 0x90
	.type	_ZN23btPoint2PointConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E,@function
_ZN23btPoint2PointConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E: # @_ZN23btPoint2PointConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpb	$0, 380(%rdi)
	sete	%al
	leal	(%rax,%rax,2), %eax
	movl	%eax, (%rsi)
	movl	%eax, 4(%rsi)
	retq
.Lfunc_end6:
	.size	_ZN23btPoint2PointConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E, .Lfunc_end6-_ZN23btPoint2PointConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E
	.cfi_endproc

	.globl	_ZN23btPoint2PointConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.p2align	4, 0x90
	.type	_ZN23btPoint2PointConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E,@function
_ZN23btPoint2PointConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E: # @_ZN23btPoint2PointConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdx
	movq	32(%rdi), %rcx
	addq	$8, %rdx
	addq	$8, %rcx
	jmp	_ZN23btPoint2PointConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_ # TAILCALL
.Lfunc_end7:
	.size	_ZN23btPoint2PointConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E, .Lfunc_end7-_ZN23btPoint2PointConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN23btPoint2PointConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_
	.p2align	4, 0x90
	.type	_ZN23btPoint2PointConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_,@function
_ZN23btPoint2PointConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_: # @_ZN23btPoint2PointConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_
	.cfi_startproc
# BB#0:
	movq	8(%rsi), %r10
	movl	$1065353216, (%r10)     # imm = 0x3F800000
	movslq	40(%rsi), %r9
	movl	$1065353216, 4(%r10,%r9,4) # imm = 0x3F800000
	leal	(%r9,%r9), %eax
	movslq	%eax, %r8
	movl	$1065353216, 8(%r10,%r8,4) # imm = 0x3F800000
	movss	348(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	352(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	356(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	16(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	20(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	24(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	mulss	32(%rdx), %xmm1
	mulss	36(%rdx), %xmm0
	addss	%xmm1, %xmm0
	mulss	40(%rdx), %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm3, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	movlps	%xmm3, -32(%rsp)
	xorps	%xmm1, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm0, -24(%rsp)
	movq	16(%rsi), %rax
	movaps	.LCPI8_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm4, %xmm5
	xorps	%xmm0, %xmm5
	movl	$0, (%rax)
	movss	%xmm2, 4(%rax)
	xorps	%xmm0, %xmm2
	movss	%xmm5, 8(%rax)
	movl	$0, 12(%rax)
	movss	%xmm2, (%rax,%r9,4)
	movl	$0, 4(%rax,%r9,4)
	movss	%xmm3, 8(%rax,%r9,4)
	xorps	%xmm0, %xmm3
	movl	$0, 12(%rax,%r9,4)
	movss	%xmm4, (%rax,%r8,4)
	movss	%xmm3, 4(%rax,%r8,4)
	movq	$0, 8(%rax,%r8,4)
	movss	364(%rdi), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	368(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	372(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	16(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm6    # xmm6 = xmm6[0],xmm3[0],xmm6[1],xmm3[1]
	movaps	%xmm5, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm6, %xmm3
	movss	20(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1]
	movaps	%xmm4, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm7, %xmm6
	addps	%xmm3, %xmm6
	movss	24(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rcx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm7    # xmm7 = xmm7[0],xmm3[0],xmm7[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm7, %xmm3
	addps	%xmm6, %xmm3
	mulss	32(%rcx), %xmm5
	mulss	36(%rcx), %xmm4
	addss	%xmm5, %xmm4
	mulss	40(%rcx), %xmm2
	addss	%xmm4, %xmm2
	movaps	%xmm3, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	movlps	%xmm3, -16(%rsp)
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm1, -8(%rsp)
	movaps	%xmm2, %xmm1
	xorps	%xmm0, %xmm1
	movq	32(%rsi), %rax
	movslq	40(%rsi), %r8
	movl	$0, (%rax)
	movss	%xmm1, 4(%rax)
	movss	%xmm4, 8(%rax)
	movl	$0, 12(%rax)
	movaps	%xmm3, %xmm1
	xorps	%xmm0, %xmm1
	movss	%xmm2, (%rax,%r8,4)
	movl	$0, 4(%rax,%r8,4)
	movss	%xmm1, 8(%rax,%r8,4)
	movl	$0, 12(%rax,%r8,4)
	movaps	%xmm4, %xmm1
	xorps	%xmm0, %xmm1
	movss	%xmm1, (%rax,%r8,8)
	movss	%xmm3, 4(%rax,%r8,8)
	movq	$0, 8(%rax,%r8,8)
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	4(%rsi), %xmm1
	movq	48(%rsi), %rax
	movss	48(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm2
	subss	-32(%rsp), %xmm2
	subss	48(%rdx), %xmm2
	mulss	%xmm1, %xmm2
	movss	%xmm2, (%rax)
	movss	52(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm2
	subss	-28(%rsp), %xmm2
	subss	52(%rdx), %xmm2
	mulss	%xmm1, %xmm2
	movss	%xmm2, (%rax,%r8,4)
	movss	-8(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	addss	56(%rcx), %xmm2
	subss	-24(%rsp), %xmm2
	subss	56(%rdx), %xmm2
	mulss	%xmm1, %xmm2
	movss	%xmm2, (%rax,%r8,8)
	movss	392(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm0
	xorps	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm1
	movaps	%xmm1, %xmm2
	jbe	.LBB8_2
# BB#1:
	movq	64(%rsi), %rax
	movss	%xmm0, (%rax)
	movq	72(%rsi), %rax
	movss	%xmm1, (%rax)
	movss	392(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
.LBB8_2:
	ucomiss	%xmm3, %xmm2
	jbe	.LBB8_4
# BB#3:
	movq	64(%rsi), %rax
	movslq	40(%rsi), %rcx
	movss	%xmm0, (%rax,%rcx,4)
	movq	72(%rsi), %rax
	movss	%xmm1, (%rax,%rcx,4)
	movss	392(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
.LBB8_4:
	xorps	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm2
	jbe	.LBB8_6
# BB#5:
	movq	64(%rsi), %rax
	movslq	40(%rsi), %rcx
	movss	%xmm0, (%rax,%rcx,8)
	movq	72(%rsi), %rax
	movss	%xmm1, (%rax,%rcx,8)
.LBB8_6:
	retq
.Lfunc_end8:
	.size	_ZN23btPoint2PointConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_, .Lfunc_end8-_ZN23btPoint2PointConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN23btPoint2PointConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.p2align	4, 0x90
	.type	_ZN23btPoint2PointConstraint23solveConstraintObsoleteER12btSolverBodyS1_f,@function
_ZN23btPoint2PointConstraint23solveConstraintObsoleteER12btSolverBodyS1_f: # @_ZN23btPoint2PointConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.cfi_startproc
# BB#0:
	movss	%xmm0, -92(%rsp)        # 4-byte Spill
	cmpb	$0, 380(%rdi)
	je	.LBB9_13
# BB#1:
	movss	348(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	352(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	356(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movq	24(%rdi), %rcx
	movq	32(%rdi), %rax
	movss	24(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	movss	28(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	movss	32(%rcx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm10
	addss	%xmm4, %xmm10
	addss	60(%rcx), %xmm10
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	8(%rcx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	12(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	40(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	mulps	%xmm2, %xmm5
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	44(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	mulps	%xmm1, %xmm2
	addps	%xmm5, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movss	16(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	48(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	mulps	%xmm0, %xmm3
	addps	%xmm2, %xmm3
	movss	56(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	addps	%xmm3, %xmm5
	movaps	%xmm5, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	movss	364(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	368(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	372(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	24(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movss	28(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	addss	%xmm1, %xmm4
	movss	32(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm4, %xmm1
	movss	60(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	addss	%xmm8, %xmm1
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	40(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm3, %xmm6
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	44(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm9, %xmm3    # xmm3 = xmm3[0],xmm9[0],xmm3[1],xmm9[1]
	mulps	%xmm2, %xmm3
	addps	%xmm6, %xmm3
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movss	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	48(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	mulps	%xmm0, %xmm4
	addps	%xmm3, %xmm4
	movss	56(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rax), %xmm9         # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm9    # xmm9 = xmm9[0],xmm0[0],xmm9[1],xmm0[1]
	addps	%xmm9, %xmm4
	movaps	%xmm4, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm0, %xmm7
	movaps	%xmm7, -40(%rsp)        # 16-byte Spill
	movss	%xmm10, -96(%rsp)       # 4-byte Spill
	movss	%xmm1, -100(%rsp)       # 4-byte Spill
	subss	%xmm1, %xmm10
	movss	%xmm10, -104(%rsp)      # 4-byte Spill
	movaps	%xmm5, -24(%rsp)        # 16-byte Spill
	movaps	%xmm4, -56(%rsp)        # 16-byte Spill
	subss	%xmm4, %xmm5
	movaps	%xmm5, -72(%rsp)        # 16-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -88(%rsp)
	leaq	176(%rdi), %r8
	xorl	%r9d, %r9d
	jmp	.LBB9_2
	.p2align	4, 0x90
.LBB9_12:                               # %._crit_edge
                                        #   in Loop: Header=BB9_2 Depth=1
	incq	%r9
	movq	32(%rdi), %rax
	movss	56(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	60(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	64(%rax), %xmm9         # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm9    # xmm9 = xmm9[0],xmm0[0],xmm9[1],xmm0[1]
	addq	$84, %r8
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movl	$1065353216, -88(%rsp,%r9,4) # imm = 0x3F800000
	movss	.LCPI9_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	(%r8), %xmm0
	movaps	%xmm0, %xmm14
	movq	24(%rdi), %rax
	movss	-96(%rsp), %xmm11       # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	subss	60(%rax), %xmm11
	movss	56(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	64(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movaps	-24(%rsp), %xmm13       # 16-byte Reload
	subps	%xmm3, %xmm13
	movq	72(%rsi), %rcx
	testq	%rcx, %rcx
	xorps	%xmm7, %xmm7
	xorps	%xmm10, %xmm10
	je	.LBB9_4
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	movsd	328(%rcx), %xmm3        # xmm3 = mem[0],zero
	movsd	(%rsi), %xmm2           # xmm2 = mem[0],zero
	addps	%xmm3, %xmm2
	movss	336(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	344(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	addss	8(%rsi), %xmm3
	addss	16(%rsi), %xmm4
	movsd	348(%rcx), %xmm5        # xmm5 = mem[0],zero
	movsd	20(%rsi), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm5, %xmm1
	movaps	%xmm13, %xmm7
	mulps	%xmm1, %xmm7
	movaps	%xmm11, %xmm5
	unpcklps	%xmm13, %xmm5   # xmm5 = xmm5[0],xmm13[0],xmm5[1],xmm13[1]
	movaps	%xmm13, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm1, %xmm0
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm11, %xmm6
	mulss	%xmm4, %xmm6
	shufps	$0, %xmm1, %xmm4        # xmm4 = xmm4[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm4      # xmm4 = xmm4[2,0],xmm1[2,3]
	mulps	%xmm5, %xmm4
	subps	%xmm4, %xmm7
	subss	%xmm0, %xmm6
	addps	%xmm2, %xmm7
	addss	%xmm3, %xmm6
	xorps	%xmm10, %xmm10
	movss	%xmm6, %xmm10           # xmm10 = xmm6[0],xmm10[1,2,3]
.LBB9_4:                                # %_ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit104
                                        #   in Loop: Header=BB9_2 Depth=1
	movss	-100(%rsp), %xmm15      # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm15
	movaps	-56(%rsp), %xmm12       # 16-byte Reload
	subps	%xmm9, %xmm12
	movq	72(%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB9_5
# BB#6:                                 #   in Loop: Header=BB9_2 Depth=1
	movsd	328(%rcx), %xmm0        # xmm0 = mem[0],zero
	movsd	(%rdx), %xmm8           # xmm8 = mem[0],zero
	addps	%xmm0, %xmm8
	movss	336(%rcx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	344(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addss	8(%rdx), %xmm2
	addss	16(%rdx), %xmm0
	movsd	348(%rcx), %xmm1        # xmm1 = mem[0],zero
	movsd	20(%rdx), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm1, %xmm4
	movaps	%xmm12, %xmm1
	mulps	%xmm4, %xmm1
	movaps	%xmm15, %xmm5
	unpcklps	%xmm12, %xmm5   # xmm5 = xmm5[0],xmm12[0],xmm5[1],xmm12[1]
	movaps	%xmm12, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm4, %xmm6
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	movaps	%xmm15, %xmm3
	mulss	%xmm0, %xmm3
	shufps	$0, %xmm4, %xmm0        # xmm0 = xmm0[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm0      # xmm0 = xmm0[2,0],xmm4[2,3]
	mulps	%xmm5, %xmm0
	subps	%xmm0, %xmm1
	subss	%xmm6, %xmm3
	addps	%xmm8, %xmm1
	addss	%xmm2, %xmm3
	xorps	%xmm2, %xmm2
	movss	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1,2,3]
	jmp	.LBB9_7
	.p2align	4, 0x90
.LBB9_5:                                #   in Loop: Header=BB9_2 Depth=1
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
.LBB9_7:                                # %_ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	movaps	%xmm14, %xmm3
	movaps	%xmm7, %xmm0
	subss	%xmm1, %xmm0
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	subss	%xmm1, %xmm7
	subss	%xmm2, %xmm10
	movss	-88(%rsp), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movss	-84(%rsp), %xmm14       # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm0
	mulss	%xmm14, %xmm7
	addss	%xmm0, %xmm7
	movss	-80(%rsp), %xmm8        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm10
	addss	%xmm7, %xmm10
	movaps	-40(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm9, %xmm0
	movss	-104(%rsp), %xmm1       # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm1
	addss	%xmm0, %xmm1
	movaps	-72(%rsp), %xmm7        # 16-byte Reload
	mulss	%xmm8, %xmm7
	addss	%xmm1, %xmm7
	mulss	384(%rdi), %xmm7
	movaps	.LCPI9_1(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm0, %xmm4
	xorps	%xmm4, %xmm7
	divss	-92(%rsp), %xmm7        # 4-byte Folded Reload
	mulss	%xmm3, %xmm7
	mulss	388(%rdi), %xmm10
	mulss	%xmm3, %xmm10
	subss	%xmm10, %xmm7
	movss	40(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	392(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	addss	%xmm7, %xmm1
	movaps	%xmm2, %xmm3
	xorps	%xmm4, %xmm3
	ucomiss	%xmm1, %xmm3
	jbe	.LBB9_9
# BB#8:                                 #   in Loop: Header=BB9_2 Depth=1
	movaps	%xmm3, %xmm7
	subss	%xmm0, %xmm7
	movaps	%xmm3, %xmm1
	jmp	.LBB9_11
	.p2align	4, 0x90
.LBB9_9:                                #   in Loop: Header=BB9_2 Depth=1
	ucomiss	%xmm2, %xmm1
	jbe	.LBB9_11
# BB#10:                                #   in Loop: Header=BB9_2 Depth=1
	movaps	%xmm2, %xmm7
	subss	%xmm0, %xmm7
	movaps	%xmm2, %xmm1
.LBB9_11:                               #   in Loop: Header=BB9_2 Depth=1
	movss	%xmm1, 40(%rdi)
	movaps	%xmm11, %xmm4
	mulss	%xmm8, %xmm4
	movaps	%xmm13, %xmm0
	mulss	%xmm14, %xmm0
	subss	%xmm0, %xmm4
	movaps	%xmm13, %xmm2
	mulss	%xmm9, %xmm2
	shufps	$229, %xmm13, %xmm13    # xmm13 = xmm13[1,1,2,3]
	movaps	%xmm13, %xmm0
	mulss	%xmm8, %xmm0
	subss	%xmm0, %xmm2
	mulss	%xmm14, %xmm13
	mulss	%xmm9, %xmm11
	subss	%xmm11, %xmm13
	movaps	%xmm15, %xmm10
	mulss	%xmm8, %xmm10
	movaps	%xmm12, %xmm0
	mulss	%xmm14, %xmm0
	subss	%xmm0, %xmm10
	movaps	%xmm12, %xmm11
	mulss	%xmm9, %xmm11
	shufps	$229, %xmm12, %xmm12    # xmm12 = xmm12[1,1,2,3]
	movaps	%xmm12, %xmm0
	mulss	%xmm8, %xmm0
	subss	%xmm0, %xmm11
	mulss	%xmm14, %xmm12
	mulss	%xmm9, %xmm15
	subss	%xmm15, %xmm12
	movss	360(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	mulss	%xmm6, %xmm0
	movaps	%xmm14, %xmm3
	mulss	%xmm6, %xmm3
	mulss	%xmm8, %xmm6
	movss	280(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	284(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm1, %xmm5
	movss	288(%rax), %xmm15       # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm15
	addss	%xmm5, %xmm15
	movss	296(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	movss	300(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	addss	%xmm5, %xmm1
	movss	304(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm5
	addss	%xmm1, %xmm5
	mulss	312(%rax), %xmm4
	mulss	316(%rax), %xmm2
	addss	%xmm4, %xmm2
	mulss	320(%rax), %xmm13
	addss	%xmm2, %xmm13
	mulss	%xmm7, %xmm0
	mulss	%xmm7, %xmm3
	mulss	%xmm7, %xmm6
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addss	4(%rsi), %xmm3
	movss	%xmm3, 4(%rsi)
	addss	8(%rsi), %xmm6
	movss	%xmm6, 8(%rsi)
	movss	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	movss	36(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	movss	40(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	mulss	%xmm15, %xmm0
	mulss	%xmm5, %xmm1
	mulss	%xmm13, %xmm2
	addss	16(%rsi), %xmm0
	movss	%xmm0, 16(%rsi)
	addss	20(%rsi), %xmm1
	movss	%xmm1, 20(%rsi)
	addss	24(%rsi), %xmm2
	movss	%xmm2, 24(%rsi)
	movq	32(%rdi), %rax
	movss	360(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm9
	mulss	%xmm2, %xmm14
	mulss	%xmm8, %xmm2
	movss	280(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm0
	movss	284(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	addss	%xmm0, %xmm1
	movss	288(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm4
	addss	%xmm1, %xmm4
	movss	296(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm0
	movss	300(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	addss	%xmm0, %xmm1
	movss	304(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	mulss	312(%rax), %xmm10
	mulss	316(%rax), %xmm11
	addss	%xmm10, %xmm11
	mulss	320(%rax), %xmm12
	addss	%xmm11, %xmm12
	mulss	%xmm7, %xmm9
	mulss	%xmm7, %xmm14
	mulss	%xmm7, %xmm2
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	subss	%xmm9, %xmm1
	movss	%xmm1, (%rdx)
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm14, %xmm1
	movss	%xmm1, 4(%rdx)
	movss	8(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	movss	%xmm1, 8(%rdx)
	movss	32(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	movss	36(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	mulss	40(%rdx), %xmm7
	mulss	%xmm4, %xmm1
	mulss	%xmm0, %xmm2
	mulss	%xmm12, %xmm7
	movss	16(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	%xmm0, 16(%rdx)
	movss	20(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm0
	movss	%xmm0, 20(%rdx)
	movss	24(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm0
	movss	%xmm0, 24(%rdx)
	movl	$0, -88(%rsp,%r9,4)
	cmpq	$2, %r9
	jne	.LBB9_12
.LBB9_13:
	retq
.Lfunc_end9:
	.size	_ZN23btPoint2PointConstraint23solveConstraintObsoleteER12btSolverBodyS1_f, .Lfunc_end9-_ZN23btPoint2PointConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.cfi_endproc

	.globl	_ZN23btPoint2PointConstraint9updateRHSEf
	.p2align	4, 0x90
	.type	_ZN23btPoint2PointConstraint9updateRHSEf,@function
_ZN23btPoint2PointConstraint9updateRHSEf: # @_ZN23btPoint2PointConstraint9updateRHSEf
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end10:
	.size	_ZN23btPoint2PointConstraint9updateRHSEf, .Lfunc_end10-_ZN23btPoint2PointConstraint9updateRHSEf
	.cfi_endproc

	.section	.text._ZN17btTypedConstraintD2Ev,"axG",@progbits,_ZN17btTypedConstraintD2Ev,comdat
	.weak	_ZN17btTypedConstraintD2Ev
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraintD2Ev,@function
_ZN17btTypedConstraintD2Ev:             # @_ZN17btTypedConstraintD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	_ZN17btTypedConstraintD2Ev, .Lfunc_end11-_ZN17btTypedConstraintD2Ev
	.cfi_endproc

	.section	.text._ZN23btPoint2PointConstraintD0Ev,"axG",@progbits,_ZN23btPoint2PointConstraintD0Ev,comdat
	.weak	_ZN23btPoint2PointConstraintD0Ev
	.p2align	4, 0x90
	.type	_ZN23btPoint2PointConstraintD0Ev,@function
_ZN23btPoint2PointConstraintD0Ev:       # @_ZN23btPoint2PointConstraintD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end12:
	.size	_ZN23btPoint2PointConstraintD0Ev, .Lfunc_end12-_ZN23btPoint2PointConstraintD0Ev
	.cfi_endproc

	.section	.text._ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,"axG",@progbits,_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,comdat
	.weak	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,@function
_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif: # @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end13:
	.size	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif, .Lfunc_end13-_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_endproc

	.type	_ZTV23btPoint2PointConstraint,@object # @_ZTV23btPoint2PointConstraint
	.section	.rodata,"a",@progbits
	.globl	_ZTV23btPoint2PointConstraint
	.p2align	3
_ZTV23btPoint2PointConstraint:
	.quad	0
	.quad	_ZTI23btPoint2PointConstraint
	.quad	_ZN17btTypedConstraintD2Ev
	.quad	_ZN23btPoint2PointConstraintD0Ev
	.quad	_ZN23btPoint2PointConstraint13buildJacobianEv
	.quad	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.quad	_ZN23btPoint2PointConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.quad	_ZN23btPoint2PointConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.quad	_ZN23btPoint2PointConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.size	_ZTV23btPoint2PointConstraint, 72

	.type	_ZTS23btPoint2PointConstraint,@object # @_ZTS23btPoint2PointConstraint
	.globl	_ZTS23btPoint2PointConstraint
	.p2align	4
_ZTS23btPoint2PointConstraint:
	.asciz	"23btPoint2PointConstraint"
	.size	_ZTS23btPoint2PointConstraint, 26

	.type	_ZTS17btTypedConstraint,@object # @_ZTS17btTypedConstraint
	.section	.rodata._ZTS17btTypedConstraint,"aG",@progbits,_ZTS17btTypedConstraint,comdat
	.weak	_ZTS17btTypedConstraint
	.p2align	4
_ZTS17btTypedConstraint:
	.asciz	"17btTypedConstraint"
	.size	_ZTS17btTypedConstraint, 20

	.type	_ZTS13btTypedObject,@object # @_ZTS13btTypedObject
	.section	.rodata._ZTS13btTypedObject,"aG",@progbits,_ZTS13btTypedObject,comdat
	.weak	_ZTS13btTypedObject
_ZTS13btTypedObject:
	.asciz	"13btTypedObject"
	.size	_ZTS13btTypedObject, 16

	.type	_ZTI13btTypedObject,@object # @_ZTI13btTypedObject
	.section	.rodata._ZTI13btTypedObject,"aG",@progbits,_ZTI13btTypedObject,comdat
	.weak	_ZTI13btTypedObject
	.p2align	3
_ZTI13btTypedObject:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13btTypedObject
	.size	_ZTI13btTypedObject, 16

	.type	_ZTI17btTypedConstraint,@object # @_ZTI17btTypedConstraint
	.section	.rodata._ZTI17btTypedConstraint,"aG",@progbits,_ZTI17btTypedConstraint,comdat
	.weak	_ZTI17btTypedConstraint
	.p2align	4
_ZTI17btTypedConstraint:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS17btTypedConstraint
	.long	0                       # 0x0
	.long	1                       # 0x1
	.quad	_ZTI13btTypedObject
	.quad	2050                    # 0x802
	.size	_ZTI17btTypedConstraint, 40

	.type	_ZTI23btPoint2PointConstraint,@object # @_ZTI23btPoint2PointConstraint
	.section	.rodata,"a",@progbits
	.globl	_ZTI23btPoint2PointConstraint
	.p2align	4
_ZTI23btPoint2PointConstraint:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS23btPoint2PointConstraint
	.quad	_ZTI17btTypedConstraint
	.size	_ZTI23btPoint2PointConstraint, 24


	.globl	_ZN23btPoint2PointConstraintC1Ev
	.type	_ZN23btPoint2PointConstraintC1Ev,@function
_ZN23btPoint2PointConstraintC1Ev = _ZN23btPoint2PointConstraintC2Ev
	.globl	_ZN23btPoint2PointConstraintC1ER11btRigidBodyS1_RK9btVector3S4_
	.type	_ZN23btPoint2PointConstraintC1ER11btRigidBodyS1_RK9btVector3S4_,@function
_ZN23btPoint2PointConstraintC1ER11btRigidBodyS1_RK9btVector3S4_ = _ZN23btPoint2PointConstraintC2ER11btRigidBodyS1_RK9btVector3S4_
	.globl	_ZN23btPoint2PointConstraintC1ER11btRigidBodyRK9btVector3
	.type	_ZN23btPoint2PointConstraintC1ER11btRigidBodyRK9btVector3,@function
_ZN23btPoint2PointConstraintC1ER11btRigidBodyRK9btVector3 = _ZN23btPoint2PointConstraintC2ER11btRigidBodyRK9btVector3
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
