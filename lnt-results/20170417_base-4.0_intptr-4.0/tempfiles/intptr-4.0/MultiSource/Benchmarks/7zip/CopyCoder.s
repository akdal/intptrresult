	.text
	.file	"CopyCoder.bc"
	.globl	_ZN9NCompress10CCopyCoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress10CCopyCoderD2Ev,@function
_ZN9NCompress10CCopyCoderD2Ev:          # @_ZN9NCompress10CCopyCoderD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$_ZTVN9NCompress10CCopyCoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress10CCopyCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rdi)
	movq	24(%rdi), %rdi
	callq	MidFree
	popq	%rax
	retq
.Lfunc_end0:
	.size	_ZN9NCompress10CCopyCoderD2Ev, .Lfunc_end0-_ZN9NCompress10CCopyCoderD2Ev
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZThn8_N9NCompress10CCopyCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress10CCopyCoderD1Ev,@function
_ZThn8_N9NCompress10CCopyCoderD1Ev:     # @_ZThn8_N9NCompress10CCopyCoderD1Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN9NCompress10CCopyCoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress10CCopyCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	movq	16(%rdi), %rdi
	jmp	MidFree                 # TAILCALL
.Lfunc_end2:
	.size	_ZThn8_N9NCompress10CCopyCoderD1Ev, .Lfunc_end2-_ZThn8_N9NCompress10CCopyCoderD1Ev
	.cfi_endproc

	.globl	_ZN9NCompress10CCopyCoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress10CCopyCoderD0Ev,@function
_ZN9NCompress10CCopyCoderD0Ev:          # @_ZN9NCompress10CCopyCoderD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 32
.Lcfi4:
	.cfi_offset %rbx, -24
.Lcfi5:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress10CCopyCoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress10CCopyCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	24(%rbx), %rdi
.Ltmp0:
	callq	MidFree
.Ltmp1:
# BB#1:                                 # %_ZN9NCompress10CCopyCoderD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN9NCompress10CCopyCoderD0Ev, .Lfunc_end3-_ZN9NCompress10CCopyCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end3-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn8_N9NCompress10CCopyCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress10CCopyCoderD0Ev,@function
_ZThn8_N9NCompress10CCopyCoderD0Ev:     # @_ZThn8_N9NCompress10CCopyCoderD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movl	$_ZTVN9NCompress10CCopyCoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress10CCopyCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	movq	16(%rdi), %rax
	leaq	-8(%rdi), %rbx
.Ltmp3:
	movq	%rax, %rdi
	callq	MidFree
.Ltmp4:
# BB#1:                                 # %_ZN9NCompress10CCopyCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB4_2:
.Ltmp5:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZThn8_N9NCompress10CCopyCoderD0Ev, .Lfunc_end4-_ZThn8_N9NCompress10CCopyCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Lfunc_end4-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN9NCompress10CCopyCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS6_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress10CCopyCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS6_P21ICompressProgressInfo,@function
_ZN9NCompress10CCopyCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS6_P21ICompressProgressInfo: # @_ZN9NCompress10CCopyCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS6_P21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 64
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %rbp
	movq	%rdx, %r14
	movq	%rsi, %r13
	movq	%rdi, %r12
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.LBB5_3
# BB#1:
	movl	$131072, %edi           # imm = 0x20000
	callq	MidAlloc
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.LBB5_2
.LBB5_3:
	leaq	32(%r12), %rbx
	movq	$0, 32(%r12)
	testq	%rbp, %rbp
	je	.LBB5_11
# BB#4:                                 # %.split.preheader
	xorl	%eax, %eax
	jmp	.LBB5_5
	.p2align	4, 0x90
.LBB5_25:                               # %..thread_crit_edge
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	(%rbx), %rax
.LBB5_5:                                # %.split
                                        # =>This Inner Loop Header: Depth=1
	movl	$131072, 4(%rsp)        # imm = 0x20000
	movq	(%rbp), %rcx
	subq	%rax, %rcx
	movl	$131072, %edx           # imm = 0x20000
	cmpq	$131071, %rcx           # imm = 0x1FFFF
	ja	.LBB5_7
# BB#6:                                 #   in Loop: Header=BB5_5 Depth=1
	movl	%ecx, 4(%rsp)
	movl	%ecx, %edx
.LBB5_7:                                #   in Loop: Header=BB5_5 Depth=1
	movq	(%r13), %rax
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	leaq	4(%rsp), %rcx
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB5_10
# BB#8:                                 #   in Loop: Header=BB5_5 Depth=1
	movl	4(%rsp), %edx
	testq	%rdx, %rdx
	je	.LBB5_9
# BB#20:                                #   in Loop: Header=BB5_5 Depth=1
	testq	%r14, %r14
	je	.LBB5_23
# BB#21:                                #   in Loop: Header=BB5_5 Depth=1
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB5_10
# BB#22:                                # %._crit_edge
                                        #   in Loop: Header=BB5_5 Depth=1
	movl	4(%rsp), %edx
.LBB5_23:                               #   in Loop: Header=BB5_5 Depth=1
	movl	%edx, %eax
	addq	(%rbx), %rax
	testq	%r15, %r15
	movq	%rax, (%rbx)
	je	.LBB5_5
# BB#24:                                #   in Loop: Header=BB5_5 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rbx, %rdx
	callq	*40(%rax)
	testl	%eax, %eax
	je	.LBB5_25
	jmp	.LBB5_10
.LBB5_11:                               # %.split.us.preheader
	movl	$131072, 4(%rsp)        # imm = 0x20000
	movq	(%r13), %rbp
	leaq	4(%rsp), %rcx
	movl	$131072, %edx           # imm = 0x20000
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	*40(%rbp)
	testl	%eax, %eax
	jne	.LBB5_10
# BB#12:                                # %.lr.ph.preheader
	leaq	4(%rsp), %rbp
	.p2align	4, 0x90
.LBB5_13:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%rsp), %edx
	testq	%rdx, %rdx
	je	.LBB5_9
# BB#14:                                #   in Loop: Header=BB5_13 Depth=1
	testq	%r14, %r14
	je	.LBB5_17
# BB#15:                                #   in Loop: Header=BB5_13 Depth=1
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB5_10
# BB#16:                                # %._crit_edge40
                                        #   in Loop: Header=BB5_13 Depth=1
	movl	4(%rsp), %edx
.LBB5_17:                               #   in Loop: Header=BB5_13 Depth=1
	movl	%edx, %eax
	addq	%rax, (%rbx)
	testq	%r15, %r15
	je	.LBB5_19
# BB#18:                                #   in Loop: Header=BB5_13 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rbx, %rdx
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB5_10
.LBB5_19:                               # %.thread.us
                                        #   in Loop: Header=BB5_13 Depth=1
	movq	24(%r12), %rsi
	movl	$131072, 4(%rsp)        # imm = 0x20000
	movq	(%r13), %rax
	movl	$131072, %edx           # imm = 0x20000
	movq	%r13, %rdi
	movq	%rbp, %rcx
	callq	*40(%rax)
	testl	%eax, %eax
	je	.LBB5_13
	jmp	.LBB5_10
.LBB5_9:                                # %.thread32
	xorl	%eax, %eax
	jmp	.LBB5_10
.LBB5_2:
	movl	$-2147024882, %eax      # imm = 0x8007000E
.LBB5_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN9NCompress10CCopyCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS6_P21ICompressProgressInfo, .Lfunc_end5-_ZN9NCompress10CCopyCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS6_P21ICompressProgressInfo
	.cfi_endproc

	.globl	_ZN9NCompress10CCopyCoder24GetInStreamProcessedSizeEPy
	.p2align	4, 0x90
	.type	_ZN9NCompress10CCopyCoder24GetInStreamProcessedSizeEPy,@function
_ZN9NCompress10CCopyCoder24GetInStreamProcessedSizeEPy: # @_ZN9NCompress10CCopyCoder24GetInStreamProcessedSizeEPy
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end6:
	.size	_ZN9NCompress10CCopyCoder24GetInStreamProcessedSizeEPy, .Lfunc_end6-_ZN9NCompress10CCopyCoder24GetInStreamProcessedSizeEPy
	.cfi_endproc

	.globl	_ZThn8_N9NCompress10CCopyCoder24GetInStreamProcessedSizeEPy
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress10CCopyCoder24GetInStreamProcessedSizeEPy,@function
_ZThn8_N9NCompress10CCopyCoder24GetInStreamProcessedSizeEPy: # @_ZThn8_N9NCompress10CCopyCoder24GetInStreamProcessedSizeEPy
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end7:
	.size	_ZThn8_N9NCompress10CCopyCoder24GetInStreamProcessedSizeEPy, .Lfunc_end7-_ZThn8_N9NCompress10CCopyCoder24GetInStreamProcessedSizeEPy
	.cfi_endproc

	.globl	_ZN9NCompress10CopyStreamEP19ISequentialInStreamP20ISequentialOutStreamP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress10CopyStreamEP19ISequentialInStreamP20ISequentialOutStreamP21ICompressProgressInfo,@function
_ZN9NCompress10CopyStreamEP19ISequentialInStreamP20ISequentialOutStreamP21ICompressProgressInfo: # @_ZN9NCompress10CopyStreamEP19ISequentialInStreamP20ISequentialOutStreamP21ICompressProgressInfo
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 80
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %r15
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	%rbx, %r14
	movl	$_ZTVN9NCompress10CCopyCoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress10CCopyCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rbx)
	movl	$1, 16(%rbx)
.Ltmp6:
	movl	$131072, %edi           # imm = 0x20000
	movq	%r14, 16(%rsp)          # 8-byte Spill
	callq	MidAlloc
	movq	%rax, %rsi
.Ltmp7:
# BB#1:                                 # %.noexc
	leaq	24(%rbx), %rbp
	movq	%rsi, (%rbp)
	testq	%rsi, %rsi
	je	.LBB8_2
# BB#3:
	addq	$32, %rbx
	movq	$0, 32(%r14)
	testq	%r13, %r13
	jne	.LBB8_13
# BB#4:                                 # %.split.us.i.us.preheader
	leaq	12(%rsp), %r13
	jmp	.LBB8_5
	.p2align	4, 0x90
.LBB8_21:                               # %.thread.us.i
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	(%rbp), %rsi
.LBB8_13:                               # %.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$131072, 12(%rsp)       # imm = 0x20000
	movq	(%r15), %rax
.Ltmp9:
	movl	$131072, %edx           # imm = 0x20000
	movq	%r15, %rdi
	leaq	12(%rsp), %rcx
	callq	*40(%rax)
	movl	%eax, %r14d
.Ltmp10:
# BB#14:                                # %.noexc10
                                        #   in Loop: Header=BB8_13 Depth=1
	testl	%r14d, %r14d
	jne	.LBB8_23
# BB#15:                                #   in Loop: Header=BB8_13 Depth=1
	movl	12(%rsp), %edx
	testq	%rdx, %rdx
	je	.LBB8_22
# BB#16:                                #   in Loop: Header=BB8_13 Depth=1
	movq	(%rbp), %rsi
.Ltmp11:
	movq	%r13, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	%eax, %r14d
.Ltmp12:
# BB#17:                                # %.noexc11
                                        #   in Loop: Header=BB8_13 Depth=1
	testl	%r14d, %r14d
	jne	.LBB8_23
# BB#18:                                # %._crit_edge40.i
                                        #   in Loop: Header=BB8_13 Depth=1
	movl	12(%rsp), %eax
	addq	%rax, (%rbx)
	testq	%r12, %r12
	je	.LBB8_21
# BB#19:                                #   in Loop: Header=BB8_13 Depth=1
	movq	(%r12), %rax
.Ltmp13:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rbx, %rdx
	callq	*40(%rax)
	movl	%eax, %r14d
.Ltmp14:
# BB#20:                                # %.noexc12
                                        #   in Loop: Header=BB8_13 Depth=1
	testl	%r14d, %r14d
	je	.LBB8_21
	jmp	.LBB8_23
	.p2align	4, 0x90
.LBB8_11:                               # %.thread.us.i.us
                                        #   in Loop: Header=BB8_5 Depth=1
	movq	(%rbp), %rsi
.LBB8_5:                                # %.split.us.i.us
                                        # =>This Inner Loop Header: Depth=1
	movl	$131072, 12(%rsp)       # imm = 0x20000
	movq	(%r15), %rax
.Ltmp16:
	movl	$131072, %edx           # imm = 0x20000
	movq	%r15, %rdi
	movq	%r13, %rcx
	callq	*40(%rax)
	movl	%eax, %r14d
.Ltmp17:
# BB#6:                                 # %.noexc10.us
                                        #   in Loop: Header=BB8_5 Depth=1
	testl	%r14d, %r14d
	jne	.LBB8_23
# BB#7:                                 #   in Loop: Header=BB8_5 Depth=1
	movl	12(%rsp), %eax
	testq	%rax, %rax
	je	.LBB8_22
# BB#8:                                 #   in Loop: Header=BB8_5 Depth=1
	addq	%rax, (%rbx)
	testq	%r12, %r12
	je	.LBB8_11
# BB#9:                                 #   in Loop: Header=BB8_5 Depth=1
	movq	(%r12), %rax
.Ltmp18:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rbx, %rdx
	callq	*40(%rax)
	movl	%eax, %r14d
.Ltmp19:
# BB#10:                                # %.noexc12.us
                                        #   in Loop: Header=BB8_5 Depth=1
	testl	%r14d, %r14d
	je	.LBB8_11
	jmp	.LBB8_23
.LBB8_2:
	movl	$-2147024882, %r14d     # imm = 0x8007000E
	jmp	.LBB8_23
.LBB8_22:                               # %.thread32.i
	xorl	%r14d, %r14d
.LBB8_23:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit5
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	callq	*16(%rax)
	movl	%r14d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_25:                               # %.loopexit.split-lp
.Ltmp8:
	jmp	.LBB8_26
.LBB8_12:                               # %.loopexit.us-lcssa.us
.Ltmp20:
	jmp	.LBB8_26
.LBB8_24:                               # %.loopexit.us-lcssa
.Ltmp15:
.LBB8_26:                               # %.loopexit
	movq	%rax, %rbx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp21:
	callq	*16(%rax)
.Ltmp22:
# BB#27:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB8_28:
.Ltmp23:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN9NCompress10CopyStreamEP19ISequentialInStreamP20ISequentialOutStreamP21ICompressProgressInfo, .Lfunc_end8-_ZN9NCompress10CopyStreamEP19ISequentialInStreamP20ISequentialOutStreamP21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp6-.Lfunc_begin2    #   Call between .Lfunc_begin2 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin2    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin2    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin2    # >> Call Site 3 <<
	.long	.Ltmp14-.Ltmp9          #   Call between .Ltmp9 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin2   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp19-.Ltmp16         #   Call between .Ltmp16 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp21-.Ltmp19         #   Call between .Ltmp19 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin2   #     jumps to .Ltmp23
	.byte	1                       #   On action: 1
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Lfunc_end8-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -32
.Lcfi41:
	.cfi_offset %r14, -24
.Lcfi42:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB9_3
# BB#1:
	movl	$IID_ICompressGetInStreamProcessedSize, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB9_2
.LBB9_3:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB9_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB9_2:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB9_4
.Lfunc_end9:
	.size	_ZN9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end9-_ZN9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress10CCopyCoder6AddRefEv,"axG",@progbits,_ZN9NCompress10CCopyCoder6AddRefEv,comdat
	.weak	_ZN9NCompress10CCopyCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress10CCopyCoder6AddRefEv,@function
_ZN9NCompress10CCopyCoder6AddRefEv:     # @_ZN9NCompress10CCopyCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end10:
	.size	_ZN9NCompress10CCopyCoder6AddRefEv, .Lfunc_end10-_ZN9NCompress10CCopyCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress10CCopyCoder7ReleaseEv,"axG",@progbits,_ZN9NCompress10CCopyCoder7ReleaseEv,comdat
	.weak	_ZN9NCompress10CCopyCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress10CCopyCoder7ReleaseEv,@function
_ZN9NCompress10CCopyCoder7ReleaseEv:    # @_ZN9NCompress10CCopyCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB11_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB11_2:
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZN9NCompress10CCopyCoder7ReleaseEv, .Lfunc_end11-_ZN9NCompress10CCopyCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB12_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB12_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB12_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB12_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB12_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB12_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB12_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB12_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB12_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB12_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB12_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB12_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB12_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB12_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB12_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB12_32
.LBB12_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressGetInStreamProcessedSize(%rip), %cl
	jne	.LBB12_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressGetInStreamProcessedSize+1(%rip), %cl
	jne	.LBB12_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressGetInStreamProcessedSize+2(%rip), %cl
	jne	.LBB12_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressGetInStreamProcessedSize+3(%rip), %cl
	jne	.LBB12_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressGetInStreamProcessedSize+4(%rip), %cl
	jne	.LBB12_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressGetInStreamProcessedSize+5(%rip), %cl
	jne	.LBB12_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressGetInStreamProcessedSize+6(%rip), %cl
	jne	.LBB12_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressGetInStreamProcessedSize+7(%rip), %cl
	jne	.LBB12_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressGetInStreamProcessedSize+8(%rip), %cl
	jne	.LBB12_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressGetInStreamProcessedSize+9(%rip), %cl
	jne	.LBB12_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressGetInStreamProcessedSize+10(%rip), %cl
	jne	.LBB12_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressGetInStreamProcessedSize+11(%rip), %cl
	jne	.LBB12_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressGetInStreamProcessedSize+12(%rip), %cl
	jne	.LBB12_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressGetInStreamProcessedSize+13(%rip), %cl
	jne	.LBB12_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressGetInStreamProcessedSize+14(%rip), %cl
	jne	.LBB12_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICompressGetInStreamProcessedSize+15(%rip), %cl
	jne	.LBB12_33
.LBB12_32:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB12_33:                              # %_ZN9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end12:
	.size	_ZThn8_N9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end12-_ZThn8_N9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress10CCopyCoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress10CCopyCoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress10CCopyCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress10CCopyCoder6AddRefEv,@function
_ZThn8_N9NCompress10CCopyCoder6AddRefEv: # @_ZThn8_N9NCompress10CCopyCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end13:
	.size	_ZThn8_N9NCompress10CCopyCoder6AddRefEv, .Lfunc_end13-_ZThn8_N9NCompress10CCopyCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress10CCopyCoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress10CCopyCoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress10CCopyCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress10CCopyCoder7ReleaseEv,@function
_ZThn8_N9NCompress10CCopyCoder7ReleaseEv: # @_ZThn8_N9NCompress10CCopyCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB14_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB14_2:                               # %_ZN9NCompress10CCopyCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end14:
	.size	_ZThn8_N9NCompress10CCopyCoder7ReleaseEv, .Lfunc_end14-_ZThn8_N9NCompress10CCopyCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB15_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB15_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB15_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB15_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB15_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB15_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB15_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB15_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB15_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB15_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB15_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB15_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB15_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB15_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB15_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB15_16:
	xorl	%eax, %eax
	retq
.Lfunc_end15:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end15-_ZeqRK4GUIDS1_
	.cfi_endproc

	.type	_ZTVN9NCompress10CCopyCoderE,@object # @_ZTVN9NCompress10CCopyCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress10CCopyCoderE
	.p2align	3
_ZTVN9NCompress10CCopyCoderE:
	.quad	0
	.quad	_ZTIN9NCompress10CCopyCoderE
	.quad	_ZN9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress10CCopyCoder6AddRefEv
	.quad	_ZN9NCompress10CCopyCoder7ReleaseEv
	.quad	_ZN9NCompress10CCopyCoderD2Ev
	.quad	_ZN9NCompress10CCopyCoderD0Ev
	.quad	_ZN9NCompress10CCopyCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS6_P21ICompressProgressInfo
	.quad	_ZN9NCompress10CCopyCoder24GetInStreamProcessedSizeEPy
	.quad	-8
	.quad	_ZTIN9NCompress10CCopyCoderE
	.quad	_ZThn8_N9NCompress10CCopyCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress10CCopyCoder6AddRefEv
	.quad	_ZThn8_N9NCompress10CCopyCoder7ReleaseEv
	.quad	_ZThn8_N9NCompress10CCopyCoderD1Ev
	.quad	_ZThn8_N9NCompress10CCopyCoderD0Ev
	.quad	_ZThn8_N9NCompress10CCopyCoder24GetInStreamProcessedSizeEPy
	.size	_ZTVN9NCompress10CCopyCoderE, 136

	.type	_ZTSN9NCompress10CCopyCoderE,@object # @_ZTSN9NCompress10CCopyCoderE
	.globl	_ZTSN9NCompress10CCopyCoderE
	.p2align	4
_ZTSN9NCompress10CCopyCoderE:
	.asciz	"N9NCompress10CCopyCoderE"
	.size	_ZTSN9NCompress10CCopyCoderE, 25

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS33ICompressGetInStreamProcessedSize,@object # @_ZTS33ICompressGetInStreamProcessedSize
	.section	.rodata._ZTS33ICompressGetInStreamProcessedSize,"aG",@progbits,_ZTS33ICompressGetInStreamProcessedSize,comdat
	.weak	_ZTS33ICompressGetInStreamProcessedSize
	.p2align	4
_ZTS33ICompressGetInStreamProcessedSize:
	.asciz	"33ICompressGetInStreamProcessedSize"
	.size	_ZTS33ICompressGetInStreamProcessedSize, 36

	.type	_ZTI33ICompressGetInStreamProcessedSize,@object # @_ZTI33ICompressGetInStreamProcessedSize
	.section	.rodata._ZTI33ICompressGetInStreamProcessedSize,"aG",@progbits,_ZTI33ICompressGetInStreamProcessedSize,comdat
	.weak	_ZTI33ICompressGetInStreamProcessedSize
	.p2align	4
_ZTI33ICompressGetInStreamProcessedSize:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS33ICompressGetInStreamProcessedSize
	.quad	_ZTI8IUnknown
	.size	_ZTI33ICompressGetInStreamProcessedSize, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress10CCopyCoderE,@object # @_ZTIN9NCompress10CCopyCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress10CCopyCoderE
	.p2align	4
_ZTIN9NCompress10CCopyCoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress10CCopyCoderE
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI33ICompressGetInStreamProcessedSize
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTIN9NCompress10CCopyCoderE, 72


	.globl	_ZN9NCompress10CCopyCoderD1Ev
	.type	_ZN9NCompress10CCopyCoderD1Ev,@function
_ZN9NCompress10CCopyCoderD1Ev = _ZN9NCompress10CCopyCoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
