	.text
	.file	"volume.bc"
	.globl	volume
	.p2align	4, 0x90
	.type	volume,@function
volume:                                 # @volume
	.cfi_startproc
# BB#0:
	movss	24(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	8(%rsp), %xmm0
	subss	12(%rsp), %xmm1
	mulss	%xmm0, %xmm1
	movss	32(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	16(%rsp), %xmm2
	mulss	%xmm1, %xmm2
	movss	36(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	20(%rsp), %xmm0
	mulss	%xmm2, %xmm0
	retq
.Lfunc_end0:
	.size	volume, .Lfunc_end0-volume
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
