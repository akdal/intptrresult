	.text
	.file	"showbord.bc"
	.globl	showboard
	.p2align	4, 0x90
	.type	showboard,@function
showboard:                              # @showboard
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	$.Lstr, %edi
	callq	puts
	movl	$.L.str.1, %edi
	movl	$19, %esi
	xorl	%eax, %eax
	callq	printf
	movq	$-19, %rbx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movzbl	p+19(%rbx), %eax
	cmpb	$1, %al
	je	.LBB0_4
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	testb	%al, %al
	jne	.LBB0_5
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	movl	$.L.str.2, %edi
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
	movl	$.L.str.3, %edi
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_5:                                #   in Loop: Header=BB0_1 Depth=1
	movl	$.L.str.4, %edi
.LBB0_6:                                #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	jne	.LBB0_1
# BB#7:
	movl	$.L.str.1, %edi
	movl	$19, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	movl	$18, %esi
	xorl	%eax, %eax
	callq	printf
	movq	$-19, %rbx
	.p2align	4, 0x90
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	movzbl	p+38(%rbx), %eax
	testb	%al, %al
	je	.LBB0_11
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=1
	cmpb	$1, %al
	jne	.LBB0_12
# BB#10:                                #   in Loop: Header=BB0_8 Depth=1
	movl	$.L.str.3, %edi
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_11:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$.L.str.2, %edi
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_12:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$.L.str.4, %edi
.LBB0_13:                               #   in Loop: Header=BB0_8 Depth=1
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	jne	.LBB0_8
# BB#14:
	movl	$.L.str.1, %edi
	movl	$18, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	movl	$17, %esi
	xorl	%eax, %eax
	callq	printf
	movq	$-19, %rbx
	.p2align	4, 0x90
.LBB0_15:                               # =>This Inner Loop Header: Depth=1
	movzbl	p+57(%rbx), %eax
	testb	%al, %al
	je	.LBB0_18
# BB#16:                                #   in Loop: Header=BB0_15 Depth=1
	cmpb	$1, %al
	jne	.LBB0_19
# BB#17:                                #   in Loop: Header=BB0_15 Depth=1
	movl	$.L.str.3, %edi
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_15 Depth=1
	movl	$.L.str.2, %edi
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_19:                               #   in Loop: Header=BB0_15 Depth=1
	movl	$.L.str.4, %edi
.LBB0_20:                               #   in Loop: Header=BB0_15 Depth=1
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	jne	.LBB0_15
# BB#21:
	movl	$.L.str.1, %edi
	movl	$17, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	movb	p+57(%rip), %al
	testb	%al, %al
	je	.LBB0_24
# BB#22:
	cmpb	$1, %al
	jne	.LBB0_25
# BB#23:
	movl	$.L.str.3, %edi
	jmp	.LBB0_26
.LBB0_24:
	movl	$.L.str.2, %edi
	jmp	.LBB0_26
.LBB0_25:
	movl	$.L.str.4, %edi
.LBB0_26:
	xorl	%eax, %eax
	callq	printf
	movb	p+58(%rip), %al
	testb	%al, %al
	je	.LBB0_29
# BB#27:
	cmpb	$1, %al
	jne	.LBB0_30
# BB#28:
	movl	$.L.str.3, %edi
	jmp	.LBB0_31
.LBB0_29:
	movl	$.L.str.2, %edi
	jmp	.LBB0_31
.LBB0_30:
	movl	$.L.str.4, %edi
.LBB0_31:
	xorl	%eax, %eax
	callq	printf
	movb	p+59(%rip), %al
	testb	%al, %al
	je	.LBB0_34
# BB#32:
	cmpb	$1, %al
	jne	.LBB0_35
# BB#33:
	movl	$.L.str.3, %edi
	jmp	.LBB0_36
.LBB0_34:
	movl	$.L.str.2, %edi
	jmp	.LBB0_36
.LBB0_35:
	movl	$.L.str.4, %edi
.LBB0_36:
	xorl	%eax, %eax
	callq	printf
	movb	p+60(%rip), %al
	testb	%al, %al
	je	.LBB0_39
# BB#37:
	cmpb	$1, %al
	jne	.LBB0_40
# BB#38:
	movl	$.L.str.3, %edi
	jmp	.LBB0_41
.LBB0_39:
	movl	$.L.str.7, %edi
	jmp	.LBB0_41
.LBB0_40:
	movl	$.L.str.4, %edi
.LBB0_41:                               # %.preheader122.preheader
	xorl	%eax, %eax
	callq	printf
	movb	p+61(%rip), %al
	cmpb	$1, %al
	je	.LBB0_44
# BB#42:                                # %.preheader122.preheader
	testb	%al, %al
	jne	.LBB0_45
# BB#43:
	movl	$.L.str.2, %edi
	jmp	.LBB0_46
.LBB0_44:
	movl	$.L.str.3, %edi
	jmp	.LBB0_46
.LBB0_45:
	movl	$.L.str.4, %edi
.LBB0_46:                               # %.preheader122.1217
	xorl	%eax, %eax
	callq	printf
	movb	p+62(%rip), %al
	testb	%al, %al
	je	.LBB0_49
# BB#47:                                # %.preheader122.1217
	cmpb	$1, %al
	jne	.LBB0_50
# BB#48:
	movl	$.L.str.3, %edi
	jmp	.LBB0_51
.LBB0_49:
	movl	$.L.str.2, %edi
	jmp	.LBB0_51
.LBB0_50:
	movl	$.L.str.4, %edi
.LBB0_51:                               # %.preheader122.2218
	xorl	%eax, %eax
	callq	printf
	movb	p+63(%rip), %al
	testb	%al, %al
	je	.LBB0_54
# BB#52:                                # %.preheader122.2218
	cmpb	$1, %al
	jne	.LBB0_55
# BB#53:
	movl	$.L.str.3, %edi
	jmp	.LBB0_56
.LBB0_54:
	movl	$.L.str.2, %edi
	jmp	.LBB0_56
.LBB0_55:
	movl	$.L.str.4, %edi
.LBB0_56:                               # %.preheader122.3219
	xorl	%eax, %eax
	callq	printf
	movb	p+64(%rip), %al
	testb	%al, %al
	je	.LBB0_59
# BB#57:                                # %.preheader122.3219
	cmpb	$1, %al
	jne	.LBB0_60
# BB#58:
	movl	$.L.str.3, %edi
	jmp	.LBB0_61
.LBB0_59:
	movl	$.L.str.2, %edi
	jmp	.LBB0_61
.LBB0_60:
	movl	$.L.str.4, %edi
.LBB0_61:                               # %.preheader122.4220
	xorl	%eax, %eax
	callq	printf
	movb	p+65(%rip), %al
	testb	%al, %al
	je	.LBB0_64
# BB#62:                                # %.preheader122.4220
	cmpb	$1, %al
	jne	.LBB0_65
# BB#63:
	movl	$.L.str.3, %edi
	jmp	.LBB0_66
.LBB0_64:
	movl	$.L.str.2, %edi
	jmp	.LBB0_66
.LBB0_65:
	movl	$.L.str.4, %edi
.LBB0_66:
	xorl	%eax, %eax
	callq	printf
	movb	p+66(%rip), %al
	testb	%al, %al
	je	.LBB0_69
# BB#67:
	cmpb	$1, %al
	jne	.LBB0_70
# BB#68:
	movl	$.L.str.3, %edi
	jmp	.LBB0_71
.LBB0_69:
	movl	$.L.str.7, %edi
	jmp	.LBB0_71
.LBB0_70:
	movl	$.L.str.4, %edi
.LBB0_71:                               # %.preheader121.preheader
	xorl	%eax, %eax
	callq	printf
	movb	p+67(%rip), %al
	cmpb	$1, %al
	je	.LBB0_74
# BB#72:                                # %.preheader121.preheader
	testb	%al, %al
	jne	.LBB0_75
# BB#73:
	movl	$.L.str.2, %edi
	jmp	.LBB0_76
.LBB0_74:
	movl	$.L.str.3, %edi
	jmp	.LBB0_76
.LBB0_75:
	movl	$.L.str.4, %edi
.LBB0_76:                               # %.preheader121.1210
	xorl	%eax, %eax
	callq	printf
	movb	p+68(%rip), %al
	testb	%al, %al
	je	.LBB0_79
# BB#77:                                # %.preheader121.1210
	cmpb	$1, %al
	jne	.LBB0_80
# BB#78:
	movl	$.L.str.3, %edi
	jmp	.LBB0_81
.LBB0_79:
	movl	$.L.str.2, %edi
	jmp	.LBB0_81
.LBB0_80:
	movl	$.L.str.4, %edi
.LBB0_81:                               # %.preheader121.2211
	xorl	%eax, %eax
	callq	printf
	movb	p+69(%rip), %al
	testb	%al, %al
	je	.LBB0_84
# BB#82:                                # %.preheader121.2211
	cmpb	$1, %al
	jne	.LBB0_85
# BB#83:
	movl	$.L.str.3, %edi
	jmp	.LBB0_86
.LBB0_84:
	movl	$.L.str.2, %edi
	jmp	.LBB0_86
.LBB0_85:
	movl	$.L.str.4, %edi
.LBB0_86:                               # %.preheader121.3212
	xorl	%eax, %eax
	callq	printf
	movb	p+70(%rip), %al
	testb	%al, %al
	je	.LBB0_89
# BB#87:                                # %.preheader121.3212
	cmpb	$1, %al
	jne	.LBB0_90
# BB#88:
	movl	$.L.str.3, %edi
	jmp	.LBB0_91
.LBB0_89:
	movl	$.L.str.2, %edi
	jmp	.LBB0_91
.LBB0_90:
	movl	$.L.str.4, %edi
.LBB0_91:                               # %.preheader121.4213
	xorl	%eax, %eax
	callq	printf
	movb	p+71(%rip), %al
	testb	%al, %al
	je	.LBB0_94
# BB#92:                                # %.preheader121.4213
	cmpb	$1, %al
	jne	.LBB0_95
# BB#93:
	movl	$.L.str.3, %edi
	jmp	.LBB0_96
.LBB0_94:
	movl	$.L.str.2, %edi
	jmp	.LBB0_96
.LBB0_95:
	movl	$.L.str.4, %edi
.LBB0_96:
	xorl	%eax, %eax
	callq	printf
	movb	p+72(%rip), %al
	testb	%al, %al
	je	.LBB0_99
# BB#97:
	cmpb	$1, %al
	jne	.LBB0_100
# BB#98:
	movl	$.L.str.3, %edi
	jmp	.LBB0_101
.LBB0_99:
	movl	$.L.str.7, %edi
	jmp	.LBB0_101
.LBB0_100:
	movl	$.L.str.4, %edi
.LBB0_101:                              # %.preheader120.preheader
	xorl	%eax, %eax
	callq	printf
	movb	p+73(%rip), %al
	cmpb	$1, %al
	je	.LBB0_104
# BB#102:                               # %.preheader120.preheader
	testb	%al, %al
	jne	.LBB0_105
# BB#103:
	movl	$.L.str.2, %edi
	jmp	.LBB0_106
.LBB0_104:
	movl	$.L.str.3, %edi
	jmp	.LBB0_106
.LBB0_105:
	movl	$.L.str.4, %edi
.LBB0_106:                              # %.preheader120.1205
	xorl	%eax, %eax
	callq	printf
	movb	p+74(%rip), %al
	testb	%al, %al
	je	.LBB0_109
# BB#107:                               # %.preheader120.1205
	cmpb	$1, %al
	jne	.LBB0_110
# BB#108:
	movl	$.L.str.3, %edi
	jmp	.LBB0_111
.LBB0_109:
	movl	$.L.str.2, %edi
	jmp	.LBB0_111
.LBB0_110:
	movl	$.L.str.4, %edi
.LBB0_111:                              # %.preheader120.2206
	xorl	%eax, %eax
	callq	printf
	movb	p+75(%rip), %al
	testb	%al, %al
	je	.LBB0_114
# BB#112:                               # %.preheader120.2206
	cmpb	$1, %al
	jne	.LBB0_115
# BB#113:
	movl	$.L.str.3, %edi
	jmp	.LBB0_116
.LBB0_114:
	movl	$.L.str.2, %edi
	jmp	.LBB0_116
.LBB0_115:
	movl	$.L.str.4, %edi
.LBB0_116:
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	movl	umove(%rip), %eax
	cmpl	$1, %eax
	je	.LBB0_119
# BB#117:
	cmpl	$2, %eax
	jne	.LBB0_120
# BB#118:
	movl	$.Lstr.4, %edi
	callq	puts
	jmp	.LBB0_121
.LBB0_119:
	movl	$.Lstr.5, %edi
	callq	puts
	jmp	.LBB0_121
.LBB0_120:
	movl	$10, %edi
	callq	putchar
.LBB0_121:                              # %.preheader119.preheader
	movl	$4, %r14d
	movl	$p+76, %r15d
	.p2align	4, 0x90
.LBB0_122:                              # %.preheader119
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_123 Depth 2
	movl	$19, %r12d
	subl	%r14d, %r12d
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	printf
	movl	$19, %ebx
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB0_123:                              #   Parent Loop BB0_122 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp), %eax
	cmpb	$1, %al
	je	.LBB0_126
# BB#124:                               #   in Loop: Header=BB0_123 Depth=2
	testb	%al, %al
	jne	.LBB0_127
# BB#125:                               #   in Loop: Header=BB0_123 Depth=2
	movl	$.L.str.2, %edi
	jmp	.LBB0_128
	.p2align	4, 0x90
.LBB0_126:                              #   in Loop: Header=BB0_123 Depth=2
	movl	$.L.str.3, %edi
	jmp	.LBB0_128
	.p2align	4, 0x90
.LBB0_127:                              #   in Loop: Header=BB0_123 Depth=2
	movl	$.L.str.4, %edi
.LBB0_128:                              #   in Loop: Header=BB0_123 Depth=2
	xorl	%eax, %eax
	callq	printf
	incq	%rbp
	decq	%rbx
	jne	.LBB0_123
# BB#129:                               #   in Loop: Header=BB0_122 Depth=1
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	printf
	cmpl	$8, %r14d
	je	.LBB0_135
# BB#130:                               #   in Loop: Header=BB0_122 Depth=1
	cmpl	$4, %r14d
	jne	.LBB0_134
# BB#131:                               #   in Loop: Header=BB0_122 Depth=1
	movl	mymove(%rip), %eax
	cmpl	$2, %eax
	je	.LBB0_136
# BB#132:                               #   in Loop: Header=BB0_122 Depth=1
	cmpl	$1, %eax
	jne	.LBB0_134
# BB#133:                               #   in Loop: Header=BB0_122 Depth=1
	movl	$.Lstr.3, %edi
	jmp	.LBB0_137
	.p2align	4, 0x90
.LBB0_134:                              #   in Loop: Header=BB0_122 Depth=1
	movl	$10, %edi
	callq	putchar
	jmp	.LBB0_138
	.p2align	4, 0x90
.LBB0_135:                              #   in Loop: Header=BB0_122 Depth=1
	movl	mk(%rip), %esi
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB0_138
.LBB0_136:                              #   in Loop: Header=BB0_122 Depth=1
	movl	$.Lstr.2, %edi
.LBB0_137:                              #   in Loop: Header=BB0_122 Depth=1
	callq	puts
.LBB0_138:                              #   in Loop: Header=BB0_122 Depth=1
	incq	%r14
	addq	$19, %r15
	cmpq	$9, %r14
	jne	.LBB0_122
# BB#139:
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	printf
	movb	p+171(%rip), %al
	cmpb	$1, %al
	je	.LBB0_142
# BB#140:
	testb	%al, %al
	jne	.LBB0_143
# BB#141:
	movl	$.L.str.2, %edi
	jmp	.LBB0_144
.LBB0_142:
	movl	$.L.str.3, %edi
	jmp	.LBB0_144
.LBB0_143:
	movl	$.L.str.4, %edi
.LBB0_144:
	xorl	%eax, %eax
	callq	printf
	movb	p+172(%rip), %al
	testb	%al, %al
	je	.LBB0_147
# BB#145:
	cmpb	$1, %al
	jne	.LBB0_148
# BB#146:
	movl	$.L.str.3, %edi
	jmp	.LBB0_149
.LBB0_147:
	movl	$.L.str.2, %edi
	jmp	.LBB0_149
.LBB0_148:
	movl	$.L.str.4, %edi
.LBB0_149:
	xorl	%eax, %eax
	callq	printf
	movb	p+173(%rip), %al
	testb	%al, %al
	je	.LBB0_152
# BB#150:
	cmpb	$1, %al
	jne	.LBB0_153
# BB#151:
	movl	$.L.str.3, %edi
	jmp	.LBB0_154
.LBB0_152:
	movl	$.L.str.2, %edi
	jmp	.LBB0_154
.LBB0_153:
	movl	$.L.str.4, %edi
.LBB0_154:
	xorl	%eax, %eax
	callq	printf
	movb	p+174(%rip), %al
	testb	%al, %al
	je	.LBB0_157
# BB#155:
	cmpb	$1, %al
	jne	.LBB0_158
# BB#156:
	movl	$.L.str.3, %edi
	jmp	.LBB0_159
.LBB0_157:
	movl	$.L.str.7, %edi
	jmp	.LBB0_159
.LBB0_158:
	movl	$.L.str.4, %edi
.LBB0_159:                              # %.preheader118.preheader
	xorl	%eax, %eax
	callq	printf
	movb	p+175(%rip), %al
	cmpb	$1, %al
	je	.LBB0_162
# BB#160:                               # %.preheader118.preheader
	testb	%al, %al
	jne	.LBB0_163
# BB#161:
	movl	$.L.str.2, %edi
	jmp	.LBB0_164
.LBB0_162:
	movl	$.L.str.3, %edi
	jmp	.LBB0_164
.LBB0_163:
	movl	$.L.str.4, %edi
.LBB0_164:                              # %.preheader118.1189
	xorl	%eax, %eax
	callq	printf
	movb	p+176(%rip), %al
	testb	%al, %al
	je	.LBB0_167
# BB#165:                               # %.preheader118.1189
	cmpb	$1, %al
	jne	.LBB0_168
# BB#166:
	movl	$.L.str.3, %edi
	jmp	.LBB0_169
.LBB0_167:
	movl	$.L.str.2, %edi
	jmp	.LBB0_169
.LBB0_168:
	movl	$.L.str.4, %edi
.LBB0_169:                              # %.preheader118.2190
	xorl	%eax, %eax
	callq	printf
	movb	p+177(%rip), %al
	testb	%al, %al
	je	.LBB0_172
# BB#170:                               # %.preheader118.2190
	cmpb	$1, %al
	jne	.LBB0_173
# BB#171:
	movl	$.L.str.3, %edi
	jmp	.LBB0_174
.LBB0_172:
	movl	$.L.str.2, %edi
	jmp	.LBB0_174
.LBB0_173:
	movl	$.L.str.4, %edi
.LBB0_174:                              # %.preheader118.3191
	xorl	%eax, %eax
	callq	printf
	movb	p+178(%rip), %al
	testb	%al, %al
	je	.LBB0_177
# BB#175:                               # %.preheader118.3191
	cmpb	$1, %al
	jne	.LBB0_178
# BB#176:
	movl	$.L.str.3, %edi
	jmp	.LBB0_179
.LBB0_177:
	movl	$.L.str.2, %edi
	jmp	.LBB0_179
.LBB0_178:
	movl	$.L.str.4, %edi
.LBB0_179:                              # %.preheader118.4192
	xorl	%eax, %eax
	callq	printf
	movb	p+179(%rip), %al
	testb	%al, %al
	je	.LBB0_182
# BB#180:                               # %.preheader118.4192
	cmpb	$1, %al
	jne	.LBB0_183
# BB#181:
	movl	$.L.str.3, %edi
	jmp	.LBB0_184
.LBB0_182:
	movl	$.L.str.2, %edi
	jmp	.LBB0_184
.LBB0_183:
	movl	$.L.str.4, %edi
.LBB0_184:
	xorl	%eax, %eax
	callq	printf
	movb	p+180(%rip), %al
	testb	%al, %al
	je	.LBB0_187
# BB#185:
	cmpb	$1, %al
	jne	.LBB0_188
# BB#186:
	movl	$.L.str.3, %edi
	jmp	.LBB0_189
.LBB0_187:
	movl	$.L.str.7, %edi
	jmp	.LBB0_189
.LBB0_188:
	movl	$.L.str.4, %edi
.LBB0_189:                              # %.preheader117.preheader
	xorl	%eax, %eax
	callq	printf
	movb	p+181(%rip), %al
	cmpb	$1, %al
	je	.LBB0_192
# BB#190:                               # %.preheader117.preheader
	testb	%al, %al
	jne	.LBB0_193
# BB#191:
	movl	$.L.str.2, %edi
	jmp	.LBB0_194
.LBB0_192:
	movl	$.L.str.3, %edi
	jmp	.LBB0_194
.LBB0_193:
	movl	$.L.str.4, %edi
.LBB0_194:                              # %.preheader117.1182
	xorl	%eax, %eax
	callq	printf
	movb	p+182(%rip), %al
	testb	%al, %al
	je	.LBB0_197
# BB#195:                               # %.preheader117.1182
	cmpb	$1, %al
	jne	.LBB0_198
# BB#196:
	movl	$.L.str.3, %edi
	jmp	.LBB0_199
.LBB0_197:
	movl	$.L.str.2, %edi
	jmp	.LBB0_199
.LBB0_198:
	movl	$.L.str.4, %edi
.LBB0_199:                              # %.preheader117.2183
	xorl	%eax, %eax
	callq	printf
	movb	p+183(%rip), %al
	testb	%al, %al
	je	.LBB0_202
# BB#200:                               # %.preheader117.2183
	cmpb	$1, %al
	jne	.LBB0_203
# BB#201:
	movl	$.L.str.3, %edi
	jmp	.LBB0_204
.LBB0_202:
	movl	$.L.str.2, %edi
	jmp	.LBB0_204
.LBB0_203:
	movl	$.L.str.4, %edi
.LBB0_204:                              # %.preheader117.3184
	xorl	%eax, %eax
	callq	printf
	movb	p+184(%rip), %al
	testb	%al, %al
	je	.LBB0_207
# BB#205:                               # %.preheader117.3184
	cmpb	$1, %al
	jne	.LBB0_208
# BB#206:
	movl	$.L.str.3, %edi
	jmp	.LBB0_209
.LBB0_207:
	movl	$.L.str.2, %edi
	jmp	.LBB0_209
.LBB0_208:
	movl	$.L.str.4, %edi
.LBB0_209:                              # %.preheader117.4185
	xorl	%eax, %eax
	callq	printf
	movb	p+185(%rip), %al
	testb	%al, %al
	je	.LBB0_212
# BB#210:                               # %.preheader117.4185
	cmpb	$1, %al
	jne	.LBB0_213
# BB#211:
	movl	$.L.str.3, %edi
	jmp	.LBB0_214
.LBB0_212:
	movl	$.L.str.2, %edi
	jmp	.LBB0_214
.LBB0_213:
	movl	$.L.str.4, %edi
.LBB0_214:
	xorl	%eax, %eax
	callq	printf
	movb	p+186(%rip), %al
	testb	%al, %al
	je	.LBB0_217
# BB#215:
	cmpb	$1, %al
	jne	.LBB0_218
# BB#216:
	movl	$.L.str.3, %edi
	jmp	.LBB0_219
.LBB0_217:
	movl	$.L.str.7, %edi
	jmp	.LBB0_219
.LBB0_218:
	movl	$.L.str.4, %edi
.LBB0_219:                              # %.preheader116.preheader
	xorl	%eax, %eax
	callq	printf
	movb	p+187(%rip), %al
	cmpb	$1, %al
	je	.LBB0_222
# BB#220:                               # %.preheader116.preheader
	testb	%al, %al
	jne	.LBB0_223
# BB#221:
	movl	$.L.str.2, %edi
	jmp	.LBB0_224
.LBB0_222:
	movl	$.L.str.3, %edi
	jmp	.LBB0_224
.LBB0_223:
	movl	$.L.str.4, %edi
.LBB0_224:                              # %.preheader116.1177
	xorl	%eax, %eax
	callq	printf
	movb	p+188(%rip), %al
	testb	%al, %al
	je	.LBB0_227
# BB#225:                               # %.preheader116.1177
	cmpb	$1, %al
	jne	.LBB0_228
# BB#226:
	movl	$.L.str.3, %edi
	jmp	.LBB0_229
.LBB0_227:
	movl	$.L.str.2, %edi
	jmp	.LBB0_229
.LBB0_228:
	movl	$.L.str.4, %edi
.LBB0_229:                              # %.preheader116.2178
	xorl	%eax, %eax
	callq	printf
	movb	p+189(%rip), %al
	testb	%al, %al
	je	.LBB0_232
# BB#230:                               # %.preheader116.2178
	cmpb	$1, %al
	jne	.LBB0_233
# BB#231:
	movl	$.L.str.3, %edi
	jmp	.LBB0_234
.LBB0_232:
	movl	$.L.str.2, %edi
	jmp	.LBB0_234
.LBB0_233:
	movl	$.L.str.4, %edi
.LBB0_234:
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	printf
	movl	uk(%rip), %esi
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.1, %edi
	movl	$9, %esi
	xorl	%eax, %eax
	callq	printf
	movq	$-19, %rbx
	.p2align	4, 0x90
.LBB0_235:                              # =>This Inner Loop Header: Depth=1
	movzbl	p+209(%rbx), %eax
	cmpb	$1, %al
	je	.LBB0_238
# BB#236:                               #   in Loop: Header=BB0_235 Depth=1
	testb	%al, %al
	jne	.LBB0_239
# BB#237:                               #   in Loop: Header=BB0_235 Depth=1
	movl	$.L.str.2, %edi
	jmp	.LBB0_240
	.p2align	4, 0x90
.LBB0_238:                              #   in Loop: Header=BB0_235 Depth=1
	movl	$.L.str.3, %edi
	jmp	.LBB0_240
	.p2align	4, 0x90
.LBB0_239:                              #   in Loop: Header=BB0_235 Depth=1
	movl	$.L.str.4, %edi
.LBB0_240:                              #   in Loop: Header=BB0_235 Depth=1
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	jne	.LBB0_235
# BB#241:
	movl	$.L.str.1, %edi
	movl	$9, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	movl	$8, %esi
	xorl	%eax, %eax
	callq	printf
	movq	$-19, %rbx
	.p2align	4, 0x90
.LBB0_242:                              # =>This Inner Loop Header: Depth=1
	movzbl	p+228(%rbx), %eax
	testb	%al, %al
	je	.LBB0_245
# BB#243:                               #   in Loop: Header=BB0_242 Depth=1
	cmpb	$1, %al
	jne	.LBB0_246
# BB#244:                               #   in Loop: Header=BB0_242 Depth=1
	movl	$.L.str.3, %edi
	jmp	.LBB0_247
	.p2align	4, 0x90
.LBB0_245:                              #   in Loop: Header=BB0_242 Depth=1
	movl	$.L.str.2, %edi
	jmp	.LBB0_247
	.p2align	4, 0x90
.LBB0_246:                              #   in Loop: Header=BB0_242 Depth=1
	movl	$.L.str.4, %edi
.LBB0_247:                              #   in Loop: Header=BB0_242 Depth=1
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	jne	.LBB0_242
# BB#248:
	movl	$.L.str.1, %edi
	movl	$8, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	movl	$7, %esi
	xorl	%eax, %eax
	callq	printf
	movq	$-19, %rbx
	.p2align	4, 0x90
.LBB0_249:                              # =>This Inner Loop Header: Depth=1
	movzbl	p+247(%rbx), %eax
	testb	%al, %al
	je	.LBB0_252
# BB#250:                               #   in Loop: Header=BB0_249 Depth=1
	cmpb	$1, %al
	jne	.LBB0_253
# BB#251:                               #   in Loop: Header=BB0_249 Depth=1
	movl	$.L.str.3, %edi
	jmp	.LBB0_254
	.p2align	4, 0x90
.LBB0_252:                              #   in Loop: Header=BB0_249 Depth=1
	movl	$.L.str.2, %edi
	jmp	.LBB0_254
	.p2align	4, 0x90
.LBB0_253:                              #   in Loop: Header=BB0_249 Depth=1
	movl	$.L.str.4, %edi
.LBB0_254:                              #   in Loop: Header=BB0_249 Depth=1
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	jne	.LBB0_249
# BB#255:
	movl	$.L.str.1, %edi
	movl	$7, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	movl	$6, %esi
	xorl	%eax, %eax
	callq	printf
	movq	$-19, %rbx
	.p2align	4, 0x90
.LBB0_256:                              # =>This Inner Loop Header: Depth=1
	movzbl	p+266(%rbx), %eax
	testb	%al, %al
	je	.LBB0_259
# BB#257:                               #   in Loop: Header=BB0_256 Depth=1
	cmpb	$1, %al
	jne	.LBB0_260
# BB#258:                               #   in Loop: Header=BB0_256 Depth=1
	movl	$.L.str.3, %edi
	jmp	.LBB0_261
	.p2align	4, 0x90
.LBB0_259:                              #   in Loop: Header=BB0_256 Depth=1
	movl	$.L.str.2, %edi
	jmp	.LBB0_261
	.p2align	4, 0x90
.LBB0_260:                              #   in Loop: Header=BB0_256 Depth=1
	movl	$.L.str.4, %edi
.LBB0_261:                              #   in Loop: Header=BB0_256 Depth=1
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	jne	.LBB0_256
# BB#262:
	movl	$.L.str.1, %edi
	movl	$6, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	movl	$5, %esi
	xorl	%eax, %eax
	callq	printf
	movq	$-19, %rbx
	.p2align	4, 0x90
.LBB0_263:                              # =>This Inner Loop Header: Depth=1
	movzbl	p+285(%rbx), %eax
	testb	%al, %al
	je	.LBB0_266
# BB#264:                               #   in Loop: Header=BB0_263 Depth=1
	cmpb	$1, %al
	jne	.LBB0_267
# BB#265:                               #   in Loop: Header=BB0_263 Depth=1
	movl	$.L.str.3, %edi
	jmp	.LBB0_268
	.p2align	4, 0x90
.LBB0_266:                              #   in Loop: Header=BB0_263 Depth=1
	movl	$.L.str.2, %edi
	jmp	.LBB0_268
	.p2align	4, 0x90
.LBB0_267:                              #   in Loop: Header=BB0_263 Depth=1
	movl	$.L.str.4, %edi
.LBB0_268:                              #   in Loop: Header=BB0_263 Depth=1
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	jne	.LBB0_263
# BB#269:
	movl	$.L.str.1, %edi
	movl	$5, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	printf
	movb	p+285(%rip), %al
	testb	%al, %al
	je	.LBB0_272
# BB#270:
	cmpb	$1, %al
	jne	.LBB0_273
# BB#271:
	movl	$.L.str.3, %edi
	jmp	.LBB0_274
.LBB0_272:
	movl	$.L.str.2, %edi
	jmp	.LBB0_274
.LBB0_273:
	movl	$.L.str.4, %edi
.LBB0_274:
	xorl	%eax, %eax
	callq	printf
	movb	p+286(%rip), %al
	testb	%al, %al
	je	.LBB0_277
# BB#275:
	cmpb	$1, %al
	jne	.LBB0_278
# BB#276:
	movl	$.L.str.3, %edi
	jmp	.LBB0_279
.LBB0_277:
	movl	$.L.str.2, %edi
	jmp	.LBB0_279
.LBB0_278:
	movl	$.L.str.4, %edi
.LBB0_279:
	xorl	%eax, %eax
	callq	printf
	movb	p+287(%rip), %al
	testb	%al, %al
	je	.LBB0_282
# BB#280:
	cmpb	$1, %al
	jne	.LBB0_283
# BB#281:
	movl	$.L.str.3, %edi
	jmp	.LBB0_284
.LBB0_282:
	movl	$.L.str.2, %edi
	jmp	.LBB0_284
.LBB0_283:
	movl	$.L.str.4, %edi
.LBB0_284:
	xorl	%eax, %eax
	callq	printf
	movb	p+288(%rip), %al
	testb	%al, %al
	je	.LBB0_287
# BB#285:
	cmpb	$1, %al
	jne	.LBB0_288
# BB#286:
	movl	$.L.str.3, %edi
	jmp	.LBB0_289
.LBB0_287:
	movl	$.L.str.7, %edi
	jmp	.LBB0_289
.LBB0_288:
	movl	$.L.str.4, %edi
.LBB0_289:                              # %.preheader115.preheader
	xorl	%eax, %eax
	callq	printf
	movb	p+289(%rip), %al
	cmpb	$1, %al
	je	.LBB0_292
# BB#290:                               # %.preheader115.preheader
	testb	%al, %al
	jne	.LBB0_293
# BB#291:
	movl	$.L.str.2, %edi
	jmp	.LBB0_294
.LBB0_292:
	movl	$.L.str.3, %edi
	jmp	.LBB0_294
.LBB0_293:
	movl	$.L.str.4, %edi
.LBB0_294:                              # %.preheader115.1161
	xorl	%eax, %eax
	callq	printf
	movb	p+290(%rip), %al
	testb	%al, %al
	je	.LBB0_297
# BB#295:                               # %.preheader115.1161
	cmpb	$1, %al
	jne	.LBB0_298
# BB#296:
	movl	$.L.str.3, %edi
	jmp	.LBB0_299
.LBB0_297:
	movl	$.L.str.2, %edi
	jmp	.LBB0_299
.LBB0_298:
	movl	$.L.str.4, %edi
.LBB0_299:                              # %.preheader115.2162
	xorl	%eax, %eax
	callq	printf
	movb	p+291(%rip), %al
	testb	%al, %al
	je	.LBB0_302
# BB#300:                               # %.preheader115.2162
	cmpb	$1, %al
	jne	.LBB0_303
# BB#301:
	movl	$.L.str.3, %edi
	jmp	.LBB0_304
.LBB0_302:
	movl	$.L.str.2, %edi
	jmp	.LBB0_304
.LBB0_303:
	movl	$.L.str.4, %edi
.LBB0_304:                              # %.preheader115.3163
	xorl	%eax, %eax
	callq	printf
	movb	p+292(%rip), %al
	testb	%al, %al
	je	.LBB0_307
# BB#305:                               # %.preheader115.3163
	cmpb	$1, %al
	jne	.LBB0_308
# BB#306:
	movl	$.L.str.3, %edi
	jmp	.LBB0_309
.LBB0_307:
	movl	$.L.str.2, %edi
	jmp	.LBB0_309
.LBB0_308:
	movl	$.L.str.4, %edi
.LBB0_309:                              # %.preheader115.4164
	xorl	%eax, %eax
	callq	printf
	movb	p+293(%rip), %al
	testb	%al, %al
	je	.LBB0_312
# BB#310:                               # %.preheader115.4164
	cmpb	$1, %al
	jne	.LBB0_313
# BB#311:
	movl	$.L.str.3, %edi
	jmp	.LBB0_314
.LBB0_312:
	movl	$.L.str.2, %edi
	jmp	.LBB0_314
.LBB0_313:
	movl	$.L.str.4, %edi
.LBB0_314:
	xorl	%eax, %eax
	callq	printf
	movb	p+294(%rip), %al
	testb	%al, %al
	je	.LBB0_317
# BB#315:
	cmpb	$1, %al
	jne	.LBB0_318
# BB#316:
	movl	$.L.str.3, %edi
	jmp	.LBB0_319
.LBB0_317:
	movl	$.L.str.7, %edi
	jmp	.LBB0_319
.LBB0_318:
	movl	$.L.str.4, %edi
.LBB0_319:                              # %.preheader114.preheader
	xorl	%eax, %eax
	callq	printf
	movb	p+295(%rip), %al
	cmpb	$1, %al
	je	.LBB0_322
# BB#320:                               # %.preheader114.preheader
	testb	%al, %al
	jne	.LBB0_323
# BB#321:
	movl	$.L.str.2, %edi
	jmp	.LBB0_324
.LBB0_322:
	movl	$.L.str.3, %edi
	jmp	.LBB0_324
.LBB0_323:
	movl	$.L.str.4, %edi
.LBB0_324:                              # %.preheader114.1154
	xorl	%eax, %eax
	callq	printf
	movb	p+296(%rip), %al
	testb	%al, %al
	je	.LBB0_327
# BB#325:                               # %.preheader114.1154
	cmpb	$1, %al
	jne	.LBB0_328
# BB#326:
	movl	$.L.str.3, %edi
	jmp	.LBB0_329
.LBB0_327:
	movl	$.L.str.2, %edi
	jmp	.LBB0_329
.LBB0_328:
	movl	$.L.str.4, %edi
.LBB0_329:                              # %.preheader114.2155
	xorl	%eax, %eax
	callq	printf
	movb	p+297(%rip), %al
	testb	%al, %al
	je	.LBB0_332
# BB#330:                               # %.preheader114.2155
	cmpb	$1, %al
	jne	.LBB0_333
# BB#331:
	movl	$.L.str.3, %edi
	jmp	.LBB0_334
.LBB0_332:
	movl	$.L.str.2, %edi
	jmp	.LBB0_334
.LBB0_333:
	movl	$.L.str.4, %edi
.LBB0_334:                              # %.preheader114.3156
	xorl	%eax, %eax
	callq	printf
	movb	p+298(%rip), %al
	testb	%al, %al
	je	.LBB0_337
# BB#335:                               # %.preheader114.3156
	cmpb	$1, %al
	jne	.LBB0_338
# BB#336:
	movl	$.L.str.3, %edi
	jmp	.LBB0_339
.LBB0_337:
	movl	$.L.str.2, %edi
	jmp	.LBB0_339
.LBB0_338:
	movl	$.L.str.4, %edi
.LBB0_339:                              # %.preheader114.4157
	xorl	%eax, %eax
	callq	printf
	movb	p+299(%rip), %al
	testb	%al, %al
	je	.LBB0_342
# BB#340:                               # %.preheader114.4157
	cmpb	$1, %al
	jne	.LBB0_343
# BB#341:
	movl	$.L.str.3, %edi
	jmp	.LBB0_344
.LBB0_342:
	movl	$.L.str.2, %edi
	jmp	.LBB0_344
.LBB0_343:
	movl	$.L.str.4, %edi
.LBB0_344:
	xorl	%eax, %eax
	callq	printf
	movb	p+300(%rip), %al
	testb	%al, %al
	je	.LBB0_347
# BB#345:
	cmpb	$1, %al
	jne	.LBB0_348
# BB#346:
	movl	$.L.str.3, %edi
	jmp	.LBB0_349
.LBB0_347:
	movl	$.L.str.7, %edi
	jmp	.LBB0_349
.LBB0_348:
	movl	$.L.str.4, %edi
.LBB0_349:                              # %.preheader.preheader
	xorl	%eax, %eax
	callq	printf
	movb	p+301(%rip), %al
	cmpb	$1, %al
	je	.LBB0_352
# BB#350:                               # %.preheader.preheader
	testb	%al, %al
	jne	.LBB0_353
# BB#351:
	movl	$.L.str.2, %edi
	jmp	.LBB0_354
.LBB0_352:
	movl	$.L.str.3, %edi
	jmp	.LBB0_354
.LBB0_353:
	movl	$.L.str.4, %edi
.LBB0_354:                              # %.preheader.1149
	xorl	%eax, %eax
	callq	printf
	movb	p+302(%rip), %al
	testb	%al, %al
	je	.LBB0_357
# BB#355:                               # %.preheader.1149
	cmpb	$1, %al
	jne	.LBB0_358
# BB#356:
	movl	$.L.str.3, %edi
	jmp	.LBB0_359
.LBB0_357:
	movl	$.L.str.2, %edi
	jmp	.LBB0_359
.LBB0_358:
	movl	$.L.str.4, %edi
.LBB0_359:                              # %.preheader.2150
	xorl	%eax, %eax
	callq	printf
	movb	p+303(%rip), %al
	testb	%al, %al
	je	.LBB0_362
# BB#360:                               # %.preheader.2150
	cmpb	$1, %al
	jne	.LBB0_363
# BB#361:
	movl	$.L.str.3, %edi
	jmp	.LBB0_364
.LBB0_362:
	movl	$.L.str.2, %edi
	jmp	.LBB0_364
.LBB0_363:
	movl	$.L.str.4, %edi
.LBB0_364:
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	movl	$3, %esi
	xorl	%eax, %eax
	callq	printf
	movq	$-19, %rbx
	.p2align	4, 0x90
.LBB0_365:                              # =>This Inner Loop Header: Depth=1
	movzbl	p+323(%rbx), %eax
	cmpb	$1, %al
	je	.LBB0_368
# BB#366:                               #   in Loop: Header=BB0_365 Depth=1
	testb	%al, %al
	jne	.LBB0_369
# BB#367:                               #   in Loop: Header=BB0_365 Depth=1
	movl	$.L.str.2, %edi
	jmp	.LBB0_370
	.p2align	4, 0x90
.LBB0_368:                              #   in Loop: Header=BB0_365 Depth=1
	movl	$.L.str.3, %edi
	jmp	.LBB0_370
	.p2align	4, 0x90
.LBB0_369:                              #   in Loop: Header=BB0_365 Depth=1
	movl	$.L.str.4, %edi
.LBB0_370:                              #   in Loop: Header=BB0_365 Depth=1
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	jne	.LBB0_365
# BB#371:
	movl	$.L.str.1, %edi
	movl	$3, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	movl	$2, %esi
	xorl	%eax, %eax
	callq	printf
	movq	$-19, %rbx
	.p2align	4, 0x90
.LBB0_372:                              # =>This Inner Loop Header: Depth=1
	movzbl	p+342(%rbx), %eax
	testb	%al, %al
	je	.LBB0_375
# BB#373:                               #   in Loop: Header=BB0_372 Depth=1
	cmpb	$1, %al
	jne	.LBB0_376
# BB#374:                               #   in Loop: Header=BB0_372 Depth=1
	movl	$.L.str.3, %edi
	jmp	.LBB0_377
	.p2align	4, 0x90
.LBB0_375:                              #   in Loop: Header=BB0_372 Depth=1
	movl	$.L.str.2, %edi
	jmp	.LBB0_377
	.p2align	4, 0x90
.LBB0_376:                              #   in Loop: Header=BB0_372 Depth=1
	movl	$.L.str.4, %edi
.LBB0_377:                              #   in Loop: Header=BB0_372 Depth=1
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	jne	.LBB0_372
# BB#378:
	movl	$.L.str.1, %edi
	movl	$2, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	movl	$1, %esi
	xorl	%eax, %eax
	callq	printf
	movq	$-19, %rbx
	.p2align	4, 0x90
.LBB0_379:                              # =>This Inner Loop Header: Depth=1
	movzbl	p+361(%rbx), %eax
	testb	%al, %al
	je	.LBB0_382
# BB#380:                               #   in Loop: Header=BB0_379 Depth=1
	cmpb	$1, %al
	jne	.LBB0_383
# BB#381:                               #   in Loop: Header=BB0_379 Depth=1
	movl	$.L.str.3, %edi
	jmp	.LBB0_384
	.p2align	4, 0x90
.LBB0_382:                              #   in Loop: Header=BB0_379 Depth=1
	movl	$.L.str.2, %edi
	jmp	.LBB0_384
	.p2align	4, 0x90
.LBB0_383:                              #   in Loop: Header=BB0_379 Depth=1
	movl	$.L.str.4, %edi
.LBB0_384:                              #   in Loop: Header=BB0_379 Depth=1
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	jne	.LBB0_379
# BB#385:
	movl	$.L.str.1, %edi
	movl	$1, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	$.Lstr.1, %edi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	puts                    # TAILCALL
.Lfunc_end0:
	.size	showboard, .Lfunc_end0-showboard
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"%2d"
	.size	.L.str.1, 4

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" -"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" O"
	.size	.L.str.3, 3

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	" X"
	.size	.L.str.4, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"16"
	.size	.L.str.6, 3

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	" +"
	.size	.L.str.7, 3

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"     You have captured %d pieces\n"
	.size	.L.str.12, 34

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"10"
	.size	.L.str.13, 3

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"     I have captured %d pieces\n"
	.size	.L.str.14, 32

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	" 4"
	.size	.L.str.15, 3

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"   A B C D E F G H J K L M N O P Q R S T"
	.size	.Lstr, 41

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"   A B C D E F G H J K L M N O P Q R S T\n"
	.size	.Lstr.1, 42

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"     My color:   Black X"
	.size	.Lstr.2, 25

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"     My color:   White O"
	.size	.Lstr.3, 25

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"     Your color: Black X"
	.size	.Lstr.4, 25

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"     Your color: White O"
	.size	.Lstr.5, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
