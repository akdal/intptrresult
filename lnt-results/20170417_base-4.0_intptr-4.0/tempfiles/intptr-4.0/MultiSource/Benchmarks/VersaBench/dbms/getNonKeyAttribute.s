	.text
	.file	"getNonKeyAttribute.bc"
	.globl	getNonKeyAttribute
	.p2align	4, 0x90
	.type	getNonKeyAttribute,@function
getNonKeyAttribute:                     # @getNonKeyAttribute
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	callq	getString
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_1
# BB#2:
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.LBB0_3
# BB#4:
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	xorl	%eax, %eax
	jmp	.LBB0_5
.LBB0_1:
	movl	$getNonKeyAttribute.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movq	$0, (%r14)
	movl	$2, %eax
	jmp	.LBB0_5
.LBB0_3:
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getNonKeyAttribute.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$1, %eax
.LBB0_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	getNonKeyAttribute, .Lfunc_end0-getNonKeyAttribute
	.cfi_endproc

	.type	getNonKeyAttribute.name,@object # @getNonKeyAttribute.name
	.data
	.p2align	4
getNonKeyAttribute.name:
	.asciz	"getNonKeyAttribute"
	.size	getNonKeyAttribute.name, 19

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"allocation failure"
	.size	.L.str, 19


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
