	.text
	.file	"mandel.bc"
	.globl	emit
	.p2align	4, 0x90
	.type	emit,@function
emit:                                   # @emit
	.cfi_startproc
# BB#0:
	addsd	accum(%rip), %xmm0
	movsd	accum+8(%rip), %xmm2    # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	movsd	%xmm0, accum(%rip)
	movsd	%xmm2, accum+8(%rip)
	retq
.Lfunc_end0:
	.size	emit, .Lfunc_end0-emit
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4616189618054758400     # double 4
.LCPI1_1:
	.quad	4662219572839972864     # double 5000
.LCPI1_2:
	.quad	-4611686018427387904    # double -2
.LCPI1_3:
	.quad	-4607182418800017408    # double -4
.LCPI1_4:
	.quad	4611686018427387904     # double 2
.LCPI1_5:
	.quad	4607182418800017408     # double 1
	.text
	.globl	mandel
	.p2align	4, 0x90
	.type	mandel,@function
mandel:                                 # @mandel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$64, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 96
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	xorl	%r14d, %r14d
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_2 Depth 2
                                        #       Child Loop BB1_3 Depth 3
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	mulsd	.LCPI1_0(%rip), %xmm0
	divsd	.LCPI1_1(%rip), %xmm0
	addsd	.LCPI1_2(%rip), %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	xorpd	%xmm0, %xmm0
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_2:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_3 Depth 3
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	mulsd	.LCPI1_3(%rip), %xmm0
	divsd	.LCPI1_1(%rip), %xmm0
	addsd	.LCPI1_2(%rip), %xmm0
	xorpd	%xmm3, %xmm3
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	mulsd	%xmm3, %xmm0
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	movl	$1, %ebx
	xorpd	%xmm4, %xmm4
	jmp	.LBB1_3
.LBB1_5:                                #   in Loop: Header=BB1_3 Depth=3
	movapd	%xmm3, %xmm0
	movapd	%xmm4, %xmm1
	movapd	%xmm3, %xmm2
	movapd	%xmm4, %xmm3
	callq	__muldc3
	jmp	.LBB1_6
	.p2align	4, 0x90
.LBB1_3:                                #   Parent Loop BB1_1 Depth=1
                                        #     Parent Loop BB1_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movapd	%xmm3, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm4, %xmm2
	mulsd	%xmm2, %xmm2
	movapd	%xmm4, %xmm1
	mulsd	%xmm3, %xmm1
	subsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm0
	jnp	.LBB1_6
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=3
	ucomisd	%xmm1, %xmm1
	jp	.LBB1_5
.LBB1_6:                                #   in Loop: Header=BB1_3 Depth=3
	addsd	32(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	addsd	40(%rsp), %xmm1         # 8-byte Folded Reload
	movsd	%xmm1, 48(%rsp)         # 8-byte Spill
	callq	hypot
	movsd	48(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	56(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	ucomisd	.LCPI1_4(%rip), %xmm0
	jae	.LBB1_8
# BB#7:                                 #   in Loop: Header=BB1_3 Depth=3
	cmpl	$10, %ebx
	leal	1(%rbx), %eax
	movl	%eax, %ebx
	jl	.LBB1_3
.LBB1_8:                                #   in Loop: Header=BB1_2 Depth=2
	addsd	accum(%rip), %xmm3
	movsd	accum+8(%rip), %xmm0    # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm4
	movsd	%xmm3, accum(%rip)
	movsd	%xmm4, accum+8(%rip)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI1_5(%rip), %xmm0
	incl	%ebp
	cmpl	$5000, %ebp             # imm = 0x1388
	jne	.LBB1_2
# BB#9:                                 #   in Loop: Header=BB1_1 Depth=1
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI1_5(%rip), %xmm0
	incl	%r14d
	cmpl	$5000, %r14d            # imm = 0x1388
	jne	.LBB1_1
# BB#10:
	addq	$64, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	mandel, .Lfunc_end1-mandel
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 16
	callq	mandel
	cvttsd2si	accum(%rip), %esi
	movsd	accum+8(%rip), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.type	accum,@object           # @accum
	.comm	accum,16,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d\n"
	.size	.L.str, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
