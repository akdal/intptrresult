	.text
	.file	"load.bc"
	.globl	init_load
	.p2align	4, 0x90
	.type	init_load,@function
init_load:                              # @init_load
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	xorl	%edi, %edi
	callq	clear_func
	movl	$0, load_adr(%rip)
	movl	$0, load_adr+4(%rip)
	movb	$0, load_str(%rip)
	movb	$0, load_const(%rip)
	popq	%rax
	retq
.Lfunc_end0:
	.size	init_load, .Lfunc_end0-init_load
	.cfi_endproc

	.globl	addbyte
	.p2align	4, 0x90
	.type	addbyte,@function
addbyte:                                # @addbyte
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 48
.Lcfi6:
	.cfi_offset %rbx, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	cmpl	$0, had_error(%rip)
	jne	.LBB1_5
# BB#1:
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jl	.LBB1_2
# BB#6:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	yyerror                 # TAILCALL
.LBB1_2:
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebx, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebx
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %r14
	movq	functions(%rip), %rdx
	addq	%r14, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB1_4
# BB#3:
	leaq	8(%rdx,%rcx,8), %r15
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r15)
.LBB1_4:
	movslq	%ebx, %rcx
	movb	%bpl, (%rax,%rcx)
	movq	functions(%rip), %rax
	incl	136(%rax,%r14)
.LBB1_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	addbyte, .Lfunc_end1-addbyte
	.cfi_endproc

	.globl	def_label
	.p2align	4, 0x90
	.type	def_label,@function
def_label:                              # @def_label
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r12, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	%r14, %rbx
	shrq	$6, %rbx
	movq	%r14, %r12
	sarq	$63, %r12
	shrq	$58, %r12
	addq	%r14, %r12
	andq	$-64, %r12
	movq	functions(%rip), %rax
	movslq	load_adr(%rip), %rcx
	imulq	$168, %rcx, %rcx
	movq	144(%rax,%rcx), %r15
	testq	%r15, %r15
	jne	.LBB2_2
# BB#1:
	leaq	144(%rax,%rcx), %r15
	movl	$520, %edi              # imm = 0x208
	callq	malloc
	movq	%rax, (%r15)
	movq	$0, 512(%rax)
	movq	(%r15), %r15
.LBB2_2:
	subq	%r12, %r14
	testl	%ebx, %ebx
	jle	.LBB2_3
# BB#4:                                 # %.lr.ph.preheader
	incl	%ebx
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	512(%r15), %rax
	testq	%rax, %rax
	jne	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	movl	$520, %edi              # imm = 0x208
	callq	malloc
	movq	%rax, 512(%r15)
	movq	$0, 512(%rax)
	movq	512(%r15), %rax
.LBB2_7:                                #   in Loop: Header=BB2_5 Depth=1
	decl	%ebx
	cmpl	$1, %ebx
	movq	%rax, %r15
	jg	.LBB2_5
	jmp	.LBB2_8
.LBB2_3:
	movq	%r15, %rax
.LBB2_8:                                # %._crit_edge
	movslq	load_adr+4(%rip), %rcx
	movq	%rcx, (%rax,%r14,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	def_label, .Lfunc_end2-def_label
	.cfi_endproc

	.globl	long_val
	.p2align	4, 0x90
	.type	long_val,@function
long_val:                               # @long_val
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 48
.Lcfi24:
	.cfi_offset %rbx, -48
.Lcfi25:
	.cfi_offset %r12, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	(%r14), %rbx
	movb	(%rbx), %bpl
	xorl	%r15d, %r15d
	cmpb	$45, %bpl
	jne	.LBB3_1
# BB#2:
	leaq	1(%rbx), %rax
	movq	%rax, (%r14)
	movb	1(%rbx), %bpl
	movb	$1, %r12b
	movq	%rax, %rbx
	jmp	.LBB3_3
.LBB3_1:
	xorl	%r12d, %r12d
.LBB3_3:                                # %.preheader
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movsbq	%bpl, %rdx
	testb	$8, 1(%rcx,%rdx,2)
	je	.LBB3_6
# BB#4:                                 # %.lr.ph.preheader
	incq	%rbx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	(%r15,%r15,4), %ecx
	movq	%rbx, (%r14)
	movsbl	-1(%rbx), %edx
	leal	-48(%rdx,%rcx,2), %r15d
	movq	(%rax), %rcx
	movsbq	(%rbx), %rdx
	incq	%rbx
	testb	$8, 1(%rcx,%rdx,2)
	jne	.LBB3_5
.LBB3_6:                                # %._crit_edge
	movl	%r15d, %eax
	negl	%eax
	testb	%r12b, %r12b
	cmovel	%r15d, %eax
	cltq
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	long_val, .Lfunc_end3-long_val
	.cfi_endproc

	.globl	load_code
	.p2align	4, 0x90
	.type	load_code,@function
load_code:                              # @load_code
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 64
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movb	(%r13), %r12b
	testb	%r12b, %r12b
	je	.LBB4_185
# BB#1:
	movl	had_error(%rip), %eax
	testl	%eax, %eax
	jne	.LBB4_185
# BB#2:                                 # %.lr.ph154.preheader
                                        # implicit-def: %RAX
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph154
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_37 Depth 2
                                        #     Child Loop BB4_43 Depth 2
                                        #     Child Loop BB4_77 Depth 2
                                        #     Child Loop BB4_79 Depth 2
                                        #       Child Loop BB4_86 Depth 3
                                        #     Child Loop BB4_89 Depth 2
                                        #       Child Loop BB4_98 Depth 3
                                        #     Child Loop BB4_110 Depth 2
                                        #     Child Loop BB4_126 Depth 2
                                        #     Child Loop BB4_57 Depth 2
                                        #     Child Loop BB4_163 Depth 2
	cmpb	$0, load_str(%rip)
	je	.LBB4_12
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	cmpb	$34, %r12b
	jne	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_3 Depth=1
	movb	$0, load_str(%rip)
	movb	(%r13), %r12b
.LBB4_6:                                #   in Loop: Header=BB4_3 Depth=1
	incq	%r13
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jl	.LBB4_8
.LBB4_7:                                #   in Loop: Header=BB4_3 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	yyerror
	jmp	.LBB4_183
	.p2align	4, 0x90
.LBB4_12:                               #   in Loop: Header=BB4_3 Depth=1
	cmpb	$0, load_const(%rip)
	je	.LBB4_29
# BB#13:                                #   in Loop: Header=BB4_3 Depth=1
	cmpb	$58, %r12b
	je	.LBB4_19
# BB#14:                                #   in Loop: Header=BB4_3 Depth=1
	cmpb	$46, %r12b
	je	.LBB4_23
# BB#15:                                #   in Loop: Header=BB4_3 Depth=1
	cmpb	$10, %r12b
	je	.LBB4_182
# BB#16:                                #   in Loop: Header=BB4_3 Depth=1
	incq	%r13
	cmpb	$65, %r12b
	jl	.LBB4_27
# BB#17:                                #   in Loop: Header=BB4_3 Depth=1
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jge	.LBB4_7
# BB#18:                                #   in Loop: Header=BB4_3 Depth=1
	addb	$-55, %r12b
	jmp	.LBB4_8
.LBB4_29:                               #   in Loop: Header=BB4_3 Depth=1
	movsbl	%r12b, %eax
	addl	$-10, %eax
	cmpl	$105, %eax
	ja	.LBB4_177
# BB#30:                                #   in Loop: Header=BB4_3 Depth=1
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_153:                              #   in Loop: Header=BB4_3 Depth=1
	leaq	1(%r13), %r15
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jl	.LBB4_155
# BB#154:                               #   in Loop: Header=BB4_3 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	yyerror
	jmp	.LBB4_158
.LBB4_19:                               #   in Loop: Header=BB4_3 Depth=1
	movb	$0, load_const(%rip)
	movb	(%r13), %r15b
	incq	%r13
	movl	load_adr+4(%rip), %ebp
	movl	%ebp, %eax
	sarl	$10, %eax
	leal	1(%rbp), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jge	.LBB4_7
# BB#20:                                #   in Loop: Header=BB4_3 Depth=1
	movl	%ebp, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebp, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebp
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %rbx
	movq	functions(%rip), %rdx
	addq	%rbx, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_22
# BB#21:                                #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r12
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r12)
.LBB4_22:                               #   in Loop: Header=BB4_3 Depth=1
	movslq	%ebp, %rcx
	movb	%r15b, (%rax,%rcx)
	movq	functions(%rip), %rax
	incl	136(%rax,%rbx)
	jmp	.LBB4_183
.LBB4_23:                               #   in Loop: Header=BB4_3 Depth=1
	incq	%r13
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jge	.LBB4_7
# BB#24:                                #   in Loop: Header=BB4_3 Depth=1
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebx, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebx
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %rbp
	movq	functions(%rip), %rdx
	addq	%rbp, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_26
# BB#25:                                #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r15
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r15)
.LBB4_26:                               #   in Loop: Header=BB4_3 Depth=1
	movslq	%ebx, %rcx
	movb	$46, (%rax,%rcx)
	jmp	.LBB4_11
.LBB4_27:                               #   in Loop: Header=BB4_3 Depth=1
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jge	.LBB4_7
# BB#28:                                #   in Loop: Header=BB4_3 Depth=1
	addb	$-48, %r12b
	.p2align	4, 0x90
.LBB4_8:                                #   in Loop: Header=BB4_3 Depth=1
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebx, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebx
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %rbp
	movq	functions(%rip), %rdx
	addq	%rbp, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_10
# BB#9:                                 #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r15
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r15)
.LBB4_10:                               #   in Loop: Header=BB4_3 Depth=1
	movslq	%ebx, %rcx
	movb	%r12b, (%rax,%rcx)
.LBB4_11:                               # %addbyte.exit.backedge
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	functions(%rip), %rax
	incl	136(%rax,%rbp)
	jmp	.LBB4_183
.LBB4_155:                              #   in Loop: Header=BB4_3 Depth=1
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebx, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebx
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %rbp
	movq	functions(%rip), %rdx
	addq	%rbp, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_157
# BB#156:                               #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r14
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r14)
.LBB4_157:                              #   in Loop: Header=BB4_3 Depth=1
	movslq	%ebx, %rcx
	movb	%r12b, (%rax,%rcx)
	movq	functions(%rip), %rax
	incl	136(%rax,%rbp)
.LBB4_158:                              # %addbyte.exit93
                                        #   in Loop: Header=BB4_3 Depth=1
	movb	(%r15), %bl
	xorl	%ebp, %ebp
	cmpb	$45, %bl
	movl	$0, %r12d
	jne	.LBB4_160
# BB#159:                               #   in Loop: Header=BB4_3 Depth=1
	movb	2(%r13), %bl
	addq	$2, %r13
	movb	$1, %r12b
	movq	%r13, %r15
.LBB4_160:                              # %.preheader.i96
                                        #   in Loop: Header=BB4_3 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	%bl, %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB4_162
# BB#161:                               #   in Loop: Header=BB4_3 Depth=1
	movq	%r15, %r13
	jmp	.LBB4_164
.LBB4_162:                              # %.lr.ph.i98.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	xorl	%ebp, %ebp
	movq	%r15, %r13
	.p2align	4, 0x90
.LBB4_163:                              # %.lr.ph.i98
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rbp,%rbp,4), %ecx
	movsbl	%bl, %edx
	leal	-48(%rdx,%rcx,2), %ebp
	movsbq	1(%r13), %rbx
	incq	%r13
	testb	$8, 1(%rax,%rbx,2)
	jne	.LBB4_163
.LBB4_164:                              # %long_val.exit102
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	%ebp, %r15d
	negl	%r15d
	testb	%r12b, %r12b
	cmovel	%ebp, %r15d
	cmpl	$127, %r15d
	jg	.LBB4_167
# BB#165:                               #   in Loop: Header=BB4_3 Depth=1
	cmpl	$0, had_error(%rip)
	jne	.LBB4_182
.LBB4_166:                              #   in Loop: Header=BB4_3 Depth=1
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jge	.LBB4_67
.LBB4_69:                               #   in Loop: Header=BB4_3 Depth=1
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebx, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebx
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %rbp
	movq	functions(%rip), %rdx
	addq	%rbp, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_71
# BB#70:                                #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r14
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r14)
.LBB4_71:                               #   in Loop: Header=BB4_3 Depth=1
	movslq	%ebx, %rcx
	movb	%r15b, (%rax,%rcx)
	jmp	.LBB4_181
.LBB4_167:                              #   in Loop: Header=BB4_3 Depth=1
	cmpl	$0, had_error(%rip)
	jne	.LBB4_182
# BB#168:                               #   in Loop: Header=BB4_3 Depth=1
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jl	.LBB4_170
# BB#169:                               #   in Loop: Header=BB4_3 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	yyerror
	cmpl	$0, had_error(%rip)
	jne	.LBB4_182
	jmp	.LBB4_166
.LBB4_47:                               #   in Loop: Header=BB4_3 Depth=1
	leaq	1(%r13), %r15
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jl	.LBB4_49
# BB#48:                                #   in Loop: Header=BB4_3 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	yyerror
	jmp	.LBB4_52
.LBB4_49:                               #   in Loop: Header=BB4_3 Depth=1
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebx, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebx
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %rbp
	movq	functions(%rip), %rdx
	addq	%rbp, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_51
# BB#50:                                #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r14
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r14)
.LBB4_51:                               #   in Loop: Header=BB4_3 Depth=1
	movslq	%ebx, %rcx
	movb	%r12b, (%rax,%rcx)
	movq	functions(%rip), %rax
	incl	136(%rax,%rbp)
.LBB4_52:                               # %addbyte.exit36
                                        #   in Loop: Header=BB4_3 Depth=1
	movb	(%r15), %bl
	xorl	%ebp, %ebp
	cmpb	$45, %bl
	movl	$0, %r12d
	jne	.LBB4_54
# BB#53:                                #   in Loop: Header=BB4_3 Depth=1
	movb	2(%r13), %bl
	addq	$2, %r13
	movb	$1, %r12b
	movq	%r13, %r15
.LBB4_54:                               # %.preheader.i39
                                        #   in Loop: Header=BB4_3 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	%bl, %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB4_56
# BB#55:                                #   in Loop: Header=BB4_3 Depth=1
	movq	%r15, %r13
	jmp	.LBB4_58
.LBB4_56:                               # %.lr.ph.i41.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	xorl	%ebp, %ebp
	movq	%r15, %r13
	.p2align	4, 0x90
.LBB4_57:                               # %.lr.ph.i41
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rbp,%rbp,4), %ecx
	movsbl	%bl, %edx
	leal	-48(%rdx,%rcx,2), %ebp
	movsbq	1(%r13), %rbx
	incq	%r13
	testb	$8, 1(%rax,%rbx,2)
	jne	.LBB4_57
.LBB4_58:                               # %long_val.exit45
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	%ebp, %r15d
	negl	%r15d
	testb	%r12b, %r12b
	cmovel	%ebp, %r15d
	cmpl	$65536, %r15d           # imm = 0x10000
	jge	.LBB4_186
# BB#59:                                #   in Loop: Header=BB4_3 Depth=1
	cmpl	$0, had_error(%rip)
	jne	.LBB4_182
# BB#60:                                #   in Loop: Header=BB4_3 Depth=1
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jl	.LBB4_62
# BB#61:                                #   in Loop: Header=BB4_3 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	yyerror
	cmpl	$0, had_error(%rip)
	jne	.LBB4_182
	jmp	.LBB4_66
.LBB4_177:                              #   in Loop: Header=BB4_3 Depth=1
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jge	.LBB4_67
# BB#178:                               #   in Loop: Header=BB4_3 Depth=1
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebx, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebx
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %rbp
	movq	functions(%rip), %rdx
	addq	%rbp, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_180
# BB#179:                               #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r14
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r14)
.LBB4_180:                              #   in Loop: Header=BB4_3 Depth=1
	movslq	%ebx, %rcx
	movb	%r12b, (%rax,%rcx)
	jmp	.LBB4_181
.LBB4_31:                               #   in Loop: Header=BB4_3 Depth=1
	movb	$1, load_str(%rip)
	jmp	.LBB4_182
.LBB4_173:                              #   in Loop: Header=BB4_3 Depth=1
	movsbl	1(%r13), %eax
	incq	%r13
	cmpl	$114, %eax
	je	.LBB4_176
# BB#174:                               #   in Loop: Header=BB4_3 Depth=1
	cmpl	$105, %eax
	jne	.LBB4_182
# BB#175:                               #   in Loop: Header=BB4_3 Depth=1
	xorl	%edi, %edi
	callq	clear_func
	movl	$0, load_adr(%rip)
	movl	$0, load_adr+4(%rip)
	movb	$0, load_str(%rip)
	movb	$0, load_const(%rip)
	jmp	.LBB4_182
.LBB4_101:                              #   in Loop: Header=BB4_3 Depth=1
	leaq	1(%r13), %r15
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jl	.LBB4_103
# BB#102:                               #   in Loop: Header=BB4_3 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	yyerror
	jmp	.LBB4_106
.LBB4_72:                               #   in Loop: Header=BB4_3 Depth=1
	movb	1(%r13), %r15b
	xorl	%r12d, %r12d
	cmpb	$45, %r15b
	jne	.LBB4_73
# BB#74:                                #   in Loop: Header=BB4_3 Depth=1
	movb	2(%r13), %r15b
	addq	$2, %r13
	movb	$1, %bl
	movq	%r13, %rbp
	jmp	.LBB4_75
.LBB4_148:                              #   in Loop: Header=BB4_3 Depth=1
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jl	.LBB4_150
# BB#149:                               #   in Loop: Header=BB4_3 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	yyerror
	movb	$1, load_const(%rip)
	jmp	.LBB4_182
.LBB4_32:                               #   in Loop: Header=BB4_3 Depth=1
	movb	1(%r13), %bpl
	xorl	%r12d, %r12d
	cmpb	$45, %bpl
	jne	.LBB4_33
# BB#34:                                #   in Loop: Header=BB4_3 Depth=1
	movb	2(%r13), %bpl
	addq	$2, %r13
	movb	$1, %r15b
	jmp	.LBB4_35
.LBB4_100:                              #   in Loop: Header=BB4_3 Depth=1
	movq	functions(%rip), %rax
	movslq	load_adr(%rip), %rcx
	imulq	$168, %rcx, %rcx
	movb	$1, (%rax,%rcx)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, load_adr(%rip)
	jmp	.LBB4_182
.LBB4_138:                              #   in Loop: Header=BB4_3 Depth=1
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jl	.LBB4_140
# BB#139:                               #   in Loop: Header=BB4_3 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	yyerror
	jmp	.LBB4_143
.LBB4_170:                              #   in Loop: Header=BB4_3 Depth=1
	movl	%r15d, %ebp
	shrl	$8, %ebp
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebx, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebx
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %r12
	movq	functions(%rip), %rdx
	addq	%r12, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_172
# BB#171:                               #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r14
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r14)
.LBB4_172:                              #   in Loop: Header=BB4_3 Depth=1
	orb	$-128, %bpl
	movslq	%ebx, %rcx
	movb	%bpl, (%rax,%rcx)
	movq	functions(%rip), %rax
	incl	136(%rax,%r12)
	cmpl	$0, had_error(%rip)
	jne	.LBB4_182
	jmp	.LBB4_166
.LBB4_62:                               #   in Loop: Header=BB4_3 Depth=1
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebx, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebx
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %rbp
	movq	functions(%rip), %rdx
	addq	%rbp, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_64
# BB#63:                                #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r14
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r14)
.LBB4_64:                               #   in Loop: Header=BB4_3 Depth=1
	movslq	%ebx, %rcx
	movb	%r15b, (%rax,%rcx)
	movq	functions(%rip), %rax
	incl	136(%rax,%rbp)
	cmpl	$0, had_error(%rip)
	jne	.LBB4_182
.LBB4_66:                               #   in Loop: Header=BB4_3 Depth=1
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jge	.LBB4_67
# BB#68:                                #   in Loop: Header=BB4_3 Depth=1
	shll	$24, %r15d
	sarl	$31, %r15d
	jmp	.LBB4_69
.LBB4_103:                              #   in Loop: Header=BB4_3 Depth=1
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebx, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebx
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %rbp
	movq	functions(%rip), %rdx
	addq	%rbp, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_105
# BB#104:                               #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r14
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r14)
.LBB4_105:                              #   in Loop: Header=BB4_3 Depth=1
	movslq	%ebx, %rcx
	movb	%r12b, (%rax,%rcx)
	movq	functions(%rip), %rax
	incl	136(%rax,%rbp)
.LBB4_106:                              # %addbyte.exit75
                                        #   in Loop: Header=BB4_3 Depth=1
	movb	(%r15), %bl
	xorl	%ebp, %ebp
	cmpb	$45, %bl
	movl	$0, %r12d
	jne	.LBB4_108
# BB#107:                               #   in Loop: Header=BB4_3 Depth=1
	movb	2(%r13), %bl
	addq	$2, %r13
	movb	$1, %r12b
	movq	%r13, %r15
.LBB4_108:                              # %.preheader.i78
                                        #   in Loop: Header=BB4_3 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	%bl, %rcx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB4_111
# BB#109:                               # %.lr.ph.i80.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_110:                              # %.lr.ph.i80
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rbp,%rbp,4), %ecx
	movsbl	%bl, %edx
	leal	-48(%rdx,%rcx,2), %ebp
	movsbq	1(%r15), %rbx
	incq	%r15
	testb	$8, 1(%rax,%rbx,2)
	jne	.LBB4_110
.LBB4_111:                              # %long_val.exit84
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	%ebp, %ebx
	negl	%ebx
	testb	%r12b, %r12b
	cmovel	%ebp, %ebx
	cmpl	$127, %ebx
	jg	.LBB4_115
# BB#112:                               #   in Loop: Header=BB4_3 Depth=1
	cmpl	$0, had_error(%rip)
	jne	.LBB4_124
.LBB4_113:                              #   in Loop: Header=BB4_3 Depth=1
	movl	load_adr+4(%rip), %ebp
	movl	%ebp, %eax
	sarl	$10, %eax
	leal	1(%rbp), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jl	.LBB4_121
# BB#114:                               #   in Loop: Header=BB4_3 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	yyerror
	jmp	.LBB4_124
.LBB4_73:                               #   in Loop: Header=BB4_3 Depth=1
	incq	%r13
	movq	%r13, %rbp
	xorl	%ebx, %ebx
.LBB4_75:                               # %.preheader.i50
                                        #   in Loop: Header=BB4_3 Depth=1
	callq	__ctype_b_loc
	movq	%rax, %r14
	movq	(%r14), %rax
	movsbq	%r15b, %rcx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB4_78
# BB#76:                                # %.lr.ph.i52.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB4_77:                               # %.lr.ph.i52
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r12,%r12,4), %ecx
	movsbl	%r15b, %edx
	leal	-48(%rdx,%rcx,2), %r12d
	movsbq	1(%rbp), %r15
	incq	%rbp
	testb	$8, 1(%rax,%r15,2)
	jne	.LBB4_77
.LBB4_78:                               # %long_val.exit56
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	%r12d, %r15d
	negl	%r15d
	testb	%bl, %bl
	cmovel	%r12d, %r15d
	movslq	%r15d, %r12
	movsbl	%r12b, %edi
	callq	clear_func
	jmp	.LBB4_79
	.p2align	4, 0x90
.LBB4_87:                               # %long_val.exit65
                                        #   in Loop: Header=BB4_79 Depth=2
	movq	%r13, %rbp
	movl	%edx, %ecx
	negl	%ecx
	testb	%al, %al
	cmovel	%edx, %ecx
	movq	functions(%rip), %rax
	imulq	$168, %r12, %rbx
	movq	152(%rax,%rbx), %rdi
	movsbl	%cl, %esi
	callq	nextarg
	movq	functions(%rip), %rcx
	movq	%rax, 152(%rcx,%rbx)
.LBB4_79:                               # %long_val.exit56
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_86 Depth 3
	leaq	1(%rbp), %r13
	cmpb	$46, (%rbp)
	je	.LBB4_89
# BB#80:                                # %.lr.ph149
                                        #   in Loop: Header=BB4_79 Depth=2
	movb	(%r13), %cl
	cmpb	$45, %cl
	je	.LBB4_83
# BB#81:                                # %.lr.ph149
                                        #   in Loop: Header=BB4_79 Depth=2
	cmpb	$46, %cl
	je	.LBB4_88
# BB#82:                                #   in Loop: Header=BB4_79 Depth=2
	xorl	%eax, %eax
	jmp	.LBB4_84
	.p2align	4, 0x90
.LBB4_83:                               #   in Loop: Header=BB4_79 Depth=2
	movb	2(%rbp), %cl
	addq	$2, %rbp
	movb	$1, %al
	movq	%rbp, %r13
.LBB4_84:                               # %.preheader.i59
                                        #   in Loop: Header=BB4_79 Depth=2
	movq	(%r14), %rsi
	movsbq	%cl, %rdi
	xorl	%edx, %edx
	testb	$8, 1(%rsi,%rdi,2)
	je	.LBB4_87
# BB#85:                                # %.lr.ph.i61.preheader
                                        #   in Loop: Header=BB4_79 Depth=2
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_86:                               # %.lr.ph.i61
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_79 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rdx,%rdx,4), %edx
	movsbl	%cl, %ecx
	leal	-48(%rcx,%rdx,2), %edx
	movsbq	1(%r13), %rcx
	incq	%r13
	testb	$8, 1(%rsi,%rcx,2)
	jne	.LBB4_86
	jmp	.LBB4_87
.LBB4_150:                              #   in Loop: Header=BB4_3 Depth=1
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebx, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebx
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %rbp
	movq	functions(%rip), %rdx
	addq	%rbp, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_152
# BB#151:                               #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r14
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r14)
.LBB4_152:                              #   in Loop: Header=BB4_3 Depth=1
	movslq	%ebx, %rcx
	movb	%r12b, (%rax,%rcx)
	movq	functions(%rip), %rax
	incl	136(%rax,%rbp)
	movb	$1, load_const(%rip)
	jmp	.LBB4_182
.LBB4_33:                               #   in Loop: Header=BB4_3 Depth=1
	incq	%r13
	xorl	%r15d, %r15d
.LBB4_35:                               # %.preheader.i
                                        #   in Loop: Header=BB4_3 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	%bpl, %rcx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB4_38
# BB#36:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB4_37:                               # %.lr.ph.i
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r12,%r12,4), %ecx
	movsbl	%bpl, %edx
	leal	-48(%rdx,%rcx,2), %r12d
	movsbq	1(%r13), %rbp
	incq	%r13
	testb	$8, 1(%rax,%rbp,2)
	jne	.LBB4_37
.LBB4_38:                               # %long_val.exit
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	%r12d, %eax
	negl	%eax
	testb	%r15b, %r15b
	cmovel	%r12d, %eax
	movslq	%eax, %r15
	movq	%r15, %rbp
	shrq	$6, %rbp
	movq	%r15, %r12
	sarq	$63, %r12
	shrq	$58, %r12
	addq	%r15, %r12
	andq	$-64, %r12
	movq	functions(%rip), %rax
	movslq	load_adr(%rip), %rcx
	imulq	$168, %rcx, %rcx
	movq	144(%rax,%rcx), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_40
# BB#39:                                #   in Loop: Header=BB4_3 Depth=1
	leaq	144(%rax,%rcx), %rbx
	movl	$520, %edi              # imm = 0x208
	callq	malloc
	movq	%rax, (%rbx)
	movq	$0, 512(%rax)
	movq	(%rbx), %rbx
.LBB4_40:                               #   in Loop: Header=BB4_3 Depth=1
	subq	%r12, %r15
	testl	%ebp, %ebp
	jle	.LBB4_41
# BB#42:                                # %.lr.ph.i35.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	incl	%ebp
	.p2align	4, 0x90
.LBB4_43:                               # %.lr.ph.i35
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	512(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB4_45
# BB#44:                                #   in Loop: Header=BB4_43 Depth=2
	movl	$520, %edi              # imm = 0x208
	callq	malloc
	movq	%rax, 512(%rbx)
	movq	$0, 512(%rax)
	movq	512(%rbx), %rax
.LBB4_45:                               #   in Loop: Header=BB4_43 Depth=2
	decl	%ebp
	cmpl	$1, %ebp
	movq	%rax, %rbx
	jg	.LBB4_43
	jmp	.LBB4_46
.LBB4_140:                              #   in Loop: Header=BB4_3 Depth=1
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebx, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebx
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %rbp
	movq	functions(%rip), %rdx
	addq	%rbp, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_142
# BB#141:                               #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r14
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r14)
.LBB4_142:                              #   in Loop: Header=BB4_3 Depth=1
	movslq	%ebx, %rcx
	movb	%r12b, (%rax,%rcx)
	movq	functions(%rip), %rax
	incl	136(%rax,%rbp)
.LBB4_143:                              # %addbyte.exit90
                                        #   in Loop: Header=BB4_3 Depth=1
	incq	%r13
	cmpl	$0, had_error(%rip)
	jne	.LBB4_182
# BB#144:                               #   in Loop: Header=BB4_3 Depth=1
	movb	(%r13), %r15b
	movl	load_adr+4(%rip), %ebp
	movl	%ebp, %eax
	sarl	$10, %eax
	leal	1(%rbp), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jge	.LBB4_67
# BB#145:                               #   in Loop: Header=BB4_3 Depth=1
	movl	%ebp, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebp, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebp
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %rbx
	movq	functions(%rip), %rdx
	addq	%rbx, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_147
# BB#146:                               #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r14
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r14)
.LBB4_147:                              #   in Loop: Header=BB4_3 Depth=1
	movslq	%ebp, %rcx
	movb	%r15b, (%rax,%rcx)
	movq	functions(%rip), %rax
	incl	136(%rax,%rbx)
	jmp	.LBB4_182
.LBB4_115:                              #   in Loop: Header=BB4_3 Depth=1
	cmpl	$0, had_error(%rip)
	jne	.LBB4_124
# BB#116:                               #   in Loop: Header=BB4_3 Depth=1
	movl	load_adr+4(%rip), %ebp
	movl	%ebp, %eax
	sarl	$10, %eax
	leal	1(%rbp), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jl	.LBB4_118
# BB#117:                               #   in Loop: Header=BB4_3 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	yyerror
	cmpl	$0, had_error(%rip)
	jne	.LBB4_124
	jmp	.LBB4_113
.LBB4_41:                               #   in Loop: Header=BB4_3 Depth=1
	movq	%rbx, %rax
.LBB4_46:                               # %def_label.exit
                                        #   in Loop: Header=BB4_3 Depth=1
	movslq	load_adr+4(%rip), %rcx
	movq	%rcx, (%rax,%r15,8)
	jmp	.LBB4_182
.LBB4_176:                              #   in Loop: Header=BB4_3 Depth=1
	callq	execute
	jmp	.LBB4_182
.LBB4_88:                               #   in Loop: Header=BB4_3 Depth=1
	addq	$2, %rbp
	movq	%rbp, %r13
	jmp	.LBB4_89
	.p2align	4, 0x90
.LBB4_99:                               # %long_val.exit74
                                        #   in Loop: Header=BB4_89 Depth=2
	movl	%ecx, %eax
	negl	%eax
	testb	%dl, %dl
	cmovel	%ecx, %eax
	movq	functions(%rip), %rcx
	imulq	$168, %r12, %rbx
	movq	160(%rcx,%rbx), %rdi
	movsbl	%al, %esi
	callq	nextarg
	movq	functions(%rip), %rcx
	movq	%rax, 160(%rcx,%rbx)
.LBB4_89:                               # %.preheader
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_98 Depth 3
	movb	(%r13), %al
	cmpb	$44, %al
	je	.LBB4_92
# BB#90:                                # %.preheader
                                        #   in Loop: Header=BB4_89 Depth=2
	cmpb	$91, %al
	jne	.LBB4_93
	jmp	.LBB4_91
	.p2align	4, 0x90
.LBB4_92:                               #   in Loop: Header=BB4_89 Depth=2
	movb	1(%r13), %al
	incq	%r13
.LBB4_93:                               #   in Loop: Header=BB4_89 Depth=2
	xorl	%ecx, %ecx
	cmpb	$45, %al
	jne	.LBB4_94
# BB#95:                                #   in Loop: Header=BB4_89 Depth=2
	movb	1(%r13), %al
	incq	%r13
	movb	$1, %dl
	jmp	.LBB4_96
	.p2align	4, 0x90
.LBB4_94:                               #   in Loop: Header=BB4_89 Depth=2
	xorl	%edx, %edx
.LBB4_96:                               # %.preheader.i68
                                        #   in Loop: Header=BB4_89 Depth=2
	movq	(%r14), %rsi
	movsbq	%al, %rdi
	testb	$8, 1(%rsi,%rdi,2)
	je	.LBB4_99
# BB#97:                                # %.lr.ph.i70.preheader
                                        #   in Loop: Header=BB4_89 Depth=2
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_98:                               # %.lr.ph.i70
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_89 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rcx,%rcx,4), %ecx
	movsbl	%al, %eax
	leal	-48(%rax,%rcx,2), %ecx
	movsbq	1(%r13), %rax
	incq	%r13
	testb	$8, 1(%rsi,%rax,2)
	jne	.LBB4_98
	jmp	.LBB4_99
.LBB4_91:                               #   in Loop: Header=BB4_3 Depth=1
	movq	load_adr(%rip), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	%r15d, load_adr(%rip)
	movl	$0, load_adr+4(%rip)
	jmp	.LBB4_182
.LBB4_121:                              #   in Loop: Header=BB4_3 Depth=1
	movl	%ebp, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebp, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebp
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %r12
	movq	functions(%rip), %rdx
	addq	%r12, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_123
# BB#122:                               #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r14
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r14)
.LBB4_123:                              #   in Loop: Header=BB4_3 Depth=1
	movslq	%ebp, %rcx
	movb	%bl, (%rax,%rcx)
	movq	functions(%rip), %rax
	incl	136(%rax,%r12)
	jmp	.LBB4_124
.LBB4_118:                              #   in Loop: Header=BB4_3 Depth=1
	movl	%ebx, %r12d
	shrl	$8, %r12d
	movl	%ebp, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebp, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebp
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %r13
	movq	functions(%rip), %rdx
	addq	%r13, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_120
# BB#119:                               #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r14
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r14)
.LBB4_120:                              #   in Loop: Header=BB4_3 Depth=1
	orb	$-128, %r12b
	movslq	%ebp, %rcx
	movb	%r12b, (%rax,%rcx)
	movq	functions(%rip), %rax
	incl	136(%rax,%r13)
	cmpl	$0, had_error(%rip)
	je	.LBB4_113
.LBB4_124:                              # %addbyte.exit85
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpb	$44, (%r15)
	leaq	1(%r15), %r13
	cmovneq	%r15, %r13
	movb	(%r13), %bpl
	cmpb	$58, %bpl
	jne	.LBB4_126
	jmp	.LBB4_128
	.p2align	4, 0x90
.LBB4_127:                              # %addbyte.exit88.backedge
                                        #   in Loop: Header=BB4_126 Depth=2
	movzbl	1(%r13), %ebp
	incq	%r13
	cmpb	$58, %bpl
	je	.LBB4_128
.LBB4_126:                              # %.lr.ph
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, had_error(%rip)
	jne	.LBB4_127
# BB#133:                               #   in Loop: Header=BB4_126 Depth=2
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jl	.LBB4_135
# BB#134:                               #   in Loop: Header=BB4_126 Depth=2
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	yyerror
	jmp	.LBB4_127
.LBB4_135:                              #   in Loop: Header=BB4_126 Depth=2
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebx, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebx
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %r15
	movq	functions(%rip), %rdx
	addq	%r15, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_137
# BB#136:                               #   in Loop: Header=BB4_126 Depth=2
	leaq	8(%rdx,%rcx,8), %r14
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r14)
.LBB4_137:                              #   in Loop: Header=BB4_126 Depth=2
	movslq	%ebx, %rcx
	movb	%bpl, (%rax,%rcx)
	movq	functions(%rip), %rax
	incl	136(%rax,%r15)
	jmp	.LBB4_127
.LBB4_128:                              # %addbyte.exit88._crit_edge
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpl	$0, had_error(%rip)
	jne	.LBB4_182
# BB#129:                               #   in Loop: Header=BB4_3 Depth=1
	movl	load_adr+4(%rip), %ebx
	movl	%ebx, %eax
	sarl	$10, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, load_adr+4(%rip)
	cmpl	$16, %eax
	jl	.LBB4_130
.LBB4_67:                               #   in Loop: Header=BB4_3 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	yyerror
	jmp	.LBB4_182
.LBB4_130:                              #   in Loop: Header=BB4_3 Depth=1
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%ebx, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %ebx
	movslq	load_adr(%rip), %rdx
	movslq	%eax, %rcx
	imulq	$168, %rdx, %rbp
	movq	functions(%rip), %rdx
	addq	%rbp, %rdx
	movq	8(%rdx,%rcx,8), %rax
	testq	%rax, %rax
	jne	.LBB4_132
# BB#131:                               #   in Loop: Header=BB4_3 Depth=1
	leaq	8(%rdx,%rcx,8), %r14
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	movq	%rax, (%r14)
.LBB4_132:                              #   in Loop: Header=BB4_3 Depth=1
	movslq	%ebx, %rcx
	movb	$58, (%rax,%rcx)
.LBB4_181:                              # %addbyte.exit47
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	functions(%rip), %rax
	incl	136(%rax,%rbp)
.LBB4_182:                              # %addbyte.exit47
                                        #   in Loop: Header=BB4_3 Depth=1
	incq	%r13
	.p2align	4, 0x90
.LBB4_183:                              # %addbyte.exit.backedge
                                        #   in Loop: Header=BB4_3 Depth=1
	movb	(%r13), %r12b
	testb	%r12b, %r12b
	je	.LBB4_185
# BB#184:                               # %addbyte.exit.backedge
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	had_error(%rip), %eax
	testl	%eax, %eax
	je	.LBB4_3
.LBB4_185:                              # %addbyte.exit._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_186:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	load_code, .Lfunc_end4-load_code
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_182
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_31
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_173
	.quad	.LBB4_153
	.quad	.LBB4_47
	.quad	.LBB4_101
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_72
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_47
	.quad	.LBB4_148
	.quad	.LBB4_153
	.quad	.LBB4_153
	.quad	.LBB4_32
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_153
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_47
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_100
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_138
	.quad	.LBB4_153
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_153
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_153
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_177
	.quad	.LBB4_153

	.type	load_adr,@object        # @load_adr
	.comm	load_adr,8,4
	.type	load_str,@object        # @load_str
	.comm	load_str,1,1
	.type	load_const,@object      # @load_const
	.comm	load_const,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Function too big."
	.size	.L.str, 18

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Program too big.\n"
	.size	.L.str.1, 18


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
