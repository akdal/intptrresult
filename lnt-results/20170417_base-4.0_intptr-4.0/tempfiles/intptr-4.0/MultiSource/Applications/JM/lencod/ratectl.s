	.text
	.file	"ratectl.bc"
	.globl	update_rc
	.p2align	4, 0x90
	.type	update_rc,@function
update_rc:                              # @update_rc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	callq	calc_MAD
	movq	generic_RC(%rip), %rdx
	movq	64(%rdx), %rsi
	movq	img(%rip), %rcx
	movslq	12(%rcx), %rdi
	movl	%eax, (%rsi,%rdi,4)
	movq	input(%rip), %rax
	movl	5128(%rax), %edi
	cmpl	15352(%rcx), %edi
	jae	.LBB0_6
# BB#1:
	movslq	12(%rcx), %rdi
	movslq	(%rsi,%rdi,4), %rsi
	addq	%rsi, 56(%rdx)
	cmpl	$0, 364(%rbx)
	sete	%sil
	movzwl	%bp, %edi
	cmpl	$10, %edi
	setne	%dl
	cmpl	$14, %edi
	je	.LBB0_3
# BB#2:
	andb	%sil, %dl
	movl	$1, %edx
	je	.LBB0_4
.LBB0_3:
	movl	$0, 4(%rbx)
	movl	496(%rbx), %edx
	movl	%edx, 8(%rbx)
	movl	%edx, 36(%rcx)
	xorl	%edx, %edx
.LBB0_4:
	movl	%edx, 504(%rbx)
	cmpl	$0, 4708(%rax)
	je	.LBB0_6
# BB#5:
	movl	4(%rbx), %eax
	movq	rdopt(%rip), %rsi
	movl	%eax, 1740(%rsi)
	movl	8(%rbx), %edi
	movl	%edi, 1728(%rsi)
	movl	%edx, 1744(%rsi)
	movslq	424(%rbx), %rdx
	movslq	15412(%rcx), %rsi
	shlq	$2, %rsi
	movl	%eax, delta_qp_mbaff(%rsi,%rdx,8)
	movl	8(%rbx), %eax
	movslq	424(%rbx), %rdx
	movslq	15412(%rcx), %rcx
	shlq	$2, %rcx
	movl	%eax, qp_mbaff(%rcx,%rdx,8)
.LBB0_6:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	set_chroma_qp           # TAILCALL
.Lfunc_end0:
	.size	update_rc, .Lfunc_end0-update_rc
	.cfi_endproc

	.globl	calc_MAD
	.p2align	4, 0x90
	.type	calc_MAD,@function
calc_MAD:                               # @calc_MAD
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	pxor	%xmm0, %xmm0
	movq	$-1024, %rax            # imm = 0xFC00
	.p2align	4, 0x90
.LBB1_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	diffy+1216(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1088(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movd	diffy+1152(%rax), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movd	diffy+1024(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	punpckldq	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm1
	pxor	%xmm2, %xmm1
	paddd	%xmm0, %xmm1
	movd	diffy+1220(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movd	diffy+1092(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movd	diffy+1156(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movd	diffy+1028(%rax), %xmm3 # xmm3 = mem[0],zero,zero,zero
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movdqa	%xmm3, %xmm0
	psrad	$31, %xmm0
	paddd	%xmm0, %xmm3
	pxor	%xmm0, %xmm3
	movd	diffy+1224(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movd	diffy+1096(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movd	diffy+1160(%rax), %xmm4 # xmm4 = mem[0],zero,zero,zero
	movd	diffy+1032(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpckldq	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	punpckldq	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	movdqa	%xmm0, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm0
	pxor	%xmm2, %xmm0
	paddd	%xmm3, %xmm0
	paddd	%xmm1, %xmm0
	movd	diffy+1228(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1100(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movd	diffy+1164(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1036(%rax), %xmm3 # xmm3 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movdqa	%xmm3, %xmm1
	psrad	$31, %xmm1
	paddd	%xmm1, %xmm3
	pxor	%xmm1, %xmm3
	movd	diffy+1232(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1104(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movd	diffy+1168(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1040(%rax), %xmm4 # xmm4 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	movdqa	%xmm4, %xmm1
	psrad	$31, %xmm1
	paddd	%xmm1, %xmm4
	pxor	%xmm1, %xmm4
	paddd	%xmm3, %xmm4
	movd	diffy+1236(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1108(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movd	diffy+1172(%rax), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movd	diffy+1044(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	punpckldq	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm1
	pxor	%xmm2, %xmm1
	paddd	%xmm4, %xmm1
	paddd	%xmm0, %xmm1
	movd	diffy+1240(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movd	diffy+1112(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movd	diffy+1176(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movd	diffy+1048(%rax), %xmm3 # xmm3 = mem[0],zero,zero,zero
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movdqa	%xmm3, %xmm0
	psrad	$31, %xmm0
	paddd	%xmm0, %xmm3
	pxor	%xmm0, %xmm3
	movd	diffy+1244(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movd	diffy+1116(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movd	diffy+1180(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movd	diffy+1052(%rax), %xmm4 # xmm4 = mem[0],zero,zero,zero
	punpckldq	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	movdqa	%xmm4, %xmm0
	psrad	$31, %xmm0
	paddd	%xmm0, %xmm4
	pxor	%xmm0, %xmm4
	paddd	%xmm3, %xmm4
	movd	diffy+1248(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movd	diffy+1120(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movd	diffy+1184(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movd	diffy+1056(%rax), %xmm3 # xmm3 = mem[0],zero,zero,zero
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movdqa	%xmm3, %xmm0
	psrad	$31, %xmm0
	paddd	%xmm0, %xmm3
	pxor	%xmm0, %xmm3
	paddd	%xmm4, %xmm3
	movd	diffy+1252(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movd	diffy+1124(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movd	diffy+1188(%rax), %xmm4 # xmm4 = mem[0],zero,zero,zero
	movd	diffy+1060(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpckldq	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	punpckldq	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	movdqa	%xmm0, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm0
	pxor	%xmm2, %xmm0
	paddd	%xmm3, %xmm0
	paddd	%xmm1, %xmm0
	movd	diffy+1256(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1128(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movd	diffy+1192(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1064(%rax), %xmm3 # xmm3 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movdqa	%xmm3, %xmm1
	psrad	$31, %xmm1
	paddd	%xmm1, %xmm3
	pxor	%xmm1, %xmm3
	movd	diffy+1260(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1132(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movd	diffy+1196(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1068(%rax), %xmm4 # xmm4 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	movdqa	%xmm4, %xmm1
	psrad	$31, %xmm1
	paddd	%xmm1, %xmm4
	pxor	%xmm1, %xmm4
	paddd	%xmm3, %xmm4
	movd	diffy+1264(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1136(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movd	diffy+1200(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1072(%rax), %xmm3 # xmm3 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movdqa	%xmm3, %xmm1
	psrad	$31, %xmm1
	paddd	%xmm1, %xmm3
	pxor	%xmm1, %xmm3
	paddd	%xmm4, %xmm3
	movd	diffy+1268(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1140(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movd	diffy+1204(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1076(%rax), %xmm4 # xmm4 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	movdqa	%xmm4, %xmm1
	psrad	$31, %xmm1
	paddd	%xmm1, %xmm4
	pxor	%xmm1, %xmm4
	paddd	%xmm3, %xmm4
	movd	diffy+1272(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1144(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movd	diffy+1208(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movd	diffy+1080(%rax), %xmm3 # xmm3 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movdqa	%xmm3, %xmm1
	psrad	$31, %xmm1
	paddd	%xmm1, %xmm3
	pxor	%xmm1, %xmm3
	paddd	%xmm4, %xmm3
	paddd	%xmm0, %xmm3
	movd	diffy+1276(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movd	diffy+1148(%rax), %xmm1 # xmm1 = mem[0],zero,zero,zero
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movd	diffy+1212(%rax), %xmm2 # xmm2 = mem[0],zero,zero,zero
	movd	diffy+1084(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpckldq	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movdqa	%xmm0, %xmm1
	psrad	$31, %xmm1
	paddd	%xmm1, %xmm0
	pxor	%xmm1, %xmm0
	paddd	%xmm3, %xmm0
	addq	$256, %rax              # imm = 0x100
	jne	.LBB1_1
# BB#2:                                 # %middle.block
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	retq
.Lfunc_end1:
	.size	calc_MAD, .Lfunc_end1-calc_MAD
	.cfi_endproc

	.globl	QP2Qstep
	.p2align	4, 0x90
	.type	QP2Qstep,@function
QP2Qstep:                               # @QP2Qstep
	.cfi_startproc
# BB#0:
	movslq	%edi, %rcx
	imulq	$715827883, %rcx, %rax  # imm = 0x2AAAAAAB
	movq	%rax, %rdx
	shrq	$63, %rdx
	shrq	$32, %rax
	addl	%edx, %eax
	leal	(%rax,%rax), %edx
	leal	(%rdx,%rdx,2), %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	movsd	QP2Qstep.QP2QSTEP(,%rdx,8), %xmm0 # xmm0 = mem[0],zero
	cmpl	$6, %ecx
	jl	.LBB2_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addsd	%xmm0, %xmm0
	incl	%ecx
	cmpl	%eax, %ecx
	jl	.LBB2_2
.LBB2_3:                                # %._crit_edge
	retq
.Lfunc_end2:
	.size	QP2Qstep, .Lfunc_end2-QP2Qstep
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4603804719079489536     # double 0.625
.LCPI3_1:
	.quad	4642085315912138752     # double 224
.LCPI3_2:
	.quad	4607745368753438720     # double 1.125
.LCPI3_3:
	.quad	4602678819172646912     # double 0.5
.LCPI3_4:
	.quad	4604086194056200192     # double 0.65625
.LCPI3_5:
	.quad	4604930618986332160     # double 0.75
.LCPI3_6:
	.quad	4605775043916464128     # double 0.84375
.LCPI3_7:
	.quad	4606619468846596096     # double 0.9375
.LCPI3_8:
	.quad	4607463893776728064     # double 1.0625
	.text
	.globl	Qstep2QP
	.p2align	4, 0x90
	.type	Qstep2QP,@function
Qstep2QP:                               # @Qstep2QP
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	movsd	.LCPI3_0(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB3_11
# BB#1:                                 # %.lr.ph.i.preheader
	movl	$51, %eax
	ucomisd	.LCPI3_1(%rip), %xmm0
	ja	.LBB3_11
# BB#2:                                 # %.preheader
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	ucomisd	.LCPI3_2(%rip), %xmm0
	jbe	.LBB3_5
# BB#3:                                 # %.lr.ph.preheader
	movsd	.LCPI3_3(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI3_2(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	mulsd	%xmm1, %xmm0
	addl	$6, %ecx
	ucomisd	%xmm2, %xmm0
	ja	.LBB3_4
.LBB3_5:                                # %._crit_edge
	movsd	.LCPI3_4(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jae	.LBB3_10
# BB#6:
	movl	$1, %eax
	movsd	.LCPI3_5(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jae	.LBB3_10
# BB#7:
	movl	$2, %eax
	movsd	.LCPI3_6(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jae	.LBB3_10
# BB#8:
	movl	$3, %eax
	movsd	.LCPI3_7(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jae	.LBB3_10
# BB#9:
	movsd	.LCPI3_8(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	sbbl	%eax, %eax
	andl	$1, %eax
	orl	$4, %eax
.LBB3_10:
	addl	%ecx, %eax
.LBB3_11:
	retq
.Lfunc_end3:
	.size	Qstep2QP, .Lfunc_end3-Qstep2QP
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4643211215818981376     # double 256
	.text
	.globl	ComputeFrameMAD
	.p2align	4, 0x90
	.type	ComputeFrameMAD,@function
ComputeFrameMAD:                        # @ComputeFrameMAD
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movl	15352(%rax), %eax
	testq	%rax, %rax
	je	.LBB4_1
# BB#2:                                 # %.lr.ph
	movq	generic_RC(%rip), %rcx
	movq	64(%rcx), %rcx
	cmpl	$4, %eax
	jb	.LBB4_3
# BB#4:                                 # %min.iters.checked
	movl	%eax, %r8d
	andl	$3, %r8d
	movq	%rax, %rdx
	subq	%r8, %rdx
	je	.LBB4_3
# BB#5:                                 # %vector.body.preheader
	leaq	8(%rcx), %rdi
	pxor	%xmm0, %xmm0
	movq	%rdx, %rsi
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB4_6:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rdi), %xmm2         # xmm2 = mem[0],zero
	movq	(%rdi), %xmm3           # xmm3 = mem[0],zero
	movdqa	%xmm2, %xmm4
	psrad	$31, %xmm4
	punpckldq	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	movdqa	%xmm3, %xmm4
	psrad	$31, %xmm4
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	paddq	%xmm2, %xmm0
	paddq	%xmm3, %xmm1
	addq	$16, %rdi
	addq	$-4, %rsi
	jne	.LBB4_6
# BB#7:                                 # %middle.block
	paddq	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddq	%xmm1, %xmm0
	movd	%xmm0, %rsi
	testl	%r8d, %r8d
	jne	.LBB4_8
	jmp	.LBB4_9
.LBB4_3:
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_8:                                # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rcx,%rdx,4), %rdi
	addq	%rdi, %rsi
	incq	%rdx
	cmpq	%rax, %rdx
	jb	.LBB4_8
.LBB4_9:                                # %._crit_edge.loopexit
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	mulsd	.LCPI4_0(%rip), %xmm1
	divsd	%xmm1, %xmm0
	retq
.LBB4_1:
	xorpd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	divsd	%xmm1, %xmm0
	retq
.Lfunc_end4:
	.size	ComputeFrameMAD, .Lfunc_end4-ComputeFrameMAD
	.cfi_endproc

	.globl	copy_rc_generic
	.p2align	4, 0x90
	.type	copy_rc_generic,@function
copy_rc_generic:                        # @copy_rc_generic
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	64(%rbx), %r15
	movl	$144, %edx
	callq	memcpy
	movq	%r15, 64(%rbx)
	movq	64(%r14), %rsi
	movq	img(%rip), %rax
	movl	15352(%rax), %edx
	shlq	$2, %rdx
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	memcpy                  # TAILCALL
.Lfunc_end5:
	.size	copy_rc_generic, .Lfunc_end5-copy_rc_generic
	.cfi_endproc

	.globl	generic_alloc
	.p2align	4, 0x90
	.type	generic_alloc,@function
generic_alloc:                          # @generic_alloc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$144, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, (%r14)
	testq	%rbx, %rbx
	jne	.LBB6_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
	movq	(%r14), %rbx
.LBB6_2:
	movq	img(%rip), %rax
	movl	15352(%rax), %edi
	movl	$4, %esi
	callq	calloc
	movq	%rax, 64(%rbx)
	movq	(%r14), %rax
	cmpq	$0, 64(%rax)
	jne	.LBB6_4
# BB#3:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
	movq	(%r14), %rax
.LBB6_4:
	movl	$1, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	generic_alloc, .Lfunc_end6-generic_alloc
	.cfi_endproc

	.globl	generic_free
	.p2align	4, 0x90
	.type	generic_free,@function
generic_free:                           # @generic_free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	movq	64(%rdi), %rax
	testq	%rax, %rax
	je	.LBB7_2
# BB#1:
	movq	%rax, %rdi
	callq	free
	movq	(%rbx), %rax
	movq	$0, 64(%rax)
	movq	(%rbx), %rdi
.LBB7_2:
	testq	%rdi, %rdi
	je	.LBB7_4
# BB#3:
	callq	free
	movq	$0, (%rbx)
.LBB7_4:
	popq	%rbx
	retq
.Lfunc_end7:
	.size	generic_free, .Lfunc_end7-generic_free
	.cfi_endproc

	.type	generic_RC,@object      # @generic_RC
	.comm	generic_RC,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	delta_qp_mbaff,@object  # @delta_qp_mbaff
	.comm	delta_qp_mbaff,16,16
	.type	qp_mbaff,@object        # @qp_mbaff
	.comm	qp_mbaff,16,16
	.type	QP2Qstep.QP2QSTEP,@object # @QP2Qstep.QP2QSTEP
	.section	.rodata,"a",@progbits
	.p2align	4
QP2Qstep.QP2QSTEP:
	.quad	4603804719079489536     # double 0.625
	.quad	4604367669032910848     # double 0.6875
	.quad	4605493568939753472     # double 0.8125
	.quad	4606056518893174784     # double 0.875
	.quad	4607182418800017408     # double 1
	.quad	4607745368753438720     # double 1.125
	.size	QP2Qstep.QP2QSTEP, 48

	.type	diffy,@object           # @diffy
	.comm	diffy,1024,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"init_global_buffers: generic_alloc"
	.size	.L.str, 35

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"init_global_buffers: (*prc)->MADofMB"
	.size	.L.str.1, 37

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	generic_RC_init,@object # @generic_RC_init
	.comm	generic_RC_init,8,8
	.type	generic_RC_best,@object # @generic_RC_best
	.comm	generic_RC_best,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
