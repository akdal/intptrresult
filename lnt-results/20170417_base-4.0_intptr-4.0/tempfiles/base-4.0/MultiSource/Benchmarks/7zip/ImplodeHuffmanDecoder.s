	.text
	.file	"ImplodeHuffmanDecoder.bc"
	.globl	_ZN9NCompress8NImplode8NHuffman8CDecoderC2Ej
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NHuffman8CDecoderC2Ej,@function
_ZN9NCompress8NImplode8NHuffman8CDecoderC2Ej: # @_ZN9NCompress8NImplode8NHuffman8CDecoderC2Ej
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	%esi, 144(%rbx)
	movl	%esi, %edi
	shlq	$2, %rdi
	callq	_Znam
	movq	%rax, 152(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN9NCompress8NImplode8NHuffman8CDecoderC2Ej, .Lfunc_end0-_ZN9NCompress8NImplode8NHuffman8CDecoderC2Ej
	.cfi_endproc

	.globl	_ZN9NCompress8NImplode8NHuffman8CDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NHuffman8CDecoderD2Ev,@function
_ZN9NCompress8NImplode8NHuffman8CDecoderD2Ev: # @_ZN9NCompress8NImplode8NHuffman8CDecoderD2Ev
	.cfi_startproc
# BB#0:
	movq	152(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB1_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB1_1:
	retq
.Lfunc_end1:
	.size	_ZN9NCompress8NImplode8NHuffman8CDecoderD2Ev, .Lfunc_end1-_ZN9NCompress8NImplode8NHuffman8CDecoderD2Ev
	.cfi_endproc

	.globl	_ZN9NCompress8NImplode8NHuffman8CDecoder14SetCodeLengthsEPKh
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NHuffman8CDecoder14SetCodeLengthsEPKh,@function
_ZN9NCompress8NImplode8NHuffman8CDecoder14SetCodeLengthsEPKh: # @_ZN9NCompress8NImplode8NHuffman8CDecoder14SetCodeLengthsEPKh
	.cfi_startproc
# BB#0:                                 # %.preheader40
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
.Lcfi4:
	.cfi_offset %rbx, -16
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -80(%rsp)
	movaps	%xmm0, -96(%rsp)
	movaps	%xmm0, -112(%rsp)
	movaps	%xmm0, -128(%rsp)
	movl	$0, -64(%rsp)
	movl	144(%rdi), %ebx
	testq	%rbx, %rbx
	je	.LBB2_5
# BB#1:                                 # %.lr.ph46
	leaq	-1(%rbx), %r8
	movq	%rbx, %rax
	xorl	%ecx, %ecx
	andq	$3, %rax
	je	.LBB2_3
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi,%rcx), %edx
	incl	-128(%rsp,%rdx,4)
	incq	%rcx
	cmpq	%rcx, %rax
	jne	.LBB2_2
.LBB2_3:                                # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB2_5
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi,%rcx), %eax
	incl	-128(%rsp,%rax,4)
	movzbl	1(%rsi,%rcx), %eax
	incl	-128(%rsp,%rax,4)
	movzbl	2(%rsi,%rcx), %eax
	incl	-128(%rsp,%rax,4)
	movzbl	3(%rsi,%rcx), %eax
	incl	-128(%rsp,%rax,4)
	addq	$4, %rcx
	cmpq	%rbx, %rcx
	jb	.LBB2_4
.LBB2_5:                                # %._crit_edge
	movl	$0, 68(%rdi)
	movl	$0, 140(%rdi)
	movl	$0, -60(%rsp)
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movl	$17, %r10d
	xorl	%r11d, %r11d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_6:                                # =>This Inner Loop Header: Depth=1
	movl	-132(%rsp,%r10,4), %eax
	leal	-1(%r8), %ecx
	movl	%eax, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	addl	%edx, %ebx
	cmpl	$65536, %ebx            # imm = 0x10000
	ja	.LBB2_17
# BB#7:                                 #   in Loop: Header=BB2_6 Depth=1
	movl	%ebx, -4(%rdi,%r10,4)
	addl	%r11d, %r9d
	movl	%r9d, 68(%rdi,%r10,4)
	addl	%r9d, %eax
	movl	%eax, -52(%rsp,%r10,4)
	movl	-136(%rsp,%r10,4), %r9d
	movl	%r9d, %edx
	movl	%r8d, %ecx
	shll	%cl, %edx
	addl	%ebx, %edx
	cmpl	$65536, %edx            # imm = 0x10000
	ja	.LBB2_17
# BB#8:                                 #   in Loop: Header=BB2_6 Depth=1
	movl	%edx, -8(%rdi,%r10,4)
	movl	%eax, 64(%rdi,%r10,4)
	leal	(%r9,%rax), %ecx
	movl	%ecx, -56(%rsp,%r10,4)
	addl	$2, %r8d
	addq	$-2, %r10
	cmpq	$1, %r10
	movl	%eax, %r11d
	jg	.LBB2_6
# BB#9:
	cmpl	$65536, %edx            # imm = 0x10000
	jne	.LBB2_17
# BB#10:                                # %.preheader
	movl	144(%rdi), %edx
	movb	$1, %al
	testl	%edx, %edx
	je	.LBB2_18
# BB#11:                                # %.lr.ph
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_12:                               # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebx
	movzbl	(%rsi,%rbx), %ebx
	testq	%rbx, %rbx
	je	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_12 Depth=1
	movq	152(%rdi), %r8
	movslq	-48(%rsp,%rbx,4), %r9
	leaq	-1(%r9), %rdx
	movl	%edx, -48(%rsp,%rbx,4)
	movl	%ecx, -4(%r8,%r9,4)
	movl	144(%rdi), %edx
.LBB2_14:                               #   in Loop: Header=BB2_12 Depth=1
	incl	%ecx
	cmpl	%edx, %ecx
	jb	.LBB2_12
	jmp	.LBB2_18
.LBB2_17:
	xorl	%eax, %eax
.LBB2_18:                               # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZN9NCompress8NImplode8NHuffman8CDecoder14SetCodeLengthsEPKh, .Lfunc_end2-_ZN9NCompress8NImplode8NHuffman8CDecoder14SetCodeLengthsEPKh
	.cfi_endproc

	.globl	_ZN9NCompress8NImplode8NHuffman8CDecoder12DecodeSymbolEPN5NBitl8CDecoderI9CInBufferEE
	.p2align	4, 0x90
	.type	_ZN9NCompress8NImplode8NHuffman8CDecoder12DecodeSymbolEPN5NBitl8CDecoderI9CInBufferEE,@function
_ZN9NCompress8NImplode8NHuffman8CDecoder12DecodeSymbolEPN5NBitl8CDecoderI9CInBufferEE: # @_ZN9NCompress8NImplode8NHuffman8CDecoder12DecodeSymbolEPN5NBitl8CDecoderI9CInBufferEE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	(%rbx), %esi
	cmpl	$7, %esi
	jbe	.LBB3_1
# BB#2:                                 # %.lr.ph.i.i
	leaq	8(%rbx), %r15
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jb	.LBB3_6
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	%r15, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB3_7
# BB#5:                                 # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	(%r15), %rax
.LBB3_6:                                # %_ZN9CInBuffer8ReadByteERh.exit.thread.i.i
                                        #   in Loop: Header=BB3_3 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r15)
	movzbl	(%rax), %eax
	jmp	.LBB3_8
	.p2align	4, 0x90
.LBB3_7:                                # %_ZN9CInBuffer8ReadByteERh.exit.i.i
                                        #   in Loop: Header=BB3_3 Depth=1
	incl	56(%rbx)
	movb	$-1, %al
.LBB3_8:                                #   in Loop: Header=BB3_3 Depth=1
	movzbl	%al, %eax
	movl	(%rbx), %esi
	movl	$32, %ecx
	subl	%esi, %ecx
	movl	%eax, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edx, 60(%rbx)
	movl	4(%rbx), %ecx
	shll	$8, %ecx
	movzbl	_ZN5NBitl12kInvertTableE(%rax), %eax
	orl	%ecx, %eax
	movl	%eax, 4(%rbx)
	addl	$-8, %esi
	movl	%esi, (%rbx)
	cmpl	$7, %esi
	ja	.LBB3_3
	jmp	.LBB3_9
.LBB3_1:                                # %._ZN5NBitl8CDecoderI9CInBufferE9NormalizeEv.exit_crit_edge.i
	movl	4(%rbx), %eax
.LBB3_9:                                # %_ZN5NBitl8CDecoderI9CInBufferE8GetValueEj.exit
	movl	$8, %ecx
	subl	%esi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	shrl	$8, %eax
	movzwl	%ax, %edi
	movl	$16, %edx
	cmpl	64(%r14), %edi
	movl	$16, %ecx
	jb	.LBB3_11
# BB#10:
	movl	$15, %ecx
	cmpl	60(%r14), %edi
	jb	.LBB3_11
# BB#14:
	movl	$14, %ecx
	cmpl	56(%r14), %edi
	jb	.LBB3_11
# BB#15:
	movl	$13, %ecx
	cmpl	52(%r14), %edi
	jb	.LBB3_11
# BB#16:
	movl	$12, %ecx
	cmpl	48(%r14), %edi
	jb	.LBB3_11
# BB#17:
	movl	$11, %ecx
	cmpl	44(%r14), %edi
	jb	.LBB3_11
# BB#18:
	movl	$10, %ecx
	cmpl	40(%r14), %edi
	jb	.LBB3_11
# BB#19:
	movl	$9, %ecx
	cmpl	36(%r14), %edi
	jb	.LBB3_11
# BB#20:
	movl	$8, %ecx
	cmpl	32(%r14), %edi
	jb	.LBB3_11
# BB#21:
	movl	$7, %ecx
	cmpl	28(%r14), %edi
	jb	.LBB3_11
# BB#22:
	movl	$6, %ecx
	cmpl	24(%r14), %edi
	jb	.LBB3_11
# BB#23:
	movl	$5, %ecx
	cmpl	20(%r14), %edi
	jb	.LBB3_11
# BB#24:
	movl	$4, %ecx
	cmpl	16(%r14), %edi
	jb	.LBB3_11
# BB#25:
	movl	$3, %ecx
	cmpl	12(%r14), %edi
	jb	.LBB3_11
# BB#26:
	movl	$2, %ecx
	cmpl	8(%r14), %edi
	jb	.LBB3_11
# BB#27:
	movl	$-1, %eax
	movl	$1, %ecx
	cmpl	4(%r14), %edi
	jae	.LBB3_13
.LBB3_11:                               # %.thread
	addl	%ecx, %esi
	movl	%esi, (%rbx)
	shrl	%cl, 60(%rbx)
	movl	%ecx, %eax
	subl	4(%r14,%rax,4), %edi
	subl	%ecx, %edx
	movl	%edx, %ecx
	shrl	%cl, %edi
	addl	72(%r14,%rax,4), %edi
	movl	$-1, %eax
	cmpl	144(%r14), %edi
	jae	.LBB3_13
# BB#12:
	movq	152(%r14), %rax
	movl	%edi, %ecx
	movl	(%rax,%rcx,4), %eax
.LBB3_13:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZN9NCompress8NImplode8NHuffman8CDecoder12DecodeSymbolEPN5NBitl8CDecoderI9CInBufferEE, .Lfunc_end3-_ZN9NCompress8NImplode8NHuffman8CDecoder12DecodeSymbolEPN5NBitl8CDecoderI9CInBufferEE
	.cfi_endproc


	.globl	_ZN9NCompress8NImplode8NHuffman8CDecoderC1Ej
	.type	_ZN9NCompress8NImplode8NHuffman8CDecoderC1Ej,@function
_ZN9NCompress8NImplode8NHuffman8CDecoderC1Ej = _ZN9NCompress8NImplode8NHuffman8CDecoderC2Ej
	.globl	_ZN9NCompress8NImplode8NHuffman8CDecoderD1Ev
	.type	_ZN9NCompress8NImplode8NHuffman8CDecoderD1Ev,@function
_ZN9NCompress8NImplode8NHuffman8CDecoderD1Ev = _ZN9NCompress8NImplode8NHuffman8CDecoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
