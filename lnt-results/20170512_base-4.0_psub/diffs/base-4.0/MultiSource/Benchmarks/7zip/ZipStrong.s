	.text
	.file	"ZipStrong.bc"
	.globl	_ZN7NCrypto10NZipStrong8CKeyInfo11SetPasswordEPKhj
	.p2align	4, 0x90
	.type	_ZN7NCrypto10NZipStrong8CKeyInfo11SetPasswordEPKhj,@function
_ZN7NCrypto10NZipStrong8CKeyInfo11SetPasswordEPKhj: # @_ZN7NCrypto10NZipStrong8CKeyInfo11SetPasswordEPKhj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$104, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 144
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	%rsp, %r12
	movq	%r12, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 32(%rsp)
	movl	%ebx, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_ZN7NCrypto10NZipStrongL9DeriveKeyERNS_5NSha18CContextEPh
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	_ZN7NCrypto10NZipStrong8CKeyInfo11SetPasswordEPKhj, .Lfunc_end0-_ZN7NCrypto10NZipStrong8CKeyInfo11SetPasswordEPKhj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.zero	16,54
.LCPI1_1:
	.zero	16,92
	.text
	.p2align	4, 0x90
	.type	_ZN7NCrypto10NZipStrongL9DeriveKeyERNS_5NSha18CContextEPh,@function
_ZN7NCrypto10NZipStrongL9DeriveKeyERNS_5NSha18CContextEPh: # @_ZN7NCrypto10NZipStrongL9DeriveKeyERNS_5NSha18CContextEPh
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	subq	$248, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 288
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r12, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rsp, %rsi
	callq	_ZN7NCrypto5NSha18CContext5FinalEPh
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54]
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	xorps	(%rsp), %xmm0
	movaps	%xmm0, 32(%rsp)
	movb	16(%rsp), %al
	xorb	$54, %al
	movb	%al, 48(%rsp)
	movb	17(%rsp), %al
	xorb	$54, %al
	movb	%al, 49(%rsp)
	movb	18(%rsp), %al
	xorb	$54, %al
	movb	%al, 50(%rsp)
	movb	19(%rsp), %al
	xorb	$54, %al
	movb	%al, 51(%rsp)
	leaq	144(%rsp), %rbx
	movq	%rbx, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 176(%rsp)
	leaq	32(%rsp), %r14
	movl	$64, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	leaq	96(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZN7NCrypto5NSha18CContext5FinalEPh
	leaq	116(%rsp), %r15
	movaps	.LCPI1_1(%rip), %xmm0   # xmm0 = [92,92,92,92,92,92,92,92,92,92,92,92,92,92,92,92]
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movb	(%rsp), %al
	xorb	$92, %al
	movb	%al, 32(%rsp)
	movb	1(%rsp), %al
	xorb	$92, %al
	movb	%al, 33(%rsp)
	movb	2(%rsp), %al
	xorb	$92, %al
	movb	%al, 34(%rsp)
	movb	3(%rsp), %al
	xorb	$92, %al
	movb	%al, 35(%rsp)
	movb	4(%rsp), %al
	xorb	$92, %al
	movb	%al, 36(%rsp)
	movb	5(%rsp), %al
	xorb	$92, %al
	movb	%al, 37(%rsp)
	movb	6(%rsp), %al
	xorb	$92, %al
	movb	%al, 38(%rsp)
	movb	7(%rsp), %al
	xorb	$92, %al
	movb	%al, 39(%rsp)
	movb	8(%rsp), %al
	xorb	$92, %al
	movb	%al, 40(%rsp)
	movb	9(%rsp), %al
	xorb	$92, %al
	movb	%al, 41(%rsp)
	movb	10(%rsp), %al
	xorb	$92, %al
	movb	%al, 42(%rsp)
	movb	11(%rsp), %al
	xorb	$92, %al
	movb	%al, 43(%rsp)
	movb	12(%rsp), %al
	xorb	$92, %al
	movb	%al, 44(%rsp)
	movb	13(%rsp), %al
	xorb	$92, %al
	movb	%al, 45(%rsp)
	movb	14(%rsp), %al
	xorb	$92, %al
	movb	%al, 46(%rsp)
	movb	15(%rsp), %al
	xorb	$92, %al
	movb	%al, 47(%rsp)
	movb	16(%rsp), %al
	xorb	$92, %al
	movb	%al, 48(%rsp)
	movb	17(%rsp), %al
	xorb	$92, %al
	movb	%al, 49(%rsp)
	movb	18(%rsp), %al
	xorb	$92, %al
	movb	%al, 50(%rsp)
	movb	19(%rsp), %al
	xorb	$92, %al
	movb	%al, 51(%rsp)
	leaq	144(%rsp), %rbx
	movq	%rbx, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 176(%rsp)
	movl	$64, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZN7NCrypto5NSha18CContext5FinalEPh
	movaps	96(%rsp), %xmm0
	movaps	112(%rsp), %xmm1
	movups	%xmm1, 16(%r12)
	movups	%xmm0, (%r12)
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZN7NCrypto10NZipStrongL9DeriveKeyERNS_5NSha18CContextEPh, .Lfunc_end1-_ZN7NCrypto10NZipStrongL9DeriveKeyERNS_5NSha18CContextEPh
	.cfi_endproc

	.globl	_ZN7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj
	.p2align	4, 0x90
	.type	_ZN7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj,@function
_ZN7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj: # @_ZN7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 40
	subq	$104, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 144
.Lcfi23:
	.cfi_offset %rbx, -40
.Lcfi24:
	.cfi_offset %r12, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	%rsp, %r12
	movq	%r12, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 32(%rsp)
	movl	%ebx, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	leaq	336(%r15), %rsi
	movq	%r12, %rdi
	callq	_ZN7NCrypto10NZipStrongL9DeriveKeyERNS_5NSha18CContextEPh
	xorl	%eax, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	_ZN7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj, .Lfunc_end2-_ZN7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj
	.cfi_endproc

	.globl	_ZThn328_N7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj
	.p2align	4, 0x90
	.type	_ZThn328_N7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj,@function
_ZThn328_N7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj: # @_ZThn328_N7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 40
	subq	$104, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 144
.Lcfi32:
	.cfi_offset %rbx, -40
.Lcfi33:
	.cfi_offset %r12, -32
.Lcfi34:
	.cfi_offset %r14, -24
.Lcfi35:
	.cfi_offset %r15, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	%rsp, %r12
	movq	%r12, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 32(%rsp)
	movl	%ebx, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	leaq	8(%r15), %rsi
	movq	%r12, %rdi
	callq	_ZN7NCrypto10NZipStrongL9DeriveKeyERNS_5NSha18CContextEPh
	xorl	%eax, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZThn328_N7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj, .Lfunc_end3-_ZThn328_N7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj
	.cfi_endproc

	.globl	_ZN7NCrypto10NZipStrong8CDecoder10ReadHeaderEP19ISequentialInStreamjy
	.p2align	4, 0x90
	.type	_ZN7NCrypto10NZipStrong8CDecoder10ReadHeaderEP19ISequentialInStreamjy,@function
_ZN7NCrypto10NZipStrong8CDecoder10ReadHeaderEP19ISequentialInStreamjy: # @_ZN7NCrypto10NZipStrong8CDecoder10ReadHeaderEP19ISequentialInStreamjy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 48
.Lcfi41:
	.cfi_offset %rbx, -40
.Lcfi42:
	.cfi_offset %r12, -32
.Lcfi43:
	.cfi_offset %r14, -24
.Lcfi44:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	leaq	4(%rsp), %rsi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	testl	%eax, %eax
	jne	.LBB4_14
# BB#1:
	movzwl	4(%rsp), %eax
	cmpl	$16, %eax
	movl	%eax, 408(%r14)
	movl	$-2147467263, %eax      # imm = 0x80004001
	jne	.LBB4_14
# BB#2:
	leaq	412(%r14), %rsi
	movl	$16, %edx
	movq	%rbx, %rdi
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	testl	%eax, %eax
	jne	.LBB4_14
# BB#3:
	leaq	4(%rsp), %rsi
	movl	$4, %edx
	movq	%rbx, %rdi
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	testl	%eax, %eax
	jne	.LBB4_14
# BB#4:
	movl	4(%rsp), %r12d
	movl	%r12d, 428(%r14)
	leal	-16(%r12), %eax
	cmpl	$262128, %eax           # imm = 0x3FFF0
	movl	$-2147467263, %eax      # imm = 0x80004001
	ja	.LBB4_14
# BB#5:
	leal	16(%r12), %eax
	cmpq	384(%r14), %rax
	jbe	.LBB4_6
# BB#7:
	leaq	384(%r14), %r15
	movq	392(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB4_9
# BB#8:
	callq	_ZdaPv
	movl	428(%r14), %r12d
.LBB4_9:                                # %_ZN7CBufferIhE4FreeEv.exit
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	movl	%r12d, %r15d
	addl	$16, %r15d
	je	.LBB4_10
# BB#11:
	movq	%r15, %rdi
	callq	_Znam
	movq	%rax, %rsi
	movq	%rsi, 392(%r14)
	movq	%r15, 384(%r14)
	jmp	.LBB4_12
.LBB4_6:                                # %._crit_edge
	movq	400(%r14), %rsi
	jmp	.LBB4_13
.LBB4_10:
	xorl	%esi, %esi
.LBB4_12:                               # %_ZN7CBufferIhE11SetCapacityEm.exit
	addq	$15, %rsi
	andq	$-16, %rsi
	movq	%rsi, 400(%r14)
.LBB4_13:
	movl	%r12d, %edx
	movq	%rbx, %rdi
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
.LBB4_14:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	_ZN7NCrypto10NZipStrong8CDecoder10ReadHeaderEP19ISequentialInStreamjy, .Lfunc_end4-_ZN7NCrypto10NZipStrong8CDecoder10ReadHeaderEP19ISequentialInStreamjy
	.cfi_endproc

	.globl	_ZN7NCrypto10NZipStrong8CDecoder13CheckPasswordERb
	.p2align	4, 0x90
	.type	_ZN7NCrypto10NZipStrong8CDecoder13CheckPasswordERb,@function
_ZN7NCrypto10NZipStrong8CDecoder13CheckPasswordERb: # @_ZN7NCrypto10NZipStrong8CDecoder13CheckPasswordERb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 208
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movb	$0, (%r14)
	movl	428(%rbx), %eax
	movl	$-2147467263, %ebp      # imm = 0x80004001
	cmpl	$16, %eax
	jb	.LBB5_18
# BB#1:
	movq	400(%rbx), %r13
	movzwl	(%r13), %ecx
	cmpl	$3, %ecx
	jne	.LBB5_18
# BB#2:
	movzwl	2(%r13), %ecx
	cmpl	$26126, %ecx            # imm = 0x660E
	jb	.LBB5_18
# BB#3:
	addl	$-26126, %ecx           # imm = 0x99F2
	movzwl	%cx, %ecx
	cmpl	$2, %ecx
	ja	.LBB5_18
# BB#4:
	movl	%ecx, %edx
	shll	$6, %edx
	subl	$-128, %edx
	movzwl	4(%r13), %esi
	cmpl	%esi, %edx
	jne	.LBB5_18
# BB#5:
	movzwl	6(%r13), %edx
	leal	16(,%rcx,8), %ecx
	movl	%ecx, 368(%rbx)
	andl	$16385, %edx            # imm = 0x4001
	cmpl	$1, %edx
	jne	.LBB5_18
# BB#6:
	movzwl	8(%r13), %r15d
	testb	$15, %r15b
	jne	.LBB5_18
# BB#7:
	leal	16(%r15), %r12d
	cmpl	%eax, %r12d
	ja	.LBB5_18
# BB#8:
	leaq	10(%r13), %rsi
	movq	%r13, %rdi
	movq	%r15, %rdx
	callq	memmove
	cmpl	$0, 10(%r13,%r15)
	jne	.LBB5_18
# BB#9:
	leaq	16(%r13,%r15), %rax
	movzwl	-2(%rax), %ecx
	testb	$15, %cl
	jne	.LBB5_18
# BB#10:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rcx, (%rsp)            # 8-byte Spill
	addl	%ecx, %r12d
	cmpl	428(%rbx), %r12d
	jne	.LBB5_18
# BB#11:
	movq	(%rbx), %rax
	leaq	336(%rbx), %rsi
	movl	368(%rbx), %edx
	movq	%rbx, %rdi
	callq	*56(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB5_18
# BB#12:
	movq	(%rbx), %rax
	leaq	412(%rbx), %r12
	movl	$16, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	*64(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB5_18
# BB#13:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*40(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	%r15d, %edx
	callq	*48(%rax)
	leaq	48(%rsp), %rbp
	movq	%rbp, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 80(%rsp)
	movl	$16, %edx
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	addl	$-16, %r15d
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	leaq	16(%rsp), %r15
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	_ZN7NCrypto10NZipStrongL9DeriveKeyERNS_5NSha18CContextEPh
	movq	(%rbx), %rax
	movl	368(%rbx), %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	*56(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB5_18
# BB#14:
	movq	(%rbx), %rax
	movl	$16, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	*64(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB5_18
# BB#15:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*40(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %r15            # 8-byte Reload
	movl	%r15d, %edx
	callq	*48(%rax)
	movl	$-2147467263, %ebp      # imm = 0x80004001
	cmpl	$4, %r15d
	jb	.LBB5_18
# BB#16:
	movq	(%rsp), %rsi            # 8-byte Reload
	addl	$-4, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	(%rdi,%rsi), %r15d
	callq	CrcCalc
	xorl	%ebp, %ebp
	cmpl	%eax, %r15d
	jne	.LBB5_18
# BB#17:
	movb	$1, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*40(%rax)
.LBB5_18:
	movl	%ebp, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN7NCrypto10NZipStrong8CDecoder13CheckPasswordERb, .Lfunc_end5-_ZN7NCrypto10NZipStrong8CDecoder13CheckPasswordERb
	.cfi_endproc

	.section	.text._ZN7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv,@function
_ZN7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv: # @_ZN7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 32
.Lcfi61:
	.cfi_offset %rbx, -32
.Lcfi62:
	.cfi_offset %r14, -24
.Lcfi63:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB6_3
# BB#1:
	movl	$IID_ICryptoProperties, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB6_2
.LBB6_3:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB6_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB6_2:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB6_4
.Lfunc_end6:
	.size	_ZN7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end6-_ZN7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN7NCrypto12CAesCbcCoder6AddRefEv,"axG",@progbits,_ZN7NCrypto12CAesCbcCoder6AddRefEv,comdat
	.weak	_ZN7NCrypto12CAesCbcCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto12CAesCbcCoder6AddRefEv,@function
_ZN7NCrypto12CAesCbcCoder6AddRefEv:     # @_ZN7NCrypto12CAesCbcCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end7:
	.size	_ZN7NCrypto12CAesCbcCoder6AddRefEv, .Lfunc_end7-_ZN7NCrypto12CAesCbcCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN7NCrypto12CAesCbcCoder7ReleaseEv,"axG",@progbits,_ZN7NCrypto12CAesCbcCoder7ReleaseEv,comdat
	.weak	_ZN7NCrypto12CAesCbcCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto12CAesCbcCoder7ReleaseEv,@function
_ZN7NCrypto12CAesCbcCoder7ReleaseEv:    # @_ZN7NCrypto12CAesCbcCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB8_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB8_2:
	popq	%rcx
	retq
.Lfunc_end8:
	.size	_ZN7NCrypto12CAesCbcCoder7ReleaseEv, .Lfunc_end8-_ZN7NCrypto12CAesCbcCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN7NCrypto10NZipStrong10CBaseCoderD2Ev,"axG",@progbits,_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev,comdat
	.weak	_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev,@function
_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev: # @_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+112, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rdi)
	movq	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+184, 328(%rdi)
	movq	$_ZTV7CBufferIhE+16, 376(%rdi)
	movq	392(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB9_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB9_1:                                # %_ZN7CBufferIhED2Ev.exit
	retq
.Lfunc_end9:
	.size	_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev, .Lfunc_end9-_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev
	.cfi_endproc

	.section	.text._ZN7NCrypto10NZipStrong10CBaseCoderD0Ev,"axG",@progbits,_ZN7NCrypto10NZipStrong10CBaseCoderD0Ev,comdat
	.weak	_ZN7NCrypto10NZipStrong10CBaseCoderD0Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto10NZipStrong10CBaseCoderD0Ev,@function
_ZN7NCrypto10NZipStrong10CBaseCoderD0Ev: # @_ZN7NCrypto10NZipStrong10CBaseCoderD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 16
.Lcfi66:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+112, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+184, 328(%rbx)
	movq	$_ZTV7CBufferIhE+16, 376(%rbx)
	movq	392(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_2
# BB#1:
	callq	_ZdaPv
.LBB10_2:                               # %_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end10:
	.size	_ZN7NCrypto10NZipStrong10CBaseCoderD0Ev, .Lfunc_end10-_ZN7NCrypto10NZipStrong10CBaseCoderD0Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 16
	movq	%rdi, %r8
	leaq	-8(%r8), %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB11_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB11_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB11_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB11_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB11_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB11_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB11_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB11_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB11_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB11_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB11_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB11_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB11_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB11_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB11_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB11_32
.LBB11_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICryptoProperties(%rip), %cl
	jne	.LBB11_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICryptoProperties+1(%rip), %cl
	jne	.LBB11_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICryptoProperties+2(%rip), %cl
	jne	.LBB11_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICryptoProperties+3(%rip), %cl
	jne	.LBB11_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICryptoProperties+4(%rip), %cl
	jne	.LBB11_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICryptoProperties+5(%rip), %cl
	jne	.LBB11_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICryptoProperties+6(%rip), %cl
	jne	.LBB11_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICryptoProperties+7(%rip), %cl
	jne	.LBB11_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICryptoProperties+8(%rip), %cl
	jne	.LBB11_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICryptoProperties+9(%rip), %cl
	jne	.LBB11_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICryptoProperties+10(%rip), %cl
	jne	.LBB11_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICryptoProperties+11(%rip), %cl
	jne	.LBB11_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICryptoProperties+12(%rip), %cl
	jne	.LBB11_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICryptoProperties+13(%rip), %cl
	jne	.LBB11_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICryptoProperties+14(%rip), %cl
	jne	.LBB11_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICryptoProperties+15(%rip), %cl
	jne	.LBB11_33
.LBB11_32:
	movq	%r8, (%rdx)
	movq	-8(%r8), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB11_33:                              # %_ZN7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZThn8_N7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end11-_ZThn8_N7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto12CAesCbcCoder6AddRefEv,"axG",@progbits,_ZThn8_N7NCrypto12CAesCbcCoder6AddRefEv,comdat
	.weak	_ZThn8_N7NCrypto12CAesCbcCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto12CAesCbcCoder6AddRefEv,@function
_ZThn8_N7NCrypto12CAesCbcCoder6AddRefEv: # @_ZThn8_N7NCrypto12CAesCbcCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end12:
	.size	_ZThn8_N7NCrypto12CAesCbcCoder6AddRefEv, .Lfunc_end12-_ZThn8_N7NCrypto12CAesCbcCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto12CAesCbcCoder7ReleaseEv,"axG",@progbits,_ZThn8_N7NCrypto12CAesCbcCoder7ReleaseEv,comdat
	.weak	_ZThn8_N7NCrypto12CAesCbcCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto12CAesCbcCoder7ReleaseEv,@function
_ZThn8_N7NCrypto12CAesCbcCoder7ReleaseEv: # @_ZThn8_N7NCrypto12CAesCbcCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB13_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB13_2:                               # %_ZN7NCrypto12CAesCbcCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end13:
	.size	_ZThn8_N7NCrypto12CAesCbcCoder7ReleaseEv, .Lfunc_end13-_ZThn8_N7NCrypto12CAesCbcCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto10NZipStrong10CBaseCoderD1Ev,"axG",@progbits,_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD1Ev,comdat
	.weak	_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD1Ev,@function
_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD1Ev: # @_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD1Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+112, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	movq	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+184, 320(%rdi)
	movq	$_ZTV7CBufferIhE+16, 368(%rdi)
	movq	384(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB14_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB14_1:                               # %_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev.exit
	retq
.Lfunc_end14:
	.size	_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD1Ev, .Lfunc_end14-_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto10NZipStrong10CBaseCoderD0Ev,"axG",@progbits,_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD0Ev,comdat
	.weak	_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD0Ev,@function
_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD0Ev: # @_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 16
.Lcfi70:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+112, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rax)
	movq	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+184, 320(%rax)
	movq	$_ZTV7CBufferIhE+16, 368(%rax)
	movq	384(%rax), %rdi
	leaq	-8(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB15_2
# BB#1:
	callq	_ZdaPv
.LBB15_2:                               # %_ZN7NCrypto10NZipStrong10CBaseCoderD0Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end15:
	.size	_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD0Ev, .Lfunc_end15-_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD0Ev
	.cfi_endproc

	.section	.text._ZThn328_N7NCrypto10NZipStrong10CBaseCoderD1Ev,"axG",@progbits,_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD1Ev,comdat
	.weak	_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD1Ev,@function
_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD1Ev: # @_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD1Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+112, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -328(%rdi)
	movq	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+184, (%rdi)
	movq	$_ZTV7CBufferIhE+16, 48(%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB16_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB16_1:                               # %_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev.exit
	retq
.Lfunc_end16:
	.size	_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD1Ev, .Lfunc_end16-_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn328_N7NCrypto10NZipStrong10CBaseCoderD0Ev,"axG",@progbits,_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD0Ev,comdat
	.weak	_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD0Ev,@function
_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD0Ev: # @_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 16
.Lcfi72:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+112, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -328(%rax)
	movq	$_ZTVN7NCrypto10NZipStrong10CBaseCoderE+184, (%rax)
	movq	$_ZTV7CBufferIhE+16, 48(%rax)
	movq	64(%rax), %rdi
	leaq	-328(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB17_2
# BB#1:
	callq	_ZdaPv
.LBB17_2:                               # %_ZN7NCrypto10NZipStrong10CBaseCoderD0Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end17:
	.size	_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD0Ev, .Lfunc_end17-_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD0Ev
	.cfi_endproc

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB18_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB18_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB18_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB18_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB18_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB18_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB18_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB18_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB18_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB18_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB18_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB18_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB18_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB18_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB18_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB18_16:
	xorl	%eax, %eax
	retq
.Lfunc_end18:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end18-_ZeqRK4GUIDS1_
	.cfi_endproc

	.section	.text._ZN7CBufferIhED2Ev,"axG",@progbits,_ZN7CBufferIhED2Ev,comdat
	.weak	_ZN7CBufferIhED2Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED2Ev,@function
_ZN7CBufferIhED2Ev:                     # @_ZN7CBufferIhED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV7CBufferIhE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB19_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB19_1:
	retq
.Lfunc_end19:
	.size	_ZN7CBufferIhED2Ev, .Lfunc_end19-_ZN7CBufferIhED2Ev
	.cfi_endproc

	.section	.text._ZN7CBufferIhED0Ev,"axG",@progbits,_ZN7CBufferIhED0Ev,comdat
	.weak	_ZN7CBufferIhED0Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED0Ev,@function
_ZN7CBufferIhED0Ev:                     # @_ZN7CBufferIhED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 16
.Lcfi74:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB20_2
# BB#1:
	callq	_ZdaPv
.LBB20_2:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end20:
	.size	_ZN7CBufferIhED0Ev, .Lfunc_end20-_ZN7CBufferIhED0Ev
	.cfi_endproc

	.type	_ZTVN7NCrypto10NZipStrong10CBaseCoderE,@object # @_ZTVN7NCrypto10NZipStrong10CBaseCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN7NCrypto10NZipStrong10CBaseCoderE
	.p2align	3
_ZTVN7NCrypto10NZipStrong10CBaseCoderE:
	.quad	0
	.quad	_ZTIN7NCrypto10NZipStrong10CBaseCoderE
	.quad	_ZN7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN7NCrypto12CAesCbcCoder6AddRefEv
	.quad	_ZN7NCrypto12CAesCbcCoder7ReleaseEv
	.quad	_ZN7NCrypto10NZipStrong10CBaseCoderD2Ev
	.quad	_ZN7NCrypto10NZipStrong10CBaseCoderD0Ev
	.quad	_ZN7NCrypto12CAesCbcCoder4InitEv
	.quad	_ZN7NCrypto12CAesCbcCoder6FilterEPhj
	.quad	_ZN7NCrypto12CAesCbcCoder6SetKeyEPKhj
	.quad	_ZN7NCrypto12CAesCbcCoder13SetInitVectorEPKhj
	.quad	_ZN7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj
	.quad	-8
	.quad	_ZTIN7NCrypto10NZipStrong10CBaseCoderE
	.quad	_ZThn8_N7NCrypto12CAesCbcCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N7NCrypto12CAesCbcCoder6AddRefEv
	.quad	_ZThn8_N7NCrypto12CAesCbcCoder7ReleaseEv
	.quad	_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD1Ev
	.quad	_ZThn8_N7NCrypto10NZipStrong10CBaseCoderD0Ev
	.quad	_ZThn8_N7NCrypto12CAesCbcCoder6SetKeyEPKhj
	.quad	_ZThn8_N7NCrypto12CAesCbcCoder13SetInitVectorEPKhj
	.quad	-328
	.quad	_ZTIN7NCrypto10NZipStrong10CBaseCoderE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD1Ev
	.quad	_ZThn328_N7NCrypto10NZipStrong10CBaseCoderD0Ev
	.quad	_ZThn328_N7NCrypto10NZipStrong10CBaseCoder17CryptoSetPasswordEPKhj
	.size	_ZTVN7NCrypto10NZipStrong10CBaseCoderE, 232

	.type	_ZTSN7NCrypto10NZipStrong10CBaseCoderE,@object # @_ZTSN7NCrypto10NZipStrong10CBaseCoderE
	.globl	_ZTSN7NCrypto10NZipStrong10CBaseCoderE
	.p2align	4
_ZTSN7NCrypto10NZipStrong10CBaseCoderE:
	.asciz	"N7NCrypto10NZipStrong10CBaseCoderE"
	.size	_ZTSN7NCrypto10NZipStrong10CBaseCoderE, 35

	.type	_ZTSN7NCrypto14CAesCbcDecoderE,@object # @_ZTSN7NCrypto14CAesCbcDecoderE
	.section	.rodata._ZTSN7NCrypto14CAesCbcDecoderE,"aG",@progbits,_ZTSN7NCrypto14CAesCbcDecoderE,comdat
	.weak	_ZTSN7NCrypto14CAesCbcDecoderE
	.p2align	4
_ZTSN7NCrypto14CAesCbcDecoderE:
	.asciz	"N7NCrypto14CAesCbcDecoderE"
	.size	_ZTSN7NCrypto14CAesCbcDecoderE, 27

	.type	_ZTIN7NCrypto14CAesCbcDecoderE,@object # @_ZTIN7NCrypto14CAesCbcDecoderE
	.section	.rodata._ZTIN7NCrypto14CAesCbcDecoderE,"aG",@progbits,_ZTIN7NCrypto14CAesCbcDecoderE,comdat
	.weak	_ZTIN7NCrypto14CAesCbcDecoderE
	.p2align	4
_ZTIN7NCrypto14CAesCbcDecoderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7NCrypto14CAesCbcDecoderE
	.quad	_ZTIN7NCrypto12CAesCbcCoderE
	.size	_ZTIN7NCrypto14CAesCbcDecoderE, 24

	.type	_ZTS18ICryptoSetPassword,@object # @_ZTS18ICryptoSetPassword
	.section	.rodata._ZTS18ICryptoSetPassword,"aG",@progbits,_ZTS18ICryptoSetPassword,comdat
	.weak	_ZTS18ICryptoSetPassword
	.p2align	4
_ZTS18ICryptoSetPassword:
	.asciz	"18ICryptoSetPassword"
	.size	_ZTS18ICryptoSetPassword, 21

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI18ICryptoSetPassword,@object # @_ZTI18ICryptoSetPassword
	.section	.rodata._ZTI18ICryptoSetPassword,"aG",@progbits,_ZTI18ICryptoSetPassword,comdat
	.weak	_ZTI18ICryptoSetPassword
	.p2align	4
_ZTI18ICryptoSetPassword:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18ICryptoSetPassword
	.quad	_ZTI8IUnknown
	.size	_ZTI18ICryptoSetPassword, 24

	.type	_ZTIN7NCrypto10NZipStrong10CBaseCoderE,@object # @_ZTIN7NCrypto10NZipStrong10CBaseCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN7NCrypto10NZipStrong10CBaseCoderE
	.p2align	4
_ZTIN7NCrypto10NZipStrong10CBaseCoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN7NCrypto10NZipStrong10CBaseCoderE
	.long	1                       # 0x1
	.long	2                       # 0x2
	.quad	_ZTIN7NCrypto14CAesCbcDecoderE
	.quad	2                       # 0x2
	.quad	_ZTI18ICryptoSetPassword
	.quad	83970                   # 0x14802
	.size	_ZTIN7NCrypto10NZipStrong10CBaseCoderE, 56

	.type	_ZTV7CBufferIhE,@object # @_ZTV7CBufferIhE
	.section	.rodata._ZTV7CBufferIhE,"aG",@progbits,_ZTV7CBufferIhE,comdat
	.weak	_ZTV7CBufferIhE
	.p2align	3
_ZTV7CBufferIhE:
	.quad	0
	.quad	_ZTI7CBufferIhE
	.quad	_ZN7CBufferIhED2Ev
	.quad	_ZN7CBufferIhED0Ev
	.size	_ZTV7CBufferIhE, 32

	.type	_ZTS7CBufferIhE,@object # @_ZTS7CBufferIhE
	.section	.rodata._ZTS7CBufferIhE,"aG",@progbits,_ZTS7CBufferIhE,comdat
	.weak	_ZTS7CBufferIhE
_ZTS7CBufferIhE:
	.asciz	"7CBufferIhE"
	.size	_ZTS7CBufferIhE, 12

	.type	_ZTI7CBufferIhE,@object # @_ZTI7CBufferIhE
	.section	.rodata._ZTI7CBufferIhE,"aG",@progbits,_ZTI7CBufferIhE,comdat
	.weak	_ZTI7CBufferIhE
	.p2align	3
_ZTI7CBufferIhE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS7CBufferIhE
	.size	_ZTI7CBufferIhE, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
