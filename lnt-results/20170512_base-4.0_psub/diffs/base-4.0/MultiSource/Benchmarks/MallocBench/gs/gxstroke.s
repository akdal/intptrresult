	.text
	.file	"gxstroke.bc"
	.globl	gx_stroke_fill
	.p2align	4, 0x90
	.type	gx_stroke_fill,@function
gx_stroke_fill:                         # @gx_stroke_fill
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	$0, stroke_path(%rip)
	movl	$stroke_fill, %esi
	movq	%rbx, %rdx
	callq	stroke
	movl	%eax, %ebp
	movq	stroke_path(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#1:
	testl	%ebp, %ebp
	js	.LBB0_3
# BB#2:
	movq	312(%rbx), %rsi
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	gx_fill_path
	movl	%eax, %ebp
	movq	stroke_path(%rip), %rdi
.LBB0_3:
	callq	gx_path_release
.LBB0_4:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	gx_stroke_fill, .Lfunc_end0-gx_stroke_fill
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_1:
	.long	1061158912              # float 0.75
.LCPI1_2:
	.long	1166016512              # float 4096
.LCPI1_3:
	.long	964689920               # float 2.44140625E-4
	.text
	.globl	stroke
	.p2align	4, 0x90
	.type	stroke,@function
stroke:                                 # @stroke
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$888, %rsp              # imm = 0x378
.Lcfi11:
	.cfi_def_cfa_offset 944
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	280(%rdx), %rcx
	movq	24(%rcx), %rax
	movq	%rax, 376(%rsp)         # 8-byte Spill
	movl	32(%rcx), %eax
	movl	%eax, 68(%rsp)          # 4-byte Spill
	movq	56(%rdx), %rax
	orq	40(%rdx), %rax
	movabsq	$9223372036854775807, %r15 # imm = 0x7FFFFFFFFFFFFFFF
	andq	%rax, %r15
	movq	%rcx, 312(%rsp)         # 8-byte Spill
	movl	(%rcx), %eax
	movss	24(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 336(%rsp)        # 16-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movss	72(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 320(%rsp)        # 16-byte Spill
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	movl	%ecx, 60(%rsp)          # 4-byte Spill
	movl	$1008981770, %r14d      # imm = 0x3C23D70A
	cmovnel	%eax, %r14d
	je	.LBB1_3
# BB#1:
	testq	%r15, %r15
	jne	.LBB1_3
# BB#2:
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	336(%rsp), %xmm4        # 16-byte Reload
	movaps	%xmm4, %xmm1
	xorps	%xmm0, %xmm1
	xorps	%xmm2, %xmm2
	movaps	%xmm4, %xmm3
	cmpltss	%xmm2, %xmm3
	andps	%xmm3, %xmm1
	andnps	%xmm4, %xmm3
	orps	%xmm1, %xmm3
	movaps	320(%rsp), %xmm4        # 16-byte Reload
	xorps	%xmm4, %xmm0
	movaps	%xmm4, %xmm1
	cmpltss	%xmm2, %xmm1
	andps	%xmm1, %xmm0
	andnps	%xmm4, %xmm1
	orps	%xmm0, %xmm1
	maxss	%xmm1, %xmm3
	movd	%eax, %xmm0
	mulss	%xmm3, %xmm0
	movss	.LCPI1_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	xorl	%ecx, %ecx
	ucomiss	%xmm0, %xmm1
	seta	%cl
	movl	%ecx, 60(%rsp)          # 4-byte Spill
	movl	%eax, %r14d
.LBB1_3:
	cmpl	$0, 112(%rdi)
	movq	%rdi, 360(%rsp)         # 8-byte Spill
	je	.LBB1_6
# BB#4:
	movq	16(%rsp), %rax          # 8-byte Reload
	movss	440(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	leaq	744(%rsp), %rsi
	callq	gx_path_flatten
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB1_79
# BB#5:                                 # %.preheader
	movq	832(%rsp), %rbp
	testq	%rbp, %rbp
	jne	.LBB1_7
	jmp	.LBB1_76
.LBB1_6:
	movq	88(%rdi), %rbp
	testq	%rbp, %rbp
	je	.LBB1_78
.LBB1_7:                                # %.lr.ph306
	movd	%r14d, %xmm0
	mulss	.LCPI1_2(%rip), %xmm0
	movss	%xmm0, 132(%rsp)        # 4-byte Spill
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	sete	%al
	cmpl	$0, 68(%rsp)            # 4-byte Folded Reload
	setne	%cl
	orb	%al, %cl
	movb	%cl, 15(%rsp)           # 1-byte Spill
	movaps	320(%rsp), %xmm0        # 16-byte Reload
	unpcklps	336(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	movaps	%xmm0, 384(%rsp)        # 16-byte Spill
                                        # implicit-def: %XMM0
	movss	%xmm0, 36(%rsp)         # 4-byte Spill
                                        # implicit-def: %RAX
                                        # implicit-def: %R9
	movq	%r15, 104(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_10 Depth 2
                                        #       Child Loop BB1_30 Depth 3
	movl	48(%rbp), %esi
	movq	8(%rbp), %rcx
	movl	%esi, %edx
	testl	%esi, %esi
	movq	%rax, 112(%rsp)         # 8-byte Spill
	je	.LBB1_72
# BB#9:                                 # %.lr.ph293.preheader
                                        #   in Loop: Header=BB1_8 Depth=1
	movq	24(%rbp), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rbp, 368(%rsp)         # 8-byte Spill
	movq	32(%rbp), %r14
	movq	312(%rsp), %rax         # 8-byte Reload
	movb	40(%rax), %bl
	movss	48(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movl	44(%rax), %r12d
	xorl	%ebp, %ebp
	movl	$0, 24(%rsp)            # 4-byte Folded Spill
	movl	%edx, %eax
	.p2align	4, 0x90
.LBB1_10:                               # %.lr.ph293
                                        #   Parent Loop BB1_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_30 Depth 3
	movq	72(%rsp), %r15          # 8-byte Reload
	decl	%eax
	movq	24(%rcx), %rdx
	movq	32(%rcx), %rsi
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	subq	%r15, %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	jne	.LBB1_13
# BB#11:                                # %.lr.ph293
                                        #   in Loop: Header=BB1_10 Depth=2
	cmpq	%r14, %rsi
	jne	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_10 Depth=2
	movq	%rsi, %r14
	jmp	.LBB1_64
	.p2align	4, 0x90
.LBB1_13:                               #   in Loop: Header=BB1_10 Depth=2
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movl	%eax, 64(%rsp)          # 4-byte Spill
	je	.LBB1_16
# BB#14:                                #   in Loop: Header=BB1_10 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	subq	%r14, %rax
	movq	80(%rsp), %rcx          # 8-byte Reload
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rcx, %xmm1
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movss	%xmm3, 4(%rsp)          # 4-byte Spill
	je	.LBB1_17
# BB#15:                                #   in Loop: Header=BB1_10 Depth=2
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rax, %xmm1
	cvtss2sd	%xmm1, %xmm1
	movb	$2, %al
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	48(%rsp), %rsi
	callq	gs_idtransform
	movss	4(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	48(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	52(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	jmp	.LBB1_18
	.p2align	4, 0x90
.LBB1_16:                               #   in Loop: Header=BB1_10 Depth=2
	leaq	272(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movl	$1, 304(%rsp)
	movl	$1, %edi
	movl	$0, %r8d
	xorl	%esi, %esi
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movq	%r9, 80(%rsp)           # 8-byte Spill
	cmpl	$0, 68(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_28
	jmp	.LBB1_57
.LBB1_17:                               #   in Loop: Header=BB1_10 Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	divps	384(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm1, 48(%rsp)
	movss	%xmm0, 52(%rsp)
.LBB1_18:                               #   in Loop: Header=BB1_10 Depth=2
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	movss	%xmm1, 36(%rsp)         # 4-byte Spill
	ucomiss	%xmm1, %xmm1
	jnp	.LBB1_20
# BB#19:                                # %call.sqrt
                                        #   in Loop: Header=BB1_10 Depth=2
	callq	sqrtf
	movss	4(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	%xmm0, 36(%rsp)         # 4-byte Spill
.LBB1_20:                               # %.split
                                        #   in Loop: Header=BB1_10 Depth=2
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	je	.LBB1_22
# BB#21:                                #   in Loop: Header=BB1_10 Depth=2
	leaq	272(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movl	$1, 304(%rsp)
	movl	$1, %edi
	jmp	.LBB1_26
.LBB1_22:                               #   in Loop: Header=BB1_10 Depth=2
	movss	132(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	divss	36(%rsp), %xmm1         # 4-byte Folded Reload
	movss	48(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movss	%xmm0, 48(%rsp)
	mulss	52(%rsp), %xmm1
	movss	%xmm1, 52(%rsp)
	movaps	336(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm1, %xmm2
	cvttss2si	%xmm2, %rdx
	movq	%rdx, %rcx
	negq	%rcx
	movq	%rcx, 272(%rsp)
	movaps	320(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm0, %xmm2
	cvttss2si	%xmm2, %rax
	movq	%rax, 280(%rsp)
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	je	.LBB1_24
# BB#23:                                #   in Loop: Header=BB1_10 Depth=2
	movq	16(%rsp), %rsi          # 8-byte Reload
	movss	56(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	cvttss2si	%xmm2, %rcx
	subq	%rdx, %rcx
	movq	%rcx, 272(%rsp)
	movss	40(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	cvttss2si	%xmm2, %rdx
	subq	%rdx, %rax
	movq	%rax, 280(%rsp)
.LBB1_24:                               #   in Loop: Header=BB1_10 Depth=2
	movq	%rcx, %rdx
	negq	%rdx
	cmovlq	%rcx, %rdx
	movq	%rax, %rsi
	negq	%rsi
	cmovlq	%rax, %rsi
	addq	%rdx, %rsi
	xorl	%edi, %edi
	cmpq	$3072, %rsi             # imm = 0xC00
	setl	%dil
	movl	%edi, 304(%rsp)
	cmpq	$3071, %rsi             # imm = 0xBFF
	jg	.LBB1_55
# BB#25:                                #   in Loop: Header=BB1_10 Depth=2
	leaq	272(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
.LBB1_26:                               #   in Loop: Header=BB1_10 Depth=2
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	xorl	%eax, %eax
	xorl	%ecx, %ecx
.LBB1_27:                               #   in Loop: Header=BB1_10 Depth=2
	cmpl	$0, 68(%rsp)            # 4-byte Folded Reload
	je	.LBB1_57
.LBB1_28:                               #   in Loop: Header=BB1_10 Depth=2
	movss	36(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	.LCPI1_3(%rip), %xmm4
	ucomiss	%xmm3, %xmm4
	jbe	.LBB1_44
# BB#29:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_10 Depth=2
	movq	80(%rsp), %rax          # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	movss	%xmm0, 140(%rsp)        # 4-byte Spill
	movq	112(%rsp), %rax         # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	movss	%xmm0, 136(%rsp)        # 4-byte Spill
	movaps	%xmm4, %xmm2
	movq	%rbp, %r13
	movl	24(%rsp), %ebp          # 4-byte Reload
	movss	%xmm4, 100(%rsp)        # 4-byte Spill
	.p2align	4, 0x90
.LBB1_30:                               # %.lr.ph
                                        #   Parent Loop BB1_8 Depth=1
                                        #     Parent Loop BB1_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r15, %rcx
	movq	%r14, %rax
	movaps	%xmm3, %xmm0
	divss	%xmm4, %xmm0
	movss	140(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	cvttss2si	%xmm1, %r15
	addq	%rcx, %r15
	mulss	136(%rsp), %xmm0        # 4-byte Folded Reload
	cvttss2si	%xmm0, %r14
	addq	%rax, %r14
	testb	%bl, %bl
	je	.LBB1_38
# BB#31:                                #   in Loop: Header=BB1_30 Depth=3
	movq	%rcx, 144(%rsp)
	movq	%rax, 152(%rsp)
	movq	%r15, 208(%rsp)
	movq	%r14, 216(%rsp)
	cmpl	$0, 304(%rsp)
	jne	.LBB1_33
# BB#32:                                #   in Loop: Header=BB1_30 Depth=3
	movq	272(%rsp), %rdx
	movq	280(%rsp), %rsi
	movq	%rcx, %rdi
	subq	%rdx, %rdi
	movq	%rdi, 160(%rsp)
	movq	%rax, %rdi
	subq	%rsi, %rdi
	movq	%rdi, 168(%rsp)
	addq	%rdx, %rcx
	movq	%rcx, 176(%rsp)
	addq	%rsi, %rax
	movq	%rax, 184(%rsp)
	leaq	(%r15,%rdx), %rax
	movq	%rax, 224(%rsp)
	leaq	(%r14,%rsi), %rax
	movq	%rax, 232(%rsp)
	movq	%r15, %rax
	subq	%rdx, %rax
	movq	%rax, 240(%rsp)
	movq	%r14, %rax
	subq	%rsi, %rax
	movq	%rax, 248(%rsp)
	movdqu	288(%rsp), %xmm0
	xorps	%xmm1, %xmm1
	psubq	%xmm0, %xmm1
	movdqu	%xmm1, 192(%rsp)
	movdqu	%xmm0, 256(%rsp)
.LBB1_33:                               #   in Loop: Header=BB1_30 Depth=3
	testl	%ebp, %ebp
	leaq	144(%rsp), %rbx
	movss	%xmm3, 4(%rsp)          # 4-byte Spill
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	jne	.LBB1_35
# BB#34:                                #   in Loop: Header=BB1_30 Depth=3
	movl	$168, %edx
	leaq	576(%rsp), %rdi
	movq	%rbx, %rsi
	callq	memcpy
.LBB1_35:                               #   in Loop: Header=BB1_30 Depth=3
	incl	%ebp
	testl	%r13d, %r13d
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%r14, %r15
	movl	%ebp, %r14d
	je	.LBB1_39
# BB#36:                                #   in Loop: Header=BB1_30 Depth=3
	leal	-1(%r13), %edi
	leaq	408(%rsp), %rbp
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movq	16(%rsp), %rcx          # 8-byte Reload
	callq	*88(%rsp)               # 8-byte Folded Reload
	movl	$168, %edx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	memcpy
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	4(%rsp), %xmm2          # 4-byte Folded Reload
	cmpl	$-1, %r13d
	je	.LBB1_41
# BB#37:                                #   in Loop: Header=BB1_30 Depth=3
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	movq	%r13, %rbp
	leaq	408(%rsp), %r13
	jmp	.LBB1_40
	.p2align	4, 0x90
.LBB1_38:                               # %.thread
                                        #   in Loop: Header=BB1_30 Depth=3
	subss	%xmm3, %xmm2
	movb	$1, %bl
	jmp	.LBB1_43
	.p2align	4, 0x90
.LBB1_39:                               # %.thread316
                                        #   in Loop: Header=BB1_30 Depth=3
	movq	%r13, %rbp
	movl	$168, %edx
	leaq	408(%rsp), %r13
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	memcpy
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	4(%rsp), %xmm0          # 4-byte Folded Reload
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
.LBB1_40:                               #   in Loop: Header=BB1_30 Depth=3
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	movl	%ebp, %edi
	movq	%r13, %rsi
	movq	16(%rsp), %rcx          # 8-byte Reload
	callq	*88(%rsp)               # 8-byte Folded Reload
	xorl	%r13d, %r13d
	movl	%r14d, %ebp
	movq	%r15, %r14
	movq	24(%rsp), %r15          # 8-byte Reload
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jmp	.LBB1_42
.LBB1_41:                               #   in Loop: Header=BB1_30 Depth=3
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	movl	%r14d, %ebp
	movq	%r15, %r14
	movq	24(%rsp), %r15          # 8-byte Reload
.LBB1_42:                               #   in Loop: Header=BB1_30 Depth=3
	movss	100(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
.LBB1_43:                               #   in Loop: Header=BB1_30 Depth=3
	incl	%r12d
	cmpl	68(%rsp), %r12d         # 4-byte Folded Reload
	movl	$0, %eax
	cmovel	%eax, %r12d
	movslq	%r12d, %rax
	movq	376(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%rax,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm2
	ja	.LBB1_30
	jmp	.LBB1_45
.LBB1_44:                               #   in Loop: Header=BB1_10 Depth=2
	movaps	%xmm4, %xmm2
	movq	%rbp, %r13
	movl	24(%rsp), %ebp          # 4-byte Reload
.LBB1_45:                               # %._crit_edge
                                        #   in Loop: Header=BB1_10 Depth=2
	movl	%ebp, 24(%rsp)          # 4-byte Spill
	movq	%r13, %rbp
	testb	%bl, %bl
	je	.LBB1_53
# BB#46:                                #   in Loop: Header=BB1_10 Depth=2
	movq	%rbx, %r13
	movq	%r15, 144(%rsp)
	movq	%r14, 152(%rsp)
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, 208(%rsp)
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 216(%rsp)
	cmpl	$0, 304(%rsp)
	jne	.LBB1_48
# BB#47:                                #   in Loop: Header=BB1_10 Depth=2
	movq	272(%rsp), %rax
	movq	280(%rsp), %rcx
	movq	%r15, %rdx
	subq	%rax, %rdx
	movq	%rdx, 160(%rsp)
	movq	%r14, %rdx
	subq	%rcx, %rdx
	movq	%rdx, 168(%rsp)
	addq	%rax, %r15
	movq	%r15, 176(%rsp)
	addq	%rcx, %r14
	movq	%r14, 184(%rsp)
	movq	72(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax), %rdx
	movq	%rdx, 224(%rsp)
	movq	40(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rcx), %rdx
	movq	%rdx, 232(%rsp)
	movq	%rsi, %rdx
	subq	%rax, %rdx
	movq	%rdx, 240(%rsp)
	movq	%rdi, %rax
	subq	%rcx, %rax
	movq	%rax, 248(%rsp)
	movdqu	288(%rsp), %xmm0
	xorps	%xmm1, %xmm1
	psubq	%xmm0, %xmm1
	movdqu	%xmm1, 192(%rsp)
	movdqu	%xmm0, 256(%rsp)
.LBB1_48:                               #   in Loop: Header=BB1_10 Depth=2
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	leaq	408(%rsp), %r15
	movss	%xmm3, 4(%rsp)          # 4-byte Spill
	leaq	144(%rsp), %rbx
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	jne	.LBB1_50
# BB#49:                                #   in Loop: Header=BB1_10 Depth=2
	movl	$168, %edx
	leaq	576(%rsp), %rdi
	movq	%rbx, %rsi
	callq	memcpy
.LBB1_50:                               #   in Loop: Header=BB1_10 Depth=2
	testl	%ebp, %ebp
	movq	40(%rsp), %r14          # 8-byte Reload
	je	.LBB1_52
# BB#51:                                #   in Loop: Header=BB1_10 Depth=2
	leal	-1(%rbp), %edi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movq	16(%rsp), %rcx          # 8-byte Reload
	callq	*88(%rsp)               # 8-byte Folded Reload
.LBB1_52:                               #   in Loop: Header=BB1_10 Depth=2
	incl	24(%rsp)                # 4-byte Folded Spill
	incl	%ebp
	movl	$168, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	memcpy
	movq	120(%rsp), %rcx         # 8-byte Reload
	movl	64(%rsp), %eax          # 4-byte Reload
	movss	4(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movq	%r13, %rbx
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jmp	.LBB1_54
.LBB1_53:                               #   in Loop: Header=BB1_10 Depth=2
	movq	120(%rsp), %rcx         # 8-byte Reload
	movl	64(%rsp), %eax          # 4-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
.LBB1_54:                               #   in Loop: Header=BB1_10 Depth=2
	subss	%xmm2, %xmm3
	movq	80(%rsp), %r9           # 8-byte Reload
	jmp	.LBB1_64
.LBB1_55:                               #   in Loop: Header=BB1_10 Depth=2
	movaps	336(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm0, %xmm2
	cvttss2si	%xmm2, %rsi
	movq	%rsi, 288(%rsp)
	movaps	320(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm1, %xmm2
	cvttss2si	%xmm2, %r8
	movq	%r8, 296(%rsp)
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	je	.LBB1_27
# BB#56:                                #   in Loop: Header=BB1_10 Depth=2
	movq	16(%rsp), %r9           # 8-byte Reload
	mulss	56(%r9), %xmm1
	cvttss2si	%xmm1, %rdx
	addq	%rdx, %rsi
	movq	%rsi, 288(%rsp)
	mulss	40(%r9), %xmm0
	cvttss2si	%xmm0, %rdx
	addq	%rdx, %r8
	movq	%r8, 296(%rsp)
	cmpl	$0, 68(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_28
.LBB1_57:                               #   in Loop: Header=BB1_10 Depth=2
	movq	%r15, 144(%rsp)
	movq	%r14, 152(%rsp)
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, 208(%rsp)
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, 216(%rsp)
	testl	%edi, %edi
	movq	%rbx, %r13
	jne	.LBB1_59
# BB#58:                                #   in Loop: Header=BB1_10 Depth=2
	movq	%r15, %rdx
	subq	%rcx, %rdx
	movq	%rdx, 160(%rsp)
	movq	%r14, %rdx
	subq	%rax, %rdx
	movq	%rdx, 168(%rsp)
	addq	%rcx, %r15
	movq	%r15, 176(%rsp)
	addq	%rax, %r14
	movq	%r14, 184(%rsp)
	movq	72(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rcx), %rdx
	movq	%rdx, 224(%rsp)
	movq	40(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rax), %rdx
	movq	%rdx, 232(%rsp)
	movq	%rdi, %rdx
	subq	%rcx, %rdx
	movq	%rdx, 240(%rsp)
	movq	%rbx, %rcx
	subq	%rax, %rcx
	movq	%rcx, 248(%rsp)
	movq	%rsi, %rax
	negq	%rax
	movq	%rax, 192(%rsp)
	movq	%r8, %rax
	negq	%rax
	movq	%rax, 200(%rsp)
	movq	%rsi, 256(%rsp)
	movq	%r8, 264(%rsp)
.LBB1_59:                               #   in Loop: Header=BB1_10 Depth=2
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	leaq	144(%rsp), %r15
	movss	%xmm3, 4(%rsp)          # 4-byte Spill
	jne	.LBB1_61
# BB#60:                                #   in Loop: Header=BB1_10 Depth=2
	movl	$168, %edx
	leaq	576(%rsp), %rdi
	movq	%r15, %rsi
	callq	memcpy
.LBB1_61:                               #   in Loop: Header=BB1_10 Depth=2
	testl	%ebp, %ebp
	leaq	408(%rsp), %rbx
	movq	40(%rsp), %r14          # 8-byte Reload
	je	.LBB1_63
# BB#62:                                #   in Loop: Header=BB1_10 Depth=2
	leal	-1(%rbp), %edi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	16(%rsp), %rcx          # 8-byte Reload
	callq	*88(%rsp)               # 8-byte Folded Reload
.LBB1_63:                               #   in Loop: Header=BB1_10 Depth=2
	incl	24(%rsp)                # 4-byte Folded Spill
	incl	%ebp
	movl	$168, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	memcpy
	movq	80(%rsp), %r9           # 8-byte Reload
	movq	120(%rsp), %rcx         # 8-byte Reload
	movl	64(%rsp), %eax          # 4-byte Reload
	movss	4(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB1_64:                               #   in Loop: Header=BB1_10 Depth=2
	movq	8(%rcx), %rcx
	testl	%eax, %eax
	jne	.LBB1_10
# BB#65:                                # %._crit_edge294
                                        #   in Loop: Header=BB1_8 Depth=1
	testl	%ebp, %ebp
	je	.LBB1_72
# BB#66:                                #   in Loop: Header=BB1_8 Depth=1
	movq	%r9, %r14
	movq	%rbx, %rax
	movq	%rcx, %rbx
	decl	%ebp
	movq	%rbp, %rdi
	testb	%al, %al
	je	.LBB1_69
# BB#67:                                #   in Loop: Header=BB1_8 Depth=1
	movq	368(%rsp), %rax         # 8-byte Reload
	movb	56(%rax), %al
	testb	%al, %al
	je	.LBB1_69
# BB#68:                                #   in Loop: Header=BB1_8 Depth=1
	movq	312(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 40(%rax)
	leaq	576(%rsp), %rdx
	jne	.LBB1_70
.LBB1_69:                               #   in Loop: Header=BB1_8 Depth=1
	xorl	%edx, %edx
.LBB1_70:                               #   in Loop: Header=BB1_8 Depth=1
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	leaq	408(%rsp), %rsi
	movq	16(%rsp), %rcx          # 8-byte Reload
	callq	*88(%rsp)               # 8-byte Folded Reload
	movq	%rbx, %rcx
	movq	%rcx, %rbp
	movq	%r14, %r9
	jmp	.LBB1_73
	.p2align	4, 0x90
.LBB1_72:                               #   in Loop: Header=BB1_8 Depth=1
	movq	%rcx, %rbp
.LBB1_73:                               #   in Loop: Header=BB1_8 Depth=1
	movl	$stroke_path_body, %eax
	cmpq	%rax, stroke_path(%rip)
	jne	.LBB1_75
# BB#74:                                #   in Loop: Header=BB1_8 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	312(%rdx), %rsi
	movl	$stroke_path_body, %edi
	movq	%rcx, %r14
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	movq	%r9, %rbx
	callq	gx_fill_path
	movq	stroke_path(%rip), %rdi
	callq	gx_path_release
	movq	%rbx, %r9
	movq	%r14, %rcx
	movq	$0, stroke_path(%rip)
.LBB1_75:                               #   in Loop: Header=BB1_8 Depth=1
	testq	%rcx, %rcx
	movq	112(%rsp), %rax         # 8-byte Reload
	jne	.LBB1_8
.LBB1_76:                               # %._crit_edge307
	xorl	%ebp, %ebp
	movq	360(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 112(%rax)
	je	.LBB1_79
# BB#77:
	leaq	744(%rsp), %rdi
	callq	gx_path_release
	jmp	.LBB1_79
.LBB1_78:
	xorl	%ebp, %ebp
.LBB1_79:
	movl	%ebp, %eax
	addq	$888, %rsp              # imm = 0x378
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	stroke, .Lfunc_end1-stroke
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.quad	1024                    # 0x400
	.quad	1024                    # 0x400
.LCPI2_1:
	.quad	512                     # 0x200
	.quad	512                     # 0x200
.LCPI2_2:
	.quad	-1024                   # 0xfffffffffffffc00
	.quad	-1024                   # 0xfffffffffffffc00
.LCPI2_3:
	.quad	-512                    # 0xfffffffffffffe00
	.quad	-512                    # 0xfffffffffffffe00
	.text
	.globl	stroke_fill
	.p2align	4, 0x90
	.type	stroke_fill,@function
stroke_fill:                            # @stroke_fill
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 48
.Lcfi23:
	.cfi_offset %rbx, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movl	%edi, %ebp
	cmpl	$0, 160(%rbx)
	je	.LBB2_3
# BB#1:
	movq	264(%r14), %rdi
	movq	(%rbx), %rsi
	movq	8(%rbx), %rdx
	movq	64(%rbx), %rcx
	movq	72(%rbx), %r8
	callq	gx_cpath_includes_rectangle
	testl	%eax, %eax
	je	.LBB2_2
# BB#8:
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	movq	64(%rbx), %rdx
	movq	72(%rbx), %rcx
	movq	312(%r14), %r8
	xorl	%eax, %eax
	movq	%r14, %r9
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	gz_draw_line_fixed      # TAILCALL
.LBB2_2:
	movdqa	.LCPI2_0(%rip), %xmm0   # xmm0 = [1024,1024]
	movdqu	%xmm0, 128(%rbx)
	movaps	.LCPI2_1(%rip), %xmm1   # xmm1 = [512,512]
	movups	%xmm1, 144(%rbx)
	movdqu	(%rbx), %xmm2
	movdqa	.LCPI2_2(%rip), %xmm3   # xmm3 = [18446744073709550592,18446744073709550592]
	movdqa	%xmm2, %xmm4
	paddq	%xmm3, %xmm4
	movdqu	%xmm4, 16(%rbx)
	paddq	%xmm0, %xmm2
	movdqu	%xmm2, 32(%rbx)
	movdqu	64(%rbx), %xmm2
	paddq	%xmm2, %xmm0
	movdqu	%xmm0, 80(%rbx)
	paddq	%xmm3, %xmm2
	movdqu	%xmm2, 96(%rbx)
	movaps	.LCPI2_3(%rip), %xmm0   # xmm0 = [18446744073709551104,18446744073709551104]
	movups	%xmm0, 48(%rbx)
	movups	%xmm1, 112(%rbx)
.LBB2_3:
	cmpq	$0, stroke_path(%rip)
	jne	.LBB2_5
# BB#4:
	movq	$stroke_path_body, stroke_path(%rip)
	leaq	8(%r14), %rsi
	movl	$stroke_path_body, %edi
	callq	gx_path_init
.LBB2_5:
	movl	%ebp, %edi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	stroke_add
	movl	$stroke_path_body, %eax
	cmpq	%rax, stroke_path(%rip)
	jne	.LBB2_7
# BB#6:
	movq	312(%r14), %rsi
	movl	$stroke_path_body, %edi
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	gx_fill_path
	movq	stroke_path(%rip), %rdi
	callq	gx_path_release
	movq	$0, stroke_path(%rip)
.LBB2_7:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	stroke_fill, .Lfunc_end2-stroke_fill
	.cfi_endproc

	.globl	gx_stroke_add
	.p2align	4, 0x90
	.type	gx_stroke_add,@function
gx_stroke_add:                          # @gx_stroke_add
	.cfi_startproc
# BB#0:
	movq	%rsi, stroke_path(%rip)
	movl	$stroke_add, %esi
	jmp	stroke                  # TAILCALL
.Lfunc_end3:
	.size	gx_stroke_add, .Lfunc_end3-gx_stroke_add
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	stroke_add
	.p2align	4, 0x90
	.type	stroke_add,@function
stroke_add:                             # @stroke_add
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 64
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	stroke_path(%rip), %r15
	testq	%r15, %r15
	je	.LBB4_1
# BB#2:
	cmpl	$0, 160(%rbx)
	je	.LBB4_4
# BB#3:
	movq	(%rbx), %rax
	movq	128(%rbx), %rcx
	movq	136(%rbx), %rdx
	movq	%rax, %rsi
	subq	%rcx, %rsi
	movq	%rsi, 16(%rbx)
	movq	8(%rbx), %rbp
	movq	%rbp, %rsi
	subq	%rdx, %rsi
	movq	%rsi, 24(%rbx)
	addq	%rcx, %rax
	movq	%rax, 32(%rbx)
	addq	%rdx, %rbp
	movq	%rbp, 40(%rbx)
	movq	64(%rbx), %rax
	leaq	(%rax,%rcx), %rsi
	movq	%rsi, 80(%rbx)
	movq	72(%rbx), %rsi
	leaq	(%rsi,%rdx), %rbp
	movq	%rbp, 88(%rbx)
	subq	%rcx, %rax
	movq	%rax, 96(%rbx)
	subq	%rdx, %rsi
	movq	%rsi, 104(%rbx)
	movdqu	144(%rbx), %xmm0
	pxor	%xmm1, %xmm1
	psubq	%xmm0, %xmm1
	movdqu	%xmm1, 48(%rbx)
	movdqu	%xmm0, 112(%rbx)
.LBB4_4:
	testl	%edi, %edi
	je	.LBB4_5
.LBB4_13:                               # %.thread
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rdx
	movq	%r15, %rdi
	callq	gx_path_add_point
	testl	%eax, %eax
	js	.LBB4_57
# BB#14:
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rdx
.LBB4_15:                               # %add_capped.exit188
	movq	%r15, %rdi
	callq	gx_path_add_line
	testl	%eax, %eax
	js	.LBB4_57
.LBB4_17:                               # %add_capped.exit188.add_capped.exit188.thread195_crit_edge
	movq	280(%r12), %rax
	testq	%r14, %r14
	jne	.LBB4_19
	jmp	.LBB4_43
.LBB4_1:
	xorl	%eax, %eax
	jmp	.LBB4_57
.LBB4_5:
	movq	280(%r12), %rax
	movl	4(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB4_13
# BB#6:
	cmpl	$2, %ecx
	je	.LBB4_11
# BB#7:
	cmpl	$1, %ecx
	jne	.LBB4_18
# BB#8:
	movq	48(%rbx), %r13
	addq	(%rbx), %r13
	movq	56(%rbx), %rbp
	addq	8(%rbx), %rbp
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rdx
	movq	%r15, %rdi
	callq	gx_path_add_point
	testl	%eax, %eax
	js	.LBB4_57
# BB#9:
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rdx
	movq	48(%rbx), %r9
	addq	%rsi, %r9
	movq	56(%rbx), %rax
	addq	%rdx, %rax
	movq	%rax, (%rsp)
	movq	%r15, %rdi
	movq	%r13, %rcx
	movq	%rbp, %r8
	callq	gx_path_add_arc
	testl	%eax, %eax
	js	.LBB4_57
# BB#10:
	movq	32(%rbx), %rcx
	movq	40(%rbx), %r8
	movq	48(%rbx), %r9
	addq	%rcx, %r9
	movq	56(%rbx), %rax
	addq	%r8, %rax
	movq	%rax, (%rsp)
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	gx_path_add_arc
	testl	%eax, %eax
	jns	.LBB4_17
	jmp	.LBB4_57
.LBB4_18:                               # %add_capped.exit188.thread195
	testq	%r14, %r14
	je	.LBB4_43
.LBB4_19:
	movl	8(%rax), %ebp
	cmpl	$1, %ebp
	je	.LBB4_46
# BB#20:
	cmpl	$0, 160(%r14)
	jne	.LBB4_51
# BB#21:
	cvtsi2ssq	128(%rbx), %xmm4
	cvtsi2ssq	136(%rbx), %xmm5
	movq	112(%rbx), %r9
	cvtsi2ssq	%r9, %xmm10
	movq	120(%rbx), %r8
	cvtsi2ssq	%r8, %xmm9
	cvtsi2ssq	128(%r14), %xmm11
	cvtsi2ssq	136(%r14), %xmm12
	movaps	%xmm5, %xmm0
	mulss	%xmm10, %xmm0
	movaps	%xmm4, %xmm6
	mulss	%xmm9, %xmm6
	ucomiss	%xmm6, %xmm0
	setae	%dl
	mulss	%xmm11, %xmm5
	mulss	%xmm12, %xmm4
	ucomiss	%xmm4, %xmm5
	setae	%cl
	leaq	32(%r14), %r13
	leaq	16(%r14), %rsi
	xorb	%dl, %cl
	cmovneq	%rsi, %r13
	movq	80(%rbx), %rsi
	movq	88(%rbx), %rdx
	movq	96(%rbx), %r10
	movq	104(%rbx), %r12
	testl	%ebp, %ebp
	je	.LBB4_23
# BB#22:
	movq	%r10, %rbx
.LBB4_40:
	movq	%r15, %rdi
	callq	gx_path_add_line
	testl	%eax, %eax
	js	.LBB4_57
# BB#41:
	movq	(%r13), %rsi
	movq	8(%r13), %rdx
	movq	%r15, %rdi
	callq	gx_path_add_line
	testl	%eax, %eax
	js	.LBB4_57
# BB#42:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	jmp	.LBB4_54
.LBB4_43:                               # %.sink.split
	movl	4(%rax), %eax
	testl	%eax, %eax
	je	.LBB4_51
# BB#44:                                # %.sink.split
	cmpl	$2, %eax
	je	.LBB4_49
# BB#45:                                # %.sink.split
	cmpl	$1, %eax
	jne	.LBB4_56
.LBB4_46:                               # %.sink.split.thread
	movq	112(%rbx), %r14
	addq	64(%rbx), %r14
	movq	120(%rbx), %rbp
	addq	72(%rbx), %rbp
	movq	80(%rbx), %rsi
	movq	88(%rbx), %rdx
	movq	%r15, %rdi
	callq	gx_path_add_line
	testl	%eax, %eax
	js	.LBB4_57
# BB#47:
	movq	80(%rbx), %rsi
	movq	88(%rbx), %rdx
	movq	112(%rbx), %r9
	addq	%rsi, %r9
	movq	120(%rbx), %rax
	addq	%rdx, %rax
	movq	%rax, (%rsp)
	movq	%r15, %rdi
	movq	%r14, %rcx
	movq	%rbp, %r8
	callq	gx_path_add_arc
	testl	%eax, %eax
	js	.LBB4_57
# BB#48:
	movq	96(%rbx), %rcx
	movq	104(%rbx), %r8
	movq	112(%rbx), %r9
	addq	%rcx, %r9
	movq	120(%rbx), %rax
	addq	%r8, %rax
	movq	%rax, (%rsp)
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	callq	gx_path_add_arc
	testl	%eax, %eax
	jns	.LBB4_56
	jmp	.LBB4_57
.LBB4_51:                               # %.sink.split.thread197
	movq	80(%rbx), %rsi
	movq	88(%rbx), %rdx
	movq	%r15, %rdi
	callq	gx_path_add_line
	testl	%eax, %eax
	js	.LBB4_57
# BB#52:
	movq	96(%rbx), %rsi
	movq	104(%rbx), %rdx
	jmp	.LBB4_53
.LBB4_11:
	movq	48(%rbx), %rsi
	movq	56(%rbx), %rdx
	addq	16(%rbx), %rsi
	addq	24(%rbx), %rdx
	movq	%r15, %rdi
	callq	gx_path_add_point
	testl	%eax, %eax
	js	.LBB4_57
# BB#12:
	movq	48(%rbx), %rsi
	movq	56(%rbx), %rdx
	addq	32(%rbx), %rsi
	addq	40(%rbx), %rdx
	jmp	.LBB4_15
.LBB4_49:
	movq	112(%rbx), %rsi
	movq	120(%rbx), %rdx
	addq	80(%rbx), %rsi
	addq	88(%rbx), %rdx
	movq	%r15, %rdi
	callq	gx_path_add_line
	testl	%eax, %eax
	js	.LBB4_57
# BB#50:
	movq	112(%rbx), %rsi
	movq	120(%rbx), %rdx
	addq	96(%rbx), %rsi
	addq	104(%rbx), %rdx
.LBB4_53:                               # %add_capped.exit
	movq	%r15, %rdi
.LBB4_54:                               # %add_capped.exit
	callq	gx_path_add_line
	testl	%eax, %eax
	js	.LBB4_57
.LBB4_56:                               # %add_capped.exit.thread198
	movq	%r15, %rdi
	callq	gx_path_close_subpath
	movl	%eax, %ecx
	sarl	$31, %ecx
	andl	%eax, %ecx
	movl	%ecx, %eax
.LBB4_57:                               # %add_capped.exit188.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_23:
	movq	112(%r14), %rbx
	movq	120(%r14), %rbp
	movss	16(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movq	%r9, %rdi
	negq	%rdi
	cmovlq	%r9, %rdi
	movq	%rbx, %rcx
	negq	%rcx
	testq	%r9, %r9
	cmovnsq	%rbx, %rcx
	movq	%r8, %rbx
	negq	%rbx
	cmovlq	%r8, %rbx
	movq	%rbp, %rax
	negq	%rax
	testq	%r8, %r8
	cmovnsq	%rbp, %rax
	cvtsi2ssq	%rdi, %xmm3
	cvtsi2ssq	%rbx, %xmm7
	cvtsi2ssq	%rcx, %xmm1
	cvtsi2ssq	%rax, %xmm2
	movaps	%xmm7, %xmm6
	mulss	%xmm1, %xmm6
	movaps	%xmm3, %xmm0
	mulss	%xmm2, %xmm0
	subss	%xmm0, %xmm6
	mulss	%xmm1, %xmm3
	mulss	%xmm2, %xmm7
	addss	%xmm3, %xmm7
	testq	%rcx, %rcx
	js	.LBB4_28
# BB#24:
	testq	%rax, %rax
	js	.LBB4_26
# BB#25:
	movaps	.LCPI4_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm6, %xmm0
	xorps	%xmm1, %xmm1
	ucomiss	%xmm7, %xmm1
	cmpltss	%xmm6, %xmm1
	movaps	%xmm1, %xmm2
	andnps	%xmm6, %xmm2
	andps	%xmm0, %xmm1
	orps	%xmm2, %xmm1
	movaps	%xmm1, %xmm6
	ja	.LBB4_32
	jmp	.LBB4_33
.LBB4_28:
	testq	%rax, %rax
	js	.LBB4_31
# BB#29:
	xorps	%xmm0, %xmm0
	ucomiss	%xmm7, %xmm0
	jbe	.LBB4_33
# BB#30:
	movaps	.LCPI4_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm6
	xorps	%xmm0, %xmm7
	jmp	.LBB4_33
.LBB4_26:
	xorps	%xmm0, %xmm0
	ucomiss	%xmm7, %xmm0
	ja	.LBB4_32
# BB#27:
	xorps	.LCPI4_0(%rip), %xmm6
	jmp	.LBB4_33
.LBB4_31:
	movaps	.LCPI4_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm6, %xmm0
	xorps	%xmm1, %xmm1
	movaps	%xmm6, %xmm2
	cmpltss	%xmm1, %xmm2
	movaps	%xmm2, %xmm3
	andnps	%xmm6, %xmm3
	andps	%xmm0, %xmm2
	orps	%xmm3, %xmm2
	movaps	%xmm2, %xmm6
	ucomiss	%xmm7, %xmm1
	jbe	.LBB4_33
.LBB4_32:
	xorps	.LCPI4_0(%rip), %xmm7
.LBB4_33:
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm6
	mulss	%xmm8, %xmm7
	movq	%r10, %rbx
	jae	.LBB4_34
# BB#35:
	ucomiss	%xmm0, %xmm8
	jbe	.LBB4_36
	jmp	.LBB4_37
.LBB4_34:
	ucomiss	%xmm0, %xmm8
	jbe	.LBB4_40
.LBB4_36:
	ucomiss	%xmm7, %xmm6
	jb	.LBB4_40
.LBB4_37:
	ucomiss	%xmm4, %xmm5
	setae	%al
	xorps	%xmm5, %xmm5
	cvtsi2ssq	48(%r14), %xmm5
	xorps	%xmm4, %xmm4
	cvtsi2ssq	56(%r14), %xmm4
	mulss	%xmm5, %xmm12
	mulss	%xmm4, %xmm11
	ucomiss	%xmm11, %xmm12
	setae	%cl
	xorb	%al, %cl
	je	.LBB4_38
# BB#39:
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rsi, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rdx, %xmm1
	xorps	%xmm3, %xmm3
	cvtsi2ssq	(%r13), %xmm3
	xorps	%xmm6, %xmm6
	cvtsi2ssq	8(%r13), %xmm6
	movaps	%xmm10, %xmm2
	mulss	%xmm4, %xmm2
	movaps	%xmm5, %xmm7
	mulss	%xmm9, %xmm7
	subss	%xmm7, %xmm2
	cvtss2sd	%xmm2, %xmm2
	mulss	%xmm9, %xmm0
	mulss	%xmm10, %xmm1
	subss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	mulss	%xmm4, %xmm3
	mulss	%xmm5, %xmm6
	subss	%xmm6, %xmm3
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm3, %xmm1
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm10, %xmm3
	mulsd	%xmm1, %xmm3
	cvtss2sd	%xmm5, %xmm5
	mulsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm3
	divsd	%xmm2, %xmm3
	cvttsd2si	%xmm3, %rsi
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm9, %xmm3
	mulsd	%xmm1, %xmm3
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm4, %xmm1
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm3
	divsd	%xmm2, %xmm3
	cvttsd2si	%xmm3, %rdx
	jmp	.LBB4_40
.LBB4_38:
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rbx, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%r12, %xmm1
	xorps	%xmm3, %xmm3
	cvtsi2ssq	(%r13), %xmm3
	xorps	%xmm6, %xmm6
	cvtsi2ssq	8(%r13), %xmm6
	movaps	%xmm10, %xmm2
	mulss	%xmm4, %xmm2
	movaps	%xmm5, %xmm7
	mulss	%xmm9, %xmm7
	subss	%xmm7, %xmm2
	cvtss2sd	%xmm2, %xmm2
	mulss	%xmm9, %xmm0
	mulss	%xmm10, %xmm1
	subss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	mulss	%xmm4, %xmm3
	mulss	%xmm5, %xmm6
	subss	%xmm6, %xmm3
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm3, %xmm1
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm10, %xmm3
	mulsd	%xmm1, %xmm3
	cvtss2sd	%xmm5, %xmm5
	mulsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm3
	divsd	%xmm2, %xmm3
	cvttsd2si	%xmm3, %rbx
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm9, %xmm3
	mulsd	%xmm1, %xmm3
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm4, %xmm1
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm3
	divsd	%xmm2, %xmm3
	cvttsd2si	%xmm3, %r12
	jmp	.LBB4_40
.Lfunc_end4:
	.size	stroke_add, .Lfunc_end4-stroke_add
	.cfi_endproc

	.globl	compute_caps
	.p2align	4, 0x90
	.type	compute_caps,@function
compute_caps:                           # @compute_caps
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	128(%rdi), %rcx
	movq	136(%rdi), %r8
	movq	%rax, %rsi
	subq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
	movq	8(%rdi), %rdx
	movq	%rdx, %rsi
	subq	%r8, %rsi
	movq	%rsi, 24(%rdi)
	addq	%rcx, %rax
	movq	%rax, 32(%rdi)
	addq	%r8, %rdx
	movq	%rdx, 40(%rdi)
	movq	64(%rdi), %rax
	leaq	(%rax,%rcx), %rdx
	movq	%rdx, 80(%rdi)
	movq	72(%rdi), %rdx
	leaq	(%rdx,%r8), %rsi
	movq	%rsi, 88(%rdi)
	subq	%rcx, %rax
	movq	%rax, 96(%rdi)
	subq	%r8, %rdx
	movq	%rdx, 104(%rdi)
	movdqu	144(%rdi), %xmm0
	pxor	%xmm1, %xmm1
	psubq	%xmm0, %xmm1
	movdqu	%xmm1, 48(%rdi)
	movdqu	%xmm0, 112(%rdi)
	retq
.Lfunc_end5:
	.size	compute_caps, .Lfunc_end5-compute_caps
	.cfi_endproc

	.globl	line_intersect
	.p2align	4, 0x90
	.type	line_intersect,@function
line_intersect:                         # @line_intersect
	.cfi_startproc
# BB#0:
	cvtsi2ssq	(%rdi), %xmm1
	cvtsi2ssq	8(%rdi), %xmm9
	cvtsi2ssq	(%rsi), %xmm7
	cvtsi2ssq	8(%rsi), %xmm6
	cvtsi2ssq	(%rdx), %xmm3
	cvtsi2ssq	8(%rdx), %xmm5
	cvtsi2ssq	(%rcx), %xmm4
	cvtsi2ssq	8(%rcx), %xmm8
	mulss	%xmm7, %xmm9
	cvtss2sd	%xmm7, %xmm0
	mulss	%xmm8, %xmm7
	mulss	%xmm6, %xmm1
	cvtss2sd	%xmm6, %xmm2
	mulss	%xmm4, %xmm6
	subss	%xmm6, %xmm7
	xorps	%xmm6, %xmm6
	cvtss2sd	%xmm7, %xmm6
	subss	%xmm9, %xmm1
	cvtss2sd	%xmm1, %xmm1
	mulss	%xmm8, %xmm3
	mulss	%xmm4, %xmm5
	subss	%xmm5, %xmm3
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm3, %xmm0
	cvtss2sd	%xmm4, %xmm4
	mulsd	%xmm1, %xmm4
	subsd	%xmm4, %xmm0
	divsd	%xmm6, %xmm0
	cvttsd2si	%xmm0, %rax
	movq	%rax, (%r8)
	mulsd	%xmm3, %xmm2
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm8, %xmm0
	mulsd	%xmm1, %xmm0
	subsd	%xmm0, %xmm2
	divsd	%xmm6, %xmm2
	cvttsd2si	%xmm2, %rax
	movq	%rax, 8(%r8)
	retq
.Lfunc_end6:
	.size	line_intersect, .Lfunc_end6-line_intersect
	.cfi_endproc

	.globl	add_capped
	.p2align	4, 0x90
	.type	add_capped,@function
add_capped:                             # @add_capped
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 48
.Lcfi45:
	.cfi_offset %rbx, -40
.Lcfi46:
	.cfi_offset %r12, -32
.Lcfi47:
	.cfi_offset %r14, -24
.Lcfi48:
	.cfi_offset %r15, -16
	movq	%rcx, %rbx
	movq	%rdx, %rcx
	movq	%rdi, %r14
	testl	%esi, %esi
	je	.LBB7_9
# BB#1:
	cmpl	$2, %esi
	je	.LBB7_7
# BB#2:
	cmpl	$1, %esi
                                        # implicit-def: %EAX
	jne	.LBB7_6
# BB#3:
	movq	48(%rbx), %r15
	addq	(%rbx), %r15
	movq	56(%rbx), %r12
	addq	8(%rbx), %r12
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rdx
	movq	%r14, %rdi
	callq	*%rcx
	testl	%eax, %eax
	js	.LBB7_6
# BB#4:
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rdx
	movq	48(%rbx), %r9
	addq	%rsi, %r9
	movq	56(%rbx), %rax
	addq	%rdx, %rax
	movq	%rax, (%rsp)
	movq	%r14, %rdi
	movq	%r15, %rcx
	movq	%r12, %r8
	callq	gx_path_add_arc
	testl	%eax, %eax
	js	.LBB7_6
# BB#5:
	movq	32(%rbx), %rcx
	movq	40(%rbx), %r8
	movq	48(%rbx), %r9
	addq	%rcx, %r9
	movq	56(%rbx), %rax
	addq	%r8, %rax
	movq	%rax, (%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	gx_path_add_arc
	jmp	.LBB7_6
.LBB7_9:
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rdx
	movq	%r14, %rdi
	callq	*%rcx
	testl	%eax, %eax
	js	.LBB7_6
# BB#10:
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rdx
	jmp	.LBB7_11
.LBB7_7:
	movq	48(%rbx), %rsi
	movq	56(%rbx), %rdx
	addq	16(%rbx), %rsi
	addq	24(%rbx), %rdx
	movq	%r14, %rdi
	callq	*%rcx
	testl	%eax, %eax
	js	.LBB7_6
# BB#8:
	movq	48(%rbx), %rsi
	movq	56(%rbx), %rdx
	addq	32(%rbx), %rsi
	addq	40(%rbx), %rdx
.LBB7_11:
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	gx_path_add_line        # TAILCALL
.LBB7_6:                                # %.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	add_capped, .Lfunc_end7-add_capped
	.cfi_endproc

	.type	stroke_path,@object     # @stroke_path
	.comm	stroke_path,8,8
	.type	stroke_path_body,@object # @stroke_path_body
	.comm	stroke_path_body,144,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
