	.text
	.file	"BZip2Decoder.bc"
	.globl	_ZN9NCompress6NBZip26CState5AllocEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip26CState5AllocEv,@function
_ZN9NCompress6NBZip26CState5AllocEv:    # @_ZN9NCompress6NBZip26CState5AllocEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB0_2
# BB#1:
	movl	$3601024, %edi          # imm = 0x36F280
	callq	BigAlloc
	movq	%rax, (%rbx)
.LBB0_2:
	testq	%rax, %rax
	setne	%al
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN9NCompress6NBZip26CState5AllocEv, .Lfunc_end0-_ZN9NCompress6NBZip26CState5AllocEv
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip26CState4FreeEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip26CState4FreeEv,@function
_ZN9NCompress6NBZip26CState4FreeEv:     # @_ZN9NCompress6NBZip26CState4FreeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	callq	BigFree
	movq	$0, (%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN9NCompress6NBZip26CState4FreeEv, .Lfunc_end1-_ZN9NCompress6NBZip26CState4FreeEv
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CDecoder8ReadBitsEj
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder8ReadBitsEj,@function
_ZN9NCompress6NBZip28CDecoder8ReadBitsEj: # @_ZN9NCompress6NBZip28CDecoder8ReadBitsEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 48
.Lcfi9:
	.cfi_offset %rbx, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	336(%rbx), %edx
	movl	340(%rbx), %ebp
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebp, %r14d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r14d
	andl	$16777215, %r14d        # imm = 0xFFFFFF
	movl	$24, %ecx
	subl	%esi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r14d
	addl	%esi, %edx
	movl	%edx, 336(%rbx)
	cmpl	$8, %edx
	jb	.LBB2_6
# BB#1:                                 # %.lr.ph.i.i.i
	leaq	344(%rbx), %r15
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	shll	$8, %ebp
	movq	344(%rbx), %rax
	cmpq	352(%rbx), %rax
	jae	.LBB2_3
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r15)
	movzbl	(%rax), %eax
	jmp	.LBB2_5
	.p2align	4, 0x90
.LBB2_3:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	336(%rbx), %edx
.LBB2_5:                                # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 340(%rbx)
	addl	$-8, %edx
	movl	%edx, 336(%rbx)
	cmpl	$7, %edx
	ja	.LBB2_2
.LBB2_6:                                # %_ZN5NBitm8CDecoderI9CInBufferE8ReadBitsEj.exit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN9NCompress6NBZip28CDecoder8ReadBitsEj, .Lfunc_end2-_ZN9NCompress6NBZip28CDecoder8ReadBitsEj
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CDecoder8ReadByteEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder8ReadByteEv,@function
_ZN9NCompress6NBZip28CDecoder8ReadByteEv: # @_ZN9NCompress6NBZip28CDecoder8ReadByteEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	336(%rbx), %eax
	movl	340(%rbx), %ebp
	movl	$8, %ecx
	subl	%eax, %ecx
	movl	%ebp, %r14d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r14d
	shrl	$16, %r14d
	leal	8(%rax), %ecx
	movl	%ecx, 336(%rbx)
	cmpl	$-9, %eax
	ja	.LBB3_6
# BB#1:                                 # %.lr.ph.i.i.i.i
	leaq	344(%rbx), %r15
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	shll	$8, %ebp
	movq	344(%rbx), %rax
	cmpq	352(%rbx), %rax
	jae	.LBB3_3
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=1
	leaq	1(%rax), %rdx
	movq	%rdx, (%r15)
	movzbl	(%rax), %eax
	jmp	.LBB3_5
	.p2align	4, 0x90
.LBB3_3:                                #   in Loop: Header=BB3_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	336(%rbx), %ecx
.LBB3_5:                                # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 340(%rbx)
	addl	$-8, %ecx
	movl	%ecx, 336(%rbx)
	cmpl	$7, %ecx
	ja	.LBB3_2
.LBB3_6:                                # %_ZN9NCompress6NBZip28CDecoder8ReadBitsEj.exit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN9NCompress6NBZip28CDecoder8ReadByteEv, .Lfunc_end3-_ZN9NCompress6NBZip28CDecoder8ReadByteEv
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CDecoder7ReadBitEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder7ReadBitEv,@function
_ZN9NCompress6NBZip28CDecoder7ReadBitEv: # @_ZN9NCompress6NBZip28CDecoder7ReadBitEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -48
.Lcfi28:
	.cfi_offset %r12, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	336(%rbx), %ecx
	movl	340(%rbx), %r15d
	movl	$8, %r14d
	subl	%ecx, %r14d
	incl	%ecx
	movl	%ecx, 336(%rbx)
	cmpl	$8, %ecx
	jb	.LBB4_6
# BB#1:                                 # %.lr.ph.i.i.i.i
	leaq	344(%rbx), %r12
	movl	%r15d, %ebp
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	shll	$8, %ebp
	movq	344(%rbx), %rax
	cmpq	352(%rbx), %rax
	jae	.LBB4_3
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	leaq	1(%rax), %rdx
	movq	%rdx, (%r12)
	movzbl	(%rax), %eax
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_3:                                #   in Loop: Header=BB4_2 Depth=1
	movq	%r12, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	336(%rbx), %ecx
.LBB4_5:                                # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i
                                        #   in Loop: Header=BB4_2 Depth=1
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 340(%rbx)
	addl	$-8, %ecx
	movl	%ecx, 336(%rbx)
	cmpl	$7, %ecx
	ja	.LBB4_2
.LBB4_6:                                # %_ZN9NCompress6NBZip28CDecoder8ReadBitsEj.exit
	movl	$8388608, %eax          # imm = 0x800000
	movl	%r14d, %ecx
	shll	%cl, %eax
	testl	%r15d, %eax
	setne	%al
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN9NCompress6NBZip28CDecoder7ReadBitEv, .Lfunc_end4-_ZN9NCompress6NBZip28CDecoder7ReadBitEv
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CDecoder7ReadCrcEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder7ReadCrcEv,@function
_ZN9NCompress6NBZip28CDecoder7ReadCrcEv: # @_ZN9NCompress6NBZip28CDecoder7ReadCrcEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	leaq	344(%r13), %r14
	movl	336(%r13), %edx
	movl	340(%r13), %ebp
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebp, %r12d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r12d
	leal	8(%rdx), %esi
	movl	%esi, 336(%r13)
	cmpl	$-9, %edx
	ja	.LBB5_6
	.p2align	4, 0x90
.LBB5_1:                                # %.lr.ph.i.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %ebx
	shll	$8, %ebx
	movq	344(%r13), %rax
	cmpq	352(%r13), %rax
	jae	.LBB5_2
# BB#3:                                 #   in Loop: Header=BB5_1 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	movl	%esi, %edx
	jmp	.LBB5_4
	.p2align	4, 0x90
.LBB5_2:                                #   in Loop: Header=BB5_1 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	336(%r13), %edx
.LBB5_4:                                # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i.i
                                        #   in Loop: Header=BB5_1 Depth=1
	movzbl	%al, %ebp
	orl	%ebx, %ebp
	movl	%ebp, 340(%r13)
	leal	-8(%rdx), %esi
	movl	%esi, 336(%r13)
	cmpl	$7, %esi
	ja	.LBB5_1
# BB#5:                                 # %_ZN9NCompress6NBZip28CDecoder8ReadByteEv.exit.thread
	movl	$16, %ecx
	subl	%edx, %ecx
	movl	%ebp, %r15d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r15d
	movl	%edx, 336(%r13)
	jmp	.LBB5_7
.LBB5_6:                                # %_ZN9NCompress6NBZip28CDecoder8ReadByteEv.exit
	movl	%edx, %ecx
	negl	%ecx
	movl	%ebp, %r15d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r15d
	addl	$16, %edx
	movl	%edx, 336(%r13)
	cmpl	$-9, %esi
	ja	.LBB5_11
	.p2align	4, 0x90
.LBB5_7:                                # %.lr.ph.i.i.i.i.i.1
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %ebx
	shll	$8, %ebx
	movq	344(%r13), %rax
	cmpq	352(%r13), %rax
	jae	.LBB5_8
# BB#9:                                 #   in Loop: Header=BB5_7 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB5_10
	.p2align	4, 0x90
.LBB5_8:                                #   in Loop: Header=BB5_7 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	336(%r13), %edx
.LBB5_10:                               # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i.i.1
                                        #   in Loop: Header=BB5_7 Depth=1
	movzbl	%al, %ebp
	orl	%ebx, %ebp
	movl	%ebp, 340(%r13)
	addl	$-8, %edx
	movl	%edx, 336(%r13)
	cmpl	$7, %edx
	ja	.LBB5_7
.LBB5_11:                               # %_ZN9NCompress6NBZip28CDecoder8ReadByteEv.exit.1
	shrl	$8, %r15d
	andl	$65280, %r15d           # imm = 0xFF00
	andl	$16711680, %r12d        # imm = 0xFF0000
	orl	%r15d, %r12d
	movl	$8, %r15d
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebp, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebx
	shrl	$16, %ebx
	leal	8(%rdx), %esi
	movl	%esi, 336(%r13)
	cmpl	$-9, %edx
	ja	.LBB5_16
	.p2align	4, 0x90
.LBB5_12:                               # %.lr.ph.i.i.i.i.i.2
                                        # =>This Inner Loop Header: Depth=1
	shll	$8, %ebp
	movq	344(%r13), %rax
	cmpq	352(%r13), %rax
	jae	.LBB5_13
# BB#14:                                #   in Loop: Header=BB5_12 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB5_15
	.p2align	4, 0x90
.LBB5_13:                               #   in Loop: Header=BB5_12 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	336(%r13), %esi
.LBB5_15:                               # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i.i.2
                                        #   in Loop: Header=BB5_12 Depth=1
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 340(%r13)
	addl	$-8, %esi
	movl	%esi, 336(%r13)
	cmpl	$7, %esi
	ja	.LBB5_12
.LBB5_16:                               # %_ZN9NCompress6NBZip28CDecoder8ReadByteEv.exit.2
	movzbl	%bl, %ebx
	orl	%r12d, %ebx
	shll	$8, %ebx
	subl	%esi, %r15d
	movl	%ebp, %r12d
	movl	%r15d, %ecx
	shrl	%cl, %r12d
	shrl	$16, %r12d
	leal	8(%rsi), %ecx
	movl	%ecx, 336(%r13)
	cmpl	$-9, %esi
	ja	.LBB5_21
	.p2align	4, 0x90
.LBB5_17:                               # %.lr.ph.i.i.i.i.i.3
                                        # =>This Inner Loop Header: Depth=1
	shll	$8, %ebp
	movq	344(%r13), %rax
	cmpq	352(%r13), %rax
	jae	.LBB5_18
# BB#19:                                #   in Loop: Header=BB5_17 Depth=1
	leaq	1(%rax), %rdx
	movq	%rdx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB5_20
	.p2align	4, 0x90
.LBB5_18:                               #   in Loop: Header=BB5_17 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	336(%r13), %ecx
.LBB5_20:                               # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i.i.3
                                        #   in Loop: Header=BB5_17 Depth=1
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 340(%r13)
	addl	$-8, %ecx
	movl	%ecx, 336(%r13)
	cmpl	$7, %ecx
	ja	.LBB5_17
.LBB5_21:                               # %_ZN9NCompress6NBZip28CDecoder8ReadByteEv.exit.3
	movzbl	%r12b, %eax
	orl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN9NCompress6NBZip28CDecoder7ReadCrcEv, .Lfunc_end5-_ZN9NCompress6NBZip28CDecoder7ReadCrcEv
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CDecoderC2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoderC2Ev,@function
_ZN9NCompress6NBZip28CDecoderC2Ev:      # @_ZN9NCompress6NBZip28CDecoderC2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 48
.Lcfi50:
	.cfi_offset %rbx, -40
.Lcfi51:
	.cfi_offset %r12, -32
.Lcfi52:
	.cfi_offset %r14, -24
.Lcfi53:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$0, 16(%rbx)
	movl	$_ZTVN9NCompress6NBZip28CDecoderE+104, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NBZip28CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$0, 24(%rbx)
	movl	$0, 32(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 64(%rbx)
	leaq	344(%rbx), %r15
.Ltmp0:
	movq	%r15, %rdi
	callq	_ZN9CInBufferC1Ev
.Ltmp1:
# BB#1:
	movl	$0, 28684(%rbx)
	leaq	28712(%rbx), %r12
	movl	$0, 28712(%rbx)
	leaq	28816(%rbx), %rdi
.Ltmp3:
	callq	CriticalSection_Init
.Ltmp4:
# BB#2:
	movl	$0, 28872(%rbx)
	movq	$0, 28696(%rbx)
	movl	$0, 28704(%rbx)
	movl	$1, 28856(%rbx)
	movb	$1, 28680(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB6_3:
.Ltmp5:
	movq	%rax, %r14
.Ltmp6:
	movq	%r12, %rdi
	callq	Event_Close
.Ltmp7:
# BB#4:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit
.Ltmp8:
	movq	%r15, %rdi
	callq	_ZN9CInBuffer4FreeEv
.Ltmp9:
# BB#5:
	movq	368(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_11
# BB#6:
	movq	(%rdi), %rax
.Ltmp14:
	callq	*16(%rax)
.Ltmp15:
	jmp	.LBB6_11
.LBB6_7:
.Ltmp10:
	movq	%rax, %r14
	movq	368(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_19
# BB#8:
	movq	(%rdi), %rax
.Ltmp11:
	callq	*16(%rax)
.Ltmp12:
	jmp	.LBB6_19
.LBB6_9:
.Ltmp13:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_10:
.Ltmp2:
	movq	%rax, %r14
.LBB6_11:                               # %_ZN5NBitm8CDecoderI9CInBufferED2Ev.exit
	leaq	24(%rbx), %rdi
.Ltmp16:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp17:
# BB#12:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_14
# BB#13:
	movq	(%rdi), %rax
.Ltmp22:
	callq	*16(%rax)
.Ltmp23:
.LBB6_14:                               # %_ZN10COutBufferD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_15:
.Ltmp18:
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_19
# BB#16:
	movq	(%rdi), %rax
.Ltmp19:
	callq	*16(%rax)
.Ltmp20:
	jmp	.LBB6_19
.LBB6_17:
.Ltmp21:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_18:
.Ltmp24:
	movq	%rax, %r14
.LBB6_19:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN9NCompress6NBZip28CDecoderC2Ev, .Lfunc_end6-_ZN9NCompress6NBZip28CDecoderC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp24-.Lfunc_begin0   #     jumps to .Ltmp24
	.byte	1                       #   On action: 1
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	1                       #   On action: 1
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp24-.Lfunc_begin0   #     jumps to .Ltmp24
	.byte	1                       #   On action: 1
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin0   #     jumps to .Ltmp18
	.byte	1                       #   On action: 1
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin0   #     jumps to .Ltmp24
	.byte	1                       #   On action: 1
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp19-.Ltmp23         #   Call between .Ltmp23 and .Ltmp19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin0   #     jumps to .Ltmp21
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end7:
	.size	__clang_call_terminate, .Lfunc_end7-__clang_call_terminate

	.text
	.globl	_ZN9NCompress6NBZip28CDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoderD2Ev,@function
_ZN9NCompress6NBZip28CDecoderD2Ev:      # @_ZN9NCompress6NBZip28CDecoderD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -24
.Lcfi58:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress6NBZip28CDecoderE+104, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NBZip28CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
.Ltmp25:
	callq	_ZN9NCompress6NBZip28CDecoder4FreeEv
.Ltmp26:
# BB#1:
	leaq	28872(%rbx), %rdi
.Ltmp30:
	callq	Event_Close
.Ltmp31:
# BB#2:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit
	leaq	28816(%rbx), %rdi
	callq	pthread_mutex_destroy
	leaq	28712(%rbx), %rdi
.Ltmp35:
	callq	Event_Close
.Ltmp36:
# BB#3:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit7
	leaq	344(%rbx), %rdi
.Ltmp46:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp47:
# BB#4:
	movq	368(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp52:
	callq	*16(%rax)
.Ltmp53:
.LBB8_6:                                # %_ZN5NBitm8CDecoderI9CInBufferED2Ev.exit
	leaq	24(%rbx), %rdi
.Ltmp64:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp65:
# BB#7:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_9
# BB#8:
	movq	(%rdi), %rax
.Ltmp70:
	callq	*16(%rax)
.Ltmp71:
.LBB8_9:                                # %_ZN10COutBufferD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB8_31:
.Ltmp72:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_19:
.Ltmp54:
	movq	%rax, %r14
	jmp	.LBB8_24
.LBB8_13:
.Ltmp66:
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_27
# BB#14:
	movq	(%rdi), %rax
.Ltmp67:
	callq	*16(%rax)
.Ltmp68:
	jmp	.LBB8_27
.LBB8_15:
.Ltmp69:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_10:
.Ltmp48:
	movq	%rax, %r14
	movq	368(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_24
# BB#11:
	movq	(%rdi), %rax
.Ltmp49:
	callq	*16(%rax)
.Ltmp50:
	jmp	.LBB8_24
.LBB8_12:
.Ltmp51:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_20:
.Ltmp37:
	movq	%rax, %r14
	jmp	.LBB8_21
.LBB8_17:
.Ltmp32:
	movq	%rax, %r14
	jmp	.LBB8_18
.LBB8_16:
.Ltmp27:
	movq	%rax, %r14
	leaq	28872(%rbx), %rdi
.Ltmp28:
	callq	Event_Close
.Ltmp29:
.LBB8_18:                               # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit12
	leaq	28816(%rbx), %rdi
	callq	pthread_mutex_destroy
	leaq	28712(%rbx), %rdi
.Ltmp33:
	callq	Event_Close
.Ltmp34:
.LBB8_21:                               # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit14
	leaq	344(%rbx), %rdi
.Ltmp38:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp39:
# BB#22:
	movq	368(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_24
# BB#23:
	movq	(%rdi), %rax
.Ltmp44:
	callq	*16(%rax)
.Ltmp45:
.LBB8_24:                               # %_ZN5NBitm8CDecoderI9CInBufferED2Ev.exit19
	leaq	24(%rbx), %rdi
.Ltmp55:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp56:
# BB#25:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_27
# BB#26:
	movq	(%rdi), %rax
.Ltmp61:
	callq	*16(%rax)
.Ltmp62:
.LBB8_27:                               # %_ZN10COutBufferD2Ev.exit24
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_28:
.Ltmp40:
	movq	%rax, %r14
	movq	368(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_36
# BB#29:
	movq	(%rdi), %rax
.Ltmp41:
	callq	*16(%rax)
.Ltmp42:
	jmp	.LBB8_36
.LBB8_30:
.Ltmp43:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_32:
.Ltmp57:
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_36
# BB#33:
	movq	(%rdi), %rax
.Ltmp58:
	callq	*16(%rax)
.Ltmp59:
	jmp	.LBB8_36
.LBB8_34:
.Ltmp60:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_35:
.Ltmp63:
	movq	%rax, %r14
.LBB8_36:                               # %.body17
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN9NCompress6NBZip28CDecoderD2Ev, .Lfunc_end8-_ZN9NCompress6NBZip28CDecoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\363\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\352\001"              # Call site table length
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin1   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin1   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin1   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin1   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin1   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin1   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin1   #     jumps to .Ltmp72
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp67-.Ltmp71         #   Call between .Ltmp71 and .Ltmp67
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin1   #     jumps to .Ltmp69
	.byte	1                       #   On action: 1
	.long	.Ltmp49-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin1   #     jumps to .Ltmp51
	.byte	1                       #   On action: 1
	.long	.Ltmp28-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp34-.Ltmp28         #   Call between .Ltmp28 and .Ltmp34
	.long	.Ltmp63-.Lfunc_begin1   #     jumps to .Ltmp63
	.byte	1                       #   On action: 1
	.long	.Ltmp38-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin1   #     jumps to .Ltmp40
	.byte	1                       #   On action: 1
	.long	.Ltmp44-.Lfunc_begin1   # >> Call Site 13 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp63-.Lfunc_begin1   #     jumps to .Ltmp63
	.byte	1                       #   On action: 1
	.long	.Ltmp55-.Lfunc_begin1   # >> Call Site 14 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin1   #     jumps to .Ltmp57
	.byte	1                       #   On action: 1
	.long	.Ltmp61-.Lfunc_begin1   # >> Call Site 15 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin1   #     jumps to .Ltmp63
	.byte	1                       #   On action: 1
	.long	.Ltmp62-.Lfunc_begin1   # >> Call Site 16 <<
	.long	.Ltmp41-.Ltmp62         #   Call between .Ltmp62 and .Ltmp41
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin1   # >> Call Site 17 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin1   #     jumps to .Ltmp43
	.byte	1                       #   On action: 1
	.long	.Ltmp58-.Lfunc_begin1   # >> Call Site 18 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin1   #     jumps to .Ltmp60
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress6NBZip28CDecoder4FreeEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder4FreeEv,@function
_ZN9NCompress6NBZip28CDecoder4FreeEv:   # @_ZN9NCompress6NBZip28CDecoder4FreeEv
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi63:
	.cfi_def_cfa_offset 48
.Lcfi64:
	.cfi_offset %rbx, -40
.Lcfi65:
	.cfi_offset %r12, -32
.Lcfi66:
	.cfi_offset %r14, -24
.Lcfi67:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpq	$0, 28696(%r15)
	je	.LBB9_13
# BB#1:
	movb	$1, 28868(%r15)
	leaq	28712(%r15), %rdi
	callq	Event_Set
	cmpl	$0, 28856(%r15)
	movq	28696(%r15), %r12
	je	.LBB9_6
# BB#2:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_3:                                # =>This Inner Loop Header: Depth=1
	movl	%ebx, %ebx
	imulq	$608, %rbx, %rax        # imm = 0x260
	leaq	(%r12,%rax), %r14
	cmpb	$0, 28860(%r15)
	je	.LBB9_5
# BB#4:                                 #   in Loop: Header=BB9_3 Depth=1
	leaq	16(%r12,%rax), %rdi
	callq	Thread_Wait
.LBB9_5:                                #   in Loop: Header=BB9_3 Depth=1
	movq	(%r14), %rdi
	callq	BigFree
	movq	$0, (%r14)
	incl	%ebx
	movq	28696(%r15), %r12
	cmpl	28856(%r15), %ebx
	jb	.LBB9_3
.LBB9_6:                                # %._crit_edge
	testq	%r12, %r12
	je	.LBB9_12
# BB#7:
	leaq	-8(%r12), %r14
	movq	-8(%r12), %rax
	testq	%rax, %rax
	je	.LBB9_11
# BB#8:                                 # %.preheader10.preheader
	imulq	$608, %rax, %rbx        # imm = 0x260
	.p2align	4, 0x90
.LBB9_9:                                # %.preheader10
                                        # =>This Inner Loop Header: Depth=1
	leaq	-608(%r12,%rbx), %rdi
.Ltmp73:
	callq	_ZN9NCompress6NBZip26CStateD2Ev
.Ltmp74:
# BB#10:                                #   in Loop: Header=BB9_9 Depth=1
	addq	$-608, %rbx             # imm = 0xFDA0
	jne	.LBB9_9
.LBB9_11:                               # %.loopexit11
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB9_12:
	movq	$0, 28696(%r15)
.LBB9_13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB9_14:
.Ltmp75:
	movq	%rax, %r15
	cmpq	$608, %rbx              # imm = 0x260
	jne	.LBB9_16
	jmp	.LBB9_18
	.p2align	4, 0x90
.LBB9_17:                               #   in Loop: Header=BB9_16 Depth=1
	addq	$-608, %rbx             # imm = 0xFDA0
	cmpq	$608, %rbx              # imm = 0x260
	je	.LBB9_18
.LBB9_16:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leaq	-1216(%r12,%rbx), %rdi
.Ltmp76:
	callq	_ZN9NCompress6NBZip26CStateD2Ev
.Ltmp77:
	jmp	.LBB9_17
.LBB9_18:                               # %.loopexit
	movq	%r14, %rdi
	callq	_ZdaPv
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB9_19:
.Ltmp78:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN9NCompress6NBZip28CDecoder4FreeEv, .Lfunc_end9-_ZN9NCompress6NBZip28CDecoder4FreeEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp73-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp73
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp73-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin2   #     jumps to .Ltmp75
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin2   #     jumps to .Ltmp78
	.byte	1                       #   On action: 1
	.long	.Ltmp77-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end9-.Ltmp77     #   Call between .Ltmp77 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn8_N9NCompress6NBZip28CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip28CDecoderD1Ev,@function
_ZThn8_N9NCompress6NBZip28CDecoderD1Ev: # @_ZThn8_N9NCompress6NBZip28CDecoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress6NBZip28CDecoderD2Ev # TAILCALL
.Lfunc_end10:
	.size	_ZThn8_N9NCompress6NBZip28CDecoderD1Ev, .Lfunc_end10-_ZThn8_N9NCompress6NBZip28CDecoderD1Ev
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoderD0Ev,@function
_ZN9NCompress6NBZip28CDecoderD0Ev:      # @_ZN9NCompress6NBZip28CDecoderD0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi70:
	.cfi_def_cfa_offset 32
.Lcfi71:
	.cfi_offset %rbx, -24
.Lcfi72:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp79:
	callq	_ZN9NCompress6NBZip28CDecoderD2Ev
.Ltmp80:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB11_2:
.Ltmp81:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZN9NCompress6NBZip28CDecoderD0Ev, .Lfunc_end11-_ZN9NCompress6NBZip28CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp79-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin3   #     jumps to .Ltmp81
	.byte	0                       #   On action: cleanup
	.long	.Ltmp80-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Lfunc_end11-.Ltmp80    #   Call between .Ltmp80 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn8_N9NCompress6NBZip28CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip28CDecoderD0Ev,@function
_ZThn8_N9NCompress6NBZip28CDecoderD0Ev: # @_ZThn8_N9NCompress6NBZip28CDecoderD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi75:
	.cfi_def_cfa_offset 32
.Lcfi76:
	.cfi_offset %rbx, -24
.Lcfi77:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp82:
	movq	%rbx, %rdi
	callq	_ZN9NCompress6NBZip28CDecoderD2Ev
.Ltmp83:
# BB#1:                                 # %_ZN9NCompress6NBZip28CDecoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB12_2:
.Ltmp84:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZThn8_N9NCompress6NBZip28CDecoderD0Ev, .Lfunc_end12-_ZThn8_N9NCompress6NBZip28CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp82-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin4   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp83-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end12-.Ltmp83    #   Call between .Ltmp83 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN9NCompress6NBZip28CDecoder6CreateEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder6CreateEv,@function
_ZN9NCompress6NBZip28CDecoder6CreateEv: # @_ZN9NCompress6NBZip28CDecoder6CreateEv
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi82:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 64
.Lcfi85:
	.cfi_offset %rbx, -56
.Lcfi86:
	.cfi_offset %r12, -48
.Lcfi87:
	.cfi_offset %r13, -40
.Lcfi88:
	.cfi_offset %r14, -32
.Lcfi89:
	.cfi_offset %r15, -24
.Lcfi90:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, 28712(%r14)
	je	.LBB13_22
.LBB13_1:                               # %_ZN8NWindows16NSynchronization17CManualResetEvent18CreateIfNotCreatedEv.exit.thread
	cmpl	$0, 28872(%r14)
	je	.LBB13_23
.LBB13_2:                               # %_ZN8NWindows16NSynchronization17CManualResetEvent18CreateIfNotCreatedEv.exit41.thread
	cmpq	$0, 28696(%r14)
	je	.LBB13_4
# BB#3:
	movl	28704(%r14), %eax
	xorl	%ebp, %ebp
	cmpl	28856(%r14), %eax
	je	.LBB13_27
.LBB13_4:
	leaq	28856(%r14), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r14, %rdi
	callq	_ZN9NCompress6NBZip28CDecoder4FreeEv
	movl	28856(%r14), %ebp
	cmpq	$1, %rbp
	seta	28860(%r14)
	seta	%r13b
	movl	%ebp, 28704(%r14)
	imulq	$608, %rbp, %rbx        # imm = 0x260
	leaq	8(%rbx), %rdi
.Ltmp85:
	callq	_Znam
	movq	%rax, %r12
.Ltmp86:
# BB#5:
	movq	%rbp, (%r12)
	addq	$8, %r12
	testl	%ebp, %ebp
	je	.LBB13_24
# BB#6:
	addq	%r12, %rbx
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB13_7:                               # =>This Inner Loop Header: Depth=1
	movq	$0, (%rax)
	movl	$0, 24(%rax)
	movl	$0, 40(%rax)
	movl	$0, 144(%rax)
	movl	$0, 248(%rax)
	addq	$608, %rax              # imm = 0x260
	cmpq	%rbx, %rax
	jne	.LBB13_7
# BB#8:                                 # %.loopexit51
	testl	%ebp, %ebp
	movq	%r12, 28696(%r14)
	je	.LBB13_25
# BB#9:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	jmp	.LBB13_11
	.p2align	4, 0x90
.LBB13_10:                              # %.thread..lr.ph_crit_edge
                                        #   in Loop: Header=BB13_11 Depth=1
	movq	28696(%r14), %r12
	movzbl	28860(%r14), %r13d
.LBB13_11:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %eax
	imulq	$608, %rax, %r15        # imm = 0x260
	movq	%r14, 8(%r12,%r15)
	testb	%r13b, %r13b
	je	.LBB13_20
# BB#12:                                #   in Loop: Header=BB13_11 Depth=1
	cmpl	$0, 40(%r12,%r15)
	jne	.LBB13_14
# BB#13:                                # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit.i
                                        #   in Loop: Header=BB13_11 Depth=1
	leaq	40(%r12,%r15), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB13_26
.LBB13_14:                              # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit.thread.i
                                        #   in Loop: Header=BB13_11 Depth=1
	cmpl	$0, 144(%r12,%r15)
	jne	.LBB13_16
# BB#15:                                # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit32.i
                                        #   in Loop: Header=BB13_11 Depth=1
	leaq	144(%r12,%r15), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB13_26
.LBB13_16:                              # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit32.thread.i
                                        #   in Loop: Header=BB13_11 Depth=1
	cmpl	$0, 248(%r12,%r15)
	jne	.LBB13_18
# BB#17:                                # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit34.i
                                        #   in Loop: Header=BB13_11 Depth=1
	leaq	248(%r12,%r15), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB13_26
.LBB13_18:                              # %_ZN9NCompress6NBZip26CState6CreateEv.exit
                                        #   in Loop: Header=BB13_11 Depth=1
	leaq	(%r12,%r15), %rdx
	leaq	16(%r12,%r15), %rdi
	movl	$_ZN9NCompress6NBZip2L8MFThreadEPv, %esi
	callq	Thread_Create
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB13_26
# BB#19:                                # %_ZN9NCompress6NBZip26CState6CreateEv.exit..thread_crit_edge
                                        #   in Loop: Header=BB13_11 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %ebp
.LBB13_20:                              # %.thread
                                        #   in Loop: Header=BB13_11 Depth=1
	incl	%ebx
	cmpl	%ebp, %ebx
	jb	.LBB13_10
# BB#21:
	xorl	%ebp, %ebp
	jmp	.LBB13_27
.LBB13_22:                              # %_ZN8NWindows16NSynchronization17CManualResetEvent18CreateIfNotCreatedEv.exit
	leaq	28712(%r14), %rdi
	callq	ManualResetEvent_CreateNotSignaled
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB13_27
	jmp	.LBB13_1
.LBB13_23:                              # %_ZN8NWindows16NSynchronization17CManualResetEvent18CreateIfNotCreatedEv.exit41
	leaq	28872(%r14), %rdi
	callq	ManualResetEvent_CreateNotSignaled
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB13_27
	jmp	.LBB13_2
.LBB13_24:                              # %.loopexit51.thread
	movq	%r12, 28696(%r14)
.LBB13_25:
	xorl	%ebp, %ebp
	jmp	.LBB13_27
.LBB13_26:                              # %_ZN9NCompress6NBZip26CState6CreateEv.exit.thread
	movl	%ebx, 28856(%r14)
	movq	%r14, %rdi
	callq	_ZN9NCompress6NBZip28CDecoder4FreeEv
.LBB13_27:                              # %.loopexit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_28:
.Ltmp87:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB13_27
.Lfunc_end13:
	.size	_ZN9NCompress6NBZip28CDecoder6CreateEv, .Lfunc_end13-_ZN9NCompress6NBZip28CDecoder6CreateEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\257\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp85-.Lfunc_begin5   #   Call between .Lfunc_begin5 and .Ltmp85
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin5   #     jumps to .Ltmp87
	.byte	1                       #   On action: 1
	.long	.Ltmp86-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Lfunc_end13-.Ltmp86    #   Call between .Ltmp86 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress6NBZip26CStateD2Ev,"axG",@progbits,_ZN9NCompress6NBZip26CStateD2Ev,comdat
	.weak	_ZN9NCompress6NBZip26CStateD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip26CStateD2Ev,@function
_ZN9NCompress6NBZip26CStateD2Ev:        # @_ZN9NCompress6NBZip26CStateD2Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 32
.Lcfi94:
	.cfi_offset %rbx, -24
.Lcfi95:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
.Ltmp88:
	callq	BigFree
.Ltmp89:
# BB#1:
	movq	$0, (%rbx)
	leaq	248(%rbx), %rdi
.Ltmp93:
	callq	Event_Close
.Ltmp94:
# BB#2:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit
	leaq	144(%rbx), %rdi
.Ltmp98:
	callq	Event_Close
.Ltmp99:
# BB#3:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit5
	leaq	40(%rbx), %rdi
.Ltmp103:
	callq	Event_Close
.Ltmp104:
# BB#4:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit6
	addq	$16, %rbx
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	Thread_Close            # TAILCALL
.LBB14_8:
.Ltmp105:
	movq	%rax, %r14
	jmp	.LBB14_11
.LBB14_9:
.Ltmp100:
	movq	%rax, %r14
	jmp	.LBB14_10
.LBB14_6:
.Ltmp95:
	movq	%rax, %r14
	jmp	.LBB14_7
.LBB14_5:
.Ltmp90:
	movq	%rax, %r14
	leaq	248(%rbx), %rdi
.Ltmp91:
	callq	Event_Close
.Ltmp92:
.LBB14_7:                               # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit7
	leaq	144(%rbx), %rdi
.Ltmp96:
	callq	Event_Close
.Ltmp97:
.LBB14_10:                              # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit8
	leaq	40(%rbx), %rdi
.Ltmp101:
	callq	Event_Close
.Ltmp102:
.LBB14_11:                              # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit9
	addq	$16, %rbx
.Ltmp106:
	movq	%rbx, %rdi
	callq	Thread_Close
.Ltmp107:
# BB#12:                                # %_ZN8NWindows7CThreadD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB14_13:
.Ltmp108:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZN9NCompress6NBZip26CStateD2Ev, .Lfunc_end14-_ZN9NCompress6NBZip26CStateD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp88-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin6   #     jumps to .Ltmp90
	.byte	0                       #   On action: cleanup
	.long	.Ltmp93-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp94-.Ltmp93         #   Call between .Ltmp93 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin6   #     jumps to .Ltmp95
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Ltmp99-.Ltmp98         #   Call between .Ltmp98 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin6  #     jumps to .Ltmp100
	.byte	0                       #   On action: cleanup
	.long	.Ltmp103-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin6  #     jumps to .Ltmp105
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp91-.Ltmp104        #   Call between .Ltmp104 and .Ltmp91
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin6   # >> Call Site 6 <<
	.long	.Ltmp107-.Ltmp91        #   Call between .Ltmp91 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin6  #     jumps to .Ltmp108
	.byte	1                       #   On action: 1
	.long	.Ltmp107-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Lfunc_end14-.Ltmp107   #   Call between .Ltmp107 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress6NBZip26CState6CreateEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip26CState6CreateEv,@function
_ZN9NCompress6NBZip26CState6CreateEv:   # @_ZN9NCompress6NBZip26CState6CreateEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 16
.Lcfi97:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpl	$0, 40(%rbx)
	je	.LBB15_1
.LBB15_2:                               # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit.thread
	cmpl	$0, 144(%rbx)
	je	.LBB15_3
.LBB15_4:                               # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit32.thread
	cmpl	$0, 248(%rbx)
	jne	.LBB15_7
# BB#5:                                 # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit34
	leaq	248(%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	jne	.LBB15_6
.LBB15_7:                               # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit34.thread
	leaq	16(%rbx), %rdi
	movl	$_ZN9NCompress6NBZip2L8MFThreadEPv, %esi
	movq	%rbx, %rdx
	popq	%rbx
	jmp	Thread_Create           # TAILCALL
.LBB15_1:                               # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit
	leaq	40(%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	jne	.LBB15_6
	jmp	.LBB15_2
.LBB15_3:                               # %_ZN8NWindows16NSynchronization15CAutoResetEvent18CreateIfNotCreatedEv.exit32
	leaq	144(%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	je	.LBB15_4
.LBB15_6:
	popq	%rbx
	retq
.Lfunc_end15:
	.size	_ZN9NCompress6NBZip26CState6CreateEv, .Lfunc_end15-_ZN9NCompress6NBZip26CState6CreateEv
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CDecoder14ReadSignaturesERbRj
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder14ReadSignaturesERbRj,@function
_ZN9NCompress6NBZip28CDecoder14ReadSignaturesERbRj: # @_ZN9NCompress6NBZip28CDecoder14ReadSignaturesERbRj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi101:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi102:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi104:
	.cfi_def_cfa_offset 80
.Lcfi105:
	.cfi_offset %rbx, -56
.Lcfi106:
	.cfi_offset %r12, -48
.Lcfi107:
	.cfi_offset %r13, -40
.Lcfi108:
	.cfi_offset %r14, -32
.Lcfi109:
	.cfi_offset %r15, -24
.Lcfi110:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movb	$0, (%rsi)
	leaq	344(%rbx), %r12
	movl	336(%rbx), %eax
	movl	340(%rbx), %ebp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB16_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_2 Depth 2
	movl	$8, %ecx
	subl	%eax, %ecx
	movl	%ebp, %r14d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r14d
	shrl	$16, %r14d
	leal	8(%rax), %ecx
	movl	%ecx, 336(%rbx)
	cmpl	$-9, %eax
	ja	.LBB16_14
	.p2align	4, 0x90
.LBB16_2:                               # %.lr.ph.i.i.i.i.i
                                        #   Parent Loop BB16_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shll	$8, %ebp
	movq	344(%rbx), %rax
	cmpq	352(%rbx), %rax
	jae	.LBB16_3
# BB#12:                                #   in Loop: Header=BB16_2 Depth=2
	leaq	1(%rax), %rdx
	movq	%rdx, (%r12)
	movzbl	(%rax), %eax
	jmp	.LBB16_13
	.p2align	4, 0x90
.LBB16_3:                               #   in Loop: Header=BB16_2 Depth=2
	movq	%r12, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	336(%rbx), %ecx
.LBB16_13:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i.i
                                        #   in Loop: Header=BB16_2 Depth=2
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 340(%rbx)
	addl	$-8, %ecx
	movl	%ecx, 336(%rbx)
	cmpl	$7, %ecx
	ja	.LBB16_2
.LBB16_14:                              # %_ZN9NCompress6NBZip28CDecoder8ReadByteEv.exit
                                        #   in Loop: Header=BB16_1 Depth=1
	movb	%r14b, 10(%rsp,%r13)
	incq	%r13
	cmpq	$6, %r13
	movl	%ecx, %eax
	jne	.LBB16_1
# BB#4:
	movq	%rbx, %rdi
	callq	_ZN9NCompress6NBZip28CDecoder7ReadCrcEv
	movl	%eax, %ecx
	movl	%ecx, (%r15)
	movl	$1, %eax
	movb	10(%rsp), %dl
	cmpb	$23, %dl
	je	.LBB16_15
# BB#5:
	cmpb	$49, %dl
	jne	.LBB16_21
# BB#6:
	cmpb	$65, 11(%rsp)
	jne	.LBB16_21
# BB#7:
	cmpb	$89, 12(%rsp)
	jne	.LBB16_21
# BB#8:
	cmpb	$38, 13(%rsp)
	jne	.LBB16_21
# BB#9:
	cmpb	$83, 14(%rsp)
	jne	.LBB16_21
# BB#10:
	cmpb	$89, 15(%rsp)
	jne	.LBB16_21
# BB#11:
	movl	28684(%rbx), %eax
	roll	%eax
	xorl	%ecx, %eax
	movl	%eax, 28684(%rbx)
	xorl	%eax, %eax
	jmp	.LBB16_21
.LBB16_15:
	cmpb	$114, 11(%rsp)
	jne	.LBB16_21
# BB#16:
	cmpb	$69, 12(%rsp)
	jne	.LBB16_21
# BB#17:
	cmpb	$56, 13(%rsp)
	jne	.LBB16_21
# BB#18:
	cmpb	$80, 14(%rsp)
	jne	.LBB16_21
# BB#19:
	cmpb	$-112, 15(%rsp)
	jne	.LBB16_21
# BB#20:
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	$1, (%rax)
	xorl	%eax, %eax
	cmpl	28684(%rbx), %ecx
	setne	%al
.LBB16_21:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	_ZN9NCompress6NBZip28CDecoder14ReadSignaturesERbRj, .Lfunc_end16-_ZN9NCompress6NBZip28CDecoder14ReadSignaturesERbRj
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CDecoder10DecodeFileERbP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder10DecodeFileERbP21ICompressProgressInfo,@function
_ZN9NCompress6NBZip28CDecoder10DecodeFileERbP21ICompressProgressInfo: # @_ZN9NCompress6NBZip28CDecoder10DecodeFileERbP21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi112:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi114:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi115:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi117:
	.cfi_def_cfa_offset 160
.Lcfi118:
	.cfi_offset %rbx, -56
.Lcfi119:
	.cfi_offset %r12, -48
.Lcfi120:
	.cfi_offset %r13, -40
.Lcfi121:
	.cfi_offset %r14, -32
.Lcfi122:
	.cfi_offset %r15, -24
.Lcfi123:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rdx, 28688(%rbx)
	callq	_ZN9NCompress6NBZip28CDecoder6CreateEv
	movl	%eax, %r12d
	testl	%r12d, %r12d
	je	.LBB17_2
.LBB17_1:                               # %.thread130
	movl	%r12d, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_2:                               # %.preheader
	cmpl	$0, 28856(%rbx)
	je	.LBB17_11
# BB#3:                                 # %.lr.ph152
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_4:                               # =>This Inner Loop Header: Depth=1
	movq	28696(%rbx), %r14
	movl	%ebp, %ebp
	imulq	$608, %rbp, %r15        # imm = 0x260
	cmpq	$0, (%r14,%r15)
	jne	.LBB17_6
# BB#5:                                 # %_ZN9NCompress6NBZip26CState5AllocEv.exit
                                        #   in Loop: Header=BB17_4 Depth=1
	leaq	(%r14,%r15), %r12
	movl	$3601024, %edi          # imm = 0x36F280
	callq	BigAlloc
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.LBB17_73
.LBB17_6:                               # %_ZN9NCompress6NBZip26CState5AllocEv.exit.thread
                                        #   in Loop: Header=BB17_4 Depth=1
	cmpb	$0, 28860(%rbx)
	je	.LBB17_10
# BB#7:                                 #   in Loop: Header=BB17_4 Depth=1
	leaq	40(%r14,%r15), %rdi
	callq	Event_Reset
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jne	.LBB17_1
# BB#8:                                 #   in Loop: Header=BB17_4 Depth=1
	leaq	144(%r14,%r15), %rdi
	callq	Event_Reset
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jne	.LBB17_1
# BB#9:                                 #   in Loop: Header=BB17_4 Depth=1
	leaq	248(%r14,%r15), %rdi
	callq	Event_Reset
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jne	.LBB17_1
.LBB17_10:                              # %.thread
                                        #   in Loop: Header=BB17_4 Depth=1
	incl	%ebp
	cmpl	28856(%rbx), %ebp
	jb	.LBB17_4
.LBB17_11:                              # %._crit_edge153
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movb	$0, (%r13)
	leaq	344(%rbx), %r13
	movl	336(%rbx), %eax
	movl	340(%rbx), %ebp
	movl	$8, %r15d
	movl	$8, %ecx
	subl	%eax, %ecx
	movl	%ebp, %r14d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r14d
	leal	8(%rax), %esi
	movl	%esi, 336(%rbx)
	cmpl	$-9, %eax
	ja	.LBB17_16
	.p2align	4, 0x90
.LBB17_12:                              # %.lr.ph.i.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	shll	$8, %ebp
	movq	344(%rbx), %rax
	cmpq	352(%rbx), %rax
	jae	.LBB17_14
# BB#13:                                #   in Loop: Header=BB17_12 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r13)
	movzbl	(%rax), %eax
	jmp	.LBB17_15
	.p2align	4, 0x90
.LBB17_14:                              #   in Loop: Header=BB17_12 Depth=1
	movq	%r13, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	336(%rbx), %esi
.LBB17_15:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i.i
                                        #   in Loop: Header=BB17_12 Depth=1
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 340(%rbx)
	addl	$-8, %esi
	movl	%esi, 336(%rbx)
	cmpl	$7, %esi
	ja	.LBB17_12
.LBB17_16:                              # %_ZN9NCompress6NBZip28CDecoder8ReadByteEv.exit
	subl	%esi, %r15d
	movl	%ebp, %r12d
	movl	%r15d, %ecx
	shrl	%cl, %r12d
	leal	8(%rsi), %edx
	movl	%edx, 336(%rbx)
	cmpl	$-9, %esi
	ja	.LBB17_21
	.p2align	4, 0x90
.LBB17_17:                              # %.lr.ph.i.i.i.i.i.1
                                        # =>This Inner Loop Header: Depth=1
	shll	$8, %ebp
	movq	344(%rbx), %rax
	cmpq	352(%rbx), %rax
	jae	.LBB17_19
# BB#18:                                #   in Loop: Header=BB17_17 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r13)
	movzbl	(%rax), %eax
	jmp	.LBB17_20
	.p2align	4, 0x90
.LBB17_19:                              #   in Loop: Header=BB17_17 Depth=1
	movq	%r13, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	336(%rbx), %edx
.LBB17_20:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i.i.1
                                        #   in Loop: Header=BB17_17 Depth=1
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 340(%rbx)
	addl	$-8, %edx
	movl	%edx, 336(%rbx)
	cmpl	$7, %edx
	ja	.LBB17_17
.LBB17_21:                              # %_ZN9NCompress6NBZip28CDecoder8ReadByteEv.exit.1
	shrl	$16, %r14d
	shrl	$16, %r12d
	movl	%r12d, 32(%rsp)         # 4-byte Spill
	movl	$8, %r15d
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebp, %r12d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r12d
	shrl	$16, %r12d
	leal	8(%rdx), %esi
	movl	%esi, 336(%rbx)
	cmpl	$-9, %edx
	ja	.LBB17_26
	.p2align	4, 0x90
.LBB17_22:                              # %.lr.ph.i.i.i.i.i.2
                                        # =>This Inner Loop Header: Depth=1
	shll	$8, %ebp
	movq	344(%rbx), %rax
	cmpq	352(%rbx), %rax
	jae	.LBB17_24
# BB#23:                                #   in Loop: Header=BB17_22 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r13)
	movzbl	(%rax), %eax
	jmp	.LBB17_25
	.p2align	4, 0x90
.LBB17_24:                              #   in Loop: Header=BB17_22 Depth=1
	movq	%r13, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	336(%rbx), %esi
.LBB17_25:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i.i.2
                                        #   in Loop: Header=BB17_22 Depth=1
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 340(%rbx)
	addl	$-8, %esi
	movl	%esi, 336(%rbx)
	cmpl	$7, %esi
	ja	.LBB17_22
.LBB17_26:                              # %_ZN9NCompress6NBZip28CDecoder8ReadByteEv.exit.2
	movl	%r14d, 24(%rsp)         # 4-byte Spill
	subl	%esi, %r15d
	movl	%ebp, %r14d
	movl	%r15d, %ecx
	shrl	%cl, %r14d
	shrl	$16, %r14d
	leal	8(%rsi), %ecx
	movl	%ecx, 336(%rbx)
	cmpl	$-9, %esi
	ja	.LBB17_31
	.p2align	4, 0x90
.LBB17_27:                              # %.lr.ph.i.i.i.i.i.3
                                        # =>This Inner Loop Header: Depth=1
	shll	$8, %ebp
	movq	344(%rbx), %rax
	cmpq	352(%rbx), %rax
	jae	.LBB17_29
# BB#28:                                #   in Loop: Header=BB17_27 Depth=1
	leaq	1(%rax), %rdx
	movq	%rdx, (%r13)
	movzbl	(%rax), %eax
	jmp	.LBB17_30
	.p2align	4, 0x90
.LBB17_29:                              #   in Loop: Header=BB17_27 Depth=1
	movq	%r13, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	336(%rbx), %ecx
.LBB17_30:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i.i.3
                                        #   in Loop: Header=BB17_27 Depth=1
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 340(%rbx)
	addl	$-8, %ecx
	movl	%ecx, 336(%rbx)
	cmpl	$7, %ecx
	ja	.LBB17_27
.LBB17_31:                              # %_ZN9NCompress6NBZip28CDecoder8ReadByteEv.exit.3
	cmpb	$66, 24(%rsp)           # 1-byte Folded Reload
	setne	%al
	cmpb	$90, 32(%rsp)           # 1-byte Folded Reload
	setne	%dil
	cmpb	$104, %r12b
	setne	%sil
	movl	%r14d, %edx
	addb	$-49, %dl
	xorl	%r12d, %r12d
	cmpb	$8, %dl
	ja	.LBB17_1
# BB#32:                                # %_ZN9NCompress6NBZip28CDecoder8ReadByteEv.exit.3
	orb	%dil, %al
	orb	%sil, %al
	movq	48(%rsp), %rax          # 8-byte Reload
	jne	.LBB17_1
# BB#33:
	movb	$1, (%rax)
	movzbl	%r14b, %eax
	imull	$100000, %eax, %r14d    # imm = 0x186A0
	addl	$-4800000, %r14d        # imm = 0xFFB6C200
	movl	$0, 28684(%rbx)
	cmpb	$0, 28860(%rbx)
	je	.LBB17_44
# BB#34:
	movl	%r14d, %ebp
	leaq	28872(%rbx), %r14
	movb	$0, 28870(%rbx)
	movw	$0, 28868(%rbx)
	movl	$0, 28864(%rbx)
	movq	%r14, %rdi
	callq	Event_Reset
	movq	28696(%rbx), %rdi
	addq	$248, %rdi
	callq	Event_Set
	movl	%ebp, 28984(%rbx)
	movq	$0, 28976(%rbx)
	leaq	28712(%rbx), %r15
	movq	%r15, %rdi
	callq	Event_Set
	cmpl	$0, 28856(%rbx)
	je	.LBB17_37
# BB#35:                                # %.lr.ph148.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_36:                              # %.lr.ph148
                                        # =>This Inner Loop Header: Depth=1
	movq	28696(%rbx), %rax
	movl	%ebp, %ecx
	imulq	$608, %rcx, %rcx        # imm = 0x260
	leaq	40(%rax,%rcx), %rdi
	callq	Event_Wait
	incl	%ebp
	cmpl	28856(%rbx), %ebp
	jb	.LBB17_36
.LBB17_37:                              # %._crit_edge149
	movq	%r15, %rdi
	callq	Event_Reset
	movq	%r14, %rdi
	callq	Event_Set
	cmpl	$0, 28856(%rbx)
	je	.LBB17_40
# BB#38:                                # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_39:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	28696(%rbx), %rax
	movl	%ebp, %ecx
	imulq	$608, %rcx, %rcx        # imm = 0x260
	leaq	144(%rax,%rcx), %rdi
	callq	Event_Wait
	incl	%ebp
	cmpl	28856(%rbx), %ebp
	jb	.LBB17_39
.LBB17_40:                              # %._crit_edge
	movq	%r14, %rdi
	callq	Event_Reset
	movl	28980(%rbx), %r12d
	testl	%r12d, %r12d
	jne	.LBB17_1
# BB#41:
	movl	28976(%rbx), %r12d
	testl	%r12d, %r12d
	jne	.LBB17_1
# BB#42:                                # %.thread132
	movq	344(%rbx), %rax
	addq	376(%rbx), %rax
	subq	360(%rbx), %rax
	movl	$32, %ecx
	subl	336(%rbx), %ecx
	shrl	$3, %ecx
	subq	%rcx, %rax
	movq	%rax, 16(%rsp)
	cmpq	$0, 28688(%rbx)
	je	.LBB17_76
# BB#43:
	subq	28672(%rbx), %rax
	movq	%rax, 16(%rsp)
	leaq	24(%rbx), %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
	movq	%rax, 40(%rsp)
	movq	28688(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	16(%rsp), %rsi
	leaq	40(%rsp), %rdx
	callq	*40(%rax)
	jmp	.LBB17_74
.LBB17_44:
	movq	28696(%rbx), %r15
	leaq	336(%rbx), %rbp
	leaq	392(%rbx), %r13
	leaq	18396(%rbx), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	24(%rbx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	leaq	16(%rsp), %rdx
	movq	%r15, 88(%rsp)          # 8-byte Spill
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movl	%r14d, 60(%rsp)         # 4-byte Spill
	movq	%r13, 72(%rsp)          # 8-byte Spill
	jmp	.LBB17_48
.LBB17_45:                              # %_ZN9NCompress6NBZip2L12DecodeBlock2EPKjjjR10COutBuffer.exit
                                        #   in Loop: Header=BB17_48 Depth=1
	notl	%r15d
	movl	64(%rsp), %r12d         # 4-byte Reload
	movl	60(%rsp), %r14d         # 4-byte Reload
	movq	72(%rsp), %r13          # 8-byte Reload
.LBB17_46:                              #   in Loop: Header=BB17_48 Depth=1
	xorl	%ecx, %ecx
	cmpl	16(%rsp), %r15d
	setne	%cl
	movl	$1, %eax
	cmovnel	%eax, %r12d
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	80(%rsp), %rbp          # 8-byte Reload
	testl	%ecx, %ecx
	jne	.LBB17_1
.LBB17_47:                              # %._crit_edge154
                                        #   in Loop: Header=BB17_48 Depth=1
	movl	(%rbp), %ecx
.LBB17_48:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_55 Depth 2
                                        #     Child Loop BB17_57 Depth 2
                                        #     Child Loop BB17_61 Depth 2
                                        #       Child Loop BB17_64 Depth 3
	movq	344(%rbx), %rax
	addq	376(%rbx), %rax
	subq	360(%rbx), %rax
	movl	$32, %esi
	subl	%ecx, %esi
	shrl	$3, %esi
	subq	%rsi, %rax
	movq	%rax, 16(%rsp)
	cmpq	$0, 28688(%rbx)
	je	.LBB17_50
# BB#49:                                # %_ZN9NCompress6NBZip28CDecoder16SetRatioProgressEy.exit124
                                        #   in Loop: Header=BB17_48 Depth=1
	subq	28672(%rbx), %rax
	movq	%rax, 16(%rsp)
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
	movq	%rax, 40(%rsp)
	movq	28688(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	16(%rsp), %rsi
	leaq	40(%rsp), %rdx
	callq	*40(%rax)
	leaq	16(%rsp), %rdx
	testl	%eax, %eax
	jne	.LBB17_74
.LBB17_50:                              #   in Loop: Header=BB17_48 Depth=1
	movq	%rbx, %rdi
	leaq	15(%rsp), %rsi
	callq	_ZN9NCompress6NBZip28CDecoder14ReadSignaturesERbRj
	testl	%eax, %eax
	cmovnel	%eax, %r12d
	jne	.LBB17_74
# BB#51:                                #   in Loop: Header=BB17_48 Depth=1
	cmpb	$0, 15(%rsp)
	jne	.LBB17_76
# BB#52:                                #   in Loop: Header=BB17_48 Depth=1
	movq	(%r15), %rsi
	movq	%rbp, %rdi
	movl	%r14d, %edx
	movq	%r13, %rcx
	movq	96(%rsp), %r8           # 8-byte Reload
	leaq	40(%rsp), %r9
	leaq	14(%rsp), %rax
	pushq	%rax
.Lcfi124:
	.cfi_adjust_cfa_offset 8
	leaq	76(%rsp), %rax
	pushq	%rax
.Lcfi125:
	.cfi_adjust_cfa_offset 8
	callq	_ZN9NCompress6NBZip2L9ReadBlockEPN5NBitm8CDecoderI9CInBufferEEPjjPhPNS_8NHuffman8CDecoderILi20ELj258EEES6_S6_Pb
	addq	$16, %rsp
.Lcfi126:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	cmovnel	%eax, %r12d
	je	.LBB17_54
# BB#53:                                #   in Loop: Header=BB17_48 Depth=1
	movl	$1, %ecx
	movl	%eax, %r12d
	leaq	16(%rsp), %rdx
	testl	%ecx, %ecx
	je	.LBB17_47
	jmp	.LBB17_1
.LBB17_54:                              #   in Loop: Header=BB17_48 Depth=1
	movq	(%r15), %rax
	movl	40(%rsp), %r13d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB17_55:                              #   Parent Loop BB17_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax,%rcx,4), %esi
	addl	%edx, %esi
	movl	%edx, (%rax,%rcx,4)
	movl	4(%rax,%rcx,4), %edx
	addl	%esi, %edx
	movl	%esi, 4(%rax,%rcx,4)
	movl	8(%rax,%rcx,4), %esi
	addl	%edx, %esi
	movl	%edx, 8(%rax,%rcx,4)
	movl	12(%rax,%rcx,4), %edx
	addl	%esi, %edx
	movl	%esi, 12(%rax,%rcx,4)
	addq	$4, %rcx
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB17_55
# BB#56:                                #   in Loop: Header=BB17_48 Depth=1
	leaq	1024(%rax), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB17_57:                              #   Parent Loop BB17_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	1024(%rax,%rdx,4), %esi
	movl	(%rax,%rsi,4), %edi
	leal	1(%rdi), %ebp
	movl	%ebp, (%rax,%rsi,4)
	orl	%ecx, 1024(%rax,%rdi,4)
	incq	%rdx
	addl	$256, %ecx              # imm = 0x100
	cmpq	%r13, %rdx
	jb	.LBB17_57
# BB#58:                                # %_ZN9NCompress6NBZip2L12DecodeBlock1EPjj.exit
                                        #   in Loop: Header=BB17_48 Depth=1
	cmpb	$0, 14(%rsp)
	movl	68(%rsp), %edx
	je	.LBB17_60
# BB#59:                                #   in Loop: Header=BB17_48 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%r13d, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	_ZN9NCompress6NBZip2L16DecodeBlock2RandEPKjjjR10COutBuffer
	movl	%eax, %r15d
	movq	72(%rsp), %r13          # 8-byte Reload
	leaq	16(%rsp), %rdx
	jmp	.LBB17_46
.LBB17_60:                              #   in Loop: Header=BB17_48 Depth=1
	movl	%r12d, 64(%rsp)         # 4-byte Spill
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%rdx,4), %eax
	shrl	$8, %eax
	movl	(%rcx,%rax,4), %ebp
	movzbl	%bpl, %r14d
	xorl	%esi, %esi
	movl	$-1, %r15d
	leaq	16(%rsp), %rdx
	.p2align	4, 0x90
.LBB17_61:                              #   Parent Loop BB17_48 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_64 Depth 3
	movl	%ebp, %eax
	movzbl	%al, %r12d
	movl	%eax, %ecx
	shrl	$8, %ecx
	movl	%esi, %edi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movl	(%rsi,%rcx,4), %ebp
	movl	%edi, %esi
	cmpl	$4, %esi
	jne	.LBB17_68
# BB#62:                                # %.preheader.i
                                        #   in Loop: Header=BB17_61 Depth=2
	xorl	%esi, %esi
	testl	%r12d, %r12d
	je	.LBB17_71
# BB#63:                                # %.lr.ph.i
                                        #   in Loop: Header=BB17_61 Depth=2
	negl	%r12d
	.p2align	4, 0x90
.LBB17_64:                              #   Parent Loop BB17_48 Depth=1
                                        #     Parent Loop BB17_61 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r15d, %eax
	shrl	$24, %eax
	xorl	%r14d, %eax
	shll	$8, %r15d
	xorl	_ZN9CBZip2Crc5TableE(,%rax,4), %r15d
	movq	24(%rbx), %rax
	movl	32(%rbx), %ecx
	leal	1(%rcx), %esi
	movl	%esi, 32(%rbx)
	movb	%r14b, (%rax,%rcx)
	movl	32(%rbx), %eax
	cmpl	36(%rbx), %eax
	jne	.LBB17_66
# BB#65:                                #   in Loop: Header=BB17_64 Depth=3
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
	leaq	16(%rsp), %rdx
.LBB17_66:                              # %_ZN10COutBuffer9WriteByteEh.exit
                                        #   in Loop: Header=BB17_64 Depth=3
	incl	%r12d
	jne	.LBB17_64
# BB#67:                                #   in Loop: Header=BB17_61 Depth=2
	movl	%r14d, %r12d
	xorl	%esi, %esi
	jmp	.LBB17_72
	.p2align	4, 0x90
.LBB17_68:                              #   in Loop: Header=BB17_61 Depth=2
	movl	%ebp, 48(%rsp)          # 4-byte Spill
	incl	%esi
	cmpl	%r14d, %r12d
	movl	$1, %ecx
	cmovnel	%ecx, %esi
	movl	%esi, %ebp
	movl	%r15d, %ecx
	shrl	$24, %ecx
	xorl	%r12d, %ecx
	shll	$8, %r15d
	xorl	_ZN9CBZip2Crc5TableE(,%rcx,4), %r15d
	movq	24(%rbx), %rcx
	movl	32(%rbx), %edi
	leal	1(%rdi), %esi
	movl	%esi, 32(%rbx)
	movb	%al, (%rcx,%rdi)
	movl	32(%rbx), %eax
	cmpl	36(%rbx), %eax
	jne	.LBB17_70
# BB#69:                                #   in Loop: Header=BB17_61 Depth=2
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
	leaq	16(%rsp), %rdx
.LBB17_70:                              # %.loopexit.i
                                        #   in Loop: Header=BB17_61 Depth=2
	movl	%ebp, %esi
	movl	48(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB17_72
.LBB17_71:                              #   in Loop: Header=BB17_61 Depth=2
	movl	%r14d, %r12d
	.p2align	4, 0x90
.LBB17_72:                              # %.loopexit.i
                                        #   in Loop: Header=BB17_61 Depth=2
	decl	%r13d
	movl	%r12d, %r14d
	jne	.LBB17_61
	jmp	.LBB17_45
.LBB17_73:
	movl	$-2147024882, %r12d     # imm = 0x8007000E
	jmp	.LBB17_1
.LBB17_74:                              # %.thread130
	movl	%eax, %r12d
	jmp	.LBB17_1
.LBB17_76:
	xorl	%r12d, %r12d
	jmp	.LBB17_1
.Lfunc_end17:
	.size	_ZN9NCompress6NBZip28CDecoder10DecodeFileERbP21ICompressProgressInfo, .Lfunc_end17-_ZN9NCompress6NBZip28CDecoder10DecodeFileERbP21ICompressProgressInfo
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CDecoder16SetRatioProgressEy
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder16SetRatioProgressEy,@function
_ZN9NCompress6NBZip28CDecoder16SetRatioProgressEy: # @_ZN9NCompress6NBZip28CDecoder16SetRatioProgressEy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi128:
	.cfi_def_cfa_offset 32
.Lcfi129:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	%rsi, (%rsp)
	cmpq	$0, 28688(%rbx)
	je	.LBB18_1
# BB#2:
	subq	28672(%rbx), %rsi
	movq	%rsi, (%rsp)
	leaq	24(%rbx), %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
	movq	%rax, 8(%rsp)
	movq	28688(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%rsp, %rsi
	leaq	8(%rsp), %rdx
	callq	*40(%rax)
	jmp	.LBB18_3
.LBB18_1:
	xorl	%eax, %eax
.LBB18_3:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end18:
	.size	_ZN9NCompress6NBZip28CDecoder16SetRatioProgressEy, .Lfunc_end18-_ZN9NCompress6NBZip28CDecoder16SetRatioProgressEy
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip2L9ReadBlockEPN5NBitm8CDecoderI9CInBufferEEPjjPhPNS_8NHuffman8CDecoderILi20ELj258EEES6_S6_Pb,@function
_ZN9NCompress6NBZip2L9ReadBlockEPN5NBitm8CDecoderI9CInBufferEEPjjPhPNS_8NHuffman8CDecoderILi20ELj258EEES6_S6_Pb: # @_ZN9NCompress6NBZip2L9ReadBlockEPN5NBitm8CDecoderI9CInBufferEEPjjPhPNS_8NHuffman8CDecoderILi20ELj258EEES6_S6_Pb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi133:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 56
	subq	$616, %rsp              # imm = 0x268
.Lcfi136:
	.cfi_def_cfa_offset 672
.Lcfi137:
	.cfi_offset %rbx, -56
.Lcfi138:
	.cfi_offset %r12, -48
.Lcfi139:
	.cfi_offset %r13, -40
.Lcfi140:
	.cfi_offset %r14, -32
.Lcfi141:
	.cfi_offset %r15, -24
.Lcfi142:
	.cfi_offset %rbp, -16
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	%edx, %r12d
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movq	680(%rsp), %r15
	testq	%r15, %r15
	movl	(%rbx), %edx
	movl	4(%rbx), %ebp
	movq	%r9, 88(%rsp)           # 8-byte Spill
	je	.LBB19_8
# BB#1:
	movl	%r12d, %r13d
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebp, %r12d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r12d
	incl	%edx
	movl	%edx, (%rbx)
	cmpl	$8, %edx
	jb	.LBB19_7
# BB#2:                                 # %.lr.ph.i.i.i.i
	leaq	8(%rbx), %r14
	.p2align	4, 0x90
.LBB19_3:                               # =>This Inner Loop Header: Depth=1
	shll	$8, %ebp
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jae	.LBB19_5
# BB#4:                                 #   in Loop: Header=BB19_3 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB19_6
	.p2align	4, 0x90
.LBB19_5:                               #   in Loop: Header=BB19_3 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	(%rbx), %edx
.LBB19_6:                               # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i
                                        #   in Loop: Header=BB19_3 Depth=1
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 4(%rbx)
	addl	$-8, %edx
	movl	%edx, (%rbx)
	cmpl	$7, %edx
	ja	.LBB19_3
.LBB19_7:                               # %_ZN9NCompress6NBZip2L7ReadBitEPN5NBitm8CDecoderI9CInBufferEE.exit
	shrl	$23, %r12d
	andb	$1, %r12b
	movb	%r12b, (%r15)
	movl	%r13d, %r12d
.LBB19_8:                               # %._crit_edge313
	movq	672(%rsp), %r13
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebp, %r15d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r15d
	andl	$16777215, %r15d        # imm = 0xFFFFFF
	addl	$24, %edx
	movl	%edx, (%rbx)
	cmpl	$8, %edx
	jb	.LBB19_14
# BB#9:                                 # %.lr.ph.i.i.i.i205
	leaq	8(%rbx), %r14
	.p2align	4, 0x90
.LBB19_10:                              # =>This Inner Loop Header: Depth=1
	shll	$8, %ebp
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jae	.LBB19_12
# BB#11:                                #   in Loop: Header=BB19_10 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB19_13
	.p2align	4, 0x90
.LBB19_12:                              #   in Loop: Header=BB19_10 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	(%rbx), %edx
.LBB19_13:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i208
                                        #   in Loop: Header=BB19_10 Depth=1
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 4(%rbx)
	addl	$-8, %edx
	movl	%edx, (%rbx)
	cmpl	$7, %edx
	ja	.LBB19_10
.LBB19_14:                              # %_ZN9NCompress6NBZip2L8ReadBitsEPN5NBitm8CDecoderI9CInBufferEEj.exit
	movl	%r15d, (%r13)
	movl	$1, %ebp
	cmpl	%r12d, %r15d
	jae	.LBB19_138
# BB#15:
	movl	%r12d, 28(%rsp)         # 4-byte Spill
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 336(%rsp)
	movdqa	%xmm0, 320(%rsp)
	movdqa	%xmm0, 304(%rsp)
	movdqa	%xmm0, 288(%rsp)
	movdqa	%xmm0, 272(%rsp)
	movdqa	%xmm0, 256(%rsp)
	movdqa	%xmm0, 240(%rsp)
	movdqa	%xmm0, 224(%rsp)
	movdqa	%xmm0, 208(%rsp)
	movdqa	%xmm0, 192(%rsp)
	movdqa	%xmm0, 176(%rsp)
	movdqa	%xmm0, 160(%rsp)
	movdqa	%xmm0, 144(%rsp)
	movdqa	%xmm0, 128(%rsp)
	movdqa	%xmm0, 112(%rsp)
	movdqa	%xmm0, 96(%rsp)
	leaq	8(%rbx), %r14
	movl	(%rbx), %edx
	movl	4(%rbx), %r12d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB19_16:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_17 Depth 2
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r12d, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	shrl	$23, %ebp
	incl	%edx
	movl	%edx, (%rbx)
	cmpl	$8, %edx
	jb	.LBB19_21
	.p2align	4, 0x90
.LBB19_17:                              # %.lr.ph.i.i.i.i209
                                        #   Parent Loop BB19_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shll	$8, %r12d
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jae	.LBB19_19
# BB#18:                                #   in Loop: Header=BB19_17 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB19_20
	.p2align	4, 0x90
.LBB19_19:                              #   in Loop: Header=BB19_17 Depth=2
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	(%rbx), %edx
.LBB19_20:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i212
                                        #   in Loop: Header=BB19_17 Depth=2
	movzbl	%al, %eax
	orl	%eax, %r12d
	movl	%r12d, 4(%rbx)
	addl	$-8, %edx
	movl	%edx, (%rbx)
	cmpl	$7, %edx
	ja	.LBB19_17
.LBB19_21:                              # %_ZN9NCompress6NBZip2L7ReadBitEPN5NBitm8CDecoderI9CInBufferEE.exit213
                                        #   in Loop: Header=BB19_16 Depth=1
	andb	$1, %bpl
	movb	%bpl, 352(%rsp,%r15)
	incq	%r15
	cmpq	$16, %r15
	jne	.LBB19_16
# BB#22:                                # %.preheader262.preheader
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB19_23:                              # %.preheader262
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_25 Depth 2
	movl	%r13d, %eax
	sarl	$4, %eax
	cltq
	cmpb	$0, 352(%rsp,%rax)
	je	.LBB19_32
# BB#24:                                #   in Loop: Header=BB19_23 Depth=1
	movl	$8, %r15d
	subl	%edx, %r15d
	incl	%edx
	movl	%edx, (%rbx)
	movl	%r12d, %ebp
	cmpl	$8, %edx
	jb	.LBB19_29
	.p2align	4, 0x90
.LBB19_25:                              # %.lr.ph.i.i.i.i214
                                        #   Parent Loop BB19_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shll	$8, %ebp
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jae	.LBB19_27
# BB#26:                                #   in Loop: Header=BB19_25 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB19_28
	.p2align	4, 0x90
.LBB19_27:                              #   in Loop: Header=BB19_25 Depth=2
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	(%rbx), %edx
.LBB19_28:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i217
                                        #   in Loop: Header=BB19_25 Depth=2
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 4(%rbx)
	addl	$-8, %edx
	movl	%edx, (%rbx)
	cmpl	$7, %edx
	ja	.LBB19_25
.LBB19_29:                              # %_ZN9NCompress6NBZip2L7ReadBitEPN5NBitm8CDecoderI9CInBufferEE.exit218
                                        #   in Loop: Header=BB19_23 Depth=1
	movl	$8388608, %eax          # imm = 0x800000
	movl	%r15d, %ecx
	shll	%cl, %eax
	testl	%r12d, %eax
	je	.LBB19_31
# BB#30:                                #   in Loop: Header=BB19_23 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	leal	1(%rdi), %eax
	movl	%edi, %ecx
	shlb	$3, %cl
	andb	$56, %cl
	movq	%r13, %rsi
	shlq	%cl, %rsi
	andl	$-8, %edi
	orq	%rsi, 96(%rsp,%rdi)
	movl	%ebp, %r12d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB19_32
	.p2align	4, 0x90
.LBB19_31:                              #   in Loop: Header=BB19_23 Depth=1
	movl	%ebp, %r12d
.LBB19_32:                              #   in Loop: Header=BB19_23 Depth=1
	incq	%r13
	cmpq	$256, %r13              # imm = 0x100
	jne	.LBB19_23
# BB#33:
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	movl	$1, %ebp
	je	.LBB19_138
# BB#34:
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r12d, %r15d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r15d
	shrl	$21, %r15d
	andl	$7, %r15d
	addl	$3, %edx
	movl	%edx, (%rbx)
	cmpl	$8, %edx
	jb	.LBB19_39
	.p2align	4, 0x90
.LBB19_35:                              # %.lr.ph.i.i.i.i219
                                        # =>This Inner Loop Header: Depth=1
	shll	$8, %r12d
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jae	.LBB19_37
# BB#36:                                #   in Loop: Header=BB19_35 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB19_38
	.p2align	4, 0x90
.LBB19_37:                              #   in Loop: Header=BB19_35 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	(%rbx), %edx
.LBB19_38:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i222
                                        #   in Loop: Header=BB19_35 Depth=1
	movzbl	%al, %eax
	orl	%eax, %r12d
	movl	%r12d, 4(%rbx)
	addl	$-8, %edx
	movl	%edx, (%rbx)
	cmpl	$7, %edx
	ja	.LBB19_35
.LBB19_39:                              # %_ZN9NCompress6NBZip2L8ReadBitsEPN5NBitm8CDecoderI9CInBufferEEj.exit223
	leal	-2(%r15), %eax
	cmpl	$4, %eax
	ja	.LBB19_138
# BB#40:
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r12d, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	shrl	$9, %ebp
	andl	$32767, %ebp            # imm = 0x7FFF
	addl	$15, %edx
	movl	%edx, (%rbx)
	cmpl	$8, %edx
	jb	.LBB19_45
	.p2align	4, 0x90
.LBB19_41:                              # %.lr.ph.i.i.i.i224
                                        # =>This Inner Loop Header: Depth=1
	shll	$8, %r12d
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jae	.LBB19_43
# BB#42:                                #   in Loop: Header=BB19_41 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB19_44
	.p2align	4, 0x90
.LBB19_43:                              #   in Loop: Header=BB19_41 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	(%rbx), %edx
.LBB19_44:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i227
                                        #   in Loop: Header=BB19_41 Depth=1
	movzbl	%al, %eax
	orl	%eax, %r12d
	movl	%r12d, 4(%rbx)
	addl	$-8, %edx
	movl	%edx, (%rbx)
	cmpl	$7, %edx
	ja	.LBB19_41
.LBB19_45:                              # %_ZN9NCompress6NBZip2L8ReadBitsEPN5NBitm8CDecoderI9CInBufferEEj.exit228
	leal	-1(%rbp), %eax
	cmpl	$18001, %eax            # imm = 0x4651
	ja	.LBB19_137
# BB#46:
	movl	%r15d, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movb	$0, 352(%rsp)
	cmpl	$1, %r15d
	jbe	.LBB19_53
# BB#47:
	movb	$1, 353(%rsp)
	cmpl	$2, %r15d
	je	.LBB19_53
# BB#48:
	movb	$2, 354(%rsp)
	cmpl	$4, %r15d
	jb	.LBB19_53
# BB#49:
	movb	$3, 355(%rsp)
	je	.LBB19_53
# BB#50:
	movb	$4, 356(%rsp)
	cmpl	$6, %r15d
	jb	.LBB19_53
# BB#51:
	movb	$5, 357(%rsp)
	cmpl	$7, %r15d
	jne	.LBB19_53
# BB#52:
	movb	$6, 358(%rsp)
.LBB19_53:                              # %.preheader261
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movl	%ebp, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB19_55
.LBB19_54:                              # %._crit_edge284._crit_edge
                                        #   in Loop: Header=BB19_55 Depth=1
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movl	(%rbx), %edx
	movl	4(%rbx), %r12d
.LBB19_55:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_56 Depth 2
                                        #       Child Loop BB19_57 Depth 3
                                        #     Child Loop BB19_64 Depth 2
	movl	$1, %r15d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB19_56:                              #   Parent Loop BB19_55 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB19_57 Depth 3
	movl	$8, %eax
	subl	%edx, %eax
	incl	%edx
	movl	%edx, (%rbx)
	cmpl	$8, %edx
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%r12d, %ebp
	jb	.LBB19_61
	.p2align	4, 0x90
.LBB19_57:                              # %.lr.ph.i.i.i.i229
                                        #   Parent Loop BB19_55 Depth=1
                                        #     Parent Loop BB19_56 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	shll	$8, %ebp
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jae	.LBB19_59
# BB#58:                                #   in Loop: Header=BB19_57 Depth=3
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB19_60
	.p2align	4, 0x90
.LBB19_59:                              #   in Loop: Header=BB19_57 Depth=3
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	(%rbx), %edx
.LBB19_60:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i232
                                        #   in Loop: Header=BB19_57 Depth=3
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 4(%rbx)
	addl	$-8, %edx
	movl	%edx, (%rbx)
	cmpl	$7, %edx
	ja	.LBB19_57
.LBB19_61:                              # %_ZN9NCompress6NBZip2L7ReadBitEPN5NBitm8CDecoderI9CInBufferEE.exit233
                                        #   in Loop: Header=BB19_56 Depth=2
	movl	$8388608, %eax          # imm = 0x800000
	movl	8(%rsp), %ecx           # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	testl	%r12d, %eax
	je	.LBB19_63
# BB#62:                                #   in Loop: Header=BB19_56 Depth=2
	incl	%r13d
	incq	%r15
	cmpl	16(%rsp), %r13d         # 4-byte Folded Reload
	movl	%ebp, %r12d
	jl	.LBB19_56
	jmp	.LBB19_137
.LBB19_63:                              #   in Loop: Header=BB19_55 Depth=1
	movslq	%r13d, %rax
	movb	352(%rsp,%rax), %al
	testl	%r13d, %r13d
	jle	.LBB19_65
	.p2align	4, 0x90
.LBB19_64:                              # %.lr.ph283
                                        #   Parent Loop BB19_55 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	350(%rsp,%r15), %ecx
	movb	%cl, 351(%rsp,%r15)
	decq	%r15
	cmpq	$1, %r15
	jg	.LBB19_64
.LBB19_65:                              # %._crit_edge284
                                        #   in Loop: Header=BB19_55 Depth=1
	movb	%al, 352(%rsp)
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movb	%al, (%rcx,%rdx)
	incq	%rdx
	cmpq	80(%rsp), %rdx          # 8-byte Folded Reload
	jb	.LBB19_54
# BB#66:                                # %.thread
	movslq	(%rsp), %rax            # 4-byte Folded Reload
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
.LBB19_67:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_68 Depth 2
                                        #     Child Loop BB19_74 Depth 2
                                        #       Child Loop BB19_76 Depth 3
                                        #         Child Loop BB19_78 Depth 4
                                        #         Child Loop BB19_85 Depth 4
	movl	(%rbx), %edx
	movl	4(%rbx), %r15d
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r15d, %r12d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r12d
	addl	$5, %edx
	movl	%edx, (%rbx)
	cmpl	$8, %edx
	jb	.LBB19_72
.LBB19_68:                              # %.lr.ph.i.i.i.i234
                                        #   Parent Loop BB19_67 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shll	$8, %r15d
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jae	.LBB19_70
# BB#69:                                #   in Loop: Header=BB19_68 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB19_71
.LBB19_70:                              #   in Loop: Header=BB19_68 Depth=2
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	(%rbx), %edx
.LBB19_71:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i237
                                        #   in Loop: Header=BB19_68 Depth=2
	movzbl	%al, %eax
	orl	%eax, %r15d
	movl	%r15d, 4(%rbx)
	addl	$-8, %edx
	movl	%edx, (%rbx)
	cmpl	$7, %edx
	ja	.LBB19_68
.LBB19_72:                              # %_ZN9NCompress6NBZip2L8ReadBitsEPN5NBitm8CDecoderI9CInBufferEEj.exit238.preheader
                                        #   in Loop: Header=BB19_67 Depth=1
	xorl	%eax, %eax
	cmpl	$-1, (%rsp)             # 4-byte Folded Reload
	jl	.LBB19_92
# BB#73:                                # %.preheader258.preheader
                                        #   in Loop: Header=BB19_67 Depth=1
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	shrl	$19, %r12d
	andl	$31, %r12d
	xorl	%eax, %eax
.LBB19_74:                              # %.preheader258
                                        #   Parent Loop BB19_67 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB19_76 Depth 3
                                        #         Child Loop BB19_78 Depth 4
                                        #         Child Loop BB19_85 Depth 4
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leal	-1(%r12), %eax
	cmpl	$19, %eax
	ja	.LBB19_137
# BB#75:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB19_74 Depth=2
	movl	%r15d, %ebp
.LBB19_76:                              # %.lr.ph
                                        #   Parent Loop BB19_67 Depth=1
                                        #     Parent Loop BB19_74 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB19_78 Depth 4
                                        #         Child Loop BB19_85 Depth 4
	movl	$8, %r13d
	subl	%edx, %r13d
	incl	%edx
	movl	%edx, (%rbx)
	cmpl	$8, %edx
	movl	%ebp, %r15d
	jb	.LBB19_82
# BB#77:                                # %.lr.ph.i.i.i.i239.preheader
                                        #   in Loop: Header=BB19_76 Depth=3
	movl	$8, %edx
	movl	%ebp, %r15d
	.p2align	4, 0x90
.LBB19_78:                              # %.lr.ph.i.i.i.i239
                                        #   Parent Loop BB19_67 Depth=1
                                        #     Parent Loop BB19_74 Depth=2
                                        #       Parent Loop BB19_76 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	shll	$8, %r15d
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jae	.LBB19_80
# BB#79:                                #   in Loop: Header=BB19_78 Depth=4
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB19_81
	.p2align	4, 0x90
.LBB19_80:                              #   in Loop: Header=BB19_78 Depth=4
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	(%rbx), %edx
.LBB19_81:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i242
                                        #   in Loop: Header=BB19_78 Depth=4
	movzbl	%al, %eax
	orl	%eax, %r15d
	movl	%r15d, 4(%rbx)
	addl	$-8, %edx
	movl	%edx, (%rbx)
	cmpl	$7, %edx
	ja	.LBB19_78
.LBB19_82:                              # %_ZN9NCompress6NBZip2L7ReadBitEPN5NBitm8CDecoderI9CInBufferEE.exit243
                                        #   in Loop: Header=BB19_76 Depth=3
	movl	$8388608, %eax          # imm = 0x800000
	movl	%r13d, %ecx
	shll	%cl, %eax
	testl	%ebp, %eax
	je	.LBB19_90
# BB#83:                                #   in Loop: Header=BB19_76 Depth=3
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%r15d, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	incl	%edx
	movl	%edx, (%rbx)
	cmpl	$8, %edx
	jb	.LBB19_89
# BB#84:                                # %.lr.ph.i.i.i.i244.preheader
                                        #   in Loop: Header=BB19_76 Depth=3
	movl	$8, %edx
	.p2align	4, 0x90
.LBB19_85:                              # %.lr.ph.i.i.i.i244
                                        #   Parent Loop BB19_67 Depth=1
                                        #     Parent Loop BB19_74 Depth=2
                                        #       Parent Loop BB19_76 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	shll	$8, %r15d
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jae	.LBB19_87
# BB#86:                                #   in Loop: Header=BB19_85 Depth=4
	leaq	1(%rax), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rax), %eax
	jmp	.LBB19_88
	.p2align	4, 0x90
.LBB19_87:                              #   in Loop: Header=BB19_85 Depth=4
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	(%rbx), %edx
.LBB19_88:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i.i247
                                        #   in Loop: Header=BB19_85 Depth=4
	movzbl	%al, %eax
	orl	%eax, %r15d
	movl	%r15d, 4(%rbx)
	addl	$-8, %edx
	movl	%edx, (%rbx)
	cmpl	$7, %edx
	ja	.LBB19_85
.LBB19_89:                              # %_ZN9NCompress6NBZip2L7ReadBitEPN5NBitm8CDecoderI9CInBufferEE.exit248
                                        #   in Loop: Header=BB19_76 Depth=3
	shrl	$22, %ebp
	andl	$2, %ebp
	incl	%r12d
	subl	%ebp, %r12d
	leal	-1(%r12), %eax
	cmpl	$19, %eax
	movl	%r15d, %ebp
	jbe	.LBB19_76
	jmp	.LBB19_137
.LBB19_90:                              # %_ZN9NCompress6NBZip2L8ReadBitsEPN5NBitm8CDecoderI9CInBufferEEj.exit238
                                        #   in Loop: Header=BB19_74 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movb	%r12b, 352(%rsp,%rax)
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	leaq	1(%rax), %rax
	jle	.LBB19_74
# BB#91:                                # %.preheader259
                                        #   in Loop: Header=BB19_67 Depth=1
	cmpl	$257, %eax              # imm = 0x101
	movq	32(%rsp), %rbp          # 8-byte Reload
	jg	.LBB19_93
.LBB19_92:                              # %.lr.ph280.preheader
                                        #   in Loop: Header=BB19_67 Depth=1
	cltq
	leaq	352(%rsp,%rax), %rdi
	movl	$257, %edx              # imm = 0x101
	subl	%eax, %edx
	incq	%rdx
	xorl	%esi, %esi
	callq	memset
.LBB19_93:                              # %._crit_edge
                                        #   in Loop: Header=BB19_67 Depth=1
	imulq	$1712, %rbp, %rdi       # imm = 0x6B0
	addq	48(%rsp), %rdi          # 8-byte Folded Reload
	leaq	352(%rsp), %rsi
	callq	_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE14SetCodeLengthsEPKh
	testb	%al, %al
	je	.LBB19_137
# BB#94:                                #   in Loop: Header=BB19_67 Depth=1
	incq	%rbp
	cmpq	72(%rsp), %rbp          # 8-byte Folded Reload
	jl	.LBB19_67
# BB#95:                                # %.preheader257.preheader
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	memset
	movq	(%rsp), %rax            # 8-byte Reload
	leal	1(%rax), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
.LBB19_96:                              # %.backedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_107 Depth 2
                                        #     Child Loop BB19_111 Depth 2
                                        #     Child Loop BB19_116 Depth 2
                                        #     Child Loop BB19_120 Depth 2
                                        #     Child Loop BB19_127 Depth 2
	testl	%r12d, %r12d
	jne	.LBB19_99
# BB#97:                                #   in Loop: Header=BB19_96 Depth=1
	movl	8(%rsp), %ecx           # 4-byte Reload
	cmpl	64(%rsp), %ecx          # 4-byte Folded Reload
	jae	.LBB19_137
# BB#98:                                #   in Loop: Header=BB19_96 Depth=1
	movl	%ecx, %eax
	incl	%ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movq	56(%rsp), %rcx          # 8-byte Reload
	movzbl	(%rcx,%rax), %eax
	imulq	$1712, %rax, %rbp       # imm = 0x6B0
	addq	48(%rsp), %rbp          # 8-byte Folded Reload
	movl	$50, %r12d
.LBB19_99:                              #   in Loop: Header=BB19_96 Depth=1
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE12DecodeSymbolIN5NBitm8CDecoderI9CInBufferEEEEjPT_
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$1, %eax
	ja	.LBB19_101
# BB#100:                               #   in Loop: Header=BB19_96 Depth=1
	incl	%eax
	leal	1(%r15), %edx
	movl	%r15d, %ecx
	shll	%cl, %eax
	addl	%r14d, %eax
	movl	28(%rsp), %ecx          # 4-byte Reload
	subl	%r13d, %ecx
	cmpl	%eax, %ecx
	movl	$31, %ecx
	movl	$1, %esi
	cmovbl	%esi, %ecx
	movl	%edx, %r15d
	movl	%eax, %r14d
	jmp	.LBB19_133
.LBB19_101:                             #   in Loop: Header=BB19_96 Depth=1
	testl	%r14d, %r14d
	je	.LBB19_122
# BB#102:                               #   in Loop: Header=BB19_96 Depth=1
	movq	%rbp, %r11
	movq	96(%rsp), %rdx
	movzbl	%dl, %r10d
	movzbl	%dl, %edx
	movq	40(%rsp), %rcx          # 8-byte Reload
	addl	%r14d, (%rcx,%rdx,4)
	cmpl	$8, %r14d
	movl	%r13d, %edi
	movl	%r14d, %esi
	jb	.LBB19_114
# BB#103:                               # %min.iters.checked
                                        #   in Loop: Header=BB19_96 Depth=1
	movl	%r14d, %r9d
	andl	$-8, %r9d
	movl	%r13d, %edi
	movl	%r14d, %esi
	je	.LBB19_114
# BB#104:                               # %vector.scevcheck
                                        #   in Loop: Header=BB19_96 Depth=1
	leal	-1(%r14), %esi
	leal	256(%r13), %ebp
	addl	%ebp, %esi
	movl	%r13d, %edi
	movl	%r14d, %esi
	jb	.LBB19_114
# BB#105:                               # %vector.ph
                                        #   in Loop: Header=BB19_96 Depth=1
	movd	%r10d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leal	-8(%r9), %r8d
	movl	%r8d, %edi
	shrl	$3, %edi
	incl	%edi
	andl	$3, %edi
	je	.LBB19_108
# BB#106:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB19_96 Depth=1
	negl	%edi
	xorl	%esi, %esi
.LBB19_107:                             # %vector.body.prol
                                        #   Parent Loop BB19_96 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rbp,%rsi), %edx
	movdqu	%xmm0, (%rcx,%rdx,4)
	movdqu	%xmm0, 16(%rcx,%rdx,4)
	addl	$8, %esi
	incl	%edi
	jne	.LBB19_107
	jmp	.LBB19_109
.LBB19_108:                             #   in Loop: Header=BB19_96 Depth=1
	xorl	%esi, %esi
.LBB19_109:                             # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB19_96 Depth=1
	cmpl	$24, %r8d
	jb	.LBB19_112
# BB#110:                               # %vector.ph.new
                                        #   in Loop: Header=BB19_96 Depth=1
	movl	%r13d, %edi
	movl	%r9d, %ebp
.LBB19_111:                             # %vector.body
                                        #   Parent Loop BB19_96 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	256(%rsi,%rdi), %edx
	movdqu	%xmm0, (%rcx,%rdx,4)
	movdqu	%xmm0, 16(%rcx,%rdx,4)
	leal	264(%rsi,%rdi), %edx
	movdqu	%xmm0, (%rcx,%rdx,4)
	movdqu	%xmm0, 16(%rcx,%rdx,4)
	leal	272(%rsi,%rdi), %edx
	movdqu	%xmm0, (%rcx,%rdx,4)
	movdqu	%xmm0, 16(%rcx,%rdx,4)
	leal	280(%rsi,%rdi), %edx
	movdqu	%xmm0, (%rcx,%rdx,4)
	movdqu	%xmm0, 16(%rcx,%rdx,4)
	addl	$-32, %ebp
	addl	$32, %edi
	cmpl	%ebp, %esi
	jne	.LBB19_111
.LBB19_112:                             # %middle.block
                                        #   in Loop: Header=BB19_96 Depth=1
	cmpl	%r9d, %r14d
	je	.LBB19_121
# BB#113:                               #   in Loop: Header=BB19_96 Depth=1
	leal	(%r13,%r9), %edi
	movl	%r14d, %esi
	subl	%r9d, %esi
.LBB19_114:                             # %scalar.ph.preheader
                                        #   in Loop: Header=BB19_96 Depth=1
	leal	-1(%rsi), %r8d
	movl	%esi, %ebp
	andl	$3, %ebp
	je	.LBB19_117
# BB#115:                               # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB19_96 Depth=1
	negl	%ebp
.LBB19_116:                             # %scalar.ph.prol
                                        #   Parent Loop BB19_96 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1(%rdi), %edx
	addl	$256, %edi              # imm = 0x100
	movl	%r10d, (%rcx,%rdi,4)
	decl	%esi
	incl	%ebp
	movl	%edx, %edi
	jne	.LBB19_116
	jmp	.LBB19_118
.LBB19_117:                             #   in Loop: Header=BB19_96 Depth=1
	movl	%edi, %edx
.LBB19_118:                             # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB19_96 Depth=1
	cmpl	$3, %r8d
	jb	.LBB19_121
# BB#119:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB19_96 Depth=1
	addl	$259, %edx              # imm = 0x103
.LBB19_120:                             # %scalar.ph
                                        #   Parent Loop BB19_96 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-3(%rdx), %edi
	movl	%r10d, (%rcx,%rdi,4)
	leal	-2(%rdx), %edi
	movl	%r10d, (%rcx,%rdi,4)
	leal	-1(%rdx), %edi
	movl	%r10d, (%rcx,%rdi,4)
	movl	%edx, %edi
	movl	%r10d, (%rcx,%rdi,4)
	addl	$4, %edx
	addl	$-4, %esi
	jne	.LBB19_120
.LBB19_121:                             # %.loopexit.loopexit
                                        #   in Loop: Header=BB19_96 Depth=1
	addl	%r14d, %r13d
	xorl	%r15d, %r15d
	movq	%r11, %rbp
.LBB19_122:                             # %.loopexit
                                        #   in Loop: Header=BB19_96 Depth=1
	cmpl	(%rsp), %eax            # 4-byte Folded Reload
	jbe	.LBB19_124
# BB#123:                               #   in Loop: Header=BB19_96 Depth=1
	cmpl	16(%rsp), %eax          # 4-byte Folded Reload
	movl	$1, %ecx
	movl	$30, %eax
	cmovel	%eax, %ecx
	xorl	%r14d, %r14d
	jmp	.LBB19_133
.LBB19_124:                             #   in Loop: Header=BB19_96 Depth=1
	movl	%eax, %edi
	decl	%edi
	movl	%edi, %r8d
	shrl	$3, %r8d
	leal	-8(,%rax,8), %ecx
	andl	$56, %ecx
	leal	(,%r8,8), %eax
	movq	96(%rsp,%rax), %rax
	shrq	%cl, %rax
	movzbl	%al, %r10d
	xorl	%esi, %esi
	btl	$3, %edi
	jae	.LBB19_126
# BB#125:                               #   in Loop: Header=BB19_96 Depth=1
	movq	96(%rsp), %rax
	movq	%rax, %rsi
	shlq	$8, %rsi
	orq	%r10, %rsi
	movq	%rsi, 96(%rsp)
	shrq	$56, %rax
	decl	%r8d
	movl	$1, %esi
	movq	%rax, %r10
.LBB19_126:                             # %.preheader.i
                                        #   in Loop: Header=BB19_96 Depth=1
	movq	%rbp, %r11
	leaq	96(%rsp,%rsi,8), %r9
	movq	96(%rsp,%rsi,8), %rdi
	cmpl	%r8d, %esi
	jae	.LBB19_128
.LBB19_127:                             # %.lr.ph.i
                                        #   Parent Loop BB19_96 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1(%rsi), %eax
	movq	96(%rsp,%rax,8), %rbp
	movq	%rbp, %rdx
	shldq	$8, %rdi, %rdx
	shlq	$8, %rdi
	orq	%r10, %rdi
	movq	%rdi, (%r9)
	movq	%rdx, 96(%rsp,%rax,8)
	shrq	$56, %rbp
	addl	$2, %esi
	leaq	96(%rsp,%rsi,8), %r9
	movq	96(%rsp,%rsi,8), %rdi
	cmpl	%r8d, %esi
	movq	%rbp, %r10
	jb	.LBB19_127
	jmp	.LBB19_129
.LBB19_128:                             #   in Loop: Header=BB19_96 Depth=1
	movq	%r10, %rbp
.LBB19_129:                             # %_ZN9NCompress12CMtf8Decoder10GetAndMoveEj.exit
                                        #   in Loop: Header=BB19_96 Depth=1
	movl	$256, %eax              # imm = 0x100
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rax
	leaq	-1(%rax), %rcx
	negq	%rax
	andq	%rdi, %rax
	shlq	$8, %rdi
	orq	%rbp, %rdi
	andq	%rcx, %rdi
	orq	%rax, %rdi
	movq	%rdi, (%r9)
	cmpl	28(%rsp), %r13d         # 4-byte Folded Reload
	jae	.LBB19_131
# BB#130:                               #   in Loop: Header=BB19_96 Depth=1
	movq	96(%rsp), %rax
	movzbl	%al, %ecx
	movzbl	%al, %eax
	movq	40(%rsp), %rdx          # 8-byte Reload
	incl	(%rdx,%rax,4)
	leal	1(%r13), %eax
	addl	$256, %r13d             # imm = 0x100
	movl	%ecx, (%rdx,%r13,4)
	xorl	%ecx, %ecx
	movl	%eax, %r13d
	xorl	%r14d, %r14d
	jmp	.LBB19_132
.LBB19_131:                             #   in Loop: Header=BB19_96 Depth=1
	xorl	%r14d, %r14d
	movl	$1, %ecx
.LBB19_132:                             #   in Loop: Header=BB19_96 Depth=1
	movq	%r11, %rbp
.LBB19_133:                             #   in Loop: Header=BB19_96 Depth=1
	decl	%r12d
	andb	$31, %cl
	je	.LBB19_96
# BB#134:                               #   in Loop: Header=BB19_96 Depth=1
	cmpb	$31, %cl
	je	.LBB19_96
# BB#135:
	movl	$1, %ebp
	cmpb	$30, %cl
	jne	.LBB19_138
# BB#136:                               # %.thread251
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	%r13d, (%rax)
	xorl	%ebp, %ebp
	movq	672(%rsp), %rax
	cmpl	%r13d, (%rax)
	setae	%bpl
	jmp	.LBB19_138
.LBB19_137:                             # %.critedge
	movl	$1, %ebp
.LBB19_138:
	movl	%ebp, %eax
	addq	$616, %rsp              # imm = 0x268
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	_ZN9NCompress6NBZip2L9ReadBlockEPN5NBitm8CDecoderI9CInBufferEEPjjPhPNS_8NHuffman8CDecoderILi20ELj258EEES6_S6_Pb, .Lfunc_end19-_ZN9NCompress6NBZip2L9ReadBlockEPN5NBitm8CDecoderI9CInBufferEEPjjPhPNS_8NHuffman8CDecoderILi20ELj258EEES6_S6_Pb
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip2L16DecodeBlock2RandEPKjjjR10COutBuffer,@function
_ZN9NCompress6NBZip2L16DecodeBlock2RandEPKjjjR10COutBuffer: # @_ZN9NCompress6NBZip2L16DecodeBlock2RandEPKjjjR10COutBuffer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi143:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi144:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi145:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi146:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi147:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi148:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi149:
	.cfi_def_cfa_offset 80
.Lcfi150:
	.cfi_offset %rbx, -56
.Lcfi151:
	.cfi_offset %r12, -48
.Lcfi152:
	.cfi_offset %r13, -40
.Lcfi153:
	.cfi_offset %r14, -32
.Lcfi154:
	.cfi_offset %r15, -24
.Lcfi155:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movl	%esi, %r15d
	movl	%edx, %eax
	movl	(%rdi,%rax,4), %eax
	shrl	$8, %eax
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	(%rdi,%rax,4), %ecx
	movzbl	%cl, %r12d
	xorl	%edx, %edx
	movl	$617, %r14d             # imm = 0x269
	movl	$-1, %ebp
	movl	$1, 4(%rsp)             # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB20_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_7 Depth 2
	movzbl	%cl, %r13d
	shrl	$8, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%rcx,4), %ecx
	testl	%r14d, %r14d
	jne	.LBB20_3
# BB#2:                                 #   in Loop: Header=BB20_1 Depth=1
	xorl	$1, %r13d
	movl	4(%rsp), %esi           # 4-byte Reload
	movl	%esi, %eax
	incl	%esi
	movzwl	_ZN9NCompress6NBZip2L9kRandNumsE(%rax,%rax), %r14d
	andl	$511, %esi              # imm = 0x1FF
	movl	%esi, 4(%rsp)           # 4-byte Spill
.LBB20_3:                               #   in Loop: Header=BB20_1 Depth=1
	cmpl	$4, %edx
	jne	.LBB20_11
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB20_1 Depth=1
	xorl	%edx, %edx
	testl	%r13d, %r13d
	je	.LBB20_5
# BB#6:                                 # %.lr.ph
                                        #   in Loop: Header=BB20_1 Depth=1
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB20_7:                               #   Parent Loop BB20_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %eax
	shrl	$24, %eax
	xorl	%r12d, %eax
	shll	$8, %ebp
	xorl	_ZN9CBZip2Crc5TableE(,%rax,4), %ebp
	movq	(%rbx), %rax
	movl	8(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 8(%rbx)
	movb	%r12b, (%rax,%rcx)
	movl	8(%rbx), %eax
	cmpl	12(%rbx), %eax
	jne	.LBB20_9
# BB#8:                                 #   in Loop: Header=BB20_7 Depth=2
	movq	%rbx, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB20_9:                               # %_ZN10COutBuffer9WriteByteEh.exit48
                                        #   in Loop: Header=BB20_7 Depth=2
	decl	%r13d
	jne	.LBB20_7
# BB#10:                                #   in Loop: Header=BB20_1 Depth=1
	movl	%r12d, %r13d
	movq	8(%rsp), %rcx           # 8-byte Reload
	xorl	%edx, %edx
	jmp	.LBB20_14
	.p2align	4, 0x90
.LBB20_11:                              #   in Loop: Header=BB20_1 Depth=1
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	incl	%edx
	cmpl	%r12d, %r13d
	movl	$1, %eax
	cmovnel	%eax, %edx
	movl	%edx, %r12d
	movl	%ebp, %eax
	shrl	$24, %eax
	xorl	%r13d, %eax
	shll	$8, %ebp
	xorl	_ZN9CBZip2Crc5TableE(,%rax,4), %ebp
	movq	(%rbx), %rax
	movl	8(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 8(%rbx)
	movb	%r13b, (%rax,%rcx)
	movl	8(%rbx), %eax
	cmpl	12(%rbx), %eax
	jne	.LBB20_13
# BB#12:                                #   in Loop: Header=BB20_1 Depth=1
	movq	%rbx, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB20_13:                              # %_ZN10COutBuffer9WriteByteEh.exit
                                        #   in Loop: Header=BB20_1 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%r12d, %edx
	jmp	.LBB20_14
.LBB20_5:                               #   in Loop: Header=BB20_1 Depth=1
	movl	%r12d, %r13d
	.p2align	4, 0x90
.LBB20_14:                              # %_ZN10COutBuffer9WriteByteEh.exit
                                        #   in Loop: Header=BB20_1 Depth=1
	decl	%r14d
	decl	%r15d
	movl	%r13d, %r12d
	jne	.LBB20_1
# BB#15:
	notl	%ebp
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	_ZN9NCompress6NBZip2L16DecodeBlock2RandEPKjjjR10COutBuffer, .Lfunc_end20-_ZN9NCompress6NBZip2L16DecodeBlock2RandEPKjjjR10COutBuffer
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamRbP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamRbP21ICompressProgressInfo,@function
_ZN9NCompress6NBZip28CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamRbP21ICompressProgressInfo: # @_ZN9NCompress6NBZip28CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamRbP21ICompressProgressInfo
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi156:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi157:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi158:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi159:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi160:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi161:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi162:
	.cfi_def_cfa_offset 80
.Lcfi163:
	.cfi_offset %rbx, -56
.Lcfi164:
	.cfi_offset %r12, -48
.Lcfi165:
	.cfi_offset %r13, -40
.Lcfi166:
	.cfi_offset %r14, -32
.Lcfi167:
	.cfi_offset %r15, -24
.Lcfi168:
	.cfi_offset %rbp, -16
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%rcx, %r13
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movb	$0, (%r13)
	leaq	344(%rbx), %r12
.Ltmp109:
	movl	$131072, %esi           # imm = 0x20000
	movq	%r12, %rdi
	callq	_ZN9CInBuffer6CreateEj
.Ltmp110:
# BB#1:                                 # %_ZN5NBitm8CDecoderI9CInBufferE6CreateEj.exit
	movl	$-2147024882, %r14d     # imm = 0x8007000E
	testb	%al, %al
	je	.LBB21_56
# BB#2:
	leaq	24(%rbx), %r15
.Ltmp111:
	movl	$131072, %esi           # imm = 0x20000
	movq	%r15, %rdi
	callq	_ZN10COutBuffer6CreateEj
.Ltmp112:
# BB#3:
	testb	%al, %al
	je	.LBB21_56
# BB#4:
	testq	%rbp, %rbp
	je	.LBB21_6
# BB#5:
.Ltmp113:
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
.Ltmp114:
.LBB21_6:                               # %_ZN5NBitm8CDecoderI9CInBufferE9SetStreamEP19ISequentialInStream.exit
	leaq	336(%rbx), %r14
	cmpb	$0, 28680(%rbx)
	movq	%rbp, (%rsp)            # 8-byte Spill
	je	.LBB21_7
# BB#8:
.Ltmp116:
	movq	%r12, %rdi
	callq	_ZN9CInBuffer4InitEv
.Ltmp117:
# BB#9:                                 # %.noexc
	movl	$32, 336(%rbx)
	movl	340(%rbx), %ebp
	movl	$32, %ecx
	.p2align	4, 0x90
.LBB21_10:                              # =>This Inner Loop Header: Depth=1
	movq	344(%rbx), %rax
	cmpq	352(%rbx), %rax
	jae	.LBB21_11
# BB#13:                                #   in Loop: Header=BB21_10 Depth=1
	leaq	1(%rax), %rdx
	movq	%rdx, (%r12)
	movzbl	(%rax), %eax
	jmp	.LBB21_14
	.p2align	4, 0x90
.LBB21_11:                              #   in Loop: Header=BB21_10 Depth=1
.Ltmp118:
	movq	%r12, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.Ltmp119:
# BB#12:                                # %.noexc27
                                        #   in Loop: Header=BB21_10 Depth=1
	movl	(%r14), %ecx
.LBB21_14:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i
                                        #   in Loop: Header=BB21_10 Depth=1
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 340(%rbx)
	addl	$-8, %ecx
	movl	%ecx, 336(%rbx)
	cmpl	$7, %ecx
	ja	.LBB21_10
# BB#15:                                # %_ZN5NBitm8CDecoderI9CInBufferE4InitEv.exit
	movb	$0, 28680(%rbx)
	jmp	.LBB21_16
.LBB21_7:                               # %_ZN5NBitm8CDecoderI9CInBufferE9SetStreamEP19ISequentialInStream.exit._crit_edge
	movl	(%r14), %ecx
.LBB21_16:
	movq	344(%rbx), %rax
	movq	376(%rbx), %rdx
	addq	%rax, %rdx
	subq	360(%rbx), %rdx
	movl	$32, %esi
	subl	%ecx, %esi
	shrl	$3, %esi
	subq	%rsi, %rdx
	movq	%rdx, 28672(%rbx)
	movl	%ecx, %edx
	negl	%edx
	andl	$7, %edx
	addl	%ecx, %edx
	movl	%edx, 336(%rbx)
	cmpl	$8, %edx
	jb	.LBB21_36
# BB#17:                                # %.lr.ph.i.i.i
	movl	340(%rbx), %ebp
	cmpq	352(%rbx), %rax
	jb	.LBB21_33
	jmp	.LBB21_19
	.p2align	4, 0x90
.LBB21_35:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i._crit_edge
	movq	(%r12), %rax
	cmpq	352(%rbx), %rax
	jae	.LBB21_19
.LBB21_33:
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	movb	(%rax), %al
	jmp	.LBB21_34
	.p2align	4, 0x90
.LBB21_19:
.Ltmp121:
	movq	%r12, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.Ltmp122:
# BB#20:                                # %.noexc31
	movl	(%r14), %edx
.LBB21_34:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i
	movl	%ebp, %ecx
	shll	$8, %ecx
	movzbl	%al, %ebp
	orl	%ecx, %ebp
	movl	%ebp, 340(%rbx)
	addl	$-8, %edx
	movl	%edx, 336(%rbx)
	cmpl	$8, %edx
	jae	.LBB21_35
.LBB21_36:                              # %_ZN5NBitm8CDecoderI9CInBufferE11AlignToByteEv.exit
.Ltmp124:
	movq	%r15, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
.Ltmp125:
# BB#37:
.Ltmp126:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer4InitEv
.Ltmp127:
# BB#38:
.Ltmp128:
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	_ZN9NCompress6NBZip28CDecoder10DecodeFileERbP21ICompressProgressInfo
	movl	%eax, %r14d
.Ltmp129:
# BB#39:
	testl	%r14d, %r14d
	movq	(%rsp), %rbp            # 8-byte Reload
	je	.LBB21_40
# BB#41:                                # %_ZN9NCompress6NBZip28CDecoder5FlushEv.exit
.Ltmp133:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer5FlushEv
.Ltmp134:
	jmp	.LBB21_42
.LBB21_40:
.Ltmp135:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer5FlushEv
	movl	%eax, %r14d
.Ltmp136:
.LBB21_42:                              # %._crit_edge.i34
	testq	%rbp, %rbp
	je	.LBB21_46
# BB#43:
	movq	368(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_46
# BB#44:
	movq	(%rdi), %rax
.Ltmp143:
	callq	*16(%rax)
.Ltmp144:
# BB#45:                                # %.noexc37
	movq	$0, 368(%rbx)
.LBB21_46:                              # %_ZN5NBitm8CDecoderI9CInBufferE13ReleaseStreamEv.exit.i.i35
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_56
# BB#47:
	movq	(%rdi), %rax
.Ltmp145:
	callq	*16(%rax)
.Ltmp146:
# BB#48:                                # %.noexc38
	movq	$0, 48(%rbx)
	jmp	.LBB21_56
.LBB21_21:
.Ltmp137:
	movq	%rdx, %rbp
	movq	%rax, %r14
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	jne	.LBB21_27
	jmp	.LBB21_30
.LBB21_49:
.Ltmp147:
	jmp	.LBB21_50
.LBB21_24:                              # %.loopexit.split-lp.loopexit.split-lp
.Ltmp130:
	jmp	.LBB21_25
.LBB21_58:
.Ltmp115:
.LBB21_50:                              # %_ZN9NCompress6NBZip28CDecoder15CDecoderFlusherD2Ev.exit
	movq	%rdx, %rbp
	movq	%rax, %r14
	jmp	.LBB21_51
.LBB21_22:                              # %.loopexit
.Ltmp123:
	jmp	.LBB21_25
.LBB21_23:                              # %.loopexit.split-lp.loopexit
.Ltmp120:
.LBB21_25:                              # %.loopexit.split-lp
	movq	%rdx, %rbp
	movq	%rax, %r14
.Ltmp131:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer5FlushEv
.Ltmp132:
# BB#26:                                # %._crit_edge.i
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB21_30
.LBB21_27:
	movq	368(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_30
# BB#28:
	movq	(%rdi), %rax
.Ltmp138:
	callq	*16(%rax)
.Ltmp139:
# BB#29:                                # %.noexc29
	movq	$0, 368(%rbx)
.LBB21_30:                              # %_ZN5NBitm8CDecoderI9CInBufferE13ReleaseStreamEv.exit.i.i
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_51
# BB#31:
	movq	(%rdi), %rax
.Ltmp140:
	callq	*16(%rax)
.Ltmp141:
# BB#32:                                # %.noexc30
	movq	$0, 48(%rbx)
.LBB21_51:                              # %_ZN9NCompress6NBZip28CDecoder15CDecoderFlusherD2Ev.exit
	movq	%r14, %rdi
	cmpl	$3, %ebp
	jne	.LBB21_53
# BB#52:
	callq	__cxa_begin_catch
	jmp	.LBB21_54
.LBB21_53:
	callq	__cxa_begin_catch
	cmpl	$2, %ebp
	jne	.LBB21_55
.LBB21_54:
	movl	(%rax), %r14d
	callq	__cxa_end_catch
	jmp	.LBB21_56
.LBB21_55:
	callq	__cxa_end_catch
	movl	$-2147467259, %r14d     # imm = 0x80004005
.LBB21_56:                              # %_ZN9NCompress6NBZip28CDecoder15CDecoderFlusherD2Ev.exit39
	movl	%r14d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_57:
.Ltmp142:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end21:
	.size	_ZN9NCompress6NBZip28CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamRbP21ICompressProgressInfo, .Lfunc_end21-_ZN9NCompress6NBZip28CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamRbP21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\227\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp109-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp114-.Ltmp109       #   Call between .Ltmp109 and .Ltmp114
	.long	.Ltmp115-.Lfunc_begin7  #     jumps to .Ltmp115
	.byte	5                       #   On action: 3
	.long	.Ltmp116-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp130-.Lfunc_begin7  #     jumps to .Ltmp130
	.byte	5                       #   On action: 3
	.long	.Ltmp118-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp119-.Ltmp118       #   Call between .Ltmp118 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin7  #     jumps to .Ltmp120
	.byte	5                       #   On action: 3
	.long	.Ltmp121-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp123-.Lfunc_begin7  #     jumps to .Ltmp123
	.byte	5                       #   On action: 3
	.long	.Ltmp124-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp129-.Ltmp124       #   Call between .Ltmp124 and .Ltmp129
	.long	.Ltmp130-.Lfunc_begin7  #     jumps to .Ltmp130
	.byte	5                       #   On action: 3
	.long	.Ltmp133-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp147-.Lfunc_begin7  #     jumps to .Ltmp147
	.byte	5                       #   On action: 3
	.long	.Ltmp135-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp136-.Ltmp135       #   Call between .Ltmp135 and .Ltmp136
	.long	.Ltmp137-.Lfunc_begin7  #     jumps to .Ltmp137
	.byte	5                       #   On action: 3
	.long	.Ltmp143-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Ltmp146-.Ltmp143       #   Call between .Ltmp143 and .Ltmp146
	.long	.Ltmp147-.Lfunc_begin7  #     jumps to .Ltmp147
	.byte	5                       #   On action: 3
	.long	.Ltmp131-.Lfunc_begin7  # >> Call Site 9 <<
	.long	.Ltmp141-.Ltmp131       #   Call between .Ltmp131 and .Ltmp141
	.long	.Ltmp142-.Lfunc_begin7  #     jumps to .Ltmp142
	.byte	1                       #   On action: 1
	.long	.Ltmp141-.Lfunc_begin7  # >> Call Site 10 <<
	.long	.Lfunc_end21-.Ltmp141   #   Call between .Ltmp141 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
	.byte	3                       # >> Action Record 3 <<
                                        #   Catch TypeInfo 3
	.byte	125                     #   Continue to action 2
                                        # >> Catch TypeInfos <<
	.long	_ZTI18CInBufferException # TypeInfo 3
	.long	_ZTI19COutBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress6NBZip28CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress6NBZip28CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress6NBZip28CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi169:
	.cfi_def_cfa_offset 16
	movb	$1, 28680(%rdi)
	leaq	7(%rsp), %rcx
	movq	%r9, %r8
	callq	_ZN9NCompress6NBZip28CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamRbP21ICompressProgressInfo
	testl	%eax, %eax
	jne	.LBB22_2
# BB#1:
	movzbl	7(%rsp), %eax
	xorl	$1, %eax
.LBB22_2:
	popq	%rcx
	retq
.Lfunc_end22:
	.size	_ZN9NCompress6NBZip28CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end22-_ZN9NCompress6NBZip28CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CDecoder10CodeResumeEP20ISequentialOutStreamRbP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder10CodeResumeEP20ISequentialOutStreamRbP21ICompressProgressInfo,@function
_ZN9NCompress6NBZip28CDecoder10CodeResumeEP20ISequentialOutStreamRbP21ICompressProgressInfo: # @_ZN9NCompress6NBZip28CDecoder10CodeResumeEP20ISequentialOutStreamRbP21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	movq	%rcx, %rax
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	xorl	%esi, %esi
	movq	%rax, %r8
	jmp	_ZN9NCompress6NBZip28CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamRbP21ICompressProgressInfo # TAILCALL
.Lfunc_end23:
	.size	_ZN9NCompress6NBZip28CDecoder10CodeResumeEP20ISequentialOutStreamRbP21ICompressProgressInfo, .Lfunc_end23-_ZN9NCompress6NBZip28CDecoder10CodeResumeEP20ISequentialOutStreamRbP21ICompressProgressInfo
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CDecoder11SetInStreamEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder11SetInStreamEP19ISequentialInStream,@function
_ZN9NCompress6NBZip28CDecoder11SetInStreamEP19ISequentialInStream: # @_ZN9NCompress6NBZip28CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi170:
	.cfi_def_cfa_offset 16
	addq	$344, %rdi              # imm = 0x158
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZN9NCompress6NBZip28CDecoder11SetInStreamEP19ISequentialInStream, .Lfunc_end24-_ZN9NCompress6NBZip28CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CDecoder15ReleaseInStreamEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder15ReleaseInStreamEv,@function
_ZN9NCompress6NBZip28CDecoder15ReleaseInStreamEv: # @_ZN9NCompress6NBZip28CDecoder15ReleaseInStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi171:
	.cfi_def_cfa_offset 16
.Lcfi172:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	368(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 368(%rbx)
.LBB25_2:                               # %_ZN5NBitm8CDecoderI9CInBufferE13ReleaseStreamEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end25:
	.size	_ZN9NCompress6NBZip28CDecoder15ReleaseInStreamEv, .Lfunc_end25-_ZN9NCompress6NBZip28CDecoder15ReleaseInStreamEv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip2L8MFThreadEPv,@function
_ZN9NCompress6NBZip2L8MFThreadEPv:      # @_ZN9NCompress6NBZip2L8MFThreadEPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi173:
	.cfi_def_cfa_offset 16
	callq	_ZN9NCompress6NBZip26CState10ThreadFuncEv
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end26:
	.size	_ZN9NCompress6NBZip2L8MFThreadEPv, .Lfunc_end26-_ZN9NCompress6NBZip2L8MFThreadEPv
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip26CState12FinishStreamEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip26CState12FinishStreamEv,@function
_ZN9NCompress6NBZip26CState12FinishStreamEv: # @_ZN9NCompress6NBZip26CState12FinishStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi174:
	.cfi_def_cfa_offset 16
.Lcfi175:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movb	$1, 28869(%rax)
	leaq	40(%rbx), %rdi
	callq	Event_Set
	movq	8(%rbx), %rdi
	addq	$28816, %rdi            # imm = 0x7090
	callq	pthread_mutex_unlock
	movq	8(%rbx), %rdi
	addq	$28872, %rdi            # imm = 0x70C8
	callq	Event_Wait
	addq	$144, %rbx
	movq	%rbx, %rdi
	popq	%rbx
	jmp	Event_Set               # TAILCALL
.Lfunc_end27:
	.size	_ZN9NCompress6NBZip26CState12FinishStreamEv, .Lfunc_end27-_ZN9NCompress6NBZip26CState12FinishStreamEv
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip26CState10ThreadFuncEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip26CState10ThreadFuncEv,@function
_ZN9NCompress6NBZip26CState10ThreadFuncEv: # @_ZN9NCompress6NBZip26CState10ThreadFuncEv
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%rbp
.Lcfi176:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi177:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi178:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi179:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi180:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi181:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi182:
	.cfi_def_cfa_offset 160
.Lcfi183:
	.cfi_offset %rbx, -56
.Lcfi184:
	.cfi_offset %r12, -48
.Lcfi185:
	.cfi_offset %r13, -40
.Lcfi186:
	.cfi_offset %r14, -32
.Lcfi187:
	.cfi_offset %r15, -24
.Lcfi188:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	8(%r12), %rdi
	addq	$28712, %rdi            # imm = 0x7028
.Lcfi189:
	.cfi_escape 0x2e, 0x00
	callq	Event_Wait
	movq	8(%r12), %rdi
	addq	$28816, %rdi            # imm = 0x7090
.Lcfi190:
	.cfi_escape 0x2e, 0x00
	callq	pthread_mutex_lock
	movq	8(%r12), %rdi
	cmpb	$0, 28868(%rdi)
	jne	.LBB28_6
# BB#1:                                 # %.lr.ph
	leaq	40(%r12), %r15
	leaq	144(%r12), %r13
	xorl	%ebp, %ebp
	leaq	14(%rsp), %rbx
	jmp	.LBB28_2
.LBB28_26:                              # %_ZN9NCompress6NBZip26CState12FinishStreamEv.exit
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	344(%rdi), %rcx
	addq	376(%rdi), %rcx
	subq	360(%rdi), %rcx
	movl	$32, %eax
	subl	336(%rdi), %eax
	shrl	$3, %eax
	subq	%rax, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
.LBB28_27:                              #   in Loop: Header=BB28_2 Depth=1
	addq	$28816, %rdi            # imm = 0x7090
.Lcfi191:
	.cfi_escape 0x2e, 0x00
	callq	pthread_mutex_unlock
	movq	(%r12), %rax
	movl	28(%rsp), %ebx
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB28_28:                              #   Parent Loop BB28_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax,%rcx,4), %esi
	addl	%edx, %esi
	movl	%edx, (%rax,%rcx,4)
	movl	4(%rax,%rcx,4), %edx
	addl	%esi, %edx
	movl	%esi, 4(%rax,%rcx,4)
	movl	8(%rax,%rcx,4), %esi
	addl	%edx, %esi
	movl	%edx, 8(%rax,%rcx,4)
	movl	12(%rax,%rcx,4), %edx
	addl	%esi, %edx
	movl	%esi, 12(%rax,%rcx,4)
	addq	$4, %rcx
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB28_28
# BB#29:                                #   in Loop: Header=BB28_2 Depth=1
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB28_30:                              #   Parent Loop BB28_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	1024(%rax,%rdx,4), %esi
	movl	(%rax,%rsi,4), %edi
	leal	1(%rdi), %ebp
	movl	%ebp, (%rax,%rsi,4)
	orl	%ecx, 1024(%rax,%rdi,4)
	incq	%rdx
	addl	$256, %ecx              # imm = 0x100
	cmpq	%rbx, %rdx
	jb	.LBB28_30
# BB#31:                                # %_ZN9NCompress6NBZip2L12DecodeBlock1EPjj.exit
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movq	8(%r12), %rax
	movq	28696(%rax), %rax
	imulq	$608, %r14, %rcx        # imm = 0x260
	leaq	248(%rax,%rcx), %rdi
	movb	$1, %bpl
.Ltmp171:
.Lcfi192:
	.cfi_escape 0x2e, 0x00
	callq	Event_Wait
.Ltmp172:
# BB#32:                                # %_ZN8NWindows16NSynchronization10CBaseEvent4LockEv.exit
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	8(%r12), %r14
	movb	$1, %bl
	cmpb	$0, 28870(%r14)
	je	.LBB28_33
.LBB28_63:                              # %.critedge
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	28696(%r14), %rax
	movl	20(%rsp), %ecx          # 4-byte Reload
	imulq	$608, %rcx, %rcx        # imm = 0x260
	leaq	248(%rax,%rcx), %rdi
.Lcfi193:
	.cfi_escape 0x2e, 0x00
	callq	Event_Set
	testb	%bl, %bl
	movl	$0, %ebp
	leaq	14(%rsp), %rbx
	jne	.LBB28_67
	jmp	.LBB28_5
.LBB28_33:                              #   in Loop: Header=BB28_2 Depth=1
	movq	(%r12), %rdi
	movl	$1024, %eax             # imm = 0x400
	addq	%rax, %rdi
	leaq	24(%r14), %rbp
	cmpb	$0, 15(%rsp)
	movl	24(%rsp), %edx
	je	.LBB28_35
# BB#34:                                #   in Loop: Header=BB28_2 Depth=1
.Ltmp173:
.Lcfi194:
	.cfi_escape 0x2e, 0x00
	movq	48(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%rbp, %rcx
	xorl	%ebp, %ebp
	callq	_ZN9NCompress6NBZip2L16DecodeBlock2RandEPKjjjR10COutBuffer
	movl	%eax, %ebx
.Ltmp174:
.LBB28_49:                              #   in Loop: Header=BB28_2 Depth=1
	movl	$1, %eax
	cmpl	60(%rsp), %ebx
	jne	.LBB28_65
# BB#50:                                #   in Loop: Header=BB28_2 Depth=1
	movq	8(%r12), %r14
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, 80(%rsp)
	cmpq	$0, 28688(%r14)
	je	.LBB28_51
# BB#52:                                #   in Loop: Header=BB28_2 Depth=1
	subq	28672(%r14), %rax
	movq	%rax, 80(%rsp)
	leaq	24(%r14), %rdi
	xorl	%ebp, %ebp
.Ltmp181:
.Lcfi195:
	.cfi_escape 0x2e, 0x00
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
.Ltmp182:
# BB#53:                                # %.noexc64
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	%rax, 96(%rsp)
	movq	28688(%r14), %rdi
	movq	(%rdi), %rax
	xorl	%ebp, %ebp
.Ltmp183:
.Lcfi196:
	.cfi_escape 0x2e, 0x00
	leaq	80(%rsp), %rsi
	leaq	96(%rsp), %rdx
	callq	*40(%rax)
.Ltmp184:
# BB#54:                                #   in Loop: Header=BB28_2 Depth=1
	movq	8(%r12), %r14
	testl	%eax, %eax
	jne	.LBB28_66
# BB#55:                                #   in Loop: Header=BB28_2 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB28_63
.LBB28_35:                              #   in Loop: Header=BB28_2 Depth=1
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movl	(%rdi,%rdx,4), %eax
	shrl	$8, %eax
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	movl	(%rdi,%rax,4), %ecx
	movzbl	%cl, %r13d
	xorl	%edx, %edx
	movl	$-1, %ebx
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB28_36:                              #   Parent Loop BB28_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB28_40 Depth 3
	movl	%ecx, %eax
	movzbl	%al, %r15d
	shrl	$8, %ecx
	movl	%edx, %esi
	movq	88(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx,%rcx,4), %ecx
	movl	%esi, %edx
	cmpl	$4, %edx
	jne	.LBB28_44
# BB#37:                                # %.preheader.i
                                        #   in Loop: Header=BB28_36 Depth=2
	xorl	%edx, %edx
	testl	%r15d, %r15d
	je	.LBB28_38
# BB#39:                                # %.lr.ph.i
                                        #   in Loop: Header=BB28_36 Depth=2
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	negl	%r15d
	.p2align	4, 0x90
.LBB28_40:                              #   Parent Loop BB28_2 Depth=1
                                        #     Parent Loop BB28_36 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebx, %eax
	shrl	$24, %eax
	xorl	%r13d, %eax
	shll	$8, %ebx
	xorl	_ZN9CBZip2Crc5TableE(,%rax,4), %ebx
	movq	24(%r14), %rax
	movl	32(%r14), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 32(%r14)
	movb	%r13b, (%rax,%rcx)
	movl	32(%r14), %eax
	cmpl	36(%r14), %eax
	jne	.LBB28_42
# BB#41:                                #   in Loop: Header=BB28_40 Depth=3
.Ltmp178:
.Lcfi197:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movq	72(%rsp), %rbp          # 8-byte Reload
.Ltmp179:
.LBB28_42:                              # %.noexc61
                                        #   in Loop: Header=BB28_40 Depth=3
	incl	%r15d
	jne	.LBB28_40
# BB#43:                                #   in Loop: Header=BB28_36 Depth=2
	movl	%r13d, %r15d
	xorl	%edx, %edx
	movl	16(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB28_47
.LBB28_44:                              #   in Loop: Header=BB28_36 Depth=2
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	incl	%edx
	cmpl	%r13d, %r15d
	movl	$1, %ecx
	cmovnel	%ecx, %edx
	movl	%edx, %r13d
	movl	%ebx, %ecx
	shrl	$24, %ecx
	xorl	%r15d, %ecx
	shll	$8, %ebx
	xorl	_ZN9CBZip2Crc5TableE(,%rcx,4), %ebx
	movq	24(%r14), %rcx
	movl	32(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 32(%r14)
	movb	%al, (%rcx,%rdx)
	movl	32(%r14), %eax
	cmpl	36(%r14), %eax
	jne	.LBB28_45
# BB#46:                                #   in Loop: Header=BB28_36 Depth=2
.Ltmp175:
.Lcfi198:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movq	72(%rsp), %rbp          # 8-byte Reload
.Ltmp176:
	movl	%r13d, %edx
	movl	16(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB28_47
.LBB28_45:                              #   in Loop: Header=BB28_36 Depth=2
	movl	%r13d, %edx
	movl	16(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB28_47
.LBB28_38:                              #   in Loop: Header=BB28_36 Depth=2
	movl	%r13d, %r15d
.LBB28_47:                              # %.loopexit.i
                                        #   in Loop: Header=BB28_36 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	decl	%eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%r15d, %r13d
	jne	.LBB28_36
# BB#48:                                # %_ZN9NCompress6NBZip2L12DecodeBlock2EPKjjjR10COutBuffer.exit
                                        #   in Loop: Header=BB28_2 Depth=1
	notl	%ebx
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	jmp	.LBB28_49
.LBB28_51:                              # %.thread87
                                        #   in Loop: Header=BB28_2 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB28_63
.LBB28_59:                              # %.loopexit.split-lp.loopexit.split-lp
                                        #   in Loop: Header=BB28_2 Depth=1
.Ltmp185:
	movl	%ebp, %ebx
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movq	%rdx, %rbp
	jmp	.LBB28_60
.LBB28_12:                              #   in Loop: Header=BB28_2 Depth=1
.Ltmp170:
	movq	%rdx, %rbp
.Lcfi199:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	cmpl	$2, %ebp
	jne	.LBB28_24
# BB#13:                                #   in Loop: Header=BB28_2 Depth=1
	movl	(%rax), %ebp
.Lcfi200:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
	cmpl	$0, %ebp
	jne	.LBB28_25
# BB#14:                                # %._crit_edge83
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	8(%r12), %rdi
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	jmp	.LBB28_27
.LBB28_24:                              #   in Loop: Header=BB28_2 Depth=1
.Lcfi201:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
.LBB28_25:                              # %select.unfold
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	8(%r12), %rax
	movl	$-2147467259, 28976(%rax) # imm = 0x80004005
	movb	$1, 28869(%rax)
.Lcfi202:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	Event_Set
	movq	8(%r12), %rdi
	addq	$28816, %rdi            # imm = 0x7090
.Lcfi203:
	.cfi_escape 0x2e, 0x00
	callq	pthread_mutex_unlock
	movq	8(%r12), %rdi
	addq	$28872, %rdi            # imm = 0x70C8
.Lcfi204:
	.cfi_escape 0x2e, 0x00
	callq	Event_Wait
.Lcfi205:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	Event_Set
	xorl	%ebp, %ebp
	jmp	.LBB28_5
.LBB28_58:                              # %.loopexit.split-lp.loopexit
                                        #   in Loop: Header=BB28_2 Depth=1
.Ltmp177:
	jmp	.LBB28_57
.LBB28_56:                              # %.loopexit
                                        #   in Loop: Header=BB28_2 Depth=1
.Ltmp180:
.LBB28_57:                              # %.loopexit.split-lp
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	%rdx, %rbp
	xorl	%ebx, %ebx
.LBB28_60:                              # %.loopexit.split-lp
                                        #   in Loop: Header=BB28_2 Depth=1
.Lcfi206:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	cmpl	$3, %ebp
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	jne	.LBB28_64
# BB#61:                                #   in Loop: Header=BB28_2 Depth=1
	movl	(%rax), %ebp
.Lcfi207:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
	cmpl	$0, %ebp
	movl	$-2147467259, %eax      # imm = 0x80004005
	jne	.LBB28_65
# BB#62:                                # %.thread73
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	8(%r12), %r14
	jmp	.LBB28_63
.LBB28_64:                              #   in Loop: Header=BB28_2 Depth=1
.Lcfi208:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
	movl	$-2147467259, %eax      # imm = 0x80004005
.LBB28_65:                              # %.thread
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	8(%r12), %r14
.LBB28_66:                              #   in Loop: Header=BB28_2 Depth=1
	movl	%eax, 28980(%r14)
	movb	$1, 28870(%r14)
	movq	28696(%r14), %rax
	movl	20(%rsp), %ecx          # 4-byte Reload
	imulq	$608, %rcx, %rcx        # imm = 0x260
	leaq	248(%rax,%rcx), %rdi
.Lcfi209:
	.cfi_escape 0x2e, 0x00
	callq	Event_Set
	xorl	%ebp, %ebp
	leaq	14(%rsp), %rbx
.LBB28_67:                              #   in Loop: Header=BB28_2 Depth=1
.Lcfi210:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	Event_Set
	movq	8(%r12), %rdi
	addq	$28872, %rdi            # imm = 0x70C8
.Lcfi211:
	.cfi_escape 0x2e, 0x00
	callq	Event_Wait
.Lcfi212:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB28_4
	.p2align	4, 0x90
.LBB28_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_28 Depth 2
                                        #     Child Loop BB28_30 Depth 2
                                        #     Child Loop BB28_36 Depth 2
                                        #       Child Loop BB28_40 Depth 3
	cmpb	$0, 28869(%rdi)
	je	.LBB28_7
# BB#3:                                 #   in Loop: Header=BB28_2 Depth=1
	movb	$1, 28869(%rdi)
.Lcfi213:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	Event_Set
	movq	8(%r12), %rdi
	addq	$28816, %rdi            # imm = 0x7090
.Lcfi214:
	.cfi_escape 0x2e, 0x00
	callq	pthread_mutex_unlock
	movq	8(%r12), %rdi
	addq	$28872, %rdi            # imm = 0x70C8
.Lcfi215:
	.cfi_escape 0x2e, 0x00
	callq	Event_Wait
.Lcfi216:
	.cfi_escape 0x2e, 0x00
.LBB28_4:                               # %.backedge
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	%r13, %rdi
	callq	Event_Set
	jmp	.LBB28_5
	.p2align	4, 0x90
.LBB28_7:                               #   in Loop: Header=BB28_2 Depth=1
	movl	28864(%rdi), %r14d
	leal	1(%r14), %eax
	cmpl	28856(%rdi), %eax
	cmovel	%ebp, %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	%eax, 28864(%rdi)
	movl	$0, 28(%rsp)
	movl	$0, 24(%rsp)
	movb	$0, 15(%rsp)
.Ltmp148:
.Lcfi217:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rsi
	leaq	60(%rsp), %rdx
	callq	_ZN9NCompress6NBZip28CDecoder14ReadSignaturesERbRj
.Ltmp149:
# BB#8:                                 #   in Loop: Header=BB28_2 Depth=1
	testl	%eax, %eax
	je	.LBB28_15
# BB#9:                                 #   in Loop: Header=BB28_2 Depth=1
	movq	8(%r12), %rcx
	movl	%eax, 28976(%rcx)
	movb	$1, 28869(%rcx)
.Ltmp150:
.Lcfi218:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	Event_Set
.Ltmp151:
# BB#10:                                # %.noexc
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	8(%r12), %rdi
	addq	$28816, %rdi            # imm = 0x7090
.Lcfi219:
	.cfi_escape 0x2e, 0x00
	callq	pthread_mutex_unlock
	movq	8(%r12), %rdi
	addq	$28872, %rdi            # imm = 0x70C8
.Ltmp152:
.Lcfi220:
	.cfi_escape 0x2e, 0x00
	callq	Event_Wait
.Ltmp153:
# BB#11:                                # %.noexc50
                                        #   in Loop: Header=BB28_2 Depth=1
.Ltmp154:
.Lcfi221:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	Event_Set
.Ltmp155:
	jmp	.LBB28_5
.LBB28_15:                              #   in Loop: Header=BB28_2 Depth=1
	cmpb	$0, 14(%rsp)
	movq	8(%r12), %r8
	je	.LBB28_19
# BB#16:                                #   in Loop: Header=BB28_2 Depth=1
	movl	$0, 28976(%r8)
	movb	$1, 28869(%r8)
.Ltmp156:
.Lcfi222:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	Event_Set
.Ltmp157:
# BB#17:                                # %.noexc52
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	8(%r12), %rdi
	addq	$28816, %rdi            # imm = 0x7090
.Lcfi223:
	.cfi_escape 0x2e, 0x00
	callq	pthread_mutex_unlock
	movq	8(%r12), %rdi
	addq	$28872, %rdi            # imm = 0x70C8
.Ltmp158:
.Lcfi224:
	.cfi_escape 0x2e, 0x00
	callq	Event_Wait
.Ltmp159:
# BB#18:                                # %.noexc53
                                        #   in Loop: Header=BB28_2 Depth=1
.Ltmp160:
.Lcfi225:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	Event_Set
.Ltmp161:
	jmp	.LBB28_5
.LBB28_19:                              #   in Loop: Header=BB28_2 Depth=1
	leaq	336(%r8), %rdi
	movq	(%r12), %rsi
	movl	28984(%r8), %edx
	leaq	392(%r8), %rcx
	addq	$18396, %r8             # imm = 0x47DC
.Ltmp162:
.Lcfi226:
	.cfi_escape 0x2e, 0x10
	leaq	28(%rsp), %r9
	leaq	15(%rsp), %rax
	pushq	%rax
.Lcfi227:
	.cfi_adjust_cfa_offset 8
	leaq	32(%rsp), %rax
	pushq	%rax
.Lcfi228:
	.cfi_adjust_cfa_offset 8
	callq	_ZN9NCompress6NBZip2L9ReadBlockEPN5NBitm8CDecoderI9CInBufferEEPjjPhPNS_8NHuffman8CDecoderILi20ELj258EEES6_S6_Pb
	addq	$16, %rsp
.Lcfi229:
	.cfi_adjust_cfa_offset -16
.Ltmp163:
# BB#20:                                #   in Loop: Header=BB28_2 Depth=1
	testl	%eax, %eax
	movq	8(%r12), %rdi
	je	.LBB28_26
# BB#21:                                #   in Loop: Header=BB28_2 Depth=1
	movl	%eax, 28976(%rdi)
	movb	$1, 28869(%rdi)
.Ltmp164:
.Lcfi230:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	Event_Set
.Ltmp165:
# BB#22:                                # %.noexc56
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	8(%r12), %rdi
	addq	$28816, %rdi            # imm = 0x7090
.Lcfi231:
	.cfi_escape 0x2e, 0x00
	callq	pthread_mutex_unlock
	movq	8(%r12), %rdi
	addq	$28872, %rdi            # imm = 0x70C8
.Ltmp166:
.Lcfi232:
	.cfi_escape 0x2e, 0x00
	callq	Event_Wait
.Ltmp167:
# BB#23:                                # %.noexc57
                                        #   in Loop: Header=BB28_2 Depth=1
.Ltmp168:
.Lcfi233:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	Event_Set
.Ltmp169:
	.p2align	4, 0x90
.LBB28_5:                               # %.backedge
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	8(%r12), %rdi
	addq	$28712, %rdi            # imm = 0x7028
.Lcfi234:
	.cfi_escape 0x2e, 0x00
	callq	Event_Wait
	movq	8(%r12), %rdi
	addq	$28816, %rdi            # imm = 0x7090
.Lcfi235:
	.cfi_escape 0x2e, 0x00
	callq	pthread_mutex_lock
	movq	8(%r12), %rdi
	cmpb	$0, 28868(%rdi)
	je	.LBB28_2
.LBB28_6:                               # %._crit_edge
	addq	$28816, %rdi            # imm = 0x7090
.Lcfi236:
	.cfi_escape 0x2e, 0x00
	callq	pthread_mutex_unlock
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	_ZN9NCompress6NBZip26CState10ThreadFuncEv, .Lfunc_end28-_ZN9NCompress6NBZip26CState10ThreadFuncEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\211\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin8-.Lfunc_begin8 # >> Call Site 1 <<
	.long	.Ltmp171-.Lfunc_begin8  #   Call between .Lfunc_begin8 and .Ltmp171
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp171-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp172-.Ltmp171       #   Call between .Ltmp171 and .Ltmp172
	.long	.Ltmp185-.Lfunc_begin8  #     jumps to .Ltmp185
	.byte	5                       #   On action: 3
	.long	.Ltmp172-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp173-.Ltmp172       #   Call between .Ltmp172 and .Ltmp173
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp173-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp184-.Ltmp173       #   Call between .Ltmp173 and .Ltmp184
	.long	.Ltmp185-.Lfunc_begin8  #     jumps to .Ltmp185
	.byte	5                       #   On action: 3
	.long	.Ltmp178-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp179-.Ltmp178       #   Call between .Ltmp178 and .Ltmp179
	.long	.Ltmp180-.Lfunc_begin8  #     jumps to .Ltmp180
	.byte	5                       #   On action: 3
	.long	.Ltmp175-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp176-.Ltmp175       #   Call between .Ltmp175 and .Ltmp176
	.long	.Ltmp177-.Lfunc_begin8  #     jumps to .Ltmp177
	.byte	5                       #   On action: 3
	.long	.Ltmp176-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Ltmp148-.Ltmp176       #   Call between .Ltmp176 and .Ltmp148
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin8  # >> Call Site 8 <<
	.long	.Ltmp169-.Ltmp148       #   Call between .Ltmp148 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin8  #     jumps to .Ltmp170
	.byte	3                       #   On action: 2
	.long	.Ltmp169-.Lfunc_begin8  # >> Call Site 9 <<
	.long	.Lfunc_end28-.Ltmp169   #   Call between .Ltmp169 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
	.byte	3                       # >> Action Record 3 <<
                                        #   Catch TypeInfo 3
	.byte	123                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTI19COutBufferException # TypeInfo 3
	.long	_ZTI18CInBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress6NBZip28CDecoder18SetNumberOfThreadsEj
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder18SetNumberOfThreadsEj,@function
_ZN9NCompress6NBZip28CDecoder18SetNumberOfThreadsEj: # @_ZN9NCompress6NBZip28CDecoder18SetNumberOfThreadsEj
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	movl	$1, %eax
	cmovnel	%esi, %eax
	cmpl	$4, %eax
	movl	$4, %ecx
	cmovbl	%eax, %ecx
	movl	%ecx, 28856(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end29:
	.size	_ZN9NCompress6NBZip28CDecoder18SetNumberOfThreadsEj, .Lfunc_end29-_ZN9NCompress6NBZip28CDecoder18SetNumberOfThreadsEj
	.cfi_endproc

	.globl	_ZThn8_N9NCompress6NBZip28CDecoder18SetNumberOfThreadsEj
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip28CDecoder18SetNumberOfThreadsEj,@function
_ZThn8_N9NCompress6NBZip28CDecoder18SetNumberOfThreadsEj: # @_ZThn8_N9NCompress6NBZip28CDecoder18SetNumberOfThreadsEj
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	movl	$1, %eax
	cmovnel	%esi, %eax
	cmpl	$4, %eax
	movl	$4, %ecx
	cmovbl	%eax, %ecx
	movl	%ecx, 28848(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end30:
	.size	_ZThn8_N9NCompress6NBZip28CDecoder18SetNumberOfThreadsEj, .Lfunc_end30-_ZThn8_N9NCompress6NBZip28CDecoder18SetNumberOfThreadsEj
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip212CNsisDecoder11SetInStreamEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip212CNsisDecoder11SetInStreamEP19ISequentialInStream,@function
_ZN9NCompress6NBZip212CNsisDecoder11SetInStreamEP19ISequentialInStream: # @_ZN9NCompress6NBZip212CNsisDecoder11SetInStreamEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi237:
	.cfi_def_cfa_offset 16
	addq	$40, %rdi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end31:
	.size	_ZN9NCompress6NBZip212CNsisDecoder11SetInStreamEP19ISequentialInStream, .Lfunc_end31-_ZN9NCompress6NBZip212CNsisDecoder11SetInStreamEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZThn8_N9NCompress6NBZip212CNsisDecoder11SetInStreamEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip212CNsisDecoder11SetInStreamEP19ISequentialInStream,@function
_ZThn8_N9NCompress6NBZip212CNsisDecoder11SetInStreamEP19ISequentialInStream: # @_ZThn8_N9NCompress6NBZip212CNsisDecoder11SetInStreamEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi238:
	.cfi_def_cfa_offset 16
	addq	$32, %rdi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end32:
	.size	_ZThn8_N9NCompress6NBZip212CNsisDecoder11SetInStreamEP19ISequentialInStream, .Lfunc_end32-_ZThn8_N9NCompress6NBZip212CNsisDecoder11SetInStreamEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip212CNsisDecoder15ReleaseInStreamEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip212CNsisDecoder15ReleaseInStreamEv,@function
_ZN9NCompress6NBZip212CNsisDecoder15ReleaseInStreamEv: # @_ZN9NCompress6NBZip212CNsisDecoder15ReleaseInStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi239:
	.cfi_def_cfa_offset 16
.Lcfi240:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB33_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 64(%rbx)
.LBB33_2:                               # %_ZN5NBitm8CDecoderI9CInBufferE13ReleaseStreamEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end33:
	.size	_ZN9NCompress6NBZip212CNsisDecoder15ReleaseInStreamEv, .Lfunc_end33-_ZN9NCompress6NBZip212CNsisDecoder15ReleaseInStreamEv
	.cfi_endproc

	.globl	_ZThn8_N9NCompress6NBZip212CNsisDecoder15ReleaseInStreamEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip212CNsisDecoder15ReleaseInStreamEv,@function
_ZThn8_N9NCompress6NBZip212CNsisDecoder15ReleaseInStreamEv: # @_ZThn8_N9NCompress6NBZip212CNsisDecoder15ReleaseInStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi241:
	.cfi_def_cfa_offset 16
.Lcfi242:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB34_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 56(%rbx)
.LBB34_2:                               # %_ZN9NCompress6NBZip212CNsisDecoder15ReleaseInStreamEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end34:
	.size	_ZThn8_N9NCompress6NBZip212CNsisDecoder15ReleaseInStreamEv, .Lfunc_end34-_ZThn8_N9NCompress6NBZip212CNsisDecoder15ReleaseInStreamEv
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip212CNsisDecoder16SetOutStreamSizeEPKy
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip212CNsisDecoder16SetOutStreamSizeEPKy,@function
_ZN9NCompress6NBZip212CNsisDecoder16SetOutStreamSizeEPKy: # @_ZN9NCompress6NBZip212CNsisDecoder16SetOutStreamSizeEPKy
	.cfi_startproc
# BB#0:
	movl	$0, 28976(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end35:
	.size	_ZN9NCompress6NBZip212CNsisDecoder16SetOutStreamSizeEPKy, .Lfunc_end35-_ZN9NCompress6NBZip212CNsisDecoder16SetOutStreamSizeEPKy
	.cfi_endproc

	.globl	_ZThn16_N9NCompress6NBZip212CNsisDecoder16SetOutStreamSizeEPKy
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NBZip212CNsisDecoder16SetOutStreamSizeEPKy,@function
_ZThn16_N9NCompress6NBZip212CNsisDecoder16SetOutStreamSizeEPKy: # @_ZThn16_N9NCompress6NBZip212CNsisDecoder16SetOutStreamSizeEPKy
	.cfi_startproc
# BB#0:
	movl	$0, 28960(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end36:
	.size	_ZThn16_N9NCompress6NBZip212CNsisDecoder16SetOutStreamSizeEPKy, .Lfunc_end36-_ZThn16_N9NCompress6NBZip212CNsisDecoder16SetOutStreamSizeEPKy
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip212CNsisDecoder4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip212CNsisDecoder4ReadEPvjPj,@function
_ZN9NCompress6NBZip212CNsisDecoder4ReadEPvjPj: # @_ZN9NCompress6NBZip212CNsisDecoder4ReadEPvjPj
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%rbp
.Lcfi243:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi244:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi245:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi246:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi247:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi248:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi249:
	.cfi_def_cfa_offset 112
.Lcfi250:
	.cfi_offset %rbx, -56
.Lcfi251:
	.cfi_offset %r12, -48
.Lcfi252:
	.cfi_offset %r13, -40
.Lcfi253:
	.cfi_offset %r14, -32
.Lcfi254:
	.cfi_offset %r15, -24
.Lcfi255:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %r15
	movl	$0, (%r12)
	movl	28976(%r15), %eax
	cmpl	$3, %eax
	je	.LBB37_3
# BB#1:
	cmpl	$4, %eax
	jne	.LBB37_4
.LBB37_2:                               # %.thread107
	movl	$1, %ebx
	jmp	.LBB37_64
.LBB37_3:
	xorl	%ebx, %ebx
	jmp	.LBB37_64
.LBB37_4:
	testl	%r14d, %r14d
	je	.LBB37_20
# BB#5:
	cmpl	$1, %eax
	je	.LBB37_21
# BB#6:
	testl	%eax, %eax
	jne	.LBB37_32
# BB#7:
	leaq	40(%r15), %rdi
.Ltmp186:
.Lcfi256:
	.cfi_escape 0x2e, 0x00
	movl	$131072, %esi           # imm = 0x20000
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	callq	_ZN9CInBuffer6CreateEj
.Ltmp187:
# BB#8:                                 # %_ZN5NBitm8CDecoderI9CInBufferE6CreateEj.exit
	movl	$-2147024882, %ebx      # imm = 0x8007000E
	testb	%al, %al
	je	.LBB37_64
# BB#9:
	cmpq	$0, 28368(%r15)
	jne	.LBB37_12
# BB#10:
.Ltmp188:
.Lcfi257:
	.cfi_escape 0x2e, 0x00
	movl	$3601024, %edi          # imm = 0x36F280
	callq	BigAlloc
.Ltmp189:
# BB#11:
	movq	%rax, 28368(%r15)
	testq	%rax, %rax
	je	.LBB37_64
.LBB37_12:                              # %.thread
.Ltmp190:
.Lcfi258:
	.cfi_escape 0x2e, 0x00
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN9CInBuffer4InitEv
.Ltmp191:
# BB#13:                                # %.noexc103
	movl	$32, 32(%r15)
	leaq	36(%r15), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	36(%r15), %r13d
	movl	$32, %ecx
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB37_14:                              # =>This Inner Loop Header: Depth=1
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jae	.LBB37_16
# BB#15:                                #   in Loop: Header=BB37_14 Depth=1
	leaq	1(%rax), %rdx
	movq	%rdx, (%rbx)
	movzbl	(%rax), %eax
	movl	%ecx, %edx
	jmp	.LBB37_18
	.p2align	4, 0x90
.LBB37_16:                              #   in Loop: Header=BB37_14 Depth=1
.Ltmp193:
.Lcfi259:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.Ltmp194:
# BB#17:                                # %.noexc104
                                        #   in Loop: Header=BB37_14 Depth=1
	movl	32(%r15), %edx
.LBB37_18:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i
                                        #   in Loop: Header=BB37_14 Depth=1
	movl	%r13d, %ecx
	shll	$8, %ecx
	movzbl	%al, %r13d
	orl	%ecx, %r13d
	movl	%r13d, 36(%r15)
	leal	-8(%rdx), %ecx
	movl	%ecx, 32(%r15)
	cmpl	$7, %ecx
	ja	.LBB37_14
# BB#19:                                # %.thread155
	movl	$1, 28976(%r15)
	leaq	32(%r15), %rbx
	movl	$16, %ecx
	subl	%edx, %ecx
	movl	%r13d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	shrl	$16, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%edx, 32(%r15)
	jmp	.LBB37_23
.LBB37_20:
	xorl	%ebx, %ebx
	jmp	.LBB37_64
.LBB37_21:
	leaq	32(%r15), %rbx
	movl	32(%r15), %eax
	movl	36(%r15), %r13d
	movl	$8, %ecx
	subl	%eax, %ecx
	movl	%r13d, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	shrl	$16, %edx
	movl	%edx, 8(%rsp)           # 4-byte Spill
	leal	8(%rax), %edx
	movl	%edx, 32(%r15)
	cmpl	$-9, %eax
	ja	.LBB37_29
# BB#22:
	leaq	36(%r15), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB37_23:                              # %.lr.ph.i.i.i
	leaq	40(%r15), %rsi
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB37_24:                              # =>This Inner Loop Header: Depth=1
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jae	.LBB37_26
# BB#25:                                #   in Loop: Header=BB37_24 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsi)
	movzbl	(%rax), %eax
	jmp	.LBB37_28
	.p2align	4, 0x90
.LBB37_26:                              #   in Loop: Header=BB37_24 Depth=1
.Ltmp196:
	movq	%r12, %rbp
	movq	%r15, %r12
	movl	%r14d, %r15d
.Lcfi260:
	.cfi_escape 0x2e, 0x00
	movq	%rsi, %r14
	movq	%rsi, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.Ltmp197:
# BB#27:                                # %.noexc105
                                        #   in Loop: Header=BB37_24 Depth=1
	movl	(%rbx), %edx
	movq	%r14, %rsi
	movl	%r15d, %r14d
	movq	%r12, %r15
	movq	%rbp, %r12
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB37_28:                              # %_ZN9CInBuffer8ReadByteEv.exit.i.i.i
                                        #   in Loop: Header=BB37_24 Depth=1
	shll	$8, %r13d
	movzbl	%al, %eax
	orl	%eax, %r13d
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r13d, (%rax)
	addl	$-8, %edx
	movl	%edx, (%rbx)
	cmpl	$7, %edx
	ja	.LBB37_24
.LBB37_29:                              # %_ZN5NBitm8CDecoderI9CInBufferE8ReadBitsEj.exit
	movl	8(%rsp), %eax           # 4-byte Reload
	cmpb	$49, %al
	je	.LBB37_49
# BB#30:                                # %_ZN5NBitm8CDecoderI9CInBufferE8ReadBitsEj.exit
	cmpb	$23, %al
	jne	.LBB37_56
# BB#31:
	movl	$3, 28976(%r15)
	xorl	%ebx, %ebx
	jmp	.LBB37_64
.LBB37_32:                              # %._crit_edge134
	leaq	28980(%r15), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	28980(%r15), %edx
	leaq	28984(%r15), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	28984(%r15), %ebx
	leaq	28992(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	28992(%r15), %r13d
	movq	28368(%r15), %rax
	leaq	28996(%r15), %rsi
	leaq	28988(%r15), %rcx
.LBB37_33:
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	(%rsi), %r11d
	.p2align	4, 0x90
.LBB37_34:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edi
	testl	%edi, %edi
	je	.LBB37_37
# BB#35:                                #   in Loop: Header=BB37_34 Depth=1
	decl	%edi
	movl	%edi, (%rcx)
	movb	%bl, (%rbp)
	incq	%rbp
	incl	(%r12)
	decl	%r14d
	jne	.LBB37_34
# BB#36:
	xorl	%ebx, %ebx
	jmp	.LBB37_64
.LBB37_37:
	testl	%r11d, %r11d
	je	.LBB37_57
	.p2align	4, 0x90
.LBB37_38:                              # %.preheader111
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_41 Depth 2
	movzbl	%dl, %edi
	movl	%edx, %esi
	shrl	$8, %esi
	movl	1024(%rax,%rsi,4), %r15d
	decl	%r11d
	cmpl	$4, %r13d
	jne	.LBB37_44
# BB#39:                                #   in Loop: Header=BB37_38 Depth=1
	xorl	%r13d, %r13d
	testl	%edi, %edi
	je	.LBB37_45
# BB#40:                                # %.preheader
                                        #   in Loop: Header=BB37_38 Depth=1
	movl	%r14d, %edi
	decq	%rdi
	movzbl	%dl, %r9d
	decq	%r9
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB37_41:                              #   Parent Loop BB37_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	%bl, (%rbp,%r8)
	incl	(%r12)
	movl	%r8d, %r10d
	incq	%r8
	cmpl	%r10d, %edi
	je	.LBB37_43
# BB#42:                                #   in Loop: Header=BB37_41 Depth=2
	cmpl	%r10d, %r9d
	jne	.LBB37_41
.LBB37_43:                              # %.loopexit.loopexit
                                        #   in Loop: Header=BB37_38 Depth=1
	subl	%r8d, %r14d
	addq	%r8, %rbp
	movzbl	%dl, %edx
	subl	%r8d, %edx
	jmp	.LBB37_46
	.p2align	4, 0x90
.LBB37_44:                              #   in Loop: Header=BB37_38 Depth=1
	incl	%r13d
	cmpl	%ebx, %edi
	movl	$1, %esi
	cmovnel	%esi, %r13d
	movb	%dl, (%rbp)
	incq	%rbp
	incl	(%r12)
	decl	%r14d
	movl	%edi, %ebx
	testl	%r11d, %r11d
	jne	.LBB37_47
	jmp	.LBB37_48
.LBB37_45:                              #   in Loop: Header=BB37_38 Depth=1
	xorl	%edx, %edx
.LBB37_46:                              #   in Loop: Header=BB37_38 Depth=1
	movl	%edx, (%rcx)
	testl	%r11d, %r11d
	je	.LBB37_48
.LBB37_47:                              #   in Loop: Header=BB37_38 Depth=1
	testl	%r14d, %r14d
	movl	%r15d, %edx
	jne	.LBB37_38
.LBB37_48:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%r15d, (%rax)
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%ebx, (%rax)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%r13d, (%rax)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r11d, (%rax)
	xorl	%ebx, %ebx
	jmp	.LBB37_64
.LBB37_49:
	movq	28368(%r15), %rsi
	leaq	88(%r15), %rcx
	leaq	18092(%r15), %r8
	leaq	28996(%r15), %r9
.Ltmp199:
.Lcfi261:
	.cfi_escape 0x2e, 0x10
	leaq	28(%rsp), %rax
	movl	$900000, %edx           # imm = 0xDBBA0
	movq	%rbx, %rdi
	movq	%r9, 16(%rsp)           # 8-byte Spill
	pushq	$0
.Lcfi262:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi263:
	.cfi_adjust_cfa_offset 8
	callq	_ZN9NCompress6NBZip2L9ReadBlockEPN5NBitm8CDecoderI9CInBufferEEPjjPhPNS_8NHuffman8CDecoderILi20ELj258EEES6_S6_Pb
	addq	$16, %rsp
.Lcfi264:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebx
.Ltmp200:
# BB#50:
	testl	%ebx, %ebx
	jne	.LBB37_64
# BB#51:
	movq	28368(%r15), %rax
	movl	28996(%r15), %r8d
	xorl	%esi, %esi
	movl	$3, %edx
	.p2align	4, 0x90
.LBB37_52:                              # =>This Inner Loop Header: Depth=1
	movl	-12(%rax,%rdx,4), %edi
	addl	%esi, %edi
	movl	%esi, -12(%rax,%rdx,4)
	movl	-8(%rax,%rdx,4), %esi
	addl	%edi, %esi
	movl	%edi, -8(%rax,%rdx,4)
	movl	-4(%rax,%rdx,4), %edi
	addl	%esi, %edi
	movl	%esi, -4(%rax,%rdx,4)
	movl	(%rax,%rdx,4), %esi
	addl	%edi, %esi
	movl	%edi, (%rax,%rdx,4)
	addq	$4, %rdx
	cmpq	$259, %rdx              # imm = 0x103
	jne	.LBB37_52
# BB#53:
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB37_54:                              # =>This Inner Loop Header: Depth=1
	movzbl	1024(%rax,%rsi,4), %edi
	movl	(%rax,%rdi,4), %ebx
	leal	1(%rbx), %ecx
	movl	%ecx, (%rax,%rdi,4)
	orl	%edx, 1024(%rax,%rbx,4)
	incq	%rsi
	addl	$256, %edx              # imm = 0x100
	cmpq	%r8, %rsi
	jb	.LBB37_54
# BB#55:                                # %.thread109
	movl	28(%rsp), %ecx
	movl	1024(%rax,%rcx,4), %ecx
	shrl	$8, %ecx
	movl	1024(%rax,%rcx,4), %edx
	leaq	28980(%r15), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	%edx, 28980(%r15)
	movzbl	%dl, %ebx
	leaq	28984(%r15), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%ebx, 28984(%r15)
	leaq	28992(%r15), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	$0, 28992(%r15)
	leaq	28988(%r15), %rcx
	movl	$0, 28988(%r15)
	movl	$2, 28976(%r15)
	xorl	%r13d, %r13d
	movq	16(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB37_33
.LBB37_56:
	movl	$4, 28976(%r15)
	jmp	.LBB37_2
.LBB37_57:
	movl	$1, 28976(%r15)
	xorl	%ebx, %ebx
	jmp	.LBB37_64
.LBB37_58:
.Ltmp201:
	jmp	.LBB37_62
.LBB37_59:                              # %.loopexit.split-lp.loopexit.split-lp
.Ltmp192:
	jmp	.LBB37_62
.LBB37_60:                              # %.loopexit.split-lp.loopexit
.Ltmp195:
	jmp	.LBB37_62
.LBB37_61:                              # %.loopexit112
.Ltmp198:
.LBB37_62:
	movq	%rdx, %rbx
	movq	%rax, %rdi
.Lcfi265:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_begin_catch
	cmpl	$2, %ebx
	jne	.LBB37_65
# BB#63:
	movl	(%rax), %ebx
.Lcfi266:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
.LBB37_64:                              # %.thread107
	movl	%ebx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB37_65:
.Lcfi267:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
	jmp	.LBB37_2
.Lfunc_end37:
	.size	_ZN9NCompress6NBZip212CNsisDecoder4ReadEPvjPj, .Lfunc_end37-_ZN9NCompress6NBZip212CNsisDecoder4ReadEPvjPj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table37:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\317\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp186-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp191-.Ltmp186       #   Call between .Ltmp186 and .Ltmp191
	.long	.Ltmp192-.Lfunc_begin9  #     jumps to .Ltmp192
	.byte	3                       #   On action: 2
	.long	.Ltmp193-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp194-.Ltmp193       #   Call between .Ltmp193 and .Ltmp194
	.long	.Ltmp195-.Lfunc_begin9  #     jumps to .Ltmp195
	.byte	3                       #   On action: 2
	.long	.Ltmp196-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp197-.Ltmp196       #   Call between .Ltmp196 and .Ltmp197
	.long	.Ltmp198-.Lfunc_begin9  #     jumps to .Ltmp198
	.byte	3                       #   On action: 2
	.long	.Ltmp199-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Ltmp200-.Ltmp199       #   Call between .Ltmp199 and .Ltmp200
	.long	.Ltmp201-.Lfunc_begin9  #     jumps to .Ltmp201
	.byte	3                       #   On action: 2
	.long	.Ltmp200-.Lfunc_begin9  # >> Call Site 5 <<
	.long	.Lfunc_end37-.Ltmp200   #   Call between .Ltmp200 and .Lfunc_end37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTI18CInBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi268:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB38_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB38_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB38_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB38_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB38_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB38_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB38_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB38_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB38_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB38_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB38_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB38_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB38_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB38_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB38_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB38_16
.LBB38_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ISequentialInStream(%rip), %cl
	jne	.LBB38_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ISequentialInStream+1(%rip), %al
	jne	.LBB38_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ISequentialInStream+2(%rip), %al
	jne	.LBB38_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ISequentialInStream+3(%rip), %al
	jne	.LBB38_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ISequentialInStream+4(%rip), %al
	jne	.LBB38_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ISequentialInStream+5(%rip), %al
	jne	.LBB38_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ISequentialInStream+6(%rip), %al
	jne	.LBB38_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ISequentialInStream+7(%rip), %al
	jne	.LBB38_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ISequentialInStream+8(%rip), %al
	jne	.LBB38_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ISequentialInStream+9(%rip), %al
	jne	.LBB38_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ISequentialInStream+10(%rip), %al
	jne	.LBB38_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ISequentialInStream+11(%rip), %al
	jne	.LBB38_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ISequentialInStream+12(%rip), %al
	jne	.LBB38_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ISequentialInStream+13(%rip), %al
	jne	.LBB38_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ISequentialInStream+14(%rip), %al
	jne	.LBB38_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit10
	movb	15(%rsi), %al
	cmpb	IID_ISequentialInStream+15(%rip), %al
	jne	.LBB38_33
.LBB38_16:
	movq	%rdi, (%rdx)
	jmp	.LBB38_68
.LBB38_33:                              # %_ZeqRK4GUIDS1_.exit10.thread
	cmpb	IID_ICompressSetInStream(%rip), %cl
	jne	.LBB38_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetInStream+1(%rip), %al
	jne	.LBB38_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetInStream+2(%rip), %al
	jne	.LBB38_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetInStream+3(%rip), %al
	jne	.LBB38_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetInStream+4(%rip), %al
	jne	.LBB38_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetInStream+5(%rip), %al
	jne	.LBB38_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetInStream+6(%rip), %al
	jne	.LBB38_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetInStream+7(%rip), %al
	jne	.LBB38_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetInStream+8(%rip), %al
	jne	.LBB38_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetInStream+9(%rip), %al
	jne	.LBB38_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetInStream+10(%rip), %al
	jne	.LBB38_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetInStream+11(%rip), %al
	jne	.LBB38_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetInStream+12(%rip), %al
	jne	.LBB38_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetInStream+13(%rip), %al
	jne	.LBB38_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetInStream+14(%rip), %al
	jne	.LBB38_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetInStream+15(%rip), %al
	jne	.LBB38_50
# BB#49:
	leaq	8(%rdi), %rax
	jmp	.LBB38_67
.LBB38_50:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressSetOutStreamSize(%rip), %cl
	jne	.LBB38_69
# BB#51:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+1(%rip), %cl
	jne	.LBB38_69
# BB#52:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+2(%rip), %cl
	jne	.LBB38_69
# BB#53:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+3(%rip), %cl
	jne	.LBB38_69
# BB#54:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+4(%rip), %cl
	jne	.LBB38_69
# BB#55:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+5(%rip), %cl
	jne	.LBB38_69
# BB#56:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+6(%rip), %cl
	jne	.LBB38_69
# BB#57:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+7(%rip), %cl
	jne	.LBB38_69
# BB#58:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+8(%rip), %cl
	jne	.LBB38_69
# BB#59:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+9(%rip), %cl
	jne	.LBB38_69
# BB#60:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+10(%rip), %cl
	jne	.LBB38_69
# BB#61:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+11(%rip), %cl
	jne	.LBB38_69
# BB#62:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+12(%rip), %cl
	jne	.LBB38_69
# BB#63:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+13(%rip), %cl
	jne	.LBB38_69
# BB#64:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+14(%rip), %cl
	jne	.LBB38_69
# BB#65:                                # %_ZeqRK4GUIDS1_.exit12
	movb	15(%rsi), %cl
	cmpb	IID_ICompressSetOutStreamSize+15(%rip), %cl
	jne	.LBB38_69
# BB#66:
	leaq	16(%rdi), %rax
.LBB38_67:                              # %_ZeqRK4GUIDS1_.exit12.thread
	movq	%rax, (%rdx)
.LBB38_68:                              # %_ZeqRK4GUIDS1_.exit12.thread
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB38_69:                              # %_ZeqRK4GUIDS1_.exit12.thread
	popq	%rcx
	retq
.Lfunc_end38:
	.size	_ZN9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end38-_ZN9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress6NBZip212CNsisDecoder6AddRefEv,"axG",@progbits,_ZN9NCompress6NBZip212CNsisDecoder6AddRefEv,comdat
	.weak	_ZN9NCompress6NBZip212CNsisDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip212CNsisDecoder6AddRefEv,@function
_ZN9NCompress6NBZip212CNsisDecoder6AddRefEv: # @_ZN9NCompress6NBZip212CNsisDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end39:
	.size	_ZN9NCompress6NBZip212CNsisDecoder6AddRefEv, .Lfunc_end39-_ZN9NCompress6NBZip212CNsisDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress6NBZip212CNsisDecoder7ReleaseEv,"axG",@progbits,_ZN9NCompress6NBZip212CNsisDecoder7ReleaseEv,comdat
	.weak	_ZN9NCompress6NBZip212CNsisDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip212CNsisDecoder7ReleaseEv,@function
_ZN9NCompress6NBZip212CNsisDecoder7ReleaseEv: # @_ZN9NCompress6NBZip212CNsisDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi269:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB40_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB40_2:
	popq	%rcx
	retq
.Lfunc_end40:
	.size	_ZN9NCompress6NBZip212CNsisDecoder7ReleaseEv, .Lfunc_end40-_ZN9NCompress6NBZip212CNsisDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN9NCompress6NBZip212CNsisDecoderD2Ev,"axG",@progbits,_ZN9NCompress6NBZip212CNsisDecoderD2Ev,comdat
	.weak	_ZN9NCompress6NBZip212CNsisDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip212CNsisDecoderD2Ev,@function
_ZN9NCompress6NBZip212CNsisDecoderD2Ev: # @_ZN9NCompress6NBZip212CNsisDecoderD2Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi270:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi271:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi272:
	.cfi_def_cfa_offset 32
.Lcfi273:
	.cfi_offset %rbx, -24
.Lcfi274:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress6NBZip212CNsisDecoderE+104, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NBZip212CNsisDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN9NCompress6NBZip212CNsisDecoderE+176, 16(%rbx)
	leaq	28368(%rbx), %rdi
.Ltmp202:
	callq	_ZN9NCompress6NBZip26CStateD2Ev
.Ltmp203:
# BB#1:
	leaq	40(%rbx), %rdi
.Ltmp214:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp215:
# BB#2:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB41_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp220:
	callq	*16(%rax)
.Ltmp221:
.LBB41_4:                               # %_ZN5NBitm8CDecoderI9CInBufferED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB41_14:
.Ltmp222:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB41_5:
.Ltmp216:
	movq	%rax, %r14
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB41_15
# BB#6:
	movq	(%rdi), %rax
.Ltmp217:
	callq	*16(%rax)
.Ltmp218:
	jmp	.LBB41_15
.LBB41_7:
.Ltmp219:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB41_8:
.Ltmp204:
	movq	%rax, %r14
	leaq	40(%rbx), %rdi
.Ltmp205:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp206:
# BB#9:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB41_15
# BB#10:
	movq	(%rdi), %rax
.Ltmp211:
	callq	*16(%rax)
.Ltmp212:
.LBB41_15:                              # %_ZN5NBitm8CDecoderI9CInBufferED2Ev.exit9
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB41_16:
.Ltmp213:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB41_11:
.Ltmp207:
	movq	%rax, %r14
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB41_17
# BB#12:
	movq	(%rdi), %rax
.Ltmp208:
	callq	*16(%rax)
.Ltmp209:
.LBB41_17:                              # %.body7
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB41_13:
.Ltmp210:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end41:
	.size	_ZN9NCompress6NBZip212CNsisDecoderD2Ev, .Lfunc_end41-_ZN9NCompress6NBZip212CNsisDecoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table41:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Ltmp202-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp203-.Ltmp202       #   Call between .Ltmp202 and .Ltmp203
	.long	.Ltmp204-.Lfunc_begin10 #     jumps to .Ltmp204
	.byte	0                       #   On action: cleanup
	.long	.Ltmp214-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp215-.Ltmp214       #   Call between .Ltmp214 and .Ltmp215
	.long	.Ltmp216-.Lfunc_begin10 #     jumps to .Ltmp216
	.byte	0                       #   On action: cleanup
	.long	.Ltmp220-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp221-.Ltmp220       #   Call between .Ltmp220 and .Ltmp221
	.long	.Ltmp222-.Lfunc_begin10 #     jumps to .Ltmp222
	.byte	0                       #   On action: cleanup
	.long	.Ltmp221-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Ltmp217-.Ltmp221       #   Call between .Ltmp221 and .Ltmp217
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp217-.Lfunc_begin10 # >> Call Site 5 <<
	.long	.Ltmp218-.Ltmp217       #   Call between .Ltmp217 and .Ltmp218
	.long	.Ltmp219-.Lfunc_begin10 #     jumps to .Ltmp219
	.byte	1                       #   On action: 1
	.long	.Ltmp205-.Lfunc_begin10 # >> Call Site 6 <<
	.long	.Ltmp206-.Ltmp205       #   Call between .Ltmp205 and .Ltmp206
	.long	.Ltmp207-.Lfunc_begin10 #     jumps to .Ltmp207
	.byte	1                       #   On action: 1
	.long	.Ltmp211-.Lfunc_begin10 # >> Call Site 7 <<
	.long	.Ltmp212-.Ltmp211       #   Call between .Ltmp211 and .Ltmp212
	.long	.Ltmp213-.Lfunc_begin10 #     jumps to .Ltmp213
	.byte	1                       #   On action: 1
	.long	.Ltmp212-.Lfunc_begin10 # >> Call Site 8 <<
	.long	.Ltmp208-.Ltmp212       #   Call between .Ltmp212 and .Ltmp208
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp208-.Lfunc_begin10 # >> Call Site 9 <<
	.long	.Ltmp209-.Ltmp208       #   Call between .Ltmp208 and .Ltmp209
	.long	.Ltmp210-.Lfunc_begin10 #     jumps to .Ltmp210
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress6NBZip212CNsisDecoderD0Ev,"axG",@progbits,_ZN9NCompress6NBZip212CNsisDecoderD0Ev,comdat
	.weak	_ZN9NCompress6NBZip212CNsisDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip212CNsisDecoderD0Ev,@function
_ZN9NCompress6NBZip212CNsisDecoderD0Ev: # @_ZN9NCompress6NBZip212CNsisDecoderD0Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi275:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi276:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi277:
	.cfi_def_cfa_offset 32
.Lcfi278:
	.cfi_offset %rbx, -24
.Lcfi279:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp223:
	callq	_ZN9NCompress6NBZip212CNsisDecoderD2Ev
.Ltmp224:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB42_2:
.Ltmp225:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end42:
	.size	_ZN9NCompress6NBZip212CNsisDecoderD0Ev, .Lfunc_end42-_ZN9NCompress6NBZip212CNsisDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table42:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp223-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp224-.Ltmp223       #   Call between .Ltmp223 and .Ltmp224
	.long	.Ltmp225-.Lfunc_begin11 #     jumps to .Ltmp225
	.byte	0                       #   On action: cleanup
	.long	.Ltmp224-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Lfunc_end42-.Ltmp224   #   Call between .Ltmp224 and .Lfunc_end42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end43:
	.size	_ZThn8_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end43-_ZThn8_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NBZip212CNsisDecoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress6NBZip212CNsisDecoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress6NBZip212CNsisDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip212CNsisDecoder6AddRefEv,@function
_ZThn8_N9NCompress6NBZip212CNsisDecoder6AddRefEv: # @_ZThn8_N9NCompress6NBZip212CNsisDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end44:
	.size	_ZThn8_N9NCompress6NBZip212CNsisDecoder6AddRefEv, .Lfunc_end44-_ZThn8_N9NCompress6NBZip212CNsisDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NBZip212CNsisDecoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress6NBZip212CNsisDecoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress6NBZip212CNsisDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip212CNsisDecoder7ReleaseEv,@function
_ZThn8_N9NCompress6NBZip212CNsisDecoder7ReleaseEv: # @_ZThn8_N9NCompress6NBZip212CNsisDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi280:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB45_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB45_2:                               # %_ZN9NCompress6NBZip212CNsisDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end45:
	.size	_ZThn8_N9NCompress6NBZip212CNsisDecoder7ReleaseEv, .Lfunc_end45-_ZThn8_N9NCompress6NBZip212CNsisDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NBZip212CNsisDecoderD1Ev,"axG",@progbits,_ZThn8_N9NCompress6NBZip212CNsisDecoderD1Ev,comdat
	.weak	_ZThn8_N9NCompress6NBZip212CNsisDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip212CNsisDecoderD1Ev,@function
_ZThn8_N9NCompress6NBZip212CNsisDecoderD1Ev: # @_ZThn8_N9NCompress6NBZip212CNsisDecoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress6NBZip212CNsisDecoderD2Ev # TAILCALL
.Lfunc_end46:
	.size	_ZThn8_N9NCompress6NBZip212CNsisDecoderD1Ev, .Lfunc_end46-_ZThn8_N9NCompress6NBZip212CNsisDecoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NBZip212CNsisDecoderD0Ev,"axG",@progbits,_ZThn8_N9NCompress6NBZip212CNsisDecoderD0Ev,comdat
	.weak	_ZThn8_N9NCompress6NBZip212CNsisDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip212CNsisDecoderD0Ev,@function
_ZThn8_N9NCompress6NBZip212CNsisDecoderD0Ev: # @_ZThn8_N9NCompress6NBZip212CNsisDecoderD0Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi281:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi282:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi283:
	.cfi_def_cfa_offset 32
.Lcfi284:
	.cfi_offset %rbx, -24
.Lcfi285:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp226:
	movq	%rbx, %rdi
	callq	_ZN9NCompress6NBZip212CNsisDecoderD2Ev
.Ltmp227:
# BB#1:                                 # %_ZN9NCompress6NBZip212CNsisDecoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB47_2:
.Ltmp228:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end47:
	.size	_ZThn8_N9NCompress6NBZip212CNsisDecoderD0Ev, .Lfunc_end47-_ZThn8_N9NCompress6NBZip212CNsisDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table47:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp226-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp227-.Ltmp226       #   Call between .Ltmp226 and .Ltmp227
	.long	.Ltmp228-.Lfunc_begin12 #     jumps to .Ltmp228
	.byte	0                       #   On action: cleanup
	.long	.Ltmp227-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Lfunc_end47-.Ltmp227   #   Call between .Ltmp227 and .Lfunc_end47
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn16_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end48:
	.size	_ZThn16_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end48-_ZThn16_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress6NBZip212CNsisDecoder6AddRefEv,"axG",@progbits,_ZThn16_N9NCompress6NBZip212CNsisDecoder6AddRefEv,comdat
	.weak	_ZThn16_N9NCompress6NBZip212CNsisDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NBZip212CNsisDecoder6AddRefEv,@function
_ZThn16_N9NCompress6NBZip212CNsisDecoder6AddRefEv: # @_ZThn16_N9NCompress6NBZip212CNsisDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end49:
	.size	_ZThn16_N9NCompress6NBZip212CNsisDecoder6AddRefEv, .Lfunc_end49-_ZThn16_N9NCompress6NBZip212CNsisDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress6NBZip212CNsisDecoder7ReleaseEv,"axG",@progbits,_ZThn16_N9NCompress6NBZip212CNsisDecoder7ReleaseEv,comdat
	.weak	_ZThn16_N9NCompress6NBZip212CNsisDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NBZip212CNsisDecoder7ReleaseEv,@function
_ZThn16_N9NCompress6NBZip212CNsisDecoder7ReleaseEv: # @_ZThn16_N9NCompress6NBZip212CNsisDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi286:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB50_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB50_2:                               # %_ZN9NCompress6NBZip212CNsisDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end50:
	.size	_ZThn16_N9NCompress6NBZip212CNsisDecoder7ReleaseEv, .Lfunc_end50-_ZThn16_N9NCompress6NBZip212CNsisDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress6NBZip212CNsisDecoderD1Ev,"axG",@progbits,_ZThn16_N9NCompress6NBZip212CNsisDecoderD1Ev,comdat
	.weak	_ZThn16_N9NCompress6NBZip212CNsisDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NBZip212CNsisDecoderD1Ev,@function
_ZThn16_N9NCompress6NBZip212CNsisDecoderD1Ev: # @_ZThn16_N9NCompress6NBZip212CNsisDecoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress6NBZip212CNsisDecoderD2Ev # TAILCALL
.Lfunc_end51:
	.size	_ZThn16_N9NCompress6NBZip212CNsisDecoderD1Ev, .Lfunc_end51-_ZThn16_N9NCompress6NBZip212CNsisDecoderD1Ev
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress6NBZip212CNsisDecoderD0Ev,"axG",@progbits,_ZThn16_N9NCompress6NBZip212CNsisDecoderD0Ev,comdat
	.weak	_ZThn16_N9NCompress6NBZip212CNsisDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NBZip212CNsisDecoderD0Ev,@function
_ZThn16_N9NCompress6NBZip212CNsisDecoderD0Ev: # @_ZThn16_N9NCompress6NBZip212CNsisDecoderD0Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi287:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi288:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi289:
	.cfi_def_cfa_offset 32
.Lcfi290:
	.cfi_offset %rbx, -24
.Lcfi291:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp229:
	movq	%rbx, %rdi
	callq	_ZN9NCompress6NBZip212CNsisDecoderD2Ev
.Ltmp230:
# BB#1:                                 # %_ZN9NCompress6NBZip212CNsisDecoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB52_2:
.Ltmp231:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end52:
	.size	_ZThn16_N9NCompress6NBZip212CNsisDecoderD0Ev, .Lfunc_end52-_ZThn16_N9NCompress6NBZip212CNsisDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table52:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp229-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp230-.Ltmp229       #   Call between .Ltmp229 and .Ltmp230
	.long	.Ltmp231-.Lfunc_begin13 #     jumps to .Ltmp231
	.byte	0                       #   On action: cleanup
	.long	.Ltmp230-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Lfunc_end52-.Ltmp230   #   Call between .Ltmp230 and .Lfunc_end52
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi292:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi293:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi294:
	.cfi_def_cfa_offset 32
.Lcfi295:
	.cfi_offset %rbx, -32
.Lcfi296:
	.cfi_offset %r14, -24
.Lcfi297:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB53_1
# BB#2:
	movl	$IID_ICompressCoder, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB53_3
.LBB53_1:
	movq	%rbx, (%r14)
.LBB53_6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB53_7:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB53_3:
	movl	$IID_ICompressSetCoderMt, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB53_4
# BB#5:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	jmp	.LBB53_6
.LBB53_4:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB53_7
.Lfunc_end53:
	.size	_ZN9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end53-_ZN9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress6NBZip28CDecoder6AddRefEv,"axG",@progbits,_ZN9NCompress6NBZip28CDecoder6AddRefEv,comdat
	.weak	_ZN9NCompress6NBZip28CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder6AddRefEv,@function
_ZN9NCompress6NBZip28CDecoder6AddRefEv: # @_ZN9NCompress6NBZip28CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end54:
	.size	_ZN9NCompress6NBZip28CDecoder6AddRefEv, .Lfunc_end54-_ZN9NCompress6NBZip28CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress6NBZip28CDecoder7ReleaseEv,"axG",@progbits,_ZN9NCompress6NBZip28CDecoder7ReleaseEv,comdat
	.weak	_ZN9NCompress6NBZip28CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CDecoder7ReleaseEv,@function
_ZN9NCompress6NBZip28CDecoder7ReleaseEv: # @_ZN9NCompress6NBZip28CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi298:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB55_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB55_2:
	popq	%rcx
	retq
.Lfunc_end55:
	.size	_ZN9NCompress6NBZip28CDecoder7ReleaseEv, .Lfunc_end55-_ZN9NCompress6NBZip28CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi299:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB56_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB56_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB56_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB56_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB56_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB56_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB56_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB56_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB56_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB56_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB56_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB56_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB56_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB56_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB56_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB56_16
.LBB56_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICompressCoder(%rip), %cl
	jne	.LBB56_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICompressCoder+1(%rip), %al
	jne	.LBB56_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICompressCoder+2(%rip), %al
	jne	.LBB56_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICompressCoder+3(%rip), %al
	jne	.LBB56_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICompressCoder+4(%rip), %al
	jne	.LBB56_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICompressCoder+5(%rip), %al
	jne	.LBB56_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICompressCoder+6(%rip), %al
	jne	.LBB56_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICompressCoder+7(%rip), %al
	jne	.LBB56_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICompressCoder+8(%rip), %al
	jne	.LBB56_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICompressCoder+9(%rip), %al
	jne	.LBB56_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICompressCoder+10(%rip), %al
	jne	.LBB56_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICompressCoder+11(%rip), %al
	jne	.LBB56_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICompressCoder+12(%rip), %al
	jne	.LBB56_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICompressCoder+13(%rip), %al
	jne	.LBB56_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICompressCoder+14(%rip), %al
	jne	.LBB56_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %al
	cmpb	IID_ICompressCoder+15(%rip), %al
	jne	.LBB56_33
.LBB56_16:
	movq	%rdi, (%rdx)
	jmp	.LBB56_50
.LBB56_33:                              # %_ZeqRK4GUIDS1_.exit6.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressSetCoderMt(%rip), %cl
	jne	.LBB56_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressSetCoderMt+1(%rip), %cl
	jne	.LBB56_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressSetCoderMt+2(%rip), %cl
	jne	.LBB56_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressSetCoderMt+3(%rip), %cl
	jne	.LBB56_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressSetCoderMt+4(%rip), %cl
	jne	.LBB56_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressSetCoderMt+5(%rip), %cl
	jne	.LBB56_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressSetCoderMt+6(%rip), %cl
	jne	.LBB56_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressSetCoderMt+7(%rip), %cl
	jne	.LBB56_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressSetCoderMt+8(%rip), %cl
	jne	.LBB56_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressSetCoderMt+9(%rip), %cl
	jne	.LBB56_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressSetCoderMt+10(%rip), %cl
	jne	.LBB56_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressSetCoderMt+11(%rip), %cl
	jne	.LBB56_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressSetCoderMt+12(%rip), %cl
	jne	.LBB56_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressSetCoderMt+13(%rip), %cl
	jne	.LBB56_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressSetCoderMt+14(%rip), %cl
	jne	.LBB56_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICompressSetCoderMt+15(%rip), %cl
	jne	.LBB56_51
# BB#49:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
.LBB56_50:                              # %_ZN9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv.exit
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB56_51:                              # %_ZN9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end56:
	.size	_ZThn8_N9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end56-_ZThn8_N9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NBZip28CDecoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress6NBZip28CDecoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress6NBZip28CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip28CDecoder6AddRefEv,@function
_ZThn8_N9NCompress6NBZip28CDecoder6AddRefEv: # @_ZThn8_N9NCompress6NBZip28CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end57:
	.size	_ZThn8_N9NCompress6NBZip28CDecoder6AddRefEv, .Lfunc_end57-_ZThn8_N9NCompress6NBZip28CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NBZip28CDecoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress6NBZip28CDecoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress6NBZip28CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip28CDecoder7ReleaseEv,@function
_ZThn8_N9NCompress6NBZip28CDecoder7ReleaseEv: # @_ZThn8_N9NCompress6NBZip28CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi300:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB58_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB58_2:                               # %_ZN9NCompress6NBZip28CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end58:
	.size	_ZThn8_N9NCompress6NBZip28CDecoder7ReleaseEv, .Lfunc_end58-_ZThn8_N9NCompress6NBZip28CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NHuffman8CDecoderILi20ELj258EE14SetCodeLengthsEPKh,"axG",@progbits,_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE14SetCodeLengthsEPKh,comdat
	.weak	_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE14SetCodeLengthsEPKh
	.p2align	4, 0x90
	.type	_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE14SetCodeLengthsEPKh,@function
_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE14SetCodeLengthsEPKh: # @_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE14SetCodeLengthsEPKh
	.cfi_startproc
# BB#0:                                 # %.preheader66.preheader
	pushq	%rbp
.Lcfi301:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi302:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi303:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi304:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi305:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi306:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi307:
	.cfi_def_cfa_offset 240
.Lcfi308:
	.cfi_offset %rbx, -56
.Lcfi309:
	.cfi_offset %r12, -48
.Lcfi310:
	.cfi_offset %r13, -40
.Lcfi311:
	.cfi_offset %r14, -32
.Lcfi312:
	.cfi_offset %r15, -24
.Lcfi313:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 68(%rsp)
	movups	%xmm0, 52(%rsp)
	movups	%xmm0, 36(%rsp)
	movups	%xmm0, 20(%rsp)
	movups	%xmm0, 4(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB59_1:                               # %.preheader66
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rax), %ecx
	cmpq	$20, %rcx
	ja	.LBB59_19
# BB#2:                                 # %.critedge
                                        #   in Loop: Header=BB59_1 Depth=1
	incl	(%rsp,%rcx,4)
	movl	$-1, 168(%r15,%rax,4)
	movzbl	1(%r14,%rax), %ecx
	cmpq	$20, %rcx
	ja	.LBB59_19
# BB#3:                                 # %.critedge.1
                                        #   in Loop: Header=BB59_1 Depth=1
	incl	(%rsp,%rcx,4)
	movl	$-1, 172(%r15,%rax,4)
	addq	$2, %rax
	cmpl	$258, %eax              # imm = 0x102
	jb	.LBB59_1
# BB#4:
	movl	$0, (%rsp)
	movl	$0, (%r15)
	movl	$0, 84(%r15)
	xorl	%eax, %eax
	movl	$19, %r13d
	movl	$1, %ebx
	movl	$1048576, %esi          # imm = 0x100000
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB59_5:                               # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edx
	movl	(%rsp,%rbx,4), %ebp
	movl	%r13d, %ecx
	shll	%cl, %ebp
	addl	%edx, %ebp
	cmpl	$1048576, %ebp          # imm = 0x100000
	ja	.LBB59_19
# BB#6:                                 #   in Loop: Header=BB59_5 Depth=1
	cmpq	$20, %rbx
	movl	%ebp, %r12d
	cmovel	%esi, %r12d
	movl	%r12d, (%r15,%rbx,4)
	movl	-4(%rsp,%rbx,4), %ecx
	addl	80(%r15,%rbx,4), %ecx
	movl	%ecx, 84(%r15,%rbx,4)
	movl	%ecx, 96(%rsp,%rbx,4)
	cmpq	$9, %rbx
	jg	.LBB59_10
# BB#7:                                 #   in Loop: Header=BB59_5 Depth=1
	shrl	$11, %r12d
	cmpl	%r12d, %eax
	jae	.LBB59_10
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB59_5 Depth=1
	movl	%eax, %eax
	leaq	1200(%r15,%rax), %rdi
	movl	%r12d, %edx
	subq	%rax, %rdx
	movl	%ebx, %esi
	callq	memset
	movl	$1048576, %esi          # imm = 0x100000
	jmp	.LBB59_11
	.p2align	4, 0x90
.LBB59_10:                              #   in Loop: Header=BB59_5 Depth=1
	movl	%eax, %r12d
.LBB59_11:                              # %.loopexit64
                                        #   in Loop: Header=BB59_5 Depth=1
	incq	%rbx
	decl	%r13d
	cmpq	$21, %rbx
	movl	%r12d, %eax
	jl	.LBB59_5
# BB#12:                                # %.preheader.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB59_13:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB59_15
# BB#14:                                #   in Loop: Header=BB59_13 Depth=1
	movl	96(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, 96(%rsp,%rcx,4)
	movl	%eax, 168(%r15,%rdx,4)
.LBB59_15:                              # %.preheader.194
                                        #   in Loop: Header=BB59_13 Depth=1
	movzbl	1(%r14,%rax), %ecx
	testq	%rcx, %rcx
	je	.LBB59_17
# BB#16:                                #   in Loop: Header=BB59_13 Depth=1
	movl	96(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, 96(%rsp,%rcx,4)
	leal	1(%rax), %ecx
	movl	%ecx, 168(%r15,%rdx,4)
.LBB59_17:                              #   in Loop: Header=BB59_13 Depth=1
	addq	$2, %rax
	cmpq	$258, %rax              # imm = 0x102
	jne	.LBB59_13
# BB#18:
	movb	$1, %al
	jmp	.LBB59_20
.LBB59_19:
	xorl	%eax, %eax
.LBB59_20:                              # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end59:
	.size	_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE14SetCodeLengthsEPKh, .Lfunc_end59-_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE14SetCodeLengthsEPKh
	.cfi_endproc

	.section	.text._ZN9NCompress8NHuffman8CDecoderILi20ELj258EE12DecodeSymbolIN5NBitm8CDecoderI9CInBufferEEEEjPT_,"axG",@progbits,_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE12DecodeSymbolIN5NBitm8CDecoderI9CInBufferEEEEjPT_,comdat
	.weak	_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE12DecodeSymbolIN5NBitm8CDecoderI9CInBufferEEEEjPT_
	.p2align	4, 0x90
	.type	_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE12DecodeSymbolIN5NBitm8CDecoderI9CInBufferEEEEjPT_,@function
_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE12DecodeSymbolIN5NBitm8CDecoderI9CInBufferEEEEjPT_: # @_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE12DecodeSymbolIN5NBitm8CDecoderI9CInBufferEEEEjPT_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi314:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi315:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi316:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi317:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi318:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi319:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi320:
	.cfi_def_cfa_offset 64
.Lcfi321:
	.cfi_offset %rbx, -56
.Lcfi322:
	.cfi_offset %r12, -48
.Lcfi323:
	.cfi_offset %r13, -40
.Lcfi324:
	.cfi_offset %r14, -32
.Lcfi325:
	.cfi_offset %r15, -24
.Lcfi326:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	movl	(%r13), %edx
	movl	4(%r13), %ebp
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	%ebp, %r12d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r12d
	shrl	$4, %r12d
	andl	$1048575, %r12d         # imm = 0xFFFFF
	cmpl	36(%r14), %r12d
	jae	.LBB60_1
# BB#13:
	movl	%r12d, %eax
	shrl	$11, %eax
	movzbl	1200(%r14,%rax), %ebx
	jmp	.LBB60_4
.LBB60_1:                               # %.preheader.preheader
	movl	$10, %ebx
	.p2align	4, 0x90
.LBB60_2:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpl	(%r14,%rbx,4), %r12d
	leaq	1(%rbx), %rbx
	jae	.LBB60_2
# BB#3:                                 # %.loopexit.loopexit
	decl	%ebx
.LBB60_4:                               # %.loopexit
	addl	%ebx, %edx
	movl	%edx, (%r13)
	cmpl	$8, %edx
	jb	.LBB60_10
# BB#5:                                 # %.lr.ph.i.i
	leaq	8(%r13), %r15
	.p2align	4, 0x90
.LBB60_6:                               # =>This Inner Loop Header: Depth=1
	shll	$8, %ebp
	movq	8(%r13), %rax
	cmpq	16(%r13), %rax
	jae	.LBB60_7
# BB#8:                                 #   in Loop: Header=BB60_6 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r15)
	movzbl	(%rax), %eax
	jmp	.LBB60_9
	.p2align	4, 0x90
.LBB60_7:                               #   in Loop: Header=BB60_6 Depth=1
	movq	%r15, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	(%r13), %edx
.LBB60_9:                               # %_ZN9CInBuffer8ReadByteEv.exit.i.i
                                        #   in Loop: Header=BB60_6 Depth=1
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 4(%r13)
	addl	$-8, %edx
	movl	%edx, (%r13)
	cmpl	$7, %edx
	ja	.LBB60_6
.LBB60_10:                              # %_ZN5NBitm8CDecoderI9CInBufferE7MovePosEj.exit
	movslq	%ebx, %rax
	subl	-4(%r14,%rax,4), %r12d
	movl	$20, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r12d
	addl	84(%r14,%rax,4), %r12d
	movl	$-1, %eax
	cmpl	$257, %r12d             # imm = 0x101
	ja	.LBB60_12
# BB#11:
	movl	%r12d, %eax
	movl	168(%r14,%rax,4), %eax
.LBB60_12:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end60:
	.size	_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE12DecodeSymbolIN5NBitm8CDecoderI9CInBufferEEEEjPT_, .Lfunc_end60-_ZN9NCompress8NHuffman8CDecoderILi20ELj258EE12DecodeSymbolIN5NBitm8CDecoderI9CInBufferEEEEjPT_
	.cfi_endproc

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB61_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB61_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB61_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB61_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB61_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB61_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB61_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB61_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB61_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB61_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB61_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB61_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB61_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB61_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB61_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB61_16:
	xorl	%eax, %eax
	retq
.Lfunc_end61:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end61-_ZeqRK4GUIDS1_
	.cfi_endproc

	.type	_ZTVN9NCompress6NBZip28CDecoderE,@object # @_ZTVN9NCompress6NBZip28CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress6NBZip28CDecoderE
	.p2align	3
_ZTVN9NCompress6NBZip28CDecoderE:
	.quad	0
	.quad	_ZTIN9NCompress6NBZip28CDecoderE
	.quad	_ZN9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress6NBZip28CDecoder6AddRefEv
	.quad	_ZN9NCompress6NBZip28CDecoder7ReleaseEv
	.quad	_ZN9NCompress6NBZip28CDecoderD2Ev
	.quad	_ZN9NCompress6NBZip28CDecoderD0Ev
	.quad	_ZN9NCompress6NBZip28CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.quad	_ZN9NCompress6NBZip28CDecoder11SetInStreamEP19ISequentialInStream
	.quad	_ZN9NCompress6NBZip28CDecoder15ReleaseInStreamEv
	.quad	_ZN9NCompress6NBZip28CDecoder18SetNumberOfThreadsEj
	.quad	-8
	.quad	_ZTIN9NCompress6NBZip28CDecoderE
	.quad	_ZThn8_N9NCompress6NBZip28CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress6NBZip28CDecoder6AddRefEv
	.quad	_ZThn8_N9NCompress6NBZip28CDecoder7ReleaseEv
	.quad	_ZThn8_N9NCompress6NBZip28CDecoderD1Ev
	.quad	_ZThn8_N9NCompress6NBZip28CDecoderD0Ev
	.quad	_ZThn8_N9NCompress6NBZip28CDecoder18SetNumberOfThreadsEj
	.size	_ZTVN9NCompress6NBZip28CDecoderE, 152

	.type	_ZTS18CInBufferException,@object # @_ZTS18CInBufferException
	.section	.rodata._ZTS18CInBufferException,"aG",@progbits,_ZTS18CInBufferException,comdat
	.weak	_ZTS18CInBufferException
	.p2align	4
_ZTS18CInBufferException:
	.asciz	"18CInBufferException"
	.size	_ZTS18CInBufferException, 21

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	_ZTI18CInBufferException,@object # @_ZTI18CInBufferException
	.section	.rodata._ZTI18CInBufferException,"aG",@progbits,_ZTI18CInBufferException,comdat
	.weak	_ZTI18CInBufferException
	.p2align	4
_ZTI18CInBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18CInBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI18CInBufferException, 24

	.type	_ZTS19COutBufferException,@object # @_ZTS19COutBufferException
	.section	.rodata._ZTS19COutBufferException,"aG",@progbits,_ZTS19COutBufferException,comdat
	.weak	_ZTS19COutBufferException
	.p2align	4
_ZTS19COutBufferException:
	.asciz	"19COutBufferException"
	.size	_ZTS19COutBufferException, 22

	.type	_ZTI19COutBufferException,@object # @_ZTI19COutBufferException
	.section	.rodata._ZTI19COutBufferException,"aG",@progbits,_ZTI19COutBufferException,comdat
	.weak	_ZTI19COutBufferException
	.p2align	4
_ZTI19COutBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19COutBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI19COutBufferException, 24

	.type	_ZTVN9NCompress6NBZip212CNsisDecoderE,@object # @_ZTVN9NCompress6NBZip212CNsisDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress6NBZip212CNsisDecoderE
	.p2align	3
_ZTVN9NCompress6NBZip212CNsisDecoderE:
	.quad	0
	.quad	_ZTIN9NCompress6NBZip212CNsisDecoderE
	.quad	_ZN9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress6NBZip212CNsisDecoder6AddRefEv
	.quad	_ZN9NCompress6NBZip212CNsisDecoder7ReleaseEv
	.quad	_ZN9NCompress6NBZip212CNsisDecoderD2Ev
	.quad	_ZN9NCompress6NBZip212CNsisDecoderD0Ev
	.quad	_ZN9NCompress6NBZip212CNsisDecoder4ReadEPvjPj
	.quad	_ZN9NCompress6NBZip212CNsisDecoder11SetInStreamEP19ISequentialInStream
	.quad	_ZN9NCompress6NBZip212CNsisDecoder15ReleaseInStreamEv
	.quad	_ZN9NCompress6NBZip212CNsisDecoder16SetOutStreamSizeEPKy
	.quad	-8
	.quad	_ZTIN9NCompress6NBZip212CNsisDecoderE
	.quad	_ZThn8_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress6NBZip212CNsisDecoder6AddRefEv
	.quad	_ZThn8_N9NCompress6NBZip212CNsisDecoder7ReleaseEv
	.quad	_ZThn8_N9NCompress6NBZip212CNsisDecoderD1Ev
	.quad	_ZThn8_N9NCompress6NBZip212CNsisDecoderD0Ev
	.quad	_ZThn8_N9NCompress6NBZip212CNsisDecoder11SetInStreamEP19ISequentialInStream
	.quad	_ZThn8_N9NCompress6NBZip212CNsisDecoder15ReleaseInStreamEv
	.quad	-16
	.quad	_ZTIN9NCompress6NBZip212CNsisDecoderE
	.quad	_ZThn16_N9NCompress6NBZip212CNsisDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N9NCompress6NBZip212CNsisDecoder6AddRefEv
	.quad	_ZThn16_N9NCompress6NBZip212CNsisDecoder7ReleaseEv
	.quad	_ZThn16_N9NCompress6NBZip212CNsisDecoderD1Ev
	.quad	_ZThn16_N9NCompress6NBZip212CNsisDecoderD0Ev
	.quad	_ZThn16_N9NCompress6NBZip212CNsisDecoder16SetOutStreamSizeEPKy
	.size	_ZTVN9NCompress6NBZip212CNsisDecoderE, 224

	.type	_ZTSN9NCompress6NBZip212CNsisDecoderE,@object # @_ZTSN9NCompress6NBZip212CNsisDecoderE
	.globl	_ZTSN9NCompress6NBZip212CNsisDecoderE
	.p2align	4
_ZTSN9NCompress6NBZip212CNsisDecoderE:
	.asciz	"N9NCompress6NBZip212CNsisDecoderE"
	.size	_ZTSN9NCompress6NBZip212CNsisDecoderE, 34

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTS20ICompressSetInStream,@object # @_ZTS20ICompressSetInStream
	.section	.rodata._ZTS20ICompressSetInStream,"aG",@progbits,_ZTS20ICompressSetInStream,comdat
	.weak	_ZTS20ICompressSetInStream
	.p2align	4
_ZTS20ICompressSetInStream:
	.asciz	"20ICompressSetInStream"
	.size	_ZTS20ICompressSetInStream, 23

	.type	_ZTI20ICompressSetInStream,@object # @_ZTI20ICompressSetInStream
	.section	.rodata._ZTI20ICompressSetInStream,"aG",@progbits,_ZTI20ICompressSetInStream,comdat
	.weak	_ZTI20ICompressSetInStream
	.p2align	4
_ZTI20ICompressSetInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ICompressSetInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ICompressSetInStream, 24

	.type	_ZTS25ICompressSetOutStreamSize,@object # @_ZTS25ICompressSetOutStreamSize
	.section	.rodata._ZTS25ICompressSetOutStreamSize,"aG",@progbits,_ZTS25ICompressSetOutStreamSize,comdat
	.weak	_ZTS25ICompressSetOutStreamSize
	.p2align	4
_ZTS25ICompressSetOutStreamSize:
	.asciz	"25ICompressSetOutStreamSize"
	.size	_ZTS25ICompressSetOutStreamSize, 28

	.type	_ZTI25ICompressSetOutStreamSize,@object # @_ZTI25ICompressSetOutStreamSize
	.section	.rodata._ZTI25ICompressSetOutStreamSize,"aG",@progbits,_ZTI25ICompressSetOutStreamSize,comdat
	.weak	_ZTI25ICompressSetOutStreamSize
	.p2align	4
_ZTI25ICompressSetOutStreamSize:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS25ICompressSetOutStreamSize
	.quad	_ZTI8IUnknown
	.size	_ZTI25ICompressSetOutStreamSize, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress6NBZip212CNsisDecoderE,@object # @_ZTIN9NCompress6NBZip212CNsisDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress6NBZip212CNsisDecoderE
	.p2align	4
_ZTIN9NCompress6NBZip212CNsisDecoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress6NBZip212CNsisDecoderE
	.long	1                       # 0x1
	.long	4                       # 0x4
	.quad	_ZTI19ISequentialInStream
	.quad	2                       # 0x2
	.quad	_ZTI20ICompressSetInStream
	.quad	2050                    # 0x802
	.quad	_ZTI25ICompressSetOutStreamSize
	.quad	4098                    # 0x1002
	.quad	_ZTI13CMyUnknownImp
	.quad	6146                    # 0x1802
	.size	_ZTIN9NCompress6NBZip212CNsisDecoderE, 88

	.type	_ZTSN9NCompress6NBZip28CDecoderE,@object # @_ZTSN9NCompress6NBZip28CDecoderE
	.globl	_ZTSN9NCompress6NBZip28CDecoderE
	.p2align	4
_ZTSN9NCompress6NBZip28CDecoderE:
	.asciz	"N9NCompress6NBZip28CDecoderE"
	.size	_ZTSN9NCompress6NBZip28CDecoderE, 29

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS19ICompressSetCoderMt,@object # @_ZTS19ICompressSetCoderMt
	.section	.rodata._ZTS19ICompressSetCoderMt,"aG",@progbits,_ZTS19ICompressSetCoderMt,comdat
	.weak	_ZTS19ICompressSetCoderMt
	.p2align	4
_ZTS19ICompressSetCoderMt:
	.asciz	"19ICompressSetCoderMt"
	.size	_ZTS19ICompressSetCoderMt, 22

	.type	_ZTI19ICompressSetCoderMt,@object # @_ZTI19ICompressSetCoderMt
	.section	.rodata._ZTI19ICompressSetCoderMt,"aG",@progbits,_ZTI19ICompressSetCoderMt,comdat
	.weak	_ZTI19ICompressSetCoderMt
	.p2align	4
_ZTI19ICompressSetCoderMt:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ICompressSetCoderMt
	.quad	_ZTI8IUnknown
	.size	_ZTI19ICompressSetCoderMt, 24

	.type	_ZTIN9NCompress6NBZip28CDecoderE,@object # @_ZTIN9NCompress6NBZip28CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress6NBZip28CDecoderE
	.p2align	4
_ZTIN9NCompress6NBZip28CDecoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress6NBZip28CDecoderE
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI19ICompressSetCoderMt
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTIN9NCompress6NBZip28CDecoderE, 72

	.type	_ZN9NCompress6NBZip2L9kRandNumsE,@object # @_ZN9NCompress6NBZip2L9kRandNumsE
	.p2align	4
_ZN9NCompress6NBZip2L9kRandNumsE:
	.short	619                     # 0x26b
	.short	720                     # 0x2d0
	.short	127                     # 0x7f
	.short	481                     # 0x1e1
	.short	931                     # 0x3a3
	.short	816                     # 0x330
	.short	813                     # 0x32d
	.short	233                     # 0xe9
	.short	566                     # 0x236
	.short	247                     # 0xf7
	.short	985                     # 0x3d9
	.short	724                     # 0x2d4
	.short	205                     # 0xcd
	.short	454                     # 0x1c6
	.short	863                     # 0x35f
	.short	491                     # 0x1eb
	.short	741                     # 0x2e5
	.short	242                     # 0xf2
	.short	949                     # 0x3b5
	.short	214                     # 0xd6
	.short	733                     # 0x2dd
	.short	859                     # 0x35b
	.short	335                     # 0x14f
	.short	708                     # 0x2c4
	.short	621                     # 0x26d
	.short	574                     # 0x23e
	.short	73                      # 0x49
	.short	654                     # 0x28e
	.short	730                     # 0x2da
	.short	472                     # 0x1d8
	.short	419                     # 0x1a3
	.short	436                     # 0x1b4
	.short	278                     # 0x116
	.short	496                     # 0x1f0
	.short	867                     # 0x363
	.short	210                     # 0xd2
	.short	399                     # 0x18f
	.short	680                     # 0x2a8
	.short	480                     # 0x1e0
	.short	51                      # 0x33
	.short	878                     # 0x36e
	.short	465                     # 0x1d1
	.short	811                     # 0x32b
	.short	169                     # 0xa9
	.short	869                     # 0x365
	.short	675                     # 0x2a3
	.short	611                     # 0x263
	.short	697                     # 0x2b9
	.short	867                     # 0x363
	.short	561                     # 0x231
	.short	862                     # 0x35e
	.short	687                     # 0x2af
	.short	507                     # 0x1fb
	.short	283                     # 0x11b
	.short	482                     # 0x1e2
	.short	129                     # 0x81
	.short	807                     # 0x327
	.short	591                     # 0x24f
	.short	733                     # 0x2dd
	.short	623                     # 0x26f
	.short	150                     # 0x96
	.short	238                     # 0xee
	.short	59                      # 0x3b
	.short	379                     # 0x17b
	.short	684                     # 0x2ac
	.short	877                     # 0x36d
	.short	625                     # 0x271
	.short	169                     # 0xa9
	.short	643                     # 0x283
	.short	105                     # 0x69
	.short	170                     # 0xaa
	.short	607                     # 0x25f
	.short	520                     # 0x208
	.short	932                     # 0x3a4
	.short	727                     # 0x2d7
	.short	476                     # 0x1dc
	.short	693                     # 0x2b5
	.short	425                     # 0x1a9
	.short	174                     # 0xae
	.short	647                     # 0x287
	.short	73                      # 0x49
	.short	122                     # 0x7a
	.short	335                     # 0x14f
	.short	530                     # 0x212
	.short	442                     # 0x1ba
	.short	853                     # 0x355
	.short	695                     # 0x2b7
	.short	249                     # 0xf9
	.short	445                     # 0x1bd
	.short	515                     # 0x203
	.short	909                     # 0x38d
	.short	545                     # 0x221
	.short	703                     # 0x2bf
	.short	919                     # 0x397
	.short	874                     # 0x36a
	.short	474                     # 0x1da
	.short	882                     # 0x372
	.short	500                     # 0x1f4
	.short	594                     # 0x252
	.short	612                     # 0x264
	.short	641                     # 0x281
	.short	801                     # 0x321
	.short	220                     # 0xdc
	.short	162                     # 0xa2
	.short	819                     # 0x333
	.short	984                     # 0x3d8
	.short	589                     # 0x24d
	.short	513                     # 0x201
	.short	495                     # 0x1ef
	.short	799                     # 0x31f
	.short	161                     # 0xa1
	.short	604                     # 0x25c
	.short	958                     # 0x3be
	.short	533                     # 0x215
	.short	221                     # 0xdd
	.short	400                     # 0x190
	.short	386                     # 0x182
	.short	867                     # 0x363
	.short	600                     # 0x258
	.short	782                     # 0x30e
	.short	382                     # 0x17e
	.short	596                     # 0x254
	.short	414                     # 0x19e
	.short	171                     # 0xab
	.short	516                     # 0x204
	.short	375                     # 0x177
	.short	682                     # 0x2aa
	.short	485                     # 0x1e5
	.short	911                     # 0x38f
	.short	276                     # 0x114
	.short	98                      # 0x62
	.short	553                     # 0x229
	.short	163                     # 0xa3
	.short	354                     # 0x162
	.short	666                     # 0x29a
	.short	933                     # 0x3a5
	.short	424                     # 0x1a8
	.short	341                     # 0x155
	.short	533                     # 0x215
	.short	870                     # 0x366
	.short	227                     # 0xe3
	.short	730                     # 0x2da
	.short	475                     # 0x1db
	.short	186                     # 0xba
	.short	263                     # 0x107
	.short	647                     # 0x287
	.short	537                     # 0x219
	.short	686                     # 0x2ae
	.short	600                     # 0x258
	.short	224                     # 0xe0
	.short	469                     # 0x1d5
	.short	68                      # 0x44
	.short	770                     # 0x302
	.short	919                     # 0x397
	.short	190                     # 0xbe
	.short	373                     # 0x175
	.short	294                     # 0x126
	.short	822                     # 0x336
	.short	808                     # 0x328
	.short	206                     # 0xce
	.short	184                     # 0xb8
	.short	943                     # 0x3af
	.short	795                     # 0x31b
	.short	384                     # 0x180
	.short	383                     # 0x17f
	.short	461                     # 0x1cd
	.short	404                     # 0x194
	.short	758                     # 0x2f6
	.short	839                     # 0x347
	.short	887                     # 0x377
	.short	715                     # 0x2cb
	.short	67                      # 0x43
	.short	618                     # 0x26a
	.short	276                     # 0x114
	.short	204                     # 0xcc
	.short	918                     # 0x396
	.short	873                     # 0x369
	.short	777                     # 0x309
	.short	604                     # 0x25c
	.short	560                     # 0x230
	.short	951                     # 0x3b7
	.short	160                     # 0xa0
	.short	578                     # 0x242
	.short	722                     # 0x2d2
	.short	79                      # 0x4f
	.short	804                     # 0x324
	.short	96                      # 0x60
	.short	409                     # 0x199
	.short	713                     # 0x2c9
	.short	940                     # 0x3ac
	.short	652                     # 0x28c
	.short	934                     # 0x3a6
	.short	970                     # 0x3ca
	.short	447                     # 0x1bf
	.short	318                     # 0x13e
	.short	353                     # 0x161
	.short	859                     # 0x35b
	.short	672                     # 0x2a0
	.short	112                     # 0x70
	.short	785                     # 0x311
	.short	645                     # 0x285
	.short	863                     # 0x35f
	.short	803                     # 0x323
	.short	350                     # 0x15e
	.short	139                     # 0x8b
	.short	93                      # 0x5d
	.short	354                     # 0x162
	.short	99                      # 0x63
	.short	820                     # 0x334
	.short	908                     # 0x38c
	.short	609                     # 0x261
	.short	772                     # 0x304
	.short	154                     # 0x9a
	.short	274                     # 0x112
	.short	580                     # 0x244
	.short	184                     # 0xb8
	.short	79                      # 0x4f
	.short	626                     # 0x272
	.short	630                     # 0x276
	.short	742                     # 0x2e6
	.short	653                     # 0x28d
	.short	282                     # 0x11a
	.short	762                     # 0x2fa
	.short	623                     # 0x26f
	.short	680                     # 0x2a8
	.short	81                      # 0x51
	.short	927                     # 0x39f
	.short	626                     # 0x272
	.short	789                     # 0x315
	.short	125                     # 0x7d
	.short	411                     # 0x19b
	.short	521                     # 0x209
	.short	938                     # 0x3aa
	.short	300                     # 0x12c
	.short	821                     # 0x335
	.short	78                      # 0x4e
	.short	343                     # 0x157
	.short	175                     # 0xaf
	.short	128                     # 0x80
	.short	250                     # 0xfa
	.short	170                     # 0xaa
	.short	774                     # 0x306
	.short	972                     # 0x3cc
	.short	275                     # 0x113
	.short	999                     # 0x3e7
	.short	639                     # 0x27f
	.short	495                     # 0x1ef
	.short	78                      # 0x4e
	.short	352                     # 0x160
	.short	126                     # 0x7e
	.short	857                     # 0x359
	.short	956                     # 0x3bc
	.short	358                     # 0x166
	.short	619                     # 0x26b
	.short	580                     # 0x244
	.short	124                     # 0x7c
	.short	737                     # 0x2e1
	.short	594                     # 0x252
	.short	701                     # 0x2bd
	.short	612                     # 0x264
	.short	669                     # 0x29d
	.short	112                     # 0x70
	.short	134                     # 0x86
	.short	694                     # 0x2b6
	.short	363                     # 0x16b
	.short	992                     # 0x3e0
	.short	809                     # 0x329
	.short	743                     # 0x2e7
	.short	168                     # 0xa8
	.short	974                     # 0x3ce
	.short	944                     # 0x3b0
	.short	375                     # 0x177
	.short	748                     # 0x2ec
	.short	52                      # 0x34
	.short	600                     # 0x258
	.short	747                     # 0x2eb
	.short	642                     # 0x282
	.short	182                     # 0xb6
	.short	862                     # 0x35e
	.short	81                      # 0x51
	.short	344                     # 0x158
	.short	805                     # 0x325
	.short	988                     # 0x3dc
	.short	739                     # 0x2e3
	.short	511                     # 0x1ff
	.short	655                     # 0x28f
	.short	814                     # 0x32e
	.short	334                     # 0x14e
	.short	249                     # 0xf9
	.short	515                     # 0x203
	.short	897                     # 0x381
	.short	955                     # 0x3bb
	.short	664                     # 0x298
	.short	981                     # 0x3d5
	.short	649                     # 0x289
	.short	113                     # 0x71
	.short	974                     # 0x3ce
	.short	459                     # 0x1cb
	.short	893                     # 0x37d
	.short	228                     # 0xe4
	.short	433                     # 0x1b1
	.short	837                     # 0x345
	.short	553                     # 0x229
	.short	268                     # 0x10c
	.short	926                     # 0x39e
	.short	240                     # 0xf0
	.short	102                     # 0x66
	.short	654                     # 0x28e
	.short	459                     # 0x1cb
	.short	51                      # 0x33
	.short	686                     # 0x2ae
	.short	754                     # 0x2f2
	.short	806                     # 0x326
	.short	760                     # 0x2f8
	.short	493                     # 0x1ed
	.short	403                     # 0x193
	.short	415                     # 0x19f
	.short	394                     # 0x18a
	.short	687                     # 0x2af
	.short	700                     # 0x2bc
	.short	946                     # 0x3b2
	.short	670                     # 0x29e
	.short	656                     # 0x290
	.short	610                     # 0x262
	.short	738                     # 0x2e2
	.short	392                     # 0x188
	.short	760                     # 0x2f8
	.short	799                     # 0x31f
	.short	887                     # 0x377
	.short	653                     # 0x28d
	.short	978                     # 0x3d2
	.short	321                     # 0x141
	.short	576                     # 0x240
	.short	617                     # 0x269
	.short	626                     # 0x272
	.short	502                     # 0x1f6
	.short	894                     # 0x37e
	.short	679                     # 0x2a7
	.short	243                     # 0xf3
	.short	440                     # 0x1b8
	.short	680                     # 0x2a8
	.short	879                     # 0x36f
	.short	194                     # 0xc2
	.short	572                     # 0x23c
	.short	640                     # 0x280
	.short	724                     # 0x2d4
	.short	926                     # 0x39e
	.short	56                      # 0x38
	.short	204                     # 0xcc
	.short	700                     # 0x2bc
	.short	707                     # 0x2c3
	.short	151                     # 0x97
	.short	457                     # 0x1c9
	.short	449                     # 0x1c1
	.short	797                     # 0x31d
	.short	195                     # 0xc3
	.short	791                     # 0x317
	.short	558                     # 0x22e
	.short	945                     # 0x3b1
	.short	679                     # 0x2a7
	.short	297                     # 0x129
	.short	59                      # 0x3b
	.short	87                      # 0x57
	.short	824                     # 0x338
	.short	713                     # 0x2c9
	.short	663                     # 0x297
	.short	412                     # 0x19c
	.short	693                     # 0x2b5
	.short	342                     # 0x156
	.short	606                     # 0x25e
	.short	134                     # 0x86
	.short	108                     # 0x6c
	.short	571                     # 0x23b
	.short	364                     # 0x16c
	.short	631                     # 0x277
	.short	212                     # 0xd4
	.short	174                     # 0xae
	.short	643                     # 0x283
	.short	304                     # 0x130
	.short	329                     # 0x149
	.short	343                     # 0x157
	.short	97                      # 0x61
	.short	430                     # 0x1ae
	.short	751                     # 0x2ef
	.short	497                     # 0x1f1
	.short	314                     # 0x13a
	.short	983                     # 0x3d7
	.short	374                     # 0x176
	.short	822                     # 0x336
	.short	928                     # 0x3a0
	.short	140                     # 0x8c
	.short	206                     # 0xce
	.short	73                      # 0x49
	.short	263                     # 0x107
	.short	980                     # 0x3d4
	.short	736                     # 0x2e0
	.short	876                     # 0x36c
	.short	478                     # 0x1de
	.short	430                     # 0x1ae
	.short	305                     # 0x131
	.short	170                     # 0xaa
	.short	514                     # 0x202
	.short	364                     # 0x16c
	.short	692                     # 0x2b4
	.short	829                     # 0x33d
	.short	82                      # 0x52
	.short	855                     # 0x357
	.short	953                     # 0x3b9
	.short	676                     # 0x2a4
	.short	246                     # 0xf6
	.short	369                     # 0x171
	.short	970                     # 0x3ca
	.short	294                     # 0x126
	.short	750                     # 0x2ee
	.short	807                     # 0x327
	.short	827                     # 0x33b
	.short	150                     # 0x96
	.short	790                     # 0x316
	.short	288                     # 0x120
	.short	923                     # 0x39b
	.short	804                     # 0x324
	.short	378                     # 0x17a
	.short	215                     # 0xd7
	.short	828                     # 0x33c
	.short	592                     # 0x250
	.short	281                     # 0x119
	.short	565                     # 0x235
	.short	555                     # 0x22b
	.short	710                     # 0x2c6
	.short	82                      # 0x52
	.short	896                     # 0x380
	.short	831                     # 0x33f
	.short	547                     # 0x223
	.short	261                     # 0x105
	.short	524                     # 0x20c
	.short	462                     # 0x1ce
	.short	293                     # 0x125
	.short	465                     # 0x1d1
	.short	502                     # 0x1f6
	.short	56                      # 0x38
	.short	661                     # 0x295
	.short	821                     # 0x335
	.short	976                     # 0x3d0
	.short	991                     # 0x3df
	.short	658                     # 0x292
	.short	869                     # 0x365
	.short	905                     # 0x389
	.short	758                     # 0x2f6
	.short	745                     # 0x2e9
	.short	193                     # 0xc1
	.short	768                     # 0x300
	.short	550                     # 0x226
	.short	608                     # 0x260
	.short	933                     # 0x3a5
	.short	378                     # 0x17a
	.short	286                     # 0x11e
	.short	215                     # 0xd7
	.short	979                     # 0x3d3
	.short	792                     # 0x318
	.short	961                     # 0x3c1
	.short	61                      # 0x3d
	.short	688                     # 0x2b0
	.short	793                     # 0x319
	.short	644                     # 0x284
	.short	986                     # 0x3da
	.short	403                     # 0x193
	.short	106                     # 0x6a
	.short	366                     # 0x16e
	.short	905                     # 0x389
	.short	644                     # 0x284
	.short	372                     # 0x174
	.short	567                     # 0x237
	.short	466                     # 0x1d2
	.short	434                     # 0x1b2
	.short	645                     # 0x285
	.short	210                     # 0xd2
	.short	389                     # 0x185
	.short	550                     # 0x226
	.short	919                     # 0x397
	.short	135                     # 0x87
	.short	780                     # 0x30c
	.short	773                     # 0x305
	.short	635                     # 0x27b
	.short	389                     # 0x185
	.short	707                     # 0x2c3
	.short	100                     # 0x64
	.short	626                     # 0x272
	.short	958                     # 0x3be
	.short	165                     # 0xa5
	.short	504                     # 0x1f8
	.short	920                     # 0x398
	.short	176                     # 0xb0
	.short	193                     # 0xc1
	.short	713                     # 0x2c9
	.short	857                     # 0x359
	.short	265                     # 0x109
	.short	203                     # 0xcb
	.short	50                      # 0x32
	.short	668                     # 0x29c
	.short	108                     # 0x6c
	.short	645                     # 0x285
	.short	990                     # 0x3de
	.short	626                     # 0x272
	.short	197                     # 0xc5
	.short	510                     # 0x1fe
	.short	357                     # 0x165
	.short	358                     # 0x166
	.short	850                     # 0x352
	.short	858                     # 0x35a
	.short	364                     # 0x16c
	.short	936                     # 0x3a8
	.short	638                     # 0x27e
	.size	_ZN9NCompress6NBZip2L9kRandNumsE, 1024


	.globl	_ZN9NCompress6NBZip28CDecoderC1Ev
	.type	_ZN9NCompress6NBZip28CDecoderC1Ev,@function
_ZN9NCompress6NBZip28CDecoderC1Ev = _ZN9NCompress6NBZip28CDecoderC2Ev
	.globl	_ZN9NCompress6NBZip28CDecoderD1Ev
	.type	_ZN9NCompress6NBZip28CDecoderD1Ev,@function
_ZN9NCompress6NBZip28CDecoderD1Ev = _ZN9NCompress6NBZip28CDecoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
