	.text
	.file	"init.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4596373779694328218     # double 0.20000000000000001
.LCPI0_1:
	.quad	4622945017495814144     # double 12
.LCPI0_2:
	.quad	4611235658464650854     # double 1.8999999999999999
.LCPI0_3:
	.quad	4562254508917369340     # double 0.001
.LCPI0_4:
	.quad	4611686018427387904     # double 2
.LCPI0_5:
	.quad	4615063718147915776     # double 3.5
.LCPI0_6:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
.LCPI0_7:
	.quad	4576918229304087675     # double 0.01
	.text
	.globl	init
	.p2align	4, 0x90
	.type	init,@function
init:                                   # @init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movq	64(%rsp), %r10
	movq	56(%rsp), %r11
	addq	$8, %r9
	addq	$8, %r11
	addq	$8, %r10
	xorl	%r15d, %r15d
	movsd	.LCPI0_0(%rip), %xmm8   # xmm8 = mem[0],zero
	movsd	.LCPI0_1(%rip), %xmm9   # xmm9 = mem[0],zero
	movsd	.LCPI0_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI0_3(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI0_4(%rip), %xmm4   # xmm4 = mem[0],zero
	movabsq	$4629137466983448576, %r14 # imm = 0x403E000000000000
	movsd	.LCPI0_5(%rip), %xmm5   # xmm5 = mem[0],zero
	movsd	.LCPI0_6(%rip), %xmm6   # xmm6 = mem[0],zero
	movsd	.LCPI0_7(%rip), %xmm7   # xmm7 = mem[0],zero
	.p2align	4, 0x90
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_2 Depth 2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r15d, %xmm0
	movapd	%xmm0, %xmm1
	mulsd	%xmm8, %xmm1
	divsd	%xmm9, %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, (%rdi,%r15,8)
	mulsd	%xmm3, %xmm1
	movsd	%xmm1, (%rsi,%r15,8)
	movapd	%xmm0, %xmm1
	addsd	%xmm4, %xmm1
	movsd	%xmm1, (%rdx,%r15,8)
	movq	%r14, (%rcx,%r15,8)
	movapd	%xmm0, %xmm1
	addsd	%xmm5, %xmm1
	movsd	%xmm1, (%r8,%r15,8)
	movq	%r10, %r12
	movq	%r11, %r13
	movq	%r9, %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_2:                                #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm6, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm7, %xmm1
	movsd	%xmm1, -8(%rbx)
	leal	(%r15,%rax), %ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	movsd	%xmm1, -8(%r13)
	movq	$0, -8(%r12)
	leal	1(%rax), %ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	mulsd	%xmm6, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm7, %xmm1
	movsd	%xmm1, (%rbx)
	leal	1(%r15,%rax), %ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	movsd	%xmm1, (%r13)
	movq	$0, (%r12)
	addq	$2, %rax
	addq	$16, %rbx
	addq	$16, %r13
	addq	$16, %r12
	cmpq	$12, %rax
	jne	.LBB0_2
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	incq	%r15
	addq	$96, %r9
	addq	$96, %r11
	addq	$96, %r10
	cmpq	$12, %r15
	jne	.LBB0_1
# BB#4:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	init, .Lfunc_end0-init
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
