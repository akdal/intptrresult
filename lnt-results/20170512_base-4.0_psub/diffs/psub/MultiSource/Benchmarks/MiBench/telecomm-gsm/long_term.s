	.text
	.file	"long_term.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
.LCPI0_1:
	.short	32767                   # 0x7fff
	.short	32767                   # 0x7fff
	.short	32767                   # 0x7fff
	.short	32767                   # 0x7fff
	.short	32767                   # 0x7fff
	.short	32767                   # 0x7fff
	.short	32767                   # 0x7fff
	.short	32767                   # 0x7fff
	.text
	.globl	Gsm_Long_Term_Predictor
	.p2align	4, 0x90
	.type	Gsm_Long_Term_Predictor,@function
Gsm_Long_Term_Predictor:                # @Gsm_Long_Term_Predictor
	.cfi_startproc
# BB#0:                                 # %vector.body
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$408, %rsp              # imm = 0x198
.Lcfi6:
	.cfi_def_cfa_offset 464
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	pxor	%xmm8, %xmm8
	movdqu	(%rbx), %xmm1
	movdqu	16(%rbx), %xmm6
	movdqu	32(%rbx), %xmm4
	movdqu	48(%rbx), %xmm9
	pxor	%xmm5, %xmm5
	pcmpgtw	%xmm1, %xmm5
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	movdqa	%xmm1, %xmm7
	pcmpeqw	%xmm0, %xmm7
	pxor	%xmm3, %xmm3
	psubw	%xmm1, %xmm3
	movdqa	.LCPI0_1(%rip), %xmm10  # xmm10 = [32767,32767,32767,32767,32767,32767,32767,32767]
	movdqa	%xmm10, %xmm2
	pand	%xmm7, %xmm2
	pandn	%xmm3, %xmm7
	por	%xmm2, %xmm7
	pand	%xmm5, %xmm7
	pandn	%xmm1, %xmm5
	por	%xmm7, %xmm5
	pmaxsw	%xmm8, %xmm5
	pxor	%xmm7, %xmm7
	pcmpgtw	%xmm6, %xmm7
	movdqa	%xmm6, %xmm1
	pcmpeqw	%xmm0, %xmm1
	pxor	%xmm2, %xmm2
	psubw	%xmm6, %xmm2
	movdqa	%xmm10, %xmm3
	pand	%xmm1, %xmm3
	pandn	%xmm2, %xmm1
	por	%xmm3, %xmm1
	pand	%xmm7, %xmm1
	pandn	%xmm6, %xmm7
	por	%xmm1, %xmm7
	pmaxsw	%xmm5, %xmm7
	pxor	%xmm5, %xmm5
	pcmpgtw	%xmm4, %xmm5
	movdqa	%xmm4, %xmm1
	pcmpeqw	%xmm0, %xmm1
	pxor	%xmm2, %xmm2
	psubw	%xmm4, %xmm2
	movdqa	%xmm10, %xmm3
	pand	%xmm1, %xmm3
	pandn	%xmm2, %xmm1
	por	%xmm3, %xmm1
	pand	%xmm5, %xmm1
	pandn	%xmm4, %xmm5
	por	%xmm1, %xmm5
	pmaxsw	%xmm7, %xmm5
	pxor	%xmm4, %xmm4
	pcmpgtw	%xmm9, %xmm4
	movdqa	%xmm9, %xmm1
	pcmpeqw	%xmm0, %xmm1
	pxor	%xmm2, %xmm2
	psubw	%xmm9, %xmm2
	movdqa	%xmm10, %xmm3
	pand	%xmm1, %xmm3
	pandn	%xmm2, %xmm1
	por	%xmm3, %xmm1
	pand	%xmm4, %xmm1
	pandn	%xmm9, %xmm4
	por	%xmm1, %xmm4
	pmaxsw	%xmm5, %xmm4
	movdqu	64(%rbx), %xmm1
	pxor	%xmm2, %xmm2
	pcmpgtw	%xmm1, %xmm2
	pcmpeqw	%xmm1, %xmm0
	psubw	%xmm1, %xmm8
	pand	%xmm0, %xmm10
	pandn	%xmm8, %xmm0
	por	%xmm10, %xmm0
	pand	%xmm2, %xmm0
	pandn	%xmm1, %xmm2
	por	%xmm0, %xmm2
	pmaxsw	%xmm4, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	pmaxsw	%xmm2, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	pmaxsw	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	movd	%xmm1, %edx
	pextrw	$1, %xmm1, %eax
	psrld	$16, %xmm1
	pcmpgtw	%xmm1, %xmm0
	movdqa	%xmm0, 384(%rsp)
	testb	$1, 384(%rsp)
	cmovnew	%dx, %ax
	testw	%ax, %ax
	movq	%r8, 96(%rsp)           # 8-byte Spill
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	%r9, 112(%rsp)          # 8-byte Spill
	je	.LBB0_1
# BB#2:
	movswq	%ax, %rdi
	shlq	$16, %rdi
	callq	gsm_norm
	cwtl
	xorl	%ecx, %ecx
	cmpl	$6, %eax
	jle	.LBB0_3
	jmp	.LBB0_4
.LBB0_1:
	xorl	%eax, %eax
.LBB0_3:                                # %.thread.i
	movl	$6, %ecx
	subl	%eax, %ecx
	movswl	%cx, %ecx
.LBB0_4:                                # %vector.body34
	movq	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	psrad	$16, %xmm1
	movq	(%rbx), %xmm0           # xmm0 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psrad	$16, %xmm2
	movl	%ecx, 108(%rsp)         # 4-byte Spill
	movd	%ecx, %xmm0
	psrad	%xmm0, %xmm2
	psrad	%xmm0, %xmm1
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	packssdw	%xmm1, %xmm2
	movdqa	%xmm2, (%rsp)
	movq	24(%rbx), %xmm1         # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	16(%rbx), %xmm2         # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	psrad	%xmm0, %xmm2
	psrad	%xmm0, %xmm1
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	packssdw	%xmm1, %xmm2
	movdqa	%xmm2, 16(%rsp)
	movq	40(%rbx), %xmm1         # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	32(%rbx), %xmm2         # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	psrad	%xmm0, %xmm2
	psrad	%xmm0, %xmm1
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	packssdw	%xmm1, %xmm2
	movdqa	%xmm2, 32(%rsp)
	movq	56(%rbx), %xmm1         # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	48(%rbx), %xmm2         # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	psrad	%xmm0, %xmm2
	psrad	%xmm0, %xmm1
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	packssdw	%xmm1, %xmm2
	movdqa	%xmm2, 48(%rsp)
	movq	72(%rbx), %xmm1         # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movq	64(%rbx), %xmm2         # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	psrad	%xmm0, %xmm2
	psrad	%xmm0, %xmm1
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	packssdw	%xmm1, %xmm2
	movdqa	%xmm2, 64(%rsp)
	movswq	(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 376(%rsp)         # 8-byte Spill
	movswq	2(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 368(%rsp)         # 8-byte Spill
	movswq	4(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	movswq	6(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 352(%rsp)         # 8-byte Spill
	movswq	8(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	movswq	10(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	movswq	12(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	movswq	14(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movswq	16(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movswq	18(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	movswq	20(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movswq	22(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movswq	24(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movswq	26(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movswq	28(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movswq	30(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movswq	32(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movswq	34(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movswq	36(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movswq	38(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movswq	40(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movswq	42(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movswq	44(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movswq	46(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movswq	48(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movswq	50(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movswq	52(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movswq	54(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movswq	56(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movswq	58(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movswq	60(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movswq	62(%rsp), %rax
	shlq	$32, %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movswq	64(%rsp), %r11
	shlq	$32, %r11
	movswq	66(%rsp), %r9
	shlq	$32, %r9
	movswq	68(%rsp), %r10
	shlq	$32, %r10
	movswq	70(%rsp), %rax
	shlq	$32, %rax
	movswq	72(%rsp), %rbx
	shlq	$32, %rbx
	movswq	74(%rsp), %rcx
	shlq	$32, %rcx
	movswq	76(%rsp), %rdx
	shlq	$32, %rdx
	movswq	78(%rsp), %r8
	shlq	$32, %r8
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	leaq	-2(%rbp), %r14
	movw	$40, %si
	movq	$-81, %r15
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	movq	%r12, %r13
	movswq	-78(%r14), %rdi
	imulq	376(%rsp), %rdi         # 8-byte Folded Reload
	movswq	-76(%r14), %rbp
	imulq	368(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rdi
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-74(%r14), %rdi
	imulq	360(%rsp), %rdi         # 8-byte Folded Reload
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-72(%r14), %rbp
	imulq	352(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-70(%r14), %rdi
	imulq	344(%rsp), %rdi         # 8-byte Folded Reload
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-68(%r14), %rbp
	imulq	336(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-66(%r14), %rdi
	imulq	328(%rsp), %rdi         # 8-byte Folded Reload
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-64(%r14), %rbp
	imulq	320(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-62(%r14), %rdi
	imulq	312(%rsp), %rdi         # 8-byte Folded Reload
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-60(%r14), %rbp
	imulq	304(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-58(%r14), %rdi
	imulq	296(%rsp), %rdi         # 8-byte Folded Reload
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-56(%r14), %rbp
	imulq	288(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-54(%r14), %rdi
	imulq	280(%rsp), %rdi         # 8-byte Folded Reload
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-52(%r14), %rbp
	imulq	272(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-50(%r14), %rdi
	imulq	264(%rsp), %rdi         # 8-byte Folded Reload
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-48(%r14), %rbp
	imulq	256(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-46(%r14), %rdi
	imulq	248(%rsp), %rdi         # 8-byte Folded Reload
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-44(%r14), %rbp
	imulq	240(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-42(%r14), %rdi
	imulq	232(%rsp), %rdi         # 8-byte Folded Reload
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-40(%r14), %rbp
	imulq	224(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-38(%r14), %rdi
	imulq	216(%rsp), %rdi         # 8-byte Folded Reload
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-36(%r14), %rbp
	imulq	208(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-34(%r14), %rdi
	imulq	200(%rsp), %rdi         # 8-byte Folded Reload
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-32(%r14), %rbp
	imulq	192(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-30(%r14), %rdi
	imulq	184(%rsp), %rdi         # 8-byte Folded Reload
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-28(%r14), %rbp
	imulq	176(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-26(%r14), %rdi
	imulq	168(%rsp), %rdi         # 8-byte Folded Reload
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-24(%r14), %rbp
	imulq	160(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-22(%r14), %rdi
	imulq	152(%rsp), %rdi         # 8-byte Folded Reload
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-20(%r14), %rbp
	imulq	144(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-18(%r14), %rdi
	imulq	136(%rsp), %rdi         # 8-byte Folded Reload
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-16(%r14), %rbp
	imulq	128(%rsp), %rbp         # 8-byte Folded Reload
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-14(%r14), %rdi
	imulq	%r11, %rdi
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-12(%r14), %rbp
	imulq	%r9, %rbp
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-10(%r14), %rdi
	imulq	%r10, %rdi
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-8(%r14), %rbp
	imulq	%rax, %rbp
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-6(%r14), %rdi
	imulq	%rbx, %rdi
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	-4(%r14), %rbp
	imulq	%rcx, %rbp
	sarq	$32, %rbp
	addq	%rdi, %rbp
	movswq	-2(%r14), %rdi
	imulq	%rdx, %rdi
	sarq	$32, %rdi
	addq	%rbp, %rdi
	movswq	(%r14), %r12
	imulq	%r8, %r12
	sarq	$32, %r12
	addq	%rdi, %r12
	cmpq	%r13, %r12
	cmovlq	%r13, %r12
	leal	121(%r15), %edi
	cmovgw	%di, %si
	leaq	-2(%r14), %r14
	incq	%r15
	jne	.LBB0_5
# BB#6:
	movq	112(%rsp), %r14         # 8-byte Reload
	movw	%si, (%r14)
	movl	$6, %ecx
	subl	108(%rsp), %ecx         # 4-byte Folded Reload
	movswq	%si, %rdx
	addq	%rdx, %rdx
	movq	120(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rax
	subq	%rdx, %rax
	xorl	%ebp, %ebp
	movl	$1, %edx
	.p2align	4, 0x90
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	movswl	-2(%rax,%rdx,2), %esi
	sarl	$3, %esi
	movslq	%esi, %rsi
	imulq	%rsi, %rsi
	addq	%rbp, %rsi
	movswl	(%rax,%rdx,2), %edi
	sarl	$3, %edi
	movslq	%edi, %rbp
	imulq	%rbp, %rbp
	addq	%rsi, %rbp
	addq	$2, %rdx
	cmpq	$41, %rdx
	jne	.LBB0_7
# BB#8:
	addq	%r12, %r12
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarq	%cl, %r12
	testq	%r12, %r12
	jle	.LBB0_9
# BB#10:
	addq	%rbp, %rbp
	movw	$3, %ax
	cmpq	%rbp, %r12
	movq	464(%rsp), %r15
	jge	.LBB0_15
# BB#11:
	movq	%rbp, %rdi
	callq	gsm_norm
	movl	%eax, %ecx
	shlq	%cl, %r12
                                        # kill: %CL<def> %CL<kill> %CX<kill>
	shlq	%cl, %rbp
	shrq	$16, %rbp
	sarl	$16, %r12d
	movswl	%bp, %edi
	movswl	gsm_DLB(%rip), %esi
	callq	gsm_mult
	cwtl
	cmpl	%eax, %r12d
	jle	.LBB0_12
# BB#13:
	movswl	gsm_DLB+2(%rip), %esi
	movswl	%bp, %ebp
	movl	%ebp, %edi
	callq	gsm_mult
	movswl	%ax, %ecx
	movw	$1, %ax
	cmpl	%ecx, %r12d
	jle	.LBB0_15
# BB#14:
	movswl	gsm_DLB+4(%rip), %esi
	movl	%ebp, %edi
	callq	gsm_mult
	movswl	%ax, %ecx
	xorl	%eax, %eax
	cmpl	%ecx, %r12d
	setg	%al
	orl	$2, %eax
	jmp	.LBB0_15
.LBB0_9:
	xorl	%eax, %eax
	movq	464(%rsp), %r15
	jmp	.LBB0_15
.LBB0_12:
	xorl	%eax, %eax
.LBB0_15:                               # %Calculation_of_the_LTP_parameters.exit
	movw	%ax, (%r15)
	movswq	(%r14), %rdx
	andb	$3, %al
	cmpb	$1, %al
	je	.LBB0_22
# BB#16:                                # %Calculation_of_the_LTP_parameters.exit
	cmpb	$2, %al
	je	.LBB0_24
# BB#17:                                # %Calculation_of_the_LTP_parameters.exit
	cmpb	$3, %al
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	jne	.LBB0_20
# BB#18:                                # %.preheader70.i
	addq	%rdx, %rdx
	subq	%rdx, %rbx
	xorl	%eax, %eax
	movw	$32767, %r8w            # imm = 0x7FFF
	.p2align	4, 0x90
.LBB0_19:                               # =>This Inner Loop Header: Depth=1
	movswq	(%rbx,%rax,2), %rdx
	movq	%rdx, %rsi
	shlq	$15, %rsi
	subq	%rdx, %rsi
	addq	$16384, %rsi            # imm = 0x4000
	shrq	$15, %rsi
	movw	%si, (%rdi,%rax,2)
	movswq	(%rcx,%rax,2), %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32768, %rdx           # imm = 0x8000
	movw	$-32768, %si            # imm = 0x8000
	cmovgw	%dx, %si
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgw	%r8w, %si
	movw	%si, (%rbp,%rax,2)
	incq	%rax
	cmpq	$40, %rax
	jne	.LBB0_19
	jmp	.LBB0_26
.LBB0_22:                               # %.preheader66.i
	addq	%rdx, %rdx
	subq	%rdx, %rbx
	xorl	%eax, %eax
	movw	$32767, %r8w            # imm = 0x7FFF
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_23:                               # =>This Inner Loop Header: Depth=1
	movswq	(%rbx,%rax,2), %rdx
	imulq	$11469, %rdx, %rdx      # imm = 0x2CCD
	addq	$16384, %rdx            # imm = 0x4000
	shrq	$15, %rdx
	movw	%dx, (%rdi,%rax,2)
	movswq	(%rcx,%rax,2), %rsi
	movswq	%dx, %rdx
	subq	%rdx, %rsi
	cmpq	$-32768, %rsi           # imm = 0x8000
	movw	$-32768, %dx            # imm = 0x8000
	cmovgw	%si, %dx
	cmpq	$32766, %rsi            # imm = 0x7FFE
	cmovgw	%r8w, %dx
	movw	%dx, (%rbp,%rax,2)
	incq	%rax
	cmpq	$40, %rax
	jne	.LBB0_23
	jmp	.LBB0_26
.LBB0_24:                               # %.preheader68.i
	addq	%rdx, %rdx
	subq	%rdx, %rbx
	xorl	%eax, %eax
	movw	$32767, %r8w            # imm = 0x7FFF
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_25:                               # =>This Inner Loop Header: Depth=1
	movswq	(%rbx,%rax,2), %rdx
	imulq	$21299, %rdx, %rdx      # imm = 0x5333
	addq	$16384, %rdx            # imm = 0x4000
	shrq	$15, %rdx
	movw	%dx, (%rdi,%rax,2)
	movswq	(%rcx,%rax,2), %rsi
	movswq	%dx, %rdx
	subq	%rdx, %rsi
	cmpq	$-32768, %rsi           # imm = 0x8000
	movw	$-32768, %dx            # imm = 0x8000
	cmovgw	%si, %dx
	cmpq	$32766, %rsi            # imm = 0x7FFE
	cmovgw	%r8w, %dx
	movw	%dx, (%rbp,%rax,2)
	incq	%rax
	cmpq	$40, %rax
	jne	.LBB0_25
	jmp	.LBB0_26
.LBB0_20:                               # %.preheader.i10
	addq	%rdx, %rdx
	subq	%rdx, %rbx
	xorl	%eax, %eax
	movw	$32767, %r8w            # imm = 0x7FFF
	.p2align	4, 0x90
.LBB0_21:                               # =>This Inner Loop Header: Depth=1
	movswq	(%rbx,%rax,2), %rdx
	imulq	$3277, %rdx, %rdx       # imm = 0xCCD
	addq	$16384, %rdx            # imm = 0x4000
	shrq	$15, %rdx
	movw	%dx, (%rdi,%rax,2)
	movswq	(%rcx,%rax,2), %rsi
	movswq	%dx, %rdx
	subq	%rdx, %rsi
	cmpq	$-32768, %rsi           # imm = 0x8000
	movw	$-32768, %dx            # imm = 0x8000
	cmovgw	%si, %dx
	cmpq	$32766, %rsi            # imm = 0x7FFE
	cmovgw	%r8w, %dx
	movw	%dx, (%rbp,%rax,2)
	incq	%rax
	cmpq	$40, %rax
	jne	.LBB0_21
.LBB0_26:                               # %Long_term_analysis_filtering.exit
	addq	$408, %rsp              # imm = 0x198
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Gsm_Long_Term_Predictor, .Lfunc_end0-Gsm_Long_Term_Predictor
	.cfi_endproc

	.globl	Gsm_Long_Term_Synthesis_Filtering
	.p2align	4, 0x90
	.type	Gsm_Long_Term_Synthesis_Filtering,@function
Gsm_Long_Term_Synthesis_Filtering:      # @Gsm_Long_Term_Synthesis_Filtering
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	addl	$-40, %eax
	movzwl	%ax, %eax
	cmpl	$81, %eax
	jb	.LBB1_2
# BB#1:
	movw	630(%rdi), %si
.LBB1_2:                                # %._crit_edge
	movw	%si, 630(%rdi)
	movswq	%dx, %rax
	movswq	gsm_QLB(%rax,%rax), %r10
	shlq	$33, %r10
	movswq	%si, %rsi
	addq	%rsi, %rsi
	movq	%r8, %r11
	subq	%rsi, %r11
	xorl	%esi, %esi
	movabsq	$140737488355328, %r9   # imm = 0x800000000000
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movswq	(%r11,%rsi,2), %rdi
	movswq	(%rcx,%rsi,2), %rax
	imulq	%r10, %rdi
	addq	%r9, %rdi
	sarq	$48, %rdi
	leaq	(%rdi,%rax), %rdx
	leaq	32768(%rdi,%rax), %rax
	xorl	%edi, %edi
	testq	%rdx, %rdx
	setle	%dil
	addl	$32767, %edi            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %di
	movw	%di, (%r8,%rsi,2)
	incq	%rsi
	cmpq	$40, %rsi
	jne	.LBB1_3
# BB#4:                                 # %vector.body
	movups	-160(%r8), %xmm0
	movups	%xmm0, -240(%r8)
	movups	-144(%r8), %xmm0
	movups	%xmm0, -224(%r8)
	movups	-128(%r8), %xmm0
	movups	%xmm0, -208(%r8)
	movups	-112(%r8), %xmm0
	movups	%xmm0, -192(%r8)
	movups	-96(%r8), %xmm0
	movups	%xmm0, -176(%r8)
	movups	-80(%r8), %xmm0
	movups	%xmm0, -160(%r8)
	movups	-64(%r8), %xmm0
	movups	%xmm0, -144(%r8)
	movups	-48(%r8), %xmm0
	movups	%xmm0, -128(%r8)
	movups	-32(%r8), %xmm0
	movups	%xmm0, -112(%r8)
	movups	-16(%r8), %xmm0
	movups	%xmm0, -96(%r8)
	movups	(%r8), %xmm0
	movups	%xmm0, -80(%r8)
	movups	16(%r8), %xmm0
	movups	%xmm0, -64(%r8)
	movups	32(%r8), %xmm0
	movups	%xmm0, -48(%r8)
	movups	48(%r8), %xmm0
	movups	%xmm0, -32(%r8)
	movups	64(%r8), %xmm0
	movups	%xmm0, -16(%r8)
	retq
.Lfunc_end1:
	.size	Gsm_Long_Term_Synthesis_Filtering, .Lfunc_end1-Gsm_Long_Term_Synthesis_Filtering
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
