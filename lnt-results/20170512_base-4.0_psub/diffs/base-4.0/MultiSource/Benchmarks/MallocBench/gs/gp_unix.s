	.text
	.file	"gp_unix.bc"
	.globl	gp_init
	.p2align	4, 0x90
	.type	gp_init,@function
gp_init:                                # @gp_init
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	gp_init, .Lfunc_end0-gp_init
	.cfi_endproc

	.globl	gs_get_clock
	.p2align	4, 0x90
	.type	gs_get_clock,@function
gs_get_clock:                           # @gs_get_clock
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 64
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	leaq	8(%rsp), %rdi
	movq	%rsp, %rsi
	callq	gettimeofday
	cmpl	$-1, %eax
	je	.LBB1_2
# BB#1:
	movq	8(%rsp), %r15
	leaq	-315576000(%r15), %r12
	movslq	(%rsp), %rax
	imulq	$60, %rax, %rbx
	subq	%rbx, %r12
	negq	%rbx
	leaq	8(%rsp), %rdi
	callq	localtime
	cmpl	$0, 32(%rax)
	leaq	-315572400(%r15,%rbx), %rcx
	cmoveq	%r12, %rcx
	movabsq	$1749024623285053783, %rdx # imm = 0x1845C8A0CE512957
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$13, %rdx
	addq	%rax, %rdx
	movq	%rdx, (%r14)
	imulq	$86400, %rdx, %rax      # imm = 0x15180
	subq	%rax, %rcx
	imulq	$1000, %rcx, %rcx       # imm = 0x3E8
	movabsq	$2361183241434822607, %rax # imm = 0x20C49BA5E353F7CF
	imulq	16(%rsp)
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$7, %rdx
	addq	%rax, %rdx
	addq	%rcx, %rdx
	movq	%rdx, 8(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB1_2:
	movl	$.L.str, %edi
	callq	perror
	movl	$-1, %edi
	callq	exit
.Lfunc_end1:
	.size	gs_get_clock, .Lfunc_end1-gs_get_clock
	.cfi_endproc

	.globl	gp_file_name_is_absolute
	.p2align	4, 0x90
	.type	gp_file_name_is_absolute,@function
gp_file_name_is_absolute:               # @gp_file_name_is_absolute
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	je	.LBB2_1
# BB#2:
	cmpb	$47, (%rdi)
	sete	%al
	movzbl	%al, %eax
	retq
.LBB2_1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.Lfunc_end2:
	.size	gp_file_name_is_absolute, .Lfunc_end2-gp_file_name_is_absolute
	.cfi_endproc

	.globl	gp_file_name_concat_string
	.p2align	4, 0x90
	.type	gp_file_name_concat_string,@function
gp_file_name_concat_string:             # @gp_file_name_concat_string
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%esi, %esi
	je	.LBB3_3
# BB#1:
	decl	%esi
	cmpb	$47, (%rdi,%rsi)
	jne	.LBB3_3
# BB#2:
	movl	$.L.str.1, %eax
	retq
.LBB3_3:
	movl	$.L.str.2, %eax
	retq
.Lfunc_end3:
	.size	gp_file_name_concat_string, .Lfunc_end3-gp_file_name_concat_string
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Ghostscript: gettimeofday failed:"
	.size	.L.str, 34

	.type	gp_file_name_list_separator,@object # @gp_file_name_list_separator
	.data
	.globl	gp_file_name_list_separator
gp_file_name_list_separator:
	.byte	58                      # 0x3a
	.size	gp_file_name_list_separator, 1

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.zero	1
	.size	.L.str.1, 1

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"/"
	.size	.L.str.2, 2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
