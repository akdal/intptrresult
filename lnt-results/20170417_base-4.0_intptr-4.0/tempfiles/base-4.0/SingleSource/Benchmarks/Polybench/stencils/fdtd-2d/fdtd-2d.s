	.text
	.file	"fdtd-2d.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4652007308841189376     # double 1000
.LCPI6_2:
	.quad	4602678819172646912     # double 0.5
.LCPI6_3:
	.quad	-4620693217682128896    # double -0.5
.LCPI6_5:
	.quad	-4618891777831180698    # double -0.69999999999999996
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_1:
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
.LCPI6_4:
	.quad	4604480259023595110     # double 0.69999999999999996
	.quad	4604480259023595110     # double 0.69999999999999996
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 176
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_107
# BB#1:
	movq	(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_107
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_107
# BB#3:                                 # %polybench_alloc_data.exit
	movq	(%rsp), %r13
	testq	%r13, %r13
	je	.LBB6_107
# BB#4:                                 # %polybench_alloc_data.exit59
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_107
# BB#5:                                 # %polybench_alloc_data.exit59
	movq	(%rsp), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_107
# BB#6:                                 # %polybench_alloc_data.exit61
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_107
# BB#7:                                 # %polybench_alloc_data.exit61
	movq	(%rsp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_107
# BB#8:                                 # %polybench_alloc_data.exit63
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_107
# BB#9:                                 # %polybench_alloc_data.exit63
	movq	(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_107
# BB#10:                                # %polybench_alloc_data.exit65
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_107
# BB#11:                                # %polybench_alloc_data.exit65
	movq	(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_107
# BB#12:                                # %polybench_alloc_data.exit67
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$400, %edx              # imm = 0x190
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_107
# BB#13:                                # %polybench_alloc_data.exit67
	movq	(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB6_107
# BB#14:                                # %polybench_alloc_data.exit69
	xorl	%eax, %eax
	movq	40(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_15:                               # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%rbp,%rax,8)
	leal	1(%rax), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movsd	%xmm0, 8(%rbp,%rax,8)
	leal	2(%rax), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movsd	%xmm0, 16(%rbp,%rax,8)
	leal	3(%rax), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movsd	%xmm0, 24(%rbp,%rax,8)
	leal	4(%rax), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movsd	%xmm0, 32(%rbp,%rax,8)
	addq	$5, %rax
	cmpq	$50, %rax
	jne	.LBB6_15
# BB#16:                                # %.preheader.i.preheader
	xorl	%eax, %eax
	movsd	.LCPI6_0(%rip), %xmm9   # xmm9 = mem[0],zero
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_17:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_18 Depth 2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movq	$-1000, %rdx            # imm = 0xFC18
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_18:                               #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1001(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	divsd	%xmm9, %xmm1
	movsd	%xmm1, (%r14,%rsi)
	leal	1002(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	divsd	%xmm9, %xmm1
	movsd	%xmm1, (%r13,%rsi)
	leal	1003(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	divsd	%xmm9, %xmm1
	movsd	%xmm1, (%r11,%rsi)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB6_18
# BB#19:                                #   in Loop: Header=BB6_17 Depth=1
	incq	%rcx
	addq	$8000, %rax             # imm = 0x1F40
	cmpq	$1000, %rcx             # imm = 0x3E8
	jne	.LBB6_17
# BB#20:                                # %.preheader6.i.preheader
	leaq	8000(%r13), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	8016(%r13), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8016(%r11), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	8008(%r13), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	8008(%r11), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	24(%r14), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	24(%r11), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	16(%r14), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	16(%r11), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movsd	.LCPI6_2(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI6_3(%rip), %xmm8   # xmm8 = mem[0],zero
	movsd	.LCPI6_5(%rip), %xmm10  # xmm10 = mem[0],zero
	movapd	.LCPI6_4(%rip), %xmm11  # xmm11 = [7.000000e-01,7.000000e-01]
	movapd	.LCPI6_1(%rip), %xmm4   # xmm4 = [5.000000e-01,5.000000e-01]
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movq	%r14, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_21:                               # %.preheader6.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_24 Depth 2
                                        #     Child Loop BB6_26 Depth 2
                                        #     Child Loop BB6_28 Depth 2
                                        #       Child Loop BB6_31 Depth 3
                                        #       Child Loop BB6_33 Depth 3
                                        #     Child Loop BB6_36 Depth 2
                                        #       Child Loop BB6_40 Depth 3
                                        #       Child Loop BB6_43 Depth 3
                                        #     Child Loop BB6_46 Depth 2
                                        #       Child Loop BB6_50 Depth 3
                                        #       Child Loop BB6_53 Depth 3
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	(%rbp,%rax,8), %rax
	cmpq	%rax, %r13
	jae	.LBB6_23
# BB#22:                                # %.preheader6.i
                                        #   in Loop: Header=BB6_21 Depth=1
	cmpq	56(%rsp), %rax          # 8-byte Folded Reload
	jae	.LBB6_23
# BB#25:                                # %scalar.ph195.preheader
                                        #   in Loop: Header=BB6_21 Depth=1
	movl	$7, %ecx
	.p2align	4, 0x90
.LBB6_26:                               # %scalar.ph195
                                        #   Parent Loop BB6_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movq	%rdx, -56(%r13,%rcx,8)
	movq	(%rax), %rdx
	movq	%rdx, -48(%r13,%rcx,8)
	movq	(%rax), %rdx
	movq	%rdx, -40(%r13,%rcx,8)
	movq	(%rax), %rdx
	movq	%rdx, -32(%r13,%rcx,8)
	movq	(%rax), %rdx
	movq	%rdx, -24(%r13,%rcx,8)
	movq	(%rax), %rdx
	movq	%rdx, -16(%r13,%rcx,8)
	movq	(%rax), %rdx
	movq	%rdx, -8(%r13,%rcx,8)
	movq	(%rax), %rdx
	movq	%rdx, (%r13,%rcx,8)
	addq	$8, %rcx
	cmpq	$1007, %rcx             # imm = 0x3EF
	jne	.LBB6_26
	jmp	.LBB6_27
	.p2align	4, 0x90
.LBB6_23:                               # %vector.body193.preheader
                                        #   in Loop: Header=BB6_21 Depth=1
	movq	(%rax), %xmm6           # xmm6 = mem[0],zero
	pshufd	$68, %xmm6, %xmm6       # xmm6 = xmm6[0,1,0,1]
	movl	$6, %eax
	.p2align	4, 0x90
.LBB6_24:                               # %vector.body193
                                        #   Parent Loop BB6_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm6, -48(%r13,%rax,8)
	movdqu	%xmm6, -32(%r13,%rax,8)
	movdqu	%xmm6, -16(%r13,%rax,8)
	movdqu	%xmm6, (%r13,%rax,8)
	addq	$8, %rax
	cmpq	$1006, %rax             # imm = 0x3EE
	jne	.LBB6_24
.LBB6_27:                               # %.preheader2.i.preheader
                                        #   in Loop: Header=BB6_21 Depth=1
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	xorl	%edi, %edi
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB6_28:                               # %.preheader2.i
                                        #   Parent Loop BB6_21 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_31 Depth 3
                                        #       Child Loop BB6_33 Depth 3
	imulq	$8000, %rdi, %rdx       # imm = 0x1F40
	leaq	8000(%r13,%rdx), %rsi
	leaq	16000(%r11,%rdx), %rbx
	cmpq	%rbx, %rsi
	jae	.LBB6_30
# BB#29:                                # %.preheader2.i
                                        #   in Loop: Header=BB6_28 Depth=2
	leaq	16000(%r13,%rdx), %rsi
	addq	%r11, %rdx
	cmpq	%rsi, %rdx
	jae	.LBB6_30
# BB#32:                                # %scalar.ph166.preheader
                                        #   in Loop: Header=BB6_28 Depth=2
	movl	$1000, %edx             # imm = 0x3E8
	movq	%r8, %rsi
	movq	%r9, %rbx
	.p2align	4, 0x90
.LBB6_33:                               # %scalar.ph166
                                        #   Parent Loop BB6_21 Depth=1
                                        #     Parent Loop BB6_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	-8(%rsi), %xmm2         # xmm2 = mem[0],zero
	subsd	-8008(%rsi), %xmm2
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	movsd	%xmm1, -8(%rbx)
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	movsd	(%rsi), %xmm2           # xmm2 = mem[0],zero
	subsd	-8000(%rsi), %xmm2
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	movsd	%xmm1, (%rbx)
	addq	$16, %rbx
	addq	$16, %rsi
	addq	$-2, %rdx
	jne	.LBB6_33
	jmp	.LBB6_34
	.p2align	4, 0x90
.LBB6_30:                               # %vector.body164.preheader
                                        #   in Loop: Header=BB6_28 Depth=2
	movl	$1000, %ebx             # imm = 0x3E8
	movq	%rax, %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB6_31:                               # %vector.body164
                                        #   Parent Loop BB6_21 Depth=1
                                        #     Parent Loop BB6_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-16(%rsi), %xmm6
	movupd	(%rsi), %xmm7
	movupd	-16(%rdx), %xmm1
	movupd	(%rdx), %xmm5
	movupd	-8016(%rdx), %xmm2
	movupd	-8000(%rdx), %xmm3
	subpd	%xmm2, %xmm1
	subpd	%xmm3, %xmm5
	mulpd	%xmm4, %xmm1
	mulpd	%xmm4, %xmm5
	subpd	%xmm1, %xmm6
	subpd	%xmm5, %xmm7
	movupd	%xmm6, -16(%rsi)
	movupd	%xmm7, (%rsi)
	addq	$32, %rsi
	addq	$32, %rdx
	addq	$-4, %rbx
	jne	.LBB6_31
.LBB6_34:                               # %middle.block165
                                        #   in Loop: Header=BB6_28 Depth=2
	incq	%rbp
	incq	%rdi
	addq	$8000, %rcx             # imm = 0x1F40
	addq	$8000, %rax             # imm = 0x1F40
	addq	$8000, %r9              # imm = 0x1F40
	addq	$8000, %r8              # imm = 0x1F40
	cmpq	$1000, %rbp             # imm = 0x3E8
	jne	.LBB6_28
# BB#35:                                # %.preheader1.i.preheader
                                        #   in Loop: Header=BB6_21 Depth=1
	movq	24(%rsp), %r10          # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_36:                               # %.preheader1.i
                                        #   Parent Loop BB6_21 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_40 Depth 3
                                        #       Child Loop BB6_43 Depth 3
	imulq	$8000, %rdx, %rbp       # imm = 0x1F40
	leaq	8(%r14,%rbp), %rax
	leaq	(%r11,%rbp), %rbx
	leaq	8000(%r11,%rbp), %rsi
	cmpq	%rsi, %rax
	jae	.LBB6_39
# BB#37:                                # %.preheader1.i
                                        #   in Loop: Header=BB6_36 Depth=2
	leaq	8000(%r14,%rbp), %rax
	cmpq	%rax, %rbx
	jae	.LBB6_39
# BB#38:                                #   in Loop: Header=BB6_36 Depth=2
	movl	$1, %eax
	jmp	.LBB6_42
	.p2align	4, 0x90
.LBB6_39:                               # %vector.body135.preheader
                                        #   in Loop: Header=BB6_36 Depth=2
	movl	$996, %eax              # imm = 0x3E4
	movq	%r8, %rsi
	movq	%r9, %rdi
	.p2align	4, 0x90
.LBB6_40:                               # %vector.body135
                                        #   Parent Loop BB6_21 Depth=1
                                        #     Parent Loop BB6_36 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-16(%rdi), %xmm1
	movupd	(%rdi), %xmm2
	movupd	-24(%rsi), %xmm3
	movupd	-16(%rsi), %xmm5
	movupd	-8(%rsi), %xmm6
	movupd	(%rsi), %xmm7
	subpd	%xmm3, %xmm5
	subpd	%xmm6, %xmm7
	mulpd	%xmm4, %xmm5
	mulpd	%xmm4, %xmm7
	subpd	%xmm5, %xmm1
	subpd	%xmm7, %xmm2
	movupd	%xmm1, -16(%rdi)
	movupd	%xmm2, (%rdi)
	addq	$32, %rdi
	addq	$32, %rsi
	addq	$-4, %rax
	jne	.LBB6_40
# BB#41:                                #   in Loop: Header=BB6_36 Depth=2
	movl	$997, %eax              # imm = 0x3E5
.LBB6_42:                               # %scalar.ph137.preheader.new
                                        #   in Loop: Header=BB6_36 Depth=2
	addq	%r14, %rbp
	movsd	(%rbx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	subsd	-8(%rbx,%rax,8), %xmm1
	mulsd	%xmm8, %xmm1
	addsd	(%rbp,%rax,8), %xmm1
	movsd	%xmm1, (%rbp,%rax,8)
	movl	$999, %ebp              # imm = 0x3E7
	subq	%rax, %rbp
	leaq	(%rcx,%rax,8), %rbx
	leaq	(%r10,%rax,8), %rax
	.p2align	4, 0x90
.LBB6_43:                               # %scalar.ph137
                                        #   Parent Loop BB6_21 Depth=1
                                        #     Parent Loop BB6_36 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	-8(%rax), %xmm2         # xmm2 = mem[0],zero
	subsd	-16(%rax), %xmm2
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	movsd	%xmm1, -8(%rbx)
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	subsd	-8(%rax), %xmm2
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	movsd	%xmm1, (%rbx)
	addq	$16, %rbx
	addq	$16, %rax
	addq	$-2, %rbp
	jne	.LBB6_43
# BB#44:                                # %.loopexit339
                                        #   in Loop: Header=BB6_36 Depth=2
	incq	%rdx
	addq	$8000, %r9              # imm = 0x1F40
	addq	$8000, %r8              # imm = 0x1F40
	addq	$8000, %rcx             # imm = 0x1F40
	addq	$8000, %r10             # imm = 0x1F40
	cmpq	$1000, %rdx             # imm = 0x3E8
	jne	.LBB6_36
# BB#45:                                # %.preheader.i73.preheader
                                        #   in Loop: Header=BB6_21 Depth=1
	xorl	%r8d, %r8d
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	24(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %r11           # 8-byte Reload
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB6_46:                               # %.preheader.i73
                                        #   Parent Loop BB6_21 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_50 Depth 3
                                        #       Child Loop BB6_53 Depth 3
	imulq	$8000, %r14, %rax       # imm = 0x1F40
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax), %rcx
	leaq	7992(%rdx,%rax), %rdx
	movq	48(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rax), %rsi
	leaq	8000(%rdi,%rax), %rdi
	leaq	(%r13,%rax), %rbp
	leaq	15992(%r13,%rax), %rax
	cmpq	%rdi, %rcx
	sbbb	%dil, %dil
	cmpq	%rdx, %rsi
	sbbb	%bl, %bl
	andb	%dil, %bl
	cmpq	%rax, %rcx
	sbbb	%al, %al
	cmpq	%rdx, %rbp
	sbbb	%cl, %cl
	testb	$1, %bl
	jne	.LBB6_47
# BB#48:                                # %.preheader.i73
                                        #   in Loop: Header=BB6_46 Depth=2
	andb	%cl, %al
	andb	$1, %al
	movl	$0, %esi
	jne	.LBB6_52
# BB#49:                                # %vector.body.preheader
                                        #   in Loop: Header=BB6_46 Depth=2
	movl	$996, %eax              # imm = 0x3E4
	movq	%r9, %rcx
	movq	%r10, %rdx
	movq	%r11, %rsi
	.p2align	4, 0x90
.LBB6_50:                               # %vector.body
                                        #   Parent Loop BB6_21 Depth=1
                                        #     Parent Loop BB6_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-16(%rdx), %xmm1
	movupd	(%rdx), %xmm2
	movupd	-24(%rsi), %xmm3
	movupd	-16(%rsi), %xmm5
	movupd	-8(%rsi), %xmm6
	movupd	(%rsi), %xmm7
	subpd	%xmm3, %xmm5
	subpd	%xmm6, %xmm7
	movupd	-16(%rcx), %xmm3
	movupd	(%rcx), %xmm6
	addpd	%xmm5, %xmm3
	addpd	%xmm7, %xmm6
	movupd	-8016(%rcx), %xmm5
	movupd	-8000(%rcx), %xmm7
	subpd	%xmm5, %xmm3
	subpd	%xmm7, %xmm6
	mulpd	%xmm11, %xmm3
	mulpd	%xmm11, %xmm6
	subpd	%xmm3, %xmm1
	subpd	%xmm6, %xmm2
	movupd	%xmm1, -16(%rdx)
	movupd	%xmm2, (%rdx)
	addq	$32, %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB6_50
# BB#51:                                #   in Loop: Header=BB6_46 Depth=2
	movl	$996, %esi              # imm = 0x3E4
	jmp	.LBB6_52
	.p2align	4, 0x90
.LBB6_47:                               #   in Loop: Header=BB6_46 Depth=2
	xorl	%esi, %esi
.LBB6_52:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB6_46 Depth=2
	incq	%r14
	leaq	(%r8,%rsi,8), %rax
	leaq	(%r13,%rax), %rcx
	movl	$999, %edx              # imm = 0x3E7
	subq	%rsi, %rdx
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax), %rsi
	addq	40(%rsp), %rax          # 8-byte Folded Reload
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_53:                               # %scalar.ph
                                        #   Parent Loop BB6_21 Depth=1
                                        #     Parent Loop BB6_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	8(%rsi,%rdi,8), %xmm1   # xmm1 = mem[0],zero
	subsd	(%rsi,%rdi,8), %xmm1
	addsd	8000(%rcx,%rdi,8), %xmm1
	subsd	(%rcx,%rdi,8), %xmm1
	mulsd	%xmm10, %xmm1
	addsd	(%rax,%rdi,8), %xmm1
	movsd	%xmm1, (%rax,%rdi,8)
	incq	%rdi
	cmpq	%rdi, %rdx
	jne	.LBB6_53
# BB#54:                                # %.loopexit338
                                        #   in Loop: Header=BB6_46 Depth=2
	addq	$8000, %r11             # imm = 0x1F40
	addq	$8000, %r10             # imm = 0x1F40
	addq	$8000, %r9              # imm = 0x1F40
	addq	$8000, %r8              # imm = 0x1F40
	cmpq	$999, %r14              # imm = 0x3E7
	jne	.LBB6_46
# BB#55:                                #   in Loop: Header=BB6_21 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	incq	%rax
	cmpq	$50, %rax
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	112(%rsp), %rbp         # 8-byte Reload
	jne	.LBB6_21
# BB#56:                                # %kernel_fdtd_2d.exit
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_57:                               # =>This Inner Loop Header: Depth=1
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movsd	%xmm1, (%rbp,%rax,8)
	leal	1(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movsd	%xmm1, 8(%rbp,%rax,8)
	leal	2(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movsd	%xmm1, 16(%rbp,%rax,8)
	leal	3(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movsd	%xmm1, 24(%rbp,%rax,8)
	leal	4(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movsd	%xmm1, 32(%rbp,%rax,8)
	addq	$5, %rax
	cmpq	$50, %rax
	jne	.LBB6_57
# BB#58:                                # %.preheader.i78.preheader
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movq	32(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_59:                               # %.preheader.i78
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_60 Depth 2
	xorps	%xmm6, %xmm6
	cvtsi2sdl	%ecx, %xmm6
	movq	$-1000, %rdx            # imm = 0xFC18
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_60:                               #   Parent Loop BB6_59 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1001(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm6, %xmm1
	divsd	%xmm9, %xmm1
	movsd	%xmm1, (%rbp,%rsi)
	leal	1002(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm6, %xmm1
	divsd	%xmm9, %xmm1
	movsd	%xmm1, (%r15,%rsi)
	leal	1003(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm6, %xmm1
	divsd	%xmm9, %xmm1
	movsd	%xmm1, (%r12,%rsi)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB6_60
# BB#61:                                #   in Loop: Header=BB6_59 Depth=1
	incq	%rcx
	addq	$8000, %rax             # imm = 0x1F40
	cmpq	$1000, %rcx             # imm = 0x3E8
	jne	.LBB6_59
# BB#62:                                # %.preheader6.i86.preheader
	leaq	8000(%r15), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	8016(%r15), %r14
	leaq	8016(%r12), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	8008(%r15), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	8008(%r12), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	24(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	24(%r12), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	16(%rbp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	16(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%r14, 104(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB6_63:                               # %.preheader6.i86
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_66 Depth 2
                                        #     Child Loop BB6_68 Depth 2
                                        #     Child Loop BB6_70 Depth 2
                                        #       Child Loop BB6_73 Depth 3
                                        #       Child Loop BB6_75 Depth 3
                                        #     Child Loop BB6_78 Depth 2
                                        #       Child Loop BB6_82 Depth 3
                                        #       Child Loop BB6_85 Depth 3
                                        #     Child Loop BB6_88 Depth 2
                                        #       Child Loop BB6_92 Depth 3
                                        #       Child Loop BB6_95 Depth 3
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	(%rax,%rcx,8), %rax
	cmpq	%rax, %r15
	movq	32(%rsp), %r11          # 8-byte Reload
	jae	.LBB6_65
# BB#64:                                # %.preheader6.i86
                                        #   in Loop: Header=BB6_63 Depth=1
	cmpq	56(%rsp), %rax          # 8-byte Folded Reload
	jae	.LBB6_65
# BB#67:                                # %scalar.ph316.preheader
                                        #   in Loop: Header=BB6_63 Depth=1
	movl	$7, %ecx
	.p2align	4, 0x90
.LBB6_68:                               # %scalar.ph316
                                        #   Parent Loop BB6_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movq	%rdx, -56(%r15,%rcx,8)
	movq	(%rax), %rdx
	movq	%rdx, -48(%r15,%rcx,8)
	movq	(%rax), %rdx
	movq	%rdx, -40(%r15,%rcx,8)
	movq	(%rax), %rdx
	movq	%rdx, -32(%r15,%rcx,8)
	movq	(%rax), %rdx
	movq	%rdx, -24(%r15,%rcx,8)
	movq	(%rax), %rdx
	movq	%rdx, -16(%r15,%rcx,8)
	movq	(%rax), %rdx
	movq	%rdx, -8(%r15,%rcx,8)
	movq	(%rax), %rdx
	movq	%rdx, (%r15,%rcx,8)
	addq	$8, %rcx
	cmpq	$1007, %rcx             # imm = 0x3EF
	jne	.LBB6_68
	jmp	.LBB6_69
	.p2align	4, 0x90
.LBB6_65:                               # %vector.body314.preheader
                                        #   in Loop: Header=BB6_63 Depth=1
	movq	(%rax), %xmm1           # xmm1 = mem[0],zero
	pshufd	$68, %xmm1, %xmm5       # xmm5 = xmm1[0,1,0,1]
	movl	$6, %eax
	.p2align	4, 0x90
.LBB6_66:                               # %vector.body314
                                        #   Parent Loop BB6_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm5, -48(%r15,%rax,8)
	movdqu	%xmm5, -32(%r15,%rax,8)
	movdqu	%xmm5, -16(%r15,%rax,8)
	movdqu	%xmm5, (%r15,%rax,8)
	addq	$8, %rax
	cmpq	$1006, %rax             # imm = 0x3EE
	jne	.LBB6_66
.LBB6_69:                               # %.preheader2.i91.preheader
                                        #   in Loop: Header=BB6_63 Depth=1
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%r14, %rcx
	xorl	%edi, %edi
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB6_70:                               # %.preheader2.i91
                                        #   Parent Loop BB6_63 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_73 Depth 3
                                        #       Child Loop BB6_75 Depth 3
	imulq	$8000, %rdi, %rdx       # imm = 0x1F40
	leaq	8000(%r15,%rdx), %rsi
	leaq	16000(%r12,%rdx), %rbx
	cmpq	%rbx, %rsi
	jae	.LBB6_72
# BB#71:                                # %.preheader2.i91
                                        #   in Loop: Header=BB6_70 Depth=2
	leaq	16000(%r15,%rdx), %rsi
	addq	%r12, %rdx
	cmpq	%rsi, %rdx
	jae	.LBB6_72
# BB#74:                                # %scalar.ph285.preheader
                                        #   in Loop: Header=BB6_70 Depth=2
	movl	$1000, %edx             # imm = 0x3E8
	movq	%r8, %rsi
	movq	%r9, %rbx
	.p2align	4, 0x90
.LBB6_75:                               # %scalar.ph285
                                        #   Parent Loop BB6_63 Depth=1
                                        #     Parent Loop BB6_70 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	-8(%rsi), %xmm2         # xmm2 = mem[0],zero
	subsd	-8008(%rsi), %xmm2
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	movsd	%xmm1, -8(%rbx)
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	movsd	(%rsi), %xmm2           # xmm2 = mem[0],zero
	subsd	-8000(%rsi), %xmm2
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	movsd	%xmm1, (%rbx)
	addq	$16, %rbx
	addq	$16, %rsi
	addq	$-2, %rdx
	jne	.LBB6_75
	jmp	.LBB6_76
	.p2align	4, 0x90
.LBB6_72:                               # %vector.body283.preheader
                                        #   in Loop: Header=BB6_70 Depth=2
	movl	$1000, %ebx             # imm = 0x3E8
	movq	%rax, %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB6_73:                               # %vector.body283
                                        #   Parent Loop BB6_63 Depth=1
                                        #     Parent Loop BB6_70 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-16(%rsi), %xmm1
	movupd	(%rsi), %xmm2
	movupd	-16(%rdx), %xmm3
	movupd	(%rdx), %xmm5
	movupd	-8016(%rdx), %xmm6
	movupd	-8000(%rdx), %xmm7
	subpd	%xmm6, %xmm3
	subpd	%xmm7, %xmm5
	mulpd	%xmm4, %xmm3
	mulpd	%xmm4, %xmm5
	subpd	%xmm3, %xmm1
	subpd	%xmm5, %xmm2
	movupd	%xmm1, -16(%rsi)
	movupd	%xmm2, (%rsi)
	addq	$32, %rsi
	addq	$32, %rdx
	addq	$-4, %rbx
	jne	.LBB6_73
.LBB6_76:                               # %middle.block284
                                        #   in Loop: Header=BB6_70 Depth=2
	incq	%rbp
	incq	%rdi
	addq	$8000, %rcx             # imm = 0x1F40
	addq	$8000, %rax             # imm = 0x1F40
	addq	$8000, %r9              # imm = 0x1F40
	addq	$8000, %r8              # imm = 0x1F40
	cmpq	$1000, %rbp             # imm = 0x3E8
	jne	.LBB6_70
# BB#77:                                # %.preheader1.i98.preheader
                                        #   in Loop: Header=BB6_63 Depth=1
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_78:                               # %.preheader1.i98
                                        #   Parent Loop BB6_63 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_82 Depth 3
                                        #       Child Loop BB6_85 Depth 3
	imulq	$8000, %rdx, %rbp       # imm = 0x1F40
	leaq	8(%r11,%rbp), %rax
	leaq	(%r12,%rbp), %rbx
	leaq	8000(%r12,%rbp), %rsi
	cmpq	%rsi, %rax
	jae	.LBB6_81
# BB#79:                                # %.preheader1.i98
                                        #   in Loop: Header=BB6_78 Depth=2
	leaq	8000(%r11,%rbp), %rax
	cmpq	%rax, %rbx
	jae	.LBB6_81
# BB#80:                                #   in Loop: Header=BB6_78 Depth=2
	movl	$1, %eax
	jmp	.LBB6_84
	.p2align	4, 0x90
.LBB6_81:                               # %vector.body253.preheader
                                        #   in Loop: Header=BB6_78 Depth=2
	movl	$996, %eax              # imm = 0x3E4
	movq	%r8, %rsi
	movq	%r9, %rdi
	.p2align	4, 0x90
.LBB6_82:                               # %vector.body253
                                        #   Parent Loop BB6_63 Depth=1
                                        #     Parent Loop BB6_78 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-16(%rdi), %xmm1
	movupd	(%rdi), %xmm2
	movupd	-24(%rsi), %xmm3
	movupd	-16(%rsi), %xmm5
	movupd	-8(%rsi), %xmm6
	movupd	(%rsi), %xmm7
	subpd	%xmm3, %xmm5
	subpd	%xmm6, %xmm7
	mulpd	%xmm4, %xmm5
	mulpd	%xmm4, %xmm7
	subpd	%xmm5, %xmm1
	subpd	%xmm7, %xmm2
	movupd	%xmm1, -16(%rdi)
	movupd	%xmm2, (%rdi)
	addq	$32, %rdi
	addq	$32, %rsi
	addq	$-4, %rax
	jne	.LBB6_82
# BB#83:                                #   in Loop: Header=BB6_78 Depth=2
	movl	$997, %eax              # imm = 0x3E5
.LBB6_84:                               # %scalar.ph255.preheader.new
                                        #   in Loop: Header=BB6_78 Depth=2
	addq	%r11, %rbp
	movsd	(%rbx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	subsd	-8(%rbx,%rax,8), %xmm1
	mulsd	%xmm8, %xmm1
	addsd	(%rbp,%rax,8), %xmm1
	movsd	%xmm1, (%rbp,%rax,8)
	movl	$999, %ebp              # imm = 0x3E7
	subq	%rax, %rbp
	leaq	(%rcx,%rax,8), %rbx
	leaq	(%r10,%rax,8), %rax
	.p2align	4, 0x90
.LBB6_85:                               # %scalar.ph255
                                        #   Parent Loop BB6_63 Depth=1
                                        #     Parent Loop BB6_78 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	-8(%rax), %xmm2         # xmm2 = mem[0],zero
	subsd	-16(%rax), %xmm2
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	movsd	%xmm1, -8(%rbx)
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	subsd	-8(%rax), %xmm2
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	movsd	%xmm1, (%rbx)
	addq	$16, %rbx
	addq	$16, %rax
	addq	$-2, %rbp
	jne	.LBB6_85
# BB#86:                                # %.loopexit336
                                        #   in Loop: Header=BB6_78 Depth=2
	incq	%rdx
	addq	$8000, %r9              # imm = 0x1F40
	addq	$8000, %r8              # imm = 0x1F40
	addq	$8000, %rcx             # imm = 0x1F40
	addq	$8000, %r10             # imm = 0x1F40
	cmpq	$1000, %rdx             # imm = 0x3E8
	jne	.LBB6_78
# BB#87:                                # %.preheader.i106.preheader
                                        #   in Loop: Header=BB6_63 Depth=1
	xorl	%r8d, %r8d
	movq	%r14, %r9
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB6_88:                               # %.preheader.i106
                                        #   Parent Loop BB6_63 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_92 Depth 3
                                        #       Child Loop BB6_95 Depth 3
	imulq	$8000, %r14, %rax       # imm = 0x1F40
	leaq	(%r12,%rax), %rcx
	leaq	7992(%r12,%rax), %rdx
	movq	32(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rax), %rsi
	leaq	8000(%rdi,%rax), %rdi
	leaq	(%r15,%rax), %rbp
	leaq	15992(%r15,%rax), %rax
	cmpq	%rdi, %rcx
	sbbb	%dil, %dil
	cmpq	%rdx, %rsi
	sbbb	%bl, %bl
	andb	%dil, %bl
	cmpq	%rax, %rcx
	sbbb	%al, %al
	cmpq	%rdx, %rbp
	sbbb	%cl, %cl
	testb	$1, %bl
	jne	.LBB6_89
# BB#90:                                # %.preheader.i106
                                        #   in Loop: Header=BB6_88 Depth=2
	andb	%cl, %al
	andb	$1, %al
	movl	$0, %esi
	jne	.LBB6_94
# BB#91:                                # %vector.body214.preheader
                                        #   in Loop: Header=BB6_88 Depth=2
	movl	$996, %eax              # imm = 0x3E4
	movq	%r9, %rcx
	movq	%r10, %rdx
	movq	%r11, %rsi
	.p2align	4, 0x90
.LBB6_92:                               # %vector.body214
                                        #   Parent Loop BB6_63 Depth=1
                                        #     Parent Loop BB6_88 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-16(%rdx), %xmm1
	movupd	(%rdx), %xmm2
	movupd	-24(%rsi), %xmm3
	movupd	-16(%rsi), %xmm5
	movupd	-8(%rsi), %xmm6
	movupd	(%rsi), %xmm7
	subpd	%xmm3, %xmm5
	subpd	%xmm6, %xmm7
	movupd	-16(%rcx), %xmm3
	movupd	(%rcx), %xmm6
	addpd	%xmm5, %xmm3
	addpd	%xmm7, %xmm6
	movupd	-8016(%rcx), %xmm5
	movupd	-8000(%rcx), %xmm7
	subpd	%xmm5, %xmm3
	subpd	%xmm7, %xmm6
	mulpd	%xmm11, %xmm3
	mulpd	%xmm11, %xmm6
	subpd	%xmm3, %xmm1
	subpd	%xmm6, %xmm2
	movupd	%xmm1, -16(%rdx)
	movupd	%xmm2, (%rdx)
	addq	$32, %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB6_92
# BB#93:                                #   in Loop: Header=BB6_88 Depth=2
	movl	$996, %esi              # imm = 0x3E4
	jmp	.LBB6_94
	.p2align	4, 0x90
.LBB6_89:                               #   in Loop: Header=BB6_88 Depth=2
	xorl	%esi, %esi
.LBB6_94:                               # %scalar.ph216.preheader
                                        #   in Loop: Header=BB6_88 Depth=2
	incq	%r14
	leaq	(%r8,%rsi,8), %rax
	leaq	(%r15,%rax), %rcx
	movl	$999, %edx              # imm = 0x3E7
	subq	%rsi, %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax), %rsi
	addq	%r12, %rax
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_95:                               # %scalar.ph216
                                        #   Parent Loop BB6_63 Depth=1
                                        #     Parent Loop BB6_88 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	8(%rsi,%rdi,8), %xmm1   # xmm1 = mem[0],zero
	subsd	(%rsi,%rdi,8), %xmm1
	addsd	8000(%rcx,%rdi,8), %xmm1
	subsd	(%rcx,%rdi,8), %xmm1
	mulsd	%xmm10, %xmm1
	addsd	(%rax,%rdi,8), %xmm1
	movsd	%xmm1, (%rax,%rdi,8)
	incq	%rdi
	cmpq	%rdi, %rdx
	jne	.LBB6_95
# BB#96:                                # %.loopexit
                                        #   in Loop: Header=BB6_88 Depth=2
	addq	$8000, %r11             # imm = 0x1F40
	addq	$8000, %r10             # imm = 0x1F40
	addq	$8000, %r9              # imm = 0x1F40
	addq	$8000, %r8              # imm = 0x1F40
	cmpq	$999, %r14              # imm = 0x3E7
	jne	.LBB6_88
# BB#97:                                #   in Loop: Header=BB6_63 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	$50, %rcx
	movq	104(%rsp), %r14         # 8-byte Reload
	jne	.LBB6_63
# BB#98:                                # %kernel_fdtd_2d_StrictFP.exit
	movl	$16001, %edi            # imm = 0x3E81
	callq	malloc
	movq	%rax, %r14
	movb	$0, 16000(%r14)
	xorl	%ebp, %ebp
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_99:                               # %.preheader.i113
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_100 Depth 2
                                        #     Child Loop BB6_102 Depth 2
                                        #     Child Loop BB6_104 Depth 2
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$15, %ecx
	.p2align	4, 0x90
.LBB6_100:                              #   Parent Loop BB6_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movl	%edx, %ebx
	andb	$15, %bl
	orb	$48, %bl
	movb	%bl, -15(%r14,%rcx)
	movb	%bl, -14(%r14,%rcx)
	movq	%rdx, %rsi
	shrq	$8, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -13(%r14,%rcx)
	movb	%sil, -12(%r14,%rcx)
	movq	%rdx, %rsi
	shrq	$16, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -11(%r14,%rcx)
	movb	%sil, -10(%r14,%rcx)
	movl	%edx, %esi
	shrl	$24, %esi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -9(%r14,%rcx)
	movb	%sil, -8(%r14,%rcx)
	movq	%rdx, %rsi
	shrq	$32, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -7(%r14,%rcx)
	movb	%sil, -6(%r14,%rcx)
	movq	%rdx, %rsi
	shrq	$40, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -5(%r14,%rcx)
	movb	%sil, -4(%r14,%rcx)
	movq	%rdx, %rsi
	shrq	$48, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -3(%r14,%rcx)
	movb	%sil, -2(%r14,%rcx)
	shrq	$56, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, -1(%r14,%rcx)
	movb	%dl, (%r14,%rcx)
	addq	$16, %rcx
	addq	$8, %rax
	cmpq	$16015, %rcx            # imm = 0x3E8F
	jne	.LBB6_100
# BB#101:                               #   in Loop: Header=BB6_99 Depth=1
	movq	stderr(%rip), %rsi
	movq	%r14, %rdi
	callq	fputs
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$15, %ecx
	.p2align	4, 0x90
.LBB6_102:                              #   Parent Loop BB6_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movl	%edx, %ebx
	andb	$15, %bl
	orb	$48, %bl
	movb	%bl, -15(%r14,%rcx)
	movb	%bl, -14(%r14,%rcx)
	movq	%rdx, %rsi
	shrq	$8, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -13(%r14,%rcx)
	movb	%sil, -12(%r14,%rcx)
	movq	%rdx, %rsi
	shrq	$16, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -11(%r14,%rcx)
	movb	%sil, -10(%r14,%rcx)
	movl	%edx, %esi
	shrl	$24, %esi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -9(%r14,%rcx)
	movb	%sil, -8(%r14,%rcx)
	movq	%rdx, %rsi
	shrq	$32, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -7(%r14,%rcx)
	movb	%sil, -6(%r14,%rcx)
	movq	%rdx, %rsi
	shrq	$40, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -5(%r14,%rcx)
	movb	%sil, -4(%r14,%rcx)
	movq	%rdx, %rsi
	shrq	$48, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -3(%r14,%rcx)
	movb	%sil, -2(%r14,%rcx)
	shrq	$56, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, -1(%r14,%rcx)
	movb	%dl, (%r14,%rcx)
	addq	$16, %rcx
	addq	$8, %rax
	cmpq	$16015, %rcx            # imm = 0x3E8F
	jne	.LBB6_102
# BB#103:                               #   in Loop: Header=BB6_99 Depth=1
	movq	stderr(%rip), %rsi
	movq	%r14, %rdi
	callq	fputs
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	$15, %ecx
	.p2align	4, 0x90
.LBB6_104:                              #   Parent Loop BB6_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movl	%edx, %ebx
	andb	$15, %bl
	orb	$48, %bl
	movb	%bl, -15(%r14,%rcx)
	movb	%bl, -14(%r14,%rcx)
	movq	%rdx, %rsi
	shrq	$8, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -13(%r14,%rcx)
	movb	%sil, -12(%r14,%rcx)
	movq	%rdx, %rsi
	shrq	$16, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -11(%r14,%rcx)
	movb	%sil, -10(%r14,%rcx)
	movl	%edx, %esi
	shrl	$24, %esi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -9(%r14,%rcx)
	movb	%sil, -8(%r14,%rcx)
	movq	%rdx, %rsi
	shrq	$32, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -7(%r14,%rcx)
	movb	%sil, -6(%r14,%rcx)
	movq	%rdx, %rsi
	shrq	$40, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -5(%r14,%rcx)
	movb	%sil, -4(%r14,%rcx)
	movq	%rdx, %rsi
	shrq	$48, %rsi
	andb	$15, %sil
	orb	$48, %sil
	movb	%sil, -3(%r14,%rcx)
	movb	%sil, -2(%r14,%rcx)
	shrq	$56, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, -1(%r14,%rcx)
	movb	%dl, (%r14,%rcx)
	addq	$16, %rcx
	addq	$8, %rax
	cmpq	$16015, %rcx            # imm = 0x3E8F
	jne	.LBB6_104
# BB#105:                               #   in Loop: Header=BB6_99 Depth=1
	movq	stderr(%rip), %rsi
	movq	%r14, %rdi
	callq	fputs
	incq	%rbp
	movq	24(%rsp), %rax          # 8-byte Reload
	addq	$8000, %rax             # imm = 0x1F40
	addq	$8000, 8(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x1F40
	addq	$8000, 16(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x1F40
	cmpq	$1000, %rbp             # imm = 0x3E8
	jne	.LBB6_99
# BB#106:                               # %print_array.exit
	movq	%r14, %rdi
	callq	free
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r13, %rdi
	callq	free
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	free
	xorl	%eax, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_107:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
