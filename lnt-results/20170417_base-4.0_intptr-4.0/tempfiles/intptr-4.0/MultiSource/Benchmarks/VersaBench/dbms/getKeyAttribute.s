	.text
	.file	"getKeyAttribute.bc"
	.globl	getKeyAttribute
	.p2align	4, 0x90
	.type	getKeyAttribute,@function
getKeyAttribute:                        # @getKeyAttribute
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	callq	getFloat
	testq	%rax, %rax
	je	.LBB0_1
# BB#2:
	cmpq	$1, %rax
	jne	.LBB0_4
# BB#3:
	movl	$getKeyAttribute.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$1, %eax
	popq	%rcx
	retq
.LBB0_1:
	xorl	%eax, %eax
	popq	%rcx
	retq
.LBB0_4:
	movq	%rax, %rcx
	orq	$1, %rcx
	cmpq	$3, %rcx
	jne	.LBB0_6
# BB#5:
	movl	$getKeyAttribute.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$2, %eax
.LBB0_6:
	popq	%rcx
	retq
.Lfunc_end0:
	.size	getKeyAttribute, .Lfunc_end0-getKeyAttribute
	.cfi_endproc

	.type	getKeyAttribute.name,@object # @getKeyAttribute.name
	.data
	.p2align	4
getKeyAttribute.name:
	.asciz	"getKeyAttribute"
	.size	getKeyAttribute.name, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
