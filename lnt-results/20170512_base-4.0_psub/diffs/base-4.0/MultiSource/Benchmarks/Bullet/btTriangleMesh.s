	.text
	.file	"btTriangleMesh.bc"
	.globl	_ZN14btTriangleMeshC2Ebb
	.p2align	4, 0x90
	.type	_ZN14btTriangleMeshC2Ebb,@function
_ZN14btTriangleMeshC2Ebb:               # @_ZN14btTriangleMeshC2Ebb
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$1065353216, 8(%r14)    # imm = 0x3F800000
	movl	$1065353216, 12(%r14)   # imm = 0x3F800000
	movl	$1065353216, 16(%r14)   # imm = 0x3F800000
	movl	$0, 20(%r14)
	movb	$1, 48(%r14)
	movq	$0, 40(%r14)
	movl	$0, 28(%r14)
	movl	$0, 32(%r14)
	movl	$0, 64(%r14)
	movq	$_ZTV14btTriangleMesh+16, (%r14)
	movb	$1, 128(%r14)
	movq	$0, 120(%r14)
	movl	$0, 108(%r14)
	movl	$0, 112(%r14)
	movb	$1, 160(%r14)
	movq	$0, 152(%r14)
	movl	$0, 140(%r14)
	movl	$0, 144(%r14)
	movb	$1, 192(%r14)
	movq	$0, 184(%r14)
	movl	$0, 172(%r14)
	movl	$0, 176(%r14)
	movb	$1, 224(%r14)
	movq	$0, 216(%r14)
	movl	$0, 204(%r14)
	movl	$0, 208(%r14)
	movb	%sil, 232(%r14)
	movb	%dl, 233(%r14)
	movl	$0, 236(%r14)
.Ltmp0:
	movl	$48, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
.Ltmp1:
# BB#1:                                 # %_ZN20btAlignedObjectArrayI13btIndexedMeshE8allocateEi.exit.i.i
	movslq	28(%r14), %rax
	testq	%rax, %rax
	jle	.LBB0_9
# BB#2:                                 # %.lr.ph.i.i.i
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB0_3
# BB#4:                                 # %.prol.preheader
	xorl	%edi, %edi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	movq	40(%r14), %rcx
	movups	(%rcx,%rdi), %xmm0
	movups	16(%rcx,%rdi), %xmm1
	movups	32(%rcx,%rdi), %xmm2
	movups	%xmm2, 32(%rbx,%rdi)
	movups	%xmm1, 16(%rbx,%rdi)
	movups	%xmm0, (%rbx,%rdi)
	incq	%rdx
	addq	$48, %rdi
	cmpq	%rdx, %rsi
	jne	.LBB0_5
	jmp	.LBB0_6
.LBB0_3:
	xorl	%edx, %edx
.LBB0_6:                                # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB0_9
# BB#7:                                 # %.lr.ph.i.i.i.new
	subq	%rdx, %rax
	leaq	(%rdx,%rdx,2), %rcx
	shlq	$4, %rcx
	addq	$144, %rcx
	.p2align	4, 0x90
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	movq	40(%r14), %rdx
	movups	-144(%rdx,%rcx), %xmm0
	movups	-128(%rdx,%rcx), %xmm1
	movups	-112(%rdx,%rcx), %xmm2
	movups	%xmm2, -112(%rbx,%rcx)
	movups	%xmm1, -128(%rbx,%rcx)
	movups	%xmm0, -144(%rbx,%rcx)
	movq	40(%r14), %rdx
	movups	-96(%rdx,%rcx), %xmm0
	movups	-80(%rdx,%rcx), %xmm1
	movups	-64(%rdx,%rcx), %xmm2
	movups	%xmm2, -64(%rbx,%rcx)
	movups	%xmm1, -80(%rbx,%rcx)
	movups	%xmm0, -96(%rbx,%rcx)
	movq	40(%r14), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	-32(%rdx,%rcx), %xmm1
	movups	-16(%rdx,%rcx), %xmm2
	movups	%xmm2, -16(%rbx,%rcx)
	movups	%xmm1, -32(%rbx,%rcx)
	movups	%xmm0, -48(%rbx,%rcx)
	movq	40(%r14), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	16(%rdx,%rcx), %xmm1
	movups	32(%rdx,%rcx), %xmm2
	movups	%xmm2, 32(%rbx,%rcx)
	movups	%xmm1, 16(%rbx,%rcx)
	movups	%xmm0, (%rbx,%rcx)
	addq	$192, %rcx
	addq	$-4, %rax
	jne	.LBB0_8
.LBB0_9:                                # %_ZNK20btAlignedObjectArrayI13btIndexedMeshE4copyEiiPS0_.exit.i.i
	movq	40(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_13
# BB#10:
	cmpb	$0, 48(%r14)
	je	.LBB0_12
# BB#11:
.Ltmp2:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp3:
.LBB0_12:                               # %.noexc48
	movq	$0, 40(%r14)
.LBB0_13:
	leaq	172(%r14), %rcx
	leaq	204(%r14), %rax
	movb	$1, 48(%r14)
	movq	%rbx, 40(%r14)
	movl	$1, 32(%r14)
	movslq	28(%r14), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$4, %rdx
	movl	$0, (%rbx,%rdx)
	movq	$0, 8(%rbx,%rdx)
	movl	$12, 16(%rbx,%rdx)
	movl	$0, 20(%rbx,%rdx)
	movq	$0, 24(%rbx,%rdx)
	movabsq	$8589934608, %rsi       # imm = 0x200000010
	movq	%rsi, 32(%rbx,%rdx)
	movl	$0, 40(%rbx,%rdx)
	incl	28(%r14)
	xorl	%edx, %edx
	cmpb	$0, 232(%r14)
	sete	%dl
	cmoveq	%rax, %rcx
	movl	$6, %esi
	movl	$12, %eax
	cmovnel	%eax, %esi
	orl	$2, %edx
	movslq	(%rcx), %rcx
	imulq	$1431655766, %rcx, %rdi # imm = 0x55555556
	movq	%rdi, %rcx
	shrq	$63, %rcx
	shrq	$32, %rdi
	addl	%ecx, %edi
	movq	40(%r14), %rcx
	movl	%edi, (%rcx)
	movq	$0, 8(%rcx)
	movl	%edx, 36(%rcx)
	movl	%esi, 16(%rcx)
	cmpb	$0, 233(%r14)
	je	.LBB0_33
# BB#14:
	movl	108(%r14), %edx
	movl	$16, %eax
	jmp	.LBB0_34
.LBB0_33:
	movslq	140(%r14), %rdx
	imulq	$1431655766, %rdx, %rdx # imm = 0x55555556
	movq	%rdx, %rsi
	shrq	$63, %rsi
	shrq	$32, %rdx
	addl	%esi, %edx
.LBB0_34:
	movl	%edx, 20(%rcx)
	movq	$0, 24(%rcx)
	movl	%eax, 32(%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_15:
.Ltmp4:
	movq	%rax, %rbx
	movq	216(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_19
# BB#16:
	cmpb	$0, 224(%r14)
	je	.LBB0_18
# BB#17:
.Ltmp5:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp6:
.LBB0_18:                               # %.noexc46
	movq	$0, 216(%r14)
.LBB0_19:
	movb	$1, 224(%r14)
	movq	$0, 216(%r14)
	movq	$0, 204(%r14)
	movq	184(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_23
# BB#20:
	cmpb	$0, 192(%r14)
	je	.LBB0_22
# BB#21:
.Ltmp7:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp8:
.LBB0_22:                               # %.noexc44
	movq	$0, 184(%r14)
.LBB0_23:
	movb	$1, 192(%r14)
	movq	$0, 184(%r14)
	movq	$0, 172(%r14)
	movq	152(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_27
# BB#24:
	cmpb	$0, 160(%r14)
	je	.LBB0_26
# BB#25:
.Ltmp9:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp10:
.LBB0_26:                               # %.noexc42
	movq	$0, 152(%r14)
.LBB0_27:
	movb	$1, 160(%r14)
	movq	$0, 152(%r14)
	movq	$0, 140(%r14)
	movq	120(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_31
# BB#28:
	cmpb	$0, 128(%r14)
	je	.LBB0_30
# BB#29:
.Ltmp11:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp12:
.LBB0_30:                               # %.noexc
	movq	$0, 120(%r14)
.LBB0_31:
	movb	$1, 128(%r14)
	movq	$0, 120(%r14)
	movq	$0, 108(%r14)
.Ltmp13:
	movq	%r14, %rdi
	callq	_ZN26btTriangleIndexVertexArrayD2Ev
.Ltmp14:
# BB#32:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_35:
.Ltmp15:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN14btTriangleMeshC2Ebb, .Lfunc_end0-_ZN14btTriangleMeshC2Ebb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\257\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp14-.Ltmp5          #   Call between .Ltmp5 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	1                       #   On action: 1
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp14     #   Call between .Ltmp14 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN14btTriangleMesh8addIndexEi
	.p2align	4, 0x90
	.type	_ZN14btTriangleMesh8addIndexEi,@function
_ZN14btTriangleMesh8addIndexEi:         # @_ZN14btTriangleMesh8addIndexEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 64
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	cmpb	$0, 232(%r13)
	je	.LBB2_34
# BB#1:
	movl	172(%r13), %eax
	cmpl	176(%r13), %eax
	jne	.LBB2_33
# BB#2:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %r15d
	cmovnel	%ecx, %r15d
	cmpl	%r15d, %eax
	jge	.LBB2_33
# BB#3:
	testl	%r15d, %r15d
	je	.LBB2_4
# BB#5:
	movslq	%r15d, %rdi
	shlq	$2, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r12
	movl	172(%r13), %eax
	jmp	.LBB2_6
.LBB2_34:
	movl	204(%r13), %r8d
	cmpl	208(%r13), %r8d
	jne	.LBB2_66
# BB#35:
	leal	(%r8,%r8), %eax
	testl	%r8d, %r8d
	movl	$1, %r15d
	cmovnel	%eax, %r15d
	cmpl	%r15d, %r8d
	jge	.LBB2_66
# BB#36:
	testl	%r15d, %r15d
	je	.LBB2_37
# BB#38:
	movslq	%r15d, %rdi
	addq	%rdi, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r12
	movl	204(%r13), %r8d
	jmp	.LBB2_39
.LBB2_4:
	xorl	%r12d, %r12d
.LBB2_6:                                # %_ZN20btAlignedObjectArrayIjE8allocateEi.exit.i.i
	movq	184(%r13), %rdi
	testl	%eax, %eax
	jle	.LBB2_28
# BB#7:                                 # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	cmpl	$8, %eax
	jae	.LBB2_9
# BB#8:
	xorl	%edx, %edx
	jmp	.LBB2_22
.LBB2_28:                               # %_ZNK20btAlignedObjectArrayIjE4copyEiiPj.exit.i.i
	testq	%rdi, %rdi
	jne	.LBB2_29
	jmp	.LBB2_32
.LBB2_9:                                # %min.iters.checked
	movq	%rcx, %rdx
	andq	$-8, %rdx
	je	.LBB2_10
# BB#11:                                # %vector.memcheck
	leaq	(%rdi,%rcx,4), %rsi
	cmpq	%rsi, %r12
	jae	.LBB2_14
# BB#12:                                # %vector.memcheck
	leaq	(%r12,%rcx,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB2_14
# BB#13:
	xorl	%edx, %edx
	jmp	.LBB2_22
.LBB2_37:
	xorl	%r12d, %r12d
.LBB2_39:                               # %_ZN20btAlignedObjectArrayItE8allocateEi.exit.i.i
	movq	216(%r13), %rdi
	testl	%r8d, %r8d
	jle	.LBB2_61
# BB#40:                                # %.lr.ph.i.i.i9
	movslq	%r8d, %rcx
	cmpl	$16, %r8d
	jae	.LBB2_42
# BB#41:
	xorl	%edx, %edx
	jmp	.LBB2_55
.LBB2_61:                               # %_ZNK20btAlignedObjectArrayItE4copyEiiPt.exit.i.i
	testq	%rdi, %rdi
	jne	.LBB2_62
	jmp	.LBB2_65
.LBB2_42:                               # %min.iters.checked30
	movq	%rcx, %rdx
	andq	$-16, %rdx
	je	.LBB2_43
# BB#44:                                # %vector.memcheck42
	leaq	(%rdi,%rcx,2), %rax
	cmpq	%rax, %r12
	jae	.LBB2_47
# BB#45:                                # %vector.memcheck42
	leaq	(%r12,%rcx,2), %rax
	cmpq	%rax, %rdi
	jae	.LBB2_47
# BB#46:
	xorl	%edx, %edx
	jmp	.LBB2_55
.LBB2_10:
	xorl	%edx, %edx
	jmp	.LBB2_22
.LBB2_14:                               # %vector.body.preheader
	leaq	-8(%rdx), %rbp
	movl	%ebp, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_15
# BB#16:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_17:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB2_17
	jmp	.LBB2_18
.LBB2_43:
	xorl	%edx, %edx
	jmp	.LBB2_55
.LBB2_47:                               # %vector.body26.preheader
	leaq	-16(%rdx), %rbp
	movl	%ebp, %esi
	shrl	$4, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_48
# BB#49:                                # %vector.body26.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
.LBB2_50:                               # %vector.body26.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,2), %xmm0
	movups	16(%rdi,%rbx,2), %xmm1
	movups	%xmm0, (%r12,%rbx,2)
	movups	%xmm1, 16(%r12,%rbx,2)
	addq	$16, %rbx
	incq	%rsi
	jne	.LBB2_50
	jmp	.LBB2_51
.LBB2_15:
	xorl	%ebx, %ebx
.LBB2_18:                               # %vector.body.prol.loopexit
	cmpq	$24, %rbp
	jb	.LBB2_21
# BB#19:                                # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rbx, %rsi
	leaq	112(%rdi,%rbx,4), %rbp
	leaq	112(%r12,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB2_20:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-32, %rsi
	jne	.LBB2_20
.LBB2_21:                               # %middle.block
	cmpq	%rdx, %rcx
	je	.LBB2_29
.LBB2_22:                               # %scalar.ph.preheader
	subl	%edx, %eax
	leaq	-1(%rcx), %rsi
	subq	%rdx, %rsi
	andq	$7, %rax
	je	.LBB2_25
# BB#23:                                # %scalar.ph.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB2_24:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %ebp
	movl	%ebp, (%r12,%rdx,4)
	incq	%rdx
	incq	%rax
	jne	.LBB2_24
.LBB2_25:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB2_29
# BB#26:                                # %scalar.ph.preheader.new
	subq	%rdx, %rcx
	leaq	28(%rdi,%rdx,4), %rax
	leaq	28(%r12,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB2_27:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rax), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$32, %rax
	addq	$32, %rdx
	addq	$-8, %rcx
	jne	.LBB2_27
.LBB2_29:                               # %_ZNK20btAlignedObjectArrayIjE4copyEiiPj.exit.thread.i.i
	cmpb	$0, 192(%r13)
	je	.LBB2_31
# BB#30:
	callq	_Z21btAlignedFreeInternalPv
.LBB2_31:
	movq	$0, 184(%r13)
	movl	172(%r13), %eax
.LBB2_32:                               # %_ZN20btAlignedObjectArrayIjE10deallocateEv.exit.i.i
	movb	$1, 192(%r13)
	movq	%r12, 184(%r13)
	movl	%r15d, 176(%r13)
.LBB2_33:                               # %_ZN20btAlignedObjectArrayIjE9push_backERKj.exit
	leaq	184(%r13), %rcx
	movq	184(%r13), %rdx
	cltq
	movl	%r14d, (%rdx,%rax,4)
	incl	172(%r13)
	jmp	.LBB2_67
.LBB2_48:
	xorl	%ebx, %ebx
.LBB2_51:                               # %vector.body26.prol.loopexit
	cmpq	$48, %rbp
	jb	.LBB2_54
# BB#52:                                # %vector.body26.preheader.new
	movq	%rdx, %rsi
	subq	%rbx, %rsi
	leaq	112(%rdi,%rbx,2), %rbp
	leaq	112(%r12,%rbx,2), %rbx
.LBB2_53:                               # %vector.body26
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-64, %rsi
	jne	.LBB2_53
.LBB2_54:                               # %middle.block27
	cmpq	%rdx, %rcx
	je	.LBB2_62
.LBB2_55:                               # %scalar.ph28.preheader
	movl	%r8d, %eax
	subl	%edx, %eax
	leaq	-1(%rcx), %rsi
	subq	%rdx, %rsi
	andq	$7, %rax
	je	.LBB2_58
# BB#56:                                # %scalar.ph28.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB2_57:                               # %scalar.ph28.prol
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rdi,%rdx,2), %ebp
	movw	%bp, (%r12,%rdx,2)
	incq	%rdx
	incq	%rax
	jne	.LBB2_57
.LBB2_58:                               # %scalar.ph28.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB2_62
# BB#59:                                # %scalar.ph28.preheader.new
	subq	%rdx, %rcx
	leaq	14(%rdi,%rdx,2), %rsi
	leaq	14(%r12,%rdx,2), %rdx
	.p2align	4, 0x90
.LBB2_60:                               # %scalar.ph28
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-14(%rsi), %eax
	movw	%ax, -14(%rdx)
	movzwl	-12(%rsi), %eax
	movw	%ax, -12(%rdx)
	movzwl	-10(%rsi), %eax
	movw	%ax, -10(%rdx)
	movzwl	-8(%rsi), %eax
	movw	%ax, -8(%rdx)
	movzwl	-6(%rsi), %eax
	movw	%ax, -6(%rdx)
	movzwl	-4(%rsi), %eax
	movw	%ax, -4(%rdx)
	movzwl	-2(%rsi), %eax
	movw	%ax, -2(%rdx)
	movzwl	(%rsi), %eax
	movw	%ax, (%rdx)
	addq	$16, %rsi
	addq	$16, %rdx
	addq	$-8, %rcx
	jne	.LBB2_60
.LBB2_62:                               # %_ZNK20btAlignedObjectArrayItE4copyEiiPt.exit.thread.i.i
	cmpb	$0, 224(%r13)
	je	.LBB2_64
# BB#63:
	callq	_Z21btAlignedFreeInternalPv
	movl	204(%r13), %r8d
.LBB2_64:
	movq	$0, 216(%r13)
.LBB2_65:                               # %_ZN20btAlignedObjectArrayItE10deallocateEv.exit.i.i
	movb	$1, 224(%r13)
	movq	%r12, 216(%r13)
	movl	%r15d, 208(%r13)
.LBB2_66:                               # %_ZN20btAlignedObjectArrayItE9push_backERKt.exit
	leaq	216(%r13), %rcx
	movq	216(%r13), %rax
	movslq	%r8d, %rdx
	movw	%r14w, (%rax,%rdx,2)
	incl	%edx
	movl	%edx, 204(%r13)
.LBB2_67:
	movq	(%rcx), %rax
	movq	40(%r13), %rcx
	movq	%rax, 8(%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN14btTriangleMesh8addIndexEi, .Lfunc_end2-_ZN14btTriangleMesh8addIndexEi
	.cfi_endproc

	.globl	_ZN14btTriangleMesh15findOrAddVertexERK9btVector3b
	.p2align	4, 0x90
	.type	_ZN14btTriangleMesh15findOrAddVertexERK9btVector3b,@function
_ZN14btTriangleMesh15findOrAddVertexERK9btVector3b: # @_ZN14btTriangleMesh15findOrAddVertexERK9btVector3b
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 64
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	cmpb	$0, 233(%r12)
	je	.LBB3_10
# BB#1:
	movslq	108(%r12), %rcx
	testq	%rcx, %rcx
	jle	.LBB3_6
# BB#2:
	testb	%dl, %dl
	je	.LBB3_6
# BB#3:                                 # %.lr.ph101
	movq	120(%r12), %rdx
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	236(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	addq	$8, %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_4:                                # =>This Inner Loop Header: Depth=1
	movss	-8(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	-4(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm4
	subss	%xmm1, %xmm5
	movss	(%rdx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm6
	mulss	%xmm4, %xmm4
	mulss	%xmm5, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm6, %xmm6
	addss	%xmm5, %xmm6
	ucomiss	%xmm6, %xmm3
	jae	.LBB3_130
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=1
	incq	%rax
	addq	$16, %rdx
	cmpq	%rcx, %rax
	jl	.LBB3_4
.LBB3_6:                                # %.critedge
	movq	40(%r12), %rax
	incl	20(%rax)
	cmpl	112(%r12), %ecx
	jne	.LBB3_34
# BB#7:
	leal	(%rcx,%rcx), %eax
	testl	%ecx, %ecx
	movl	$1, %ebp
	cmovnel	%eax, %ebp
	cmpl	%ebp, %ecx
	jge	.LBB3_34
# BB#8:
	testl	%ebp, %ebp
	je	.LBB3_21
# BB#9:
	movslq	%ebp, %rdi
	shlq	$4, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	108(%r12), %ecx
	testl	%ecx, %ecx
	jg	.LBB3_22
	jmp	.LBB3_29
.LBB3_10:
	movslq	140(%r12), %r8
	testq	%r8, %r8
	jle	.LBB3_15
# BB#11:
	testb	%dl, %dl
	je	.LBB3_15
# BB#12:                                # %.lr.ph
	movq	152(%r12), %rcx
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	236(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_13:                               # =>This Inner Loop Header: Depth=1
	movss	(%rcx,%rdx,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	movss	4(%rcx,%rdx,4), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movss	8(%rcx,%rdx,4), %xmm6   # xmm6 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm4
	subss	%xmm1, %xmm5
	subss	%xmm2, %xmm6
	mulss	%xmm4, %xmm4
	mulss	%xmm5, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm6, %xmm6
	addss	%xmm5, %xmm6
	ucomiss	%xmm6, %xmm3
	jae	.LBB3_19
# BB#14:                                #   in Loop: Header=BB3_13 Depth=1
	addq	$3, %rdx
	cmpq	%r8, %rdx
	jl	.LBB3_13
.LBB3_15:                               # %.thread
	movl	(%r14), %ebx
	movl	144(%r12), %r13d
	cmpl	%r13d, %r8d
	jne	.LBB3_63
# BB#16:
	leal	(%r8,%r8), %eax
	testl	%r8d, %r8d
	movl	$1, %r13d
	cmovnel	%eax, %r13d
	cmpl	%r13d, %r8d
	jge	.LBB3_20
# BB#17:
	testl	%r13d, %r13d
	je	.LBB3_35
# BB#18:
	movslq	%r13d, %rdi
	shlq	$2, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	140(%r12), %r8d
	jmp	.LBB3_36
.LBB3_19:                               # %.loopexit
	movslq	%edx, %rax
	imulq	$1431655766, %rax, %rax # imm = 0x55555556
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	addl	%ecx, %eax
	jmp	.LBB3_130
.LBB3_20:
	movl	%r8d, %r13d
	jmp	.LBB3_63
.LBB3_21:
	xorl	%ebx, %ebx
	testl	%ecx, %ecx
	jle	.LBB3_29
.LBB3_22:                               # %.lr.ph.i.i.i70
	movslq	%ecx, %rax
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB3_25
# BB#23:                                # %.prol.preheader
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_24:                               # =>This Inner Loop Header: Depth=1
	movq	120(%r12), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%rbx,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB3_24
	jmp	.LBB3_26
.LBB3_25:
	xorl	%ecx, %ecx
.LBB3_26:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB3_29
# BB#27:                                # %.lr.ph.i.i.i70.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB3_28:                               # =>This Inner Loop Header: Depth=1
	movq	120(%r12), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbx,%rcx)
	movq	120(%r12), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbx,%rcx)
	movq	120(%r12), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbx,%rcx)
	movq	120(%r12), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbx,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB3_28
.LBB3_29:                               # %_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_.exit.i.i
	movq	120(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB3_33
# BB#30:
	cmpb	$0, 128(%r12)
	je	.LBB3_32
# BB#31:
	callq	_Z21btAlignedFreeInternalPv
.LBB3_32:
	movq	$0, 120(%r12)
.LBB3_33:                               # %_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv.exit.i.i
	movb	$1, 128(%r12)
	movq	%rbx, 120(%r12)
	movl	%ebp, 112(%r12)
	movl	108(%r12), %ecx
.LBB3_34:                               # %_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_.exit
	movq	120(%r12), %rax
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	movups	(%r14), %xmm0
	movups	%xmm0, (%rax,%rcx)
	movl	108(%r12), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 108(%r12)
	movq	40(%r12), %rcx
	movq	120(%r12), %rdx
	movq	%rdx, 24(%rcx)
	jmp	.LBB3_130
.LBB3_35:
	xorl	%r15d, %r15d
.LBB3_36:                               # %_ZN20btAlignedObjectArrayIfE8allocateEi.exit.i.i51
	movq	152(%r12), %rdi
	testl	%r8d, %r8d
	jle	.LBB3_39
# BB#37:                                # %.lr.ph.i.i.i53
	movslq	%r8d, %rcx
	cmpl	$8, %r8d
	jae	.LBB3_40
# BB#38:
	xorl	%edx, %edx
	jmp	.LBB3_53
.LBB3_39:                               # %_ZNK20btAlignedObjectArrayIfE4copyEiiPf.exit.i.i57
	testq	%rdi, %rdi
	jne	.LBB3_59
	jmp	.LBB3_62
.LBB3_40:                               # %min.iters.checked
	movq	%rcx, %rdx
	andq	$-8, %rdx
	je	.LBB3_44
# BB#41:                                # %vector.memcheck
	leaq	(%rdi,%rcx,4), %rax
	cmpq	%rax, %r15
	jae	.LBB3_45
# BB#42:                                # %vector.memcheck
	leaq	(%r15,%rcx,4), %rax
	cmpq	%rax, %rdi
	jae	.LBB3_45
.LBB3_44:
	xorl	%edx, %edx
.LBB3_53:                               # %scalar.ph.preheader
	movl	%r8d, %eax
	subl	%edx, %eax
	leaq	-1(%rcx), %rsi
	subq	%rdx, %rsi
	andq	$7, %rax
	je	.LBB3_56
# BB#54:                                # %scalar.ph.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB3_55:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %ebp
	movl	%ebp, (%r15,%rdx,4)
	incq	%rdx
	incq	%rax
	jne	.LBB3_55
.LBB3_56:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB3_59
# BB#57:                                # %scalar.ph.preheader.new
	subq	%rdx, %rcx
	leaq	28(%rdi,%rdx,4), %rsi
	leaq	28(%r15,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB3_58:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rsi), %eax
	movl	%eax, -28(%rdx)
	movl	-24(%rsi), %eax
	movl	%eax, -24(%rdx)
	movl	-20(%rsi), %eax
	movl	%eax, -20(%rdx)
	movl	-16(%rsi), %eax
	movl	%eax, -16(%rdx)
	movl	-12(%rsi), %eax
	movl	%eax, -12(%rdx)
	movl	-8(%rsi), %eax
	movl	%eax, -8(%rdx)
	movl	-4(%rsi), %eax
	movl	%eax, -4(%rdx)
	movl	(%rsi), %eax
	movl	%eax, (%rdx)
	addq	$32, %rsi
	addq	$32, %rdx
	addq	$-8, %rcx
	jne	.LBB3_58
.LBB3_59:                               # %_ZNK20btAlignedObjectArrayIfE4copyEiiPf.exit.thread.i.i60
	cmpb	$0, 160(%r12)
	je	.LBB3_61
# BB#60:
	callq	_Z21btAlignedFreeInternalPv
	movl	140(%r12), %r8d
.LBB3_61:
	movq	$0, 152(%r12)
.LBB3_62:                               # %_ZN20btAlignedObjectArrayIfE10deallocateEv.exit.i.i65
	movb	$1, 160(%r12)
	movq	%r15, 152(%r12)
	movl	%r13d, 144(%r12)
.LBB3_63:                               # %_ZN20btAlignedObjectArrayIfE9push_backERKf.exit66
	movq	152(%r12), %rdi
	movslq	%r8d, %rax
	movl	%ebx, (%rdi,%rax,4)
	incl	%eax
	movl	%eax, 140(%r12)
	movl	4(%r14), %ebp
	cmpl	%r13d, %eax
	jne	.LBB3_67
# BB#64:
	leal	(%r13,%r13), %eax
	testl	%r13d, %r13d
	movl	$1, %ebx
	cmovnel	%eax, %ebx
	cmpl	%ebx, %r13d
	jge	.LBB3_68
# BB#65:
	testl	%ebx, %ebx
	je	.LBB3_69
# BB#66:
	movslq	%ebx, %rdi
	shlq	$2, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	140(%r12), %r13d
	movq	152(%r12), %rdi
	testl	%r13d, %r13d
	jg	.LBB3_70
	jmp	.LBB3_72
.LBB3_67:
	movl	%r13d, %ebx
	jmp	.LBB3_97
.LBB3_68:
	movl	%r13d, %ebx
	jmp	.LBB3_96
.LBB3_69:
	xorl	%r15d, %r15d
	testl	%r13d, %r13d
	jle	.LBB3_72
.LBB3_70:                               # %.lr.ph.i.i.i35
	movslq	%r13d, %rax
	cmpl	$8, %r13d
	jae	.LBB3_73
# BB#71:
	xorl	%ecx, %ecx
	jmp	.LBB3_86
.LBB3_72:                               # %_ZNK20btAlignedObjectArrayIfE4copyEiiPf.exit.i.i39
	testq	%rdi, %rdi
	jne	.LBB3_92
	jmp	.LBB3_95
.LBB3_73:                               # %min.iters.checked124
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB3_77
# BB#74:                                # %vector.memcheck136
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r15
	jae	.LBB3_78
# BB#75:                                # %vector.memcheck136
	leaq	(%r15,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB3_78
.LBB3_77:
	xorl	%ecx, %ecx
.LBB3_86:                               # %scalar.ph122.preheader
	movl	%ebp, %r8d
	movl	%r13d, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB3_89
# BB#87:                                # %scalar.ph122.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB3_88:                               # %scalar.ph122.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %ebp
	movl	%ebp, (%r15,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB3_88
.LBB3_89:                               # %scalar.ph122.prol.loopexit
	cmpq	$7, %rdx
	movl	%r8d, %ebp
	jb	.LBB3_92
# BB#90:                                # %scalar.ph122.preheader.new
	subq	%rcx, %rax
	leaq	28(%rdi,%rcx,4), %rdx
	leaq	28(%r15,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_91:                               # %scalar.ph122
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rdx), %esi
	movl	%esi, -28(%rcx)
	movl	-24(%rdx), %esi
	movl	%esi, -24(%rcx)
	movl	-20(%rdx), %esi
	movl	%esi, -20(%rcx)
	movl	-16(%rdx), %esi
	movl	%esi, -16(%rcx)
	movl	-12(%rdx), %esi
	movl	%esi, -12(%rcx)
	movl	-8(%rdx), %esi
	movl	%esi, -8(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	movl	(%rdx), %esi
	movl	%esi, (%rcx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB3_91
.LBB3_92:                               # %_ZNK20btAlignedObjectArrayIfE4copyEiiPf.exit.thread.i.i42
	cmpb	$0, 160(%r12)
	je	.LBB3_94
# BB#93:
	callq	_Z21btAlignedFreeInternalPv
	movl	140(%r12), %r13d
.LBB3_94:
	movq	$0, 152(%r12)
.LBB3_95:                               # %_ZN20btAlignedObjectArrayIfE10deallocateEv.exit.i.i47
	movb	$1, 160(%r12)
	movq	%r15, 152(%r12)
	movl	%ebx, 144(%r12)
	movq	%r15, %rdi
.LBB3_96:                               # %_ZN20btAlignedObjectArrayIfE9push_backERKf.exit48
	movl	%r13d, %eax
.LBB3_97:                               # %_ZN20btAlignedObjectArrayIfE9push_backERKf.exit48
	cltq
	movl	%ebp, (%rdi,%rax,4)
	incl	%eax
	movl	%eax, 140(%r12)
	movl	8(%r14), %r15d
	cmpl	%ebx, %eax
	jne	.LBB3_129
# BB#98:
	leal	(%rbx,%rbx), %eax
	testl	%ebx, %ebx
	movl	$1, %r13d
	cmovnel	%eax, %r13d
	cmpl	%r13d, %ebx
	jge	.LBB3_128
# BB#99:
	testl	%r13d, %r13d
	je	.LBB3_101
# BB#100:
	movslq	%r13d, %rdi
	shlq	$2, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r14
	movl	140(%r12), %ebx
	movq	152(%r12), %rdi
	testl	%ebx, %ebx
	jg	.LBB3_102
	jmp	.LBB3_104
.LBB3_101:
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	jle	.LBB3_104
.LBB3_102:                              # %.lr.ph.i.i.i
	movslq	%ebx, %rax
	cmpl	$8, %ebx
	jae	.LBB3_105
# BB#103:
	xorl	%ecx, %ecx
	jmp	.LBB3_118
.LBB3_104:                              # %_ZNK20btAlignedObjectArrayIfE4copyEiiPf.exit.i.i
	testq	%rdi, %rdi
	jne	.LBB3_124
	jmp	.LBB3_127
.LBB3_105:                              # %min.iters.checked152
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB3_109
# BB#106:                               # %vector.memcheck164
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r14
	jae	.LBB3_110
# BB#107:                               # %vector.memcheck164
	leaq	(%r14,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB3_110
.LBB3_109:
	xorl	%ecx, %ecx
.LBB3_118:                              # %scalar.ph150.preheader
	movl	%ebx, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB3_121
# BB#119:                               # %scalar.ph150.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB3_120:                              # %scalar.ph150.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %ebp
	movl	%ebp, (%r14,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB3_120
.LBB3_121:                              # %scalar.ph150.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB3_124
# BB#122:                               # %scalar.ph150.preheader.new
	subq	%rcx, %rax
	leaq	28(%rdi,%rcx,4), %rdx
	leaq	28(%r14,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_123:                              # %scalar.ph150
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rdx), %esi
	movl	%esi, -28(%rcx)
	movl	-24(%rdx), %esi
	movl	%esi, -24(%rcx)
	movl	-20(%rdx), %esi
	movl	%esi, -20(%rcx)
	movl	-16(%rdx), %esi
	movl	%esi, -16(%rcx)
	movl	-12(%rdx), %esi
	movl	%esi, -12(%rcx)
	movl	-8(%rdx), %esi
	movl	%esi, -8(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	movl	(%rdx), %esi
	movl	%esi, (%rcx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB3_123
.LBB3_124:                              # %_ZNK20btAlignedObjectArrayIfE4copyEiiPf.exit.thread.i.i
	cmpb	$0, 160(%r12)
	je	.LBB3_126
# BB#125:
	callq	_Z21btAlignedFreeInternalPv
	movl	140(%r12), %ebx
.LBB3_126:
	movq	$0, 152(%r12)
.LBB3_127:                              # %_ZN20btAlignedObjectArrayIfE10deallocateEv.exit.i.i
	movb	$1, 160(%r12)
	movq	%r14, 152(%r12)
	movl	%r13d, 144(%r12)
	movq	%r14, %rdi
.LBB3_128:                              # %_ZN20btAlignedObjectArrayIfE9push_backERKf.exit
	movl	%ebx, %eax
.LBB3_129:                              # %_ZN20btAlignedObjectArrayIfE9push_backERKf.exit
	cltq
	movl	%r15d, (%rdi,%rax,4)
	incl	%eax
	movl	%eax, 140(%r12)
	movq	40(%r12), %rcx
	incl	20(%rcx)
	movq	%rdi, 24(%rcx)
	cltq
	imulq	$1431655766, %rax, %rax # imm = 0x55555556
	movq	%rax, %rcx
	shrq	$32, %rcx
	shrq	$63, %rax
	leal	-1(%rcx,%rax), %eax
.LBB3_130:                              # %.loopexit97
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_45:                               # %vector.body.preheader
	leaq	-8(%rdx), %rbp
	movl	%ebp, %esi
	shrl	$3, %esi
	incl	%esi
	movl	%ebx, %eax
	andq	$3, %rsi
	je	.LBB3_48
# BB#46:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
.LBB3_47:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r15,%rbx,4)
	movups	%xmm1, 16(%r15,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB3_47
	jmp	.LBB3_49
.LBB3_78:                               # %vector.body120.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	movl	%ebp, %r8d
	andq	$3, %rsi
	je	.LBB3_81
# BB#79:                                # %vector.body120.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB3_80:                               # %vector.body120.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp,4), %xmm0
	movups	16(%rdi,%rbp,4), %xmm1
	movups	%xmm0, (%r15,%rbp,4)
	movups	%xmm1, 16(%r15,%rbp,4)
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB3_80
	jmp	.LBB3_82
.LBB3_110:                              # %vector.body148.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB3_113
# BB#111:                               # %vector.body148.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB3_112:                              # %vector.body148.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp,4), %xmm0
	movups	16(%rdi,%rbp,4), %xmm1
	movups	%xmm0, (%r14,%rbp,4)
	movups	%xmm1, 16(%r14,%rbp,4)
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB3_112
	jmp	.LBB3_114
.LBB3_48:
	xorl	%ebx, %ebx
.LBB3_49:                               # %vector.body.prol.loopexit
	cmpq	$24, %rbp
	jb	.LBB3_52
# BB#50:                                # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rbx, %rsi
	leaq	112(%rdi,%rbx,4), %rbp
	leaq	112(%r15,%rbx,4), %rbx
.LBB3_51:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-32, %rsi
	jne	.LBB3_51
.LBB3_52:                               # %middle.block
	cmpq	%rdx, %rcx
	movl	%eax, %ebx
	je	.LBB3_59
	jmp	.LBB3_53
.LBB3_81:
	xorl	%ebp, %ebp
.LBB3_82:                               # %vector.body120.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB3_85
# BB#83:                                # %vector.body120.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%rdi,%rbp,4), %rsi
	leaq	112(%r15,%rbp,4), %rbp
.LBB3_84:                               # %vector.body120
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-32, %rdx
	jne	.LBB3_84
.LBB3_85:                               # %middle.block121
	cmpq	%rcx, %rax
	movl	%r8d, %ebp
	je	.LBB3_92
	jmp	.LBB3_86
.LBB3_113:
	xorl	%ebp, %ebp
.LBB3_114:                              # %vector.body148.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB3_117
# BB#115:                               # %vector.body148.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%rdi,%rbp,4), %rsi
	leaq	112(%r14,%rbp,4), %rbp
.LBB3_116:                              # %vector.body148
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-32, %rdx
	jne	.LBB3_116
.LBB3_117:                              # %middle.block149
	cmpq	%rcx, %rax
	je	.LBB3_124
	jmp	.LBB3_118
.Lfunc_end3:
	.size	_ZN14btTriangleMesh15findOrAddVertexERK9btVector3b, .Lfunc_end3-_ZN14btTriangleMesh15findOrAddVertexERK9btVector3b
	.cfi_endproc

	.globl	_ZN14btTriangleMesh11addTriangleERK9btVector3S2_S2_b
	.p2align	4, 0x90
	.type	_ZN14btTriangleMesh11addTriangleERK9btVector3S2_S2_b,@function
_ZN14btTriangleMesh11addTriangleERK9btVector3S2_S2_b: # @_ZN14btTriangleMesh11addTriangleERK9btVector3S2_S2_b
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 48
.Lcfi36:
	.cfi_offset %rbx, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movq	40(%rbx), %rax
	incl	(%rax)
	movzbl	%r8b, %ebp
	movl	%ebp, %edx
	callq	_ZN14btTriangleMesh15findOrAddVertexERK9btVector3b
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	_ZN14btTriangleMesh8addIndexEi
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	_ZN14btTriangleMesh15findOrAddVertexERK9btVector3b
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	_ZN14btTriangleMesh8addIndexEi
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	_ZN14btTriangleMesh15findOrAddVertexERK9btVector3b
	movq	%rbx, %rdi
	movl	%eax, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN14btTriangleMesh8addIndexEi # TAILCALL
.Lfunc_end4:
	.size	_ZN14btTriangleMesh11addTriangleERK9btVector3S2_S2_b, .Lfunc_end4-_ZN14btTriangleMesh11addTriangleERK9btVector3S2_S2_b
	.cfi_endproc

	.globl	_ZNK14btTriangleMesh15getNumTrianglesEv
	.p2align	4, 0x90
	.type	_ZNK14btTriangleMesh15getNumTrianglesEv,@function
_ZNK14btTriangleMesh15getNumTrianglesEv: # @_ZNK14btTriangleMesh15getNumTrianglesEv
	.cfi_startproc
# BB#0:
	cmpb	$0, 232(%rdi)
	leaq	204(%rdi), %rax
	leaq	172(%rdi), %rcx
	cmovneq	%rcx, %rax
	movslq	(%rax), %rax
	imulq	$1431655766, %rax, %rax # imm = 0x55555556
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	addl	%ecx, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end5:
	.size	_ZNK14btTriangleMesh15getNumTrianglesEv, .Lfunc_end5-_ZNK14btTriangleMesh15getNumTrianglesEv
	.cfi_endproc

	.section	.text._ZN14btTriangleMeshD2Ev,"axG",@progbits,_ZN14btTriangleMeshD2Ev,comdat
	.weak	_ZN14btTriangleMeshD2Ev
	.p2align	4, 0x90
	.type	_ZN14btTriangleMeshD2Ev,@function
_ZN14btTriangleMeshD2Ev:                # @_ZN14btTriangleMeshD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -24
.Lcfi44:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV14btTriangleMesh+16, (%rbx)
	movq	216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_4
# BB#1:
	cmpb	$0, 224(%rbx)
	je	.LBB6_3
# BB#2:
.Ltmp16:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp17:
.LBB6_3:                                # %.noexc
	movq	$0, 216(%rbx)
.LBB6_4:
	movb	$1, 224(%rbx)
	movq	$0, 216(%rbx)
	movq	$0, 204(%rbx)
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_8
# BB#5:
	cmpb	$0, 192(%rbx)
	je	.LBB6_7
# BB#6:
.Ltmp21:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp22:
.LBB6_7:                                # %.noexc6
	movq	$0, 184(%rbx)
.LBB6_8:
	movb	$1, 192(%rbx)
	movq	$0, 184(%rbx)
	movq	$0, 172(%rbx)
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_12
# BB#9:
	cmpb	$0, 160(%rbx)
	je	.LBB6_11
# BB#10:
.Ltmp26:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp27:
.LBB6_11:                               # %.noexc8
	movq	$0, 152(%rbx)
.LBB6_12:
	movb	$1, 160(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 140(%rbx)
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_16
# BB#13:
	cmpb	$0, 128(%rbx)
	je	.LBB6_15
# BB#14:
.Ltmp31:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp32:
.LBB6_15:                               # %.noexc10
	movq	$0, 120(%rbx)
.LBB6_16:
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN26btTriangleIndexVertexArrayD2Ev # TAILCALL
.LBB6_28:
.Ltmp33:
	movq	%rax, %r14
	jmp	.LBB6_35
.LBB6_29:
.Ltmp28:
	movq	%rax, %r14
	jmp	.LBB6_30
.LBB6_22:
.Ltmp23:
	movq	%rax, %r14
	jmp	.LBB6_23
.LBB6_17:
.Ltmp18:
	movq	%rax, %r14
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_21
# BB#18:
	cmpb	$0, 192(%rbx)
	je	.LBB6_20
# BB#19:
.Ltmp19:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp20:
.LBB6_20:                               # %.noexc12
	movq	$0, 184(%rbx)
.LBB6_21:                               # %_ZN20btAlignedObjectArrayIjED2Ev.exit13
	movb	$1, 192(%rbx)
	movq	$0, 184(%rbx)
	movq	$0, 172(%rbx)
.LBB6_23:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_27
# BB#24:
	cmpb	$0, 160(%rbx)
	je	.LBB6_26
# BB#25:
.Ltmp24:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp25:
.LBB6_26:                               # %.noexc15
	movq	$0, 152(%rbx)
.LBB6_27:                               # %_ZN20btAlignedObjectArrayIfED2Ev.exit16
	movb	$1, 160(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 140(%rbx)
.LBB6_30:
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_34
# BB#31:
	cmpb	$0, 128(%rbx)
	je	.LBB6_33
# BB#32:
.Ltmp29:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp30:
.LBB6_33:                               # %.noexc18
	movq	$0, 120(%rbx)
.LBB6_34:                               # %_ZN20btAlignedObjectArrayI9btVector3ED2Ev.exit19
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
.LBB6_35:
.Ltmp34:
	movq	%rbx, %rdi
	callq	_ZN26btTriangleIndexVertexArrayD2Ev
.Ltmp35:
# BB#36:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_37:
.Ltmp36:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN14btTriangleMeshD2Ev, .Lfunc_end6-_ZN14btTriangleMeshD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin1   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin1   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin1   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp19-.Ltmp32         #   Call between .Ltmp32 and .Ltmp19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp35-.Ltmp19         #   Call between .Ltmp19 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin1   #     jumps to .Ltmp36
	.byte	1                       #   On action: 1
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Lfunc_end6-.Ltmp35     #   Call between .Ltmp35 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN14btTriangleMeshD0Ev,"axG",@progbits,_ZN14btTriangleMeshD0Ev,comdat
	.weak	_ZN14btTriangleMeshD0Ev
	.p2align	4, 0x90
	.type	_ZN14btTriangleMeshD0Ev,@function
_ZN14btTriangleMeshD0Ev:                # @_ZN14btTriangleMeshD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 32
.Lcfi48:
	.cfi_offset %rbx, -24
.Lcfi49:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp37:
	callq	_ZN14btTriangleMeshD2Ev
.Ltmp38:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB7_2:
.Ltmp39:
	movq	%rax, %r14
.Ltmp40:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp41:
# BB#3:                                 # %_ZN26btTriangleIndexVertexArraydlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_4:
.Ltmp42:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN14btTriangleMeshD0Ev, .Lfunc_end7-_ZN14btTriangleMeshD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp37-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin2   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp40-.Ltmp38         #   Call between .Ltmp38 and .Ltmp40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin2   #     jumps to .Ltmp42
	.byte	1                       #   On action: 1
	.long	.Ltmp41-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end7-.Ltmp41     #   Call between .Ltmp41 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN26btTriangleIndexVertexArray16unLockVertexBaseEi,"axG",@progbits,_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi,comdat
	.weak	_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi
	.p2align	4, 0x90
	.type	_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi,@function
_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi: # @_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end8:
	.size	_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi, .Lfunc_end8-_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi
	.cfi_endproc

	.section	.text._ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi,"axG",@progbits,_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi,comdat
	.weak	_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi
	.p2align	4, 0x90
	.type	_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi,@function
_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi: # @_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end9:
	.size	_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi, .Lfunc_end9-_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi
	.cfi_endproc

	.section	.text._ZNK26btTriangleIndexVertexArray14getNumSubPartsEv,"axG",@progbits,_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv,comdat
	.weak	_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv
	.p2align	4, 0x90
	.type	_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv,@function
_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv: # @_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv
	.cfi_startproc
# BB#0:
	movl	28(%rdi), %eax
	retq
.Lfunc_end10:
	.size	_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv, .Lfunc_end10-_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv
	.cfi_endproc

	.section	.text._ZN14btTriangleMesh19preallocateVerticesEi,"axG",@progbits,_ZN14btTriangleMesh19preallocateVerticesEi,comdat
	.weak	_ZN14btTriangleMesh19preallocateVerticesEi
	.p2align	4, 0x90
	.type	_ZN14btTriangleMesh19preallocateVerticesEi,@function
_ZN14btTriangleMesh19preallocateVerticesEi: # @_ZN14btTriangleMesh19preallocateVerticesEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	_ZN14btTriangleMesh19preallocateVerticesEi, .Lfunc_end11-_ZN14btTriangleMesh19preallocateVerticesEi
	.cfi_endproc

	.section	.text._ZN14btTriangleMesh18preallocateIndicesEi,"axG",@progbits,_ZN14btTriangleMesh18preallocateIndicesEi,comdat
	.weak	_ZN14btTriangleMesh18preallocateIndicesEi
	.p2align	4, 0x90
	.type	_ZN14btTriangleMesh18preallocateIndicesEi,@function
_ZN14btTriangleMesh18preallocateIndicesEi: # @_ZN14btTriangleMesh18preallocateIndicesEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end12:
	.size	_ZN14btTriangleMesh18preallocateIndicesEi, .Lfunc_end12-_ZN14btTriangleMesh18preallocateIndicesEi
	.cfi_endproc

	.type	_ZTV14btTriangleMesh,@object # @_ZTV14btTriangleMesh
	.section	.rodata._ZTV14btTriangleMesh,"aG",@progbits,_ZTV14btTriangleMesh,comdat
	.weak	_ZTV14btTriangleMesh
	.p2align	3
_ZTV14btTriangleMesh:
	.quad	0
	.quad	_ZTI14btTriangleMesh
	.quad	_ZN14btTriangleMeshD2Ev
	.quad	_ZN14btTriangleMeshD0Ev
	.quad	_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_
	.quad	_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i
	.quad	_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i
	.quad	_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi
	.quad	_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi
	.quad	_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv
	.quad	_ZN14btTriangleMesh19preallocateVerticesEi
	.quad	_ZN14btTriangleMesh18preallocateIndicesEi
	.quad	_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv
	.quad	_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_
	.quad	_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_
	.size	_ZTV14btTriangleMesh, 120

	.type	_ZTS14btTriangleMesh,@object # @_ZTS14btTriangleMesh
	.section	.rodata._ZTS14btTriangleMesh,"aG",@progbits,_ZTS14btTriangleMesh,comdat
	.weak	_ZTS14btTriangleMesh
	.p2align	4
_ZTS14btTriangleMesh:
	.asciz	"14btTriangleMesh"
	.size	_ZTS14btTriangleMesh, 17

	.type	_ZTI14btTriangleMesh,@object # @_ZTI14btTriangleMesh
	.section	.rodata._ZTI14btTriangleMesh,"aG",@progbits,_ZTI14btTriangleMesh,comdat
	.weak	_ZTI14btTriangleMesh
	.p2align	4
_ZTI14btTriangleMesh:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14btTriangleMesh
	.quad	_ZTI26btTriangleIndexVertexArray
	.size	_ZTI14btTriangleMesh, 24


	.globl	_ZN14btTriangleMeshC1Ebb
	.type	_ZN14btTriangleMeshC1Ebb,@function
_ZN14btTriangleMeshC1Ebb = _ZN14btTriangleMeshC2Ebb
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
