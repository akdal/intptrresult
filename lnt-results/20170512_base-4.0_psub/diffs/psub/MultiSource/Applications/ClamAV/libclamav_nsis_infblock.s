	.text
	.file	"libclamav_nsis_infblock.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.text
	.globl	nsis_inflate
	.p2align	4, 0x90
	.type	nsis_inflate,@function
nsis_inflate:                           # @nsis_inflate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 192
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	(%r15), %rbp
	movl	8(%r15), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	1360(%r15), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	1356(%r15), %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	39912(%r15), %r14
	movq	39904(%r15), %r13
	cmpq	%r13, %r14
	jae	.LBB0_2
# BB#1:
	subq	%r14, %r13
	decq	%r13
	leaq	39896(%r15), %r12
	jmp	.LBB0_3
.LBB0_2:
	leaq	39896(%r15), %r12
	movq	39896(%r15), %r13
	subq	%r14, %r13
.LBB0_3:
	leaq	39912(%r15), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	7128(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	56(%r15), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	1344(%r15), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	1368(%r15), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	1336(%r15), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	jmp	.LBB0_8
.LBB0_7:                                # %.critedge
                                        #   in Loop: Header=BB0_8 Depth=1
	movl	$17, 40(%r15)
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_8
.LBB0_4:                                #   in Loop: Header=BB0_8 Depth=1
	testb	$64, %cl
	jne	.LBB0_256
.LBB0_5:                                #   in Loop: Header=BB0_8 Depth=1
	leaq	(%rdx,%rax,4), %rsi
	movl	%ecx, 64(%r15)
	movzwl	2(%rdx,%rax,4), %eax
	leaq	(%rsi,%rax,4), %rax
	movq	%rax, 56(%r15)
	jmp	.LBB0_8
.LBB0_6:                                #   in Loop: Header=BB0_8 Depth=1
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB0_8:                                # %.thread958thread-pre-split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_67 Depth 2
                                        #       Child Loop BB0_178 Depth 3
                                        #       Child Loop BB0_184 Depth 3
                                        #       Child Loop BB0_190 Depth 3
                                        #         Child Loop BB0_192 Depth 4
                                        #         Child Loop BB0_199 Depth 4
                                        #         Child Loop BB0_215 Depth 4
                                        #         Child Loop BB0_219 Depth 4
                                        #         Child Loop BB0_224 Depth 4
                                        #         Child Loop BB0_227 Depth 4
                                        #       Child Loop BB0_113 Depth 3
                                        #       Child Loop BB0_18 Depth 3
                                        #       Child Loop BB0_72 Depth 3
                                        #       Child Loop BB0_95 Depth 3
                                        #       Child Loop BB0_88 Depth 3
                                        #       Child Loop BB0_142 Depth 3
                                        #         Child Loop BB0_150 Depth 4
                                        #       Child Loop BB0_83 Depth 3
                                        #       Child Loop BB0_130 Depth 3
                                        #       Child Loop BB0_239 Depth 3
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movl	40(%r15), %ebx
	cmpl	$15, %ebx
	jbe	.LBB0_67
	jmp	.LBB0_255
.LBB0_197:                              #   in Loop: Header=BB0_190 Depth=3
	movl	%r8d, 24(%rsp)          # 4-byte Spill
	movq	%rbp, %r8
	leal	-14(%rdi), %r11d
	xorl	%r10d, %r10d
	cmpl	$18, %edi
	sete	88(%rsp)                # 1-byte Folded Spill
	movl	$7, %eax
	cmovel	%eax, %r11d
	leal	(%r11,%rdx), %ebx
	cmpl	%ebx, (%rsp)            # 4-byte Folded Reload
	jae	.LBB0_201
# BB#198:                               # %.lr.ph1447.preheader
                                        #   in Loop: Header=BB0_190 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_199:                              # %.lr.ph1447
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        #       Parent Loop BB0_190 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testl	%eax, %eax
	je	.LBB0_281
# BB#200:                               #   in Loop: Header=BB0_199 Depth=4
	decl	%eax
	movq	%rbp, %r9
	movzbl	(%r8), %ebp
	incq	%r8
	movq	(%rsp), %rcx            # 8-byte Reload
	shlq	%cl, %rbp
	orq	%rbp, %r9
	movq	%r9, %rbp
	addl	$8, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	cmpl	%ebx, %ecx
	jb	.LBB0_199
	jmp	.LBB0_202
.LBB0_201:                              #   in Loop: Header=BB0_190 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB0_202:                              # %._crit_edge1448
                                        #   in Loop: Header=BB0_190 Depth=3
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movb	88(%rsp), %al           # 1-byte Reload
	movb	%al, %r10b
	leal	3(,%r10,8), %r10d
	movl	%edx, %ecx
	shrq	%cl, %rbp
	movq	(%rsp), %rbx            # 8-byte Reload
	subl	%edx, %ebx
	movl	%r11d, %ecx
	movzwl	inflate_mask(%rcx,%rcx), %r9d
	andl	%ebp, %r9d
	movq	%rbp, %rax
	leal	(%r9,%r10), %ebp
	movl	%r11d, %ecx
	shrq	%cl, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	subl	%r11d, %ebx
	movq	%rbx, (%rsp)            # 8-byte Spill
	leal	(%rbp,%rsi), %eax
	cmpl	24(%rsp), %eax          # 4-byte Folded Reload
	ja	.LBB0_308
# BB#203:                               #   in Loop: Header=BB0_190 Depth=3
	cmpl	$16, %edi
	jne	.LBB0_205
# BB#204:                               #   in Loop: Header=BB0_190 Depth=3
	testl	%esi, %esi
	je	.LBB0_308
.LBB0_205:                              #   in Loop: Header=BB0_190 Depth=3
	xorl	%eax, %eax
	movzwl	%di, %ecx
	cmpl	$16, %ecx
	jne	.LBB0_207
# BB#206:                               #   in Loop: Header=BB0_190 Depth=3
	leal	-1(%rsi), %eax
	movl	56(%r15,%rax,4), %eax
.LBB0_207:                              #   in Loop: Header=BB0_190 Depth=3
	leal	(%r10,%rsi), %r11d
	cmpl	$8, %ebp
	jae	.LBB0_209
# BB#208:                               #   in Loop: Header=BB0_190 Depth=3
	movq	56(%rsp), %r10          # 8-byte Reload
	jmp	.LBB0_222
.LBB0_209:                              # %min.iters.checked
                                        #   in Loop: Header=BB0_190 Depth=3
	movl	%ebp, %edx
	andl	$131064, %edx           # imm = 0x1FFF8
	je	.LBB0_212
# BB#210:                               # %vector.scevcheck
                                        #   in Loop: Header=BB0_190 Depth=3
	leal	-1(%r10,%r9), %ecx
	addl	%esi, %ecx
	jae	.LBB0_213
.LBB0_212:                              #   in Loop: Header=BB0_190 Depth=3
	movq	56(%rsp), %r10          # 8-byte Reload
.LBB0_222:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_190 Depth=3
	leal	-1(%rbp), %edi
	movl	%ebp, %ecx
	andl	$7, %ecx
	je	.LBB0_225
# BB#223:                               # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB0_190 Depth=3
	negl	%ecx
	.p2align	4, 0x90
.LBB0_224:                              # %scalar.ph.prol
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        #       Parent Loop BB0_190 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%esi, %edx
	incl	%esi
	movl	%eax, 56(%r15,%rdx,4)
	decl	%ebp
	incl	%ecx
	jne	.LBB0_224
.LBB0_225:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB0_190 Depth=3
	cmpl	$7, %edi
	jb	.LBB0_228
# BB#226:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB0_190 Depth=3
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_227:                              # %scalar.ph
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        #       Parent Loop BB0_190 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%rsi,%rdi), %ecx
	leal	1(%rsi,%rdi), %edx
	movl	%eax, 56(%r15,%rcx,4)
	leal	2(%rsi,%rdi), %ecx
	movl	%eax, 56(%r15,%rdx,4)
	leal	3(%rsi,%rdi), %edx
	movl	%eax, 56(%r15,%rcx,4)
	leal	4(%rsi,%rdi), %ecx
	movl	%eax, 56(%r15,%rdx,4)
	leal	5(%rsi,%rdi), %edx
	movl	%eax, 56(%r15,%rcx,4)
	leal	6(%rsi,%rdi), %ecx
	movl	%eax, 56(%r15,%rdx,4)
	leal	7(%rsi,%rdi), %edx
	movl	%eax, 56(%r15,%rcx,4)
	movl	%eax, 56(%r15,%rdx,4)
	addl	$8, %edi
	cmpl	%edi, %ebp
	jne	.LBB0_227
.LBB0_228:                              # %.loopexit2359
                                        #   in Loop: Header=BB0_190 Depth=3
	addl	%r9d, %r11d
	movl	%r11d, 52(%r15)
	movl	%r11d, %esi
	movq	%r8, %rbp
	movl	24(%rsp), %r8d          # 4-byte Reload
	cmpl	%r8d, %esi
	jb	.LBB0_190
	jmp	.LBB0_230
.LBB0_213:                              # %vector.ph
                                        #   in Loop: Header=BB0_190 Depth=3
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	leal	-8(%rdx), %edx
	movl	%edx, %ecx
	shrl	$3, %ecx
	incl	%ecx
	andl	$3, %ecx
	movq	56(%rsp), %r10          # 8-byte Reload
	je	.LBB0_216
# BB#214:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_190 Depth=3
	negl	%ecx
	xorl	%ebx, %ebx
.LBB0_215:                              # %vector.body.prol
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        #       Parent Loop BB0_190 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%rsi,%rbx), %edi
	movdqu	%xmm0, 56(%r15,%rdi,4)
	movdqu	%xmm0, 72(%r15,%rdi,4)
	addl	$8, %ebx
	incl	%ecx
	jne	.LBB0_215
	jmp	.LBB0_217
.LBB0_216:                              #   in Loop: Header=BB0_190 Depth=3
	xorl	%ebx, %ebx
.LBB0_217:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_190 Depth=3
	cmpl	$24, %edx
	jb	.LBB0_220
# BB#218:                               # %vector.ph.new
                                        #   in Loop: Header=BB0_190 Depth=3
	movl	%esi, %edi
	movq	88(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
.LBB0_219:                              # %vector.body
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        #       Parent Loop BB0_190 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%rbx,%rdi), %edx
	movdqu	%xmm0, 56(%r15,%rdx,4)
	movdqu	%xmm0, 72(%r15,%rdx,4)
	leal	8(%rbx,%rdi), %edx
	movdqu	%xmm0, 56(%r15,%rdx,4)
	movdqu	%xmm0, 72(%r15,%rdx,4)
	leal	16(%rbx,%rdi), %edx
	movdqu	%xmm0, 56(%r15,%rdx,4)
	movdqu	%xmm0, 72(%r15,%rdx,4)
	leal	24(%rbx,%rdi), %edx
	movdqu	%xmm0, 56(%r15,%rdx,4)
	movdqu	%xmm0, 72(%r15,%rdx,4)
	addl	$-32, %ecx
	addl	$32, %edi
	cmpl	%ecx, %ebx
	jne	.LBB0_219
.LBB0_220:                              # %middle.block
                                        #   in Loop: Header=BB0_190 Depth=3
	movq	88(%rsp), %rcx          # 8-byte Reload
	cmpl	%ecx, %ebp
	je	.LBB0_228
# BB#221:                               #   in Loop: Header=BB0_190 Depth=3
	addl	%ecx, %esi
	subl	%ecx, %ebp
	jmp	.LBB0_222
.LBB0_190:                              # %.lr.ph1457
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_192 Depth 4
                                        #         Child Loop BB0_199 Depth 4
                                        #         Child Loop BB0_215 Depth 4
                                        #         Child Loop BB0_219 Depth 4
                                        #         Child Loop BB0_224 Depth 4
                                        #         Child Loop BB0_227 Depth 4
	movq	120(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	cmpl	%eax, (%rsp)            # 4-byte Folded Reload
	jae	.LBB0_194
# BB#191:                               # %.lr.ph1436.preheader
                                        #   in Loop: Header=BB0_190 Depth=3
	movq	16(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_192:                              # %.lr.ph1436
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        #       Parent Loop BB0_190 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testl	%edi, %edi
	je	.LBB0_266
# BB#193:                               #   in Loop: Header=BB0_192 Depth=4
	decl	%edi
	movzbl	(%rbp), %edx
	incq	%rbp
	movq	(%rsp), %rcx            # 8-byte Reload
	shlq	%cl, %rdx
	orq	%rdx, 8(%rsp)           # 8-byte Folded Spill
	addl	$8, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	cmpl	%eax, %ecx
	jb	.LBB0_192
	jmp	.LBB0_195
.LBB0_194:                              #   in Loop: Header=BB0_190 Depth=3
	movq	16(%rsp), %rdi          # 8-byte Reload
.LBB0_195:                              # %._crit_edge1437
                                        #   in Loop: Header=BB0_190 Depth=3
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %rcx
	movzwl	inflate_mask(%rax,%rax), %eax
	movq	8(%rsp), %rbx           # 8-byte Reload
	andq	%rbx, %rax
	movzbl	1(%rcx,%rax,4), %edx
	movzwl	2(%rcx,%rax,4), %edi
	cmpl	$15, %edi
	ja	.LBB0_197
# BB#196:                               #   in Loop: Header=BB0_190 Depth=3
	movl	%edx, %ecx
	shrq	%cl, %rbx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	(%rsp), %rax            # 8-byte Reload
	subl	%edx, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	%esi, %eax
	leal	1(%rsi), %ecx
	movl	%ecx, 52(%r15)
	movl	%edi, 56(%r15,%rax,4)
	movl	%ecx, %esi
	cmpl	%r8d, %esi
	jb	.LBB0_190
	jmp	.LBB0_230
.LBB0_65:                               # %.thread958.backedge.sink.split
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movl	%ebx, 40(%r15)
	cmpl	$15, %ebx
	jbe	.LBB0_67
	jmp	.LBB0_255
.LBB0_9:                                #   in Loop: Header=BB0_67 Depth=2
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
.LBB0_10:                               # %._crit_edge1474
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	56(%r15), %rdx
	movl	%eax, %eax
	movzwl	inflate_mask(%rax,%rax), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, %rsi
	andq	%rsi, %rax
	movzbl	1(%rdx,%rax,4), %ecx
	shrq	%cl, %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	(%rsp), %rsi            # 8-byte Reload
	subl	%ecx, %esi
	movq	%rsi, (%rsp)            # 8-byte Spill
	movzbl	(%rdx,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB0_14
# BB#11:                                #   in Loop: Header=BB0_67 Depth=2
	testb	$16, %cl
	jne	.LBB0_15
# BB#12:                                #   in Loop: Header=BB0_67 Depth=2
	testb	$64, %cl
	je	.LBB0_5
# BB#13:                                #   in Loop: Header=BB0_67 Depth=2
	movl	$7, %ebx
	testb	$32, %cl
	jne	.LBB0_65
	jmp	.LBB0_256
.LBB0_14:                               #   in Loop: Header=BB0_67 Depth=2
	movzwl	2(%rdx,%rax,4), %eax
	movl	%eax, 56(%r15)
	movl	$6, %ebx
	jmp	.LBB0_65
.LBB0_15:                               #   in Loop: Header=BB0_67 Depth=2
	andl	$15, %ecx
	movl	%ecx, 56(%r15)
	movzwl	2(%rdx,%rax,4), %eax
	movl	%eax, 48(%r15)
	movl	$2, %ebx
	jmp	.LBB0_65
.LBB0_16:                               #   in Loop: Header=BB0_67 Depth=2
	cmpb	$0, fixed_built(%rip)
	jne	.LBB0_26
# BB#17:                                #   in Loop: Header=BB0_67 Depth=2
	movl	$0, 64(%rsp)
	movl	$1, %eax
.LBB0_18:                               #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	-1(%rax), %rdx
	movl	$8, %ecx
	cmpq	$144, %rdx
	movl	$8, %esi
	jl	.LBB0_21
# BB#19:                                #   in Loop: Header=BB0_18 Depth=3
	movl	$9, %esi
	cmpq	$256, %rdx              # imm = 0x100
	jl	.LBB0_21
# BB#20:                                #   in Loop: Header=BB0_18 Depth=3
	xorl	%esi, %esi
	cmpq	$279, %rdx              # imm = 0x117
	setg	%sil
	addl	$7, %esi
.LBB0_21:                               #   in Loop: Header=BB0_18 Depth=3
	movl	%esi, nsis_inflate.lc-4(,%rax,4)
	cmpq	$142, %rdx
	jle	.LBB0_24
# BB#22:                                #   in Loop: Header=BB0_18 Depth=3
	movl	$9, %ecx
	cmpq	$256, %rax              # imm = 0x100
	jl	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_18 Depth=3
	xorl	%ecx, %ecx
	cmpq	$279, %rax              # imm = 0x117
	setg	%cl
	addl	$7, %ecx
.LBB0_24:                               #   in Loop: Header=BB0_18 Depth=3
	movl	%ecx, nsis_inflate.lc(,%rax,4)
	addq	$2, %rax
	cmpq	$289, %rax              # imm = 0x121
	jne	.LBB0_18
# BB#25:                                #   in Loop: Header=BB0_67 Depth=2
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movl	$nsis_inflate.lc, %edi
	movl	$288, %esi              # imm = 0x120
	movl	$257, %edx              # imm = 0x101
	movl	$cplens, %ecx
	movl	$cplext, %r8d
	movl	$fixed_tl, %r9d
	leaq	72(%rsp), %rax
	movq	%rax, %rbx
	pushq	%rbx
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	$fixed_mem
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	$fixed_bl
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	huft_build
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [5,5,5,5]
	movdqa	%xmm0, nsis_inflate.lc(%rip)
	movdqa	%xmm0, nsis_inflate.lc+16(%rip)
	movdqa	%xmm0, nsis_inflate.lc+32(%rip)
	movdqa	%xmm0, nsis_inflate.lc+48(%rip)
	movdqa	%xmm0, nsis_inflate.lc+64(%rip)
	movdqa	%xmm0, nsis_inflate.lc+80(%rip)
	movdqa	%xmm0, nsis_inflate.lc+96(%rip)
	movabsq	$21474836485, %rax      # imm = 0x500000005
	movq	%rax, nsis_inflate.lc+112(%rip)
	subq	$8, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	movl	$nsis_inflate.lc, %edi
	movl	$30, %esi
	xorl	%edx, %edx
	movl	$cpdist, %ecx
	movl	$cpdext, %r8d
	movl	$fixed_td, %r9d
	pushq	%rbx
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	$fixed_mem
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	$fixed_bd
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	callq	huft_build
	addq	$32, %rsp
.Lcfi22:
	.cfi_adjust_cfa_offset -32
	incb	fixed_built(%rip)
.LBB0_26:                               #   in Loop: Header=BB0_67 Depth=2
	movb	fixed_bl(%rip), %al
	movb	%al, 72(%r15)
	movb	fixed_bd(%rip), %al
	movb	%al, 73(%r15)
	movq	fixed_tl(%rip), %rax
	movq	%rax, 80(%r15)
	movq	fixed_td(%rip), %rax
	movq	%rax, 88(%r15)
	xorl	%ebx, %ebx
	jmp	.LBB0_65
.LBB0_27:                               #   in Loop: Header=BB0_67 Depth=2
	movl	$11, %ebx
	jmp	.LBB0_65
.LBB0_28:                               #   in Loop: Header=BB0_67 Depth=2
	movq	%rax, %rbp
	movl	%edx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB0_29:                               # %._crit_edge1488
                                        #   in Loop: Header=BB0_67 Depth=2
	movzwl	8(%rsp), %eax           # 2-byte Folded Reload
	movl	%eax, 48(%r15)
	testl	%eax, %eax
	je	.LBB0_31
# BB#30:                                #   in Loop: Header=BB0_67 Depth=2
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$10, %ebx
	jmp	.LBB0_32
.LBB0_31:                               #   in Loop: Header=BB0_67 Depth=2
	movl	1352(%r15), %ebx
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB0_32:                               # %.thread958.backedge.sink.split
                                        #   in Loop: Header=BB0_67 Depth=2
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB0_65
.LBB0_33:                               #   in Loop: Header=BB0_67 Depth=2
	movq	%r14, %r13
	subq	%rax, %r13
.LBB0_34:                               #   in Loop: Header=BB0_67 Depth=2
	movq	48(%rsp), %r12          # 8-byte Reload
	cmpq	%r14, %rax
	je	.LBB0_45
# BB#35:                                #   in Loop: Header=BB0_67 Depth=2
	movq	%rax, %r14
	testl	%r13d, %r13d
	jne	.LBB0_58
	jmp	.LBB0_328
.LBB0_45:                               #   in Loop: Header=BB0_67 Depth=2
	movq	%rdx, %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rcx, %rax
	je	.LBB0_57
# BB#46:                                #   in Loop: Header=BB0_67 Depth=2
	cmpq	%rdx, %rcx
	jae	.LBB0_55
# BB#47:                                #   in Loop: Header=BB0_67 Depth=2
	decq	%rax
	movq	%rax, %r13
	jmp	.LBB0_56
.LBB0_55:                               #   in Loop: Header=BB0_67 Depth=2
	subq	%rcx, %r14
	movq	%r14, %r13
.LBB0_56:                               #   in Loop: Header=BB0_67 Depth=2
	movq	%rcx, %r14
.LBB0_57:                               #   in Loop: Header=BB0_67 Depth=2
	testl	%r13d, %r13d
	jne	.LBB0_58
	jmp	.LBB0_328
.LBB0_36:                               #   in Loop: Header=BB0_67 Depth=2
	movq	%r14, %r13
	subq	%rax, %r13
.LBB0_37:                               #   in Loop: Header=BB0_67 Depth=2
	movq	48(%rsp), %r12          # 8-byte Reload
	cmpq	%r14, %rax
	je	.LBB0_48
# BB#38:                                #   in Loop: Header=BB0_67 Depth=2
	movq	%rax, %r14
	testl	%r13d, %r13d
	jne	.LBB0_62
	jmp	.LBB0_330
.LBB0_48:                               #   in Loop: Header=BB0_67 Depth=2
	movq	%rdx, %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rcx, %rax
	je	.LBB0_61
# BB#49:                                #   in Loop: Header=BB0_67 Depth=2
	cmpq	%rdx, %rcx
	jae	.LBB0_59
# BB#50:                                #   in Loop: Header=BB0_67 Depth=2
	decq	%rax
	movq	%rax, %r13
	jmp	.LBB0_60
.LBB0_59:                               #   in Loop: Header=BB0_67 Depth=2
	subq	%rcx, %r14
	movq	%r14, %r13
.LBB0_60:                               #   in Loop: Header=BB0_67 Depth=2
	movq	%rcx, %r14
.LBB0_61:                               #   in Loop: Header=BB0_67 Depth=2
	testl	%r13d, %r13d
	je	.LBB0_330
.LBB0_62:                               #   in Loop: Header=BB0_67 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB0_63
.LBB0_39:                               #   in Loop: Header=BB0_67 Depth=2
	movq	39904(%r15), %rax
	movq	%rax, %r13
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rcx, %r13
	je	.LBB0_93
# BB#40:                                #   in Loop: Header=BB0_67 Depth=2
	cmpq	%rax, %rcx
	jae	.LBB0_51
# BB#41:                                #   in Loop: Header=BB0_67 Depth=2
	decq	%r13
	jmp	.LBB0_52
.LBB0_51:                               #   in Loop: Header=BB0_67 Depth=2
	subq	%rcx, %r14
	movq	%r14, %r13
.LBB0_52:                               #   in Loop: Header=BB0_67 Depth=2
	testl	%r13d, %r13d
	movq	%rcx, %r14
	jne	.LBB0_58
	jmp	.LBB0_93
.LBB0_42:                               #   in Loop: Header=BB0_67 Depth=2
	movq	39904(%r15), %rax
	movq	%rax, %r13
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rcx, %r13
	je	.LBB0_111
# BB#43:                                #   in Loop: Header=BB0_67 Depth=2
	cmpq	%rax, %rcx
	jae	.LBB0_53
# BB#44:                                #   in Loop: Header=BB0_67 Depth=2
	decq	%r13
	jmp	.LBB0_54
.LBB0_53:                               #   in Loop: Header=BB0_67 Depth=2
	subq	%rcx, %r14
	movq	%r14, %r13
.LBB0_54:                               #   in Loop: Header=BB0_67 Depth=2
	testl	%r13d, %r13d
	movq	%rcx, %r14
	jne	.LBB0_63
	jmp	.LBB0_111
.LBB0_58:                               #   in Loop: Header=BB0_67 Depth=2
	movq	96(%rsp), %rax          # 8-byte Reload
	movb	(%rax), %al
	movb	%al, (%r14)
	incq	%r14
	decl	%r13d
	xorl	%ebx, %ebx
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_65
.LBB0_63:                               #   in Loop: Header=BB0_67 Depth=2
	cmpl	%edx, %r13d
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%edx, %ebp
	cmovbl	%r13d, %ebp
	movl	48(%r15), %eax
	cmpl	%ebp, %eax
	cmovbl	%eax, %ebp
	movq	%r14, %rdi
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	addq	%rbp, %rbx
	movq	%rbx, %rcx
	movq	16(%rsp), %rax          # 8-byte Reload
	subl	%ebp, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	addq	%rbp, %r14
	subl	%ebp, %r13d
	movl	48(%r15), %eax
	subl	%ebp, %eax
	movl	%eax, 48(%r15)
	jne	.LBB0_6
# BB#64:                                #   in Loop: Header=BB0_67 Depth=2
	movl	1352(%r15), %ebx
	movq	%rcx, %rbp
	jmp	.LBB0_65
	.p2align	4, 0x90
.LBB0_66:                               # %.thread958
                                        #   in Loop: Header=BB0_67 Depth=2
	cmpl	$15, %ebx
	ja	.LBB0_255
.LBB0_67:                               # %.thread958
                                        #   Parent Loop BB0_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_178 Depth 3
                                        #       Child Loop BB0_184 Depth 3
                                        #       Child Loop BB0_190 Depth 3
                                        #         Child Loop BB0_192 Depth 4
                                        #         Child Loop BB0_199 Depth 4
                                        #         Child Loop BB0_215 Depth 4
                                        #         Child Loop BB0_219 Depth 4
                                        #         Child Loop BB0_224 Depth 4
                                        #         Child Loop BB0_227 Depth 4
                                        #       Child Loop BB0_113 Depth 3
                                        #       Child Loop BB0_18 Depth 3
                                        #       Child Loop BB0_72 Depth 3
                                        #       Child Loop BB0_95 Depth 3
                                        #       Child Loop BB0_88 Depth 3
                                        #       Child Loop BB0_142 Depth 3
                                        #         Child Loop BB0_150 Depth 4
                                        #       Child Loop BB0_83 Depth 3
                                        #       Child Loop BB0_130 Depth 3
                                        #       Child Loop BB0_239 Depth 3
	movl	%ebx, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_68:                               #   in Loop: Header=BB0_67 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	$8, %eax
	jb	.LBB0_70
# BB#69:                                #   in Loop: Header=BB0_67 Depth=2
	addl	$-8, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	decq	24(%rsp)                # 8-byte Folded Spill
.LBB0_70:                               #   in Loop: Header=BB0_67 Depth=2
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r14
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	movq	39896(%r15), %rdx
	cmpq	%rdx, %r14
	je	.LBB0_72
# BB#71:                                #   in Loop: Header=BB0_67 Depth=2
	movq	%rbx, %rbp
	jmp	.LBB0_75
	.p2align	4, 0x90
.LBB0_72:                               # %.lr.ph1351
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_74
# BB#73:                                #   in Loop: Header=BB0_72 Depth=3
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_74:                               # %.backedge.i821.backedge
                                        #   in Loop: Header=BB0_72 Depth=3
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	movq	39896(%r15), %rdx
	cmpq	%rdx, %r14
	movq	%rbp, %rbx
	je	.LBB0_72
.LBB0_75:                               # %inflate_flush.exit822
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%r14, 39904(%r15)
	movq	39912(%r15), %rcx
	cmpq	%r14, %rcx
	jae	.LBB0_77
# BB#76:                                #   in Loop: Header=BB0_67 Depth=2
	movq	%r14, %r13
	subq	%rcx, %r13
	decq	%r13
	cmpq	%r14, %rcx
	je	.LBB0_78
	jmp	.LBB0_241
	.p2align	4, 0x90
.LBB0_77:                               #   in Loop: Header=BB0_67 Depth=2
	movq	%rdx, %r13
	subq	%rcx, %r13
	cmpq	%r14, %rcx
	jne	.LBB0_241
.LBB0_78:                               #   in Loop: Header=BB0_67 Depth=2
	movl	1352(%r15), %ecx
	movl	%ecx, 40(%r15)
	movl	$8, %ebx
	cmpl	$8, %ecx
	je	.LBB0_66
	jmp	.LBB0_246
.LBB0_79:                               # %.loopexit2079.loopexit
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%r12, 48(%rsp)          # 8-byte Spill
	jmp	.LBB0_236
.LBB0_80:                               # %.thread958._crit_edge1756
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movl	64(%r15), %eax
	jmp	.LBB0_237
.LBB0_81:                               #   in Loop: Header=BB0_67 Depth=2
	movl	56(%r15), %eax
	cmpl	%eax, (%rsp)            # 4-byte Folded Reload
	jae	.LBB0_126
# BB#82:                                # %.lr.ph1383.preheader
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_83:                               # %.lr.ph1383
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%edi, %edi
	je	.LBB0_288
# BB#84:                                #   in Loop: Header=BB0_83 Depth=3
	decl	%edi
	movzbl	(%rsi), %edx
	incq	%rsi
	movq	(%rsp), %rcx            # 8-byte Reload
	shlq	%cl, %rdx
	orq	%rdx, %rbp
	addl	$8, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	cmpl	%eax, %ecx
	jb	.LBB0_83
	jmp	.LBB0_127
.LBB0_85:                               # %.thread958._crit_edge1751
                                        #   in Loop: Header=BB0_67 Depth=2
	movl	64(%r15), %eax
	movq	(%rsp), %rdx            # 8-byte Reload
	jmp	.LBB0_128
.LBB0_86:                               #   in Loop: Header=BB0_67 Depth=2
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpl	%eax, (%rsp)            # 4-byte Folded Reload
	jae	.LBB0_135
# BB#87:                                # %.lr.ph1361.preheader
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_88:                               # %.lr.ph1361
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%edi, %edi
	je	.LBB0_293
# BB#89:                                #   in Loop: Header=BB0_88 Depth=3
	decl	%edi
	movzbl	(%rsi), %edx
	incq	%rsi
	movq	(%rsp), %rcx            # 8-byte Reload
	shlq	%cl, %rdx
	orq	%rdx, %rbp
	addl	$8, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	cmpl	%eax, %ecx
	jb	.LBB0_88
	jmp	.LBB0_136
.LBB0_90:                               # %.thread958._crit_edge
                                        #   in Loop: Header=BB0_67 Depth=2
	movl	60(%r15), %edx
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB0_137
.LBB0_91:                               #   in Loop: Header=BB0_67 Depth=2
	testl	%r13d, %r13d
	jne	.LBB0_58
# BB#92:                                #   in Loop: Header=BB0_67 Depth=2
	cmpq	(%r12), %r14
	je	.LBB0_39
.LBB0_93:                               # %.thread966
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%r12, %rax
	movq	40(%rsp), %rbx          # 8-byte Reload
	cmovbeq	%rbx, %rax
	movl	(%rax), %r12d
	subl	%ebp, %r12d
	movl	32(%r15), %eax
	cmpl	%eax, %r12d
	cmovael	%eax, %r12d
	subl	%r12d, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	memcpy
	movq	%rbp, %rdx
	addq	%r12, %rdx
	addq	24(%r15), %r12
	movq	%r12, 24(%r15)
	movq	39896(%r15), %r14
	cmpq	%r14, %rdx
	je	.LBB0_95
# BB#94:                                #   in Loop: Header=BB0_67 Depth=2
	movq	%r12, %rbp
	jmp	.LBB0_98
.LBB0_95:                               # %.lr.ph1355
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx), %rax
	cmpq	%rdx, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_97
# BB#96:                                #   in Loop: Header=BB0_95 Depth=3
	movq	%rsi, (%rbx)
	movq	%rsi, %rax
.LBB0_97:                               # %.backedge.i835.backedge
                                        #   in Loop: Header=BB0_95 Depth=3
	cmpq	%rax, %rsi
	movq	48(%rsp), %rax          # 8-byte Reload
	cmovbeq	%rbx, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %rdx
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	movq	39896(%r15), %r14
	cmpq	%r14, %rdx
	movq	%rbp, %r12
	je	.LBB0_95
.LBB0_98:                               # %inflate_flush.exit836
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%rdx, 39904(%r15)
	movq	39912(%r15), %rax
	cmpq	%rdx, %rax
	jae	.LBB0_33
# BB#99:                                #   in Loop: Header=BB0_67 Depth=2
	movq	%rdx, %r13
	subq	%rax, %r13
	decq	%r13
	jmp	.LBB0_34
.LBB0_100:                              # %.preheader1025
                                        #   in Loop: Header=BB0_67 Depth=2
	cmpl	$2, (%rsp)              # 4-byte Folded Reload
	ja	.LBB0_168
# BB#101:                               # %.lr.ph1496
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	16(%rsp), %rsi          # 8-byte Reload
	testl	%esi, %esi
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_323
# BB#102:                               # %._crit_edge1497.loopexit
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	(%rsp), %rcx            # 8-byte Reload
	leal	8(%rcx), %eax
	movzbl	(%rbp), %edx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	orq	%rdx, %rcx
	incq	%rbp
	decl	%esi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%eax, %edx
	jmp	.LBB0_169
.LBB0_103:                              # %.preheader1026
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	$16, %eax
	jae	.LBB0_173
# BB#104:                               # %.lr.ph1487
                                        #   in Loop: Header=BB0_67 Depth=2
	movl	%eax, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_333
# BB#105:                               #   in Loop: Header=BB0_67 Depth=2
	leal	-1(%rax), %edx
	leaq	1(%rbp), %rax
	movzbl	(%rbp), %esi
	shlq	%cl, %rsi
	orq	%rsi, 8(%rsp)           # 8-byte Folded Spill
	addq	$8, %rcx
	cmpq	$16, %rcx
	jae	.LBB0_28
# BB#106:                               # %.lr.ph1487.1
                                        #   in Loop: Header=BB0_67 Depth=2
	testl	%edx, %edx
	je	.LBB0_332
# BB#107:                               #   in Loop: Header=BB0_67 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	addl	$-2, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movzbl	1(%rbp), %eax
	addq	$2, %rbp
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rax
	orq	%rax, 8(%rsp)           # 8-byte Folded Spill
	jmp	.LBB0_29
.LBB0_108:                              #   in Loop: Header=BB0_67 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	testl	%edx, %edx
	je	.LBB0_313
# BB#109:                               #   in Loop: Header=BB0_67 Depth=2
	testl	%r13d, %r13d
	jne	.LBB0_63
# BB#110:                               #   in Loop: Header=BB0_67 Depth=2
	cmpq	(%r12), %r14
	je	.LBB0_42
.LBB0_111:                              # %.thread
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%r12, %rax
	movq	40(%rsp), %rbx          # 8-byte Reload
	cmovbeq	%rbx, %rax
	movl	(%rax), %r12d
	subl	%ebp, %r12d
	movl	32(%r15), %eax
	cmpl	%eax, %r12d
	cmovael	%eax, %r12d
	subl	%r12d, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	memcpy
	movq	%rbp, %rdx
	addq	%r12, %rdx
	addq	24(%r15), %r12
	movq	%r12, 24(%r15)
	movq	39896(%r15), %r14
	cmpq	%r14, %rdx
	je	.LBB0_113
# BB#112:                               #   in Loop: Header=BB0_67 Depth=2
	movq	%r12, %rbp
	jmp	.LBB0_116
.LBB0_113:                              # %.lr.ph1481
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx), %rax
	cmpq	%rdx, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_115
# BB#114:                               #   in Loop: Header=BB0_113 Depth=3
	movq	%rsi, (%rbx)
	movq	%rsi, %rax
.LBB0_115:                              # %.backedge.i926.backedge
                                        #   in Loop: Header=BB0_113 Depth=3
	cmpq	%rax, %rsi
	movq	48(%rsp), %rax          # 8-byte Reload
	cmovbeq	%rbx, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %rdx
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	movq	39896(%r15), %r14
	cmpq	%r14, %rdx
	movq	%rbp, %r12
	je	.LBB0_113
.LBB0_116:                              # %inflate_flush.exit927
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%rdx, 39904(%r15)
	movq	39912(%r15), %rax
	cmpq	%rdx, %rax
	jae	.LBB0_36
# BB#117:                               #   in Loop: Header=BB0_67 Depth=2
	movq	%rdx, %r13
	subq	%rax, %r13
	decq	%r13
	jmp	.LBB0_37
.LBB0_118:                              # %.preheader1034
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	$14, %eax
	jae	.LBB0_174
# BB#119:                               # %.lr.ph1404
                                        #   in Loop: Header=BB0_67 Depth=2
	movl	%eax, %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_339
# BB#120:                               #   in Loop: Header=BB0_67 Depth=2
	leal	-1(%rcx), %esi
	leaq	1(%rbp), %rdi
	movzbl	(%rbp), %edx
	movl	%eax, %ecx
	shlq	%cl, %rdx
	orq	8(%rsp), %rdx           # 8-byte Folded Reload
	leaq	8(%rax), %rbx
	movq	%rbx, %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	cmpq	$14, %rbx
	jae	.LBB0_123
# BB#121:                               # %.lr.ph1404.1
                                        #   in Loop: Header=BB0_67 Depth=2
	testl	%esi, %esi
	je	.LBB0_338
# BB#122:                               #   in Loop: Header=BB0_67 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	addl	$-2, %edi
	movzbl	1(%rbp), %esi
	addq	$2, %rbp
	movq	(%rsp), %rcx            # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rsi
	orq	%rsi, %rdx
	addq	$16, %rax
	movl	%edi, %esi
	movq	%rbp, %rdi
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB0_123:                              # %._crit_edge1405.loopexit
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%rdx, %rcx
	movl	%esi, %ebp
	jmp	.LBB0_175
.LBB0_124:                              # %.preheader1033
                                        #   in Loop: Header=BB0_67 Depth=2
	movl	48(%r15), %ecx
	movl	52(%r15), %eax
	shrl	$10, %ecx
	addl	$4, %ecx
	cmpl	%ecx, %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	jb	.LBB0_178
	jmp	.LBB0_183
.LBB0_125:                              # %.thread958..preheader1031_crit_edge
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movl	52(%r15), %esi
	jmp	.LBB0_188
.LBB0_126:                              #   in Loop: Header=BB0_67 Depth=2
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB0_127:                              # %._crit_edge1384
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movzwl	inflate_mask(%rax,%rax), %ecx
	andl	%ebp, %ecx
	addl	%ecx, 48(%r15)
	movl	%eax, %ecx
	shrq	%cl, %rbp
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	(%rsp), %rdx            # 8-byte Reload
	subl	%eax, %edx
	movzbl	73(%r15), %eax
	movl	%eax, 64(%r15)
	movq	88(%r15), %rcx
	movq	%rcx, 56(%r15)
	movl	$3, 40(%r15)
.LBB0_128:                              #   in Loop: Header=BB0_67 Depth=2
	movq	%rdx, (%rsp)            # 8-byte Spill
	cmpl	%eax, %edx
	jae	.LBB0_132
# BB#129:                               # %.lr.ph1394.preheader
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_130:                              # %.lr.ph1394
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%esi, %esi
	je	.LBB0_276
# BB#131:                               #   in Loop: Header=BB0_130 Depth=3
	decl	%esi
	movzbl	(%rbp), %edx
	incq	%rbp
	movq	(%rsp), %rcx            # 8-byte Reload
	shlq	%cl, %rdx
	orq	%rdx, 8(%rsp)           # 8-byte Folded Spill
	addl	$8, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	cmpl	%eax, %ecx
	jb	.LBB0_130
	jmp	.LBB0_133
.LBB0_132:                              #   in Loop: Header=BB0_67 Depth=2
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
.LBB0_133:                              # %._crit_edge1395
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	56(%r15), %rdx
	movl	%eax, %eax
	movzwl	inflate_mask(%rax,%rax), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, %rsi
	andq	%rsi, %rax
	movzbl	1(%rdx,%rax,4), %ecx
	shrq	%cl, %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	(%rsp), %rsi            # 8-byte Reload
	subl	%ecx, %esi
	movq	%rsi, (%rsp)            # 8-byte Spill
	movzbl	(%rdx,%rax,4), %ecx
	testb	$16, %cl
	je	.LBB0_4
# BB#134:                               #   in Loop: Header=BB0_67 Depth=2
	andl	$15, %ecx
	movl	%ecx, 56(%r15)
	movzwl	2(%rdx,%rax,4), %eax
	movl	%eax, 60(%r15)
	movl	$4, %ebx
	jmp	.LBB0_65
.LBB0_135:                              #   in Loop: Header=BB0_67 Depth=2
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB0_136:                              # %._crit_edge
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movzwl	inflate_mask(%rax,%rax), %edx
	andl	%ebp, %edx
	addl	60(%r15), %edx
	movl	%edx, 60(%r15)
	movl	%eax, %ecx
	shrq	%cl, %rbp
	movq	(%rsp), %rcx            # 8-byte Reload
	subl	%eax, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movl	$5, 40(%r15)
.LBB0_137:                              #   in Loop: Header=BB0_67 Depth=2
	movq	%r14, %rcx
	subq	32(%rsp), %rcx          # 8-byte Folded Reload
	cmpl	%edx, %ecx
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jae	.LBB0_139
# BB#138:                               #   in Loop: Header=BB0_67 Depth=2
	movq	(%r12), %rdi
	movl	%edx, %eax
	subq	%rcx, %rax
	jmp	.LBB0_140
.LBB0_139:                              #   in Loop: Header=BB0_67 Depth=2
	movl	%edx, %eax
	movq	%r14, %rdi
.LBB0_140:                              #   in Loop: Header=BB0_67 Depth=2
	xorl	%ebx, %ebx
	cmpl	$0, 48(%r15)
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_65
# BB#141:                               # %.lr.ph1374.preheader
                                        #   in Loop: Header=BB0_67 Depth=2
	subq	%rax, %rdi
	movq	%r12, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_142:                              # %.lr.ph1374
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_150 Depth 4
	testl	%r13d, %r13d
	jne	.LBB0_167
# BB#143:                               #   in Loop: Header=BB0_142 Depth=3
	cmpq	(%r12), %r14
	je	.LBB0_146
.LBB0_144:                              # %.thread963
                                        #   in Loop: Header=BB0_142 Depth=3
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	%r12, %rax
	cmovbeq	40(%rsp), %rax          # 8-byte Folded Reload
	movl	(%rax), %r13d
	subl	%ebp, %r13d
	movl	32(%r15), %eax
	cmpl	%eax, %r13d
	cmovael	%eax, %r13d
	subl	%r13d, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%r13, %rdx
	callq	memcpy
	movq	%rbp, %rsi
	addq	%r13, %rsi
	addq	24(%r15), %r13
	movq	%r13, 24(%r15)
	movq	39896(%r15), %r14
	cmpq	%r14, %rsi
	je	.LBB0_149
# BB#145:                               #   in Loop: Header=BB0_142 Depth=3
	movq	%r13, %r12
	jmp	.LBB0_153
.LBB0_146:                              #   in Loop: Header=BB0_142 Depth=3
	movq	39904(%r15), %rax
	movq	%rax, %r13
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rcx, %r13
	je	.LBB0_144
# BB#147:                               #   in Loop: Header=BB0_142 Depth=3
	cmpq	%rax, %rcx
	jae	.LBB0_161
# BB#148:                               #   in Loop: Header=BB0_142 Depth=3
	decq	%r13
	jmp	.LBB0_162
.LBB0_149:                              # %.lr.ph1368.preheader
                                        #   in Loop: Header=BB0_142 Depth=3
	movq	40(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_150:                              # %.lr.ph1368
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        #       Parent Loop BB0_142 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rbp), %rax
	cmpq	%rsi, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_152
# BB#151:                               #   in Loop: Header=BB0_150 Depth=4
	movq	%rsi, (%rbp)
	movq	%rsi, %rax
.LBB0_152:                              # %.backedge.i849.backedge
                                        #   in Loop: Header=BB0_150 Depth=4
	cmpq	%rax, %rsi
	movq	48(%rsp), %rax          # 8-byte Reload
	cmovbeq	%rbp, %rax
	movl	(%rax), %r12d
	subl	%esi, %r12d
	movl	32(%r15), %eax
	cmpl	%eax, %r12d
	cmovael	%eax, %r12d
	subl	%r12d, %eax
	movl	%eax, 32(%r15)
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	memcpy
	leaq	7128(%r15,%r12), %rsi
	addq	24(%r15), %r12
	movq	%r12, 24(%r15)
	movq	39896(%r15), %r14
	cmpq	%r14, %rsi
	movq	%r12, %r13
	je	.LBB0_150
.LBB0_153:                              # %inflate_flush.exit850
                                        #   in Loop: Header=BB0_142 Depth=3
	movq	%rsi, 39904(%r15)
	movq	39912(%r15), %rax
	cmpq	%rsi, %rax
	jae	.LBB0_155
# BB#154:                               #   in Loop: Header=BB0_142 Depth=3
	movq	%rsi, %r13
	subq	%rax, %r13
	decq	%r13
	jmp	.LBB0_156
.LBB0_155:                              #   in Loop: Header=BB0_142 Depth=3
	movq	%r14, %r13
	subq	%rax, %r13
.LBB0_156:                              #   in Loop: Header=BB0_142 Depth=3
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	56(%rsp), %rdi          # 8-byte Reload
	cmpq	%r14, %rax
	je	.LBB0_158
# BB#157:                               #   in Loop: Header=BB0_142 Depth=3
	movq	%rax, %r14
	testl	%r13d, %r13d
	jne	.LBB0_166
	jmp	.LBB0_286
.LBB0_158:                              #   in Loop: Header=BB0_142 Depth=3
	movq	%rsi, %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rcx, %rax
	je	.LBB0_165
# BB#159:                               #   in Loop: Header=BB0_142 Depth=3
	cmpq	%rsi, %rcx
	jae	.LBB0_163
# BB#160:                               #   in Loop: Header=BB0_142 Depth=3
	decq	%rax
	movq	%rax, %r13
	jmp	.LBB0_164
.LBB0_161:                              #   in Loop: Header=BB0_142 Depth=3
	subq	%rcx, %r14
	movq	%r14, %r13
.LBB0_162:                              #   in Loop: Header=BB0_142 Depth=3
	testl	%r13d, %r13d
	movq	%rcx, %r14
	jne	.LBB0_167
	jmp	.LBB0_144
.LBB0_163:                              #   in Loop: Header=BB0_142 Depth=3
	subq	%rcx, %r14
	movq	%r14, %r13
.LBB0_164:                              #   in Loop: Header=BB0_142 Depth=3
	movq	%rcx, %r14
.LBB0_165:                              #   in Loop: Header=BB0_142 Depth=3
	testl	%r13d, %r13d
	je	.LBB0_286
.LBB0_166:                              #   in Loop: Header=BB0_142 Depth=3
	movq	48(%rsp), %r12          # 8-byte Reload
.LBB0_167:                              #   in Loop: Header=BB0_142 Depth=3
	movb	(%rdi), %al
	incq	%rdi
	movb	%al, (%r14)
	incq	%r14
	decl	%r13d
	cmpq	39896(%r15), %rdi
	cmoveq	32(%rsp), %rdi          # 8-byte Folded Reload
	movl	48(%r15), %eax
	decl	%eax
	movl	%eax, 48(%r15)
	jne	.LBB0_142
	jmp	.LBB0_65
.LBB0_168:                              #   in Loop: Header=BB0_67 Depth=2
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rsp), %rdx            # 8-byte Reload
.LBB0_169:                              # %._crit_edge1497
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%rcx, %rax
	shrq	$3, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	addl	$-3, %edx
	movq	%rdx, (%rsp)            # 8-byte Spill
	testb	$1, %al
	movl	$8, %ecx
	movl	$15, %edx
	cmovnel	%edx, %ecx
	movl	%ecx, 1352(%r15)
	shrb	%al
	andb	$3, %al
	cmpb	$1, %al
	je	.LBB0_16
# BB#170:                               # %._crit_edge1497
                                        #   in Loop: Header=BB0_67 Depth=2
	cmpb	$2, %al
	je	.LBB0_27
# BB#171:                               # %._crit_edge1497
                                        #   in Loop: Header=BB0_67 Depth=2
	cmpb	$3, %al
	je	.LBB0_256
# BB#172:                               #   in Loop: Header=BB0_67 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, %ecx
	andl	$7, %ecx
	shrq	%cl, 8(%rsp)            # 8-byte Folded Spill
	subl	%ecx, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$9, %ebx
	jmp	.LBB0_65
.LBB0_173:                              #   in Loop: Header=BB0_67 Depth=2
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_29
.LBB0_174:                              #   in Loop: Header=BB0_67 Depth=2
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB0_175:                              # %._crit_edge1405
                                        #   in Loop: Header=BB0_67 Depth=2
	movl	%ecx, %eax
	andl	$16383, %eax            # imm = 0x3FFF
	movl	%eax, 48(%r15)
	movl	%ecx, %eax
	andl	$30, %eax
	cmpl	$29, %eax
	ja	.LBB0_318
# BB#176:                               # %._crit_edge1405
                                        #   in Loop: Header=BB0_67 Depth=2
	movl	%ecx, %eax
	andl	$960, %eax              # imm = 0x3C0
	cmpl	$929, %eax              # imm = 0x3A1
	jae	.LBB0_318
# BB#177:                               # %.preheader1033.thread
                                        #   in Loop: Header=BB0_67 Depth=2
	shrq	$14, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	(%rsp), %rsi            # 8-byte Reload
	addl	$-14, %esi
	movl	$0, 52(%r15)
	movl	$12, 40(%r15)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_178:                              # %.preheader
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rsi, %rcx
	cmpl	$2, %esi
	ja	.LBB0_181
# BB#179:                               # %.lr.ph1414
                                        #   in Loop: Header=BB0_178 Depth=3
	testl	%ebp, %ebp
	je	.LBB0_271
# BB#180:                               # %._crit_edge1415.loopexit
                                        #   in Loop: Header=BB0_178 Depth=3
	leal	8(%rcx), %edx
	movzbl	(%rdi), %esi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rsi
	movq	8(%rsp), %rbx           # 8-byte Reload
	orq	%rsi, %rbx
	incq	%rdi
	decl	%ebp
	movl	%edx, %esi
	jmp	.LBB0_182
	.p2align	4, 0x90
.LBB0_181:                              #   in Loop: Header=BB0_178 Depth=3
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rcx, %rsi
.LBB0_182:                              # %._crit_edge1415
                                        #   in Loop: Header=BB0_178 Depth=3
	movl	%ebx, %ecx
	andl	$7, %ecx
	leal	1(%rax), %edx
	movl	%edx, 52(%r15)
	movl	%eax, %eax
	movsbq	border(%rax), %rax
	movl	%ecx, 56(%r15,%rax,4)
	shrq	$3, %rbx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	addl	$-3, %esi
	movl	48(%r15), %ecx
	movl	52(%r15), %eax
	shrl	$10, %ecx
	addl	$4, %ecx
	cmpl	%ecx, %eax
	jb	.LBB0_178
.LBB0_183:                              # %.preheader1032
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	cmpl	$18, %eax
	ja	.LBB0_185
	.p2align	4, 0x90
.LBB0_184:                              # %.lr.ph1429
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	1(%rax), %ecx
	movl	%ecx, 52(%r15)
	movl	%eax, %eax
	movsbq	border(%rax), %rax
	movl	$0, 56(%r15,%rax,4)
	movl	52(%r15), %eax
	cmpl	$19, %eax
	jb	.LBB0_184
.LBB0_185:                              # %._crit_edge1430
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	120(%rsp), %rbx         # 8-byte Reload
	movl	$7, (%rbx)
	movl	$0, 64(%rsp)
	subq	$8, %rsp
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	movl	$19, %esi
	movl	$19, %edx
	movl	$0, %ecx
	movl	$0, %r8d
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	120(%rsp), %r9          # 8-byte Reload
	leaq	72(%rsp), %rax
	pushq	%rax
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)               # 8-byte Folded Reload
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	callq	huft_build
	addq	$32, %rsp
.Lcfi27:
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB0_7
# BB#186:                               #   in Loop: Header=BB0_67 Depth=2
	cmpl	$0, (%rbx)
	je	.LBB0_7
# BB#187:                               #   in Loop: Header=BB0_67 Depth=2
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movl	$0, 52(%r15)
	movl	$13, 40(%r15)
	xorl	%esi, %esi
.LBB0_188:                              # %.preheader1031
                                        #   in Loop: Header=BB0_67 Depth=2
	movl	48(%r15), %r12d
	movl	%r12d, %r10d
	andl	$31, %r10d
	shrl	$5, %r12d
	andl	$31, %r12d
	leal	258(%r10,%r12), %r8d
	cmpl	%r8d, %esi
	movq	%r10, 56(%rsp)          # 8-byte Spill
	jae	.LBB0_229
# BB#189:                               # %.lr.ph1457.preheader
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_190
.LBB0_229:                              #   in Loop: Header=BB0_67 Depth=2
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB0_230:                              # %._crit_edge1458
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	$0, (%rax)
	movl	$0, 84(%rsp)
	leal	257(%r10), %ebp
	movl	$9, 80(%rsp)
	movl	$6, 76(%rsp)
	subq	$8, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	movl	$257, %edx              # imm = 0x101
	movl	$cplens, %ecx
	movl	$cplext, %r8d
	movq	104(%rsp), %rdi         # 8-byte Reload
	movl	%ebp, %esi
	leaq	72(%rsp), %r9
	leaq	92(%rsp), %rax
	pushq	%rax
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)               # 8-byte Folded Reload
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	leaq	104(%rsp), %rax
	pushq	%rax
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	callq	huft_build
	addq	$32, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB0_298
# BB#231:                               # %._crit_edge1458
                                        #   in Loop: Header=BB0_67 Depth=2
	movl	80(%rsp), %ebx
	testl	%ebx, %ebx
	je	.LBB0_298
# BB#232:                               #   in Loop: Header=BB0_67 Depth=2
	incl	%r12d
	movl	%ebp, %eax
	leaq	56(%r15,%rax,4), %rdi
	subq	$8, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	xorl	%edx, %edx
	movl	$cpdist, %ecx
	movl	$cpdext, %r8d
	movl	%r12d, %esi
	leaq	136(%rsp), %r9
	leaq	92(%rsp), %rax
	pushq	%rax
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)               # 8-byte Folded Reload
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	leaq	100(%rsp), %rax
	pushq	%rax
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	callq	huft_build
	addq	$32, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB0_298
# BB#233:                               #   in Loop: Header=BB0_67 Depth=2
	movl	76(%rsp), %eax
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	je	.LBB0_235
# BB#234:                               #   in Loop: Header=BB0_67 Depth=2
	testl	%eax, %eax
	je	.LBB0_298
.LBB0_235:                              #   in Loop: Header=BB0_67 Depth=2
	movb	%bl, 72(%r15)
	movb	%al, 73(%r15)
	movq	64(%rsp), %rax
	movq	%rax, 80(%r15)
	movq	128(%rsp), %rax
	movq	%rax, 88(%r15)
	movl	$0, 40(%r15)
.LBB0_236:                              # %.loopexit2079
                                        #   in Loop: Header=BB0_67 Depth=2
	movzbl	72(%r15), %eax
	movl	%eax, 64(%r15)
	movq	80(%r15), %rcx
	movq	%rcx, 56(%r15)
	movl	$1, 40(%r15)
.LBB0_237:                              #   in Loop: Header=BB0_67 Depth=2
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%rsp)            # 8-byte Spill
	cmpl	%eax, %ecx
	jae	.LBB0_9
# BB#238:                               # %.lr.ph1473.preheader
                                        #   in Loop: Header=BB0_67 Depth=2
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_239:                              # %.lr.ph1473
                                        #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_67 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%esi, %esi
	je	.LBB0_261
# BB#240:                               #   in Loop: Header=BB0_239 Depth=3
	decl	%esi
	movzbl	(%rbp), %edx
	incq	%rbp
	movq	(%rsp), %rcx            # 8-byte Reload
	shlq	%cl, %rdx
	orq	%rdx, 8(%rsp)           # 8-byte Folded Spill
	addl	$8, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	cmpl	%eax, %ecx
	jb	.LBB0_239
	jmp	.LBB0_10
.LBB0_241:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, 8(%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	cmpq	%rcx, %r14
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%r14d, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r14
	jne	.LBB0_253
	.p2align	4, 0x90
.LBB0_242:                              # %.lr.ph1347
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_244
# BB#243:                               #   in Loop: Header=BB0_242 Depth=1
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_244:                              # %.backedge.i814.backedge
                                        #   in Loop: Header=BB0_242 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	movq	%rbp, %rbx
	je	.LBB0_242
# BB#245:
	xorl	%eax, %eax
	jmp	.LBB0_253
.LBB0_246:
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 1360(%r15)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 1356(%r15)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 8(%r15)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%r15)
	movq	%r14, 39912(%r15)
	movq	%rbp, 24(%r15)
	movl	$1, %eax
	cmpq	%rdx, %r14
	jne	.LBB0_253
# BB#247:                               # %.lr.ph1343.preheader
	movq	%rdx, %r14
	movq	40(%rsp), %r13          # 8-byte Reload
	jmp	.LBB0_249
	.p2align	4, 0x90
.LBB0_248:                              # %.backedge.i807.backedge..lr.ph1343_crit_edge
                                        #   in Loop: Header=BB0_249 Depth=1
	movq	(%r13), %rdx
.LBB0_249:                              # %.lr.ph1343
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r14, %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_251
# BB#250:                               #   in Loop: Header=BB0_249 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rsi, (%rax)
	movq	%rsi, %rdx
.LBB0_251:                              # %.backedge.i807.backedge
                                        #   in Loop: Header=BB0_249 Depth=1
	cmpq	%rdx, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %r14
	movq	%rbx, %rbp
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	je	.LBB0_248
# BB#252:
	movl	$1, %eax
.LBB0_253:                              # %.thread960.sink.split
	movq	%r14, 39904(%r15)
.LBB0_254:                              # %.thread960
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_255:                              # %.loopexit.loopexit
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB0_256:                              # %.loopexit
	movl	$17, 40(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r14
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	movl	$-2, %eax
	cmpq	39896(%r15), %r14
	jne	.LBB0_253
	.p2align	4, 0x90
.LBB0_257:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_259
# BB#258:                               #   in Loop: Header=BB0_257 Depth=1
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_259:                              # %.backedge.i800.backedge
                                        #   in Loop: Header=BB0_257 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	movq	%rbp, %rbx
	je	.LBB0_257
# BB#260:
	movl	$-2, %eax
	jmp	.LBB0_253
.LBB0_261:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r14
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r14
	jne	.LBB0_253
	.p2align	4, 0x90
.LBB0_262:                              # %.lr.ph1319
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_264
# BB#263:                               #   in Loop: Header=BB0_262 Depth=1
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_264:                              # %.backedge.i877.backedge
                                        #   in Loop: Header=BB0_262 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	movq	%rbp, %rbx
	je	.LBB0_262
# BB#265:
	xorl	%eax, %eax
	jmp	.LBB0_253
.LBB0_266:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r14
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r14
	jne	.LBB0_253
	.p2align	4, 0x90
.LBB0_267:                              # %.lr.ph1307
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_269
# BB#268:                               #   in Loop: Header=BB0_267 Depth=1
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_269:                              # %.backedge.i919.backedge
                                        #   in Loop: Header=BB0_267 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	movq	%rbp, %rbx
	je	.LBB0_267
# BB#270:
	xorl	%eax, %eax
	jmp	.LBB0_253
.LBB0_271:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 1360(%r15)
	movl	%ecx, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rdi, (%r15)
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r14
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r14
	jne	.LBB0_253
.LBB0_272:                              # %.lr.ph1303
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_274
# BB#273:                               #   in Loop: Header=BB0_272 Depth=1
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_274:                              # %.backedge.i933.backedge
                                        #   in Loop: Header=BB0_272 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	movq	%rbp, %rbx
	je	.LBB0_272
# BB#275:
	xorl	%eax, %eax
	jmp	.LBB0_253
.LBB0_276:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r14
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r14
	jne	.LBB0_253
.LBB0_277:                              # %.lr.ph1327
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_279
# BB#278:                               #   in Loop: Header=BB0_277 Depth=1
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_279:                              # %.backedge.i863.backedge
                                        #   in Loop: Header=BB0_277 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	movq	%rbp, %rbx
	je	.LBB0_277
# BB#280:
	xorl	%eax, %eax
	jmp	.LBB0_253
.LBB0_281:
	movq	%rbp, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%r8, (%r15)
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r14
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r14
	jne	.LBB0_253
.LBB0_282:                              # %.lr.ph1311
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_284
# BB#283:                               #   in Loop: Header=BB0_282 Depth=1
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_284:                              # %.backedge.i912.backedge
                                        #   in Loop: Header=BB0_282 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	movq	%rbp, %rbx
	je	.LBB0_282
# BB#285:
	xorl	%eax, %eax
	jmp	.LBB0_253
.LBB0_286:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r14, 39912(%r15)
	cmpq	%r14, %rsi
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %r14d
	subl	%esi, %r14d
	movl	32(%r15), %eax
	cmpl	%eax, %r14d
	cmovael	%eax, %r14d
	subl	%r14d, %eax
	movl	%eax, 32(%r15)
	movq	%r12, %rdi
	movq	%rsi, %rbp
	movq	%r14, %rdx
	callq	memcpy
	movq	%rbp, %rcx
	addq	%r14, %rcx
	addq	24(%r15), %r14
	movq	%r14, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %rcx
	je	.LBB0_303
# BB#287:
	movq	%rcx, %r14
	jmp	.LBB0_253
.LBB0_288:
	movq	%rbp, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rsi, (%r15)
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r14
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r14
	jne	.LBB0_253
.LBB0_289:                              # %.lr.ph1323
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_291
# BB#290:                               #   in Loop: Header=BB0_289 Depth=1
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_291:                              # %.backedge.i870.backedge
                                        #   in Loop: Header=BB0_289 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	movq	%rbp, %rbx
	je	.LBB0_289
# BB#292:
	xorl	%eax, %eax
	jmp	.LBB0_253
.LBB0_293:
	movq	%rbp, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rsi, (%r15)
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r14
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r14
	jne	.LBB0_253
.LBB0_294:                              # %.lr.ph1331
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_296
# BB#295:                               #   in Loop: Header=BB0_294 Depth=1
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_296:                              # %.backedge.i856.backedge
                                        #   in Loop: Header=BB0_294 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	movq	%rbp, %rbx
	je	.LBB0_294
# BB#297:
	xorl	%eax, %eax
	jmp	.LBB0_253
.LBB0_298:                              # %.thread961
	movl	$17, 40(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, 8(%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %r14d
	subl	%ebp, %r14d
	movl	32(%r15), %eax
	cmpl	%eax, %r14d
	cmovael	%eax, %r14d
	subl	%r14d, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	memcpy
	addq	%r14, %rbp
	addq	24(%r15), %r14
	movq	%r14, 24(%r15)
	cmpq	39896(%r15), %rbp
	jne	.LBB0_302
.LBB0_299:                              # %.lr.ph1466
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%rbp, %rax
	jne	.LBB0_301
# BB#300:                               #   in Loop: Header=BB0_299 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
.LBB0_301:                              # %.backedge.i884.backedge
                                        #   in Loop: Header=BB0_299 Depth=1
	movq	32(%rsp), %rsi          # 8-byte Reload
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %rbp
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %rbp
	movq	%rbx, %r14
	je	.LBB0_299
.LBB0_302:                              # %inflate_flush.exit885
	movq	%rbp, 39904(%r15)
	movl	$-3, %eax
	jmp	.LBB0_254
.LBB0_303:                              # %.lr.ph1335.preheader
	movq	%rbx, %r12
.LBB0_304:                              # %.lr.ph1335
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%rcx, %rax
	jne	.LBB0_306
# BB#305:                               #   in Loop: Header=BB0_304 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
.LBB0_306:                              # %.backedge.i842.backedge
                                        #   in Loop: Header=BB0_304 Depth=1
	movq	32(%rsp), %rsi          # 8-byte Reload
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%esi, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbx), %rcx
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	cmpq	39896(%r15), %rcx
	movq	%rbx, %r14
	je	.LBB0_304
.LBB0_307:
	movq	%rcx, %r14
	xorl	%eax, %eax
	jmp	.LBB0_253
.LBB0_308:
	movl	$17, 40(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, 8(%r15)
	movq	%r8, (%r15)
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r14
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	movl	$-3, %eax
	cmpq	39896(%r15), %r14
	jne	.LBB0_253
.LBB0_309:                              # %.lr.ph1315
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_311
# BB#310:                               #   in Loop: Header=BB0_309 Depth=1
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_311:                              # %.backedge.i905.backedge
                                        #   in Loop: Header=BB0_309 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	movq	%rbp, %rbx
	je	.LBB0_309
# BB#312:
	movl	$-3, %eax
	jmp	.LBB0_253
.LBB0_313:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r14
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r14
	jne	.LBB0_253
.LBB0_314:                              # %.lr.ph1287
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_316
# BB#315:                               #   in Loop: Header=BB0_314 Depth=1
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_316:                              # %.backedge.i898.backedge
                                        #   in Loop: Header=BB0_314 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	movq	%rbp, %rbx
	je	.LBB0_314
# BB#317:
	xorl	%eax, %eax
	jmp	.LBB0_253
.LBB0_318:
	movl	$17, 40(%r15)
	movq	%rcx, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movl	%ebp, 8(%r15)
	movq	%rdi, (%r15)
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r14
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	movl	$-3, %eax
	cmpq	39896(%r15), %r14
	jne	.LBB0_253
.LBB0_319:                              # %.lr.ph1299
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_321
# BB#320:                               #   in Loop: Header=BB0_319 Depth=1
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_321:                              # %.backedge.i940.backedge
                                        #   in Loop: Header=BB0_319 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	movq	%rbp, %rbx
	je	.LBB0_319
# BB#322:
	movl	$-3, %eax
	jmp	.LBB0_253
.LBB0_323:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r14
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r14
	jne	.LBB0_253
.LBB0_324:                              # %.lr.ph1279
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_326
# BB#325:                               #   in Loop: Header=BB0_324 Depth=1
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_326:                              # %.backedge.i.backedge
                                        #   in Loop: Header=BB0_324 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	movq	%rbp, %rbx
	je	.LBB0_324
# BB#327:
	xorl	%eax, %eax
	jmp	.LBB0_253
.LBB0_328:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, 8(%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	movq	%r14, 39912(%r15)
	cmpq	%r14, %rdx
	movq	%r12, %rax
	cmovbeq	40(%rsp), %rax          # 8-byte Folded Reload
	movl	(%rax), %ebx
	subl	%edx, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rdx, %rbp
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %rcx
	addq	%rbx, %rcx
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %rcx
	je	.LBB0_344
# BB#329:
	movq	%rcx, %r14
	jmp	.LBB0_253
.LBB0_330:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 1360(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 1356(%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, 8(%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	movq	%r14, 39912(%r15)
	cmpq	%r14, %rdx
	movq	%r12, %rax
	cmovbeq	40(%rsp), %rax          # 8-byte Folded Reload
	movl	(%rax), %ebx
	subl	%edx, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rdi
	movq	%rdx, %rbp
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %rcx
	addq	%rbx, %rcx
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %rcx
	je	.LBB0_348
# BB#331:
	movq	%rcx, %r14
	jmp	.LBB0_253
.LBB0_332:
	movq	%rax, %rbp
.LBB0_333:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 1360(%r15)
	movl	%ecx, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r14
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r14
	jne	.LBB0_253
.LBB0_334:                              # %.lr.ph1283
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_336
# BB#335:                               #   in Loop: Header=BB0_334 Depth=1
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_336:                              # %.backedge.i891.backedge
                                        #   in Loop: Header=BB0_334 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	movq	%rbp, %rbx
	je	.LBB0_334
# BB#337:
	xorl	%eax, %eax
	jmp	.LBB0_253
.LBB0_338:
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %rbp
.LBB0_339:
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 1360(%r15)
	movl	%eax, 1356(%r15)
	movl	$0, 8(%r15)
	movq	%rbp, (%r15)
	movq	%r14, 39912(%r15)
	movq	39904(%r15), %rbp
	movq	24(%r15), %rdi
	cmpq	%r14, %rbp
	movq	%r12, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	cmovbeq	%r13, %rax
	movl	(%rax), %ebx
	subl	%ebp, %ebx
	movl	32(%r15), %eax
	cmpl	%eax, %ebx
	cmovael	%eax, %ebx
	subl	%ebx, %eax
	movl	%eax, 32(%r15)
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%rbp, %r14
	addq	%rbx, %r14
	addq	24(%r15), %rbx
	movq	%rbx, 24(%r15)
	xorl	%eax, %eax
	cmpq	39896(%r15), %r14
	jne	.LBB0_253
.LBB0_340:                              # %.lr.ph1295
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	cmpq	%r14, %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	jne	.LBB0_342
# BB#341:                               #   in Loop: Header=BB0_340 Depth=1
	movq	%rsi, (%r13)
	movq	%rsi, %rax
.LBB0_342:                              # %.backedge.i947.backedge
                                        #   in Loop: Header=BB0_340 Depth=1
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r13, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %r14
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %r14
	movq	%rbp, %rbx
	je	.LBB0_340
# BB#343:
	xorl	%eax, %eax
	jmp	.LBB0_253
.LBB0_344:                              # %.lr.ph1339.preheader
	movq	40(%rsp), %r14          # 8-byte Reload
.LBB0_345:                              # %.lr.ph1339
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	%rcx, %rax
	jne	.LBB0_347
# BB#346:                               #   in Loop: Header=BB0_345 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r14)
.LBB0_347:                              # %.backedge.i828.backedge
                                        #   in Loop: Header=BB0_345 Depth=1
	movq	32(%rsp), %rsi          # 8-byte Reload
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %rcx
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %rcx
	movq	%rbp, %rbx
	je	.LBB0_345
	jmp	.LBB0_307
.LBB0_348:                              # %.lr.ph1291.preheader
	movq	40(%rsp), %r14          # 8-byte Reload
.LBB0_349:                              # %.lr.ph1291
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	cmpq	%rcx, %rax
	jne	.LBB0_351
# BB#350:                               #   in Loop: Header=BB0_349 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r14)
.LBB0_351:                              # %.backedge.i954.backedge
                                        #   in Loop: Header=BB0_349 Depth=1
	movq	32(%rsp), %rsi          # 8-byte Reload
	cmpq	%rax, %rsi
	movq	%r12, %rax
	cmovbeq	%r14, %rax
	movl	(%rax), %ebp
	subl	%esi, %ebp
	movl	32(%r15), %eax
	cmpl	%eax, %ebp
	cmovael	%eax, %ebp
	subl	%ebp, %eax
	movl	%eax, 32(%r15)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	leaq	7128(%r15,%rbp), %rcx
	addq	24(%r15), %rbp
	movq	%rbp, 24(%r15)
	cmpq	39896(%r15), %rcx
	movq	%rbp, %rbx
	je	.LBB0_349
	jmp	.LBB0_307
.Lfunc_end0:
	.size	nsis_inflate, .Lfunc_end0-nsis_inflate
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_79
	.quad	.LBB0_80
	.quad	.LBB0_81
	.quad	.LBB0_85
	.quad	.LBB0_86
	.quad	.LBB0_90
	.quad	.LBB0_91
	.quad	.LBB0_68
	.quad	.LBB0_100
	.quad	.LBB0_103
	.quad	.LBB0_108
	.quad	.LBB0_118
	.quad	.LBB0_124
	.quad	.LBB0_125
	.quad	.LBB0_255
	.quad	.LBB0_70

	.text
	.p2align	4, 0x90
	.type	huft_build,@function
huft_build:                             # @huft_build
	.cfi_startproc
# BB#0:                                 # %.preheader229.preheader
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 272
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%r9, -8(%rsp)           # 8-byte Spill
	movq	%rcx, %r15
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -64(%rsp)
	movaps	%xmm0, -80(%rsp)
	movaps	%xmm0, -96(%rsp)
	movaps	%xmm0, -112(%rsp)
	leal	-1(%rsi), %r9d
	movl	%esi, %ebp
	andl	$3, %ebp
	movl	%esi, %eax
	movq	%rdi, %rcx
	je	.LBB1_3
# BB#1:                                 # %.preheader229.prol.preheader
	negl	%ebp
	movl	%esi, %eax
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader229.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %ebx
	addq	$4, %rcx
	incl	-112(%rsp,%rbx,4)
	decl	%eax
	incl	%ebp
	jne	.LBB1_2
.LBB1_3:                                # %.preheader229.prol.loopexit
	cmpl	$3, %r9d
	jb	.LBB1_5
	.p2align	4, 0x90
.LBB1_4:                                # %.preheader229
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %ebp
	incl	-112(%rsp,%rbp,4)
	movl	4(%rcx), %ebp
	incl	-112(%rsp,%rbp,4)
	movl	8(%rcx), %ebp
	incl	-112(%rsp,%rbp,4)
	movl	12(%rcx), %ebp
	incl	-112(%rsp,%rbp,4)
	addq	$16, %rcx
	addl	$-4, %eax
	jne	.LBB1_4
.LBB1_5:
	movq	272(%rsp), %rax
	cmpl	%esi, -112(%rsp)
	jne	.LBB1_7
# BB#6:
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movq	$0, (%rcx)
	movl	$0, (%rax)
	xorl	%eax, %eax
	jmp	.LBB1_92
.LBB1_7:
	movl	(%rax), %ebx
	movl	-108(%rsp), %ebp
	movl	$1, %ecx
	testl	%ebp, %ebp
	jne	.LBB1_22
# BB#8:
	movl	$2, %ecx
	cmpl	$0, -104(%rsp)
	jne	.LBB1_22
# BB#9:
	movl	$3, %ecx
	cmpl	$0, -100(%rsp)
	jne	.LBB1_22
# BB#10:
	movl	$4, %ecx
	cmpl	$0, -96(%rsp)
	jne	.LBB1_22
# BB#11:
	movl	$5, %ecx
	cmpl	$0, -92(%rsp)
	jne	.LBB1_22
# BB#12:
	movl	$6, %ecx
	cmpl	$0, -88(%rsp)
	jne	.LBB1_22
# BB#13:
	movl	$7, %ecx
	cmpl	$0, -84(%rsp)
	jne	.LBB1_22
# BB#14:
	movl	$8, %ecx
	cmpl	$0, -80(%rsp)
	jne	.LBB1_22
# BB#15:
	movl	$9, %ecx
	cmpl	$0, -76(%rsp)
	jne	.LBB1_22
# BB#16:
	movl	$10, %ecx
	cmpl	$0, -72(%rsp)
	jne	.LBB1_22
# BB#17:
	movl	$11, %ecx
	cmpl	$0, -68(%rsp)
	jne	.LBB1_22
# BB#18:
	movl	$12, %ecx
	cmpl	$0, -64(%rsp)
	jne	.LBB1_22
# BB#19:
	movl	$13, %ecx
	cmpl	$0, -60(%rsp)
	jne	.LBB1_22
# BB#20:
	movl	$14, %ecx
	cmpl	$0, -56(%rsp)
	jne	.LBB1_22
# BB#21:
	cmpl	$1, -52(%rsp)
	movl	$15, %ecx
	adcl	$0, %ecx
.LBB1_22:
	cmpl	%ecx, %ebx
	cmovbl	%ecx, %ebx
	movl	$15, %r14d
	cmpl	$0, -52(%rsp)
	jne	.LBB1_36
# BB#23:
	movl	$14, %r14d
	cmpl	$0, -56(%rsp)
	jne	.LBB1_36
# BB#24:
	movl	$13, %r14d
	cmpl	$0, -60(%rsp)
	jne	.LBB1_36
# BB#25:
	movl	$12, %r14d
	cmpl	$0, -64(%rsp)
	jne	.LBB1_36
# BB#26:
	movl	$11, %r14d
	cmpl	$0, -68(%rsp)
	jne	.LBB1_36
# BB#27:
	movl	$10, %r14d
	cmpl	$0, -72(%rsp)
	jne	.LBB1_36
# BB#28:
	movl	$9, %r14d
	cmpl	$0, -76(%rsp)
	jne	.LBB1_36
# BB#29:
	movl	$8, %r14d
	cmpl	$0, -80(%rsp)
	jne	.LBB1_36
# BB#30:
	movl	$7, %r14d
	cmpl	$0, -84(%rsp)
	jne	.LBB1_36
# BB#31:
	movl	$6, %r14d
	cmpl	$0, -88(%rsp)
	jne	.LBB1_36
# BB#32:
	movl	$5, %r14d
	cmpl	$0, -92(%rsp)
	jne	.LBB1_36
# BB#33:
	movl	$4, %r14d
	cmpl	$0, -96(%rsp)
	jne	.LBB1_36
# BB#34:
	movl	$3, %r14d
	cmpl	$0, -100(%rsp)
	jne	.LBB1_36
# BB#35:
	xorl	%r9d, %r9d
	testl	%ebp, %ebp
	setne	%r9b
	cmpl	$0, -104(%rsp)
	movl	$2, %r14d
	cmovel	%r9d, %r14d
.LBB1_36:
	cmpl	%r14d, %ebx
	movl	%ebx, %r11d
	cmoval	%r14d, %r11d
	movl	%r11d, (%rax)
	movl	$1, %eax
	shll	%cl, %eax
	cmpl	%r14d, %ecx
	jae	.LBB1_40
# BB#37:                                # %.lr.ph327.preheader
	movl	%ecx, %ebp
	.p2align	4, 0x90
.LBB1_38:                               # %.lr.ph327
                                        # =>This Inner Loop Header: Depth=1
	subl	-112(%rsp,%rbp,4), %eax
	js	.LBB1_47
# BB#39:                                #   in Loop: Header=BB1_38 Depth=1
	addl	%eax, %eax
	incq	%rbp
	cmpl	%r14d, %ebp
	jb	.LBB1_38
.LBB1_40:                               # %._crit_edge328
	movl	%r14d, %r13d
	movl	%eax, %ebp
	subl	-112(%rsp,%r13,4), %ebp
	js	.LBB1_47
# BB#41:
	movl	%ebp, -36(%rsp)         # 4-byte Spill
	movq	%rbx, %r12
	movl	%eax, -112(%rsp,%r13,4)
	movl	$0, 36(%rsp)
	movl	%r14d, %r9d
	decl	%r9d
	je	.LBB1_50
# BB#42:                                # %.lr.ph323.preheader
	leal	-2(%r14), %r10d
	movl	%r9d, %eax
	andl	$3, %eax
	je	.LBB1_48
# BB#43:                                # %.lr.ph323.prol.preheader
	negl	%eax
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_44:                               # %.lr.ph323.prol
                                        # =>This Inner Loop Header: Depth=1
	addl	-108(%rsp,%rbx), %ebp
	movl	%ebp, 40(%rsp,%rbx)
	decl	%r9d
	addq	$4, %rbx
	incl	%eax
	jne	.LBB1_44
# BB#45:                                # %.lr.ph323.prol.loopexit.unr-lcssa
	leaq	40(%rsp,%rbx), %rax
	leaq	-108(%rsp,%rbx), %rbx
	cmpl	$3, %r10d
	jae	.LBB1_49
	jmp	.LBB1_50
.LBB1_47:
	movl	$-3, %eax
	jmp	.LBB1_92
.LBB1_48:
	leaq	-108(%rsp), %rbx
	leaq	40(%rsp), %rax
	xorl	%ebp, %ebp
	cmpl	$3, %r10d
	jb	.LBB1_50
	.p2align	4, 0x90
.LBB1_49:                               # %.lr.ph323
                                        # =>This Inner Loop Header: Depth=1
	addl	(%rbx), %ebp
	movl	%ebp, (%rax)
	addl	4(%rbx), %ebp
	movl	%ebp, 4(%rax)
	addl	8(%rbx), %ebp
	movl	%ebp, 8(%rax)
	addl	12(%rbx), %ebp
	movl	%ebp, 12(%rax)
	addq	$16, %rbx
	addq	$16, %rax
	addl	$-4, %r9d
	jne	.LBB1_49
.LBB1_50:                               # %.preheader227.preheader
	xorl	%eax, %eax
	movq	%r12, %r9
	.p2align	4, 0x90
.LBB1_51:                               # %.preheader227
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax,4), %ebp
	testq	%rbp, %rbp
	je	.LBB1_53
# BB#52:                                #   in Loop: Header=BB1_51 Depth=1
	movl	32(%rsp,%rbp,4), %r9d
	leal	1(%r9), %ebx
	movl	%ebx, 32(%rsp,%rbp,4)
	movl	%eax, huft_build.v(,%r9,4)
	movq	%r12, %r9
.LBB1_53:                               #   in Loop: Header=BB1_51 Depth=1
	incq	%rax
	cmpl	%esi, %eax
	jb	.LBB1_51
# BB#54:
	movl	%edx, -32(%rsp)         # 4-byte Spill
	movq	%r15, (%rsp)            # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movl	32(%rsp,%r13,4), %eax
	movl	$0, 32(%rsp)
	movq	$0, 96(%rsp)
	cmpl	%r14d, %ecx
	movq	288(%rsp), %rdx
	jbe	.LBB1_56
.LBB1_55:                               # %._crit_edge318
	movl	-36(%rsp), %eax         # 4-byte Reload
	testl	%eax, %eax
	movl	$-5, %ecx
	cmovel	%eax, %ecx
	xorl	%eax, %eax
	cmpl	$1, %r14d
	cmovnel	%ecx, %eax
	jmp	.LBB1_92
.LBB1_56:                               # %.lr.ph317
	movl	%r11d, %esi
	negl	%esi
	leaq	huft_build.v(,%rax,4), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$huft_build.v, %eax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movl	%ecx, %r8d
	leaq	-108(%rsp,%r8,4), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movl	%r14d, %eax
	notl	%eax
	notl	%r9d
	cmpl	%r9d, %eax
	cmoval	%eax, %r9d
	movl	$-1, %r13d
	xorl	%r10d, %r10d
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	movq	%r9, -24(%rsp)          # 8-byte Spill
.LBB1_57:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_59 Depth 2
                                        #       Child Loop BB1_60 Depth 3
                                        #         Child Loop BB1_61 Depth 4
                                        #           Child Loop BB1_65 Depth 5
                                        #       Child Loop BB1_81 Depth 3
                                        #       Child Loop BB1_83 Depth 3
                                        #       Child Loop BB1_87 Depth 3
	movl	-112(%rsp,%r8,4), %eax
	movl	%eax, -116(%rsp)        # 4-byte Spill
	testl	%eax, %eax
	je	.LBB1_89
# BB#58:                                # %.preheader226.lr.ph
                                        #   in Loop: Header=BB1_57 Depth=1
	leal	-1(%r8), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movl	%eax, -28(%rsp)         # 4-byte Spill
.LBB1_59:                               # %.preheader226
                                        #   Parent Loop BB1_57 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_60 Depth 3
                                        #         Child Loop BB1_61 Depth 4
                                        #           Child Loop BB1_65 Depth 5
                                        #       Child Loop BB1_81 Depth 3
                                        #       Child Loop BB1_83 Depth 3
                                        #       Child Loop BB1_87 Depth 3
	leal	(%rsi,%r11), %eax
	movslq	%eax, %rcx
	cmpq	%rcx, %r8
	jle	.LBB1_73
.LBB1_60:                               # %.lr.ph264
                                        #   Parent Loop BB1_57 Depth=1
                                        #     Parent Loop BB1_59 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_61 Depth 4
                                        #           Child Loop BB1_65 Depth 5
	movl	%esi, %edi
	movl	%r13d, %r9d
	.p2align	4, 0x90
.LBB1_61:                               #   Parent Loop BB1_57 Depth=1
                                        #     Parent Loop BB1_59 Depth=2
                                        #       Parent Loop BB1_60 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB1_65 Depth 5
	movl	%eax, %esi
	movl	%r14d, %ebp
	subl	%esi, %ebp
	cmpl	%r11d, %ebp
	cmoval	%r11d, %ebp
	movl	%r8d, %eax
	subl	%esi, %eax
	movl	$1, %ebx
	movl	%eax, %ecx
	shll	%cl, %ebx
	subl	-116(%rsp), %ebx        # 4-byte Folded Reload
	jbe	.LBB1_67
# BB#62:                                #   in Loop: Header=BB1_61 Depth=4
	cmpl	%ebp, %eax
	jae	.LBB1_67
# BB#63:                                # %.preheader
                                        #   in Loop: Header=BB1_61 Depth=4
	incl	%eax
	cmpl	%ebp, %eax
	jae	.LBB1_67
# BB#64:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_61 Depth=4
	movq	-16(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_65:                               # %.lr.ph
                                        #   Parent Loop BB1_57 Depth=1
                                        #     Parent Loop BB1_59 Depth=2
                                        #       Parent Loop BB1_60 Depth=3
                                        #         Parent Loop BB1_61 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	addl	%ebx, %ebx
	subl	(%rcx), %ebx
	jbe	.LBB1_67
# BB#66:                                #   in Loop: Header=BB1_65 Depth=5
	incl	%eax
	addq	$4, %rcx
	cmpl	%ebp, %eax
	jb	.LBB1_65
	.p2align	4, 0x90
.LBB1_67:                               # %.critedge
                                        #   in Loop: Header=BB1_61 Depth=4
	movl	$1, %r10d
	movl	%eax, %ecx
	shll	%cl, %r10d
	movl	(%rdx), %ecx
	movq	%rdx, %rbx
	leal	(%rcx,%r10), %edx
	cmpl	$1440, %edx             # imm = 0x5A0
	ja	.LBB1_91
# BB#68:                                #   in Loop: Header=BB1_61 Depth=4
	leal	1(%r9), %r13d
	movq	280(%rsp), %rbp
	leaq	(%rbp,%rcx,4), %rbp
	movslq	%r13d, %rcx
	movq	%rbp, 96(%rsp,%rcx,8)
	movl	%edx, (%rbx)
	testl	%ecx, %ecx
	movq	%rbx, %rdx
	jne	.LBB1_70
# BB#69:                                #   in Loop: Header=BB1_61 Depth=4
	movq	-8(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
	leal	(%rsi,%r11), %eax
	movslq	%eax, %rcx
	xorl	%r9d, %r9d
	cmpq	%rcx, %r8
	movl	%esi, %edi
	jg	.LBB1_61
	jmp	.LBB1_71
.LBB1_70:                               # %.outer
                                        #   in Loop: Header=BB1_60 Depth=3
	movl	%r12d, 32(%rsp,%rcx,4)
	movl	%r12d, %edx
	movl	%edi, %ecx
	shrl	%cl, %edx
	movslq	%r9d, %rcx
	movq	96(%rsp,%rcx,8), %rcx
	movl	%ebp, %r15d
	subl	%ecx, %r15d
	shrl	$2, %r15d
	subl	%edx, %r15d
	movb	%al, (%rcx,%rdx,4)
	movb	%r11b, 1(%rcx,%rdx,4)
	movw	%r15w, 2(%rcx,%rdx,4)
	movq	288(%rsp), %rdx
	leal	(%rsi,%r11), %eax
	movslq	%eax, %rcx
	cmpq	%rcx, %r8
	jg	.LBB1_60
	jmp	.LBB1_72
.LBB1_71:                               #   in Loop: Header=BB1_59 Depth=2
	xorl	%r13d, %r13d
.LBB1_72:                               # %.outer._crit_edge
                                        #   in Loop: Header=BB1_59 Depth=2
	movq	-24(%rsp), %r9          # 8-byte Reload
.LBB1_73:                               # %.outer._crit_edge
                                        #   in Loop: Header=BB1_59 Depth=2
	movl	%r8d, %eax
	subl	%esi, %eax
	movb	$-64, %dil
	movq	-48(%rsp), %rdx         # 8-byte Reload
	cmpq	24(%rsp), %rdx          # 8-byte Folded Reload
	jae	.LBB1_80
# BB#74:                                #   in Loop: Header=BB1_59 Depth=2
	movl	(%rdx), %r15d
	movl	%r15d, %ecx
	subl	-32(%rsp), %ecx         # 4-byte Folded Reload
	jae	.LBB1_77
# BB#75:                                #   in Loop: Header=BB1_59 Depth=2
	cmpl	$256, %r15d             # imm = 0x100
	jb	.LBB1_78
# BB#76:                                #   in Loop: Header=BB1_59 Depth=2
	movb	$96, %dil
	jmp	.LBB1_79
.LBB1_77:                               #   in Loop: Header=BB1_59 Depth=2
	movq	8(%rsp), %rdx           # 8-byte Reload
	movb	(%rdx,%rcx,2), %dil
	addb	$80, %dil
	movq	(%rsp), %rdx            # 8-byte Reload
	movw	(%rdx,%rcx,2), %r15w
	movq	-48(%rsp), %rdx         # 8-byte Reload
	jmp	.LBB1_79
.LBB1_78:                               #   in Loop: Header=BB1_59 Depth=2
	xorl	%edi, %edi
.LBB1_79:                               #   in Loop: Header=BB1_59 Depth=2
	addq	$4, %rdx
	movq	%rdx, -48(%rsp)         # 8-byte Spill
.LBB1_80:                               #   in Loop: Header=BB1_59 Depth=2
	movl	$1, %ebx
	movl	%eax, %ecx
	shll	%cl, %ebx
	movl	%r12d, %edx
	movl	%esi, %ecx
	shrl	%cl, %edx
	cmpl	%r10d, %edx
	jae	.LBB1_82
	.p2align	4, 0x90
.LBB1_81:                               # %.lr.ph285
                                        #   Parent Loop BB1_57 Depth=1
                                        #     Parent Loop BB1_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edx, %ecx
	movb	%dil, (%rbp,%rcx,4)
	movb	%al, 1(%rbp,%rcx,4)
	movw	%r15w, 2(%rbp,%rcx,4)
	addl	%ebx, %edx
	cmpl	%r10d, %edx
	jb	.LBB1_81
.LBB1_82:                               # %._crit_edge286.preheader
                                        #   in Loop: Header=BB1_59 Depth=2
	decl	-116(%rsp)              # 4-byte Folded Spill
	movl	-28(%rsp), %eax         # 4-byte Reload
	.p2align	4, 0x90
.LBB1_83:                               # %._crit_edge286
                                        #   Parent Loop BB1_57 Depth=1
                                        #     Parent Loop BB1_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r12d, %ecx
	movl	%eax, %r12d
	xorl	%ecx, %r12d
	movl	%eax, %edx
	shrl	%edx
	testl	%ecx, %eax
	movl	%edx, %eax
	jne	.LBB1_83
# BB#84:                                # %.preheader225
                                        #   in Loop: Header=BB1_59 Depth=2
	movl	$1, %edx
	movl	%esi, %ecx
	shll	%cl, %edx
	decl	%edx
	andl	%r12d, %edx
	movslq	%r13d, %rax
	cmpl	32(%rsp,%rax,4), %edx
	jne	.LBB1_86
# BB#85:                                #   in Loop: Header=BB1_59 Depth=2
	movl	%esi, %eax
	jmp	.LBB1_88
.LBB1_86:                               # %.lr.ph289.preheader
                                        #   in Loop: Header=BB1_59 Depth=2
	leaq	28(%rsp), %rcx
	leaq	(%rcx,%rax,4), %rdi
	.p2align	4, 0x90
.LBB1_87:                               # %.lr.ph289
                                        #   Parent Loop BB1_57 Depth=1
                                        #     Parent Loop BB1_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%esi, %eax
	subl	%r11d, %eax
	leal	1(%r9,%rsi), %ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	decl	%edx
	andl	%r12d, %edx
	decl	%r13d
	cmpl	(%rdi), %edx
	leaq	-4(%rdi), %rdi
	movl	%eax, %esi
	jne	.LBB1_87
.LBB1_88:                               # %.thread.loopexit
                                        #   in Loop: Header=BB1_59 Depth=2
	cmpl	$0, -116(%rsp)          # 4-byte Folded Reload
	movl	%eax, %esi
	movq	288(%rsp), %rdx
	jne	.LBB1_59
	jmp	.LBB1_90
.LBB1_89:                               #   in Loop: Header=BB1_57 Depth=1
	movl	%esi, %eax
.LBB1_90:                               # %.thread._crit_edge
                                        #   in Loop: Header=BB1_57 Depth=1
	addq	$4, -16(%rsp)           # 8-byte Folded Spill
	cmpq	16(%rsp), %r8           # 8-byte Folded Reload
	leaq	1(%r8), %r8
	movl	%eax, %esi
	jl	.LBB1_57
	jmp	.LBB1_55
.LBB1_91:
	movl	$-4, %eax
.LBB1_92:                               # %.loopexit
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	huft_build, .Lfunc_end1-huft_build
	.cfi_endproc

	.type	fixed_built,@object     # @fixed_built
	.local	fixed_built
	.comm	fixed_built,1,1
	.type	nsis_inflate.lc,@object # @nsis_inflate.lc
	.local	nsis_inflate.lc
	.comm	nsis_inflate.lc,1152,16
	.type	cplens,@object          # @cplens
	.section	.rodata,"a",@progbits
	.p2align	4
cplens:
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	13                      # 0xd
	.short	15                      # 0xf
	.short	17                      # 0x11
	.short	19                      # 0x13
	.short	23                      # 0x17
	.short	27                      # 0x1b
	.short	31                      # 0x1f
	.short	35                      # 0x23
	.short	43                      # 0x2b
	.short	51                      # 0x33
	.short	59                      # 0x3b
	.short	67                      # 0x43
	.short	83                      # 0x53
	.short	99                      # 0x63
	.short	115                     # 0x73
	.short	131                     # 0x83
	.short	163                     # 0xa3
	.short	195                     # 0xc3
	.short	227                     # 0xe3
	.short	258                     # 0x102
	.short	0                       # 0x0
	.short	0                       # 0x0
	.size	cplens, 62

	.type	cplext,@object          # @cplext
	.p2align	4
cplext:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	0                       # 0x0
	.short	112                     # 0x70
	.short	112                     # 0x70
	.size	cplext, 62

	.type	fixed_tl,@object        # @fixed_tl
	.local	fixed_tl
	.comm	fixed_tl,8,8
	.type	fixed_bl,@object        # @fixed_bl
	.data
	.p2align	2
fixed_bl:
	.long	9                       # 0x9
	.size	fixed_bl, 4

	.type	fixed_mem,@object       # @fixed_mem
	.local	fixed_mem
	.comm	fixed_mem,2176,16
	.type	cpdist,@object          # @cpdist
	.section	.rodata,"a",@progbits
	.p2align	4
cpdist:
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	7                       # 0x7
	.short	9                       # 0x9
	.short	13                      # 0xd
	.short	17                      # 0x11
	.short	25                      # 0x19
	.short	33                      # 0x21
	.short	49                      # 0x31
	.short	65                      # 0x41
	.short	97                      # 0x61
	.short	129                     # 0x81
	.short	193                     # 0xc1
	.short	257                     # 0x101
	.short	385                     # 0x181
	.short	513                     # 0x201
	.short	769                     # 0x301
	.short	1025                    # 0x401
	.short	1537                    # 0x601
	.short	2049                    # 0x801
	.short	3073                    # 0xc01
	.short	4097                    # 0x1001
	.short	6145                    # 0x1801
	.short	8193                    # 0x2001
	.short	12289                   # 0x3001
	.short	16385                   # 0x4001
	.short	24577                   # 0x6001
	.size	cpdist, 60

	.type	cpdext,@object          # @cpdext
	.p2align	4
cpdext:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	13                      # 0xd
	.size	cpdext, 60

	.type	fixed_td,@object        # @fixed_td
	.local	fixed_td
	.comm	fixed_td,8,8
	.type	fixed_bd,@object        # @fixed_bd
	.data
	.p2align	2
fixed_bd:
	.long	5                       # 0x5
	.size	fixed_bd, 4

	.type	border,@object          # @border
	.section	.rodata,"a",@progbits
	.p2align	4
border:
	.ascii	"\020\021\022\000\b\007\t\006\n\005\013\004\f\003\r\002\016\001\017"
	.size	border, 19

	.type	inflate_mask,@object    # @inflate_mask
	.p2align	4
inflate_mask:
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	3                       # 0x3
	.short	7                       # 0x7
	.short	15                      # 0xf
	.short	31                      # 0x1f
	.short	63                      # 0x3f
	.short	127                     # 0x7f
	.short	255                     # 0xff
	.short	511                     # 0x1ff
	.short	1023                    # 0x3ff
	.short	2047                    # 0x7ff
	.short	4095                    # 0xfff
	.short	8191                    # 0x1fff
	.short	16383                   # 0x3fff
	.short	32767                   # 0x7fff
	.short	65535                   # 0xffff
	.size	inflate_mask, 34

	.type	huft_build.v,@object    # @huft_build.v
	.local	huft_build.v
	.comm	huft_build.v,1152,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
