	.text
	.file	"itop.bc"
	.globl	itop
	.p2align	4, 0x90
	.type	itop,@function
itop:                                   # @itop
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movl	$2, %edi
	xorl	%eax, %eax
	callq	palloc
	testq	%rax, %rax
	je	.LBB0_4
# BB#1:
	movl	%ebx, %ecx
	shrl	$31, %ecx
	movb	%cl, 6(%rax)
	movl	%ebx, %edx
	negl	%edx
	testb	%cl, %cl
	cmovel	%ebx, %edx
	movq	%rax, %rsi
	addq	$8, %rsi
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movw	%dx, (%rcx)
	addq	$2, %rcx
	sarl	$16, %edx
	jne	.LBB0_2
# BB#3:
	subl	%esi, %ecx
	shrl	%ecx
	movw	%cx, 4(%rax)
	movq	%rax, %rdi
	popq	%rbx
	jmp	presult                 # TAILCALL
.LBB0_4:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	itop, .Lfunc_end0-itop
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
