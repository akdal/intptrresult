	.text
	.file	"main.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 64
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	callq	showinst
	movl	$.L.str, %edi
	movl	$.L.str.1, %esi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	movl	$0, 8(%rsp)
	je	.LBB0_4
# BB#1:                                 # %.preheader12.preheader
	movl	$p+18, %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader12
                                        # =>This Inner Loop Header: Depth=1
	leaq	-18(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-17(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-16(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-15(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-14(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-13(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-12(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-11(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-10(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-9(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-8(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-7(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-6(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-5(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-4(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-3(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-2(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	leaq	-1(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fscanf
	incq	%r15
	addq	$19, %rbx
	cmpq	$19, %r15
	jl	.LBB0_2
# BB#3:                                 # %.critedge42
	movl	$19, 12(%rsp)
	movl	$.L.str.3, %esi
	movl	$mymove, %edx
	movl	$mk, %ecx
	movl	$uk, %r8d
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	movl	$.L.str.4, %esi
	movl	$opn, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	movl	$.L.str.4, %esi
	movl	$opn+4, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	movl	$.L.str.4, %esi
	movl	$opn+8, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	movl	$.L.str.4, %esi
	movl	$opn+12, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	movl	$.L.str.4, %esi
	movl	$opn+16, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	movl	$.L.str.4, %esi
	movl	$opn+20, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	movl	$.L.str.4, %esi
	movl	$opn+24, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	movl	$.L.str.4, %esi
	movl	$opn+28, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	movl	$.L.str.4, %esi
	movl	$opn+32, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fscanf
	movl	$9, 8(%rsp)
	movq	%r14, %rdi
	callq	fclose
	movl	$3, %eax
	subl	mymove(%rip), %eax
	movl	%eax, umove(%rip)
	movl	$.L.str, %edi
	callq	unlink
	movl	$1, play(%rip)
	movl	$0, pass(%rip)
	movl	$-1, mik(%rip)
	movl	$-1, mjk(%rip)
	movl	$-1, uik(%rip)
	movl	$-1, ujk(%rip)
	movl	$rd, %edi
	callq	seed
	jmp	.LBB0_8
.LBB0_4:                                # %.preheader11
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, opn(%rip)
	movq	%rax, opn+8(%rip)
	movl	$1, opn+16(%rip)
	movl	$1, opn+20(%rip)
	movl	$1, opn+24(%rip)
	movl	$1, opn+28(%rip)
	movl	$1, opn+32(%rip)
	movl	$0, opn+16(%rip)
	movl	$p, %edi
	xorl	%esi, %esi
	movl	$361, %edx              # imm = 0x169
	callq	memset
	movl	$19, 12(%rsp)
	movl	$19, 8(%rsp)
	movl	$0, mk(%rip)
	movl	$0, uk(%rip)
	movl	$1, play(%rip)
	movl	$0, pass(%rip)
	movl	$-1, mik(%rip)
	movl	$-1, mjk(%rip)
	movl	$-1, uik(%rip)
	movl	$-1, ujk(%rip)
	movl	$rd, %edi
	callq	seed
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	printf
	leaq	8(%rsp), %rsi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	scanf
	movq	stdin(%rip), %rdi
	callq	_IO_getc
	movl	8(%rsp), %edi
	callq	sethand
	callq	showboard
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	leaq	17(%rsp), %rsi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	scanf
	cmpb	$98, 17(%rsp)
	jne	.LBB0_6
# BB#5:
	movl	$1, mymove(%rip)
	movl	$2, umove(%rip)
	cmpl	$0, 8(%rsp)
	jne	.LBB0_7
	jmp	.LBB0_8
.LBB0_6:
	movl	$2, mymove(%rip)
	movl	$1, umove(%rip)
	cmpl	$0, 8(%rsp)
	jne	.LBB0_8
.LBB0_7:
	leaq	8(%rsp), %rdi
	leaq	12(%rsp), %rsi
	callq	genmove
	movb	mymove(%rip), %al
	movslq	8(%rsp), %rcx
	movslq	12(%rsp), %rdx
	imulq	$19, %rcx, %rcx
	movb	%al, p(%rcx,%rdx)
.LBB0_8:
	callq	showboard
	movl	play(%rip), %eax
	testl	%eax, %eax
	jle	.LBB0_20
# BB#9:
	leaq	22(%rsp), %rbx
	leaq	8(%rsp), %r14
	leaq	12(%rsp), %r15
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	scanf
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	getmove
	cmpl	$0, play(%rip)
	jle	.LBB0_18
# BB#12:                                #   in Loop: Header=BB0_11 Depth=1
	movslq	8(%rsp), %rax
	testq	%rax, %rax
	js	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_11 Depth=1
	movzbl	umove(%rip), %ecx
	movslq	12(%rsp), %rdx
	imulq	$19, %rax, %rax
	movb	%cl, p(%rax,%rdx)
	movl	mymove(%rip), %edi
	callq	examboard
.LBB0_14:                               #   in Loop: Header=BB0_11 Depth=1
	cmpl	$2, pass(%rip)
	je	.LBB0_17
# BB#15:                                #   in Loop: Header=BB0_11 Depth=1
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	genmove
	movslq	8(%rsp), %rax
	testq	%rax, %rax
	js	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_11 Depth=1
	movzbl	mymove(%rip), %ecx
	movslq	12(%rsp), %rdx
	imulq	$19, %rax, %rax
	movb	%cl, p(%rax,%rdx)
	movl	umove(%rip), %edi
	callq	examboard
.LBB0_17:                               #   in Loop: Header=BB0_11 Depth=1
	callq	showboard
.LBB0_18:                               #   in Loop: Header=BB0_11 Depth=1
	cmpl	$2, pass(%rip)
	je	.LBB0_19
# BB#10:                                # %thread-pre-split.loopexit
                                        #   in Loop: Header=BB0_11 Depth=1
	movl	play(%rip), %eax
	testl	%eax, %eax
	jg	.LBB0_11
.LBB0_20:                               # %thread-pre-split._crit_edge
	testl	%eax, %eax
	jne	.LBB0_23
	jmp	.LBB0_21
.LBB0_19:                               # %thread-pre-split._crit_edge.thread
	movl	$0, play(%rip)
.LBB0_21:
	movq	stdin(%rip), %rdi
	callq	_IO_getc
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	printf
	leaq	17(%rsp), %rsi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	scanf
	cmpb	$121, 17(%rsp)
	jne	.LBB0_23
# BB#22:
	callq	endgame
.LBB0_23:
	xorl	%eax, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"gnugo.dat"
	.size	.L.str, 10

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"r"
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%c"
	.size	.L.str.2, 3

	.type	p,@object               # @p
	.comm	p,361,16
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%d %d %d "
	.size	.L.str.3, 10

	.type	mymove,@object          # @mymove
	.comm	mymove,4,4
	.type	mk,@object              # @mk
	.comm	mk,4,4
	.type	uk,@object              # @uk
	.comm	uk,4,4
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%d "
	.size	.L.str.4, 4

	.type	opn,@object             # @opn
	.comm	opn,36,16
	.type	umove,@object           # @umove
	.comm	umove,4,4
	.type	play,@object            # @play
	.comm	play,4,4
	.type	pass,@object            # @pass
	.comm	pass,4,4
	.type	mik,@object             # @mik
	.comm	mik,4,4
	.type	mjk,@object             # @mjk
	.comm	mjk,4,4
	.type	uik,@object             # @uik
	.comm	uik,4,4
	.type	ujk,@object             # @ujk
	.comm	ujk,4,4
	.type	rd,@object              # @rd
	.comm	rd,4,4
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Number of handicap for black (0 to 17)? "
	.size	.L.str.5, 41

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%d"
	.size	.L.str.6, 3

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\nChoose side(b or w)? "
	.size	.L.str.7, 23

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"your move? "
	.size	.L.str.8, 12

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%s"
	.size	.L.str.9, 3

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Do you want to count score (y or n)? "
	.size	.L.str.10, 38

	.type	l,@object               # @l
	.comm	l,361,16
	.type	ma,@object              # @ma
	.comm	ma,361,16
	.type	ml,@object              # @ml
	.comm	ml,361,16
	.type	lib,@object             # @lib
	.comm	lib,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
