	.text
	.file	"CabHandler.bc"
	.globl	_ZN8NArchive4NCab8CHandler21GetNumberOfPropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab8CHandler21GetNumberOfPropertiesEPj,@function
_ZN8NArchive4NCab8CHandler21GetNumberOfPropertiesEPj: # @_ZN8NArchive4NCab8CHandler21GetNumberOfPropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$6, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	_ZN8NArchive4NCab8CHandler21GetNumberOfPropertiesEPj, .Lfunc_end0-_ZN8NArchive4NCab8CHandler21GetNumberOfPropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive4NCab8CHandler15GetPropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab8CHandler15GetPropertyInfoEjPPwPjPt,@function
_ZN8NArchive4NCab8CHandler15GetPropertyInfoEjPPwPjPt: # @_ZN8NArchive4NCab8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$5, %esi
	ja	.LBB1_2
# BB#1:
	movl	%esi, %eax
	shlq	$4, %rax
	movl	_ZN8NArchive4NCabL6kPropsE+8(%rax), %esi
	movl	%esi, (%rcx)
	movzwl	_ZN8NArchive4NCabL6kPropsE+12(%rax), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB1_2:
	retq
.Lfunc_end1:
	.size	_ZN8NArchive4NCab8CHandler15GetPropertyInfoEjPPwPjPt, .Lfunc_end1-_ZN8NArchive4NCab8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive4NCab8CHandler28GetNumberOfArchivePropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab8CHandler28GetNumberOfArchivePropertiesEPj,@function
_ZN8NArchive4NCab8CHandler28GetNumberOfArchivePropertiesEPj: # @_ZN8NArchive4NCab8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$3, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	_ZN8NArchive4NCab8CHandler28GetNumberOfArchivePropertiesEPj, .Lfunc_end2-_ZN8NArchive4NCab8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive4NCab8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab8CHandler22GetArchivePropertyInfoEjPPwPjPt,@function
_ZN8NArchive4NCab8CHandler22GetArchivePropertyInfoEjPPwPjPt: # @_ZN8NArchive4NCab8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$2, %esi
	ja	.LBB3_2
# BB#1:
	movl	%esi, %eax
	shlq	$4, %rax
	movl	_ZN8NArchive4NCabL9kArcPropsE+8(%rax), %esi
	movl	%esi, (%rcx)
	movzwl	_ZN8NArchive4NCabL9kArcPropsE+12(%rax), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB3_2:
	retq
.Lfunc_end3:
	.size	_ZN8NArchive4NCab8CHandler22GetArchivePropertyInfoEjPPwPjPt, .Lfunc_end3-_ZN8NArchive4NCab8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.zero	16
	.text
	.globl	_ZN8NArchive4NCab8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab8CHandler18GetArchivePropertyEjP14tagPROPVARIANT,@function
_ZN8NArchive4NCab8CHandler18GetArchivePropertyEjP14tagPROPVARIANT: # @_ZN8NArchive4NCab8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 160
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movl	$0, 72(%rsp)
	cmpl	$22, %esi
	je	.LBB4_9
# BB#1:
	cmpl	$39, %esi
	je	.LBB4_8
# BB#2:
	cmpl	$38, %esi
	jne	.LBB4_44
# BB#3:                                 # %.preheader63
	movslq	28(%rbx), %rbp
	testq	%rbp, %rbp
	jle	.LBB4_37
# BB#4:                                 # %.lr.ph80
	movq	32(%rbx), %rcx
	leaq	-1(%rbp), %rdx
	movq	%rbp, %rax
	xorl	%edi, %edi
	xorl	%esi, %esi
	andq	$3, %rax
	je	.LBB4_6
	.p2align	4, 0x90
.LBB4_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rbx
	addl	108(%rbx), %esi
	incq	%rdi
	cmpq	%rdi, %rax
	jne	.LBB4_5
.LBB4_6:                                # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB4_38
	.p2align	4, 0x90
.LBB4_7:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rax
	addl	108(%rax), %esi
	movq	8(%rcx,%rdi,8), %rax
	addl	108(%rax), %esi
	movq	16(%rcx,%rdi,8), %rax
	addl	108(%rax), %esi
	movq	24(%rcx,%rdi,8), %rax
	addl	108(%rax), %esi
	addq	$4, %rdi
	cmpq	%rbp, %rdi
	jl	.LBB4_7
	jmp	.LBB4_38
.LBB4_8:
	movl	28(%rbx), %esi
.Ltmp0:
	leaq	72(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp1:
	jmp	.LBB4_44
.LBB4_9:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp5:
	movl	$4, %edi
	callq	_Znam
.Ltmp6:
# BB#10:
	movq	%rax, (%rsp)
	movb	$0, (%rax)
	movl	$4, 12(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rsp)
	movq	$1, 64(%rsp)
	movq	$_ZTV13CRecordVectorIhE+16, 40(%rsp)
	cmpl	$0, 28(%rbx)
	jle	.LBB4_39
# BB#11:                                # %.lr.ph77
	movq	%r14, 88(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	leaq	40(%rsp), %rdi
	movq	%rbx, 96(%rsp)          # 8-byte Spill
.LBB4_12:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_14 Depth 2
                                        #       Child Loop BB4_16 Depth 3
	movq	32(%rbx), %rax
	movq	(%rax,%r12,8), %r13
	movl	108(%r13), %r8d
	testl	%r8d, %r8d
	jle	.LBB4_22
# BB#13:                                # %.lr.ph73
                                        #   in Loop: Header=BB4_12 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_14:                               #   Parent Loop BB4_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_16 Depth 3
	movq	112(%r13), %rax
	movq	(%rax,%r14,8), %rax
	movb	6(%rax), %r15b
	andb	$15, %r15b
	movl	52(%rsp), %ebx
	testl	%ebx, %ebx
	je	.LBB4_18
# BB#15:                                # %.lr.ph.i
                                        #   in Loop: Header=BB4_14 Depth=2
	xorl	%edx, %edx
	movq	56(%rsp), %rax
	.p2align	4, 0x90
.LBB4_16:                               #   Parent Loop BB4_12 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rbx,%rdx), %ecx
	movl	%ecx, %esi
	shrl	$31, %esi
	addl	%ecx, %esi
	sarl	%esi
	movslq	%esi, %rcx
	movzbl	(%rax,%rcx), %ecx
	cmpb	%cl, %r15b
	je	.LBB4_21
# BB#17:                                # %.thread.i
                                        #   in Loop: Header=BB4_16 Depth=3
	leal	1(%rsi), %ebp
	cmpb	%cl, %r15b
	cmovbl	%esi, %ebx
	cmovbl	%edx, %ebp
	cmpl	%ebx, %ebp
	movl	%ebp, %edx
	jne	.LBB4_16
	jmp	.LBB4_19
	.p2align	4, 0x90
.LBB4_18:                               #   in Loop: Header=BB4_14 Depth=2
	xorl	%ebx, %ebx
.LBB4_19:                               # %._crit_edge.i
                                        #   in Loop: Header=BB4_14 Depth=2
.Ltmp7:
	movq	%rdi, %rbp
	movl	%ebx, %esi
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
.Ltmp8:
# BB#20:                                # %.noexc
                                        #   in Loop: Header=BB4_14 Depth=2
	movq	56(%rsp), %rax
	movslq	%ebx, %rcx
	movb	%r15b, (%rax,%rcx)
	movl	108(%r13), %r8d
	movq	%rbp, %rdi
.LBB4_21:                               # %_ZN13CRecordVectorIhE17AddToUniqueSortedERKh.exit
                                        #   in Loop: Header=BB4_14 Depth=2
	incq	%r14
	movslq	%r8d, %rax
	cmpq	%rax, %r14
	jl	.LBB4_14
.LBB4_22:                               # %._crit_edge74
                                        #   in Loop: Header=BB4_12 Depth=1
	incq	%r12
	movq	96(%rsp), %rbx          # 8-byte Reload
	movslq	28(%rbx), %rax
	cmpq	%rax, %r12
	jl	.LBB4_12
# BB#23:                                # %.preheader
	cmpl	$0, 52(%rsp)
	jle	.LBB4_48
# BB#24:                                # %.lr.ph
	movq	%rdi, %r13
	xorl	%ebx, %ebx
	movq	%rsp, %r15
.LBB4_25:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_26 Depth 2
                                        #     Child Loop BB4_29 Depth 2
	movq	56(%rsp), %rax
	movzbl	(%rax,%rbx), %eax
	cmpq	$4, %rax
	leaq	_ZN8NArchive4NCabL8kMethodsE(,%rax,8), %rax
	movl	$_ZN8NArchive4NCabL14kUnknownMethodE, %ecx
	cmovaeq	%rcx, %rax
	movq	(%rax), %rbp
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$-1, %r12d
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB4_26:                               #   Parent Loop BB4_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%r12d
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB4_26
# BB#27:                                # %_Z11MyStringLenIcEiPKT_.exit.i
                                        #   in Loop: Header=BB4_25 Depth=1
	leal	1(%r12), %r14d
	movslq	%r14d, %rdi
	cmpl	$-1, %r12d
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp10:
	callq	_Znam
.Ltmp11:
# BB#28:                                # %.noexc61
                                        #   in Loop: Header=BB4_25 Depth=1
	movq	%rax, 16(%rsp)
	movb	$0, (%rax)
	movl	%r14d, 28(%rsp)
	.p2align	4, 0x90
.LBB4_29:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        #   Parent Loop BB4_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp), %ecx
	incq	%rbp
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB4_29
# BB#30:                                #   in Loop: Header=BB4_25 Depth=1
	movl	%r12d, 24(%rsp)
	cmpl	$0, 8(%rsp)
	je	.LBB4_32
# BB#31:                                #   in Loop: Header=BB4_25 Depth=1
.Ltmp13:
	movl	$32, %esi
	movq	%r15, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp14:
.LBB4_32:                               #   in Loop: Header=BB4_25 Depth=1
.Ltmp15:
	movq	%r15, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZN11CStringBaseIcEpLERKS0_
.Ltmp16:
# BB#33:                                #   in Loop: Header=BB4_25 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_35
# BB#34:                                #   in Loop: Header=BB4_25 Depth=1
	callq	_ZdaPv
.LBB4_35:                               # %_ZN11CStringBaseIcED2Ev.exit59
                                        #   in Loop: Header=BB4_25 Depth=1
	incq	%rbx
	movslq	52(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB4_25
# BB#36:
	movq	88(%rsp), %r14          # 8-byte Reload
	jmp	.LBB4_40
.LBB4_37:
	xorl	%esi, %esi
.LBB4_38:                               # %._crit_edge81
.Ltmp2:
	leaq	72(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp3:
	jmp	.LBB4_44
.LBB4_39:                               # %.preheader.thread
	leaq	40(%rsp), %r13
.LBB4_40:                               # %._crit_edge
	movq	(%rsp), %rsi
.Ltmp18:
	leaq	72(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKc
.Ltmp19:
# BB#41:
.Ltmp23:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp24:
# BB#42:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_44
# BB#43:
	callq	_ZdaPv
.LBB4_44:
.Ltmp26:
	leaq	72(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp27:
# BB#45:
.Ltmp32:
	leaq	72(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp33:
# BB#46:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit57
	xorl	%eax, %eax
.LBB4_47:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_48:
	movq	%rdi, %r13
	movq	88(%rsp), %r14          # 8-byte Reload
	jmp	.LBB4_40
.LBB4_49:
.Ltmp25:
	movq	%rdx, %r14
	movq	%rax, %r15
	jmp	.LBB4_63
.LBB4_50:
.Ltmp20:
	movq	%rdx, %r14
	movq	%rax, %r15
	movq	%r13, %rdi
	jmp	.LBB4_62
.LBB4_51:
.Ltmp4:
	jmp	.LBB4_54
.LBB4_52:
.Ltmp34:
	movq	%rdx, %r14
	movq	%rax, %r15
	jmp	.LBB4_66
.LBB4_53:
.Ltmp28:
.LBB4_54:
	movq	%rdx, %r14
	movq	%rax, %r15
	jmp	.LBB4_65
.LBB4_55:
.Ltmp12:
	jmp	.LBB4_60
.LBB4_56:
.Ltmp17:
	movq	%rdx, %r14
	movq	%rax, %r15
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_61
# BB#57:
	callq	_ZdaPv
	jmp	.LBB4_61
.LBB4_59:
.Ltmp9:
.LBB4_60:
	movq	%rdx, %r14
	movq	%rax, %r15
.LBB4_61:
	leaq	40(%rsp), %rdi
.LBB4_62:
.Ltmp21:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp22:
.LBB4_63:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_65
# BB#64:
	callq	_ZdaPv
.LBB4_65:
.Ltmp29:
	leaq	72(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp30:
.LBB4_66:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%r15, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB4_68
# BB#67:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB4_47
.LBB4_68:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp35:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp36:
# BB#69:
.LBB4_70:
.Ltmp37:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_71:
.Ltmp31:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN8NArchive4NCab8CHandler18GetArchivePropertyEjP14tagPROPVARIANT, .Lfunc_end4-_ZN8NArchive4NCab8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\270\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\251\001"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp6-.Ltmp0           #   Call between .Ltmp0 and .Ltmp6
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	3                       #   On action: 2
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	3                       #   On action: 2
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	3                       #   On action: 2
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp16-.Ltmp13         #   Call between .Ltmp13 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	3                       #   On action: 2
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp3-.Ltmp2           #   Call between .Ltmp2 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	3                       #   On action: 2
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	3                       #   On action: 2
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	3                       #   On action: 2
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	3                       #   On action: 2
	.long	.Ltmp32-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin0   #     jumps to .Ltmp34
	.byte	3                       #   On action: 2
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp30-.Ltmp21         #   Call between .Ltmp21 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin0   #     jumps to .Ltmp31
	.byte	1                       #   On action: 1
	.long	.Ltmp30-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp35-.Ltmp30         #   Call between .Ltmp30 and .Ltmp35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin0   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Lfunc_end4-.Ltmp36     #   Call between .Ltmp36 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11CStringBaseIcEpLEc,"axG",@progbits,_ZN11CStringBaseIcEpLEc,comdat
	.weak	_ZN11CStringBaseIcEpLEc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEc,@function
_ZN11CStringBaseIcEpLEc:                # @_ZN11CStringBaseIcEpLEc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB5_28
# BB#1:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB5_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB5_3:                                # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rsi,%rbx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB5_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB5_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB5_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB5_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB5_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB5_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB5_17
.LBB5_7:
	xorl	%ecx, %ecx
.LBB5_8:                                # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB5_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB5_10:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB5_10
.LBB5_11:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB5_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB5_13:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB5_13
	jmp	.LBB5_26
.LBB5_25:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB5_27
.LBB5_26:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB5_27:
	movq	%r12, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%r13)
.LBB5_28:                               # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	movb	%r14b, (%rax,%rcx)
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movb	$0, 1(%rax,%rcx)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_17:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r12,%rbx)
	movups	%xmm1, 16(%r12,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB5_20
	jmp	.LBB5_21
.LBB5_18:
	xorl	%ebx, %ebx
.LBB5_21:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB5_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB5_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB5_23
.LBB5_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB5_8
	jmp	.LBB5_26
.Lfunc_end5:
	.size	_ZN11CStringBaseIcEpLEc, .Lfunc_end5-_ZN11CStringBaseIcEpLEc
	.cfi_endproc

	.section	.text._ZN11CStringBaseIcEpLERKS0_,"axG",@progbits,_ZN11CStringBaseIcEpLERKS0_,comdat
	.weak	_ZN11CStringBaseIcEpLERKS0_
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLERKS0_,@function
_ZN11CStringBaseIcEpLERKS0_:            # @_ZN11CStringBaseIcEpLERKS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	8(%r14), %eax
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %ecx
	subl	%ebp, %ecx
	cmpl	%eax, %ecx
	jg	.LBB6_28
# BB#1:
	decl	%ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB6_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB6_3:                                # %select.end
	movl	%eax, %esi
	subl	%ecx, %esi
	addl	%edx, %ecx
	cmpl	%eax, %ecx
	cmovgel	%edx, %esi
	leal	1(%rsi,%rbx), %r12d
	cmpl	%ebx, %r12d
	je	.LBB6_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r12d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
	testl	%ebx, %ebx
	jle	.LBB6_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB6_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB6_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB6_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r15
	jae	.LBB6_17
# BB#16:                                # %vector.memcheck
	leaq	(%r15,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB6_17
.LBB6_7:
	xorl	%ecx, %ecx
.LBB6_8:                                # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB6_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB6_10:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r15,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB6_10
.LBB6_11:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB6_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r15,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB6_13:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB6_13
	jmp	.LBB6_26
.LBB6_25:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB6_27
.LBB6_26:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB6_27:
	movq	%r15, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r15,%rax)
	movl	%r12d, 12(%r13)
.LBB6_28:                               # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movslq	%ebp, %rax
	addq	(%r13), %rax
	movq	(%r14), %rcx
	.p2align	4, 0x90
.LBB6_29:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB6_29
# BB#30:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit
	movl	8(%r14), %eax
	addl	%eax, 8(%r13)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_17:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB6_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r15,%rbx)
	movups	%xmm1, 16(%r15,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB6_20
	jmp	.LBB6_21
.LBB6_18:
	xorl	%ebx, %ebx
.LBB6_21:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB6_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r15,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB6_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB6_23
.LBB6_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB6_8
	jmp	.LBB6_26
.Lfunc_end6:
	.size	_ZN11CStringBaseIcEpLERKS0_, .Lfunc_end6-_ZN11CStringBaseIcEpLERKS0_
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end7:
	.size	__clang_call_terminate, .Lfunc_end7-__clang_call_terminate

	.text
	.globl	_ZN8NArchive4NCab8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab8CHandler11GetPropertyEjjP14tagPROPVARIANT,@function
_ZN8NArchive4NCab8CHandler11GetPropertyEjjP14tagPROPVARIANT: # @_ZN8NArchive4NCab8CHandler11GetPropertyEjjP14tagPROPVARIANT
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 144
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	$0, 16(%rsp)
	addl	$-3, %edx
	cmpl	$24, %edx
	ja	.LBB8_77
# BB#1:
	movq	32(%rdi), %rax
	movq	64(%rdi), %rbp
	movslq	%esi, %rsi
	movslq	(%rbp,%rsi,8), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	144(%rax), %rbx
	movslq	4(%rbp,%rsi,8), %rsi
	movq	(%rbx,%rsi,8), %rbx
	jmpq	*.LJTI8_0(,%rdx,8)
.LBB8_2:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
.Ltmp66:
	movl	$16, %edi
	callq	_Znam
.Ltmp67:
# BB#3:
	movq	%rax, 32(%rsp)
	movl	$0, (%rax)
	movl	$4, 44(%rsp)
	cmpb	$0, 32(%rbx)
	js	.LBB8_30
# BB#4:
.Ltmp68:
	movq	%rsp, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp69:
# BB#5:
	movl	$0, 40(%rsp)
	movq	32(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	8(%rsp), %r14
	incq	%r14
	movl	44(%rsp), %ebp
	cmpl	%ebp, %r14d
	je	.LBB8_11
# BB#6:
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp71:
	callq	_Znam
	movq	%rax, %r12
.Ltmp72:
# BB#7:                                 # %.noexc
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB8_10
# BB#8:                                 # %.noexc
	testl	%ebp, %ebp
	jle	.LBB8_10
# BB#9:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	40(%rsp), %rax
.LBB8_10:                               # %._crit_edge16.i.i
	movq	%r12, 32(%rsp)
	movl	$0, (%r12,%rax,4)
	movl	%r14d, 44(%rsp)
	movq	%r12, %rbx
.LBB8_11:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_12:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_12
# BB#13:
	movl	8(%rsp), %eax
	movl	%eax, 40(%rsp)
	testq	%rdi, %rdi
	je	.LBB8_31
# BB#14:
	callq	_ZdaPv
	jmp	.LBB8_31
.LBB8_15:
	movzwl	28(%rbx), %edx
	movl	%edx, %esi
	andl	$65533, %esi            # imm = 0xFFFD
	xorl	%ecx, %ecx
	cmpl	$65533, %esi            # imm = 0xFFFD
	je	.LBB8_38
# BB#16:
	cmpl	$65534, %edx            # imm = 0xFFFE
	jb	.LBB8_37
# BB#17:
	movl	108(%rax), %ecx
	decl	%ecx
	jmp	.LBB8_38
.LBB8_18:
	movzwl	32(%rbx), %esi
	andl	$16, %esi
	shrl	$4, %esi
.Ltmp64:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp65:
	jmp	.LBB8_77
.LBB8_19:
	movq	96(%rdi), %rdx
	movl	(%rdx,%rcx,4), %ecx
	movzwl	28(%rbx), %edx
	movl	%edx, %edi
	andl	$65533, %edi            # imm = 0xFFFD
	xorl	%esi, %esi
	cmpl	$65533, %edi            # imm = 0xFFFD
	je	.LBB8_67
# BB#20:
	cmpl	$65534, %edx            # imm = 0xFFFE
	jb	.LBB8_66
# BB#21:
	movl	108(%rax), %esi
	decl	%esi
	jmp	.LBB8_67
.LBB8_22:
	movl	20(%rbx), %esi
.Ltmp62:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp63:
	jmp	.LBB8_77
.LBB8_23:
	movzwl	32(%rbx), %esi
	andl	$65407, %esi            # imm = 0xFF7F
.Ltmp60:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp61:
	jmp	.LBB8_77
.LBB8_24:
	movl	24(%rbx), %edi
.Ltmp53:
	leaq	32(%rsp), %rsi
	callq	_ZN8NWindows5NTime17DosTimeToFileTimeEjR9_FILETIME
.Ltmp54:
# BB#25:
	testb	%al, %al
	je	.LBB8_28
# BB#26:
.Ltmp55:
	leaq	32(%rsp), %rdi
	movq	%rsp, %rsi
	callq	LocalFileTimeToFileTime
.Ltmp56:
# BB#27:
	testl	%eax, %eax
	jne	.LBB8_29
.LBB8_28:                               # %.sink.split
	movq	$0, (%rsp)
.LBB8_29:
.Ltmp57:
	leaq	16(%rsp), %rdi
	movq	%rsp, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp58:
	jmp	.LBB8_77
.LBB8_30:
.Ltmp74:
	leaq	32(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_Z20ConvertUTF8ToUnicodeRK11CStringBaseIcERS_IwE
.Ltmp75:
.LBB8_31:
.Ltmp76:
	leaq	72(%rsp), %rdi
	leaq	32(%rsp), %rsi
	callq	_ZN8NArchive9NItemName15WinNameToOSNameERK11CStringBaseIwE
.Ltmp77:
# BB#32:
	movq	72(%rsp), %rsi
.Ltmp79:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp80:
# BB#33:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_35
# BB#34:
	callq	_ZdaPv
.LBB8_35:                               # %_ZN11CStringBaseIwED2Ev.exit68
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB8_76
	jmp	.LBB8_77
.LBB8_37:
	movl	%edx, %ecx
.LBB8_38:                               # %_ZNK8NArchive4NCab5CItem14GetFolderIndexEi.exit
	movq	112(%rax), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %r14
	movzbl	6(%r14), %eax
	movl	%eax, %r12d
	andb	$15, %r12b
	andl	$15, %eax
	cmpb	$4, %r12b
	leaq	_ZN8NArchive4NCabL8kMethodsE(,%rax,8), %rax
	movl	$_ZN8NArchive4NCabL14kUnknownMethodE, %ecx
	cmovbq	%rax, %rcx
	movq	(%rcx), %rbp
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %ebx
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB8_39:                               # =>This Inner Loop Header: Depth=1
	incl	%ebx
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB8_39
# BB#40:                                # %_Z11MyStringLenIcEiPKT_.exit.i83
	leal	1(%rbx), %r13d
	movslq	%r13d, %rax
	cmpl	$-1, %ebx
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp40:
	callq	_Znam
.Ltmp41:
# BB#41:                                # %.noexc86
	movq	%rax, (%rsp)
	movb	$0, (%rax)
	movl	%r13d, 12(%rsp)
	.p2align	4, 0x90
.LBB8_42:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %ecx
	incq	%rbp
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB8_42
# BB#43:
	movl	%ebx, 8(%rsp)
	orb	$1, %r12b
	cmpb	$3, %r12b
	jne	.LBB8_74
# BB#44:
.Ltmp43:
	movq	%rsp, %rdi
	movl	$58, %esi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp44:
# BB#45:
	movzbl	7(%r14), %edi
.Ltmp45:
	leaq	32(%rsp), %rbx
	movl	$10, %edx
	movq	%rbx, %rsi
	callq	_Z21ConvertUInt64ToStringyPcj
.Ltmp46:
# BB#46:                                # %.preheader.preheader
	movl	$-1, %r12d
	.p2align	4, 0x90
.LBB8_47:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	incl	%r12d
	cmpb	$0, (%rbx)
	leaq	1(%rbx), %rbx
	jne	.LBB8_47
# BB#48:                                # %_Z11MyStringLenIcEiPKT_.exit.i
	movl	8(%rsp), %ebx
	movl	12(%rsp), %ebp
	movl	%ebp, %eax
	subl	%ebx, %eax
	cmpl	%r12d, %eax
	jg	.LBB8_71
# BB#49:
	decl	%eax
	cmpl	$8, %ebp
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebp
	jl	.LBB8_51
# BB#50:                                # %select.true.sink
	movl	%ebp, %ecx
	shrl	$31, %ecx
	addl	%ebp, %ecx
	sarl	%ecx
.LBB8_51:                               # %select.end
	addl	%ecx, %eax
	movl	%ebx, %edx
	subl	%ebp, %edx
	cmpl	%r12d, %eax
	leal	1(%rdx,%r12), %eax
	cmovgel	%ecx, %eax
	leal	1(%rax,%rbp), %r13d
	cmpl	%ebp, %r13d
	je	.LBB8_71
# BB#52:
	addl	%ebp, %eax
	movslq	%r13d, %rcx
	cmpl	$-1, %eax
	movq	$-1, %rdi
	cmovgeq	%rcx, %rdi
.Ltmp47:
	callq	_Znam
	movq	%rax, %r14
.Ltmp48:
# BB#53:                                # %.noexc79
	testl	%ebp, %ebp
	jle	.LBB8_70
# BB#54:                                # %.preheader.i.i
	movq	(%rsp), %rdi
	testl	%ebx, %ebx
	jle	.LBB8_68
# BB#55:                                # %.lr.ph.preheader.i.i
	movslq	%ebx, %rax
	cmpl	$31, %ebx
	jbe	.LBB8_59
# BB#56:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB8_59
# BB#57:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r14
	jae	.LBB8_81
# BB#58:                                # %vector.memcheck
	leaq	(%r14,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB8_81
.LBB8_59:
	xorl	%ecx, %ecx
.LBB8_60:                               # %.lr.ph.i.i.preheader
	subl	%ecx, %ebx
	leaq	-1(%rax), %rsi
	subq	%rcx, %rsi
	andq	$7, %rbx
	je	.LBB8_63
# BB#61:                                # %.lr.ph.i.i.prol.preheader
	negq	%rbx
.LBB8_62:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r14,%rcx)
	incq	%rcx
	incq	%rbx
	jne	.LBB8_62
.LBB8_63:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB8_69
# BB#64:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r14,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
.LBB8_65:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB8_65
	jmp	.LBB8_69
.LBB8_66:
	movl	%edx, %esi
.LBB8_67:
	addl	%ecx, %esi
.Ltmp38:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEi
.Ltmp39:
	jmp	.LBB8_77
.LBB8_68:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB8_70
.LBB8_69:                               # %._crit_edge.thread.i.i78
	callq	_ZdaPv
	movl	8(%rsp), %ebx
.LBB8_70:                               # %._crit_edge17.i.i
	movq	%r14, (%rsp)
	movslq	%ebx, %rax
	movb	$0, (%r14,%rax)
	movl	%r13d, 12(%rsp)
.LBB8_71:                               # %.noexc74
	leaq	32(%rsp), %rax
	movslq	%ebx, %rcx
	addq	(%rsp), %rcx
	.p2align	4, 0x90
.LBB8_72:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	incq	%rax
	movb	%dl, (%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB8_72
# BB#73:
	addl	%r12d, 8(%rsp)
.LBB8_74:
	movq	(%rsp), %rsi
.Ltmp50:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKc
.Ltmp51:
# BB#75:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_77
.LBB8_76:
	callq	_ZdaPv
.LBB8_77:
.Ltmp82:
	leaq	16(%rsp), %rdi
	movq	%r15, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp83:
# BB#78:
.Ltmp88:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp89:
# BB#79:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit64
	xorl	%eax, %eax
.LBB8_80:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_81:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB8_84
# BB#82:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB8_83:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movups	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%r14,%rbp)
	movups	%xmm1, 16(%r14,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB8_83
	jmp	.LBB8_85
.LBB8_84:
	xorl	%ebp, %ebp
.LBB8_85:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB8_88
# BB#86:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r14,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB8_87:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB8_87
.LBB8_88:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB8_60
	jmp	.LBB8_69
.LBB8_89:
.Ltmp73:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB8_93
	jmp	.LBB8_97
.LBB8_90:
.Ltmp49:
	jmp	.LBB8_100
.LBB8_91:
.Ltmp70:
	jmp	.LBB8_96
.LBB8_92:
.Ltmp81:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_97
.LBB8_93:
	callq	_ZdaPv
	jmp	.LBB8_97
.LBB8_94:
.Ltmp42:
	jmp	.LBB8_105
.LBB8_95:
.Ltmp78:
.LBB8_96:                               # %_ZN11CStringBaseIwED2Ev.exit70
	movq	%rdx, %r14
	movq	%rax, %rbx
.LBB8_97:                               # %_ZN11CStringBaseIwED2Ev.exit70
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_106
# BB#98:
	callq	_ZdaPv
	jmp	.LBB8_106
.LBB8_99:
.Ltmp52:
.LBB8_100:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_106
# BB#101:
	callq	_ZdaPv
	jmp	.LBB8_106
.LBB8_102:
.Ltmp59:
	jmp	.LBB8_105
.LBB8_103:
.Ltmp90:
	movq	%rdx, %r14
	movq	%rax, %rbx
	jmp	.LBB8_107
.LBB8_104:
.Ltmp84:
.LBB8_105:
	movq	%rdx, %r14
	movq	%rax, %rbx
.LBB8_106:
.Ltmp85:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp86:
.LBB8_107:                              # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB8_109
# BB#108:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB8_80
.LBB8_109:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp91:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp92:
# BB#110:
.LBB8_111:
.Ltmp93:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB8_112:
.Ltmp87:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN8NArchive4NCab8CHandler11GetPropertyEjjP14tagPROPVARIANT, .Lfunc_end8-_ZN8NArchive4NCab8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI8_0:
	.quad	.LBB8_2
	.quad	.LBB8_77
	.quad	.LBB8_77
	.quad	.LBB8_18
	.quad	.LBB8_22
	.quad	.LBB8_77
	.quad	.LBB8_23
	.quad	.LBB8_77
	.quad	.LBB8_77
	.quad	.LBB8_24
	.quad	.LBB8_77
	.quad	.LBB8_77
	.quad	.LBB8_77
	.quad	.LBB8_77
	.quad	.LBB8_77
	.quad	.LBB8_77
	.quad	.LBB8_77
	.quad	.LBB8_77
	.quad	.LBB8_77
	.quad	.LBB8_15
	.quad	.LBB8_77
	.quad	.LBB8_77
	.quad	.LBB8_77
	.quad	.LBB8_77
	.quad	.LBB8_19
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\371\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\352\001"              # Call site table length
	.long	.Ltmp66-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp84-.Lfunc_begin1   #     jumps to .Ltmp84
	.byte	3                       #   On action: 2
	.long	.Ltmp68-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin1   #     jumps to .Ltmp70
	.byte	3                       #   On action: 2
	.long	.Ltmp71-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin1   #     jumps to .Ltmp73
	.byte	3                       #   On action: 2
	.long	.Ltmp64-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp61-.Ltmp64         #   Call between .Ltmp64 and .Ltmp61
	.long	.Ltmp84-.Lfunc_begin1   #     jumps to .Ltmp84
	.byte	3                       #   On action: 2
	.long	.Ltmp53-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp58-.Ltmp53         #   Call between .Ltmp53 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin1   #     jumps to .Ltmp59
	.byte	3                       #   On action: 2
	.long	.Ltmp74-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp77-.Ltmp74         #   Call between .Ltmp74 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin1   #     jumps to .Ltmp78
	.byte	3                       #   On action: 2
	.long	.Ltmp79-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin1   #     jumps to .Ltmp81
	.byte	3                       #   On action: 2
	.long	.Ltmp40-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin1   #     jumps to .Ltmp42
	.byte	3                       #   On action: 2
	.long	.Ltmp43-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp52-.Lfunc_begin1   #     jumps to .Ltmp52
	.byte	3                       #   On action: 2
	.long	.Ltmp45-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp48-.Ltmp45         #   Call between .Ltmp45 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin1   #     jumps to .Ltmp49
	.byte	3                       #   On action: 2
	.long	.Ltmp38-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp84-.Lfunc_begin1   #     jumps to .Ltmp84
	.byte	3                       #   On action: 2
	.long	.Ltmp50-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin1   #     jumps to .Ltmp52
	.byte	3                       #   On action: 2
	.long	.Ltmp82-.Lfunc_begin1   # >> Call Site 13 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin1   #     jumps to .Ltmp84
	.byte	3                       #   On action: 2
	.long	.Ltmp88-.Lfunc_begin1   # >> Call Site 14 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin1   #     jumps to .Ltmp90
	.byte	3                       #   On action: 2
	.long	.Ltmp85-.Lfunc_begin1   # >> Call Site 15 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin1   #     jumps to .Ltmp87
	.byte	1                       #   On action: 1
	.long	.Ltmp86-.Lfunc_begin1   # >> Call Site 16 <<
	.long	.Ltmp91-.Ltmp86         #   Call between .Ltmp86 and .Ltmp91
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin1   # >> Call Site 17 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin1   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin1   # >> Call Site 18 <<
	.long	.Lfunc_end8-.Ltmp92     #   Call between .Ltmp92 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NCab8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback,@function
_ZN8NArchive4NCab8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback: # @_ZN8NArchive4NCab8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi58:
	.cfi_def_cfa_offset 352
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, %r12
	movq	(%r12), %rax
.Ltmp94:
	callq	*48(%rax)
.Ltmp95:
# BB#1:
.Ltmp97:
	leaq	80(%rsp), %rdi
	callq	_ZN9CInBufferC1Ev
.Ltmp98:
# BB#2:                                 # %_ZN8NArchive4NCab10CInArchiveC2Ev.exit
	movq	$0, 24(%rsp)
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp100:
	leaq	24(%rsp), %rdx
	movl	$IID_IArchiveOpenVolumeCallback, %esi
	callq	*(%rax)
.Ltmp101:
# BB#3:
	movq	%rbp, 16(%rsp)
	testq	%rbp, %rbp
	je	.LBB9_5
# BB#4:
	movq	(%rbp), %rax
.Ltmp102:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp103:
.LBB9_5:                                # %_ZN9CMyComPtrI9IInStreamEC2EPS0_.exit
	movq	$0, 48(%rsp)
	leaq	136(%rsp), %rbx
	leaq	16(%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, %r15d
	xorl	%r14d, %r14d
                                        # implicit-def: %EAX
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB9_6
	.p2align	4, 0x90
.LBB9_11:                               #   in Loop: Header=BB9_6 Depth=1
.Ltmp105:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NCab12CArchiveInfoC2Ev
.Ltmp106:
# BB#12:                                #   in Loop: Header=BB9_6 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, 96(%rbx)
	movq	$8, 248(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE+16, 224(%rsp)
	movups	%xmm0, 128(%rbx)
	movq	$8, 280(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE+16, 256(%rsp)
	movq	$0, 288(%rsp)
	movq	16(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB9_16
# BB#13:                                #   in Loop: Header=BB9_6 Depth=1
	movq	(%rbp), %rax
.Ltmp108:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp109:
# BB#14:                                # %.noexc103
                                        #   in Loop: Header=BB9_6 Depth=1
	movq	288(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_16
# BB#15:                                #   in Loop: Header=BB9_6 Depth=1
	movq	(%rdi), %rax
.Ltmp110:
	callq	*16(%rax)
.Ltmp111:
.LBB9_16:                               # %.noexc103.thread
                                        #   in Loop: Header=BB9_6 Depth=1
	movq	%rbp, 288(%rsp)
.Ltmp112:
	leaq	80(%rsp), %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	leaq	128(%rsp), %rdx
	callq	_ZN8NArchive4NCab10CInArchive4OpenEPKyRNS0_11CDatabaseExE
	movl	%eax, %r15d
.Ltmp113:
# BB#17:                                #   in Loop: Header=BB9_6 Depth=1
	movl	$1, %ebp
	cmpl	$1, %r15d
	je	.LBB9_34
# BB#18:                                #   in Loop: Header=BB9_6 Depth=1
	testl	%r15d, %r15d
	jne	.LBB9_19
# BB#20:                                #   in Loop: Header=BB9_6 Depth=1
	movslq	28(%r12), %rsi
	movl	%r14d, %ecx
	andb	$1, %cl
	testq	%rsi, %rsi
	je	.LBB9_23
# BB#21:                                #   in Loop: Header=BB9_6 Depth=1
	leaq	-1(%rsi), %rax
	testb	%cl, %cl
	movq	32(%r12), %rdx
	movl	$0, %edi
	cmovneq	%rax, %rdi
	movq	(%rdx,%rdi,8), %rax
	movzwl	16(%rax), %edx
	cmpw	144(%rsp), %dx
	jne	.LBB9_36
# BB#22:                                #   in Loop: Header=BB9_6 Depth=1
	movzwl	18(%rax), %eax
	movl	%ecx, %edx
	addb	%dl, %dl
	movzbl	%dl, %edx
	leal	-1(%rdx,%rax), %eax
	movzwl	146(%rsp), %edx
	cmpl	%edx, %eax
	jne	.LBB9_36
.LBB9_23:                               # %.thread
                                        #   in Loop: Header=BB9_6 Depth=1
	xorl	%r15d, %r15d
	testb	%cl, %cl
	cmovel	%r15d, %esi
.Ltmp114:
	movq	32(%rsp), %rdi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	leaq	128(%rsp), %rdx
	callq	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6InsertEiRKS2_
.Ltmp115:
	movl	%r14d, %r13d
	jmp	.LBB9_24
	.p2align	4, 0x90
.LBB9_34:                               # %.thread129
                                        #   in Loop: Header=BB9_6 Depth=1
	cmpl	$0, 28(%r12)
	je	.LBB9_35
.LBB9_36:                               # %.thread129.thread
                                        #   in Loop: Header=BB9_6 Depth=1
	movb	$1, %r13b
	movl	$1, %r15d
	testb	$1, %r14b
	jne	.LBB9_37
.LBB9_24:                               #   in Loop: Header=BB9_6 Depth=1
	movslq	268(%rsp), %rax
	addq	%rax, 48(%rsp)
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp116:
	xorl	%edx, %edx
	leaq	48(%rsp), %rsi
	callq	*48(%rax)
.Ltmp117:
# BB#25:                                #   in Loop: Header=BB9_6 Depth=1
	testl	%eax, %eax
	je	.LBB9_38
# BB#26:                                #   in Loop: Header=BB9_6 Depth=1
	movb	%r13b, %r14b
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB9_66
	.p2align	4, 0x90
.LBB9_19:                               #   in Loop: Header=BB9_6 Depth=1
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	jmp	.LBB9_66
.LBB9_37:                               #   in Loop: Header=BB9_6 Depth=1
	movl	$3, %ebp
	jmp	.LBB9_66
.LBB9_38:                               #   in Loop: Header=BB9_6 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_40
# BB#39:                                #   in Loop: Header=BB9_6 Depth=1
	movq	(%rdi), %rax
.Ltmp118:
	callq	*16(%rax)
.Ltmp119:
.LBB9_40:                               # %_ZN9CMyComPtrI9IInStreamEaSEPS0_.exit
                                        #   in Loop: Header=BB9_6 Depth=1
	movq	$0, 16(%rsp)
	.p2align	4, 0x90
.LBB9_41:                               #   Parent Loop BB9_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	$1, %r13b
	jne	.LBB9_42
# BB#44:                                #   in Loop: Header=BB9_41 Depth=2
	movq	32(%r12), %rcx
	movq	(%rcx), %rsi
	testb	$1, 14(%rsi)
	movb	$1, %r14b
	je	.LBB9_45
# BB#48:                                #   in Loop: Header=BB9_41 Depth=2
	addq	$24, %rsi
	jmp	.LBB9_49
	.p2align	4, 0x90
.LBB9_42:                               # %..thread132_crit_edge
                                        #   in Loop: Header=BB9_41 Depth=2
	movq	32(%r12), %rcx
	movb	%r13b, %r14b
.LBB9_45:                               # %.thread132
                                        #   in Loop: Header=BB9_41 Depth=2
	movslq	28(%r12), %rax
	movq	-8(%rcx,%rax,8), %rsi
	testb	$2, 14(%rsi)
	je	.LBB9_47
# BB#46:                                #   in Loop: Header=BB9_41 Depth=2
	addq	$56, %rsi
	movl	%r14d, %r13d
.LBB9_49:                               # %select.unfold
                                        #   in Loop: Header=BB9_41 Depth=2
.Ltmp121:
	xorl	%edx, %edx
	leaq	64(%rsp), %rdi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp122:
# BB#50:                                #   in Loop: Header=BB9_41 Depth=2
	movq	24(%rsp), %rdi
	movl	$4, %ebp
	testq	%rdi, %rdi
	je	.LBB9_60
# BB#51:                                #   in Loop: Header=BB9_41 Depth=2
	movq	(%rdi), %rax
	movq	64(%rsp), %rsi
.Ltmp124:
	leaq	16(%rsp), %rdx
	callq	*48(%rax)
.Ltmp125:
# BB#52:                                #   in Loop: Header=BB9_41 Depth=2
	testl	%eax, %eax
	je	.LBB9_60
# BB#53:                                #   in Loop: Header=BB9_41 Depth=2
	cmpl	$1, %eax
	jne	.LBB9_54
# BB#57:                                #   in Loop: Header=BB9_41 Depth=2
	movl	%r13d, %eax
	andb	$1, %al
	movb	$1, %cl
	je	.LBB9_59
# BB#58:                                #   in Loop: Header=BB9_41 Depth=2
	movl	%r13d, %ecx
.LBB9_59:                               #   in Loop: Header=BB9_41 Depth=2
	shlb	$2, %al
	movzbl	%al, %ebp
	movl	%ecx, %r13d
	jmp	.LBB9_60
.LBB9_54:                               #   in Loop: Header=BB9_41 Depth=2
	movl	$1, %ebp
	movl	%eax, 12(%rsp)          # 4-byte Spill
.LBB9_60:                               #   in Loop: Header=BB9_41 Depth=2
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_62
# BB#61:                                #   in Loop: Header=BB9_41 Depth=2
	callq	_ZdaPv
.LBB9_62:                               #   in Loop: Header=BB9_41 Depth=2
	testb	%bpl, %bpl
	je	.LBB9_41
# BB#63:                                #   in Loop: Header=BB9_6 Depth=1
	cmpb	$4, %bpl
	jne	.LBB9_65
# BB#64:                                #   in Loop: Header=BB9_6 Depth=1
	xorl	%ebp, %ebp
.LBB9_65:                               # %.thread140.loopexit186
                                        #   in Loop: Header=BB9_6 Depth=1
	movb	%r13b, %r14b
	jmp	.LBB9_66
.LBB9_35:                               #   in Loop: Header=BB9_6 Depth=1
	movl	$1, %r15d
	movl	$1, %ebp
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB9_66:                               # %.thread140
                                        #   in Loop: Header=BB9_6 Depth=1
	movq	288(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_68
# BB#67:                                #   in Loop: Header=BB9_6 Depth=1
	movq	(%rdi), %rax
.Ltmp135:
	callq	*16(%rax)
.Ltmp136:
.LBB9_68:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit.i121
                                        #   in Loop: Header=BB9_6 Depth=1
.Ltmp141:
	leaq	128(%rsp), %rdi
	callq	_ZN8NArchive4NCab9CDatabaseD2Ev
.Ltmp142:
# BB#69:                                # %_ZN8NArchive4NCab11CDatabaseExD2Ev.exit125
                                        #   in Loop: Header=BB9_6 Depth=1
	testl	%ebp, %ebp
	je	.LBB9_6
	jmp	.LBB9_70
.LBB9_47:                               #   in Loop: Header=BB9_6 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB9_66
	.p2align	4, 0x90
.LBB9_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_41 Depth 2
	cmpq	$0, 16(%rsp)
	jne	.LBB9_11
	jmp	.LBB9_7
.LBB9_70:                               # %_ZN8NArchive4NCab11CDatabaseExD2Ev.exit125
	cmpl	$3, %ebp
	jne	.LBB9_71
.LBB9_7:
	movl	12(%rsp), %ebx          # 4-byte Reload
	testl	%r15d, %r15d
	jne	.LBB9_32
# BB#8:
.Ltmp144:
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	_ZN8NArchive4NCab13CMvDatabaseEx17FillSortAndShrinkEv
.Ltmp145:
# BB#9:
.Ltmp146:
	movq	%rbp, %rdi
	callq	_ZN8NArchive4NCab13CMvDatabaseEx5CheckEv
.Ltmp147:
# BB#10:
	movl	$1, %r15d
	xorl	%ebp, %ebp
	testb	%al, %al
	je	.LBB9_32
	jmp	.LBB9_72
.LBB9_71:                               # %.loopexit149.loopexit
	movl	12(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB9_72
.LBB9_33:                               # %.loopexit.split-lp
.Ltmp148:
	jmp	.LBB9_30
.LBB9_28:
.Ltmp99:
	jmp	.LBB9_109
.LBB9_27:
.Ltmp96:
	jmp	.LBB9_109
.LBB9_98:
.Ltmp104:
	jmp	.LBB9_99
.LBB9_81:
.Ltmp137:
	movq	%rax, %rbp
.Ltmp138:
	leaq	128(%rsp), %rdi
	callq	_ZN8NArchive4NCab9CDatabaseD2Ev
.Ltmp139:
	jmp	.LBB9_31
.LBB9_82:
.Ltmp140:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_55:
.Ltmp126:
	movq	%rax, %rbp
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_86
# BB#56:
	callq	_ZdaPv
	jmp	.LBB9_86
.LBB9_83:
.Ltmp143:
	jmp	.LBB9_30
.LBB9_29:                               # %.loopexit148
.Ltmp107:
.LBB9_30:
	movq	%rax, %rbp
	jmp	.LBB9_31
.LBB9_43:
.Ltmp123:
	jmp	.LBB9_85
.LBB9_84:
.Ltmp120:
.LBB9_85:
	movq	%rax, %rbp
.LBB9_86:
	movq	288(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_88
# BB#87:
	movq	(%rdi), %rax
.Ltmp127:
	callq	*16(%rax)
.Ltmp128:
.LBB9_88:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit.i
.Ltmp133:
	leaq	128(%rsp), %rdi
	callq	_ZN8NArchive4NCab9CDatabaseD2Ev
.Ltmp134:
.LBB9_31:
	movq	%rbp, %rdi
	callq	__cxa_begin_catch
	movl	$1, %r15d
.Ltmp149:
	callq	__cxa_end_catch
.Ltmp150:
.LBB9_32:
	movq	(%r12), %rax
	movl	$1, %ebp
.Ltmp151:
	movq	%r12, %rdi
	callq	*48(%rax)
.Ltmp152:
	movl	%r15d, %ebx
.LBB9_72:                               # %.loopexit149
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_74
# BB#73:
	movq	(%rdi), %rax
.Ltmp156:
	callq	*16(%rax)
.Ltmp157:
.LBB9_74:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_76
# BB#75:
	movq	(%rdi), %rax
.Ltmp161:
	callq	*16(%rax)
.Ltmp162:
.LBB9_76:                               # %_ZN9CMyComPtrI26IArchiveOpenVolumeCallbackED2Ev.exit113
.Ltmp173:
	leaq	80(%rsp), %rdi
	callq	_ZN9CInBuffer4FreeEv
.Ltmp174:
# BB#77:
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_79
# BB#78:
	movq	(%rdi), %rax
.Ltmp179:
	callq	*16(%rax)
.Ltmp180:
.LBB9_79:                               # %_ZN8NArchive4NCab10CInArchiveD2Ev.exit110
	testl	%ebp, %ebp
	cmovel	%ebp, %ebx
	movl	%ebx, %eax
.LBB9_80:                               # %_ZN8NArchive4NCab10CInArchiveD2Ev.exit110
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_89:
.Ltmp129:
	movq	%rax, %rbx
.Ltmp130:
	leaq	128(%rsp), %rdi
	callq	_ZN8NArchive4NCab9CDatabaseD2Ev
.Ltmp131:
	jmp	.LBB9_115
.LBB9_90:
.Ltmp132:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_108:
.Ltmp181:
.LBB9_109:
	movq	%rdx, %rbx
	movq	%rax, %rbp
	jmp	.LBB9_110
.LBB9_97:
.Ltmp163:
	movq	%rdx, %rbx
	movq	%rax, %rbp
	jmp	.LBB9_102
.LBB9_96:
.Ltmp158:
.LBB9_99:
	movq	%rdx, %rbx
	movq	%rax, %rbp
	jmp	.LBB9_100
.LBB9_93:
.Ltmp175:
	movq	%rdx, %rbx
	movq	%rax, %rbp
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_110
# BB#94:
	movq	(%rdi), %rax
.Ltmp176:
	callq	*16(%rax)
.Ltmp177:
	jmp	.LBB9_110
.LBB9_95:
.Ltmp178:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_91:
.Ltmp153:
	movq	%rdx, %rbx
	movq	%rax, %rbp
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_100
# BB#92:
	movq	(%rdi), %rax
.Ltmp154:
	callq	*16(%rax)
.Ltmp155:
.LBB9_100:
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_102
# BB#101:
	movq	(%rdi), %rax
.Ltmp159:
	callq	*16(%rax)
.Ltmp160:
.LBB9_102:                              # %_ZN9CMyComPtrI26IArchiveOpenVolumeCallbackED2Ev.exit
.Ltmp164:
	leaq	80(%rsp), %rdi
	callq	_ZN9CInBuffer4FreeEv
.Ltmp165:
# BB#103:
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_110
# BB#104:
	movq	(%rdi), %rax
.Ltmp170:
	callq	*16(%rax)
.Ltmp171:
.LBB9_110:
	movq	%rbp, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbp
	cmpl	$2, %ebx
	je	.LBB9_111
# BB#112:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB9_80
.LBB9_111:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbp, (%rax)
.Ltmp182:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp183:
# BB#116:
.LBB9_113:
.Ltmp184:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB9_105:
.Ltmp166:
	movq	%rax, %rbx
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_115
# BB#106:
	movq	(%rdi), %rax
.Ltmp167:
	callq	*16(%rax)
.Ltmp168:
	jmp	.LBB9_115
.LBB9_107:
.Ltmp169:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_114:
.Ltmp172:
	movq	%rax, %rbx
.LBB9_115:                              # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN8NArchive4NCab8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback, .Lfunc_end9-_ZN8NArchive4NCab8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\373\202"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\354\002"              # Call site table length
	.long	.Ltmp94-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin2   #     jumps to .Ltmp96
	.byte	3                       #   On action: 2
	.long	.Ltmp97-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin2   #     jumps to .Ltmp99
	.byte	3                       #   On action: 2
	.long	.Ltmp100-.Lfunc_begin2  # >> Call Site 3 <<
	.long	.Ltmp103-.Ltmp100       #   Call between .Ltmp100 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin2  #     jumps to .Ltmp104
	.byte	3                       #   On action: 2
	.long	.Ltmp105-.Lfunc_begin2  # >> Call Site 4 <<
	.long	.Ltmp106-.Ltmp105       #   Call between .Ltmp105 and .Ltmp106
	.long	.Ltmp107-.Lfunc_begin2  #     jumps to .Ltmp107
	.byte	1                       #   On action: 1
	.long	.Ltmp108-.Lfunc_begin2  # >> Call Site 5 <<
	.long	.Ltmp119-.Ltmp108       #   Call between .Ltmp108 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin2  #     jumps to .Ltmp120
	.byte	1                       #   On action: 1
	.long	.Ltmp121-.Lfunc_begin2  # >> Call Site 6 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp123-.Lfunc_begin2  #     jumps to .Ltmp123
	.byte	1                       #   On action: 1
	.long	.Ltmp124-.Lfunc_begin2  # >> Call Site 7 <<
	.long	.Ltmp125-.Ltmp124       #   Call between .Ltmp124 and .Ltmp125
	.long	.Ltmp126-.Lfunc_begin2  #     jumps to .Ltmp126
	.byte	1                       #   On action: 1
	.long	.Ltmp135-.Lfunc_begin2  # >> Call Site 8 <<
	.long	.Ltmp136-.Ltmp135       #   Call between .Ltmp135 and .Ltmp136
	.long	.Ltmp137-.Lfunc_begin2  #     jumps to .Ltmp137
	.byte	1                       #   On action: 1
	.long	.Ltmp141-.Lfunc_begin2  # >> Call Site 9 <<
	.long	.Ltmp142-.Ltmp141       #   Call between .Ltmp141 and .Ltmp142
	.long	.Ltmp143-.Lfunc_begin2  #     jumps to .Ltmp143
	.byte	1                       #   On action: 1
	.long	.Ltmp144-.Lfunc_begin2  # >> Call Site 10 <<
	.long	.Ltmp147-.Ltmp144       #   Call between .Ltmp144 and .Ltmp147
	.long	.Ltmp148-.Lfunc_begin2  #     jumps to .Ltmp148
	.byte	1                       #   On action: 1
	.long	.Ltmp138-.Lfunc_begin2  # >> Call Site 11 <<
	.long	.Ltmp139-.Ltmp138       #   Call between .Ltmp138 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin2  #     jumps to .Ltmp140
	.byte	1                       #   On action: 1
	.long	.Ltmp127-.Lfunc_begin2  # >> Call Site 12 <<
	.long	.Ltmp128-.Ltmp127       #   Call between .Ltmp127 and .Ltmp128
	.long	.Ltmp129-.Lfunc_begin2  #     jumps to .Ltmp129
	.byte	1                       #   On action: 1
	.long	.Ltmp133-.Lfunc_begin2  # >> Call Site 13 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp172-.Lfunc_begin2  #     jumps to .Ltmp172
	.byte	1                       #   On action: 1
	.long	.Ltmp134-.Lfunc_begin2  # >> Call Site 14 <<
	.long	.Ltmp149-.Ltmp134       #   Call between .Ltmp134 and .Ltmp149
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp149-.Lfunc_begin2  # >> Call Site 15 <<
	.long	.Ltmp152-.Ltmp149       #   Call between .Ltmp149 and .Ltmp152
	.long	.Ltmp153-.Lfunc_begin2  #     jumps to .Ltmp153
	.byte	3                       #   On action: 2
	.long	.Ltmp156-.Lfunc_begin2  # >> Call Site 16 <<
	.long	.Ltmp157-.Ltmp156       #   Call between .Ltmp156 and .Ltmp157
	.long	.Ltmp158-.Lfunc_begin2  #     jumps to .Ltmp158
	.byte	3                       #   On action: 2
	.long	.Ltmp161-.Lfunc_begin2  # >> Call Site 17 <<
	.long	.Ltmp162-.Ltmp161       #   Call between .Ltmp161 and .Ltmp162
	.long	.Ltmp163-.Lfunc_begin2  #     jumps to .Ltmp163
	.byte	3                       #   On action: 2
	.long	.Ltmp173-.Lfunc_begin2  # >> Call Site 18 <<
	.long	.Ltmp174-.Ltmp173       #   Call between .Ltmp173 and .Ltmp174
	.long	.Ltmp175-.Lfunc_begin2  #     jumps to .Ltmp175
	.byte	3                       #   On action: 2
	.long	.Ltmp179-.Lfunc_begin2  # >> Call Site 19 <<
	.long	.Ltmp180-.Ltmp179       #   Call between .Ltmp179 and .Ltmp180
	.long	.Ltmp181-.Lfunc_begin2  #     jumps to .Ltmp181
	.byte	3                       #   On action: 2
	.long	.Ltmp130-.Lfunc_begin2  # >> Call Site 20 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin2  #     jumps to .Ltmp132
	.byte	1                       #   On action: 1
	.long	.Ltmp176-.Lfunc_begin2  # >> Call Site 21 <<
	.long	.Ltmp177-.Ltmp176       #   Call between .Ltmp176 and .Ltmp177
	.long	.Ltmp178-.Lfunc_begin2  #     jumps to .Ltmp178
	.byte	1                       #   On action: 1
	.long	.Ltmp154-.Lfunc_begin2  # >> Call Site 22 <<
	.long	.Ltmp160-.Ltmp154       #   Call between .Ltmp154 and .Ltmp160
	.long	.Ltmp172-.Lfunc_begin2  #     jumps to .Ltmp172
	.byte	1                       #   On action: 1
	.long	.Ltmp164-.Lfunc_begin2  # >> Call Site 23 <<
	.long	.Ltmp165-.Ltmp164       #   Call between .Ltmp164 and .Ltmp165
	.long	.Ltmp166-.Lfunc_begin2  #     jumps to .Ltmp166
	.byte	1                       #   On action: 1
	.long	.Ltmp170-.Lfunc_begin2  # >> Call Site 24 <<
	.long	.Ltmp171-.Ltmp170       #   Call between .Ltmp170 and .Ltmp171
	.long	.Ltmp172-.Lfunc_begin2  #     jumps to .Ltmp172
	.byte	1                       #   On action: 1
	.long	.Ltmp171-.Lfunc_begin2  # >> Call Site 25 <<
	.long	.Ltmp182-.Ltmp171       #   Call between .Ltmp171 and .Ltmp182
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp182-.Lfunc_begin2  # >> Call Site 26 <<
	.long	.Ltmp183-.Ltmp182       #   Call between .Ltmp182 and .Ltmp183
	.long	.Ltmp184-.Lfunc_begin2  #     jumps to .Ltmp184
	.byte	0                       #   On action: cleanup
	.long	.Ltmp183-.Lfunc_begin2  # >> Call Site 27 <<
	.long	.Ltmp167-.Ltmp183       #   Call between .Ltmp183 and .Ltmp167
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp167-.Lfunc_begin2  # >> Call Site 28 <<
	.long	.Ltmp168-.Ltmp167       #   Call between .Ltmp167 and .Ltmp168
	.long	.Ltmp169-.Lfunc_begin2  #     jumps to .Ltmp169
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6InsertEiRKS2_,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6InsertEiRKS2_,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6InsertEiRKS2_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6InsertEiRKS2_,@function
_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6InsertEiRKS2_: # @_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6InsertEiRKS2_
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi69:
	.cfi_def_cfa_offset 48
.Lcfi70:
	.cfi_offset %rbx, -40
.Lcfi71:
	.cfi_offset %r14, -32
.Lcfi72:
	.cfi_offset %r15, -24
.Lcfi73:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movl	%esi, %r14d
	movq	%rdi, %r15
	movl	$168, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp185:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZN8NArchive4NCab9CDatabaseC2ERKS1_
.Ltmp186:
# BB#1:                                 # %.noexc
	movq	160(%rbp), %rdi
	movq	%rdi, 160(%rbx)
	testq	%rdi, %rdi
	je	.LBB10_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp188:
	callq	*8(%rax)
.Ltmp189:
.LBB10_3:                               # %_ZN8NArchive4NCab11CDatabaseExC2ERKS1_.exit
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
	movq	16(%r15), %rax
	movslq	%r14d, %rcx
	movq	%rbx, (%rax,%rcx,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_4:
.Ltmp190:
	movq	%rax, %rbp
.Ltmp191:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NCab9CDatabaseD2Ev
.Ltmp192:
	jmp	.LBB10_7
.LBB10_5:
.Ltmp193:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_6:
.Ltmp187:
	movq	%rax, %rbp
.LBB10_7:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6InsertEiRKS2_, .Lfunc_end10-_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6InsertEiRKS2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp185-.Lfunc_begin3  #   Call between .Lfunc_begin3 and .Ltmp185
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp185-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp186-.Ltmp185       #   Call between .Ltmp185 and .Ltmp186
	.long	.Ltmp187-.Lfunc_begin3  #     jumps to .Ltmp187
	.byte	0                       #   On action: cleanup
	.long	.Ltmp188-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Ltmp189-.Ltmp188       #   Call between .Ltmp188 and .Ltmp189
	.long	.Ltmp190-.Lfunc_begin3  #     jumps to .Ltmp190
	.byte	0                       #   On action: cleanup
	.long	.Ltmp189-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Ltmp191-.Ltmp189       #   Call between .Ltmp189 and .Ltmp191
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp191-.Lfunc_begin3  # >> Call Site 5 <<
	.long	.Ltmp192-.Ltmp191       #   Call between .Ltmp191 and .Ltmp192
	.long	.Ltmp193-.Lfunc_begin3  #     jumps to .Ltmp193
	.byte	1                       #   On action: 1
	.long	.Ltmp192-.Lfunc_begin3  # >> Call Site 6 <<
	.long	.Lfunc_end10-.Ltmp192   #   Call between .Ltmp192 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NCab8CHandler5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab8CHandler5CloseEv,@function
_ZN8NArchive4NCab8CHandler5CloseEv:     # @_ZN8NArchive4NCab8CHandler5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 16
.Lcfi75:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	16(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	48(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	80(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	addq	$112, %rbx
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end11:
	.size	_ZN8NArchive4NCab8CHandler5CloseEv, .Lfunc_end11-_ZN8NArchive4NCab8CHandler5CloseEv
	.cfi_endproc

	.globl	_ZN8NArchive4NCab16CFolderOutStream4InitEPKNS0_13CMvDatabaseExEPK13CRecordVectorIbEiyP23IArchiveExtractCallbackb
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab16CFolderOutStream4InitEPKNS0_13CMvDatabaseExEPK13CRecordVectorIbEiyP23IArchiveExtractCallbackb,@function
_ZN8NArchive4NCab16CFolderOutStream4InitEPKNS0_13CMvDatabaseExEPK13CRecordVectorIbEiyP23IArchiveExtractCallbackb: # @_ZN8NArchive4NCab16CFolderOutStream4InitEPKNS0_13CMvDatabaseExEPK13CRecordVectorIbEiyP23IArchiveExtractCallbackb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 32
.Lcfi79:
	.cfi_offset %rbx, -32
.Lcfi80:
	.cfi_offset %r14, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%rdi, %rbx
	movq	%rsi, 16(%rbx)
	movq	%rdx, 24(%rbx)
	movl	%ecx, 56(%rbx)
	movq	%r8, 96(%rbx)
	testq	%r14, %r14
	je	.LBB12_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB12_2:
	movb	32(%rsp), %bpl
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB12_4:                               # %_ZN9CMyComPtrI23IArchiveExtractCallbackEaSEPS0_.exit
	movq	%r14, 64(%rbx)
	movb	%bpl, 72(%rbx)
	movl	$0, 60(%rbx)
	movq	$0, 104(%rbx)
	movb	$0, 89(%rbx)
	movb	$1, 88(%rbx)
	movb	$0, 48(%rbx)
	movl	$0, 44(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN8NArchive4NCab16CFolderOutStream4InitEPKNS0_13CMvDatabaseExEPK13CRecordVectorIbEiyP23IArchiveExtractCallbackb, .Lfunc_end12-_ZN8NArchive4NCab16CFolderOutStream4InitEPKNS0_13CMvDatabaseExEPK13CRecordVectorIbEiyP23IArchiveExtractCallbackb
	.cfi_endproc

	.globl	_ZN8NArchive4NCab16CFolderOutStream18CloseFileWithResOpEi
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab16CFolderOutStream18CloseFileWithResOpEi,@function
_ZN8NArchive4NCab16CFolderOutStream18CloseFileWithResOpEi: # @_ZN8NArchive4NCab16CFolderOutStream18CloseFileWithResOpEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 32
.Lcfi85:
	.cfi_offset %rbx, -24
.Lcfi86:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 80(%rbx)
.LBB13_2:                               # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
	movb	$0, 89(%rbx)
	decl	44(%rbx)
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	movl	%ebp, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.Lfunc_end13:
	.size	_ZN8NArchive4NCab16CFolderOutStream18CloseFileWithResOpEi, .Lfunc_end13-_ZN8NArchive4NCab16CFolderOutStream18CloseFileWithResOpEi
	.cfi_endproc

	.globl	_ZN8NArchive4NCab16CFolderOutStream9CloseFileEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab16CFolderOutStream9CloseFileEv,@function
_ZN8NArchive4NCab16CFolderOutStream9CloseFileEv: # @_ZN8NArchive4NCab16CFolderOutStream9CloseFileEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 32
.Lcfi90:
	.cfi_offset %rbx, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%ebp, %ebp
	cmpb	$0, 88(%rbx)
	sete	%bpl
	addl	%ebp, %ebp
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 80(%rbx)
.LBB14_2:                               # %_ZN8NArchive4NCab16CFolderOutStream18CloseFileWithResOpEi.exit
	movb	$0, 89(%rbx)
	decl	44(%rbx)
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	movl	%ebp, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.Lfunc_end14:
	.size	_ZN8NArchive4NCab16CFolderOutStream9CloseFileEv, .Lfunc_end14-_ZN8NArchive4NCab16CFolderOutStream9CloseFileEv
	.cfi_endproc

	.globl	_ZN8NArchive4NCab16CFolderOutStream8OpenFileEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab16CFolderOutStream8OpenFileEv,@function
_ZN8NArchive4NCab16CFolderOutStream8OpenFileEv: # @_ZN8NArchive4NCab16CFolderOutStream8OpenFileEv
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi95:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi96:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi98:
	.cfi_def_cfa_offset 64
.Lcfi99:
	.cfi_offset %rbx, -56
.Lcfi100:
	.cfi_offset %r12, -48
.Lcfi101:
	.cfi_offset %r13, -40
.Lcfi102:
	.cfi_offset %r14, -32
.Lcfi103:
	.cfi_offset %r15, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	cmpl	$0, 44(%r12)
	je	.LBB15_2
# BB#1:                                 # %..critedge.thread_crit_edge
	leaq	24(%r12), %r14
	leaq	60(%r12), %r13
.LBB15_37:                              # %.critedge.thread
	movq	(%r14), %rcx
	movslq	(%r13), %rax
	movq	16(%rcx), %rcx
	cmpb	$0, (%rcx,%rax)
	je	.LBB15_38
# BB#39:
	movzbl	72(%r12), %r14d
	jmp	.LBB15_40
.LBB15_2:
	movslq	56(%r12), %r10
	movl	60(%r12), %r9d
	leal	(%r9,%r10), %eax
	movq	16(%r12), %rcx
	movq	24(%r12), %r8
	movq	16(%rcx), %r14
	movq	48(%rcx), %rbp
	cltq
	movslq	(%rbp,%rax,8), %rcx
	movq	(%r14,%rcx,8), %rcx
	movq	144(%rcx), %rcx
	movslq	4(%rbp,%rax,8), %rax
	movq	(%rcx,%rax,8), %r15
	movslq	12(%r8), %rsi
	xorl	%r11d, %r11d
	cmpl	%esi, %r9d
	movl	%r9d, %edx
	jge	.LBB15_10
# BB#3:                                 # %.lr.ph
	movslq	%r9d, %rdx
	movl	16(%r15), %eax
	leaq	4(%rbp,%r10,8), %rbp
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB15_4:                               # =>This Inner Loop Header: Depth=1
	movslq	-4(%rbp,%rdx,8), %rcx
	movq	(%r14,%rcx,8), %rcx
	movq	144(%rcx), %rcx
	movslq	(%rbp,%rdx,8), %rdi
	movq	(%rcx,%rdi,8), %rcx
	cmpl	16(%rcx), %eax
	jne	.LBB15_10
# BB#5:                                 #   in Loop: Header=BB15_4 Depth=1
	movl	20(%r15), %edi
	testl	%edi, %edi
	je	.LBB15_10
# BB#6:                                 #   in Loop: Header=BB15_4 Depth=1
	cmpl	20(%rcx), %edi
	jne	.LBB15_10
# BB#7:                                 #   in Loop: Header=BB15_4 Depth=1
	cmpb	$0, 72(%r12)
	jne	.LBB15_9
# BB#8:                                 #   in Loop: Header=BB15_4 Depth=1
	movq	16(%r8), %rcx
	movzbl	(%rcx,%rdx), %ecx
	addl	%ecx, %r11d
.LBB15_9:                               #   in Loop: Header=BB15_4 Depth=1
	incq	%rdx
	cmpq	%rsi, %rdx
	jl	.LBB15_4
.LBB15_10:                              # %._crit_edge
	leaq	60(%r12), %r13
	leaq	24(%r12), %r14
	subl	%r9d, %edx
	movl	$1, %eax
	cmovnel	%edx, %eax
	movl	%eax, 44(%r12)
	movb	$0, 48(%r12)
	cmpl	$2, %r11d
	jl	.LBB15_15
# BB#11:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB15_13
# BB#12:
	movl	20(%r15), %eax
	cmpl	40(%r12), %eax
	jbe	.LBB15_36
.LBB15_13:
	callq	MyFree
	movq	$0, 32(%r12)
	movl	20(%r15), %edi
	callq	MyAlloc
	movq	%rax, 32(%r12)
	movl	20(%r15), %ecx
	movl	%ecx, 40(%r12)
	testq	%rax, %rax
	je	.LBB15_14
.LBB15_36:
	movb	$1, 48(%r12)
	movl	16(%r15), %eax
	movl	%eax, 52(%r12)
	jmp	.LBB15_37
.LBB15_38:
	movl	$2, %r14d
.LBB15_40:
	movq	64(%r12), %rdi
	movq	(%rdi), %rbx
	movl	56(%r12), %esi
	addl	%eax, %esi
	leaq	80(%r12), %rbp
	movq	%rbp, %rdx
	movl	%r14d, %ecx
	callq	*56(%rbx)
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jne	.LBB15_44
# BB#41:
	cmpq	$0, (%rbp)
	jne	.LBB15_43
# BB#42:
	cmpb	$0, 72(%r12)
	movl	$2, %eax
	cmovel	%eax, %r14d
.LBB15_43:
	movq	64(%r12), %rdi
	movq	(%rdi), %rax
	movl	%r14d, %esi
	callq	*64(%rax)
	movl	%eax, %r15d
.LBB15_44:                              # %.critedge
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_15:
	cmpl	$1, %r11d
	jne	.LBB15_37
# BB#16:                                # %.preheader
                                        # implicit-def: %R15D
	.p2align	4, 0x90
.LBB15_17:                              # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movslq	(%r13), %rsi
	movq	16(%rax), %rax
	cmpb	$0, (%rax,%rsi)
	jne	.LBB15_37
# BB#18:                                #   in Loop: Header=BB15_17 Depth=1
	movq	$0, (%rsp)
	movq	64(%r12), %rdi
	movq	(%rdi), %rax
	addl	56(%r12), %esi
.Ltmp194:
	movl	$2, %ecx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%rsp, %rdx
	callq	*56(%rax)
.Ltmp195:
# BB#19:                                #   in Loop: Header=BB15_17 Depth=1
	testl	%eax, %eax
	je	.LBB15_24
.LBB15_20:                              #   in Loop: Header=BB15_17 Depth=1
	movl	$1, %ebp
	movl	%eax, %r15d
.LBB15_32:                              # %_ZN8NArchive4NCab16CFolderOutStream9CloseFileEv.exit
                                        #   in Loop: Header=BB15_17 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB15_33
	jmp	.LBB15_34
	.p2align	4, 0x90
.LBB15_24:                              #   in Loop: Header=BB15_17 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_26
# BB#25:                                #   in Loop: Header=BB15_17 Depth=1
	movl	$1, %ebp
	movl	$-2147467259, %r15d     # imm = 0x80004005
.LBB15_33:                              # %_ZN8NArchive4NCab16CFolderOutStream9CloseFileEv.exit.thread
                                        #   in Loop: Header=BB15_17 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB15_34:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
                                        #   in Loop: Header=BB15_17 Depth=1
	testl	%ebp, %ebp
	jne	.LBB15_44
# BB#35:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit._crit_edge
                                        #   in Loop: Header=BB15_17 Depth=1
	cmpl	$0, 44(%r12)
	jne	.LBB15_17
	jmp	.LBB15_37
.LBB15_26:                              #   in Loop: Header=BB15_17 Depth=1
	movq	64(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp196:
	movl	$2, %esi
	callq	*64(%rax)
.Ltmp197:
# BB#27:                                #   in Loop: Header=BB15_17 Depth=1
	testl	%eax, %eax
	jne	.LBB15_20
# BB#28:                                #   in Loop: Header=BB15_17 Depth=1
	incl	60(%r12)
	movb	$1, 89(%r12)
	xorl	%ebx, %ebx
	cmpb	$0, 88(%r12)
	sete	%bpl
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB15_31
# BB#29:                                #   in Loop: Header=BB15_17 Depth=1
	movq	(%rdi), %rax
.Ltmp198:
	callq	*16(%rax)
.Ltmp199:
# BB#30:                                # %.noexc
                                        #   in Loop: Header=BB15_17 Depth=1
	movq	$0, 80(%r12)
.LBB15_31:                              # %_ZN8NArchive4NCab16CFolderOutStream18CloseFileWithResOpEi.exit.i
                                        #   in Loop: Header=BB15_17 Depth=1
	movb	%bpl, %bl
	movb	$0, 89(%r12)
	decl	44(%r12)
	addl	%ebx, %ebx
	movq	64(%r12), %rdi
	movq	(%rdi), %rax
	xorl	%ebp, %ebp
.Ltmp200:
	movl	%ebx, %esi
	callq	*72(%rax)
.Ltmp201:
	jmp	.LBB15_32
.LBB15_14:
	movl	$-2147024882, %r15d     # imm = 0x8007000E
	jmp	.LBB15_44
.LBB15_21:
.Ltmp202:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_23
# BB#22:
	movq	(%rdi), %rax
.Ltmp203:
	callq	*16(%rax)
.Ltmp204:
.LBB15_23:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit83
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB15_45:
.Ltmp205:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN8NArchive4NCab16CFolderOutStream8OpenFileEv, .Lfunc_end15-_ZN8NArchive4NCab16CFolderOutStream8OpenFileEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp194-.Lfunc_begin4  #   Call between .Lfunc_begin4 and .Ltmp194
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp194-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp195-.Ltmp194       #   Call between .Ltmp194 and .Ltmp195
	.long	.Ltmp202-.Lfunc_begin4  #     jumps to .Ltmp202
	.byte	0                       #   On action: cleanup
	.long	.Ltmp195-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp196-.Ltmp195       #   Call between .Ltmp195 and .Ltmp196
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp196-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp201-.Ltmp196       #   Call between .Ltmp196 and .Ltmp201
	.long	.Ltmp202-.Lfunc_begin4  #     jumps to .Ltmp202
	.byte	0                       #   On action: cleanup
	.long	.Ltmp203-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp204-.Ltmp203       #   Call between .Ltmp203 and .Ltmp204
	.long	.Ltmp205-.Lfunc_begin4  #     jumps to .Ltmp205
	.byte	1                       #   On action: 1
	.long	.Ltmp204-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Lfunc_end15-.Ltmp204   #   Call between .Ltmp204 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NCab16CFolderOutStream15WriteEmptyFilesEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab16CFolderOutStream15WriteEmptyFilesEv,@function
_ZN8NArchive4NCab16CFolderOutStream15WriteEmptyFilesEv: # @_ZN8NArchive4NCab16CFolderOutStream15WriteEmptyFilesEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 32
.Lcfi108:
	.cfi_offset %rbx, -32
.Lcfi109:
	.cfi_offset %r14, -24
.Lcfi110:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	cmpb	$0, 89(%rbx)
	jne	.LBB16_11
# BB#1:                                 # %.preheader
	movl	60(%rbx), %eax
	movq	24(%rbx), %rcx
	cmpl	12(%rcx), %eax
	jge	.LBB16_11
# BB#2:                                 # %.lr.ph
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB16_3:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rcx
	movslq	56(%rbx), %rdx
	cltq
	addq	%rdx, %rax
	movq	16(%rcx), %rdx
	movq	48(%rcx), %rcx
	movslq	(%rcx,%rax,8), %rsi
	movq	(%rdx,%rsi,8), %rdx
	movq	144(%rdx), %rdx
	movslq	4(%rcx,%rax,8), %rax
	movq	(%rdx,%rax,8), %rax
	cmpl	$0, 20(%rax)
	jne	.LBB16_11
# BB#4:                                 #   in Loop: Header=BB16_3 Depth=1
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NCab16CFolderOutStream8OpenFileEv
	movl	%eax, %ebp
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB16_6
# BB#5:                                 #   in Loop: Header=BB16_3 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 80(%rbx)
.LBB16_6:                               # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
                                        #   in Loop: Header=BB16_3 Depth=1
	testl	%ebp, %ebp
	jne	.LBB16_7
# BB#8:                                 #   in Loop: Header=BB16_3 Depth=1
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	xorl	%esi, %esi
	callq	*72(%rax)
	testl	%eax, %eax
	jne	.LBB16_9
# BB#10:                                # %.thread
                                        #   in Loop: Header=BB16_3 Depth=1
	movl	60(%rbx), %eax
	incl	%eax
	movl	%eax, 60(%rbx)
	movq	24(%rbx), %rcx
	cmpl	12(%rcx), %eax
	jl	.LBB16_3
	jmp	.LBB16_11
.LBB16_7:
	movl	%ebp, %r14d
	jmp	.LBB16_11
.LBB16_9:
	movl	%eax, %r14d
.LBB16_11:                              # %.thread32
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end16:
	.size	_ZN8NArchive4NCab16CFolderOutStream15WriteEmptyFilesEv, .Lfunc_end16-_ZN8NArchive4NCab16CFolderOutStream15WriteEmptyFilesEv
	.cfi_endproc

	.globl	_ZN8NArchive4NCab16CFolderOutStream6Write2EPKvjPjb
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab16CFolderOutStream6Write2EPKvjPjb,@function
_ZN8NArchive4NCab16CFolderOutStream6Write2EPKvjPjb: # @_ZN8NArchive4NCab16CFolderOutStream6Write2EPKvjPjb
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi112:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi114:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi115:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi117:
	.cfi_def_cfa_offset 96
.Lcfi118:
	.cfi_offset %rbx, -56
.Lcfi119:
	.cfi_offset %r12, -48
.Lcfi120:
	.cfi_offset %r13, -40
.Lcfi121:
	.cfi_offset %r14, -32
.Lcfi122:
	.cfi_offset %r15, -24
.Lcfi123:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movq	%rcx, %r8
	movl	%edx, %ebp
	movq	%rsi, %r12
	movq	%rdi, %rbx
	testq	%r8, %r8
	je	.LBB17_2
# BB#1:
	movl	$0, (%r8)
.LBB17_2:                               # %.thread156.preheader
	xorl	%r15d, %r15d
                                        # implicit-def: %EAX
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	jmp	.LBB17_70
	.p2align	4, 0x90
.LBB17_3:                               # %.lr.ph
                                        #   in Loop: Header=BB17_70 Depth=1
	testq	%r8, %r8
	movb	89(%rbx), %cl
	je	.LBB17_15
	.p2align	4, 0x90
.LBB17_4:                               # %.lr.ph.split.us
                                        #   Parent Loop BB17_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	%cl, %cl
	jne	.LBB17_25
# BB#5:                                 #   in Loop: Header=BB17_4 Depth=2
	movslq	60(%rbx), %rax
	movq	24(%rbx), %rcx
	movl	$-2147467259, %r14d     # imm = 0x80004005
	cmpl	12(%rcx), %eax
	jge	.LBB17_84
# BB#6:                                 #   in Loop: Header=BB17_4 Depth=2
	movq	16(%rbx), %rcx
	movslq	56(%rbx), %rdx
	addq	%rax, %rdx
	movq	16(%rcx), %rax
	movq	48(%rcx), %rcx
	movslq	(%rcx,%rdx,8), %rsi
	movq	(%rax,%rsi,8), %rax
	movq	144(%rax), %rax
	movslq	4(%rcx,%rdx,8), %rcx
	movq	(%rax,%rcx,8), %rax
	movl	20(%rax), %ecx
	movl	%ecx, 92(%rbx)
	movl	16(%rax), %eax
	movq	104(%rbx), %rcx
	cmpq	%rcx, %rax
	jb	.LBB17_84
# BB#7:                                 #   in Loop: Header=BB17_4 Depth=2
	jbe	.LBB17_9
# BB#8:                                 #   in Loop: Header=BB17_4 Depth=2
	movl	%eax, %edx
	subl	%ecx, %edx
	cmpl	%ebp, %edx
	cmovael	%ebp, %edx
	addl	%edx, %r15d
	movl	%r15d, (%r8)
	addq	%rdx, %r12
	subl	%edx, %ebp
	addq	%rdx, %rcx
	movq	%rcx, 104(%rbx)
.LBB17_9:                               #   in Loop: Header=BB17_4 Depth=2
	cmpq	%rcx, %rax
	jne	.LBB17_13
# BB#10:                                #   in Loop: Header=BB17_4 Depth=2
.Ltmp231:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NCab16CFolderOutStream8OpenFileEv
	movl	%eax, %r14d
.Ltmp232:
# BB#11:                                #   in Loop: Header=BB17_4 Depth=2
	testl	%r14d, %r14d
	jne	.LBB17_84
# BB#12:                                #   in Loop: Header=BB17_4 Depth=2
	movb	$1, 89(%rbx)
	incl	60(%rbx)
	movb	$1, 88(%rbx)
	movb	$1, %cl
	movq	8(%rsp), %r8            # 8-byte Reload
	jmp	.LBB17_14
	.p2align	4, 0x90
.LBB17_13:                              #   in Loop: Header=BB17_4 Depth=2
	xorl	%ecx, %ecx
.LBB17_14:                              # %.thread156.backedge.us
                                        #   in Loop: Header=BB17_4 Depth=2
	testl	%ebp, %ebp
	jne	.LBB17_4
	jmp	.LBB17_71
	.p2align	4, 0x90
.LBB17_15:                              # %.lr.ph.split
                                        #   Parent Loop BB17_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	%cl, %cl
	jne	.LBB17_25
# BB#16:                                #   in Loop: Header=BB17_15 Depth=2
	movslq	60(%rbx), %rax
	movq	24(%rbx), %rcx
	movl	$-2147467259, %r14d     # imm = 0x80004005
	cmpl	12(%rcx), %eax
	jge	.LBB17_84
# BB#17:                                #   in Loop: Header=BB17_15 Depth=2
	movq	16(%rbx), %rcx
	movslq	56(%rbx), %rdx
	addq	%rax, %rdx
	movq	16(%rcx), %rax
	movq	48(%rcx), %rcx
	movslq	(%rcx,%rdx,8), %rsi
	movq	(%rax,%rsi,8), %rax
	movq	144(%rax), %rax
	movslq	4(%rcx,%rdx,8), %rcx
	movq	(%rax,%rcx,8), %rax
	movl	20(%rax), %ecx
	movl	%ecx, 92(%rbx)
	movl	16(%rax), %eax
	movq	104(%rbx), %rcx
	cmpq	%rcx, %rax
	jb	.LBB17_84
# BB#18:                                #   in Loop: Header=BB17_15 Depth=2
	jbe	.LBB17_20
# BB#19:                                #   in Loop: Header=BB17_15 Depth=2
	movl	%eax, %edx
	subl	%ecx, %edx
	cmpl	%ebp, %edx
	cmovael	%ebp, %edx
	addl	%edx, %r15d
	addq	%rdx, %r12
	subl	%edx, %ebp
	addq	%rdx, %rcx
	movq	%rcx, 104(%rbx)
.LBB17_20:                              #   in Loop: Header=BB17_15 Depth=2
	cmpq	%rcx, %rax
	jne	.LBB17_24
# BB#21:                                #   in Loop: Header=BB17_15 Depth=2
.Ltmp206:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NCab16CFolderOutStream8OpenFileEv
	movl	%eax, %r14d
.Ltmp207:
# BB#22:                                #   in Loop: Header=BB17_15 Depth=2
	testl	%r14d, %r14d
	movq	8(%rsp), %r8            # 8-byte Reload
	jne	.LBB17_84
# BB#23:                                #   in Loop: Header=BB17_15 Depth=2
	movb	$1, 89(%rbx)
	incl	60(%rbx)
	movb	$1, 88(%rbx)
	movb	$1, %cl
	testl	%ebp, %ebp
	jne	.LBB17_15
	jmp	.LBB17_71
	.p2align	4, 0x90
.LBB17_24:                              #   in Loop: Header=BB17_15 Depth=2
	xorl	%ecx, %ecx
	testl	%ebp, %ebp
	jne	.LBB17_15
	jmp	.LBB17_71
	.p2align	4, 0x90
.LBB17_25:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB17_70 Depth=1
	movl	92(%rbx), %r9d
	cmpl	%ebp, %r9d
	cmovael	%ebp, %r9d
	testl	%r9d, %r9d
	je	.LBB17_31
# BB#26:                                #   in Loop: Header=BB17_70 Depth=1
	testb	%r13b, %r13b
	jne	.LBB17_28
# BB#27:                                #   in Loop: Header=BB17_70 Depth=1
	movb	$0, 88(%rbx)
.LBB17_28:                              #   in Loop: Header=BB17_70 Depth=1
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_32
# BB#29:                                #   in Loop: Header=BB17_70 Depth=1
	movl	$0, 28(%rsp)
	movq	(%rdi), %rax
.Ltmp209:
	movq	%r12, %rsi
	movl	%r9d, %edx
	leaq	28(%rsp), %rcx
	callq	*40(%rax)
	movl	%eax, %r14d
.Ltmp210:
# BB#30:                                #   in Loop: Header=BB17_70 Depth=1
	movl	28(%rsp), %r9d
	movq	8(%rsp), %r8            # 8-byte Reload
	cmpb	$0, 48(%rbx)
	jne	.LBB17_33
	jmp	.LBB17_35
.LBB17_31:                              #   in Loop: Header=BB17_70 Depth=1
	xorl	%r14d, %r14d
	xorl	%r9d, %r9d
	jmp	.LBB17_35
.LBB17_32:                              #   in Loop: Header=BB17_70 Depth=1
	xorl	%r14d, %r14d
	cmpb	$0, 48(%rbx)
	je	.LBB17_35
.LBB17_33:                              #   in Loop: Header=BB17_70 Depth=1
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_35
# BB#34:                                #   in Loop: Header=BB17_70 Depth=1
	movq	104(%rbx), %rax
	movl	52(%rbx), %ecx
	subq	%rcx, %rax
	addq	%rax, %rdi
	movl	%r13d, 4(%rsp)          # 4-byte Spill
	movl	%r9d, %r13d
	movl	%r13d, %edx
	movq	%r12, %rsi
	callq	memcpy
	movl	%r13d, %r9d
	movl	4(%rsp), %r13d          # 4-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB17_35:                              #   in Loop: Header=BB17_70 Depth=1
	addl	%r9d, %r15d
	testq	%r8, %r8
	je	.LBB17_37
# BB#36:                                #   in Loop: Header=BB17_70 Depth=1
	movl	%r15d, (%r8)
.LBB17_37:                              #   in Loop: Header=BB17_70 Depth=1
	movl	%r9d, %ecx
	movl	92(%rbx), %eax
	subl	%r9d, %eax
	movl	%eax, 92(%rbx)
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	addq	%rcx, 104(%rbx)
	testl	%r14d, %r14d
	jne	.LBB17_84
# BB#38:                                #   in Loop: Header=BB17_70 Depth=1
	testl	%eax, %eax
	je	.LBB17_40
.LBB17_39:                              #   in Loop: Header=BB17_70 Depth=1
	xorl	%eax, %eax
	testl	%r15d, %r15d
	setne	%al
	leal	(%rax,%rax,2), %ecx
	jmp	.LBB17_46
.LBB17_40:                              #   in Loop: Header=BB17_70 Depth=1
	movl	%r9d, 20(%rsp)          # 4-byte Spill
	movl	%r13d, 4(%rsp)          # 4-byte Spill
	xorl	%r14d, %r14d
	cmpb	$0, 88(%rbx)
	sete	%r13b
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_43
# BB#41:                                #   in Loop: Header=BB17_70 Depth=1
	movq	(%rdi), %rax
.Ltmp212:
	callq	*16(%rax)
.Ltmp213:
# BB#42:                                # %.noexc148
                                        #   in Loop: Header=BB17_70 Depth=1
	movq	$0, 80(%rbx)
.LBB17_43:                              # %_ZN8NArchive4NCab16CFolderOutStream18CloseFileWithResOpEi.exit.i147
                                        #   in Loop: Header=BB17_70 Depth=1
	movb	%r13b, %r14b
	addl	%r14d, %r14d
	movb	$0, 89(%rbx)
	decl	44(%rbx)
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp214:
	movl	%r14d, %esi
	callq	*72(%rax)
.Ltmp215:
# BB#44:                                # %_ZN8NArchive4NCab16CFolderOutStream9CloseFileEv.exit150
                                        #   in Loop: Header=BB17_70 Depth=1
	testl	%eax, %eax
	je	.LBB17_47
# BB#45:                                #   in Loop: Header=BB17_70 Depth=1
	movl	$1, %ecx
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	4(%rsp), %r13d          # 4-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movl	20(%rsp), %r9d          # 4-byte Reload
.LBB17_46:                              #   in Loop: Header=BB17_70 Depth=1
	addq	32(%rsp), %r12          # 8-byte Folded Reload
	subl	%r9d, %ebp
	andb	$3, %cl
	je	.LBB17_70
	jmp	.LBB17_72
.LBB17_47:                              # %.thread.preheader
                                        #   in Loop: Header=BB17_70 Depth=1
	movl	4(%rsp), %r13d          # 4-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movl	20(%rsp), %r9d          # 4-byte Reload
	jmp	.LBB17_68
.LBB17_48:                              #   in Loop: Header=BB17_68 Depth=2
.Ltmp217:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NCab16CFolderOutStream8OpenFileEv
	movl	%eax, %r14d
.Ltmp218:
# BB#49:                                #   in Loop: Header=BB17_68 Depth=2
	movb	$1, 89(%rbx)
	incl	60(%rbx)
	testl	%r14d, %r14d
	je	.LBB17_51
.LBB17_50:                              #   in Loop: Header=BB17_68 Depth=2
	cmpq	$0, 32(%rbx)
	jne	.LBB17_61
	jmp	.LBB17_56
.LBB17_51:                              #   in Loop: Header=BB17_68 Depth=2
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_54
# BB#52:                                #   in Loop: Header=BB17_68 Depth=2
	movq	32(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB17_55
# BB#53:                                #   in Loop: Header=BB17_68 Depth=2
	movq	104(%rbx), %rdx
	movl	52(%rbx), %eax
	subq	%rax, %rdx
.Ltmp220:
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	%eax, %r14d
.Ltmp221:
	jmp	.LBB17_50
.LBB17_54:                              #   in Loop: Header=BB17_68 Depth=2
	xorl	%r14d, %r14d
	cmpq	$0, 32(%rbx)
	jne	.LBB17_61
	jmp	.LBB17_56
.LBB17_55:                              #   in Loop: Header=BB17_68 Depth=2
	xorl	%r14d, %r14d
.LBB17_56:                              # %.thread210
                                        #   in Loop: Header=BB17_68 Depth=2
	cmpb	$0, 48(%rbx)
	je	.LBB17_61
# BB#57:                                #   in Loop: Header=BB17_68 Depth=2
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_67
# BB#58:                                #   in Loop: Header=BB17_68 Depth=2
	movq	(%rdi), %rax
.Ltmp222:
	callq	*16(%rax)
.Ltmp223:
# BB#59:                                # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit.i
                                        #   in Loop: Header=BB17_68 Depth=2
	movq	$0, 80(%rbx)
	movb	$0, 89(%rbx)
	decl	44(%rbx)
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp224:
	movl	$1, %esi
	callq	*72(%rax)
.Ltmp225:
# BB#60:                                # %_ZN8NArchive4NCab16CFolderOutStream18CloseFileWithResOpEi.exit
                                        #   in Loop: Header=BB17_68 Depth=2
	testl	%eax, %eax
	je	.LBB17_66
	jmp	.LBB17_74
.LBB17_61:                              #   in Loop: Header=BB17_68 Depth=2
	xorl	%r13d, %r13d
	cmpb	$0, 88(%rbx)
	movq	80(%rbx), %rdi
	sete	%r13b
	addl	%r13d, %r13d
	testq	%rdi, %rdi
	je	.LBB17_64
# BB#62:                                #   in Loop: Header=BB17_68 Depth=2
	movq	(%rdi), %rax
.Ltmp226:
	callq	*16(%rax)
.Ltmp227:
# BB#63:                                # %.noexc
                                        #   in Loop: Header=BB17_68 Depth=2
	movq	$0, 80(%rbx)
.LBB17_64:                              # %_ZN8NArchive4NCab16CFolderOutStream18CloseFileWithResOpEi.exit.i
                                        #   in Loop: Header=BB17_68 Depth=2
	movb	$0, 89(%rbx)
	decl	44(%rbx)
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp228:
	movl	%r13d, %esi
	callq	*72(%rax)
.Ltmp229:
# BB#65:                                # %_ZN8NArchive4NCab16CFolderOutStream9CloseFileEv.exit
                                        #   in Loop: Header=BB17_68 Depth=2
	testl	%eax, %eax
	jne	.LBB17_74
.LBB17_66:                              #   in Loop: Header=BB17_68 Depth=2
	testl	%r14d, %r14d
	movl	4(%rsp), %r13d          # 4-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movl	20(%rsp), %r9d          # 4-byte Reload
	je	.LBB17_68
	jmp	.LBB17_84
.LBB17_67:                              # %.thread215
                                        #   in Loop: Header=BB17_68 Depth=2
	xorl	%r13d, %r13d
	cmpb	$0, 88(%rbx)
	sete	%r13b
	addl	%r13d, %r13d
	jmp	.LBB17_64
.LBB17_68:                              # %.thread
                                        #   Parent Loop BB17_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, 44(%rbx)
	jne	.LBB17_48
# BB#69:                                #   in Loop: Header=BB17_70 Depth=1
	movb	$0, 48(%rbx)
	jmp	.LBB17_39
	.p2align	4, 0x90
.LBB17_70:                              # %.thread156.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_15 Depth 2
                                        #     Child Loop BB17_4 Depth 2
                                        #     Child Loop BB17_68 Depth 2
	testl	%ebp, %ebp
	jne	.LBB17_3
.LBB17_71:                              # %.loopexit162
.Ltmp234:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NCab16CFolderOutStream15WriteEmptyFilesEv
	movl	%eax, %r14d
.Ltmp235:
	jmp	.LBB17_84
.LBB17_72:
	cmpb	$3, %cl
	movl	24(%rsp), %r14d         # 4-byte Reload
	jne	.LBB17_84
	jmp	.LBB17_71
.LBB17_74:
	movl	%eax, %r14d
	jmp	.LBB17_84
.LBB17_75:
.Ltmp236:
	jmp	.LBB17_82
.LBB17_76:
.Ltmp211:
	jmp	.LBB17_82
.LBB17_77:                              # %.loopexit.split-lp
.Ltmp216:
	jmp	.LBB17_82
.LBB17_78:                              # %.loopexit
.Ltmp219:
	jmp	.LBB17_82
.LBB17_79:
.Ltmp230:
	jmp	.LBB17_82
.LBB17_80:                              # %.us-lcssa181
.Ltmp208:
	jmp	.LBB17_82
.LBB17_81:                              # %.us-lcssa181.us
.Ltmp233:
.LBB17_82:
	movq	%rdx, %rbx
	movq	%rax, %rcx
	movq	%rcx, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbp
	cmpl	$2, %ebx
	je	.LBB17_85
# BB#83:
	callq	__cxa_end_catch
	movl	$-2147024882, %r14d     # imm = 0x8007000E
.LBB17_84:                              # %.thread154
	movl	%r14d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_85:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbp, (%rax)
.Ltmp237:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp238:
# BB#86:
.LBB17_87:
.Ltmp239:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end17:
	.size	_ZN8NArchive4NCab16CFolderOutStream6Write2EPKvjPjb, .Lfunc_end17-_ZN8NArchive4NCab16CFolderOutStream6Write2EPKvjPjb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\236\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp231-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp232-.Ltmp231       #   Call between .Ltmp231 and .Ltmp232
	.long	.Ltmp233-.Lfunc_begin5  #     jumps to .Ltmp233
	.byte	3                       #   On action: 2
	.long	.Ltmp206-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp207-.Ltmp206       #   Call between .Ltmp206 and .Ltmp207
	.long	.Ltmp208-.Lfunc_begin5  #     jumps to .Ltmp208
	.byte	3                       #   On action: 2
	.long	.Ltmp209-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp210-.Ltmp209       #   Call between .Ltmp209 and .Ltmp210
	.long	.Ltmp211-.Lfunc_begin5  #     jumps to .Ltmp211
	.byte	3                       #   On action: 2
	.long	.Ltmp210-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp212-.Ltmp210       #   Call between .Ltmp210 and .Ltmp212
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp212-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp215-.Ltmp212       #   Call between .Ltmp212 and .Ltmp215
	.long	.Ltmp216-.Lfunc_begin5  #     jumps to .Ltmp216
	.byte	3                       #   On action: 2
	.long	.Ltmp217-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp218-.Ltmp217       #   Call between .Ltmp217 and .Ltmp218
	.long	.Ltmp219-.Lfunc_begin5  #     jumps to .Ltmp219
	.byte	3                       #   On action: 2
	.long	.Ltmp220-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp229-.Ltmp220       #   Call between .Ltmp220 and .Ltmp229
	.long	.Ltmp230-.Lfunc_begin5  #     jumps to .Ltmp230
	.byte	3                       #   On action: 2
	.long	.Ltmp234-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp235-.Ltmp234       #   Call between .Ltmp234 and .Ltmp235
	.long	.Ltmp236-.Lfunc_begin5  #     jumps to .Ltmp236
	.byte	3                       #   On action: 2
	.long	.Ltmp235-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp237-.Ltmp235       #   Call between .Ltmp235 and .Ltmp237
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp237-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp238-.Ltmp237       #   Call between .Ltmp237 and .Ltmp238
	.long	.Ltmp239-.Lfunc_begin5  #     jumps to .Ltmp239
	.byte	0                       #   On action: cleanup
	.long	.Ltmp238-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Lfunc_end17-.Ltmp238   #   Call between .Ltmp238 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NCab16CFolderOutStream5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab16CFolderOutStream5WriteEPKvjPj,@function
_ZN8NArchive4NCab16CFolderOutStream5WriteEPKvjPj: # @_ZN8NArchive4NCab16CFolderOutStream5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	movl	$1, %r8d
	jmp	_ZN8NArchive4NCab16CFolderOutStream6Write2EPKvjPjb # TAILCALL
.Lfunc_end18:
	.size	_ZN8NArchive4NCab16CFolderOutStream5WriteEPKvjPj, .Lfunc_end18-_ZN8NArchive4NCab16CFolderOutStream5WriteEPKvjPj
	.cfi_endproc

	.globl	_ZN8NArchive4NCab16CFolderOutStream14FlushCorruptedEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab16CFolderOutStream14FlushCorruptedEv,@function
_ZN8NArchive4NCab16CFolderOutStream14FlushCorruptedEv: # @_ZN8NArchive4NCab16CFolderOutStream14FlushCorruptedEv
	.cfi_startproc
# BB#0:                                 # %.thread.preheader
	pushq	%rbp
.Lcfi124:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi125:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi126:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi127:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 48
	subq	$1040, %rsp             # imm = 0x410
.Lcfi129:
	.cfi_def_cfa_offset 1088
.Lcfi130:
	.cfi_offset %rbx, -48
.Lcfi131:
	.cfi_offset %r12, -40
.Lcfi132:
	.cfi_offset %r14, -32
.Lcfi133:
	.cfi_offset %r15, -24
.Lcfi134:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	16(%rsp), %r15
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	movq	%r15, %rdi
	callq	memset
	movl	$1024, %r12d            # imm = 0x400
	leaq	12(%rsp), %rbp
	.p2align	4, 0x90
.LBB19_1:                               # %.thread
                                        # =>This Inner Loop Header: Depth=1
	movq	96(%rbx), %rdx
	subq	104(%rbx), %rdx
	je	.LBB19_4
# BB#2:                                 #   in Loop: Header=BB19_1 Depth=1
	cmpq	$1024, %rdx             # imm = 0x400
	cmovael	%r12d, %edx
	movl	$0, 12(%rsp)
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%r15, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%rbp, %rcx
	callq	_ZN8NArchive4NCab16CFolderOutStream6Write2EPKvjPjb
	testl	%eax, %eax
	je	.LBB19_1
# BB#3:
	movl	%eax, %r14d
.LBB19_4:                               # %.thread26
	movl	%r14d, %eax
	addq	$1040, %rsp             # imm = 0x410
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	_ZN8NArchive4NCab16CFolderOutStream14FlushCorruptedEv, .Lfunc_end19-_ZN8NArchive4NCab16CFolderOutStream14FlushCorruptedEv
	.cfi_endproc

	.globl	_ZN8NArchive4NCab16CFolderOutStream11UnsupportedEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab16CFolderOutStream11UnsupportedEv,@function
_ZN8NArchive4NCab16CFolderOutStream11UnsupportedEv: # @_ZN8NArchive4NCab16CFolderOutStream11UnsupportedEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi135:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi136:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi137:
	.cfi_def_cfa_offset 32
.Lcfi138:
	.cfi_offset %rbx, -24
.Lcfi139:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	60(%rbx), %eax
	movq	24(%rbx), %rcx
	xorl	%ebp, %ebp
	cmpl	12(%rcx), %eax
	jl	.LBB20_2
	jmp	.LBB20_7
	.p2align	4, 0x90
.LBB20_8:                               # %.thread
                                        #   in Loop: Header=BB20_2 Depth=1
	movl	60(%rbx), %eax
	incl	%eax
	movl	%eax, 60(%rbx)
	movq	24(%rbx), %rcx
	cmpl	12(%rcx), %eax
	jge	.LBB20_7
.LBB20_2:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NCab16CFolderOutStream8OpenFileEv
	cmpl	$1, %eax
	ja	.LBB20_6
# BB#3:                                 #   in Loop: Header=BB20_2 Depth=1
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB20_5
# BB#4:                                 #   in Loop: Header=BB20_2 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 80(%rbx)
.LBB20_5:                               # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$1, %esi
	callq	*72(%rax)
	testl	%eax, %eax
	je	.LBB20_8
.LBB20_6:
	movl	%eax, %ebp
.LBB20_7:                               # %.thread15
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end20:
	.size	_ZN8NArchive4NCab16CFolderOutStream11UnsupportedEv, .Lfunc_end20-_ZN8NArchive4NCab16CFolderOutStream11UnsupportedEv
	.cfi_endproc

	.globl	_ZN8NArchive4NCab8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab8CHandler7ExtractEPKjjiP23IArchiveExtractCallback,@function
_ZN8NArchive4NCab8CHandler7ExtractEPKjjiP23IArchiveExtractCallback: # @_ZN8NArchive4NCab8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi140:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi141:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi143:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi144:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi145:
	.cfi_def_cfa_offset 56
	subq	$1336, %rsp             # imm = 0x538
.Lcfi146:
	.cfi_def_cfa_offset 1392
.Lcfi147:
	.cfi_offset %rbx, -56
.Lcfi148:
	.cfi_offset %r12, -48
.Lcfi149:
	.cfi_offset %r13, -40
.Lcfi150:
	.cfi_offset %r14, -32
.Lcfi151:
	.cfi_offset %r15, -24
.Lcfi152:
	.cfi_offset %rbp, -16
	movq	%rsi, 224(%rsp)         # 8-byte Spill
	movq	%rdi, %r15
	cmpl	$-1, %edx
	movl	%edx, 132(%rsp)         # 4-byte Spill
	movl	%edx, %r14d
	jne	.LBB21_2
# BB#1:
	movl	60(%r15), %r14d
.LBB21_2:
	testl	%r14d, %r14d
	je	.LBB21_12
# BB#3:                                 # %.lr.ph1039
	movq	%r8, 80(%rsp)           # 8-byte Spill
	movq	32(%r15), %r9
	movq	64(%r15), %r12
	cmpl	$-1, 132(%rsp)          # 4-byte Folded Reload
	movl	%r14d, 92(%rsp)         # 4-byte Spill
	movl	%ecx, 112(%rsp)         # 4-byte Spill
	je	.LBB21_13
# BB#4:                                 # %.lr.ph1039.split.preheader
	movl	%r14d, %edi
	movl	$-2, %r11d
	xorl	%r8d, %r8d
	movq	224(%rsp), %rbp         # 8-byte Reload
	xorl	%r10d, %r10d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB21_5:                               # %.lr.ph1039.split
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rbp), %rax
	movslq	(%r12,%rax,8), %rdx
	movq	(%r9,%rdx,8), %rcx
	movq	144(%rcx), %rbx
	movslq	4(%r12,%rax,8), %rax
	movq	(%rbx,%rax,8), %rax
	testb	$16, 32(%rax)
	jne	.LBB21_11
# BB#6:                                 #   in Loop: Header=BB21_5 Depth=1
	movq	96(%r15), %rbx
	movl	(%rbx,%rdx,4), %r13d
	movzwl	28(%rax), %r14d
	movl	%r14d, %edx
	andl	$65533, %edx            # imm = 0xFFFD
	xorl	%ebx, %ebx
	cmpl	$65533, %edx            # imm = 0xFFFD
	je	.LBB21_10
# BB#7:                                 #   in Loop: Header=BB21_5 Depth=1
	cmpl	$65534, %r14d           # imm = 0xFFFE
	jb	.LBB21_9
# BB#8:                                 #   in Loop: Header=BB21_5 Depth=1
	movl	108(%rcx), %ebx
	decl	%ebx
	jmp	.LBB21_10
.LBB21_9:                               #   in Loop: Header=BB21_5 Depth=1
	movl	%r14d, %ebx
.LBB21_10:                              #   in Loop: Header=BB21_5 Depth=1
	addl	%r13d, %ebx
	cmpl	%r11d, %ebx
	cmoveq	%r8, %rsi
	addq	%rsi, %r10
	movl	16(%rax), %ecx
	movl	20(%rax), %esi
	addq	%rcx, %rsi
	movl	%ebx, %r11d
.LBB21_11:                              #   in Loop: Header=BB21_5 Depth=1
	addq	$4, %rbp
	decq	%rdi
	jne	.LBB21_5
	jmp	.LBB21_20
.LBB21_12:
	xorl	%eax, %eax
	jmp	.LBB21_285
.LBB21_13:                              # %.lr.ph1039.split.us.preheader
	movl	$-2, %r11d
	xorl	%r8d, %r8d
	xorl	%r10d, %r10d
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB21_14:                              # %.lr.ph1039.split.us
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ebp, %rax
	movslq	(%r12,%rax,8), %rcx
	movq	(%r9,%rcx,8), %rdx
	movq	144(%rdx), %rdi
	movslq	4(%r12,%rax,8), %rax
	movq	(%rdi,%rax,8), %rax
	testb	$16, 32(%rax)
	jne	.LBB21_19
# BB#15:                                #   in Loop: Header=BB21_14 Depth=1
	movq	96(%r15), %rdi
	movl	(%rdi,%rcx,4), %r13d
	movzwl	28(%rax), %edi
	movl	%edi, %ecx
	andl	$65533, %ecx            # imm = 0xFFFD
	xorl	%ebx, %ebx
	cmpl	$65533, %ecx            # imm = 0xFFFD
	je	.LBB21_18
# BB#16:                                #   in Loop: Header=BB21_14 Depth=1
	cmpl	$65533, %edi            # imm = 0xFFFD
	movl	%edi, %ebx
	jbe	.LBB21_18
# BB#17:                                #   in Loop: Header=BB21_14 Depth=1
	movl	108(%rdx), %ebx
	decl	%ebx
.LBB21_18:                              #   in Loop: Header=BB21_14 Depth=1
	addl	%r13d, %ebx
	cmpl	%r11d, %ebx
	cmoveq	%r8, %rsi
	addq	%rsi, %r10
	movl	16(%rax), %ecx
	movl	20(%rax), %esi
	addq	%rcx, %rsi
	movl	%ebx, %r11d
.LBB21_19:                              #   in Loop: Header=BB21_14 Depth=1
	incl	%ebp
	cmpl	%ebp, %r14d
	jne	.LBB21_14
.LBB21_20:                              # %._crit_edge1040
	addq	%r10, %rsi
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp240:
	movq	%rdi, %r12
	callq	*40(%rax)
.Ltmp241:
# BB#21:
.Ltmp243:
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp244:
# BB#22:
.Ltmp246:
	movq	%r13, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp247:
# BB#23:
	movq	(%r13), %rax
.Ltmp249:
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp250:
# BB#24:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit
.Ltmp252:
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp253:
# BB#25:
.Ltmp254:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp255:
# BB#26:
	movl	$0, 16(%r14)
	movl	$_ZTVN9NCompress10CCopyCoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress10CCopyCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r14)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%r14)
.Ltmp257:
	movq	%r14, %rdi
	callq	*_ZTVN9NCompress10CCopyCoderE+24(%rip)
.Ltmp258:
# BB#27:                                # %_ZN9CMyComPtrI14ICompressCoderEC2EPS0_.exit
.Ltmp260:
	movl	$56, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp261:
# BB#28:
	movl	$0, 8(%rbx)
	movq	$_ZTVN8NArchive4NCab17CCabBlockInStreamE+16, (%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 16(%rbx)
	movw	$0, 48(%rbx)
	movq	$0, 40(%rbx)
.Ltmp263:
	movq	%rbx, %rdi
	callq	*_ZTVN8NArchive4NCab17CCabBlockInStreamE+24(%rip)
.Ltmp264:
# BB#29:                                # %_ZN9CMyComPtrI19ISequentialInStreamEC2EPS0_.exit
.Ltmp266:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NCab17CCabBlockInStream6CreateEv
.Ltmp267:
# BB#30:
	testb	%al, %al
	je	.LBB21_208
# BB#31:                                # %.lr.ph1024
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movq	%r13, 32(%rsp)          # 8-byte Spill
	leaq	16(%rbx), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	addq	$40, %rbx
	movq	%rbx, 256(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	cmpl	$0, 112(%rsp)           # 4-byte Folded Reload
	setne	%cl
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 152(%rsp)
	movq	$1, 168(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 144(%rsp)
	leaq	16(%r15), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movl	$_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 288(%rsp)        # 16-byte Spill
	movl	$_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE+264, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 272(%rsp)        # 16-byte Spill
                                        # implicit-def: %EBP
	movl	$0, %eax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movl	$0, %edi
	movl	$0, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	%ecx, %r12d
	movl	%r12d, 16(%rsp)         # 4-byte Spill
	jmp	.LBB21_38
	.p2align	4, 0x90
.LBB21_37:                              # %.backedge
                                        #   in Loop: Header=BB21_38 Depth=1
	cmpl	92(%rsp), %edi          # 4-byte Folded Reload
	jb	.LBB21_38
	jmp	.LBB21_218
.LBB21_32:                              #   in Loop: Header=BB21_38 Depth=1
	movq	56(%rsp), %rbp          # 8-byte Reload
.LBB21_35:                              #   in Loop: Header=BB21_38 Depth=1
	movl	16(%rsp), %r12d         # 4-byte Reload
.LBB21_36:                              # %.loopexit759.thread1248
                                        #   in Loop: Header=BB21_38 Depth=1
	movl	$1, %r14d
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movl	%eax, %ebp
	jmp	.LBB21_204
.LBB21_33:                              #   in Loop: Header=BB21_38 Depth=1
	cmpl	$1, %eax
	movl	16(%rsp), %r12d         # 4-byte Reload
	jne	.LBB21_36
# BB#34:                                # %.loopexit759
                                        #   in Loop: Header=BB21_38 Depth=1
	movq	%rbp, %r13
	jmp	.LBB21_196
	.p2align	4, 0x90
.LBB21_38:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_61 Depth 2
                                        #     Child Loop BB21_67 Depth 2
                                        #       Child Loop BB21_74 Depth 3
                                        #     Child Loop BB21_79 Depth 2
                                        #       Child Loop BB21_85 Depth 3
                                        #     Child Loop BB21_156 Depth 2
                                        #       Child Loop BB21_157 Depth 3
                                        #     Child Loop BB21_198 Depth 2
                                        #     Child Loop BB21_133 Depth 2
	cmpl	$-1, 132(%rsp)          # 4-byte Folded Reload
	movl	%edi, %r13d
	je	.LBB21_40
# BB#39:                                #   in Loop: Header=BB21_38 Depth=1
	movl	%edi, %eax
	movq	224(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %r13d
.LBB21_40:                              #   in Loop: Header=BB21_38 Depth=1
	movq	32(%r15), %rcx
	movq	64(%r15), %r9
	movslq	%r13d, %rbx
	movslq	(%r9,%rbx,8), %rax
	movq	(%rcx,%rax,8), %r8
	movq	144(%r8), %rcx
	movslq	4(%r9,%rbx,8), %rdx
	movq	(%rcx,%rdx,8), %rdx
	incl	%edi
	testb	$16, 32(%rdx)
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	jne	.LBB21_44
# BB#41:                                #   in Loop: Header=BB21_38 Depth=1
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	movq	96(%r15), %rcx
	movslq	(%rcx,%rax,4), %r10
	movzwl	28(%rdx), %eax
	movl	%eax, %esi
	andl	$65533, %esi            # imm = 0xFFFD
	xorl	%ecx, %ecx
	cmpl	$65533, %esi            # imm = 0xFFFD
	leaq	144(%rsp), %r12
	je	.LBB21_57
# BB#42:                                #   in Loop: Header=BB21_38 Depth=1
	cmpl	$65534, %eax            # imm = 0xFFFE
	jb	.LBB21_56
# BB#43:                                #   in Loop: Header=BB21_38 Depth=1
	movl	108(%r8), %ecx
	decl	%ecx
	jmp	.LBB21_57
	.p2align	4, 0x90
.LBB21_44:                              #   in Loop: Header=BB21_38 Depth=1
	movq	$0, 120(%rsp)
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp269:
	movl	%r13d, %esi
	leaq	120(%rsp), %rdx
	movl	%r12d, %ecx
	callq	*56(%rax)
	movl	%eax, %ebx
.Ltmp270:
# BB#45:                                #   in Loop: Header=BB21_38 Depth=1
	testl	%ebx, %ebx
	cmovnel	%ebx, %ebp
	movl	$1, %r14d
	jne	.LBB21_53
# BB#46:                                #   in Loop: Header=BB21_38 Depth=1
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp271:
	movl	%r12d, %esi
	callq	*64(%rax)
	movl	%eax, %ebx
.Ltmp272:
# BB#47:                                #   in Loop: Header=BB21_38 Depth=1
	testl	%ebx, %ebx
	cmovnel	%ebx, %ebp
	jne	.LBB21_53
# BB#48:                                #   in Loop: Header=BB21_38 Depth=1
	movq	120(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_51
# BB#49:                                #   in Loop: Header=BB21_38 Depth=1
	movq	(%rdi), %rax
.Ltmp273:
	callq	*16(%rax)
.Ltmp274:
# BB#50:                                # %.noexc627
                                        #   in Loop: Header=BB21_38 Depth=1
	movq	$0, 120(%rsp)
.LBB21_51:                              # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
                                        #   in Loop: Header=BB21_38 Depth=1
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp275:
	xorl	%esi, %esi
	callq	*72(%rax)
.Ltmp276:
# BB#52:                                #   in Loop: Header=BB21_38 Depth=1
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setne	%r14b
	cmovnel	%eax, %ebp
	movl	$6, %eax
	cmovel	%eax, %r14d
	movl	%ebp, %ebx
	.p2align	4, 0x90
.LBB21_53:                              #   in Loop: Header=BB21_38 Depth=1
	movq	120(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_55
# BB#54:                                #   in Loop: Header=BB21_38 Depth=1
	movq	(%rdi), %rax
.Ltmp280:
	callq	*16(%rax)
.Ltmp281:
.LBB21_55:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit629
                                        #   in Loop: Header=BB21_38 Depth=1
	movl	%ebx, %ebp
	jmp	.LBB21_205
.LBB21_56:                              #   in Loop: Header=BB21_38 Depth=1
	movl	%eax, %ecx
.LBB21_57:                              #   in Loop: Header=BB21_38 Depth=1
	movslq	%ecx, %rcx
	addq	%r10, %rcx
	testl	%ecx, %ecx
	js	.LBB21_96
# BB#58:                                #   in Loop: Header=BB21_38 Depth=1
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%rbx, 248(%rsp)         # 8-byte Spill
	movq	%r9, 136(%rsp)          # 8-byte Spill
	movq	%r8, 184(%rsp)          # 8-byte Spill
	movq	128(%r15), %rax
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	(%rax,%rcx,4), %ebx
.Ltmp283:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp284:
# BB#59:                                # %.preheader761
                                        #   in Loop: Header=BB21_38 Depth=1
	cmpl	%r13d, %ebx
	movl	%ebx, %r14d
	jge	.LBB21_63
# BB#60:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB21_38 Depth=1
	movl	%ebx, %r14d
	.p2align	4, 0x90
.LBB21_61:                              # %.lr.ph
                                        #   Parent Loop BB21_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp285:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp286:
# BB#62:                                #   in Loop: Header=BB21_61 Depth=2
	movq	160(%rsp), %rax
	movslq	156(%rsp), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 156(%rsp)
	incl	%r14d
	cmpl	%r13d, %r14d
	jl	.LBB21_61
.LBB21_63:                              # %._crit_edge
                                        #   in Loop: Header=BB21_38 Depth=1
.Ltmp288:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp289:
# BB#64:                                #   in Loop: Header=BB21_38 Depth=1
	movl	%ebx, 232(%rsp)         # 4-byte Spill
	movq	160(%rsp), %rax
	movslq	156(%rsp), %rcx
	movb	$1, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 156(%rsp)
	movq	72(%rsp), %rcx          # 8-byte Reload
	movl	16(%rcx), %eax
	movl	20(%rcx), %ecx
	addq	%rax, %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	movq	112(%rsp), %rbx         # 8-byte Reload
	cmpl	92(%rsp), %ebx          # 4-byte Folded Reload
	jae	.LBB21_90
# BB#65:                                # %.lr.ph968
                                        #   in Loop: Header=BB21_38 Depth=1
	incl	%r14d
	cmpl	$-1, 132(%rsp)          # 4-byte Folded Reload
	je	.LBB21_79
# BB#66:                                # %.lr.ph968.split.preheader
                                        #   in Loop: Header=BB21_38 Depth=1
	movl	%ebx, %ebx
	.p2align	4, 0x90
.LBB21_67:                              # %.lr.ph968.split
                                        #   Parent Loop BB21_38 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB21_74 Depth 3
	movq	224(%rsp), %rax         # 8-byte Reload
	movslq	(%rax,%rbx,4), %rbp
	movq	32(%r15), %rcx
	movq	64(%r15), %rdx
	movslq	(%rdx,%rbp,8), %rax
	movq	(%rcx,%rax,8), %rcx
	movq	144(%rcx), %rsi
	movslq	4(%rdx,%rbp,8), %rdx
	movq	(%rsi,%rdx,8), %r13
	testb	$16, 32(%r13)
	jne	.LBB21_78
# BB#68:                                #   in Loop: Header=BB21_67 Depth=2
	movq	96(%r15), %rdx
	movl	(%rdx,%rax,4), %eax
	movzwl	28(%r13), %esi
	movl	%esi, %edi
	andl	$65533, %edi            # imm = 0xFFFD
	xorl	%edx, %edx
	cmpl	$65533, %edi            # imm = 0xFFFD
	je	.LBB21_72
# BB#69:                                #   in Loop: Header=BB21_67 Depth=2
	cmpl	$65534, %esi            # imm = 0xFFFE
	jb	.LBB21_71
# BB#70:                                #   in Loop: Header=BB21_67 Depth=2
	movl	108(%rcx), %edx
	decl	%edx
	jmp	.LBB21_72
.LBB21_71:                              #   in Loop: Header=BB21_67 Depth=2
	movl	%esi, %edx
.LBB21_72:                              #   in Loop: Header=BB21_67 Depth=2
	addl	%eax, %edx
	cmpl	48(%rsp), %edx          # 4-byte Folded Reload
	je	.LBB21_74
	jmp	.LBB21_90
	.p2align	4, 0x90
.LBB21_73:                              #   in Loop: Header=BB21_74 Depth=3
	movq	160(%rsp), %rax
	movslq	156(%rsp), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 156(%rsp)
	incl	%r14d
.LBB21_74:                              # %.preheader
                                        #   Parent Loop BB21_38 Depth=1
                                        #     Parent Loop BB21_67 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ebp, %r14d
	jge	.LBB21_76
# BB#75:                                # %.lr.ph960
                                        #   in Loop: Header=BB21_74 Depth=3
.Ltmp291:
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp292:
	jmp	.LBB21_73
.LBB21_76:                              # %._crit_edge961
                                        #   in Loop: Header=BB21_67 Depth=2
.Ltmp294:
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp295:
# BB#77:                                #   in Loop: Header=BB21_67 Depth=2
	movq	160(%rsp), %rax
	movslq	156(%rsp), %rcx
	movb	$1, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 156(%rsp)
	incl	%r14d
	movl	16(%r13), %eax
	movl	20(%r13), %ecx
	addq	%rax, %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
.LBB21_78:                              #   in Loop: Header=BB21_67 Depth=2
	incq	%rbx
	cmpl	92(%rsp), %ebx          # 4-byte Folded Reload
	jb	.LBB21_67
	jmp	.LBB21_90
	.p2align	4, 0x90
.LBB21_79:                              # %.lr.ph968.split.us
                                        #   Parent Loop BB21_38 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB21_85 Depth 3
	movq	32(%r15), %rax
	movq	64(%r15), %rdx
	movslq	%ebx, %rsi
	movslq	(%rdx,%rsi,8), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	144(%rax), %rdi
	movslq	4(%rdx,%rsi,8), %rdx
	movq	(%rdi,%rdx,8), %rbp
	testb	$16, 32(%rbp)
	jne	.LBB21_89
# BB#80:                                #   in Loop: Header=BB21_79 Depth=2
	movq	96(%r15), %rdx
	movl	(%rdx,%rcx,4), %ecx
	movzwl	28(%rbp), %esi
	movl	%esi, %edi
	andl	$65533, %edi            # imm = 0xFFFD
	xorl	%edx, %edx
	cmpl	$65533, %edi            # imm = 0xFFFD
	je	.LBB21_83
# BB#81:                                #   in Loop: Header=BB21_79 Depth=2
	cmpl	$65533, %esi            # imm = 0xFFFD
	movl	%esi, %edx
	jbe	.LBB21_83
# BB#82:                                #   in Loop: Header=BB21_79 Depth=2
	movl	108(%rax), %edx
	decl	%edx
.LBB21_83:                              #   in Loop: Header=BB21_79 Depth=2
	addl	%ecx, %edx
	cmpl	48(%rsp), %edx          # 4-byte Folded Reload
	jne	.LBB21_90
# BB#84:                                # %.preheader.us
                                        #   in Loop: Header=BB21_79 Depth=2
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movq	%rbx, %r13
	cmpl	%ebx, %r14d
	jge	.LBB21_87
	.p2align	4, 0x90
.LBB21_85:                              # %.lr.ph960.us
                                        #   Parent Loop BB21_38 Depth=1
                                        #     Parent Loop BB21_79 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
.Ltmp297:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp298:
# BB#86:                                #   in Loop: Header=BB21_85 Depth=3
	movq	160(%rsp), %rax
	movslq	156(%rsp), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 156(%rsp)
	incl	%r14d
	cmpl	%r13d, %r14d
	jl	.LBB21_85
.LBB21_87:                              # %._crit_edge961.us
                                        #   in Loop: Header=BB21_79 Depth=2
.Ltmp300:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp301:
# BB#88:                                #   in Loop: Header=BB21_79 Depth=2
	movq	160(%rsp), %rax
	movslq	156(%rsp), %rcx
	movb	$1, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 156(%rsp)
	incl	%r14d
	movq	112(%rsp), %rcx         # 8-byte Reload
	movl	16(%rcx), %eax
	movl	20(%rcx), %ecx
	addq	%rax, %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	movq	%r13, %rbx
.LBB21_89:                              #   in Loop: Header=BB21_79 Depth=2
	incl	%ebx
	cmpl	92(%rsp), %ebx          # 4-byte Folded Reload
	jb	.LBB21_79
.LBB21_90:                              # %._crit_edge969
                                        #   in Loop: Header=BB21_38 Depth=1
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	%rax, 56(%r13)
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, 48(%r13)
.Ltmp303:
	movq	%r13, %rdi
	callq	_ZN14CLocalProgress6SetCurEv
.Ltmp304:
	movl	20(%rsp), %ebp          # 4-byte Reload
	movq	184(%rsp), %r14         # 8-byte Reload
	movq	72(%rsp), %rbx          # 8-byte Reload
# BB#91:                                #   in Loop: Header=BB21_38 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %ebp
	jne	.LBB21_221
# BB#92:                                #   in Loop: Header=BB21_38 Depth=1
.Ltmp305:
	movl	$112, %edi
	callq	_Znwm
	movq	%rax, %rsi
.Ltmp306:
# BB#93:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit
                                        #   in Loop: Header=BB21_38 Depth=1
	movq	$_ZTVN8NArchive4NCab16CFolderOutStreamE+16, (%rsi)
	movq	$0, 32(%rsi)
	movq	$0, 64(%rsi)
	movq	$0, 80(%rsi)
	movl	$1, 8(%rsi)
	movzwl	28(%rbx), %ecx
	movl	%ecx, %edx
	andl	$65533, %edx            # imm = 0xFFFD
	xorl	%eax, %eax
	cmpl	$65533, %edx            # imm = 0xFFFD
	movl	232(%rsp), %edx         # 4-byte Reload
	je	.LBB21_109
# BB#94:                                #   in Loop: Header=BB21_38 Depth=1
	cmpl	$65534, %ecx            # imm = 0xFFFE
	jb	.LBB21_108
# BB#95:                                #   in Loop: Header=BB21_38 Depth=1
	movl	108(%r14), %eax
	decl	%eax
	jmp	.LBB21_109
.LBB21_96:                              #   in Loop: Header=BB21_38 Depth=1
	movq	$0, 104(%rsp)
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp380:
	movl	%r13d, %esi
	leaq	104(%rsp), %rdx
	movl	16(%rsp), %r12d         # 4-byte Reload
	movl	%r12d, %ecx
	callq	*56(%rax)
	movl	%eax, %ebx
.Ltmp381:
	movl	20(%rsp), %ebp          # 4-byte Reload
# BB#97:                                #   in Loop: Header=BB21_38 Depth=1
	testl	%ebx, %ebx
	cmovnel	%ebx, %ebp
	movl	$1, %r14d
	jne	.LBB21_105
# BB#98:                                #   in Loop: Header=BB21_38 Depth=1
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp382:
	movl	%r12d, %esi
	callq	*64(%rax)
	movl	%eax, %ebx
.Ltmp383:
# BB#99:                                #   in Loop: Header=BB21_38 Depth=1
	testl	%ebx, %ebx
	cmovnel	%ebx, %ebp
	jne	.LBB21_105
# BB#100:                               #   in Loop: Header=BB21_38 Depth=1
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_103
# BB#101:                               #   in Loop: Header=BB21_38 Depth=1
	movq	(%rdi), %rax
.Ltmp384:
	callq	*16(%rax)
.Ltmp385:
# BB#102:                               # %.noexc634
                                        #   in Loop: Header=BB21_38 Depth=1
	movq	$0, 104(%rsp)
.LBB21_103:                             # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit635
                                        #   in Loop: Header=BB21_38 Depth=1
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp386:
	movl	$2, %esi
	callq	*72(%rax)
.Ltmp387:
# BB#104:                               #   in Loop: Header=BB21_38 Depth=1
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setne	%r14b
	cmovnel	%eax, %ebp
	movl	$6, %eax
	cmovel	%eax, %r14d
	movl	%ebp, %ebx
.LBB21_105:                             #   in Loop: Header=BB21_38 Depth=1
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_107
# BB#106:                               #   in Loop: Header=BB21_38 Depth=1
	movq	(%rdi), %rax
.Ltmp391:
	callq	*16(%rax)
.Ltmp392:
.LBB21_107:                             # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit637
                                        #   in Loop: Header=BB21_38 Depth=1
	movl	%ebx, %ebp
	jmp	.LBB21_205
.LBB21_108:                             #   in Loop: Header=BB21_38 Depth=1
	movl	%ecx, %eax
.LBB21_109:                             # %_ZNK8NArchive4NCab5CItem14GetFolderIndexEi.exit660
                                        #   in Loop: Header=BB21_38 Depth=1
	movq	112(%r14), %rcx
	cltq
	movq	(%rcx,%rax,8), %r14
	movq	264(%rsp), %rax         # 8-byte Reload
	movq	%rax, 16(%rsi)
	leaq	144(%rsp), %rax
	movq	%rax, 24(%rsi)
	movl	%edx, 56(%rsi)
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	%rax, 96(%rsi)
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp308:
	callq	*8(%rax)
.Ltmp309:
# BB#110:                               # %.noexc664
                                        #   in Loop: Header=BB21_38 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	64(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB21_112
# BB#111:                               #   in Loop: Header=BB21_38 Depth=1
	movq	(%rdi), %rax
.Ltmp310:
	callq	*16(%rax)
.Ltmp311:
.LBB21_112:                             #   in Loop: Header=BB21_38 Depth=1
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, 64(%rbx)
	movl	16(%rsp), %eax          # 4-byte Reload
	movb	%al, 72(%rbx)
	movl	$0, 60(%rbx)
	movq	$0, 104(%rbx)
	movb	$0, 89(%rbx)
	movb	$1, 88(%rbx)
	movb	$0, 48(%rbx)
	movl	$0, 44(%rbx)
	movq	8(%rsp), %rax           # 8-byte Reload
	movb	$0, 49(%rax)
	movzbl	6(%r14), %eax
	movl	%eax, %ecx
	andb	$15, %cl
	cmpb	$3, %cl
	ja	.LBB21_132
# BB#113:                               #   in Loop: Header=BB21_38 Depth=1
	andl	$15, %eax
	jmpq	*.LJTI21_0(,%rax,8)
.LBB21_114:                             #   in Loop: Header=BB21_38 Depth=1
	cmpq	$0, 176(%rsp)           # 8-byte Folded Reload
	jne	.LBB21_149
# BB#115:                               #   in Loop: Header=BB21_38 Depth=1
.Ltmp332:
	movl	$3480, %edi             # imm = 0xD98
	callq	_Znwm
	movq	%rax, 176(%rsp)         # 8-byte Spill
.Ltmp333:
# BB#116:                               #   in Loop: Header=BB21_38 Depth=1
.Ltmp334:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	176(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderC2Ebb
.Ltmp335:
# BB#117:                               # %.noexc661
                                        #   in Loop: Header=BB21_38 Depth=1
	movaps	288(%rsp), %xmm0        # 16-byte Reload
	movups	%xmm0, (%rbx)
	movdqa	272(%rsp), %xmm0        # 16-byte Reload
	movdqu	%xmm0, 16(%rbx)
	movq	$_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE+328, 32(%rbx)
	incl	40(%rbx)
	movq	24(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB21_148
# BB#118:                               #   in Loop: Header=BB21_38 Depth=1
	movq	(%rdi), %rax
.Ltmp337:
	callq	*16(%rax)
.Ltmp338:
# BB#119:                               #   in Loop: Header=BB21_38 Depth=1
	movq	176(%rsp), %rax         # 8-byte Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB21_149
.LBB21_120:                             #   in Loop: Header=BB21_38 Depth=1
	movq	208(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	je	.LBB21_141
# BB#121:                               #   in Loop: Header=BB21_38 Depth=1
	movq	%rax, %rbx
	jmp	.LBB21_147
.LBB21_122:                             #   in Loop: Header=BB21_38 Depth=1
	movq	216(%rsp), %rdi         # 8-byte Reload
	testq	%rdi, %rdi
	jne	.LBB21_129
# BB#123:                               #   in Loop: Header=BB21_38 Depth=1
.Ltmp321:
	movl	$7416, %edi             # imm = 0x1CF8
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp322:
# BB#124:                               #   in Loop: Header=BB21_38 Depth=1
.Ltmp323:
	movl	%ebp, %r13d
	xorl	%esi, %esi
	movq	%rbx, %rbp
	movq	%rbp, %rdi
	callq	_ZN9NCompress4NLzx8CDecoderC1Eb
.Ltmp324:
# BB#125:                               #   in Loop: Header=BB21_38 Depth=1
	movq	(%rbp), %rax
.Ltmp326:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp327:
	movl	%r13d, %ebp
# BB#126:                               # %.noexc655
                                        #   in Loop: Header=BB21_38 Depth=1
	movq	64(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB21_128
# BB#127:                               #   in Loop: Header=BB21_38 Depth=1
	movq	(%rdi), %rax
.Ltmp328:
	callq	*16(%rax)
.Ltmp329:
.LBB21_128:                             #   in Loop: Header=BB21_38 Depth=1
	movq	%rbx, %rdi
	movq	%rdi, 64(%rsp)          # 8-byte Spill
.LBB21_129:                             # %_ZN9CMyComPtrI14ICompressCoderEaSEPS0_.exit657
                                        #   in Loop: Header=BB21_38 Depth=1
	movzbl	7(%r14), %esi
.Ltmp330:
	movq	%rdi, %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	callq	_ZN9NCompress4NLzx8CDecoder9SetParamsEj
.Ltmp331:
# BB#130:                               #   in Loop: Header=BB21_38 Depth=1
	testl	%eax, %eax
	jne	.LBB21_131
	jmp	.LBB21_150
.LBB21_132:                             #   in Loop: Header=BB21_38 Depth=1
	movq	24(%rbx), %rax
	xorl	%r12d, %r12d
	cmpl	$0, 12(%rax)
	jle	.LBB21_193
.LBB21_133:                             # %.lr.ph.i
                                        #   Parent Loop BB21_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp368:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NCab16CFolderOutStream8OpenFileEv
.Ltmp369:
# BB#134:                               # %.noexc644
                                        #   in Loop: Header=BB21_133 Depth=2
	cmpl	$1, %eax
	ja	.LBB21_192
# BB#135:                               #   in Loop: Header=BB21_133 Depth=2
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_138
# BB#136:                               #   in Loop: Header=BB21_133 Depth=2
	movq	(%rdi), %rax
.Ltmp370:
	callq	*16(%rax)
.Ltmp371:
# BB#137:                               # %.noexc645
                                        #   in Loop: Header=BB21_133 Depth=2
	movq	$0, 80(%rbx)
.LBB21_138:                             # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit.i
                                        #   in Loop: Header=BB21_133 Depth=2
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp372:
	movl	$1, %esi
	callq	*72(%rax)
.Ltmp373:
# BB#139:                               # %.noexc646
                                        #   in Loop: Header=BB21_133 Depth=2
	testl	%eax, %eax
	jne	.LBB21_192
# BB#140:                               # %.thread.i643
                                        #   in Loop: Header=BB21_133 Depth=2
	movl	60(%rbx), %eax
	incl	%eax
	movl	%eax, 60(%rbx)
	movq	24(%rbx), %rcx
	cmpl	12(%rcx), %eax
	jl	.LBB21_133
	jmp	.LBB21_193
.LBB21_141:                             #   in Loop: Header=BB21_38 Depth=1
.Ltmp312:
	movl	$2024, %edi             # imm = 0x7E8
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp313:
# BB#142:                               #   in Loop: Header=BB21_38 Depth=1
.Ltmp314:
	movq	%rbx, %r12
	movq	%r12, %rdi
	callq	_ZN9NCompress8NQuantum8CDecoderC2Ev
.Ltmp315:
# BB#143:                               #   in Loop: Header=BB21_38 Depth=1
	movq	(%r12), %rax
.Ltmp317:
	movq	%r12, %rdi
	callq	*8(%rax)
.Ltmp318:
# BB#144:                               # %.noexc647
                                        #   in Loop: Header=BB21_38 Depth=1
	movq	96(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB21_146
# BB#145:                               #   in Loop: Header=BB21_38 Depth=1
	movq	(%rdi), %rax
.Ltmp319:
	callq	*16(%rax)
.Ltmp320:
.LBB21_146:                             #   in Loop: Header=BB21_38 Depth=1
	movq	%rbx, 96(%rsp)          # 8-byte Spill
.LBB21_147:                             # %_ZN9CMyComPtrI14ICompressCoderEaSEPS0_.exit
                                        #   in Loop: Header=BB21_38 Depth=1
	movzbl	7(%r14), %eax
	movq	%rbx, %rdx
	movq	%rdx, 208(%rsp)         # 8-byte Spill
	movl	%eax, 176(%rbx)
	jmp	.LBB21_150
.LBB21_192:                             #   in Loop: Header=BB21_38 Depth=1
	movl	%eax, %r12d
.LBB21_193:                             # %_ZN8NArchive4NCab16CFolderOutStream11UnsupportedEv.exit
                                        #   in Loop: Header=BB21_38 Depth=1
	xorl	%r14d, %r14d
	testl	%r12d, %r12d
	setne	%r14b
	cmovnel	%r12d, %ebp
	movl	$0, %eax
	movq	192(%rsp), %rcx         # 8-byte Reload
	cmovneq	%rax, %rcx
	movl	$6, %eax
	cmovel	%eax, %r14d
	addq	%rcx, 200(%rsp)         # 8-byte Folded Spill
	movl	16(%rsp), %r12d         # 4-byte Reload
	jmp	.LBB21_204
.LBB21_148:                             #   in Loop: Header=BB21_38 Depth=1
	movq	%rbx, 24(%rsp)          # 8-byte Spill
.LBB21_149:                             # %_ZN9CMyComPtrI14ICompressCoderEaSEPS0_.exit663
                                        #   in Loop: Header=BB21_38 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movb	$1, 49(%rax)
.LBB21_150:                             #   in Loop: Header=BB21_38 Depth=1
	movq	256(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax)
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	248(%rsp), %rcx         # 8-byte Reload
	movl	(%rax,%rcx,8), %r14d
	movq	72(%rsp), %rax          # 8-byte Reload
	movzwl	28(%rax), %eax
	movl	%eax, %ecx
	andl	$65533, %ecx            # imm = 0xFFFD
	xorl	%r13d, %r13d
	cmpl	$65533, %ecx            # imm = 0xFFFD
	je	.LBB21_154
# BB#151:                               #   in Loop: Header=BB21_38 Depth=1
	cmpl	$65534, %eax            # imm = 0xFFFE
	jb	.LBB21_153
# BB#152:                               #   in Loop: Header=BB21_38 Depth=1
	movq	184(%rsp), %rax         # 8-byte Reload
	movl	108(%rax), %r13d
	decl	%r13d
	jmp	.LBB21_154
.LBB21_153:                             #   in Loop: Header=BB21_38 Depth=1
	movl	%eax, %r13d
.LBB21_154:                             # %_ZNK8NArchive4NCab5CItem14GetFolderIndexEi.exit.preheader
                                        #   in Loop: Header=BB21_38 Depth=1
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	96(%rcx), %rax
	cmpq	104(%rcx), %rax
	je	.LBB21_170
# BB#155:                               # %.lr.ph979.lr.ph
                                        #   in Loop: Header=BB21_38 Depth=1
	xorl	%r12d, %r12d
	movl	$0, 184(%rsp)           # 4-byte Folded Spill
	movl	$0, 136(%rsp)           # 4-byte Folded Spill
.LBB21_156:                             # %.lr.ph979
                                        #   Parent Loop BB21_38 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB21_157 Depth 3
	movslq	%r14d, %r14
	.p2align	4, 0x90
.LBB21_157:                             #   Parent Loop BB21_38 Depth=1
                                        #     Parent Loop BB21_156 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	28(%r15), %rax
	cmpq	%rax, %r14
	jge	.LBB21_195
# BB#158:                               #   in Loop: Header=BB21_157 Depth=3
	movq	32(%r15), %rax
	movq	(%rax,%r14,8), %rbp
	movq	112(%rbp), %rax
	movslq	%r13d, %rcx
	movq	(%rax,%rcx,8), %rcx
	testl	%r12d, %r12d
	jne	.LBB21_168
# BB#159:                               #   in Loop: Header=BB21_157 Depth=3
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	160(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB21_161
# BB#160:                               #   in Loop: Header=BB21_157 Depth=3
	movq	(%rbx), %rax
.Ltmp340:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp341:
.LBB21_161:                             # %.noexc638
                                        #   in Loop: Header=BB21_157 Depth=3
	movq	240(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB21_163
# BB#162:                               #   in Loop: Header=BB21_157 Depth=3
	movq	(%rdi), %rax
.Ltmp342:
	callq	*16(%rax)
.Ltmp343:
.LBB21_163:                             #   in Loop: Header=BB21_157 Depth=3
	movq	240(%rsp), %rax         # 8-byte Reload
	movq	%rbx, (%rax)
	testb	$4, 14(%rbp)
	jne	.LBB21_165
# BB#164:                               #   in Loop: Header=BB21_157 Depth=3
	xorl	%eax, %eax
	jmp	.LBB21_166
.LBB21_165:                             #   in Loop: Header=BB21_157 Depth=3
	movb	23(%rbp), %al
.LBB21_166:                             # %_ZNK8NArchive4NCab12CArchiveInfo23GetDataBlockReserveSizeEv.exit
                                        #   in Loop: Header=BB21_157 Depth=3
	movq	72(%rsp), %rdx          # 8-byte Reload
	movzbl	%al, %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%eax, 44(%rcx)
	movq	160(%rbp), %rdi
	movq	(%rdi), %rax
	movl	(%rdx), %esi
	addq	(%rbp), %rsi
.Ltmp344:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
.Ltmp345:
# BB#167:                               #   in Loop: Header=BB21_157 Depth=3
	testl	%eax, %eax
	movq	72(%rsp), %rcx          # 8-byte Reload
	jne	.LBB21_131
.LBB21_168:                             #   in Loop: Header=BB21_157 Depth=3
	movzwl	4(%rcx), %eax
	cmpl	%eax, %r12d
	jne	.LBB21_171
# BB#169:                               # %.thread716
                                        #   in Loop: Header=BB21_157 Depth=3
	incq	%r14
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	96(%rcx), %rax
	xorl	%r12d, %r12d
	cmpq	104(%rcx), %rax
	movl	$0, %r13d
	jne	.LBB21_157
	jmp	.LBB21_170
.LBB21_171:                             #   in Loop: Header=BB21_156 Depth=2
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	%r13d, %ebx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movb	$0, 48(%rdi)
	cmpb	$0, 184(%rsp)           # 1-byte Folded Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	jne	.LBB21_173
# BB#172:                               #   in Loop: Header=BB21_156 Depth=2
	movl	$0, 36(%rdi)
.LBB21_173:                             #   in Loop: Header=BB21_156 Depth=2
.Ltmp347:
	leaq	120(%rsp), %rsi
	leaq	104(%rsp), %rdx
	callq	_ZN8NArchive4NCab17CCabBlockInStream7PreReadERjS2_
.Ltmp348:
# BB#174:                               #   in Loop: Header=BB21_156 Depth=2
	testl	%eax, %eax
	jne	.LBB21_194
# BB#175:                               #   in Loop: Header=BB21_156 Depth=2
	cmpl	$0, 104(%rsp)
	sete	%al
	movl	%eax, 184(%rsp)         # 4-byte Spill
	je	.LBB21_181
# BB#176:                               #   in Loop: Header=BB21_156 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	104(%rax), %rax
	addq	200(%rsp), %rax         # 8-byte Folded Reload
	movl	120(%rsp), %ebp
	addq	56(%rsp), %rbp          # 8-byte Folded Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rax, 56(%rdi)
	movq	%rbp, 48(%rdi)
.Ltmp350:
	callq	_ZN14CLocalProgress6SetCurEv
.Ltmp351:
	movl	%ebx, %r13d
# BB#177:                               #   in Loop: Header=BB21_156 Depth=2
	testl	%eax, %eax
	jne	.LBB21_35
# BB#178:                               #   in Loop: Header=BB21_156 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	96(%rcx), %rax
	subq	104(%rcx), %rax
	cmpq	$32768, %rax            # imm = 0x8000
	movl	$32768, %ecx            # imm = 0x8000
	cmovaeq	%rcx, %rax
	movl	104(%rsp), %ecx
	cmpq	%rcx, %rax
	cmovaq	%rcx, %rax
	movq	%rax, 304(%rsp)
	movq	72(%rsp), %rax          # 8-byte Reload
	movzbl	6(%rax), %eax
	movl	%eax, %ecx
	andb	$15, %cl
	cmpb	$3, %cl
	ja	.LBB21_186
# BB#179:                               #   in Loop: Header=BB21_156 Depth=2
	andl	$15, %eax
	jmpq	*.LJTI21_1(,%rax,8)
.LBB21_180:                             #   in Loop: Header=BB21_156 Depth=2
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp359:
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	304(%rsp), %r8
	callq	*40(%rax)
.Ltmp360:
	jmp	.LBB21_185
.LBB21_181:                             # %.loopexit759.thread1241
                                        #   in Loop: Header=BB21_156 Depth=2
	movq	56(%rsp), %rbp          # 8-byte Reload
	movl	%ebx, %r13d
	jmp	.LBB21_187
.LBB21_182:                             #   in Loop: Header=BB21_156 Depth=2
	movl	136(%rsp), %ecx         # 4-byte Reload
	andb	$1, %cl
	movq	208(%rsp), %rax         # 8-byte Reload
	movb	%cl, 180(%rax)
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp353:
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	304(%rsp), %r8
	callq	*40(%rax)
.Ltmp354:
	jmp	.LBB21_185
.LBB21_183:                             #   in Loop: Header=BB21_156 Depth=2
	movl	136(%rsp), %ecx         # 4-byte Reload
	andb	$1, %cl
	movq	216(%rsp), %rax         # 8-byte Reload
	movb	%cl, 7404(%rax)
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp355:
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	304(%rsp), %r8
	callq	*40(%rax)
.Ltmp356:
	jmp	.LBB21_185
.LBB21_184:                             #   in Loop: Header=BB21_156 Depth=2
	movl	136(%rsp), %ecx         # 4-byte Reload
	andb	$1, %cl
	movq	176(%rsp), %rax         # 8-byte Reload
	movb	%cl, 3458(%rax)
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp357:
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	304(%rsp), %r8
	callq	*40(%rax)
.Ltmp358:
.LBB21_185:                             #   in Loop: Header=BB21_156 Depth=2
	testl	%eax, %eax
	jne	.LBB21_33
.LBB21_186:                             # %.loopexit759.thread1249
                                        #   in Loop: Header=BB21_156 Depth=2
	movb	$1, %al
	movl	%eax, 136(%rsp)         # 4-byte Spill
.LBB21_187:                             # %_ZNK8NArchive4NCab5CItem14GetFolderIndexEi.exit.outer.backedge
                                        #   in Loop: Header=BB21_156 Depth=2
	incl	%r12d
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	96(%rcx), %rax
	cmpq	104(%rcx), %rax
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	jne	.LBB21_156
	jmp	.LBB21_188
.LBB21_170:                             #   in Loop: Header=BB21_38 Depth=1
	movq	56(%rsp), %rbp          # 8-byte Reload
.LBB21_188:                             # %.loopexit1255
                                        #   in Loop: Header=BB21_38 Depth=1
.Ltmp362:
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NCab16CFolderOutStream15WriteEmptyFilesEv
.Ltmp363:
# BB#189:                               #   in Loop: Header=BB21_38 Depth=1
	testl	%eax, %eax
	je	.LBB21_202
# BB#190:                               #   in Loop: Header=BB21_38 Depth=1
	movl	$1, %r14d
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	jmp	.LBB21_191
.LBB21_202:                             # %.thread725.thread746
                                        #   in Loop: Header=BB21_38 Depth=1
	movq	%rbp, %r13
	movq	96(%rbx), %rax
	cmpq	104(%rbx), %rax
	movl	16(%rsp), %r12d         # 4-byte Reload
	movl	20(%rsp), %ebp          # 4-byte Reload
	jne	.LBB21_197
	jmp	.LBB21_203
.LBB21_131:                             #   in Loop: Header=BB21_38 Depth=1
	movl	$1, %r14d
.LBB21_191:                             # %.loopexit757
                                        #   in Loop: Header=BB21_38 Depth=1
	movl	%eax, %ebp
	movl	16(%rsp), %r12d         # 4-byte Reload
	jmp	.LBB21_204
.LBB21_194:                             #   in Loop: Header=BB21_38 Depth=1
	cmpl	$1, %eax
	jne	.LBB21_32
.LBB21_195:                             # %.loopexit759.thread1247
                                        #   in Loop: Header=BB21_38 Depth=1
	movq	56(%rsp), %r13          # 8-byte Reload
	movl	16(%rsp), %r12d         # 4-byte Reload
.LBB21_196:                             # %.thread725.thread
                                        #   in Loop: Header=BB21_38 Depth=1
	movl	20(%rsp), %ebp          # 4-byte Reload
	movq	48(%rsp), %rbx          # 8-byte Reload
.LBB21_197:                             # %.thread725.thread
                                        #   in Loop: Header=BB21_38 Depth=1
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	leaq	304(%rsp), %rdi
	callq	memset
	.p2align	4, 0x90
.LBB21_198:                             # %.thread.i
                                        #   Parent Loop BB21_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	96(%rbx), %rdx
	subq	104(%rbx), %rdx
	je	.LBB21_203
# BB#199:                               #   in Loop: Header=BB21_198 Depth=2
	cmpq	$1024, %rdx             # imm = 0x400
	movl	$1024, %eax             # imm = 0x400
	cmovael	%eax, %edx
	movl	$0, 236(%rsp)
.Ltmp365:
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	leaq	304(%rsp), %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	leaq	236(%rsp), %rcx
	callq	_ZN8NArchive4NCab16CFolderOutStream6Write2EPKvjPjb
.Ltmp366:
# BB#200:                               # %.noexc624
                                        #   in Loop: Header=BB21_198 Depth=2
	testl	%eax, %eax
	je	.LBB21_198
# BB#201:                               #   in Loop: Header=BB21_38 Depth=1
	movl	$1, %r14d
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movl	%eax, %ebp
	jmp	.LBB21_204
.LBB21_203:                             #   in Loop: Header=BB21_38 Depth=1
	movq	192(%rsp), %rax         # 8-byte Reload
	addq	%rax, 200(%rsp)         # 8-byte Folded Spill
	xorl	%r14d, %r14d
	movq	%r13, 56(%rsp)          # 8-byte Spill
.LBB21_204:                             # %.loopexit757
                                        #   in Loop: Header=BB21_38 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp377:
	callq	*16(%rax)
.Ltmp378:
	.p2align	4, 0x90
.LBB21_205:                             # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit623
                                        #   in Loop: Header=BB21_38 Depth=1
	cmpl	$6, %r14d
	movq	112(%rsp), %rdi         # 8-byte Reload
	je	.LBB21_37
# BB#206:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit623
                                        #   in Loop: Header=BB21_38 Depth=1
	testl	%r14d, %r14d
	je	.LBB21_37
# BB#207:
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	jmp	.LBB21_219
.LBB21_208:
	movl	$-2147024882, 20(%rsp)  # 4-byte Folded Spill
                                        # imm = 0x8007000E
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
.LBB21_209:
	movq	(%rbx), %rax
.Ltmp401:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp402:
# BB#210:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit620
	testq	%rbp, %rbp
	je	.LBB21_212
# BB#211:
	movq	(%rbp), %rax
.Ltmp406:
	movq	%rbp, %rdi
	callq	*16(%rax)
.Ltmp407:
.LBB21_212:                             # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit617
	testq	%r15, %r15
	je	.LBB21_214
# BB#213:
	movq	(%r15), %rax
.Ltmp411:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp412:
.LBB21_214:                             # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit614
	testq	%r12, %r12
	je	.LBB21_216
# BB#215:
	movq	(%r12), %rax
.Ltmp416:
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp417:
.LBB21_216:                             # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit606
	movq	(%r14), %rax
.Ltmp421:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp422:
# BB#217:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit602
	movq	(%r13), %rax
.Ltmp427:
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp428:
	movl	20(%rsp), %eax          # 4-byte Reload
	jmp	.LBB21_285
.LBB21_218:
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
.LBB21_219:                             # %._crit_edge1025
	movq	32(%rsp), %r13          # 8-byte Reload
.LBB21_220:                             # %._crit_edge1025
.Ltmp396:
	leaq	144(%rsp), %rdi
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	96(%rsp), %rbp          # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp397:
	jmp	.LBB21_209
.LBB21_221:
	movl	%eax, 20(%rsp)          # 4-byte Spill
	jmp	.LBB21_220
.LBB21_222:
.Ltmp398:
	movq	%rdx, %rbp
	movq	%rax, %r15
	jmp	.LBB21_274
.LBB21_223:
.Ltmp418:
	jmp	.LBB21_232
.LBB21_224:
.Ltmp413:
	movq	%rdx, %rbp
	movq	%rax, %r15
	testq	%r12, %r12
	jne	.LBB21_280
	jmp	.LBB21_281
.LBB21_225:
.Ltmp408:
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movq	%rdx, %rbp
	movq	%rax, %r15
	jmp	.LBB21_277
.LBB21_226:
.Ltmp429:
	jmp	.LBB21_238
.LBB21_227:
.Ltmp423:
	jmp	.LBB21_240
.LBB21_228:
.Ltmp403:
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movq	%rdx, %rbp
	movq	%rax, %r15
	jmp	.LBB21_275
.LBB21_229:
.Ltmp268:
	movq	%rdx, %rbp
	movq	%rax, %r15
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	jmp	.LBB21_274
.LBB21_230:
.Ltmp265:
	jmp	.LBB21_232
.LBB21_231:
.Ltmp262:
.LBB21_232:                             # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit604
	movq	%rdx, %rbp
	movq	%rax, %r15
	jmp	.LBB21_281
.LBB21_233:
.Ltmp259:
	jmp	.LBB21_240
.LBB21_234:
.Ltmp251:
	jmp	.LBB21_238
.LBB21_235:
.Ltmp248:
	movq	%rdx, %rbp
	movq	%rax, %r15
	movq	%r13, %rdi
	callq	_ZdlPv
	jmp	.LBB21_283
.LBB21_236:
.Ltmp245:
	jmp	.LBB21_238
.LBB21_237:
.Ltmp242:
.LBB21_238:                             # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	movq	%rdx, %rbp
	movq	%rax, %r15
	jmp	.LBB21_283
.LBB21_239:
.Ltmp256:
.LBB21_240:                             # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	%rdx, %rbp
	movq	%rax, %r15
	jmp	.LBB21_282
.LBB21_241:
.Ltmp361:
	jmp	.LBB21_261
.LBB21_242:
.Ltmp352:
	jmp	.LBB21_261
.LBB21_243:
.Ltmp325:
	movq	%rax, %r15
	movq	%rbp, %rdi
	movq	%rdx, %rbp
	jmp	.LBB21_246
.LBB21_244:
.Ltmp316:
	movq	%rdx, %rbp
	movq	%rax, %r15
	movq	%r12, %rdi
	jmp	.LBB21_246
.LBB21_245:
.Ltmp336:
	movq	%rdx, %rbp
	movq	%rax, %r15
	movq	%rbx, %rdi
.LBB21_246:
	callq	_ZdlPv
	jmp	.LBB21_262
.LBB21_247:
.Ltmp349:
	movq	%rdx, %rbp
	movq	%rax, %r15
	jmp	.LBB21_263
.LBB21_248:
.Ltmp364:
	jmp	.LBB21_261
.LBB21_249:                             # %.loopexit.split-lp.us-lcssa.us
.Ltmp302:
	jmp	.LBB21_270
.LBB21_250:
.Ltmp379:
	jmp	.LBB21_270
.LBB21_251:
.Ltmp393:
	jmp	.LBB21_270
.LBB21_252:                             # %.loopexit.split-lp.us-lcssa
.Ltmp296:
	jmp	.LBB21_270
.LBB21_253:                             # %.loopexit752
.Ltmp374:
	jmp	.LBB21_261
.LBB21_254:
.Ltmp367:
	jmp	.LBB21_261
.LBB21_255:
.Ltmp307:
	movq	%rdx, %rbp
	movq	%rax, %r15
	jmp	.LBB21_272
.LBB21_256:                             # %.loopexit.split-lp753
.Ltmp339:
	jmp	.LBB21_261
.LBB21_257:
.Ltmp388:
	movq	%rdx, %rbp
	movq	%rax, %r15
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_271
# BB#258:
	movq	(%rdi), %rax
.Ltmp389:
	callq	*16(%rax)
.Ltmp390:
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB21_273
.LBB21_259:
.Ltmp282:
	jmp	.LBB21_270
.LBB21_260:
.Ltmp346:
.LBB21_261:
	movq	%rdx, %rbp
	movq	%rax, %r15
.LBB21_262:
	movq	32(%rsp), %r13          # 8-byte Reload
.LBB21_263:
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp375:
	callq	*16(%rax)
.Ltmp376:
	jmp	.LBB21_273
.LBB21_264:                             # %.loopexit.split-lp763
.Ltmp290:
	jmp	.LBB21_270
.LBB21_265:
.Ltmp277:
	movq	%rdx, %rbp
	movq	%rax, %r15
	movq	120(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_271
# BB#266:
	movq	(%rdi), %rax
.Ltmp278:
	callq	*16(%rax)
.Ltmp279:
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB21_273
.LBB21_267:                             # %.loopexit.us-lcssa.us
.Ltmp299:
	jmp	.LBB21_270
.LBB21_268:                             # %.loopexit.us-lcssa
.Ltmp293:
	jmp	.LBB21_270
.LBB21_269:                             # %.loopexit762
.Ltmp287:
.LBB21_270:
	movq	%rdx, %rbp
	movq	%rax, %r15
.LBB21_271:                             # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	32(%rsp), %r13          # 8-byte Reload
.LBB21_272:                             # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB21_273:                             # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
.Ltmp394:
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp395:
.LBB21_274:
	movq	(%rbx), %rax
.Ltmp399:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp400:
.LBB21_275:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	96(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB21_277
# BB#276:
	movq	(%rdi), %rax
.Ltmp404:
	callq	*16(%rax)
.Ltmp405:
.LBB21_277:                             # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit612
	movq	64(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB21_279
# BB#278:
	movq	(%rdi), %rax
.Ltmp409:
	callq	*16(%rax)
.Ltmp410:
.LBB21_279:                             # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit609
	testq	%r12, %r12
	je	.LBB21_281
.LBB21_280:
	movq	(%r12), %rax
.Ltmp414:
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp415:
.LBB21_281:                             # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit604
	movq	(%r14), %rax
.Ltmp419:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp420:
.LBB21_282:                             # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	(%r13), %rax
.Ltmp424:
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp425:
.LBB21_283:                             # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	movq	%r15, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %ebp
	je	.LBB21_286
# BB#284:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
.LBB21_285:                             # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit599
	addq	$1336, %rsp             # imm = 0x538
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_286:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp430:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp431:
# BB#287:
.LBB21_288:
.Ltmp432:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB21_289:
.Ltmp426:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end21:
	.size	_ZN8NArchive4NCab8CHandler7ExtractEPKjjiP23IArchiveExtractCallback, .Lfunc_end21-_ZN8NArchive4NCab8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI21_0:
	.quad	.LBB21_150
	.quad	.LBB21_114
	.quad	.LBB21_120
	.quad	.LBB21_122
.LJTI21_1:
	.quad	.LBB21_180
	.quad	.LBB21_184
	.quad	.LBB21_182
	.quad	.LBB21_183
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\214\005"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\375\004"              # Call site table length
	.long	.Ltmp240-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp241-.Ltmp240       #   Call between .Ltmp240 and .Ltmp241
	.long	.Ltmp242-.Lfunc_begin6  #     jumps to .Ltmp242
	.byte	3                       #   On action: 2
	.long	.Ltmp243-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp244-.Ltmp243       #   Call between .Ltmp243 and .Ltmp244
	.long	.Ltmp245-.Lfunc_begin6  #     jumps to .Ltmp245
	.byte	3                       #   On action: 2
	.long	.Ltmp246-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp247-.Ltmp246       #   Call between .Ltmp246 and .Ltmp247
	.long	.Ltmp248-.Lfunc_begin6  #     jumps to .Ltmp248
	.byte	3                       #   On action: 2
	.long	.Ltmp249-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp250-.Ltmp249       #   Call between .Ltmp249 and .Ltmp250
	.long	.Ltmp251-.Lfunc_begin6  #     jumps to .Ltmp251
	.byte	3                       #   On action: 2
	.long	.Ltmp252-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp255-.Ltmp252       #   Call between .Ltmp252 and .Ltmp255
	.long	.Ltmp256-.Lfunc_begin6  #     jumps to .Ltmp256
	.byte	3                       #   On action: 2
	.long	.Ltmp257-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp258-.Ltmp257       #   Call between .Ltmp257 and .Ltmp258
	.long	.Ltmp259-.Lfunc_begin6  #     jumps to .Ltmp259
	.byte	3                       #   On action: 2
	.long	.Ltmp260-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp261-.Ltmp260       #   Call between .Ltmp260 and .Ltmp261
	.long	.Ltmp262-.Lfunc_begin6  #     jumps to .Ltmp262
	.byte	3                       #   On action: 2
	.long	.Ltmp263-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp264-.Ltmp263       #   Call between .Ltmp263 and .Ltmp264
	.long	.Ltmp265-.Lfunc_begin6  #     jumps to .Ltmp265
	.byte	3                       #   On action: 2
	.long	.Ltmp266-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp267-.Ltmp266       #   Call between .Ltmp266 and .Ltmp267
	.long	.Ltmp268-.Lfunc_begin6  #     jumps to .Ltmp268
	.byte	3                       #   On action: 2
	.long	.Ltmp269-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp276-.Ltmp269       #   Call between .Ltmp269 and .Ltmp276
	.long	.Ltmp277-.Lfunc_begin6  #     jumps to .Ltmp277
	.byte	3                       #   On action: 2
	.long	.Ltmp280-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp281-.Ltmp280       #   Call between .Ltmp280 and .Ltmp281
	.long	.Ltmp282-.Lfunc_begin6  #     jumps to .Ltmp282
	.byte	3                       #   On action: 2
	.long	.Ltmp283-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Ltmp284-.Ltmp283       #   Call between .Ltmp283 and .Ltmp284
	.long	.Ltmp290-.Lfunc_begin6  #     jumps to .Ltmp290
	.byte	3                       #   On action: 2
	.long	.Ltmp285-.Lfunc_begin6  # >> Call Site 13 <<
	.long	.Ltmp286-.Ltmp285       #   Call between .Ltmp285 and .Ltmp286
	.long	.Ltmp287-.Lfunc_begin6  #     jumps to .Ltmp287
	.byte	3                       #   On action: 2
	.long	.Ltmp288-.Lfunc_begin6  # >> Call Site 14 <<
	.long	.Ltmp289-.Ltmp288       #   Call between .Ltmp288 and .Ltmp289
	.long	.Ltmp290-.Lfunc_begin6  #     jumps to .Ltmp290
	.byte	3                       #   On action: 2
	.long	.Ltmp291-.Lfunc_begin6  # >> Call Site 15 <<
	.long	.Ltmp292-.Ltmp291       #   Call between .Ltmp291 and .Ltmp292
	.long	.Ltmp293-.Lfunc_begin6  #     jumps to .Ltmp293
	.byte	3                       #   On action: 2
	.long	.Ltmp294-.Lfunc_begin6  # >> Call Site 16 <<
	.long	.Ltmp295-.Ltmp294       #   Call between .Ltmp294 and .Ltmp295
	.long	.Ltmp296-.Lfunc_begin6  #     jumps to .Ltmp296
	.byte	3                       #   On action: 2
	.long	.Ltmp297-.Lfunc_begin6  # >> Call Site 17 <<
	.long	.Ltmp298-.Ltmp297       #   Call between .Ltmp297 and .Ltmp298
	.long	.Ltmp299-.Lfunc_begin6  #     jumps to .Ltmp299
	.byte	3                       #   On action: 2
	.long	.Ltmp300-.Lfunc_begin6  # >> Call Site 18 <<
	.long	.Ltmp301-.Ltmp300       #   Call between .Ltmp300 and .Ltmp301
	.long	.Ltmp302-.Lfunc_begin6  #     jumps to .Ltmp302
	.byte	3                       #   On action: 2
	.long	.Ltmp303-.Lfunc_begin6  # >> Call Site 19 <<
	.long	.Ltmp306-.Ltmp303       #   Call between .Ltmp303 and .Ltmp306
	.long	.Ltmp307-.Lfunc_begin6  #     jumps to .Ltmp307
	.byte	3                       #   On action: 2
	.long	.Ltmp380-.Lfunc_begin6  # >> Call Site 20 <<
	.long	.Ltmp387-.Ltmp380       #   Call between .Ltmp380 and .Ltmp387
	.long	.Ltmp388-.Lfunc_begin6  #     jumps to .Ltmp388
	.byte	3                       #   On action: 2
	.long	.Ltmp391-.Lfunc_begin6  # >> Call Site 21 <<
	.long	.Ltmp392-.Ltmp391       #   Call between .Ltmp391 and .Ltmp392
	.long	.Ltmp393-.Lfunc_begin6  #     jumps to .Ltmp393
	.byte	3                       #   On action: 2
	.long	.Ltmp308-.Lfunc_begin6  # >> Call Site 22 <<
	.long	.Ltmp333-.Ltmp308       #   Call between .Ltmp308 and .Ltmp333
	.long	.Ltmp339-.Lfunc_begin6  #     jumps to .Ltmp339
	.byte	3                       #   On action: 2
	.long	.Ltmp334-.Lfunc_begin6  # >> Call Site 23 <<
	.long	.Ltmp335-.Ltmp334       #   Call between .Ltmp334 and .Ltmp335
	.long	.Ltmp336-.Lfunc_begin6  #     jumps to .Ltmp336
	.byte	3                       #   On action: 2
	.long	.Ltmp337-.Lfunc_begin6  # >> Call Site 24 <<
	.long	.Ltmp322-.Ltmp337       #   Call between .Ltmp337 and .Ltmp322
	.long	.Ltmp339-.Lfunc_begin6  #     jumps to .Ltmp339
	.byte	3                       #   On action: 2
	.long	.Ltmp323-.Lfunc_begin6  # >> Call Site 25 <<
	.long	.Ltmp324-.Ltmp323       #   Call between .Ltmp323 and .Ltmp324
	.long	.Ltmp325-.Lfunc_begin6  #     jumps to .Ltmp325
	.byte	3                       #   On action: 2
	.long	.Ltmp326-.Lfunc_begin6  # >> Call Site 26 <<
	.long	.Ltmp331-.Ltmp326       #   Call between .Ltmp326 and .Ltmp331
	.long	.Ltmp339-.Lfunc_begin6  #     jumps to .Ltmp339
	.byte	3                       #   On action: 2
	.long	.Ltmp368-.Lfunc_begin6  # >> Call Site 27 <<
	.long	.Ltmp373-.Ltmp368       #   Call between .Ltmp368 and .Ltmp373
	.long	.Ltmp374-.Lfunc_begin6  #     jumps to .Ltmp374
	.byte	3                       #   On action: 2
	.long	.Ltmp312-.Lfunc_begin6  # >> Call Site 28 <<
	.long	.Ltmp313-.Ltmp312       #   Call between .Ltmp312 and .Ltmp313
	.long	.Ltmp339-.Lfunc_begin6  #     jumps to .Ltmp339
	.byte	3                       #   On action: 2
	.long	.Ltmp314-.Lfunc_begin6  # >> Call Site 29 <<
	.long	.Ltmp315-.Ltmp314       #   Call between .Ltmp314 and .Ltmp315
	.long	.Ltmp316-.Lfunc_begin6  #     jumps to .Ltmp316
	.byte	3                       #   On action: 2
	.long	.Ltmp317-.Lfunc_begin6  # >> Call Site 30 <<
	.long	.Ltmp320-.Ltmp317       #   Call between .Ltmp317 and .Ltmp320
	.long	.Ltmp339-.Lfunc_begin6  #     jumps to .Ltmp339
	.byte	3                       #   On action: 2
	.long	.Ltmp340-.Lfunc_begin6  # >> Call Site 31 <<
	.long	.Ltmp345-.Ltmp340       #   Call between .Ltmp340 and .Ltmp345
	.long	.Ltmp346-.Lfunc_begin6  #     jumps to .Ltmp346
	.byte	3                       #   On action: 2
	.long	.Ltmp347-.Lfunc_begin6  # >> Call Site 32 <<
	.long	.Ltmp348-.Ltmp347       #   Call between .Ltmp347 and .Ltmp348
	.long	.Ltmp349-.Lfunc_begin6  #     jumps to .Ltmp349
	.byte	3                       #   On action: 2
	.long	.Ltmp350-.Lfunc_begin6  # >> Call Site 33 <<
	.long	.Ltmp351-.Ltmp350       #   Call between .Ltmp350 and .Ltmp351
	.long	.Ltmp352-.Lfunc_begin6  #     jumps to .Ltmp352
	.byte	3                       #   On action: 2
	.long	.Ltmp359-.Lfunc_begin6  # >> Call Site 34 <<
	.long	.Ltmp358-.Ltmp359       #   Call between .Ltmp359 and .Ltmp358
	.long	.Ltmp361-.Lfunc_begin6  #     jumps to .Ltmp361
	.byte	3                       #   On action: 2
	.long	.Ltmp362-.Lfunc_begin6  # >> Call Site 35 <<
	.long	.Ltmp363-.Ltmp362       #   Call between .Ltmp362 and .Ltmp363
	.long	.Ltmp364-.Lfunc_begin6  #     jumps to .Ltmp364
	.byte	3                       #   On action: 2
	.long	.Ltmp363-.Lfunc_begin6  # >> Call Site 36 <<
	.long	.Ltmp365-.Ltmp363       #   Call between .Ltmp363 and .Ltmp365
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp365-.Lfunc_begin6  # >> Call Site 37 <<
	.long	.Ltmp366-.Ltmp365       #   Call between .Ltmp365 and .Ltmp366
	.long	.Ltmp367-.Lfunc_begin6  #     jumps to .Ltmp367
	.byte	3                       #   On action: 2
	.long	.Ltmp377-.Lfunc_begin6  # >> Call Site 38 <<
	.long	.Ltmp378-.Ltmp377       #   Call between .Ltmp377 and .Ltmp378
	.long	.Ltmp379-.Lfunc_begin6  #     jumps to .Ltmp379
	.byte	3                       #   On action: 2
	.long	.Ltmp401-.Lfunc_begin6  # >> Call Site 39 <<
	.long	.Ltmp402-.Ltmp401       #   Call between .Ltmp401 and .Ltmp402
	.long	.Ltmp403-.Lfunc_begin6  #     jumps to .Ltmp403
	.byte	3                       #   On action: 2
	.long	.Ltmp406-.Lfunc_begin6  # >> Call Site 40 <<
	.long	.Ltmp407-.Ltmp406       #   Call between .Ltmp406 and .Ltmp407
	.long	.Ltmp408-.Lfunc_begin6  #     jumps to .Ltmp408
	.byte	3                       #   On action: 2
	.long	.Ltmp411-.Lfunc_begin6  # >> Call Site 41 <<
	.long	.Ltmp412-.Ltmp411       #   Call between .Ltmp411 and .Ltmp412
	.long	.Ltmp413-.Lfunc_begin6  #     jumps to .Ltmp413
	.byte	3                       #   On action: 2
	.long	.Ltmp416-.Lfunc_begin6  # >> Call Site 42 <<
	.long	.Ltmp417-.Ltmp416       #   Call between .Ltmp416 and .Ltmp417
	.long	.Ltmp418-.Lfunc_begin6  #     jumps to .Ltmp418
	.byte	3                       #   On action: 2
	.long	.Ltmp421-.Lfunc_begin6  # >> Call Site 43 <<
	.long	.Ltmp422-.Ltmp421       #   Call between .Ltmp421 and .Ltmp422
	.long	.Ltmp423-.Lfunc_begin6  #     jumps to .Ltmp423
	.byte	3                       #   On action: 2
	.long	.Ltmp427-.Lfunc_begin6  # >> Call Site 44 <<
	.long	.Ltmp428-.Ltmp427       #   Call between .Ltmp427 and .Ltmp428
	.long	.Ltmp429-.Lfunc_begin6  #     jumps to .Ltmp429
	.byte	3                       #   On action: 2
	.long	.Ltmp396-.Lfunc_begin6  # >> Call Site 45 <<
	.long	.Ltmp397-.Ltmp396       #   Call between .Ltmp396 and .Ltmp397
	.long	.Ltmp398-.Lfunc_begin6  #     jumps to .Ltmp398
	.byte	3                       #   On action: 2
	.long	.Ltmp389-.Lfunc_begin6  # >> Call Site 46 <<
	.long	.Ltmp425-.Ltmp389       #   Call between .Ltmp389 and .Ltmp425
	.long	.Ltmp426-.Lfunc_begin6  #     jumps to .Ltmp426
	.byte	1                       #   On action: 1
	.long	.Ltmp425-.Lfunc_begin6  # >> Call Site 47 <<
	.long	.Ltmp430-.Ltmp425       #   Call between .Ltmp425 and .Ltmp430
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp430-.Lfunc_begin6  # >> Call Site 48 <<
	.long	.Ltmp431-.Ltmp430       #   Call between .Ltmp430 and .Ltmp431
	.long	.Ltmp432-.Lfunc_begin6  #     jumps to .Ltmp432
	.byte	0                       #   On action: cleanup
	.long	.Ltmp431-.Lfunc_begin6  # >> Call Site 49 <<
	.long	.Lfunc_end21-.Ltmp431   #   Call between .Ltmp431 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress8NQuantum8CDecoderC2Ev,"axG",@progbits,_ZN9NCompress8NQuantum8CDecoderC2Ev,comdat
	.weak	_ZN9NCompress8NQuantum8CDecoderC2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NQuantum8CDecoderC2Ev,@function
_ZN9NCompress8NQuantum8CDecoderC2Ev:    # @_ZN9NCompress8NQuantum8CDecoderC2Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi155:
	.cfi_def_cfa_offset 32
.Lcfi156:
	.cfi_offset %rbx, -32
.Lcfi157:
	.cfi_offset %r14, -24
.Lcfi158:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$0, 24(%rbx)
	movl	$_ZTVN9NCompress8NQuantum8CDecoderE+104, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NQuantum8CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN9NCompress8NQuantum8CDecoderE+176, 16(%rbx)
	leaq	32(%rbx), %r15
	movq	$0, 32(%rbx)
	movl	$0, 40(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 72(%rbx)
	leaq	112(%rbx), %rdi
.Ltmp433:
	callq	_ZN9CInBufferC1Ev
.Ltmp434:
# BB#1:                                 # %_ZN9NCompress8NQuantum11NRangeCoder8CDecoderC2Ev.exit
	movb	$0, 180(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB22_2:
.Ltmp435:
	movq	%rax, %r14
.Ltmp436:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer4FreeEv
.Ltmp437:
# BB#3:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp442:
	callq	*16(%rax)
.Ltmp443:
.LBB22_5:                               # %_ZN10COutBufferD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_9:
.Ltmp444:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB22_6:
.Ltmp438:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_10
# BB#7:
	movq	(%rdi), %rax
.Ltmp439:
	callq	*16(%rax)
.Ltmp440:
.LBB22_10:                              # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB22_8:
.Ltmp441:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZN9NCompress8NQuantum8CDecoderC2Ev, .Lfunc_end22-_ZN9NCompress8NQuantum8CDecoderC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp433-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp434-.Ltmp433       #   Call between .Ltmp433 and .Ltmp434
	.long	.Ltmp435-.Lfunc_begin7  #     jumps to .Ltmp435
	.byte	0                       #   On action: cleanup
	.long	.Ltmp436-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp437-.Ltmp436       #   Call between .Ltmp436 and .Ltmp437
	.long	.Ltmp438-.Lfunc_begin7  #     jumps to .Ltmp438
	.byte	1                       #   On action: 1
	.long	.Ltmp442-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp443-.Ltmp442       #   Call between .Ltmp442 and .Ltmp443
	.long	.Ltmp444-.Lfunc_begin7  #     jumps to .Ltmp444
	.byte	1                       #   On action: 1
	.long	.Ltmp443-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp439-.Ltmp443       #   Call between .Ltmp443 and .Ltmp439
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp439-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp440-.Ltmp439       #   Call between .Ltmp439 and .Ltmp440
	.long	.Ltmp441-.Lfunc_begin7  #     jumps to .Ltmp441
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NCab8CHandler16GetNumberOfItemsEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab8CHandler16GetNumberOfItemsEPj,@function
_ZN8NArchive4NCab8CHandler16GetNumberOfItemsEPj: # @_ZN8NArchive4NCab8CHandler16GetNumberOfItemsEPj
	.cfi_startproc
# BB#0:
	movl	60(%rdi), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end23:
	.size	_ZN8NArchive4NCab8CHandler16GetNumberOfItemsEPj, .Lfunc_end23-_ZN8NArchive4NCab8CHandler16GetNumberOfItemsEPj
	.cfi_endproc

	.section	.text._ZN8NArchive4NCab8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive4NCab8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive4NCab8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive4NCab8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive4NCab8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi159:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB24_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB24_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB24_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB24_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB24_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB24_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB24_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB24_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB24_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB24_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB24_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB24_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB24_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB24_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB24_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB24_32
.LBB24_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IInArchive(%rip), %cl
	jne	.LBB24_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_IInArchive+1(%rip), %cl
	jne	.LBB24_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_IInArchive+2(%rip), %cl
	jne	.LBB24_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_IInArchive+3(%rip), %cl
	jne	.LBB24_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_IInArchive+4(%rip), %cl
	jne	.LBB24_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_IInArchive+5(%rip), %cl
	jne	.LBB24_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_IInArchive+6(%rip), %cl
	jne	.LBB24_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_IInArchive+7(%rip), %cl
	jne	.LBB24_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_IInArchive+8(%rip), %cl
	jne	.LBB24_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_IInArchive+9(%rip), %cl
	jne	.LBB24_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_IInArchive+10(%rip), %cl
	jne	.LBB24_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_IInArchive+11(%rip), %cl
	jne	.LBB24_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_IInArchive+12(%rip), %cl
	jne	.LBB24_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_IInArchive+13(%rip), %cl
	jne	.LBB24_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_IInArchive+14(%rip), %cl
	jne	.LBB24_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %cl
	cmpb	IID_IInArchive+15(%rip), %cl
	jne	.LBB24_33
.LBB24_32:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB24_33:                              # %_ZeqRK4GUIDS1_.exit6.thread
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZN8NArchive4NCab8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end24-_ZN8NArchive4NCab8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive4NCab8CHandler6AddRefEv,"axG",@progbits,_ZN8NArchive4NCab8CHandler6AddRefEv,comdat
	.weak	_ZN8NArchive4NCab8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab8CHandler6AddRefEv,@function
_ZN8NArchive4NCab8CHandler6AddRefEv:    # @_ZN8NArchive4NCab8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end25:
	.size	_ZN8NArchive4NCab8CHandler6AddRefEv, .Lfunc_end25-_ZN8NArchive4NCab8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NCab8CHandler7ReleaseEv,"axG",@progbits,_ZN8NArchive4NCab8CHandler7ReleaseEv,comdat
	.weak	_ZN8NArchive4NCab8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab8CHandler7ReleaseEv,@function
_ZN8NArchive4NCab8CHandler7ReleaseEv:   # @_ZN8NArchive4NCab8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi160:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB26_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB26_2:
	popq	%rcx
	retq
.Lfunc_end26:
	.size	_ZN8NArchive4NCab8CHandler7ReleaseEv, .Lfunc_end26-_ZN8NArchive4NCab8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NCab8CHandlerD2Ev,"axG",@progbits,_ZN8NArchive4NCab8CHandlerD2Ev,comdat
	.weak	_ZN8NArchive4NCab8CHandlerD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab8CHandlerD2Ev,@function
_ZN8NArchive4NCab8CHandlerD2Ev:         # @_ZN8NArchive4NCab8CHandlerD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTVN8NArchive4NCab8CHandlerE+16, (%rdi)
	addq	$16, %rdi
	jmp	_ZN8NArchive4NCab13CMvDatabaseExD2Ev # TAILCALL
.Lfunc_end27:
	.size	_ZN8NArchive4NCab8CHandlerD2Ev, .Lfunc_end27-_ZN8NArchive4NCab8CHandlerD2Ev
	.cfi_endproc

	.section	.text._ZN8NArchive4NCab8CHandlerD0Ev,"axG",@progbits,_ZN8NArchive4NCab8CHandlerD0Ev,comdat
	.weak	_ZN8NArchive4NCab8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab8CHandlerD0Ev,@function
_ZN8NArchive4NCab8CHandlerD0Ev:         # @_ZN8NArchive4NCab8CHandlerD0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi163:
	.cfi_def_cfa_offset 32
.Lcfi164:
	.cfi_offset %rbx, -24
.Lcfi165:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive4NCab8CHandlerE+16, (%rbx)
	leaq	16(%rbx), %rdi
.Ltmp445:
	callq	_ZN8NArchive4NCab13CMvDatabaseExD2Ev
.Ltmp446:
# BB#1:                                 # %_ZN8NArchive4NCab8CHandlerD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB28_2:
.Ltmp447:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end28:
	.size	_ZN8NArchive4NCab8CHandlerD0Ev, .Lfunc_end28-_ZN8NArchive4NCab8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp445-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp446-.Ltmp445       #   Call between .Ltmp445 and .Ltmp446
	.long	.Ltmp447-.Lfunc_begin8  #     jumps to .Ltmp447
	.byte	0                       #   On action: cleanup
	.long	.Ltmp446-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Lfunc_end28-.Ltmp446   #   Call between .Ltmp446 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive4NCab16CFolderOutStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive4NCab16CFolderOutStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive4NCab16CFolderOutStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab16CFolderOutStream14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive4NCab16CFolderOutStream14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive4NCab16CFolderOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi166:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB29_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB29_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB29_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB29_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB29_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB29_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB29_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB29_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB29_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB29_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB29_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB29_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB29_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB29_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB29_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB29_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB29_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end29:
	.size	_ZN8NArchive4NCab16CFolderOutStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end29-_ZN8NArchive4NCab16CFolderOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive4NCab16CFolderOutStream6AddRefEv,"axG",@progbits,_ZN8NArchive4NCab16CFolderOutStream6AddRefEv,comdat
	.weak	_ZN8NArchive4NCab16CFolderOutStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab16CFolderOutStream6AddRefEv,@function
_ZN8NArchive4NCab16CFolderOutStream6AddRefEv: # @_ZN8NArchive4NCab16CFolderOutStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end30:
	.size	_ZN8NArchive4NCab16CFolderOutStream6AddRefEv, .Lfunc_end30-_ZN8NArchive4NCab16CFolderOutStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NCab16CFolderOutStream7ReleaseEv,"axG",@progbits,_ZN8NArchive4NCab16CFolderOutStream7ReleaseEv,comdat
	.weak	_ZN8NArchive4NCab16CFolderOutStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab16CFolderOutStream7ReleaseEv,@function
_ZN8NArchive4NCab16CFolderOutStream7ReleaseEv: # @_ZN8NArchive4NCab16CFolderOutStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi167:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB31_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB31_2:
	popq	%rcx
	retq
.Lfunc_end31:
	.size	_ZN8NArchive4NCab16CFolderOutStream7ReleaseEv, .Lfunc_end31-_ZN8NArchive4NCab16CFolderOutStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NCab16CFolderOutStreamD2Ev,"axG",@progbits,_ZN8NArchive4NCab16CFolderOutStreamD2Ev,comdat
	.weak	_ZN8NArchive4NCab16CFolderOutStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab16CFolderOutStreamD2Ev,@function
_ZN8NArchive4NCab16CFolderOutStreamD2Ev: # @_ZN8NArchive4NCab16CFolderOutStreamD2Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi168:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi169:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi170:
	.cfi_def_cfa_offset 32
.Lcfi171:
	.cfi_offset %rbx, -24
.Lcfi172:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive4NCab16CFolderOutStreamE+16, (%rbx)
	movq	32(%rbx), %rdi
.Ltmp448:
	callq	MyFree
.Ltmp449:
# BB#1:
	movq	$0, 32(%rbx)
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp453:
	callq	*16(%rax)
.Ltmp454:
.LBB32_3:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp459:
	callq	*16(%rax)
.Ltmp460:
.LBB32_5:                               # %_ZN9CMyComPtrI23IArchiveExtractCallbackED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB32_8:
.Ltmp461:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB32_9:
.Ltmp455:
	movq	%rax, %r14
	jmp	.LBB32_10
.LBB32_6:
.Ltmp450:
	movq	%rax, %r14
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_10
# BB#7:
	movq	(%rdi), %rax
.Ltmp451:
	callq	*16(%rax)
.Ltmp452:
.LBB32_10:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit6
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_12
# BB#11:
	movq	(%rdi), %rax
.Ltmp456:
	callq	*16(%rax)
.Ltmp457:
.LBB32_12:                              # %_ZN9CMyComPtrI23IArchiveExtractCallbackED2Ev.exit8
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB32_13:
.Ltmp458:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end32:
	.size	_ZN8NArchive4NCab16CFolderOutStreamD2Ev, .Lfunc_end32-_ZN8NArchive4NCab16CFolderOutStreamD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp448-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp449-.Ltmp448       #   Call between .Ltmp448 and .Ltmp449
	.long	.Ltmp450-.Lfunc_begin9  #     jumps to .Ltmp450
	.byte	0                       #   On action: cleanup
	.long	.Ltmp453-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp454-.Ltmp453       #   Call between .Ltmp453 and .Ltmp454
	.long	.Ltmp455-.Lfunc_begin9  #     jumps to .Ltmp455
	.byte	0                       #   On action: cleanup
	.long	.Ltmp459-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp460-.Ltmp459       #   Call between .Ltmp459 and .Ltmp460
	.long	.Ltmp461-.Lfunc_begin9  #     jumps to .Ltmp461
	.byte	0                       #   On action: cleanup
	.long	.Ltmp460-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Ltmp451-.Ltmp460       #   Call between .Ltmp460 and .Ltmp451
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp451-.Lfunc_begin9  # >> Call Site 5 <<
	.long	.Ltmp457-.Ltmp451       #   Call between .Ltmp451 and .Ltmp457
	.long	.Ltmp458-.Lfunc_begin9  #     jumps to .Ltmp458
	.byte	1                       #   On action: 1
	.long	.Ltmp457-.Lfunc_begin9  # >> Call Site 6 <<
	.long	.Lfunc_end32-.Ltmp457   #   Call between .Ltmp457 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NCab16CFolderOutStreamD0Ev,"axG",@progbits,_ZN8NArchive4NCab16CFolderOutStreamD0Ev,comdat
	.weak	_ZN8NArchive4NCab16CFolderOutStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab16CFolderOutStreamD0Ev,@function
_ZN8NArchive4NCab16CFolderOutStreamD0Ev: # @_ZN8NArchive4NCab16CFolderOutStreamD0Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi173:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi174:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi175:
	.cfi_def_cfa_offset 32
.Lcfi176:
	.cfi_offset %rbx, -24
.Lcfi177:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp462:
	callq	_ZN8NArchive4NCab16CFolderOutStreamD2Ev
.Ltmp463:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB33_2:
.Ltmp464:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end33:
	.size	_ZN8NArchive4NCab16CFolderOutStreamD0Ev, .Lfunc_end33-_ZN8NArchive4NCab16CFolderOutStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp462-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp463-.Ltmp462       #   Call between .Ltmp462 and .Ltmp463
	.long	.Ltmp464-.Lfunc_begin10 #     jumps to .Ltmp464
	.byte	0                       #   On action: cleanup
	.long	.Ltmp463-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Lfunc_end33-.Ltmp463   #   Call between .Ltmp463 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive4NCab9CDatabaseD2Ev,"axG",@progbits,_ZN8NArchive4NCab9CDatabaseD2Ev,comdat
	.weak	_ZN8NArchive4NCab9CDatabaseD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab9CDatabaseD2Ev,@function
_ZN8NArchive4NCab9CDatabaseD2Ev:        # @_ZN8NArchive4NCab9CDatabaseD2Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r15
.Lcfi178:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi179:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi180:
	.cfi_def_cfa_offset 32
.Lcfi181:
	.cfi_offset %rbx, -32
.Lcfi182:
	.cfi_offset %r14, -24
.Lcfi183:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	%r15, %rbx
	subq	$-128, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE+16, 128(%r15)
.Ltmp465:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp466:
# BB#1:
.Ltmp471:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp472:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev.exit
	leaq	96(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE+16, 96(%r15)
.Ltmp483:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp484:
# BB#3:
.Ltmp489:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp490:
# BB#4:                                 # %_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev.exit
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB34_6
# BB#5:
	callq	_ZdaPv
.LBB34_6:                               # %_ZN11CStringBaseIcED2Ev.exit.i.i
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB34_8
# BB#7:
	callq	_ZdaPv
.LBB34_8:                               # %_ZN8NArchive4NCab13COtherArchiveD2Ev.exit.i
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB34_10
# BB#9:
	callq	_ZdaPv
.LBB34_10:                              # %_ZN11CStringBaseIcED2Ev.exit.i2.i
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB34_16
# BB#11:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdaPv                  # TAILCALL
.LBB34_16:                              # %_ZN8NArchive4NCab12CArchiveInfoD2Ev.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB34_22:
.Ltmp491:
	movq	%rax, %r14
	jmp	.LBB34_23
.LBB34_14:
.Ltmp485:
	movq	%rax, %r14
.Ltmp486:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp487:
	jmp	.LBB34_23
.LBB34_15:
.Ltmp488:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB34_17:
.Ltmp473:
	movq	%rax, %r14
	jmp	.LBB34_18
.LBB34_12:
.Ltmp467:
	movq	%rax, %r14
.Ltmp468:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp469:
.LBB34_18:                              # %.body
	leaq	96(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE+16, 96(%r15)
.Ltmp474:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp475:
# BB#19:
.Ltmp480:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp481:
.LBB34_23:                              # %_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev.exit7
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB34_25
# BB#24:
	callq	_ZdaPv
.LBB34_25:                              # %_ZN11CStringBaseIcED2Ev.exit.i.i8
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB34_27
# BB#26:
	callq	_ZdaPv
.LBB34_27:                              # %_ZN8NArchive4NCab13COtherArchiveD2Ev.exit.i9
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB34_29
# BB#28:
	callq	_ZdaPv
.LBB34_29:                              # %_ZN11CStringBaseIcED2Ev.exit.i2.i10
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB34_31
# BB#30:
	callq	_ZdaPv
.LBB34_31:                              # %_ZN8NArchive4NCab12CArchiveInfoD2Ev.exit11
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB34_13:
.Ltmp470:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB34_32:
.Ltmp482:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB34_20:
.Ltmp476:
	movq	%rax, %r14
.Ltmp477:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp478:
# BB#33:                                # %.body5
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB34_21:
.Ltmp479:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end34:
	.size	_ZN8NArchive4NCab9CDatabaseD2Ev, .Lfunc_end34-_ZN8NArchive4NCab9CDatabaseD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp465-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp466-.Ltmp465       #   Call between .Ltmp465 and .Ltmp466
	.long	.Ltmp467-.Lfunc_begin11 #     jumps to .Ltmp467
	.byte	0                       #   On action: cleanup
	.long	.Ltmp471-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp472-.Ltmp471       #   Call between .Ltmp471 and .Ltmp472
	.long	.Ltmp473-.Lfunc_begin11 #     jumps to .Ltmp473
	.byte	0                       #   On action: cleanup
	.long	.Ltmp483-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp484-.Ltmp483       #   Call between .Ltmp483 and .Ltmp484
	.long	.Ltmp485-.Lfunc_begin11 #     jumps to .Ltmp485
	.byte	0                       #   On action: cleanup
	.long	.Ltmp489-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Ltmp490-.Ltmp489       #   Call between .Ltmp489 and .Ltmp490
	.long	.Ltmp491-.Lfunc_begin11 #     jumps to .Ltmp491
	.byte	0                       #   On action: cleanup
	.long	.Ltmp486-.Lfunc_begin11 # >> Call Site 5 <<
	.long	.Ltmp487-.Ltmp486       #   Call between .Ltmp486 and .Ltmp487
	.long	.Ltmp488-.Lfunc_begin11 #     jumps to .Ltmp488
	.byte	1                       #   On action: 1
	.long	.Ltmp468-.Lfunc_begin11 # >> Call Site 6 <<
	.long	.Ltmp469-.Ltmp468       #   Call between .Ltmp468 and .Ltmp469
	.long	.Ltmp470-.Lfunc_begin11 #     jumps to .Ltmp470
	.byte	1                       #   On action: 1
	.long	.Ltmp474-.Lfunc_begin11 # >> Call Site 7 <<
	.long	.Ltmp475-.Ltmp474       #   Call between .Ltmp474 and .Ltmp475
	.long	.Ltmp476-.Lfunc_begin11 #     jumps to .Ltmp476
	.byte	1                       #   On action: 1
	.long	.Ltmp480-.Lfunc_begin11 # >> Call Site 8 <<
	.long	.Ltmp481-.Ltmp480       #   Call between .Ltmp480 and .Ltmp481
	.long	.Ltmp482-.Lfunc_begin11 #     jumps to .Ltmp482
	.byte	1                       #   On action: 1
	.long	.Ltmp481-.Lfunc_begin11 # >> Call Site 9 <<
	.long	.Ltmp477-.Ltmp481       #   Call between .Ltmp481 and .Ltmp477
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp477-.Lfunc_begin11 # >> Call Site 10 <<
	.long	.Ltmp478-.Ltmp477       #   Call between .Ltmp477 and .Ltmp478
	.long	.Ltmp479-.Lfunc_begin11 #     jumps to .Ltmp479
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi184:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi185:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi186:
	.cfi_def_cfa_offset 32
.Lcfi187:
	.cfi_offset %rbx, -24
.Lcfi188:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE+16, (%rbx)
.Ltmp492:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp493:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB35_2:
.Ltmp494:
	movq	%rax, %r14
.Ltmp495:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp496:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB35_4:
.Ltmp497:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end35:
	.size	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev, .Lfunc_end35-_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp492-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp493-.Ltmp492       #   Call between .Ltmp492 and .Ltmp493
	.long	.Ltmp494-.Lfunc_begin12 #     jumps to .Ltmp494
	.byte	0                       #   On action: cleanup
	.long	.Ltmp493-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp495-.Ltmp493       #   Call between .Ltmp493 and .Ltmp495
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp495-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp496-.Ltmp495       #   Call between .Ltmp495 and .Ltmp496
	.long	.Ltmp497-.Lfunc_begin12 #     jumps to .Ltmp497
	.byte	1                       #   On action: 1
	.long	.Ltmp496-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Lfunc_end35-.Ltmp496   #   Call between .Ltmp496 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI36_0:
	.zero	16
	.section	.text._ZN8NArchive4NCab12CArchiveInfoC2Ev,"axG",@progbits,_ZN8NArchive4NCab12CArchiveInfoC2Ev,comdat
	.weak	_ZN8NArchive4NCab12CArchiveInfoC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab12CArchiveInfoC2Ev,@function
_ZN8NArchive4NCab12CArchiveInfoC2Ev:    # @_ZN8NArchive4NCab12CArchiveInfoC2Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r15
.Lcfi189:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi190:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi191:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi192:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi193:
	.cfi_def_cfa_offset 48
.Lcfi194:
	.cfi_offset %rbx, -40
.Lcfi195:
	.cfi_offset %r12, -32
.Lcfi196:
	.cfi_offset %r14, -24
.Lcfi197:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r12
	movq	%r12, 16(%rbx)
	movb	$0, (%r12)
	movl	$4, 28(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
.Ltmp498:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp499:
# BB#1:                                 # %_ZN8NArchive4NCab13COtherArchiveC2Ev.exit
	movq	%r15, 32(%rbx)
	movb	$0, (%r15)
	movl	$4, 44(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
.Ltmp501:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp502:
# BB#2:                                 # %.noexc
	movq	%r12, 48(%rbx)
	movb	$0, (%r12)
	movl	$4, 60(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%rbx)
.Ltmp504:
	movl	$4, %edi
	callq	_Znam
.Ltmp505:
# BB#3:
	movq	%rax, 64(%rbx)
	movb	$0, (%rax)
	movl	$4, 76(%rbx)
	movw	$0, 12(%rbx)
	movw	$0, 14(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB36_5:                               # %.body
.Ltmp506:
	movq	%rax, %r14
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	32(%rbx), %r15
	testq	%r15, %r15
	jne	.LBB36_6
	jmp	.LBB36_7
.LBB36_4:                               # %.body.thread
.Ltmp503:
	movq	%rax, %r14
.LBB36_6:
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB36_7:                               # %_ZN11CStringBaseIcED2Ev.exit.i4
	addq	$16, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB36_8
	jmp	.LBB36_9
.LBB36_10:                              # %_ZN11CStringBaseIcED2Ev.exit.i
.Ltmp500:
	movq	%rax, %r14
	movq	%r12, %rdi
.LBB36_8:
	callq	_ZdaPv
.LBB36_9:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end36:
	.size	_ZN8NArchive4NCab12CArchiveInfoC2Ev, .Lfunc_end36-_ZN8NArchive4NCab12CArchiveInfoC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table36:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin13-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp498-.Lfunc_begin13 #   Call between .Lfunc_begin13 and .Ltmp498
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp498-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp499-.Ltmp498       #   Call between .Ltmp498 and .Ltmp499
	.long	.Ltmp500-.Lfunc_begin13 #     jumps to .Ltmp500
	.byte	0                       #   On action: cleanup
	.long	.Ltmp501-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp502-.Ltmp501       #   Call between .Ltmp501 and .Ltmp502
	.long	.Ltmp503-.Lfunc_begin13 #     jumps to .Ltmp503
	.byte	0                       #   On action: cleanup
	.long	.Ltmp504-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Ltmp505-.Ltmp504       #   Call between .Ltmp504 and .Ltmp505
	.long	.Ltmp506-.Lfunc_begin13 #     jumps to .Ltmp506
	.byte	0                       #   On action: cleanup
	.long	.Ltmp505-.Lfunc_begin13 # >> Call Site 5 <<
	.long	.Lfunc_end36-.Ltmp505   #   Call between .Ltmp505 and .Lfunc_end36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi198:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi199:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi200:
	.cfi_def_cfa_offset 32
.Lcfi201:
	.cfi_offset %rbx, -24
.Lcfi202:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE+16, (%rbx)
.Ltmp507:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp508:
# BB#1:
.Ltmp513:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp514:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB37_5:
.Ltmp515:
	movq	%rax, %r14
	jmp	.LBB37_6
.LBB37_3:
.Ltmp509:
	movq	%rax, %r14
.Ltmp510:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp511:
.LBB37_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB37_4:
.Ltmp512:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end37:
	.size	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev, .Lfunc_end37-_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table37:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp507-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp508-.Ltmp507       #   Call between .Ltmp507 and .Ltmp508
	.long	.Ltmp509-.Lfunc_begin14 #     jumps to .Ltmp509
	.byte	0                       #   On action: cleanup
	.long	.Ltmp513-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp514-.Ltmp513       #   Call between .Ltmp513 and .Ltmp514
	.long	.Ltmp515-.Lfunc_begin14 #     jumps to .Ltmp515
	.byte	0                       #   On action: cleanup
	.long	.Ltmp510-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp511-.Ltmp510       #   Call between .Ltmp510 and .Ltmp511
	.long	.Ltmp512-.Lfunc_begin14 #     jumps to .Ltmp512
	.byte	1                       #   On action: 1
	.long	.Ltmp511-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end37-.Ltmp511   #   Call between .Ltmp511 and .Lfunc_end37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi203:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi204:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi205:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi206:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi207:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi208:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi209:
	.cfi_def_cfa_offset 64
.Lcfi210:
	.cfi_offset %rbx, -56
.Lcfi211:
	.cfi_offset %r12, -48
.Lcfi212:
	.cfi_offset %r13, -40
.Lcfi213:
	.cfi_offset %r14, -32
.Lcfi214:
	.cfi_offset %r15, -24
.Lcfi215:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r14d
	movq	%rdi, %r12
	leal	(%rdx,%r14), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	subl	%r14d, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB38_5
# BB#1:                                 # %.lr.ph
	movslq	%r14d, %rbp
	movslq	%r15d, %r13
	shlq	$3, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB38_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbp, %rax
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB38_4
# BB#3:                                 #   in Loop: Header=BB38_2 Depth=1
	callq	_ZdlPv
.LBB38_4:                               #   in Loop: Header=BB38_2 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jl	.LBB38_2
.LBB38_5:                               # %._crit_edge
	movq	%r12, %rdi
	movl	%r14d, %esi
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end38:
	.size	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii, .Lfunc_end38-_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi216:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi217:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi218:
	.cfi_def_cfa_offset 32
.Lcfi219:
	.cfi_offset %rbx, -24
.Lcfi220:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE+16, (%rbx)
.Ltmp516:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp517:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB39_2:
.Ltmp518:
	movq	%rax, %r14
.Ltmp519:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp520:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB39_4:
.Ltmp521:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end39:
	.size	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev, .Lfunc_end39-_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table39:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp516-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp517-.Ltmp516       #   Call between .Ltmp516 and .Ltmp517
	.long	.Ltmp518-.Lfunc_begin15 #     jumps to .Ltmp518
	.byte	0                       #   On action: cleanup
	.long	.Ltmp517-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp519-.Ltmp517       #   Call between .Ltmp517 and .Ltmp519
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp519-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp520-.Ltmp519       #   Call between .Ltmp519 and .Ltmp520
	.long	.Ltmp521-.Lfunc_begin15 #     jumps to .Ltmp521
	.byte	1                       #   On action: 1
	.long	.Ltmp520-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Lfunc_end39-.Ltmp520   #   Call between .Ltmp520 and .Lfunc_end39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%r14
.Lcfi221:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi222:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi223:
	.cfi_def_cfa_offset 32
.Lcfi224:
	.cfi_offset %rbx, -24
.Lcfi225:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE+16, (%rbx)
.Ltmp522:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp523:
# BB#1:
.Ltmp528:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp529:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB40_5:
.Ltmp530:
	movq	%rax, %r14
	jmp	.LBB40_6
.LBB40_3:
.Ltmp524:
	movq	%rax, %r14
.Ltmp525:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp526:
.LBB40_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB40_4:
.Ltmp527:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end40:
	.size	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev, .Lfunc_end40-_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp522-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp523-.Ltmp522       #   Call between .Ltmp522 and .Ltmp523
	.long	.Ltmp524-.Lfunc_begin16 #     jumps to .Ltmp524
	.byte	0                       #   On action: cleanup
	.long	.Ltmp528-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp529-.Ltmp528       #   Call between .Ltmp528 and .Ltmp529
	.long	.Ltmp530-.Lfunc_begin16 #     jumps to .Ltmp530
	.byte	0                       #   On action: cleanup
	.long	.Ltmp525-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Ltmp526-.Ltmp525       #   Call between .Ltmp525 and .Ltmp526
	.long	.Ltmp527-.Lfunc_begin16 #     jumps to .Ltmp527
	.byte	1                       #   On action: 1
	.long	.Ltmp526-.Lfunc_begin16 # >> Call Site 4 <<
	.long	.Lfunc_end40-.Ltmp526   #   Call between .Ltmp526 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi226:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi227:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi228:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi229:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi230:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi231:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi232:
	.cfi_def_cfa_offset 64
.Lcfi233:
	.cfi_offset %rbx, -56
.Lcfi234:
	.cfi_offset %r12, -48
.Lcfi235:
	.cfi_offset %r13, -40
.Lcfi236:
	.cfi_offset %r14, -32
.Lcfi237:
	.cfi_offset %r15, -24
.Lcfi238:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB41_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB41_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB41_6
# BB#3:                                 #   in Loop: Header=BB41_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB41_5
# BB#4:                                 #   in Loop: Header=BB41_2 Depth=1
	callq	_ZdaPv
.LBB41_5:                               # %_ZN8NArchive4NCab5CItemD2Ev.exit
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB41_6:                               #   in Loop: Header=BB41_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB41_2
.LBB41_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end41:
	.size	_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii, .Lfunc_end41-_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi239:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB42_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB42_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB42_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB42_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB42_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB42_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB42_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB42_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB42_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB42_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB42_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB42_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB42_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB42_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB42_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB42_16
.LBB42_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICompressGetInStreamProcessedSize(%rip), %cl
	jne	.LBB42_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+1(%rip), %al
	jne	.LBB42_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+2(%rip), %al
	jne	.LBB42_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+3(%rip), %al
	jne	.LBB42_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+4(%rip), %al
	jne	.LBB42_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+5(%rip), %al
	jne	.LBB42_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+6(%rip), %al
	jne	.LBB42_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+7(%rip), %al
	jne	.LBB42_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+8(%rip), %al
	jne	.LBB42_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+9(%rip), %al
	jne	.LBB42_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+10(%rip), %al
	jne	.LBB42_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+11(%rip), %al
	jne	.LBB42_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+12(%rip), %al
	jne	.LBB42_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+13(%rip), %al
	jne	.LBB42_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+14(%rip), %al
	jne	.LBB42_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit12
	movb	15(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+15(%rip), %al
	jne	.LBB42_33
.LBB42_16:
	leaq	8(%rdi), %rax
	jmp	.LBB42_84
.LBB42_33:                              # %_ZeqRK4GUIDS1_.exit12.thread
	cmpb	IID_ICompressSetInStream(%rip), %cl
	jne	.LBB42_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetInStream+1(%rip), %al
	jne	.LBB42_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetInStream+2(%rip), %al
	jne	.LBB42_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetInStream+3(%rip), %al
	jne	.LBB42_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetInStream+4(%rip), %al
	jne	.LBB42_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetInStream+5(%rip), %al
	jne	.LBB42_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetInStream+6(%rip), %al
	jne	.LBB42_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetInStream+7(%rip), %al
	jne	.LBB42_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetInStream+8(%rip), %al
	jne	.LBB42_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetInStream+9(%rip), %al
	jne	.LBB42_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetInStream+10(%rip), %al
	jne	.LBB42_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetInStream+11(%rip), %al
	jne	.LBB42_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetInStream+12(%rip), %al
	jne	.LBB42_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetInStream+13(%rip), %al
	jne	.LBB42_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetInStream+14(%rip), %al
	jne	.LBB42_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit16
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetInStream+15(%rip), %al
	jne	.LBB42_50
# BB#49:
	leaq	16(%rdi), %rax
	jmp	.LBB42_84
.LBB42_50:                              # %_ZeqRK4GUIDS1_.exit16.thread
	cmpb	IID_ICompressSetOutStreamSize(%rip), %cl
	jne	.LBB42_67
# BB#51:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+1(%rip), %al
	jne	.LBB42_67
# BB#52:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+2(%rip), %al
	jne	.LBB42_67
# BB#53:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+3(%rip), %al
	jne	.LBB42_67
# BB#54:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+4(%rip), %al
	jne	.LBB42_67
# BB#55:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+5(%rip), %al
	jne	.LBB42_67
# BB#56:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+6(%rip), %al
	jne	.LBB42_67
# BB#57:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+7(%rip), %al
	jne	.LBB42_67
# BB#58:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+8(%rip), %al
	jne	.LBB42_67
# BB#59:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+9(%rip), %al
	jne	.LBB42_67
# BB#60:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+10(%rip), %al
	jne	.LBB42_67
# BB#61:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+11(%rip), %al
	jne	.LBB42_67
# BB#62:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+12(%rip), %al
	jne	.LBB42_67
# BB#63:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+13(%rip), %al
	jne	.LBB42_67
# BB#64:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+14(%rip), %al
	jne	.LBB42_67
# BB#65:                                # %_ZeqRK4GUIDS1_.exit18
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+15(%rip), %al
	jne	.LBB42_67
# BB#66:
	leaq	24(%rdi), %rax
	jmp	.LBB42_84
.LBB42_67:                              # %_ZeqRK4GUIDS1_.exit18.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ISequentialInStream(%rip), %cl
	jne	.LBB42_85
# BB#68:
	movb	1(%rsi), %cl
	cmpb	IID_ISequentialInStream+1(%rip), %cl
	jne	.LBB42_85
# BB#69:
	movb	2(%rsi), %cl
	cmpb	IID_ISequentialInStream+2(%rip), %cl
	jne	.LBB42_85
# BB#70:
	movb	3(%rsi), %cl
	cmpb	IID_ISequentialInStream+3(%rip), %cl
	jne	.LBB42_85
# BB#71:
	movb	4(%rsi), %cl
	cmpb	IID_ISequentialInStream+4(%rip), %cl
	jne	.LBB42_85
# BB#72:
	movb	5(%rsi), %cl
	cmpb	IID_ISequentialInStream+5(%rip), %cl
	jne	.LBB42_85
# BB#73:
	movb	6(%rsi), %cl
	cmpb	IID_ISequentialInStream+6(%rip), %cl
	jne	.LBB42_85
# BB#74:
	movb	7(%rsi), %cl
	cmpb	IID_ISequentialInStream+7(%rip), %cl
	jne	.LBB42_85
# BB#75:
	movb	8(%rsi), %cl
	cmpb	IID_ISequentialInStream+8(%rip), %cl
	jne	.LBB42_85
# BB#76:
	movb	9(%rsi), %cl
	cmpb	IID_ISequentialInStream+9(%rip), %cl
	jne	.LBB42_85
# BB#77:
	movb	10(%rsi), %cl
	cmpb	IID_ISequentialInStream+10(%rip), %cl
	jne	.LBB42_85
# BB#78:
	movb	11(%rsi), %cl
	cmpb	IID_ISequentialInStream+11(%rip), %cl
	jne	.LBB42_85
# BB#79:
	movb	12(%rsi), %cl
	cmpb	IID_ISequentialInStream+12(%rip), %cl
	jne	.LBB42_85
# BB#80:
	movb	13(%rsi), %cl
	cmpb	IID_ISequentialInStream+13(%rip), %cl
	jne	.LBB42_85
# BB#81:
	movb	14(%rsi), %cl
	cmpb	IID_ISequentialInStream+14(%rip), %cl
	jne	.LBB42_85
# BB#82:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %cl
	cmpb	IID_ISequentialInStream+15(%rip), %cl
	jne	.LBB42_85
# BB#83:
	leaq	32(%rdi), %rax
.LBB42_84:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB42_85:                              # %_ZeqRK4GUIDS1_.exit14.thread
	popq	%rcx
	retq
.Lfunc_end42:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end42-_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	40(%rdi), %eax
	incl	%eax
	movl	%eax, 40(%rdi)
	retq
.Lfunc_end43:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end43-_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi240:
	.cfi_def_cfa_offset 16
	movl	40(%rdi), %eax
	decl	%eax
	movl	%eax, 40(%rdi)
	jne	.LBB44_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB44_2:
	popq	%rcx
	retq
.Lfunc_end44:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end44-_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev,@function
_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev: # @_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi241:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi242:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi243:
	.cfi_def_cfa_offset 32
.Lcfi244:
	.cfi_offset %rbx, -24
.Lcfi245:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+264, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movq	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+328, 32(%rbx)
	leaq	112(%rbx), %rdi
.Ltmp531:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp532:
# BB#1:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB45_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp537:
	callq	*16(%rax)
.Ltmp538:
.LBB45_3:                               # %_ZN5NBitl12CBaseDecoderI9CInBufferED2Ev.exit
	leaq	48(%rbx), %rdi
.Ltmp549:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp550:
# BB#4:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB45_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp555:
	callq	*16(%rax)
.Ltmp556:
.LBB45_6:                               # %_ZN10COutBufferD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB45_20:
.Ltmp557:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB45_13:
.Ltmp539:
	movq	%rax, %r14
	jmp	.LBB45_14
.LBB45_10:
.Ltmp551:
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB45_21
# BB#11:
	movq	(%rdi), %rax
.Ltmp552:
	callq	*16(%rax)
.Ltmp553:
	jmp	.LBB45_21
.LBB45_12:
.Ltmp554:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB45_7:
.Ltmp533:
	movq	%rax, %r14
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB45_14
# BB#8:
	movq	(%rdi), %rax
.Ltmp534:
	callq	*16(%rax)
.Ltmp535:
.LBB45_14:                              # %.body
	leaq	48(%rbx), %rdi
.Ltmp540:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp541:
# BB#15:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB45_21
# BB#16:
	movq	(%rdi), %rax
.Ltmp546:
	callq	*16(%rax)
.Ltmp547:
.LBB45_21:                              # %_ZN10COutBufferD2Ev.exit14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB45_9:
.Ltmp536:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB45_22:
.Ltmp548:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB45_17:
.Ltmp542:
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB45_23
# BB#18:
	movq	(%rdi), %rax
.Ltmp543:
	callq	*16(%rax)
.Ltmp544:
.LBB45_23:                              # %.body12
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB45_19:
.Ltmp545:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end45:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev, .Lfunc_end45-_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table45:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp531-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp532-.Ltmp531       #   Call between .Ltmp531 and .Ltmp532
	.long	.Ltmp533-.Lfunc_begin17 #     jumps to .Ltmp533
	.byte	0                       #   On action: cleanup
	.long	.Ltmp537-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Ltmp538-.Ltmp537       #   Call between .Ltmp537 and .Ltmp538
	.long	.Ltmp539-.Lfunc_begin17 #     jumps to .Ltmp539
	.byte	0                       #   On action: cleanup
	.long	.Ltmp549-.Lfunc_begin17 # >> Call Site 3 <<
	.long	.Ltmp550-.Ltmp549       #   Call between .Ltmp549 and .Ltmp550
	.long	.Ltmp551-.Lfunc_begin17 #     jumps to .Ltmp551
	.byte	0                       #   On action: cleanup
	.long	.Ltmp555-.Lfunc_begin17 # >> Call Site 4 <<
	.long	.Ltmp556-.Ltmp555       #   Call between .Ltmp555 and .Ltmp556
	.long	.Ltmp557-.Lfunc_begin17 #     jumps to .Ltmp557
	.byte	0                       #   On action: cleanup
	.long	.Ltmp556-.Lfunc_begin17 # >> Call Site 5 <<
	.long	.Ltmp552-.Ltmp556       #   Call between .Ltmp556 and .Ltmp552
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp552-.Lfunc_begin17 # >> Call Site 6 <<
	.long	.Ltmp553-.Ltmp552       #   Call between .Ltmp552 and .Ltmp553
	.long	.Ltmp554-.Lfunc_begin17 #     jumps to .Ltmp554
	.byte	1                       #   On action: 1
	.long	.Ltmp534-.Lfunc_begin17 # >> Call Site 7 <<
	.long	.Ltmp535-.Ltmp534       #   Call between .Ltmp534 and .Ltmp535
	.long	.Ltmp536-.Lfunc_begin17 #     jumps to .Ltmp536
	.byte	1                       #   On action: 1
	.long	.Ltmp540-.Lfunc_begin17 # >> Call Site 8 <<
	.long	.Ltmp541-.Ltmp540       #   Call between .Ltmp540 and .Ltmp541
	.long	.Ltmp542-.Lfunc_begin17 #     jumps to .Ltmp542
	.byte	1                       #   On action: 1
	.long	.Ltmp546-.Lfunc_begin17 # >> Call Site 9 <<
	.long	.Ltmp547-.Ltmp546       #   Call between .Ltmp546 and .Ltmp547
	.long	.Ltmp548-.Lfunc_begin17 #     jumps to .Ltmp548
	.byte	1                       #   On action: 1
	.long	.Ltmp547-.Lfunc_begin17 # >> Call Site 10 <<
	.long	.Ltmp543-.Ltmp547       #   Call between .Ltmp547 and .Ltmp543
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp543-.Lfunc_begin17 # >> Call Site 11 <<
	.long	.Ltmp544-.Ltmp543       #   Call between .Ltmp543 and .Ltmp544
	.long	.Ltmp545-.Lfunc_begin17 #     jumps to .Ltmp545
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,@function
_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev: # @_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi246:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi247:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi248:
	.cfi_def_cfa_offset 32
.Lcfi249:
	.cfi_offset %rbx, -24
.Lcfi250:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp558:
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp559:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB46_2:
.Ltmp560:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end46:
	.size	_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev, .Lfunc_end46-_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table46:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp558-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp559-.Ltmp558       #   Call between .Ltmp558 and .Ltmp559
	.long	.Ltmp560-.Lfunc_begin18 #     jumps to .Ltmp560
	.byte	0                       #   On action: cleanup
	.long	.Ltmp559-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Lfunc_end46-.Ltmp559   #   Call between .Ltmp559 and .Lfunc_end46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end47:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end47-_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	32(%rdi), %eax
	incl	%eax
	movl	%eax, 32(%rdi)
	retq
.Lfunc_end48:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end48-_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi251:
	.cfi_def_cfa_offset 16
	movl	32(%rdi), %eax
	decl	%eax
	movl	%eax, 32(%rdi)
	jne	.LBB49_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB49_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end49:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end49-_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,@function
_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev: # @_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end50:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev, .Lfunc_end50-_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,@function
_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev: # @_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:
	pushq	%r14
.Lcfi252:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi253:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi254:
	.cfi_def_cfa_offset 32
.Lcfi255:
	.cfi_offset %rbx, -24
.Lcfi256:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp561:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp562:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB51_2:
.Ltmp563:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end51:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev, .Lfunc_end51-_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table51:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp561-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp562-.Ltmp561       #   Call between .Ltmp561 and .Ltmp562
	.long	.Ltmp563-.Lfunc_begin19 #     jumps to .Ltmp563
	.byte	0                       #   On action: cleanup
	.long	.Ltmp562-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Lfunc_end51-.Ltmp562   #   Call between .Ltmp562 and .Lfunc_end51
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end52:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end52-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end53:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end53-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi257:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB54_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB54_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end54:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end54-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,@function
_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev: # @_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end55:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev, .Lfunc_end55-_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,@function
_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev: # @_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%r14
.Lcfi258:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi259:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi260:
	.cfi_def_cfa_offset 32
.Lcfi261:
	.cfi_offset %rbx, -24
.Lcfi262:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp564:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp565:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB56_2:
.Ltmp566:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end56:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev, .Lfunc_end56-_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table56:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp564-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp565-.Ltmp564       #   Call between .Ltmp564 and .Ltmp565
	.long	.Ltmp566-.Lfunc_begin20 #     jumps to .Ltmp566
	.byte	0                       #   On action: cleanup
	.long	.Ltmp565-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Lfunc_end56-.Ltmp565   #   Call between .Ltmp565 and .Lfunc_end56
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end57:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end57-_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end58:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end58-_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi263:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB59_2
# BB#1:
	addq	$-24, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB59_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end59:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end59-_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,@function
_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev: # @_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end60:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev, .Lfunc_end60-_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,@function
_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev: # @_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%r14
.Lcfi264:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi265:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi266:
	.cfi_def_cfa_offset 32
.Lcfi267:
	.cfi_offset %rbx, -24
.Lcfi268:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-24, %rbx
.Ltmp567:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp568:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB61_2:
.Ltmp569:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end61:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev, .Lfunc_end61-_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table61:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp567-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp568-.Ltmp567       #   Call between .Ltmp567 and .Ltmp568
	.long	.Ltmp569-.Lfunc_begin21 #     jumps to .Ltmp569
	.byte	0                       #   On action: cleanup
	.long	.Ltmp568-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Lfunc_end61-.Ltmp568   #   Call between .Ltmp568 and .Lfunc_end61
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end62:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end62-_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end63:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end63-_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi269:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB64_2
# BB#1:
	addq	$-32, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB64_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end64:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end64-_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev,@function
_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev: # @_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end65:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev, .Lfunc_end65-_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev,@function
_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev: # @_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:
	pushq	%r14
.Lcfi270:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi271:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi272:
	.cfi_def_cfa_offset 32
.Lcfi273:
	.cfi_offset %rbx, -24
.Lcfi274:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-32, %rbx
.Ltmp570:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp571:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB66_2:
.Ltmp572:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end66:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev, .Lfunc_end66-_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table66:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp570-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp571-.Ltmp570       #   Call between .Ltmp570 and .Ltmp571
	.long	.Ltmp572-.Lfunc_begin22 #     jumps to .Ltmp572
	.byte	0                       #   On action: cleanup
	.long	.Ltmp571-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Lfunc_end66-.Ltmp571   #   Call between .Ltmp571 and .Lfunc_end66
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive4NCab13CMvDatabaseExD2Ev,"axG",@progbits,_ZN8NArchive4NCab13CMvDatabaseExD2Ev,comdat
	.weak	_ZN8NArchive4NCab13CMvDatabaseExD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab13CMvDatabaseExD2Ev,@function
_ZN8NArchive4NCab13CMvDatabaseExD2Ev:   # @_ZN8NArchive4NCab13CMvDatabaseExD2Ev
.Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception23
# BB#0:
	pushq	%r14
.Lcfi275:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi276:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi277:
	.cfi_def_cfa_offset 32
.Lcfi278:
	.cfi_offset %rbx, -24
.Lcfi279:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	96(%rbx), %rdi
.Ltmp573:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp574:
# BB#1:
	leaq	64(%rbx), %rdi
.Ltmp578:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp579:
# BB#2:
	leaq	32(%rbx), %rdi
.Ltmp583:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp584:
# BB#3:
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE+16, (%rbx)
.Ltmp595:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp596:
# BB#4:                                 # %_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB67_5:
.Ltmp597:
	movq	%rax, %r14
.Ltmp598:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp599:
	jmp	.LBB67_6
.LBB67_7:
.Ltmp600:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB67_9:
.Ltmp585:
	movq	%rax, %r14
	jmp	.LBB67_12
.LBB67_10:
.Ltmp580:
	movq	%rax, %r14
	jmp	.LBB67_11
.LBB67_8:
.Ltmp575:
	movq	%rax, %r14
	leaq	64(%rbx), %rdi
.Ltmp576:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp577:
.LBB67_11:
	leaq	32(%rbx), %rdi
.Ltmp581:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp582:
.LBB67_12:
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE+16, (%rbx)
.Ltmp586:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp587:
# BB#13:
.Ltmp592:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp593:
.LBB67_6:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB67_14:
.Ltmp588:
	movq	%rax, %r14
.Ltmp589:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp590:
	jmp	.LBB67_17
.LBB67_15:
.Ltmp591:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB67_16:
.Ltmp594:
	movq	%rax, %r14
.LBB67_17:                              # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end67:
	.size	_ZN8NArchive4NCab13CMvDatabaseExD2Ev, .Lfunc_end67-_ZN8NArchive4NCab13CMvDatabaseExD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table67:
.Lexception23:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp573-.Lfunc_begin23 # >> Call Site 1 <<
	.long	.Ltmp574-.Ltmp573       #   Call between .Ltmp573 and .Ltmp574
	.long	.Ltmp575-.Lfunc_begin23 #     jumps to .Ltmp575
	.byte	0                       #   On action: cleanup
	.long	.Ltmp578-.Lfunc_begin23 # >> Call Site 2 <<
	.long	.Ltmp579-.Ltmp578       #   Call between .Ltmp578 and .Ltmp579
	.long	.Ltmp580-.Lfunc_begin23 #     jumps to .Ltmp580
	.byte	0                       #   On action: cleanup
	.long	.Ltmp583-.Lfunc_begin23 # >> Call Site 3 <<
	.long	.Ltmp584-.Ltmp583       #   Call between .Ltmp583 and .Ltmp584
	.long	.Ltmp585-.Lfunc_begin23 #     jumps to .Ltmp585
	.byte	0                       #   On action: cleanup
	.long	.Ltmp595-.Lfunc_begin23 # >> Call Site 4 <<
	.long	.Ltmp596-.Ltmp595       #   Call between .Ltmp595 and .Ltmp596
	.long	.Ltmp597-.Lfunc_begin23 #     jumps to .Ltmp597
	.byte	0                       #   On action: cleanup
	.long	.Ltmp596-.Lfunc_begin23 # >> Call Site 5 <<
	.long	.Ltmp598-.Ltmp596       #   Call between .Ltmp596 and .Ltmp598
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp598-.Lfunc_begin23 # >> Call Site 6 <<
	.long	.Ltmp599-.Ltmp598       #   Call between .Ltmp598 and .Ltmp599
	.long	.Ltmp600-.Lfunc_begin23 #     jumps to .Ltmp600
	.byte	1                       #   On action: 1
	.long	.Ltmp576-.Lfunc_begin23 # >> Call Site 7 <<
	.long	.Ltmp582-.Ltmp576       #   Call between .Ltmp576 and .Ltmp582
	.long	.Ltmp594-.Lfunc_begin23 #     jumps to .Ltmp594
	.byte	1                       #   On action: 1
	.long	.Ltmp586-.Lfunc_begin23 # >> Call Site 8 <<
	.long	.Ltmp587-.Ltmp586       #   Call between .Ltmp586 and .Ltmp587
	.long	.Ltmp588-.Lfunc_begin23 #     jumps to .Ltmp588
	.byte	1                       #   On action: 1
	.long	.Ltmp592-.Lfunc_begin23 # >> Call Site 9 <<
	.long	.Ltmp593-.Ltmp592       #   Call between .Ltmp592 and .Ltmp593
	.long	.Ltmp594-.Lfunc_begin23 #     jumps to .Ltmp594
	.byte	1                       #   On action: 1
	.long	.Ltmp593-.Lfunc_begin23 # >> Call Site 10 <<
	.long	.Ltmp589-.Ltmp593       #   Call between .Ltmp593 and .Ltmp589
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp589-.Lfunc_begin23 # >> Call Site 11 <<
	.long	.Ltmp590-.Ltmp589       #   Call between .Ltmp589 and .Ltmp590
	.long	.Ltmp591-.Lfunc_begin23 #     jumps to .Ltmp591
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev
.Lfunc_begin24:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception24
# BB#0:
	pushq	%r14
.Lcfi280:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi281:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi282:
	.cfi_def_cfa_offset 32
.Lcfi283:
	.cfi_offset %rbx, -24
.Lcfi284:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE+16, (%rbx)
.Ltmp601:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp602:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB68_2:
.Ltmp603:
	movq	%rax, %r14
.Ltmp604:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp605:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB68_4:
.Ltmp606:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end68:
	.size	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev, .Lfunc_end68-_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table68:
.Lexception24:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp601-.Lfunc_begin24 # >> Call Site 1 <<
	.long	.Ltmp602-.Ltmp601       #   Call between .Ltmp601 and .Ltmp602
	.long	.Ltmp603-.Lfunc_begin24 #     jumps to .Ltmp603
	.byte	0                       #   On action: cleanup
	.long	.Ltmp602-.Lfunc_begin24 # >> Call Site 2 <<
	.long	.Ltmp604-.Ltmp602       #   Call between .Ltmp602 and .Ltmp604
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp604-.Lfunc_begin24 # >> Call Site 3 <<
	.long	.Ltmp605-.Ltmp604       #   Call between .Ltmp604 and .Ltmp605
	.long	.Ltmp606-.Lfunc_begin24 #     jumps to .Ltmp606
	.byte	1                       #   On action: 1
	.long	.Ltmp605-.Lfunc_begin24 # >> Call Site 4 <<
	.long	.Lfunc_end68-.Ltmp605   #   Call between .Ltmp605 and .Lfunc_end68
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev
.Lfunc_begin25:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception25
# BB#0:
	pushq	%r14
.Lcfi285:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi286:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi287:
	.cfi_def_cfa_offset 32
.Lcfi288:
	.cfi_offset %rbx, -24
.Lcfi289:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE+16, (%rbx)
.Ltmp607:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp608:
# BB#1:
.Ltmp613:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp614:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB69_5:
.Ltmp615:
	movq	%rax, %r14
	jmp	.LBB69_6
.LBB69_3:
.Ltmp609:
	movq	%rax, %r14
.Ltmp610:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp611:
.LBB69_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB69_4:
.Ltmp612:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end69:
	.size	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev, .Lfunc_end69-_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table69:
.Lexception25:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp607-.Lfunc_begin25 # >> Call Site 1 <<
	.long	.Ltmp608-.Ltmp607       #   Call between .Ltmp607 and .Ltmp608
	.long	.Ltmp609-.Lfunc_begin25 #     jumps to .Ltmp609
	.byte	0                       #   On action: cleanup
	.long	.Ltmp613-.Lfunc_begin25 # >> Call Site 2 <<
	.long	.Ltmp614-.Ltmp613       #   Call between .Ltmp613 and .Ltmp614
	.long	.Ltmp615-.Lfunc_begin25 #     jumps to .Ltmp615
	.byte	0                       #   On action: cleanup
	.long	.Ltmp610-.Lfunc_begin25 # >> Call Site 3 <<
	.long	.Ltmp611-.Ltmp610       #   Call between .Ltmp610 and .Ltmp611
	.long	.Ltmp612-.Lfunc_begin25 #     jumps to .Ltmp612
	.byte	1                       #   On action: 1
	.long	.Ltmp611-.Lfunc_begin25 # >> Call Site 4 <<
	.long	.Lfunc_end69-.Ltmp611   #   Call between .Ltmp611 and .Lfunc_end69
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii
.Lfunc_begin26:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception26
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi290:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi291:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi292:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi293:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi294:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi295:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi296:
	.cfi_def_cfa_offset 64
.Lcfi297:
	.cfi_offset %rbx, -56
.Lcfi298:
	.cfi_offset %r12, -48
.Lcfi299:
	.cfi_offset %r13, -40
.Lcfi300:
	.cfi_offset %r14, -32
.Lcfi301:
	.cfi_offset %r15, -24
.Lcfi302:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB70_8
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB70_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB70_7
# BB#3:                                 #   in Loop: Header=BB70_2 Depth=1
	movq	160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB70_5
# BB#4:                                 #   in Loop: Header=BB70_2 Depth=1
	movq	(%rdi), %rax
.Ltmp616:
	callq	*16(%rax)
.Ltmp617:
.LBB70_5:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit.i
                                        #   in Loop: Header=BB70_2 Depth=1
.Ltmp622:
	movq	%rbp, %rdi
	callq	_ZN8NArchive4NCab9CDatabaseD2Ev
.Ltmp623:
# BB#6:                                 # %_ZN8NArchive4NCab11CDatabaseExD2Ev.exit
                                        #   in Loop: Header=BB70_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB70_7:                               #   in Loop: Header=BB70_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB70_2
.LBB70_8:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB70_9:
.Ltmp618:
	movq	%rax, %rbx
.Ltmp619:
	movq	%rbp, %rdi
	callq	_ZN8NArchive4NCab9CDatabaseD2Ev
.Ltmp620:
	jmp	.LBB70_12
.LBB70_10:
.Ltmp621:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB70_11:
.Ltmp624:
	movq	%rax, %rbx
.LBB70_12:                              # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end70:
	.size	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii, .Lfunc_end70-_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table70:
.Lexception26:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp616-.Lfunc_begin26 # >> Call Site 1 <<
	.long	.Ltmp617-.Ltmp616       #   Call between .Ltmp616 and .Ltmp617
	.long	.Ltmp618-.Lfunc_begin26 #     jumps to .Ltmp618
	.byte	0                       #   On action: cleanup
	.long	.Ltmp622-.Lfunc_begin26 # >> Call Site 2 <<
	.long	.Ltmp623-.Ltmp622       #   Call between .Ltmp622 and .Ltmp623
	.long	.Ltmp624-.Lfunc_begin26 #     jumps to .Ltmp624
	.byte	0                       #   On action: cleanup
	.long	.Ltmp623-.Lfunc_begin26 # >> Call Site 3 <<
	.long	.Ltmp619-.Ltmp623       #   Call between .Ltmp623 and .Ltmp619
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp619-.Lfunc_begin26 # >> Call Site 4 <<
	.long	.Ltmp620-.Ltmp619       #   Call between .Ltmp619 and .Ltmp620
	.long	.Ltmp621-.Lfunc_begin26 #     jumps to .Ltmp621
	.byte	1                       #   On action: 1
	.long	.Ltmp620-.Lfunc_begin26 # >> Call Site 5 <<
	.long	.Lfunc_end70-.Ltmp620   #   Call between .Ltmp620 and .Lfunc_end70
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIhED0Ev,"axG",@progbits,_ZN13CRecordVectorIhED0Ev,comdat
	.weak	_ZN13CRecordVectorIhED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIhED0Ev,@function
_ZN13CRecordVectorIhED0Ev:              # @_ZN13CRecordVectorIhED0Ev
.Lfunc_begin27:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception27
# BB#0:
	pushq	%r14
.Lcfi303:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi304:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi305:
	.cfi_def_cfa_offset 32
.Lcfi306:
	.cfi_offset %rbx, -24
.Lcfi307:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp625:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp626:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB71_2:
.Ltmp627:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end71:
	.size	_ZN13CRecordVectorIhED0Ev, .Lfunc_end71-_ZN13CRecordVectorIhED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table71:
.Lexception27:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp625-.Lfunc_begin27 # >> Call Site 1 <<
	.long	.Ltmp626-.Ltmp625       #   Call between .Ltmp625 and .Ltmp626
	.long	.Ltmp627-.Lfunc_begin27 #     jumps to .Ltmp627
	.byte	0                       #   On action: cleanup
	.long	.Ltmp626-.Lfunc_begin27 # >> Call Site 2 <<
	.long	.Lfunc_end71-.Ltmp626   #   Call between .Ltmp626 and .Lfunc_end71
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive4NCab9CDatabaseC2ERKS1_,"axG",@progbits,_ZN8NArchive4NCab9CDatabaseC2ERKS1_,comdat
	.weak	_ZN8NArchive4NCab9CDatabaseC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab9CDatabaseC2ERKS1_,@function
_ZN8NArchive4NCab9CDatabaseC2ERKS1_:    # @_ZN8NArchive4NCab9CDatabaseC2ERKS1_
.Lfunc_begin28:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception28
# BB#0:
	pushq	%rbp
.Lcfi308:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi309:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi310:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi311:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi312:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi313:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi314:
	.cfi_def_cfa_offset 64
.Lcfi315:
	.cfi_offset %rbx, -56
.Lcfi316:
	.cfi_offset %r12, -48
.Lcfi317:
	.cfi_offset %r13, -40
.Lcfi318:
	.cfi_offset %r14, -32
.Lcfi319:
	.cfi_offset %r15, -24
.Lcfi320:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	movups	8(%rsi), %xmm0
	movups	%xmm0, 8(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%r12)
	movq	%rsi, (%rsp)            # 8-byte Spill
	movslq	32(%rsi), %rax
	leaq	1(%rax), %rbx
	testl	%ebx, %ebx
	je	.LBB72_2
# BB#1:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rbx, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, 24(%r12)
	movb	$0, (%rax)
	movl	%ebx, 36(%r12)
	jmp	.LBB72_3
.LBB72_2:
	xorl	%eax, %eax
.LBB72_3:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i
	leaq	24(%r12), %r15
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	24(%rcx), %rcx
	.p2align	4, 0x90
.LBB72_4:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB72_4
# BB#5:                                 # %_ZN11CStringBaseIcEC2ERKS0_.exit.i
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	32(%rcx), %eax
	movl	%eax, 32(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%r12)
	movslq	48(%rcx), %rax
	leaq	1(%rax), %rbx
	testl	%ebx, %ebx
	je	.LBB72_8
# BB#6:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rbx, %rdi
	cmovlq	%rax, %rdi
.Ltmp628:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp629:
# BB#7:                                 # %.noexc.i
	movq	%rcx, 40(%r12)
	movb	$0, (%rcx)
	movl	%ebx, 52(%r12)
	jmp	.LBB72_9
.LBB72_8:
	xorl	%ecx, %ecx
.LBB72_9:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i4.i
	movq	(%rsp), %rax            # 8-byte Reload
	movq	40(%rax), %rdx
	.p2align	4, 0x90
.LBB72_10:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %eax
	incq	%rdx
	movb	%al, (%rcx)
	incq	%rcx
	testb	%al, %al
	jne	.LBB72_10
# BB#11:                                # %_ZN8NArchive4NCab13COtherArchiveC2ERKS1_.exit
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	48(%rcx), %eax
	movl	%eax, 48(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%r12)
	movslq	64(%rcx), %rax
	leaq	1(%rax), %rbx
	testl	%ebx, %ebx
	je	.LBB72_14
# BB#12:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rbx, %rdi
	cmovlq	%rax, %rdi
.Ltmp631:
	callq	_Znam
.Ltmp632:
# BB#13:                                # %.noexc
	movq	%rax, 56(%r12)
	movb	$0, (%rax)
	movl	%ebx, 68(%r12)
	jmp	.LBB72_15
.LBB72_14:
	xorl	%eax, %eax
.LBB72_15:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i9
	leaq	56(%r12), %rbx
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	56(%rcx), %rcx
	.p2align	4, 0x90
.LBB72_16:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB72_16
# BB#17:                                # %_ZN11CStringBaseIcEC2ERKS0_.exit.i12
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	64(%rcx), %eax
	movl	%eax, 64(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 72(%r12)
	movslq	80(%rcx), %rax
	leaq	1(%rax), %rbp
	testl	%ebp, %ebp
	je	.LBB72_20
# BB#18:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rbp, %rdi
	cmovlq	%rax, %rdi
.Ltmp634:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp635:
# BB#19:                                # %.noexc.i13
	movq	%rcx, 72(%r12)
	movb	$0, (%rcx)
	movl	%ebp, 84(%r12)
	jmp	.LBB72_21
.LBB72_20:
	xorl	%ecx, %ecx
.LBB72_21:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i4.i14
	movq	(%rsp), %rax            # 8-byte Reload
	movq	72(%rax), %rsi
	.p2align	4, 0x90
.LBB72_22:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %eax
	incq	%rsi
	movb	%al, (%rcx)
	incq	%rcx
	testb	%al, %al
	jne	.LBB72_22
# BB#23:                                # %_ZN8NArchive4NCab14CInArchiveInfoC2ERKS1_.exit
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	80(%rbx), %eax
	movl	%eax, 80(%r12)
	movq	88(%rbx), %rax
	movq	%rax, 88(%r12)
	leaq	96(%r12), %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%r12)
	movq	$8, 120(%r12)
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE+16, 96(%r12)
.Ltmp637:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp638:
# BB#24:                                # %.noexc21
	movl	108(%rbx), %r13d
	movl	108(%r12), %esi
	addl	%r13d, %esi
.Ltmp639:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp640:
# BB#25:                                # %.noexc22
	testl	%r13d, %r13d
	jle	.LBB72_30
# BB#26:                                # %.lr.ph.i.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB72_27:                              # =>This Inner Loop Header: Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	112(%rax), %rax
	movq	(%rax,%rbp,8), %r14
.Ltmp642:
	movl	$8, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp643:
# BB#28:                                # %.noexc23
                                        #   in Loop: Header=BB72_27 Depth=1
	movq	(%r14), %rax
	movq	%rax, (%rbx)
.Ltmp644:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp645:
# BB#29:                                # %.noexc24
                                        #   in Loop: Header=BB72_27 Depth=1
	movq	112(%r12), %rax
	movslq	108(%r12), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 108(%r12)
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB72_27
.LBB72_30:                              # %_ZN13CObjectVectorIN8NArchive4NCab7CFolderEEC2ERKS3_.exit
	movq	%r12, %rdi
	subq	$-128, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	subq	$-128, %rsi
.Ltmp650:
	callq	_ZN13CObjectVectorIN8NArchive4NCab5CItemEEC2ERKS3_
.Ltmp651:
# BB#31:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB72_32:
.Ltmp636:
	movq	%rax, %r14
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB72_35
# BB#33:
	callq	_ZdaPv
	jmp	.LBB72_35
.LBB72_34:
.Ltmp633:
	movq	%rax, %r14
.LBB72_35:                              # %.body18
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB72_37
# BB#36:
	callq	_ZdaPv
	jmp	.LBB72_37
.LBB72_38:
.Ltmp630:
	movq	%rax, %r14
.LBB72_37:                              # %_ZN11CStringBaseIcED2Ev.exit.i.i.i
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	jne	.LBB72_55
	jmp	.LBB72_56
.LBB72_39:
.Ltmp652:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE+16, (%r15)
.Ltmp653:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp654:
# BB#40:
.Ltmp659:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp660:
	jmp	.LBB72_48
.LBB72_41:
.Ltmp661:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB72_42:
.Ltmp655:
	movq	%rax, %rbx
.Ltmp656:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp657:
# BB#43:                                # %.body7
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB72_44:
.Ltmp658:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB72_45:                              # %.loopexit.split-lp
.Ltmp641:
	jmp	.LBB72_47
.LBB72_46:                              # %.loopexit
.Ltmp646:
.LBB72_47:
	movq	%rax, %r14
.Ltmp647:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp648:
.LBB72_48:                              # %_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev.exit
	movq	72(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB72_50
# BB#49:
	callq	_ZdaPv
.LBB72_50:                              # %_ZN11CStringBaseIcED2Ev.exit.i.i
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB72_52
# BB#51:
	callq	_ZdaPv
.LBB72_52:                              # %_ZN8NArchive4NCab13COtherArchiveD2Ev.exit.i
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB72_54
# BB#53:
	callq	_ZdaPv
.LBB72_54:                              # %_ZN11CStringBaseIcED2Ev.exit.i2.i
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB72_56
.LBB72_55:
	callq	_ZdaPv
.LBB72_56:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB72_57:
.Ltmp649:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end72:
	.size	_ZN8NArchive4NCab9CDatabaseC2ERKS1_, .Lfunc_end72-_ZN8NArchive4NCab9CDatabaseC2ERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table72:
.Lexception28:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\245\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Lfunc_begin28-.Lfunc_begin28 # >> Call Site 1 <<
	.long	.Ltmp628-.Lfunc_begin28 #   Call between .Lfunc_begin28 and .Ltmp628
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp628-.Lfunc_begin28 # >> Call Site 2 <<
	.long	.Ltmp629-.Ltmp628       #   Call between .Ltmp628 and .Ltmp629
	.long	.Ltmp630-.Lfunc_begin28 #     jumps to .Ltmp630
	.byte	0                       #   On action: cleanup
	.long	.Ltmp631-.Lfunc_begin28 # >> Call Site 3 <<
	.long	.Ltmp632-.Ltmp631       #   Call between .Ltmp631 and .Ltmp632
	.long	.Ltmp633-.Lfunc_begin28 #     jumps to .Ltmp633
	.byte	0                       #   On action: cleanup
	.long	.Ltmp634-.Lfunc_begin28 # >> Call Site 4 <<
	.long	.Ltmp635-.Ltmp634       #   Call between .Ltmp634 and .Ltmp635
	.long	.Ltmp636-.Lfunc_begin28 #     jumps to .Ltmp636
	.byte	0                       #   On action: cleanup
	.long	.Ltmp637-.Lfunc_begin28 # >> Call Site 5 <<
	.long	.Ltmp640-.Ltmp637       #   Call between .Ltmp637 and .Ltmp640
	.long	.Ltmp641-.Lfunc_begin28 #     jumps to .Ltmp641
	.byte	0                       #   On action: cleanup
	.long	.Ltmp642-.Lfunc_begin28 # >> Call Site 6 <<
	.long	.Ltmp645-.Ltmp642       #   Call between .Ltmp642 and .Ltmp645
	.long	.Ltmp646-.Lfunc_begin28 #     jumps to .Ltmp646
	.byte	0                       #   On action: cleanup
	.long	.Ltmp650-.Lfunc_begin28 # >> Call Site 7 <<
	.long	.Ltmp651-.Ltmp650       #   Call between .Ltmp650 and .Ltmp651
	.long	.Ltmp652-.Lfunc_begin28 #     jumps to .Ltmp652
	.byte	0                       #   On action: cleanup
	.long	.Ltmp653-.Lfunc_begin28 # >> Call Site 8 <<
	.long	.Ltmp654-.Ltmp653       #   Call between .Ltmp653 and .Ltmp654
	.long	.Ltmp655-.Lfunc_begin28 #     jumps to .Ltmp655
	.byte	1                       #   On action: 1
	.long	.Ltmp659-.Lfunc_begin28 # >> Call Site 9 <<
	.long	.Ltmp660-.Ltmp659       #   Call between .Ltmp659 and .Ltmp660
	.long	.Ltmp661-.Lfunc_begin28 #     jumps to .Ltmp661
	.byte	1                       #   On action: 1
	.long	.Ltmp656-.Lfunc_begin28 # >> Call Site 10 <<
	.long	.Ltmp657-.Ltmp656       #   Call between .Ltmp656 and .Ltmp657
	.long	.Ltmp658-.Lfunc_begin28 #     jumps to .Ltmp658
	.byte	1                       #   On action: 1
	.long	.Ltmp647-.Lfunc_begin28 # >> Call Site 11 <<
	.long	.Ltmp648-.Ltmp647       #   Call between .Ltmp647 and .Ltmp648
	.long	.Ltmp649-.Lfunc_begin28 #     jumps to .Ltmp649
	.byte	1                       #   On action: 1
	.long	.Ltmp648-.Lfunc_begin28 # >> Call Site 12 <<
	.long	.Lfunc_end72-.Ltmp648   #   Call between .Ltmp648 and .Lfunc_end72
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI73_0:
	.zero	16
	.section	.text._ZN13CObjectVectorIN8NArchive4NCab5CItemEEC2ERKS3_,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab5CItemEEC2ERKS3_,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab5CItemEEC2ERKS3_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab5CItemEEC2ERKS3_,@function
_ZN13CObjectVectorIN8NArchive4NCab5CItemEEC2ERKS3_: # @_ZN13CObjectVectorIN8NArchive4NCab5CItemEEC2ERKS3_
.Lfunc_begin29:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception29
# BB#0:
	pushq	%rbp
.Lcfi321:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi322:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi323:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi324:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi325:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi326:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi327:
	.cfi_def_cfa_offset 64
.Lcfi328:
	.cfi_offset %rbx, -56
.Lcfi329:
	.cfi_offset %r12, -48
.Lcfi330:
	.cfi_offset %r13, -40
.Lcfi331:
	.cfi_offset %r14, -32
.Lcfi332:
	.cfi_offset %r15, -24
.Lcfi333:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r15)
	movq	$8, 24(%r15)
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE+16, (%r15)
.Ltmp662:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp663:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r12d
	movl	12(%r15), %esi
	addl	%r12d, %esi
.Ltmp664:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp665:
# BB#2:                                 # %.noexc3
	testl	%r12d, %r12d
	jle	.LBB73_13
# BB#3:                                 # %.lr.ph.i.i
	xorl	%r13d, %r13d
	movq	%r12, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB73_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB73_10 Depth 2
	movq	16(%r14), %rax
	movq	(%rax,%r13,8), %rbp
.Ltmp667:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp668:
# BB#5:                                 # %.noexc5
                                        #   in Loop: Header=BB73_4 Depth=1
	movq	%r14, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	8(%rbp), %rax
	leaq	1(%rax), %r14
	testl	%r14d, %r14d
	je	.LBB73_6
# BB#7:                                 #   in Loop: Header=BB73_4 Depth=1
	cmpl	$-1, %eax
	movq	%r14, %rdi
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp669:
	callq	_Znam
.Ltmp670:
# BB#8:                                 # %.noexc.i
                                        #   in Loop: Header=BB73_4 Depth=1
	movq	%rax, (%rbx)
	movb	$0, (%rax)
	movl	%r14d, 12(%rbx)
	jmp	.LBB73_9
	.p2align	4, 0x90
.LBB73_6:                               #   in Loop: Header=BB73_4 Depth=1
	xorl	%eax, %eax
.LBB73_9:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i.i
                                        #   in Loop: Header=BB73_4 Depth=1
	movq	(%rbp), %rcx
	movq	%r12, %r14
	.p2align	4, 0x90
.LBB73_10:                              #   Parent Loop BB73_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB73_10
# BB#11:                                #   in Loop: Header=BB73_4 Depth=1
	movl	8(%rbp), %eax
	movl	%eax, 8(%rbx)
	movzwl	32(%rbp), %eax
	movw	%ax, 32(%rbx)
	movups	16(%rbp), %xmm0
	movups	%xmm0, 16(%rbx)
.Ltmp672:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp673:
	movq	(%rsp), %r12            # 8-byte Reload
# BB#12:                                # %.noexc4
                                        #   in Loop: Header=BB73_4 Depth=1
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	incq	%r13
	cmpq	%r12, %r13
	jne	.LBB73_4
.LBB73_13:                              # %_ZN13CObjectVectorIN8NArchive4NCab5CItemEEaSERKS3_.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB73_15:                              # %.loopexit.split-lp
.Ltmp666:
	jmp	.LBB73_16
.LBB73_20:
.Ltmp671:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB73_17
.LBB73_14:                              # %.loopexit
.Ltmp674:
.LBB73_16:                              # %.body
	movq	%rax, %r14
.LBB73_17:                              # %.body
.Ltmp675:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp676:
# BB#18:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB73_19:
.Ltmp677:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end73:
	.size	_ZN13CObjectVectorIN8NArchive4NCab5CItemEEC2ERKS3_, .Lfunc_end73-_ZN13CObjectVectorIN8NArchive4NCab5CItemEEC2ERKS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table73:
.Lexception29:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp662-.Lfunc_begin29 # >> Call Site 1 <<
	.long	.Ltmp665-.Ltmp662       #   Call between .Ltmp662 and .Ltmp665
	.long	.Ltmp666-.Lfunc_begin29 #     jumps to .Ltmp666
	.byte	0                       #   On action: cleanup
	.long	.Ltmp667-.Lfunc_begin29 # >> Call Site 2 <<
	.long	.Ltmp668-.Ltmp667       #   Call between .Ltmp667 and .Ltmp668
	.long	.Ltmp674-.Lfunc_begin29 #     jumps to .Ltmp674
	.byte	0                       #   On action: cleanup
	.long	.Ltmp669-.Lfunc_begin29 # >> Call Site 3 <<
	.long	.Ltmp670-.Ltmp669       #   Call between .Ltmp669 and .Ltmp670
	.long	.Ltmp671-.Lfunc_begin29 #     jumps to .Ltmp671
	.byte	0                       #   On action: cleanup
	.long	.Ltmp672-.Lfunc_begin29 # >> Call Site 4 <<
	.long	.Ltmp673-.Ltmp672       #   Call between .Ltmp672 and .Ltmp673
	.long	.Ltmp674-.Lfunc_begin29 #     jumps to .Ltmp674
	.byte	0                       #   On action: cleanup
	.long	.Ltmp675-.Lfunc_begin29 # >> Call Site 5 <<
	.long	.Ltmp676-.Ltmp675       #   Call between .Ltmp675 and .Ltmp676
	.long	.Ltmp677-.Lfunc_begin29 #     jumps to .Ltmp677
	.byte	1                       #   On action: 1
	.long	.Ltmp676-.Lfunc_begin29 # >> Call Site 6 <<
	.long	.Lfunc_end73-.Ltmp676   #   Call between .Ltmp676 and .Lfunc_end73
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIbED0Ev,"axG",@progbits,_ZN13CRecordVectorIbED0Ev,comdat
	.weak	_ZN13CRecordVectorIbED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIbED0Ev,@function
_ZN13CRecordVectorIbED0Ev:              # @_ZN13CRecordVectorIbED0Ev
.Lfunc_begin30:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception30
# BB#0:
	pushq	%r14
.Lcfi334:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi335:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi336:
	.cfi_def_cfa_offset 32
.Lcfi337:
	.cfi_offset %rbx, -24
.Lcfi338:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp678:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp679:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB74_2:
.Ltmp680:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end74:
	.size	_ZN13CRecordVectorIbED0Ev, .Lfunc_end74-_ZN13CRecordVectorIbED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table74:
.Lexception30:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp678-.Lfunc_begin30 # >> Call Site 1 <<
	.long	.Ltmp679-.Ltmp678       #   Call between .Ltmp678 and .Ltmp679
	.long	.Ltmp680-.Lfunc_begin30 #     jumps to .Ltmp680
	.byte	0                       #   On action: cleanup
	.long	.Ltmp679-.Lfunc_begin30 # >> Call Site 2 <<
	.long	.Lfunc_end74-.Ltmp679   #   Call between .Ltmp679 and .Lfunc_end74
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZN8NArchive4NCabL6kPropsE,@object # @_ZN8NArchive4NCabL6kPropsE
	.section	.rodata,"a",@progbits
	.p2align	4
_ZN8NArchive4NCabL6kPropsE:
	.quad	0
	.long	3                       # 0x3
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	7                       # 0x7
	.short	21                      # 0x15
	.zero	2
	.quad	0
	.long	12                      # 0xc
	.short	64                      # 0x40
	.zero	2
	.quad	0
	.long	9                       # 0x9
	.short	19                      # 0x13
	.zero	2
	.quad	0
	.long	22                      # 0x16
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	27                      # 0x1b
	.short	3                       # 0x3
	.zero	2
	.size	_ZN8NArchive4NCabL6kPropsE, 96

	.type	_ZN8NArchive4NCabL9kArcPropsE,@object # @_ZN8NArchive4NCabL9kArcPropsE
	.p2align	4
_ZN8NArchive4NCabL9kArcPropsE:
	.quad	0
	.long	22                      # 0x16
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	38                      # 0x26
	.short	19                      # 0x13
	.zero	2
	.quad	0
	.long	39                      # 0x27
	.short	19                      # 0x13
	.zero	2
	.size	_ZN8NArchive4NCabL9kArcPropsE, 48

	.type	_ZN8NArchive4NCabL8kMethodsE,@object # @_ZN8NArchive4NCabL8kMethodsE
	.p2align	4
_ZN8NArchive4NCabL8kMethodsE:
	.quad	.L.str
	.quad	.L.str.1
	.quad	.L.str.2
	.quad	.L.str.3
	.size	_ZN8NArchive4NCabL8kMethodsE, 32

	.type	_ZN8NArchive4NCabL14kUnknownMethodE,@object # @_ZN8NArchive4NCabL14kUnknownMethodE
	.p2align	3
_ZN8NArchive4NCabL14kUnknownMethodE:
	.quad	.L.str.4
	.size	_ZN8NArchive4NCabL14kUnknownMethodE, 8

	.type	_ZTVN8NArchive4NCab8CHandlerE,@object # @_ZTVN8NArchive4NCab8CHandlerE
	.globl	_ZTVN8NArchive4NCab8CHandlerE
	.p2align	3
_ZTVN8NArchive4NCab8CHandlerE:
	.quad	0
	.quad	_ZTIN8NArchive4NCab8CHandlerE
	.quad	_ZN8NArchive4NCab8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive4NCab8CHandler6AddRefEv
	.quad	_ZN8NArchive4NCab8CHandler7ReleaseEv
	.quad	_ZN8NArchive4NCab8CHandlerD2Ev
	.quad	_ZN8NArchive4NCab8CHandlerD0Ev
	.quad	_ZN8NArchive4NCab8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.quad	_ZN8NArchive4NCab8CHandler5CloseEv
	.quad	_ZN8NArchive4NCab8CHandler16GetNumberOfItemsEPj
	.quad	_ZN8NArchive4NCab8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.quad	_ZN8NArchive4NCab8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.quad	_ZN8NArchive4NCab8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.quad	_ZN8NArchive4NCab8CHandler21GetNumberOfPropertiesEPj
	.quad	_ZN8NArchive4NCab8CHandler15GetPropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive4NCab8CHandler28GetNumberOfArchivePropertiesEPj
	.quad	_ZN8NArchive4NCab8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.size	_ZTVN8NArchive4NCab8CHandlerE, 136

	.type	_ZTSN8NArchive4NCab8CHandlerE,@object # @_ZTSN8NArchive4NCab8CHandlerE
	.globl	_ZTSN8NArchive4NCab8CHandlerE
	.p2align	4
_ZTSN8NArchive4NCab8CHandlerE:
	.asciz	"N8NArchive4NCab8CHandlerE"
	.size	_ZTSN8NArchive4NCab8CHandlerE, 26

	.type	_ZTS10IInArchive,@object # @_ZTS10IInArchive
	.section	.rodata._ZTS10IInArchive,"aG",@progbits,_ZTS10IInArchive,comdat
	.weak	_ZTS10IInArchive
_ZTS10IInArchive:
	.asciz	"10IInArchive"
	.size	_ZTS10IInArchive, 13

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI10IInArchive,@object # @_ZTI10IInArchive
	.section	.rodata._ZTI10IInArchive,"aG",@progbits,_ZTI10IInArchive,comdat
	.weak	_ZTI10IInArchive
	.p2align	4
_ZTI10IInArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IInArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI10IInArchive, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive4NCab8CHandlerE,@object # @_ZTIN8NArchive4NCab8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive4NCab8CHandlerE
	.p2align	4
_ZTIN8NArchive4NCab8CHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive4NCab8CHandlerE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI10IInArchive
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN8NArchive4NCab8CHandlerE, 56

	.type	_ZTVN8NArchive4NCab16CFolderOutStreamE,@object # @_ZTVN8NArchive4NCab16CFolderOutStreamE
	.globl	_ZTVN8NArchive4NCab16CFolderOutStreamE
	.p2align	3
_ZTVN8NArchive4NCab16CFolderOutStreamE:
	.quad	0
	.quad	_ZTIN8NArchive4NCab16CFolderOutStreamE
	.quad	_ZN8NArchive4NCab16CFolderOutStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive4NCab16CFolderOutStream6AddRefEv
	.quad	_ZN8NArchive4NCab16CFolderOutStream7ReleaseEv
	.quad	_ZN8NArchive4NCab16CFolderOutStreamD2Ev
	.quad	_ZN8NArchive4NCab16CFolderOutStreamD0Ev
	.quad	_ZN8NArchive4NCab16CFolderOutStream5WriteEPKvjPj
	.size	_ZTVN8NArchive4NCab16CFolderOutStreamE, 64

	.type	_ZTSN8NArchive4NCab16CFolderOutStreamE,@object # @_ZTSN8NArchive4NCab16CFolderOutStreamE
	.globl	_ZTSN8NArchive4NCab16CFolderOutStreamE
	.p2align	4
_ZTSN8NArchive4NCab16CFolderOutStreamE:
	.asciz	"N8NArchive4NCab16CFolderOutStreamE"
	.size	_ZTSN8NArchive4NCab16CFolderOutStreamE, 35

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTIN8NArchive4NCab16CFolderOutStreamE,@object # @_ZTIN8NArchive4NCab16CFolderOutStreamE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive4NCab16CFolderOutStreamE
	.p2align	4
_ZTIN8NArchive4NCab16CFolderOutStreamE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive4NCab16CFolderOutStreamE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI20ISequentialOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN8NArchive4NCab16CFolderOutStreamE, 56

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"None"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"MSZip"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Quantum"
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"LZX"
	.size	.L.str.3, 4

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Unknown"
	.size	.L.str.4, 8

	.type	_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE,@object # @_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE
	.quad	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE,@object # @_ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE:
	.asciz	"13CObjectVectorIN8NArchive4NCab7CFolderEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE, 42

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE,@object # @_ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE, 24

	.type	_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE,@object # @_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NCab5CItemEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NCab5CItemEE
	.quad	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NCab5CItemEE,@object # @_ZTS13CObjectVectorIN8NArchive4NCab5CItemEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NCab5CItemEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NCab5CItemEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NCab5CItemEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NCab5CItemEE:
	.asciz	"13CObjectVectorIN8NArchive4NCab5CItemEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NCab5CItemEE, 40

	.type	_ZTI13CObjectVectorIN8NArchive4NCab5CItemEE,@object # @_ZTI13CObjectVectorIN8NArchive4NCab5CItemEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NCab5CItemEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NCab5CItemEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NCab5CItemEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NCab5CItemEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NCab5CItemEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NCab5CItemEE, 24

	.type	_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE,@object # @_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.section	.rodata._ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE,"aG",@progbits,_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE,comdat
	.weak	_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.p2align	3
_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE:
	.quad	0
	.quad	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
	.quad	_ZN9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder10CodeResumeEP20ISequentialOutStreamPKyP21ICompressProgressInfo
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy
	.quad	-8
	.quad	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy
	.quad	-16
	.quad	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv
	.quad	-24
	.quad	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy
	.quad	-32
	.quad	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD1Ev
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder9CCOMCoderD0Ev
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj
	.size	_ZTVN9NCompress8NDeflate8NDecoder9CCOMCoderE, 376

	.type	_ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE,@object # @_ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.section	.rodata._ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE,"aG",@progbits,_ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE,comdat
	.weak	_ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.p2align	4
_ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE:
	.asciz	"N9NCompress8NDeflate8NDecoder9CCOMCoderE"
	.size	_ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE, 41

	.type	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE,@object # @_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.section	.rodata._ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE,"aG",@progbits,_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE,comdat
	.weak	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.p2align	4
_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN9NCompress8NDeflate8NDecoder9CCOMCoderE
	.quad	_ZTIN9NCompress8NDeflate8NDecoder6CCoderE
	.size	_ZTIN9NCompress8NDeflate8NDecoder9CCOMCoderE, 24

	.type	_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,@object # @_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.quad	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,@object # @_ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE:
	.asciz	"13CObjectVectorIN8NArchive4NCab11CDatabaseExEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE, 47

	.type	_ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,@object # @_ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE, 24

	.type	_ZTV13CRecordVectorIhE,@object # @_ZTV13CRecordVectorIhE
	.section	.rodata._ZTV13CRecordVectorIhE,"aG",@progbits,_ZTV13CRecordVectorIhE,comdat
	.weak	_ZTV13CRecordVectorIhE
	.p2align	3
_ZTV13CRecordVectorIhE:
	.quad	0
	.quad	_ZTI13CRecordVectorIhE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIhED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIhE, 40

	.type	_ZTS13CRecordVectorIhE,@object # @_ZTS13CRecordVectorIhE
	.section	.rodata._ZTS13CRecordVectorIhE,"aG",@progbits,_ZTS13CRecordVectorIhE,comdat
	.weak	_ZTS13CRecordVectorIhE
	.p2align	4
_ZTS13CRecordVectorIhE:
	.asciz	"13CRecordVectorIhE"
	.size	_ZTS13CRecordVectorIhE, 19

	.type	_ZTI13CRecordVectorIhE,@object # @_ZTI13CRecordVectorIhE
	.section	.rodata._ZTI13CRecordVectorIhE,"aG",@progbits,_ZTI13CRecordVectorIhE,comdat
	.weak	_ZTI13CRecordVectorIhE
	.p2align	4
_ZTI13CRecordVectorIhE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIhE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIhE, 24

	.type	_ZTV13CRecordVectorIbE,@object # @_ZTV13CRecordVectorIbE
	.section	.rodata._ZTV13CRecordVectorIbE,"aG",@progbits,_ZTV13CRecordVectorIbE,comdat
	.weak	_ZTV13CRecordVectorIbE
	.p2align	3
_ZTV13CRecordVectorIbE:
	.quad	0
	.quad	_ZTI13CRecordVectorIbE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIbED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIbE, 40

	.type	_ZTS13CRecordVectorIbE,@object # @_ZTS13CRecordVectorIbE
	.section	.rodata._ZTS13CRecordVectorIbE,"aG",@progbits,_ZTS13CRecordVectorIbE,comdat
	.weak	_ZTS13CRecordVectorIbE
	.p2align	4
_ZTS13CRecordVectorIbE:
	.asciz	"13CRecordVectorIbE"
	.size	_ZTS13CRecordVectorIbE, 19

	.type	_ZTI13CRecordVectorIbE,@object # @_ZTI13CRecordVectorIbE
	.section	.rodata._ZTI13CRecordVectorIbE,"aG",@progbits,_ZTI13CRecordVectorIbE,comdat
	.weak	_ZTI13CRecordVectorIbE
	.p2align	4
_ZTI13CRecordVectorIbE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIbE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIbE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
