	.text
	.file	"7zSpecStream.bc"
	.globl	_ZN29CSequentialInStreamSizeCount24ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN29CSequentialInStreamSizeCount24ReadEPvjPj,@function
_ZN29CSequentialInStreamSizeCount24ReadEPvjPj: # @_ZN29CSequentialInStreamSizeCount24ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	4(%rsp), %rcx
	callq	*40(%rax)
	movl	4(%rsp), %ecx
	addq	%rcx, 40(%rbx)
	testq	%r14, %r14
	je	.LBB0_2
# BB#1:
	movl	%ecx, (%r14)
.LBB0_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	_ZN29CSequentialInStreamSizeCount24ReadEPvjPj, .Lfunc_end0-_ZN29CSequentialInStreamSizeCount24ReadEPvjPj
	.cfi_endproc

	.globl	_ZN29CSequentialInStreamSizeCount216GetSubStreamSizeEyPy
	.p2align	4, 0x90
	.type	_ZN29CSequentialInStreamSizeCount216GetSubStreamSizeEyPy,@function
_ZN29CSequentialInStreamSizeCount216GetSubStreamSizeEyPy: # @_ZN29CSequentialInStreamSizeCount216GetSubStreamSizeEyPy
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB1_1
# BB#2:
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.LBB1_1:
	movl	$-2147467263, %eax      # imm = 0x80004001
	retq
.Lfunc_end1:
	.size	_ZN29CSequentialInStreamSizeCount216GetSubStreamSizeEyPy, .Lfunc_end1-_ZN29CSequentialInStreamSizeCount216GetSubStreamSizeEyPy
	.cfi_endproc

	.globl	_ZThn8_N29CSequentialInStreamSizeCount216GetSubStreamSizeEyPy
	.p2align	4, 0x90
	.type	_ZThn8_N29CSequentialInStreamSizeCount216GetSubStreamSizeEyPy,@function
_ZThn8_N29CSequentialInStreamSizeCount216GetSubStreamSizeEyPy: # @_ZThn8_N29CSequentialInStreamSizeCount216GetSubStreamSizeEyPy
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB2_1
# BB#2:
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.LBB2_1:                                # %_ZN29CSequentialInStreamSizeCount216GetSubStreamSizeEyPy.exit
	movl	$-2147467263, %eax      # imm = 0x80004001
	retq
.Lfunc_end2:
	.size	_ZThn8_N29CSequentialInStreamSizeCount216GetSubStreamSizeEyPy, .Lfunc_end2-_ZThn8_N29CSequentialInStreamSizeCount216GetSubStreamSizeEyPy
	.cfi_endproc

	.section	.text._ZN29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv,@function
_ZN29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv: # @_ZN29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB3_3
# BB#1:
	movl	$IID_ICompressGetSubStreamSize, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB3_2
.LBB3_3:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB3_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB3_2:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB3_4
.Lfunc_end3:
	.size	_ZN29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv, .Lfunc_end3-_ZN29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN29CSequentialInStreamSizeCount26AddRefEv,"axG",@progbits,_ZN29CSequentialInStreamSizeCount26AddRefEv,comdat
	.weak	_ZN29CSequentialInStreamSizeCount26AddRefEv
	.p2align	4, 0x90
	.type	_ZN29CSequentialInStreamSizeCount26AddRefEv,@function
_ZN29CSequentialInStreamSizeCount26AddRefEv: # @_ZN29CSequentialInStreamSizeCount26AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end4:
	.size	_ZN29CSequentialInStreamSizeCount26AddRefEv, .Lfunc_end4-_ZN29CSequentialInStreamSizeCount26AddRefEv
	.cfi_endproc

	.section	.text._ZN29CSequentialInStreamSizeCount27ReleaseEv,"axG",@progbits,_ZN29CSequentialInStreamSizeCount27ReleaseEv,comdat
	.weak	_ZN29CSequentialInStreamSizeCount27ReleaseEv
	.p2align	4, 0x90
	.type	_ZN29CSequentialInStreamSizeCount27ReleaseEv,@function
_ZN29CSequentialInStreamSizeCount27ReleaseEv: # @_ZN29CSequentialInStreamSizeCount27ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB5_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB5_2:
	popq	%rcx
	retq
.Lfunc_end5:
	.size	_ZN29CSequentialInStreamSizeCount27ReleaseEv, .Lfunc_end5-_ZN29CSequentialInStreamSizeCount27ReleaseEv
	.cfi_endproc

	.section	.text._ZN29CSequentialInStreamSizeCount2D2Ev,"axG",@progbits,_ZN29CSequentialInStreamSizeCount2D2Ev,comdat
	.weak	_ZN29CSequentialInStreamSizeCount2D2Ev
	.p2align	4, 0x90
	.type	_ZN29CSequentialInStreamSizeCount2D2Ev,@function
_ZN29CSequentialInStreamSizeCount2D2Ev: # @_ZN29CSequentialInStreamSizeCount2D2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -24
.Lcfi16:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTV29CSequentialInStreamSizeCount2+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTV29CSequentialInStreamSizeCount2+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp0:
	callq	*16(%rax)
.Ltmp1:
.LBB6_2:                                # %_ZN9CMyComPtrI25ICompressGetSubStreamSizeED2Ev.exit
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp6:
	callq	*16(%rax)
.Ltmp7:
.LBB6_4:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB6_7:
.Ltmp8:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_5:
.Ltmp2:
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp3:
	callq	*16(%rax)
.Ltmp4:
.LBB6_8:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit6
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_9:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN29CSequentialInStreamSizeCount2D2Ev, .Lfunc_end6-_ZN29CSequentialInStreamSizeCount2D2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp7           #   Call between .Ltmp7 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end6-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN29CSequentialInStreamSizeCount2D0Ev,"axG",@progbits,_ZN29CSequentialInStreamSizeCount2D0Ev,comdat
	.weak	_ZN29CSequentialInStreamSizeCount2D0Ev
	.p2align	4, 0x90
	.type	_ZN29CSequentialInStreamSizeCount2D0Ev,@function
_ZN29CSequentialInStreamSizeCount2D0Ev: # @_ZN29CSequentialInStreamSizeCount2D0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -24
.Lcfi21:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTV29CSequentialInStreamSizeCount2+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTV29CSequentialInStreamSizeCount2+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp9:
	callq	*16(%rax)
.Ltmp10:
.LBB7_2:                                # %_ZN9CMyComPtrI25ICompressGetSubStreamSizeED2Ev.exit.i
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp15:
	callq	*16(%rax)
.Ltmp16:
.LBB7_4:                                # %_ZN29CSequentialInStreamSizeCount2D2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB7_8:
.Ltmp17:
	movq	%rax, %r14
	jmp	.LBB7_9
.LBB7_5:
.Ltmp11:
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_9
# BB#6:
	movq	(%rdi), %rax
.Ltmp12:
	callq	*16(%rax)
.Ltmp13:
.LBB7_9:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_7:
.Ltmp14:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN29CSequentialInStreamSizeCount2D0Ev, .Lfunc_end7-_ZN29CSequentialInStreamSizeCount2D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	1                       #   On action: 1
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end7-.Ltmp13     #   Call between .Ltmp13 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZThn8_N29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv: # @_ZThn8_N29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB8_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB8_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB8_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB8_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB8_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB8_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB8_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB8_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB8_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB8_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB8_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB8_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB8_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB8_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB8_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB8_32
.LBB8_16:                               # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressGetSubStreamSize(%rip), %cl
	jne	.LBB8_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+1(%rip), %cl
	jne	.LBB8_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+2(%rip), %cl
	jne	.LBB8_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+3(%rip), %cl
	jne	.LBB8_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+4(%rip), %cl
	jne	.LBB8_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+5(%rip), %cl
	jne	.LBB8_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+6(%rip), %cl
	jne	.LBB8_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+7(%rip), %cl
	jne	.LBB8_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+8(%rip), %cl
	jne	.LBB8_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+9(%rip), %cl
	jne	.LBB8_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+10(%rip), %cl
	jne	.LBB8_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+11(%rip), %cl
	jne	.LBB8_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+12(%rip), %cl
	jne	.LBB8_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+13(%rip), %cl
	jne	.LBB8_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+14(%rip), %cl
	jne	.LBB8_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+15(%rip), %cl
	jne	.LBB8_33
.LBB8_32:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB8_33:                               # %_ZN29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end8:
	.size	_ZThn8_N29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv, .Lfunc_end8-_ZThn8_N29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N29CSequentialInStreamSizeCount26AddRefEv,"axG",@progbits,_ZThn8_N29CSequentialInStreamSizeCount26AddRefEv,comdat
	.weak	_ZThn8_N29CSequentialInStreamSizeCount26AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N29CSequentialInStreamSizeCount26AddRefEv,@function
_ZThn8_N29CSequentialInStreamSizeCount26AddRefEv: # @_ZThn8_N29CSequentialInStreamSizeCount26AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end9:
	.size	_ZThn8_N29CSequentialInStreamSizeCount26AddRefEv, .Lfunc_end9-_ZThn8_N29CSequentialInStreamSizeCount26AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N29CSequentialInStreamSizeCount27ReleaseEv,"axG",@progbits,_ZThn8_N29CSequentialInStreamSizeCount27ReleaseEv,comdat
	.weak	_ZThn8_N29CSequentialInStreamSizeCount27ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N29CSequentialInStreamSizeCount27ReleaseEv,@function
_ZThn8_N29CSequentialInStreamSizeCount27ReleaseEv: # @_ZThn8_N29CSequentialInStreamSizeCount27ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB10_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB10_2:                               # %_ZN29CSequentialInStreamSizeCount27ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end10:
	.size	_ZThn8_N29CSequentialInStreamSizeCount27ReleaseEv, .Lfunc_end10-_ZThn8_N29CSequentialInStreamSizeCount27ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N29CSequentialInStreamSizeCount2D1Ev,"axG",@progbits,_ZThn8_N29CSequentialInStreamSizeCount2D1Ev,comdat
	.weak	_ZThn8_N29CSequentialInStreamSizeCount2D1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N29CSequentialInStreamSizeCount2D1Ev,@function
_ZThn8_N29CSequentialInStreamSizeCount2D1Ev: # @_ZThn8_N29CSequentialInStreamSizeCount2D1Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -24
.Lcfi28:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTV29CSequentialInStreamSizeCount2+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTV29CSequentialInStreamSizeCount2+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp18:
	callq	*16(%rax)
.Ltmp19:
.LBB11_2:                               # %_ZN9CMyComPtrI25ICompressGetSubStreamSizeED2Ev.exit.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp24:
	callq	*16(%rax)
.Ltmp25:
.LBB11_4:                               # %_ZN29CSequentialInStreamSizeCount2D2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB11_7:
.Ltmp26:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB11_5:
.Ltmp20:
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp21:
	callq	*16(%rax)
.Ltmp22:
.LBB11_8:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit6.i
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB11_9:
.Ltmp23:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZThn8_N29CSequentialInStreamSizeCount2D1Ev, .Lfunc_end11-_ZThn8_N29CSequentialInStreamSizeCount2D1Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin2   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp21-.Ltmp25         #   Call between .Ltmp25 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin2   #     jumps to .Ltmp23
	.byte	1                       #   On action: 1
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Lfunc_end11-.Ltmp22    #   Call between .Ltmp22 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZThn8_N29CSequentialInStreamSizeCount2D0Ev,"axG",@progbits,_ZThn8_N29CSequentialInStreamSizeCount2D0Ev,comdat
	.weak	_ZThn8_N29CSequentialInStreamSizeCount2D0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N29CSequentialInStreamSizeCount2D0Ev,@function
_ZThn8_N29CSequentialInStreamSizeCount2D0Ev: # @_ZThn8_N29CSequentialInStreamSizeCount2D0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -24
.Lcfi33:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	movl	$_ZTV29CSequentialInStreamSizeCount2+88, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTV29CSequentialInStreamSizeCount2+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rax)
	movq	24(%rax), %rdi
	leaq	-8(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB12_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp27:
	callq	*16(%rax)
.Ltmp28:
.LBB12_2:                               # %_ZN9CMyComPtrI25ICompressGetSubStreamSizeED2Ev.exit.i.i
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp33:
	callq	*16(%rax)
.Ltmp34:
.LBB12_4:                               # %_ZN29CSequentialInStreamSizeCount2D0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB12_8:
.Ltmp35:
	movq	%rax, %r14
	jmp	.LBB12_9
.LBB12_5:
.Ltmp29:
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_9
# BB#6:
	movq	(%rdi), %rax
.Ltmp30:
	callq	*16(%rax)
.Ltmp31:
.LBB12_9:                               # %.body.i
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_7:
.Ltmp32:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZThn8_N29CSequentialInStreamSizeCount2D0Ev, .Lfunc_end12-_ZThn8_N29CSequentialInStreamSizeCount2D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp27-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin3   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin3   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin3   #     jumps to .Ltmp32
	.byte	1                       #   On action: 1
	.long	.Ltmp31-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end12-.Ltmp31    #   Call between .Ltmp31 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB13_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB13_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB13_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB13_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB13_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB13_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB13_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB13_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB13_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB13_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB13_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB13_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB13_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB13_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB13_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB13_16:
	xorl	%eax, %eax
	retq
.Lfunc_end13:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end13-_ZeqRK4GUIDS1_
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end14:
	.size	__clang_call_terminate, .Lfunc_end14-__clang_call_terminate

	.type	_ZTV29CSequentialInStreamSizeCount2,@object # @_ZTV29CSequentialInStreamSizeCount2
	.section	.rodata,"a",@progbits
	.globl	_ZTV29CSequentialInStreamSizeCount2
	.p2align	3
_ZTV29CSequentialInStreamSizeCount2:
	.quad	0
	.quad	_ZTI29CSequentialInStreamSizeCount2
	.quad	_ZN29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv
	.quad	_ZN29CSequentialInStreamSizeCount26AddRefEv
	.quad	_ZN29CSequentialInStreamSizeCount27ReleaseEv
	.quad	_ZN29CSequentialInStreamSizeCount2D2Ev
	.quad	_ZN29CSequentialInStreamSizeCount2D0Ev
	.quad	_ZN29CSequentialInStreamSizeCount24ReadEPvjPj
	.quad	_ZN29CSequentialInStreamSizeCount216GetSubStreamSizeEyPy
	.quad	-8
	.quad	_ZTI29CSequentialInStreamSizeCount2
	.quad	_ZThn8_N29CSequentialInStreamSizeCount214QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N29CSequentialInStreamSizeCount26AddRefEv
	.quad	_ZThn8_N29CSequentialInStreamSizeCount27ReleaseEv
	.quad	_ZThn8_N29CSequentialInStreamSizeCount2D1Ev
	.quad	_ZThn8_N29CSequentialInStreamSizeCount2D0Ev
	.quad	_ZThn8_N29CSequentialInStreamSizeCount216GetSubStreamSizeEyPy
	.size	_ZTV29CSequentialInStreamSizeCount2, 136

	.type	_ZTS29CSequentialInStreamSizeCount2,@object # @_ZTS29CSequentialInStreamSizeCount2
	.globl	_ZTS29CSequentialInStreamSizeCount2
	.p2align	4
_ZTS29CSequentialInStreamSizeCount2:
	.asciz	"29CSequentialInStreamSizeCount2"
	.size	_ZTS29CSequentialInStreamSizeCount2, 32

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTS25ICompressGetSubStreamSize,@object # @_ZTS25ICompressGetSubStreamSize
	.section	.rodata._ZTS25ICompressGetSubStreamSize,"aG",@progbits,_ZTS25ICompressGetSubStreamSize,comdat
	.weak	_ZTS25ICompressGetSubStreamSize
	.p2align	4
_ZTS25ICompressGetSubStreamSize:
	.asciz	"25ICompressGetSubStreamSize"
	.size	_ZTS25ICompressGetSubStreamSize, 28

	.type	_ZTI25ICompressGetSubStreamSize,@object # @_ZTI25ICompressGetSubStreamSize
	.section	.rodata._ZTI25ICompressGetSubStreamSize,"aG",@progbits,_ZTI25ICompressGetSubStreamSize,comdat
	.weak	_ZTI25ICompressGetSubStreamSize
	.p2align	4
_ZTI25ICompressGetSubStreamSize:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS25ICompressGetSubStreamSize
	.quad	_ZTI8IUnknown
	.size	_ZTI25ICompressGetSubStreamSize, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI29CSequentialInStreamSizeCount2,@object # @_ZTI29CSequentialInStreamSizeCount2
	.section	.rodata,"a",@progbits
	.globl	_ZTI29CSequentialInStreamSizeCount2
	.p2align	4
_ZTI29CSequentialInStreamSizeCount2:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS29CSequentialInStreamSizeCount2
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTI19ISequentialInStream
	.quad	2                       # 0x2
	.quad	_ZTI25ICompressGetSubStreamSize
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTI29CSequentialInStreamSizeCount2, 72


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
