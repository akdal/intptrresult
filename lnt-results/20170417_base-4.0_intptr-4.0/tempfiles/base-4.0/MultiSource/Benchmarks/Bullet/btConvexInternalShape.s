	.text
	.file	"btConvexInternalShape.bc"
	.globl	_ZN21btConvexInternalShapeC2Ev
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShapeC2Ev,@function
_ZN21btConvexInternalShapeC2Ev:         # @_ZN21btConvexInternalShapeC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN13btConvexShapeC2Ev
	movq	$_ZTV21btConvexInternalShape+16, (%rbx)
	movabsq	$4575657222473777152, %rax # imm = 0x3F8000003F800000
	movq	%rax, 24(%rbx)
	movq	$1065353216, 32(%rbx)   # imm = 0x3F800000
	movl	$1025758986, 56(%rbx)   # imm = 0x3D23D70A
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN21btConvexInternalShapeC2Ev, .Lfunc_end0-_ZN21btConvexInternalShapeC2Ev
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3,@function
_ZN21btConvexInternalShape15setLocalScalingERK9btVector3: # @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movaps	.LCPI2_0(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	andps	%xmm1, %xmm0
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm0, 24(%rdi)
	movlps	%xmm1, 32(%rdi)
	retq
.Lfunc_end2:
	.size	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3, .Lfunc_end2-_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.zero	16
	.text
	.globl	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_,@function
_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_: # @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 128
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	(%r12), %rax
	callq	*88(%rax)
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	xorl	%r13d, %r13d
	leaq	56(%rsp), %rbp
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$1065353216, 16(%rsp,%r13,4) # imm = 0x3F800000
	movq	(%r12), %rax
	movq	96(%rax), %rax
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movsd	(%rbx), %xmm3           # xmm3 = mem[0],zero
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	movsd	16(%rbx), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm1, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm4, %xmm5
	movsd	32(%rbx), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm5, %xmm4
	mulss	8(%rbx), %xmm0
	mulss	24(%rbx), %xmm1
	addss	%xmm0, %xmm1
	mulss	40(%rbx), %xmm2
	addss	%xmm1, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm4, 40(%rsp)
	movlps	%xmm0, 48(%rsp)
	movq	%r12, %rdi
	leaq	40(%rsp), %rsi
	callq	*%rax
	movaps	%xmm0, %xmm2
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm2, %xmm5
	movss	20(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0],xmm6[1],xmm2[1]
	mulps	%xmm0, %xmm6
	addps	%xmm5, %xmm6
	movss	40(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	24(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm1, %xmm5
	addps	%xmm6, %xmm5
	movsd	48(%rbx), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm5, %xmm1
	movss	36(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm3, %xmm4
	addss	%xmm4, %xmm2
	addss	56(%rbx), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm1, 40(%rsp)
	movlps	%xmm0, 48(%rsp)
	movss	40(%rsp,%r13,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	12(%rsp), %xmm0         # 4-byte Folded Reload
	movss	%xmm0, (%r14,%r13,4)
	movl	$-1082130432, 16(%rsp,%r13,4) # imm = 0xBF800000
	movq	(%r12), %rax
	movq	96(%rax), %rax
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movsd	(%rbx), %xmm3           # xmm3 = mem[0],zero
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	movsd	16(%rbx), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm1, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm4, %xmm5
	movsd	32(%rbx), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm5, %xmm4
	mulss	8(%rbx), %xmm0
	mulss	24(%rbx), %xmm1
	addss	%xmm0, %xmm1
	mulss	40(%rbx), %xmm2
	addss	%xmm1, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm4, 56(%rsp)
	movlps	%xmm0, 64(%rsp)
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	*%rax
	movaps	%xmm0, %xmm2
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	movss	20(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm0, %xmm6
	addps	%xmm2, %xmm6
	movss	40(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	24(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm1, %xmm5
	addps	%xmm6, %xmm5
	movsd	48(%rbx), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm5, %xmm1
	movss	36(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm3, %xmm4
	addss	%xmm4, %xmm2
	addss	56(%rbx), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm1, 40(%rsp)
	movlps	%xmm0, 48(%rsp)
	movss	40(%rsp,%r13,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	subss	12(%rsp), %xmm0         # 4-byte Folded Reload
	movss	%xmm0, (%r15,%r13,4)
	incq	%r13
	cmpq	$3, %r13
	jne	.LBB3_1
# BB#2:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_, .Lfunc_end3-_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	679477248               # float 1.42108547E-14
.LCPI4_2:
	.long	3212836864              # float -1
.LCPI4_3:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_1:
	.long	3212836864              # float -1
	.long	3212836864              # float -1
	.zero	4
	.zero	4
	.text
	.globl	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3,@function
_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3: # @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 96
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*104(%rax)
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB4_2
	jnp	.LBB4_1
.LBB4_2:
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	movss	.LCPI4_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	xorl	%eax, %eax
	ucomiss	%xmm1, %xmm2
	seta	%al
	movd	%eax, %xmm1
	pshufd	$80, %xmm1, %xmm2       # xmm2 = xmm1[0,0,1,1]
	pslld	$31, %xmm2
	psrad	$31, %xmm2
	movdqa	%xmm2, %xmm1
	pandn	%xmm0, %xmm1
	pand	.LCPI4_1(%rip), %xmm2
	por	%xmm1, %xmm2
	pshufd	$229, %xmm2, %xmm0      # xmm0 = xmm2[1,1,2,3]
	movdqa	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	jbe	.LBB4_4
# BB#3:
	movss	.LCPI4_2(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
.LBB4_4:
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB4_6
# BB#5:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movdqa	%xmm2, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movdqa	(%rsp), %xmm2           # 16-byte Reload
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB4_6:                                # %.split
	movss	.LCPI4_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	mulss	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)           # 16-byte Spill
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	movaps	(%rsp), %xmm2           # 16-byte Reload
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	16(%rsp), %xmm0         # 16-byte Folded Reload
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	addps	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	addss	%xmm2, %xmm1
	jmp	.LBB4_7
.LBB4_1:
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movaps	32(%rsp), %xmm1         # 16-byte Reload
.LBB4_7:
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3, .Lfunc_end4-_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.cfi_endproc

	.globl	_ZN32btConvexInternalAabbCachingShapeC2Ev
	.p2align	4, 0x90
	.type	_ZN32btConvexInternalAabbCachingShapeC2Ev,@function
_ZN32btConvexInternalAabbCachingShapeC2Ev: # @_ZN32btConvexInternalAabbCachingShapeC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 16
.Lcfi21:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN13btConvexShapeC2Ev
	movl	$1065353216, 24(%rbx)   # imm = 0x3F800000
	movl	$1065353216, 28(%rbx)   # imm = 0x3F800000
	movl	$1065353216, 32(%rbx)   # imm = 0x3F800000
	movl	$0, 36(%rbx)
	movl	$1025758986, 56(%rbx)   # imm = 0x3D23D70A
	movq	$_ZTV32btConvexInternalAabbCachingShape+16, (%rbx)
	movl	$1065353216, 64(%rbx)   # imm = 0x3F800000
	movl	$1065353216, 68(%rbx)   # imm = 0x3F800000
	movl	$1065353216, 72(%rbx)   # imm = 0x3F800000
	movl	$0, 76(%rbx)
	movl	$-1082130432, 80(%rbx)  # imm = 0xBF800000
	movl	$-1082130432, 84(%rbx)  # imm = 0xBF800000
	movl	$-1082130432, 88(%rbx)  # imm = 0xBF800000
	movl	$0, 92(%rbx)
	movb	$0, 96(%rbx)
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_ZN32btConvexInternalAabbCachingShapeC2Ev, .Lfunc_end5-_ZN32btConvexInternalAabbCachingShapeC2Ev
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	1056964608              # float 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZNK32btConvexInternalAabbCachingShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK32btConvexInternalAabbCachingShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK32btConvexInternalAabbCachingShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK32btConvexInternalAabbCachingShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -40
.Lcfi28:
	.cfi_offset %r12, -32
.Lcfi29:
	.cfi_offset %r14, -24
.Lcfi30:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	movss	80(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm11
	subss	%xmm2, %xmm11
	movss	84(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm12
	subss	%xmm8, %xmm12
	movss	88(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm13
	subss	%xmm7, %xmm13
	movss	.LCPI6_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm11
	mulss	%xmm3, %xmm12
	mulss	%xmm3, %xmm13
	addss	%xmm0, %xmm11
	addss	%xmm0, %xmm12
	addss	%xmm0, %xmm13
	addss	%xmm2, %xmm5
	addss	%xmm8, %xmm6
	addss	%xmm7, %xmm4
	mulss	%xmm3, %xmm5
	mulss	%xmm3, %xmm6
	mulss	%xmm3, %xmm4
	movss	16(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%r12), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm9          # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm8    # xmm8 = xmm8[0],xmm0[0],xmm8[1],xmm0[1]
	movaps	.LCPI6_1(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	movaps	%xmm5, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm8, %xmm2
	andps	%xmm0, %xmm8
	movss	20(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm9    # xmm9 = xmm9[0],xmm3[0],xmm9[1],xmm3[1]
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm9, %xmm3
	andps	%xmm0, %xmm9
	movss	24(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm10         # xmm10 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm10   # xmm10 = xmm10[0],xmm7[0],xmm10[1],xmm7[1]
	movaps	%xmm4, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm10, %xmm1
	andps	%xmm0, %xmm10
	addps	%xmm2, %xmm3
	movss	32(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	andps	%xmm0, %xmm2
	addps	%xmm3, %xmm1
	movsd	48(%r12), %xmm7         # xmm7 = mem[0],zero
	addps	%xmm1, %xmm7
	movss	36(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	andps	%xmm0, %xmm3
	addss	%xmm5, %xmm6
	movss	40(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm0
	mulss	%xmm1, %xmm4
	addss	%xmm6, %xmm4
	mulss	%xmm11, %xmm2
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm8, %xmm11
	mulss	%xmm12, %xmm3
	shufps	$224, %xmm12, %xmm12    # xmm12 = xmm12[0,0,2,3]
	mulps	%xmm9, %xmm12
	addps	%xmm11, %xmm12
	mulss	%xmm13, %xmm0
	shufps	$224, %xmm13, %xmm13    # xmm13 = xmm13[0,0,2,3]
	mulps	%xmm10, %xmm13
	addps	%xmm12, %xmm13
	addss	56(%r12), %xmm4
	addss	%xmm2, %xmm3
	addss	%xmm3, %xmm0
	movaps	%xmm4, %xmm1
	subss	%xmm0, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movaps	%xmm7, %xmm1
	subps	%xmm13, %xmm1
	movlps	%xmm1, (%r15)
	movlps	%xmm2, 8(%r15)
	addps	%xmm7, %xmm13
	addss	%xmm4, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm13, (%r14)
	movlps	%xmm1, 8(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	_ZNK32btConvexInternalAabbCachingShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end6-_ZNK32btConvexInternalAabbCachingShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZN32btConvexInternalAabbCachingShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN32btConvexInternalAabbCachingShape15setLocalScalingERK9btVector3,@function
_ZN32btConvexInternalAabbCachingShape15setLocalScalingERK9btVector3: # @_ZN32btConvexInternalAabbCachingShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movaps	.LCPI7_0(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	andps	%xmm1, %xmm0
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm0, 24(%rdi)
	movlps	%xmm1, 32(%rdi)
	jmp	_ZN32btConvexInternalAabbCachingShape15recalcLocalAabbEv # TAILCALL
.Lfunc_end7:
	.size	_ZN32btConvexInternalAabbCachingShape15setLocalScalingERK9btVector3, .Lfunc_end7-_ZN32btConvexInternalAabbCachingShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.globl	_ZN32btConvexInternalAabbCachingShape15recalcLocalAabbEv
	.p2align	4, 0x90
	.type	_ZN32btConvexInternalAabbCachingShape15recalcLocalAabbEv,@function
_ZN32btConvexInternalAabbCachingShape15recalcLocalAabbEv: # @_ZN32btConvexInternalAabbCachingShape15recalcLocalAabbEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 16
	subq	$96, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 112
.Lcfi33:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	$1, 96(%rbx)
	movb	_ZGVZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions(%rip), %al
	testb	%al, %al
	jne	.LBB8_3
# BB#1:
	movl	$_ZGVZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB8_3
# BB#2:
	movl	$1065353216, _ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions(%rip) # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions+4(%rip)
	movl	$1065353216, _ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions+20(%rip) # imm = 0x3F800000
	movups	%xmm0, _ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions+24(%rip)
	movl	$1065353216, _ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions+40(%rip) # imm = 0x3F800000
	movl	$0, _ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions+44(%rip)
	movl	$-1082130432, _ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions+48(%rip) # imm = 0xBF800000
	movups	%xmm0, _ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions+52(%rip)
	movl	$-1082130432, _ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions+68(%rip) # imm = 0xBF800000
	movups	%xmm0, _ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions+72(%rip)
	movl	$3212836864, %eax       # imm = 0xBF800000
	movq	%rax, _ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions+88(%rip)
	movl	$_ZGVZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions, %edi
	callq	__cxa_guard_release
.LBB8_3:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movq	(%rbx), %rax
	movq	%rsp, %rdx
	movl	$_ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions, %esi
	movl	$6, %ecx
	movq	%rbx, %rdi
	callq	*112(%rax)
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%rsp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 80(%rbx)
	movss	48(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 64(%rbx)
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 84(%rbx)
	movss	68(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 68(%rbx)
	movss	40(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 88(%rbx)
	movss	88(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 72(%rbx)
	addq	$96, %rsp
	popq	%rbx
	retq
.Lfunc_end8:
	.size	_ZN32btConvexInternalAabbCachingShape15recalcLocalAabbEv, .Lfunc_end8-_ZN32btConvexInternalAabbCachingShape15recalcLocalAabbEv
	.cfi_endproc

	.section	.text._ZN21btConvexInternalShapeD0Ev,"axG",@progbits,_ZN21btConvexInternalShapeD0Ev,comdat
	.weak	_ZN21btConvexInternalShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShapeD0Ev,@function
_ZN21btConvexInternalShapeD0Ev:         # @_ZN21btConvexInternalShapeD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 32
.Lcfi37:
	.cfi_offset %rbx, -24
.Lcfi38:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp1:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB9_2:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB9_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN21btConvexInternalShapeD0Ev, .Lfunc_end9-_ZN21btConvexInternalShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end9-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_,"axG",@progbits,_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_,comdat
	.weak	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	120(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end10:
	.size	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end10-_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end11:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end11-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZN21btConvexInternalShape9setMarginEf,"axG",@progbits,_ZN21btConvexInternalShape9setMarginEf,comdat
	.weak	_ZN21btConvexInternalShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShape9setMarginEf,@function
_ZN21btConvexInternalShape9setMarginEf: # @_ZN21btConvexInternalShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 56(%rdi)
	retq
.Lfunc_end12:
	.size	_ZN21btConvexInternalShape9setMarginEf, .Lfunc_end12-_ZN21btConvexInternalShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape9getMarginEv,"axG",@progbits,_ZNK21btConvexInternalShape9getMarginEv,comdat
	.weak	_ZNK21btConvexInternalShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape9getMarginEv,@function
_ZNK21btConvexInternalShape9getMarginEv: # @_ZNK21btConvexInternalShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end13:
	.size	_ZNK21btConvexInternalShape9getMarginEv, .Lfunc_end13-_ZNK21btConvexInternalShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end14:
	.size	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end14-_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end15:
	.size	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end15-_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text._ZN32btConvexInternalAabbCachingShapeD0Ev,"axG",@progbits,_ZN32btConvexInternalAabbCachingShapeD0Ev,comdat
	.weak	_ZN32btConvexInternalAabbCachingShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN32btConvexInternalAabbCachingShapeD0Ev,@function
_ZN32btConvexInternalAabbCachingShapeD0Ev: # @_ZN32btConvexInternalAabbCachingShapeD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 32
.Lcfi42:
	.cfi_offset %rbx, -24
.Lcfi43:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp6:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp7:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB16_2:
.Ltmp8:
	movq	%rax, %r14
.Ltmp9:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp10:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB16_4:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN32btConvexInternalAabbCachingShapeD0Ev, .Lfunc_end16-_ZN32btConvexInternalAabbCachingShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end16-.Ltmp10    #   Call between .Ltmp10 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.type	_ZTV21btConvexInternalShape,@object # @_ZTV21btConvexInternalShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV21btConvexInternalShape
	.p2align	3
_ZTV21btConvexInternalShape:
	.quad	0
	.quad	_ZTI21btConvexInternalShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN21btConvexInternalShapeD0Ev
	.quad	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.size	_ZTV21btConvexInternalShape, 160

	.type	_ZTV32btConvexInternalAabbCachingShape,@object # @_ZTV32btConvexInternalAabbCachingShape
	.globl	_ZTV32btConvexInternalAabbCachingShape
	.p2align	3
_ZTV32btConvexInternalAabbCachingShape:
	.quad	0
	.quad	_ZTI32btConvexInternalAabbCachingShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN32btConvexInternalAabbCachingShapeD0Ev
	.quad	_ZNK32btConvexInternalAabbCachingShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN32btConvexInternalAabbCachingShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.size	_ZTV32btConvexInternalAabbCachingShape, 160

	.type	_ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions,@object # @_ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions
	.local	_ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions
	.comm	_ZZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions,96,16
	.type	_ZGVZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions,@object # @_ZGVZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions
	.local	_ZGVZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions
	.comm	_ZGVZN32btConvexInternalAabbCachingShape15recalcLocalAabbEvE11_directions,8,8
	.type	_ZTS21btConvexInternalShape,@object # @_ZTS21btConvexInternalShape
	.globl	_ZTS21btConvexInternalShape
	.p2align	4
_ZTS21btConvexInternalShape:
	.asciz	"21btConvexInternalShape"
	.size	_ZTS21btConvexInternalShape, 24

	.type	_ZTI21btConvexInternalShape,@object # @_ZTI21btConvexInternalShape
	.globl	_ZTI21btConvexInternalShape
	.p2align	4
_ZTI21btConvexInternalShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21btConvexInternalShape
	.quad	_ZTI13btConvexShape
	.size	_ZTI21btConvexInternalShape, 24

	.type	_ZTS32btConvexInternalAabbCachingShape,@object # @_ZTS32btConvexInternalAabbCachingShape
	.globl	_ZTS32btConvexInternalAabbCachingShape
	.p2align	4
_ZTS32btConvexInternalAabbCachingShape:
	.asciz	"32btConvexInternalAabbCachingShape"
	.size	_ZTS32btConvexInternalAabbCachingShape, 35

	.type	_ZTI32btConvexInternalAabbCachingShape,@object # @_ZTI32btConvexInternalAabbCachingShape
	.globl	_ZTI32btConvexInternalAabbCachingShape
	.p2align	4
_ZTI32btConvexInternalAabbCachingShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS32btConvexInternalAabbCachingShape
	.quad	_ZTI21btConvexInternalShape
	.size	_ZTI32btConvexInternalAabbCachingShape, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
