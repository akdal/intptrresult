	.text
	.file	"CoderMixer2.bc"
	.globl	_ZN11NCoderMixer21CBindReverseConverterC2ERKNS_9CBindInfoE
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer21CBindReverseConverterC2ERKNS_9CBindInfoE,@function
_ZN11NCoderMixer21CBindReverseConverterC2ERKNS_9CBindInfoE: # @_ZN11NCoderMixer21CBindReverseConverterC2ERKNS_9CBindInfoE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	leaq	8(%r13), %r14
	movq	%r14, %rdi
	callq	_ZN11NCoderMixer9CBindInfoC2ERKS0_
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%r13)
	movq	$4, 160(%r13)
	movq	$_ZTV13CRecordVectorIjE+16, 136(%r13)
	movups	%xmm0, 176(%r13)
	movq	$4, 192(%r13)
	movq	$_ZTV13CRecordVectorIjE+16, 168(%r13)
	movups	%xmm0, 208(%r13)
	movq	$4, 224(%r13)
	movq	$_ZTV13CRecordVectorIjE+16, 200(%r13)
	movups	%xmm0, 248(%r13)
	movq	$4, 264(%r13)
	movq	$_ZTV13CRecordVectorIjE+16, 240(%r13)
	movl	$0, 232(%r13)
	movl	$0, (%r13)
	cmpl	$0, 12(%r12)
	jle	.LBB0_46
# BB#1:                                 # %.lr.ph.i
	leaq	136(%r13), %r15
	leaq	168(%r13), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	200(%r13), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	240(%r13), %rbx
	movq	16(%r12), %rcx
	xorl	%eax, %eax
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	addl	(%rcx,%rsi,8), %edx
	movl	%edx, 232(%r13)
	addl	4(%rcx,%rsi,8), %eax
	movl	%eax, (%r13)
	incq	%rsi
	movslq	12(%r12), %rdi
	cmpq	%rdi, %rsi
	jl	.LBB0_2
# BB#3:                                 # %_ZNK11NCoderMixer9CBindInfo13GetNumStreamsERjS1_.exit.preheader
	testl	%edx, %edx
	je	.LBB0_9
# BB#4:                                 # %.lr.ph93
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
.Ltmp0:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp1:
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=1
	movq	152(%r13), %rax
	movslq	148(%r13), %rcx
	movl	$0, (%rax,%rcx,4)
	incl	148(%r13)
.Ltmp2:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp3:
# BB#7:                                 # %_ZNK11NCoderMixer9CBindInfo13GetNumStreamsERjS1_.exit
                                        #   in Loop: Header=BB0_5 Depth=1
	movq	256(%r13), %rax
	movslq	252(%r13), %rcx
	movl	$0, (%rax,%rcx,4)
	incl	252(%r13)
	incl	%ebp
	cmpl	232(%r13), %ebp
	jb	.LBB0_5
# BB#8:                                 # %.preheader72.loopexit
	movl	(%r13), %eax
.LBB0_9:                                # %.preheader72
	testl	%eax, %eax
	je	.LBB0_10
# BB#11:                                # %.lr.ph89
	movq	%rbx, %rbp
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	xorl	%r15d, %r15d
	movq	24(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_12:                               # =>This Inner Loop Header: Depth=1
.Ltmp5:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp6:
# BB#13:                                #   in Loop: Header=BB0_12 Depth=1
	movq	184(%r13), %rax
	movslq	180(%r13), %rcx
	movl	$0, (%rax,%rcx,4)
	incl	180(%r13)
.Ltmp7:
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp8:
# BB#14:                                #   in Loop: Header=BB0_12 Depth=1
	movq	216(%r13), %rax
	movslq	212(%r13), %rcx
	movl	$0, (%rax,%rcx,4)
	incl	212(%r13)
	incl	%r15d
	movl	(%r13), %r14d
	cmpl	%r14d, %r15d
	jb	.LBB0_12
	jmp	.LBB0_15
.LBB0_10:
	xorl	%r14d, %r14d
.LBB0_15:                               # %._crit_edge90
	movslq	12(%r12), %rcx
	testq	%rcx, %rcx
	jle	.LBB0_46
# BB#16:                                # %.lr.ph87
	movl	232(%r13), %r10d
	movq	16(%r12), %rsi
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB0_19:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_26 Depth 2
                                        #     Child Loop BB0_24 Depth 2
	movl	-8(%rsi,%rcx,8), %edx
	subl	%edx, %r10d
	movl	-4(%rsi,%rcx,8), %r8d
	testl	%edx, %edx
	je	.LBB0_20
# BB#25:                                # %.lr.ph
                                        #   in Loop: Header=BB0_19 Depth=1
	movq	152(%r13), %rdi
	movq	256(%r13), %rdx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_26:                               #   Parent Loop BB0_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r9,%rbx), %ebp
	leal	(%r10,%rbx), %eax
	cltq
	movl	%ebp, (%rdi,%rax,4)
	movslq	%ebp, %rbp
	movl	%eax, (%rdx,%rbp,4)
	incl	%ebx
	cmpl	-8(%rsi,%rcx,8), %ebx
	jb	.LBB0_26
# BB#21:                                # %.preheader.loopexit
                                        #   in Loop: Header=BB0_19 Depth=1
	movl	-4(%rsi,%rcx,8), %edx
	addl	%ebx, %r9d
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_20:                               #   in Loop: Header=BB0_19 Depth=1
	movl	%r8d, %edx
.LBB0_22:                               # %.preheader
                                        #   in Loop: Header=BB0_19 Depth=1
	subl	%r8d, %r14d
	testl	%edx, %edx
	je	.LBB0_18
# BB#23:                                # %.lr.ph79
                                        #   in Loop: Header=BB0_19 Depth=1
	movq	184(%r13), %rdi
	movq	216(%r13), %rdx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_24:                               #   Parent Loop BB0_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r11,%rbx), %eax
	leal	(%r14,%rbx), %ebp
	movslq	%ebp, %rbp
	movl	%eax, (%rdi,%rbp,4)
	cltq
	movl	%ebp, (%rdx,%rax,4)
	incl	%ebx
	cmpl	-4(%rsi,%rcx,8), %ebx
	jb	.LBB0_24
# BB#17:                                # %.loopexit.loopexit
                                        #   in Loop: Header=BB0_19 Depth=1
	addl	%ebx, %r11d
.LBB0_18:                               # %.loopexit
                                        #   in Loop: Header=BB0_19 Depth=1
	cmpq	$1, %rcx
	leaq	-1(%rcx), %rcx
	jg	.LBB0_19
.LBB0_46:                               # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_27:                               # %.loopexit73
.Ltmp9:
	jmp	.LBB0_29
.LBB0_28:                               # %.loopexit.split-lp
.Ltmp4:
	movq	%rbx, %rbp
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
.LBB0_29:
	movq	%rax, %r12
.Ltmp10:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp11:
# BB#30:
.Ltmp12:
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp13:
# BB#31:
.Ltmp14:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp15:
# BB#32:
.Ltmp16:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp17:
# BB#33:
	leaq	104(%r13), %rdi
.Ltmp18:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp19:
# BB#34:
	leaq	72(%r13), %rdi
.Ltmp23:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp24:
# BB#35:
	addq	$40, %r13
.Ltmp28:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp29:
# BB#36:
.Ltmp34:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp35:
# BB#37:                                # %_ZN11NCoderMixer9CBindInfoD2Ev.exit
	movq	%r12, %rdi
	callq	_Unwind_Resume
.LBB0_39:
.Ltmp30:
	movq	%rax, %rbx
	jmp	.LBB0_42
.LBB0_40:
.Ltmp25:
	movq	%rax, %rbx
	jmp	.LBB0_41
.LBB0_38:
.Ltmp20:
	movq	%rax, %rbx
	leaq	72(%r13), %rdi
.Ltmp21:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp22:
.LBB0_41:
	addq	$40, %r13
.Ltmp26:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp27:
.LBB0_42:
.Ltmp31:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp32:
	jmp	.LBB0_45
.LBB0_43:
.Ltmp33:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_44:
.Ltmp36:
	movq	%rax, %rbx
.LBB0_45:                               # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN11NCoderMixer21CBindReverseConverterC2ERKNS_9CBindInfoE, .Lfunc_end0-_ZN11NCoderMixer21CBindReverseConverterC2ERKNS_9CBindInfoE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp8-.Ltmp5           #   Call between .Ltmp5 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp17-.Ltmp10         #   Call between .Ltmp10 and .Ltmp17
	.long	.Ltmp36-.Lfunc_begin0   #     jumps to .Ltmp36
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	1                       #   On action: 1
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	1                       #   On action: 1
	.long	.Ltmp28-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin0   #     jumps to .Ltmp30
	.byte	1                       #   On action: 1
	.long	.Ltmp34-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin0   #     jumps to .Ltmp36
	.byte	1                       #   On action: 1
	.long	.Ltmp35-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp21-.Ltmp35         #   Call between .Ltmp35 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp32-.Ltmp21         #   Call between .Ltmp21 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin0   #     jumps to .Ltmp33
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11NCoderMixer9CBindInfoC2ERKS0_,"axG",@progbits,_ZN11NCoderMixer9CBindInfoC2ERKS0_,comdat
	.weak	_ZN11NCoderMixer9CBindInfoC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer9CBindInfoC2ERKS0_,@function
_ZN11NCoderMixer9CBindInfoC2ERKS0_:     # @_ZN11NCoderMixer9CBindInfoC2ERKS0_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -48
.Lcfi19:
	.cfi_offset %r12, -40
.Lcfi20:
	.cfi_offset %r13, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	callq	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEEC2ERKS2_
	leaq	32(%r12), %r14
	leaq	32(%rbx), %rsi
.Ltmp37:
	movq	%r14, %rdi
	callq	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEEC2ERKS2_
.Ltmp38:
# BB#1:
	leaq	64(%r12), %r13
	leaq	64(%rbx), %rsi
.Ltmp40:
	movq	%r13, %rdi
	callq	_ZN13CRecordVectorIjEC2ERKS0_
.Ltmp41:
# BB#2:
	leaq	96(%r12), %rdi
	addq	$96, %rbx
.Ltmp43:
	movq	%rbx, %rsi
	callq	_ZN13CRecordVectorIjEC2ERKS0_
.Ltmp44:
# BB#3:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB1_6:
.Ltmp45:
	movq	%rax, %r15
.Ltmp46:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp47:
	jmp	.LBB1_7
.LBB1_5:
.Ltmp42:
	movq	%rax, %r15
.LBB1_7:
.Ltmp48:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp49:
	jmp	.LBB1_8
.LBB1_4:
.Ltmp39:
	movq	%rax, %r15
.LBB1_8:
.Ltmp50:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp51:
# BB#9:
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB1_10:
.Ltmp52:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end1:
	.size	_ZN11NCoderMixer9CBindInfoC2ERKS0_, .Lfunc_end1-_ZN11NCoderMixer9CBindInfoC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp37-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin1   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin1   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin1   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp51-.Ltmp46         #   Call between .Ltmp46 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin1   #     jumps to .Ltmp52
	.byte	1                       #   On action: 1
	.long	.Ltmp51-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Lfunc_end1-.Ltmp51     #   Call between .Ltmp51 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN11NCoderMixer21CBindReverseConverter21CreateReverseBindInfoERNS_9CBindInfoE
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer21CBindReverseConverter21CreateReverseBindInfoERNS_9CBindInfoE,@function
_ZN11NCoderMixer21CBindReverseConverter21CreateReverseBindInfoERNS_9CBindInfoE: # @_ZN11NCoderMixer21CBindReverseConverter21CreateReverseBindInfoERNS_9CBindInfoE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 80
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	32(%r12), %r13
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	64(%r12), %rdi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	96(%r12), %rdi
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	callq	_ZN17CBaseRecordVector5ClearEv
	movslq	20(%r14), %rbp
	testq	%rbp, %rbp
	jle	.LBB3_3
# BB#1:                                 # %.lr.ph49
	incq	%rbp
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	24(%r14), %rax
	movl	-16(%rax,%rbp,8), %ecx
	shlq	$32, %rcx
	movl	-12(%rax,%rbp,8), %r15d
	orq	%rcx, %r15
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r15, (%rax,%rcx,8)
	incl	12(%r12)
	decq	%rbp
	cmpq	$1, %rbp
	jg	.LBB3_2
.LBB3_3:                                # %._crit_edge50
	movslq	52(%r14), %rbp
	testq	%rbp, %rbp
	jle	.LBB3_6
# BB#4:                                 # %.lr.ph45
	incq	%rbp
	.p2align	4, 0x90
.LBB3_5:                                # =>This Inner Loop Header: Depth=1
	movq	184(%r14), %rax
	movq	56(%r14), %rcx
	movq	152(%r14), %rdx
	movslq	-12(%rcx,%rbp,8), %rsi
	movslq	-16(%rcx,%rbp,8), %rcx
	movl	(%rdx,%rcx,4), %ecx
	shlq	$32, %rcx
	movl	(%rax,%rsi,4), %ebx
	orq	%rcx, %rbx
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	48(%r12), %rax
	movslq	44(%r12), %rcx
	movq	%rbx, (%rax,%rcx,8)
	incl	44(%r12)
	decq	%rbp
	cmpq	$1, %rbp
	jg	.LBB3_5
.LBB3_6:                                # %.preheader38
	cmpl	$0, 84(%r14)
	movq	8(%rsp), %r15           # 8-byte Reload
	jle	.LBB3_9
# BB#7:                                 # %.lr.ph41
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_8:                                # =>This Inner Loop Header: Depth=1
	movq	88(%r14), %rax
	movq	152(%r14), %rcx
	movslq	(%rax,%rbp,4), %rax
	movl	(%rcx,%rax,4), %ebx
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	112(%r12), %rax
	movslq	108(%r12), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	108(%r12)
	incq	%rbp
	movslq	84(%r14), %rax
	cmpq	%rax, %rbp
	jl	.LBB3_8
.LBB3_9:                                # %.preheader
	cmpl	$0, 116(%r14)
	movq	16(%rsp), %r15          # 8-byte Reload
	jle	.LBB3_12
# BB#10:                                # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_11:                               # =>This Inner Loop Header: Depth=1
	movq	120(%r14), %rax
	movq	184(%r14), %rcx
	movslq	(%rax,%rbp,4), %rax
	movl	(%rcx,%rax,4), %ebx
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	80(%r12), %rax
	movslq	76(%r12), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	76(%r12)
	incq	%rbp
	movslq	116(%r14), %rax
	cmpq	%rax, %rbp
	jl	.LBB3_11
.LBB3_12:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN11NCoderMixer21CBindReverseConverter21CreateReverseBindInfoERNS_9CBindInfoE, .Lfunc_end3-_ZN11NCoderMixer21CBindReverseConverter21CreateReverseBindInfoERNS_9CBindInfoE
	.cfi_endproc

	.globl	_ZN11NCoderMixer11CCoderInfo2C2Ejj
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer11CCoderInfo2C2Ejj,@function
_ZN11NCoderMixer11CCoderInfo2C2Ejj:     # @_ZN11NCoderMixer11CCoderInfo2C2Ejj
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 64
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movl	%esi, 16(%rbx)
	movl	%edx, 20(%rbx)
	leaq	24(%rbx), %r14
	movups	%xmm0, 32(%rbx)
	movq	$8, 48(%rbx)
	movq	$_ZTV13CRecordVectorIyE+16, 24(%rbx)
	leaq	56(%rbx), %r15
	movups	%xmm0, 64(%rbx)
	movq	$8, 80(%rbx)
	movq	$_ZTV13CRecordVectorIyE+16, 56(%rbx)
	leaq	88(%rbx), %r13
	movups	%xmm0, 96(%rbx)
	movq	$8, 112(%rbx)
	movq	$_ZTV13CRecordVectorIPKyE+16, 88(%rbx)
	leaq	120(%rbx), %rbp
	movups	%xmm0, 128(%rbx)
	movq	$8, 144(%rbx)
	movq	$_ZTV13CRecordVectorIPKyE+16, 120(%rbx)
.Ltmp53:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp54:
# BB#1:
	movl	16(%rbx), %esi
.Ltmp55:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp56:
# BB#2:
	movl	20(%rbx), %esi
.Ltmp57:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp58:
# BB#3:
	movl	20(%rbx), %esi
.Ltmp59:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp60:
# BB#4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_5:
.Ltmp61:
	movq	%rax, %r12
.Ltmp62:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp63:
# BB#6:
.Ltmp64:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp65:
# BB#7:
.Ltmp66:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp67:
# BB#8:
.Ltmp68:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp69:
# BB#9:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_11
# BB#10:
	movq	(%rdi), %rax
.Ltmp70:
	callq	*16(%rax)
.Ltmp71:
.LBB4_11:                               # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_13
# BB#12:
	movq	(%rdi), %rax
.Ltmp72:
	callq	*16(%rax)
.Ltmp73:
.LBB4_13:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	%r12, %rdi
	callq	_Unwind_Resume
.LBB4_14:
.Ltmp74:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN11NCoderMixer11CCoderInfo2C2Ejj, .Lfunc_end4-_ZN11NCoderMixer11CCoderInfo2C2Ejj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\257\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp53-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp60-.Ltmp53         #   Call between .Ltmp53 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin2   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp73-.Ltmp62         #   Call between .Ltmp62 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin2   #     jumps to .Ltmp74
	.byte	1                       #   On action: 1
	.long	.Ltmp73-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Lfunc_end4-.Ltmp73     #   Call between .Ltmp73 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN11NCoderMixer11CCoderInfo212SetCoderInfoEPPKyS3_
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer11CCoderInfo212SetCoderInfoEPPKyS3_,@function
_ZN11NCoderMixer11CCoderInfo212SetCoderInfoEPPKyS3_: # @_ZN11NCoderMixer11CCoderInfo212SetCoderInfoEPPKyS3_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -24
.Lcfi53:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	leaq	24(%rbx), %rax
	leaq	88(%rbx), %rdx
	movl	16(%rbx), %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej
	movl	20(%rbx), %ecx
	leaq	56(%rbx), %rsi
	leaq	120(%rbx), %rdx
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej # TAILCALL
.Lfunc_end5:
	.size	_ZN11NCoderMixer11CCoderInfo212SetCoderInfoEPPKyS3_, .Lfunc_end5-_ZN11NCoderMixer11CCoderInfo212SetCoderInfoEPPKyS3_
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej,@function
_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej: # @_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 64
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	testl	%ebp, %ebp
	je	.LBB6_6
# BB#1:                                 # %.lr.ph
	testq	%rbx, %rbx
	je	.LBB6_5
# BB#2:                                 # %.lr.ph.split.preheader
	movl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.LBB6_4
# BB#7:                                 #   in Loop: Header=BB6_3 Depth=1
	movq	(%rax), %r13
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	leaq	(%rax,%rcx,8), %r12
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	jmp	.LBB6_8
	.p2align	4, 0x90
.LBB6_4:                                #   in Loop: Header=BB6_3 Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	$0, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	xorl	%r12d, %r12d
.LBB6_8:                                #   in Loop: Header=BB6_3 Depth=1
	movq	16(%r14), %rax
	movslq	12(%r14), %rcx
	movq	%r12, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r14)
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB6_3
	jmp	.LBB6_6
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	$0, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r14), %rax
	movslq	12(%r14), %rcx
	movq	$0, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r14)
	decl	%ebp
	jne	.LBB6_5
.LBB6_6:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej, .Lfunc_end6-_ZN11NCoderMixerL8SetSizesEPPKyR13CRecordVectorIyERS3_IS1_Ej
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEEC2ERKS2_,"axG",@progbits,_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEEC2ERKS2_,comdat
	.weak	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEEC2ERKS2_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEEC2ERKS2_,@function
_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEEC2ERKS2_: # @_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEEC2ERKS2_
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 48
.Lcfi72:
	.cfi_offset %rbx, -48
.Lcfi73:
	.cfi_offset %r12, -40
.Lcfi74:
	.cfi_offset %r13, -32
.Lcfi75:
	.cfi_offset %r14, -24
.Lcfi76:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$8, 24(%r12)
	movq	$_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE+16, (%r12)
.Ltmp75:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp76:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp77:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp78:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB7_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_4:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp80:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp81:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	incl	12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB7_4
.LBB7_6:                                # %_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEEaSERKS2_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB7_8:                                # %.loopexit.split-lp
.Ltmp79:
	jmp	.LBB7_9
.LBB7_7:                                # %.loopexit
.Ltmp82:
.LBB7_9:
	movq	%rax, %r14
.Ltmp83:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp84:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_11:
.Ltmp85:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEEC2ERKS2_, .Lfunc_end7-_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEEC2ERKS2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp75-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp78-.Ltmp75         #   Call between .Ltmp75 and .Ltmp78
	.long	.Ltmp79-.Lfunc_begin3   #     jumps to .Ltmp79
	.byte	0                       #   On action: cleanup
	.long	.Ltmp80-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp81-.Ltmp80         #   Call between .Ltmp80 and .Ltmp81
	.long	.Ltmp82-.Lfunc_begin3   #     jumps to .Ltmp82
	.byte	0                       #   On action: cleanup
	.long	.Ltmp83-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp84-.Ltmp83         #   Call between .Ltmp83 and .Ltmp84
	.long	.Ltmp85-.Lfunc_begin3   #     jumps to .Ltmp85
	.byte	1                       #   On action: 1
	.long	.Ltmp84-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end7-.Ltmp84     #   Call between .Ltmp84 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIN11NCoderMixer9CBindPairEEC2ERKS2_,"axG",@progbits,_ZN13CRecordVectorIN11NCoderMixer9CBindPairEEC2ERKS2_,comdat
	.weak	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEEC2ERKS2_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEEC2ERKS2_,@function
_ZN13CRecordVectorIN11NCoderMixer9CBindPairEEC2ERKS2_: # @_ZN13CRecordVectorIN11NCoderMixer9CBindPairEEC2ERKS2_
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r15
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 48
.Lcfi82:
	.cfi_offset %rbx, -48
.Lcfi83:
	.cfi_offset %r12, -40
.Lcfi84:
	.cfi_offset %r13, -32
.Lcfi85:
	.cfi_offset %r14, -24
.Lcfi86:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$8, 24(%r12)
	movq	$_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE+16, (%r12)
.Ltmp86:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp87:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp88:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp89:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB8_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_4:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp91:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp92:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	incl	12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB8_4
.LBB8_6:                                # %_ZN13CRecordVectorIN11NCoderMixer9CBindPairEEaSERKS2_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB8_8:                                # %.loopexit.split-lp
.Ltmp90:
	jmp	.LBB8_9
.LBB8_7:                                # %.loopexit
.Ltmp93:
.LBB8_9:
	movq	%rax, %r14
.Ltmp94:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp95:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_11:
.Ltmp96:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEEC2ERKS2_, .Lfunc_end8-_ZN13CRecordVectorIN11NCoderMixer9CBindPairEEC2ERKS2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp86-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp89-.Ltmp86         #   Call between .Ltmp86 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin4   #     jumps to .Ltmp90
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin4   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin4   #     jumps to .Ltmp96
	.byte	1                       #   On action: 1
	.long	.Ltmp95-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp95     #   Call between .Ltmp95 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIjEC2ERKS0_,"axG",@progbits,_ZN13CRecordVectorIjEC2ERKS0_,comdat
	.weak	_ZN13CRecordVectorIjEC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjEC2ERKS0_,@function
_ZN13CRecordVectorIjEC2ERKS0_:          # @_ZN13CRecordVectorIjEC2ERKS0_
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi89:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi90:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 48
.Lcfi92:
	.cfi_offset %rbx, -48
.Lcfi93:
	.cfi_offset %r12, -40
.Lcfi94:
	.cfi_offset %r14, -32
.Lcfi95:
	.cfi_offset %r15, -24
.Lcfi96:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$4, 24(%r12)
	movq	$_ZTV13CRecordVectorIjE+16, (%r12)
.Ltmp97:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp98:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp99:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp100:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB9_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_4:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movl	(%rax,%rbx,4), %ebp
.Ltmp102:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp103:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB9_4
.LBB9_6:                                # %_ZN13CRecordVectorIjEaSERKS0_.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_8:                                # %.loopexit.split-lp
.Ltmp101:
	jmp	.LBB9_9
.LBB9_7:                                # %.loopexit
.Ltmp104:
.LBB9_9:
	movq	%rax, %r14
.Ltmp105:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp106:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB9_11:
.Ltmp107:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN13CRecordVectorIjEC2ERKS0_, .Lfunc_end9-_ZN13CRecordVectorIjEC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp97-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp100-.Ltmp97        #   Call between .Ltmp97 and .Ltmp100
	.long	.Ltmp101-.Lfunc_begin5  #     jumps to .Ltmp101
	.byte	0                       #   On action: cleanup
	.long	.Ltmp102-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp103-.Ltmp102       #   Call between .Ltmp102 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin5  #     jumps to .Ltmp104
	.byte	0                       #   On action: cleanup
	.long	.Ltmp105-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp106-.Ltmp105       #   Call between .Ltmp105 and .Ltmp106
	.long	.Ltmp107-.Lfunc_begin5  #     jumps to .Ltmp107
	.byte	1                       #   On action: 1
	.long	.Ltmp106-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Lfunc_end9-.Ltmp106    #   Call between .Ltmp106 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev,@function
_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev: # @_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 32
.Lcfi100:
	.cfi_offset %rbx, -24
.Lcfi101:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp108:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp109:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB10_2:
.Ltmp110:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev, .Lfunc_end10-_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp108-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp109-.Ltmp108       #   Call between .Ltmp108 and .Ltmp109
	.long	.Ltmp110-.Lfunc_begin6  #     jumps to .Ltmp110
	.byte	0                       #   On action: cleanup
	.long	.Ltmp109-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Lfunc_end10-.Ltmp109   #   Call between .Ltmp109 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev,@function
_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev: # @_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi104:
	.cfi_def_cfa_offset 32
.Lcfi105:
	.cfi_offset %rbx, -24
.Lcfi106:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp111:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp112:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB11_2:
.Ltmp113:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev, .Lfunc_end11-_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp111-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp112-.Ltmp111       #   Call between .Ltmp111 and .Ltmp112
	.long	.Ltmp113-.Lfunc_begin7  #     jumps to .Ltmp113
	.byte	0                       #   On action: cleanup
	.long	.Ltmp112-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Lfunc_end11-.Ltmp112   #   Call between .Ltmp112 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIjED0Ev,"axG",@progbits,_ZN13CRecordVectorIjED0Ev,comdat
	.weak	_ZN13CRecordVectorIjED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjED0Ev,@function
_ZN13CRecordVectorIjED0Ev:              # @_ZN13CRecordVectorIjED0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi109:
	.cfi_def_cfa_offset 32
.Lcfi110:
	.cfi_offset %rbx, -24
.Lcfi111:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp114:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp115:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB12_2:
.Ltmp116:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN13CRecordVectorIjED0Ev, .Lfunc_end12-_ZN13CRecordVectorIjED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp114-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp115-.Ltmp114       #   Call between .Ltmp114 and .Ltmp115
	.long	.Ltmp116-.Lfunc_begin8  #     jumps to .Ltmp116
	.byte	0                       #   On action: cleanup
	.long	.Ltmp115-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Lfunc_end12-.Ltmp115   #   Call between .Ltmp115 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi114:
	.cfi_def_cfa_offset 32
.Lcfi115:
	.cfi_offset %rbx, -24
.Lcfi116:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp117:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp118:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB13_2:
.Ltmp119:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end13-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp117-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp118-.Ltmp117       #   Call between .Ltmp117 and .Ltmp118
	.long	.Ltmp119-.Lfunc_begin9  #     jumps to .Ltmp119
	.byte	0                       #   On action: cleanup
	.long	.Ltmp118-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Lfunc_end13-.Ltmp118   #   Call between .Ltmp118 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIPKyED0Ev,"axG",@progbits,_ZN13CRecordVectorIPKyED0Ev,comdat
	.weak	_ZN13CRecordVectorIPKyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIPKyED0Ev,@function
_ZN13CRecordVectorIPKyED0Ev:            # @_ZN13CRecordVectorIPKyED0Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi119:
	.cfi_def_cfa_offset 32
.Lcfi120:
	.cfi_offset %rbx, -24
.Lcfi121:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp120:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp121:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB14_2:
.Ltmp122:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN13CRecordVectorIPKyED0Ev, .Lfunc_end14-_ZN13CRecordVectorIPKyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp120-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp121-.Ltmp120       #   Call between .Ltmp120 and .Ltmp121
	.long	.Ltmp122-.Lfunc_begin10 #     jumps to .Ltmp122
	.byte	0                       #   On action: cleanup
	.long	.Ltmp121-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Lfunc_end14-.Ltmp121   #   Call between .Ltmp121 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,@object # @_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.section	.rodata._ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,"aG",@progbits,_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,comdat
	.weak	_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.p2align	3
_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE, 40

	.type	_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,@object # @_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.section	.rodata._ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,"aG",@progbits,_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,comdat
	.weak	_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.p2align	4
_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE:
	.asciz	"13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE"
	.size	_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE, 52

	.type	_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,@object # @_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.section	.rodata._ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,"aG",@progbits,_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,comdat
	.weak	_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.p2align	4
_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE, 24

	.type	_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE,@object # @_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE
	.section	.rodata._ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE,"aG",@progbits,_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE,comdat
	.weak	_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE
	.p2align	3
_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE, 40

	.type	_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE,@object # @_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE
	.section	.rodata._ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE,"aG",@progbits,_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE,comdat
	.weak	_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE
	.p2align	4
_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE:
	.asciz	"13CRecordVectorIN11NCoderMixer9CBindPairEE"
	.size	_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE, 43

	.type	_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE,@object # @_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE
	.section	.rodata._ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE,"aG",@progbits,_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE,comdat
	.weak	_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE
	.p2align	4
_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE, 24

	.type	_ZTV13CRecordVectorIjE,@object # @_ZTV13CRecordVectorIjE
	.section	.rodata._ZTV13CRecordVectorIjE,"aG",@progbits,_ZTV13CRecordVectorIjE,comdat
	.weak	_ZTV13CRecordVectorIjE
	.p2align	3
_ZTV13CRecordVectorIjE:
	.quad	0
	.quad	_ZTI13CRecordVectorIjE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIjED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIjE, 40

	.type	_ZTS13CRecordVectorIjE,@object # @_ZTS13CRecordVectorIjE
	.section	.rodata._ZTS13CRecordVectorIjE,"aG",@progbits,_ZTS13CRecordVectorIjE,comdat
	.weak	_ZTS13CRecordVectorIjE
	.p2align	4
_ZTS13CRecordVectorIjE:
	.asciz	"13CRecordVectorIjE"
	.size	_ZTS13CRecordVectorIjE, 19

	.type	_ZTI13CRecordVectorIjE,@object # @_ZTI13CRecordVectorIjE
	.section	.rodata._ZTI13CRecordVectorIjE,"aG",@progbits,_ZTI13CRecordVectorIjE,comdat
	.weak	_ZTI13CRecordVectorIjE
	.p2align	4
_ZTI13CRecordVectorIjE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIjE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIjE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24

	.type	_ZTV13CRecordVectorIPKyE,@object # @_ZTV13CRecordVectorIPKyE
	.section	.rodata._ZTV13CRecordVectorIPKyE,"aG",@progbits,_ZTV13CRecordVectorIPKyE,comdat
	.weak	_ZTV13CRecordVectorIPKyE
	.p2align	3
_ZTV13CRecordVectorIPKyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIPKyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIPKyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIPKyE, 40

	.type	_ZTS13CRecordVectorIPKyE,@object # @_ZTS13CRecordVectorIPKyE
	.section	.rodata._ZTS13CRecordVectorIPKyE,"aG",@progbits,_ZTS13CRecordVectorIPKyE,comdat
	.weak	_ZTS13CRecordVectorIPKyE
	.p2align	4
_ZTS13CRecordVectorIPKyE:
	.asciz	"13CRecordVectorIPKyE"
	.size	_ZTS13CRecordVectorIPKyE, 21

	.type	_ZTI13CRecordVectorIPKyE,@object # @_ZTI13CRecordVectorIPKyE
	.section	.rodata._ZTI13CRecordVectorIPKyE,"aG",@progbits,_ZTI13CRecordVectorIPKyE,comdat
	.weak	_ZTI13CRecordVectorIPKyE
	.p2align	4
_ZTI13CRecordVectorIPKyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPKyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPKyE, 24


	.globl	_ZN11NCoderMixer21CBindReverseConverterC1ERKNS_9CBindInfoE
	.type	_ZN11NCoderMixer21CBindReverseConverterC1ERKNS_9CBindInfoE,@function
_ZN11NCoderMixer21CBindReverseConverterC1ERKNS_9CBindInfoE = _ZN11NCoderMixer21CBindReverseConverterC2ERKNS_9CBindInfoE
	.globl	_ZN11NCoderMixer11CCoderInfo2C1Ejj
	.type	_ZN11NCoderMixer11CCoderInfo2C1Ejj,@function
_ZN11NCoderMixer11CCoderInfo2C1Ejj = _ZN11NCoderMixer11CCoderInfo2C2Ejj
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
