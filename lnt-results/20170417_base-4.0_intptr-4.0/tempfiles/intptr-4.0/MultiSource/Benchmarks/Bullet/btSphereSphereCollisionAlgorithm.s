	.text
	.file	"btSphereSphereCollisionAlgorithm.bc"
	.globl	_ZN32btSphereSphereCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_
	.p2align	4, 0x90
	.type	_ZN32btSphereSphereCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_,@function
_ZN32btSphereSphereCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_: # @_ZN32btSphereSphereCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	%rdx, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	movq	$_ZTV32btSphereSphereCollisionAlgorithm+16, (%rbx)
	movb	$0, 16(%rbx)
	movq	%r12, 24(%rbx)
	testq	%r12, %r12
	jne	.LBB0_3
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp0:
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*24(%rax)
.Ltmp1:
# BB#2:
	movq	%rax, 24(%rbx)
	movb	$1, 16(%rbx)
.LBB0_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB0_4:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp4:
# BB#5:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_6:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN32btSphereSphereCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_, .Lfunc_end0-_ZN32btSphereSphereCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN32btSphereSphereCollisionAlgorithmD2Ev
	.p2align	4, 0x90
	.type	_ZN32btSphereSphereCollisionAlgorithmD2Ev,@function
_ZN32btSphereSphereCollisionAlgorithmD2Ev: # @_ZN32btSphereSphereCollisionAlgorithmD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV32btSphereSphereCollisionAlgorithm+16, (%rbx)
	cmpb	$0, 16(%rbx)
	je	.LBB2_3
# BB#1:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB2_3
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp6:
	callq	*32(%rax)
.Ltmp7:
.LBB2_3:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN30btActivatingCollisionAlgorithmD2Ev # TAILCALL
.LBB2_4:
.Ltmp8:
	movq	%rax, %r14
.Ltmp9:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp10:
# BB#5:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_6:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN32btSphereSphereCollisionAlgorithmD2Ev, .Lfunc_end2-_ZN32btSphereSphereCollisionAlgorithmD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN32btSphereSphereCollisionAlgorithmD0Ev
	.p2align	4, 0x90
	.type	_ZN32btSphereSphereCollisionAlgorithmD0Ev,@function
_ZN32btSphereSphereCollisionAlgorithmD0Ev: # @_ZN32btSphereSphereCollisionAlgorithmD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV32btSphereSphereCollisionAlgorithm+16, (%rbx)
	cmpb	$0, 16(%rbx)
	je	.LBB3_3
# BB#1:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB3_3
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp12:
	callq	*32(%rax)
.Ltmp13:
.LBB3_3:
.Ltmp18:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp19:
# BB#4:                                 # %_ZN32btSphereSphereCollisionAlgorithmD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_5:
.Ltmp14:
	movq	%rax, %r14
.Ltmp15:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp16:
	jmp	.LBB3_8
.LBB3_6:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_7:
.Ltmp20:
	movq	%rax, %r14
.LBB3_8:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN32btSphereSphereCollisionAlgorithmD0Ev, .Lfunc_end3-_ZN32btSphereSphereCollisionAlgorithmD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin2   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1065353216              # float 1
.LCPI4_1:
	.long	872415232               # float 1.1920929E-7
	.text
	.globl	_ZN32btSphereSphereCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN32btSphereSphereCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN32btSphereSphereCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN32btSphereSphereCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 40
	subq	$72, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 112
.Lcfi24:
	.cfi_offset %rbx, -40
.Lcfi25:
	.cfi_offset %r12, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rdx, %r15
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.LBB4_17
# BB#1:
	movq	%rax, 8(%r14)
	movq	200(%rsi), %rbx
	movq	200(%r15), %r12
	movsd	56(%rsi), %xmm5         # xmm5 = mem[0],zero
	movsd	56(%r15), %xmm0         # xmm0 = mem[0],zero
	subps	%xmm0, %xmm5
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	64(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	64(%r15), %xmm4
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB4_3
# BB#2:                                 # %call.sqrt
	movss	%xmm4, 12(%rsp)         # 4-byte Spill
	movaps	%xmm5, 48(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	48(%rsp), %xmm5         # 16-byte Reload
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB4_3:                                # %.split
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	24(%rbx), %xmm3
	movss	40(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	24(%r12), %xmm2
	addss	%xmm2, %xmm3
	ucomiss	%xmm3, %xmm1
	jbe	.LBB4_9
# BB#4:
	movq	8(%r14), %rdi
	cmpl	$0, 728(%rdi)
	je	.LBB4_17
# BB#5:
	movq	712(%rdi), %rax
	cmpq	144(%r14), %rax
	je	.LBB4_8
# BB#6:
	leaq	80(%r14), %rsi
	addq	$16, %r14
	jmp	.LBB4_7
.LBB4_9:
	movaps	%xmm1, %xmm0
	subss	%xmm3, %xmm0
	movq	$1065353216, 16(%rsp)   # imm = 0x3F800000
	movq	$0, 24(%rsp)
	movss	.LCPI4_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	ucomiss	.LCPI4_1(%rip), %xmm1
	jbe	.LBB4_10
# BB#11:
	divss	%xmm1, %xmm3
	mulss	%xmm3, %xmm4
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	xorps	%xmm1, %xmm1
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	movlps	%xmm3, 16(%rsp)
	movlps	%xmm1, 24(%rsp)
	jmp	.LBB4_12
.LBB4_10:
	xorps	%xmm4, %xmm4
.LBB4_12:
	movaps	%xmm2, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm3, %xmm1
	mulss	%xmm2, %xmm4
	movsd	56(%r15), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	64(%r15), %xmm4
	xorps	%xmm1, %xmm1
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	movlps	%xmm2, 32(%rsp)
	movlps	%xmm1, 40(%rsp)
	movq	(%r14), %rax
	leaq	16(%rsp), %rsi
	leaq	32(%rsp), %rdx
	movq	%r14, %rdi
	callq	*32(%rax)
	movq	8(%r14), %rdi
	cmpl	$0, 728(%rdi)
	je	.LBB4_17
# BB#13:
	movq	712(%rdi), %rax
	cmpq	144(%r14), %rax
	je	.LBB4_15
# BB#14:
	leaq	80(%r14), %rsi
	addq	$16, %r14
	jmp	.LBB4_16
.LBB4_8:
	leaq	16(%r14), %rsi
	addq	$80, %r14
.LBB4_7:
	movq	%r14, %rdx
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_ # TAILCALL
.LBB4_15:
	leaq	16(%r14), %rsi
	addq	$80, %r14
.LBB4_16:                               # %_ZN16btManifoldResult20refreshContactPointsEv.exit
	movq	%r14, %rdx
	callq	_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_
.LBB4_17:                               # %_ZN16btManifoldResult20refreshContactPointsEv.exit34
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	_ZN32btSphereSphereCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end4-_ZN32btSphereSphereCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN32btSphereSphereCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN32btSphereSphereCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN32btSphereSphereCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN32btSphereSphereCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	movss	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end5:
	.size	_ZN32btSphereSphereCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end5-_ZN32btSphereSphereCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.text._ZN32btSphereSphereCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,"axG",@progbits,_ZN32btSphereSphereCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,comdat
	.weak	_ZN32btSphereSphereCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.p2align	4, 0x90
	.type	_ZN32btSphereSphereCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,@function
_ZN32btSphereSphereCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE: # @_ZN32btSphereSphereCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 48
.Lcfi33:
	.cfi_offset %rbx, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	24(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB6_18
# BB#1:
	cmpb	$0, 16(%r14)
	je	.LBB6_18
# BB#2:
	movl	4(%rbx), %eax
	cmpl	8(%rbx), %eax
	jne	.LBB6_17
# BB#3:
	leal	(%rax,%rax), %edx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%edx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB6_17
# BB#4:
	testl	%ebp, %ebp
	je	.LBB6_5
# BB#6:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB6_8
	jmp	.LBB6_12
.LBB6_5:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB6_12
.LBB6_8:                                # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB6_10
	.p2align	4, 0x90
.LBB6_9:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB6_9
.LBB6_10:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB6_12
	.p2align	4, 0x90
.LBB6_11:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r15,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB6_11
.LBB6_12:                               # %_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_.exit.i.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_16
# BB#13:
	cmpb	$0, 24(%rbx)
	je	.LBB6_15
# BB#14:
	callq	_Z21btAlignedFreeInternalPv
	movl	4(%rbx), %eax
.LBB6_15:
	movq	$0, 16(%rbx)
.LBB6_16:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv.exit.i.i
	movb	$1, 24(%rbx)
	movq	%r15, 16(%rbx)
	movl	%ebp, 8(%rbx)
	movq	24(%r14), %rcx
.LBB6_17:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_.exit
	movq	16(%rbx), %rdx
	cltq
	movq	%rcx, (%rdx,%rax,8)
	incl	%eax
	movl	%eax, 4(%rbx)
.LBB6_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN32btSphereSphereCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE, .Lfunc_end6-_ZN32btSphereSphereCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_endproc

	.type	_ZTV32btSphereSphereCollisionAlgorithm,@object # @_ZTV32btSphereSphereCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTV32btSphereSphereCollisionAlgorithm
	.p2align	3
_ZTV32btSphereSphereCollisionAlgorithm:
	.quad	0
	.quad	_ZTI32btSphereSphereCollisionAlgorithm
	.quad	_ZN32btSphereSphereCollisionAlgorithmD2Ev
	.quad	_ZN32btSphereSphereCollisionAlgorithmD0Ev
	.quad	_ZN32btSphereSphereCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN32btSphereSphereCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN32btSphereSphereCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.size	_ZTV32btSphereSphereCollisionAlgorithm, 56

	.type	_ZTS32btSphereSphereCollisionAlgorithm,@object # @_ZTS32btSphereSphereCollisionAlgorithm
	.globl	_ZTS32btSphereSphereCollisionAlgorithm
	.p2align	4
_ZTS32btSphereSphereCollisionAlgorithm:
	.asciz	"32btSphereSphereCollisionAlgorithm"
	.size	_ZTS32btSphereSphereCollisionAlgorithm, 35

	.type	_ZTI32btSphereSphereCollisionAlgorithm,@object # @_ZTI32btSphereSphereCollisionAlgorithm
	.globl	_ZTI32btSphereSphereCollisionAlgorithm
	.p2align	4
_ZTI32btSphereSphereCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS32btSphereSphereCollisionAlgorithm
	.quad	_ZTI30btActivatingCollisionAlgorithm
	.size	_ZTI32btSphereSphereCollisionAlgorithm, 24


	.globl	_ZN32btSphereSphereCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_
	.type	_ZN32btSphereSphereCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_,@function
_ZN32btSphereSphereCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_ = _ZN32btSphereSphereCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_
	.globl	_ZN32btSphereSphereCollisionAlgorithmD1Ev
	.type	_ZN32btSphereSphereCollisionAlgorithmD1Ev,@function
_ZN32btSphereSphereCollisionAlgorithmD1Ev = _ZN32btSphereSphereCollisionAlgorithmD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
