	.text
	.file	"st.bc"
	.globl	st_IndexCreate
	.p2align	4, 0x90
	.type	st_IndexCreate,@function
st_IndexCreate:                         # @st_IndexCreate
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$32, %edi
	callq	memory_Malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 12(%rax)
	movups	%xmm0, (%rax)
	popq	%rcx
	retq
.Lfunc_end0:
	.size	st_IndexCreate, .Lfunc_end0-st_IndexCreate
	.cfi_endproc

	.globl	st_EntryCreate
	.p2align	4, 0x90
	.type	st_EntryCreate,@function
st_EntryCreate:                         # @st_EntryCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 96
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r13
	callq	cont_Check
	movq	%r12, %rdi
	callq	term_ComputeSize
	movl	cont_INDEXVARSCANNER(%rip), %ecx
	movl	symbol_INDEXVARCOUNTER(%rip), %esi
	cmpl	%esi, %ecx
	je	.LBB1_5
# BB#1:                                 # %.preheader.i
	movslq	%ecx, %rdx
	shlq	$5, %rdx
	leaq	48(%rbp,%rdx), %rdx
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rdx)
	je	.LBB1_6
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	incl	%ecx
	addq	$32, %rdx
	cmpl	%ecx, %esi
	jne	.LBB1_2
# BB#4:                                 # %.sink.split.loopexit.i
	movl	%esi, cont_INDEXVARSCANNER(%rip)
.LBB1_5:                                # %.sink.split.i
	incl	%esi
	movl	%esi, symbol_INDEXVARCOUNTER(%rip)
	jmp	.LBB1_7
.LBB1_6:                                # %cont_NextIndexVariable.exit.loopexit
	incl	%ecx
	movl	%ecx, %esi
.LBB1_7:                                # %cont_NextIndexVariable.exit
	movl	%esi, cont_INDEXVARSCANNER(%rip)
	movslq	%esi, %rsi
	shlq	$5, %rsi
	leaq	(%rbp,%rsi), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	%r12, 8(%rbp,%rsi)
	movq	%rbp, 16(%rbp,%rsi)
	movq	cont_LASTBINDING(%rip), %rdx
	movq	%rdx, 24(%rbp,%rsi)
	incl	cont_BINDINGS(%rip)
	movq	%rcx, cont_LASTBINDING(%rip)
	movzwl	24(%r13), %ecx
	movzwl	%ax, %edx
	cmpl	%edx, %ecx
	jae	.LBB1_9
# BB#8:
	movw	%ax, 24(%r13)
	jmp	.LBB1_11
.LBB1_9:
	movzwl	26(%r13), %ecx
	cmpl	%edx, %ecx
	jbe	.LBB1_11
# BB#10:
	movw	%ax, 26(%r13)
.LBB1_11:                               # %.preheader
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	je	.LBB1_13
# BB#12:
                                        # implicit-def: %R14
.LBB1_34:                               # %.critedge
	cmpl	$0, cont_BINDINGS(%rip)
	je	.LBB1_35
.LBB1_36:                               # %.critedge.thread
	testq	%r14, %r14
	je	.LBB1_43
# BB#37:
	cmpq	$0, 16(%r14)
	jne	.LBB1_39
# BB#38:
	movq	8(%r14), %rsi
	movq	%rbp, %rdi
	callq	st_CloseUsedVariables
.LBB1_39:                               # %._crit_edge
	movq	(%r14), %rsi
	leaq	32(%rsp), %rdx
	leaq	24(%rsp), %rcx
	movq	%rbp, %rdi
	callq	subst_ComGen
	movq	%rax, %rbp
	movq	32(%rsp), %rbx
	movq	24(%rsp), %rdi
	callq	subst_CloseOpenVariables
	movq	%rax, %r12
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%rbx, (%r13)
	movq	16(%r14), %rax
	movq	%rax, 16(%r13)
	movq	8(%r14), %rax
	movq	%rax, 8(%r13)
	movzwl	24(%r14), %eax
	movw	%ax, 24(%r13)
	movzwl	26(%r14), %eax
	movw	%ax, 26(%r13)
	movq	(%r14), %rdi
	callq	subst_Delete
	movq	%rbp, (%r14)
	movq	$0, 16(%r14)
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	$0, 8(%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 16(%rbp)
	movq	%r12, (%rbp)
	movl	4(%rsp), %r15d          # 4-byte Reload
	movw	%r15w, 24(%rbp)
	movw	%r15w, 26(%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r13, 8(%rbx)
	movq	$0, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 8(%r14)
	cmpw	%r15w, 24(%r14)
	jae	.LBB1_41
# BB#40:
	movw	%r15w, 24(%r14)
	jmp	.LBB1_44
.LBB1_13:                               # %.lr.ph.preheader
	movl	%edx, 20(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB1_14:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_16 Depth 2
                                        #       Child Loop BB1_23 Depth 3
	movq	8(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB1_43
# BB#15:                                # %.lr.ph.i52
                                        #   in Loop: Header=BB1_14 Depth=1
	movl	cont_BINDINGS(%rip), %eax
	movl	cont_STACKPOINTER(%rip), %ecx
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_16:                               #   Parent Loop BB1_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_23 Depth 3
	movq	8(%rbx), %r15
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	(%r15), %rsi
	movq	%rbp, %rdi
	callq	subst_Variation
	testl	%eax, %eax
	je	.LBB1_19
# BB#17:                                #   in Loop: Header=BB1_16 Depth=2
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	jne	.LBB1_26
# BB#18:                                #   in Loop: Header=BB1_16 Depth=2
	movq	%r15, %r12
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_19:                               #   in Loop: Header=BB1_16 Depth=2
	testq	%r14, %r14
	jne	.LBB1_21
# BB#20:                                #   in Loop: Header=BB1_16 Depth=2
	movq	(%r15), %rsi
	movq	%rbp, %rdi
	callq	subst_MatchTops
	testl	%eax, %eax
	movl	$0, %r14d
	cmovneq	%r15, %r14
	.p2align	4, 0x90
.LBB1_21:                               #   in Loop: Header=BB1_16 Depth=2
	xorps	%xmm0, %xmm0
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB1_22
	.p2align	4, 0x90
.LBB1_23:                               # %.lr.ph.i.i
                                        #   Parent Loop BB1_14 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB1_23
	jmp	.LBB1_24
	.p2align	4, 0x90
.LBB1_22:                               #   in Loop: Header=BB1_16 Depth=2
	movl	%ecx, %eax
.LBB1_24:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB1_16 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB1_25
# BB#27:                                #   in Loop: Header=BB1_16 Depth=2
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB1_28
	.p2align	4, 0x90
.LBB1_25:                               #   in Loop: Header=BB1_16 Depth=2
	xorl	%ecx, %ecx
.LBB1_28:                               #   in Loop: Header=BB1_16 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_16
# BB#29:                                # %st_FirstVariant.exit
                                        #   in Loop: Header=BB1_14 Depth=1
	testq	%r12, %r12
	jne	.LBB1_30
	jmp	.LBB1_36
	.p2align	4, 0x90
.LBB1_26:                               # %st_FirstVariant.exit.thread60
                                        #   in Loop: Header=BB1_14 Depth=1
	movq	%rbp, %rdi
	callq	subst_CloseVariables
	movq	%r15, %r12
.LBB1_30:                               #   in Loop: Header=BB1_14 Depth=1
	movq	%r12, %r13
	movzwl	24(%r13), %eax
	movl	20(%rsp), %edx          # 4-byte Reload
	cmpl	%edx, %eax
	jae	.LBB1_32
# BB#31:                                #   in Loop: Header=BB1_14 Depth=1
	movl	4(%rsp), %eax           # 4-byte Reload
	movw	%ax, 24(%r13)
	jmp	.LBB1_33
	.p2align	4, 0x90
.LBB1_32:                               #   in Loop: Header=BB1_14 Depth=1
	movzwl	26(%r13), %eax
	cmpl	%edx, %eax
	jbe	.LBB1_33
# BB#48:                                #   in Loop: Header=BB1_14 Depth=1
	movl	4(%rsp), %eax           # 4-byte Reload
	movw	%ax, 26(%r13)
.LBB1_33:                               # %.backedge
                                        #   in Loop: Header=BB1_14 Depth=1
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB1_14
	jmp	.LBB1_34
.LBB1_43:                               # %.critedge.thread.thread
	xorl	%edi, %edi
	callq	subst_CloseOpenVariables
	movq	%rax, %r12
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	$0, 8(%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 16(%rbp)
	movq	%r12, (%rbp)
	movl	4(%rsp), %eax           # 4-byte Reload
	movw	%ax, 24(%rbp)
	movw	%ax, 26(%rbp)
	movq	8(%r13), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 8(%r13)
	jmp	.LBB1_44
.LBB1_35:
	movl	$16, %edi
	callq	memory_Malloc
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 16(%r13)
	jmp	.LBB1_44
.LBB1_41:
	cmpw	%r15w, 26(%r14)
	jbe	.LBB1_44
# BB#42:
	movw	%r15w, 26(%r14)
.LBB1_44:
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB1_47
# BB#45:                                # %.lr.ph.preheader.i
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_46:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB1_46
.LBB1_47:                               # %cont_Reset.exit
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	st_EntryCreate, .Lfunc_end1-st_EntryCreate
	.cfi_endproc

	.p2align	4, 0x90
	.type	st_CloseUsedVariables,@function
st_CloseUsedVariables:                  # @st_CloseUsedVariables
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%r14, %r14
	jne	.LBB2_2
	jmp	.LBB2_10
	.p2align	4, 0x90
.LBB2_6:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	8(%r14), %rcx
.LBB2_7:                                # %._crit_edge
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpq	$0, 16(%rcx)
	jne	.LBB2_9
# BB#8:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	8(%rcx), %rsi
	movq	%rbx, %rdi
	callq	st_CloseUsedVariables
.LBB2_9:                                #   in Loop: Header=BB2_2 Depth=1
	movq	(%r14), %r14
	testq	%r14, %r14
	je	.LBB2_10
.LBB2_2:                                # %.lr.ph24
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
	movq	8(%r14), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.LBB2_7
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	8(%rax), %rcx
	shlq	$5, %rcx
	cmpq	$0, 16(%rbx,%rcx)
	jne	.LBB2_5
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=2
	leaq	16(%rbx,%rcx), %rdx
	leaq	(%rbx,%rcx), %rsi
	movq	%rsi, cont_CURRENTBINDING(%rip)
	movq	$0, 8(%rbx,%rcx)
	movq	%rbx, (%rdx)
	movq	cont_LASTBINDING(%rip), %rdx
	movq	%rdx, 24(%rbx,%rcx)
	movq	%rsi, cont_LASTBINDING(%rip)
	incl	cont_BINDINGS(%rip)
.LBB2_5:                                #   in Loop: Header=BB2_3 Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB2_3
	jmp	.LBB2_6
.LBB2_10:                               # %._crit_edge25
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	st_CloseUsedVariables, .Lfunc_end2-st_CloseUsedVariables
	.cfi_endproc

	.globl	st_IndexDelete
	.p2align	4, 0x90
	.type	st_IndexDelete,@function
st_IndexDelete:                         # @st_IndexDelete
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 16
.Lcfi20:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB3_5
# BB#1:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB3_3
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB3_2
	jmp	.LBB3_4
.LBB3_3:
	movq	8(%rbx), %rdi
	movl	$st_IndexDelete, %esi
	callq	list_DeleteWithElement
.LBB3_4:                                # %list_Delete.exit
	movq	(%rbx), %rdi
	callq	subst_Delete
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rbx, (%rax)
.LBB3_5:
	popq	%rbx
	retq
.Lfunc_end3:
	.size	st_IndexDelete, .Lfunc_end3-st_IndexDelete
	.cfi_endproc

	.globl	st_EntryDelete
	.p2align	4, 0x90
	.type	st_EntryDelete,@function
st_EntryDelete:                         # @st_EntryDelete
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 64
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %rbp
	movq	%rsi, %r15
	movq	%rdi, %r14
	callq	cont_Check
	leaq	64032(%r13), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	%rbp, 64040(%r13)
	movq	%r13, 64048(%r13)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 64056(%r13)
	movq	%rax, cont_LASTBINDING(%rip)
	movl	cont_BINDINGS(%rip), %eax
	incl	%eax
	movl	%eax, cont_BINDINGS(%rip)
	movl	$0, 4(%rsp)
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB4_1
# BB#2:                                 # %.lr.ph62.preheader
	movl	cont_STACKPOINTER(%rip), %ecx
	leaq	4(%rsp), %r12
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph62
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_16 Depth 2
	movq	8(%rbx), %rbp
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	(%rbp), %rsi
	movq	%r13, %rdi
	callq	subst_Variation
	testl	%eax, %eax
	je	.LBB4_13
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	st_EntryDeleteHelp
	movq	%rax, 8(%rbx)
	movl	4(%rsp), %ebp
	testl	%ebp, %ebp
	jne	.LBB4_5
.LBB4_13:                               #   in Loop: Header=BB4_3 Depth=1
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB4_14
# BB#15:                                # %.lr.ph.i52.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_16:                               # %.lr.ph.i52
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB4_16
	jmp	.LBB4_17
	.p2align	4, 0x90
.LBB4_14:                               #   in Loop: Header=BB4_3 Depth=1
	movl	%ecx, %eax
.LBB4_17:                               # %._crit_edge.i
                                        #   in Loop: Header=BB4_3 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rdx
	xorl	%ebp, %ebp
	testq	%rdx, %rdx
	movl	$0, %ecx
	je	.LBB4_19
# BB#18:                                #   in Loop: Header=BB4_3 Depth=1
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB4_19:                               #   in Loop: Header=BB4_3 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_3
	jmp	.LBB4_20
.LBB4_1:
	xorl	%ebp, %ebp
	jmp	.LBB4_20
.LBB4_5:
	movq	8(%r14), %rdi
	xorl	%esi, %esi
	callq	list_PointerDeleteElement
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.LBB4_12
# BB#6:
	movq	8(%rax), %rdx
	movzwl	24(%rdx), %ecx
	movw	%cx, 24(%r14)
	movzwl	26(%rdx), %edx
	movw	%dx, 26(%r14)
	jmp	.LBB4_7
	.p2align	4, 0x90
.LBB4_11:                               #   in Loop: Header=BB4_7 Depth=1
	movw	%si, 26(%r14)
	movl	%esi, %edx
.LBB4_7:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB4_20
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB4_7 Depth=1
	movq	8(%rax), %rsi
	movzwl	24(%rsi), %edi
	cmpw	%cx, %di
	jbe	.LBB4_10
# BB#9:                                 #   in Loop: Header=BB4_7 Depth=1
	movw	%di, 24(%r14)
	movl	%edi, %ecx
.LBB4_10:                               #   in Loop: Header=BB4_7 Depth=1
	movzwl	26(%rsi), %esi
	cmpw	%dx, %si
	jae	.LBB4_7
	jmp	.LBB4_11
.LBB4_12:
	movl	$0, 24(%r14)
.LBB4_20:                               # %cont_BackTrack.exit
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB4_23
# BB#21:                                # %.lr.ph.preheader.i
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_22:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB4_22
.LBB4_23:                               # %cont_Reset.exit
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	st_EntryDelete, .Lfunc_end4-st_EntryDelete
	.cfi_endproc

	.p2align	4, 0x90
	.type	st_EntryDeleteHelp,@function
st_EntryDeleteHelp:                     # @st_EntryDeleteHelp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 64
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	$0, 16(%r14)
	je	.LBB5_3
# BB#1:
	leaq	16(%r14), %rbx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	list_DeleteFromList
	movl	%eax, (%r13)
	cmpq	$0, (%rbx)
	jne	.LBB5_26
# BB#2:
	movq	(%r14), %rdi
	callq	subst_Delete
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%r14, (%rax)
	xorl	%r14d, %r14d
	jmp	.LBB5_26
.LBB5_3:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.LBB5_26
# BB#4:                                 # %.lr.ph71.preheader
	leaq	8(%r14), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	cont_BINDINGS(%rip), %eax
	movl	cont_STACKPOINTER(%rip), %ecx
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph71
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_21 Depth 2
	movq	8(%r12), %rbp
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	(%rbp), %rsi
	movq	%rbx, %rdi
	callq	subst_Variation
	testl	%eax, %eax
	je	.LBB5_18
# BB#6:                                 #   in Loop: Header=BB5_5 Depth=1
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r13, %rcx
	callq	st_EntryDeleteHelp
	movq	%rax, 8(%r12)
	cmpl	$0, (%r13)
	jne	.LBB5_7
.LBB5_18:                               #   in Loop: Header=BB5_5 Depth=1
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB5_19
# BB#20:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB5_5 Depth=1
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB5_21:                               # %.lr.ph.i
                                        #   Parent Loop BB5_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB5_21
	jmp	.LBB5_22
	.p2align	4, 0x90
.LBB5_19:                               #   in Loop: Header=BB5_5 Depth=1
	movl	%ecx, %eax
.LBB5_22:                               # %._crit_edge.i
                                        #   in Loop: Header=BB5_5 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB5_23
# BB#24:                                #   in Loop: Header=BB5_5 Depth=1
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB5_25
	.p2align	4, 0x90
.LBB5_23:                               #   in Loop: Header=BB5_5 Depth=1
	xorl	%ecx, %ecx
.LBB5_25:                               #   in Loop: Header=BB5_5 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB5_5
	jmp	.LBB5_26
.LBB5_7:
	xorl	%esi, %esi
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, %rdi
	callq	list_DeleteFromList
	testl	%eax, %eax
	movq	(%rbx), %rcx
	je	.LBB5_12
# BB#8:
	cmpq	$0, (%rcx)
	je	.LBB5_9
.LBB5_12:                               # %._crit_edge
	movq	8(%rcx), %rdx
	movzwl	24(%rdx), %eax
	movw	%ax, 24(%r14)
	movzwl	26(%rdx), %edx
	movw	%dx, 26(%r14)
	jmp	.LBB5_13
	.p2align	4, 0x90
.LBB5_17:                               #   in Loop: Header=BB5_13 Depth=1
	movw	%si, 26(%r14)
	movl	%esi, %edx
.LBB5_13:                               # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB5_26
# BB#14:                                # %.lr.ph
                                        #   in Loop: Header=BB5_13 Depth=1
	movq	8(%rcx), %rsi
	movzwl	24(%rsi), %edi
	cmpw	%ax, %di
	jbe	.LBB5_16
# BB#15:                                #   in Loop: Header=BB5_13 Depth=1
	movw	%di, 24(%r14)
	movl	%edi, %eax
.LBB5_16:                               #   in Loop: Header=BB5_13 Depth=1
	movzwl	26(%rsi), %esi
	cmpw	%dx, %si
	jae	.LBB5_13
	jmp	.LBB5_17
.LBB5_9:
	movq	8(%rcx), %rbx
	testq	%rcx, %rcx
	je	.LBB5_11
# BB#10:                                # %st_NodeMergeWithSon.exit.loopexit
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rcx, (%rax)
.LBB5_11:                               # %st_NodeMergeWithSon.exit
	movq	(%rbx), %rdi
	movq	(%r14), %rsi
	callq	subst_Merge
	movq	%rax, (%r14)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r14)
	movq	8(%rbx), %rax
	movq	%rax, 8(%r14)
	movzwl	24(%rbx), %eax
	movw	%ax, 24(%r14)
	movzwl	26(%rbx), %eax
	movw	%ax, 26(%r14)
	movq	(%rbx), %rdi
	callq	subst_Delete
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rbx, (%rax)
.LBB5_26:                               # %cont_BackTrack.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	st_EntryDeleteHelp, .Lfunc_end5-st_EntryDeleteHelp
	.cfi_endproc

	.globl	st_GetUnifier
	.p2align	4, 0x90
	.type	st_GetUnifier,@function
st_GetUnifier:                          # @st_GetUnifier
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 64
.Lcfi54:
	.cfi_offset %rbx, -56
.Lcfi55:
	.cfi_offset %r12, -48
.Lcfi56:
	.cfi_offset %r13, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r13
	callq	cont_Check
	leaq	64032(%r13), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	%r14, 64040(%r13)
	movq	%r15, 64048(%r13)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 64056(%r13)
	movq	%rax, cont_LASTBINDING(%rip)
	movl	cont_BINDINGS(%rip), %eax
	incl	%eax
	movl	%eax, cont_BINDINGS(%rip)
	movq	8(%r12), %rbp
	movl	stack_POINTER(%rip), %r15d
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	xorl	%r14d, %r14d
	jmp	.LBB6_19
	.p2align	4, 0x90
.LBB6_1:                                #   in Loop: Header=BB6_19 Depth=1
	decl	%ecx
	movl	%ecx, stack_POINTER(%rip)
	leaq	stack_STACK(,%rcx,8), %rbx
.LBB6_2:                                # %.sink.split.i
                                        #   in Loop: Header=BB6_19 Depth=1
	movq	(%rbx), %rbp
.LBB6_3:                                #   in Loop: Header=BB6_19 Depth=1
	movq	8(%rbp), %rbx
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	callq	subst_Unify
	testl	%eax, %eax
	je	.LBB6_13
# BB#4:                                 #   in Loop: Header=BB6_19 Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_10
# BB#5:                                 #   in Loop: Header=BB6_19 Depth=1
	callq	list_Copy
	testq	%r14, %r14
	je	.LBB6_12
# BB#6:                                 # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB6_19 Depth=1
	movq	%rax, %rdx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB6_7:                                # %.preheader.i.i
                                        #   Parent Loop BB6_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB6_7
# BB#8:                                 #   in Loop: Header=BB6_19 Depth=1
	movq	%r14, (%rcx)
	movq	%rax, %r14
	jmp	.LBB6_14
	.p2align	4, 0x90
.LBB6_10:                               #   in Loop: Header=BB6_19 Depth=1
	movq	(%rbp), %rax
	testq	%rax, %rax
	je	.LBB6_17
# BB#11:                                #   in Loop: Header=BB6_19 Depth=1
	movl	stack_POINTER(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rcx,8)
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	jmp	.LBB6_18
.LBB6_12:                               #   in Loop: Header=BB6_19 Depth=1
	movq	%rax, %r14
.LBB6_13:                               # %list_Append.exit.i
                                        #   in Loop: Header=BB6_19 Depth=1
	xorps	%xmm0, %xmm0
.LBB6_14:                               # %list_Append.exit.i
                                        #   in Loop: Header=BB6_19 Depth=1
	movq	(%rbp), %rbp
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB6_19
# BB#15:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB6_19 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB6_16:                               # %.lr.ph.i.i
                                        #   Parent Loop BB6_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB6_16
	jmp	.LBB6_19
.LBB6_17:                               #   in Loop: Header=BB6_19 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	movl	cont_BINDINGS(%rip), %ecx
	addl	%ecx, cont_STACK-4(,%rax,4)
.LBB6_18:                               #   in Loop: Header=BB6_19 Depth=1
	movl	$0, cont_BINDINGS(%rip)
	addq	$8, %rbx
	jmp	.LBB6_2
	.p2align	4, 0x90
.LBB6_19:                               # %cont_BackTrackAndStart.exit.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_22 Depth 2
                                        #     Child Loop BB6_7 Depth 2
                                        #     Child Loop BB6_16 Depth 2
	testq	%rbp, %rbp
	jne	.LBB6_3
# BB#20:                                #   in Loop: Header=BB6_19 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	testl	%ecx, %ecx
	jle	.LBB6_23
# BB#21:                                # %.lr.ph.i29.i.preheader
                                        #   in Loop: Header=BB6_19 Depth=1
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB6_22:                               # %.lr.ph.i29.i
                                        #   Parent Loop BB6_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB6_22
	jmp	.LBB6_24
.LBB6_23:                               #   in Loop: Header=BB6_19 Depth=1
	movl	%ecx, %eax
.LBB6_24:                               # %cont_StopAndBackTrack.exit.i
                                        #   in Loop: Header=BB6_19 Depth=1
	movl	stack_POINTER(%rip), %ecx
	cmpl	%r15d, %ecx
	jne	.LBB6_1
# BB#25:                                # %st_TraverseTreeUnifier.exit
	movq	cont_LASTBINDING(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB6_28
# BB#26:                                # %.lr.ph.i.preheader
	decl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB6_27:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	movl	%eax, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rcx
	decl	%eax
	testq	%rcx, %rcx
	jne	.LBB6_27
.LBB6_28:                               # %cont_Reset.exit
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	st_GetUnifier, .Lfunc_end6-st_GetUnifier
	.cfi_endproc

	.globl	st_GetGen
	.p2align	4, 0x90
	.type	st_GetGen,@function
st_GetGen:                              # @st_GetGen
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 48
.Lcfi65:
	.cfi_offset %rbx, -48
.Lcfi66:
	.cfi_offset %r12, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	callq	cont_Check
	movq	cont_INSTANCECONTEXT(%rip), %rax
	leaq	64032(%r12), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	%r14, 64040(%r12)
	movq	%rax, 64048(%r12)
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, 64056(%r12)
	movq	%rcx, cont_LASTBINDING(%rip)
	movl	cont_BINDINGS(%rip), %eax
	incl	%eax
	movl	%eax, cont_BINDINGS(%rip)
	movq	8(%r15), %rbp
	movl	stack_POINTER(%rip), %r15d
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	xorl	%r14d, %r14d
	jmp	.LBB7_19
	.p2align	4, 0x90
.LBB7_1:                                #   in Loop: Header=BB7_19 Depth=1
	decl	%ecx
	movl	%ecx, stack_POINTER(%rip)
	leaq	stack_STACK(,%rcx,8), %rbx
.LBB7_2:                                # %.sink.split.i
                                        #   in Loop: Header=BB7_19 Depth=1
	movq	(%rbx), %rbp
.LBB7_3:                                #   in Loop: Header=BB7_19 Depth=1
	movq	8(%rbp), %rbx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	callq	subst_Match
	testl	%eax, %eax
	je	.LBB7_13
# BB#4:                                 #   in Loop: Header=BB7_19 Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_10
# BB#5:                                 #   in Loop: Header=BB7_19 Depth=1
	callq	list_Copy
	testq	%r14, %r14
	je	.LBB7_12
# BB#6:                                 # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB7_19 Depth=1
	movq	%rax, %rdx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB7_7:                                # %.preheader.i.i
                                        #   Parent Loop BB7_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB7_7
# BB#8:                                 #   in Loop: Header=BB7_19 Depth=1
	movq	%r14, (%rcx)
	movq	%rax, %r14
	jmp	.LBB7_14
	.p2align	4, 0x90
.LBB7_10:                               #   in Loop: Header=BB7_19 Depth=1
	movq	(%rbp), %rax
	testq	%rax, %rax
	je	.LBB7_17
# BB#11:                                #   in Loop: Header=BB7_19 Depth=1
	movl	stack_POINTER(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rcx,8)
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	jmp	.LBB7_18
.LBB7_12:                               #   in Loop: Header=BB7_19 Depth=1
	movq	%rax, %r14
.LBB7_13:                               # %list_Append.exit.i
                                        #   in Loop: Header=BB7_19 Depth=1
	xorps	%xmm0, %xmm0
.LBB7_14:                               # %list_Append.exit.i
                                        #   in Loop: Header=BB7_19 Depth=1
	movq	(%rbp), %rbp
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB7_19
# BB#15:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB7_19 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB7_16:                               # %.lr.ph.i.i
                                        #   Parent Loop BB7_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB7_16
	jmp	.LBB7_19
.LBB7_17:                               #   in Loop: Header=BB7_19 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	movl	cont_BINDINGS(%rip), %ecx
	addl	%ecx, cont_STACK-4(,%rax,4)
.LBB7_18:                               #   in Loop: Header=BB7_19 Depth=1
	movl	$0, cont_BINDINGS(%rip)
	addq	$8, %rbx
	jmp	.LBB7_2
	.p2align	4, 0x90
.LBB7_19:                               # %cont_BackTrackAndStart.exit.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_22 Depth 2
                                        #     Child Loop BB7_7 Depth 2
                                        #     Child Loop BB7_16 Depth 2
	testq	%rbp, %rbp
	jne	.LBB7_3
# BB#20:                                #   in Loop: Header=BB7_19 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	testl	%ecx, %ecx
	jle	.LBB7_23
# BB#21:                                # %.lr.ph.i29.i.preheader
                                        #   in Loop: Header=BB7_19 Depth=1
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB7_22:                               # %.lr.ph.i29.i
                                        #   Parent Loop BB7_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB7_22
	jmp	.LBB7_24
.LBB7_23:                               #   in Loop: Header=BB7_19 Depth=1
	movl	%ecx, %eax
.LBB7_24:                               # %cont_StopAndBackTrack.exit.i
                                        #   in Loop: Header=BB7_19 Depth=1
	movl	stack_POINTER(%rip), %ecx
	cmpl	%r15d, %ecx
	jne	.LBB7_1
# BB#25:                                # %st_TraverseTreeGen.exit
	movq	cont_LASTBINDING(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB7_28
# BB#26:                                # %.lr.ph.i.preheader
	decl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB7_27:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	movl	%eax, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rcx
	decl	%eax
	testq	%rcx, %rcx
	jne	.LBB7_27
.LBB7_28:                               # %cont_Reset.exit
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	st_GetGen, .Lfunc_end7-st_GetGen
	.cfi_endproc

	.globl	st_GetGenPreTest
	.p2align	4, 0x90
	.type	st_GetGenPreTest,@function
st_GetGenPreTest:                       # @st_GetGenPreTest
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi76:
	.cfi_def_cfa_offset 64
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movq	%rdi, %r12
	callq	cont_Check
	movq	cont_INSTANCECONTEXT(%rip), %rax
	leaq	64032(%r12), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	%rbp, 64040(%r12)
	movq	%rax, 64048(%r12)
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, 64056(%r12)
	movq	%rcx, cont_LASTBINDING(%rip)
	incl	cont_BINDINGS(%rip)
	movq	%rbp, %rdi
	callq	term_ComputeSize
	movl	%eax, %r13d
	movq	8(%r14), %rbx
	movl	stack_POINTER(%rip), %r15d
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	xorl	%r14d, %r14d
	xorps	%xmm0, %xmm0
	jmp	.LBB8_1
	.p2align	4, 0x90
.LBB8_6:                                #   in Loop: Header=BB8_1 Depth=1
	decl	%ecx
	movl	%ecx, stack_POINTER(%rip)
	leaq	stack_STACK(,%rcx,8), %rbp
.LBB8_7:                                # %.sink.split.i
                                        #   in Loop: Header=BB8_1 Depth=1
	movq	(%rbp), %rbx
	.p2align	4, 0x90
.LBB8_8:                                #   in Loop: Header=BB8_1 Depth=1
	movq	8(%rbx), %rbp
	cmpw	%r13w, 26(%rbp)
	jbe	.LBB8_9
.LBB8_17:                               # %.critedge.i
                                        #   in Loop: Header=BB8_1 Depth=1
	movq	(%rbx), %rbx
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB8_1
# BB#18:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB8_1 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB8_19:                               # %.lr.ph.i.i
                                        #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB8_19
	jmp	.LBB8_1
	.p2align	4, 0x90
.LBB8_9:                                #   in Loop: Header=BB8_1 Depth=1
	movq	(%rbp), %rsi
	movq	%r12, %rdi
	callq	subst_Match
	testl	%eax, %eax
	je	.LBB8_10
# BB#11:                                #   in Loop: Header=BB8_1 Depth=1
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_20
# BB#12:                                #   in Loop: Header=BB8_1 Depth=1
	callq	list_Copy
	testq	%r14, %r14
	je	.LBB8_13
# BB#14:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB8_1 Depth=1
	movq	%rax, %rdx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB8_15:                               # %.preheader.i.i
                                        #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB8_15
# BB#16:                                #   in Loop: Header=BB8_1 Depth=1
	movq	%r14, (%rcx)
	movq	%rax, %r14
	jmp	.LBB8_17
	.p2align	4, 0x90
.LBB8_10:                               #   in Loop: Header=BB8_1 Depth=1
	xorps	%xmm0, %xmm0
	jmp	.LBB8_17
.LBB8_20:                               #   in Loop: Header=BB8_1 Depth=1
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.LBB8_22
# BB#21:                                #   in Loop: Header=BB8_1 Depth=1
	movl	stack_POINTER(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rcx,8)
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	jmp	.LBB8_23
.LBB8_13:                               #   in Loop: Header=BB8_1 Depth=1
	movq	%rax, %r14
	xorps	%xmm0, %xmm0
	jmp	.LBB8_17
.LBB8_22:                               #   in Loop: Header=BB8_1 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	movl	cont_BINDINGS(%rip), %ecx
	addl	%ecx, cont_STACK-4(,%rax,4)
.LBB8_23:                               #   in Loop: Header=BB8_1 Depth=1
	xorps	%xmm0, %xmm0
	movl	$0, cont_BINDINGS(%rip)
	addq	$8, %rbp
	jmp	.LBB8_7
	.p2align	4, 0x90
.LBB8_1:                                # %cont_BackTrackAndStart.exit.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_4 Depth 2
                                        #     Child Loop BB8_15 Depth 2
                                        #     Child Loop BB8_19 Depth 2
	testq	%rbx, %rbx
	jne	.LBB8_8
# BB#2:                                 #   in Loop: Header=BB8_1 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	testl	%ecx, %ecx
	jle	.LBB8_3
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph.i32.i
                                        #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB8_4
	jmp	.LBB8_5
.LBB8_3:                                #   in Loop: Header=BB8_1 Depth=1
	movl	%ecx, %eax
.LBB8_5:                                # %cont_StopAndBackTrack.exit.i
                                        #   in Loop: Header=BB8_1 Depth=1
	movl	stack_POINTER(%rip), %ecx
	cmpl	%r15d, %ecx
	jne	.LBB8_6
# BB#24:                                # %st_TraverseTreeGenPreTest.exit
	movq	cont_LASTBINDING(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB8_27
# BB#25:                                # %.lr.ph.i.preheader
	decl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB8_26:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	movl	%eax, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rcx
	decl	%eax
	testq	%rcx, %rcx
	jne	.LBB8_26
.LBB8_27:                               # %cont_Reset.exit
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	st_GetGenPreTest, .Lfunc_end8-st_GetGenPreTest
	.cfi_endproc

	.globl	st_GetInstance
	.p2align	4, 0x90
	.type	st_GetInstance,@function
st_GetInstance:                         # @st_GetInstance
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 48
.Lcfi88:
	.cfi_offset %rbx, -48
.Lcfi89:
	.cfi_offset %r12, -40
.Lcfi90:
	.cfi_offset %r14, -32
.Lcfi91:
	.cfi_offset %r15, -24
.Lcfi92:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	callq	cont_Check
	leaq	64032(%r12), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	%r14, 64040(%r12)
	movq	%r12, 64048(%r12)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 64056(%r12)
	movq	%rax, cont_LASTBINDING(%rip)
	movl	cont_BINDINGS(%rip), %eax
	incl	%eax
	movl	%eax, cont_BINDINGS(%rip)
	movq	8(%r15), %rbp
	movl	stack_POINTER(%rip), %r15d
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	xorl	%r14d, %r14d
	jmp	.LBB9_19
	.p2align	4, 0x90
.LBB9_1:                                #   in Loop: Header=BB9_19 Depth=1
	decl	%ecx
	movl	%ecx, stack_POINTER(%rip)
	leaq	stack_STACK(,%rcx,8), %rbx
.LBB9_2:                                # %.sink.split.i
                                        #   in Loop: Header=BB9_19 Depth=1
	movq	(%rbx), %rbp
.LBB9_3:                                #   in Loop: Header=BB9_19 Depth=1
	movq	8(%rbp), %rbx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	callq	subst_MatchReverse
	testl	%eax, %eax
	je	.LBB9_13
# BB#4:                                 #   in Loop: Header=BB9_19 Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_10
# BB#5:                                 #   in Loop: Header=BB9_19 Depth=1
	callq	list_Copy
	testq	%r14, %r14
	je	.LBB9_12
# BB#6:                                 # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB9_19 Depth=1
	movq	%rax, %rdx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB9_7:                                # %.preheader.i.i
                                        #   Parent Loop BB9_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB9_7
# BB#8:                                 #   in Loop: Header=BB9_19 Depth=1
	movq	%r14, (%rcx)
	movq	%rax, %r14
	jmp	.LBB9_14
	.p2align	4, 0x90
.LBB9_10:                               #   in Loop: Header=BB9_19 Depth=1
	movq	(%rbp), %rax
	testq	%rax, %rax
	je	.LBB9_17
# BB#11:                                #   in Loop: Header=BB9_19 Depth=1
	movl	stack_POINTER(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rcx,8)
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	jmp	.LBB9_18
.LBB9_12:                               #   in Loop: Header=BB9_19 Depth=1
	movq	%rax, %r14
.LBB9_13:                               # %list_Append.exit.i
                                        #   in Loop: Header=BB9_19 Depth=1
	xorps	%xmm0, %xmm0
.LBB9_14:                               # %list_Append.exit.i
                                        #   in Loop: Header=BB9_19 Depth=1
	movq	(%rbp), %rbp
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB9_19
# BB#15:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB9_19 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB9_16:                               # %.lr.ph.i.i
                                        #   Parent Loop BB9_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB9_16
	jmp	.LBB9_19
.LBB9_17:                               #   in Loop: Header=BB9_19 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	movl	cont_BINDINGS(%rip), %ecx
	addl	%ecx, cont_STACK-4(,%rax,4)
.LBB9_18:                               #   in Loop: Header=BB9_19 Depth=1
	movl	$0, cont_BINDINGS(%rip)
	addq	$8, %rbx
	jmp	.LBB9_2
	.p2align	4, 0x90
.LBB9_19:                               # %cont_BackTrackAndStart.exit.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_22 Depth 2
                                        #     Child Loop BB9_7 Depth 2
                                        #     Child Loop BB9_16 Depth 2
	testq	%rbp, %rbp
	jne	.LBB9_3
# BB#20:                                #   in Loop: Header=BB9_19 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	testl	%ecx, %ecx
	jle	.LBB9_23
# BB#21:                                # %.lr.ph.i29.i.preheader
                                        #   in Loop: Header=BB9_19 Depth=1
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB9_22:                               # %.lr.ph.i29.i
                                        #   Parent Loop BB9_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB9_22
	jmp	.LBB9_24
.LBB9_23:                               #   in Loop: Header=BB9_19 Depth=1
	movl	%ecx, %eax
.LBB9_24:                               # %cont_StopAndBackTrack.exit.i
                                        #   in Loop: Header=BB9_19 Depth=1
	movl	stack_POINTER(%rip), %ecx
	cmpl	%r15d, %ecx
	jne	.LBB9_1
# BB#25:                                # %st_TraverseTreeInstance.exit
	movq	cont_LASTBINDING(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB9_28
# BB#26:                                # %.lr.ph.i.preheader
	decl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB9_27:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	movl	%eax, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rcx
	decl	%eax
	testq	%rcx, %rcx
	jne	.LBB9_27
.LBB9_28:                               # %cont_Reset.exit
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	st_GetInstance, .Lfunc_end9-st_GetInstance
	.cfi_endproc

	.globl	st_GetInstancePreTest
	.p2align	4, 0x90
	.type	st_GetInstancePreTest,@function
st_GetInstancePreTest:                  # @st_GetInstancePreTest
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi96:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi97:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 64
.Lcfi100:
	.cfi_offset %rbx, -56
.Lcfi101:
	.cfi_offset %r12, -48
.Lcfi102:
	.cfi_offset %r13, -40
.Lcfi103:
	.cfi_offset %r14, -32
.Lcfi104:
	.cfi_offset %r15, -24
.Lcfi105:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movq	%rdi, %r12
	callq	cont_Check
	leaq	64032(%r12), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	%rbp, 64040(%r12)
	movq	%r12, 64048(%r12)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 64056(%r12)
	movq	%rax, cont_LASTBINDING(%rip)
	incl	cont_BINDINGS(%rip)
	movq	%rbp, %rdi
	callq	term_ComputeSize
	movl	%eax, %r13d
	movq	8(%r14), %rbx
	movl	stack_POINTER(%rip), %r15d
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	xorl	%r14d, %r14d
	xorps	%xmm0, %xmm0
	jmp	.LBB10_1
	.p2align	4, 0x90
.LBB10_6:                               #   in Loop: Header=BB10_1 Depth=1
	decl	%ecx
	movl	%ecx, stack_POINTER(%rip)
	leaq	stack_STACK(,%rcx,8), %rbp
.LBB10_7:                               # %.sink.split.i
                                        #   in Loop: Header=BB10_1 Depth=1
	movq	(%rbp), %rbx
	.p2align	4, 0x90
.LBB10_8:                               #   in Loop: Header=BB10_1 Depth=1
	movq	8(%rbx), %rbp
	cmpw	%r13w, 24(%rbp)
	jae	.LBB10_9
.LBB10_17:                              # %.critedge.i
                                        #   in Loop: Header=BB10_1 Depth=1
	movq	(%rbx), %rbx
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB10_1
# BB#18:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB10_1 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB10_19:                              # %.lr.ph.i.i
                                        #   Parent Loop BB10_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB10_19
	jmp	.LBB10_1
	.p2align	4, 0x90
.LBB10_9:                               #   in Loop: Header=BB10_1 Depth=1
	movq	(%rbp), %rsi
	movq	%r12, %rdi
	callq	subst_MatchReverse
	testl	%eax, %eax
	je	.LBB10_10
# BB#11:                                #   in Loop: Header=BB10_1 Depth=1
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_20
# BB#12:                                #   in Loop: Header=BB10_1 Depth=1
	callq	list_Copy
	testq	%r14, %r14
	je	.LBB10_13
# BB#14:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB10_1 Depth=1
	movq	%rax, %rdx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB10_15:                              # %.preheader.i.i
                                        #   Parent Loop BB10_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB10_15
# BB#16:                                #   in Loop: Header=BB10_1 Depth=1
	movq	%r14, (%rcx)
	movq	%rax, %r14
	jmp	.LBB10_17
	.p2align	4, 0x90
.LBB10_10:                              #   in Loop: Header=BB10_1 Depth=1
	xorps	%xmm0, %xmm0
	jmp	.LBB10_17
.LBB10_20:                              #   in Loop: Header=BB10_1 Depth=1
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.LBB10_22
# BB#21:                                #   in Loop: Header=BB10_1 Depth=1
	movl	stack_POINTER(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rcx,8)
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	jmp	.LBB10_23
.LBB10_13:                              #   in Loop: Header=BB10_1 Depth=1
	movq	%rax, %r14
	xorps	%xmm0, %xmm0
	jmp	.LBB10_17
.LBB10_22:                              #   in Loop: Header=BB10_1 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	movl	cont_BINDINGS(%rip), %ecx
	addl	%ecx, cont_STACK-4(,%rax,4)
.LBB10_23:                              #   in Loop: Header=BB10_1 Depth=1
	xorps	%xmm0, %xmm0
	movl	$0, cont_BINDINGS(%rip)
	addq	$8, %rbp
	jmp	.LBB10_7
	.p2align	4, 0x90
.LBB10_1:                               # %cont_BackTrackAndStart.exit.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_4 Depth 2
                                        #     Child Loop BB10_15 Depth 2
                                        #     Child Loop BB10_19 Depth 2
	testq	%rbx, %rbx
	jne	.LBB10_8
# BB#2:                                 #   in Loop: Header=BB10_1 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	testl	%ecx, %ecx
	jle	.LBB10_3
	.p2align	4, 0x90
.LBB10_4:                               # %.lr.ph.i32.i
                                        #   Parent Loop BB10_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB10_4
	jmp	.LBB10_5
.LBB10_3:                               #   in Loop: Header=BB10_1 Depth=1
	movl	%ecx, %eax
.LBB10_5:                               # %cont_StopAndBackTrack.exit.i
                                        #   in Loop: Header=BB10_1 Depth=1
	movl	stack_POINTER(%rip), %ecx
	cmpl	%r15d, %ecx
	jne	.LBB10_6
# BB#24:                                # %st_TraverseTreeInstancePreTest.exit
	movq	cont_LASTBINDING(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB10_27
# BB#25:                                # %.lr.ph.i.preheader
	decl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB10_26:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	movl	%eax, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rcx
	decl	%eax
	testq	%rcx, %rcx
	jne	.LBB10_26
.LBB10_27:                              # %cont_Reset.exit
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	st_GetInstancePreTest, .Lfunc_end10-st_GetInstancePreTest
	.cfi_endproc

	.globl	st_CancelExistRetrieval
	.p2align	4, 0x90
	.type	st_CancelExistRetrieval,@function
st_CancelExistRetrieval:                # @st_CancelExistRetrieval
	.cfi_startproc
# BB#0:
	cmpl	$0, st_CURRENT_RETRIEVAL(%rip)
	je	.LBB11_7
# BB#1:
	movl	st_STACKSAVE(%rip), %eax
	movl	%eax, st_STACKPOINTER(%rip)
	movb	st_WHICH_CONTEXTS(%rip), %al
	testb	%al, %al
	jne	.LBB11_6
# BB#2:
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB11_5
# BB#3:                                 # %.lr.ph.preheader.i
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB11_4:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB11_4
.LBB11_5:                               # %cont_Reset.exit
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
.LBB11_6:
	movl	$0, st_CURRENT_RETRIEVAL(%rip)
	movb	$1, st_WHICH_CONTEXTS(%rip)
	movq	$0, st_INDEX_CONTEXT(%rip)
	movw	$0, st_EXIST_MINMAX(%rip)
.LBB11_7:
	retq
.Lfunc_end11:
	.size	st_CancelExistRetrieval, .Lfunc_end11-st_CancelExistRetrieval
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_DumpCore,@function
misc_DumpCore:                          # @misc_DumpCore
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi106:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end12:
	.size	misc_DumpCore, .Lfunc_end12-misc_DumpCore
	.cfi_endproc

	.globl	st_ExistUnifier
	.p2align	4, 0x90
	.type	st_ExistUnifier,@function
st_ExistUnifier:                        # @st_ExistUnifier
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi107:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi109:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi111:
	.cfi_def_cfa_offset 48
.Lcfi112:
	.cfi_offset %rbx, -40
.Lcfi113:
	.cfi_offset %r12, -32
.Lcfi114:
	.cfi_offset %r14, -24
.Lcfi115:
	.cfi_offset %r15, -16
	movq	%rcx, %r12
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	cont_Check
	testq	%r14, %r14
	je	.LBB13_11
# BB#1:
	cmpq	$0, 16(%r14)
	jne	.LBB13_3
# BB#2:                                 # %st_Exist.exit
	cmpq	$0, 8(%r14)
	je	.LBB13_11
.LBB13_3:                               # %st_Exist.exit.thread13
	movl	$1, st_CURRENT_RETRIEVAL(%rip)
	movb	$0, st_WHICH_CONTEXTS(%rip)
	movq	%rbx, st_INDEX_CONTEXT(%rip)
	movslq	st_STACKPOINTER(%rip), %rax
	movl	%eax, st_STACKSAVE(%rip)
	leaq	64032(%rbx), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	%r12, 64040(%rbx)
	movq	%r15, 64048(%rbx)
	movq	cont_LASTBINDING(%rip), %rdx
	movq	%rdx, 64056(%rbx)
	movq	%rcx, cont_LASTBINDING(%rip)
	movl	cont_BINDINGS(%rip), %ecx
	incl	%ecx
	movslq	cont_STACKPOINTER(%rip), %rdx
	leaq	1(%rdx), %rsi
	movl	%esi, cont_STACKPOINTER(%rip)
	movl	%ecx, cont_STACK(,%rdx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	8(%r14), %rcx
	leal	1(%rax), %esi
	movl	%esi, st_STACKPOINTER(%rip)
	movq	%rcx, st_STACK(,%rax,8)
	leal	2(%rdx), %eax
	movl	%eax, cont_STACKPOINTER(%rip)
	movl	$0, cont_STACK+4(,%rdx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	%rbx, %rdi
	callq	st_TraverseForExistUnifier
	testq	%rax, %rax
	jne	.LBB13_12
# BB#4:
	cmpl	$0, st_CURRENT_RETRIEVAL(%rip)
	je	.LBB13_11
# BB#5:
	movl	st_STACKSAVE(%rip), %eax
	movl	%eax, st_STACKPOINTER(%rip)
	movb	st_WHICH_CONTEXTS(%rip), %al
	testb	%al, %al
	jne	.LBB13_10
# BB#6:
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB13_9
# BB#7:                                 # %.lr.ph.preheader.i.i
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB13_8:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB13_8
.LBB13_9:                               # %cont_Reset.exit.i
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
.LBB13_10:
	movl	$0, st_CURRENT_RETRIEVAL(%rip)
	movb	$1, st_WHICH_CONTEXTS(%rip)
	movq	$0, st_INDEX_CONTEXT(%rip)
	movw	$0, st_EXIST_MINMAX(%rip)
.LBB13_11:                              # %st_CancelExistRetrieval.exit
	xorl	%eax, %eax
.LBB13_12:                              # %st_CancelExistRetrieval.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	st_ExistUnifier, .Lfunc_end13-st_ExistUnifier
	.cfi_endproc

	.p2align	4, 0x90
	.type	st_TraverseForExistUnifier,@function
st_TraverseForExistUnifier:             # @st_TraverseForExistUnifier
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi116:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi118:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi120:
	.cfi_def_cfa_offset 48
.Lcfi121:
	.cfi_offset %rbx, -40
.Lcfi122:
	.cfi_offset %r12, -32
.Lcfi123:
	.cfi_offset %r14, -24
.Lcfi124:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	xorps	%xmm0, %xmm0
	xorl	%ebx, %ebx
	jmp	.LBB14_1
	.p2align	4, 0x90
.LBB14_15:                              #   in Loop: Header=BB14_1 Depth=1
	addq	$8, %r12
	jmp	.LBB14_7
	.p2align	4, 0x90
.LBB14_1:                               # %cont_BackTrackAndStart.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_4 Depth 2
                                        #     Child Loop BB14_18 Depth 2
	testq	%rbx, %rbx
	jne	.LBB14_8
# BB#2:                                 #   in Loop: Header=BB14_1 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	testl	%eax, %eax
	jle	.LBB14_5
# BB#3:                                 # %.lr.ph.i21.preheader
                                        #   in Loop: Header=BB14_1 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB14_4:                               # %.lr.ph.i21
                                        #   Parent Loop BB14_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB14_4
.LBB14_5:                               # %cont_StopAndBackTrack.exit
                                        #   in Loop: Header=BB14_1 Depth=1
	movslq	st_STACKPOINTER(%rip), %rax
	cmpl	st_STACKSAVE(%rip), %eax
	je	.LBB14_14
# BB#6:                                 #   in Loop: Header=BB14_1 Depth=1
	leaq	-1(%rax), %rcx
	movl	%ecx, st_STACKPOINTER(%rip)
	leaq	st_STACK-8(,%rax,8), %r12
	.p2align	4, 0x90
.LBB14_7:                               # %.sink.split
                                        #   in Loop: Header=BB14_1 Depth=1
	movq	(%r12), %rbx
.LBB14_8:                               #   in Loop: Header=BB14_1 Depth=1
	movq	8(%rbx), %r12
	movq	(%r12), %rsi
	movq	%r15, %rdi
	callq	subst_Unify
	testl	%eax, %eax
	movq	(%rbx), %rbx
	je	.LBB14_16
# BB#9:                                 #   in Loop: Header=BB14_1 Depth=1
	testq	%rbx, %rbx
	je	.LBB14_11
# BB#10:                                #   in Loop: Header=BB14_1 Depth=1
	movslq	st_STACKPOINTER(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, st_STACKPOINTER(%rip)
	movq	%rbx, st_STACK(,%rax,8)
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	jmp	.LBB14_12
	.p2align	4, 0x90
.LBB14_16:                              #   in Loop: Header=BB14_1 Depth=1
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB14_1
# BB#17:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB14_1 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB14_18:                              # %.lr.ph.i
                                        #   Parent Loop BB14_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB14_18
	jmp	.LBB14_1
	.p2align	4, 0x90
.LBB14_11:                              #   in Loop: Header=BB14_1 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	movl	cont_BINDINGS(%rip), %ecx
	addl	%ecx, cont_STACK-4(,%rax,4)
.LBB14_12:                              #   in Loop: Header=BB14_1 Depth=1
	movl	$0, cont_BINDINGS(%rip)
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.LBB14_15
# BB#13:
	movq	(%rax), %rax
	movslq	st_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, st_STACKPOINTER(%rip)
	movq	%rax, st_STACK(,%rcx,8)
	movq	16(%r12), %rax
	movq	8(%rax), %r14
.LBB14_14:                              # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	st_TraverseForExistUnifier, .Lfunc_end14-st_TraverseForExistUnifier
	.cfi_endproc

	.globl	st_ExistGen
	.p2align	4, 0x90
	.type	st_ExistGen,@function
st_ExistGen:                            # @st_ExistGen
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi125:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi126:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 32
.Lcfi128:
	.cfi_offset %rbx, -32
.Lcfi129:
	.cfi_offset %r14, -24
.Lcfi130:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	cont_Check
	testq	%r14, %r14
	je	.LBB15_11
# BB#1:
	cmpq	$0, 16(%r14)
	jne	.LBB15_3
# BB#2:                                 # %st_Exist.exit
	cmpq	$0, 8(%r14)
	je	.LBB15_11
.LBB15_3:                               # %st_Exist.exit.thread12
	movl	$2, st_CURRENT_RETRIEVAL(%rip)
	movb	$0, st_WHICH_CONTEXTS(%rip)
	movq	%rbx, st_INDEX_CONTEXT(%rip)
	movslq	st_STACKPOINTER(%rip), %rax
	movl	%eax, st_STACKSAVE(%rip)
	movq	cont_INSTANCECONTEXT(%rip), %rcx
	leaq	64032(%rbx), %rdx
	movq	%rdx, cont_CURRENTBINDING(%rip)
	movq	%r15, 64040(%rbx)
	movq	%rcx, 64048(%rbx)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 64056(%rbx)
	movq	%rdx, cont_LASTBINDING(%rip)
	movl	cont_BINDINGS(%rip), %ecx
	incl	%ecx
	movslq	cont_STACKPOINTER(%rip), %rdx
	leaq	1(%rdx), %rsi
	movl	%esi, cont_STACKPOINTER(%rip)
	movl	%ecx, cont_STACK(,%rdx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	8(%r14), %rcx
	leal	1(%rax), %esi
	movl	%esi, st_STACKPOINTER(%rip)
	movq	%rcx, st_STACK(,%rax,8)
	leal	2(%rdx), %eax
	movl	%eax, cont_STACKPOINTER(%rip)
	movl	$0, cont_STACK+4(,%rdx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	%rbx, %rdi
	callq	st_TraverseForExistGen
	testq	%rax, %rax
	jne	.LBB15_12
# BB#4:
	cmpl	$0, st_CURRENT_RETRIEVAL(%rip)
	je	.LBB15_11
# BB#5:
	movl	st_STACKSAVE(%rip), %eax
	movl	%eax, st_STACKPOINTER(%rip)
	movb	st_WHICH_CONTEXTS(%rip), %al
	testb	%al, %al
	jne	.LBB15_10
# BB#6:
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB15_9
# BB#7:                                 # %.lr.ph.preheader.i.i
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB15_8:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB15_8
.LBB15_9:                               # %cont_Reset.exit.i
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
.LBB15_10:
	movl	$0, st_CURRENT_RETRIEVAL(%rip)
	movb	$1, st_WHICH_CONTEXTS(%rip)
	movq	$0, st_INDEX_CONTEXT(%rip)
	movw	$0, st_EXIST_MINMAX(%rip)
.LBB15_11:                              # %st_CancelExistRetrieval.exit
	xorl	%eax, %eax
.LBB15_12:                              # %st_CancelExistRetrieval.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	st_ExistGen, .Lfunc_end15-st_ExistGen
	.cfi_endproc

	.p2align	4, 0x90
	.type	st_TraverseForExistGen,@function
st_TraverseForExistGen:                 # @st_TraverseForExistGen
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi133:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi135:
	.cfi_def_cfa_offset 48
.Lcfi136:
	.cfi_offset %rbx, -40
.Lcfi137:
	.cfi_offset %r12, -32
.Lcfi138:
	.cfi_offset %r14, -24
.Lcfi139:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	xorps	%xmm0, %xmm0
	xorl	%ebx, %ebx
	jmp	.LBB16_1
	.p2align	4, 0x90
.LBB16_15:                              #   in Loop: Header=BB16_1 Depth=1
	addq	$8, %r12
	jmp	.LBB16_7
	.p2align	4, 0x90
.LBB16_1:                               # %cont_BackTrackAndStart.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_4 Depth 2
                                        #     Child Loop BB16_18 Depth 2
	testq	%rbx, %rbx
	jne	.LBB16_8
# BB#2:                                 #   in Loop: Header=BB16_1 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	testl	%eax, %eax
	jle	.LBB16_5
# BB#3:                                 # %.lr.ph.i21.preheader
                                        #   in Loop: Header=BB16_1 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB16_4:                               # %.lr.ph.i21
                                        #   Parent Loop BB16_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB16_4
.LBB16_5:                               # %cont_StopAndBackTrack.exit
                                        #   in Loop: Header=BB16_1 Depth=1
	movslq	st_STACKPOINTER(%rip), %rax
	cmpl	st_STACKSAVE(%rip), %eax
	je	.LBB16_14
# BB#6:                                 #   in Loop: Header=BB16_1 Depth=1
	leaq	-1(%rax), %rcx
	movl	%ecx, st_STACKPOINTER(%rip)
	leaq	st_STACK-8(,%rax,8), %r12
	.p2align	4, 0x90
.LBB16_7:                               # %.sink.split
                                        #   in Loop: Header=BB16_1 Depth=1
	movq	(%r12), %rbx
.LBB16_8:                               #   in Loop: Header=BB16_1 Depth=1
	movq	8(%rbx), %r12
	movq	(%r12), %rsi
	movq	%r15, %rdi
	callq	subst_Match
	testl	%eax, %eax
	movq	(%rbx), %rbx
	je	.LBB16_16
# BB#9:                                 #   in Loop: Header=BB16_1 Depth=1
	testq	%rbx, %rbx
	je	.LBB16_11
# BB#10:                                #   in Loop: Header=BB16_1 Depth=1
	movslq	st_STACKPOINTER(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, st_STACKPOINTER(%rip)
	movq	%rbx, st_STACK(,%rax,8)
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	jmp	.LBB16_12
	.p2align	4, 0x90
.LBB16_16:                              #   in Loop: Header=BB16_1 Depth=1
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB16_1
# BB#17:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB16_1 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB16_18:                              # %.lr.ph.i
                                        #   Parent Loop BB16_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB16_18
	jmp	.LBB16_1
	.p2align	4, 0x90
.LBB16_11:                              #   in Loop: Header=BB16_1 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	movl	cont_BINDINGS(%rip), %ecx
	addl	%ecx, cont_STACK-4(,%rax,4)
.LBB16_12:                              #   in Loop: Header=BB16_1 Depth=1
	movl	$0, cont_BINDINGS(%rip)
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.LBB16_15
# BB#13:
	movq	(%rax), %rax
	movslq	st_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, st_STACKPOINTER(%rip)
	movq	%rax, st_STACK(,%rcx,8)
	movq	16(%r12), %rax
	movq	8(%rax), %r14
.LBB16_14:                              # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	st_TraverseForExistGen, .Lfunc_end16-st_TraverseForExistGen
	.cfi_endproc

	.globl	st_ExistGenPreTest
	.p2align	4, 0x90
	.type	st_ExistGenPreTest,@function
st_ExistGenPreTest:                     # @st_ExistGenPreTest
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi140:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi141:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 32
.Lcfi143:
	.cfi_offset %rbx, -32
.Lcfi144:
	.cfi_offset %r14, -24
.Lcfi145:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	cont_Check
	testq	%r14, %r14
	je	.LBB17_11
# BB#1:
	cmpq	$0, 16(%r14)
	jne	.LBB17_3
# BB#2:                                 # %st_Exist.exit
	cmpq	$0, 8(%r14)
	je	.LBB17_11
.LBB17_3:                               # %st_Exist.exit.thread14
	movl	$3, st_CURRENT_RETRIEVAL(%rip)
	movb	$0, st_WHICH_CONTEXTS(%rip)
	movq	%rbx, st_INDEX_CONTEXT(%rip)
	movl	st_STACKPOINTER(%rip), %eax
	movl	%eax, st_STACKSAVE(%rip)
	movq	%r15, %rdi
	callq	term_ComputeSize
	movw	%ax, st_EXIST_MINMAX(%rip)
	leaq	64032(%rbx), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	%r15, 64040(%rbx)
	movq	%rbx, 64048(%rbx)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 64056(%rbx)
	movq	%rax, cont_LASTBINDING(%rip)
	movl	cont_BINDINGS(%rip), %eax
	incl	%eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	8(%r14), %rax
	movslq	st_STACKPOINTER(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, st_STACKPOINTER(%rip)
	movq	%rax, st_STACK(,%rdx,8)
	leal	2(%rcx), %eax
	movl	%eax, cont_STACKPOINTER(%rip)
	movl	$0, cont_STACK+4(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	%rbx, %rdi
	callq	st_TraverseForExistGenPreTest
	testq	%rax, %rax
	jne	.LBB17_12
# BB#4:
	cmpl	$0, st_CURRENT_RETRIEVAL(%rip)
	je	.LBB17_11
# BB#5:
	movl	st_STACKSAVE(%rip), %eax
	movl	%eax, st_STACKPOINTER(%rip)
	movb	st_WHICH_CONTEXTS(%rip), %al
	testb	%al, %al
	jne	.LBB17_10
# BB#6:
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB17_9
# BB#7:                                 # %.lr.ph.preheader.i.i
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB17_8:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB17_8
.LBB17_9:                               # %cont_Reset.exit.i
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
.LBB17_10:
	movl	$0, st_CURRENT_RETRIEVAL(%rip)
	movb	$1, st_WHICH_CONTEXTS(%rip)
	movq	$0, st_INDEX_CONTEXT(%rip)
	movw	$0, st_EXIST_MINMAX(%rip)
.LBB17_11:                              # %st_CancelExistRetrieval.exit
	xorl	%eax, %eax
.LBB17_12:                              # %st_CancelExistRetrieval.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	st_ExistGenPreTest, .Lfunc_end17-st_ExistGenPreTest
	.cfi_endproc

	.p2align	4, 0x90
	.type	st_TraverseForExistGenPreTest,@function
st_TraverseForExistGenPreTest:          # @st_TraverseForExistGenPreTest
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi146:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi148:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi149:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi150:
	.cfi_def_cfa_offset 48
.Lcfi151:
	.cfi_offset %rbx, -40
.Lcfi152:
	.cfi_offset %r12, -32
.Lcfi153:
	.cfi_offset %r14, -24
.Lcfi154:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	xorps	%xmm0, %xmm0
	xorl	%r12d, %r12d
	jmp	.LBB18_1
	.p2align	4, 0x90
.LBB18_21:                              #   in Loop: Header=BB18_1 Depth=1
	addq	$8, %rbx
	jmp	.LBB18_7
	.p2align	4, 0x90
.LBB18_1:                               # %cont_BackTrackAndStart.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_4 Depth 2
                                        #     Child Loop BB18_12 Depth 2
	testq	%r12, %r12
	jne	.LBB18_8
# BB#2:                                 #   in Loop: Header=BB18_1 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	testl	%eax, %eax
	jle	.LBB18_5
# BB#3:                                 # %.lr.ph.i23.preheader
                                        #   in Loop: Header=BB18_1 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB18_4:                               # %.lr.ph.i23
                                        #   Parent Loop BB18_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB18_4
.LBB18_5:                               # %cont_StopAndBackTrack.exit
                                        #   in Loop: Header=BB18_1 Depth=1
	movslq	st_STACKPOINTER(%rip), %rax
	cmpl	st_STACKSAVE(%rip), %eax
	je	.LBB18_20
# BB#6:                                 #   in Loop: Header=BB18_1 Depth=1
	leaq	-1(%rax), %rcx
	movl	%ecx, st_STACKPOINTER(%rip)
	leaq	st_STACK-8(,%rax,8), %rbx
.LBB18_7:                               # %.sink.split
                                        #   in Loop: Header=BB18_1 Depth=1
	movq	(%rbx), %r12
.LBB18_8:                               #   in Loop: Header=BB18_1 Depth=1
	movq	8(%r12), %rbx
	movzwl	st_EXIST_MINMAX(%rip), %eax
	cmpw	26(%rbx), %ax
	jae	.LBB18_13
# BB#9:                                 # %.thread
                                        #   in Loop: Header=BB18_1 Depth=1
	movq	(%r12), %r12
	jmp	.LBB18_10
	.p2align	4, 0x90
.LBB18_13:                              #   in Loop: Header=BB18_1 Depth=1
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	callq	subst_Match
	testl	%eax, %eax
	movq	(%r12), %r12
	je	.LBB18_14
# BB#15:                                #   in Loop: Header=BB18_1 Depth=1
	testq	%r12, %r12
	xorps	%xmm0, %xmm0
	je	.LBB18_17
# BB#16:                                #   in Loop: Header=BB18_1 Depth=1
	movslq	st_STACKPOINTER(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, st_STACKPOINTER(%rip)
	movq	%r12, st_STACK(,%rax,8)
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	jmp	.LBB18_18
	.p2align	4, 0x90
.LBB18_14:                              #   in Loop: Header=BB18_1 Depth=1
	xorps	%xmm0, %xmm0
.LBB18_10:                              #   in Loop: Header=BB18_1 Depth=1
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB18_1
# BB#11:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB18_1 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB18_12:                              # %.lr.ph.i
                                        #   Parent Loop BB18_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB18_12
	jmp	.LBB18_1
.LBB18_17:                              #   in Loop: Header=BB18_1 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	movl	cont_BINDINGS(%rip), %ecx
	addl	%ecx, cont_STACK-4(,%rax,4)
.LBB18_18:                              #   in Loop: Header=BB18_1 Depth=1
	movl	$0, cont_BINDINGS(%rip)
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB18_21
# BB#19:
	movq	(%rax), %rax
	movslq	st_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, st_STACKPOINTER(%rip)
	movq	%rax, st_STACK(,%rcx,8)
	movq	16(%rbx), %rax
	movq	8(%rax), %r14
.LBB18_20:                              # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end18:
	.size	st_TraverseForExistGenPreTest, .Lfunc_end18-st_TraverseForExistGenPreTest
	.cfi_endproc

	.globl	st_ExistInstance
	.p2align	4, 0x90
	.type	st_ExistInstance,@function
st_ExistInstance:                       # @st_ExistInstance
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi155:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi156:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 32
.Lcfi158:
	.cfi_offset %rbx, -32
.Lcfi159:
	.cfi_offset %r14, -24
.Lcfi160:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	cont_Check
	testq	%r14, %r14
	je	.LBB19_11
# BB#1:
	cmpq	$0, 16(%r14)
	jne	.LBB19_3
# BB#2:                                 # %st_Exist.exit
	cmpq	$0, 8(%r14)
	je	.LBB19_11
.LBB19_3:                               # %st_Exist.exit.thread13
	movl	$4, st_CURRENT_RETRIEVAL(%rip)
	movb	$0, st_WHICH_CONTEXTS(%rip)
	movq	%rbx, st_INDEX_CONTEXT(%rip)
	movslq	st_STACKPOINTER(%rip), %rax
	movl	%eax, st_STACKSAVE(%rip)
	leaq	64032(%rbx), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	%r15, 64040(%rbx)
	movq	%rbx, 64048(%rbx)
	movq	cont_LASTBINDING(%rip), %rdx
	movq	%rdx, 64056(%rbx)
	movq	%rcx, cont_LASTBINDING(%rip)
	movl	cont_BINDINGS(%rip), %ecx
	incl	%ecx
	movslq	cont_STACKPOINTER(%rip), %rdx
	leaq	1(%rdx), %rsi
	movl	%esi, cont_STACKPOINTER(%rip)
	movl	%ecx, cont_STACK(,%rdx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	8(%r14), %rcx
	leal	1(%rax), %esi
	movl	%esi, st_STACKPOINTER(%rip)
	movq	%rcx, st_STACK(,%rax,8)
	leal	2(%rdx), %eax
	movl	%eax, cont_STACKPOINTER(%rip)
	movl	$0, cont_STACK+4(,%rdx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	%rbx, %rdi
	callq	st_TraverseForExistInstance
	testq	%rax, %rax
	jne	.LBB19_12
# BB#4:
	cmpl	$0, st_CURRENT_RETRIEVAL(%rip)
	je	.LBB19_11
# BB#5:
	movl	st_STACKSAVE(%rip), %eax
	movl	%eax, st_STACKPOINTER(%rip)
	movb	st_WHICH_CONTEXTS(%rip), %al
	testb	%al, %al
	jne	.LBB19_10
# BB#6:
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB19_9
# BB#7:                                 # %.lr.ph.preheader.i.i
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB19_8:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB19_8
.LBB19_9:                               # %cont_Reset.exit.i
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
.LBB19_10:
	movl	$0, st_CURRENT_RETRIEVAL(%rip)
	movb	$1, st_WHICH_CONTEXTS(%rip)
	movq	$0, st_INDEX_CONTEXT(%rip)
	movw	$0, st_EXIST_MINMAX(%rip)
.LBB19_11:                              # %st_CancelExistRetrieval.exit
	xorl	%eax, %eax
.LBB19_12:                              # %st_CancelExistRetrieval.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end19:
	.size	st_ExistInstance, .Lfunc_end19-st_ExistInstance
	.cfi_endproc

	.p2align	4, 0x90
	.type	st_TraverseForExistInstance,@function
st_TraverseForExistInstance:            # @st_TraverseForExistInstance
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi163:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi164:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi165:
	.cfi_def_cfa_offset 48
.Lcfi166:
	.cfi_offset %rbx, -40
.Lcfi167:
	.cfi_offset %r12, -32
.Lcfi168:
	.cfi_offset %r14, -24
.Lcfi169:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	xorps	%xmm0, %xmm0
	xorl	%ebx, %ebx
	jmp	.LBB20_1
	.p2align	4, 0x90
.LBB20_15:                              #   in Loop: Header=BB20_1 Depth=1
	addq	$8, %r12
	jmp	.LBB20_7
	.p2align	4, 0x90
.LBB20_1:                               # %cont_BackTrackAndStart.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_4 Depth 2
                                        #     Child Loop BB20_18 Depth 2
	testq	%rbx, %rbx
	jne	.LBB20_8
# BB#2:                                 #   in Loop: Header=BB20_1 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	testl	%eax, %eax
	jle	.LBB20_5
# BB#3:                                 # %.lr.ph.i21.preheader
                                        #   in Loop: Header=BB20_1 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB20_4:                               # %.lr.ph.i21
                                        #   Parent Loop BB20_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB20_4
.LBB20_5:                               # %cont_StopAndBackTrack.exit
                                        #   in Loop: Header=BB20_1 Depth=1
	movslq	st_STACKPOINTER(%rip), %rax
	cmpl	st_STACKSAVE(%rip), %eax
	je	.LBB20_14
# BB#6:                                 #   in Loop: Header=BB20_1 Depth=1
	leaq	-1(%rax), %rcx
	movl	%ecx, st_STACKPOINTER(%rip)
	leaq	st_STACK-8(,%rax,8), %r12
	.p2align	4, 0x90
.LBB20_7:                               # %.sink.split
                                        #   in Loop: Header=BB20_1 Depth=1
	movq	(%r12), %rbx
.LBB20_8:                               #   in Loop: Header=BB20_1 Depth=1
	movq	8(%rbx), %r12
	movq	(%r12), %rsi
	movq	%r15, %rdi
	callq	subst_MatchReverse
	testl	%eax, %eax
	movq	(%rbx), %rbx
	je	.LBB20_16
# BB#9:                                 #   in Loop: Header=BB20_1 Depth=1
	testq	%rbx, %rbx
	je	.LBB20_11
# BB#10:                                #   in Loop: Header=BB20_1 Depth=1
	movslq	st_STACKPOINTER(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, st_STACKPOINTER(%rip)
	movq	%rbx, st_STACK(,%rax,8)
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	jmp	.LBB20_12
	.p2align	4, 0x90
.LBB20_16:                              #   in Loop: Header=BB20_1 Depth=1
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB20_1
# BB#17:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB20_1 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB20_18:                              # %.lr.ph.i
                                        #   Parent Loop BB20_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB20_18
	jmp	.LBB20_1
	.p2align	4, 0x90
.LBB20_11:                              #   in Loop: Header=BB20_1 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	movl	cont_BINDINGS(%rip), %ecx
	addl	%ecx, cont_STACK-4(,%rax,4)
.LBB20_12:                              #   in Loop: Header=BB20_1 Depth=1
	movl	$0, cont_BINDINGS(%rip)
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.LBB20_15
# BB#13:
	movq	(%rax), %rax
	movslq	st_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, st_STACKPOINTER(%rip)
	movq	%rax, st_STACK(,%rcx,8)
	movq	16(%r12), %rax
	movq	8(%rax), %r14
.LBB20_14:                              # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end20:
	.size	st_TraverseForExistInstance, .Lfunc_end20-st_TraverseForExistInstance
	.cfi_endproc

	.globl	st_ExistInstancePreTest
	.p2align	4, 0x90
	.type	st_ExistInstancePreTest,@function
st_ExistInstancePreTest:                # @st_ExistInstancePreTest
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi170:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi171:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi172:
	.cfi_def_cfa_offset 32
.Lcfi173:
	.cfi_offset %rbx, -32
.Lcfi174:
	.cfi_offset %r14, -24
.Lcfi175:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	cont_Check
	testq	%r14, %r14
	je	.LBB21_11
# BB#1:
	cmpq	$0, 16(%r14)
	jne	.LBB21_3
# BB#2:                                 # %st_Exist.exit
	cmpq	$0, 8(%r14)
	je	.LBB21_11
.LBB21_3:                               # %st_Exist.exit.thread14
	movl	$5, st_CURRENT_RETRIEVAL(%rip)
	movb	$0, st_WHICH_CONTEXTS(%rip)
	movq	%rbx, st_INDEX_CONTEXT(%rip)
	movl	st_STACKPOINTER(%rip), %eax
	movl	%eax, st_STACKSAVE(%rip)
	movq	%r15, %rdi
	callq	term_ComputeSize
	movw	%ax, st_EXIST_MINMAX(%rip)
	leaq	64032(%rbx), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	%r15, 64040(%rbx)
	movq	%rbx, 64048(%rbx)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 64056(%rbx)
	movq	%rax, cont_LASTBINDING(%rip)
	movl	cont_BINDINGS(%rip), %eax
	incl	%eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	8(%r14), %rax
	movslq	st_STACKPOINTER(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, st_STACKPOINTER(%rip)
	movq	%rax, st_STACK(,%rdx,8)
	leal	2(%rcx), %eax
	movl	%eax, cont_STACKPOINTER(%rip)
	movl	$0, cont_STACK+4(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	%rbx, %rdi
	callq	st_TraverseForExistInstancePreTest
	testq	%rax, %rax
	jne	.LBB21_12
# BB#4:
	cmpl	$0, st_CURRENT_RETRIEVAL(%rip)
	je	.LBB21_11
# BB#5:
	movl	st_STACKSAVE(%rip), %eax
	movl	%eax, st_STACKPOINTER(%rip)
	movb	st_WHICH_CONTEXTS(%rip), %al
	testb	%al, %al
	jne	.LBB21_10
# BB#6:
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB21_9
# BB#7:                                 # %.lr.ph.preheader.i.i
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB21_8:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB21_8
.LBB21_9:                               # %cont_Reset.exit.i
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
.LBB21_10:
	movl	$0, st_CURRENT_RETRIEVAL(%rip)
	movb	$1, st_WHICH_CONTEXTS(%rip)
	movq	$0, st_INDEX_CONTEXT(%rip)
	movw	$0, st_EXIST_MINMAX(%rip)
.LBB21_11:                              # %st_CancelExistRetrieval.exit
	xorl	%eax, %eax
.LBB21_12:                              # %st_CancelExistRetrieval.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	st_ExistInstancePreTest, .Lfunc_end21-st_ExistInstancePreTest
	.cfi_endproc

	.p2align	4, 0x90
	.type	st_TraverseForExistInstancePreTest,@function
st_TraverseForExistInstancePreTest:     # @st_TraverseForExistInstancePreTest
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi176:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi177:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi178:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi179:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi180:
	.cfi_def_cfa_offset 48
.Lcfi181:
	.cfi_offset %rbx, -40
.Lcfi182:
	.cfi_offset %r12, -32
.Lcfi183:
	.cfi_offset %r14, -24
.Lcfi184:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	xorps	%xmm0, %xmm0
	xorl	%r12d, %r12d
	jmp	.LBB22_1
	.p2align	4, 0x90
.LBB22_21:                              #   in Loop: Header=BB22_1 Depth=1
	addq	$8, %rbx
	jmp	.LBB22_7
	.p2align	4, 0x90
.LBB22_1:                               # %cont_BackTrackAndStart.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_4 Depth 2
                                        #     Child Loop BB22_12 Depth 2
	testq	%r12, %r12
	jne	.LBB22_8
# BB#2:                                 #   in Loop: Header=BB22_1 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	testl	%eax, %eax
	jle	.LBB22_5
# BB#3:                                 # %.lr.ph.i23.preheader
                                        #   in Loop: Header=BB22_1 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB22_4:                               # %.lr.ph.i23
                                        #   Parent Loop BB22_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB22_4
.LBB22_5:                               # %cont_StopAndBackTrack.exit
                                        #   in Loop: Header=BB22_1 Depth=1
	movslq	st_STACKPOINTER(%rip), %rax
	cmpl	st_STACKSAVE(%rip), %eax
	je	.LBB22_20
# BB#6:                                 #   in Loop: Header=BB22_1 Depth=1
	leaq	-1(%rax), %rcx
	movl	%ecx, st_STACKPOINTER(%rip)
	leaq	st_STACK-8(,%rax,8), %rbx
.LBB22_7:                               # %.sink.split
                                        #   in Loop: Header=BB22_1 Depth=1
	movq	(%rbx), %r12
.LBB22_8:                               #   in Loop: Header=BB22_1 Depth=1
	movq	8(%r12), %rbx
	movzwl	st_EXIST_MINMAX(%rip), %eax
	cmpw	24(%rbx), %ax
	jbe	.LBB22_13
# BB#9:                                 # %.thread
                                        #   in Loop: Header=BB22_1 Depth=1
	movq	(%r12), %r12
	jmp	.LBB22_10
	.p2align	4, 0x90
.LBB22_13:                              #   in Loop: Header=BB22_1 Depth=1
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	callq	subst_MatchReverse
	testl	%eax, %eax
	movq	(%r12), %r12
	je	.LBB22_14
# BB#15:                                #   in Loop: Header=BB22_1 Depth=1
	testq	%r12, %r12
	xorps	%xmm0, %xmm0
	je	.LBB22_17
# BB#16:                                #   in Loop: Header=BB22_1 Depth=1
	movslq	st_STACKPOINTER(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, st_STACKPOINTER(%rip)
	movq	%r12, st_STACK(,%rax,8)
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	jmp	.LBB22_18
	.p2align	4, 0x90
.LBB22_14:                              #   in Loop: Header=BB22_1 Depth=1
	xorps	%xmm0, %xmm0
.LBB22_10:                              #   in Loop: Header=BB22_1 Depth=1
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB22_1
# BB#11:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB22_1 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB22_12:                              # %.lr.ph.i
                                        #   Parent Loop BB22_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB22_12
	jmp	.LBB22_1
.LBB22_17:                              #   in Loop: Header=BB22_1 Depth=1
	movslq	cont_STACKPOINTER(%rip), %rax
	movl	cont_BINDINGS(%rip), %ecx
	addl	%ecx, cont_STACK-4(,%rax,4)
.LBB22_18:                              #   in Loop: Header=BB22_1 Depth=1
	movl	$0, cont_BINDINGS(%rip)
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB22_21
# BB#19:
	movq	(%rax), %rax
	movslq	st_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, st_STACKPOINTER(%rip)
	movq	%rax, st_STACK(,%rcx,8)
	movq	16(%rbx), %rax
	movq	8(%rax), %r14
.LBB22_20:                              # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end22:
	.size	st_TraverseForExistInstancePreTest, .Lfunc_end22-st_TraverseForExistInstancePreTest
	.cfi_endproc

	.globl	st_NextCandidate
	.p2align	4, 0x90
	.type	st_NextCandidate,@function
st_NextCandidate:                       # @st_NextCandidate
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi185:
	.cfi_def_cfa_offset 16
	movl	st_STACKPOINTER(%rip), %eax
	cltq
	leaq	-1(%rax), %rcx
	movl	%ecx, st_STACKPOINTER(%rip)
	movq	st_STACK-8(,%rax,8), %rcx
	testq	%rcx, %rcx
	je	.LBB23_2
# BB#1:
	movq	(%rcx), %rdx
	movl	%eax, st_STACKPOINTER(%rip)
	movq	%rdx, st_STACK-8(,%rax,8)
	movq	8(%rcx), %rax
	popq	%rcx
	retq
.LBB23_2:
	movb	st_WHICH_CONTEXTS(%rip), %al
	testb	%al, %al
	jne	.LBB23_11
# BB#3:
	movl	st_CURRENT_RETRIEVAL(%rip), %eax
	decl	%eax
	cmpl	$4, %eax
	ja	.LBB23_9
# BB#4:
	jmpq	*.LJTI23_0(,%rax,8)
.LBB23_12:
	movq	st_INDEX_CONTEXT(%rip), %rdi
	callq	st_TraverseForExistUnifier
	testq	%rax, %rax
	jne	.LBB23_22
	jmp	.LBB23_14
.LBB23_6:
	movq	st_INDEX_CONTEXT(%rip), %rdi
	callq	st_TraverseForExistGenPreTest
	testq	%rax, %rax
	jne	.LBB23_22
	jmp	.LBB23_14
.LBB23_5:
	movq	st_INDEX_CONTEXT(%rip), %rdi
	callq	st_TraverseForExistGen
	testq	%rax, %rax
	jne	.LBB23_22
	jmp	.LBB23_14
.LBB23_7:
	movq	st_INDEX_CONTEXT(%rip), %rdi
	callq	st_TraverseForExistInstance
	testq	%rax, %rax
	jne	.LBB23_22
.LBB23_14:
	cmpl	$0, st_CURRENT_RETRIEVAL(%rip)
	je	.LBB23_21
# BB#15:
	movl	st_STACKSAVE(%rip), %eax
	movl	%eax, st_STACKPOINTER(%rip)
	movb	st_WHICH_CONTEXTS(%rip), %al
	testb	%al, %al
	jne	.LBB23_20
# BB#16:
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB23_19
# BB#17:                                # %.lr.ph.preheader.i.i
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB23_18:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB23_18
.LBB23_19:                              # %cont_Reset.exit.i
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
.LBB23_20:
	movl	$0, st_CURRENT_RETRIEVAL(%rip)
	movb	$1, st_WHICH_CONTEXTS(%rip)
	movq	$0, st_INDEX_CONTEXT(%rip)
	movw	$0, st_EXIST_MINMAX(%rip)
.LBB23_21:                              # %st_CancelExistRetrieval.exit
	xorl	%eax, %eax
.LBB23_22:                              # %st_CancelExistRetrieval.exit
	popq	%rcx
	retq
.LBB23_11:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$1582, %ecx             # imm = 0x62E
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.5, %edi
	jmp	.LBB23_10
.LBB23_8:
	movq	st_INDEX_CONTEXT(%rip), %rdi
	callq	st_TraverseForExistInstancePreTest
.LBB23_9:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$1577, %ecx             # imm = 0x629
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.4, %edi
.LBB23_10:
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end23:
	.size	st_NextCandidate, .Lfunc_end23-st_NextCandidate
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI23_0:
	.quad	.LBB23_12
	.quad	.LBB23_5
	.quad	.LBB23_6
	.quad	.LBB23_7
	.quad	.LBB23_8

	.text
	.globl	st_Print
	.p2align	4, 0x90
	.type	st_Print,@function
st_Print:                               # @st_Print
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi186:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi187:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi188:
	.cfi_def_cfa_offset 32
.Lcfi189:
	.cfi_offset %rbx, -32
.Lcfi190:
	.cfi_offset %r14, -24
.Lcfi191:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB24_8
# BB#1:
	cmpq	$0, 16(%r14)
	jne	.LBB24_3
# BB#2:                                 # %st_Empty.exit
	cmpq	$0, 8(%r14)
	je	.LBB24_8
.LBB24_3:                               # %st_Empty.exit.thread
	movq	stdout(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$8, %esi
	movl	$1, %edx
	callq	fwrite
	movzwl	24(%r14), %esi
	movzwl	26(%r14), %edx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%r14), %rdi
	callq	subst_Print
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	cmpq	$0, 16(%r14)
	je	.LBB24_9
# BB#4:
	movq	stdout(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	testq	%r15, %r15
	movq	16(%r14), %rbx
	jne	.LBB24_7
# BB#5:
	movq	%rbx, %rdi
	callq	list_Length
	movl	%eax, %ecx
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	jmp	.LBB24_11
	.p2align	4, 0x90
.LBB24_6:                               # %.lr.ph27
                                        #   in Loop: Header=BB24_7 Depth=1
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	8(%rbx), %rdi
	callq	*%r15
	movq	(%rbx), %rbx
.LBB24_7:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	jne	.LBB24_6
	jmp	.LBB24_11
.LBB24_8:                               # %st_Empty.exit.thread21
	movl	$.L.str.6, %edi
	jmp	.LBB24_12
.LBB24_9:
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB24_11
	.p2align	4, 0x90
.LBB24_10:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	$2, %esi
	movq	%r15, %rdx
	callq	st_PrintHelp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB24_10
.LBB24_11:                              # %.loopexit
	movl	$.L.str.11, %edi
.LBB24_12:                              # %st_Empty.exit.thread21
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	puts                    # TAILCALL
.Lfunc_end24:
	.size	st_Print, .Lfunc_end24-st_Print
	.cfi_endproc

	.p2align	4, 0x90
	.type	st_PrintHelp,@function
st_PrintHelp:                           # @st_PrintHelp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi192:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi193:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi194:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi195:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi196:
	.cfi_def_cfa_offset 48
.Lcfi197:
	.cfi_offset %rbx, -40
.Lcfi198:
	.cfi_offset %r14, -32
.Lcfi199:
	.cfi_offset %r15, -24
.Lcfi200:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %ebp
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB25_18
# BB#1:                                 # %.preheader39
	testl	%ebp, %ebp
	jle	.LBB25_19
# BB#2:
	movl	%ebp, %ebx
	.p2align	4, 0x90
.LBB25_3:                               # %.lr.ph53
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	decl	%ebx
	jne	.LBB25_3
# BB#4:                                 # %._crit_edge54
	movl	$.L.str.13, %edi
	callq	puts
	movq	stdout(%rip), %rax
	testl	%ebp, %ebp
	jle	.LBB25_7
# BB#5:                                 # %.lr.ph49.preheader
	movl	%ebp, %ebx
	.p2align	4, 0x90
.LBB25_6:                               # %.lr.ph49
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	movq	stdout(%rip), %rax
	decl	%ebx
	jne	.LBB25_6
	jmp	.LBB25_7
.LBB25_19:                              # %._crit_edge54.thread
	movl	$.L.str.13, %edi
	callq	puts
	movq	stdout(%rip), %rax
.LBB25_7:                               # %._crit_edge50
	movl	$.L.str.14, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movzwl	24(%r14), %esi
	movzwl	26(%r14), %edx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%r14), %rdi
	callq	subst_Print
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	cmpq	$0, 16(%r14)
	je	.LBB25_15
# BB#8:                                 # %.preheader38
	movq	stdout(%rip), %rax
	testl	%ebp, %ebp
	jle	.LBB25_10
	.p2align	4, 0x90
.LBB25_9:                               # %.lr.ph46
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	movq	stdout(%rip), %rax
	decl	%ebp
	jne	.LBB25_9
.LBB25_10:                              # %._crit_edge
	movl	$.L.str.9, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	testq	%r15, %r15
	movq	16(%r14), %rbp
	jne	.LBB25_11
# BB#13:
	movq	%rbp, %rdi
	callq	list_Length
	movl	%eax, %ecx
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	jmp	.LBB25_14
	.p2align	4, 0x90
.LBB25_12:                              # %.lr.ph44
                                        #   in Loop: Header=BB25_11 Depth=1
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	8(%rbp), %rdi
	callq	*%r15
	movq	(%rbp), %rbp
.LBB25_11:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbp, %rbp
	jne	.LBB25_12
.LBB25_14:                              # %.loopexit37
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.LBB25_15:
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB25_18
# BB#16:                                # %.lr.ph
	addl	$2, %ebp
	.p2align	4, 0x90
.LBB25_17:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	st_PrintHelp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB25_17
.LBB25_18:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	st_PrintHelp, .Lfunc_end25-st_PrintHelp
	.cfi_endproc

	.type	st_CURRENT_RETRIEVAL,@object # @st_CURRENT_RETRIEVAL
	.local	st_CURRENT_RETRIEVAL
	.comm	st_CURRENT_RETRIEVAL,4,4
	.type	st_STACKSAVE,@object    # @st_STACKSAVE
	.comm	st_STACKSAVE,4,4
	.type	st_WHICH_CONTEXTS,@object # @st_WHICH_CONTEXTS
	.local	st_WHICH_CONTEXTS
	.comm	st_WHICH_CONTEXTS,1,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str, 31

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/SPASS/st.c"
	.size	.L.str.1, 74

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.3, 133

	.type	st_INDEX_CONTEXT,@object # @st_INDEX_CONTEXT
	.local	st_INDEX_CONTEXT
	.comm	st_INDEX_CONTEXT,8,8
	.type	st_EXIST_MINMAX,@object # @st_EXIST_MINMAX
	.local	st_EXIST_MINMAX
	.comm	st_EXIST_MINMAX,2,2
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n In st_NextCandidate: Unknown retrieval type.\n"
	.size	.L.str.4, 48

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\n In st_NextCandidate: Unknown context type.\n"
	.size	.L.str.5, 46

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n\nIndex empty."
	.size	.L.str.6, 15

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\n\nroot: "
	.size	.L.str.7, 9

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" Max: %d, Min: %d, "
	.size	.L.str.8, 20

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"  =>"
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	" %d Entries"
	.size	.L.str.10, 12

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\n"
	.size	.L.str.11, 2

	.type	st_STACK,@object        # @st_STACK
	.comm	st_STACK,8000,16
	.type	st_STACKPOINTER,@object # @st_STACKPOINTER
	.comm	st_STACKPOINTER,4,4
	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\n\n"
	.size	.L.str.12, 3

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"|"
	.size	.L.str.13, 2

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"+-"
	.size	.L.str.14, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
