	.text
	.file	"storage.bc"
	.globl	init_storage
	.p2align	4, 0x90
	.type	init_storage,@function
init_storage:                           # @init_storage
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$0, f_count(%rip)
	callq	more_functions
	movq	f_names(%rip), %rax
	movq	$.L.str, (%rax)
	movl	$0, v_count(%rip)
	callq	more_variables
	movl	$0, a_count(%rip)
	callq	more_arrays
	movq	$0, ex_stack(%rip)
	movq	$0, fn_stack(%rip)
	movl	$10, i_base(%rip)
	movl	$10, o_base(%rip)
	movl	$0, scale(%rip)
	movb	$0, c_code(%rip)
	popq	%rax
	jmp	init_numbers            # TAILCALL
.Lfunc_end0:
	.size	init_storage, .Lfunc_end0-init_storage
	.cfi_endproc

	.globl	more_functions
	.p2align	4, 0x90
	.type	more_functions,@function
more_functions:                         # @more_functions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 64
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movslq	f_count(%rip), %r12
	movq	functions(%rip), %r15
	movq	f_names(%rip), %r14
	leaq	32(%r12), %rbp
	movl	%ebp, f_count(%rip)
	imulq	$168, %rbp, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, functions(%rip)
	leaq	256(,%r12,8), %rdi
	callq	malloc
	testq	%r12, %r12
	movq	%rax, f_names(%rip)
	jle	.LBB1_1
# BB#2:                                 # %.lr.ph38.preheader
	movl	$168, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	memcpy
	movq	(%r14), %rax
	movq	f_names(%rip), %rcx
	movq	%rax, (%rcx)
	cmpl	$1, %r12d
	je	.LBB1_9
# BB#3:                                 # %.lr.ph38..lr.ph38_crit_edge.preheader
	movl	%r12d, %r13d
	testb	$1, %r13b
	jne	.LBB1_4
# BB#5:                                 # %.lr.ph38..lr.ph38_crit_edge.prol
	movq	functions(%rip), %rdi
	addq	$168, %rdi
	leaq	168(%r15), %rsi
	movl	$168, %edx
	callq	memcpy
	movq	8(%r14), %rax
	movq	f_names(%rip), %rcx
	movq	%rax, 8(%rcx)
	movl	$2, %ebx
	cmpl	$2, %r12d
	jne	.LBB1_7
	jmp	.LBB1_9
.LBB1_1:
	xorl	%edx, %edx
	cmpl	%ebp, %edx
	jl	.LBB1_11
	jmp	.LBB1_17
.LBB1_4:
	movl	$1, %ebx
	cmpl	$2, %r12d
	je	.LBB1_9
.LBB1_7:                                # %.lr.ph38..lr.ph38_crit_edge.preheader.new
	imulq	$168, %rbx, %rbp
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph38..lr.ph38_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	functions(%rip), %rdi
	addq	%rbp, %rdi
	leaq	(%r15,%rbp), %rsi
	movl	$168, %edx
	callq	memcpy
	movq	(%r14,%rbx,8), %rax
	movq	f_names(%rip), %rcx
	movq	%rax, (%rcx,%rbx,8)
	movq	functions(%rip), %rax
	leaq	168(%rax,%rbp), %rdi
	leaq	168(%r15,%rbp), %rsi
	movl	$168, %edx
	callq	memcpy
	movq	8(%r14,%rbx,8), %rax
	movq	f_names(%rip), %rcx
	movq	%rax, 8(%rcx,%rbx,8)
	addq	$2, %rbx
	addq	$336, %rbp              # imm = 0x150
	cmpq	%rbx, %r13
	jne	.LBB1_8
.LBB1_9:                                # %.preheader.loopexit
	movl	f_count(%rip), %ebp
	movl	%r12d, %edx
	cmpl	%ebp, %edx
	jge	.LBB1_17
.LBB1_11:                               # %.lr.ph
	movq	functions(%rip), %rax
	movslq	%edx, %rdi
	movslq	%ebp, %rcx
	subl	%edx, %ebp
	leaq	-1(%rcx), %rsi
	testb	$1, %bpl
	jne	.LBB1_13
# BB#12:
	xorl	%ebp, %ebp
	movq	%rdi, %rdx
	cmpq	%rdi, %rsi
	jne	.LBB1_15
	jmp	.LBB1_17
.LBB1_13:
	imulq	$168, %rdi, %rbp
	movb	$0, (%rax,%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 120(%rax,%rbp)
	movups	%xmm0, 104(%rax,%rbp)
	movups	%xmm0, 88(%rax,%rbp)
	movups	%xmm0, 72(%rax,%rbp)
	movups	%xmm0, 56(%rax,%rbp)
	movups	%xmm0, 40(%rax,%rbp)
	movups	%xmm0, 24(%rax,%rbp)
	movups	%xmm0, 8(%rax,%rbp)
	movl	$0, 136(%rax,%rbp)
	leaq	1(%rdi), %rdx
	movups	%xmm0, 144(%rax,%rbp)
	movq	$0, 160(%rax,%rbp)
	movl	$1, %ebp
	cmpq	%rdi, %rsi
	je	.LBB1_17
.LBB1_15:                               # %.lr.ph.new
	imulq	$168, %rdx, %rsi
	imulq	$168, %rbp, %rbp
	imulq	$168, %rdi, %rdi
	addq	%rbp, %rdi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_16:                               # =>This Inner Loop Header: Depth=1
	movb	$0, (%rsi,%rax)
	movups	%xmm0, 120(%rax,%rdi)
	movups	%xmm0, 104(%rax,%rdi)
	movups	%xmm0, 88(%rax,%rdi)
	movups	%xmm0, 72(%rax,%rdi)
	movups	%xmm0, 56(%rax,%rdi)
	movups	%xmm0, 40(%rax,%rdi)
	movups	%xmm0, 24(%rax,%rdi)
	movups	%xmm0, 8(%rax,%rdi)
	movl	$0, 136(%rsi,%rax)
	movups	%xmm0, 144(%rsi,%rax)
	movq	$0, 160(%rsi,%rax)
	movb	$0, 168(%rsi,%rax)
	movups	%xmm0, 288(%rax,%rdi)
	movups	%xmm0, 272(%rax,%rdi)
	movups	%xmm0, 256(%rax,%rdi)
	movups	%xmm0, 240(%rax,%rdi)
	movups	%xmm0, 224(%rax,%rdi)
	movups	%xmm0, 208(%rax,%rdi)
	movups	%xmm0, 192(%rax,%rdi)
	movups	%xmm0, 176(%rax,%rdi)
	movl	$0, 304(%rsi,%rax)
	addq	$2, %rdx
	movups	%xmm0, 312(%rsi,%rax)
	movq	$0, 328(%rsi,%rax)
	addq	$336, %rax              # imm = 0x150
	cmpq	%rcx, %rdx
	jl	.LBB1_16
.LBB1_17:                               # %._crit_edge
	testl	%r12d, %r12d
	je	.LBB1_18
# BB#19:
	movq	%r15, %rdi
	callq	free
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB1_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	more_functions, .Lfunc_end1-more_functions
	.cfi_endproc

	.globl	more_variables
	.p2align	4, 0x90
	.type	more_variables,@function
more_variables:                         # @more_variables
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 64
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movslq	v_count(%rip), %r13
	movq	variables(%rip), %rbx
	movq	v_names(%rip), %r14
	leaq	32(%r13), %rbp
	movl	%ebp, v_count(%rip)
	leaq	256(,%r13,8), %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %r15
	movq	%r15, variables(%rip)
	movq	%r12, %rdi
	callq	malloc
	cmpq	$4, %r13
	movq	%rax, v_names(%rip)
	jl	.LBB2_10
# BB#1:                                 # %.lr.ph21.preheader
	movl	%r13d, %eax
	movq	24(%rbx), %rcx
	movq	%rcx, 24(%r15)
	cmpl	$4, %eax
	movq	%r13, %rdx
	je	.LBB2_12
# BB#2:                                 # %.lr.ph21..lr.ph21_crit_edge.preheader
	leaq	-5(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB2_3
# BB#4:                                 # %.lr.ph21..lr.ph21_crit_edge.prol.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph21..lr.ph21_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	variables(%rip), %rdi
	movq	32(%rbx,%rcx,8), %rdx
	movq	%rdx, 32(%rdi,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB2_5
# BB#6:                                 # %.lr.ph21..lr.ph21_crit_edge.prol.loopexit.unr-lcssa
	addq	$4, %rcx
	jmp	.LBB2_7
.LBB2_10:                               # %.preheader
	cmpl	$4, %ebp
	jl	.LBB2_13
# BB#11:
	movl	$3, %edx
	jmp	.LBB2_12
.LBB2_3:
	movl	$4, %ecx
.LBB2_7:                                # %.lr.ph21..lr.ph21_crit_edge.prol.loopexit
	cmpq	$3, %r8
	movq	%r13, %rdx
	jb	.LBB2_12
	.p2align	4, 0x90
.LBB2_8:                                # %.lr.ph21..lr.ph21_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	variables(%rip), %rdx
	movq	(%rbx,%rcx,8), %rsi
	movq	%rsi, (%rdx,%rcx,8)
	movq	variables(%rip), %rdx
	movq	8(%rbx,%rcx,8), %rsi
	movq	%rsi, 8(%rdx,%rcx,8)
	movq	variables(%rip), %rdx
	movq	16(%rbx,%rcx,8), %rsi
	movq	%rsi, 16(%rdx,%rcx,8)
	movq	variables(%rip), %rdx
	movq	24(%rbx,%rcx,8), %rsi
	movq	%rsi, 24(%rdx,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB2_8
# BB#9:
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB2_12:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	variables(%rip), %rax
	movq	$0, (%rax,%rdx,8)
	incq	%rdx
	cmpq	%rbp, %rdx
	jl	.LBB2_12
.LBB2_13:                               # %._crit_edge
	testl	%r13d, %r13d
	je	.LBB2_14
# BB#15:
	movq	%rbx, %rdi
	callq	free
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB2_14:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	more_variables, .Lfunc_end2-more_variables
	.cfi_endproc

	.globl	more_arrays
	.p2align	4, 0x90
	.type	more_arrays,@function
more_arrays:                            # @more_arrays
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 48
.Lcfi32:
	.cfi_offset %rbx, -48
.Lcfi33:
	.cfi_offset %r12, -40
.Lcfi34:
	.cfi_offset %r13, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %r15, -16
	movslq	a_count(%rip), %r13
	movq	arrays(%rip), %rbx
	movq	a_names(%rip), %r14
	leaq	32(%r13), %rax
	movl	%eax, a_count(%rip)
	leaq	256(,%r13,8), %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %r15
	movq	%r15, arrays(%rip)
	movq	%r12, %rdi
	callq	malloc
	cmpq	$2, %r13
	movq	%rax, a_names(%rip)
	movl	$1, %edx
	jl	.LBB3_8
# BB#1:                                 # %.lr.ph21.preheader
	movq	8(%rbx), %rax
	movq	%rax, 8(%r15)
	cmpl	$2, %r13d
	movl	%r13d, %edx
	je	.LBB3_8
# BB#2:                                 # %.lr.ph21..lr.ph21_crit_edge.preheader
	movl	%r13d, %eax
	leal	2(%rax), %esi
	leaq	-3(%rax), %r8
	andq	$3, %rsi
	je	.LBB3_3
# BB#4:                                 # %.lr.ph21..lr.ph21_crit_edge.prol.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph21..lr.ph21_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	arrays(%rip), %rdi
	movq	16(%rbx,%rcx,8), %rdx
	movq	%rdx, 16(%rdi,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB3_5
# BB#6:                                 # %.lr.ph21..lr.ph21_crit_edge.prol.loopexit.unr-lcssa
	addq	$2, %rcx
	jmp	.LBB3_7
.LBB3_3:
	movl	$2, %ecx
.LBB3_7:                                # %.lr.ph21..lr.ph21_crit_edge.prol.loopexit
	cmpq	$3, %r8
	movl	%r13d, %edx
	jb	.LBB3_8
	.p2align	4, 0x90
.LBB3_16:                               # %.lr.ph21..lr.ph21_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	arrays(%rip), %rdx
	movq	(%rbx,%rcx,8), %rsi
	movq	%rsi, (%rdx,%rcx,8)
	movq	arrays(%rip), %rdx
	movq	8(%rbx,%rcx,8), %rsi
	movq	%rsi, 8(%rdx,%rcx,8)
	movq	arrays(%rip), %rdx
	movq	16(%rbx,%rcx,8), %rsi
	movq	%rsi, 16(%rdx,%rcx,8)
	movq	arrays(%rip), %rdx
	movq	24(%rbx,%rcx,8), %rsi
	movq	%rsi, 24(%rdx,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB3_16
# BB#17:
	movl	%r13d, %edx
.LBB3_8:                                # %.preheader
	movslq	v_count(%rip), %rax
	cmpl	%eax, %edx
	jge	.LBB3_14
# BB#9:                                 # %.lr.ph
	movslq	%edx, %rcx
	movl	%eax, %esi
	subl	%edx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB3_12
# BB#10:                                # %.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB3_11:                               # =>This Inner Loop Header: Depth=1
	movq	arrays(%rip), %rdi
	movq	$0, (%rdi,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB3_11
.LBB3_12:                               # %.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB3_14
	.p2align	4, 0x90
.LBB3_13:                               # =>This Inner Loop Header: Depth=1
	movq	arrays(%rip), %rdx
	movq	$0, (%rdx,%rcx,8)
	movq	arrays(%rip), %rdx
	movq	$0, 8(%rdx,%rcx,8)
	movq	arrays(%rip), %rdx
	movq	$0, 16(%rdx,%rcx,8)
	movq	arrays(%rip), %rdx
	movq	$0, 24(%rdx,%rcx,8)
	movq	arrays(%rip), %rdx
	movq	$0, 32(%rdx,%rcx,8)
	movq	arrays(%rip), %rdx
	movq	$0, 40(%rdx,%rcx,8)
	movq	arrays(%rip), %rdx
	movq	$0, 48(%rdx,%rcx,8)
	movq	arrays(%rip), %rdx
	movq	$0, 56(%rdx,%rcx,8)
	addq	$8, %rcx
	cmpq	%rax, %rcx
	jl	.LBB3_13
.LBB3_14:                               # %._crit_edge
	testl	%r13d, %r13d
	je	.LBB3_18
# BB#15:
	movq	%rbx, %rdi
	callq	free
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB3_18:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	more_arrays, .Lfunc_end3-more_arrays
	.cfi_endproc

	.globl	clear_func
	.p2align	4, 0x90
	.type	clear_func,@function
clear_func:                             # @clear_func
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -32
.Lcfi41:
	.cfi_offset %r14, -24
.Lcfi42:
	.cfi_offset %r15, -16
	movq	functions(%rip), %r14
	movslq	%edi, %rax
	imulq	$168, %rax, %rbx
	movb	$0, (%r14,%rbx)
	movq	8(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_2
# BB#1:
	leaq	8(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_2:
	movq	16(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_4
# BB#3:
	leaq	16(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_4:
	movq	24(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_6
# BB#5:
	leaq	24(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_6:
	movq	32(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_8
# BB#7:
	leaq	32(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_8:
	movq	40(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_10
# BB#9:
	leaq	40(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_10:
	movq	48(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_12
# BB#11:
	leaq	48(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_12:
	movq	56(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_14
# BB#13:
	leaq	56(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_14:
	movq	64(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_16
# BB#15:
	leaq	64(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_16:
	movq	72(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_18
# BB#17:
	leaq	72(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_18:
	movq	80(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_20
# BB#19:
	leaq	80(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_20:
	movq	88(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_22
# BB#21:
	leaq	88(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_22:
	movq	96(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_24
# BB#23:
	leaq	96(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_24:
	movq	104(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_26
# BB#25:
	leaq	104(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_26:
	movq	112(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_28
# BB#27:
	leaq	112(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_28:
	movq	120(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_30
# BB#29:
	leaq	120(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_30:
	movq	128(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_32
# BB#31:
	leaq	128(%r14,%rbx), %r15
	callq	free
	movq	$0, (%r15)
.LBB4_32:
	movl	$0, 136(%r14,%rbx)
	movq	160(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_34
# BB#33:
	leaq	160(%r14,%rbx), %r15
	callq	free_args
	movq	$0, (%r15)
.LBB4_34:
	movq	152(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_36
# BB#35:
	leaq	152(%r14,%rbx), %r15
	callq	free_args
	movq	$0, (%r15)
.LBB4_36:                               # %.preheader
	movq	144(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_39
# BB#37:                                # %.lr.ph
	leaq	144(%r14,%rbx), %r14
	.p2align	4, 0x90
.LBB4_38:                               # =>This Inner Loop Header: Depth=1
	movq	512(%rdi), %rbx
	callq	free
	movq	%rbx, (%r14)
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB4_38
.LBB4_39:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	clear_func, .Lfunc_end4-clear_func
	.cfi_endproc

	.globl	fpop
	.p2align	4, 0x90
	.type	fpop,@function
fpop:                                   # @fpop
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 16
.Lcfi44:
	.cfi_offset %rbx, -16
	movq	fn_stack(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB5_1
# BB#2:
	movq	8(%rdi), %rax
	movq	%rax, fn_stack(%rip)
	movl	(%rdi), %ebx
	callq	free
	jmp	.LBB5_3
.LBB5_1:
                                        # implicit-def: %EBX
.LBB5_3:
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	fpop, .Lfunc_end5-fpop
	.cfi_endproc

	.globl	fpush
	.p2align	4, 0x90
	.type	fpush,@function
fpush:                                  # @fpush
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 16
.Lcfi46:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movl	$16, %edi
	callq	malloc
	movq	fn_stack(%rip), %rcx
	movq	%rcx, 8(%rax)
	movl	%ebx, (%rax)
	movq	%rax, fn_stack(%rip)
	popq	%rbx
	retq
.Lfunc_end6:
	.size	fpush, .Lfunc_end6-fpush
	.cfi_endproc

	.globl	pop
	.p2align	4, 0x90
	.type	pop,@function
pop:                                    # @pop
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 16
.Lcfi48:
	.cfi_offset %rbx, -16
	movq	ex_stack(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB7_1
# BB#2:
	movq	8(%rbx), %rax
	movq	%rax, ex_stack(%rip)
	movq	%rbx, %rdi
	callq	free_num
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB7_1:
	popq	%rbx
	retq
.Lfunc_end7:
	.size	pop, .Lfunc_end7-pop
	.cfi_endproc

	.globl	push_copy
	.p2align	4, 0x90
	.type	push_copy,@function
push_copy:                              # @push_copy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -24
.Lcfi53:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	copy_num
	movq	%rax, (%rbx)
	movq	ex_stack(%rip), %rax
	movq	%rax, 8(%rbx)
	movq	%rbx, ex_stack(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	push_copy, .Lfunc_end8-push_copy
	.cfi_endproc

	.globl	push_num
	.p2align	4, 0x90
	.type	push_num,@function
push_num:                               # @push_num
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 16
.Lcfi55:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$16, %edi
	callq	malloc
	movq	%rbx, (%rax)
	movq	ex_stack(%rip), %rcx
	movq	%rcx, 8(%rax)
	movq	%rax, ex_stack(%rip)
	popq	%rbx
	retq
.Lfunc_end9:
	.size	push_num, .Lfunc_end9-push_num
	.cfi_endproc

	.globl	check_stack
	.p2align	4, 0x90
	.type	check_stack,@function
check_stack:                            # @check_stack
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	setg	%al
	jle	.LBB10_5
# BB#1:
	movq	ex_stack(%rip), %rcx
	testq	%rcx, %rcx
	jne	.LBB10_3
	jmp	.LBB10_5
	.p2align	4, 0x90
.LBB10_4:                               # %.lr.ph
                                        #   in Loop: Header=BB10_3 Depth=1
	decl	%edi
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB10_5
.LBB10_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$1, %edi
	setg	%al
	cmpl	$2, %edi
	jge	.LBB10_4
.LBB10_5:                               # %._crit_edge
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 16
.Lcfi57:
	.cfi_offset %rbx, -16
	movb	$1, %bl
	testb	%al, %al
	je	.LBB10_7
# BB#6:
	xorl	%ebx, %ebx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	rt_error
.LBB10_7:
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end10:
	.size	check_stack, .Lfunc_end10-check_stack
	.cfi_endproc

	.globl	get_var
	.p2align	4, 0x90
	.type	get_var,@function
get_var:                                # @get_var
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 32
.Lcfi61:
	.cfi_offset %rbx, -32
.Lcfi62:
	.cfi_offset %r14, -24
.Lcfi63:
	.cfi_offset %r15, -16
	movq	variables(%rip), %r14
	movslq	%edi, %r15
	movq	(%r14,%r15,8), %rbx
	testq	%rbx, %rbx
	jne	.LBB11_2
# BB#1:
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, (%r14,%r15,8)
	movq	%rbx, %rdi
	callq	init_num
.LBB11_2:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	get_var, .Lfunc_end11-get_var
	.cfi_endproc

	.globl	get_array_num
	.p2align	4, 0x90
	.type	get_array_num,@function
get_array_num:                          # @get_array_num
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi68:
	.cfi_def_cfa_offset 64
.Lcfi69:
	.cfi_offset %rbx, -40
.Lcfi70:
	.cfi_offset %r12, -32
.Lcfi71:
	.cfi_offset %r14, -24
.Lcfi72:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	arrays(%rip), %r14
	movslq	%edi, %r12
	movq	(%r14,%r12,8), %r15
	testq	%r15, %r15
	jne	.LBB12_2
# BB#1:
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r15
	movq	%r15, (%r14,%r12,8)
	movq	$0, (%r15)
	movq	$0, 16(%r15)
	movb	$0, 8(%r15)
.LBB12_2:
	movq	(%r15), %r14
	testq	%r14, %r14
	jne	.LBB12_4
# BB#3:
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r14
	movq	%r14, (%r15)
	movq	$0, (%r14)
	movw	$0, 8(%r14)
.LBB12_4:
	movl	%ebx, %eax
	andl	$15, %eax
	movl	%eax, (%rsp)
	shrq	$4, %rbx
	movzwl	8(%r14), %ecx
	movswl	%cx, %eax
	testl	%ebx, %ebx
	jg	.LBB12_9
# BB#5:
	cmpl	$2, %eax
	jge	.LBB12_9
# BB#6:
	movl	$1, %r15d
	cmpl	%eax, %r15d
	jg	.LBB12_12
	jmp	.LBB12_8
.LBB12_9:                               # %.critedge.preheader
	movswq	%cx, %rcx
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB12_10:                              # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %edx
	andl	$15, %edx
	movl	%edx, (%rsp,%r15,4)
	sarl	$4, %ebx
	incq	%r15
	testl	%ebx, %ebx
	jg	.LBB12_10
# BB#11:                                # %.critedge
                                        #   in Loop: Header=BB12_10 Depth=1
	cmpq	%rcx, %r15
	jl	.LBB12_10
# BB#7:                                 # %.preheader77
	cmpl	%eax, %r15d
	jle	.LBB12_8
	.p2align	4, 0x90
.LBB12_12:                              # =>This Inner Loop Header: Depth=1
	movl	$256, %edi              # imm = 0x100
	callq	malloc
	movq	%rax, %rbx
	cmpw	$0, 8(%r14)
	je	.LBB12_13
# BB#14:                                # %.loopexit75.loopexit86
                                        #   in Loop: Header=BB12_12 Depth=1
	movq	(%r14), %rax
	movq	%rax, 128(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 232(%rbx)
	movups	%xmm0, 216(%rbx)
	movups	%xmm0, 200(%rbx)
	movups	%xmm0, 184(%rbx)
	movups	%xmm0, 168(%rbx)
	movups	%xmm0, 152(%rbx)
	movups	%xmm0, 136(%rbx)
	movq	$0, 248(%rbx)
	jmp	.LBB12_15
	.p2align	4, 0x90
.LBB12_13:                              # %.preheader74
                                        #   in Loop: Header=BB12_12 Depth=1
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, (%rbx)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 8(%rbx)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 16(%rbx)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 24(%rbx)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 32(%rbx)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 40(%rbx)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 48(%rbx)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 56(%rbx)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 64(%rbx)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 72(%rbx)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 80(%rbx)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 88(%rbx)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 96(%rbx)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 104(%rbx)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 112(%rbx)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 120(%rbx)
.LBB12_15:                              # %.loopexit75
                                        #   in Loop: Header=BB12_12 Depth=1
	movq	%rbx, (%r14)
	movzwl	8(%r14), %eax
	incl	%eax
	movw	%ax, 8(%r14)
	cwtl
	cmpl	%eax, %r15d
	jg	.LBB12_12
	jmp	.LBB12_16
.LBB12_8:                               # %.preheader77.._crit_edge84_crit_edge
	movq	(%r14), %rbx
.LBB12_16:                              # %._crit_edge84
	cmpl	$2, %r15d
	jl	.LBB12_17
# BB#18:                                # %.lr.ph.preheader
	movslq	%r15d, %r15
	decq	%r15
	jmp	.LBB12_19
	.p2align	4, 0x90
.LBB12_21:                              # %.lr.ph.backedge
                                        #   in Loop: Header=BB12_19 Depth=1
	decq	%r15
	movq	%r14, %rbx
.LBB12_19:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rsp,%r15,4), %r12
	movq	128(%rbx,%r12,8), %r14
	testq	%r14, %r14
	je	.LBB12_22
# BB#20:                                # %.backedge
                                        #   in Loop: Header=BB12_19 Depth=1
	cmpq	$2, %r15
	jge	.LBB12_21
	jmp	.LBB12_24
	.p2align	4, 0x90
.LBB12_22:                              #   in Loop: Header=BB12_19 Depth=1
	movl	$256, %edi              # imm = 0x100
	callq	malloc
	movq	%rax, %r14
	movq	%r14, 128(%rbx,%r12,8)
	cmpq	$1, %r15
	jle	.LBB12_23
# BB#25:                                # %.backedge.thread
                                        #   in Loop: Header=BB12_19 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, 240(%r14)
	movups	%xmm0, 224(%r14)
	movups	%xmm0, 208(%r14)
	movups	%xmm0, 192(%r14)
	movups	%xmm0, 176(%r14)
	movups	%xmm0, 160(%r14)
	movups	%xmm0, 144(%r14)
	movups	%xmm0, 128(%r14)
	jmp	.LBB12_21
.LBB12_17:
	movq	%rbx, %r14
	jmp	.LBB12_24
.LBB12_23:                              # %.backedge.thread96
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, (%r14)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 8(%r14)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 16(%r14)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 24(%r14)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 32(%r14)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 40(%r14)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 48(%r14)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 56(%r14)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 64(%r14)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 72(%r14)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 80(%r14)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 88(%r14)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 96(%r14)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 104(%r14)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 112(%r14)
	movq	_zero_(%rip), %rdi
	callq	copy_num
	movq	%rax, 120(%r14)
.LBB12_24:                              # %._crit_edge
	movslq	(%rsp), %rax
	leaq	(%r14,%rax,8), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	get_array_num, .Lfunc_end12-get_array_num
	.cfi_endproc

	.globl	store_var
	.p2align	4, 0x90
	.type	store_var,@function
store_var:                              # @store_var
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 32
.Lcfi76:
	.cfi_offset %rbx, -32
.Lcfi77:
	.cfi_offset %r14, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	cmpl	$3, %ebp
	jl	.LBB13_4
# BB#1:
	movq	variables(%rip), %r14
	movslq	%ebp, %rbp
	movq	(%r14,%rbp,8), %rbx
	testq	%rbx, %rbx
	jne	.LBB13_3
# BB#2:                                 # %get_var.exit
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, (%r14,%rbp,8)
	movq	%rbx, %rdi
	callq	init_num
	testq	%rbx, %rbx
	je	.LBB13_36
.LBB13_3:                               # %get_var.exit.thread
	movq	%rbx, %rdi
	callq	free_num
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	copy_num
	movq	%rax, (%rbx)
.LBB13_36:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB13_4:
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	is_neg
	testb	%al, %al
	je	.LBB13_9
# BB#5:
	xorl	%ebx, %ebx
	cmpl	$2, %ebp
	je	.LBB13_31
# BB#6:
	cmpl	$1, %ebp
	je	.LBB13_25
# BB#7:
	testl	%ebp, %ebp
	jne	.LBB13_8
# BB#17:                                # %.thread41
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	rt_warn
	movl	$2, %r14d
	cmpq	$16, %r14
	jle	.LBB13_19
	jmp	.LBB13_20
.LBB13_9:
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	num2long
	movq	%rax, %r14
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	is_zero
	testb	%al, %al
	sete	%al
	testq	%r14, %r14
	sete	%bl
	andb	%al, %bl
	cmpl	$2, %ebp
	jne	.LBB13_11
	jmp	.LBB13_32
.LBB13_31:                              # %.thread38
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	rt_warn
	xorl	%r14d, %r14d
	testb	%bl, %bl
	je	.LBB13_33
	jmp	.LBB13_34
.LBB13_25:                              # %.thread45
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	rt_warn
	movl	$2, %r14d
	cmpq	$99, %r14
	jle	.LBB13_27
	jmp	.LBB13_28
.LBB13_8:
                                        # implicit-def: %R14
	cmpl	$2, %ebp
	je	.LBB13_32
.LBB13_11:
	cmpl	$1, %ebp
	je	.LBB13_22
# BB#12:
	testl	%ebp, %ebp
	jne	.LBB13_36
# BB#13:
	cmpq	$1, %r14
	jg	.LBB13_18
# BB#14:
	testb	%bl, %bl
	jne	.LBB13_18
# BB#15:
	movl	$2, i_base(%rip)
	movl	$.L.str.5, %edi
	jmp	.LBB13_16
.LBB13_32:
	testb	%bl, %bl
	jne	.LBB13_34
.LBB13_33:
	cmpq	$100, %r14
	jge	.LBB13_34
# BB#35:
	movl	%r14d, scale(%rip)
	jmp	.LBB13_36
.LBB13_18:
	cmpq	$16, %r14
	jg	.LBB13_20
.LBB13_19:
	testb	%bl, %bl
	jne	.LBB13_20
# BB#21:
	movl	%r14d, i_base(%rip)
	jmp	.LBB13_36
.LBB13_34:
	movl	$99, scale(%rip)
	movl	$.L.str.9, %edi
	jmp	.LBB13_29
.LBB13_22:
	cmpq	$1, %r14
	jg	.LBB13_26
# BB#23:
	testb	%bl, %bl
	jne	.LBB13_26
# BB#24:
	movl	$2, o_base(%rip)
	movl	$.L.str.7, %edi
	jmp	.LBB13_16
.LBB13_26:
	cmpq	$99, %r14
	jg	.LBB13_28
.LBB13_27:
	testb	%bl, %bl
	jne	.LBB13_28
# BB#30:
	movl	%r14d, o_base(%rip)
	jmp	.LBB13_36
.LBB13_20:
	movl	$16, i_base(%rip)
	movl	$.L.str.6, %edi
.LBB13_16:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	rt_warn                 # TAILCALL
.LBB13_28:
	movl	$99, o_base(%rip)
	movl	$.L.str.8, %edi
.LBB13_29:
	movl	$99, %esi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	rt_warn                 # TAILCALL
.Lfunc_end13:
	.size	store_var, .Lfunc_end13-store_var
	.cfi_endproc

	.globl	store_array
	.p2align	4, 0x90
	.type	store_array,@function
store_array:                            # @store_array
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi81:
	.cfi_def_cfa_offset 32
.Lcfi82:
	.cfi_offset %rbx, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movq	ex_stack(%rip), %rax
	testq	%rax, %rax
	je	.LBB14_11
# BB#1:                                 # %.lr.ph.i
	cmpq	$0, 8(%rax)
	je	.LBB14_2
# BB#10:                                # %.lr.ph.i.1
	xorl	%ecx, %ecx
	testb	%cl, %cl
	jne	.LBB14_11
	jmp	.LBB14_4
.LBB14_2:
	movb	$1, %cl
	testb	%cl, %cl
	je	.LBB14_4
.LBB14_11:                              # %check_stack.exit.thread
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	rt_error                # TAILCALL
.LBB14_4:                               # %check_stack.exit
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	callq	num2long
	movq	%rax, %rbx
	cmpq	$2048, %rbx             # imm = 0x800
	jbe	.LBB14_5
.LBB14_12:
	movq	a_names(%rip), %rax
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rsi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	rt_error                # TAILCALL
.LBB14_5:
	testq	%rbx, %rbx
	jne	.LBB14_7
# BB#6:
	movq	ex_stack(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	callq	is_zero
	testb	%al, %al
	je	.LBB14_12
.LBB14_7:
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	get_array_num
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB14_9
# BB#8:
	movq	%rbx, %rdi
	callq	free_num
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	copy_num
	movq	%rax, (%rbx)
	movq	ex_stack(%rip), %rax
	movq	8(%rax), %rdi
	callq	free_num
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, (%rax)
	movq	ex_stack(%rip), %rdi
	callq	init_num
	movq	ex_stack(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB14_9
# BB#13:
	movq	8(%rbx), %rax
	movq	%rax, ex_stack(%rip)
	movq	%rbx, %rdi
	callq	free_num
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB14_9:                               # %pop.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end14:
	.size	store_array, .Lfunc_end14-store_array
	.cfi_endproc

	.globl	load_var
	.p2align	4, 0x90
	.type	load_var,@function
load_var:                               # @load_var
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi86:
	.cfi_def_cfa_offset 32
.Lcfi87:
	.cfi_offset %rbx, -24
.Lcfi88:
	.cfi_offset %r14, -16
	cmpl	$2, %edi
	je	.LBB15_6
# BB#1:
	cmpl	$1, %edi
	je	.LBB15_5
# BB#2:
	testl	%edi, %edi
	jne	.LBB15_7
# BB#3:
	movq	_zero_(%rip), %r14
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	copy_num
	movq	%rax, (%rbx)
	movq	ex_stack(%rip), %rax
	movq	%rax, 8(%rbx)
	movq	%rbx, ex_stack(%rip)
	movl	i_base(%rip), %esi
	jmp	.LBB15_4
.LBB15_6:
	movq	_zero_(%rip), %r14
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	copy_num
	movq	%rax, (%rbx)
	movq	ex_stack(%rip), %rax
	movq	%rax, 8(%rbx)
	movq	%rbx, ex_stack(%rip)
	movl	scale(%rip), %esi
	jmp	.LBB15_4
.LBB15_5:
	movq	_zero_(%rip), %r14
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	copy_num
	movq	%rax, (%rbx)
	movq	ex_stack(%rip), %rax
	movq	%rax, 8(%rbx)
	movq	%rbx, ex_stack(%rip)
	movl	o_base(%rip), %esi
.LBB15_4:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	int2num                 # TAILCALL
.LBB15_7:
	movq	variables(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	testq	%rax, %rax
	je	.LBB15_9
# BB#8:
	movq	(%rax), %r14
	jmp	.LBB15_10
.LBB15_9:
	movq	_zero_(%rip), %r14
.LBB15_10:
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	copy_num
	movq	%rax, (%rbx)
	movq	ex_stack(%rip), %rax
	movq	%rax, 8(%rbx)
	movq	%rbx, ex_stack(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end15:
	.size	load_var, .Lfunc_end15-load_var
	.cfi_endproc

	.globl	load_array
	.p2align	4, 0x90
	.type	load_array,@function
load_array:                             # @load_array
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 32
.Lcfi92:
	.cfi_offset %rbx, -24
.Lcfi93:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movq	ex_stack(%rip), %rax
	testq	%rax, %rax
	je	.LBB16_9
# BB#1:                                 # %check_stack.exit
	movq	(%rax), %rdi
	callq	num2long
	movq	%rax, %rbx
	cmpq	$2048, %rbx             # imm = 0x800
	jbe	.LBB16_2
.LBB16_10:
	movq	a_names(%rip), %rax
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rsi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	rt_error                # TAILCALL
.LBB16_9:                               # %check_stack.exit.thread
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	rt_error                # TAILCALL
.LBB16_2:
	testq	%rbx, %rbx
	jne	.LBB16_4
# BB#3:
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	is_zero
	testb	%al, %al
	je	.LBB16_10
.LBB16_4:
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	get_array_num
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB16_8
# BB#5:
	movq	ex_stack(%rip), %rbp
	testq	%rbp, %rbp
	je	.LBB16_7
# BB#6:
	movq	8(%rbp), %rax
	movq	%rax, ex_stack(%rip)
	movq	%rbp, %rdi
	callq	free_num
	movq	%rbp, %rdi
	callq	free
.LBB16_7:                               # %pop.exit
	movq	(%rbx), %rbx
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	copy_num
	movq	%rax, (%rbp)
	movq	ex_stack(%rip), %rax
	movq	%rax, 8(%rbp)
	movq	%rbp, ex_stack(%rip)
.LBB16_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end16:
	.size	load_array, .Lfunc_end16-load_array
	.cfi_endproc

	.globl	decr_var
	.p2align	4, 0x90
	.type	decr_var,@function
decr_var:                               # @decr_var
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 32
.Lcfi97:
	.cfi_offset %rbx, -32
.Lcfi98:
	.cfi_offset %r14, -24
.Lcfi99:
	.cfi_offset %r15, -16
	cmpl	$2, %edi
	je	.LBB17_10
# BB#1:
	cmpl	$1, %edi
	je	.LBB17_7
# BB#2:
	testl	%edi, %edi
	jne	.LBB17_13
# BB#3:
	movl	i_base(%rip), %eax
	cmpl	$3, %eax
	jl	.LBB17_5
# BB#4:
	decl	%eax
	movl	%eax, i_base(%rip)
	jmp	.LBB17_15
.LBB17_10:
	movl	scale(%rip), %eax
	testl	%eax, %eax
	jle	.LBB17_12
# BB#11:
	decl	%eax
	movl	%eax, scale(%rip)
	jmp	.LBB17_15
.LBB17_7:
	movl	o_base(%rip), %eax
	cmpl	$3, %eax
	jl	.LBB17_9
# BB#8:
	decl	%eax
	movl	%eax, o_base(%rip)
	jmp	.LBB17_15
.LBB17_13:
	movq	variables(%rip), %r14
	movslq	%edi, %r15
	movq	(%r14,%r15,8), %rbx
	testq	%rbx, %rbx
	jne	.LBB17_16
# BB#14:                                # %get_var.exit
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, (%r14,%r15,8)
	movq	%rbx, %rdi
	callq	init_num
	testq	%rbx, %rbx
	je	.LBB17_15
.LBB17_16:                              # %get_var.exit.thread
	movq	(%rbx), %rdi
	movq	_one_(%rip), %rsi
	movq	%rbx, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	bc_sub                  # TAILCALL
.LBB17_15:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB17_5:
	movl	$.L.str.11, %edi
	jmp	.LBB17_6
.LBB17_9:
	movl	$.L.str.12, %edi
	jmp	.LBB17_6
.LBB17_12:
	movl	$.L.str.13, %edi
.LBB17_6:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	rt_warn                 # TAILCALL
.Lfunc_end17:
	.size	decr_var, .Lfunc_end17-decr_var
	.cfi_endproc

	.globl	decr_array
	.p2align	4, 0x90
	.type	decr_array,@function
decr_array:                             # @decr_array
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi102:
	.cfi_def_cfa_offset 32
.Lcfi103:
	.cfi_offset %rbx, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movq	ex_stack(%rip), %rax
	testq	%rax, %rax
	je	.LBB18_9
# BB#1:                                 # %check_stack.exit
	movq	(%rax), %rdi
	callq	num2long
	movq	%rax, %rbx
	cmpq	$2048, %rbx             # imm = 0x800
	jbe	.LBB18_2
.LBB18_10:
	movq	a_names(%rip), %rax
	movsbq	%bpl, %rcx
	movq	(%rax,%rcx,8), %rsi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	rt_error                # TAILCALL
.LBB18_9:                               # %check_stack.exit.thread
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	rt_error                # TAILCALL
.LBB18_2:
	testq	%rbx, %rbx
	jne	.LBB18_4
# BB#3:
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	is_zero
	testb	%al, %al
	je	.LBB18_10
.LBB18_4:
	movsbl	%bpl, %edi
	movq	%rbx, %rsi
	callq	get_array_num
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB18_8
# BB#5:
	movq	ex_stack(%rip), %rbp
	testq	%rbp, %rbp
	je	.LBB18_7
# BB#6:
	movq	8(%rbp), %rax
	movq	%rax, ex_stack(%rip)
	movq	%rbp, %rdi
	callq	free_num
	movq	%rbp, %rdi
	callq	free
.LBB18_7:                               # %pop.exit
	movq	(%rbx), %rdi
	movq	_one_(%rip), %rsi
	movq	%rbx, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	bc_sub                  # TAILCALL
.LBB18_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end18:
	.size	decr_array, .Lfunc_end18-decr_array
	.cfi_endproc

	.globl	incr_var
	.p2align	4, 0x90
	.type	incr_var,@function
incr_var:                               # @incr_var
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 32
.Lcfi108:
	.cfi_offset %rbx, -32
.Lcfi109:
	.cfi_offset %r14, -24
.Lcfi110:
	.cfi_offset %r15, -16
	cmpl	$2, %edi
	je	.LBB19_10
# BB#1:
	cmpl	$1, %edi
	je	.LBB19_7
# BB#2:
	testl	%edi, %edi
	jne	.LBB19_13
# BB#3:
	movl	i_base(%rip), %eax
	cmpl	$15, %eax
	jg	.LBB19_5
# BB#4:
	incl	%eax
	movl	%eax, i_base(%rip)
	jmp	.LBB19_15
.LBB19_10:
	movl	scale(%rip), %eax
	cmpl	$98, %eax
	jg	.LBB19_12
# BB#11:
	incl	%eax
	movl	%eax, scale(%rip)
	jmp	.LBB19_15
.LBB19_7:
	movl	o_base(%rip), %eax
	cmpl	$98, %eax
	jg	.LBB19_9
# BB#8:
	incl	%eax
	movl	%eax, o_base(%rip)
	jmp	.LBB19_15
.LBB19_13:
	movq	variables(%rip), %r14
	movslq	%edi, %r15
	movq	(%r14,%r15,8), %rbx
	testq	%rbx, %rbx
	jne	.LBB19_16
# BB#14:                                # %get_var.exit
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, (%r14,%r15,8)
	movq	%rbx, %rdi
	callq	init_num
	testq	%rbx, %rbx
	je	.LBB19_15
.LBB19_16:                              # %get_var.exit.thread
	movq	(%rbx), %rdi
	movq	_one_(%rip), %rsi
	movq	%rbx, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	bc_add                  # TAILCALL
.LBB19_15:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB19_5:
	movl	$.L.str.14, %edi
	jmp	.LBB19_6
.LBB19_12:
	movl	$.L.str.16, %edi
	jmp	.LBB19_6
.LBB19_9:
	movl	$.L.str.15, %edi
.LBB19_6:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	rt_warn                 # TAILCALL
.Lfunc_end19:
	.size	incr_var, .Lfunc_end19-incr_var
	.cfi_endproc

	.globl	incr_array
	.p2align	4, 0x90
	.type	incr_array,@function
incr_array:                             # @incr_array
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi113:
	.cfi_def_cfa_offset 32
.Lcfi114:
	.cfi_offset %rbx, -24
.Lcfi115:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movq	ex_stack(%rip), %rax
	testq	%rax, %rax
	je	.LBB20_9
# BB#1:                                 # %check_stack.exit
	movq	(%rax), %rdi
	callq	num2long
	movq	%rax, %rbx
	cmpq	$2048, %rbx             # imm = 0x800
	jbe	.LBB20_2
.LBB20_10:
	movq	a_names(%rip), %rax
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rsi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	rt_error                # TAILCALL
.LBB20_9:                               # %check_stack.exit.thread
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	rt_error                # TAILCALL
.LBB20_2:
	testq	%rbx, %rbx
	jne	.LBB20_4
# BB#3:
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	is_zero
	testb	%al, %al
	je	.LBB20_10
.LBB20_4:
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	get_array_num
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB20_8
# BB#5:
	movq	ex_stack(%rip), %rbp
	testq	%rbp, %rbp
	je	.LBB20_7
# BB#6:
	movq	8(%rbp), %rax
	movq	%rax, ex_stack(%rip)
	movq	%rbp, %rdi
	callq	free_num
	movq	%rbp, %rdi
	callq	free
.LBB20_7:                               # %pop.exit
	movq	(%rbx), %rdi
	movq	_one_(%rip), %rsi
	movq	%rbx, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	bc_add                  # TAILCALL
.LBB20_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end20:
	.size	incr_array, .Lfunc_end20-incr_array
	.cfi_endproc

	.globl	auto_var
	.p2align	4, 0x90
	.type	auto_var,@function
auto_var:                               # @auto_var
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi116:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi118:
	.cfi_def_cfa_offset 32
.Lcfi119:
	.cfi_offset %rbx, -24
.Lcfi120:
	.cfi_offset %r14, -16
	movl	%edi, %ebx
	testl	%ebx, %ebx
	jle	.LBB21_2
# BB#1:
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r14
	movq	variables(%rip), %rax
	movslq	%ebx, %rbx
	movq	(%rax,%rbx,8), %rax
	movq	%rax, 8(%r14)
	movq	%r14, %rdi
	callq	init_num
	movq	variables(%rip), %rax
	movq	%r14, (%rax,%rbx,8)
	jmp	.LBB21_3
.LBB21_2:
	negl	%ebx
	movl	$24, %edi
	callq	malloc
	movq	arrays(%rip), %rcx
	movslq	%ebx, %rdx
	movq	(%rcx,%rdx,8), %rsi
	movq	%rsi, 16(%rax)
	movq	$0, (%rax)
	movb	$0, 8(%rax)
	movq	%rax, (%rcx,%rdx,8)
.LBB21_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end21:
	.size	auto_var, .Lfunc_end21-auto_var
	.cfi_endproc

	.globl	free_a_tree
	.p2align	4, 0x90
	.type	free_a_tree,@function
free_a_tree:                            # @free_a_tree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi123:
	.cfi_def_cfa_offset 32
.Lcfi124:
	.cfi_offset %rbx, -24
.Lcfi125:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB22_5
# BB#1:
	cmpl	$1, %ebp
	jle	.LBB22_2
# BB#3:                                 # %.preheader
	decl	%ebp
	movq	128(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	movq	136(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	movq	144(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	movq	152(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	movq	160(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	movq	168(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	movq	176(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	movq	184(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	movq	192(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	movq	200(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	movq	208(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	movq	216(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	movq	224(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	movq	232(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	movq	240(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	movq	248(%rbx), %rdi
	movl	%ebp, %esi
	callq	free_a_tree
	jmp	.LBB22_4
.LBB22_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB22_2:                               # %.preheader12.preheader
	movq	%rbx, %rdi
	callq	free_num
	leaq	8(%rbx), %rdi
	callq	free_num
	leaq	16(%rbx), %rdi
	callq	free_num
	leaq	24(%rbx), %rdi
	callq	free_num
	leaq	32(%rbx), %rdi
	callq	free_num
	leaq	40(%rbx), %rdi
	callq	free_num
	leaq	48(%rbx), %rdi
	callq	free_num
	leaq	56(%rbx), %rdi
	callq	free_num
	leaq	64(%rbx), %rdi
	callq	free_num
	leaq	72(%rbx), %rdi
	callq	free_num
	leaq	80(%rbx), %rdi
	callq	free_num
	leaq	88(%rbx), %rdi
	callq	free_num
	leaq	96(%rbx), %rdi
	callq	free_num
	leaq	104(%rbx), %rdi
	callq	free_num
	leaq	112(%rbx), %rdi
	callq	free_num
	leaq	120(%rbx), %rdi
	callq	free_num
.LBB22_4:                               # %.loopexit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end22:
	.size	free_a_tree, .Lfunc_end22-free_a_tree
	.cfi_endproc

	.globl	pop_vars
	.p2align	4, 0x90
	.type	pop_vars,@function
pop_vars:                               # @pop_vars
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi126:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi128:
	.cfi_def_cfa_offset 32
.Lcfi129:
	.cfi_offset %rbx, -24
.Lcfi130:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB23_2
	jmp	.LBB23_11
	.p2align	4, 0x90
.LBB23_10:                              #   in Loop: Header=BB23_2 Depth=1
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB23_11
.LBB23_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB23_5
# BB#3:                                 #   in Loop: Header=BB23_2 Depth=1
	movq	variables(%rip), %rcx
	movq	(%rcx,%rax,8), %r14
	testq	%r14, %r14
	je	.LBB23_10
# BB#4:                                 #   in Loop: Header=BB23_2 Depth=1
	movq	8(%r14), %rdx
	movq	%rdx, (%rcx,%rax,8)
	movq	%r14, %rdi
	callq	free_num
	jmp	.LBB23_9
	.p2align	4, 0x90
.LBB23_5:                               #   in Loop: Header=BB23_2 Depth=1
	negl	%eax
	movq	arrays(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %r14
	testq	%r14, %r14
	je	.LBB23_10
# BB#6:                                 #   in Loop: Header=BB23_2 Depth=1
	movq	16(%r14), %rdx
	movq	%rdx, (%rcx,%rax,8)
	cmpb	$0, 8(%r14)
	jne	.LBB23_9
# BB#7:                                 #   in Loop: Header=BB23_2 Depth=1
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.LBB23_9
# BB#8:                                 #   in Loop: Header=BB23_2 Depth=1
	movq	(%rax), %rdi
	movswl	8(%rax), %esi
	callq	free_a_tree
	movq	(%r14), %rdi
	callq	free
	.p2align	4, 0x90
.LBB23_9:                               #   in Loop: Header=BB23_2 Depth=1
	movq	%r14, %rdi
	callq	free
	jmp	.LBB23_10
.LBB23_11:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end23:
	.size	pop_vars, .Lfunc_end23-pop_vars
	.cfi_endproc

	.globl	process_params
	.p2align	4, 0x90
	.type	process_params,@function
process_params:                         # @process_params
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi133:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 48
.Lcfi136:
	.cfi_offset %rbx, -48
.Lcfi137:
	.cfi_offset %r12, -40
.Lcfi138:
	.cfi_offset %r13, -32
.Lcfi139:
	.cfi_offset %r14, -24
.Lcfi140:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	functions(%rip), %rax
	movslq	%esi, %rcx
	imulq	$168, %rcx, %rcx
	movq	152(%rax,%rcx), %r12
	jmp	.LBB24_1
	.p2align	4, 0x90
.LBB24_21:                              # %pop.exit
                                        #   in Loop: Header=BB24_1 Depth=1
	movq	8(%r12), %r12
.LBB24_1:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	byte
	testq	%r12, %r12
	setne	%cl
	cmpb	$58, %al
	je	.LBB24_22
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB24_1 Depth=1
	testb	$1, %cl
	je	.LBB24_24
# BB#3:                                 #   in Loop: Header=BB24_1 Depth=1
	cmpb	$48, %al
	je	.LBB24_8
# BB#4:                                 #   in Loop: Header=BB24_1 Depth=1
	cmpb	$49, %al
	jne	.LBB24_5
# BB#10:                                #   in Loop: Header=BB24_1 Depth=1
	movl	(%r12), %ebx
	testl	%ebx, %ebx
	jns	.LBB24_17
# BB#11:                                #   in Loop: Header=BB24_1 Depth=1
	movq	ex_stack(%rip), %rax
	movq	(%rax), %rdi
	callq	num2long
	movq	%rax, %r15
	xorl	%esi, %esi
	movl	%r15d, %edi
	callq	get_array_num
	movslq	(%r12), %rbx
	testq	%rbx, %rbx
	jle	.LBB24_13
# BB#12:                                #   in Loop: Header=BB24_1 Depth=1
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r13
	movq	variables(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	movq	%rax, 8(%r13)
	movq	%r13, %rdi
	callq	init_num
	movq	variables(%rip), %rax
	movq	%r13, (%rax,%rbx,8)
	jmp	.LBB24_14
	.p2align	4, 0x90
.LBB24_8:                               #   in Loop: Header=BB24_1 Depth=1
	movslq	(%r12), %rbx
	testq	%rbx, %rbx
	jle	.LBB24_6
# BB#9:                                 #   in Loop: Header=BB24_1 Depth=1
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r15
	movq	variables(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	movq	%rax, 8(%r15)
	movq	ex_stack(%rip), %rdi
	movq	(%rdi), %rax
	movq	%rax, (%r15)
	callq	init_num
	movq	variables(%rip), %rax
	movq	%r15, (%rax,%rbx,8)
	jmp	.LBB24_19
	.p2align	4, 0x90
.LBB24_5:                               # %._crit_edge48
                                        #   in Loop: Header=BB24_1 Depth=1
	movl	(%r12), %ebx
.LBB24_6:                               #   in Loop: Header=BB24_1 Depth=1
	testl	%ebx, %ebx
	js	.LBB24_7
.LBB24_17:                              # %.thread
                                        #   in Loop: Header=BB24_1 Depth=1
	movq	v_names(%rip), %rax
	movslq	%ebx, %rcx
	movq	(%rax,%rcx,8), %rsi
	movl	$.L.str.18, %edi
	jmp	.LBB24_18
.LBB24_7:                               #   in Loop: Header=BB24_1 Depth=1
	movq	a_names(%rip), %rax
	negl	%ebx
	movslq	%ebx, %rcx
	movq	(%rax,%rcx,8), %rsi
	movl	$.L.str.17, %edi
.LBB24_18:                              #   in Loop: Header=BB24_1 Depth=1
	xorl	%eax, %eax
	callq	rt_error
	addq	$16, %r12
.LBB24_19:                              #   in Loop: Header=BB24_1 Depth=1
	movq	ex_stack(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB24_21
# BB#20:                                #   in Loop: Header=BB24_1 Depth=1
	movq	8(%rbx), %rax
	movq	%rax, ex_stack(%rip)
	movq	%rbx, %rdi
	callq	free_num
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB24_21
.LBB24_13:                              #   in Loop: Header=BB24_1 Depth=1
	negl	%ebx
	movl	$24, %edi
	callq	malloc
	movq	arrays(%rip), %rcx
	movslq	%ebx, %rdx
	movq	(%rcx,%rdx,8), %rsi
	movq	%rsi, 16(%rax)
	movq	$0, (%rax)
	movb	$0, 8(%rax)
	movq	%rax, (%rcx,%rdx,8)
.LBB24_14:                              # %auto_var.exit
                                        #   in Loop: Header=BB24_1 Depth=1
	movslq	(%r12), %rax
	negq	%rax
	movq	arrays(%rip), %rcx
	movslq	%r15d, %rdx
	movq	(%rcx,%rdx,8), %rdx
	cmpl	%eax, %r15d
	jne	.LBB24_16
# BB#15:                                #   in Loop: Header=BB24_1 Depth=1
	movq	16(%rdx), %rdx
.LBB24_16:                              #   in Loop: Header=BB24_1 Depth=1
	movq	(%rcx,%rax,8), %rax
	movb	$1, 8(%rax)
	movq	(%rdx), %rcx
	movq	%rcx, (%rax)
	jmp	.LBB24_19
.LBB24_22:                              # %._crit_edge
	testb	%cl, %cl
	je	.LBB24_23
# BB#25:
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	rt_error                # TAILCALL
.LBB24_23:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB24_24:
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	rt_error
.Lfunc_end24:
	.size	process_params, .Lfunc_end24-process_params
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"(main)"
	.size	.L.str, 7

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Stack error."
	.size	.L.str.1, 13

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"negative ibase, set to 2"
	.size	.L.str.2, 25

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"negative obase, set to 2"
	.size	.L.str.3, 25

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"negative scale, set to 0"
	.size	.L.str.4, 25

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"ibase too small, set to 2"
	.size	.L.str.5, 26

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"ibase too large, set to 16"
	.size	.L.str.6, 27

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"obase too small, set to 2"
	.size	.L.str.7, 26

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"obase too large, set to %d"
	.size	.L.str.8, 27

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"scale too large, set to %d"
	.size	.L.str.9, 27

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Array %s subscript out of bounds."
	.size	.L.str.10, 34

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"ibase too small in --"
	.size	.L.str.11, 22

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"obase too small in --"
	.size	.L.str.12, 22

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"scale can not be negative in -- "
	.size	.L.str.13, 33

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"ibase too big in ++"
	.size	.L.str.14, 20

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"obase too big in ++"
	.size	.L.str.15, 20

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Scale too big in ++"
	.size	.L.str.16, 20

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Parameter type mismatch parameter %s."
	.size	.L.str.17, 38

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Parameter type mismatch, parameter %s."
	.size	.L.str.18, 39

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Parameter number mismatch"
	.size	.L.str.19, 26


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
