	.text
	.file	"ZipItem.bc"
	.globl	_ZN8NArchive4NZipeqERKNS0_8CVersionES3_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZipeqERKNS0_8CVersionES3_,@function
_ZN8NArchive4NZipeqERKNS0_8CVersionES3_: # @_ZN8NArchive4NZipeqERKNS0_8CVersionES3_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB0_1
# BB#2:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	sete	%al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB0_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end0:
	.size	_ZN8NArchive4NZipeqERKNS0_8CVersionES3_, .Lfunc_end0-_ZN8NArchive4NZipeqERKNS0_8CVersionES3_
	.cfi_endproc

	.globl	_ZN8NArchive4NZipneERKNS0_8CVersionES3_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZipneERKNS0_8CVersionES3_,@function
_ZN8NArchive4NZipneERKNS0_8CVersionES3_: # @_ZN8NArchive4NZipneERKNS0_8CVersionES3_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %cl
	movb	$1, %al
	cmpb	(%rsi), %cl
	jne	.LBB1_2
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	setne	%al
.LBB1_2:                                # %_ZN8NArchive4NZipeqERKNS0_8CVersionES3_.exit
	retq
.Lfunc_end1:
	.size	_ZN8NArchive4NZipneERKNS0_8CVersionES3_, .Lfunc_end1-_ZN8NArchive4NZipneERKNS0_8CVersionES3_
	.cfi_endproc

	.globl	_ZNK8NArchive4NZip14CExtraSubBlock15ExtractNtfsTimeEiR9_FILETIME
	.p2align	4, 0x90
	.type	_ZNK8NArchive4NZip14CExtraSubBlock15ExtractNtfsTimeEiR9_FILETIME,@function
_ZNK8NArchive4NZip14CExtraSubBlock15ExtractNtfsTimeEiR9_FILETIME: # @_ZNK8NArchive4NZip14CExtraSubBlock15ExtractNtfsTimeEiR9_FILETIME
	.cfi_startproc
# BB#0:
	movq	$0, (%rdx)
	movzwl	(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	$10, %ecx
	jne	.LBB2_10
# BB#1:
	movl	16(%rdi), %ecx
	cmpl	$32, %ecx
	jb	.LBB2_10
# BB#2:
	addl	$-4, %ecx
	cmpl	$5, %ecx
	jb	.LBB2_3
# BB#4:                                 # %.lr.ph.preheader
	movq	24(%rdi), %rax
	addq	$4, %rax
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rax), %r9d
	addl	$-4, %ecx
	cmpl	%ecx, %r9d
	cmoval	%ecx, %r9d
	movzwl	(%rax), %r8d
	leaq	4(%rax), %rax
	cmpl	$1, %r8d
	jne	.LBB2_7
# BB#6:                                 # %.lr.ph
                                        #   in Loop: Header=BB2_5 Depth=1
	cmpl	$23, %r9d
	ja	.LBB2_9
.LBB2_7:                                # %.thread
                                        #   in Loop: Header=BB2_5 Depth=1
	movl	%r9d, %edi
	addq	%rdi, %rax
	subl	%r9d, %ecx
	cmpl	$4, %ecx
	ja	.LBB2_5
# BB#8:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB2_3:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB2_9:
	movslq	%esi, %rcx
	movl	(%rax,%rcx,8), %esi
	movl	%esi, (%rdx)
	movl	4(%rax,%rcx,8), %eax
	movl	%eax, 4(%rdx)
	movb	$1, %al
.LBB2_10:                               # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end2:
	.size	_ZNK8NArchive4NZip14CExtraSubBlock15ExtractNtfsTimeEiR9_FILETIME, .Lfunc_end2-_ZNK8NArchive4NZip14CExtraSubBlock15ExtractNtfsTimeEiR9_FILETIME
	.cfi_endproc

	.globl	_ZNK8NArchive4NZip14CExtraSubBlock15ExtractUnixTimeEiRj
	.p2align	4, 0x90
	.type	_ZNK8NArchive4NZip14CExtraSubBlock15ExtractUnixTimeEiRj,@function
_ZNK8NArchive4NZip14CExtraSubBlock15ExtractUnixTimeEiRj: # @_ZNK8NArchive4NZip14CExtraSubBlock15ExtractUnixTimeEiRj
	.cfi_startproc
# BB#0:
	movl	$0, (%rdx)
	movzwl	(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	$21589, %ecx            # imm = 0x5455
	jne	.LBB3_16
# BB#1:
	movl	16(%rdi), %ecx
	cmpl	$5, %ecx
	jb	.LBB3_16
# BB#2:
	movq	24(%rdi), %rax
	leaq	1(%rax), %r8
	leal	-1(%rcx), %r9d
	movzbl	(%rax), %edi
	testb	$1, %dil
	je	.LBB3_8
# BB#3:
	cmpl	$4, %r9d
	jae	.LBB3_5
# BB#4:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB3_5:
	testl	%esi, %esi
	je	.LBB3_6
# BB#7:
	addq	$5, %rax
	addl	$-5, %ecx
	movl	%ecx, %r9d
	movq	%rax, %r8
.LBB3_8:
	testb	$2, %dil
	je	.LBB3_13
# BB#9:
	cmpl	$4, %r9d
	jae	.LBB3_11
# BB#10:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB3_11:
	cmpl	$1, %esi
	je	.LBB3_6
# BB#12:
	addq	$4, %r8
	addl	$-4, %r9d
.LBB3_13:
	xorl	%eax, %eax
	cmpl	$4, %r9d
	jb	.LBB3_16
# BB#14:
	andl	$4, %edi
	je	.LBB3_16
# BB#15:
	cmpl	$2, %esi
	jne	.LBB3_16
.LBB3_6:
	movl	(%r8), %eax
	movl	%eax, (%rdx)
	movb	$1, %al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB3_16:                               # %.thread
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end3:
	.size	_ZNK8NArchive4NZip14CExtraSubBlock15ExtractUnixTimeEiRj, .Lfunc_end3-_ZNK8NArchive4NZip14CExtraSubBlock15ExtractUnixTimeEiRj
	.cfi_endproc

	.globl	_ZNK8NArchive4NZip10CLocalItem5IsDirEv
	.p2align	4, 0x90
	.type	_ZNK8NArchive4NZip10CLocalItem5IsDirEv,@function
_ZNK8NArchive4NZip10CLocalItem5IsDirEv: # @_ZNK8NArchive4NZip10CLocalItem5IsDirEv
	.cfi_startproc
# BB#0:
	addq	$32, %rdi
	movl	$1, %esi
	jmp	_ZN8NArchive9NItemName12HasTailSlashERK11CStringBaseIcEj # TAILCALL
.Lfunc_end4:
	.size	_ZNK8NArchive4NZip10CLocalItem5IsDirEv, .Lfunc_end4-_ZNK8NArchive4NZip10CLocalItem5IsDirEv
	.cfi_endproc

	.globl	_ZNK8NArchive4NZip5CItem5IsDirEv
	.p2align	4, 0x90
	.type	_ZNK8NArchive4NZip5CItem5IsDirEv,@function
_ZNK8NArchive4NZip5CItem5IsDirEv:       # @_ZNK8NArchive4NZip5CItem5IsDirEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	32(%rbx), %rdi
	movb	81(%rbx), %al
	testb	%al, %al
	sete	%cl
	cmpb	$11, %al
	sete	%al
	orb	%cl, %al
	movzbl	%al, %esi
	callq	_ZN8NArchive9NItemName12HasTailSlashERK11CStringBaseIcEj
	movl	%eax, %ecx
	movb	$1, %al
	testb	%cl, %cl
	jne	.LBB5_8
# BB#1:
	cmpb	$0, 177(%rbx)
	je	.LBB5_2
# BB#3:
	xorl	%eax, %eax
	movzbl	81(%rbx), %ecx
	cmpq	$14, %rcx
	ja	.LBB5_8
# BB#4:
	movl	$18497, %edx            # imm = 0x4841
	btq	%rcx, %rdx
	jae	.LBB5_5
# BB#7:
	movb	84(%rbx), %al
	andb	$16, %al
	shrb	$4, %al
	jmp	.LBB5_8
.LBB5_2:
	xorl	%eax, %eax
	jmp	.LBB5_8
.LBB5_5:
	cmpq	$1, %rcx
	jne	.LBB5_8
# BB#6:
	movl	$201326592, %eax        # imm = 0xC000000
	andl	84(%rbx), %eax
	cmpl	$134217728, %eax        # imm = 0x8000000
	sete	%al
.LBB5_8:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_ZNK8NArchive4NZip5CItem5IsDirEv, .Lfunc_end5-_ZNK8NArchive4NZip5CItem5IsDirEv
	.cfi_endproc

	.globl	_ZNK8NArchive4NZip5CItem16GetWinAttributesEv
	.p2align	4, 0x90
	.type	_ZNK8NArchive4NZip5CItem16GetWinAttributesEv,@function
_ZNK8NArchive4NZip5CItem16GetWinAttributesEv: # @_ZNK8NArchive4NZip5CItem16GetWinAttributesEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movb	81(%rbx), %al
	xorl	%ebp, %ebp
	cmpb	$11, %al
	je	.LBB6_3
# BB#1:
	cmpb	$3, %al
	je	.LBB6_14
# BB#2:
	testb	%al, %al
	jne	.LBB6_5
.LBB6_3:
	cmpb	$0, 177(%rbx)
	je	.LBB6_5
# BB#4:
	movl	84(%rbx), %ebp
.LBB6_5:
	leaq	32(%rbx), %rdi
	testb	%al, %al
	sete	%cl
	cmpb	$11, %al
	sete	%al
	orb	%cl, %al
	movzbl	%al, %esi
	callq	_ZN8NArchive9NItemName12HasTailSlashERK11CStringBaseIcEj
	testb	%al, %al
	je	.LBB6_6
.LBB6_12:                               # %_ZNK8NArchive4NZip5CItem5IsDirEv.exit.thread8
	orl	$16, %ebp
	jmp	.LBB6_13
.LBB6_6:
	cmpb	$0, 177(%rbx)
	je	.LBB6_13
# BB#7:
	movzbl	81(%rbx), %eax
	cmpq	$14, %rax
	ja	.LBB6_13
# BB#8:
	movl	$18497, %ecx            # imm = 0x4841
	btq	%rax, %rcx
	jae	.LBB6_9
# BB#11:
	testb	$16, 84(%rbx)
	jne	.LBB6_12
	jmp	.LBB6_13
.LBB6_14:
	movl	84(%rbx), %eax
	movl	%eax, %ecx
	andl	$-65536, %ecx           # imm = 0xFFFF0000
	shrl	$26, %eax
	andl	$16, %eax
	leal	32768(%rcx,%rax), %ebp
.LBB6_13:                               # %_ZNK8NArchive4NZip5CItem5IsDirEv.exit.thread
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB6_9:
	cmpq	$1, %rax
	jne	.LBB6_13
# BB#10:                                # %_ZNK8NArchive4NZip5CItem5IsDirEv.exit
	movl	$201326592, %eax        # imm = 0xC000000
	andl	84(%rbx), %eax
	cmpl	$134217728, %eax        # imm = 0x8000000
	je	.LBB6_12
	jmp	.LBB6_13
.Lfunc_end6:
	.size	_ZNK8NArchive4NZip5CItem16GetWinAttributesEv, .Lfunc_end6-_ZNK8NArchive4NZip5CItem16GetWinAttributesEv
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CLocalItem11SetFlagBitsEiii
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CLocalItem11SetFlagBitsEiii,@function
_ZN8NArchive4NZip10CLocalItem11SetFlagBitsEiii: # @_ZN8NArchive4NZip10CLocalItem11SetFlagBitsEiii
	.cfi_startproc
# BB#0:
	movl	%ecx, %eax
	movl	%esi, %r8d
	movl	$1, %esi
	movl	%edx, %ecx
	shll	%cl, %esi
	decl	%esi
	movl	%r8d, %ecx
	shll	%cl, %esi
	notl	%esi
	movzwl	2(%rdi), %edx
	andl	%esi, %edx
	movl	%r8d, %ecx
	shll	%cl, %eax
	orl	%edx, %eax
	movw	%ax, 2(%rdi)
	retq
.Lfunc_end7:
	.size	_ZN8NArchive4NZip10CLocalItem11SetFlagBitsEiii, .Lfunc_end7-_ZN8NArchive4NZip10CLocalItem11SetFlagBitsEiii
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CLocalItem10SetBitMaskEib
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CLocalItem10SetBitMaskEib,@function
_ZN8NArchive4NZip10CLocalItem10SetBitMaskEib: # @_ZN8NArchive4NZip10CLocalItem10SetBitMaskEib
	.cfi_startproc
# BB#0:
	testb	%dl, %dl
	je	.LBB8_2
# BB#1:
	movzwl	2(%rdi), %eax
	addq	$2, %rdi
	orl	%esi, %eax
	movw	%ax, (%rdi)
	retq
.LBB8_2:
	notl	%esi
	movzwl	2(%rdi), %eax
	addq	$2, %rdi
	andl	%esi, %eax
	movw	%ax, (%rdi)
	retq
.Lfunc_end8:
	.size	_ZN8NArchive4NZip10CLocalItem10SetBitMaskEib, .Lfunc_end8-_ZN8NArchive4NZip10CLocalItem10SetBitMaskEib
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CLocalItem12SetEncryptedEb
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CLocalItem12SetEncryptedEb,@function
_ZN8NArchive4NZip10CLocalItem12SetEncryptedEb: # @_ZN8NArchive4NZip10CLocalItem12SetEncryptedEb
	.cfi_startproc
# BB#0:                                 # %_ZN8NArchive4NZip10CLocalItem10SetBitMaskEib.exit
	movzwl	2(%rdi), %eax
	movl	%eax, %ecx
	andl	$65534, %ecx            # imm = 0xFFFE
	orl	$1, %eax
	testb	%sil, %sil
	cmovew	%cx, %ax
	movw	%ax, 2(%rdi)
	retq
.Lfunc_end9:
	.size	_ZN8NArchive4NZip10CLocalItem12SetEncryptedEb, .Lfunc_end9-_ZN8NArchive4NZip10CLocalItem12SetEncryptedEb
	.cfi_endproc

	.globl	_ZN8NArchive4NZip10CLocalItem7SetUtf8Eb
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip10CLocalItem7SetUtf8Eb,@function
_ZN8NArchive4NZip10CLocalItem7SetUtf8Eb: # @_ZN8NArchive4NZip10CLocalItem7SetUtf8Eb
	.cfi_startproc
# BB#0:                                 # %_ZN8NArchive4NZip10CLocalItem10SetBitMaskEib.exit
	movzwl	2(%rdi), %eax
	movl	%eax, %ecx
	andl	$63487, %ecx            # imm = 0xF7FF
	orl	$2048, %eax             # imm = 0x800
	testb	%sil, %sil
	cmovew	%cx, %ax
	movw	%ax, 2(%rdi)
	retq
.Lfunc_end10:
	.size	_ZN8NArchive4NZip10CLocalItem7SetUtf8Eb, .Lfunc_end10-_ZN8NArchive4NZip10CLocalItem7SetUtf8Eb
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
