	.text
	.file	"libclamav_nsis_LZMADecode.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	lzmaInit
	.p2align	4, 0x90
	.type	lzmaInit,@function
lzmaInit:                               # @lzmaInit
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 128(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	$0, 144(%rdi)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1,1,1,1]
	movups	%xmm0, 116(%rdi)
	movl	$-1, 144(%rdi)
	retq
.Lfunc_end0:
	.size	lzmaInit, .Lfunc_end0-lzmaInit
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.short	1024                    # 0x400
	.text
	.globl	lzmaDecode
	.p2align	4, 0x90
	.type	lzmaDecode,@function
lzmaDecode:                             # @lzmaDecode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi6:
	.cfi_def_cfa_offset 336
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	(%rdi), %eax
	movl	4(%rdi), %r11d
	movl	8(%rdi), %r8d
	movl	12(%rdi), %ecx
	movl	%ecx, 172(%rsp)         # 4-byte Spill
	movl	16(%rdi), %r12d
	movl	20(%rdi), %r10d
	movq	24(%rdi), %rbp
	movl	32(%rdi), %edx
	movl	36(%rdi), %esi
	movq	40(%rdi), %r13
	movl	48(%rdi), %r14d
	movl	52(%rdi), %r9d
	movb	56(%rdi), %r15b
	movb	57(%rdi), %bl
	movzwl	62(%rdi), %ecx
	movw	%cx, 212(%rsp)
	movl	58(%rdi), %ecx
	movl	%ecx, 208(%rsp)
	movl	112(%rdi), %ecx
	cmpl	$-1, %ecx
	je	.LBB1_2
# BB#1:                                 # %.outer.outer.preheader
	movl	%r11d, 92(%rsp)         # 4-byte Spill
	movl	%edx, 20(%rsp)          # 4-byte Spill
	movl	%r12d, 168(%rsp)        # 4-byte Spill
	movl	%esi, 220(%rsp)         # 4-byte Spill
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movb	%bl, 47(%rsp)           # 1-byte Spill
	movl	%r8d, 120(%rsp)         # 4-byte Spill
	leaq	58(%rdi), %rcx
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	movq	160(%rdi), %r12
	movq	152(%rdi), %r11
	movl	148(%rdi), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	144(%rdi), %ecx
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	movl	140(%rdi), %ebp
	movl	136(%rdi), %ecx
	movl	%ecx, 188(%rsp)         # 4-byte Spill
	movl	132(%rdi), %ecx
	movl	%ecx, 196(%rsp)         # 4-byte Spill
	movl	128(%rdi), %ecx
	movl	%ecx, 156(%rsp)         # 4-byte Spill
	movl	124(%rdi), %ecx
	movl	%ecx, 124(%rsp)         # 4-byte Spill
	movl	120(%rdi), %ecx
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movl	116(%rdi), %ecx
	movl	%ecx, 68(%rsp)          # 4-byte Spill
	movl	108(%rdi), %ecx
	movl	%ecx, 192(%rsp)         # 4-byte Spill
	movl	104(%rdi), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [1024,1024,1024,1024,1024,1024,1024,1024]
	movl	100(%rdi), %edx
	movl	96(%rdi), %ecx
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	movl	92(%rdi), %ecx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movl	88(%rdi), %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	84(%rdi), %ecx
	movl	80(%rdi), %ebx
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movq	64(%rdi), %rsi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movq	72(%rdi), %rsi
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	jmp	.LBB1_4
.LBB1_2:
	movl	$1, %r12d
	jmp	.LBB1_218
.LBB1_3:                                #   in Loop: Header=BB1_4 Depth=1
	shll	$8, 24(%rsp)            # 4-byte Folded Spill
	movl	12(%rsp), %esi          # 4-byte Reload
	shll	$8, %esi
	decl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	96(%rsp), %rbx          # 8-byte Reload
	movzbl	(%rbx), %eax
	incq	%rbx
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	orl	%eax, %esi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	92(%rsp), %eax          # 4-byte Reload
	.p2align	4, 0x90
.LBB1_4:                                # %.outer.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_6 Depth 2
                                        #       Child Loop BB1_32 Depth 3
                                        #         Child Loop BB1_44 Depth 4
                                        #         Child Loop BB1_56 Depth 4
                                        #         Child Loop BB1_88 Depth 4
                                        #     Child Loop BB1_170 Depth 2
                                        #     Child Loop BB1_178 Depth 2
                                        #     Child Loop BB1_184 Depth 2
	leaq	3692(%r12), %rsi
	movq	%rsi, 232(%rsp)         # 8-byte Spill
	movl	$8, %esi
	subl	%edx, %esi
	movl	%esi, 216(%rsp)         # 4-byte Spill
	leaq	864(%r12), %rsi
	movq	%rsi, 256(%rsp)         # 8-byte Spill
	movq	%r12, %rbx
	movq	%rcx, %r12
	leal	2(,%r12,8), %ecx
	movslq	%ecx, %rcx
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	movq	%r12, 176(%rsp)         # 8-byte Spill
	leal	130(,%r12,8), %ecx
	movq	%rbx, %r12
	movslq	%ecx, %rcx
	movq	%rcx, 248(%rsp)         # 8-byte Spill
	leaq	1376(%r12), %rcx
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	leaq	1604(%r12), %rcx
	movq	%rcx, 264(%rsp)         # 8-byte Spill
	movq	136(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	68(%rsp), %r8d          # 4-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	%edx, 16(%rsp)          # 4-byte Spill
	jmp	.LBB1_6
.LBB1_5:                                #   in Loop: Header=BB1_6 Depth=2
	movl	$2, %eax
	.p2align	4, 0x90
.LBB1_6:                                # %.outer
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_32 Depth 3
                                        #         Child Loop BB1_44 Depth 4
                                        #         Child Loop BB1_56 Depth 4
                                        #         Child Loop BB1_88 Depth 4
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movl	%r8d, 68(%rsp)          # 4-byte Spill
	jmp	.LBB1_32
.LBB1_7:                                # %.us-lcssa1702.us
                                        #   in Loop: Header=BB1_32 Depth=3
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_188
# BB#8:                                 #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, 8(%rsp)           # 4-byte Spill
	jmp	.LBB1_14
.LBB1_9:                                # %.us-lcssa1703.us.loopexit6041
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	48(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB1_16
.LBB1_10:                               # %.us-lcssa1700.us.loopexit6033
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, 8(%rsp)           # 4-byte Spill
	jmp	.LBB1_18
.LBB1_11:                               # %.sink.split28.loopexit6042
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	$1, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	48(%rsp), %eax          # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jmp	.LBB1_19
.LBB1_12:                               # %.loopexit571.loopexit6043
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	16(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_21
.LBB1_13:                               # %.us-lcssa1702.us.thread.loopexit
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB1_14:                               # %.us-lcssa1702.us.thread
                                        #   in Loop: Header=BB1_32 Depth=3
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	240(%rsp), %rcx         # 8-byte Reload
	leaq	(%rax,%rcx,2), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	$20, 172(%rsp)          # 4-byte Folded Spill
	movl	$1, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	$3, 48(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	$3, %eax
	jmp	.LBB1_19
.LBB1_15:                               # %.us-lcssa1703.us.loopexit
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, 8(%rsp)           # 4-byte Spill
	xorl	%ecx, %ecx
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB1_16:                               # %.us-lcssa1703.us
                                        #   in Loop: Header=BB1_32 Depth=3
	xorl	%eax, %eax
	testl	%ecx, %ecx
	movl	$258, %ecx              # imm = 0x102
	cmoveq	248(%rsp), %rcx         # 8-byte Folded Reload
	setne	%al
	leal	8(,%rax,8), %ebx
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	leal	3(%rax,%rax,4), %esi
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,2), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	$20, 172(%rsp)          # 4-byte Folded Spill
	movl	$1, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	%esi, 48(%rsp)          # 4-byte Spill
	movl	%esi, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	68(%rsp), %r8d          # 4-byte Reload
	jmp	.LBB1_23
.LBB1_17:                               # %.us-lcssa1700.us.loopexit
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB1_18:                               # %.us-lcssa1700.us
                                        #   in Loop: Header=BB1_32 Depth=3
	movq	136(%rsp), %rcx         # 8-byte Reload
	cmpl	$4, %ecx
	movl	$3, %eax
	cmovll	%ecx, %eax
	shll	$6, %eax
	cltq
	movq	256(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,2), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	$25, 172(%rsp)          # 4-byte Folded Spill
	movl	$1, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	$6, 48(%rsp)            # 4-byte Folded Spill
	movl	$6, %eax
.LBB1_19:                               # %.sink.split28
                                        #   in Loop: Header=BB1_32 Depth=3
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jmp	.LBB1_23
.LBB1_22:                               # %.sink.split28.loopexit
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	$1, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_23
.LBB1_20:                               # %.loopexit571.loopexit
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB1_21:                               # %.loopexit571
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	24(%rsp), %esi          # 4-byte Reload
	jmp	.LBB1_28
	.p2align	4, 0x90
.LBB1_23:                               # %.sink.split28
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	12(%rsp), %esi          # 4-byte Reload
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_31
# BB#24:                                #   in Loop: Header=BB1_32 Depth=3
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	128(%rsp), %rbx         # 8-byte Reload
	movslq	%ebx, %rcx
	movq	104(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,2), %r9
	movl	24(%rsp), %r12d         # 4-byte Reload
	movl	%r12d, %eax
	shrl	$11, %eax
	movzwl	(%rdx,%rcx,2), %ecx
	imull	%ecx, %eax
	movl	%esi, %edx
	subl	%eax, %edx
	movq	%r9, 160(%rsp)          # 8-byte Spill
	jae	.LBB1_26
# BB#25:                                #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	$2048, %edx             # imm = 0x800
	subl	%ecx, %edx
	shrl	$5, %edx
	addl	%ecx, %edx
	movw	%dx, (%r9)
	addl	%ebx, %ebx
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	jmp	.LBB1_27
.LBB1_26:                               #   in Loop: Header=BB1_32 Depth=3
	subl	%eax, %r12d
	movl	%ecx, %eax
	shrl	$5, %eax
	subl	%eax, %ecx
	movw	%cx, (%r9)
	leal	1(%rbx,%rbx), %ebx
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movl	%r12d, %eax
	movl	%edx, 12(%rsp)          # 4-byte Spill
.LBB1_27:                               #   in Loop: Header=BB1_32 Depth=3
	cmpl	$16777216, %eax         # imm = 0x1000000
	movl	%eax, %esi
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	68(%rsp), %r8d          # 4-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	jae	.LBB1_30
.LBB1_28:                               # %.loopexit571
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	20(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	%esi, %ebx
	movl	12(%rsp), %esi          # 4-byte Reload
	je	.LBB1_220
# BB#29:                                #   in Loop: Header=BB1_32 Depth=3
	shll	$8, %ebx
	shll	$8, %esi
	decl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movzbl	(%rcx), %eax
	incq	%rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	orl	%eax, %esi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	%ebx, %eax
.LBB1_30:                               #   in Loop: Header=BB1_32 Depth=3
	movq	56(%rsp), %rcx          # 8-byte Reload
	decl	%ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	%eax, 24(%rsp)          # 4-byte Spill
	jmp	.LBB1_23
.LBB1_31:                               #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	$1, %eax
	movl	48(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movq	128(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	subl	%eax, %ecx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movl	172(%rsp), %eax         # 4-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	.p2align	4, 0x90
.LBB1_32:                               # %.outer535.outer
                                        #   Parent Loop BB1_4 Depth=1
                                        #     Parent Loop BB1_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_44 Depth 4
                                        #         Child Loop BB1_56 Depth 4
                                        #         Child Loop BB1_88 Depth 4
	movq	176(%rsp), %rcx         # 8-byte Reload
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_56
	jmp	.LBB1_42
.LBB1_72:                               #   in Loop: Header=BB1_32 Depth=3
	movslq	%eax, %rdx
	movq	104(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rdx,2), %r9
	movl	24(%rsp), %ecx          # 4-byte Reload
	movq	%rax, %rsi
	movl	%ecx, %eax
	shrl	$11, %eax
	movzwl	(%rbx,%rdx,2), %edx
	imull	%edx, %eax
	movl	12(%rsp), %ebx          # 4-byte Reload
	subl	%eax, %ebx
	jae	.LBB1_74
# BB#73:                                #   in Loop: Header=BB1_32 Depth=3
	movl	$2048, %ebx             # imm = 0x800
	subl	%edx, %ebx
	shrl	$5, %ebx
	addl	%edx, %ebx
	movq	%r9, 160(%rsp)          # 8-byte Spill
	movw	%bx, (%r9)
	addl	%esi, %esi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	jmp	.LBB1_75
.LBB1_74:                               #   in Loop: Header=BB1_32 Depth=3
	subl	%eax, %ecx
	movl	%edx, %eax
	shrl	$5, %eax
	subl	%eax, %edx
	movq	%r9, 160(%rsp)          # 8-byte Spill
	movw	%dx, (%r9)
	leal	1(%rsi,%rsi), %esi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movl	%ecx, %eax
	movl	%ebx, 12(%rsp)          # 4-byte Spill
.LBB1_75:                               #   in Loop: Header=BB1_32 Depth=3
	cmpl	$16777216, %eax         # imm = 0x1000000
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movq	%r11, %r9
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	68(%rsp), %r8d          # 4-byte Reload
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	144(%rsp), %r11         # 8-byte Reload
	movl	8(%rsp), %ebx           # 4-byte Reload
	jae	.LBB1_78
.LBB1_76:                               # %.loopexit609
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	20(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	je	.LBB1_229
# BB#77:                                #   in Loop: Header=BB1_32 Depth=3
	shll	$8, 24(%rsp)            # 4-byte Folded Spill
	movl	12(%rsp), %edx          # 4-byte Reload
	shll	$8, %edx
	decl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	96(%rsp), %rcx          # 8-byte Reload
	movzbl	(%rcx), %eax
	incq	%rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	orl	%eax, %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
.LBB1_78:                               #   in Loop: Header=BB1_32 Depth=3
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpl	$256, %eax              # imm = 0x100
	jge	.LBB1_38
# BB#79:                                #   in Loop: Header=BB1_32 Depth=3
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movq	%r11, 144(%rsp)         # 8-byte Spill
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	%r9, %r11
	jmp	.LBB1_72
	.p2align	4, 0x90
.LBB1_55:                               #   in Loop: Header=BB1_56 Depth=4
	movl	%ebp, %eax
	subl	%r8d, %eax
	xorl	%ecx, %ecx
	cmpl	%r10d, %eax
	cmovael	%r10d, %ecx
	addl	%eax, %ecx
	movzbl	(%r11,%rcx), %r15d
	movl	%ebp, %eax
	movb	%r15b, (%r11,%rax)
	incl	%ebp
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	%r10d
	incl	%r9d
	movb	%r15b, (%r13)
	incq	%r13
	decl	%r14d
	movl	$2, %eax
	movl	%edx, %ebp
	movq	176(%rsp), %rcx         # 8-byte Reload
.LBB1_56:                               # %.outer572.split
                                        #   Parent Loop BB1_4 Depth=1
                                        #     Parent Loop BB1_6 Depth=2
                                        #       Parent Loop BB1_32 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	$28, %eax
	ja	.LBB1_219
# BB#57:                                # %.outer572.split
                                        #   in Loop: Header=BB1_56 Depth=4
	movl	%eax, %eax
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_212:                              #   in Loop: Header=BB1_56 Depth=4
	testl	%r14d, %r14d
	jne	.LBB1_55
	jmp	.LBB1_213
.LBB1_58:                               # %.us-lcssa1694.us.loopexit6025
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	16(%rsp), %edx          # 4-byte Reload
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	je	.LBB1_59
	jmp	.LBB1_187
.LBB1_65:                               # %.loopexit607.loopexit6026
                                        #   in Loop: Header=BB1_32 Depth=3
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movl	%esi, %ebx
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_82
.LBB1_66:                               # %.loopexit522.loopexit6027
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, %ebx
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%r11, 144(%rsp)         # 8-byte Spill
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_34
.LBB1_67:                               # %.loopexit609.loopexit6028
                                        #   in Loop: Header=BB1_32 Depth=3
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movl	%esi, %ebx
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_76
.LBB1_68:                               # %.us-lcssa1704.us.loopexit6044
                                        #   in Loop: Header=BB1_56 Depth=4
	movq	136(%rsp), %rax         # 8-byte Reload
	addl	80(%rsp), %eax          # 4-byte Folded Reload
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	120(%rsp), %eax         # 4-byte Reload
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_56
	jmp	.LBB1_42
.LBB1_69:                               # %.loopexit610.loopexit6029
                                        #   in Loop: Header=BB1_32 Depth=3
	movq	%r12, 32(%rsp)          # 8-byte Spill
	jmp	.LBB1_70
.LBB1_33:                               #   in Loop: Header=BB1_32 Depth=3
	movq	%rax, 80(%rsp)          # 8-byte Spill
.LBB1_34:                               # %.loopexit522
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	20(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	je	.LBB1_224
# BB#35:                                #   in Loop: Header=BB1_32 Depth=3
	shll	$8, 24(%rsp)            # 4-byte Folded Spill
	movl	12(%rsp), %edx          # 4-byte Reload
	shll	$8, %edx
	decl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	96(%rsp), %rcx          # 8-byte Reload
	movzbl	(%rcx), %eax
	incq	%rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	orl	%eax, %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	jmp	.LBB1_87
.LBB1_36:                               #   in Loop: Header=BB1_32 Depth=3
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	144(%rsp), %r11         # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
.LBB1_37:                               # %.loopexit521
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	$0, 192(%rsp)           # 4-byte Folded Spill
.LBB1_38:                               # %.loopexit610
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	%eax, %r15d
.LBB1_39:                               # %.loopexit610
                                        #   in Loop: Header=BB1_32 Depth=3
	movq	%rax, 80(%rsp)          # 8-byte Spill
	testl	%r14d, %r14d
	je	.LBB1_222
# BB#40:                                #   in Loop: Header=BB1_32 Depth=3
	incl	%r9d
	movb	%r15b, (%r13)
	incq	%r13
	decl	%r14d
	movl	%ebp, %eax
	movb	%r15b, (%r11,%rax)
	incl	%ebp
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	%r10d
	movl	$2, %eax
	movl	%ebx, %esi
	movl	%edx, %ebp
.LBB1_41:                               # %.outer572
                                        #   in Loop: Header=BB1_32 Depth=3
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_56
.LBB1_42:                               # %.outer572.split.us.preheader
                                        #   in Loop: Header=BB1_32 Depth=3
	movq	%r12, 32(%rsp)          # 8-byte Spill
	jmp	.LBB1_44
	.p2align	4, 0x90
.LBB1_43:                               #   in Loop: Header=BB1_44 Depth=4
	movl	%ebp, %eax
	subl	%r8d, %eax
	xorl	%ecx, %ecx
	cmpl	%r10d, %eax
	cmovael	%r10d, %ecx
	addl	%eax, %ecx
	movzbl	(%r11,%rcx), %r15d
	movl	%ebp, %eax
	movb	%r15b, (%r11,%rax)
	incl	%ebp
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	%r10d
	incl	%r9d
	movb	%r15b, (%r13)
	incq	%r13
	decl	%r14d
	movl	$2, %eax
	movl	%edx, %ebp
.LBB1_44:                               # %.outer572.split.us
                                        #   Parent Loop BB1_4 Depth=1
                                        #     Parent Loop BB1_6 Depth=2
                                        #       Parent Loop BB1_32 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	$-1, %r12d
	cmpl	$28, %eax
	ja	.LBB1_218
# BB#45:                                # %.outer572.split.us
                                        #   in Loop: Header=BB1_44 Depth=4
	movl	%eax, %eax
	movl	16(%rsp), %edx          # 4-byte Reload
	jmpq	*.LJTI1_1(,%rax,8)
.LBB1_46:                               #   in Loop: Header=BB1_44 Depth=4
	movq	%r9, %rcx
	testl	%r9d, %r9d
	je	.LBB1_218
# BB#47:                                #   in Loop: Header=BB1_44 Depth=4
	xorl	%eax, %eax
	cmpl	$6, %esi
	setg	%al
	leal	9(%rax,%rax), %esi
	movq	%rcx, %r9
	movl	68(%rsp), %r8d          # 4-byte Reload
.LBB1_48:                               #   in Loop: Header=BB1_44 Depth=4
	testl	%r14d, %r14d
	jne	.LBB1_43
	jmp	.LBB1_49
.LBB1_186:                              # %.us-lcssa1694.us.loopexit
                                        #   in Loop: Header=BB1_32 Depth=3
	movq	32(%rsp), %r12          # 8-byte Reload
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_187
.LBB1_59:                               #   in Loop: Header=BB1_32 Depth=3
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movl	188(%rsp), %eax         # 4-byte Reload
	andl	%r9d, %eax
	movl	%edx, %ecx
	shll	%cl, %eax
	movzbl	%r15b, %edx
	movl	216(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	addl	%eax, %edx
	shll	$8, %edx
	leal	(%rdx,%rdx,2), %eax
	xorl	%ebx, %ebx
	cmpl	$4, %esi
	jl	.LBB1_61
# BB#60:                                #   in Loop: Header=BB1_32 Depth=3
	xorl	%ecx, %ecx
	cmpl	$9, %esi
	setg	%cl
	leal	3(%rcx,%rcx,2), %ecx
	subl	%ecx, %esi
	movl	%esi, %ebx
.LBB1_61:                               #   in Loop: Header=BB1_32 Depth=3
	movq	232(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,2), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	cmpl	$0, 192(%rsp)           # 4-byte Folded Reload
	je	.LBB1_71
# BB#62:                                #   in Loop: Header=BB1_32 Depth=3
	movl	%ebp, %eax
	subl	%r8d, %eax
	cmpl	%r10d, %eax
	movl	%r10d, %edx
	movl	$0, %ecx
	cmovbl	%ecx, %edx
	addl	%eax, %edx
	movb	(%r11,%rdx), %al
	movb	%al, 47(%rsp)           # 1-byte Spill
	movl	$1, %eax
	jmp	.LBB1_63
.LBB1_71:                               #   in Loop: Header=BB1_32 Depth=3
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movq	%r11, 144(%rsp)         # 8-byte Spill
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	%r9, %r11
	movl	$1, %eax
	jmp	.LBB1_72
.LBB1_50:                               # %.loopexit607.loopexit
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movl	%esi, %ebx
	movl	28(%rsp), %esi          # 4-byte Reload
	jmp	.LBB1_82
.LBB1_51:                               # %.loopexit522.loopexit6019
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, %ebx
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%r11, 144(%rsp)         # 8-byte Spill
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_34
.LBB1_52:                               # %.loopexit609.loopexit
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, %ebx
	movl	28(%rsp), %esi          # 4-byte Reload
	jmp	.LBB1_76
.LBB1_53:                               # %.us-lcssa1704.us.loopexit
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movq	136(%rsp), %rax         # 8-byte Reload
	addl	80(%rsp), %eax          # 4-byte Folded Reload
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	120(%rsp), %eax         # 4-byte Reload
	jmp	.LBB1_41
.LBB1_54:                               # %.loopexit610.loopexit
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
.LBB1_70:                               # %.loopexit610
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	%esi, %ebx
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	jmp	.LBB1_39
.LBB1_63:                               #   in Loop: Header=BB1_32 Depth=3
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movq	%r11, 144(%rsp)         # 8-byte Spill
	movq	%r9, %r11
	movzbl	47(%rsp), %ecx          # 1-byte Folded Reload
	shrl	$7, %ecx
	movq	%rax, %rsi
	movq	%rcx, %r12
	movl	%ecx, %eax
	shll	$8, %eax
	movq	104(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,2), %rdx
	movslq	%esi, %rbx
	leaq	512(%rdx,%rbx,2), %r9
	movl	24(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	shrl	$11, %eax
	movzwl	512(%rdx,%rbx,2), %edx
	imull	%edx, %eax
	movl	12(%rsp), %ebx          # 4-byte Reload
	subl	%eax, %ebx
	movq	%r9, 160(%rsp)          # 8-byte Spill
	movq	%r12, 56(%rsp)          # 8-byte Spill
	jae	.LBB1_80
# BB#64:                                #   in Loop: Header=BB1_32 Depth=3
	movl	$2048, %ebx             # imm = 0x800
	subl	%edx, %ebx
	shrl	$5, %ebx
	addl	%edx, %ebx
	movw	%bx, (%r9)
	addl	%esi, %esi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	jmp	.LBB1_81
.LBB1_80:                               #   in Loop: Header=BB1_32 Depth=3
	subl	%eax, %ecx
	movl	%edx, %eax
	shrl	$5, %eax
	subl	%eax, %edx
	movw	%dx, (%r9)
	leal	1(%rsi,%rsi), %esi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movl	$1, 48(%rsp)            # 4-byte Folded Spill
	movl	%ecx, %eax
	movl	%ebx, 12(%rsp)          # 4-byte Spill
.LBB1_81:                               #   in Loop: Header=BB1_32 Depth=3
	movb	47(%rsp), %dl           # 1-byte Reload
	addb	%dl, %dl
	movb	%dl, 47(%rsp)           # 1-byte Spill
	cmpl	$16777216, %eax         # imm = 0x1000000
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movq	%r11, %r9
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	68(%rsp), %r8d          # 4-byte Reload
	movq	144(%rsp), %r11         # 8-byte Reload
	movl	8(%rsp), %ebx           # 4-byte Reload
	jae	.LBB1_84
.LBB1_82:                               # %.loopexit607
                                        #   in Loop: Header=BB1_32 Depth=3
	movl	20(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	je	.LBB1_230
# BB#83:                                #   in Loop: Header=BB1_32 Depth=3
	shll	$8, 24(%rsp)            # 4-byte Folded Spill
	movl	12(%rsp), %edx          # 4-byte Reload
	shll	$8, %edx
	decl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	96(%rsp), %rcx          # 8-byte Reload
	movzbl	(%rcx), %eax
	incq	%rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	orl	%eax, %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
.LBB1_84:                               #   in Loop: Header=BB1_32 Depth=3
	movl	48(%rsp), %ecx          # 4-byte Reload
	cmpl	%ecx, 56(%rsp)          # 4-byte Folded Reload
	jne	.LBB1_86
# BB#85:                                #   in Loop: Header=BB1_32 Depth=3
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpl	$256, %eax              # imm = 0x100
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	jge	.LBB1_37
	jmp	.LBB1_63
.LBB1_86:                               #   in Loop: Header=BB1_32 Depth=3
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%r11, 144(%rsp)         # 8-byte Spill
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB1_87:                               # %.preheader520
                                        #   in Loop: Header=BB1_32 Depth=3
	movq	80(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_88:                               #   Parent Loop BB1_4 Depth=1
                                        #     Parent Loop BB1_6 Depth=2
                                        #       Parent Loop BB1_32 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	$255, %eax
	jg	.LBB1_36
# BB#89:                                #   in Loop: Header=BB1_88 Depth=4
	movq	%r12, %r9
	movslq	%eax, %rcx
	movq	104(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,2), %rsi
	movq	%rax, %r12
	movl	24(%rsp), %r11d         # 4-byte Reload
	movl	%r11d, %eax
	shrl	$11, %eax
	movzwl	(%rdx,%rcx,2), %ecx
	imull	%ecx, %eax
	movl	12(%rsp), %edx          # 4-byte Reload
	subl	%eax, %edx
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	jae	.LBB1_91
# BB#90:                                #   in Loop: Header=BB1_88 Depth=4
	movl	$2048, %edx             # imm = 0x800
	subl	%ecx, %edx
	shrl	$5, %edx
	addl	%ecx, %edx
	movw	%dx, (%rsi)
	addl	%r12d, %r12d
	movl	%eax, %r11d
	jmp	.LBB1_92
	.p2align	4, 0x90
.LBB1_91:                               #   in Loop: Header=BB1_88 Depth=4
	subl	%eax, %r11d
	movl	%ecx, %eax
	shrl	$5, %eax
	subl	%eax, %ecx
	movw	%cx, (%rsi)
	leal	1(%r12,%r12), %r12d
	movl	%edx, 12(%rsp)          # 4-byte Spill
.LBB1_92:                               #   in Loop: Header=BB1_88 Depth=4
	movq	%r12, %rax
	movl	%r11d, 24(%rsp)         # 4-byte Spill
	cmpl	$16777216, %r11d        # imm = 0x1000000
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	movq	%r9, %r12
	jae	.LBB1_88
	jmp	.LBB1_33
.LBB1_93:                               # %.loopexit529.loopexit6035
                                        #   in Loop: Header=BB1_6 Depth=2
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	$12, 116(%rsp)          # 4-byte Folded Spill
	movl	16(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_98
.LBB1_94:                               # %.loopexit530.loopexit6036
                                        #   in Loop: Header=BB1_6 Depth=2
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movl	$16, 116(%rsp)          # 4-byte Folded Spill
	movl	48(%rsp), %r8d          # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_100
.LBB1_95:                               # %.us-lcssa1699.us.loopexit6032
                                        #   in Loop: Header=BB1_6 Depth=2
	movl	16(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_102
.LBB1_96:                               # %.us-lcssa1701.us.loopexit6034
                                        #   in Loop: Header=BB1_6 Depth=2
	movl	16(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_104
.LBB1_214:                              # %.loopexit531.loopexit6037
                                        #   in Loop: Header=BB1_6 Depth=2
	movl	16(%rsp), %edx          # 4-byte Reload
	testl	%r14d, %r14d
	jne	.LBB1_128
	jmp	.LBB1_215
.LBB1_97:                               # %.loopexit529.loopexit
                                        #   in Loop: Header=BB1_6 Depth=2
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	$12, 116(%rsp)          # 4-byte Folded Spill
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB1_98:                               # %.loopexit529
                                        #   in Loop: Header=BB1_6 Depth=2
	movl	24(%rsp), %esi          # 4-byte Reload
	jmp	.LBB1_111
.LBB1_99:                               # %.loopexit530.loopexit
                                        #   in Loop: Header=BB1_6 Depth=2
	movq	%r9, 72(%rsp)           # 8-byte Spill
	xorl	%r8d, %r8d
	movl	$16, 116(%rsp)          # 4-byte Folded Spill
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB1_100:                              # %.loopexit530
                                        #   in Loop: Header=BB1_6 Depth=2
	movl	24(%rsp), %eax          # 4-byte Reload
	jmp	.LBB1_119
.LBB1_101:                              # %.us-lcssa1699.us.loopexit
                                        #   in Loop: Header=BB1_6 Depth=2
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB1_102:                              # %.us-lcssa1699.us
                                        #   in Loop: Header=BB1_6 Depth=2
	xorl	%eax, %eax
	cmpl	$6, %esi
	setg	%al
	leal	8(%rax,%rax,2), %esi
	movl	$21, 116(%rsp)          # 4-byte Folded Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	jmp	.LBB1_124
.LBB1_103:                              # %.us-lcssa1701.us.loopexit
                                        #   in Loop: Header=BB1_6 Depth=2
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB1_104:                              # %.us-lcssa1701.us
                                        #   in Loop: Header=BB1_6 Depth=2
	movl	$25, 116(%rsp)          # 4-byte Folded Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpl	$4, %eax
	jl	.LBB1_107
# BB#105:                               #   in Loop: Header=BB1_6 Depth=2
	movl	%eax, %ecx
	shrl	%ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leal	-1(%rcx), %ecx
	movl	%eax, %ebx
	andl	$1, %ebx
	orl	$2, %ebx
	shll	%cl, %ebx
	cmpl	$13, %eax
	jg	.LBB1_109
# BB#106:                               #   in Loop: Header=BB1_6 Depth=2
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%r11, %r9
	movl	%ecx, %r11d
	movq	%rax, %r8
	movl	%ebx, %eax
	movq	224(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,2), %rcx
	movslq	%r8d, %rax
	movl	%r11d, %r8d
	movq	%r9, %r11
	movq	72(%rsp), %r9           # 8-byte Reload
	addq	%rax, %rax
	subq	%rax, %rcx
	addq	$-2, %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movl	$1, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movl	$25, 116(%rsp)          # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	cmpl	%r8d, 56(%rsp)          # 4-byte Folded Reload
	jl	.LBB1_115
	jmp	.LBB1_122
.LBB1_107:                              #   in Loop: Header=BB1_6 Depth=2
	movl	%eax, %ecx
	jmp	.LBB1_123
.LBB1_108:                              # %.loopexit531.loopexit
                                        #   in Loop: Header=BB1_6 Depth=2
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movq	32(%rsp), %r12          # 8-byte Reload
	testl	%r14d, %r14d
	jne	.LBB1_128
	jmp	.LBB1_215
.LBB1_109:                              #   in Loop: Header=BB1_6 Depth=2
	movl	%ebx, 68(%rsp)          # 4-byte Spill
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	addl	$-5, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	$25, 116(%rsp)          # 4-byte Folded Spill
	movl	24(%rsp), %esi          # 4-byte Reload
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_114
.LBB1_110:                              #   in Loop: Header=BB1_6 Depth=2
	movl	%esi, %eax
	shrl	%eax
	xorl	%ecx, %ecx
	movl	%esi, 24(%rsp)          # 4-byte Spill
	movl	12(%rsp), %esi          # 4-byte Reload
	cmpl	%eax, %esi
	setae	%cl
	movq	%r9, %r8
	movq	80(%rsp), %rbx          # 8-byte Reload
	leal	(%rcx,%rbx,2), %ebx
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movl	%eax, %ecx
	movl	$0, %ebx
	cmovbl	%ebx, %ecx
	subl	%ecx, %esi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	cmpl	$33554432, 24(%rsp)     # 4-byte Folded Reload
                                        # imm = 0x2000000
	movl	%eax, %esi
	jae	.LBB1_113
.LBB1_111:                              # %.loopexit529
                                        #   in Loop: Header=BB1_6 Depth=2
	movl	20(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	%esi, %ebx
	movl	12(%rsp), %esi          # 4-byte Reload
	je	.LBB1_228
# BB#112:                               #   in Loop: Header=BB1_6 Depth=2
	shll	$8, %ebx
	shll	$8, %esi
	decl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movzbl	(%rcx), %eax
	incq	%rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	orl	%eax, %esi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	%ebx, %eax
.LBB1_113:                              #   in Loop: Header=BB1_6 Depth=2
	movq	56(%rsp), %rcx          # 8-byte Reload
	decl	%ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	%eax, %esi
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	jg	.LBB1_110
.LBB1_114:                              #   in Loop: Header=BB1_6 Depth=2
	movl	%esi, 24(%rsp)          # 4-byte Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	shll	$4, %ecx
	addl	68(%rsp), %ecx          # 4-byte Folded Reload
	movl	$1, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	$4, %r8d
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	264(%rsp), %rax         # 8-byte Reload
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	8(%rsp), %esi           # 4-byte Reload
	cmpl	%r8d, 56(%rsp)          # 4-byte Folded Reload
	jge	.LBB1_122
.LBB1_115:                              #   in Loop: Header=BB1_6 Depth=2
	movl	%ecx, 80(%rsp)          # 4-byte Spill
	movl	%r8d, 48(%rsp)          # 4-byte Spill
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	%ebx, 68(%rsp)          # 4-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	%r10, 200(%rsp)         # 8-byte Spill
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%r11, 144(%rsp)         # 8-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	128(%rsp), %rsi         # 8-byte Reload
	movslq	%esi, %rcx
	leaq	(%rax,%rcx,2), %r12
	movl	24(%rsp), %r8d          # 4-byte Reload
	movl	%r8d, %ebx
	shrl	$11, %ebx
	movzwl	(%rax,%rcx,2), %r9d
	imull	%r9d, %ebx
	movl	12(%rsp), %r10d         # 4-byte Reload
	subl	%ebx, %r10d
	movq	%r12, 160(%rsp)         # 8-byte Spill
	jae	.LBB1_117
# BB#116:                               #   in Loop: Header=BB1_6 Depth=2
	movl	$2048, %ecx             # imm = 0x800
	subl	%r9d, %ecx
	shrl	$5, %ecx
	addl	%r9d, %ecx
	movw	%cx, (%r12)
	addl	%esi, %esi
	movq	%rsi, 128(%rsp)         # 8-byte Spill
	movq	144(%rsp), %r11         # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	movl	80(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB1_118
.LBB1_117:                              #   in Loop: Header=BB1_6 Depth=2
	movl	$1, %r11d
	movq	56(%rsp), %rcx          # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %r11d
	movl	80(%rsp), %eax          # 4-byte Reload
	orl	%r11d, %eax
	movq	32(%rsp), %r11          # 8-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	subl	%ebx, %r8d
	movl	%r9d, %ecx
	shrl	$5, %ecx
	subl	%ecx, %r9d
	movl	%eax, %ecx
	movw	%r9w, (%r12)
	movq	%r11, %r12
	leal	1(%rsi,%rsi), %esi
	movq	%rsi, 128(%rsp)         # 8-byte Spill
	movl	%r8d, %ebx
	movl	%r10d, 12(%rsp)         # 4-byte Spill
	movq	144(%rsp), %r11         # 8-byte Reload
.LBB1_118:                              #   in Loop: Header=BB1_6 Depth=2
	cmpl	$16777216, %ebx         # imm = 0x1000000
	movl	%ecx, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	%ebx, %eax
	movq	200(%rsp), %r10         # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	48(%rsp), %r8d          # 4-byte Reload
	jae	.LBB1_121
.LBB1_119:                              # %.loopexit530
                                        #   in Loop: Header=BB1_6 Depth=2
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	%eax, %r12d
	movl	20(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	80(%rsp), %rbx          # 8-byte Reload
	je	.LBB1_223
# BB#120:                               #   in Loop: Header=BB1_6 Depth=2
	shll	$8, %r12d
	shll	$8, %esi
	decl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movzbl	(%rcx), %eax
	incq	%rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	orl	%eax, %esi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	%ebx, %ecx
	movl	%r12d, %ebx
	movl	8(%rsp), %esi           # 4-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB1_121:                              #   in Loop: Header=BB1_6 Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	movq	72(%rsp), %r9           # 8-byte Reload
	movl	68(%rsp), %ebx          # 4-byte Reload
	cmpl	%r8d, 56(%rsp)          # 4-byte Folded Reload
	jl	.LBB1_115
.LBB1_122:                              #   in Loop: Header=BB1_6 Depth=2
	movl	%r8d, 48(%rsp)          # 4-byte Spill
	addl	%ecx, %ebx
	movl	%ebx, %eax
.LBB1_123:                              #   in Loop: Header=BB1_6 Depth=2
	incl	%eax
	movl	%eax, %r8d
	movl	%ecx, %eax
.LBB1_124:                              #   in Loop: Header=BB1_6 Depth=2
	testl	%r8d, %r8d
	movq	%rax, 80(%rsp)          # 8-byte Spill
	je	.LBB1_225
# BB#125:                               #   in Loop: Header=BB1_6 Depth=2
	cmpl	%r9d, %r8d
	ja	.LBB1_219
# BB#126:                               #   in Loop: Header=BB1_6 Depth=2
	movq	136(%rsp), %rax         # 8-byte Reload
	leal	2(%r9,%rax), %r9d
	leal	2(%rax), %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 136(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_127:                              # %.loopexit531
                                        #   in Loop: Header=BB1_6 Depth=2
	testl	%r14d, %r14d
	je	.LBB1_215
.LBB1_128:                              #   in Loop: Header=BB1_6 Depth=2
	movl	%ebp, %eax
	subl	%r8d, %eax
	cmpl	%r10d, %eax
	movl	%r10d, %ecx
	movl	$0, %edx
	cmovbl	%edx, %ecx
	addl	%eax, %ecx
	movb	(%r11,%rcx), %r15b
	movl	%ebp, %eax
	movb	%r15b, (%r11,%rax)
	incl	%ebp
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	%r10d
	movb	%r15b, (%r13)
	incq	%r13
	decl	%r14d
	movq	136(%rsp), %rax         # 8-byte Reload
	leal	-1(%rax), %ecx
	cmpl	$1, %eax
	movl	%ecx, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	%edx, %ebp
	movl	16(%rsp), %edx          # 4-byte Reload
	jg	.LBB1_127
	jmp	.LBB1_5
.LBB1_232:                              # %.us-lcssa.us.loopexit6021
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movl	16(%rsp), %edx          # 4-byte Reload
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_143
	jmp	.LBB1_233
.LBB1_129:                              # %.loopexit526.loopexit6022
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movl	16(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_193
.LBB1_204:                              # %.loopexit528.loopexit6024
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	16(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_205
.LBB1_130:                              # %.loopexit527.loopexit6023
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	16(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_202
.LBB1_131:                              # %.sink.split.loopexit6038
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	jmp	.LBB1_157
.LBB1_132:                              # %.loopexit532.loopexit6039
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	16(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_210
.LBB1_133:                              # %.us-lcssa1695.us
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	cmpl	$1, 48(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_160
# BB#134:                               #   in Loop: Header=BB1_4 Depth=1
	movslq	%esi, %rax
	leaq	408(%r12,%rax,2), %rdx
	movl	$8, 92(%rsp)            # 4-byte Folded Spill
	jmp	.LBB1_206
.LBB1_135:                              # %.us-lcssa1696.us
                                        #   in Loop: Header=BB1_4 Depth=1
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	movq	%r9, 72(%rsp)           # 8-byte Spill
	je	.LBB1_162
# BB#136:                               #   in Loop: Header=BB1_4 Depth=1
	movslq	%esi, %rax
	leaq	432(%r12,%rax,2), %rdx
	movl	$10, 92(%rsp)           # 4-byte Folded Spill
	jmp	.LBB1_206
.LBB1_137:                              # %.sink.split19.loopexit6030
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movl	$21, 120(%rsp)          # 4-byte Folded Spill
	movl	$1332, %ecx             # imm = 0x534
	movl	%r8d, %ebx
	movl	28(%rsp), %r8d          # 4-byte Reload
	jmp	.LBB1_173
.LBB1_138:                              # %.us-lcssa1697.us
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	je	.LBB1_172
# BB#139:                               #   in Loop: Header=BB1_4 Depth=1
	movslq	%esi, %rax
	leaq	456(%r12,%rax,2), %rdx
	movl	$11, 92(%rsp)           # 4-byte Folded Spill
	jmp	.LBB1_206
.LBB1_140:                              # %.us-lcssa1698.us.loopexit6031
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movl	124(%rsp), %eax         # 4-byte Reload
	movl	48(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB1_165
.LBB1_141:                              # %.loopexit533.loopexit6040
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movl	$18, 92(%rsp)           # 4-byte Folded Spill
	movq	104(%rsp), %rdx         # 8-byte Reload
	jmp	.LBB1_206
.LBB1_142:                              # %.us-lcssa.us.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB1_233
.LBB1_143:                              #   in Loop: Header=BB1_4 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	movb	(%rax), %cl
	movl	$-1, %r12d
	cmpb	$-31, %cl
	ja	.LBB1_218
# BB#144:                               #   in Loop: Header=BB1_4 Depth=1
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movzbl	%cl, %eax
	imull	$109, %eax, %eax
	shrl	$8, %eax
	movl	%ecx, %edx
	subb	%al, %dl
	shrb	%dl
	addb	%al, %dl
	shrb	$5, %dl
	movzbl	%dl, %esi
	movl	%esi, %eax
	movb	$45, %bl
	mulb	%bl
	subb	%al, %cl
	movzbl	%cl, %ecx
	imull	$57, %ecx, %ebx
	andl	$15872, %ebx            # imm = 0x3E00
	shrl	$9, %ebx
	movl	%ebx, %eax
	movb	$9, %dl
	mulb	%dl
	subb	%al, %cl
	movzbl	%cl, %edx
	movl	$1, %eax
	movl	%esi, %ecx
	shll	%cl, %eax
	movl	%eax, 196(%rsp)         # 4-byte Spill
	movl	$1, %eax
	movl	%ebx, %ecx
	shll	%cl, %eax
	movl	%eax, 188(%rsp)         # 4-byte Spill
	movl	%edx, 16(%rsp)          # 4-byte Spill
	addl	%edx, %esi
	movl	$768, %ebx              # imm = 0x300
	movl	%esi, %ecx
	shll	%cl, %ebx
	leal	3692(%rbx,%rbx), %ecx
	movl	168(%rsp), %eax         # 4-byte Reload
	cmpl	%eax, %ecx
	jne	.LBB1_146
# BB#145:                               #   in Loop: Header=BB1_4 Depth=1
	movl	%eax, %r9d
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	jmp	.LBB1_150
.LBB1_146:                              #   in Loop: Header=BB1_4 Depth=1
	movq	%r10, 200(%rsp)         # 8-byte Spill
	movq	%r11, 144(%rsp)         # 8-byte Spill
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	je	.LBB1_148
# BB#147:                               #   in Loop: Header=BB1_4 Depth=1
	callq	free
	movl	56(%rsp), %ecx          # 4-byte Reload
.LBB1_148:                              #   in Loop: Header=BB1_4 Depth=1
	movl	%ecx, %edi
	callq	cli_malloc
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	144(%rsp), %r11         # 8-byte Reload
	movq	200(%rsp), %r10         # 8-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [1024,1024,1024,1024,1024,1024,1024,1024]
	je	.LBB1_218
# BB#149:                               #   in Loop: Header=BB1_4 Depth=1
	movl	56(%rsp), %r9d          # 4-byte Reload
.LBB1_150:                              # %.preheader
                                        #   in Loop: Header=BB1_4 Depth=1
	decl	196(%rsp)               # 4-byte Folded Spill
	decl	188(%rsp)               # 4-byte Folded Spill
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movl	%ebx, %eax
	addl	$1846, %eax             # imm = 0x736
	movq	32(%rsp), %rcx          # 8-byte Reload
	je	.LBB1_189
# BB#151:                               # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_4 Depth=1
	addl	$1845, %ebx             # imm = 0x735
	leaq	1(%rbx), %rax
	cmpq	$16, %rax
	jae	.LBB1_167
# BB#152:                               #   in Loop: Header=BB1_4 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	92(%rsp), %r8d          # 4-byte Reload
	jmp	.LBB1_183
.LBB1_189:                              #   in Loop: Header=BB1_4 Depth=1
	xorl	%r12d, %r12d
	movl	%r9d, 168(%rsp)         # 4-byte Spill
	movl	68(%rsp), %r8d          # 4-byte Reload
	movq	96(%rsp), %r9           # 8-byte Reload
	jmp	.LBB1_190
.LBB1_153:                              # %.loopexit526.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	jmp	.LBB1_193
.LBB1_154:                              # %.loopexit528.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_205
.LBB1_155:                              # %.loopexit527.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_202
.LBB1_156:                              # %.sink.split.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB1_157:                              # %.sink.split
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	160(%rsp), %rdx         # 8-byte Reload
	jmp	.LBB1_206
.LBB1_158:                              # %.loopexit532.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_210
.LBB1_159:                              # %.us-lcssa1695.us.thread.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB1_160:                              # %.us-lcssa1695.us.thread
                                        #   in Loop: Header=BB1_4 Depth=1
	xorl	%eax, %eax
	cmpl	$6, %esi
	setg	%al
	leal	7(%rax,%rax,2), %esi
	movl	$22, 120(%rsp)          # 4-byte Folded Spill
	movl	%r8d, %ebx
	movl	$818, %ecx              # imm = 0x332
	movl	28(%rsp), %edx          # 4-byte Reload
	movl	124(%rsp), %eax         # 4-byte Reload
	jmp	.LBB1_174
.LBB1_161:                              # %.us-lcssa1696.us.thread.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB1_162:                              # %.us-lcssa1696.us.thread
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	%esi, %eax
	shll	$4, %eax
	cltq
	leaq	(%r12,%rax,2), %rax
	movslq	%ecx, %rcx
	leaq	480(%rax,%rcx,2), %rdx
	movl	$9, 92(%rsp)            # 4-byte Folded Spill
	jmp	.LBB1_206
.LBB1_163:                              # %.sink.split19.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movl	$21, 120(%rsp)          # 4-byte Folded Spill
	movl	$1332, %ecx             # imm = 0x534
	movl	28(%rsp), %ebx          # 4-byte Reload
	movl	124(%rsp), %edx         # 4-byte Reload
	movl	156(%rsp), %eax         # 4-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_174
.LBB1_164:                              # %.us-lcssa1698.us.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	xorl	%ecx, %ecx
	movl	124(%rsp), %eax         # 4-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB1_165:                              # %.us-lcssa1698.us
                                        #   in Loop: Header=BB1_4 Depth=1
	testl	%ecx, %ecx
	movl	%eax, %ecx
	movl	156(%rsp), %edx         # 4-byte Reload
	cmovel	%edx, %ecx
	cmovel	%eax, %edx
	movl	$21, 120(%rsp)          # 4-byte Folded Spill
	movl	%edx, %ebx
	movl	%ecx, %eax
	movl	$1332, %ecx             # imm = 0x534
	movl	28(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_174
.LBB1_166:                              # %.loopexit533.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movl	$18, 92(%rsp)           # 4-byte Folded Spill
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_206
.LBB1_167:                              # %min.iters.checked
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	%r9d, 56(%rsp)          # 4-byte Spill
	movq	%r10, 200(%rsp)         # 8-byte Spill
	movq	%rax, %r9
	movabsq	$8589934576, %rcx       # imm = 0x1FFFFFFF0
	andq	%rcx, %r9
	movq	%rax, %r10
	andq	%rcx, %r10
	je	.LBB1_175
# BB#168:                               # %vector.body.preheader
                                        #   in Loop: Header=BB1_4 Depth=1
	leaq	-16(%r10), %r8
	movl	%r8d, %esi
	shrl	$4, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB1_176
# BB#169:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	-14(%rcx,%rbx,2), %rcx
	negq	%rsi
	xorl	%edx, %edx
.LBB1_170:                              # %vector.body.prol
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm0, (%rcx,%rdx,2)
	movups	%xmm0, -16(%rcx,%rdx,2)
	addq	$-16, %rdx
	incq	%rsi
	jne	.LBB1_170
# BB#171:                               # %vector.body.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB1_4 Depth=1
	negq	%rdx
	cmpq	$48, %r8
	jae	.LBB1_177
	jmp	.LBB1_179
.LBB1_172:                              #   in Loop: Header=BB1_4 Depth=1
	movl	$21, 120(%rsp)          # 4-byte Folded Spill
	movl	$1332, %ecx             # imm = 0x534
	movl	28(%rsp), %ebx          # 4-byte Reload
.LBB1_173:                              # %.sink.split19
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	124(%rsp), %edx         # 4-byte Reload
	movl	156(%rsp), %eax         # 4-byte Reload
.LBB1_174:                              # %.sink.split19
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	%eax, 156(%rsp)         # 4-byte Spill
	movl	%edx, 124(%rsp)         # 4-byte Spill
	movl	%r8d, 28(%rsp)          # 4-byte Spill
	leaq	(%r12,%rcx,2), %rdx
	movl	$18, 92(%rsp)           # 4-byte Folded Spill
	movl	%ebx, %r8d
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	jmp	.LBB1_206
.LBB1_175:                              #   in Loop: Header=BB1_4 Depth=1
	movq	200(%rsp), %r10         # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	92(%rsp), %r8d          # 4-byte Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	jmp	.LBB1_182
.LBB1_176:                              #   in Loop: Header=BB1_4 Depth=1
	xorl	%edx, %edx
	cmpq	$48, %r8
	jb	.LBB1_179
.LBB1_177:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r10, %rsi
	subq	%rdx, %rsi
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	-14(%rdx,%rcx,2), %rcx
.LBB1_178:                              # %vector.body
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm0, (%rcx)
	movups	%xmm0, -16(%rcx)
	movups	%xmm0, -32(%rcx)
	movups	%xmm0, -48(%rcx)
	movups	%xmm0, -64(%rcx)
	movups	%xmm0, -80(%rcx)
	movups	%xmm0, -96(%rcx)
	movups	%xmm0, -112(%rcx)
	addq	$-128, %rcx
	addq	$-64, %rsi
	jne	.LBB1_178
.LBB1_179:                              # %middle.block
                                        #   in Loop: Header=BB1_4 Depth=1
	cmpq	%r10, %rax
	jne	.LBB1_181
# BB#180:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%r12d, %r12d
	movl	56(%rsp), %eax          # 4-byte Reload
	movl	%eax, 168(%rsp)         # 4-byte Spill
	movq	96(%rsp), %r9           # 8-byte Reload
	movq	200(%rsp), %r10         # 8-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	68(%rsp), %r8d          # 4-byte Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	jmp	.LBB1_191
.LBB1_181:                              #   in Loop: Header=BB1_4 Depth=1
	subq	%r9, %rbx
	movq	200(%rsp), %r10         # 8-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	92(%rsp), %r8d          # 4-byte Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
.LBB1_182:                              # %.lr.ph.preheader6048
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	56(%rsp), %r9d          # 4-byte Reload
.LBB1_183:                              # %.lr.ph.preheader6048
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	%r8d, 92(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB1_184:                              # %.lr.ph
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	$1024, (%rcx,%rbx,2)    # imm = 0x400
	decq	%rbx
	cmpq	$-1, %rbx
	jne	.LBB1_184
# BB#185:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%r12d, %r12d
	movl	%r9d, 168(%rsp)         # 4-byte Spill
	movl	68(%rsp), %r8d          # 4-byte Reload
	movq	96(%rsp), %r9           # 8-byte Reload
	jmp	.LBB1_191
.LBB1_187:                              #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movslq	%esi, %rax
	leaq	384(%r12,%rax,2), %rdx
	movl	$7, 92(%rsp)            # 4-byte Folded Spill
	movl	$1, 192(%rsp)           # 4-byte Folded Spill
	jmp	.LBB1_206
.LBB1_188:                              #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	2(%rax), %rdx
	movl	$19, 92(%rsp)           # 4-byte Folded Spill
	jmp	.LBB1_206
.LBB1_190:                              # %.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
.LBB1_191:                              # %.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r12, 56(%rsp)          # 8-byte Spill
	incq	%r9
	movq	%r9, 96(%rsp)           # 8-byte Spill
	decl	20(%rsp)                # 4-byte Folded Spill
	cmpl	$4, %r12d
	jge	.LBB1_195
# BB#192:                               #   in Loop: Header=BB1_4 Depth=1
	movq	%rax, 80(%rsp)          # 8-byte Spill
.LBB1_193:                              # %.loopexit526
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	56(%rsp), %r12          # 8-byte Reload
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB1_226
# BB#194:                               #   in Loop: Header=BB1_4 Depth=1
	movq	96(%rsp), %r9           # 8-byte Reload
	movzbl	(%r9), %eax
	movl	%r12d, %ecx
	shlb	$3, %cl
	shll	%cl, %eax
	orl	%eax, 48(%rsp)          # 4-byte Folded Spill
	incl	%r12d
	jmp	.LBB1_190
.LBB1_195:                              #   in Loop: Header=BB1_4 Depth=1
	cmpl	%r10d, 48(%rsp)         # 4-byte Folded Reload
	movq	%rax, 80(%rsp)          # 8-byte Spill
	jne	.LBB1_197
# BB#196:                               #   in Loop: Header=BB1_4 Depth=1
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	jmp	.LBB1_200
.LBB1_197:                              #   in Loop: Header=BB1_4 Depth=1
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	%r8d, 68(%rsp)          # 4-byte Spill
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movq	%rdi, %rbx
	testq	%r11, %r11
	je	.LBB1_199
# BB#198:                               #   in Loop: Header=BB1_4 Depth=1
	movq	%r11, %rdi
	callq	free
.LBB1_199:                              #   in Loop: Header=BB1_4 Depth=1
	movl	48(%rsp), %r12d         # 4-byte Reload
	movl	%r12d, %edi
	callq	cli_malloc
	movq	%rax, %r11
	testq	%r11, %r11
	movl	%r12d, %r10d
	movq	%rbx, %rdi
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	68(%rsp), %r8d          # 4-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [1024,1024,1024,1024,1024,1024,1024,1024]
	je	.LBB1_219
.LBB1_200:                              #   in Loop: Header=BB1_4 Depth=1
	leal	-1(%r10), %eax
	movb	$0, (%r11,%rax)
	movl	$5, %ecx
.LBB1_201:                              #   in Loop: Header=BB1_4 Depth=1
	leal	-1(%rcx), %eax
	testl	%ecx, %ecx
	movl	%eax, %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	je	.LBB1_205
.LBB1_202:                              # %.loopexit527
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movl	%esi, %ebx
	movl	20(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	je	.LBB1_221
# BB#203:                               #   in Loop: Header=BB1_4 Depth=1
	movl	12(%rsp), %esi          # 4-byte Reload
	shll	$8, %esi
	decl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	96(%rsp), %rcx          # 8-byte Reload
	movzbl	(%rcx), %eax
	incq	%rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	orl	%eax, %esi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	%ebx, %esi
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	jmp	.LBB1_201
.LBB1_205:                              # %.loopexit528
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movl	196(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %edx
	movq	%r9, 72(%rsp)           # 8-byte Spill
	andl	%r9d, %edx
	movl	%esi, %ecx
	shll	$4, %ecx
	movslq	%ecx, %rcx
	leaq	(%r12,%rcx,2), %rcx
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	movslq	%edx, %rdx
	leaq	(%rcx,%rdx,2), %rdx
	movl	$6, 92(%rsp)            # 4-byte Folded Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 56(%rsp)          # 8-byte Spill
.LBB1_206:                              # %.sink.split
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	24(%rsp), %r9d          # 4-byte Reload
	movl	%r9d, %ecx
	shrl	$11, %ecx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movzwl	(%rdx), %eax
	imull	%eax, %ecx
	movl	12(%rsp), %edx          # 4-byte Reload
	subl	%ecx, %edx
	movl	%r8d, 68(%rsp)          # 4-byte Spill
	movl	%esi, 8(%rsp)           # 4-byte Spill
	jae	.LBB1_208
# BB#207:                               #   in Loop: Header=BB1_4 Depth=1
	movl	$2048, %edx             # imm = 0x800
	subl	%eax, %edx
	sarl	$5, %edx
	addl	%eax, %edx
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	jmp	.LBB1_209
.LBB1_208:                              #   in Loop: Header=BB1_4 Depth=1
	subl	%ecx, %r9d
	movl	%eax, %ecx
	shrl	$5, %ecx
	subl	%ecx, %eax
	movl	$1, 48(%rsp)            # 4-byte Folded Spill
	movl	%r9d, %ecx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%eax, %edx
.LBB1_209:                              #   in Loop: Header=BB1_4 Depth=1
	movq	160(%rsp), %rax         # 8-byte Reload
	movw	%dx, (%rax)
	cmpl	$16777216, %ecx         # imm = 0x1000000
	movl	92(%rsp), %eax          # 4-byte Reload
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	movl	16(%rsp), %edx          # 4-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	jae	.LBB1_4
.LBB1_210:                              # %.loopexit532
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	20(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jne	.LBB1_3
# BB#211:
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movl	$5, %eax
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	68(%rsp), %r8d          # 4-byte Reload
	movl	8(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB1_217
.LBB1_213:
	movl	%esi, %ebx
	movl	$27, %eax
	xorl	%r14d, %r14d
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_217
.LBB1_49:
	movl	%esi, %ebx
	movl	$27, %eax
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	xorl	%r14d, %r14d
.LBB1_234:                              # %.loopexit534
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_217
.LBB1_215:
	movl	%esi, %ebx
	movl	$28, %eax
	xorl	%r14d, %r14d
.LBB1_216:                              # %.loopexit534
	movl	28(%rsp), %esi          # 4-byte Reload
.LBB1_217:                              # %.loopexit534
	movl	%eax, (%rdi)
	movl	92(%rsp), %eax          # 4-byte Reload
	movl	%eax, 4(%rdi)
	movl	120(%rsp), %eax         # 4-byte Reload
	movl	%eax, 8(%rdi)
	movl	172(%rsp), %eax         # 4-byte Reload
	movl	%eax, 12(%rdi)
	movl	168(%rsp), %eax         # 4-byte Reload
	movl	%eax, 16(%rdi)
	movl	%r10d, 20(%rdi)
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, 24(%rdi)
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, 32(%rdi)
	movl	220(%rsp), %eax         # 4-byte Reload
	movl	%eax, 36(%rdi)
	movq	%r13, 40(%rdi)
	movl	%r14d, 48(%rdi)
	movl	%r9d, 52(%rdi)
	movb	%r15b, 56(%rdi)
	movb	47(%rsp), %al           # 1-byte Reload
	movb	%al, 57(%rdi)
	movzwl	212(%rsp), %eax
	movq	272(%rsp), %rcx         # 8-byte Reload
	movw	%ax, 4(%rcx)
	movl	208(%rsp), %eax
	movl	%eax, (%rcx)
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rax, 64(%rdi)
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	%rax, 72(%rdi)
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	%eax, 80(%rdi)
	movq	176(%rsp), %rax         # 8-byte Reload
	movl	%eax, 84(%rdi)
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	%eax, 88(%rdi)
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	%eax, 92(%rdi)
	movl	48(%rsp), %eax          # 4-byte Reload
	movl	%eax, 96(%rdi)
	movl	%edx, 100(%rdi)
	movl	%ebx, 104(%rdi)
	movl	192(%rsp), %eax         # 4-byte Reload
	movl	%eax, 108(%rdi)
	movq	136(%rsp), %rax         # 8-byte Reload
	movl	%eax, 112(%rdi)
	movl	%r8d, 116(%rdi)
	movl	%esi, 120(%rdi)
	movl	124(%rsp), %eax         # 4-byte Reload
	movl	%eax, 124(%rdi)
	movl	156(%rsp), %eax         # 4-byte Reload
	movl	%eax, 128(%rdi)
	movl	196(%rsp), %eax         # 4-byte Reload
	movl	%eax, 132(%rdi)
	movl	188(%rsp), %eax         # 4-byte Reload
	movl	%eax, 136(%rdi)
	movl	%ebp, 140(%rdi)
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	%eax, 144(%rdi)
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 148(%rdi)
	movq	%r11, 152(%rdi)
	movq	%r12, 160(%rdi)
	xorl	%r12d, %r12d
.LBB1_218:                              # %.loopexit524
	movl	%r12d, %eax
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_219:                              # %.loopexit524.loopexit6020
	movl	$-1, %r12d
	jmp	.LBB1_218
.LBB1_220:
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	movl	$24, %eax
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movl	8(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB1_216
.LBB1_221:
	movl	$3, %eax
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movq	72(%rsp), %r9           # 8-byte Reload
	jmp	.LBB1_216
.LBB1_222:
	movl	$26, %eax
	xorl	%r14d, %r14d
	movl	16(%rsp), %edx          # 4-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_217
.LBB1_223:
	movl	$16, %eax
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movl	%r8d, 48(%rsp)          # 4-byte Spill
	movq	72(%rsp), %r9           # 8-byte Reload
	movl	68(%rsp), %r8d          # 4-byte Reload
	movl	%r12d, 24(%rsp)         # 4-byte Spill
	movl	8(%rsp), %ebx           # 4-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_216
.LBB1_224:
	movl	$14, %eax
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movq	144(%rsp), %r11         # 8-byte Reload
	jmp	.LBB1_227
.LBB1_225:
	movl	%esi, %ebx
	movl	$-1, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	xorl	%r8d, %r8d
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	116(%rsp), %eax         # 4-byte Reload
	jmp	.LBB1_217
.LBB1_226:
	movl	%esi, %ebx
	movl	$1, %eax
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB1_227:                              # %.loopexit534
	movq	72(%rsp), %r9           # 8-byte Reload
	jmp	.LBB1_217
.LBB1_228:
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	movl	$12, %eax
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movl	8(%rsp), %ebx           # 4-byte Reload
	movl	68(%rsp), %r8d          # 4-byte Reload
	jmp	.LBB1_216
.LBB1_229:
	movl	$15, %eax
	jmp	.LBB1_231
.LBB1_230:
	movl	$13, %eax
.LBB1_231:                              # %.loopexit534
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB1_217
.LBB1_233:
	movl	%esi, %ebx
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	jmp	.LBB1_234
.Lfunc_end1:
	.size	lzmaDecode, .Lfunc_end1-lzmaDecode
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_232
	.quad	.LBB1_129
	.quad	.LBB1_204
	.quad	.LBB1_130
	.quad	.LBB1_131
	.quad	.LBB1_132
	.quad	.LBB1_58
	.quad	.LBB1_133
	.quad	.LBB1_135
	.quad	.LBB1_137
	.quad	.LBB1_138
	.quad	.LBB1_140
	.quad	.LBB1_93
	.quad	.LBB1_65
	.quad	.LBB1_66
	.quad	.LBB1_67
	.quad	.LBB1_94
	.quad	.LBB1_141
	.quad	.LBB1_7
	.quad	.LBB1_9
	.quad	.LBB1_68
	.quad	.LBB1_95
	.quad	.LBB1_10
	.quad	.LBB1_11
	.quad	.LBB1_12
	.quad	.LBB1_96
	.quad	.LBB1_69
	.quad	.LBB1_212
	.quad	.LBB1_214
.LJTI1_1:
	.quad	.LBB1_142
	.quad	.LBB1_153
	.quad	.LBB1_154
	.quad	.LBB1_155
	.quad	.LBB1_156
	.quad	.LBB1_158
	.quad	.LBB1_186
	.quad	.LBB1_159
	.quad	.LBB1_161
	.quad	.LBB1_46
	.quad	.LBB1_163
	.quad	.LBB1_164
	.quad	.LBB1_97
	.quad	.LBB1_50
	.quad	.LBB1_51
	.quad	.LBB1_52
	.quad	.LBB1_99
	.quad	.LBB1_166
	.quad	.LBB1_13
	.quad	.LBB1_15
	.quad	.LBB1_53
	.quad	.LBB1_101
	.quad	.LBB1_17
	.quad	.LBB1_22
	.quad	.LBB1_20
	.quad	.LBB1_103
	.quad	.LBB1_54
	.quad	.LBB1_48
	.quad	.LBB1_108

	.text
	.globl	lzmaShutdown
	.p2align	4, 0x90
	.type	lzmaShutdown,@function
lzmaShutdown:                           # @lzmaShutdown
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
	subq	$160, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 192
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	leaq	8(%rsp), %rdi
	movl	$152, %edx
	movq	%rbx, %rsi
	callq	memcpy
	movq	152(%rbx), %r14
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	callq	free
.LBB2_2:
	leaq	152(%rbx), %r15
	testq	%r14, %r14
	je	.LBB2_4
# BB#3:
	movq	%r14, %rdi
	callq	free
.LBB2_4:
	leaq	8(%rsp), %rsi
	movl	$152, %edx
	movq	%rbx, %rdi
	callq	memcpy
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	addq	$160, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	lzmaShutdown, .Lfunc_end2-lzmaShutdown
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
