	.text
	.file	"z08.bc"
	.globl	ReplaceWithTidy
	.p2align	4, 0x90
	.type	ReplaceWithTidy,@function
ReplaceWithTidy:                        # @ReplaceWithTidy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	cmpb	$17, 32(%r12)
	jne	.LBB0_1
# BB#2:
	movl	%esi, 20(%rsp)          # 4-byte Spill
	movq	8(%r12), %rbx
	cmpq	%r12, %rbx
	movq	%r12, %r14
	je	.LBB0_21
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader142
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB0_4:                                #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_4
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpb	$17, %al
	jne	.LBB0_19
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	8(%rbp), %r14
	cmpq	%rbp, %r14
	je	.LBB0_11
# BB#7:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpb	$0, 32(%r14)
	je	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_9:                                #   in Loop: Header=BB0_3 Depth=1
	movq	%r14, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r14), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%r14, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_11
# BB#10:                                #   in Loop: Header=BB0_3 Depth=1
	movq	(%rbx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r14), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB0_11:                               #   in Loop: Header=BB0_3 Depth=1
	movq	%rbx, xx_link(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB0_12
# BB#13:                                #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, zz_res(%rip)
	movq	16(%rbx), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rbx), %rcx
	movq	%rax, 24(%rcx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	jmp	.LBB0_14
.LBB0_12:                               #   in Loop: Header=BB0_3 Depth=1
	xorl	%eax, %eax
.LBB0_14:                               #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB0_16
# BB#15:                                #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, zz_res(%rip)
	movq	(%rbx), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rbx
.LBB0_16:                               #   in Loop: Header=BB0_3 Depth=1
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_18
# BB#17:                                #   in Loop: Header=BB0_3 Depth=1
	callq	DisposeObject
.LBB0_18:                               #   in Loop: Header=BB0_3 Depth=1
	movq	(%r14), %rbx
.LBB0_19:                               # %.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r12, %rbx
	jne	.LBB0_3
# BB#20:                                # %._crit_edge160.loopexit
	movq	8(%r12), %r14
.LBB0_21:                               # %._crit_edge160
	movl	$0, ReplaceWithTidy.buff_len(%rip)
	movl	$11, ReplaceWithTidy.buff_typ(%rip)
	movzwl	34(%r12), %eax
	movw	%ax, ReplaceWithTidy.buff_pos+2(%rip)
	movl	36(%r12), %eax
	movl	%eax, ReplaceWithTidy.buff_pos+4(%rip)
	cmpq	%r12, %r14
	je	.LBB0_22
# BB#23:                                # %.preheader141.lr.ph
	xorl	%ebx, %ebx
.LBB0_24:                               # %.preheader141
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_25 Depth 2
                                        #     Child Loop BB0_34 Depth 2
	movq	%r14, %r13
	.p2align	4, 0x90
.LBB0_25:                               #   Parent Loop BB0_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %r13
	movzbl	32(%r13), %eax
	testb	%al, %al
	je	.LBB0_25
# BB#26:                                #   in Loop: Header=BB0_24 Depth=1
	leaq	32(%r13), %r15
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jae	.LBB0_27
# BB#40:                                #   in Loop: Header=BB0_24 Depth=1
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movslq	ReplaceWithTidy.buff_len(%rip), %rbx
	leaq	64(%r13), %rbp
	movq	%rbp, %rdi
	callq	strlen
	addq	%rbx, %rax
	cmpq	$512, %rax              # imm = 0x200
	jb	.LBB0_42
# BB#41:                                #   in Loop: Header=BB0_24 Depth=1
	movl	$8, %edi
	movl	$1, %esi
	movl	$.L.str.2, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB0_37
	.p2align	4, 0x90
.LBB0_27:                               #   in Loop: Header=BB0_24 Depth=1
	cmpb	$1, %al
	jne	.LBB0_70
# BB#28:                                #   in Loop: Header=BB0_24 Depth=1
	cmpq	%r13, 8(%r13)
	jne	.LBB0_30
# BB#29:                                #   in Loop: Header=BB0_24 Depth=1
	movzbl	41(%r13), %eax
	movzbl	42(%r13), %ecx
	addl	%eax, %ecx
	je	.LBB0_37
.LBB0_30:                               #   in Loop: Header=BB0_24 Depth=1
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB0_48
# BB#31:                                #   in Loop: Header=BB0_24 Depth=1
	movslq	ReplaceWithTidy.buff_len(%rip), %rax
	movzbl	41(%r13), %ecx
	leal	(%rcx,%rax), %esi
	movzbl	42(%r13), %edx
	addl	%edx, %esi
	cmpl	$511, %esi              # imm = 0x1FF
	jle	.LBB0_32
# BB#47:                                #   in Loop: Header=BB0_24 Depth=1
	movl	$8, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
	jmp	.LBB0_37
	.p2align	4, 0x90
.LBB0_42:                               #   in Loop: Header=BB0_24 Depth=1
	testl	%ebx, %ebx
	jne	.LBB0_44
# BB#43:                                #   in Loop: Header=BB0_24 Depth=1
	movzwl	34(%r13), %eax
	movw	%ax, ReplaceWithTidy.buff_pos+2(%rip)
	movl	36(%r13), %eax
	movl	%eax, ReplaceWithTidy.buff_pos+4(%rip)
.LBB0_44:                               #   in Loop: Header=BB0_24 Depth=1
	leaq	ReplaceWithTidy.buff(%rbx), %rdi
	movq	%rbp, %rsi
	callq	strcpy
	movq	%rbp, %rdi
	callq	strlen
	addl	%eax, ReplaceWithTidy.buff_len(%rip)
	cmpb	$12, (%r15)
	jne	.LBB0_46
# BB#45:                                #   in Loop: Header=BB0_24 Depth=1
	movl	$12, ReplaceWithTidy.buff_typ(%rip)
.LBB0_46:                               #   in Loop: Header=BB0_24 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB0_37
.LBB0_48:                               #   in Loop: Header=BB0_24 Depth=1
	movl	ReplaceWithTidy.buff_typ(%rip), %edi
	movl	$ReplaceWithTidy.buff, %esi
	movl	$ReplaceWithTidy.buff_pos, %edx
	callq	MakeWord
	movq	%rax, %r15
	movl	$0, ReplaceWithTidy.buff_len(%rip)
	movl	$11, ReplaceWithTidy.buff_typ(%rip)
	testq	%rbx, %rbx
	jne	.LBB0_53
# BB#49:                                #   in Loop: Header=BB0_24 Depth=1
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_50
# BB#51:                                #   in Loop: Header=BB0_24 Depth=1
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_52
.LBB0_32:                               # %.preheader
                                        #   in Loop: Header=BB0_24 Depth=1
	addl	%ecx, %edx
	je	.LBB0_36
# BB#33:                                # %.lr.ph
                                        #   in Loop: Header=BB0_24 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_34:                               #   Parent Loop BB0_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	$32, ReplaceWithTidy.buff(%rax,%rcx)
	movzbl	41(%r13), %edx
	movzbl	42(%r13), %esi
	addl	%edx, %esi
	incq	%rcx
	cmpl	%esi, %ecx
	jl	.LBB0_34
# BB#35:                                # %._crit_edge
                                        #   in Loop: Header=BB0_24 Depth=1
	addl	%eax, %ecx
	movl	%ecx, ReplaceWithTidy.buff_len(%rip)
.LBB0_36:                               #   in Loop: Header=BB0_24 Depth=1
	movl	$12, ReplaceWithTidy.buff_typ(%rip)
	jmp	.LBB0_37
.LBB0_50:                               #   in Loop: Header=BB0_24 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB0_52:                               #   in Loop: Header=BB0_24 Depth=1
	movb	$17, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	34(%r12), %eax
	movw	%ax, 34(%rbx)
	movl	36(%r12), %eax
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	%ecx, %eax
	movl	36(%rbx), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movl	36(%r12), %ecx
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
.LBB0_53:                               #   in Loop: Header=BB0_24 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_54
# BB#55:                                #   in Loop: Header=BB0_24 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_56
.LBB0_54:                               #   in Loop: Header=BB0_24 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_56:                               #   in Loop: Header=BB0_24 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#57:                                #   in Loop: Header=BB0_24 Depth=1
	testq	%rax, %rax
	je	.LBB0_59
# BB#58:                                #   in Loop: Header=BB0_24 Depth=1
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_59:                               #   in Loop: Header=BB0_24 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB0_62
# BB#60:                                #   in Loop: Header=BB0_24 Depth=1
	testq	%rax, %rax
	je	.LBB0_62
# BB#61:                                #   in Loop: Header=BB0_24 Depth=1
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_62:                               #   in Loop: Header=BB0_24 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_63
# BB#64:                                #   in Loop: Header=BB0_24 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_65
.LBB0_63:                               #   in Loop: Header=BB0_24 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_65:                               #   in Loop: Header=BB0_24 Depth=1
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB0_68
# BB#66:                                #   in Loop: Header=BB0_24 Depth=1
	testq	%rax, %rax
	je	.LBB0_68
# BB#67:                                #   in Loop: Header=BB0_24 Depth=1
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_68:                               #   in Loop: Header=BB0_24 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_37
# BB#69:                                #   in Loop: Header=BB0_24 Depth=1
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	.p2align	4, 0x90
.LBB0_37:                               #   in Loop: Header=BB0_24 Depth=1
	movq	8(%r14), %r14
	cmpq	%r12, %r14
	jne	.LBB0_24
# BB#38:                                # %._crit_edge156
	movl	ReplaceWithTidy.buff_typ(%rip), %edi
	movl	$ReplaceWithTidy.buff, %esi
	movl	$ReplaceWithTidy.buff_pos, %edx
	callq	MakeWord
	movq	%rax, %rbp
	testq	%rbx, %rbx
	je	.LBB0_39
# BB#73:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_74
# BB#75:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_76
.LBB0_1:
	movq	%r12, %rbx
	jmp	.LBB0_87
.LBB0_70:
	testq	%rbx, %rbx
	je	.LBB0_71
# BB#72:
	movq	%rbx, %rdi
	callq	DisposeObject
	movq	%r12, %rbx
	jmp	.LBB0_87
.LBB0_22:                               # %._crit_edge156.thread
	movl	$11, %edi
	movl	$ReplaceWithTidy.buff, %esi
	movl	$ReplaceWithTidy.buff_pos, %edx
	callq	MakeWord
	movq	%rax, %rbx
.LBB0_81:
	movq	%r12, zz_hold(%rip)
	movq	24(%r12), %rax
	cmpq	%r12, %rax
	je	.LBB0_82
# BB#83:
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r12), %rcx
	movq	%rax, 24(%rcx)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_86
# BB#84:
	testq	%rax, %rax
	je	.LBB0_86
# BB#85:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB0_86
.LBB0_71:
	movq	%r12, %rbx
	jmp	.LBB0_87
.LBB0_82:                               # %.thread
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB0_86:
	movq	%r12, %rdi
	callq	DisposeObject
.LBB0_87:
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_39:
	movq	%rbp, %rbx
	jmp	.LBB0_81
.LBB0_74:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_76:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_78
# BB#77:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_78:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_81
# BB#79:
	testq	%rax, %rax
	je	.LBB0_81
# BB#80:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	jmp	.LBB0_81
.Lfunc_end0:
	.size	ReplaceWithTidy, .Lfunc_end0-ReplaceWithTidy
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1124073472              # float 128
	.text
	.globl	Manifest
	.p2align	4, 0x90
	.type	Manifest,@function
Manifest:                               # @Manifest
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$728, %rsp              # imm = 0x2D8
.Lcfi19:
	.cfi_def_cfa_offset 784
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r13
	movl	Manifest.depth(%rip), %eax
	incl	%eax
	movl	%eax, Manifest.depth(%rip)
	cmpl	$1000, %eax             # imm = 0x3E8
	jne	.LBB1_2
# BB#1:
	leaq	32(%r13), %r8
	movl	$8, %edi
	movl	$40, %esi
	movl	$.L.str.4, %edx
	movl	$1, %ecx
	movl	$1000, %r9d             # imm = 0x3E8
	xorl	%eax, %eax
	callq	Error
.LBB1_2:
	movzbl	32(%r13), %ebp
	movl	%ebp, %eax
	addb	$-2, %al
	cmpb	$97, %al
	ja	.LBB1_161
# BB#3:
	movl	800(%rsp), %ecx
	movq	784(%rsp), %r12
	leaq	32(%r13), %rbx
	movzbl	%al, %eax
	movq	%r13, 112(%rsp)         # 8-byte Spill
	movq	%r15, 56(%rsp)          # 8-byte Spill
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_4:
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_5
# BB#6:
	subq	$8, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset -48
	cmpq	$0, 8(%r14)
	jne	.LBB1_518
	jmp	.LBB1_515
.LBB1_7:
	movq	%rbx, %rbp
	movq	%r14, 88(%rsp)          # 8-byte Spill
	leaq	8(%r13), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	8(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_10
# BB#8:
	cmpq	%r13, 8(%rax)
	je	.LBB1_10
# BB#9:                                 # %._crit_edge3138
	movq	%r12, %r14
	jmp	.LBB1_11
.LBB1_10:
	movq	%r12, %r14
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.31, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
.LBB1_11:
	movzwl	(%r15), %ecx
	movl	%ecx, %edx
	andl	$65408, %edx            # imm = 0xFF80
	movw	%dx, 40(%rsp)
	movzwl	2(%r15), %esi
	movw	%si, 42(%rsp)
	movb	4(%r15), %bl
	andb	$127, %bl
	movb	%bl, 44(%rsp)
	movl	%ecx, %ebx
	andb	$8, %bl
	orb	%bl, %dl
	movzwl	4(%r15), %esi
	movl	%esi, %edi
	andl	$128, %edi
	movzwl	44(%rsp), %ebx
	andl	$127, %ebx
	orl	%edi, %ebx
	movl	%esi, %edi
	andl	$256, %edi              # imm = 0x100
	orl	%ebx, %edi
	movl	%esi, %ebx
	andl	$512, %ebx              # imm = 0x200
	orl	%edi, %ebx
	andl	$64512, %esi            # imm = 0xFC00
	orl	%ebx, %esi
	movw	%si, 44(%rsp)
	movzwl	6(%r15), %esi
	movw	%si, 46(%rsp)
	movl	12(%r15), %esi
	movl	%esi, 52(%rsp)
	movl	%ecx, %ebx
	andb	$3, %bl
	andb	$116, %cl
	orb	%bl, %cl
	orb	%dl, %cl
	movb	%cl, 40(%rsp)
	movzwl	8(%r15), %ecx
	movw	%cx, 48(%rsp)
	movzwl	10(%r15), %ecx
	movw	%cx, 50(%rsp)
	addq	$16, %rax
	movq	8(%rsp), %r12           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_12:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_12
# BB#13:
	subq	$8, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	%r12, %rsi
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi39:
	.cfi_adjust_cfa_offset -48
	xorl	%esi, %esi
	cmpb	$65, (%rbp)
	sete	%sil
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movb	(%rbp), %cl
	addb	$-59, %cl
	cmpb	$8, %cl
	ja	.LBB1_295
# BB#14:
	movzbl	%cl, %ecx
	jmpq	*.LJTI1_1(,%rcx,8)
.LBB1_15:
	leaq	40(%rsp), %rdi
	movq	%rax, %rsi
	callq	FontChange
	jmp	.LBB1_295
.LBB1_16:
	movzwl	(%r15), %eax
	movl	%eax, %ecx
	andl	$65408, %ecx            # imm = 0xFF80
	movw	%cx, 40(%rsp)
	movzwl	2(%r15), %edx
	movw	%dx, 42(%rsp)
	movb	4(%r15), %dl
	andb	$127, %dl
	movb	%dl, 44(%rsp)
	movl	%eax, %edx
	andb	$8, %dl
	orb	%dl, %cl
	movzwl	4(%r15), %edx
	movl	%edx, %esi
	andl	$128, %esi
	movzwl	44(%rsp), %edi
	andl	$127, %edi
	orl	%esi, %edi
	movl	%edx, %esi
	andl	$256, %esi              # imm = 0x100
	orl	%edi, %esi
	movl	%edx, %edi
	andl	$512, %edi              # imm = 0x200
	orl	%esi, %edi
	andl	$64512, %edx            # imm = 0xFC00
	orl	%edi, %edx
	movw	%dx, 44(%rsp)
	movzwl	6(%r15), %edx
	movw	%dx, 46(%rsp)
	movl	12(%r15), %edx
	movl	%edx, 52(%rsp)
	movl	%eax, %ebx
	andb	$3, %bl
	andb	$116, %al
	orb	%bl, %al
	orb	%cl, %al
	movb	%al, 40(%rsp)
	movzwl	8(%r15), %ecx
	movw	%cx, 48(%rsp)
	movzwl	10(%r15), %ecx
	movw	%cx, 50(%rsp)
	cmpb	$49, %bpl
	je	.LBB1_131
# BB#17:
	cmpb	$66, %bpl
	jne	.LBB1_132
# BB#18:
	andl	$-12582913, %edx        # imm = 0xFF3FFFFF
	orl	$4194304, %edx          # imm = 0x400000
	movl	%edx, 52(%rsp)
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB1_137
.LBB1_19:
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r14, 88(%rsp)          # 8-byte Spill
	leaq	8(%r13), %r14
	movq	8(%r13), %rbp
	cmpq	%r13, %rbp
	je	.LBB1_21
# BB#20:
	cmpq	%r13, 8(%rbp)
	jne	.LBB1_22
.LBB1_21:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.33, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r14), %rbp
	.p2align	4, 0x90
.LBB1_22:                               # =>This Inner Loop Header: Depth=1
	addq	$16, %rbp
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB1_22
# BB#23:
	movq	%r12, %r13
	cmpb	$17, %al
	jne	.LBB1_117
# BB#24:                                # %.loopexit2600.loopexit
	movq	%rbp, %rbx
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	8(%rsp), %r12           # 8-byte Reload
	jmp	.LBB1_25
.LBB1_37:
	movq	8(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_39
# BB#38:
	cmpq	%rax, (%r13)
	jne	.LBB1_40
.LBB1_39:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_40:
	movl	816(%rsp), %eax
	testl	%eax, %eax
	jne	.LBB1_665
# BB#41:
	movq	8(%r13), %rax
	.p2align	4, 0x90
.LBB1_42:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB1_42
# BB#43:
	cmpb	$2, %cl
	jne	.LBB1_412
# BB#44:
	leaq	120(%rsp), %r8
	movq	%r13, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r15, %rdx
	movq	%r12, %r13
	movq	%r12, %rcx
	callq	CrossExpand
	movq	%rax, %rbp
	cmpb	$2, 32(%rbp)
	je	.LBB1_46
# BB#45:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_46:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_975
# BB#47:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_976
.LBB1_48:
	movl	792(%rsp), %eax
	testl	%eax, %eax
	je	.LBB1_141
# BB#49:
	cmpq	$0, (%r12)
	je	.LBB1_141
# BB#50:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_701
# BB#51:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_702
.LBB1_52:
	movzwl	(%r15), %eax
	movl	%eax, %ecx
	andl	$65408, %ecx            # imm = 0xFF80
	movw	%cx, 40(%rsp)
	movzwl	2(%r15), %edx
	movw	%dx, 42(%rsp)
	movb	4(%r15), %dl
	andb	$127, %dl
	movb	%dl, 44(%rsp)
	movl	%eax, %edx
	andb	$8, %dl
	orb	%dl, %cl
	movzwl	4(%r15), %edx
	movl	%edx, %esi
	andl	$128, %esi
	movzwl	44(%rsp), %edi
	andl	$127, %edi
	orl	%esi, %edi
	movl	%edx, %esi
	andl	$256, %esi              # imm = 0x100
	orl	%edi, %esi
	movl	%edx, %edi
	andl	$512, %edi              # imm = 0x200
	orl	%esi, %edi
	andl	$64512, %edx            # imm = 0xFC00
	orl	%edi, %edx
	movw	%dx, 44(%rsp)
	movzwl	6(%r15), %edx
	movw	%dx, 46(%rsp)
	movl	12(%r15), %edx
	movl	%edx, 52(%rsp)
	movl	%eax, %edx
	andb	$3, %dl
	movl	%eax, %ebx
	andb	$116, %bl
	orb	%dl, %bl
	orb	%cl, %bl
	movzwl	8(%r15), %ecx
	movw	%cx, 48(%rsp)
	movzwl	10(%r15), %ecx
	movw	%cx, 50(%rsp)
	cmpb	$18, %bpl
	sete	%cl
	setne	%r15b
	shrb	%cl, %al
	movl	%r15d, %ecx
	addb	$-3, %cl
	movzwl	42(%r13), %edx
	andb	$1, %al
	movzbl	%al, %eax
	shll	$11, %eax
	andl	$63487, %edx            # imm = 0xF7FF
	orl	%eax, %edx
	movw	%dx, 42(%r13)
	andb	%bl, %cl
	movb	%cl, 40(%rsp)
	movq	8(%r13), %r12
	movq	8(%r12), %rax
	cmpq	%r13, %r12
	je	.LBB1_54
# BB#53:
	cmpq	%r13, %rax
	jne	.LBB1_55
.LBB1_54:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.56, %r9d
	movq	%rax, %rbx
	xorl	%eax, %eax
	callq	Error
	movq	%rbx, %rax
.LBB1_55:                               # %.preheader2571.preheader
	movq	%r14, 88(%rsp)          # 8-byte Spill
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB1_56:                               # %.preheader2571
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB1_56
# BB#57:                                # %.preheader2570.preheader
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB1_58:                               # %.preheader2570
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %r14
	cmpb	$0, 32(%r14)
	je	.LBB1_58
# BB#59:
	cmpb	$18, %bpl
	movl	$0, %ebp
	sete	%bpl
	movl	$0, %edx
	movb	%r15b, %dl
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rdx,8), %rax
	movq	%rax, 208(%rsp,%rdx,8)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	movq	%rax, 128(%rsp,%rdx,8)
	cmpq	$0, (%rcx,%rbp,8)
	je	.LBB1_342
# BB#60:
	movzbl	zz_lengths+139(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_402
# BB#61:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_403
.LBB1_62:
	movq	%rcx, %rbx
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_63:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_63
# BB#64:
	subq	$8, %rsp
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi46:
	.cfi_adjust_cfa_offset -48
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	leaq	64(%r13), %rdx
	leaq	100(%rsp), %rcx
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	GetGap
	movq	(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_65:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_65
# BB#66:
	subq	$8, %rsp
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi53:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB1_665
.LBB1_67:
	movq	%rcx, %rbp
	movl	$11, %edi
	movl	$.L.str.8, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_343
# BB#68:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_344
.LBB1_69:
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_70:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_70
# BB#71:
	subq	$8, %rsp
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi60:
	.cfi_adjust_cfa_offset -48
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %rbp
	leaq	72(%rsp), %rdx
	leaq	100(%rsp), %rcx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	GetGap
	cmpl	$158, 100(%rsp)
	jne	.LBB1_119
# BB#72:
	movzwl	72(%rsp), %eax
	andl	$64512, %eax            # imm = 0xFC00
	cmpl	$9216, %eax             # imm = 0x2400
	jne	.LBB1_119
# BB#73:                                # %._crit_edge3095
	movswl	74(%rsp), %eax
	jmp	.LBB1_120
.LBB1_74:
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_75:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_75
# BB#76:
	subq	$8, %rsp
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi67:
	.cfi_adjust_cfa_offset -48
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %rbx
	leaq	64(%r13), %rdx
	leaq	100(%rsp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	GetGap
	movzwl	100(%rsp), %eax
	movw	%ax, 70(%r13)
	movzwl	64(%r13), %eax
	movl	%eax, %ecx
	andl	$57344, %ecx            # imm = 0xE000
	cmpl	$8192, %ecx             # imm = 0x2000
	jne	.LBB1_78
# BB#77:
	andl	$60416, %eax            # imm = 0xEC00
	shrl	$10, %eax
	orb	$4, %al
	andb	$7, %al
	cmpb	$5, %al
	je	.LBB1_79
.LBB1_78:
	addq	$32, %rbx
	movzbl	32(%r13), %edi
	callq	Image
	movq	%rax, %rbp
	movl	$8, %edi
	movl	$27, %esi
	movl	$.L.str.19, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
	movw	$159, 70(%r13)
	movzwl	64(%r13), %eax
	movw	$0, 66(%r13)
	andl	$1023, %eax             # imm = 0x3FF
	orl	$9216, %eax             # imm = 0x2400
	movw	%ax, 64(%r13)
	jmp	.LBB1_79
.LBB1_81:
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_82:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_82
# BB#83:
	subq	$8, %rsp
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi73:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi74:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB1_140
.LBB1_84:
	movl	$4095, %edi             # imm = 0xFFF
	andl	12(%r15), %edi
	je	.LBB1_350
# BB#85:
	cmpb	$69, %bpl
	jne	.LBB1_356
# BB#86:
	callq	FontFamily
	jmp	.LBB1_357
.LBB1_87:
	cmpb	$71, %bpl
	jne	.LBB1_284
# BB#88:
	movswl	8(%r15), %eax
	jmp	.LBB1_285
.LBB1_89:
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_90:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_90
# BB#91:
	subq	$8, %rsp
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi78:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi80:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi81:
	.cfi_adjust_cfa_offset -48
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %rbp
	movq	8(%r13), %rax
	movq	8(%rax), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_92:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_92
# BB#93:
	subq	$8, %rsp
.Lcfi82:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi83:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi84:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi85:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi87:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi88:
	.cfi_adjust_cfa_offset -48
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, 64(%rsp)
	movb	32(%rbp), %al
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB1_115
# BB#94:
	addq	$64, %rbp
	leaq	188(%rsp), %rdx
	movl	$.L.str.39, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB1_115
# BB#95:
	movq	64(%rsp), %rdi
	movb	32(%rdi), %al
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB1_115
# BB#96:
	addq	$64, %rdi
	leaq	184(%rsp), %rdx
	movl	$.L.str.39, %esi
	xorl	%eax, %eax
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB1_115
# BB#97:
	movl	184(%rsp), %eax
	movl	%eax, %edx
	negl	%edx
	movq	32(%rsp), %rbp          # 8-byte Reload
	cmpb	$79, (%rbp)
	cmovel	%eax, %edx
	addl	188(%rsp), %edx
	leaq	208(%rsp), %rbx
	movl	$.L.str.39, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	movl	$11, %edi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	jmp	.LBB1_116
.LBB1_98:
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movzwl	(%r15), %ecx
	andl	$128, %ecx
	movzwl	64(%r13), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$256, %ecx              # imm = 0x100
	movl	%eax, %edx
	andl	$-257, %edx             # imm = 0xFEFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %edx             # imm = 0xFDFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %edx            # imm = 0xE3FF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	2(%r15), %ecx
	movw	%cx, 66(%r13)
	movb	4(%r15), %cl
	andb	$3, %cl
	movb	68(%r13), %dl
	andb	$-4, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	4(%r15), %cl
	andb	$12, %cl
	andb	$-13, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	4(%r15), %cl
	andb	$112, %cl
	andb	$-113, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	(%r15), %cl
	andb	$8, %cl
	andb	$-9, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	4(%r15), %edx
	andl	$128, %edx
	movzwl	68(%r13), %ecx
	andl	$65407, %ecx            # imm = 0xFF7F
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$256, %edx              # imm = 0x100
	andl	$-257, %ecx             # imm = 0xFEFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$512, %edx              # imm = 0x200
	andl	$-513, %ecx             # imm = 0xFDFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$7168, %edx             # imm = 0x1C00
	andl	$-7169, %ecx            # imm = 0xE3FF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$57344, %edx            # imm = 0xE000
	andl	$8191, %ecx             # imm = 0x1FFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	6(%r15), %ecx
	movw	%cx, 70(%r13)
	movl	$4095, %edx             # imm = 0xFFF
	andl	12(%r15), %edx
	movl	$-4096, %ecx            # imm = 0xF000
	andl	76(%r13), %ecx
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	12(%r15), %edx
	andl	$-4190209, %ecx         # imm = 0xFFC00FFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$12582912, %edx         # imm = 0xC00000
	andl	12(%r15), %edx
	andl	$-12582913, %ecx        # imm = 0xFF3FFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1056964608, %edx       # imm = 0x3F000000
	andl	12(%r15), %edx
	andl	$-1056964609, %ecx      # imm = 0xC0FFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	12(%r15), %edx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1073741824, %edx       # imm = 0x40000000
	andl	12(%r15), %edx
	andl	$-1073741825, %ecx      # imm = 0xBFFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movb	(%r15), %cl
	andb	$1, %cl
	andb	$-2, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$2, %cl
	andb	$-3, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$4, %cl
	andb	$-5, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$112, %cl
	andb	$-113, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	8(%r15), %eax
	movw	%ax, 72(%r13)
	movzwl	10(%r15), %eax
	movw	%ax, 74(%r13)
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_99:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_99
# BB#100:
	subq	$8, %rsp
.Lcfi89:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi90:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi91:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi92:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi95:
	.cfi_adjust_cfa_offset -48
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %rbp
	movb	32(%rbp), %al
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB1_514
# BB#101:
	addq	$32, %rbp
	cmpb	$94, 32(%r13)
	movl	$.L.str.44, %eax
	movl	$.L.str.45, %r9d
	cmoveq	%rax, %r9
	movl	$8, %edi
	movl	$37, %esi
	movl	$.L.str.43, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
	movl	$11, %edi
	movl	$.L.str.8, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, %rbx
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_416
# BB#102:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB1_417
# BB#103:
	testq	%rax, %rax
	je	.LBB1_417
# BB#104:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB1_417
.LBB1_105:
	movq	(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_106:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_106
# BB#107:
	subq	$8, %rsp
.Lcfi96:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi97:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi98:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi100:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi101:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi102:
	.cfi_adjust_cfa_offset -48
	movzwl	(%r15), %ecx
	andl	$128, %ecx
	movzwl	64(%r13), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$256, %ecx              # imm = 0x100
	movl	%eax, %edx
	andl	$-257, %edx             # imm = 0xFEFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %edx             # imm = 0xFDFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %edx            # imm = 0xE3FF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	2(%r15), %ecx
	movw	%cx, 66(%r13)
	movb	4(%r15), %cl
	andb	$3, %cl
	movb	68(%r13), %dl
	andb	$-4, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	4(%r15), %cl
	andb	$12, %cl
	andb	$-13, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	4(%r15), %cl
	andb	$112, %cl
	andb	$-113, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	(%r15), %cl
	andb	$8, %cl
	andb	$-9, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	4(%r15), %edx
	andl	$128, %edx
	movzwl	68(%r13), %ecx
	andl	$65407, %ecx            # imm = 0xFF7F
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$256, %edx              # imm = 0x100
	andl	$-257, %ecx             # imm = 0xFEFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$512, %edx              # imm = 0x200
	andl	$-513, %ecx             # imm = 0xFDFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$7168, %edx             # imm = 0x1C00
	andl	$-7169, %ecx            # imm = 0xE3FF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$57344, %edx            # imm = 0xE000
	andl	$8191, %ecx             # imm = 0x1FFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	6(%r15), %ecx
	movw	%cx, 70(%r13)
	movl	$4095, %edx             # imm = 0xFFF
	andl	12(%r15), %edx
	movl	$-4096, %ecx            # imm = 0xF000
	andl	76(%r13), %ecx
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	12(%r15), %edx
	andl	$-4190209, %ecx         # imm = 0xFFC00FFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$12582912, %edx         # imm = 0xC00000
	andl	12(%r15), %edx
	andl	$-12582913, %ecx        # imm = 0xFF3FFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1056964608, %edx       # imm = 0x3F000000
	andl	12(%r15), %edx
	andl	$-1056964609, %ecx      # imm = 0xC0FFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	12(%r15), %edx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1073741824, %edx       # imm = 0x40000000
	andl	12(%r15), %edx
	andl	$-1073741825, %ecx      # imm = 0xBFFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movb	(%r15), %cl
	andb	$1, %cl
	andb	$-2, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$2, %cl
	andb	$-3, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$4, %cl
	andb	$-5, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$112, %cl
	andb	$-113, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	8(%r15), %eax
	movw	%ax, 72(%r13)
	movzwl	10(%r15), %eax
	movw	%ax, 74(%r13)
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_108:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_108
# BB#109:
	subq	$8, %rsp
.Lcfi103:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi104:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi105:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi106:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi107:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi108:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi109:
	.cfi_adjust_cfa_offset -48
	cmpq	$0, 8(%r14)
	jne	.LBB1_518
	jmp	.LBB1_515
.LBB1_110:
	movq	(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_111:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_111
# BB#112:
	subq	$8, %rsp
.Lcfi110:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi111:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi112:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi113:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi114:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi115:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi116:
	.cfi_adjust_cfa_offset -48
	movzwl	(%r15), %ecx
	andl	$128, %ecx
	movzwl	64(%r13), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$256, %ecx              # imm = 0x100
	movl	%eax, %edx
	andl	$-257, %edx             # imm = 0xFEFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %edx             # imm = 0xFDFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %edx            # imm = 0xE3FF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	2(%r15), %ecx
	movw	%cx, 66(%r13)
	movb	4(%r15), %cl
	andb	$3, %cl
	movb	68(%r13), %dl
	andb	$-4, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	4(%r15), %cl
	andb	$12, %cl
	andb	$-13, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	4(%r15), %cl
	andb	$112, %cl
	andb	$-113, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	(%r15), %cl
	andb	$8, %cl
	andb	$-9, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	4(%r15), %edx
	andl	$128, %edx
	movzwl	68(%r13), %ecx
	andl	$65407, %ecx            # imm = 0xFF7F
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$256, %edx              # imm = 0x100
	andl	$-257, %ecx             # imm = 0xFEFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$512, %edx              # imm = 0x200
	andl	$-513, %ecx             # imm = 0xFDFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$7168, %edx             # imm = 0x1C00
	andl	$-7169, %ecx            # imm = 0xE3FF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$57344, %edx            # imm = 0xE000
	andl	$8191, %ecx             # imm = 0x1FFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	6(%r15), %ecx
	movw	%cx, 70(%r13)
	movl	$4095, %edx             # imm = 0xFFF
	andl	12(%r15), %edx
	movl	$-4096, %ecx            # imm = 0xF000
	andl	76(%r13), %ecx
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	12(%r15), %edx
	andl	$-4190209, %ecx         # imm = 0xFFC00FFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$12582912, %edx         # imm = 0xC00000
	andl	12(%r15), %edx
	andl	$-12582913, %ecx        # imm = 0xFF3FFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1056964608, %edx       # imm = 0x3F000000
	andl	12(%r15), %edx
	andl	$-1056964609, %ecx      # imm = 0xC0FFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	12(%r15), %edx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1073741824, %edx       # imm = 0x40000000
	andl	12(%r15), %edx
	andl	$-1073741825, %ecx      # imm = 0xBFFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movb	(%r15), %cl
	andb	$1, %cl
	andb	$-2, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$2, %cl
	andb	$-3, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$4, %cl
	andb	$-5, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$112, %cl
	andb	$-113, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	8(%r15), %eax
	movw	%ax, 72(%r13)
	movzwl	10(%r15), %eax
	movw	%ax, 74(%r13)
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_113:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_113
# BB#114:
	subq	$8, %rsp
.Lcfi117:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi118:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi119:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi120:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi121:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi122:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi123:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB1_165
.LBB1_115:
	movl	$11, %edi
	movl	$.L.str.40, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB1_116:
	callq	MakeWord
	subq	$8, %rsp
.Lcfi124:
	.cfi_adjust_cfa_offset 8
	movq	%rax, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi125:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi126:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi127:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi128:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi129:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi130:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB1_394
.LBB1_117:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	movq	8(%rsp), %r12           # 8-byte Reload
	je	.LBB1_364
# BB#118:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_365
.LBB1_119:                              # %._crit_edge3142
	addq	$32, %rbp
	movzbl	(%rbx), %edi
	callq	Image
	movq	%rax, %rbx
	movl	$8, %edi
	movl	$26, %esi
	movl	$.L.str.18, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	movq	%rbx, %r9
	callq	Error
	movzwl	72(%rsp), %eax
	andl	$58367, %eax            # imm = 0xE3FF
	orl	$1024, %eax             # imm = 0x400
	movw	%ax, 72(%rsp)
	movw	$2880, 74(%rsp)         # imm = 0xB40
	movl	$2880, %eax             # imm = 0xB40
.LBB1_120:
	movl	$8388607, 64(%r13)      # imm = 0x7FFFFF
	movl	%eax, 68(%r13)
	movl	$8388607, 72(%r13)      # imm = 0x7FFFFF
.LBB1_79:
	movq	8(%r13), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_80
# BB#121:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_122
.LBB1_80:
	xorl	%ecx, %ecx
.LBB1_122:
	movq	784(%rsp), %r12
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_124
# BB#123:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_124:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_126
# BB#125:
	callq	DisposeObject
.LBB1_126:
	movb	32(%r13), %cl
	addb	$-24, %cl
	movl	$45034, %ebx            # imm = 0xAFEA
	shrl	%cl, %ebx
	andl	$1, %ebx
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_127:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_127
# BB#128:
	cmpb	$17, %cl
	sbbb	%al, %al
	xorl	%edx, %edx
	cmpb	$16, %cl
	seta	%dl
	testl	%ebx, %ebx
	sete	%cl
	orq	%rdx, %rbx
	movq	$0, 128(%rsp,%rbx,8)
	movq	$0, 208(%rsp,%rbx,8)
	andb	%al, %cl
	movzbl	%cl, %ebp
	movq	(%r14,%rbp,8), %rax
	movq	%rax, 208(%rsp,%rbp,8)
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	(%r15,%rbp,8), %rax
	movq	%rax, 128(%rsp,%rbp,8)
	subq	$8, %rsp
.Lcfi131:
	.cfi_adjust_cfa_offset 8
	leaq	216(%rsp), %rcx
	leaq	136(%rsp), %r8
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi132:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi133:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi134:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi135:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi136:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi137:
	.cfi_adjust_cfa_offset -48
	movq	(%r14,%rbx,8), %rax
	movq	%rax, 208(%rsp,%rbx,8)
	movq	(%r15,%rbx,8), %rax
	movq	%rax, 128(%rsp,%rbx,8)
	movq	$0, 128(%rsp,%rbp,8)
	movq	$0, 208(%rsp,%rbp,8)
	movq	216(%rsp), %rax
	orq	208(%rsp), %rax
	jne	.LBB1_130
# BB#129:
	movq	136(%rsp), %rax
	orq	128(%rsp), %rax
	je	.LBB1_665
.LBB1_130:
	leaq	208(%rsp), %rsi
	leaq	128(%rsp), %rdx
	movq	%r13, %rdi
	jmp	.LBB1_521
.LBB1_131:
	movb	$-2, %cl
	movb	$1, %dl
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB1_136
.LBB1_132:
	cmpb	$48, %bpl
	movb	$-3, %cl
	movq	8(%rsp), %rsi           # 8-byte Reload
	je	.LBB1_134
# BB#133:
	movb	$-5, %cl
.LBB1_134:
	movb	$2, %dl
	je	.LBB1_136
# BB#135:
	movb	$4, %dl
.LBB1_136:
	andb	%cl, %al
	orb	%dl, %al
	movb	%al, 40(%rsp)
.LBB1_137:
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_138:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_138
# BB#139:
	subq	$8, %rsp
.Lcfi138:
	.cfi_adjust_cfa_offset 8
	leaq	48(%rsp), %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi139:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi140:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi141:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi142:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi143:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi144:
	.cfi_adjust_cfa_offset -48
.LBB1_140:
	movq	%rax, %r12
	movq	8(%r13), %rax
	jmp	.LBB1_305
.LBB1_141:
	movl	$4095, %ecx             # imm = 0xFFF
	andl	12(%r15), %ecx
	movl	$-4096, %eax            # imm = 0xF000
	andl	40(%r13), %eax
	orl	%ecx, %eax
	movl	%eax, 40(%r13)
	movl	$4190208, %ecx          # imm = 0x3FF000
	andl	12(%r15), %ecx
	andl	$-4190209, %eax         # imm = 0xFFC00FFF
	orl	%ecx, %eax
	movl	%eax, 40(%r13)
	movl	$4194304, %ecx          # imm = 0x400000
	andl	12(%r15), %ecx
	andl	$-4194305, %eax         # imm = 0xFFBFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r13)
	movl	12(%r15), %ecx
	shrl	%ecx
	andl	$528482304, %ecx        # imm = 0x1F800000
	andl	$-528482305, %eax       # imm = 0xE07FFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r13)
	movb	4(%r15), %cl
	andb	$3, %cl
	xorl	%edx, %edx
	cmpb	$2, %cl
	sete	%dl
	shll	$31, %edx
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	orl	%edx, %eax
	movl	%eax, 40(%r13)
	movl	792(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB1_144
# BB#142:
	movb	(%r15), %cl
	andb	$8, %cl
	je	.LBB1_144
# BB#143:
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	MapSmallCaps
	movq	%rax, %r13
	movl	40(%r13), %eax
.LBB1_144:
	andl	$-1610612737, %eax      # imm = 0x9FFFFFFF
	orl	$536870912, %eax        # imm = 0x20000000
	movl	%eax, 40(%r13)
	cmpq	$0, 8(%r14)
	jne	.LBB1_518
	jmp	.LBB1_515
.LBB1_145:
	movq	80(%r13), %rbp
	movzwl	(%r15), %ecx
	andl	$128, %ecx
	movzwl	64(%r13), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$256, %ecx              # imm = 0x100
	movl	%eax, %edx
	andl	$-257, %edx             # imm = 0xFEFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %edx             # imm = 0xFDFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %edx            # imm = 0xE3FF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	2(%r15), %ecx
	movw	%cx, 66(%r13)
	movb	4(%r15), %cl
	andb	$3, %cl
	movb	68(%r13), %dl
	andb	$-4, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	4(%r15), %cl
	andb	$12, %cl
	andb	$-13, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	4(%r15), %cl
	andb	$112, %cl
	andb	$-113, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	(%r15), %cl
	andb	$8, %cl
	andb	$-9, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	4(%r15), %edx
	andl	$128, %edx
	movzwl	68(%r13), %ecx
	andl	$65407, %ecx            # imm = 0xFF7F
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$256, %edx              # imm = 0x100
	andl	$-257, %ecx             # imm = 0xFEFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$512, %edx              # imm = 0x200
	andl	$-513, %ecx             # imm = 0xFDFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$7168, %edx             # imm = 0x1C00
	andl	$-7169, %ecx            # imm = 0xE3FF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$57344, %edx            # imm = 0xE000
	andl	$8191, %ecx             # imm = 0x1FFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	6(%r15), %ecx
	movw	%cx, 70(%r13)
	movl	$4095, %edx             # imm = 0xFFF
	andl	12(%r15), %edx
	movl	$-4096, %ecx            # imm = 0xF000
	andl	76(%r13), %ecx
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	12(%r15), %edx
	andl	$-4190209, %ecx         # imm = 0xFFC00FFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$12582912, %edx         # imm = 0xC00000
	andl	12(%r15), %edx
	andl	$-12582913, %ecx        # imm = 0xFF3FFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1056964608, %edx       # imm = 0x3F000000
	andl	12(%r15), %edx
	andl	$-1056964609, %ecx      # imm = 0xC0FFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	12(%r15), %edx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1073741824, %edx       # imm = 0x40000000
	andl	12(%r15), %edx
	andl	$-1073741825, %ecx      # imm = 0xBFFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movb	(%r15), %cl
	andb	$1, %cl
	andb	$-2, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$2, %cl
	andb	$-3, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$4, %cl
	andb	$-5, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$112, %cl
	andb	$-113, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	8(%r15), %eax
	movw	%ax, 72(%r13)
	movzwl	10(%r15), %eax
	movw	%ax, 74(%r13)
	movq	808(%rsp), %rax
	cmpq	$0, (%rax)
	je	.LBB1_148
# BB#146:
	movq	80(%r13), %rax
	cmpq	GalleySym(%rip), %rax
	je	.LBB1_699
# BB#147:
	cmpq	ForceGalleySym(%rip), %rax
	je	.LBB1_699
.LBB1_148:                              # %.preheader2550
	movq	%r14, 88(%rsp)          # 8-byte Spill
	movq	8(%r13), %r14
	cmpq	%r13, %r14
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	je	.LBB1_336
# BB#149:                               # %.preheader.preheader
	movl	$1, %r15d
	jmp	.LBB1_150
.LBB1_161:
	movq	no_fpos(%rip), %rbx
	movl	%ebp, %edi
	callq	Image
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi145:
	.cfi_adjust_cfa_offset 8
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.46, %edx
	movl	$0, %ecx
	movl	$.L.str.47, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	pushq	%rbp
.Lcfi146:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi147:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB1_665
.LBB1_162:
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_163:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_163
# BB#164:
	subq	$8, %rsp
.Lcfi148:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi149:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi150:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi151:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi152:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi153:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi154:
	.cfi_adjust_cfa_offset -48
.LBB1_165:
	movl	$1, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	cmpq	$0, 8(%r14)
	jne	.LBB1_518
	jmp	.LBB1_515
.LBB1_166:
	movzwl	(%r15), %ecx
	andl	$128, %ecx
	movzwl	64(%r13), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$256, %ecx              # imm = 0x100
	movl	%eax, %edx
	andl	$-257, %edx             # imm = 0xFEFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %edx             # imm = 0xFDFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %edx            # imm = 0xE3FF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	2(%r15), %ecx
	movw	%cx, 66(%r13)
	movb	4(%r15), %cl
	andb	$3, %cl
	movb	68(%r13), %dl
	andb	$-4, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	4(%r15), %cl
	andb	$12, %cl
	andb	$-13, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	4(%r15), %cl
	andb	$112, %cl
	andb	$-113, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	(%r15), %cl
	andb	$8, %cl
	andb	$-9, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	4(%r15), %edx
	andl	$128, %edx
	movzwl	68(%r13), %ecx
	andl	$65407, %ecx            # imm = 0xFF7F
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$256, %edx              # imm = 0x100
	andl	$-257, %ecx             # imm = 0xFEFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$512, %edx              # imm = 0x200
	andl	$-513, %ecx             # imm = 0xFDFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$7168, %edx             # imm = 0x1C00
	andl	$-7169, %ecx            # imm = 0xE3FF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$57344, %edx            # imm = 0xE000
	andl	$8191, %ecx             # imm = 0x1FFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	6(%r15), %ecx
	movw	%cx, 70(%r13)
	movl	$4095, %edx             # imm = 0xFFF
	andl	12(%r15), %edx
	movl	$-4096, %ecx            # imm = 0xF000
	andl	76(%r13), %ecx
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	12(%r15), %edx
	andl	$-4190209, %ecx         # imm = 0xFFC00FFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$12582912, %edx         # imm = 0xC00000
	andl	12(%r15), %edx
	andl	$-12582913, %ecx        # imm = 0xFF3FFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1056964608, %edx       # imm = 0x3F000000
	andl	12(%r15), %edx
	andl	$-1056964609, %ecx      # imm = 0xC0FFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	12(%r15), %edx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1073741824, %edx       # imm = 0x40000000
	andl	12(%r15), %edx
	andl	$-1073741825, %ecx      # imm = 0xBFFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movb	(%r15), %cl
	andb	$1, %cl
	andb	$-2, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$2, %cl
	andb	$-3, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$4, %cl
	andb	$-5, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$112, %cl
	andb	$-113, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	8(%r15), %eax
	movw	%ax, 72(%r13)
	movzwl	10(%r15), %eax
	movw	%ax, 74(%r13)
	cmpq	$0, 8(%r14)
	jne	.LBB1_518
	jmp	.LBB1_515
.LBB1_167:
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_168:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_168
# BB#169:
	subq	$8, %rsp
.Lcfi155:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi156:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi157:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi158:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi159:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi160:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi161:
	.cfi_adjust_cfa_offset -48
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %rbx
	movb	32(%rbx), %al
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$1, %cl
	ja	.LBB1_380
# BB#170:
	cmpb	$0, 64(%rbx)
	jne	.LBB1_386
# BB#171:
	movl	$0, 72(%r13)
	movl	$0, 64(%r13)
	jmp	.LBB1_387
.LBB1_172:
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_173:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_173
# BB#174:
	subq	$8, %rsp
.Lcfi162:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi163:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi164:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi165:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi166:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi167:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi168:
	.cfi_adjust_cfa_offset -48
	movq	(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_175:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_175
# BB#176:
	subq	$8, %rsp
.Lcfi169:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi170:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi171:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi172:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi173:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi174:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi175:
	.cfi_adjust_cfa_offset -48
	cmpq	$0, 8(%r14)
	jne	.LBB1_518
	jmp	.LBB1_515
.LBB1_177:
	movq	%rbx, %r12
	movq	%r14, %r15
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_178:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_178
# BB#179:
	subq	$8, %rsp
.Lcfi176:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi177:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi178:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi179:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi180:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi181:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi182:
	.cfi_adjust_cfa_offset -48
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %rbp
	leaq	72(%rsp), %rdx
	leaq	100(%rsp), %rcx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	GetGap
	cmpl	$158, 100(%rsp)
	jne	.LBB1_351
# BB#180:
	movzwl	72(%rsp), %eax
	andl	$64512, %eax            # imm = 0xFC00
	cmpl	$12288, %eax            # imm = 0x3000
	jne	.LBB1_351
# BB#181:                               # %._crit_edge3089
	movswl	74(%rsp), %r14d
	jmp	.LBB1_352
.LBB1_182:
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_183:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_183
# BB#184:
	subq	$8, %rsp
.Lcfi183:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi184:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi185:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi186:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi187:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi188:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi189:
	.cfi_adjust_cfa_offset -48
	movq	(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_185:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_185
# BB#186:
	subq	$8, %rsp
.Lcfi190:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi191:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi192:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi193:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi194:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi195:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi196:
	.cfi_adjust_cfa_offset -48
	cmpq	$0, 8(%r14)
	jne	.LBB1_518
	jmp	.LBB1_515
.LBB1_187:
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_188:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_188
# BB#189:
	subq	$8, %rsp
.Lcfi197:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi198:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi199:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi200:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi201:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi202:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi203:
	.cfi_adjust_cfa_offset -48
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %r15
	movq	(%r13), %rbx
	.p2align	4, 0x90
.LBB1_190:                              # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_190
# BB#191:
	cmpb	$55, %al
	jne	.LBB1_946
# BB#192:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	movq	8(%rsp), %r12           # 8-byte Reload
	je	.LBB1_878
# BB#193:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_879
.LBB1_194:
	movl	$8, %edi
	movl	$29, %esi
	movl	$.L.str.21, %edx
	movl	$1, %ecx
	movl	$.L.str.22, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	jmp	.LBB1_665
.LBB1_195:
	movq	BackEnd(%rip), %rax
	movq	8(%rax), %rsi
	movl	$11, %edi
	movq	%rbx, %rdx
	callq	MakeWord
	movq	%rax, %rbx
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_418
# BB#196:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB1_419
# BB#197:
	testq	%rax, %rax
	je	.LBB1_419
# BB#198:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB1_419
.LBB1_199:
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_200:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_200
# BB#201:
	subq	$8, %rsp
.Lcfi204:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi205:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi206:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi207:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi208:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi209:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi210:
	.cfi_adjust_cfa_offset -48
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %rbp
	leaq	32(%rbp), %r8
	movb	32(%rbp), %al
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB1_390
# BB#202:
	movl	$-4096, %eax            # imm = 0xF000
	andl	40(%rbp), %eax
	movl	12(%r15), %ecx
	movl	$4095, %edi             # imm = 0xFFF
	movl	$4095, %edx             # imm = 0xFFF
	andl	%ecx, %edx
	orl	%eax, %edx
	andl	%ecx, %edi
	movl	%edx, 40(%rbp)
	je	.LBB1_886
# BB#203:
	movq	%rbp, %rbx
	addq	$64, %rbx
	movq	%r8, %rsi
	movq	%r8, %r15
	callq	FontMapping
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	MapCharEncoding
	movl	%eax, %r12d
	testb	%r12b, %r12b
	je	.LBB1_1047
# BB#204:
	movl	$12, %edi
	movl	$.L.str.3, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, %rbx
	movb	%r12b, 64(%rbx)
	jmp	.LBB1_1049
.LBB1_205:
	movq	8(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_207
# BB#206:
	cmpq	%r13, 8(%rax)
	je	.LBB1_208
.LBB1_207:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.32, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_208:
	movb	$17, 32(%r13)
	movb	(%r15), %al
	shrb	$2, %al
	movzwl	42(%r13), %ecx
	andb	$1, %al
	movzbl	%al, %eax
	shll	$11, %eax
	andl	$63487, %ecx            # imm = 0xF7FF
	orl	%eax, %ecx
	movw	%cx, 42(%r13)
	andb	$-5, (%r15)
	movzwl	(%r15), %ecx
	andl	$128, %ecx
	movzwl	64(%r13), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$256, %ecx              # imm = 0x100
	movl	%eax, %edx
	andl	$-257, %edx             # imm = 0xFEFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %edx             # imm = 0xFDFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %edx            # imm = 0xE3FF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	2(%r15), %ecx
	movw	%cx, 66(%r13)
	movb	4(%r15), %cl
	andb	$3, %cl
	movb	68(%r13), %dl
	andb	$-4, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	4(%r15), %cl
	andb	$12, %cl
	andb	$-13, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	4(%r15), %cl
	andb	$112, %cl
	andb	$-113, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	(%r15), %cl
	andb	$8, %cl
	andb	$-9, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	4(%r15), %edx
	andl	$128, %edx
	movzwl	68(%r13), %ecx
	andl	$65407, %ecx            # imm = 0xFF7F
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$256, %edx              # imm = 0x100
	andl	$-257, %ecx             # imm = 0xFEFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$512, %edx              # imm = 0x200
	andl	$-513, %ecx             # imm = 0xFDFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$7168, %edx             # imm = 0x1C00
	andl	$-7169, %ecx            # imm = 0xE3FF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$57344, %edx            # imm = 0xE000
	andl	$8191, %ecx             # imm = 0x1FFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	6(%r15), %ecx
	movw	%cx, 70(%r13)
	movl	$4095, %edx             # imm = 0xFFF
	andl	12(%r15), %edx
	movl	$-4096, %ecx            # imm = 0xF000
	andl	76(%r13), %ecx
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	12(%r15), %edx
	andl	$-4190209, %ecx         # imm = 0xFFC00FFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$12582912, %edx         # imm = 0xC00000
	andl	12(%r15), %edx
	andl	$-12582913, %ecx        # imm = 0xFF3FFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1056964608, %edx       # imm = 0x3F000000
	andl	12(%r15), %edx
	andl	$-1056964609, %ecx      # imm = 0xC0FFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	12(%r15), %edx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1073741824, %edx       # imm = 0x40000000
	andl	12(%r15), %edx
	andl	$-1073741825, %ecx      # imm = 0xBFFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movb	(%r15), %cl
	andb	$1, %cl
	andb	$-2, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$2, %cl
	andb	$-3, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$4, %cl
	andb	$-5, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$112, %cl
	andb	$-113, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	8(%r15), %eax
	movw	%ax, 72(%r13)
	movzwl	10(%r15), %eax
	movw	%ax, 74(%r13)
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_209:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_209
# BB#210:
	subq	$8, %rsp
.Lcfi211:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi212:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi213:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi214:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi215:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi216:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi217:
	.cfi_adjust_cfa_offset -48
	movq	%r13, %rdi
	callq	SetUnderline
	cmpq	$0, 8(%r14)
	jne	.LBB1_518
	jmp	.LBB1_515
.LBB1_211:
	movzbl	15(%r15), %edi
	andl	$63, %edi
	je	.LBB1_420
# BB#212:
	callq	LanguageString
	movl	$11, %edi
	movq	%rax, %rsi
	jmp	.LBB1_421
.LBB1_213:
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_214:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_214
# BB#215:
	subq	$8, %rsp
.Lcfi218:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi219:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi220:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi221:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi222:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi223:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi224:
	.cfi_adjust_cfa_offset -48
	movq	%rax, 64(%rsp)
	movq	8(%r13), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_217
# BB#216:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_217:
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_219
# BB#218:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_219:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_220:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_220
# BB#221:
	subq	$8, %rsp
.Lcfi225:
	.cfi_adjust_cfa_offset 8
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi226:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi227:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi228:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi229:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi230:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi231:
	.cfi_adjust_cfa_offset -48
	movq	(%r13), %rcx
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB1_223
# BB#222:
	movq	%rdx, zz_res(%rip)
	movq	16(%rcx), %rsi
	movq	%rsi, 16(%rdx)
	movq	16(%rcx), %rsi
	movq	%rdx, 24(%rsi)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
.LBB1_223:
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB1_225
# BB#224:
	movq	%rdx, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rcx
.LBB1_225:
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rcx), %edx
	leaq	zz_lengths(%rdx), %rsi
                                        # kill: %DL<def> %DL<kill> %RDX<kill>
	addb	$-11, %dl
	leaq	33(%rcx), %rdi
	cmpb	$2, %dl
	cmovbq	%rdi, %rsi
	movzbl	(%rsi), %edx
	movl	%edx, zz_size(%rip)
	movq	zz_free(,%rdx,8), %rsi
	movq	%rsi, (%rcx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rdx,8)
	movq	%rax, xx_res(%rip)
	movq	%r13, xx_hold(%rip)
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rcx
	cmpq	%r13, %rcx
	je	.LBB1_425
# BB#226:
	movq	16(%r13), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%r13), %rdx
	movq	%rcx, 24(%rdx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB1_426
# BB#227:
	testq	%rax, %rax
	je	.LBB1_426
# BB#228:
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	jmp	.LBB1_426
.LBB1_229:
	movq	8(%r13), %rbp
	.p2align	4, 0x90
.LBB1_230:                              # =>This Inner Loop Header: Depth=1
	addq	$16, %rbp
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB1_230
# BB#231:
	cmpb	$17, %al
	jne	.LBB1_391
# BB#232:                               # %.preheader2603
	movq	%r14, %r15
	movq	8(%rbp), %rbx
	movq	24(%rsp), %r12          # 8-byte Reload
.LBB1_233:                              # %.preheader2601
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_234 Depth 2
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB1_234:                              #   Parent Loop BB1_233 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB1_234
# BB#235:                               #   in Loop: Header=BB1_233 Depth=1
	cmpb	$1, %cl
	jne	.LBB1_237
# BB#236:                               # %.loopexit2602
                                        #   in Loop: Header=BB1_233 Depth=1
	movq	%rax, 64(%rsp)
	jmp	.LBB1_238
.LBB1_237:                              #   in Loop: Header=BB1_233 Depth=1
	movq	(%r12), %r14
	subq	$8, %rsp
.Lcfi232:
	.cfi_adjust_cfa_offset 8
	movq	%rax, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	%r12, %r9
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi233:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi234:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi235:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi236:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi237:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi238:
	.cfi_adjust_cfa_offset -48
	movq	%rax, 64(%rsp)
	cmpq	%r14, (%r12)
	jne	.LBB1_239
.LBB1_238:                              # %.backedge2604
                                        #   in Loop: Header=BB1_233 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%rbp, %rbx
	jne	.LBB1_233
.LBB1_239:                              # %._crit_edge2886
	movq	24(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_241
# BB#240:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_241:
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_243
# BB#242:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_243:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_887
# BB#244:
	movq	%rax, zz_res(%rip)
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	jmp	.LBB1_888
.LBB1_245:
	movq	8(%r13), %rax
	cmpq	%r13, %rax
	jne	.LBB1_247
# BB#246:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.38, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%r13), %rax
.LBB1_247:
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_248:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_248
# BB#249:
	subq	$8, %rsp
.Lcfi239:
	.cfi_adjust_cfa_offset 8
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi240:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi241:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi242:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi243:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi244:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi245:
	.cfi_adjust_cfa_offset -48
	movl	$0, 180(%rsp)
	leaq	180(%rsp), %rdx
	movl	$1, %esi
	movq	%rax, %rdi
	callq	Next
	jmp	.LBB1_140
.LBB1_250:
	movq	8(%r13), %rax
	leaq	16(%rax), %rcx
	.p2align	4, 0x90
.LBB1_251:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rbx
	leaq	16(%rbx), %rcx
	cmpb	$0, 32(%rbx)
	je	.LBB1_251
# BB#252:
	movq	8(%rax), %rbp
	.p2align	4, 0x90
.LBB1_253:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB1_253
# BB#254:
	cmpb	$82, %al
	jne	.LBB1_392
# BB#255:                               # %.loopexit
	movq	%rbp, 120(%rsp)
	jmp	.LBB1_393
.LBB1_256:
	movq	8(%r13), %rcx
	leaq	16(%rcx), %rdx
	.p2align	4, 0x90
.LBB1_257:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rbx
	movzbl	32(%rbx), %eax
	leaq	16(%rbx), %rdx
	testb	%al, %al
	je	.LBB1_257
# BB#258:
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB1_260
# BB#259:
	movq	%rdx, zz_res(%rip)
	movq	16(%rcx), %rsi
	movq	%rsi, 16(%rdx)
	movq	16(%rcx), %rsi
	movq	%rdx, 24(%rsi)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
.LBB1_260:
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB1_262
# BB#261:
	movq	%rdx, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rcx
.LBB1_262:
	movq	%rcx, %rbp
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rbp), %esi
	leaq	zz_lengths(%rsi), %rdi
	movl	%esi, %edx
	leaq	32(%rbx), %r8
	addb	$-11, %dl
	addq	$33, %rcx
	cmpb	$2, %dl
	cmovbq	%rcx, %rdi
	movzbl	(%rdi), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rbp)
	movq	zz_hold(%rip), %rdx
	movq	%rdx, zz_free(,%rcx,8)
	movq	(%r13), %rcx
	addq	$16, %rcx
	.p2align	4, 0x90
.LBB1_263:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rbp
	leaq	16(%rbp), %rcx
	cmpb	$0, 32(%rbp)
	je	.LBB1_263
# BB#264:
	movl	%eax, %ecx
	addb	$-6, %cl
	cmpb	$2, %cl
	jae	.LBB1_398
# BB#265:
	movq	8(%rbx), %rax
	.p2align	4, 0x90
.LBB1_266:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB1_266
# BB#267:
	cmpb	$2, %cl
	jne	.LBB1_726
# BB#268:
	movq	%rax, 64(%rsp)
	leaq	120(%rsp), %r8
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	784(%rsp), %rcx
	callq	CrossExpand
	movq	%rax, %rbx
	movq	120(%rsp), %rdi
	movq	%rbx, %rsi
	callq	AttachEnv
	movq	%rbx, %rdi
	movq	%r12, %rsi
	jmp	.LBB1_400
.LBB1_269:
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	8(%r13), %r12
	cmpq	%r13, %r12
	je	.LBB1_272
# BB#270:
	movq	8(%r12), %rax
	cmpq	%r13, %rax
	je	.LBB1_272
# BB#271:
	cmpq	%r13, 8(%rax)
	je	.LBB1_273
.LBB1_272:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.76, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%r13), %r12
	.p2align	4, 0x90
.LBB1_273:                              # %._crit_edge3135
                                        # =>This Inner Loop Header: Depth=1
	addq	$16, %r12
	movq	(%r12), %r12
	movzbl	32(%r12), %eax
	testb	%al, %al
	je	.LBB1_273
# BB#274:
	addb	$-6, %al
	cmpb	$2, %al
	jae	.LBB1_694
.LBB1_275:                              # %.loopexit2605
	movq	8(%r12), %rax
	leaq	16(%rax), %rbp
	jmp	.LBB1_277
	.p2align	4, 0x90
.LBB1_276:                              #   in Loop: Header=BB1_277 Depth=1
	addq	$16, %rbp
.LBB1_277:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB1_276
# BB#278:
	cmpb	$2, %cl
	jne	.LBB1_408
# BB#279:
	movq	80(%rbp), %rdi
	movzwl	41(%rdi), %ecx
	testb	$2, %cl
	jne	.LBB1_856
# BB#280:
	addq	$32, %rbp
	callq	SymName
	movq	%rax, %rbx
	subq	$8, %rsp
.Lcfi246:
	.cfi_adjust_cfa_offset 8
	movl	$8, %edi
	movl	$15, %esi
	movl	$.L.str.80, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	movq	%rbx, %r9
	pushq	$.L.str.81
.Lcfi247:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi248:
	.cfi_adjust_cfa_offset -16
	movl	$11, %edi
	movl	$.L.str.8, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, %rbx
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_1107
# BB#281:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	movq	784(%rsp), %rbp
	movq	%rbp, %r12
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB1_1108
# BB#282:
	testq	%rax, %rax
	je	.LBB1_1108
# BB#283:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB1_1108
.LBB1_284:
	movswl	10(%r15), %eax
.LBB1_285:
	imull	$26215, %eax, %edx      # imm = 0x6667
	movl	%edx, %eax
	shrl	$31, %eax
	sarl	$19, %edx
	addl	%eax, %edx
	leaq	208(%rsp), %rdi
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	callq	sprintf
	leaq	208(%rsp), %rsi
	movl	$11, %edi
	movq	%rbx, %rdx
	callq	MakeWord
	movq	%rax, %rbx
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB1_354
# BB#286:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_355
# BB#287:
	testq	%rax, %rax
	je	.LBB1_355
# BB#288:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB1_355
.LBB1_289:
	leaq	40(%rsp), %rdi
	movq	%rax, %rsi
	callq	SpaceChange
	jmp	.LBB1_295
.LBB1_290:
	leaq	40(%rsp), %rdi
	movq	%rax, %rsi
	callq	YUnitChange
	jmp	.LBB1_295
.LBB1_291:
	leaq	40(%rsp), %rdi
	movq	%rax, %rsi
	callq	ZUnitChange
	jmp	.LBB1_295
.LBB1_292:
	leaq	40(%rsp), %rdi
	movq	%rax, %rsi
	callq	BreakChange
	jmp	.LBB1_295
.LBB1_293:
	leaq	40(%rsp), %rdi
	movq	%rax, %rsi
	callq	ColourChange
	jmp	.LBB1_295
.LBB1_294:
	leaq	40(%rsp), %rdi
	movq	%rax, %rsi
	callq	LanguageChange
.LBB1_295:
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	movq	88(%rsp), %rbp          # 8-byte Reload
	je	.LBB1_297
# BB#296:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_298
.LBB1_297:
	xorl	%ecx, %ecx
.LBB1_298:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_300
# BB#299:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_300:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_302
# BB#301:
	callq	DisposeObject
.LBB1_302:
	movq	(%rbx), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_303:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_303
# BB#304:
	subq	$8, %rsp
.Lcfi249:
	.cfi_adjust_cfa_offset 8
	leaq	48(%rsp), %rdx
	movq	%r12, %rsi
	movq	%rbp, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi250:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi251:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi252:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi253:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi254:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi255:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	movq	(%rbx), %rax
.LBB1_305:
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_307
# BB#306:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_307:
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_309
# BB#308:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_309:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	%r12, xx_res(%rip)
	movq	%r13, xx_hold(%rip)
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_313
# BB#310:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_314
# BB#311:
	testq	%r12, %r12
	je	.LBB1_314
# BB#312:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r12), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%r12)
	movq	%r12, 24(%rcx)
	jmp	.LBB1_314
.LBB1_313:                              # %.thread3240
	movq	$0, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_314:
	movq	%r13, zz_hold(%rip)
	movq	8(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_318
# BB#315:
	movq	%rax, zz_res(%rip)
	movq	(%r13), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	zz_res(%rip), %rax
	movq	xx_res(%rip), %rcx
	movq	%rax, xx_tmp(%rip)
	movq	%rcx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_319
# BB#316:
	testq	%rcx, %rcx
	je	.LBB1_319
# BB#317:
	movq	(%rax), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	jmp	.LBB1_319
.LBB1_318:                              # %.thread3242
	movq	$0, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_319:
	movq	xx_hold(%rip), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	jmp	.LBB1_1126
.LBB1_320:                              #   in Loop: Header=BB1_150 Depth=1
	cmpb	$2, %al
	je	.LBB1_323
# BB#321:                               #   in Loop: Header=BB1_150 Depth=1
	cmpb	$78, %al
	movq	784(%rsp), %r12
	jne	.LBB1_329
# BB#322:                               #   in Loop: Header=BB1_150 Depth=1
	subq	$8, %rsp
.Lcfi256:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi257:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi258:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi259:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi260:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi261:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi262:
	.cfi_adjust_cfa_offset -48
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %r13
	jmp	.LBB1_334
.LBB1_323:                              #   in Loop: Header=BB1_150 Depth=1
	movq	80(%rbx), %rax
	movb	32(%rax), %al
	addb	$112, %al
	cmpb	$2, %al
	movq	784(%rsp), %r12
	ja	.LBB1_329
# BB#324:                               #   in Loop: Header=BB1_150 Depth=1
	movq	%rbx, %rdi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rsi
	callq	ParameterCheck
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB1_329
# BB#325:                               #   in Loop: Header=BB1_150 Depth=1
	movq	%rbx, zz_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB1_330
# BB#326:                               #   in Loop: Header=BB1_150 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rbx), %rcx
	movq	%rax, 24(%rcx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rax, xx_tmp(%rip)
	movq	%r13, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_331
# BB#327:                               #   in Loop: Header=BB1_150 Depth=1
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%r13), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r13), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%r13)
	movq	%r13, 24(%rcx)
	jmp	.LBB1_331
.LBB1_329:                              #   in Loop: Header=BB1_150 Depth=1
	movq	%rbx, %r13
	jmp	.LBB1_334
.LBB1_330:                              # %.thread3219
                                        #   in Loop: Header=BB1_150 Depth=1
	movq	$0, xx_tmp(%rip)
	movq	%r13, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_331:                              #   in Loop: Header=BB1_150 Depth=1
	movq	%rbx, %rdi
	callq	DisposeObject
	jmp	.LBB1_334
.LBB1_150:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_151 Depth 2
                                        #     Child Loop BB1_155 Depth 2
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB1_151:                              #   Parent Loop BB1_150 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB1_151
# BB#152:                               #   in Loop: Header=BB1_150 Depth=1
	cmpb	$10, %al
	je	.LBB1_154
# BB#153:                               #   in Loop: Header=BB1_150 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.48, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_154:                              # %.loopexit2549
                                        #   in Loop: Header=BB1_150 Depth=1
	leaq	32(%rbp), %r12
	movq	8(%rbp), %rbx
	.p2align	4, 0x90
.LBB1_155:                              #   Parent Loop BB1_150 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_155
# BB#156:                               #   in Loop: Header=BB1_150 Depth=1
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jb	.LBB1_332
# BB#157:                               #   in Loop: Header=BB1_150 Depth=1
	movq	80(%rbp), %rcx
	movzwl	41(%rcx), %edx
	movzbl	43(%rcx), %ecx
	shll	$16, %ecx
	orl	%edx, %ecx
	testl	$524288, %ecx           # imm = 0x80000
	jne	.LBB1_332
# BB#158:                               #   in Loop: Header=BB1_150 Depth=1
	testw	$8193, %cx              # imm = 0x2001
	je	.LBB1_320
# BB#159:                               #   in Loop: Header=BB1_150 Depth=1
	subq	$8, %rsp
.Lcfi263:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi264:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi265:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi266:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi267:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi268:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi269:
	.cfi_adjust_cfa_offset -48
	movl	$1, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %r13
	movb	32(%r13), %al
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB1_333
# BB#160:                               #   in Loop: Header=BB1_150 Depth=1
	movq	80(%rbp), %rdi
	callq	SymName
	movq	%rax, %rbx
	movl	$8, %edi
	movl	$41, %esi
	movl	$.L.str.49, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r12, %r8
	movq	%rbx, %r9
	callq	Error
	jmp	.LBB1_333
.LBB1_332:                              # %.loopexit2548.loopexit
                                        #   in Loop: Header=BB1_150 Depth=1
	movq	%rbx, %r13
.LBB1_333:                              # %.loopexit2548
                                        #   in Loop: Header=BB1_150 Depth=1
	movq	784(%rsp), %r12
.LBB1_334:                              # %.loopexit2548
                                        #   in Loop: Header=BB1_150 Depth=1
	movb	32(%r13), %al
	addb	$-11, %al
	cmpb	$2, %al
	movl	$0, %eax
	cmovael	%eax, %r15d
	movq	8(%r14), %r14
	movq	112(%rsp), %r13         # 8-byte Reload
	cmpq	%r13, %r14
	jne	.LBB1_150
# BB#335:                               # %._crit_edge
	testl	%r15d, %r15d
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB1_337
.LBB1_336:                              # %._crit_edge.thread
	cmpq	$0, 96(%rbp)
	je	.LBB1_866
.LBB1_337:
	xorl	%ebx, %ebx
	movq	88(%rsp), %r14          # 8-byte Reload
.LBB1_338:
	movzwl	41(%rbp), %ecx
	movzbl	43(%rbp), %eax
	shll	$16, %eax
	orl	%ecx, %eax
	movl	800(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.LBB1_680
# BB#339:
	movl	%eax, %ecx
	andl	$64, %ecx
	je	.LBB1_680
# BB#340:
	movq	8(%rsp), %r15           # 8-byte Reload
	movzbl	zz_lengths+8(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r12
	testq	%r12, %r12
	je	.LBB1_1000
# BB#341:
	movq	%r12, zz_hold(%rip)
	movq	(%r12), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_1001
.LBB1_342:
	xorl	%eax, %eax
	jmp	.LBB1_404
.LBB1_343:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_344:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%rax, %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
	je	.LBB1_346
# BB#345:
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_346:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_349
# BB#347:
	testq	%rax, %rax
	je	.LBB1_349
# BB#348:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_349:
	subq	$8, %rsp
.Lcfi270:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi271:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi272:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi273:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi274:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi275:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi276:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB1_665
.LBB1_350:
	movl	$8, %edi
	movl	$38, %esi
	movl	$.L.str.29, %edx
	movl	$2, %ecx
	movl	$.L.str.28, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movl	$11, %edi
	movl	$.L.str.28, %esi
	jmp	.LBB1_358
.LBB1_351:                              # %._crit_edge3141
	addq	$32, %rbp
	movzbl	(%r12), %edi
	callq	Image
	movq	%rax, %rbx
	movl	$8, %edi
	movl	$28, %esi
	movl	$.L.str.20, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	movq	%rbx, %r9
	callq	Error
	movzwl	72(%rsp), %eax
	andl	$58367, %eax            # imm = 0xE3FF
	orl	$4096, %eax             # imm = 0x1000
	movw	%ax, 72(%rsp)
	movw	$0, 74(%rsp)
.LBB1_352:
	movl	%r14d, 76(%r13)
	movq	8(%r13), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	movq	8(%rsp), %r12           # 8-byte Reload
	je	.LBB1_430
# BB#353:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_431
.LBB1_354:                              # %.thread3263
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_355:
	movq	%r13, %rdi
	callq	DisposeObject
	subq	$8, %rsp
.Lcfi277:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi278:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi279:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi280:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi281:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi282:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi283:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	jmp	.LBB1_1126
.LBB1_356:
	callq	FontFace
.LBB1_357:
	movl	$11, %edi
	movq	%rax, %rsi
.LBB1_358:
	movq	%rbx, %rdx
	callq	MakeWord
	movq	%rax, %rbx
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB1_362
# BB#359:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_363
# BB#360:
	testq	%rax, %rax
	je	.LBB1_363
# BB#361:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB1_363
.LBB1_362:                              # %.thread3261
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_363:
	movq	%r13, %rdi
	callq	DisposeObject
	subq	$8, %rsp
.Lcfi284:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi285:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi286:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi287:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi288:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi289:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi290:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	jmp	.LBB1_1126
.LBB1_364:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_365:
	movb	$17, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movb	(%r15), %al
	shrb	$2, %al
	movzwl	42(%rbx), %ecx
	andb	$1, %al
	movzbl	%al, %eax
	shll	$11, %eax
	andl	$63487, %ecx            # imm = 0xF7FF
	orl	%eax, %ecx
	movw	%cx, 42(%rbx)
	andb	$-5, (%r15)
	movq	(%r14), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_367
# BB#366:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_367:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_370
# BB#368:
	testq	%rax, %rax
	je	.LBB1_370
# BB#369:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_370:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_372
# BB#371:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_373
.LBB1_372:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_373:
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB1_376
# BB#374:
	testq	%rax, %rax
	je	.LBB1_376
# BB#375:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_376:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_379
# BB#377:
	testq	%rax, %rax
	je	.LBB1_379
# BB#378:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_379:
	movq	24(%rsp), %r9           # 8-byte Reload
.LBB1_25:                               # %.loopexit2600
	subq	$8, %rsp
.Lcfi291:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi292:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi293:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi294:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi295:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi296:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi297:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r15
	movq	8(%r15), %rbx
	cmpq	%r15, %rbx
	je	.LBB1_449
# BB#26:                                # %.preheader2597.lr.ph.lr.ph
	leaq	8(%r15), %r12
	.p2align	4, 0x90
.LBB1_27:                               # %.preheader2597
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_28 Depth 2
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB1_28:                               #   Parent Loop BB1_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB1_28
# BB#29:                                #   in Loop: Header=BB1_27 Depth=1
	cmpb	$17, %al
	jne	.LBB1_442
# BB#30:                                #   in Loop: Header=BB1_27 Depth=1
	movq	%rbp, 64(%rsp)
	movq	8(%rbp), %r13
	cmpq	%rbp, %r13
	je	.LBB1_35
# BB#31:                                #   in Loop: Header=BB1_27 Depth=1
	cmpb	$0, 32(%r13)
	je	.LBB1_33
# BB#32:                                #   in Loop: Header=BB1_27 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_33:                               #   in Loop: Header=BB1_27 Depth=1
	movq	%r13, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r13), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%r13, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_35
# BB#34:                                #   in Loop: Header=BB1_27 Depth=1
	movq	(%rbx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r13), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_35:                               #   in Loop: Header=BB1_27 Depth=1
	movq	64(%rsp), %rax
	movq	24(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_443
# BB#36:                                #   in Loop: Header=BB1_27 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_444
	.p2align	4, 0x90
.LBB1_442:                              # %.outer2598
                                        #   in Loop: Header=BB1_27 Depth=1
	movq	%rbp, 64(%rsp)
	movq	%rbx, %r12
	movq	8(%rbx), %rbx
	addq	$8, %r12
	cmpq	%r15, %rbx
	jne	.LBB1_27
	jmp	.LBB1_449
.LBB1_443:                              #   in Loop: Header=BB1_27 Depth=1
	xorl	%ecx, %ecx
.LBB1_444:                              #   in Loop: Header=BB1_27 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_446
# BB#445:                               #   in Loop: Header=BB1_27 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_446:                              #   in Loop: Header=BB1_27 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_448
# BB#447:                               #   in Loop: Header=BB1_27 Depth=1
	callq	DisposeObject
.LBB1_448:                              # %.backedge2599
                                        #   in Loop: Header=BB1_27 Depth=1
	movq	(%r12), %rbx
	cmpq	%r15, %rbx
	jne	.LBB1_27
.LBB1_449:                              # %.outer2598._crit_edge
	movq	(%r14), %rax
	movq	8(%rax), %rbx
	.p2align	4, 0x90
.LBB1_450:                              # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_450
# BB#451:
	cmpb	$17, %al
	movq	784(%rsp), %r12
	jne	.LBB1_453
# BB#452:                               # %.loopexit2596.loopexit
	movq	%rbx, %rbp
	movl	792(%rsp), %r14d
	jmp	.LBB1_470
.LBB1_453:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_455
# BB#454:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_456
.LBB1_455:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_456:
	movb	$17, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	56(%rsp), %rdx          # 8-byte Reload
	movb	(%rdx), %al
	shrb	$2, %al
	movzwl	42(%rbp), %ecx
	andb	$1, %al
	movzbl	%al, %eax
	shll	$11, %eax
	andl	$63487, %ecx            # imm = 0xF7FF
	orl	%eax, %ecx
	movw	%cx, 42(%rbp)
	andb	$-5, (%rdx)
	movq	(%r14), %rax
	movq	8(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_458
# BB#457:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_458:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	movl	792(%rsp), %r14d
	je	.LBB1_461
# BB#459:
	testq	%rax, %rax
	je	.LBB1_461
# BB#460:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_461:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_463
# BB#462:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_464
.LBB1_463:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_464:
	testq	%rbp, %rbp
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB1_467
# BB#465:
	testq	%rax, %rax
	je	.LBB1_467
# BB#466:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_467:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_470
# BB#468:
	testq	%rax, %rax
	je	.LBB1_470
# BB#469:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_470:                              # %.loopexit2596
	movq	24(%rsp), %r9           # 8-byte Reload
	subq	$8, %rsp
.Lcfi298:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movq	%rbp, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi299:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi300:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi301:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi302:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi303:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi304:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r13
	movq	8(%r13), %rbx
	cmpq	%r13, %rbx
	movq	32(%rsp), %r8           # 8-byte Reload
	je	.LBB1_489
# BB#471:                               # %.preheader2593.lr.ph.lr.ph
	leaq	8(%r13), %r14
	.p2align	4, 0x90
.LBB1_472:                              # %.preheader2593
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_473 Depth 2
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB1_473:                              #   Parent Loop BB1_472 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB1_473
# BB#474:                               #   in Loop: Header=BB1_472 Depth=1
	cmpb	$17, %al
	jne	.LBB1_482
# BB#475:                               #   in Loop: Header=BB1_472 Depth=1
	movq	%rbp, 64(%rsp)
	movq	8(%rbp), %r12
	cmpq	%rbp, %r12
	je	.LBB1_480
# BB#476:                               #   in Loop: Header=BB1_472 Depth=1
	cmpb	$0, 32(%r12)
	je	.LBB1_478
# BB#477:                               #   in Loop: Header=BB1_472 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	32(%rsp), %r8           # 8-byte Reload
.LBB1_478:                              #   in Loop: Header=BB1_472 Depth=1
	movq	%r12, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r12), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%r12, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_480
# BB#479:                               #   in Loop: Header=BB1_472 Depth=1
	movq	(%rbx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r12), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_480:                              #   in Loop: Header=BB1_472 Depth=1
	movq	64(%rsp), %rax
	movq	24(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_483
# BB#481:                               #   in Loop: Header=BB1_472 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_484
	.p2align	4, 0x90
.LBB1_482:                              # %.outer2594
                                        #   in Loop: Header=BB1_472 Depth=1
	movq	%rbp, 64(%rsp)
	movq	%rbx, %r14
	movq	8(%rbx), %rbx
	addq	$8, %r14
	cmpq	%r13, %rbx
	jne	.LBB1_472
	jmp	.LBB1_489
.LBB1_483:                              #   in Loop: Header=BB1_472 Depth=1
	xorl	%ecx, %ecx
.LBB1_484:                              #   in Loop: Header=BB1_472 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_486
# BB#485:                               #   in Loop: Header=BB1_472 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_486:                              #   in Loop: Header=BB1_472 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_488
# BB#487:                               #   in Loop: Header=BB1_472 Depth=1
	callq	DisposeObject
	movq	32(%rsp), %r8           # 8-byte Reload
.LBB1_488:                              # %.backedge2595
                                        #   in Loop: Header=BB1_472 Depth=1
	movq	(%r14), %rbx
	cmpq	%r13, %rbx
	jne	.LBB1_472
.LBB1_489:                              # %.outer2594._crit_edge
	movb	(%r8), %r12b
	cmpb	$75, %r12b
	jne	.LBB1_491
# BB#490:
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	Meld
	movq	%rax, %r12
	jmp	.LBB1_1037
.LBB1_491:
	movq	8(%r15), %rbp
	movq	8(%r13), %rdx
	cmpq	%r13, %rdx
	sete	%cl
	cmpq	%r15, %rbp
	sete	%al
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	je	.LBB1_671
# BB#492:
	cmpq	%r13, %rdx
	movq	%rdx, %r14
	je	.LBB1_672
# BB#493:                               # %.lr.ph2860.preheader
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB1_494:                              # %.lr.ph2860
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_495 Depth 2
                                        #     Child Loop BB1_497 Depth 2
	leaq	16(%rbp), %rcx
	.p2align	4, 0x90
.LBB1_495:                              #   Parent Loop BB1_494 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdi
	movzbl	32(%rdi), %eax
	leaq	16(%rdi), %rcx
	testb	%al, %al
	je	.LBB1_495
# BB#496:                               #   in Loop: Header=BB1_494 Depth=1
	leaq	16(%r14), %rdx
	.p2align	4, 0x90
.LBB1_497:                              #   Parent Loop BB1_494 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rsi
	movzbl	32(%rsi), %ebx
	leaq	16(%rsi), %rdx
	testb	%bl, %bl
	je	.LBB1_497
# BB#498:                               #   in Loop: Header=BB1_494 Depth=1
	movl	%eax, %edx
	addb	$-11, %dl
	movl	%ebx, %ecx
	addb	$-11, %cl
	orb	%dl, %cl
	cmpb	$1, %cl
	ja	.LBB1_503
# BB#499:                               #   in Loop: Header=BB1_494 Depth=1
	addq	$64, %rdi
	addq	$64, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_504
	jmp	.LBB1_500
.LBB1_503:                              #   in Loop: Header=BB1_494 Depth=1
	cmpb	%bl, %al
	jne	.LBB1_500
.LBB1_504:                              #   in Loop: Header=BB1_494 Depth=1
	movq	8(%rbp), %rbp
	movq	8(%r14), %r14
	cmpq	%r13, %r14
	sete	%cl
	cmpq	%r15, %rbp
	sete	%al
	je	.LBB1_672
# BB#505:                               #   in Loop: Header=BB1_494 Depth=1
	cmpq	%r13, %r14
	jne	.LBB1_494
	jmp	.LBB1_672
.LBB1_380:                              # %thread-pre-split
	cmpb	$17, %al
	jne	.LBB1_386
# BB#381:
	movq	8(%rbx), %rax
	addq	$16, %rax
.LBB1_382:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_382
# BB#383:
	movq	%rdi, 64(%rsp)
	callq	GetScaleFactor
	mulss	.LCPI1_0(%rip), %xmm0
	cvttss2si	%xmm0, %eax
	movl	%eax, 64(%r13)
	movq	(%rbx), %rax
	addq	$16, %rax
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB1_384:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_384
# BB#385:
	movq	%rdi, 64(%rsp)
	callq	GetScaleFactor
	mulss	.LCPI1_0(%rip), %xmm0
	cvttss2si	%xmm0, %eax
	movl	%eax, 72(%r13)
	movq	784(%rsp), %rbx
	jmp	.LBB1_388
.LBB1_386:                              # %thread-pre-split.thread
	movq	%rbx, %rdi
	callq	GetScaleFactor
	mulss	.LCPI1_0(%rip), %xmm0
	cvttss2si	%xmm0, %eax
	movl	%eax, 72(%r13)
	movl	%eax, 64(%r13)
.LBB1_387:
	movq	784(%rsp), %rbx
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB1_388:
	movq	8(%r13), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_506
# BB#389:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_507
.LBB1_946:                              # %.loopexit2592.loopexit
	movq	%rbx, %rbp
	movq	8(%rsp), %r12           # 8-byte Reload
	cmpb	$17, 32(%rbp)
	je	.LBB1_903
	jmp	.LBB1_947
.LBB1_390:
	movl	$8, %edi
	movl	$30, %esi
	movl	$.L.str.23, %edx
	movl	$2, %ecx
	movl	$.L.str.24, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	$11, %edi
	movl	$.L.str.8, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, %rbx
	jmp	.LBB1_1050
.LBB1_391:
	movl	$8, %edi
	movl	$39, %esi
	movl	$.L.str.36, %edx
	movl	$2, %ecx
	movl	$.L.str.37, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	subq	$8, %rsp
.Lcfi305:
	.cfi_adjust_cfa_offset 8
	movq	%rbp, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi306:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi307:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi308:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi309:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi310:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi311:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB1_140
.LBB1_392:
	movq	%rbp, 120(%rsp)
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_393:
	movq	24(%rsp), %r9           # 8-byte Reload
	subq	$8, %rsp
.Lcfi312:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi313:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi314:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi315:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi316:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi317:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi318:
	.cfi_adjust_cfa_offset -48
.LBB1_394:
	movq	%rax, %r12
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_397
# BB#395:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB1_891
# BB#396:
	testq	%rax, %rax
	jne	.LBB1_890
	jmp	.LBB1_891
.LBB1_397:                              # %.thread3217
	movq	$0, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	jmp	.LBB1_891
.LBB1_398:
	cmpb	$2, %al
	jne	.LBB1_727
# BB#399:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	AttachEnv
	movzwl	(%r15), %ecx
	andl	$128, %ecx
	movzwl	64(%rbx), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 64(%rbx)
	movzwl	(%r15), %ecx
	andl	$256, %ecx              # imm = 0x100
	movl	%eax, %edx
	andl	$-257, %edx             # imm = 0xFEFF
	orl	%ecx, %edx
	movw	%dx, 64(%rbx)
	movzwl	(%r15), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %edx             # imm = 0xFDFF
	orl	%ecx, %edx
	movw	%dx, 64(%rbx)
	movzwl	(%r15), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %edx            # imm = 0xE3FF
	orl	%ecx, %edx
	movw	%dx, 64(%rbx)
	movzwl	(%r15), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%ecx, %edx
	movw	%dx, 64(%rbx)
	movzwl	2(%r15), %ecx
	movw	%cx, 66(%rbx)
	movb	4(%r15), %cl
	andb	$3, %cl
	movb	68(%rbx), %dl
	andb	$-4, %dl
	orb	%cl, %dl
	movb	%dl, 68(%rbx)
	movb	4(%r15), %cl
	andb	$12, %cl
	andb	$-13, %dl
	orb	%cl, %dl
	movb	%dl, 68(%rbx)
	movb	4(%r15), %cl
	andb	$112, %cl
	andb	$-113, %dl
	orb	%cl, %dl
	movb	%dl, 68(%rbx)
	movb	(%r15), %cl
	andb	$8, %cl
	andb	$-9, %al
	orb	%cl, %al
	movb	%al, 64(%rbx)
	movzwl	4(%r15), %edx
	andl	$128, %edx
	movzwl	68(%rbx), %ecx
	andl	$65407, %ecx            # imm = 0xFF7F
	orl	%edx, %ecx
	movw	%cx, 68(%rbx)
	movzwl	4(%r15), %edx
	andl	$256, %edx              # imm = 0x100
	andl	$-257, %ecx             # imm = 0xFEFF
	orl	%edx, %ecx
	movw	%cx, 68(%rbx)
	movzwl	4(%r15), %edx
	andl	$512, %edx              # imm = 0x200
	andl	$-513, %ecx             # imm = 0xFDFF
	orl	%edx, %ecx
	movw	%cx, 68(%rbx)
	movzwl	4(%r15), %edx
	andl	$7168, %edx             # imm = 0x1C00
	andl	$-7169, %ecx            # imm = 0xE3FF
	orl	%edx, %ecx
	movw	%cx, 68(%rbx)
	movzwl	4(%r15), %edx
	andl	$57344, %edx            # imm = 0xE000
	andl	$8191, %ecx             # imm = 0x1FFF
	orl	%edx, %ecx
	movw	%cx, 68(%rbx)
	movzwl	6(%r15), %ecx
	movw	%cx, 70(%rbx)
	movl	$4095, %edx             # imm = 0xFFF
	andl	12(%r15), %edx
	movl	$-4096, %ecx            # imm = 0xF000
	andl	76(%rbx), %ecx
	orl	%edx, %ecx
	movl	%ecx, 76(%rbx)
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	12(%r15), %edx
	andl	$-4190209, %ecx         # imm = 0xFFC00FFF
	orl	%edx, %ecx
	movl	%ecx, 76(%rbx)
	movl	$12582912, %edx         # imm = 0xC00000
	andl	12(%r15), %edx
	andl	$-12582913, %ecx        # imm = 0xFF3FFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%rbx)
	movl	$1056964608, %edx       # imm = 0x3F000000
	andl	12(%r15), %edx
	andl	$-1056964609, %ecx      # imm = 0xC0FFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%rbx)
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	12(%r15), %edx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%rbx)
	movl	$1073741824, %edx       # imm = 0x40000000
	andl	12(%r15), %edx
	andl	$-1073741825, %ecx      # imm = 0xBFFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%rbx)
	movb	(%r15), %cl
	andb	$1, %cl
	andb	$-2, %al
	orb	%cl, %al
	movb	%al, 64(%rbx)
	movb	(%r15), %cl
	andb	$2, %cl
	andb	$-3, %al
	orb	%cl, %al
	movb	%al, 64(%rbx)
	movb	(%r15), %cl
	andb	$4, %cl
	andb	$-5, %al
	orb	%cl, %al
	movb	%al, 64(%rbx)
	movb	(%r15), %cl
	andb	$112, %cl
	andb	$-113, %al
	orb	%cl, %al
	movb	%al, 64(%rbx)
	movzwl	8(%r15), %eax
	movw	%ax, 72(%rbx)
	movzwl	10(%r15), %eax
	movw	%ax, 74(%rbx)
	xorl	%esi, %esi
	movq	%rbx, %rdi
.LBB1_400:
	callq	SetEnv
	movq	%rax, 120(%rsp)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_892
# BB#401:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_893
.LBB1_402:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_403:
	movb	$-117, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
.LBB1_404:
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	%rax, 208(%rsp,%rbp,8)
	testb	$2, 45(%r14)
	jne	.LBB1_406
# BB#405:
	xorl	%eax, %eax
	jmp	.LBB1_524
.LBB1_406:
	movzbl	zz_lengths+139(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_522
# BB#407:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_523
.LBB1_408:
	addq	$32, %r12
	movl	$8, %edi
	movl	$14, %esi
	movl	$.L.str.79, %edx
	movl	$2, %ecx
	movl	$.L.str.78, %r9d
	xorl	%eax, %eax
	movq	%r12, %r8
	callq	Error
	movl	$11, %edi
	movl	$.L.str.8, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, %rbx
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_973
# BB#409:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	movq	784(%rsp), %rbp
	movq	%rbp, %r12
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB1_974
# BB#410:
	testq	%rax, %rax
	je	.LBB1_974
# BB#411:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB1_974
.LBB1_500:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	cmpb	$74, %r12b
	jne	.LBB1_673
	jmp	.LBB1_501
.LBB1_412:
	movl	$11, %edi
	movl	$.L.str.8, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	movq	%rax, %rbx
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_978
# BB#413:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB1_979
# BB#414:
	testq	%rax, %rax
	je	.LBB1_979
# BB#415:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB1_979
.LBB1_416:                              # %.thread3307
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB1_417:
	movq	%r13, %rdi
	callq	DisposeObject
	subq	$8, %rsp
.Lcfi319:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi320:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi321:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi322:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi323:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi324:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi325:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	jmp	.LBB1_1127
.LBB1_418:                              # %.thread3255
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB1_419:
	movq	%r13, %rdi
	callq	DisposeObject
	subq	$8, %rsp
.Lcfi326:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi327:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi328:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi329:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi330:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi331:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi332:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	jmp	.LBB1_1126
.LBB1_420:
	movl	$8, %edi
	movl	$33, %esi
	movl	$.L.str.27, %edx
	movl	$2, %ecx
	movl	$.L.str.28, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movl	$11, %edi
	movl	$.L.str.28, %esi
.LBB1_421:
	movq	%rbx, %rdx
	callq	MakeWord
	movq	%rax, %rbx
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB1_666
# BB#422:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_667
# BB#423:
	testq	%rax, %rax
	je	.LBB1_667
# BB#424:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB1_667
.LBB1_425:                              # %.thread3275
	movq	$0, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_426:
	movq	%r13, zz_hold(%rip)
	movq	8(%r13), %rcx
	cmpq	%r13, %rcx
	je	.LBB1_668
# BB#427:
	movq	%rcx, zz_res(%rip)
	movq	(%r13), %rdx
	movq	%rdx, (%rcx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	xx_res(%rip), %rdx
	movq	%rcx, xx_tmp(%rip)
	movq	%rdx, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB1_669
# BB#428:
	testq	%rdx, %rdx
	je	.LBB1_669
# BB#429:
	movq	(%rcx), %rsi
	movq	%rsi, zz_tmp(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	zz_hold(%rip), %rcx
	movq	zz_res(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	zz_tmp(%rip), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_tmp(%rip), %rdx
	movq	%rcx, 8(%rdx)
	jmp	.LBB1_669
.LBB1_430:
	xorl	%ecx, %ecx
.LBB1_431:
	movq	%r15, %rbx
	movl	792(%rsp), %r14d
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	movq	56(%rsp), %rbp          # 8-byte Reload
	je	.LBB1_433
# BB#432:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_433:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_435
# BB#434:
	callq	DisposeObject
.LBB1_435:
	movq	8(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_436:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_436
# BB#437:
	subq	$8, %rsp
.Lcfi333:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi334:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi335:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi336:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi337:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi338:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi339:
	.cfi_adjust_cfa_offset -48
	cmpq	$0, 8(%rbx)
	jne	.LBB1_441
# BB#438:
	cmpq	$0, (%rbx)
	jne	.LBB1_441
# BB#439:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, 8(%rax)
	jne	.LBB1_441
# BB#440:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, (%rax)
	je	.LBB1_665
.LBB1_441:
	movq	%r13, %rdi
	movq	%rbx, %rsi
	jmp	.LBB1_520
.LBB1_671:
	movq	%rdx, %r14
.LBB1_672:                              # %.critedge
	cmpb	$74, %r12b
	je	.LBB1_501
.LBB1_673:                              # %.critedge
	cmpb	$73, %r12b
                                        # implicit-def: %R12
	jne	.LBB1_1037
# BB#674:
	testb	%cl, %cl
	movq	%r15, %r12
	cmovneq	%r13, %r12
	orb	%cl, %al
	jne	.LBB1_1037
# BB#675:
	cmpq	32(%rsp), %r14          # 8-byte Folded Reload
	je	.LBB1_502
# BB#676:
	movq	(%r14), %rbx
	cmpq	%r13, %rbx
	je	.LBB1_1036
# BB#677:
	cmpb	$0, 32(%rbx)
	je	.LBB1_679
# BB#678:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_679:
	movq	%rbx, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%r13)
	jmp	.LBB1_1032
.LBB1_501:
	testb	%cl, %cl
	je	.LBB1_729
.LBB1_502:
	addq	$32, %r13
	movl	$11, %edi
	movl	$.L.str.8, %esi
	movq	%r13, %rdx
	callq	MakeWord
	movq	%rax, %r12
	jmp	.LBB1_1037
.LBB1_506:
	xorl	%ecx, %ecx
.LBB1_507:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_509
# BB#508:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_509:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_511
# BB#510:
	callq	DisposeObject
.LBB1_511:
	movq	(%r13), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_512:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_512
# BB#513:
	subq	$8, %rsp
.Lcfi340:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi341:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi342:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi343:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi344:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi345:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi346:
	.cfi_adjust_cfa_offset -48
.LBB1_514:
	cmpq	$0, 8(%r14)
	jne	.LBB1_518
.LBB1_515:
	cmpq	$0, (%r14)
	jne	.LBB1_518
# BB#516:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, 8(%rax)
	jne	.LBB1_518
# BB#517:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, (%rax)
	je	.LBB1_665
.LBB1_518:
	movq	%r13, %rdi
.LBB1_519:                              # %ManifestTg.exit
	movq	%r14, %rsi
.LBB1_520:                              # %ManifestTg.exit
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB1_521:                              # %ManifestTg.exit
	callq	insert_split
	movq	%rax, %r12
	jmp	.LBB1_1126
.LBB1_522:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_523:
	movb	$-117, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
.LBB1_524:
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rax, 128(%rsp,%rbp,8)
	movl	$0, 104(%rsp)           # 4-byte Folded Spill
	cmpq	$0, 168(%rsp)           # 8-byte Folded Reload
	setne	%r15b
	subq	$8, %rsp
.Lcfi347:
	.cfi_adjust_cfa_offset 8
	leaq	48(%rsp), %rdx
	leaq	216(%rsp), %rcx
	leaq	136(%rsp), %r8
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi348:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi349:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi350:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	movq	%rax, %rbx
	pushq	%rbx
.Lcfi351:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi352:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi353:
	.cfi_adjust_cfa_offset -48
	testl	%ebx, %ebx
	je	.LBB1_533
# BB#525:
	cmpb	$19, 32(%r13)
	jne	.LBB1_533
# BB#526:
	movq	784(%rsp), %rax
	movq	(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB1_533
# BB#527:
	movq	8(%rbp), %rbx
	cmpq	%rbp, %rbx
	je	.LBB1_532
# BB#528:
	cmpb	$0, 32(%rbx)
	je	.LBB1_530
# BB#529:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_530:
	movq	%rbx, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%rbx, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB1_532
# BB#531:
	movq	(%r12), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_532:
	movq	784(%rsp), %rax
	movq	%rax, %rbx
	movq	(%rbx), %rdi
	callq	DisposeObject
	movq	$0, (%rbx)
.LBB1_533:                              # %.lr.ph2769
	movl	104(%rsp), %eax         # 4-byte Reload
	movb	%r15b, %al
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movq	784(%rsp), %r12
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	192(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_534:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_539 Depth 2
                                        #     Child Loop BB1_561 Depth 2
                                        #     Child Loop BB1_564 Depth 2
                                        #     Child Loop BB1_599 Depth 2
                                        #     Child Loop BB1_601 Depth 2
                                        #     Child Loop BB1_607 Depth 2
                                        #       Child Loop BB1_609 Depth 3
                                        #       Child Loop BB1_611 Depth 3
	cmpb	$1, 32(%r14)
	je	.LBB1_536
# BB#535:                               #   in Loop: Header=BB1_534 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.57, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_536:                              #   in Loop: Header=BB1_534 Depth=1
	movq	8(%r14), %rax
	cmpq	%r14, %rax
	jne	.LBB1_538
# BB#537:                               #   in Loop: Header=BB1_534 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.58, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%r14), %rax
.LBB1_538:                              #   in Loop: Header=BB1_534 Depth=1
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_539:                              #   Parent Loop BB1_534 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_539
# BB#540:                               #   in Loop: Header=BB1_534 Depth=1
	subq	$8, %rsp
.Lcfi354:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	48(%rsp), %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi355:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi356:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi357:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi358:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi359:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi360:
	.cfi_adjust_cfa_offset -48
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	leaq	44(%r14), %rdx
	movq	%rax, %rdi
	movq	%r15, %rsi
	leaq	120(%rsp), %rcx
	callq	GetGap
	cmpq	$0, 208(%rsp,%rbx,8)
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB1_549
# BB#541:                               #   in Loop: Header=BB1_534 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_543
# BB#542:                               #   in Loop: Header=BB1_534 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_544
.LBB1_543:                              #   in Loop: Header=BB1_534 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_544:                              #   in Loop: Header=BB1_534 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	208(%rsp,%rbx,8), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_547
# BB#545:                               #   in Loop: Header=BB1_534 Depth=1
	testq	%rcx, %rcx
	je	.LBB1_547
# BB#546:                               #   in Loop: Header=BB1_534 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_547:                              #   in Loop: Header=BB1_534 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_549
# BB#548:                               #   in Loop: Header=BB1_534 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_549:                              #   in Loop: Header=BB1_534 Depth=1
	cmpq	$0, 128(%rsp,%rbx,8)
	je	.LBB1_558
# BB#550:                               #   in Loop: Header=BB1_534 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_552
# BB#551:                               #   in Loop: Header=BB1_534 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_553
.LBB1_552:                              #   in Loop: Header=BB1_534 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_553:                              #   in Loop: Header=BB1_534 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	128(%rsp,%rbx,8), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_556
# BB#554:                               #   in Loop: Header=BB1_534 Depth=1
	testq	%rcx, %rcx
	je	.LBB1_556
# BB#555:                               #   in Loop: Header=BB1_534 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_556:                              #   in Loop: Header=BB1_534 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_558
# BB#557:                               #   in Loop: Header=BB1_534 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_558:                              #   in Loop: Header=BB1_534 Depth=1
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	8(%rax), %r15
	cmpq	%r13, %r15
	jne	.LBB1_560
# BB#559:                               #   in Loop: Header=BB1_534 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.59, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_560:                              # %.preheader2568.preheader
                                        #   in Loop: Header=BB1_534 Depth=1
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB1_561:                              # %.preheader2568
                                        #   Parent Loop BB1_534 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB1_561
# BB#562:                               #   in Loop: Header=BB1_534 Depth=1
	movq	8(%r15), %rax
	cmpq	%r13, %rax
	je	.LBB1_565
# BB#563:                               # %.preheader2566.preheader
                                        #   in Loop: Header=BB1_534 Depth=1
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB1_564:                              # %.preheader2566
                                        #   Parent Loop BB1_534 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r14), %r14
	cmpb	$0, 32(%r14)
	je	.LBB1_564
	jmp	.LBB1_566
	.p2align	4, 0x90
.LBB1_565:                              #   in Loop: Header=BB1_534 Depth=1
	xorl	%r14d, %r14d
.LBB1_566:                              # %.loopexit2567
                                        #   in Loop: Header=BB1_534 Depth=1
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	128(%rsp,%rbp,8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB1_569
# BB#567:                               #   in Loop: Header=BB1_534 Depth=1
	movzbl	zz_lengths+139(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_570
# BB#568:                               #   in Loop: Header=BB1_534 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_571
	.p2align	4, 0x90
.LBB1_569:                              #   in Loop: Header=BB1_534 Depth=1
	xorl	%eax, %eax
	jmp	.LBB1_572
.LBB1_570:                              #   in Loop: Header=BB1_534 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_571:                              #   in Loop: Header=BB1_534 Depth=1
	movb	$-117, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
.LBB1_572:                              #   in Loop: Header=BB1_534 Depth=1
	movq	%rax, 208(%rsp,%rbp,8)
	testq	%r14, %r14
	je	.LBB1_575
# BB#573:                               #   in Loop: Header=BB1_534 Depth=1
	testb	$2, 45(%r14)
	jne	.LBB1_576
	jmp	.LBB1_574
	.p2align	4, 0x90
.LBB1_575:                              #   in Loop: Header=BB1_534 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, (%rax,%rbp,8)
	je	.LBB1_574
.LBB1_576:                              #   in Loop: Header=BB1_534 Depth=1
	movzbl	zz_lengths+139(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_578
# BB#577:                               #   in Loop: Header=BB1_534 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_579
	.p2align	4, 0x90
.LBB1_574:                              #   in Loop: Header=BB1_534 Depth=1
	xorl	%eax, %eax
	jmp	.LBB1_580
.LBB1_578:                              #   in Loop: Header=BB1_534 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_579:                              #   in Loop: Header=BB1_534 Depth=1
	movb	$-117, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
.LBB1_580:                              #   in Loop: Header=BB1_534 Depth=1
	movq	%rax, 128(%rsp,%rbp,8)
	subq	$8, %rsp
.Lcfi361:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	48(%rsp), %rdx
	leaq	216(%rsp), %rcx
	leaq	136(%rsp), %r8
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi362:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi363:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi364:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	movq	%rax, %rbx
	pushq	%rbx
.Lcfi365:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi366:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi367:
	.cfi_adjust_cfa_offset -48
	testl	%ebx, %ebx
	je	.LBB1_589
# BB#581:                               #   in Loop: Header=BB1_534 Depth=1
	cmpb	$19, 32(%r13)
	jne	.LBB1_589
# BB#582:                               #   in Loop: Header=BB1_534 Depth=1
	movq	(%r12), %rbp
	testq	%rbp, %rbp
	je	.LBB1_589
# BB#583:                               #   in Loop: Header=BB1_534 Depth=1
	movq	8(%rbp), %rbx
	cmpq	%rbp, %rbx
	je	.LBB1_588
# BB#584:                               #   in Loop: Header=BB1_534 Depth=1
	cmpb	$0, 32(%rbx)
	je	.LBB1_586
# BB#585:                               #   in Loop: Header=BB1_534 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_586:                              #   in Loop: Header=BB1_534 Depth=1
	movq	%rbx, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%rbx, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_588
# BB#587:                               #   in Loop: Header=BB1_534 Depth=1
	movq	(%r15), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_588:                              #   in Loop: Header=BB1_534 Depth=1
	movq	(%r12), %rdi
	callq	DisposeObject
	movq	$0, (%r12)
.LBB1_589:                              #   in Loop: Header=BB1_534 Depth=1
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	208(%rsp,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB1_603
# BB#590:                               #   in Loop: Header=BB1_534 Depth=1
	cmpq	%rax, 8(%rax)
	jne	.LBB1_592
# BB#591:                               #   in Loop: Header=BB1_534 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.60, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_592:                              #   in Loop: Header=BB1_534 Depth=1
	movq	80(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB1_594
# BB#593:                               #   in Loop: Header=BB1_534 Depth=1
	cmpq	%rdi, 8(%rdi)
	jne	.LBB1_595
.LBB1_594:                              #   in Loop: Header=BB1_534 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.61, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	80(%rsp), %rdi          # 8-byte Reload
.LBB1_595:                              #   in Loop: Header=BB1_534 Depth=1
	movq	128(%rsp,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB1_604
# BB#596:                               #   in Loop: Header=BB1_534 Depth=1
	cmpq	%rax, 8(%rax)
	jne	.LBB1_598
# BB#597:                               #   in Loop: Header=BB1_534 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.62, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	80(%rsp), %rdi          # 8-byte Reload
.LBB1_598:                              #   in Loop: Header=BB1_534 Depth=1
	movq	208(%rsp,%rbx,8), %r15
	movq	(%r15), %rcx
	addq	$16, %rcx
	.p2align	4, 0x90
.LBB1_599:                              #   Parent Loop BB1_534 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rax
	leaq	16(%rax), %rcx
	cmpb	$0, 32(%rax)
	je	.LBB1_599
# BB#600:                               #   in Loop: Header=BB1_534 Depth=1
	movq	128(%rsp,%rbx,8), %rcx
	movq	(%rcx), %rcx
	addq	$16, %rcx
	.p2align	4, 0x90
.LBB1_601:                              #   Parent Loop BB1_534 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	leaq	16(%rdx), %rcx
	cmpb	$0, 32(%rdx)
	je	.LBB1_601
# BB#602:                               #   in Loop: Header=BB1_534 Depth=1
	xorl	%ecx, %ecx
	cmpq	%rdx, %rax
	sete	%cl
	movl	%ecx, 164(%rsp)         # 4-byte Spill
	jmp	.LBB1_605
	.p2align	4, 0x90
.LBB1_603:                              #   in Loop: Header=BB1_534 Depth=1
	movl	$0, 104(%rsp)           # 4-byte Folded Spill
	movq	56(%rsp), %r15          # 8-byte Reload
	jmp	.LBB1_642
.LBB1_604:                              # %._crit_edge3103
                                        #   in Loop: Header=BB1_534 Depth=1
	movq	208(%rsp,%rbx,8), %r15
	movl	$0, 164(%rsp)           # 4-byte Folded Spill
.LBB1_605:                              #   in Loop: Header=BB1_534 Depth=1
	movq	8(%r15), %r13
	movq	8(%rdi), %r12
	cmpq	%rdi, %r12
	je	.LBB1_625
# BB#606:                               # %.lr.ph2758.preheader
                                        #   in Loop: Header=BB1_534 Depth=1
	movb	$1, %al
	movl	%eax, 144(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB1_607:                              # %.lr.ph2758.preheader
                                        #   Parent Loop BB1_534 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_609 Depth 3
                                        #       Child Loop BB1_611 Depth 3
	cmpq	%r15, %r13
	je	.LBB1_626
# BB#608:                               # %.preheader2565.preheader
                                        #   in Loop: Header=BB1_607 Depth=2
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB1_609:                              # %.preheader2565
                                        #   Parent Loop BB1_534 Depth=1
                                        #     Parent Loop BB1_607 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbp), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB1_609
# BB#610:                               # %.preheader2564.preheader
                                        #   in Loop: Header=BB1_607 Depth=2
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB1_611:                              # %.preheader2564
                                        #   Parent Loop BB1_534 Depth=1
                                        #     Parent Loop BB1_607 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB1_611
# BB#612:                               #   in Loop: Header=BB1_607 Depth=2
	cmpq	%rbx, %rbp
	jne	.LBB1_614
# BB#613:                               #   in Loop: Header=BB1_607 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.63, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	80(%rsp), %rdi          # 8-byte Reload
.LBB1_614:                              #   in Loop: Header=BB1_607 Depth=2
	movq	%rbp, xx_res(%rip)
	movq	%rbx, xx_hold(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB1_618
# BB#615:                               #   in Loop: Header=BB1_607 Depth=2
	movq	16(%rbx), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rbx), %rcx
	movq	%rax, 24(%rcx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rax, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_619
# BB#616:                               #   in Loop: Header=BB1_607 Depth=2
	testq	%rbp, %rbp
	je	.LBB1_619
# BB#617:                               #   in Loop: Header=BB1_607 Depth=2
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbp), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbp)
	movq	%rbp, 24(%rcx)
	jmp	.LBB1_619
	.p2align	4, 0x90
.LBB1_618:                              # %.thread3228
                                        #   in Loop: Header=BB1_607 Depth=2
	movq	$0, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_619:                              #   in Loop: Header=BB1_607 Depth=2
	movq	%rbx, zz_hold(%rip)
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB1_623
# BB#620:                               #   in Loop: Header=BB1_607 Depth=2
	movq	%rax, zz_res(%rip)
	movq	(%rbx), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	zz_res(%rip), %rax
	movq	xx_res(%rip), %rcx
	movq	%rax, xx_tmp(%rip)
	movq	%rcx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_624
# BB#621:                               #   in Loop: Header=BB1_607 Depth=2
	testq	%rcx, %rcx
	je	.LBB1_624
# BB#622:                               #   in Loop: Header=BB1_607 Depth=2
	movq	(%rax), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	jmp	.LBB1_624
	.p2align	4, 0x90
.LBB1_623:                              # %.thread3230
                                        #   in Loop: Header=BB1_607 Depth=2
	movq	$0, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_624:                              #   in Loop: Header=BB1_607 Depth=2
	movq	xx_hold(%rip), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	8(%r13), %r13
	movq	8(%r12), %r12
	cmpq	%rdi, %r12
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	208(%rsp,%rbx,8), %r15
	jne	.LBB1_607
.LBB1_625:                              #   in Loop: Header=BB1_534 Depth=1
	movl	$0, 144(%rsp)           # 4-byte Folded Spill
	movq	%rdi, %r12
	cmpl	$0, 104(%rsp)           # 4-byte Folded Reload
	jne	.LBB1_627
	jmp	.LBB1_634
	.p2align	4, 0x90
.LBB1_626:                              #   in Loop: Header=BB1_534 Depth=1
	movq	%r15, %r13
	cmpl	$0, 104(%rsp)           # 4-byte Folded Reload
	je	.LBB1_634
.LBB1_627:                              # %.critedge.i
                                        #   in Loop: Header=BB1_534 Depth=1
	cmpq	%r15, %r13
	je	.LBB1_634
# BB#628:                               #   in Loop: Header=BB1_534 Depth=1
	cmpb	$0, 32(%r13)
	je	.LBB1_630
# BB#629:                               #   in Loop: Header=BB1_534 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_630:                              #   in Loop: Header=BB1_534 Depth=1
	movq	%r13, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_632
# BB#631:                               #   in Loop: Header=BB1_534 Depth=1
	movq	(%r15), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r13), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_632:                              #   in Loop: Header=BB1_534 Depth=1
	movq	168(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	movq	%r13, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	je	.LBB1_634
# BB#633:                               #   in Loop: Header=BB1_534 Depth=1
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r13), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_634:                              #   in Loop: Header=BB1_534 Depth=1
	movq	208(%rsp,%rbx,8), %rdi
	callq	DisposeObject
	movl	164(%rsp), %ebp         # 4-byte Reload
	testl	%ebp, %ebp
	setne	%al
	andb	144(%rsp), %al          # 1-byte Folded Reload
	cmpb	$1, %al
	movq	112(%rsp), %r13         # 8-byte Reload
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	80(%rsp), %rdi          # 8-byte Reload
	jne	.LBB1_641
# BB#635:                               #   in Loop: Header=BB1_534 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	128(%rsp,%rax,8), %rbx
	cmpb	$0, 32(%r12)
	je	.LBB1_637
# BB#636:                               #   in Loop: Header=BB1_534 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	80(%rsp), %rdi          # 8-byte Reload
.LBB1_637:                              #   in Loop: Header=BB1_534 Depth=1
	movq	%r12, zz_res(%rip)
	movq	%rdi, zz_hold(%rip)
	testq	%rdi, %rdi
	je	.LBB1_639
# BB#638:                               #   in Loop: Header=BB1_534 Depth=1
	movq	(%rdi), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r12), %rax
	movq	%rax, (%rdi)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_639:                              #   in Loop: Header=BB1_534 Depth=1
	movq	%r12, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_641
# BB#640:                               #   in Loop: Header=BB1_534 Depth=1
	movq	(%rbx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r12), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_641:                              #   in Loop: Header=BB1_534 Depth=1
	callq	DisposeObject
	testl	%ebp, %ebp
	movl	104(%rsp), %eax         # 4-byte Reload
	cmovel	%ebp, %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movq	784(%rsp), %r12
.LBB1_642:                              # %.backedge
                                        #   in Loop: Header=BB1_534 Depth=1
	testq	%r14, %r14
	movq	192(%rsp), %rbx         # 8-byte Reload
	jne	.LBB1_534
# BB#643:                               # %._crit_edge2770
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	testq	%rax, %rax
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	168(%rsp), %rbx         # 8-byte Reload
	je	.LBB1_654
# BB#644:
	movq	%rax, xx_res(%rip)
	movq	128(%rsp,%rcx,8), %rcx
	movq	%rcx, xx_hold(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB1_647
# BB#645:
	movq	16(%rcx), %rsi
	movq	%rsi, 16(%rdx)
	movq	16(%rcx), %rsi
	movq	%rdx, 24(%rsi)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
	movq	%rdx, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rdx, zz_hold(%rip)
	testq	%rdx, %rdx
	je	.LBB1_648
# BB#646:
	movq	16(%rdx), %rsi
	movq	%rsi, zz_tmp(%rip)
	movq	16(%rax), %rdi
	movq	%rdi, 16(%rdx)
	movq	16(%rax), %rdi
	movq	%rdx, 24(%rdi)
	movq	%rsi, 16(%rax)
	movq	%rax, 24(%rsi)
	jmp	.LBB1_648
.LBB1_647:                              # %.thread3232
	movq	$0, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_648:
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB1_652
# BB#649:
	movq	%rdx, zz_res(%rip)
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	zz_res(%rip), %rax
	movq	xx_res(%rip), %rcx
	movq	%rax, xx_tmp(%rip)
	movq	%rcx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_653
# BB#650:
	testq	%rcx, %rcx
	je	.LBB1_653
# BB#651:
	movq	(%rax), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	jmp	.LBB1_653
.LBB1_652:                              # %.thread3234
	movq	$0, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_653:
	movq	xx_hold(%rip), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	32(%rsp), %rcx          # 8-byte Reload
.LBB1_654:
	movq	(%rbp,%rcx,8), %rax
	testq	%rax, %rax
	je	.LBB1_665
# BB#655:
	movq	%rax, xx_res(%rip)
	movq	%rbx, xx_hold(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	24(%rbx), %rcx
	cmpq	%rbx, %rcx
	je	.LBB1_658
# BB#656:
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rbx), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB1_659
# BB#657:
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	jmp	.LBB1_659
.LBB1_658:                              # %.thread3236
	movq	$0, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_659:
	movq	%rbx, zz_hold(%rip)
	movq	8(%rbx), %rcx
	cmpq	%rbx, %rcx
	je	.LBB1_663
# BB#660:
	movq	%rcx, zz_res(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	zz_res(%rip), %rax
	movq	xx_res(%rip), %rcx
	movq	%rax, xx_tmp(%rip)
	movq	%rcx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_664
# BB#661:
	testq	%rcx, %rcx
	je	.LBB1_664
# BB#662:
	movq	(%rax), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	jmp	.LBB1_664
.LBB1_663:                              # %.thread3238
	movq	$0, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_664:
	movq	xx_hold(%rip), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
.LBB1_665:                              # %ManifestTg.exit
	movq	%r13, %r12
	jmp	.LBB1_1126
.LBB1_666:                              # %.thread3259
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_667:
	movq	%r13, %rdi
	callq	DisposeObject
	subq	$8, %rsp
.Lcfi368:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi369:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi370:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi371:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi372:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi373:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi374:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	jmp	.LBB1_1126
.LBB1_668:                              # %.thread3277
	movq	$0, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_669:
	movq	xx_hold(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rcx), %edx
	leaq	zz_lengths(%rdx), %rsi
                                        # kill: %DL<def> %DL<kill> %RDX<kill>
	addb	$-11, %dl
	leaq	33(%rcx), %rdi
	cmpb	$2, %dl
	cmovbq	%rdi, %rsi
	movzbl	(%rsi), %edx
	movl	%edx, zz_size(%rip)
	movq	zz_free(,%rdx,8), %rsi
	movq	%rsi, (%rcx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rdx,8)
	leaq	64(%rsp), %rsi
	movq	%rax, %rdi
	movq	%r15, %rdx
	callq	InsertObject
	movq	%rax, %r12
	cmpq	$0, 64(%rsp)
	je	.LBB1_1126
# BB#670:
	movq	%r12, %r8
	addq	$32, %r8
	movl	$8, %edi
	movl	$34, %esi
	movl	$.L.str.34, %edx
	movl	$2, %ecx
	movl	$.L.str.35, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	64(%rsp), %rdi
	jmp	.LBB1_1125
.LBB1_680:
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rsi
	cmpq	%rbp, %rsi
	je	.LBB1_1005
# BB#681:
	testb	$6, %ah
	je	.LBB1_1006
# BB#682:
	movl	800(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.LBB1_1006
# BB#683:
	movl	%eax, %ecx
	andl	$1049600, %ecx          # imm = 0x100400
	andl	$16777215, %ecx         # imm = 0xFFFFFF
	cmpl	$1048576, %ecx          # imm = 0x100000
	je	.LBB1_1006
# BB#684:
	testb	$2, %ah
	je	.LBB1_687
# BB#685:
	testq	%rsi, %rsi
	je	.LBB1_687
# BB#686:
	movq	%rbp, %rdi
	callq	SearchUses
	testl	%eax, %eax
	jne	.LBB1_1006
.LBB1_687:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r13, %rsi
	callq	AttachEnv
	movb	$1, %al
	cmpq	$0, (%r14)
	jne	.LBB1_689
# BB#688:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, (%rax)
	setne	%al
.LBB1_689:
	movzbl	%al, %eax
	movzwl	42(%r13), %ecx
	andl	$65531, %ecx            # imm = 0xFFFB
	leal	(%rcx,%rax,4), %eax
	movw	%ax, 42(%r13)
	cmpq	$0, 8(%r14)
	jne	.LBB1_693
# BB#690:
	cmpq	$0, (%r14)
	jne	.LBB1_693
# BB#691:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, 8(%rax)
	jne	.LBB1_693
# BB#692:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, (%rax)
	je	.LBB1_1171
.LBB1_693:
	movq	%r13, %rdi
	jmp	.LBB1_1026
.LBB1_694:
	subq	$8, %rsp
.Lcfi375:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	pushq	$1
.Lcfi376:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi377:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi378:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi379:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi380:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi381:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	movb	32(%r12), %al
	andb	$-2, %al
	cmpb	$6, %al
	je	.LBB1_275
# BB#695:
	addq	$32, %r12
	movl	$8, %edi
	movl	$13, %esi
	movl	$.L.str.77, %edx
	movl	$2, %ecx
	movl	$.L.str.78, %r9d
	xorl	%eax, %eax
	movq	%r12, %r8
	callq	Error
	movl	$11, %edi
	movl	$.L.str.8, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, %rbx
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_1111
# BB#696:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	movq	784(%rsp), %rbp
	movq	%rbp, %r12
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB1_1112
# BB#697:
	testq	%rax, %rax
	je	.LBB1_1112
# BB#698:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB1_1112
.LBB1_699:
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_1056
# BB#700:
	movq	%rax, zz_res(%rip)
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	jmp	.LBB1_1057
.LBB1_701:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_702:
	movb	$17, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	34(%r13), %eax
	movw	%ax, 34(%rbx)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r13), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbx), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbx)
	andl	36(%r13), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_706
# BB#703:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%bpl
	je	.LBB1_707
# BB#704:
	testq	%rax, %rax
	je	.LBB1_707
# BB#705:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB1_707
.LBB1_706:                              # %.thread3226
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%bpl
.LBB1_707:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_709
# BB#708:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_710
.LBB1_709:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_710:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	sete	%cl
	orb	%cl, %bpl
	jne	.LBB1_712
# BB#711:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_712:
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_714
# BB#713:
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_714:
	movq	%rbx, %r13
.LBB1_715:
	movq	%r14, 88(%rsp)          # 8-byte Spill
	movzwl	(%r15), %ecx
	andl	$128, %ecx
	movzwl	64(%r13), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$256, %ecx              # imm = 0x100
	movl	%eax, %edx
	andl	$-257, %edx             # imm = 0xFEFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %edx             # imm = 0xFDFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %edx            # imm = 0xE3FF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	(%r15), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	2(%r15), %ecx
	movw	%cx, 66(%r13)
	movb	4(%r15), %cl
	andb	$3, %cl
	movb	68(%r13), %dl
	andb	$-4, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	4(%r15), %cl
	andb	$12, %cl
	andb	$-13, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	4(%r15), %cl
	andb	$112, %cl
	andb	$-113, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	(%r15), %cl
	andb	$8, %cl
	andb	$-9, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	4(%r15), %edx
	andl	$128, %edx
	movzwl	68(%r13), %ecx
	andl	$65407, %ecx            # imm = 0xFF7F
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$256, %edx              # imm = 0x100
	andl	$-257, %ecx             # imm = 0xFEFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$512, %edx              # imm = 0x200
	andl	$-513, %ecx             # imm = 0xFDFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$7168, %edx             # imm = 0x1C00
	andl	$-7169, %ecx            # imm = 0xE3FF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	4(%r15), %edx
	andl	$57344, %edx            # imm = 0xE000
	andl	$8191, %ecx             # imm = 0x1FFF
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	6(%r15), %ecx
	movw	%cx, 70(%r13)
	movl	$4095, %edx             # imm = 0xFFF
	andl	12(%r15), %edx
	movl	$-4096, %ecx            # imm = 0xF000
	andl	76(%r13), %ecx
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	12(%r15), %edx
	andl	$-4190209, %ecx         # imm = 0xFFC00FFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$12582912, %edx         # imm = 0xC00000
	andl	12(%r15), %edx
	andl	$-12582913, %ecx        # imm = 0xFF3FFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1056964608, %edx       # imm = 0x3F000000
	andl	12(%r15), %edx
	andl	$-1056964609, %ecx      # imm = 0xC0FFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	12(%r15), %edx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movl	$1073741824, %edx       # imm = 0x40000000
	andl	12(%r15), %edx
	andl	$-1073741825, %ecx      # imm = 0xBFFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%r13)
	movb	(%r15), %cl
	andb	$1, %cl
	andb	$-2, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$2, %cl
	andb	$-3, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$4, %cl
	andb	$-5, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movb	(%r15), %cl
	andb	$112, %cl
	andb	$-113, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	8(%r15), %eax
	movw	%ax, 72(%r13)
	movzwl	10(%r15), %eax
	movw	%ax, 74(%r13)
	movb	(%r15), %al
	shrb	$2, %al
	movzwl	42(%r13), %ecx
	andb	$1, %al
	movzbl	%al, %eax
	shll	$11, %eax
	andl	$63487, %ecx            # imm = 0xF7FF
	orl	%eax, %ecx
	movw	%cx, 42(%r13)
	movzwl	(%r15), %eax
	movl	%eax, %ecx
	andl	$128, %ecx
	movzwl	40(%rsp), %edx
	andl	$127, %edx
	orl	%ecx, %edx
	movl	%eax, %ecx
	andl	$256, %ecx              # imm = 0x100
	orl	%edx, %ecx
	movl	%eax, %edx
	andl	$512, %edx              # imm = 0x200
	orl	%ecx, %edx
	movl	%eax, %ecx
	andl	$64512, %ecx            # imm = 0xFC00
	orl	%edx, %ecx
	movw	%cx, 40(%rsp)
	movzwl	2(%r15), %ecx
	movw	%cx, 42(%rsp)
	movb	4(%r15), %cl
	movl	%ecx, %edx
	andb	$3, %dl
	movb	44(%rsp), %bl
	andb	$-128, %bl
	orb	%dl, %bl
	movl	%ecx, %edx
	andb	$12, %dl
	andb	$112, %cl
	orb	%dl, %cl
	orb	%bl, %cl
	movb	%cl, 44(%rsp)
	movzwl	4(%r15), %ecx
	movl	%ecx, %edx
	andl	$128, %edx
	movzwl	44(%rsp), %esi
	andl	$127, %esi
	orl	%edx, %esi
	movl	%ecx, %edx
	andl	$256, %edx              # imm = 0x100
	orl	%esi, %edx
	movl	%ecx, %esi
	andl	$512, %esi              # imm = 0x200
	orl	%edx, %esi
	andl	$64512, %ecx            # imm = 0xFC00
	orl	%esi, %ecx
	movw	%cx, 44(%rsp)
	movzwl	6(%r15), %ecx
	movw	%cx, 46(%rsp)
	movl	12(%r15), %ecx
	movl	%ecx, 52(%rsp)
	andb	$-5, %al
	movzwl	8(%r15), %ecx
	movw	%cx, 48(%rsp)
	movzwl	10(%r15), %ecx
	movw	%cx, 50(%rsp)
	movb	%al, 40(%rsp)
	movq	8(%r13), %r14
	cmpq	%r13, %r14
	jne	.LBB1_717
# BB#716:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.9, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%r13), %r14
.LBB1_717:
	movq	%r14, %r15
	.p2align	4, 0x90
.LBB1_718:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %r15
	movzbl	32(%r15), %eax
	testb	%al, %al
	je	.LBB1_718
# BB#719:
	cmpb	$1, %al
	jne	.LBB1_721
# BB#720:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.10, %r9d
	xorl	%eax, %eax
	callq	Error
	movb	32(%r15), %al
.LBB1_721:                              # %.loopexit2563.loopexit
	movq	8(%rsp), %r12           # 8-byte Reload
	addb	$-11, %al
	cmpb	$1, %al
	movq	%r13, 112(%rsp)         # 8-byte Spill
	ja	.LBB1_725
# BB#722:
	movl	$4095, %ecx             # imm = 0xFFF
	movq	56(%rsp), %rsi          # 8-byte Reload
	andl	12(%rsi), %ecx
	movl	$-4096, %eax            # imm = 0xF000
	andl	40(%r15), %eax
	orl	%ecx, %eax
	movl	%eax, 40(%r15)
	movl	$4190208, %ecx          # imm = 0x3FF000
	andl	12(%rsi), %ecx
	andl	$-4190209, %eax         # imm = 0xFFC00FFF
	orl	%ecx, %eax
	movl	%eax, 40(%r15)
	movl	$4194304, %ecx          # imm = 0x400000
	andl	12(%rsi), %ecx
	andl	$-4194305, %eax         # imm = 0xFFBFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r15)
	movl	12(%rsi), %ecx
	shrl	%ecx
	andl	$528482304, %ecx        # imm = 0x1F800000
	andl	$-528482305, %eax       # imm = 0xE07FFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r15)
	movb	4(%rsi), %cl
	andb	$3, %cl
	xorl	%edx, %edx
	cmpb	$2, %cl
	sete	%dl
	shll	$31, %edx
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	orl	%edx, %eax
	movl	%eax, 40(%r15)
	movl	792(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB1_737
# BB#723:
	movb	(%rsi), %cl
	andb	$8, %cl
	je	.LBB1_737
# BB#724:                               # %.thread2445
	movq	%r15, %rdi
	callq	MapSmallCaps
	movq	%rax, %r15
	movl	$-1610612737, %eax      # imm = 0x9FFFFFFF
	andl	40(%r15), %eax
	orl	$536870912, %eax        # imm = 0x20000000
	movl	%eax, 40(%r15)
	jmp	.LBB1_739
.LBB1_725:
	subq	$8, %rsp
.Lcfi382:
	.cfi_adjust_cfa_offset 8
	leaq	48(%rsp), %rdx
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi383:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi384:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi385:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %ebx
	pushq	%rbx
.Lcfi386:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi387:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi388:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r15
	movl	40(%r15), %eax
	jmp	.LBB1_738
.LBB1_737:
	movl	792(%rsp), %ebx
.LBB1_738:
	andl	$-1610612737, %eax      # imm = 0x9FFFFFFF
	orl	$536870912, %eax        # imm = 0x20000000
	movl	%eax, 40(%r15)
	testl	%ebx, %ebx
	je	.LBB1_746
.LBB1_739:
	movq	784(%rsp), %rax
	movq	(%rax), %rbx
	movb	$1, %al
	movl	%eax, 80(%rsp)          # 4-byte Spill
	testq	%rbx, %rbx
	je	.LBB1_747
# BB#740:
	movq	8(%rbx), %rbp
	cmpq	%rbx, %rbp
	je	.LBB1_745
# BB#741:
	cmpb	$0, 32(%rbp)
	je	.LBB1_743
# BB#742:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_743:
	movq	%rbp, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbp), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%rbp, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB1_745
# BB#744:
	movq	(%r14), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbp), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_745:
	movq	784(%rsp), %rax
	movq	%rax, %rbx
	movq	(%rbx), %rdi
	callq	DisposeObject
	movq	$0, (%rbx)
	jmp	.LBB1_747
.LBB1_746:
	movl	$0, 80(%rsp)            # 4-byte Folded Spill
.LBB1_747:                              # %.preheader2560
	movq	8(%r14), %r13
	cmpq	112(%rsp), %r13         # 8-byte Folded Reload
	je	.LBB1_851
# BB#748:
	movl	$-1610612737, %ebp      # imm = 0x9FFFFFFF
	jmp	.LBB1_749
.LBB1_762:                              #   in Loop: Header=BB1_749 Depth=1
	movl	%eax, %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	cmpl	$1024, %ecx             # imm = 0x400
	jne	.LBB1_842
# BB#763:                               #   in Loop: Header=BB1_749 Depth=1
	movzbl	42(%r14), %ecx
	movzbl	41(%r14), %edx
	negl	%edx
	cmpl	%edx, %ecx
	jne	.LBB1_842
# BB#764:                               #   in Loop: Header=BB1_749 Depth=1
	testq	%r15, %r15
	je	.LBB1_842
# BB#765:                               #   in Loop: Header=BB1_749 Depth=1
	movl	%eax, %ecx
	andl	$57344, %ecx            # imm = 0xE000
	movzwl	%cx, %ecx
	cmpl	$8192, %ecx             # imm = 0x2000
	jne	.LBB1_842
# BB#766:                               #   in Loop: Header=BB1_749 Depth=1
	testb	$1, %ah
	jne	.LBB1_842
# BB#767:                               #   in Loop: Header=BB1_749 Depth=1
	movb	32(%r15), %al
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB1_842
# BB#768:                               #   in Loop: Header=BB1_749 Depth=1
	movl	40(%r15), %ecx
	movl	40(%r12), %eax
	movl	%eax, %edx
	xorl	%ecx, %edx
	testl	$536870911, %edx        # imm = 0x1FFFFFFF
	jne	.LBB1_842
# BB#769:                               #   in Loop: Header=BB1_749 Depth=1
	andl	$1610612736, %ecx       # imm = 0x60000000
	cmpl	$536870912, %ecx        # imm = 0x20000000
	je	.LBB1_771
# BB#770:                               #   in Loop: Header=BB1_749 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	40(%r12), %eax
.LBB1_771:                              #   in Loop: Header=BB1_749 Depth=1
	andl	$1610612736, %eax       # imm = 0x60000000
	cmpl	$536870912, %eax        # imm = 0x20000000
	je	.LBB1_773
# BB#772:                               #   in Loop: Header=BB1_749 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.16, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_773:                              #   in Loop: Header=BB1_749 Depth=1
	leaq	32(%r15), %r14
	leaq	64(%r15), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	64(%r12), %rbx
	movq	%rbx, %rdi
	callq	strlen
	addq	104(%rsp), %rax         # 8-byte Folded Reload
	cmpq	$512, %rax              # imm = 0x200
	jb	.LBB1_775
# BB#774:                               #   in Loop: Header=BB1_749 Depth=1
	subq	$8, %rsp
.Lcfi389:
	.cfi_adjust_cfa_offset 8
	movl	$8, %edi
	movl	$24, %esi
	movl	$.L.str.17, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbp, %r9
	pushq	%rbx
.Lcfi390:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi391:
	.cfi_adjust_cfa_offset -16
.LBB1_775:                              #   in Loop: Header=BB1_749 Depth=1
	movq	%r12, 64(%rsp)
	cmpb	$12, (%r14)
	je	.LBB1_777
# BB#776:                               #   in Loop: Header=BB1_749 Depth=1
	movl	$11, %edi
	cmpb	$12, 32(%r12)
	jne	.LBB1_778
.LBB1_777:                              # %.thread2449
                                        #   in Loop: Header=BB1_749 Depth=1
	movl	$12, %edi
.LBB1_778:                              #   in Loop: Header=BB1_749 Depth=1
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	callq	MakeWordTwo
	movq	%rax, %r12
	movl	40(%r15), %ecx
	movl	$4095, %eax             # imm = 0xFFF
	andl	%eax, %ecx
	movl	40(%r12), %eax
	movl	$-4096, %edx            # imm = 0xF000
	andl	%edx, %eax
	orl	%ecx, %eax
	movl	%eax, 40(%r12)
	movl	40(%r15), %ecx
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	%edx, %ecx
	andl	$-4190209, %eax         # imm = 0xFFC00FFF
	orl	%ecx, %eax
	movl	%eax, 40(%r12)
	movl	40(%r15), %ecx
	movl	$4194304, %edx          # imm = 0x400000
	andl	%edx, %ecx
	andl	$-4194305, %eax         # imm = 0xFFBFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r12)
	movl	40(%r15), %ecx
	movl	$528482304, %edx        # imm = 0x1F800000
	andl	%edx, %ecx
	andl	$-528482305, %eax       # imm = 0xE07FFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r12)
	movl	40(%r15), %ecx
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	%edx, %ecx
	andl	$536870911, %eax        # imm = 0x1FFFFFFF
	leal	536870912(%rcx,%rax), %eax
	movl	%eax, 40(%r12)
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, xx_link(%rip)
	movq	%rdx, zz_hold(%rip)
	movq	24(%rdx), %rax
	cmpq	%rdx, %rax
	je	.LBB1_780
# BB#779:                               #   in Loop: Header=BB1_749 Depth=1
	movq	%rax, zz_res(%rip)
	movq	16(%rdx), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rdx), %rcx
	movq	%rax, 24(%rcx)
	movq	%rdx, 24(%rdx)
	movq	%rdx, 16(%rdx)
.LBB1_780:                              #   in Loop: Header=BB1_749 Depth=1
	movq	%rdx, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB1_782
# BB#781:                               #   in Loop: Header=BB1_749 Depth=1
	movq	16(%r12), %rax
	movq	%rax, zz_tmp(%rip)
	movq	16(%rdx), %rcx
	movq	%rcx, 16(%r12)
	movq	16(%rdx), %rcx
	movq	%r12, 24(%rcx)
	movq	%rax, 16(%rdx)
	movq	%rdx, 24(%rax)
.LBB1_782:                              #   in Loop: Header=BB1_749 Depth=1
	movq	64(%rsp), %rdi
	callq	DisposeObject
	movq	24(%r15), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_786
# BB#783:                               #   in Loop: Header=BB1_749 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_787
.LBB1_784:                              #   in Loop: Header=BB1_749 Depth=1
	movl	$0, 80(%rsp)            # 4-byte Folded Spill
	jmp	.LBB1_850
.LBB1_785:                              #   in Loop: Header=BB1_749 Depth=1
	movb	$1, %al
	movl	%eax, 80(%rsp)          # 4-byte Spill
	jmp	.LBB1_850
.LBB1_786:                              #   in Loop: Header=BB1_749 Depth=1
	xorl	%ecx, %ecx
.LBB1_787:                              #   in Loop: Header=BB1_749 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_789
# BB#788:                               #   in Loop: Header=BB1_749 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_789:                              #   in Loop: Header=BB1_749 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_791
# BB#790:                               #   in Loop: Header=BB1_749 Depth=1
	callq	DisposeObject
.LBB1_791:                              #   in Loop: Header=BB1_749 Depth=1
	movq	%r13, xx_link(%rip)
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_793
# BB#792:                               #   in Loop: Header=BB1_749 Depth=1
	movq	%rax, zz_res(%rip)
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	jmp	.LBB1_794
.LBB1_793:                              #   in Loop: Header=BB1_749 Depth=1
	xorl	%eax, %eax
.LBB1_794:                              #   in Loop: Header=BB1_749 Depth=1
	movq	%rax, xx_tmp(%rip)
	movq	%r13, zz_hold(%rip)
	movq	8(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_796
# BB#795:                               #   in Loop: Header=BB1_749 Depth=1
	movq	%rax, zz_res(%rip)
	movq	(%r13), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %r13
.LBB1_796:                              #   in Loop: Header=BB1_749 Depth=1
	movq	%r13, zz_hold(%rip)
	movzbl	32(%r13), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r13), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r13)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_842
# BB#797:                               #   in Loop: Header=BB1_749 Depth=1
	callq	DisposeObject
	jmp	.LBB1_842
	.p2align	4, 0x90
.LBB1_749:                              # %.preheader2558
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_750 Depth 2
                                        #     Child Loop BB1_756 Depth 2
                                        #     Child Loop BB1_801 Depth 2
                                        #     Child Loop BB1_826 Depth 2
                                        #       Child Loop BB1_828 Depth 3
                                        #     Child Loop BB1_832 Depth 2
                                        #     Child Loop BB1_812 Depth 2
                                        #       Child Loop BB1_814 Depth 3
                                        #     Child Loop BB1_818 Depth 2
	movq	%r13, %r14
	.p2align	4, 0x90
.LBB1_750:                              #   Parent Loop BB1_749 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r14), %r14
	movzbl	32(%r14), %eax
	testb	%al, %al
	je	.LBB1_750
# BB#751:                               #   in Loop: Header=BB1_749 Depth=1
	cmpb	$1, %al
	je	.LBB1_753
# BB#752:                               #   in Loop: Header=BB1_749 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_753:                              # %.loopexit2559
                                        #   in Loop: Header=BB1_749 Depth=1
	movl	40(%r14), %eax
	andl	%ebp, %eax
	orl	$536870912, %eax        # imm = 0x20000000
	movl	%eax, 40(%r14)
	movq	8(%r13), %r12
	cmpq	112(%rsp), %r12         # 8-byte Folded Reload
	jne	.LBB1_755
# BB#754:                               #   in Loop: Header=BB1_749 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_755:                              # %.preheader2556.preheader
                                        #   in Loop: Header=BB1_749 Depth=1
	movq	%r12, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_756:                              # %.preheader2556
                                        #   Parent Loop BB1_749 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r12), %r12
	movzbl	32(%r12), %eax
	testb	%al, %al
	je	.LBB1_756
# BB#757:                               # %.preheader2556
                                        #   in Loop: Header=BB1_749 Depth=1
	cmpb	$1, %al
	jne	.LBB1_759
# BB#758:                               #   in Loop: Header=BB1_749 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
	movb	32(%r12), %al
.LBB1_759:                              # %.loopexit2557
                                        #   in Loop: Header=BB1_749 Depth=1
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB1_798
# BB#760:                               #   in Loop: Header=BB1_749 Depth=1
	movq	56(%rsp), %rsi          # 8-byte Reload
	movl	12(%rsi), %ecx
	movl	$4095, %eax             # imm = 0xFFF
	andl	%eax, %ecx
	movl	40(%r12), %eax
	movl	$-4096, %edx            # imm = 0xF000
	andl	%edx, %eax
	orl	%ecx, %eax
	movl	%eax, 40(%r12)
	movl	12(%rsi), %ecx
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	%edx, %ecx
	andl	$-4190209, %eax         # imm = 0xFFC00FFF
	orl	%ecx, %eax
	movl	%eax, 40(%r12)
	movl	12(%rsi), %ecx
	movl	$4194304, %edx          # imm = 0x400000
	andl	%edx, %ecx
	andl	$-4194305, %eax         # imm = 0xFFBFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r12)
	movl	12(%rsi), %ecx
	shrl	%ecx
	andl	$528482304, %ecx        # imm = 0x1F800000
	andl	$-528482305, %eax       # imm = 0xE07FFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r12)
	movb	4(%rsi), %cl
	andb	$3, %cl
	xorl	%edx, %edx
	cmpb	$2, %cl
	sete	%dl
	shll	$31, %edx
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	orl	%edx, %eax
	movl	%eax, 40(%r12)
	movb	(%rsi), %al
	andb	$8, %al
	shrb	$3, %al
	andb	80(%rsp), %al           # 1-byte Folded Reload
	cmpb	$1, %al
	jne	.LBB1_800
# BB#761:                               #   in Loop: Header=BB1_749 Depth=1
	movq	%r12, %rdi
	callq	MapSmallCaps
	jmp	.LBB1_799
	.p2align	4, 0x90
.LBB1_798:                              #   in Loop: Header=BB1_749 Depth=1
	subq	$8, %rsp
.Lcfi392:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	48(%rsp), %rdx
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi393:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi394:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi395:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi396:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi397:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi398:
	.cfi_adjust_cfa_offset -48
.LBB1_799:                              #   in Loop: Header=BB1_749 Depth=1
	movq	%rax, %r12
.LBB1_800:                              #   in Loop: Header=BB1_749 Depth=1
	movl	40(%r12), %eax
	andl	%ebp, %eax
	orl	$536870912, %eax        # imm = 0x20000000
	movl	%eax, 40(%r12)
	movq	8(%r14), %rdi
	cmpq	%r14, %rdi
	je	.LBB1_803
	.p2align	4, 0x90
.LBB1_801:                              # %.preheader2555
                                        #   Parent Loop BB1_749 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB1_801
# BB#802:                               #   in Loop: Header=BB1_749 Depth=1
	movq	%rdi, 64(%rsp)
	subq	$8, %rsp
.Lcfi399:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	48(%rsp), %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi400:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi401:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi402:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi403:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi404:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi405:
	.cfi_adjust_cfa_offset -48
	movq	%rax, 64(%rsp)
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, 64(%rsp)
	leaq	44(%r14), %rdx
	movq	%rax, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	leaq	100(%rsp), %rcx
	callq	GetGap
	movw	$0, 41(%r14)
	jmp	.LBB1_839
.LBB1_803:                              #   in Loop: Header=BB1_749 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	movzwl	4(%rdx), %ecx
	andl	$128, %ecx
	movzwl	44(%r14), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 44(%r14)
	movzwl	4(%rdx), %ecx
	andl	$256, %ecx              # imm = 0x100
	andl	$-257, %eax             # imm = 0xFEFF
	orl	%ecx, %eax
	movw	%ax, 44(%r14)
	movzwl	4(%rdx), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %eax             # imm = 0xFDFF
	orl	%ecx, %eax
	movw	%ax, 44(%r14)
	movzwl	4(%rdx), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %eax            # imm = 0xE3FF
	orl	%ecx, %eax
	movw	%ax, 44(%r14)
	movzwl	4(%rdx), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %eax             # imm = 0x1FFF
	orl	%ecx, %eax
	movw	%ax, 44(%r14)
	movw	6(%rdx), %ax
	movw	%ax, 46(%r14)
	movb	(%rdx), %cl
	shrb	$4, %cl
	andb	$7, %cl
	cmpb	$4, %cl
	ja	.LBB1_806
# BB#804:                               #   in Loop: Header=BB1_749 Depth=1
	movzbl	%cl, %ecx
	jmpq	*.LJTI1_2(,%rcx,8)
.LBB1_805:                              #   in Loop: Header=BB1_749 Depth=1
	movzbl	42(%r14), %ecx
	movzbl	41(%r14), %edx
	addl	%ecx, %edx
	imull	%edx, %eax
	movw	%ax, 46(%r14)
	jmp	.LBB1_838
.LBB1_806:                              #   in Loop: Header=BB1_749 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.14, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB1_838
.LBB1_807:                              #   in Loop: Header=BB1_749 Depth=1
	movzbl	42(%r14), %eax
	movzbl	41(%r14), %ecx
	addl	%eax, %ecx
	jne	.LBB1_838
# BB#808:                               #   in Loop: Header=BB1_749 Depth=1
	movw	$0, 46(%r14)
	jmp	.LBB1_838
.LBB1_809:                              #   in Loop: Header=BB1_749 Depth=1
	movzbl	42(%r14), %ecx
	movzbl	41(%r14), %edx
	addl	%ecx, %edx
	imull	%edx, %eax
	testw	%cx, %cx
	movw	%ax, 46(%r14)
	je	.LBB1_838
# BB#810:                               #   in Loop: Header=BB1_749 Depth=1
	movq	%r15, 64(%rsp)
	movb	32(%r15), %al
	addb	$-17, %al
	cmpb	$20, %al
	movq	%r15, %rdi
	ja	.LBB1_816
# BB#811:                               # %.lr.ph2740.preheader
                                        #   in Loop: Header=BB1_749 Depth=1
	movq	%r15, %rdi
.LBB1_812:                              # %.lr.ph2740
                                        #   Parent Loop BB1_749 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_814 Depth 3
	movzbl	%al, %eax
	movl	$1573249, %ecx          # imm = 0x180181
	btl	%eax, %ecx
	jae	.LBB1_816
# BB#813:                               #   in Loop: Header=BB1_812 Depth=2
	movq	(%rdi), %rdi
	.p2align	4, 0x90
.LBB1_814:                              #   Parent Loop BB1_749 Depth=1
                                        #     Parent Loop BB1_812 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %eax
	testb	%al, %al
	je	.LBB1_814
# BB#815:                               # %.loopexit2551
                                        #   in Loop: Header=BB1_812 Depth=2
	movq	%rdi, 64(%rsp)
	addb	$-17, %al
	cmpb	$20, %al
	jbe	.LBB1_812
.LBB1_816:                              # %.loopexit2553
                                        #   in Loop: Header=BB1_749 Depth=1
	movb	32(%rdi), %al
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB1_838
# BB#817:                               #   in Loop: Header=BB1_749 Depth=1
	movq	%rdi, %rax
	decq	%rax
	.p2align	4, 0x90
.LBB1_818:                              #   Parent Loop BB1_749 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, 65(%rax)
	leaq	1(%rax), %rax
	jne	.LBB1_818
# BB#819:                               #   in Loop: Header=BB1_749 Depth=1
	cmpq	%rax, %rdi
	je	.LBB1_838
# BB#820:                               #   in Loop: Header=BB1_749 Depth=1
	movzbl	63(%rax), %eax
	cmpl	$0, LanguageSentenceEnds(,%rax,4)
	je	.LBB1_838
# BB#821:                               #   in Loop: Header=BB1_749 Depth=1
	xorl	%esi, %esi
	jmp	.LBB1_836
.LBB1_822:                              #   in Loop: Header=BB1_749 Depth=1
	movzbl	42(%r14), %eax
	movzbl	41(%r14), %ecx
	addl	%eax, %ecx
	jne	.LBB1_824
# BB#823:                               #   in Loop: Header=BB1_749 Depth=1
	movw	$0, 46(%r14)
	jmp	.LBB1_838
.LBB1_824:                              #   in Loop: Header=BB1_749 Depth=1
	movq	%r15, 64(%rsp)
	movb	32(%r15), %al
	addb	$-17, %al
	cmpb	$20, %al
	movq	%r15, %rdi
	ja	.LBB1_830
# BB#825:                               # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_749 Depth=1
	movq	%r15, %rdi
.LBB1_826:                              # %.lr.ph
                                        #   Parent Loop BB1_749 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_828 Depth 3
	movzbl	%al, %eax
	movl	$1573249, %ecx          # imm = 0x180181
	btl	%eax, %ecx
	jae	.LBB1_830
# BB#827:                               #   in Loop: Header=BB1_826 Depth=2
	movq	(%rdi), %rdi
	.p2align	4, 0x90
.LBB1_828:                              #   Parent Loop BB1_749 Depth=1
                                        #     Parent Loop BB1_826 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %eax
	testb	%al, %al
	je	.LBB1_828
# BB#829:                               # %.loopexit2552
                                        #   in Loop: Header=BB1_826 Depth=2
	movq	%rdi, 64(%rsp)
	addb	$-17, %al
	cmpb	$20, %al
	jbe	.LBB1_826
.LBB1_830:                              # %.loopexit2554
                                        #   in Loop: Header=BB1_749 Depth=1
	movb	32(%rdi), %al
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB1_838
# BB#831:                               #   in Loop: Header=BB1_749 Depth=1
	movq	%rdi, %rax
	decq	%rax
	.p2align	4, 0x90
.LBB1_832:                              #   Parent Loop BB1_749 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, 65(%rax)
	leaq	1(%rax), %rax
	jne	.LBB1_832
# BB#833:                               #   in Loop: Header=BB1_749 Depth=1
	cmpq	%rax, %rdi
	je	.LBB1_838
# BB#834:                               #   in Loop: Header=BB1_749 Depth=1
	movzbl	63(%rax), %eax
	cmpl	$0, LanguageSentenceEnds(,%rax,4)
	je	.LBB1_838
# BB#835:                               #   in Loop: Header=BB1_749 Depth=1
	movl	$1, %esi
.LBB1_836:                              #   in Loop: Header=BB1_749 Depth=1
	callq	LanguageWordEndsSentence
	testl	%eax, %eax
	je	.LBB1_838
# BB#837:                               #   in Loop: Header=BB1_749 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movzwl	6(%rax), %eax
	addw	%ax, 46(%r14)
.LBB1_838:                              #   in Loop: Header=BB1_749 Depth=1
	xorl	%eax, %eax
	cmpw	$0, 46(%r14)
	sete	%al
	movzwl	44(%r14), %ecx
	shll	$7, %eax
	andl	$65407, %ecx            # imm = 0xFF7F
	orl	%eax, %ecx
	movw	%cx, 44(%r14)
.LBB1_839:                              #   in Loop: Header=BB1_749 Depth=1
	movb	32(%r12), %al
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB1_842
# BB#840:                               #   in Loop: Header=BB1_749 Depth=1
	cmpw	$0, 46(%r14)
	jne	.LBB1_842
# BB#841:                               #   in Loop: Header=BB1_749 Depth=1
	movw	44(%r14), %ax
	testb	%al, %al
	js	.LBB1_762
	.p2align	4, 0x90
.LBB1_842:                              #   in Loop: Header=BB1_749 Depth=1
	movq	%r12, %r15
	testb	$1, 80(%rsp)            # 1-byte Folded Reload
	movq	784(%rsp), %r14
	je	.LBB1_784
# BB#843:                               #   in Loop: Header=BB1_749 Depth=1
	movq	(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB1_785
# BB#844:                               #   in Loop: Header=BB1_749 Depth=1
	movq	8(%rbp), %rbx
	cmpq	%rbp, %rbx
	je	.LBB1_849
# BB#845:                               #   in Loop: Header=BB1_749 Depth=1
	cmpb	$0, 32(%rbx)
	je	.LBB1_847
# BB#846:                               #   in Loop: Header=BB1_749 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_847:                              #   in Loop: Header=BB1_749 Depth=1
	movq	%rbx, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%rbx, zz_res(%rip)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_849
# BB#848:                               #   in Loop: Header=BB1_749 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_849:                              #   in Loop: Header=BB1_749 Depth=1
	movq	(%r14), %rdi
	callq	DisposeObject
	movq	$0, (%r14)
.LBB1_850:                              # %.outer.loopexit
                                        #   in Loop: Header=BB1_749 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %r13
	cmpq	112(%rsp), %r13         # 8-byte Folded Reload
	movl	$-1610612737, %ebp      # imm = 0x9FFFFFFF
	jne	.LBB1_749
.LBB1_851:                              # %.outer._crit_edge
	movq	88(%rsp), %rsi          # 8-byte Reload
	cmpq	$0, 8(%rsi)
	jne	.LBB1_855
# BB#852:
	cmpq	$0, (%rsi)
	jne	.LBB1_855
# BB#853:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, 8(%rax)
	jne	.LBB1_855
# BB#854:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, (%rax)
	je	.LBB1_1133
.LBB1_855:
	movq	112(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB1_520
.LBB1_726:
	movq	%rax, 64(%rsp)
	xorl	%ebx, %ebx
	movl	$8, %edi
	movl	$35, %esi
	jmp	.LBB1_728
.LBB1_727:
	xorl	%ebx, %ebx
	movl	$8, %edi
	movl	$36, %esi
.LBB1_728:
	movl	$.L.str.41, %edx
	movl	$2, %ecx
	movl	$.L.str.42, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB1_966
.LBB1_729:
	testb	%al, %al
	je	.LBB1_1027
# BB#730:
	movq	8(%r14), %rbx
	movq	32(%rsp), %rbp          # 8-byte Reload
	cmpq	%rbx, %rbp
	je	.LBB1_1036
# BB#731:
	cmpb	$0, 32(%rbp)
	je	.LBB1_733
# BB#732:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_733:
	movq	%rbp, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_735
# BB#734:
	movq	(%rbx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbp), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_735:
	movq	%rbp, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_1036
# BB#736:
	movq	(%r15), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbp), %rax
	jmp	.LBB1_1035
.LBB1_856:
	movq	8(%rax), %rax
	addq	$16, %rax
.LBB1_857:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_857
# BB#858:
	subq	$8, %rsp
.Lcfi406:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi407:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi408:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi409:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi410:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi411:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi412:
	.cfi_adjust_cfa_offset -48
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %rbp
	movb	32(%rbp), %al
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB1_862
# BB#859:
	leaq	64(%rbp), %r15
	movl	$.L.str.82, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_1131
# BB#860:
	movl	$.L.str.83, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_1134
# BB#861:
	movl	$.L.str.84, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	movq	56(%rsp), %r15          # 8-byte Reload
	je	.LBB1_1147
.LBB1_862:
	addq	$32, %rbp
	subq	$8, %rsp
.Lcfi413:
	.cfi_adjust_cfa_offset 8
	movl	$8, %edi
	movl	$16, %esi
	movl	$.L.str.85, %edx
	movl	$2, %ecx
	movl	$.L.str.82, %r9d
	movl	$0, %eax
	movq	%rbp, %r8
	pushq	$.L.str.78
.Lcfi414:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.84
.Lcfi415:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.83
.Lcfi416:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$32, %rsp
.Lcfi417:
	.cfi_adjust_cfa_offset -32
	movl	$11, %edi
	movl	$.L.str.8, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, %rbx
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_1128
# BB#863:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	movq	784(%rsp), %rbp
	movq	%rbp, %r12
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB1_1129
# BB#864:
	testq	%rax, %rax
	je	.LBB1_1129
# BB#865:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB1_1129
.LBB1_866:
	movq	48(%rbp), %rsi
	cmpq	StartSym(%rip), %rsi
	movq	88(%rsp), %r14          # 8-byte Reload
	je	.LBB1_1109
# BB#867:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	SearchEnv
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_1130
# BB#868:
	cmpb	$2, 32(%rbx)
	jne	.LBB1_1132
# BB#869:
	movq	24(%rbx), %rbp
.LBB1_870:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %edi
	cmpl	$82, %edi
	je	.LBB1_874
# BB#871:                               #   in Loop: Header=BB1_870 Depth=1
	testb	%dil, %dil
	je	.LBB1_870
# BB#872:
	movq	stderr(%rip), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	callq	Image
	movq	%rax, %rcx
	movl	$.L.str.50, %esi
	xorl	%eax, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rcx, %rdx
	callq	fprintf
	cmpb	$82, 32(%rbp)
	je	.LBB1_874
# BB#873:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.51, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_874:                              # %.thread
	movq	8(%rbp), %rax
	cmpq	(%rbp), %rax
	je	.LBB1_876
# BB#875:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	SetEnv
	movq	%rax, %rbp
.LBB1_876:
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_1161
# BB#877:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_1162
.LBB1_878:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_879:
	movb	$17, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	24(%rbx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_881
# BB#880:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_881:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_884
# BB#882:
	testq	%rax, %rax
	je	.LBB1_884
# BB#883:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_884:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_895
# BB#885:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_896
.LBB1_886:
	movl	$8, %edi
	movl	$31, %esi
	movl	$.L.str.25, %edx
	movl	$2, %ecx
	movl	$.L.str.24, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB1_1048
.LBB1_887:
	xorl	%eax, %eax
.LBB1_888:
	movq	%rax, xx_tmp(%rip)
	movq	64(%rsp), %r12
	movq	%r12, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_891
# BB#889:
	testq	%r12, %r12
	je	.LBB1_891
.LBB1_890:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r12), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%r12)
	movq	%r12, 24(%rcx)
.LBB1_891:
	movq	%r13, %rdi
	jmp	.LBB1_1125
.LBB1_892:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_893:
	movb	$17, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_959
# BB#894:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_960
.LBB1_895:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_896:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB1_899
# BB#897:
	testq	%rax, %rax
	je	.LBB1_899
# BB#898:
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_899:
	testq	%rbp, %rbp
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB1_902
# BB#900:
	testq	%rax, %rax
	je	.LBB1_902
# BB#901:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_902:                              # %.loopexit2592
	cmpb	$17, 32(%rbp)
	jne	.LBB1_947
.LBB1_903:                              # %.preheader2584
	movq	8(%rbp), %rcx
	cmpq	%rbp, %rcx
	je	.LBB1_972
# BB#904:                               # %.lr.ph2777.lr.ph.lr.ph
	movq	%r14, 88(%rsp)          # 8-byte Spill
	movq	%r15, 80(%rsp)          # 8-byte Spill
	leaq	64(%r15), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
                                        # implicit-def: %RAX
	movq	%rax, 152(%rsp)         # 8-byte Spill
.LBB1_905:                              # %.lr.ph2777.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_907 Depth 2
                                        #     Child Loop BB1_911 Depth 2
                                        #     Child Loop BB1_927 Depth 2
                                        #       Child Loop BB1_929 Depth 3
                                        #       Child Loop BB1_933 Depth 3
                                        #     Child Loop BB1_939 Depth 2
                                        #     Child Loop BB1_918 Depth 2
	leaq	16(%rcx), %r15
	jmp	.LBB1_907
	.p2align	4, 0x90
.LBB1_906:                              #   in Loop: Header=BB1_907 Depth=2
	addq	$16, %r15
.LBB1_907:                              #   Parent Loop BB1_905 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %r15
	movzbl	32(%r15), %eax
	testb	%al, %al
	je	.LBB1_906
# BB#908:                               #   in Loop: Header=BB1_905 Depth=1
	cmpb	$1, %al
	je	.LBB1_915
# BB#909:                               #   in Loop: Header=BB1_905 Depth=1
	cmpb	$55, %al
	jne	.LBB1_948
# BB#910:                               #   in Loop: Header=BB1_905 Depth=1
	movq	%rcx, %r14
	movq	8(%r15), %rax
	addq	$16, %rax
.LBB1_911:                              #   Parent Loop BB1_905 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_911
# BB#912:                               #   in Loop: Header=BB1_905 Depth=1
	subq	$8, %rsp
.Lcfi418:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	%r12, %rsi
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi419:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi420:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi421:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi422:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi423:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi424:
	.cfi_adjust_cfa_offset -48
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %rbx
	movb	32(%rbx), %al
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jb	.LBB1_916
# BB#913:                               #   in Loop: Header=BB1_905 Depth=1
	cmpb	$17, %al
	je	.LBB1_926
# BB#914:                               #   in Loop: Header=BB1_905 Depth=1
	addq	$32, %rbx
	movl	$8, %edi
	movl	$10, %esi
	movl	$.L.str.73, %edx
	movl	$2, %ecx
	movl	$.L.str.22, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movq	%r14, %rcx
.LBB1_915:                              # %.backedge2591.us
                                        #   in Loop: Header=BB1_905 Depth=1
	movq	8(%rcx), %rcx
	cmpq	%rbp, %rcx
	jne	.LBB1_905
	jmp	.LBB1_949
.LBB1_916:                              # %.us-lcssa2792
                                        #   in Loop: Header=BB1_905 Depth=1
	testq	%r13, %r13
	jne	.LBB1_920
# BB#917:                               #   in Loop: Header=BB1_905 Depth=1
	movq	(%r15), %rax
	addq	$16, %rax
.LBB1_918:                              #   Parent Loop BB1_905 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	leaq	16(%rcx), %rax
	cmpb	$0, 32(%rcx)
	je	.LBB1_918
# BB#919:                               # %.loopexit2576
                                        #   in Loop: Header=BB1_905 Depth=1
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	%rbx, %r13
.LBB1_920:                              #   in Loop: Header=BB1_905 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movb	32(%rax), %al
	addb	$-11, %al
	addq	$64, %rbx
	cmpb	$1, %al
	ja	.LBB1_922
# BB#921:                               #   in Loop: Header=BB1_905 Depth=1
	movq	%rbx, %rdi
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_923
.LBB1_922:                              # %._crit_edge3140
                                        #   in Loop: Header=BB1_905 Depth=1
	movl	$.L.str.72, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_945
	jmp	.LBB1_923
.LBB1_926:                              #   in Loop: Header=BB1_905 Depth=1
	movq	%rbx, 144(%rsp)         # 8-byte Spill
.LBB1_927:                              # %.loopexit2575
                                        #   Parent Loop BB1_905 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_929 Depth 3
                                        #       Child Loop BB1_933 Depth 3
	movq	8(%rbx), %rbx
	cmpq	144(%rsp), %rbx         # 8-byte Folded Reload
	je	.LBB1_945
# BB#928:                               # %.preheader2574
                                        #   in Loop: Header=BB1_927 Depth=2
	movq	%rbx, %r12
.LBB1_929:                              #   Parent Loop BB1_905 Depth=1
                                        #     Parent Loop BB1_927 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r12), %r12
	movzbl	32(%r12), %eax
	testb	%al, %al
	je	.LBB1_929
# BB#930:                               #   in Loop: Header=BB1_927 Depth=2
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jae	.LBB1_943
# BB#931:                               #   in Loop: Header=BB1_927 Depth=2
	testq	%r13, %r13
	jne	.LBB1_935
# BB#932:                               #   in Loop: Header=BB1_927 Depth=2
	movq	(%r15), %rax
	addq	$16, %rax
.LBB1_933:                              #   Parent Loop BB1_905 Depth=1
                                        #     Parent Loop BB1_927 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rcx
	leaq	16(%rcx), %rax
	cmpb	$0, 32(%rcx)
	je	.LBB1_933
# BB#934:                               # %.loopexit2573
                                        #   in Loop: Header=BB1_927 Depth=2
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	%r12, %r13
.LBB1_935:                              #   in Loop: Header=BB1_927 Depth=2
	movq	80(%rsp), %rax          # 8-byte Reload
	movb	32(%rax), %al
	addb	$-11, %al
	addq	$64, %r12
	cmpb	$1, %al
	ja	.LBB1_937
# BB#936:                               #   in Loop: Header=BB1_927 Depth=2
	movq	%r12, %rdi
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_938
.LBB1_937:                              # %._crit_edge3139
                                        #   in Loop: Header=BB1_927 Depth=2
	movl	$.L.str.72, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_927
	jmp	.LBB1_938
.LBB1_943:                              #   in Loop: Header=BB1_927 Depth=2
	cmpb	$1, %al
	je	.LBB1_927
# BB#944:                               #   in Loop: Header=BB1_905 Depth=1
	addq	$32, %r12
	movl	$8, %edi
	movl	$9, %esi
	movl	$.L.str.73, %edx
	movl	$2, %ecx
	movl	$.L.str.22, %r9d
	xorl	%eax, %eax
	movq	%r12, %r8
	callq	Error
.LBB1_945:                              # %.outer2586.backedge
                                        #   in Loop: Header=BB1_905 Depth=1
	movq	%r14, %rcx
	movq	8(%rcx), %rcx
	cmpq	%rbp, %rcx
	movq	8(%rsp), %r12           # 8-byte Reload
	jne	.LBB1_905
	jmp	.LBB1_949
.LBB1_938:                              #   in Loop: Header=BB1_905 Depth=1
	movq	(%r15), %rax
	addq	$16, %rax
.LBB1_939:                              #   Parent Loop BB1_905 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %r15
	leaq	16(%r15), %rax
	cmpb	$0, 32(%r15)
	je	.LBB1_939
# BB#940:                               # %.loopexit2577
                                        #   in Loop: Header=BB1_905 Depth=1
	testq	%r15, %r15
	jne	.LBB1_925
# BB#941:                               # %.loopexit2577
                                        #   in Loop: Header=BB1_905 Depth=1
	movq	%r14, %rcx
	movq	8(%rcx), %rcx
	cmpq	%rbp, %rcx
	movq	8(%rsp), %r12           # 8-byte Reload
	jne	.LBB1_905
	jmp	.LBB1_942
.LBB1_947:
	addq	$32, %rbp
	movl	$8, %edi
	movl	$7, %esi
	movl	$.L.str.69, %edx
	movl	$2, %ecx
	movl	$.L.str.70, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
	jmp	.LBB1_1082
.LBB1_948:                              # %.us-lcssa
	addq	$32, %r15
	movl	$8, %edi
	movl	$8, %esi
	movl	$.L.str.71, %edx
	movl	$2, %ecx
	movl	$.L.str.22, %r9d
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
.LBB1_949:                              # %.loopexit2585.thread
	movq	80(%rsp), %r8           # 8-byte Reload
	addq	$32, %r8
	testq	%r13, %r13
	je	.LBB1_1080
# BB#950:
	addq	$64, %r13
	movl	$8, %edi
	movl	$11, %esi
	movl	$.L.str.74, %edx
	movl	$2, %ecx
	movl	$.L.str.70, %r9d
	movl	$0, %eax
	pushq	%r13
.Lcfi425:
	.cfi_adjust_cfa_offset 8
	pushq	112(%rsp)               # 8-byte Folded Reload
.Lcfi426:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi427:
	.cfi_adjust_cfa_offset -16
	movq	152(%rsp), %r15         # 8-byte Reload
.LBB1_951:
	movq	24(%r15), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_953
# BB#952:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_953:
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	88(%rsp), %r14          # 8-byte Reload
	movq	784(%rsp), %rbx
	je	.LBB1_955
# BB#954:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_955:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	%rdi, zz_hold(%rip)
	movq	24(%rdi), %rax
	cmpq	%rdi, %rax
	je	.LBB1_1110
# BB#956:
	movq	16(%rdi), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rdi), %rcx
	movq	%rax, 24(%rcx)
	movq	%rdi, 24(%rdi)
	movq	%rdi, 16(%rdi)
	movq	%rax, xx_tmp(%rip)
	movq	%r15, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_1088
# BB#957:
	testq	%rax, %rax
	je	.LBB1_1088
# BB#958:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%r15), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r15), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%r15)
	movq	%r15, 24(%rcx)
	jmp	.LBB1_1088
.LBB1_959:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_960:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_963
# BB#961:
	testq	%rax, %rax
	je	.LBB1_963
# BB#962:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_963:
	movq	%rax, zz_res(%rip)
	movq	120(%rsp), %r12
	movq	%r12, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_966
# BB#964:
	testq	%r12, %r12
	je	.LBB1_966
# BB#965:
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_966:
	subq	$8, %rsp
.Lcfi428:
	.cfi_adjust_cfa_offset 8
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi429:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi430:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi431:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi432:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi433:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi434:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_970
# BB#967:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB1_971
# BB#968:
	testq	%rax, %rax
	je	.LBB1_971
# BB#969:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r12), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%r12)
	movq	%r12, 24(%rcx)
	jmp	.LBB1_971
.LBB1_970:                              # %.thread3289
	movq	$0, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_971:
	movq	%r13, %rdi
	callq	DisposeObject
	testq	%rbx, %rbx
	jne	.LBB1_1124
	jmp	.LBB1_1126
.LBB1_972:                              # %.loopexit2585.thread.thread
	leaq	32(%r15), %rax
	addq	$64, %r15
	movq	%rax, %r8
	jmp	.LBB1_1081
.LBB1_973:                              # %.thread3293
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	movq	784(%rsp), %rbp
	movq	%rbp, %r12
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB1_974:
	movq	%r13, %rdi
	callq	DisposeObject
	subq	$8, %rsp
.Lcfi435:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi436:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi437:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi438:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi439:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi440:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi441:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	jmp	.LBB1_1126
.LBB1_975:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_976:
	movb	$17, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_980
# BB#977:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_981
.LBB1_978:                              # %.thread3224
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB1_979:
	movq	%r13, %rdi
	callq	DisposeObject
	subq	$8, %rsp
.Lcfi442:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	pushq	$0
.Lcfi443:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi444:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi445:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi446:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi447:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi448:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	jmp	.LBB1_1126
.LBB1_980:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_981:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_984
# BB#982:
	testq	%rax, %rax
	je	.LBB1_984
# BB#983:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_984:
	movq	%r14, %r15
	movq	%rax, zz_res(%rip)
	movq	120(%rsp), %rsi
	movq	%rsi, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_987
# BB#985:
	testq	%rsi, %rsi
	je	.LBB1_987
# BB#986:
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_987:
	leaq	200(%rsp), %r8
	xorl	%edx, %edx
	movq	%rbp, %rdi
	movq	%r13, %rcx
	callq	ClosureExpand
	movq	%rax, %r14
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_989
# BB#988:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_990
.LBB1_989:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_990:
	movb	$17, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_992
# BB#991:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_993
.LBB1_992:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_993:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_996
# BB#994:
	testq	%rax, %rax
	je	.LBB1_996
# BB#995:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_996:
	movq	%rax, zz_res(%rip)
	movq	200(%rsp), %rsi
	movq	%rsi, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_999
# BB#997:
	testq	%rsi, %rsi
	je	.LBB1_999
# BB#998:
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_999:
	subq	$8, %rsp
.Lcfi449:
	.cfi_adjust_cfa_offset 8
	movq	%r14, %rdi
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	pushq	$0
.Lcfi450:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi451:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi452:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi453:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi454:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi455:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	movq	%rbx, %rdi
	callq	DisposeObject
	movq	%rbp, %rdi
	jmp	.LBB1_1125
.LBB1_1000:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r12
	movq	%r12, zz_hold(%rip)
.LBB1_1001:
	movq	%rbx, %rbp
	movb	$8, 32(%r12)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%r12, 8(%r12)
	movq	%r12, (%r12)
	movzwl	34(%r13), %eax
	movw	%ax, 34(%r12)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r13), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%r12), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%r12)
	andl	36(%r13), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r12)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, 80(%r12)
	movq	$0, 128(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%r12)
	movzbl	43(%rax), %eax
	movzwl	42(%r12), %ecx
	andl	$32, %eax
	andl	$65149, %ecx            # imm = 0xFE7D
	movq	$0, 96(%r12)
	leal	128(%rcx,%rax,8), %eax
	movw	%ax, 42(%r12)
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_1008
# BB#1002:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%r12, %r12
	sete	%bl
	je	.LBB1_1009
# BB#1003:
	testq	%rax, %rax
	je	.LBB1_1009
# BB#1004:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r12), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%r12)
	movq	%r12, 24(%rcx)
	jmp	.LBB1_1009
.LBB1_1005:
	movq	$0, (%rcx)
.LBB1_1006:
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	leaq	208(%rsp), %r8
	movl	$1, %edx
	movq	%r13, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r12, %rcx
	callq	ClosureExpand
	movq	%rax, %r14
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_1089
# BB#1007:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_1090
.LBB1_1008:                             # %.thread3220
	movq	$0, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	testq	%r12, %r12
	sete	%bl
.LBB1_1009:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_1011
# BB#1010:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_1012
.LBB1_1011:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_1012:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%rax, %rax
	sete	%cl
	orb	%cl, %bl
	jne	.LBB1_1014
# BB#1013:
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_1014:
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%rax, %rax
	movq	%rbp, %rbx
	je	.LBB1_1016
# BB#1015:
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_1016:
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	AttachEnv
	movq	%r12, %rdi
	callq	SetTarget
	movq	32(%rsp), %rax          # 8-byte Reload
	testb	$32, 126(%rax)
	jne	.LBB1_1018
# BB#1017:
	xorl	%eax, %eax
	jmp	.LBB1_1019
.LBB1_1018:
	movq	%r12, %rdi
	callq	BuildEnclose
.LBB1_1019:
	movq	%rax, 136(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%r12)
	movb	$1, %al
	cmpq	$0, (%r14)
	jne	.LBB1_1021
# BB#1020:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, (%rax)
	setne	%al
.LBB1_1021:
	movzbl	%al, %eax
	movzwl	42(%r12), %ecx
	andl	$65531, %ecx            # imm = 0xFFFB
	leal	(%rcx,%rax,4), %eax
	movw	%ax, 42(%r12)
	cmpq	$0, 8(%r14)
	jne	.LBB1_1025
# BB#1022:
	cmpq	$0, (%r14)
	jne	.LBB1_1025
# BB#1023:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, 8(%rax)
	jne	.LBB1_1025
# BB#1024:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, (%rax)
	je	.LBB1_1123
.LBB1_1025:
	movq	%r12, %rdi
.LBB1_1026:
	movq	%r14, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	insert_split
	movq	%rax, %r12
	testq	%rbx, %rbx
	jne	.LBB1_1124
	jmp	.LBB1_1126
.LBB1_1027:
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpq	%r14, %rax
	je	.LBB1_1036
# BB#1028:
	movq	%rax, %rbx
	cmpb	$0, 32(%rax)
	je	.LBB1_1030
# BB#1029:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_1030:
	movq	%rbx, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB1_1033
# BB#1031:
	movq	(%r14), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%r14)
.LBB1_1032:
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_1033:
	movq	%rbx, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_1036
# BB#1034:
	movq	(%r15), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
.LBB1_1035:
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_1036:
	movq	%r13, %r12
.LBB1_1037:
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	%rdi, zz_hold(%rip)
	movq	24(%rdi), %rax
	cmpq	%rdi, %rax
	je	.LBB1_1041
# BB#1038:
	movq	16(%rdi), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rdi), %rcx
	movq	%rax, 24(%rcx)
	movq	%rdi, 24(%rdi)
	movq	%rdi, 16(%rdi)
	movq	%rax, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%r12, %r12
	movq	88(%rsp), %rbx          # 8-byte Reload
	je	.LBB1_1042
# BB#1039:
	testq	%rax, %rax
	je	.LBB1_1042
# BB#1040:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r12), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%r12)
	movq	%r12, 24(%rcx)
	jmp	.LBB1_1042
.LBB1_1041:                             # %.thread3273
	movq	$0, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	movq	88(%rsp), %rbx          # 8-byte Reload
.LBB1_1042:
	callq	DisposeObject
	cmpq	$0, 8(%rbx)
	jne	.LBB1_1046
# BB#1043:
	cmpq	$0, (%rbx)
	jne	.LBB1_1046
# BB#1044:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, 8(%rax)
	jne	.LBB1_1046
# BB#1045:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, (%rax)
	je	.LBB1_1126
.LBB1_1046:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	jmp	.LBB1_520
.LBB1_1047:
	movb	$12, 32(%rbp)
	movq	%rbp, %rdi
	callq	StringQuotedWord
	movq	%rax, %rbx
	movl	40(%rbp), %edi
	andl	$4095, %edi             # imm = 0xFFF
	callq	FontFamilyAndFace
	movq	%rax, %rbp
	movl	$8, %edi
	movl	$32, %esi
	movl	$.L.str.26, %edx
	movl	$2, %ecx
	movl	$.L.str.24, %r9d
	movl	$0, %eax
	movq	%r15, %r8
	pushq	%rbp
.Lcfi456:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi457:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi458:
	.cfi_adjust_cfa_offset -16
.LBB1_1048:
	movl	$11, %edi
	movl	$.L.str.8, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, %rbx
.LBB1_1049:
	movq	56(%rsp), %r15          # 8-byte Reload
.LBB1_1050:
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_1054
# BB#1051:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_1055
# BB#1052:
	testq	%rax, %rax
	je	.LBB1_1055
# BB#1053:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB1_1055
.LBB1_1054:                             # %.thread3257
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_1055:
	movq	%r13, %rdi
	callq	DisposeObject
	subq	$8, %rsp
.Lcfi459:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi460:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi461:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi462:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi463:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi464:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi465:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	jmp	.LBB1_1126
.LBB1_1056:
	xorl	%eax, %eax
.LBB1_1057:
	movq	%rax, xx_tmp(%rip)
	movq	808(%rsp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_1060
# BB#1058:
	testq	%rcx, %rcx
	je	.LBB1_1060
# BB#1059:
	movq	16(%rax), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rcx), %rsi
	movq	%rsi, 16(%rax)
	movq	16(%rcx), %rsi
	movq	%rax, 24(%rsi)
	movq	%rdx, 16(%rcx)
	movq	%rcx, 24(%rdx)
.LBB1_1060:
	movq	%r12, %rbp
	movq	808(%rsp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
.LBB1_1061:                             # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB1_1061
# BB#1062:
	movq	8(%rax), %rax
	addq	$16, %rax
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB1_1063:                             # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rbx
	leaq	16(%rbx), %rax
	cmpb	$0, 32(%rbx)
	je	.LBB1_1063
# BB#1064:
	movq	8(%rbx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_1066
# BB#1065:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_1067
.LBB1_1066:
	xorl	%ecx, %ecx
.LBB1_1067:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_1069
# BB#1068:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_1069:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_1071
# BB#1070:
	callq	DisposeObject
.LBB1_1071:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_1073
# BB#1072:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_1074
.LBB1_1073:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_1074:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_1076
# BB#1075:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_1076:
	movq	%rbp, %rbx
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB1_1079
# BB#1077:
	testq	%rax, %rax
	je	.LBB1_1079
# BB#1078:
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_1079:
	movq	808(%rsp), %rax
	movq	%rax, %rbp
	movq	(%rbp), %rdi
	movq	$0, (%rbp)
	subq	$8, %rsp
.Lcfi466:
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi467:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi468:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi469:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi470:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi471:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi472:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	jmp	.LBB1_1126
.LBB1_1080:
	movq	112(%rsp), %r13         # 8-byte Reload
	movq	88(%rsp), %r14          # 8-byte Reload
	movq	104(%rsp), %r15         # 8-byte Reload
.LBB1_1081:
	subq	$8, %rsp
.Lcfi473:
	.cfi_adjust_cfa_offset 8
	movl	$8, %edi
	movl	$12, %esi
	movl	$.L.str.75, %edx
	movl	$2, %ecx
	movl	$.L.str.70, %r9d
	xorl	%eax, %eax
	pushq	%r15
.Lcfi474:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi475:
	.cfi_adjust_cfa_offset -16
.LBB1_1082:
	movl	$11, %edi
	movl	$.L.str.8, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, %r15
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	movq	784(%rsp), %rbx
	je	.LBB1_1086
# BB#1083:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%r15, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_1087
# BB#1084:
	testq	%rax, %rax
	je	.LBB1_1087
# BB#1085:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%r15), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r15), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%r15)
	movq	%r15, 24(%rcx)
	jmp	.LBB1_1087
.LBB1_1086:                             # %.thread3244
	movq	$0, xx_tmp(%rip)
	movq	%r15, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_1087:
	movq	%r13, %rdi
.LBB1_1088:
	callq	DisposeObject
	subq	$8, %rsp
.Lcfi476:
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi477:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi478:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi479:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi480:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi481:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi482:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	jmp	.LBB1_1126
.LBB1_1089:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_1090:
	movb	$17, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	movq	8(%rsp), %r12           # 8-byte Reload
	je	.LBB1_1092
# BB#1091:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_1093
.LBB1_1092:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_1093:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_1096
# BB#1094:
	testq	%rax, %rax
	je	.LBB1_1096
# BB#1095:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_1096:
	movq	%rax, zz_res(%rip)
	movq	208(%rsp), %rsi
	movq	%rsi, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_1099
# BB#1097:
	testq	%rsi, %rsi
	je	.LBB1_1099
# BB#1098:
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_1099:
	cmpb	$57, 32(%r14)
	jne	.LBB1_1106
# BB#1100:
	cmpb	$-110, 32(%rbp)
	je	.LBB1_1102
# BB#1101:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.52, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_1102:
	movq	48(%rbp), %rax
	cmpq	$0, 104(%rax)
	jne	.LBB1_1104
# BB#1103:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.53, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_1104:
	movzbl	zz_lengths+2(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_1113
# BB#1105:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_1114
.LBB1_1106:
	movq	%r14, %rbp
	jmp	.LBB1_1122
.LBB1_1107:                             # %.thread3295
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	movq	784(%rsp), %rbp
	movq	%rbp, %r12
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB1_1108:
	movq	%r13, %rdi
	callq	DisposeObject
	subq	$8, %rsp
.Lcfi483:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi484:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi485:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi486:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi487:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi488:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi489:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	jmp	.LBB1_1126
.LBB1_1109:
	xorl	%ebx, %ebx
	jmp	.LBB1_338
.LBB1_1110:                             # %.thread3253
	movq	$0, xx_tmp(%rip)
	movq	%r15, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	jmp	.LBB1_1088
.LBB1_1111:                             # %.thread3291
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	movq	784(%rsp), %rbp
	movq	%rbp, %r12
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB1_1112:
	movq	%r13, %rdi
	callq	DisposeObject
	subq	$8, %rsp
.Lcfi490:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi491:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi492:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi493:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi494:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi495:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi496:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	jmp	.LBB1_1126
.LBB1_1113:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_1114:
	movb	$2, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movzwl	34(%r14), %eax
	movw	%ax, 34(%rbp)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r14), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbp), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbp)
	andl	36(%r14), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbp)
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	48(%r13), %rax
	movq	104(%rax), %rax
	movq	%rax, 80(%rbp)
	movq	%r14, %rdi
	callq	FilterSetFileNames
	subq	$8, %rsp
.Lcfi497:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi498:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi499:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi500:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi501:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi502:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi503:
	.cfi_adjust_cfa_offset -48
	movl	$1, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %r12
	movb	32(%r12), %al
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB1_1116
# BB#1115:
	leaq	32(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	48(%r13), %rdi
	callq	SymName
	movq	%rax, %rbp
	movl	$8, %edi
	movl	$19, %esi
	movl	$.L.str.54, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	%rbp, %r9
	callq	Error
.LBB1_1116:
	movq	%r12, %rsi
	addq	$64, %rsi
	movq	208(%rsp), %rdx
	movq	%r14, %rdi
	callq	FilterExecute
	movq	%rax, %rbp
	movq	%r12, %rdi
	callq	DisposeObject
	movq	%r14, zz_hold(%rip)
	movq	24(%r14), %rax
	cmpq	%r14, %rax
	je	.LBB1_1120
# BB#1117:
	movq	16(%r14), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r14), %rcx
	movq	%rax, 24(%rcx)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%rax, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_1121
# BB#1118:
	testq	%rax, %rax
	je	.LBB1_1121
# BB#1119:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbp), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbp)
	movq	%rbp, 24(%rcx)
	jmp	.LBB1_1121
.LBB1_1120:                             # %.thread3222
	movq	$0, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_1121:
	movq	%r14, %rdi
	callq	DisposeObject
	movq	208(%rsp), %rsi
.LBB1_1122:
	movq	24(%rsp), %r9           # 8-byte Reload
	subq	$8, %rsp
.Lcfi504:
	.cfi_adjust_cfa_offset 8
	movq	%rbp, %rdi
	movq	%r15, %rdx
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi505:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi506:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi507:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi508:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi509:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi510:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	movq	%rbx, %rdi
	callq	DisposeObject
	movq	80(%rsp), %rbx          # 8-byte Reload
.LBB1_1123:
	testq	%rbx, %rbx
	je	.LBB1_1126
.LBB1_1124:
	movq	%rbx, %rdi
.LBB1_1125:                             # %ManifestTg.exit
	callq	DisposeObject
.LBB1_1126:                             # %ManifestTg.exit
	decl	Manifest.depth(%rip)
.LBB1_1127:
	movq	%r12, %rax
	addq	$728, %rsp              # imm = 0x2D8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_1128:                             # %.thread3299
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	movq	784(%rsp), %rbp
	movq	%rbp, %r12
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB1_1129:
	movq	%r13, %rdi
	callq	DisposeObject
	subq	$8, %rsp
.Lcfi511:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi512:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi513:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi514:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi515:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi516:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi517:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	jmp	.LBB1_1126
.LBB1_1130:
	xorl	%ebx, %ebx
	jmp	.LBB1_338
.LBB1_1131:
	movb	$-122, %al
	jmp	.LBB1_1135
.LBB1_1132:
	xorl	%ebx, %ebx
	jmp	.LBB1_338
.LBB1_1133:
	movq	112(%rsp), %r12         # 8-byte Reload
	jmp	.LBB1_1126
.LBB1_1134:
	movb	$127, %al
.LBB1_1135:
	movq	56(%rsp), %r15          # 8-byte Reload
.LBB1_1136:
	movb	%al, 41(%r12)
	movq	(%r13), %rax
	addq	$16, %rax
.LBB1_1137:                             # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB1_1137
# BB#1138:
	subq	$8, %rsp
.Lcfi518:
	.cfi_adjust_cfa_offset 8
	movl	$nbt, %ecx
	movl	$nft, %r8d
	movl	$ntarget, %r9d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi519:
	.cfi_adjust_cfa_offset 8
	pushq	$nenclose
.Lcfi520:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi521:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi522:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi523:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi524:
	.cfi_adjust_cfa_offset -48
	movl	$1, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %r8
	movb	32(%r8), %al
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB1_1143
# BB#1139:
	movq	%rbp, zz_hold(%rip)
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB1_1148
# BB#1140:
	movq	16(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rbp), %rcx
	movq	%rax, 24(%rcx)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rax, xx_tmp(%rip)
	movq	%r8, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%r8, %r8
	je	.LBB1_1149
# BB#1141:
	testq	%rax, %rax
	je	.LBB1_1149
# BB#1142:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%r8), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r8), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%r8)
	movq	%r8, 24(%rcx)
	jmp	.LBB1_1149
.LBB1_1143:
	addq	$32, %r8
	movl	$8, %edi
	movl	$17, %esi
	movl	$.L.str.86, %edx
	movl	$2, %ecx
	movl	$.L.str.78, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	$11, %edi
	movl	$.L.str.8, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, %rbx
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_1153
# BB#1144:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	movq	784(%rsp), %rbp
	movq	%rbp, %r12
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB1_1154
# BB#1145:
	testq	%rax, %rax
	je	.LBB1_1154
# BB#1146:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB1_1154
.LBB1_1147:
	movb	$-128, %al
	jmp	.LBB1_1136
.LBB1_1148:                             # %.thread3303
	movq	$0, xx_tmp(%rip)
	movq	%r8, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_1149:
	movq	%rbp, %rdi
	callq	DisposeObject
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_1155
# BB#1150:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB1_1156
# BB#1151:
	testq	%rax, %rax
	je	.LBB1_1156
# BB#1152:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r12), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%r12)
	movq	%r12, 24(%rcx)
	jmp	.LBB1_1156
.LBB1_1153:                             # %.thread3301
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	movq	784(%rsp), %rbp
	movq	%rbp, %r12
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB1_1154:
	movq	%r13, %rdi
	callq	DisposeObject
	subq	$8, %rsp
.Lcfi525:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi526:
	.cfi_adjust_cfa_offset 8
	pushq	824(%rsp)
.Lcfi527:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi528:
	.cfi_adjust_cfa_offset 8
	movl	824(%rsp), %eax
	pushq	%rax
.Lcfi529:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi530:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi531:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r12
	jmp	.LBB1_1126
.LBB1_1155:                             # %.thread3305
	movq	$0, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB1_1156:
	movq	%r13, %rdi
	callq	DisposeObject
	cmpq	$0, 8(%r14)
	jne	.LBB1_1160
# BB#1157:
	cmpq	$0, (%r14)
	jne	.LBB1_1160
# BB#1158:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, 8(%rax)
	jne	.LBB1_1160
# BB#1159:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$0, (%rax)
	je	.LBB1_1126
.LBB1_1160:
	movq	%r12, %rdi
	jmp	.LBB1_519
.LBB1_1161:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_1162:
	movq	32(%rsp), %rbp          # 8-byte Reload
	movb	$17, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_1164
# BB#1163:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_1165
.LBB1_1164:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_1165:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_1168
# BB#1166:
	testq	%rax, %rax
	je	.LBB1_1168
# BB#1167:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_1168:
	movq	%rax, zz_res(%rip)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB1_338
# BB#1169:
	testq	%rax, %rax
	je	.LBB1_338
# BB#1170:
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	jmp	.LBB1_338
.LBB1_1171:
	movq	%r13, %r12
	testq	%rbx, %rbx
	jne	.LBB1_1124
	jmp	.LBB1_1126
.LBB1_923:
	movq	(%r15), %rax
	addq	$16, %rax
.LBB1_924:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %r15
	leaq	16(%r15), %rax
	cmpb	$0, 32(%r15)
	je	.LBB1_924
.LBB1_925:
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB1_942:                              # %.loopexit2585
	testq	%r15, %r15
	jne	.LBB1_951
	jmp	.LBB1_949
.Lfunc_end1:
	.size	Manifest, .Lfunc_end1-Manifest
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_145
	.quad	.LBB1_161
	.quad	.LBB1_162
	.quad	.LBB1_166
	.quad	.LBB1_37
	.quad	.LBB1_37
	.quad	.LBB1_161
	.quad	.LBB1_161
	.quad	.LBB1_161
	.quad	.LBB1_48
	.quad	.LBB1_48
	.quad	.LBB1_161
	.quad	.LBB1_161
	.quad	.LBB1_161
	.quad	.LBB1_161
	.quad	.LBB1_715
	.quad	.LBB1_52
	.quad	.LBB1_52
	.quad	.LBB1_62
	.quad	.LBB1_67
	.quad	.LBB1_62
	.quad	.LBB1_67
	.quad	.LBB1_126
	.quad	.LBB1_126
	.quad	.LBB1_69
	.quad	.LBB1_69
	.quad	.LBB1_74
	.quad	.LBB1_74
	.quad	.LBB1_4
	.quad	.LBB1_4
	.quad	.LBB1_4
	.quad	.LBB1_4
	.quad	.LBB1_167
	.quad	.LBB1_172
	.quad	.LBB1_126
	.quad	.LBB1_126
	.quad	.LBB1_126
	.quad	.LBB1_126
	.quad	.LBB1_126
	.quad	.LBB1_126
	.quad	.LBB1_4
	.quad	.LBB1_4
	.quad	.LBB1_4
	.quad	.LBB1_514
	.quad	.LBB1_514
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_177
	.quad	.LBB1_182
	.quad	.LBB1_187
	.quad	.LBB1_81
	.quad	.LBB1_81
	.quad	.LBB1_194
	.quad	.LBB1_195
	.quad	.LBB1_161
	.quad	.LBB1_199
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_7
	.quad	.LBB1_205
	.quad	.LBB1_7
	.quad	.LBB1_16
	.quad	.LBB1_7
	.quad	.LBB1_211
	.quad	.LBB1_84
	.quad	.LBB1_84
	.quad	.LBB1_87
	.quad	.LBB1_87
	.quad	.LBB1_19
	.quad	.LBB1_19
	.quad	.LBB1_19
	.quad	.LBB1_213
	.quad	.LBB1_229
	.quad	.LBB1_245
	.quad	.LBB1_89
	.quad	.LBB1_89
	.quad	.LBB1_250
	.quad	.LBB1_161
	.quad	.LBB1_161
	.quad	.LBB1_161
	.quad	.LBB1_161
	.quad	.LBB1_161
	.quad	.LBB1_161
	.quad	.LBB1_161
	.quad	.LBB1_161
	.quad	.LBB1_161
	.quad	.LBB1_161
	.quad	.LBB1_256
	.quad	.LBB1_269
	.quad	.LBB1_98
	.quad	.LBB1_98
	.quad	.LBB1_105
	.quad	.LBB1_105
	.quad	.LBB1_110
	.quad	.LBB1_110
.LJTI1_1:
	.quad	.LBB1_15
	.quad	.LBB1_289
	.quad	.LBB1_290
	.quad	.LBB1_291
	.quad	.LBB1_292
	.quad	.LBB1_295
	.quad	.LBB1_293
	.quad	.LBB1_295
	.quad	.LBB1_294
.LJTI1_2:
	.quad	.LBB1_805
	.quad	.LBB1_807
	.quad	.LBB1_838
	.quad	.LBB1_809
	.quad	.LBB1_822

	.text
	.p2align	4, 0x90
	.type	insert_split,@function
insert_split:                           # @insert_split
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi532:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi533:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi534:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi535:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi536:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi537:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi538:
	.cfi_def_cfa_offset 80
.Lcfi539:
	.cfi_offset %rbx, -56
.Lcfi540:
	.cfi_offset %r12, -48
.Lcfi541:
	.cfi_offset %r13, -40
.Lcfi542:
	.cfi_offset %r14, -32
.Lcfi543:
	.cfi_offset %r15, -24
.Lcfi544:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r8
	cmpb	$9, 32(%r8)
	movq	%r8, 8(%rsp)            # 8-byte Spill
	jne	.LBB2_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.55, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB2_2:
	movzbl	zz_lengths+9(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r14
	testq	%r14, %r14
	je	.LBB2_3
# BB#4:
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_5
.LBB2_3:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB2_5:
	movb	$9, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movzwl	34(%r8), %eax
	movw	%ax, 34(%r14)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r8), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%r14), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%r14)
	andl	36(%r8), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r14)
	movq	%r8, zz_hold(%rip)
	movq	24(%r8), %rax
	cmpq	%r8, %rax
	je	.LBB2_6
# BB#7:
	movq	16(%r8), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r8), %rcx
	movq	%rax, 24(%rcx)
	movq	%r8, 24(%r8)
	movq	%r8, 16(%r8)
	movq	%rax, xx_tmp(%rip)
	movq	%r14, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%r14, %r14
	sete	7(%rsp)                 # 1-byte Folded Spill
	je	.LBB2_10
# BB#8:
	testq	%rax, %rax
	je	.LBB2_10
# BB#9:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%r14), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r14), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%r14)
	movq	%r14, 24(%rcx)
	jmp	.LBB2_10
.LBB2_6:                                # %.thread
	movq	$0, xx_tmp(%rip)
	movq	%r14, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	testq	%r14, %r14
	sete	7(%rsp)                 # 1-byte Folded Spill
.LBB2_10:                               # %.preheader
	xorl	%ebp, %ebp
	movl	$30, %esi
	.p2align	4, 0x90
.LBB2_11:                               # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%r12,%rbp)
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	jne	.LBB2_13
# BB#12:                                #   in Loop: Header=BB2_11 Depth=1
	cmpq	$0, (%r15,%rbp)
	je	.LBB2_52
.LBB2_13:                               #   in Loop: Header=BB2_11 Depth=1
	movq	%r12, %r13
	movq	%r15, %r12
	cmpq	$1, %rbp
	movl	$15, %r15d
	adcl	$0, %r15d
	movzbl	zz_lengths(%r15), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB2_14
# BB#15:                                #   in Loop: Header=BB2_11 Depth=1
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_16
	.p2align	4, 0x90
.LBB2_14:                               #   in Loop: Header=BB2_11 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB2_16:                               #   in Loop: Header=BB2_11 Depth=1
	movb	%r15b, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movb	$0, 41(%rbx)
	movl	$0, (%rbx,%rsi,2)
	movl	$0, -8(%rbx,%rsi,2)
	movzwl	34(%r8), %eax
	movw	%ax, 34(%rbx)
	movl	36(%r8), %eax
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	%ecx, %eax
	movl	36(%rbx), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movl	36(%r8), %ecx
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_17
# BB#18:                                #   in Loop: Header=BB2_11 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_19
	.p2align	4, 0x90
.LBB2_17:                               #   in Loop: Header=BB2_11 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	%rax, zz_hold(%rip)
.LBB2_19:                               #   in Loop: Header=BB2_11 Depth=1
	movq	%r12, %r15
	movq	%r13, %r12
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%rax, %rax
	sete	%cl
	orb	7(%rsp), %cl            # 1-byte Folded Reload
	jne	.LBB2_21
# BB#20:                                #   in Loop: Header=BB2_11 Depth=1
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_21:                               #   in Loop: Header=BB2_11 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB2_24
# BB#22:                                #   in Loop: Header=BB2_11 Depth=1
	testq	%rax, %rax
	je	.LBB2_24
# BB#23:                                #   in Loop: Header=BB2_11 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_24:                               #   in Loop: Header=BB2_11 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_25
# BB#26:                                #   in Loop: Header=BB2_11 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_27
	.p2align	4, 0x90
.LBB2_25:                               #   in Loop: Header=BB2_11 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	%rax, zz_hold(%rip)
.LBB2_27:                               #   in Loop: Header=BB2_11 Depth=1
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB2_30
# BB#28:                                #   in Loop: Header=BB2_11 Depth=1
	testq	%rax, %rax
	je	.LBB2_30
# BB#29:                                #   in Loop: Header=BB2_11 Depth=1
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_30:                               #   in Loop: Header=BB2_11 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r8, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_32
# BB#31:                                #   in Loop: Header=BB2_11 Depth=1
	movq	16(%r8), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r8)
	movq	16(%rax), %rdx
	movq	%r8, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_32:                               #   in Loop: Header=BB2_11 Depth=1
	cmpq	$0, (%r12,%rbp)
	je	.LBB2_42
# BB#33:                                #   in Loop: Header=BB2_11 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_34
# BB#35:                                #   in Loop: Header=BB2_11 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_36
	.p2align	4, 0x90
.LBB2_34:                               #   in Loop: Header=BB2_11 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	%rax, zz_hold(%rip)
.LBB2_36:                               #   in Loop: Header=BB2_11 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%r12,%rbp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_39
# BB#37:                                #   in Loop: Header=BB2_11 Depth=1
	testq	%rcx, %rcx
	je	.LBB2_39
# BB#38:                                #   in Loop: Header=BB2_11 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_39:                               #   in Loop: Header=BB2_11 Depth=1
	testq	%rbx, %rbx
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB2_42
# BB#40:                                #   in Loop: Header=BB2_11 Depth=1
	testq	%rax, %rax
	je	.LBB2_42
# BB#41:                                #   in Loop: Header=BB2_11 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_42:                               #   in Loop: Header=BB2_11 Depth=1
	cmpq	$0, (%r15,%rbp)
	je	.LBB2_60
# BB#43:                                #   in Loop: Header=BB2_11 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_44
# BB#45:                                #   in Loop: Header=BB2_11 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_46
	.p2align	4, 0x90
.LBB2_44:                               #   in Loop: Header=BB2_11 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	%rax, zz_hold(%rip)
.LBB2_46:                               #   in Loop: Header=BB2_11 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%r15,%rbp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_49
# BB#47:                                #   in Loop: Header=BB2_11 Depth=1
	testq	%rcx, %rcx
	je	.LBB2_49
# BB#48:                                #   in Loop: Header=BB2_11 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_49:                               #   in Loop: Header=BB2_11 Depth=1
	testq	%rbx, %rbx
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB2_60
# BB#50:                                #   in Loop: Header=BB2_11 Depth=1
	testq	%rax, %rax
	je	.LBB2_60
# BB#51:                                #   in Loop: Header=BB2_11 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	jmp	.LBB2_59
.LBB2_52:                               #   in Loop: Header=BB2_11 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_53
# BB#54:                                #   in Loop: Header=BB2_11 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_55
.LBB2_53:                               #   in Loop: Header=BB2_11 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	%rax, zz_hold(%rip)
.LBB2_55:                               #   in Loop: Header=BB2_11 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%rax, %rax
	sete	%cl
	orb	7(%rsp), %cl            # 1-byte Folded Reload
	jne	.LBB2_57
# BB#56:                                #   in Loop: Header=BB2_11 Depth=1
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_57:                               #   in Loop: Header=BB2_11 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r8, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_60
# BB#58:                                #   in Loop: Header=BB2_11 Depth=1
	movq	16(%r8), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r8)
	movq	16(%rax), %rdx
	movq	%r8, 24(%rdx)
.LBB2_59:                               #   in Loop: Header=BB2_11 Depth=1
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_60:                               #   in Loop: Header=BB2_11 Depth=1
	addq	$8, %rbp
	addq	$-2, %rsi
	cmpq	$16, %rbp
	jne	.LBB2_11
# BB#61:
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	insert_split, .Lfunc_end2-insert_split
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	1065353216              # float 1
.LCPI3_2:
	.long	1120403456              # float 100
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_1:
	.quad	4576918229304087675     # double 0.01
	.text
	.p2align	4, 0x90
	.type	GetScaleFactor,@function
GetScaleFactor:                         # @GetScaleFactor
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi545:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi546:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi547:
	.cfi_def_cfa_offset 32
.Lcfi548:
	.cfi_offset %rbx, -24
.Lcfi549:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	32(%rbx), %r14
	movb	32(%rbx), %al
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB3_1
# BB#2:
	addq	$64, %rbx
	leaq	4(%rsp), %rdx
	movl	$.L.str.65, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB3_3
# BB#4:
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm1
	movsd	.LCPI3_1(%rip), %xmm2   # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	jbe	.LBB3_6
# BB#5:
	movl	$8, %edi
	movl	$5, %esi
	movl	$.L.str.67, %edx
	jmp	.LBB3_8
.LBB3_1:
	movl	$8, %edi
	movl	$3, %esi
	movl	$.L.str.64, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
	jmp	.LBB3_9
.LBB3_3:
	movl	$8, %edi
	movl	$4, %esi
	movl	$.L.str.66, %edx
	jmp	.LBB3_8
.LBB3_6:
	ucomiss	.LCPI3_2(%rip), %xmm0
	jbe	.LBB3_10
# BB#7:
	movl	$8, %edi
	movl	$6, %esi
	movl	$.L.str.68, %edx
.LBB3_8:
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbx, %r9
	callq	Error
.LBB3_9:
	movl	$1065353216, 4(%rsp)    # imm = 0x3F800000
	movss	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB3_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	GetScaleFactor, .Lfunc_end3-GetScaleFactor
	.cfi_endproc

	.p2align	4, 0x90
	.type	SetUnderline,@function
SetUnderline:                           # @SetUnderline
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi550:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi551:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi552:
	.cfi_def_cfa_offset 32
.Lcfi553:
	.cfi_offset %rbx, -24
.Lcfi554:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpb	$17, 32(%r14)
	jne	.LBB4_6
# BB#1:                                 # %.preheader13
	movq	8(%r14), %rbx
	cmpq	%r14, %rbx
	jne	.LBB4_3
	jmp	.LBB4_6
	.p2align	4, 0x90
.LBB4_5:                                #   in Loop: Header=BB4_3 Depth=1
	callq	SetUnderline
	movq	8(%rbx), %rbx
	cmpq	%r14, %rbx
	je	.LBB4_6
.LBB4_3:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB4_4:                                #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB4_4
	jmp	.LBB4_5
.LBB4_6:                                # %.loopexit
	movl	$-1610612737, %eax      # imm = 0x9FFFFFFF
	andl	40(%r14), %eax
	orl	$1073741824, %eax       # imm = 0x40000000
	movl	%eax, 40(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	SetUnderline, .Lfunc_end4-SetUnderline
	.cfi_endproc

	.type	ReplaceWithTidy.buff,@object # @ReplaceWithTidy.buff
	.local	ReplaceWithTidy.buff
	.comm	ReplaceWithTidy.buff,512,16
	.type	ReplaceWithTidy.buff_len,@object # @ReplaceWithTidy.buff_len
	.local	ReplaceWithTidy.buff_len
	.comm	ReplaceWithTidy.buff_len,4,4
	.type	ReplaceWithTidy.buff_pos,@object # @ReplaceWithTidy.buff_pos
	.local	ReplaceWithTidy.buff_pos
	.comm	ReplaceWithTidy.buff_pos,8,4
	.type	ReplaceWithTidy.buff_typ,@object # @ReplaceWithTidy.buff_typ
	.local	ReplaceWithTidy.buff_typ
	.comm	ReplaceWithTidy.buff_typ,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"TransferLinks: start_link!"
	.size	.L.str.1, 27

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"word is too long"
	.size	.L.str.2, 17

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" "
	.size	.L.str.3, 2

	.type	Manifest.depth,@object  # @Manifest.depth
	.local	Manifest.depth
	.comm	Manifest.depth,4,4
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"maximum depth of symbol expansion (%d) reached"
	.size	.L.str.4, 47

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Manifest/ENV_OBJ: res_env!"
	.size	.L.str.5, 27

	.type	nbt,@object             # @nbt
	.local	nbt
	.comm	nbt,16,16
	.type	nft,@object             # @nft
	.local	nft
	.comm	nft,16,16
	.type	ntarget,@object         # @ntarget
	.local	ntarget
	.comm	ntarget,8,8
	.type	nenclose,@object        # @nenclose
	.local	nenclose
	.comm	nenclose,8,8
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Manifest: CROSS child!"
	.size	.L.str.6, 23

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Manifest/CROSS: type(x)!"
	.size	.L.str.7, 25

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.zero	1
	.size	.L.str.8, 1

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Manifest: ACAT!"
	.size	.L.str.9, 16

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Manifest ACAT: GAP_OBJ is first!"
	.size	.L.str.10, 33

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Manifest ACAT: no GAP_OBJ!"
	.size	.L.str.11, 27

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Manifest ACAT: GAP_OBJ is last!"
	.size	.L.str.12, 32

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Manifest ACAT: double GAP_OBJ!"
	.size	.L.str.13, 31

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Manifest: unexpected space_style!"
	.size	.L.str.14, 34

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Manifest/ACAT: underline(prev)!"
	.size	.L.str.15, 32

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Manifest/ACAT: underline(y)!"
	.size	.L.str.16, 29

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"word %s%s is too long"
	.size	.L.str.17, 22

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"replacing invalid left parameter of %s by 2i"
	.size	.L.str.18, 45

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"replacing invalid left parameter of %s by +0i"
	.size	.L.str.19, 46

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"replacing invalid left parameter of %s by 0d"
	.size	.L.str.20, 45

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%s not expected here"
	.size	.L.str.21, 21

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"@Yield"
	.size	.L.str.22, 7

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"%s dropped (parameter is not a simple word)"
	.size	.L.str.23, 44

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"@Char"
	.size	.L.str.24, 6

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"%s dropped (no current font at this point)"
	.size	.L.str.25, 43

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"%s dropped (character %s unknown in font %s)"
	.size	.L.str.26, 45

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"no current language at this point, using %s"
	.size	.L.str.27, 44

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"none"
	.size	.L.str.28, 5

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"no current font at this point, using %s"
	.size	.L.str.29, 40

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"%dp"
	.size	.L.str.30, 4

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Manifest: FONT!"
	.size	.L.str.31, 16

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Manifest: UNDERLINE!"
	.size	.L.str.32, 21

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"Manifest: COMMON!"
	.size	.L.str.33, 18

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"object dropped by %s: no suitable insert point"
	.size	.L.str.34, 47

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"@Insert"
	.size	.L.str.35, 8

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"%s ignored: no choices in right parameter"
	.size	.L.str.36, 42

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"@OneOf"
	.size	.L.str.37, 7

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"Manifest/NEXT: Down(x) == x!"
	.size	.L.str.38, 29

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"%d"
	.size	.L.str.39, 3

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"??"
	.size	.L.str.40, 3

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"invalid left parameter of %s"
	.size	.L.str.41, 29

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"@Open"
	.size	.L.str.42, 6

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"%s deleted (invalid right parameter)"
	.size	.L.str.43, 37

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"@IncludeGraphic"
	.size	.L.str.44, 16

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"@SysIncludeGraphic"
	.size	.L.str.45, 19

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"assert failed in %s %s"
	.size	.L.str.46, 23

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"Manifest:"
	.size	.L.str.47, 10

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"Manifest/CLOSURE: type(y) != PAR!"
	.size	.L.str.48, 34

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"this %s is not a sequence of one or more words"
	.size	.L.str.49, 47

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"%s\n"
	.size	.L.str.50, 4

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"Manifest: prntenv!"
	.size	.L.str.51, 19

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"ManifestCl/filtered: type(sym)!"
	.size	.L.str.52, 32

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"ManifestCl filter-encl!"
	.size	.L.str.53, 24

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"filter parameter of %s symbol is not simple"
	.size	.L.str.54, 44

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"ReplaceWithSplit: type(x) already SPLIT!"
	.size	.L.str.55, 41

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"Manifest/VCAT: less than two children!"
	.size	.L.str.56, 39

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"Manifest/VCAT: type(g) != GAP_OBJECT!"
	.size	.L.str.57, 38

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"Manifest/VCAT: GAP_OBJ has no child!"
	.size	.L.str.58, 37

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"Manifest/VCAT: GAP_OBJ is last child!"
	.size	.L.str.59, 38

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"Manifest: bt[par] no children!"
	.size	.L.str.60, 31

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"Manifest:last_ft!"
	.size	.L.str.61, 18

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"Manifest: ft[par] child!"
	.size	.L.str.62, 25

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"Manifest: lthread == rthread!"
	.size	.L.str.63, 30

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"replacing invalid scale factor by 1.0"
	.size	.L.str.64, 38

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"%f"
	.size	.L.str.65, 3

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"replacing invalid scale factor %s by 1.0"
	.size	.L.str.66, 41

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"replacing undersized scale factor %s by 1.0"
	.size	.L.str.67, 44

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"replacing oversized scale factor %s by 1.0"
	.size	.L.str.68, 43

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"%s deleted (right parameter is malformed)"
	.size	.L.str.69, 42

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"@Case"
	.size	.L.str.70, 6

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"%s expected here"
	.size	.L.str.71, 17

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"else"
	.size	.L.str.72, 5

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"error in left parameter of %s"
	.size	.L.str.73, 30

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"replacing unknown %s option %s by %s"
	.size	.L.str.74, 37

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"%s deleted (choice %s unknown)"
	.size	.L.str.75, 31

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"Manifest TAGGED: children!"
	.size	.L.str.76, 27

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"left parameter of %s is not a cross reference"
	.size	.L.str.77, 46

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"@Tagged"
	.size	.L.str.78, 8

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"left parameter of %s must be a symbol"
	.size	.L.str.79, 38

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"symbol %s not allowed here (it has no %s)"
	.size	.L.str.80, 42

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"@Tag"
	.size	.L.str.81, 5

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"preceding"
	.size	.L.str.82, 10

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"following"
	.size	.L.str.83, 10

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"foll_or_prec"
	.size	.L.str.84, 13

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"%s, %s or %s expected in left parameter of %s"
	.size	.L.str.85, 46

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"right parameter of %s must be a simple word"
	.size	.L.str.86, 44


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
