	.text
	.file	"btBvhTriangleMeshShape.bc"
	.globl	_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebb
	.p2align	4, 0x90
	.type	_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebb,@function
_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebb: # @_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebb
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 80
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN19btTriangleMeshShapeC2EP23btStridingMeshInterface
	movq	$_ZTV22btBvhTriangleMeshShape+16, (%rbx)
	movq	$0, 72(%rbx)
	movb	%bpl, 80(%rbx)
	movb	$0, 81(%rbx)
	movl	$21, 8(%rbx)
	movq	(%r14), %rax
.Ltmp0:
	movq	%r14, %rdi
	callq	*80(%rax)
.Ltmp1:
# BB#1:
	testb	%al, %al
	je	.LBB0_3
# BB#2:
	movq	(%r14), %rax
.Ltmp4:
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rdx
	movq	%r14, %rdi
	callq	*96(%rax)
.Ltmp5:
	jmp	.LBB0_4
.LBB0_3:
.Ltmp2:
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rdx
	movq	%r14, %rdi
	callq	_ZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_
.Ltmp3:
.LBB0_4:
	testb	%r15b, %r15b
	je	.LBB0_9
# BB#5:
.Ltmp6:
	movl	$248, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
.Ltmp7:
# BB#6:
.Ltmp9:
	movq	%rbp, %rdi
	callq	_ZN14btOptimizedBvhC1Ev
.Ltmp10:
# BB#7:
	movq	%rbp, 72(%rbx)
	movzbl	80(%rbx), %edx
.Ltmp12:
	leaq	24(%rsp), %rcx
	leaq	8(%rsp), %r8
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_
.Ltmp13:
# BB#8:
	movb	$1, 81(%rbx)
.LBB0_9:
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_10:
.Ltmp14:
	jmp	.LBB0_13
.LBB0_11:
.Ltmp11:
	jmp	.LBB0_13
.LBB0_12:
.Ltmp8:
.LBB0_13:
	movq	%rax, %rbp
.Ltmp15:
	movq	%rbx, %rdi
	callq	_ZN19btTriangleMeshShapeD2Ev
.Ltmp16:
# BB#14:
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB0_15:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebb, .Lfunc_end0-_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp0           #   Call between .Ltmp0 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Lfunc_end0-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebRK9btVector3S4_b
	.p2align	4, 0x90
	.type	_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebRK9btVector3S4_b,@function
_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebRK9btVector3S4_b: # @_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebRK9btVector3S4_b
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 64
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
	movq	%r8, %r14
	movq	%rcx, %r15
	movl	%edx, %ebp
	movq	%rsi, %r12
	movq	%rdi, %rbx
	callq	_ZN19btTriangleMeshShapeC2EP23btStridingMeshInterface
	movq	$_ZTV22btBvhTriangleMeshShape+16, (%rbx)
	movq	$0, 72(%rbx)
	movb	%bpl, 80(%rbx)
	movb	$0, 81(%rbx)
	movl	$21, 8(%rbx)
	testb	%r13b, %r13b
	je	.LBB2_5
# BB#1:
.Ltmp18:
	movl	$248, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r13
.Ltmp19:
# BB#2:
.Ltmp21:
	movq	%r13, %rdi
	callq	_ZN14btOptimizedBvhC1Ev
.Ltmp22:
# BB#3:
	movq	%r13, 72(%rbx)
	movzbl	80(%rbx), %edx
.Ltmp24:
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_
.Ltmp25:
# BB#4:
	movb	$1, 81(%rbx)
.LBB2_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_6:
.Ltmp26:
	jmp	.LBB2_9
.LBB2_7:
.Ltmp23:
	jmp	.LBB2_9
.LBB2_8:
.Ltmp20:
.LBB2_9:
	movq	%rax, %r14
.Ltmp27:
	movq	%rbx, %rdi
	callq	_ZN19btTriangleMeshShapeD2Ev
.Ltmp28:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_11:
.Ltmp29:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebRK9btVector3S4_b, .Lfunc_end2-_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebRK9btVector3S4_b
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp18-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin1   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin1   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin1   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin1   #     jumps to .Ltmp29
	.byte	1                       #   On action: 1
	.long	.Ltmp28-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Lfunc_end2-.Ltmp28     #   Call between .Ltmp28 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN22btBvhTriangleMeshShape16partialRefitTreeERK9btVector3S2_
	.p2align	4, 0x90
	.type	_ZN22btBvhTriangleMeshShape16partialRefitTreeERK9btVector3S2_,@function
_ZN22btBvhTriangleMeshShape16partialRefitTreeERK9btVector3S2_: # @_ZN22btBvhTriangleMeshShape16partialRefitTreeERK9btVector3S2_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	64(%rbx), %rsi
	movq	72(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	_ZN14btOptimizedBvh12refitPartialEP23btStridingMeshInterfaceRK9btVector3S4_
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	28(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB3_2
# BB#1:
	movss	%xmm0, 28(%rbx)
.LBB3_2:                                # %_Z8btSetMinIfEvRT_RKS0_.exit.i
	movss	4(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	32(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB3_4
# BB#3:
	movss	%xmm0, 32(%rbx)
.LBB3_4:                                # %_Z8btSetMinIfEvRT_RKS0_.exit7.i
	movss	8(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB3_6
# BB#5:
	movss	%xmm0, 36(%rbx)
.LBB3_6:                                # %_Z8btSetMinIfEvRT_RKS0_.exit6.i
	movss	12(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	40(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB3_8
# BB#7:
	movss	%xmm0, 40(%rbx)
.LBB3_8:                                # %_ZN9btVector36setMinERKS_.exit
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	44(%rbx), %xmm0
	jbe	.LBB3_10
# BB#9:
	movss	%xmm0, 44(%rbx)
.LBB3_10:                               # %_Z8btSetMaxIfEvRT_RKS0_.exit.i
	movss	4(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	48(%rbx), %xmm0
	jbe	.LBB3_12
# BB#11:
	movss	%xmm0, 48(%rbx)
.LBB3_12:                               # %_Z8btSetMaxIfEvRT_RKS0_.exit7.i
	movss	8(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	52(%rbx), %xmm0
	jbe	.LBB3_14
# BB#13:
	movss	%xmm0, 52(%rbx)
.LBB3_14:                               # %_Z8btSetMaxIfEvRT_RKS0_.exit6.i
	movss	12(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	56(%rbx), %xmm0
	jbe	.LBB3_16
# BB#15:
	movss	%xmm0, 56(%rbx)
.LBB3_16:                               # %_ZN9btVector36setMaxERKS_.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZN22btBvhTriangleMeshShape16partialRefitTreeERK9btVector3S2_, .Lfunc_end3-_ZN22btBvhTriangleMeshShape16partialRefitTreeERK9btVector3S2_
	.cfi_endproc

	.globl	_ZN22btBvhTriangleMeshShape9refitTreeERK9btVector3S2_
	.p2align	4, 0x90
	.type	_ZN22btBvhTriangleMeshShape9refitTreeERK9btVector3S2_,@function
_ZN22btBvhTriangleMeshShape9refitTreeERK9btVector3S2_: # @_ZN22btBvhTriangleMeshShape9refitTreeERK9btVector3S2_
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
.Lcfi29:
	.cfi_offset %rbx, -16
	movq	%rdx, %rax
	movq	%rsi, %rcx
	movq	%rdi, %rbx
	movq	64(%rbx), %rsi
	movq	72(%rbx), %rdi
	movq	%rcx, %rdx
	movq	%rax, %rcx
	callq	_ZN14btOptimizedBvh5refitEP23btStridingMeshInterfaceRK9btVector3S4_
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZN19btTriangleMeshShape15recalcLocalAabbEv # TAILCALL
.Lfunc_end4:
	.size	_ZN22btBvhTriangleMeshShape9refitTreeERK9btVector3S2_, .Lfunc_end4-_ZN22btBvhTriangleMeshShape9refitTreeERK9btVector3S2_
	.cfi_endproc

	.globl	_ZN22btBvhTriangleMeshShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN22btBvhTriangleMeshShapeD2Ev,@function
_ZN22btBvhTriangleMeshShapeD2Ev:        # @_ZN22btBvhTriangleMeshShapeD2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -24
.Lcfi34:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV22btBvhTriangleMeshShape+16, (%rbx)
	cmpb	$0, 81(%rbx)
	je	.LBB5_3
# BB#1:
	movq	72(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp30:
	callq	*(%rax)
.Ltmp31:
# BB#2:
	movq	72(%rbx), %rdi
.Ltmp32:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp33:
.LBB5_3:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN19btTriangleMeshShapeD2Ev # TAILCALL
.LBB5_4:
.Ltmp34:
	movq	%rax, %r14
.Ltmp35:
	movq	%rbx, %rdi
	callq	_ZN19btTriangleMeshShapeD2Ev
.Ltmp36:
# BB#5:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_6:
.Ltmp37:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN22btBvhTriangleMeshShapeD2Ev, .Lfunc_end5-_ZN22btBvhTriangleMeshShapeD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp30-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp33-.Ltmp30         #   Call between .Ltmp30 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin2   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp35-.Ltmp33         #   Call between .Ltmp33 and .Ltmp35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin2   #     jumps to .Ltmp37
	.byte	1                       #   On action: 1
	.long	.Ltmp36-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp36     #   Call between .Ltmp36 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN22btBvhTriangleMeshShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN22btBvhTriangleMeshShapeD0Ev,@function
_ZN22btBvhTriangleMeshShapeD0Ev:        # @_ZN22btBvhTriangleMeshShapeD0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV22btBvhTriangleMeshShape+16, (%rbx)
	cmpb	$0, 81(%rbx)
	je	.LBB6_3
# BB#1:
	movq	72(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp38:
	callq	*(%rax)
.Ltmp39:
# BB#2:
	movq	72(%rbx), %rdi
.Ltmp40:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp41:
.LBB6_3:
.Ltmp46:
	movq	%rbx, %rdi
	callq	_ZN19btTriangleMeshShapeD2Ev
.Ltmp47:
# BB#4:                                 # %_ZN22btBvhTriangleMeshShapeD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB6_7:
.Ltmp48:
	movq	%rax, %r14
	jmp	.LBB6_8
.LBB6_5:
.Ltmp42:
	movq	%rax, %r14
.Ltmp43:
	movq	%rbx, %rdi
	callq	_ZN19btTriangleMeshShapeD2Ev
.Ltmp44:
.LBB6_8:                                # %.body
.Ltmp49:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp50:
# BB#9:                                 # %_ZN22btBvhTriangleMeshShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_6:
.Ltmp45:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_10:
.Ltmp51:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN22btBvhTriangleMeshShapeD0Ev, .Lfunc_end6-_ZN22btBvhTriangleMeshShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp38-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp41-.Ltmp38         #   Call between .Ltmp38 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin3   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin3   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp43-.Ltmp47         #   Call between .Ltmp47 and .Ltmp43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin3   #     jumps to .Ltmp45
	.byte	1                       #   On action: 1
	.long	.Ltmp49-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin3   #     jumps to .Ltmp51
	.byte	1                       #   On action: 1
	.long	.Ltmp50-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Lfunc_end6-.Ltmp50     #   Call between .Ltmp50 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_,@function
_ZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_: # @_ZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	subq	$24, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 32
	movq	64(%rdi), %rax
	movq	$_ZTVZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback+16, (%rsp)
	movq	%rax, 8(%rsp)
	movq	%rsi, 16(%rsp)
	movq	72(%rdi), %rdi
.Ltmp52:
	movq	%rsp, %rsi
	callq	_ZNK14btQuantizedBvh25reportRayOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_
.Ltmp53:
# BB#1:
	addq	$24, %rsp
	retq
.LBB7_2:
.Ltmp54:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_, .Lfunc_end7-_ZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp52-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin4   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end7-.Ltmp53     #   Call between .Ltmp53 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN21btNodeOverlapCallbackD2Ev,"axG",@progbits,_ZN21btNodeOverlapCallbackD2Ev,comdat
	.weak	_ZN21btNodeOverlapCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN21btNodeOverlapCallbackD2Ev,@function
_ZN21btNodeOverlapCallbackD2Ev:         # @_ZN21btNodeOverlapCallbackD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end8:
	.size	_ZN21btNodeOverlapCallbackD2Ev, .Lfunc_end8-_ZN21btNodeOverlapCallbackD2Ev
	.cfi_endproc

	.text
	.globl	_ZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_
	.p2align	4, 0x90
	.type	_ZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_,@function
_ZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_: # @_ZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	subq	$24, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 32
	movq	64(%rdi), %rax
	movq	$_ZTVZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback+16, (%rsp)
	movq	%rax, 8(%rsp)
	movq	%rsi, 16(%rsp)
	movq	72(%rdi), %rdi
.Ltmp55:
	movq	%rsp, %rsi
	callq	_ZNK14btQuantizedBvh29reportBoxCastOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_
.Ltmp56:
# BB#1:
	addq	$24, %rsp
	retq
.LBB9_2:
.Ltmp57:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end9:
	.size	_ZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_, .Lfunc_end9-_ZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp55-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin5   #     jumps to .Ltmp57
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end9-.Ltmp56     #   Call between .Ltmp56 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_,@function
_ZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_: # @_ZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	subq	$72, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 80
	movq	64(%rdi), %rax
	movq	$_ZTVZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback+16, (%rsp)
	movq	%rax, 8(%rsp)
	movq	%rsi, 16(%rsp)
	movq	72(%rdi), %rdi
.Ltmp58:
	movq	%rsp, %rsi
	callq	_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_
.Ltmp59:
# BB#1:
	addq	$72, %rsp
	retq
.LBB10_2:
.Ltmp60:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_, .Lfunc_end10-_ZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp58-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin6   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Lfunc_end10-.Ltmp59    #   Call between .Ltmp59 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_0:
	.long	872415232               # float 1.1920929E-7
	.text
	.globl	_ZN22btBvhTriangleMeshShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN22btBvhTriangleMeshShape15setLocalScalingERK9btVector3,@function
_ZN22btBvhTriangleMeshShape15setLocalScalingERK9btVector3: # @_ZN22btBvhTriangleMeshShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 32
.Lcfi46:
	.cfi_offset %rbx, -24
.Lcfi47:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*56(%rax)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	(%rbx), %xmm0
	subss	4(%rbx), %xmm1
	movss	8(%rax), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	8(%rbx), %xmm2
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	ucomiss	.LCPI11_0(%rip), %xmm2
	jbe	.LBB11_4
# BB#1:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN19btTriangleMeshShape15setLocalScalingERK9btVector3
	cmpb	$0, 81(%r14)
	je	.LBB11_3
# BB#2:
	movq	72(%r14), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	72(%r14), %rdi
	callq	_Z21btAlignedFreeInternalPv
.LBB11_3:
	movl	$248, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_ZN14btOptimizedBvhC1Ev
	movq	%rbx, 72(%r14)
	movq	64(%r14), %rsi
	leaq	28(%r14), %rcx
	leaq	44(%r14), %r8
	movzbl	80(%r14), %edx
	movq	%rbx, %rdi
	callq	_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_
	movb	$1, 81(%r14)
.LBB11_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	_ZN22btBvhTriangleMeshShape15setLocalScalingERK9btVector3, .Lfunc_end11-_ZN22btBvhTriangleMeshShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_0:
	.long	872415232               # float 1.1920929E-7
	.text
	.globl	_ZN22btBvhTriangleMeshShape15setOptimizedBvhEP14btOptimizedBvhRK9btVector3
	.p2align	4, 0x90
	.type	_ZN22btBvhTriangleMeshShape15setOptimizedBvhEP14btOptimizedBvhRK9btVector3,@function
_ZN22btBvhTriangleMeshShape15setOptimizedBvhEP14btOptimizedBvhRK9btVector3: # @_ZN22btBvhTriangleMeshShape15setOptimizedBvhEP14btOptimizedBvhRK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 32
.Lcfi51:
	.cfi_offset %rbx, -24
.Lcfi52:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movq	%rsi, 72(%rbx)
	movb	$0, 81(%rbx)
	movq	(%rbx), %rax
	callq	*56(%rax)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	(%r14), %xmm0
	subss	4(%r14), %xmm1
	movss	8(%rax), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	8(%r14), %xmm2
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	ucomiss	.LCPI12_0(%rip), %xmm2
	jbe	.LBB12_1
# BB#2:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN19btTriangleMeshShape15setLocalScalingERK9btVector3 # TAILCALL
.LBB12_1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	_ZN22btBvhTriangleMeshShape15setOptimizedBvhEP14btOptimizedBvhRK9btVector3, .Lfunc_end12-_ZN22btBvhTriangleMeshShape15setOptimizedBvhEP14btOptimizedBvhRK9btVector3
	.cfi_endproc

	.section	.text._ZNK22btBvhTriangleMeshShape7getNameEv,"axG",@progbits,_ZNK22btBvhTriangleMeshShape7getNameEv,comdat
	.weak	_ZNK22btBvhTriangleMeshShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK22btBvhTriangleMeshShape7getNameEv,@function
_ZNK22btBvhTriangleMeshShape7getNameEv: # @_ZNK22btBvhTriangleMeshShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end13:
	.size	_ZNK22btBvhTriangleMeshShape7getNameEv, .Lfunc_end13-_ZNK22btBvhTriangleMeshShape7getNameEv
	.cfi_endproc

	.section	.text._ZN14btConcaveShape9setMarginEf,"axG",@progbits,_ZN14btConcaveShape9setMarginEf,comdat
	.weak	_ZN14btConcaveShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN14btConcaveShape9setMarginEf,@function
_ZN14btConcaveShape9setMarginEf:        # @_ZN14btConcaveShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 24(%rdi)
	retq
.Lfunc_end14:
	.size	_ZN14btConcaveShape9setMarginEf, .Lfunc_end14-_ZN14btConcaveShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK14btConcaveShape9getMarginEv,"axG",@progbits,_ZNK14btConcaveShape9getMarginEv,comdat
	.weak	_ZNK14btConcaveShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK14btConcaveShape9getMarginEv,@function
_ZNK14btConcaveShape9getMarginEv:       # @_ZNK14btConcaveShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	24(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end15:
	.size	_ZNK14btConcaveShape9getMarginEv, .Lfunc_end15-_ZNK14btConcaveShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3,"axG",@progbits,_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3,comdat
	.weak	_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	104(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end16:
	.size	_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end16-_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev,@function
_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev: # @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end17:
	.size	_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev, .Lfunc_end17-_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii,@function
_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii: # @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 48
	subq	$112, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 160
.Lcfi59:
	.cfi_offset %rbx, -48
.Lcfi60:
	.cfi_offset %r12, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %r14d
	movq	%rdi, %r12
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	leaq	76(%rsp), %rbp
	leaq	104(%rsp), %rbx
	leaq	80(%rsp), %r10
	leaq	96(%rsp), %rsi
	leaq	108(%rsp), %rdx
	leaq	84(%rsp), %rcx
	leaq	12(%rsp), %r8
	leaq	88(%rsp), %r9
	pushq	%r14
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	callq	*32(%rax)
	addq	$32, %rsp
.Lcfi68:
	.cfi_adjust_cfa_offset -32
	movq	88(%rsp), %rdx
	movslq	80(%rsp), %rax
	movslq	%r15d, %rsi
	imulq	%rax, %rsi
	movq	8(%r12), %rax
	movl	84(%rsp), %edi
	movq	96(%rsp), %rcx
	cmpl	$3, 76(%rsp)
	jne	.LBB18_6
# BB#1:                                 # %.split.us.preheader
	movzwl	4(%rdx,%rsi), %ebx
	movl	12(%rsp), %ebp
	imull	%ebp, %ebx
	testl	%edi, %edi
	movslq	%ebx, %rbx
	je	.LBB18_3
# BB#2:
	movupd	(%rcx,%rbx), %xmm0
	cvtpd2ps	%xmm0, %xmm1
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movsd	16(%rcx,%rbx), %xmm1    # xmm1 = mem[0],zero
	cvtsd2ss	%xmm1, %xmm1
	jmp	.LBB18_4
.LBB18_6:                               # %.split
	movl	12(%rsp), %ebp
	movl	8(%rdx,%rsi), %ebx
	imull	%ebp, %ebx
	testl	%edi, %edi
	movslq	%ebx, %rdi
	je	.LBB18_8
# BB#7:                                 # %.split.split.preheader
	movsd	(%rcx,%rdi), %xmm0      # xmm0 = mem[0],zero
	movsd	8(%rcx,%rdi), %xmm1     # xmm1 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	mulss	8(%rax), %xmm0
	cvtsd2ss	%xmm1, %xmm1
	mulss	12(%rax), %xmm1
	movsd	16(%rcx,%rdi), %xmm2    # xmm2 = mem[0],zero
	cvtsd2ss	%xmm2, %xmm2
	mulss	16(%rax), %xmm2
	movss	%xmm0, 48(%rsp)
	movss	%xmm1, 52(%rsp)
	movss	%xmm2, 56(%rsp)
	movl	$0, 60(%rsp)
	movl	4(%rdx,%rsi), %edi
	imull	%ebp, %edi
	movslq	%edi, %rdi
	movsd	(%rcx,%rdi), %xmm0      # xmm0 = mem[0],zero
	movsd	8(%rcx,%rdi), %xmm1     # xmm1 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	mulss	8(%rax), %xmm0
	cvtsd2ss	%xmm1, %xmm1
	mulss	12(%rax), %xmm1
	movsd	16(%rcx,%rdi), %xmm2    # xmm2 = mem[0],zero
	cvtsd2ss	%xmm2, %xmm2
	mulss	16(%rax), %xmm2
	movss	%xmm0, 32(%rsp)
	movss	%xmm1, 36(%rsp)
	movss	%xmm2, 40(%rsp)
	movl	$0, 44(%rsp)
	imull	(%rdx,%rsi), %ebp
	movslq	%ebp, %rdx
	jmp	.LBB18_13
.LBB18_3:
	movsd	(%rcx,%rbx), %xmm1      # xmm1 = mem[0],zero
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movss	8(%rcx,%rbx), %xmm1     # xmm1 = mem[0],zero,zero,zero
.LBB18_4:                               # %.split.us.185
	mulss	16(%rax), %xmm1
	movss	%xmm0, 48(%rsp)
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm0, 52(%rsp)
	movss	%xmm1, 56(%rsp)
	movl	$0, 60(%rsp)
	movzwl	2(%rdx,%rsi), %ebx
	imull	%ebp, %ebx
	testl	%edi, %edi
	movslq	%ebx, %rbx
	je	.LBB18_11
# BB#5:
	movupd	(%rcx,%rbx), %xmm0
	cvtpd2ps	%xmm0, %xmm1
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movsd	16(%rcx,%rbx), %xmm1    # xmm1 = mem[0],zero
	cvtsd2ss	%xmm1, %xmm1
	jmp	.LBB18_12
.LBB18_8:                               # %.split.split.us.preheader
	movss	(%rcx,%rdi), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movss	4(%rcx,%rdi), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	8(%rax), %xmm0
	mulss	12(%rax), %xmm1
	movss	8(%rcx,%rdi), %xmm2     # xmm2 = mem[0],zero,zero,zero
	mulss	16(%rax), %xmm2
	movss	%xmm0, 48(%rsp)
	movss	%xmm1, 52(%rsp)
	movss	%xmm2, 56(%rsp)
	movl	$0, 60(%rsp)
	movl	4(%rdx,%rsi), %edi
	imull	%ebp, %edi
	movslq	%edi, %rdi
	movss	(%rcx,%rdi), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movss	4(%rcx,%rdi), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	8(%rax), %xmm0
	mulss	12(%rax), %xmm1
	movss	8(%rcx,%rdi), %xmm2     # xmm2 = mem[0],zero,zero,zero
	mulss	16(%rax), %xmm2
	movss	%xmm0, 32(%rsp)
	movss	%xmm1, 36(%rsp)
	movss	%xmm2, 40(%rsp)
	movl	$0, 44(%rsp)
	imull	(%rdx,%rsi), %ebp
	movslq	%ebp, %rdx
	jmp	.LBB18_9
.LBB18_11:
	movsd	(%rcx,%rbx), %xmm1      # xmm1 = mem[0],zero
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movss	8(%rcx,%rbx), %xmm1     # xmm1 = mem[0],zero,zero,zero
.LBB18_12:                              # %.split.us.286
	mulss	16(%rax), %xmm1
	movss	%xmm0, 32(%rsp)
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm0, 36(%rsp)
	movss	%xmm1, 40(%rsp)
	movl	$0, 44(%rsp)
	movzwl	(%rdx,%rsi), %edx
	imull	%edx, %ebp
	testl	%edi, %edi
	movslq	%ebp, %rdx
	je	.LBB18_9
.LBB18_13:
	movupd	(%rcx,%rdx), %xmm0
	cvtpd2ps	%xmm0, %xmm1
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movsd	16(%rcx,%rdx), %xmm1    # xmm1 = mem[0],zero
	cvtsd2ss	%xmm1, %xmm1
	jmp	.LBB18_10
.LBB18_9:                               # %.us-lcssa.us
	movsd	(%rcx,%rdx), %xmm1      # xmm1 = mem[0],zero
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movss	8(%rcx,%rdx), %xmm1     # xmm1 = mem[0],zero,zero,zero
.LBB18_10:                              # %.us-lcssa.us
	mulss	16(%rax), %xmm1
	movss	%xmm0, 16(%rsp)
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm0, 20(%rsp)
	movss	%xmm1, 24(%rsp)
	movl	$0, 28(%rsp)
	movq	16(%r12), %rdi
	movq	(%rdi), %rax
	leaq	16(%rsp), %rsi
	movl	%r14d, %edx
	movl	%r15d, %ecx
	callq	*16(%rax)
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	movl	%r14d, %esi
	callq	*48(%rax)
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii, .Lfunc_end18-_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackD0Ev,@function
_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackD0Ev: # @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end19:
	.size	_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackD0Ev, .Lfunc_end19-_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackD0Ev
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallback11processNodeEii,@function
_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallback11processNodeEii: # @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallback11processNodeEii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 48
	subq	$112, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 160
.Lcfi75:
	.cfi_offset %rbx, -48
.Lcfi76:
	.cfi_offset %r12, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %r14d
	movq	%rdi, %r12
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	leaq	76(%rsp), %rbp
	leaq	104(%rsp), %rbx
	leaq	80(%rsp), %r10
	leaq	96(%rsp), %rsi
	leaq	108(%rsp), %rdx
	leaq	84(%rsp), %rcx
	leaq	12(%rsp), %r8
	leaq	88(%rsp), %r9
	pushq	%r14
.Lcfi80:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi81:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi82:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi83:
	.cfi_adjust_cfa_offset 8
	callq	*32(%rax)
	addq	$32, %rsp
.Lcfi84:
	.cfi_adjust_cfa_offset -32
	movq	88(%rsp), %rdx
	movslq	80(%rsp), %rax
	movslq	%r15d, %rsi
	imulq	%rax, %rsi
	movq	8(%r12), %rax
	movl	84(%rsp), %edi
	movq	96(%rsp), %rcx
	cmpl	$3, 76(%rsp)
	jne	.LBB20_6
# BB#1:                                 # %.split.us.preheader
	movzwl	4(%rdx,%rsi), %ebx
	movl	12(%rsp), %ebp
	imull	%ebp, %ebx
	testl	%edi, %edi
	movslq	%ebx, %rbx
	je	.LBB20_3
# BB#2:
	movupd	(%rcx,%rbx), %xmm0
	cvtpd2ps	%xmm0, %xmm1
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movsd	16(%rcx,%rbx), %xmm1    # xmm1 = mem[0],zero
	cvtsd2ss	%xmm1, %xmm1
	jmp	.LBB20_4
.LBB20_6:                               # %.split
	movl	12(%rsp), %ebp
	movl	8(%rdx,%rsi), %ebx
	imull	%ebp, %ebx
	testl	%edi, %edi
	movslq	%ebx, %rdi
	je	.LBB20_8
# BB#7:                                 # %.split.split.preheader
	movsd	(%rcx,%rdi), %xmm0      # xmm0 = mem[0],zero
	movsd	8(%rcx,%rdi), %xmm1     # xmm1 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	mulss	8(%rax), %xmm0
	cvtsd2ss	%xmm1, %xmm1
	mulss	12(%rax), %xmm1
	movsd	16(%rcx,%rdi), %xmm2    # xmm2 = mem[0],zero
	cvtsd2ss	%xmm2, %xmm2
	mulss	16(%rax), %xmm2
	movss	%xmm0, 48(%rsp)
	movss	%xmm1, 52(%rsp)
	movss	%xmm2, 56(%rsp)
	movl	$0, 60(%rsp)
	movl	4(%rdx,%rsi), %edi
	imull	%ebp, %edi
	movslq	%edi, %rdi
	movsd	(%rcx,%rdi), %xmm0      # xmm0 = mem[0],zero
	movsd	8(%rcx,%rdi), %xmm1     # xmm1 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	mulss	8(%rax), %xmm0
	cvtsd2ss	%xmm1, %xmm1
	mulss	12(%rax), %xmm1
	movsd	16(%rcx,%rdi), %xmm2    # xmm2 = mem[0],zero
	cvtsd2ss	%xmm2, %xmm2
	mulss	16(%rax), %xmm2
	movss	%xmm0, 32(%rsp)
	movss	%xmm1, 36(%rsp)
	movss	%xmm2, 40(%rsp)
	movl	$0, 44(%rsp)
	imull	(%rdx,%rsi), %ebp
	movslq	%ebp, %rdx
	jmp	.LBB20_13
.LBB20_3:
	movsd	(%rcx,%rbx), %xmm1      # xmm1 = mem[0],zero
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movss	8(%rcx,%rbx), %xmm1     # xmm1 = mem[0],zero,zero,zero
.LBB20_4:                               # %.split.us.185
	mulss	16(%rax), %xmm1
	movss	%xmm0, 48(%rsp)
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm0, 52(%rsp)
	movss	%xmm1, 56(%rsp)
	movl	$0, 60(%rsp)
	movzwl	2(%rdx,%rsi), %ebx
	imull	%ebp, %ebx
	testl	%edi, %edi
	movslq	%ebx, %rbx
	je	.LBB20_11
# BB#5:
	movupd	(%rcx,%rbx), %xmm0
	cvtpd2ps	%xmm0, %xmm1
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movsd	16(%rcx,%rbx), %xmm1    # xmm1 = mem[0],zero
	cvtsd2ss	%xmm1, %xmm1
	jmp	.LBB20_12
.LBB20_8:                               # %.split.split.us.preheader
	movss	(%rcx,%rdi), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movss	4(%rcx,%rdi), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	8(%rax), %xmm0
	mulss	12(%rax), %xmm1
	movss	8(%rcx,%rdi), %xmm2     # xmm2 = mem[0],zero,zero,zero
	mulss	16(%rax), %xmm2
	movss	%xmm0, 48(%rsp)
	movss	%xmm1, 52(%rsp)
	movss	%xmm2, 56(%rsp)
	movl	$0, 60(%rsp)
	movl	4(%rdx,%rsi), %edi
	imull	%ebp, %edi
	movslq	%edi, %rdi
	movss	(%rcx,%rdi), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movss	4(%rcx,%rdi), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	8(%rax), %xmm0
	mulss	12(%rax), %xmm1
	movss	8(%rcx,%rdi), %xmm2     # xmm2 = mem[0],zero,zero,zero
	mulss	16(%rax), %xmm2
	movss	%xmm0, 32(%rsp)
	movss	%xmm1, 36(%rsp)
	movss	%xmm2, 40(%rsp)
	movl	$0, 44(%rsp)
	imull	(%rdx,%rsi), %ebp
	movslq	%ebp, %rdx
	jmp	.LBB20_9
.LBB20_11:
	movsd	(%rcx,%rbx), %xmm1      # xmm1 = mem[0],zero
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movss	8(%rcx,%rbx), %xmm1     # xmm1 = mem[0],zero,zero,zero
.LBB20_12:                              # %.split.us.286
	mulss	16(%rax), %xmm1
	movss	%xmm0, 32(%rsp)
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm0, 36(%rsp)
	movss	%xmm1, 40(%rsp)
	movl	$0, 44(%rsp)
	movzwl	(%rdx,%rsi), %edx
	imull	%edx, %ebp
	testl	%edi, %edi
	movslq	%ebp, %rdx
	je	.LBB20_9
.LBB20_13:
	movupd	(%rcx,%rdx), %xmm0
	cvtpd2ps	%xmm0, %xmm1
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movsd	16(%rcx,%rdx), %xmm1    # xmm1 = mem[0],zero
	cvtsd2ss	%xmm1, %xmm1
	jmp	.LBB20_10
.LBB20_9:                               # %.us-lcssa.us
	movsd	(%rcx,%rdx), %xmm1      # xmm1 = mem[0],zero
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movss	8(%rcx,%rdx), %xmm1     # xmm1 = mem[0],zero,zero,zero
.LBB20_10:                              # %.us-lcssa.us
	mulss	16(%rax), %xmm1
	movss	%xmm0, 16(%rsp)
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm0, 20(%rsp)
	movss	%xmm1, 24(%rsp)
	movl	$0, 28(%rsp)
	movq	16(%r12), %rdi
	movq	(%rdi), %rax
	leaq	16(%rsp), %rsi
	movl	%r14d, %edx
	movl	%r15d, %ecx
	callq	*16(%rax)
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	movl	%r14d, %esi
	callq	*48(%rax)
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallback11processNodeEii, .Lfunc_end20-_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallback11processNodeEii
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev,@function
_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev: # @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end21:
	.size	_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev, .Lfunc_end21-_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii,@function
_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii: # @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi89:
	.cfi_def_cfa_offset 80
.Lcfi90:
	.cfi_offset %rbx, -40
.Lcfi91:
	.cfi_offset %r14, -32
.Lcfi92:
	.cfi_offset %r15, -24
.Lcfi93:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	4(%rsp), %rbp
	leaq	32(%rsp), %r10
	leaq	8(%rsp), %r11
	leaq	24(%rsp), %rsi
	leaq	36(%rsp), %rdx
	leaq	12(%rsp), %rcx
	movq	%rsp, %r8
	leaq	16(%rsp), %r9
	pushq	%r14
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi95:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi96:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi97:
	.cfi_adjust_cfa_offset 8
	callq	*32(%rax)
	addq	$32, %rsp
.Lcfi98:
	.cfi_adjust_cfa_offset -32
	movq	16(%rsp), %r8
	movslq	8(%rsp), %rax
	movslq	%r15d, %rsi
	imulq	%rax, %rsi
	movq	8(%rbx), %rax
	movl	12(%rsp), %edi
	movq	24(%rsp), %rcx
	cmpl	$3, 4(%rsp)
	jne	.LBB22_6
# BB#1:                                 # %.split.us.preheader
	movzwl	4(%r8,%rsi), %edx
	movl	(%rsp), %ebp
	imull	%ebp, %edx
	testl	%edi, %edi
	movslq	%edx, %rdx
	je	.LBB22_3
# BB#2:
	movupd	(%rcx,%rdx), %xmm0
	cvtpd2ps	%xmm0, %xmm1
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movsd	16(%rcx,%rdx), %xmm1    # xmm1 = mem[0],zero
	cvtsd2ss	%xmm1, %xmm1
	jmp	.LBB22_4
.LBB22_6:                               # %.split
	movl	(%rsp), %ebp
	movl	8(%r8,%rsi), %edx
	imull	%ebp, %edx
	testl	%edi, %edi
	movslq	%edx, %rdi
	je	.LBB22_8
# BB#7:                                 # %.split.split.preheader
	movsd	(%rcx,%rdi), %xmm0      # xmm0 = mem[0],zero
	movsd	8(%rcx,%rdi), %xmm1     # xmm1 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	mulss	8(%rax), %xmm0
	cvtsd2ss	%xmm1, %xmm1
	mulss	12(%rax), %xmm1
	movsd	16(%rcx,%rdi), %xmm2    # xmm2 = mem[0],zero
	cvtsd2ss	%xmm2, %xmm2
	mulss	16(%rax), %xmm2
	movss	%xmm0, 56(%rbx)
	movss	%xmm1, 60(%rbx)
	movss	%xmm2, 64(%rbx)
	movl	$0, 68(%rbx)
	movl	4(%r8,%rsi), %edx
	imull	%ebp, %edx
	movslq	%edx, %rdx
	movsd	(%rcx,%rdx), %xmm0      # xmm0 = mem[0],zero
	movsd	8(%rcx,%rdx), %xmm1     # xmm1 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	mulss	8(%rax), %xmm0
	cvtsd2ss	%xmm1, %xmm1
	mulss	12(%rax), %xmm1
	movsd	16(%rcx,%rdx), %xmm2    # xmm2 = mem[0],zero
	cvtsd2ss	%xmm2, %xmm2
	mulss	16(%rax), %xmm2
	movss	%xmm0, 40(%rbx)
	movss	%xmm1, 44(%rbx)
	movss	%xmm2, 48(%rbx)
	movl	$0, 52(%rbx)
	imull	(%r8,%rsi), %ebp
	movslq	%ebp, %rdx
	jmp	.LBB22_13
.LBB22_3:
	movsd	(%rcx,%rdx), %xmm1      # xmm1 = mem[0],zero
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movss	8(%rcx,%rdx), %xmm1     # xmm1 = mem[0],zero,zero,zero
.LBB22_4:                               # %.split.us.185
	mulss	16(%rax), %xmm1
	movss	%xmm0, 56(%rbx)
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm0, 60(%rbx)
	movss	%xmm1, 64(%rbx)
	movl	$0, 68(%rbx)
	movzwl	2(%r8,%rsi), %edx
	imull	%ebp, %edx
	testl	%edi, %edi
	movslq	%edx, %rdx
	je	.LBB22_11
# BB#5:
	movupd	(%rcx,%rdx), %xmm0
	cvtpd2ps	%xmm0, %xmm1
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movsd	16(%rcx,%rdx), %xmm1    # xmm1 = mem[0],zero
	cvtsd2ss	%xmm1, %xmm1
	jmp	.LBB22_12
.LBB22_8:                               # %.split.split.us.preheader
	movss	(%rcx,%rdi), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movss	4(%rcx,%rdi), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	8(%rax), %xmm0
	mulss	12(%rax), %xmm1
	movss	8(%rcx,%rdi), %xmm2     # xmm2 = mem[0],zero,zero,zero
	mulss	16(%rax), %xmm2
	movss	%xmm0, 56(%rbx)
	movss	%xmm1, 60(%rbx)
	movss	%xmm2, 64(%rbx)
	movl	$0, 68(%rbx)
	movl	4(%r8,%rsi), %edx
	imull	%ebp, %edx
	movslq	%edx, %rdx
	movss	(%rcx,%rdx), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movss	4(%rcx,%rdx), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	8(%rax), %xmm0
	mulss	12(%rax), %xmm1
	movss	8(%rcx,%rdx), %xmm2     # xmm2 = mem[0],zero,zero,zero
	mulss	16(%rax), %xmm2
	movss	%xmm0, 40(%rbx)
	movss	%xmm1, 44(%rbx)
	movss	%xmm2, 48(%rbx)
	movl	$0, 52(%rbx)
	imull	(%r8,%rsi), %ebp
	movslq	%ebp, %rdx
	jmp	.LBB22_9
.LBB22_11:
	movsd	(%rcx,%rdx), %xmm1      # xmm1 = mem[0],zero
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movss	8(%rcx,%rdx), %xmm1     # xmm1 = mem[0],zero,zero,zero
.LBB22_12:                              # %.split.us.286
	mulss	16(%rax), %xmm1
	movss	%xmm0, 40(%rbx)
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm0, 44(%rbx)
	movss	%xmm1, 48(%rbx)
	movl	$0, 52(%rbx)
	movzwl	(%r8,%rsi), %edx
	imull	%edx, %ebp
	testl	%edi, %edi
	movslq	%ebp, %rdx
	je	.LBB22_9
.LBB22_13:
	movupd	(%rcx,%rdx), %xmm0
	cvtpd2ps	%xmm0, %xmm1
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movsd	16(%rcx,%rdx), %xmm1    # xmm1 = mem[0],zero
	cvtsd2ss	%xmm1, %xmm1
	jmp	.LBB22_10
.LBB22_9:                               # %.us-lcssa.us
	movsd	(%rcx,%rdx), %xmm1      # xmm1 = mem[0],zero
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movss	8(%rcx,%rdx), %xmm1     # xmm1 = mem[0],zero,zero,zero
.LBB22_10:                              # %.us-lcssa.us
	mulss	16(%rax), %xmm1
	leaq	24(%rbx), %rsi
	movss	%xmm0, 24(%rbx)
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm0, 28(%rbx)
	movss	%xmm1, 32(%rbx)
	movl	$0, 36(%rbx)
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movl	%r14d, %edx
	movl	%r15d, %ecx
	callq	*16(%rax)
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movl	%r14d, %esi
	callq	*48(%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii, .Lfunc_end22-_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii
	.cfi_endproc

	.type	_ZTV22btBvhTriangleMeshShape,@object # @_ZTV22btBvhTriangleMeshShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV22btBvhTriangleMeshShape
	.p2align	3
_ZTV22btBvhTriangleMeshShape:
	.quad	0
	.quad	_ZTI22btBvhTriangleMeshShape
	.quad	_ZN22btBvhTriangleMeshShapeD2Ev
	.quad	_ZN22btBvhTriangleMeshShapeD0Ev
	.quad	_ZNK19btTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN22btBvhTriangleMeshShape15setLocalScalingERK9btVector3
	.quad	_ZNK19btTriangleMeshShape15getLocalScalingEv
	.quad	_ZNK19btTriangleMeshShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK22btBvhTriangleMeshShape7getNameEv
	.quad	_ZN14btConcaveShape9setMarginEf
	.quad	_ZNK14btConcaveShape9getMarginEv
	.quad	_ZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.quad	_ZNK19btTriangleMeshShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.size	_ZTV22btBvhTriangleMeshShape, 136

	.type	_ZTS22btBvhTriangleMeshShape,@object # @_ZTS22btBvhTriangleMeshShape
	.globl	_ZTS22btBvhTriangleMeshShape
	.p2align	4
_ZTS22btBvhTriangleMeshShape:
	.asciz	"22btBvhTriangleMeshShape"
	.size	_ZTS22btBvhTriangleMeshShape, 25

	.type	_ZTI22btBvhTriangleMeshShape,@object # @_ZTI22btBvhTriangleMeshShape
	.globl	_ZTI22btBvhTriangleMeshShape
	.p2align	4
_ZTI22btBvhTriangleMeshShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22btBvhTriangleMeshShape
	.quad	_ZTI19btTriangleMeshShape
	.size	_ZTI22btBvhTriangleMeshShape, 24

	.type	_ZTVZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback,@object # @_ZTVZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback
	.p2align	3
_ZTVZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback:
	.quad	0
	.quad	_ZTIZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback
	.quad	_ZN21btNodeOverlapCallbackD2Ev
	.quad	_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev
	.quad	_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii
	.size	_ZTVZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback, 40

	.type	_ZTSZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback,@object # @_ZTSZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback
	.p2align	4
_ZTSZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback:
	.asciz	"ZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback"
	.size	_ZTSZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback, 104

	.type	_ZTS21btNodeOverlapCallback,@object # @_ZTS21btNodeOverlapCallback
	.section	.rodata._ZTS21btNodeOverlapCallback,"aG",@progbits,_ZTS21btNodeOverlapCallback,comdat
	.weak	_ZTS21btNodeOverlapCallback
	.p2align	4
_ZTS21btNodeOverlapCallback:
	.asciz	"21btNodeOverlapCallback"
	.size	_ZTS21btNodeOverlapCallback, 24

	.type	_ZTI21btNodeOverlapCallback,@object # @_ZTI21btNodeOverlapCallback
	.section	.rodata._ZTI21btNodeOverlapCallback,"aG",@progbits,_ZTI21btNodeOverlapCallback,comdat
	.weak	_ZTI21btNodeOverlapCallback
	.p2align	3
_ZTI21btNodeOverlapCallback:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS21btNodeOverlapCallback
	.size	_ZTI21btNodeOverlapCallback, 16

	.type	_ZTIZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback,@object # @_ZTIZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback
	.section	.rodata,"a",@progbits
	.p2align	4
_ZTIZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback
	.quad	_ZTI21btNodeOverlapCallback
	.size	_ZTIZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback, 24

	.type	_ZTVZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback,@object # @_ZTVZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback
	.p2align	3
_ZTVZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback:
	.quad	0
	.quad	_ZTIZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback
	.quad	_ZN21btNodeOverlapCallbackD2Ev
	.quad	_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackD0Ev
	.quad	_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallback11processNodeEii
	.size	_ZTVZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback, 40

	.type	_ZTSZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback,@object # @_ZTSZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback
	.p2align	4
_ZTSZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback:
	.asciz	"ZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback"
	.size	_ZTSZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback, 113

	.type	_ZTIZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback,@object # @_ZTIZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback
	.p2align	4
_ZTIZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback
	.quad	_ZTI21btNodeOverlapCallback
	.size	_ZTIZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback, 24

	.type	_ZTVZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback,@object # @_ZTVZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback
	.p2align	3
_ZTVZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback:
	.quad	0
	.quad	_ZTIZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback
	.quad	_ZN21btNodeOverlapCallbackD2Ev
	.quad	_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev
	.quad	_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii
	.size	_ZTVZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback, 40

	.type	_ZTSZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback,@object # @_ZTSZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback
	.p2align	4
_ZTSZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback:
	.asciz	"ZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback"
	.size	_ZTSZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback, 110

	.type	_ZTIZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback,@object # @_ZTIZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback
	.p2align	4
_ZTIZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback
	.quad	_ZTI21btNodeOverlapCallback
	.size	_ZTIZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"BVHTRIANGLEMESH"
	.size	.L.str, 16


	.globl	_ZN22btBvhTriangleMeshShapeC1EP23btStridingMeshInterfacebb
	.type	_ZN22btBvhTriangleMeshShapeC1EP23btStridingMeshInterfacebb,@function
_ZN22btBvhTriangleMeshShapeC1EP23btStridingMeshInterfacebb = _ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebb
	.globl	_ZN22btBvhTriangleMeshShapeC1EP23btStridingMeshInterfacebRK9btVector3S4_b
	.type	_ZN22btBvhTriangleMeshShapeC1EP23btStridingMeshInterfacebRK9btVector3S4_b,@function
_ZN22btBvhTriangleMeshShapeC1EP23btStridingMeshInterfacebRK9btVector3S4_b = _ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebRK9btVector3S4_b
	.globl	_ZN22btBvhTriangleMeshShapeD1Ev
	.type	_ZN22btBvhTriangleMeshShapeD1Ev,@function
_ZN22btBvhTriangleMeshShapeD1Ev = _ZN22btBvhTriangleMeshShapeD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
