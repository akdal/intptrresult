	.text
	.file	"libclamav_phishcheck.bc"
	.globl	phishingScan
	.p2align	4, 0x90
	.type	phishingScan,@function
phishingScan:                           # @phishingScan
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$344, %rsp              # imm = 0x158
.Lcfi6:
	.cfi_def_cfa_offset 400
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	24(%r13), %rax
	movq	72(%rax), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_2
# BB#1:
	movq	224(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 192(%rax)
	je	.LBB0_3
.LBB0_2:                                # %.loopexit
	xorl	%eax, %eax
	addq	$344, %rsp              # imm = 0x158
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_3:
	cmpl	$0, 52(%r13)
	jne	.LBB0_5
# BB#4:
	movq	(%r13), %rax
	movq	$0, (%rax)
.LBB0_5:                                # %.preheader131
	cmpl	$0, (%r14)
	jle	.LBB0_2
# BB#6:                                 # %.lr.ph
	xorl	%r12d, %r12d
	movq	%r13, 240(%rsp)         # 8-byte Spill
	jmp	.LBB0_262
.LBB0_7:                                #   in Loop: Header=BB0_262 Depth=1
	movq	32(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB0_288
# BB#8:                                 # %isRealURL.exit.i
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	32(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	callq	cli_regexec
	testl	%eax, %eax
	jne	.LBB0_288
# BB#9:                                 #   in Loop: Header=BB0_262 Depth=1
	testb	$2, 105(%rsp)
	je	.LBB0_12
# BB#10:                                #   in Loop: Header=BB0_262 Depth=1
	movq	32(%rsp), %rsi
	movq	56(%rsp), %rdx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	leaq	104(%rsp), %r9
	callq	domainlist_match
	testl	%eax, %eax
	je	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_262 Depth=1
	movl	$8, 12(%rsp)
.LBB0_12:                               #   in Loop: Header=BB0_262 Depth=1
	movl	$0, 112(%rsp)
	movq	$empty_string, 128(%rsp)
	movq	$0, 120(%rsp)
	movl	$0, 136(%rsp)
	movq	$empty_string, 152(%rsp)
	movq	$0, 144(%rsp)
	movl	$0, 160(%rsp)
	movq	$empty_string, 176(%rsp)
	movq	$0, 168(%rsp)
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	leaq	16(%rsp), %rsi
	leaq	112(%rsp), %r15
	movq	%r15, %rdx
	leaq	12(%rsp), %r8
	callq	url_get_host
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB0_28
.LBB0_13:                               # %.preheader201
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rax
	decl	(%rax)
	jne	.LBB0_17
# BB#14:                                #   in Loop: Header=BB0_13 Depth=2
	movq	8(%rax), %r15
	testq	%r15, %r15
	jne	.LBB0_13
# BB#15:                                #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_17:                               # %string_free.exit.i.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	136(%rsp), %rcx
.LBB0_18:                               # %string_free.exit.i.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_22
# BB#19:                                #   in Loop: Header=BB0_18 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_18
# BB#20:                                #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_22:                               # %string_free.exit4.i.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	160(%rsp), %rcx
.LBB0_23:                               # %string_free.exit4.i.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_27
# BB#24:                                #   in Loop: Header=BB0_23 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_23
# BB#25:                                #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_27
# BB#26:                                #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_27:                               # %free_if_needed.exit.i
                                        #   in Loop: Header=BB0_262 Depth=1
	leal	-100(%rbp), %eax
	cmpl	$15, %eax
	movl	$100, %eax
	cmovael	%eax, %ebp
	jmp	.LBB0_289
.LBB0_28:                               #   in Loop: Header=BB0_262 Depth=1
	testb	$2, 105(%rsp)
	je	.LBB0_32
# BB#29:                                #   in Loop: Header=BB0_262 Depth=1
	movl	12(%rsp), %ebp
	testb	$8, %bpl
	jne	.LBB0_32
# BB#30:                                #   in Loop: Header=BB0_262 Depth=1
	movq	128(%rsp), %rdx
	movq	152(%rsp), %rsi
	movl	$1, %r8d
	movq	%r13, %rdi
	leaq	64(%rsp), %rcx
	leaq	104(%rsp), %r9
	callq	domainlist_match
	testl	%eax, %eax
	je	.LBB0_32
# BB#31:                                #   in Loop: Header=BB0_262 Depth=1
	orl	$8, %ebp
	movl	%ebp, 12(%rsp)
.LBB0_32:                               #   in Loop: Header=BB0_262 Depth=1
	movzwl	108(%rsp), %eax
	movzwl	104(%rsp), %ecx
	movl	$104, %ebp
	testb	$1, %al
	je	.LBB0_34
# BB#33:                                #   in Loop: Header=BB0_262 Depth=1
	movl	%ecx, %eax
	andl	$256, %eax              # imm = 0x100
	testw	%ax, %ax
	je	.LBB0_289
.LBB0_34:                               # %._crit_edge276.i
                                        #   in Loop: Header=BB0_262 Depth=1
	testb	$2, %ch
	je	.LBB0_37
# BB#35:                                #   in Loop: Header=BB0_262 Depth=1
	testb	$8, 12(%rsp)
	jne	.LBB0_37
# BB#36:                                #   in Loop: Header=BB0_262 Depth=1
	andw	106(%rsp), %cx
	movw	%cx, 104(%rsp)
	je	.LBB0_127
.LBB0_37:                               #   in Loop: Header=BB0_262 Depth=1
	testb	$32, %cl
	jne	.LBB0_59
# BB#38:                                # %._crit_edge277.i
                                        #   in Loop: Header=BB0_262 Depth=1
	movq	56(%rsp), %r15
.LBB0_39:                               #   in Loop: Header=BB0_262 Depth=1
	cmpb	$0, (%r15)
	je	.LBB0_75
# BB#40:                                #   in Loop: Header=BB0_262 Depth=1
	testb	$16, %cl
	je	.LBB0_43
# BB#41:                                #   in Loop: Header=BB0_262 Depth=1
	testq	%r15, %r15
	je	.LBB0_43
# BB#42:                                # %isSSL.exit.i
                                        #   in Loop: Header=BB0_262 Depth=1
	movl	$https, %edi
	movl	$8, %edx
	movq	%r15, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_159
.LBB0_43:                               # %isSSL.exit.thread.i
                                        #   in Loop: Header=BB0_262 Depth=1
	movl	$1, %ecx
	movq	%rbx, %rdi
	leaq	16(%rsp), %rsi
	leaq	112(%rsp), %r15
	movq	%r15, %rdx
	leaq	12(%rsp), %r8
	callq	url_get_host
	testl	%eax, %eax
	je	.LBB0_109
# BB#44:                                # %.preheader193.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	movl	%eax, %ebx
.LBB0_45:                               # %.preheader193
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rax
	decl	(%rax)
	jne	.LBB0_49
# BB#46:                                #   in Loop: Header=BB0_45 Depth=2
	movq	8(%rax), %r15
	testq	%r15, %r15
	jne	.LBB0_45
# BB#47:                                #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_49
# BB#48:                                #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_49:                               # %string_free.exit.i119.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	136(%rsp), %rcx
.LBB0_50:                               # %string_free.exit.i119.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_54
# BB#51:                                #   in Loop: Header=BB0_50 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_50
# BB#52:                                #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_54
# BB#53:                                #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_54:                               # %string_free.exit4.i121.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	160(%rsp), %rcx
.LBB0_55:                               # %string_free.exit4.i121.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_142
# BB#56:                                #   in Loop: Header=BB0_55 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_55
# BB#57:                                #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_177
# BB#58:                                #   in Loop: Header=BB0_262 Depth=1
	callq	free
	movl	%ebx, %ebp
	jmp	.LBB0_289
.LBB0_59:                               #   in Loop: Header=BB0_262 Depth=1
	movl	%ecx, 212(%rsp)         # 4-byte Spill
	movq	32(%rsp), %rdi
	movl	$1, %esi
	callq	strchr
	testq	%rax, %rax
	je	.LBB0_90
# BB#60:                                # %.preheader198.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	112(%rsp), %rcx
.LBB0_61:                               # %.preheader198
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_65
# BB#62:                                #   in Loop: Header=BB0_61 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_61
# BB#63:                                #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_65
# BB#64:                                #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_65:                               # %string_free.exit.i93.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	136(%rsp), %rcx
.LBB0_66:                               # %string_free.exit.i93.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_70
# BB#67:                                #   in Loop: Header=BB0_66 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_66
# BB#68:                                #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_70
# BB#69:                                #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_70:                               # %string_free.exit4.i95.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	160(%rsp), %rcx
.LBB0_71:                               # %string_free.exit4.i95.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	movl	$118, %ebp
	jne	.LBB0_289
# BB#72:                                #   in Loop: Header=BB0_71 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_71
# BB#73:                                #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_289
# BB#74:                                #   in Loop: Header=BB0_262 Depth=1
	callq	free
	jmp	.LBB0_289
.LBB0_75:                               # %.preheader185.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	112(%rsp), %rcx
.LBB0_76:                               # %.preheader185
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_80
# BB#77:                                #   in Loop: Header=BB0_76 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_76
# BB#78:                                #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_80
# BB#79:                                #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_80:                               # %string_free.exit.i106.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	136(%rsp), %rcx
.LBB0_81:                               # %string_free.exit.i106.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_85
# BB#82:                                #   in Loop: Header=BB0_81 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_81
# BB#83:                                #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_85
# BB#84:                                #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_85:                               # %string_free.exit4.i108.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	160(%rsp), %rcx
.LBB0_86:                               # %string_free.exit4.i108.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_126
# BB#87:                                #   in Loop: Header=BB0_86 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_86
# BB#88:                                #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_176
# BB#89:                                #   in Loop: Header=BB0_262 Depth=1
	callq	free
	movl	$100, %ebp
	jmp	.LBB0_289
.LBB0_90:                               #   in Loop: Header=BB0_262 Depth=1
	movq	56(%rsp), %rax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
.LBB0_91:                               #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, 216(%rsp)         # 8-byte Spill
	movl	$.L.str.33, %esi
	movq	%rax, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB0_93
# BB#92:                                #   in Loop: Header=BB0_91 Depth=2
	movq	216(%rsp), %rcx         # 8-byte Reload
	leaq	1(%rcx), %r15
	movl	$59, %esi
	movq	%rax, %rdi
	callq	strchr
	testq	%rax, %rax
	jne	.LBB0_91
.LBB0_93:                               # %isEncoded.exit.i
                                        #   in Loop: Header=BB0_262 Depth=1
	movq	232(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rdi
	callq	strlen
	leaq	(,%rax,8), %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movabsq	$-3689348814741910323, %rcx # imm = 0xCCCCCCCCCCCCCCCD
	mulq	%rcx
	shrq	$3, %rdx
	cmpq	%rdx, 216(%rsp)         # 8-byte Folded Reload
	movl	212(%rsp), %ecx         # 4-byte Reload
	jbe	.LBB0_39
# BB#94:                                # %.preheader196.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	112(%rsp), %rcx
.LBB0_95:                               # %.preheader196
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_99
# BB#96:                                #   in Loop: Header=BB0_95 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_95
# BB#97:                                #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_99
# BB#98:                                #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_99:                               # %string_free.exit.i100.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	136(%rsp), %rcx
.LBB0_100:                              # %string_free.exit.i100.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_104
# BB#101:                               #   in Loop: Header=BB0_100 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_100
# BB#102:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_104
# BB#103:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_104:                              # %string_free.exit4.i102.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	160(%rsp), %rcx
.LBB0_105:                              # %string_free.exit4.i102.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	movl	$117, %ebp
	jne	.LBB0_289
# BB#106:                               #   in Loop: Header=BB0_105 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_105
# BB#107:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_289
# BB#108:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
	jmp	.LBB0_289
.LBB0_109:                              #   in Loop: Header=BB0_262 Depth=1
	testb	$2, 105(%rsp)
	je	.LBB0_143
# BB#110:                               #   in Loop: Header=BB0_262 Depth=1
	testb	$8, 12(%rsp)
	jne	.LBB0_143
# BB#111:                               # %.preheader191.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	112(%rsp), %rcx
.LBB0_112:                              # %.preheader191
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_116
# BB#113:                               #   in Loop: Header=BB0_112 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_112
# BB#114:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_116
# BB#115:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_116:                              # %string_free.exit.i125.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	136(%rsp), %rcx
.LBB0_117:                              # %string_free.exit.i125.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_121
# BB#118:                               #   in Loop: Header=BB0_117 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_117
# BB#119:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_121
# BB#120:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_121:                              # %string_free.exit4.i127.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	160(%rsp), %rcx
.LBB0_122:                              # %string_free.exit4.i127.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_289
# BB#123:                               #   in Loop: Header=BB0_122 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_122
# BB#124:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_289
# BB#125:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
	jmp	.LBB0_289
.LBB0_126:                              #   in Loop: Header=BB0_262 Depth=1
	movl	$100, %ebp
	jmp	.LBB0_289
.LBB0_127:                              # %.preheader200.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	112(%rsp), %rcx
.LBB0_128:                              # %.preheader200
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_132
# BB#129:                               #   in Loop: Header=BB0_128 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_128
# BB#130:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_132
# BB#131:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_132:                              # %string_free.exit.i87.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	136(%rsp), %rcx
.LBB0_133:                              # %string_free.exit.i87.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_137
# BB#134:                               #   in Loop: Header=BB0_133 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_133
# BB#135:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_137
# BB#136:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_137:                              # %string_free.exit4.i89.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	160(%rsp), %rcx
.LBB0_138:                              # %string_free.exit4.i89.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_289
# BB#139:                               #   in Loop: Header=BB0_138 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_138
# BB#140:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_289
# BB#141:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
	jmp	.LBB0_289
.LBB0_142:                              #   in Loop: Header=BB0_262 Depth=1
	movl	%ebx, %ebp
	jmp	.LBB0_289
.LBB0_143:                              #   in Loop: Header=BB0_262 Depth=1
	movq	128(%rsp), %rsi
	movq	152(%rsp), %rdx
	movl	$1, %ecx
	movq	%r13, %rdi
	callq	whitelist_match
	testl	%eax, %eax
	je	.LBB0_178
# BB#144:                               # %.preheader189.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	112(%rsp), %rcx
.LBB0_145:                              # %.preheader189
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_149
# BB#146:                               #   in Loop: Header=BB0_145 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_145
# BB#147:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_149
# BB#148:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_149:                              # %string_free.exit.i131.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	136(%rsp), %rcx
.LBB0_150:                              # %string_free.exit.i131.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_154
# BB#151:                               #   in Loop: Header=BB0_150 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_150
# BB#152:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_154
# BB#153:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_154:                              # %string_free.exit4.i133.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	160(%rsp), %rcx
.LBB0_155:                              # %string_free.exit4.i133.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	movl	$111, %ebp
	jne	.LBB0_289
# BB#156:                               #   in Loop: Header=BB0_155 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_155
# BB#157:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_289
# BB#158:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
	jmp	.LBB0_289
.LBB0_159:                              #   in Loop: Header=BB0_262 Depth=1
	movq	32(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB0_161
# BB#160:                               # %isSSL.exit111.i
                                        #   in Loop: Header=BB0_262 Depth=1
	movl	$https, %edi
	movl	$8, %edx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_43
.LBB0_161:                              # %isSSL.exit111.thread.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	112(%rsp), %rcx
.LBB0_162:                              # %isSSL.exit111.thread.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_166
# BB#163:                               #   in Loop: Header=BB0_162 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_162
# BB#164:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_166
# BB#165:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_166:                              # %string_free.exit.i113.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	136(%rsp), %rcx
.LBB0_167:                              # %string_free.exit.i113.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_171
# BB#168:                               #   in Loop: Header=BB0_167 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_167
# BB#169:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_171
# BB#170:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_171:                              # %string_free.exit4.i115.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	160(%rsp), %rcx
.LBB0_172:                              # %string_free.exit4.i115.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	movl	$119, %ebp
	jne	.LBB0_289
# BB#173:                               #   in Loop: Header=BB0_172 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_172
# BB#174:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_289
# BB#175:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
	jmp	.LBB0_289
.LBB0_176:                              #   in Loop: Header=BB0_262 Depth=1
	movl	$100, %ebp
	jmp	.LBB0_289
.LBB0_177:                              #   in Loop: Header=BB0_262 Depth=1
	movl	%ebx, %ebp
	jmp	.LBB0_289
.LBB0_178:                              #   in Loop: Header=BB0_262 Depth=1
	movzwl	104(%rsp), %r15d
	testb	$1, %r15b
	jne	.LBB0_183
.LBB0_179:                              # %free_if_needed.exit165.i
                                        #   in Loop: Header=BB0_262 Depth=1
	movzwl	104(%rsp), %ecx
	movl	12(%rsp), %eax
	testb	$2, %ch
	je	.LBB0_181
# BB#180:                               # %free_if_needed.exit165.i
                                        #   in Loop: Header=BB0_262 Depth=1
	movl	%eax, %ecx
	andl	$8, %ecx
	je	.LBB0_289
.LBB0_181:                              # %free_if_needed.exit165._crit_edge.i
                                        #   in Loop: Header=BB0_262 Depth=1
	movl	$115, %ebp
	testb	$1, %al
	jne	.LBB0_289
# BB#182:                               #   in Loop: Header=BB0_262 Depth=1
	addl	%eax, %eax
	notl	%eax
	andl	$4, %eax
	addl	$116, %eax
	movl	%eax, %ebp
	jmp	.LBB0_289
.LBB0_183:                              #   in Loop: Header=BB0_262 Depth=1
	movq	32(%rsp), %rdi
	movq	56(%rsp), %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_216
# BB#184:                               #   in Loop: Header=BB0_262 Depth=1
	testb	$3, %r15b
	je	.LBB0_201
# BB#185:                               #   in Loop: Header=BB0_262 Depth=1
	movl	$0, 248(%rsp)
	movq	$empty_string, 264(%rsp)
	movq	$0, 256(%rsp)
	movl	$0, 272(%rsp)
	movq	$empty_string, 288(%rsp)
	movq	$0, 280(%rsp)
	movl	$0, 296(%rsp)
	movq	$empty_string, 312(%rsp)
	movq	$0, 304(%rsp)
	movq	%rbx, %rdi
	leaq	248(%rsp), %rsi
	leaq	112(%rsp), %rdx
	callq	get_domain
	movq	%rbx, %rdi
	leaq	272(%rsp), %rsi
	leaq	136(%rsp), %rdx
	callq	get_domain
	movzwl	200(%rsp), %eax
	movw	%ax, 336(%rsp)
	movq	264(%rsp), %rdi
	movq	288(%rsp), %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_231
# BB#186:                               # %.critedge.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	248(%rsp), %rcx
.LBB0_187:                              # %.critedge.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_191
# BB#188:                               #   in Loop: Header=BB0_187 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_187
# BB#189:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_191
# BB#190:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_191:                              # %string_free.exit.i155.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	272(%rsp), %rcx
.LBB0_192:                              # %string_free.exit.i155.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_196
# BB#193:                               #   in Loop: Header=BB0_192 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_192
# BB#194:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_196
# BB#195:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_196:                              # %string_free.exit4.i157.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	296(%rsp), %rcx
.LBB0_197:                              # %string_free.exit4.i157.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_201
# BB#198:                               #   in Loop: Header=BB0_197 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_197
# BB#199:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_201
# BB#200:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_201:                              # %.preheader285.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	112(%rsp), %rcx
.LBB0_202:                              # %.preheader285.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_206
# BB#203:                               #   in Loop: Header=BB0_202 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_202
# BB#204:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_206
# BB#205:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_206:                              # %string_free.exit.i161.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	136(%rsp), %rcx
.LBB0_207:                              # %string_free.exit.i161.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_211
# BB#208:                               #   in Loop: Header=BB0_207 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_207
# BB#209:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_211
# BB#210:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_211:                              # %string_free.exit4.i163.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	160(%rsp), %rcx
.LBB0_212:                              # %string_free.exit4.i163.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_179
# BB#213:                               #   in Loop: Header=BB0_212 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_212
# BB#214:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_179
# BB#215:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
	jmp	.LBB0_179
.LBB0_216:                              # %.preheader187.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	112(%rsp), %rcx
.LBB0_217:                              # %.preheader187
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_221
# BB#218:                               #   in Loop: Header=BB0_217 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_217
# BB#219:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_221
# BB#220:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_221:                              # %string_free.exit.i137.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	136(%rsp), %rcx
.LBB0_222:                              # %string_free.exit.i137.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_226
# BB#223:                               #   in Loop: Header=BB0_222 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_222
# BB#224:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_226
# BB#225:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_226:                              # %string_free.exit4.i139.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	160(%rsp), %rcx
.LBB0_227:                              # %string_free.exit4.i139.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	movl	$102, %ebp
	jne	.LBB0_289
# BB#228:                               #   in Loop: Header=BB0_227 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_227
# BB#229:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_289
# BB#230:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
	jmp	.LBB0_289
.LBB0_231:                              # %.preheader.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	112(%rsp), %rcx
.LBB0_232:                              # %.preheader.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_236
# BB#233:                               #   in Loop: Header=BB0_232 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_232
# BB#234:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_236
# BB#235:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_236:                              # %string_free.exit.i143.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	136(%rsp), %rcx
.LBB0_237:                              # %string_free.exit.i143.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_241
# BB#238:                               #   in Loop: Header=BB0_237 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_237
# BB#239:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_241
# BB#240:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_241:                              # %string_free.exit4.i145.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	160(%rsp), %rcx
.LBB0_242:                              # %string_free.exit4.i145.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_246
# BB#243:                               #   in Loop: Header=BB0_242 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_242
# BB#244:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_246
# BB#245:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_246:                              # %free_if_needed.exit147.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	248(%rsp), %rcx
.LBB0_247:                              # %free_if_needed.exit147.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_251
# BB#248:                               #   in Loop: Header=BB0_247 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_247
# BB#249:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_251
# BB#250:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_251:                              # %string_free.exit.i149.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	272(%rsp), %rcx
.LBB0_252:                              # %string_free.exit.i149.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_256
# BB#253:                               #   in Loop: Header=BB0_252 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_252
# BB#254:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_256
# BB#255:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_256:                              # %string_free.exit4.i151.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	296(%rsp), %rcx
.LBB0_257:                              # %string_free.exit4.i151.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_261
# BB#258:                               #   in Loop: Header=BB0_257 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_257
# BB#259:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_261
# BB#260:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_261:                              # %free_if_needed.exit153.i
                                        #   in Loop: Header=BB0_262 Depth=1
	movl	$103, %ebp
	jmp	.LBB0_289
	.p2align	4, 0x90
.LBB0_262:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_13 Depth 2
                                        #     Child Loop BB0_18 Depth 2
                                        #     Child Loop BB0_23 Depth 2
                                        #     Child Loop BB0_128 Depth 2
                                        #     Child Loop BB0_133 Depth 2
                                        #     Child Loop BB0_138 Depth 2
                                        #     Child Loop BB0_61 Depth 2
                                        #     Child Loop BB0_66 Depth 2
                                        #     Child Loop BB0_71 Depth 2
                                        #     Child Loop BB0_91 Depth 2
                                        #     Child Loop BB0_95 Depth 2
                                        #     Child Loop BB0_100 Depth 2
                                        #     Child Loop BB0_105 Depth 2
                                        #     Child Loop BB0_162 Depth 2
                                        #     Child Loop BB0_167 Depth 2
                                        #     Child Loop BB0_172 Depth 2
                                        #     Child Loop BB0_45 Depth 2
                                        #     Child Loop BB0_50 Depth 2
                                        #     Child Loop BB0_55 Depth 2
                                        #     Child Loop BB0_112 Depth 2
                                        #     Child Loop BB0_117 Depth 2
                                        #     Child Loop BB0_122 Depth 2
                                        #     Child Loop BB0_145 Depth 2
                                        #     Child Loop BB0_150 Depth 2
                                        #     Child Loop BB0_155 Depth 2
                                        #     Child Loop BB0_187 Depth 2
                                        #     Child Loop BB0_192 Depth 2
                                        #     Child Loop BB0_197 Depth 2
                                        #     Child Loop BB0_232 Depth 2
                                        #     Child Loop BB0_237 Depth 2
                                        #     Child Loop BB0_242 Depth 2
                                        #     Child Loop BB0_247 Depth 2
                                        #     Child Loop BB0_252 Depth 2
                                        #     Child Loop BB0_257 Depth 2
                                        #     Child Loop BB0_202 Depth 2
                                        #     Child Loop BB0_207 Depth 2
                                        #     Child Loop BB0_212 Depth 2
                                        #     Child Loop BB0_217 Depth 2
                                        #     Child Loop BB0_222 Depth 2
                                        #     Child Loop BB0_227 Depth 2
                                        #     Child Loop BB0_76 Depth 2
                                        #     Child Loop BB0_81 Depth 2
                                        #     Child Loop BB0_86 Depth 2
                                        #     Child Loop BB0_291 Depth 2
                                        #     Child Loop BB0_296 Depth 2
                                        #     Child Loop BB0_301 Depth 2
	movq	24(%r14), %r15
	cmpq	$0, (%r15,%r12,8)
	je	.LBB0_271
# BB#263:                               #   in Loop: Header=BB0_262 Depth=1
	movw	$512, 106(%rsp)         # imm = 0x200
	movq	8(%r14), %rax
	movq	(%rax,%r12,8), %rbp
	movl	$.L.str.1, %esi
	movl	$5, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	movw	$371, %bx               # imm = 0x173
	movw	$355, %ax               # imm = 0x163
	cmovnew	%ax, %bx
	movw	%bx, 104(%rsp)
	movw	$0, 108(%rsp)
	movl	$src_text, %esi
	movl	$4, %edx
	movq	%rbp, %rdi
	callq	strncmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	movw	%cx, 108(%rsp)
	movl	40(%r13), %eax
	testb	$4, %ah
	je	.LBB0_265
# BB#264:                               #   in Loop: Header=BB0_262 Depth=1
	orl	$512, %ebx              # imm = 0x200
	movw	%bx, 104(%rsp)
.LBB0_265:                              #   in Loop: Header=BB0_262 Depth=1
	movl	%eax, %ecx
	andl	$4096, %ecx             # imm = 0x1000
	andl	$2048, %eax             # imm = 0x800
	jne	.LBB0_267
# BB#266:                               #   in Loop: Header=BB0_262 Depth=1
	movl	%ecx, %edx
	shrl	$12, %edx
	testb	%dl, %dl
	je	.LBB0_268
.LBB0_267:                              #   in Loop: Header=BB0_262 Depth=1
	andl	$61312, %eax            # imm = 0xEF80
	shrl	$7, %eax
	orl	$544, %eax              # imm = 0x220
	testl	%ecx, %ecx
	movw	$528, %cx               # imm = 0x210
	cmovew	%cx, %ax
	movw	%ax, 106(%rsp)
.LBB0_268:                              #   in Loop: Header=BB0_262 Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%r12,8), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	setne	%cl
	movl	%ecx, 16(%rsp)
	movl	$empty_string, %ebx
	cmoveq	%rbx, %rax
	movq	%rax, 32(%rsp)
	movq	$0, 24(%rsp)
	movq	(%r15,%r12,8), %rdi
	callq	blobGetData
	movq	%rax, %rbp
	xorl	%eax, %eax
	testq	%rbp, %rbp
	setne	%al
	movl	%eax, 40(%rsp)
	cmoveq	%rbx, %rbp
	movq	%rbp, 56(%rsp)
	movq	$0, 48(%rsp)
	movl	$0, 64(%rsp)
	movq	$empty_string, 80(%rsp)
	movq	$0, 72(%rsp)
	movq	24(%r14), %rax
	movq	(%rax,%r12,8), %rdi
	callq	blobGetDataSize
	cmpb	$0, -1(%rbp,%rax)
	jne	.LBB0_311
# BB#269:                               #   in Loop: Header=BB0_262 Depth=1
	movl	$-1, 16(%rsp)
	movl	$-1, 40(%rsp)
	movq	8(%r14), %rax
	movq	(%rax,%r12,8), %rdi
	movl	$.L.str.1, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_273
# BB#270:                               #   in Loop: Header=BB0_262 Depth=1
	movq	32(%rsp), %rax
	movq	56(%rsp), %rsi
	movq	%rsi, 32(%rsp)
	movq	%rax, 56(%rsp)
	jmp	.LBB0_274
	.p2align	4, 0x90
.LBB0_271:                              #   in Loop: Header=BB0_262 Depth=1
	movq	8(%r14), %rax
	movq	(%rax,%r12,8), %rdi
	movl	$.L.str.1, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_310
# BB#272:                               #   in Loop: Header=BB0_262 Depth=1
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_310
	.p2align	4, 0x90
.LBB0_273:                              # %._crit_edge
                                        #   in Loop: Header=BB0_262 Depth=1
	movq	32(%rsp), %rsi
.LBB0_274:                              #   in Loop: Header=BB0_262 Depth=1
	movq	24(%r13), %r13
	movl	$0, 12(%rsp)
	testq	%rsi, %rsi
	je	.LBB0_282
# BB#275:                               #   in Loop: Header=BB0_262 Depth=1
	movq	72(%r13), %rbx
	movq	56(%rsp), %rdx
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	32(%rsp), %r15
	movq	56(%rsp), %rbp
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_282
# BB#276:                               #   in Loop: Header=BB0_262 Depth=1
	testb	$64, 104(%rsp)
	je	.LBB0_285
# BB#277:                               #   in Loop: Header=BB0_262 Depth=1
	xorl	%esi, %esi
	movl	$1, %edx
	leaq	16(%rsp), %rdi
	callq	cleanupURL
	xorl	%edx, %edx
	leaq	40(%rsp), %rdi
	leaq	64(%rsp), %rsi
	callq	cleanupURL
	movq	32(%rsp), %r15
	movq	56(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB0_283
# BB#278:                               #   in Loop: Header=BB0_262 Depth=1
	testq	%r15, %r15
	je	.LBB0_284
# BB#279:                               #   in Loop: Header=BB0_262 Depth=1
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_285
# BB#280:                               #   in Loop: Header=BB0_262 Depth=1
	movl	$101, %ebp
	jmp	.LBB0_289
	.p2align	4, 0x90
.LBB0_282:                              #   in Loop: Header=BB0_262 Depth=1
	movl	$100, %ebp
	jmp	.LBB0_289
.LBB0_283:                              #   in Loop: Header=BB0_262 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB0_285
.LBB0_284:                              #   in Loop: Header=BB0_262 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_285:                              # %._crit_edge.i
                                        #   in Loop: Header=BB0_262 Depth=1
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	whitelist_match
	movl	$110, %ebp
	testl	%eax, %eax
	jne	.LBB0_289
# BB#286:                               #   in Loop: Header=BB0_262 Depth=1
	movq	56(%rsp), %rsi
	testq	%rsi, %rsi
	movq	%rbx, %rbp
	je	.LBB0_288
# BB#287:                               # %isURL.exit.i
                                        #   in Loop: Header=BB0_262 Depth=1
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	callq	cli_regexec
	testl	%eax, %eax
	je	.LBB0_7
.LBB0_288:                              # %isNumericURL.exit.thread.i
                                        #   in Loop: Header=BB0_262 Depth=1
	movq	56(%rsp), %rsi
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$113, %ebp
	.p2align	4, 0x90
.LBB0_289:                              # %phishingCheck.exit
                                        #   in Loop: Header=BB0_262 Depth=1
	movq	224(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 192(%rax)
	jne	.LBB0_2
# BB#290:                               # %.preheader.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	16(%rsp), %rcx
	movq	240(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_291:                              # %.preheader
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_295
# BB#292:                               #   in Loop: Header=BB0_291 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_291
# BB#293:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_295
# BB#294:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_295:                              # %string_free.exit.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	40(%rsp), %rcx
	.p2align	4, 0x90
.LBB0_296:                              # %string_free.exit.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_300
# BB#297:                               #   in Loop: Header=BB0_296 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_296
# BB#298:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_300
# BB#299:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_300:                              # %string_free.exit4.i.preheader
                                        #   in Loop: Header=BB0_262 Depth=1
	leaq	64(%rsp), %rcx
	.p2align	4, 0x90
.LBB0_301:                              # %string_free.exit4.i
                                        #   Parent Loop BB0_262 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB0_305
# BB#302:                               #   in Loop: Header=BB0_301 Depth=2
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_301
# BB#303:                               #   in Loop: Header=BB0_262 Depth=1
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_305
# BB#304:                               #   in Loop: Header=BB0_262 Depth=1
	callq	free
.LBB0_305:                              # %free_if_needed.exit
                                        #   in Loop: Header=BB0_262 Depth=1
	addl	$-100, %ebp
	cmpl	$20, %ebp
	ja	.LBB0_307
# BB#306:                               # %switch.lookup.i
                                        #   in Loop: Header=BB0_262 Depth=1
	movslq	%ebp, %rax
	movq	.Lswitch.table(,%rax,8), %rsi
	jmp	.LBB0_308
	.p2align	4, 0x90
.LBB0_307:                              #   in Loop: Header=BB0_262 Depth=1
	movl	$.L.str.56, %esi
.LBB0_308:                              # %phishing_ret_toString.exit
                                        #   in Loop: Header=BB0_262 Depth=1
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$19, %ebp
	ja	.LBB0_312
# BB#309:                               # %phishing_ret_toString.exit
                                        #   in Loop: Header=BB0_262 Depth=1
	jmpq	*.LJTI0_0(,%rbp,8)
.LBB0_310:                              #   in Loop: Header=BB0_262 Depth=1
	incq	%r12
	movslq	(%r14), %rax
	cmpq	%rax, %r12
	jl	.LBB0_262
	jmp	.LBB0_2
.LBB0_311:                              # %.critedge
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB0_2
.LBB0_312:                              # %.critedge115
	movq	(%r13), %rax
	movq	$.L.str.8, (%rax)
	jmp	.LBB0_318
.LBB0_313:                              # %.critedge116
	movq	(%r13), %rax
	movq	$.L.str.7, (%rax)
	jmp	.LBB0_318
.LBB0_314:                              # %.critedge119
	movq	(%r13), %rax
	movq	$.L.str.4, (%rax)
	jmp	.LBB0_318
.LBB0_315:                              # %.critedge120
	movq	(%r13), %rax
	movq	$.L.str.3, (%rax)
	jmp	.LBB0_318
.LBB0_316:                              # %.critedge118
	movq	(%r13), %rax
	movq	$.L.str.5, (%rax)
	jmp	.LBB0_318
.LBB0_317:                              # %.critedge117
	movq	(%r13), %rax
	movq	$.L.str.6, (%rax)
.LBB0_318:                              # %.loopexit
	movl	$1, 52(%r13)
	movq	(%r13), %rax
	movq	(%rax), %rsi
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_2
.Lfunc_end0:
	.size	phishingScan, .Lfunc_end0-phishingScan
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_310
	.quad	.LBB0_310
	.quad	.LBB0_310
	.quad	.LBB0_310
	.quad	.LBB0_310
	.quad	.LBB0_310
	.quad	.LBB0_310
	.quad	.LBB0_310
	.quad	.LBB0_310
	.quad	.LBB0_310
	.quad	.LBB0_310
	.quad	.LBB0_310
	.quad	.LBB0_310
	.quad	.LBB0_310
	.quad	.LBB0_310
	.quad	.LBB0_313
	.quad	.LBB0_314
	.quad	.LBB0_315
	.quad	.LBB0_316
	.quad	.LBB0_317

	.text
	.globl	phishing_init
	.p2align	4, 0x90
	.type	phishing_init,@function
phishing_init:                          # @phishing_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	72(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB1_3
# BB#1:
	cmpl	$0, 192(%rbx)
	jne	.LBB1_5
# BB#2:
	xorl	%ebx, %ebx
	jmp	.LBB1_28
.LBB1_3:
	movl	$200, %edi
	callq	cli_malloc
	movq	%rax, %rbx
	movq	%rbx, 72(%r14)
	testq	%rbx, %rbx
	je	.LBB1_9
# BB#4:
	movl	$1, 192(%rbx)
.LBB1_5:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leaq	160(%rbx), %rdi
	movl	$cloaked_host_regex, %esi
	callq	build_regex
	testl	%eax, %eax
	jne	.LBB1_27
# BB#6:
	leaq	96(%rbx), %r15
	movl	$cctld_regex, %esi
	movq	%r15, %rdi
	callq	build_regex
	testl	%eax, %eax
	jne	.LBB1_27
# BB#7:
	leaq	64(%rbx), %r12
	movl	$tld_regex, %esi
	movq	%r12, %rdi
	callq	build_regex
	testl	%eax, %eax
	je	.LBB1_10
# BB#8:                                 # %free_regex.exit
	movq	%r15, %rdi
.LBB1_26:
	callq	cli_regfree
	jmp	.LBB1_27
.LBB1_9:
	movl	$-114, %ebx
	jmp	.LBB1_28
.LBB1_10:
	movl	$779, %edi              # imm = 0x30B
	callq	cli_malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB1_12
# BB#11:
	movl	$.L.str.11, %esi
	movl	$196, %edx
	movq	%r13, %rdi
	callq	memcpy
	leaq	196(%r13), %rdi
	movl	$.L.str.12, %esi
	movl	$425, %edx              # imm = 0x1A9
	callq	memcpy
	movq	%r13, %rdi
	addq	$621, %rdi              # imm = 0x26D
	movl	$.L.str.13, %esi
	movl	$157, %edx
	callq	memcpy
	movb	$0, 778(%r13)
	jmp	.LBB1_13
.LBB1_12:
	xorl	%r13d, %r13d
.LBB1_13:                               # %str_compose.exit
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	build_regex
	testl	%eax, %eax
	je	.LBB1_15
# BB#14:                                # %free_regex.exit46
	movq	%r15, %rdi
	callq	cli_regfree
	movq	%r12, %rdi
	callq	cli_regfree
	movq	%r13, %rdi
	callq	free
	jmp	.LBB1_27
.LBB1_15:
	movq	%r13, %rdi
	callq	free
	movl	$720, %edi              # imm = 0x2D0
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB1_17
# BB#16:
	movl	$.L.str.14, %esi
	movl	$137, %edx
	movq	%rax, %rdi
	movq	%rax, %rbp
	callq	memcpy
	leaq	137(%rbp), %rdi
	movl	$.L.str.12, %esi
	movl	$425, %edx              # imm = 0x1A9
	callq	memcpy
	movq	%rbp, %rdi
	addq	$562, %rdi              # imm = 0x232
	movl	$.L.str.13, %esi
	movl	$157, %edx
	callq	memcpy
	movq	%rbp, %rsi
	movb	$0, 719(%rsi)
	jmp	.LBB1_18
.LBB1_17:
	xorl	%esi, %esi
.LBB1_18:                               # %str_compose.exit49
	movq	%rsi, (%rsp)            # 8-byte Spill
	leaq	32(%rbx), %rbp
	movq	%rbp, %rdi
	callq	build_regex
	testl	%eax, %eax
	je	.LBB1_22
# BB#19:                                # %free_regex.exit50
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%r15, %rdi
	callq	cli_regfree
	movq	%r12, %rdi
	callq	cli_regfree
	testq	%rbx, %rbx
	je	.LBB1_21
# BB#20:
	movq	%rbx, %rdi
	callq	cli_regfree
.LBB1_21:                               # %free_regex.exit52
	movq	%r13, %rdi
	callq	free
	movq	%rbp, %rdi
	callq	free
.LBB1_27:
	movq	%rbx, %rdi
	callq	free
	movq	$0, 72(%r14)
	movl	$-124, %ebx
.LBB1_28:
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_22:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	movq	%rbx, %rdi
	subq	$-128, %rdi
	movl	$numeric_url_regex, %esi
	callq	build_regex
	testl	%eax, %eax
	je	.LBB1_29
# BB#23:                                # %free_regex.exit53
	movq	%r15, %rdi
	callq	cli_regfree
	movq	%r12, %rdi
	callq	cli_regfree
	testq	%rbx, %rbx
	je	.LBB1_25
# BB#24:
	movq	%rbx, %rdi
	callq	cli_regfree
.LBB1_25:                               # %free_regex.exit55
	movq	%rbp, %rdi
	jmp	.LBB1_26
.LBB1_29:
	movl	$0, 192(%rbx)
	xorl	%ebx, %ebx
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_28
.Lfunc_end1:
	.size	phishing_init, .Lfunc_end1-phishing_init
	.cfi_endproc

	.p2align	4, 0x90
	.type	build_regex,@function
build_regex:                            # @build_regex
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$7, %edx
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	cli_regcomp
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB2_5
# BB#1:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	%ebp, %edi
	movq	%r14, %rsi
	callq	cli_regerror
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_3
# BB#2:
	movl	%ebp, %edi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	cli_regerror
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB2_4
.LBB2_3:
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB2_4:
	movl	$1, %ebx
.LBB2_5:
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	build_regex, .Lfunc_end2-build_regex
	.cfi_endproc

	.globl	phishing_done
	.p2align	4, 0x90
	.type	phishing_done,@function
phishing_done:                          # @phishing_done
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	72(%r14), %rbx
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testq	%rbx, %rbx
	je	.LBB3_4
# BB#1:
	cmpl	$0, 192(%rbx)
	jne	.LBB3_3
# BB#2:                                 # %free_regex.exit
	movq	%rbx, %rdi
	callq	cli_regfree
	leaq	160(%rbx), %rdi
	callq	cli_regfree
	leaq	96(%rbx), %rdi
	callq	cli_regfree
	leaq	64(%rbx), %rdi
	callq	cli_regfree
	movq	%rbx, %rdi
	subq	$-128, %rdi
	callq	cli_regfree
	leaq	32(%rbx), %rdi
	callq	cli_regfree
	movl	$1, 192(%rbx)
.LBB3_3:
	movq	%r14, %rdi
	callq	whitelist_done
	movq	%r14, %rdi
	callq	domainlist_done
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	free
	movq	$0, 72(%r14)
	jmp	.LBB3_5
.LBB3_4:                                # %.critedge
	movq	%r14, %rdi
	callq	whitelist_done
	movq	%r14, %rdi
	callq	domainlist_done
.LBB3_5:
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	cli_dbgmsg              # TAILCALL
.Lfunc_end3:
	.size	phishing_done, .Lfunc_end3-phishing_done
	.cfi_endproc

	.p2align	4, 0x90
	.type	url_get_host,@function
url_get_host:                           # @url_get_host
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 128
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
	movq	%rdi, %r15
	leaq	24(%rdx), %r12
	leaq	24(%rsi), %rax
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	testl	%ecx, %ecx
	cmovneq	%rdx, %r12
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	cmovneq	%rsi, %rax
	movq	16(%rax), %rbp
	testq	%rbp, %rbp
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	je	.LBB4_15
# BB#1:
	movl	$.L.str.28, %esi
	movq	%rbp, %rdi
	callq	strstr
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB4_16
# BB#2:
	addq	$3, %r13
.LBB4_3:                                # %.thread.preheader.i
	leaq	64(%r15), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	jmp	.LBB4_12
	.p2align	4, 0x90
.LBB4_4:                                # %__strcspn_c3.exit125.i
                                        #   in Loop: Header=BB4_12 Depth=1
	movl	$64, %esi
	movq	%r13, %rdi
	callq	strchr
	movq	%rax, %r15
	testq	%r15, %r15
	leaq	-1(%r13,%rbp), %r14
	je	.LBB4_28
# BB#5:                                 #   in Loop: Header=BB4_12 Depth=1
	cmpq	$1, %rbp
	je	.LBB4_7
# BB#6:                                 #   in Loop: Header=BB4_12 Depth=1
	cmpq	%r14, %r15
	ja	.LBB4_28
.LBB4_7:                                #   in Loop: Header=BB4_12 Depth=1
	movl	$46, %esi
	movq	%r15, %rdi
	callq	strrchr
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB4_11
# BB#8:                                 #   in Loop: Header=BB4_12 Depth=1
	movq	%r14, %r13
	subq	%r15, %r13
	movslq	%r13d, %rdi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB4_19
# BB#9:                                 #   in Loop: Header=BB4_12 Depth=1
	shlq	$32, %r13
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	addq	%rax, %r13
	sarq	$32, %r13
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	strncpy
	movb	$0, (%rbx,%r13)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	cli_regexec
	movl	%eax, %r14d
	movq	%rbx, %rdi
	callq	free
	testl	%r14d, %r14d
	jne	.LBB4_11
# BB#10:                                #   in Loop: Header=BB4_12 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	orb	$1, (%rax)
.LBB4_11:                               # %.thread127.i
                                        #   in Loop: Header=BB4_12 Depth=1
	incq	%r15
	movq	%r15, %r13
.LBB4_12:                               # %.thread.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_13 Depth 2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_13:                               #   Parent Loop BB4_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r13,%rbp), %ebx
	testb	%bl, %bl
	setne	%al
	cmpb	$58, %bl
	setne	%dl
	cmpb	$47, %bl
	setne	%cl
	incq	%rbp
	cmpb	$63, %bl
	je	.LBB4_4
# BB#14:                                #   in Loop: Header=BB4_13 Depth=2
	andb	%dl, %al
	andb	%al, %cl
	jne	.LBB4_13
	jmp	.LBB4_4
.LBB4_15:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	testq	%r12, %r12
	jne	.LBB4_43
	jmp	.LBB4_48
.LBB4_16:
	movl	$mailto, %esi
	movl	$7, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB4_20
# BB#17:
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB4_21
# BB#18:                                # %.critedge.i
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	%rbp, %r13
	jmp	.LBB4_3
.LBB4_19:
	movl	$-114, %ebx
	jmp	.LBB4_62
.LBB4_20:
	leaq	7(%rbp), %r13
	xorl	%r14d, %r14d
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB4_27
	jmp	.LBB4_73
.LBB4_21:
	testb	$4, (%rbx)
	movq	%rbp, %r13
	je	.LBB4_3
# BB#22:
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rbx,%rax), %r14
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_23:                               # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rcx
	movsbl	(%rbx,%rcx), %edx
	testl	%edx, %edx
	je	.LBB4_26
# BB#24:                                # %switch.early.test.i
                                        #   in Loop: Header=BB4_23 Depth=1
	cmpl	$58, %edx
	je	.LBB4_26
# BB#25:                                # %switch.early.test.i
                                        #   in Loop: Header=BB4_23 Depth=1
	leaq	1(%rcx), %rsi
	cmpl	$32, %edx
	jne	.LBB4_23
.LBB4_26:                               # %__strcspn_c2.exit.i
	leaq	1(%rbx,%rcx), %r13
	cmpq	%rcx, %rax
	cmoveq	%rbx, %r13
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB4_73
.LBB4_27:
	movq	16(%rsp), %rax          # 8-byte Reload
	orb	$4, (%rax)
.LBB4_28:                               # %isTLD.exit.thread.i
	testq	%r14, %r14
	jne	.LBB4_34
# BB#29:
	leaq	-1(%r13), %r14
	.p2align	4, 0x90
.LBB4_30:                               # =>This Inner Loop Header: Depth=1
	movzbl	1(%r14), %ebx
	incq	%r14
	testb	%bl, %bl
	setne	%al
	cmpb	$58, %bl
	setne	%dl
	cmpb	$47, %bl
	setne	%cl
	cmpb	$63, %bl
	je	.LBB4_32
# BB#31:                                #   in Loop: Header=BB4_30 Depth=1
	andb	%dl, %al
	andb	%al, %cl
	jne	.LBB4_30
.LBB4_32:                               # %__strcspn_c3.exit.i
	testq	%r14, %r14
	jne	.LBB4_34
# BB#33:
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %r14
	addq	%r13, %r14
.LBB4_34:
	testq	%r13, %r13
	je	.LBB4_42
# BB#35:
	testq	%r14, %r14
	je	.LBB4_42
# BB#36:
	movq	%r14, %rbp
	subq	%r13, %rbp
	leaq	1(%rbp), %rdi
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB4_49
# BB#37:
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	strncpy
	movb	$0, (%r15,%rbp)
	movq	%r12, %rcx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB4_38:                               # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB4_51
# BB#39:                                #   in Loop: Header=BB4_38 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB4_38
# BB#40:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB4_51
# BB#41:
	callq	free
	jmp	.LBB4_51
.LBB4_42:                               # %.thread
	testq	%r12, %r12
	je	.LBB4_48
.LBB4_43:
	movl	$empty_string, %r15d
	movq	%r12, %rcx
	movl	$-1, %ebp
	.p2align	4, 0x90
.LBB4_44:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB4_51
# BB#45:                                #   in Loop: Header=BB4_44 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB4_44
# BB#46:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB4_50
# BB#47:
	callq	free
.LBB4_50:
	movl	$empty_string, %r15d
.LBB4_51:                               # %string_assign_null.exit.sink.split
	leaq	16(%r12), %rbx
	movq	%r15, 16(%r12)
	movl	%ebp, (%r12)
	movq	$0, 8(%r12)
	jmp	.LBB4_52
.LBB4_48:                               # %.string_assign_null.exit_crit_edge
	movq	16, %r15
	movl	$16, %ebx
.LBB4_52:                               # %string_assign_null.exit
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB4_54
# BB#53:
	movq	48(%rsp), %rcx          # 8-byte Reload
	subq	%rcx, %r13
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%r13, 72(%rax)
	subq	%rcx, %r14
	movq	%r14, 80(%rax)
.LBB4_54:
	movq	(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB4_61
# BB#55:
	movq	%rbx, %r14
	movl	$114, %ebx
	movq	16(%rsp), %rax          # 8-byte Reload
	testb	$4, (%rax)
	jne	.LBB4_62
# BB#56:
	movl	$32, %esi
	movq	%rbp, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB4_63
	.p2align	4, 0x90
.LBB4_57:                               # %.preheader52
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rax
	decl	(%rax)
	movl	$113, %ebx
	jne	.LBB4_62
# BB#58:                                #   in Loop: Header=BB4_57 Depth=1
	movq	8(%rax), %r12
	testq	%r12, %r12
	jne	.LBB4_57
# BB#59:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB4_62
# BB#60:
	callq	free
	jmp	.LBB4_62
.LBB4_61:
	movl	$101, %ebx
	jmp	.LBB4_62
.LBB4_49:
	movl	$-114, %ebx
	jmp	.LBB4_62
.LBB4_63:
	movq	56(%rsp), %rax          # 8-byte Reload
	testb	$32, 88(%rax)
	je	.LBB4_65
# BB#64:
	movq	40(%rsp), %rdi          # 8-byte Reload
	addq	$160, %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbp, %rsi
	callq	cli_regexec
	testl	%eax, %eax
	je	.LBB4_69
.LBB4_65:
	movq	(%r14), %rbp
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB4_67
# BB#66:
	cmpb	$0, (%rbp)
	je	.LBB4_80
.LBB4_67:                               # %._crit_edge
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %r14
	movl	$0, 12(%rsp)
	leal	-7(%r14), %eax
	cmpl	$8, %eax
	jbe	.LBB4_74
# BB#68:                                # %get_host.exit.critedge51
	xorl	%ebx, %ebx
	jmp	.LBB4_62
	.p2align	4, 0x90
.LBB4_69:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rax
	decl	(%rax)
	movl	$117, %ebx
	jne	.LBB4_62
# BB#70:                                #   in Loop: Header=BB4_69 Depth=1
	movq	8(%rax), %r12
	testq	%r12, %r12
	jne	.LBB4_69
# BB#71:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB4_62
# BB#72:
	callq	free
	jmp	.LBB4_62
.LBB4_73:
	movq	40(%rsp), %r15          # 8-byte Reload
	jmp	.LBB4_3
.LBB4_74:
	leaq	12(%rsp), %rax
	movq	%rax, (%rsp)
	xorl	%ebx, %ebx
	leaq	36(%rsp), %rdx
	leaq	32(%rsp), %rcx
	leaq	28(%rsp), %r8
	leaq	24(%rsp), %r9
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sscanf
	cmpl	%r14d, 12(%rsp)
	jne	.LBB4_62
# BB#75:
	cmpl	$256, 24(%rsp)          # imm = 0x100
	ja	.LBB4_62
# BB#76:
	cmpl	$256, 28(%rsp)          # imm = 0x100
	ja	.LBB4_62
# BB#77:
	cmpl	$256, 36(%rsp)          # imm = 0x100
	ja	.LBB4_62
# BB#78:
	cmpl	$257, 32(%rsp)          # imm = 0x101
	jae	.LBB4_62
# BB#79:                                # %isNumeric.exit
	movq	16(%rsp), %rax          # 8-byte Reload
	orb	$2, (%rax)
	jmp	.LBB4_62
.LBB4_80:
	movl	$100, %ebx
.LBB4_62:                               # %get_host.exit
	movl	%ebx, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	url_get_host, .Lfunc_end4-url_get_host
	.cfi_endproc

	.p2align	4, 0x90
	.type	cleanupURL,@function
cleanupURL:                             # @cleanupURL
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi59:
	.cfi_def_cfa_offset 128
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	16(%r15), %rbp
	movq	%rbp, (%rsp)
	movb	(%rbp), %al
	testb	%al, %al
	je	.LBB5_1
# BB#2:                                 # %.lr.ph.i.preheader
	incq	%rbp
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	andb	$127, %al
	cmpb	$32, %al
	ja	.LBB5_5
# BB#4:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB5_3 Depth=1
	movb	$32, %al
.LBB5_5:                                # %.lr.ph.i
                                        #   in Loop: Header=BB5_3 Depth=1
	movb	%al, -1(%rbp)
	movzbl	(%rbp), %eax
	incq	%rbp
	testb	%al, %al
	jne	.LBB5_3
# BB#6:                                 # %clear_msb.exit.preheader.loopexit
	movq	(%rsp), %rbp
	movb	(%rbp), %bl
	jmp	.LBB5_7
.LBB5_1:
	xorl	%ebx, %ebx
.LBB5_7:                                # %clear_msb.exit.preheader
	callq	__ctype_b_loc
	movq	%rax, %r14
	movq	(%r14), %rax
	movsbq	%bl, %rcx
	testb	$32, 1(%rax,%rcx,2)
	je	.LBB5_11
# BB#8:                                 # %clear_msb.exit.preheader139
	incq	%rbp
	.p2align	4, 0x90
.LBB5_9:                                # %clear_msb.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rsp)
	movq	(%r14), %rax
	movsbq	(%rbp), %rcx
	incq	%rbp
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB5_9
# BB#10:                                # %clear_msb.exit._crit_edge.loopexit
	decq	%rbp
.LBB5_11:                               # %clear_msb.exit._crit_edge
	movq	%rbp, %rdi
	callq	strlen
	testq	%rax, %rax
	je	.LBB5_12
# BB#26:
	leaq	-1(%rbp,%rax), %rbx
	movq	%rbx, 8(%rsp)
	cmpq	%rbx, %rbp
	jae	.LBB5_45
# BB#27:                                # %.preheader59
	movq	(%r14), %rcx
	movsbq	(%rbx), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	je	.LBB5_31
# BB#28:                                # %.lr.ph77.preheader
	leaq	-2(%rbp,%rax), %rbx
	.p2align	4, 0x90
.LBB5_29:                               # %.lr.ph77
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, 8(%rsp)
	movsbq	(%rbx), %rax
	decq	%rbx
	testb	$32, 1(%rcx,%rax,2)
	jne	.LBB5_29
# BB#30:                                # %._crit_edge.loopexit
	incq	%rbx
.LBB5_31:                               # %._crit_edge
	movl	$dotnet, %esi
	movl	$4, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB5_34
# BB#32:
	movl	$adonet, %esi
	movl	$7, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB5_34
# BB#33:
	movl	$aspnet, %esi
	movl	$7, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB5_34
# BB#56:
	cmpq	%rbx, %rbp
	ja	.LBB5_61
	.p2align	4, 0x90
.LBB5_57:                               # %.lr.ph.i28
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$92, (%rbp)
	jne	.LBB5_59
# BB#58:                                #   in Loop: Header=BB5_57 Depth=1
	movb	$47, (%rbp)
.LBB5_59:                               #   in Loop: Header=BB5_57 Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jbe	.LBB5_57
# BB#60:                                # %str_replace.exit.loopexit
	movq	(%rsp), %rbp
	movq	8(%rsp), %rbx
.LBB5_61:                               # %str_replace.exit
	cmpq	%rbx, %rbp
	ja	.LBB5_66
	.p2align	4, 0x90
.LBB5_62:                               # %.lr.ph.i30
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$60, (%rbp)
	jne	.LBB5_64
# BB#63:                                #   in Loop: Header=BB5_62 Depth=1
	movb	$32, (%rbp)
.LBB5_64:                               #   in Loop: Header=BB5_62 Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jbe	.LBB5_62
# BB#65:                                # %str_replace.exit31.loopexit
	movq	(%rsp), %rbp
	movq	8(%rsp), %rbx
.LBB5_66:                               # %str_replace.exit31
	cmpq	%rbx, %rbp
	ja	.LBB5_71
	.p2align	4, 0x90
.LBB5_67:                               # %.lr.ph.i33
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$62, (%rbp)
	jne	.LBB5_69
# BB#68:                                #   in Loop: Header=BB5_67 Depth=1
	movb	$32, (%rbp)
.LBB5_69:                               #   in Loop: Header=BB5_67 Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jbe	.LBB5_67
# BB#70:                                # %str_replace.exit34.loopexit
	movq	(%rsp), %rbp
	movq	8(%rsp), %rbx
.LBB5_71:                               # %str_replace.exit34
	cmpq	%rbx, %rbp
	ja	.LBB5_76
	.p2align	4, 0x90
.LBB5_72:                               # %.lr.ph.i36
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$34, (%rbp)
	jne	.LBB5_74
# BB#73:                                #   in Loop: Header=BB5_72 Depth=1
	movb	$32, (%rbp)
.LBB5_74:                               #   in Loop: Header=BB5_72 Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jbe	.LBB5_72
# BB#75:                                # %str_replace.exit37.loopexit
	movq	(%rsp), %rbp
	movq	8(%rsp), %rbx
	cmpq	%rbx, %rbp
	jbe	.LBB5_77
	jmp	.LBB5_80
	.p2align	4, 0x90
.LBB5_79:
	incq	%rbp
.LBB5_76:                               # %str_replace.exit37
	cmpq	%rbx, %rbp
	ja	.LBB5_80
.LBB5_77:                               # %.lr.ph.i39
	cmpb	$59, (%rbp)
	jne	.LBB5_79
# BB#78:
	movb	$32, (%rbp)
	jmp	.LBB5_79
.LBB5_12:
	testq	%r15, %r15
	je	.LBB5_19
# BB#13:                                # %.preheader.i.preheader
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_14:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB5_18
# BB#15:                                #   in Loop: Header=BB5_14 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_14
# BB#16:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB5_18
# BB#17:
	callq	free
.LBB5_18:                               # %string_free.exit.i7
	movq	$empty_string, 16(%r15)
	movl	$-1, (%r15)
	movq	$0, 8(%r15)
.LBB5_19:                               # %string_assign_null.exit
	testq	%r12, %r12
	je	.LBB5_159
# BB#20:                                # %.preheader.i9.preheader
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB5_21:                               # %.preheader.i9
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB5_25
# BB#22:                                #   in Loop: Header=BB5_21 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_21
	jmp	.LBB5_23
.LBB5_45:
	testq	%r15, %r15
	je	.LBB5_52
# BB#46:                                # %.preheader.i13.preheader
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_47:                               # %.preheader.i13
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB5_51
# BB#48:                                #   in Loop: Header=BB5_47 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_47
# BB#49:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB5_51
# BB#50:
	callq	free
.LBB5_51:                               # %string_free.exit.i14
	movq	$empty_string, 16(%r15)
	movl	$-1, (%r15)
	movq	$0, 8(%r15)
.LBB5_52:                               # %string_assign_null.exit15
	testq	%r12, %r12
	je	.LBB5_159
# BB#53:                                # %.preheader.i17.preheader
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB5_54:                               # %.preheader.i17
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB5_25
# BB#55:                                #   in Loop: Header=BB5_54 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_54
	jmp	.LBB5_23
.LBB5_34:
	testq	%r15, %r15
	je	.LBB5_41
# BB#35:                                # %.preheader.i21.preheader
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_36:                               # %.preheader.i21
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB5_40
# BB#37:                                #   in Loop: Header=BB5_36 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_36
# BB#38:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB5_40
# BB#39:
	callq	free
.LBB5_40:                               # %string_free.exit.i22
	movq	$empty_string, 16(%r15)
	movl	$-1, (%r15)
	movq	$0, 8(%r15)
.LBB5_41:                               # %string_assign_null.exit23
	testq	%r12, %r12
	je	.LBB5_159
# BB#42:                                # %.preheader.i25.preheader
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB5_43:                               # %.preheader.i25
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB5_25
# BB#44:                                #   in Loop: Header=BB5_43 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_43
.LBB5_23:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB5_25
# BB#24:
	callq	free
.LBB5_25:                               # %string_free.exit.i10
	movq	$empty_string, 16(%r12)
	movl	$-1, (%r12)
	movq	$0, 8(%r12)
.LBB5_159:                              # %.critedge5
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_80:                               # %str_replace.exit40
	movq	%rsp, %rbx
	leaq	8(%rsp), %rbp
	movl	$lt, %edx
	movl	$3, %ecx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	str_strip
	movl	$gt, %edx
	movl	$3, %ecx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	str_strip
	movq	(%rsp), %rbx
	movl	$58, %esi
	movq	%rbx, %rdi
	callq	strchr
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB5_81:                               # =>This Inner Loop Header: Depth=1
	testq	%rbp, %rbp
	je	.LBB5_82
# BB#83:                                #   in Loop: Header=BB5_81 Depth=1
	cmpb	$47, 1(%rbp)
	leaq	1(%rbp), %rbp
	je	.LBB5_81
	jmp	.LBB5_84
.LBB5_82:
	movq	%rbx, %rbp
.LBB5_84:                               # %.critedge2
	movq	%rbx, %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_85:                               # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	movsbl	(%rbp,%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB5_87
# BB#86:                                # %switch.early.test
                                        #   in Loop: Header=BB5_85 Depth=1
	leaq	1(%rbx), %rax
	orl	$16, %ecx
	cmpl	$63, %ecx
	jne	.LBB5_85
.LBB5_87:                               # %__strcspn_c2.exit
	testq	%rbx, %rbx
	je	.LBB5_95
# BB#88:                                # %.lr.ph.i55
	callq	__ctype_tolower_loc
	leaq	-1(%rbx), %r8
	testb	$3, %bl
	je	.LBB5_92
# BB#89:                                # %.prol.preheader
	movl	%ebx, %edx
	andb	$3, %dl
	movzbl	%dl, %esi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_90:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	movsbq	(%rbp,%rdx), %rcx
	movzbl	(%rdi,%rcx,4), %ecx
	movb	%cl, (%rbp,%rdx)
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB5_90
# BB#91:                                # %.prol.loopexit.unr-lcssa
	addq	%rdx, %rbp
	subq	%rdx, %rbx
.LBB5_92:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB5_94
	.p2align	4, 0x90
.LBB5_93:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movsbq	(%rbp), %rdx
	movzbl	(%rcx,%rdx,4), %ecx
	movb	%cl, (%rbp)
	movq	(%rax), %rcx
	movsbq	1(%rbp), %rdx
	movzbl	(%rcx,%rdx,4), %ecx
	movb	%cl, 1(%rbp)
	movq	(%rax), %rcx
	movsbq	2(%rbp), %rdx
	movzbl	(%rcx,%rdx,4), %ecx
	movb	%cl, 2(%rbp)
	movq	(%rax), %rcx
	movsbq	3(%rbp), %rdx
	movzbl	(%rcx,%rdx,4), %ecx
	movb	%cl, 3(%rbp)
	addq	$4, %rbp
	addq	$-4, %rbx
	jne	.LBB5_93
.LBB5_94:                               # %str_make_lowercase.exit.loopexit
	movq	(%rsp), %rdx
.LBB5_95:                               # %str_make_lowercase.exit
	movq	8(%rsp), %rbp
	movq	%rdx, %rbx
	cmpq	%rbx, %rbp
	movq	%r14, 64(%rsp)          # 8-byte Spill
	movl	%r13d, 20(%rsp)         # 4-byte Spill
	jbe	.LBB5_104
# BB#96:
	movq	%rbx, %rdi
	callq	strlen
	cmpq	$3, %rax
	jb	.LBB5_104
# BB#97:
	cmpb	$37, (%rbx)
	jne	.LBB5_102
# BB#98:
	movzbl	1(%rbx), %eax
	cmpq	$48, %rax
	movb	2(%rbx), %dl
	leaq	2(%rbx), %rbx
	jne	.LBB5_100
# BB#99:
	movb	$1, %cl
	cmpb	$48, %dl
	je	.LBB5_101
.LBB5_100:                              # %._crit_edge.i.i
	movzbl	%dl, %edx
	movb	hextable(%rax,%rax), %cl
	shlb	$4, %cl
	orb	hextable(%rdx,%rdx), %cl
.LBB5_101:                              # %hex2int.exit.i
	movb	%cl, (%rbx)
.LBB5_102:
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%r15, 56(%rsp)          # 8-byte Spill
	movq	%rbx, (%rsp)
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	leaq	4(%rbx), %rbx
	cmpq	%rbp, %rbx
	jae	.LBB5_103
	.p2align	4, 0x90
.LBB5_111:                              # %.preheader.i57
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_113 Depth 2
	movq	24(%rsp), %r13          # 8-byte Reload
	leaq	1(%r13), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpq	%rbp, %rbx
	jae	.LBB5_110
# BB#112:                               # %.lr.ph.i58
                                        #   in Loop: Header=BB5_111 Depth=1
	leaq	2(%r13), %r14
	movl	$1, %r15d
	subq	%rbx, %r15
	movb	1(%r13), %r12b
	.p2align	4, 0x90
.LBB5_113:                              #   Parent Loop BB5_111 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$37, %r12b
	jne	.LBB5_110
# BB#114:                               #   in Loop: Header=BB5_113 Depth=2
	movzbl	2(%r13), %eax
	cmpq	$48, %rax
	movzbl	3(%r13), %ecx
	jne	.LBB5_116
# BB#115:                               #   in Loop: Header=BB5_113 Depth=2
	movb	$1, %r12b
	cmpb	$48, %cl
	je	.LBB5_117
.LBB5_116:                              # %._crit_edge.i35.i
                                        #   in Loop: Header=BB5_113 Depth=2
	movzbl	%cl, %ecx
	movzbl	hextable(%rax,%rax), %r12d
	shlb	$4, %r12b
	orb	hextable(%rcx,%rcx), %r12b
.LBB5_117:                              # %hex2int.exit36.i
                                        #   in Loop: Header=BB5_113 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movb	%r12b, (%rax)
	leaq	(%r15,%rbp), %rdx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	memmove
	addq	$-2, %rbp
	cmpq	%rbp, %rbx
	jb	.LBB5_113
.LBB5_110:                              # %.critedge.loopexit.i
                                        #   in Loop: Header=BB5_111 Depth=1
	addq	$5, %r13
	cmpq	%rbp, %r13
	movq	%r13, %rbx
	jb	.LBB5_111
.LBB5_103:                              # %.critedge._crit_edge.i
	movq	%rbp, 8(%rsp)
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	movl	20(%rsp), %r13d         # 4-byte Reload
.LBB5_104:                              # %str_hex_to_char.exit
	testl	%r13d, %r13d
	je	.LBB5_105
# BB#118:
	movq	%rsp, %rdi
	leaq	8(%rsp), %rsi
	movl	$.L.str.26, %edx
	movl	$1, %ecx
	callq	str_strip
	movq	(%rsp), %rbx
	movq	8(%rsp), %rbp
	jmp	.LBB5_119
.LBB5_105:                              # %.preheader
	movq	(%rsp), %rbx
	cmpq	%rbp, %rbx
	jbe	.LBB5_149
	jmp	.LBB5_107
	.p2align	4, 0x90
.LBB5_150:                              #   in Loop: Header=BB5_149 Depth=1
	incq	%rbx
	movq	%rbx, (%rsp)
	cmpq	%rbp, %rbx
	ja	.LBB5_107
.LBB5_149:                              # %.lr.ph75
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$32, (%rbx)
	je	.LBB5_150
	jmp	.LBB5_107
	.p2align	4, 0x90
.LBB5_109:                              # %.critedge
                                        #   in Loop: Header=BB5_107 Depth=1
	decq	%rbp
	movq	%rbp, 8(%rsp)
.LBB5_107:                              # %.critedge.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rbp, %rbx
	ja	.LBB5_119
# BB#108:                               # %.lr.ph
                                        #   in Loop: Header=BB5_107 Depth=1
	cmpb	$32, (%rbp)
	je	.LBB5_109
.LBB5_119:                              # %.critedge1
	testl	%r13d, %r13d
	cmovneq	%r15, %r12
	leaq	1(%rbp), %r13
	subq	%rbx, %r13
	leaq	1(%r13), %rdi
	callq	cli_malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB5_151
# BB#120:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	strncpy
	movb	$0, (%r14,%r13)
	movq	%r12, %rcx
	movq	64(%rsp), %r13          # 8-byte Reload
	movl	20(%rsp), %ebx          # 4-byte Reload
	.p2align	4, 0x90
.LBB5_121:                              # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB5_125
# BB#122:                               #   in Loop: Header=BB5_121 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_121
# BB#123:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB5_125
# BB#124:
	callq	free
.LBB5_125:                              # %.loopexit
	testl	%ebx, %ebx
	movq	%r14, 16(%r12)
	movl	$1, (%r12)
	movq	$0, 8(%r12)
	jne	.LBB5_159
# BB#126:
	movq	(%rsp), %r14
	movq	%r14, 40(%rsp)
	movq	%rbp, 32(%rsp)
	cmpq	%r14, %rbp
	jb	.LBB5_142
# BB#127:
	testq	%r14, %r14
	je	.LBB5_142
# BB#128:
	testq	%rbp, %rbp
	je	.LBB5_142
# BB#129:
	leaq	40(%rsp), %rdi
	leaq	32(%rsp), %rsi
	movl	$.L.str.26, %edx
	movl	$1, %ecx
	callq	str_strip
	movq	(%r13), %rax
	movq	40(%rsp), %r14
	movsbq	(%r14), %rcx
	movq	32(%rsp), %rbp
	testb	$8, (%rax,%rcx,2)
	jne	.LBB5_135
# BB#130:
	cmpq	%rbp, %r14
	ja	.LBB5_135
# BB#131:                               # %.lr.ph15.i.preheader
	incq	%r14
	movq	%r14, %rcx
.LBB5_132:                              # %.lr.ph15.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, 40(%rsp)
	movq	(%r13), %rax
	leaq	1(%rcx), %r14
	cmpq	%rbp, %rcx
	ja	.LBB5_134
# BB#133:                               # %.lr.ph15.i
                                        #   in Loop: Header=BB5_132 Depth=1
	movsbq	(%rcx), %rcx
	movzwl	(%rax,%rcx,2), %ecx
	andl	$8, %ecx
	testw	%cx, %cx
	movq	%r14, %rcx
	je	.LBB5_132
.LBB5_134:                              # %.critedge.preheader.i.loopexit
	decq	%r14
.LBB5_135:                              # %.critedge.preheader.i
	cmpq	%r14, %rbp
	jb	.LBB5_141
# BB#136:                               # %.critedge.preheader.i
	movsbq	(%rbp), %rcx
	movzwl	(%rax,%rcx,2), %ecx
	andl	$8, %ecx
	testw	%cx, %cx
	jne	.LBB5_141
# BB#137:                               # %.critedge.i.preheader
	decq	%rbp
	movq	%rbp, %rcx
.LBB5_138:                              # %.critedge.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	-1(%rcx), %rbp
	cmpq	%r14, %rcx
	jb	.LBB5_140
# BB#139:                               # %.critedge.i
                                        #   in Loop: Header=BB5_138 Depth=1
	movsbq	(%rcx), %rcx
	movzwl	(%rax,%rcx,2), %ecx
	andl	$8, %ecx
	testw	%cx, %cx
	movq	%rbp, %rcx
	je	.LBB5_138
.LBB5_140:                              # %.critedge..critedge2_crit_edge.i
	incq	%rbp
	movq	%rbp, 32(%rsp)
.LBB5_141:                              # %.critedge2.i
	movq	%r14, (%rsp)
	movq	%rbp, 8(%rsp)
.LBB5_142:                              # %str_fixup_spaces.exit
	incq	%rbp
	subq	%r14, %rbp
	leaq	1(%rbp), %rdi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_159
# BB#143:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	callq	strncpy
	movb	$0, (%rbx,%rbp)
	movq	%r15, %rcx
.LBB5_144:                              # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB5_148
# BB#145:                               #   in Loop: Header=BB5_144 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_144
# BB#146:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB5_148
# BB#147:
	callq	free
.LBB5_148:                              # %string_free.exit.i
	movq	%rbx, 16(%r15)
	movl	$1, (%r15)
	jmp	.LBB5_158
.LBB5_151:                              # %.critedge4
	testq	%r15, %r15
	je	.LBB5_159
# BB#152:                               # %.preheader.i42.preheader
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB5_153:                              # %.preheader.i42
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB5_157
# BB#154:                               #   in Loop: Header=BB5_153 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB5_153
# BB#155:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB5_157
# BB#156:
	callq	free
.LBB5_157:                              # %string_free.exit.i43
	movq	$empty_string, 16(%r15)
	movl	$-1, (%r15)
.LBB5_158:                              # %.critedge5
	movq	$0, 8(%r15)
	jmp	.LBB5_159
.Lfunc_end5:
	.size	cleanupURL, .Lfunc_end5-cleanupURL
	.cfi_endproc

	.p2align	4, 0x90
	.type	str_strip,@function
str_strip:                              # @str_strip
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi72:
	.cfi_def_cfa_offset 80
.Lcfi73:
	.cfi_offset %rbx, -56
.Lcfi74:
	.cfi_offset %r12, -48
.Lcfi75:
	.cfi_offset %r13, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB6_21
# BB#1:
	movq	(%r15), %rbp
	movq	(%rbx), %r14
	cmpq	%rbp, %r14
	jbe	.LBB6_21
# BB#2:
	movq	%rbp, %rdi
	callq	strlen
	cmpq	%r12, %rax
	jb	.LBB6_21
# BB#3:                                 # %select.unfold.preheader
	testq	%r12, %r12
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	je	.LBB6_8
# BB#4:                                 # %.lr.ph82.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph82
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%r12,%r13), %rbx
	movq	%rbp, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rbx, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB6_8
# BB#6:                                 # %select.unfold
                                        #   in Loop: Header=BB6_5 Depth=1
	addq	%r12, %rbp
	testq	%r13, %r13
	je	.LBB6_8
# BB#7:                                 # %select.unfold
                                        #   in Loop: Header=BB6_5 Depth=1
	subq	%r12, %r13
	cmpq	%r12, %rbx
	ja	.LBB6_5
.LBB6_8:                                # %.critedge66
	movq	%r14, %rax
	subq	%rbp, %rax
	cmpq	%r12, %rax
	jb	.LBB6_13
# BB#9:
	movq	%r14, %rax
	subq	%r12, %rax
	incq	%rax
	cmpq	%rbp, %rax
	jbe	.LBB6_13
# BB#10:                                # %.lr.ph76.preheader
	movq	%r12, %rbx
	negq	%rbx
	.p2align	4, 0x90
.LBB6_11:                               # %.lr.ph76
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%r14,%rbx), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r12, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB6_13
# BB#12:                                #   in Loop: Header=BB6_11 Depth=1
	addq	%rbx, %r14
	leaq	1(%rbx,%r14), %rax
	cmpq	%rbp, %rax
	ja	.LBB6_11
.LBB6_13:                               # %.critedge
	movq	%rbp, (%r15)
	leaq	1(%rbp,%r12), %rbx
	cmpq	%r14, %rbx
	ja	.LBB6_20
# BB#14:                                # %.preheader.lr.ph
	incq	%rbp
	movq	%r12, %r13
	negq	%r13
	.p2align	4, 0x90
.LBB6_16:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_18 Depth 2
	cmpq	%r14, %rbx
	ja	.LBB6_15
# BB#17:                                # %.lr.ph
                                        #   in Loop: Header=BB6_16 Depth=1
	movl	$1, %r15d
	subq	%rbx, %r15
	.p2align	4, 0x90
.LBB6_18:                               #   Parent Loop BB6_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r12, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB6_15
# BB#19:                                #   in Loop: Header=BB6_18 Depth=2
	leaq	(%r15,%r14), %rdx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	memmove
	addq	%r13, %r14
	cmpq	%r14, %rbx
	jbe	.LBB6_18
.LBB6_15:                               # %.critedge1.loopexit
                                        #   in Loop: Header=BB6_16 Depth=1
	leaq	1(%rbp,%r12), %rbx
	incq	%rbp
	cmpq	%r14, %rbx
	jbe	.LBB6_16
.LBB6_20:                               # %.critedge1._crit_edge
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%r14, (%rax)
.LBB6_21:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	str_strip, .Lfunc_end6-str_strip
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_domain,@function
get_domain:                             # @get_domain
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi82:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi83:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi85:
	.cfi_def_cfa_offset 64
.Lcfi86:
	.cfi_offset %rbx, -56
.Lcfi87:
	.cfi_offset %r12, -48
.Lcfi88:
	.cfi_offset %r13, -40
.Lcfi89:
	.cfi_offset %r14, -32
.Lcfi90:
	.cfi_offset %r15, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	16(%r15), %rbp
	movl	$46, %esi
	movq	%rbp, %rdi
	callq	strrchr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB7_6
# BB#1:
	leaq	1(%rbx), %rbp
	leaq	96(%r12), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbp, %rsi
	callq	cli_regexec
	testl	%eax, %eax
	je	.LBB7_9
.LBB7_2:                                # %isTLD.exit.thread
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.LBB7_21
# BB#3:                                 # %isTLD.exit.thread
	movq	%rbx, %rcx
	subq	%rax, %rcx
	testq	%rcx, %rcx
	jle	.LBB7_21
	.p2align	4, 0x90
.LBB7_4:                                # %.lr.ph.i42
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$46, -1(%rbx)
	je	.LBB7_14
# BB#5:                                 #   in Loop: Header=BB7_4 Depth=1
	leaq	-1(%rbx), %rcx
	addq	$-2, %rbx
	cmpq	%rax, %rbx
	movq	%rcx, %rbx
	jae	.LBB7_4
	jmp	.LBB7_21
.LBB7_6:
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB7_7:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB7_41
# BB#8:                                 #   in Loop: Header=BB7_7 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB7_7
	jmp	.LBB7_39
.LBB7_9:
	movq	16(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB7_36
# BB#10:
	movq	%rbx, %rax
	subq	%rsi, %rax
	testq	%rax, %rax
	jle	.LBB7_36
# BB#11:                                # %.lr.ph.i.preheader
	movq	%rbx, %r13
	.p2align	4, 0x90
.LBB7_12:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$46, -1(%r13)
	je	.LBB7_27
# BB#13:                                #   in Loop: Header=BB7_12 Depth=1
	leaq	-1(%r13), %rax
	addq	$-2, %r13
	cmpq	%rsi, %r13
	movq	%rax, %r13
	jae	.LBB7_12
	jmp	.LBB7_36
.LBB7_14:                               # %rfind.exit44
	cmpq	$1, %rbx
	je	.LBB7_21
# BB#15:
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB7_16:                               # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB7_20
# BB#17:                                #   in Loop: Header=BB7_16 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB7_16
# BB#18:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB7_20
# BB#19:
	callq	free
.LBB7_20:                               # %string_assign_ref.exit
	incl	(%r15)
	movq	%rbx, 16(%r14)
	jmp	.LBB7_43
.LBB7_21:                               # %rfind.exit44.thread.preheader
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB7_22:                               # %rfind.exit44.thread
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	decl	(%rcx)
	jne	.LBB7_26
# BB#23:                                #   in Loop: Header=BB7_22 Depth=1
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB7_22
# BB#24:
	movq	16(%rcx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_26
# BB#25:
	callq	free
	movq	16(%r15), %rax
.LBB7_26:                               # %string_assign.exit46
	incl	(%r15)
	jmp	.LBB7_42
.LBB7_27:                               # %rfind.exit
	cmpq	$1, %r13
	je	.LBB7_36
# BB#28:
	leaq	-1(%r13), %rbx
	subq	%rbx, %rbp
	movslq	%ebp, %rdi
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB7_2
# BB#29:                                # %isTLD.exit
	shlq	$32, %rbp
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	addq	%rbp, %rdx
	sarq	$32, %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rax, %rbp
	movq	%rbp, %rdi
	movq	%r13, %rsi
	callq	strncpy
	movq	(%rsp), %rax            # 8-byte Reload
	movb	$0, (%rbp,%rax)
	addq	$64, %r12
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	cli_regexec
	movl	%eax, %r12d
	movq	%rbp, %rdi
	callq	free
	testl	%r12d, %r12d
	je	.LBB7_2
# BB#30:                                # %.critedge.preheader
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB7_31:                               # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB7_35
# BB#32:                                #   in Loop: Header=BB7_31 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB7_31
# BB#33:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB7_35
# BB#34:
	callq	free
.LBB7_35:                               # %string_assign_ref.exit40
	incl	(%r15)
	movq	%r13, 16(%r14)
	jmp	.LBB7_43
.LBB7_36:                               # %.critedge35
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB7_37:                               # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	decl	(%rax)
	jne	.LBB7_41
# BB#38:                                #   in Loop: Header=BB7_37 Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB7_37
.LBB7_39:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB7_41
# BB#40:
	callq	free
.LBB7_41:                               # %string_assign.exit
	incl	(%r15)
	movq	16(%r15), %rax
.LBB7_42:
	movq	%rax, 16(%r14)
.LBB7_43:
	movl	$1, (%r14)
	movq	%r15, 8(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	get_domain, .Lfunc_end7-get_domain
	.cfi_endproc

	.type	src_text,@object        # @src_text
	.section	.rodata,"a",@progbits
src_text:
	.asciz	"src"
	.size	src_text, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"urls.displayLink.data[...]"
	.size	.L.str, 27

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata,"a",@progbits
.L.str.1:
	.asciz	"href"
	.size	.L.str.1, 5

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"Phishcheck: Phishing scan result: %s\n"
	.size	.L.str.2, 38

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Phishing.Heuristics.Email.HexURL"
	.size	.L.str.3, 33

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Phishing.Heuristics.Email.Cloaked.NumericIP"
	.size	.L.str.4, 44

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Phishing.Heuristics.Email.Cloaked.Null"
	.size	.L.str.5, 39

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Phishing.Heuristics.Email.SSL-Spoof"
	.size	.L.str.6, 36

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Phishing.Heuristics.Email.Cloaked.Username"
	.size	.L.str.7, 43

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Phishing.Heuristics.Email.SpoofedDomain"
	.size	.L.str.8, 40

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Phishcheck: href with no contents?\n"
	.size	.L.str.9, 36

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Initializing phishcheck module\n"
	.size	.L.str.10, 32

	.type	cloaked_host_regex,@object # @cloaked_host_regex
	.section	.rodata,"a",@progbits
	.p2align	4
cloaked_host_regex:
	.asciz	"^(0[xX][0-9a-fA-F]+|[0-9]+)(\\.(0[xX][0-9a-fA-F]+|[0-9]+)){0,3}$"
	.size	cloaked_host_regex, 64

	.type	cctld_regex,@object     # @cctld_regex
	.p2align	4
cctld_regex:
	.asciz	"^(a[dfilmoqrtuwxz]|b[bdeghijmorstwyz]|c[ahlmnosuy]|d[ejkmz]|e[cegrstu]|f[ijr]|g[abdeghilmnprtuwy]|h[nrtu]|i[delnqst]|j[emop]|k[eghimwz]|l[birstuv]|m[acglmnoqrstuvwxyz]|n[aegilopru]|om|p[aehkltwy]|qa|r[ow]|s[cdeginorz]|t[dghjklmnorvwz]|u[agyz]|v[enu]|ws|y[etu])$"
	.size	cctld_regex, 262

	.type	tld_regex,@object       # @tld_regex
	.p2align	4
tld_regex:
	.asciz	"^(A[CDEFGILMNOQRSTUWXZ]|B[ABDEFGHIJMNORSTVWYZ]|C[ACDFGHIKLMNORUVXYZ]|D[EJKMOZ]|E[CEGRSTU]|F[IJKMOR]|G[ABDEFGHILMNPQRSTUWY]|H[KMNRTU]|I[DELMNOQRST]|J[EMOP]|K[EGHIMNRWYZ]|L[ABCIKRSTUVY]|M[ACDGHKLMNOPQRSTUVWXYZ]|N[ACEFGILOPRUZ]|OM|P[AEFGHKLMNRSTWY]|QA|R[EOUW]|S[ABCDEGHIJKLMNORTUVYZ]|T[CDFGHJKLMNOPRTVWZ]|U[AGKMSYZ]|V[ACEGINU]|W[FS]|Y[ETU]|Z[AMW]|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|AERO|ARPA|COOP|INFO|JOBS|MOBI|NAME|MUSEUM)$"
	.size	tld_regex, 428

	.type	.L.str.11,@object       # @.str.11
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.11:
	.asciz	"^ *(((http|https|ftp|mailto)://.+)|(([a-zA-Z]([-$_@&a-zA-Z0-9!*\"'(),]|%[0-9a-fA-f]{2})*:(//)?)?(([-$_@&a-zA-Z0-9!*\"'(),]|%[0-9a-fA-f]{2})|\\+)+\\.((([-$_@&a-zA-Z0-9!*\"'(),]|%[0-9a-fA-f]{2})|\\+)+\\.)*"
	.size	.L.str.11, 197

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"(A[CDEFGILMNOQRSTUWXZ]|B[ABDEFGHIJMNORSTVWYZ]|C[ACDFGHIKLMNORUVXYZ]|D[EJKMOZ]|E[CEGRSTU]|F[IJKMOR]|G[ABDEFGHILMNPQRSTUWY]|H[KMNRTU]|I[DELMNOQRST]|J[EMOP]|K[EGHIMNRWYZ]|L[ABCIKRSTUVY]|M[ACDGHKLMNOPQRSTUVWXYZ]|N[ACEFGILOPRUZ]|OM|P[AEFGHKLMNRSTWY]|QA|R[EOUW]|S[ABCDEGHIJKLMNORTUVYZ]|T[CDFGHJKLMNOPRTVWZ]|U[AGKMSYZ]|V[ACEGINU]|W[FS]|Y[ETU]|Z[AMW]|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|AERO|ARPA|COOP|INFO|JOBS|MOBI|NAME|MUSEUM)"
	.size	.L.str.12, 426

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"(/(([-$_@.&a-zA-Z0-9!*\"'(),]|%[0-9a-fA-f]{2})|\\+|=)*)*(\\?(([-$_@.&a-zA-Z0-9!*\"'(),]|%[0-9a-fA-f]{2})+)*)?(#([-$_@.&a-zA-Z0-9!*\"'(),]|%[0-9a-fA-f]{2})+)?)) *$"
	.size	.L.str.13, 158

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"^ *(((http|https|ftp|mailto)://.+)|((([-$_@&a-zA-Z0-9!*\"'(),]|%[0-9a-fA-f]{2})|\\+)+\\.((([-$_@&a-zA-Z0-9!*\"'(),]|%[0-9a-fA-f]{2})|\\+)+\\.)*"
	.size	.L.str.14, 138

	.type	numeric_url_regex,@object # @numeric_url_regex
	.section	.rodata,"a",@progbits
	.p2align	4
numeric_url_regex:
	.asciz	"^ *([a-zA-Z]([-$_@&a-zA-Z0-9!*\"'(),]|%[0-9a-fA-f]{2})*:(//)?)?[0-9]{1,3}(\\.[0-9]{1,3}){3}(:(([-$_@&a-zA-Z0-9!*\"'(),]|%[0-9a-fA-f]{2})|\\+)+)?(/((([-$_@.&a-zA-Z0-9!*\"'(),]|%[0-9a-fA-f]{2})|\\+)+/?)*)?(\\?(([-$_@.&a-zA-Z0-9!*\"'(),]|%[0-9a-fA-f]{2})+)*)?(#([-$_@.&a-zA-Z0-9!*\"'(),]|%[0-9a-fA-f]{2})+)? *$"
	.size	numeric_url_regex, 299

	.type	.L.str.15,@object       # @.str.15
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.15:
	.asciz	"Phishcheck module initialized\n"
	.size	.L.str.15, 31

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Cleaning up phishcheck\n"
	.size	.L.str.16, 24

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Freeing phishcheck struct\n"
	.size	.L.str.17, 27

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Phishcheck cleaned up\n"
	.size	.L.str.18, 23

	.type	empty_string,@object    # @empty_string
	.local	empty_string
	.comm	empty_string,1,1
	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Phishcheck: found Possibly Unwanted: %s\n"
	.size	.L.str.19, 41

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Phishcheck: Compiling regex: %s\n"
	.size	.L.str.20, 33

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Phishcheck: Error in compiling regex:%s\nDisabling phishing checks\n"
	.size	.L.str.21, 67

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Phishcheck: Error in compiling regex, disabling phishing checks. Additionally an Out-of-memory error was encountered while generating a detailed error message\n"
	.size	.L.str.22, 160

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"Phishcheck:Checking url %s->%s\n"
	.size	.L.str.23, 32

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Displayed 'url' is not url:%s\n"
	.size	.L.str.24, 31

	.type	dotnet,@object          # @dotnet
	.section	.rodata,"a",@progbits
dotnet:
	.asciz	".net"
	.size	dotnet, 5

	.type	adonet,@object          # @adonet
adonet:
	.asciz	"ado.net"
	.size	adonet, 8

	.type	aspnet,@object          # @aspnet
aspnet:
	.asciz	"asp.net"
	.size	aspnet, 8

	.type	lt,@object              # @lt
lt:
	.asciz	"&lt"
	.size	lt, 4

	.type	gt,@object              # @gt
gt:
	.asciz	"&gt"
	.size	gt, 4

	.type	.L.str.25,@object       # @.str.25
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.25:
	.asciz	"/?"
	.size	.L.str.25, 3

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	" "
	.size	.L.str.26, 2

	.type	hextable,@object        # @hextable
	.section	.rodata,"a",@progbits
	.p2align	4
hextable:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	14                      # 0xe
	.short	15                      # 0xf
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	14                      # 0xe
	.short	15                      # 0xf
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.size	hextable, 512

	.type	.L.str.27,@object       # @.str.27
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.27:
	.asciz	"Phishcheck:host:%s\n"
	.size	.L.str.27, 20

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"://"
	.size	.L.str.28, 4

	.type	mailto,@object          # @mailto
	.section	.rodata,"a",@progbits
mailto:
	.asciz	"mailto:"
	.size	mailto, 8

	.type	.L.str.29,@object       # @.str.29
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.29:
	.asciz	": "
	.size	.L.str.29, 3

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Phishcheck: Real URL without protocol: %s\n"
	.size	.L.str.30, 43

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	":/?"
	.size	.L.str.31, 4

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"%d.%d.%d.%d%n"
	.size	.L.str.32, 14

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"&#"
	.size	.L.str.33, 3

	.type	https,@object           # @https
	.section	.rodata,"a",@progbits
https:
	.asciz	"https://"
	.size	https, 9

	.type	.L.str.35,@object       # @.str.35
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.35:
	.asciz	"Phishcheck: Encountered a host without a tld? (%s)\n"
	.size	.L.str.35, 52

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"Phishcheck: Weird, a name with only 2 levels (%s)\n"
	.size	.L.str.36, 51

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Clean"
	.size	.L.str.37, 6

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"URLs match after cleanup"
	.size	.L.str.38, 25

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"URL is whitelisted"
	.size	.L.str.39, 19

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"host part of URL is whitelist"
	.size	.L.str.40, 30

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"Hosts match"
	.size	.L.str.41, 12

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"Domains match"
	.size	.L.str.42, 14

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"After redirecting realURL, they match"
	.size	.L.str.43, 38

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"After redirecting realURL, hosts match"
	.size	.L.str.44, 39

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"After redirecting the domains match"
	.size	.L.str.45, 36

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"URL is mailto"
	.size	.L.str.46, 14

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"IP address encountered in hostname"
	.size	.L.str.47, 35

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"Displayed link is not an URL, can't check if phishing or not"
	.size	.L.str.48, 61

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"Link URL is cloaked (null byte %00)"
	.size	.L.str.49, 36

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"Link URL contains username, and real<->displayed hosts don't match."
	.size	.L.str.50, 68

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"Visible links is SSL, real link is not"
	.size	.L.str.51, 39

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"URLs are way too different"
	.size	.L.str.52, 27

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"Host not listed in .pdb -> not checked"
	.size	.L.str.53, 39

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"Embedded image in mail -> clean"
	.size	.L.str.54, 32

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"Embedded hex urls"
	.size	.L.str.55, 18

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"Unknown return code"
	.size	.L.str.56, 20

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	.L.str.37
	.quad	.L.str.38
	.quad	.L.str.41
	.quad	.L.str.42
	.quad	.L.str.53
	.quad	.L.str.43
	.quad	.L.str.44
	.quad	.L.str.45
	.quad	.L.str.56
	.quad	.L.str.56
	.quad	.L.str.39
	.quad	.L.str.40
	.quad	.L.str.54
	.quad	.L.str.48
	.quad	.L.str.46
	.quad	.L.str.50
	.quad	.L.str.47
	.quad	.L.str.55
	.quad	.L.str.49
	.quad	.L.str.51
	.quad	.L.str.52
	.size	.Lswitch.table, 168


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
