	.text
	.file	"testbench.bc"
	.globl	runTestbench
	.p2align	4, 0x90
	.type	runTestbench,@function
runTestbench:                           # @runTestbench
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movl	%edi, %r12d
	movslq	%r12d, %rbx
	leaq	(,%rbx,8), %rdi
	callq	malloc
	movq	%rax, %rbp
	movl	$.L.str, %esi
	movq	%r15, %rdi
	callq	fopen
	movq	%rax, %r15
	movl	$4, %edx
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rcx
	callq	fread
	movq	%r15, %rdi
	callq	fclose
	testl	%ebx, %ebx
	jle	.LBB0_8
# BB#1:                                 # %.preheader.us.preheader
	movl	%r12d, %r15d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_3 Depth 2
	movq	%r15, %rbp
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB0_3:                                #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r14), %esi
	movzbl	%sil, %edi
	shrl	$8, %esi
	callq	calc
	movl	%eax, (%r14,%rbx,4)
	addq	$4, %r14
	decq	%rbp
	jne	.LBB0_3
# BB#4:                                 # %._crit_edge49.us
                                        #   in Loop: Header=BB0_2 Depth=1
	incl	%r13d
	cmpl	%r12d, %r13d
	jne	.LBB0_2
# BB#5:                                 # %._crit_edge51
	testl	%r12d, %r12d
	jle	.LBB0_8
# BB#6:                                 # %.lr.ph.preheader
	movq	stdout(%rip), %r14
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%rbx,4), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rbp,4), %edx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	subq	$-128, %rbp
	cmpq	%rbx, %rbp
	jl	.LBB0_7
.LBB0_8:                                # %._crit_edge
	movq	(%rsp), %rdi            # 8-byte Reload
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end0:
	.size	runTestbench, .Lfunc_end0-runTestbench
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"r"
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%8.8X\n"
	.size	.L.str.1, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
