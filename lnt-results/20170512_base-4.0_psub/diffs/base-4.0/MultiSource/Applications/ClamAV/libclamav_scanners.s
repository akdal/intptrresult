	.text
	.file	"libclamav_scanners.bc"
	.globl	cli_scandir
	.p2align	4, 0x90
	.type	cli_scandir,@function
cli_scandir:                            # @cli_scandir
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	callq	opendir
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_21
# BB#1:                                 # %.preheader
	movq	%r13, %rdi
	callq	readdir
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_19
# BB#2:                                 # %.lr.ph
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rbp)
	je	.LBB0_18
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpb	$46, 19(%rbp)
	jne	.LBB0_8
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpb	$0, 20(%rbp)
	je	.LBB0_18
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpb	$46, 20(%rbp)
	jne	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpb	$0, 21(%rbp)
	je	.LBB0_18
	.p2align	4, 0x90
.LBB0_8:                                # %.thread
                                        #   in Loop: Header=BB0_3 Depth=1
	addq	$19, %rbp
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	strlen
	leaq	2(%rbx,%rax), %rdi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_9
# BB#10:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	movq	%rbp, %rcx
	callq	sprintf
	movl	$1, %edi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	__lxstat
	cmpl	$-1, %eax
	je	.LBB0_17
# BB#11:                                #   in Loop: Header=BB0_3 Depth=1
	movzwl	32(%rsp), %eax
	andl	$61440, %eax            # imm = 0xF000
	cmpl	$32768, %eax            # imm = 0x8000
	je	.LBB0_15
# BB#12:                                #   in Loop: Header=BB0_3 Depth=1
	movzwl	%ax, %eax
	cmpl	$16384, %eax            # imm = 0x4000
	jne	.LBB0_17
# BB#13:                                #   in Loop: Header=BB0_3 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	cli_scandir
	cmpl	$1, %eax
	jne	.LBB0_17
	jmp	.LBB0_14
.LBB0_15:                               #   in Loop: Header=BB0_3 Depth=1
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB0_17
# BB#16:                                # %cli_scanfile.exit
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%ebp, %edi
	movq	%r15, %rsi
	callq	cli_magic_scandesc
	movq	%r15, %r12
	movl	%eax, %r15d
	movl	%ebp, %edi
	callq	close
	cmpl	$1, %r15d
	movq	%r12, %r15
	leaq	8(%rsp), %r12
	je	.LBB0_14
	.p2align	4, 0x90
.LBB0_17:                               # %cli_scanfile.exit.thread
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rbx, %rdi
	callq	free
.LBB0_18:                               # %.backedge
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%r13, %rdi
	callq	readdir
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB0_3
.LBB0_19:                               # %._crit_edge
	movq	%r13, %rdi
	callq	closedir
	xorl	%eax, %eax
	jmp	.LBB0_20
.LBB0_21:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movl	$-115, %eax
	jmp	.LBB0_20
.LBB0_9:
	movq	%r13, %rdi
	callq	closedir
	movl	$-114, %eax
	jmp	.LBB0_20
.LBB0_14:
	movq	%rbx, %rdi
	callq	free
	movq	%r13, %rdi
	callq	closedir
	movl	$1, %eax
.LBB0_20:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cli_scandir, .Lfunc_end0-cli_scandir
	.cfi_endproc

	.globl	cli_magic_scandesc
	.p2align	4, 0x90
	.type	cli_magic_scandesc,@function
cli_magic_scandesc:                     # @cli_magic_scandesc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$680, %rsp              # imm = 0x2A8
.Lcfi19:
	.cfi_def_cfa_offset 736
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %ebx
	leaq	536(%rsp), %rdx
	movl	$1, %edi
	movl	%ebx, %esi
	callq	__fxstat
	cmpl	$-1, %eax
	je	.LBB1_3
# BB#1:
	movq	584(%rsp), %rsi
	cmpq	$5, %rsi
	jg	.LBB1_4
# BB#2:
	xorl	%ebp, %ebp
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	jmp	.LBB1_42
.LBB1_3:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
	movl	$-123, %ebp
	jmp	.LBB1_42
.LBB1_4:
	cmpq	$0, 24(%r14)
	je	.LBB1_12
# BB#5:
	movl	40(%r14), %eax
	testl	%eax, %eax
	je	.LBB1_22
# BB#6:
	testb	$1, %al
	je	.LBB1_13
# BB#7:
	movq	32(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB1_13
# BB#8:
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	je	.LBB1_13
# BB#9:
	movl	44(%r14), %esi
	cmpl	%ecx, %esi
	jbe	.LBB1_13
# BB#10:
	xorl	%ebp, %ebp
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	$1, 41(%r14)
	je	.LBB1_42
# BB#11:
	movq	(%r14), %rax
	movq	$.L.str.10, (%rax)
	movl	$1, %ebp
	jmp	.LBB1_42
.LBB1_12:
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-116, %ebp
	jmp	.LBB1_42
.LBB1_13:
	testb	$2, %al
	je	.LBB1_16
# BB#14:
	movl	48(%r14), %esi
	cmpl	$16, %esi
	jb	.LBB1_16
# BB#15:
	xorl	%ebp, %ebp
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_42
.LBB1_16:
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebx, %edi
	callq	lseek
	movq	24(%r14), %rsi
	movl	%ebx, %edi
	callq	cli_filetype2
	movl	%eax, %r15d
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebx, %edi
	callq	lseek
	cmpl	$504, %r15d             # imm = 0x1F8
	jne	.LBB1_24
# BB#17:                                # %.thread186
	incl	44(%r14)
	leaq	16(%rsp), %rdx
	movl	$1, %edi
	movl	%ebx, %esi
	callq	__fxstat
	movl	$504, %ebp              # imm = 0x1F8
	testl	%eax, %eax
	jne	.LBB1_20
# BB#18:
	movl	$61440, %eax            # imm = 0xF000
	andl	40(%rsp), %eax
	cmpl	$32768, %eax            # imm = 0x8000
	jne	.LBB1_20
# BB#19:
	xorl	%eax, %eax
	cmpq	$65535, 64(%rsp)        # imm = 0xFFFF
	setg	%al
	leal	501(%rax,%rax,2), %ebp
.LBB1_20:
	movq	(%r14), %rsi
	movl	%ebx, %edi
	callq	cli_check_mydoom_log
	movl	%eax, %r13d
	movl	%ebp, %r15d
.LBB1_21:                               # %cli_scantnef.exit.thread.thread220
	decl	44(%r14)
	movb	$1, %bpl
	cmpl	$1, %r13d
	jne	.LBB1_33
	jmp	.LBB1_36
.LBB1_22:
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	cli_scandesc
	movl	%eax, %ebp
	cmpl	$1, %ebp
	jne	.LBB1_42
# BB#23:
	movq	(%r14), %rax
	movq	(%rax), %rsi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	cli_dbgmsg
	movl	$1, %ebp
	jmp	.LBB1_42
.LBB1_24:
	movq	24(%r14), %rax
	cmpw	$0, 4(%rax)
	je	.LBB1_27
# BB#25:
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	cli_scanraw
	movl	%eax, %r13d
	movl	$1, %ebp
	cmpl	$1, %r13d
	je	.LBB1_42
# BB#26:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebx, %edi
	callq	lseek
.LBB1_27:
	cmpl	$529, %r15d             # imm = 0x211
	jne	.LBB1_43
# BB#28:                                # %.thread184
	incl	48(%r14)
	testb	$2, 40(%r14)
	je	.LBB1_31
# BB#29:
	movq	56(%r14), %rax
	testb	$1, 16(%rax)
	je	.LBB1_31
# BB#30:
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	cli_scanmail
	movl	%eax, %r13d
.LBB1_31:                               # %.thread196
	decl	48(%r14)
	movb	$1, %bpl
	movl	$529, %r15d             # imm = 0x211
.LBB1_32:
	cmpl	$1, %r13d
	je	.LBB1_36
.LBB1_33:
	cmpl	$504, %r15d             # imm = 0x1F8
	je	.LBB1_36
# BB#34:
	movq	24(%r14), %rax
	cmpw	$0, 4(%rax)
	jne	.LBB1_36
# BB#35:
	movzbl	%bpl, %ecx
	movl	%ebx, %edi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	cli_scanraw
	movl	$1, %ebp
	cmpl	$1, %eax
	je	.LBB1_42
.LBB1_36:
	incl	44(%r14)
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebx, %edi
	callq	lseek
	cmpl	$502, %r15d             # imm = 0x1F6
	jne	.LBB1_40
# BB#37:
	testb	$32, 40(%r14)
	je	.LBB1_40
# BB#38:
	movq	56(%r14), %rax
	cmpl	$0, (%rax)
	je	.LBB1_40
# BB#39:
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	cli_scanpe
	movl	%eax, %r13d
.LBB1_40:
	decl	44(%r14)
	cmpl	$-124, %r13d
	movl	%r13d, %ebp
	jne	.LBB1_42
# BB#41:
	movl	$-124, %edi
	callq	cl_strerror
	movq	%rax, %rcx
	xorl	%ebp, %ebp
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movq	%rcx, %rdx
	callq	cli_dbgmsg
.LBB1_42:
	movl	%ebp, %eax
	addq	$680, %rsp              # imm = 0x2A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_43:
	movl	44(%r14), %eax
	incl	%eax
	movl	%eax, 44(%r14)
	movl	%r15d, %ecx
	addl	$-501, %ecx             # imm = 0xFE0B
	cmpl	$35, %ecx
	ja	.LBB1_21
# BB#44:
	movl	$501, %ebp              # imm = 0x1F5
	jmpq	*.LJTI1_0(,%rcx,8)
.LBB1_45:
	testb	$32, 41(%r14)
	je	.LBB1_21
# BB#46:
	movq	56(%r14), %rax
	cmpl	$0, 4(%rax)
	je	.LBB1_21
# BB#47:
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	cli_scanelf
	movl	%eax, %r13d
	jmp	.LBB1_21
.LBB1_48:
	testb	$1, 40(%r14)
	je	.LBB1_21
# BB#49:
	movq	56(%r14), %rax
	testb	$1, 9(%rax)
	je	.LBB1_21
# BB#50:
	movl	$1, %edx
	jmp	.LBB1_54
.LBB1_51:
	testb	$1, 40(%r14)
	je	.LBB1_21
# BB#52:
	movq	56(%r14), %rax
	testb	$1, 9(%rax)
	je	.LBB1_21
# BB#53:
	xorl	%edx, %edx
.LBB1_54:                               # %cli_scantnef.exit.thread.thread
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	cli_scantar
	movl	%eax, %r13d
	jmp	.LBB1_21
.LBB1_55:
	testb	$1, 40(%r14)
	je	.LBB1_21
# BB#56:
	movq	56(%r14), %rax
	testb	$4, 8(%rax)
	je	.LBB1_21
# BB#57:
	movl	$.L.str.56, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebx, %edi
	callq	dup
	movl	$.L.str.57, %esi
	movl	%eax, %edi
	callq	gzdopen
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testq	%rax, %rax
	je	.LBB1_169
# BB#58:
	leaq	16(%rsp), %rsi
	leaq	4(%rsp), %rdx
	xorl	%edi, %edi
	callq	cli_gentempfd
	movl	%eax, %r13d
	testl	%r13d, %r13d
	je	.LBB1_188
# BB#59:
	movl	$.L.str.59, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	gzclose
	movl	$507, %r15d             # imm = 0x1FB
	jmp	.LBB1_21
.LBB1_60:
	testb	$1, 40(%r14)
	je	.LBB1_63
# BB#61:
	movq	56(%r14), %rcx
	testb	$2, 8(%rcx)
	je	.LBB1_63
# BB#62:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	cli_scanzip
	movl	%eax, %r13d
	movl	44(%r14), %eax
.LBB1_63:
	decl	%eax
	movl	%eax, 44(%r14)
	movb	$1, %bpl
	movl	$508, %r15d             # imm = 0x1FC
	testb	$1, 40(%r14)
	je	.LBB1_32
# BB#64:
	movq	56(%r14), %rax
	movl	8(%rax), %eax
	testb	$2, %al
	je	.LBB1_32
# BB#65:
	cmpq	$1048577, 584(%rsp)     # imm = 0x100001
	jl	.LBB1_32
# BB#66:
	xorl	%ebp, %ebp
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$1, %r13d
	jne	.LBB1_33
	jmp	.LBB1_36
.LBB1_67:
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB1_21
.LBB1_68:
	testb	$1, 40(%r14)
	je	.LBB1_21
# BB#69:
	movq	56(%r14), %rax
	testb	$16, 9(%rax)
	je	.LBB1_21
# BB#70:
	xorl	%edx, %edx
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	cli_scanarj
	movl	%eax, %r13d
	jmp	.LBB1_21
.LBB1_71:
	testb	$4, 40(%r14)
	je	.LBB1_21
# BB#72:
	movq	56(%r14), %rax
	cmpb	$0, 8(%rax)
	jns	.LBB1_21
# BB#73:
	movl	$.L.str.105, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r12
	movl	$448, %esi              # imm = 0x1C0
	movq	%r12, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB1_185
# BB#74:
	movl	$.L.str.106, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_dbgmsg
	jmp	.LBB1_100
.LBB1_75:
	testb	$1, 40(%r14)
	je	.LBB1_21
# BB#76:
	movq	56(%r14), %rax
	testb	$32, 8(%rax)
	je	.LBB1_21
# BB#77:
	xorl	%edx, %edx
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	cli_scanmscab
	movl	%eax, %r13d
	jmp	.LBB1_21
.LBB1_78:
	testb	$1, 40(%r14)
	je	.LBB1_21
# BB#79:
	movq	56(%r14), %rax
	testb	$64, 8(%rax)
	je	.LBB1_21
# BB#80:
	xorl	%r13d, %r13d
	movl	$.L.str.103, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %rbp
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbp, %rdi
	callq	mkdir
	testl	%eax, %eax
	jne	.LBB1_87
# BB#81:
	movl	%ebx, %edi
	movq	%rbp, %rsi
	callq	chm_unpack
	jmp	.LBB1_165
.LBB1_82:
	testb	$1, 40(%r14)
	je	.LBB1_21
# BB#83:
	movq	56(%r14), %rax
	testb	$4, 9(%rax)
	je	.LBB1_21
# BB#84:
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	cli_scansis
	movl	%eax, %r13d
	jmp	.LBB1_21
.LBB1_85:
	movq	56(%r14), %rax
	testb	$2, 20(%rax)
	je	.LBB1_21
# BB#86:
	xorl	%r13d, %r13d
	movl	$.L.str.122, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %rbp
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbp, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB1_164
.LBB1_87:
	movl	$.L.str.104, %edi
	jmp	.LBB1_141
.LBB1_88:
	testb	$2, 41(%r14)
	je	.LBB1_21
# BB#89:
	movq	56(%r14), %rax
	testb	$8, 20(%rax)
	je	.LBB1_21
# BB#90:
	movq	(%r14), %rbp
	movl	%ebx, %edi
	callq	cli_check_jpeg_exploit
	xorl	%r13d, %r13d
	cmpl	$1, %eax
	jne	.LBB1_21
# BB#91:
	movq	$.L.str.124, (%rbp)
	movl	$1, %r13d
	jmp	.LBB1_21
.LBB1_92:
	testb	$2, 41(%r14)
	je	.LBB1_21
# BB#93:
	movq	56(%r14), %rax
	testb	$4, 20(%rax)
	je	.LBB1_21
# BB#94:
	movq	(%r14), %rbp
	movl	%ebx, %edi
	callq	cli_check_riff_exploit
	xorl	%r13d, %r13d
	cmpl	$2, %eax
	jne	.LBB1_21
# BB#95:
	movq	$.L.str.123, (%rbp)
	movl	$1, %r13d
	jmp	.LBB1_21
.LBB1_96:
	testb	$1, 40(%r14)
	je	.LBB1_21
# BB#97:
	movq	56(%r14), %rax
	testb	$2, 9(%rax)
	je	.LBB1_21
# BB#98:
	movl	$.L.str.119, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r12
	movl	$448, %esi              # imm = 0x1C0
	movq	%r12, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB1_171
# BB#99:
	movl	$.L.str.120, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_errmsg
.LBB1_100:                              # %cli_scantnef.exit.thread.thread
	movq	%r12, %rdi
	jmp	.LBB1_142
.LBB1_101:
	testb	$2, 40(%r14)
	je	.LBB1_21
# BB#102:
	movq	56(%r14), %rax
	testb	$2, 16(%rax)
	je	.LBB1_21
# BB#103:
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %rbp
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbp, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB1_173
# BB#104:
	movl	$.L.str.100, %edi
	jmp	.LBB1_141
.LBB1_105:
	movq	56(%r14), %rax
	testb	$16, 20(%rax)
	je	.LBB1_21
# BB#106:
	leaq	16(%rsp), %rdx
	movl	$1, %edi
	movl	%ebx, %esi
	callq	__fxstat
	cmpl	$-1, %eax
	je	.LBB1_166
# BB#107:
	xorl	%r13d, %r13d
	movl	$16, %esi
	xorl	%edx, %edx
	movl	%ebx, %edi
	callq	lseek
	testq	%rax, %rax
	js	.LBB1_168
# BB#108:
	movl	$4294967280, %ebp       # imm = 0xFFFFFFF0
	addq	64(%rsp), %rbp
	movl	%ebp, %r15d
	movq	%r15, %rdi
	callq	cli_malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB1_183
# BB#109:
	movq	%r15, %rdi
	callq	cli_malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB1_198
# BB#110:
	movl	%ebx, %edi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	read
	cmpl	%ebp, %eax
	jne	.LBB1_201
# BB#111:                               # %.preheader205
	testl	%ebp, %ebp
	je	.LBB1_123
# BB#112:                               # %.lr.ph210.preheader
	movl	%ebp, %r9d
	cmpq	$31, %r9
	jbe	.LBB1_116
# BB#113:                               # %min.iters.checked
	movl	%ebp, %r8d
	andl	$31, %r8d
	movq	%r9, %rcx
	subq	%r8, %rcx
	je	.LBB1_116
# BB#114:                               # %vector.memcheck
	leaq	(%r13,%r9), %rdx
	cmpq	%rdx, %r12
	jae	.LBB1_231
# BB#115:                               # %vector.memcheck
	leaq	(%r12,%r9), %rdx
	cmpq	%rdx, %r13
	jae	.LBB1_231
.LBB1_116:
	xorl	%ecx, %ecx
.LBB1_117:                              # %.lr.ph210.preheader232
	subl	%ecx, %ebp
	leaq	-1(%r9), %rsi
	subq	%rcx, %rsi
	andq	$3, %rbp
	je	.LBB1_120
# BB#118:                               # %.lr.ph210.prol.preheader
	negq	%rbp
.LBB1_119:                              # %.lr.ph210.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r13,%rcx), %edx
	notb	%dl
	movb	%dl, (%r12,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB1_119
.LBB1_120:                              # %.lr.ph210.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB1_123
# BB#121:                               # %.lr.ph210.preheader232.new
	subq	%rcx, %r9
	leaq	3(%r12,%rcx), %rdx
	leaq	3(%r13,%rcx), %rcx
.LBB1_122:                              # %.lr.ph210
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rcx), %eax
	notb	%al
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	notb	%al
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	notb	%al
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	notb	%al
	movb	%al, (%rdx)
	addq	$4, %rdx
	addq	$4, %rcx
	addq	$-4, %r9
	jne	.LBB1_122
.LBB1_123:                              # %._crit_edge211
	movq	%r13, %rdi
	callq	free
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %rcx
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rcx, %rdi
	callq	open
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB1_213
# BB#124:
	movl	%ebp, %edi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	write
	cmpq	$-1, %rax
	je	.LBB1_214
# BB#125:
	movq	%r12, %rdi
	callq	free
	movl	%ebp, %edi
	callq	fsync
	cmpl	$-1, %eax
	je	.LBB1_224
# BB#126:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	movl	$.L.str.133, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebp, %edi
	movq	%r14, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %r13d
	cmpl	$1, %r13d
	jne	.LBB1_128
# BB#127:
	movq	(%r14), %rax
	movq	(%rax), %rsi
	movl	$.L.str.134, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_128:
	movl	%ebp, %edi
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB1_237
# BB#129:
	movl	$.L.str.135, %edi
	xorl	%eax, %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	cli_dbgmsg
	jmp	.LBB1_238
.LBB1_130:
	testb	$64, 41(%r14)
	je	.LBB1_21
# BB#131:
	movq	56(%r14), %rax
	testb	$4, 12(%rax)
	je	.LBB1_21
# BB#132:
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %rbp
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbp, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB1_174
# BB#133:
	movl	$.L.str.125, %edi
	jmp	.LBB1_141
.LBB1_134:
	movq	56(%r14), %rax
	testb	$1, 20(%rax)
	je	.LBB1_21
# BB#135:
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %rbp
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbp, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB1_167
# BB#136:
	movl	$.L.str.101, %edi
	jmp	.LBB1_141
.LBB1_137:
	testb	$2, 40(%r14)
	je	.LBB1_21
# BB#138:
	movq	56(%r14), %rax
	testb	$4, 16(%rax)
	je	.LBB1_21
# BB#139:
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %rbp
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbp, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB1_175
# BB#140:
	movl	$.L.str.102, %edi
.LBB1_141:                              # %cli_scantnef.exit.thread.thread
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	%rbp, %rdi
.LBB1_142:                              # %cli_scantnef.exit.thread.thread
	callq	free
	movl	$-118, %r13d
	jmp	.LBB1_21
.LBB1_143:
	testb	$16, 40(%r14)
	je	.LBB1_21
# BB#144:
	movq	56(%r14), %rax
	testb	$1, 12(%rax)
	je	.LBB1_21
# BB#145:
	movl	$.L.str.94, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r15
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	open
	movl	%eax, %r12d
	testl	%r12d, %r12d
	js	.LBB1_181
# BB#146:                               # %.preheader204
	leaq	16(%rsp), %rsi
	movl	$512, %edx              # imm = 0x200
	movl	%ebx, %edi
	callq	read
	testl	%eax, %eax
	jle	.LBB1_152
# BB#147:
	leaq	16(%rsp), %r13
.LBB1_148:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r13, %rdi
	movl	%eax, %esi
	callq	cli_utf16toascii
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB1_151
# BB#149:                               #   in Loop: Header=BB1_148 Depth=1
	movq	%rbp, %rdi
	callq	strlen
	movl	%r12d, %edi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	write
	cmpq	$-1, %rax
	je	.LBB1_202
# BB#150:                               #   in Loop: Header=BB1_148 Depth=1
	movq	%rbp, %rdi
	callq	free
.LBB1_151:                              # %.backedge
                                        #   in Loop: Header=BB1_148 Depth=1
	movl	$512, %edx              # imm = 0x200
	movl	%ebx, %edi
	movq	%r13, %rsi
	callq	read
	testl	%eax, %eax
	jg	.LBB1_148
.LBB1_152:                              # %._crit_edge
	movl	%r12d, %edi
	callq	fsync
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r12d, %edi
	callq	lseek
	movl	%r12d, %edi
	movq	%r14, %rsi
	callq	cli_scanhtml
	movl	%eax, %r13d
	movl	%r12d, %edi
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB1_199
# BB#153:
	movl	$.L.str.97, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	jmp	.LBB1_200
.LBB1_154:
	movq	56(%r14), %rax
	testb	$2, 12(%rax)
	je	.LBB1_21
# BB#155:
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	cli_scanrtf
	movl	%eax, %r13d
	jmp	.LBB1_21
.LBB1_156:
	testb	$16, 40(%r14)
	je	.LBB1_21
# BB#157:
	movq	56(%r14), %rax
	testb	$1, 12(%rax)
	je	.LBB1_21
# BB#158:
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	cli_scanhtml
	movl	%eax, %r13d
	jmp	.LBB1_21
.LBB1_159:
	testb	$1, 40(%r14)
	je	.LBB1_21
# BB#160:
	xorl	%edx, %edx
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	cli_scannulsft
	movl	%eax, %r13d
	jmp	.LBB1_21
.LBB1_161:
	testb	$1, 40(%r14)
	je	.LBB1_21
# BB#162:
	movq	56(%r14), %rax
	testb	$32, 9(%rax)
	je	.LBB1_21
# BB#163:
	movl	$23, %edx
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	cli_scanautoit
	movl	%eax, %r13d
	jmp	.LBB1_21
.LBB1_164:
	movl	%ebx, %edi
	movq	%rbp, %rsi
	callq	html_screnc_decode
.LBB1_165:
	testl	%eax, %eax
	jne	.LBB1_177
	jmp	.LBB1_178
.LBB1_166:
	movl	$.L.str.126, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
	jmp	.LBB1_227
.LBB1_167:
	movq	%rbp, %rdi
	movl	%ebx, %esi
	callq	cli_uuencode
	jmp	.LBB1_176
.LBB1_168:
	movl	$.L.str.127, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
	movl	$522, %r15d             # imm = 0x20A
	jmp	.LBB1_21
.LBB1_169:
	movl	$.L.str.58, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
.LBB1_170:                              # %cli_scantnef.exit.thread.thread220
	movl	$-105, %r13d
	movl	$507, %r15d             # imm = 0x1FB
	jmp	.LBB1_21
.LBB1_171:
	movq	%r12, %rdi
	movl	%ebx, %esi
	callq	cli_binhex
	movl	%eax, %r13d
	testl	%r13d, %r13d
	je	.LBB1_203
# BB#172:
	movl	%r13d, %edi
	callq	cl_strerror
	movq	%rax, %rcx
	movl	$.L.str.121, %edi
	jmp	.LBB1_187
.LBB1_173:
	movq	%rbp, %rdi
	movl	%ebx, %esi
	callq	cli_tnef
	jmp	.LBB1_176
.LBB1_174:
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r14, %rdx
	callq	cli_pdf
	jmp	.LBB1_176
.LBB1_175:
	movq	%rbp, %rdi
	movl	%ebx, %esi
	callq	cli_pst
.LBB1_176:
	movl	%eax, %r13d
	testl	%r13d, %r13d
	jne	.LBB1_178
.LBB1_177:
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	cli_scandir
	movl	%eax, %r13d
.LBB1_178:
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB1_180
# BB#179:
	movq	%rbp, %rdi
	callq	cli_rmdirs
.LBB1_180:
	movq	%rbp, %rdi
	callq	free
	jmp	.LBB1_21
.LBB1_181:
	movl	$.L.str.95, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_errmsg
	movq	%r15, %rdi
	callq	free
	jmp	.LBB1_182
.LBB1_183:
	movl	$.L.str.128, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_184
.LBB1_185:
	movq	32(%r14), %rdx
	movl	%ebx, %edi
	movq	%r12, %rsi
	callq	cli_ole2_extract
	movl	%eax, %r13d
	testl	%r13d, %r13d
	je	.LBB1_204
# BB#186:
	movl	%r13d, %edi
	callq	cl_strerror
	movq	%rax, %rcx
	movl	$.L.str.107, %edi
.LBB1_187:
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_dbgmsg
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB1_208
	jmp	.LBB1_207
.LBB1_188:
	movl	$8192, %edi             # imm = 0x2000
	callq	cli_malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB1_209
# BB#189:                               # %.preheader
	xorl	%r15d, %r15d
.LBB1_190:                              # =>This Inner Loop Header: Depth=1
	movq	%r15, %r13
	movl	$8192, %edx             # imm = 0x2000
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r12, %rsi
	callq	gzread
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jle	.LBB1_212
# BB#191:                               #   in Loop: Header=BB1_190 Depth=1
	movslq	%ebp, %r15
	addq	%r13, %r15
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.LBB1_194
# BB#192:                               #   in Loop: Header=BB1_190 Depth=1
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB1_194
# BB#193:                               #   in Loop: Header=BB1_190 Depth=1
	leaq	8192(%r15), %rax
	cmpq	%rdx, %rax
	ja	.LBB1_215
.LBB1_194:                              #   in Loop: Header=BB1_190 Depth=1
	movl	4(%rsp), %edi
	movq	%r12, %rsi
	movl	%ebp, %edx
	callq	cli_writen
	cmpl	%ebp, %eax
	je	.LBB1_190
# BB#195:
	movl	$.L.str.63, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	4(%rsp), %edi
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB1_197
# BB#196:
	movq	16(%rsp), %rdi
	callq	unlink
.LBB1_197:
	movq	16(%rsp), %rdi
	callq	free
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	gzclose
	movq	%r12, %rdi
	callq	free
	jmp	.LBB1_170
.LBB1_198:
	movl	$.L.str.128, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r12, %rdi
	callq	free
.LBB1_184:                              # %cli_scantnef.exit.thread.thread220
	movl	$-114, %r13d
	movl	$522, %r15d             # imm = 0x20A
	jmp	.LBB1_21
.LBB1_199:
	movq	%r15, %rdi
	callq	unlink
.LBB1_200:
	movq	%r15, %rdi
	callq	free
	movl	$526, %r15d             # imm = 0x20E
	jmp	.LBB1_21
.LBB1_201:
	movl	$.L.str.129, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
	movq	%r12, %rdi
	callq	free
	movq	%r13, %rdi
	jmp	.LBB1_226
.LBB1_202:
	movl	$.L.str.96, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_errmsg
	movq	%rbp, %rdi
	callq	free
	movq	%r15, %rdi
	callq	unlink
	movq	%r15, %rdi
	callq	free
	movl	%r12d, %edi
	callq	close
.LBB1_182:                              # %cli_scantnef.exit.thread.thread220
	movl	$-123, %r13d
	movl	$526, %r15d             # imm = 0x20E
	jmp	.LBB1_21
.LBB1_203:
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	cli_scandir
	movl	%eax, %r13d
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB1_208
	jmp	.LBB1_207
.LBB1_204:
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	cli_vba_scandir
	movl	%eax, %ebp
	movl	$1, %r13d
	cmpl	$1, %ebp
	je	.LBB1_206
# BB#205:
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	cli_scandir
	cmpl	$1, %eax
	cmovel	%eax, %ebp
	movl	%ebp, %r13d
.LBB1_206:
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB1_208
.LBB1_207:
	movq	%r12, %rdi
	callq	cli_rmdirs
.LBB1_208:
	movq	%r12, %rdi
	callq	free
	jmp	.LBB1_21
.LBB1_209:
	movl	$.L.str.60, %edi
	movl	$8192, %esi             # imm = 0x2000
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	gzclose
	movl	4(%rsp), %edi
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB1_211
# BB#210:
	movq	16(%rsp), %rdi
	callq	unlink
.LBB1_211:
	movq	16(%rsp), %rdi
	callq	free
	movl	$-114, %r13d
	movl	$507, %r15d             # imm = 0x1FB
	jmp	.LBB1_21
.LBB1_212:
	xorl	%ebp, %ebp
	jmp	.LBB1_217
.LBB1_213:
	movl	$.L.str.130, %edi
	xorl	%eax, %eax
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rsi
	callq	cli_errmsg
	movq	%r12, %rdi
	callq	free
	movq	%rbp, %rdi
	jmp	.LBB1_226
.LBB1_214:
	movl	$.L.str.131, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	movq	%r12, %rdi
	callq	free
	jmp	.LBB1_225
.LBB1_215:
	xorl	%ebp, %ebp
	movl	$.L.str.61, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	testb	$1, 41(%r14)
	je	.LBB1_217
# BB#216:
	movq	(%r14), %rax
	movq	$.L.str.62, (%rax)
	movb	$1, %bpl
.LBB1_217:                              # %.loopexit
	movq	%r12, %rdi
	callq	free
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	gzclose
	movl	4(%rsp), %edi
	testb	%bpl, %bpl
	jne	.LBB1_221
# BB#218:
	callq	fsync
	movl	4(%rsp), %ecx
	cmpl	$-1, %eax
	je	.LBB1_228
# BB#219:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ecx, %edi
	callq	lseek
	movl	4(%rsp), %edi
	movq	%r14, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %r13d
	cmpl	$1, %r13d
	jne	.LBB1_234
# BB#220:
	movq	(%r14), %rax
	movq	(%rax), %rsi
	movl	$.L.str.65, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	4(%rsp), %edi
.LBB1_221:
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB1_223
# BB#222:
	movq	16(%rsp), %rdi
	callq	unlink
.LBB1_223:
	movq	16(%rsp), %rdi
	callq	free
	movl	$1, %r13d
	movl	$507, %r15d             # imm = 0x1FB
	jmp	.LBB1_21
.LBB1_224:
	movl	$.L.str.132, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_errmsg
.LBB1_225:                              # %cli_scancryptff.exit
	movl	%ebp, %edi
	callq	close
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB1_226:                              # %cli_scancryptff.exit
	callq	free
.LBB1_227:                              # %cli_scantnef.exit.thread.thread220
	movl	$-123, %r13d
	movl	$522, %r15d             # imm = 0x20A
	jmp	.LBB1_21
.LBB1_228:
	movl	$.L.str.64, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	cli_dbgmsg
	movl	4(%rsp), %edi
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB1_230
# BB#229:
	movq	16(%rsp), %rdi
	callq	unlink
.LBB1_230:
	movq	16(%rsp), %rdi
	callq	free
	movl	$-113, %r13d
	movl	$507, %r15d             # imm = 0x1FB
	jmp	.LBB1_21
.LBB1_231:                              # %vector.body.preheader
	leaq	16(%r13), %rsi
	leaq	16(%r12), %rdi
	pcmpeqd	%xmm0, %xmm0
	movq	%rcx, %rdx
.LBB1_232:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rsi), %xmm1
	movdqu	(%rsi), %xmm2
	pxor	%xmm0, %xmm1
	pxor	%xmm0, %xmm2
	movdqu	%xmm1, -16(%rdi)
	movdqu	%xmm2, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-32, %rdx
	jne	.LBB1_232
# BB#233:                               # %middle.block
	testq	%r8, %r8
	jne	.LBB1_117
	jmp	.LBB1_123
.LBB1_234:
	movl	4(%rsp), %edi
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB1_236
# BB#235:
	movq	16(%rsp), %rdi
	callq	unlink
.LBB1_236:
	movq	16(%rsp), %rdi
	callq	free
	movl	$507, %r15d             # imm = 0x1FB
	jmp	.LBB1_21
.LBB1_237:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	unlink
.LBB1_238:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	free
	movl	$522, %r15d             # imm = 0x20A
	jmp	.LBB1_21
.Lfunc_end1:
	.size	cli_magic_scandesc, .Lfunc_end1-cli_magic_scandesc
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_20
	.quad	.LBB1_21
	.quad	.LBB1_45
	.quad	.LBB1_21
	.quad	.LBB1_48
	.quad	.LBB1_51
	.quad	.LBB1_55
	.quad	.LBB1_60
	.quad	.LBB1_21
	.quad	.LBB1_67
	.quad	.LBB1_68
	.quad	.LBB1_21
	.quad	.LBB1_71
	.quad	.LBB1_75
	.quad	.LBB1_78
	.quad	.LBB1_82
	.quad	.LBB1_85
	.quad	.LBB1_88
	.quad	.LBB1_92
	.quad	.LBB1_96
	.quad	.LBB1_101
	.quad	.LBB1_105
	.quad	.LBB1_130
	.quad	.LBB1_134
	.quad	.LBB1_137
	.quad	.LBB1_143
	.quad	.LBB1_154
	.quad	.LBB1_156
	.quad	.LBB1_21
	.quad	.LBB1_21
	.quad	.LBB1_21
	.quad	.LBB1_21
	.quad	.LBB1_21
	.quad	.LBB1_21
	.quad	.LBB1_159
	.quad	.LBB1_161

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.zero	16
	.text
	.p2align	4, 0x90
	.type	cli_scanraw,@function
cli_scanraw:                            # @cli_scanraw
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$600, %rsp              # imm = 0x258
.Lcfi32:
	.cfi_def_cfa_offset 656
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, %rbx
	movl	%edi, %r15d
	movq	$0, 16(%rsp)
	testb	%cl, %cl
	je	.LBB2_3
# BB#1:
	leal	-500(%r12), %eax
	cmpl	$8, %eax
	ja	.LBB2_3
# BB#2:
	movb	$1, %bpl
	movl	$261, %ecx              # imm = 0x105
	btl	%eax, %ecx
	jb	.LBB2_4
.LBB2_3:
	xorl	%ebp, %ebp
.LBB2_4:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r15d, %edi
	callq	lseek
	testq	%rax, %rax
	js	.LBB2_9
# BB#5:
	movzbl	%bpl, %edx
	leaq	16(%rsp), %r9
	xorl	%r8d, %r8d
	movl	%r15d, %edi
	movq	%rbx, %rsi
	movl	%r12d, %ecx
	callq	cli_scandesc
	movl	%eax, %r14d
	cmpl	$500, %r14d             # imm = 0x1F4
	jl	.LBB2_10
# BB#6:
	cmpl	$500, %r12d             # imm = 0x1F4
	jne	.LBB2_11
# BB#7:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r15d, %edi
	callq	lseek
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%r9d, %r9d
	movl	%r15d, %edi
	movq	%rbx, %rsi
	movl	%r14d, %ecx
	callq	cli_scandesc
	movl	%eax, %r13d
	cmpl	$1, %r13d
	jne	.LBB2_46
# BB#8:
	movq	(%rbx), %rax
	movq	(%rax), %rsi
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movl	%r15d, %edx
	movl	%r14d, %ecx
	callq	cli_dbgmsg
.LBB2_48:
	movl	$1, %r13d
	cmpl	$529, %r14d             # imm = 0x211
	je	.LBB2_47
	jmp	.LBB2_49
.LBB2_9:
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-123, %ebp
	jmp	.LBB2_74
.LBB2_10:
	movl	%r14d, %ebp
	jmp	.LBB2_71
.LBB2_11:
	xorl	%r13d, %r13d
	cmpl	$508, %r12d             # imm = 0x1FC
	je	.LBB2_13
# BB#12:
	cmpl	$502, %r12d             # imm = 0x1F6
	jne	.LBB2_46
.LBB2_13:
	movl	$-559038737, 28(%rsp)   # imm = 0xDEADBEEF
	movq	16(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB2_46
# BB#14:                                # %.lr.ph134
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_15:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %esi
	leal	-502(%rsi), %eax
	cmpl	$34, %eax
	ja	.LBB2_21
# BB#16:                                #   in Loop: Header=BB2_15 Depth=1
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_17:                               #   in Loop: Header=BB2_15 Depth=1
	testb	$32, 40(%rbx)
	je	.LBB2_45
# BB#18:                                #   in Loop: Header=BB2_15 Depth=1
	movq	56(%rbx), %rax
	cmpl	$0, (%rax)
	je	.LBB2_45
# BB#19:                                #   in Loop: Header=BB2_15 Depth=1
	movq	8(%rbp), %rsi
	testq	%rsi, %rsi
	je	.LBB2_45
# BB#20:                                #   in Loop: Header=BB2_15 Depth=1
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movq	$0, 64(%rsp)
	movq	8(%rbp), %rsi
	movq	%rsi, 56(%rsp)
	xorl	%edx, %edx
	movl	%r15d, %edi
	callq	lseek
	movl	%r15d, %edi
	leaq	48(%rsp), %rsi
	callq	cli_peheader
	testl	%eax, %eax
	jne	.LBB2_45
	jmp	.LBB2_75
.LBB2_21:                               #   in Loop: Header=BB2_15 Depth=1
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_warnmsg
	jmp	.LBB2_45
.LBB2_22:                               #   in Loop: Header=BB2_15 Depth=1
	cmpl	$502, %r12d             # imm = 0x1F6
	jne	.LBB2_45
# BB#23:                                #   in Loop: Header=BB2_15 Depth=1
	movl	40(%rbx), %eax
	andl	$1, %eax
	je	.LBB2_45
# BB#24:                                #   in Loop: Header=BB2_15 Depth=1
	movq	56(%rbx), %rax
	testb	$2, 8(%rax)
	je	.LBB2_45
# BB#25:                                #   in Loop: Header=BB2_15 Depth=1
	movq	8(%rbp), %rsi
	testq	%rsi, %rsi
	je	.LBB2_45
# BB#26:                                #   in Loop: Header=BB2_15 Depth=1
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movq	8(%rbp), %rdx
	movl	%r15d, %edi
	movq	%rbx, %rsi
	leaq	28(%rsp), %rcx
	callq	cli_scanzip
	jmp	.LBB2_44
.LBB2_27:                               #   in Loop: Header=BB2_15 Depth=1
	cmpl	$502, %r12d             # imm = 0x1F6
	jne	.LBB2_45
# BB#28:                                #   in Loop: Header=BB2_15 Depth=1
	movl	40(%rbx), %eax
	andl	$1, %eax
	je	.LBB2_45
# BB#29:                                #   in Loop: Header=BB2_15 Depth=1
	movq	56(%rbx), %rax
	testb	$32, 8(%rax)
	je	.LBB2_45
# BB#30:                                #   in Loop: Header=BB2_15 Depth=1
	movl	8(%rbp), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%rbp), %rdx
	movl	%r15d, %edi
	movq	%rbx, %rsi
	callq	cli_scanmscab
	jmp	.LBB2_44
.LBB2_31:                               #   in Loop: Header=BB2_15 Depth=1
	cmpl	$502, %r12d             # imm = 0x1F6
	jne	.LBB2_45
# BB#32:                                #   in Loop: Header=BB2_15 Depth=1
	movl	40(%rbx), %eax
	andl	$1, %eax
	je	.LBB2_45
# BB#33:                                #   in Loop: Header=BB2_15 Depth=1
	movq	56(%rbx), %rax
	testb	$16, 9(%rax)
	je	.LBB2_45
# BB#34:                                #   in Loop: Header=BB2_15 Depth=1
	movl	8(%rbp), %esi
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%rbp), %rdx
	movl	%r15d, %edi
	movq	%rbx, %rsi
	callq	cli_scanarj
	jmp	.LBB2_44
.LBB2_35:                               #   in Loop: Header=BB2_15 Depth=1
	cmpl	$502, %r12d             # imm = 0x1F6
	jne	.LBB2_45
# BB#36:                                #   in Loop: Header=BB2_15 Depth=1
	movl	40(%rbx), %eax
	andl	$1, %eax
	je	.LBB2_45
# BB#37:                                #   in Loop: Header=BB2_15 Depth=1
	movq	56(%rbx), %rax
	testb	$8, 9(%rax)
	je	.LBB2_45
# BB#38:                                #   in Loop: Header=BB2_15 Depth=1
	movq	8(%rbp), %rsi
	cmpq	$5, %rsi
	jl	.LBB2_45
# BB#39:                                #   in Loop: Header=BB2_15 Depth=1
	addl	$-4, %esi
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movq	8(%rbp), %rdx
	addq	$-4, %rdx
	movl	%r15d, %edi
	movq	%rbx, %rsi
	callq	cli_scannulsft
	jmp	.LBB2_44
.LBB2_40:                               #   in Loop: Header=BB2_15 Depth=1
	cmpl	$502, %r12d             # imm = 0x1F6
	jne	.LBB2_45
# BB#41:                                #   in Loop: Header=BB2_15 Depth=1
	movl	40(%rbx), %eax
	andl	$1, %eax
	je	.LBB2_45
# BB#42:                                #   in Loop: Header=BB2_15 Depth=1
	movq	56(%rbx), %rax
	testb	$32, 9(%rax)
	je	.LBB2_45
# BB#43:                                #   in Loop: Header=BB2_15 Depth=1
	movl	8(%rbp), %esi
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%rbp), %rdx
	addq	$23, %rdx
	movl	%r15d, %edi
	movq	%rbx, %rsi
	callq	cli_scanautoit
.LBB2_44:                               #   in Loop: Header=BB2_15 Depth=1
	movl	%eax, %r13d
	cmpl	$1, %r13d
	je	.LBB2_48
	.p2align	4, 0x90
.LBB2_45:                               # %.thread140
                                        #   in Loop: Header=BB2_15 Depth=1
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB2_15
.LBB2_46:                               # %.thread
	cmpl	$529, %r14d             # imm = 0x211
	jne	.LBB2_49
.LBB2_47:
	incl	48(%rbx)
	jmp	.LBB2_50
.LBB2_75:
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_77
# BB#76:
	callq	free
.LBB2_77:
	movq	8(%rbp), %rsi
	xorl	%edx, %edx
	movl	%r15d, %edi
	callq	lseek
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB2_87
# BB#78:
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	movl	%eax, %r13d
	testl	%r13d, %r13d
	js	.LBB2_88
# BB#79:                                # %.preheader126
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	leaq	80(%rsp), %rcx
	movl	%r13d, 12(%rsp)         # 4-byte Spill
.LBB2_80:                               # =>This Inner Loop Header: Depth=1
	movq	%rdx, %r13
	movl	$512, %edx              # imm = 0x200
	movl	%r15d, %edi
	movq	%rcx, %rsi
	callq	read
	movq	%rax, %rbp
	testl	%ebp, %ebp
	jle	.LBB2_91
# BB#81:                                #   in Loop: Header=BB2_80 Depth=1
	movslq	%ebp, %rsi
	addq	%r13, %rsi
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB2_84
# BB#82:                                #   in Loop: Header=BB2_80 Depth=1
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB2_84
# BB#83:                                #   in Loop: Header=BB2_80 Depth=1
	leaq	512(%rsi), %rax
	cmpq	%rdx, %rax
	ja	.LBB2_90
.LBB2_84:                               #   in Loop: Header=BB2_80 Depth=1
	movl	12(%rsp), %edi          # 4-byte Reload
	leaq	80(%rsp), %r13
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%r13, %rsi
	movl	%ebp, %edx
	callq	cli_writen
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%r13, %rcx
	movl	12(%rsp), %r13d         # 4-byte Reload
	cmpl	%ebp, %eax
	je	.LBB2_80
# BB#85:
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r13d, %edi
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	movq	32(%rsp), %rbp          # 8-byte Reload
	jne	.LBB2_89
# BB#86:
	movq	%rbp, %rdi
	callq	unlink
	jmp	.LBB2_89
.LBB2_87:
	movl	$-114, %r13d
	cmpl	$529, %r14d             # imm = 0x211
	jne	.LBB2_49
	jmp	.LBB2_47
.LBB2_88:
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_errmsg
.LBB2_89:
	movq	%rbp, %rdi
	callq	free
	movl	$-123, %r13d
	cmpl	$529, %r14d             # imm = 0x211
	je	.LBB2_47
.LBB2_49:
	incl	44(%rbx)
.LBB2_50:
	movl	$1, %ebp
	cmpl	$1, %r13d
	jne	.LBB2_51
.LBB2_67:
	cmpl	$529, %r14d             # imm = 0x211
	jne	.LBB2_69
	jmp	.LBB2_68
.LBB2_51:
	cmpl	$529, %r14d             # imm = 0x211
	je	.LBB2_57
# BB#52:
	cmpl	$528, %r14d             # imm = 0x210
	jne	.LBB2_62
# BB#53:
	cmpl	$500, %r12d             # imm = 0x1F4
	jne	.LBB2_62
# BB#54:
	movl	40(%rbx), %eax
	andl	$16, %eax
	je	.LBB2_62
# BB#55:
	movq	56(%rbx), %rax
	testb	$1, 12(%rax)
	movl	%r13d, %ebp
	je	.LBB2_67
# BB#56:
	movl	%r15d, %edi
	movq	%rbx, %rsi
	callq	cli_scanhtml
	jmp	.LBB2_61
.LBB2_57:
	cmpl	$500, %r12d             # imm = 0x1F4
	jne	.LBB2_62
# BB#58:
	movl	40(%rbx), %eax
	andl	$2, %eax
	je	.LBB2_66
# BB#59:
	movq	56(%rbx), %rax
	testb	$1, 16(%rax)
	movl	%r13d, %ebp
	je	.LBB2_67
# BB#60:
	movl	%r15d, %edi
	movq	%rbx, %rsi
	callq	cli_scanmail
.LBB2_61:
	movl	%eax, %ebp
	cmpl	$529, %r14d             # imm = 0x211
	je	.LBB2_68
	jmp	.LBB2_69
.LBB2_62:
	movl	%r13d, %ebp
	cmpl	$529, %r14d             # imm = 0x211
	jne	.LBB2_69
.LBB2_68:
	decl	48(%rbx)
	jmp	.LBB2_71
.LBB2_66:
	movl	%r13d, %ebp
	cmpl	$529, %r14d             # imm = 0x211
	je	.LBB2_68
.LBB2_69:
	decl	44(%rbx)
	jmp	.LBB2_71
	.p2align	4, 0x90
.LBB2_70:                               #   in Loop: Header=BB2_71 Depth=1
	movq	24(%rdi), %rax
	movq	%rax, 16(%rsp)
	callq	free
.LBB2_71:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB2_70
# BB#72:                                # %._crit_edge
	cmpl	$1, %ebp
	jne	.LBB2_74
# BB#73:
	movq	(%rbx), %rax
	movq	(%rax), %rsi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	cli_dbgmsg
	movl	$1, %ebp
.LBB2_74:
	movl	%ebp, %eax
	addq	$600, %rsp              # imm = 0x258
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_90:
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB2_91:                               # %.loopexit
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %edi
	callq	fsync
	cmpl	$-1, %eax
	movq	32(%rsp), %r13          # 8-byte Reload
	je	.LBB2_96
# BB#92:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	cli_magic_scandesc
	cmpl	$1, %eax
	jne	.LBB2_99
# BB#93:
	movq	(%rbx), %rax
	movq	(%rax), %rsi
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebp, %edi
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB2_95
# BB#94:
	movq	%r13, %rdi
	callq	unlink
.LBB2_95:
	movq	%r13, %rdi
	callq	free
	jmp	.LBB2_48
.LBB2_96:
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	movl	%ebp, %edi
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB2_98
# BB#97:
	movq	%r13, %rdi
	callq	unlink
.LBB2_98:
	movq	%r13, %rdi
	callq	free
	movl	$-113, %r13d
	cmpl	$529, %r14d             # imm = 0x211
	jne	.LBB2_49
	jmp	.LBB2_47
.LBB2_99:
	movl	%ebp, %edi
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB2_101
# BB#100:
	movq	%r13, %rdi
	callq	unlink
.LBB2_101:
	movq	%r13, %rdi
	callq	free
	xorl	%r13d, %r13d
	cmpl	$529, %r14d             # imm = 0x211
	jne	.LBB2_49
	jmp	.LBB2_47
.Lfunc_end2:
	.size	cli_scanraw, .Lfunc_end2-cli_scanraw
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_17
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_21
	.quad	.LBB2_22
	.quad	.LBB2_45
	.quad	.LBB2_27
	.quad	.LBB2_31
	.quad	.LBB2_35
	.quad	.LBB2_40

	.text
	.p2align	4, 0x90
	.type	cli_scanzip,@function
cli_scanzip:                            # @cli_scanzip
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi45:
	.cfi_def_cfa_offset 320
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movl	%edi, %ebp
	movq	$0, 24(%rsp)
	movl	$-1, 4(%rsp)
	xorl	%r13d, %r13d
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leaq	108(%rsp), %rdx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	zip_dir_open
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB3_81
# BB#1:
	leaq	120(%rsp), %rdx
	movl	$1, %edi
	movl	%ebp, %esi
	callq	__fxstat
	movl	$8192, %edi             # imm = 0x2000
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB3_82
# BB#2:                                 # %.preheader152
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	56(%rsp), %rsi
	movq	%r15, %rdi
	callq	zip_dir_read
	testl	%eax, %eax
	je	.LBB3_83
# BB#3:                                 # %.lr.ph.lr.ph.lr.ph
	movl	%ebp, 52(%rsp)          # 4-byte Spill
	movb	$1, %al
	movl	%eax, 48(%rsp)          # 4-byte Spill
	xorl	%r13d, %r13d
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movq	%r15, 96(%rsp)          # 8-byte Spill
	movq	%r12, 88(%rsp)          # 8-byte Spill
	movq	%r14, 40(%rsp)          # 8-byte Spill
.LBB3_4:                                # %.lr.ph.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_18 Depth 2
                                        #       Child Loop BB3_26 Depth 3
                                        #       Child Loop BB3_66 Depth 3
                                        #         Child Loop BB3_67 Depth 4
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	movl	52(%rsp), %r13d         # 4-byte Reload
	testq	%r14, %r14
	jne	.LBB3_15
	jmp	.LBB3_18
.LBB3_12:                               #   in Loop: Header=BB3_18 Depth=2
	movq	(%r12), %rax
	movq	(%rax), %rsi
	movl	$.L.str.53, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %r13d
	movb	$1, %bl
	jmp	.LBB3_8
.LBB3_13:                               # %.lr.ph
                                        #   in Loop: Header=BB3_18 Depth=2
	movl	52(%rsp), %r13d         # 4-byte Reload
	testq	%r14, %r14
	jne	.LBB3_15
	jmp	.LBB3_18
.LBB3_5:                                #   in Loop: Header=BB3_18 Depth=2
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-113, %r13d
	movb	$1, %bl
	jmp	.LBB3_8
.LBB3_6:                                #   in Loop: Header=BB3_18 Depth=2
	movl	$-104, %r13d
.LBB3_7:                                #   in Loop: Header=BB3_18 Depth=2
	movq	32(%rsp), %rbx          # 8-byte Reload
.LBB3_8:                                # %.loopexit149
                                        #   in Loop: Header=BB3_18 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	zip_file_close
	testb	%bl, %bl
	movq	40(%rsp), %r14          # 8-byte Reload
	jne	.LBB3_10
# BB#9:                                 # %.loopexit149
                                        #   in Loop: Header=BB3_18 Depth=2
	testl	%r13d, %r13d
	je	.LBB3_103
.LBB3_10:                               #   in Loop: Header=BB3_18 Depth=2
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	testl	%r13d, %r13d
	jne	.LBB3_91
.LBB3_11:                               # %.outer154.backedge
                                        #   in Loop: Header=BB3_18 Depth=2
	movq	%r15, %rdi
	leaq	56(%rsp), %rsi
	callq	zip_dir_read
	testl	%eax, %eax
	movl	8(%rsp), %r13d          # 4-byte Reload
	jne	.LBB3_13
	jmp	.LBB3_91
	.p2align	4, 0x90
.LBB3_14:                               #   in Loop: Header=BB3_18 Depth=2
	testq	%r14, %r14
	je	.LBB3_18
.LBB3_15:                               #   in Loop: Header=BB3_4 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_18
# BB#16:                                #   in Loop: Header=BB3_4 Depth=1
	movl	80(%rsp), %eax
	cmpl	%eax, (%r14)
	je	.LBB3_86
# BB#17:                                #   in Loop: Header=BB3_4 Depth=1
	movl	%eax, (%r14)
.LBB3_18:                               #   Parent Loop BB3_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_26 Depth 3
                                        #       Child Loop BB3_66 Depth 3
                                        #         Child Loop BB3_67 Depth 4
	movq	72(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB3_84
# BB#19:                                #   in Loop: Header=BB3_18 Depth=2
	xorl	%ebx, %ebx
	movzwl	68(%rsp), %r14d
	movw	$8257, %ax              # imm = 0x2041
	andw	%ax, %r14w
	setne	%dil
	movl	80(%rsp), %r8d
	movl	84(%rsp), %ecx
	movl	60(%rsp), %r9d
	movl	64(%rsp), %r10d
	movzwl	56(%rsp), %r11d
	xorl	%r15d, %r15d
	testl	%r9d, %r9d
	movl	$0, %ebp
	je	.LBB3_21
# BB#20:                                #   in Loop: Header=BB3_18 Depth=2
	xorl	%edx, %edx
	movl	%r10d, %eax
	divl	%r9d
	movl	%eax, %ebp
.LBB3_21:                               #   in Loop: Header=BB3_18 Depth=2
	movb	%dil, %bl
	movq	32(%r12), %rax
	testq	%rax, %rax
	je	.LBB3_23
# BB#22:                                #   in Loop: Header=BB3_18 Depth=2
	movl	12(%rax), %r15d
.LBB3_23:                               #   in Loop: Header=BB3_18 Depth=2
	incl	12(%rsp)                # 4-byte Folded Spill
	movl	$.L.str.35, %edi
	movl	$0, %eax
	movl	%r8d, %edx
	movl	%ebx, %r8d
	pushq	%r15
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	callq	cli_dbgmsg
	addq	$32, %rsp
.Lcfi56:
	.cfi_adjust_cfa_offset -32
	movl	64(%rsp), %ebp
	testq	%rbp, %rbp
	je	.LBB3_43
# BB#24:                                #   in Loop: Header=BB3_18 Depth=2
	movq	24(%r12), %rax
	movq	40(%rax), %r12
	testq	%r12, %r12
	je	.LBB3_44
# BB#25:                                # %.preheader150
                                        #   in Loop: Header=BB3_18 Depth=2
	movl	80(%rsp), %r15d
	movl	60(%rsp), %r13d
	movzwl	56(%rsp), %edx
	movq	72(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_26:                               #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ebx, 20(%r12)
	jne	.LBB3_41
# BB#27:                                #   in Loop: Header=BB3_26 Depth=3
	movl	12(%r12), %eax
	testl	%eax, %eax
	je	.LBB3_29
# BB#28:                                #   in Loop: Header=BB3_26 Depth=3
	cmpl	%r15d, %eax
	jne	.LBB3_41
.LBB3_29:                               #   in Loop: Header=BB3_26 Depth=3
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.LBB3_31
# BB#30:                                #   in Loop: Header=BB3_26 Depth=3
	cmpl	%r13d, %eax
	jne	.LBB3_41
.LBB3_31:                               #   in Loop: Header=BB3_26 Depth=3
	movl	4(%r12), %eax
	testl	%eax, %eax
	js	.LBB3_33
# BB#32:                                #   in Loop: Header=BB3_26 Depth=3
	cmpl	%ebp, %eax
	jne	.LBB3_41
.LBB3_33:                               #   in Loop: Header=BB3_26 Depth=3
	movl	8(%r12), %eax
	testl	%eax, %eax
	js	.LBB3_35
# BB#34:                                #   in Loop: Header=BB3_26 Depth=3
	cmpw	%ax, %dx
	jne	.LBB3_41
.LBB3_35:                               #   in Loop: Header=BB3_26 Depth=3
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.LBB3_37
# BB#36:                                #   in Loop: Header=BB3_26 Depth=3
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB3_41
.LBB3_37:                               #   in Loop: Header=BB3_26 Depth=3
	movl	24(%r12), %eax
	testl	%eax, %eax
	je	.LBB3_39
# BB#38:                                #   in Loop: Header=BB3_26 Depth=3
	movq	88(%rsp), %rcx          # 8-byte Reload
	cmpl	%eax, 44(%rcx)
	ja	.LBB3_41
.LBB3_39:                               #   in Loop: Header=BB3_26 Depth=3
	movq	32(%r12), %rsi
	testq	%rsi, %rsi
	je	.LBB3_97
# BB#40:                                #   in Loop: Header=BB3_26 Depth=3
	movq	16(%rsp), %rdi          # 8-byte Reload
	movw	%dx, 32(%rsp)           # 2-byte Spill
	callq	strcmp
	movzwl	32(%rsp), %edx          # 2-byte Folded Reload
	testl	%eax, %eax
	je	.LBB3_97
	.p2align	4, 0x90
.LBB3_41:                               #   in Loop: Header=BB3_26 Depth=3
	movq	48(%r12), %r12
	testq	%r12, %r12
	jne	.LBB3_26
# BB#42:                                #   in Loop: Header=BB3_18 Depth=2
	movq	96(%rsp), %r15          # 8-byte Reload
	movl	52(%rsp), %r13d         # 4-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB3_45
	.p2align	4, 0x90
.LBB3_43:                               #   in Loop: Header=BB3_18 Depth=2
	cmpl	$0, 80(%rsp)
	movq	96(%rsp), %r15          # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	je	.LBB3_57
	jmp	.LBB3_87
	.p2align	4, 0x90
.LBB3_44:                               # %..loopexit151_crit_edge
                                        #   in Loop: Header=BB3_18 Depth=2
	movq	72(%rsp), %rbx
	movq	96(%rsp), %r15          # 8-byte Reload
.LBB3_45:                               # %.loopexit151
                                        #   in Loop: Header=BB3_18 Depth=2
	movq	%rbx, %rdi
	callq	strlen
	cmpb	$47, -1(%rbx,%rax)
	movq	88(%rsp), %r12          # 8-byte Reload
	jne	.LBB3_47
# BB#46:                                #   in Loop: Header=BB3_18 Depth=2
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	40(%rsp), %r14          # 8-byte Reload
	jmp	.LBB3_57
	.p2align	4, 0x90
.LBB3_47:                               #   in Loop: Header=BB3_18 Depth=2
	movl	60(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB3_88
# BB#48:                                #   in Loop: Header=BB3_18 Depth=2
	movq	32(%r12), %rsi
	testq	%rsi, %rsi
	je	.LBB3_51
# BB#49:                                #   in Loop: Header=BB3_18 Depth=2
	movl	12(%rsi), %edi
	testl	%edi, %edi
	je	.LBB3_51
# BB#50:                                #   in Loop: Header=BB3_18 Depth=2
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	%ecx
	cmpl	%edi, %eax
	jae	.LBB3_101
.LBB3_51:                               #   in Loop: Header=BB3_18 Depth=2
	testw	%r14w, %r14w
	je	.LBB3_53
# BB#52:                                #   in Loop: Header=BB3_18 Depth=2
	testb	$8, 40(%r12)
	movq	40(%rsp), %r14          # 8-byte Reload
	je	.LBB3_57
	jmp	.LBB3_98
.LBB3_53:                               #   in Loop: Header=BB3_18 Depth=2
	testq	%rsi, %rsi
	movq	40(%rsp), %r14          # 8-byte Reload
	je	.LBB3_60
# BB#54:                                #   in Loop: Header=BB3_18 Depth=2
	movq	24(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB3_58
# BB#55:                                #   in Loop: Header=BB3_18 Depth=2
	cmpq	%rcx, %rbp
	jbe	.LBB3_58
# BB#56:                                #   in Loop: Header=BB3_18 Depth=2
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movl	%ebp, %edx
	callq	cli_dbgmsg
	testb	$1, 41(%r12)
	jne	.LBB3_102
	.p2align	4, 0x90
.LBB3_57:                               # %.backedge
                                        #   in Loop: Header=BB3_18 Depth=2
	movq	%r15, %rdi
	leaq	56(%rsp), %rsi
	callq	zip_dir_read
	testl	%eax, %eax
	jne	.LBB3_14
	jmp	.LBB3_86
.LBB3_58:                               #   in Loop: Header=BB3_18 Depth=2
	movl	4(%rsi), %esi
	testl	%esi, %esi
	je	.LBB3_60
# BB#59:                                #   in Loop: Header=BB3_18 Depth=2
	cmpl	%esi, 12(%rsp)          # 4-byte Folded Reload
	ja	.LBB3_104
.LBB3_60:                               # %.loopexit157
                                        #   in Loop: Header=BB3_18 Depth=2
	movl	84(%rsp), %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	zip_file_open
	testq	%rax, %rax
	je	.LBB3_63
# BB#61:                                # %.preheader148
                                        #   in Loop: Header=BB3_18 Depth=2
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%edi, %edi
	leaq	24(%rsp), %rsi
	leaq	4(%rsp), %rdx
	callq	cli_gentempfd
	movl	%eax, %r13d
	testl	%r13d, %r13d
	je	.LBB3_65
# BB#62:                                #   in Loop: Header=BB3_18 Depth=2
	xorl	%ebx, %ebx
	jmp	.LBB3_8
.LBB3_63:                               #   in Loop: Header=BB3_18 Depth=2
	cmpl	$-125, 4(%r15)
	jne	.LBB3_106
# BB#64:                                #   in Loop: Header=BB3_18 Depth=2
	movl	$-125, 8(%rsp)          # 4-byte Folded Spill
	testb	$1, 48(%rsp)            # 1-byte Folded Reload
	je	.LBB3_11
	jmp	.LBB3_80
.LBB3_65:                               # %.preheader.lr.ph
                                        #   in Loop: Header=BB3_18 Depth=2
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB3_66:                               # %.preheader
                                        #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_18 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_67 Depth 4
	xorl	%ebx, %ebx
	movq	112(%rsp), %r12         # 8-byte Reload
	movq	16(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_67:                               #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_18 Depth=2
                                        #       Parent Loop BB3_66 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	$8192, %edx             # imm = 0x2000
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	zip_file_read
	movq	%rax, %rbp
	testl	%ebp, %ebp
	jle	.LBB3_70
# BB#68:                                #   in Loop: Header=BB3_67 Depth=4
	movslq	%ebp, %rax
	addq	%rax, %rbx
	movl	4(%rsp), %edi
	movq	%r12, %rsi
	movl	%ebp, %edx
	callq	cli_writen
	cmpl	%ebp, %eax
	je	.LBB3_67
# BB#69:                                #   in Loop: Header=BB3_66 Depth=3
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-123, %r13d
	jmp	.LBB3_71
.LBB3_70:                               #   in Loop: Header=BB3_66 Depth=3
	xorl	%r13d, %r13d
.LBB3_71:                               # %.loopexit
                                        #   in Loop: Header=BB3_66 Depth=3
	movl	64(%rsp), %edx
	cmpq	%rdx, %rbx
	movq	88(%rsp), %r12          # 8-byte Reload
	jne	.LBB3_75
# BB#72:                                #   in Loop: Header=BB3_66 Depth=3
	movq	24(%rsp), %rsi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	4(%rsp), %edi
	callq	fsync
	cmpl	$-1, %eax
	je	.LBB3_5
# BB#73:                                #   in Loop: Header=BB3_66 Depth=3
	movl	4(%rsp), %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	lseek
	movl	4(%rsp), %edi
	movq	%r12, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %r13d
	cmpl	$1, %r13d
	je	.LBB3_12
# BB#74:                                #   in Loop: Header=BB3_66 Depth=3
	movb	$1, %al
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB3_76
.LBB3_75:                               #   in Loop: Header=BB3_66 Depth=3
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movzwl	(%rax), %eax
	cmpl	$65535, %eax            # imm = 0xFFFF
	je	.LBB3_6
.LBB3_76:                               # %.thread
                                        #   in Loop: Header=BB3_66 Depth=3
	movl	4(%rsp), %edi
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB3_78
# BB#77:                                #   in Loop: Header=BB3_66 Depth=3
	movq	24(%rsp), %rdi
	callq	unlink
.LBB3_78:                               #   in Loop: Header=BB3_66 Depth=3
	movq	24(%rsp), %rdi
	callq	free
	movl	$-1, 4(%rsp)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rax
	movl	%r14d, %ecx
	movzwl	(%rax,%rcx,2), %esi
	cmpl	$65535, %esi            # imm = 0xFFFF
	je	.LBB3_7
# BB#79:                                #   in Loop: Header=BB3_66 Depth=3
	movw	%si, 8(%rdx)
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	incl	%r14d
	xorl	%edi, %edi
	leaq	24(%rsp), %rsi
	leaq	4(%rsp), %rdx
	callq	cli_gentempfd
	movl	%eax, %r13d
	testl	%r13d, %r13d
	je	.LBB3_66
	jmp	.LBB3_7
.LBB3_80:                               # %.outer
                                        #   in Loop: Header=BB3_4 Depth=1
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movl	$.L.str.47, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movq	%r15, %rdi
	leaq	56(%rsp), %rsi
	callq	zip_dir_read
	movl	$-125, %r13d
	testl	%eax, %eax
	jne	.LBB3_4
	jmp	.LBB3_91
.LBB3_81:
	movl	108(%rsp), %esi
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB3_96
.LBB3_82:
	movl	$.L.str.32, %edi
	movl	$8192, %esi             # imm = 0x2000
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	zip_dir_close
	movl	$-114, %r13d
	jmp	.LBB3_96
.LBB3_83:
	xorl	%r13d, %r13d
	jmp	.LBB3_91
.LBB3_86:
	movl	8(%rsp), %r13d          # 4-byte Reload
	jmp	.LBB3_91
.LBB3_84:
	movl	$.L.str.33, %edi
	jmp	.LBB3_89
.LBB3_87:
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%r12), %rax
	movq	$.L.str.37, (%rax)
	jmp	.LBB3_90
.LBB3_88:
	movl	$.L.str.39, %edi
.LBB3_89:                               # %.loopexit153
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%r12), %rax
	movq	$.L.str.34, (%rax)
.LBB3_90:                               # %.loopexit153
	movl	$1, %r13d
.LBB3_91:                               # %.loopexit153
	movq	%r15, %rdi
	callq	zip_dir_close
	movl	4(%rsp), %edi
	cmpl	$-1, %edi
	je	.LBB3_95
# BB#92:
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB3_94
# BB#93:
	movq	24(%rsp), %rdi
	callq	unlink
.LBB3_94:
	movq	24(%rsp), %rdi
	callq	free
.LBB3_95:
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	free
.LBB3_96:
	movl	%r13d, %eax
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_97:
	movq	40(%r12), %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	movq	%rax, (%rcx)
	movl	$1, %r13d
	movq	96(%rsp), %r15          # 8-byte Reload
	jmp	.LBB3_91
.LBB3_98:
	movl	$.L.str.41, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r13d, %edi
	callq	lseek
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%r13d, %edi
	movq	%r12, %rsi
	callq	cli_scandesc
	movl	%eax, %r13d
	testl	%r13d, %r13d
	js	.LBB3_91
# BB#99:
	cmpl	$1, %r13d
	je	.LBB3_91
# BB#100:
	movq	(%r12), %rax
	movq	$.L.str.42, (%rax)
	jmp	.LBB3_90
.LBB3_101:
	movq	(%r12), %rax
	movq	$.L.str.40, (%rax)
	jmp	.LBB3_90
.LBB3_102:
	movq	(%r12), %rax
	movq	$.L.str.44, (%rax)
	jmp	.LBB3_90
.LBB3_103:                              # %.thread147
	movl	$.L.str.55, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-104, %r13d
	jmp	.LBB3_91
.LBB3_104:
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	$1, 41(%r12)
	movl	8(%rsp), %r13d          # 4-byte Reload
	je	.LBB3_91
# BB#105:
	movq	(%r12), %rax
	movq	$.L.str.46, (%rax)
	jmp	.LBB3_90
.LBB3_106:
	movq	72(%rsp), %rsi
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-104, %r13d
	jmp	.LBB3_91
.Lfunc_end3:
	.size	cli_scanzip, .Lfunc_end3-cli_scanzip
	.cfi_endproc

	.p2align	4, 0x90
	.type	cli_scanarj,@function
cli_scanarj:                            # @cli_scanarj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 96
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movl	%edi, %r12d
	movl	$.L.str.66, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r13
	movl	$448, %esi              # imm = 0x1C0
	movq	%r13, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB4_2
# BB#1:
	movl	$.L.str.67, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	cli_dbgmsg
	movq	%r13, %rdi
	callq	free
	movl	$-118, %ebp
	jmp	.LBB4_34
.LBB4_2:
	testq	%rbp, %rbp
	je	.LBB4_4
# BB#3:
	xorl	%edx, %edx
	movl	%r12d, %edi
	movq	%rbp, %rsi
	callq	lseek
.LBB4_4:
	movl	%r12d, %edi
	movq	%r13, %rsi
	callq	cli_unarj_open
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB4_8
# BB#5:
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB4_7
# BB#6:
	movq	%r13, %rdi
	callq	cli_rmdirs
.LBB4_7:
	movq	%r13, %rdi
	callq	free
	movl	%ebp, %edi
	callq	cl_strerror
	movq	%rax, %rcx
	movl	$.L.str.68, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_dbgmsg
	jmp	.LBB4_34
.LBB4_8:
	movq	$0, 24(%rsp)
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB4_9:                                # =>This Inner Loop Header: Depth=1
	movl	%r12d, %edi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	cli_unarj_prepare_file
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB4_29
# BB#10:                                #   in Loop: Header=BB4_9 Depth=1
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.LBB4_22
# BB#11:                                #   in Loop: Header=BB4_9 Depth=1
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB4_14
# BB#12:                                #   in Loop: Header=BB4_9 Depth=1
	movl	12(%rsp), %edx
	cmpq	%rcx, %rdx
	jbe	.LBB4_14
# BB#13:                                #   in Loop: Header=BB4_9 Depth=1
	movq	24(%rsp), %rsi
	testq	%rsi, %rsi
	movl	$.L.str.72, %eax
	cmoveq	%rax, %rsi
	movl	$.L.str.71, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	$1, 41(%r14)
	je	.LBB4_22
	jmp	.LBB4_28
	.p2align	4, 0x90
.LBB4_14:                               #   in Loop: Header=BB4_9 Depth=1
	movl	12(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB4_22
# BB#15:                                #   in Loop: Header=BB4_9 Depth=1
	movl	12(%rsp), %eax
	testl	%eax, %eax
	je	.LBB4_22
# BB#16:                                #   in Loop: Header=BB4_9 Depth=1
	movl	8(%rsp), %esi
	testl	%esi, %esi
	je	.LBB4_22
# BB#17:                                #   in Loop: Header=BB4_9 Depth=1
	xorl	%edx, %edx
	divl	%esi
	movl	%eax, %edx
	cmpl	%ecx, %edx
	jb	.LBB4_22
# BB#18:                                #   in Loop: Header=BB4_9 Depth=1
	movl	$.L.str.74, %edi
	xorl	%eax, %eax
	movl	%edx, %esi
	movl	%ecx, %edx
	callq	cli_dbgmsg
	movq	32(%r14), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.LBB4_20
# BB#19:                                #   in Loop: Header=BB4_9 Depth=1
	movl	12(%rsp), %ecx
	cmpq	%rax, %rcx
	jbe	.LBB4_21
.LBB4_20:                               #   in Loop: Header=BB4_9 Depth=1
	testb	$1, 41(%r14)
	je	.LBB4_22
	jmp	.LBB4_35
.LBB4_21:                               #   in Loop: Header=BB4_9 Depth=1
	movl	$.L.str.75, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	.p2align	4, 0x90
.LBB4_22:                               #   in Loop: Header=BB4_9 Depth=1
	movl	%r12d, %edi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	cli_unarj_extract_file
	movl	%eax, %ebp
	movl	36(%rsp), %edi
	testl	%edi, %edi
	js	.LBB4_24
# BB#23:                                #   in Loop: Header=BB4_9 Depth=1
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	lseek
	movl	36(%rsp), %edi
	movq	%r14, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %ebx
	movl	36(%rsp), %edi
	callq	close
	cmpl	$1, %ebx
	je	.LBB4_27
.LBB4_24:                               #   in Loop: Header=BB4_9 Depth=1
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_26
# BB#25:                                #   in Loop: Header=BB4_9 Depth=1
	callq	free
	movq	$0, 24(%rsp)
.LBB4_26:                               #   in Loop: Header=BB4_9 Depth=1
	testl	%ebp, %ebp
	je	.LBB4_9
.LBB4_29:                               # %cli_unarj_checklimits.exit
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB4_31
.LBB4_30:
	movq	%r13, %rdi
	callq	cli_rmdirs
	jmp	.LBB4_31
.LBB4_27:
	movq	(%r14), %rax
	movq	(%rax), %rsi
	movl	$.L.str.69, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB4_36:
	movl	$1, %ebp
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB4_31
	jmp	.LBB4_30
.LBB4_28:
	movq	(%r14), %rax
	movq	$.L.str.73, (%rax)
	movl	$1, %ebp
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB4_30
.LBB4_31:
	movq	%r13, %rdi
	callq	free
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_33
# BB#32:
	callq	free
.LBB4_33:
	xorl	%ebx, %ebx
	movl	$.L.str.70, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	cmpl	$2, %ebp
	cmovel	%ebx, %ebp
.LBB4_34:
	movl	%ebp, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_35:
	movq	(%r14), %rax
	movq	$.L.str.76, (%rax)
	jmp	.LBB4_36
.Lfunc_end4:
	.size	cli_scanarj, .Lfunc_end4-cli_scanarj
	.cfi_endproc

	.p2align	4, 0x90
	.type	cli_scanmscab,@function
cli_scanmscab:                          # @cli_scanmscab
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 96
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movl	%edi, %ebp
	xorl	%r15d, %r15d
	movl	$.L.str.79, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leaq	8(%rsp), %rdx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	cab_open
	movl	%eax, %ebx
	testl	%ebx, %ebx
	jne	.LBB5_23
# BB#1:
	movq	32(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB5_22
# BB#2:                                 # %.lr.ph
	movl	$1, %r13d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB5_3:                                # =>This Inner Loop Header: Depth=1
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.LBB5_12
# BB#4:                                 #   in Loop: Header=BB5_3 Depth=1
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB5_8
# BB#5:                                 #   in Loop: Header=BB5_3 Depth=1
	movl	(%rbx), %edx
	cmpq	%rcx, %rdx
	jbe	.LBB5_8
# BB#6:                                 #   in Loop: Header=BB5_3 Depth=1
	movq	16(%rbx), %rsi
	movl	$.L.str.80, %edi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_dbgmsg
	testb	$1, 41(%r14)
	je	.LBB5_21
	jmp	.LBB5_7
	.p2align	4, 0x90
.LBB5_8:                                #   in Loop: Header=BB5_3 Depth=1
	movl	4(%rax), %esi
	testl	%esi, %esi
	je	.LBB5_12
# BB#9:                                 #   in Loop: Header=BB5_3 Depth=1
	cmpl	%esi, %r13d
	ja	.LBB5_10
.LBB5_12:                               # %.thread
                                        #   in Loop: Header=BB5_3 Depth=1
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %rbp
	movq	16(%rbx), %rsi
	movl	(%rbx), %ecx
	movl	$.L.str.84, %edi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	cab_extract
	movl	%eax, %r15d
	testl	%r15d, %r15d
	je	.LBB5_14
# BB#13:                                #   in Loop: Header=BB5_3 Depth=1
	movl	%r15d, %edi
	callq	cl_strerror
	movq	%rax, %rcx
	movl	$.L.str.85, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_dbgmsg
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB5_19
	jmp	.LBB5_18
	.p2align	4, 0x90
.LBB5_14:                               #   in Loop: Header=BB5_3 Depth=1
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	movl	%eax, %r12d
	cmpl	$-1, %r12d
	je	.LBB5_15
# BB#16:                                #   in Loop: Header=BB5_3 Depth=1
	movl	%r12d, %edi
	movq	%r14, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %r15d
	movl	%r12d, %edi
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB5_19
	jmp	.LBB5_18
.LBB5_15:                               #   in Loop: Header=BB5_3 Depth=1
	movl	$-115, %r15d
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB5_19
	.p2align	4, 0x90
.LBB5_18:                               #   in Loop: Header=BB5_3 Depth=1
	movq	%rbp, %rdi
	callq	unlink
.LBB5_19:                               #   in Loop: Header=BB5_3 Depth=1
	movq	%rbp, %rdi
	callq	free
	cmpl	$1, %r15d
	je	.LBB5_20
.LBB5_21:                               #   in Loop: Header=BB5_3 Depth=1
	movq	48(%rbx), %rbx
	incl	%r13d
	testq	%rbx, %rbx
	jne	.LBB5_3
	jmp	.LBB5_22
.LBB5_20:
	movl	$1, %r15d
.LBB5_22:                               # %._crit_edge
	leaq	8(%rsp), %rdi
	callq	cab_free
	movl	%r15d, %ebx
.LBB5_23:
	movl	%ebx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_10:
	xorl	%ebx, %ebx
	movl	$.L.str.82, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leaq	8(%rsp), %rdi
	callq	cab_free
	testb	$1, 41(%r14)
	je	.LBB5_23
# BB#11:
	movq	(%r14), %rax
	movq	$.L.str.83, (%rax)
	movl	$1, %ebx
	jmp	.LBB5_23
.LBB5_7:
	movq	(%r14), %rax
	movq	$.L.str.81, (%rax)
	leaq	8(%rsp), %rdi
	callq	cab_free
	movl	$1, %ebx
	jmp	.LBB5_23
.Lfunc_end5:
	.size	cli_scanmscab, .Lfunc_end5-cli_scanmscab
	.cfi_endproc

	.p2align	4, 0x90
	.type	cli_scanhtml,@function
cli_scanhtml:                           # @cli_scanhtml
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 40
	subq	$1176, %rsp             # imm = 0x498
.Lcfi87:
	.cfi_def_cfa_offset 1216
.Lcfi88:
	.cfi_offset %rbx, -40
.Lcfi89:
	.cfi_offset %r14, -32
.Lcfi90:
	.cfi_offset %r15, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %ebp
	movl	$.L.str.86, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rsp, %rdx
	movl	$1, %edi
	movl	%ebp, %esi
	callq	__fxstat
	cmpl	$-1, %eax
	je	.LBB6_1
# BB#2:
	cmpq	$10485761, 48(%rsp)     # imm = 0xA00001
	jl	.LBB6_4
# BB#3:
	xorl	%ebx, %ebx
	movl	$.L.str.88, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB6_25
.LBB6_1:
	movl	$.L.str.87, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_errmsg
	movl	$-123, %ebx
	jmp	.LBB6_25
.LBB6_4:
	xorl	%ebx, %ebx
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r15
	movl	$448, %esi              # imm = 0x1C0
	movq	%r15, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB6_6
# BB#5:
	movl	$.L.str.89, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_errmsg
	movq	%r15, %rdi
	callq	free
	movl	$-118, %ebx
	jmp	.LBB6_25
.LBB6_6:
	movq	56(%r14), %rcx
	xorl	%edx, %edx
	movl	%ebp, %edi
	movq	%r15, %rsi
	callq	html_normalise_fd
	leaq	144(%rsp), %rbp
	movl	$1024, %esi             # imm = 0x400
	movl	$.L.str.90, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rcx
	callq	snprintf
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB6_8
# BB#7:
	xorl	%edx, %edx
	movl	$528, %ecx              # imm = 0x210
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%ebp, %edi
	movq	%r14, %rsi
	callq	cli_scandesc
	movl	%eax, %ebx
	movl	%ebp, %edi
	callq	close
.LBB6_8:
	testl	%ebx, %ebx
	js	.LBB6_22
# BB#9:
	cmpl	$1, %ebx
	je	.LBB6_22
# BB#10:
	testl	%ebx, %ebx
	jne	.LBB6_13
# BB#11:
	xorl	%ebx, %ebx
	leaq	144(%rsp), %rbp
	movl	$1024, %esi             # imm = 0x400
	movl	$.L.str.91, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rcx
	callq	snprintf
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB6_13
# BB#12:
	xorl	%edx, %edx
	movl	$528, %ecx              # imm = 0x210
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%ebp, %edi
	movq	%r14, %rsi
	callq	cli_scandesc
	movl	%eax, %ebx
	movl	%ebp, %edi
	callq	close
.LBB6_13:
	testl	%ebx, %ebx
	js	.LBB6_22
# BB#14:
	cmpl	$1, %ebx
	je	.LBB6_22
# BB#15:
	testl	%ebx, %ebx
	jne	.LBB6_18
# BB#16:
	xorl	%ebx, %ebx
	leaq	144(%rsp), %rbp
	movl	$1024, %esi             # imm = 0x400
	movl	$.L.str.92, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rcx
	callq	snprintf
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB6_18
# BB#17:
	xorl	%edx, %edx
	movl	$528, %ecx              # imm = 0x210
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%ebp, %edi
	movq	%r14, %rsi
	callq	cli_scandesc
	movl	%eax, %ebx
	movl	%ebp, %edi
	callq	close
.LBB6_18:
	testl	%ebx, %ebx
	js	.LBB6_22
# BB#19:
	cmpl	$1, %ebx
	je	.LBB6_22
# BB#20:
	testl	%ebx, %ebx
	jne	.LBB6_22
# BB#21:
	leaq	144(%rsp), %rbx
	movl	$1024, %esi             # imm = 0x400
	movl	$.L.str.93, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rcx
	callq	snprintf
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	cli_scandir
	movl	%eax, %ebx
.LBB6_22:
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB6_24
# BB#23:
	movq	%r15, %rdi
	callq	cli_rmdirs
.LBB6_24:
	movq	%r15, %rdi
	callq	free
.LBB6_25:
	movl	%ebx, %eax
	addq	$1176, %rsp             # imm = 0x498
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	cli_scanhtml, .Lfunc_end6-cli_scanhtml
	.cfi_endproc

	.p2align	4, 0x90
	.type	cli_scanmail,@function
cli_scanmail:                           # @cli_scanmail
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 32
.Lcfi95:
	.cfi_offset %rbx, -32
.Lcfi96:
	.cfi_offset %r14, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	44(%rbx), %edx
	movl	48(%rbx), %esi
	movl	$.L.str.98, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r14
	movl	$448, %esi              # imm = 0x1C0
	movq	%r14, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB7_2
# BB#1:
	movl	$.L.str.99, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movq	%r14, %rdi
	callq	free
	movl	$-118, %ebp
	jmp	.LBB7_7
.LBB7_2:
	movq	%r14, %rdi
	movl	%ebp, %esi
	movq	%rbx, %rdx
	callq	cli_mbox
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB7_4
# BB#3:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	cli_scandir
	movl	%eax, %ebp
.LBB7_4:
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB7_6
# BB#5:
	movq	%r14, %rdi
	callq	cli_rmdirs
.LBB7_6:
	movq	%r14, %rdi
	callq	free
.LBB7_7:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end7:
	.size	cli_scanmail, .Lfunc_end7-cli_scanmail
	.cfi_endproc

	.p2align	4, 0x90
	.type	cli_scantar,@function
cli_scantar:                            # @cli_scantar
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi102:
	.cfi_def_cfa_offset 48
.Lcfi103:
	.cfi_offset %rbx, -40
.Lcfi104:
	.cfi_offset %r14, -32
.Lcfi105:
	.cfi_offset %r15, -24
.Lcfi106:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r14
	movl	%edi, %ebp
	movl	$.L.str.116, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %rbx
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbx, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB8_2
# BB#1:
	movl	$.L.str.117, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
	movq	%rbx, %rdi
	callq	free
	movl	$-118, %ebp
	jmp	.LBB8_8
.LBB8_2:
	movq	32(%r14), %rcx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%r15d, %edx
	callq	cli_untar
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB8_4
# BB#3:
	movl	%ebp, %edi
	callq	cl_strerror
	movq	%rax, %rcx
	movl	$.L.str.118, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_dbgmsg
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB8_7
	jmp	.LBB8_6
.LBB8_4:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	cli_scandir
	movl	%eax, %ebp
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB8_7
.LBB8_6:
	movq	%rbx, %rdi
	callq	cli_rmdirs
.LBB8_7:
	movq	%rbx, %rdi
	callq	free
.LBB8_8:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	cli_scantar, .Lfunc_end8-cli_scantar
	.cfi_endproc

	.globl	cl_scandesc
	.p2align	4, 0x90
	.type	cl_scandesc,@function
cl_scandesc:                            # @cl_scandesc
	.cfi_startproc
# BB#0:
	subq	$72, %rsp
.Lcfi107:
	.cfi_def_cfa_offset 80
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%rsp)
	movups	%xmm0, 40(%rsp)
	movups	%xmm0, 24(%rsp)
	movq	%rcx, 32(%rsp)
	movq	%rsi, 8(%rsp)
	movq	%r8, 40(%rsp)
	movq	%rdx, 16(%rsp)
	movl	%r9d, 48(%rsp)
	movl	$0, 60(%rsp)
	movq	80(%rcx), %rax
	movq	%rax, 64(%rsp)
	leaq	8(%rsp), %rsi
	callq	cli_magic_scandesc
	cmpl	$0, 60(%rsp)
	movl	$1, %ecx
	cmovel	%eax, %ecx
	testl	%eax, %eax
	cmovel	%ecx, %eax
	addq	$72, %rsp
	retq
.Lfunc_end9:
	.size	cl_scandesc, .Lfunc_end9-cl_scandesc
	.cfi_endproc

	.globl	cl_scanfile
	.p2align	4, 0x90
	.type	cl_scanfile,@function
cl_scanfile:                            # @cl_scanfile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi111:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi112:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi114:
	.cfi_def_cfa_offset 128
.Lcfi115:
	.cfi_offset %rbx, -56
.Lcfi116:
	.cfi_offset %r12, -48
.Lcfi117:
	.cfi_offset %r13, -40
.Lcfi118:
	.cfi_offset %r14, -32
.Lcfi119:
	.cfi_offset %r15, -24
.Lcfi120:
	.cfi_offset %rbp, -16
	movl	%r9d, %r15d
	movq	%r8, %r12
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	%rsi, %rbp
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	open
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB10_1
# BB#2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%rsp)
	movups	%xmm0, 40(%rsp)
	movups	%xmm0, 24(%rsp)
	movq	%r14, 32(%rsp)
	movq	%rbp, 8(%rsp)
	movq	%r12, 40(%rsp)
	movq	%r13, 16(%rsp)
	movl	%r15d, 48(%rsp)
	movl	$0, 60(%rsp)
	movq	80(%r14), %rax
	movq	%rax, 64(%rsp)
	leaq	8(%rsp), %rsi
	movl	%ebx, %edi
	callq	cli_magic_scandesc
	cmpl	$0, 60(%rsp)
	movl	$1, %ebp
	cmovel	%eax, %ebp
	testl	%eax, %eax
	cmovnel	%eax, %ebp
	movl	%ebx, %edi
	callq	close
	jmp	.LBB10_3
.LBB10_1:
	movl	$-115, %ebp
.LBB10_3:
	movl	%ebp, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	cl_scanfile, .Lfunc_end10-cl_scanfile
	.cfi_endproc

	.p2align	4, 0x90
	.type	cli_vba_scandir,@function
cli_vba_scandir:                        # @cli_vba_scandir
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi124:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi125:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi127:
	.cfi_def_cfa_offset 224
.Lcfi128:
	.cfi_offset %rbx, -56
.Lcfi129:
	.cfi_offset %r12, -48
.Lcfi130:
	.cfi_offset %r13, -40
.Lcfi131:
	.cfi_offset %r14, -32
.Lcfi132:
	.cfi_offset %r15, -24
.Lcfi133:
	.cfi_offset %rbp, -16
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %r15
	movl	$.L.str.108, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	vba56_dir_read
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB11_22
# BB#1:                                 # %.preheader228
	cmpl	$0, (%r13)
	jle	.LBB11_2
# BB#3:                                 # %.lr.ph258
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	%r13, %r14
	addq	$8, %r14
	leaq	20(%rsp), %r12
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB11_4:                               # =>This Inner Loop Header: Depth=1
	movq	40(%r13), %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	8(%r13), %rax
	movq	(%rax,%r15,8), %rdi
	callq	strlen
	leaq	2(%rbx,%rax), %rdi
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB11_5
# BB#6:                                 #   in Loop: Header=BB11_4 Depth=1
	movq	8(%r13), %rax
	movq	40(%r13), %rdx
	movq	(%rax,%r15,8), %rcx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sprintf
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB11_7
# BB#8:                                 #   in Loop: Header=BB11_4 Depth=1
	movq	%rbp, %rdi
	callq	free
	movq	8(%r13), %rax
	movq	(%rax,%r15,8), %rsi
	movl	$.L.str.110, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	16(%r13), %rax
	movl	(%rax,%r15,4), %esi
	movl	%ebx, %edi
	movq	%r12, %rdx
	callq	vba_decompress
	movq	%rax, %rbp
	movl	%ebx, %edi
	callq	close
	testq	%rbp, %rbp
	je	.LBB11_9
# BB#12:                                #   in Loop: Header=BB11_4 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rax), %rax
	testq	%rax, %rax
	movl	20(%rsp), %esi
	je	.LBB11_14
# BB#13:                                #   in Loop: Header=BB11_4 Depth=1
	movl	%esi, %ecx
	sarl	$31, %ecx
	shrl	$20, %ecx
	addl	%esi, %ecx
	sarl	$12, %ecx
	movslq	%ecx, %rcx
	addq	%rcx, (%rax)
.LBB11_14:                              # %._crit_edge278
                                        #   in Loop: Header=BB11_4 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rdx
	movq	24(%rax), %rcx
	movl	$513, %r8d              # imm = 0x201
	movq	%rbp, %rdi
	callq	cli_scanbuff
	movl	%eax, %ebx
	movq	%rbp, %rdi
	callq	free
	cmpl	$1, %ebx
	jne	.LBB11_10
	jmp	.LBB11_15
	.p2align	4, 0x90
.LBB11_9:                               #   in Loop: Header=BB11_4 Depth=1
	movq	(%r14), %rax
	movq	(%rax,%r15,8), %rsi
	movl	$.L.str.111, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB11_10:                              #   in Loop: Header=BB11_4 Depth=1
	incq	%r15
	movslq	(%r13), %rax
	cmpq	%rax, %r15
	jl	.LBB11_4
# BB#11:
	xorl	%ebp, %ebp
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB11_18
.LBB11_22:
	movq	%r15, %rdi
	callq	ppt_vba_read
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB11_26
# BB#23:
	movq	%rbx, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	cli_scandir
	xorl	%ebp, %ebp
	cmpl	$1, %eax
	sete	%r14b
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB11_25
# BB#24:
	movq	%rbx, %rdi
	callq	cli_rmdirs
.LBB11_25:
	movb	%r14b, %bpl
	movq	%rbx, %rdi
	jmp	.LBB11_50
.LBB11_2:                               # %.preheader228..loopexit229_crit_edge
	movq	%r13, %r14
	addq	$8, %r14
	xorl	%ebp, %ebp
	jmp	.LBB11_17
.LBB11_26:
	movq	%r15, %rdi
	callq	wm_dir_read
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB11_51
# BB#27:                                # %.preheader226
	cmpl	$0, (%r12)
	jle	.LBB11_28
# BB#29:                                # %.lr.ph243
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_30:                              # =>This Inner Loop Header: Depth=1
	movq	40(%r12), %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	8(%r12), %rax
	movq	(%rax,%rbp,8), %rdi
	callq	strlen
	leaq	2(%rbx,%rax), %rdi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB11_31
# BB#32:                                #   in Loop: Header=BB11_30 Depth=1
	movq	8(%r12), %rax
	movq	40(%r12), %rdx
	movq	(%rax,%rbp,8), %rcx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movl	%eax, %r13d
	cmpl	$-1, %r13d
	je	.LBB11_33
# BB#34:                                #   in Loop: Header=BB11_30 Depth=1
	movq	%rbx, %rdi
	callq	free
	movq	8(%r12), %rax
	movq	24(%r12), %rdx
	movq	(%rax,%rbp,8), %rsi
	movq	32(%r12), %rax
	movzbl	(%rax,%rbp), %ecx
	movl	(%rdx,%rbp,4), %r8d
	movl	$.L.str.112, %edi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	cli_dbgmsg
	movq	24(%r12), %rax
	movl	(%rax,%rbp,4), %edx
	testl	%edx, %edx
	je	.LBB11_35
# BB#36:                                #   in Loop: Header=BB11_30 Depth=1
	movq	16(%r12), %rax
	movq	32(%r12), %rcx
	movl	(%rax,%rbp,4), %esi
	movzbl	(%rcx,%rbp), %ecx
	movl	%r13d, %edi
	callq	wm_decrypt_macro
	movq	%rax, %rbx
	movl	%r13d, %edi
	callq	close
	testq	%rbx, %rbx
	je	.LBB11_37
# BB#40:                                #   in Loop: Header=BB11_30 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rax), %rax
	testq	%rax, %rax
	movq	24(%r12), %rcx
	movl	(%rcx,%rbp,4), %esi
	je	.LBB11_42
# BB#41:                                #   in Loop: Header=BB11_30 Depth=1
	movl	%esi, %ecx
	shrl	$12, %ecx
	addq	%rcx, (%rax)
.LBB11_42:                              # %._crit_edge279
                                        #   in Loop: Header=BB11_30 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rdx
	movq	24(%rax), %rcx
	movl	$513, %r8d              # imm = 0x201
	movq	%rbx, %rdi
	callq	cli_scanbuff
	movl	%eax, %r13d
	movq	%rbx, %rdi
	callq	free
	cmpl	$1, %r13d
	jne	.LBB11_38
	jmp	.LBB11_43
.LBB11_35:                              # %.thread
                                        #   in Loop: Header=BB11_30 Depth=1
	movl	%r13d, %edi
	callq	close
.LBB11_37:                              #   in Loop: Header=BB11_30 Depth=1
	movq	8(%r12), %rax
	movq	(%rax,%rbp,8), %rsi
	movl	$.L.str.113, %edi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	cli_dbgmsg
.LBB11_38:                              #   in Loop: Header=BB11_30 Depth=1
	incq	%rbp
	movslq	(%r12), %rax
	cmpq	%rax, %rbp
	jl	.LBB11_30
# BB#39:
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jg	.LBB11_47
	jmp	.LBB11_46
.LBB11_5:
	movl	$-114, %ebp
	jmp	.LBB11_16
.LBB11_7:
	movl	$.L.str.109, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	free
	movl	$-115, %ebp
	jmp	.LBB11_16
.LBB11_15:
	movl	$1, %ebp
.LBB11_16:                              # %.loopexit229thread-pre-split
	movq	8(%rsp), %r15           # 8-byte Reload
.LBB11_17:                              # %.loopexit229thread-pre-split
	movl	(%r13), %eax
.LBB11_18:                              # %.loopexit229
	movq	(%r14), %rdi
	testl	%eax, %eax
	jle	.LBB11_21
# BB#19:                                # %.lr.ph254.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_20:                              # %.lr.ph254
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rdi
	callq	free
	incq	%rbx
	movslq	(%r13), %rax
	movq	(%r14), %rdi
	cmpq	%rax, %rbx
	jl	.LBB11_20
.LBB11_21:                              # %._crit_edge255
	callq	free
	movq	40(%r13), %rdi
	callq	free
	movq	16(%r13), %rdi
	callq	free
	movq	%r13, %rdi
.LBB11_50:
	callq	free
	testl	%ebp, %ebp
	jne	.LBB11_75
.LBB11_51:                              # %.thread223
	movq	%r15, %rdi
	callq	strlen
	leaq	16(%rax), %rdi
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB11_52
# BB#53:
	movl	$.L.str.114, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rdx
	callq	sprintf
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	movl	%eax, %ebx
	movq	%rbp, %rdi
	callq	free
	testl	%ebx, %ebx
	js	.LBB11_57
# BB#54:
	movl	%ebx, %edi
	movq	%r15, %rsi
	callq	cli_decode_ole_object
	movl	%eax, %r12d
	testl	%r12d, %r12d
	js	.LBB11_55
# BB#56:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%r12d, %edi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	cli_scandesc
	movl	%eax, %ebp
	movl	%r12d, %edi
	callq	close
	movl	%ebx, %edi
	callq	close
	testl	%ebp, %ebp
	jne	.LBB11_75
	jmp	.LBB11_57
.LBB11_52:
	movl	$-114, %ebp
	jmp	.LBB11_75
.LBB11_55:                              # %.thread225
	movl	%ebx, %edi
	callq	close
.LBB11_57:
	movq	%r15, %rdi
	callq	opendir
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB11_76
# BB#58:                                # %.preheader
	movq	%r13, %rdi
	callq	readdir
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB11_73
# BB#59:                                # %.lr.ph
	leaq	24(%rsp), %r12
	movl	$61440, %r14d           # imm = 0xF000
	.p2align	4, 0x90
.LBB11_60:                              # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rbp)
	je	.LBB11_72
# BB#61:                                #   in Loop: Header=BB11_60 Depth=1
	cmpb	$46, 19(%rbp)
	jne	.LBB11_65
# BB#62:                                #   in Loop: Header=BB11_60 Depth=1
	cmpb	$0, 20(%rbp)
	je	.LBB11_72
# BB#63:                                #   in Loop: Header=BB11_60 Depth=1
	cmpb	$46, 20(%rbp)
	jne	.LBB11_65
# BB#64:                                #   in Loop: Header=BB11_60 Depth=1
	cmpb	$0, 21(%rbp)
	je	.LBB11_72
	.p2align	4, 0x90
.LBB11_65:                              # %.thread290
                                        #   in Loop: Header=BB11_60 Depth=1
	addq	$19, %rbp
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	strlen
	leaq	2(%rbx,%rax), %rdi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB11_66
# BB#67:                                #   in Loop: Header=BB11_60 Depth=1
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	callq	sprintf
	movl	$1, %edi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	__lxstat
	cmpl	$-1, %eax
	je	.LBB11_71
# BB#68:                                #   in Loop: Header=BB11_60 Depth=1
	movl	48(%rsp), %eax
	andl	%r14d, %eax
	cmpl	$16384, %eax            # imm = 0x4000
	jne	.LBB11_71
# BB#69:                                #   in Loop: Header=BB11_60 Depth=1
	movq	%rbx, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	cli_vba_scandir
	cmpl	$1, %eax
	je	.LBB11_70
.LBB11_71:                              #   in Loop: Header=BB11_60 Depth=1
	movq	%rbx, %rdi
	callq	free
.LBB11_72:                              # %.backedge
                                        #   in Loop: Header=BB11_60 Depth=1
	movq	%r13, %rdi
	callq	readdir
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB11_60
.LBB11_73:
	xorl	%ebp, %ebp
.LBB11_74:                              # %.loopexit
	movq	%r13, %rdi
	callq	closedir
	jmp	.LBB11_75
.LBB11_76:
	movl	$.L.str.115, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movl	$-115, %ebp
.LBB11_75:
	movl	%ebp, %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_28:
	xorl	%ebp, %ebp
	jmp	.LBB11_46
.LBB11_66:
	movl	$-114, %ebp
	jmp	.LBB11_74
.LBB11_31:
	movl	$-114, %ebp
	jmp	.LBB11_44
.LBB11_33:
	movl	$.L.str.109, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	free
	movl	$-115, %ebp
	jmp	.LBB11_44
.LBB11_43:
	movl	$1, %ebp
.LBB11_44:                              # %.loopexit227thread-pre-split
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.LBB11_46
.LBB11_47:                              # %.lr.ph240
	movq	%r12, %r14
	addq	$8, %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_48:                              # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	free
	incq	%rbx
	movslq	(%r12), %rax
	cmpq	%rax, %rbx
	jl	.LBB11_48
	jmp	.LBB11_49
.LBB11_46:                              # %.loopexit227.._crit_edge_crit_edge
	movq	%r12, %r14
	addq	$8, %r14
.LBB11_49:                              # %._crit_edge
	movq	32(%r12), %rdi
	callq	free
	movq	24(%r12), %rdi
	callq	free
	movq	16(%r12), %rdi
	callq	free
	movq	(%r14), %rdi
	callq	free
	movq	40(%r12), %rdi
	callq	free
	movq	%r12, %rdi
	jmp	.LBB11_50
.LBB11_70:
	movq	%rbx, %rdi
	callq	free
	movl	$1, %ebp
	jmp	.LBB11_74
.Lfunc_end11:
	.size	cli_vba_scandir, .Lfunc_end11-cli_vba_scandir
	.cfi_endproc

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"%s/%s"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"ScanDir: Can't open directory %s.\n"
	.size	.L.str.3, 35

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Can't fstat descriptor %d\n"
	.size	.L.str.4, 27

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Small data (%u bytes)\n"
	.size	.L.str.5, 23

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"CRITICAL: engine == NULL\n"
	.size	.L.str.6, 26

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Raw mode: No support for special files\n"
	.size	.L.str.7, 40

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%s found in descriptor %d\n"
	.size	.L.str.8, 27

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Archive recursion limit exceeded (arec == %u).\n"
	.size	.L.str.9, 48

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Archive.ExceededRecursionLimit"
	.size	.L.str.10, 31

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Mail recursion level exceeded (mrec == %u).\n"
	.size	.L.str.11, 45

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"RAR code not compiled-in\n"
	.size	.L.str.12, 26

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"cli_magic_scandesc: Not checking for embedded PEs (zip file > 1 MB)\n"
	.size	.L.str.13, 69

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Descriptor[%d]: %s\n"
	.size	.L.str.14, 20

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"cli_scanraw: lseek() failed\n"
	.size	.L.str.15, 29

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"%s found in descriptor %d when scanning file type %u\n"
	.size	.L.str.16, 54

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"ZIP-SFX signature found at %u\n"
	.size	.L.str.17, 31

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"CAB-SFX signature found at %u\n"
	.size	.L.str.18, 31

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"ARJ-SFX signature found at %u\n"
	.size	.L.str.19, 31

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"NSIS signature found at %u\n"
	.size	.L.str.20, 28

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"AUTOIT signature found at %u\n"
	.size	.L.str.21, 30

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"PE signature found at %u\n"
	.size	.L.str.22, 26

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"*** Detected embedded PE file ***\n"
	.size	.L.str.23, 35

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"cli_scanraw: Type %u not handled in fpt loop\n"
	.size	.L.str.24, 46

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"cli_scanembpe: Can't create file %s\n"
	.size	.L.str.25, 37

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"cli_scanembpe: Size exceeded (stopped at %lu, max: %lu)\n"
	.size	.L.str.26, 57

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"cli_scanembpe: Can't write to temporary file\n"
	.size	.L.str.27, 46

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"cli_scanembpe: Can't synchronise descriptor %d\n"
	.size	.L.str.28, 48

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"cli_scanembpe: Infected with %s\n"
	.size	.L.str.29, 33

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"in scanzip()\n"
	.size	.L.str.30, 14

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Zip: zip_dir_open() return code: %d\n"
	.size	.L.str.31, 37

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Zip: unable to malloc(%u)\n"
	.size	.L.str.32, 27

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"Zip: zdirent.d_name == NULL\n"
	.size	.L.str.33, 29

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"Suspect.Zip"
	.size	.L.str.34, 12

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"Zip: %s, crc32: 0x%x, offset: %u, encrypted: %u, compressed: %u, normal: %u, method: %u, ratio: %u (max: %u)\n"
	.size	.L.str.35, 110

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"Zip: Broken file or modified information in local header part of archive\n"
	.size	.L.str.36, 74

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Exploit.Zip.ModifiedHeaders"
	.size	.L.str.37, 28

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"Zip: Directory entry with st_size != 0\n"
	.size	.L.str.38, 40

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"Zip: Malformed file (d_csize == 0 but st_size != 0)\n"
	.size	.L.str.39, 53

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"Oversized.Zip"
	.size	.L.str.40, 14

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"Zip: Encrypted files found in archive.\n"
	.size	.L.str.41, 40

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"Encrypted.Zip"
	.size	.L.str.42, 14

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"Zip: %s: Size exceeded (%u, max: %lu)\n"
	.size	.L.str.43, 39

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"Zip.ExceededFileSize"
	.size	.L.str.44, 21

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"Zip: Files limit reached (max: %u)\n"
	.size	.L.str.45, 36

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"Zip.ExceededFilesLimit"
	.size	.L.str.46, 23

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"Not supported compression method in one or more files\n"
	.size	.L.str.47, 55

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"Zip: Can't open file %s\n"
	.size	.L.str.48, 25

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"Zip: Can't write to file.\n"
	.size	.L.str.49, 27

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"Zip: Incorrectly decompressed (%lu != %lu)\n"
	.size	.L.str.50, 44

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"Zip: File decompressed to %s\n"
	.size	.L.str.51, 30

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"Zip: fsync() failed\n"
	.size	.L.str.52, 21

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"Zip: Infected with %s\n"
	.size	.L.str.53, 23

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"Zip: Brute force mode - checking compression method %u\n"
	.size	.L.str.54, 56

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"Zip: All attempts to decompress file failed\n"
	.size	.L.str.55, 45

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"in cli_scangzip()\n"
	.size	.L.str.56, 19

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"rb"
	.size	.L.str.57, 3

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"GZip: Can't open descriptor %d\n"
	.size	.L.str.58, 32

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"GZip: Can't generate temporary file.\n"
	.size	.L.str.59, 38

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"GZip: Unable to malloc %u bytes.\n"
	.size	.L.str.60, 34

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"GZip: Size exceeded (stopped at %ld, max: %ld)\n"
	.size	.L.str.61, 48

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"GZip.ExceededFileSize"
	.size	.L.str.62, 22

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"GZip: Can't write to file.\n"
	.size	.L.str.63, 28

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"GZip: Can't synchronise descriptor %d\n"
	.size	.L.str.64, 39

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"GZip: Infected with %s\n"
	.size	.L.str.65, 24

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"in cli_scanarj()\n"
	.size	.L.str.66, 18

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"RAR: Can't create temporary directory %s\n"
	.size	.L.str.67, 42

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"ARJ: Error: %s\n"
	.size	.L.str.68, 16

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"ARJ: infected with %s\n"
	.size	.L.str.69, 23

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"ARJ: Exit code: %d\n"
	.size	.L.str.70, 20

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"ARJ: %s: Size exceeded (%lu, max: %lu)\n"
	.size	.L.str.71, 40

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"(none)"
	.size	.L.str.72, 7

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"ARJ.ExceededFileSize"
	.size	.L.str.73, 21

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"ARJ: Max ratio reached (%u, max: %u)\n"
	.size	.L.str.74, 38

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"ARJ: Ignoring ratio limit (file size doesn't hit limits)\n"
	.size	.L.str.75, 58

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"Oversized.ARJ"
	.size	.L.str.76, 14

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"in cli_scanmscab()\n"
	.size	.L.str.79, 20

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"CAB: %s: Size exceeded (%u, max: %lu)\n"
	.size	.L.str.80, 39

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"CAB.ExceededFileSize"
	.size	.L.str.81, 21

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"CAB: Files limit reached (max: %u)\n"
	.size	.L.str.82, 36

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"CAB.ExceededFilesLimit"
	.size	.L.str.83, 23

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"CAB: Extracting file %s to %s, size %u\n"
	.size	.L.str.84, 40

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"CAB: Failed to extract file: %s\n"
	.size	.L.str.85, 33

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"in cli_scanhtml()\n"
	.size	.L.str.86, 19

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"cli_scanhtml: fstat() failed for descriptor %d\n"
	.size	.L.str.87, 48

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"cli_scanhtml: exiting (file larger than 10 MB)\n"
	.size	.L.str.88, 48

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"cli_scanhtml: Can't create temporary directory %s\n"
	.size	.L.str.89, 51

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"%s/comment.html"
	.size	.L.str.90, 16

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"%s/nocomment.html"
	.size	.L.str.91, 18

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"%s/script.html"
	.size	.L.str.92, 15

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"%s/rfc2397"
	.size	.L.str.93, 11

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"in cli_scanhtml_utf16()\n"
	.size	.L.str.94, 25

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"cli_scanhtml_utf16: Can't create file %s\n"
	.size	.L.str.95, 42

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"cli_scanhtml_utf16: Can't write to file %s\n"
	.size	.L.str.96, 44

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"cli_scanhtml_utf16: Decoded HTML data saved in %s\n"
	.size	.L.str.97, 51

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"Starting cli_scanmail(), mrec == %u, arec == %u\n"
	.size	.L.str.98, 49

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"Mail: Can't create temporary directory %s\n"
	.size	.L.str.99, 43

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"Can't create temporary directory for tnef file %s\n"
	.size	.L.str.100, 51

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"Can't create temporary directory for uuencoded file %s\n"
	.size	.L.str.101, 56

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"Can't create temporary directory for PST file %s\n"
	.size	.L.str.102, 50

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"in cli_scanmschm()\n"
	.size	.L.str.103, 20

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"CHM: Can't create temporary directory %s\n"
	.size	.L.str.104, 42

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"in cli_scanole2()\n"
	.size	.L.str.105, 19

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"OLE2: Can't create temporary directory %s\n"
	.size	.L.str.106, 43

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"OLE2: %s\n"
	.size	.L.str.107, 10

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"VBADir: %s\n"
	.size	.L.str.108, 12

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"VBADir: Can't open file %s\n"
	.size	.L.str.109, 28

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"VBADir: Decompress VBA project '%s'\n"
	.size	.L.str.110, 37

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"VBADir: WARNING: VBA project '%s' decompressed to NULL\n"
	.size	.L.str.111, 56

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"VBADir: Decompress WM project '%s' macro:%d key:%d length:%d\n"
	.size	.L.str.112, 62

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"VBADir: WARNING: WM project '%s' macro %d decrypted to NULL\n"
	.size	.L.str.113, 61

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"%s/_1_Ole10Native"
	.size	.L.str.114, 18

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"VBADir: Can't open directory %s.\n"
	.size	.L.str.115, 34

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"in cli_scantar()\n"
	.size	.L.str.116, 18

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"Tar: Can't create temporary directory %s\n"
	.size	.L.str.117, 42

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"Tar: %s\n"
	.size	.L.str.118, 9

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"in cli_scanbinhex()\n"
	.size	.L.str.119, 21

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"Binhex: Can't create temporary directory %s\n"
	.size	.L.str.120, 45

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"Binhex: %s\n"
	.size	.L.str.121, 12

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"in cli_scanscrenc()\n"
	.size	.L.str.122, 21

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"Exploit.W32.MS05-002"
	.size	.L.str.123, 21

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"Exploit.W32.MS04-028"
	.size	.L.str.124, 21

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"Can't create temporary directory for PDF file %s\n"
	.size	.L.str.125, 50

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"CryptFF: Can't fstat descriptor %d\n"
	.size	.L.str.126, 36

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"CryptFF: Can't lseek descriptor %d\n"
	.size	.L.str.127, 36

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"CryptFF: Can't allocate memory\n"
	.size	.L.str.128, 32

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"CryptFF: Can't read from descriptor %d\n"
	.size	.L.str.129, 40

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"CryptFF: Can't create file %s\n"
	.size	.L.str.130, 31

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"CryptFF: Can't write to descriptor %d\n"
	.size	.L.str.131, 39

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"CryptFF: Can't fsync descriptor %d\n"
	.size	.L.str.132, 36

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"CryptFF: Scanning decrypted data\n"
	.size	.L.str.133, 34

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"CryptFF: Infected with %s\n"
	.size	.L.str.134, 27

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"CryptFF: Decompressed data saved in %s\n"
	.size	.L.str.135, 40


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
