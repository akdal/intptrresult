	.text
	.file	"btMultiSapBroadphase.bc"
	.globl	_ZN20btMultiSapBroadphaseC2EiP22btOverlappingPairCache
	.p2align	4, 0x90
	.type	_ZN20btMultiSapBroadphaseC2EiP22btOverlappingPairCache,@function
_ZN20btMultiSapBroadphaseC2EiP22btOverlappingPairCache: # @_ZN20btMultiSapBroadphaseC2EiP22btOverlappingPairCache
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV20btMultiSapBroadphase+16, (%rbx)
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movl	$0, 12(%rbx)
	movl	$0, 16(%rbx)
	movq	%rdx, 48(%rbx)
	movq	$0, 56(%rbx)
	movb	$0, 64(%rbx)
	movl	$0, 80(%rbx)
	movb	$1, 112(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 92(%rbx)
	testq	%rdx, %rdx
	jne	.LBB0_4
# BB#1:
	movb	$1, 64(%rbx)
.Ltmp0:
	movl	$64, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r14
.Ltmp1:
# BB#2:
.Ltmp2:
	movq	%r14, %rdi
	callq	_ZN28btSortedOverlappingPairCacheC1Ev
.Ltmp3:
# BB#3:
	movq	%r14, 48(%rbx)
.LBB0_4:
.Ltmp5:
	movl	$8, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
.Ltmp6:
# BB#5:
	movq	$_ZTVZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback+16, (%rax)
	movq	%rax, 72(%rbx)
	movq	48(%rbx), %rdi
	movq	(%rdi), %rcx
.Ltmp8:
	movq	%rax, %rsi
	callq	*88(%rcx)
.Ltmp9:
# BB#6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_8:
.Ltmp4:
	jmp	.LBB0_10
.LBB0_9:
.Ltmp10:
	jmp	.LBB0_10
.LBB0_7:
.Ltmp7:
.LBB0_10:
	movq	%rax, %r14
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_14
# BB#11:
	cmpb	$0, 112(%rbx)
	je	.LBB0_13
# BB#12:
.Ltmp11:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp12:
.LBB0_13:                               # %.noexc15
	movq	$0, 104(%rbx)
.LBB0_14:
	movb	$1, 112(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 92(%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_18
# BB#15:
	cmpb	$0, 32(%rbx)
	je	.LBB0_17
# BB#16:
.Ltmp13:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp14:
.LBB0_17:                               # %.noexc
	movq	$0, 24(%rbx)
.LBB0_18:
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_19:
.Ltmp15:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN20btMultiSapBroadphaseC2EiP22btOverlappingPairCache, .Lfunc_end0-_ZN20btMultiSapBroadphaseC2EiP22btOverlappingPairCache
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp2           #   Call between .Ltmp2 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp14-.Ltmp11         #   Call between .Ltmp11 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	1                       #   On action: 1
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Lfunc_end0-.Ltmp14     #   Call between .Ltmp14 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN20btMultiSapBroadphaseD2Ev
	.p2align	4, 0x90
	.type	_ZN20btMultiSapBroadphaseD2Ev,@function
_ZN20btMultiSapBroadphaseD2Ev:          # @_ZN20btMultiSapBroadphaseD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV20btMultiSapBroadphase+16, (%rbx)
	cmpb	$0, 64(%rbx)
	je	.LBB2_3
# BB#1:
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp16:
	callq	*(%rax)
.Ltmp17:
# BB#2:
	movq	48(%rbx), %rdi
.Ltmp18:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp19:
.LBB2_3:
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_7
# BB#4:
	cmpb	$0, 112(%rbx)
	je	.LBB2_6
# BB#5:
.Ltmp23:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp24:
.LBB2_6:                                # %.noexc5
	movq	$0, 104(%rbx)
.LBB2_7:
	movb	$1, 112(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 92(%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_11
# BB#8:
	cmpb	$0, 32(%rbx)
	je	.LBB2_10
# BB#9:
.Ltmp29:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp30:
.LBB2_10:                               # %.noexc8
	movq	$0, 24(%rbx)
.LBB2_11:
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_17:
.Ltmp31:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_18:
.Ltmp25:
	movq	%rax, %r14
	jmp	.LBB2_19
.LBB2_12:
.Ltmp20:
	movq	%rax, %r14
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_16
# BB#13:
	cmpb	$0, 112(%rbx)
	je	.LBB2_15
# BB#14:
.Ltmp21:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp22:
.LBB2_15:                               # %.noexc
	movq	$0, 104(%rbx)
.LBB2_16:                               # %_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEED2Ev.exit
	movb	$1, 112(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 92(%rbx)
.LBB2_19:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_23
# BB#20:
	cmpb	$0, 32(%rbx)
	je	.LBB2_22
# BB#21:
.Ltmp26:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp27:
.LBB2_22:                               # %.noexc10
	movq	$0, 24(%rbx)
.LBB2_23:                               # %_ZN20btAlignedObjectArrayIP21btBroadphaseInterfaceED2Ev.exit11
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_25:
.Ltmp28:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN20btMultiSapBroadphaseD2Ev, .Lfunc_end2-_ZN20btMultiSapBroadphaseD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp19-.Ltmp16         #   Call between .Ltmp16 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin1   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin1   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin1   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp21-.Ltmp30         #   Call between .Ltmp30 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp27-.Ltmp21         #   Call between .Ltmp21 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin1   #     jumps to .Ltmp28
	.byte	1                       #   On action: 1
	.long	.Ltmp27-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Lfunc_end2-.Ltmp27     #   Call between .Ltmp27 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN20btMultiSapBroadphaseD0Ev
	.p2align	4, 0x90
	.type	_ZN20btMultiSapBroadphaseD0Ev,@function
_ZN20btMultiSapBroadphaseD0Ev:          # @_ZN20btMultiSapBroadphaseD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp32:
	callq	_ZN20btMultiSapBroadphaseD2Ev
.Ltmp33:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_2:
.Ltmp34:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN20btMultiSapBroadphaseD0Ev, .Lfunc_end3-_ZN20btMultiSapBroadphaseD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp32-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin2   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end3-.Ltmp33     #   Call between .Ltmp33 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN20btMultiSapBroadphase9buildTreeERK9btVector3S2_
	.p2align	4, 0x90
	.type	_ZN20btMultiSapBroadphase9buildTreeERK9btVector3S2_,@function
_ZN20btMultiSapBroadphase9buildTreeERK9btVector3S2_: # @_ZN20btMultiSapBroadphase9buildTreeERK9btVector3S2_
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 112
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbp
	movl	$248, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
.Ltmp35:
	movq	%rbx, %rdi
	callq	_ZN14btQuantizedBvhC1Ev
.Ltmp36:
# BB#1:
	movq	%rbx, 56(%rbp)
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f
	movq	56(%rbp), %r13
	cmpl	$0, 12(%rbp)
	movq	%rbp, %rbx
	jle	.LBB4_8
# BB#2:                                 # %.lr.ph
	xorl	%ebp, %ebp
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_14 Depth 2
                                        #     Child Loop BB4_17 Depth 2
	movq	24(%rbx), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	(%rdi), %rax
	leaq	40(%rsp), %rsi
	leaq	24(%rsp), %rdx
	callq	*80(%rax)
	movq	56(%rbx), %rax
	movss	40(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	44(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	subss	%xmm8, %xmm3
	movss	48(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm4
	movss	40(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	movss	44(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm3
	movss	48(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	cvttss2si	%xmm2, %edx
	andl	$65534, %edx            # imm = 0xFFFE
	cvttss2si	%xmm3, %esi
	andl	$65534, %esi            # imm = 0xFFFE
	cvttss2si	%xmm4, %edi
	andl	$65534, %edi            # imm = 0xFFFE
	movss	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	subss	%xmm8, %xmm3
	movss	32(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm1
	mulss	%xmm6, %xmm2
	mulss	%xmm7, %xmm3
	mulss	%xmm0, %xmm1
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm2
	cvttss2si	%xmm2, %r15d
	orl	$1, %r15d
	addss	%xmm0, %xmm3
	cvttss2si	%xmm3, %r12d
	orl	$1, %r12d
	addss	%xmm0, %xmm1
	cvttss2si	%xmm1, %r14d
	orl	$1, %r14d
	movl	140(%r13), %eax
	cmpl	144(%r13), %eax
	jne	.LBB4_23
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	leal	(%rax,%rax), %r9d
	testl	%eax, %eax
	movl	$1, %ecx
	cmovel	%ecx, %r9d
	cmpl	%r9d, %eax
	jge	.LBB4_23
# BB#5:                                 #   in Loop: Header=BB4_3 Depth=1
	movl	%edi, 4(%rsp)           # 4-byte Spill
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	%edx, 12(%rsp)          # 4-byte Spill
	testl	%r9d, %r9d
	je	.LBB4_6
# BB#9:                                 #   in Loop: Header=BB4_3 Depth=1
	movslq	%r9d, %rdi
	shlq	$4, %rdi
	movl	$16, %esi
	movl	%r9d, %ebx
	callq	_Z22btAlignedAllocInternalmi
	movl	%ebx, %r9d
	movq	%rax, %rbx
	movl	140(%r13), %eax
	testl	%eax, %eax
	jg	.LBB4_11
	jmp	.LBB4_18
.LBB4_6:                                #   in Loop: Header=BB4_3 Depth=1
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB4_18
.LBB4_11:                               # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB4_3 Depth=1
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB4_12
# BB#13:                                # %.prol.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_14:                               #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	152(%r13), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%rbx,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB4_14
	jmp	.LBB4_15
.LBB4_12:                               #   in Loop: Header=BB4_3 Depth=1
	xorl	%ecx, %ecx
.LBB4_15:                               # %.prol.loopexit
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpq	$3, %r8
	jb	.LBB4_18
# BB#16:                                # %.lr.ph.i.i.i.new
                                        #   in Loop: Header=BB4_3 Depth=1
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB4_17:                               #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	152(%r13), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbx,%rcx)
	movq	152(%r13), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbx,%rcx)
	movq	152(%r13), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbx,%rcx)
	movq	152(%r13), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbx,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB4_17
.LBB4_18:                               # %_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_.exit.i.i
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB4_22
# BB#19:                                #   in Loop: Header=BB4_3 Depth=1
	cmpb	$0, 160(%r13)
	je	.LBB4_21
# BB#20:                                #   in Loop: Header=BB4_3 Depth=1
	movl	%r9d, (%rsp)            # 4-byte Spill
	callq	_Z21btAlignedFreeInternalPv
	movl	(%rsp), %r9d            # 4-byte Reload
.LBB4_21:                               #   in Loop: Header=BB4_3 Depth=1
	movq	$0, 152(%r13)
.LBB4_22:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv.exit.i.i
                                        #   in Loop: Header=BB4_3 Depth=1
	movb	$1, 160(%r13)
	movq	%rbx, 152(%r13)
	movl	%r9d, 144(%r13)
	movl	140(%r13), %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	4(%rsp), %edi           # 4-byte Reload
.LBB4_23:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9push_backERKS0_.exit
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	152(%r13), %rcx
	cltq
	shlq	$4, %rax
	movw	%dx, (%rcx,%rax)
	movw	%si, 2(%rcx,%rax)
	movw	%di, 4(%rcx,%rax)
	movw	%r15w, 6(%rcx,%rax)
	movw	%r12w, 8(%rcx,%rax)
	movw	%r14w, 10(%rcx,%rax)
	movl	%ebp, 12(%rcx,%rax)
	incl	140(%r13)
	incq	%rbp
	movslq	12(%rbx), %rax
	cmpq	%rax, %rbp
	jl	.LBB4_3
# BB#7:                                 # %._crit_edge.loopexit
	movq	56(%rbx), %r13
.LBB4_8:                                # %._crit_edge
	movq	%r13, %rdi
	callq	_ZN14btQuantizedBvh13buildInternalEv
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_24:
.Ltmp37:
	movq	%rax, %r14
.Ltmp38:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp39:
# BB#25:                                # %_ZN14btQuantizedBvhdlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_26:
.Ltmp40:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN20btMultiSapBroadphase9buildTreeERK9btVector3S2_, .Lfunc_end4-_ZN20btMultiSapBroadphase9buildTreeERK9btVector3S2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp35-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin3   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp38-.Ltmp36         #   Call between .Ltmp36 and .Ltmp38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin3   #     jumps to .Ltmp40
	.byte	1                       #   On action: 1
	.long	.Ltmp39-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Lfunc_end4-.Ltmp39     #   Call between .Ltmp39 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN20btMultiSapBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_
	.p2align	4, 0x90
	.type	_ZN20btMultiSapBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_,@function
_ZN20btMultiSapBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_: # @_ZN20btMultiSapBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 64
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movq	%r8, %r15
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$136, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r13
	movq	%r15, (%r13)
	movq	%r12, %r15
	movq	%r14, %r12
	movw	%bp, 8(%r13)
	movzwl	64(%rsp), %eax
	movw	%ax, 10(%r13)
	movups	(%r12), %xmm0
	movups	%xmm0, 28(%r13)
	movups	(%r15), %xmm0
	movups	%xmm0, 44(%r13)
	movq	$0, 16(%r13)
	movb	$1, 88(%r13)
	movq	$0, 80(%r13)
	movq	$0, 68(%r13)
	movups	(%r12), %xmm0
	movups	%xmm0, 96(%r13)
	movups	(%r15), %xmm0
	movups	%xmm0, 112(%r13)
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 128(%r13)
	movq	%r13, 16(%r13)
	movl	92(%rbx), %eax
	cmpl	96(%rbx), %eax
	jne	.LBB5_15
# BB#1:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %r14d
	cmovnel	%ecx, %r14d
	cmpl	%r14d, %eax
	jge	.LBB5_15
# BB#2:
	testl	%r14d, %r14d
	je	.LBB5_3
# BB#4:
	movslq	%r14d, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
	movl	92(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB5_6
	jmp	.LBB5_10
.LBB5_3:
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jle	.LBB5_10
.LBB5_6:                                # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB5_8
	.p2align	4, 0x90
.LBB5_7:                                # =>This Inner Loop Header: Depth=1
	movq	104(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbp,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB5_7
.LBB5_8:                                # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB5_10
	.p2align	4, 0x90
.LBB5_9:                                # =>This Inner Loop Header: Depth=1
	movq	104(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbp,%rdx,8)
	movq	104(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%rbp,%rdx,8)
	movq	104(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%rbp,%rdx,8)
	movq	104(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%rbp,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB5_9
.LBB5_10:                               # %_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE4copyEiiPS2_.exit.i.i
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_14
# BB#11:
	cmpb	$0, 112(%rbx)
	je	.LBB5_13
# BB#12:
	callq	_Z21btAlignedFreeInternalPv
	movl	92(%rbx), %eax
.LBB5_13:
	movq	$0, 104(%rbx)
.LBB5_14:                               # %_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE10deallocateEv.exit.i.i
	movb	$1, 112(%rbx)
	movq	%rbp, 104(%rbx)
	movl	%r14d, 96(%rbx)
.LBB5_15:                               # %_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase15btMultiSapProxyEE9push_backERKS2_.exit
	movq	104(%rbx), %rcx
	cltq
	movq	%r13, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 92(%rbx)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	72(%rsp), %r8
	callq	*32(%rax)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN20btMultiSapBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_, .Lfunc_end5-_ZN20btMultiSapBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_
	.cfi_endproc

	.globl	_ZN20btMultiSapBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btMultiSapBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher,@function
_ZN20btMultiSapBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher: # @_ZN20btMultiSapBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end6:
	.size	_ZN20btMultiSapBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher, .Lfunc_end6-_ZN20btMultiSapBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.cfi_endproc

	.globl	_ZN20btMultiSapBroadphase20addToChildBroadphaseEPNS_15btMultiSapProxyEP17btBroadphaseProxyP21btBroadphaseInterface
	.p2align	4, 0x90
	.type	_ZN20btMultiSapBroadphase20addToChildBroadphaseEPNS_15btMultiSapProxyEP17btBroadphaseProxyP21btBroadphaseInterface,@function
_ZN20btMultiSapBroadphase20addToChildBroadphaseEPNS_15btMultiSapProxyEP17btBroadphaseProxyP21btBroadphaseInterface: # @_ZN20btMultiSapBroadphase20addToChildBroadphaseEPNS_15btMultiSapProxyEP17btBroadphaseProxyP21btBroadphaseInterface
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 48
.Lcfi46:
	.cfi_offset %rbx, -48
.Lcfi47:
	.cfi_offset %r12, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r14
	movq	%rbx, (%r14)
	movq	%r12, 8(%r14)
	movl	68(%r15), %eax
	cmpl	72(%r15), %eax
	jne	.LBB7_15
# BB#1:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB7_15
# BB#2:
	testl	%ebp, %ebp
	je	.LBB7_3
# BB#4:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	68(%r15), %eax
	testl	%eax, %eax
	jg	.LBB7_6
	jmp	.LBB7_10
.LBB7_3:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB7_10
.LBB7_6:                                # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB7_8
	.p2align	4, 0x90
.LBB7_7:                                # =>This Inner Loop Header: Depth=1
	movq	80(%r15), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB7_7
.LBB7_8:                                # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB7_10
	.p2align	4, 0x90
.LBB7_9:                                # =>This Inner Loop Header: Depth=1
	movq	80(%r15), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	movq	80(%r15), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%rbx,%rdx,8)
	movq	80(%r15), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%rbx,%rdx,8)
	movq	80(%r15), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%rbx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB7_9
.LBB7_10:                               # %_ZNK20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE4copyEiiPS2_.exit.i.i
	movq	80(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB7_14
# BB#11:
	cmpb	$0, 88(%r15)
	je	.LBB7_13
# BB#12:
	callq	_Z21btAlignedFreeInternalPv
	movl	68(%r15), %eax
.LBB7_13:
	movq	$0, 80(%r15)
.LBB7_14:                               # %_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE10deallocateEv.exit.i.i
	movb	$1, 88(%r15)
	movq	%rbx, 80(%r15)
	movl	%ebp, 72(%r15)
.LBB7_15:                               # %_ZN20btAlignedObjectArrayIPN20btMultiSapBroadphase13btBridgeProxyEE9push_backERKS2_.exit
	movq	80(%r15), %rcx
	cltq
	movq	%r14, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 68(%r15)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN20btMultiSapBroadphase20addToChildBroadphaseEPNS_15btMultiSapProxyEP17btBroadphaseProxyP21btBroadphaseInterface, .Lfunc_end7-_ZN20btMultiSapBroadphase20addToChildBroadphaseEPNS_15btMultiSapProxyEP17btBroadphaseProxyP21btBroadphaseInterface
	.cfi_endproc

	.globl	_Z23boxIsContainedWithinBoxRK9btVector3S1_S1_S1_
	.p2align	4, 0x90
	.type	_Z23boxIsContainedWithinBoxRK9btVector3S1_S1_S1_,@function
_Z23boxIsContainedWithinBoxRK9btVector3S1_S1_S1_: # @_Z23boxIsContainedWithinBoxRK9btVector3S1_S1_S1_
	.cfi_startproc
# BB#0:
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%rdx), %xmm0
	jae	.LBB8_2
# BB#1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB8_2:
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%rsi), %xmm0
	jae	.LBB8_4
# BB#3:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB8_4:
	movss	4(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%rdx), %xmm0
	jae	.LBB8_6
# BB#5:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB8_6:
	movss	4(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%rsi), %xmm0
	jae	.LBB8_8
# BB#7:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB8_8:
	movss	8(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%rdx), %xmm0
	jae	.LBB8_10
# BB#9:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB8_10:
	movss	8(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%rsi), %xmm0
	setae	%al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end8:
	.size	_Z23boxIsContainedWithinBoxRK9btVector3S1_S1_S1_, .Lfunc_end8-_Z23boxIsContainedWithinBoxRK9btVector3S1_S1_S1_
	.cfi_endproc

	.globl	_ZNK20btMultiSapBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_
	.p2align	4, 0x90
	.type	_ZNK20btMultiSapBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_,@function
_ZNK20btMultiSapBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_: # @_ZNK20btMultiSapBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_
	.cfi_startproc
# BB#0:
	movups	96(%rsi), %xmm0
	movups	%xmm0, (%rdx)
	movups	112(%rsi), %xmm0
	movups	%xmm0, (%rcx)
	retq
.Lfunc_end9:
	.size	_ZNK20btMultiSapBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_, .Lfunc_end9-_ZNK20btMultiSapBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_
	.cfi_endproc

	.globl	_ZN20btMultiSapBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_
	.p2align	4, 0x90
	.type	_ZN20btMultiSapBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_,@function
_ZN20btMultiSapBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_: # @_ZN20btMultiSapBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 32
.Lcfi54:
	.cfi_offset %rbx, -32
.Lcfi55:
	.cfi_offset %r14, -24
.Lcfi56:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdi, %r15
	cmpl	$0, 92(%r15)
	jle	.LBB10_3
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	104(%r15), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movq	%r14, %rdi
	callq	*16(%rax)
	incq	%rbx
	movslq	92(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB10_2
.LBB10_3:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	_ZN20btMultiSapBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_, .Lfunc_end10-_ZN20btMultiSapBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_
	.cfi_endproc

	.globl	_ZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher,@function
_ZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher: # @_ZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 144
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rcx, %r14
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	movups	(%rbp), %xmm0
	movups	%xmm0, 96(%rbx)
	movups	(%r14), %xmm0
	movups	%xmm0, 112(%rbx)
	movq	$_ZTVZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback+16, 56(%rsp)
	movq	%rdi, 64(%rsp)
	movq	%rbx, 72(%rsp)
	movq	%r12, 80(%rsp)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB11_2
# BB#1:
.Ltmp41:
	leaq	56(%rsp), %rsi
	movq	%rbp, %rdx
	movq	%r14, %rcx
	callq	_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_
.Ltmp42:
.LBB11_2:                               # %.preheader59
	cmpl	$0, 68(%rbx)
	jle	.LBB11_11
# BB#3:                                 # %.lr.ph68
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	leaq	96(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	112(%rbx), %r15
	xorl	%r14d, %r14d
	leaq	40(%rsp), %r13
	leaq	24(%rsp), %rbp
	.p2align	4, 0x90
.LBB11_4:                               # =>This Inner Loop Header: Depth=1
	movq	80(%rbx), %rax
	movq	(%rax,%r14,8), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
.Ltmp44:
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	*80(%rax)
.Ltmp45:
# BB#5:                                 #   in Loop: Header=BB11_4 Depth=1
	movss	40(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%r15), %xmm0
	jbe	.LBB11_12
# BB#6:                                 #   in Loop: Header=BB11_4 Depth=1
	xorl	%eax, %eax
	jmp	.LBB11_15
	.p2align	4, 0x90
.LBB11_12:                              #   in Loop: Header=BB11_4 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	24(%rsp), %xmm0
	jbe	.LBB11_14
# BB#13:                                #   in Loop: Header=BB11_4 Depth=1
	xorl	%eax, %eax
	jmp	.LBB11_15
	.p2align	4, 0x90
.LBB11_14:                              #   in Loop: Header=BB11_4 Depth=1
	movb	$1, %al
.LBB11_15:                              #   in Loop: Header=BB11_4 Depth=1
	movss	48(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	120(%rbx), %xmm0
	ja	.LBB11_17
# BB#16:                                #   in Loop: Header=BB11_4 Depth=1
	movss	104(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	32(%rsp), %xmm0
	jbe	.LBB11_18
.LBB11_17:                              #   in Loop: Header=BB11_4 Depth=1
	xorl	%eax, %eax
.LBB11_18:                              #   in Loop: Header=BB11_4 Depth=1
	movss	44(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	116(%rbx), %xmm0
	ja	.LBB11_22
# BB#19:                                #   in Loop: Header=BB11_4 Depth=1
	movss	100(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	28(%rsp), %xmm0
	ja	.LBB11_22
# BB#20:                                #   in Loop: Header=BB11_4 Depth=1
	xorb	$1, %al
	jne	.LBB11_22
# BB#21:                                # %._crit_edge71
                                        #   in Loop: Header=BB11_4 Depth=1
	movl	68(%rbx), %ecx
	jmp	.LBB11_24
	.p2align	4, 0x90
.LBB11_22:                              # %_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_.exit.thread
                                        #   in Loop: Header=BB11_4 Depth=1
	movq	80(%rbx), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
.Ltmp47:
	movq	%r12, %rdx
	callq	*24(%rax)
.Ltmp48:
# BB#23:                                #   in Loop: Header=BB11_4 Depth=1
	movslq	68(%rbx), %rax
	leaq	-1(%rax), %rcx
	movq	80(%rbx), %rdx
	movq	(%rdx,%r14,8), %rsi
	movq	-8(%rdx,%rax,8), %rdi
	movq	%rdi, (%rdx,%r14,8)
	movq	80(%rbx), %rdx
	movq	%rsi, -8(%rdx,%rax,8)
	movl	%ecx, 68(%rbx)
.LBB11_24:                              #   in Loop: Header=BB11_4 Depth=1
	incq	%r14
	movslq	%ecx, %rax
	cmpq	%rax, %r14
	jl	.LBB11_4
# BB#7:                                 # %.preheader
	testl	%ecx, %ecx
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	(%rsp), %r15            # 8-byte Reload
	jle	.LBB11_11
# BB#8:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_9:                               # =>This Inner Loop Header: Depth=1
	movq	80(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
.Ltmp50:
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	%r12, %r8
	callq	*32(%rax)
.Ltmp51:
# BB#10:                                #   in Loop: Header=BB11_9 Depth=1
	incq	%rbp
	movslq	68(%rbx), %rax
	cmpq	%rax, %rbp
	jl	.LBB11_9
.LBB11_11:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_27:
.Ltmp43:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB11_26:
.Ltmp52:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB11_29:
.Ltmp49:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB11_25:
.Ltmp46:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher, .Lfunc_end11-_ZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp41-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin4   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin4   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin4   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin4   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Lfunc_end11-.Ltmp51    #   Call between .Ltmp51 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.zero	16
	.text
	.globl	_ZN20btMultiSapBroadphase25calculateOverlappingPairsEP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btMultiSapBroadphase25calculateOverlappingPairsEP12btDispatcher,@function
_ZN20btMultiSapBroadphase25calculateOverlappingPairsEP12btDispatcher: # @_ZN20btMultiSapBroadphase25calculateOverlappingPairsEP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 128
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	cmpb	$0, stopUpdating(%rip)
	jne	.LBB12_38
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*112(%rcx)
	testb	%al, %al
	je	.LBB12_38
# BB#2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*56(%rcx)
	movq	%rax, %r15
	movl	4(%r15), %eax
	cmpl	$2, %eax
	jl	.LBB12_4
# BB#3:
	decl	%eax
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%eax, %edx
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvT_ii
	movl	4(%r15), %eax
.LBB12_4:                               # %_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI37btMultiSapBroadphasePairSortPredicateEEvT_.exit
	subl	80(%rbx), %eax
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	leaq	32(%rsp), %rdx
	movq	%r15, %rdi
	movl	%eax, %esi
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	movl	$0, 80(%rbx)
	movl	4(%r15), %r8d
	testl	%r8d, %r8d
	jle	.LBB12_5
# BB#6:                                 # %.lr.ph
	movq	%rbx, (%rsp)            # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r15, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB12_7:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %r9
	movq	(%r9,%r13), %r12
	testq	%r12, %r12
	je	.LBB12_8
# BB#9:                                 #   in Loop: Header=BB12_7 Depth=1
	movq	16(%r12), %rdi
	jmp	.LBB12_10
	.p2align	4, 0x90
.LBB12_8:                               #   in Loop: Header=BB12_7 Depth=1
	xorl	%edi, %edi
.LBB12_10:                              #   in Loop: Header=BB12_7 Depth=1
	movq	8(%r9,%r13), %rbp
	testq	%rbp, %rbp
	je	.LBB12_11
# BB#12:                                #   in Loop: Header=BB12_7 Depth=1
	movq	16(%rbp), %rbx
	testq	%rsi, %rsi
	jne	.LBB12_15
	jmp	.LBB12_14
	.p2align	4, 0x90
.LBB12_11:                              #   in Loop: Header=BB12_7 Depth=1
	xorl	%ebx, %ebx
	testq	%rsi, %rsi
	je	.LBB12_14
.LBB12_15:                              #   in Loop: Header=BB12_7 Depth=1
	movq	16(%rsi), %rsi
	testq	%rdx, %rdx
	jne	.LBB12_18
	jmp	.LBB12_17
	.p2align	4, 0x90
.LBB12_14:                              #   in Loop: Header=BB12_7 Depth=1
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.LBB12_17
.LBB12_18:                              #   in Loop: Header=BB12_7 Depth=1
	movq	16(%rdx), %rdx
	cmpq	%rsi, %rdi
	je	.LBB12_20
	jmp	.LBB12_21
	.p2align	4, 0x90
.LBB12_17:                              #   in Loop: Header=BB12_7 Depth=1
	xorl	%edx, %edx
	cmpq	%rsi, %rdi
	jne	.LBB12_21
.LBB12_20:                              #   in Loop: Header=BB12_7 Depth=1
	cmpq	%rdx, %rbx
	je	.LBB12_32
.LBB12_21:                              #   in Loop: Header=BB12_7 Depth=1
	movq	16(%r12), %rdx
	movq	16(%rbp), %rsi
	movss	96(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	112(%rsi), %xmm0
	jbe	.LBB12_23
# BB#22:                                #   in Loop: Header=BB12_7 Depth=1
	xorl	%edi, %edi
	jmp	.LBB12_26
	.p2align	4, 0x90
.LBB12_23:                              #   in Loop: Header=BB12_7 Depth=1
	movss	96(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	112(%rdx), %xmm0
	jbe	.LBB12_25
# BB#24:                                #   in Loop: Header=BB12_7 Depth=1
	xorl	%edi, %edi
	jmp	.LBB12_26
.LBB12_25:                              #   in Loop: Header=BB12_7 Depth=1
	movb	$1, %dil
.LBB12_26:                              #   in Loop: Header=BB12_7 Depth=1
	movss	104(%rdx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	120(%rsi), %xmm0
	ja	.LBB12_28
# BB#27:                                #   in Loop: Header=BB12_7 Depth=1
	movss	104(%rsi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	120(%rdx), %xmm0
	jbe	.LBB12_29
.LBB12_28:                              #   in Loop: Header=BB12_7 Depth=1
	xorl	%edi, %edi
.LBB12_29:                              #   in Loop: Header=BB12_7 Depth=1
	movss	100(%rdx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	116(%rsi), %xmm0
	ja	.LBB12_32
# BB#30:                                #   in Loop: Header=BB12_7 Depth=1
	movss	100(%rsi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	116(%rdx), %xmm0
	ja	.LBB12_32
# BB#31:                                #   in Loop: Header=BB12_7 Depth=1
	xorb	$1, %dil
	je	.LBB12_33
	.p2align	4, 0x90
.LBB12_32:                              # %_ZN20btMultiSapBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_.exit.thread
                                        #   in Loop: Header=BB12_7 Depth=1
	leaq	(%r9,%r13), %rbx
	movq	(%rsp), %r15            # 8-byte Reload
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r9, 24(%rsp)           # 8-byte Spill
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	*64(%rcx)
	xorps	%xmm0, %xmm0
	movq	24(%rsp), %rax          # 8-byte Reload
	movups	%xmm0, (%rax,%r13)
	movl	80(%r15), %eax
	incl	%eax
	movl	%eax, 80(%r15)
	movq	8(%rsp), %r15           # 8-byte Reload
	decl	gOverlappingPairs(%rip)
	movl	4(%r15), %r8d
.LBB12_33:                              # %.thread51
                                        #   in Loop: Header=BB12_7 Depth=1
	incq	%r14
	movslq	%r8d, %rdx
	addq	$32, %r13
	cmpq	%rdx, %r14
	movq	%r12, %rsi
	movq	%rbp, %rdx
	jl	.LBB12_7
# BB#34:                                # %._crit_edge
	cmpl	$2, %r8d
	jl	.LBB12_35
# BB#36:
	decl	%r8d
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%r8d, %edx
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvT_ii
	movl	4(%r15), %r8d
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	80(%rbx), %eax
	jmp	.LBB12_37
.LBB12_5:
	xorl	%eax, %eax
	jmp	.LBB12_37
.LBB12_35:
	movq	(%rsp), %rbx            # 8-byte Reload
.LBB12_37:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI37btMultiSapBroadphasePairSortPredicateEEvT_.exit43
	subl	%eax, %r8d
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	leaq	32(%rsp), %rdx
	movq	%r15, %rdi
	movl	%r8d, %esi
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	movl	$0, 80(%rbx)
.LBB12_38:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN20btMultiSapBroadphase25calculateOverlappingPairsEP12btDispatcher, .Lfunc_end12-_ZN20btMultiSapBroadphase25calculateOverlappingPairsEP12btDispatcher
	.cfi_endproc

	.section	.text._ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_,"axG",@progbits,_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_,comdat
	.weak	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_,@function
_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_: # @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 48
.Lcfi88:
	.cfi_offset %rbx, -48
.Lcfi89:
	.cfi_offset %r12, -40
.Lcfi90:
	.cfi_offset %r14, -32
.Lcfi91:
	.cfi_offset %r15, -24
.Lcfi92:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %r14d
	movq	%rdi, %r15
	movl	4(%r15), %r12d
	cmpl	%r14d, %r12d
	jg	.LBB13_19
# BB#1:
	jge	.LBB13_19
# BB#2:
	cmpl	%r14d, 8(%r15)
	jge	.LBB13_14
# BB#3:
	testl	%r14d, %r14d
	je	.LBB13_4
# BB#5:
	movslq	%r14d, %rdi
	shlq	$5, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
	movl	4(%r15), %eax
	testl	%eax, %eax
	jg	.LBB13_7
	jmp	.LBB13_9
.LBB13_4:
	xorl	%ebp, %ebp
	movl	%r12d, %eax
	testl	%eax, %eax
	jle	.LBB13_9
.LBB13_7:                               # %.lr.ph.i.i
	cltq
	movl	$24, %ecx
	.p2align	4, 0x90
.LBB13_8:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rdx
	movups	-24(%rdx,%rcx), %xmm0
	movups	%xmm0, -24(%rbp,%rcx)
	movq	-8(%rdx,%rcx), %rsi
	movq	%rsi, -8(%rbp,%rcx)
	movq	(%rdx,%rcx), %rdx
	movq	%rdx, (%rbp,%rcx)
	addq	$32, %rcx
	decq	%rax
	jne	.LBB13_8
.LBB13_9:                               # %_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_.exit.i
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB13_13
# BB#10:
	cmpb	$0, 24(%r15)
	je	.LBB13_12
# BB#11:
	callq	_Z21btAlignedFreeInternalPv
.LBB13_12:
	movq	$0, 16(%r15)
.LBB13_13:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit.preheader
	movb	$1, 24(%r15)
	movq	%rbp, 16(%r15)
	movl	%r14d, 8(%r15)
	cmpl	%r14d, %r12d
	jge	.LBB13_19
.LBB13_14:                              # %.lr.ph
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	movl	%r14d, %ecx
	subl	%r12d, %ecx
	leaq	-1(%rax), %rsi
	testb	$1, %cl
	movq	%rdx, %rcx
	je	.LBB13_16
# BB#15:                                # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit.prol
	movq	16(%r15), %rcx
	movq	%rdx, %rdi
	shlq	$5, %rdi
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rcx,%rdi)
	movq	16(%rbx), %rbp
	movq	%rbp, 16(%rcx,%rdi)
	movq	24(%rbx), %rbp
	movq	%rbp, 24(%rcx,%rdi)
	leaq	1(%rdx), %rcx
.LBB13_16:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit.prol.loopexit
	cmpq	%rdx, %rsi
	je	.LBB13_19
# BB#17:                                # %.lr.ph.new
	subq	%rcx, %rax
	shlq	$5, %rcx
	.p2align	4, 0x90
.LBB13_18:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rdx
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rdx,%rcx)
	movq	16(%rbx), %rsi
	movq	%rsi, 16(%rdx,%rcx)
	movq	24(%rbx), %rsi
	movq	%rsi, 24(%rdx,%rcx)
	movq	16(%r15), %rdx
	movups	(%rbx), %xmm0
	movups	%xmm0, 32(%rdx,%rcx)
	movq	16(%rbx), %rsi
	movq	%rsi, 48(%rdx,%rcx)
	movq	24(%rbx), %rsi
	movq	%rsi, 56(%rdx,%rcx)
	addq	$64, %rcx
	addq	$-2, %rax
	jne	.LBB13_18
.LBB13_19:                              # %.loopexit
	movl	%r14d, 4(%r15)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_, .Lfunc_end13-_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	.cfi_endproc

	.text
	.globl	_ZN20btMultiSapBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_
	.p2align	4, 0x90
	.type	_ZN20btMultiSapBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_,@function
_ZN20btMultiSapBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_: # @_ZN20btMultiSapBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_
	.cfi_startproc
# BB#0:
	movq	16(%rsi), %rcx
	movq	16(%rdx), %rdx
	movss	96(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	112(%rdx), %xmm0
	jbe	.LBB14_2
# BB#1:
	xorl	%eax, %eax
	jmp	.LBB14_5
.LBB14_2:
	movss	96(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	112(%rcx), %xmm0
	jbe	.LBB14_4
# BB#3:
	xorl	%eax, %eax
	jmp	.LBB14_5
.LBB14_4:
	movb	$1, %al
.LBB14_5:
	movss	104(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	120(%rdx), %xmm0
	ja	.LBB14_7
# BB#6:
	movss	104(%rdx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	120(%rcx), %xmm0
	jbe	.LBB14_8
.LBB14_7:
	xorl	%eax, %eax
.LBB14_8:
	movss	100(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	116(%rdx), %xmm0
	ja	.LBB14_10
# BB#9:
	movss	100(%rdx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	116(%rcx), %xmm0
	jbe	.LBB14_11
.LBB14_10:
	xorl	%eax, %eax
.LBB14_11:                              # %_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end14:
	.size	_ZN20btMultiSapBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_, .Lfunc_end14-_ZN20btMultiSapBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_
	.cfi_endproc

	.globl	_ZN20btMultiSapBroadphase10printStatsEv
	.p2align	4, 0x90
	.type	_ZN20btMultiSapBroadphase10printStatsEv,@function
_ZN20btMultiSapBroadphase10printStatsEv: # @_ZN20btMultiSapBroadphase10printStatsEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end15:
	.size	_ZN20btMultiSapBroadphase10printStatsEv, .Lfunc_end15-_ZN20btMultiSapBroadphase10printStatsEv
	.cfi_endproc

	.globl	_ZN20btMultiSapBroadphase9resetPoolEP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btMultiSapBroadphase9resetPoolEP12btDispatcher,@function
_ZN20btMultiSapBroadphase9resetPoolEP12btDispatcher: # @_ZN20btMultiSapBroadphase9resetPoolEP12btDispatcher
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end16:
	.size	_ZN20btMultiSapBroadphase9resetPoolEP12btDispatcher, .Lfunc_end16-_ZN20btMultiSapBroadphase9resetPoolEP12btDispatcher
	.cfi_endproc

	.section	.text._ZN20btMultiSapBroadphase23getOverlappingPairCacheEv,"axG",@progbits,_ZN20btMultiSapBroadphase23getOverlappingPairCacheEv,comdat
	.weak	_ZN20btMultiSapBroadphase23getOverlappingPairCacheEv
	.p2align	4, 0x90
	.type	_ZN20btMultiSapBroadphase23getOverlappingPairCacheEv,@function
_ZN20btMultiSapBroadphase23getOverlappingPairCacheEv: # @_ZN20btMultiSapBroadphase23getOverlappingPairCacheEv
	.cfi_startproc
# BB#0:
	movq	48(%rdi), %rax
	retq
.Lfunc_end17:
	.size	_ZN20btMultiSapBroadphase23getOverlappingPairCacheEv, .Lfunc_end17-_ZN20btMultiSapBroadphase23getOverlappingPairCacheEv
	.cfi_endproc

	.section	.text._ZNK20btMultiSapBroadphase23getOverlappingPairCacheEv,"axG",@progbits,_ZNK20btMultiSapBroadphase23getOverlappingPairCacheEv,comdat
	.weak	_ZNK20btMultiSapBroadphase23getOverlappingPairCacheEv
	.p2align	4, 0x90
	.type	_ZNK20btMultiSapBroadphase23getOverlappingPairCacheEv,@function
_ZNK20btMultiSapBroadphase23getOverlappingPairCacheEv: # @_ZNK20btMultiSapBroadphase23getOverlappingPairCacheEv
	.cfi_startproc
# BB#0:
	movq	48(%rdi), %rax
	retq
.Lfunc_end18:
	.size	_ZNK20btMultiSapBroadphase23getOverlappingPairCacheEv, .Lfunc_end18-_ZNK20btMultiSapBroadphase23getOverlappingPairCacheEv
	.cfi_endproc

	.section	.text._ZNK20btMultiSapBroadphase17getBroadphaseAabbER9btVector3S1_,"axG",@progbits,_ZNK20btMultiSapBroadphase17getBroadphaseAabbER9btVector3S1_,comdat
	.weak	_ZNK20btMultiSapBroadphase17getBroadphaseAabbER9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK20btMultiSapBroadphase17getBroadphaseAabbER9btVector3S1_,@function
_ZNK20btMultiSapBroadphase17getBroadphaseAabbER9btVector3S1_: # @_ZNK20btMultiSapBroadphase17getBroadphaseAabbER9btVector3S1_
	.cfi_startproc
# BB#0:
	movl	$-581039253, (%rsi)     # imm = 0xDD5E0B6B
	movl	$-581039253, 4(%rsi)    # imm = 0xDD5E0B6B
	movl	$-581039253, 8(%rsi)    # imm = 0xDD5E0B6B
	movl	$0, 12(%rsi)
	movabsq	$6727827449093950315, %rax # imm = 0x5D5E0B6B5D5E0B6B
	movq	%rax, (%rdx)
	movq	$1566444395, 8(%rdx)    # imm = 0x5D5E0B6B
	retq
.Lfunc_end19:
	.size	_ZNK20btMultiSapBroadphase17getBroadphaseAabbER9btVector3S1_, .Lfunc_end19-_ZNK20btMultiSapBroadphase17getBroadphaseAabbER9btVector3S1_
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheEN31btMultiSapOverlapFilterCallbackD0Ev,@function
_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheEN31btMultiSapOverlapFilterCallbackD0Ev: # @_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheEN31btMultiSapOverlapFilterCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end20:
	.size	_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheEN31btMultiSapOverlapFilterCallbackD0Ev, .Lfunc_end20-_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheEN31btMultiSapOverlapFilterCallbackD0Ev
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheENK31btMultiSapOverlapFilterCallback23needBroadphaseCollisionEP17btBroadphaseProxyS4_,@function
_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheENK31btMultiSapOverlapFilterCallback23needBroadphaseCollisionEP17btBroadphaseProxyS4_: # @_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheENK31btMultiSapOverlapFilterCallback23needBroadphaseCollisionEP17btBroadphaseProxyS4_
	.cfi_startproc
# BB#0:
	movq	16(%rsi), %rax
	movq	16(%rdx), %rcx
	movzwl	10(%rcx), %edx
	testw	8(%rax), %dx
	je	.LBB21_1
# BB#2:
	movzwl	10(%rax), %eax
	testw	8(%rcx), %ax
	setne	%al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB21_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end21:
	.size	_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheENK31btMultiSapOverlapFilterCallback23needBroadphaseCollisionEP17btBroadphaseProxyS4_, .Lfunc_end21-_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheENK31btMultiSapOverlapFilterCallback23needBroadphaseCollisionEP17btBroadphaseProxyS4_
	.cfi_endproc

	.section	.text._ZN23btOverlapFilterCallbackD2Ev,"axG",@progbits,_ZN23btOverlapFilterCallbackD2Ev,comdat
	.weak	_ZN23btOverlapFilterCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN23btOverlapFilterCallbackD2Ev,@function
_ZN23btOverlapFilterCallbackD2Ev:       # @_ZN23btOverlapFilterCallbackD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end22:
	.size	_ZN23btOverlapFilterCallbackD2Ev, .Lfunc_end22-_ZN23btOverlapFilterCallbackD2Ev
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallbackD0Ev,@function
_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallbackD0Ev: # @_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end23:
	.size	_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallbackD0Ev, .Lfunc_end23-_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallbackD0Ev
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallback11processNodeEii,@function
_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallback11processNodeEii: # @_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallback11processNodeEii
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi95:
	.cfi_def_cfa_offset 32
.Lcfi96:
	.cfi_offset %rbx, -24
.Lcfi97:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	8(%r14), %rcx
	movq	16(%r14), %rax
	movq	24(%rcx), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rbx
	movslq	68(%rax), %rcx
	testq	%rcx, %rcx
	jle	.LBB24_4
# BB#1:                                 # %.lr.ph.preheader
	movq	80(%rax), %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB24_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rdx,8), %rdi
	cmpq	%rbx, 8(%rdi)
	je	.LBB24_5
# BB#3:                                 #   in Loop: Header=BB24_2 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB24_2
	jmp	.LBB24_4
.LBB24_5:
	testl	%edx, %edx
	js	.LBB24_4
# BB#6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB24_4:                               # %.thread
	movq	(%rbx), %r10
	leaq	96(%rax), %rsi
	leaq	112(%rax), %rdx
	movl	128(%rax), %ecx
	movq	(%rax), %r8
	movswl	8(%rax), %r9d
	movswl	10(%rax), %r11d
	subq	$8, %rsp
.Lcfi98:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	pushq	%rax
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	pushq	24(%r14)
.Lcfi100:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi101:
	.cfi_adjust_cfa_offset 8
	callq	*16(%r10)
	addq	$32, %rsp
.Lcfi102:
	.cfi_adjust_cfa_offset -32
	movq	16(%r14), %rsi
	movq	%rax, %rdx
	movq	%rbx, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN20btMultiSapBroadphase20addToChildBroadphaseEPNS_15btMultiSapProxyEP17btBroadphaseProxyP21btBroadphaseInterface # TAILCALL
.Lfunc_end24:
	.size	_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallback11processNodeEii, .Lfunc_end24-_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallback11processNodeEii
	.cfi_endproc

	.section	.text._ZN21btNodeOverlapCallbackD2Ev,"axG",@progbits,_ZN21btNodeOverlapCallbackD2Ev,comdat
	.weak	_ZN21btNodeOverlapCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN21btNodeOverlapCallbackD2Ev,@function
_ZN21btNodeOverlapCallbackD2Ev:         # @_ZN21btNodeOverlapCallbackD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end25:
	.size	_ZN21btNodeOverlapCallbackD2Ev, .Lfunc_end25-_ZN21btNodeOverlapCallbackD2Ev
	.cfi_endproc

	.section	.text._ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvT_ii,"axG",@progbits,_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvT_ii,comdat
	.weak	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvT_ii
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvT_ii,@function
_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvT_ii: # @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvT_ii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi106:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi107:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi109:
	.cfi_def_cfa_offset 64
.Lcfi110:
	.cfi_offset %rbx, -56
.Lcfi111:
	.cfi_offset %r12, -48
.Lcfi112:
	.cfi_offset %r13, -40
.Lcfi113:
	.cfi_offset %r14, -32
.Lcfi114:
	.cfi_offset %r15, -24
.Lcfi115:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r12d
	movq	%rdi, %r15
	.p2align	4, 0x90
.LBB26_1:                               # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_2 Depth 2
                                        #       Child Loop BB26_19 Depth 3
                                        #       Child Loop BB26_4 Depth 3
	movl	%r12d, %esi
	movq	16(%r15), %r9
	leal	(%rsi,%r14), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	shlq	$5, %rax
	movq	(%r9,%rax), %r8
	movq	8(%r9,%rax), %r11
	movq	16(%r9,%rax), %r10
	movl	%r14d, %edx
	jmp	.LBB26_2
	.p2align	4, 0x90
.LBB26_50:                              # %._crit_edge
                                        #   in Loop: Header=BB26_2 Depth=2
	movq	16(%r15), %r9
.LBB26_2:                               #   Parent Loop BB26_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB26_19 Depth 3
                                        #       Child Loop BB26_4 Depth 3
	movslq	%r12d, %r12
	testq	%r8, %r8
	je	.LBB26_3
# BB#18:                                # %.split.preheader
                                        #   in Loop: Header=BB26_2 Depth=2
	movq	16(%r8), %r13
	movq	%r12, %rax
	shlq	$5, %rax
	leaq	16(%r9,%rax), %rdi
	jmp	.LBB26_19
	.p2align	4, 0x90
.LBB26_57:                              # %_ZN37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread
                                        #   in Loop: Header=BB26_19 Depth=3
	incq	%r12
	addq	$32, %rdi
.LBB26_19:                              # %.split
                                        #   Parent Loop BB26_1 Depth=1
                                        #     Parent Loop BB26_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB26_20
# BB#21:                                #   in Loop: Header=BB26_19 Depth=3
	movq	16(%rax), %rbx
	jmp	.LBB26_22
	.p2align	4, 0x90
.LBB26_20:                              #   in Loop: Header=BB26_19 Depth=3
	xorl	%ebx, %ebx
.LBB26_22:                              #   in Loop: Header=BB26_19 Depth=3
	movq	-8(%rdi), %rax
	testq	%rax, %rax
	je	.LBB26_23
# BB#24:                                #   in Loop: Header=BB26_19 Depth=3
	movq	16(%rax), %rcx
	testq	%r11, %r11
	jne	.LBB26_27
	jmp	.LBB26_26
	.p2align	4, 0x90
.LBB26_23:                              #   in Loop: Header=BB26_19 Depth=3
	xorl	%ecx, %ecx
	testq	%r11, %r11
	je	.LBB26_26
.LBB26_27:                              #   in Loop: Header=BB26_19 Depth=3
	movq	16(%r11), %rax
	cmpq	%r13, %rbx
	ja	.LBB26_57
	jmp	.LBB26_29
	.p2align	4, 0x90
.LBB26_26:                              #   in Loop: Header=BB26_19 Depth=3
	xorl	%eax, %eax
	cmpq	%r13, %rbx
	ja	.LBB26_57
.LBB26_29:                              #   in Loop: Header=BB26_19 Depth=3
	setne	%bpl
	cmpq	%rax, %rcx
	ja	.LBB26_55
# BB#30:                                #   in Loop: Header=BB26_19 Depth=3
	testb	%bpl, %bpl
	jne	.LBB26_55
# BB#31:                                #   in Loop: Header=BB26_19 Depth=3
	cmpq	%rax, %rcx
	jne	.LBB26_32
# BB#54:                                #   in Loop: Header=BB26_19 Depth=3
	cmpq	%r10, (%rdi)
	ja	.LBB26_57
	jmp	.LBB26_32
	.p2align	4, 0x90
.LBB26_55:                              # %_ZN37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit
                                        #   in Loop: Header=BB26_19 Depth=3
	cmpq	%r13, %rbx
	jne	.LBB26_32
# BB#56:                                # %_ZN37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit
                                        #   in Loop: Header=BB26_19 Depth=3
	cmpq	%rax, %rcx
	ja	.LBB26_57
	jmp	.LBB26_32
	.p2align	4, 0x90
.LBB26_3:                               # %.split.us.preheader
                                        #   in Loop: Header=BB26_2 Depth=2
	movq	%r12, %rax
	shlq	$5, %rax
	leaq	16(%r9,%rax), %rax
	jmp	.LBB26_4
	.p2align	4, 0x90
.LBB26_17:                              # %_ZN37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread.us
                                        #   in Loop: Header=BB26_4 Depth=3
	incq	%r12
	addq	$32, %rax
.LBB26_4:                               # %.split.us
                                        #   Parent Loop BB26_1 Depth=1
                                        #     Parent Loop BB26_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB26_5
# BB#6:                                 #   in Loop: Header=BB26_4 Depth=3
	movq	16(%rcx), %rcx
	jmp	.LBB26_7
	.p2align	4, 0x90
.LBB26_5:                               #   in Loop: Header=BB26_4 Depth=3
	xorl	%ecx, %ecx
.LBB26_7:                               #   in Loop: Header=BB26_4 Depth=3
	movq	-8(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB26_8
# BB#9:                                 #   in Loop: Header=BB26_4 Depth=3
	movq	16(%rdi), %rdi
	testq	%r11, %r11
	jne	.LBB26_12
	jmp	.LBB26_11
	.p2align	4, 0x90
.LBB26_8:                               #   in Loop: Header=BB26_4 Depth=3
	xorl	%edi, %edi
	testq	%r11, %r11
	je	.LBB26_11
.LBB26_12:                              #   in Loop: Header=BB26_4 Depth=3
	movq	16(%r11), %rbx
	testq	%rcx, %rcx
	je	.LBB26_14
	jmp	.LBB26_17
	.p2align	4, 0x90
.LBB26_11:                              #   in Loop: Header=BB26_4 Depth=3
	xorl	%ebx, %ebx
	testq	%rcx, %rcx
	jne	.LBB26_17
.LBB26_14:                              #   in Loop: Header=BB26_4 Depth=3
	cmpq	%rbx, %rdi
	ja	.LBB26_17
# BB#15:                                #   in Loop: Header=BB26_4 Depth=3
	jne	.LBB26_32
# BB#16:                                #   in Loop: Header=BB26_4 Depth=3
	cmpq	%r10, (%rax)
	ja	.LBB26_17
	.p2align	4, 0x90
.LBB26_32:                              # %_ZN37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.preheader
                                        #   in Loop: Header=BB26_2 Depth=2
	movslq	%edx, %rdx
	testq	%r8, %r8
	je	.LBB26_33
# BB#58:                                # %_ZN37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.preheader47
                                        #   in Loop: Header=BB26_2 Depth=2
	movq	16(%r8), %r13
	movq	%rdx, %rax
	shlq	$5, %rax
	leaq	16(%r9,%rax), %rdi
	testq	%r11, %r11
	jne	.LBB26_61
	jmp	.LBB26_60
	.p2align	4, 0x90
.LBB26_75:                              # %_ZN37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.thread
                                        #   in Loop: Header=BB26_2 Depth=2
	decq	%rdx
	addq	$-32, %rdi
	testq	%r11, %r11
	je	.LBB26_60
.LBB26_61:                              #   in Loop: Header=BB26_2 Depth=2
	movq	16(%r11), %rbp
	jmp	.LBB26_62
	.p2align	4, 0x90
.LBB26_60:                              #   in Loop: Header=BB26_2 Depth=2
	xorl	%ebp, %ebp
.LBB26_62:                              #   in Loop: Header=BB26_2 Depth=2
	movq	-16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB26_63
# BB#64:                                #   in Loop: Header=BB26_2 Depth=2
	movq	16(%rax), %rcx
	jmp	.LBB26_65
	.p2align	4, 0x90
.LBB26_63:                              #   in Loop: Header=BB26_2 Depth=2
	xorl	%ecx, %ecx
.LBB26_65:                              #   in Loop: Header=BB26_2 Depth=2
	movq	-8(%rdi), %rax
	testq	%rax, %rax
	je	.LBB26_66
# BB#67:                                #   in Loop: Header=BB26_2 Depth=2
	movq	16(%rax), %rax
	cmpq	%rcx, %r13
	ja	.LBB26_75
	jmp	.LBB26_69
	.p2align	4, 0x90
.LBB26_66:                              #   in Loop: Header=BB26_2 Depth=2
	xorl	%eax, %eax
	cmpq	%rcx, %r13
	ja	.LBB26_75
.LBB26_69:                              #   in Loop: Header=BB26_2 Depth=2
	setne	%bl
	cmpq	%rax, %rbp
	ja	.LBB26_73
# BB#70:                                #   in Loop: Header=BB26_2 Depth=2
	testb	%bl, %bl
	jne	.LBB26_73
# BB#71:                                #   in Loop: Header=BB26_2 Depth=2
	cmpq	%rax, %rbp
	jne	.LBB26_47
# BB#72:                                #   in Loop: Header=BB26_2 Depth=2
	cmpq	(%rdi), %r10
	ja	.LBB26_75
	jmp	.LBB26_47
	.p2align	4, 0x90
.LBB26_73:                              # %_ZN37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32
                                        #   in Loop: Header=BB26_2 Depth=2
	cmpq	%rcx, %r13
	jne	.LBB26_47
# BB#74:                                # %_ZN37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32
                                        #   in Loop: Header=BB26_2 Depth=2
	cmpq	%rax, %rbp
	ja	.LBB26_75
	jmp	.LBB26_47
	.p2align	4, 0x90
.LBB26_33:                              # %_ZN37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.us.preheader
                                        #   in Loop: Header=BB26_2 Depth=2
	movq	%rdx, %rax
	shlq	$5, %rax
	leaq	16(%r9,%rax), %rbp
	testq	%r11, %r11
	jne	.LBB26_36
	jmp	.LBB26_35
	.p2align	4, 0x90
.LBB26_53:                              # %_ZN37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.thread.us
                                        #   in Loop: Header=BB26_2 Depth=2
	decq	%rdx
	addq	$-32, %rbp
	testq	%r11, %r11
	je	.LBB26_35
.LBB26_36:                              #   in Loop: Header=BB26_2 Depth=2
	movq	16(%r11), %rdi
	jmp	.LBB26_37
	.p2align	4, 0x90
.LBB26_35:                              #   in Loop: Header=BB26_2 Depth=2
	xorl	%edi, %edi
.LBB26_37:                              #   in Loop: Header=BB26_2 Depth=2
	movq	-16(%rbp), %rax
	testq	%rax, %rax
	je	.LBB26_38
# BB#39:                                #   in Loop: Header=BB26_2 Depth=2
	movq	16(%rax), %rcx
	jmp	.LBB26_40
	.p2align	4, 0x90
.LBB26_38:                              #   in Loop: Header=BB26_2 Depth=2
	xorl	%ecx, %ecx
.LBB26_40:                              #   in Loop: Header=BB26_2 Depth=2
	movq	-8(%rbp), %rax
	testq	%rax, %rax
	je	.LBB26_41
# BB#42:                                #   in Loop: Header=BB26_2 Depth=2
	movq	16(%rax), %rbx
	jmp	.LBB26_43
	.p2align	4, 0x90
.LBB26_41:                              #   in Loop: Header=BB26_2 Depth=2
	xorl	%ebx, %ebx
.LBB26_43:                              #   in Loop: Header=BB26_2 Depth=2
	testq	%rcx, %rcx
	setne	%al
	cmpq	%rbx, %rdi
	ja	.LBB26_51
# BB#44:                                #   in Loop: Header=BB26_2 Depth=2
	testb	%al, %al
	jne	.LBB26_51
# BB#45:                                #   in Loop: Header=BB26_2 Depth=2
	cmpq	%rbx, %rdi
	jne	.LBB26_47
# BB#46:                                #   in Loop: Header=BB26_2 Depth=2
	cmpq	(%rbp), %r10
	ja	.LBB26_53
	jmp	.LBB26_47
	.p2align	4, 0x90
.LBB26_51:                              # %_ZN37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.us
                                        #   in Loop: Header=BB26_2 Depth=2
	testq	%rcx, %rcx
	jne	.LBB26_47
# BB#52:                                # %_ZN37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.us
                                        #   in Loop: Header=BB26_2 Depth=2
	cmpq	%rbx, %rdi
	ja	.LBB26_53
	.p2align	4, 0x90
.LBB26_47:                              # %_ZN37btMultiSapBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.thread41
                                        #   in Loop: Header=BB26_2 Depth=2
	cmpl	%edx, %r12d
	jg	.LBB26_49
# BB#48:                                #   in Loop: Header=BB26_2 Depth=2
	movq	%r12, %rax
	shlq	$5, %rax
	movups	(%r9,%rax), %xmm0
	movups	16(%r9,%rax), %xmm1
	movq	%rdx, %rcx
	shlq	$5, %rcx
	movups	(%r9,%rcx), %xmm2
	movups	16(%r9,%rcx), %xmm3
	movups	%xmm3, 16(%r9,%rax)
	movups	%xmm2, (%r9,%rax)
	movq	16(%r15), %rax
	movups	%xmm0, (%rax,%rcx)
	movups	%xmm1, 16(%rax,%rcx)
	leal	1(%r12), %r12d
	leal	-1(%rdx), %edx
.LBB26_49:                              #   in Loop: Header=BB26_2 Depth=2
	cmpl	%edx, %r12d
	jle	.LBB26_50
# BB#76:                                #   in Loop: Header=BB26_1 Depth=1
	cmpl	%esi, %edx
	jle	.LBB26_78
# BB#77:                                #   in Loop: Header=BB26_1 Depth=1
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvT_ii
.LBB26_78:                              #   in Loop: Header=BB26_1 Depth=1
	cmpl	%r14d, %r12d
	jl	.LBB26_1
# BB#79:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvT_ii, .Lfunc_end26-_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI37btMultiSapBroadphasePairSortPredicateEEvT_ii
	.cfi_endproc

	.type	_ZTV20btMultiSapBroadphase,@object # @_ZTV20btMultiSapBroadphase
	.section	.rodata,"a",@progbits
	.globl	_ZTV20btMultiSapBroadphase
	.p2align	3
_ZTV20btMultiSapBroadphase:
	.quad	0
	.quad	_ZTI20btMultiSapBroadphase
	.quad	_ZN20btMultiSapBroadphaseD2Ev
	.quad	_ZN20btMultiSapBroadphaseD0Ev
	.quad	_ZN20btMultiSapBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_
	.quad	_ZN20btMultiSapBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.quad	_ZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher
	.quad	_ZNK20btMultiSapBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_
	.quad	_ZN20btMultiSapBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_
	.quad	_ZN20btMultiSapBroadphase25calculateOverlappingPairsEP12btDispatcher
	.quad	_ZN20btMultiSapBroadphase23getOverlappingPairCacheEv
	.quad	_ZNK20btMultiSapBroadphase23getOverlappingPairCacheEv
	.quad	_ZNK20btMultiSapBroadphase17getBroadphaseAabbER9btVector3S1_
	.quad	_ZN20btMultiSapBroadphase9resetPoolEP12btDispatcher
	.quad	_ZN20btMultiSapBroadphase10printStatsEv
	.size	_ZTV20btMultiSapBroadphase, 120

	.type	stopUpdating,@object    # @stopUpdating
	.bss
	.globl	stopUpdating
stopUpdating:
	.byte	0                       # 0x0
	.size	stopUpdating, 1

	.type	_ZTS20btMultiSapBroadphase,@object # @_ZTS20btMultiSapBroadphase
	.section	.rodata,"a",@progbits
	.globl	_ZTS20btMultiSapBroadphase
	.p2align	4
_ZTS20btMultiSapBroadphase:
	.asciz	"20btMultiSapBroadphase"
	.size	_ZTS20btMultiSapBroadphase, 23

	.type	_ZTS21btBroadphaseInterface,@object # @_ZTS21btBroadphaseInterface
	.section	.rodata._ZTS21btBroadphaseInterface,"aG",@progbits,_ZTS21btBroadphaseInterface,comdat
	.weak	_ZTS21btBroadphaseInterface
	.p2align	4
_ZTS21btBroadphaseInterface:
	.asciz	"21btBroadphaseInterface"
	.size	_ZTS21btBroadphaseInterface, 24

	.type	_ZTI21btBroadphaseInterface,@object # @_ZTI21btBroadphaseInterface
	.section	.rodata._ZTI21btBroadphaseInterface,"aG",@progbits,_ZTI21btBroadphaseInterface,comdat
	.weak	_ZTI21btBroadphaseInterface
	.p2align	3
_ZTI21btBroadphaseInterface:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS21btBroadphaseInterface
	.size	_ZTI21btBroadphaseInterface, 16

	.type	_ZTI20btMultiSapBroadphase,@object # @_ZTI20btMultiSapBroadphase
	.section	.rodata,"a",@progbits
	.globl	_ZTI20btMultiSapBroadphase
	.p2align	4
_ZTI20btMultiSapBroadphase:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20btMultiSapBroadphase
	.quad	_ZTI21btBroadphaseInterface
	.size	_ZTI20btMultiSapBroadphase, 24

	.type	_ZTVZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback,@object # @_ZTVZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback
	.p2align	3
_ZTVZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback:
	.quad	0
	.quad	_ZTIZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback
	.quad	_ZN23btOverlapFilterCallbackD2Ev
	.quad	_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheEN31btMultiSapOverlapFilterCallbackD0Ev
	.quad	_ZZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheENK31btMultiSapOverlapFilterCallback23needBroadphaseCollisionEP17btBroadphaseProxyS4_
	.size	_ZTVZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback, 40

	.type	_ZTSZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback,@object # @_ZTSZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback
	.p2align	4
_ZTSZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback:
	.asciz	"ZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback"
	.size	_ZTSZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback, 88

	.type	_ZTS23btOverlapFilterCallback,@object # @_ZTS23btOverlapFilterCallback
	.section	.rodata._ZTS23btOverlapFilterCallback,"aG",@progbits,_ZTS23btOverlapFilterCallback,comdat
	.weak	_ZTS23btOverlapFilterCallback
	.p2align	4
_ZTS23btOverlapFilterCallback:
	.asciz	"23btOverlapFilterCallback"
	.size	_ZTS23btOverlapFilterCallback, 26

	.type	_ZTI23btOverlapFilterCallback,@object # @_ZTI23btOverlapFilterCallback
	.section	.rodata._ZTI23btOverlapFilterCallback,"aG",@progbits,_ZTI23btOverlapFilterCallback,comdat
	.weak	_ZTI23btOverlapFilterCallback
	.p2align	3
_ZTI23btOverlapFilterCallback:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS23btOverlapFilterCallback
	.size	_ZTI23btOverlapFilterCallback, 16

	.type	_ZTIZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback,@object # @_ZTIZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback
	.section	.rodata,"a",@progbits
	.p2align	4
_ZTIZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback
	.quad	_ZTI23btOverlapFilterCallback
	.size	_ZTIZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCacheE31btMultiSapOverlapFilterCallback, 24

	.type	_ZTVZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback,@object # @_ZTVZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback
	.p2align	3
_ZTVZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback:
	.quad	0
	.quad	_ZTIZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback
	.quad	_ZN21btNodeOverlapCallbackD2Ev
	.quad	_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallbackD0Ev
	.quad	_ZZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherEN21MyNodeOverlapCallback11processNodeEii
	.size	_ZTVZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback, 40

	.type	_ZTSZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback,@object # @_ZTSZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback
	.p2align	4
_ZTSZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback:
	.asciz	"ZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback"
	.size	_ZTSZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback, 108

	.type	_ZTS21btNodeOverlapCallback,@object # @_ZTS21btNodeOverlapCallback
	.section	.rodata._ZTS21btNodeOverlapCallback,"aG",@progbits,_ZTS21btNodeOverlapCallback,comdat
	.weak	_ZTS21btNodeOverlapCallback
	.p2align	4
_ZTS21btNodeOverlapCallback:
	.asciz	"21btNodeOverlapCallback"
	.size	_ZTS21btNodeOverlapCallback, 24

	.type	_ZTI21btNodeOverlapCallback,@object # @_ZTI21btNodeOverlapCallback
	.section	.rodata._ZTI21btNodeOverlapCallback,"aG",@progbits,_ZTI21btNodeOverlapCallback,comdat
	.weak	_ZTI21btNodeOverlapCallback
	.p2align	3
_ZTI21btNodeOverlapCallback:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS21btNodeOverlapCallback
	.size	_ZTI21btNodeOverlapCallback, 16

	.type	_ZTIZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback,@object # @_ZTIZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback
	.section	.rodata,"a",@progbits
	.p2align	4
_ZTIZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback
	.quad	_ZTI21btNodeOverlapCallback
	.size	_ZTIZN20btMultiSapBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcherE21MyNodeOverlapCallback, 24


	.globl	_ZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCache
	.type	_ZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCache,@function
_ZN20btMultiSapBroadphaseC1EiP22btOverlappingPairCache = _ZN20btMultiSapBroadphaseC2EiP22btOverlappingPairCache
	.globl	_ZN20btMultiSapBroadphaseD1Ev
	.type	_ZN20btMultiSapBroadphaseD1Ev,@function
_ZN20btMultiSapBroadphaseD1Ev = _ZN20btMultiSapBroadphaseD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
