	.text
	.file	"hexxagonmove.bc"
	.globl	_ZN16HexxagonMoveList7addMoveER12HexxagonMove
	.p2align	4, 0x90
	.type	_ZN16HexxagonMoveList7addMoveER12HexxagonMove,@function
_ZN16HexxagonMoveList7addMoveER12HexxagonMove: # @_ZN16HexxagonMoveList7addMoveER12HexxagonMove
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %r15
	movl	(%r15), %r13d
	testb	$15, %r13b
	jne	.LBB0_8
# BB#1:
	cmpl	$16, %r13d
	jl	.LBB0_8
# BB#2:                                 # %.lr.ph
	movl	$16, %ebp
	movq	$-1, %r14
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	cmpl	%ebp, %r13d
	jne	.LBB0_7
# BB#4:                                 # %.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	leal	(%r13,%r13), %eax
	cltq
	movl	$8, %ecx
	mulq	%rcx
	cmovoq	%r14, %rax
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	movq	8(%r15), %rbx
	movslq	%r13d, %rdx
	shlq	$3, %rdx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	memcpy
	testq	%rbx, %rbx
	je	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	%rbx, %rdi
	callq	_ZdlPv
	movl	(%r15), %r13d
.LBB0_6:                                #   in Loop: Header=BB0_3 Depth=1
	movq	%r12, 8(%r15)
.LBB0_7:                                #   in Loop: Header=BB0_3 Depth=1
	addl	%ebp, %ebp
	cmpl	%r13d, %ebp
	jle	.LBB0_3
.LBB0_8:                                # %.loopexit11
	movq	8(%r15), %rax
	movslq	%r13d, %rcx
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	(%rdx), %rdx
	movq	%rdx, (%rax,%rcx,8)
	incl	(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN16HexxagonMoveList7addMoveER12HexxagonMove, .Lfunc_end0-_ZN16HexxagonMoveList7addMoveER12HexxagonMove
	.cfi_endproc

	.globl	_Z7comparePKvS0_
	.p2align	4, 0x90
	.type	_Z7comparePKvS0_,@function
_Z7comparePKvS0_:                       # @_Z7comparePKvS0_
	.cfi_startproc
# BB#0:
	movl	4(%rsi), %eax
	subl	4(%rdi), %eax
	retq
.Lfunc_end1:
	.size	_Z7comparePKvS0_, .Lfunc_end1-_Z7comparePKvS0_
	.cfi_endproc

	.globl	_ZN16HexxagonMoveList13sortListQuickEv
	.p2align	4, 0x90
	.type	_ZN16HexxagonMoveList13sortListQuickEv,@function
_ZN16HexxagonMoveList13sortListQuickEv: # @_ZN16HexxagonMoveList13sortListQuickEv
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movslq	(%rdi), %rsi
	movl	$8, %edx
	movl	$_Z7comparePKvS0_, %ecx
	movq	%rax, %rdi
	jmp	qsort                   # TAILCALL
.Lfunc_end2:
	.size	_ZN16HexxagonMoveList13sortListQuickEv, .Lfunc_end2-_ZN16HexxagonMoveList13sortListQuickEv
	.cfi_endproc

	.globl	_ZN16HexxagonMoveList8sortListEv
	.p2align	4, 0x90
	.type	_ZN16HexxagonMoveList8sortListEv,@function
_ZN16HexxagonMoveList8sortListEv:       # @_ZN16HexxagonMoveList8sortListEv
	.cfi_startproc
# BB#0:
	.p2align	4, 0x90
.LBB3_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_3 Depth 2
                                        #       Child Loop BB3_4 Depth 3
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.LBB3_3
	.p2align	4, 0x90
.LBB3_6:                                #   in Loop: Header=BB3_3 Depth=2
	movq	(%rax,%rsi,8), %r8
	movq	8(%rax,%rsi,8), %rdx
	movq	%rdx, (%rax,%rsi,8)
	movq	8(%rdi), %rax
	movq	%r8, 8(%rax,%rsi,8)
	movl	$1, %r8d
.LBB3_3:                                # %.outer
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_4 Depth 3
	movslq	(%rdi), %r9
	decq	%r9
	movslq	%ecx, %rcx
	.p2align	4, 0x90
.LBB3_4:                                #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rsi
	cmpq	%r9, %rsi
	jge	.LBB3_1
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=3
	movq	8(%rdi), %rax
	movl	4(%rax,%rsi,8), %edx
	leaq	1(%rsi), %rcx
	cmpl	12(%rax,%rsi,8), %edx
	jge	.LBB3_4
	jmp	.LBB3_6
	.p2align	4, 0x90
.LBB3_1:                                # %.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	testl	%r8d, %r8d
	jne	.LBB3_2
# BB#7:
	retq
.Lfunc_end3:
	.size	_ZN16HexxagonMoveList8sortListEv, .Lfunc_end3-_ZN16HexxagonMoveList8sortListEv
	.cfi_endproc

	.globl	_Z7getTimev
	.p2align	4, 0x90
	.type	_Z7getTimev,@function
_Z7getTimev:                            # @_Z7getTimev
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 32
	leaq	8(%rsp), %rdi
	callq	ftime
	imull	$1000, 8(%rsp), %ecx    # imm = 0x3E8
	movzwl	16(%rsp), %eax
	addl	%ecx, %eax
	addq	$24, %rsp
	retq
.Lfunc_end4:
	.size	_Z7getTimev, .Lfunc_end4-_Z7getTimev
	.cfi_endproc

	.globl	_ZN16HexxagonMoveList7getMoveEi
	.p2align	4, 0x90
	.type	_ZN16HexxagonMoveList7getMoveEi,@function
_ZN16HexxagonMoveList7getMoveEi:        # @_ZN16HexxagonMoveList7getMoveEi
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	shlq	$3, %rax
	addq	8(%rdi), %rax
	retq
.Lfunc_end5:
	.size	_ZN16HexxagonMoveList7getMoveEi, .Lfunc_end5-_ZN16HexxagonMoveList7getMoveEi
	.cfi_endproc

	.globl	_ZN16HexxagonMoveList10getNrMovesEv
	.p2align	4, 0x90
	.type	_ZN16HexxagonMoveList10getNrMovesEv,@function
_ZN16HexxagonMoveList10getNrMovesEv:    # @_ZN16HexxagonMoveList10getNrMovesEv
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	retq
.Lfunc_end6:
	.size	_ZN16HexxagonMoveList10getNrMovesEv, .Lfunc_end6-_ZN16HexxagonMoveList10getNrMovesEv
	.cfi_endproc

	.globl	_Z9alphaBetaR13HexxagonBoardiiiPFvvE
	.p2align	4, 0x90
	.type	_Z9alphaBetaR13HexxagonBoardiiiPFvvE,@function
_Z9alphaBetaR13HexxagonBoardiiiPFvvE:   # @_Z9alphaBetaR13HexxagonBoardiiiPFvvE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 112
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%edx, %ebp
	movq	%rdi, %rbx
	movl	%esi, 12(%rsp)          # 4-byte Spill
	testl	%esi, %esi
	je	.LBB7_19
# BB#1:
	movq	%rbx, %rdi
	callq	_ZN13HexxagonBoard16generateMoveListEv
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB7_2
# BB#6:
	movb	hexx_count(%rip), %al
	incb	%al
	movb	%al, hexx_count(%rip)
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB7_9
# BB#7:
	testb	%al, %al
	jne	.LBB7_9
# BB#8:
	callq	*16(%rsp)               # 8-byte Folded Reload
.LBB7_9:                                # %.preheader
	cmpl	$-31999, 8(%rsp)        # 4-byte Folded Reload
                                        # imm = 0x8301
	jl	.LBB7_11
# BB#10:                                # %.preheader
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.LBB7_11
# BB#12:                                # %.lr.ph
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	decl	12(%rsp)                # 4-byte Folded Spill
	movl	8(%rsp), %eax           # 4-byte Reload
	negl	%eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	$-32000, %r14d          # imm = 0x8300
	xorl	%r13d, %r13d
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB7_13:                               # =>This Inner Loop Header: Depth=1
	movl	%r14d, %ebx
	cmpl	%ebp, %ebx
	cmovgel	%ebx, %ebp
	leaq	40(%rsp), %r14
	movq	%r14, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	_ZN13HexxagonBoardC1ERKS_
	movq	8(%r12), %rsi
	addq	%r13, %rsi
	movq	%r14, %rdi
	callq	_ZN13HexxagonBoard9applyMoveER12HexxagonMove
	movl	%ebp, %ecx
	negl	%ecx
	movq	%r14, %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	28(%rsp), %edx          # 4-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	_Z9alphaBetaR13HexxagonBoardiiiPFvvE
	movl	%eax, %r14d
	negl	%r14d
	cmpl	%r14d, %ebx
	cmovgel	%ebx, %r14d
	cmpl	8(%rsp), %r14d          # 4-byte Folded Reload
	jge	.LBB7_15
# BB#14:                                #   in Loop: Header=BB7_13 Depth=1
	movslq	(%r12), %rax
	addq	$8, %r13
	cmpq	%rax, %r15
	leaq	1(%r15), %r15
	jl	.LBB7_13
	jmp	.LBB7_15
.LBB7_2:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN13HexxagonBoard11countBricksEi
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jle	.LBB7_4
# BB#3:
	addl	$20000, %r14d           # imm = 0x4E20
	jmp	.LBB7_18
.LBB7_11:                               # %.preheader..critedge_crit_edge
	movl	$-32000, %r14d          # imm = 0x8300
.LBB7_15:                               # %.critedge
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB7_17
# BB#16:
	callq	_ZdlPv
.LBB7_17:                               # %_ZN16HexxagonMoveListD2Ev.exit
	movq	%r12, %rdi
	callq	_ZdlPv
.LBB7_18:
	movl	%r14d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_4:
	js	.LBB7_5
.LBB7_19:
	movq	%rbx, %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN13HexxagonBoard8evaluateEv # TAILCALL
.LBB7_5:
	addl	$-20000, %r14d          # imm = 0xB1E0
	jmp	.LBB7_18
.Lfunc_end7:
	.size	_Z9alphaBetaR13HexxagonBoardiiiPFvvE, .Lfunc_end7-_Z9alphaBetaR13HexxagonBoardiiiPFvvE
	.cfi_endproc

	.globl	_ZN16HexxagonMoveList13scoreAllMovesE13HexxagonBoardiPFvvEi
	.p2align	4, 0x90
	.type	_ZN16HexxagonMoveList13scoreAllMovesE13HexxagonBoardiPFvvEi,@function
_ZN16HexxagonMoveList13scoreAllMovesE13HexxagonBoardiPFvvEi: # @_ZN16HexxagonMoveList13scoreAllMovesE13HexxagonBoardiPFvvEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 96
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%edx, %ebp
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %rbx
	leaq	24(%rsp), %rdi
	callq	ftime
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	cmpl	$2, %ebp
	jl	.LBB8_13
# BB#1:                                 # %.preheader.lr.ph
	movl	(%rbx), %r8d
	movl	$1, (%rsp)              # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB8_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_4 Depth 2
                                        #     Child Loop BB8_7 Depth 2
                                        #       Child Loop BB8_8 Depth 3
                                        #         Child Loop BB8_9 Depth 4
	testl	%r8d, %r8d
	jle	.LBB8_7
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	movl	$-32000, %r14d          # imm = 0x8300
	movl	$1, %r15d
	xorl	%r12d, %r12d
	movl	$-32000, %eax           # imm = 0x8300
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %r13d
	cmpl	%r14d, %r13d
	cmovgel	%r13d, %r14d
	leaq	24(%rsp), %rbp
	movq	%rbp, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	_ZN13HexxagonBoardC1ERKS_
	movq	8(%rbx), %rsi
	addq	%r12, %rsi
	movq	%rbp, %rdi
	callq	_ZN13HexxagonBoard9applyMoveER12HexxagonMove
	movl	%r14d, %ecx
	negl	%ecx
	movl	$-32000, %edx           # imm = 0x8300
	movq	%rbp, %rdi
	movl	(%rsp), %esi            # 4-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	_Z9alphaBetaR13HexxagonBoardiiiPFvvE
	negl	%eax
	movq	8(%rbx), %rcx
	movl	%eax, 4(%rcx,%r12)
	cmpl	%eax, %r13d
	cmovgel	%r13d, %eax
	movl	(%rbx), %r8d
	cmpl	$31999, %eax            # imm = 0x7CFF
	jg	.LBB8_7
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB8_4 Depth=2
	movslq	%r8d, %rcx
	addq	$8, %r12
	cmpq	%rcx, %r15
	leaq	1(%r15), %r15
	jl	.LBB8_4
	.p2align	4, 0x90
.LBB8_7:                                # %.preheader.i
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_8 Depth 3
                                        #         Child Loop BB8_9 Depth 4
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.LBB8_8
	.p2align	4, 0x90
.LBB8_11:                               #   in Loop: Header=BB8_8 Depth=3
	movq	(%rbp,%rsi,8), %rax
	movq	8(%rbp,%rsi,8), %rcx
	movq	%rcx, (%rbp,%rsi,8)
	movq	8(%rbx), %rcx
	movq	%rax, 8(%rcx,%rsi,8)
	movl	(%rbx), %r8d
	movl	$1, %eax
.LBB8_8:                                # %.outer.i
                                        #   Parent Loop BB8_2 Depth=1
                                        #     Parent Loop BB8_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB8_9 Depth 4
	leal	-1(%r8), %esi
	movslq	%edx, %rdx
	movslq	%esi, %rdi
	.p2align	4, 0x90
.LBB8_9:                                #   Parent Loop BB8_2 Depth=1
                                        #     Parent Loop BB8_7 Depth=2
                                        #       Parent Loop BB8_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rdx, %rsi
	cmpq	%rdi, %rsi
	jge	.LBB8_6
# BB#10:                                #   in Loop: Header=BB8_9 Depth=4
	movq	8(%rbx), %rbp
	movl	4(%rbp,%rsi,8), %ecx
	leaq	1(%rsi), %rdx
	cmpl	12(%rbp,%rsi,8), %ecx
	jge	.LBB8_9
	jmp	.LBB8_11
	.p2align	4, 0x90
.LBB8_6:                                # %.loopexit.i
                                        #   in Loop: Header=BB8_7 Depth=2
	testl	%eax, %eax
	jne	.LBB8_7
# BB#12:                                # %_ZN16HexxagonMoveList8sortListEv.exit
                                        #   in Loop: Header=BB8_2 Depth=1
	movl	(%rsp), %eax            # 4-byte Reload
	incl	%eax
	movl	%eax, (%rsp)            # 4-byte Spill
	cmpl	4(%rsp), %eax           # 4-byte Folded Reload
	jne	.LBB8_2
.LBB8_13:                               # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN16HexxagonMoveList13scoreAllMovesE13HexxagonBoardiPFvvEi, .Lfunc_end8-_ZN16HexxagonMoveList13scoreAllMovesE13HexxagonBoardiPFvvEi
	.cfi_endproc

	.globl	_ZN16HexxagonMoveList11getBestMoveEv
	.p2align	4, 0x90
	.type	_ZN16HexxagonMoveList11getBestMoveEv,@function
_ZN16HexxagonMoveList11getBestMoveEv:   # @_ZN16HexxagonMoveList11getBestMoveEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 32
.Lcfi42:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	%rsp, %rdi
	callq	ftime
	imull	$1000, (%rsp), %eax     # imm = 0x3E8
	movzwl	8(%rsp), %edi
	addl	%eax, %edi
	callq	srandom
	cmpl	$0, (%rbx)
	je	.LBB9_1
# BB#2:
	movq	8(%rbx), %rax
	jmp	.LBB9_3
.LBB9_1:
	xorl	%eax, %eax
.LBB9_3:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end9:
	.size	_ZN16HexxagonMoveList11getBestMoveEv, .Lfunc_end9-_ZN16HexxagonMoveList11getBestMoveEv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_hexxagonmove.ii,@function
_GLOBAL__sub_I_hexxagonmove.ii:         # @_GLOBAL__sub_I_hexxagonmove.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end10:
	.size	_GLOBAL__sub_I_hexxagonmove.ii, .Lfunc_end10-_GLOBAL__sub_I_hexxagonmove.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	hexx_count,@object      # @hexx_count
	.bss
	.globl	hexx_count
hexx_count:
	.byte	0                       # 0x0
	.size	hexx_count, 1

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_hexxagonmove.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
