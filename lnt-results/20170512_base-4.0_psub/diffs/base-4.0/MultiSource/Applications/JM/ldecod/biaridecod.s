	.text
	.file	"biaridecod.bc"
	.globl	arideco_create_decoding_environment
	.p2align	4, 0x90
	.type	arideco_create_decoding_environment,@function
arideco_create_decoding_environment:    # @arideco_create_decoding_environment
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	movl	$40, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB0_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_2:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	arideco_create_decoding_environment, .Lfunc_end0-arideco_create_decoding_environment
	.cfi_endproc

	.globl	arideco_delete_decoding_environment
	.p2align	4, 0x90
	.type	arideco_delete_decoding_environment,@function
arideco_delete_decoding_environment:    # @arideco_delete_decoding_environment
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB1_1
# BB#2:
	jmp	free                    # TAILCALL
.LBB1_1:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$200, %esi
	popq	%rax
	jmp	error                   # TAILCALL
.Lfunc_end1:
	.size	arideco_delete_decoding_environment, .Lfunc_end1-arideco_delete_decoding_environment
	.cfi_endproc

	.globl	arideco_start_decoding
	.p2align	4, 0x90
	.type	arideco_start_decoding,@function
arideco_start_decoding:                 # @arideco_start_decoding
	.cfi_startproc
# BB#0:
	movq	%rcx, %r8
	movq	%rsi, 24(%rdi)
	movq	%r8, 32(%rdi)
	movl	%edx, (%r8)
	movl	$0, 16(%rdi)
	movl	$-1, 16(%rdi)
	movslq	(%r8), %rax
	leal	1(%rax), %ecx
	movl	%ecx, (%r8)
	movzbl	(%rsi,%rax), %r9d
	movl	%r9d, 12(%rdi)
	movl	$7, 16(%rdi)
	shrl	$7, %r9d
	movl	$6, 16(%rdi)
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB2_2
# BB#1:                                 # %._crit_edge.1
	movl	$6, %edx
	movl	12(%rdi), %eax
	jmp	.LBB2_3
.LBB2_2:
	movslq	(%r8), %rax
	leal	1(%rax), %ecx
	movl	%ecx, (%r8)
	movzbl	(%rsi,%rax), %eax
	movl	%eax, 12(%rdi)
	movl	$7, 16(%rdi)
	movl	$7, %edx
.LBB2_3:
	movl	%edx, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	(%rax,%r9,2), %eax
	leal	-1(%rdx), %ecx
	movl	%ecx, 16(%rdi)
	testl	%edx, %edx
	jle	.LBB2_5
# BB#4:                                 # %._crit_edge.2
	movl	12(%rdi), %edx
	jmp	.LBB2_6
.LBB2_5:
	movslq	(%r8), %rcx
	leal	1(%rcx), %edx
	movl	%edx, (%r8)
	movzbl	(%rsi,%rcx), %edx
	movl	%edx, 12(%rdi)
	movl	$7, 16(%rdi)
	movl	$7, %ecx
.LBB2_6:
	shrl	%cl, %edx
	andl	$1, %edx
	leal	(%rdx,%rax,2), %r9d
	leal	-1(%rcx), %edx
	movl	%edx, 16(%rdi)
	testl	%ecx, %ecx
	jle	.LBB2_8
# BB#7:                                 # %._crit_edge.3
	movl	12(%rdi), %eax
	jmp	.LBB2_9
.LBB2_8:
	movslq	(%r8), %rax
	leal	1(%rax), %ecx
	movl	%ecx, (%r8)
	movzbl	(%rsi,%rax), %eax
	movl	%eax, 12(%rdi)
	movl	$7, 16(%rdi)
	movl	$7, %edx
.LBB2_9:
	movl	%edx, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	(%rax,%r9,2), %eax
	leal	-1(%rdx), %ecx
	movl	%ecx, 16(%rdi)
	testl	%edx, %edx
	jle	.LBB2_11
# BB#10:                                # %._crit_edge.4
	movl	12(%rdi), %edx
	jmp	.LBB2_12
.LBB2_11:
	movslq	(%r8), %rcx
	leal	1(%rcx), %edx
	movl	%edx, (%r8)
	movzbl	(%rsi,%rcx), %edx
	movl	%edx, 12(%rdi)
	movl	$7, 16(%rdi)
	movl	$7, %ecx
.LBB2_12:
	shrl	%cl, %edx
	andl	$1, %edx
	leal	(%rdx,%rax,2), %r9d
	leal	-1(%rcx), %edx
	movl	%edx, 16(%rdi)
	testl	%ecx, %ecx
	jle	.LBB2_14
# BB#13:                                # %._crit_edge.5
	movl	12(%rdi), %eax
	jmp	.LBB2_15
.LBB2_14:
	movslq	(%r8), %rax
	leal	1(%rax), %ecx
	movl	%ecx, (%r8)
	movzbl	(%rsi,%rax), %eax
	movl	%eax, 12(%rdi)
	movl	$7, 16(%rdi)
	movl	$7, %edx
.LBB2_15:
	movl	%edx, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	(%rax,%r9,2), %eax
	leal	-1(%rdx), %ecx
	movl	%ecx, 16(%rdi)
	testl	%edx, %edx
	jle	.LBB2_17
# BB#16:                                # %._crit_edge.6
	movl	12(%rdi), %edx
	jmp	.LBB2_18
.LBB2_17:
	movslq	(%r8), %rcx
	leal	1(%rcx), %edx
	movl	%edx, (%r8)
	movzbl	(%rsi,%rcx), %edx
	movl	%edx, 12(%rdi)
	movl	$7, 16(%rdi)
	movl	$7, %ecx
.LBB2_18:
	shrl	%cl, %edx
	andl	$1, %edx
	leal	(%rdx,%rax,2), %r9d
	leal	-1(%rcx), %edx
	movl	%edx, 16(%rdi)
	testl	%ecx, %ecx
	jle	.LBB2_20
# BB#19:                                # %._crit_edge.7
	movl	12(%rdi), %eax
	jmp	.LBB2_21
.LBB2_20:
	movslq	(%r8), %rax
	leal	1(%rax), %ecx
	movl	%ecx, (%r8)
	movzbl	(%rsi,%rax), %eax
	movl	%eax, 12(%rdi)
	movl	$7, 16(%rdi)
	movl	$7, %edx
.LBB2_21:
	movl	%edx, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	(%rax,%r9,2), %eax
	leal	-1(%rdx), %ecx
	movl	%ecx, 16(%rdi)
	testl	%edx, %edx
	jle	.LBB2_23
# BB#22:                                # %._crit_edge.8
	movl	12(%rdi), %edx
	jmp	.LBB2_24
.LBB2_23:
	movslq	(%r8), %rcx
	leal	1(%rcx), %edx
	movl	%edx, (%r8)
	movzbl	(%rsi,%rcx), %edx
	movl	%edx, 12(%rdi)
	movl	$7, 16(%rdi)
	movl	$7, %ecx
.LBB2_24:
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	andl	$1, %edx
	leal	(%rdx,%rax,2), %eax
	movl	$510, 4(%rdi)           # imm = 0x1FE
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end2:
	.size	arideco_start_decoding, .Lfunc_end2-arideco_start_decoding
	.cfi_endproc

	.globl	arideco_bits_read
	.p2align	4, 0x90
	.type	arideco_bits_read,@function
arideco_bits_read:                      # @arideco_bits_read
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rax
	movl	(%rax), %eax
	leal	-16(,%rax,8), %eax
	subl	16(%rdi), %eax
	retq
.Lfunc_end3:
	.size	arideco_bits_read, .Lfunc_end3-arideco_bits_read
	.cfi_endproc

	.globl	arideco_done_decoding
	.p2align	4, 0x90
	.type	arideco_done_decoding,@function
arideco_done_decoding:                  # @arideco_done_decoding
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rax
	incl	(%rax)
	retq
.Lfunc_end4:
	.size	arideco_done_decoding, .Lfunc_end4-arideco_done_decoding
	.cfi_endproc

	.globl	biari_decode_symbol
	.p2align	4, 0x90
	.type	biari_decode_symbol,@function
biari_decode_symbol:                    # @biari_decode_symbol
	.cfi_startproc
# BB#0:
	movzbl	2(%rsi), %eax
	movl	4(%rdi), %r8d
	movl	8(%rdi), %edx
	movzwl	(%rsi), %r10d
	movl	%r8d, %ecx
	shrl	$6, %ecx
	andl	$3, %ecx
	movzbl	rLPS_table_64x4(%rcx,%r10,4), %r9d
	subl	%r9d, %r8d
	movl	%edx, %ecx
	subl	%r8d, %ecx
	jae	.LBB5_1
# BB#4:
	movzwl	AC_next_state_MPS_64(%r10,%r10), %ecx
	movw	%cx, (%rsi)
	cmpl	$255, %r8d
	movl	%r8d, %r9d
	movl	%edx, %ecx
	jbe	.LBB5_5
	jmp	.LBB5_10
.LBB5_1:
	xorl	%edx, %edx
	testb	%al, %al
	sete	%r8b
	testw	%r10w, %r10w
	jne	.LBB5_3
# BB#2:
	xorb	$1, %al
	movb	%al, 2(%rsi)
.LBB5_3:                                # %.thread
	movb	%r8b, %dl
	movzwl	AC_next_state_LPS_64(%r10,%r10), %eax
	movw	%ax, (%rsi)
	movl	%edx, %eax
.LBB5_5:                                # %.lr.ph
	movl	16(%rdi), %esi
	movl	%r9d, %r8d
	movl	%ecx, %edx
	.p2align	4, 0x90
.LBB5_6:                                # =>This Inner Loop Header: Depth=1
	addl	%r8d, %r8d
	leal	-1(%rsi), %ecx
	movl	%ecx, 16(%rdi)
	testl	%esi, %esi
	jle	.LBB5_8
# BB#7:                                 # %._crit_edge48
                                        #   in Loop: Header=BB5_6 Depth=1
	movl	12(%rdi), %esi
	jmp	.LBB5_9
	.p2align	4, 0x90
.LBB5_8:                                #   in Loop: Header=BB5_6 Depth=1
	movq	24(%rdi), %r9
	movq	32(%rdi), %rsi
	movslq	(%rsi), %r10
	leal	1(%r10), %ecx
	movl	%ecx, (%rsi)
	movzbl	(%r9,%r10), %esi
	movl	%esi, 12(%rdi)
	movl	$7, 16(%rdi)
	movl	$7, %ecx
.LBB5_9:                                #   in Loop: Header=BB5_6 Depth=1
	shrl	%cl, %esi
	andl	$1, %esi
	leal	(%rsi,%rdx,2), %edx
	cmpl	$256, %r8d              # imm = 0x100
	movl	%ecx, %esi
	jb	.LBB5_6
.LBB5_10:                               # %._crit_edge
	movl	%r8d, 4(%rdi)
	movl	%edx, 8(%rdi)
	retq
.Lfunc_end5:
	.size	biari_decode_symbol, .Lfunc_end5-biari_decode_symbol
	.cfi_endproc

	.globl	biari_decode_symbol_eq_prob
	.p2align	4, 0x90
	.type	biari_decode_symbol_eq_prob,@function
biari_decode_symbol_eq_prob:            # @biari_decode_symbol_eq_prob
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	movl	16(%rdi), %edx
	addl	%eax, %eax
	leal	-1(%rdx), %ecx
	movl	%ecx, 16(%rdi)
	testl	%edx, %edx
	jle	.LBB6_2
# BB#1:                                 # %._crit_edge
	movl	12(%rdi), %edx
	jmp	.LBB6_3
.LBB6_2:
	movq	24(%rdi), %r8
	movq	32(%rdi), %rdx
	movslq	(%rdx), %rsi
	leal	1(%rsi), %ecx
	movl	%ecx, (%rdx)
	movzbl	(%r8,%rsi), %edx
	movl	%edx, 12(%rdi)
	movl	$7, 16(%rdi)
	movl	$7, %ecx
.LBB6_3:
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	andl	$1, %edx
	orl	%eax, %edx
	movl	4(%rdi), %ecx
	xorl	%esi, %esi
	xorl	%eax, %eax
	cmpl	%ecx, %edx
	setae	%al
	cmovael	%ecx, %esi
	subl	%esi, %edx
	movl	%edx, 8(%rdi)
	retq
.Lfunc_end6:
	.size	biari_decode_symbol_eq_prob, .Lfunc_end6-biari_decode_symbol_eq_prob
	.cfi_endproc

	.globl	biari_decode_final
	.p2align	4, 0x90
	.type	biari_decode_final,@function
biari_decode_final:                     # @biari_decode_final
	.cfi_startproc
# BB#0:
	movl	4(%rdi), %r9d
	movl	8(%rdi), %esi
	addl	$-2, %r9d
	movl	$1, %eax
	cmpl	%r9d, %esi
	jae	.LBB7_8
# BB#1:                                 # %.preheader
	cmpl	$255, %r9d
	ja	.LBB7_7
# BB#2:                                 # %.lr.ph
	movl	16(%rdi), %eax
	.p2align	4, 0x90
.LBB7_3:                                # =>This Inner Loop Header: Depth=1
	addl	%r9d, %r9d
	leal	-1(%rax), %ecx
	movl	%ecx, 16(%rdi)
	testl	%eax, %eax
	jle	.LBB7_5
# BB#4:                                 # %._crit_edge27
                                        #   in Loop: Header=BB7_3 Depth=1
	movl	12(%rdi), %eax
	jmp	.LBB7_6
	.p2align	4, 0x90
.LBB7_5:                                #   in Loop: Header=BB7_3 Depth=1
	movq	24(%rdi), %r8
	movq	32(%rdi), %rcx
	movslq	(%rcx), %rax
	leal	1(%rax), %edx
	movl	%edx, (%rcx)
	movzbl	(%r8,%rax), %eax
	movl	%eax, 12(%rdi)
	movl	$7, 16(%rdi)
	movl	$7, %ecx
.LBB7_6:                                #   in Loop: Header=BB7_3 Depth=1
	shrl	%cl, %eax
	andl	$1, %eax
	leal	(%rax,%rsi,2), %esi
	cmpl	$256, %r9d              # imm = 0x100
	movl	%ecx, %eax
	jb	.LBB7_3
.LBB7_7:                                # %._crit_edge
	movl	%esi, 8(%rdi)
	movl	%r9d, 4(%rdi)
	xorl	%eax, %eax
.LBB7_8:
	retq
.Lfunc_end7:
	.size	biari_decode_final, .Lfunc_end7-biari_decode_final
	.cfi_endproc

	.globl	biari_init_context
	.p2align	4, 0x90
	.type	biari_init_context,@function
biari_init_context:                     # @biari_init_context
	.cfi_startproc
# BB#0:
	movl	28(%rdi), %eax
	xorl	%ecx, %ecx
	testl	%eax, %eax
	cmovnsl	%eax, %ecx
	imull	(%rdx), %ecx
	sarl	$4, %ecx
	addl	4(%rdx), %ecx
	testl	%ecx, %ecx
	movl	$1, %eax
	cmovgl	%ecx, %eax
	cmpl	$127, %eax
	movl	$126, %ecx
	cmovll	%eax, %ecx
	movl	$63, %eax
	subl	%ecx, %eax
	leal	65472(%rcx), %edx
	cmpl	$63, %ecx
	cmovgw	%dx, %ax
	movw	%ax, (%rsi)
	setg	2(%rsi)
	retq
.Lfunc_end8:
	.size	biari_init_context, .Lfunc_end8-biari_init_context
	.cfi_endproc

	.type	binCount,@object        # @binCount
	.bss
	.globl	binCount
	.p2align	2
binCount:
	.long	0                       # 0x0
	.size	binCount, 4

	.type	rLPS_table_64x4,@object # @rLPS_table_64x4
	.section	.rodata,"a",@progbits
	.globl	rLPS_table_64x4
	.p2align	4
rLPS_table_64x4:
	.ascii	"\200\260\320\360"
	.ascii	"\200\247\305\343"
	.ascii	"\200\236\273\330"
	.ascii	"{\226\262\315"
	.ascii	"t\216\251\303"
	.ascii	"o\207\240\271"
	.ascii	"i\200\230\257"
	.ascii	"dz\220\246"
	.ascii	"_t\211\236"
	.ascii	"Zn\202\226"
	.ascii	"Uh{\216"
	.ascii	"Qcu\207"
	.ascii	"M^o\200"
	.ascii	"IYiz"
	.ascii	"EUdt"
	.ascii	"BP_n"
	.ascii	">LZh"
	.ascii	";HVc"
	.ascii	"8EQ^"
	.ascii	"5AMY"
	.ascii	"3>IU"
	.ascii	"0;EP"
	.ascii	".8BL"
	.ascii	"+5?H"
	.ascii	")2;E"
	.ascii	"'08A"
	.ascii	"%-6>"
	.ascii	"#+3;"
	.ascii	"!)08"
	.ascii	" '.5"
	.ascii	"\036%+2"
	.ascii	"\035#)0"
	.ascii	"\033!'-"
	.ascii	"\032\037%+"
	.ascii	"\030\036#)"
	.ascii	"\027\034!'"
	.ascii	"\026\033 %"
	.ascii	"\025\032\036#"
	.ascii	"\024\030\035!"
	.ascii	"\023\027\033\037"
	.ascii	"\022\026\032\036"
	.ascii	"\021\025\031\034"
	.ascii	"\020\024\027\033"
	.ascii	"\017\023\026\031"
	.ascii	"\016\022\025\030"
	.ascii	"\016\021\024\027"
	.ascii	"\r\020\023\026"
	.ascii	"\f\017\022\025"
	.ascii	"\f\016\021\024"
	.ascii	"\013\016\020\023"
	.ascii	"\013\r\017\022"
	.ascii	"\n\f\017\021"
	.ascii	"\n\f\016\020"
	.ascii	"\t\013\r\017"
	.ascii	"\t\013\f\016"
	.ascii	"\b\n\f\016"
	.ascii	"\b\t\013\r"
	.ascii	"\007\t\013\f"
	.ascii	"\007\t\n\f"
	.ascii	"\007\b\n\013"
	.ascii	"\006\b\t\013"
	.ascii	"\006\007\t\n"
	.ascii	"\006\007\b\t"
	.zero	4,2
	.size	rLPS_table_64x4, 256

	.type	AC_next_state_MPS_64,@object # @AC_next_state_MPS_64
	.globl	AC_next_state_MPS_64
	.p2align	4
AC_next_state_MPS_64:
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	14                      # 0xe
	.short	15                      # 0xf
	.short	16                      # 0x10
	.short	17                      # 0x11
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	23                      # 0x17
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	26                      # 0x1a
	.short	27                      # 0x1b
	.short	28                      # 0x1c
	.short	29                      # 0x1d
	.short	30                      # 0x1e
	.short	31                      # 0x1f
	.short	32                      # 0x20
	.short	33                      # 0x21
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	36                      # 0x24
	.short	37                      # 0x25
	.short	38                      # 0x26
	.short	39                      # 0x27
	.short	40                      # 0x28
	.short	41                      # 0x29
	.short	42                      # 0x2a
	.short	43                      # 0x2b
	.short	44                      # 0x2c
	.short	45                      # 0x2d
	.short	46                      # 0x2e
	.short	47                      # 0x2f
	.short	48                      # 0x30
	.short	49                      # 0x31
	.short	50                      # 0x32
	.short	51                      # 0x33
	.short	52                      # 0x34
	.short	53                      # 0x35
	.short	54                      # 0x36
	.short	55                      # 0x37
	.short	56                      # 0x38
	.short	57                      # 0x39
	.short	58                      # 0x3a
	.short	59                      # 0x3b
	.short	60                      # 0x3c
	.short	61                      # 0x3d
	.short	62                      # 0x3e
	.short	62                      # 0x3e
	.short	63                      # 0x3f
	.size	AC_next_state_MPS_64, 128

	.type	AC_next_state_LPS_64,@object # @AC_next_state_LPS_64
	.globl	AC_next_state_LPS_64
	.p2align	4
AC_next_state_LPS_64:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	11                      # 0xb
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	13                      # 0xd
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	16                      # 0x10
	.short	16                      # 0x10
	.short	18                      # 0x12
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	19                      # 0x13
	.short	21                      # 0x15
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	23                      # 0x17
	.short	24                      # 0x18
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	26                      # 0x1a
	.short	26                      # 0x1a
	.short	27                      # 0x1b
	.short	27                      # 0x1b
	.short	28                      # 0x1c
	.short	29                      # 0x1d
	.short	29                      # 0x1d
	.short	30                      # 0x1e
	.short	30                      # 0x1e
	.short	30                      # 0x1e
	.short	31                      # 0x1f
	.short	32                      # 0x20
	.short	32                      # 0x20
	.short	33                      # 0x21
	.short	33                      # 0x21
	.short	33                      # 0x21
	.short	34                      # 0x22
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	36                      # 0x24
	.short	36                      # 0x24
	.short	36                      # 0x24
	.short	37                      # 0x25
	.short	37                      # 0x25
	.short	37                      # 0x25
	.short	38                      # 0x26
	.short	38                      # 0x26
	.short	63                      # 0x3f
	.size	AC_next_state_LPS_64, 128

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"arideco_create_decoding_environment: dep"
	.size	.L.str, 41

	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Error freeing dep (NULL pointer)"
	.size	.L.str.1, 33

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
