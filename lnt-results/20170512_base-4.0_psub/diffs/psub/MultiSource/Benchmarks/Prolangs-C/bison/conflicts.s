	.text
	.file	"conflicts.bc"
	.globl	initialize_conflicts
	.p2align	4, 0x90
	.type	initialize_conflicts,@function
initialize_conflicts:                   # @initialize_conflicts
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	nstates(%rip), %edi
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, conflicts(%rip)
	movl	tokensetsize(%rip), %edi
	shll	$2, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, shiftset(%rip)
	movl	tokensetsize(%rip), %edi
	shll	$2, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, lookaheadset(%rip)
	movl	nstates(%rip), %edi
	shll	$3, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, err_table(%rip)
	movb	$0, any_conflicts(%rip)
	cmpl	$0, nstates(%rip)
	jle	.LBB0_2
	.p2align	4, 0x90
.LBB0_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %edi
	callq	set_conflicts
	incl	%ebx
	cmpl	nstates(%rip), %ebx
	jl	.LBB0_1
.LBB0_2:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end0:
	.size	initialize_conflicts, .Lfunc_end0-initialize_conflicts
	.cfi_endproc

	.globl	set_conflicts
	.p2align	4, 0x90
	.type	set_conflicts,@function
set_conflicts:                          # @set_conflicts
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 112
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movq	consistent(%rip), %rax
	movslq	%r14d, %r12
	cmpb	$0, (%rax,%r12)
	jne	.LBB1_39
# BB#1:                                 # %.preheader
	movl	tokensetsize(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB1_4
# BB#2:                                 # %.lr.ph96
	movq	lookaheadset(%rip), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movl	$0, (%rax,%rdx,4)
	incq	%rdx
	movslq	tokensetsize(%rip), %rcx
	cmpq	%rcx, %rdx
	jl	.LBB1_3
.LBB1_4:                                # %._crit_edge97
	movq	shift_table(%rip), %rax
	movq	(%rax,%r12,8), %rax
	testq	%rax, %rax
	je	.LBB1_10
# BB#5:
	movswq	10(%rax), %rdx
	testq	%rdx, %rdx
	jle	.LBB1_10
# BB#6:                                 # %.lr.ph93
	movq	accessing_symbol(%rip), %rsi
	movq	lookaheadset(%rip), %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_7:                                # =>This Inner Loop Header: Depth=1
	movswq	12(%rax,%rbp,2), %rcx
	movswl	(%rsi,%rcx,2), %ecx
	cmpl	ntokens(%rip), %ecx
	jge	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=1
	movl	$1, %ebx
	shll	%cl, %ebx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%ebx, (%rdi,%rcx,4)
	incq	%rbp
	cmpq	%rdx, %rbp
	jl	.LBB1_7
.LBB1_9:                                # %.loopexit66.loopexit
	movl	tokensetsize(%rip), %ecx
.LBB1_10:                               # %.loopexit66
	movq	lookaheads(%rip), %rax
	movswl	2(%rax,%r12,2), %r15d
	movq	lookaheadset(%rip), %rdx
	movslq	%ecx, %rcx
	leaq	(%rdx,%rcx,4), %rbp
	movzwl	(%rax,%r12,2), %eax
	cmpw	%r15w, %ax
	jge	.LBB1_19
# BB#11:                                # %.lr.ph89.preheader
	movswq	%ax, %rbx
	movslq	%r15d, %r13
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph89
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_14 Depth 2
	movq	rprec(%rip), %rax
	movq	LAruleno(%rip), %rcx
	movswq	(%rcx,%rbx,2), %rcx
	cmpw	$0, (%rax,%rcx,2)
	je	.LBB1_17
# BB#13:                                #   in Loop: Header=BB1_12 Depth=1
	movslq	tokensetsize(%rip), %rcx
	movslq	%ebx, %rax
	imulq	%rcx, %rax
	shlq	$2, %rax
	addq	LA(%rip), %rax
	movq	lookaheadset(%rip), %rcx
	.p2align	4, 0x90
.LBB1_14:                               #   Parent Loop BB1_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbp, %rcx
	jae	.LBB1_17
# BB#15:                                #   in Loop: Header=BB1_14 Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	testl	(%rax), %edx
	leaq	4(%rax), %rax
	je	.LBB1_14
# BB#16:                                #   in Loop: Header=BB1_12 Depth=1
	movl	%r14d, %edi
	movl	%ebx, %esi
	callq	resolve_sr_conflict
.LBB1_17:                               # %.loopexit65
                                        #   in Loop: Header=BB1_12 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jne	.LBB1_12
# BB#18:                                # %._crit_edge90.loopexit
	movq	lookaheads(%rip), %rax
	movzwl	(%rax,%r12,2), %eax
.LBB1_19:                               # %._crit_edge90
	cmpw	%r15w, %ax
	jge	.LBB1_39
# BB#20:                                # %.lr.ph76
	movq	lookaheadset(%rip), %r14
	cmpq	%rbp, %r14
	jae	.LBB1_39
# BB#21:                                # %.lr.ph76.split.us.preheader
	movswl	%ax, %ecx
	movq	%r14, %r9
	notq	%r9
	addq	%rbp, %r9
	shrq	$2, %r9
	leaq	1(%r9), %rsi
	leaq	4(%r14,%r9,4), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movabsq	$9223372036854775800, %rax # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rsi, %rax
	leaq	-8(%rax), %rdx
	shrq	$3, %rdx
	movl	%r9d, %r10d
	andl	$1, %r10d
	leaq	4(%r14), %rdi
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	(%r14,%rax,4), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	andl	$1, %edx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	leaq	48(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB1_22
.LBB1_43:                               # %vector.body.preheader
                                        #   in Loop: Header=BB1_22 Depth=1
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	jne	.LBB1_44
# BB#45:                                # %vector.body.prol
                                        #   in Loop: Header=BB1_22 Depth=1
	movups	(%r11), %xmm0
	movups	16(%r11), %xmm1
	movups	(%r14), %xmm2
	movups	16(%r14), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%r14)
	movups	%xmm3, 16(%r14)
	movl	$8, %edi
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	jne	.LBB1_47
	jmp	.LBB1_49
.LBB1_44:                               #   in Loop: Header=BB1_22 Depth=1
	xorl	%edi, %edi
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	je	.LBB1_49
.LBB1_47:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB1_22 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	subq	%rdi, %rax
	addq	%rdi, %r8
	leaq	48(%r13,%r8,4), %rbx
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB1_48:                               # %vector.body
                                        #   Parent Loop BB1_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	-48(%rdi), %xmm2
	movups	-32(%rdi), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -48(%rdi)
	movups	%xmm3, -32(%rdi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	-16(%rdi), %xmm2
	movups	(%rdi), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -16(%rdi)
	movups	%xmm3, (%rdi)
	addq	$64, %rbx
	addq	$64, %rdi
	addq	$-16, %rax
	jne	.LBB1_48
.LBB1_49:                               # %middle.block
                                        #   in Loop: Header=BB1_22 Depth=1
	cmpq	(%rsp), %rsi            # 8-byte Folded Reload
	je	.LBB1_38
# BB#50:                                #   in Loop: Header=BB1_22 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%r11,%rax,4), %r11
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB1_34
	.p2align	4, 0x90
.LBB1_22:                               # %.lr.ph76.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_27 Depth 2
                                        #     Child Loop BB1_48 Depth 2
                                        #     Child Loop BB1_36 Depth 2
                                        #     Child Loop BB1_51 Depth 2
	movq	LA(%rip), %r13
	movl	tokensetsize(%rip), %eax
	imull	%ecx, %eax
	movslq	%eax, %r8
	leaq	(%r13,%r8,4), %r11
	testq	%r10, %r10
	movq	%r14, %rbx
	movq	%r11, %rax
	jne	.LBB1_26
# BB#23:                                #   in Loop: Header=BB1_22 Depth=1
	leaq	4(%r11), %rax
	movl	(%r14), %edi
	testl	(%r11), %edi
	je	.LBB1_25
# BB#24:                                #   in Loop: Header=BB1_22 Depth=1
	movq	conflicts(%rip), %rdi
	movb	$1, (%rdi,%r12)
	movb	$1, any_conflicts(%rip)
.LBB1_25:                               # %.prol.loopexit
                                        #   in Loop: Header=BB1_22 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB1_26:                               # %.prol.loopexit
                                        #   in Loop: Header=BB1_22 Depth=1
	testq	%r9, %r9
	je	.LBB1_32
	.p2align	4, 0x90
.LBB1_27:                               #   Parent Loop BB1_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %edi
	testl	(%rax), %edi
	je	.LBB1_29
# BB#28:                                #   in Loop: Header=BB1_27 Depth=2
	movq	conflicts(%rip), %rdi
	movb	$1, (%rdi,%r12)
	movb	$1, any_conflicts(%rip)
.LBB1_29:                               # %.backedge.us
                                        #   in Loop: Header=BB1_27 Depth=2
	movl	4(%rbx), %edi
	testl	4(%rax), %edi
	je	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_27 Depth=2
	movq	conflicts(%rip), %rdx
	movb	$1, (%rdx,%r12)
	movb	$1, any_conflicts(%rip)
.LBB1_31:                               # %.backedge.us.1
                                        #   in Loop: Header=BB1_27 Depth=2
	addq	$8, %rbx
	addq	$8, %rax
	cmpq	%rbp, %rbx
	jb	.LBB1_27
.LBB1_32:                               # %.lr.ph72.us.preheader
                                        #   in Loop: Header=BB1_22 Depth=1
	cmpq	$7, %rsi
	jbe	.LBB1_33
# BB#40:                                # %min.iters.checked
                                        #   in Loop: Header=BB1_22 Depth=1
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB1_33
# BB#41:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_22 Depth=1
	leaq	(%r13,%rsi,4), %rax
	leaq	(%rax,%r8,4), %rax
	cmpq	%rax, %r14
	jae	.LBB1_43
# BB#42:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_22 Depth=1
	cmpq	48(%rsp), %r11          # 8-byte Folded Reload
	jae	.LBB1_43
	.p2align	4, 0x90
.LBB1_33:                               #   in Loop: Header=BB1_22 Depth=1
	movq	%r14, %rax
.LBB1_34:                               # %.lr.ph72.us.preheader128
                                        #   in Loop: Header=BB1_22 Depth=1
	leaq	4(%rax), %rbx
	cmpq	%rbx, %rbp
	cmovaq	%rbp, %rbx
	subq	%rax, %rbx
	decq	%rbx
	movl	%ebx, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_37
# BB#35:                                # %.lr.ph72.us.prol.preheader
                                        #   in Loop: Header=BB1_22 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB1_36:                               # %.lr.ph72.us.prol
                                        #   Parent Loop BB1_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r11), %edx
	addq	$4, %r11
	orl	%edx, (%rax)
	addq	$4, %rax
	incq	%rdi
	jne	.LBB1_36
.LBB1_37:                               # %.lr.ph72.us.prol.loopexit
                                        #   in Loop: Header=BB1_22 Depth=1
	cmpq	$12, %rbx
	jb	.LBB1_38
	.p2align	4, 0x90
.LBB1_51:                               # %.lr.ph72.us
                                        #   Parent Loop BB1_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r11), %edi
	orl	%edi, (%rax)
	movl	4(%r11), %edi
	orl	%edi, 4(%rax)
	movl	8(%r11), %edi
	orl	%edi, 8(%rax)
	movl	12(%r11), %edi
	orl	%edi, 12(%rax)
	addq	$16, %rax
	addq	$16, %r11
	cmpq	%rbp, %rax
	jb	.LBB1_51
.LBB1_38:                               # %._crit_edge73.us
                                        #   in Loop: Header=BB1_22 Depth=1
	incl	%ecx
	cmpl	%r15d, %ecx
	jne	.LBB1_22
.LBB1_39:                               # %.loopexit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	set_conflicts, .Lfunc_end1-set_conflicts
	.cfi_endproc

	.globl	resolve_sr_conflict
	.p2align	4, 0x90
	.type	resolve_sr_conflict,@function
resolve_sr_conflict:                    # @resolve_sr_conflict
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 96
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movl	%edi, (%rsp)            # 4-byte Spill
	movslq	ntokens(%rip), %rbx
	leaq	4(%rbx,%rbx), %rdi
	callq	malloc
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	2(%rax), %rcx
	testq	%rbx, %rbx
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	jle	.LBB2_62
# BB#1:                                 # %.lr.ph
	movq	rprec(%rip), %rax
	movq	LAruleno(%rip), %rcx
	movslq	%ebp, %rbp
	movswq	(%rcx,%rbp,2), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, 6(%rsp)            # 2-byte Spill
	movq	lookaheadset(%rip), %r12
	movslq	tokensetsize(%rip), %r14
	imulq	%rbp, %r14
	shlq	$2, %r14
	addq	LA(%rip), %r14
	movslq	(%rsp), %rax            # 4-byte Folded Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$1, %r15d
	xorl	%r13d, %r13d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_50 Depth 2
                                        #     Child Loop BB2_17 Depth 2
	movl	(%r12), %ecx
	movl	%ecx, %edx
	andl	%ebx, %edx
	movl	(%r14), %eax
	testl	%eax, %edx
	je	.LBB2_61
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	sprec(%rip), %rdx
	movzwl	(%rdx,%r13,2), %edx
	testw	%dx, %dx
	je	.LBB2_61
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpw	6(%rsp), %dx            # 2-byte Folded Reload
	jge	.LBB2_24
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpl	$0, verboseflag(%rip)
	je	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	foutput(%rip), %rdi
	movq	LAruleno(%rip), %rax
	movswl	(%rax,%rbp,2), %ecx
	movq	tags(%rip), %rax
	movq	(%rax,%r13,8), %r8
	movl	$.L.str.3, %esi
	movl	$.L.str, %r9d
	xorl	%eax, %eax
	movl	(%rsp), %edx            # 4-byte Reload
	callq	fprintf
	movl	(%r12), %ecx
.LBB2_7:                                #   in Loop: Header=BB2_2 Depth=1
	movl	%ebx, %eax
	notl	%eax
	andl	%ecx, %eax
	movl	%eax, (%r12)
	movq	shift_table(%rip), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.LBB2_61
# BB#8:                                 #   in Loop: Header=BB2_2 Depth=1
	movswq	10(%rdx), %rsi
	testq	%rsi, %rsi
	jle	.LBB2_61
# BB#9:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	accessing_symbol(%rip), %rax
	movl	%esi, %ecx
	testb	$1, %sil
	jne	.LBB2_11
# BB#10:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%esi, %esi
	cmpq	$1, %rcx
	jne	.LBB2_16
	jmp	.LBB2_61
.LBB2_24:                               #   in Loop: Header=BB2_2 Depth=1
	jle	.LBB2_28
# BB#25:                                #   in Loop: Header=BB2_2 Depth=1
	cmpl	$0, verboseflag(%rip)
	je	.LBB2_27
# BB#26:                                #   in Loop: Header=BB2_2 Depth=1
	movq	foutput(%rip), %rdi
	movq	LAruleno(%rip), %rax
	movswl	(%rax,%rbp,2), %ecx
	movq	tags(%rip), %rax
	movq	(%rax,%r13,8), %r8
	movl	$.L.str.3, %esi
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	movl	(%rsp), %edx            # 4-byte Reload
	callq	fprintf
	movl	(%r14), %eax
.LBB2_27:                               #   in Loop: Header=BB2_2 Depth=1
	movl	%ebx, %ecx
	notl	%ecx
	andl	%eax, %ecx
	movl	%ecx, (%r14)
	jmp	.LBB2_61
.LBB2_28:                               #   in Loop: Header=BB2_2 Depth=1
	movq	sassoc(%rip), %rax
	movswl	(%rax,%r13,2), %eax
	cmpl	$3, %eax
	je	.LBB2_35
# BB#29:                                #   in Loop: Header=BB2_2 Depth=1
	cmpl	$2, %eax
	je	.LBB2_33
# BB#30:                                #   in Loop: Header=BB2_2 Depth=1
	cmpl	$1, %eax
	jne	.LBB2_38
# BB#31:                                #   in Loop: Header=BB2_2 Depth=1
	cmpl	$0, verboseflag(%rip)
	je	.LBB2_38
# BB#32:                                #   in Loop: Header=BB2_2 Depth=1
	movq	foutput(%rip), %rdi
	movq	LAruleno(%rip), %rax
	movswl	(%rax,%rbp,2), %ecx
	movq	tags(%rip), %rax
	movq	(%rax,%r13,8), %r8
	movl	$.L.str.3, %esi
	movl	$.L.str.1, %r9d
	jmp	.LBB2_37
.LBB2_11:                               #   in Loop: Header=BB2_2 Depth=1
	movswq	12(%rdx), %rsi
	testq	%rsi, %rsi
	je	.LBB2_14
# BB#12:                                #   in Loop: Header=BB2_2 Depth=1
	movswq	(%rax,%rsi,2), %rsi
	movl	%esi, %esi
	cmpq	%r13, %rsi
	jne	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_2 Depth=1
	movw	$0, 12(%rdx)
.LBB2_14:                               # %.prol.loopexit99
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	$1, %esi
	cmpq	$1, %rcx
	je	.LBB2_61
.LBB2_16:                               # %.lr.ph.i.new
                                        #   in Loop: Header=BB2_2 Depth=1
	subq	%rsi, %rcx
	leaq	14(%rdx,%rsi,2), %rdx
	.p2align	4, 0x90
.LBB2_17:                               #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	-2(%rdx), %rsi
	testq	%rsi, %rsi
	je	.LBB2_20
# BB#18:                                #   in Loop: Header=BB2_17 Depth=2
	movswq	(%rax,%rsi,2), %rsi
	movl	%esi, %esi
	cmpq	%r13, %rsi
	jne	.LBB2_20
# BB#19:                                #   in Loop: Header=BB2_17 Depth=2
	movw	$0, -2(%rdx)
.LBB2_20:                               #   in Loop: Header=BB2_17 Depth=2
	movswq	(%rdx), %rsi
	testq	%rsi, %rsi
	je	.LBB2_23
# BB#21:                                #   in Loop: Header=BB2_17 Depth=2
	movswq	(%rax,%rsi,2), %rsi
	movl	%esi, %esi
	cmpq	%r13, %rsi
	jne	.LBB2_23
# BB#22:                                #   in Loop: Header=BB2_17 Depth=2
	movw	$0, (%rdx)
.LBB2_23:                               #   in Loop: Header=BB2_17 Depth=2
	addq	$4, %rdx
	addq	$-2, %rcx
	jne	.LBB2_17
	jmp	.LBB2_61
.LBB2_35:                               #   in Loop: Header=BB2_2 Depth=1
	cmpl	$0, verboseflag(%rip)
	je	.LBB2_38
# BB#36:                                #   in Loop: Header=BB2_2 Depth=1
	movq	foutput(%rip), %rdi
	movq	LAruleno(%rip), %rax
	movswl	(%rax,%rbp,2), %ecx
	movq	tags(%rip), %rax
	movq	(%rax,%r13,8), %r8
	movl	$.L.str.3, %esi
	movl	$.L.str.2, %r9d
	jmp	.LBB2_37
.LBB2_33:                               #   in Loop: Header=BB2_2 Depth=1
	cmpl	$0, verboseflag(%rip)
	je	.LBB2_38
# BB#34:                                #   in Loop: Header=BB2_2 Depth=1
	movq	foutput(%rip), %rdi
	movq	LAruleno(%rip), %rax
	movswl	(%rax,%rbp,2), %ecx
	movq	tags(%rip), %rax
	movq	(%rax,%r13,8), %r8
	movl	$.L.str.3, %esi
	movl	$.L.str, %r9d
.LBB2_37:                               #   in Loop: Header=BB2_2 Depth=1
	xorl	%eax, %eax
	movl	(%rsp), %edx            # 4-byte Reload
	callq	fprintf
.LBB2_38:                               #   in Loop: Header=BB2_2 Depth=1
	movq	sassoc(%rip), %rcx
	movzwl	(%rcx,%r13,2), %edx
	movl	%ebx, %eax
	notl	%eax
	cmpl	$1, %edx
	jne	.LBB2_40
# BB#39:                                # %.thread97
                                        #   in Loop: Header=BB2_2 Depth=1
	andl	%eax, (%r14)
	jmp	.LBB2_61
.LBB2_40:                               #   in Loop: Header=BB2_2 Depth=1
	andl	%eax, (%r12)
	movq	shift_table(%rip), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rsi,%rdi,8), %rdi
	movq	%rbp, %r8
	testq	%rdi, %rdi
	je	.LBB2_58
# BB#41:                                #   in Loop: Header=BB2_2 Depth=1
	movswq	10(%rdi), %rbp
	testq	%rbp, %rbp
	jle	.LBB2_58
# BB#42:                                # %.lr.ph.i82
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	accessing_symbol(%rip), %rdx
	movl	%ebp, %esi
	testb	$1, %bpl
	jne	.LBB2_44
# BB#43:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%ebp, %ebp
	cmpq	$1, %rsi
	jne	.LBB2_49
	jmp	.LBB2_57
.LBB2_44:                               #   in Loop: Header=BB2_2 Depth=1
	movswq	12(%rdi), %rbp
	testq	%rbp, %rbp
	je	.LBB2_47
# BB#45:                                #   in Loop: Header=BB2_2 Depth=1
	movswq	(%rdx,%rbp,2), %rbp
	movl	%ebp, %ebp
	cmpq	%r13, %rbp
	jne	.LBB2_47
# BB#46:                                #   in Loop: Header=BB2_2 Depth=1
	movw	$0, 12(%rdi)
.LBB2_47:                               # %.prol.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	$1, %ebp
	cmpq	$1, %rsi
	je	.LBB2_57
.LBB2_49:                               # %.lr.ph.i82.new
                                        #   in Loop: Header=BB2_2 Depth=1
	subq	%rbp, %rsi
	leaq	14(%rdi,%rbp,2), %rdi
	.p2align	4, 0x90
.LBB2_50:                               #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	-2(%rdi), %rbp
	testq	%rbp, %rbp
	je	.LBB2_53
# BB#51:                                #   in Loop: Header=BB2_50 Depth=2
	movswq	(%rdx,%rbp,2), %rbp
	movl	%ebp, %ebp
	cmpq	%r13, %rbp
	jne	.LBB2_53
# BB#52:                                #   in Loop: Header=BB2_50 Depth=2
	movw	$0, -2(%rdi)
.LBB2_53:                               #   in Loop: Header=BB2_50 Depth=2
	movswq	(%rdi), %rbp
	testq	%rbp, %rbp
	je	.LBB2_56
# BB#54:                                #   in Loop: Header=BB2_50 Depth=2
	movswq	(%rdx,%rbp,2), %rbp
	movl	%ebp, %ebp
	cmpq	%r13, %rbp
	jne	.LBB2_56
# BB#55:                                #   in Loop: Header=BB2_50 Depth=2
	movw	$0, (%rdi)
.LBB2_56:                               #   in Loop: Header=BB2_50 Depth=2
	addq	$4, %rdi
	addq	$-2, %rsi
	jne	.LBB2_50
.LBB2_57:                               # %flush_shift.exit86.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movw	(%rcx,%r13,2), %dx
.LBB2_58:                               # %flush_shift.exit86
                                        #   in Loop: Header=BB2_2 Depth=1
	movzwl	%dx, %ecx
	cmpl	$2, %ecx
	movq	%r8, %rbp
	je	.LBB2_61
# BB#59:                                #   in Loop: Header=BB2_2 Depth=1
	andl	%eax, (%r14)
	cmpl	$3, %ecx
	jne	.LBB2_61
# BB#60:                                #   in Loop: Header=BB2_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movw	%r13w, (%rax)
	addq	$2, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB2_61:                               # %flush_shift.exit
                                        #   in Loop: Header=BB2_2 Depth=1
	addl	%ebx, %ebx
	leaq	4(%r12), %rax
	leaq	4(%r14), %rcx
	testl	%ebx, %ebx
	cmoveq	%rcx, %r14
	cmoveq	%rax, %r12
	cmovel	%r15d, %ebx
	incq	%r13
	movslq	ntokens(%rip), %rax
	cmpq	%rax, %r13
	jl	.LBB2_2
.LBB2_62:                               # %._crit_edge
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%ebp, %eax
	subl	16(%rsp), %eax          # 4-byte Folded Reload
	shrl	%eax
	movq	32(%rsp), %rbx          # 8-byte Reload
	movw	%ax, (%rbx)
	testw	%ax, %ax
	je	.LBB2_63
# BB#64:
	subq	%rbx, %rbp
	xorl	%eax, %eax
	movl	%ebp, %edi
	callq	mallocate
	movq	err_table(%rip), %rcx
	movslq	(%rsp), %rdx            # 4-byte Folded Reload
	movq	%rax, (%rcx,%rdx,8)
	movq	err_table(%rip), %rax
	movq	(%rax,%rdx,8), %rsi
	movslq	%ebp, %rdx
	movq	%rbx, %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	bcopy                   # TAILCALL
.LBB2_63:
	movq	err_table(%rip), %rax
	movslq	(%rsp), %rcx            # 4-byte Folded Reload
	movq	$0, (%rax,%rcx,8)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	resolve_sr_conflict, .Lfunc_end2-resolve_sr_conflict
	.cfi_endproc

	.globl	log_resolution
	.p2align	4, 0x90
	.type	log_resolution,@function
log_resolution:                         # @log_resolution
	.cfi_startproc
# BB#0:
	movq	%rcx, %r9
	movl	%edi, %r10d
	movq	foutput(%rip), %rdi
	movq	LAruleno(%rip), %rax
	movslq	%esi, %rcx
	movswl	(%rax,%rcx,2), %ecx
	movq	tags(%rip), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %r8
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movl	%r10d, %edx
	jmp	fprintf                 # TAILCALL
.Lfunc_end3:
	.size	log_resolution, .Lfunc_end3-log_resolution
	.cfi_endproc

	.globl	flush_shift
	.p2align	4, 0x90
	.type	flush_shift,@function
flush_shift:                            # @flush_shift
	.cfi_startproc
# BB#0:
	movq	shift_table(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.LBB4_17
# BB#1:
	movswq	10(%rdx), %rdi
	testq	%rdi, %rdi
	jle	.LBB4_17
# BB#2:                                 # %.lr.ph
	movq	accessing_symbol(%rip), %rax
	movl	%edi, %ecx
	testb	$1, %dil
	jne	.LBB4_4
# BB#3:
	xorl	%edi, %edi
	cmpq	$1, %rcx
	jne	.LBB4_9
	jmp	.LBB4_17
.LBB4_4:
	movswq	12(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_7
# BB#5:
	movswl	(%rax,%rdi,2), %edi
	cmpl	%esi, %edi
	jne	.LBB4_7
# BB#6:
	movw	$0, 12(%rdx)
.LBB4_7:                                # %.prol.loopexit
	movl	$1, %edi
	cmpq	$1, %rcx
	je	.LBB4_17
.LBB4_9:                                # %.lr.ph.new
	subq	%rdi, %rcx
	leaq	14(%rdx,%rdi,2), %rdx
	.p2align	4, 0x90
.LBB4_10:                               # =>This Inner Loop Header: Depth=1
	movswq	-2(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_13
# BB#11:                                #   in Loop: Header=BB4_10 Depth=1
	movswl	(%rax,%rdi,2), %edi
	cmpl	%esi, %edi
	jne	.LBB4_13
# BB#12:                                #   in Loop: Header=BB4_10 Depth=1
	movw	$0, -2(%rdx)
.LBB4_13:                               #   in Loop: Header=BB4_10 Depth=1
	movswq	(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_16
# BB#14:                                #   in Loop: Header=BB4_10 Depth=1
	movswl	(%rax,%rdi,2), %edi
	cmpl	%esi, %edi
	jne	.LBB4_16
# BB#15:                                #   in Loop: Header=BB4_10 Depth=1
	movw	$0, (%rdx)
.LBB4_16:                               #   in Loop: Header=BB4_10 Depth=1
	addq	$4, %rdx
	addq	$-2, %rcx
	jne	.LBB4_10
.LBB4_17:                               # %.loopexit
	retq
.Lfunc_end4:
	.size	flush_shift, .Lfunc_end4-flush_shift
	.cfi_endproc

	.globl	conflict_log
	.p2align	4, 0x90
	.type	conflict_log,@function
conflict_log:                           # @conflict_log
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 80
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movl	$0, src_total(%rip)
	movl	$0, rrc_total(%rip)
	movl	nstates(%rip), %eax
	testl	%eax, %eax
	jle	.LBB5_19
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_8 Depth 2
                                        #       Child Loop BB5_10 Depth 3
                                        #       Child Loop BB5_13 Depth 3
	movq	conflicts(%rip), %rcx
	cmpb	$0, (%rcx,%r15)
	je	.LBB5_3
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=1
	movl	%r15d, %edi
	callq	count_sr_conflicts
	movl	$0, rrc_count(%rip)
	movq	lookaheads(%rip), %rax
	movswl	(%rax,%r15,2), %r10d
	movswl	2(%rax,%r15,2), %eax
	movl	%eax, %r12d
	subl	%r10d, %r12d
	xorl	%r13d, %r13d
	cmpl	$2, %r12d
	jl	.LBB5_17
# BB#5:                                 #   in Loop: Header=BB5_2 Depth=1
	movl	ntokens(%rip), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	testl	%ecx, %ecx
	jle	.LBB5_17
# BB#6:                                 # %.preheader.lr.ph.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpw	%r10w, %ax
	jle	.LBB5_17
# BB#7:                                 # %.preheader.us.i.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	movslq	tokensetsize(%rip), %rbp
	movl	%ebp, %eax
	imull	%r10d, %eax
	movslq	%eax, %r9
	shlq	$2, %r9
	addq	LA(%rip), %r9
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %r11d
	subl	%r10d, %r11d
	andl	$3, %r12d
	shlq	$2, %rbp
	movl	%r12d, %eax
	negl	%eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	$1, %esi
	xorl	%r13d, %r13d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB5_8:                                # %.preheader.us.i
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_10 Depth 3
                                        #       Child Loop BB5_13 Depth 3
	testl	%r12d, %r12d
	movl	%r10d, %eax
	movq	%r9, %rcx
	movl	$0, %edx
	je	.LBB5_11
# BB#9:                                 # %.prol.preheader
                                        #   in Loop: Header=BB5_8 Depth=2
	xorl	%edx, %edx
	movl	8(%rsp), %ebx           # 4-byte Reload
	movq	%r9, %rcx
	movl	%r10d, %eax
	.p2align	4, 0x90
.LBB5_10:                               #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rcx), %edi
	andl	%esi, %edi
	cmpl	$1, %edi
	sbbl	$-1, %edx
	incl	%eax
	addq	%rbp, %rcx
	incl	%ebx
	jne	.LBB5_10
.LBB5_11:                               # %.prol.loopexit
                                        #   in Loop: Header=BB5_8 Depth=2
	cmpl	$3, %r11d
	jb	.LBB5_14
# BB#12:                                # %.preheader.us.i.new
                                        #   in Loop: Header=BB5_8 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%edi, %r14d
	subl	%eax, %r14d
	.p2align	4, 0x90
.LBB5_13:                               #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rcx), %eax
	andl	%esi, %eax
	cmpl	$1, %eax
	sbbl	$-1, %edx
	movl	(%rcx,%rbp), %eax
	andl	%esi, %eax
	addq	%rbp, %rcx
	cmpl	$1, %eax
	sbbl	$-1, %edx
	movl	(%rbp,%rcx), %eax
	andl	%esi, %eax
	addq	%rbp, %rcx
	cmpl	$1, %eax
	sbbl	$-1, %edx
	movl	(%rbp,%rcx), %eax
	andl	%esi, %eax
	addq	%rbp, %rcx
	cmpl	$1, %eax
	sbbl	$-1, %edx
	addq	%rbp, %rcx
	addl	$-4, %r14d
	jne	.LBB5_13
.LBB5_14:                               # %._crit_edge.us.i
                                        #   in Loop: Header=BB5_8 Depth=2
	cmpl	$1, %edx
	jle	.LBB5_16
# BB#15:                                #   in Loop: Header=BB5_8 Depth=2
	incl	%r13d
	movl	%r13d, rrc_count(%rip)
.LBB5_16:                               #   in Loop: Header=BB5_8 Depth=2
	addl	%esi, %esi
	leaq	4(%r9), %rax
	testl	%esi, %esi
	movl	$1, %ecx
	cmovel	%ecx, %esi
	cmoveq	%rax, %r9
	incl	%r8d
	cmpl	12(%rsp), %r8d          # 4-byte Folded Reload
	jne	.LBB5_8
	.p2align	4, 0x90
.LBB5_17:                               # %count_rr_conflicts.exit
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	src_count(%rip), %eax
	addl	%eax, src_total(%rip)
	incq	%r15
	addl	%r13d, rrc_total(%rip)
	movl	nstates(%rip), %eax
	jmp	.LBB5_18
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph._crit_edge
                                        #   in Loop: Header=BB5_2 Depth=1
	incq	%r15
.LBB5_18:                               #   in Loop: Header=BB5_2 Depth=1
	movslq	%eax, %rcx
	cmpq	%rcx, %r15
	jl	.LBB5_2
.LBB5_19:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	total_conflicts         # TAILCALL
.Lfunc_end5:
	.size	conflict_log, .Lfunc_end5-conflict_log
	.cfi_endproc

	.globl	count_sr_conflicts
	.p2align	4, 0x90
	.type	count_sr_conflicts,@function
count_sr_conflicts:                     # @count_sr_conflicts
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 56
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movl	$0, src_count(%rip)
	movq	shift_table(%rip), %rcx
	movslq	%edi, %r8
	movq	(%rcx,%r8,8), %rdx
	testq	%rdx, %rdx
	je	.LBB6_67
# BB#1:                                 # %.preheader
	movl	tokensetsize(%rip), %esi
	testl	%esi, %esi
	jle	.LBB6_4
# BB#2:                                 # %.lr.ph81
	movq	shiftset(%rip), %rcx
	movq	lookaheadset(%rip), %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_3:                                # =>This Inner Loop Header: Depth=1
	movl	$0, (%rcx,%rbp,4)
	movl	$0, (%rdi,%rbp,4)
	incq	%rbp
	movslq	tokensetsize(%rip), %rsi
	cmpq	%rsi, %rbp
	jl	.LBB6_3
.LBB6_4:                                # %._crit_edge82
	movswq	10(%rdx), %rdi
	testq	%rdi, %rdi
	jle	.LBB6_11
# BB#5:                                 # %.lr.ph77
	movq	accessing_symbol(%rip), %rsi
	movq	shiftset(%rip), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_6:                                # =>This Inner Loop Header: Depth=1
	movswq	12(%rdx,%rbx,2), %rcx
	testq	%rcx, %rcx
	je	.LBB6_9
# BB#7:                                 #   in Loop: Header=BB6_6 Depth=1
	movswl	(%rsi,%rcx,2), %ecx
	cmpl	ntokens(%rip), %ecx
	jge	.LBB6_10
# BB#8:                                 #   in Loop: Header=BB6_6 Depth=1
	movl	$1, %eax
	shll	%cl, %eax
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%eax, (%rbp,%rcx,4)
.LBB6_9:                                #   in Loop: Header=BB6_6 Depth=1
	incq	%rbx
	cmpq	%rdi, %rbx
	jl	.LBB6_6
.LBB6_10:                               # %._crit_edge78.loopexit
	movl	tokensetsize(%rip), %esi
.LBB6_11:                               # %._crit_edge78
	movq	lookaheads(%rip), %rax
	movswl	2(%rax,%r8,2), %r13d
	movq	lookaheadset(%rip), %rbp
	movslq	%esi, %rcx
	leaq	(%rbp,%rcx,4), %rcx
	movswl	(%rax,%r8,2), %edx
	cmpw	%r13w, %dx
	jge	.LBB6_35
# BB#12:                                # %.lr.ph73
	testl	%esi, %esi
	jle	.LBB6_55
# BB#13:                                # %.lr.ph73.split.us.preheader
	movq	LA(%rip), %r9
	leaq	4(%rbp), %rax
	cmpq	%rax, %rcx
	cmovaq	%rcx, %rax
	movq	%rbp, %rdi
	notq	%rdi
	addq	%rax, %rdi
	shrq	$2, %rdi
	leaq	4(%rbp,%rdi,4), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	leaq	4(%r9,%rdi,4), %r11
	leaq	1(%rdi), %r14
	movq	%r14, %r10
	movabsq	$9223372036854775800, %rax # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rax, %r10
	leaq	-8(%r10), %r15
	shrq	$3, %r15
	leaq	(%rbp,%r10,4), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movl	%r15d, %eax
	andl	$1, %eax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	leaq	48(%r9), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	leaq	48(%rbp), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movl	%esi, %eax
	jmp	.LBB6_15
	.p2align	4, 0x90
.LBB6_14:                               # %._crit_edge70.us..lr.ph73.split.us_crit_edge
                                        #   in Loop: Header=BB6_15 Depth=1
	movl	tokensetsize(%rip), %eax
.LBB6_15:                               # %.lr.ph73.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_26 Depth 2
                                        #     Child Loop BB6_31 Depth 2
                                        #     Child Loop BB6_33 Depth 2
	imull	%edx, %eax
	cmpq	$8, %r14
	cltq
	leaq	(%r9,%rax,4), %r8
	jae	.LBB6_17
# BB#16:                                #   in Loop: Header=BB6_15 Depth=1
	movq	%rbp, %rax
	jmp	.LBB6_29
	.p2align	4, 0x90
.LBB6_17:                               # %min.iters.checked
                                        #   in Loop: Header=BB6_15 Depth=1
	testq	%r10, %r10
	je	.LBB6_21
# BB#18:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_15 Depth=1
	leaq	(%r11,%rax,4), %rdi
	cmpq	%rdi, %rbp
	jae	.LBB6_22
# BB#19:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_15 Depth=1
	cmpq	-8(%rsp), %r8           # 8-byte Folded Reload
	jae	.LBB6_22
.LBB6_21:                               #   in Loop: Header=BB6_15 Depth=1
	movq	%rbp, %rax
.LBB6_29:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB6_15 Depth=1
	leaq	4(%rax), %r12
	cmpq	%r12, %rcx
	cmovaq	%rcx, %r12
	subq	%rax, %r12
	decq	%r12
	movl	%r12d, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB6_32
# BB#30:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB6_15 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB6_31:                               # %scalar.ph.prol
                                        #   Parent Loop BB6_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r8), %ebx
	addq	$4, %r8
	orl	%ebx, (%rax)
	addq	$4, %rax
	incq	%rdi
	jne	.LBB6_31
.LBB6_32:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB6_15 Depth=1
	cmpq	$12, %r12
	jb	.LBB6_34
	.p2align	4, 0x90
.LBB6_33:                               # %scalar.ph
                                        #   Parent Loop BB6_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r8), %edi
	orl	%edi, (%rax)
	movl	4(%r8), %edi
	orl	%edi, 4(%rax)
	movl	8(%r8), %edi
	orl	%edi, 8(%rax)
	movl	12(%r8), %edi
	orl	%edi, 12(%rax)
	addq	$16, %rax
	addq	$16, %r8
	cmpq	%rcx, %rax
	jb	.LBB6_33
.LBB6_34:                               # %._crit_edge70.us
                                        #   in Loop: Header=BB6_15 Depth=1
	incl	%edx
	cmpl	%r13d, %edx
	jne	.LBB6_14
	jmp	.LBB6_35
.LBB6_22:                               # %vector.body.preheader
                                        #   in Loop: Header=BB6_15 Depth=1
	cmpq	$0, -16(%rsp)           # 8-byte Folded Reload
	jne	.LBB6_24
# BB#23:                                # %vector.body.prol
                                        #   in Loop: Header=BB6_15 Depth=1
	movups	(%r8), %xmm0
	movups	16(%r8), %xmm1
	movups	(%rbp), %xmm2
	movups	16(%rbp), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rbp)
	movups	%xmm3, 16(%rbp)
	movl	$8, %r12d
	testq	%r15, %r15
	jne	.LBB6_25
	jmp	.LBB6_27
.LBB6_24:                               #   in Loop: Header=BB6_15 Depth=1
	xorl	%r12d, %r12d
	testq	%r15, %r15
	je	.LBB6_27
.LBB6_25:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB6_15 Depth=1
	movq	%r10, %rdi
	subq	%r12, %rdi
	addq	%r12, %rax
	movq	-24(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rax,4), %rax
	movq	-32(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%r12,4), %rbx
	.p2align	4, 0x90
.LBB6_26:                               # %vector.body
                                        #   Parent Loop BB6_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rax), %xmm0
	movups	-32(%rax), %xmm1
	movups	-48(%rbx), %xmm2
	movups	-32(%rbx), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -48(%rbx)
	movups	%xmm3, -32(%rbx)
	movups	-16(%rax), %xmm0
	movups	(%rax), %xmm1
	movups	-16(%rbx), %xmm2
	movups	(%rbx), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -16(%rbx)
	movups	%xmm3, (%rbx)
	addq	$64, %rax
	addq	$64, %rbx
	addq	$-16, %rdi
	jne	.LBB6_26
.LBB6_27:                               # %middle.block
                                        #   in Loop: Header=BB6_15 Depth=1
	cmpq	%r10, %r14
	je	.LBB6_34
# BB#28:                                #   in Loop: Header=BB6_15 Depth=1
	leaq	(%r8,%r10,4), %r8
	movq	-40(%rsp), %rax         # 8-byte Reload
	jmp	.LBB6_29
.LBB6_35:                               # %._crit_edge74
	testl	%esi, %esi
	jle	.LBB6_55
# BB#36:                                # %.lr.ph65.preheader
	movq	shiftset(%rip), %rdx
	leaq	4(%rbp), %rax
	cmpq	%rax, %rcx
	movq	%rax, %rsi
	cmovaq	%rcx, %rsi
	movq	%rbp, %rdi
	notq	%rdi
	addq	%rdi, %rsi
	shrq	$2, %rsi
	incq	%rsi
	cmpq	$8, %rsi
	jae	.LBB6_38
# BB#37:
	movq	%rbp, %rsi
	jmp	.LBB6_50
.LBB6_38:                               # %min.iters.checked117
	movabsq	$9223372036854775800, %r8 # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rsi, %r8
	je	.LBB6_42
# BB#39:                                # %vector.memcheck135
	cmpq	%rax, %rcx
	cmovaq	%rcx, %rax
	addq	%rdi, %rax
	andq	$-4, %rax
	leaq	4(%rdx,%rax), %rdi
	cmpq	%rdi, %rbp
	jae	.LBB6_43
# BB#40:                                # %vector.memcheck135
	leaq	4(%rbp,%rax), %rax
	cmpq	%rax, %rdx
	jae	.LBB6_43
.LBB6_42:
	movq	%rbp, %rsi
.LBB6_50:                               # %.lr.ph65.preheader152
	leaq	4(%rsi), %rdi
	cmpq	%rdi, %rcx
	cmovaq	%rcx, %rdi
	movq	%rsi, %rax
	notq	%rax
	addq	%rdi, %rax
	movl	%eax, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB6_53
# BB#51:                                # %.lr.ph65.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB6_52:                               # %.lr.ph65.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx), %ebx
	addq	$4, %rdx
	andl	%ebx, (%rsi)
	addq	$4, %rsi
	incq	%rdi
	jne	.LBB6_52
.LBB6_53:                               # %.lr.ph65.prol.loopexit
	cmpq	$12, %rax
	jb	.LBB6_55
	.p2align	4, 0x90
.LBB6_54:                               # %.lr.ph65
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx), %eax
	andl	%eax, (%rsi)
	movl	4(%rdx), %eax
	andl	%eax, 4(%rsi)
	movl	8(%rdx), %eax
	andl	%eax, 8(%rsi)
	movl	12(%rdx), %eax
	andl	%eax, 12(%rsi)
	addq	$16, %rsi
	addq	$16, %rdx
	cmpq	%rcx, %rsi
	jb	.LBB6_54
.LBB6_55:                               # %._crit_edge
	movl	ntokens(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB6_67
# BB#56:                                # %.lr.ph.preheader
	movl	$1, %edx
	xorl	%esi, %esi
	testb	$1, %cl
	jne	.LBB6_58
# BB#57:
	xorl	%eax, %eax
	cmpl	$1, %ecx
	jne	.LBB6_61
	jmp	.LBB6_67
.LBB6_58:                               # %.lr.ph.prol
	movl	$2, %edx
	movl	$1, %eax
	testb	$1, (%rbp)
	je	.LBB6_60
# BB#59:
	movl	$1, src_count(%rip)
	movl	$1, %esi
	movl	$1, %eax
.LBB6_60:                               # %.lr.ph.prol.loopexit
	cmpl	$1, %ecx
	je	.LBB6_67
.LBB6_61:                               # %.lr.ph.preheader.new
	movl	$1, %edi
	.p2align	4, 0x90
.LBB6_62:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testl	(%rbp), %edx
	je	.LBB6_64
# BB#63:                                #   in Loop: Header=BB6_62 Depth=1
	incl	%esi
	movl	%esi, src_count(%rip)
.LBB6_64:                               # %.lr.ph.1153
                                        #   in Loop: Header=BB6_62 Depth=1
	addl	%edx, %edx
	leaq	4(%rbp), %rbx
	testl	%edx, %edx
	cmovel	%edi, %edx
	cmovneq	%rbp, %rbx
	testl	(%rbx), %edx
	je	.LBB6_66
# BB#65:                                #   in Loop: Header=BB6_62 Depth=1
	incl	%esi
	movl	%esi, src_count(%rip)
.LBB6_66:                               #   in Loop: Header=BB6_62 Depth=1
	addl	%edx, %edx
	leaq	4(%rbx), %rbp
	testl	%edx, %edx
	cmovel	%edi, %edx
	cmovneq	%rbx, %rbp
	addl	$2, %eax
	cmpl	%ecx, %eax
	jl	.LBB6_62
.LBB6_67:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_43:                               # %vector.body108.preheader
	leaq	-8(%r8), %rdi
	movq	%rdi, %rax
	shrq	$3, %rax
	btl	$3, %edi
	jb	.LBB6_45
# BB#44:                                # %vector.body108.prol
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movups	(%rbp), %xmm2
	movups	16(%rbp), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rbp)
	movups	%xmm3, 16(%rbp)
	movl	$8, %ebx
	testq	%rax, %rax
	jne	.LBB6_46
	jmp	.LBB6_48
.LBB6_45:
	xorl	%ebx, %ebx
	testq	%rax, %rax
	je	.LBB6_48
.LBB6_46:                               # %vector.body108.preheader.new
	movq	%r8, %rax
	subq	%rbx, %rax
	leaq	48(%rdx,%rbx,4), %rdi
	leaq	48(%rbp,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB6_47:                               # %vector.body108
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	-48(%rbx), %xmm2
	movups	-32(%rbx), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -48(%rbx)
	movups	%xmm3, -32(%rbx)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	-16(%rbx), %xmm2
	movups	(%rbx), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -16(%rbx)
	movups	%xmm3, (%rbx)
	addq	$64, %rdi
	addq	$64, %rbx
	addq	$-16, %rax
	jne	.LBB6_47
.LBB6_48:                               # %middle.block109
	movq	%r8, %rax
	cmpq	%r8, %rsi
	je	.LBB6_55
# BB#49:
	leaq	(%rbp,%rax,4), %rsi
	leaq	(%rdx,%rax,4), %rdx
	jmp	.LBB6_50
.Lfunc_end6:
	.size	count_sr_conflicts, .Lfunc_end6-count_sr_conflicts
	.cfi_endproc

	.globl	count_rr_conflicts
	.p2align	4, 0x90
	.type	count_rr_conflicts,@function
count_rr_conflicts:                     # @count_rr_conflicts
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movl	$0, rrc_count(%rip)
	movq	lookaheads(%rip), %rax
	movslq	%edi, %rcx
	movswl	(%rax,%rcx,2), %r11d
	movswl	2(%rax,%rcx,2), %eax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movl	%eax, %r12d
	subl	%r11d, %r12d
	cmpl	$2, %r12d
	jl	.LBB7_13
# BB#1:
	movl	ntokens(%rip), %r9d
	testl	%r9d, %r9d
	jle	.LBB7_13
# BB#2:                                 # %.preheader.lr.ph
	cmpw	%r11w, -16(%rsp)        # 2-byte Folded Reload
	jle	.LBB7_13
# BB#3:                                 # %.preheader.us.preheader
	movslq	tokensetsize(%rip), %rdi
	movl	%edi, %eax
	imull	%r11d, %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	addq	LA(%rip), %rdx
	movq	-16(%rsp), %rax         # 8-byte Reload
	leal	-1(%rax), %r14d
	subl	%r11d, %r14d
	andl	$3, %r12d
	shlq	$2, %rdi
	movl	%r12d, %eax
	negl	%eax
	movl	%eax, -4(%rsp)          # 4-byte Spill
	xorl	%r15d, %r15d
	xorl	%r8d, %r8d
	movl	$1, %esi
	.p2align	4, 0x90
.LBB7_4:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_6 Depth 2
                                        #     Child Loop BB7_9 Depth 2
	testl	%r12d, %r12d
	movl	%r11d, %r13d
	movq	%rdx, %rcx
	movl	$0, %ebx
	je	.LBB7_7
# BB#5:                                 # %.prol.preheader
                                        #   in Loop: Header=BB7_4 Depth=1
	xorl	%ebx, %ebx
	movl	-4(%rsp), %eax          # 4-byte Reload
	movq	%rdx, %rcx
	movl	%r11d, %r13d
	.p2align	4, 0x90
.LBB7_6:                                #   Parent Loop BB7_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %r10d
	andl	%esi, %r10d
	cmpl	$1, %r10d
	sbbl	$-1, %ebx
	incl	%r13d
	addq	%rdi, %rcx
	incl	%eax
	jne	.LBB7_6
.LBB7_7:                                # %.prol.loopexit
                                        #   in Loop: Header=BB7_4 Depth=1
	cmpl	$3, %r14d
	jb	.LBB7_10
# BB#8:                                 # %.preheader.us.new
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	-16(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%r13d, %eax
	.p2align	4, 0x90
.LBB7_9:                                #   Parent Loop BB7_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %ebp
	andl	%esi, %ebp
	cmpl	$1, %ebp
	sbbl	$-1, %ebx
	movl	(%rcx,%rdi), %ebp
	andl	%esi, %ebp
	addq	%rdi, %rcx
	cmpl	$1, %ebp
	sbbl	$-1, %ebx
	movl	(%rdi,%rcx), %ebp
	andl	%esi, %ebp
	addq	%rdi, %rcx
	cmpl	$1, %ebp
	sbbl	$-1, %ebx
	movl	(%rdi,%rcx), %ebp
	andl	%esi, %ebp
	addq	%rdi, %rcx
	cmpl	$1, %ebp
	sbbl	$-1, %ebx
	addq	%rdi, %rcx
	addl	$-4, %eax
	jne	.LBB7_9
.LBB7_10:                               # %._crit_edge.us
                                        #   in Loop: Header=BB7_4 Depth=1
	cmpl	$1, %ebx
	jle	.LBB7_12
# BB#11:                                #   in Loop: Header=BB7_4 Depth=1
	incl	%r15d
	movl	%r15d, rrc_count(%rip)
.LBB7_12:                               #   in Loop: Header=BB7_4 Depth=1
	addl	%esi, %esi
	leaq	4(%rdx), %rax
	testl	%esi, %esi
	movl	$1, %ecx
	cmovel	%ecx, %esi
	cmoveq	%rax, %rdx
	incl	%r8d
	cmpl	%r9d, %r8d
	jl	.LBB7_4
.LBB7_13:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	count_rr_conflicts, .Lfunc_end7-count_rr_conflicts
	.cfi_endproc

	.globl	total_conflicts
	.p2align	4, 0x90
	.type	total_conflicts,@function
total_conflicts:                        # @total_conflicts
	.cfi_startproc
# BB#0:
	movl	src_total(%rip), %eax
	cmpl	expected_conflicts(%rip), %eax
	jne	.LBB8_2
# BB#1:
	movl	rrc_total(%rip), %eax
	testl	%eax, %eax
	jne	.LBB8_2
# BB#14:
	retq
.LBB8_2:
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 32
.Lcfi68:
	.cfi_offset %rbx, -24
.Lcfi69:
	.cfi_offset %r14, -16
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %r14
	movq	infile(%rip), %rbx
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rdx
	cmoveq	%rbx, %rdx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	src_total(%rip), %edx
	cmpl	$1, %edx
	je	.LBB8_3
# BB#4:
	cmpl	$2, %edx
	jge	.LBB8_5
.LBB8_6:
	movl	rrc_total(%rip), %edx
	cmpl	$0, src_total(%rip)
	jle	.LBB8_9
# BB#7:
	testl	%edx, %edx
	jg	.LBB8_8
.LBB8_9:
	cmpl	$1, %edx
	je	.LBB8_10
.LBB8_11:
	cmpl	$2, %edx
	jge	.LBB8_12
.LBB8_13:
	movq	stderr(%rip), %rsi
	movl	$46, %edi
	callq	_IO_putc
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_IO_putc                # TAILCALL
.LBB8_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB8_6
.LBB8_5:
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB8_6
.LBB8_8:
	movq	stderr(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movl	rrc_total(%rip), %edx
	cmpl	$1, %edx
	jne	.LBB8_11
.LBB8_10:
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB8_13
.LBB8_12:
	movq	stderr(%rip), %rdi
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB8_13
.Lfunc_end8:
	.size	total_conflicts, .Lfunc_end8-total_conflicts
	.cfi_endproc

	.globl	verbose_conflict_log
	.p2align	4, 0x90
	.type	verbose_conflict_log,@function
verbose_conflict_log:                   # @verbose_conflict_log
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 80
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movl	$0, src_total(%rip)
	movl	$0, rrc_total(%rip)
	movl	nstates(%rip), %eax
	testl	%eax, %eax
	jle	.LBB9_30
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_8 Depth 2
                                        #       Child Loop BB9_10 Depth 3
                                        #       Child Loop BB9_13 Depth 3
	movq	conflicts(%rip), %rcx
	cmpb	$0, (%rcx,%rbx)
	je	.LBB9_3
# BB#4:                                 #   in Loop: Header=BB9_2 Depth=1
	movl	%ebx, %edi
	callq	count_sr_conflicts
	movl	$0, rrc_count(%rip)
	movq	lookaheads(%rip), %rax
	movswl	(%rax,%rbx,2), %r9d
	movswl	2(%rax,%rbx,2), %eax
	movl	%eax, %r12d
	subl	%r9d, %r12d
	xorl	%r13d, %r13d
	cmpl	$2, %r12d
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	jl	.LBB9_17
# BB#5:                                 #   in Loop: Header=BB9_2 Depth=1
	movl	ntokens(%rip), %r10d
	testl	%r10d, %r10d
	jle	.LBB9_17
# BB#6:                                 # %.preheader.lr.ph.i
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpw	%r9w, %ax
	jle	.LBB9_17
# BB#7:                                 # %.preheader.us.i.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movslq	tokensetsize(%rip), %rbp
	movl	%ebp, %eax
	imull	%r9d, %eax
	movslq	%eax, %r15
	shlq	$2, %r15
	addq	LA(%rip), %r15
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	-1(%rax), %r11d
	subl	%r9d, %r11d
	andl	$3, %r12d
	shlq	$2, %rbp
	movl	%r12d, %eax
	negl	%eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	$1, %esi
	xorl	%r13d, %r13d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB9_8:                                # %.preheader.us.i
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_10 Depth 3
                                        #       Child Loop BB9_13 Depth 3
	testl	%r12d, %r12d
	movl	%r9d, %eax
	movq	%r15, %rcx
	movl	$0, %edx
	je	.LBB9_11
# BB#9:                                 # %.prol.preheader
                                        #   in Loop: Header=BB9_8 Depth=2
	xorl	%edx, %edx
	movl	4(%rsp), %ebx           # 4-byte Reload
	movq	%r15, %rcx
	movl	%r9d, %eax
	.p2align	4, 0x90
.LBB9_10:                               #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rcx), %edi
	andl	%esi, %edi
	cmpl	$1, %edi
	sbbl	$-1, %edx
	incl	%eax
	addq	%rbp, %rcx
	incl	%ebx
	jne	.LBB9_10
.LBB9_11:                               # %.prol.loopexit
                                        #   in Loop: Header=BB9_8 Depth=2
	cmpl	$3, %r11d
	jb	.LBB9_14
# BB#12:                                # %.preheader.us.i.new
                                        #   in Loop: Header=BB9_8 Depth=2
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%edi, %r14d
	subl	%eax, %r14d
	.p2align	4, 0x90
.LBB9_13:                               #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rcx), %eax
	andl	%esi, %eax
	cmpl	$1, %eax
	sbbl	$-1, %edx
	movl	(%rcx,%rbp), %eax
	andl	%esi, %eax
	addq	%rbp, %rcx
	cmpl	$1, %eax
	sbbl	$-1, %edx
	movl	(%rbp,%rcx), %eax
	andl	%esi, %eax
	addq	%rbp, %rcx
	cmpl	$1, %eax
	sbbl	$-1, %edx
	movl	(%rbp,%rcx), %eax
	andl	%esi, %eax
	addq	%rbp, %rcx
	cmpl	$1, %eax
	sbbl	$-1, %edx
	addq	%rbp, %rcx
	addl	$-4, %r14d
	jne	.LBB9_13
.LBB9_14:                               # %._crit_edge.us.i
                                        #   in Loop: Header=BB9_8 Depth=2
	cmpl	$1, %edx
	jle	.LBB9_16
# BB#15:                                #   in Loop: Header=BB9_8 Depth=2
	incl	%r13d
	movl	%r13d, rrc_count(%rip)
.LBB9_16:                               #   in Loop: Header=BB9_8 Depth=2
	addl	%esi, %esi
	leaq	4(%r15), %rax
	testl	%esi, %esi
	movl	$1, %ecx
	cmovel	%ecx, %esi
	cmoveq	%rax, %r15
	incl	%r8d
	cmpl	%r10d, %r8d
	jne	.LBB9_8
	.p2align	4, 0x90
.LBB9_17:                               # %count_rr_conflicts.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	src_count(%rip), %eax
	addl	%eax, src_total(%rip)
	addl	%r13d, rrc_total(%rip)
	movq	foutput(%rip), %rdi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %edx
	callq	fprintf
	movl	src_count(%rip), %edx
	cmpl	$1, %edx
	jne	.LBB9_19
# BB#18:                                #   in Loop: Header=BB9_2 Depth=1
	movq	foutput(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB9_21
	.p2align	4, 0x90
.LBB9_3:                                # %.lr.ph._crit_edge
                                        #   in Loop: Header=BB9_2 Depth=1
	incq	%rbx
	jmp	.LBB9_29
	.p2align	4, 0x90
.LBB9_19:                               #   in Loop: Header=BB9_2 Depth=1
	cmpl	$2, %edx
	jl	.LBB9_21
# BB#20:                                #   in Loop: Header=BB9_2 Depth=1
	movq	foutput(%rip), %rdi
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB9_21:                               #   in Loop: Header=BB9_2 Depth=1
	movl	rrc_count(%rip), %edx
	cmpl	$0, src_count(%rip)
	jle	.LBB9_24
# BB#22:                                #   in Loop: Header=BB9_2 Depth=1
	testl	%edx, %edx
	jle	.LBB9_24
# BB#23:                                #   in Loop: Header=BB9_2 Depth=1
	movq	foutput(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movl	rrc_count(%rip), %edx
.LBB9_24:                               #   in Loop: Header=BB9_2 Depth=1
	cmpl	$1, %edx
	jne	.LBB9_26
# BB#25:                                #   in Loop: Header=BB9_2 Depth=1
	movq	foutput(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB9_28
	.p2align	4, 0x90
.LBB9_26:                               #   in Loop: Header=BB9_2 Depth=1
	cmpl	$2, %edx
	jl	.LBB9_28
# BB#27:                                #   in Loop: Header=BB9_2 Depth=1
	movq	foutput(%rip), %rdi
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB9_28:                               #   in Loop: Header=BB9_2 Depth=1
	incq	%rbx
	movq	foutput(%rip), %rsi
	movl	$46, %edi
	callq	_IO_putc
	movq	foutput(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	nstates(%rip), %eax
.LBB9_29:                               #   in Loop: Header=BB9_2 Depth=1
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB9_2
.LBB9_30:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	total_conflicts         # TAILCALL
.Lfunc_end9:
	.size	verbose_conflict_log, .Lfunc_end9-verbose_conflict_log
	.cfi_endproc

	.globl	print_reductions
	.p2align	4, 0x90
	.type	print_reductions,@function
print_reductions:                       # @print_reductions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi87:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi89:
	.cfi_def_cfa_offset 192
.Lcfi90:
	.cfi_offset %rbx, -56
.Lcfi91:
	.cfi_offset %r12, -48
.Lcfi92:
	.cfi_offset %r13, -40
.Lcfi93:
	.cfi_offset %r14, -32
.Lcfi94:
	.cfi_offset %r15, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	cmpl	$0, tokensetsize(%rip)
	jle	.LBB10_3
# BB#1:                                 # %.lr.ph319
	movq	shiftset(%rip), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rax,%rcx,4)
	incq	%rcx
	movslq	tokensetsize(%rip), %rdx
	cmpq	%rdx, %rcx
	jl	.LBB10_2
.LBB10_3:                               # %._crit_edge320
	movq	shift_table(%rip), %rax
	movslq	%edi, %r8
	movq	(%rax,%r8,8), %r13
	xorl	%edx, %edx
	testq	%r13, %r13
	je	.LBB10_10
# BB#4:
	movswq	10(%r13), %rax
	testq	%rax, %rax
	jle	.LBB10_10
# BB#5:                                 # %.lr.ph313
	movq	accessing_symbol(%rip), %rdi
	xorl	%ebp, %ebp
	movq	shiftset(%rip), %rbx
	movl	$1, %r9d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_6:                               # =>This Inner Loop Header: Depth=1
	movswq	12(%r13,%rbp,2), %rcx
	testq	%rcx, %rcx
	je	.LBB10_9
# BB#7:                                 #   in Loop: Header=BB10_6 Depth=1
	movswl	(%rdi,%rcx,2), %ecx
	cmpl	ntokens(%rip), %ecx
	jge	.LBB10_10
# BB#8:                                 #   in Loop: Header=BB10_6 Depth=1
	cmpl	error_token_number(%rip), %ecx
	cmovel	%r9d, %edx
	movl	$1, %esi
	shll	%cl, %esi
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%esi, (%rbx,%rcx,4)
.LBB10_9:                               #   in Loop: Header=BB10_6 Depth=1
	incq	%rbp
	cmpq	%rax, %rbp
	jl	.LBB10_6
.LBB10_10:                              # %.loopexit221
	movq	err_table(%rip), %rax
	movq	(%rax,%r8,8), %rax
	testq	%rax, %rax
	je	.LBB10_23
# BB#11:
	movswq	(%rax), %rcx
	testq	%rcx, %rcx
	jle	.LBB10_23
# BB#12:                                # %.lr.ph309
	movq	shiftset(%rip), %rdi
	movl	%ecx, %ebp
	testb	$1, %cl
	jne	.LBB10_14
# BB#13:
	xorl	%ecx, %ecx
	cmpq	$1, %rbp
	jne	.LBB10_17
	jmp	.LBB10_23
.LBB10_14:
	movswl	2(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB10_16
# BB#15:
	movl	$1, %esi
	shll	%cl, %esi
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%esi, (%rdi,%rcx,4)
.LBB10_16:                              # %.prol.loopexit
	movl	$1, %ecx
	cmpq	$1, %rbp
	je	.LBB10_23
.LBB10_17:                              # %.lr.ph309.new
	subq	%rcx, %rbp
	leaq	4(%rax,%rcx,2), %rax
	.p2align	4, 0x90
.LBB10_18:                              # =>This Inner Loop Header: Depth=1
	movswl	-2(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB10_20
# BB#19:                                #   in Loop: Header=BB10_18 Depth=1
	movl	$1, %esi
	shll	%cl, %esi
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%esi, (%rdi,%rcx,4)
.LBB10_20:                              #   in Loop: Header=BB10_18 Depth=1
	movswl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB10_22
# BB#21:                                #   in Loop: Header=BB10_18 Depth=1
	movl	$1, %esi
	shll	%cl, %esi
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%esi, (%rdi,%rcx,4)
.LBB10_22:                              #   in Loop: Header=BB10_18 Depth=1
	addq	$4, %rax
	addq	$-2, %rbp
	jne	.LBB10_18
.LBB10_23:                              # %.loopexit220
	movabsq	$9223372036854775800, %r9 # imm = 0x7FFFFFFFFFFFFFF8
	movq	lookaheads(%rip), %rax
	movswl	(%rax,%r8,2), %ebp
	movslq	%ebp, %rdi
	movswl	2(%rax,%r8,2), %eax
	movl	%eax, %ecx
	movq	%rdi, %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	subl	%edi, %ecx
	testl	%edx, %edx
	jne	.LBB10_28
# BB#24:                                # %.loopexit220
	cmpl	$1, %ecx
	jne	.LBB10_28
# BB#25:
	movq	LAruleno(%rip), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movswl	(%rax,%rcx,2), %r8d
	movslq	tokensetsize(%rip), %rax
	testq	%rax, %rax
	movq	lookaheadset(%rip), %r12
	jle	.LBB10_135
# BB#26:                                # %.lr.ph305.preheader
	leaq	(%r12,%rax,4), %r14
	movq	shiftset(%rip), %rsi
	movq	LA(%rip), %r10
	imull	%eax, %ebp
	movslq	%ebp, %r11
	leaq	(%r10,%r11,4), %rdi
	leaq	4(%r12), %rax
	cmpq	%rax, %r14
	movq	%rax, %rcx
	cmovaq	%r14, %rcx
	movq	%r12, %rdx
	notq	%rdx
	addq	%rdx, %rcx
	shrq	$2, %rcx
	incq	%rcx
	cmpq	$8, %rcx
	jae	.LBB10_74
# BB#27:
	movq	%r12, %rcx
	jmp	.LBB10_130
.LBB10_28:
	testl	%ecx, %ecx
	jle	.LBB10_73
# BB#29:
	movslq	%eax, %rcx
	movslq	tokensetsize(%rip), %r8
	testl	%edx, %edx
	sete	%al
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmpw	8(%rsp), %cx            # 2-byte Folded Reload
	movl	%ebp, 68(%rsp)          # 4-byte Spill
	jle	.LBB10_79
# BB#30:
	testb	%al, %al
	je	.LBB10_79
# BB#31:                                # %.lr.ph264
	movq	lookaheadset(%rip), %r12
	movq	LAruleno(%rip), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testl	%r8d, %r8d
	jle	.LBB10_80
# BB#32:                                # %.lr.ph264.split.us.preheader
	leaq	(%r12,%r8,4), %rbp
	movq	LA(%rip), %rdx
	movq	shiftset(%rip), %r10
	leaq	4(%r12), %rax
	cmpq	%rax, %rbp
	cmovaq	%rbp, %rax
	movq	%r12, %rcx
	notq	%rcx
	addq	%rax, %rcx
	shrq	$2, %rcx
	leaq	4(%r10,%rcx,4), %rax
	leaq	4(%r12,%rcx,4), %rsi
	leaq	4(%rdx,%rcx,4), %rdi
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	leaq	1(%rcx), %r15
	andq	%r15, %r9
	leaq	-8(%r9), %rdi
	shrq	$3, %rdi
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	cmpq	%rsi, %r10
	sbbb	%cl, %cl
	cmpq	%rax, %r12
	sbbb	%al, %al
	andb	%cl, %al
	andb	$1, %al
	movb	%al, 24(%rsp)           # 1-byte Spill
	leaq	(%r12,%r9,4), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	(%r10,%r9,4), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rdi, 112(%rsp)         # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	andl	$1, %edi
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	leaq	48(%r10), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	48(%r12), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	leaq	48(%rdx), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	$-1, (%rsp)             # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %r11d
	movq	8(%rsp), %r14           # 8-byte Reload
                                        # implicit-def: %EAX
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jmp	.LBB10_33
.LBB10_37:                              #   in Loop: Header=BB10_33 Depth=1
	xorl	%ebx, %ebx
	cmpq	$0, 112(%rsp)           # 8-byte Folded Reload
	je	.LBB10_40
.LBB10_38:                              # %vector.body434.preheader.new
                                        #   in Loop: Header=BB10_33 Depth=1
	movq	%r9, %rsi
	subq	%rbx, %rsi
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rbx,4), %rdi
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rbx,4), %rdx
	addq	%rbx, %rax
	movq	120(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rax,4), %rax
	.p2align	4, 0x90
.LBB10_39:                              # %vector.body434
                                        #   Parent Loop BB10_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rax), %xmm0
	movups	-32(%rax), %xmm1
	movups	-48(%rdi), %xmm2
	movups	-32(%rdi), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -48(%rdx)
	movups	%xmm3, -32(%rdx)
	movups	-16(%rax), %xmm0
	movups	(%rax), %xmm1
	movups	-16(%rdi), %xmm2
	movups	(%rdi), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -16(%rdx)
	movups	%xmm3, (%rdx)
	addq	$64, %rdi
	addq	$64, %rdx
	addq	$64, %rax
	addq	$-16, %rsi
	jne	.LBB10_39
.LBB10_40:                              # %middle.block435
                                        #   in Loop: Header=BB10_33 Depth=1
	cmpq	%r9, %r15
	je	.LBB10_47
# BB#41:                                #   in Loop: Header=BB10_33 Depth=1
	leaq	(%rcx,%r9,4), %rcx
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB10_42
	.p2align	4, 0x90
.LBB10_33:                              # %.lr.ph264.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_39 Depth 2
                                        #     Child Loop BB10_44 Depth 2
                                        #     Child Loop BB10_46 Depth 2
                                        #     Child Loop BB10_53 Depth 2
                                        #     Child Loop BB10_65 Depth 2
                                        #     Child Loop BB10_69 Depth 2
                                        #     Child Loop BB10_71 Depth 2
	imull	%r14d, %r8d
	movslq	%r8d, %rax
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	cmpq	$7, %r15
	jbe	.LBB10_36
# BB#34:                                # %min.iters.checked443
                                        #   in Loop: Header=BB10_33 Depth=1
	testq	%r9, %r9
	je	.LBB10_36
# BB#35:                                # %vector.memcheck468
                                        #   in Loop: Header=BB10_33 Depth=1
	movq	128(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdx
	cmpq	%rdx, %r12
	sbbb	%dl, %dl
	cmpq	48(%rsp), %rcx          # 8-byte Folded Reload
	sbbb	%bl, %bl
	andb	%dl, %bl
	andb	$1, %bl
	orb	24(%rsp), %bl           # 1-byte Folded Reload
	je	.LBB10_61
	.p2align	4, 0x90
.LBB10_36:                              #   in Loop: Header=BB10_33 Depth=1
	movq	%r12, %rsi
	movq	%r10, %rdi
.LBB10_42:                              # %scalar.ph436.preheader
                                        #   in Loop: Header=BB10_33 Depth=1
	leaq	4(%rsi), %rax
	cmpq	%rax, %rbp
	cmovaq	%rbp, %rax
	subq	%rsi, %rax
	decq	%rax
	movl	%eax, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB10_45
# BB#43:                                # %scalar.ph436.prol.preheader
                                        #   in Loop: Header=BB10_33 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB10_44:                              # %scalar.ph436.prol
                                        #   Parent Loop BB10_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ebx
	addq	$4, %rdi
	notl	%ebx
	andl	(%rcx), %ebx
	addq	$4, %rcx
	movl	%ebx, (%rsi)
	addq	$4, %rsi
	incq	%rdx
	jne	.LBB10_44
.LBB10_45:                              # %scalar.ph436.prol.loopexit
                                        #   in Loop: Header=BB10_33 Depth=1
	cmpq	$12, %rax
	jb	.LBB10_47
	.p2align	4, 0x90
.LBB10_46:                              # %scalar.ph436
                                        #   Parent Loop BB10_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %eax
	notl	%eax
	andl	(%rcx), %eax
	movl	%eax, (%rsi)
	movl	4(%rdi), %eax
	notl	%eax
	andl	4(%rcx), %eax
	movl	%eax, 4(%rsi)
	movl	8(%rdi), %eax
	notl	%eax
	andl	8(%rcx), %eax
	movl	%eax, 8(%rsi)
	movl	12(%rdi), %eax
	notl	%eax
	andl	12(%rcx), %eax
	movl	%eax, 12(%rsi)
	addq	$16, %rsi
	addq	$16, %rcx
	addq	$16, %rdi
	cmpq	%rbp, %rsi
	jb	.LBB10_46
.LBB10_47:                              # %._crit_edge247.us
                                        #   in Loop: Header=BB10_33 Depth=1
	movl	ntokens(%rip), %esi
	testl	%esi, %esi
	jle	.LBB10_50
# BB#48:                                # %.lr.ph253.us.preheader
                                        #   in Loop: Header=BB10_33 Depth=1
	testb	$1, %sil
	jne	.LBB10_51
# BB#49:                                #   in Loop: Header=BB10_33 Depth=1
	movl	$1, %eax
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	jmp	.LBB10_52
	.p2align	4, 0x90
.LBB10_50:                              #   in Loop: Header=BB10_33 Depth=1
	xorl	%ecx, %ecx
	cmpl	16(%rsp), %ecx          # 4-byte Folded Reload
	jg	.LBB10_55
	jmp	.LBB10_56
	.p2align	4, 0x90
.LBB10_51:                              # %.lr.ph253.us.prol
                                        #   in Loop: Header=BB10_33 Depth=1
	movl	(%r12), %ecx
	andl	$1, %ecx
	movl	$2, %eax
	movl	$1, %edi
.LBB10_52:                              # %.lr.ph253.us.prol.loopexit
                                        #   in Loop: Header=BB10_33 Depth=1
	cmpl	$1, %esi
	movq	%r12, %rdx
	je	.LBB10_54
	.p2align	4, 0x90
.LBB10_53:                              # %.lr.ph253.us
                                        #   Parent Loop BB10_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %ebx
	andl	%eax, %ebx
	cmpl	$1, %ebx
	sbbl	$-1, %ecx
	addl	%eax, %eax
	leaq	4(%rdx), %rbx
	testl	%eax, %eax
	cmovneq	%rdx, %rbx
	cmovel	%r11d, %eax
	movl	(%rbx), %edx
	andl	%eax, %edx
	cmpl	$1, %edx
	sbbl	$-1, %ecx
	addl	%eax, %eax
	leaq	4(%rbx), %rdx
	testl	%eax, %eax
	cmovneq	%rbx, %rdx
	cmovel	%r11d, %eax
	addl	$2, %edi
	cmpl	%esi, %edi
	jl	.LBB10_53
.LBB10_54:                              # %._crit_edge254.us
                                        #   in Loop: Header=BB10_33 Depth=1
	cmpl	16(%rsp), %ecx          # 4-byte Folded Reload
	jle	.LBB10_56
.LBB10_55:                              #   in Loop: Header=BB10_33 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movswl	(%rax,%r14,2), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	%r14d, %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movl	%ecx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB10_56:                              # %.lr.ph258.us.preheader
                                        #   in Loop: Header=BB10_33 Depth=1
	cmpq	$7, %r15
	movq	%r12, %rcx
	movq	%r10, %rsi
	jbe	.LBB10_67
# BB#57:                                # %min.iters.checked399
                                        #   in Loop: Header=BB10_33 Depth=1
	testq	%r9, %r9
	movq	%r12, %rcx
	movq	%r10, %rsi
	je	.LBB10_67
# BB#58:                                # %vector.memcheck417
                                        #   in Loop: Header=BB10_33 Depth=1
	cmpb	$0, 24(%rsp)            # 1-byte Folded Reload
	movq	%r12, %rcx
	movq	%r10, %rsi
	jne	.LBB10_67
# BB#59:                                # %vector.body390.preheader
                                        #   in Loop: Header=BB10_33 Depth=1
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	jne	.LBB10_63
# BB#60:                                # %vector.body390.prol
                                        #   in Loop: Header=BB10_33 Depth=1
	movups	(%r12), %xmm0
	movups	16(%r12), %xmm1
	movups	(%r10), %xmm2
	movups	16(%r10), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%r10)
	movups	%xmm3, 16(%r10)
	movl	$8, %edx
	cmpq	$0, 112(%rsp)           # 8-byte Folded Reload
	jne	.LBB10_64
	jmp	.LBB10_66
.LBB10_61:                              # %vector.body434.preheader
                                        #   in Loop: Header=BB10_33 Depth=1
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	jne	.LBB10_37
# BB#62:                                # %vector.body434.prol
                                        #   in Loop: Header=BB10_33 Depth=1
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	(%r10), %xmm2
	movups	16(%r10), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, (%r12)
	movups	%xmm3, 16(%r12)
	movl	$8, %ebx
	cmpq	$0, 112(%rsp)           # 8-byte Folded Reload
	jne	.LBB10_38
	jmp	.LBB10_40
.LBB10_63:                              #   in Loop: Header=BB10_33 Depth=1
	xorl	%edx, %edx
	cmpq	$0, 112(%rsp)           # 8-byte Folded Reload
	je	.LBB10_66
.LBB10_64:                              # %vector.body390.preheader.new
                                        #   in Loop: Header=BB10_33 Depth=1
	movq	%r9, %rax
	subq	%rdx, %rax
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,4), %rcx
	movq	72(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB10_65:                              # %vector.body390
                                        #   Parent Loop BB10_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	-48(%rcx), %xmm2
	movups	-32(%rcx), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -48(%rcx)
	movups	%xmm3, -32(%rcx)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	-16(%rcx), %xmm2
	movups	(%rcx), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -16(%rcx)
	movups	%xmm3, (%rcx)
	addq	$64, %rcx
	addq	$64, %rdx
	addq	$-16, %rax
	jne	.LBB10_65
.LBB10_66:                              # %middle.block391
                                        #   in Loop: Header=BB10_33 Depth=1
	cmpq	%r9, %r15
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	88(%rsp), %rsi          # 8-byte Reload
	je	.LBB10_72
	.p2align	4, 0x90
.LBB10_67:                              # %.lr.ph258.us.preheader490
                                        #   in Loop: Header=BB10_33 Depth=1
	leaq	4(%rcx), %rax
	cmpq	%rax, %rbp
	cmovaq	%rbp, %rax
	subq	%rcx, %rax
	decq	%rax
	movl	%eax, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB10_70
# BB#68:                                # %.lr.ph258.us.prol.preheader
                                        #   in Loop: Header=BB10_33 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB10_69:                              # %.lr.ph258.us.prol
                                        #   Parent Loop BB10_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edi
	addq	$4, %rcx
	orl	%edi, (%rsi)
	addq	$4, %rsi
	incq	%rdx
	jne	.LBB10_69
.LBB10_70:                              # %.lr.ph258.us.prol.loopexit
                                        #   in Loop: Header=BB10_33 Depth=1
	cmpq	$12, %rax
	jb	.LBB10_72
	.p2align	4, 0x90
.LBB10_71:                              # %.lr.ph258.us
                                        #   Parent Loop BB10_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %eax
	orl	%eax, (%rsi)
	movl	4(%rcx), %eax
	orl	%eax, 4(%rsi)
	movl	8(%rcx), %eax
	orl	%eax, 8(%rsi)
	movl	12(%rcx), %eax
	orl	%eax, 12(%rsi)
	addq	$16, %rcx
	addq	$16, %rsi
	cmpq	%rbp, %rcx
	jb	.LBB10_71
.LBB10_72:                              # %._crit_edge259.us
                                        #   in Loop: Header=BB10_33 Depth=1
	incq	%r14
	movl	tokensetsize(%rip), %r8d
	cmpq	40(%rsp), %r14          # 8-byte Folded Reload
	jne	.LBB10_33
	jmp	.LBB10_91
.LBB10_73:
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_74:                              # %min.iters.checked
	andq	%rcx, %r9
	je	.LBB10_124
# BB#75:                                # %vector.memcheck
	cmpq	%rax, %r14
	cmovaq	%r14, %rax
	addq	%rdx, %rax
	shrq	$2, %rax
	leaq	4(%r12,%rax,4), %rdx
	leaq	(%rax,%r11), %rbx
	leaq	4(%r10,%rbx,4), %rbx
	leaq	4(%rsi,%rax,4), %rbp
	cmpq	%rbx, %r12
	sbbb	%al, %al
	cmpq	%rdx, %rdi
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%rbp, %r12
	sbbb	%al, %al
	cmpq	%rdx, %rsi
	sbbb	%dl, %dl
	testb	$1, %bl
	jne	.LBB10_124
# BB#76:                                # %vector.memcheck
	andb	%dl, %al
	andb	$1, %al
	jne	.LBB10_124
# BB#77:                                # %vector.body.preheader
	leaq	-8(%r9), %rdx
	movq	%rdx, %rax
	shrq	$3, %rax
	btl	$3, %edx
	jb	.LBB10_125
# BB#78:                                # %vector.body.prol
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	(%rsi), %xmm2
	movups	16(%rsi), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%r12)
	movups	%xmm3, 16(%r12)
	movl	$8, %edx
	testq	%rax, %rax
	jne	.LBB10_126
	jmp	.LBB10_128
.LBB10_79:
	movl	$-1, (%rsp)             # 4-byte Folded Spill
                                        # implicit-def: %EAX
	movl	%eax, 4(%rsp)           # 4-byte Spill
	testl	%r8d, %r8d
	jg	.LBB10_92
	jmp	.LBB10_94
.LBB10_124:
	movq	%r12, %rcx
.LBB10_130:                             # %.lr.ph305.preheader492
	leaq	4(%rcx), %rdx
	cmpq	%rdx, %r14
	cmovaq	%r14, %rdx
	movq	%rcx, %rax
	notq	%rax
	addq	%rdx, %rax
	movl	%eax, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB10_133
# BB#131:                               # %.lr.ph305.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB10_132:                             # %.lr.ph305.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %ebp
	addq	$4, %rsi
	andl	(%rdi), %ebp
	addq	$4, %rdi
	movl	%ebp, (%rcx)
	addq	$4, %rcx
	incq	%rdx
	jne	.LBB10_132
.LBB10_133:                             # %.lr.ph305.prol.loopexit
	cmpq	$12, %rax
	jb	.LBB10_135
	.p2align	4, 0x90
.LBB10_134:                             # %.lr.ph305
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	andl	(%rdi), %eax
	movl	%eax, (%rcx)
	movl	4(%rsi), %eax
	andl	4(%rdi), %eax
	movl	%eax, 4(%rcx)
	movl	8(%rsi), %eax
	andl	8(%rdi), %eax
	movl	%eax, 8(%rcx)
	movl	12(%rsi), %eax
	andl	12(%rdi), %eax
	movl	%eax, 12(%rcx)
	addq	$16, %rcx
	addq	$16, %rdi
	addq	$16, %rsi
	cmpq	%r14, %rcx
	jb	.LBB10_134
.LBB10_135:                             # %._crit_edge306
	movl	ntokens(%rip), %eax
	movslq	%r8d, %r15
	testl	%eax, %eax
	jle	.LBB10_140
# BB#136:                               # %.lr.ph299.preheader
	movl	$1, %r14d
	xorl	%ebx, %ebx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB10_137:                             # %.lr.ph299
                                        # =>This Inner Loop Header: Depth=1
	testl	(%r12), %ebp
	je	.LBB10_139
# BB#138:                               #   in Loop: Header=BB10_137 Depth=1
	movq	foutput(%rip), %rdi
	movq	tags(%rip), %rax
	movq	(%rax,%rbx,8), %rdx
	movq	rlhs(%rip), %rcx
	movswq	(%rcx,%r15,2), %rcx
	movq	(%rax,%rcx,8), %r8
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movl	%r15d, %ecx
	callq	fprintf
	movl	ntokens(%rip), %eax
.LBB10_139:                             #   in Loop: Header=BB10_137 Depth=1
	addl	%ebp, %ebp
	leaq	4(%r12), %rcx
	testl	%ebp, %ebp
	cmoveq	%rcx, %r12
	cmovel	%r14d, %ebp
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB10_137
.LBB10_140:                             # %._crit_edge300
	movq	foutput(%rip), %rdi
	movq	tags(%rip), %rax
	movq	rlhs(%rip), %rcx
	movswq	(%rcx,%r15,2), %rcx
	movq	(%rax,%rcx,8), %rcx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fprintf                 # TAILCALL
.LBB10_80:                              # %.lr.ph264.split.split.preheader
	movl	ntokens(%rip), %ecx
	movl	%ecx, %r9d
	andl	$1, %r9d
	movl	$-1, %r14d
	xorl	%r10d, %r10d
	movl	$1, %esi
	movq	8(%rsp), %r11           # 8-byte Reload
                                        # implicit-def: %R15D
	.p2align	4, 0x90
.LBB10_81:                              # %.lr.ph264.split.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_86 Depth 2
	testl	%ecx, %ecx
	movl	$0, %ebp
	jle	.LBB10_87
# BB#82:                                # %.lr.ph253.preheader
                                        #   in Loop: Header=BB10_81 Depth=1
	testl	%r9d, %r9d
	jne	.LBB10_84
# BB#83:                                #   in Loop: Header=BB10_81 Depth=1
	movl	$1, %eax
	xorl	%edx, %edx
	xorl	%ebp, %ebp
	cmpl	$1, %ecx
	jne	.LBB10_85
	jmp	.LBB10_87
	.p2align	4, 0x90
.LBB10_84:                              # %.lr.ph253.prol
                                        #   in Loop: Header=BB10_81 Depth=1
	movl	(%r12), %ebp
	andl	$1, %ebp
	movl	$2, %eax
	movl	$1, %edx
	cmpl	$1, %ecx
	je	.LBB10_87
.LBB10_85:                              # %.lr.ph253.preheader.new
                                        #   in Loop: Header=BB10_81 Depth=1
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB10_86:                              # %.lr.ph253
                                        #   Parent Loop BB10_81 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %edi
	andl	%eax, %edi
	cmpl	$1, %edi
	sbbl	$-1, %ebp
	addl	%eax, %eax
	leaq	4(%rbx), %rdi
	testl	%eax, %eax
	cmovneq	%rbx, %rdi
	cmovel	%esi, %eax
	movl	(%rdi), %ebx
	andl	%eax, %ebx
	cmpl	$1, %ebx
	sbbl	$-1, %ebp
	addl	%eax, %eax
	leaq	4(%rdi), %rbx
	testl	%eax, %eax
	cmovneq	%rdi, %rbx
	cmovel	%esi, %eax
	addl	$2, %edx
	cmpl	%ecx, %edx
	jl	.LBB10_86
.LBB10_87:                              # %._crit_edge254
                                        #   in Loop: Header=BB10_81 Depth=1
	cmpl	%r10d, %ebp
	jle	.LBB10_89
# BB#88:                                #   in Loop: Header=BB10_81 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movswl	(%rax,%r11,2), %r15d
	movl	%r11d, %r14d
	movl	%ebp, %r10d
.LBB10_89:                              #   in Loop: Header=BB10_81 Depth=1
	incq	%r11
	cmpq	40(%rsp), %r11          # 8-byte Folded Reload
	jne	.LBB10_81
# BB#90:
	movl	%r15d, 4(%rsp)          # 4-byte Spill
	movl	%r14d, (%rsp)           # 4-byte Spill
.LBB10_91:                              # %.loopexit219
	testl	%r8d, %r8d
	jle	.LBB10_94
.LBB10_92:                              # %.lr.ph240
	movq	shiftset(%rip), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_93:                              # =>This Inner Loop Header: Depth=1
	movl	$0, (%rax,%rcx,4)
	incq	%rcx
	movslq	tokensetsize(%rip), %r8
	cmpq	%r8, %rcx
	jl	.LBB10_93
.LBB10_94:                              # %._crit_edge241
	testq	%r13, %r13
	movl	68(%rsp), %ebx          # 4-byte Reload
	je	.LBB10_102
# BB#95:
	movswq	10(%r13), %rax
	testq	%rax, %rax
	jle	.LBB10_102
# BB#96:                                # %.lr.ph236
	movq	accessing_symbol(%rip), %rdx
	movq	shiftset(%rip), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB10_97:                              # =>This Inner Loop Header: Depth=1
	movswq	12(%r13,%rdi,2), %rcx
	testq	%rcx, %rcx
	je	.LBB10_100
# BB#98:                                #   in Loop: Header=BB10_97 Depth=1
	movswl	(%rdx,%rcx,2), %ecx
	cmpl	ntokens(%rip), %ecx
	jge	.LBB10_101
# BB#99:                                #   in Loop: Header=BB10_97 Depth=1
	movl	$1, %ebp
	shll	%cl, %ebp
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%ebp, (%rsi,%rcx,4)
.LBB10_100:                             #   in Loop: Header=BB10_97 Depth=1
	incq	%rdi
	cmpq	%rax, %rdi
	jl	.LBB10_97
.LBB10_101:                             # %.loopexit.loopexit
	movl	tokensetsize(%rip), %r8d
.LBB10_102:                             # %.loopexit
	movl	ntokens(%rip), %eax
	testl	%eax, %eax
	jle	.LBB10_120
# BB#103:                               # %.lr.ph232
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpw	8(%rsp), %cx            # 2-byte Folded Reload
	jle	.LBB10_116
# BB#104:                               # %.lr.ph232.split.us.preheader
	imull	%ebx, %r8d
	movslq	%r8d, %rbp
	shlq	$2, %rbp
	addq	LA(%rip), %rbp
	movq	shiftset(%rip), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	(%rsp), %rdx            # 4-byte Folded Reload
	movq	%rdx, %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	subl	%ebx, %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	xorl	%edx, %edx
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB10_105:                             # %.lr.ph232.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_106 Depth 2
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movq	24(%rsp), %rax          # 8-byte Reload
	testl	(%rax), %r14d
	setne	%r12b
	movl	48(%rsp), %r13d         # 4-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	xorl	%r15d, %r15d
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	jmp	.LBB10_106
.LBB10_141:                             #   in Loop: Header=BB10_106 Depth=2
	movl	$1, %r15d
	jmp	.LBB10_114
	.p2align	4, 0x90
.LBB10_106:                             #   Parent Loop BB10_105 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	(%rbp), %r14d
	je	.LBB10_114
# BB#107:                               #   in Loop: Header=BB10_106 Depth=2
	testl	%r12d, %r12d
	je	.LBB10_111
# BB#108:                               #   in Loop: Header=BB10_106 Depth=2
	testl	%r15d, %r15d
	movq	16(%rsp), %r15          # 8-byte Reload
	je	.LBB10_110
# BB#109:                               #   in Loop: Header=BB10_106 Depth=2
	movq	LAruleno(%rip), %rax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movswq	(%rax,%rcx,2), %rcx
	movq	foutput(%rip), %rdi
	movq	tags(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	movq	rlhs(%rip), %rsi
	movswq	(%rsi,%rcx,2), %rsi
	movq	(%rax,%rsi,8), %r8
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
.LBB10_110:                             #   in Loop: Header=BB10_106 Depth=2
	movq	LAruleno(%rip), %rax
	movswq	(%rax,%rbx,2), %rcx
	movq	foutput(%rip), %rdi
	movq	tags(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	movq	rlhs(%rip), %rsi
	movswq	(%rsi,%rcx,2), %rsi
	movq	(%rax,%rsi,8), %r8
	xorl	%r15d, %r15d
	movl	$.L.str.11, %esi
	jmp	.LBB10_113
	.p2align	4, 0x90
.LBB10_111:                             #   in Loop: Header=BB10_106 Depth=2
	movl	$1, %r12d
	testl	%r13d, %r13d
	je	.LBB10_141
# BB#112:                               #   in Loop: Header=BB10_106 Depth=2
	movq	LAruleno(%rip), %rax
	movswq	(%rax,%rbx,2), %rcx
	movq	foutput(%rip), %rdi
	movq	tags(%rip), %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rax,%rdx,8), %rdx
	movq	rlhs(%rip), %rsi
	movswq	(%rsi,%rcx,2), %rsi
	movq	(%rax,%rsi,8), %r8
	movl	$.L.str.13, %esi
.LBB10_113:                             #   in Loop: Header=BB10_106 Depth=2
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
.LBB10_114:                             #   in Loop: Header=BB10_106 Depth=2
	movslq	tokensetsize(%rip), %rax
	leaq	(%rbp,%rax,4), %rbp
	incq	%rbx
	decl	%r13d
	cmpq	%rbx, 40(%rsp)          # 8-byte Folded Reload
	jne	.LBB10_106
# BB#115:                               # %._crit_edge.us
                                        #   in Loop: Header=BB10_105 Depth=1
	addl	%r14d, %r14d
	movq	32(%rsp), %rbp          # 8-byte Reload
	leaq	4(%rbp), %rax
	testl	%r14d, %r14d
	movl	$1, %edx
	cmovel	%edx, %r14d
	cmoveq	%rax, %rbp
	movq	16(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movslq	ntokens(%rip), %rax
	cmpq	%rax, %rdx
	jl	.LBB10_105
	jmp	.LBB10_120
.LBB10_116:                             # %.lr.ph232.split.preheader
	leal	-1(%rax), %edx
	movl	%eax, %esi
	xorl	%ecx, %ecx
	andl	$7, %esi
	je	.LBB10_118
	.p2align	4, 0x90
.LBB10_117:                             # %.lr.ph232.split.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	%ecx, %esi
	jne	.LBB10_117
.LBB10_118:                             # %.lr.ph232.split.prol.loopexit
	cmpl	$7, %edx
	jb	.LBB10_120
	.p2align	4, 0x90
.LBB10_119:                             # %.lr.ph232.split
                                        # =>This Inner Loop Header: Depth=1
	addl	$8, %ecx
	cmpl	%eax, %ecx
	jl	.LBB10_119
.LBB10_120:                             # %._crit_edge233
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	js	.LBB10_122
# BB#121:
	movq	foutput(%rip), %rdi
	movq	tags(%rip), %rax
	movq	rlhs(%rip), %rcx
	movslq	4(%rsp), %rdx           # 4-byte Folded Reload
	movswq	(%rcx,%rdx,2), %rcx
	movq	(%rax,%rcx,8), %rcx
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
.LBB10_122:
	movq	foutput(%rip), %rsi
	movl	$10, %edi
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.LBB10_125:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.LBB10_128
.LBB10_126:                             # %vector.body.preheader.new
	movq	%r9, %rbp
	subq	%rdx, %rbp
	leaq	48(%rsi,%rdx,4), %rbx
	leaq	48(%r12,%rdx,4), %rax
	addq	%rdx, %r11
	leaq	48(%r10,%r11,4), %rdx
	.p2align	4, 0x90
.LBB10_127:                             # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	-48(%rbx), %xmm2
	movups	-32(%rbx), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -48(%rax)
	movups	%xmm3, -32(%rax)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	-16(%rbx), %xmm2
	movups	(%rbx), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -16(%rax)
	movups	%xmm3, (%rax)
	addq	$64, %rbx
	addq	$64, %rax
	addq	$64, %rdx
	addq	$-16, %rbp
	jne	.LBB10_127
.LBB10_128:                             # %middle.block
	cmpq	%r9, %rcx
	je	.LBB10_135
# BB#129:
	leaq	(%rdi,%r9,4), %rdi
	leaq	(%r12,%r9,4), %rcx
	leaq	(%rsi,%r9,4), %rsi
	jmp	.LBB10_130
.Lfunc_end10:
	.size	print_reductions, .Lfunc_end10-print_reductions
	.cfi_endproc

	.globl	finalize_conflicts
	.p2align	4, 0x90
	.type	finalize_conflicts,@function
finalize_conflicts:                     # @finalize_conflicts
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi96:
	.cfi_def_cfa_offset 16
	movq	conflicts(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB11_2
# BB#1:
	callq	free
.LBB11_2:
	movq	shiftset(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB11_4
# BB#3:
	callq	free
.LBB11_4:
	movq	lookaheadset(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB11_5
# BB#6:
	popq	%rax
	jmp	free                    # TAILCALL
.LBB11_5:
	popq	%rax
	retq
.Lfunc_end11:
	.size	finalize_conflicts, .Lfunc_end11-finalize_conflicts
	.cfi_endproc

	.type	conflicts,@object       # @conflicts
	.comm	conflicts,8,8
	.type	shiftset,@object        # @shiftset
	.local	shiftset
	.comm	shiftset,8,8
	.type	lookaheadset,@object    # @lookaheadset
	.local	lookaheadset
	.comm	lookaheadset,8,8
	.type	err_table,@object       # @err_table
	.comm	err_table,8,8
	.type	any_conflicts,@object   # @any_conflicts
	.comm	any_conflicts,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"reduce"
	.size	.L.str, 7

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"shift"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"an error"
	.size	.L.str.2, 9

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Conflict in state %d between rule %d and token %s resolved as %s.\n"
	.size	.L.str.3, 67

	.type	src_total,@object       # @src_total
	.local	src_total
	.comm	src_total,4,4
	.type	rrc_total,@object       # @rrc_total
	.local	rrc_total
	.comm	rrc_total,4,4
	.type	src_count,@object       # @src_count
	.local	src_count
	.comm	src_count,4,4
	.type	rrc_count,@object       # @rrc_count
	.local	rrc_count
	.comm	rrc_count,4,4
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"State %d contains"
	.size	.L.str.4, 18

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" 1 shift/reduce conflict"
	.size	.L.str.5, 25

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	" %d shift/reduce conflicts"
	.size	.L.str.6, 27

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	" and"
	.size	.L.str.7, 5

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" 1 reduce/reduce conflict"
	.size	.L.str.8, 26

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	" %d reduce/reduce conflicts"
	.size	.L.str.9, 28

	.type	expected_conflicts,@object # @expected_conflicts
	.comm	expected_conflicts,4,4
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%s contains"
	.size	.L.str.10, 12

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"    %-4s\t[reduce  %d  (%s)]\n"
	.size	.L.str.11, 29

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"    $default\treduce  %d  (%s)\n\n"
	.size	.L.str.12, 32

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"    %-4s\treduce  %d  (%s)\n"
	.size	.L.str.13, 27

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"    $default\treduce  %d  (%s)\n"
	.size	.L.str.14, 31


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
