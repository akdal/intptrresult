	.text
	.file	"transform8x8.bc"
	.globl	intrapred8x8
	.p2align	4, 0x90
	.type	intrapred8x8,@function
intrapred8x8:                           # @intrapred8x8
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$472, %rsp              # imm = 0x1D8
.Lcfi6:
	.cfi_def_cfa_offset 528
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %r12d
	movq	dec_picture(%rip), %rax
	movq	316920(%rax), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	4(%rdi), %r15d
	movl	68(%rdi), %eax
	movl	72(%rdi), %ecx
	movl	%r12d, %r14d
	shrl	$31, %r14d
	addl	%r12d, %r14d
	movl	%r14d, %edx
	andl	$-2, %edx
	subl	%edx, %r12d
	leal	(%r12,%r12), %esi
	leal	(%rsi,%rcx,4), %ecx
	sarl	%r14d
	leal	(%rdx,%rax,4), %eax
	leal	(,%r12,8), %r13d
	leal	(,%r14,8), %esi
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	5544(%rdi), %rdx
	cltq
	movq	(%rdx,%rax,8), %rax
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leal	-1(,%r12,8), %ebp
	movl	%esi, 104(%rsp)         # 4-byte Spill
	movslq	%esi, %rbx
	leaq	256(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	movl	%ebp, %esi
	movl	%ebx, %edx
	callq	*getNeighbour(%rip)
	leaq	1(%rbx), %rdx
	leaq	280(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	movl	%ebp, %esi
	movq	%rdx, 72(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*getNeighbour(%rip)
	leaq	2(%rbx), %rdx
	leaq	304(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	movl	%ebp, %esi
	movq	%rdx, 96(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*getNeighbour(%rip)
	leaq	3(%rbx), %rdx
	leaq	328(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	movl	%ebp, %esi
	movq	%rdx, 152(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*getNeighbour(%rip)
	leaq	4(%rbx), %rdx
	leaq	352(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	movl	%ebp, %esi
	movq	%rdx, 160(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*getNeighbour(%rip)
	leaq	5(%rbx), %rdx
	leaq	376(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	movl	%ebp, %esi
	movq	%rdx, 120(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*getNeighbour(%rip)
	leaq	6(%rbx), %rdx
	leaq	400(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	movl	%ebp, %esi
	movq	%rdx, 144(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*getNeighbour(%rip)
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	leaq	7(%rbx), %rdx
	leaq	424(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	movl	%ebp, %esi
	movq	%rdx, 176(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*getNeighbour(%rip)
	leal	-1(,%r14,8), %ebx
	leaq	232(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	movl	%r13d, %esi
	movl	%ebx, %edx
	callq	*getNeighbour(%rip)
	movq	%r12, 112(%rsp)         # 8-byte Spill
	leal	8(,%r12,8), %esi
	leaq	208(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	movl	%esi, 168(%rsp)         # 4-byte Spill
	movl	%ebx, %edx
	callq	*getNeighbour(%rip)
	leaq	448(%rsp), %r12
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	movl	%ebp, %esi
	movl	%ebx, %edx
	movq	%r12, %r8
	callq	*getNeighbour(%rip)
	cmpl	$0, 208(%rsp)
	movl	%r13d, 80(%rsp)         # 4-byte Spill
	je	.LBB0_2
# BB#1:
	cmpl	$8, %r13d
	setne	%al
	cmpl	$8, 104(%rsp)           # 4-byte Folded Reload
	setne	%cl
	orb	%al, %cl
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	jmp	.LBB0_3
.LBB0_2:
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	xorl	%ecx, %ecx
.LBB0_3:
	movzbl	%cl, %eax
	movl	%eax, 208(%rsp)
	movq	active_pps(%rip), %rcx
	cmpl	$0, 1148(%rcx)
	je	.LBB0_26
# BB#4:                                 # %.preheader1436
	xorl	%esi, %esi
	cmpl	$0, 256(%rsp)
	movl	$0, %edx
	je	.LBB0_6
# BB#5:
	movq	16(%r8), %rcx
	movslq	260(%rsp), %rdx
	movl	(%rcx,%rdx,4), %edx
.LBB0_6:
	cmpl	$0, 280(%rsp)
	je	.LBB0_8
# BB#7:
	movq	16(%r8), %rcx
	movslq	284(%rsp), %rsi
	movl	(%rcx,%rsi,4), %esi
.LBB0_8:
	xorl	%ecx, %ecx
	cmpl	$0, 304(%rsp)
	movl	$0, %edi
	je	.LBB0_10
# BB#9:
	movq	16(%r8), %rdi
	movslq	308(%rsp), %rbp
	movl	(%rdi,%rbp,4), %edi
.LBB0_10:
	andl	$1, %edx
	cmpl	$0, 328(%rsp)
	je	.LBB0_12
# BB#11:
	movq	16(%r8), %rcx
	movslq	332(%rsp), %rbp
	movl	(%rcx,%rbp,4), %ecx
.LBB0_12:
	andl	%edx, %esi
	xorl	%edx, %edx
	cmpl	$0, 352(%rsp)
	movl	$0, %ebp
	je	.LBB0_14
# BB#13:
	movq	16(%r8), %rbp
	movslq	356(%rsp), %rbx
	movl	(%rbp,%rbx,4), %ebp
.LBB0_14:
	andl	%esi, %edi
	cmpl	$0, 376(%rsp)
	je	.LBB0_16
# BB#15:
	movq	16(%r8), %rdx
	movslq	380(%rsp), %rsi
	movl	(%rdx,%rsi,4), %edx
.LBB0_16:
	andl	%edi, %ecx
	xorl	%ebx, %ebx
	cmpl	$0, 400(%rsp)
	movl	$0, %esi
	je	.LBB0_18
# BB#17:
	movq	16(%r8), %rsi
	movslq	404(%rsp), %rdi
	movl	(%rsi,%rdi,4), %esi
.LBB0_18:
	andl	%ecx, %ebp
	cmpl	$0, 424(%rsp)
	je	.LBB0_20
# BB#19:
	movq	16(%r8), %rcx
	movslq	428(%rsp), %rdi
	movl	(%rcx,%rdi,4), %ebx
.LBB0_20:
	andl	%ebp, %edx
	xorl	%ecx, %ecx
	cmpl	$0, 232(%rsp)
	movl	$0, %r15d
	je	.LBB0_22
# BB#21:
	movq	16(%r8), %rdi
	movslq	236(%rsp), %rbp
	movl	(%rdi,%rbp,4), %r15d
.LBB0_22:
	andl	%edx, %esi
	testl	%eax, %eax
	je	.LBB0_24
# BB#23:
	movq	16(%r8), %rax
	movslq	212(%rsp), %rcx
	movl	(%rax,%rcx,4), %ecx
.LBB0_24:
	andl	%esi, %ebx
	cmpl	$0, 448(%rsp)
	je	.LBB0_28
# BB#25:
	movslq	452(%rsp), %r12
	shlq	$2, %r12
	addq	16(%r8), %r12
	jmp	.LBB0_27
.LBB0_26:
	movl	256(%rsp), %ebx
	movl	232(%rsp), %r15d
	movl	%eax, %ecx
.LBB0_27:                               # %.sink.split
	movl	(%r12), %r13d
	testl	%r15d, %r15d
	jne	.LBB0_29
	jmp	.LBB0_30
.LBB0_28:
	xorl	%r13d, %r13d
	testl	%r15d, %r15d
	je	.LBB0_30
.LBB0_29:
	movslq	252(%rsp), %rax
	movq	(%r9,%rax,8), %rax
	movslq	248(%rsp), %rdx
	movzwl	(%rax,%rdx,2), %esi
	movw	%si, 2(%rsp)
	movzwl	2(%rax,%rdx,2), %esi
	movw	%si, 4(%rsp)
	movzwl	4(%rax,%rdx,2), %esi
	movw	%si, 6(%rsp)
	movzwl	6(%rax,%rdx,2), %esi
	movw	%si, 8(%rsp)
	movzwl	8(%rax,%rdx,2), %esi
	movw	%si, 10(%rsp)
	movzwl	10(%rax,%rdx,2), %esi
	movw	%si, 12(%rsp)
	movzwl	12(%rax,%rdx,2), %esi
	movw	%si, 14(%rsp)
	movw	14(%rax,%rdx,2), %ax
	movw	%ax, 16(%rsp)
	testl	%ecx, %ecx
	jne	.LBB0_31
	jmp	.LBB0_32
.LBB0_30:
	movl	5892(%r8), %eax
	movd	%eax, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, 2(%rsp)
	testl	%ecx, %ecx
	je	.LBB0_32
.LBB0_31:
	movslq	228(%rsp), %rax
	movq	(%r9,%rax,8), %rax
	movslq	224(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %edx
	movw	%dx, 18(%rsp)
	movzwl	2(%rax,%rcx,2), %edx
	movw	%dx, 20(%rsp)
	movzwl	4(%rax,%rcx,2), %edx
	movw	%dx, 22(%rsp)
	movzwl	6(%rax,%rcx,2), %edx
	movw	%dx, 24(%rsp)
	movzwl	8(%rax,%rcx,2), %edx
	movw	%dx, 26(%rsp)
	movzwl	10(%rax,%rcx,2), %edx
	movw	%dx, 28(%rsp)
	movzwl	12(%rax,%rcx,2), %edx
	movw	%dx, 30(%rsp)
	movzwl	14(%rax,%rcx,2), %eax
	movw	%ax, 32(%rsp)
	testl	%ebx, %ebx
	jne	.LBB0_33
	jmp	.LBB0_34
.LBB0_32:
	movd	%eax, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, 18(%rsp)
	testl	%ebx, %ebx
	je	.LBB0_34
.LBB0_33:
	movslq	276(%rsp), %rax
	movq	(%r9,%rax,8), %rax
	movslq	272(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, 34(%rsp)
	movslq	300(%rsp), %rax
	movq	(%r9,%rax,8), %rax
	movslq	296(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, 36(%rsp)
	movslq	324(%rsp), %rax
	movq	(%r9,%rax,8), %rax
	movslq	320(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, 38(%rsp)
	movslq	348(%rsp), %rax
	movq	(%r9,%rax,8), %rax
	movslq	344(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, 40(%rsp)
	movslq	372(%rsp), %rax
	movq	(%r9,%rax,8), %rax
	movslq	368(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, 42(%rsp)
	movslq	396(%rsp), %rax
	movq	(%r9,%rax,8), %rax
	movslq	392(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, 44(%rsp)
	movslq	420(%rsp), %rax
	movq	(%r9,%rax,8), %rax
	movslq	416(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, 46(%rsp)
	movslq	444(%rsp), %rax
	movq	(%r9,%rax,8), %rax
	movslq	440(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, 48(%rsp)
	testl	%r13d, %r13d
	jne	.LBB0_35
	jmp	.LBB0_36
.LBB0_34:
	movd	5892(%r8), %xmm0        # xmm0 = mem[0],zero,zero,zero
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, 34(%rsp)
	testl	%r13d, %r13d
	je	.LBB0_36
.LBB0_35:
	movslq	468(%rsp), %rax
	movq	(%r9,%rax,8), %rax
	movslq	464(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	jmp	.LBB0_37
.LBB0_36:
	movzwl	5892(%r8), %eax
.LBB0_37:
	movw	%ax, (%rsp)
	movq	%rsp, %rdi
	movl	%r13d, %esi
	movl	%r15d, %edx
	movl	%ebx, %ecx
	callq	LowPassForIntra8x8Pred
	movq	136(%rsp), %rsi         # 8-byte Reload
	cmpb	$8, %sil
	ja	.LBB0_47
# BB#38:
	movl	%ebx, %r11d
	movl	%r15d, 128(%rsp)        # 4-byte Spill
	leal	1(,%r14,8), %r8d
	leal	3(,%r14,8), %ebp
	leal	5(,%r14,8), %r12d
	leal	7(,%r14,8), %r10d
	movq	112(%rsp), %rbx         # 8-byte Reload
	leal	2(,%rbx,8), %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	leal	4(,%rbx,8), %eax
	leal	6(,%rbx,8), %edi
	leal	2(,%r14,8), %ecx
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	leal	4(,%r14,8), %r15d
	leal	6(,%r14,8), %edx
	leal	1(,%rbx,8), %ecx
	leal	3(,%rbx,8), %r9d
	leal	5(,%rbx,8), %r14d
	leal	7(,%rbx,8), %ebx
	jmpq	*.LJTI0_0(,%rsi,8)
.LBB0_39:
	movl	%r10d, %ebx
	cmpl	$0, 128(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_41
# BB#40:
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %esi
	movl	%r12d, 152(%rsp)        # 4-byte Spill
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebp, %r14d
	movl	%r8d, %r13d
	movl	%edx, %ebp
	callq	printf
	movl	%ebp, %edx
	movl	%r13d, %r8d
	movl	%r14d, %ebp
	movl	152(%rsp), %r12d        # 4-byte Reload
.LBB0_41:                               # %.preheader1432
	movslq	%ebx, %rcx
	movslq	%edx, %rdx
	movslq	%r12d, %rsi
	movslq	%r15d, %rdi
	movslq	%ebp, %r12
	movslq	56(%rsp), %r14          # 4-byte Folded Reload
	movslq	%r8d, %r10
	movslq	80(%rsp), %rbp          # 4-byte Folded Reload
	movzwl	2(%rsp), %ebx
	shlq	$5, %rcx
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	104(%rax,%rcx), %r8
	movw	%bx, (%r8,%rbp,2)
	shlq	$5, %rdx
	leaq	104(%rax,%rdx), %r9
	movw	%bx, (%r9,%rbp,2)
	shlq	$5, %rsi
	leaq	104(%rax,%rsi), %r11
	movw	%bx, (%r11,%rbp,2)
	shlq	$5, %rdi
	leaq	104(%rax,%rdi), %r15
	movw	%bx, (%r15,%rbp,2)
	shlq	$5, %r12
	leaq	104(%rax,%r12), %rdi
	movw	%bx, (%rdi,%rbp,2)
	shlq	$5, %r14
	leaq	104(%rax,%r14), %rsi
	movw	%bx, (%rsi,%rbp,2)
	shlq	$5, %r10
	leaq	104(%rax,%r10), %rcx
	movw	%bx, (%rcx,%rbp,2)
	movq	88(%rsp), %rdx          # 8-byte Reload
	shlq	$5, %rdx
	leaq	104(%rax,%rdx), %rdx
	movw	%bx, (%rdx,%rbp,2)
	movq	%rbp, %rax
	orq	$1, %rax
	movzwl	4(%rsp), %ebx
	movw	%bx, (%r8,%rax,2)
	movw	%bx, (%r9,%rax,2)
	movw	%bx, (%r11,%rax,2)
	movw	%bx, (%r15,%rax,2)
	movw	%bx, (%rdi,%rax,2)
	movw	%bx, (%rsi,%rax,2)
	movw	%bx, (%rcx,%rax,2)
	movw	%bx, (%rdx,%rax,2)
	movq	%rbp, %rax
	orq	$2, %rax
	movzwl	6(%rsp), %ebx
	movw	%bx, (%r8,%rax,2)
	movw	%bx, (%r9,%rax,2)
	movw	%bx, (%r11,%rax,2)
	movw	%bx, (%r15,%rax,2)
	movw	%bx, (%rdi,%rax,2)
	movw	%bx, (%rsi,%rax,2)
	movw	%bx, (%rcx,%rax,2)
	movw	%bx, (%rdx,%rax,2)
	movq	%rbp, %rax
	orq	$3, %rax
	movzwl	8(%rsp), %ebx
	movw	%bx, (%r8,%rax,2)
	movw	%bx, (%r9,%rax,2)
	movw	%bx, (%r11,%rax,2)
	movw	%bx, (%r15,%rax,2)
	movw	%bx, (%rdi,%rax,2)
	movw	%bx, (%rsi,%rax,2)
	movw	%bx, (%rcx,%rax,2)
	movw	%bx, (%rdx,%rax,2)
	movq	%rbp, %rax
	orq	$4, %rax
	movzwl	10(%rsp), %ebx
	movw	%bx, (%r8,%rax,2)
	movw	%bx, (%r9,%rax,2)
	movw	%bx, (%r11,%rax,2)
	movw	%bx, (%r15,%rax,2)
	movw	%bx, (%rdi,%rax,2)
	movw	%bx, (%rsi,%rax,2)
	movw	%bx, (%rcx,%rax,2)
	movw	%bx, (%rdx,%rax,2)
	movq	%rbp, %rax
	orq	$5, %rax
	movzwl	12(%rsp), %ebx
	movw	%bx, (%r8,%rax,2)
	movw	%bx, (%r9,%rax,2)
	movw	%bx, (%r11,%rax,2)
	movw	%bx, (%r15,%rax,2)
	movw	%bx, (%rdi,%rax,2)
	movw	%bx, (%rsi,%rax,2)
	movw	%bx, (%rcx,%rax,2)
	movw	%bx, (%rdx,%rax,2)
	movq	%rbp, %rax
	orq	$6, %rax
	movzwl	14(%rsp), %ebx
	movw	%bx, (%r8,%rax,2)
	movw	%bx, (%r9,%rax,2)
	movw	%bx, (%r11,%rax,2)
	movw	%bx, (%r15,%rax,2)
	movw	%bx, (%rdi,%rax,2)
	movw	%bx, (%rsi,%rax,2)
	movw	%bx, (%rcx,%rax,2)
	movw	%bx, (%rdx,%rax,2)
	orq	$7, %rbp
	movzwl	16(%rsp), %eax
	movw	%ax, (%r8,%rbp,2)
	movw	%ax, (%r9,%rbp,2)
	movw	%ax, (%r11,%rbp,2)
	movw	%ax, (%r15,%rbp,2)
	movw	%ax, (%rdi,%rbp,2)
	movw	%ax, (%rsi,%rbp,2)
	movw	%ax, (%rcx,%rbp,2)
	movw	%ax, (%rdx,%rbp,2)
	jmp	.LBB0_85
.LBB0_42:
	movl	%r10d, 136(%rsp)        # 4-byte Spill
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movl	%r15d, 168(%rsp)        # 4-byte Spill
	movl	%ecx, 144(%rsp)         # 4-byte Spill
	movl	%edx, 96(%rsp)          # 4-byte Spill
	movl	%edi, 160(%rsp)         # 4-byte Spill
	movl	%r8d, 176(%rsp)         # 4-byte Spill
	movl	%ebp, 184(%rsp)         # 4-byte Spill
	movl	%ebx, 112(%rsp)         # 4-byte Spill
	testl	%r13d, %r13d
	movl	%r14d, %ecx
	je	.LBB0_45
# BB#43:
	testl	%r11d, %r11d
	je	.LBB0_45
# BB#44:
	cmpl	$0, 128(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_46
.LBB0_45:
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %esi
	movl	%r9d, %r15d
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%ecx, %ebx
	callq	printf
	movl	%ebx, %ecx
	movl	%r15d, %r9d
.LBB0_46:
	movl	%ecx, 192(%rsp)         # 4-byte Spill
	movzwl	48(%rsp), %ecx
	movzwl	44(%rsp), %eax
	addl	%eax, %ecx
	movzwl	46(%rsp), %esi
	leal	2(%rcx,%rsi,2), %ecx
	shrl	$2, %ecx
	movslq	136(%rsp), %rdx         # 4-byte Folded Reload
	movslq	80(%rsp), %rbx          # 4-byte Folded Reload
	shlq	$5, %rdx
	movq	64(%rsp), %rbp          # 8-byte Reload
	leaq	104(%rbp,%rdx), %rdx
	movw	%cx, (%rdx,%rbx,2)
	movq	%rdx, %rdi
	movzwl	42(%rsp), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rax,2), %ecx
	shrl	$2, %ecx
	movslq	144(%rsp), %rsi         # 4-byte Folded Reload
	movw	%cx, (%rdi,%rsi,2)
	movq	%rdi, %r8
	movq	%rsi, %r13
	movslq	96(%rsp), %rsi          # 4-byte Folded Reload
	shlq	$5, %rsi
	leaq	104(%rbp,%rsi), %rsi
	movw	%cx, (%rsi,%rbx,2)
	movq	%rsi, %rdi
	movzwl	40(%rsp), %ecx
	addl	%ecx, %eax
	leal	2(%rax,%rdx,2), %eax
	shrl	$2, %eax
	movslq	104(%rsp), %r11         # 4-byte Folded Reload
	movw	%ax, (%r8,%r11,2)
	movq	%r8, %r10
	movw	%ax, (%rdi,%r13,2)
	movslq	%r12d, %rsi
	shlq	$5, %rsi
	leaq	104(%rbp,%rsi), %rsi
	movq	%rbx, 152(%rsp)         # 8-byte Spill
	movw	%ax, (%rsi,%rbx,2)
	movq	%rsi, %r8
	movzwl	38(%rsp), %eax
	addl	%eax, %edx
	leal	2(%rdx,%rcx,2), %edx
	shrl	$2, %edx
	movslq	%r9d, %rsi
	movw	%dx, (%r10,%rsi,2)
	movq	%rsi, %r9
	movw	%dx, (%rdi,%r11,2)
	movw	%dx, (%r8,%r13,2)
	movq	%r8, %r15
	movq	%r15, 136(%rsp)         # 8-byte Spill
	movslq	168(%rsp), %rsi         # 4-byte Folded Reload
	shlq	$5, %rsi
	leaq	104(%rbp,%rsi), %rsi
	movw	%dx, (%rsi,%rbx,2)
	movq	%rsi, %r12
	movzwl	36(%rsp), %r8d
	addl	%r8d, %ecx
	leal	2(%rcx,%rax,2), %ecx
	shrl	$2, %ecx
	movslq	72(%rsp), %rdx          # 4-byte Folded Reload
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movw	%cx, (%r10,%rdx,2)
	movq	%r10, %r14
	movq	%r14, 200(%rsp)         # 8-byte Spill
	movq	%r9, %rsi
	movw	%cx, (%rdi,%rsi,2)
	movq	%rdi, %r10
	movq	%r10, 80(%rsp)          # 8-byte Spill
	movq	%r11, %rbp
	movw	%cx, (%r15,%rbp,2)
	movq	%r13, %rbx
	movw	%cx, (%r12,%rbx,2)
	movq	%r12, 120(%rsp)         # 8-byte Spill
	movslq	184(%rsp), %rdx         # 4-byte Folded Reload
	shlq	$5, %rdx
	movq	64(%rsp), %rdi          # 8-byte Reload
	leaq	104(%rdi,%rdx), %rdx
	movq	152(%rsp), %r11         # 8-byte Reload
	movw	%cx, (%rdx,%r11,2)
	movq	%rdx, %r15
	movzwl	34(%rsp), %r13d
	addl	%r13d, %eax
	leal	2(%rax,%r8,2), %eax
	shrl	$2, %eax
	movslq	192(%rsp), %rcx         # 4-byte Folded Reload
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movw	%ax, (%r14,%rcx,2)
	movq	128(%rsp), %r9          # 8-byte Reload
	movw	%ax, (%r10,%r9,2)
	movq	136(%rsp), %rdx         # 8-byte Reload
	movw	%ax, (%rdx,%rsi,2)
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	movw	%ax, (%r12,%rbp,2)
	movq	%rbp, %r14
	movq	%r14, 104(%rsp)         # 8-byte Spill
	movw	%ax, (%r15,%rbx,2)
	movq	%r15, %r10
	movq	%r10, 168(%rsp)         # 8-byte Spill
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	movslq	56(%rsp), %rcx          # 4-byte Folded Reload
	shlq	$5, %rcx
	leaq	104(%rdi,%rcx), %rcx
	movw	%ax, (%rcx,%r11,2)
	movq	%rcx, %rbp
	movq	%rbp, 192(%rsp)         # 8-byte Spill
	movzwl	(%rsp), %r12d
	addl	%r12d, %r8d
	leal	2(%r8,%r13,2), %eax
	shrl	$2, %eax
	movslq	160(%rsp), %rcx         # 4-byte Folded Reload
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	200(%rsp), %r15         # 8-byte Reload
	movw	%ax, (%r15,%rcx,2)
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movw	%ax, (%rcx,%rdi,2)
	movw	%ax, (%rdx,%r9,2)
	movq	120(%rsp), %rdx         # 8-byte Reload
	movw	%ax, (%rdx,%rsi,2)
	movw	%ax, (%r10,%r14,2)
	movw	%ax, (%rbp,%rbx,2)
	movslq	176(%rsp), %rcx         # 4-byte Folded Reload
	shlq	$5, %rcx
	movq	64(%rsp), %r8           # 8-byte Reload
	leaq	104(%r8,%rcx), %rcx
	movw	%ax, (%rcx,%r11,2)
	movq	%rcx, %r9
	movq	%r9, 160(%rsp)          # 8-byte Spill
	movzwl	2(%rsp), %r10d
	addl	%r10d, %r13d
	leal	2(%r13,%r12,2), %eax
	shrl	$2, %eax
	movslq	112(%rsp), %rcx         # 4-byte Folded Reload
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movw	%ax, (%r15,%rcx,2)
	movq	88(%rsp), %rsi          # 8-byte Reload
	shlq	$5, %rsi
	leaq	104(%r8,%rsi), %r13
	movq	80(%rsp), %r11          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	movw	%ax, (%r11,%rbx,2)
	movq	136(%rsp), %r14         # 8-byte Reload
	movw	%ax, (%r14,%rdi,2)
	movq	128(%rsp), %rbp         # 8-byte Reload
	movw	%ax, (%rdx,%rbp,2)
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	168(%rsp), %rsi         # 8-byte Reload
	movw	%ax, (%rsi,%rdi,2)
	movq	104(%rsp), %r8          # 8-byte Reload
	movq	192(%rsp), %rcx         # 8-byte Reload
	movw	%ax, (%rcx,%r8,2)
	movq	144(%rsp), %r15         # 8-byte Reload
	movw	%ax, (%r9,%r15,2)
	movq	152(%rsp), %rdx         # 8-byte Reload
	movw	%ax, (%r13,%rdx,2)
	movzwl	4(%rsp), %eax
	addl	%eax, %r12d
	leal	2(%r12,%r10,2), %edx
	shrl	$2, %edx
	movq	112(%rsp), %r9          # 8-byte Reload
	movw	%dx, (%r11,%r9,2)
	movw	%dx, (%r14,%rbx,2)
	movq	120(%rsp), %r11         # 8-byte Reload
	movq	72(%rsp), %r14          # 8-byte Reload
	movw	%dx, (%r11,%r14,2)
	movw	%dx, (%rsi,%rbp,2)
	movq	%rsi, %rbx
	movw	%dx, (%rcx,%rdi,2)
	movq	%rcx, %r12
	movq	160(%rsp), %rsi         # 8-byte Reload
	movw	%dx, (%rsi,%r8,2)
	movw	%dx, (%r13,%r15,2)
	movzwl	6(%rsp), %edx
	addl	%edx, %r10d
	leal	2(%r10,%rax,2), %ecx
	shrl	$2, %ecx
	movq	136(%rsp), %rdi         # 8-byte Reload
	movw	%cx, (%rdi,%r9,2)
	movq	%r9, %r10
	movq	%r11, %r8
	movq	56(%rsp), %rdi          # 8-byte Reload
	movw	%cx, (%r8,%rdi,2)
	movq	%rbx, %r9
	movw	%cx, (%r9,%r14,2)
	movq	%r12, %rbx
	movw	%cx, (%rbx,%rbp,2)
	movq	96(%rsp), %r12          # 8-byte Reload
	movq	%rsi, %r15
	movw	%cx, (%r15,%r12,2)
	movq	104(%rsp), %rsi         # 8-byte Reload
	movw	%cx, (%r13,%rsi,2)
	movzwl	8(%rsp), %ecx
	addl	%ecx, %eax
	leal	2(%rax,%rdx,2), %eax
	shrl	$2, %eax
	movq	%r10, %r11
	movw	%ax, (%r8,%r11,2)
	movw	%ax, (%r9,%rdi,2)
	movw	%ax, (%rbx,%r14,2)
	movw	%ax, (%r15,%rbp,2)
	movw	%ax, (%r13,%r12,2)
	movzwl	10(%rsp), %eax
	addl	%eax, %edx
	leal	2(%rdx,%rcx,2), %edx
	shrl	$2, %edx
	movw	%dx, (%r9,%r11,2)
	movq	%rbx, %rsi
	movw	%dx, (%rsi,%rdi,2)
	movw	%dx, (%r15,%r14,2)
	movw	%dx, (%r13,%rbp,2)
	movzwl	12(%rsp), %edx
	addl	%edx, %ecx
	leal	2(%rcx,%rax,2), %ecx
	shrl	$2, %ecx
	movq	%r11, %rbx
	movw	%cx, (%rsi,%rbx,2)
	movq	%r15, %rsi
	movw	%cx, (%rsi,%rdi,2)
	movw	%cx, (%r13,%r14,2)
	movzwl	14(%rsp), %ecx
	addl	%ecx, %eax
	leal	2(%rax,%rdx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%rsi,%rbx,2)
	movw	%ax, (%r13,%rdi,2)
	movzwl	16(%rsp), %eax
	addl	%edx, %eax
	leal	2(%rax,%rcx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%r13,%rbx,2)
	jmp	.LBB0_85
.LBB0_47:
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movl	$1, %eax
	jmp	.LBB0_86
.LBB0_48:
	testl	%r11d, %r11d
	jne	.LBB0_50
# BB#49:
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
.LBB0_50:                               # %.preheader1434
	movslq	80(%rsp), %rax          # 4-byte Folded Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
	shlq	$5, %rcx
	movq	64(%rsp), %rsi          # 8-byte Reload
	leaq	104(%rsi,%rcx), %rcx
	movzwl	34(%rsp), %edx
	movd	%edx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, (%rcx,%rax,2)
	movq	72(%rsp), %rcx          # 8-byte Reload
	shlq	$5, %rcx
	leaq	104(%rsi,%rcx), %rcx
	movd	36(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, (%rcx,%rax,2)
	movq	96(%rsp), %rcx          # 8-byte Reload
	shlq	$5, %rcx
	leaq	104(%rsi,%rcx), %rcx
	movzwl	38(%rsp), %edx
	movd	%edx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, (%rcx,%rax,2)
	movq	152(%rsp), %rcx         # 8-byte Reload
	shlq	$5, %rcx
	leaq	104(%rsi,%rcx), %rcx
	movd	40(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, (%rcx,%rax,2)
	movq	160(%rsp), %rcx         # 8-byte Reload
	shlq	$5, %rcx
	leaq	104(%rsi,%rcx), %rcx
	movzwl	42(%rsp), %edx
	movd	%edx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, (%rcx,%rax,2)
	movq	120(%rsp), %rcx         # 8-byte Reload
	shlq	$5, %rcx
	leaq	104(%rsi,%rcx), %rcx
	movd	44(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, (%rcx,%rax,2)
	movq	144(%rsp), %rcx         # 8-byte Reload
	shlq	$5, %rcx
	leaq	104(%rsi,%rcx), %rcx
	movzwl	46(%rsp), %edx
	movd	%edx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, (%rcx,%rax,2)
	movq	176(%rsp), %rcx         # 8-byte Reload
	shlq	$5, %rcx
	leaq	104(%rsi,%rcx), %rcx
	movd	48(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, (%rcx,%rax,2)
	jmp	.LBB0_85
.LBB0_51:
	movl	%r11d, %eax
	testl	%eax, %eax
	movl	128(%rsp), %r11d        # 4-byte Reload
	je	.LBB0_73
# BB#52:
	testl	%r11d, %r11d
	je	.LBB0_73
# BB#53:
	movl	%r10d, %edi
	movzwl	2(%rsp), %ecx
	movzwl	4(%rsp), %eax
	addl	%ecx, %eax
	movzwl	6(%rsp), %ecx
	addl	%ecx, %eax
	movzwl	8(%rsp), %ecx
	addl	%ecx, %eax
	movzwl	10(%rsp), %ecx
	addl	%ecx, %eax
	movzwl	12(%rsp), %ecx
	addl	%ecx, %eax
	movzwl	14(%rsp), %ecx
	addl	%ecx, %eax
	movzwl	16(%rsp), %ecx
	addl	%ecx, %eax
	movzwl	34(%rsp), %ecx
	addl	%ecx, %eax
	movzwl	36(%rsp), %ecx
	addl	%ecx, %eax
	movzwl	38(%rsp), %ecx
	addl	%ecx, %eax
	movzwl	40(%rsp), %ecx
	addl	%ecx, %eax
	movzwl	42(%rsp), %ecx
	addl	%ecx, %eax
	movzwl	44(%rsp), %ecx
	addl	%ecx, %eax
	movzwl	46(%rsp), %ecx
	addl	%ecx, %eax
	movzwl	48(%rsp), %ecx
	leal	8(%rcx,%rax), %eax
	sarl	$4, %eax
	movq	64(%rsp), %r9           # 8-byte Reload
	jmp	.LBB0_81
.LBB0_54:
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movl	%r15d, 168(%rsp)        # 4-byte Spill
	movl	%ecx, %r15d
	movl	%edx, 96(%rsp)          # 4-byte Spill
	movl	%edi, 160(%rsp)         # 4-byte Spill
	movl	%r12d, 152(%rsp)        # 4-byte Spill
	movl	%ebx, 112(%rsp)         # 4-byte Spill
	movl	%r10d, 136(%rsp)        # 4-byte Spill
	cmpl	$0, 128(%rsp)           # 4-byte Folded Reload
	movl	%r14d, 192(%rsp)        # 4-byte Spill
	movl	%ebp, %ecx
	jne	.LBB0_56
# BB#55:
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%r9d, %ebp
	movl	%ecx, %r13d
	movl	%r8d, %ebx
	callq	printf
	movl	%ebx, %r8d
	movl	%r13d, %ecx
	movl	%ebp, %r9d
.LBB0_56:
	movl	%ecx, 184(%rsp)         # 4-byte Spill
	movzwl	2(%rsp), %ecx
	movzwl	6(%rsp), %eax
	addl	%eax, %ecx
	movzwl	4(%rsp), %esi
	leal	2(%rcx,%rsi,2), %ecx
	shrl	$2, %ecx
	movslq	80(%rsp), %rdi          # 4-byte Folded Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	shlq	$5, %rdx
	movq	64(%rsp), %r10          # 8-byte Reload
	leaq	104(%r10,%rdx), %r13
	movw	%cx, (%r13,%rdi,2)
	movzwl	8(%rsp), %edx
	addl	%edx, %esi
	leal	2(%rsi,%rax,2), %ecx
	shrl	$2, %ecx
	movslq	%r15d, %rsi
	movw	%cx, (%r13,%rsi,2)
	movq	%rsi, %r12
	movslq	%r8d, %rsi
	shlq	$5, %rsi
	leaq	104(%r10,%rsi), %rsi
	movw	%cx, (%rsi,%rdi,2)
	movq	%rsi, %rbp
	movzwl	10(%rsp), %ecx
	addl	%ecx, %eax
	leal	2(%rax,%rdx,2), %eax
	shrl	$2, %eax
	movslq	104(%rsp), %rsi         # 4-byte Folded Reload
	movw	%ax, (%r13,%rsi,2)
	movq	%rsi, %r11
	movw	%ax, (%rbp,%r12,2)
	movslq	56(%rsp), %rsi          # 4-byte Folded Reload
	shlq	$5, %rsi
	leaq	104(%r10,%rsi), %rsi
	movw	%ax, (%rsi,%rdi,2)
	movq	%rsi, %r14
	movzwl	12(%rsp), %eax
	addl	%eax, %edx
	leal	2(%rdx,%rcx,2), %edx
	shrl	$2, %edx
	movslq	%r9d, %rsi
	movw	%dx, (%r13,%rsi,2)
	movq	%rsi, %r8
	movq	%r8, 144(%rsp)          # 8-byte Spill
	movw	%dx, (%rbp,%r11,2)
	movq	%rbp, %r15
	movq	%r11, %r9
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movq	%r14, %rbp
	movq	%rbp, 128(%rsp)         # 8-byte Spill
	movw	%dx, (%rbp,%r12,2)
	movslq	184(%rsp), %rsi         # 4-byte Folded Reload
	shlq	$5, %rsi
	leaq	104(%r10,%rsi), %rbx
	movq	%rdi, %rsi
	movq	%rsi, 176(%rsp)         # 8-byte Spill
	movw	%dx, (%rbx,%rsi,2)
	movq	%rbx, %r11
	movzwl	14(%rsp), %edi
	addl	%edi, %ecx
	leal	2(%rcx,%rax,2), %ecx
	shrl	$2, %ecx
	movslq	72(%rsp), %rdx          # 4-byte Folded Reload
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movw	%cx, (%r13,%rdx,2)
	movq	%r13, %r14
	movq	%r14, 200(%rsp)         # 8-byte Spill
	movw	%cx, (%r15,%r8,2)
	movq	%r15, %r8
	movq	%r8, 120(%rsp)          # 8-byte Spill
	movw	%cx, (%rbp,%r9,2)
	movq	%r12, %rbx
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movw	%cx, (%r11,%rbx,2)
	movslq	168(%rsp), %rdx         # 4-byte Folded Reload
	shlq	$5, %rdx
	leaq	104(%r10,%rdx), %r15
	movw	%cx, (%r15,%rsi,2)
	movzwl	16(%rsp), %r13d
	addl	%r13d, %eax
	leal	2(%rax,%rdi,2), %eax
	shrl	$2, %eax
	movslq	192(%rsp), %rcx         # 4-byte Folded Reload
	movw	%ax, (%r14,%rcx,2)
	movq	%rcx, %r12
	movq	56(%rsp), %rbp          # 8-byte Reload
	movw	%ax, (%r8,%rbp,2)
	movq	144(%rsp), %r9          # 8-byte Reload
	movq	128(%rsp), %rsi         # 8-byte Reload
	movw	%ax, (%rsi,%r9,2)
	movq	88(%rsp), %rdx          # 8-byte Reload
	movw	%ax, (%r11,%rdx,2)
	movq	%r11, %r14
	movq	%r14, 184(%rsp)         # 8-byte Spill
	movw	%ax, (%r15,%rbx,2)
	movslq	152(%rsp), %rcx         # 4-byte Folded Reload
	shlq	$5, %rcx
	movq	%r10, %r8
	leaq	104(%r8,%rcx), %rcx
	movq	176(%rsp), %r10         # 8-byte Reload
	movw	%ax, (%rcx,%r10,2)
	movq	%rcx, %r11
	movq	%r11, 72(%rsp)          # 8-byte Spill
	movzwl	18(%rsp), %ebx
	addl	%ebx, %edi
	leal	2(%rdi,%r13,2), %eax
	shrl	$2, %eax
	movslq	160(%rsp), %rcx         # 4-byte Folded Reload
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	200(%rsp), %rdi         # 8-byte Reload
	movw	%ax, (%rdi,%rcx,2)
	movq	120(%rsp), %rcx         # 8-byte Reload
	movw	%ax, (%rcx,%r12,2)
	movw	%ax, (%rsi,%rbp,2)
	movw	%ax, (%r14,%r9,2)
	movw	%ax, (%r15,%rdx,2)
	movq	%r15, %r14
	movq	80(%rsp), %rcx          # 8-byte Reload
	movw	%ax, (%r11,%rcx,2)
	movslq	96(%rsp), %rcx          # 4-byte Folded Reload
	shlq	$5, %rcx
	leaq	104(%r8,%rcx), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movw	%ax, (%rcx,%r10,2)
	movzwl	20(%rsp), %r10d
	addl	%r10d, %r13d
	leal	2(%r13,%rbx,2), %eax
	shrl	$2, %eax
	movslq	112(%rsp), %rcx         # 4-byte Folded Reload
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movw	%ax, (%rdi,%rcx,2)
	movslq	136(%rsp), %rsi         # 4-byte Folded Reload
	shlq	$5, %rsi
	leaq	104(%r8,%rsi), %r8
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	120(%rsp), %r15         # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	movw	%ax, (%r15,%rcx,2)
	movq	%r12, %rbp
	movq	128(%rsp), %rcx         # 8-byte Reload
	movw	%ax, (%rcx,%rbp,2)
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	184(%rsp), %r9          # 8-byte Reload
	movw	%ax, (%r9,%rsi,2)
	movq	144(%rsp), %rdi         # 8-byte Reload
	movw	%ax, (%r14,%rdi,2)
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	movw	%ax, (%r12,%rdx,2)
	movq	96(%rsp), %r13          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movw	%ax, (%r13,%rdx,2)
	movq	176(%rsp), %rdx         # 8-byte Reload
	movw	%ax, (%r8,%rdx,2)
	movzwl	22(%rsp), %eax
	addl	%eax, %ebx
	leal	2(%rbx,%r10,2), %edx
	shrl	$2, %edx
	movq	112(%rsp), %rbx         # 8-byte Reload
	movw	%dx, (%r15,%rbx,2)
	movq	104(%rsp), %r8          # 8-byte Reload
	movw	%dx, (%rcx,%r8,2)
	movq	%rcx, %r15
	movq	%rbp, %r11
	movw	%dx, (%r9,%r11,2)
	movw	%dx, (%r14,%rsi,2)
	movq	%r12, %rsi
	movw	%dx, (%rsi,%rdi,2)
	movq	%rdi, %rbx
	movq	%r13, %r12
	movq	88(%rsp), %rcx          # 8-byte Reload
	movw	%dx, (%r12,%rcx,2)
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movw	%dx, (%rbp,%rcx,2)
	movzwl	24(%rsp), %edx
	addl	%edx, %r10d
	leal	2(%r10,%rax,2), %ecx
	shrl	$2, %ecx
	movq	112(%rsp), %r13         # 8-byte Reload
	movw	%cx, (%r15,%r13,2)
	movq	%r9, %r15
	movw	%cx, (%r15,%r8,2)
	movq	%r14, %r10
	movw	%cx, (%r10,%r11,2)
	movq	56(%rsp), %rdi          # 8-byte Reload
	movw	%cx, (%rsi,%rdi,2)
	movq	%r12, %r9
	movq	%rbx, %r12
	movw	%cx, (%r9,%r12,2)
	movq	88(%rsp), %rbx          # 8-byte Reload
	movw	%cx, (%rbp,%rbx,2)
	movzwl	26(%rsp), %ecx
	addl	%ecx, %eax
	leal	2(%rax,%rdx,2), %eax
	shrl	$2, %eax
	movq	%r13, %r14
	movw	%ax, (%r15,%r14,2)
	movw	%ax, (%r10,%r8,2)
	movw	%ax, (%rsi,%r11,2)
	movw	%ax, (%r9,%rdi,2)
	movw	%ax, (%rbp,%r12,2)
	movzwl	28(%rsp), %eax
	addl	%eax, %edx
	leal	2(%rdx,%rcx,2), %edx
	shrl	$2, %edx
	movw	%dx, (%r10,%r14,2)
	movw	%dx, (%rsi,%r8,2)
	movw	%dx, (%r9,%r11,2)
	movw	%dx, (%rbp,%rdi,2)
	movzwl	30(%rsp), %edx
	addl	%edx, %ecx
	leal	2(%rcx,%rax,2), %ecx
	shrl	$2, %ecx
	movw	%cx, (%rsi,%r14,2)
	movw	%cx, (%r9,%r8,2)
	movw	%cx, (%rbp,%r11,2)
	movzwl	32(%rsp), %ecx
	addl	%ecx, %eax
	leal	2(%rax,%rdx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%r9,%r14,2)
	movw	%ax, (%rbp,%r8,2)
	leal	(%rcx,%rcx,2), %eax
	leal	2(%rdx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, (%rbp,%r14,2)
	jmp	.LBB0_85
.LBB0_57:
	movl	%ebx, 112(%rsp)         # 4-byte Spill
	movl	%r9d, 120(%rsp)         # 4-byte Spill
	movl	%r10d, 136(%rsp)        # 4-byte Spill
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movl	%edi, 160(%rsp)         # 4-byte Spill
	movl	%r8d, 176(%rsp)         # 4-byte Spill
	movl	%ebp, 184(%rsp)         # 4-byte Spill
	movl	%r12d, 152(%rsp)        # 4-byte Spill
	testl	%r13d, %r13d
	movl	%r14d, %r10d
	je	.LBB0_60
# BB#58:
	testl	%r11d, %r11d
	je	.LBB0_60
# BB#59:
	cmpl	$0, 128(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_61
.LBB0_60:
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %esi
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movl	%r10d, %ebx
	movl	%edx, %r14d
	movl	%ecx, %ebp
	callq	printf
	movl	%ebp, %ecx
	movl	%r14d, %edx
	movl	%ebx, %r10d
.LBB0_61:
	movzwl	(%rsp), %eax
	movzwl	2(%rsp), %ebx
	leal	1(%rax,%rbx), %eax
	shrl	%eax
	movslq	%edx, %rdx
	movslq	120(%rsp), %r14         # 4-byte Folded Reload
	shlq	$5, %rdx
	movq	64(%rsp), %rsi          # 8-byte Reload
	leaq	104(%rsi,%rdx), %rdx
	movw	%ax, (%rdx,%r14,2)
	movq	%rdx, %r8
	movslq	%r15d, %rdx
	movslq	104(%rsp), %r12         # 4-byte Folded Reload
	shlq	$5, %rdx
	leaq	104(%rsi,%rdx), %rdx
	movw	%ax, (%rdx,%r12,2)
	movq	%rdx, %r15
	movslq	56(%rsp), %rdx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	shlq	$5, %rdx
	leaq	104(%rsi,%rdx), %rdx
	movw	%ax, (%rdx,%rcx,2)
	movq	%rdx, %r13
	movq	%rcx, %rbp
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movslq	80(%rsp), %rcx          # 4-byte Folded Reload
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rdx          # 8-byte Reload
	shlq	$5, %rdx
	leaq	104(%rsi,%rdx), %rdi
	movw	%ax, (%rdi,%rcx,2)
	movzwl	4(%rsp), %eax
	leal	1(%rbx,%rax), %ecx
	shrl	%ecx
	movslq	72(%rsp), %r9           # 4-byte Folded Reload
	movq	%r8, %rbx
	movw	%cx, (%rbx,%r9,2)
	movw	%cx, (%r15,%r14,2)
	movq	%r12, 144(%rsp)         # 8-byte Spill
	movw	%cx, (%r13,%r12,2)
	movw	%cx, (%rdi,%rbp,2)
	movzwl	6(%rsp), %ecx
	leal	1(%rax,%rcx), %eax
	shrl	%eax
	movslq	%r10d, %r10
	movw	%ax, (%rbx,%r10,2)
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movq	%r15, 104(%rsp)         # 8-byte Spill
	movw	%ax, (%r15,%r9,2)
	movw	%ax, (%r13,%r14,2)
	movw	%ax, (%rdi,%r12,2)
	movzwl	8(%rsp), %eax
	leal	1(%rcx,%rax), %ecx
	shrl	%ecx
	movslq	160(%rsp), %rbp         # 4-byte Folded Reload
	movw	%cx, (%rbx,%rbp,2)
	movw	%cx, (%r15,%r10,2)
	movq	%r13, %rdx
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movw	%cx, (%rdx,%r9,2)
	movw	%cx, (%rdi,%r14,2)
	movzwl	10(%rsp), %ecx
	leal	1(%rax,%rcx), %eax
	shrl	%eax
	movslq	112(%rsp), %r13         # 4-byte Folded Reload
	movw	%ax, (%rbx,%r13,2)
	movw	%ax, (%r15,%rbp,2)
	movw	%ax, (%rdx,%r10,2)
	movw	%ax, (%rdi,%r9,2)
	movzwl	12(%rsp), %eax
	leal	1(%rcx,%rax), %ecx
	shrl	%ecx
	movw	%cx, (%r15,%r13,2)
	movw	%cx, (%rdx,%rbp,2)
	movw	%cx, (%rdi,%r10,2)
	movzwl	14(%rsp), %ecx
	leal	1(%rax,%rcx), %eax
	shrl	%eax
	movw	%ax, (%rdx,%r13,2)
	movw	%ax, (%rdi,%rbp,2)
	movzwl	16(%rsp), %eax
	leal	1(%rcx,%rax), %eax
	shrl	%eax
	movw	%ax, (%rdi,%r13,2)
	movslq	136(%rsp), %rax         # 4-byte Folded Reload
	shlq	$5, %rax
	leaq	104(%rsi,%rax), %r12
	movslq	152(%rsp), %rax         # 4-byte Folded Reload
	shlq	$5, %rax
	leaq	104(%rsi,%rax), %r11
	movslq	184(%rsp), %rax         # 4-byte Folded Reload
	shlq	$5, %rax
	leaq	104(%rsi,%rax), %rbx
	movslq	176(%rsp), %rax         # 4-byte Folded Reload
	shlq	$5, %rax
	leaq	104(%rsi,%rax), %r8
	movzwl	34(%rsp), %ecx
	movzwl	2(%rsp), %edx
	addl	%edx, %ecx
	movzwl	(%rsp), %eax
	leal	2(%rcx,%rax,2), %ecx
	shrl	$2, %ecx
	movw	%cx, (%r12,%r14,2)
	movq	144(%rsp), %rdi         # 8-byte Reload
	movw	%cx, (%r11,%rdi,2)
	movq	96(%rsp), %rsi          # 8-byte Reload
	movw	%cx, (%rbx,%rsi,2)
	movq	56(%rsp), %r15          # 8-byte Reload
	movw	%cx, (%r8,%r15,2)
	movzwl	4(%rsp), %ecx
	addl	%ecx, %eax
	leal	2(%rax,%rdx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%r12,%r9,2)
	movw	%ax, (%r11,%r14,2)
	movw	%ax, (%rbx,%rdi,2)
	movw	%ax, (%r8,%rsi,2)
	movzwl	6(%rsp), %eax
	addl	%eax, %edx
	leal	2(%rdx,%rcx,2), %edx
	shrl	$2, %edx
	movw	%dx, (%r12,%r10,2)
	movw	%dx, (%r11,%r9,2)
	movw	%dx, (%rbx,%r14,2)
	movw	%dx, (%r8,%rdi,2)
	movq	%rdi, %r15
	movzwl	8(%rsp), %edx
	addl	%edx, %ecx
	leal	2(%rcx,%rax,2), %ecx
	shrl	$2, %ecx
	movw	%cx, (%r12,%rbp,2)
	movw	%cx, (%r11,%r10,2)
	movw	%cx, (%rbx,%r9,2)
	movw	%cx, (%r8,%r14,2)
	movzwl	10(%rsp), %ecx
	addl	%ecx, %eax
	leal	2(%rax,%rdx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%r12,%r13,2)
	movw	%ax, (%r11,%rbp,2)
	movw	%ax, (%rbx,%r10,2)
	movw	%ax, (%r8,%r9,2)
	movzwl	12(%rsp), %eax
	addl	%eax, %edx
	leal	2(%rdx,%rcx,2), %edx
	shrl	$2, %edx
	movw	%dx, (%r11,%r13,2)
	movw	%dx, (%rbx,%rbp,2)
	movw	%dx, (%r8,%r10,2)
	movzwl	14(%rsp), %edx
	addl	%edx, %ecx
	leal	2(%rcx,%rax,2), %ecx
	shrl	$2, %ecx
	movw	%cx, (%rbx,%r13,2)
	movw	%cx, (%r8,%rbp,2)
	movzwl	16(%rsp), %ecx
	addl	%eax, %ecx
	leal	2(%rcx,%rdx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%r8,%r13,2)
	movzwl	36(%rsp), %eax
	movzwl	(%rsp), %ecx
	movzwl	34(%rsp), %edx
	addl	%eax, %ecx
	leal	2(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movq	128(%rsp), %r9          # 8-byte Reload
	movw	%cx, (%r9,%r15,2)
	movq	104(%rsp), %r8          # 8-byte Reload
	movw	%cx, (%r8,%rsi,2)
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	120(%rsp), %rbp         # 8-byte Reload
	movw	%cx, (%rbp,%rdi,2)
	movzwl	38(%rsp), %ecx
	addl	%ecx, %edx
	leal	2(%rdx,%rax,2), %edx
	shrl	$2, %edx
	movw	%dx, (%r12,%r15,2)
	movw	%dx, (%r11,%rsi,2)
	movq	%rsi, %r10
	movw	%dx, (%rbx,%rdi,2)
	movq	%rdi, %rsi
	movzwl	40(%rsp), %edx
	addl	%edx, %eax
	leal	2(%rax,%rcx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%r9,%r10,2)
	movq	%r9, %rdi
	movw	%ax, (%r8,%rsi,2)
	movzwl	42(%rsp), %eax
	addl	%eax, %ecx
	leal	2(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, (%r12,%r10,2)
	movw	%cx, (%r11,%rsi,2)
	movzwl	44(%rsp), %ecx
	addl	%ecx, %edx
	leal	2(%rdx,%rax,2), %edx
	shrl	$2, %edx
	movw	%dx, (%rdi,%rsi,2)
	movzwl	46(%rsp), %edx
	addl	%eax, %edx
	leal	2(%rdx,%rcx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%r12,%rsi,2)
	jmp	.LBB0_85
.LBB0_62:
	movl	%ebx, 112(%rsp)         # 4-byte Spill
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movl	%ecx, 144(%rsp)         # 4-byte Spill
	movl	%edx, 96(%rsp)          # 4-byte Spill
	movl	%r9d, 120(%rsp)         # 4-byte Spill
	movl	%r14d, 192(%rsp)        # 4-byte Spill
	movl	%r12d, 152(%rsp)        # 4-byte Spill
	movl	%r10d, 136(%rsp)        # 4-byte Spill
	testl	%r13d, %r13d
	movl	%r15d, %r9d
	movl	%edi, %ebx
	je	.LBB0_65
# BB#63:
	testl	%r11d, %r11d
	je	.LBB0_65
# BB#64:
	cmpl	$0, 128(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_66
.LBB0_65:
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %esi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movl	%r8d, %r15d
	movl	%r9d, %r12d
	callq	printf
	movl	%r12d, %r9d
	movl	%r15d, %r8d
.LBB0_66:
	movzwl	34(%rsp), %eax
	movzwl	(%rsp), %ecx
	leal	1(%rax,%rcx), %ecx
	shrl	%ecx
	movslq	%ebp, %rdx
	movslq	%ebx, %rsi
	shlq	$5, %rdx
	movq	64(%rsp), %r14          # 8-byte Reload
	leaq	104(%r14,%rdx), %r12
	movw	%cx, (%r12,%rsi,2)
	movq	%rsi, %r10
	movslq	56(%rsp), %rdx          # 4-byte Folded Reload
	movslq	72(%rsp), %rsi          # 4-byte Folded Reload
	shlq	$5, %rdx
	leaq	104(%r14,%rdx), %rdx
	movw	%cx, (%rdx,%rsi,2)
	movq	%rdx, %rbp
	movq	%rsi, %r11
	movslq	%r8d, %rdx
	movslq	104(%rsp), %rbx         # 4-byte Folded Reload
	shlq	$5, %rdx
	leaq	104(%r14,%rdx), %rdx
	movw	%cx, (%rdx,%rbx,2)
	movq	%rdx, %rdi
	movq	%rdi, 160(%rsp)         # 8-byte Spill
	movslq	80(%rsp), %rsi          # 4-byte Folded Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	shlq	$5, %rdx
	leaq	104(%r14,%rdx), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movw	%cx, (%rdx,%rsi,2)
	movzwl	36(%rsp), %edx
	leal	1(%rdx,%rax), %eax
	shrl	%eax
	movslq	%r9d, %rcx
	shlq	$5, %rcx
	leaq	104(%r14,%rcx), %r15
	movw	%ax, (%r15,%r10,2)
	movw	%ax, (%r12,%r11,2)
	movq	%rbp, 176(%rsp)         # 8-byte Spill
	movw	%ax, (%rbp,%rbx,2)
	movw	%ax, (%rdi,%rsi,2)
	movzwl	38(%rsp), %eax
	leal	1(%rax,%rdx), %edi
	shrl	%edi
	movslq	152(%rsp), %rdx         # 4-byte Folded Reload
	shlq	$5, %rdx
	leaq	104(%r14,%rdx), %r9
	movq	%r10, %rcx
	movw	%di, (%r9,%rcx,2)
	movw	%di, (%r15,%r11,2)
	movw	%di, (%r12,%rbx,2)
	movw	%di, (%rbp,%rsi,2)
	movzwl	40(%rsp), %edx
	leal	1(%rdx,%rax), %eax
	shrl	%eax
	movslq	96(%rsp), %rdi          # 4-byte Folded Reload
	shlq	$5, %rdi
	leaq	104(%r14,%rdi), %r8
	movw	%ax, (%r8,%rcx,2)
	movq	%rcx, %rdi
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movq	%r11, 72(%rsp)          # 8-byte Spill
	movw	%ax, (%r9,%r11,2)
	movq	%rbx, %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movw	%ax, (%r15,%rcx,2)
	movw	%ax, (%r12,%rsi,2)
	movslq	136(%rsp), %rax         # 4-byte Folded Reload
	shlq	$5, %rax
	leaq	104(%r14,%rax), %rbp
	movzwl	42(%rsp), %eax
	leal	1(%rax,%rdx), %ebx
	shrl	%ebx
	movw	%bx, (%rbp,%rdi,2)
	movw	%bx, (%r8,%r11,2)
	movw	%bx, (%r9,%rcx,2)
	movw	%bx, (%r15,%rsi,2)
	movzwl	44(%rsp), %ebx
	leal	1(%rbx,%rax), %eax
	shrl	%eax
	movw	%ax, (%rbp,%r11,2)
	movw	%ax, (%r8,%rcx,2)
	movw	%ax, (%r9,%rsi,2)
	movzwl	46(%rsp), %eax
	leal	1(%rax,%rbx), %ebx
	shrl	%ebx
	movw	%bx, (%rbp,%rcx,2)
	movw	%bx, (%r8,%rsi,2)
	movzwl	48(%rsp), %ebx
	leal	1(%rbx,%rax), %eax
	shrl	%eax
	movw	%ax, (%rbp,%rsi,2)
	movzwl	34(%rsp), %edi
	movzwl	2(%rsp), %esi
	leal	2(%rdi,%rsi), %esi
	movzwl	(%rsp), %ecx
	leal	(%rsi,%rcx,2), %eax
	shrl	$2, %eax
	movslq	112(%rsp), %r13         # 4-byte Folded Reload
	movw	%ax, (%r12,%r13,2)
	movslq	192(%rsp), %r10         # 4-byte Folded Reload
	movq	176(%rsp), %rsi         # 8-byte Reload
	movw	%ax, (%rsi,%r10,2)
	movslq	120(%rsp), %rbx         # 4-byte Folded Reload
	movq	160(%rsp), %rdx         # 8-byte Reload
	movw	%ax, (%rdx,%rbx,2)
	movslq	144(%rsp), %r14         # 4-byte Folded Reload
	movq	56(%rsp), %r11          # 8-byte Reload
	movw	%ax, (%r11,%r14,2)
	movzwl	36(%rsp), %eax
	addl	%eax, %ecx
	leal	2(%rcx,%rdi,2), %ecx
	shrl	$2, %ecx
	movw	%cx, (%r15,%r13,2)
	movw	%cx, (%r12,%r10,2)
	movw	%cx, (%rsi,%rbx,2)
	movw	%cx, (%rdx,%r14,2)
	movq	%rdx, %r11
	movzwl	38(%rsp), %ecx
	leal	2(%rdi,%rcx), %edx
	leal	(%rdx,%rax,2), %edx
	shrl	$2, %edx
	movw	%dx, (%r9,%r13,2)
	movw	%dx, (%r15,%r10,2)
	movw	%dx, (%r12,%rbx,2)
	movw	%dx, (%rsi,%r14,2)
	movzwl	40(%rsp), %edx
	addl	%edx, %eax
	leal	2(%rax,%rcx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%r8,%r13,2)
	movw	%ax, (%r9,%r10,2)
	movw	%ax, (%r15,%rbx,2)
	movw	%ax, (%r12,%r14,2)
	movzwl	42(%rsp), %eax
	addl	%eax, %ecx
	leal	2(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movw	%cx, (%rbp,%r13,2)
	movw	%cx, (%r8,%r10,2)
	movw	%cx, (%r9,%rbx,2)
	movw	%cx, (%r15,%r14,2)
	movzwl	44(%rsp), %ecx
	addl	%ecx, %edx
	leal	2(%rdx,%rax,2), %edx
	shrl	$2, %edx
	movw	%dx, (%rbp,%r10,2)
	movw	%dx, (%r8,%rbx,2)
	movw	%dx, (%r9,%r14,2)
	movzwl	46(%rsp), %edx
	addl	%edx, %eax
	leal	2(%rax,%rcx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%rbp,%rbx,2)
	movw	%ax, (%r8,%r14,2)
	movzwl	48(%rsp), %eax
	addl	%ecx, %eax
	leal	2(%rax,%rdx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%rbp,%r14,2)
	movzwl	(%rsp), %eax
	movzwl	4(%rsp), %ecx
	movzwl	2(%rsp), %edx
	addl	%ecx, %eax
	leal	2(%rax,%rdx,2), %eax
	shrl	$2, %eax
	movq	128(%rsp), %r8          # 8-byte Reload
	movq	%rsi, %r9
	movw	%ax, (%r9,%r8,2)
	movq	%r11, %rbp
	movq	72(%rsp), %r11          # 8-byte Reload
	movw	%ax, (%rbp,%r11,2)
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	104(%rsp), %rsi         # 8-byte Reload
	movw	%ax, (%rdi,%rsi,2)
	movzwl	6(%rsp), %eax
	addl	%eax, %edx
	leal	2(%rdx,%rcx,2), %edx
	shrl	$2, %edx
	movw	%dx, (%r9,%r13,2)
	movw	%dx, (%rbp,%r10,2)
	movw	%dx, (%rdi,%rbx,2)
	movzwl	8(%rsp), %edx
	addl	%edx, %ecx
	leal	2(%rcx,%rax,2), %ecx
	shrl	$2, %ecx
	movw	%cx, (%rbp,%r8,2)
	movw	%cx, (%rdi,%r11,2)
	movzwl	10(%rsp), %ecx
	addl	%ecx, %eax
	leal	2(%rax,%rdx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%rbp,%r13,2)
	movw	%ax, (%rdi,%r10,2)
	movzwl	12(%rsp), %eax
	addl	%eax, %edx
	leal	2(%rdx,%rcx,2), %edx
	shrl	$2, %edx
	movw	%dx, (%rdi,%r8,2)
	movzwl	14(%rsp), %edx
	addl	%ecx, %edx
	leal	2(%rdx,%rax,2), %eax
	shrl	$2, %eax
	movw	%ax, (%rdi,%r13,2)
	jmp	.LBB0_85
.LBB0_67:
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movl	%ecx, %r13d
	movl	%edx, 96(%rsp)          # 4-byte Spill
	movl	%r9d, 120(%rsp)         # 4-byte Spill
	movl	%edi, 160(%rsp)         # 4-byte Spill
	movl	%r8d, 176(%rsp)         # 4-byte Spill
	movl	%ebp, 184(%rsp)         # 4-byte Spill
	movl	%r12d, 152(%rsp)        # 4-byte Spill
	movl	%ebx, 112(%rsp)         # 4-byte Spill
	movl	%r10d, 136(%rsp)        # 4-byte Spill
	cmpl	$0, 128(%rsp)           # 4-byte Folded Reload
	movl	%r14d, %ebx
	jne	.LBB0_69
# BB#68:
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %esi
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
.LBB0_69:
	movzwl	2(%rsp), %eax
	movzwl	4(%rsp), %edx
	leal	1(%rax,%rdx), %eax
	shrl	%eax
	movslq	80(%rsp), %r8           # 4-byte Folded Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
	shlq	$5, %rcx
	movq	64(%rsp), %r9           # 8-byte Reload
	leaq	104(%r9,%rcx), %rcx
	movw	%ax, (%rcx,%r8,2)
	movzwl	6(%rsp), %eax
	leal	1(%rdx,%rax), %esi
	shrl	%esi
	movslq	56(%rsp), %rdx          # 4-byte Folded Reload
	shlq	$5, %rdx
	leaq	104(%r9,%rdx), %rdx
	movw	%si, (%rdx,%r8,2)
	movslq	%r13d, %r14
	movw	%si, (%rcx,%r14,2)
	movzwl	8(%rsp), %edi
	leal	1(%rax,%rdi), %eax
	shrl	%eax
	movslq	%r15d, %rsi
	shlq	$5, %rsi
	leaq	104(%r9,%rsi), %rsi
	movw	%ax, (%rsi,%r8,2)
	movw	%ax, (%rdx,%r14,2)
	movslq	104(%rsp), %r12         # 4-byte Folded Reload
	movw	%ax, (%rcx,%r12,2)
	movzwl	10(%rsp), %eax
	leal	1(%rdi,%rax), %ebp
	shrl	%ebp
	movslq	96(%rsp), %rdi          # 4-byte Folded Reload
	shlq	$5, %rdi
	leaq	104(%r9,%rdi), %rdi
	movw	%bp, (%rdi,%r8,2)
	movw	%bp, (%rsi,%r14,2)
	movw	%bp, (%rdx,%r12,2)
	movslq	120(%rsp), %r13         # 4-byte Folded Reload
	movw	%bp, (%rcx,%r13,2)
	movzwl	12(%rsp), %ebp
	leal	1(%rax,%rbp), %eax
	shrl	%eax
	movw	%ax, (%rdi,%r14,2)
	movw	%ax, (%rsi,%r12,2)
	movw	%ax, (%rdx,%r13,2)
	movslq	72(%rsp), %r15          # 4-byte Folded Reload
	movw	%ax, (%rcx,%r15,2)
	movzwl	14(%rsp), %eax
	leal	1(%rbp,%rax), %ebp
	shrl	%ebp
	movw	%bp, (%rdi,%r12,2)
	movw	%bp, (%rsi,%r13,2)
	movw	%bp, (%rdx,%r15,2)
	movslq	%ebx, %r11
	movw	%bp, (%rcx,%r11,2)
	movzwl	16(%rsp), %ebx
	leal	1(%rax,%rbx), %eax
	shrl	%eax
	movw	%ax, (%rdi,%r13,2)
	movw	%ax, (%rsi,%r15,2)
	movw	%ax, (%rdx,%r11,2)
	movslq	160(%rsp), %rbp         # 4-byte Folded Reload
	movw	%ax, (%rcx,%rbp,2)
	movzwl	18(%rsp), %eax
	leal	1(%rbx,%rax), %ebx
	shrl	%ebx
	movw	%bx, (%rdi,%r15,2)
	movw	%bx, (%rsi,%r11,2)
	movw	%bx, (%rdx,%rbp,2)
	movslq	112(%rsp), %r10         # 4-byte Folded Reload
	movw	%bx, (%rcx,%r10,2)
	movzwl	20(%rsp), %ecx
	leal	1(%rax,%rcx), %eax
	shrl	%eax
	movw	%ax, (%rdi,%r11,2)
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movw	%ax, (%rsi,%rbp,2)
	movw	%ax, (%rdx,%r10,2)
	movzwl	22(%rsp), %eax
	leal	1(%rcx,%rax), %ecx
	shrl	%ecx
	movw	%cx, (%rdi,%rbp,2)
	movw	%cx, (%rsi,%r10,2)
	movzwl	24(%rsp), %ecx
	leal	1(%rax,%rcx), %eax
	shrl	%eax
	movw	%ax, (%rdi,%r10,2)
	movzwl	2(%rsp), %eax
	movzwl	6(%rsp), %esi
	addl	%esi, %eax
	movzwl	4(%rsp), %edx
	leal	2(%rax,%rdx,2), %eax
	shrl	$2, %eax
	movslq	176(%rsp), %rcx         # 4-byte Folded Reload
	shlq	$5, %rcx
	leaq	104(%r9,%rcx), %rcx
	movw	%ax, (%rcx,%r8,2)
	movzwl	8(%rsp), %ebx
	addl	%ebx, %edx
	leal	2(%rdx,%rsi,2), %eax
	shrl	$2, %eax
	movslq	184(%rsp), %rdx         # 4-byte Folded Reload
	shlq	$5, %rdx
	leaq	104(%r9,%rdx), %rdx
	movw	%ax, (%rdx,%r8,2)
	movw	%ax, (%rcx,%r14,2)
	movzwl	10(%rsp), %ebp
	addl	%ebp, %esi
	leal	2(%rsi,%rbx,2), %eax
	shrl	$2, %eax
	movslq	152(%rsp), %rsi         # 4-byte Folded Reload
	shlq	$5, %rsi
	leaq	104(%r9,%rsi), %rsi
	movw	%ax, (%rsi,%r8,2)
	movw	%ax, (%rdx,%r14,2)
	movw	%ax, (%rcx,%r12,2)
	movslq	136(%rsp), %rax         # 4-byte Folded Reload
	shlq	$5, %rax
	leaq	104(%r9,%rax), %r9
	movzwl	12(%rsp), %eax
	addl	%eax, %ebx
	leal	2(%rbx,%rbp,2), %ebx
	shrl	$2, %ebx
	movw	%bx, (%r9,%r8,2)
	movw	%bx, (%rsi,%r14,2)
	movw	%bx, (%rdx,%r12,2)
	movw	%bx, (%rcx,%r13,2)
	movzwl	14(%rsp), %ebx
	addl	%ebx, %ebp
	leal	2(%rbp,%rax,2), %ebp
	shrl	$2, %ebp
	movw	%bp, (%r9,%r14,2)
	movw	%bp, (%rsi,%r12,2)
	movw	%bp, (%rdx,%r13,2)
	movw	%bp, (%rcx,%r15,2)
	movzwl	16(%rsp), %ebp
	addl	%ebp, %eax
	leal	2(%rax,%rbx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%r9,%r12,2)
	movw	%ax, (%rsi,%r13,2)
	movw	%ax, (%rdx,%r15,2)
	movw	%ax, (%rcx,%r11,2)
	movzwl	18(%rsp), %eax
	addl	%eax, %ebx
	leal	2(%rbx,%rbp,2), %ebx
	shrl	$2, %ebx
	movw	%bx, (%r9,%r13,2)
	movw	%bx, (%rsi,%r15,2)
	movw	%bx, (%rdx,%r11,2)
	movq	56(%rsp), %rdi          # 8-byte Reload
	movw	%bx, (%rcx,%rdi,2)
	movzwl	20(%rsp), %ebx
	addl	%ebx, %ebp
	leal	2(%rbp,%rax,2), %ebp
	shrl	$2, %ebp
	movw	%bp, (%r9,%r15,2)
	movw	%bp, (%rsi,%r11,2)
	movw	%bp, (%rdx,%rdi,2)
	movw	%bp, (%rcx,%r10,2)
	movzwl	22(%rsp), %ecx
	addl	%ecx, %eax
	leal	2(%rax,%rbx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%r9,%r11,2)
	movw	%ax, (%rsi,%rdi,2)
	movw	%ax, (%rdx,%r10,2)
	movzwl	24(%rsp), %eax
	addl	%eax, %ebx
	leal	2(%rbx,%rcx,2), %edx
	shrl	$2, %edx
	movw	%dx, (%r9,%rdi,2)
	movw	%dx, (%rsi,%r10,2)
	movzwl	26(%rsp), %edx
	addl	%ecx, %edx
	leal	2(%rdx,%rax,2), %eax
	shrl	$2, %eax
	movw	%ax, (%r9,%r10,2)
	jmp	.LBB0_85
.LBB0_70:
	movl	%edi, %r13d
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movl	%r15d, 168(%rsp)        # 4-byte Spill
	movl	%ecx, 144(%rsp)         # 4-byte Spill
	movl	%edx, 96(%rsp)          # 4-byte Spill
	movl	%r9d, 120(%rsp)         # 4-byte Spill
	movl	%r14d, 192(%rsp)        # 4-byte Spill
	movl	%r12d, %r14d
	movl	%ebx, 112(%rsp)         # 4-byte Spill
	movl	%r10d, 136(%rsp)        # 4-byte Spill
	testl	%r11d, %r11d
	movl	%r8d, %ebx
	jne	.LBB0_72
# BB#71:
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %esi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movl	%ebx, %r15d
	movl	%r13d, %ebx
	callq	printf
	movl	%r15d, %ebx
.LBB0_72:
	movzwl	34(%rsp), %eax
	movzwl	36(%rsp), %ecx
	leal	1(%rax,%rcx), %eax
	shrl	%eax
	movslq	80(%rsp), %r8           # 4-byte Folded Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	shlq	$5, %rdx
	movq	64(%rsp), %rdi          # 8-byte Reload
	leaq	104(%rdi,%rdx), %rdx
	movw	%ax, (%rdx,%r8,2)
	movzwl	38(%rsp), %eax
	leal	1(%rcx,%rax), %ecx
	shrl	%ecx
	movslq	104(%rsp), %r12         # 4-byte Folded Reload
	movw	%cx, (%rdx,%r12,2)
	movq	%rdx, %rsi
	movslq	%ebx, %rdx
	shlq	$5, %rdx
	leaq	104(%rdi,%rdx), %r9
	movw	%cx, (%r9,%r8,2)
	movzwl	40(%rsp), %ecx
	leal	1(%rax,%rcx), %edx
	shrl	%edx
	movslq	72(%rsp), %rax          # 4-byte Folded Reload
	movw	%dx, (%rsi,%rax,2)
	movq	%rsi, %rbx
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	movw	%dx, (%r9,%r12,2)
	movslq	56(%rsp), %rsi          # 4-byte Folded Reload
	shlq	$5, %rsi
	leaq	104(%rdi,%rsi), %r10
	movw	%dx, (%r10,%r8,2)
	movzwl	42(%rsp), %edx
	leal	1(%rcx,%rdx), %esi
	shrl	%esi
	movslq	%r13d, %rcx
	movw	%si, (%rbx,%rcx,2)
	movw	%si, (%r9,%rax,2)
	movw	%si, (%r10,%r12,2)
	movslq	%ebp, %rbp
	shlq	$5, %rbp
	leaq	104(%rdi,%rbp), %r15
	movw	%si, (%r15,%r8,2)
	movzwl	44(%rsp), %esi
	leal	1(%rdx,%rsi), %edx
	shrl	%edx
	movw	%dx, (%r9,%rcx,2)
	movw	%dx, (%r10,%rax,2)
	movw	%dx, (%r15,%r12,2)
	movslq	168(%rsp), %rbp         # 4-byte Folded Reload
	shlq	$5, %rbp
	leaq	104(%rdi,%rbp), %rbx
	movw	%dx, (%rbx,%r8,2)
	movzwl	46(%rsp), %r11d
	leal	1(%rsi,%r11), %edx
	shrl	%edx
	movw	%dx, (%r10,%rcx,2)
	movw	%dx, (%r15,%rax,2)
	movw	%dx, (%rbx,%r12,2)
	movslq	%r14d, %rsi
	shlq	$5, %rsi
	leaq	104(%rdi,%rsi), %rsi
	movw	%dx, (%rsi,%r8,2)
	movzwl	48(%rsp), %r13d
	leal	1(%r11,%r13), %edx
	shrl	%edx
	movw	%dx, (%r15,%rcx,2)
	movw	%dx, (%rbx,%rax,2)
	movw	%dx, (%rsi,%r12,2)
	movslq	96(%rsp), %rbp          # 4-byte Folded Reload
	shlq	$5, %rbp
	leaq	104(%rdi,%rbp), %r14
	movw	%dx, (%r14,%r8,2)
	movslq	136(%rsp), %rdx         # 4-byte Folded Reload
	shlq	$5, %rdx
	leaq	104(%rdi,%rdx), %rdx
	movd	%r13d, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, (%rdx,%r8,2)
	movslq	112(%rsp), %rbp         # 4-byte Folded Reload
	movslq	192(%rsp), %rdx         # 4-byte Folded Reload
	movslq	120(%rsp), %rdi         # 4-byte Folded Reload
	movw	%r13w, (%r14,%rbp,2)
	movw	%r13w, (%r14,%rcx,2)
	movw	%r13w, (%r14,%rdx,2)
	movw	%r13w, (%r14,%rax,2)
	movw	%r13w, (%r14,%rdi,2)
	movw	%r13w, (%r14,%r12,2)
	movw	%r13w, (%rsi,%rbp,2)
	movw	%r13w, (%rsi,%rcx,2)
	movw	%r13w, (%rsi,%rdx,2)
	movw	%r13w, (%rsi,%rax,2)
	movslq	144(%rsp), %r12         # 4-byte Folded Reload
	movw	%r13w, (%rbx,%rbp,2)
	movw	%r13w, (%rbx,%rcx,2)
	leal	(%r13,%r13,2), %ecx
	leal	2(%r11,%rcx), %ecx
	shrl	$2, %ecx
	movw	%cx, (%r15,%rbp,2)
	movw	%cx, (%rbx,%rdx,2)
	movw	%cx, (%rsi,%rdi,2)
	movw	%cx, (%r14,%r12,2)
	movzwl	44(%rsp), %ecx
	addl	%ecx, %r13d
	leal	2(%r13,%r11,2), %eax
	shrl	$2, %eax
	movw	%ax, (%r10,%rbp,2)
	movw	%ax, (%r15,%rdx,2)
	movw	%ax, (%rbx,%rdi,2)
	movw	%ax, (%rsi,%r12,2)
	movzwl	46(%rsp), %eax
	movzwl	42(%rsp), %esi
	addl	%esi, %eax
	leal	2(%rax,%rcx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%r9,%rbp,2)
	movw	%ax, (%r10,%rdx,2)
	movw	%ax, (%r15,%rdi,2)
	movw	%ax, (%rbx,%r12,2)
	movzwl	40(%rsp), %eax
	addl	%eax, %ecx
	leal	2(%rcx,%rsi,2), %ecx
	shrl	$2, %ecx
	movq	88(%rsp), %rbx          # 8-byte Reload
	movw	%cx, (%rbx,%rbp,2)
	movw	%cx, (%r9,%rdx,2)
	movw	%cx, (%r10,%rdi,2)
	movw	%cx, (%r15,%r12,2)
	movzwl	38(%rsp), %ecx
	addl	%ecx, %esi
	leal	2(%rsi,%rax,2), %esi
	shrl	$2, %esi
	movq	%rbx, %rbp
	movw	%si, (%rbp,%rdx,2)
	movw	%si, (%r9,%rdi,2)
	movw	%si, (%r10,%r12,2)
	movzwl	36(%rsp), %edx
	addl	%edx, %eax
	leal	2(%rax,%rcx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%rbp,%rdi,2)
	movw	%ax, (%r9,%r12,2)
	movzwl	34(%rsp), %eax
	addl	%ecx, %eax
	leal	2(%rax,%rdx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%rbp,%r12,2)
	jmp	.LBB0_85
.LBB0_73:
	testl	%eax, %eax
	movq	64(%rsp), %r9           # 8-byte Reload
	je	.LBB0_76
# BB#74:
	testl	%r11d, %r11d
	jne	.LBB0_76
# BB#75:
	movl	%r10d, %edi
	movzwl	34(%rsp), %eax
	movzwl	36(%rsp), %ecx
	movzwl	38(%rsp), %edx
	movzwl	40(%rsp), %esi
	movzwl	42(%rsp), %ebx
	movzwl	44(%rsp), %ebp
	movzwl	46(%rsp), %r10d
	movzwl	48(%rsp), %r8d
	jmp	.LBB0_79
.LBB0_76:
	movl	%r10d, %edi
	testl	%eax, %eax
	jne	.LBB0_80
# BB#77:
	testl	%r11d, %r11d
	je	.LBB0_80
# BB#78:
	movzwl	2(%rsp), %eax
	movzwl	4(%rsp), %ecx
	movzwl	6(%rsp), %edx
	movzwl	8(%rsp), %esi
	movzwl	10(%rsp), %ebx
	movzwl	12(%rsp), %ebp
	movzwl	14(%rsp), %r10d
	movzwl	16(%rsp), %r8d
.LBB0_79:
	addl	%eax, %ecx
	addl	%edx, %ecx
	addl	%esi, %ecx
	addl	%ebx, %ecx
	addl	%ebp, %ecx
	addl	%r10d, %ecx
	leal	4(%r8,%rcx), %eax
	sarl	$3, %eax
	jmp	.LBB0_81
.LBB0_80:
	movl	5892(%r9), %eax
.LBB0_81:
	movq	88(%rsp), %rbp          # 8-byte Reload
	movl	80(%rsp), %ecx          # 4-byte Reload
	movl	168(%rsp), %edx         # 4-byte Reload
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	movslq	%edi, %rsi
	leaq	-1(%rbp), %rdi
	shlq	$5, %rbp
	leaq	(%rbp,%rcx,2), %rbp
	orq	$1, %rcx
	leaq	118(%r9,%rbp), %rbp
	.p2align	4, 0x90
.LBB0_82:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rdx, %rcx
	movw	%ax, -14(%rbp)
	jge	.LBB0_84
# BB#83:                                #   in Loop: Header=BB0_82 Depth=1
	movw	%ax, -12(%rbp)
	movw	%ax, -10(%rbp)
	movw	%ax, -8(%rbp)
	movw	%ax, -6(%rbp)
	movw	%ax, -4(%rbp)
	movw	%ax, -2(%rbp)
	movw	%ax, (%rbp)
.LBB0_84:                               #   in Loop: Header=BB0_82 Depth=1
	incq	%rdi
	addq	$32, %rbp
	cmpq	%rsi, %rdi
	jl	.LBB0_82
.LBB0_85:
	xorl	%eax, %eax
.LBB0_86:                               # %.loopexit
	addq	$472, %rsp              # imm = 0x1D8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	intrapred8x8, .Lfunc_end0-intrapred8x8
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_39
	.quad	.LBB0_48
	.quad	.LBB0_51
	.quad	.LBB0_54
	.quad	.LBB0_42
	.quad	.LBB0_57
	.quad	.LBB0_62
	.quad	.LBB0_67
	.quad	.LBB0_70

	.text
	.globl	LowPassForIntra8x8Pred
	.p2align	4, 0x90
	.type	LowPassForIntra8x8Pred,@function
LowPassForIntra8x8Pred:                 # @LowPassForIntra8x8Pred
	.cfi_startproc
# BB#0:
	movzwl	48(%rdi), %eax
	movw	%ax, -8(%rsp)
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	32(%rdi), %xmm2
	movaps	%xmm2, -24(%rsp)
	movaps	%xmm1, -40(%rsp)
	movaps	%xmm0, -56(%rsp)
	testl	%edx, %edx
	je	.LBB1_7
# BB#1:
	testl	%esi, %esi
	movw	2(%rdi), %r8w
	movzwl	%r8w, %r9d
	je	.LBB1_3
# BB#2:
	movzwl	(%rdi), %eax
	leal	(%rax,%r9,2), %eax
	jmp	.LBB1_4
.LBB1_3:
	leal	(%r9,%r9,2), %eax
.LBB1_4:                                # %.preheader101
	movzwl	4(%rdi), %r9d
	leal	2(%rax,%r9), %eax
	shrl	$2, %eax
	movw	%ax, -54(%rsp)
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB1_5:                                # =>This Inner Loop Header: Depth=1
	movzwl	%r8w, %r8d
	movzwl	%r9w, %r9d
	leal	(%r8,%r9,2), %r10d
	movzwl	6(%rdi,%r11,2), %r8d
	leal	2(%r8,%r10), %eax
	shrl	$2, %eax
	movw	%ax, -52(%rsp,%r11,2)
	leal	(%r9,%r8,2), %eax
	movzwl	8(%rdi,%r11,2), %r9d
	leal	2(%r9,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -50(%rsp,%r11,2)
	addq	$2, %r11
	cmpq	$14, %r11
	jne	.LBB1_5
# BB#6:
	movzwl	32(%rdi), %eax
	leal	(%rax,%rax,2), %r8d
	movzwl	30(%rdi), %eax
	leal	2(%rax,%r8), %eax
	shrl	$2, %eax
	movw	%ax, -24(%rsp)
.LBB1_7:
	testl	%esi, %esi
	je	.LBB1_13
# BB#8:
	testl	%edx, %edx
	je	.LBB1_11
# BB#9:
	testl	%ecx, %ecx
	je	.LBB1_11
# BB#10:
	movzwl	34(%rdi), %ecx
	movzwl	(%rdi), %eax
	leal	(%rcx,%rax,2), %ecx
	movl	$1, %edx
	jmp	.LBB1_18
.LBB1_11:
	testl	%edx, %edx
	je	.LBB1_16
# BB#12:
	movzwl	(%rdi), %eax
	leal	(%rax,%rax,2), %eax
	movzwl	2(%rdi), %edx
	leal	2(%rdx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -56(%rsp)
.LBB1_13:
	testl	%ecx, %ecx
	je	.LBB1_22
# BB#14:
	testl	%esi, %esi
	je	.LBB1_20
# BB#15:                                # %..thread94_crit_edge
	movw	(%rdi), %ax
	jmp	.LBB1_19
.LBB1_20:
	movzwl	34(%rdi), %eax
	leal	(%rax,%rax,2), %ecx
	jmp	.LBB1_21
.LBB1_16:
	testl	%ecx, %ecx
	je	.LBB1_22
# BB#17:
	movzwl	(%rdi), %eax
	leal	(%rax,%rax,2), %ecx
	movl	$17, %edx
.LBB1_18:                               # %.thread94.sink.split
	movzwl	(%rdi,%rdx,2), %edx
	leal	2(%rcx,%rdx), %ecx
	shrl	$2, %ecx
	movw	%cx, -56(%rsp)
.LBB1_19:                               # %.thread94
	movzwl	%ax, %ecx
	movzwl	34(%rdi), %eax
	leal	(%rcx,%rax,2), %ecx
.LBB1_21:                               # %.preheader
	movzwl	36(%rdi), %edx
	leal	2(%rcx,%rdx), %ecx
	shrl	$2, %ecx
	movw	%cx, -22(%rsp)
	movzwl	%ax, %eax
	leal	(%rax,%rdx,2), %eax
	movzwl	38(%rdi), %ecx
	leal	2(%rcx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -20(%rsp)
	leal	(%rdx,%rcx,2), %eax
	movzwl	40(%rdi), %edx
	leal	2(%rdx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -18(%rsp)
	leal	(%rcx,%rdx,2), %eax
	movzwl	42(%rdi), %ecx
	leal	2(%rcx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -16(%rsp)
	leal	(%rdx,%rcx,2), %eax
	movzwl	44(%rdi), %edx
	leal	2(%rdx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -14(%rsp)
	leal	(%rcx,%rdx,2), %eax
	movzwl	46(%rdi), %ecx
	leal	2(%rcx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -12(%rsp)
	leal	(%rdx,%rcx,2), %eax
	movzwl	48(%rdi), %edx
	leal	2(%rdx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, -10(%rsp)
	addl	%edx, %ecx
	leal	2(%rcx,%rdx,2), %eax
	shrl	$2, %eax
	movw	%ax, -8(%rsp)
.LBB1_22:                               # %.thread93
	movzwl	-8(%rsp), %eax
	movw	%ax, 48(%rdi)
	movaps	-56(%rsp), %xmm0
	movaps	-40(%rsp), %xmm1
	movaps	-24(%rsp), %xmm2
	movups	%xmm2, 32(%rdi)
	movups	%xmm1, 16(%rdi)
	movups	%xmm0, (%rdi)
	retq
.Lfunc_end1:
	.size	LowPassForIntra8x8Pred, .Lfunc_end1-LowPassForIntra8x8Pred
	.cfi_endproc

	.globl	itrans8x8
	.p2align	4, 0x90
	.type	itrans8x8,@function
itrans8x8:                              # @itrans8x8
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 288
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	28(%rdi), %eax
	addl	5884(%rdi), %eax
	jne	.LBB2_2
# BB#1:
	cmpl	$1, 5920(%rdi)
	jne	.LBB2_2
# BB#8:                                 # %.preheader142
	movslq	%esi, %rsi
	movslq	%edx, %rbp
	addl	$7, %edx
	movslq	%edx, %rax
	leaq	-1(%rbp), %rcx
	shlq	$5, %rbp
	leaq	(%rbp,%rsi,2), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_9:                                # =>This Inner Loop Header: Depth=1
	movl	5900(%rdi), %ebx
	movzwl	104(%rdi,%rdx), %ebp
	addl	1384(%rdi,%rdx,2), %ebp
	cmovsl	%esi, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movl	%ebp, 1384(%rdi,%rdx,2)
	movl	5900(%rdi), %ebx
	movzwl	106(%rdi,%rdx), %ebp
	addl	1388(%rdi,%rdx,2), %ebp
	cmovsl	%esi, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movl	%ebp, 1388(%rdi,%rdx,2)
	movl	5900(%rdi), %ebx
	movzwl	108(%rdi,%rdx), %ebp
	addl	1392(%rdi,%rdx,2), %ebp
	cmovsl	%esi, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movl	%ebp, 1392(%rdi,%rdx,2)
	movl	5900(%rdi), %ebx
	movzwl	110(%rdi,%rdx), %ebp
	addl	1396(%rdi,%rdx,2), %ebp
	cmovsl	%esi, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movl	%ebp, 1396(%rdi,%rdx,2)
	movl	5900(%rdi), %ebx
	movzwl	112(%rdi,%rdx), %ebp
	addl	1400(%rdi,%rdx,2), %ebp
	cmovsl	%esi, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movl	%ebp, 1400(%rdi,%rdx,2)
	movl	5900(%rdi), %ebx
	movzwl	114(%rdi,%rdx), %ebp
	addl	1404(%rdi,%rdx,2), %ebp
	cmovsl	%esi, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movl	%ebp, 1404(%rdi,%rdx,2)
	movl	5900(%rdi), %ebx
	movzwl	116(%rdi,%rdx), %ebp
	addl	1408(%rdi,%rdx,2), %ebp
	cmovsl	%esi, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movl	%ebp, 1408(%rdi,%rdx,2)
	movl	5900(%rdi), %ebx
	movzwl	118(%rdi,%rdx), %ebp
	addl	1412(%rdi,%rdx,2), %ebp
	cmovsl	%esi, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movl	%ebp, 1412(%rdi,%rdx,2)
	incq	%rcx
	addq	$32, %rdx
	cmpq	%rax, %rcx
	jl	.LBB2_9
	jmp	.LBB2_10
.LBB2_2:                                # %.critedge.preheader
	movslq	%esi, %rax
	movslq	%edx, %rcx
	movq	%rcx, -80(%rsp)         # 8-byte Spill
	shlq	$6, %rcx
	movq	%rax, -72(%rsp)         # 8-byte Spill
	movq	%rcx, %rdx
	movq	%rdx, -104(%rsp)        # 8-byte Spill
	leaq	(%rcx,%rax,4), %rax
	movq	%rdi, -64(%rsp)         # 8-byte Spill
	leaq	1412(%rdi,%rax), %r12
	movq	$-8, %r13
	.p2align	4, 0x90
.LBB2_3:                                # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%r12), %eax
	movl	-28(%r12), %ecx
	movl	-24(%r12), %r9d
	leal	(%rax,%rcx), %r8d
	subl	%eax, %ecx
	movl	-20(%r12), %eax
	movl	%eax, %r14d
	sarl	%r14d
	movl	-4(%r12), %esi
	subl	%esi, %r14d
	sarl	%esi
	addl	%eax, %esi
	leal	(%r8,%rsi), %eax
	movq	%rax, -128(%rsp)        # 8-byte Spill
	leal	(%r14,%rcx), %r11d
	movq	%r11, -112(%rsp)        # 8-byte Spill
	movq	%rcx, -88(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	subl	%r14d, %ecx
	movq	%rcx, -120(%rsp)        # 8-byte Spill
	movl	-16(%r12), %ebx
	movl	-8(%r12), %eax
	movl	%eax, %ebp
	subl	%ebx, %ebp
	movl	(%r12), %edi
	subl	%edi, %ebp
	movl	%edi, %ecx
	sarl	%ecx
	subl	%ecx, %ebp
	leal	(%rdi,%r9), %ecx
	subl	%ebx, %ecx
	movl	%ebx, %edx
	sarl	%edx
	subl	%edx, %ecx
	subl	%r9d, %edi
	addl	%eax, %edi
	addl	%eax, %ebx
	sarl	%eax
	addl	%edi, %eax
	addl	%r9d, %ebx
	sarl	%r9d
	addl	%ebx, %r9d
	movq	%r8, %rdi
	movl	%ebp, %edx
	sarl	$2, %edx
	movl	%r9d, %r15d
	subl	%edx, %r15d
	movl	%eax, %r10d
	sarl	$2, %r10d
	addl	%ecx, %r10d
	movl	%ecx, %r8d
	sarl	$2, %r8d
	subl	%eax, %r8d
	movq	-128(%rsp), %rdx        # 8-byte Reload
	leal	(%r15,%rdx), %edx
	movl	%edx, (%rsp,%r13,4)
	leal	(%r8,%r11), %edx
	movl	%edx, 32(%rsp,%r13,4)
	movq	-120(%rsp), %rbx        # 8-byte Reload
	leal	(%r10,%rbx), %edx
	movl	%edx, 64(%rsp,%r13,4)
	movq	%rdi, %rdx
	movq	%rdx, -96(%rsp)         # 8-byte Spill
	movl	%edi, %r11d
	subl	%esi, %r11d
	movl	%r9d, %edx
	sarl	$2, %edx
	addl	%ebp, %edx
	leal	(%rdx,%r11), %edi
	movl	%edi, 96(%rsp,%r13,4)
	movl	%r11d, %edi
	subl	%edx, %edi
	movl	%edi, 128(%rsp,%r13,4)
	movl	%ebx, %edi
	subl	%r10d, %edi
	movl	%edi, 160(%rsp,%r13,4)
	movq	-112(%rsp), %rdi        # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	subl	%r8d, %edi
	movl	%edi, 192(%rsp,%r13,4)
	movq	-128(%rsp), %rdi        # 8-byte Reload
	movq	%rdi, %rbx
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	subl	%r15d, %edi
	movl	%edi, 224(%rsp,%r13,4)
	addq	$64, %r12
	incq	%r13
	jne	.LBB2_3
# BB#4:                                 # %.preheader146
	movq	-96(%rsp), %rdi         # 8-byte Reload
	movl	%edi, itrans8x8.a.0(%rip)
	movq	-88(%rsp), %rdi         # 8-byte Reload
	movl	%edi, itrans8x8.a.4(%rip)
	movl	%r14d, itrans8x8.a.2(%rip)
	movl	%esi, itrans8x8.a.6(%rip)
	movl	%ebx, itrans8x8.b.0(%rip)
	movq	-112(%rsp), %rsi        # 8-byte Reload
	movl	%esi, itrans8x8.b.2(%rip)
	movq	-120(%rsp), %rsi        # 8-byte Reload
	movl	%esi, itrans8x8.b.4(%rip)
	movl	%r11d, itrans8x8.b.6(%rip)
	movl	%ebp, itrans8x8.a.1(%rip)
	movl	%ecx, itrans8x8.a.3(%rip)
	movl	%eax, itrans8x8.a.5(%rip)
	movl	%r9d, itrans8x8.a.7(%rip)
	movl	%edx, itrans8x8.b.1(%rip)
	movl	%r15d, itrans8x8.b.7(%rip)
	movl	%r10d, itrans8x8.b.3(%rip)
	movl	%r8d, itrans8x8.b.5(%rip)
	movq	-80(%rsp), %rax         # 8-byte Reload
	leaq	7(%rax), %rcx
	movq	-72(%rsp), %rax         # 8-byte Reload
	leaq	(,%rax,4), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	movq	%rcx, -56(%rsp)         # 8-byte Spill
	movq	%rcx, %rax
	shlq	$6, %rax
	movq	-64(%rsp), %rcx         # 8-byte Reload
	leaq	1384(%rcx,%rax), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	movq	-104(%rsp), %rax        # 8-byte Reload
	leaq	1768(%rcx,%rax), %rax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_5:                                # =>This Inner Loop Header: Depth=1
	movl	-32(%rsp,%rax,8), %edx
	movl	-16(%rsp,%rax,8), %ecx
	leal	(%rcx,%rdx), %r10d
	subl	%ecx, %edx
	movl	-24(%rsp,%rax,8), %esi
	movl	%esi, %ecx
	sarl	%ecx
	movl	-8(%rsp,%rax,8), %ebx
	subl	%ebx, %ecx
	sarl	%ebx
	addl	%esi, %ebx
	leal	(%r10,%rbx), %ebp
	movq	%rbp, -112(%rsp)        # 8-byte Spill
	leal	(%rcx,%rdx), %r12d
	movq	%r12, -120(%rsp)        # 8-byte Spill
	movq	%rdx, -40(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%ecx, %edx
	movq	%rdx, -128(%rsp)        # 8-byte Spill
	movl	-20(%rsp,%rax,8), %esi
	movl	-12(%rsp,%rax,8), %r9d
	movl	%r9d, %r8d
	subl	%esi, %r8d
	movl	-4(%rsp,%rax,8), %edi
	subl	%edi, %r8d
	movl	%edi, %edx
	sarl	%edx
	subl	%edx, %r8d
	movl	-28(%rsp,%rax,8), %r13d
	leal	(%rdi,%r13), %r11d
	subl	%esi, %r11d
	movl	%esi, %edx
	sarl	%edx
	subl	%edx, %r11d
	subl	%r13d, %edi
	addl	%r9d, %edi
	addl	%r9d, %esi
	sarl	%r9d
	addl	%edi, %r9d
	addl	%r13d, %esi
	sarl	%r13d
	addl	%esi, %r13d
	movl	%r8d, %edx
	sarl	$2, %edx
	movl	%r13d, %r14d
	subl	%edx, %r14d
	movl	%r11d, %r15d
	sarl	$2, %r15d
	subl	%r9d, %r15d
	leal	(%r14,%rbp), %edx
	movq	-88(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rax), %rdi
	movq	-104(%rsp), %rbp        # 8-byte Reload
	movl	%edx, -384(%rbp,%rdi)
	leal	(%r15,%r12), %edx
	movl	%edx, -320(%rbp,%rdi)
	movl	%r9d, %r12d
	sarl	$2, %r12d
	addl	%r11d, %r12d
	movq	-128(%rsp), %rdx        # 8-byte Reload
	leal	(%r12,%rdx), %edx
	movl	%edx, -256(%rbp,%rdi)
	movq	%r10, -48(%rsp)         # 8-byte Spill
	movl	%r10d, %esi
	subl	%ebx, %esi
	movl	%r13d, %r10d
	sarl	$2, %r10d
	addl	%r8d, %r10d
	leal	(%r10,%rsi), %edx
	movl	%edx, -192(%rbp,%rdi)
	movl	%esi, %edx
	subl	%r10d, %edx
	movl	%edx, -128(%rbp,%rdi)
	movq	-128(%rsp), %rdx        # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	subl	%r12d, %edx
	movl	%edx, -64(%rbp,%rdi)
	movq	-120(%rsp), %rdx        # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	subl	%r15d, %edx
	movl	%edx, (%rbp,%rdi)
	movq	-112(%rsp), %rdx        # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	subl	%r14d, %edx
	movq	-96(%rsp), %rbp         # 8-byte Reload
	movl	%edx, (%rbp,%rdi)
	addq	$4, %rax
	cmpq	$32, %rax
	jne	.LBB2_5
# BB#6:                                 # %.preheader144
	movq	-72(%rsp), %rdx         # 8-byte Reload
	leal	7(%rdx), %eax
	movl	%eax, itrans8x8.ipos(%rip)
	movq	-48(%rsp), %rax         # 8-byte Reload
	movl	%eax, itrans8x8.a.0(%rip)
	movq	-40(%rsp), %rax         # 8-byte Reload
	movl	%eax, itrans8x8.a.4(%rip)
	movl	%ecx, itrans8x8.a.2(%rip)
	movl	%ebx, itrans8x8.a.6(%rip)
	movq	-112(%rsp), %rax        # 8-byte Reload
	movl	%eax, itrans8x8.b.0(%rip)
	movq	-120(%rsp), %rax        # 8-byte Reload
	movl	%eax, itrans8x8.b.2(%rip)
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, itrans8x8.b.4(%rip)
	movl	%esi, itrans8x8.b.6(%rip)
	movl	%r8d, itrans8x8.a.1(%rip)
	movl	%r11d, itrans8x8.a.3(%rip)
	movl	%r9d, itrans8x8.a.5(%rip)
	movl	%r13d, itrans8x8.a.7(%rip)
	movl	%r10d, itrans8x8.b.1(%rip)
	movl	%r14d, itrans8x8.b.7(%rip)
	movl	%r12d, itrans8x8.b.3(%rip)
	movl	%r15d, itrans8x8.b.5(%rip)
	movq	-80(%rsp), %rax         # 8-byte Reload
	leaq	-1(%rax), %rsi
	shlq	$5, %rax
	leaq	(%rax,%rdx,2), %rax
	xorl	%ecx, %ecx
	movq	-64(%rsp), %rbx         # 8-byte Reload
	movq	-56(%rsp), %r8          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	movl	5900(%rbx), %edx
	movl	1384(%rbx,%rax,2), %edi
	movzwl	104(%rbx,%rax), %ebp
	shll	$6, %ebp
	leal	32(%rdi,%rbp), %edi
	sarl	$6, %edi
	cmovsl	%ecx, %edi
	cmpl	%edx, %edi
	cmovgl	%edx, %edi
	movl	%edi, 1384(%rbx,%rax,2)
	movl	5900(%rbx), %edx
	movl	1388(%rbx,%rax,2), %edi
	movzwl	106(%rbx,%rax), %ebp
	shll	$6, %ebp
	leal	32(%rdi,%rbp), %edi
	sarl	$6, %edi
	cmovsl	%ecx, %edi
	cmpl	%edx, %edi
	cmovgl	%edx, %edi
	movl	%edi, 1388(%rbx,%rax,2)
	movl	5900(%rbx), %edx
	movl	1392(%rbx,%rax,2), %edi
	movzwl	108(%rbx,%rax), %ebp
	shll	$6, %ebp
	leal	32(%rdi,%rbp), %edi
	sarl	$6, %edi
	cmovsl	%ecx, %edi
	cmpl	%edx, %edi
	cmovgl	%edx, %edi
	movl	%edi, 1392(%rbx,%rax,2)
	movl	5900(%rbx), %edx
	movl	1396(%rbx,%rax,2), %edi
	movzwl	110(%rbx,%rax), %ebp
	shll	$6, %ebp
	leal	32(%rdi,%rbp), %edi
	sarl	$6, %edi
	cmovsl	%ecx, %edi
	cmpl	%edx, %edi
	cmovgl	%edx, %edi
	movl	%edi, 1396(%rbx,%rax,2)
	movl	5900(%rbx), %edx
	movl	1400(%rbx,%rax,2), %edi
	movzwl	112(%rbx,%rax), %ebp
	shll	$6, %ebp
	leal	32(%rdi,%rbp), %edi
	sarl	$6, %edi
	cmovsl	%ecx, %edi
	cmpl	%edx, %edi
	cmovgl	%edx, %edi
	movl	%edi, 1400(%rbx,%rax,2)
	movl	5900(%rbx), %edx
	movl	1404(%rbx,%rax,2), %edi
	movzwl	114(%rbx,%rax), %ebp
	shll	$6, %ebp
	leal	32(%rdi,%rbp), %edi
	sarl	$6, %edi
	cmovsl	%ecx, %edi
	cmpl	%edx, %edi
	cmovgl	%edx, %edi
	movl	%edi, 1404(%rbx,%rax,2)
	movl	5900(%rbx), %edx
	movl	1408(%rbx,%rax,2), %edi
	movzwl	116(%rbx,%rax), %ebp
	shll	$6, %ebp
	leal	32(%rdi,%rbp), %edi
	sarl	$6, %edi
	cmovsl	%ecx, %edi
	cmpl	%edx, %edi
	cmovgl	%edx, %edi
	movl	%edi, 1408(%rbx,%rax,2)
	movl	5900(%rbx), %edx
	movl	1412(%rbx,%rax,2), %edi
	movzwl	118(%rbx,%rax), %ebp
	shll	$6, %ebp
	leal	32(%rdi,%rbp), %edi
	sarl	$6, %edi
	cmovsl	%ecx, %edi
	cmpl	%edx, %edi
	cmovgl	%edx, %edi
	movl	%edi, 1412(%rbx,%rax,2)
	incq	%rsi
	addq	$32, %rax
	cmpq	%r8, %rsi
	jl	.LBB2_7
.LBB2_10:                               # %.loopexit
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	itrans8x8, .Lfunc_end2-itrans8x8
	.cfi_endproc

	.type	quant_coef8,@object     # @quant_coef8
	.section	.rodata,"a",@progbits
	.globl	quant_coef8
	.p2align	4
quant_coef8:
	.long	13107                   # 0x3333
	.long	12222                   # 0x2fbe
	.long	16777                   # 0x4189
	.long	12222                   # 0x2fbe
	.long	13107                   # 0x3333
	.long	12222                   # 0x2fbe
	.long	16777                   # 0x4189
	.long	12222                   # 0x2fbe
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	16777                   # 0x4189
	.long	15481                   # 0x3c79
	.long	20972                   # 0x51ec
	.long	15481                   # 0x3c79
	.long	16777                   # 0x4189
	.long	15481                   # 0x3c79
	.long	20972                   # 0x51ec
	.long	15481                   # 0x3c79
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	13107                   # 0x3333
	.long	12222                   # 0x2fbe
	.long	16777                   # 0x4189
	.long	12222                   # 0x2fbe
	.long	13107                   # 0x3333
	.long	12222                   # 0x2fbe
	.long	16777                   # 0x4189
	.long	12222                   # 0x2fbe
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	16777                   # 0x4189
	.long	15481                   # 0x3c79
	.long	20972                   # 0x51ec
	.long	15481                   # 0x3c79
	.long	16777                   # 0x4189
	.long	15481                   # 0x3c79
	.long	20972                   # 0x51ec
	.long	15481                   # 0x3c79
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	12222                   # 0x2fbe
	.long	11428                   # 0x2ca4
	.long	15481                   # 0x3c79
	.long	11428                   # 0x2ca4
	.long	11916                   # 0x2e8c
	.long	11058                   # 0x2b32
	.long	14980                   # 0x3a84
	.long	11058                   # 0x2b32
	.long	11916                   # 0x2e8c
	.long	11058                   # 0x2b32
	.long	14980                   # 0x3a84
	.long	11058                   # 0x2b32
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	14980                   # 0x3a84
	.long	14290                   # 0x37d2
	.long	19174                   # 0x4ae6
	.long	14290                   # 0x37d2
	.long	14980                   # 0x3a84
	.long	14290                   # 0x37d2
	.long	19174                   # 0x4ae6
	.long	14290                   # 0x37d2
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	11916                   # 0x2e8c
	.long	11058                   # 0x2b32
	.long	14980                   # 0x3a84
	.long	11058                   # 0x2b32
	.long	11916                   # 0x2e8c
	.long	11058                   # 0x2b32
	.long	14980                   # 0x3a84
	.long	11058                   # 0x2b32
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	14980                   # 0x3a84
	.long	14290                   # 0x37d2
	.long	19174                   # 0x4ae6
	.long	14290                   # 0x37d2
	.long	14980                   # 0x3a84
	.long	14290                   # 0x37d2
	.long	19174                   # 0x4ae6
	.long	14290                   # 0x37d2
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	11058                   # 0x2b32
	.long	10826                   # 0x2a4a
	.long	14290                   # 0x37d2
	.long	10826                   # 0x2a4a
	.long	10082                   # 0x2762
	.long	9675                    # 0x25cb
	.long	12710                   # 0x31a6
	.long	9675                    # 0x25cb
	.long	10082                   # 0x2762
	.long	9675                    # 0x25cb
	.long	12710                   # 0x31a6
	.long	9675                    # 0x25cb
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	12710                   # 0x31a6
	.long	11985                   # 0x2ed1
	.long	15978                   # 0x3e6a
	.long	11985                   # 0x2ed1
	.long	12710                   # 0x31a6
	.long	11985                   # 0x2ed1
	.long	15978                   # 0x3e6a
	.long	11985                   # 0x2ed1
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	10082                   # 0x2762
	.long	9675                    # 0x25cb
	.long	12710                   # 0x31a6
	.long	9675                    # 0x25cb
	.long	10082                   # 0x2762
	.long	9675                    # 0x25cb
	.long	12710                   # 0x31a6
	.long	9675                    # 0x25cb
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	12710                   # 0x31a6
	.long	11985                   # 0x2ed1
	.long	15978                   # 0x3e6a
	.long	11985                   # 0x2ed1
	.long	12710                   # 0x31a6
	.long	11985                   # 0x2ed1
	.long	15978                   # 0x3e6a
	.long	11985                   # 0x2ed1
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	9675                    # 0x25cb
	.long	8943                    # 0x22ef
	.long	11985                   # 0x2ed1
	.long	8943                    # 0x22ef
	.long	9362                    # 0x2492
	.long	8931                    # 0x22e3
	.long	11984                   # 0x2ed0
	.long	8931                    # 0x22e3
	.long	9362                    # 0x2492
	.long	8931                    # 0x22e3
	.long	11984                   # 0x2ed0
	.long	8931                    # 0x22e3
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	11984                   # 0x2ed0
	.long	11259                   # 0x2bfb
	.long	14913                   # 0x3a41
	.long	11259                   # 0x2bfb
	.long	11984                   # 0x2ed0
	.long	11259                   # 0x2bfb
	.long	14913                   # 0x3a41
	.long	11259                   # 0x2bfb
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	9362                    # 0x2492
	.long	8931                    # 0x22e3
	.long	11984                   # 0x2ed0
	.long	8931                    # 0x22e3
	.long	9362                    # 0x2492
	.long	8931                    # 0x22e3
	.long	11984                   # 0x2ed0
	.long	8931                    # 0x22e3
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	11984                   # 0x2ed0
	.long	11259                   # 0x2bfb
	.long	14913                   # 0x3a41
	.long	11259                   # 0x2bfb
	.long	11984                   # 0x2ed0
	.long	11259                   # 0x2bfb
	.long	14913                   # 0x3a41
	.long	11259                   # 0x2bfb
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	8931                    # 0x22e3
	.long	8228                    # 0x2024
	.long	11259                   # 0x2bfb
	.long	8228                    # 0x2024
	.long	8192                    # 0x2000
	.long	7740                    # 0x1e3c
	.long	10486                   # 0x28f6
	.long	7740                    # 0x1e3c
	.long	8192                    # 0x2000
	.long	7740                    # 0x1e3c
	.long	10486                   # 0x28f6
	.long	7740                    # 0x1e3c
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	10486                   # 0x28f6
	.long	9777                    # 0x2631
	.long	13159                   # 0x3367
	.long	9777                    # 0x2631
	.long	10486                   # 0x28f6
	.long	9777                    # 0x2631
	.long	13159                   # 0x3367
	.long	9777                    # 0x2631
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	8192                    # 0x2000
	.long	7740                    # 0x1e3c
	.long	10486                   # 0x28f6
	.long	7740                    # 0x1e3c
	.long	8192                    # 0x2000
	.long	7740                    # 0x1e3c
	.long	10486                   # 0x28f6
	.long	7740                    # 0x1e3c
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	10486                   # 0x28f6
	.long	9777                    # 0x2631
	.long	13159                   # 0x3367
	.long	9777                    # 0x2631
	.long	10486                   # 0x28f6
	.long	9777                    # 0x2631
	.long	13159                   # 0x3367
	.long	9777                    # 0x2631
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	7740                    # 0x1e3c
	.long	7346                    # 0x1cb2
	.long	9777                    # 0x2631
	.long	7346                    # 0x1cb2
	.long	7282                    # 0x1c72
	.long	6830                    # 0x1aae
	.long	9118                    # 0x239e
	.long	6830                    # 0x1aae
	.long	7282                    # 0x1c72
	.long	6830                    # 0x1aae
	.long	9118                    # 0x239e
	.long	6830                    # 0x1aae
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.long	9118                    # 0x239e
	.long	8640                    # 0x21c0
	.long	11570                   # 0x2d32
	.long	8640                    # 0x21c0
	.long	9118                    # 0x239e
	.long	8640                    # 0x21c0
	.long	11570                   # 0x2d32
	.long	8640                    # 0x21c0
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.long	7282                    # 0x1c72
	.long	6830                    # 0x1aae
	.long	9118                    # 0x239e
	.long	6830                    # 0x1aae
	.long	7282                    # 0x1c72
	.long	6830                    # 0x1aae
	.long	9118                    # 0x239e
	.long	6830                    # 0x1aae
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.long	9118                    # 0x239e
	.long	8640                    # 0x21c0
	.long	11570                   # 0x2d32
	.long	8640                    # 0x21c0
	.long	9118                    # 0x239e
	.long	8640                    # 0x21c0
	.long	11570                   # 0x2d32
	.long	8640                    # 0x21c0
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.long	6830                    # 0x1aae
	.long	6428                    # 0x191c
	.long	8640                    # 0x21c0
	.long	6428                    # 0x191c
	.size	quant_coef8, 1536

	.type	dequant_coef8,@object   # @dequant_coef8
	.globl	dequant_coef8
	.p2align	4
dequant_coef8:
	.long	20                      # 0x14
	.long	19                      # 0x13
	.long	25                      # 0x19
	.long	19                      # 0x13
	.long	20                      # 0x14
	.long	19                      # 0x13
	.long	25                      # 0x19
	.long	19                      # 0x13
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	25                      # 0x19
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	24                      # 0x18
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	20                      # 0x14
	.long	19                      # 0x13
	.long	25                      # 0x19
	.long	19                      # 0x13
	.long	20                      # 0x14
	.long	19                      # 0x13
	.long	25                      # 0x19
	.long	19                      # 0x13
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	25                      # 0x19
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	24                      # 0x18
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	22                      # 0x16
	.long	21                      # 0x15
	.long	28                      # 0x1c
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	21                      # 0x15
	.long	28                      # 0x1c
	.long	21                      # 0x15
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	22                      # 0x16
	.long	21                      # 0x15
	.long	28                      # 0x1c
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	21                      # 0x15
	.long	28                      # 0x1c
	.long	21                      # 0x15
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	24                      # 0x18
	.long	33                      # 0x21
	.long	24                      # 0x18
	.long	26                      # 0x1a
	.long	24                      # 0x18
	.long	33                      # 0x21
	.long	24                      # 0x18
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	33                      # 0x21
	.long	31                      # 0x1f
	.long	42                      # 0x2a
	.long	31                      # 0x1f
	.long	33                      # 0x21
	.long	31                      # 0x1f
	.long	42                      # 0x2a
	.long	31                      # 0x1f
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	26                      # 0x1a
	.long	24                      # 0x18
	.long	33                      # 0x21
	.long	24                      # 0x18
	.long	26                      # 0x1a
	.long	24                      # 0x18
	.long	33                      # 0x21
	.long	24                      # 0x18
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	33                      # 0x21
	.long	31                      # 0x1f
	.long	42                      # 0x2a
	.long	31                      # 0x1f
	.long	33                      # 0x21
	.long	31                      # 0x1f
	.long	42                      # 0x2a
	.long	31                      # 0x1f
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	35                      # 0x23
	.long	33                      # 0x21
	.long	45                      # 0x2d
	.long	33                      # 0x21
	.long	35                      # 0x23
	.long	33                      # 0x21
	.long	45                      # 0x2d
	.long	33                      # 0x21
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	35                      # 0x23
	.long	33                      # 0x21
	.long	45                      # 0x2d
	.long	33                      # 0x21
	.long	35                      # 0x23
	.long	33                      # 0x21
	.long	45                      # 0x2d
	.long	33                      # 0x21
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	32                      # 0x20
	.long	30                      # 0x1e
	.long	40                      # 0x28
	.long	30                      # 0x1e
	.long	32                      # 0x20
	.long	30                      # 0x1e
	.long	40                      # 0x28
	.long	30                      # 0x1e
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	40                      # 0x28
	.long	38                      # 0x26
	.long	51                      # 0x33
	.long	38                      # 0x26
	.long	40                      # 0x28
	.long	38                      # 0x26
	.long	51                      # 0x33
	.long	38                      # 0x26
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	32                      # 0x20
	.long	30                      # 0x1e
	.long	40                      # 0x28
	.long	30                      # 0x1e
	.long	32                      # 0x20
	.long	30                      # 0x1e
	.long	40                      # 0x28
	.long	30                      # 0x1e
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	40                      # 0x28
	.long	38                      # 0x26
	.long	51                      # 0x33
	.long	38                      # 0x26
	.long	40                      # 0x28
	.long	38                      # 0x26
	.long	51                      # 0x33
	.long	38                      # 0x26
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	36                      # 0x24
	.long	34                      # 0x22
	.long	46                      # 0x2e
	.long	34                      # 0x22
	.long	36                      # 0x24
	.long	34                      # 0x22
	.long	46                      # 0x2e
	.long	34                      # 0x22
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	46                      # 0x2e
	.long	43                      # 0x2b
	.long	58                      # 0x3a
	.long	43                      # 0x2b
	.long	46                      # 0x2e
	.long	43                      # 0x2b
	.long	58                      # 0x3a
	.long	43                      # 0x2b
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	36                      # 0x24
	.long	34                      # 0x22
	.long	46                      # 0x2e
	.long	34                      # 0x22
	.long	36                      # 0x24
	.long	34                      # 0x22
	.long	46                      # 0x2e
	.long	34                      # 0x22
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	46                      # 0x2e
	.long	43                      # 0x2b
	.long	58                      # 0x3a
	.long	43                      # 0x2b
	.long	46                      # 0x2e
	.long	43                      # 0x2b
	.long	58                      # 0x3a
	.long	43                      # 0x2b
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.size	dequant_coef8, 1536

	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"warning: Intra_8x8_Vertical prediction mode not allowed at mb %d\n"
	.size	.L.str, 66

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"warning: Intra_8x8_Horizontal prediction mode not allowed at mb %d\n"
	.size	.L.str.1, 68

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"warning: Intra_8x8_Diagonal_Down_Left prediction mode not allowed at mb %d\n"
	.size	.L.str.2, 76

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"warning: Intra_4x4_Vertical_Left prediction mode not allowed at mb %d\n"
	.size	.L.str.3, 71

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"warning: Intra_8x8_Diagonal_Down_Right prediction mode not allowed at mb %d\n"
	.size	.L.str.4, 77

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"warning: Intra_8x8_Vertical_Right prediction mode not allowed at mb %d\n"
	.size	.L.str.5, 72

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"warning: Intra_8x8_Horizontal_Down prediction mode not allowed at mb %d\n"
	.size	.L.str.6, 73

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"warning: Intra_8x8_Horizontal_Up prediction mode not allowed at mb %d\n"
	.size	.L.str.7, 71

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Error: illegal intra_4x4 prediction mode: %d\n"
	.size	.L.str.8, 46

	.type	itrans8x8.ipos,@object  # @itrans8x8.ipos
	.local	itrans8x8.ipos
	.comm	itrans8x8.ipos,4,4
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	cofAC8x8_intra,@object  # @cofAC8x8_intra
	.comm	cofAC8x8_intra,8,8
	.type	cofAC8x8_iintra,@object # @cofAC8x8_iintra
	.comm	cofAC8x8_iintra,8,8
	.type	itrans8x8.a.0,@object   # @itrans8x8.a.0
	.local	itrans8x8.a.0
	.comm	itrans8x8.a.0,4,16
	.type	itrans8x8.a.1,@object   # @itrans8x8.a.1
	.local	itrans8x8.a.1
	.comm	itrans8x8.a.1,4,16
	.type	itrans8x8.a.2,@object   # @itrans8x8.a.2
	.local	itrans8x8.a.2
	.comm	itrans8x8.a.2,4,8
	.type	itrans8x8.a.3,@object   # @itrans8x8.a.3
	.local	itrans8x8.a.3
	.comm	itrans8x8.a.3,4,16
	.type	itrans8x8.a.4,@object   # @itrans8x8.a.4
	.local	itrans8x8.a.4
	.comm	itrans8x8.a.4,4,16
	.type	itrans8x8.a.5,@object   # @itrans8x8.a.5
	.local	itrans8x8.a.5
	.comm	itrans8x8.a.5,4,16
	.type	itrans8x8.a.6,@object   # @itrans8x8.a.6
	.local	itrans8x8.a.6
	.comm	itrans8x8.a.6,4,8
	.type	itrans8x8.a.7,@object   # @itrans8x8.a.7
	.local	itrans8x8.a.7
	.comm	itrans8x8.a.7,4,16
	.type	itrans8x8.b.0,@object   # @itrans8x8.b.0
	.local	itrans8x8.b.0
	.comm	itrans8x8.b.0,4,16
	.type	itrans8x8.b.1,@object   # @itrans8x8.b.1
	.local	itrans8x8.b.1
	.comm	itrans8x8.b.1,4,16
	.type	itrans8x8.b.2,@object   # @itrans8x8.b.2
	.local	itrans8x8.b.2
	.comm	itrans8x8.b.2,4,8
	.type	itrans8x8.b.3,@object   # @itrans8x8.b.3
	.local	itrans8x8.b.3
	.comm	itrans8x8.b.3,4,16
	.type	itrans8x8.b.4,@object   # @itrans8x8.b.4
	.local	itrans8x8.b.4
	.comm	itrans8x8.b.4,4,16
	.type	itrans8x8.b.5,@object   # @itrans8x8.b.5
	.local	itrans8x8.b.5
	.comm	itrans8x8.b.5,4,16
	.type	itrans8x8.b.6,@object   # @itrans8x8.b.6
	.local	itrans8x8.b.6
	.comm	itrans8x8.b.6,4,8
	.type	itrans8x8.b.7,@object   # @itrans8x8.b.7
	.local	itrans8x8.b.7
	.comm	itrans8x8.b.7,4,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
