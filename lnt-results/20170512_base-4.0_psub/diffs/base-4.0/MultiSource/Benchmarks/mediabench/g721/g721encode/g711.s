	.text
	.file	"g711.bc"
	.globl	linear2alaw
	.p2align	4, 0x90
	.type	linear2alaw,@function
linear2alaw:                            # @linear2alaw
	.cfi_startproc
# BB#0:
	movl	$-8, %eax
	subl	%edi, %eax
	testl	%edi, %edi
	cmovnsl	%edi, %eax
	sarl	$31, %edi
	andl	$-128, %edi
	addl	$213, %edi
	movswl	seg_end(%rip), %ecx
	xorl	%r8d, %r8d
	cmpl	%eax, %ecx
	jge	.LBB0_9
# BB#1:
	movswl	seg_end+2(%rip), %ecx
	movl	$1, %r8d
	cmpl	%eax, %ecx
	jge	.LBB0_9
# BB#2:
	movswl	seg_end+4(%rip), %ecx
	movl	$2, %r8d
	cmpl	%eax, %ecx
	jge	.LBB0_9
# BB#3:
	movswl	seg_end+6(%rip), %ecx
	movl	$3, %r8d
	cmpl	%eax, %ecx
	jge	.LBB0_9
# BB#4:
	movswl	seg_end+8(%rip), %ecx
	movl	$4, %r8d
	cmpl	%eax, %ecx
	jge	.LBB0_9
# BB#5:
	movswl	seg_end+10(%rip), %ecx
	movl	$5, %r8d
	cmpl	%eax, %ecx
	jge	.LBB0_9
# BB#6:
	movswl	seg_end+12(%rip), %ecx
	movl	$6, %r8d
	cmpl	%eax, %ecx
	jge	.LBB0_9
# BB#7:
	movswl	seg_end+14(%rip), %ecx
	movl	$7, %r8d
	cmpl	%eax, %ecx
	jge	.LBB0_9
# BB#8:                                 # %search.exit
	xorl	$127, %edi
	movl	%edi, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB0_9:
	movl	%r8d, %esi
	shll	$4, %esi
	leal	3(%r8), %ecx
	movl	%eax, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	shrl	$4, %eax
	cmpl	$2, %r8d
	cmovael	%edx, %eax
	andl	$15, %eax
	orl	%esi, %eax
	xorl	%edi, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end0:
	.size	linear2alaw, .Lfunc_end0-linear2alaw
	.cfi_endproc

	.globl	alaw2linear
	.p2align	4, 0x90
	.type	alaw2linear,@function
alaw2linear:                            # @alaw2linear
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	xorb	$85, %al
	movzbl	%al, %ecx
	movl	%ecx, %edx
	andl	$15, %edx
	shll	$4, %edx
	shrl	$4, %ecx
	movl	%ecx, %eax
	andb	$7, %al
	cmpb	$1, %al
	je	.LBB1_3
# BB#1:
	testb	%al, %al
	jne	.LBB1_4
# BB#2:
	orl	$8, %edx
	jmp	.LBB1_5
.LBB1_3:
	orl	$264, %edx              # imm = 0x108
	jmp	.LBB1_5
.LBB1_4:
	andl	$7, %ecx
	orl	$264, %edx              # imm = 0x108
	decl	%ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
.LBB1_5:
	movl	%edx, %eax
	negl	%eax
	testb	%dil, %dil
	cmovsl	%edx, %eax
	retq
.Lfunc_end1:
	.size	alaw2linear, .Lfunc_end1-alaw2linear
	.cfi_endproc

	.globl	linear2ulaw
	.p2align	4, 0x90
	.type	linear2ulaw,@function
linear2ulaw:                            # @linear2ulaw
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	negl	%eax
	cmovll	%edi, %eax
	sarl	$31, %edi
	andl	$-128, %edi
	addl	$255, %edi
	addl	$132, %eax
	movswl	seg_end(%rip), %edx
	xorl	%ecx, %ecx
	cmpl	%eax, %edx
	jge	.LBB2_9
# BB#1:
	movswl	seg_end+2(%rip), %edx
	movl	$1, %ecx
	cmpl	%eax, %edx
	jge	.LBB2_9
# BB#2:
	movswl	seg_end+4(%rip), %edx
	movl	$2, %ecx
	cmpl	%eax, %edx
	jge	.LBB2_9
# BB#3:
	movswl	seg_end+6(%rip), %edx
	movl	$3, %ecx
	cmpl	%eax, %edx
	jge	.LBB2_9
# BB#4:
	movswl	seg_end+8(%rip), %edx
	movl	$4, %ecx
	cmpl	%eax, %edx
	jge	.LBB2_9
# BB#5:
	movswl	seg_end+10(%rip), %edx
	movl	$5, %ecx
	cmpl	%eax, %edx
	jge	.LBB2_9
# BB#6:
	movswl	seg_end+12(%rip), %edx
	movl	$6, %ecx
	cmpl	%eax, %edx
	jge	.LBB2_9
# BB#7:
	movswl	seg_end+14(%rip), %edx
	movl	$7, %ecx
	cmpl	%eax, %edx
	jge	.LBB2_9
# BB#8:                                 # %search.exit
	andl	$128, %edi
	movl	%edi, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB2_9:
	movl	%ecx, %edx
	shll	$4, %edx
	addl	$3, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	andl	$15, %eax
	orl	%edx, %eax
	xorl	%edi, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end2:
	.size	linear2ulaw, .Lfunc_end2-linear2ulaw
	.cfi_endproc

	.globl	ulaw2linear
	.p2align	4, 0x90
	.type	ulaw2linear,@function
ulaw2linear:                            # @ulaw2linear
	.cfi_startproc
# BB#0:
	movl	%edi, %ecx
	notb	%cl
	movl	%ecx, %eax
	shlb	$3, %al
	movzbl	%al, %eax
	andl	$120, %eax
	orl	$132, %eax
	shrb	$4, %cl
	andb	$7, %cl
	shll	%cl, %eax
	movl	$132, %ecx
	subl	%eax, %ecx
	addl	$-132, %eax
	testb	%dil, %dil
	cmovnsl	%ecx, %eax
	retq
.Lfunc_end3:
	.size	ulaw2linear, .Lfunc_end3-ulaw2linear
	.cfi_endproc

	.globl	alaw2ulaw
	.p2align	4, 0x90
	.type	alaw2ulaw,@function
alaw2ulaw:                              # @alaw2ulaw
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testb	%dil, %dil
	movl	$213, %eax
	movl	$85, %ecx
	cmovsq	%rax, %rcx
	xorq	%rdi, %rcx
	movl	%edi, %eax
	orb	$127, %al
	xorb	_a2u(%rcx), %al
	retq
.Lfunc_end4:
	.size	alaw2ulaw, .Lfunc_end4-alaw2ulaw
	.cfi_endproc

	.globl	ulaw2alaw
	.p2align	4, 0x90
	.type	ulaw2alaw,@function
ulaw2alaw:                              # @ulaw2alaw
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testb	%dil, %dil
	movl	$255, %eax
	movl	$127, %ecx
	cmovsq	%rax, %rcx
	xorq	%rdi, %rcx
	movl	%edi, %edx
	andb	$-128, %dl
	orb	$85, %dl
	movb	_u2a(%rcx), %al
	decb	%al
	xorb	%dl, %al
	retq
.Lfunc_end5:
	.size	ulaw2alaw, .Lfunc_end5-ulaw2alaw
	.cfi_endproc

	.type	_u2a,@object            # @_u2a
	.data
	.globl	_u2a
	.p2align	4
_u2a:
	.ascii	"\001\001\002\002\003\003\004\004\005\005\006\006\007\007\b\b\t\n\013\f\r\016\017\020\021\022\023\024\025\026\027\030\031\033\035\037!\"#$%&'()*+,.0123456789:;<=>@ABCDEFGHIJKLMNOQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\177\200"
	.size	_u2a, 128

	.type	_a2u,@object            # @_a2u
	.globl	_a2u
	.p2align	4
_a2u:
	.ascii	"\001\003\005\007\t\013\r\017\020\021\022\023\024\025\026\027\030\031\032\033\034\035\036\037  !!\"\"##$%&'()*+,-./001123456789:;<=>?@@ABCDEFGHIJKLMNOOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\177"
	.size	_a2u, 128

	.type	seg_end,@object         # @seg_end
	.p2align	4
seg_end:
	.short	255                     # 0xff
	.short	511                     # 0x1ff
	.short	1023                    # 0x3ff
	.short	2047                    # 0x7ff
	.short	4095                    # 0xfff
	.short	8191                    # 0x1fff
	.short	16383                   # 0x3fff
	.short	32767                   # 0x7fff
	.size	seg_end, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
