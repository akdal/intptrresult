	.text
	.file	"z13.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1006632960              # float 0.0078125
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_1:
	.quad	4607632778762754458     # double 1.1000000000000001
	.text
	.globl	BreakObject
	.p2align	4, 0x90
	.type	BreakObject,@function
BreakObject:                            # @BreakObject
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r12
	movl	(%rbp), %edi
	testl	%edi, %edi
	js	.LBB0_10
# BB#1:
	movl	4(%rbp), %eax
	testl	%eax, %eax
	js	.LBB0_10
# BB#2:
	movl	8(%rbp), %r14d
	testl	%r14d, %r14d
	js	.LBB0_10
# BB#3:
	movl	48(%r12), %ecx
	cmpl	%edi, %ecx
	jle	.LBB0_16
.LBB0_4:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movb	32(%r12), %bl
	movl	%ebx, %edx
	addb	$-9, %dl
	cmpb	$90, %dl
	ja	.LBB0_40
# BB#5:
	leaq	48(%r12), %rbp
	leaq	32(%r12), %r15
	movzbl	%dl, %edx
	jmpq	*.LJTI0_0(,%rdx,8)
.LBB0_6:
	movq	8(%r12), %rax
	cmpq	(%r12), %rax
	je	.LBB0_8
# BB#7:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%r12), %rax
.LBB0_8:
	movq	16(%rsp), %rsi          # 8-byte Reload
	addq	$16, %rax
	.p2align	4, 0x90
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB0_9
	jmp	.LBB0_91
.LBB0_10:                               # %._crit_edge576
	leaq	32(%r12), %r14
	callq	EchoLength
	movq	%rax, %r15
	movl	4(%rbp), %edi
	callq	EchoLength
	movq	%rax, %rbx
	movl	8(%rbp), %edi
	callq	EchoLength
	movq	%rax, 8(%rsp)
	movq	%rbx, (%rsp)
	movl	$13, %edi
	movl	$11, %esi
	movl	$.L.str, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%r15, %r9
	callq	Error
	movl	$11, %edi
	movl	$.L.str.1, %esi
	movq	%r14, %rdx
	callq	MakeWord
	movq	%rax, %rbp
	movl	$0, 56(%rbp)
	movl	$0, 48(%rbp)
	movq	%r12, zz_hold(%rip)
	movq	24(%r12), %rax
	cmpq	%r12, %rax
	je	.LBB0_14
# BB#11:
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r12), %rcx
	movq	%rax, 24(%rcx)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%rax, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_15
# BB#12:
	testq	%rax, %rax
	je	.LBB0_15
# BB#13:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbp), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbp)
	movq	%rbp, 24(%rcx)
	jmp	.LBB0_15
.LBB0_14:                               # %.thread
	movq	$0, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB0_15:
	movq	%r12, %rdi
	callq	DisposeObject
	jmp	.LBB0_98
.LBB0_16:
	movl	56(%r12), %edx
	cmpl	%r14d, %edx
	jg	.LBB0_4
# BB#17:
	addl	%ecx, %edx
	cmpl	%eax, %edx
	jle	.LBB0_97
	jmp	.LBB0_4
.LBB0_18:
	movq	8(%r12), %rdi
	.p2align	4, 0x90
.LBB0_19:                               # =>This Inner Loop Header: Depth=1
	addq	$16, %rdi
	movq	(%rdi), %rdi
	movzbl	32(%rdi), %eax
	testb	%al, %al
	je	.LBB0_19
# BB#20:
	cmpb	$13, %al
	jne	.LBB0_24
# BB#21:
	movl	$0, (%rbp)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	4(%rcx), %eax
	movl	8(%rcx), %ecx
	cmpl	%ecx, %eax
	cmovlel	%eax, %ecx
	movl	%ecx, 56(%r12)
	cmpl	$0, 48(%r12)
	jns	.LBB0_95
	jmp	.LBB0_94
.LBB0_22:
	movq	(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB0_23:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB0_23
	jmp	.LBB0_90
.LBB0_24:
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	BreakObject
	movl	48(%rax), %ecx
	movl	%ecx, (%rbp)
	jmp	.LBB0_92
.LBB0_25:
	cmpl	$0, 40(%r12)
	js	.LBB0_111
# BB#26:
	movq	BackEnd(%rip), %rax
	cmpl	$0, 16(%rax)
	movq	16(%rsp), %rsi          # 8-byte Reload
	je	.LBB0_110
# BB#27:
	movq	%r12, %rdi
	callq	InsertScale
	testl	%eax, %eax
	je	.LBB0_110
# BB#28:
	movq	24(%r12), %rbx
.LBB0_29:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB0_29
# BB#30:
	movq	%rbx, %r8
	addq	$32, %r8
	addq	$64, %r12
	movl	64(%rbx), %eax
	cvtsi2ssl	%eax, %xmm0
	mulss	.LCPI0_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$13, %edi
	movl	$5, %esi
	movl	$.L.str.6, %edx
	movl	$2, %ecx
	movb	$1, %al
	movq	%r12, %r9
	callq	Error
	movq	%rbx, %r12
	cmpl	$0, 48(%r12)
	jns	.LBB0_95
	jmp	.LBB0_94
.LBB0_31:
	movq	(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB0_32:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB0_32
	jmp	.LBB0_90
.LBB0_33:
	movq	BackEnd(%rip), %rax
	cmpl	$0, 16(%rax)
	je	.LBB0_115
# BB#34:
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	InsertScale
	testl	%eax, %eax
	je	.LBB0_114
# BB#35:
	movq	24(%r12), %r12
.LBB0_36:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12), %r12
	movzbl	32(%r12), %eax
	testb	%al, %al
	je	.LBB0_36
# BB#37:
	movq	%r12, %r8
	addq	$32, %r8
	cmpb	$94, %al
	movl	$.L.str.9, %eax
	movl	$.L.str.10, %r9d
	cmoveq	%rax, %r9
	movl	64(%r12), %eax
	cvtsi2ssl	%eax, %xmm0
	mulss	.LCPI0_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$13, %edi
	movl	$7, %esi
	movl	$.L.str.8, %edx
	movl	$2, %ecx
	jmp	.LBB0_104
.LBB0_38:
	movq	8(%r12), %rax
	addq	$16, %rax
.LBB0_39:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB0_39
	jmp	.LBB0_90
.LBB0_40:
	movzbl	%bl, %edi
	movq	no_fpos(%rip), %rbx
	callq	Image
	movq	%rax, (%rsp)
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.16, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	cmpl	$0, 48(%r12)
	jns	.LBB0_95
	jmp	.LBB0_94
.LBB0_41:
	movq	(%r12), %rsi
	movq	8(%r12), %rdi
	leaq	56(%r12), %r9
	xorl	%edx, %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, %r8
	callq	BreakJoinedGroup
	cmpl	$0, 48(%r12)
	jns	.LBB0_95
	jmp	.LBB0_94
.LBB0_42:
	testl	%ecx, %ecx
	movq	16(%rsp), %rbx          # 8-byte Reload
	jle	.LBB0_128
# BB#43:
	addl	56(%r12), %ecx
	cmpl	$8388608, %ecx          # imm = 0x800000
	movl	$8388607, %eax          # imm = 0x7FFFFF
	cmovll	%ecx, %eax
	movl	%eax, 56(%r12)
	movl	$0, 48(%r12)
	movq	8(%r12), %rax
	cmpq	%r12, %rax
	movq	%r12, %r8
	je	.LBB0_124
# BB#44:                                # %.preheader.lr.ph.preheader
	movq	%r12, %rcx
.LBB0_45:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_46 Depth 2
	movq	%rax, %r8
	.p2align	4, 0x90
.LBB0_46:                               #   Parent Loop BB0_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r8), %r8
	movzbl	32(%r8), %edx
	testb	%dl, %dl
	je	.LBB0_46
# BB#47:                                #   in Loop: Header=BB0_45 Depth=1
	cmpb	$1, %dl
	jne	.LBB0_49
# BB#48:                                #   in Loop: Header=BB0_45 Depth=1
	movzwl	44(%r8), %edx
	testb	$1, %dh
	jne	.LBB0_50
.LBB0_49:                               # %.backedge
                                        #   in Loop: Header=BB0_45 Depth=1
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	jne	.LBB0_45
	jmp	.LBB0_123
.LBB0_50:                               # %.outer
                                        #   in Loop: Header=BB0_45 Depth=1
	andl	$65279, %edx            # imm = 0xFEFF
	movw	%dx, 44(%r8)
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	movq	%r8, %rcx
	jne	.LBB0_45
	jmp	.LBB0_124
.LBB0_51:
	leaq	8(%r12), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	8(%r12), %rbx
.LBB0_52:                               # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %ecx
	testb	%cl, %cl
	je	.LBB0_52
# BB#53:
	cmpb	$1, %cl
	jne	.LBB0_55
# BB#54:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.20, %r9d
	xorl	%eax, %eax
	callq	Error
	movb	32(%rbx), %cl
.LBB0_55:                               # %.loopexit397
	movl	%ecx, %eax
	addb	$-119, %al
	cmpb	$19, %al
	ja	.LBB0_57
# BB#56:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.21, %r9d
	xorl	%eax, %eax
	callq	Error
	movb	32(%rbx), %cl
.LBB0_57:
	addb	$-2, %cl
	cmpb	$7, %cl
	sbbl	%eax, %eax
	xorl	%edx, %edx
	cmpb	$7, %cl
	movq	%rbx, %rbp
	cmovbq	%rdx, %rbp
	andl	$1, %eax
	movl	%eax, 52(%rbx)
	xorl	%eax, %eax
	cmpb	$6, %cl
	seta	%al
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	movq	136(%rsp), %r13         # 8-byte Reload
	xorl	%ecx, %ecx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	jmp	.LBB0_156
.LBB0_58:
	movq	8(%r12), %r15
	cmpq	%r12, %r15
	jne	.LBB0_60
# BB#59:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.28, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	4(%rcx), %eax
	movl	8(%rcx), %r14d
	movq	8(%r12), %r15
.LBB0_60:
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	movl	$8388607, 56(%rsp)      # imm = 0x7FFFFF
	cmpl	%r14d, %eax
	cmovlel	%eax, %r14d
	movl	%r14d, 60(%rsp)
	movl	$8388607, 64(%rsp)      # imm = 0x7FFFFF
	cmpq	%r12, %r15
	je	.LBB0_129
# BB#61:                                # %.preheader398.preheader
                                        # implicit-def: %EBX
                                        # implicit-def: %ESI
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	xorl	%r13d, %r13d
                                        # implicit-def: %RDX
	xorl	%edi, %edi
.LBB0_62:                               # %.preheader398
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_63 Depth 2
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB0_63:                               #   Parent Loop BB0_62 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_63
# BB#64:                                #   in Loop: Header=BB0_62 Depth=1
	movl	%eax, %ecx
	addb	$-119, %cl
	cmpb	$20, %cl
	jb	.LBB0_77
# BB#65:                                #   in Loop: Header=BB0_62 Depth=1
	cmpb	$1, %al
	jne	.LBB0_71
# BB#66:                                #   in Loop: Header=BB0_62 Depth=1
	testq	%rdi, %rdi
	jne	.LBB0_68
# BB#67:                                #   in Loop: Header=BB0_62 Depth=1
	movq	no_fpos(%rip), %r8
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	movl	$1, %edi
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movl	$2, %esi
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movq	%rdx, %rbx
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.29, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	120(%rsp), %rdi         # 8-byte Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	movq	104(%rsp), %rbx         # 8-byte Reload
.LBB0_68:                               #   in Loop: Header=BB0_62 Depth=1
	testb	$2, 45(%rbp)
	jne	.LBB0_77
# BB#69:                                #   in Loop: Header=BB0_62 Depth=1
	cmpl	$8388607, %esi          # imm = 0x7FFFFF
	jle	.LBB0_74
.LBB0_70:                               #   in Loop: Header=BB0_62 Depth=1
	movq	%r15, %rsi
	movq	%rdx, %rbp
	leaq	56(%rsp), %rcx
	leaq	32(%rsp), %r8
	leaq	48(%rsp), %r9
	callq	BreakJoinedGroup
	movq	%rbp, %rdx
	movl	32(%rsp), %esi
	movl	48(%rsp), %ebx
	jmp	.LBB0_76
.LBB0_71:                               #   in Loop: Header=BB0_62 Depth=1
	movl	48(%rbp), %eax
	testq	%rdi, %rdi
	je	.LBB0_73
# BB#72:                                #   in Loop: Header=BB0_62 Depth=1
	cmpl	%eax, %esi
	cmovll	%eax, %esi
	movl	%esi, 32(%rsp)
	movl	56(%rbp), %eax
	cmpl	%eax, %ebx
	cmovll	%eax, %ebx
	movl	%ebx, 48(%rsp)
	cmpl	56(%rdx), %eax
	cmovgq	%rbp, %rdx
	jmp	.LBB0_77
.LBB0_73:                               #   in Loop: Header=BB0_62 Depth=1
	movl	%eax, 32(%rsp)
	movl	56(%rbp), %ebx
	movl	%ebx, 48(%rsp)
	movl	%eax, %esi
	movq	%r15, %rdi
	movq	%rbp, %rdx
	jmp	.LBB0_77
.LBB0_74:                               #   in Loop: Header=BB0_62 Depth=1
	leal	(%rbx,%rsi), %eax
	cmpl	%r14d, %eax
	jg	.LBB0_70
# BB#75:                                #   in Loop: Header=BB0_62 Depth=1
	cmpl	$8388608, %ebx          # imm = 0x800000
	jge	.LBB0_70
.LBB0_76:                               #   in Loop: Header=BB0_62 Depth=1
	leal	(%rbx,%rsi), %eax
	cmpl	%eax, %r13d
	cmovll	%eax, %r13d
	movl	$1, 40(%rsp)            # 4-byte Folded Spill
	xorl	%edi, %edi
.LBB0_77:                               #   in Loop: Header=BB0_62 Depth=1
	movq	8(%r15), %r15
	cmpq	%r12, %r15
	jne	.LBB0_62
# BB#78:                                # %._crit_edge501
	testq	%rdi, %rdi
	jne	.LBB0_131
	jmp	.LBB0_130
.LBB0_79:
	movq	%rbp, %rbx
	leaq	64(%r12), %rbp
	movq	%rbp, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	MinConstraint
	movq	8(%r12), %rax
	addq	$16, %rax
.LBB0_80:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB0_80
# BB#81:
	movq	%rbp, %rsi
	callq	BreakObject
	movl	48(%rax), %ecx
	movl	%ecx, 48(%r12)
	movl	56(%rax), %eax
	leaq	56(%r12), %rsi
	movl	%eax, 56(%r12)
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	EnlargeToConstraint
	cmpl	$0, 48(%r12)
	jns	.LBB0_95
	jmp	.LBB0_94
.LBB0_82:
	movq	8(%r12), %rax
	addq	$16, %rax
.LBB0_83:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rbp
	leaq	16(%rbp), %rax
	cmpb	$0, 32(%rbp)
	je	.LBB0_83
# BB#84:
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	FindShift
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	(%rsi), %ecx
	movl	4(%rsi), %edx
	cmpl	%edx, %ecx
	cmovgl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, 56(%rsp)
	movl	%edx, 60(%rsp)
	movl	8(%rsi), %ecx
	cmpl	%edx, %ecx
	cmovlel	%ecx, %edx
	addl	%eax, %edx
	movl	%edx, 64(%rsp)
	leaq	56(%rsp), %rsi
	movq	%rbp, %rdi
	callq	BreakObject
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	FindShift
	movl	48(%rbp), %ecx
	addl	%eax, %ecx
	movl	$0, %edx
	cmovnsl	%ecx, %edx
	cmpl	$8388607, %ecx          # imm = 0x7FFFFF
	movl	$8388607, %ecx          # imm = 0x7FFFFF
	cmovgl	%ecx, %edx
	movl	%edx, 48(%r12)
	movl	56(%rbp), %edx
	subl	%eax, %edx
	cmovnsl	%edx, %ebx
	cmpl	$8388607, %edx          # imm = 0x7FFFFF
	cmovgl	%ecx, %ebx
	movl	%ebx, 56(%r12)
	cmpl	$0, 48(%r12)
	jns	.LBB0_95
	jmp	.LBB0_94
.LBB0_85:
	movl	64(%r12), %esi
	leaq	56(%rsp), %rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	InvScaleConstraint
	movq	8(%r12), %rax
	addq	$16, %rax
.LBB0_86:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB0_86
# BB#87:
	leaq	56(%rsp), %rsi
	callq	BreakObject
	movl	64(%r12), %ecx
	movl	48(%rax), %edx
	imull	%ecx, %edx
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$25, %esi
	addl	%edx, %esi
	sarl	$7, %esi
	movl	%esi, 48(%r12)
	imull	56(%rax), %ecx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	$25, %eax
	addl	%ecx, %eax
	sarl	$7, %eax
	jmp	.LBB0_138
.LBB0_88:
	movq	(%r12), %rax
	addq	$16, %rax
.LBB0_89:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB0_89
.LBB0_90:
	movq	16(%rsp), %rsi          # 8-byte Reload
.LBB0_91:
	callq	BreakObject
	movl	48(%rax), %ecx
	movl	%ecx, 48(%r12)
.LBB0_92:
	movl	56(%rax), %eax
	movl	%eax, 56(%r12)
.LBB0_93:
	cmpl	$0, 48(%r12)
	jns	.LBB0_95
.LBB0_94:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.18, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_95:
	cmpl	$0, 56(%r12)
	jns	.LBB0_97
# BB#96:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.19, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_97:
	movq	%r12, %rbp
.LBB0_98:
	movq	%rbp, %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_99:
	movq	BackEnd(%rip), %rax
	cmpl	$0, 16(%rax)
	je	.LBB0_113
# BB#100:
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	InsertScale
	testl	%eax, %eax
	je	.LBB0_113
# BB#101:
	movq	24(%r12), %r12
.LBB0_102:                              # =>This Inner Loop Header: Depth=1
	movq	(%r12), %r12
	cmpb	$0, 32(%r12)
	je	.LBB0_102
# BB#103:
	movq	%r12, %r8
	addq	$32, %r8
	movl	64(%r12), %eax
	cvtsi2ssl	%eax, %xmm0
	mulss	.LCPI0_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$13, %edi
	movl	$3, %esi
	movl	$.L.str.2, %edx
	movl	$2, %ecx
	movl	$.L.str.3, %r9d
.LBB0_104:
	movb	$1, %al
	callq	Error
	cmpl	$0, 48(%r12)
	jns	.LBB0_95
	jmp	.LBB0_94
.LBB0_105:
	movq	8(%r12), %rax
	addq	$16, %rax
.LBB0_106:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB0_106
# BB#107:
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	callq	BreakObject
	movq	(%r12), %rax
	addq	$16, %rax
.LBB0_108:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB0_108
# BB#109:
	movq	%rbx, %rsi
	jmp	.LBB0_91
.LBB0_110:
	leaq	64(%r12), %r9
	movl	$13, %edi
	movl	$6, %esi
	movl	$.L.str.7, %edx
	jmp	.LBB0_116
.LBB0_111:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	movq	16(%rsp), %r14          # 8-byte Reload
	je	.LBB0_139
# BB#112:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_140
.LBB0_113:
	movq	$.L.str.3, (%rsp)
	movl	$13, %edi
	movl	$4, %esi
	movl	$.L.str.4, %edx
	movl	$2, %ecx
	movl	$.L.str.3, %r9d
	jmp	.LBB0_117
.LBB0_114:                              # %._crit_edge
	movb	(%r15), %bl
.LBB0_115:
	cmpb	$94, %bl
	movl	$.L.str.9, %eax
	movl	$.L.str.10, %r9d
	cmoveq	%rax, %r9
	movl	$13, %edi
	movl	$8, %esi
	movl	$.L.str.11, %edx
.LBB0_116:
	movl	$2, %ecx
.LBB0_117:
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
	movl	$11, %edi
	movl	$.L.str.1, %esi
	movq	%r15, %rdx
	callq	MakeWord
	movq	%rax, %rbp
	movl	$0, 56(%rbp)
	movl	$0, 48(%rbp)
	movq	%r12, zz_hold(%rip)
	movq	24(%r12), %rax
	cmpq	%r12, %rax
	je	.LBB0_121
# BB#118:
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r12), %rcx
	movq	%rax, 24(%rcx)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%rax, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_122
# BB#119:
	testq	%rax, %rax
	je	.LBB0_122
# BB#120:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbp), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbp)
	movq	%rbp, 24(%rcx)
	jmp	.LBB0_122
.LBB0_121:                              # %.thread590
	movq	$0, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB0_122:
	movq	%r12, %rdi
	callq	DisposeObject
	movq	%rbp, %r12
	cmpl	$0, 48(%r12)
	jns	.LBB0_95
	jmp	.LBB0_94
.LBB0_123:
	movq	%rcx, %r8
.LBB0_124:                              # %.outer._crit_edge
	movl	48(%r12), %eax
	cmpl	(%rbx), %eax
	jg	.LBB0_127
# BB#125:
	movl	56(%r12), %ecx
	addl	%ecx, %eax
	cmpl	4(%rbx), %eax
	jg	.LBB0_127
# BB#126:
	cmpl	8(%rbx), %ecx
	jle	.LBB0_154
.LBB0_127:
	addq	$32, %r8
	movl	$13, %edi
	movl	$10, %esi
	movl	$.L.str.15, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_128:
	leaq	32(%rsp), %rax
	movq	%rax, (%rsp)
	xorl	%edx, %edx
	movl	$1, %ecx
	movl	$1, %r8d
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	FillObject
	jmp	.LBB0_153
.LBB0_129:
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	xorl	%r13d, %r13d
                                        # implicit-def: %RDX
                                        # implicit-def: %ESI
                                        # implicit-def: %EBX
.LBB0_130:                              # %._crit_edge501.thread
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movq	%rsi, %r15
	movl	$2, %esi
	movq	%rdx, %rbp
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.30, %r9d
	xorl	%eax, %eax
	callq	Error
	xorl	%edi, %edi
	movq	%r15, %rsi
	movq	%rbp, %rdx
.LBB0_131:
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	je	.LBB0_134
# BB#132:
	cmpl	$8388607, %esi          # imm = 0x7FFFFF
	jle	.LBB0_135
.LBB0_133:
	movq	(%r12), %rsi
	leaq	56(%rsp), %rcx
	leaq	32(%rsp), %r8
	leaq	48(%rsp), %r9
	callq	BreakJoinedGroup
	movl	32(%rsp), %esi
	movl	48(%rsp), %ebx
	jmp	.LBB0_137
.LBB0_134:
	movq	(%r12), %rsi
	leaq	32(%rsp), %r8
	leaq	48(%rsp), %r9
	movq	16(%rsp), %rcx          # 8-byte Reload
	callq	BreakJoinedGroup
	movl	32(%rsp), %eax
	movl	%eax, 48(%r12)
	movl	48(%rsp), %eax
	jmp	.LBB0_138
.LBB0_135:
	leal	(%rbx,%rsi), %eax
	cmpl	%r14d, %eax
	jg	.LBB0_133
# BB#136:
	cmpl	$8388608, %ebx          # imm = 0x800000
	jge	.LBB0_133
.LBB0_137:
	addl	%esi, %ebx
	cmpl	%ebx, %r13d
	cmovgel	%r13d, %ebx
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax)
	cmpl	$8388608, %ebx          # imm = 0x800000
	movl	$8388607, %eax          # imm = 0x7FFFFF
	cmovll	%ebx, %eax
.LBB0_138:
	movl	%eax, 56(%r12)
	cmpl	$0, 48(%r12)
	jns	.LBB0_95
	jmp	.LBB0_94
.LBB0_139:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB0_140:
	movb	$17, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movzwl	34(%r12), %eax
	movw	%ax, 34(%rbp)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r12), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbp), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbp)
	andl	36(%r12), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbp)
	movl	48(%r12), %eax
	movl	%eax, 48(%rbp)
	movl	56(%r12), %eax
	movl	%eax, 56(%rbp)
	movl	52(%r12), %eax
	movl	%eax, 52(%rbp)
	movl	60(%r12), %eax
	movl	%eax, 60(%rbp)
	movzwl	64(%rbp), %eax
	andl	$127, %eax
	orl	$25600, %eax            # imm = 0x6400
	movw	%ax, 64(%rbp)
	movl	40(%r12), %edi
	andl	$4095, %edi             # imm = 0xFFF
	movq	%r12, %rsi
	callq	FontSize
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI0_1(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movw	%ax, 66(%rbp)
	movzwl	68(%rbp), %eax
	andl	$127, %eax
	orl	$9728, %eax             # imm = 0x2600
	movw	%ax, 68(%rbp)
	movw	$0, 70(%rbp)
	movb	$74, 68(%rbp)
	andb	$-9, 64(%rbp)
	movl	$4095, %eax             # imm = 0xFFF
	andl	40(%r12), %eax
	movl	$-4096, %ecx            # imm = 0xF000
	andl	76(%rbp), %ecx
	orl	%eax, %ecx
	movl	%ecx, 76(%rbp)
	movl	$4190208, %eax          # imm = 0x3FF000
	andl	40(%r12), %eax
	andl	$-4190209, %ecx         # imm = 0xFFC00FFF
	orl	%eax, %ecx
	movl	%ecx, 76(%rbp)
	movl	$4194304, %eax          # imm = 0x400000
	andl	40(%r12), %eax
	andl	$-12582913, %ecx        # imm = 0xFF3FFFFF
	orl	%eax, %ecx
	movl	%ecx, 76(%rbp)
	movl	40(%r12), %eax
	andl	$528482304, %eax        # imm = 0x1F800000
	andl	$-1065353217, %ecx      # imm = 0xC07FFFFF
	leal	(%rcx,%rax,2), %eax
	movl	%eax, 76(%rbp)
	movq	%r12, zz_hold(%rip)
	movq	24(%r12), %rax
	cmpq	%r12, %rax
	je	.LBB0_144
# BB#141:
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r12), %rcx
	movq	%rax, 24(%rcx)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%rax, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbp, %rbp
	sete	%bl
	je	.LBB0_145
# BB#142:
	testq	%rax, %rax
	je	.LBB0_145
# BB#143:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbp), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbp)
	movq	%rbp, 24(%rcx)
	jmp	.LBB0_145
.LBB0_144:                              # %.thread592
	movq	$0, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	testq	%rbp, %rbp
	sete	%bl
.LBB0_145:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_147
# BB#146:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_148
.LBB0_147:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_148:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rax, %rax
	sete	%cl
	orb	%cl, %bl
	jne	.LBB0_150
# BB#149:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_150:
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_152
# BB#151:
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_152:
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	BreakObject
.LBB0_153:
	movq	%rax, %r12
	cmpl	$0, 48(%r12)
	jns	.LBB0_95
	jmp	.LBB0_94
.LBB0_154:                              # %.critedge
	addq	$32, %r8
	movl	$13, %edi
	movl	$9, %esi
	movl	$.L.str.14, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	callq	Error
	cmpl	$0, 48(%r12)
	jns	.LBB0_95
	jmp	.LBB0_94
.LBB0_155:                              #   in Loop: Header=BB0_156 Depth=1
	movl	24(%rsp), %eax          # 4-byte Reload
	addl	%eax, 28(%rsp)          # 4-byte Folded Spill
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	MinGap
	movq	80(%rsp), %rcx          # 8-byte Reload
	addl	%r14d, %ecx
	addl	%eax, %ecx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movl	$1, %eax
.LBB0_156:                              # %.outer391.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_159 Depth 2
                                        #       Child Loop BB0_160 Depth 3
                                        #         Child Loop BB0_161 Depth 4
                                        #           Child Loop BB0_163 Depth 5
                                        #           Child Loop BB0_168 Depth 5
	xorl	%r14d, %r14d
	jmp	.LBB0_159
.LBB0_157:                              #   in Loop: Header=BB0_159 Depth=2
	testb	$1, 45(%rbx)
	leaq	44(%rbx), %rcx
	jne	.LBB0_155
# BB#158:                               #   in Loop: Header=BB0_159 Depth=2
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	MinGap
	addl	%eax, %r14d
	movl	24(%rsp), %eax          # 4-byte Reload
	incl	%eax
.LBB0_159:                              # %.outer391.outer392
                                        #   Parent Loop BB0_156 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_160 Depth 3
                                        #         Child Loop BB0_161 Depth 4
                                        #           Child Loop BB0_163 Depth 5
                                        #           Child Loop BB0_168 Depth 5
	movq	%rbp, 40(%rsp)          # 8-byte Spill
.LBB0_160:                              # %.outer391
                                        #   Parent Loop BB0_156 Depth=1
                                        #     Parent Loop BB0_159 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_161 Depth 4
                                        #           Child Loop BB0_163 Depth 5
                                        #           Child Loop BB0_168 Depth 5
	movl	%eax, 24(%rsp)          # 4-byte Spill
.LBB0_161:                              #   Parent Loop BB0_156 Depth=1
                                        #     Parent Loop BB0_159 Depth=2
                                        #       Parent Loop BB0_160 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_163 Depth 5
                                        #           Child Loop BB0_168 Depth 5
	movq	(%r13), %rax
	movq	8(%rax), %r13
	cmpq	%r12, %r13
	je	.LBB0_175
# BB#162:                               # %.preheader389.preheader
                                        #   in Loop: Header=BB0_161 Depth=4
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB0_163:                              # %.preheader389
                                        #   Parent Loop BB0_156 Depth=1
                                        #     Parent Loop BB0_159 Depth=2
                                        #       Parent Loop BB0_160 Depth=3
                                        #         Parent Loop BB0_161 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_163
# BB#164:                               # %.preheader389
                                        #   in Loop: Header=BB0_161 Depth=4
	cmpb	$1, %al
	je	.LBB0_166
# BB#165:                               #   in Loop: Header=BB0_161 Depth=4
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.20, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_166:                              # %.loopexit390
                                        #   in Loop: Header=BB0_161 Depth=4
	movq	8(%r13), %rbp
	addq	$8, %r13
	cmpq	%r12, %rbp
	jne	.LBB0_168
# BB#167:                               #   in Loop: Header=BB0_161 Depth=4
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.22, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r13), %rbp
	.p2align	4, 0x90
.LBB0_168:                              #   Parent Loop BB0_156 Depth=1
                                        #     Parent Loop BB0_159 Depth=2
                                        #       Parent Loop BB0_160 Depth=3
                                        #         Parent Loop BB0_161 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	addq	$16, %rbp
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_168
# BB#169:                               #   in Loop: Header=BB0_161 Depth=4
	cmpb	$1, %al
	jne	.LBB0_171
# BB#170:                               #   in Loop: Header=BB0_161 Depth=4
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.20, %r9d
	xorl	%eax, %eax
	callq	Error
	movb	32(%rbp), %al
.LBB0_171:                              # %.loopexit388
                                        #   in Loop: Header=BB0_161 Depth=4
	movl	%eax, %ecx
	addb	$-119, %cl
	cmpb	$19, %cl
	ja	.LBB0_173
# BB#172:                               #   in Loop: Header=BB0_161 Depth=4
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.21, %r9d
	xorl	%eax, %eax
	callq	Error
	movb	32(%rbp), %al
.LBB0_173:                              #   in Loop: Header=BB0_161 Depth=4
	addb	$-2, %al
	cmpb	$7, %al
	sbbl	%ecx, %ecx
	andl	$1, %ecx
	cmpb	$7, %al
	movl	%ecx, 52(%rbp)
	jb	.LBB0_161
# BB#174:                               #   in Loop: Header=BB0_160 Depth=3
	movl	$1, %eax
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	je	.LBB0_160
	jmp	.LBB0_157
.LBB0_175:
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	80(%rsp), %rbx          # 8-byte Reload
	cmpl	(%rcx), %ebx
	movq	%r14, %rbp
	jg	.LBB0_178
# BB#176:
	leal	(%rbx,%rbp), %eax
	cmpl	4(%rcx), %eax
	jg	.LBB0_178
# BB#177:
	cmpl	8(%rcx), %ebp
	jle	.LBB0_185
.LBB0_178:
	xorl	%ebp, %ebp
	movl	$13, %edi
	movl	$2, %esi
	movl	$.L.str.23, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
	movq	8(%r12), %rax
	cmpq	%r12, %rax
	je	.LBB0_184
.LBB0_180:                              # %.preheader386
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_181 Depth 2
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB0_181:                              #   Parent Loop BB0_180 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB0_181
# BB#182:                               #   in Loop: Header=BB0_180 Depth=1
	cmpb	$1, %dl
	jne	.LBB0_179
# BB#183:                               #   in Loop: Header=BB0_180 Depth=1
	movzwl	44(%rcx), %edx
	andl	$1023, %edx             # imm = 0x3FF
	orl	$9216, %edx             # imm = 0x2400
	movw	%dx, 44(%rcx)
	movw	$0, 46(%rcx)
.LBB0_179:                              # %.loopexit387
                                        #   in Loop: Header=BB0_180 Depth=1
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	jne	.LBB0_180
.LBB0_184:
	xorl	%ebx, %ebx
.LBB0_185:                              # %.preheader374
	movl	28(%rsp), %eax          # 4-byte Reload
	addl	24(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 96(%rsp)          # 4-byte Spill
	jle	.LBB0_243
# BB#186:                               # %.lr.ph.lr.ph.lr.ph
	movl	$0, %ecx
                                        # implicit-def: %RAX
	movq	%rax, 40(%rsp)          # 8-byte Spill
                                        # implicit-def: %ESI
.LBB0_187:                              # %.lr.ph.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_188 Depth 2
                                        #       Child Loop BB0_190 Depth 3
                                        #         Child Loop BB0_194 Depth 4
                                        #           Child Loop BB0_195 Depth 5
                                        #           Child Loop BB0_202 Depth 5
	movl	%ecx, %edi
	movq	%rbp, 88(%rsp)          # 8-byte Spill
.LBB0_188:                              # %.lr.ph
                                        #   Parent Loop BB0_187 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_190 Depth 3
                                        #         Child Loop BB0_194 Depth 4
                                        #           Child Loop BB0_195 Depth 5
                                        #           Child Loop BB0_202 Depth 5
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	(%rax), %ebx
	jg	.LBB0_243
# BB#189:                               #   in Loop: Header=BB0_188 Depth=2
	leal	(%rbx,%rbp), %eax
	movl	%eax, 120(%rsp)         # 4-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rbx, 80(%rsp)          # 8-byte Spill
.LBB0_190:                              # %.lr.ph734
                                        #   Parent Loop BB0_187 Depth=1
                                        #     Parent Loop BB0_188 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_194 Depth 4
                                        #           Child Loop BB0_195 Depth 5
                                        #           Child Loop BB0_202 Depth 5
	movl	120(%rsp), %edx         # 4-byte Reload
	cmpl	4(%rax), %edx
	jg	.LBB0_243
# BB#191:                               #   in Loop: Header=BB0_190 Depth=3
	cmpl	8(%rax), %ebp
	jg	.LBB0_243
# BB#192:                               #   in Loop: Header=BB0_190 Depth=3
	movl	%edi, 100(%rsp)         # 4-byte Spill
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	movl	56(%r12), %r15d
	addl	48(%r12), %r15d
	xorl	%r13d, %r13d
	movq	136(%rsp), %rbx         # 8-byte Reload
	jmp	.LBB0_194
	.p2align	4, 0x90
.LBB0_193:                              # %.loopexit372
                                        #   in Loop: Header=BB0_194 Depth=4
	testb	$1, 45(%rbp)
	movl	$1, %eax
	cmovnel	%eax, %esi
	addq	$8, %rbx
.LBB0_194:                              #   Parent Loop BB0_187 Depth=1
                                        #     Parent Loop BB0_188 Depth=2
                                        #       Parent Loop BB0_190 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_195 Depth 5
                                        #           Child Loop BB0_202 Depth 5
	movq	(%rbx), %r14
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB0_195:                              #   Parent Loop BB0_187 Depth=1
                                        #     Parent Loop BB0_188 Depth=2
                                        #       Parent Loop BB0_190 Depth=3
                                        #         Parent Loop BB0_194 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_195
# BB#196:                               #   in Loop: Header=BB0_194 Depth=4
	cmpb	$1, %al
	jne	.LBB0_198
# BB#197:                               #   in Loop: Header=BB0_194 Depth=4
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	%esi, %ebx
	movl	$2, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.24, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	%ebx, %esi
.LBB0_198:                              # %.loopexit373
                                        #   in Loop: Header=BB0_194 Depth=4
	cmpl	$0, 52(%rbp)
	jne	.LBB0_200
# BB#199:                               #   in Loop: Header=BB0_194 Depth=4
	movl	56(%rbp), %eax
	addl	48(%rbp), %eax
	cmpl	%r15d, %eax
	setl	%cl
	testq	%r13, %r13
	sete	%dl
	orb	%cl, %dl
	cmovneq	%rbp, %r13
	movl	$0, %ecx
	cmovnel	%ecx, %esi
	cmovnel	%eax, %r15d
	movq	40(%rsp), %rax          # 8-byte Reload
	cmovneq	%r14, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB0_200:                              #   in Loop: Header=BB0_194 Depth=4
	movq	8(%r14), %rbx
	cmpq	%r12, %rbx
	je	.LBB0_205
# BB#201:                               # %.preheader371.preheader
                                        #   in Loop: Header=BB0_194 Depth=4
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB0_202:                              # %.preheader371
                                        #   Parent Loop BB0_187 Depth=1
                                        #     Parent Loop BB0_188 Depth=2
                                        #       Parent Loop BB0_190 Depth=3
                                        #         Parent Loop BB0_194 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_202
# BB#203:                               # %.preheader371
                                        #   in Loop: Header=BB0_194 Depth=4
	cmpb	$1, %al
	je	.LBB0_193
# BB#204:                               #   in Loop: Header=BB0_194 Depth=4
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	%esi, %r14d
	movl	$2, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.25, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	%r14d, %esi
	jmp	.LBB0_193
.LBB0_205:                              #   in Loop: Header=BB0_190 Depth=3
	leaq	116(%rsp), %rax
	movq	%rax, (%rsp)
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	%esi, 104(%rsp)         # 4-byte Spill
	leaq	32(%rsp), %rdx
	leaq	48(%rsp), %rcx
	leaq	128(%rsp), %r8
	leaq	144(%rsp), %r9
	callq	SetNeighbours
	movq	32(%rsp), %rdx
	xorl	%edi, %edi
	testq	%rdx, %rdx
	movl	$0, %ebx
	movq	88(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_210
# BB#206:                               #   in Loop: Header=BB0_190 Depth=3
	movq	48(%rsp), %rax
	cmpl	$0, 52(%rax)
	je	.LBB0_208
# BB#207:                               #   in Loop: Header=BB0_190 Depth=3
	movl	56(%rax), %edi
	jmp	.LBB0_209
.LBB0_208:                              #   in Loop: Header=BB0_190 Depth=3
	xorl	%edi, %edi
.LBB0_209:                              #   in Loop: Header=BB0_190 Depth=3
	addq	$44, %rdx
	xorl	%esi, %esi
	movl	$151, %ecx
	callq	ExtraGap
	movl	%eax, %ebx
	xorl	%edi, %edi
.LBB0_210:                              #   in Loop: Header=BB0_190 Depth=3
	movq	128(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB0_215
# BB#211:                               #   in Loop: Header=BB0_190 Depth=3
	movq	144(%rsp), %rax
	cmpl	$0, 52(%rax)
	je	.LBB0_213
# BB#212:                               #   in Loop: Header=BB0_190 Depth=3
	movl	48(%rax), %esi
	jmp	.LBB0_214
.LBB0_213:                              #   in Loop: Header=BB0_190 Depth=3
	xorl	%esi, %esi
.LBB0_214:                              #   in Loop: Header=BB0_190 Depth=3
	addq	$44, %rdx
	xorl	%edi, %edi
	movl	$153, %ecx
	callq	ExtraGap
	movl	%eax, %edi
.LBB0_215:                              #   in Loop: Header=BB0_190 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %eax
	subl	120(%rsp), %eax         # 4-byte Folded Reload
	cltd
	idivl	96(%rsp)                # 4-byte Folded Reload
	movl	116(%rsp), %ecx
	cmpl	$153, %ecx
	je	.LBB0_219
# BB#216:                               #   in Loop: Header=BB0_190 Depth=3
	cmpl	$152, %ecx
	je	.LBB0_221
# BB#217:                               #   in Loop: Header=BB0_190 Depth=3
	cmpl	$151, %ecx
	jne	.LBB0_222
# BB#218:                               #   in Loop: Header=BB0_190 Depth=3
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ecx
	movl	28(%rsp), %esi          # 4-byte Reload
	imull	%esi, %eax
	movq	80(%rsp), %rdx          # 8-byte Reload
	addl	%edx, %eax
	cmpl	%eax, %ecx
	cmovlel	%ecx, %eax
	subl	%edx, %eax
	jmp	.LBB0_220
.LBB0_219:                              #   in Loop: Header=BB0_190 Depth=3
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	8(%rcx), %ecx
	movl	24(%rsp), %esi          # 4-byte Reload
	imull	%esi, %eax
	addl	%ebp, %eax
	cmpl	%eax, %ecx
	cmovlel	%ecx, %eax
	subl	%ebp, %eax
.LBB0_220:                              #   in Loop: Header=BB0_190 Depth=3
	cltd
	idivl	%esi
	movl	%eax, %ecx
	movl	100(%rsp), %edx         # 4-byte Reload
	subl	%edx, %ecx
	cmpl	$20, %ecx
	movl	%eax, %ecx
	cmovll	%edx, %ecx
	cmpl	%edx, %eax
	cmovlel	%eax, %ecx
	addl	%ecx, %ebx
	cmpl	$8388608, %ebx          # imm = 0x800000
	movl	$8388607, %eax          # imm = 0x7FFFFF
	cmovll	%ebx, %eax
	movl	%eax, 56(%rsp)
	addl	%edi, %ebx
	cmpl	$8388608, %ebx          # imm = 0x800000
	movl	$8388607, %eax          # imm = 0x7FFFFF
	cmovgel	%eax, %ebx
	movl	%ebx, 60(%rsp)
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	addl	%ecx, %edi
	cmpl	$8388608, %edi          # imm = 0x800000
	cmovgel	%eax, %edi
	movl	%edi, 64(%rsp)
	jmp	.LBB0_223
.LBB0_221:                              #   in Loop: Header=BB0_190 Depth=3
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	8(%rcx), %ecx
	movl	24(%rsp), %esi          # 4-byte Reload
	imull	%esi, %eax
	addl	%ebp, %eax
	cmpl	%eax, %ecx
	cmovlel	%ecx, %eax
	subl	%ebp, %eax
	cltd
	idivl	%esi
	movl	%eax, %ecx
	movl	100(%rsp), %edx         # 4-byte Reload
	subl	%edx, %ecx
	cmpl	$20, %ecx
	movl	%eax, %esi
	cmovll	%edx, %esi
	cmpl	%edx, %eax
	cmovlel	%eax, %esi
	movl	48(%r13), %eax
	leal	(%rax,%rbx), %ecx
	cmpl	$8388608, %ecx          # imm = 0x800000
	movl	$8388607, %edx          # imm = 0x7FFFFF
	cmovgel	%edx, %ecx
	movl	%ecx, 56(%rsp)
	addl	%edi, %ebx
	addl	%eax, %ebx
	addl	%esi, %ebx
	cmpl	$8388608, %ebx          # imm = 0x800000
	cmovgel	%edx, %ebx
	movl	%ebx, 60(%rsp)
	movl	%esi, 72(%rsp)          # 4-byte Spill
	addl	%esi, %edi
	cmpl	$8388608, %edi          # imm = 0x800000
	cmovgel	%edx, %edi
	movl	%edi, 64(%rsp)
	jmp	.LBB0_223
.LBB0_222:                              #   in Loop: Header=BB0_190 Depth=3
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_223:                              #   in Loop: Header=BB0_190 Depth=3
	movq	%r13, %rdi
	leaq	56(%rsp), %rsi
	callq	BreakObject
	movq	%rax, %rbp
	movl	$1, 52(%rbp)
	movq	32(%rsp), %rcx
	testq	%rcx, %rcx
	je	.LBB0_226
# BB#224:                               #   in Loop: Header=BB0_190 Depth=3
	movq	48(%rsp), %rax
	cmpl	$0, 52(%rax)
	movq	80(%rsp), %rbx          # 8-byte Reload
	je	.LBB0_227
# BB#225:                               #   in Loop: Header=BB0_190 Depth=3
	movl	56(%rax), %r15d
	jmp	.LBB0_228
.LBB0_226:                              #   in Loop: Header=BB0_190 Depth=3
	movl	48(%rbp), %r13d
	movq	80(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB0_229
.LBB0_227:                              #   in Loop: Header=BB0_190 Depth=3
	xorl	%r15d, %r15d
.LBB0_228:                              #   in Loop: Header=BB0_190 Depth=3
	movl	48(%rbp), %esi
	movl	56(%rbp), %edx
	addq	$44, %rcx
	movl	%r15d, %edi
	callq	MinGap
	movl	%eax, %r13d
	movq	32(%rsp), %rcx
	addq	$44, %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r15d, %edi
	callq	MinGap
	subl	%eax, %r13d
.LBB0_229:                              #   in Loop: Header=BB0_190 Depth=3
	movl	104(%rsp), %esi         # 4-byte Reload
	movq	128(%rsp), %rcx
	testq	%rcx, %rcx
	je	.LBB0_232
# BB#230:                               #   in Loop: Header=BB0_190 Depth=3
	movq	144(%rsp), %rax
	cmpl	$0, 52(%rax)
	je	.LBB0_233
# BB#231:                               #   in Loop: Header=BB0_190 Depth=3
	movl	48(%rax), %r14d
	movl	56(%rax), %r15d
	jmp	.LBB0_234
.LBB0_232:                              #   in Loop: Header=BB0_190 Depth=3
	movl	56(%rbp), %ebp
	jmp	.LBB0_235
.LBB0_233:                              #   in Loop: Header=BB0_190 Depth=3
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
.LBB0_234:                              # %.thread598
                                        #   in Loop: Header=BB0_190 Depth=3
	movl	56(%rbp), %edi
	addq	$44, %rcx
	movl	%r14d, %esi
	movl	%r15d, %edx
	callq	MinGap
	movl	%eax, %ebp
	movq	128(%rsp), %rcx
	addq	$44, %rcx
	xorl	%edi, %edi
	movl	%r14d, %esi
	movl	%r15d, %edx
	callq	MinGap
	subl	%eax, %ebp
	movl	104(%rsp), %esi         # 4-byte Reload
.LBB0_235:                              #   in Loop: Header=BB0_190 Depth=3
	movl	116(%rsp), %eax
	cmpl	$153, %eax
	je	.LBB0_240
# BB#236:                               #   in Loop: Header=BB0_190 Depth=3
	cmpl	$152, %eax
	je	.LBB0_241
# BB#237:                               #   in Loop: Header=BB0_190 Depth=3
	cmpl	$151, %eax
	je	.LBB0_239
# BB#238:                               #   in Loop: Header=BB0_190 Depth=3
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	104(%rsp), %esi         # 4-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	(%rax), %ebx
	movl	72(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %edi
	movq	88(%rsp), %rbp          # 8-byte Reload
	jle	.LBB0_190
	jmp	.LBB0_243
.LBB0_239:                              # %.outer378
                                        #   in Loop: Header=BB0_188 Depth=2
	addl	%r13d, %ebx
	addl	%ebp, %ebx
	movl	28(%rsp), %eax          # 4-byte Reload
	decl	%eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	addl	24(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 96(%rsp)          # 4-byte Spill
	movl	72(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %edi
	movq	88(%rsp), %rbp          # 8-byte Reload
	jg	.LBB0_188
	jmp	.LBB0_243
.LBB0_240:                              #   in Loop: Header=BB0_187 Depth=1
	addl	88(%rsp), %r13d         # 4-byte Folded Reload
	addl	%ebp, %r13d
	movl	%r13d, %ebp
	jmp	.LBB0_242
.LBB0_241:                              #   in Loop: Header=BB0_187 Depth=1
	addl	%ebx, %r13d
	addl	88(%rsp), %ebp          # 4-byte Folded Reload
                                        # kill: %EBP<def> %EBP<kill> %RBP<def>
	movl	%r13d, %ebx
.LBB0_242:                              # %.outer375.backedge
                                        #   in Loop: Header=BB0_187 Depth=1
	movl	24(%rsp), %eax          # 4-byte Reload
	decl	%eax
	movl	28(%rsp), %ecx          # 4-byte Reload
	movl	%eax, 24(%rsp)          # 4-byte Spill
	addl	%eax, %ecx
	movl	%ecx, 96(%rsp)          # 4-byte Spill
	movl	72(%rsp), %ecx          # 4-byte Reload
	jg	.LBB0_187
.LBB0_243:                              # %BreakTable.exit
	movl	%ebx, 48(%r12)
	movl	%ebp, 56(%r12)
	cmpl	$0, 48(%r12)
	jns	.LBB0_95
	jmp	.LBB0_94
.Lfunc_end0:
	.size	BreakObject, .Lfunc_end0-BreakObject
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_38
	.quad	.LBB0_40
	.quad	.LBB0_25
	.quad	.LBB0_25
	.quad	.LBB0_6
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_41
	.quad	.LBB0_42
	.quad	.LBB0_51
	.quad	.LBB0_58
	.quad	.LBB0_31
	.quad	.LBB0_93
	.quad	.LBB0_31
	.quad	.LBB0_93
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_79
	.quad	.LBB0_6
	.quad	.LBB0_82
	.quad	.LBB0_6
	.quad	.LBB0_40
	.quad	.LBB0_6
	.quad	.LBB0_40
	.quad	.LBB0_6
	.quad	.LBB0_85
	.quad	.LBB0_88
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_18
	.quad	.LBB0_18
	.quad	.LBB0_18
	.quad	.LBB0_18
	.quad	.LBB0_18
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_99
	.quad	.LBB0_105
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_40
	.quad	.LBB0_33
	.quad	.LBB0_33
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_22

	.text
	.p2align	4, 0x90
	.type	BreakJoinedGroup,@function
BreakJoinedGroup:                       # @BreakJoinedGroup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 96
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movq	8(%r13), %rdi
	xorl	%r12d, %r12d
	cmpq	%rbp, %rdi
	je	.LBB1_1
# BB#2:                                 # %.preheader107.preheader
	xorl	%ecx, %ecx
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader107
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
	movq	%rsi, %rbx
	.p2align	4, 0x90
.LBB1_4:                                #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_4
# BB#5:                                 #   in Loop: Header=BB1_3 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	ja	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_3 Depth=1
	movl	48(%rbx), %eax
	cmpl	%eax, %ecx
	cmovll	%eax, %ecx
.LBB1_7:                                #   in Loop: Header=BB1_3 Depth=1
	movq	8(%rsi), %rsi
	cmpq	%rdi, %rsi
	jne	.LBB1_3
	jmp	.LBB1_8
.LBB1_1:
                                        # implicit-def: %RBX
	xorl	%ecx, %ecx
.LBB1_8:                                # %._crit_edge118
	movq	16(%rsp), %r8           # 8-byte Reload
	movl	(%r8), %eax
	movl	4(%r8), %esi
	cmpl	%eax, %ecx
	cmovgl	%r12d, %ecx
	cmpl	%esi, %eax
	cmovgl	%esi, %eax
	movl	%eax, (%rsp)
	movl	%esi, 4(%rsp)
	movl	8(%r8), %eax
	subl	%ecx, %esi
	cmpl	%esi, %eax
	cmovlel	%eax, %esi
	movl	%esi, 8(%rsp)
	testq	%rdx, %rdx
	je	.LBB1_9
# BB#10:
	movq	%rsp, %rsi
	movq	%rdx, %rdi
	callq	BreakObject
	movq	%rax, %r14
	movl	48(%r14), %r12d
	movl	56(%r14), %r15d
	movl	(%rsp), %eax
	movl	4(%rsp), %ecx
	movl	%ecx, %edx
	subl	%r15d, %edx
	cmpl	%edx, %eax
	cmovlel	%eax, %edx
	movl	%edx, (%rsp)
	movl	8(%rsp), %eax
	subl	%r12d, %ecx
	cmpl	%ecx, %eax
	cmovlel	%eax, %ecx
	movl	%ecx, 8(%rsp)
	movq	8(%r13), %rdi
	cmpq	%rbp, %rdi
	jne	.LBB1_12
	jmp	.LBB1_18
.LBB1_9:
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	cmpq	%rbp, %rdi
	je	.LBB1_18
	.p2align	4, 0x90
.LBB1_12:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_13 Depth 2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB1_13:                               #   Parent Loop BB1_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %ecx
	testb	%cl, %cl
	je	.LBB1_13
# BB#14:                                #   in Loop: Header=BB1_12 Depth=1
	cmpq	%r14, %rbx
	je	.LBB1_17
# BB#15:                                #   in Loop: Header=BB1_12 Depth=1
	addb	$-9, %cl
	cmpb	$90, %cl
	ja	.LBB1_17
# BB#16:                                #   in Loop: Header=BB1_12 Depth=1
	movq	%rbx, %rdi
	movq	%rsp, %rsi
	callq	BreakObject
	movq	%rax, %rbx
	movl	48(%rbx), %eax
	movl	56(%rbx), %ecx
	cmpl	%eax, %r12d
	cmovll	%eax, %r12d
	cmpl	%ecx, %r15d
	cmovll	%ecx, %r15d
	movl	(%rsp), %eax
	movl	4(%rsp), %ecx
	movl	%ecx, %edx
	subl	%r15d, %edx
	cmpl	%edx, %eax
	cmovlel	%eax, %edx
	movl	%edx, (%rsp)
	movl	8(%rsp), %eax
	subl	%r12d, %ecx
	cmpl	%ecx, %eax
	cmovlel	%eax, %ecx
	movl	%ecx, 8(%rsp)
	movq	8(%r13), %rdi
.LBB1_17:                               #   in Loop: Header=BB1_12 Depth=1
	movq	8(%rbp), %rbp
	cmpq	%rdi, %rbp
	jne	.LBB1_12
.LBB1_18:                               # %._crit_edge
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx), %r12d
	jg	.LBB1_21
# BB#19:
	leal	(%r15,%r12), %eax
	cmpl	4(%rcx), %eax
	jg	.LBB1_21
# BB#20:
	cmpl	8(%rcx), %r15d
	jle	.LBB1_25
.LBB1_21:
	testq	%r14, %r14
	je	.LBB1_23
# BB#22:
	addq	$32, %r14
	jmp	.LBB1_24
.LBB1_23:
	testq	%rbx, %rbx
	leaq	32(%rbx), %r14
	cmoveq	no_fpos(%rip), %r14
.LBB1_24:
	movl	$13, %edi
	movl	$1, %esi
	movl	$.L.str.27, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
.LBB1_25:
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%r12d, (%rax)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%r15d, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	BreakJoinedGroup, .Lfunc_end1-BreakJoinedGroup
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"replacing with empty object: negative size constraint %s,%s,%s"
	.size	.L.str, 63

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.zero	1
	.size	.L.str.1, 1

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s object scaled horizontally by factor %.2f (too wide)"
	.size	.L.str.2, 56

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"@Rotate"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%s deleted (too wide; cannot break %s)"
	.size	.L.str.4, 39

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"word %s scaled horizontally by factor %.2f (too wide)"
	.size	.L.str.6, 54

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"word %s deleted (too wide)"
	.size	.L.str.7, 27

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%s scaled horizontally by factor %.2f (too wide)"
	.size	.L.str.8, 49

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"@IncludeGraphic"
	.size	.L.str.9, 16

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"@SysIncludeGraphic"
	.size	.L.str.10, 19

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%s deleted (too wide)"
	.size	.L.str.11, 22

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"assert failed in %s"
	.size	.L.str.12, 20

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"BreakObject: downs!"
	.size	.L.str.13, 20

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"column mark of unbroken paragraph moved left"
	.size	.L.str.14, 45

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"column mark of paragraph moved left before breaking"
	.size	.L.str.15, 52

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"assert failed in %s %s"
	.size	.L.str.16, 23

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"BreakObject:"
	.size	.L.str.17, 13

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"BreakObject: back(x, COLM) < 0!"
	.size	.L.str.18, 32

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"BreakObject: fwd(x, COLM) < 0!"
	.size	.L.str.19, 31

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"BreakTable: GAP_OBJ!"
	.size	.L.str.20, 21

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"BreakTable: index!"
	.size	.L.str.21, 19

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"BreakTable: GAP_OBJ is last!"
	.size	.L.str.22, 29

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"reducing column gaps to 0i (object is too wide)"
	.size	.L.str.23, 48

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"BreakTable: type(y) == GAP_OBJ!"
	.size	.L.str.24, 32

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"BreakTable: type(g) != GAP_OBJ!"
	.size	.L.str.25, 32

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"BreakTable: mside"
	.size	.L.str.26, 18

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"failed to break column to fit into its available space"
	.size	.L.str.27, 55

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"BreakVcat: Down(x) == x!"
	.size	.L.str.28, 25

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"BreakVcat: start_group == nilobj!"
	.size	.L.str.29, 34

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"BreakVcat: start_group == nilobj (2)!"
	.size	.L.str.30, 38


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
