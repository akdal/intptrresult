	.text
	.file	"gxcolor.bc"
	.globl	gx_color_from_rgb
	.p2align	4, 0x90
	.type	gx_color_from_rgb,@function
gx_color_from_rgb:                      # @gx_color_from_rgb
	.cfi_startproc
# BB#0:
	movzwl	(%rdi), %eax
	cmpw	2(%rdi), %ax
	jne	.LBB0_4
# BB#1:
	cmpw	4(%rdi), %ax
	jne	.LBB0_4
# BB#2:
	movw	%ax, 6(%rdi)
	movb	$1, %al
	jmp	.LBB0_5
.LBB0_4:
	xorl	%eax, %eax
.LBB0_5:
	movb	%al, 9(%rdi)
	movb	%al, 8(%rdi)
	retq
.Lfunc_end0:
	.size	gx_color_from_rgb, .Lfunc_end0-gx_color_from_rgb
	.cfi_endproc

	.globl	gx_color_luminance
	.p2align	4, 0x90
	.type	gx_color_luminance,@function
gx_color_luminance:                     # @gx_color_luminance
	.cfi_startproc
# BB#0:
	cmpb	$0, 9(%rdi)
	je	.LBB1_2
# BB#1:                                 # %._crit_edge
	movw	6(%rdi), %ax
                                        # kill: %AX<def> %AX<kill> %RAX<kill>
	retq
.LBB1_2:
	movzwl	(%rdi), %eax
	imulq	$30, %rax, %rax
	movzwl	2(%rdi), %ecx
	imulq	$59, %rcx, %rcx
	movzwl	4(%rdi), %edx
	imulq	$11, %rdx, %rdx
	addq	%rax, %rcx
	leaq	50(%rdx,%rcx), %rax
	shrq	$2, %rax
	movabsq	$2951479051793528259, %rcx # imm = 0x28F5C28F5C28F5C3
	mulq	%rcx
	movq	%rdx, %rax
	shrq	$2, %rax
	movw	%ax, 6(%rdi)
	movb	$1, 9(%rdi)
                                        # kill: %AX<def> %AX<kill> %RAX<kill>
	retq
.Lfunc_end1:
	.size	gx_color_luminance, .Lfunc_end1-gx_color_luminance
	.cfi_endproc

	.globl	gx_color_to_hsb
	.p2align	4, 0x90
	.type	gx_color_to_hsb,@function
gx_color_to_hsb:                        # @gx_color_to_hsb
	.cfi_startproc
# BB#0:
	cmpb	$0, 8(%rdi)
	je	.LBB2_2
# BB#1:
	movl	$0, (%rsi)
	movzwl	(%rdi), %r10d
	movw	%r10w, 4(%rsi)
	retq
.LBB2_2:
	movzwl	(%rdi), %eax
	movzwl	2(%rdi), %ecx
	movzwl	4(%rdi), %edx
	cmpw	%cx, %ax
	movl	%ecx, %r10d
	cmovaw	%ax, %r10w
	movl	%eax, %edi
	cmovaw	%cx, %di
	cmpw	%r10w, %dx
	cmovaw	%dx, %r10w
	cmpw	%di, %dx
	cmovbw	%dx, %di
	movzwl	%r10w, %r8d
	movzwl	%di, %edi
	movq	%r8, %r9
	subq	%rdi, %r9
	cmpw	%ax, %r8w
	jne	.LBB2_4
# BB#3:
	subl	%edx, %ecx
	movslq	%ecx, %rcx
	movq	%rcx, %rax
	shlq	$16, %rax
	subq	%rcx, %rax
	cqto
	idivq	%r9
	jmp	.LBB2_7
.LBB2_4:
	cmpw	%cx, %r10w
	jne	.LBB2_6
# BB#5:
	subl	%eax, %edx
	movslq	%edx, %rcx
	movq	%rcx, %rax
	shlq	$16, %rax
	subq	%rcx, %rax
	cqto
	idivq	%r9
	addq	$131070, %rax           # imm = 0x1FFFE
	jmp	.LBB2_7
.LBB2_6:
	subl	%ecx, %eax
	movslq	%eax, %rcx
	movq	%rcx, %rax
	shlq	$16, %rax
	subq	%rcx, %rax
	cqto
	idivq	%r9
	addq	$262140, %rax           # imm = 0x3FFFC
.LBB2_7:
	leaq	393210(%rax), %rcx
	testq	%rax, %rax
	cmovnsq	%rax, %rcx
	movabsq	$3074457345618258603, %rdx # imm = 0x2AAAAAAAAAAAAAAB
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addl	%edx, %eax
	movw	%ax, (%rsi)
	movq	%r9, %rax
	shlq	$16, %rax
	subq	%r9, %rax
	cqto
	idivq	%r8
	movw	%ax, 2(%rsi)
	movw	%r10w, 4(%rsi)
	retq
.Lfunc_end2:
	.size	gx_color_to_hsb, .Lfunc_end2-gx_color_to_hsb
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	1199570688              # float 65535
.LCPI3_1:
	.long	1177201664              # float 10923
.LCPI3_2:
	.long	1065353216              # float 1
	.text
	.globl	gx_color_from_hsb
	.p2align	4, 0x90
	.type	gx_color_from_hsb,@function
gx_color_from_hsb:                      # @gx_color_from_hsb
	.cfi_startproc
# BB#0:
	testw	%dx, %dx
	je	.LBB3_1
# BB#2:
	movzwl	%cx, %eax
	cvtsi2ssl	%eax, %xmm2
	movss	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movzwl	%dx, %eax
	cvtsi2ssl	%eax, %xmm3
	divss	%xmm0, %xmm3
	movzwl	%si, %eax
	imull	$49151, %eax, %eax      # imm = 0xBFFF
	shrl	$29, %eax
	imull	$10923, %eax, %ecx      # imm = 0x2AAB
	subl	%ecx, %esi
	movswl	%si, %ecx
	cvtsi2ssl	%ecx, %xmm5
	divss	.LCPI3_1(%rip), %xmm5
	movss	.LCPI3_2(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	subss	%xmm3, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm4, %xmm7
	subss	%xmm5, %xmm7
	mulss	%xmm3, %xmm7
	movaps	%xmm4, %xmm6
	subss	%xmm7, %xmm6
	mulss	%xmm2, %xmm6
	decb	%al
	cmpb	$4, %al
	ja	.LBB3_3
# BB#4:
	mulss	%xmm5, %xmm3
	subss	%xmm3, %xmm4
	mulss	%xmm2, %xmm4
	movzbl	%al, %eax
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_5:
	movaps	%xmm4, %xmm3
	movaps	%xmm2, %xmm5
	jmp	.LBB3_10
.LBB3_1:                                # %.thread64
	movw	%cx, 4(%rdi)
	movw	%cx, 2(%rdi)
	movw	%cx, (%rdi)
	jmp	.LBB3_12
.LBB3_3:
	movaps	%xmm2, %xmm3
	movaps	%xmm6, %xmm5
	jmp	.LBB3_10
.LBB3_6:
	movaps	%xmm1, %xmm3
	movaps	%xmm2, %xmm5
	movaps	%xmm6, %xmm1
	jmp	.LBB3_10
.LBB3_7:
	movaps	%xmm1, %xmm3
	movaps	%xmm4, %xmm5
	movaps	%xmm2, %xmm1
	jmp	.LBB3_10
.LBB3_8:
	movaps	%xmm6, %xmm3
	movaps	%xmm1, %xmm5
	movaps	%xmm2, %xmm1
	jmp	.LBB3_10
.LBB3_9:
	movaps	%xmm2, %xmm3
	movaps	%xmm1, %xmm5
	movaps	%xmm4, %xmm1
.LBB3_10:
	mulss	%xmm0, %xmm3
	cvttss2si	%xmm3, %esi
	movw	%si, (%rdi)
	mulss	%xmm0, %xmm5
	cvttss2si	%xmm5, %edx
	movw	%dx, 2(%rdi)
	mulss	%xmm0, %xmm1
	cvttss2si	%xmm1, %ecx
	movw	%cx, 4(%rdi)
	xorl	%eax, %eax
	cmpw	%dx, %si
	jne	.LBB3_13
# BB#11:
	cmpw	%cx, %dx
	jne	.LBB3_13
.LBB3_12:
	movw	%cx, 6(%rdi)
	movb	$1, %al
.LBB3_13:                               # %gx_color_from_rgb.exit
	movb	%al, 9(%rdi)
	movb	%al, 8(%rdi)
	retq
.Lfunc_end3:
	.size	gx_color_from_hsb, .Lfunc_end3-gx_color_from_hsb
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_5
	.quad	.LBB3_6
	.quad	.LBB3_7
	.quad	.LBB3_8
	.quad	.LBB3_9

	.text
	.globl	gx_sort_ht_order
	.p2align	4, 0x90
	.type	gx_sort_ht_order,@function
gx_sort_ht_order:                       # @gx_sort_ht_order
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	cmpl	$2, %esi
	jb	.LBB4_12
# BB#1:                                 # %.preheader
	leal	-1(%rsi), %r10d
	shrl	%esi
	testl	%esi, %esi
	jne	.LBB4_3
	jmp	.LBB4_10
	.p2align	4, 0x90
.LBB4_9:                                # %.critedge
	movl	%ecx, %eax
	movw	%r8w, (%rdi,%rax,4)
	movw	%r9w, 2(%rdi,%rax,4)
	testl	%esi, %esi
	je	.LBB4_10
.LBB4_3:
	decl	%esi
	movzwl	(%rdi,%rsi,4), %r8d
	movzwl	2(%rdi,%rsi,4), %r9d
	jmp	.LBB4_4
	.p2align	4, 0x90
.LBB4_10:
	movl	%r10d, %eax
	movzwl	(%rdi,%rax,4), %r8d
	movzwl	2(%rdi,%rax,4), %r9d
	movl	(%rdi), %ecx
	movl	%ecx, (%rdi,%rax,4)
	xorl	%esi, %esi
	decl	%r10d
	je	.LBB4_11
.LBB4_4:
	movl	%esi, %ecx
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_13:                               #   in Loop: Header=BB4_5 Depth=1
	movl	%ecx, %ecx
	movl	(%rdi,%rax,4), %eax
	movl	%eax, (%rdi,%rcx,4)
	movl	%edx, %ecx
.LBB4_5:                                # =>This Inner Loop Header: Depth=1
	leal	1(%rcx,%rcx), %edx
	cmpl	%r10d, %edx
	jae	.LBB4_7
# BB#6:                                 #   in Loop: Header=BB4_5 Depth=1
	movl	%edx, %eax
	movzwl	2(%rdi,%rax,4), %r11d
	leal	1(%rdx), %eax
	cmpw	2(%rdi,%rax,4), %r11w
	cmovbl	%eax, %edx
.LBB4_7:                                #   in Loop: Header=BB4_5 Depth=1
	cmpl	%r10d, %edx
	ja	.LBB4_9
# BB#8:                                 #   in Loop: Header=BB4_5 Depth=1
	movl	%edx, %eax
	cmpw	2(%rdi,%rax,4), %r9w
	jb	.LBB4_13
	jmp	.LBB4_9
.LBB4_11:
	movw	%r8w, (%rdi)
	movw	%r9w, 2(%rdi)
.LBB4_12:
	retq
.Lfunc_end4:
	.size	gx_sort_ht_order, .Lfunc_end4-gx_sort_ht_order
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
