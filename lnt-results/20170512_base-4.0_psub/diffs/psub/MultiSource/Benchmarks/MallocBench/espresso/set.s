	.text
	.file	"set.bc"
	.globl	bit_index
	.p2align	4, 0x90
	.type	bit_index,@function
bit_index:                              # @bit_index
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	je	.LBB0_1
# BB#2:                                 # %.preheader
	xorl	%eax, %eax
	testb	$1, %dil
	jne	.LBB0_5
# BB#3:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%edi, %ecx
	shrl	%ecx
	incl	%eax
	btl	$1, %edi
	movl	%ecx, %edi
	jae	.LBB0_4
.LBB0_5:                                # %.loopexit
	retq
.LBB0_1:
	movl	$-1, %eax
	retq
.Lfunc_end0:
	.size	bit_index, .Lfunc_end0-bit_index
	.cfi_endproc

	.globl	set_ord
	.p2align	4, 0x90
	.type	set_ord,@function
set_ord:                                # @set_ord
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	(%rdi), %ecx
	testw	$1023, %cx              # imm = 0x3FF
	je	.LBB1_1
# BB#2:                                 # %.lr.ph.preheader
	andl	$1023, %ecx             # imm = 0x3FF
	incq	%rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdi,%rcx,4), %edx
	testl	%edx, %edx
	je	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	movzbl	%dl, %r8d
	movzbl	%dh, %ebx  # NOREX
	movl	%edx, %esi
	shrl	$16, %esi
	movzbl	%sil, %esi
	shrl	$24, %edx
	addl	bit_count(,%r8,4), %eax
	addl	bit_count(,%rbx,4), %eax
	addl	bit_count(,%rsi,4), %eax
	addl	bit_count(,%rdx,4), %eax
.LBB1_5:                                #   in Loop: Header=BB1_3 Depth=1
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB1_3
# BB#6:                                 # %._crit_edge
	popq	%rbx
	retq
.LBB1_1:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	set_ord, .Lfunc_end1-set_ord
	.cfi_endproc

	.globl	set_dist
	.p2align	4, 0x90
	.type	set_dist,@function
set_dist:                               # @set_dist
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
.Lcfi4:
	.cfi_offset %rbx, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	(%rdi), %ecx
	testw	$1023, %cx              # imm = 0x3FF
	je	.LBB2_1
# BB#2:                                 # %.lr.ph.preheader
	andl	$1023, %ecx             # imm = 0x3FF
	incq	%rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rsi,%rcx,4), %edx
	andl	-4(%rdi,%rcx,4), %edx
	je	.LBB2_5
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=1
	movzbl	%dl, %r8d
	movzbl	%dh, %ebp  # NOREX
	movl	%edx, %ebx
	shrl	$16, %ebx
	movzbl	%bl, %ebx
	shrl	$24, %edx
	addl	bit_count(,%r8,4), %eax
	addl	bit_count(,%rbp,4), %eax
	addl	bit_count(,%rbx,4), %eax
	addl	bit_count(,%rdx,4), %eax
.LBB2_5:                                #   in Loop: Header=BB2_3 Depth=1
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB2_3
	jmp	.LBB2_6
.LBB2_1:
	xorl	%eax, %eax
.LBB2_6:                                # %._crit_edge
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end2:
	.size	set_dist, .Lfunc_end2-set_dist
	.cfi_endproc

	.globl	set_clear
	.p2align	4, 0x90
	.type	set_clear,@function
set_clear:                              # @set_clear
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %eax
	cmpl	$33, %esi
	jl	.LBB3_2
# BB#1:
	decl	%esi
	sarl	$5, %esi
	incl	%esi
	movl	%esi, %eax
.LBB3_2:
	movl	%eax, (%rbx)
	movslq	%eax, %rcx
	movl	%ecx, %edx
	notl	%edx
	cmpl	$-3, %edx
	movl	$-2, %esi
	cmovgl	%edx, %esi
	leal	1(%rax,%rsi), %eax
	subq	%rax, %rcx
	leaq	(%rbx,%rcx,4), %rdi
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	set_clear, .Lfunc_end3-set_clear
	.cfi_endproc

	.globl	set_fill
	.p2align	4, 0x90
	.type	set_fill,@function
set_fill:                               # @set_fill
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %rbx
	movl	$1, %eax
	cmpl	$33, %esi
	jl	.LBB4_2
# BB#1:
	leal	-1(%rsi), %eax
	sarl	$5, %eax
	incl	%eax
.LBB4_2:
	movl	%eax, (%rbx)
	cltq
	movl	%eax, %ecx
	shll	$5, %ecx
	subl	%esi, %ecx
	movl	$-1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	movl	%edx, (%rbx,%rax,4)
	cmpl	$2, %eax
	jl	.LBB4_4
# BB#3:                                 # %.lr.ph.preheader
	leaq	-1(%rax), %rcx
	leal	-2(%rax), %eax
	subq	%rax, %rcx
	leaq	(%rbx,%rcx,4), %rdi
	leaq	4(,%rax,4), %rdx
	movl	$255, %esi
	callq	memset
.LBB4_4:                                # %._crit_edge
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	set_fill, .Lfunc_end4-set_fill
	.cfi_endproc

	.globl	set_copy
	.p2align	4, 0x90
	.type	set_copy,@function
set_copy:                               # @set_copy
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	andl	$1023, %eax             # imm = 0x3FF
	leaq	1(%rax), %r10
	cmpq	$8, %r10
	jb	.LBB5_14
# BB#1:                                 # %min.iters.checked
	movq	%r10, %r8
	andq	$2040, %r8              # imm = 0x7F8
	je	.LBB5_14
# BB#2:                                 # %vector.memcheck
	leaq	4(%rsi,%rax,4), %rcx
	cmpq	%rdi, %rcx
	jbe	.LBB5_4
# BB#3:                                 # %vector.memcheck
	leaq	4(%rdi,%rax,4), %rcx
	cmpq	%rsi, %rcx
	ja	.LBB5_14
.LBB5_4:                                # %vector.body.preheader
	leaq	-8(%r8), %r9
	movl	%r9d, %ecx
	shrl	$3, %ecx
	incl	%ecx
	andq	$3, %rcx
	je	.LBB5_5
# BB#6:                                 # %vector.body.prol.preheader
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	leaq	-12(%rdi,%rax,4), %r11
	leaq	-12(%rsi,%rax,4), %rbx
	negq	%rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_7:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbx,%rdx,4), %xmm0
	movups	-16(%rbx,%rdx,4), %xmm1
	movups	%xmm0, (%r11,%rdx,4)
	movups	%xmm1, -16(%r11,%rdx,4)
	addq	$-8, %rdx
	incq	%rcx
	jne	.LBB5_7
# BB#8:                                 # %vector.body.prol.loopexit.unr-lcssa
	negq	%rdx
	popq	%rbx
	cmpq	$24, %r9
	jae	.LBB5_10
	jmp	.LBB5_12
.LBB5_5:
	xorl	%edx, %edx
	cmpq	$24, %r9
	jb	.LBB5_12
.LBB5_10:                               # %vector.body.preheader.new
	movq	%r10, %r9
	andq	$-8, %r9
	negq	%r9
	negq	%rdx
	leaq	-12(%rdi,%rax,4), %r11
	leaq	-12(%rsi,%rax,4), %rcx
	.p2align	4, 0x90
.LBB5_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rdx,4), %xmm0
	movups	-16(%rcx,%rdx,4), %xmm1
	movups	%xmm0, (%r11,%rdx,4)
	movups	%xmm1, -16(%r11,%rdx,4)
	movups	-32(%rcx,%rdx,4), %xmm0
	movups	-48(%rcx,%rdx,4), %xmm1
	movups	%xmm0, -32(%r11,%rdx,4)
	movups	%xmm1, -48(%r11,%rdx,4)
	movups	-64(%rcx,%rdx,4), %xmm0
	movups	-80(%rcx,%rdx,4), %xmm1
	movups	%xmm0, -64(%r11,%rdx,4)
	movups	%xmm1, -80(%r11,%rdx,4)
	movups	-96(%rcx,%rdx,4), %xmm0
	movups	-112(%rcx,%rdx,4), %xmm1
	movups	%xmm0, -96(%r11,%rdx,4)
	movups	%xmm1, -112(%r11,%rdx,4)
	addq	$-32, %rdx
	cmpq	%rdx, %r9
	jne	.LBB5_11
.LBB5_12:                               # %middle.block
	cmpq	%r8, %r10
	je	.LBB5_16
# BB#13:
	subq	%r8, %rax
.LBB5_14:                               # %scalar.ph.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB5_15:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rsi,%rax,4), %ecx
	movl	%ecx, -4(%rdi,%rax,4)
	decq	%rax
	jg	.LBB5_15
.LBB5_16:                               # %.loopexit
	movq	%rdi, %rax
	retq
.Lfunc_end5:
	.size	set_copy, .Lfunc_end5-set_copy
	.cfi_endproc

	.globl	set_and
	.p2align	4, 0x90
	.type	set_and,@function
set_and:                                # @set_and
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -48
.Lcfi18:
	.cfi_offset %r12, -40
.Lcfi19:
	.cfi_offset %r13, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movl	(%rsi), %eax
	movl	%eax, %ecx
	notl	%ecx
	movl	$-1024, %ebx            # imm = 0xFC00
	andl	(%rdi), %ebx
	orq	$-1024, %rcx            # imm = 0xFC00
	andl	$1023, %eax             # imm = 0x3FF
	movq	$-2, %r8
	cmoveq	%rcx, %r8
	orl	%eax, %ebx
	movl	%ebx, (%rdi)
	leaq	2(%r8,%rax), %r9
	cmpq	$8, %r9
	jb	.LBB6_12
# BB#1:                                 # %min.iters.checked
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB6_12
# BB#2:                                 # %vector.memcheck
	testl	%eax, %eax
	movl	$1, %ecx
	cmoveq	%rax, %rcx
	leaq	(%rdi,%rcx,4), %r13
	leaq	4(%rdi,%rax,4), %r10
	leaq	(%rsi,%rcx,4), %r11
	leaq	4(%rsi,%rax,4), %r14
	leaq	(%rdx,%rcx,4), %r15
	leaq	4(%rdx,%rax,4), %r12
	cmpq	%r14, %r13
	sbbb	%cl, %cl
	cmpq	%r10, %r11
	sbbb	%bl, %bl
	andb	%cl, %bl
	cmpq	%r12, %r13
	sbbb	%cl, %cl
	cmpq	%r10, %r15
	sbbb	%r10b, %r10b
	testb	$1, %bl
	jne	.LBB6_12
# BB#3:                                 # %vector.memcheck
	andb	%r10b, %cl
	andb	$1, %cl
	jne	.LBB6_12
# BB#4:                                 # %vector.body.preheader
	leaq	-8(%r8), %rcx
	movq	%rcx, %rbx
	shrq	$3, %rbx
	btl	$3, %ecx
	jb	.LBB6_5
# BB#6:                                 # %vector.body.prol
	movups	-12(%rsi,%rax,4), %xmm0
	movups	-28(%rsi,%rax,4), %xmm1
	movups	-12(%rdx,%rax,4), %xmm2
	movups	-28(%rdx,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rdi,%rax,4)
	movups	%xmm3, -28(%rdi,%rax,4)
	movl	$8, %ecx
	testq	%rbx, %rbx
	jne	.LBB6_8
	jmp	.LBB6_10
.LBB6_5:
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB6_10
.LBB6_8:                                # %vector.body.preheader.new
	movq	%r8, %r10
	negq	%r10
	negq	%rcx
	leaq	-12(%rsi,%rax,4), %r11
	leaq	-12(%rdx,%rax,4), %r14
	leaq	-12(%rdi,%rax,4), %rbx
	.p2align	4, 0x90
.LBB6_9:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r11,%rcx,4), %xmm0
	movups	-16(%r11,%rcx,4), %xmm1
	movups	(%r14,%rcx,4), %xmm2
	movups	-16(%r14,%rcx,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rbx,%rcx,4)
	movups	%xmm3, -16(%rbx,%rcx,4)
	movups	-32(%r11,%rcx,4), %xmm0
	movups	-48(%r11,%rcx,4), %xmm1
	movups	-32(%r14,%rcx,4), %xmm2
	movups	-48(%r14,%rcx,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbx,%rcx,4)
	movups	%xmm3, -48(%rbx,%rcx,4)
	addq	$-16, %rcx
	cmpq	%rcx, %r10
	jne	.LBB6_9
.LBB6_10:                               # %middle.block
	cmpq	%r8, %r9
	je	.LBB6_14
# BB#11:
	subq	%r8, %rax
.LBB6_12:                               # %scalar.ph.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB6_13:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdx,%rax,4), %ecx
	andl	-4(%rsi,%rax,4), %ecx
	movl	%ecx, -4(%rdi,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB6_13
.LBB6_14:                               # %.loopexit
	movq	%rdi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	set_and, .Lfunc_end6-set_and
	.cfi_endproc

	.globl	set_or
	.p2align	4, 0x90
	.type	set_or,@function
set_or:                                 # @set_or
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -48
.Lcfi28:
	.cfi_offset %r12, -40
.Lcfi29:
	.cfi_offset %r13, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movl	(%rsi), %eax
	movl	%eax, %ecx
	notl	%ecx
	movl	$-1024, %ebx            # imm = 0xFC00
	andl	(%rdi), %ebx
	orq	$-1024, %rcx            # imm = 0xFC00
	andl	$1023, %eax             # imm = 0x3FF
	movq	$-2, %r8
	cmoveq	%rcx, %r8
	orl	%eax, %ebx
	movl	%ebx, (%rdi)
	leaq	2(%r8,%rax), %r9
	cmpq	$8, %r9
	jb	.LBB7_12
# BB#1:                                 # %min.iters.checked
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB7_12
# BB#2:                                 # %vector.memcheck
	testl	%eax, %eax
	movl	$1, %ecx
	cmoveq	%rax, %rcx
	leaq	(%rdi,%rcx,4), %r13
	leaq	4(%rdi,%rax,4), %r10
	leaq	(%rsi,%rcx,4), %r11
	leaq	4(%rsi,%rax,4), %r14
	leaq	(%rdx,%rcx,4), %r15
	leaq	4(%rdx,%rax,4), %r12
	cmpq	%r14, %r13
	sbbb	%cl, %cl
	cmpq	%r10, %r11
	sbbb	%bl, %bl
	andb	%cl, %bl
	cmpq	%r12, %r13
	sbbb	%cl, %cl
	cmpq	%r10, %r15
	sbbb	%r10b, %r10b
	testb	$1, %bl
	jne	.LBB7_12
# BB#3:                                 # %vector.memcheck
	andb	%r10b, %cl
	andb	$1, %cl
	jne	.LBB7_12
# BB#4:                                 # %vector.body.preheader
	leaq	-8(%r8), %rcx
	movq	%rcx, %rbx
	shrq	$3, %rbx
	btl	$3, %ecx
	jb	.LBB7_5
# BB#6:                                 # %vector.body.prol
	movups	-12(%rsi,%rax,4), %xmm0
	movups	-28(%rsi,%rax,4), %xmm1
	movups	-12(%rdx,%rax,4), %xmm2
	movups	-28(%rdx,%rax,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rdi,%rax,4)
	movups	%xmm3, -28(%rdi,%rax,4)
	movl	$8, %ecx
	testq	%rbx, %rbx
	jne	.LBB7_8
	jmp	.LBB7_10
.LBB7_5:
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB7_10
.LBB7_8:                                # %vector.body.preheader.new
	movq	%r8, %r10
	negq	%r10
	negq	%rcx
	leaq	-12(%rsi,%rax,4), %r11
	leaq	-12(%rdx,%rax,4), %r14
	leaq	-12(%rdi,%rax,4), %rbx
	.p2align	4, 0x90
.LBB7_9:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r11,%rcx,4), %xmm0
	movups	-16(%r11,%rcx,4), %xmm1
	movups	(%r14,%rcx,4), %xmm2
	movups	-16(%r14,%rcx,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rbx,%rcx,4)
	movups	%xmm3, -16(%rbx,%rcx,4)
	movups	-32(%r11,%rcx,4), %xmm0
	movups	-48(%r11,%rcx,4), %xmm1
	movups	-32(%r14,%rcx,4), %xmm2
	movups	-48(%r14,%rcx,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbx,%rcx,4)
	movups	%xmm3, -48(%rbx,%rcx,4)
	addq	$-16, %rcx
	cmpq	%rcx, %r10
	jne	.LBB7_9
.LBB7_10:                               # %middle.block
	cmpq	%r8, %r9
	je	.LBB7_14
# BB#11:
	subq	%r8, %rax
.LBB7_12:                               # %scalar.ph.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB7_13:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdx,%rax,4), %ecx
	orl	-4(%rsi,%rax,4), %ecx
	movl	%ecx, -4(%rdi,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB7_13
.LBB7_14:                               # %.loopexit
	movq	%rdi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	set_or, .Lfunc_end7-set_or
	.cfi_endproc

	.globl	set_diff
	.p2align	4, 0x90
	.type	set_diff,@function
set_diff:                               # @set_diff
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 48
.Lcfi37:
	.cfi_offset %rbx, -48
.Lcfi38:
	.cfi_offset %r12, -40
.Lcfi39:
	.cfi_offset %r13, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	movl	(%rsi), %eax
	movl	%eax, %ecx
	notl	%ecx
	movl	$-1024, %ebx            # imm = 0xFC00
	andl	(%rdi), %ebx
	orq	$-1024, %rcx            # imm = 0xFC00
	andl	$1023, %eax             # imm = 0x3FF
	movq	$-2, %r8
	cmoveq	%rcx, %r8
	orl	%eax, %ebx
	movl	%ebx, (%rdi)
	leaq	2(%r8,%rax), %r9
	cmpq	$8, %r9
	jb	.LBB8_12
# BB#1:                                 # %min.iters.checked
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB8_12
# BB#2:                                 # %vector.memcheck
	testl	%eax, %eax
	movl	$1, %ecx
	cmoveq	%rax, %rcx
	leaq	(%rdi,%rcx,4), %r13
	leaq	4(%rdi,%rax,4), %r10
	leaq	(%rsi,%rcx,4), %r11
	leaq	4(%rsi,%rax,4), %r14
	leaq	(%rdx,%rcx,4), %r15
	leaq	4(%rdx,%rax,4), %r12
	cmpq	%r14, %r13
	sbbb	%cl, %cl
	cmpq	%r10, %r11
	sbbb	%bl, %bl
	andb	%cl, %bl
	cmpq	%r12, %r13
	sbbb	%cl, %cl
	cmpq	%r10, %r15
	sbbb	%r10b, %r10b
	testb	$1, %bl
	jne	.LBB8_12
# BB#3:                                 # %vector.memcheck
	andb	%r10b, %cl
	andb	$1, %cl
	jne	.LBB8_12
# BB#4:                                 # %vector.body.preheader
	leaq	-8(%r8), %rcx
	movq	%rcx, %rbx
	shrq	$3, %rbx
	btl	$3, %ecx
	jb	.LBB8_5
# BB#6:                                 # %vector.body.prol
	movups	-12(%rsi,%rax,4), %xmm0
	movups	-28(%rsi,%rax,4), %xmm1
	movups	-12(%rdx,%rax,4), %xmm2
	movups	-28(%rdx,%rax,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -12(%rdi,%rax,4)
	movups	%xmm3, -28(%rdi,%rax,4)
	movl	$8, %ecx
	testq	%rbx, %rbx
	jne	.LBB8_8
	jmp	.LBB8_10
.LBB8_5:
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB8_10
.LBB8_8:                                # %vector.body.preheader.new
	movq	%r8, %r10
	negq	%r10
	negq	%rcx
	leaq	-12(%rsi,%rax,4), %r11
	leaq	-12(%rdx,%rax,4), %r14
	leaq	-12(%rdi,%rax,4), %rbx
	.p2align	4, 0x90
.LBB8_9:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r11,%rcx,4), %xmm0
	movups	-16(%r11,%rcx,4), %xmm1
	movups	(%r14,%rcx,4), %xmm2
	movups	-16(%r14,%rcx,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, (%rbx,%rcx,4)
	movups	%xmm3, -16(%rbx,%rcx,4)
	movups	-32(%r11,%rcx,4), %xmm0
	movups	-48(%r11,%rcx,4), %xmm1
	movups	-32(%r14,%rcx,4), %xmm2
	movups	-48(%r14,%rcx,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbx,%rcx,4)
	movups	%xmm3, -48(%rbx,%rcx,4)
	addq	$-16, %rcx
	cmpq	%rcx, %r10
	jne	.LBB8_9
.LBB8_10:                               # %middle.block
	cmpq	%r8, %r9
	je	.LBB8_14
# BB#11:
	subq	%r8, %rax
.LBB8_12:                               # %scalar.ph.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB8_13:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdx,%rax,4), %ecx
	notl	%ecx
	andl	-4(%rsi,%rax,4), %ecx
	movl	%ecx, -4(%rdi,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB8_13
.LBB8_14:                               # %.loopexit
	movq	%rdi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	set_diff, .Lfunc_end8-set_diff
	.cfi_endproc

	.globl	set_xor
	.p2align	4, 0x90
	.type	set_xor,@function
set_xor:                                # @set_xor
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 48
.Lcfi47:
	.cfi_offset %rbx, -48
.Lcfi48:
	.cfi_offset %r12, -40
.Lcfi49:
	.cfi_offset %r13, -32
.Lcfi50:
	.cfi_offset %r14, -24
.Lcfi51:
	.cfi_offset %r15, -16
	movl	(%rsi), %eax
	movl	%eax, %ecx
	notl	%ecx
	movl	$-1024, %ebx            # imm = 0xFC00
	andl	(%rdi), %ebx
	orq	$-1024, %rcx            # imm = 0xFC00
	andl	$1023, %eax             # imm = 0x3FF
	movq	$-2, %r8
	cmoveq	%rcx, %r8
	orl	%eax, %ebx
	movl	%ebx, (%rdi)
	leaq	2(%r8,%rax), %r9
	cmpq	$8, %r9
	jb	.LBB9_12
# BB#1:                                 # %min.iters.checked
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB9_12
# BB#2:                                 # %vector.memcheck
	testl	%eax, %eax
	movl	$1, %ecx
	cmoveq	%rax, %rcx
	leaq	(%rdi,%rcx,4), %r13
	leaq	4(%rdi,%rax,4), %r10
	leaq	(%rsi,%rcx,4), %r11
	leaq	4(%rsi,%rax,4), %r14
	leaq	(%rdx,%rcx,4), %r15
	leaq	4(%rdx,%rax,4), %r12
	cmpq	%r14, %r13
	sbbb	%cl, %cl
	cmpq	%r10, %r11
	sbbb	%bl, %bl
	andb	%cl, %bl
	cmpq	%r12, %r13
	sbbb	%cl, %cl
	cmpq	%r10, %r15
	sbbb	%r10b, %r10b
	testb	$1, %bl
	jne	.LBB9_12
# BB#3:                                 # %vector.memcheck
	andb	%r10b, %cl
	andb	$1, %cl
	jne	.LBB9_12
# BB#4:                                 # %vector.body.preheader
	leaq	-8(%r8), %rcx
	movq	%rcx, %rbx
	shrq	$3, %rbx
	btl	$3, %ecx
	jb	.LBB9_5
# BB#6:                                 # %vector.body.prol
	movups	-12(%rsi,%rax,4), %xmm0
	movups	-28(%rsi,%rax,4), %xmm1
	movups	-12(%rdx,%rax,4), %xmm2
	movups	-28(%rdx,%rax,4), %xmm3
	xorps	%xmm0, %xmm2
	xorps	%xmm1, %xmm3
	movups	%xmm2, -12(%rdi,%rax,4)
	movups	%xmm3, -28(%rdi,%rax,4)
	movl	$8, %ecx
	testq	%rbx, %rbx
	jne	.LBB9_8
	jmp	.LBB9_10
.LBB9_5:
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB9_10
.LBB9_8:                                # %vector.body.preheader.new
	movq	%r8, %r10
	negq	%r10
	negq	%rcx
	leaq	-12(%rsi,%rax,4), %r11
	leaq	-12(%rdx,%rax,4), %r14
	leaq	-12(%rdi,%rax,4), %rbx
	.p2align	4, 0x90
.LBB9_9:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r11,%rcx,4), %xmm0
	movups	-16(%r11,%rcx,4), %xmm1
	movups	(%r14,%rcx,4), %xmm2
	movups	-16(%r14,%rcx,4), %xmm3
	xorps	%xmm0, %xmm2
	xorps	%xmm1, %xmm3
	movups	%xmm2, (%rbx,%rcx,4)
	movups	%xmm3, -16(%rbx,%rcx,4)
	movups	-32(%r11,%rcx,4), %xmm0
	movups	-48(%r11,%rcx,4), %xmm1
	movups	-32(%r14,%rcx,4), %xmm2
	movups	-48(%r14,%rcx,4), %xmm3
	xorps	%xmm0, %xmm2
	xorps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbx,%rcx,4)
	movups	%xmm3, -48(%rbx,%rcx,4)
	addq	$-16, %rcx
	cmpq	%rcx, %r10
	jne	.LBB9_9
.LBB9_10:                               # %middle.block
	cmpq	%r8, %r9
	je	.LBB9_14
# BB#11:
	subq	%r8, %rax
.LBB9_12:                               # %scalar.ph.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB9_13:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdx,%rax,4), %ecx
	xorl	-4(%rsi,%rax,4), %ecx
	movl	%ecx, -4(%rdi,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB9_13
.LBB9_14:                               # %.loopexit
	movq	%rdi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	set_xor, .Lfunc_end9-set_xor
	.cfi_endproc

	.globl	set_merge
	.p2align	4, 0x90
	.type	set_merge,@function
set_merge:                              # @set_merge
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movl	(%rsi), %r10d
	movl	%r10d, %eax
	notl	%eax
	movl	$-1024, %ebp            # imm = 0xFC00
	andl	(%rdi), %ebp
	orq	$-1024, %rax            # imm = 0xFC00
	andl	$1023, %r10d            # imm = 0x3FF
	movq	$-2, %rbx
	cmoveq	%rax, %rbx
	orl	%r10d, %ebp
	movl	%ebp, (%rdi)
	leaq	2(%rbx,%r10), %r9
	cmpq	$8, %r9
	jb	.LBB10_12
# BB#2:                                 # %min.iters.checked
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB10_12
# BB#3:                                 # %vector.memcheck
	testl	%r10d, %r10d
	movl	$1, %eax
	cmoveq	%r10, %rax
	leaq	(%rdi,%rax,4), %rbp
	leaq	4(%rdi,%r10,4), %r11
	leaq	(%rsi,%rax,4), %r14
	leaq	4(%rsi,%r10,4), %rbx
	leaq	(%rcx,%rax,4), %r15
	leaq	4(%rcx,%r10,4), %r12
	leaq	(%rdx,%rax,4), %r13
	leaq	4(%rdx,%r10,4), %rax
	cmpq	%rbx, %rbp
	sbbb	%bl, %bl
	cmpq	%r11, %r14
	sbbb	%r14b, %r14b
	andb	%bl, %r14b
	cmpq	%r12, %rbp
	sbbb	%bl, %bl
	cmpq	%r11, %r15
	sbbb	%r15b, %r15b
	cmpq	%rax, %rbp
	sbbb	%al, %al
	cmpq	%r11, %r13
	sbbb	%r11b, %r11b
	testb	$1, %r14b
	jne	.LBB10_12
# BB#4:                                 # %vector.memcheck
	andb	%r15b, %bl
	andb	$1, %bl
	jne	.LBB10_12
# BB#5:                                 # %vector.memcheck
	andb	%r11b, %al
	andb	$1, %al
	jne	.LBB10_12
# BB#6:                                 # %vector.body.preheader
	movq	%r10, %rax
	subq	%r8, %rax
	leaq	-12(%rsi,%r10,4), %r11
	leaq	-12(%rcx,%r10,4), %r14
	leaq	-12(%rdx,%r10,4), %r15
	leaq	-12(%rdi,%r10,4), %r12
	movq	%r8, %r10
	negq	%r10
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_7:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r11,%rbx,4), %xmm0
	movups	-16(%r11,%rbx,4), %xmm1
	movups	(%r14,%rbx,4), %xmm2
	movups	-16(%r14,%rbx,4), %xmm3
	andps	%xmm2, %xmm0
	andps	%xmm3, %xmm1
	movups	(%r15,%rbx,4), %xmm4
	movups	-16(%r15,%rbx,4), %xmm5
	andnps	%xmm4, %xmm2
	andnps	%xmm5, %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%r12,%rbx,4)
	movups	%xmm3, -16(%r12,%rbx,4)
	addq	$-8, %rbx
	cmpq	%rbx, %r10
	jne	.LBB10_7
# BB#8:                                 # %middle.block
	cmpq	%r8, %r9
	jne	.LBB10_13
	jmp	.LBB10_15
.LBB10_12:
	movq	%r10, %rax
.LBB10_13:                              # %scalar.ph.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB10_14:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rcx,%rax,4), %ebp
	movl	-4(%rsi,%rax,4), %ebx
	andl	%ebp, %ebx
	notl	%ebp
	andl	-4(%rdx,%rax,4), %ebp
	orl	%ebx, %ebp
	movl	%ebp, -4(%rdi,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB10_14
.LBB10_15:                              # %.loopexit
	movq	%rdi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	set_merge, .Lfunc_end10-set_merge
	.cfi_endproc

	.globl	set_andp
	.p2align	4, 0x90
	.type	set_andp,@function
set_andp:                               # @set_andp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
.Lcfi70:
	.cfi_offset %rbx, -56
.Lcfi71:
	.cfi_offset %r12, -48
.Lcfi72:
	.cfi_offset %r13, -40
.Lcfi73:
	.cfi_offset %r14, -32
.Lcfi74:
	.cfi_offset %r15, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movl	(%rsi), %r10d
	movl	%r10d, %eax
	notl	%eax
	movl	$-1024, %ecx            # imm = 0xFC00
	andl	(%rdi), %ecx
	orq	$-1024, %rax            # imm = 0xFC00
	andl	$1023, %r10d            # imm = 0x3FF
	movq	$-2, %rbx
	cmoveq	%rax, %rbx
	orl	%r10d, %ecx
	movl	%ecx, (%rdi)
	leaq	2(%rbx,%r10), %r8
	xorl	%ecx, %ecx
	cmpq	$8, %r8
	jb	.LBB11_10
# BB#2:                                 # %min.iters.checked
	movq	%r8, %r9
	andq	$-8, %r9
	je	.LBB11_10
# BB#3:                                 # %vector.memcheck
	testl	%r10d, %r10d
	movl	$1, %eax
	cmoveq	%r10, %rax
	leaq	(%rdi,%rax,4), %rbp
	leaq	4(%rdi,%r10,4), %r11
	leaq	(%rsi,%rax,4), %r14
	leaq	4(%rsi,%r10,4), %r15
	leaq	(%rdx,%rax,4), %r12
	leaq	4(%rdx,%r10,4), %r13
	cmpq	%r15, %rbp
	sbbb	%al, %al
	cmpq	%r11, %r14
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%r13, %rbp
	sbbb	%al, %al
	cmpq	%r11, %r12
	sbbb	%r11b, %r11b
	testb	$1, %bl
	jne	.LBB11_10
# BB#4:                                 # %vector.memcheck
	andb	%r11b, %al
	andb	$1, %al
	jne	.LBB11_10
# BB#5:                                 # %vector.body.preheader
	movq	%r10, %rax
	subq	%r9, %rax
	leaq	-12(%rsi,%r10,4), %r11
	leaq	-12(%rdx,%r10,4), %r14
	leaq	-12(%rdi,%r10,4), %rcx
	movq	%r9, %r10
	negq	%r10
	pxor	%xmm0, %xmm0
	xorl	%ebx, %ebx
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB11_6:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r11,%rbx,4), %xmm2
	movdqu	-16(%r11,%rbx,4), %xmm3
	movdqu	(%r14,%rbx,4), %xmm4
	movdqu	-16(%r14,%rbx,4), %xmm5
	pand	%xmm2, %xmm4
	pshufd	$27, %xmm4, %xmm2       # xmm2 = xmm4[3,2,1,0]
	pand	%xmm3, %xmm5
	pshufd	$27, %xmm5, %xmm3       # xmm3 = xmm5[3,2,1,0]
	movdqu	%xmm4, (%rcx,%rbx,4)
	movdqu	%xmm5, -16(%rcx,%rbx,4)
	por	%xmm2, %xmm0
	por	%xmm3, %xmm1
	addq	$-8, %rbx
	cmpq	%rbx, %r10
	jne	.LBB11_6
# BB#7:                                 # %middle.block
	por	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	por	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	por	%xmm0, %xmm1
	movd	%xmm1, %ecx
	cmpq	%r9, %r8
	jne	.LBB11_11
	jmp	.LBB11_13
.LBB11_10:
	movq	%r10, %rax
.LBB11_11:                              # %scalar.ph.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB11_12:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdx,%rax,4), %ebp
	andl	-4(%rsi,%rax,4), %ebp
	movl	%ebp, -4(%rdi,%rax,4)
	orl	%ebp, %ecx
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB11_12
.LBB11_13:                              # %.loopexit
	xorl	%eax, %eax
	testl	%ecx, %ecx
	setne	%al
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	set_andp, .Lfunc_end11-set_andp
	.cfi_endproc

	.globl	set_orp
	.p2align	4, 0x90
	.type	set_orp,@function
set_orp:                                # @set_orp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi79:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi80:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 56
.Lcfi82:
	.cfi_offset %rbx, -56
.Lcfi83:
	.cfi_offset %r12, -48
.Lcfi84:
	.cfi_offset %r13, -40
.Lcfi85:
	.cfi_offset %r14, -32
.Lcfi86:
	.cfi_offset %r15, -24
.Lcfi87:
	.cfi_offset %rbp, -16
	movl	(%rsi), %r10d
	movl	%r10d, %eax
	notl	%eax
	movl	$-1024, %ecx            # imm = 0xFC00
	andl	(%rdi), %ecx
	orq	$-1024, %rax            # imm = 0xFC00
	andl	$1023, %r10d            # imm = 0x3FF
	movq	$-2, %rbx
	cmoveq	%rax, %rbx
	orl	%r10d, %ecx
	movl	%ecx, (%rdi)
	leaq	2(%rbx,%r10), %r8
	xorl	%ecx, %ecx
	cmpq	$8, %r8
	jb	.LBB12_10
# BB#2:                                 # %min.iters.checked
	movq	%r8, %r9
	andq	$-8, %r9
	je	.LBB12_10
# BB#3:                                 # %vector.memcheck
	testl	%r10d, %r10d
	movl	$1, %eax
	cmoveq	%r10, %rax
	leaq	(%rdi,%rax,4), %rbp
	leaq	4(%rdi,%r10,4), %r11
	leaq	(%rsi,%rax,4), %r14
	leaq	4(%rsi,%r10,4), %r15
	leaq	(%rdx,%rax,4), %r12
	leaq	4(%rdx,%r10,4), %r13
	cmpq	%r15, %rbp
	sbbb	%al, %al
	cmpq	%r11, %r14
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%r13, %rbp
	sbbb	%al, %al
	cmpq	%r11, %r12
	sbbb	%r11b, %r11b
	testb	$1, %bl
	jne	.LBB12_10
# BB#4:                                 # %vector.memcheck
	andb	%r11b, %al
	andb	$1, %al
	jne	.LBB12_10
# BB#5:                                 # %vector.body.preheader
	movq	%r10, %rax
	subq	%r9, %rax
	leaq	-12(%rsi,%r10,4), %r11
	leaq	-12(%rdx,%r10,4), %r14
	leaq	-12(%rdi,%r10,4), %rcx
	movq	%r9, %r10
	negq	%r10
	pxor	%xmm0, %xmm0
	xorl	%ebx, %ebx
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB12_6:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r11,%rbx,4), %xmm2
	movdqu	-16(%r11,%rbx,4), %xmm3
	movdqu	(%r14,%rbx,4), %xmm4
	movdqu	-16(%r14,%rbx,4), %xmm5
	por	%xmm2, %xmm4
	pshufd	$27, %xmm4, %xmm2       # xmm2 = xmm4[3,2,1,0]
	por	%xmm3, %xmm5
	pshufd	$27, %xmm5, %xmm3       # xmm3 = xmm5[3,2,1,0]
	movdqu	%xmm4, (%rcx,%rbx,4)
	movdqu	%xmm5, -16(%rcx,%rbx,4)
	por	%xmm2, %xmm0
	por	%xmm3, %xmm1
	addq	$-8, %rbx
	cmpq	%rbx, %r10
	jne	.LBB12_6
# BB#7:                                 # %middle.block
	por	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	por	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	por	%xmm0, %xmm1
	movd	%xmm1, %ecx
	cmpq	%r9, %r8
	jne	.LBB12_11
	jmp	.LBB12_13
.LBB12_10:
	movq	%r10, %rax
.LBB12_11:                              # %scalar.ph.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB12_12:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdx,%rax,4), %ebp
	orl	-4(%rsi,%rax,4), %ebp
	movl	%ebp, -4(%rdi,%rax,4)
	orl	%ebp, %ecx
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB12_12
.LBB12_13:                              # %.loopexit
	xorl	%eax, %eax
	testl	%ecx, %ecx
	setne	%al
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	set_orp, .Lfunc_end12-set_orp
	.cfi_endproc

	.globl	setp_empty
	.p2align	4, 0x90
	.type	setp_empty,@function
setp_empty:                             # @setp_empty
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	andl	$1023, %eax             # imm = 0x3FF
	incq	%rax
	.p2align	4, 0x90
.LBB13_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	$0, -4(%rdi,%rax,4)
	jne	.LBB13_2
# BB#3:                                 #   in Loop: Header=BB13_1 Depth=1
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB13_1
# BB#4:
	movl	$1, %eax
	retq
.LBB13_2:
	xorl	%eax, %eax
	retq
.Lfunc_end13:
	.size	setp_empty, .Lfunc_end13-setp_empty
	.cfi_endproc

	.globl	setp_full
	.p2align	4, 0x90
	.type	setp_full,@function
setp_full:                              # @setp_full
	.cfi_startproc
# BB#0:
	movl	(%rdi), %edx
	movl	%edx, %ecx
	shll	$5, %ecx
	subl	%esi, %ecx
	movl	$-1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	andl	$1023, %edx             # imm = 0x3FF
	xorl	%eax, %eax
	cmpl	%esi, (%rdi,%rdx,4)
	jne	.LBB14_4
	.p2align	4, 0x90
.LBB14_1:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$2, %rdx
	jl	.LBB14_2
# BB#3:                                 #   in Loop: Header=BB14_1 Depth=1
	cmpl	$-1, -4(%rdi,%rdx,4)
	leaq	-1(%rdx), %rdx
	je	.LBB14_1
.LBB14_4:                               # %.loopexit
	retq
.LBB14_2:
	movl	$1, %eax
	retq
.Lfunc_end14:
	.size	setp_full, .Lfunc_end14-setp_full
	.cfi_endproc

	.globl	setp_equal
	.p2align	4, 0x90
	.type	setp_equal,@function
setp_equal:                             # @setp_equal
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	andl	$1023, %eax             # imm = 0x3FF
	incq	%rax
	.p2align	4, 0x90
.LBB15_1:                               # =>This Inner Loop Header: Depth=1
	movl	-4(%rdi,%rax,4), %ecx
	cmpl	-4(%rsi,%rax,4), %ecx
	jne	.LBB15_2
# BB#3:                                 #   in Loop: Header=BB15_1 Depth=1
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB15_1
# BB#4:
	movl	$1, %eax
	retq
.LBB15_2:
	xorl	%eax, %eax
	retq
.Lfunc_end15:
	.size	setp_equal, .Lfunc_end15-setp_equal
	.cfi_endproc

	.globl	setp_disjoint
	.p2align	4, 0x90
	.type	setp_disjoint,@function
setp_disjoint:                          # @setp_disjoint
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	andl	$1023, %eax             # imm = 0x3FF
	incq	%rax
	.p2align	4, 0x90
.LBB16_1:                               # =>This Inner Loop Header: Depth=1
	movl	-4(%rsi,%rax,4), %ecx
	testl	-4(%rdi,%rax,4), %ecx
	jne	.LBB16_2
# BB#3:                                 #   in Loop: Header=BB16_1 Depth=1
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB16_1
# BB#4:
	movl	$1, %eax
	retq
.LBB16_2:
	xorl	%eax, %eax
	retq
.Lfunc_end16:
	.size	setp_disjoint, .Lfunc_end16-setp_disjoint
	.cfi_endproc

	.globl	setp_implies
	.p2align	4, 0x90
	.type	setp_implies,@function
setp_implies:                           # @setp_implies
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	andl	$1023, %eax             # imm = 0x3FF
	incq	%rax
	.p2align	4, 0x90
.LBB17_1:                               # =>This Inner Loop Header: Depth=1
	movl	-4(%rsi,%rax,4), %ecx
	notl	%ecx
	testl	-4(%rdi,%rax,4), %ecx
	jne	.LBB17_2
# BB#3:                                 #   in Loop: Header=BB17_1 Depth=1
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB17_1
# BB#4:
	movl	$1, %eax
	retq
.LBB17_2:
	xorl	%eax, %eax
	retq
.Lfunc_end17:
	.size	setp_implies, .Lfunc_end17-setp_implies
	.cfi_endproc

	.globl	sf_or
	.p2align	4, 0x90
	.type	sf_or,@function
sf_or:                                  # @sf_or
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi91:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi92:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi94:
	.cfi_def_cfa_offset 80
.Lcfi95:
	.cfi_offset %rbx, -56
.Lcfi96:
	.cfi_offset %r12, -48
.Lcfi97:
	.cfi_offset %r13, -40
.Lcfi98:
	.cfi_offset %r14, -32
.Lcfi99:
	.cfi_offset %r15, -24
.Lcfi100:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	4(%r14), %eax
	cmpl	$32, %eax
	jg	.LBB18_2
# BB#1:                                 # %.thread
	movl	$8, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	$1, %ebp
	jmp	.LBB18_3
.LBB18_2:
	decl	%eax
	sarl	$5, %eax
	movslq	%eax, %rbp
	leaq	8(,%rbp,4), %rdi
	callq	malloc
	movq	%rax, %rbx
	incl	%ebp
.LBB18_3:                               # %set_clear.exit
	movl	%ebp, (%rbx)
	movslq	%ebp, %rax
	movl	%eax, %ecx
	notl	%ecx
	cmpl	$-3, %ecx
	movl	$-2, %edx
	cmovgl	%ecx, %edx
	leal	1(%rbp,%rdx), %ecx
	subq	%rcx, %rax
	leaq	(%rbx,%rax,4), %rdi
	leaq	4(,%rcx,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movslq	(%r14), %rdx
	movslq	12(%r14), %rax
	movq	%rdx, %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	imulq	%rdx, %rax
	testl	%eax, %eax
	jle	.LBB18_20
# BB#4:                                 # %.lr.ph.preheader
	movq	24(%r14), %r15
	leaq	(%r15,%rax,4), %r12
	leaq	-4(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	4(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(,%rax,4), %r13
	xorl	%r10d, %r10d
	movl	$1, %r14d
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB18_5:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_14 Depth 2
                                        #     Child Loop BB18_18 Depth 2
	movl	(%rbx), %edx
	movl	%edx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	testw	$1023, %dx              # imm = 0x3FF
	movq	%rax, %rdx
	cmoveq	%r14, %rdx
	cmpq	$8, %rdx
	jb	.LBB18_17
# BB#6:                                 # %min.iters.checked
                                        #   in Loop: Header=BB18_5 Depth=1
	movq	%rdx, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB18_17
# BB#7:                                 # %vector.memcheck
                                        #   in Loop: Header=BB18_5 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	imulq	%r10, %rdi
	leaq	4(%r15,%rdi,4), %r11
	movq	%rax, %rbp
	notq	%rbp
	testl	%eax, %eax
	movq	$-2, %rsi
	cmovneq	%rsi, %rbp
	shlq	$2, %rbp
	movq	16(%rsp), %r9           # 8-byte Reload
	subq	%rbp, %r9
	leaq	(%r11,%rax,4), %rsi
	cmpq	%rsi, %r9
	jae	.LBB18_9
# BB#8:                                 # %vector.memcheck
                                        #   in Loop: Header=BB18_5 Depth=1
	leaq	-4(%r15,%rdi,4), %rsi
	leal	(,%rax,4), %edi
	addq	8(%rsp), %rdi           # 8-byte Folded Reload
	subq	%rbp, %rsi
	cmpq	%rdi, %rsi
	jb	.LBB18_17
.LBB18_9:                               # %vector.body.preheader
                                        #   in Loop: Header=BB18_5 Depth=1
	leaq	-8(%r8), %rsi
	movq	%rsi, %rbp
	shrq	$3, %rbp
	btl	$3, %esi
	jb	.LBB18_10
# BB#11:                                # %vector.body.prol
                                        #   in Loop: Header=BB18_5 Depth=1
	movups	-12(%rbx,%rax,4), %xmm0
	movups	-28(%rbx,%rax,4), %xmm1
	movups	-12(%rcx,%rax,4), %xmm2
	movups	-28(%rcx,%rax,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rax,4)
	movups	%xmm3, -28(%rbx,%rax,4)
	movl	$8, %edi
	testq	%rbp, %rbp
	jne	.LBB18_13
	jmp	.LBB18_15
.LBB18_10:                              #   in Loop: Header=BB18_5 Depth=1
	xorl	%edi, %edi
	testq	%rbp, %rbp
	je	.LBB18_15
.LBB18_13:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB18_5 Depth=1
	movq	%r8, %rbp
	subq	%rdi, %rbp
	leaq	(,%rax,4), %r11
	shlq	$2, %rdi
	subq	%rdi, %r11
	.p2align	4, 0x90
.LBB18_14:                              # %vector.body
                                        #   Parent Loop BB18_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-28(%rbx,%r11), %xmm0
	movups	-12(%rbx,%r11), %xmm1
	movups	-28(%rcx,%r11), %xmm2
	movups	-12(%rcx,%r11), %xmm3
	orps	%xmm1, %xmm3
	orps	%xmm0, %xmm2
	movups	%xmm3, -12(%rbx,%r11)
	movups	%xmm2, -28(%rbx,%r11)
	movups	-60(%rbx,%r11), %xmm0
	movups	-44(%rbx,%r11), %xmm1
	movups	-60(%rcx,%r11), %xmm2
	movups	-44(%rcx,%r11), %xmm3
	orps	%xmm1, %xmm3
	orps	%xmm0, %xmm2
	movups	%xmm3, -44(%rbx,%r11)
	movups	%xmm2, -60(%rbx,%r11)
	addq	$-64, %r11
	addq	$-16, %rbp
	jne	.LBB18_14
.LBB18_15:                              # %middle.block
                                        #   in Loop: Header=BB18_5 Depth=1
	cmpq	%r8, %rdx
	je	.LBB18_19
# BB#16:                                #   in Loop: Header=BB18_5 Depth=1
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB18_17:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB18_5 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB18_18:                              # %scalar.ph
                                        #   Parent Loop BB18_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rcx,%rax,4), %edx
	orl	%edx, -4(%rbx,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB18_18
.LBB18_19:                              # %.loopexit
                                        #   in Loop: Header=BB18_5 Depth=1
	addq	%r13, %rcx
	incq	%r10
	cmpq	%r12, %rcx
	jb	.LBB18_5
.LBB18_20:                              # %._crit_edge
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	sf_or, .Lfunc_end18-sf_or
	.cfi_endproc

	.globl	sf_and
	.p2align	4, 0x90
	.type	sf_and,@function
sf_and:                                 # @sf_and
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi107:
	.cfi_def_cfa_offset 80
.Lcfi108:
	.cfi_offset %rbx, -56
.Lcfi109:
	.cfi_offset %r12, -48
.Lcfi110:
	.cfi_offset %r13, -40
.Lcfi111:
	.cfi_offset %r14, -32
.Lcfi112:
	.cfi_offset %r15, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	4(%r14), %r15d
	cmpl	$32, %r15d
	jg	.LBB19_2
# BB#1:                                 # %.thread
	movl	$8, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	$1, %ebp
	jmp	.LBB19_3
.LBB19_2:
	leal	-1(%r15), %eax
	sarl	$5, %eax
	movslq	%eax, %rbp
	leaq	8(,%rbp,4), %rdi
	callq	malloc
	movq	%rax, %rbx
	incl	%ebp
.LBB19_3:                               # %set_clear.exit
	movl	%ebp, (%rbx)
	movslq	%ebp, %rax
	movl	%eax, %ecx
	notl	%ecx
	cmpl	$-3, %ecx
	movl	$-2, %edx
	cmovgl	%ecx, %edx
	leal	1(%rbp,%rdx), %ecx
	subq	%rcx, %rax
	leaq	(%rbx,%rax,4), %rdi
	leaq	4(,%rcx,4), %rdx
	xorl	%esi, %esi
	callq	memset
	cmpl	$33, %r15d
	movl	$1, %eax
	jl	.LBB19_5
# BB#4:
	leal	-1(%r15), %eax
	sarl	$5, %eax
	incl	%eax
.LBB19_5:
	movl	%eax, (%rbx)
	cltq
	movl	%eax, %ecx
	shll	$5, %ecx
	subl	%r15d, %ecx
	movl	$-1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	movl	%edx, (%rbx,%rax,4)
	cmpl	$2, %eax
	jl	.LBB19_7
# BB#6:                                 # %.lr.ph.preheader.i
	leaq	-1(%rax), %rcx
	leal	-2(%rax), %eax
	subq	%rax, %rcx
	leaq	(%rbx,%rcx,4), %rdi
	leaq	4(,%rax,4), %rdx
	movl	$255, %esi
	callq	memset
.LBB19_7:                               # %set_fill.exit
	movslq	(%r14), %rdx
	movslq	12(%r14), %rax
	movq	%rdx, %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	imulq	%rdx, %rax
	testl	%eax, %eax
	jle	.LBB19_24
# BB#8:                                 # %.lr.ph.preheader
	movq	24(%r14), %r15
	leaq	(%r15,%rax,4), %r12
	leaq	-4(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	4(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(,%rax,4), %r13
	xorl	%r10d, %r10d
	movl	$1, %r14d
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB19_9:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_18 Depth 2
                                        #     Child Loop BB19_22 Depth 2
	movl	(%rbx), %edx
	movl	%edx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	testw	$1023, %dx              # imm = 0x3FF
	movq	%rax, %rdx
	cmoveq	%r14, %rdx
	cmpq	$8, %rdx
	jb	.LBB19_21
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB19_9 Depth=1
	movq	%rdx, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB19_21
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB19_9 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	imulq	%r10, %rdi
	leaq	4(%r15,%rdi,4), %r11
	movq	%rax, %rbp
	notq	%rbp
	testl	%eax, %eax
	movq	$-2, %rsi
	cmovneq	%rsi, %rbp
	shlq	$2, %rbp
	movq	16(%rsp), %r9           # 8-byte Reload
	subq	%rbp, %r9
	leaq	(%r11,%rax,4), %rsi
	cmpq	%rsi, %r9
	jae	.LBB19_13
# BB#12:                                # %vector.memcheck
                                        #   in Loop: Header=BB19_9 Depth=1
	leaq	-4(%r15,%rdi,4), %rsi
	leal	(,%rax,4), %edi
	addq	8(%rsp), %rdi           # 8-byte Folded Reload
	subq	%rbp, %rsi
	cmpq	%rdi, %rsi
	jb	.LBB19_21
.LBB19_13:                              # %vector.body.preheader
                                        #   in Loop: Header=BB19_9 Depth=1
	leaq	-8(%r8), %rsi
	movq	%rsi, %rbp
	shrq	$3, %rbp
	btl	$3, %esi
	jb	.LBB19_14
# BB#15:                                # %vector.body.prol
                                        #   in Loop: Header=BB19_9 Depth=1
	movups	-12(%rbx,%rax,4), %xmm0
	movups	-28(%rbx,%rax,4), %xmm1
	movups	-12(%rcx,%rax,4), %xmm2
	movups	-28(%rcx,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rax,4)
	movups	%xmm3, -28(%rbx,%rax,4)
	movl	$8, %edi
	testq	%rbp, %rbp
	jne	.LBB19_17
	jmp	.LBB19_19
.LBB19_14:                              #   in Loop: Header=BB19_9 Depth=1
	xorl	%edi, %edi
	testq	%rbp, %rbp
	je	.LBB19_19
.LBB19_17:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB19_9 Depth=1
	movq	%r8, %rbp
	subq	%rdi, %rbp
	leaq	(,%rax,4), %r11
	shlq	$2, %rdi
	subq	%rdi, %r11
	.p2align	4, 0x90
.LBB19_18:                              # %vector.body
                                        #   Parent Loop BB19_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-28(%rbx,%r11), %xmm0
	movups	-12(%rbx,%r11), %xmm1
	movups	-28(%rcx,%r11), %xmm2
	movups	-12(%rcx,%r11), %xmm3
	andps	%xmm1, %xmm3
	andps	%xmm0, %xmm2
	movups	%xmm3, -12(%rbx,%r11)
	movups	%xmm2, -28(%rbx,%r11)
	movups	-60(%rbx,%r11), %xmm0
	movups	-44(%rbx,%r11), %xmm1
	movups	-60(%rcx,%r11), %xmm2
	movups	-44(%rcx,%r11), %xmm3
	andps	%xmm1, %xmm3
	andps	%xmm0, %xmm2
	movups	%xmm3, -44(%rbx,%r11)
	movups	%xmm2, -60(%rbx,%r11)
	addq	$-64, %r11
	addq	$-16, %rbp
	jne	.LBB19_18
.LBB19_19:                              # %middle.block
                                        #   in Loop: Header=BB19_9 Depth=1
	cmpq	%r8, %rdx
	je	.LBB19_23
# BB#20:                                #   in Loop: Header=BB19_9 Depth=1
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB19_21:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB19_9 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB19_22:                              # %scalar.ph
                                        #   Parent Loop BB19_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rcx,%rax,4), %edx
	andl	%edx, -4(%rbx,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB19_22
.LBB19_23:                              # %.loopexit
                                        #   in Loop: Header=BB19_9 Depth=1
	addq	%r13, %rcx
	incq	%r10
	cmpq	%r12, %rcx
	jb	.LBB19_9
.LBB19_24:                              # %._crit_edge
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	sf_and, .Lfunc_end19-sf_and
	.cfi_endproc

	.globl	sf_active
	.p2align	4, 0x90
	.type	sf_active,@function
sf_active:                              # @sf_active
	.cfi_startproc
# BB#0:
	movslq	12(%rdi), %rax
	movslq	(%rdi), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB20_4
# BB#1:                                 # %.lr.ph.preheader
	movq	24(%rdi), %rax
	leaq	(%rax,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB20_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	orb	$32, 1(%rax)
	movslq	(%rdi), %rdx
	leaq	(%rax,%rdx,4), %rax
	cmpq	%rcx, %rax
	jb	.LBB20_2
# BB#3:                                 # %._crit_edge.loopexit
	movl	12(%rdi), %eax
.LBB20_4:                               # %._crit_edge
	movl	%eax, 16(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end20:
	.size	sf_active, .Lfunc_end20-sf_active
	.cfi_endproc

	.globl	sf_inactive
	.p2align	4, 0x90
	.type	sf_inactive,@function
sf_inactive:                            # @sf_inactive
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 16
.Lcfi115:
	.cfi_offset %rbx, -16
	movl	(%rdi), %esi
	movl	12(%rdi), %ecx
	imull	%esi, %ecx
	testl	%ecx, %ecx
	jle	.LBB21_14
# BB#1:                                 # %.lr.ph.preheader
	movq	24(%rdi), %rax
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %r8
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB21_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_6 Depth 2
                                        #     Child Loop BB21_9 Depth 2
	movl	(%rdx), %ecx
	testb	$32, %ch
	jne	.LBB21_3
# BB#12:                                #   in Loop: Header=BB21_2 Depth=1
	decl	12(%rdi)
	jmp	.LBB21_13
	.p2align	4, 0x90
.LBB21_3:                               #   in Loop: Header=BB21_2 Depth=1
	cmpq	%rdx, %rax
	je	.LBB21_11
# BB#4:                                 #   in Loop: Header=BB21_2 Depth=1
	andl	$1023, %ecx             # imm = 0x3FF
	movl	%ecx, %ebx
	incl	%ebx
	andq	$7, %rbx
	movq	%rcx, %r9
	movq	%rcx, %rsi
	je	.LBB21_7
# BB#5:                                 # %.prol.preheader
                                        #   in Loop: Header=BB21_2 Depth=1
	negq	%rbx
	movq	%r9, %rcx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB21_6:                               #   Parent Loop BB21_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx,%rsi,4), %ecx
	movl	%ecx, (%rax,%rsi,4)
	decq	%rsi
	incq	%rbx
	jne	.LBB21_6
.LBB21_7:                               # %.prol.loopexit
                                        #   in Loop: Header=BB21_2 Depth=1
	cmpl	$7, %r9d
	jb	.LBB21_10
# BB#8:                                 # %.new
                                        #   in Loop: Header=BB21_2 Depth=1
	addq	$8, %rsi
	.p2align	4, 0x90
.LBB21_9:                               #   Parent Loop BB21_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-32(%rdx,%rsi,4), %ecx
	movl	%ecx, -32(%rax,%rsi,4)
	movl	-36(%rdx,%rsi,4), %ecx
	movl	%ecx, -36(%rax,%rsi,4)
	movl	-40(%rdx,%rsi,4), %ecx
	movl	%ecx, -40(%rax,%rsi,4)
	movl	-44(%rdx,%rsi,4), %ecx
	movl	%ecx, -44(%rax,%rsi,4)
	movl	-48(%rdx,%rsi,4), %ecx
	movl	%ecx, -48(%rax,%rsi,4)
	movl	-52(%rdx,%rsi,4), %ecx
	movl	%ecx, -52(%rax,%rsi,4)
	movl	-56(%rdx,%rsi,4), %ecx
	movl	%ecx, -56(%rax,%rsi,4)
	movl	-60(%rdx,%rsi,4), %ecx
	movl	%ecx, -60(%rax,%rsi,4)
	addq	$-8, %rsi
	cmpq	$7, %rsi
	jg	.LBB21_9
.LBB21_10:                              # %.loopexit.loopexit
                                        #   in Loop: Header=BB21_2 Depth=1
	movl	(%rdi), %esi
.LBB21_11:                              # %.loopexit
                                        #   in Loop: Header=BB21_2 Depth=1
	movslq	%esi, %rcx
	leaq	(%rax,%rcx,4), %rax
.LBB21_13:                              #   in Loop: Header=BB21_2 Depth=1
	movslq	%esi, %rcx
	leaq	(%rdx,%rcx,4), %rdx
	cmpq	%r8, %rdx
	jb	.LBB21_2
.LBB21_14:                              # %._crit_edge
	movq	%rdi, %rax
	popq	%rbx
	retq
.Lfunc_end21:
	.size	sf_inactive, .Lfunc_end21-sf_inactive
	.cfi_endproc

	.globl	sf_copy
	.p2align	4, 0x90
	.type	sf_copy,@function
sf_copy:                                # @sf_copy
	.cfi_startproc
# BB#0:
	movl	4(%rsi), %eax
	movl	%eax, 4(%rdi)
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	movl	12(%rsi), %eax
	movl	%eax, 12(%rdi)
	movl	16(%rsi), %eax
	movl	%eax, 16(%rdi)
	movslq	(%rsi), %rax
	movslq	12(%rsi), %r9
	imulq	%rax, %r9
	testq	%r9, %r9
	jle	.LBB22_23
# BB#1:                                 # %.lr.ph.i.preheader
	movq	24(%rsi), %rcx
	movq	24(%rdi), %rdx
	cmpq	$8, %r9
	jae	.LBB22_3
# BB#2:
	xorl	%r10d, %r10d
	jmp	.LBB22_17
.LBB22_3:                               # %min.iters.checked
	movq	%r9, %r10
	andq	$-8, %r10
	je	.LBB22_7
# BB#4:                                 # %vector.memcheck
	leaq	(%rcx,%r9,4), %rax
	cmpq	%rax, %rdx
	jae	.LBB22_8
# BB#5:                                 # %vector.memcheck
	leaq	(%rdx,%r9,4), %rax
	cmpq	%rax, %rcx
	jae	.LBB22_8
.LBB22_7:
	xorl	%r10d, %r10d
.LBB22_17:                              # %.lr.ph.i.preheader24
	movl	%r9d, %eax
	subl	%r10d, %eax
	leaq	-1(%r9), %r8
	subq	%r10, %r8
	andq	$7, %rax
	je	.LBB22_20
# BB#18:                                # %.lr.ph.i.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB22_19:                              # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %esi
	addq	$4, %rcx
	movl	%esi, (%rdx)
	addq	$4, %rdx
	incq	%r10
	incq	%rax
	jne	.LBB22_19
.LBB22_20:                              # %.lr.ph.i.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB22_23
# BB#21:                                # %.lr.ph.i.preheader24.new
	subq	%r10, %r9
	.p2align	4, 0x90
.LBB22_22:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %eax
	movl	%eax, (%rdx)
	movl	4(%rcx), %eax
	movl	%eax, 4(%rdx)
	movl	8(%rcx), %eax
	movl	%eax, 8(%rdx)
	movl	12(%rcx), %eax
	movl	%eax, 12(%rdx)
	movl	16(%rcx), %eax
	movl	%eax, 16(%rdx)
	movl	20(%rcx), %eax
	movl	%eax, 20(%rdx)
	movl	24(%rcx), %eax
	movl	%eax, 24(%rdx)
	movl	28(%rcx), %eax
	movl	%eax, 28(%rdx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %r9
	jne	.LBB22_22
.LBB22_23:                              # %intcpy.exit
	movq	%rdi, %rax
	retq
.LBB22_8:                               # %vector.body.preheader
	leaq	-8(%r10), %r8
	movl	%r8d, %eax
	shrl	$3, %eax
	incl	%eax
	andq	$3, %rax
	je	.LBB22_11
# BB#9:                                 # %vector.body.prol.preheader
	negq	%rax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB22_10:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rsi,4), %xmm0
	movups	16(%rcx,%rsi,4), %xmm1
	movups	%xmm0, (%rdx,%rsi,4)
	movups	%xmm1, 16(%rdx,%rsi,4)
	addq	$8, %rsi
	incq	%rax
	jne	.LBB22_10
	jmp	.LBB22_12
.LBB22_11:
	xorl	%esi, %esi
.LBB22_12:                              # %vector.body.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB22_15
# BB#13:                                # %vector.body.preheader.new
	movq	%r10, %r8
	subq	%rsi, %r8
	leaq	112(%rcx,%rsi,4), %rax
	leaq	112(%rdx,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB22_14:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rax), %xmm0
	movups	-96(%rax), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rax), %xmm0
	movups	-64(%rax), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rax), %xmm0
	movups	-32(%rax), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rax), %xmm0
	movups	(%rax), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rax
	subq	$-128, %rsi
	addq	$-32, %r8
	jne	.LBB22_14
.LBB22_15:                              # %middle.block
	cmpq	%r10, %r9
	je	.LBB22_23
# BB#16:
	leaq	(%rdx,%r10,4), %rdx
	leaq	(%rcx,%r10,4), %rcx
	jmp	.LBB22_17
.Lfunc_end22:
	.size	sf_copy, .Lfunc_end22-sf_copy
	.cfi_endproc

	.globl	sf_join
	.p2align	4, 0x90
	.type	sf_join,@function
sf_join:                                # @sf_join
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi116:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi117:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi118:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi119:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi120:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi121:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi122:
	.cfi_def_cfa_offset 80
.Lcfi123:
	.cfi_offset %rbx, -56
.Lcfi124:
	.cfi_offset %r12, -48
.Lcfi125:
	.cfi_offset %r13, -40
.Lcfi126:
	.cfi_offset %r14, -32
.Lcfi127:
	.cfi_offset %r15, -24
.Lcfi128:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movslq	12(%r12), %r14
	movslq	(%r12), %rcx
	movslq	12(%rbx), %rdx
	movslq	(%rbx), %r15
	movl	4(%r12), %ebp
	cmpl	4(%rbx), %ebp
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	jne	.LBB23_2
# BB#1:
	movl	%edx, %r13d
	movl	%r14d, %esi
	jmp	.LBB23_3
.LBB23_2:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rcx, %r13
	movq	%rdx, %rbp
	callq	fatal
	movq	%rbp, %rdx
	movq	%r13, %rcx
	movl	4(%r12), %ebp
	movl	12(%r12), %esi
	movl	12(%rbx), %r13d
.LBB23_3:
	imulq	%r14, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	set_family_garbage(%rip), %rax
	testq	%rax, %rax
	je	.LBB23_5
# BB#4:
	movq	32(%rax), %rcx
	movq	%rcx, set_family_garbage(%rip)
	jmp	.LBB23_6
.LBB23_5:
	movl	$40, %edi
	movq	%rdx, %rbx
	movl	%esi, %r14d
	callq	malloc
	movl	%r14d, %esi
	movq	%rbx, %rdx
.LBB23_6:
	imulq	%rdx, %r15
	addl	%esi, %r13d
	movl	%ebp, 4(%rax)
	movl	$2, %edx
	cmpl	$33, %ebp
	jl	.LBB23_8
# BB#7:
	decl	%ebp
	sarl	$5, %ebp
	addl	$2, %ebp
	movl	%ebp, %edx
.LBB23_8:                               # %sf_new.exit
	movl	%edx, (%rax)
	movl	%r13d, 8(%rax)
	movslq	%r13d, %rcx
	movslq	%edx, %rdi
	imulq	%rcx, %rdi
	shlq	$2, %rdi
	movq	%rax, %rbx
	callq	malloc
	movq	%rbx, %r11
	movq	%rax, 24(%r11)
	movq	$0, 12(%r11)
	movq	16(%rsp), %r8           # 8-byte Reload
	movl	12(%r8), %ecx
	addl	12(%r12), %ecx
	movl	%ecx, 12(%r11)
	movl	16(%r8), %ecx
	addl	16(%r12), %ecx
	movl	%ecx, 16(%r11)
	movq	8(%rsp), %r10           # 8-byte Reload
	testl	%r10d, %r10d
	jle	.LBB23_20
# BB#9:                                 # %.lr.ph.i.preheader
	movq	24(%r12), %rcx
	cmpl	$8, %r10d
	jb	.LBB23_13
# BB#10:                                # %min.iters.checked
	movq	%r10, %rdx
	andq	$-8, %rdx
	je	.LBB23_13
# BB#11:                                # %vector.memcheck
	leaq	(%rcx,%r10,4), %rsi
	cmpq	%rsi, %rax
	jae	.LBB23_28
# BB#12:                                # %vector.memcheck
	leaq	(%rax,%r10,4), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB23_28
.LBB23_13:
	xorl	%edx, %edx
	movq	%rax, %rsi
.LBB23_14:                              # %.lr.ph.i.preheader83
	movl	%r10d, %ebp
	subl	%edx, %ebp
	leaq	-1(%r10), %rdi
	subq	%rdx, %rdi
	andq	$7, %rbp
	je	.LBB23_17
# BB#15:                                # %.lr.ph.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB23_16:                              # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %ebx
	addq	$4, %rcx
	movl	%ebx, (%rsi)
	addq	$4, %rsi
	incq	%rdx
	incq	%rbp
	jne	.LBB23_16
.LBB23_17:                              # %.lr.ph.i.prol.loopexit
	cmpq	$7, %rdi
	jb	.LBB23_20
# BB#18:                                # %.lr.ph.i.preheader83.new
	movq	%r10, %rdi
	subq	%rdx, %rdi
	.p2align	4, 0x90
.LBB23_19:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	movl	%edx, (%rsi)
	movl	4(%rcx), %edx
	movl	%edx, 4(%rsi)
	movl	8(%rcx), %edx
	movl	%edx, 8(%rsi)
	movl	12(%rcx), %edx
	movl	%edx, 12(%rsi)
	movl	16(%rcx), %edx
	movl	%edx, 16(%rsi)
	movl	20(%rcx), %edx
	movl	%edx, 20(%rsi)
	movl	24(%rcx), %edx
	movl	%edx, 24(%rsi)
	movl	28(%rcx), %edx
	movl	%edx, 28(%rsi)
	addq	$32, %rcx
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB23_19
.LBB23_20:                              # %intcpy.exit
	testl	%r15d, %r15d
	jle	.LBB23_52
# BB#21:                                # %.lr.ph.i32.preheader
	movq	24(%r8), %rcx
	leaq	(%rax,%r10,4), %rdx
	cmpl	$8, %r15d
	jae	.LBB23_23
# BB#22:
	xorl	%esi, %esi
	jmp	.LBB23_46
.LBB23_23:                              # %min.iters.checked49
	movq	%r15, %rsi
	andq	$-8, %rsi
	je	.LBB23_27
# BB#24:                                # %vector.memcheck62
	leaq	(,%r10,4), %r8
	leaq	(%rcx,%r15,4), %rdi
	cmpq	%rdi, %rdx
	jae	.LBB23_31
# BB#25:                                # %vector.memcheck62
	leaq	(%r8,%r15,4), %rdi
	addq	%rax, %rdi
	cmpq	%rdi, %rcx
	jae	.LBB23_31
.LBB23_27:
	xorl	%esi, %esi
.LBB23_46:                              # %.lr.ph.i32.preheader82
	movl	%r15d, %edi
	subl	%esi, %edi
	leaq	-1(%r15), %rax
	subq	%rsi, %rax
	andq	$7, %rdi
	je	.LBB23_49
# BB#47:                                # %.lr.ph.i32.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB23_48:                              # %.lr.ph.i32.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %ebp
	addq	$4, %rcx
	movl	%ebp, (%rdx)
	addq	$4, %rdx
	incq	%rsi
	incq	%rdi
	jne	.LBB23_48
.LBB23_49:                              # %.lr.ph.i32.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB23_52
# BB#50:                                # %.lr.ph.i32.preheader82.new
	subq	%rsi, %r15
	.p2align	4, 0x90
.LBB23_51:                              # %.lr.ph.i32
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %eax
	movl	%eax, (%rdx)
	movl	4(%rcx), %eax
	movl	%eax, 4(%rdx)
	movl	8(%rcx), %eax
	movl	%eax, 8(%rdx)
	movl	12(%rcx), %eax
	movl	%eax, 12(%rdx)
	movl	16(%rcx), %eax
	movl	%eax, 16(%rdx)
	movl	20(%rcx), %eax
	movl	%eax, 20(%rdx)
	movl	24(%rcx), %eax
	movl	%eax, 24(%rdx)
	movl	28(%rcx), %eax
	movl	%eax, 28(%rdx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %r15
	jne	.LBB23_51
.LBB23_52:                              # %intcpy.exit33
	movq	%r11, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_28:                              # %vector.body.preheader
	leaq	-8(%rdx), %rsi
	movl	%esi, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB23_34
# BB#29:                                # %vector.body.prol.preheader
	negq	%rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB23_30:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rbp,4), %xmm0
	movups	16(%rcx,%rbp,4), %xmm1
	movups	%xmm0, (%rax,%rbp,4)
	movups	%xmm1, 16(%rax,%rbp,4)
	addq	$8, %rbp
	incq	%rdi
	jne	.LBB23_30
	jmp	.LBB23_35
.LBB23_31:                              # %vector.body45.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB23_40
# BB#32:                                # %vector.body45.prol.preheader
	leaq	16(%rax,%r10,4), %rbp
	negq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB23_33:                              # %vector.body45.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rbx,4), %xmm0
	movups	16(%rcx,%rbx,4), %xmm1
	movups	%xmm0, -16(%rbp,%rbx,4)
	movups	%xmm1, (%rbp,%rbx,4)
	addq	$8, %rbx
	incq	%rdi
	jne	.LBB23_33
	jmp	.LBB23_41
.LBB23_34:
	xorl	%ebp, %ebp
.LBB23_35:                              # %vector.body.prol.loopexit
	cmpq	$24, %rsi
	jb	.LBB23_38
# BB#36:                                # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rbp, %rsi
	leaq	112(%rcx,%rbp,4), %rdi
	leaq	112(%rax,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB23_37:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rdi
	subq	$-128, %rbp
	addq	$-32, %rsi
	jne	.LBB23_37
.LBB23_38:                              # %middle.block
	cmpq	%rdx, %r10
	je	.LBB23_20
# BB#39:
	leaq	(%rax,%rdx,4), %rsi
	leaq	(%rcx,%rdx,4), %rcx
	jmp	.LBB23_14
.LBB23_40:
	xorl	%ebx, %ebx
.LBB23_41:                              # %vector.body45.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB23_44
# BB#42:                                # %vector.body45.preheader.new
	movq	%rsi, %rbp
	subq	%rbx, %rbp
	leaq	112(%rcx,%rbx,4), %rdi
	leaq	(%r8,%rbx,4), %rbx
	leaq	112(%rax,%rbx), %rax
	.p2align	4, 0x90
.LBB23_43:                              # %vector.body45
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rax)
	movups	%xmm1, -96(%rax)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rax)
	movups	%xmm1, -64(%rax)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rax)
	movups	%xmm1, -32(%rax)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	subq	$-128, %rdi
	subq	$-128, %rax
	addq	$-32, %rbp
	jne	.LBB23_43
.LBB23_44:                              # %middle.block46
	cmpq	%rsi, %r15
	je	.LBB23_52
# BB#45:
	leaq	(%rdx,%rsi,4), %rdx
	leaq	(%rcx,%rsi,4), %rcx
	jmp	.LBB23_46
.Lfunc_end23:
	.size	sf_join, .Lfunc_end23-sf_join
	.cfi_endproc

	.globl	sf_append
	.p2align	4, 0x90
	.type	sf_append,@function
sf_append:                              # @sf_append
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi129:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi130:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi132:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi133:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi135:
	.cfi_def_cfa_offset 64
.Lcfi136:
	.cfi_offset %rbx, -56
.Lcfi137:
	.cfi_offset %r12, -48
.Lcfi138:
	.cfi_offset %r13, -40
.Lcfi139:
	.cfi_offset %r14, -32
.Lcfi140:
	.cfi_offset %r15, -24
.Lcfi141:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	12(%r15), %r13
	movslq	(%r15), %r12
	movslq	12(%r14), %rax
	movslq	(%r14), %rbp
	imulq	%rax, %rbp
	movl	4(%r15), %ecx
	cmpl	4(%r14), %ecx
	jne	.LBB24_2
# BB#1:
	movl	%r13d, %ecx
	jmp	.LBB24_3
.LBB24_2:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	fatal
	movl	12(%r15), %ecx
	movl	12(%r14), %eax
.LBB24_3:
	addl	%ecx, %eax
	movl	%eax, 8(%r15)
	movq	24(%r15), %rdi
	cltq
	testq	%rdi, %rdi
	je	.LBB24_5
# BB#4:
	movslq	(%r15), %rsi
	imulq	%rax, %rsi
	shlq	$2, %rsi
	callq	realloc
	jmp	.LBB24_6
.LBB24_5:
	movslq	(%r15), %rdi
	imulq	%rax, %rdi
	shlq	$2, %rdi
	callq	malloc
.LBB24_6:
	movq	%rax, 24(%r15)
	movq	24(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB24_27
# BB#7:                                 # %.lr.ph.i.preheader
	imulq	%r13, %r12
	leaq	(%rax,%r12,4), %rcx
	cmpl	$8, %ebp
	jb	.LBB24_8
# BB#9:                                 # %min.iters.checked
	movq	%rbp, %rdx
	andq	$-8, %rdx
	je	.LBB24_8
# BB#10:                                # %vector.memcheck
	leaq	(,%r12,4), %r9
	leaq	(%rdi,%rbp,4), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB24_12
# BB#11:                                # %vector.memcheck
	leaq	(%r9,%rbp,4), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %rdi
	jae	.LBB24_12
.LBB24_8:
	xorl	%edx, %edx
	movq	%rdi, %rax
.LBB24_21:                              # %.lr.ph.i.preheader39
	movl	%ebp, %ebx
	subl	%edx, %ebx
	leaq	-1(%rbp), %r8
	subq	%rdx, %r8
	andq	$7, %rbx
	je	.LBB24_24
# BB#22:                                # %.lr.ph.i.prol.preheader
	negq	%rbx
	.p2align	4, 0x90
.LBB24_23:                              # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %esi
	addq	$4, %rax
	movl	%esi, (%rcx)
	addq	$4, %rcx
	incq	%rdx
	incq	%rbx
	jne	.LBB24_23
.LBB24_24:                              # %.lr.ph.i.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB24_27
# BB#25:                                # %.lr.ph.i.preheader39.new
	subq	%rdx, %rbp
	.p2align	4, 0x90
.LBB24_26:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	movl	%edx, (%rcx)
	movl	4(%rax), %edx
	movl	%edx, 4(%rcx)
	movl	8(%rax), %edx
	movl	%edx, 8(%rcx)
	movl	12(%rax), %edx
	movl	%edx, 12(%rcx)
	movl	16(%rax), %edx
	movl	%edx, 16(%rcx)
	movl	20(%rax), %edx
	movl	%edx, 20(%rcx)
	movl	24(%rax), %edx
	movl	%edx, 24(%rcx)
	movl	28(%rax), %edx
	movl	%edx, 28(%rcx)
	addq	$32, %rax
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB24_26
.LBB24_27:                              # %intcpy.exit
	movl	12(%r14), %eax
	addl	%eax, 12(%r15)
	movl	16(%r14), %eax
	addl	%eax, 16(%r15)
	testq	%rdi, %rdi
	je	.LBB24_29
# BB#28:
	callq	free
	movq	$0, 24(%r14)
.LBB24_29:                              # %sf_free.exit
	movq	set_family_garbage(%rip), %rax
	movq	%rax, 32(%r14)
	movq	%r14, set_family_garbage(%rip)
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_12:                              # %vector.body.preheader
	leaq	-8(%rdx), %r8
	movl	%r8d, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB24_13
# BB#14:                                # %vector.body.prol.preheader
	leaq	16(%rax,%r12,4), %r10
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB24_15:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, -16(%r10,%rbx,4)
	movups	%xmm1, (%r10,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB24_15
	jmp	.LBB24_16
.LBB24_13:
	xorl	%ebx, %ebx
.LBB24_16:                              # %vector.body.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB24_19
# BB#17:                                # %vector.body.preheader.new
	movq	%rdx, %r8
	subq	%rbx, %r8
	leaq	112(%rdi,%rbx,4), %rsi
	leaq	(%r9,%rbx,4), %rbx
	leaq	112(%rax,%rbx), %rax
	.p2align	4, 0x90
.LBB24_18:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rax)
	movups	%xmm1, -96(%rax)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rax)
	movups	%xmm1, -64(%rax)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rax)
	movups	%xmm1, -32(%rax)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	subq	$-128, %rsi
	subq	$-128, %rax
	addq	$-32, %r8
	jne	.LBB24_18
.LBB24_19:                              # %middle.block
	cmpq	%rdx, %rbp
	je	.LBB24_27
# BB#20:
	leaq	(%rcx,%rdx,4), %rcx
	leaq	(%rdi,%rdx,4), %rax
	jmp	.LBB24_21
.Lfunc_end24:
	.size	sf_append, .Lfunc_end24-sf_append
	.cfi_endproc

	.globl	sf_new
	.p2align	4, 0x90
	.type	sf_new,@function
sf_new:                                 # @sf_new
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 32
.Lcfi145:
	.cfi_offset %rbx, -32
.Lcfi146:
	.cfi_offset %r14, -24
.Lcfi147:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movl	%edi, %r14d
	movq	set_family_garbage(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB25_1
# BB#2:
	movq	32(%rbx), %rax
	movq	%rax, set_family_garbage(%rip)
	jmp	.LBB25_3
.LBB25_1:
	movl	$40, %edi
	callq	malloc
	movq	%rax, %rbx
.LBB25_3:
	movl	%ebp, 4(%rbx)
	movl	$2, %eax
	cmpl	$33, %ebp
	jl	.LBB25_5
# BB#4:
	decl	%ebp
	sarl	$5, %ebp
	addl	$2, %ebp
	movl	%ebp, %eax
.LBB25_5:
	movl	%eax, (%rbx)
	movl	%r14d, 8(%rbx)
	movslq	%r14d, %rcx
	movslq	%eax, %rdi
	imulq	%rcx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, 24(%rbx)
	movq	$0, 12(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end25:
	.size	sf_new, .Lfunc_end25-sf_new
	.cfi_endproc

	.globl	sf_save
	.p2align	4, 0x90
	.type	sf_save,@function
sf_save:                                # @sf_save
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi148:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi149:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi150:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi152:
	.cfi_def_cfa_offset 48
.Lcfi153:
	.cfi_offset %rbx, -40
.Lcfi154:
	.cfi_offset %r14, -32
.Lcfi155:
	.cfi_offset %r15, -24
.Lcfi156:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	4(%rbx), %ebp
	movl	12(%rbx), %r15d
	movq	set_family_garbage(%rip), %r14
	testq	%r14, %r14
	je	.LBB26_2
# BB#1:
	movq	32(%r14), %rax
	movq	%rax, set_family_garbage(%rip)
	jmp	.LBB26_3
.LBB26_2:
	movl	$40, %edi
	callq	malloc
	movq	%rax, %r14
.LBB26_3:
	movslq	%r15d, %rax
	movl	%ebp, 4(%r14)
	movl	$2, %ecx
	cmpl	$33, %ebp
	jl	.LBB26_5
# BB#4:
	decl	%ebp
	sarl	$5, %ebp
	addl	$2, %ebp
	movl	%ebp, %ecx
.LBB26_5:                               # %sf_new.exit
	movl	%ecx, (%r14)
	movl	%eax, 8(%r14)
	movslq	%ecx, %rdi
	imulq	%rax, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, 24(%r14)
	movq	$0, 12(%r14)
	movl	4(%rbx), %ecx
	movl	%ecx, 4(%r14)
	movl	(%rbx), %ecx
	movl	%ecx, (%r14)
	movl	12(%rbx), %ecx
	movl	%ecx, 12(%r14)
	movl	16(%rbx), %ecx
	movl	%ecx, 16(%r14)
	movslq	(%rbx), %rcx
	movslq	12(%rbx), %rdi
	imulq	%rcx, %rdi
	testq	%rdi, %rdi
	jle	.LBB26_28
# BB#6:                                 # %.lr.ph.i.preheader.i
	movq	24(%rbx), %rdx
	cmpq	$8, %rdi
	jae	.LBB26_8
# BB#7:
	xorl	%esi, %esi
	jmp	.LBB26_22
.LBB26_8:                               # %min.iters.checked
	movq	%rdi, %rsi
	andq	$-8, %rsi
	je	.LBB26_12
# BB#9:                                 # %vector.memcheck
	leaq	(%rdx,%rdi,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB26_13
# BB#10:                                # %vector.memcheck
	leaq	(%rax,%rdi,4), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB26_13
.LBB26_12:
	xorl	%esi, %esi
.LBB26_22:                              # %.lr.ph.i.i.preheader
	movl	%edi, %ecx
	subl	%esi, %ecx
	leaq	-1(%rdi), %rbx
	subq	%rsi, %rbx
	andq	$7, %rcx
	je	.LBB26_25
# BB#23:                                # %.lr.ph.i.i.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB26_24:                              # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx), %ebp
	addq	$4, %rdx
	movl	%ebp, (%rax)
	addq	$4, %rax
	incq	%rsi
	incq	%rcx
	jne	.LBB26_24
.LBB26_25:                              # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rbx
	jb	.LBB26_28
# BB#26:                                # %.lr.ph.i.i.preheader.new
	subq	%rsi, %rdi
	.p2align	4, 0x90
.LBB26_27:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx), %ecx
	movl	%ecx, (%rax)
	movl	4(%rdx), %ecx
	movl	%ecx, 4(%rax)
	movl	8(%rdx), %ecx
	movl	%ecx, 8(%rax)
	movl	12(%rdx), %ecx
	movl	%ecx, 12(%rax)
	movl	16(%rdx), %ecx
	movl	%ecx, 16(%rax)
	movl	20(%rdx), %ecx
	movl	%ecx, 20(%rax)
	movl	24(%rdx), %ecx
	movl	%ecx, 24(%rax)
	movl	28(%rdx), %ecx
	movl	%ecx, 28(%rax)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rdi
	jne	.LBB26_27
.LBB26_28:                              # %sf_copy.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB26_13:                              # %vector.body.preheader
	leaq	-8(%rsi), %rbp
	movl	%ebp, %ecx
	shrl	$3, %ecx
	incl	%ecx
	andq	$3, %rcx
	je	.LBB26_16
# BB#14:                                # %vector.body.prol.preheader
	negq	%rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB26_15:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdx,%rbx,4), %xmm0
	movups	16(%rdx,%rbx,4), %xmm1
	movups	%xmm0, (%rax,%rbx,4)
	movups	%xmm1, 16(%rax,%rbx,4)
	addq	$8, %rbx
	incq	%rcx
	jne	.LBB26_15
	jmp	.LBB26_17
.LBB26_16:
	xorl	%ebx, %ebx
.LBB26_17:                              # %vector.body.prol.loopexit
	cmpq	$24, %rbp
	jb	.LBB26_20
# BB#18:                                # %vector.body.preheader.new
	movq	%rsi, %rcx
	subq	%rbx, %rcx
	leaq	112(%rdx,%rbx,4), %rbp
	leaq	112(%rax,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB26_19:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-32, %rcx
	jne	.LBB26_19
.LBB26_20:                              # %middle.block
	cmpq	%rsi, %rdi
	je	.LBB26_28
# BB#21:
	leaq	(%rax,%rsi,4), %rax
	leaq	(%rdx,%rsi,4), %rdx
	jmp	.LBB26_22
.Lfunc_end26:
	.size	sf_save, .Lfunc_end26-sf_save
	.cfi_endproc

	.globl	sf_free
	.p2align	4, 0x90
	.type	sf_free,@function
sf_free:                                # @sf_free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 16
.Lcfi158:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_2
# BB#1:
	callq	free
	movq	$0, 24(%rbx)
.LBB27_2:
	movq	set_family_garbage(%rip), %rax
	movq	%rax, 32(%rbx)
	movq	%rbx, set_family_garbage(%rip)
	popq	%rbx
	retq
.Lfunc_end27:
	.size	sf_free, .Lfunc_end27-sf_free
	.cfi_endproc

	.globl	sf_cleanup
	.p2align	4, 0x90
	.type	sf_cleanup,@function
sf_cleanup:                             # @sf_cleanup
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi159:
	.cfi_def_cfa_offset 16
.Lcfi160:
	.cfi_offset %rbx, -16
	movq	set_family_garbage(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB28_2
	.p2align	4, 0x90
.LBB28_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB28_1
.LBB28_2:                               # %._crit_edge
	movq	$0, set_family_garbage(%rip)
	popq	%rbx
	retq
.Lfunc_end28:
	.size	sf_cleanup, .Lfunc_end28-sf_cleanup
	.cfi_endproc

	.globl	sf_addset
	.p2align	4, 0x90
	.type	sf_addset,@function
sf_addset:                              # @sf_addset
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi163:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi164:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi165:
	.cfi_def_cfa_offset 48
.Lcfi166:
	.cfi_offset %rbx, -40
.Lcfi167:
	.cfi_offset %r12, -32
.Lcfi168:
	.cfi_offset %r14, -24
.Lcfi169:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	8(%r14), %eax
	movl	12(%r14), %r15d
	cmpl	%eax, %r15d
	jge	.LBB29_2
# BB#1:                                 # %._crit_edge
	movq	24(%r14), %rax
	jmp	.LBB29_6
.LBB29_2:
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	leal	1(%rax,%rcx), %eax
	movl	%eax, 8(%r14)
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	movslq	(%r14), %rcx
	je	.LBB29_4
# BB#3:
	movslq	%eax, %rsi
	imulq	%rcx, %rsi
	shlq	$2, %rsi
	callq	realloc
	movl	12(%r14), %r15d
	jmp	.LBB29_5
.LBB29_4:
	movslq	%eax, %rdi
	imulq	%rcx, %rdi
	shlq	$2, %rdi
	callq	malloc
.LBB29_5:
	movq	%rax, 24(%r14)
.LBB29_6:
	movslq	(%r14), %rcx
	leal	1(%r15), %edx
	movl	%edx, 12(%r14)
	movslq	%r15d, %r11
	imulq	%rcx, %r11
	leaq	(%rax,%r11,4), %rcx
	movl	(%rbx), %edx
	andl	$1023, %edx             # imm = 0x3FF
	leaq	1(%rdx), %r9
	cmpq	$8, %r9
	jb	.LBB29_20
# BB#7:                                 # %min.iters.checked
	movq	%r9, %r8
	andq	$2040, %r8              # imm = 0x7F8
	je	.LBB29_20
# BB#8:                                 # %vector.memcheck
	addq	%rdx, %r11
	leaq	4(%rbx,%rdx,4), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB29_10
# BB#9:                                 # %vector.memcheck
	leaq	4(%rax,%r11,4), %rsi
	cmpq	%rbx, %rsi
	ja	.LBB29_20
.LBB29_10:                              # %vector.body.preheader
	leaq	-8(%r8), %r10
	movl	%r10d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB29_11
# BB#12:                                # %vector.body.prol.preheader
	leaq	-12(%rax,%r11,4), %r15
	leaq	-12(%rbx,%rdx,4), %r12
	negq	%rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB29_13:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r12,%rsi,4), %xmm0
	movups	-16(%r12,%rsi,4), %xmm1
	movups	%xmm0, (%r15,%rsi,4)
	movups	%xmm1, -16(%r15,%rsi,4)
	addq	$-8, %rsi
	incq	%rdi
	jne	.LBB29_13
# BB#14:                                # %vector.body.prol.loopexit.unr-lcssa
	negq	%rsi
	cmpq	$24, %r10
	jae	.LBB29_16
	jmp	.LBB29_18
.LBB29_11:
	xorl	%esi, %esi
	cmpq	$24, %r10
	jb	.LBB29_18
.LBB29_16:                              # %vector.body.preheader.new
	movq	%r9, %r10
	andq	$-8, %r10
	negq	%r10
	negq	%rsi
	leaq	(%rax,%r11,4), %rax
	leaq	-12(%rbx,%rdx,4), %rdi
	.p2align	4, 0x90
.LBB29_17:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rsi,4), %xmm0
	movups	-16(%rdi,%rsi,4), %xmm1
	movups	%xmm0, -12(%rax,%rsi,4)
	movups	%xmm1, -28(%rax,%rsi,4)
	movups	-32(%rdi,%rsi,4), %xmm0
	movups	-48(%rdi,%rsi,4), %xmm1
	movups	%xmm0, -44(%rax,%rsi,4)
	movups	%xmm1, -60(%rax,%rsi,4)
	movups	-64(%rdi,%rsi,4), %xmm0
	movups	-80(%rdi,%rsi,4), %xmm1
	movups	%xmm0, -76(%rax,%rsi,4)
	movups	%xmm1, -92(%rax,%rsi,4)
	movups	-96(%rdi,%rsi,4), %xmm0
	movups	-112(%rdi,%rsi,4), %xmm1
	movups	%xmm0, -108(%rax,%rsi,4)
	movups	%xmm1, -124(%rax,%rsi,4)
	addq	$-32, %rsi
	cmpq	%rsi, %r10
	jne	.LBB29_17
.LBB29_18:                              # %middle.block
	cmpq	%r8, %r9
	je	.LBB29_22
# BB#19:
	subq	%r8, %rdx
.LBB29_20:                              # %scalar.ph.preheader
	incq	%rdx
	.p2align	4, 0x90
.LBB29_21:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rbx,%rdx,4), %eax
	movl	%eax, -4(%rcx,%rdx,4)
	decq	%rdx
	jg	.LBB29_21
.LBB29_22:                              # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end29:
	.size	sf_addset, .Lfunc_end29-sf_addset
	.cfi_endproc

	.globl	sf_delset
	.p2align	4, 0x90
	.type	sf_delset,@function
sf_delset:                              # @sf_delset
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %r8
	movslq	(%rdi), %rax
	movslq	%esi, %r9
	imulq	%rax, %r9
	movslq	12(%rdi), %r11
	decq	%r11
	movl	%r11d, 12(%rdi)
	imulq	%rax, %r11
	movl	(%r8,%r11,4), %edi
	movl	%edi, %r10d
	andl	$1023, %r10d            # imm = 0x3FF
	incl	%edi
	andq	$7, %rdi
	movq	%r10, %rdx
	je	.LBB30_3
# BB#1:                                 # %.prol.preheader
	leaq	(%r8,%r9,4), %rcx
	leaq	(%r8,%r11,4), %rax
	negq	%rdi
	movq	%r10, %rdx
	.p2align	4, 0x90
.LBB30_2:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rdx,4), %esi
	movl	%esi, (%rcx,%rdx,4)
	decq	%rdx
	incq	%rdi
	jne	.LBB30_2
.LBB30_3:                               # %.prol.loopexit
	cmpl	$7, %r10d
	jb	.LBB30_6
# BB#4:                                 # %.new
	addq	$8, %rdx
	leaq	-28(%r8,%r9,4), %rax
	leaq	-28(%r8,%r11,4), %rcx
	.p2align	4, 0x90
.LBB30_5:                               # =>This Inner Loop Header: Depth=1
	movl	-4(%rcx,%rdx,4), %esi
	movl	%esi, -4(%rax,%rdx,4)
	movl	-8(%rcx,%rdx,4), %esi
	movl	%esi, -8(%rax,%rdx,4)
	movl	-12(%rcx,%rdx,4), %esi
	movl	%esi, -12(%rax,%rdx,4)
	movl	-16(%rcx,%rdx,4), %esi
	movl	%esi, -16(%rax,%rdx,4)
	movl	-20(%rcx,%rdx,4), %esi
	movl	%esi, -20(%rax,%rdx,4)
	movl	-24(%rcx,%rdx,4), %esi
	movl	%esi, -24(%rax,%rdx,4)
	movl	-28(%rcx,%rdx,4), %esi
	movl	%esi, -28(%rax,%rdx,4)
	movl	-32(%rcx,%rdx,4), %esi
	movl	%esi, -32(%rax,%rdx,4)
	addq	$-8, %rdx
	cmpq	$7, %rdx
	jg	.LBB30_5
.LBB30_6:                               # %set_copy.exit
	retq
.Lfunc_end30:
	.size	sf_delset, .Lfunc_end30-sf_delset
	.cfi_endproc

	.globl	sf_print
	.p2align	4, 0x90
	.type	sf_print,@function
sf_print:                               # @sf_print
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi170:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi171:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi172:
	.cfi_def_cfa_offset 32
.Lcfi173:
	.cfi_offset %rbx, -32
.Lcfi174:
	.cfi_offset %r14, -24
.Lcfi175:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, 12(%r14)
	jle	.LBB31_3
# BB#1:                                 # %.lr.ph
	movq	24(%r14), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB31_2:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	ps1
	movl	$.L.str.2, %edi
	movl	$s1, %edx
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	incl	%ebp
	cmpl	12(%r14), %ebp
	jl	.LBB31_2
.LBB31_3:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end31:
	.size	sf_print, .Lfunc_end31-sf_print
	.cfi_endproc

	.globl	sf_bm_print
	.p2align	4, 0x90
	.type	sf_bm_print,@function
sf_bm_print:                            # @sf_bm_print
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi176:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi177:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi178:
	.cfi_def_cfa_offset 32
.Lcfi179:
	.cfi_offset %rbx, -32
.Lcfi180:
	.cfi_offset %r14, -24
.Lcfi181:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, 12(%r14)
	jle	.LBB32_10
# BB#1:                                 # %.lr.ph
	movq	24(%r14), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB32_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_8 Depth 2
	movslq	4(%r14), %r8
	testq	%r8, %r8
	jle	.LBB32_9
# BB#3:                                 # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB32_2 Depth=1
	movl	%r8d, %ecx
	testb	$1, %cl
	jne	.LBB32_5
# BB#4:                                 #   in Loop: Header=BB32_2 Depth=1
	xorl	%edx, %edx
	cmpl	$1, %ecx
	jne	.LBB32_7
	jmp	.LBB32_9
	.p2align	4, 0x90
.LBB32_5:                               # %.lr.ph.i.prol
                                        #   in Loop: Header=BB32_2 Depth=1
	movb	4(%rbx), %dl
	andb	$1, %dl
	orb	$48, %dl
	movb	%dl, s1(%rip)
	movl	$1, %edx
	cmpl	$1, %ecx
	je	.LBB32_9
.LBB32_7:                               # %.lr.ph.preheader.i.new
                                        #   in Loop: Header=BB32_2 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB32_8:                               # %.lr.ph.i
                                        #   Parent Loop BB32_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rdx,%rsi), %rdi
	movl	%edi, %eax
	sarl	$5, %eax
	cltq
	movl	4(%rbx,%rax,4), %eax
	btl	%edi, %eax
	setb	%al
	orb	$48, %al
	movb	%al, s1(%rdx,%rsi)
	incl	%edi
	movl	%edi, %eax
	sarl	$5, %eax
	cltq
	movl	4(%rbx,%rax,4), %eax
	btl	%edi, %eax
	setb	%al
	orb	$48, %al
	movb	%al, s1+1(%rdx,%rsi)
	leaq	2(%rdx,%rsi), %rax
	addq	$2, %rsi
	cmpq	%rcx, %rax
	jne	.LBB32_8
.LBB32_9:                               # %pbv1.exit
                                        #   in Loop: Header=BB32_2 Depth=1
	movb	$0, s1(%r8)
	movl	$.L.str.3, %edi
	movl	$s1, %edx
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	incl	%ebp
	cmpl	12(%r14), %ebp
	jl	.LBB32_2
.LBB32_10:                              # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end32:
	.size	sf_bm_print, .Lfunc_end32-sf_bm_print
	.cfi_endproc

	.globl	sf_write
	.p2align	4, 0x90
	.type	sf_write,@function
sf_write:                               # @sf_write
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi182:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi183:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi184:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi185:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi186:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi187:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi188:
	.cfi_def_cfa_offset 64
.Lcfi189:
	.cfi_offset %rbx, -56
.Lcfi190:
	.cfi_offset %r12, -48
.Lcfi191:
	.cfi_offset %r13, -40
.Lcfi192:
	.cfi_offset %r14, -32
.Lcfi193:
	.cfi_offset %r15, -24
.Lcfi194:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movl	4(%rbx), %ecx
	movl	12(%rbx), %edx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	callq	fprintf
	movslq	(%rbx), %rcx
	movq	%rbx, (%rsp)            # 8-byte Spill
	movslq	12(%rbx), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB33_9
# BB#1:                                 # %.lr.ph.preheader
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	24(%rcx), %r15
	leaq	(%r15,%rax,4), %r12
	.p2align	4, 0x90
.LBB33_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_3 Depth 2
	movl	(%r15), %edx
	movl	%edx, %ebx
	andl	$1023, %ebx             # imm = 0x3FF
	xorl	%ebp, %ebp
	jmp	.LBB33_3
	.p2align	4, 0x90
.LBB33_6:                               # %.backedge._crit_edge.i
                                        #   in Loop: Header=BB33_3 Depth=2
	movl	4(%r15,%rbp,4), %edx
	movq	%r14, %rbp
.LBB33_3:                               #   Parent Loop BB33_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	leaq	1(%rbp), %r14
	cmpq	%rbp, %rbx
	je	.LBB33_5
# BB#4:                                 #   in Loop: Header=BB33_3 Depth=2
	movl	%r14d, %eax
	andl	$7, %eax
	testq	%rax, %rax
	jne	.LBB33_5
# BB#7:                                 #   in Loop: Header=BB33_3 Depth=2
	movl	$.L.str.7, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
.LBB33_5:                               # %.backedge.i
                                        #   in Loop: Header=BB33_3 Depth=2
	cmpq	%rbp, %rbx
	jne	.LBB33_6
# BB#8:                                 # %set_write.exit
                                        #   in Loop: Header=BB33_2 Depth=1
	movl	$10, %edi
	movq	%r13, %rsi
	callq	fputc
	movq	(%rsp), %rax            # 8-byte Reload
	movslq	(%rax), %rax
	leaq	(%r15,%rax,4), %r15
	cmpq	%r12, %r15
	jb	.LBB33_2
.LBB33_9:                               # %._crit_edge
	movq	%r13, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fflush                  # TAILCALL
.Lfunc_end33:
	.size	sf_write, .Lfunc_end33-sf_write
	.cfi_endproc

	.globl	sf_read
	.p2align	4, 0x90
	.type	sf_read,@function
sf_read:                                # @sf_read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi195:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi196:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi197:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi198:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi199:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi200:
	.cfi_def_cfa_offset 64
.Lcfi201:
	.cfi_offset %rbx, -48
.Lcfi202:
	.cfi_offset %r12, -40
.Lcfi203:
	.cfi_offset %r14, -32
.Lcfi204:
	.cfi_offset %r15, -24
.Lcfi205:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	leaq	12(%rsp), %rdx
	leaq	8(%rsp), %rcx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	callq	fscanf
	movl	12(%rsp), %ebp
	movl	8(%rsp), %ebx
	movq	set_family_garbage(%rip), %r14
	testq	%r14, %r14
	je	.LBB34_1
# BB#2:
	movq	32(%r14), %rax
	movq	%rax, set_family_garbage(%rip)
	jmp	.LBB34_3
.LBB34_1:
	movl	$40, %edi
	callq	malloc
	movq	%rax, %r14
.LBB34_3:
	movslq	%ebp, %rax
	movl	%ebx, 4(%r14)
	movl	$2, %ecx
	cmpl	$33, %ebx
	jl	.LBB34_5
# BB#4:
	decl	%ebx
	sarl	$5, %ebx
	addl	$2, %ebx
	movl	%ebx, %ecx
.LBB34_5:                               # %sf_new.exit
	movl	%ecx, (%r14)
	movl	%eax, 8(%r14)
	movslq	%ecx, %rdi
	imulq	%rax, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, 24(%r14)
	movl	$0, 16(%r14)
	movslq	12(%rsp), %rcx
	movl	%ecx, 12(%r14)
	movslq	(%r14), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB34_11
# BB#6:                                 # %.lr.ph24.preheader
	leaq	(%rbx,%rax,4), %r12
	movl	$1023, %ebp             # imm = 0x3FF
	.p2align	4, 0x90
.LBB34_7:                               # %.lr.ph24
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB34_9 Depth 2
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	fscanf
	movl	$1, 8(%rsp)
	movzwl	(%rbx), %eax
	testw	$1023, %ax              # imm = 0x3FF
	je	.LBB34_10
# BB#8:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB34_7 Depth=1
	movl	$1, %eax
	.p2align	4, 0x90
.LBB34_9:                               # %.lr.ph
                                        #   Parent Loop BB34_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cltq
	leaq	(%rbx,%rax,4), %rdx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fscanf
	movl	8(%rsp), %eax
	incl	%eax
	movl	%eax, 8(%rsp)
	movl	(%rbx), %ecx
	andl	%ebp, %ecx
	cmpl	%ecx, %eax
	jbe	.LBB34_9
.LBB34_10:                              # %._crit_edge
                                        #   in Loop: Header=BB34_7 Depth=1
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r12, %rbx
	jb	.LBB34_7
.LBB34_11:                              # %._crit_edge25
	movq	%r14, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end34:
	.size	sf_read, .Lfunc_end34-sf_read
	.cfi_endproc

	.globl	set_write
	.p2align	4, 0x90
	.type	set_write,@function
set_write:                              # @set_write
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi206:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi207:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi208:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi209:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi210:
	.cfi_def_cfa_offset 48
.Lcfi211:
	.cfi_offset %rbx, -48
.Lcfi212:
	.cfi_offset %r12, -40
.Lcfi213:
	.cfi_offset %r13, -32
.Lcfi214:
	.cfi_offset %r14, -24
.Lcfi215:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	(%r14), %edx
	movl	%edx, %r12d
	andl	$1023, %r12d            # imm = 0x3FF
	xorl	%ebx, %ebx
	jmp	.LBB35_1
	.p2align	4, 0x90
.LBB35_4:                               # %.backedge._crit_edge
                                        #   in Loop: Header=BB35_1 Depth=1
	movl	4(%r14,%rbx,4), %edx
	movq	%r13, %rbx
.LBB35_1:                               # =>This Inner Loop Header: Depth=1
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	leaq	1(%rbx), %r13
	cmpq	%rbx, %r12
	je	.LBB35_3
# BB#2:                                 #   in Loop: Header=BB35_1 Depth=1
	movl	%r13d, %eax
	andl	$7, %eax
	testq	%rax, %rax
	jne	.LBB35_3
# BB#5:                                 #   in Loop: Header=BB35_1 Depth=1
	movl	$.L.str.7, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
.LBB35_3:                               # %.backedge
                                        #   in Loop: Header=BB35_1 Depth=1
	cmpq	%rbx, %r12
	jne	.LBB35_4
# BB#6:
	movl	$10, %edi
	movq	%r15, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	fputc                   # TAILCALL
.Lfunc_end35:
	.size	set_write, .Lfunc_end35-set_write
	.cfi_endproc

	.globl	sf_bm_read
	.p2align	4, 0x90
	.type	sf_bm_read,@function
sf_bm_read:                             # @sf_bm_read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi216:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi217:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi218:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi219:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi220:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi221:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi222:
	.cfi_def_cfa_offset 64
.Lcfi223:
	.cfi_offset %rbx, -56
.Lcfi224:
	.cfi_offset %r12, -48
.Lcfi225:
	.cfi_offset %r13, -40
.Lcfi226:
	.cfi_offset %r14, -32
.Lcfi227:
	.cfi_offset %r15, -24
.Lcfi228:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	4(%rsp), %rdx
	movq	%rsp, %rcx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	callq	fscanf
	movl	4(%rsp), %r15d
	movl	(%rsp), %ebp
	movq	set_family_garbage(%rip), %r14
	testq	%r14, %r14
	je	.LBB36_1
# BB#2:
	movq	32(%r14), %rax
	movq	%rax, set_family_garbage(%rip)
	jmp	.LBB36_3
.LBB36_1:
	movl	$40, %edi
	callq	malloc
	movq	%rax, %r14
.LBB36_3:
	movslq	%r15d, %rax
	movl	%ebp, 4(%r14)
	movl	$2, %ecx
	cmpl	$33, %ebp
	jl	.LBB36_5
# BB#4:
	decl	%ebp
	sarl	$5, %ebp
	addl	$2, %ebp
	movl	%ebp, %ecx
.LBB36_5:                               # %sf_new.exit
	movl	%ecx, (%r14)
	movl	%eax, 8(%r14)
	movslq	%ecx, %rdi
	imulq	%rax, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, 24(%r14)
	movq	$0, 12(%r14)
	cmpl	$0, 4(%rsp)
	jle	.LBB36_20
# BB#6:                                 # %.lr.ph23.preheader
	xorl	%ecx, %ecx
	xorl	%r12d, %r12d
	jmp	.LBB36_7
	.p2align	4, 0x90
.LBB36_16:                              # %._crit_edge
                                        #   in Loop: Header=BB36_7 Depth=1
	cmpl	$10, %eax
	je	.LBB36_18
# BB#17:                                #   in Loop: Header=BB36_7 Depth=1
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB36_18:                              #   in Loop: Header=BB36_7 Depth=1
	incl	%r12d
	cmpl	4(%rsp), %r12d
	jge	.LBB36_20
# BB#19:                                # %..lr.ph23_crit_edge
                                        #   in Loop: Header=BB36_7 Depth=1
	movq	24(%r14), %rax
	movl	12(%r14), %ecx
.LBB36_7:                               # %.lr.ph23
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB36_11 Depth 2
	movslq	(%r14), %rdx
	leal	1(%rcx), %esi
	movl	%esi, 12(%r14)
	movslq	%ecx, %rcx
	imulq	%rdx, %rcx
	leaq	(%rax,%rcx,4), %r13
	movl	4(%r14), %ecx
	cmpl	$33, %ecx
	movl	$1, %eax
	jl	.LBB36_9
# BB#8:                                 #   in Loop: Header=BB36_7 Depth=1
	decl	%ecx
	sarl	$5, %ecx
	incl	%ecx
	movl	%ecx, %eax
.LBB36_9:                               # %set_clear.exit
                                        #   in Loop: Header=BB36_7 Depth=1
	movl	%eax, (%r13)
	movslq	%eax, %rcx
	movl	%ecx, %edx
	notl	%edx
	cmpl	$-3, %edx
	movl	$-2, %esi
	cmovlel	%esi, %edx
	leal	1(%rax,%rdx), %eax
	subq	%rax, %rcx
	leaq	(%r13,%rcx,4), %rdi
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movl	(%rsp), %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$0, %ebp
	jle	.LBB36_16
# BB#10:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB36_7 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB36_11:                              # %.lr.ph
                                        #   Parent Loop BB36_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$48, %eax
	je	.LBB36_15
# BB#12:                                # %.lr.ph
                                        #   in Loop: Header=BB36_11 Depth=2
	cmpl	$49, %eax
	jne	.LBB36_14
# BB#13:                                #   in Loop: Header=BB36_11 Depth=2
	movl	$1, %eax
	movl	%ebp, %ecx
	shll	%cl, %eax
	movl	%ebp, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%eax, 4(%r13,%rcx,4)
	jmp	.LBB36_15
	.p2align	4, 0x90
.LBB36_14:                              #   in Loop: Header=BB36_11 Depth=2
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB36_15:                              #   in Loop: Header=BB36_11 Depth=2
	incl	%ebp
	movl	(%rsp), %r15d
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	%r15d, %ebp
	jl	.LBB36_11
	jmp	.LBB36_16
.LBB36_20:                              # %._crit_edge24
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end36:
	.size	sf_bm_read, .Lfunc_end36-sf_bm_read
	.cfi_endproc

	.globl	ps1
	.p2align	4, 0x90
	.type	ps1,@function
ps1:                                    # @ps1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi229:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi230:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi231:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi232:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi233:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi234:
	.cfi_def_cfa_offset 56
.Lcfi235:
	.cfi_offset %rbx, -56
.Lcfi236:
	.cfi_offset %r12, -48
.Lcfi237:
	.cfi_offset %r13, -40
.Lcfi238:
	.cfi_offset %r14, -32
.Lcfi239:
	.cfi_offset %r15, -24
.Lcfi240:
	.cfi_offset %rbp, -16
	movl	(%rdi), %r15d
	shll	$5, %r15d
	andl	$32736, %r15d           # imm = 0x7FE0
	movb	$91, s1(%rip)
	je	.LBB37_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%r8d, %r8d
	movl	$1, %esi
	leaq	-24(%rsp), %r12
	movabsq	$4294967296, %r9        # imm = 0x100000000
	pxor	%xmm0, %xmm0
	movl	$1, %r14d
	movl	%r15d, -28(%rsp)        # 4-byte Spill
	.p2align	4, 0x90
.LBB37_3:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_7 Depth 2
                                        #     Child Loop BB37_17 Depth 2
                                        #     Child Loop BB37_22 Depth 2
	movl	%r8d, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%rdi,%rcx,4), %ecx
	btl	%r8d, %ecx
	jae	.LBB37_26
# BB#4:                                 #   in Loop: Header=BB37_3 Depth=1
	testl	%esi, %esi
	jne	.LBB37_6
# BB#5:                                 #   in Loop: Header=BB37_3 Depth=1
	movslq	%r14d, %rcx
	incl	%r14d
	movb	$44, s1(%rcx)
.LBB37_6:                               #   in Loop: Header=BB37_3 Depth=1
	movl	$1, %ebx
	xorl	%esi, %esi
	movq	%r12, %rbp
	movl	%r8d, %edx
	.p2align	4, 0x90
.LBB37_7:                               #   Parent Loop BB37_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %r10d
	movslq	%edx, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rbx
	shrq	$63, %rbx
	sarq	$34, %rcx
	addl	%ebx, %ecx
	leal	(%rcx,%rcx), %ebx
	leal	(%rbx,%rbx,4), %ebx
	negl	%ebx
	leal	48(%rdx,%rbx), %edx
	movb	%dl, (%rbp)
	incq	%rbp
	addq	%r9, %rsi
	leal	1(%r10), %ebx
	cmpl	$9, %eax
	movl	%ecx, %edx
	jg	.LBB37_7
# BB#8:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB37_3 Depth=1
	sarq	$32, %rsi
	movslq	%r14d, %r11
	movq	%rsi, %rax
	notq	%rax
	cmpq	$-3, %rax
	movq	$-2, %rcx
	cmovleq	%rcx, %rax
	leaq	2(%rax,%rsi), %r13
	cmpq	$32, %r13
	jae	.LBB37_10
# BB#9:                                 #   in Loop: Header=BB37_3 Depth=1
	movq	%r11, %r14
	jmp	.LBB37_21
	.p2align	4, 0x90
.LBB37_10:                              # %min.iters.checked
                                        #   in Loop: Header=BB37_3 Depth=1
	movq	%r13, %r15
	andq	$-32, %r15
	movq	%r13, %r12
	andq	$-32, %r12
	je	.LBB37_11
# BB#12:                                # %vector.body.preheader
                                        #   in Loop: Header=BB37_3 Depth=1
	leaq	-32(%r12), %rcx
	movq	%rcx, %rax
	shrq	$5, %rax
	btl	$5, %ecx
	jb	.LBB37_13
# BB#14:                                # %vector.body.prol
                                        #   in Loop: Header=BB37_3 Depth=1
	movdqu	-56(%rsp,%rsi), %xmm1
	movdqu	-40(%rsp,%rsi), %xmm2
	movdqa	%xmm2, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm1    # xmm1 = xmm1[8],xmm0[8],xmm1[9],xmm0[9],xmm1[10],xmm0[10],xmm1[11],xmm0[11],xmm1[12],xmm0[12],xmm1[13],xmm0[13],xmm1[14],xmm0[14],xmm1[15],xmm0[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqu	%xmm2, s1(%r11)
	movdqu	%xmm1, s1+16(%r11)
	movl	$32, %ecx
	testq	%rax, %rax
	jne	.LBB37_16
	jmp	.LBB37_18
.LBB37_11:                              #   in Loop: Header=BB37_3 Depth=1
	movq	%r11, %r14
	jmp	.LBB37_20
.LBB37_13:                              #   in Loop: Header=BB37_3 Depth=1
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.LBB37_18
.LBB37_16:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB37_3 Depth=1
	movslq	%r10d, %rdx
	movq	%rdx, %rax
	notq	%rax
	cmpq	$-3, %rax
	movq	$-2, %rbp
	cmovleq	%rbp, %rax
	leaq	2(%rax,%rdx), %rax
	andq	$-32, %rax
	subq	%rcx, %rax
	leaq	s1+48(%rcx,%r11), %rbx
	leaq	-40(%rsp), %rbp
	subq	%rcx, %rbp
	addq	%rdx, %rbp
	.p2align	4, 0x90
.LBB37_17:                              # %vector.body
                                        #   Parent Loop BB37_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rbp), %xmm1
	movdqu	(%rbp), %xmm2
	movdqa	%xmm2, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm1    # xmm1 = xmm1[8],xmm0[8],xmm1[9],xmm0[9],xmm1[10],xmm0[10],xmm1[11],xmm0[11],xmm1[12],xmm0[12],xmm1[13],xmm0[13],xmm1[14],xmm0[14],xmm1[15],xmm0[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqu	%xmm2, -48(%rbx)
	movdqu	%xmm1, -32(%rbx)
	movdqu	-48(%rbp), %xmm1
	movdqu	-32(%rbp), %xmm2
	movdqa	%xmm2, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm1    # xmm1 = xmm1[8],xmm0[8],xmm1[9],xmm0[9],xmm1[10],xmm0[10],xmm1[11],xmm0[11],xmm1[12],xmm0[12],xmm1[13],xmm0[13],xmm1[14],xmm0[14],xmm1[15],xmm0[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqu	%xmm2, -16(%rbx)
	movdqu	%xmm1, (%rbx)
	addq	$64, %rbx
	addq	$-64, %rbp
	addq	$-64, %rax
	jne	.LBB37_17
.LBB37_18:                              # %middle.block
                                        #   in Loop: Header=BB37_3 Depth=1
	leaq	(%r11,%r15), %r14
	cmpq	%r12, %r13
	jne	.LBB37_19
# BB#28:                                #   in Loop: Header=BB37_3 Depth=1
	leaq	-1(%r11,%r12), %rcx
	movl	-28(%rsp), %r15d        # 4-byte Reload
	leaq	-24(%rsp), %r12
	cmpl	$105, %ecx
	jl	.LBB37_25
	jmp	.LBB37_29
.LBB37_19:                              #   in Loop: Header=BB37_3 Depth=1
	subq	%r15, %rsi
.LBB37_20:                              # %.preheader.preheader63
                                        #   in Loop: Header=BB37_3 Depth=1
	movl	-28(%rsp), %r15d        # 4-byte Reload
	leaq	-24(%rsp), %r12
.LBB37_21:                              # %.preheader.preheader63
                                        #   in Loop: Header=BB37_3 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB37_22:                              # %.preheader
                                        #   Parent Loop BB37_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-26(%rsp,%rsi), %eax
	movb	%al, s1(%r14)
	incq	%r14
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB37_22
# BB#23:                                # %.loopexit62.loopexit
                                        #   in Loop: Header=BB37_3 Depth=1
	leaq	-1(%r14), %rcx
	cmpl	$105, %ecx
	jge	.LBB37_29
.LBB37_25:                              #   in Loop: Header=BB37_3 Depth=1
	xorl	%esi, %esi
.LBB37_26:                              #   in Loop: Header=BB37_3 Depth=1
	incl	%r8d
	cmpl	%r15d, %r8d
	jl	.LBB37_3
	jmp	.LBB37_27
.LBB37_1:
	movl	$1, %r14d
	jmp	.LBB37_27
.LBB37_29:
	movslq	%r14d, %rax
	movb	$46, s1(%rax)
	movq	%rcx, %rax
	shlq	$32, %rax
	movabsq	$8589934592, %rdx       # imm = 0x200000000
	addq	%rax, %rdx
	sarq	$32, %rdx
	movb	$46, s1(%rdx)
	leal	4(%rcx), %r14d
	movabsq	$12884901888, %rcx      # imm = 0x300000000
	addq	%rax, %rcx
	sarq	$32, %rcx
	movb	$46, s1(%rcx)
.LBB37_27:                              # %.loopexit
	movslq	%r14d, %rax
	movw	$93, s1(%rax)
	movl	$s1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end37:
	.size	ps1, .Lfunc_end37-ps1
	.cfi_endproc

	.globl	pbv1
	.p2align	4, 0x90
	.type	pbv1,@function
pbv1:                                   # @pbv1
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB38_7
# BB#1:                                 # %.lr.ph.preheader
	movl	%esi, %r8d
	testb	$1, %r8b
	jne	.LBB38_3
# BB#2:
	xorl	%r9d, %r9d
	cmpl	$1, %esi
	jne	.LBB38_5
	jmp	.LBB38_7
.LBB38_3:                               # %.lr.ph.prol
	movb	4(%rdi), %cl
	andb	$1, %cl
	orb	$48, %cl
	movb	%cl, s1(%rip)
	movl	$1, %r9d
	cmpl	$1, %esi
	je	.LBB38_7
.LBB38_5:                               # %.lr.ph.preheader.new
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB38_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%r9,%rdx), %rax
	movl	%eax, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%rdi,%rcx,4), %ecx
	btl	%eax, %ecx
	setb	%cl
	orb	$48, %cl
	movb	%cl, s1(%r9,%rdx)
	incl	%eax
	movl	%eax, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%rdi,%rcx,4), %ecx
	btl	%eax, %ecx
	setb	%al
	orb	$48, %al
	movb	%al, s1+1(%r9,%rdx)
	leaq	2(%r9,%rdx), %rax
	addq	$2, %rdx
	cmpq	%r8, %rax
	jne	.LBB38_6
.LBB38_7:                               # %._crit_edge
	movslq	%esi, %rax
	movb	$0, s1(%rax)
	movl	$s1, %eax
	retq
.Lfunc_end38:
	.size	pbv1, .Lfunc_end38-pbv1
	.cfi_endproc

	.globl	set_adjcnt
	.p2align	4, 0x90
	.type	set_adjcnt,@function
set_adjcnt:                             # @set_adjcnt
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	andl	$1023, %eax             # imm = 0x3FF
	je	.LBB39_8
# BB#1:                                 # %.lr.ph21.preheader
	movl	%eax, %ecx
	shll	$5, %ecx
	leaq	-128(%rsi,%rcx,4), %r8
	.p2align	4, 0x90
.LBB39_3:                               # %.lr.ph21
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_5 Depth 2
	movl	(%rdi,%rax,4), %esi
	testl	%esi, %esi
	je	.LBB39_2
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB39_3 Depth=1
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB39_5:                               # %.lr.ph
                                        #   Parent Loop BB39_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	$1, %sil
	je	.LBB39_7
# BB#6:                                 #   in Loop: Header=BB39_5 Depth=2
	addl	%edx, (%rcx)
.LBB39_7:                               #   in Loop: Header=BB39_5 Depth=2
	shrl	%esi
	addq	$4, %rcx
	testl	%esi, %esi
	jne	.LBB39_5
.LBB39_2:                               # %.loopexit
                                        #   in Loop: Header=BB39_3 Depth=1
	addq	$-128, %r8
	cmpq	$2, %rax
	leaq	-1(%rax), %rax
	jge	.LBB39_3
.LBB39_8:                               # %._crit_edge
	retq
.Lfunc_end39:
	.size	set_adjcnt, .Lfunc_end39-set_adjcnt
	.cfi_endproc

	.globl	sf_count
	.p2align	4, 0x90
	.type	sf_count,@function
sf_count:                               # @sf_count
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi241:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi242:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi243:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi244:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi245:
	.cfi_def_cfa_offset 48
.Lcfi246:
	.cfi_offset %rbx, -40
.Lcfi247:
	.cfi_offset %r12, -32
.Lcfi248:
	.cfi_offset %r14, -24
.Lcfi249:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movslq	4(%r15), %r12
	leaq	(,%r12,4), %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r14
	testq	%r12, %r12
	jle	.LBB40_2
# BB#1:                                 # %.lr.ph50.preheader
	addq	$-4, %rbx
	movl	%r12d, %eax
	notl	%eax
	cmpl	$-3, %eax
	movl	$-2, %ecx
	cmovgl	%eax, %ecx
	leal	1(%r12,%rcx), %eax
	leaq	(,%rax,4), %rcx
	subq	%rcx, %rbx
	addq	%r14, %rbx
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	memset
.LBB40_2:                               # %._crit_edge51
	movslq	(%r15), %r9
	movslq	12(%r15), %rax
	imulq	%r9, %rax
	testl	%eax, %eax
	jle	.LBB40_13
# BB#3:                                 # %.lr.ph45.preheader
	movq	24(%r15), %rcx
	leaq	(%rcx,%rax,4), %rdx
	movq	%r14, %r8
	addq	$-128, %r8
	.p2align	4, 0x90
.LBB40_4:                               # %.lr.ph45
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_7 Depth 2
                                        #       Child Loop BB40_9 Depth 3
	movl	(%rcx), %edi
	andl	$1023, %edi             # imm = 0x3FF
	je	.LBB40_12
# BB#5:                                 # %.lr.ph42.preheader
                                        #   in Loop: Header=BB40_4 Depth=1
	movl	%edi, %eax
	shll	$5, %eax
	leaq	(%r8,%rax,4), %rax
	.p2align	4, 0x90
.LBB40_7:                               # %.lr.ph42
                                        #   Parent Loop BB40_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB40_9 Depth 3
	movl	(%rcx,%rdi,4), %esi
	testl	%esi, %esi
	je	.LBB40_6
# BB#8:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB40_7 Depth=2
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB40_9:                               # %.lr.ph
                                        #   Parent Loop BB40_4 Depth=1
                                        #     Parent Loop BB40_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	$1, %sil
	je	.LBB40_11
# BB#10:                                #   in Loop: Header=BB40_9 Depth=3
	incl	(%rbx)
.LBB40_11:                              #   in Loop: Header=BB40_9 Depth=3
	shrl	%esi
	addq	$4, %rbx
	testl	%esi, %esi
	jne	.LBB40_9
.LBB40_6:                               # %.loopexit
                                        #   in Loop: Header=BB40_7 Depth=2
	addq	$-128, %rax
	cmpq	$2, %rdi
	leaq	-1(%rdi), %rdi
	jge	.LBB40_7
.LBB40_12:                              # %._crit_edge
                                        #   in Loop: Header=BB40_4 Depth=1
	leaq	(%rcx,%r9,4), %rcx
	cmpq	%rdx, %rcx
	jb	.LBB40_4
.LBB40_13:                              # %._crit_edge46
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end40:
	.size	sf_count, .Lfunc_end40-sf_count
	.cfi_endproc

	.globl	sf_count_restricted
	.p2align	4, 0x90
	.type	sf_count_restricted,@function
sf_count_restricted:                    # @sf_count_restricted
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi250:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi251:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi252:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi253:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi254:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi255:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi256:
	.cfi_def_cfa_offset 64
.Lcfi257:
	.cfi_offset %rbx, -56
.Lcfi258:
	.cfi_offset %r12, -48
.Lcfi259:
	.cfi_offset %r13, -40
.Lcfi260:
	.cfi_offset %r14, -32
.Lcfi261:
	.cfi_offset %r15, -24
.Lcfi262:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	4(%r15), %r13
	leaq	(,%r13,4), %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%r13, %r13
	jle	.LBB41_2
# BB#1:                                 # %.lr.ph57.preheader
	addq	$-4, %r12
	movl	%r13d, %eax
	notl	%eax
	cmpl	$-3, %eax
	movl	$-2, %ecx
	cmovgl	%eax, %ecx
	leal	1(%r13,%rcx), %eax
	leaq	(,%rax,4), %rcx
	subq	%rcx, %r12
	addq	%rbx, %r12
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
.LBB41_2:                               # %._crit_edge58
	movq	%rbx, %r12
	movslq	(%r15), %r10
	movslq	12(%r15), %rax
	imulq	%r10, %rax
	testl	%eax, %eax
	jle	.LBB41_18
# BB#3:                                 # %.lr.ph52.preheader
	movq	24(%r15), %r11
	leaq	(%r11,%rax,4), %r9
	shlq	$2, %r10
	movq	%r12, %r8
	addq	$-128, %r8
	.p2align	4, 0x90
.LBB41_4:                               # %.lr.ph52
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB41_6 Depth 2
                                        #     Child Loop BB41_12 Depth 2
                                        #       Child Loop BB41_14 Depth 3
	movl	(%r11), %edi
	andl	$1023, %edi             # imm = 0x3FF
	je	.LBB41_17
# BB#5:                                 # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB41_4 Depth=1
	movl	%edi, %eax
	incq	%rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB41_6:                               # %.lr.ph.i
                                        #   Parent Loop BB41_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%r11,%rax,4), %edx
	testl	%edx, %edx
	je	.LBB41_8
# BB#7:                                 #   in Loop: Header=BB41_6 Depth=2
	movzbl	%dl, %ebx
	movzbl	%dh, %ebp  # NOREX
	movl	%edx, %esi
	shrl	$16, %esi
	movzbl	%sil, %esi
	shrl	$24, %edx
	addl	bit_count(,%rbx,4), %ecx
	addl	bit_count(,%rbp,4), %ecx
	addl	bit_count(,%rsi,4), %ecx
	addl	bit_count(,%rdx,4), %ecx
.LBB41_8:                               #   in Loop: Header=BB41_6 Depth=2
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB41_6
# BB#9:                                 # %set_ord.exit
                                        #   in Loop: Header=BB41_4 Depth=1
	decl	%ecx
	movl	$1024, %eax             # imm = 0x400
	xorl	%edx, %edx
	idivl	%ecx
	testl	%edi, %edi
	je	.LBB41_17
# BB#10:                                # %.lr.ph48.preheader
                                        #   in Loop: Header=BB41_4 Depth=1
	movl	%edi, %ecx
	shll	$5, %ecx
	leaq	(%r8,%rcx,4), %rbx
	.p2align	4, 0x90
.LBB41_12:                              # %.lr.ph48
                                        #   Parent Loop BB41_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB41_14 Depth 3
	movl	(%r14,%rdi,4), %edx
	andl	(%r11,%rdi,4), %edx
	je	.LBB41_11
# BB#13:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB41_12 Depth=2
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB41_14:                              # %.lr.ph
                                        #   Parent Loop BB41_4 Depth=1
                                        #     Parent Loop BB41_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	$1, %dl
	je	.LBB41_16
# BB#15:                                #   in Loop: Header=BB41_14 Depth=3
	addl	%eax, (%rcx)
.LBB41_16:                              #   in Loop: Header=BB41_14 Depth=3
	shrl	%edx
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB41_14
.LBB41_11:                              # %.loopexit
                                        #   in Loop: Header=BB41_12 Depth=2
	addq	$-128, %rbx
	cmpq	$2, %rdi
	leaq	-1(%rdi), %rdi
	jge	.LBB41_12
.LBB41_17:                              # %._crit_edge
                                        #   in Loop: Header=BB41_4 Depth=1
	addq	%r10, %r11
	cmpq	%r9, %r11
	jb	.LBB41_4
.LBB41_18:                              # %._crit_edge53
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end41:
	.size	sf_count_restricted, .Lfunc_end41-sf_count_restricted
	.cfi_endproc

	.globl	sf_delc
	.p2align	4, 0x90
	.type	sf_delc,@function
sf_delc:                                # @sf_delc
	.cfi_startproc
# BB#0:
	movl	$1, %eax
	subl	%esi, %eax
	addl	%eax, %edx
	jmp	sf_delcol               # TAILCALL
.Lfunc_end42:
	.size	sf_delc, .Lfunc_end42-sf_delc
	.cfi_endproc

	.globl	sf_addcol
	.p2align	4, 0x90
	.type	sf_addcol,@function
sf_addcol:                              # @sf_addcol
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	cmpl	%esi, 4(%rdi)
	jne	.LBB43_3
# BB#1:
	leal	31(%rsi), %eax
	andl	$-32, %eax
	cmpl	$33, %esi
	movl	$32, %ecx
	cmovgel	%eax, %ecx
	leal	(%rsi,%rdx), %eax
	cmpl	%ecx, %eax
	jle	.LBB43_2
.LBB43_3:
	negl	%edx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	jmp	sf_delcol               # TAILCALL
.LBB43_2:
	movl	%eax, 4(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end43:
	.size	sf_addcol, .Lfunc_end43-sf_addcol
	.cfi_endproc

	.globl	sf_delcol
	.p2align	4, 0x90
	.type	sf_delcol,@function
sf_delcol:                              # @sf_delcol
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi263:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi264:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi265:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi266:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi267:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi268:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi269:
	.cfi_def_cfa_offset 96
.Lcfi270:
	.cfi_offset %rbx, -56
.Lcfi271:
	.cfi_offset %r12, -48
.Lcfi272:
	.cfi_offset %r13, -40
.Lcfi273:
	.cfi_offset %r14, -32
.Lcfi274:
	.cfi_offset %r15, -24
.Lcfi275:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %r13
	movl	4(%r13), %r12d
	movl	12(%r13), %r14d
	subl	%r15d, %r12d
	movq	set_family_garbage(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB44_1
# BB#2:
	movq	32(%rbx), %rcx
	movq	%rcx, set_family_garbage(%rip)
	jmp	.LBB44_3
.LBB44_1:
	movl	$40, %edi
	callq	malloc
	movq	%rax, %rbx
.LBB44_3:
	movslq	%r14d, %rdx
	movl	%r12d, 4(%rbx)
	movl	$2, %ecx
	cmpl	$33, %r12d
	jl	.LBB44_5
# BB#4:
	decl	%r12d
	sarl	$5, %r12d
	addl	$2, %r12d
	movl	%r12d, %ecx
.LBB44_5:                               # %sf_new.exit
	movl	%ecx, (%rbx)
	movl	%edx, 8(%rbx)
	movslq	%ecx, %rdi
	imulq	%rdx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rbx)
	movq	$0, 12(%rbx)
	movslq	(%r13), %rcx
	movslq	12(%r13), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB44_30
# BB#6:                                 # %.lr.ph64
	movq	24(%r13), %r12
	leaq	(%r12,%rax,4), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	testl	%r15d, %r15d
	movl	$0, %ecx
	cmovnsl	%r15d, %ecx
	addl	%ebp, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	negl	%r15d
	testl	%ebp, %ebp
	jle	.LBB44_7
# BB#17:                                # %.lr.ph64.split.us.preheader
	xorl	%eax, %eax
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	jmp	.LBB44_18
	.p2align	4, 0x90
.LBB44_25:                              # %._crit_edge62.us
                                        #   in Loop: Header=BB44_18 Depth=1
	movslq	(%r13), %rax
	leaq	(%r12,%rax,4), %r12
	cmpq	24(%rsp), %r12          # 8-byte Folded Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	jae	.LBB44_30
# BB#26:                                # %._crit_edge62.us..lr.ph64.split.us_crit_edge
                                        #   in Loop: Header=BB44_18 Depth=1
	movl	12(%rbx), %eax
.LBB44_18:                              # %.lr.ph64.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB44_21 Depth 2
                                        #     Child Loop BB44_27 Depth 2
	movslq	(%rbx), %rcx
	leal	1(%rax), %edx
	movl	%edx, 12(%rbx)
	cltq
	imulq	%rcx, %rax
	movl	4(%rbx), %edx
	cmpl	$33, %edx
	movl	$1, %ecx
	jl	.LBB44_20
# BB#19:                                #   in Loop: Header=BB44_18 Depth=1
	decl	%edx
	sarl	$5, %edx
	incl	%edx
	movl	%edx, %ecx
.LBB44_20:                              # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB44_18 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax,4), %r14
	movl	%ecx, (%rsi,%rax,4)
	movslq	%ecx, %rdx
	addq	%rdx, %rax
	notl	%edx
	cmpl	$-3, %edx
	movl	$-2, %edi
	cmovlel	%edi, %edx
	leal	1(%rcx,%rdx), %ecx
	subq	%rcx, %rax
	leaq	(%rsi,%rax,4), %rdi
	leaq	4(,%rcx,4), %rdx
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	callq	memset
	.p2align	4, 0x90
.LBB44_21:                              # %.lr.ph.us
                                        #   Parent Loop BB44_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %eax
	sarl	$5, %eax
	cltq
	movl	4(%r12,%rax,4), %esi
	movl	$1, %edx
	movl	%ebx, %ecx
	shll	%cl, %edx
	andb	$31, %cl
	movzbl	%cl, %ecx
	btl	%ecx, %esi
	jae	.LBB44_23
# BB#22:                                #   in Loop: Header=BB44_21 Depth=2
	incq	%rax
	orl	%edx, (%r14,%rax,4)
.LBB44_23:                              #   in Loop: Header=BB44_21 Depth=2
	incl	%ebx
	cmpl	%ebx, %ebp
	jne	.LBB44_21
# BB#24:                                # %._crit_edge.us
                                        #   in Loop: Header=BB44_18 Depth=1
	movl	4(%r13), %ecx
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	%ecx, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jge	.LBB44_25
	.p2align	4, 0x90
.LBB44_27:                              # %.lr.ph61.us
                                        #   Parent Loop BB44_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	movl	4(%r12,%rdx,4), %edx
	btl	%eax, %edx
	jae	.LBB44_29
# BB#28:                                #   in Loop: Header=BB44_27 Depth=2
	leal	(%r15,%rax), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%edx, 4(%r14,%rcx,4)
	movl	4(%r13), %ecx
.LBB44_29:                              #   in Loop: Header=BB44_27 Depth=2
	incl	%eax
	cmpl	%ecx, %eax
	jl	.LBB44_27
	jmp	.LBB44_25
.LBB44_7:                               # %.lr.ph64.split.preheader
	movl	$-2, %r14d
	jmp	.LBB44_8
	.p2align	4, 0x90
.LBB44_15:                              # %._crit_edge62
                                        #   in Loop: Header=BB44_8 Depth=1
	movslq	(%r13), %rax
	leaq	(%r12,%rax,4), %r12
	cmpq	24(%rsp), %r12          # 8-byte Folded Reload
	jae	.LBB44_30
# BB#16:                                # %._crit_edge62..lr.ph64.split_crit_edge
                                        #   in Loop: Header=BB44_8 Depth=1
	movl	12(%rbx), %eax
.LBB44_8:                               # %.lr.ph64.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB44_12 Depth 2
	movslq	(%rbx), %rcx
	leal	1(%rax), %edx
	movl	%edx, 12(%rbx)
	movslq	%eax, %rbp
	imulq	%rcx, %rbp
	movl	4(%rbx), %ecx
	cmpl	$33, %ecx
	movl	$1, %eax
	jl	.LBB44_10
# BB#9:                                 #   in Loop: Header=BB44_8 Depth=1
	decl	%ecx
	sarl	$5, %ecx
	incl	%ecx
	movl	%ecx, %eax
.LBB44_10:                              # %.preheader
                                        #   in Loop: Header=BB44_8 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%eax, (%rsi,%rbp,4)
	movslq	%eax, %rcx
	leaq	(%rcx,%rbp), %rdx
	notl	%ecx
	cmpl	$-3, %ecx
	cmovlel	%r14d, %ecx
	leal	1(%rax,%rcx), %eax
	subq	%rax, %rdx
	leaq	(%rsi,%rdx,4), %rdi
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movl	4(%r13), %ecx
	cmpl	%ecx, 12(%rsp)          # 4-byte Folded Reload
	jge	.LBB44_15
# BB#11:                                # %.lr.ph61.preheader
                                        #   in Loop: Header=BB44_8 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp,4), %rax
	movl	12(%rsp), %edx          # 4-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	.p2align	4, 0x90
.LBB44_12:                              # %.lr.ph61
                                        #   Parent Loop BB44_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %esi
	sarl	$5, %esi
	movslq	%esi, %rsi
	movl	4(%r12,%rsi,4), %esi
	btl	%edx, %esi
	jae	.LBB44_14
# BB#13:                                #   in Loop: Header=BB44_12 Depth=2
	leal	(%r15,%rdx), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%esi, 4(%rax,%rcx,4)
	movl	4(%r13), %ecx
.LBB44_14:                              #   in Loop: Header=BB44_12 Depth=2
	incl	%edx
	cmpl	%ecx, %edx
	jl	.LBB44_12
	jmp	.LBB44_15
.LBB44_30:                              # %._crit_edge65
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB44_32
# BB#31:
	callq	free
	movq	$0, 24(%r13)
.LBB44_32:                              # %sf_free.exit
	movq	set_family_garbage(%rip), %rax
	movq	%rax, 32(%r13)
	movq	%r13, set_family_garbage(%rip)
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end44:
	.size	sf_delcol, .Lfunc_end44-sf_delcol
	.cfi_endproc

	.globl	sf_copy_col
	.p2align	4, 0x90
	.type	sf_copy_col,@function
sf_copy_col:                            # @sf_copy_col
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi276:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi277:
	.cfi_def_cfa_offset 24
.Lcfi278:
	.cfi_offset %rbx, -24
.Lcfi279:
	.cfi_offset %r14, -16
	movl	%ecx, %r8d
	movl	$1, %r9d
	movl	$1, %r10d
	shll	%cl, %r10d
	movl	%esi, %ecx
	shll	%cl, %r9d
	movl	(%rdx), %ecx
	movl	12(%rdx), %ebx
	imull	%ecx, %ebx
	testl	%ebx, %ebx
	jle	.LBB45_5
# BB#1:                                 # %.lr.ph
	movq	24(%rdx), %rax
	movslq	%ebx, %rbx
	leaq	(%rax,%rbx,4), %r11
	movq	24(%rdi), %rbx
	sarl	$5, %esi
	incl	%esi
	sarl	$5, %r8d
	incl	%r8d
	movslq	%r8d, %r8
	movslq	%esi, %r14
	.p2align	4, 0x90
.LBB45_2:                               # =>This Inner Loop Header: Depth=1
	testl	(%rax,%r8,4), %r10d
	je	.LBB45_4
# BB#3:                                 #   in Loop: Header=BB45_2 Depth=1
	orl	%r9d, (%rbx,%r14,4)
	movl	(%rdx), %ecx
.LBB45_4:                               #   in Loop: Header=BB45_2 Depth=1
	movslq	(%rdi), %rsi
	leaq	(%rbx,%rsi,4), %rbx
	movslq	%ecx, %rsi
	leaq	(%rax,%rsi,4), %rax
	cmpq	%r11, %rax
	jb	.LBB45_2
.LBB45_5:                               # %._crit_edge
	movq	%rdi, %rax
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end45:
	.size	sf_copy_col, .Lfunc_end45-sf_copy_col
	.cfi_endproc

	.globl	sf_compress
	.p2align	4, 0x90
	.type	sf_compress,@function
sf_compress:                            # @sf_compress
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi280:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi281:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi282:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi283:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi284:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi285:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi286:
	.cfi_def_cfa_offset 64
.Lcfi287:
	.cfi_offset %rbx, -56
.Lcfi288:
	.cfi_offset %r12, -48
.Lcfi289:
	.cfi_offset %r13, -40
.Lcfi290:
	.cfi_offset %r14, -32
.Lcfi291:
	.cfi_offset %r15, -24
.Lcfi292:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	12(%r15), %r12d
	movl	$1023, %eax             # imm = 0x3FF
	andl	(%r14), %eax
	je	.LBB46_1
# BB#2:                                 # %.lr.ph.preheader.i
	movl	%eax, %eax
	incq	%rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB46_3:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r14,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB46_5
# BB#4:                                 #   in Loop: Header=BB46_3 Depth=1
	movzbl	%cl, %edx
	movzbl	%ch, %esi  # NOREX
	movl	%ecx, %edi
	shrl	$16, %edi
	movzbl	%dil, %edi
	shrl	$24, %ecx
	addl	bit_count(,%rdx,4), %ebp
	addl	bit_count(,%rsi,4), %ebp
	addl	bit_count(,%rdi,4), %ebp
	addl	bit_count(,%rcx,4), %ebp
.LBB46_5:                               #   in Loop: Header=BB46_3 Depth=1
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB46_3
	jmp	.LBB46_6
.LBB46_1:
	xorl	%ebp, %ebp
.LBB46_6:                               # %set_ord.exit
	movq	set_family_garbage(%rip), %r13
	testq	%r13, %r13
	je	.LBB46_7
# BB#8:
	movq	32(%r13), %rax
	movq	%rax, set_family_garbage(%rip)
	jmp	.LBB46_9
.LBB46_7:
	movl	$40, %edi
	callq	malloc
	movq	%rax, %r13
.LBB46_9:
	movslq	%r12d, %rax
	movl	%ebp, 4(%r13)
	movl	$2, %ecx
	cmpl	$33, %ebp
	jl	.LBB46_11
# BB#10:
	decl	%ebp
	sarl	$5, %ebp
	addl	$2, %ebp
	movl	%ebp, %ecx
.LBB46_11:                              # %sf_new.exit
	movl	%ecx, (%r13)
	movl	%eax, 8(%r13)
	movslq	%ecx, %rdi
	imulq	%rax, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %r12
	movq	%r12, 24(%r13)
	movq	$0, 12(%r13)
	cmpl	$0, 12(%r15)
	jle	.LBB46_16
# BB#12:                                # %.lr.ph42.preheader
	xorl	%eax, %eax
	movl	$1, %ebp
	movl	$-2, %ebx
	jmp	.LBB46_13
	.p2align	4, 0x90
.LBB46_30:                              # %._crit_edge44
                                        #   in Loop: Header=BB46_13 Depth=1
	movl	12(%r13), %eax
	incl	%ebp
.LBB46_13:                              # %.lr.ph42
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%r13), %rcx
	leal	1(%rax), %edx
	movl	%edx, 12(%r13)
	cltq
	imulq	%rcx, %rax
	movl	4(%r13), %edx
	cmpl	$33, %edx
	movl	$1, %ecx
	jl	.LBB46_15
# BB#14:                                #   in Loop: Header=BB46_13 Depth=1
	decl	%edx
	sarl	$5, %edx
	incl	%edx
	movl	%edx, %ecx
.LBB46_15:                              #   in Loop: Header=BB46_13 Depth=1
	movl	%ecx, (%r12,%rax,4)
	movslq	%ecx, %rdx
	addq	%rdx, %rax
	notl	%edx
	cmpl	$-3, %edx
	cmovlel	%ebx, %edx
	leal	1(%rcx,%rdx), %ecx
	subq	%rcx, %rax
	leaq	(%r12,%rax,4), %rdi
	leaq	4(,%rcx,4), %rdx
	xorl	%esi, %esi
	callq	memset
	cmpl	12(%r15), %ebp
	jl	.LBB46_30
.LBB46_16:                              # %.preheader
	movl	4(%r15), %r8d
	testl	%r8d, %r8d
	jle	.LBB46_27
# BB#17:                                # %.lr.ph
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB46_18:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB46_22 Depth 2
	movl	%r10d, %eax
	sarl	$5, %eax
	movslq	%eax, %rdi
	movl	4(%r14,%rdi,4), %eax
	movl	$1, %ebp
	movl	%r10d, %ecx
	shll	%cl, %ebp
	andb	$31, %cl
	movzbl	%cl, %ecx
	btl	%ecx, %eax
	jae	.LBB46_19
# BB#20:                                #   in Loop: Header=BB46_18 Depth=1
	leal	1(%r11), %r9d
	movl	$1, %r12d
	movl	%r11d, %ecx
	shll	%cl, %r12d
	movl	(%r15), %ecx
	movl	12(%r15), %eax
	imull	%ecx, %eax
	testl	%eax, %eax
	jle	.LBB46_26
# BB#21:                                # %.lr.ph.i38
                                        #   in Loop: Header=BB46_18 Depth=1
	incq	%rdi
	movq	24(%r15), %rbx
	cltq
	leaq	(%rbx,%rax,4), %rax
	movq	24(%r13), %rdx
	sarl	$5, %r11d
	incl	%r11d
	movslq	%r11d, %r8
	.p2align	4, 0x90
.LBB46_22:                              #   Parent Loop BB46_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	(%rbx,%rdi,4), %ebp
	je	.LBB46_24
# BB#23:                                #   in Loop: Header=BB46_22 Depth=2
	orl	%r12d, (%rdx,%r8,4)
	movl	(%r15), %ecx
.LBB46_24:                              #   in Loop: Header=BB46_22 Depth=2
	movslq	(%r13), %rsi
	leaq	(%rdx,%rsi,4), %rdx
	movslq	%ecx, %rsi
	leaq	(%rbx,%rsi,4), %rbx
	cmpq	%rax, %rbx
	jb	.LBB46_22
# BB#25:                                # %sf_copy_col.exit.loopexit
                                        #   in Loop: Header=BB46_18 Depth=1
	movl	4(%r15), %r8d
	jmp	.LBB46_26
	.p2align	4, 0x90
.LBB46_19:                              #   in Loop: Header=BB46_18 Depth=1
	movl	%r11d, %r9d
.LBB46_26:                              # %sf_copy_col.exit
                                        #   in Loop: Header=BB46_18 Depth=1
	incl	%r10d
	cmpl	%r8d, %r10d
	movl	%r9d, %r11d
	jl	.LBB46_18
.LBB46_27:                              # %._crit_edge
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB46_29
# BB#28:
	callq	free
	movq	$0, 24(%r15)
.LBB46_29:                              # %sf_free.exit
	movq	set_family_garbage(%rip), %rax
	movq	%rax, 32(%r15)
	movq	%r15, set_family_garbage(%rip)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end46:
	.size	sf_compress, .Lfunc_end46-sf_compress
	.cfi_endproc

	.globl	sf_transpose
	.p2align	4, 0x90
	.type	sf_transpose,@function
sf_transpose:                           # @sf_transpose
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi293:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi294:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi295:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi296:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi297:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi298:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi299:
	.cfi_def_cfa_offset 64
.Lcfi300:
	.cfi_offset %rbx, -56
.Lcfi301:
	.cfi_offset %r12, -48
.Lcfi302:
	.cfi_offset %r13, -40
.Lcfi303:
	.cfi_offset %r14, -32
.Lcfi304:
	.cfi_offset %r15, -24
.Lcfi305:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	4(%r14), %ebx
	movl	12(%r14), %ebp
	movq	set_family_garbage(%rip), %r13
	testq	%r13, %r13
	je	.LBB47_1
# BB#2:
	movq	32(%r13), %rax
	movq	%rax, set_family_garbage(%rip)
	jmp	.LBB47_3
.LBB47_1:
	movl	$40, %edi
	callq	malloc
	movq	%rax, %r13
.LBB47_3:
	movslq	%ebx, %rax
	movl	%ebp, 4(%r13)
	movl	$2, %ecx
	cmpl	$33, %ebp
	jl	.LBB47_5
# BB#4:
	decl	%ebp
	sarl	$5, %ebp
	addl	$2, %ebp
	movl	%ebp, %ecx
.LBB47_5:                               # %sf_new.exit
	movl	%ecx, (%r13)
	movl	%eax, 8(%r13)
	movslq	%ecx, %rdi
	imulq	%rax, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %r15
	movq	%r15, 24(%r13)
	movl	$0, 16(%r13)
	movl	4(%r14), %eax
	movl	%eax, 12(%r13)
	testl	%eax, %eax
	jle	.LBB47_10
# BB#6:                                 # %.lr.ph54.preheader
	xorl	%ebp, %ebp
	movl	$-2, %r12d
	.p2align	4, 0x90
.LBB47_7:                               # %.lr.ph54
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%r13), %ecx
	movl	$1, %eax
	cmpl	$33, %ecx
	jl	.LBB47_9
# BB#8:                                 #   in Loop: Header=BB47_7 Depth=1
	decl	%ecx
	sarl	$5, %ecx
	incl	%ecx
	movl	%ecx, %eax
.LBB47_9:                               #   in Loop: Header=BB47_7 Depth=1
	movl	%eax, (%r15)
	movslq	%eax, %rcx
	movl	%ecx, %edx
	notl	%edx
	cmpl	$-3, %edx
	cmovlel	%r12d, %edx
	leal	1(%rax,%rdx), %eax
	subq	%rax, %rcx
	leaq	(%r15,%rcx,4), %rdi
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movslq	(%r13), %rax
	leaq	(%r15,%rax,4), %r15
	incl	%ebp
	cmpl	12(%r13), %ebp
	jl	.LBB47_7
.LBB47_10:                              # %._crit_edge55
	movq	24(%r14), %rdi
	cmpl	$0, 12(%r14)
	jle	.LBB47_18
# BB#11:                                # %.preheader.lr.ph
	movl	4(%r14), %edx
	xorl	%ecx, %ecx
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB47_12:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB47_14 Depth 2
	testl	%edx, %edx
	jle	.LBB47_17
# BB#13:                                # %.lr.ph
                                        #   in Loop: Header=BB47_12 Depth=1
	movl	$1, %r8d
	shll	%cl, %r8d
	movl	%ecx, %ebp
	sarl	$5, %ebp
	incl	%ebp
	movslq	%ebp, %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB47_14:                              #   Parent Loop BB47_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %ebx
	sarl	$5, %ebx
	movslq	%ebx, %rbx
	movl	4(%rax,%rbx,4), %ebx
	btl	%esi, %ebx
	jae	.LBB47_16
# BB#15:                                #   in Loop: Header=BB47_14 Depth=2
	movslq	(%r13), %rdx
	movslq	%esi, %rbx
	imulq	%rdx, %rbx
	shlq	$2, %rbx
	addq	24(%r13), %rbx
	orl	%r8d, (%rbx,%rbp,4)
	movl	4(%r14), %edx
.LBB47_16:                              #   in Loop: Header=BB47_14 Depth=2
	incl	%esi
	cmpl	%edx, %esi
	jl	.LBB47_14
.LBB47_17:                              # %._crit_edge
                                        #   in Loop: Header=BB47_12 Depth=1
	movslq	(%r14), %rsi
	leaq	(%rax,%rsi,4), %rax
	incl	%ecx
	cmpl	12(%r14), %ecx
	jl	.LBB47_12
.LBB47_18:                              # %._crit_edge51
	testq	%rdi, %rdi
	je	.LBB47_20
# BB#19:
	callq	free
	movq	$0, 24(%r14)
.LBB47_20:                              # %sf_free.exit
	movq	set_family_garbage(%rip), %rax
	movq	%rax, 32(%r14)
	movq	%r14, set_family_garbage(%rip)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end47:
	.size	sf_transpose, .Lfunc_end47-sf_transpose
	.cfi_endproc

	.globl	sf_permute
	.p2align	4, 0x90
	.type	sf_permute,@function
sf_permute:                             # @sf_permute
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi306:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi307:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi308:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi309:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi310:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi311:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi312:
	.cfi_def_cfa_offset 80
.Lcfi313:
	.cfi_offset %rbx, -56
.Lcfi314:
	.cfi_offset %r12, -48
.Lcfi315:
	.cfi_offset %r13, -40
.Lcfi316:
	.cfi_offset %r14, -32
.Lcfi317:
	.cfi_offset %r15, -24
.Lcfi318:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	12(%rbx), %r12d
	movq	set_family_garbage(%rip), %r15
	testq	%r15, %r15
	je	.LBB48_1
# BB#2:
	movq	32(%r15), %rax
	movq	%rax, set_family_garbage(%rip)
	jmp	.LBB48_3
.LBB48_1:
	movl	$40, %edi
	callq	malloc
	movq	%rax, %r15
.LBB48_3:
	movslq	%r12d, %rax
	movl	%ebp, 4(%r15)
	movl	$2, %ecx
	cmpl	$33, %ebp
	jl	.LBB48_5
# BB#4:
	leal	-1(%rbp), %ecx
	sarl	$5, %ecx
	addl	$2, %ecx
.LBB48_5:                               # %sf_new.exit
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movl	%ecx, (%r15)
	movl	%eax, 8(%r15)
	movslq	%ecx, %rdi
	imulq	%rax, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %r13
	movq	%r13, 24(%r15)
	movq	$0, 12(%r15)
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movslq	12(%rbx), %rcx
	movl	%ecx, 12(%r15)
	movslq	(%r15), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB48_9
# BB#6:                                 # %.lr.ph61
	leaq	(%r13,%rax,4), %r12
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$32, %eax
	jg	.LBB48_7
	.p2align	4, 0x90
.LBB48_22:                              # %.lr.ph61.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	$1, (%r13)
	movslq	(%r15), %rax
	leaq	(%r13,%rax,4), %r13
	cmpq	%r12, %r13
	jb	.LBB48_22
	jmp	.LBB48_9
.LBB48_7:                               # %.lr.ph61.split.preheader
	leal	-1(%rax), %eax
	sarl	$5, %eax
	leal	1(%rax), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movslq	%ecx, %rbx
	movl	$-2, %ecx
	movl	$-2, %edx
	subl	%eax, %edx
	cmpl	$-3, %edx
	cmovlel	%ecx, %edx
	leal	2(%rax,%rdx), %eax
	subq	%rax, %rbx
	leaq	4(,%rax,4), %rbp
	.p2align	4, 0x90
.LBB48_8:                               # %.lr.ph61.split
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, (%r13)
	leaq	(%r13,%rbx,4), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movslq	(%r15), %rax
	leaq	(%r13,%rax,4), %r13
	cmpq	%r12, %r13
	jb	.LBB48_8
.LBB48_9:                               # %._crit_edge62
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	24(%r12), %rdi
	movslq	(%r12), %rcx
	movslq	12(%r12), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB48_19
# BB#10:                                # %.preheader.lr.ph
	leaq	(%rdi,%rax,4), %r8
	movq	8(%rsp), %rax           # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB48_11
# BB#13:                                # %.preheader.us.preheader
	movq	24(%r15), %rdx
	movl	%eax, %esi
	movq	%rdi, %rbp
	.p2align	4, 0x90
.LBB48_14:                              # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB48_15 Depth 2
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB48_15:                              #   Parent Loop BB48_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r14,%rcx,4), %ebx
	movl	%ebx, %eax
	sarl	$5, %eax
	cltq
	movl	4(%rbp,%rax,4), %eax
	btl	%ebx, %eax
	jae	.LBB48_17
# BB#16:                                #   in Loop: Header=BB48_15 Depth=2
	movl	$1, %eax
	shll	%cl, %eax
	movl	%ecx, %ebx
	sarl	$5, %ebx
	movslq	%ebx, %rbx
	orl	%eax, 4(%rdx,%rbx,4)
.LBB48_17:                              #   in Loop: Header=BB48_15 Depth=2
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB48_15
# BB#18:                                # %._crit_edge.us
                                        #   in Loop: Header=BB48_14 Depth=1
	movslq	(%r15), %rax
	leaq	(%rdx,%rax,4), %rdx
	movslq	(%r12), %rax
	leaq	(%rbp,%rax,4), %rbp
	cmpq	%r8, %rbp
	jb	.LBB48_14
	jmp	.LBB48_19
.LBB48_11:                              # %.preheader.preheader
	shlq	$2, %rcx
	movq	%rdi, %rdx
	.p2align	4, 0x90
.LBB48_12:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	addq	%rcx, %rdx
	cmpq	%r8, %rdx
	jb	.LBB48_12
.LBB48_19:                              # %._crit_edge59
	testq	%rdi, %rdi
	je	.LBB48_21
# BB#20:
	callq	free
	movq	$0, 24(%r12)
.LBB48_21:                              # %sf_free.exit
	movq	set_family_garbage(%rip), %rax
	movq	%rax, 32(%r12)
	movq	%r12, set_family_garbage(%rip)
	movq	%r15, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end48:
	.size	sf_permute, .Lfunc_end48-sf_permute
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"sf_join: sf_size mismatch"
	.size	.L.str, 26

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"sf_append: sf_size mismatch"
	.size	.L.str.1, 28

	.type	set_family_garbage,@object # @set_family_garbage
	.local	set_family_garbage
	.comm	set_family_garbage,8,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d] = %s\n"
	.size	.L.str.2, 12

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"[%4d] %s\n"
	.size	.L.str.3, 10

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%d %d\n"
	.size	.L.str.4, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%x"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%x "
	.size	.L.str.6, 4

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\n\t"
	.size	.L.str.7, 3

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Error reading set family"
	.size	.L.str.9, 25

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Error reading set family (at end of line)"
	.size	.L.str.10, 42

	.type	s1,@object              # @s1
	.local	s1
	.comm	s1,120,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
