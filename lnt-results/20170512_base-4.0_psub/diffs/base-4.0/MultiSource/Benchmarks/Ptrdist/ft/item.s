	.text
	.file	"item.bc"
	.globl	LessThan
	.p2align	4, 0x90
	.type	LessThan,@function
LessThan:                               # @LessThan
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	24(%rsi), %ecx
	setl	%al
	retq
.Lfunc_end0:
	.size	LessThan, .Lfunc_end0-LessThan
	.cfi_endproc

	.globl	Equal
	.p2align	4, 0x90
	.type	Equal,@function
Equal:                                  # @Equal
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	24(%rsi), %ecx
	sete	%al
	retq
.Lfunc_end1:
	.size	Equal, .Lfunc_end1-Equal
	.cfi_endproc

	.globl	Subtract
	.p2align	4, 0x90
	.type	Subtract,@function
Subtract:                               # @Subtract
	.cfi_startproc
# BB#0:
	subl	%esi, 24(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end2:
	.size	Subtract, .Lfunc_end2-Subtract
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
