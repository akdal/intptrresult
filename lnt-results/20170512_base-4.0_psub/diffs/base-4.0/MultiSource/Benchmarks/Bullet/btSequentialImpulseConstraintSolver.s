	.text
	.file	"btSequentialImpulseConstraintSolver.bc"
	.globl	_ZN35btSequentialImpulseConstraintSolverC2Ev
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolverC2Ev,@function
_ZN35btSequentialImpulseConstraintSolverC2Ev: # @_ZN35btSequentialImpulseConstraintSolverC2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV35btSequentialImpulseConstraintSolver+16, (%rdi)
	movb	$1, 32(%rdi)
	movq	$0, 24(%rdi)
	movl	$0, 12(%rdi)
	movl	$0, 16(%rdi)
	movb	$1, 64(%rdi)
	movq	$0, 56(%rdi)
	movl	$0, 44(%rdi)
	movl	$0, 48(%rdi)
	movb	$1, 96(%rdi)
	movq	$0, 88(%rdi)
	movl	$0, 76(%rdi)
	movl	$0, 80(%rdi)
	movb	$1, 128(%rdi)
	movq	$0, 120(%rdi)
	movl	$0, 108(%rdi)
	movl	$0, 112(%rdi)
	movb	$1, 160(%rdi)
	movq	$0, 152(%rdi)
	movl	$0, 140(%rdi)
	movl	$0, 144(%rdi)
	movb	$1, 192(%rdi)
	movq	$0, 184(%rdi)
	movl	$0, 172(%rdi)
	movl	$0, 176(%rdi)
	movb	$1, 224(%rdi)
	movq	$0, 216(%rdi)
	movl	$0, 204(%rdi)
	movl	$0, 208(%rdi)
	movq	$0, 232(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN35btSequentialImpulseConstraintSolverC2Ev, .Lfunc_end0-_ZN35btSequentialImpulseConstraintSolverC2Ev
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN35btSequentialImpulseConstraintSolverD2Ev
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolverD2Ev,@function
_ZN35btSequentialImpulseConstraintSolverD2Ev: # @_ZN35btSequentialImpulseConstraintSolverD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV35btSequentialImpulseConstraintSolver+16, (%rbx)
	movq	216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#1:
	cmpb	$0, 224(%rbx)
	je	.LBB2_3
# BB#2:
.Ltmp0:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp1:
.LBB2_3:                                # %.noexc
	movq	$0, 216(%rbx)
.LBB2_4:
	movb	$1, 224(%rbx)
	movq	$0, 216(%rbx)
	movq	$0, 204(%rbx)
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_8
# BB#5:
	cmpb	$0, 192(%rbx)
	je	.LBB2_7
# BB#6:
.Ltmp5:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp6:
.LBB2_7:                                # %.noexc9
	movq	$0, 184(%rbx)
.LBB2_8:
	movb	$1, 192(%rbx)
	movq	$0, 184(%rbx)
	movq	$0, 172(%rbx)
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_12
# BB#9:
	cmpb	$0, 160(%rbx)
	je	.LBB2_11
# BB#10:
.Ltmp10:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp11:
.LBB2_11:                               # %.noexc11
	movq	$0, 152(%rbx)
.LBB2_12:
	movb	$1, 160(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 140(%rbx)
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_16
# BB#13:
	cmpb	$0, 128(%rbx)
	je	.LBB2_15
# BB#14:
.Ltmp15:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp16:
.LBB2_15:                               # %.noexc14
	movq	$0, 120(%rbx)
.LBB2_16:
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_20
# BB#17:
	cmpb	$0, 96(%rbx)
	je	.LBB2_19
# BB#18:
.Ltmp20:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp21:
.LBB2_19:                               # %.noexc16
	movq	$0, 88(%rbx)
.LBB2_20:
	movb	$1, 96(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 76(%rbx)
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_24
# BB#21:
	cmpb	$0, 64(%rbx)
	je	.LBB2_23
# BB#22:
.Ltmp25:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp26:
.LBB2_23:                               # %.noexc19
	movq	$0, 56(%rbx)
.LBB2_24:
	movb	$1, 64(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 44(%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_28
# BB#25:
	cmpb	$0, 32(%rbx)
	je	.LBB2_27
# BB#26:
.Ltmp31:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp32:
.LBB2_27:                               # %.noexc22
	movq	$0, 24(%rbx)
.LBB2_28:
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_58:
.Ltmp33:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_59:
.Ltmp27:
	movq	%rax, %r14
	jmp	.LBB2_60
.LBB2_52:
.Ltmp22:
	movq	%rax, %r14
	jmp	.LBB2_53
.LBB2_46:
.Ltmp17:
	movq	%rax, %r14
	jmp	.LBB2_47
.LBB2_40:
.Ltmp12:
	movq	%rax, %r14
	jmp	.LBB2_41
.LBB2_34:
.Ltmp7:
	movq	%rax, %r14
	jmp	.LBB2_35
.LBB2_29:
.Ltmp2:
	movq	%rax, %r14
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_33
# BB#30:
	cmpb	$0, 192(%rbx)
	je	.LBB2_32
# BB#31:
.Ltmp3:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
.LBB2_32:                               # %.noexc24
	movq	$0, 184(%rbx)
.LBB2_33:                               # %_ZN20btAlignedObjectArrayIiED2Ev.exit25
	movb	$1, 192(%rbx)
	movq	$0, 184(%rbx)
	movq	$0, 172(%rbx)
.LBB2_35:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_39
# BB#36:
	cmpb	$0, 160(%rbx)
	je	.LBB2_38
# BB#37:
.Ltmp8:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp9:
.LBB2_38:                               # %.noexc27
	movq	$0, 152(%rbx)
.LBB2_39:                               # %_ZN20btAlignedObjectArrayIiED2Ev.exit28
	movb	$1, 160(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 140(%rbx)
.LBB2_41:
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_45
# BB#42:
	cmpb	$0, 128(%rbx)
	je	.LBB2_44
# BB#43:
.Ltmp13:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp14:
.LBB2_44:                               # %.noexc30
	movq	$0, 120(%rbx)
.LBB2_45:                               # %_ZN20btAlignedObjectArrayI18btSolverConstraintED2Ev.exit31
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
.LBB2_47:
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_51
# BB#48:
	cmpb	$0, 96(%rbx)
	je	.LBB2_50
# BB#49:
.Ltmp18:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp19:
.LBB2_50:                               # %.noexc33
	movq	$0, 88(%rbx)
.LBB2_51:                               # %_ZN20btAlignedObjectArrayI18btSolverConstraintED2Ev.exit34
	movb	$1, 96(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 76(%rbx)
.LBB2_53:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_57
# BB#54:
	cmpb	$0, 64(%rbx)
	je	.LBB2_56
# BB#55:
.Ltmp23:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp24:
.LBB2_56:                               # %.noexc36
	movq	$0, 56(%rbx)
.LBB2_57:                               # %_ZN20btAlignedObjectArrayI18btSolverConstraintED2Ev.exit37
	movb	$1, 64(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 44(%rbx)
.LBB2_60:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_64
# BB#61:
	cmpb	$0, 32(%rbx)
	je	.LBB2_63
# BB#62:
.Ltmp28:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp29:
.LBB2_63:                               # %.noexc39
	movq	$0, 24(%rbx)
.LBB2_64:                               # %_ZN20btAlignedObjectArrayI12btSolverBodyED2Ev.exit40
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_66:
.Ltmp30:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN35btSequentialImpulseConstraintSolverD2Ev, .Lfunc_end2-_ZN35btSequentialImpulseConstraintSolverD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin0   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin0   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp3-.Ltmp32          #   Call between .Ltmp32 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 9 <<
	.long	.Ltmp29-.Ltmp3          #   Call between .Ltmp3 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin0   #     jumps to .Ltmp30
	.byte	1                       #   On action: 1
	.long	.Ltmp29-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Lfunc_end2-.Ltmp29     #   Call between .Ltmp29 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN35btSequentialImpulseConstraintSolverD0Ev
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolverD0Ev,@function
_ZN35btSequentialImpulseConstraintSolverD0Ev: # @_ZN35btSequentialImpulseConstraintSolverD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp34:
	callq	_ZN35btSequentialImpulseConstraintSolverD2Ev
.Ltmp35:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_2:
.Ltmp36:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN35btSequentialImpulseConstraintSolverD0Ev, .Lfunc_end3-_ZN35btSequentialImpulseConstraintSolverD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp34-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin1   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end3-.Ltmp35     #   Call between .Ltmp35 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint,@function
_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint: # @_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint
	.cfi_startproc
# BB#0:
	jmp	_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint # TAILCALL
.Lfunc_end4:
	.size	_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint, .Lfunc_end4-_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint
	.cfi_endproc

	.globl	_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint,@function
_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint: # @_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint
	.cfi_startproc
# BB#0:
	movss	120(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	84(%rcx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	124(%rcx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm1
	subss	%xmm1, %xmm0
	movss	16(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movss	20(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	24(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	addss	%xmm5, %xmm6
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	mulss	16(%rsi), %xmm5
	mulss	20(%rsi), %xmm7
	addss	%xmm5, %xmm7
	movss	8(%rcx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	24(%rsi), %xmm5
	addss	%xmm7, %xmm5
	addss	%xmm6, %xmm5
	movss	(%rdx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	movss	4(%rdx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm7
	addss	%xmm6, %xmm7
	movss	8(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	addss	%xmm7, %xmm6
	movss	32(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	16(%rdx), %xmm7
	movss	36(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	20(%rdx), %xmm4
	addss	%xmm7, %xmm4
	movss	40(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	24(%rdx), %xmm7
	addss	%xmm4, %xmm7
	subss	%xmm6, %xmm7
	movss	92(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	subss	%xmm5, %xmm0
	mulss	%xmm4, %xmm7
	subss	%xmm7, %xmm0
	movaps	%xmm8, %xmm5
	addss	%xmm0, %xmm5
	movss	128(%rcx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	ucomiss	%xmm5, %xmm6
	ja	.LBB5_2
# BB#1:
	movss	132(%rcx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm5
	jbe	.LBB5_3
.LBB5_2:
	movaps	%xmm6, %xmm0
	subss	%xmm8, %xmm0
	movaps	%xmm6, %xmm5
.LBB5_3:
	movss	%xmm5, 84(%rcx)
	mulss	48(%rsi), %xmm1
	mulss	52(%rsi), %xmm2
	mulss	56(%rsi), %xmm3
	mulss	%xmm0, %xmm1
	mulss	%xmm0, %xmm2
	mulss	%xmm0, %xmm3
	addss	(%rsi), %xmm1
	movss	%xmm1, (%rsi)
	addss	4(%rsi), %xmm2
	movss	%xmm2, 4(%rsi)
	addss	8(%rsi), %xmm3
	movss	%xmm3, 8(%rsi)
	movss	32(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	36(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	40(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	mulss	48(%rcx), %xmm1
	mulss	52(%rcx), %xmm2
	mulss	56(%rcx), %xmm3
	addss	16(%rsi), %xmm1
	movss	%xmm1, 16(%rsi)
	addss	20(%rsi), %xmm2
	movss	%xmm2, 20(%rsi)
	addss	24(%rsi), %xmm3
	movss	%xmm3, 24(%rsi)
	movss	16(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	20(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	24(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	48(%rdx), %xmm1
	mulss	52(%rdx), %xmm2
	mulss	56(%rdx), %xmm3
	mulss	%xmm0, %xmm1
	mulss	%xmm0, %xmm2
	mulss	%xmm0, %xmm3
	movss	(%rdx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm4
	movss	%xmm4, (%rdx)
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	movss	%xmm1, 4(%rdx)
	movss	8(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm1
	movss	%xmm1, 8(%rdx)
	movss	32(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	36(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	mulss	40(%rdx), %xmm0
	mulss	64(%rcx), %xmm1
	mulss	68(%rcx), %xmm2
	mulss	72(%rcx), %xmm0
	addss	16(%rdx), %xmm1
	movss	%xmm1, 16(%rdx)
	addss	20(%rdx), %xmm2
	movss	%xmm2, 20(%rdx)
	addss	24(%rdx), %xmm0
	movss	%xmm0, 24(%rdx)
	retq
.Lfunc_end5:
	.size	_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint, .Lfunc_end5-_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint
	.cfi_endproc

	.globl	_ZN35btSequentialImpulseConstraintSolver40resolveSingleConstraintRowLowerLimitSIMDER12btSolverBodyS1_RK18btSolverConstraint
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver40resolveSingleConstraintRowLowerLimitSIMDER12btSolverBodyS1_RK18btSolverConstraint,@function
_ZN35btSequentialImpulseConstraintSolver40resolveSingleConstraintRowLowerLimitSIMDER12btSolverBodyS1_RK18btSolverConstraint: # @_ZN35btSequentialImpulseConstraintSolver40resolveSingleConstraintRowLowerLimitSIMDER12btSolverBodyS1_RK18btSolverConstraint
	.cfi_startproc
# BB#0:
	jmp	_ZN35btSequentialImpulseConstraintSolver36resolveSingleConstraintRowLowerLimitER12btSolverBodyS1_RK18btSolverConstraint # TAILCALL
.Lfunc_end6:
	.size	_ZN35btSequentialImpulseConstraintSolver40resolveSingleConstraintRowLowerLimitSIMDER12btSolverBodyS1_RK18btSolverConstraint, .Lfunc_end6-_ZN35btSequentialImpulseConstraintSolver40resolveSingleConstraintRowLowerLimitSIMDER12btSolverBodyS1_RK18btSolverConstraint
	.cfi_endproc

	.globl	_ZN35btSequentialImpulseConstraintSolver36resolveSingleConstraintRowLowerLimitER12btSolverBodyS1_RK18btSolverConstraint
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver36resolveSingleConstraintRowLowerLimitER12btSolverBodyS1_RK18btSolverConstraint,@function
_ZN35btSequentialImpulseConstraintSolver36resolveSingleConstraintRowLowerLimitER12btSolverBodyS1_RK18btSolverConstraint: # @_ZN35btSequentialImpulseConstraintSolver36resolveSingleConstraintRowLowerLimitER12btSolverBodyS1_RK18btSolverConstraint
	.cfi_startproc
# BB#0:
	movss	120(%rcx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	84(%rcx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	124(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm0
	subss	%xmm0, %xmm5
	movss	16(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	20(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	movss	24(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	addss	%xmm3, %xmm6
	movss	(%rcx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	mulss	16(%rsi), %xmm3
	mulss	20(%rsi), %xmm7
	addss	%xmm3, %xmm7
	movss	8(%rcx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	24(%rsi), %xmm3
	addss	%xmm7, %xmm3
	addss	%xmm6, %xmm3
	movss	(%rdx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	movss	4(%rdx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	addss	%xmm6, %xmm7
	movss	8(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	addss	%xmm7, %xmm6
	movss	32(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	16(%rdx), %xmm7
	movss	36(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	20(%rdx), %xmm4
	addss	%xmm7, %xmm4
	movss	40(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	24(%rdx), %xmm7
	addss	%xmm4, %xmm7
	subss	%xmm6, %xmm7
	movss	92(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	subss	%xmm3, %xmm5
	mulss	%xmm4, %xmm7
	subss	%xmm7, %xmm5
	movss	128(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm6
	subss	%xmm8, %xmm6
	addss	%xmm5, %xmm8
	movaps	%xmm8, %xmm3
	cmpltss	%xmm4, %xmm3
	movaps	%xmm3, %xmm7
	andnps	%xmm5, %xmm7
	andps	%xmm6, %xmm3
	orps	%xmm7, %xmm3
	maxss	%xmm8, %xmm4
	movss	%xmm4, 84(%rcx)
	mulss	48(%rsi), %xmm0
	mulss	52(%rsi), %xmm1
	mulss	56(%rsi), %xmm2
	mulss	%xmm3, %xmm0
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm2
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addss	4(%rsi), %xmm1
	movss	%xmm1, 4(%rsi)
	addss	8(%rsi), %xmm2
	movss	%xmm2, 8(%rsi)
	movss	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movss	36(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movss	40(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	mulss	48(%rcx), %xmm0
	mulss	52(%rcx), %xmm1
	mulss	56(%rcx), %xmm2
	addss	16(%rsi), %xmm0
	movss	%xmm0, 16(%rsi)
	addss	20(%rsi), %xmm1
	movss	%xmm1, 20(%rsi)
	addss	24(%rsi), %xmm2
	movss	%xmm2, 24(%rsi)
	movss	16(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	48(%rdx), %xmm0
	mulss	52(%rdx), %xmm1
	mulss	56(%rdx), %xmm2
	mulss	%xmm3, %xmm0
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm2
	movss	(%rdx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm4
	movss	%xmm4, (%rdx)
	movss	4(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	%xmm0, 4(%rdx)
	movss	8(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm0
	movss	%xmm0, 8(%rdx)
	movss	32(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movss	36(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movss	40(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	mulss	64(%rcx), %xmm0
	mulss	68(%rcx), %xmm1
	mulss	72(%rcx), %xmm2
	addss	16(%rdx), %xmm0
	movss	%xmm0, 16(%rdx)
	addss	20(%rdx), %xmm1
	movss	%xmm1, 20(%rdx)
	addss	24(%rdx), %xmm2
	movss	%xmm2, 24(%rdx)
	retq
.Lfunc_end7:
	.size	_ZN35btSequentialImpulseConstraintSolver36resolveSingleConstraintRowLowerLimitER12btSolverBodyS1_RK18btSolverConstraint, .Lfunc_end7-_ZN35btSequentialImpulseConstraintSolver36resolveSingleConstraintRowLowerLimitER12btSolverBodyS1_RK18btSolverConstraint
	.cfi_endproc

	.globl	_ZN35btSequentialImpulseConstraintSolver43resolveSplitPenetrationImpulseCacheFriendlyER12btSolverBodyS1_RK18btSolverConstraint
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver43resolveSplitPenetrationImpulseCacheFriendlyER12btSolverBodyS1_RK18btSolverConstraint,@function
_ZN35btSequentialImpulseConstraintSolver43resolveSplitPenetrationImpulseCacheFriendlyER12btSolverBodyS1_RK18btSolverConstraint: # @_ZN35btSequentialImpulseConstraintSolver43resolveSplitPenetrationImpulseCacheFriendlyER12btSolverBodyS1_RK18btSolverConstraint
	.cfi_startproc
# BB#0:
	movss	136(%rcx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm5
	jne	.LBB8_1
	jnp	.LBB8_5
.LBB8_1:
	incl	gNumSplitImpulseRecoveries(%rip)
	movss	80(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	124(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	subss	%xmm0, %xmm5
	movss	16(%rcx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	80(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	movss	20(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	84(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	addss	%xmm2, %xmm6
	movss	24(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	88(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm7
	addss	%xmm6, %xmm7
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	96(%rsi), %xmm2
	mulss	100(%rsi), %xmm6
	addss	%xmm2, %xmm6
	movss	8(%rcx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	104(%rsi), %xmm2
	addss	%xmm6, %xmm2
	addss	%xmm7, %xmm2
	movss	80(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm6
	movss	84(%rdx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	addss	%xmm6, %xmm7
	movss	88(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	addss	%xmm7, %xmm6
	movss	32(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	96(%rdx), %xmm7
	movss	36(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	100(%rdx), %xmm0
	addss	%xmm7, %xmm0
	movss	40(%rcx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	104(%rdx), %xmm7
	addss	%xmm0, %xmm7
	subss	%xmm6, %xmm7
	movss	92(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm5
	mulss	%xmm0, %xmm7
	subss	%xmm7, %xmm5
	movss	128(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm6
	subss	%xmm4, %xmm6
	addss	%xmm5, %xmm4
	movaps	%xmm4, %xmm2
	cmpltss	%xmm0, %xmm2
	movaps	%xmm2, %xmm7
	andnps	%xmm5, %xmm7
	andps	%xmm6, %xmm2
	orps	%xmm7, %xmm2
	maxss	%xmm4, %xmm0
	movss	%xmm0, 80(%rcx)
	cmpq	$0, 72(%rsi)
	je	.LBB8_3
# BB#2:
	mulss	48(%rsi), %xmm8
	mulss	52(%rsi), %xmm1
	mulss	56(%rsi), %xmm3
	mulss	%xmm2, %xmm8
	mulss	%xmm2, %xmm1
	mulss	%xmm2, %xmm3
	addss	80(%rsi), %xmm8
	movss	%xmm8, 80(%rsi)
	addss	84(%rsi), %xmm1
	movss	%xmm1, 84(%rsi)
	addss	88(%rsi), %xmm3
	movss	%xmm3, 88(%rsi)
	movss	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	36(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	movss	40(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	mulss	48(%rcx), %xmm0
	mulss	52(%rcx), %xmm1
	mulss	56(%rcx), %xmm3
	addss	96(%rsi), %xmm0
	movss	%xmm0, 96(%rsi)
	addss	100(%rsi), %xmm1
	movss	%xmm1, 100(%rsi)
	addss	104(%rsi), %xmm3
	movss	%xmm3, 104(%rsi)
.LBB8_3:                                # %_ZN12btSolverBody24internalApplyPushImpulseERK9btVector3S2_f.exit54
	cmpq	$0, 72(%rdx)
	je	.LBB8_5
# BB#4:
	movss	16(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	48(%rdx), %xmm0
	mulss	52(%rdx), %xmm1
	movss	24(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	56(%rdx), %xmm3
	mulss	%xmm2, %xmm0
	mulss	%xmm2, %xmm1
	mulss	%xmm2, %xmm3
	movss	80(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm4
	movss	%xmm4, 80(%rdx)
	movss	84(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	%xmm0, 84(%rdx)
	movss	88(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	movss	%xmm0, 88(%rdx)
	movss	32(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	36(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	movss	40(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	mulss	64(%rcx), %xmm0
	mulss	68(%rcx), %xmm1
	mulss	72(%rcx), %xmm3
	addss	96(%rdx), %xmm0
	movss	%xmm0, 96(%rdx)
	addss	100(%rdx), %xmm1
	movss	%xmm1, 100(%rdx)
	addss	104(%rdx), %xmm3
	movss	%xmm3, 104(%rdx)
.LBB8_5:                                # %_ZN12btSolverBody24internalApplyPushImpulseERK9btVector3S2_f.exit
	retq
.Lfunc_end8:
	.size	_ZN35btSequentialImpulseConstraintSolver43resolveSplitPenetrationImpulseCacheFriendlyER12btSolverBodyS1_RK18btSolverConstraint, .Lfunc_end8-_ZN35btSequentialImpulseConstraintSolver43resolveSplitPenetrationImpulseCacheFriendlyER12btSolverBodyS1_RK18btSolverConstraint
	.cfi_endproc

	.globl	_ZN35btSequentialImpulseConstraintSolver27resolveSplitPenetrationSIMDER12btSolverBodyS1_RK18btSolverConstraint
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver27resolveSplitPenetrationSIMDER12btSolverBodyS1_RK18btSolverConstraint,@function
_ZN35btSequentialImpulseConstraintSolver27resolveSplitPenetrationSIMDER12btSolverBodyS1_RK18btSolverConstraint: # @_ZN35btSequentialImpulseConstraintSolver27resolveSplitPenetrationSIMDER12btSolverBodyS1_RK18btSolverConstraint
	.cfi_startproc
# BB#0:
	jmp	_ZN35btSequentialImpulseConstraintSolver43resolveSplitPenetrationImpulseCacheFriendlyER12btSolverBodyS1_RK18btSolverConstraint # TAILCALL
.Lfunc_end9:
	.size	_ZN35btSequentialImpulseConstraintSolver27resolveSplitPenetrationSIMDER12btSolverBodyS1_RK18btSolverConstraint, .Lfunc_end9-_ZN35btSequentialImpulseConstraintSolver27resolveSplitPenetrationSIMDER12btSolverBodyS1_RK18btSolverConstraint
	.cfi_endproc

	.globl	_ZN35btSequentialImpulseConstraintSolver7btRand2Ev
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver7btRand2Ev,@function
_ZN35btSequentialImpulseConstraintSolver7btRand2Ev: # @_ZN35btSequentialImpulseConstraintSolver7btRand2Ev
	.cfi_startproc
# BB#0:
	imull	$1664525, 232(%rdi), %eax # imm = 0x19660D
	addl	$1013904223, %eax       # imm = 0x3C6EF35F
	movq	%rax, 232(%rdi)
	retq
.Lfunc_end10:
	.size	_ZN35btSequentialImpulseConstraintSolver7btRand2Ev, .Lfunc_end10-_ZN35btSequentialImpulseConstraintSolver7btRand2Ev
	.cfi_endproc

	.globl	_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei,@function
_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei: # @_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei
	.cfi_startproc
# BB#0:
	movslq	%esi, %rcx
	imull	$1664525, 232(%rdi), %eax # imm = 0x19660D
	addl	$1013904223, %eax       # imm = 0x3C6EF35F
	movq	%rax, 232(%rdi)
	cmpl	$65536, %ecx            # imm = 0x10000
	ja	.LBB11_6
# BB#1:
	movq	%rax, %rdx
	shrq	$16, %rdx
	xorq	%rdx, %rax
	cmpl	$256, %esi              # imm = 0x100
	ja	.LBB11_6
# BB#2:
	movq	%rax, %rdx
	shrq	$8, %rdx
	xorq	%rdx, %rax
	cmpl	$16, %esi
	ja	.LBB11_6
# BB#3:
	movq	%rax, %rdx
	shrq	$4, %rdx
	xorq	%rdx, %rax
	cmpl	$4, %esi
	ja	.LBB11_6
# BB#4:
	movq	%rax, %rdx
	shrq	$2, %rdx
	xorq	%rdx, %rax
	cmpl	$2, %esi
	ja	.LBB11_6
# BB#5:
	movq	%rax, %rdx
	shrq	%rdx
	xorq	%rdx, %rax
.LBB11_6:
	xorl	%edx, %edx
	divq	%rcx
	movl	%edx, %eax
	retq
.Lfunc_end11:
	.size	_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei, .Lfunc_end11-_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei
	.cfi_endproc

	.globl	_ZN35btSequentialImpulseConstraintSolver14initSolverBodyEP12btSolverBodyP17btCollisionObject
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver14initSolverBodyEP12btSolverBodyP17btCollisionObject,@function
_ZN35btSequentialImpulseConstraintSolver14initSolverBodyEP12btSolverBodyP17btCollisionObject: # @_ZN35btSequentialImpulseConstraintSolver14initSolverBodyEP12btSolverBodyP17btCollisionObject
	.cfi_startproc
# BB#0:
	testq	%rdx, %rdx
	je	.LBB12_2
# BB#1:
	cmpl	$2, 256(%rdx)
	jne	.LBB12_2
# BB#4:                                 # %select.unfold
	testq	%rdx, %rdx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rsi)
	movups	%xmm0, (%rsi)
	movups	%xmm0, 96(%rsi)
	movups	%xmm0, 80(%rsi)
	je	.LBB12_3
# BB#5:
	movss	360(%rdx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movsd	380(%rdx), %xmm2        # xmm2 = mem[0],zero
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm2, %xmm3
	mulss	388(%rdx), %xmm1
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm3, 48(%rsi)
	movlps	%xmm0, 56(%rsi)
	movq	%rdx, 72(%rsi)
	movups	364(%rdx), %xmm0
	movups	%xmm0, 32(%rsi)
	retq
.LBB12_2:                               # %select.unfold.thread
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rsi)
	movups	%xmm0, (%rsi)
	movups	%xmm0, 96(%rsi)
	movups	%xmm0, 80(%rsi)
.LBB12_3:
	movq	$0, 72(%rsi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rsi)
	movabsq	$4575657222473777152, %rax # imm = 0x3F8000003F800000
	movq	%rax, 32(%rsi)
	movq	$1065353216, 40(%rsi)   # imm = 0x3F800000
	retq
.Lfunc_end12:
	.size	_ZN35btSequentialImpulseConstraintSolver14initSolverBodyEP12btSolverBodyP17btCollisionObject, .Lfunc_end12-_ZN35btSequentialImpulseConstraintSolver14initSolverBodyEP12btSolverBodyP17btCollisionObject
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN35btSequentialImpulseConstraintSolver16restitutionCurveEff
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver16restitutionCurveEff,@function
_ZN35btSequentialImpulseConstraintSolver16restitutionCurveEff: # @_ZN35btSequentialImpulseConstraintSolver16restitutionCurveEff
	.cfi_startproc
# BB#0:
	mulss	%xmm1, %xmm0
	xorps	.LCPI13_0(%rip), %xmm0
	retq
.Lfunc_end13:
	.size	_ZN35btSequentialImpulseConstraintSolver16restitutionCurveEff, .Lfunc_end13-_ZN35btSequentialImpulseConstraintSolver16restitutionCurveEff
	.cfi_endproc

	.globl	_Z24applyAnisotropicFrictionP17btCollisionObjectR9btVector3
	.p2align	4, 0x90
	.type	_Z24applyAnisotropicFrictionP17btCollisionObjectR9btVector3,@function
_Z24applyAnisotropicFrictionP17btCollisionObjectR9btVector3: # @_Z24applyAnisotropicFrictionP17btCollisionObjectR9btVector3
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB14_3
# BB#1:
	cmpb	$0, 184(%rdi)
	je	.LBB14_3
# BB#2:
	movss	(%rsi), %xmm10          # xmm10 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movss	12(%rdi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm4
	mulss	%xmm10, %xmm4
	movss	24(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm9    # xmm9 = xmm9[0],xmm5[0],xmm9[1],xmm5[1]
	mulss	%xmm1, %xmm5
	addss	%xmm4, %xmm5
	movss	40(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm7
	mulss	%xmm3, %xmm7
	addss	%xmm5, %xmm7
	movaps	%xmm10, %xmm5
	mulss	%xmm8, %xmm5
	movss	28(%rdi), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	mulss	%xmm12, %xmm2
	addss	%xmm5, %xmm2
	movss	44(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm6
	mulss	%xmm5, %xmm6
	addss	%xmm2, %xmm6
	movss	16(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm10
	movss	32(%rdi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	addss	%xmm10, %xmm1
	movss	48(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm1, %xmm3
	mulss	168(%rdi), %xmm7
	mulss	172(%rdi), %xmm6
	mulss	176(%rdi), %xmm3
	mulss	%xmm7, %xmm4
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm9, %xmm7
	unpcklps	%xmm12, %xmm8   # xmm8 = xmm8[0],xmm12[0],xmm8[1],xmm12[1]
	mulss	%xmm6, %xmm5
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm8, %xmm6
	addps	%xmm7, %xmm6
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	mulss	%xmm3, %xmm0
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm2, %xmm3
	addps	%xmm6, %xmm3
	addss	%xmm4, %xmm5
	addss	%xmm0, %xmm5
	xorps	%xmm0, %xmm0
	movss	%xmm5, %xmm0            # xmm0 = xmm5[0],xmm0[1,2,3]
	movlps	%xmm3, (%rsi)
	movlps	%xmm0, 8(%rsi)
.LBB14_3:
	retq
.Lfunc_end14:
	.size	_Z24applyAnisotropicFrictionP17btCollisionObjectR9btVector3, .Lfunc_end14-_Z24applyAnisotropicFrictionP17btCollisionObjectR9btVector3
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_f
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_f,@function
_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_f: # @_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_f
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi16:
	.cfi_def_cfa_offset 336
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r15
	movq	360(%rsp), %rax
	movq	352(%rsp), %rbp
	xorl	%r14d, %r14d
	cmpl	$2, 256(%rbp)
	cmovneq	%r14, %rbp
	cmpl	$2, 256(%rax)
	cmoveq	%rax, %r14
	xorps	%xmm1, %xmm1
	movaps	%xmm1, 256(%rsp)
	movaps	%xmm1, 240(%rsp)
	movaps	%xmm1, 224(%rsp)
	movaps	%xmm1, 208(%rsp)
	movaps	%xmm1, 192(%rsp)
	movaps	%xmm1, 176(%rsp)
	movaps	%xmm1, 160(%rsp)
	movaps	%xmm1, 144(%rsp)
	movaps	%xmm1, 128(%rsp)
	movl	108(%r15), %eax
	movslq	%eax, %rdi
	cmpl	112(%r15), %edi
	movdqa	%xmm0, 112(%rsp)        # 16-byte Spill
	movq	%r9, (%rsp)             # 8-byte Spill
	movl	%r8d, 48(%rsp)          # 4-byte Spill
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	movl	%edx, 28(%rsp)          # 4-byte Spill
	jne	.LBB15_18
# BB#1:
	leal	(%rax,%rax), %esi
	testl	%eax, %eax
	movl	$1, %ebx
	cmovnel	%esi, %ebx
	cmpl	%ebx, %eax
	jge	.LBB15_18
# BB#2:
	testl	%ebx, %ebx
	movq	%r14, 104(%rsp)         # 8-byte Spill
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movq	%r13, 88(%rsp)          # 8-byte Spill
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	je	.LBB15_3
# BB#4:
	movslq	%ebx, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,8), %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	108(%r15), %eax
	testl	%eax, %eax
	jg	.LBB15_6
	jmp	.LBB15_13
.LBB15_3:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB15_13
.LBB15_6:                               # %.lr.ph.i.i.i
	movslq	%eax, %r14
	leaq	-1(%r14), %rax
	movq	%r14, %r13
	andq	$3, %r13
	movq	%rax, 72(%rsp)          # 8-byte Spill
	je	.LBB15_7
# BB#8:                                 # %.prol.preheader
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB15_9:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rbx,%r12), %rdi
	movq	120(%r15), %rsi
	addq	%r12, %rsi
	movl	$144, %edx
	callq	memcpy
	incq	%rbp
	addq	$144, %r12
	cmpq	%rbp, %r13
	jne	.LBB15_9
	jmp	.LBB15_10
.LBB15_7:
	xorl	%ebp, %ebp
.LBB15_10:                              # %.prol.loopexit
	cmpq	$3, 72(%rsp)            # 8-byte Folded Reload
	jb	.LBB15_13
# BB#11:                                # %.lr.ph.i.i.i.new
	subq	%rbp, %r14
	leaq	(%rbp,%rbp,8), %r13
	shlq	$4, %r13
	addq	$432, %r13              # imm = 0x1B0
	.p2align	4, 0x90
.LBB15_12:                              # =>This Inner Loop Header: Depth=1
	leaq	(%rbx,%r13), %r12
	leaq	-432(%rbx,%r13), %rdi
	movq	120(%r15), %rax
	leaq	-432(%rax,%r13), %rsi
	movl	$144, %edx
	callq	memcpy
	leaq	-288(%rbx,%r13), %rdi
	movq	120(%r15), %rax
	leaq	-288(%rax,%r13), %rsi
	movl	$144, %edx
	callq	memcpy
	leaq	-144(%rbx,%r13), %rdi
	movq	120(%r15), %rax
	leaq	-144(%rax,%r13), %rsi
	movl	$144, %edx
	callq	memcpy
	movq	120(%r15), %rsi
	addq	%r13, %rsi
	movl	$144, %edx
	movq	%r12, %rdi
	callq	memcpy
	addq	$576, %r13              # imm = 0x240
	addq	$-4, %r14
	jne	.LBB15_12
.LBB15_13:                              # %_ZNK20btAlignedObjectArrayI18btSolverConstraintE4copyEiiPS0_.exit.i.i
	movq	120(%r15), %rdi
	testq	%rdi, %rdi
	movq	104(%rsp), %r14         # 8-byte Reload
	movq	96(%rsp), %rbp          # 8-byte Reload
	movq	88(%rsp), %r13          # 8-byte Reload
	je	.LBB15_17
# BB#14:
	cmpb	$0, 128(%r15)
	je	.LBB15_16
# BB#15:
	callq	_Z21btAlignedFreeInternalPv
.LBB15_16:
	movq	$0, 120(%r15)
.LBB15_17:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE10deallocateEv.exit.i.i
	movb	$1, 128(%r15)
	movq	%rbx, 120(%r15)
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	%eax, 112(%r15)
	movl	108(%r15), %eax
	movq	80(%rsp), %rdi          # 8-byte Reload
.LBB15_18:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE6expandERKS0_.exit
	incl	%eax
	movl	%eax, 108(%r15)
	movq	%rdi, %r12
	movq	%rdi, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,8), %rbx
	movq	120(%r15), %rdi
	addq	%rbx, %rdi
	leaq	128(%rsp), %rsi
	movl	$144, %edx
	callq	memcpy
	movq	120(%r15), %rax
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 128(%rax,%rbx)
	movdqu	%xmm0, 112(%rax,%rbx)
	movdqu	%xmm0, 96(%rax,%rbx)
	movdqu	%xmm0, 80(%rax,%rbx)
	movdqu	%xmm0, 64(%rax,%rbx)
	movdqu	%xmm0, 48(%rax,%rbx)
	movdqu	%xmm0, 32(%rax,%rbx)
	movdqu	%xmm0, 16(%rax,%rbx)
	movdqu	%xmm0, (%rax,%rbx)
	movups	(%r13), %xmm0
	movups	%xmm0, 16(%rax,%rbx)
	movl	28(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 104(%rax,%rbx)
	movl	32(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 108(%rax,%rbx)
	movl	48(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 100(%rax,%rbx)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	84(%rcx), %ecx
	movl	%ecx, 88(%rax,%rbx)
	movq	$0, 112(%rax,%rbx)
	movq	$0, 80(%rax,%rbx)
	movq	336(%rsp), %rdx
	movss	(%rdx), %xmm11          # xmm11 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	24(%rax,%rbx), %xmm9    # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm15
	mulss	%xmm9, %xmm15
	movss	8(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	16(%rax,%rbx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movss	20(%rax,%rbx), %xmm13   # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	mulss	%xmm13, %xmm3
	subss	%xmm3, %xmm15
	mulss	%xmm1, %xmm0
	movaps	%xmm9, %xmm3
	mulss	%xmm11, %xmm3
	subss	%xmm3, %xmm0
	mulss	%xmm13, %xmm11
	movaps	%xmm1, (%rsp)           # 16-byte Spill
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm11
	movaps	%xmm15, %xmm2
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	xorps	%xmm3, %xmm3
	movss	%xmm11, %xmm3           # xmm3 = xmm11[0],xmm3[1,2,3]
	movlps	%xmm2, (%rax,%rbx)
	movlps	%xmm3, 8(%rax,%rbx)
	testq	%rbp, %rbp
	xorps	%xmm8, %xmm8
	xorps	%xmm1, %xmm1
	je	.LBB15_20
# BB#19:
	movaps	%xmm15, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	296(%rbp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	280(%rbp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	284(%rbp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	mulps	%xmm2, %xmm4
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	300(%rbp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	mulps	%xmm2, %xmm5
	addps	%xmm4, %xmm5
	movaps	%xmm11, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	304(%rbp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	288(%rbp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	mulps	%xmm2, %xmm4
	addps	%xmm5, %xmm4
	movss	312(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm2
	mulss	316(%rbp), %xmm0
	addss	%xmm2, %xmm0
	movss	320(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	addss	%xmm0, %xmm2
	movsd	364(%rbp), %xmm1        # xmm1 = mem[0],zero
	mulps	%xmm4, %xmm1
	mulss	372(%rbp), %xmm2
	xorps	%xmm8, %xmm8
	movss	%xmm2, %xmm8            # xmm8 = xmm2[0],xmm8[1,2,3]
.LBB15_20:                              # %.critedge145
	leaq	(%r12,%r12,8), %rcx
	shlq	$4, %rcx
	movlps	%xmm1, 48(%rax,%rcx)
	movlps	%xmm8, 56(%rax,%rcx)
	movaps	.LCPI15_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm13, %xmm2
	xorps	%xmm0, %xmm2
	movaps	%xmm9, %xmm3
	unpcklps	(%rsp), %xmm3   # 16-byte Folded Reload
                                        # xmm3 = xmm3[0],mem[0],xmm3[1],mem[1]
	xorps	%xmm0, %xmm3
	movq	344(%rsp), %rsi
	movq	4(%rsi), %xmm10         # xmm10 = mem[0],zero
	pshufd	$229, %xmm10, %xmm0     # xmm0 = xmm10[1,1,2,3]
	pshufd	$229, %xmm3, %xmm4      # xmm4 = xmm3[1,1,2,3]
	mulss	%xmm10, %xmm4
	mulps	%xmm3, %xmm10
	movss	(%rsi), %xmm12          # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm5
	shufps	$0, %xmm0, %xmm5        # xmm5 = xmm5[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm5      # xmm5 = xmm5[2,0],xmm0[2,3]
	mulss	%xmm2, %xmm12
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	mulps	%xmm5, %xmm2
	subps	%xmm2, %xmm10
	subss	%xmm4, %xmm12
	xorps	%xmm14, %xmm14
	xorps	%xmm0, %xmm0
	movss	%xmm12, %xmm0           # xmm0 = xmm12[0],xmm0[1,2,3]
	movlps	%xmm10, 32(%rax,%rcx)
	movlps	%xmm0, 40(%rax,%rcx)
	testq	%r14, %r14
	xorps	%xmm3, %xmm3
	je	.LBB15_22
# BB#21:
	movaps	%xmm10, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm10, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	296(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	280(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	284(%r14), %xmm5        # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	mulps	%xmm2, %xmm4
	movss	300(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1]
	mulps	%xmm0, %xmm5
	addps	%xmm4, %xmm5
	movaps	%xmm12, %xmm2
	shufps	$0, %xmm12, %xmm2       # xmm2 = xmm2[0,0],xmm12[0,0]
	shufps	$226, %xmm12, %xmm2     # xmm2 = xmm2[2,0],xmm12[2,3]
	movss	304(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	288(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	mulps	%xmm2, %xmm4
	addps	%xmm5, %xmm4
	movss	312(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	movss	316(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	movss	320(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	addss	%xmm3, %xmm0
	movsd	364(%r14), %xmm14       # xmm14 = mem[0],zero
	mulps	%xmm4, %xmm14
	mulss	372(%r14), %xmm0
	xorps	%xmm3, %xmm3
	movss	%xmm0, %xmm3            # xmm3 = xmm0[0],xmm3[1,2,3]
.LBB15_22:                              # %.critedge148
	movlps	%xmm14, 64(%rax,%rcx)
	movlps	%xmm3, 72(%rax,%rcx)
	xorps	%xmm4, %xmm4
	testq	%rbp, %rbp
	xorps	%xmm0, %xmm0
	je	.LBB15_24
# BB#23:
	movss	52(%rax,%rcx), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm13, 48(%rsp)        # 16-byte Spill
	movaps	%xmm11, 32(%rsp)        # 16-byte Spill
	movaps	%xmm9, %xmm11
	movaps	%xmm1, %xmm9
	movss	8(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	mulss	%xmm1, %xmm5
	movaps	%xmm15, %xmm6
	movss	(%rdx), %xmm15          # xmm15 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm7
	mulss	%xmm0, %xmm7
	subss	%xmm7, %xmm5
	mulss	%xmm15, %xmm8
	mulss	%xmm9, %xmm1
	subss	%xmm1, %xmm8
	mulss	%xmm9, %xmm0
	movaps	%xmm11, %xmm9
	movaps	32(%rsp), %xmm11        # 16-byte Reload
	movaps	48(%rsp), %xmm13        # 16-byte Reload
	mulss	%xmm15, %xmm2
	movaps	%xmm6, %xmm15
	subss	%xmm2, %xmm0
	mulss	(%r13), %xmm5
	mulss	4(%r13), %xmm8
	addss	%xmm5, %xmm8
	mulss	8(%r13), %xmm0
	addss	%xmm8, %xmm0
	addss	360(%rbp), %xmm0
.LBB15_24:
	testq	%r14, %r14
	je	.LBB15_26
# BB#25:
	movss	68(%rax,%rcx), %xmm8    # xmm8 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	mulss	%xmm8, %xmm5
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm7
	mulss	%xmm3, %xmm7
	subss	%xmm5, %xmm7
	mulss	%xmm4, %xmm3
	mulss	%xmm14, %xmm2
	subss	%xmm3, %xmm2
	mulss	%xmm14, %xmm1
	mulss	%xmm8, %xmm4
	subss	%xmm1, %xmm4
	mulss	(%r13), %xmm7
	mulss	4(%r13), %xmm2
	addss	%xmm7, %xmm2
	mulss	8(%r13), %xmm4
	addss	%xmm2, %xmm4
	addss	360(%r14), %xmm4
.LBB15_26:
	testq	%rbp, %rbp
	addss	%xmm4, %xmm0
	movaps	112(%rsp), %xmm4        # 16-byte Reload
	divss	%xmm0, %xmm4
	movss	%xmm4, 92(%rax,%rcx)
	je	.LBB15_27
# BB#28:
	movss	328(%rbp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	(%rsp), %xmm0           # 16-byte Folded Reload
	movss	332(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm1
	addss	%xmm0, %xmm1
	movss	336(%rbp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm0
	addss	%xmm1, %xmm0
	movss	344(%rbp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	348(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	352(%rbp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	jmp	.LBB15_29
.LBB15_27:
	xorps	%xmm7, %xmm7
	movaps	(%rsp), %xmm0           # 16-byte Reload
	mulss	%xmm7, %xmm0
	movaps	%xmm13, %xmm1
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm9, %xmm0
	mulss	%xmm7, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm2, %xmm2
	xorps	%xmm3, %xmm3
.LBB15_29:                              # %.critedge151
	addq	%rax, %rbx
	testq	%r14, %r14
	mulss	%xmm15, %xmm3
	mulss	4(%rax,%rcx), %xmm2
	addss	%xmm3, %xmm2
	mulss	%xmm11, %xmm7
	addss	%xmm2, %xmm7
	addss	%xmm0, %xmm7
	je	.LBB15_30
# BB#31:
	movaps	(%rsp), %xmm0           # 16-byte Reload
	mulss	328(%r14), %xmm0
	mulss	332(%r14), %xmm13
	addss	%xmm0, %xmm13
	mulss	336(%r14), %xmm9
	addss	%xmm13, %xmm9
	movss	344(%r14), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	348(%r14), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	352(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB15_32
.LBB15_30:
	xorps	%xmm0, %xmm0
	movaps	(%rsp), %xmm1           # 16-byte Reload
	mulss	%xmm0, %xmm1
	mulss	%xmm0, %xmm13
	addss	%xmm1, %xmm13
	mulss	%xmm0, %xmm9
	addss	%xmm13, %xmm9
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
.LBB15_32:                              # %.critedge157
	mulss	%xmm10, %xmm2
	mulss	36(%rax,%rcx), %xmm1
	addss	%xmm2, %xmm1
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	subss	%xmm9, %xmm0
	addss	%xmm0, %xmm7
	mulss	%xmm7, %xmm4
	xorps	.LCPI15_0(%rip), %xmm4
	movss	%xmm4, 120(%rax,%rcx)
	movl	$0, 124(%rax,%rcx)
	movabsq	$5770521766015270912, %rdx # imm = 0x501502F900000000
	movq	%rdx, 128(%rax,%rcx)
	movq	%rbx, %rax
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_f, .Lfunc_end15-_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_f
	.cfi_endproc

	.globl	_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObject
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObject,@function
_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObject: # @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObject
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 176
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	224(%r14), %r12d
	testl	%r12d, %r12d
	jns	.LBB16_25
# BB#1:
	xorl	%r12d, %r12d
	cmpl	$2, 256(%r14)
	jne	.LBB16_25
# BB#2:
	movss	360(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB16_3
	jnp	.LBB16_25
.LBB16_3:
	movl	12(%r15), %r12d
	movslq	%r12d, %r13
	xorps	%xmm4, %xmm4
	movaps	%xmm4, 96(%rsp)
	movaps	%xmm4, 80(%rsp)
	movaps	%xmm4, 64(%rsp)
	movaps	%xmm4, 48(%rsp)
	movaps	%xmm4, 32(%rsp)
	movaps	%xmm4, 16(%rsp)
	movaps	%xmm4, (%rsp)
	cmpl	16(%r15), %r13d
	movl	%r12d, %eax
	jne	.LBB16_21
# BB#4:
	leal	(%r12,%r12), %eax
	testl	%r12d, %r12d
	movl	$1, %ebp
	cmovnel	%eax, %ebp
	cmpl	%ebp, %r12d
	movl	%r12d, %eax
	jge	.LBB16_21
# BB#5:
	testl	%ebp, %ebp
	je	.LBB16_6
# BB#7:
	movslq	%ebp, %rax
	imulq	$112, %rax, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	xorps	%xmm4, %xmm4
	movq	%rax, %rbx
	movl	12(%r15), %eax
	testl	%eax, %eax
	jg	.LBB16_9
	jmp	.LBB16_16
.LBB16_6:
	xorl	%ebx, %ebx
	movl	%r12d, %eax
	testl	%eax, %eax
	jle	.LBB16_16
.LBB16_9:                               # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB16_10
# BB#11:                                # %.prol.preheader
	xorl	%edi, %edi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB16_12:                              # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rcx
	movups	96(%rcx,%rdi), %xmm0
	movups	%xmm0, 96(%rbx,%rdi)
	movups	80(%rcx,%rdi), %xmm0
	movups	%xmm0, 80(%rbx,%rdi)
	movups	64(%rcx,%rdi), %xmm0
	movups	%xmm0, 64(%rbx,%rdi)
	movups	(%rcx,%rdi), %xmm0
	movups	16(%rcx,%rdi), %xmm1
	movups	32(%rcx,%rdi), %xmm2
	movups	48(%rcx,%rdi), %xmm3
	movups	%xmm3, 48(%rbx,%rdi)
	movups	%xmm2, 32(%rbx,%rdi)
	movups	%xmm1, 16(%rbx,%rdi)
	movups	%xmm0, (%rbx,%rdi)
	incq	%rdx
	addq	$112, %rdi
	cmpq	%rdx, %rsi
	jne	.LBB16_12
	jmp	.LBB16_13
.LBB16_10:
	xorl	%edx, %edx
.LBB16_13:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB16_16
# BB#14:                                # %.lr.ph.i.i.i.new
	subq	%rdx, %rax
	imulq	$112, %rdx, %rcx
	addq	$336, %rcx              # imm = 0x150
	.p2align	4, 0x90
.LBB16_15:                              # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rdx
	movups	-240(%rdx,%rcx), %xmm0
	movups	%xmm0, -240(%rbx,%rcx)
	movups	-256(%rdx,%rcx), %xmm0
	movups	%xmm0, -256(%rbx,%rcx)
	movups	-272(%rdx,%rcx), %xmm0
	movups	%xmm0, -272(%rbx,%rcx)
	movups	-336(%rdx,%rcx), %xmm0
	movups	-320(%rdx,%rcx), %xmm1
	movups	-304(%rdx,%rcx), %xmm2
	movups	-288(%rdx,%rcx), %xmm3
	movups	%xmm3, -288(%rbx,%rcx)
	movups	%xmm2, -304(%rbx,%rcx)
	movups	%xmm1, -320(%rbx,%rcx)
	movups	%xmm0, -336(%rbx,%rcx)
	movq	24(%r15), %rdx
	movups	-128(%rdx,%rcx), %xmm0
	movups	%xmm0, -128(%rbx,%rcx)
	movups	-144(%rdx,%rcx), %xmm0
	movups	%xmm0, -144(%rbx,%rcx)
	movups	-160(%rdx,%rcx), %xmm0
	movups	%xmm0, -160(%rbx,%rcx)
	movups	-224(%rdx,%rcx), %xmm0
	movups	-208(%rdx,%rcx), %xmm1
	movups	-192(%rdx,%rcx), %xmm2
	movups	-176(%rdx,%rcx), %xmm3
	movups	%xmm3, -176(%rbx,%rcx)
	movups	%xmm2, -192(%rbx,%rcx)
	movups	%xmm1, -208(%rbx,%rcx)
	movups	%xmm0, -224(%rbx,%rcx)
	movq	24(%r15), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbx,%rcx)
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbx,%rcx)
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbx,%rcx)
	movups	-112(%rdx,%rcx), %xmm0
	movups	-96(%rdx,%rcx), %xmm1
	movups	-80(%rdx,%rcx), %xmm2
	movups	-64(%rdx,%rcx), %xmm3
	movups	%xmm3, -64(%rbx,%rcx)
	movups	%xmm2, -80(%rbx,%rcx)
	movups	%xmm1, -96(%rbx,%rcx)
	movups	%xmm0, -112(%rbx,%rcx)
	movq	24(%r15), %rdx
	movups	96(%rdx,%rcx), %xmm0
	movups	%xmm0, 96(%rbx,%rcx)
	movups	80(%rdx,%rcx), %xmm0
	movups	%xmm0, 80(%rbx,%rcx)
	movups	64(%rdx,%rcx), %xmm0
	movups	%xmm0, 64(%rbx,%rcx)
	movups	(%rdx,%rcx), %xmm0
	movups	16(%rdx,%rcx), %xmm1
	movups	32(%rdx,%rcx), %xmm2
	movups	48(%rdx,%rcx), %xmm3
	movups	%xmm3, 48(%rbx,%rcx)
	movups	%xmm2, 32(%rbx,%rcx)
	movups	%xmm1, 16(%rbx,%rcx)
	movups	%xmm0, (%rbx,%rcx)
	addq	$448, %rcx              # imm = 0x1C0
	addq	$-4, %rax
	jne	.LBB16_15
.LBB16_16:                              # %_ZNK20btAlignedObjectArrayI12btSolverBodyE4copyEiiPS0_.exit.i.i
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB16_20
# BB#17:
	cmpb	$0, 32(%r15)
	je	.LBB16_19
# BB#18:
	callq	_Z21btAlignedFreeInternalPv
	xorps	%xmm4, %xmm4
.LBB16_19:
	movq	$0, 24(%r15)
.LBB16_20:                              # %_ZN20btAlignedObjectArrayI12btSolverBodyE10deallocateEv.exit.i.i
	movb	$1, 32(%r15)
	movq	%rbx, 24(%r15)
	movl	%ebp, 16(%r15)
	movl	12(%r15), %eax
.LBB16_21:                              # %_ZN20btAlignedObjectArrayI12btSolverBodyE6expandERKS0_.exit
	incl	%eax
	movl	%eax, 12(%r15)
	movq	24(%r15), %rcx
	imulq	$112, %r13, %rax
	movaps	96(%rsp), %xmm0
	movups	%xmm0, 96(%rcx,%rax)
	movaps	80(%rsp), %xmm0
	movups	%xmm0, 80(%rcx,%rax)
	movaps	64(%rsp), %xmm0
	movups	%xmm0, 64(%rcx,%rax)
	movaps	(%rsp), %xmm0
	movaps	16(%rsp), %xmm1
	movaps	32(%rsp), %xmm2
	movaps	48(%rsp), %xmm3
	movups	%xmm3, 48(%rcx,%rax)
	movups	%xmm2, 32(%rcx,%rax)
	movups	%xmm1, 16(%rcx,%rax)
	movups	%xmm0, (%rcx,%rax)
	movq	24(%r15), %rcx
	cmpl	$2, 256(%r14)
	movups	%xmm4, 16(%rcx,%rax)
	movups	%xmm4, (%rcx,%rax)
	movups	%xmm4, 96(%rcx,%rax)
	movups	%xmm4, 80(%rcx,%rax)
	jne	.LBB16_22
# BB#23:                                # %select.unfold.i
	movss	360(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movsd	380(%r14), %xmm1        # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	mulss	388(%r14), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 48(%rcx,%rax)
	movlps	%xmm1, 56(%rcx,%rax)
	movq	%r14, 72(%rcx,%rax)
	movups	364(%r14), %xmm0
	movups	%xmm0, 32(%rcx,%rax)
	jmp	.LBB16_24
.LBB16_22:                              # %select.unfold.thread.i
	movq	$0, 72(%rcx,%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rcx,%rax)
	movabsq	$4575657222473777152, %rdx # imm = 0x3F8000003F800000
	movq	%rdx, 32(%rcx,%rax)
	movq	$1065353216, 40(%rcx,%rax) # imm = 0x3F800000
.LBB16_24:
	movl	%r12d, 224(%r14)
.LBB16_25:                              # %.thread
	movl	%r12d, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObject, .Lfunc_end16-_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObject
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI17_4:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI17_1:
	.long	1065353216              # float 1
.LCPI17_2:
	.long	2147483648              # float -0
.LCPI17_3:
	.long	872415232               # float 1.1920929E-7
.LCPI17_5:
	.long	1060439283              # float 0.707106769
	.text
	.globl	_ZN35btSequentialImpulseConstraintSolver14convertContactEP20btPersistentManifoldRK19btContactSolverInfo
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver14convertContactEP20btPersistentManifoldRK19btContactSolverInfo,@function
_ZN35btSequentialImpulseConstraintSolver14convertContactEP20btPersistentManifoldRK19btContactSolverInfo: # @_ZN35btSequentialImpulseConstraintSolver14convertContactEP20btPersistentManifoldRK19btContactSolverInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	subq	$408, %rsp              # imm = 0x198
.Lcfi42:
	.cfi_def_cfa_offset 464
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rdi, %rbx
	movq	712(%rsi), %rbp
	movq	720(%rsi), %r14
	cmpl	$0, 728(%rsi)
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	je	.LBB17_1
# BB#2:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObject
	movl	%eax, %r15d
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObject
	movq	32(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB17_3
.LBB17_1:
	movl	$-1, %eax
	movl	$-1, %r15d
.LBB17_3:
	movl	%r15d, %edx
	orl	%eax, %edx
	je	.LBB17_99
# BB#4:
	movl	%eax, (%rsp)            # 4-byte Spill
	movl	%r15d, 4(%rsp)          # 4-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	cmpl	$0, 728(%rsi)
	jle	.LBB17_99
# BB#5:                                 # %.lr.ph
	movslq	4(%rsp), %rax           # 4-byte Folded Reload
	movslq	(%rsp), %rcx            # 4-byte Folded Reload
	xorl	%ebx, %ebx
	imulq	$112, %rax, %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	imulq	$112, %rcx, %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	jmp	.LBB17_6
.LBB17_90:                              #   in Loop: Header=BB17_6 Depth=1
	movss	140(%rsi,%r11), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	56(%r12), %xmm0
	leaq	(%rdx,%rdx,8), %rdx
	shlq	$4, %rdx
	movss	%xmm0, 84(%rax,%rdx)
	testq	%r15, %r15
	je	.LBB17_92
# BB#91:                                #   in Loop: Header=BB17_6 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	24(%rcx), %rcx
	movslq	(%r8), %rdi
	movss	360(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	16(%rax,%rdx), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	20(%rax,%rdx), %xmm3    # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	mulss	24(%rax,%rdx), %xmm1
	mulss	%xmm0, %xmm2
	mulss	%xmm0, %xmm3
	mulss	%xmm0, %xmm1
	imulq	$112, %rdi, %rdi
	addss	(%rcx,%rdi), %xmm2
	movss	%xmm2, (%rcx,%rdi)
	addss	4(%rcx,%rdi), %xmm3
	movss	%xmm3, 4(%rcx,%rdi)
	addss	8(%rcx,%rdi), %xmm1
	movss	%xmm1, 8(%rcx,%rdi)
	movss	32(%rcx,%rdi), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	36(%rcx,%rdi), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	mulss	40(%rcx,%rdi), %xmm0
	mulss	48(%rax,%rdx), %xmm1
	mulss	52(%rax,%rdx), %xmm2
	mulss	56(%rax,%rdx), %xmm0
	addss	16(%rcx,%rdi), %xmm1
	movss	%xmm1, 16(%rcx,%rdi)
	addss	20(%rcx,%rdi), %xmm2
	movss	%xmm2, 20(%rcx,%rdi)
	addss	24(%rcx,%rdi), %xmm0
	movss	%xmm0, 24(%rcx,%rdi)
.LBB17_92:                              #   in Loop: Header=BB17_6 Depth=1
	testq	%r13, %r13
	je	.LBB17_98
# BB#93:                                #   in Loop: Header=BB17_6 Depth=1
	leaq	84(%rax,%rdx), %rbp
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	24(%rcx), %rcx
	movslq	4(%r8), %rdi
	movss	360(%r13), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	16(%rax,%rdx), %xmm4    # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	movss	20(%rax,%rdx), %xmm5    # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	mulss	24(%rax,%rdx), %xmm3
	movss	64(%rax,%rdx), %xmm6    # xmm6 = mem[0],zero,zero,zero
	movss	68(%rax,%rdx), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	72(%rax,%rdx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	24(%rsp), %rbp          # 8-byte Reload
	mulss	%xmm0, %xmm4
	mulss	%xmm0, %xmm5
	mulss	%xmm0, %xmm3
	imulq	$112, %rdi, %rax
	movss	(%rcx,%rax), %xmm7      # xmm7 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm7
	movss	%xmm7, (%rcx,%rax)
	movss	4(%rcx,%rax), %xmm4     # xmm4 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm4
	movss	%xmm4, 4(%rcx,%rax)
	movss	8(%rcx,%rax), %xmm4     # xmm4 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm4
	movss	%xmm4, 8(%rcx,%rax)
	movss	32(%rcx,%rax), %xmm3    # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	36(%rcx,%rax), %xmm4    # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	mulss	40(%rcx,%rax), %xmm0
	mulss	%xmm6, %xmm3
	mulss	%xmm2, %xmm4
	mulss	%xmm1, %xmm0
	addss	16(%rcx,%rax), %xmm3
	movss	%xmm3, 16(%rcx,%rax)
	addss	20(%rcx,%rax), %xmm4
	movss	%xmm4, 20(%rcx,%rax)
	addss	24(%rcx,%rax), %xmm0
	movss	%xmm0, 24(%rcx,%rax)
	jmp	.LBB17_98
	.p2align	4, 0x90
.LBB17_6:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_16 Depth 2
                                        #     Child Loop BB17_19 Depth 2
	imulq	$176, %rbx, %rcx
	movss	736(%rsi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	88(%rsi,%rcx), %xmm0
	jb	.LBB17_98
# BB#7:                                 #   in Loop: Header=BB17_6 Depth=1
	leaq	8(%rsi,%rcx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movss	56(%rsi,%rcx), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movss	60(%rsi,%rcx), %xmm5    # xmm5 = mem[0],zero,zero,zero
	subss	60(%rbp), %xmm5
	movss	64(%rsi,%rcx), %xmm2    # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movss	56(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	subps	%xmm1, %xmm2
	movaps	%xmm2, 160(%rsp)        # 16-byte Spill
	movaps	%xmm2, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm5, %xmm0
	shufps	$0, %xmm6, %xmm0        # xmm0 = xmm0[0,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm0      # xmm0 = xmm0[2,0],xmm6[2,3]
	movaps	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movlps	%xmm0, 88(%rsp)
	movlps	%xmm1, 96(%rsp)
	movss	40(%rsi,%rcx), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movss	44(%rsi,%rcx), %xmm3    # xmm3 = mem[0],zero,zero,zero
	subss	60(%r14), %xmm3
	movss	48(%rsi,%rcx), %xmm4    # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	movss	56(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	subps	%xmm1, %xmm4
	movaps	%xmm4, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm1, 176(%rsp)        # 16-byte Spill
	movaps	%xmm3, %xmm0
	shufps	$0, %xmm1, %xmm0        # xmm0 = xmm0[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm0      # xmm0 = xmm0[2,0],xmm1[2,3]
	movaps	%xmm4, %xmm1
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movlps	%xmm0, 72(%rsp)
	movlps	%xmm1, 80(%rsp)
	movq	8(%rsp), %r13           # 8-byte Reload
	movl	44(%r13), %edx
	movslq	%edx, %rsi
	movaps	%xmm2, 384(%rsp)
	movaps	%xmm2, 368(%rsp)
	movaps	%xmm2, 352(%rsp)
	movaps	%xmm2, 336(%rsp)
	movaps	%xmm2, 320(%rsp)
	movaps	%xmm2, 304(%rsp)
	movaps	%xmm2, 288(%rsp)
	movaps	%xmm2, 272(%rsp)
	movaps	%xmm2, 256(%rsp)
	cmpl	48(%r13), %esi
	movl	%edx, %eax
	movq	%rbx, 152(%rsp)         # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movaps	%xmm3, 128(%rsp)        # 16-byte Spill
	movaps	%xmm4, 240(%rsp)        # 16-byte Spill
	movaps	%xmm5, 224(%rsp)        # 16-byte Spill
	movaps	%xmm6, 112(%rsp)        # 16-byte Spill
	jne	.LBB17_25
# BB#8:                                 #   in Loop: Header=BB17_6 Depth=1
	leal	(%rdx,%rdx), %ecx
	testl	%edx, %edx
	movl	$1, %eax
	cmovel	%eax, %ecx
	cmpl	%ecx, %edx
	movl	%edx, %eax
	jge	.LBB17_25
# BB#9:                                 #   in Loop: Header=BB17_6 Depth=1
	testl	%ecx, %ecx
	movq	%rsi, 216(%rsp)         # 8-byte Spill
	movl	%ecx, 108(%rsp)         # 4-byte Spill
	je	.LBB17_10
# BB#11:                                #   in Loop: Header=BB17_6 Depth=1
	movslq	%ecx, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,8), %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r14
	movl	44(%r13), %eax
	testl	%eax, %eax
	jg	.LBB17_13
	jmp	.LBB17_20
.LBB17_10:                              #   in Loop: Header=BB17_6 Depth=1
	movl	%edx, %eax
	xorl	%r14d, %r14d
	testl	%eax, %eax
	jle	.LBB17_20
.LBB17_13:                              # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB17_6 Depth=1
	movslq	%eax, %rbx
	leaq	-1(%rbx), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	%rbx, %r12
	andq	$3, %r12
	je	.LBB17_14
# BB#15:                                # %.prol.preheader
                                        #   in Loop: Header=BB17_6 Depth=1
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_16:                              #   Parent Loop BB17_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r14,%r15), %rdi
	movq	56(%r13), %rsi
	addq	%r15, %rsi
	movl	$144, %edx
	callq	memcpy
	incq	%rbp
	addq	$144, %r15
	cmpq	%rbp, %r12
	jne	.LBB17_16
	jmp	.LBB17_17
.LBB17_14:                              #   in Loop: Header=BB17_6 Depth=1
	xorl	%ebp, %ebp
.LBB17_17:                              # %.prol.loopexit
                                        #   in Loop: Header=BB17_6 Depth=1
	cmpq	$3, 192(%rsp)           # 8-byte Folded Reload
	jb	.LBB17_20
# BB#18:                                # %.lr.ph.i.i.i.new
                                        #   in Loop: Header=BB17_6 Depth=1
	subq	%rbp, %rbx
	leaq	(%rbp,%rbp,8), %r12
	shlq	$4, %r12
	addq	$432, %r12              # imm = 0x1B0
	.p2align	4, 0x90
.LBB17_19:                              #   Parent Loop BB17_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r14,%r12), %r15
	leaq	-432(%r14,%r12), %rdi
	movq	56(%r13), %rax
	leaq	-432(%rax,%r12), %rsi
	movl	$144, %edx
	callq	memcpy
	leaq	-288(%r14,%r12), %rdi
	movq	56(%r13), %rax
	leaq	-288(%rax,%r12), %rsi
	movl	$144, %edx
	callq	memcpy
	leaq	-144(%r14,%r12), %rdi
	movq	56(%r13), %rax
	leaq	-144(%rax,%r12), %rsi
	movl	$144, %edx
	callq	memcpy
	movq	56(%r13), %rsi
	addq	%r12, %rsi
	movl	$144, %edx
	movq	%r15, %rdi
	callq	memcpy
	addq	$576, %r12              # imm = 0x240
	addq	$-4, %rbx
	jne	.LBB17_19
.LBB17_20:                              # %_ZNK20btAlignedObjectArrayI18btSolverConstraintE4copyEiiPS0_.exit.i.i
                                        #   in Loop: Header=BB17_6 Depth=1
	movq	56(%r13), %rdi
	testq	%rdi, %rdi
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB17_24
# BB#21:                                #   in Loop: Header=BB17_6 Depth=1
	cmpb	$0, 64(%r13)
	je	.LBB17_23
# BB#22:                                #   in Loop: Header=BB17_6 Depth=1
	callq	_Z21btAlignedFreeInternalPv
.LBB17_23:                              #   in Loop: Header=BB17_6 Depth=1
	movq	$0, 56(%r13)
.LBB17_24:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE10deallocateEv.exit.i.i
                                        #   in Loop: Header=BB17_6 Depth=1
	movb	$1, 64(%r13)
	movq	%r14, 56(%r13)
	movl	108(%rsp), %eax         # 4-byte Reload
	movl	%eax, 48(%r13)
	movl	44(%r13), %eax
	movq	216(%rsp), %rsi         # 8-byte Reload
.LBB17_25:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE6expandERKS0_.exit
                                        #   in Loop: Header=BB17_6 Depth=1
	incl	%eax
	movl	%eax, 44(%r13)
	movq	%rsi, %r12
	movq	%rsi, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,8), %rbx
	movq	56(%r13), %rdi
	addq	%rbx, %rdi
	movl	$144, %edx
	leaq	256(%rsp), %rsi
	callq	memcpy
	movaps	112(%rsp), %xmm8        # 16-byte Reload
	movaps	224(%rsp), %xmm15       # 16-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	56(%r13), %r14
	cmpl	$2, 256(%rbp)
	movl	$0, %r15d
	cmoveq	%rbp, %r15
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	$2, 256(%rax)
	movl	$0, %r13d
	cmoveq	%rax, %r13
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	%edx, 104(%r14,%rbx)
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, 108(%r14,%rbx)
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	%r9, 112(%r14,%rbx)
	xorps	%xmm0, %xmm0
	testq	%r15, %r15
	xorps	%xmm1, %xmm1
	xorps	%xmm10, %xmm10
	je	.LBB17_27
# BB#26:                                #   in Loop: Header=BB17_6 Depth=1
	movss	80(%r11,%r10), %xmm4    # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm1
	mulss	%xmm4, %xmm1
	movss	72(%r11,%r10), %xmm5    # xmm5 = mem[0],zero,zero,zero
	movss	76(%r11,%r10), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movaps	160(%rsp), %xmm6        # 16-byte Reload
	movaps	%xmm6, %xmm2
	mulss	%xmm3, %xmm2
	subss	%xmm2, %xmm1
	movaps	%xmm6, %xmm2
	mulss	%xmm5, %xmm2
	mulss	%xmm8, %xmm4
	subss	%xmm4, %xmm2
	mulss	%xmm8, %xmm3
	mulss	%xmm15, %xmm5
	subss	%xmm5, %xmm3
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	296(%r15), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	280(%r15), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	284(%r15), %xmm7        # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	mulps	%xmm4, %xmm6
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	300(%r15), %xmm5        # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm7    # xmm7 = xmm7[0],xmm5[0],xmm7[1],xmm5[1]
	mulps	%xmm4, %xmm7
	addps	%xmm6, %xmm7
	movaps	%xmm3, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	304(%r15), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	288(%r15), %xmm6        # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	mulps	%xmm4, %xmm6
	addps	%xmm7, %xmm6
	mulss	312(%r15), %xmm1
	mulss	316(%r15), %xmm2
	addss	%xmm1, %xmm2
	mulss	320(%r15), %xmm3
	addss	%xmm2, %xmm3
	movsd	364(%r15), %xmm1        # xmm1 = mem[0],zero
	mulps	%xmm6, %xmm1
	mulss	372(%r15), %xmm3
	xorps	%xmm10, %xmm10
	movss	%xmm3, %xmm10           # xmm10 = xmm3[0],xmm10[1,2,3]
.LBB17_27:                              # %.critedge450
                                        #   in Loop: Header=BB17_6 Depth=1
	leaq	72(%r11,%r10), %rbp
	leaq	(%r12,%r12,8), %rax
	shlq	$4, %rax
	movlps	%xmm1, 48(%r14,%rax)
	movlps	%xmm10, 56(%r14,%rax)
	testq	%r13, %r13
	xorps	%xmm4, %xmm4
	movss	.LCPI17_1(%rip), %xmm13 # xmm13 = mem[0],zero,zero,zero
	movaps	128(%rsp), %xmm12       # 16-byte Reload
	movaps	240(%rsp), %xmm14       # 16-byte Reload
	je	.LBB17_29
# BB#28:                                #   in Loop: Header=BB17_6 Depth=1
	movss	8(%rbp), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	4(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	(%rbp), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm3
	mulss	%xmm5, %xmm3
	movaps	%xmm14, %xmm4
	mulss	%xmm0, %xmm4
	subss	%xmm4, %xmm3
	movaps	%xmm14, %xmm4
	mulss	%xmm6, %xmm4
	movaps	176(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm2, %xmm5
	subss	%xmm5, %xmm4
	mulss	%xmm2, %xmm0
	mulss	%xmm12, %xmm6
	subss	%xmm6, %xmm0
	movaps	.LCPI17_0(%rip), %xmm2  # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm3
	movaps	%xmm4, %xmm5
	xorps	%xmm2, %xmm5
	movaps	%xmm0, %xmm9
	xorps	%xmm2, %xmm9
	movss	296(%r13), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movss	280(%r13), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	284(%r13), %xmm6        # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm7    # xmm7 = xmm7[0],xmm8[0],xmm7[1],xmm8[1]
	movaps	112(%rsp), %xmm8        # 16-byte Reload
	pshufd	$224, %xmm3, %xmm2      # xmm2 = xmm3[0,0,2,3]
	mulps	%xmm7, %xmm2
	movss	300(%r13), %xmm7        # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1]
	pshufd	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm6, %xmm5
	addps	%xmm2, %xmm5
	movss	304(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	288(%r13), %xmm6        # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0],xmm6[1],xmm2[1]
	pshufd	$224, %xmm9, %xmm2      # xmm2 = xmm9[0,0,2,3]
	mulps	%xmm6, %xmm2
	addps	%xmm5, %xmm2
	movss	312(%r13), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	mulss	316(%r13), %xmm4
	subss	%xmm4, %xmm5
	mulss	320(%r13), %xmm0
	subss	%xmm0, %xmm5
	movsd	364(%r13), %xmm0        # xmm0 = mem[0],zero
	mulps	%xmm2, %xmm0
	mulss	372(%r13), %xmm5
	xorps	%xmm4, %xmm4
	movss	%xmm5, %xmm4            # xmm4 = xmm5[0],xmm4[1,2,3]
.LBB17_29:                              # %.critedge455
                                        #   in Loop: Header=BB17_6 Depth=1
	movlps	%xmm0, 64(%r14,%rax)
	movlps	%xmm4, 72(%r14,%rax)
	xorps	%xmm3, %xmm3
	testq	%r15, %r15
	xorps	%xmm5, %xmm5
	movaps	160(%rsp), %xmm7        # 16-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	je	.LBB17_31
# BB#30:                                #   in Loop: Header=BB17_6 Depth=1
	movss	52(%r14,%rax), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	mulss	%xmm7, %xmm5
	movaps	%xmm10, %xmm6
	mulss	%xmm15, %xmm6
	subss	%xmm6, %xmm5
	mulss	%xmm8, %xmm10
	movaps	%xmm7, %xmm6
	mulss	%xmm1, %xmm6
	subss	%xmm6, %xmm10
	mulss	%xmm15, %xmm1
	mulss	%xmm8, %xmm2
	subss	%xmm2, %xmm1
	mulss	(%rbp), %xmm5
	mulss	4(%rbp), %xmm10
	addss	%xmm5, %xmm10
	mulss	8(%rbp), %xmm1
	addss	%xmm10, %xmm1
	addss	360(%r15), %xmm1
	movaps	%xmm1, %xmm5
.LBB17_31:                              #   in Loop: Header=BB17_6 Depth=1
	leaq	(%r14,%rbx), %r8
	testq	%r13, %r13
	movaps	176(%rsp), %xmm6        # 16-byte Reload
	je	.LBB17_33
# BB#32:                                #   in Loop: Header=BB17_6 Depth=1
	movss	68(%r14,%rax), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm1
	mulss	%xmm3, %xmm1
	movaps	%xmm12, %xmm2
	mulss	%xmm4, %xmm2
	subss	%xmm1, %xmm2
	mulss	%xmm6, %xmm4
	movaps	%xmm14, %xmm1
	mulss	%xmm0, %xmm1
	subss	%xmm4, %xmm1
	mulss	%xmm12, %xmm0
	mulss	%xmm6, %xmm3
	subss	%xmm0, %xmm3
	mulss	(%rbp), %xmm2
	mulss	4(%rbp), %xmm1
	addss	%xmm2, %xmm1
	mulss	8(%rbp), %xmm3
	addss	%xmm1, %xmm3
	addss	360(%r13), %xmm3
.LBB17_33:                              #   in Loop: Header=BB17_6 Depth=1
	addss	%xmm3, %xmm5
	movaps	%xmm13, %xmm0
	divss	%xmm5, %xmm0
	movss	%xmm0, 92(%r14,%rax)
	movups	(%rbp), %xmm0
	movups	%xmm0, 16(%r14,%rax)
	movss	4(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	(%rbp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm3
	unpcklps	%xmm7, %xmm3    # xmm3 = xmm3[0],xmm7[0],xmm3[1],xmm7[1]
	movss	8(%rbp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	unpcklps	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	mulps	%xmm3, %xmm2
	mulps	%xmm7, %xmm4
	subps	%xmm4, %xmm2
	mulss	%xmm8, %xmm0
	mulss	%xmm15, %xmm1
	subss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, (%r8)
	movlps	%xmm1, 8(%r14,%rax)
	movss	4(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	.LCPI17_0(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm1, %xmm2
	xorps	%xmm2, %xmm0
	movss	(%rbp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbp), %xmm4          # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	xorps	%xmm2, %xmm4
	movaps	%xmm12, %xmm2
	unpcklps	%xmm14, %xmm2   # xmm2 = xmm2[0],xmm14[0],xmm2[1],xmm14[1]
	movaps	%xmm2, %xmm1
	mulps	%xmm4, %xmm1
	movaps	%xmm6, %xmm5
	mulss	%xmm0, %xmm5
	unpcklps	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	mulps	%xmm14, %xmm0
	subps	%xmm0, %xmm1
	pshufd	$229, %xmm4, %xmm0      # xmm0 = xmm4[1,1,2,3]
	mulss	%xmm12, %xmm0
	subss	%xmm0, %xmm5
	xorps	%xmm0, %xmm0
	movss	%xmm5, %xmm0            # xmm0 = xmm5[0],xmm0[1,2,3]
	xorps	%xmm4, %xmm4
	movlps	%xmm1, 32(%r14,%rax)
	movlps	%xmm0, 40(%r14,%rax)
	testq	%r15, %r15
	xorps	%xmm11, %xmm11
	xorps	%xmm0, %xmm0
	je	.LBB17_35
# BB#34:                                # %.critedge457.critedge
                                        #   in Loop: Header=BB17_6 Depth=1
	movsd	348(%r15), %xmm0        # xmm0 = mem[0],zero
	mulps	%xmm0, %xmm7
	movss	344(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	pshufd	$229, %xmm0, %xmm5      # xmm5 = xmm0[1,1,2,3]
	mulss	%xmm1, %xmm15
	shufps	$0, %xmm5, %xmm1        # xmm1 = xmm1[0,0],xmm5[0,0]
	shufps	$226, %xmm5, %xmm1      # xmm1 = xmm1[2,0],xmm5[2,3]
	mulps	%xmm3, %xmm1
	subps	%xmm1, %xmm7
	mulss	%xmm0, %xmm8
	subss	%xmm8, %xmm15
	movsd	328(%r15), %xmm11       # xmm11 = mem[0],zero
	addps	%xmm7, %xmm11
	addss	336(%r15), %xmm15
	xorps	%xmm0, %xmm0
	movss	%xmm15, %xmm0           # xmm0 = xmm15[0],xmm0[1,2,3]
.LBB17_35:                              # %.critedge457
                                        #   in Loop: Header=BB17_6 Depth=1
	leaq	88(%r11,%r10), %rcx
	testq	%r13, %r13
	xorps	%xmm1, %xmm1
	je	.LBB17_37
# BB#36:                                # %.critedge460.critedge
                                        #   in Loop: Header=BB17_6 Depth=1
	movsd	348(%r13), %xmm1        # xmm1 = mem[0],zero
	mulps	%xmm1, %xmm14
	movss	344(%r13), %xmm3        # xmm3 = mem[0],zero,zero,zero
	pshufd	$229, %xmm1, %xmm4      # xmm4 = xmm1[1,1,2,3]
	mulss	%xmm3, %xmm12
	shufps	$0, %xmm4, %xmm3        # xmm3 = xmm3[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	mulps	%xmm2, %xmm3
	subps	%xmm3, %xmm14
	mulss	%xmm1, %xmm6
	subss	%xmm6, %xmm12
	movsd	328(%r13), %xmm4        # xmm4 = mem[0],zero
	addps	%xmm14, %xmm4
	addss	336(%r13), %xmm12
	xorps	%xmm1, %xmm1
	movss	%xmm12, %xmm1           # xmm1 = xmm12[0],xmm1[1,2,3]
.LBB17_37:                              # %.critedge460
                                        #   in Loop: Header=BB17_6 Depth=1
	subps	%xmm4, %xmm11
	movaps	%xmm11, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	subss	%xmm1, %xmm0
	movss	(%rbp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	mulss	4(%rbp), %xmm2
	addss	%xmm1, %xmm2
	movss	8(%rbp), %xmm10         # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm10
	addss	%xmm2, %xmm10
	movss	(%rcx), %xmm8           # xmm8 = mem[0],zero,zero,zero
	addss	52(%r12), %xmm8
	movl	92(%r11,%r10), %ecx
	movl	%ecx, 88(%r14,%rax)
	movl	144(%r11,%r10), %ecx
	xorps	%xmm9, %xmm9
	cmpl	64(%r12), %ecx
	jg	.LBB17_40
# BB#38:                                #   in Loop: Header=BB17_6 Depth=1
	movss	96(%r11,%r10), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm1
	movss	.LCPI17_2(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	jae	.LBB17_40
# BB#39:                                #   in Loop: Header=BB17_6 Depth=1
	shufps	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	xorps	%xmm2, %xmm1
	movaps	%xmm1, %xmm9
.LBB17_40:                              #   in Loop: Header=BB17_6 Depth=1
	leaq	16(%r14,%rax), %rcx
	testb	$4, 60(%r12)
	movq	%rbx, 160(%rsp)         # 8-byte Spill
	jne	.LBB17_41
# BB#45:                                #   in Loop: Header=BB17_6 Depth=1
	movl	$0, 84(%r14,%rax)
	jmp	.LBB17_46
	.p2align	4, 0x90
.LBB17_41:                              #   in Loop: Header=BB17_6 Depth=1
	movss	128(%r11,%r10), %xmm5   # xmm5 = mem[0],zero,zero,zero
	mulss	56(%r12), %xmm5
	movss	%xmm5, 84(%r14,%rax)
	testq	%r15, %r15
	je	.LBB17_43
# BB#42:                                #   in Loop: Header=BB17_6 Depth=1
	leaq	56(%r14,%rax), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	24(%rsi), %rsi
	movss	360(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	20(%r14,%rax), %xmm3    # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	mulss	24(%r14,%rax), %xmm1
	mulss	380(%r15), %xmm2
	mulss	384(%r15), %xmm3
	mulss	388(%r15), %xmm1
	mulss	%xmm5, %xmm2
	mulss	%xmm5, %xmm3
	mulss	%xmm5, %xmm1
	movq	208(%rsp), %rbx         # 8-byte Reload
	addss	(%rsi,%rbx), %xmm2
	movss	%xmm2, (%rsi,%rbx)
	addss	4(%rsi,%rbx), %xmm3
	movss	%xmm3, 4(%rsi,%rbx)
	addss	8(%rsi,%rbx), %xmm1
	movss	%xmm1, 8(%rsi,%rbx)
	movss	32(%rsi,%rbx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm1
	movss	36(%rsi,%rbx), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	mulss	40(%rsi,%rbx), %xmm5
	mulss	48(%r14,%rax), %xmm1
	mulss	52(%r14,%rax), %xmm2
	mulss	(%rdi), %xmm5
	addss	16(%rsi,%rbx), %xmm1
	movss	%xmm1, 16(%rsi,%rbx)
	addss	20(%rsi,%rbx), %xmm2
	movss	%xmm2, 20(%rsi,%rbx)
	addss	24(%rsi,%rbx), %xmm5
	movss	%xmm5, 24(%rsi,%rbx)
.LBB17_43:                              #   in Loop: Header=BB17_6 Depth=1
	testq	%r13, %r13
	je	.LBB17_46
# BB#44:                                #   in Loop: Header=BB17_6 Depth=1
	leaq	64(%r14,%rax), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	24(%rsi), %rsi
	movss	360(%r13), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	20(%r14,%rax), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	mulss	24(%r14,%rax), %xmm4
	mulss	380(%r13), %xmm1
	mulss	384(%r13), %xmm2
	mulss	388(%r13), %xmm4
	movss	(%rdi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	68(%r14,%rax), %xmm7    # xmm7 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movss	20(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm2
	mulss	%xmm3, %xmm4
	movq	200(%rsp), %rdi         # 8-byte Reload
	movss	(%rsi,%rdi), %xmm5      # xmm5 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm5
	movss	%xmm5, (%rsi,%rdi)
	movss	4(%rsi,%rdi), %xmm1     # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	movss	%xmm1, 4(%rsi,%rdi)
	movss	8(%rsi,%rdi), %xmm1     # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movss	%xmm1, 8(%rsi,%rdi)
	movss	32(%rsi,%rdi), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movss	36(%rsi,%rdi), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	mulss	40(%rsi,%rdi), %xmm3
	mulss	%xmm6, %xmm1
	mulss	%xmm7, %xmm2
	mulss	%xmm12, %xmm3
	addss	16(%rsi,%rdi), %xmm1
	movss	%xmm1, 16(%rsi,%rdi)
	addss	20(%rsi,%rdi), %xmm2
	movss	%xmm2, 20(%rsi,%rdi)
	addss	24(%rsi,%rdi), %xmm3
	movss	%xmm3, 24(%rsi,%rdi)
.LBB17_46:                              #   in Loop: Header=BB17_6 Depth=1
	movl	$0, 80(%r14,%rax)
	xorps	%xmm3, %xmm3
	testq	%r15, %r15
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
	xorps	%xmm5, %xmm5
	je	.LBB17_48
# BB#47:                                #   in Loop: Header=BB17_6 Depth=1
	movss	328(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	332(%r15), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	336(%r15), %xmm5        # xmm5 = mem[0],zero,zero,zero
.LBB17_48:                              #   in Loop: Header=BB17_6 Depth=1
	leaq	8(%r14,%rax), %rsi
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	20(%r14,%rax), %xmm7    # xmm7 = mem[0],zero,zero,zero
	movss	24(%r14,%rax), %xmm6    # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm6, %xmm5
	addss	%xmm2, %xmm5
	testq	%r15, %r15
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
	je	.LBB17_50
# BB#49:                                #   in Loop: Header=BB17_6 Depth=1
	movss	344(%r15), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	348(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	352(%r15), %xmm2        # xmm2 = mem[0],zero,zero,zero
.LBB17_50:                              # %.critedge463
                                        #   in Loop: Header=BB17_6 Depth=1
	leaq	92(%r14,%rax), %rcx
	testq	%r13, %r13
	mulss	(%r8), %xmm3
	mulss	4(%r14,%rax), %xmm1
	addss	%xmm3, %xmm1
	mulss	(%rsi), %xmm2
	addss	%xmm1, %xmm2
	addss	%xmm2, %xmm5
	je	.LBB17_51
# BB#52:                                #   in Loop: Header=BB17_6 Depth=1
	mulss	328(%r13), %xmm4
	mulss	332(%r13), %xmm7
	addss	%xmm4, %xmm7
	mulss	336(%r13), %xmm6
	addss	%xmm7, %xmm6
	movss	344(%r13), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	348(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	352(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	jmp	.LBB17_53
.LBB17_51:                              #   in Loop: Header=BB17_6 Depth=1
	xorps	%xmm3, %xmm3
	mulss	%xmm3, %xmm4
	mulss	%xmm3, %xmm7
	addss	%xmm4, %xmm7
	mulss	%xmm3, %xmm6
	addss	%xmm7, %xmm6
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
.LBB17_53:                              # %.critedge469
                                        #   in Loop: Header=BB17_6 Depth=1
	mulss	-60(%rcx), %xmm3
	mulss	36(%r14,%rax), %xmm1
	addss	%xmm3, %xmm1
	mulss	-52(%rcx), %xmm2
	addss	%xmm1, %xmm2
	subss	%xmm6, %xmm2
	addss	%xmm2, %xmm5
	movss	32(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	xorps	.LCPI17_0(%rip), %xmm3
	divss	12(%r12), %xmm3
	subss	%xmm5, %xmm9
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	mulss	%xmm1, %xmm9
	cmpl	$0, 44(%r12)
	sete	%cl
	ucomiss	48(%r12), %xmm8
	seta	%bl
	orb	%cl, %bl
	je	.LBB17_55
# BB#54:                                #   in Loop: Header=BB17_6 Depth=1
	addss	%xmm3, %xmm9
.LBB17_55:                              # %.critedge469
                                        #   in Loop: Header=BB17_6 Depth=1
	xorps	%xmm1, %xmm1
	jne	.LBB17_57
# BB#56:                                # %.critedge469
                                        #   in Loop: Header=BB17_6 Depth=1
	movaps	%xmm3, %xmm1
.LBB17_57:                              # %.critedge469
                                        #   in Loop: Header=BB17_6 Depth=1
	movss	%xmm9, 120(%r14,%rax)
	movss	%xmm1, 136(%r14,%rax)
	movl	$0, 124(%r14,%rax)
	movabsq	$5770521766015270912, %rcx # imm = 0x501502F900000000
	movq	%rcx, 128(%r14,%rax)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	108(%rcx), %ecx
	leaq	100(%r14,%rax), %rbx
	movl	%ecx, 100(%r14,%rax)
	testb	$32, 60(%r12)
	movq	%rbx, 176(%rsp)         # 8-byte Spill
	je	.LBB17_59
# BB#58:                                #   in Loop: Header=BB17_6 Depth=1
	cmpb	$0, 132(%r11,%r10)
	je	.LBB17_59
# BB#79:                                #   in Loop: Header=BB17_6 Depth=1
	leaq	148(%r11,%r10), %rsi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	movl	(%rsp), %ecx            # 4-byte Reload
	movq	%r12, %rbx
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movaps	%xmm13, %xmm0
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	pushq	32(%rsp)                # 8-byte Folded Reload
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	leaq	88(%rsp), %rax
	pushq	%rax
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	leaq	112(%rsp), %rax
	pushq	%rax
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	callq	_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_f
	movq	72(%rsp), %r11          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	addq	$32, %rsp
.Lcfi53:
	.cfi_adjust_cfa_offset -32
	testb	$16, 60(%rbx)
	je	.LBB17_81
# BB#80:                                #   in Loop: Header=BB17_6 Depth=1
	leaq	164(%rsi,%r11), %rsi
	movq	%rbp, %rdi
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	(%rsp), %ecx            # 4-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	56(%rsp), %r9           # 8-byte Reload
	movss	.LCPI17_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	pushq	32(%rsp)                # 8-byte Folded Reload
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	leaq	88(%rsp), %rax
	pushq	%rax
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	leaq	112(%rsp), %rax
	pushq	%rax
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	callq	_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_f
	movq	72(%rsp), %r11          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	addq	$32, %rsp
.Lcfi58:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB17_81
	.p2align	4, 0x90
.LBB17_59:                              #   in Loop: Header=BB17_6 Depth=1
	movsd	(%rbp), %xmm3           # xmm3 = mem[0],zero
	movaps	%xmm10, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm3, %xmm1
	movss	8(%rbp), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm10
	subps	%xmm1, %xmm11
	subss	%xmm10, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movq	%r12, %rax
	leaq	148(%r11,%r10), %r12
	movlps	%xmm11, 148(%r11,%r10)
	movlps	%xmm1, 156(%r11,%r10)
	movl	60(%rax), %eax
	testb	$64, %al
	jne	.LBB17_68
# BB#60:                                #   in Loop: Header=BB17_6 Depth=1
	mulss	%xmm11, %xmm11
	movss	152(%r11,%r10), %xmm1   # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	addss	%xmm11, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	.LCPI17_3(%rip), %xmm0
	jbe	.LBB17_68
# BB#61:                                #   in Loop: Header=BB17_6 Depth=1
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB17_63
# BB#62:                                # %call.sqrt
                                        #   in Loop: Header=BB17_6 Depth=1
	callq	sqrtf
	movss	.LCPI17_1(%rip), %xmm13 # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB17_63:                              # %.split
                                        #   in Loop: Header=BB17_6 Depth=1
	divss	%xmm1, %xmm13
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm1
	movss	%xmm1, (%r12)
	movsd	4(%r12), %xmm0          # xmm0 = mem[0],zero
	shufps	$224, %xmm13, %xmm13    # xmm13 = xmm13[0,0,2,3]
	mulps	%xmm0, %xmm13
	movaps	%xmm13, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movss	%xmm13, 4(%r12)
	movss	%xmm3, 8(%r12)
	movq	64(%rsp), %rax          # 8-byte Reload
	testb	$16, 60(%rax)
	je	.LBB17_67
# BB#64:                                #   in Loop: Header=BB17_6 Depth=1
	movss	4(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	(%rbp), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	8(%rbp), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm6
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm13, %xmm7
	mulps	%xmm5, %xmm7
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	mulps	%xmm6, %xmm3
	subps	%xmm3, %xmm7
	mulss	%xmm1, %xmm0
	mulss	%xmm13, %xmm4
	subss	%xmm4, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	164(%rax,%rcx), %rbp
	movlps	%xmm7, 164(%rax,%rcx)
	movlps	%xmm1, 172(%rax,%rcx)
	mulss	%xmm7, %xmm7
	movss	168(%rax,%rcx), %xmm1   # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	addss	%xmm7, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB17_66
# BB#65:                                # %call.sqrt967
                                        #   in Loop: Header=BB17_6 Depth=1
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB17_66:                              # %.split966
                                        #   in Loop: Header=BB17_6 Depth=1
	movss	.LCPI17_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	(%rbp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rbp)
	movss	4(%rbp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 4(%rbp)
	mulss	8(%rbp), %xmm0
	movss	%xmm0, 8(%rbp)
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	callq	_Z24applyAnisotropicFrictionP17btCollisionObjectR9btVector3
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_Z24applyAnisotropicFrictionP17btCollisionObjectR9btVector3
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbp, %rsi
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	(%rsp), %ecx            # 4-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	56(%rsp), %r9           # 8-byte Reload
	movss	.LCPI17_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	pushq	%rbx
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	pushq	32(%rsp)                # 8-byte Folded Reload
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	leaq	88(%rsp), %rax
	pushq	%rax
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	leaq	112(%rsp), %rax
	pushq	%rax
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	callq	_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_f
	addq	$32, %rsp
.Lcfi63:
	.cfi_adjust_cfa_offset -32
.LBB17_67:                              #   in Loop: Header=BB17_6 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	_Z24applyAnisotropicFrictionP17btCollisionObjectR9btVector3
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	_Z24applyAnisotropicFrictionP17btCollisionObjectR9btVector3
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r12, %rsi
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	(%rsp), %ecx            # 4-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	56(%rsp), %r9           # 8-byte Reload
	movss	.LCPI17_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	pushq	%rbp
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	leaq	88(%rsp), %rax
	pushq	%rax
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	leaq	112(%rsp), %rax
	pushq	%rax
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	callq	_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_f
	addq	$32, %rsp
.Lcfi68:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB17_78
.LBB17_68:                              #   in Loop: Header=BB17_6 Depth=1
	leaq	164(%r11,%r10), %rcx
	movaps	%xmm4, %xmm0
	andps	.LCPI17_4(%rip), %xmm0
	ucomiss	.LCPI17_5(%rip), %xmm0
	jbe	.LBB17_72
# BB#69:                                #   in Loop: Header=BB17_6 Depth=1
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	mulss	%xmm3, %xmm3
	mulss	%xmm4, %xmm4
	addss	%xmm3, %xmm4
	xorps	%xmm0, %xmm0
	sqrtss	%xmm4, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB17_71
# BB#70:                                # %call.sqrt969
                                        #   in Loop: Header=BB17_6 Depth=1
	movaps	%xmm4, %xmm0
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movaps	%xmm4, 128(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	128(%rsp), %xmm4        # 16-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movss	.LCPI17_1(%rip), %xmm13 # xmm13 = mem[0],zero,zero,zero
	movq	32(%rsp), %r11          # 8-byte Reload
.LBB17_71:                              # %.split968
                                        #   in Loop: Header=BB17_6 Depth=1
	divss	%xmm0, %xmm13
	movss	8(%rbp), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm3
	movaps	.LCPI17_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm0, %xmm2
	xorps	%xmm2, %xmm3
	mulss	%xmm13, %xmm4
	mulss	4(%rbp), %xmm13
	movl	$0, (%r12)
	movss	%xmm3, 4(%r12)
	movss	%xmm13, 8(%r12)
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm13
	xorps	%xmm2, %xmm13
	mulss	%xmm0, %xmm3
	jmp	.LBB17_75
.LBB17_72:                              #   in Loop: Header=BB17_6 Depth=1
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	xorps	%xmm0, %xmm0
	sqrtss	%xmm3, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB17_74
# BB#73:                                # %call.sqrt971
                                        #   in Loop: Header=BB17_6 Depth=1
	movaps	%xmm3, %xmm0
	movaps	%xmm3, 128(%rsp)        # 16-byte Spill
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	callq	sqrtf
	movq	112(%rsp), %rcx         # 8-byte Reload
	movaps	128(%rsp), %xmm3        # 16-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movss	.LCPI17_1(%rip), %xmm13 # xmm13 = mem[0],zero,zero,zero
	movq	32(%rsp), %r11          # 8-byte Reload
.LBB17_74:                              # %.split970
                                        #   in Loop: Header=BB17_6 Depth=1
	movaps	%xmm13, %xmm4
	divss	%xmm0, %xmm4
	movss	4(%rbp), %xmm13         # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm13
	movaps	.LCPI17_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm0, %xmm2
	xorps	%xmm2, %xmm13
	mulss	%xmm4, %xmm3
	mulss	(%rbp), %xmm4
	movss	%xmm13, (%r12)
	movss	%xmm4, 4(%r12)
	movl	$0, 8(%r12)
	movss	8(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	xorps	%xmm2, %xmm4
	mulss	%xmm0, %xmm13
.LBB17_75:                              # %_Z13btPlaneSpace1RK9btVector3RS_S2_.exit
                                        #   in Loop: Header=BB17_6 Depth=1
	movl	$0, 160(%r11,%r10)
	movss	%xmm4, (%rcx)
	movss	%xmm13, 168(%r11,%r10)
	movss	%xmm3, 172(%r11,%r10)
	movl	$0, 176(%r11,%r10)
	movq	64(%rsp), %rax          # 8-byte Reload
	testb	$16, 60(%rax)
	movq	24(%rsp), %rbx          # 8-byte Reload
	je	.LBB17_77
# BB#76:                                #   in Loop: Header=BB17_6 Depth=1
	movq	%rbx, %rdi
	movq	%rcx, %rbp
	movq	%rbp, %rsi
	callq	_Z24applyAnisotropicFrictionP17btCollisionObjectR9btVector3
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	callq	_Z24applyAnisotropicFrictionP17btCollisionObjectR9btVector3
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbp, %rsi
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	(%rsp), %ecx            # 4-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	56(%rsp), %r9           # 8-byte Reload
	movss	.LCPI17_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	leaq	88(%rsp), %rax
	pushq	%rax
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	leaq	112(%rsp), %rax
	pushq	%rax
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	callq	_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_f
	addq	$32, %rsp
.Lcfi73:
	.cfi_adjust_cfa_offset -32
.LBB17_77:                              #   in Loop: Header=BB17_6 Depth=1
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	_Z24applyAnisotropicFrictionP17btCollisionObjectR9btVector3
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	_Z24applyAnisotropicFrictionP17btCollisionObjectR9btVector3
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r12, %rsi
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	(%rsp), %ecx            # 4-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	56(%rsp), %r9           # 8-byte Reload
	movss	.LCPI17_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	pushq	%rbp
.Lcfi74:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	leaq	88(%rsp), %rax
	pushq	%rax
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	leaq	112(%rsp), %rax
	pushq	%rax
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	callq	_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_f
	addq	$32, %rsp
.Lcfi78:
	.cfi_adjust_cfa_offset -32
.LBB17_78:                              #   in Loop: Header=BB17_6 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %r11          # 8-byte Reload
	movb	$1, 132(%rsi,%r11)
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB17_81:                              #   in Loop: Header=BB17_6 Depth=1
	movl	60(%r12), %ebx
	testb	$8, %bl
	movq	176(%rsp), %rax         # 8-byte Reload
	movslq	(%rax), %rdx
	movq	120(%rbp), %rax
	jne	.LBB17_82
# BB#95:                                #   in Loop: Header=BB17_6 Depth=1
	leaq	(%rdx,%rdx,8), %rcx
	shlq	$4, %rcx
	movl	$0, 84(%rax,%rcx)
	testb	$16, %bl
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	152(%rsp), %rbx         # 8-byte Reload
	je	.LBB17_98
# BB#96:                                #   in Loop: Header=BB17_6 Depth=1
	incl	%edx
	movslq	%edx, %rcx
	leaq	(%rcx,%rcx,8), %rcx
	jmp	.LBB17_97
	.p2align	4, 0x90
.LBB17_82:                              #   in Loop: Header=BB17_6 Depth=1
	movq	160(%rsp), %rcx         # 8-byte Reload
	leaq	104(%r14,%rcx), %r8
	movl	%ebx, %r9d
	andl	$4, %r9d
	jne	.LBB17_83
# BB#87:                                #   in Loop: Header=BB17_6 Depth=1
	leaq	(%rdx,%rdx,8), %rcx
	shlq	$4, %rcx
	movl	$0, 84(%rax,%rcx)
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB17_88
.LBB17_83:                              #   in Loop: Header=BB17_6 Depth=1
	movss	136(%rsi,%r11), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	56(%r12), %xmm0
	leaq	(%rdx,%rdx,8), %rbp
	shlq	$4, %rbp
	movss	%xmm0, 84(%rax,%rbp)
	testq	%r15, %r15
	je	.LBB17_85
# BB#84:                                #   in Loop: Header=BB17_6 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	24(%rcx), %rcx
	movslq	(%r8), %rdi
	movss	360(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	16(%rax,%rbp), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	20(%rax,%rbp), %xmm3    # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	mulss	24(%rax,%rbp), %xmm1
	mulss	380(%r15), %xmm2
	mulss	384(%r15), %xmm3
	mulss	388(%r15), %xmm1
	mulss	%xmm0, %xmm2
	mulss	%xmm0, %xmm3
	mulss	%xmm0, %xmm1
	imulq	$112, %rdi, %rdi
	addss	(%rcx,%rdi), %xmm2
	movss	%xmm2, (%rcx,%rdi)
	addss	4(%rcx,%rdi), %xmm3
	movss	%xmm3, 4(%rcx,%rdi)
	addss	8(%rcx,%rdi), %xmm1
	movss	%xmm1, 8(%rcx,%rdi)
	movss	32(%rcx,%rdi), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	36(%rcx,%rdi), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	mulss	40(%rcx,%rdi), %xmm0
	mulss	48(%rax,%rbp), %xmm1
	mulss	52(%rax,%rbp), %xmm2
	mulss	56(%rax,%rbp), %xmm0
	addss	16(%rcx,%rdi), %xmm1
	movss	%xmm1, 16(%rcx,%rdi)
	addss	20(%rcx,%rdi), %xmm2
	movss	%xmm2, 20(%rcx,%rdi)
	addss	24(%rcx,%rdi), %xmm0
	movss	%xmm0, 24(%rcx,%rdi)
.LBB17_85:                              #   in Loop: Header=BB17_6 Depth=1
	testq	%r13, %r13
	movq	16(%rsp), %r14          # 8-byte Reload
	je	.LBB17_88
# BB#86:                                #   in Loop: Header=BB17_6 Depth=1
	leaq	84(%rax,%rbp), %rdi
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	24(%rcx), %rcx
	movslq	4(%r8), %r10
	movss	360(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	16(%rax,%rbp), %xmm4    # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	movss	20(%rax,%rbp), %xmm5    # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	mulss	24(%rax,%rbp), %xmm1
	mulss	380(%r13), %xmm4
	mulss	384(%r13), %xmm5
	mulss	388(%r13), %xmm1
	movss	64(%rax,%rbp), %xmm6    # xmm6 = mem[0],zero,zero,zero
	movss	68(%rax,%rbp), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movss	72(%rax,%rbp), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	mulss	%xmm0, %xmm5
	mulss	%xmm0, %xmm1
	imulq	$112, %r10, %rbp
	movss	(%rcx,%rbp), %xmm7      # xmm7 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm7
	movss	%xmm7, (%rcx,%rbp)
	movss	4(%rcx,%rbp), %xmm4     # xmm4 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm4
	movss	%xmm4, 4(%rcx,%rbp)
	movss	8(%rcx,%rbp), %xmm4     # xmm4 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm4
	movss	%xmm4, 8(%rcx,%rbp)
	movss	32(%rcx,%rbp), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	36(%rcx,%rbp), %xmm4    # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	mulss	40(%rcx,%rbp), %xmm0
	mulss	%xmm6, %xmm1
	mulss	%xmm3, %xmm4
	mulss	%xmm2, %xmm0
	addss	16(%rcx,%rbp), %xmm1
	movss	%xmm1, 16(%rcx,%rbp)
	addss	20(%rcx,%rbp), %xmm4
	movss	%xmm4, 20(%rcx,%rbp)
	addss	24(%rcx,%rbp), %xmm0
	movss	%xmm0, 24(%rcx,%rbp)
.LBB17_88:                              #   in Loop: Header=BB17_6 Depth=1
	testb	$16, %bl
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	152(%rsp), %rbx         # 8-byte Reload
	je	.LBB17_98
# BB#89:                                #   in Loop: Header=BB17_6 Depth=1
	incq	%rdx
	testl	%r9d, %r9d
	jne	.LBB17_90
# BB#94:                                #   in Loop: Header=BB17_6 Depth=1
	leaq	(%rdx,%rdx,8), %rcx
.LBB17_97:                              #   in Loop: Header=BB17_6 Depth=1
	shlq	$4, %rcx
	movl	$0, 84(%rax,%rcx)
	.p2align	4, 0x90
.LBB17_98:                              #   in Loop: Header=BB17_6 Depth=1
	incq	%rbx
	movslq	728(%rsi), %rax
	cmpq	%rax, %rbx
	jl	.LBB17_6
.LBB17_99:
	addq	$408, %rsp              # imm = 0x198
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	_ZN35btSequentialImpulseConstraintSolver14convertContactEP20btPersistentManifoldRK19btContactSolverInfo, .Lfunc_end17-_ZN35btSequentialImpulseConstraintSolver14convertContactEP20btPersistentManifoldRK19btContactSolverInfo
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI18_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_1:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI18_2:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI18_3:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.text
	.globl	_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc,@function
_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc: # @_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi82:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi83:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi85:
	.cfi_def_cfa_offset 304
.Lcfi86:
	.cfi_offset %rbx, -56
.Lcfi87:
	.cfi_offset %r12, -48
.Lcfi88:
	.cfi_offset %r13, -40
.Lcfi89:
	.cfi_offset %r14, -32
.Lcfi90:
	.cfi_offset %r15, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movl	%r8d, %ebx
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	movq	%rdi, %r12
	movl	304(%rsp), %r14d
	movl	$.L.str, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	movl	%r14d, %eax
	addl	%ebx, %eax
	je	.LBB18_181
# BB#1:                                 # %.preheader482
	movl	%ebx, 48(%rsp)          # 4-byte Spill
	testl	%r14d, %r14d
	jle	.LBB18_5
# BB#2:                                 # %.lr.ph508.preheader
	movslq	%r14d, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB18_3:                               # %.lr.ph508
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	movq	(%rdi), %rax
.Ltmp37:
	callq	*16(%rax)
.Ltmp38:
# BB#4:                                 #   in Loop: Header=BB18_3 Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB18_3
.LBB18_5:                               # %._crit_edge509
	xorps	%xmm4, %xmm4
	movaps	%xmm4, 160(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm4, 128(%rsp)
	movaps	%xmm4, 112(%rsp)
	movaps	%xmm4, 96(%rsp)
	movaps	%xmm4, 80(%rsp)
	movaps	%xmm4, 64(%rsp)
	movl	12(%r12), %ebx
	cmpl	16(%r12), %ebx
	movl	%ebx, %eax
	jne	.LBB18_23
# BB#6:
	leal	(%rbx,%rbx), %eax
	testl	%ebx, %ebx
	movl	$1, %r15d
	cmovnel	%eax, %r15d
	cmpl	%r15d, %ebx
	movl	%ebx, %eax
	jge	.LBB18_23
# BB#7:
	testl	%r15d, %r15d
	je	.LBB18_10
# BB#8:
	movslq	%r15d, %rax
	imulq	$112, %rax, %rdi
.Ltmp40:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	xorps	%xmm4, %xmm4
	movq	%rax, %rbp
.Ltmp41:
# BB#9:                                 # %.noexc
	movl	12(%r12), %eax
	testl	%eax, %eax
	jg	.LBB18_11
	jmp	.LBB18_18
.LBB18_10:
	xorl	%ebp, %ebp
	movl	%ebx, %eax
	testl	%eax, %eax
	jle	.LBB18_18
.LBB18_11:                              # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB18_14
# BB#12:                                # %.prol.preheader635
	xorl	%edi, %edi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB18_13:                              # =>This Inner Loop Header: Depth=1
	movq	24(%r12), %rcx
	movups	96(%rcx,%rdi), %xmm0
	movups	%xmm0, 96(%rbp,%rdi)
	movups	80(%rcx,%rdi), %xmm0
	movups	%xmm0, 80(%rbp,%rdi)
	movups	64(%rcx,%rdi), %xmm0
	movups	%xmm0, 64(%rbp,%rdi)
	movups	(%rcx,%rdi), %xmm0
	movups	16(%rcx,%rdi), %xmm1
	movups	32(%rcx,%rdi), %xmm2
	movups	48(%rcx,%rdi), %xmm3
	movups	%xmm3, 48(%rbp,%rdi)
	movups	%xmm2, 32(%rbp,%rdi)
	movups	%xmm1, 16(%rbp,%rdi)
	movups	%xmm0, (%rbp,%rdi)
	incq	%rdx
	addq	$112, %rdi
	cmpq	%rdx, %rsi
	jne	.LBB18_13
	jmp	.LBB18_15
.LBB18_14:
	xorl	%edx, %edx
.LBB18_15:                              # %.prol.loopexit636
	cmpq	$3, %r8
	jb	.LBB18_18
# BB#16:                                # %.lr.ph.i.i.i.new
	subq	%rdx, %rax
	imulq	$112, %rdx, %rcx
	addq	$336, %rcx              # imm = 0x150
	.p2align	4, 0x90
.LBB18_17:                              # =>This Inner Loop Header: Depth=1
	movq	24(%r12), %rdx
	movups	-240(%rdx,%rcx), %xmm0
	movups	%xmm0, -240(%rbp,%rcx)
	movups	-256(%rdx,%rcx), %xmm0
	movups	%xmm0, -256(%rbp,%rcx)
	movups	-272(%rdx,%rcx), %xmm0
	movups	%xmm0, -272(%rbp,%rcx)
	movups	-336(%rdx,%rcx), %xmm0
	movups	-320(%rdx,%rcx), %xmm1
	movups	-304(%rdx,%rcx), %xmm2
	movups	-288(%rdx,%rcx), %xmm3
	movups	%xmm3, -288(%rbp,%rcx)
	movups	%xmm2, -304(%rbp,%rcx)
	movups	%xmm1, -320(%rbp,%rcx)
	movups	%xmm0, -336(%rbp,%rcx)
	movq	24(%r12), %rdx
	movups	-128(%rdx,%rcx), %xmm0
	movups	%xmm0, -128(%rbp,%rcx)
	movups	-144(%rdx,%rcx), %xmm0
	movups	%xmm0, -144(%rbp,%rcx)
	movups	-160(%rdx,%rcx), %xmm0
	movups	%xmm0, -160(%rbp,%rcx)
	movups	-224(%rdx,%rcx), %xmm0
	movups	-208(%rdx,%rcx), %xmm1
	movups	-192(%rdx,%rcx), %xmm2
	movups	-176(%rdx,%rcx), %xmm3
	movups	%xmm3, -176(%rbp,%rcx)
	movups	%xmm2, -192(%rbp,%rcx)
	movups	%xmm1, -208(%rbp,%rcx)
	movups	%xmm0, -224(%rbp,%rcx)
	movq	24(%r12), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbp,%rcx)
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbp,%rcx)
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbp,%rcx)
	movups	-112(%rdx,%rcx), %xmm0
	movups	-96(%rdx,%rcx), %xmm1
	movups	-80(%rdx,%rcx), %xmm2
	movups	-64(%rdx,%rcx), %xmm3
	movups	%xmm3, -64(%rbp,%rcx)
	movups	%xmm2, -80(%rbp,%rcx)
	movups	%xmm1, -96(%rbp,%rcx)
	movups	%xmm0, -112(%rbp,%rcx)
	movq	24(%r12), %rdx
	movups	96(%rdx,%rcx), %xmm0
	movups	%xmm0, 96(%rbp,%rcx)
	movups	80(%rdx,%rcx), %xmm0
	movups	%xmm0, 80(%rbp,%rcx)
	movups	64(%rdx,%rcx), %xmm0
	movups	%xmm0, 64(%rbp,%rcx)
	movups	(%rdx,%rcx), %xmm0
	movups	16(%rdx,%rcx), %xmm1
	movups	32(%rdx,%rcx), %xmm2
	movups	48(%rdx,%rcx), %xmm3
	movups	%xmm3, 48(%rbp,%rcx)
	movups	%xmm2, 32(%rbp,%rcx)
	movups	%xmm1, 16(%rbp,%rcx)
	movups	%xmm0, (%rbp,%rcx)
	addq	$448, %rcx              # imm = 0x1C0
	addq	$-4, %rax
	jne	.LBB18_17
.LBB18_18:                              # %_ZNK20btAlignedObjectArrayI12btSolverBodyE4copyEiiPS0_.exit.i.i
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB18_22
# BB#19:
	cmpb	$0, 32(%r12)
	je	.LBB18_21
# BB#20:
.Ltmp42:
	callq	_Z21btAlignedFreeInternalPv
	xorps	%xmm4, %xmm4
.Ltmp43:
.LBB18_21:                              # %.noexc284
	movq	$0, 24(%r12)
.LBB18_22:                              # %_ZN20btAlignedObjectArrayI12btSolverBodyE10deallocateEv.exit.i.i
	movb	$1, 32(%r12)
	movq	%rbp, 24(%r12)
	movl	%r15d, 16(%r12)
	movl	12(%r12), %eax
.LBB18_23:
	movslq	%ebx, %rcx
	incl	%eax
	movl	%eax, 12(%r12)
	movq	24(%r12), %rax
	imulq	$112, %rcx, %rcx
	movaps	160(%rsp), %xmm0
	movups	%xmm0, 96(%rax,%rcx)
	movaps	144(%rsp), %xmm0
	movups	%xmm0, 80(%rax,%rcx)
	movaps	128(%rsp), %xmm0
	movups	%xmm0, 64(%rax,%rcx)
	movaps	64(%rsp), %xmm0
	movaps	80(%rsp), %xmm1
	movaps	96(%rsp), %xmm2
	movaps	112(%rsp), %xmm3
	movups	%xmm3, 48(%rax,%rcx)
	movups	%xmm2, 32(%rax,%rcx)
	movups	%xmm1, 16(%rax,%rcx)
	movups	%xmm0, (%rax,%rcx)
	movq	24(%r12), %rax
	movups	%xmm4, 16(%rax,%rcx)
	movups	%xmm4, (%rax,%rcx)
	movups	%xmm4, 48(%rax,%rcx)
	movups	%xmm4, 88(%rax,%rcx)
	movups	%xmm4, 72(%rax,%rcx)
	movq	$0, 104(%rax,%rcx)
	movabsq	$4575657222473777152, %rdx # imm = 0x3F8000003F800000
	movq	%rdx, 32(%rax,%rcx)
	movq	$1065353216, 40(%rax,%rcx) # imm = 0x3F800000
	movl	204(%r12), %r15d
	cmpl	%r14d, %r15d
	jge	.LBB18_45
# BB#24:
	cmpl	%r14d, 208(%r12)
	jge	.LBB18_28
# BB#25:
	testl	%r14d, %r14d
	je	.LBB18_29
# BB#26:
	movslq	%r14d, %rdi
	shlq	$3, %rdi
.Ltmp45:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
.Ltmp46:
# BB#27:                                # %.noexc293
	movl	204(%r12), %eax
	jmp	.LBB18_30
.LBB18_28:                              # %..lr.ph.i_crit_edge
	leaq	216(%r12), %rbx
	jmp	.LBB18_40
.LBB18_29:
	xorl	%ebp, %ebp
	movl	%r15d, %eax
.LBB18_30:                              # %_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE8allocateEi.exit.i.i
	leaq	216(%r12), %rbx
	testl	%eax, %eax
	jle	.LBB18_35
# BB#31:                                # %.lr.ph.i.i.i288
	cltq
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB18_33
	.p2align	4, 0x90
.LBB18_32:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	%rdi, (%rbp,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB18_32
.LBB18_33:                              # %.prol.loopexit631
	cmpq	$3, %rdx
	jb	.LBB18_35
	.p2align	4, 0x90
.LBB18_34:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, (%rbp,%rcx,8)
	movq	(%rbx), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	%rdx, 8(%rbp,%rcx,8)
	movq	(%rbx), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	%rdx, 16(%rbp,%rcx,8)
	movq	(%rbx), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	%rdx, 24(%rbp,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB18_34
.LBB18_35:                              # %_ZNK20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE4copyEiiPS1_.exit.i.i
	movq	216(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB18_39
# BB#36:
	cmpb	$0, 224(%r12)
	je	.LBB18_38
# BB#37:
.Ltmp47:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp48:
.LBB18_38:                              # %.noexc294
	movq	$0, (%rbx)
.LBB18_39:                              # %_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE7reserveEi.exit.preheader.i
	movb	$1, 224(%r12)
	movq	%rbp, 216(%r12)
	movl	%r14d, 208(%r12)
.LBB18_40:                              # %.lr.ph.i
	movslq	%r15d, %rax
	movslq	%r14d, %rcx
	movl	%r14d, %esi
	subl	%r15d, %esi
	leaq	-1(%rcx), %rdx
	subq	%rax, %rdx
	andq	$7, %rsi
	je	.LBB18_43
# BB#41:                                # %_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE7reserveEi.exit.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB18_42:                              # %_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	$0, (%rdi,%rax,8)
	incq	%rax
	incq	%rsi
	jne	.LBB18_42
.LBB18_43:                              # %_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE7reserveEi.exit.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB18_45
	.p2align	4, 0x90
.LBB18_44:                              # %_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movq	$0, (%rdx,%rax,8)
	movq	(%rbx), %rdx
	movq	$0, 8(%rdx,%rax,8)
	movq	(%rbx), %rdx
	movq	$0, 16(%rdx,%rax,8)
	movq	(%rbx), %rdx
	movq	$0, 24(%rdx,%rax,8)
	movq	(%rbx), %rdx
	movq	$0, 32(%rdx,%rax,8)
	movq	(%rbx), %rdx
	movq	$0, 40(%rdx,%rax,8)
	movq	(%rbx), %rdx
	movq	$0, 48(%rdx,%rax,8)
	movq	(%rbx), %rdx
	movq	$0, 56(%rdx,%rax,8)
	addq	$8, %rax
	cmpq	%rax, %rcx
	jne	.LBB18_44
.LBB18_45:                              # %.loopexit481
	movl	%r14d, 204(%r12)
	testl	%r14d, %r14d
	jle	.LBB18_49
# BB#46:                                # %.lr.ph505
	movslq	%r14d, %r14
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB18_47:                              # =>This Inner Loop Header: Depth=1
	movq	216(%r12), %rbx
	leaq	(%rbx,%rbp), %rsi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rbp), %rdi
	movq	(%rdi), %rax
.Ltmp50:
	callq	*32(%rax)
.Ltmp51:
# BB#48:                                #   in Loop: Header=BB18_47 Depth=1
	addl	(%rbx,%rbp), %r13d
	incq	%r15
	addq	$8, %rbp
	cmpq	%r14, %r15
	jl	.LBB18_47
	jmp	.LBB18_50
.LBB18_49:
	xorl	%r13d, %r13d
.LBB18_50:                              # %._crit_edge506
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 192(%rsp)
	movaps	%xmm0, 176(%rsp)
	movaps	%xmm0, 160(%rsp)
	movaps	%xmm0, 144(%rsp)
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movq	%r12, (%rsp)            # 8-byte Spill
	movl	76(%r12), %eax
	cmpl	%r13d, %eax
	jge	.LBB18_76
# BB#51:
	movq	(%rsp), %rcx            # 8-byte Reload
	cmpl	%r13d, 80(%rcx)
	jge	.LBB18_55
# BB#52:
	testl	%r13d, %r13d
	movl	%eax, 16(%rsp)          # 4-byte Spill
	je	.LBB18_56
# BB#53:
	movslq	%r13d, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,8), %rdi
.Ltmp53:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
.Ltmp54:
# BB#54:                                # %.noexc309
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	76(%rcx), %eax
	jmp	.LBB18_57
.LBB18_55:                              # %..lr.ph.i305_crit_edge
	leaq	88(%rcx), %r12
	jmp	.LBB18_70
.LBB18_56:
	xorl	%r15d, %r15d
.LBB18_57:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE8allocateEi.exit.i.i
	leaq	88(%rcx), %r12
	testl	%eax, %eax
	jle	.LBB18_65
# BB#58:                                # %.lr.ph.i.i.i299
	movslq	%eax, %rcx
	leaq	-1(%rcx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movq	%rcx, %rax
	andq	$3, %rax
	je	.LBB18_61
# BB#59:                                # %.prol.preheader
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB18_60:                              # =>This Inner Loop Header: Depth=1
	leaq	(%r15,%rbx), %rdi
	movq	(%r12), %rsi
	addq	%rbx, %rsi
	movl	$144, %edx
	movq	%r15, %r14
	movq	%rax, %r15
	callq	memcpy
	movq	%r15, %rax
	movq	%r14, %r15
	incq	%rbp
	addq	$144, %rbx
	cmpq	%rbp, %rax
	jne	.LBB18_60
	jmp	.LBB18_62
.LBB18_61:
	xorl	%ebp, %ebp
.LBB18_62:                              # %.prol.loopexit
	cmpq	$3, 40(%rsp)            # 8-byte Folded Reload
	movq	208(%rsp), %r14         # 8-byte Reload
	jb	.LBB18_65
# BB#63:                                # %.lr.ph.i.i.i299.new
	subq	%rbp, %r14
	leaq	(%rbp,%rbp,8), %rbx
	shlq	$4, %rbx
	addq	$432, %rbx              # imm = 0x1B0
	.p2align	4, 0x90
.LBB18_64:                              # =>This Inner Loop Header: Depth=1
	leaq	(%r15,%rbx), %rbp
	leaq	-432(%r15,%rbx), %rdi
	movq	(%r12), %rax
	leaq	-432(%rax,%rbx), %rsi
	movl	$144, %edx
	callq	memcpy
	leaq	-288(%r15,%rbx), %rdi
	movq	(%r12), %rax
	leaq	-288(%rax,%rbx), %rsi
	movl	$144, %edx
	callq	memcpy
	leaq	-144(%r15,%rbx), %rdi
	movq	(%r12), %rax
	leaq	-144(%rax,%rbx), %rsi
	movl	$144, %edx
	callq	memcpy
	movq	(%r12), %rsi
	addq	%rbx, %rsi
	movl	$144, %edx
	movq	%rbp, %rdi
	callq	memcpy
	addq	$576, %rbx              # imm = 0x240
	addq	$-4, %r14
	jne	.LBB18_64
.LBB18_65:                              # %_ZNK20btAlignedObjectArrayI18btSolverConstraintE4copyEiiPS0_.exit.i.i
	movq	(%rsp), %rax            # 8-byte Reload
	movq	88(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB18_69
# BB#66:
	cmpb	$0, 96(%rax)
	je	.LBB18_68
# BB#67:
.Ltmp55:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp56:
.LBB18_68:                              # %.noexc310
	movq	$0, (%r12)
	movq	(%rsp), %rax            # 8-byte Reload
.LBB18_69:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.preheader.i
	movb	$1, 96(%rax)
	movq	%r15, 88(%rax)
	movl	%r13d, 80(%rax)
	movl	16(%rsp), %eax          # 4-byte Reload
.LBB18_70:                              # %.lr.ph.i305
	movslq	%eax, %rbx
	movslq	%r13d, %rbp
	movl	%r13d, %r14d
	subl	%eax, %r14d
	leaq	-1(%rbp), %rax
	subq	%rbx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	andq	$3, %r14
	je	.LBB18_73
# BB#71:                                # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i.prol.preheader
	movq	%rbx, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,8), %r15
	negq	%r14
	.p2align	4, 0x90
.LBB18_72:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdi
	addq	%r15, %rdi
	movl	$144, %edx
	leaq	64(%rsp), %rsi
	callq	memcpy
	incq	%rbx
	addq	$144, %r15
	incq	%r14
	jne	.LBB18_72
.LBB18_73:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i.prol.loopexit
	cmpq	$3, 16(%rsp)            # 8-byte Folded Reload
	jb	.LBB18_76
# BB#74:                                # %.lr.ph.i305.new
	subq	%rbx, %rbp
	leaq	(%rbx,%rbx,8), %rbx
	shlq	$4, %rbx
	addq	$432, %rbx              # imm = 0x1B0
	leaq	64(%rsp), %r15
	.p2align	4, 0x90
.LBB18_75:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	leaq	-432(%rax,%rbx), %rdi
	movl	$144, %edx
	movq	%r15, %rsi
	callq	memcpy
	movq	(%r12), %rax
	leaq	-288(%rax,%rbx), %rdi
	movl	$144, %edx
	movq	%r15, %rsi
	callq	memcpy
	movq	(%r12), %rax
	leaq	-144(%rax,%rbx), %rdi
	movl	$144, %edx
	movq	%r15, %rsi
	callq	memcpy
	movq	(%r12), %rdi
	addq	%rbx, %rdi
	movl	$144, %edx
	movq	%r15, %rsi
	callq	memcpy
	addq	$576, %rbx              # imm = 0x240
	addq	$-4, %rbp
	jne	.LBB18_75
.LBB18_76:                              # %.loopexit480
	movq	312(%rsp), %r14
	movq	(%rsp), %r12            # 8-byte Reload
	movl	%r13d, 76(%r12)
	movl	304(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB18_91
# BB#77:                                # %.lr.ph501
	cltq
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movq	216(%r12), %rcx
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB18_78:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_83 Depth 2
                                        #     Child Loop BB18_87 Depth 2
	cmpl	$0, (%rcx,%rbx,8)
	je	.LBB18_89
# BB#79:                                #   in Loop: Header=BB18_78 Depth=1
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	88(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %r15
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movq	32(%rax), %r13
.Ltmp58:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObject
	movl	%eax, %r14d
.Ltmp59:
# BB#80:                                #   in Loop: Header=BB18_78 Depth=1
.Ltmp61:
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObject
.Ltmp62:
	movq	%r12, %rdx
# BB#81:                                #   in Loop: Header=BB18_78 Depth=1
	movl	%ebp, 52(%rsp)          # 4-byte Spill
	movslq	%ebp, %rcx
	leaq	(%rcx,%rcx,8), %rcx
	shlq	$4, %rcx
	movq	16(%rsp), %r10          # 8-byte Reload
	leaq	(%r10,%rcx), %r12
	movq	24(%rdx), %rdx
	movslq	%r14d, %rsi
	imulq	$112, %rsi, %r8
	addq	%rdx, %r8
	movslq	%eax, %rsi
	imulq	$112, %rsi, %r9
	addq	%rdx, %r9
	movq	56(%rsp), %rdi          # 8-byte Reload
	cmpl	$0, (%rdi,%rbx,8)
	xorps	%xmm0, %xmm0
	movss	.LCPI18_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	jle	.LBB18_84
# BB#82:                                # %.lr.ph493.preheader
                                        #   in Loop: Header=BB18_78 Depth=1
	movq	%r12, %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB18_83:                              # %.lr.ph493
                                        #   Parent Loop BB18_78 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm0, 128(%rbp)
	movups	%xmm0, 112(%rbp)
	movups	%xmm0, 96(%rbp)
	movups	%xmm0, 80(%rbp)
	movups	%xmm0, 64(%rbp)
	movups	%xmm0, 48(%rbp)
	movups	%xmm0, 32(%rbp)
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movl	$-8388609, 128(%rbp)    # imm = 0xFF7FFFFF
	movl	$2139095039, 132(%rbp)  # imm = 0x7F7FFFFF
	movl	$0, 84(%rbp)
	movl	$0, 80(%rbp)
	movl	%r14d, 104(%rbp)
	movl	%eax, 108(%rbp)
	incq	%rsi
	movslq	(%rdi,%rbx,8), %rdx
	addq	$144, %rbp
	cmpq	%rdx, %rsi
	jl	.LBB18_83
.LBB18_84:                              # %._crit_edge494
                                        #   in Loop: Header=BB18_78 Depth=1
	movups	%xmm0, 16(%r8)
	movups	%xmm0, (%r8)
	movups	%xmm0, 16(%r9)
	movups	%xmm0, (%r9)
	movaps	%xmm1, %xmm0
	movq	312(%rsp), %r14
	divss	12(%r14), %xmm0
	movss	%xmm0, 64(%rsp)
	movl	32(%r14), %eax
	movl	%eax, 68(%rsp)
	leaq	16(%r10,%rcx), %rax
	movq	%rax, 72(%rsp)
	movq	%r12, 80(%rsp)
	movq	$0, 88(%rsp)
	leaq	32(%r10,%rcx), %rax
	movq	%rax, 96(%rsp)
	movl	$36, 104(%rsp)
	leaq	120(%r10,%rcx), %rax
	movq	%rax, 112(%rsp)
	leaq	124(%r10,%rcx), %rax
	movq	%rax, 120(%rsp)
	leaq	128(%r10,%rcx), %rax
	movq	%rax, 128(%rsp)
	leaq	132(%r10,%rcx), %rax
	movq	%rax, 136(%rsp)
	movl	20(%r14), %eax
	movl	%eax, 152(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	movq	(%rdi), %rax
.Ltmp64:
	leaq	64(%rsp), %rsi
	callq	*40(%rax)
.Ltmp65:
# BB#85:                                # %.preheader479
                                        #   in Loop: Header=BB18_78 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	cmpl	$0, (%rdx,%rbx,8)
	movq	240(%rsp), %rsi         # 8-byte Reload
	jle	.LBB18_88
# BB#86:                                # %.lr.ph496
                                        #   in Loop: Header=BB18_78 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB18_87:                              #   Parent Loop BB18_78 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rsi), %rcx
	movss	(%r12), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	296(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	280(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	284(%rcx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movss	300(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm6
	movss	304(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	288(%rcx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0],xmm7[1],xmm4[1]
	movaps	%xmm0, %xmm4
	mulps	%xmm5, %xmm3
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm7, %xmm4
	mulss	312(%rcx), %xmm2
	addps	%xmm6, %xmm3
	mulss	316(%rcx), %xmm1
	addss	%xmm2, %xmm1
	mulss	320(%rcx), %xmm0
	addps	%xmm3, %xmm4
	addss	%xmm1, %xmm0
	movsd	364(%rcx), %xmm1        # xmm1 = mem[0],zero
	mulss	372(%rcx), %xmm0
	mulps	%xmm4, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 48(%r12)
	movlps	%xmm2, 56(%r12)
	movq	32(%rsi), %rcx
	movss	32(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	36(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	40(%r12), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movss	296(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	280(%rcx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	284(%rcx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm3
	movaps	%xmm4, %xmm5
	movaps	%xmm5, 16(%rsp)         # 16-byte Spill
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	300(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movaps	%xmm6, %xmm0
	mulps	%xmm1, %xmm3
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movss	304(%rcx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	288(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulps	%xmm2, %xmm0
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movaps	%xmm15, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	312(%rcx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulps	%xmm4, %xmm1
	mulss	%xmm5, %xmm2
	movss	316(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm4
	movaps	%xmm6, %xmm8
	addps	%xmm3, %xmm0
	addss	%xmm2, %xmm4
	movss	320(%rcx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm2
	addps	%xmm0, %xmm1
	addss	%xmm4, %xmm2
	movsd	364(%rcx), %xmm0        # xmm0 = mem[0],zero
	mulss	372(%rcx), %xmm2
	mulps	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm0, 64(%r12)
	movlps	%xmm1, 72(%r12)
	movss	360(%r15), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movss	16(%r12), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm9
	movss	280(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm2
	movss	%xmm2, 40(%rsp)         # 4-byte Spill
	movss	284(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm1
	movss	8(%r12), %xmm14         # xmm14 = mem[0],zero,zero,zero
	movss	288(%r15), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movss	296(%r15), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm10
	mulss	%xmm2, %xmm6
	movss	300(%r15), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm3
	addss	%xmm0, %xmm1
	movss	304(%r15), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm4
	movss	312(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm3
	mulss	%xmm2, %xmm0
	movss	316(%r15), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm6
	addss	%xmm0, %xmm6
	movss	320(%r15), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm5
	movss	280(%r13), %xmm7        # xmm7 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm10
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm1, %xmm7
	movss	284(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	mulss	%xmm2, %xmm0
	addss	%xmm3, %xmm4
	addss	%xmm7, %xmm0
	movss	288(%r13), %xmm8        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm8
	addss	%xmm6, %xmm5
	movss	296(%r13), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movss	300(%r13), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm2, 208(%rsp)        # 16-byte Spill
	mulss	%xmm2, %xmm6
	addss	%xmm3, %xmm6
	movss	304(%r13), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm3
	addss	%xmm0, %xmm8
	addss	%xmm6, %xmm3
	movss	312(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	316(%r13), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	mulss	%xmm2, %xmm6
	addss	%xmm0, %xmm6
	movss	320(%r13), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm9
	mulss	%xmm15, %xmm7
	movaps	%xmm15, %xmm1
	addss	%xmm6, %xmm7
	movss	20(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm15
	mulss	%xmm6, %xmm15
	mulss	%xmm12, %xmm9
	mulss	%xmm6, %xmm15
	addss	%xmm9, %xmm15
	movss	24(%r12), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm11
	mulss	%xmm9, %xmm11
	addss	%xmm15, %xmm11
	movss	40(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm10
	mulss	%xmm13, %xmm4
	addss	%xmm10, %xmm4
	movss	360(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm5
	addss	%xmm4, %xmm5
	movaps	%xmm12, %xmm4
	mulss	%xmm2, %xmm4
	movaps	%xmm6, %xmm0
	mulss	%xmm2, %xmm0
	mulss	%xmm12, %xmm4
	mulss	%xmm6, %xmm0
	addss	%xmm4, %xmm0
	mulss	%xmm9, %xmm2
	mulss	%xmm9, %xmm2
	addss	%xmm0, %xmm2
	mulss	16(%rsp), %xmm8         # 16-byte Folded Reload
	mulss	208(%rsp), %xmm3        # 16-byte Folded Reload
	addss	%xmm11, %xmm5
	addss	%xmm8, %xmm3
	mulss	%xmm1, %xmm7
	addss	%xmm3, %xmm7
	movss	.LCPI18_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm2
	addss	%xmm2, %xmm7
	movaps	%xmm0, %xmm1
	divss	%xmm7, %xmm1
	movss	%xmm1, 92(%r12)
	movss	328(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	movss	332(%r15), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	addss	%xmm0, %xmm2
	movss	336(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm0
	addss	%xmm2, %xmm0
	movaps	%xmm15, %xmm2
	mulss	344(%r15), %xmm2
	mulss	348(%r15), %xmm13
	addss	%xmm2, %xmm13
	mulss	352(%r15), %xmm14
	addss	%xmm13, %xmm14
	mulss	328(%r13), %xmm12
	mulss	332(%r13), %xmm6
	addss	%xmm12, %xmm6
	addss	%xmm0, %xmm14
	mulss	336(%r13), %xmm9
	addss	%xmm6, %xmm9
	movss	32(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	344(%r13), %xmm0
	mulss	348(%r13), %xmm2
	addss	%xmm0, %xmm2
	movss	40(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	352(%r13), %xmm0
	addss	%xmm2, %xmm0
	subss	%xmm9, %xmm0
	addss	%xmm14, %xmm0
	xorps	%xmm2, %xmm2
	subss	%xmm0, %xmm2
	movss	120(%r12), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm2
	addss	%xmm0, %xmm2
	movss	%xmm2, 120(%r12)
	movl	$0, 84(%r12)
	incq	%rax
	movslq	(%rdx,%rbx,8), %rcx
	addq	$144, %r12
	cmpq	%rcx, %rax
	jl	.LBB18_87
.LBB18_88:                              # %._crit_edge497
                                        #   in Loop: Header=BB18_78 Depth=1
	movq	(%rsp), %r12            # 8-byte Reload
	movq	216(%r12), %rcx
	movl	(%rcx,%rbx,8), %eax
	movl	52(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB18_90
	.p2align	4, 0x90
.LBB18_89:                              #   in Loop: Header=BB18_78 Depth=1
	xorl	%eax, %eax
.LBB18_90:                              #   in Loop: Header=BB18_78 Depth=1
	addl	%eax, %ebp
	incq	%rbx
	cmpq	224(%rsp), %rbx         # 8-byte Folded Reload
	jl	.LBB18_78
.LBB18_91:                              # %.preheader478
	movl	48(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB18_95
# BB#92:                                # %.lr.ph490.preheader
	movslq	%eax, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB18_93:                              # %.lr.ph490
                                        # =>This Inner Loop Header: Depth=1
	movq	232(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbp,8), %rsi
.Ltmp67:
	movq	%r12, %rdi
	movq	%r14, %rdx
	callq	_ZN35btSequentialImpulseConstraintSolver14convertContactEP20btPersistentManifoldRK19btContactSolverInfo
.Ltmp68:
# BB#94:                                #   in Loop: Header=BB18_93 Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB18_93
.LBB18_95:                              # %._crit_edge
	movq	%r12, %r15
	movl	44(%r15), %r12d
	movl	108(%r15), %r13d
	movl	140(%r15), %ebp
	cmpl	%r12d, %ebp
	jge	.LBB18_130
# BB#96:
	movslq	%r12d, %r14
	cmpl	%r12d, 144(%r15)
	jge	.LBB18_100
# BB#97:
	testl	%r12d, %r12d
	je	.LBB18_101
# BB#98:
	leaq	(,%r14,4), %rdi
.Ltmp70:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
.Ltmp71:
# BB#99:                                # %.noexc345
	movq	(%rsp), %rax            # 8-byte Reload
	movl	140(%rax), %ecx
	jmp	.LBB18_102
.LBB18_100:                             # %..lr.ph.i340_crit_edge
	movq	152(%r15), %r15
	jmp	.LBB18_129
.LBB18_101:
	xorl	%r15d, %r15d
	movl	%ebp, %ecx
	movq	(%rsp), %rax            # 8-byte Reload
.LBB18_102:                             # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i328
	movq	152(%rax), %rdi
	testl	%ecx, %ecx
	jle	.LBB18_105
# BB#103:                               # %.lr.ph.i.i.i330
	movslq	%ecx, %rax
	cmpl	$8, %ecx
	jae	.LBB18_107
# BB#104:
	xorl	%edx, %edx
	jmp	.LBB18_120
.LBB18_105:                             # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i334
	testq	%rdi, %rdi
	jne	.LBB18_126
# BB#106:                               # %_ZN20btAlignedObjectArrayIiE7reserveEi.exit.preheader.thread26.i336
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	160(%rax), %rbx
	jmp	.LBB18_128
.LBB18_107:                             # %min.iters.checked
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB18_111
# BB#108:                               # %vector.memcheck
	leaq	(%rdi,%rax,4), %rsi
	cmpq	%rsi, %r15
	jae	.LBB18_112
# BB#109:                               # %vector.memcheck
	leaq	(%r15,%rax,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB18_112
.LBB18_111:
	xorl	%edx, %edx
.LBB18_120:                             # %scalar.ph.preheader
	movl	%ebp, %ebx
	subl	%edx, %ecx
	leaq	-1(%rax), %rsi
	subq	%rdx, %rsi
	andq	$7, %rcx
	je	.LBB18_123
# BB#121:                               # %scalar.ph.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB18_122:                             # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %ebp
	movl	%ebp, (%r15,%rdx,4)
	incq	%rdx
	incq	%rcx
	jne	.LBB18_122
.LBB18_123:                             # %scalar.ph.prol.loopexit
	cmpq	$7, %rsi
	movl	%ebx, %ebp
	jb	.LBB18_126
# BB#124:                               # %scalar.ph.preheader.new
	subq	%rdx, %rax
	leaq	28(%rdi,%rdx,4), %rcx
	leaq	28(%r15,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB18_125:                             # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %rax
	jne	.LBB18_125
.LBB18_126:                             # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i337
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	160(%rax), %rbx
	cmpb	$0, 160(%rax)
	je	.LBB18_128
# BB#127:
.Ltmp72:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp73:
.LBB18_128:                             # %.lr.ph.i340.sink.split
	movb	$1, (%rbx)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%r15, 152(%rax)
	movl	%r12d, 144(%rax)
.LBB18_129:                             # %.lr.ph.i340
	movslq	%ebp, %rax
	leaq	(%r15,%rax,4), %rdi
	subq	%rax, %r14
	shlq	$2, %r14
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	(%rsp), %r15            # 8-byte Reload
.LBB18_130:                             # %.loopexit477
	movl	%r12d, 140(%r15)
	movl	172(%r15), %ebp
	cmpl	%r13d, %ebp
	jge	.LBB18_165
# BB#131:
	movslq	%r13d, %r14
	cmpl	%r13d, 176(%r15)
	jge	.LBB18_135
# BB#132:
	testl	%r13d, %r13d
	je	.LBB18_136
# BB#133:
	leaq	(,%r14,4), %rdi
.Ltmp75:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
.Ltmp76:
# BB#134:                               # %.noexc324
	movq	(%rsp), %rax            # 8-byte Reload
	movl	172(%rax), %ecx
	jmp	.LBB18_137
.LBB18_135:                             # %..lr.ph.i320_crit_edge
	movq	184(%r15), %r15
	jmp	.LBB18_164
.LBB18_136:
	xorl	%r15d, %r15d
	movl	%ebp, %ecx
	movq	(%rsp), %rax            # 8-byte Reload
.LBB18_137:                             # %_ZN20btAlignedObjectArrayIiE8allocateEi.exit.i.i
	movq	184(%rax), %rdi
	testl	%ecx, %ecx
	jle	.LBB18_140
# BB#138:                               # %.lr.ph.i.i.i314
	movslq	%ecx, %rax
	cmpl	$8, %ecx
	jae	.LBB18_142
# BB#139:
	xorl	%edx, %edx
	jmp	.LBB18_155
.LBB18_140:                             # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.i.i
	testq	%rdi, %rdi
	jne	.LBB18_161
# BB#141:                               # %_ZN20btAlignedObjectArrayIiE7reserveEi.exit.preheader.thread26.i
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	192(%rax), %rbx
	jmp	.LBB18_163
.LBB18_142:                             # %min.iters.checked549
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB18_146
# BB#143:                               # %vector.memcheck561
	leaq	(%rdi,%rax,4), %rsi
	cmpq	%rsi, %r15
	jae	.LBB18_147
# BB#144:                               # %vector.memcheck561
	leaq	(%r15,%rax,4), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB18_147
.LBB18_146:
	xorl	%edx, %edx
.LBB18_155:                             # %scalar.ph547.preheader
	movl	%ebp, %ebx
	subl	%edx, %ecx
	leaq	-1(%rax), %rsi
	subq	%rdx, %rsi
	andq	$7, %rcx
	je	.LBB18_158
# BB#156:                               # %scalar.ph547.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB18_157:                             # %scalar.ph547.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,4), %ebp
	movl	%ebp, (%r15,%rdx,4)
	incq	%rdx
	incq	%rcx
	jne	.LBB18_157
.LBB18_158:                             # %scalar.ph547.prol.loopexit
	cmpq	$7, %rsi
	movl	%ebx, %ebp
	jb	.LBB18_161
# BB#159:                               # %scalar.ph547.preheader.new
	subq	%rdx, %rax
	leaq	28(%rdi,%rdx,4), %rcx
	leaq	28(%r15,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB18_160:                             # %scalar.ph547
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-8, %rax
	jne	.LBB18_160
.LBB18_161:                             # %_ZNK20btAlignedObjectArrayIiE4copyEiiPi.exit.thread.i.i
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	192(%rax), %rbx
	cmpb	$0, 192(%rax)
	je	.LBB18_163
# BB#162:
.Ltmp77:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp78:
.LBB18_163:                             # %.lr.ph.i320.sink.split
	movb	$1, (%rbx)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%r15, 184(%rax)
	movl	%r13d, 176(%rax)
.LBB18_164:                             # %.lr.ph.i320
	movslq	%ebp, %rax
	leaq	(%r15,%rax,4), %rdi
	subq	%rax, %r14
	shlq	$2, %r14
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	movq	(%rsp), %r15            # 8-byte Reload
.LBB18_165:                             # %.loopexit476
	movl	%r13d, 172(%r15)
	testl	%r12d, %r12d
	jle	.LBB18_173
# BB#166:                               # %.lr.ph488
	movq	(%rsp), %rax            # 8-byte Reload
	movq	152(%rax), %rcx
	cmpl	$7, %r12d
	jbe	.LBB18_171
# BB#167:                               # %min.iters.checked577
	movl	%r12d, %eax
	andl	$7, %eax
	movq	%r12, %rsi
	subq	%rax, %rsi
	je	.LBB18_171
# BB#168:                               # %vector.body573.preheader
	leaq	16(%rcx), %rdx
	movdqa	.LCPI18_1(%rip), %xmm0  # xmm0 = [0,1,2,3]
	movdqa	.LCPI18_2(%rip), %xmm1  # xmm1 = [4,4,4,4]
	movdqa	.LCPI18_3(%rip), %xmm2  # xmm2 = [8,8,8,8]
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB18_169:                             # %vector.body573
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm3
	paddd	%xmm1, %xmm3
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm3, (%rdx)
	paddd	%xmm2, %xmm0
	addq	$32, %rdx
	addq	$-8, %rdi
	jne	.LBB18_169
# BB#170:                               # %middle.block574
	testl	%eax, %eax
	jne	.LBB18_172
	jmp	.LBB18_173
.LBB18_171:
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB18_172:                             # %scalar.ph575
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, (%rcx,%rsi,4)
	incq	%rsi
	cmpq	%rsi, %r12
	jne	.LBB18_172
.LBB18_173:                             # %.preheader
	testl	%r13d, %r13d
	movq	(%rsp), %rax            # 8-byte Reload
	jle	.LBB18_181
# BB#174:                               # %.lr.ph
	movq	184(%rax), %rax
	cmpl	$7, %r13d
	jbe	.LBB18_179
# BB#175:                               # %min.iters.checked595
	movl	%r13d, %edx
	andl	$7, %edx
	movq	%r13, %rcx
	subq	%rdx, %rcx
	je	.LBB18_179
# BB#176:                               # %vector.body591.preheader
	leaq	16(%rax), %rsi
	movdqa	.LCPI18_1(%rip), %xmm0  # xmm0 = [0,1,2,3]
	movdqa	.LCPI18_2(%rip), %xmm1  # xmm1 = [4,4,4,4]
	movdqa	.LCPI18_3(%rip), %xmm2  # xmm2 = [8,8,8,8]
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB18_177:                             # %vector.body591
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm3
	paddd	%xmm1, %xmm3
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	paddd	%xmm2, %xmm0
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB18_177
# BB#178:                               # %middle.block592
	testl	%edx, %edx
	jne	.LBB18_180
	jmp	.LBB18_181
.LBB18_179:
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB18_180:                             # %scalar.ph593
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, (%rax,%rcx,4)
	incq	%rcx
	cmpq	%rcx, %r13
	jne	.LBB18_180
.LBB18_181:                             # %.loopexit
	callq	_ZN15CProfileManager12Stop_ProfileEv
	xorps	%xmm0, %xmm0
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_112:                             # %vector.body.preheader
	movl	%ebp, %r8d
	leaq	-8(%rdx), %rbx
	movl	%ebx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB18_115
# BB#113:                               # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB18_114:                             # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp,4), %xmm0
	movups	16(%rdi,%rbp,4), %xmm1
	movups	%xmm0, (%r15,%rbp,4)
	movups	%xmm1, 16(%r15,%rbp,4)
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB18_114
	jmp	.LBB18_116
.LBB18_147:                             # %vector.body545.preheader
	movl	%ebp, %r8d
	leaq	-8(%rdx), %rbx
	movl	%ebx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB18_150
# BB#148:                               # %vector.body545.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB18_149:                             # %vector.body545.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp,4), %xmm0
	movups	16(%rdi,%rbp,4), %xmm1
	movups	%xmm0, (%r15,%rbp,4)
	movups	%xmm1, 16(%r15,%rbp,4)
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB18_149
	jmp	.LBB18_151
.LBB18_115:
	xorl	%ebp, %ebp
.LBB18_116:                             # %vector.body.prol.loopexit
	cmpq	$24, %rbx
	jb	.LBB18_119
# BB#117:                               # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rbp, %rsi
	leaq	112(%rdi,%rbp,4), %rbx
	leaq	112(%r15,%rbp,4), %rbp
.LBB18_118:                             # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rbx
	subq	$-128, %rbp
	addq	$-32, %rsi
	jne	.LBB18_118
.LBB18_119:                             # %middle.block
	cmpq	%rdx, %rax
	movl	%r8d, %ebp
	je	.LBB18_126
	jmp	.LBB18_120
.LBB18_150:
	xorl	%ebp, %ebp
.LBB18_151:                             # %vector.body545.prol.loopexit
	cmpq	$24, %rbx
	jb	.LBB18_154
# BB#152:                               # %vector.body545.preheader.new
	movq	%rdx, %rsi
	subq	%rbp, %rsi
	leaq	112(%rdi,%rbp,4), %rbx
	leaq	112(%r15,%rbp,4), %rbp
.LBB18_153:                             # %vector.body545
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rbx
	subq	$-128, %rbp
	addq	$-32, %rsi
	jne	.LBB18_153
.LBB18_154:                             # %middle.block546
	cmpq	%rdx, %rax
	movl	%r8d, %ebp
	je	.LBB18_161
	jmp	.LBB18_155
.LBB18_182:
.Ltmp57:
	jmp	.LBB18_193
.LBB18_183:
.Ltmp49:
	jmp	.LBB18_193
.LBB18_184:
.Ltmp44:
	jmp	.LBB18_193
.LBB18_185:
.Ltmp79:
	jmp	.LBB18_193
.LBB18_186:
.Ltmp74:
	jmp	.LBB18_193
.LBB18_187:
.Ltmp66:
	jmp	.LBB18_193
.LBB18_188:
.Ltmp60:
	jmp	.LBB18_193
.LBB18_189:
.Ltmp63:
	jmp	.LBB18_193
.LBB18_190:
.Ltmp69:
	jmp	.LBB18_193
.LBB18_191:
.Ltmp52:
	jmp	.LBB18_193
.LBB18_192:
.Ltmp39:
.LBB18_193:
	movq	%rax, %rbx
.Ltmp80:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp81:
# BB#194:                               # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB18_195:
.Ltmp82:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc, .Lfunc_end18-_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\200\002"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\367\001"              # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp37-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin2   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp43-.Ltmp40         #   Call between .Ltmp40 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin2   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp48-.Ltmp45         #   Call between .Ltmp45 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin2   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin2   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp54-.Ltmp53         #   Call between .Ltmp53 and .Ltmp54
	.long	.Ltmp57-.Lfunc_begin2   #     jumps to .Ltmp57
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin2   #     jumps to .Ltmp57
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp58-.Ltmp56         #   Call between .Ltmp56 and .Ltmp58
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin2   # >> Call Site 11 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin2   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin2   # >> Call Site 12 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin2   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin2   # >> Call Site 13 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin2   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin2   # >> Call Site 14 <<
	.long	.Ltmp73-.Ltmp70         #   Call between .Ltmp70 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin2   #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.long	.Ltmp73-.Lfunc_begin2   # >> Call Site 15 <<
	.long	.Ltmp75-.Ltmp73         #   Call between .Ltmp73 and .Ltmp75
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin2   # >> Call Site 16 <<
	.long	.Ltmp78-.Ltmp75         #   Call between .Ltmp75 and .Ltmp78
	.long	.Ltmp79-.Lfunc_begin2   #     jumps to .Ltmp79
	.byte	0                       #   On action: cleanup
	.long	.Ltmp78-.Lfunc_begin2   # >> Call Site 17 <<
	.long	.Ltmp80-.Ltmp78         #   Call between .Ltmp78 and .Ltmp80
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp80-.Lfunc_begin2   # >> Call Site 18 <<
	.long	.Ltmp81-.Ltmp80         #   Call between .Ltmp80 and .Ltmp81
	.long	.Ltmp82-.Lfunc_begin2   #     jumps to .Ltmp82
	.byte	1                       #   On action: 1
	.long	.Ltmp81-.Lfunc_begin2   # >> Call Site 19 <<
	.long	.Lfunc_end18-.Ltmp81    #   Call between .Ltmp81 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI19_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI19_1:
	.long	0                       # float 0
	.text
	.globl	_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc,@function
_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc: # @_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi95:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi96:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi98:
	.cfi_def_cfa_offset 96
.Lcfi99:
	.cfi_offset %rbx, -56
.Lcfi100:
	.cfi_offset %r12, -48
.Lcfi101:
	.cfi_offset %r13, -40
.Lcfi102:
	.cfi_offset %r14, -32
.Lcfi103:
	.cfi_offset %r15, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%rdi, %r12
	movq	104(%rsp), %r13
	movl	$.L.str.1, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	movl	20(%r13), %eax
	testl	%eax, %eax
	jle	.LBB19_45
# BB#1:                                 # %.lr.ph323
	movl	44(%r12), %r15d
	movl	108(%r12), %ebx
	movslq	96(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB19_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_6 Depth 2
                                        #     Child Loop BB19_16 Depth 2
                                        #     Child Loop BB19_27 Depth 2
                                        #     Child Loop BB19_31 Depth 2
                                        #     Child Loop BB19_37 Depth 2
                                        #     Child Loop BB19_41 Depth 2
                                        #     Child Loop BB19_58 Depth 2
                                        #     Child Loop BB19_62 Depth 2
                                        #     Child Loop BB19_68 Depth 2
                                        #     Child Loop BB19_72 Depth 2
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	testb	$7, %cl
	jne	.LBB19_24
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	movl	60(%r13), %eax
	andl	$1, %eax
	je	.LBB19_24
# BB#4:                                 # %.preheader268
                                        #   in Loop: Header=BB19_2 Depth=1
	testl	%r15d, %r15d
	jle	.LBB19_14
# BB#5:                                 # %.lr.ph291
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	152(%r12), %rsi
	movq	232(%r12), %rcx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB19_6:                               #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, %rbp
	movl	(%rsi,%rbp,4), %r8d
	leaq	1(%rbp), %rdi
	imull	$1664525, %ecx, %ecx    # imm = 0x19660D
	addl	$1013904223, %ecx       # imm = 0x3C6EF35F
	cmpq	$65536, %rdi            # imm = 0x10000
	movq	%rcx, %rax
	ja	.LBB19_12
# BB#7:                                 #   in Loop: Header=BB19_6 Depth=2
	movq	%rcx, %rax
	shrq	$16, %rax
	xorq	%rcx, %rax
	cmpq	$256, %rdi              # imm = 0x100
	ja	.LBB19_12
# BB#8:                                 #   in Loop: Header=BB19_6 Depth=2
	movq	%rax, %rdx
	shrq	$8, %rdx
	xorq	%rdx, %rax
	cmpq	$16, %rdi
	ja	.LBB19_12
# BB#9:                                 #   in Loop: Header=BB19_6 Depth=2
	movq	%rax, %rdx
	shrq	$4, %rdx
	xorq	%rdx, %rax
	cmpq	$4, %rdi
	ja	.LBB19_12
# BB#10:                                #   in Loop: Header=BB19_6 Depth=2
	movq	%rax, %rdx
	shrq	$2, %rdx
	xorq	%rdx, %rax
	cmpq	$2, %rdi
	ja	.LBB19_12
# BB#11:                                #   in Loop: Header=BB19_6 Depth=2
	movq	%rax, %rdx
	shrq	%rdx
	xorq	%rdx, %rax
	.p2align	4, 0x90
.LBB19_12:                              # %_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei.exit
                                        #   in Loop: Header=BB19_6 Depth=2
	xorl	%edx, %edx
	divq	%rdi
	movslq	%edx, %rax
	movl	(%rsi,%rax,4), %edx
	movl	%edx, (%rsi,%rbp,4)
	movl	%r8d, (%rsi,%rax,4)
	cmpq	%rdi, %r15
	jne	.LBB19_6
# BB#13:                                # %..preheader266_crit_edge
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	%rcx, 232(%r12)
.LBB19_14:                              # %.preheader266
                                        #   in Loop: Header=BB19_2 Depth=1
	testl	%ebx, %ebx
	jle	.LBB19_24
# BB#15:                                # %.lr.ph294
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	184(%r12), %rsi
	movq	232(%r12), %rcx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB19_16:                              #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, %rbp
	movl	(%rsi,%rbp,4), %r8d
	leaq	1(%rbp), %rdi
	imull	$1664525, %ecx, %ecx    # imm = 0x19660D
	addl	$1013904223, %ecx       # imm = 0x3C6EF35F
	cmpq	$65536, %rdi            # imm = 0x10000
	movq	%rcx, %rax
	ja	.LBB19_22
# BB#17:                                #   in Loop: Header=BB19_16 Depth=2
	movq	%rcx, %rax
	shrq	$16, %rax
	xorq	%rcx, %rax
	cmpq	$256, %rdi              # imm = 0x100
	ja	.LBB19_22
# BB#18:                                #   in Loop: Header=BB19_16 Depth=2
	movq	%rax, %rdx
	shrq	$8, %rdx
	xorq	%rdx, %rax
	cmpq	$16, %rdi
	ja	.LBB19_22
# BB#19:                                #   in Loop: Header=BB19_16 Depth=2
	movq	%rax, %rdx
	shrq	$4, %rdx
	xorq	%rdx, %rax
	cmpq	$4, %rdi
	ja	.LBB19_22
# BB#20:                                #   in Loop: Header=BB19_16 Depth=2
	movq	%rax, %rdx
	shrq	$2, %rdx
	xorq	%rdx, %rax
	cmpq	$2, %rdi
	ja	.LBB19_22
# BB#21:                                #   in Loop: Header=BB19_16 Depth=2
	movq	%rax, %rdx
	shrq	%rdx
	xorq	%rdx, %rax
	.p2align	4, 0x90
.LBB19_22:                              # %_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei.exit254
                                        #   in Loop: Header=BB19_16 Depth=2
	xorl	%edx, %edx
	divq	%rdi
	movslq	%edx, %rax
	movl	(%rsi,%rax,4), %edx
	movl	%edx, (%rsi,%rbp,4)
	movl	%r8d, (%rsi,%rax,4)
	cmpq	%rdi, %rbx
	jne	.LBB19_16
# BB#23:                                # %..loopexit267_crit_edge
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	%rcx, 232(%r12)
.LBB19_24:                              # %.loopexit267
                                        #   in Loop: Header=BB19_2 Depth=1
	testb	$1, 61(%r13)
	movl	76(%r12), %eax
	jne	.LBB19_25
# BB#56:                                # %.preheader262
                                        #   in Loop: Header=BB19_2 Depth=1
	testl	%eax, %eax
	jle	.LBB19_60
# BB#57:                                # %.lr.ph310.preheader
                                        #   in Loop: Header=BB19_2 Depth=1
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB19_58:                              # %.lr.ph310
                                        #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%r12), %rax
	movq	88(%r12), %rcx
	movslq	104(%rcx,%rbx), %rdx
	imulq	$112, %rdx, %rsi
	addq	%rax, %rsi
	movslq	108(%rcx,%rbx), %rdx
	leaq	(%rcx,%rbx), %rcx
	imulq	$112, %rdx, %rdx
	addq	%rax, %rdx
.Ltmp101:
	callq	_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint
.Ltmp102:
# BB#59:                                #   in Loop: Header=BB19_58 Depth=2
	incq	%rbp
	movslq	76(%r12), %rax
	addq	$144, %rbx
	cmpq	%rax, %rbp
	jl	.LBB19_58
.LBB19_60:                              # %.preheader260
                                        #   in Loop: Header=BB19_2 Depth=1
	cmpl	$0, 96(%rsp)
	jle	.LBB19_66
# BB#61:                                # %.lr.ph312.preheader
                                        #   in Loop: Header=BB19_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB19_62:                              # %.lr.ph312
                                        #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rbx,8), %rax
	movq	24(%rax), %rsi
.Ltmp104:
	movq	%r12, %rdi
	callq	_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObject
	movl	%eax, %ebp
.Ltmp105:
# BB#63:                                #   in Loop: Header=BB19_62 Depth=2
	movq	(%r14,%rbx,8), %rax
	movq	32(%rax), %rsi
.Ltmp107:
	movq	%r12, %rdi
	callq	_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObject
.Ltmp108:
# BB#64:                                #   in Loop: Header=BB19_62 Depth=2
	movq	24(%r12), %rcx
	movslq	%ebp, %rdx
	imulq	$112, %rdx, %rsi
	addq	%rcx, %rsi
	cltq
	imulq	$112, %rax, %rdx
	addq	%rcx, %rdx
	movq	(%r14,%rbx,8), %rdi
	movq	(%rdi), %rax
	movss	12(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
.Ltmp110:
	callq	*48(%rax)
.Ltmp111:
# BB#65:                                #   in Loop: Header=BB19_62 Depth=2
	incq	%rbx
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	jl	.LBB19_62
.LBB19_66:                              # %._crit_edge313
                                        #   in Loop: Header=BB19_2 Depth=1
	movslq	44(%r12), %rbx
	testq	%rbx, %rbx
	jle	.LBB19_70
# BB#67:                                # %.lr.ph316.preheader
                                        #   in Loop: Header=BB19_2 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB19_68:                              # %.lr.ph316
                                        #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	152(%r12), %rax
	movq	24(%r12), %rdi
	movq	56(%r12), %rdx
	movslq	(%rax,%rbp,4), %rax
	leaq	(%rax,%rax,8), %rax
	shlq	$4, %rax
	leaq	(%rdx,%rax), %rcx
	movslq	104(%rdx,%rax), %rsi
	imulq	$112, %rsi, %rsi
	addq	%rdi, %rsi
	movslq	108(%rdx,%rax), %rax
	imulq	$112, %rax, %rdx
	addq	%rdi, %rdx
.Ltmp113:
	callq	_ZN35btSequentialImpulseConstraintSolver36resolveSingleConstraintRowLowerLimitER12btSolverBodyS1_RK18btSolverConstraint
.Ltmp114:
# BB#69:                                #   in Loop: Header=BB19_68 Depth=2
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB19_68
.LBB19_70:                              # %._crit_edge317
                                        #   in Loop: Header=BB19_2 Depth=1
	movslq	108(%r12), %rbp
	testq	%rbp, %rbp
	jle	.LBB19_44
# BB#71:                                # %.lr.ph320.preheader
                                        #   in Loop: Header=BB19_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB19_72:                              # %.lr.ph320
                                        #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	184(%r12), %rcx
	movq	56(%r12), %rsi
	movq	120(%r12), %rax
	movslq	(%rcx,%rbx,4), %rcx
	leaq	(%rcx,%rcx,8), %rdx
	shlq	$4, %rdx
	movslq	100(%rax,%rdx), %rcx
	leaq	(%rcx,%rcx,8), %rcx
	shlq	$4, %rcx
	movss	84(%rsi,%rcx), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI19_1, %xmm0
	jbe	.LBB19_74
# BB#73:                                #   in Loop: Header=BB19_72 Depth=2
	leaq	(%rax,%rdx), %rcx
	mulss	88(%rax,%rdx), %xmm0
	movaps	%xmm0, %xmm1
	xorps	.LCPI19_0(%rip), %xmm1
	movss	%xmm1, 128(%rax,%rdx)
	movss	%xmm0, 132(%rax,%rdx)
	movq	24(%r12), %rdi
	movslq	104(%rax,%rdx), %rsi
	imulq	$112, %rsi, %rsi
	addq	%rdi, %rsi
	movslq	108(%rax,%rdx), %rax
	imulq	$112, %rax, %rdx
	addq	%rdi, %rdx
.Ltmp116:
	callq	_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint
.Ltmp117:
.LBB19_74:                              #   in Loop: Header=BB19_72 Depth=2
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB19_72
	jmp	.LBB19_44
	.p2align	4, 0x90
.LBB19_25:                              # %.preheader265
                                        #   in Loop: Header=BB19_2 Depth=1
	testl	%eax, %eax
	jle	.LBB19_29
# BB#26:                                # %.lr.ph298.preheader
                                        #   in Loop: Header=BB19_2 Depth=1
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB19_27:                              # %.lr.ph298
                                        #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%r12), %rax
	movq	88(%r12), %rcx
	movslq	104(%rcx,%rbx), %rdx
	imulq	$112, %rdx, %rsi
	addq	%rax, %rsi
	movslq	108(%rcx,%rbx), %rdx
	leaq	(%rcx,%rbx), %rcx
	imulq	$112, %rdx, %rdx
	addq	%rax, %rdx
.Ltmp83:
	callq	_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint
.Ltmp84:
# BB#28:                                # %_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint.exit255
                                        #   in Loop: Header=BB19_27 Depth=2
	incq	%rbp
	movslq	76(%r12), %rax
	addq	$144, %rbx
	cmpq	%rax, %rbp
	jl	.LBB19_27
.LBB19_29:                              # %.preheader264
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	%r13, %r15
	cmpl	$0, 96(%rsp)
	jle	.LBB19_35
# BB#30:                                # %.lr.ph300.preheader
                                        #   in Loop: Header=BB19_2 Depth=1
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB19_31:                              # %.lr.ph300
                                        #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r13,8), %rax
	movq	24(%rax), %rsi
.Ltmp86:
	movq	%r12, %rdi
	callq	_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObject
	movl	%eax, %ebp
.Ltmp87:
# BB#32:                                #   in Loop: Header=BB19_31 Depth=2
	movq	(%r14,%r13,8), %rax
	movq	32(%rax), %rsi
.Ltmp89:
	movq	%r12, %rdi
	callq	_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObject
.Ltmp90:
# BB#33:                                #   in Loop: Header=BB19_31 Depth=2
	movq	24(%r12), %rcx
	movslq	%ebp, %rdx
	imulq	$112, %rdx, %rsi
	addq	%rcx, %rsi
	cltq
	imulq	$112, %rax, %rdx
	addq	%rcx, %rdx
	movq	(%r14,%r13,8), %rdi
	movq	(%rdi), %rax
	movss	12(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
.Ltmp92:
	callq	*48(%rax)
.Ltmp93:
# BB#34:                                #   in Loop: Header=BB19_31 Depth=2
	incq	%r13
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	jl	.LBB19_31
.LBB19_35:                              # %._crit_edge301
                                        #   in Loop: Header=BB19_2 Depth=1
	movslq	44(%r12), %rbx
	testq	%rbx, %rbx
	movq	%r15, %r13
	movq	16(%rsp), %r15          # 8-byte Reload
	jle	.LBB19_39
# BB#36:                                # %.lr.ph304.preheader
                                        #   in Loop: Header=BB19_2 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB19_37:                              # %.lr.ph304
                                        #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	152(%r12), %rax
	movq	24(%r12), %rdi
	movq	56(%r12), %rdx
	movslq	(%rax,%rbp,4), %rax
	leaq	(%rax,%rax,8), %rax
	shlq	$4, %rax
	leaq	(%rdx,%rax), %rcx
	movslq	104(%rdx,%rax), %rsi
	imulq	$112, %rsi, %rsi
	addq	%rdi, %rsi
	movslq	108(%rdx,%rax), %rax
	imulq	$112, %rax, %rdx
	addq	%rdi, %rdx
.Ltmp95:
	callq	_ZN35btSequentialImpulseConstraintSolver36resolveSingleConstraintRowLowerLimitER12btSolverBodyS1_RK18btSolverConstraint
.Ltmp96:
# BB#38:                                # %_ZN35btSequentialImpulseConstraintSolver40resolveSingleConstraintRowLowerLimitSIMDER12btSolverBodyS1_RK18btSolverConstraint.exit
                                        #   in Loop: Header=BB19_37 Depth=2
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB19_37
.LBB19_39:                              # %._crit_edge305
                                        #   in Loop: Header=BB19_2 Depth=1
	movslq	108(%r12), %rbp
	testq	%rbp, %rbp
	jle	.LBB19_44
# BB#40:                                # %.lr.ph308.preheader
                                        #   in Loop: Header=BB19_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB19_41:                              # %.lr.ph308
                                        #   Parent Loop BB19_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	184(%r12), %rcx
	movq	56(%r12), %rsi
	movq	120(%r12), %rax
	movslq	(%rcx,%rbx,4), %rcx
	leaq	(%rcx,%rcx,8), %rdx
	shlq	$4, %rdx
	movslq	100(%rax,%rdx), %rcx
	leaq	(%rcx,%rcx,8), %rcx
	shlq	$4, %rcx
	movss	84(%rsi,%rcx), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI19_1, %xmm0
	jbe	.LBB19_43
# BB#42:                                #   in Loop: Header=BB19_41 Depth=2
	leaq	(%rax,%rdx), %rcx
	mulss	88(%rax,%rdx), %xmm0
	movaps	%xmm0, %xmm1
	xorps	.LCPI19_0(%rip), %xmm1
	movss	%xmm1, 128(%rax,%rdx)
	movss	%xmm0, 132(%rax,%rdx)
	movq	24(%r12), %rdi
	movslq	104(%rax,%rdx), %rsi
	imulq	$112, %rsi, %rsi
	addq	%rdi, %rsi
	movslq	108(%rax,%rdx), %rax
	imulq	$112, %rax, %rdx
	addq	%rdi, %rdx
.Ltmp98:
	callq	_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint
.Ltmp99:
.LBB19_43:                              # %_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint.exit
                                        #   in Loop: Header=BB19_41 Depth=2
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB19_41
.LBB19_44:                              # %.loopexit259
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	incl	%ecx
	movl	20(%r13), %eax
	cmpl	%eax, %ecx
	movq	24(%rsp), %rbx          # 8-byte Reload
	jl	.LBB19_2
.LBB19_45:                              # %._crit_edge324
	cmpl	$0, 44(%r13)
	je	.LBB19_55
# BB#46:
	testb	$1, 61(%r13)
	jne	.LBB19_47
# BB#83:                                # %.preheader
	testl	%eax, %eax
	jle	.LBB19_55
# BB#84:                                # %.lr.ph283
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB19_85:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_87 Depth 2
	movslq	44(%r12), %rbx
	testq	%rbx, %rbx
	jle	.LBB19_90
# BB#86:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB19_85 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB19_87:                              # %.lr.ph
                                        #   Parent Loop BB19_85 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	152(%r12), %rax
	movq	24(%r12), %rdi
	movq	56(%r12), %rdx
	movslq	(%rax,%rbp,4), %rax
	leaq	(%rax,%rax,8), %rax
	shlq	$4, %rax
	leaq	(%rdx,%rax), %rcx
	movslq	104(%rdx,%rax), %rsi
	imulq	$112, %rsi, %rsi
	addq	%rdi, %rsi
	movslq	108(%rdx,%rax), %rax
	imulq	$112, %rax, %rdx
	addq	%rdi, %rdx
.Ltmp122:
	callq	_ZN35btSequentialImpulseConstraintSolver43resolveSplitPenetrationImpulseCacheFriendlyER12btSolverBodyS1_RK18btSolverConstraint
.Ltmp123:
# BB#88:                                #   in Loop: Header=BB19_87 Depth=2
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB19_87
# BB#89:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB19_85 Depth=1
	movl	20(%r13), %eax
.LBB19_90:                              # %._crit_edge
                                        #   in Loop: Header=BB19_85 Depth=1
	incl	%r14d
	cmpl	%eax, %r14d
	jl	.LBB19_85
	jmp	.LBB19_55
.LBB19_47:                              # %.preheader257
	testl	%eax, %eax
	jle	.LBB19_55
# BB#48:                                # %.lr.ph289
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB19_49:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_51 Depth 2
	movslq	44(%r12), %rbx
	testq	%rbx, %rbx
	jle	.LBB19_54
# BB#50:                                # %.lr.ph286.preheader
                                        #   in Loop: Header=BB19_49 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB19_51:                              # %.lr.ph286
                                        #   Parent Loop BB19_49 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	152(%r12), %rax
	movq	24(%r12), %rdi
	movq	56(%r12), %rdx
	movslq	(%rax,%rbp,4), %rax
	leaq	(%rax,%rax,8), %rax
	shlq	$4, %rax
	leaq	(%rdx,%rax), %rcx
	movslq	104(%rdx,%rax), %rsi
	imulq	$112, %rsi, %rsi
	addq	%rdi, %rsi
	movslq	108(%rdx,%rax), %rax
	imulq	$112, %rax, %rdx
	addq	%rdi, %rdx
.Ltmp119:
	callq	_ZN35btSequentialImpulseConstraintSolver43resolveSplitPenetrationImpulseCacheFriendlyER12btSolverBodyS1_RK18btSolverConstraint
.Ltmp120:
# BB#52:                                # %_ZN35btSequentialImpulseConstraintSolver27resolveSplitPenetrationSIMDER12btSolverBodyS1_RK18btSolverConstraint.exit
                                        #   in Loop: Header=BB19_51 Depth=2
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB19_51
# BB#53:                                # %._crit_edge287.loopexit
                                        #   in Loop: Header=BB19_49 Depth=1
	movl	20(%r13), %eax
.LBB19_54:                              # %._crit_edge287
                                        #   in Loop: Header=BB19_49 Depth=1
	incl	%r14d
	cmpl	%eax, %r14d
	jl	.LBB19_49
.LBB19_55:                              # %.loopexit
	callq	_ZN15CProfileManager12Stop_ProfileEv
	xorps	%xmm0, %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB19_78:
.Ltmp100:
	jmp	.LBB19_94
.LBB19_82:
.Ltmp118:
	jmp	.LBB19_94
.LBB19_91:
.Ltmp121:
	jmp	.LBB19_94
.LBB19_92:
.Ltmp124:
	jmp	.LBB19_94
.LBB19_75:
.Ltmp91:
	jmp	.LBB19_94
.LBB19_97:                              # %.loopexit.split-lp
.Ltmp88:
	jmp	.LBB19_94
.LBB19_80:
.Ltmp112:
	jmp	.LBB19_94
.LBB19_79:
.Ltmp109:
	jmp	.LBB19_94
.LBB19_93:                              # %.loopexit261
.Ltmp106:
	jmp	.LBB19_94
.LBB19_76:
.Ltmp94:
	jmp	.LBB19_94
.LBB19_81:
.Ltmp115:
	jmp	.LBB19_94
.LBB19_77:
.Ltmp97:
	jmp	.LBB19_94
.LBB19_99:
.Ltmp103:
	jmp	.LBB19_94
.LBB19_98:
.Ltmp85:
.LBB19_94:
	movq	%rax, %rbx
.Ltmp125:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp126:
# BB#95:                                # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB19_96:
.Ltmp127:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end19:
	.size	_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc, .Lfunc_end19-_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\363\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\352\001"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp101-.Lfunc_begin3  #   Call between .Lfunc_begin3 and .Ltmp101
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin3  #     jumps to .Ltmp103
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin3  #     jumps to .Ltmp106
	.byte	0                       #   On action: cleanup
	.long	.Ltmp107-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Ltmp108-.Ltmp107       #   Call between .Ltmp107 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin3  #     jumps to .Ltmp109
	.byte	0                       #   On action: cleanup
	.long	.Ltmp110-.Lfunc_begin3  # >> Call Site 5 <<
	.long	.Ltmp111-.Ltmp110       #   Call between .Ltmp110 and .Ltmp111
	.long	.Ltmp112-.Lfunc_begin3  #     jumps to .Ltmp112
	.byte	0                       #   On action: cleanup
	.long	.Ltmp113-.Lfunc_begin3  # >> Call Site 6 <<
	.long	.Ltmp114-.Ltmp113       #   Call between .Ltmp113 and .Ltmp114
	.long	.Ltmp115-.Lfunc_begin3  #     jumps to .Ltmp115
	.byte	0                       #   On action: cleanup
	.long	.Ltmp116-.Lfunc_begin3  # >> Call Site 7 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin3  #     jumps to .Ltmp118
	.byte	0                       #   On action: cleanup
	.long	.Ltmp83-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp84-.Ltmp83         #   Call between .Ltmp83 and .Ltmp84
	.long	.Ltmp85-.Lfunc_begin3   #     jumps to .Ltmp85
	.byte	0                       #   On action: cleanup
	.long	.Ltmp86-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp87-.Ltmp86         #   Call between .Ltmp86 and .Ltmp87
	.long	.Ltmp88-.Lfunc_begin3   #     jumps to .Ltmp88
	.byte	0                       #   On action: cleanup
	.long	.Ltmp89-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Ltmp90-.Ltmp89         #   Call between .Ltmp89 and .Ltmp90
	.long	.Ltmp91-.Lfunc_begin3   #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin3   # >> Call Site 11 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin3   #     jumps to .Ltmp94
	.byte	0                       #   On action: cleanup
	.long	.Ltmp95-.Lfunc_begin3   # >> Call Site 12 <<
	.long	.Ltmp96-.Ltmp95         #   Call between .Ltmp95 and .Ltmp96
	.long	.Ltmp97-.Lfunc_begin3   #     jumps to .Ltmp97
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin3   # >> Call Site 13 <<
	.long	.Ltmp99-.Ltmp98         #   Call between .Ltmp98 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin3  #     jumps to .Ltmp100
	.byte	0                       #   On action: cleanup
	.long	.Ltmp122-.Lfunc_begin3  # >> Call Site 14 <<
	.long	.Ltmp123-.Ltmp122       #   Call between .Ltmp122 and .Ltmp123
	.long	.Ltmp124-.Lfunc_begin3  #     jumps to .Ltmp124
	.byte	0                       #   On action: cleanup
	.long	.Ltmp119-.Lfunc_begin3  # >> Call Site 15 <<
	.long	.Ltmp120-.Ltmp119       #   Call between .Ltmp119 and .Ltmp120
	.long	.Ltmp121-.Lfunc_begin3  #     jumps to .Ltmp121
	.byte	0                       #   On action: cleanup
	.long	.Ltmp120-.Lfunc_begin3  # >> Call Site 16 <<
	.long	.Ltmp125-.Ltmp120       #   Call between .Ltmp120 and .Ltmp125
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp125-.Lfunc_begin3  # >> Call Site 17 <<
	.long	.Ltmp126-.Ltmp125       #   Call between .Ltmp125 and .Ltmp126
	.long	.Ltmp127-.Lfunc_begin3  #     jumps to .Ltmp127
	.byte	1                       #   On action: 1
	.long	.Ltmp126-.Lfunc_begin3  # >> Call Site 18 <<
	.long	.Lfunc_end19-.Ltmp126   #   Call between .Ltmp126 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAllocP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAllocP12btDispatcher,@function
_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAllocP12btDispatcher: # @_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAllocP12btDispatcher
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi108:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi109:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi111:
	.cfi_def_cfa_offset 208
.Lcfi112:
	.cfi_offset %rbx, -56
.Lcfi113:
	.cfi_offset %r12, -48
.Lcfi114:
	.cfi_offset %r13, -40
.Lcfi115:
	.cfi_offset %r14, -32
.Lcfi116:
	.cfi_offset %r15, -24
.Lcfi117:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movl	%r8d, %ebx
	movq	%rcx, %rbp
	movq	%rdi, %r15
	movq	216(%rsp), %r13
	movl	208(%rsp), %r12d
.Lcfi118:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.2, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
.Ltmp128:
.Lcfi119:
	.cfi_escape 0x2e, 0x20
	subq	$16, %rsp
.Lcfi120:
	.cfi_adjust_cfa_offset 16
	movq	%r15, %rdi
	movq	%rbp, %rcx
	movl	%ebx, %r8d
	movq	%r14, %r9
	pushq	%r13
.Lcfi121:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi122:
	.cfi_adjust_cfa_offset 8
	callq	_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc
	addq	$32, %rsp
.Lcfi123:
	.cfi_adjust_cfa_offset -32
.Ltmp129:
# BB#1:
.Ltmp130:
.Lcfi124:
	.cfi_escape 0x2e, 0x20
	subq	$16, %rsp
.Lcfi125:
	.cfi_adjust_cfa_offset 16
	movq	%r15, %rdi
	movq	%r14, %r9
	pushq	%r13
.Lcfi126:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi127:
	.cfi_adjust_cfa_offset 8
	callq	_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc
	addq	$32, %rsp
.Lcfi128:
	.cfi_adjust_cfa_offset -32
.Ltmp131:
# BB#2:
	movl	44(%r15), %eax
	testl	%eax, %eax
	jle	.LBB20_13
# BB#3:                                 # %.lr.ph135
	movq	56(%r15), %rcx
	testb	$8, 60(%r13)
	jne	.LBB20_11
# BB#4:                                 # %.lr.ph135.split.us.preheader
	leaq	-1(%rax), %r8
	movq	%rax, %rdi
	andq	$3, %rdi
	je	.LBB20_5
# BB#6:                                 # %.lr.ph135.split.us.prol.preheader
	leaq	112(%rcx), %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB20_7:                               # %.lr.ph135.split.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbx
	movl	-28(%rbp), %edx
	movl	%edx, 120(%rbx)
	incq	%rsi
	addq	$144, %rbp
	cmpq	%rsi, %rdi
	jne	.LBB20_7
	jmp	.LBB20_8
.LBB20_11:                              # %.lr.ph135.split.preheader
	movq	120(%r15), %rdx
	addq	$112, %rcx
	.p2align	4, 0x90
.LBB20_12:                              # %.lr.ph135.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rsi
	movl	-28(%rcx), %edi
	movl	%edi, 120(%rsi)
	movslq	-12(%rcx), %rdi
	leaq	(%rdi,%rdi,8), %rdi
	shlq	$4, %rdi
	movl	84(%rdx,%rdi), %ebp
	movl	%ebp, 128(%rsi)
	movl	228(%rdx,%rdi), %edi
	movl	%edi, 132(%rsi)
	addq	$144, %rcx
	decq	%rax
	jne	.LBB20_12
	jmp	.LBB20_13
.LBB20_5:
	xorl	%esi, %esi
.LBB20_8:                               # %.lr.ph135.split.us.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB20_13
# BB#9:                                 # %.lr.ph135.split.us.preheader.new
	subq	%rsi, %rax
	leaq	(%rsi,%rsi,8), %rdx
	shlq	$4, %rdx
	leaq	544(%rcx,%rdx), %rcx
	.p2align	4, 0x90
.LBB20_10:                              # %.lr.ph135.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-432(%rcx), %rdx
	movl	-460(%rcx), %esi
	movl	%esi, 120(%rdx)
	movq	-288(%rcx), %rdx
	movl	-316(%rcx), %esi
	movl	%esi, 120(%rdx)
	movq	-144(%rcx), %rdx
	movl	-172(%rcx), %esi
	movl	%esi, 120(%rdx)
	movq	(%rcx), %rdx
	movl	-28(%rcx), %esi
	movl	%esi, 120(%rdx)
	addq	$576, %rcx              # imm = 0x240
	addq	$-4, %rax
	jne	.LBB20_10
.LBB20_13:                              # %._crit_edge
	cmpl	$0, 44(%r13)
	movl	12(%r15), %r13d
	je	.LBB20_22
# BB#14:                                # %.preheader129
	testl	%r13d, %r13d
	jle	.LBB20_27
# BB#15:                                # %.lr.ph133
	leaq	16(%rsp), %rbx
	xorl	%ebp, %ebp
	movl	$96, %r12d
	.p2align	4, 0x90
.LBB20_16:                              # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %r14
	movq	-24(%r14,%r12), %rax
	testq	%rax, %rax
	je	.LBB20_19
# BB#17:                                #   in Loop: Header=BB20_16 Depth=1
	movq	216(%rsp), %rcx
	movss	12(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movsd	328(%rax), %xmm1        # xmm1 = mem[0],zero
	leaq	(%r14,%r12), %rdx
	movsd	-96(%r14,%r12), %xmm2   # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	movss	336(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addss	-88(%r14,%r12), %xmm1
	xorps	%xmm3, %xmm3
	movss	%xmm1, %xmm3            # xmm3 = xmm1[0],xmm3[1,2,3]
	movlps	%xmm2, 328(%rax)
	movlps	%xmm3, 336(%rax)
	movq	-24(%r14,%r12), %rax
	movsd	344(%rax), %xmm1        # xmm1 = mem[0],zero
	movsd	-80(%r14,%r12), %xmm2   # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	movss	352(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addss	-72(%r14,%r12), %xmm1
	xorps	%xmm3, %xmm3
	movss	%xmm1, %xmm3            # xmm3 = xmm1[0],xmm3[1,2,3]
	movlps	%xmm2, 344(%rax)
	movlps	%xmm3, 352(%rax)
	movq	-24(%r14,%r12), %rdi
	addq	$8, %rdi
	leaq	-16(%r14,%r12), %rsi
.Ltmp133:
.Lcfi129:
	.cfi_escape 0x2e, 0x00
	movq	%rsp, %rcx
	callq	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
.Ltmp134:
# BB#18:                                # %.noexc87
                                        #   in Loop: Header=BB20_16 Depth=1
	movq	-24(%r14,%r12), %rax
	movups	(%rsp), %xmm0
	movups	%xmm0, 8(%rax)
	movups	(%rbx), %xmm0
	movups	%xmm0, 24(%rax)
	movups	16(%rbx), %xmm0
	movups	%xmm0, 40(%rax)
	movups	32(%rbx), %xmm0
	movups	%xmm0, 56(%rax)
	movl	12(%r15), %r13d
.LBB20_19:                              # %_ZN12btSolverBody17writebackVelocityEf.exit
                                        #   in Loop: Header=BB20_16 Depth=1
	incq	%rbp
	movslq	%r13d, %rax
	addq	$112, %r12
	cmpq	%rax, %rbp
	jl	.LBB20_16
	jmp	.LBB20_27
.LBB20_22:                              # %.preheader
	testl	%r13d, %r13d
	jle	.LBB20_27
# BB#23:                                # %.lr.ph
	xorl	%eax, %eax
	movl	$72, %ecx
	.p2align	4, 0x90
.LBB20_24:                              # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rdx
	movq	(%rdx,%rcx), %rsi
	testq	%rsi, %rsi
	je	.LBB20_26
# BB#25:                                #   in Loop: Header=BB20_24 Depth=1
	movsd	328(%rsi), %xmm0        # xmm0 = mem[0],zero
	movsd	-72(%rdx,%rcx), %xmm1   # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movss	336(%rsi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addss	-64(%rdx,%rcx), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 328(%rsi)
	movlps	%xmm2, 336(%rsi)
	movq	(%rdx,%rcx), %rsi
	movsd	344(%rsi), %xmm0        # xmm0 = mem[0],zero
	movsd	-56(%rdx,%rcx), %xmm1   # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movss	352(%rsi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addss	-48(%rdx,%rcx), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 344(%rsi)
	movlps	%xmm2, 352(%rsi)
	movl	12(%r15), %r13d
.LBB20_26:                              # %_ZN12btSolverBody17writebackVelocityEv.exit
                                        #   in Loop: Header=BB20_24 Depth=1
	incq	%rax
	movslq	%r13d, %rdx
	addq	$112, %rcx
	cmpq	%rdx, %rax
	jl	.LBB20_24
.LBB20_27:                              # %.loopexit128
	xorps	%xmm4, %xmm4
	movaps	%xmm4, 96(%rsp)
	movaps	%xmm4, 80(%rsp)
	movaps	%xmm4, 64(%rsp)
	movaps	%xmm4, 48(%rsp)
	movaps	%xmm4, 32(%rsp)
	movaps	%xmm4, 16(%rsp)
	movaps	%xmm4, (%rsp)
	testl	%r13d, %r13d
	jns	.LBB20_40
# BB#28:
	cmpl	$0, 16(%r15)
	jns	.LBB20_34
# BB#29:                                # %_ZNK20btAlignedObjectArrayI12btSolverBodyE4copyEiiPS0_.exit.i.i
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB20_33
# BB#30:
	cmpb	$0, 32(%r15)
	je	.LBB20_32
# BB#31:
.Ltmp136:
.Lcfi130:
	.cfi_escape 0x2e, 0x00
	callq	_Z21btAlignedFreeInternalPv
	xorps	%xmm4, %xmm4
.Ltmp137:
.LBB20_32:                              # %.noexc98
	movq	$0, 24(%r15)
.LBB20_33:                              # %_ZN20btAlignedObjectArrayI12btSolverBodyE7reserveEi.exit.preheader.i
	movb	$1, 32(%r15)
	movq	$0, 24(%r15)
	movl	$0, 16(%r15)
.LBB20_34:                              # %.lr.ph.i94
	movslq	%r13d, %rax
	movl	%r13d, %ecx
	negl	%ecx
	andq	$3, %rcx
	je	.LBB20_37
# BB#35:                                # %_ZN20btAlignedObjectArrayI12btSolverBodyE7reserveEi.exit.i.prol.preheader
	imulq	$112, %rax, %rdx
	negq	%rcx
	.p2align	4, 0x90
.LBB20_36:                              # %_ZN20btAlignedObjectArrayI12btSolverBodyE7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rsi
	movaps	96(%rsp), %xmm0
	movups	%xmm0, 96(%rsi,%rdx)
	movaps	80(%rsp), %xmm0
	movups	%xmm0, 80(%rsi,%rdx)
	movaps	64(%rsp), %xmm0
	movups	%xmm0, 64(%rsi,%rdx)
	movaps	(%rsp), %xmm0
	movaps	16(%rsp), %xmm1
	movaps	32(%rsp), %xmm2
	movaps	48(%rsp), %xmm3
	movups	%xmm3, 48(%rsi,%rdx)
	movups	%xmm2, 32(%rsi,%rdx)
	movups	%xmm1, 16(%rsi,%rdx)
	movups	%xmm0, (%rsi,%rdx)
	incq	%rax
	addq	$112, %rdx
	incq	%rcx
	jne	.LBB20_36
.LBB20_37:                              # %_ZN20btAlignedObjectArrayI12btSolverBodyE7reserveEi.exit.i.prol.loopexit
	cmpl	$-4, %r13d
	ja	.LBB20_40
# BB#38:                                # %.lr.ph.i94.new
	imulq	$112, %rax, %rcx
	addq	$336, %rcx              # imm = 0x150
	.p2align	4, 0x90
.LBB20_39:                              # %_ZN20btAlignedObjectArrayI12btSolverBodyE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rdx
	movaps	96(%rsp), %xmm0
	movups	%xmm0, -240(%rdx,%rcx)
	movaps	80(%rsp), %xmm0
	movups	%xmm0, -256(%rdx,%rcx)
	movaps	64(%rsp), %xmm0
	movups	%xmm0, -272(%rdx,%rcx)
	movaps	(%rsp), %xmm0
	movaps	16(%rsp), %xmm1
	movaps	32(%rsp), %xmm2
	movaps	48(%rsp), %xmm3
	movups	%xmm3, -288(%rdx,%rcx)
	movups	%xmm2, -304(%rdx,%rcx)
	movups	%xmm1, -320(%rdx,%rcx)
	movups	%xmm0, -336(%rdx,%rcx)
	movq	24(%r15), %rdx
	movaps	96(%rsp), %xmm0
	movups	%xmm0, -128(%rdx,%rcx)
	movaps	80(%rsp), %xmm0
	movups	%xmm0, -144(%rdx,%rcx)
	movaps	64(%rsp), %xmm0
	movups	%xmm0, -160(%rdx,%rcx)
	movaps	(%rsp), %xmm0
	movaps	16(%rsp), %xmm1
	movaps	32(%rsp), %xmm2
	movaps	48(%rsp), %xmm3
	movups	%xmm3, -176(%rdx,%rcx)
	movups	%xmm2, -192(%rdx,%rcx)
	movups	%xmm1, -208(%rdx,%rcx)
	movups	%xmm0, -224(%rdx,%rcx)
	movq	24(%r15), %rdx
	movaps	96(%rsp), %xmm0
	movups	%xmm0, -16(%rdx,%rcx)
	movaps	80(%rsp), %xmm0
	movups	%xmm0, -32(%rdx,%rcx)
	movaps	64(%rsp), %xmm0
	movups	%xmm0, -48(%rdx,%rcx)
	movaps	(%rsp), %xmm0
	movaps	16(%rsp), %xmm1
	movaps	32(%rsp), %xmm2
	movaps	48(%rsp), %xmm3
	movups	%xmm3, -64(%rdx,%rcx)
	movups	%xmm2, -80(%rdx,%rcx)
	movups	%xmm1, -96(%rdx,%rcx)
	movups	%xmm0, -112(%rdx,%rcx)
	movq	24(%r15), %rdx
	movaps	96(%rsp), %xmm0
	movups	%xmm0, 96(%rdx,%rcx)
	movaps	80(%rsp), %xmm0
	movups	%xmm0, 80(%rdx,%rcx)
	movaps	64(%rsp), %xmm0
	movups	%xmm0, 64(%rdx,%rcx)
	movaps	(%rsp), %xmm0
	movaps	16(%rsp), %xmm1
	movaps	32(%rsp), %xmm2
	movaps	48(%rsp), %xmm3
	movups	%xmm3, 48(%rdx,%rcx)
	movups	%xmm2, 32(%rdx,%rcx)
	movups	%xmm1, 16(%rdx,%rcx)
	movups	%xmm0, (%rdx,%rcx)
	addq	$448, %rcx              # imm = 0x1C0
	addq	$4, %rax
	jne	.LBB20_39
.LBB20_40:                              # %.loopexit127
	movl	$0, 12(%r15)
	movaps	%xmm4, 128(%rsp)
	movaps	%xmm4, 112(%rsp)
	movaps	%xmm4, 96(%rsp)
	movaps	%xmm4, 80(%rsp)
	movaps	%xmm4, 64(%rsp)
	movaps	%xmm4, 48(%rsp)
	movaps	%xmm4, 32(%rsp)
	movaps	%xmm4, 16(%rsp)
	movaps	%xmm4, (%rsp)
	movslq	44(%r15), %r12
	testq	%r12, %r12
	jns	.LBB20_53
# BB#41:
	cmpl	$0, 48(%r15)
	jns	.LBB20_47
# BB#42:                                # %_ZNK20btAlignedObjectArrayI18btSolverConstraintE4copyEiiPS0_.exit.i.i78
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB20_46
# BB#43:
	cmpb	$0, 64(%r15)
	je	.LBB20_45
# BB#44:
.Ltmp139:
.Lcfi131:
	.cfi_escape 0x2e, 0x00
	callq	_Z21btAlignedFreeInternalPv
.Ltmp140:
.LBB20_45:                              # %.noexc85
	movq	$0, 56(%r15)
.LBB20_46:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.preheader.i79
	movb	$1, 64(%r15)
	movq	$0, 56(%r15)
	movl	$0, 48(%r15)
.LBB20_47:                              # %.lr.ph.i80
	movl	%r12d, %r13d
	negl	%r13d
	andq	$3, %r13
	movq	%r12, %rbp
	je	.LBB20_50
# BB#48:                                # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i84.prol.preheader
	movq	%r12, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,8), %rbx
	negq	%r13
	movq	%rsp, %r14
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB20_49:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i84.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rdi
	addq	%rbx, %rdi
.Lcfi132:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	incq	%rbp
	addq	$144, %rbx
	incq	%r13
	jne	.LBB20_49
.LBB20_50:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i84.prol.loopexit
	cmpl	$-4, %r12d
	ja	.LBB20_53
# BB#51:                                # %.lr.ph.i80.new
	leaq	(%rbp,%rbp,8), %rbx
	shlq	$4, %rbx
	addq	$432, %rbx              # imm = 0x1B0
	movq	%rsp, %r14
	.p2align	4, 0x90
.LBB20_52:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i84
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	leaq	-432(%rax,%rbx), %rdi
.Lcfi133:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	movq	56(%r15), %rax
	leaq	-288(%rax,%rbx), %rdi
.Lcfi134:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	movq	56(%r15), %rax
	leaq	-144(%rax,%rbx), %rdi
.Lcfi135:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	movq	56(%r15), %rdi
	addq	%rbx, %rdi
.Lcfi136:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	addq	$576, %rbx              # imm = 0x240
	addq	$4, %rbp
	jne	.LBB20_52
.LBB20_53:                              # %.loopexit126
	movl	$0, 44(%r15)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movslq	76(%r15), %r12
	testq	%r12, %r12
	jns	.LBB20_66
# BB#54:
	cmpl	$0, 80(%r15)
	jns	.LBB20_60
# BB#55:                                # %_ZNK20btAlignedObjectArrayI18btSolverConstraintE4copyEiiPS0_.exit.i.i63
	movq	88(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB20_59
# BB#56:
	cmpb	$0, 96(%r15)
	je	.LBB20_58
# BB#57:
.Ltmp142:
.Lcfi137:
	.cfi_escape 0x2e, 0x00
	callq	_Z21btAlignedFreeInternalPv
.Ltmp143:
.LBB20_58:                              # %.noexc70
	movq	$0, 88(%r15)
.LBB20_59:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.preheader.i64
	movb	$1, 96(%r15)
	movq	$0, 88(%r15)
	movl	$0, 80(%r15)
.LBB20_60:                              # %.lr.ph.i65
	movl	%r12d, %r13d
	negl	%r13d
	andq	$3, %r13
	movq	%r12, %rbp
	je	.LBB20_63
# BB#61:                                # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i69.prol.preheader
	movq	%r12, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,8), %rbx
	negq	%r13
	movq	%rsp, %r14
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB20_62:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i69.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	88(%r15), %rdi
	addq	%rbx, %rdi
.Lcfi138:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	incq	%rbp
	addq	$144, %rbx
	incq	%r13
	jne	.LBB20_62
.LBB20_63:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i69.prol.loopexit
	cmpl	$-4, %r12d
	ja	.LBB20_66
# BB#64:                                # %.lr.ph.i65.new
	leaq	(%rbp,%rbp,8), %rbx
	shlq	$4, %rbx
	addq	$432, %rbx              # imm = 0x1B0
	movq	%rsp, %r14
	.p2align	4, 0x90
.LBB20_65:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i69
                                        # =>This Inner Loop Header: Depth=1
	movq	88(%r15), %rax
	leaq	-432(%rax,%rbx), %rdi
.Lcfi139:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	movq	88(%r15), %rax
	leaq	-288(%rax,%rbx), %rdi
.Lcfi140:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	movq	88(%r15), %rax
	leaq	-144(%rax,%rbx), %rdi
.Lcfi141:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	movq	88(%r15), %rdi
	addq	%rbx, %rdi
.Lcfi142:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	addq	$576, %rbx              # imm = 0x240
	addq	$4, %rbp
	jne	.LBB20_65
.LBB20_66:                              # %.loopexit125
	movl	$0, 76(%r15)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movslq	108(%r15), %r12
	testq	%r12, %r12
	jns	.LBB20_79
# BB#67:
	cmpl	$0, 112(%r15)
	jns	.LBB20_73
# BB#68:                                # %_ZNK20btAlignedObjectArrayI18btSolverConstraintE4copyEiiPS0_.exit.i.i
	movq	120(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB20_72
# BB#69:
	cmpb	$0, 128(%r15)
	je	.LBB20_71
# BB#70:
.Ltmp145:
.Lcfi143:
	.cfi_escape 0x2e, 0x00
	callq	_Z21btAlignedFreeInternalPv
.Ltmp146:
.LBB20_71:                              # %.noexc
	movq	$0, 120(%r15)
.LBB20_72:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.preheader.i
	movb	$1, 128(%r15)
	movq	$0, 120(%r15)
	movl	$0, 112(%r15)
.LBB20_73:                              # %.lr.ph.i
	movl	%r12d, %r13d
	negl	%r13d
	andq	$3, %r13
	movq	%r12, %rbp
	je	.LBB20_76
# BB#74:                                # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i.prol.preheader
	movq	%r12, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,8), %rbx
	negq	%r13
	movq	%rsp, %r14
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB20_75:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	120(%r15), %rdi
	addq	%rbx, %rdi
.Lcfi144:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	incq	%rbp
	addq	$144, %rbx
	incq	%r13
	jne	.LBB20_75
.LBB20_76:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i.prol.loopexit
	cmpl	$-4, %r12d
	ja	.LBB20_79
# BB#77:                                # %.lr.ph.i.new
	leaq	(%rbp,%rbp,8), %rbx
	shlq	$4, %rbx
	addq	$432, %rbx              # imm = 0x1B0
	movq	%rsp, %r14
	.p2align	4, 0x90
.LBB20_78:                              # %_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	120(%r15), %rax
	leaq	-432(%rax,%rbx), %rdi
.Lcfi145:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	movq	120(%r15), %rax
	leaq	-288(%rax,%rbx), %rdi
.Lcfi146:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	movq	120(%r15), %rax
	leaq	-144(%rax,%rbx), %rdi
.Lcfi147:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	movq	120(%r15), %rdi
	addq	%rbx, %rdi
.Lcfi148:
	.cfi_escape 0x2e, 0x00
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	addq	$576, %rbx              # imm = 0x240
	addq	$4, %rbp
	jne	.LBB20_78
.LBB20_79:                              # %.loopexit
	movl	$0, 108(%r15)
.Lcfi149:
	.cfi_escape 0x2e, 0x00
	callq	_ZN15CProfileManager12Stop_ProfileEv
	xorps	%xmm0, %xmm0
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB20_83:
.Ltmp147:
	jmp	.LBB20_84
.LBB20_82:
.Ltmp144:
	jmp	.LBB20_84
.LBB20_81:
.Ltmp141:
	jmp	.LBB20_84
.LBB20_80:
.Ltmp138:
	jmp	.LBB20_84
.LBB20_20:
.Ltmp132:
	jmp	.LBB20_84
.LBB20_21:
.Ltmp135:
.LBB20_84:
	movq	%rax, %rbx
.Ltmp148:
.Lcfi150:
	.cfi_escape 0x2e, 0x00
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp149:
# BB#85:                                # %_ZN14CProfileSampleD2Ev.exit
.Lcfi151:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB20_86:
.Ltmp150:
.Lcfi152:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end20:
	.size	_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAllocP12btDispatcher, .Lfunc_end20-_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAllocP12btDispatcher
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\245\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp128-.Lfunc_begin4  #   Call between .Lfunc_begin4 and .Ltmp128
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp128-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp131-.Ltmp128       #   Call between .Ltmp128 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin4  #     jumps to .Ltmp132
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp135-.Lfunc_begin4  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp136-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp137-.Ltmp136       #   Call between .Ltmp136 and .Ltmp137
	.long	.Ltmp138-.Lfunc_begin4  #     jumps to .Ltmp138
	.byte	0                       #   On action: cleanup
	.long	.Ltmp139-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp140-.Ltmp139       #   Call between .Ltmp139 and .Ltmp140
	.long	.Ltmp141-.Lfunc_begin4  #     jumps to .Ltmp141
	.byte	0                       #   On action: cleanup
	.long	.Ltmp140-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp142-.Ltmp140       #   Call between .Ltmp140 and .Ltmp142
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp142-.Lfunc_begin4  # >> Call Site 7 <<
	.long	.Ltmp143-.Ltmp142       #   Call between .Ltmp142 and .Ltmp143
	.long	.Ltmp144-.Lfunc_begin4  #     jumps to .Ltmp144
	.byte	0                       #   On action: cleanup
	.long	.Ltmp143-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Ltmp145-.Ltmp143       #   Call between .Ltmp143 and .Ltmp145
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp145-.Lfunc_begin4  # >> Call Site 9 <<
	.long	.Ltmp146-.Ltmp145       #   Call between .Ltmp145 and .Ltmp146
	.long	.Ltmp147-.Lfunc_begin4  #     jumps to .Ltmp147
	.byte	0                       #   On action: cleanup
	.long	.Ltmp146-.Lfunc_begin4  # >> Call Site 10 <<
	.long	.Ltmp148-.Ltmp146       #   Call between .Ltmp146 and .Ltmp148
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin4  # >> Call Site 11 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin4  #     jumps to .Ltmp150
	.byte	1                       #   On action: 1
	.long	.Ltmp149-.Lfunc_begin4  # >> Call Site 12 <<
	.long	.Lfunc_end20-.Ltmp149   #   Call between .Ltmp149 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN35btSequentialImpulseConstraintSolver5resetEv
	.p2align	4, 0x90
	.type	_ZN35btSequentialImpulseConstraintSolver5resetEv,@function
_ZN35btSequentialImpulseConstraintSolver5resetEv: # @_ZN35btSequentialImpulseConstraintSolver5resetEv
	.cfi_startproc
# BB#0:
	movq	$0, 232(%rdi)
	retq
.Lfunc_end21:
	.size	_ZN35btSequentialImpulseConstraintSolver5resetEv, .Lfunc_end21-_ZN35btSequentialImpulseConstraintSolver5resetEv
	.cfi_endproc

	.section	.text._ZN18btConstraintSolver12prepareSolveEii,"axG",@progbits,_ZN18btConstraintSolver12prepareSolveEii,comdat
	.weak	_ZN18btConstraintSolver12prepareSolveEii
	.p2align	4, 0x90
	.type	_ZN18btConstraintSolver12prepareSolveEii,@function
_ZN18btConstraintSolver12prepareSolveEii: # @_ZN18btConstraintSolver12prepareSolveEii
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end22:
	.size	_ZN18btConstraintSolver12prepareSolveEii, .Lfunc_end22-_ZN18btConstraintSolver12prepareSolveEii
	.cfi_endproc

	.section	.text._ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc,"axG",@progbits,_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc,comdat
	.weak	_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc
	.p2align	4, 0x90
	.type	_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc,@function
_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc: # @_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end23:
	.size	_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc, .Lfunc_end23-_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI24_0:
	.long	1061752795              # float 0.785398185
.LCPI24_1:
	.long	981668463               # float 0.00100000005
.LCPI24_2:
	.long	1056964608              # float 0.5
.LCPI24_3:
	.long	3165301419              # float -0.020833334
.LCPI24_4:
	.long	1065353216              # float 1
.LCPI24_5:
	.long	1073741824              # float 2
	.section	.text._ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_,"axG",@progbits,_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_,comdat
	.weak	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	.p2align	4, 0x90
	.type	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_,@function
_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_: # @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi155:
	.cfi_def_cfa_offset 32
	subq	$64, %rsp
.Lcfi156:
	.cfi_def_cfa_offset 96
.Lcfi157:
	.cfi_offset %rbx, -32
.Lcfi158:
	.cfi_offset %r14, -24
.Lcfi159:
	.cfi_offset %r15, -16
	movq	%rcx, %r15
	movaps	%xmm0, %xmm3
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movaps	%xmm3, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm0, %xmm1
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movsd	48(%r14), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	56(%r14), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 48(%r15)
	movlps	%xmm1, 56(%r15)
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm2, %xmm2
	sqrtss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm2
	jnp	.LBB24_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm0, %xmm2
.LBB24_2:                               # %.split
	movaps	%xmm2, %xmm0
	mulss	%xmm3, %xmm0
	ucomiss	.LCPI24_0(%rip), %xmm0
	jbe	.LBB24_4
# BB#3:                                 # %select.true.sink
	movss	.LCPI24_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm2
.LBB24_4:                               # %select.end
	movss	.LCPI24_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jbe	.LBB24_6
# BB#5:
	movss	.LCPI24_2(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm3, %xmm0
	mulss	.LCPI24_3(%rip), %xmm0
	mulss	%xmm2, %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	jmp	.LBB24_7
.LBB24_6:
	movss	.LCPI24_2(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	mulss	%xmm3, %xmm0
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	callq	sinf
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	divss	%xmm2, %xmm0
.LBB24_7:
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	mulss	8(%rbx), %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	mulss	%xmm3, %xmm2
	mulss	.LCPI24_2(%rip), %xmm2
	movaps	%xmm2, %xmm0
	callq	cosf
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	leaq	48(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movsd	48(%rsp), %xmm12        # xmm12 = mem[0],zero
	movq	56(%rsp), %xmm9         # xmm9 = mem[0],zero
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	mulss	%xmm12, %xmm3
	pshufd	$229, %xmm9, %xmm8      # xmm8 = xmm9[1,1,2,3]
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm8, %xmm4
	addss	%xmm3, %xmm4
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm7
	mulss	%xmm9, %xmm7
	addss	%xmm4, %xmm7
	pshufd	$229, %xmm12, %xmm11    # xmm11 = xmm12[1,1,2,3]
	movaps	32(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm3, %xmm4
	mulss	%xmm11, %xmm4
	subss	%xmm4, %xmm7
	movaps	%xmm1, %xmm4
	movaps	%xmm1, %xmm13
	mulss	%xmm11, %xmm4
	movaps	%xmm2, %xmm5
	mulss	%xmm8, %xmm5
	addss	%xmm4, %xmm5
	movaps	%xmm3, %xmm6
	mulss	%xmm12, %xmm6
	addss	%xmm5, %xmm6
	movaps	%xmm0, %xmm4
	mulss	%xmm9, %xmm4
	subss	%xmm4, %xmm6
	movaps	%xmm13, %xmm4
	mulss	%xmm9, %xmm4
	movaps	%xmm3, %xmm5
	movaps	%xmm3, %xmm1
	mulss	%xmm8, %xmm5
	addss	%xmm4, %xmm5
	movaps	%xmm0, %xmm10
	mulss	%xmm11, %xmm10
	addss	%xmm5, %xmm10
	movaps	%xmm2, %xmm4
	mulss	%xmm12, %xmm4
	subss	%xmm4, %xmm10
	movaps	%xmm13, %xmm3
	mulss	%xmm8, %xmm3
	movaps	%xmm0, %xmm4
	mulss	%xmm12, %xmm4
	subss	%xmm4, %xmm3
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm3
	mulss	%xmm9, %xmm1
	subss	%xmm1, %xmm3
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm10, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB24_9
# BB#8:                                 # %call.sqrt113
	movaps	%xmm1, %xmm0
	movss	%xmm7, 8(%rsp)          # 4-byte Spill
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movss	%xmm10, 32(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	32(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm7          # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
.LBB24_9:                               # %.split112
	movss	.LCPI24_4(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	divss	%xmm0, %xmm2
	mulss	%xmm2, %xmm7
	mulss	%xmm2, %xmm6
	mulss	%xmm2, %xmm10
	mulss	%xmm3, %xmm2
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm10, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movss	.LCPI24_5(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm4
	movaps	%xmm7, %xmm0
	mulss	%xmm4, %xmm0
	movaps	%xmm6, %xmm3
	mulss	%xmm4, %xmm3
	mulss	%xmm10, %xmm4
	movaps	%xmm2, %xmm9
	mulss	%xmm0, %xmm9
	movaps	%xmm2, %xmm11
	mulss	%xmm3, %xmm11
	mulss	%xmm4, %xmm2
	mulss	%xmm7, %xmm0
	movaps	%xmm7, %xmm5
	mulss	%xmm3, %xmm5
	mulss	%xmm4, %xmm7
	mulss	%xmm6, %xmm3
	mulss	%xmm4, %xmm6
	mulss	%xmm10, %xmm4
	movaps	%xmm3, %xmm1
	addss	%xmm4, %xmm1
	movaps	%xmm8, %xmm10
	subss	%xmm1, %xmm10
	movaps	%xmm5, %xmm12
	subss	%xmm2, %xmm12
	movaps	%xmm7, %xmm1
	addss	%xmm11, %xmm1
	addss	%xmm2, %xmm5
	addss	%xmm0, %xmm4
	movaps	%xmm8, %xmm2
	subss	%xmm4, %xmm2
	movaps	%xmm6, %xmm4
	subss	%xmm9, %xmm4
	subss	%xmm11, %xmm7
	addss	%xmm9, %xmm6
	addss	%xmm0, %xmm3
	subss	%xmm3, %xmm8
	movss	%xmm10, (%r15)
	movss	%xmm12, 4(%r15)
	movss	%xmm1, 8(%r15)
	movl	$0, 12(%r15)
	movss	%xmm5, 16(%r15)
	movss	%xmm2, 20(%r15)
	movss	%xmm4, 24(%r15)
	movl	$0, 28(%r15)
	movss	%xmm7, 32(%r15)
	movss	%xmm6, 36(%r15)
	movss	%xmm8, 40(%r15)
	movl	$0, 44(%r15)
	addq	$64, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end24:
	.size	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_, .Lfunc_end24-_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI25_0:
	.long	1065353216              # float 1
.LCPI25_1:
	.long	1056964608              # float 0.5
	.section	.text._ZNK11btMatrix3x311getRotationER12btQuaternion,"axG",@progbits,_ZNK11btMatrix3x311getRotationER12btQuaternion,comdat
	.weak	_ZNK11btMatrix3x311getRotationER12btQuaternion
	.p2align	4, 0x90
	.type	_ZNK11btMatrix3x311getRotationER12btQuaternion,@function
_ZNK11btMatrix3x311getRotationER12btQuaternion: # @_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi162:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi163:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi164:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi166:
	.cfi_def_cfa_offset 80
.Lcfi167:
	.cfi_offset %rbx, -56
.Lcfi168:
	.cfi_offset %r12, -48
.Lcfi169:
	.cfi_offset %r13, -40
.Lcfi170:
	.cfi_offset %r14, -32
.Lcfi171:
	.cfi_offset %r15, -24
.Lcfi172:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm1
	jbe	.LBB25_4
# BB#1:
	addss	.LCPI25_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB25_3
# BB#2:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movq	%rsi, %rbp
	callq	sqrtf
	movq	%rbp, %rsi
.LBB25_3:                               # %.split
	movss	.LCPI25_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	32(%rbx), %xmm2
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movaps	%xmm1, %xmm3
	divss	%xmm0, %xmm3
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	24(%rbx), %xmm1
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	4(%rbx), %xmm4
	movaps	%xmm3, %xmm5
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	unpcklps	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1]
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	mulps	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)
	jmp	.LBB25_7
.LBB25_4:
	xorl	%eax, %eax
	ucomiss	%xmm0, %xmm2
	seta	%al
	maxss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm3
	movl	$2, %r15d
	cmovbel	%eax, %r15d
	leal	1(%r15), %eax
	movl	$2863311531, %ecx       # imm = 0xAAAAAAAB
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	1(%r15,%rax), %edx
	movl	%r15d, %eax
	addl	$2, %eax
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	2(%r15,%rax), %r14d
	movq	%r15, %rbp
	shlq	$4, %rbp
	addq	%rbx, %rbp
	movss	(%rbp,%r15,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	%rdx, %r12
	shlq	$4, %r12
	addq	%rbx, %r12
	subss	(%r12,%rdx,4), %xmm0
	movq	%r14, %r13
	shlq	$4, %r13
	addq	%rbx, %r13
	subss	(%r13,%r14,4), %xmm0
	addss	.LCPI25_0(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB25_6
# BB#5:                                 # %call.sqrt72
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %rbx
	callq	sqrtf
	movq	%rbx, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movaps	%xmm0, %xmm1
.LBB25_6:                               # %.split71
	movss	.LCPI25_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, (%rsp,%r15,4)
	divss	%xmm1, %xmm0
	movss	(%r13,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	(%r12,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)
	movss	(%r12,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%rdx,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%rdx,4)
	movss	(%r13,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%r14,4)
	movaps	(%rsp), %xmm1
.LBB25_7:
	movups	%xmm1, (%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	_ZNK11btMatrix3x311getRotationER12btQuaternion, .Lfunc_end25-_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_endproc

	.type	gNumSplitImpulseRecoveries,@object # @gNumSplitImpulseRecoveries
	.bss
	.globl	gNumSplitImpulseRecoveries
	.p2align	2
gNumSplitImpulseRecoveries:
	.long	0                       # 0x0
	.size	gNumSplitImpulseRecoveries, 4

	.type	_ZTV35btSequentialImpulseConstraintSolver,@object # @_ZTV35btSequentialImpulseConstraintSolver
	.section	.rodata,"a",@progbits
	.globl	_ZTV35btSequentialImpulseConstraintSolver
	.p2align	3
_ZTV35btSequentialImpulseConstraintSolver:
	.quad	0
	.quad	_ZTI35btSequentialImpulseConstraintSolver
	.quad	_ZN35btSequentialImpulseConstraintSolverD2Ev
	.quad	_ZN35btSequentialImpulseConstraintSolverD0Ev
	.quad	_ZN18btConstraintSolver12prepareSolveEii
	.quad	_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btStackAllocP12btDispatcher
	.quad	_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDrawP12btStackAlloc
	.quad	_ZN35btSequentialImpulseConstraintSolver5resetEv
	.size	_ZTV35btSequentialImpulseConstraintSolver, 64

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"solveGroupCacheFriendlySetup"
	.size	.L.str, 29

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"solveGroupCacheFriendlyIterations"
	.size	.L.str.1, 34

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"solveGroup"
	.size	.L.str.2, 11

	.type	_ZTS35btSequentialImpulseConstraintSolver,@object # @_ZTS35btSequentialImpulseConstraintSolver
	.section	.rodata,"a",@progbits
	.globl	_ZTS35btSequentialImpulseConstraintSolver
	.p2align	4
_ZTS35btSequentialImpulseConstraintSolver:
	.asciz	"35btSequentialImpulseConstraintSolver"
	.size	_ZTS35btSequentialImpulseConstraintSolver, 38

	.type	_ZTS18btConstraintSolver,@object # @_ZTS18btConstraintSolver
	.section	.rodata._ZTS18btConstraintSolver,"aG",@progbits,_ZTS18btConstraintSolver,comdat
	.weak	_ZTS18btConstraintSolver
	.p2align	4
_ZTS18btConstraintSolver:
	.asciz	"18btConstraintSolver"
	.size	_ZTS18btConstraintSolver, 21

	.type	_ZTI18btConstraintSolver,@object # @_ZTI18btConstraintSolver
	.section	.rodata._ZTI18btConstraintSolver,"aG",@progbits,_ZTI18btConstraintSolver,comdat
	.weak	_ZTI18btConstraintSolver
	.p2align	3
_ZTI18btConstraintSolver:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS18btConstraintSolver
	.size	_ZTI18btConstraintSolver, 16

	.type	_ZTI35btSequentialImpulseConstraintSolver,@object # @_ZTI35btSequentialImpulseConstraintSolver
	.section	.rodata,"a",@progbits
	.globl	_ZTI35btSequentialImpulseConstraintSolver
	.p2align	4
_ZTI35btSequentialImpulseConstraintSolver:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS35btSequentialImpulseConstraintSolver
	.quad	_ZTI18btConstraintSolver
	.size	_ZTI35btSequentialImpulseConstraintSolver, 24


	.globl	_ZN35btSequentialImpulseConstraintSolverC1Ev
	.type	_ZN35btSequentialImpulseConstraintSolverC1Ev,@function
_ZN35btSequentialImpulseConstraintSolverC1Ev = _ZN35btSequentialImpulseConstraintSolverC2Ev
	.globl	_ZN35btSequentialImpulseConstraintSolverD1Ev
	.type	_ZN35btSequentialImpulseConstraintSolverD1Ev,@function
_ZN35btSequentialImpulseConstraintSolverD1Ev = _ZN35btSequentialImpulseConstraintSolverD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
