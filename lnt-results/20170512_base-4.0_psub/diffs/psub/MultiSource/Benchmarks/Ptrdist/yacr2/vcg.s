	.text
	.file	"vcg.bc"
	.globl	AllocVCG
	.p2align	4, 0x90
	.type	AllocVCG,@function
AllocVCG:                               # @AllocVCG
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	channelNets(%rip), %r14
	movq	%r14, %rdi
	shlq	$6, %rdi
	addq	$64, %rdi
	callq	malloc
	movq	%rax, VCG(%rip)
	leaq	1(%r14), %rbx
	imulq	%rbx, %rbx
	movq	%rbx, %rdi
	shlq	$5, %rdi
	callq	malloc
	movq	%rax, storageRootVCG(%rip)
	movq	%rax, storageVCG(%rip)
	movq	%rbx, storageLimitVCG(%rip)
	leaq	8(,%r14,8), %r14
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, SCC(%rip)
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, perSCC(%rip)
	shlq	$3, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, removeVCG(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	AllocVCG, .Lfunc_end0-AllocVCG
	.cfi_endproc

	.globl	FreeVCG
	.p2align	4, 0x90
	.type	FreeVCG,@function
FreeVCG:                                # @FreeVCG
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movq	VCG(%rip), %rdi
	callq	free
	movq	storageRootVCG(%rip), %rdi
	callq	free
	movq	$0, storageLimitVCG(%rip)
	movq	SCC(%rip), %rdi
	callq	free
	movq	perSCC(%rip), %rdi
	callq	free
	movq	removeVCG(%rip), %rdi
	popq	%rax
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	FreeVCG, .Lfunc_end1-FreeVCG
	.cfi_endproc

	.globl	BuildVCG
	.p2align	4, 0x90
	.type	BuildVCG,@function
BuildVCG:                               # @BuildVCG
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 48
.Lcfi11:
	.cfi_offset %rbx, -48
.Lcfi12:
	.cfi_offset %r12, -40
.Lcfi13:
	.cfi_offset %r13, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	channelNets(%rip), %rbx
	movq	%rbx, %rdi
	shlq	$6, %rdi
	addq	$64, %rdi
	callq	malloc
	movq	%rax, %r14
	movq	%r14, VCG(%rip)
	leaq	1(%rbx), %r15
	imulq	%r15, %r15
	movq	%r15, %rdi
	shlq	$5, %rdi
	callq	malloc
	movq	%rax, %r13
	movq	%r13, storageRootVCG(%rip)
	movq	%r13, storageVCG(%rip)
	movq	%r15, storageLimitVCG(%rip)
	leaq	8(,%rbx,8), %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, SCC(%rip)
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, perSCC(%rip)
	shlq	$3, %r15
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, removeVCG(%rip)
	testq	%rbx, %rbx
	je	.LBB2_27
# BB#1:                                 # %.lr.ph97.preheader
	movq	channelColumns(%rip), %r11
	movl	$1, %r15d
	movq	%r11, %rsi
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph97
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_5 Depth 2
                                        #       Child Loop BB2_11 Depth 3
                                        #     Child Loop BB2_17 Depth 2
                                        #       Child Loop BB2_23 Depth 3
	movq	%r15, %r8
	shlq	$6, %r8
	movq	%r13, (%r14,%r8)
	testq	%rsi, %rsi
	je	.LBB2_3
# BB#4:                                 # %.lr.ph84.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	TOP(%rip), %r9
	xorl	%ecx, %ecx
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph84
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_11 Depth 3
	cmpq	%r15, (%r9,%rbx,8)
	jne	.LBB2_13
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=2
	movq	BOT(%rip), %r10
	movq	(%r10,%rbx,8), %rdx
	cmpq	%r15, %rdx
	je	.LBB2_13
# BB#7:                                 #   in Loop: Header=BB2_5 Depth=2
	testq	%rdx, %rdx
	je	.LBB2_13
# BB#8:                                 # %.preheader78
                                        #   in Loop: Header=BB2_5 Depth=2
	testq	%rcx, %rcx
	movq	VCG(%rip), %rax
	movq	(%rax,%r8), %r14
	je	.LBB2_12
# BB#9:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_5 Depth=2
	leaq	8(%r14), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_11:                               # %.lr.ph
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rdx, (%rdi)
	je	.LBB2_13
# BB#10:                                #   in Loop: Header=BB2_11 Depth=3
	incq	%rax
	addq	$32, %rdi
	cmpq	%rcx, %rax
	jb	.LBB2_11
.LBB2_12:                               # %.critedge
                                        #   in Loop: Header=BB2_5 Depth=2
	movq	%rcx, %rax
	shlq	$5, %rax
	movq	%r15, (%r14,%rax)
	movq	(%r10,%rbx,8), %rdx
	movq	%rdx, 8(%r14,%rax)
	movq	%rbx, 16(%r14,%rax)
	movq	$0, 24(%r14,%rax)
	addq	$32, storageVCG(%rip)
	decq	storageLimitVCG(%rip)
	incq	%rcx
	movq	channelColumns(%rip), %r11
	movq	%r11, %rsi
	.p2align	4, 0x90
.LBB2_13:                               # %.loopexit79
                                        #   in Loop: Header=BB2_5 Depth=2
	incq	%rbx
	cmpq	%rsi, %rbx
	jbe	.LBB2_5
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_3:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%esi, %esi
	xorl	%ecx, %ecx
.LBB2_14:                               # %._crit_edge
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	VCG(%rip), %r14
	movq	%rcx, 8(%r14,%r8)
	movq	storageVCG(%rip), %r10
	movq	%r10, 32(%r14,%r8)
	testq	%rsi, %rsi
	je	.LBB2_15
# BB#16:                                # %.lr.ph91
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	BOT(%rip), %rcx
	movq	TOP(%rip), %r9
	movq	%r10, %r13
	xorl	%ebx, %ebx
	movl	$1, %edi
	.p2align	4, 0x90
.LBB2_17:                               #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_23 Depth 3
	cmpq	%r15, (%rcx,%rdi,8)
	jne	.LBB2_25
# BB#18:                                #   in Loop: Header=BB2_17 Depth=2
	movq	(%r9,%rdi,8), %rsi
	cmpq	%r15, %rsi
	je	.LBB2_25
# BB#19:                                #   in Loop: Header=BB2_17 Depth=2
	testq	%rsi, %rsi
	je	.LBB2_25
# BB#20:                                # %.preheader
                                        #   in Loop: Header=BB2_17 Depth=2
	testq	%rbx, %rbx
	je	.LBB2_24
# BB#21:                                # %.lr.ph86.preheader
                                        #   in Loop: Header=BB2_17 Depth=2
	movq	%r10, %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_23:                               # %.lr.ph86
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rsi, (%rax)
	je	.LBB2_25
# BB#22:                                #   in Loop: Header=BB2_23 Depth=3
	incq	%rdx
	addq	$32, %rax
	cmpq	%rbx, %rdx
	jb	.LBB2_23
.LBB2_24:                               # %.critedge77
                                        #   in Loop: Header=BB2_17 Depth=2
	movq	%rbx, %rax
	shlq	$5, %rax
	movq	%rsi, (%r10,%rax)
	movq	(%rcx,%rdi,8), %rdx
	movq	%rdx, 8(%r10,%rax)
	movq	%rdi, 16(%r10,%rax)
	movq	$0, 24(%r10,%rax)
	addq	$32, %r13
	movq	%r13, storageVCG(%rip)
	decq	storageLimitVCG(%rip)
	incq	%rbx
	movq	channelColumns(%rip), %r11
	.p2align	4, 0x90
.LBB2_25:                               # %.loopexit
                                        #   in Loop: Header=BB2_17 Depth=2
	incq	%rdi
	cmpq	%r11, %rdi
	movq	%r11, %rsi
	jbe	.LBB2_17
	jmp	.LBB2_26
	.p2align	4, 0x90
.LBB2_15:                               #   in Loop: Header=BB2_2 Depth=1
	xorl	%esi, %esi
	movq	%r10, %r13
	xorl	%ebx, %ebx
.LBB2_26:                               # %._crit_edge92
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%rbx, 40(%r14,%r8)
	incq	%r15
	cmpq	channelNets(%rip), %r15
	jbe	.LBB2_2
.LBB2_27:                               # %._crit_edge98
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	BuildVCG, .Lfunc_end2-BuildVCG
	.cfi_endproc

	.globl	DFSClearVCG
	.p2align	4, 0x90
	.type	DFSClearVCG,@function
DFSClearVCG:                            # @DFSClearVCG
	.cfi_startproc
# BB#0:
	movq	channelNets(%rip), %rax
	testq	%rax, %rax
	je	.LBB3_3
# BB#1:                                 # %.lr.ph.preheader
	addq	$80, %rdi
	movl	$1, %ecx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incq	%rcx
	movups	%xmm0, (%rdi)
	movups	%xmm0, 32(%rdi)
	addq	$64, %rdi
	cmpq	%rax, %rcx
	jbe	.LBB3_2
.LBB3_3:                                # %._crit_edge
	retq
.Lfunc_end3:
	.size	DFSClearVCG, .Lfunc_end3-DFSClearVCG
	.cfi_endproc

	.globl	DumpVCG
	.p2align	4, 0x90
	.type	DumpVCG,@function
DumpVCG:                                # @DumpVCG
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 64
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdi, (%rsp)            # 8-byte Spill
	cmpq	$0, channelNets(%rip)
	je	.LBB4_13
# BB#1:                                 # %.lr.ph33.preheader
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph33
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
                                        #     Child Loop BB4_9 Depth 2
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	printf
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movq	%r15, %r12
	shlq	$6, %r12
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rax,%r12), %rax
	testq	%rax, %rax
	je	.LBB4_7
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	8(%rcx,%r12), %r13
	leaq	(%rcx,%r12), %rbx
	movl	$24, %ebp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_4:                                #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rcx
	cmpq	$0, (%rcx,%rbp)
	jne	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_4 Depth=2
	movq	-16(%rcx,%rbp), %rsi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%r13), %rax
.LBB4_6:                                #   in Loop: Header=BB4_4 Depth=2
	incq	%r14
	addq	$32, %rbp
	cmpq	%rax, %r14
	jb	.LBB4_4
.LBB4_7:                                # %._crit_edge
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rax            # 8-byte Reload
	movq	40(%rax,%r12), %rax
	testq	%rax, %rax
	je	.LBB4_12
# BB#8:                                 # %.lr.ph29
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	40(%rcx,%r12), %r14
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_9:                                #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%r14), %rcx
	cmpq	$0, 24(%rcx,%rbp)
	jne	.LBB4_11
# BB#10:                                #   in Loop: Header=BB4_9 Depth=2
	movq	(%rcx,%rbp), %rsi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%r14), %rax
.LBB4_11:                               #   in Loop: Header=BB4_9 Depth=2
	incq	%rbx
	addq	$32, %rbp
	cmpq	%rax, %rbx
	jb	.LBB4_9
.LBB4_12:                               # %._crit_edge30
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	$.Lstr, %edi
	callq	puts
	incq	%r15
	cmpq	channelNets(%rip), %r15
	jbe	.LBB4_2
.LBB4_13:                               # %._crit_edge34
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	DumpVCG, .Lfunc_end4-DumpVCG
	.cfi_endproc

	.globl	DFSAboveVCG
	.p2align	4, 0x90
	.type	DFSAboveVCG,@function
DFSAboveVCG:                            # @DFSAboveVCG
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 48
.Lcfi34:
	.cfi_offset %rbx, -48
.Lcfi35:
	.cfi_offset %r12, -40
.Lcfi36:
	.cfi_offset %r13, -32
.Lcfi37:
	.cfi_offset %r14, -24
.Lcfi38:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	shlq	$6, %r12
	movq	$1, 24(%r14,%r12)
	movq	8(%r14,%r12), %rax
	testq	%rax, %rax
	je	.LBB5_6
# BB#1:                                 # %.lr.ph
	leaq	8(%r14,%r12), %r15
	addq	%r14, %r12
	xorl	%r13d, %r13d
	movl	$24, %ebx
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rcx
	cmpq	$0, (%rcx,%rbx)
	jne	.LBB5_5
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	-16(%rcx,%rbx), %rsi
	movq	%rsi, %rcx
	shlq	$6, %rcx
	cmpq	$0, 24(%r14,%rcx)
	jne	.LBB5_5
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	%r14, %rdi
	callq	DFSAboveVCG
	movq	(%r15), %rax
	.p2align	4, 0x90
.LBB5_5:                                #   in Loop: Header=BB5_2 Depth=1
	incq	%r13
	addq	$32, %rbx
	cmpq	%rax, %r13
	jb	.LBB5_2
.LBB5_6:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	DFSAboveVCG, .Lfunc_end5-DFSAboveVCG
	.cfi_endproc

	.globl	DFSBelowVCG
	.p2align	4, 0x90
	.type	DFSBelowVCG,@function
DFSBelowVCG:                            # @DFSBelowVCG
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 48
.Lcfi44:
	.cfi_offset %rbx, -40
.Lcfi45:
	.cfi_offset %r12, -32
.Lcfi46:
	.cfi_offset %r14, -24
.Lcfi47:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	shlq	$6, %rsi
	movq	$1, 56(%r14,%rsi)
	movq	40(%r14,%rsi), %rax
	testq	%rax, %rax
	je	.LBB6_6
# BB#1:                                 # %.lr.ph
	leaq	40(%r14,%rsi), %r15
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	-8(%r15), %rcx
	cmpq	$0, 24(%rcx,%r12)
	jne	.LBB6_5
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	(%rcx,%r12), %rsi
	movq	%rsi, %rcx
	shlq	$6, %rcx
	cmpq	$0, 56(%r14,%rcx)
	jne	.LBB6_5
# BB#4:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	%r14, %rdi
	callq	DFSBelowVCG
	movq	(%r15), %rax
	.p2align	4, 0x90
.LBB6_5:                                #   in Loop: Header=BB6_2 Depth=1
	incq	%rbx
	addq	$32, %r12
	cmpq	%rax, %rbx
	jb	.LBB6_2
.LBB6_6:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	DFSBelowVCG, .Lfunc_end6-DFSBelowVCG
	.cfi_endproc

	.globl	SCCofVCG
	.p2align	4, 0x90
	.type	SCCofVCG,@function
SCCofVCG:                               # @SCCofVCG
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi54:
	.cfi_def_cfa_offset 80
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	$0, 16(%rsp)
	movq	channelNets(%rip), %rax
	testq	%rax, %rax
	je	.LBB7_1
# BB#2:                                 # %.lr.ph86.preheader
	leaq	88(%r12), %rbp
	movl	$1, %ebx
	leaq	16(%rsp), %r13
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph86
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rbp)
	jne	.LBB7_5
# BB#4:                                 #   in Loop: Header=BB7_3 Depth=1
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	SCC_DFSAboveVCG
	movq	channelNets(%rip), %rax
.LBB7_5:                                #   in Loop: Header=BB7_3 Depth=1
	incq	%rbx
	addq	$64, %rbp
	cmpq	%rax, %rbx
	jbe	.LBB7_3
	jmp	.LBB7_6
.LBB7_1:
	xorl	%eax, %eax
.LBB7_6:                                # %.preheader67.preheader
	movq	%r14, (%rsp)            # 8-byte Spill
	testq	%rax, %rax
	sete	%r8b
	je	.LBB7_14
# BB#7:                                 # %.lr.ph80.preheader.preheader
	leaq	80(%r12), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB7_8:                                # %.lr.ph80.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_9 Depth 2
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	$1, %edx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB7_9:                                # %.lr.ph80
                                        #   Parent Loop BB7_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$0, 40(%rdi)
	jne	.LBB7_11
# BB#10:                                #   in Loop: Header=BB7_9 Depth=2
	movq	(%rdi), %rbx
	cmpq	%rcx, %rbx
	cmovaq	%rbp, %rsi
	cmovaq	%rbx, %rcx
	cmovaq	%r14, %rdx
.LBB7_11:                               #   in Loop: Header=BB7_9 Depth=2
	incq	%rbp
	addq	$64, %rdi
	cmpq	%rax, %rbp
	jbe	.LBB7_9
# BB#12:                                # %._crit_edge81
                                        #   in Loop: Header=BB7_8 Depth=1
	testq	%rdx, %rdx
	jne	.LBB7_16
# BB#13:                                # %.preheader67
                                        #   in Loop: Header=BB7_8 Depth=1
	incq	%r13
	movq	%r12, %rdi
	movq	%r13, %rdx
	callq	SCC_DFSBelowVCG
	movq	channelNets(%rip), %rax
	testq	%rax, %rax
	sete	%r8b
	jne	.LBB7_8
.LBB7_14:                               # %.thread
	movq	$0, totalSCC(%rip)
.LBB7_15:                               # %._crit_edge71
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_16:
	movq	$0, totalSCC(%rip)
	testb	$1, %r8b
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB7_15
# BB#17:                                # %.lr.ph74.preheader
	addq	$112, %r12
	movl	$1, %eax
	.p2align	4, 0x90
.LBB7_18:                               # %.lr.ph74
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rcx
	movq	%rcx, (%r15,%rax,8)
	movq	totalSCC(%rip), %rdx
	cmpq	%rdx, %rcx
	jbe	.LBB7_20
# BB#19:                                #   in Loop: Header=BB7_18 Depth=1
	movq	%rcx, totalSCC(%rip)
	movq	%rcx, %rdx
.LBB7_20:                               #   in Loop: Header=BB7_18 Depth=1
	incq	%rax
	movq	channelNets(%rip), %rcx
	addq	$64, %r12
	cmpq	%rcx, %rax
	jbe	.LBB7_18
# BB#21:                                # %.preheader66
	testq	%rdx, %rdx
	je	.LBB7_15
# BB#22:                                # %.preheader.preheader
	movl	$1, %eax
	jmp	.LBB7_23
	.p2align	4, 0x90
.LBB7_27:                               # %._crit_edge..preheader_crit_edge
                                        #   in Loop: Header=BB7_23 Depth=1
	movq	channelNets(%rip), %rcx
.LBB7_23:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_25 Depth 2
	xorl	%edx, %edx
	testq	%rcx, %rcx
	je	.LBB7_26
# BB#24:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB7_23 Depth=1
	movl	$1, %esi
	.p2align	4, 0x90
.LBB7_25:                               # %.lr.ph
                                        #   Parent Loop BB7_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%edi, %edi
	cmpq	%rax, (%r15,%rsi,8)
	sete	%dil
	addq	%rdi, %rdx
	incq	%rsi
	cmpq	%rcx, %rsi
	jbe	.LBB7_25
.LBB7_26:                               # %._crit_edge
                                        #   in Loop: Header=BB7_23 Depth=1
	movq	%rdx, (%rbp,%rax,8)
	incq	%rax
	cmpq	totalSCC(%rip), %rax
	jbe	.LBB7_27
	jmp	.LBB7_15
.Lfunc_end7:
	.size	SCCofVCG, .Lfunc_end7-SCCofVCG
	.cfi_endproc

	.globl	SCC_DFSAboveVCG
	.p2align	4, 0x90
	.type	SCC_DFSAboveVCG,@function
SCC_DFSAboveVCG:                        # @SCC_DFSAboveVCG
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi64:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 64
.Lcfi68:
	.cfi_offset %rbx, -56
.Lcfi69:
	.cfi_offset %r12, -48
.Lcfi70:
	.cfi_offset %r13, -40
.Lcfi71:
	.cfi_offset %r14, -32
.Lcfi72:
	.cfi_offset %r15, -24
.Lcfi73:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r13
	shlq	$6, %r15
	movq	$1, 24(%r13,%r15)
	movq	8(%r13,%r15), %rax
	testq	%rax, %rax
	je	.LBB8_6
# BB#1:                                 # %.lr.ph
	leaq	8(%r13,%r15), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	leaq	(%r13,%r15), %rbx
	xorl	%ebp, %ebp
	movl	$24, %r12d
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	cmpq	$0, (%rcx,%r12)
	jne	.LBB8_5
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	-16(%rcx,%r12), %rsi
	movq	%rsi, %rcx
	shlq	$6, %rcx
	cmpq	$0, 24(%r13,%rcx)
	jne	.LBB8_5
# BB#4:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	SCC_DFSAboveVCG
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rax
	.p2align	4, 0x90
.LBB8_5:                                #   in Loop: Header=BB8_2 Depth=1
	incq	%rbp
	addq	$32, %r12
	cmpq	%rax, %rbp
	jb	.LBB8_2
.LBB8_6:                                # %._crit_edge
	movq	(%r14), %rax
	incq	%rax
	movq	%rax, (%r14)
	movq	%rax, 16(%r13,%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	SCC_DFSAboveVCG, .Lfunc_end8-SCC_DFSAboveVCG
	.cfi_endproc

	.globl	SCC_DFSBelowVCG
	.p2align	4, 0x90
	.type	SCC_DFSBelowVCG,@function
SCC_DFSBelowVCG:                        # @SCC_DFSBelowVCG
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi77:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi78:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi80:
	.cfi_def_cfa_offset 64
.Lcfi81:
	.cfi_offset %rbx, -56
.Lcfi82:
	.cfi_offset %r12, -48
.Lcfi83:
	.cfi_offset %r13, -40
.Lcfi84:
	.cfi_offset %r14, -32
.Lcfi85:
	.cfi_offset %r15, -24
.Lcfi86:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	shlq	$6, %r15
	movq	$1, 56(%r12,%r15)
	movq	40(%r12,%r15), %rax
	testq	%rax, %rax
	je	.LBB9_6
# BB#1:                                 # %.lr.ph
	leaq	40(%r12,%r15), %r13
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movq	-8(%r13), %rcx
	cmpq	$0, 24(%rcx,%rbx)
	jne	.LBB9_5
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	(%rcx,%rbx), %rsi
	movq	%rsi, %rcx
	shlq	$6, %rcx
	cmpq	$0, 56(%r12,%rcx)
	jne	.LBB9_5
# BB#4:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	%r12, %rdi
	movq	%r14, %rdx
	callq	SCC_DFSBelowVCG
	movq	(%r13), %rax
	.p2align	4, 0x90
.LBB9_5:                                #   in Loop: Header=BB9_2 Depth=1
	incq	%rbp
	addq	$32, %rbx
	cmpq	%rax, %rbp
	jb	.LBB9_2
.LBB9_6:                                # %._crit_edge
	movq	%r14, 48(%r12,%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	SCC_DFSBelowVCG, .Lfunc_end9-SCC_DFSBelowVCG
	.cfi_endproc

	.globl	DumpSCC
	.p2align	4, 0x90
	.type	DumpSCC,@function
DumpSCC:                                # @DumpSCC
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 48
.Lcfi92:
	.cfi_offset %rbx, -40
.Lcfi93:
	.cfi_offset %r12, -32
.Lcfi94:
	.cfi_offset %r14, -24
.Lcfi95:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpq	$0, totalSCC(%rip)
	je	.LBB10_8
# BB#1:                                 # %.lr.ph18.preheader
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph18
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_4 Depth 2
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	printf
	movq	channelNets(%rip), %rax
	testq	%rax, %rax
	je	.LBB10_7
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB10_2 Depth=1
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB10_4:                               # %.lr.ph
                                        #   Parent Loop BB10_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%r12, (%r15,%rbx,8)
	jne	.LBB10_6
# BB#5:                                 #   in Loop: Header=BB10_4 Depth=2
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	movq	channelNets(%rip), %rax
.LBB10_6:                               #   in Loop: Header=BB10_4 Depth=2
	incq	%rbx
	cmpq	%rax, %rbx
	jbe	.LBB10_4
.LBB10_7:                               # %._crit_edge
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	(%r14,%r12,8), %rsi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	incq	%r12
	cmpq	totalSCC(%rip), %r12
	jbe	.LBB10_2
.LBB10_8:                               # %._crit_edge19
	movl	$10, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	putchar                 # TAILCALL
.Lfunc_end10:
	.size	DumpSCC, .Lfunc_end10-DumpSCC
	.cfi_endproc

	.globl	AcyclicVCG
	.p2align	4, 0x90
	.type	AcyclicVCG,@function
AcyclicVCG:                             # @AcyclicVCG
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi102:
	.cfi_def_cfa_offset 64
.Lcfi103:
	.cfi_offset %rbx, -56
.Lcfi104:
	.cfi_offset %r12, -48
.Lcfi105:
	.cfi_offset %r13, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movq	channelNets(%rip), %r8
	testq	%r8, %r8
	je	.LBB11_19
# BB#1:                                 # %.preheader99.lr.ph
	movq	VCG(%rip), %r9
	movl	$1, %edx
	.p2align	4, 0x90
.LBB11_2:                               # %.preheader99
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_6 Depth 2
                                        #     Child Loop BB11_9 Depth 2
                                        #     Child Loop BB11_14 Depth 2
                                        #     Child Loop BB11_17 Depth 2
	movq	%rdx, %rsi
	shlq	$6, %rsi
	movq	8(%r9,%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB11_10
# BB#3:                                 # %.lr.ph125
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	(%r9,%rsi), %r10
	leaq	-1(%rcx), %rax
	movq	%rcx, %rdi
	andq	$7, %rdi
	je	.LBB11_4
# BB#5:                                 # %.prol.preheader
                                        #   in Loop: Header=BB11_2 Depth=1
	leaq	24(%r10), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_6:                               #   Parent Loop BB11_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	$0, (%rbx)
	incq	%rbp
	addq	$32, %rbx
	cmpq	%rbp, %rdi
	jne	.LBB11_6
	jmp	.LBB11_7
	.p2align	4, 0x90
.LBB11_4:                               #   in Loop: Header=BB11_2 Depth=1
	xorl	%ebp, %ebp
.LBB11_7:                               # %.prol.loopexit
                                        #   in Loop: Header=BB11_2 Depth=1
	cmpq	$7, %rax
	jb	.LBB11_10
# BB#8:                                 # %.lr.ph125.new
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	%rbp, %rax
	shlq	$5, %rax
	leaq	248(%r10,%rax), %rax
	.p2align	4, 0x90
.LBB11_9:                               #   Parent Loop BB11_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	$0, -224(%rax)
	movq	$0, -192(%rax)
	movq	$0, -160(%rax)
	movq	$0, -128(%rax)
	movq	$0, -96(%rax)
	movq	$0, -64(%rax)
	movq	$0, -32(%rax)
	movq	$0, (%rax)
	addq	$8, %rbp
	addq	$256, %rax              # imm = 0x100
	cmpq	%rcx, %rbp
	jb	.LBB11_9
.LBB11_10:                              # %.preheader98
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	40(%r9,%rsi), %rdi
	testq	%rdi, %rdi
	je	.LBB11_18
# BB#11:                                # %.lr.ph127
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	32(%r9,%rsi), %rbp
	leaq	-1(%rdi), %rax
	movq	%rdi, %rcx
	andq	$7, %rcx
	je	.LBB11_12
# BB#13:                                # %.prol.preheader178
                                        #   in Loop: Header=BB11_2 Depth=1
	leaq	24(%rbp), %rbx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB11_14:                              #   Parent Loop BB11_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	$0, (%rbx)
	incq	%rsi
	addq	$32, %rbx
	cmpq	%rsi, %rcx
	jne	.LBB11_14
	jmp	.LBB11_15
	.p2align	4, 0x90
.LBB11_12:                              #   in Loop: Header=BB11_2 Depth=1
	xorl	%esi, %esi
.LBB11_15:                              # %.prol.loopexit179
                                        #   in Loop: Header=BB11_2 Depth=1
	cmpq	$7, %rax
	jb	.LBB11_18
# BB#16:                                # %.lr.ph127.new
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	%rsi, %rax
	shlq	$5, %rax
	leaq	248(%rbp,%rax), %rax
	.p2align	4, 0x90
.LBB11_17:                              #   Parent Loop BB11_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	$0, -224(%rax)
	movq	$0, -192(%rax)
	movq	$0, -160(%rax)
	movq	$0, -128(%rax)
	movq	$0, -96(%rax)
	movq	$0, -64(%rax)
	movq	$0, -32(%rax)
	movq	$0, (%rax)
	addq	$8, %rsi
	addq	$256, %rax              # imm = 0x100
	cmpq	%rdi, %rsi
	jb	.LBB11_17
.LBB11_18:                              # %._crit_edge128
                                        #   in Loop: Header=BB11_2 Depth=1
	incq	%rdx
	cmpq	%r8, %rdx
	jbe	.LBB11_2
.LBB11_19:                              # %._crit_edge131
	movq	$0, removeTotalVCG(%rip)
	movl	$1, %ebx
	jmp	.LBB11_20
	.p2align	4, 0x90
.LBB11_25:                              #   in Loop: Header=BB11_26 Depth=2
	incq	%rcx
	cmpq	%rax, %rcx
	jbe	.LBB11_26
	jmp	.LBB11_28
	.p2align	4, 0x90
.LBB11_27:                              #   in Loop: Header=BB11_20 Depth=1
	movq	VCG(%rip), %rdi
	movq	SCC(%rip), %rsi
	movq	removeVCG(%rip), %rcx
	callq	RemoveConstraintVCG
	movq	channelNets(%rip), %r8
	xorl	%ebx, %ebx
.LBB11_20:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_22 Depth 2
                                        #     Child Loop BB11_26 Depth 2
	xorps	%xmm0, %xmm0
	movq	VCG(%rip), %rdi
	testq	%r8, %r8
	je	.LBB11_23
# BB#21:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB11_20 Depth=1
	leaq	112(%rdi), %rax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB11_22:                              # %.lr.ph.i
                                        #   Parent Loop BB11_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rcx
	movups	%xmm0, -32(%rax)
	movups	%xmm0, (%rax)
	addq	$64, %rax
	cmpq	%r8, %rcx
	jbe	.LBB11_22
.LBB11_23:                              # %DFSClearVCG.exit
                                        #   in Loop: Header=BB11_20 Depth=1
	movq	SCC(%rip), %rsi
	movq	perSCC(%rip), %rdx
	callq	SCCofVCG
	movq	totalSCC(%rip), %rax
	testq	%rax, %rax
	je	.LBB11_28
# BB#24:                                # %.lr.ph121
                                        #   in Loop: Header=BB11_20 Depth=1
	movq	perSCC(%rip), %rdx
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB11_26:                              #   Parent Loop BB11_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$2, (%rdx,%rcx,8)
	jb	.LBB11_25
	jmp	.LBB11_27
.LBB11_28:                              # %.critedge
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	removeTotalVCG(%rip), %r15
	testq	%r15, %r15
	je	.LBB11_29
# BB#30:                                # %.lr.ph119.preheader
	xorl	%r12d, %r12d
	xorps	%xmm0, %xmm0
	jmp	.LBB11_31
	.p2align	4, 0x90
.LBB11_47:                              # %.preheader95
                                        #   in Loop: Header=BB11_31 Depth=1
	movq	VCG(%rip), %rax
	movq	8(%rax,%rbp), %rcx
	testq	%rcx, %rcx
	je	.LBB11_51
# BB#48:                                # %.lr.ph113
                                        #   in Loop: Header=BB11_31 Depth=1
	movq	(%rax,%rbp), %rdx
	addq	$24, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB11_49:                              #   Parent Loop BB11_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbx, -16(%rdx)
	je	.LBB11_50
# BB#61:                                #   in Loop: Header=BB11_49 Depth=2
	incq	%rsi
	addq	$32, %rdx
	cmpq	%rcx, %rsi
	jb	.LBB11_49
	jmp	.LBB11_51
.LBB11_50:                              #   in Loop: Header=BB11_31 Depth=1
	movq	$1, (%rdx)
.LBB11_51:                              # %.preheader
                                        #   in Loop: Header=BB11_31 Depth=1
	movq	40(%rax,%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB11_57
# BB#52:                                # %.lr.ph115
                                        #   in Loop: Header=BB11_31 Depth=1
	movq	32(%rax,%r14), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB11_53:                              #   Parent Loop BB11_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%r13, (%rax)
	je	.LBB11_54
# BB#55:                                #   in Loop: Header=BB11_53 Depth=2
	incq	%rdx
	addq	$32, %rax
	cmpq	%rcx, %rdx
	jb	.LBB11_53
	jmp	.LBB11_57
.LBB11_54:                              #   in Loop: Header=BB11_31 Depth=1
	movq	$1, 24(%rax)
	jmp	.LBB11_57
	.p2align	4, 0x90
.LBB11_31:                              # %.lr.ph119
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_33 Depth 2
                                        #     Child Loop BB11_37 Depth 2
                                        #     Child Loop BB11_42 Depth 2
                                        #     Child Loop BB11_46 Depth 2
                                        #     Child Loop BB11_49 Depth 2
                                        #     Child Loop BB11_53 Depth 2
	movq	removeVCG(%rip), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax), %r13
	movq	8(%rax), %rbx
	movq	VCG(%rip), %rdi
	movq	%r13, %rbp
	shlq	$6, %rbp
	movq	8(%rdi,%rbp), %rax
	testq	%rax, %rax
	je	.LBB11_35
# BB#32:                                # %.lr.ph
                                        #   in Loop: Header=BB11_31 Depth=1
	movq	(%rdi,%rbp), %rcx
	addq	$24, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB11_33:                              #   Parent Loop BB11_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbx, -16(%rcx)
	je	.LBB11_34
# BB#60:                                #   in Loop: Header=BB11_33 Depth=2
	incq	%rdx
	addq	$32, %rcx
	cmpq	%rax, %rdx
	jb	.LBB11_33
	jmp	.LBB11_35
	.p2align	4, 0x90
.LBB11_34:                              #   in Loop: Header=BB11_31 Depth=1
	movq	$0, (%rcx)
.LBB11_35:                              # %.preheader96
                                        #   in Loop: Header=BB11_31 Depth=1
	movq	%rbx, %r14
	shlq	$6, %r14
	movq	40(%rdi,%r14), %rax
	testq	%rax, %rax
	je	.LBB11_40
# BB#36:                                # %.lr.ph109
                                        #   in Loop: Header=BB11_31 Depth=1
	movq	32(%rdi,%r14), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB11_37:                              #   Parent Loop BB11_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%r13, (%rcx)
	je	.LBB11_38
# BB#39:                                #   in Loop: Header=BB11_37 Depth=2
	incq	%rdx
	addq	$32, %rcx
	cmpq	%rax, %rdx
	jb	.LBB11_37
	jmp	.LBB11_40
	.p2align	4, 0x90
.LBB11_38:                              #   in Loop: Header=BB11_31 Depth=1
	movq	$0, 24(%rcx)
.LBB11_40:                              # %.loopexit97
                                        #   in Loop: Header=BB11_31 Depth=1
	movq	channelNets(%rip), %rax
	testq	%rax, %rax
	je	.LBB11_43
# BB#41:                                # %.lr.ph.i92.preheader
                                        #   in Loop: Header=BB11_31 Depth=1
	leaq	112(%rdi), %rcx
	movl	$1, %edx
	.p2align	4, 0x90
.LBB11_42:                              # %.lr.ph.i92
                                        #   Parent Loop BB11_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rdx
	movups	%xmm0, -32(%rcx)
	movups	%xmm0, (%rcx)
	addq	$64, %rcx
	cmpq	%rax, %rdx
	jbe	.LBB11_42
.LBB11_43:                              # %DFSClearVCG.exit93
                                        #   in Loop: Header=BB11_31 Depth=1
	movq	SCC(%rip), %rsi
	movq	perSCC(%rip), %rdx
	callq	SCCofVCG
	xorps	%xmm0, %xmm0
	movq	totalSCC(%rip), %rax
	testq	%rax, %rax
	je	.LBB11_56
# BB#44:                                # %.lr.ph111
                                        #   in Loop: Header=BB11_31 Depth=1
	movq	perSCC(%rip), %rcx
	movl	$1, %edx
	.p2align	4, 0x90
.LBB11_46:                              #   Parent Loop BB11_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$2, (%rcx,%rdx,8)
	jae	.LBB11_47
# BB#45:                                #   in Loop: Header=BB11_46 Depth=2
	incq	%rdx
	cmpq	%rax, %rdx
	jbe	.LBB11_46
.LBB11_56:                              # %.critedge90
                                        #   in Loop: Header=BB11_31 Depth=1
	decq	%r15
.LBB11_57:                              # %.loopexit
                                        #   in Loop: Header=BB11_31 Depth=1
	incq	%r12
	cmpq	removeTotalVCG(%rip), %r12
	jb	.LBB11_31
	jmp	.LBB11_58
.LBB11_29:
	xorl	%r15d, %r15d
.LBB11_58:                              # %._crit_edge
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB11_59
# BB#62:
	movl	$.Lstr.2, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	puts                    # TAILCALL
.LBB11_59:
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	printf                  # TAILCALL
.Lfunc_end11:
	.size	AcyclicVCG, .Lfunc_end11-AcyclicVCG
	.cfi_endproc

	.globl	RemoveConstraintVCG
	.p2align	4, 0x90
	.type	RemoveConstraintVCG,@function
RemoveConstraintVCG:                    # @RemoveConstraintVCG
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi112:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi113:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi115:
	.cfi_def_cfa_offset 96
.Lcfi116:
	.cfi_offset %rbx, -56
.Lcfi117:
	.cfi_offset %r12, -48
.Lcfi118:
	.cfi_offset %r13, -40
.Lcfi119:
	.cfi_offset %r14, -32
.Lcfi120:
	.cfi_offset %r15, -24
.Lcfi121:
	.cfi_offset %rbp, -16
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	cmpq	$0, totalSCC(%rip)
	je	.LBB12_45
# BB#1:                                 # %.lr.ph119.preheader
	movl	$1, %ebx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB12_2:                               # %.lr.ph119
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_5 Depth 2
                                        #       Child Loop BB12_8 Depth 3
                                        #     Child Loop BB12_41 Depth 2
	cmpq	$2, (%rdx,%rbx,8)
	jb	.LBB12_44
# BB#3:                                 # %.preheader102
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	channelNets(%rip), %r9
	testq	%r9, %r9
	je	.LBB12_38
# BB#4:                                 # %.lr.ph112
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	TOP(%rip), %r10
	movq	BOT(%rip), %r11
	movq	channelColumns(%rip), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movl	$1, %edi
	movl	$7, %r8d
	.p2align	4, 0x90
.LBB12_5:                               #   Parent Loop BB12_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_8 Depth 3
	cmpq	%rbx, (%r13,%rdi,8)
	jne	.LBB12_37
# BB#6:                                 # %.preheader
                                        #   in Loop: Header=BB12_5 Depth=2
	movq	%rdi, %rcx
	shlq	$6, %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	8(%rdx,%rcx), %rax
	testq	%rax, %rax
	je	.LBB12_37
# BB#7:                                 # %.lr.ph
                                        #   in Loop: Header=BB12_5 Depth=2
	movq	(%rdx,%rcx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB12_8:                               #   Parent Loop BB12_2 Depth=1
                                        #     Parent Loop BB12_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rcx), %rsi
	cmpq	%rbx, (%r13,%rsi,8)
	jne	.LBB12_36
# BB#9:                                 #   in Loop: Header=BB12_8 Depth=3
	cmpq	$0, 24(%rcx)
	jne	.LBB12_36
# BB#10:                                #   in Loop: Header=BB12_8 Depth=3
	movq	16(%rcx), %r15
	cmpq	$1, %r15
	jne	.LBB12_14
# BB#11:                                #   in Loop: Header=BB12_8 Depth=3
	cmpq	$0, 16(%r10)
	je	.LBB12_21
# BB#12:                                #   in Loop: Header=BB12_8 Depth=3
	cmpq	$0, 16(%r11)
	je	.LBB12_22
# BB#13:                                #   in Loop: Header=BB12_8 Depth=3
	movl	$6, %r14d
	jmp	.LBB12_35
.LBB12_14:                              #   in Loop: Header=BB12_8 Depth=3
	movq	-8(%r10,%r15,8), %rsi
	movq	-8(%r11,%r15,8), %rbp
	cmpq	32(%rsp), %r15          # 8-byte Folded Reload
	jne	.LBB12_18
# BB#15:                                #   in Loop: Header=BB12_8 Depth=3
	testq	%rsi, %rsi
	je	.LBB12_23
# BB#16:                                #   in Loop: Header=BB12_8 Depth=3
	testq	%rbp, %rbp
	je	.LBB12_24
# BB#17:                                #   in Loop: Header=BB12_8 Depth=3
	movl	$6, %r14d
	jmp	.LBB12_35
.LBB12_18:                              #   in Loop: Header=BB12_8 Depth=3
	testq	%rsi, %rsi
	je	.LBB12_25
# BB#19:                                #   in Loop: Header=BB12_8 Depth=3
	testq	%rbp, %rbp
	je	.LBB12_26
# BB#20:                                #   in Loop: Header=BB12_8 Depth=3
	movl	$3, %r14d
	jmp	.LBB12_30
.LBB12_21:                              #   in Loop: Header=BB12_8 Depth=3
	cmpq	$0, 16(%r11)
	je	.LBB12_28
.LBB12_22:                              #   in Loop: Header=BB12_8 Depth=3
	movl	$5, %r14d
	jmp	.LBB12_35
.LBB12_23:                              #   in Loop: Header=BB12_8 Depth=3
	testq	%rbp, %rbp
	je	.LBB12_28
.LBB12_24:                              #   in Loop: Header=BB12_8 Depth=3
	movl	$5, %r14d
	jmp	.LBB12_35
.LBB12_28:                              #   in Loop: Header=BB12_8 Depth=3
	movl	$3, %r14d
	jmp	.LBB12_35
.LBB12_25:                              #   in Loop: Header=BB12_8 Depth=3
	testq	%rbp, %rbp
	je	.LBB12_29
.LBB12_26:                              #   in Loop: Header=BB12_8 Depth=3
	movl	$2, %r14d
	jmp	.LBB12_30
.LBB12_29:                              #   in Loop: Header=BB12_8 Depth=3
	xorl	%r14d, %r14d
.LBB12_30:                              #   in Loop: Header=BB12_8 Depth=3
	cmpq	$0, 8(%r10,%r15,8)
	movq	8(%r11,%r15,8), %rsi
	je	.LBB12_33
# BB#31:                                #   in Loop: Header=BB12_8 Depth=3
	testq	%rsi, %rsi
	je	.LBB12_34
# BB#32:                                #   in Loop: Header=BB12_8 Depth=3
	addq	$3, %r14
	jmp	.LBB12_35
.LBB12_33:                              #   in Loop: Header=BB12_8 Depth=3
	testq	%rsi, %rsi
	je	.LBB12_35
.LBB12_34:                              #   in Loop: Header=BB12_8 Depth=3
	addq	$2, %r14
.LBB12_35:                              #   in Loop: Header=BB12_8 Depth=3
	cmpq	%r8, %r14
	cmovbq	%r14, %r8
	cmovbq	%rcx, %r12
	.p2align	4, 0x90
.LBB12_36:                              #   in Loop: Header=BB12_8 Depth=3
	incq	%rdx
	addq	$32, %rcx
	cmpq	%rax, %rdx
	jb	.LBB12_8
.LBB12_37:                              # %.loopexit
                                        #   in Loop: Header=BB12_5 Depth=2
	incq	%rdi
	cmpq	%r9, %rdi
	jbe	.LBB12_5
	jmp	.LBB12_39
.LBB12_38:                              #   in Loop: Header=BB12_2 Depth=1
	xorl	%r12d, %r12d
.LBB12_39:                              # %._crit_edge
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	removeTotalVCG(%rip), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%r12, (%rcx,%rax,8)
	incq	%rax
	movq	%rax, removeTotalVCG(%rip)
	movq	(%r12), %rax
	movq	8(%r12), %rsi
	movq	$1, 24(%r12)
	shlq	$6, %rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	40(%rcx,%rsi), %rcx
	testq	%rcx, %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	je	.LBB12_44
# BB#40:                                # %.lr.ph116
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	32(%rdi,%rsi), %rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB12_41:                              #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, (%rdi)
	je	.LBB12_43
# BB#42:                                #   in Loop: Header=BB12_41 Depth=2
	incq	%rsi
	addq	$32, %rdi
	cmpq	%rcx, %rsi
	jb	.LBB12_41
	jmp	.LBB12_44
.LBB12_43:                              #   in Loop: Header=BB12_2 Depth=1
	movq	$1, 24(%rdi)
	.p2align	4, 0x90
.LBB12_44:                              # %.loopexit101
                                        #   in Loop: Header=BB12_2 Depth=1
	incq	%rbx
	cmpq	totalSCC(%rip), %rbx
	jbe	.LBB12_2
.LBB12_45:                              # %._crit_edge120
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	RemoveConstraintVCG, .Lfunc_end12-RemoveConstraintVCG
	.cfi_endproc

	.globl	ExistPathAboveVCG
	.p2align	4, 0x90
	.type	ExistPathAboveVCG,@function
ExistPathAboveVCG:                      # @ExistPathAboveVCG
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi124:
	.cfi_def_cfa_offset 32
.Lcfi125:
	.cfi_offset %rbx, -24
.Lcfi126:
	.cfi_offset %r14, -16
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movq	channelNets(%rip), %rax
	testq	%rax, %rax
	je	.LBB13_3
# BB#1:                                 # %.lr.ph.i.preheader
	leaq	112(%r14), %rcx
	movl	$1, %edx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB13_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	incq	%rdx
	movups	%xmm0, -32(%rcx)
	movups	%xmm0, (%rcx)
	addq	$64, %rcx
	cmpq	%rax, %rdx
	jbe	.LBB13_2
.LBB13_3:                               # %DFSClearVCG.exit
	movq	%r14, %rdi
	callq	DFSAboveVCG
	shlq	$6, %rbx
	movq	24(%r14,%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end13:
	.size	ExistPathAboveVCG, .Lfunc_end13-ExistPathAboveVCG
	.cfi_endproc

	.globl	LongestPathVCG
	.p2align	4, 0x90
	.type	LongestPathVCG,@function
LongestPathVCG:                         # @LongestPathVCG
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi129:
	.cfi_def_cfa_offset 32
.Lcfi130:
	.cfi_offset %rbx, -24
.Lcfi131:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	channelNets(%rip), %rax
	testq	%rax, %rax
	je	.LBB14_3
# BB#1:                                 # %.lr.ph.i.preheader
	leaq	112(%rbx), %rcx
	movl	$1, %edx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB14_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	incq	%rdx
	movups	%xmm0, -32(%rcx)
	movups	%xmm0, (%rcx)
	addq	$64, %rcx
	cmpq	%rax, %rdx
	jbe	.LBB14_2
.LBB14_3:                               # %DFSClearVCG.exit
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	DFSAboveLongestPathVCG
	leaq	-1(%rax), %rdx
	movq	%rdx, cardBotNotPref(%rip)
	movq	channelTracks(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB14_16
# BB#4:                                 # %.lr.ph46
	movq	tracksBotNotPref(%rip), %rcx
	testb	$1, %sil
	jne	.LBB14_6
# BB#5:
	movq	%rsi, %rax
	cmpq	$1, %rsi
	jne	.LBB14_10
	jmp	.LBB14_16
.LBB14_6:
	testq	%rdx, %rdx
	je	.LBB14_8
# BB#7:
	movq	$1, (%rcx,%rsi,8)
	addq	$-2, %rax
	movq	%rax, %rdx
	jmp	.LBB14_9
.LBB14_8:
	movq	$0, (%rcx,%rsi,8)
	xorl	%edx, %edx
.LBB14_9:
	leaq	-1(%rsi), %rax
	cmpq	$1, %rsi
	je	.LBB14_16
	.p2align	4, 0x90
.LBB14_10:                              # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	je	.LBB14_12
# BB#11:                                #   in Loop: Header=BB14_10 Depth=1
	movq	$1, (%rcx,%rax,8)
	decq	%rdx
	testq	%rdx, %rdx
	jne	.LBB14_13
	jmp	.LBB14_14
	.p2align	4, 0x90
.LBB14_12:                              #   in Loop: Header=BB14_10 Depth=1
	movq	$0, (%rcx,%rax,8)
	xorl	%edx, %edx
	testq	%rdx, %rdx
	je	.LBB14_14
.LBB14_13:                              #   in Loop: Header=BB14_10 Depth=1
	movq	$1, -8(%rcx,%rax,8)
	decq	%rdx
	addq	$-2, %rax
	jne	.LBB14_10
	jmp	.LBB14_16
	.p2align	4, 0x90
.LBB14_14:                              #   in Loop: Header=BB14_10 Depth=1
	movq	$0, -8(%rcx,%rax,8)
	xorl	%edx, %edx
	addq	$-2, %rax
	jne	.LBB14_10
.LBB14_16:                              # %._crit_edge47
	movq	channelNets(%rip), %rax
	testq	%rax, %rax
	je	.LBB14_19
# BB#17:                                # %.lr.ph.i37.preheader
	leaq	112(%rbx), %rcx
	movl	$1, %edx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB14_18:                              # %.lr.ph.i37
                                        # =>This Inner Loop Header: Depth=1
	incq	%rdx
	movups	%xmm0, -32(%rcx)
	movups	%xmm0, (%rcx)
	addq	$64, %rcx
	cmpq	%rax, %rdx
	jbe	.LBB14_18
.LBB14_19:                              # %DFSClearVCG.exit38
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	DFSBelowLongestPathVCG
	decq	%rax
	movq	%rax, cardTopNotPref(%rip)
	cmpq	$0, channelTracks(%rip)
	je	.LBB14_33
# BB#20:                                # %.lr.ph43
	movq	tracksTopNotPref(%rip), %rcx
	movl	$1, %edx
	.p2align	4, 0x90
.LBB14_21:                              # =>This Inner Loop Header: Depth=1
	testq	%rax, %rax
	je	.LBB14_23
# BB#22:                                #   in Loop: Header=BB14_21 Depth=1
	movq	$1, (%rcx,%rdx,8)
	decq	%rax
	jmp	.LBB14_24
	.p2align	4, 0x90
.LBB14_23:                              #   in Loop: Header=BB14_21 Depth=1
	movq	$0, (%rcx,%rdx,8)
	xorl	%eax, %eax
.LBB14_24:                              #   in Loop: Header=BB14_21 Depth=1
	incq	%rdx
	movq	channelTracks(%rip), %rsi
	cmpq	%rsi, %rdx
	jbe	.LBB14_21
# BB#25:                                # %.preheader
	testq	%rsi, %rsi
	je	.LBB14_33
# BB#26:                                # %.lr.ph
	movq	tracksTopNotPref(%rip), %rcx
	movq	tracksBotNotPref(%rip), %rdx
	movq	tracksNotPref(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	jmp	.LBB14_28
.LBB14_27:                              #   in Loop: Header=BB14_28 Depth=1
	movq	$0, (%rsi,%rdi,8)
	jmp	.LBB14_31
	.p2align	4, 0x90
.LBB14_28:                              # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rcx,%rdi,8)
	jne	.LBB14_30
# BB#29:                                #   in Loop: Header=BB14_28 Depth=1
	cmpq	$0, (%rdx,%rdi,8)
	je	.LBB14_27
.LBB14_30:                              #   in Loop: Header=BB14_28 Depth=1
	movq	$1, (%rsi,%rdi,8)
	incq	%rax
.LBB14_31:                              #   in Loop: Header=BB14_28 Depth=1
	incq	%rdi
	cmpq	channelTracks(%rip), %rdi
	jbe	.LBB14_28
	jmp	.LBB14_34
.LBB14_33:
	xorl	%eax, %eax
.LBB14_34:                              # %._crit_edge
	movq	%rax, cardNotPref(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end14:
	.size	LongestPathVCG, .Lfunc_end14-LongestPathVCG
	.cfi_endproc

	.globl	DFSAboveLongestPathVCG
	.p2align	4, 0x90
	.type	DFSAboveLongestPathVCG,@function
DFSAboveLongestPathVCG:                 # @DFSAboveLongestPathVCG
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi135:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi136:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi138:
	.cfi_def_cfa_offset 64
.Lcfi139:
	.cfi_offset %rbx, -56
.Lcfi140:
	.cfi_offset %r12, -48
.Lcfi141:
	.cfi_offset %r13, -40
.Lcfi142:
	.cfi_offset %r14, -32
.Lcfi143:
	.cfi_offset %r15, -24
.Lcfi144:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	shlq	$6, %r13
	movq	$1, 24(%r14,%r13)
	movq	8(%r14,%r13), %rax
	testq	%rax, %rax
	je	.LBB15_1
# BB#2:                                 # %.lr.ph
	leaq	8(%r14,%r13), %r12
	addq	%r14, %r13
	xorl	%r15d, %r15d
	movl	$24, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB15_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rcx
	cmpq	$0, (%rcx,%rbx)
	jne	.LBB15_6
# BB#4:                                 #   in Loop: Header=BB15_3 Depth=1
	movq	-16(%rcx,%rbx), %rsi
	movq	%rsi, %rcx
	shlq	$6, %rcx
	cmpq	$0, 24(%r14,%rcx)
	jne	.LBB15_6
# BB#5:                                 #   in Loop: Header=BB15_3 Depth=1
	movq	%r14, %rdi
	callq	DFSAboveLongestPathVCG
	cmpq	%r15, %rax
	cmovaq	%rax, %r15
	movq	(%r12), %rax
	.p2align	4, 0x90
.LBB15_6:                               #   in Loop: Header=BB15_3 Depth=1
	incq	%rbp
	addq	$32, %rbx
	cmpq	%rax, %rbp
	jb	.LBB15_3
	jmp	.LBB15_7
.LBB15_1:
	xorl	%r15d, %r15d
.LBB15_7:                               # %._crit_edge
	incq	%r15
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	DFSAboveLongestPathVCG, .Lfunc_end15-DFSAboveLongestPathVCG
	.cfi_endproc

	.globl	DFSBelowLongestPathVCG
	.p2align	4, 0x90
	.type	DFSBelowLongestPathVCG,@function
DFSBelowLongestPathVCG:                 # @DFSBelowLongestPathVCG
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi147:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi148:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi149:
	.cfi_def_cfa_offset 48
.Lcfi150:
	.cfi_offset %rbx, -48
.Lcfi151:
	.cfi_offset %r12, -40
.Lcfi152:
	.cfi_offset %r13, -32
.Lcfi153:
	.cfi_offset %r14, -24
.Lcfi154:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	shlq	$6, %rsi
	movq	$1, 56(%r15,%rsi)
	movq	40(%r15,%rsi), %rax
	testq	%rax, %rax
	je	.LBB16_1
# BB#2:                                 # %.lr.ph
	leaq	40(%r15,%rsi), %r12
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_3:                               # =>This Inner Loop Header: Depth=1
	movq	-8(%r12), %rcx
	cmpq	$0, 24(%rcx,%r13)
	jne	.LBB16_6
# BB#4:                                 #   in Loop: Header=BB16_3 Depth=1
	movq	(%rcx,%r13), %rsi
	movq	%rsi, %rcx
	shlq	$6, %rcx
	cmpq	$0, 56(%r15,%rcx)
	jne	.LBB16_6
# BB#5:                                 #   in Loop: Header=BB16_3 Depth=1
	movq	%r15, %rdi
	callq	DFSBelowLongestPathVCG
	cmpq	%r14, %rax
	cmovaq	%rax, %r14
	movq	(%r12), %rax
	.p2align	4, 0x90
.LBB16_6:                               #   in Loop: Header=BB16_3 Depth=1
	incq	%rbx
	addq	$32, %r13
	cmpq	%rax, %rbx
	jb	.LBB16_3
	jmp	.LBB16_7
.LBB16_1:
	xorl	%r14d, %r14d
.LBB16_7:                               # %._crit_edge
	incq	%r14
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	DFSBelowLongestPathVCG, .Lfunc_end16-DFSBelowLongestPathVCG
	.cfi_endproc

	.globl	VCV
	.p2align	4, 0x90
	.type	VCV,@function
VCV:                                    # @VCV
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi155:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi156:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi157:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi158:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi159:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi161:
	.cfi_def_cfa_offset 80
.Lcfi162:
	.cfi_offset %rbx, -56
.Lcfi163:
	.cfi_offset %r12, -48
.Lcfi164:
	.cfi_offset %r13, -40
.Lcfi165:
	.cfi_offset %r14, -32
.Lcfi166:
	.cfi_offset %r15, -24
.Lcfi167:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r14
	movq	%rdi, %r12
	movq	channelNets(%rip), %rax
	testq	%rax, %rax
	je	.LBB17_1
# BB#2:                                 # %.lr.ph
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %rcx
	shlq	$6, %rcx
	leaq	24(%r12,%rcx), %r15
	leaq	112(%r12), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$1, %ebp
	xorl	%ebx, %ebx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB17_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_16 Depth 2
                                        #     Child Loop BB17_20 Depth 2
                                        #     Child Loop BB17_12 Depth 2
                                        #     Child Loop BB17_7 Depth 2
	movq	(%r13,%rbp,8), %rcx
	testq	%rcx, %rcx
	je	.LBB17_24
# BB#4:                                 #   in Loop: Header=BB17_3 Depth=1
	cmpq	%r14, %rcx
	jae	.LBB17_9
# BB#5:                                 #   in Loop: Header=BB17_3 Depth=1
	testq	%rax, %rax
	je	.LBB17_8
# BB#6:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	$1, %edx
	.p2align	4, 0x90
.LBB17_7:                               # %.lr.ph.i.i
                                        #   Parent Loop BB17_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rdx
	movups	%xmm0, -32(%rcx)
	movups	%xmm0, (%rcx)
	addq	$64, %rcx
	cmpq	%rax, %rdx
	jbe	.LBB17_7
.LBB17_8:                               # %ExistPathAboveVCG.exit
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	DFSAboveVCG
	xorps	%xmm0, %xmm0
	cmpq	$1, (%r15)
	sbbq	$-1, %rbx
	jmp	.LBB17_24
	.p2align	4, 0x90
.LBB17_9:                               #   in Loop: Header=BB17_3 Depth=1
	jbe	.LBB17_14
# BB#10:                                #   in Loop: Header=BB17_3 Depth=1
	testq	%rax, %rax
	je	.LBB17_13
# BB#11:                                # %.lr.ph.i.i30.preheader
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	$1, %edx
	.p2align	4, 0x90
.LBB17_12:                              # %.lr.ph.i.i30
                                        #   Parent Loop BB17_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rdx
	movups	%xmm0, -32(%rcx)
	movups	%xmm0, (%rcx)
	addq	$64, %rcx
	cmpq	%rax, %rdx
	jbe	.LBB17_12
.LBB17_13:                              # %ExistPathAboveVCG.exit31
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	DFSAboveVCG
	xorps	%xmm0, %xmm0
	movq	%rbp, %rax
	shlq	$6, %rax
	cmpq	$1, 24(%r12,%rax)
	sbbq	$-1, %rbx
	jmp	.LBB17_24
.LBB17_14:                              #   in Loop: Header=BB17_3 Depth=1
	testq	%rax, %rax
	je	.LBB17_17
# BB#15:                                # %.lr.ph.i.i33.preheader
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	$1, %edx
	.p2align	4, 0x90
.LBB17_16:                              # %.lr.ph.i.i33
                                        #   Parent Loop BB17_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rdx
	movups	%xmm0, -32(%rcx)
	movups	%xmm0, (%rcx)
	addq	$64, %rcx
	cmpq	%rax, %rdx
	jbe	.LBB17_16
.LBB17_17:                              # %ExistPathAboveVCG.exit34
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	DFSAboveVCG
	cmpq	$0, (%r15)
	jne	.LBB17_22
# BB#18:                                #   in Loop: Header=BB17_3 Depth=1
	movq	channelNets(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB17_21
# BB#19:                                # %.lr.ph.i.i36.preheader
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	$1, %edx
	.p2align	4, 0x90
.LBB17_20:                              # %.lr.ph.i.i36
                                        #   Parent Loop BB17_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rdx
	movups	%xmm0, -32(%rcx)
	movups	%xmm0, (%rcx)
	addq	$64, %rcx
	cmpq	%rax, %rdx
	jbe	.LBB17_20
.LBB17_21:                              # %ExistPathAboveVCG.exit37
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	DFSAboveVCG
	movq	%rbp, %rax
	shlq	$6, %rax
	cmpq	$0, 24(%r12,%rax)
	je	.LBB17_23
.LBB17_22:                              #   in Loop: Header=BB17_3 Depth=1
	incq	%rbx
.LBB17_23:                              #   in Loop: Header=BB17_3 Depth=1
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB17_24:                              #   in Loop: Header=BB17_3 Depth=1
	incq	%rbp
	movq	channelNets(%rip), %rax
	cmpq	%rax, %rbp
	jbe	.LBB17_3
	jmp	.LBB17_25
.LBB17_1:
	xorl	%ebx, %ebx
.LBB17_25:                              # %._crit_edge
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	VCV, .Lfunc_end17-VCV
	.cfi_endproc

	.type	VCG,@object             # @VCG
	.comm	VCG,8,8
	.type	storageRootVCG,@object  # @storageRootVCG
	.comm	storageRootVCG,8,8
	.type	storageVCG,@object      # @storageVCG
	.comm	storageVCG,8,8
	.type	storageLimitVCG,@object # @storageLimitVCG
	.comm	storageLimitVCG,8,8
	.type	SCC,@object             # @SCC
	.comm	SCC,8,8
	.type	perSCC,@object          # @perSCC
	.comm	perSCC,8,8
	.type	removeVCG,@object       # @removeVCG
	.comm	removeVCG,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"[%d]\n"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"above: "
	.size	.L.str.1, 8

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%d "
	.size	.L.str.2, 4

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"below: "
	.size	.L.str.4, 8

	.type	totalSCC,@object        # @totalSCC
	.comm	totalSCC,8,8
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"[%d]\t"
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"<%d>"
	.size	.L.str.7, 5

	.type	removeTotalVCG,@object  # @removeTotalVCG
	.comm	removeTotalVCG,8,8
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"*** VC's removed (%d) ***\n"
	.size	.L.str.10, 27

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"\n"
	.size	.Lstr, 2

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.1:
	.asciz	"\n*** Input is cyclic! ***"
	.size	.Lstr.1, 26

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"\n*** Input is acyclic! ***"
	.size	.Lstr.2, 27


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
