	.text
	.file	"btKinematicCharacterController.bc"
	.globl	_ZN30btKinematicCharacterController26computeReflectionDirectionERK9btVector3S2_
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController26computeReflectionDirectionERK9btVector3S2_,@function
_ZN30btKinematicCharacterController26computeReflectionDirectionERK9btVector3S2_: # @_ZN30btKinematicCharacterController26computeReflectionDirectionERK9btVector3S2_
	.cfi_startproc
# BB#0:
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	mulss	%xmm1, %xmm2
	pshufd	$229, %xmm0, %xmm3      # xmm3 = xmm0[1,1,2,3]
	pshufd	$229, %xmm1, %xmm4      # xmm4 = xmm1[1,1,2,3]
	mulss	%xmm3, %xmm4
	addss	%xmm2, %xmm4
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	mulss	%xmm3, %xmm5
	addss	%xmm4, %xmm5
	addss	%xmm5, %xmm5
	mulss	%xmm5, %xmm3
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm1, %xmm5
	subps	%xmm5, %xmm0
	subss	%xmm3, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	retq
.Lfunc_end0:
	.size	_ZN30btKinematicCharacterController26computeReflectionDirectionERK9btVector3S2_, .Lfunc_end0-_ZN30btKinematicCharacterController26computeReflectionDirectionERK9btVector3S2_
	.cfi_endproc

	.globl	_ZN30btKinematicCharacterController17parallelComponentERK9btVector3S2_
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController17parallelComponentERK9btVector3S2_,@function
_ZN30btKinematicCharacterController17parallelComponentERK9btVector3S2_: # @_ZN30btKinematicCharacterController17parallelComponentERK9btVector3S2_
	.cfi_startproc
# BB#0:
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	pshufd	$229, %xmm1, %xmm2      # xmm2 = xmm1[1,1,2,3]
	mulss	4(%rsi), %xmm2
	addss	%xmm0, %xmm2
	movss	8(%rdx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm0, %xmm3
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	retq
.Lfunc_end1:
	.size	_ZN30btKinematicCharacterController17parallelComponentERK9btVector3S2_, .Lfunc_end1-_ZN30btKinematicCharacterController17parallelComponentERK9btVector3S2_
	.cfi_endproc

	.globl	_ZN30btKinematicCharacterController22perpindicularComponentERK9btVector3S2_
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController22perpindicularComponentERK9btVector3S2_,@function
_ZN30btKinematicCharacterController22perpindicularComponentERK9btVector3S2_: # @_ZN30btKinematicCharacterController22perpindicularComponentERK9btVector3S2_
	.cfi_startproc
# BB#0:
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	mulss	%xmm1, %xmm2
	pshufd	$229, %xmm0, %xmm3      # xmm3 = xmm0[1,1,2,3]
	pshufd	$229, %xmm1, %xmm4      # xmm4 = xmm1[1,1,2,3]
	mulss	%xmm3, %xmm4
	addss	%xmm2, %xmm4
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	mulss	%xmm3, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm5, %xmm3
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm1, %xmm5
	subps	%xmm5, %xmm0
	subss	%xmm3, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	retq
.Lfunc_end2:
	.size	_ZN30btKinematicCharacterController22perpindicularComponentERK9btVector3S2_, .Lfunc_end2-_ZN30btKinematicCharacterController22perpindicularComponentERK9btVector3S2_
	.cfi_endproc

	.globl	_ZN30btKinematicCharacterControllerC2EP24btPairCachingGhostObjectP13btConvexShapefi
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterControllerC2EP24btPairCachingGhostObjectP13btConvexShapefi,@function
_ZN30btKinematicCharacterControllerC2EP24btPairCachingGhostObjectP13btConvexShapefi: # @_ZN30btKinematicCharacterControllerC2EP24btPairCachingGhostObjectP13btConvexShapefi
	.cfi_startproc
# BB#0:
	movq	$_ZTV30btKinematicCharacterController+16, (%rdi)
	movb	$1, 152(%rdi)
	movq	$0, 144(%rdi)
	movl	$0, 132(%rdi)
	movl	$0, 136(%rdi)
	movl	%ecx, 188(%rdi)
	movl	$1017370378, 52(%rdi)   # imm = 0x3CA3D70A
	xorps	%xmm1, %xmm1
	movups	%xmm1, 56(%rdi)
	movb	$1, 180(%rdi)
	movq	%rsi, 16(%rdi)
	movss	%xmm0, 48(%rdi)
	movl	$0, 44(%rdi)
	movq	%rdx, 24(%rdi)
	movb	$1, 181(%rdi)
	movl	$0, 184(%rdi)
	retq
.Lfunc_end3:
	.size	_ZN30btKinematicCharacterControllerC2EP24btPairCachingGhostObjectP13btConvexShapefi, .Lfunc_end3-_ZN30btKinematicCharacterControllerC2EP24btPairCachingGhostObjectP13btConvexShapefi
	.cfi_endproc

	.globl	_ZN30btKinematicCharacterControllerD2Ev
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterControllerD2Ev,@function
_ZN30btKinematicCharacterControllerD2Ev: # @_ZN30btKinematicCharacterControllerD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV30btKinematicCharacterController+16, (%rbx)
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_4
# BB#1:
	cmpb	$0, 152(%rbx)
	je	.LBB4_3
# BB#2:
	callq	_Z21btAlignedFreeInternalPv
.LBB4_3:                                # %.noexc
	movq	$0, 144(%rbx)
.LBB4_4:
	movb	$1, 152(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 132(%rbx)
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZN30btKinematicCharacterControllerD2Ev, .Lfunc_end4-_ZN30btKinematicCharacterControllerD2Ev
	.cfi_endproc

	.globl	_ZN30btKinematicCharacterControllerD0Ev
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterControllerD0Ev,@function
_ZN30btKinematicCharacterControllerD0Ev: # @_ZN30btKinematicCharacterControllerD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV30btKinematicCharacterController+16, (%rbx)
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_3
# BB#1:
	cmpb	$0, 152(%rbx)
	je	.LBB5_3
# BB#2:
.Ltmp0:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp1:
.LBB5_3:                                # %.noexc.i
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB5_4:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZN30btKinematicCharacterControllerD0Ev, .Lfunc_end5-_ZN30btKinematicCharacterControllerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end5-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN30btKinematicCharacterController14getGhostObjectEv
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController14getGhostObjectEv,@function
_ZN30btKinematicCharacterController14getGhostObjectEv: # @_ZN30btKinematicCharacterController14getGhostObjectEv
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	retq
.Lfunc_end6:
	.size	_ZN30btKinematicCharacterController14getGhostObjectEv, .Lfunc_end6-_ZN30btKinematicCharacterController14getGhostObjectEv
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	2
.LCPI7_0:
	.long	1065353216              # float 1
	.long	3212836864              # float -1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_1:
	.long	1045220557              # float 0.200000003
	.text
	.globl	_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld,@function
_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld: # @_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 144
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	40(%rsi), %rdi
	movq	(%rdi), %rbp
	movq	16(%rbx), %rax
	movq	312(%rax), %rax
	leaq	48(%rsi), %rdx
	movq	%rax, %rsi
	movq	%rdi, %rcx
	callq	*64(%rbp)
	movq	16(%rbx), %rax
	leaq	88(%rbx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movups	56(%rax), %xmm0
	movups	%xmm0, 88(%rbx)
	movq	312(%rax), %rdi
	movq	(%rdi), %rax
	callq	*72(%rax)
	testl	%eax, %eax
	jle	.LBB7_1
# BB#3:                                 # %.lr.ph105
	movq	%rbx, %r14
	subq	$-128, %r14
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB7_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_13 Depth 2
                                        #     Child Loop BB7_15 Depth 2
                                        #     Child Loop BB7_22 Depth 2
                                        #       Child Loop BB7_24 Depth 3
	movslq	132(%rbx), %r13
	testq	%r13, %r13
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	jns	.LBB7_16
# BB#5:                                 #   in Loop: Header=BB7_4 Depth=1
	cmpl	$0, 136(%rbx)
	jns	.LBB7_11
# BB#6:                                 # %_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_.exit.i.i
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_10
# BB#7:                                 #   in Loop: Header=BB7_4 Depth=1
	cmpb	$0, 152(%rbx)
	je	.LBB7_9
# BB#8:                                 #   in Loop: Header=BB7_4 Depth=1
	callq	_Z21btAlignedFreeInternalPv
.LBB7_9:                                #   in Loop: Header=BB7_4 Depth=1
	movq	$0, 144(%rbx)
.LBB7_10:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi.exit.preheader.i
                                        #   in Loop: Header=BB7_4 Depth=1
	movb	$1, 152(%rbx)
	movq	$0, 144(%rbx)
	movl	$0, 136(%rbx)
.LBB7_11:                               # %.lr.ph.i
                                        #   in Loop: Header=BB7_4 Depth=1
	movl	%r13d, %ecx
	negl	%ecx
	andq	$7, %rcx
	movq	%r13, %rax
	je	.LBB7_14
# BB#12:                                # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi.exit.i.prol.preheader
                                        #   in Loop: Header=BB7_4 Depth=1
	negq	%rcx
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB7_13:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi.exit.i.prol
                                        #   Parent Loop BB7_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	144(%rbx), %rdx
	movq	$0, (%rdx,%rax,8)
	incq	%rax
	incq	%rcx
	jne	.LBB7_13
.LBB7_14:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi.exit.i.prol.loopexit
                                        #   in Loop: Header=BB7_4 Depth=1
	cmpl	$-8, %r13d
	ja	.LBB7_16
	.p2align	4, 0x90
.LBB7_15:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi.exit.i
                                        #   Parent Loop BB7_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	144(%rbx), %rcx
	movq	$0, (%rcx,%rax,8)
	movq	144(%rbx), %rcx
	movq	$0, 8(%rcx,%rax,8)
	movq	144(%rbx), %rcx
	movq	$0, 16(%rcx,%rax,8)
	movq	144(%rbx), %rcx
	movq	$0, 24(%rcx,%rax,8)
	movq	144(%rbx), %rcx
	movq	$0, 32(%rcx,%rax,8)
	movq	144(%rbx), %rcx
	movq	$0, 40(%rcx,%rax,8)
	movq	144(%rbx), %rcx
	movq	$0, 48(%rcx,%rax,8)
	movq	144(%rbx), %rcx
	movq	$0, 56(%rcx,%rax,8)
	addq	$8, %rax
	jne	.LBB7_15
	.p2align	4, 0x90
.LBB7_16:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_.exit
                                        #   in Loop: Header=BB7_4 Depth=1
	movl	$0, 132(%rbx)
	movq	16(%rbx), %rax
	movq	312(%rax), %rdi
	movq	(%rdi), %rax
	callq	*56(%rax)
	movq	16(%rax), %rax
	movq	%r12, %rcx
	shlq	$5, %rcx
	movq	16(%rax,%rcx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_18
# BB#17:                                #   in Loop: Header=BB7_4 Depth=1
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*32(%rax)
.LBB7_18:                               # %.preheader
                                        #   in Loop: Header=BB7_4 Depth=1
	movslq	132(%rbx), %r10
	testq	%r10, %r10
	jle	.LBB7_19
# BB#21:                                # %.lr.ph97
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	16(%rbx), %r8
	movq	144(%rbx), %r9
	xorl	%esi, %esi
	xorps	%xmm6, %xmm6
	movss	.LCPI7_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB7_22:                               #   Parent Loop BB7_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_24 Depth 3
	movq	(%r9,%rsi,8), %rdi
	xorl	%eax, %eax
	cmpq	%r8, 712(%rdi)
	sete	%dl
	movl	728(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.LBB7_27
# BB#23:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB7_22 Depth=2
	movb	%dl, %al
	movss	.LCPI7_0(,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm8
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	leaq	88(%rdi), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB7_24:                               # %.lr.ph
                                        #   Parent Loop BB7_4 Depth=1
                                        #     Parent Loop BB7_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm6
	jbe	.LBB7_31
# BB#25:                                #   in Loop: Header=BB7_24 Depth=3
	ucomiss	%xmm2, %xmm1
	jbe	.LBB7_26
# BB#29:                                #   in Loop: Header=BB7_24 Depth=3
	movsd	-16(%rax), %xmm3        # xmm3 = mem[0],zero
	mulps	%xmm8, %xmm3
	imulq	$176, %rdx, %rcx
	leaq	80(%rdi,%rcx), %rbp
	movss	-8(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	xorps	%xmm5, %xmm5
	movss	%xmm4, %xmm5            # xmm5 = xmm4[0],xmm5[1,2,3]
	movlps	%xmm3, 164(%rbx)
	movlps	%xmm5, 172(%rbx)
	movss	(%rax), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movl	728(%rdi), %ecx
	movaps	%xmm2, %xmm1
	jmp	.LBB7_30
	.p2align	4, 0x90
.LBB7_26:                               # %._crit_edge117
                                        #   in Loop: Header=BB7_24 Depth=3
	leaq	-8(%rax), %rbp
	movaps	%xmm2, %xmm3
.LBB7_30:                               #   in Loop: Header=BB7_24 Depth=3
	movss	-16(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	-12(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	(%rbp), %xmm5           # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	mulss	%xmm3, %xmm2
	mulss	%xmm3, %xmm4
	mulss	%xmm3, %xmm5
	mulss	%xmm7, %xmm2
	mulss	%xmm7, %xmm4
	mulss	%xmm7, %xmm5
	addss	88(%rbx), %xmm2
	movss	%xmm2, 88(%rbx)
	addss	92(%rbx), %xmm4
	movss	%xmm4, 92(%rbx)
	addss	96(%rbx), %xmm5
	movss	%xmm5, 96(%rbx)
	movb	$1, %r15b
.LBB7_31:                               #   in Loop: Header=BB7_24 Depth=3
	incq	%rdx
	movslq	%ecx, %rbp
	addq	$176, %rax
	cmpq	%rbp, %rdx
	jl	.LBB7_24
.LBB7_27:                               # %._crit_edge
                                        #   in Loop: Header=BB7_22 Depth=2
	incq	%rsi
	cmpq	%r10, %rsi
	jl	.LBB7_22
# BB#28:                                #   in Loop: Header=BB7_4 Depth=1
	movss	%xmm1, 4(%rsp)          # 4-byte Spill
	jmp	.LBB7_20
	.p2align	4, 0x90
.LBB7_19:                               # %.preheader.._crit_edge98_crit_edge
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	16(%rbx), %r8
.LBB7_20:                               # %._crit_edge98
                                        #   in Loop: Header=BB7_4 Depth=1
	incq	%r12
	movq	312(%r8), %rdi
	movq	(%rdi), %rax
	callq	*72(%rax)
	cltq
	cmpq	%rax, %r12
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	jl	.LBB7_4
	jmp	.LBB7_2
.LBB7_1:
	xorl	%r15d, %r15d
.LBB7_2:                                # %._crit_edge106
	movq	16(%rbx), %rax
	movups	8(%rax), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	24(%rax), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	40(%rax), %xmm0
	movaps	%xmm0, 48(%rsp)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movups	(%rcx), %xmm0
	movaps	%xmm0, 64(%rsp)
	movaps	16(%rsp), %xmm0
	movups	%xmm0, 8(%rax)
	movaps	32(%rsp), %xmm0
	movups	%xmm0, 24(%rax)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 40(%rax)
	movaps	64(%rsp), %xmm0
	movups	%xmm0, 56(%rax)
	andb	$1, %r15b
	movl	%r15d, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld, .Lfunc_end7-_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	1036831949              # float 0.100000001
	.long	1036831949              # float 0.100000001
	.zero	4
	.zero	4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_1:
	.long	1036831949              # float 0.100000001
.LCPI8_2:
	.long	1065353216              # float 1
	.text
	.globl	_ZN30btKinematicCharacterController6stepUpEP16btCollisionWorld
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController6stepUpEP16btCollisionWorld,@function
_ZN30btKinematicCharacterController6stepUpEP16btCollisionWorld: # @_ZN30btKinematicCharacterController6stepUpEP16btCollisionWorld
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 16
	subq	$224, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 240
.Lcfi22:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movslq	188(%rbx), %rax
	shlq	$4, %rax
	movss	48(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	96(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	_ZL15upAxisDirection(%rax), %xmm3
	mulss	_ZL15upAxisDirection+8(%rax), %xmm2
	movsd	88(%rbx), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm4, %xmm3
	addss	%xmm0, %xmm2
	xorps	%xmm1, %xmm1
	xorps	%xmm5, %xmm5
	movss	%xmm2, %xmm5            # xmm5 = xmm2[0],xmm5[1,2,3]
	movlps	%xmm3, 108(%rbx)
	movlps	%xmm5, 116(%rbx)
	movl	$1065353216, 64(%rsp)   # imm = 0x3F800000
	movups	%xmm1, 68(%rsp)
	movl	$1065353216, 84(%rsp)   # imm = 0x3F800000
	movups	%xmm1, 88(%rsp)
	movl	$1065353216, 104(%rsp)  # imm = 0x3F800000
	movl	$0, 108(%rsp)
	movl	$1065353216, (%rsp)     # imm = 0x3F800000
	movups	%xmm1, 4(%rsp)
	movl	$1065353216, 20(%rsp)   # imm = 0x3F800000
	movups	%xmm1, 24(%rsp)
	movl	$1065353216, 40(%rsp)   # imm = 0x3F800000
	movups	%xmm1, 44(%rsp)
	movl	$0, 60(%rsp)
	movaps	_ZL15upAxisDirection(%rax), %xmm2
	mulps	.LCPI8_0(%rip), %xmm2
	movss	_ZL15upAxisDirection+8(%rax), %xmm3 # xmm3 = mem[0],zero,zero,zero
	mulss	.LCPI8_1(%rip), %xmm3
	addps	%xmm4, %xmm2
	addss	%xmm0, %xmm3
	xorps	%xmm0, %xmm0
	movss	%xmm3, %xmm0            # xmm0 = xmm3[0],xmm0[1,2,3]
	movlps	%xmm2, 112(%rsp)
	movlps	%xmm0, 120(%rsp)
	movups	108(%rbx), %xmm0
	movups	%xmm0, 48(%rsp)
	movq	16(%rbx), %rdi
	movl	$1065353216, 136(%rsp)  # imm = 0x3F800000
	movq	$0, 208(%rsp)
	movups	%xmm1, 160(%rsp)
	movups	%xmm1, 144(%rsp)
	movq	$_ZTV43btKinematicClosestNotMeConvexResultCallback+16, 128(%rsp)
	movq	%rdi, 216(%rsp)
	movq	192(%rdi), %rax
	movzwl	8(%rax), %ecx
	movw	%cx, 140(%rsp)
	movzwl	10(%rax), %eax
	movw	%ax, 142(%rsp)
	cmpb	$0, 180(%rbx)
	je	.LBB8_3
# BB#1:
	movq	24(%rbx), %rax
	movss	84(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
.Ltmp3:
	leaq	64(%rsp), %rdx
	movq	%rsp, %rcx
	leaq	128(%rsp), %r8
	movq	%rax, %rsi
	callq	_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf
.Ltmp4:
	jmp	.LBB8_4
.LBB8_3:
	movq	24(%rbx), %rax
.Ltmp5:
	leaq	64(%rsp), %rdx
	movq	%rsp, %rcx
	leaq	128(%rsp), %r8
	xorps	%xmm0, %xmm0
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf
.Ltmp6:
.LBB8_4:
	movss	136(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	48(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI8_2(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jbe	.LBB8_6
# BB#5:
	mulss	%xmm1, %xmm0
	subss	%xmm1, %xmm2
	movss	88(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	movss	108(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	movss	%xmm4, 88(%rbx)
	movss	92(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	movss	112(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	movss	%xmm4, 92(%rbx)
	mulss	96(%rbx), %xmm2
	mulss	116(%rbx), %xmm1
	addss	%xmm2, %xmm1
	movss	%xmm1, 96(%rbx)
	jmp	.LBB8_7
.LBB8_6:
	leaq	88(%rbx), %rax
	leaq	108(%rbx), %rcx
	movups	(%rcx), %xmm1
	movups	%xmm1, (%rax)
.LBB8_7:
	movss	%xmm0, 104(%rbx)
	addq	$224, %rsp
	popq	%rbx
	retq
.LBB8_2:
.Ltmp7:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN30btKinematicCharacterController6stepUpEP16btCollisionWorld, .Lfunc_end8-_ZN30btKinematicCharacterController6stepUpEP16btCollisionWorld
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin1    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Lfunc_end8-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	872415232               # float 1.1920929E-7
.LCPI9_1:
	.long	1065353216              # float 1
	.text
	.globl	_ZN30btKinematicCharacterController36updateTargetPositionBasedOnCollisionERK9btVector3ff
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController36updateTargetPositionBasedOnCollisionERK9btVector3ff,@function
_ZN30btKinematicCharacterController36updateTargetPositionBasedOnCollisionERK9btVector3ff: # @_ZN30btKinematicCharacterController36updateTargetPositionBasedOnCollisionERK9btVector3ff
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 64
.Lcfi28:
	.cfi_offset %rbx, -40
.Lcfi29:
	.cfi_offset %r12, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movss	108(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	112(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	subss	88(%rbx), %xmm6
	subss	92(%rbx), %xmm5
	movss	116(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	subss	96(%rbx), %xmm7
	movaps	%xmm6, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm5, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm7, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	sqrtss	%xmm3, %xmm8
	ucomiss	%xmm8, %xmm8
	jnp	.LBB9_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm3, %xmm0
	movss	%xmm5, 20(%rsp)         # 4-byte Spill
	movss	%xmm6, 16(%rsp)         # 4-byte Spill
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	%xmm7, 8(%rsp)          # 4-byte Spill
	movss	%xmm3, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm7          # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm8
.LBB9_2:                                # %.split
	ucomiss	.LCPI9_0(%rip), %xmm8
	jbe	.LBB9_9
# BB#3:
	xorps	%xmm0, %xmm0
	sqrtss	%xmm3, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB9_5
# BB#4:                                 # %call.sqrt97
	movaps	%xmm3, %xmm0
	movss	%xmm5, 20(%rsp)         # 4-byte Spill
	movss	%xmm6, 16(%rsp)         # 4-byte Spill
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	%xmm8, 4(%rsp)          # 4-byte Spill
	movss	%xmm7, 8(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	8(%rsp), %xmm7          # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
.LBB9_5:                                # %.split96
	leaq	108(%rbx), %r15
	leaq	88(%rbx), %r12
	movss	.LCPI9_1(%rip), %xmm10  # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm2
	divss	%xmm0, %xmm2
	mulss	%xmm2, %xmm6
	mulss	%xmm2, %xmm5
	mulss	%xmm2, %xmm7
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	mulss	%xmm6, %xmm2
	movaps	%xmm5, %xmm3
	mulss	%xmm9, %xmm3
	addss	%xmm2, %xmm3
	movss	8(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm4
	mulss	%xmm2, %xmm4
	addss	%xmm3, %xmm4
	addss	%xmm4, %xmm4
	mulss	%xmm4, %xmm0
	mulss	%xmm4, %xmm9
	mulss	%xmm2, %xmm4
	subss	%xmm0, %xmm6
	subss	%xmm9, %xmm5
	subss	%xmm4, %xmm7
	movaps	%xmm6, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm5, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm7, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	xorps	%xmm0, %xmm0
	sqrtss	%xmm3, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB9_7
# BB#6:                                 # %call.sqrt98
	movaps	%xmm3, %xmm0
	movss	%xmm5, 20(%rsp)         # 4-byte Spill
	movss	%xmm6, 16(%rsp)         # 4-byte Spill
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	%xmm8, 4(%rsp)          # 4-byte Spill
	movss	%xmm7, 8(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	.LCPI9_1(%rip), %xmm10  # xmm10 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm7          # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
.LBB9_7:                                # %.split96.split
	movss	(%r14), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movups	(%r12), %xmm4
	movups	%xmm4, (%r15)
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm1
	jne	.LBB9_8
	jnp	.LBB9_9
.LBB9_8:
	divss	%xmm0, %xmm10
	mulss	%xmm10, %xmm6
	mulss	%xmm10, %xmm5
	mulss	%xmm7, %xmm10
	movaps	%xmm3, %xmm0
	mulss	%xmm6, %xmm0
	movaps	%xmm2, %xmm4
	mulss	%xmm5, %xmm4
	addss	%xmm0, %xmm4
	movaps	%xmm9, %xmm0
	mulss	%xmm10, %xmm0
	addss	%xmm4, %xmm0
	mulss	%xmm0, %xmm3
	subss	%xmm3, %xmm6
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm5
	mulss	%xmm9, %xmm0
	subss	%xmm0, %xmm10
	mulss	%xmm1, %xmm8
	mulss	%xmm8, %xmm6
	mulss	%xmm8, %xmm5
	mulss	%xmm10, %xmm8
	addss	108(%rbx), %xmm6
	movss	%xmm6, 108(%rbx)
	addss	112(%rbx), %xmm5
	movss	%xmm5, 112(%rbx)
	addss	116(%rbx), %xmm8
	movss	%xmm8, 116(%rbx)
.LBB9_9:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	_ZN30btKinematicCharacterController36updateTargetPositionBasedOnCollisionERK9btVector3ff, .Lfunc_end9-_ZN30btKinematicCharacterController36updateTargetPositionBasedOnCollisionERK9btVector3ff
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_0:
	.long	1065353216              # float 1
.LCPI10_1:
	.long	872415232               # float 1.1920929E-7
.LCPI10_2:
	.long	1008981770              # float 0.00999999977
.LCPI10_3:
	.long	0                       # float 0
	.text
	.globl	_ZN30btKinematicCharacterController20stepForwardAndStrafeEP16btCollisionWorldRK9btVector3
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController20stepForwardAndStrafeEP16btCollisionWorldRK9btVector3,@function
_ZN30btKinematicCharacterController20stepForwardAndStrafeEP16btCollisionWorldRK9btVector3: # @_ZN30btKinematicCharacterController20stepForwardAndStrafeEP16btCollisionWorldRK9btVector3
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi38:
	.cfi_def_cfa_offset 336
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movsd	88(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movss	96(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	8(%rdx), %xmm0
	xorps	%xmm2, %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm0, %xmm3            # xmm3 = xmm0[0],xmm3[1,2,3]
	movlps	%xmm1, 108(%rbx)
	movlps	%xmm3, 116(%rbx)
	movl	$1065353216, 216(%rsp)  # imm = 0x3F800000
	movups	%xmm2, 220(%rsp)
	movl	$1065353216, 236(%rsp)  # imm = 0x3F800000
	movups	%xmm2, 240(%rsp)
	movl	$1065353216, 256(%rsp)  # imm = 0x3F800000
	movups	%xmm2, 260(%rsp)
	movl	$0, 276(%rsp)
	movl	$1065353216, 152(%rsp)  # imm = 0x3F800000
	movups	%xmm2, 156(%rsp)
	movl	$1065353216, 172(%rsp)  # imm = 0x3F800000
	movups	%xmm2, 176(%rsp)
	movl	$1065353216, 192(%rsp)  # imm = 0x3F800000
	movups	%xmm2, 196(%rsp)
	movl	$0, 212(%rsp)
	cmpb	$0, 160(%rbx)
	je	.LBB10_1
# BB#2:
	leaq	72(%rbx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movss	72(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	76(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	164(%rbx), %xmm0
	leaq	76(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	mulss	168(%rbx), %xmm1
	addss	%xmm0, %xmm1
	leaq	80(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movss	80(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	172(%rbx), %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.LBB10_4
# BB#3:
	leaq	164(%rbx), %rsi
	movss	.LCPI10_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movq	%rbx, %rdi
	callq	_ZN30btKinematicCharacterController36updateTargetPositionBasedOnCollisionERK9btVector3ff
	jmp	.LBB10_4
.LBB10_1:                               # %..preheader_crit_edge
	leaq	72(%rbx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	76(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	80(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB10_4:                               # %.preheader
	leaq	88(%rbx), %r15
	leaq	108(%rbx), %r12
	leaq	72(%rsp), %r13
	movss	.LCPI10_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movl	$11, %ebp
	.p2align	4, 0x90
.LBB10_5:                               # =>This Inner Loop Header: Depth=1
	decl	%ebp
	jle	.LBB10_25
# BB#6:                                 #   in Loop: Header=BB10_5 Depth=1
	movss	%xmm0, 28(%rsp)         # 4-byte Spill
	movups	(%r15), %xmm0
	leaq	264(%rsp), %rax
	movups	%xmm0, (%rax)
	movups	(%r12), %xmm0
	leaq	200(%rsp), %rax
	movups	%xmm0, (%rax)
	movq	16(%rbx), %rax
	movl	$1065353216, 64(%rsp)   # imm = 0x3F800000
	movq	$0, 136(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r13)
	movups	%xmm0, (%r13)
	movq	$_ZTV43btKinematicClosestNotMeConvexResultCallback+16, 56(%rsp)
	movq	%rax, 144(%rsp)
	movq	192(%rax), %rax
	movzwl	8(%rax), %ecx
	movw	%cx, 68(%rsp)
	movzwl	10(%rax), %eax
	movw	%ax, 70(%rsp)
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp8:
	callq	*88(%rax)
	movaps	%xmm0, %xmm1
.Ltmp9:
# BB#7:                                 #   in Loop: Header=BB10_5 Depth=1
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movss	52(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	addss	%xmm1, %xmm0
.Ltmp11:
	callq	*80(%rax)
.Ltmp12:
# BB#8:                                 #   in Loop: Header=BB10_5 Depth=1
	cmpb	$0, 180(%rbx)
	je	.LBB10_11
# BB#9:                                 #   in Loop: Header=BB10_5 Depth=1
	movq	16(%rbx), %rdi
	movq	24(%rbx), %rsi
	movss	84(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
.Ltmp13:
	leaq	216(%rsp), %rdx
	leaq	152(%rsp), %rcx
	leaq	56(%rsp), %r8
	callq	_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf
.Ltmp14:
	jmp	.LBB10_12
	.p2align	4, 0x90
.LBB10_11:                              #   in Loop: Header=BB10_5 Depth=1
	movq	24(%rbx), %rsi
	movss	84(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
.Ltmp15:
	movq	%r14, %rdi
	leaq	216(%rsp), %rdx
	leaq	152(%rsp), %rcx
	leaq	56(%rsp), %r8
	callq	_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf
.Ltmp16:
.LBB10_12:                              #   in Loop: Header=BB10_5 Depth=1
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp17:
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	*80(%rax)
.Ltmp18:
# BB#13:                                #   in Loop: Header=BB10_5 Depth=1
	movss	64(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI10_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jbe	.LBB10_23
# BB#14:                                #   in Loop: Header=BB10_5 Depth=1
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	movss	120(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	124(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	88(%rbx), %xmm1
	subss	92(%rbx), %xmm2
	movss	128(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	subss	96(%rbx), %xmm0
	mulss	%xmm1, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB10_16
# BB#15:                                # %call.sqrt
                                        #   in Loop: Header=BB10_5 Depth=1
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB10_16:                              # %.split
                                        #   in Loop: Header=BB10_5 Depth=1
	ucomiss	52(%rbx), %xmm1
	jbe	.LBB10_18
# BB#17:                                #   in Loop: Header=BB10_5 Depth=1
	movss	64(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI10_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	88(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	108(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	movss	%xmm3, 88(%rbx)
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	112(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	movss	%xmm3, 92(%rbx)
	mulss	96(%rbx), %xmm1
	mulss	116(%rbx), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, 96(%rbx)
.LBB10_18:                              #   in Loop: Header=BB10_5 Depth=1
.Ltmp20:
	movq	%rbx, %rdi
	leaq	104(%rsp), %rsi
	movss	.LCPI10_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	callq	_ZN30btKinematicCharacterController36updateTargetPositionBasedOnCollisionERK9btVector3ff
.Ltmp21:
# BB#19:                                #   in Loop: Header=BB10_5 Depth=1
	movss	108(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	112(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	subss	88(%rbx), %xmm4
	subss	92(%rbx), %xmm3
	movss	116(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	subss	96(%rbx), %xmm5
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	.LCPI10_1(%rip), %xmm0
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jbe	.LBB10_25
# BB#20:                                #   in Loop: Header=BB10_5 Depth=1
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB10_22
# BB#21:                                # %call.sqrt102
                                        #   in Loop: Header=BB10_5 Depth=1
	movss	%xmm3, 24(%rsp)         # 4-byte Spill
	movss	%xmm4, 20(%rsp)         # 4-byte Spill
	movss	%xmm5, 16(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	16(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB10_22:                              # %.split101
                                        #   in Loop: Header=BB10_5 Depth=1
	movss	.LCPI10_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm4
	mulss	%xmm0, %xmm3
	mulss	%xmm0, %xmm5
	movq	48(%rsp), %rax          # 8-byte Reload
	mulss	(%rax), %xmm4
	movq	40(%rsp), %rax          # 8-byte Reload
	mulss	(%rax), %xmm3
	addss	%xmm4, %xmm3
	movq	32(%rsp), %rax          # 8-byte Reload
	mulss	(%rax), %xmm5
	addss	%xmm3, %xmm5
	xorps	%xmm0, %xmm0
	ucomiss	%xmm5, %xmm0
	jb	.LBB10_24
	jmp	.LBB10_25
	.p2align	4, 0x90
.LBB10_23:                              #   in Loop: Header=BB10_5 Depth=1
	movups	(%r12), %xmm0
	movups	%xmm0, (%r15)
.LBB10_24:                              #   in Loop: Header=BB10_5 Depth=1
	movss	28(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm0
	ucomiss	.LCPI10_2(%rip), %xmm0
	ja	.LBB10_5
.LBB10_25:                              # %.critedge
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_28:
.Ltmp22:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB10_26:
.Ltmp10:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB10_10:
.Ltmp19:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN30btKinematicCharacterController20stepForwardAndStrafeEP16btCollisionWorldRK9btVector3, .Lfunc_end10-_ZN30btKinematicCharacterController20stepForwardAndStrafeEP16btCollisionWorldRK9btVector3
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp8-.Lfunc_begin2    #   Call between .Lfunc_begin2 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin2    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin2   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp18-.Ltmp11         #   Call between .Ltmp11 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin2   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin2   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Lfunc_end10-.Ltmp21    #   Call between .Ltmp21 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN30btKinematicCharacterController8stepDownEP16btCollisionWorldf
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController8stepDownEP16btCollisionWorldf,@function
_ZN30btKinematicCharacterController8stepDownEP16btCollisionWorldf: # @_ZN30btKinematicCharacterController8stepDownEP16btCollisionWorldf
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 16
	subq	$224, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 240
.Lcfi47:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movslq	188(%rbx), %rax
	shlq	$4, %rax
	movss	_ZL15upAxisDirection(%rax), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	48(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	104(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	mulss	%xmm2, %xmm3
	movss	_ZL15upAxisDirection+4(%rax), %xmm4 # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	mulss	%xmm4, %xmm5
	movss	_ZL15upAxisDirection+8(%rax), %xmm6 # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm4
	mulss	%xmm1, %xmm6
	addss	%xmm3, %xmm0
	addss	%xmm5, %xmm4
	addss	%xmm2, %xmm6
	movss	108(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 108(%rbx)
	movss	112(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm0
	movss	%xmm0, 112(%rbx)
	movss	116(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm0
	movss	%xmm0, 116(%rbx)
	movl	$1065353216, (%rsp)     # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 4(%rsp)
	movl	$1065353216, 20(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 24(%rsp)
	movl	$1065353216, 40(%rsp)   # imm = 0x3F800000
	movl	$0, 44(%rsp)
	movl	$1065353216, 64(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 68(%rsp)
	movl	$1065353216, 84(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 88(%rsp)
	movq	$1065353216, 104(%rsp)  # imm = 0x3F800000
	movups	88(%rbx), %xmm1
	movups	%xmm1, 48(%rsp)
	movups	108(%rbx), %xmm1
	movups	%xmm1, 112(%rsp)
	movq	16(%rbx), %rdi
	movl	$1065353216, 136(%rsp)  # imm = 0x3F800000
	movq	$0, 208(%rsp)
	movups	%xmm0, 160(%rsp)
	movups	%xmm0, 144(%rsp)
	movq	$_ZTV43btKinematicClosestNotMeConvexResultCallback+16, 128(%rsp)
	movq	%rdi, 216(%rsp)
	movq	192(%rdi), %rax
	movzwl	8(%rax), %ecx
	movw	%cx, 140(%rsp)
	movzwl	10(%rax), %eax
	movw	%ax, 142(%rsp)
	cmpb	$0, 180(%rbx)
	je	.LBB11_3
# BB#1:
	movq	24(%rbx), %rax
	movss	84(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
.Ltmp23:
	movq	%rsp, %rdx
	leaq	64(%rsp), %rcx
	leaq	128(%rsp), %r8
	movq	%rax, %rsi
	callq	_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf
.Ltmp24:
	jmp	.LBB11_4
.LBB11_3:
	movq	24(%rbx), %rax
	movss	84(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
.Ltmp25:
	movq	%rsp, %rdx
	leaq	64(%rsp), %rcx
	leaq	128(%rsp), %r8
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf
.Ltmp26:
.LBB11_4:
	movss	136(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI11_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB11_6
# BB#5:
	subss	%xmm0, %xmm1
	movss	88(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	108(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	movss	%xmm3, 88(%rbx)
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	112(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	movss	%xmm3, 92(%rbx)
	mulss	96(%rbx), %xmm1
	mulss	116(%rbx), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, 96(%rbx)
	jmp	.LBB11_7
.LBB11_6:
	leaq	108(%rbx), %rax
	addq	$88, %rbx
	movups	(%rax), %xmm0
	movups	%xmm0, (%rbx)
.LBB11_7:
	addq	$224, %rsp
	popq	%rbx
	retq
.LBB11_2:
.Ltmp27:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZN30btKinematicCharacterController8stepDownEP16btCollisionWorldf, .Lfunc_end11-_ZN30btKinematicCharacterController8stepDownEP16btCollisionWorldf
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp23-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp26-.Ltmp23         #   Call between .Ltmp23 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin3   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Lfunc_end11-.Ltmp26    #   Call between .Ltmp26 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_0:
	.long	1065353216              # float 1
.LCPI12_1:
	.long	872415232               # float 1.1920929E-7
	.text
	.globl	_ZN30btKinematicCharacterController16setWalkDirectionERK9btVector3
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController16setWalkDirectionERK9btVector3,@function
_ZN30btKinematicCharacterController16setWalkDirectionERK9btVector3: # @_ZN30btKinematicCharacterController16setWalkDirectionERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 48
.Lcfi50:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	$1, 181(%rbx)
	movups	(%rsi), %xmm0
	movups	%xmm0, 56(%rbx)
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	60(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	64(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB12_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB12_2:                               # %.split
	movss	.LCPI12_0(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm4
	movsd	56(%rbx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm4, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm0, %xmm3
	movaps	%xmm3, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	64(%rbx), %xmm4
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB12_4
# BB#3:                                 # %call.sqrt5
	movaps	%xmm1, %xmm0
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	movaps	%xmm4, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movaps	(%rsp), %xmm4           # 16-byte Reload
	movaps	16(%rsp), %xmm3         # 16-byte Reload
.LBB12_4:                               # %.split.split
	xorps	%xmm1, %xmm1
	movss	.LCPI12_1(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	ja	.LBB12_5
# BB#6:                                 # %.split.split
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	jmp	.LBB12_7
.LBB12_5:
	xorps	%xmm3, %xmm3
.LBB12_7:                               # %.split.split
	movlps	%xmm3, 72(%rbx)
	movlps	%xmm1, 80(%rbx)
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end12:
	.size	_ZN30btKinematicCharacterController16setWalkDirectionERK9btVector3, .Lfunc_end12-_ZN30btKinematicCharacterController16setWalkDirectionERK9btVector3
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI13_0:
	.long	1065353216              # float 1
.LCPI13_1:
	.long	872415232               # float 1.1920929E-7
	.text
	.globl	_ZN30btKinematicCharacterController26setVelocityForTimeIntervalERK9btVector3f
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController26setVelocityForTimeIntervalERK9btVector3f,@function
_ZN30btKinematicCharacterController26setVelocityForTimeIntervalERK9btVector3f: # @_ZN30btKinematicCharacterController26setVelocityForTimeIntervalERK9btVector3f
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 16
	subq	$48, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 64
.Lcfi53:
	.cfi_offset %rbx, -16
	movaps	%xmm0, %xmm3
	movq	%rdi, %rbx
	movb	$0, 181(%rbx)
	movups	(%rsi), %xmm0
	movups	%xmm0, 56(%rbx)
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	60(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	64(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB13_2
# BB#1:                                 # %call.sqrt
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB13_2:                               # %.split
	movss	.LCPI13_0(%rip), %xmm5  # xmm5 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm5
	movsd	56(%rbx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm0, %xmm4
	movaps	%xmm4, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	64(%rbx), %xmm5
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm5, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB13_4
# BB#3:                                 # %call.sqrt6
	movaps	%xmm1, %xmm0
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movaps	%xmm4, 32(%rsp)         # 16-byte Spill
	movaps	%xmm5, 16(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	16(%rsp), %xmm5         # 16-byte Reload
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB13_4:                               # %.split.split
	xorps	%xmm1, %xmm1
	movss	.LCPI13_1(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	ja	.LBB13_5
# BB#6:                                 # %.split.split
	movss	%xmm5, %xmm1            # xmm1 = xmm5[0],xmm1[1,2,3]
	jmp	.LBB13_7
.LBB13_5:
	xorps	%xmm4, %xmm4
.LBB13_7:                               # %.split.split
	movlps	%xmm4, 72(%rbx)
	movlps	%xmm1, 80(%rbx)
	movss	%xmm3, 184(%rbx)
	addq	$48, %rsp
	popq	%rbx
	retq
.Lfunc_end13:
	.size	_ZN30btKinematicCharacterController26setVelocityForTimeIntervalERK9btVector3f, .Lfunc_end13-_ZN30btKinematicCharacterController26setVelocityForTimeIntervalERK9btVector3f
	.cfi_endproc

	.globl	_ZN30btKinematicCharacterController5resetEv
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController5resetEv,@function
_ZN30btKinematicCharacterController5resetEv: # @_ZN30btKinematicCharacterController5resetEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end14:
	.size	_ZN30btKinematicCharacterController5resetEv, .Lfunc_end14-_ZN30btKinematicCharacterController5resetEv
	.cfi_endproc

	.globl	_ZN30btKinematicCharacterController4warpERK9btVector3
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController4warpERK9btVector3,@function
_ZN30btKinematicCharacterController4warpERK9btVector3: # @_ZN30btKinematicCharacterController4warpERK9btVector3
	.cfi_startproc
# BB#0:
	movl	$0, -20(%rsp)
	movups	(%rsi), %xmm0
	movups	%xmm0, -16(%rsp)
	movq	16(%rdi), %rax
	movl	$1065353216, 8(%rax)    # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 12(%rax)
	movl	$1065353216, 28(%rax)   # imm = 0x3F800000
	movups	%xmm0, 32(%rax)
	movq	$1065353216, 48(%rax)   # imm = 0x3F800000
	movups	-16(%rsp), %xmm0
	movups	%xmm0, 56(%rax)
	retq
.Lfunc_end15:
	.size	_ZN30btKinematicCharacterController4warpERK9btVector3, .Lfunc_end15-_ZN30btKinematicCharacterController4warpERK9btVector3
	.cfi_endproc

	.globl	_ZN30btKinematicCharacterController7preStepEP16btCollisionWorld
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController7preStepEP16btCollisionWorld,@function
_ZN30btKinematicCharacterController7preStepEP16btCollisionWorld: # @_ZN30btKinematicCharacterController7preStepEP16btCollisionWorld
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -24
.Lcfi58:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movb	$0, 160(%rbx)
	callq	_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld
	testb	%al, %al
	je	.LBB16_6
# BB#1:
	movb	$1, 160(%rbx)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld
	testb	%al, %al
	je	.LBB16_6
# BB#2:
	movb	$1, 160(%rbx)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld
	testb	%al, %al
	je	.LBB16_6
# BB#3:
	movb	$1, 160(%rbx)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld
	testb	%al, %al
	je	.LBB16_6
# BB#4:
	movb	$1, 160(%rbx)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld
	testb	%al, %al
	je	.LBB16_6
# BB#5:
	movb	$1, 160(%rbx)
.LBB16_6:
	movq	16(%rbx), %rax
	movups	56(%rax), %xmm0
	movups	%xmm0, 88(%rbx)
	movups	56(%rax), %xmm0
	movups	%xmm0, 108(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end16:
	.size	_ZN30btKinematicCharacterController7preStepEP16btCollisionWorld, .Lfunc_end16-_ZN30btKinematicCharacterController7preStepEP16btCollisionWorld
	.cfi_endproc

	.globl	_ZN30btKinematicCharacterController10playerStepEP16btCollisionWorldf
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController10playerStepEP16btCollisionWorldf,@function
_ZN30btKinematicCharacterController10playerStepEP16btCollisionWorldf: # @_ZN30btKinematicCharacterController10playerStepEP16btCollisionWorldf
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 32
	subq	$96, %rsp
.Lcfi62:
	.cfi_def_cfa_offset 128
.Lcfi63:
	.cfi_offset %rbx, -32
.Lcfi64:
	.cfi_offset %r14, -24
.Lcfi65:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpb	$0, 181(%rbx)
	jne	.LBB17_2
# BB#1:
	xorps	%xmm1, %xmm1
	ucomiss	184(%rbx), %xmm1
	jae	.LBB17_6
.LBB17_2:
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	16(%rbx), %rax
	movups	8(%rax), %xmm0
	movaps	%xmm0, 32(%rsp)
	leaq	48(%rsp), %r15
	movups	24(%rax), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	40(%rax), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	56(%rax), %xmm0
	movaps	%xmm0, 80(%rsp)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN30btKinematicCharacterController6stepUpEP16btCollisionWorld
	cmpb	$0, 181(%rbx)
	je	.LBB17_4
# BB#3:
	leaq	56(%rbx), %rdx
	jmp	.LBB17_5
.LBB17_4:
	movss	184(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm1
	minss	%xmm0, %xmm1
	subss	%xmm2, %xmm0
	movss	%xmm0, 184(%rbx)
	movsd	56(%rbx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm1, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm0, %xmm2
	mulss	64(%rbx), %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm2, 16(%rsp)
	movlps	%xmm0, 24(%rsp)
	leaq	16(%rsp), %rdx
.LBB17_5:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN30btKinematicCharacterController20stepForwardAndStrafeEP16btCollisionWorldRK9btVector3
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN30btKinematicCharacterController8stepDownEP16btCollisionWorldf
	movups	88(%rbx), %xmm0
	movups	%xmm0, 32(%r15)
	movq	16(%rbx), %rax
	movaps	32(%rsp), %xmm0
	movups	%xmm0, 8(%rax)
	movups	(%r15), %xmm0
	movups	%xmm0, 24(%rax)
	movups	16(%r15), %xmm0
	movups	%xmm0, 40(%rax)
	movups	32(%r15), %xmm0
	movups	%xmm0, 56(%rax)
.LBB17_6:
	addq	$96, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	_ZN30btKinematicCharacterController10playerStepEP16btCollisionWorldf, .Lfunc_end17-_ZN30btKinematicCharacterController10playerStepEP16btCollisionWorldf
	.cfi_endproc

	.globl	_ZN30btKinematicCharacterController12setFallSpeedEf
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController12setFallSpeedEf,@function
_ZN30btKinematicCharacterController12setFallSpeedEf: # @_ZN30btKinematicCharacterController12setFallSpeedEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 32(%rdi)
	retq
.Lfunc_end18:
	.size	_ZN30btKinematicCharacterController12setFallSpeedEf, .Lfunc_end18-_ZN30btKinematicCharacterController12setFallSpeedEf
	.cfi_endproc

	.globl	_ZN30btKinematicCharacterController12setJumpSpeedEf
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController12setJumpSpeedEf,@function
_ZN30btKinematicCharacterController12setJumpSpeedEf: # @_ZN30btKinematicCharacterController12setJumpSpeedEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 36(%rdi)
	retq
.Lfunc_end19:
	.size	_ZN30btKinematicCharacterController12setJumpSpeedEf, .Lfunc_end19-_ZN30btKinematicCharacterController12setJumpSpeedEf
	.cfi_endproc

	.globl	_ZN30btKinematicCharacterController16setMaxJumpHeightEf
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController16setMaxJumpHeightEf,@function
_ZN30btKinematicCharacterController16setMaxJumpHeightEf: # @_ZN30btKinematicCharacterController16setMaxJumpHeightEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 40(%rdi)
	retq
.Lfunc_end20:
	.size	_ZN30btKinematicCharacterController16setMaxJumpHeightEf, .Lfunc_end20-_ZN30btKinematicCharacterController16setMaxJumpHeightEf
	.cfi_endproc

	.globl	_ZNK30btKinematicCharacterController7canJumpEv
	.p2align	4, 0x90
	.type	_ZNK30btKinematicCharacterController7canJumpEv,@function
_ZNK30btKinematicCharacterController7canJumpEv: # @_ZNK30btKinematicCharacterController7canJumpEv
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	jmpq	*96(%rax)               # TAILCALL
.Lfunc_end21:
	.size	_ZNK30btKinematicCharacterController7canJumpEv, .Lfunc_end21-_ZNK30btKinematicCharacterController7canJumpEv
	.cfi_endproc

	.globl	_ZN30btKinematicCharacterController4jumpEv
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController4jumpEv,@function
_ZN30btKinematicCharacterController4jumpEv: # @_ZN30btKinematicCharacterController4jumpEv
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	jmpq	*80(%rax)               # TAILCALL
.Lfunc_end22:
	.size	_ZN30btKinematicCharacterController4jumpEv, .Lfunc_end22-_ZN30btKinematicCharacterController4jumpEv
	.cfi_endproc

	.globl	_ZNK30btKinematicCharacterController8onGroundEv
	.p2align	4, 0x90
	.type	_ZNK30btKinematicCharacterController8onGroundEv,@function
_ZNK30btKinematicCharacterController8onGroundEv: # @_ZNK30btKinematicCharacterController8onGroundEv
	.cfi_startproc
# BB#0:
	movb	$1, %al
	retq
.Lfunc_end23:
	.size	_ZNK30btKinematicCharacterController8onGroundEv, .Lfunc_end23-_ZNK30btKinematicCharacterController8onGroundEv
	.cfi_endproc

	.globl	_ZN30btKinematicCharacterController9debugDrawEP12btIDebugDraw
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController9debugDrawEP12btIDebugDraw,@function
_ZN30btKinematicCharacterController9debugDrawEP12btIDebugDraw: # @_ZN30btKinematicCharacterController9debugDrawEP12btIDebugDraw
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end24:
	.size	_ZN30btKinematicCharacterController9debugDrawEP12btIDebugDraw, .Lfunc_end24-_ZN30btKinematicCharacterController9debugDrawEP12btIDebugDraw
	.cfi_endproc

	.section	.text._ZN30btKinematicCharacterController12updateActionEP16btCollisionWorldf,"axG",@progbits,_ZN30btKinematicCharacterController12updateActionEP16btCollisionWorldf,comdat
	.weak	_ZN30btKinematicCharacterController12updateActionEP16btCollisionWorldf
	.p2align	4, 0x90
	.type	_ZN30btKinematicCharacterController12updateActionEP16btCollisionWorldf,@function
_ZN30btKinematicCharacterController12updateActionEP16btCollisionWorldf: # @_ZN30btKinematicCharacterController12updateActionEP16btCollisionWorldf
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 32
.Lcfi69:
	.cfi_offset %rbx, -24
.Lcfi70:
	.cfi_offset %r14, -16
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*64(%rax)
	movq	(%rbx), %rax
	movq	72(%rax), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*%rax                   # TAILCALL
.Lfunc_end25:
	.size	_ZN30btKinematicCharacterController12updateActionEP16btCollisionWorldf, .Lfunc_end25-_ZN30btKinematicCharacterController12updateActionEP16btCollisionWorldf
	.cfi_endproc

	.section	.text._ZN43btKinematicClosestNotMeConvexResultCallbackD0Ev,"axG",@progbits,_ZN43btKinematicClosestNotMeConvexResultCallbackD0Ev,comdat
	.weak	_ZN43btKinematicClosestNotMeConvexResultCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN43btKinematicClosestNotMeConvexResultCallbackD0Ev,@function
_ZN43btKinematicClosestNotMeConvexResultCallbackD0Ev: # @_ZN43btKinematicClosestNotMeConvexResultCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end26:
	.size	_ZN43btKinematicClosestNotMeConvexResultCallbackD0Ev, .Lfunc_end26-_ZN43btKinematicClosestNotMeConvexResultCallbackD0Ev
	.cfi_endproc

	.section	.text._ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy,"axG",@progbits,_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy,comdat
	.weak	_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy
	.p2align	4, 0x90
	.type	_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy,@function
_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy: # @_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy
	.cfi_startproc
# BB#0:
	movzwl	14(%rdi), %eax
	testw	8(%rsi), %ax
	je	.LBB27_1
# BB#2:
	movzwl	10(%rsi), %eax
	testw	12(%rdi), %ax
	setne	%al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB27_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end27:
	.size	_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy, .Lfunc_end27-_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI28_0:
	.long	1065353216              # float 1
	.section	.text._ZN43btKinematicClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb,"axG",@progbits,_ZN43btKinematicClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb,comdat
	.weak	_ZN43btKinematicClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb
	.p2align	4, 0x90
	.type	_ZN43btKinematicClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb,@function
_ZN43btKinematicClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb: # @_ZN43btKinematicClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb
	.cfi_startproc
# BB#0:
	movq	(%rsi), %rax
	cmpq	88(%rdi), %rax
	je	.LBB28_1
# BB#2:
	movl	48(%rsi), %ecx
	movl	%ecx, 8(%rdi)
	movq	%rax, 80(%rdi)
	testb	%dl, %dl
	je	.LBB28_4
# BB#3:
	movups	16(%rsi), %xmm0
	movups	%xmm0, 48(%rdi)
	jmp	.LBB28_5
.LBB28_1:
	movss	.LCPI28_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	retq
.LBB28_4:
	movss	16(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	20(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	24(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	28(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	32(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	mulss	40(%rax), %xmm2
	mulss	44(%rax), %xmm1
	addss	%xmm2, %xmm1
	mulss	48(%rax), %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm3, 48(%rdi)
	movlps	%xmm1, 56(%rdi)
.LBB28_5:                               # %_ZN16btCollisionWorld27ClosestConvexResultCallback15addSingleResultERNS_17LocalConvexResultEb.exit
	movups	32(%rsi), %xmm0
	movups	%xmm0, 64(%rdi)
	movss	48(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end28:
	.size	_ZN43btKinematicClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb, .Lfunc_end28-_ZN43btKinematicClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb
	.cfi_endproc

	.section	.text._ZN16btCollisionWorld20ConvexResultCallbackD2Ev,"axG",@progbits,_ZN16btCollisionWorld20ConvexResultCallbackD2Ev,comdat
	.weak	_ZN16btCollisionWorld20ConvexResultCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld20ConvexResultCallbackD2Ev,@function
_ZN16btCollisionWorld20ConvexResultCallbackD2Ev: # @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end29:
	.size	_ZN16btCollisionWorld20ConvexResultCallbackD2Ev, .Lfunc_end29-_ZN16btCollisionWorld20ConvexResultCallbackD2Ev
	.cfi_endproc

	.type	_ZL15upAxisDirection,@object # @_ZL15upAxisDirection
	.data
	.p2align	4
_ZL15upAxisDirection:
	.long	1065353216              # float 1
	.long	0                       # float 0
	.long	0                       # float 0
	.long	0                       # float 0
	.long	0                       # float 0
	.long	1065353216              # float 1
	.long	0                       # float 0
	.long	0                       # float 0
	.long	0                       # float 0
	.long	0                       # float 0
	.long	1065353216              # float 1
	.long	0                       # float 0
	.size	_ZL15upAxisDirection, 48

	.type	_ZTV30btKinematicCharacterController,@object # @_ZTV30btKinematicCharacterController
	.section	.rodata,"a",@progbits
	.globl	_ZTV30btKinematicCharacterController
	.p2align	3
_ZTV30btKinematicCharacterController:
	.quad	0
	.quad	_ZTI30btKinematicCharacterController
	.quad	_ZN30btKinematicCharacterControllerD2Ev
	.quad	_ZN30btKinematicCharacterControllerD0Ev
	.quad	_ZN30btKinematicCharacterController12updateActionEP16btCollisionWorldf
	.quad	_ZN30btKinematicCharacterController9debugDrawEP12btIDebugDraw
	.quad	_ZN30btKinematicCharacterController16setWalkDirectionERK9btVector3
	.quad	_ZN30btKinematicCharacterController26setVelocityForTimeIntervalERK9btVector3f
	.quad	_ZN30btKinematicCharacterController5resetEv
	.quad	_ZN30btKinematicCharacterController4warpERK9btVector3
	.quad	_ZN30btKinematicCharacterController7preStepEP16btCollisionWorld
	.quad	_ZN30btKinematicCharacterController10playerStepEP16btCollisionWorldf
	.quad	_ZNK30btKinematicCharacterController7canJumpEv
	.quad	_ZN30btKinematicCharacterController4jumpEv
	.quad	_ZNK30btKinematicCharacterController8onGroundEv
	.size	_ZTV30btKinematicCharacterController, 120

	.type	_ZTS30btKinematicCharacterController,@object # @_ZTS30btKinematicCharacterController
	.globl	_ZTS30btKinematicCharacterController
	.p2align	4
_ZTS30btKinematicCharacterController:
	.asciz	"30btKinematicCharacterController"
	.size	_ZTS30btKinematicCharacterController, 33

	.type	_ZTS30btCharacterControllerInterface,@object # @_ZTS30btCharacterControllerInterface
	.section	.rodata._ZTS30btCharacterControllerInterface,"aG",@progbits,_ZTS30btCharacterControllerInterface,comdat
	.weak	_ZTS30btCharacterControllerInterface
	.p2align	4
_ZTS30btCharacterControllerInterface:
	.asciz	"30btCharacterControllerInterface"
	.size	_ZTS30btCharacterControllerInterface, 33

	.type	_ZTS17btActionInterface,@object # @_ZTS17btActionInterface
	.section	.rodata._ZTS17btActionInterface,"aG",@progbits,_ZTS17btActionInterface,comdat
	.weak	_ZTS17btActionInterface
	.p2align	4
_ZTS17btActionInterface:
	.asciz	"17btActionInterface"
	.size	_ZTS17btActionInterface, 20

	.type	_ZTI17btActionInterface,@object # @_ZTI17btActionInterface
	.section	.rodata._ZTI17btActionInterface,"aG",@progbits,_ZTI17btActionInterface,comdat
	.weak	_ZTI17btActionInterface
	.p2align	3
_ZTI17btActionInterface:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS17btActionInterface
	.size	_ZTI17btActionInterface, 16

	.type	_ZTI30btCharacterControllerInterface,@object # @_ZTI30btCharacterControllerInterface
	.section	.rodata._ZTI30btCharacterControllerInterface,"aG",@progbits,_ZTI30btCharacterControllerInterface,comdat
	.weak	_ZTI30btCharacterControllerInterface
	.p2align	4
_ZTI30btCharacterControllerInterface:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS30btCharacterControllerInterface
	.quad	_ZTI17btActionInterface
	.size	_ZTI30btCharacterControllerInterface, 24

	.type	_ZTI30btKinematicCharacterController,@object # @_ZTI30btKinematicCharacterController
	.section	.rodata,"a",@progbits
	.globl	_ZTI30btKinematicCharacterController
	.p2align	4
_ZTI30btKinematicCharacterController:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS30btKinematicCharacterController
	.quad	_ZTI30btCharacterControllerInterface
	.size	_ZTI30btKinematicCharacterController, 24

	.type	_ZTV43btKinematicClosestNotMeConvexResultCallback,@object # @_ZTV43btKinematicClosestNotMeConvexResultCallback
	.section	.rodata._ZTV43btKinematicClosestNotMeConvexResultCallback,"aG",@progbits,_ZTV43btKinematicClosestNotMeConvexResultCallback,comdat
	.weak	_ZTV43btKinematicClosestNotMeConvexResultCallback
	.p2align	3
_ZTV43btKinematicClosestNotMeConvexResultCallback:
	.quad	0
	.quad	_ZTI43btKinematicClosestNotMeConvexResultCallback
	.quad	_ZN16btCollisionWorld20ConvexResultCallbackD2Ev
	.quad	_ZN43btKinematicClosestNotMeConvexResultCallbackD0Ev
	.quad	_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy
	.quad	_ZN43btKinematicClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb
	.size	_ZTV43btKinematicClosestNotMeConvexResultCallback, 48

	.type	_ZTS43btKinematicClosestNotMeConvexResultCallback,@object # @_ZTS43btKinematicClosestNotMeConvexResultCallback
	.section	.rodata._ZTS43btKinematicClosestNotMeConvexResultCallback,"aG",@progbits,_ZTS43btKinematicClosestNotMeConvexResultCallback,comdat
	.weak	_ZTS43btKinematicClosestNotMeConvexResultCallback
	.p2align	4
_ZTS43btKinematicClosestNotMeConvexResultCallback:
	.asciz	"43btKinematicClosestNotMeConvexResultCallback"
	.size	_ZTS43btKinematicClosestNotMeConvexResultCallback, 46

	.type	_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE,@object # @_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE
	.section	.rodata._ZTSN16btCollisionWorld27ClosestConvexResultCallbackE,"aG",@progbits,_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE,comdat
	.weak	_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE
	.p2align	4
_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE:
	.asciz	"N16btCollisionWorld27ClosestConvexResultCallbackE"
	.size	_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE, 50

	.type	_ZTSN16btCollisionWorld20ConvexResultCallbackE,@object # @_ZTSN16btCollisionWorld20ConvexResultCallbackE
	.section	.rodata._ZTSN16btCollisionWorld20ConvexResultCallbackE,"aG",@progbits,_ZTSN16btCollisionWorld20ConvexResultCallbackE,comdat
	.weak	_ZTSN16btCollisionWorld20ConvexResultCallbackE
	.p2align	4
_ZTSN16btCollisionWorld20ConvexResultCallbackE:
	.asciz	"N16btCollisionWorld20ConvexResultCallbackE"
	.size	_ZTSN16btCollisionWorld20ConvexResultCallbackE, 43

	.type	_ZTIN16btCollisionWorld20ConvexResultCallbackE,@object # @_ZTIN16btCollisionWorld20ConvexResultCallbackE
	.section	.rodata._ZTIN16btCollisionWorld20ConvexResultCallbackE,"aG",@progbits,_ZTIN16btCollisionWorld20ConvexResultCallbackE,comdat
	.weak	_ZTIN16btCollisionWorld20ConvexResultCallbackE
	.p2align	3
_ZTIN16btCollisionWorld20ConvexResultCallbackE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN16btCollisionWorld20ConvexResultCallbackE
	.size	_ZTIN16btCollisionWorld20ConvexResultCallbackE, 16

	.type	_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE,@object # @_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE
	.section	.rodata._ZTIN16btCollisionWorld27ClosestConvexResultCallbackE,"aG",@progbits,_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE,comdat
	.weak	_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE
	.p2align	4
_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE
	.quad	_ZTIN16btCollisionWorld20ConvexResultCallbackE
	.size	_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE, 24

	.type	_ZTI43btKinematicClosestNotMeConvexResultCallback,@object # @_ZTI43btKinematicClosestNotMeConvexResultCallback
	.section	.rodata._ZTI43btKinematicClosestNotMeConvexResultCallback,"aG",@progbits,_ZTI43btKinematicClosestNotMeConvexResultCallback,comdat
	.weak	_ZTI43btKinematicClosestNotMeConvexResultCallback
	.p2align	4
_ZTI43btKinematicClosestNotMeConvexResultCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS43btKinematicClosestNotMeConvexResultCallback
	.quad	_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE
	.size	_ZTI43btKinematicClosestNotMeConvexResultCallback, 24


	.globl	_ZN30btKinematicCharacterControllerC1EP24btPairCachingGhostObjectP13btConvexShapefi
	.type	_ZN30btKinematicCharacterControllerC1EP24btPairCachingGhostObjectP13btConvexShapefi,@function
_ZN30btKinematicCharacterControllerC1EP24btPairCachingGhostObjectP13btConvexShapefi = _ZN30btKinematicCharacterControllerC2EP24btPairCachingGhostObjectP13btConvexShapefi
	.globl	_ZN30btKinematicCharacterControllerD1Ev
	.type	_ZN30btKinematicCharacterControllerD1Ev,@function
_ZN30btKinematicCharacterControllerD1Ev = _ZN30btKinematicCharacterControllerD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
