	.text
	.file	"StringConvert.bc"
	.globl	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
	.p2align	4, 0x90
	.type	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj,@function
_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj: # @_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, global_use_utf16_conversion(%rip)
	movq	%rsi, (%rsp)            # 8-byte Spill
	je	.LBB0_10
# BB#1:
	movslq	8(%rsi), %rbx
	testq	%rbx, %rbx
	je	.LBB0_10
# BB#2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r15
	movq	%r15, (%r14)
	movl	$0, (%r15)
	movl	$4, 12(%r14)
	cmpl	$4, %ebx
	jge	.LBB0_4
# BB#3:
	movq	(%rsp), %rcx            # 8-byte Reload
	jmp	.LBB0_8
.LBB0_4:
	incq	%rbx
	cmpl	$4, %ebx
	jne	.LBB0_6
# BB#5:
	movl	$3, %ebx
	movq	(%rsp), %rcx            # 8-byte Reload
	jmp	.LBB0_8
.LBB0_6:
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp0:
	callq	_Znam
	movq	%rax, %r13
.Ltmp1:
# BB#7:                                 # %._crit_edge16.i.i
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	8(%rcx), %eax
	movq	%r13, (%r14)
	movl	$0, (%r13)
	movl	%ebx, 12(%r14)
	movq	%r13, %r15
	movl	%eax, %ebx
.LBB0_8:
	movq	(%rcx), %rsi
	incl	%ebx
	movslq	%ebx, %rdx
	movq	%r15, %rdi
	callq	mbstowcs
	testl	%eax, %eax
	js	.LBB0_9
# BB#55:                                # %_ZN11CStringBaseIwED2Ev.exit36
	movslq	%eax, %rcx
	movl	$0, (%r15,%rcx,4)
	movl	%eax, 8(%r14)
	jmp	.LBB0_56
.LBB0_9:
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB0_10:                               # %_ZN11CStringBaseIwED2Ev.exit36.thread
	movl	$16, %edi
	callq	_Znam
	movq	(%rsp), %r8             # 8-byte Reload
	movq	%rax, %r15
	movl	$0, (%r15)
	movl	8(%r8), %r12d
	testl	%r12d, %r12d
	jle	.LBB0_11
# BB#19:                                # %.lr.ph
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	%r15, %rbx
	movl	$4, %r15d
	movq	$-8, %r14
	xorl	%r13d, %r13d
	movl	$16, %edi
	movq	%rbx, %rbp
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	jmp	.LBB0_20
.LBB0_11:
	xorl	%r13d, %r13d
	movq	%r15, %rbp
	jmp	.LBB0_13
.LBB0_40:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_20 Depth=1
	leaq	-8(%r13), %r8
	movq	%r8, %rsi
	shrq	$3, %rsi
	incq	%rsi
	testb	$3, %sil
	je	.LBB0_41
# BB#42:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_20 Depth=1
	movl	%r14d, %esi
	shrl	$3, %esi
	incl	%esi
	andl	$3, %esi
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_43:                               # %vector.body.prol
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbp,%rdx,4), %xmm0
	movups	16(%rbp,%rdx,4), %xmm1
	movups	%xmm0, (%rbx,%rdx,4)
	movups	%xmm1, 16(%rbx,%rdx,4)
	addq	$8, %rdx
	incq	%rsi
	jne	.LBB0_43
	jmp	.LBB0_44
.LBB0_41:                               #   in Loop: Header=BB0_20 Depth=1
	xorl	%edx, %edx
.LBB0_44:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_20 Depth=1
	cmpq	$24, %r8
	jb	.LBB0_47
# BB#45:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_20 Depth=1
	movq	%r13, %r8
	andq	$-8, %r8
	subq	%rdx, %r8
	leaq	112(%rax,%rdx,4), %rax
	leaq	112(%rbp,%rdx,4), %rsi
	.p2align	4, 0x90
.LBB0_46:                               # %vector.body
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rax)
	movups	%xmm1, -96(%rax)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rax)
	movups	%xmm1, -64(%rax)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rax)
	movups	%xmm1, -32(%rax)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	subq	$-128, %rax
	subq	$-128, %rsi
	addq	$-32, %r8
	jne	.LBB0_46
.LBB0_47:                               # %middle.block
                                        #   in Loop: Header=BB0_20 Depth=1
	cmpq	%rcx, %r13
	jne	.LBB0_32
	jmp	.LBB0_49
	.p2align	4, 0x90
.LBB0_20:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_43 Depth 2
                                        #     Child Loop BB0_46 Depth 2
                                        #     Child Loop BB0_34 Depth 2
                                        #     Child Loop BB0_36 Depth 2
	movq	(%r8), %rax
	movzbl	(%rax,%r13), %r9d
	movl	%r15d, %eax
	subl	%r13d, %eax
	cmpl	$1, %eax
	jle	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_20 Depth=1
	movl	%r15d, %ecx
	jmp	.LBB0_51
	.p2align	4, 0x90
.LBB0_22:                               #   in Loop: Header=BB0_20 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$8, %r15d
	movl	$4, %edx
	cmovgl	%edi, %edx
	cmpl	$65, %r15d
	jl	.LBB0_24
# BB#23:                                # %select.true.sink
                                        #   in Loop: Header=BB0_20 Depth=1
	movl	%r15d, %edx
	shrl	$31, %edx
	addl	%r15d, %edx
	sarl	%edx
.LBB0_24:                               # %select.end
                                        #   in Loop: Header=BB0_20 Depth=1
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%r15,%rsi), %eax
	cmpl	%r15d, %eax
	jne	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_20 Depth=1
	movl	%r15d, %ecx
	jmp	.LBB0_51
	.p2align	4, 0x90
.LBB0_26:                               #   in Loop: Header=BB0_20 Depth=1
	movl	%r9d, 12(%rsp)          # 4-byte Spill
	movl	%eax, 28(%rsp)          # 4-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp3:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp4:
# BB#27:                                # %.noexc34
                                        #   in Loop: Header=BB0_20 Depth=1
	movq	%rax, %rbx
	testl	%r15d, %r15d
	jle	.LBB0_28
# BB#29:                                # %.preheader.i.i26
                                        #   in Loop: Header=BB0_20 Depth=1
	testq	%r13, %r13
	movq	(%rsp), %r8             # 8-byte Reload
	movl	12(%rsp), %r9d          # 4-byte Reload
	jle	.LBB0_48
# BB#30:                                # %.lr.ph.i.i27.preheader
                                        #   in Loop: Header=BB0_20 Depth=1
	cmpq	$7, %r13
	movq	16(%rsp), %rdi          # 8-byte Reload
	jbe	.LBB0_31
# BB#37:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_20 Depth=1
	movq	%r13, %rcx
	andq	$-8, %rcx
	je	.LBB0_31
# BB#38:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_20 Depth=1
	leaq	(%rbp,%r13,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB0_40
# BB#39:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_20 Depth=1
	leaq	(,%r13,4), %rdx
	addq	%rax, %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_40
.LBB0_31:                               #   in Loop: Header=BB0_20 Depth=1
	xorl	%ecx, %ecx
.LBB0_32:                               # %.lr.ph.i.i27.preheader92
                                        #   in Loop: Header=BB0_20 Depth=1
	leaq	-1(%r13), %rax
	movl	%r13d, %edx
	subl	%ecx, %edx
	subq	%rcx, %rax
	testb	$7, %dl
	je	.LBB0_35
# BB#33:                                # %.lr.ph.i.i27.prol.preheader
                                        #   in Loop: Header=BB0_20 Depth=1
	movl	%r13d, %edx
	subl	%ecx, %edx
	andl	$7, %edx
	negq	%rdx
	.p2align	4, 0x90
.LBB0_34:                               # %.lr.ph.i.i27.prol
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rcx,4), %esi
	movl	%esi, (%rbx,%rcx,4)
	incq	%rcx
	incq	%rdx
	jne	.LBB0_34
.LBB0_35:                               # %.lr.ph.i.i27.prol.loopexit
                                        #   in Loop: Header=BB0_20 Depth=1
	cmpq	$7, %rax
	jb	.LBB0_49
	.p2align	4, 0x90
.LBB0_36:                               # %.lr.ph.i.i27
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rcx,4), %eax
	movl	%eax, (%rbx,%rcx,4)
	movl	4(%rbp,%rcx,4), %eax
	movl	%eax, 4(%rbx,%rcx,4)
	movl	8(%rbp,%rcx,4), %eax
	movl	%eax, 8(%rbx,%rcx,4)
	movl	12(%rbp,%rcx,4), %eax
	movl	%eax, 12(%rbx,%rcx,4)
	movl	16(%rbp,%rcx,4), %eax
	movl	%eax, 16(%rbx,%rcx,4)
	movl	20(%rbp,%rcx,4), %eax
	movl	%eax, 20(%rbx,%rcx,4)
	movl	24(%rbp,%rcx,4), %eax
	movl	%eax, 24(%rbx,%rcx,4)
	movl	28(%rbp,%rcx,4), %eax
	movl	%eax, 28(%rbx,%rcx,4)
	addq	$8, %rcx
	cmpq	%rcx, %r13
	jne	.LBB0_36
	jmp	.LBB0_49
.LBB0_28:                               #   in Loop: Header=BB0_20 Depth=1
	movq	(%rsp), %r8             # 8-byte Reload
	movl	12(%rsp), %r9d          # 4-byte Reload
	jmp	.LBB0_50
.LBB0_48:                               # %._crit_edge.i.i28
                                        #   in Loop: Header=BB0_20 Depth=1
	testq	%rbp, %rbp
	movq	16(%rsp), %rdi          # 8-byte Reload
	je	.LBB0_50
.LBB0_49:                               # %._crit_edge.thread.i.i32
                                        #   in Loop: Header=BB0_20 Depth=1
	callq	_ZdaPv
	movl	12(%rsp), %r9d          # 4-byte Reload
	movq	(%rsp), %r8             # 8-byte Reload
	movl	8(%r8), %r12d
.LBB0_50:                               # %._crit_edge16.i.i33
                                        #   in Loop: Header=BB0_20 Depth=1
	movl	$0, (%rbx,%r13,4)
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rbp
	movl	$16, %edi
	movl	28(%rsp), %ecx          # 4-byte Reload
.LBB0_51:                               #   in Loop: Header=BB0_20 Depth=1
	movl	%r9d, (%rbp,%r13,4)
	movl	$0, 4(%rbp,%r13,4)
	incq	%r13
	movslq	%r12d, %rax
	incq	%r14
	cmpq	%rax, %r13
	movl	%ecx, %r15d
	jl	.LBB0_20
# BB#12:                                # %._crit_edge16.i.i37.loopexit
	movq	%rbx, %r15
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB0_13:                               # %._crit_edge16.i.i37
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	leal	1(%r13), %ebx
	movslq	%ebx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp6:
	callq	_Znam
.Ltmp7:
# BB#14:                                # %.noexc38
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%ebx, 12(%r14)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_15:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB0_15
# BB#16:
	movl	%r13d, 8(%r14)
	testq	%rbp, %rbp
	je	.LBB0_56
# BB#17:
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB0_56:                               # %_ZN11CStringBaseIwED2Ev.exit24
	movq	%r14, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_57:
.Ltmp2:
	movq	%rax, %r14
	movq	%r15, %rdi
	jmp	.LBB0_58
.LBB0_52:
.Ltmp8:
	movq	%rax, %r14
	movq	%r15, %rbx
	testq	%rbp, %rbp
	jne	.LBB0_54
	jmp	.LBB0_59
.LBB0_18:
.Ltmp5:
	movq	%rax, %r14
	testq	%rbp, %rbp
	je	.LBB0_59
.LBB0_54:
	movq	%rbx, %rdi
.LBB0_58:                               # %_ZN11CStringBaseIwED2Ev.exit35
	callq	_ZdaPv
.LBB0_59:                               # %_ZN11CStringBaseIwED2Ev.exit35
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj, .Lfunc_end0-_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\320"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Lfunc_end0-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
	.p2align	4, 0x90
	.type	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj,@function
_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj: # @_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	cmpl	$0, global_use_utf16_conversion(%rip)
	je	.LBB1_8
# BB#1:
	movl	8(%r13), %ebx
	testl	%ebx, %ebx
	je	.LBB1_8
# BB#2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r15
	movq	%r15, (%r14)
	movb	$0, (%r15)
	movl	$4, 12(%r14)
	addl	%ebx, %ebx
	leal	(%rbx,%rbx,2), %ebx
	orl	$1, %ebx
	cmpl	$4, %ebx
	jl	.LBB1_6
# BB#3:
	leal	1(%rbx), %ebp
	cmpl	$4, %ebp
	je	.LBB1_6
# BB#4:
	movslq	%ebp, %rdi
.Ltmp9:
	callq	_Znam
	movq	%rax, %r12
.Ltmp10:
# BB#5:
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	%r12, (%r14)
	movb	$0, (%r12)
	movl	%ebp, 12(%r14)
	movq	%r12, %r15
.LBB1_6:
	movq	(%r13), %rsi
	movslq	%ebx, %rdx
	movq	%r15, %rdi
	callq	wcstombs
	testl	%eax, %eax
	js	.LBB1_7
# BB#26:                                # %_ZN11CStringBaseIcED2Ev.exit32
	movslq	%eax, %rcx
	movb	$0, (%r15,%rcx)
	movl	%eax, 8(%r14)
	jmp	.LBB1_27
.LBB1_7:
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB1_8:                                # %_ZN11CStringBaseIcED2Ev.exit32.thread
	movq	$0, 16(%rsp)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, 8(%rsp)
	movb	$0, (%rax)
	movl	$4, 20(%rsp)
	cmpl	$0, 8(%r13)
	jle	.LBB1_9
# BB#19:                                # %.lr.ph
	xorl	%ebx, %ebx
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB1_20:                               # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movl	(%rax,%rbx,4), %eax
	cmpl	$256, %eax              # imm = 0x100
	jl	.LBB1_22
# BB#21:                                #   in Loop: Header=BB1_20 Depth=1
.Ltmp14:
	movl	$63, %esi
	movq	%r15, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp15:
	jmp	.LBB1_23
	.p2align	4, 0x90
.LBB1_22:                               #   in Loop: Header=BB1_20 Depth=1
.Ltmp12:
	movsbl	%al, %esi
	movq	%r15, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp13:
.LBB1_23:                               #   in Loop: Header=BB1_20 Depth=1
	incq	%rbx
	movslq	8(%r13), %rax
	cmpq	%rax, %rbx
	jl	.LBB1_20
# BB#12:                                # %._crit_edge
	leaq	16(%rsp), %r15
	movl	16(%rsp), %eax
	leaq	8(%r14), %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	%eax, %ebp
	incl	%ebp
	jne	.LBB1_10
# BB#13:
	xorl	%ecx, %ecx
	jmp	.LBB1_14
.LBB1_9:                                # %._crit_edge.thread
	leaq	16(%rsp), %r15
	leaq	8(%r14), %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	xorl	%eax, %eax
	movl	$1, %ebp
.LBB1_10:
	movslq	%ebp, %rcx
	cmpl	$-1, %eax
	movq	$-1, %rdi
	cmovgeq	%rcx, %rdi
.Ltmp17:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp18:
# BB#11:                                # %.noexc33
	movq	%rcx, (%r14)
	movb	$0, (%rcx)
	movl	%ebp, 12(%r14)
.LBB1_14:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	8(%rsp), %rsi
	.p2align	4, 0x90
.LBB1_15:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %eax
	incq	%rsi
	movb	%al, (%rcx)
	incq	%rcx
	testb	%al, %al
	jne	.LBB1_15
# BB#16:
	movl	(%r15), %eax
	movl	%eax, (%rbx)
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_27
# BB#17:
	callq	_ZdaPv
.LBB1_27:
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_28:
.Ltmp11:
	movq	%rax, %rbx
	movq	%r15, %rdi
	jmp	.LBB1_29
.LBB1_24:
.Ltmp19:
	jmp	.LBB1_25
.LBB1_18:
.Ltmp16:
.LBB1_25:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_30
.LBB1_29:                               # %_ZN11CStringBaseIcED2Ev.exit31
	callq	_ZdaPv
.LBB1_30:                               # %_ZN11CStringBaseIcED2Ev.exit31
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj, .Lfunc_end1-_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\320"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp9-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp14-.Ltmp10         #   Call between .Ltmp10 and .Ltmp14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp13-.Ltmp14         #   Call between .Ltmp14 and .Ltmp13
	.long	.Ltmp16-.Lfunc_begin1   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin1   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Lfunc_end1-.Ltmp18     #   Call between .Ltmp18 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIcEpLEc,"axG",@progbits,_ZN11CStringBaseIcEpLEc,comdat
	.weak	_ZN11CStringBaseIcEpLEc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEc,@function
_ZN11CStringBaseIcEpLEc:                # @_ZN11CStringBaseIcEpLEc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB2_28
# BB#1:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB2_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB2_3:                                # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rsi,%rbx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB2_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB2_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB2_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB2_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB2_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB2_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB2_17
.LBB2_7:
	xorl	%ecx, %ecx
.LBB2_8:                                # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB2_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB2_10:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB2_10
.LBB2_11:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB2_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB2_13:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB2_13
	jmp	.LBB2_26
.LBB2_25:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB2_27
.LBB2_26:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB2_27:
	movq	%r12, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%r13)
.LBB2_28:                               # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	movb	%r14b, (%rax,%rcx)
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movb	$0, 1(%rax,%rcx)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_17:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r12,%rbx)
	movups	%xmm1, 16(%r12,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB2_20
	jmp	.LBB2_21
.LBB2_18:
	xorl	%ebx, %ebx
.LBB2_21:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB2_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB2_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB2_23
.LBB2_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB2_8
	jmp	.LBB2_26
.Lfunc_end2:
	.size	_ZN11CStringBaseIcEpLEc, .Lfunc_end2-_ZN11CStringBaseIcEpLEc
	.cfi_endproc

	.type	global_use_utf16_conversion,@object # @global_use_utf16_conversion
	.bss
	.globl	global_use_utf16_conversion
	.p2align	2
global_use_utf16_conversion:
	.long	0                       # 0x0
	.size	global_use_utf16_conversion, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
