	.text
	.file	"StreamUtils.bc"
	.globl	_Z10ReadStreamP19ISequentialInStreamPvPm
	.p2align	4, 0x90
	.type	_Z10ReadStreamP19ISequentialInStreamPvPm,@function
_Z10ReadStreamP19ISequentialInStreamPvPm: # @_Z10ReadStreamP19ISequentialInStreamPvPm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	(%r14), %rbp
	movq	$0, (%r14)
	leaq	4(%rsp), %r12
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_1:                                # %.thread
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbp, %rbp
	je	.LBB0_5
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	testq	$-2147483648, %rbp      # imm = 0x80000000
	movl	$-2147483648, %edx      # imm = 0x80000000
	cmovel	%ebp, %edx
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rcx
	callq	*40(%rax)
	movl	4(%rsp), %ecx
	addq	%rcx, (%r14)
	testl	%eax, %eax
	jne	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	subq	%rcx, %rbp
	addq	%rcx, %rbx
	testl	%ecx, %ecx
	jne	.LBB0_1
	jmp	.LBB0_5
.LBB0_4:
	movl	%eax, %r13d
.LBB0_5:                                # %.thread30
	movl	%r13d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_Z10ReadStreamP19ISequentialInStreamPvPm, .Lfunc_end0-_Z10ReadStreamP19ISequentialInStreamPvPm
	.cfi_endproc

	.globl	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	.p2align	4, 0x90
	.type	_Z16ReadStream_FALSEP19ISequentialInStreamPvm,@function
_Z16ReadStream_FALSEP19ISequentialInStreamPvm: # @_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	xorl	%r13d, %r13d
	leaq	12(%rsp), %r12
	xorl	%ebp, %ebp
	movq	%r14, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_1:                                # %.thread.i
                                        # =>This Inner Loop Header: Depth=1
	testq	%r14, %r14
	je	.LBB1_5
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	testq	$-2147483648, %r14      # imm = 0x80000000
	movl	$-2147483648, %edx      # imm = 0x80000000
	cmovel	%r14d, %edx
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rcx
	callq	*40(%rax)
	movl	12(%rsp), %ecx
	addq	%rcx, %rbp
	testl	%eax, %eax
	jne	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_1 Depth=1
	subq	%rcx, %r14
	addq	%rcx, %rbx
	testl	%ecx, %ecx
	jne	.LBB1_1
	jmp	.LBB1_5
.LBB1_4:
	movl	%eax, %r13d
.LBB1_5:                                # %_Z10ReadStreamP19ISequentialInStreamPvPm.exit
	xorl	%eax, %eax
	cmpq	16(%rsp), %rbp          # 8-byte Folded Reload
	setne	%al
	testl	%r13d, %r13d
	cmovnel	%r13d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_Z16ReadStream_FALSEP19ISequentialInStreamPvm, .Lfunc_end1-_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	.cfi_endproc

	.globl	_Z15ReadStream_FAILP19ISequentialInStreamPvm
	.p2align	4, 0x90
	.type	_Z15ReadStream_FAILP19ISequentialInStreamPvm,@function
_Z15ReadStream_FAILP19ISequentialInStreamPvm: # @_Z15ReadStream_FAILP19ISequentialInStreamPvm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	xorl	%r13d, %r13d
	leaq	4(%rsp), %r12
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB2_1:                                # %.thread.i
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbp, %rbp
	je	.LBB2_4
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	testq	$-2147483648, %rbp      # imm = 0x80000000
	movl	$-2147483648, %edx      # imm = 0x80000000
	cmovel	%ebp, %edx
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rcx
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB2_5
# BB#3:                                 #   in Loop: Header=BB2_1 Depth=1
	movl	4(%rsp), %eax
	addq	%rax, %r13
	subq	%rax, %rbp
	addq	%rax, %rbx
	testl	%eax, %eax
	jne	.LBB2_1
.LBB2_4:
	xorl	%ecx, %ecx
	cmpq	%r14, %r13
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovel	%ecx, %eax
.LBB2_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_Z15ReadStream_FAILP19ISequentialInStreamPvm, .Lfunc_end2-_Z15ReadStream_FAILP19ISequentialInStreamPvm
	.cfi_endproc

	.globl	_Z11WriteStreamP20ISequentialOutStreamPKvm
	.p2align	4, 0x90
	.type	_Z11WriteStreamP20ISequentialOutStreamPKvm,@function
_Z11WriteStreamP20ISequentialOutStreamPKvm: # @_Z11WriteStreamP20ISequentialOutStreamPKvm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 48
.Lcfi44:
	.cfi_offset %rbx, -40
.Lcfi45:
	.cfi_offset %r12, -32
.Lcfi46:
	.cfi_offset %r14, -24
.Lcfi47:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	leaq	4(%rsp), %r12
	.p2align	4, 0x90
.LBB3_1:                                # %.thread
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	je	.LBB3_2
# BB#3:                                 #   in Loop: Header=BB3_1 Depth=1
	testq	$-2147483648, %rbx      # imm = 0x80000000
	movl	$-2147483648, %edx      # imm = 0x80000000
	cmovel	%ebx, %edx
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r12, %rcx
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB3_6
# BB#4:                                 #   in Loop: Header=BB3_1 Depth=1
	movl	4(%rsp), %eax
	subq	%rax, %rbx
	addq	%rax, %r14
	testl	%eax, %eax
	jne	.LBB3_1
# BB#5:
	movl	$-2147467259, %eax      # imm = 0x80004005
	jmp	.LBB3_6
.LBB3_2:
	xorl	%eax, %eax
.LBB3_6:                                # %.thread24
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_Z11WriteStreamP20ISequentialOutStreamPKvm, .Lfunc_end3-_Z11WriteStreamP20ISequentialOutStreamPKvm
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
