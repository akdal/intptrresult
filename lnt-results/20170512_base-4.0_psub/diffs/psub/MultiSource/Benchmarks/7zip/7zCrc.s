	.text
	.file	"7zCrc.bc"
	.globl	CrcUpdate
	.p2align	4, 0x90
	.type	CrcUpdate,@function
CrcUpdate:                              # @CrcUpdate
	.cfi_startproc
# BB#0:
	movl	$g_CrcTable, %ecx
	jmp	CrcUpdateT4             # TAILCALL
.Lfunc_end0:
	.size	CrcUpdate, .Lfunc_end0-CrcUpdate
	.cfi_endproc

	.globl	CrcCalc
	.p2align	4, 0x90
	.type	CrcCalc,@function
CrcCalc:                                # @CrcCalc
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	%rsi, %rax
	movq	%rdi, %rdx
	movl	$-1, %edi
	movl	$g_CrcTable, %ecx
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	CrcUpdateT4
	notl	%eax
	popq	%rcx
	retq
.Lfunc_end1:
	.size	CrcCalc, .Lfunc_end1-CrcCalc
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI2_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI2_2:
	.long	3988292384              # 0xedb88320
	.long	3988292384              # 0xedb88320
	.long	3988292384              # 0xedb88320
	.long	3988292384              # 0xedb88320
.LCPI2_3:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.text
	.globl	CrcGenerateTable
	.p2align	4, 0x90
	.type	CrcGenerateTable,@function
CrcGenerateTable:                       # @CrcGenerateTable
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movdqa	.LCPI2_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movq	$-1024, %rax            # imm = 0xFC00
	movdqa	.LCPI2_1(%rip), %xmm1   # xmm1 = [1,1,1,1]
	movdqa	.LCPI2_2(%rip), %xmm2   # xmm2 = [3988292384,3988292384,3988292384,3988292384]
	movdqa	.LCPI2_3(%rip), %xmm3   # xmm3 = [4,4,4,4]
	.p2align	4, 0x90
.LBB2_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	psrld	$1, %xmm4
	movdqa	%xmm0, %xmm5
	pand	%xmm1, %xmm5
	pxor	%xmm6, %xmm6
	psubd	%xmm5, %xmm6
	pand	%xmm2, %xmm6
	pxor	%xmm4, %xmm6
	psrld	$1, %xmm6
	pand	%xmm1, %xmm4
	pxor	%xmm5, %xmm5
	psubd	%xmm4, %xmm5
	pand	%xmm2, %xmm5
	pxor	%xmm6, %xmm5
	psrld	$1, %xmm5
	pand	%xmm1, %xmm6
	pxor	%xmm4, %xmm4
	psubd	%xmm6, %xmm4
	pand	%xmm2, %xmm4
	pxor	%xmm5, %xmm4
	psrld	$1, %xmm4
	pand	%xmm1, %xmm5
	pxor	%xmm6, %xmm6
	psubd	%xmm5, %xmm6
	pand	%xmm2, %xmm6
	pxor	%xmm4, %xmm6
	psrld	$1, %xmm6
	pand	%xmm1, %xmm4
	pxor	%xmm5, %xmm5
	psubd	%xmm4, %xmm5
	pand	%xmm2, %xmm5
	pxor	%xmm6, %xmm5
	psrld	$1, %xmm5
	pand	%xmm1, %xmm6
	pxor	%xmm4, %xmm4
	psubd	%xmm6, %xmm4
	pand	%xmm2, %xmm4
	pxor	%xmm5, %xmm4
	psrld	$1, %xmm4
	pand	%xmm1, %xmm5
	pxor	%xmm6, %xmm6
	psubd	%xmm5, %xmm6
	pand	%xmm2, %xmm6
	pxor	%xmm4, %xmm6
	psrld	$1, %xmm6
	pand	%xmm1, %xmm4
	pxor	%xmm5, %xmm5
	psubd	%xmm4, %xmm5
	pand	%xmm2, %xmm5
	pxor	%xmm6, %xmm5
	movdqa	%xmm5, g_CrcTable+1024(%rax)
	paddd	%xmm3, %xmm0
	addq	$16, %rax
	jne	.LBB2_1
# BB#2:                                 # %.lr.ph.preheader
	movq	$-7168, %rax            # imm = 0xE400
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	g_CrcTable+7168(%rax), %ecx
	movzbl	%cl, %edx
	shrl	$8, %ecx
	xorl	g_CrcTable(,%rdx,4), %ecx
	movl	%ecx, g_CrcTable+8192(%rax)
	movl	g_CrcTable+7172(%rax), %ecx
	movzbl	%cl, %edx
	shrl	$8, %ecx
	xorl	g_CrcTable(,%rdx,4), %ecx
	movl	%ecx, g_CrcTable+8196(%rax)
	addq	$8, %rax
	jne	.LBB2_3
# BB#4:                                 # %._crit_edge
	retq
.Lfunc_end2:
	.size	CrcGenerateTable, .Lfunc_end2-CrcGenerateTable
	.cfi_endproc

	.type	g_CrcTable,@object      # @g_CrcTable
	.comm	g_CrcTable,8192,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
