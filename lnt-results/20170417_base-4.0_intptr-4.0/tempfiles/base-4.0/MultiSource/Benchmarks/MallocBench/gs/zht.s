	.text
	.file	"zht.bc"
	.globl	zsetscreen
	.p2align	4, 0x90
	.type	zsetscreen,@function
zsetscreen:                             # @zsetscreen
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 80
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	-16(%rbx), %r14
	leaq	8(%rsp), %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	callq	num_params
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB0_16
# BB#1:
	movw	8(%rbx), %ax
	movl	%eax, %ecx
	shrb	$2, %cl
	cmpb	$10, %cl
	je	.LBB0_3
# BB#2:
	movl	$-20, %ebp
	testb	%cl, %cl
	jne	.LBB0_16
.LBB0_3:
	andl	$3, %eax
	movl	$-7, %ebp
	cmpl	$3, %eax
	jne	.LBB0_16
# BB#4:
	movl	gs_screen_enum_sizeof(%rip), %esi
	movl	$1, %edi
	movl	$.L.str, %edx
	callq	alloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_5
# BB#6:
	movq	igs(%rip), %rsi
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movq	%r15, %rdi
	callq	gs_screen_init
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB0_7
# BB#8:
	movq	esp(%rip), %rax
	leaq	48(%rax), %rcx
	movl	$-5, %ebp
	cmpq	estop(%rip), %rcx
	ja	.LBB0_16
# BB#9:
	movw	$0, 16(%rax)
	movw	$33, 24(%rax)
	leaq	32(%rax), %rdx
	movq	%rdx, esp(%rip)
	movups	(%rbx), %xmm0
	movups	%xmm0, 32(%rax)
	movq	%rcx, esp(%rip)
	movq	%r15, 48(%rax)
	movw	$52, 56(%rax)
	movzwl	gs_screen_enum_sizeof(%rip), %ecx
	movw	%cx, 58(%rax)
	addq	$-48, osp(%rip)
	leaq	-48(%rbx), %r15
	movq	48(%rax), %rdi
	movq	%rsp, %rsi
	callq	gs_screen_currentpoint
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB0_16
# BB#10:
	je	.LBB0_12
# BB#11:
	movq	esp(%rip), %rax
	movups	-16(%rax), %xmm0
	movups	%xmm0, istate+8(%rip)
	addq	$-48, %rax
	movq	%rax, esp(%rip)
	jmp	.LBB0_15
.LBB0_5:
	movl	$-25, %ebp
	jmp	.LBB0_16
.LBB0_7:
	movl	gs_screen_enum_sizeof(%rip), %edx
	movl	$1, %esi
	movl	$.L.str, %ecx
	movq	%r15, %rdi
	callq	alloc_free
	jmp	.LBB0_16
.LBB0_12:
	movq	%r14, osp(%rip)
	cmpq	ostop(%rip), %r14
	jbe	.LBB0_14
# BB#13:
	movq	%r15, osp(%rip)
	movl	$-16, %ebp
	jmp	.LBB0_16
.LBB0_14:
	movl	(%rsp), %eax
	movl	%eax, -32(%rbx)
	movw	$44, -24(%rbx)
	movl	4(%rsp), %eax
	movl	%eax, -16(%rbx)
	movw	$44, -8(%rbx)
	movq	esp(%rip), %rax
	movups	-16(%rax), %xmm0
	movaps	%xmm0, 16(%rsp)
	movq	$set_screen_continue, 16(%rax)
	movw	$37, 24(%rax)
	movw	$0, 26(%rax)
	leaq	32(%rax), %rcx
	movq	%rcx, esp(%rip)
	movaps	16(%rsp), %xmm0
	movups	%xmm0, 32(%rax)
.LBB0_15:                               # %screen_sample.exit
	movl	$1, %ebp
.LBB0_16:
	movl	%ebp, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	zsetscreen, .Lfunc_end0-zsetscreen
	.cfi_endproc

	.globl	screen_sample
	.p2align	4, 0x90
	.type	screen_sample,@function
screen_sample:                          # @screen_sample
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 48
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	esp(%rip), %rax
	movq	(%rax), %rdi
	leaq	8(%rsp), %rsi
	callq	gs_screen_currentpoint
	testl	%eax, %eax
	js	.LBB1_7
# BB#1:
	je	.LBB1_3
# BB#2:
	movq	esp(%rip), %rax
	movups	-16(%rax), %xmm0
	movups	%xmm0, istate+8(%rip)
	addq	$-48, %rax
	movq	%rax, esp(%rip)
	jmp	.LBB1_6
.LBB1_3:
	leaq	32(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB1_5
# BB#4:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB1_7
.LBB1_5:
	movl	8(%rsp), %eax
	movl	%eax, 16(%rbx)
	movw	$44, 24(%rbx)
	movl	12(%rsp), %eax
	movl	%eax, 32(%rbx)
	movw	$44, 40(%rbx)
	movq	esp(%rip), %rax
	movups	-16(%rax), %xmm0
	movaps	%xmm0, 16(%rsp)
	movq	$set_screen_continue, 16(%rax)
	movw	$37, 24(%rax)
	movw	$0, 26(%rax)
	leaq	32(%rax), %rcx
	movq	%rcx, esp(%rip)
	movaps	16(%rsp), %xmm0
	movups	%xmm0, 32(%rax)
.LBB1_6:
	movl	$1, %eax
.LBB1_7:
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end1:
	.size	screen_sample, .Lfunc_end1-screen_sample
	.cfi_endproc

	.globl	set_screen_continue
	.p2align	4, 0x90
	.type	set_screen_continue,@function
set_screen_continue:                    # @set_screen_continue
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 64
.Lcfi15:
	.cfi_offset %rbx, -24
.Lcfi16:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	4(%rsp), %rdx
	movl	$1, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB2_9
# BB#1:
	movq	esp(%rip), %rax
	movq	(%rax), %rdi
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	callq	gs_screen_next
	testl	%eax, %eax
	js	.LBB2_9
# BB#2:
	addq	$-16, osp(%rip)
	leaq	-16(%rbx), %r14
	movq	esp(%rip), %rax
	movq	(%rax), %rdi
	leaq	8(%rsp), %rsi
	callq	gs_screen_currentpoint
	testl	%eax, %eax
	js	.LBB2_9
# BB#3:
	je	.LBB2_5
# BB#4:
	movq	esp(%rip), %rax
	movups	-16(%rax), %xmm0
	movups	%xmm0, istate+8(%rip)
	addq	$-48, %rax
	movq	%rax, esp(%rip)
	jmp	.LBB2_8
.LBB2_5:
	leaq	16(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB2_7
# BB#6:
	movq	%r14, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB2_9
.LBB2_7:
	movl	8(%rsp), %eax
	movl	%eax, (%rbx)
	movw	$44, 8(%rbx)
	movl	12(%rsp), %eax
	movl	%eax, 16(%rbx)
	movw	$44, 24(%rbx)
	movq	esp(%rip), %rax
	movups	-16(%rax), %xmm0
	movaps	%xmm0, 16(%rsp)
	movq	$set_screen_continue, 16(%rax)
	movw	$37, 24(%rax)
	movw	$0, 26(%rax)
	leaq	32(%rax), %rcx
	movq	%rcx, esp(%rip)
	movaps	16(%rsp), %xmm0
	movups	%xmm0, 32(%rax)
.LBB2_8:                                # %screen_sample.exit
	movl	$1, %eax
.LBB2_9:
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	set_screen_continue, .Lfunc_end2-set_screen_continue
	.cfi_endproc

	.globl	zht_op_init
	.p2align	4, 0x90
	.type	zht_op_init,@function
zht_op_init:                            # @zht_op_init
	.cfi_startproc
# BB#0:
	movl	$zht_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end3:
	.size	zht_op_init, .Lfunc_end3-zht_op_init
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"setscreen"
	.size	.L.str, 10

	.type	zht_op_init.my_defs,@object # @zht_op_init.my_defs
	.data
	.p2align	4
zht_op_init.my_defs:
	.quad	.L.str.1
	.quad	zsetscreen
	.zero	16
	.size	zht_op_init.my_defs, 32

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"3setscreen"
	.size	.L.str.1, 11


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
