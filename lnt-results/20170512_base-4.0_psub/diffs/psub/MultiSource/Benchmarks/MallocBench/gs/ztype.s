	.text
	.file	"ztype.bc"
	.globl	ztypenumber
	.p2align	4, 0x90
	.type	ztypenumber,@function
ztypenumber:                            # @ztypenumber
	.cfi_startproc
# BB#0:
	movl	$-17, %eax
	cmpq	%rdi, osp_nargs(%rip)
	ja	.LBB0_2
# BB#1:
	movzwl	8(%rdi), %eax
	andl	$252, %eax
	movq	%rax, %rcx
	shrq	$2, %rcx
	cmpl	$60, %eax
	movl	$9, %eax
	cmovbeq	%rcx, %rax
	movq	%rax, (%rdi)
	movw	$20, 8(%rdi)
	xorl	%eax, %eax
.LBB0_2:
	retq
.Lfunc_end0:
	.size	ztypenumber, .Lfunc_end0-ztypenumber
	.cfi_endproc

	.globl	zcvlit
	.p2align	4, 0x90
	.type	zcvlit,@function
zcvlit:                                 # @zcvlit
	.cfi_startproc
# BB#0:
	movl	$-17, %eax
	cmpq	%rdi, osp_nargs(%rip)
	ja	.LBB1_4
# BB#1:
	movzwl	8(%rdi), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	cmpl	$8, %ecx
	jne	.LBB1_3
# BB#2:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	callq	dict_access_ref
	movq	%rax, %rdi
	movw	8(%rdi), %ax
	addq	$8, %rsp
.LBB1_3:
	andl	$65534, %eax            # imm = 0xFFFE
	movw	%ax, 8(%rdi)
	xorl	%eax, %eax
.LBB1_4:
	retq
.Lfunc_end1:
	.size	zcvlit, .Lfunc_end1-zcvlit
	.cfi_endproc

	.globl	zcvx
	.p2align	4, 0x90
	.type	zcvx,@function
zcvx:                                   # @zcvx
	.cfi_startproc
# BB#0:
	movl	$-17, %eax
	cmpq	%rdi, osp_nargs(%rip)
	ja	.LBB2_4
# BB#1:
	movzwl	8(%rdi), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	cmpl	$8, %ecx
	jne	.LBB2_3
# BB#2:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	callq	dict_access_ref
	movq	%rax, %rdi
	movw	8(%rdi), %ax
	addq	$8, %rsp
.LBB2_3:
	orl	$1, %eax
	movw	%ax, 8(%rdi)
	xorl	%eax, %eax
.LBB2_4:
	retq
.Lfunc_end2:
	.size	zcvx, .Lfunc_end2-zcvx
	.cfi_endproc

	.globl	zxcheck
	.p2align	4, 0x90
	.type	zxcheck,@function
zxcheck:                                # @zxcheck
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$-17, %eax
	cmpq	%rbx, osp_nargs(%rip)
	ja	.LBB3_4
# BB#1:
	movzwl	8(%rbx), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	cmpl	$8, %ecx
	jne	.LBB3_3
# BB#2:
	movq	%rbx, %rdi
	callq	dict_access_ref
	movw	8(%rax), %ax
.LBB3_3:
	andl	$1, %eax
	movw	%ax, (%rbx)
	movw	$4, 8(%rbx)
	xorl	%eax, %eax
.LBB3_4:
	popq	%rbx
	retq
.Lfunc_end3:
	.size	zxcheck, .Lfunc_end3-zxcheck
	.cfi_endproc

	.globl	zexecuteonly
	.p2align	4, 0x90
	.type	zexecuteonly,@function
zexecuteonly:                           # @zexecuteonly
	.cfi_startproc
# BB#0:
	movl	$-17, %eax
	cmpq	%rdi, osp_nargs(%rip)
	ja	.LBB4_8
# BB#1:
	movzwl	8(%rdi), %esi
	movl	%esi, %edx
	andl	$252, %edx
	movl	$-20, %eax
	cmpl	$8, %edx
	je	.LBB4_8
# BB#2:
	movl	%esi, %edx
	andl	$65532, %edx            # imm = 0xFFFC
	shrl	$2, %edx
	movl	%edx, %ecx
	andb	$63, %cl
	cmpb	$13, %cl
	ja	.LBB4_8
# BB#3:
	andl	$63, %edx
	movl	$9225, %ecx             # imm = 0x2409
	btq	%rdx, %rcx
	jb	.LBB4_6
# BB#4:
	cmpq	$2, %rdx
	jne	.LBB4_8
# BB#5:
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 16
	callq	dict_access_ref
	movq	%rax, %rdi
	movw	8(%rdi), %si
	addq	$8, %rsp
.LBB4_6:
	movl	$-7, %eax
	testb	$2, %sil
	je	.LBB4_8
# BB#7:
	andl	$-771, %esi             # imm = 0xFCFD
	orl	$2, %esi
	movw	%si, 8(%rdi)
	xorl	%eax, %eax
.LBB4_8:                                # %access_check.exit
	retq
.Lfunc_end4:
	.size	zexecuteonly, .Lfunc_end4-zexecuteonly
	.cfi_endproc

	.globl	access_check
	.p2align	4, 0x90
	.type	access_check,@function
access_check:                           # @access_check
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %r14d
	movzwl	8(%rdi), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	movl	%edx, %ebx
	andb	$63, %bl
	movl	$-20, %eax
	cmpb	$13, %bl
	ja	.LBB5_8
# BB#1:
	andl	$63, %edx
	movl	$9225, %esi             # imm = 0x2409
	btq	%rdx, %rsi
	jb	.LBB5_4
# BB#2:
	cmpq	$2, %rdx
	jne	.LBB5_8
# BB#3:
	callq	dict_access_ref
	movq	%rax, %rdi
	movw	8(%rdi), %cx
.LBB5_4:
	testl	%ebp, %ebp
	movzwl	%cx, %ecx
	je	.LBB5_7
# BB#5:
	movl	%ecx, %edx
	notl	%edx
	movl	$-7, %eax
	testl	%r14d, %edx
	jne	.LBB5_8
# BB#6:
	andl	$64765, %ecx            # imm = 0xFCFD
	orl	%r14d, %ecx
	movw	%cx, 8(%rdi)
	xorl	%eax, %eax
	jmp	.LBB5_8
.LBB5_7:
	andl	%r14d, %ecx
	xorl	%eax, %eax
	cmpl	%r14d, %ecx
	sete	%al
.LBB5_8:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end5:
	.size	access_check, .Lfunc_end5-access_check
	.cfi_endproc

	.globl	znoaccess
	.p2align	4, 0x90
	.type	znoaccess,@function
znoaccess:                              # @znoaccess
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %esi
	movl	%esi, %edx
	shrl	$2, %edx
	movl	%edx, %ecx
	andb	$63, %cl
	movl	$-20, %eax
	cmpb	$13, %cl
	ja	.LBB6_5
# BB#1:
	andl	$63, %edx
	movl	$9225, %ecx             # imm = 0x2409
	btq	%rdx, %rcx
	jb	.LBB6_4
# BB#2:
	cmpq	$2, %rdx
	jne	.LBB6_5
# BB#3:
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 16
	callq	dict_access_ref
	movq	%rax, %rdi
	movw	8(%rdi), %si
	addq	$8, %rsp
.LBB6_4:
	andl	$64765, %esi            # imm = 0xFCFD
	movw	%si, 8(%rdi)
	xorl	%eax, %eax
.LBB6_5:                                # %access_check.exit
	retq
.Lfunc_end6:
	.size	znoaccess, .Lfunc_end6-znoaccess
	.cfi_endproc

	.globl	zreadonly
	.p2align	4, 0x90
	.type	zreadonly,@function
zreadonly:                              # @zreadonly
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %esi
	movl	%esi, %edx
	shrl	$2, %edx
	movl	%edx, %ecx
	andb	$63, %cl
	movl	$-20, %eax
	cmpb	$13, %cl
	ja	.LBB7_6
# BB#1:
	andl	$63, %edx
	movl	$9225, %ecx             # imm = 0x2409
	btq	%rdx, %rcx
	jb	.LBB7_4
# BB#2:
	cmpq	$2, %rdx
	jne	.LBB7_6
# BB#3:
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 16
	callq	dict_access_ref
	movq	%rax, %rdi
	movw	8(%rdi), %si
	addq	$8, %rsp
.LBB7_4:
	movl	%esi, %ecx
	andl	$514, %ecx              # imm = 0x202
	movl	$-7, %eax
	cmpl	$514, %ecx              # imm = 0x202
	jne	.LBB7_6
# BB#5:
	andl	$-771, %esi             # imm = 0xFCFD
	orl	$514, %esi              # imm = 0x202
	movw	%si, 8(%rdi)
	xorl	%eax, %eax
.LBB7_6:                                # %access_check.exit
	retq
.Lfunc_end7:
	.size	zreadonly, .Lfunc_end7-zreadonly
	.cfi_endproc

	.globl	zrcheck
	.p2align	4, 0x90
	.type	zrcheck,@function
zrcheck:                                # @zrcheck
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movzwl	8(%r14), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	movl	%edx, %ebx
	andb	$63, %bl
	movl	$-20, %eax
	cmpb	$13, %bl
	ja	.LBB8_5
# BB#1:
	andl	$63, %edx
	movl	$9225, %esi             # imm = 0x2409
	btq	%rdx, %rsi
	jb	.LBB8_4
# BB#2:
	cmpq	$2, %rdx
	jne	.LBB8_5
# BB#3:
	movq	%r14, %rdi
	callq	dict_access_ref
	movw	8(%rax), %cx
.LBB8_4:
	shrl	$9, %ecx
	andl	$1, %ecx
	movw	%cx, (%r14)
	movw	$4, 8(%r14)
	movl	%ecx, %eax
.LBB8_5:                                # %access_check.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	zrcheck, .Lfunc_end8-zrcheck
	.cfi_endproc

	.globl	zwcheck
	.p2align	4, 0x90
	.type	zwcheck,@function
zwcheck:                                # @zwcheck
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movzwl	8(%r14), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	movl	%edx, %ebx
	andb	$63, %bl
	movl	$-20, %eax
	cmpb	$13, %bl
	ja	.LBB9_5
# BB#1:
	andl	$63, %edx
	movl	$9225, %esi             # imm = 0x2409
	btq	%rdx, %rsi
	jb	.LBB9_4
# BB#2:
	cmpq	$2, %rdx
	jne	.LBB9_5
# BB#3:
	movq	%r14, %rdi
	callq	dict_access_ref
	movw	8(%rax), %cx
.LBB9_4:
	shrl	$8, %ecx
	andl	$1, %ecx
	movw	%cx, (%r14)
	movw	$4, 8(%r14)
	movl	%ecx, %eax
.LBB9_5:                                # %access_check.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	zwcheck, .Lfunc_end9-zwcheck
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_0:
	.long	1325400064              # float 2.14748365E+9
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI10_1:
	.quad	-4476578029604175872    # double -2147483649
	.text
	.globl	zcvi
	.p2align	4, 0x90
	.type	zcvi,@function
zcvi:                                   # @zcvi
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	subq	$136, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 160
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movw	8(%rbx), %cx
	movl	%ecx, %edx
	shrb	$2, %dl
	xorl	%eax, %eax
	cmpb	$5, %dl
	je	.LBB10_17
# BB#1:
	cmpb	$13, %dl
	je	.LBB10_5
# BB#2:
	cmpb	$11, %dl
	jne	.LBB10_4
# BB#3:
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
.LBB10_13:
	movl	$-15, %eax
	ucomiss	.LCPI10_0(%rip), %xmm0
	jae	.LBB10_17
# BB#14:
	cvtss2sd	%xmm0, %xmm1
	movsd	.LCPI10_1(%rip), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	jae	.LBB10_17
# BB#15:
	cvttss2si	%xmm0, %rax
	movq	%rax, (%rbx)
	movw	$20, 8(%rbx)
.LBB10_16:
	xorl	%eax, %eax
	jmp	.LBB10_17
.LBB10_5:
	movl	$-7, %eax
	testb	$2, %ch
	je	.LBB10_17
# BB#6:
	movq	(%rbx), %rsi
	movzwl	10(%rbx), %edx
	leaq	16(%rsp), %r14
	movq	%r14, %rdi
	callq	sread_string
	movq	%rsp, %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	scan_number
	testl	%eax, %eax
	jne	.LBB10_17
# BB#7:
	movq	16(%rsp), %rax
	cmpq	24(%rsp), %rax
	jae	.LBB10_9
# BB#8:                                 # %.thread
	incq	%rax
	movq	%rax, 16(%rsp)
	movl	$-18, %eax
	jmp	.LBB10_17
.LBB10_4:
	movl	$-20, %eax
.LBB10_17:
	addq	$136, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB10_9:
	leaq	16(%rsp), %rdi
	callq	*56(%rsp)
	movl	%eax, %ecx
	movl	$-18, %eax
	cmpl	$-1, %ecx
	jne	.LBB10_17
# BB#10:
	movzwl	8(%rsp), %eax
	andl	$252, %eax
	cmpl	$20, %eax
	jne	.LBB10_12
# BB#11:
	movups	(%rsp), %xmm0
	movups	%xmm0, (%rbx)
	jmp	.LBB10_16
.LBB10_12:
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB10_13
.Lfunc_end10:
	.size	zcvi, .Lfunc_end10-zcvi
	.cfi_endproc

	.globl	zcvn
	.p2align	4, 0x90
	.type	zcvn,@function
zcvn:                                   # @zcvn
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %edx
	movl	%edx, %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$52, %ecx
	jne	.LBB11_4
# BB#1:
	movl	$-7, %eax
	testb	$2, %dh
	je	.LBB11_4
# BB#2:
	movq	(%rbx), %rdi
	movzwl	10(%rbx), %esi
	movl	$1, %ecx
	movl	%edx, %ebp
	movq	%rbx, %rdx
	callq	name_ref
	movl	%ebp, %ecx
	testl	%eax, %eax
	jne	.LBB11_4
# BB#3:
	andl	$1, %ecx
	orw	%cx, 8(%rbx)
	xorl	%eax, %eax
.LBB11_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end11:
	.size	zcvn, .Lfunc_end11-zcvn
	.cfi_endproc

	.globl	zcvr
	.p2align	4, 0x90
	.type	zcvr,@function
zcvr:                                   # @zcvr
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 24
	subq	$136, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 160
.Lcfi36:
	.cfi_offset %rbx, -24
.Lcfi37:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movw	8(%rbx), %cx
	movl	%ecx, %edx
	shrb	$2, %dl
	cmpb	$13, %dl
	je	.LBB12_5
# BB#1:
	xorl	%eax, %eax
	cmpb	$11, %dl
	je	.LBB12_14
# BB#2:
	cmpb	$5, %dl
	jne	.LBB12_4
# BB#3:
	cvtsi2ssq	(%rbx), %xmm0
	movss	%xmm0, (%rbx)
	movw	$44, 8(%rbx)
	jmp	.LBB12_14
.LBB12_5:
	movl	$-7, %eax
	testb	$2, %ch
	je	.LBB12_14
# BB#6:
	movq	(%rbx), %rsi
	movzwl	10(%rbx), %edx
	leaq	16(%rsp), %r14
	movq	%r14, %rdi
	callq	sread_string
	movq	%rsp, %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	scan_number
	testl	%eax, %eax
	jne	.LBB12_14
# BB#7:
	movq	16(%rsp), %rax
	cmpq	24(%rsp), %rax
	jae	.LBB12_9
# BB#8:                                 # %.thread
	incq	%rax
	movq	%rax, 16(%rsp)
	movl	$-18, %eax
	jmp	.LBB12_14
.LBB12_4:
	movl	$-20, %eax
.LBB12_14:
	addq	$136, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB12_9:
	leaq	16(%rsp), %rdi
	callq	*56(%rsp)
	movl	%eax, %ecx
	movl	$-18, %eax
	cmpl	$-1, %ecx
	jne	.LBB12_14
# BB#10:
	movzwl	8(%rsp), %eax
	andl	$252, %eax
	cmpl	$44, %eax
	jne	.LBB12_12
# BB#11:
	movups	(%rsp), %xmm0
	movups	%xmm0, (%rbx)
	jmp	.LBB12_13
.LBB12_12:
	cvtsi2ssq	(%rsp), %xmm0
	movss	%xmm0, (%rbx)
	movw	$44, 8(%rbx)
.LBB12_13:
	xorl	%eax, %eax
	jmp	.LBB12_14
.Lfunc_end12:
	.size	zcvr, .Lfunc_end12-zcvr
	.cfi_endproc

	.globl	zcvrs
	.p2align	4, 0x90
	.type	zcvrs,@function
zcvrs:                                  # @zcvrs
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 80
.Lcfi43:
	.cfi_offset %rbx, -40
.Lcfi44:
	.cfi_offset %r12, -32
.Lcfi45:
	.cfi_offset %r14, -24
.Lcfi46:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movzwl	-8(%r12), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB13_13
# BB#1:
	movq	-16(%r12), %rdx
	leaq	-2(%rdx), %rcx
	cmpq	$34, %rcx
	jbe	.LBB13_3
# BB#2:
	movl	$-15, %eax
	jmp	.LBB13_13
.LBB13_3:
	movzwl	8(%r12), %ecx
	movl	%ecx, %esi
	andl	$252, %esi
	cmpl	$52, %esi
	jne	.LBB13_13
# BB#4:
	movl	$-7, %eax
	testb	$1, %ch
	je	.LBB13_13
# BB#5:
	movzwl	-24(%r12), %eax
	andl	$252, %eax
	cmpl	$20, %eax
	movl	$-20, %eax
	jne	.LBB13_13
# BB#6:
	leaq	31(%rsp), %r15
	leaq	-32(%r12), %r14
	movq	(%r14), %r8
	movq	%r8, %rcx
	negq	%rcx
	cmovlq	%r8, %rcx
	movslq	%edx, %rdi
	leaq	30(%rsp), %rsi
	movl	$48, %r9d
	.p2align	4, 0x90
.LBB13_7:                               # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rdi
	cmpl	$10, %edx
	movl	$55, %ebx
	cmovll	%r9d, %ebx
	addl	%edx, %ebx
	movb	%bl, (%rsi)
	decq	%rsi
	cmpq	%rcx, %rdi
	movq	%rax, %rcx
	jbe	.LBB13_7
# BB#8:
	testq	%r8, %r8
	js	.LBB13_10
# BB#9:
	incq	%rsi
	jmp	.LBB13_11
.LBB13_10:
	movb	$45, (%rsi)
.LBB13_11:
	subq	%rsi, %r15
	movzwl	10(%r12), %eax
	cmpq	%rax, %r15
	movl	$-15, %eax
	jg	.LBB13_13
# BB#12:
	movq	(%r12), %rdi
	movl	%r15d, %edx
	callq	memcpy
	movw	%r15w, 10(%r12)
	orb	$-128, 9(%r12)
	movups	(%r12), %xmm0
	movups	%xmm0, (%r14)
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
.LBB13_13:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	zcvrs, .Lfunc_end13-zcvrs
	.cfi_endproc

	.globl	zcvs
	.p2align	4, 0x90
	.type	zcvs,@function
zcvs:                                   # @zcvs
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 32
	subq	$80, %rsp
.Lcfi50:
	.cfi_def_cfa_offset 112
.Lcfi51:
	.cfi_offset %rbx, -32
.Lcfi52:
	.cfi_offset %r14, -24
.Lcfi53:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	movl	$-20, %eax
	cmpl	$52, %edx
	jne	.LBB14_24
# BB#1:
	movl	$-7, %eax
	testb	$1, %ch
	je	.LBB14_24
# BB#2:
	leaq	16(%rsp), %rcx
	movq	%rcx, (%rsp)
	movzwl	-8(%rbx), %ecx
	movl	%ecx, %esi
	andl	$252, %esi
	cmpl	$60, %esi
	movb	$9, %dl
	ja	.LBB14_4
# BB#3:
	shrl	$2, %esi
	movl	%esi, %edx
.LBB14_4:
	leaq	-16(%rbx), %r14
	decb	%dl
	cmpb	$12, %dl
	ja	.LBB14_19
# BB#5:
	movzbl	%dl, %edx
	jmpq	*.LJTI14_0(,%rdx,8)
.LBB14_6:
	cmpw	$0, (%r14)
	movl	$.L.str, %eax
	movl	$.L.str.1, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, (%rsp)
	jmp	.LBB14_21
.LBB14_19:
	movl	$-17, %eax
	cmpq	%rbx, osp_nargs(%rip)
	ja	.LBB14_24
# BB#20:
	movq	$.L.str.5, (%rsp)
	jmp	.LBB14_21
.LBB14_7:
	movq	(%r14), %rdx
	leaq	16(%rsp), %rdi
	movl	$.L.str.2, %esi
	jmp	.LBB14_8
.LBB14_9:
	movq	%rsp, %rsi
	movq	%r14, %rdi
	callq	name_string_ref
	jmp	.LBB14_22
.LBB14_10:
	movl	$dstack, %edi
	callq	dict_first
	leaq	48(%rsp), %rdx
	movl	$dstack, %edi
	movl	%eax, %esi
	callq	dict_next
	testl	%eax, %eax
	js	.LBB14_15
# BB#11:                                # %.lr.ph
	leaq	48(%rsp), %r15
	.p2align	4, 0x90
.LBB14_12:                              # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rcx
	cmpq	64(%rsp), %rcx
	jne	.LBB14_14
# BB#13:                                #   in Loop: Header=BB14_12 Depth=1
	movzwl	56(%rsp), %ecx
	andl	$252, %ecx
	cmpl	$28, %ecx
	je	.LBB14_25
.LBB14_14:                              # %.backedge
                                        #   in Loop: Header=BB14_12 Depth=1
	movl	$dstack, %edi
	movl	%eax, %esi
	movq	%r15, %rdx
	callq	dict_next
	testl	%eax, %eax
	jns	.LBB14_12
.LBB14_15:                              # %._crit_edge
	movq	(%r14), %rdx
	leaq	16(%rsp), %rdi
	movl	$.L.str.3, %esi
.LBB14_8:
	xorl	%eax, %eax
	callq	sprintf
	jmp	.LBB14_21
.LBB14_16:
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	leaq	16(%rsp), %rdi
	movl	$.L.str.4, %esi
	movb	$1, %al
	callq	sprintf
.LBB14_21:
	movq	(%rsp), %rdi
	callq	strlen
	movw	%ax, 10(%rsp)
.LBB14_22:
	movzwl	10(%rsp), %edx
	movl	$-15, %eax
	cmpw	10(%rbx), %dx
	ja	.LBB14_24
# BB#23:
	movq	(%rbx), %rdi
	movq	(%rsp), %rsi
	callq	memcpy
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
	movzwl	10(%rsp), %eax
	movw	%ax, -6(%rbx)
	orb	$-128, -7(%rbx)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB14_24:
	addq	$80, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB14_17:
	testb	$2, %ch
	je	.LBB14_24
# BB#18:
	movups	(%r14), %xmm0
	movaps	%xmm0, (%rsp)
	jmp	.LBB14_22
.LBB14_25:
	leaq	48(%rsp), %rdi
	movq	%rsp, %rsi
	callq	name_string_ref
	jmp	.LBB14_22
.Lfunc_end14:
	.size	zcvs, .Lfunc_end14-zcvs
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI14_0:
	.quad	.LBB14_6
	.quad	.LBB14_19
	.quad	.LBB14_19
	.quad	.LBB14_19
	.quad	.LBB14_7
	.quad	.LBB14_19
	.quad	.LBB14_9
	.quad	.LBB14_19
	.quad	.LBB14_10
	.quad	.LBB14_19
	.quad	.LBB14_16
	.quad	.LBB14_19
	.quad	.LBB14_17

	.text
	.globl	ztype_op_init
	.p2align	4, 0x90
	.type	ztype_op_init,@function
ztype_op_init:                          # @ztype_op_init
	.cfi_startproc
# BB#0:
	movl	$ztype_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end15:
	.size	ztype_op_init, .Lfunc_end15-ztype_op_init
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"true"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"false"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%ld"
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"operator %lx"
	.size	.L.str.3, 13

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%g"
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"--nostringval--"
	.size	.L.str.5, 16

	.type	ztype_op_init.my_defs,@object # @ztype_op_init.my_defs
	.data
	.p2align	4
ztype_op_init.my_defs:
	.quad	.L.str.6
	.quad	zcvi
	.quad	.L.str.7
	.quad	zcvlit
	.quad	.L.str.8
	.quad	zcvn
	.quad	.L.str.9
	.quad	zcvr
	.quad	.L.str.10
	.quad	zcvrs
	.quad	.L.str.11
	.quad	zcvs
	.quad	.L.str.12
	.quad	zcvx
	.quad	.L.str.13
	.quad	zexecuteonly
	.quad	.L.str.14
	.quad	znoaccess
	.quad	.L.str.15
	.quad	zrcheck
	.quad	.L.str.16
	.quad	zreadonly
	.quad	.L.str.17
	.quad	ztypenumber
	.quad	.L.str.18
	.quad	zwcheck
	.quad	.L.str.19
	.quad	zxcheck
	.zero	16
	.size	ztype_op_init.my_defs, 240

	.type	.L.str.6,@object        # @.str.6
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.6:
	.asciz	"1cvi"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"1cvlit"
	.size	.L.str.7, 7

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"1cvn"
	.size	.L.str.8, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"1cvr"
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"3cvrs"
	.size	.L.str.10, 6

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"2cvs"
	.size	.L.str.11, 5

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"1cvx"
	.size	.L.str.12, 5

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"1executeonly"
	.size	.L.str.13, 13

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"1noaccess"
	.size	.L.str.14, 10

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"1rcheck"
	.size	.L.str.15, 8

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"1readonly"
	.size	.L.str.16, 10

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"1.typenumber"
	.size	.L.str.17, 13

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"1wcheck"
	.size	.L.str.18, 8

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"1xcheck"
	.size	.L.str.19, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
