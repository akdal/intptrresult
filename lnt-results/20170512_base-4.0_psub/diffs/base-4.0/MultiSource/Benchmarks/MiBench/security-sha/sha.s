	.text
	.file	"sha.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1732584193              # 0x67452301
	.long	4023233417              # 0xefcdab89
	.long	2562383102              # 0x98badcfe
	.long	271733878               # 0x10325476
	.text
	.globl	sha_init
	.p2align	4, 0x90
	.type	sha_init,@function
sha_init:                               # @sha_init
	.cfi_startproc
# BB#0:
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1732584193,4023233417,2562383102,271733878]
	movups	%xmm0, (%rdi)
	movl	$3285377520, %eax       # imm = 0xC3D2E1F0
	movq	%rax, 16(%rdi)
	movl	$0, 24(%rdi)
	retq
.Lfunc_end0:
	.size	sha_init, .Lfunc_end0-sha_init
	.cfi_endproc

	.globl	sha_update
	.p2align	4, 0x90
	.type	sha_update,@function
sha_update:                             # @sha_update
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rdi, %r15
	leal	(,%rbx,8), %ecx
	addl	20(%r15), %ecx
	movl	24(%r15), %eax
	jae	.LBB1_2
# BB#1:
	incl	%eax
	movl	%eax, 24(%r15)
.LBB1_2:                                # %._crit_edge21
	movl	%ecx, 20(%r15)
	movl	%ebx, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	movl	%ecx, 24(%r15)
	leaq	28(%r15), %r12
	cmpl	$64, %ebx
	jl	.LBB1_6
# BB#3:                                 # %.lr.ph.preheader
	leal	-64(%rbx), %r13d
	movl	%r13d, %ebp
	andl	$-64, %ebp
	leaq	64(%rbp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	32(%r14), %xmm2
	movups	48(%r14), %xmm3
	movups	%xmm3, 48(%r12)
	movups	%xmm2, 32(%r12)
	movups	%xmm1, 16(%r12)
	movups	%xmm0, (%r12)
	movq	%r12, %rdi
	callq	byte_reverse
	movq	%r15, %rdi
	callq	sha_transform
	addq	$64, %r14
	addl	$-64, %ebx
	cmpl	$63, %ebx
	jg	.LBB1_4
# BB#5:                                 # %._crit_edge.loopexit
	movq	16(%rsp), %rsi          # 8-byte Reload
	addq	8(%rsp), %rsi           # 8-byte Folded Reload
	subl	%ebp, %r13d
	movl	%r13d, %ebx
.LBB1_6:                                # %._crit_edge
	movslq	%ebx, %rdx
	movq	%r12, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	memcpy                  # TAILCALL
.Lfunc_end1:
	.size	sha_update, .Lfunc_end1-sha_update
	.cfi_endproc

	.p2align	4, 0x90
	.type	byte_reverse,@function
byte_reverse:                           # @byte_reverse
	.cfi_startproc
# BB#0:
	movb	(%rdi), %sil
	movb	1(%rdi), %cl
	movb	2(%rdi), %dl
	movb	3(%rdi), %al
	movb	%al, (%rdi)
	movb	%dl, 1(%rdi)
	movb	%cl, 2(%rdi)
	movb	%sil, 3(%rdi)
	movb	4(%rdi), %sil
	movb	5(%rdi), %cl
	movb	6(%rdi), %dl
	movb	7(%rdi), %al
	movb	%al, 4(%rdi)
	movb	%dl, 5(%rdi)
	movb	%cl, 6(%rdi)
	movb	%sil, 7(%rdi)
	movb	8(%rdi), %sil
	movb	9(%rdi), %cl
	movb	10(%rdi), %dl
	movb	11(%rdi), %al
	movb	%al, 8(%rdi)
	movb	%dl, 9(%rdi)
	movb	%cl, 10(%rdi)
	movb	%sil, 11(%rdi)
	movb	12(%rdi), %sil
	movb	13(%rdi), %cl
	movb	14(%rdi), %dl
	movb	15(%rdi), %al
	movb	%al, 12(%rdi)
	movb	%dl, 13(%rdi)
	movb	%cl, 14(%rdi)
	movb	%sil, 15(%rdi)
	movb	16(%rdi), %sil
	movb	17(%rdi), %cl
	movb	18(%rdi), %dl
	movb	19(%rdi), %al
	movb	%al, 16(%rdi)
	movb	%dl, 17(%rdi)
	movb	%cl, 18(%rdi)
	movb	%sil, 19(%rdi)
	movb	20(%rdi), %sil
	movb	21(%rdi), %cl
	movb	22(%rdi), %dl
	movb	23(%rdi), %al
	movb	%al, 20(%rdi)
	movb	%dl, 21(%rdi)
	movb	%cl, 22(%rdi)
	movb	%sil, 23(%rdi)
	movb	24(%rdi), %sil
	movb	25(%rdi), %cl
	movb	26(%rdi), %dl
	movb	27(%rdi), %al
	movb	%al, 24(%rdi)
	movb	%dl, 25(%rdi)
	movb	%cl, 26(%rdi)
	movb	%sil, 27(%rdi)
	movb	28(%rdi), %sil
	movb	29(%rdi), %cl
	movb	30(%rdi), %dl
	movb	31(%rdi), %al
	movb	%al, 28(%rdi)
	movb	%dl, 29(%rdi)
	movb	%cl, 30(%rdi)
	movb	%sil, 31(%rdi)
	movb	32(%rdi), %sil
	movb	33(%rdi), %cl
	movb	34(%rdi), %dl
	movb	35(%rdi), %al
	movb	%al, 32(%rdi)
	movb	%dl, 33(%rdi)
	movb	%cl, 34(%rdi)
	movb	%sil, 35(%rdi)
	movb	36(%rdi), %sil
	movb	37(%rdi), %cl
	movb	38(%rdi), %dl
	movb	39(%rdi), %al
	movb	%al, 36(%rdi)
	movb	%dl, 37(%rdi)
	movb	%cl, 38(%rdi)
	movb	%sil, 39(%rdi)
	movb	40(%rdi), %sil
	movb	41(%rdi), %cl
	movb	42(%rdi), %dl
	movb	43(%rdi), %al
	movb	%al, 40(%rdi)
	movb	%dl, 41(%rdi)
	movb	%cl, 42(%rdi)
	movb	%sil, 43(%rdi)
	movb	44(%rdi), %sil
	movb	45(%rdi), %cl
	movb	46(%rdi), %dl
	movb	47(%rdi), %al
	movb	%al, 44(%rdi)
	movb	%dl, 45(%rdi)
	movb	%cl, 46(%rdi)
	movb	%sil, 47(%rdi)
	movb	48(%rdi), %sil
	movb	49(%rdi), %cl
	movb	50(%rdi), %dl
	movb	51(%rdi), %al
	movb	%al, 48(%rdi)
	movb	%dl, 49(%rdi)
	movb	%cl, 50(%rdi)
	movb	%sil, 51(%rdi)
	movb	52(%rdi), %sil
	movb	53(%rdi), %cl
	movb	54(%rdi), %dl
	movb	55(%rdi), %al
	movb	%al, 52(%rdi)
	movb	%dl, 53(%rdi)
	movb	%cl, 54(%rdi)
	movb	%sil, 55(%rdi)
	movb	56(%rdi), %sil
	movb	57(%rdi), %cl
	movb	58(%rdi), %dl
	movb	59(%rdi), %al
	movb	%al, 56(%rdi)
	movb	%dl, 57(%rdi)
	movb	%cl, 58(%rdi)
	movb	%sil, 59(%rdi)
	movb	60(%rdi), %sil
	movb	61(%rdi), %cl
	movb	62(%rdi), %dl
	movb	63(%rdi), %al
	movb	%al, 60(%rdi)
	movb	%dl, 61(%rdi)
	movb	%cl, 62(%rdi)
	movb	%sil, 63(%rdi)
	retq
.Lfunc_end2:
	.size	byte_reverse, .Lfunc_end2-byte_reverse
	.cfi_endproc

	.p2align	4, 0x90
	.type	sha_transform,@function
sha_transform:                          # @sha_transform
	.cfi_startproc
# BB#0:                                 # %.preheader124.preheader
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	subq	$200, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 224
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movups	28(%rdi), %xmm0
	movups	44(%rdi), %xmm1
	movups	60(%rdi), %xmm2
	movups	76(%rdi), %xmm3
	movaps	%xmm3, -80(%rsp)
	movaps	%xmm2, -96(%rsp)
	movaps	%xmm1, -112(%rsp)
	movaps	%xmm0, -128(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_1:                                # %.preheader124
                                        # =>This Inner Loop Header: Depth=1
	movl	-96(%rsp,%rax,4), %ecx
	xorl	-76(%rsp,%rax,4), %ecx
	xorl	-120(%rsp,%rax,4), %ecx
	xorl	-128(%rsp,%rax,4), %ecx
	movl	%ecx, -64(%rsp,%rax,4)
	movl	-92(%rsp,%rax,4), %ecx
	xorl	-72(%rsp,%rax,4), %ecx
	xorl	-116(%rsp,%rax,4), %ecx
	xorl	-124(%rsp,%rax,4), %ecx
	movl	%ecx, -60(%rsp,%rax,4)
	addq	$2, %rax
	cmpq	$64, %rax
	jne	.LBB3_1
# BB#2:
	movdqu	(%rdi), %xmm0
	movl	16(%rdi), %r8d
	movd	%xmm0, %edx
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	movd	%xmm1, %r11d
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	movd	%xmm1, %ecx
	pshufd	$231, %xmm0, %xmm1      # xmm1 = xmm0[3,1,2,3]
	movd	%xmm1, %ebp
	xorl	%ebx, %ebx
	movl	%r8d, %r10d
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	movl	%ecx, %eax
	movl	%ebp, %r9d
	roll	$5, %edx
	movl	%eax, %ebp
	andl	%r11d, %ebp
	movl	%r11d, %ecx
	notl	%r11d
	andl	%r9d, %r11d
	orl	%ebp, %r11d
	movl	-128(%rsp,%rbx,4), %ebp
	addl	%r10d, %edx
	addl	%r11d, %edx
	leal	1518500249(%rbp,%rdx), %edx
	roll	$30, %ecx
	incq	%rbx
	cmpq	$20, %rbx
	movl	%r9d, %r10d
	movl	%eax, %ebp
	movl	%esi, %r11d
	jne	.LBB3_3
# BB#4:                                 # %.preheader123.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_5:                                # %.preheader123
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %r10d
	movl	%ecx, %ebx
	movl	%eax, %r11d
	movl	%r10d, %eax
	roll	$5, %eax
	xorl	%esi, %ecx
	xorl	%r11d, %ecx
	movl	-48(%rsp,%rbp,4), %edx
	addl	%eax, %ecx
	addl	%r9d, %ecx
	leal	1859775393(%rdx,%rcx), %edx
	movl	%esi, %ecx
	roll	$30, %ecx
	incq	%rbp
	cmpq	$20, %rbp
	movl	%r11d, %r9d
	movl	%ebx, %eax
	movl	%r10d, %esi
	jne	.LBB3_5
# BB#6:                                 # %.preheader122.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_7:                                # %.preheader122
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %r9d
	movl	%ecx, %esi
	movl	%ebx, %ebp
	movl	%r9d, %ecx
	roll	$5, %ecx
	movl	%ebp, %edx
	orl	%esi, %edx
	andl	%r10d, %edx
	andl	%esi, %ebx
	orl	%edx, %ebx
	movl	32(%rsp,%rax,4), %edx
	addl	%r11d, %ecx
	addl	%ebx, %ecx
	leal	-1894007588(%rdx,%rcx), %edx
	movl	%r10d, %ecx
	roll	$30, %ecx
	incq	%rax
	cmpq	$20, %rax
	movl	%ebp, %r11d
	movl	%esi, %ebx
	movl	%r9d, %r10d
	jne	.LBB3_7
# BB#8:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_9:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %r11d
	movl	%ecx, %eax
	movl	%esi, %r10d
	movl	%r11d, %ecx
	roll	$5, %ecx
	movl	%eax, %edx
	xorl	%r9d, %edx
	xorl	%r10d, %edx
	movl	112(%rsp,%rbx,4), %esi
	addl	%ecx, %edx
	addl	%ebp, %edx
	leal	-899497514(%rsi,%rdx), %edx
	movl	%r9d, %ecx
	roll	$30, %ecx
	incq	%rbx
	cmpq	$20, %rbx
	movl	%r10d, %ebp
	movl	%eax, %esi
	movl	%r11d, %r9d
	jne	.LBB3_9
# BB#10:
	movd	%eax, %xmm1
	movd	%r11d, %xmm2
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movd	%ecx, %xmm1
	movd	%edx, %xmm3
	punpckldq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	paddd	%xmm3, %xmm0
	movdqu	%xmm0, (%rdi)
	addl	%r10d, %r8d
	movl	%r8d, 16(%rdi)
	addq	$200, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end3:
	.size	sha_transform, .Lfunc_end3-sha_transform
	.cfi_endproc

	.globl	sha_final
	.p2align	4, 0x90
	.type	sha_final,@function
sha_final:                              # @sha_final
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 48
.Lcfi23:
	.cfi_offset %rbx, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	20(%rbx), %ebp
	movl	24(%rbx), %r15d
	movl	%ebp, %edx
	shrl	$3, %edx
	andl	$63, %edx
	leaq	28(%rbx), %r14
	leal	1(%rdx), %eax
	movb	$-128, 28(%rbx,%rdx)
	leaq	29(%rbx,%rdx), %rdi
	cmpl	$57, %eax
	jb	.LBB4_2
# BB#1:
	xorl	$63, %edx
	xorl	%esi, %esi
	callq	memset
	movq	%r14, %rdi
	callq	byte_reverse
	movq	%rbx, %rdi
	callq	sha_transform
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r14)
	movups	%xmm0, 16(%r14)
	movups	%xmm0, (%r14)
	movq	$0, 48(%r14)
	jmp	.LBB4_3
.LBB4_2:
	movl	$55, %eax
	subl	%edx, %eax
	movslq	%eax, %rdx
	xorl	%esi, %esi
	callq	memset
.LBB4_3:
	movq	%r14, %rdi
	callq	byte_reverse
	movl	%r15d, 84(%rbx)
	movl	%ebp, 88(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sha_transform           # TAILCALL
.Lfunc_end4:
	.size	sha_final, .Lfunc_end4-sha_final
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	1732584193              # 0x67452301
	.long	4023233417              # 0xefcdab89
	.long	2562383102              # 0x98badcfe
	.long	271733878               # 0x10325476
	.text
	.globl	sha_stream
	.p2align	4, 0x90
	.type	sha_stream,@function
sha_stream:                             # @sha_stream
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$8216, %rsp             # imm = 0x2018
.Lcfi33:
	.cfi_def_cfa_offset 8272
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [1732584193,4023233417,2562383102,271733878]
	movups	%xmm0, (%r12)
	movl	$3285377520, %eax       # imm = 0xC3D2E1F0
	movq	%rax, 16(%r12)
	movl	$0, 24(%r12)
	leaq	16(%rsp), %r15
	movl	$1, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	fread
	movq	%rax, %rbx
	movl	20(%r12), %ebp
	testl	%ebx, %ebx
	jle	.LBB5_10
# BB#1:                                 # %.lr.ph
	leaq	28(%r12), %r13
	movq	%r14, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB5_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_7 Depth 2
	leal	(,%rbx,8), %ecx
	addl	%ebp, %ecx
	movl	24(%r12), %eax
	jae	.LBB5_4
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	incl	%eax
	movl	%eax, 24(%r12)
.LBB5_4:                                # %._crit_edge21.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	%ecx, 20(%r12)
	movl	%ebx, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	movl	%ecx, 24(%r12)
	cmpl	$64, %ebx
	jl	.LBB5_5
# BB#6:                                 # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB5_2 Depth=1
	leal	-64(%rbx), %ebp
	movl	%ebp, %r14d
	andl	$-64, %r14d
	.p2align	4, 0x90
.LBB5_7:                                # %.lr.ph.i
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r15), %xmm0
	movups	16(%r15), %xmm1
	movups	32(%r15), %xmm2
	movups	48(%r15), %xmm3
	movups	%xmm3, 48(%r13)
	movups	%xmm2, 32(%r13)
	movups	%xmm1, 16(%r13)
	movups	%xmm0, (%r13)
	movq	%r13, %rdi
	callq	byte_reverse
	movq	%r12, %rdi
	callq	sha_transform
	addq	$64, %r15
	addl	$-64, %ebx
	cmpl	$63, %ebx
	jg	.LBB5_7
# BB#8:                                 # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB5_2 Depth=1
	leaq	80(%rsp,%r14), %rsi
	subl	%r14d, %ebp
	movl	%ebp, %ebx
	movq	8(%rsp), %r14           # 8-byte Reload
	leaq	16(%rsp), %r15
	jmp	.LBB5_9
	.p2align	4, 0x90
.LBB5_5:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%r15, %rsi
.LBB5_9:                                # %sha_update.exit
                                        #   in Loop: Header=BB5_2 Depth=1
	movslq	%ebx, %rdx
	movq	%r13, %rdi
	callq	memcpy
	movl	$1, %esi
	movl	$8192, %edx             # imm = 0x2000
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	fread
	movq	%rax, %rbx
	testl	%ebx, %ebx
	movl	20(%r12), %ebp
	jg	.LBB5_2
.LBB5_10:                               # %._crit_edge
	movl	24(%r12), %r14d
	movl	%ebp, %edx
	shrl	$3, %edx
	andl	$63, %edx
	leaq	28(%r12), %rbx
	leal	1(%rdx), %eax
	movb	$-128, 28(%r12,%rdx)
	leaq	29(%r12,%rdx), %rdi
	cmpl	$57, %eax
	jb	.LBB5_12
# BB#11:
	xorl	$63, %edx
	xorl	%esi, %esi
	callq	memset
	movq	%rbx, %rdi
	callq	byte_reverse
	movq	%r12, %rdi
	callq	sha_transform
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 48(%rbx)
	jmp	.LBB5_13
.LBB5_12:
	movl	$55, %eax
	subl	%edx, %eax
	movslq	%eax, %rdx
	xorl	%esi, %esi
	callq	memset
.LBB5_13:                               # %sha_final.exit
	movq	%rbx, %rdi
	callq	byte_reverse
	movl	%r14d, 84(%r12)
	movl	%ebp, 88(%r12)
	movq	%r12, %rdi
	callq	sha_transform
	addq	$8216, %rsp             # imm = 0x2018
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	sha_stream, .Lfunc_end5-sha_stream
	.cfi_endproc

	.globl	sha_print
	.p2align	4, 0x90
	.type	sha_print,@function
sha_print:                              # @sha_print
	.cfi_startproc
# BB#0:
	movl	(%rdi), %esi
	movl	4(%rdi), %edx
	movl	8(%rdi), %ecx
	movl	12(%rdi), %r8d
	movl	16(%rdi), %r9d
	movl	$.L.str, %edi
	xorl	%eax, %eax
	jmp	printf                  # TAILCALL
.Lfunc_end6:
	.size	sha_print, .Lfunc_end6-sha_print
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%08x %08x %08x %08x %08x\n"
	.size	.L.str, 26


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
