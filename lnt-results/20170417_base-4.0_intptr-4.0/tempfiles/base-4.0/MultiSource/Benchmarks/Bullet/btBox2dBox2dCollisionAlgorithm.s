	.text
	.file	"btBox2dBox2dCollisionAlgorithm.bc"
	.globl	_ZN30btBox2dBox2dCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_
	.p2align	4, 0x90
	.type	_ZN30btBox2dBox2dCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_,@function
_ZN30btBox2dBox2dCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_: # @_ZN30btBox2dBox2dCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	%rdx, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	movq	$_ZTV30btBox2dBox2dCollisionAlgorithm+16, (%r12)
	movb	$0, 16(%r12)
	movq	%rbx, 24(%r12)
	testq	%rbx, %rbx
	jne	.LBB0_5
# BB#1:
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp0:
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*48(%rax)
.Ltmp1:
# BB#2:
	testb	%al, %al
	je	.LBB0_5
# BB#3:
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp2:
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*24(%rax)
.Ltmp3:
# BB#4:
	movq	%rax, 24(%r12)
	movb	$1, 16(%r12)
.LBB0_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB0_6:
.Ltmp4:
	movq	%rax, %r14
.Ltmp5:
	movq	%r12, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp6:
# BB#7:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_8:
.Ltmp7:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN30btBox2dBox2dCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_, .Lfunc_end0-_ZN30btBox2dBox2dCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	1                       #   On action: 1
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN30btBox2dBox2dCollisionAlgorithmD2Ev
	.p2align	4, 0x90
	.type	_ZN30btBox2dBox2dCollisionAlgorithmD2Ev,@function
_ZN30btBox2dBox2dCollisionAlgorithmD2Ev: # @_ZN30btBox2dBox2dCollisionAlgorithmD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV30btBox2dBox2dCollisionAlgorithm+16, (%rbx)
	cmpb	$0, 16(%rbx)
	je	.LBB2_3
# BB#1:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB2_3
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp8:
	callq	*32(%rax)
.Ltmp9:
.LBB2_3:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN30btActivatingCollisionAlgorithmD2Ev # TAILCALL
.LBB2_4:
.Ltmp10:
	movq	%rax, %r14
.Ltmp11:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp12:
# BB#5:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_6:
.Ltmp13:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN30btBox2dBox2dCollisionAlgorithmD2Ev, .Lfunc_end2-_ZN30btBox2dBox2dCollisionAlgorithmD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin1   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp11-.Ltmp9          #   Call between .Ltmp9 and .Ltmp11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	1                       #   On action: 1
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp12     #   Call between .Ltmp12 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN30btBox2dBox2dCollisionAlgorithmD0Ev
	.p2align	4, 0x90
	.type	_ZN30btBox2dBox2dCollisionAlgorithmD0Ev,@function
_ZN30btBox2dBox2dCollisionAlgorithmD0Ev: # @_ZN30btBox2dBox2dCollisionAlgorithmD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV30btBox2dBox2dCollisionAlgorithm+16, (%rbx)
	cmpb	$0, 16(%rbx)
	je	.LBB3_3
# BB#1:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB3_3
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp14:
	callq	*32(%rax)
.Ltmp15:
.LBB3_3:
.Ltmp20:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp21:
# BB#4:                                 # %_ZN30btBox2dBox2dCollisionAlgorithmD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_5:
.Ltmp16:
	movq	%rax, %r14
.Ltmp17:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp18:
	jmp	.LBB3_8
.LBB3_6:
.Ltmp19:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_7:
.Ltmp22:
	movq	%rax, %r14
.LBB3_8:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN30btBox2dBox2dCollisionAlgorithmD0Ev, .Lfunc_end3-_ZN30btBox2dBox2dCollisionAlgorithmD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin2   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin2   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin2   #     jumps to .Ltmp19
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp18     #   Call between .Ltmp18 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN30btBox2dBox2dCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN30btBox2dBox2dCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN30btBox2dBox2dCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN30btBox2dBox2dCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%r8, %r14
	movq	%rdx, %rax
	movq	%rsi, %rdx
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_7
# BB#1:
	movq	200(%rdx), %rsi
	movq	200(%rax), %rcx
	movq	%rdi, 8(%r14)
	addq	$8, %rdx
	addq	$8, %rax
	movq	%r14, %rdi
	movq	%rax, %r8
	callq	_Z17b2CollidePolygonsP16btManifoldResultPK12btBox2dShapeRK11btTransformS3_S6_
	cmpb	$0, 16(%rbx)
	je	.LBB4_7
# BB#2:
	movq	8(%r14), %rdi
	cmpl	$0, 728(%rdi)
	je	.LBB4_7
# BB#3:
	movq	712(%rdi), %rax
	cmpq	144(%r14), %rax
	je	.LBB4_6
# BB#4:
	leaq	80(%r14), %rsi
	addq	$16, %r14
	jmp	.LBB4_5
.LBB4_7:                                # %_ZN16btManifoldResult20refreshContactPointsEv.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_6:
	leaq	16(%r14), %rsi
	addq	$80, %r14
.LBB4_5:
	movq	%r14, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_ # TAILCALL
.Lfunc_end4:
	.size	_ZN30btBox2dBox2dCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end4-_ZN30btBox2dBox2dCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1065017672              # float 0.980000019
.LCPI5_1:
	.long	981668463               # float 0.00100000005
.LCPI5_2:
	.long	1566444395              # float 9.99999984E+17
.LCPI5_3:
	.long	1065353216              # float 1
.LCPI5_5:
	.long	2147483648              # float -0
.LCPI5_6:
	.long	0                       # float 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_4:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_Z17b2CollidePolygonsP16btManifoldResultPK12btBox2dShapeRK11btTransformS3_S6_
	.p2align	4, 0x90
	.type	_Z17b2CollidePolygonsP16btManifoldResultPK12btBox2dShapeRK11btTransformS3_S6_,@function
_Z17b2CollidePolygonsP16btManifoldResultPK12btBox2dShapeRK11btTransformS3_S6_: # @_Z17b2CollidePolygonsP16btManifoldResultPK12btBox2dShapeRK11btTransformS3_S6_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	subq	$392, %rsp              # imm = 0x188
.Lcfi30:
	.cfi_def_cfa_offset 448
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %rbx
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movl	$0, 108(%rsp)
	leaq	108(%rsp), %r13
	movq	%r13, %rdi
	callq	_ZL17FindMaxSeparationPiPK12btBox2dShapeRK11btTransformS2_S5_
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	ja	.LBB5_26
# BB#1:
	movaps	%xmm0, 256(%rsp)        # 16-byte Spill
	movq	%rbp, 312(%rsp)         # 8-byte Spill
	movl	$0, 104(%rsp)
	leaq	104(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%r12, %r8
	callq	_ZL17FindMaxSeparationPiPK12btBox2dShapeRK11btTransformS2_S5_
	ucomiss	.LCPI5_6, %xmm0
	ja	.LBB5_26
# BB#2:
	movaps	256(%rsp), %xmm1        # 16-byte Reload
	mulss	.LCPI5_0(%rip), %xmm1
	addss	.LCPI5_1(%rip), %xmm1
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	movaps	%xmm1, 256(%rsp)        # 16-byte Spill
	ucomiss	%xmm1, %xmm0
	movq	%r14, %rsi
	cmovaq	%r12, %rsi
	cmovaq	%r14, %r12
	movq	%rbx, %rcx
	cmovaq	%r15, %rcx
	movss	40(%r12), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	%xmm9, 40(%rsp)         # 4-byte Spill
	movss	36(%r12), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	%xmm10, 36(%rsp)        # 4-byte Spill
	movss	32(%r12), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	%xmm12, 12(%rsp)        # 4-byte Spill
	movss	24(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	%xmm5, 28(%rsp)         # 4-byte Spill
	movss	20(%r12), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movss	16(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, 4(%rsp)          # 4-byte Spill
	movss	8(%r12), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, 32(%rsp)         # 4-byte Spill
	movss	(%r12), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 24(%rsp)         # 4-byte Spill
	movss	4(%r12), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	%xmm4, 8(%rsp)          # 4-byte Spill
	movss	(%rsi), %xmm11          # xmm11 = mem[0],zero,zero,zero
	cmovaq	%rbp, %r13
	movslq	(%r13), %rdx
	cmovaq	%rbx, %r15
	movq	%rdx, %rax
	shlq	$4, %rax
	movss	144(%r15,%rax), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	148(%r15,%rax), %xmm1   # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	mulss	%xmm1, %xmm4
	addss	%xmm2, %xmm4
	movss	152(%r15,%rax), %xmm7   # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm3
	addss	%xmm4, %xmm3
	movaps	%xmm6, %xmm2
	mulss	%xmm0, %xmm2
	movaps	%xmm15, %xmm4
	mulss	%xmm1, %xmm4
	addss	%xmm2, %xmm4
	movaps	%xmm5, %xmm6
	mulss	%xmm7, %xmm6
	addss	%xmm4, %xmm6
	movss	16(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	mulss	%xmm10, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm9, %xmm7
	addss	%xmm1, %xmm7
	movaps	%xmm11, %xmm0
	mulss	%xmm3, %xmm0
	movaps	%xmm8, %xmm1
	mulss	%xmm6, %xmm1
	addss	%xmm0, %xmm1
	movss	32(%rsi), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm2
	mulss	%xmm7, %xmm2
	addss	%xmm1, %xmm2
	movss	4(%rsi), %xmm14         # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm3, %xmm0
	movss	20(%rsi), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm5
	mulss	%xmm6, %xmm5
	addss	%xmm0, %xmm5
	movss	36(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	mulss	%xmm7, %xmm1
	addss	%xmm5, %xmm1
	movss	8(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	movss	24(%rsi), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm6
	addss	%xmm3, %xmm6
	movss	40(%rsi), %xmm13        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm7
	addss	%xmm6, %xmm7
	movss	144(%rcx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	movss	148(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm6, %xmm3
	movss	152(%rcx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm6
	addss	%xmm3, %xmm6
	movss	160(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	movss	164(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	movss	168(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm3
	addss	%xmm4, %xmm3
	minss	.LCPI5_2(%rip), %xmm6
	xorl	%edi, %edi
	ucomiss	%xmm3, %xmm6
	minss	%xmm6, %xmm3
	movss	176(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	movss	180(%rcx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	addss	%xmm4, %xmm6
	movss	184(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm4
	addss	%xmm6, %xmm4
	seta	%dil
	ucomiss	%xmm4, %xmm3
	minss	%xmm3, %xmm4
	mulss	192(%rcx), %xmm2
	mulss	196(%rcx), %xmm1
	addss	%xmm2, %xmm1
	mulss	200(%rcx), %xmm7
	addss	%xmm1, %xmm7
	movl	$2, %ebp
	cmovbel	%edi, %ebp
	ucomiss	%xmm7, %xmm4
	unpcklps	%xmm8, %xmm11   # xmm11 = xmm11[0],xmm8[0],xmm11[1],xmm8[1]
	movaps	%xmm15, %xmm7
	unpcklps	%xmm10, %xmm14  # xmm14 = xmm14[0],xmm10[0],xmm14[1],xmm10[1]
	movl	$3, %ebx
	cmovbel	%ebp, %ebx
	leal	1(%rbx), %edi
	shlq	$4, %rbx
	movss	80(%rcx,%rbx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm2
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm11, %xmm1
	movss	84(%rcx,%rbx), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm3, %xmm4
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm14, %xmm3
	addps	%xmm1, %xmm3
	unpcklps	%xmm9, %xmm5    # xmm5 = xmm5[0],xmm9[0],xmm5[1],xmm9[1]
	movss	8(%rsp), %xmm10         # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	88(%rcx,%rbx), %xmm6    # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm1
	mulss	%xmm6, %xmm1
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm5, %xmm6
	addps	%xmm3, %xmm6
	addss	%xmm2, %xmm4
	addss	%xmm1, %xmm4
	movss	56(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm4
	xorps	%xmm3, %xmm3
	movss	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1,2,3]
	movsd	48(%rsi), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm2, %xmm6
	movss	56(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	%xmm4, 20(%rsp)         # 4-byte Spill
	movss	52(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	%xmm4, 16(%rsp)         # 4-byte Spill
	movss	48(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, 48(%rsp)         # 16-byte Spill
	movlps	%xmm6, 192(%rsp)
	movlps	%xmm3, 200(%rsp)
	xorl	%ebx, %ebx
	cmpl	$4, %edi
	cmovaeq	%rbx, %rdi
	shlq	$4, %rdi
	movss	80(%rcx,%rdi), %xmm3    # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm12
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm11, %xmm3
	movss	84(%rcx,%rdi), %xmm4    # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm14, %xmm4
	addps	%xmm3, %xmm4
	movss	88(%rcx,%rdi), %xmm3    # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm13
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	movss	4(%rsp), %xmm9          # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	addps	%xmm2, %xmm3
	addss	%xmm12, %xmm0
	leal	1(%rdx), %ecx
	addss	%xmm13, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm3, 212(%rsp)
	movlps	%xmm1, 220(%rsp)
	movsd	80(%r15,%rax), %xmm13   # xmm13 = mem[0],zero
	cmpl	$4, %ecx
	movslq	%ecx, %rcx
	cmovgeq	%rbx, %rcx
	shlq	$4, %rcx
	movsd	80(%r15,%rcx), %xmm1    # xmm1 = mem[0],zero
	movaps	%xmm1, %xmm0
	subss	%xmm13, %xmm0
	movaps	%xmm1, 352(%rsp)        # 16-byte Spill
	pshufd	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	pshufd	$229, %xmm13, %xmm12    # xmm12 = xmm13[1,1,2,3]
	movdqa	%xmm1, 336(%rsp)        # 16-byte Spill
	subss	%xmm12, %xmm1
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movaps	%xmm10, %xmm8
	movaps	%xmm10, %xmm3
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	movss	88(%r15,%rcx), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	88(%r15,%rax), %xmm5    # xmm5 = mem[0],zero,zero,zero
	movss	%xmm2, 100(%rsp)        # 4-byte Spill
	movaps	%xmm2, %xmm4
	subss	%xmm5, %xmm4
	movss	32(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm6
	addss	%xmm3, %xmm6
	movaps	%xmm9, %xmm2
	mulss	%xmm0, %xmm2
	movaps	%xmm7, %xmm3
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	movss	28(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm10
	addss	%xmm3, %xmm10
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	mulss	36(%rsp), %xmm1         # 4-byte Folded Reload
	addss	%xmm0, %xmm1
	mulss	40(%rsp), %xmm4         # 4-byte Folded Reload
	addss	%xmm1, %xmm4
	movaps	%xmm6, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm10, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	xorps	%xmm7, %xmm7
	movss	%xmm15, 44(%rsp)        # 4-byte Spill
	jnp	.LBB5_4
# BB#3:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movaps	%xmm10, 64(%rsp)        # 16-byte Spill
	movaps	%xmm6, 160(%rsp)        # 16-byte Spill
	movaps	%xmm5, 240(%rsp)        # 16-byte Spill
	movaps	%xmm13, 176(%rsp)       # 16-byte Spill
	movaps	%xmm12, 320(%rsp)       # 16-byte Spill
	movss	%xmm4, 92(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	92(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	xorps	%xmm7, %xmm7
	movaps	320(%rsp), %xmm12       # 16-byte Reload
	movaps	176(%rsp), %xmm13       # 16-byte Reload
	movaps	240(%rsp), %xmm5        # 16-byte Reload
	movaps	160(%rsp), %xmm6        # 16-byte Reload
	movaps	64(%rsp), %xmm10        # 16-byte Reload
	movss	4(%rsp), %xmm9          # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	44(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB5_4:                                # %.split
	movss	.LCPI5_3(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm11
	mulss	%xmm11, %xmm6
	mulss	%xmm11, %xmm10
	mulss	%xmm4, %xmm11
	movaps	.LCPI5_4(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm6, %xmm14
	xorps	%xmm2, %xmm14
	movss	24(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm0
	mulss	%xmm12, %xmm8
	addss	%xmm0, %xmm8
	movss	32(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm4
	addss	%xmm8, %xmm4
	addss	48(%rsp), %xmm4         # 16-byte Folded Reload
	movaps	%xmm9, %xmm0
	movaps	%xmm4, %xmm9
	mulss	%xmm13, %xmm0
	movaps	%xmm15, %xmm4
	mulss	%xmm12, %xmm4
	addss	%xmm0, %xmm4
	movss	28(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm15
	addss	%xmm4, %xmm15
	addss	16(%rsp), %xmm15        # 4-byte Folded Reload
	movaps	%xmm3, %xmm0
	mulss	%xmm13, %xmm0
	movss	36(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm4
	addss	%xmm0, %xmm4
	mulss	40(%rsp), %xmm5         # 4-byte Folded Reload
	addss	%xmm4, %xmm5
	addss	20(%rsp), %xmm5         # 4-byte Folded Reload
	movaps	%xmm7, 112(%rsp)
	movups	%xmm7, 132(%rsp)
	movaps	%xmm10, %xmm12
	xorps	%xmm2, %xmm12
	xorps	%xmm11, %xmm2
	movaps	%xmm7, 272(%rsp)
	movups	%xmm7, 292(%rsp)
	movaps	192(%rsp), %xmm4
	movaps	%xmm9, %xmm0
	unpcklps	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	movaps	%xmm6, 160(%rsp)        # 16-byte Spill
	movaps	%xmm5, %xmm13
	movaps	%xmm6, %xmm5
	unpcklps	%xmm14, %xmm5   # xmm5 = xmm5[0],xmm14[0],xmm5[1],xmm14[1]
	mulps	%xmm0, %xmm5
	movaps	%xmm4, %xmm0
	shufps	$1, %xmm15, %xmm0       # xmm0 = xmm0[1,0],xmm15[0,0]
	shufps	$226, %xmm15, %xmm0     # xmm0 = xmm0[2,0],xmm15[2,3]
	movaps	%xmm10, %xmm6
	unpcklps	%xmm12, %xmm6   # xmm6 = xmm6[0],xmm12[0],xmm6[1],xmm12[1]
	mulps	%xmm0, %xmm6
	addps	%xmm5, %xmm6
	movss	200(%rsp), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm0
	unpcklps	%xmm8, %xmm0    # xmm0 = xmm0[0],xmm8[0],xmm0[1],xmm8[1]
	movaps	%xmm11, %xmm3
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	mulps	%xmm0, %xmm3
	addps	%xmm6, %xmm3
	movq	212(%rsp), %xmm0        # xmm0 = mem[0],zero
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	movdqa	%xmm0, %xmm6
	movaps	%xmm14, 176(%rsp)       # 16-byte Spill
	mulss	%xmm14, %xmm6
	mulss	%xmm10, %xmm2
	subss	%xmm2, %xmm6
	movss	220(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm7
	mulss	%xmm11, %xmm7
	subss	%xmm7, %xmm6
	movaps	%xmm3, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	addss	%xmm3, %xmm7
	addss	%xmm3, %xmm6
	xorps	%xmm1, %xmm1
	ucomiss	%xmm7, %xmm1
	movq	312(%rsp), %r14         # 8-byte Reload
	movaps	%xmm13, %xmm14
	jb	.LBB5_6
# BB#5:
	movl	208(%rsp), %eax
	movl	%eax, 128(%rsp)
	movaps	192(%rsp), %xmm3
	movaps	%xmm3, 112(%rsp)
	movl	$1, %ebx
.LBB5_6:
	ucomiss	%xmm6, %xmm1
	movaps	%xmm10, 64(%rsp)        # 16-byte Spill
	jb	.LBB5_8
# BB#7:
	leaq	212(%rsp), %rax
	leaq	(%rbx,%rbx,4), %rcx
	leal	1(%rbx), %edx
	movl	16(%rax), %esi
	movl	%esi, 128(%rsp,%rcx,4)
	movups	(%rax), %xmm3
	movups	%xmm3, 112(%rsp,%rcx,4)
	movl	%edx, %ebx
.LBB5_8:
	movaps	%xmm7, %xmm3
	mulss	%xmm6, %xmm3
	xorps	%xmm10, %xmm10
	ucomiss	%xmm3, %xmm10
	jbe	.LBB5_10
# BB#9:
	movaps	%xmm7, %xmm3
	subss	%xmm6, %xmm3
	movaps	%xmm7, %xmm6
	divss	%xmm3, %xmm6
	subps	%xmm4, %xmm0
	subss	%xmm8, %xmm2
	mulss	%xmm6, %xmm2
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm0, %xmm6
	addps	%xmm4, %xmm6
	addss	%xmm8, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movslq	%ebx, %rbx
	leaq	(%rbx,%rbx,4), %rax
	movlps	%xmm6, 112(%rsp,%rax,4)
	movlps	%xmm0, 120(%rsp,%rax,4)
	xorl	%ecx, %ecx
	ucomiss	%xmm10, %xmm7
	setbe	%cl
	leaq	(%rcx,%rcx,4), %rcx
	movl	208(%rsp,%rcx,4), %ecx
	movl	%ecx, 128(%rsp,%rax,4)
	incl	%ebx
.LBB5_10:                               # %_ZL17ClipSegmentToLineP10ClipVertexS0_RK9btVector3f.exit118
	movss	4(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	64(%rsp), %xmm10        # 16-byte Reload
	movaps	48(%rsp), %xmm4         # 16-byte Reload
	movss	20(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	cmpl	$2, %ebx
	jl	.LBB5_26
# BB#11:
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	movss	24(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	352(%rsp), %xmm1        # 16-byte Reload
	mulss	%xmm1, %xmm0
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	336(%rsp), %xmm5        # 16-byte Reload
	mulss	%xmm5, %xmm2
	addss	%xmm0, %xmm2
	mulss	%xmm1, %xmm3
	movss	44(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	addss	%xmm3, %xmm0
	movss	12(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm8
	movss	36(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm7
	movss	32(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	100(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	movss	28(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	%xmm0, %xmm2
	addss	%xmm8, %xmm7
	movss	40(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	addss	%xmm2, %xmm6
	addss	%xmm7, %xmm0
	addss	%xmm3, %xmm4
	addss	%xmm0, %xmm13
	mulss	%xmm10, %xmm6
	movaps	160(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm2, %xmm4
	mulss	%xmm11, %xmm13
	addss	%xmm6, %xmm4
	addss	%xmm13, %xmm4
	movaps	112(%rsp), %xmm12
	movaps	%xmm10, %xmm1
	movss	120(%rsp), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movsd	132(%rsp), %xmm8        # xmm8 = mem[0],zero
	movaps	%xmm2, %xmm0
	shufps	$0, %xmm2, %xmm0        # xmm0 = xmm0[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm0      # xmm0 = xmm0[2,0],xmm2[2,3]
	movaps	%xmm12, %xmm2
	unpcklps	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1]
	mulps	%xmm0, %xmm2
	movaps	%xmm1, %xmm0
	shufps	$0, %xmm1, %xmm0        # xmm0 = xmm0[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm0      # xmm0 = xmm0[2,0],xmm1[2,3]
	movaps	%xmm8, %xmm6
	shufps	$17, %xmm12, %xmm6      # xmm6 = xmm6[1,0],xmm12[1,0]
	shufps	$226, %xmm12, %xmm6     # xmm6 = xmm6[2,0],xmm12[2,3]
	mulps	%xmm0, %xmm6
	addps	%xmm2, %xmm6
	movss	140(%rsp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	shufps	$0, %xmm11, %xmm0       # xmm0 = xmm0[0,0],xmm11[0,0]
	shufps	$226, %xmm11, %xmm0     # xmm0 = xmm0[2,0],xmm11[2,3]
	movaps	%xmm10, %xmm3
	unpcklps	%xmm7, %xmm3    # xmm3 = xmm3[0],xmm7[0],xmm3[1],xmm7[1]
	mulps	%xmm0, %xmm3
	addps	%xmm6, %xmm3
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	subps	%xmm4, %xmm3
	xorl	%eax, %eax
	xorps	%xmm0, %xmm0
	ucomiss	%xmm3, %xmm0
	jb	.LBB5_13
# BB#12:
	movl	128(%rsp), %eax
	movl	%eax, 288(%rsp)
	movaps	112(%rsp), %xmm2
	movaps	%xmm2, 272(%rsp)
	movl	$1, %eax
.LBB5_13:
	movaps	%xmm3, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	ucomiss	%xmm2, %xmm0
	jb	.LBB5_15
# BB#14:
	leaq	132(%rsp), %rcx
	leaq	(%rax,%rax,4), %rdx
	leal	1(%rax), %eax
	movl	16(%rcx), %esi
	movl	%esi, 288(%rsp,%rdx,4)
	movups	(%rcx), %xmm0
	movups	%xmm0, 272(%rsp,%rdx,4)
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
.LBB5_15:
	movaps	%xmm3, %xmm6
	mulss	%xmm2, %xmm6
	xorps	%xmm0, %xmm0
	ucomiss	%xmm6, %xmm0
	jbe	.LBB5_17
# BB#16:
	movaps	%xmm3, %xmm6
	subss	%xmm2, %xmm6
	movaps	%xmm3, %xmm2
	divss	%xmm6, %xmm2
	subps	%xmm12, %xmm8
	subss	%xmm10, %xmm7
	mulss	%xmm2, %xmm7
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm8, %xmm2
	addps	%xmm12, %xmm2
	addss	%xmm10, %xmm7
	xorps	%xmm4, %xmm4
	movss	%xmm7, %xmm4            # xmm4 = xmm7[0],xmm4[1,2,3]
	cltq
	leaq	(%rax,%rax,4), %rcx
	movlps	%xmm2, 272(%rsp,%rcx,4)
	movlps	%xmm4, 280(%rsp,%rcx,4)
	xorl	%edx, %edx
	ucomiss	%xmm0, %xmm3
	setbe	%dl
	leaq	(%rdx,%rdx,4), %rdx
	movl	128(%rsp,%rdx,4), %edx
	movl	%edx, 288(%rsp,%rcx,4)
	incl	%eax
.LBB5_17:                               # %_ZL17ClipSegmentToLineP10ClipVertexS0_RK9btVector3f.exit
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movaps	48(%rsp), %xmm4         # 16-byte Reload
	xorps	%xmm0, %xmm0
	cmpl	$2, %eax
	jl	.LBB5_26
# BB#18:                                # %.preheader
	movl	b2_maxManifoldPoints(%rip), %eax
	testl	%eax, %eax
	jle	.LBB5_26
# BB#19:                                # %.lr.ph
	movaps	160(%rsp), %xmm3        # 16-byte Reload
	mulss	%xmm3, %xmm15
	mulss	%xmm2, %xmm9
	mulss	%xmm0, %xmm14
	subss	%xmm15, %xmm9
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	256(%rsp), %xmm1        # 16-byte Reload
	ucomiss	%xmm1, %xmm0
	ja	.LBB5_20
# BB#21:                                # %.lr.ph
	movaps	%xmm2, %xmm4
	unpcklps	176(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	jmp	.LBB5_22
.LBB5_20:
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
.LBB5_22:                               # %.lr.ph
	addss	%xmm9, %xmm14
	xorps	.LCPI5_4(%rip), %xmm4
	cmpltss	%xmm0, %xmm1
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	andnps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	leaq	272(%rsp), %rbx
	xorl	%ebp, %ebp
	xorps	%xmm3, %xmm3
	unpcklpd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0]
	leaq	376(%rsp), %r15
	movaps	%xmm14, 240(%rsp)       # 16-byte Spill
	movapd	%xmm4, 48(%rsp)         # 16-byte Spill
	.p2align	4, 0x90
.LBB5_23:                               # =>This Inner Loop Header: Depth=1
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	176(%rsp), %xmm1        # 16-byte Folded Reload
	addss	%xmm0, %xmm1
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	addss	%xmm1, %xmm0
	subss	%xmm14, %xmm0
	ucomiss	%xmm0, %xmm3
	jb	.LBB5_25
# BB#24:                                #   in Loop: Header=BB5_23 Depth=1
	movq	(%r14), %rax
	movq	32(%rax), %rax
	movupd	%xmm4, 376(%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	*%rax
	xorps	%xmm3, %xmm3
	movapd	48(%rsp), %xmm4         # 16-byte Reload
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movaps	240(%rsp), %xmm14       # 16-byte Reload
	movl	b2_maxManifoldPoints(%rip), %eax
.LBB5_25:                               #   in Loop: Header=BB5_23 Depth=1
	incq	%rbp
	movslq	%eax, %rcx
	addq	$20, %rbx
	cmpq	%rcx, %rbp
	jl	.LBB5_23
.LBB5_26:
	addq	$392, %rsp              # imm = 0x188
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_Z17b2CollidePolygonsP16btManifoldResultPK12btBox2dShapeRK11btTransformS3_S6_, .Lfunc_end5-_Z17b2CollidePolygonsP16btManifoldResultPK12btBox2dShapeRK11btTransformS3_S6_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN30btBox2dBox2dCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN30btBox2dBox2dCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN30btBox2dBox2dCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN30btBox2dBox2dCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	movss	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end6:
	.size	_ZN30btBox2dBox2dCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end6-_ZN30btBox2dBox2dCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_0:
	.long	3713928043              # float -9.99999984E+17
.LCPI7_1:
	.long	0                       # float 0
	.text
	.p2align	4, 0x90
	.type	_ZL17FindMaxSeparationPiPK12btBox2dShapeRK11btTransformS2_S5_,@function
_ZL17FindMaxSeparationPiPK12btBox2dShapeRK11btTransformS2_S5_: # @_ZL17FindMaxSeparationPiPK12btBox2dShapeRK11btTransformS2_S5_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi43:
	.cfi_def_cfa_offset 96
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movss	64(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	68(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movss	4(%r12), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	addss	%xmm0, %xmm4
	movss	72(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm15         # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm15
	addss	%xmm4, %xmm15
	addss	48(%r12), %xmm15
	movss	16(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	movss	20(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm4, %xmm5
	movss	24(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	addss	52(%r12), %xmm4
	mulss	32(%r12), %xmm3
	mulss	36(%r12), %xmm2
	addss	%xmm3, %xmm2
	mulss	40(%r12), %xmm0
	addss	%xmm2, %xmm0
	addss	56(%r12), %xmm0
	movss	(%r13), %xmm11          # xmm11 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm2
	mulss	%xmm13, %xmm2
	movaps	%xmm10, %xmm5
	mulss	%xmm1, %xmm5
	addss	%xmm2, %xmm5
	movss	8(%r13), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm6
	mulss	%xmm12, %xmm6
	addss	%xmm5, %xmm6
	addss	48(%r13), %xmm6
	movss	16(%r13), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm2
	mulss	%xmm5, %xmm2
	movss	20(%r13), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm7
	mulss	%xmm14, %xmm7
	addss	%xmm2, %xmm7
	movss	24(%r13), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm3
	mulss	%xmm9, %xmm3
	addss	%xmm7, %xmm3
	addss	52(%r13), %xmm3
	movss	32(%r13), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm13
	movss	36(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	addss	%xmm13, %xmm1
	movss	40(%r13), %xmm13        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm12
	addss	%xmm1, %xmm12
	addss	56(%r13), %xmm12
	subss	%xmm6, %xmm15
	subss	%xmm3, %xmm4
	subss	%xmm12, %xmm0
	mulss	%xmm15, %xmm11
	mulss	%xmm4, %xmm5
	addss	%xmm11, %xmm5
	mulss	%xmm0, %xmm7
	addss	%xmm5, %xmm7
	mulss	%xmm15, %xmm10
	mulss	%xmm4, %xmm14
	addss	%xmm10, %xmm14
	mulss	%xmm0, %xmm2
	addss	%xmm14, %xmm2
	mulss	%xmm8, %xmm15
	mulss	%xmm9, %xmm4
	addss	%xmm15, %xmm4
	mulss	%xmm13, %xmm0
	addss	%xmm4, %xmm0
	movss	144(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	movss	148(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	addss	%xmm1, %xmm3
	movss	152(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm3, %xmm1
	maxss	.LCPI7_0(%rip), %xmm1
	movss	160(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm3
	movss	164(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	addss	%xmm3, %xmm4
	movss	168(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm4, %xmm3
	xorl	%eax, %eax
	ucomiss	%xmm1, %xmm3
	seta	%al
	maxss	%xmm1, %xmm3
	movss	176(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	movss	180(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	addss	%xmm1, %xmm4
	movss	184(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm4, %xmm1
	ucomiss	%xmm3, %xmm1
	maxss	%xmm3, %xmm1
	movl	$2, %ebp
	cmovbel	%eax, %ebp
	mulss	192(%rbx), %xmm7
	mulss	196(%rbx), %xmm2
	addss	%xmm7, %xmm2
	mulss	200(%rbx), %xmm0
	addss	%xmm2, %xmm0
	ucomiss	%xmm1, %xmm0
	movl	$3, %r14d
	cmoval	%r14d, %ebp
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	%ebp, %edx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	callq	_ZL14EdgeSeparationPK12btBox2dShapeRK11btTransformiS1_S4_
	movaps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm1
	ja	.LBB7_17
# BB#1:
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	leal	-1(%rbp), %eax
	testl	%ebp, %ebp
	cmovnel	%eax, %r14d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	%r14d, %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r12, %r8
	callq	_ZL14EdgeSeparationPK12btBox2dShapeRK11btTransformiS1_S4_
	ucomiss	.LCPI7_1, %xmm0
	ja	.LBB7_16
# BB#3:
	movss	%xmm0, 28(%rsp)         # 4-byte Spill
	movq	%r15, 32(%rsp)          # 8-byte Spill
	leal	1(%rbp), %eax
	xorl	%r15d, %r15d
	cmpl	$4, %eax
	cmovbl	%eax, %r15d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	%r15d, %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r12, %r8
	callq	_ZL14EdgeSeparationPK12btBox2dShapeRK11btTransformiS1_S4_
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.LBB7_5
.LBB7_16:
	movaps	%xmm0, %xmm1
.LBB7_17:                               # %.loopexit
	movaps	%xmm1, %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_5:
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jbe	.LBB7_10
# BB#6:
	ucomiss	%xmm0, %xmm2
	jbe	.LBB7_10
# BB#7:
	movl	$3, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_8:                                # %.preheader.split.us
                                        # =>This Inner Loop Header: Depth=1
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	movl	%r14d, %ebp
	leal	-1(%rbp), %r14d
	testl	%ebp, %ebp
	cmovlel	%r15d, %r14d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	%r14d, %edx
	movq	%rax, %rcx
	movq	%r12, %r8
	callq	_ZL14EdgeSeparationPK12btBox2dShapeRK11btTransformiS1_S4_
	movaps	%xmm0, %xmm2
	ucomiss	.LCPI7_1, %xmm2
	ja	.LBB7_15
# BB#9:                                 #   in Loop: Header=BB7_8 Depth=1
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	movq	16(%rsp), %rax          # 8-byte Reload
	ja	.LBB7_8
	jmp	.LBB7_14
.LBB7_10:
	ucomiss	%xmm1, %xmm0
	movq	16(%rsp), %rax          # 8-byte Reload
	jbe	.LBB7_14
# BB#11:
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_12:                               # %.preheader.split
                                        # =>This Inner Loop Header: Depth=1
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movl	%r15d, %ebp
	leal	1(%rbp), %r15d
	cmpl	$4, %r15d
	cmovgel	%r14d, %r15d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	%r15d, %edx
	movq	%rax, %rcx
	movq	%r12, %r8
	callq	_ZL14EdgeSeparationPK12btBox2dShapeRK11btTransformiS1_S4_
	ucomiss	.LCPI7_1, %xmm0
	ja	.LBB7_16
# BB#13:                                #   in Loop: Header=BB7_12 Depth=1
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	movq	16(%rsp), %rax          # 8-byte Reload
	ja	.LBB7_12
.LBB7_14:                               # %.sink.split
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%ebp, (%rax)
	jmp	.LBB7_17
.LBB7_15:
	movaps	%xmm2, %xmm1
	jmp	.LBB7_17
.Lfunc_end7:
	.size	_ZL17FindMaxSeparationPiPK12btBox2dShapeRK11btTransformS2_S5_, .Lfunc_end7-_ZL17FindMaxSeparationPiPK12btBox2dShapeRK11btTransformS2_S5_
	.cfi_endproc

	.section	.text._ZN30btBox2dBox2dCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,"axG",@progbits,_ZN30btBox2dBox2dCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,comdat
	.weak	_ZN30btBox2dBox2dCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.p2align	4, 0x90
	.type	_ZN30btBox2dBox2dCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,@function
_ZN30btBox2dBox2dCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE: # @_ZN30btBox2dBox2dCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 48
.Lcfi55:
	.cfi_offset %rbx, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	24(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB8_18
# BB#1:
	cmpb	$0, 16(%r14)
	je	.LBB8_18
# BB#2:
	movl	4(%rbx), %eax
	cmpl	8(%rbx), %eax
	jne	.LBB8_17
# BB#3:
	leal	(%rax,%rax), %edx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%edx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB8_17
# BB#4:
	testl	%ebp, %ebp
	je	.LBB8_5
# BB#6:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB8_8
	jmp	.LBB8_12
.LBB8_5:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB8_12
.LBB8_8:                                # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB8_10
	.p2align	4, 0x90
.LBB8_9:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB8_9
.LBB8_10:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB8_12
	.p2align	4, 0x90
.LBB8_11:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r15,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB8_11
.LBB8_12:                               # %_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_.exit.i.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_16
# BB#13:
	cmpb	$0, 24(%rbx)
	je	.LBB8_15
# BB#14:
	callq	_Z21btAlignedFreeInternalPv
	movl	4(%rbx), %eax
.LBB8_15:
	movq	$0, 16(%rbx)
.LBB8_16:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv.exit.i.i
	movb	$1, 24(%rbx)
	movq	%r15, 16(%rbx)
	movl	%ebp, 8(%rbx)
	movq	24(%r14), %rcx
.LBB8_17:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_.exit
	movq	16(%rbx), %rdx
	cltq
	movq	%rcx, (%rdx,%rax,8)
	incl	%eax
	movl	%eax, 4(%rbx)
.LBB8_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN30btBox2dBox2dCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE, .Lfunc_end8-_ZN30btBox2dBox2dCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	1566444395              # float 9.99999984E+17
	.text
	.p2align	4, 0x90
	.type	_ZL14EdgeSeparationPK12btBox2dShapeRK11btTransformiS1_S4_,@function
_ZL14EdgeSeparationPK12btBox2dShapeRK11btTransformiS1_S4_: # @_ZL14EdgeSeparationPK12btBox2dShapeRK11btTransformiS1_S4_
	.cfi_startproc
# BB#0:
	movslq	%edx, %r9
	shlq	$4, %r9
	movss	144(%rdi,%r9), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm13          # xmm13 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm0
	mulss	%xmm1, %xmm0
	movss	148(%rdi,%r9), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, -28(%rsp)        # 4-byte Spill
	movss	152(%rdi,%r9), %xmm8    # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm0
	addss	%xmm3, %xmm0
	movss	16(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	%xmm4, -24(%rsp)        # 4-byte Spill
	movaps	%xmm1, %xmm3
	mulss	%xmm4, %xmm3
	movss	20(%rsi), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	mulss	%xmm9, %xmm5
	addss	%xmm3, %xmm5
	movss	24(%rsi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm15
	mulss	%xmm14, %xmm15
	addss	%xmm5, %xmm15
	movss	32(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, -32(%rsp)        # 4-byte Spill
	mulss	%xmm3, %xmm1
	movss	36(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, -36(%rsp)        # 4-byte Spill
	mulss	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	movss	40(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, -44(%rsp)        # 4-byte Spill
	mulss	%xmm1, %xmm8
	addss	%xmm2, %xmm8
	movss	16(%r8), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, -16(%rsp)        # 4-byte Spill
	movss	(%r8), %xmm2            # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, -40(%rsp)        # 4-byte Spill
	movaps	%xmm0, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm15, %xmm2
	mulss	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	movss	32(%r8), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, -8(%rsp)         # 4-byte Spill
	movaps	%xmm8, %xmm4
	mulss	%xmm1, %xmm4
	addss	%xmm2, %xmm4
	movss	4(%r8), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	movss	%xmm0, -4(%rsp)         # 4-byte Spill
	mulss	%xmm5, %xmm2
	movss	20(%r8), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm3
	mulss	%xmm10, %xmm3
	addss	%xmm2, %xmm3
	movss	36(%r8), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, -12(%rsp)        # 4-byte Spill
	movaps	%xmm8, %xmm2
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	8(%r8), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	mulss	%xmm7, %xmm3
	movss	24(%r8), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm11
	mulss	%xmm6, %xmm11
	addss	%xmm3, %xmm11
	movss	40(%r8), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, -20(%rsp)        # 4-byte Spill
	movaps	%xmm8, %xmm3
	mulss	%xmm0, %xmm3
	addss	%xmm11, %xmm3
	movss	80(%rcx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm11
	movss	84(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	addss	%xmm11, %xmm0
	movss	88(%rcx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm11
	addss	%xmm0, %xmm11
	movss	96(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	100(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	movss	104(%rcx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	%xmm0, %xmm1
	minss	.LCPI9_0(%rip), %xmm11
	xorl	%eax, %eax
	ucomiss	%xmm1, %xmm11
	minss	%xmm11, %xmm1
	movss	112(%rcx), %xmm11       # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm11
	movss	116(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	addss	%xmm11, %xmm0
	movss	120(%rcx), %xmm11       # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm11
	addss	%xmm0, %xmm11
	seta	%al
	ucomiss	%xmm11, %xmm1
	minss	%xmm1, %xmm11
	mulss	128(%rcx), %xmm4
	mulss	132(%rcx), %xmm2
	addss	%xmm4, %xmm2
	mulss	136(%rcx), %xmm3
	addss	%xmm2, %xmm3
	movl	$2, %edx
	cmovbeq	%rax, %rdx
	ucomiss	%xmm3, %xmm11
	movss	80(%rdi,%r9), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm13
	movss	84(%rdi,%r9), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm12
	addss	%xmm13, %xmm12
	movss	88(%rdi,%r9), %xmm13    # xmm13 = mem[0],zero,zero,zero
	movss	-28(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm2
	addss	%xmm12, %xmm2
	movaps	%xmm2, %xmm3
	movss	-24(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	mulss	%xmm1, %xmm9
	addss	%xmm2, %xmm9
	mulss	%xmm13, %xmm14
	addss	%xmm9, %xmm14
	mulss	-32(%rsp), %xmm0        # 4-byte Folded Reload
	mulss	-36(%rsp), %xmm1        # 4-byte Folded Reload
	addss	%xmm0, %xmm1
	mulss	-44(%rsp), %xmm13       # 4-byte Folded Reload
	addss	%xmm1, %xmm13
	movl	$3, %eax
	cmovbeq	%rdx, %rax
	shlq	$4, %rax
	movss	80(%rcx,%rax), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movss	-40(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movss	84(%rcx,%rax), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm0, %xmm5
	movss	88(%rcx,%rax), %xmm0    # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	addss	%xmm5, %xmm7
	movss	-16(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	mulss	%xmm2, %xmm10
	addss	%xmm4, %xmm10
	mulss	%xmm0, %xmm6
	addss	%xmm10, %xmm6
	mulss	-8(%rsp), %xmm1         # 4-byte Folded Reload
	mulss	-12(%rsp), %xmm2        # 4-byte Folded Reload
	addss	%xmm1, %xmm2
	mulss	-20(%rsp), %xmm0        # 4-byte Folded Reload
	addss	%xmm2, %xmm0
	addss	48(%rsi), %xmm3
	addss	48(%r8), %xmm7
	subss	%xmm3, %xmm7
	addss	52(%rsi), %xmm14
	addss	52(%r8), %xmm6
	subss	%xmm14, %xmm6
	addss	56(%rsi), %xmm13
	addss	56(%r8), %xmm0
	subss	%xmm13, %xmm0
	mulss	-4(%rsp), %xmm7         # 4-byte Folded Reload
	mulss	%xmm15, %xmm6
	addss	%xmm7, %xmm6
	mulss	%xmm8, %xmm0
	addss	%xmm6, %xmm0
	retq
.Lfunc_end9:
	.size	_ZL14EdgeSeparationPK12btBox2dShapeRK11btTransformiS1_S4_, .Lfunc_end9-_ZL14EdgeSeparationPK12btBox2dShapeRK11btTransformiS1_S4_
	.cfi_endproc

	.type	_ZTV30btBox2dBox2dCollisionAlgorithm,@object # @_ZTV30btBox2dBox2dCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTV30btBox2dBox2dCollisionAlgorithm
	.p2align	3
_ZTV30btBox2dBox2dCollisionAlgorithm:
	.quad	0
	.quad	_ZTI30btBox2dBox2dCollisionAlgorithm
	.quad	_ZN30btBox2dBox2dCollisionAlgorithmD2Ev
	.quad	_ZN30btBox2dBox2dCollisionAlgorithmD0Ev
	.quad	_ZN30btBox2dBox2dCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN30btBox2dBox2dCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN30btBox2dBox2dCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.size	_ZTV30btBox2dBox2dCollisionAlgorithm, 56

	.type	b2_maxManifoldPoints,@object # @b2_maxManifoldPoints
	.data
	.globl	b2_maxManifoldPoints
	.p2align	2
b2_maxManifoldPoints:
	.long	2                       # 0x2
	.size	b2_maxManifoldPoints, 4

	.type	_ZTS30btBox2dBox2dCollisionAlgorithm,@object # @_ZTS30btBox2dBox2dCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTS30btBox2dBox2dCollisionAlgorithm
	.p2align	4
_ZTS30btBox2dBox2dCollisionAlgorithm:
	.asciz	"30btBox2dBox2dCollisionAlgorithm"
	.size	_ZTS30btBox2dBox2dCollisionAlgorithm, 33

	.type	_ZTI30btBox2dBox2dCollisionAlgorithm,@object # @_ZTI30btBox2dBox2dCollisionAlgorithm
	.globl	_ZTI30btBox2dBox2dCollisionAlgorithm
	.p2align	4
_ZTI30btBox2dBox2dCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS30btBox2dBox2dCollisionAlgorithm
	.quad	_ZTI30btActivatingCollisionAlgorithm
	.size	_ZTI30btBox2dBox2dCollisionAlgorithm, 24


	.globl	_ZN30btBox2dBox2dCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_
	.type	_ZN30btBox2dBox2dCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_,@function
_ZN30btBox2dBox2dCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_ = _ZN30btBox2dBox2dCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_
	.globl	_ZN30btBox2dBox2dCollisionAlgorithmD1Ev
	.type	_ZN30btBox2dBox2dCollisionAlgorithmD1Ev,@function
_ZN30btBox2dBox2dCollisionAlgorithmD1Ev = _ZN30btBox2dBox2dCollisionAlgorithmD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
