	.text
	.file	"d7-cntrl.bc"
	.globl	_Z5doif1P9Classfile
	.p2align	4, 0x90
	.type	_Z5doif1P9Classfile,@function
_Z5doif1P9Classfile:                    # @_Z5doif1P9Classfile
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %ebp
	leal	-1(%rbp), %r15d
	movq	stkptr(%rip), %rax
	leaq	-8(%rax), %rcx
	movq	%rcx, stkptr(%rip)
	movq	-8(%rax), %rbx
	movq	%rbx, (%rsp)
	movq	(%rbx), %rax
	movl	8(%rax), %edx
	addl	$-4, %edx
	cmpl	$8, %edx
	ja	.LBB0_16
# BB#1:
	jmpq	*.LJTI0_0(,%rdx,8)
.LBB0_7:
	movq	(%rcx), %r12
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r14
	movl	$1, 8(%r14)
	movl	%r15d, 16(%r14)
	movl	%r15d, 12(%r14)
	movq	$std_exps+48, (%r14)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	16(%r12), %eax
	movl	ch(%rip), %r13d
	movl	$1, 8(%rbx)
	movl	%r15d, 12(%rbx)
	movl	%eax, 16(%rbx)
.Ltmp3:
	movl	$24, %edi
	callq	_Znwm
.Ltmp4:
# BB#8:
	addl	$-125, %r13d
	jmp	.LBB0_9
.LBB0_4:
	cmpl	$153, ch(%rip)
	jne	.LBB0_16
# BB#5:
	movq	%rsp, %rdi
	callq	_Z6notexpPP3Exp
	testl	%eax, %eax
	jne	.LBB0_3
# BB#6:                                 # %._crit_edge
	movq	(%rsp), %rbx
	movl	currpc(%rip), %ebp
	jmp	.LBB0_16
.LBB0_12:
	movq	(%rcx), %r12
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r14
	movl	$1, 8(%r14)
	movl	%r15d, 16(%r14)
	movl	%r15d, 12(%r14)
	movq	$std_exps, (%r14)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	16(%r12), %eax
	movl	ch(%rip), %r13d
	movl	$1, 8(%rbx)
	movl	%r15d, 12(%rbx)
	movl	%eax, 16(%rbx)
.Ltmp0:
	movl	$24, %edi
	callq	_Znwm
.Ltmp1:
# BB#13:
	addl	$-170, %r13d
.LBB0_9:
	movl	$0, (%rax)
	movl	$4, 4(%rax)
	movl	$10, 8(%rax)
	movl	%r13d, 12(%rax)
	movq	%rax, (%rbx)
	movq	%r12, 24(%rbx)
	movq	%r14, 32(%rbx)
	movq	%rbx, (%rsp)
	jmp	.LBB0_16
.LBB0_2:
	cmpl	$26, 12(%rax)
	jne	.LBB0_3
# BB#15:
	movl	ch(%rip), %ecx
	addl	$-125, %ecx
	movl	%ecx, 12(%rax)
	movl	$10, 8(%rax)
.LBB0_16:
	movl	$64, %edi
	callq	_Znwm
	movl	16(%rbx), %ecx
	addl	$2, %ebp
	movl	%ebp, currpc(%rip)
	addl	$-2, bufflength(%rip)
	movq	inbuff(%rip), %rdx
	leaq	2(%rdx), %rsi
	movq	%rsi, inbuff(%rip)
	movzbl	(%rdx), %esi
	shll	$8, %esi
	movzbl	1(%rdx), %edx
	orl	%esi, %edx
	movswl	%dx, %edx
	addl	%r15d, %edx
	movl	$1, 8(%rax)
	movl	%r15d, 12(%rax)
	movl	%ecx, 16(%rax)
	movq	$std_exps+408, (%rax)
	movq	%rbx, 24(%rax)
	movl	%edx, 48(%rax)
	movq	donestkptr(%rip), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, donestkptr(%rip)
	movq	%rax, (%rcx)
	xorl	%eax, %eax
.LBB0_17:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$12, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %eax
	jmp	.LBB0_17
.LBB0_14:
.Ltmp2:
	jmp	.LBB0_11
.LBB0_10:
.Ltmp5:
.LBB0_11:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z5doif1P9Classfile, .Lfunc_end0-_Z5doif1P9Classfile
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_7
	.quad	.LBB0_16
	.quad	.LBB0_16
	.quad	.LBB0_16
	.quad	.LBB0_12
	.quad	.LBB0_16
	.quad	.LBB0_4
	.quad	.LBB0_16
	.quad	.LBB0_2
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp3-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp0-.Ltmp4           #   Call between .Ltmp4 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end0-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z5doif2P9Classfile
	.p2align	4, 0x90
	.type	_Z5doif2P9Classfile,@function
_Z5doif2P9Classfile:                    # @_Z5doif2P9Classfile
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %ebp
	leal	-1(%rbp), %r14d
	movq	stkptr(%rip), %rax
	leaq	-8(%rax), %rcx
	movq	%rcx, stkptr(%rip)
	movq	-8(%rax), %r12
	leaq	-16(%rax), %rcx
	movq	%rcx, stkptr(%rip)
	movq	-16(%rax), %r13
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	16(%r13), %eax
	cmpl	16(%r12), %eax
	movq	%r12, %rax
	cmovbq	%r13, %rax
	movl	16(%rax), %eax
	movl	ch(%rip), %r15d
	movl	$1, 8(%rbx)
	movl	%r14d, 12(%rbx)
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	%eax, 16(%rbx)
.Ltmp6:
	movl	$24, %edi
	callq	_Znwm
.Ltmp7:
# BB#1:
	addl	$-159, %r15d
	movslq	%r15d, %rcx
	imulq	$715827883, %rcx, %rdx  # imm = 0x2AAAAAAB
	movq	%rdx, %rsi
	shrq	$63, %rsi
	shrq	$32, %rdx
	addl	%esi, %edx
	addl	%edx, %edx
	leal	(%rdx,%rdx,2), %edx
	subl	%edx, %ecx
	addl	$28, %ecx
	movl	$0, (%rax)
	movl	$4, 4(%rax)
	movl	$10, 8(%rax)
	movl	%ecx, 12(%rax)
	movq	%rax, (%rbx)
	movq	%r13, 24(%rbx)
	movq	%r12, 32(%rbx)
	movl	$64, %edi
	callq	_Znwm
	addl	$2, %ebp
	movl	%ebp, currpc(%rip)
	addl	$-2, bufflength(%rip)
	movq	inbuff(%rip), %rcx
	leaq	2(%rcx), %rdx
	movq	%rdx, inbuff(%rip)
	movzbl	(%rcx), %edx
	shll	$8, %edx
	movzbl	1(%rcx), %ecx
	orl	%edx, %ecx
	movswl	%cx, %ecx
	addl	%r14d, %ecx
	movl	$1, 8(%rax)
	movl	%r14d, 12(%rax)
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	%edx, 16(%rax)
	movq	$std_exps+408, (%rax)
	movq	%rbx, 24(%rax)
	movl	%ecx, 48(%rax)
	movq	donestkptr(%rip), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, donestkptr(%rip)
	movq	%rax, (%rcx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_2:
.Ltmp8:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_Z5doif2P9Classfile, .Lfunc_end1-_Z5doif2P9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp6-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Lfunc_end1-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	12                      # 0xc
	.long	26                      # 0x1a
	.text
	.globl	_Z5docmpP9Classfile
	.p2align	4, 0x90
	.type	_Z5docmpP9Classfile,@function
_Z5docmpP9Classfile:                    # @_Z5docmpP9Classfile
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -48
.Lcfi32:
	.cfi_offset %r12, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %ebp
	decl	%ebp
	movq	stkptr(%rip), %r14
	leaq	-8(%r14), %rax
	movq	%rax, stkptr(%rip)
	movq	-16(%r14), %r12
	movq	-8(%r14), %r15
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	16(%r12), %eax
	cmpl	16(%r15), %eax
	movq	%r15, %rax
	cmovbq	%r12, %rax
	movl	16(%rax), %eax
	movl	$1, 8(%rbx)
	movl	%ebp, 12(%rbx)
	movl	%eax, 16(%rbx)
.Ltmp9:
	movl	$24, %edi
	callq	_Znwm
.Ltmp10:
# BB#1:
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [0,4,12,26]
	movups	%xmm0, (%rax)
	movq	%rax, (%rbx)
	movq	%r12, 24(%rbx)
	movq	%r15, 32(%rbx)
	movq	%rbx, -16(%r14)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_2:
.Ltmp11:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_Z5docmpP9Classfile, .Lfunc_end2-_Z5docmpP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp9-.Lfunc_begin2    #   Call between .Lfunc_begin2 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin2    # >> Call Site 2 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin2   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Lfunc_end2-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	10                      # 0xa
	.long	34                      # 0x22
	.text
	.globl	_Z17finishconditionalP9Classfile
	.p2align	4, 0x90
	.type	_Z17finishconditionalP9Classfile,@function
_Z17finishconditionalP9Classfile:       # @_Z17finishconditionalP9Classfile
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 32
.Lcfi39:
	.cfi_offset %rbx, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	movq	stkptr(%rip), %rax
	cmpq	cond_stkptr(%rip), %rax
	jne	.LBB3_2
# BB#1:
	movq	donestkptr(%rip), %rax
	cmpq	cond_donestkptr(%rip), %rax
	jne	.LBB3_2
# BB#4:
	movq	cond_e(%rip), %rax
	addq	$24, (%rax)
	movq	cond_e(%rip), %r14
	movq	24(%r14), %r15
	movq	(%r15), %rax
	movl	12(%rax), %ecx
	leal	-28(%rcx), %edx
	cmpl	$6, %edx
	jb	.LBB3_10
# BB#5:
	cmpl	$10, 8(%rax)
	jne	.LBB3_6
# BB#7:
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	currpc(%rip), %eax
	movl	16(%r15), %ecx
	movl	$1, 8(%rbx)
	movl	%eax, 12(%rbx)
	movl	%ecx, 16(%rbx)
.Ltmp12:
	movl	$24, %edi
	callq	_Znwm
.Ltmp13:
# BB#8:
	movaps	.LCPI3_0(%rip), %xmm0   # xmm0 = [0,2,10,34]
	movups	%xmm0, (%rax)
	movq	%rax, (%rbx)
	movq	%r15, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	%rbx, 24(%r14)
	jmp	.LBB3_11
.LBB3_10:
	xorl	$1, %ecx
	movl	%ecx, 12(%rax)
.LBB3_11:
	movq	cond_e2(%rip), %rax
	movq	%rax, 32(%r14)
	movq	stkptr(%rip), %rax
	movq	-8(%rax), %rcx
	movq	%rcx, 40(%r14)
	movq	%r14, -8(%rax)
	movl	$-1, cond_pcend(%rip)
	xorl	%eax, %eax
.LBB3_12:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB3_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$11, %esi
.LBB3_3:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %eax
	jmp	.LBB3_12
.LBB3_6:
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$24, %esi
	jmp	.LBB3_3
.LBB3_9:
.Ltmp14:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_Z17finishconditionalP9Classfile, .Lfunc_end3-_Z17finishconditionalP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp12-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin3   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Lfunc_end3-.Ltmp13     #   Call between .Ltmp13 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	cond_pcend,@object      # @cond_pcend
	.bss
	.globl	cond_pcend
	.p2align	2
cond_pcend:
	.long	0                       # 0x0
	.size	cond_pcend, 4

	.type	cond_e,@object          # @cond_e
	.globl	cond_e
	.p2align	3
cond_e:
	.quad	0
	.size	cond_e, 8

	.type	cond_e2,@object         # @cond_e2
	.globl	cond_e2
	.p2align	3
cond_e2:
	.quad	0
	.size	cond_e2, 8

	.type	cond_donestkptr,@object # @cond_donestkptr
	.globl	cond_donestkptr
	.p2align	3
cond_donestkptr:
	.quad	0
	.size	cond_donestkptr, 8

	.type	cond_stkptr,@object     # @cond_stkptr
	.globl	cond_stkptr
	.p2align	3
cond_stkptr:
	.quad	0
	.size	cond_stkptr, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"doif1 error\n"
	.size	.L.str, 13

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Error cond\n"
	.size	.L.str.1, 12

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Can't not a non-boolean\n"
	.size	.L.str.2, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
