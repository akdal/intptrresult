	.text
	.file	"Salignmm.bc"
	.globl	imp_match_out_sc
	.p2align	4, 0x90
	.type	imp_match_out_sc,@function
imp_match_out_sc:                       # @imp_match_out_sc
	.cfi_startproc
# BB#0:
	movq	impmtx(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	%esi, %rcx
	movss	(%rax,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end0:
	.size	imp_match_out_sc, .Lfunc_end0-imp_match_out_sc
	.cfi_endproc

	.globl	imp_rna
	.p2align	4, 0x90
	.type	imp_rna,@function
imp_rna:                                # @imp_rna
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	48(%rsp)
.Lcfi1:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)
.Lcfi2:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)
.Lcfi3:
	.cfi_adjust_cfa_offset 8
	pushq	impmtx(%rip)
.Lcfi4:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)
.Lcfi5:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)
.Lcfi6:
	.cfi_adjust_cfa_offset 8
	callq	foldrna
	addq	$48, %rsp
.Lcfi7:
	.cfi_adjust_cfa_offset -48
	popq	%rax
	retq
.Lfunc_end1:
	.size	imp_rna, .Lfunc_end1-imp_rna
	.cfi_endproc

	.globl	imp_match_init_strict
	.p2align	4, 0x90
	.type	imp_match_init_strict,@function
imp_match_init_strict:                  # @imp_match_init_strict
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 112
.Lcfi15:
	.cfi_offset %rbx, -56
.Lcfi16:
	.cfi_offset %r12, -48
.Lcfi17:
	.cfi_offset %r13, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movl	%r8d, %r15d
	movl	%ecx, %r13d
	movl	%edx, %ebp
	movl	%esi, %r14d
	movl	impalloclen(%rip), %eax
	leal	2(%r13), %ecx
	cmpl	%ecx, %eax
	jl	.LBB2_2
# BB#1:
	leal	2(%r15), %ecx
	cmpl	%ecx, %eax
	jge	.LBB2_9
.LBB2_2:
	movq	impmtx(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#3:
	callq	FreeFloatMtx
.LBB2_4:
	movq	imp_match_init_strict.nocount1(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_6
# BB#5:
	callq	free
.LBB2_6:
	movq	imp_match_init_strict.nocount2(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_8
# BB#7:
	callq	free
.LBB2_8:
	cmpl	%r15d, %r13d
	movl	%r15d, %edi
	cmovgel	%r13d, %edi
	addl	$2, %edi
	movl	%edi, impalloclen(%rip)
	movl	%edi, %esi
	callq	AllocateFloatMtx
	movq	%rax, impmtx(%rip)
	movl	impalloclen(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, imp_match_init_strict.nocount1(%rip)
	movl	impalloclen(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, imp_match_init_strict.nocount2(%rip)
.LBB2_9:                                # %.preheader208
	testl	%r13d, %r13d
	jle	.LBB2_19
# BB#10:                                # %.preheader207.lr.ph
	testl	%r14d, %r14d
	movq	imp_match_init_strict.nocount1(%rip), %rax
	jle	.LBB2_11
# BB#23:                                # %.preheader207.us.preheader
	movslq	%r14d, %rcx
	movl	%r13d, %edx
	xorl	%esi, %esi
	testb	$1, %dl
	je	.LBB2_27
	.p2align	4, 0x90
.LBB2_24:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi,%rsi,8), %rdi
	cmpb	$45, (%rdi)
	je	.LBB2_26
# BB#25:                                #   in Loop: Header=BB2_24 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB2_24
.LBB2_26:                               # %._crit_edge229.us.prol
	cmpl	%r14d, %esi
	setne	(%rax)
	movl	$1, %esi
.LBB2_27:                               # %.preheader207.us.prol.loopexit
	cmpl	$1, %r13d
	je	.LBB2_19
	.p2align	4, 0x90
.LBB2_28:                               # %.preheader207.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_29 Depth 2
                                        #     Child Loop BB2_32 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_29:                               #   Parent Loop BB2_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, (%rbx,%rsi)
	je	.LBB2_31
# BB#30:                                #   in Loop: Header=BB2_29 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB2_29
.LBB2_31:                               # %._crit_edge229.us
                                        #   in Loop: Header=BB2_28 Depth=1
	cmpl	%r14d, %edi
	setne	(%rax,%rsi)
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_32:                               #   Parent Loop BB2_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, 1(%rbx,%rsi)
	je	.LBB2_34
# BB#33:                                #   in Loop: Header=BB2_32 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB2_32
.LBB2_34:                               # %._crit_edge229.us.1
                                        #   in Loop: Header=BB2_28 Depth=1
	cmpl	%r14d, %edi
	setne	1(%rax,%rsi)
	addq	$2, %rsi
	cmpq	%rdx, %rsi
	jne	.LBB2_28
	jmp	.LBB2_19
.LBB2_11:                               # %.preheader207.preheader
	setne	%cl
	movl	%r13d, %edx
	cmpl	$31, %r13d
	jbe	.LBB2_12
# BB#15:                                # %min.iters.checked
	movl	%r13d, %r8d
	andl	$31, %r8d
	movq	%rdx, %rdi
	subq	%r8, %rdi
	je	.LBB2_12
# BB#16:                                # %vector.ph
	movzbl	%cl, %esi
	movd	%esi, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	16(%rax), %rbx
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB2_17:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm0, (%rbx)
	addq	$32, %rbx
	addq	$-32, %rsi
	jne	.LBB2_17
# BB#18:                                # %middle.block
	testl	%r8d, %r8d
	jne	.LBB2_13
	jmp	.LBB2_19
.LBB2_12:
	xorl	%edi, %edi
.LBB2_13:                               # %.preheader207.preheader307
	addq	%rdi, %rax
	subq	%rdi, %rdx
	.p2align	4, 0x90
.LBB2_14:                               # %.preheader207
                                        # =>This Inner Loop Header: Depth=1
	movb	%cl, (%rax)
	incq	%rax
	decq	%rdx
	jne	.LBB2_14
.LBB2_19:                               # %.preheader206
	testl	%r15d, %r15d
	jle	.LBB2_41
# BB#20:                                # %.preheader205.lr.ph
	testl	%ebp, %ebp
	movq	imp_match_init_strict.nocount2(%rip), %rax
	jle	.LBB2_21
# BB#45:                                # %.preheader205.us.preheader
	movslq	%ebp, %rcx
	movl	%r15d, %edx
	xorl	%esi, %esi
	testb	$1, %dl
	je	.LBB2_49
	.p2align	4, 0x90
.LBB2_46:                               # =>This Inner Loop Header: Depth=1
	movq	112(%rsp), %rdi
	movq	(%rdi,%rsi,8), %rdi
	cmpb	$45, (%rdi)
	je	.LBB2_48
# BB#47:                                #   in Loop: Header=BB2_46 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB2_46
.LBB2_48:                               # %._crit_edge223.us.prol
	cmpl	%ebp, %esi
	setne	(%rax)
	movl	$1, %esi
.LBB2_49:                               # %.preheader205.us.prol.loopexit
	cmpl	$1, %r15d
	je	.LBB2_41
	.p2align	4, 0x90
.LBB2_50:                               # %.preheader205.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_51 Depth 2
                                        #     Child Loop BB2_54 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_51:                               #   Parent Loop BB2_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rsp), %rbx
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, (%rbx,%rsi)
	je	.LBB2_53
# BB#52:                                #   in Loop: Header=BB2_51 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB2_51
.LBB2_53:                               # %._crit_edge223.us
                                        #   in Loop: Header=BB2_50 Depth=1
	cmpl	%ebp, %edi
	setne	(%rax,%rsi)
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_54:                               #   Parent Loop BB2_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rsp), %rbx
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, 1(%rbx,%rsi)
	je	.LBB2_56
# BB#55:                                #   in Loop: Header=BB2_54 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB2_54
.LBB2_56:                               # %._crit_edge223.us.1
                                        #   in Loop: Header=BB2_50 Depth=1
	cmpl	%ebp, %edi
	setne	1(%rax,%rsi)
	addq	$2, %rsi
	cmpq	%rdx, %rsi
	jne	.LBB2_50
	jmp	.LBB2_41
.LBB2_21:                               # %.preheader205.preheader
	setne	%cl
	movl	%r15d, %edx
	cmpl	$31, %r15d
	jbe	.LBB2_22
# BB#35:                                # %min.iters.checked290
	movl	%r15d, %r8d
	andl	$31, %r8d
	movq	%rdx, %rdi
	subq	%r8, %rdi
	je	.LBB2_22
# BB#36:                                # %vector.ph294
	movzbl	%cl, %esi
	movd	%esi, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	16(%rax), %rbx
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB2_37:                               # %vector.body286
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm0, (%rbx)
	addq	$32, %rbx
	addq	$-32, %rsi
	jne	.LBB2_37
# BB#38:                                # %middle.block287
	testl	%r8d, %r8d
	jne	.LBB2_39
	jmp	.LBB2_41
.LBB2_22:
	xorl	%edi, %edi
.LBB2_39:                               # %.preheader205.preheader305
	addq	%rdi, %rax
	subq	%rdi, %rdx
	.p2align	4, 0x90
.LBB2_40:                               # %.preheader205
                                        # =>This Inner Loop Header: Depth=1
	movb	%cl, (%rax)
	incq	%rax
	decq	%rdx
	jne	.LBB2_40
.LBB2_41:                               # %.preheader204
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	testl	%r13d, %r13d
	jle	.LBB2_62
# BB#42:                                # %.preheader203.lr.ph
	testl	%r15d, %r15d
	jle	.LBB2_62
# BB#43:                                # %.preheader203.us.preheader
	movq	impmtx(%rip), %r12
	decl	%r15d
	leaq	4(,%r15,4), %rbp
	movl	%r13d, %r15d
	leaq	-1(%r15), %rax
	movq	%r15, %rbx
	andq	$7, %rbx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	je	.LBB2_44
# BB#57:                                # %.preheader203.us.prol.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_58:                               # %.preheader203.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%r13,8), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	incq	%r13
	cmpq	%r13, %rbx
	jne	.LBB2_58
	jmp	.LBB2_59
.LBB2_44:
	xorl	%r13d, %r13d
.LBB2_59:                               # %.preheader203.us.prol.loopexit
	cmpq	$7, 24(%rsp)            # 8-byte Folded Reload
	jb	.LBB2_62
# BB#60:                                # %.preheader203.us.preheader.new
	subq	%r13, %r15
	leaq	56(%r12,%r13,8), %rbx
	.p2align	4, 0x90
.LBB2_61:                               # %.preheader203.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-16(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	addq	$64, %rbx
	addq	$-8, %r15
	jne	.LBB2_61
.LBB2_62:                               # %._crit_edge220
	testl	%r14d, %r14d
	movl	4(%rsp), %eax           # 4-byte Reload
	jle	.LBB2_100
# BB#63:                                # %.preheader202.lr.ph
	movsd	fastathreshold(%rip), %xmm0 # xmm0 = mem[0],zero
	movq	impmtx(%rip), %r12
	movl	%eax, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%r14d, %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$4294967295, %r15d      # imm = 0xFFFFFFFF
	.p2align	4, 0x90
.LBB2_64:                               # %.preheader202
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_66 Depth 2
                                        #       Child Loop BB2_68 Depth 3
                                        #         Child Loop BB2_69 Depth 4
                                        #         Child Loop BB2_73 Depth 4
                                        #         Child Loop BB2_77 Depth 4
                                        #         Child Loop BB2_80 Depth 4
                                        #         Child Loop BB2_84 Depth 4
	testl	%eax, %eax
	jle	.LBB2_99
# BB#65:                                # %.lr.ph212
                                        #   in Loop: Header=BB2_64 Depth=1
	movq	120(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movsd	(%rax,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movq	136(%rsp), %rax
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_66:                               #   Parent Loop BB2_64 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_68 Depth 3
                                        #         Child Loop BB2_69 Depth 4
                                        #         Child Loop BB2_73 Depth 4
                                        #         Child Loop BB2_77 Depth 4
                                        #         Child Loop BB2_80 Depth 4
                                        #         Child Loop BB2_84 Depth 4
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rsi,8), %r14
	testq	%r14, %r14
	je	.LBB2_98
# BB#67:                                # %.lr.ph
                                        #   in Loop: Header=BB2_66 Depth=2
	movq	128(%rsp), %rax
	movsd	(%rax,%rsi,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rax,%rcx,8), %r8
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_68:                               #   Parent Loop BB2_64 Depth=1
                                        #     Parent Loop BB2_66 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_69 Depth 4
                                        #         Child Loop BB2_73 Depth 4
                                        #         Child Loop BB2_77 Depth 4
                                        #         Child Loop BB2_80 Depth 4
                                        #         Child Loop BB2_84 Depth 4
	movl	$-1, %ebp
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB2_69:                               #   Parent Loop BB2_64 Depth=1
                                        #     Parent Loop BB2_66 Depth=2
                                        #       Parent Loop BB2_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.LBB2_70
# BB#71:                                #   in Loop: Header=BB2_69 Depth=4
	incq	%rdi
	xorl	%ecx, %ecx
	cmpb	$45, %al
	setne	%cl
	addl	%ecx, %ebp
	cmpl	24(%r14), %ebp
	movl	%ebp, %ecx
	jne	.LBB2_69
	jmp	.LBB2_72
	.p2align	4, 0x90
.LBB2_70:                               # %._crit_edge272
                                        #   in Loop: Header=BB2_68 Depth=3
	movl	24(%r14), %ecx
.LBB2_72:                               # %.loopexit
                                        #   in Loop: Header=BB2_68 Depth=3
	movq	%rdi, %rax
	subq	%r8, %rax
	addq	%r15, %rax
	movl	28(%r14), %edx
	cmpl	%edx, %ecx
	movl	%eax, %r9d
	je	.LBB2_76
	.p2align	4, 0x90
.LBB2_73:                               # %.preheader201
                                        #   Parent Loop BB2_64 Depth=1
                                        #     Parent Loop BB2_66 Depth=2
                                        #       Parent Loop BB2_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %ecx
	testb	%cl, %cl
	je	.LBB2_75
# BB#74:                                #   in Loop: Header=BB2_73 Depth=4
	incq	%rdi
	xorl	%ebx, %ebx
	cmpb	$45, %cl
	setne	%bl
	addl	%ebx, %ebp
	cmpl	%edx, %ebp
	jne	.LBB2_73
.LBB2_75:                               #   in Loop: Header=BB2_68 Depth=3
	subl	%r8d, %edi
	leal	(%r15,%rdi), %r9d
.LBB2_76:                               #   in Loop: Header=BB2_68 Depth=3
	movq	112(%rsp), %rcx
	movq	(%rcx,%rsi,8), %r13
	movl	$-1, %ebp
	movq	%r13, %rdi
	.p2align	4, 0x90
.LBB2_77:                               #   Parent Loop BB2_64 Depth=1
                                        #     Parent Loop BB2_66 Depth=2
                                        #       Parent Loop BB2_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %ecx
	testb	%cl, %cl
	je	.LBB2_79
# BB#78:                                #   in Loop: Header=BB2_77 Depth=4
	incq	%rdi
	xorl	%edx, %edx
	cmpb	$45, %cl
	setne	%dl
	addl	%edx, %ebp
	cmpl	32(%r14), %ebp
	jne	.LBB2_77
.LBB2_79:                               #   in Loop: Header=BB2_68 Depth=3
	movq	%rdi, %r10
	subq	%r13, %r10
	addq	%r15, %r10
	movl	36(%r14), %edx
	cmpl	%edx, 32(%r14)
	movl	%r10d, %r11d
	je	.LBB2_83
	.p2align	4, 0x90
.LBB2_80:                               # %.preheader
                                        #   Parent Loop BB2_64 Depth=1
                                        #     Parent Loop BB2_66 Depth=2
                                        #       Parent Loop BB2_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %ecx
	testb	%cl, %cl
	je	.LBB2_82
# BB#81:                                #   in Loop: Header=BB2_80 Depth=4
	incq	%rdi
	xorl	%ebx, %ebx
	cmpb	$45, %cl
	setne	%bl
	addl	%ebx, %ebp
	cmpl	%edx, %ebp
	jne	.LBB2_80
.LBB2_82:                               #   in Loop: Header=BB2_68 Depth=3
	subl	%r13d, %edi
	leal	(%r15,%rdi), %r11d
.LBB2_83:                               #   in Loop: Header=BB2_68 Depth=3
	movslq	%eax, %r15
	addq	%r8, %r15
	movslq	%r10d, %rcx
	addq	%rcx, %r13
	.p2align	4, 0x90
.LBB2_84:                               #   Parent Loop BB2_64 Depth=1
                                        #     Parent Loop BB2_66 Depth=2
                                        #       Parent Loop BB2_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%r15), %ecx
	testb	%cl, %cl
	je	.LBB2_97
# BB#85:                                #   in Loop: Header=BB2_84 Depth=4
	movzbl	(%r13), %edx
	testb	%dl, %dl
	je	.LBB2_97
# BB#86:                                #   in Loop: Header=BB2_84 Depth=4
	cmpb	$45, %cl
	je	.LBB2_89
# BB#87:                                #   in Loop: Header=BB2_84 Depth=4
	cmpb	$45, %dl
	je	.LBB2_89
# BB#88:                                #   in Loop: Header=BB2_84 Depth=4
	movss	64(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	cltq
	movq	(%r12,%rax,8), %rcx
	movslq	%r10d, %r10
	addss	(%rcx,%r10,4), %xmm3
	movss	%xmm3, (%rcx,%r10,4)
	incl	%eax
	incl	%r10d
	incq	%r15
	jmp	.LBB2_94
	.p2align	4, 0x90
.LBB2_89:                               #   in Loop: Header=BB2_84 Depth=4
	cmpb	$45, %dl
	setne	%bl
	cmpb	$45, %cl
	je	.LBB2_91
# BB#90:                                #   in Loop: Header=BB2_84 Depth=4
	testb	%bl, %bl
	je	.LBB2_93
.LBB2_91:                               #   in Loop: Header=BB2_84 Depth=4
	cmpb	$45, %cl
	jne	.LBB2_95
# BB#92:                                #   in Loop: Header=BB2_84 Depth=4
	incl	%eax
	incq	%r15
	cmpb	$45, %dl
	jne	.LBB2_95
	.p2align	4, 0x90
.LBB2_93:                               #   in Loop: Header=BB2_84 Depth=4
	incl	%r10d
.LBB2_94:                               # %.thread
                                        #   in Loop: Header=BB2_84 Depth=4
	incq	%r13
.LBB2_95:                               # %.thread
                                        #   in Loop: Header=BB2_84 Depth=4
	cmpl	%r11d, %r10d
	jg	.LBB2_97
# BB#96:                                # %.thread
                                        #   in Loop: Header=BB2_84 Depth=4
	cmpl	%r9d, %eax
	jle	.LBB2_84
	.p2align	4, 0x90
.LBB2_97:                               # %.critedge
                                        #   in Loop: Header=BB2_68 Depth=3
	movq	8(%r14), %r14
	testq	%r14, %r14
	movl	$4294967295, %r15d      # imm = 0xFFFFFFFF
	movq	24(%rsp), %rsi          # 8-byte Reload
	jne	.LBB2_68
.LBB2_98:                               # %._crit_edge
                                        #   in Loop: Header=BB2_66 Depth=2
	incq	%rsi
	cmpq	40(%rsp), %rsi          # 8-byte Folded Reload
	jne	.LBB2_66
.LBB2_99:                               # %._crit_edge213
                                        #   in Loop: Header=BB2_64 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	32(%rsp), %rcx          # 8-byte Folded Reload
	movl	4(%rsp), %eax           # 4-byte Reload
	jne	.LBB2_64
.LBB2_100:                              # %._crit_edge215
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	imp_match_init_strict, .Lfunc_end2-imp_match_init_strict
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4608533498688228557     # double 1.3
.LCPI3_3:
	.quad	4607182418800017408     # double 1
.LCPI3_4:
	.quad	4602678819172646912     # double 0.5
.LCPI3_5:
	.quad	-4620693217682128896    # double -0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_1:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI3_2:
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
.LCPI3_6:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI3_7:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI3_8:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI3_9:
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
.LCPI3_10:
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.text
	.globl	A__align
	.p2align	4, 0x90
	.type	A__align,@function
A__align:                               # @A__align
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi27:
	.cfi_def_cfa_offset 368
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movq	%r9, 80(%rsp)           # 8-byte Spill
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%rcx, %rbp
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %r15
	cvtsi2ssl	penalty(%rip), %xmm0
	movss	%xmm0, 56(%rsp)         # 4-byte Spill
	movl	A__align.orlgth1(%rip), %r14d
	testl	%r14d, %r14d
	jne	.LBB3_2
# BB#1:
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, A__align.mseq1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, A__align.mseq2(%rip)
	movl	A__align.orlgth1(%rip), %r14d
.LBB3_2:
	movq	(%r15), %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	(%r13), %rdi
	callq	strlen
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	cmpl	%r14d, %ebx
	movl	A__align.orlgth2(%rip), %r12d
	movq	%r15, 72(%rsp)          # 8-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r13, 88(%rsp)          # 8-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jg	.LBB3_4
# BB#3:
	cmpl	%r12d, %eax
	jle	.LBB3_9
.LBB3_4:
	testl	%r14d, %r14d
	jle	.LBB3_7
# BB#5:
	testl	%r12d, %r12d
	movq	32(%rsp), %rbx          # 8-byte Reload
	jle	.LBB3_8
# BB#6:
	movq	A__align.w1(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align.w2(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align.match(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align.m(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align.mp(%rip), %rdi
	callq	FreeIntVec
	movq	A__align.mseq(%rip), %rdi
	callq	FreeCharMtx
	movq	A__align.ogcp1(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align.ogcp2(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align.fgcp1(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align.fgcp2(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align.cpmx1(%rip), %rdi
	callq	FreeFloatMtx
	movq	A__align.cpmx2(%rip), %rdi
	callq	FreeFloatMtx
	movq	A__align.floatwork(%rip), %rdi
	callq	FreeFloatMtx
	movq	A__align.intwork(%rip), %rdi
	callq	FreeIntMtx
	movl	A__align.orlgth1(%rip), %r14d
	movl	A__align.orlgth2(%rip), %r12d
	jmp	.LBB3_8
.LBB3_7:
	movq	32(%rsp), %rbx          # 8-byte Reload
.LBB3_8:
	movq	16(%rsp), %rax          # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI3_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r14d, %eax
	cmovgel	%eax, %r14d
	leal	100(%r14), %r15d
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r12d, %eax
	cmovgel	%eax, %r12d
	leal	100(%r12), %ebx
	leal	102(%r12), %ebp
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align.w1(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align.w2(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align.match(%rip)
	leal	102(%r14), %r13d
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align.initverticalw(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align.lastverticalw(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align.m(%rip)
	movl	%ebp, %edi
	callq	AllocateIntVec
	movq	%rax, A__align.mp(%rip)
	movl	njob(%rip), %edi
	leal	200(%r12,%r14), %esi
	callq	AllocateCharMtx
	movq	%rax, A__align.mseq(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align.ogcp1(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align.ogcp2(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align.fgcp1(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align.fgcp2(%rip)
	movl	$26, %edi
	movl	%r13d, %esi
	callq	AllocateFloatMtx
	movq	%rax, A__align.cpmx1(%rip)
	movl	$26, %edi
	movl	%ebp, %esi
	callq	AllocateFloatMtx
	movq	%rax, A__align.cpmx2(%rip)
	cmpl	%ebx, %r15d
	cmovgel	%r15d, %ebx
	addl	$2, %ebx
	movl	$26, %esi
	movl	%ebx, %edi
	callq	AllocateFloatMtx
	movq	%rax, A__align.floatwork(%rip)
	movl	$27, %esi
	movl	%ebx, %edi
	callq	AllocateIntMtx
	movq	%rax, A__align.intwork(%rip)
	movl	%r14d, A__align.orlgth1(%rip)
	movl	%r12d, A__align.orlgth2(%rip)
	movq	88(%rsp), %r13          # 8-byte Reload
	movq	72(%rsp), %r15          # 8-byte Reload
.LBB3_9:                                # %.preheader378
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_15
# BB#10:                                # %.lr.ph432
	movq	A__align.mseq(%rip), %rsi
	movq	A__align.mseq1(%rip), %rdi
	movslq	16(%rsp), %rax          # 4-byte Folded Reload
	movl	48(%rsp), %ecx          # 4-byte Reload
	leaq	-1(%rcx), %r8
	movq	%rcx, %rbx
	xorl	%ebp, %ebp
	andq	$3, %rbx
	je	.LBB3_12
	.p2align	4, 0x90
.LBB3_11:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rbp,8), %rdx
	movq	%rdx, (%rdi,%rbp,8)
	movq	(%r15,%rbp,8), %rdx
	movb	$0, (%rdx,%rax)
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB3_11
.LBB3_12:                               # %.prol.loopexit898
	cmpq	$3, %r8
	jb	.LBB3_15
# BB#13:                                # %.lr.ph432.new
	subq	%rbp, %rcx
	leaq	24(%r15,%rbp,8), %rdx
	leaq	24(%rdi,%rbp,8), %rdi
	leaq	24(%rsi,%rbp,8), %rsi
	.p2align	4, 0x90
.LBB3_14:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rsi), %rbp
	movq	%rbp, -24(%rdi)
	movq	-24(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	-16(%rsi), %rbp
	movq	%rbp, -16(%rdi)
	movq	-16(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	-8(%rsi), %rbp
	movq	%rbp, -8(%rdi)
	movq	-8(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	(%rsi), %rbp
	movq	%rbp, (%rdi)
	movq	(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	addq	$32, %rdx
	addq	$32, %rdi
	addq	$32, %rsi
	addq	$-4, %rcx
	jne	.LBB3_14
.LBB3_15:                               # %.preheader377
	cmpl	$0, 80(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_23
# BB#16:                                # %.lr.ph429
	movq	A__align.mseq(%rip), %r8
	movq	A__align.mseq2(%rip), %rdi
	movslq	32(%rsp), %rax          # 4-byte Folded Reload
	movslq	48(%rsp), %r10          # 4-byte Folded Reload
	movl	80(%rsp), %ecx          # 4-byte Reload
	leaq	-1(%rcx), %r9
	movq	%rcx, %rdx
	andq	$3, %rdx
	je	.LBB3_19
# BB#17:                                # %.prol.preheader892
	leaq	(%r8,%r10,8), %rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_18:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rbx,8), %rbp
	movq	%rbp, (%rdi,%rbx,8)
	movq	(%r13,%rbx,8), %rbp
	movb	$0, (%rbp,%rax)
	incq	%rbx
	cmpq	%rbx, %rdx
	jne	.LBB3_18
	jmp	.LBB3_20
.LBB3_19:
	xorl	%ebx, %ebx
.LBB3_20:                               # %.prol.loopexit893
	cmpq	$3, %r9
	jb	.LBB3_23
# BB#21:                                # %.lr.ph429.new
	subq	%rbx, %rcx
	leaq	24(%r13,%rbx,8), %rsi
	leaq	24(%rdi,%rbx,8), %rdi
	addq	%rbx, %r10
	leaq	24(%r8,%r10,8), %rdx
	.p2align	4, 0x90
.LBB3_22:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rbp
	movq	%rbp, -24(%rdi)
	movq	-24(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	-16(%rdx), %rbp
	movq	%rbp, -16(%rdi)
	movq	-16(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	-8(%rdx), %rbp
	movq	%rbp, -8(%rdi)
	movq	-8(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	(%rdx), %rbp
	movq	%rbp, (%rdi)
	movq	(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB3_22
.LBB3_23:                               # %._crit_edge430
	movl	commonAlloc1(%rip), %ebp
	cmpl	%ebp, %r14d
	movl	commonAlloc2(%rip), %ebx
	jg	.LBB3_26
# BB#24:                                # %._crit_edge430
	cmpl	%ebx, %r12d
	jg	.LBB3_26
# BB#25:                                # %._crit_edge503
	movq	commonIP(%rip), %rax
	jmp	.LBB3_30
.LBB3_26:                               # %._crit_edge430._crit_edge
	testl	%ebp, %ebp
	je	.LBB3_29
# BB#27:                                # %._crit_edge430._crit_edge
	testl	%ebx, %ebx
	je	.LBB3_29
# BB#28:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movl	A__align.orlgth1(%rip), %r14d
	movl	commonAlloc1(%rip), %ebp
	movl	A__align.orlgth2(%rip), %r12d
	movl	commonAlloc2(%rip), %ebx
.LBB3_29:
	cmpl	%ebp, %r14d
	cmovgel	%r14d, %ebp
	cmpl	%ebx, %r12d
	cmovgel	%r12d, %ebx
	leal	10(%rbp), %edi
	leal	10(%rbx), %esi
	callq	AllocateIntMtx
	movq	%rax, commonIP(%rip)
	movl	%ebp, commonAlloc1(%rip)
	movl	%ebx, commonAlloc2(%rip)
.LBB3_30:
	movq	80(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rax, A__align.ijp(%rip)
	movq	A__align.cpmx1(%rip), %rsi
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	movq	%rbx, %rdx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	48(%rsp), %r15          # 8-byte Reload
	movl	%r15d, %r8d
	callq	cpmx_calc_new
	movq	A__align.cpmx2(%rip), %rsi
	movq	%r13, %rdi
	movq	%rbp, %rdx
	movq	32(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %ecx
	movl	%r14d, %r8d
	callq	cpmx_calc_new
	movq	392(%rsp), %r9
	testq	%r9, %r9
	movq	A__align.ogcp1(%rip), %rdi
	movl	%r15d, %esi
	movq	%r12, %rdx
	je	.LBB3_32
# BB#31:
	movq	%rbx, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	new_OpeningGapCount
	movq	A__align.ogcp2(%rip), %rdi
	movl	%r14d, %esi
	movq	%r13, %rdx
	movq	%r13, %rbx
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%r13, %rcx
	movl	%ebp, %r8d
	movq	400(%rsp), %r9
	callq	new_OpeningGapCount
	movq	A__align.fgcp1(%rip), %rdi
	movl	%r15d, %esi
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%r12, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%r15d, %r8d
	movq	408(%rsp), %r9
	callq	new_FinalGapCount
	movq	A__align.fgcp2(%rip), %rdi
	movl	%r14d, %esi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	movl	%ebp, %r8d
	movq	416(%rsp), %r9
	callq	new_FinalGapCount
	jmp	.LBB3_33
.LBB3_32:
	movq	%rbx, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	st_OpeningGapCount
	movq	A__align.ogcp2(%rip), %rdi
	movl	%r14d, %esi
	movq	%r13, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ebp, %r8d
	callq	st_OpeningGapCount
	movq	A__align.fgcp1(%rip), %rdi
	movl	%r15d, %esi
	movq	%r12, %rdx
	movq	%rbx, %rcx
	movq	16(%rsp), %r15          # 8-byte Reload
	movl	%r15d, %r8d
	callq	st_FinalGapCount
	movq	A__align.fgcp2(%rip), %rdi
	movl	%r14d, %esi
	movq	%r13, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ebp, %r8d
	callq	st_FinalGapCount
.LBB3_33:                               # %.preheader376
	movq	%rbp, %r13
	movss	56(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	testl	%r15d, %r15d
	jle	.LBB3_41
# BB#34:                                # %.lr.ph427
	movq	A__align.ogcp1(%rip), %r8
	cvtss2sd	%xmm7, %xmm0
	movq	A__align.fgcp1(%rip), %rbx
	movl	%r15d, %eax
	cmpq	$3, %rax
	jbe	.LBB3_38
# BB#35:                                # %min.iters.checked
	movl	%r15d, %edi
	andl	$3, %edi
	movq	%rax, %rbp
	subq	%rdi, %rbp
	je	.LBB3_38
# BB#36:                                # %vector.memcheck
	leaq	(%rbx,%rax,4), %rcx
	cmpq	%rcx, %r8
	jae	.LBB3_179
# BB#37:                                # %vector.memcheck
	leaq	(%r8,%rax,4), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB3_179
.LBB3_38:
	xorl	%ebp, %ebp
.LBB3_39:                               # %scalar.ph.preheader
	leaq	(%r8,%rbp,4), %rcx
	leaq	(%rbx,%rbp,4), %rdx
	subq	%rbp, %rax
	movsd	.LCPI3_3(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI3_4(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB3_40:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rcx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rcx)
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rdx)
	addq	$4, %rcx
	addq	$4, %rdx
	decq	%rax
	jne	.LBB3_40
.LBB3_41:                               # %.preheader375
	movq	376(%rsp), %r14
	testl	%r13d, %r13d
	jle	.LBB3_49
# BB#42:                                # %.lr.ph424
	movq	A__align.ogcp2(%rip), %r8
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm7, %xmm0
	movq	A__align.fgcp2(%rip), %rbx
	movl	%r13d, %eax
	cmpq	$3, %rax
	jbe	.LBB3_46
# BB#43:                                # %min.iters.checked524
	movl	%r13d, %edi
	andl	$3, %edi
	movq	%rax, %rbp
	subq	%rdi, %rbp
	je	.LBB3_46
# BB#44:                                # %vector.memcheck537
	leaq	(%rbx,%rax,4), %rcx
	cmpq	%rcx, %r8
	jae	.LBB3_182
# BB#45:                                # %vector.memcheck537
	leaq	(%r8,%rax,4), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB3_182
.LBB3_46:
	xorl	%ebp, %ebp
.LBB3_47:                               # %scalar.ph522.preheader
	leaq	(%r8,%rbp,4), %rcx
	leaq	(%rbx,%rbp,4), %rdx
	subq	%rbp, %rax
	movsd	.LCPI3_3(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI3_4(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB3_48:                               # %scalar.ph522
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rcx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rcx)
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rdx)
	addq	$4, %rcx
	addq	$4, %rdx
	decq	%rax
	jne	.LBB3_48
.LBB3_49:                               # %._crit_edge425
	movq	A__align.w1(%rip), %rbx
	movq	A__align.w2(%rip), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	A__align.initverticalw(%rip), %rdi
	movq	A__align.cpmx2(%rip), %rsi
	movq	A__align.cpmx1(%rip), %rdx
	movq	A__align.floatwork(%rip), %r9
	movl	$0, %ecx
	movl	%r15d, %r8d
	pushq	$1
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	A__align.intwork(%rip)
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi36:
	.cfi_adjust_cfa_offset -16
	testq	%r14, %r14
	je	.LBB3_55
# BB#50:
	testl	%r15d, %r15d
	jle	.LBB3_59
# BB#51:                                # %.lr.ph.i
	movq	A__align.initverticalw(%rip), %rax
	movq	impmtx(%rip), %r8
	movl	%r15d, %ecx
	leaq	-1(%rcx), %r9
	movq	%r15, %rbp
	andq	$3, %rbp
	je	.LBB3_56
# BB#52:                                # %.prol.preheader887
	negq	%rbp
	xorl	%edi, %edi
	movq	%r8, %rdx
	.p2align	4, 0x90
.LBB3_53:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	leaq	4(%rax), %rax
	decq	%rdi
	addq	$8, %rdx
	cmpq	%rdi, %rbp
	jne	.LBB3_53
# BB#54:                                # %.prol.loopexit888.unr-lcssa
	negq	%rdi
	cmpq	$3, %r9
	jae	.LBB3_57
	jmp	.LBB3_59
.LBB3_55:                               # %.critedge
	movq	A__align.cpmx1(%rip), %rsi
	movq	A__align.cpmx2(%rip), %rdx
	movq	A__align.floatwork(%rip), %r9
	movl	$0, %ecx
	movq	%rbx, %rdi
	movl	%r13d, %r8d
	pushq	$1
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	pushq	A__align.intwork(%rip)
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi39:
	.cfi_adjust_cfa_offset -16
	cmpl	$1, outgap(%rip)
	je	.LBB3_71
	jmp	.LBB3_79
.LBB3_56:
	xorl	%edi, %edi
	cmpq	$3, %r9
	jb	.LBB3_59
.LBB3_57:                               # %.lr.ph.i.new
	subq	%rdi, %rcx
	leaq	24(%r8,%rdi,8), %rdx
	.p2align	4, 0x90
.LBB3_58:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	movq	-16(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	4(%rax), %xmm0
	movss	%xmm0, 4(%rax)
	movq	-8(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	8(%rax), %xmm0
	movss	%xmm0, 8(%rax)
	movq	(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	12(%rax), %xmm0
	movss	%xmm0, 12(%rax)
	addq	$32, %rdx
	addq	$16, %rax
	addq	$-4, %rcx
	jne	.LBB3_58
.LBB3_59:                               # %imp_match_out_vead_tate.exit
	movq	A__align.cpmx1(%rip), %rsi
	movq	A__align.cpmx2(%rip), %rdx
	movq	A__align.floatwork(%rip), %r9
	movl	$0, %ecx
	movq	%rbx, %rdi
	movl	%r13d, %r8d
	pushq	$1
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	pushq	A__align.intwork(%rip)
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi42:
	.cfi_adjust_cfa_offset -16
	testl	%r13d, %r13d
	je	.LBB3_70
# BB#60:                                # %.lr.ph.preheader.i
	movq	impmtx(%rip), %rax
	movq	(%rax), %rsi
	leal	-1(%r13), %edi
	incq	%rdi
	cmpq	$8, %rdi
	jb	.LBB3_64
# BB#61:                                # %min.iters.checked554
	movl	%r13d, %r8d
	andl	$7, %r8d
	subq	%r8, %rdi
	je	.LBB3_64
# BB#62:                                # %vector.memcheck567
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	addl	%r13d, %eax
	leaq	4(%rsi,%rax,4), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB3_208
# BB#63:                                # %vector.memcheck567
	leaq	4(%rbx,%rax,4), %rax
	cmpq	%rax, %rsi
	jae	.LBB3_208
.LBB3_64:
	movq	%rsi, %rax
	movl	%r13d, %ecx
	movq	%rbx, %rdx
.LBB3_65:                               # %.lr.ph.i346.preheader
	leal	-1(%rcx), %esi
	movl	%ecx, %edi
	andl	$3, %edi
	je	.LBB3_68
# BB#66:                                # %.lr.ph.i346.prol.preheader
	negl	%edi
	.p2align	4, 0x90
.LBB3_67:                               # %.lr.ph.i346.prol
                                        # =>This Inner Loop Header: Depth=1
	decl	%ecx
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rax
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	leaq	4(%rdx), %rdx
	incl	%edi
	jne	.LBB3_67
.LBB3_68:                               # %.lr.ph.i346.prol.loopexit
	cmpl	$3, %esi
	jb	.LBB3_70
	.p2align	4, 0x90
.LBB3_69:                               # %.lr.ph.i346
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	4(%rdx), %xmm0
	movss	%xmm0, 4(%rdx)
	movss	8(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	8(%rdx), %xmm0
	movss	%xmm0, 8(%rdx)
	addl	$-4, %ecx
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	leaq	16(%rax), %rax
	addss	12(%rdx), %xmm0
	movss	%xmm0, 12(%rdx)
	leaq	16(%rdx), %rdx
	jne	.LBB3_69
.LBB3_70:                               # %imp_match_out_vead.exit
	cmpl	$1, outgap(%rip)
	jne	.LBB3_79
.LBB3_71:                               # %.preheader371
	movq	%r15, %r12
	testl	%r15d, %r15d
	jle	.LBB3_88
# BB#72:                                # %.lr.ph418
	movq	A__align.ogcp1(%rip), %rax
	movq	A__align.fgcp1(%rip), %r8
	movq	A__align.initverticalw(%rip), %r9
	movq	%r12, %rdx
	leaq	1(%rdx), %r15
	movl	%r15d, %ecx
	leaq	-1(%rcx), %r11
	cmpq	$7, %r11
	jbe	.LBB3_82
# BB#73:                                # %min.iters.checked592
	movl	%edx, %r10d
	andl	$7, %r10d
	movq	%r11, %rdi
	subq	%r10, %rdi
	je	.LBB3_82
# BB#74:                                # %vector.memcheck611
	leaq	4(%r9), %rbp
	leaq	(%r9,%rcx,4), %rsi
	leaq	-4(%r8,%rcx,4), %r12
	cmpq	%rax, %rbp
	sbbb	%r14b, %r14b
	cmpq	%rsi, %rax
	sbbb	%dl, %dl
	andb	%r14b, %dl
	cmpq	%r12, %rbp
	sbbb	%r14b, %r14b
	cmpq	%rsi, %r8
	sbbb	%bpl, %bpl
	movl	$1, %esi
	testb	$1, %dl
	jne	.LBB3_297
# BB#75:                                # %vector.memcheck611
	andb	%bpl, %r14b
	andb	$1, %r14b
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	376(%rsp), %r14
	jne	.LBB3_83
# BB#76:                                # %vector.body588.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r8), %rbp
	leaq	20(%r9), %rdx
	leaq	1(%rdi), %rsi
	.p2align	4, 0x90
.LBB3_77:                               # %vector.body588
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbp), %xmm1
	movups	(%rbp), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rdx), %xmm3
	movups	(%rdx), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rdx)
	movups	%xmm4, (%rdx)
	addq	$32, %rbp
	addq	$32, %rdx
	addq	$-8, %rdi
	jne	.LBB3_77
# BB#78:                                # %middle.block589
	testq	%r10, %r10
	jne	.LBB3_83
	jmp	.LBB3_88
.LBB3_79:                               # %.preheader374
	testl	%r13d, %r13d
	jle	.LBB3_106
# BB#80:                                # %.lr.ph422
	movslq	offset(%rip), %r8
	leaq	1(%r13), %rax
	movl	%eax, %ecx
	testb	$1, %al
	jne	.LBB3_103
# BB#81:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r8d, %xmm0
	mulsd	.LCPI3_5(%rip), %xmm0
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rbx)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB3_104
	jmp	.LBB3_106
.LBB3_82:
	movl	$1, %esi
.LBB3_83:                               # %scalar.ph590.preheader
	subl	%esi, %r15d
	testb	$1, %r15b
	movq	%rsi, %rdi
	je	.LBB3_85
# BB#84:                                # %scalar.ph590.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%r8,%rsi,4), %xmm0
	addss	(%r9,%rsi,4), %xmm0
	movss	%xmm0, (%r9,%rsi,4)
	leaq	1(%rsi), %rdi
.LBB3_85:                               # %scalar.ph590.prol.loopexit
	cmpq	%rsi, %r11
	je	.LBB3_88
# BB#86:                                # %scalar.ph590.preheader.new
	subq	%rdi, %rcx
	leaq	(%r8,%rdi,4), %rdx
	leaq	4(%r9,%rdi,4), %rsi
	.p2align	4, 0x90
.LBB3_87:                               # %scalar.ph590
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB3_87
.LBB3_88:                               # %.preheader370
	testl	%r13d, %r13d
	jle	.LBB3_102
# BB#89:                                # %.lr.ph416
	movq	A__align.ogcp2(%rip), %rax
	movq	A__align.fgcp2(%rip), %r8
	leaq	1(%r13), %r11
	movl	%r11d, %ecx
	leaq	-1(%rcx), %r10
	cmpq	$7, %r10
	movq	%r12, %r15
	jbe	.LBB3_96
# BB#90:                                # %min.iters.checked631
	movl	%r13d, %r9d
	andl	$7, %r9d
	movq	%r10, %rsi
	subq	%r9, %rsi
	je	.LBB3_96
# BB#91:                                # %vector.memcheck652
	movq	%r14, %r12
	leaq	4(%rbx), %rdx
	leaq	(%rbx,%rcx,4), %rbp
	leaq	-4(%r8,%rcx,4), %rdi
	cmpq	%rax, %rdx
	sbbb	%r14b, %r14b
	cmpq	%rbp, %rax
	sbbb	%r15b, %r15b
	andb	%r14b, %r15b
	cmpq	%rdi, %rdx
	sbbb	%dil, %dil
	cmpq	%rbp, %r8
	sbbb	%bpl, %bpl
	movl	$1, %edx
	testb	$1, %r15b
	jne	.LBB3_298
# BB#92:                                # %vector.memcheck652
	andb	%bpl, %dil
	andb	$1, %dil
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%r12, %r14
	jne	.LBB3_97
# BB#93:                                # %vector.body627.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r8), %rbp
	leaq	20(%rbx), %rdi
	leaq	1(%rsi), %rdx
	.p2align	4, 0x90
.LBB3_94:                               # %vector.body627
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbp), %xmm1
	movups	(%rbp), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rdi), %xmm3
	movups	(%rdi), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rdi)
	movups	%xmm4, (%rdi)
	addq	$32, %rbp
	addq	$32, %rdi
	addq	$-8, %rsi
	jne	.LBB3_94
# BB#95:                                # %middle.block628
	testq	%r9, %r9
	jne	.LBB3_97
	jmp	.LBB3_112
.LBB3_96:
	movl	$1, %edx
.LBB3_97:                               # %scalar.ph629.preheader
	subl	%edx, %r11d
	testb	$1, %r11b
	movq	%rdx, %rsi
	je	.LBB3_99
# BB#98:                                # %scalar.ph629.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%r8,%rdx,4), %xmm0
	addss	(%rbx,%rdx,4), %xmm0
	movss	%xmm0, (%rbx,%rdx,4)
	leaq	1(%rdx), %rsi
.LBB3_99:                               # %scalar.ph629.prol.loopexit
	cmpq	%rdx, %r10
	je	.LBB3_112
# BB#100:                               # %scalar.ph629.preheader.new
	subq	%rsi, %rcx
	leaq	(%r8,%rsi,4), %rdx
	leaq	4(%rbx,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB3_101:                              # %scalar.ph629
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB3_101
	jmp	.LBB3_112
.LBB3_102:
	movb	$1, 40(%rsp)            # 1-byte Folded Spill
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r12, %r15
	jmp	.LBB3_130
.LBB3_103:
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB3_106
.LBB3_104:                              # %.lr.ph422.new
	subq	%rdx, %rcx
	leaq	4(%rbx,%rdx,4), %rsi
	leaq	1(%rdx), %rdi
	imull	%r8d, %edx
	imull	%r8d, %edi
	addl	%r8d, %r8d
	xorl	%eax, %eax
	movsd	.LCPI3_4(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB3_105:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rax), %ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rax), %ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%r8d, %eax
	addq	$-2, %rcx
	jne	.LBB3_105
.LBB3_106:                              # %.preheader372
	testl	%r15d, %r15d
	jle	.LBB3_112
# BB#107:                               # %.lr.ph420
	movq	A__align.initverticalw(%rip), %rax
	movslq	offset(%rip), %r8
	leaq	1(%r15), %rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB3_109
# BB#108:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r8d, %xmm0
	mulsd	.LCPI3_5(%rip), %xmm0
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rax)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB3_110
	jmp	.LBB3_112
.LBB3_109:
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB3_112
.LBB3_110:                              # %.lr.ph420.new
	subq	%rdx, %rcx
	leaq	4(%rax,%rdx,4), %rsi
	leaq	1(%rdx), %rdi
	imull	%r8d, %edx
	imull	%r8d, %edi
	addl	%r8d, %r8d
	xorl	%eax, %eax
	movsd	.LCPI3_4(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB3_111:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rax), %ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rax), %ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%r8d, %eax
	addq	$-2, %rcx
	jne	.LBB3_111
.LBB3_112:                              # %.preheader369
	testl	%r13d, %r13d
	setle	40(%rsp)                # 1-byte Folded Spill
	jle	.LBB3_128
# BB#113:                               # %.lr.ph413
	movq	%r15, %r8
	movq	A__align.ogcp1(%rip), %rdx
	leaq	4(%rdx), %rax
	movq	A__align.m(%rip), %r9
	movq	A__align.mp(%rip), %rbp
	leaq	1(%r13), %r15
	movl	%r15d, %ecx
	leaq	-1(%rcx), %r11
	cmpq	$7, %r11
	jbe	.LBB3_121
# BB#114:                               # %min.iters.checked672
	movl	%r13d, %r10d
	andl	$7, %r10d
	movq	%r11, %rdi
	subq	%r10, %rdi
	je	.LBB3_121
# BB#115:                               # %vector.memcheck693
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	leaq	4(%r9), %r14
	leaq	(%r9,%rcx,4), %rbp
	leaq	-4(%rbx,%rcx,4), %rsi
	cmpq	%rsi, %r14
	sbbb	%sil, %sil
	cmpq	%rbp, %rbx
	sbbb	%r14b, %r14b
	andb	%sil, %r14b
	cmpq	%rdx, %r9
	sbbb	%dl, %dl
	cmpq	%rbp, %rax
	sbbb	%sil, %sil
	movl	$1, %r12d
	testb	$1, %r14b
	jne	.LBB3_296
# BB#116:                               # %vector.memcheck693
	andb	%sil, %dl
	andb	$1, %dl
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	376(%rsp), %r14
	movq	24(%rsp), %rbp          # 8-byte Reload
	jne	.LBB3_122
# BB#117:                               # %vector.body668.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%rbx), %rsi
	movq	%rbp, %rdx
	leaq	20(%r9), %rbp
	leaq	20(%rdx), %rdx
	xorps	%xmm1, %xmm1
	leaq	1(%rdi), %r12
	.p2align	4, 0x90
.LBB3_118:                              # %vector.body668
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm2
	movups	(%rsi), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm0, %xmm3
	movups	%xmm2, -16(%rbp)
	movups	%xmm3, (%rbp)
	movups	%xmm1, -16(%rdx)
	movups	%xmm1, (%rdx)
	addq	$32, %rsi
	addq	$32, %rbp
	addq	$32, %rdx
	addq	$-8, %rdi
	jne	.LBB3_118
# BB#119:                               # %middle.block669
	testq	%r10, %r10
	movq	24(%rsp), %rbp          # 8-byte Reload
	jne	.LBB3_122
# BB#120:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r8, %r15
	jmp	.LBB3_130
.LBB3_121:
	movl	$1, %r12d
.LBB3_122:                              # %scalar.ph670.preheader
	subl	%r12d, %r15d
	testb	$1, %r15b
	movq	%r12, %rdi
	je	.LBB3_124
# BB#123:                               # %scalar.ph670.prol
	movss	-4(%rbx,%r12,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%r9,%r12,4)
	movl	$0, (%rbp,%r12,4)
	leaq	1(%r12), %rdi
.LBB3_124:                              # %scalar.ph670.prol.loopexit
	cmpq	%r12, %r11
	movq	%r8, %r15
	je	.LBB3_129
# BB#125:                               # %scalar.ph670.preheader.new
	subq	%rdi, %rcx
	leaq	(%rbx,%rdi,4), %rdx
	leaq	4(%r9,%rdi,4), %rsi
	leaq	4(%rbp,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB3_126:                              # %scalar.ph670
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, -4(%rsi)
	movl	$0, -4(%rdi)
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%rsi)
	movl	$0, (%rdi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rcx
	jne	.LBB3_126
	jmp	.LBB3_129
.LBB3_128:
	movb	$1, 40(%rsp)            # 1-byte Folded Spill
.LBB3_129:                              # %._crit_edge414
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB3_130:                              # %._crit_edge414
	movabsq	$-4294967296, %r8       # imm = 0xFFFFFFFF00000000
	xorpd	%xmm5, %xmm5
	testl	%r13d, %r13d
	xorps	%xmm0, %xmm0
	je	.LBB3_132
# BB#131:
	movq	%r13, %rcx
	shlq	$32, %rcx
	addq	%r8, %rcx
	sarq	$30, %rcx
	movss	(%rbx,%rcx), %xmm0      # xmm0 = mem[0],zero,zero,zero
.LBB3_132:
	movq	A__align.lastverticalw(%rip), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movss	%xmm0, (%rcx)
	movl	outgap(%rip), %edx
	cmpl	$1, %edx
	movl	%r15d, %ecx
	sbbl	$-1, %ecx
	cmpl	$2, %ecx
	jl	.LBB3_175
# BB#133:                               # %.lr.ph406
	movl	%edx, 160(%rsp)         # 4-byte Spill
	testl	%r13d, %r13d
	sete	%dl
	movq	A__align.initverticalw(%rip), %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	A__align.cpmx1(%rip), %rsi
	movq	A__align.floatwork(%rip), %rbp
	movq	%rbp, 152(%rsp)         # 8-byte Spill
	movq	A__align.intwork(%rip), %rbp
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	testq	%r14, %r14
	sete	%al
	orb	%dl, %al
	movb	%al, 136(%rsp)          # 1-byte Spill
	movq	A__align.ogcp2(%rip), %r14
	movq	A__align.ijp(%rip), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	%r13, %rax
	shlq	$32, %rax
	addq	%r8, %rax
	sarq	$32, %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	%ecx, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leal	-1(%r13), %r11d
	leaq	1(%r11), %rax
	movabsq	$8589934584, %rcx       # imm = 0x1FFFFFFF8
	movq	%rax, 104(%rsp)         # 8-byte Spill
	andq	%rax, %rcx
	leaq	-8(%rcx), %rax
	shrq	$3, %rax
	movl	%r13d, %edx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	subl	%ecx, %edx
	movl	%edx, 164(%rsp)         # 4-byte Spill
	movq	%rax, 176(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	xorpd	%xmm5, %xmm5
	movq	A__align.m(%rip), %r9
	movq	A__align.mp(%rip), %r12
	movq	A__align.fgcp2(%rip), %r10
	movq	A__align.fgcp1(%rip), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	A__align.ogcp1(%rip), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	impmtx(%rip), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movl	$1, %r13d
	jmp	.LBB3_141
.LBB3_134:                              # %vector.body707.preheader
                                        #   in Loop: Header=BB3_141 Depth=1
	cmpq	$0, 168(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_136
# BB#135:                               # %vector.body707.prol
                                        #   in Loop: Header=BB3_141 Depth=1
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	(%r15), %xmm2
	movups	16(%r15), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, (%r15)
	movups	%xmm3, 16(%r15)
	movl	$8, %edi
	cmpq	$0, 176(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_137
	jmp	.LBB3_139
.LBB3_136:                              #   in Loop: Header=BB3_141 Depth=1
	xorl	%edi, %edi
	cmpq	$0, 176(%rsp)           # 8-byte Folded Reload
	je	.LBB3_139
.LBB3_137:                              # %vector.body707.preheader.new
                                        #   in Loop: Header=BB3_141 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	subq	%rdi, %rax
	leaq	48(%r15,%rdi,4), %rdx
	leaq	48(%rcx,%rdi,4), %rbx
	.p2align	4, 0x90
.LBB3_138:                              # %vector.body707
                                        #   Parent Loop BB3_141 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	-48(%rdx), %xmm2
	movups	-32(%rdx), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, -48(%rdx)
	movups	%xmm3, -32(%rdx)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	-16(%rdx), %xmm2
	movups	(%rdx), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, -16(%rdx)
	movups	%xmm3, (%rdx)
	addq	$64, %rdx
	addq	$64, %rbx
	addq	$-16, %rax
	jne	.LBB3_138
.LBB3_139:                              # %middle.block708
                                        #   in Loop: Header=BB3_141 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, 104(%rsp)         # 8-byte Folded Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	je	.LBB3_161
# BB#140:                               #   in Loop: Header=BB3_141 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	leaq	(%r15,%rax,4), %rbx
	movl	164(%rsp), %eax         # 4-byte Reload
	movl	%eax, %edx
	jmp	.LBB3_156
	.p2align	4, 0x90
.LBB3_141:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_142 Depth 2
                                        #       Child Loop BB3_143 Depth 3
                                        #     Child Loop BB3_146 Depth 2
                                        #       Child Loop BB3_148 Depth 3
                                        #     Child Loop BB3_138 Depth 2
                                        #     Child Loop BB3_158 Depth 2
                                        #     Child Loop BB3_160 Depth 2
                                        #     Child Loop BB3_164 Depth 2
	movq	%rdi, %r15
	leaq	-1(%r13), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	-4(%rax,%r13,4), %eax
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	%eax, (%rbx)
	movl	$n_dis_consweight_multi, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_142:                              #   Parent Loop BB3_141 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_143 Depth 3
	movl	$0, 208(%rsp,%rcx,4)
	xorps	%xmm0, %xmm0
	movl	$1, %edx
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB3_143:                              #   Parent Loop BB3_141 Depth=1
                                        #     Parent Loop BB3_142 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-8(%rsi,%rdx,8), %rbp
	movss	(%rdi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rbp,%r13,4), %xmm1
	addss	%xmm0, %xmm1
	movss	104(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movq	(%rsi,%rdx,8), %rbp
	mulss	(%rbp,%r13,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rdi
	addq	$2, %rdx
	cmpq	$27, %rdx
	jne	.LBB3_143
# BB#144:                               #   in Loop: Header=BB3_142 Depth=2
	movss	%xmm0, 208(%rsp,%rcx,4)
	incq	%rcx
	addq	$4, %rax
	cmpq	$26, %rcx
	jne	.LBB3_142
# BB#145:                               # %.preheader.i353
                                        #   in Loop: Header=BB3_141 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	movl	%eax, %r8d
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	152(%rsp), %rbx         # 8-byte Reload
	movq	%r15, %rax
	je	.LBB3_150
	.p2align	4, 0x90
.LBB3_146:                              # %.lr.ph84.i
                                        #   Parent Loop BB3_141 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_148 Depth 3
	decl	%r8d
	movl	$0, (%rax)
	movq	(%rdx), %rbp
	movl	(%rbp), %ecx
	testl	%ecx, %ecx
	js	.LBB3_149
# BB#147:                               # %.lr.ph.preheader.i357
                                        #   in Loop: Header=BB3_146 Depth=2
	movq	(%rbx), %rdi
	addq	$4, %rbp
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB3_148:                              # %.lr.ph.i358
                                        #   Parent Loop BB3_141 Depth=1
                                        #     Parent Loop BB3_146 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ecx, %rcx
	movss	208(%rsp,%rcx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi), %xmm1
	addq	$4, %rdi
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rax)
	movl	(%rbp), %ecx
	addq	$4, %rbp
	testl	%ecx, %ecx
	jns	.LBB3_148
.LBB3_149:                              # %._crit_edge.i
                                        #   in Loop: Header=BB3_146 Depth=2
	addq	$8, %rdx
	addq	$8, %rbx
	addq	$4, %rax
	testl	%r8d, %r8d
	jne	.LBB3_146
.LBB3_150:                              # %match_calc.exit
                                        #   in Loop: Header=BB3_141 Depth=1
	cmpb	$0, 136(%rsp)           # 1-byte Folded Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	jne	.LBB3_161
# BB#151:                               # %.lr.ph.preheader.i359
                                        #   in Loop: Header=BB3_141 Depth=1
	movq	104(%rsp), %rdx         # 8-byte Reload
	cmpq	$8, %rdx
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r13,8), %rcx
	jb	.LBB3_155
# BB#152:                               # %min.iters.checked711
                                        #   in Loop: Header=BB3_141 Depth=1
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	je	.LBB3_155
# BB#153:                               # %vector.memcheck725
                                        #   in Loop: Header=BB3_141 Depth=1
	leaq	(%rcx,%rdx,4), %rax
	cmpq	%rax, %r15
	jae	.LBB3_134
# BB#154:                               # %vector.memcheck725
                                        #   in Loop: Header=BB3_141 Depth=1
	leaq	(%r15,%rdx,4), %rax
	cmpq	%rax, %rcx
	jae	.LBB3_134
	.p2align	4, 0x90
.LBB3_155:                              #   in Loop: Header=BB3_141 Depth=1
	movq	32(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	movq	%r15, %rbx
.LBB3_156:                              # %.lr.ph.i363.preheader
                                        #   in Loop: Header=BB3_141 Depth=1
	leal	-1(%rdx), %eax
	movl	%edx, %ebp
	andl	$3, %ebp
	je	.LBB3_159
# BB#157:                               # %.lr.ph.i363.prol.preheader
                                        #   in Loop: Header=BB3_141 Depth=1
	negl	%ebp
	.p2align	4, 0x90
.LBB3_158:                              # %.lr.ph.i363.prol
                                        #   Parent Loop BB3_141 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%edx
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rcx
	addss	(%rbx), %xmm0
	movss	%xmm0, (%rbx)
	leaq	4(%rbx), %rbx
	incl	%ebp
	jne	.LBB3_158
.LBB3_159:                              # %.lr.ph.i363.prol.loopexit
                                        #   in Loop: Header=BB3_141 Depth=1
	cmpl	$3, %eax
	jb	.LBB3_161
	.p2align	4, 0x90
.LBB3_160:                              # %.lr.ph.i363
                                        #   Parent Loop BB3_141 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rbx), %xmm0
	movss	%xmm0, (%rbx)
	movss	4(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	4(%rbx), %xmm0
	movss	%xmm0, 4(%rbx)
	movss	8(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	8(%rbx), %xmm0
	movss	%xmm0, 8(%rbx)
	addl	$-4, %edx
	movss	12(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	leaq	16(%rcx), %rcx
	addss	12(%rbx), %xmm0
	movss	%xmm0, 12(%rbx)
	leaq	16(%rbx), %rbx
	jne	.LBB3_160
.LBB3_161:                              # %imp_match_out_vead.exit365
                                        #   in Loop: Header=BB3_141 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r13,4), %eax
	movl	%eax, (%r15)
	movss	(%rdi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm0
	movss	%xmm0, A__align.mi(%rip)
	xorl	%ecx, %ecx
	cmpb	$0, 40(%rsp)            # 1-byte Folded Reload
	jne	.LBB3_173
# BB#162:                               # %.lr.ph399.preheader
                                        #   in Loop: Header=BB3_141 Depth=1
	movq	200(%rsp), %rax         # 8-byte Reload
	movss	-4(%rax,%r13,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movq	192(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r13,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r13,8), %rdx
	xorl	%ebx, %ebx
	movl	$-1, %r8d
	xorl	%ecx, %ecx
	movq	56(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB3_164
	.p2align	4, 0x90
.LBB3_163:                              # %..lr.ph399_crit_edge
                                        #   in Loop: Header=BB3_164 Depth=2
	movss	4(%rdi,%rbx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	incq	%rbx
	decl	%r8d
.LBB3_164:                              # %.lr.ph399
                                        #   Parent Loop BB3_141 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, 4(%rdx,%rbx,4)
	movss	(%r10,%rbx,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	ucomiss	%xmm3, %xmm4
	movaps	%xmm3, %xmm5
	jbe	.LBB3_166
# BB#165:                               #   in Loop: Header=BB3_164 Depth=2
	leal	(%rcx,%r8), %eax
	movl	%eax, 4(%rdx,%rbx,4)
	movaps	%xmm4, %xmm5
.LBB3_166:                              #   in Loop: Header=BB3_164 Depth=2
	addss	4(%r14,%rbx,4), %xmm3
	ucomiss	%xmm0, %xmm3
	jb	.LBB3_168
# BB#167:                               #   in Loop: Header=BB3_164 Depth=2
	movss	%xmm3, A__align.mi(%rip)
	movaps	%xmm3, %xmm0
	movl	%ebx, %ecx
.LBB3_168:                              #   in Loop: Header=BB3_164 Depth=2
	movss	4(%r9,%rbx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	ucomiss	%xmm5, %xmm4
	jbe	.LBB3_170
# BB#169:                               #   in Loop: Header=BB3_164 Depth=2
	movl	%r13d, %eax
	subl	4(%r12,%rbx,4), %eax
	movl	%eax, 4(%rdx,%rbx,4)
	movaps	%xmm4, %xmm5
.LBB3_170:                              #   in Loop: Header=BB3_164 Depth=2
	movss	(%rdi,%rbx,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm4
	ucomiss	%xmm3, %xmm4
	jb	.LBB3_172
# BB#171:                               #   in Loop: Header=BB3_164 Depth=2
	movss	%xmm4, 4(%r9,%rbx,4)
	movl	%ebp, 4(%r12,%rbx,4)
.LBB3_172:                              #   in Loop: Header=BB3_164 Depth=2
	movss	4(%r15,%rbx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm3
	movss	%xmm3, 4(%r15,%rbx,4)
	cmpl	%ebx, %r11d
	jne	.LBB3_163
.LBB3_173:                              # %._crit_edge400
                                        #   in Loop: Header=BB3_141 Depth=1
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	(%r15,%rax,4), %eax
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	%eax, (%rdx,%r13,4)
	incq	%r13
	cmpq	120(%rsp), %r13         # 8-byte Folded Reload
	movq	%r15, %rbx
	jne	.LBB3_141
# BB#174:                               # %._crit_edge407
	movl	%ecx, A__align.mpi(%rip)
	movq	48(%rsp), %r10          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	376(%rsp), %r14
	movl	160(%rsp), %edx         # 4-byte Reload
	testl	%edx, %edx
	jne	.LBB3_194
	jmp	.LBB3_176
.LBB3_175:
	movq	%r15, %r8
	movq	%rbx, %r15
	movq	48(%rsp), %r10          # 8-byte Reload
	testl	%edx, %edx
	jne	.LBB3_194
.LBB3_176:                              # %.preheader368
	cmpb	$0, 40(%rsp)            # 1-byte Folded Reload
	jne	.LBB3_188
# BB#177:                               # %.lr.ph385
	movslq	offset(%rip), %rax
	movq	%r13, %rdx
	incq	%rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB3_185
# BB#178:
	leal	-1(%r13), %edx
	imull	%eax, %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	mulsd	.LCPI3_5(%rip), %xmm0
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%r15)
	movl	$2, %ebp
	cmpq	$2, %rcx
	jne	.LBB3_186
	jmp	.LBB3_188
.LBB3_179:                              # %vector.ph
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movapd	.LCPI3_1(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI3_2(%rip), %xmm3   # xmm3 = [5.000000e-01,5.000000e-01]
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB3_180:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	8(%rcx), %xmm4
	cvtps2pd	(%rcx), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rcx)
	cvtps2pd	8(%rdx), %xmm4
	cvtps2pd	(%rdx), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rdx)
	addq	$16, %rcx
	addq	$16, %rdx
	addq	$-4, %rsi
	jne	.LBB3_180
# BB#181:                               # %middle.block
	testq	%rdi, %rdi
	jne	.LBB3_39
	jmp	.LBB3_41
.LBB3_182:                              # %vector.ph538
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movapd	.LCPI3_1(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI3_2(%rip), %xmm3   # xmm3 = [5.000000e-01,5.000000e-01]
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB3_183:                              # %vector.body520
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	8(%rcx), %xmm4
	cvtps2pd	(%rcx), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rcx)
	cvtps2pd	8(%rdx), %xmm4
	cvtps2pd	(%rdx), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rdx)
	addq	$16, %rcx
	addq	$16, %rdx
	addq	$-4, %rsi
	jne	.LBB3_183
# BB#184:                               # %middle.block521
	testq	%rdi, %rdi
	jne	.LBB3_47
	jmp	.LBB3_49
.LBB3_185:
	movl	$1, %ebp
	cmpq	$2, %rcx
	je	.LBB3_188
.LBB3_186:                              # %.lr.ph385.new
	subq	%rbp, %rcx
	movl	%r13d, %edx
	subl	%ebp, %edx
	imull	%eax, %edx
	leaq	4(%r15,%rbp,4), %rsi
	leal	-1(%r13), %edi
	subl	%ebp, %edi
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI3_4(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB3_187:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	subl	%eax, %ebp
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB3_187
.LBB3_188:                              # %.preheader367
	testl	%r8d, %r8d
	movq	64(%rsp), %rsi          # 8-byte Reload
	jle	.LBB3_194
# BB#189:                               # %.lr.ph383
	xorps	%xmm0, %xmm0
	cvtsi2sdl	offset(%rip), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r8d, %xmm1
	incq	%r8
	movl	%r8d, %eax
	testb	$1, %r8b
	jne	.LBB3_191
# BB#190:
	movsd	.LCPI3_5(%rip), %xmm2   # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm2, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, 4(%rsi)
	movl	$2, %ecx
	cmpq	$2, %rax
	jne	.LBB3_192
	jmp	.LBB3_194
.LBB3_191:
	movl	$1, %ecx
	cmpq	$2, %rax
	je	.LBB3_194
.LBB3_192:
	movsd	.LCPI3_4(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB3_193:                              # =>This Inner Loop Header: Depth=1
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%ecx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	(%rsi,%rcx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, (%rsi,%rcx,4)
	leal	1(%rcx), %edx
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%edx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	4(%rsi,%rcx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, 4(%rsi,%rcx,4)
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jne	.LBB3_193
.LBB3_194:                              # %.loopexit
	movl	368(%rsp), %ebx
	movq	A__align.mseq1(%rip), %r13
	movq	A__align.mseq2(%rip), %r9
	movq	A__align.ijp(%rip), %rbp
	testq	%r14, %r14
	movss	%xmm5, 128(%rsp)        # 4-byte Spill
	je	.LBB3_205
# BB#195:
	movq	%r9, 120(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	callq	strlen
	movq	%rax, %rbx
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, %r14
	leal	1(%r14,%rbx), %ebp
	movl	%ebp, %edi
	callq	AllocateCharVec
	movq	%rax, %r12
	movl	%ebp, %edi
	callq	AllocateCharVec
	movq	%r14, %r10
	movq	%rbx, %r9
	movq	40(%rsp), %r11          # 8-byte Reload
	cmpl	$1, outgap(%rip)
	je	.LBB3_218
# BB#196:
	movq	64(%rsp), %rbx          # 8-byte Reload
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	testl	%r9d, %r9d
	jle	.LBB3_202
# BB#197:                               # %.lr.ph55.i
	movslq	%r9d, %rbp
	movslq	%r10d, %rcx
	movl	%ebp, %edx
	addq	$4, %rbx
	decq	%rdx
	movl	%r9d, %esi
	movaps	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB3_200
	.p2align	4, 0x90
.LBB3_199:
	movq	(%r11,%rbp,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movaps	%xmm0, %xmm1
	testq	%rdx, %rdx
	je	.LBB3_201
.LBB3_198:                              # %._crit_edge97.i
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rbx
	decq	%rdx
	decl	%esi
	ucomiss	%xmm1, %xmm0
	jae	.LBB3_199
.LBB3_200:
	testq	%rdx, %rdx
	jne	.LBB3_198
.LBB3_201:
	movaps	%xmm1, %xmm0
.LBB3_202:                              # %.preheader9.i
	testl	%r10d, %r10d
	jle	.LBB3_218
# BB#203:                               # %.lr.ph51.i
	movslq	%r9d, %r8
	movslq	%r10d, %rcx
	movl	%ecx, %edx
	testb	$1, %r10b
	jne	.LBB3_206
# BB#204:
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB3_212
	jmp	.LBB3_218
.LBB3_205:
	subq	$8, %rsp
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdi
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	80(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	%r13, %r8
	movq	%rcx, %r14
	movq	88(%rsp), %r13          # 8-byte Reload
	pushq	%r13
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	movq	%r10, %r12
	pushq	%r10
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	callq	Atracking
	addq	$32, %rsp
.Lcfi47:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB3_286
.LBB3_206:
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB3_211
# BB#207:
	movl	$1, %esi
	cmpq	$1, %rdx
	jne	.LBB3_212
	jmp	.LBB3_218
.LBB3_208:                              # %vector.body550.preheader
	leaq	(%rsi,%rdi,4), %rax
	movl	%r13d, %ecx
	subl	%edi, %ecx
	leaq	(%rbx,%rdi,4), %rdx
	leaq	16(%rbx), %rbp
	addq	$16, %rsi
	.p2align	4, 0x90
.LBB3_209:                              # %vector.body550
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	-16(%rbp), %xmm2
	movups	(%rbp), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, -16(%rbp)
	movups	%xmm3, (%rbp)
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB3_209
# BB#210:                               # %middle.block551
	testq	%r8, %r8
	jne	.LBB3_65
	jmp	.LBB3_70
.LBB3_211:
	movl	%r10d, %esi
	negl	%esi
	movq	(%r11,%r8,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movl	$1, %esi
	movaps	%xmm1, %xmm0
	cmpq	$1, %rdx
	je	.LBB3_218
.LBB3_212:                              # %.lr.ph51.i.new
	movl	%r10d, %edi
	negl	%edi
	.p2align	4, 0x90
.LBB3_213:                              # =>This Inner Loop Header: Depth=1
	movss	(%r15,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB3_215
# BB#214:                               #   in Loop: Header=BB3_213 Depth=1
	leal	(%rdi,%rsi), %ebp
	movq	(%r11,%r8,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB3_215:                              #   in Loop: Header=BB3_213 Depth=1
	movss	4(%r15,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB3_217
# BB#216:                               #   in Loop: Header=BB3_213 Depth=1
	leal	1(%rdi,%rsi), %ebp
	movq	(%r11,%r8,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB3_217:                              #   in Loop: Header=BB3_213 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB3_213
.LBB3_218:                              # %.preheader8.i
	testl	%r9d, %r9d
	js	.LBB3_224
# BB#219:                               # %.lr.ph48.preheader.i
	movq	%r9, %rsi
	incq	%rsi
	movl	%esi, %ebp
	leaq	-1(%rbp), %rcx
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB3_221
	.p2align	4, 0x90
.LBB3_220:                              # %.lr.ph48.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r11,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB3_220
.LBB3_221:                              # %.lr.ph48.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB3_224
# BB#222:                               # %.lr.ph48.preheader.i.new
	negq	%rbp
	leaq	4(%rdx,%rbp), %r8
	leaq	56(%r11,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_223:                              # %.lr.ph48.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %ebp
	leal	-3(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-48(%rcx,%rsi,8), %rdi
	leal	-2(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-40(%rcx,%rsi,8), %rdi
	leal	-1(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-32(%rcx,%rsi,8), %rdi
	movl	%ebp, (%rdi)
	movq	-24(%rcx,%rsi,8), %rdi
	leal	1(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-16(%rcx,%rsi,8), %rdi
	leal	2(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-8(%rcx,%rsi,8), %rdi
	leal	3(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	(%rcx,%rsi,8), %rdi
	leal	4(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	leaq	8(%r8,%rsi), %rdi
	addq	$8, %rsi
	cmpq	$4, %rdi
	jne	.LBB3_223
.LBB3_224:                              # %.preheader7.i
	leal	(%r10,%r9), %ecx
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	testl	%r10d, %r10d
	js	.LBB3_233
# BB#225:                               # %.lr.ph45.i
	movq	(%r11), %rcx
	movq	%r10, %rsi
	incq	%rsi
	movl	%esi, %ebp
	cmpq	$7, %rbp
	jbe	.LBB3_230
# BB#226:                               # %min.iters.checked754
	andl	$7, %esi
	movq	%rbp, %rbx
	subq	%rsi, %rbx
	je	.LBB3_230
# BB#227:                               # %vector.body750.preheader
	leaq	16(%rcx), %rdi
	movdqa	.LCPI3_6(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI3_7(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI3_8(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB3_228:                              # %vector.body750
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdi)
	movdqu	%xmm4, (%rdi)
	paddd	%xmm3, %xmm0
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB3_228
# BB#229:                               # %middle.block751
	testq	%rsi, %rsi
	jne	.LBB3_231
	jmp	.LBB3_233
.LBB3_230:
	xorl	%ebx, %ebx
.LBB3_231:                              # %scalar.ph752.preheader
	movl	%ebx, %edx
	notl	%edx
	leaq	(%rcx,%rbx,4), %rcx
	subq	%rbx, %rbp
	.p2align	4, 0x90
.LBB3_232:                              # %scalar.ph752
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rcx)
	decl	%edx
	addq	$4, %rcx
	decq	%rbp
	jne	.LBB3_232
.LBB3_233:                              # %._crit_edge46.i
	movslq	%r9d, %rdx
	movq	%r12, 104(%rsp)         # 8-byte Spill
	leaq	(%r12,%rdx), %r14
	movslq	%r10d, %rcx
	movb	$0, (%rcx,%r14)
	addq	%rcx, %r14
	addq	%rax, %rdx
	leaq	(%rdx,%rcx), %r15
	movb	$0, (%rcx,%rdx)
	movq	384(%rsp), %rcx
	movl	$0, (%rcx)
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	movq	%rax, 112(%rsp)         # 8-byte Spill
	js	.LBB3_279
# BB#234:                               # %.lr.ph37.preheader.i
	xorl	%edx, %edx
	movq	impmtx(%rip), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movdqa	.LCPI3_10(%rip), %xmm0  # xmm0 = [45,0,45,0,45,0,45,0,45,0,45,0,45,0,45,0]
	packuswb	%xmm0, %xmm0
	movdqa	.LCPI3_9(%rip), %xmm1   # xmm1 = [111,0,111,0,111,0,111,0,111,0,111,0,111,0,111,0]
	packuswb	%xmm1, %xmm1
	movl	%r9d, %r8d
	movq	%r10, 144(%rsp)         # 8-byte Spill
                                        # kill: %R10D<def> %R10D<kill> %R10<kill>
	movq	%r9, 152(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_235:                              # %.lr.ph37.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_249 Depth 2
                                        #     Child Loop BB3_253 Depth 2
                                        #     Child Loop BB3_256 Depth 2
                                        #     Child Loop BB3_264 Depth 2
                                        #     Child Loop BB3_268 Depth 2
                                        #     Child Loop BB3_271 Depth 2
	movslq	%r8d, %rcx
	movq	(%r11,%rcx,8), %rax
	movslq	%r10d, %rsi
	movl	(%rax,%rsi,4), %r12d
	testl	%r12d, %r12d
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	js	.LBB3_238
# BB#236:                               #   in Loop: Header=BB3_235 Depth=1
	je	.LBB3_239
# BB#237:                               #   in Loop: Header=BB3_235 Depth=1
	movl	%r8d, %ebx
	subl	%r12d, %ebx
	jmp	.LBB3_240
	.p2align	4, 0x90
.LBB3_238:                              #   in Loop: Header=BB3_235 Depth=1
	leal	-1(%r8), %ebx
	jmp	.LBB3_241
	.p2align	4, 0x90
.LBB3_239:                              #   in Loop: Header=BB3_235 Depth=1
	leal	-1(%r8), %ebx
.LBB3_240:                              #   in Loop: Header=BB3_235 Depth=1
	movl	$-1, %r12d
.LBB3_241:                              #   in Loop: Header=BB3_235 Depth=1
	movl	%r8d, %eax
	subl	%ebx, %eax
	decl	%eax
	je	.LBB3_258
# BB#242:                               # %.lr.ph18.preheader.i
                                        #   in Loop: Header=BB3_235 Depth=1
	leal	-2(%r8), %ecx
	subl	%ebx, %ecx
	movq	%rcx, %r9
	negq	%r9
	leaq	1(%rcx), %rdi
	cmpq	$16, %rdi
	jae	.LBB3_244
# BB#243:                               #   in Loop: Header=BB3_235 Depth=1
	movq	%r15, %rcx
	movq	%r14, %rsi
	jmp	.LBB3_251
	.p2align	4, 0x90
.LBB3_244:                              # %min.iters.checked810
                                        #   in Loop: Header=BB3_235 Depth=1
	movl	%edi, %r11d
	andl	$15, %r11d
	subq	%r11, %rdi
	je	.LBB3_247
# BB#245:                               # %vector.memcheck823
                                        #   in Loop: Header=BB3_235 Depth=1
	leaq	-1(%r14,%r9), %rsi
	cmpq	%r15, %rsi
	jae	.LBB3_248
# BB#246:                               # %vector.memcheck823
                                        #   in Loop: Header=BB3_235 Depth=1
	leaq	-1(%r15,%r9), %rsi
	cmpq	%r14, %rsi
	jae	.LBB3_248
.LBB3_247:                              #   in Loop: Header=BB3_235 Depth=1
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	40(%rsp), %r11          # 8-byte Reload
	jmp	.LBB3_251
.LBB3_248:                              # %vector.body806.preheader
                                        #   in Loop: Header=BB3_235 Depth=1
	movl	%ebx, 56(%rsp)          # 4-byte Spill
	subl	%edi, %eax
	leaq	-1(%r11), %rsi
	subq	%rcx, %rsi
	leaq	(%r15,%rsi), %rcx
	addq	%r14, %rsi
	leaq	-8(%r14), %rbp
	leaq	-8(%r15), %rbx
	.p2align	4, 0x90
.LBB3_249:                              # %vector.body806
                                        #   Parent Loop BB3_235 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm1, (%rbp)
	movq	%xmm1, -8(%rbp)
	movq	%xmm0, (%rbx)
	movq	%xmm0, -8(%rbx)
	addq	$-16, %rbp
	addq	$-16, %rbx
	addq	$-16, %rdi
	jne	.LBB3_249
# BB#250:                               # %middle.block807
                                        #   in Loop: Header=BB3_235 Depth=1
	testq	%r11, %r11
	movq	40(%rsp), %r11          # 8-byte Reload
	movl	56(%rsp), %ebx          # 4-byte Reload
	je	.LBB3_257
	.p2align	4, 0x90
.LBB3_251:                              # %.lr.ph18.i.preheader
                                        #   in Loop: Header=BB3_235 Depth=1
	leal	-1(%rax), %edi
	movl	%eax, %ebp
	andl	$7, %ebp
	je	.LBB3_254
# BB#252:                               # %.lr.ph18.i.prol.preheader
                                        #   in Loop: Header=BB3_235 Depth=1
	negl	%ebp
	.p2align	4, 0x90
.LBB3_253:                              # %.lr.ph18.i.prol
                                        #   Parent Loop BB3_235 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, -1(%rsi)
	decq	%rsi
	movb	$45, -1(%rcx)
	decq	%rcx
	decl	%eax
	incl	%ebp
	jne	.LBB3_253
.LBB3_254:                              # %.lr.ph18.i.prol.loopexit
                                        #   in Loop: Header=BB3_235 Depth=1
	cmpl	$7, %edi
	jb	.LBB3_257
# BB#255:                               # %.lr.ph18.i.preheader.new
                                        #   in Loop: Header=BB3_235 Depth=1
	decq	%rcx
	decq	%rsi
	.p2align	4, 0x90
.LBB3_256:                              # %.lr.ph18.i
                                        #   Parent Loop BB3_235 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, (%rsi)
	movb	$45, (%rcx)
	movb	$111, -1(%rsi)
	movb	$45, -1(%rcx)
	movb	$111, -2(%rsi)
	movb	$45, -2(%rcx)
	movb	$111, -3(%rsi)
	movb	$45, -3(%rcx)
	movb	$111, -4(%rsi)
	movb	$45, -4(%rcx)
	movb	$111, -5(%rsi)
	movb	$45, -5(%rcx)
	movb	$111, -6(%rsi)
	movb	$45, -6(%rcx)
	movb	$111, -7(%rsi)
	movb	$45, -7(%rcx)
	addq	$-8, %rcx
	addq	$-8, %rsi
	addl	$-8, %eax
	jne	.LBB3_256
.LBB3_257:                              # %._crit_edge19.loopexit.i
                                        #   in Loop: Header=BB3_235 Depth=1
	leaq	-1(%r14,%r9), %r14
	leaq	-1(%r15,%r9), %r15
	leal	-1(%rdx,%r8), %edx
	subl	%ebx, %edx
.LBB3_258:                              # %._crit_edge19.i
                                        #   in Loop: Header=BB3_235 Depth=1
	cmpl	$-1, %r12d
	movl	%r12d, 8(%rsp)          # 4-byte Spill
	je	.LBB3_273
# BB#259:                               # %.lr.ph26.preheader.i
                                        #   in Loop: Header=BB3_235 Depth=1
	movl	%ebx, 56(%rsp)          # 4-byte Spill
	movl	%r12d, %eax
	notl	%eax
	movl	$-2, %esi
	subl	%r12d, %esi
	movq	%rsi, %r11
	negq	%r11
	leaq	1(%rsi), %rdi
	cmpq	$16, %rdi
	movl	%eax, %ecx
	movq	%r15, %rbx
	movq	%r14, %rbp
	jb	.LBB3_266
# BB#260:                               # %min.iters.checked772
                                        #   in Loop: Header=BB3_235 Depth=1
	movl	%edi, %r9d
	andl	$15, %r9d
	subq	%r9, %rdi
	movl	%eax, %ecx
	movq	%r15, %rbx
	movq	%r14, %rbp
	je	.LBB3_266
# BB#261:                               # %vector.memcheck785
                                        #   in Loop: Header=BB3_235 Depth=1
	leaq	-1(%r14,%r11), %rcx
	cmpq	%r15, %rcx
	jae	.LBB3_263
# BB#262:                               # %vector.memcheck785
                                        #   in Loop: Header=BB3_235 Depth=1
	leaq	-1(%r15,%r11), %rcx
	cmpq	%r14, %rcx
	movl	%eax, %ecx
	movq	%r15, %rbx
	movq	%r14, %rbp
	jb	.LBB3_266
.LBB3_263:                              # %vector.body768.preheader
                                        #   in Loop: Header=BB3_235 Depth=1
	movl	%eax, %ecx
	subl	%edi, %ecx
	leaq	-1(%r9), %rbp
	subq	%rsi, %rbp
	leaq	(%r15,%rbp), %rbx
	addq	%r14, %rbp
	leaq	-8(%r14), %rsi
	leaq	-8(%r15), %r12
	.p2align	4, 0x90
.LBB3_264:                              # %vector.body768
                                        #   Parent Loop BB3_235 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm0, (%rsi)
	movq	%xmm0, -8(%rsi)
	movq	%xmm1, (%r12)
	movq	%xmm1, -8(%r12)
	addq	$-16, %rsi
	addq	$-16, %r12
	addq	$-16, %rdi
	jne	.LBB3_264
# BB#265:                               # %middle.block769
                                        #   in Loop: Header=BB3_235 Depth=1
	testq	%r9, %r9
	je	.LBB3_272
	.p2align	4, 0x90
.LBB3_266:                              # %.lr.ph26.i.preheader
                                        #   in Loop: Header=BB3_235 Depth=1
	leal	-1(%rcx), %edi
	movl	%ecx, %esi
	andl	$7, %esi
	je	.LBB3_269
# BB#267:                               # %.lr.ph26.i.prol.preheader
                                        #   in Loop: Header=BB3_235 Depth=1
	negl	%esi
	.p2align	4, 0x90
.LBB3_268:                              # %.lr.ph26.i.prol
                                        #   Parent Loop BB3_235 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, -1(%rbp)
	decq	%rbp
	movb	$111, -1(%rbx)
	decq	%rbx
	decl	%ecx
	incl	%esi
	jne	.LBB3_268
.LBB3_269:                              # %.lr.ph26.i.prol.loopexit
                                        #   in Loop: Header=BB3_235 Depth=1
	cmpl	$7, %edi
	jb	.LBB3_272
# BB#270:                               # %.lr.ph26.i.preheader.new
                                        #   in Loop: Header=BB3_235 Depth=1
	decq	%rbx
	decq	%rbp
	.p2align	4, 0x90
.LBB3_271:                              # %.lr.ph26.i
                                        #   Parent Loop BB3_235 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, (%rbp)
	movb	$111, (%rbx)
	movb	$45, -1(%rbp)
	movb	$111, -1(%rbx)
	movb	$45, -2(%rbp)
	movb	$111, -2(%rbx)
	movb	$45, -3(%rbp)
	movb	$111, -3(%rbx)
	movb	$45, -4(%rbp)
	movb	$111, -4(%rbx)
	movb	$45, -5(%rbp)
	movb	$111, -5(%rbx)
	movb	$45, -6(%rbp)
	movb	$111, -6(%rbx)
	movb	$45, -7(%rbp)
	movb	$111, -7(%rbx)
	addq	$-8, %rbx
	addq	$-8, %rbp
	addl	$-8, %ecx
	jne	.LBB3_271
.LBB3_272:                              # %._crit_edge27.loopexit.i
                                        #   in Loop: Header=BB3_235 Depth=1
	leaq	-1(%r14,%r11), %r14
	leaq	-1(%r15,%r11), %r15
	addl	%eax, %edx
	movq	40(%rsp), %r11          # 8-byte Reload
	movl	56(%rsp), %ebx          # 4-byte Reload
.LBB3_273:                              # %._crit_edge27.i
                                        #   in Loop: Header=BB3_235 Depth=1
	movq	152(%rsp), %r9          # 8-byte Reload
	cmpl	%r9d, %r8d
	je	.LBB3_276
# BB#274:                               # %._crit_edge27.i
                                        #   in Loop: Header=BB3_235 Depth=1
	cmpl	144(%rsp), %r10d        # 4-byte Folded Reload
	je	.LBB3_276
# BB#275:                               #   in Loop: Header=BB3_235 Depth=1
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movss	(%rax,%rcx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movq	384(%rsp), %rax
	addss	(%rax), %xmm2
	movss	%xmm2, (%rax)
.LBB3_276:                              #   in Loop: Header=BB3_235 Depth=1
	testl	%r8d, %r8d
	movl	8(%rsp), %eax           # 4-byte Reload
	jle	.LBB3_279
# BB#277:                               #   in Loop: Header=BB3_235 Depth=1
	testl	%r10d, %r10d
	jle	.LBB3_279
# BB#278:                               #   in Loop: Header=BB3_235 Depth=1
	addl	%eax, %r10d
	movb	$111, -1(%r14)
	decq	%r14
	movb	$111, -1(%r15)
	decq	%r15
	addl	$2, %edx
	cmpl	64(%rsp), %edx          # 4-byte Folded Reload
	movl	%ebx, %r8d
	jle	.LBB3_235
.LBB3_279:                              # %._crit_edge38.i
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_282
# BB#280:                               # %.lr.ph13.preheader.i
	movl	48(%rsp), %ebx          # 4-byte Reload
	movq	72(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_281:                              # %.lr.ph13.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	movq	(%rbp), %rsi
	movq	%r14, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %r13
	decq	%rbx
	jne	.LBB3_281
.LBB3_282:                              # %.preheader.i
	movq	80(%rsp), %r13          # 8-byte Reload
	testl	%r13d, %r13d
	movq	88(%rsp), %r14          # 8-byte Reload
	movq	120(%rsp), %r12         # 8-byte Reload
	jle	.LBB3_285
# BB#283:                               # %.lr.ph.preheader.i348
	movl	%r13d, %ebx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB3_284:                              # %.lr.ph.i352
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdi
	movq	(%rbp), %rsi
	movq	%r15, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %r12
	decq	%rbx
	jne	.LBB3_284
.LBB3_285:                              # %Atracking_localhom.exit
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	72(%rsp), %r15          # 8-byte Reload
	movl	368(%rsp), %ebx
	movq	48(%rsp), %r12          # 8-byte Reload
.LBB3_286:
	movq	A__align.mseq1(%rip), %rax
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, %rcx
	cmpl	%ebx, %ecx
	jg	.LBB3_295
# BB#287:
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB3_295
# BB#288:                               # %.preheader366
	testl	%r12d, %r12d
	jle	.LBB3_291
.LBB3_289:                              # %.lr.ph381.preheader
	movl	%r12d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_290:                              # %.lr.ph381
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbp,8), %rdi
	movq	A__align.mseq1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB3_290
.LBB3_291:                              # %.preheader
	testl	%r13d, %r13d
	jle	.LBB3_294
# BB#292:                               # %.lr.ph.preheader
	movl	%r13d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_293:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbp,8), %rdi
	movq	A__align.mseq2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB3_293
.LBB3_294:                              # %._crit_edge
	movss	128(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_295:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
	movl	%ebx, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.1, %edi
	callq	ErrorExit
	testl	%r12d, %r12d
	jg	.LBB3_289
	jmp	.LBB3_291
.LBB3_296:
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	376(%rsp), %r14
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB3_122
.LBB3_297:
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	376(%rsp), %r14
	jmp	.LBB3_83
.LBB3_298:
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%r12, %r14
	jmp	.LBB3_97
.Lfunc_end3:
	.size	A__align, .Lfunc_end3-A__align
	.cfi_endproc

	.p2align	4, 0x90
	.type	match_calc,@function
match_calc:                             # @match_calc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 40
.Lcfi52:
	.cfi_offset %rbx, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	40(%rsp), %r11
	cmpl	$0, 48(%rsp)
	je	.LBB4_10
# BB#1:
	testl	%r8d, %r8d
	jle	.LBB4_10
# BB#2:                                 # %.preheader77.preheader
	movl	%r8d, %r10d
	xorl	%r14d, %r14d
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader77
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB4_4:                                #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rax,8), %rbx
	movss	(%rbx,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB4_5
	jnp	.LBB4_6
.LBB4_5:                                #   in Loop: Header=BB4_4 Depth=2
	movq	(%r9,%r14,8), %rbx
	movslq	%r15d, %r15
	movss	%xmm1, (%rbx,%r15,4)
	movq	(%r11,%r14,8), %rbx
	movl	%eax, (%rbx,%r15,4)
	incl	%r15d
.LBB4_6:                                #   in Loop: Header=BB4_4 Depth=2
	movq	8(%rdx,%rax,8), %rbx
	movss	(%rbx,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB4_7
	jnp	.LBB4_8
.LBB4_7:                                #   in Loop: Header=BB4_4 Depth=2
	movq	(%r9,%r14,8), %rbx
	movslq	%r15d, %r15
	movss	%xmm1, (%rbx,%r15,4)
	movq	(%r11,%r14,8), %rbx
	leal	1(%rax), %ebp
	movl	%ebp, (%rbx,%r15,4)
	incl	%r15d
.LBB4_8:                                #   in Loop: Header=BB4_4 Depth=2
	addq	$2, %rax
	cmpq	$26, %rax
	jne	.LBB4_4
# BB#9:                                 #   in Loop: Header=BB4_3 Depth=1
	movq	(%r11,%r14,8), %rax
	movslq	%r15d, %rbx
	movl	$-1, (%rax,%rbx,4)
	incq	%r14
	cmpq	%r10, %r14
	jne	.LBB4_3
.LBB4_10:                               # %.preheader76
	movl	$n_dis_consweight_multi, %r10d
	movslq	%ecx, %rcx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_12 Depth 2
	movl	$0, -104(%rsp,%r14,4)
	xorps	%xmm0, %xmm0
	movl	$1, %ebx
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB4_12:                               #   Parent Loop BB4_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rsi,%rbx,8), %rdx
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdx,%rcx,4), %xmm1
	addss	%xmm0, %xmm1
	movss	104(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movq	(%rsi,%rbx,8), %rdx
	mulss	(%rdx,%rcx,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rax
	addq	$2, %rbx
	cmpq	$27, %rbx
	jne	.LBB4_12
# BB#13:                                #   in Loop: Header=BB4_11 Depth=1
	movss	%xmm0, -104(%rsp,%r14,4)
	incq	%r14
	addq	$4, %r10
	cmpq	$26, %r14
	jne	.LBB4_11
	jmp	.LBB4_14
	.p2align	4, 0x90
.LBB4_18:                               # %._crit_edge
                                        #   in Loop: Header=BB4_14 Depth=1
	addq	$8, %r11
	addq	$8, %r9
	addq	$4, %rdi
.LBB4_14:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_17 Depth 2
	testl	%r8d, %r8d
	je	.LBB4_19
# BB#15:                                # %.lr.ph84
                                        #   in Loop: Header=BB4_14 Depth=1
	decl	%r8d
	movl	$0, (%rdi)
	movq	(%r11), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	js	.LBB4_18
# BB#16:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_14 Depth=1
	movq	(%r9), %rcx
	addq	$4, %rax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_17:                               # %.lr.ph
                                        #   Parent Loop BB4_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	movss	-104(%rsp,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addq	$4, %rcx
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdi)
	movl	(%rax), %edx
	addq	$4, %rax
	testl	%edx, %edx
	jns	.LBB4_17
	jmp	.LBB4_18
.LBB4_19:                               # %._crit_edge85
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	match_calc, .Lfunc_end4-match_calc
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI5_1:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI5_2:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI5_3:
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
.LCPI5_4:
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.text
	.p2align	4, 0x90
	.type	Atracking,@function
Atracking:                              # @Atracking
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi62:
	.cfi_def_cfa_offset 128
.Lcfi63:
	.cfi_offset %rbx, -56
.Lcfi64:
	.cfi_offset %r12, -48
.Lcfi65:
	.cfi_offset %r13, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movq	%r8, %r14
	movq	%rcx, %rbx
	movq	%rdx, %r13
	movq	%rsi, %r15
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	144(%rsp), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movl	136(%rsp), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movq	128(%rsp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	(%r13), %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%eax, %ebx
	movq	%rbp, (%rsp)            # 8-byte Spill
	leal	1(%rbx,%rbp), %r12d
	movl	%r12d, %edi
	callq	AllocateCharVec
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%r12d, %edi
	callq	AllocateCharVec
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	%rbx, %r10
	movq	48(%rsp), %r9           # 8-byte Reload
	cmpl	$1, outgap(%rip)
	je	.LBB5_1
# BB#12:
	movd	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	(%rsp), %rsi            # 8-byte Reload
	testl	%esi, %esi
	jle	.LBB5_18
# BB#13:                                # %.lr.ph54
	movslq	%esi, %rbp
	movslq	8(%rsp), %rcx           # 4-byte Folded Reload
	movl	%ebp, %edx
	addq	$4, %r15
	decq	%rdx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movdqa	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB5_16
	jmp	.LBB5_15
	.p2align	4, 0x90
.LBB5_85:                               # %._crit_edge96
                                        #   in Loop: Header=BB5_16 Depth=1
	movd	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	decl	%esi
	addq	$4, %r15
	decq	%rdx
	ucomiss	%xmm1, %xmm0
	jb	.LBB5_16
.LBB5_15:
	movq	(%r9,%rbp,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movdqa	%xmm0, %xmm1
.LBB5_16:                               # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	jne	.LBB5_85
# BB#17:
	movdqa	%xmm1, %xmm0
.LBB5_18:                               # %.preheader8
	testl	%r10d, %r10d
	jle	.LBB5_1
# BB#19:                                # %.lr.ph50
	movslq	(%rsp), %r8             # 4-byte Folded Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movslq	%esi, %rcx
	movl	%ecx, %edx
	testb	$1, %sil
	jne	.LBB5_21
# BB#20:
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB5_25
	jmp	.LBB5_1
.LBB5_21:
	movd	(%r11), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB5_23
# BB#22:
	movl	$1, %esi
	cmpq	$1, %rdx
	jne	.LBB5_25
	jmp	.LBB5_1
.LBB5_23:
	movl	%r10d, %esi
	negl	%esi
	movq	(%r9,%r8,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movl	$1, %esi
	movdqa	%xmm1, %xmm0
	cmpq	$1, %rdx
	je	.LBB5_1
.LBB5_25:                               # %.lr.ph50.new
	movl	%r10d, %edi
	negl	%edi
	.p2align	4, 0x90
.LBB5_26:                               # =>This Inner Loop Header: Depth=1
	movd	(%r11,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB5_28
# BB#27:                                #   in Loop: Header=BB5_26 Depth=1
	leal	(%rdi,%rsi), %ebp
	movq	(%r9,%r8,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movdqa	%xmm1, %xmm0
.LBB5_28:                               #   in Loop: Header=BB5_26 Depth=1
	movd	4(%r11,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB5_30
# BB#29:                                #   in Loop: Header=BB5_26 Depth=1
	leal	1(%rdi,%rsi), %ebp
	movq	(%r9,%r8,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movdqa	%xmm1, %xmm0
.LBB5_30:                               #   in Loop: Header=BB5_26 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB5_26
.LBB5_1:                                # %.preheader7
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	js	.LBB5_7
# BB#2:                                 # %.lr.ph47.preheader
	movq	(%rsp), %rsi            # 8-byte Reload
	incq	%rsi
	movl	%esi, %ebp
	leaq	-1(%rbp), %rcx
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB5_4
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph47.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB5_3
.LBB5_4:                                # %.lr.ph47.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB5_7
# BB#5:                                 # %.lr.ph47.preheader.new
	negq	%rbp
	leaq	4(%rdx,%rbp), %r8
	leaq	56(%r9,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_6:                                # %.lr.ph47
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %ebp
	leal	-3(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-48(%rcx,%rsi,8), %rdi
	leal	-2(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-40(%rcx,%rsi,8), %rdi
	leal	-1(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-32(%rcx,%rsi,8), %rdi
	movl	%ebp, (%rdi)
	movq	-24(%rcx,%rsi,8), %rdi
	leal	1(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-16(%rcx,%rsi,8), %rdi
	leal	2(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-8(%rcx,%rsi,8), %rdi
	leal	3(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	(%rcx,%rsi,8), %rdi
	leal	4(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	leaq	8(%r8,%rsi), %rdi
	addq	$8, %rsi
	cmpq	$4, %rdi
	jne	.LBB5_6
.LBB5_7:                                # %.preheader6
	movq	(%rsp), %r8             # 8-byte Reload
	leal	(%r10,%r8), %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	testl	%r10d, %r10d
	js	.LBB5_35
# BB#8:                                 # %.lr.ph44
	movq	(%r9), %rcx
	movq	24(%rsp), %rbx          # 8-byte Reload
	incq	%rbx
	movl	%ebx, %edx
	cmpq	$7, %rdx
	jbe	.LBB5_9
# BB#31:                                # %min.iters.checked
	andl	$7, %ebx
	movq	%rdx, %rbp
	subq	%rbx, %rbp
	je	.LBB5_9
# BB#32:                                # %vector.body.preheader
	leaq	16(%rcx), %rsi
	movdqa	.LCPI5_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI5_1(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI5_2(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rbp, %rdi
	.p2align	4, 0x90
.LBB5_33:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rsi)
	movdqu	%xmm4, (%rsi)
	paddd	%xmm3, %xmm0
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB5_33
# BB#34:                                # %middle.block
	testq	%rbx, %rbx
	jne	.LBB5_10
	jmp	.LBB5_35
.LBB5_9:
	xorl	%ebp, %ebp
.LBB5_10:                               # %scalar.ph.preheader
	movl	%ebp, %esi
	notl	%esi
	leaq	(%rcx,%rbp,4), %rcx
	subq	%rbp, %rdx
	.p2align	4, 0x90
.LBB5_11:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, (%rcx)
	decl	%esi
	addq	$4, %rcx
	decq	%rdx
	jne	.LBB5_11
.LBB5_35:                               # %._crit_edge45
	movslq	%r8d, %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx), %r12
	movslq	8(%rsp), %rcx           # 4-byte Folded Reload
	movb	$0, (%rcx,%r12)
	addq	%rcx, %r12
	movq	%rax, 24(%rsp)          # 8-byte Spill
	addq	%rax, %rdx
	leaq	(%rdx,%rcx), %r15
	movb	$0, (%rcx,%rdx)
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	js	.LBB5_78
# BB#36:                                # %.lr.ph36.preheader
	xorl	%ecx, %ecx
	movdqa	.LCPI5_4(%rip), %xmm0   # xmm0 = [45,0,45,0,45,0,45,0,45,0,45,0,45,0,45,0]
	packuswb	%xmm0, %xmm0
	movdqa	.LCPI5_3(%rip), %xmm1   # xmm1 = [111,0,111,0,111,0,111,0,111,0,111,0,111,0,111,0]
	packuswb	%xmm1, %xmm1
	jmp	.LBB5_37
.LBB5_49:                               # %vector.body129.preheader
                                        #   in Loop: Header=BB5_37 Depth=1
	movq	%r8, (%rsp)             # 8-byte Spill
	subl	%ebp, %edi
	leaq	-1(%rsi), %rbx
	subq	%rax, %rbx
	leaq	(%r15,%rbx), %rax
	addq	%r12, %rbx
	leaq	-8(%r12), %r8
	addq	$-8, %r15
	.p2align	4, 0x90
.LBB5_50:                               # %vector.body129
                                        #   Parent Loop BB5_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm1, (%r8)
	movq	%xmm1, -8(%r8)
	movq	%xmm0, (%r15)
	movq	%xmm0, -8(%r15)
	addq	$-16, %r8
	addq	$-16, %r15
	addq	$-16, %rbp
	jne	.LBB5_50
# BB#51:                                # %middle.block130
                                        #   in Loop: Header=BB5_37 Depth=1
	testq	%rsi, %rsi
	movq	(%rsp), %r8             # 8-byte Reload
	jne	.LBB5_52
	jmp	.LBB5_58
.LBB5_65:                               # %vector.body103.preheader
                                        #   in Loop: Header=BB5_37 Depth=1
	subl	%ebp, %edi
	leaq	-1(%rsi), %rax
	subq	%rbx, %rax
	leaq	(%r15,%rax), %rbx
	addq	%r12, %rax
	leaq	-8(%r12), %rdx
	addq	$-8, %r15
	.p2align	4, 0x90
.LBB5_66:                               # %vector.body103
                                        #   Parent Loop BB5_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm0, (%rdx)
	movq	%xmm0, -8(%rdx)
	movq	%xmm1, (%r15)
	movq	%xmm1, -8(%r15)
	addq	$-16, %rdx
	addq	$-16, %r15
	addq	$-16, %rbp
	jne	.LBB5_66
# BB#67:                                # %middle.block104
                                        #   in Loop: Header=BB5_37 Depth=1
	testq	%rsi, %rsi
	jne	.LBB5_68
	jmp	.LBB5_74
	.p2align	4, 0x90
.LBB5_37:                               # %.lr.ph36
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_50 Depth 2
                                        #     Child Loop BB5_54 Depth 2
                                        #     Child Loop BB5_57 Depth 2
                                        #     Child Loop BB5_66 Depth 2
                                        #     Child Loop BB5_70 Depth 2
                                        #     Child Loop BB5_73 Depth 2
	movslq	%r8d, %rax
	movq	(%r9,%rax,8), %rax
	movslq	%r10d, %rdx
	movl	(%rax,%rdx,4), %r11d
	testl	%r11d, %r11d
	js	.LBB5_38
# BB#39:                                #   in Loop: Header=BB5_37 Depth=1
	je	.LBB5_41
# BB#40:                                #   in Loop: Header=BB5_37 Depth=1
	movl	%r8d, %r9d
	subl	%r11d, %r9d
	jmp	.LBB5_42
	.p2align	4, 0x90
.LBB5_38:                               #   in Loop: Header=BB5_37 Depth=1
	leal	-1(%r8), %r9d
	jmp	.LBB5_43
	.p2align	4, 0x90
.LBB5_41:                               #   in Loop: Header=BB5_37 Depth=1
	leal	-1(%r8), %r9d
.LBB5_42:                               #   in Loop: Header=BB5_37 Depth=1
	movl	$-1, %r11d
.LBB5_43:                               #   in Loop: Header=BB5_37 Depth=1
	movq	%r10, 8(%rsp)           # 8-byte Spill
	movl	%r8d, %edi
	subl	%r9d, %edi
	decl	%edi
	je	.LBB5_59
# BB#44:                                # %.lr.ph17.preheader
                                        #   in Loop: Header=BB5_37 Depth=1
	leal	-2(%r8), %eax
	subl	%r9d, %eax
	movq	%rax, %rdx
	negq	%rdx
	leaq	-1(%r15), %r10
	leal	-1(%r8,%rcx), %ecx
	leaq	1(%rax), %rbp
	cmpq	$16, %rbp
	jb	.LBB5_45
# BB#46:                                # %min.iters.checked133
                                        #   in Loop: Header=BB5_37 Depth=1
	movl	%ebp, %esi
	andl	$15, %esi
	subq	%rsi, %rbp
	je	.LBB5_45
# BB#47:                                # %vector.memcheck146
                                        #   in Loop: Header=BB5_37 Depth=1
	leaq	-1(%r12,%rdx), %rbx
	cmpq	%r15, %rbx
	jae	.LBB5_49
# BB#48:                                # %vector.memcheck146
                                        #   in Loop: Header=BB5_37 Depth=1
	leaq	-1(%r15,%rdx), %rbx
	cmpq	%r12, %rbx
	jae	.LBB5_49
	.p2align	4, 0x90
.LBB5_45:                               #   in Loop: Header=BB5_37 Depth=1
	movq	%r15, %rax
	movq	%r12, %rbx
.LBB5_52:                               # %.lr.ph17.preheader168
                                        #   in Loop: Header=BB5_37 Depth=1
	leal	-1(%rdi), %esi
	movl	%edi, %ebp
	andl	$7, %ebp
	je	.LBB5_55
# BB#53:                                # %.lr.ph17.prol.preheader
                                        #   in Loop: Header=BB5_37 Depth=1
	negl	%ebp
	.p2align	4, 0x90
.LBB5_54:                               # %.lr.ph17.prol
                                        #   Parent Loop BB5_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, -1(%rbx)
	decq	%rbx
	movb	$45, -1(%rax)
	decq	%rax
	decl	%edi
	incl	%ebp
	jne	.LBB5_54
.LBB5_55:                               # %.lr.ph17.prol.loopexit
                                        #   in Loop: Header=BB5_37 Depth=1
	cmpl	$7, %esi
	jb	.LBB5_58
# BB#56:                                # %.lr.ph17.preheader168.new
                                        #   in Loop: Header=BB5_37 Depth=1
	decq	%rax
	decq	%rbx
	.p2align	4, 0x90
.LBB5_57:                               # %.lr.ph17
                                        #   Parent Loop BB5_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, (%rbx)
	movb	$45, (%rax)
	movb	$111, -1(%rbx)
	movb	$45, -1(%rax)
	movb	$111, -2(%rbx)
	movb	$45, -2(%rax)
	movb	$111, -3(%rbx)
	movb	$45, -3(%rax)
	movb	$111, -4(%rbx)
	movb	$45, -4(%rax)
	movb	$111, -5(%rbx)
	movb	$45, -5(%rax)
	movb	$111, -6(%rbx)
	movb	$45, -6(%rax)
	movb	$111, -7(%rbx)
	movb	$45, -7(%rax)
	addq	$-8, %rax
	addq	$-8, %rbx
	addl	$-8, %edi
	jne	.LBB5_57
.LBB5_58:                               # %._crit_edge18.loopexit
                                        #   in Loop: Header=BB5_37 Depth=1
	leaq	-1(%r12,%rdx), %r12
	addq	%rdx, %r10
	subl	%r9d, %ecx
	movq	%r10, %r15
.LBB5_59:                               # %._crit_edge18
                                        #   in Loop: Header=BB5_37 Depth=1
	cmpl	$-1, %r11d
	je	.LBB5_75
# BB#60:                                # %.lr.ph25.preheader
                                        #   in Loop: Header=BB5_37 Depth=1
	movq	%r8, (%rsp)             # 8-byte Spill
	movl	%r11d, %edi
	notl	%edi
	movl	$-2, %ebx
	subl	%r11d, %ebx
	movq	%rbx, %r8
	negq	%r8
	leaq	-1(%r15), %r10
	decl	%ecx
	leaq	1(%rbx), %rbp
	cmpq	$16, %rbp
	jb	.LBB5_61
# BB#62:                                # %min.iters.checked107
                                        #   in Loop: Header=BB5_37 Depth=1
	movl	%ebp, %esi
	andl	$15, %esi
	subq	%rsi, %rbp
	je	.LBB5_61
# BB#63:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_37 Depth=1
	leaq	-1(%r12,%r8), %rax
	cmpq	%r15, %rax
	jae	.LBB5_65
# BB#64:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_37 Depth=1
	leaq	-1(%r15,%r8), %rax
	cmpq	%r12, %rax
	jae	.LBB5_65
	.p2align	4, 0x90
.LBB5_61:                               #   in Loop: Header=BB5_37 Depth=1
	movq	%r15, %rbx
	movq	%r12, %rax
.LBB5_68:                               # %.lr.ph25.preheader167
                                        #   in Loop: Header=BB5_37 Depth=1
	leal	-1(%rdi), %esi
	movl	%edi, %ebp
	andl	$7, %ebp
	je	.LBB5_71
# BB#69:                                # %.lr.ph25.prol.preheader
                                        #   in Loop: Header=BB5_37 Depth=1
	negl	%ebp
	.p2align	4, 0x90
.LBB5_70:                               # %.lr.ph25.prol
                                        #   Parent Loop BB5_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, -1(%rax)
	decq	%rax
	movb	$111, -1(%rbx)
	decq	%rbx
	decl	%edi
	incl	%ebp
	jne	.LBB5_70
.LBB5_71:                               # %.lr.ph25.prol.loopexit
                                        #   in Loop: Header=BB5_37 Depth=1
	cmpl	$7, %esi
	jb	.LBB5_74
# BB#72:                                # %.lr.ph25.preheader167.new
                                        #   in Loop: Header=BB5_37 Depth=1
	decq	%rbx
	decq	%rax
	.p2align	4, 0x90
.LBB5_73:                               # %.lr.ph25
                                        #   Parent Loop BB5_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, (%rax)
	movb	$111, (%rbx)
	movb	$45, -1(%rax)
	movb	$111, -1(%rbx)
	movb	$45, -2(%rax)
	movb	$111, -2(%rbx)
	movb	$45, -3(%rax)
	movb	$111, -3(%rbx)
	movb	$45, -4(%rax)
	movb	$111, -4(%rbx)
	movb	$45, -5(%rax)
	movb	$111, -5(%rbx)
	movb	$45, -6(%rax)
	movb	$111, -6(%rbx)
	movb	$45, -7(%rax)
	movb	$111, -7(%rbx)
	addq	$-8, %rbx
	addq	$-8, %rax
	addl	$-8, %edi
	jne	.LBB5_73
.LBB5_74:                               # %._crit_edge26.loopexit
                                        #   in Loop: Header=BB5_37 Depth=1
	leaq	-1(%r12,%r8), %r12
	addq	%r8, %r10
	subl	%r11d, %ecx
	movq	%r10, %r15
	movq	(%rsp), %r8             # 8-byte Reload
.LBB5_75:                               # %._crit_edge26
                                        #   in Loop: Header=BB5_37 Depth=1
	movq	8(%rsp), %r10           # 8-byte Reload
	testl	%r10d, %r10d
	jle	.LBB5_78
# BB#76:                                # %._crit_edge26
                                        #   in Loop: Header=BB5_37 Depth=1
	testl	%r8d, %r8d
	jle	.LBB5_78
# BB#77:                                #   in Loop: Header=BB5_37 Depth=1
	addl	%r11d, %r10d
	movb	$111, -1(%r12)
	decq	%r12
	movb	$111, -1(%r15)
	decq	%r15
	addl	$2, %ecx
	cmpl	16(%rsp), %ecx          # 4-byte Folded Reload
	movl	%r9d, %r8d
	movq	48(%rsp), %r9           # 8-byte Reload
	jle	.LBB5_37
.LBB5_78:                               # %._crit_edge37
	movl	32(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB5_81
# BB#79:                                # %.lr.ph12.preheader
	movl	%eax, %ebx
	.p2align	4, 0x90
.LBB5_80:                               # %.lr.ph12
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rdi
	movq	(%r13), %rsi
	movq	%r12, %rdx
	callq	gapireru
	addq	$8, %r14
	addq	$8, %r13
	decq	%rbx
	jne	.LBB5_80
.LBB5_81:                               # %.preheader
	movl	36(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	jle	.LBB5_84
# BB#82:                                # %.lr.ph.preheader
	movl	%eax, %r14d
	.p2align	4, 0x90
.LBB5_83:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movq	(%rbx), %rsi
	movq	%r15, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %rbx
	decq	%r14
	jne	.LBB5_83
.LBB5_84:                               # %._crit_edge
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end5:
	.size	Atracking, .Lfunc_end5-Atracking
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4608533498688228557     # double 1.3
.LCPI6_3:
	.quad	4607182418800017408     # double 1
.LCPI6_4:
	.quad	4602678819172646912     # double 0.5
.LCPI6_5:
	.quad	-4620693217682128896    # double -0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_1:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI6_2:
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
.LCPI6_6:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI6_7:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI6_8:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI6_9:
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
.LCPI6_10:
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.text
	.globl	A__align_gapmap
	.p2align	4, 0x90
	.type	A__align_gapmap,@function
A__align_gapmap:                        # @A__align_gapmap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi73:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi75:
	.cfi_def_cfa_offset 336
.Lcfi76:
	.cfi_offset %rbx, -56
.Lcfi77:
	.cfi_offset %r12, -48
.Lcfi78:
	.cfi_offset %r13, -40
.Lcfi79:
	.cfi_offset %r14, -32
.Lcfi80:
	.cfi_offset %r15, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movl	A__align_gapmap.orlgth1(%rip), %r14d
	testl	%r14d, %r14d
	jne	.LBB6_2
# BB#1:
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, A__align_gapmap.mseq1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, A__align_gapmap.mseq2(%rip)
	movl	A__align_gapmap.orlgth1(%rip), %r14d
.LBB6_2:
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	(%r12), %rdi
	callq	strlen
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	cmpl	%r14d, %ebx
	movl	A__align_gapmap.orlgth2(%rip), %r15d
	movq	%r12, 96(%rsp)          # 8-byte Spill
	movq	%r13, 72(%rsp)          # 8-byte Spill
	movq	%rax, (%rsp)            # 8-byte Spill
	jg	.LBB6_4
# BB#3:
	cmpl	%r15d, %eax
	jle	.LBB6_9
.LBB6_4:
	testl	%r14d, %r14d
	jle	.LBB6_5
# BB#6:
	testl	%r15d, %r15d
	movq	16(%rsp), %rbx          # 8-byte Reload
	jle	.LBB6_8
# BB#7:
	movq	A__align_gapmap.w1(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align_gapmap.w2(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align_gapmap.match(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align_gapmap.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align_gapmap.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align_gapmap.m(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align_gapmap.mp(%rip), %rdi
	callq	FreeIntVec
	movq	A__align_gapmap.mseq(%rip), %rdi
	callq	FreeCharMtx
	movq	A__align_gapmap.ogcp1(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align_gapmap.ogcp2(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align_gapmap.fgcp1(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align_gapmap.fgcp2(%rip), %rdi
	callq	FreeFloatVec
	movq	A__align_gapmap.cpmx1(%rip), %rdi
	callq	FreeFloatMtx
	movq	A__align_gapmap.cpmx2(%rip), %rdi
	callq	FreeFloatMtx
	movq	A__align_gapmap.floatwork(%rip), %rdi
	callq	FreeFloatMtx
	movq	A__align_gapmap.intwork(%rip), %rdi
	callq	FreeIntMtx
	movl	A__align_gapmap.orlgth1(%rip), %r14d
	movl	A__align_gapmap.orlgth2(%rip), %r15d
	jmp	.LBB6_8
.LBB6_5:
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB6_8:
	cvtsi2sdl	%ebx, %xmm0
	movsd	.LCPI6_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r14d, %eax
	cmovgel	%eax, %r14d
	leal	100(%r14), %ebp
	movq	(%rsp), %rax            # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	leal	100(%r15), %r12d
	leal	102(%r15), %ebx
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align_gapmap.w1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align_gapmap.w2(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align_gapmap.match(%rip)
	leal	102(%r14), %r13d
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align_gapmap.initverticalw(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align_gapmap.lastverticalw(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align_gapmap.m(%rip)
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, A__align_gapmap.mp(%rip)
	movl	njob(%rip), %edi
	leal	200(%r15,%r14), %esi
	callq	AllocateCharMtx
	movq	%rax, A__align_gapmap.mseq(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align_gapmap.ogcp1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align_gapmap.ogcp2(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align_gapmap.fgcp1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, A__align_gapmap.fgcp2(%rip)
	movl	$26, %edi
	movl	%r13d, %esi
	callq	AllocateFloatMtx
	movq	%rax, A__align_gapmap.cpmx1(%rip)
	movl	$26, %edi
	movl	%ebx, %esi
	callq	AllocateFloatMtx
	movq	%rax, A__align_gapmap.cpmx2(%rip)
	cmpl	%r12d, %ebp
	cmovgel	%ebp, %r12d
	addl	$2, %r12d
	movl	$26, %esi
	movl	%r12d, %edi
	callq	AllocateFloatMtx
	movq	%rax, A__align_gapmap.floatwork(%rip)
	movl	$26, %esi
	movl	%r12d, %edi
	callq	AllocateIntMtx
	movq	%rax, A__align_gapmap.intwork(%rip)
	movl	%r14d, A__align_gapmap.orlgth1(%rip)
	movl	%r15d, A__align_gapmap.orlgth2(%rip)
	movq	96(%rsp), %r12          # 8-byte Reload
	movq	72(%rsp), %r13          # 8-byte Reload
.LBB6_9:                                # %.preheader360
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB6_16
# BB#10:                                # %.lr.ph415
	movq	A__align_gapmap.mseq(%rip), %r9
	movq	A__align_gapmap.mseq1(%rip), %rdi
	movslq	16(%rsp), %rax          # 4-byte Folded Reload
	movl	24(%rsp), %ecx          # 4-byte Reload
	leaq	-1(%rcx), %r8
	movq	%rcx, %rbx
	xorl	%ebp, %ebp
	andq	$3, %rbx
	je	.LBB6_13
# BB#11:                                # %.prol.preheader790
	movq	40(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_12:                               # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rbp,8), %rdx
	movq	%rdx, (%rdi,%rbp,8)
	movq	(%rsi,%rbp,8), %rdx
	movb	$0, (%rdx,%rax)
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB6_12
.LBB6_13:                               # %.prol.loopexit791
	cmpq	$3, %r8
	jb	.LBB6_16
# BB#14:                                # %.lr.ph415.new
	subq	%rbp, %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	24(%rdx,%rbp,8), %rdx
	leaq	24(%rdi,%rbp,8), %rdi
	leaq	24(%r9,%rbp,8), %rsi
	.p2align	4, 0x90
.LBB6_15:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rsi), %rbp
	movq	%rbp, -24(%rdi)
	movq	-24(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	-16(%rsi), %rbp
	movq	%rbp, -16(%rdi)
	movq	-16(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	-8(%rsi), %rbp
	movq	%rbp, -8(%rdi)
	movq	-8(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	(%rsi), %rbp
	movq	%rbp, (%rdi)
	movq	(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	addq	$32, %rdx
	addq	$32, %rdi
	addq	$32, %rsi
	addq	$-4, %rcx
	jne	.LBB6_15
.LBB6_16:                               # %.preheader359
	testl	%r13d, %r13d
	jle	.LBB6_24
# BB#17:                                # %.lr.ph412
	movq	A__align_gapmap.mseq(%rip), %r8
	movq	A__align_gapmap.mseq2(%rip), %rdi
	movslq	(%rsp), %rax            # 4-byte Folded Reload
	movslq	24(%rsp), %r10          # 4-byte Folded Reload
	movl	%r13d, %ecx
	leaq	-1(%rcx), %r9
	movq	%rcx, %rdx
	andq	$3, %rdx
	je	.LBB6_18
# BB#19:                                # %.prol.preheader785
	leaq	(%r8,%r10,8), %rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_20:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rbx,8), %rbp
	movq	%rbp, (%rdi,%rbx,8)
	movq	(%r12,%rbx,8), %rbp
	movb	$0, (%rbp,%rax)
	incq	%rbx
	cmpq	%rbx, %rdx
	jne	.LBB6_20
	jmp	.LBB6_21
.LBB6_18:
	xorl	%ebx, %ebx
.LBB6_21:                               # %.prol.loopexit786
	cmpq	$3, %r9
	jb	.LBB6_24
# BB#22:                                # %.lr.ph412.new
	subq	%rbx, %rcx
	leaq	24(%r12,%rbx,8), %rsi
	leaq	24(%rdi,%rbx,8), %rdi
	addq	%rbx, %r10
	leaq	24(%r8,%r10,8), %rdx
	.p2align	4, 0x90
.LBB6_23:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rbp
	movq	%rbp, -24(%rdi)
	movq	-24(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	-16(%rdx), %rbp
	movq	%rbp, -16(%rdi)
	movq	-16(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	-8(%rdx), %rbp
	movq	%rbp, -8(%rdi)
	movq	-8(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	(%rdx), %rbp
	movq	%rbp, (%rdi)
	movq	(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB6_23
.LBB6_24:                               # %._crit_edge413
	movl	commonAlloc1(%rip), %ebx
	cmpl	%ebx, %r14d
	movl	commonAlloc2(%rip), %ebp
	jg	.LBB6_27
# BB#25:                                # %._crit_edge413
	cmpl	%ebp, %r15d
	jg	.LBB6_27
# BB#26:                                # %._crit_edge486
	movq	%r12, %r13
	movq	commonIP(%rip), %rax
	jmp	.LBB6_31
.LBB6_27:                               # %._crit_edge413._crit_edge
	movq	%r12, %r13
	testl	%ebx, %ebx
	je	.LBB6_30
# BB#28:                                # %._crit_edge413._crit_edge
	testl	%ebp, %ebp
	je	.LBB6_30
# BB#29:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movl	A__align_gapmap.orlgth1(%rip), %r14d
	movl	commonAlloc1(%rip), %ebx
	movl	A__align_gapmap.orlgth2(%rip), %r15d
	movl	commonAlloc2(%rip), %ebp
.LBB6_30:
	cmpl	%ebx, %r14d
	cmovgel	%r14d, %ebx
	cmpl	%ebp, %r15d
	cmovgel	%r15d, %ebp
	leal	10(%rbx), %edi
	leal	10(%rbp), %esi
	callq	AllocateIntMtx
	movq	%rax, commonIP(%rip)
	movl	%ebx, commonAlloc1(%rip)
	movl	%ebp, commonAlloc2(%rip)
.LBB6_31:
	movq	%rax, A__align_gapmap.ijp(%rip)
	movq	A__align_gapmap.cpmx1(%rip), %rbx
	movq	40(%rsp), %r12          # 8-byte Reload
	movq	(%r12), %rdi
	callq	strlen
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	movl	%eax, %ecx
	movq	24(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r8d
	callq	cpmx_calc_new
	movq	A__align_gapmap.cpmx2(%rip), %r14
	movq	(%r13), %rdi
	callq	strlen
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdx
	movl	%eax, %ecx
	movq	72(%rsp), %r15          # 8-byte Reload
	movl	%r15d, %r8d
	callq	cpmx_calc_new
	movq	A__align_gapmap.ogcp1(%rip), %rdi
	movl	%ebx, %esi
	movq	%r12, %rdx
	movq	%rbp, %rcx
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r8d
	callq	st_OpeningGapCount
	movq	A__align_gapmap.ogcp2(%rip), %rdi
	movl	%r15d, %esi
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	%ebp, %r8d
	callq	st_OpeningGapCount
	movq	A__align_gapmap.fgcp1(%rip), %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r12, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%ebx, %r8d
	callq	st_FinalGapCount
	movq	A__align_gapmap.fgcp2(%rip), %rdi
	movl	%r15d, %esi
	movq	%rbx, %r12
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	%rbp, %r15
	movl	%ebp, %r8d
	callq	st_FinalGapCount
	testl	%r12d, %r12d
	jle	.LBB6_45
# BB#32:                                # %.lr.ph410
	movq	A__align_gapmap.ogcp1(%rip), %r8
	movq	A__align_gapmap.fgcp1(%rip), %rbx
	movl	%r12d, %eax
	cmpq	$3, %rax
	jbe	.LBB6_33
# BB#39:                                # %min.iters.checked
	movl	%r12d, %edi
	andl	$3, %edi
	movq	%rax, %rbp
	subq	%rdi, %rbp
	je	.LBB6_33
# BB#40:                                # %vector.memcheck
	leaq	(%rbx,%rax,4), %rcx
	cmpq	%rcx, %r8
	jae	.LBB6_42
# BB#41:                                # %vector.memcheck
	leaq	(%r8,%rax,4), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB6_42
.LBB6_33:
	xorl	%ebp, %ebp
.LBB6_34:                               # %scalar.ph.preheader
	movl	%r12d, %edx
	subl	%ebp, %edx
	leaq	-1(%rax), %rcx
	testb	$1, %dl
	movq	%rbp, %rdx
	je	.LBB6_36
# BB#35:                                # %scalar.ph.prol
	movss	(%r8,%rbp,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movsd	.LCPI6_3(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	subsd	%xmm0, %xmm2
	movsd	.LCPI6_4(%rip), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	movss	%xmm2, (%r8,%rbp,4)
	movss	(%rbx,%rbp,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, (%rbx,%rbp,4)
	leaq	1(%rbp), %rdx
.LBB6_36:                               # %scalar.ph.prol.loopexit
	cmpq	%rbp, %rcx
	je	.LBB6_45
# BB#37:                                # %scalar.ph.preheader.new
	subq	%rdx, %rax
	leaq	4(%r8,%rdx,4), %rcx
	leaq	4(%rbx,%rdx,4), %rdx
	movsd	.LCPI6_3(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI6_4(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB6_38:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movapd	%xmm0, %xmm3
	subsd	%xmm2, %xmm3
	mulsd	%xmm1, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, -4(%rcx)
	movss	-4(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movapd	%xmm0, %xmm3
	subsd	%xmm2, %xmm3
	mulsd	%xmm1, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, -4(%rdx)
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movapd	%xmm0, %xmm3
	subsd	%xmm2, %xmm3
	mulsd	%xmm1, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, (%rcx)
	movss	(%rdx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movapd	%xmm0, %xmm3
	subsd	%xmm2, %xmm3
	mulsd	%xmm1, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, (%rdx)
	addq	$8, %rcx
	addq	$8, %rdx
	addq	$-2, %rax
	jne	.LBB6_38
.LBB6_45:                               # %.preheader358
	movq	%r15, %rbx
	testl	%ebx, %ebx
	jle	.LBB6_59
# BB#46:                                # %.lr.ph406
	movq	A__align_gapmap.ogcp2(%rip), %r8
	movq	A__align_gapmap.fgcp2(%rip), %r9
	movl	%ebx, %eax
	cmpq	$3, %rax
	jbe	.LBB6_47
# BB#53:                                # %min.iters.checked505
	movl	%ebx, %edi
	andl	$3, %edi
	movq	%rax, %rbp
	subq	%rdi, %rbp
	je	.LBB6_47
# BB#54:                                # %vector.memcheck518
	leaq	(%r9,%rax,4), %rcx
	cmpq	%rcx, %r8
	jae	.LBB6_56
# BB#55:                                # %vector.memcheck518
	leaq	(%r8,%rax,4), %rcx
	cmpq	%rcx, %r9
	jae	.LBB6_56
.LBB6_47:
	xorl	%ebp, %ebp
.LBB6_48:                               # %scalar.ph503.preheader
	movl	%ebx, %edx
	subl	%ebp, %edx
	leaq	-1(%rax), %rcx
	testb	$1, %dl
	movq	%rbp, %rdx
	je	.LBB6_50
# BB#49:                                # %scalar.ph503.prol
	movss	(%r8,%rbp,4), %xmm0     # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movsd	.LCPI6_3(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	subsd	%xmm0, %xmm2
	movsd	.LCPI6_4(%rip), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	movss	%xmm2, (%r8,%rbp,4)
	movss	(%r9,%rbp,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, (%r9,%rbp,4)
	leaq	1(%rbp), %rdx
.LBB6_50:                               # %scalar.ph503.prol.loopexit
	cmpq	%rbp, %rcx
	je	.LBB6_59
# BB#51:                                # %scalar.ph503.preheader.new
	subq	%rdx, %rax
	leaq	4(%r8,%rdx,4), %rcx
	leaq	4(%r9,%rdx,4), %rdx
	movsd	.LCPI6_3(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI6_4(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB6_52:                               # %scalar.ph503
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movapd	%xmm0, %xmm3
	subsd	%xmm2, %xmm3
	mulsd	%xmm1, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, -4(%rcx)
	movss	-4(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movapd	%xmm0, %xmm3
	subsd	%xmm2, %xmm3
	mulsd	%xmm1, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, -4(%rdx)
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movapd	%xmm0, %xmm3
	subsd	%xmm2, %xmm3
	mulsd	%xmm1, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, (%rcx)
	movss	(%rdx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movapd	%xmm0, %xmm3
	subsd	%xmm2, %xmm3
	mulsd	%xmm1, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, (%rdx)
	addq	$8, %rcx
	addq	$8, %rdx
	addq	$-2, %rax
	jne	.LBB6_52
.LBB6_59:                               # %._crit_edge407
	movq	A__align_gapmap.w1(%rip), %rbx
	movq	A__align_gapmap.w2(%rip), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	A__align_gapmap.initverticalw(%rip), %rdi
	movq	A__align_gapmap.cpmx2(%rip), %rsi
	movq	A__align_gapmap.cpmx1(%rip), %rdx
	movq	A__align_gapmap.floatwork(%rip), %r9
	movl	$0, %ecx
	movl	%r12d, %r8d
	pushq	$1
.Lcfi82:
	.cfi_adjust_cfa_offset 8
	pushq	A__align_gapmap.intwork(%rip)
.Lcfi83:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi84:
	.cfi_adjust_cfa_offset -16
	cmpq	$0, 344(%rsp)
	je	.LBB6_71
# BB#60:
	testl	%r12d, %r12d
	movq	%r15, %r8
	je	.LBB6_66
# BB#61:                                # %.lr.ph.i
	movq	A__align_gapmap.initverticalw(%rip), %rax
	movq	impmtx(%rip), %rcx
	movq	368(%rsp), %rdx
	movslq	(%rdx), %rdx
	testb	$1, %r12b
	jne	.LBB6_63
# BB#62:
	movq	360(%rsp), %rsi
	movl	%r12d, %edi
	cmpl	$1, %r12d
	jne	.LBB6_65
	jmp	.LBB6_66
.LBB6_71:                               # %.critedge
	movq	A__align_gapmap.cpmx1(%rip), %rsi
	movq	A__align_gapmap.cpmx2(%rip), %rdx
	movq	A__align_gapmap.floatwork(%rip), %r9
	movl	$0, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rbp
	movl	%ebp, %r8d
	pushq	$1
.Lcfi85:
	.cfi_adjust_cfa_offset 8
	pushq	A__align_gapmap.intwork(%rip)
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi87:
	.cfi_adjust_cfa_offset -16
	cmpl	$1, outgap(%rip)
	je	.LBB6_83
	jmp	.LBB6_73
.LBB6_63:
	leal	-1(%r12), %edi
	movq	360(%rsp), %rsi
	movq	%rsi, %rbp
	leaq	4(%rbp), %rsi
	movslq	(%rbp), %rbp
	movq	(%rcx,%rbp,8), %rbp
	movss	(%rbp,%rdx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	leaq	4(%rax), %rax
	cmpl	$1, %r12d
	je	.LBB6_66
	.p2align	4, 0x90
.LBB6_65:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rsi), %rbp
	movq	(%rcx,%rbp,8), %rbp
	movss	(%rbp,%rdx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	addl	$-2, %edi
	movslq	4(%rsi), %rbp
	leaq	8(%rsi), %rsi
	movq	(%rcx,%rbp,8), %rbp
	movss	(%rbp,%rdx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	4(%rax), %xmm0
	movss	%xmm0, 4(%rax)
	leaq	8(%rax), %rax
	jne	.LBB6_65
.LBB6_66:                               # %imp_match_out_vead_tate_gapmap.exit
	movq	A__align_gapmap.cpmx1(%rip), %rsi
	movq	A__align_gapmap.cpmx2(%rip), %rdx
	movq	A__align_gapmap.floatwork(%rip), %r9
	movl	$0, %ecx
	movq	%rbx, %rdi
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	pushq	$1
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	pushq	A__align_gapmap.intwork(%rip)
.Lcfi89:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	movq	%r15, %rbp
	addq	$16, %rsp
.Lcfi90:
	.cfi_adjust_cfa_offset -16
	testl	%ebp, %ebp
	je	.LBB6_72
# BB#67:                                # %.lr.ph.i335.preheader
	movq	impmtx(%rip), %rax
	movq	360(%rsp), %rcx
	movslq	(%rcx), %rcx
	movq	(%rax,%rcx,8), %rax
	testb	$1, %bpl
	movq	368(%rsp), %rcx
	movq	%rbx, %rsi
	movl	%ebp, %edx
	je	.LBB6_69
# BB#68:                                # %.lr.ph.i335.prol
	leal	-1(%rbp), %edx
	movq	368(%rsp), %rcx
	movq	%rcx, %rsi
	leaq	4(%rsi), %rcx
	movslq	(%rsi), %rsi
	movss	(%rax,%rsi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	leaq	4(%rbx), %rsi
	addss	(%rbx), %xmm0
	movss	%xmm0, (%rbx)
.LBB6_69:                               # %.lr.ph.i335.prol.loopexit
	cmpl	$1, %ebp
	je	.LBB6_72
	.p2align	4, 0x90
.LBB6_70:                               # %.lr.ph.i335
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rcx), %rdi
	movss	(%rax,%rdi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addl	$-2, %edx
	movslq	4(%rcx), %rdi
	leaq	8(%rcx), %rcx
	movss	(%rax,%rdi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	4(%rsi), %xmm0
	movss	%xmm0, 4(%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB6_70
.LBB6_72:                               # %imp_match_out_vead_gapmap.exit
	cmpl	$1, outgap(%rip)
	jne	.LBB6_73
.LBB6_83:                               # %.preheader354
	testl	%r12d, %r12d
	jle	.LBB6_98
# BB#84:                                # %.lr.ph400
	movq	A__align_gapmap.ogcp1(%rip), %rax
	movq	A__align_gapmap.fgcp1(%rip), %r8
	movq	A__align_gapmap.initverticalw(%rip), %r9
	leaq	1(%r12), %r14
	movl	%r14d, %ecx
	leaq	-1(%rcx), %r11
	cmpq	$7, %r11
	jbe	.LBB6_85
# BB#91:                                # %min.iters.checked533
	movl	%r12d, %r10d
	andl	$7, %r10d
	movq	%r11, %rdi
	subq	%r10, %rdi
	je	.LBB6_85
# BB#92:                                # %vector.memcheck552
	leaq	4(%r9), %rdx
	leaq	(%r9,%rcx,4), %rbp
	leaq	-4(%r8,%rcx,4), %rsi
	cmpq	%rax, %rdx
	sbbb	%r15b, %r15b
	cmpq	%rbp, %rax
	sbbb	%r12b, %r12b
	andb	%r15b, %r12b
	cmpq	%rsi, %rdx
	sbbb	%sil, %sil
	cmpq	%rbp, %r8
	sbbb	%bpl, %bpl
	movl	$1, %edx
	testb	$1, %r12b
	jne	.LBB6_93
# BB#94:                                # %vector.memcheck552
	andb	%bpl, %sil
	andb	$1, %sil
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	(%rsp), %r15            # 8-byte Reload
	jne	.LBB6_86
# BB#95:                                # %vector.body529.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r8), %rsi
	leaq	20(%r9), %rbp
	leaq	1(%rdi), %rdx
	.p2align	4, 0x90
.LBB6_96:                               # %vector.body529
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm1
	movups	(%rsi), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rbp), %xmm3
	movups	(%rbp), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rbp)
	movups	%xmm4, (%rbp)
	addq	$32, %rsi
	addq	$32, %rbp
	addq	$-8, %rdi
	jne	.LBB6_96
# BB#97:                                # %middle.block530
	testq	%r10, %r10
	jne	.LBB6_86
	jmp	.LBB6_98
.LBB6_73:                               # %.preheader357
	testl	%ebp, %ebp
	jle	.LBB6_80
# BB#74:                                # %.lr.ph404
	movslq	offset(%rip), %r8
	leaq	1(%rbp), %rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB6_75
# BB#76:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r8d, %xmm0
	mulsd	.LCPI6_5(%rip), %xmm0
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rbx)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB6_78
	jmp	.LBB6_80
.LBB6_85:
	movl	$1, %edx
.LBB6_86:                               # %scalar.ph531.preheader
	subl	%edx, %r14d
	testb	$1, %r14b
	movq	%rdx, %rsi
	je	.LBB6_88
# BB#87:                                # %scalar.ph531.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%r8,%rdx,4), %xmm0
	addss	(%r9,%rdx,4), %xmm0
	movss	%xmm0, (%r9,%rdx,4)
	leaq	1(%rdx), %rsi
.LBB6_88:                               # %scalar.ph531.prol.loopexit
	cmpq	%rdx, %r11
	je	.LBB6_98
# BB#89:                                # %scalar.ph531.preheader.new
	subq	%rsi, %rcx
	leaq	(%r8,%rsi,4), %rdx
	leaq	4(%r9,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB6_90:                               # %scalar.ph531
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB6_90
.LBB6_98:                               # %.preheader353
	movq	%r15, %rbp
	testl	%ebp, %ebp
	jle	.LBB6_99
# BB#100:                               # %.lr.ph398
	movq	A__align_gapmap.ogcp2(%rip), %rax
	movq	A__align_gapmap.fgcp2(%rip), %r8
	leaq	1(%rbp), %r11
	movl	%r11d, %ecx
	leaq	-1(%rcx), %r10
	cmpq	$7, %r10
	jbe	.LBB6_101
# BB#107:                               # %min.iters.checked570
	movl	%ebp, %r9d
	andl	$7, %r9d
	movq	%r10, %rsi
	subq	%r9, %rsi
	je	.LBB6_101
# BB#108:                               # %vector.memcheck591
	leaq	4(%rbx), %rbp
	leaq	(%rbx,%rcx,4), %rdi
	leaq	-4(%r8,%rcx,4), %r15
	cmpq	%rax, %rbp
	sbbb	%r14b, %r14b
	cmpq	%rdi, %rax
	sbbb	%dl, %dl
	andb	%r14b, %dl
	cmpq	%r15, %rbp
	sbbb	%r14b, %r14b
	cmpq	%rdi, %r8
	sbbb	%dil, %dil
	movl	$1, %ebp
	testb	$1, %dl
	jne	.LBB6_109
# BB#110:                               # %vector.memcheck591
	andb	%dil, %r14b
	andb	$1, %r14b
	movq	(%rsp), %r15            # 8-byte Reload
	jne	.LBB6_102
# BB#111:                               # %vector.body566.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r8), %rdx
	leaq	20(%rbx), %rdi
	leaq	1(%rsi), %rbp
	.p2align	4, 0x90
.LBB6_112:                              # %vector.body566
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdx), %xmm1
	movups	(%rdx), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rdi), %xmm3
	movups	(%rdi), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rdi)
	movups	%xmm4, (%rdi)
	addq	$32, %rdx
	addq	$32, %rdi
	addq	$-8, %rsi
	jne	.LBB6_112
# BB#113:                               # %middle.block567
	testq	%r9, %r9
	jne	.LBB6_102
	jmp	.LBB6_114
.LBB6_101:
	movl	$1, %ebp
.LBB6_102:                              # %scalar.ph568.preheader
	subl	%ebp, %r11d
	testb	$1, %r11b
	movq	%rbp, %rsi
	je	.LBB6_104
# BB#103:                               # %scalar.ph568.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%r8,%rbp,4), %xmm0
	addss	(%rbx,%rbp,4), %xmm0
	movss	%xmm0, (%rbx,%rbp,4)
	leaq	1(%rbp), %rsi
.LBB6_104:                              # %scalar.ph568.prol.loopexit
	cmpq	%rbp, %r10
	je	.LBB6_114
# BB#105:                               # %scalar.ph568.preheader.new
	subq	%rsi, %rcx
	leaq	(%r8,%rsi,4), %rdx
	leaq	4(%rbx,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB6_106:                              # %scalar.ph568
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB6_106
	jmp	.LBB6_114
.LBB6_99:
	movb	$1, 64(%rsp)            # 1-byte Folded Spill
	jmp	.LBB6_136
.LBB6_75:
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB6_80
.LBB6_78:                               # %.lr.ph404.new
	subq	%rdx, %rcx
	leaq	4(%rbx,%rdx,4), %rsi
	leaq	1(%rdx), %rdi
	imull	%r8d, %edx
	imull	%r8d, %edi
	addl	%r8d, %r8d
	xorl	%ebp, %ebp
	movsd	.LCPI6_4(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_79:                               # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%r8d, %ebp
	addq	$-2, %rcx
	jne	.LBB6_79
.LBB6_80:                               # %.preheader355
	testl	%r12d, %r12d
	jle	.LBB6_114
# BB#81:                                # %.lr.ph402
	movq	A__align_gapmap.initverticalw(%rip), %rsi
	movslq	offset(%rip), %r8
	leaq	1(%r12), %rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB6_82
# BB#116:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r8d, %xmm0
	mulsd	.LCPI6_5(%rip), %xmm0
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rsi)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB6_118
	jmp	.LBB6_114
.LBB6_82:
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB6_114
.LBB6_118:                              # %.lr.ph402.new
	subq	%rdx, %rcx
	leaq	4(%rsi,%rdx,4), %rsi
	leaq	1(%rdx), %rdi
	imull	%r8d, %edx
	imull	%r8d, %edi
	addl	%r8d, %r8d
	xorl	%ebp, %ebp
	movsd	.LCPI6_4(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_119:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%r8d, %ebp
	addq	$-2, %rcx
	jne	.LBB6_119
.LBB6_114:                              # %.preheader352
	movq	%r15, %rbp
	testl	%ebp, %ebp
	setle	64(%rsp)                # 1-byte Folded Spill
	jle	.LBB6_115
# BB#120:                               # %.lr.ph395
	movq	A__align_gapmap.ogcp1(%rip), %rdx
	leaq	4(%rdx), %rax
	movq	A__align_gapmap.m(%rip), %r9
	movq	A__align_gapmap.mp(%rip), %r8
	leaq	1(%rbp), %r14
	movl	%r14d, %ecx
	leaq	-1(%rcx), %r11
	cmpq	$7, %r11
	jbe	.LBB6_121
# BB#126:                               # %min.iters.checked611
	movl	%ebp, %r10d
	andl	$7, %r10d
	movq	%r11, %rdi
	subq	%r10, %rdi
	je	.LBB6_121
# BB#127:                               # %vector.memcheck632
	leaq	4(%r9), %r15
	leaq	(%r9,%rcx,4), %rbp
	leaq	-4(%rbx,%rcx,4), %rsi
	cmpq	%rsi, %r15
	sbbb	%sil, %sil
	cmpq	%rbp, %rbx
	sbbb	%r12b, %r12b
	andb	%sil, %r12b
	cmpq	%rdx, %r9
	sbbb	%dl, %dl
	cmpq	%rbp, %rax
	sbbb	%sil, %sil
	movl	$1, %r15d
	testb	$1, %r12b
	jne	.LBB6_128
# BB#129:                               # %vector.memcheck632
	andb	%sil, %dl
	andb	$1, %dl
	movq	16(%rsp), %r12          # 8-byte Reload
	jne	.LBB6_122
# BB#130:                               # %vector.body607.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%rbx), %rdx
	leaq	20(%r9), %rbp
	leaq	20(%r8), %rsi
	xorpd	%xmm1, %xmm1
	leaq	1(%rdi), %r15
	.p2align	4, 0x90
.LBB6_131:                              # %vector.body607
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdx), %xmm2
	movups	(%rdx), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm0, %xmm3
	movups	%xmm2, -16(%rbp)
	movups	%xmm3, (%rbp)
	movupd	%xmm1, -16(%rsi)
	movupd	%xmm1, (%rsi)
	addq	$32, %rdx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB6_131
# BB#132:                               # %middle.block608
	testq	%r10, %r10
	jne	.LBB6_122
# BB#133:
	movq	(%rsp), %rbp            # 8-byte Reload
	jmp	.LBB6_136
.LBB6_121:
	movl	$1, %r15d
.LBB6_122:                              # %scalar.ph609.preheader
	subl	%r15d, %r14d
	testb	$1, %r14b
	movq	%r15, %rdi
	je	.LBB6_124
# BB#123:                               # %scalar.ph609.prol
	movss	-4(%rbx,%r15,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%r9,%r15,4)
	movl	$0, (%r8,%r15,4)
	leaq	1(%r15), %rdi
.LBB6_124:                              # %scalar.ph609.prol.loopexit
	cmpq	%r15, %r11
	jne	.LBB6_134
# BB#125:
	movq	(%rsp), %rbp            # 8-byte Reload
	jmp	.LBB6_136
.LBB6_115:
	movb	$1, 64(%rsp)            # 1-byte Folded Spill
	jmp	.LBB6_136
.LBB6_134:                              # %scalar.ph609.preheader.new
	subq	%rdi, %rcx
	leaq	(%rbx,%rdi,4), %rdx
	leaq	4(%r9,%rdi,4), %rsi
	leaq	4(%r8,%rdi,4), %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	.p2align	4, 0x90
.LBB6_135:                              # %scalar.ph609
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, -4(%rsi)
	movl	$0, -4(%rdi)
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%rsi)
	movl	$0, (%rdi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rcx
	jne	.LBB6_135
.LBB6_136:                              # %._crit_edge396
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	xorps	%xmm5, %xmm5
	testl	%ebp, %ebp
	xorpd	%xmm0, %xmm0
	je	.LBB6_138
# BB#137:
	movq	%rbp, %rax
	shlq	$32, %rax
	addq	%rcx, %rax
	sarq	$30, %rax
	movss	(%rbx,%rax), %xmm0      # xmm0 = mem[0],zero,zero,zero
.LBB6_138:
	movq	A__align_gapmap.lastverticalw(%rip), %r15
	movss	%xmm0, (%r15)
	movl	outgap(%rip), %edx
	cmpl	$1, %edx
	movl	%r12d, %eax
	sbbl	$-1, %eax
	cmpl	$2, %eax
	jl	.LBB6_139
# BB#140:                               # %.lr.ph388
	movl	%edx, 152(%rsp)         # 4-byte Spill
	movq	%r15, 88(%rsp)          # 8-byte Spill
	movq	A__align_gapmap.initverticalw(%rip), %rdi
	movq	A__align_gapmap.cpmx1(%rip), %rsi
	movq	A__align_gapmap.floatwork(%rip), %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	A__align_gapmap.intwork(%rip), %rdx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	movq	impmtx(%rip), %rdx
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	movq	A__align_gapmap.ogcp2(%rip), %r9
	movq	A__align_gapmap.ijp(%rip), %rdx
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movq	A__align_gapmap.m(%rip), %r14
	movq	A__align_gapmap.mp(%rip), %rdx
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	A__align_gapmap.fgcp2(%rip), %r8
	movq	A__align_gapmap.fgcp1(%rip), %rdx
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	A__align_gapmap.ogcp1(%rip), %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	%rbp, %rdx
	shlq	$32, %rdx
	addq	%rcx, %rdx
	sarq	$32, %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	%ebp, %eax
	andl	$1, %eax
	movl	%eax, 156(%rsp)         # 4-byte Spill
	xorps	%xmm5, %xmm5
	movq	%rbp, %r15
	leal	-1(%rbp), %r10d
	movq	368(%rsp), %rax
	leaq	4(%rax), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	$1, %r13d
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_141:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_142 Depth 2
                                        #       Child Loop BB6_143 Depth 3
                                        #     Child Loop BB6_146 Depth 2
                                        #       Child Loop BB6_148 Depth 3
                                        #     Child Loop BB6_155 Depth 2
                                        #     Child Loop BB6_158 Depth 2
	movq	%rcx, %r12
	leaq	-1(%r13), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	-4(%rdi,%r13,4), %eax
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movl	%eax, (%rbx)
	movl	$n_dis_consweight_multi, %ecx
	xorl	%edx, %edx
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB6_142:                              #   Parent Loop BB6_141 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_143 Depth 3
	movl	$0, 176(%rsp,%rdx,4)
	xorpd	%xmm0, %xmm0
	movl	$1, %eax
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB6_143:                              #   Parent Loop BB6_141 Depth=1
                                        #     Parent Loop BB6_142 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-8(%rsi,%rax,8), %rbp
	movss	(%rdi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rbp,%r13,4), %xmm1
	addss	%xmm0, %xmm1
	movss	104(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movq	(%rsi,%rax,8), %rbp
	mulss	(%rbp,%r13,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rdi
	addq	$2, %rax
	cmpq	$27, %rax
	jne	.LBB6_143
# BB#144:                               #   in Loop: Header=BB6_142 Depth=2
	movss	%xmm0, 176(%rsp,%rdx,4)
	incq	%rdx
	addq	$4, %rcx
	cmpq	$26, %rdx
	jne	.LBB6_142
# BB#145:                               # %.preheader.i337
                                        #   in Loop: Header=BB6_141 Depth=1
	testl	%ebx, %ebx
	movl	%ebx, %r11d
	movq	144(%rsp), %rbx         # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%r12, %rdi
	je	.LBB6_150
	.p2align	4, 0x90
.LBB6_146:                              # %.lr.ph84.i
                                        #   Parent Loop BB6_141 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_148 Depth 3
	decl	%r11d
	movl	$0, (%rdi)
	movq	(%rbx), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	js	.LBB6_149
# BB#147:                               # %.lr.ph.preheader.i341
                                        #   in Loop: Header=BB6_146 Depth=2
	movq	(%rcx), %rbp
	addq	$4, %rax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB6_148:                              # %.lr.ph.i342
                                        #   Parent Loop BB6_141 Depth=1
                                        #     Parent Loop BB6_146 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%edx, %rdx
	movss	176(%rsp,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rbp), %xmm1
	addq	$4, %rbp
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdi)
	movl	(%rax), %edx
	addq	$4, %rax
	testl	%edx, %edx
	jns	.LBB6_148
.LBB6_149:                              # %._crit_edge.i
                                        #   in Loop: Header=BB6_146 Depth=2
	addq	$8, %rbx
	addq	$8, %rcx
	addq	$4, %rdi
	testl	%r11d, %r11d
	jne	.LBB6_146
.LBB6_150:                              # %match_calc.exit
                                        #   in Loop: Header=BB6_141 Depth=1
	cmpq	$0, 344(%rsp)
	movq	%r15, %rbp
	je	.LBB6_156
# BB#151:                               #   in Loop: Header=BB6_141 Depth=1
	testl	%ebp, %ebp
	je	.LBB6_156
# BB#152:                               # %.lr.ph.i346.preheader
                                        #   in Loop: Header=BB6_141 Depth=1
	movq	360(%rsp), %rax
	movslq	(%rax,%r13,4), %rax
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %rcx
	cmpl	$0, 156(%rsp)           # 4-byte Folded Reload
	movq	368(%rsp), %rdi
	movq	%r12, %rdx
	movl	%ebp, %eax
	je	.LBB6_154
# BB#153:                               # %.lr.ph.i346.prol
                                        #   in Loop: Header=BB6_141 Depth=1
	movq	368(%rsp), %rax
	movslq	(%rax), %rax
	movss	(%rcx,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	leaq	4(%r12), %rdx
	addss	(%r12), %xmm0
	movss	%xmm0, (%r12)
	movq	160(%rsp), %rdi         # 8-byte Reload
	movl	%r10d, %eax
.LBB6_154:                              # %.lr.ph.i346.prol.loopexit
                                        #   in Loop: Header=BB6_141 Depth=1
	cmpl	$1, %ebp
	je	.LBB6_156
	.p2align	4, 0x90
.LBB6_155:                              # %.lr.ph.i346
                                        #   Parent Loop BB6_141 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rdi), %rbp
	movss	(%rcx,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	addl	$-2, %eax
	movslq	4(%rdi), %rbp
	leaq	8(%rdi), %rdi
	movss	(%rcx,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	4(%rdx), %xmm0
	movss	%xmm0, 4(%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB6_155
	.p2align	4, 0x90
.LBB6_156:                              # %imp_match_out_vead_gapmap.exit348
                                        #   in Loop: Header=BB6_141 Depth=1
	movq	80(%rsp), %rdi          # 8-byte Reload
	movl	(%rdi,%r13,4), %eax
	movl	%eax, (%r12)
	movq	56(%rsp), %rcx          # 8-byte Reload
	movss	(%rcx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%r9), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm0
	movss	%xmm0, A__align_gapmap.mi(%rip)
	xorl	%r11d, %r11d
	cmpb	$0, 64(%rsp)            # 1-byte Folded Reload
	jne	.LBB6_168
# BB#157:                               # %.lr.ph381.preheader
                                        #   in Loop: Header=BB6_141 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	movss	-4(%rax,%r13,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movq	104(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r13,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r13,8), %rdx
	xorl	%ebx, %ebx
	movl	$-1, %r15d
	xorl	%r11d, %r11d
	movq	120(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB6_158
	.p2align	4, 0x90
.LBB6_288:                              # %..lr.ph381_crit_edge
                                        #   in Loop: Header=BB6_158 Depth=2
	movss	4(%rcx,%rbx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	incq	%rbx
	decl	%r15d
.LBB6_158:                              # %.lr.ph381
                                        #   Parent Loop BB6_141 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, 4(%rdx,%rbx,4)
	movss	(%r8,%rbx,4), %xmm4     # xmm4 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	ucomiss	%xmm3, %xmm4
	movaps	%xmm3, %xmm5
	jbe	.LBB6_160
# BB#159:                               #   in Loop: Header=BB6_158 Depth=2
	leal	(%r11,%r15), %eax
	movl	%eax, 4(%rdx,%rbx,4)
	movaps	%xmm4, %xmm5
.LBB6_160:                              #   in Loop: Header=BB6_158 Depth=2
	addss	4(%r9,%rbx,4), %xmm3
	ucomiss	%xmm0, %xmm3
	jb	.LBB6_162
# BB#161:                               #   in Loop: Header=BB6_158 Depth=2
	movss	%xmm3, A__align_gapmap.mi(%rip)
	movaps	%xmm3, %xmm0
	movl	%ebx, %r11d
.LBB6_162:                              #   in Loop: Header=BB6_158 Depth=2
	movss	4(%r14,%rbx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	ucomiss	%xmm5, %xmm4
	jbe	.LBB6_164
# BB#163:                               #   in Loop: Header=BB6_158 Depth=2
	movl	%r13d, %eax
	subl	4(%rbp,%rbx,4), %eax
	movl	%eax, 4(%rdx,%rbx,4)
	movaps	%xmm4, %xmm5
.LBB6_164:                              #   in Loop: Header=BB6_158 Depth=2
	movss	(%rcx,%rbx,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm4
	ucomiss	%xmm3, %xmm4
	jb	.LBB6_166
# BB#165:                               #   in Loop: Header=BB6_158 Depth=2
	movss	%xmm4, 4(%r14,%rbx,4)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, 4(%rbp,%rbx,4)
.LBB6_166:                              #   in Loop: Header=BB6_158 Depth=2
	movss	4(%r12,%rbx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm3
	movss	%xmm3, 4(%r12,%rbx,4)
	cmpl	%ebx, %r10d
	jne	.LBB6_288
# BB#167:                               #   in Loop: Header=BB6_141 Depth=1
	movq	(%rsp), %r15            # 8-byte Reload
.LBB6_168:                              # %._crit_edge382
                                        #   in Loop: Header=BB6_141 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%r12,%rax,4), %eax
	movq	88(%rsp), %rdx          # 8-byte Reload
	movl	%eax, (%rdx,%r13,4)
	incq	%r13
	cmpq	136(%rsp), %r13         # 8-byte Folded Reload
	movq	%r12, %rbx
	jne	.LBB6_141
# BB#169:                               # %._crit_edge389
	movl	%r11d, A__align_gapmap.mpi(%rip)
	movq	96(%rsp), %r13          # 8-byte Reload
	movq	%r15, %rbp
	movq	88(%rsp), %r15          # 8-byte Reload
	movl	152(%rsp), %edx         # 4-byte Reload
	testl	%edx, %edx
	jne	.LBB6_185
	jmp	.LBB6_171
.LBB6_139:
	movq	%rbx, %r12
	testl	%edx, %edx
	jne	.LBB6_185
.LBB6_171:                              # %.preheader351
	cmpb	$0, 64(%rsp)            # 1-byte Folded Reload
	jne	.LBB6_178
# BB#172:                               # %.lr.ph367
	movslq	offset(%rip), %rax
	movq	%rbp, %rdx
	incq	%rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB6_173
# BB#174:
	leal	-1(%rbp), %edx
	imull	%eax, %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	mulsd	.LCPI6_5(%rip), %xmm0
	movss	4(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%r12)
	movl	$2, %ebx
	cmpq	$2, %rcx
	jne	.LBB6_176
	jmp	.LBB6_178
.LBB6_42:                               # %vector.body.preheader
	movapd	.LCPI6_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI6_2(%rip), %xmm1   # xmm1 = [5.000000e-01,5.000000e-01]
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB6_43:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	(%rcx), %xmm2
	cvtps2pd	8(%rcx), %xmm3
	movapd	%xmm0, %xmm4
	subpd	%xmm3, %xmm4
	movapd	%xmm0, %xmm3
	subpd	%xmm2, %xmm3
	mulpd	%xmm1, %xmm3
	mulpd	%xmm1, %xmm4
	cvtpd2ps	%xmm4, %xmm2
	cvtpd2ps	%xmm3, %xmm3
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	movupd	%xmm3, (%rcx)
	cvtps2pd	(%rdx), %xmm2
	cvtps2pd	8(%rdx), %xmm3
	movapd	%xmm0, %xmm4
	subpd	%xmm3, %xmm4
	movapd	%xmm0, %xmm3
	subpd	%xmm2, %xmm3
	mulpd	%xmm1, %xmm3
	mulpd	%xmm1, %xmm4
	cvtpd2ps	%xmm4, %xmm2
	cvtpd2ps	%xmm3, %xmm3
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	movupd	%xmm3, (%rdx)
	addq	$16, %rcx
	addq	$16, %rdx
	addq	$-4, %rsi
	jne	.LBB6_43
# BB#44:                                # %middle.block
	testq	%rdi, %rdi
	jne	.LBB6_34
	jmp	.LBB6_45
.LBB6_56:                               # %vector.body501.preheader
	movapd	.LCPI6_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI6_2(%rip), %xmm1   # xmm1 = [5.000000e-01,5.000000e-01]
	movq	%rbp, %rsi
	movq	%r9, %rdx
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB6_57:                               # %vector.body501
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	(%rcx), %xmm2
	cvtps2pd	8(%rcx), %xmm3
	movapd	%xmm0, %xmm4
	subpd	%xmm3, %xmm4
	movapd	%xmm0, %xmm3
	subpd	%xmm2, %xmm3
	mulpd	%xmm1, %xmm3
	mulpd	%xmm1, %xmm4
	cvtpd2ps	%xmm4, %xmm2
	cvtpd2ps	%xmm3, %xmm3
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	movupd	%xmm3, (%rcx)
	cvtps2pd	(%rdx), %xmm2
	cvtps2pd	8(%rdx), %xmm3
	movapd	%xmm0, %xmm4
	subpd	%xmm3, %xmm4
	movapd	%xmm0, %xmm3
	subpd	%xmm2, %xmm3
	mulpd	%xmm1, %xmm3
	mulpd	%xmm1, %xmm4
	cvtpd2ps	%xmm4, %xmm2
	cvtpd2ps	%xmm3, %xmm3
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	movupd	%xmm3, (%rdx)
	addq	$16, %rcx
	addq	$16, %rdx
	addq	$-4, %rsi
	jne	.LBB6_57
# BB#58:                                # %middle.block502
	testq	%rdi, %rdi
	jne	.LBB6_48
	jmp	.LBB6_59
.LBB6_173:
	movl	$1, %ebx
	cmpq	$2, %rcx
	je	.LBB6_178
.LBB6_176:                              # %.lr.ph367.new
	subq	%rbx, %rcx
	movl	%ebp, %edx
	subl	%ebx, %edx
	imull	%eax, %edx
	leaq	4(%r12,%rbx,4), %rsi
	leal	-1(%rbp), %edi
	subl	%ebx, %edi
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI6_4(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_177:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	subl	%eax, %ebp
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB6_177
.LBB6_178:                              # %.preheader350
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB6_185
# BB#179:                               # %.lr.ph365
	xorps	%xmm0, %xmm0
	cvtsi2sdl	offset(%rip), %xmm0
	movq	16(%rsp), %rcx          # 8-byte Reload
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	incq	%rcx
	movl	%ecx, %eax
	testb	$1, %cl
	jne	.LBB6_180
# BB#181:
	movsd	.LCPI6_5(%rip), %xmm2   # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movss	4(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm2, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, 4(%r15)
	movl	$2, %ecx
	cmpq	$2, %rax
	jne	.LBB6_183
	jmp	.LBB6_185
.LBB6_180:
	movl	$1, %ecx
	cmpq	$2, %rax
	je	.LBB6_185
.LBB6_183:
	movsd	.LCPI6_4(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB6_184:                              # =>This Inner Loop Header: Depth=1
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%ecx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	(%r15,%rcx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, (%r15,%rcx,4)
	leal	1(%rcx), %edx
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%edx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	4(%r15,%rcx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, 4(%r15,%rcx,4)
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jne	.LBB6_184
.LBB6_185:                              # %.loopexit
	movq	A__align_gapmap.mseq1(%rip), %r8
	movq	A__align_gapmap.mseq2(%rip), %r9
	movq	A__align_gapmap.ijp(%rip), %rbp
	cmpq	$0, 344(%rsp)
	movss	%xmm5, (%rsp)           # 4-byte Spill
	je	.LBB6_277
# BB#186:
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	callq	strlen
	movq	%rax, %rbx
	movq	(%r13), %rdi
	callq	strlen
	movq	%rax, %r13
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	leal	1(%r13,%rbx), %ebx
	movl	%ebx, %edi
	callq	AllocateCharVec
	movq	%rax, %r14
	movl	%ebx, %edi
	callq	AllocateCharVec
	movq	48(%rsp), %r9           # 8-byte Reload
	cmpl	$1, outgap(%rip)
	je	.LBB6_187
# BB#198:
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%esi, %esi
	jle	.LBB6_204
# BB#199:                               # %.lr.ph55.i
	movslq	%esi, %rbp
	movslq	%r13d, %rcx
	movl	%ebp, %edx
	addq	$4, %r15
	decq	%rdx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movaps	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB6_202
	jmp	.LBB6_201
	.p2align	4, 0x90
.LBB6_289:                              # %._crit_edge97.i
                                        #   in Loop: Header=BB6_202 Depth=1
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %r15
	decq	%rdx
	decl	%esi
	ucomiss	%xmm1, %xmm0
	jb	.LBB6_202
.LBB6_201:
	movq	(%r9,%rbp,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movaps	%xmm0, %xmm1
.LBB6_202:                              # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	jne	.LBB6_289
# BB#203:
	movaps	%xmm1, %xmm0
.LBB6_204:                              # %.preheader9.i
	testl	%r13d, %r13d
	jle	.LBB6_187
# BB#205:                               # %.lr.ph51.i
	movslq	8(%rsp), %r8            # 4-byte Folded Reload
	movslq	%r13d, %rcx
	movl	%ecx, %edx
	testb	$1, %r13b
	jne	.LBB6_207
# BB#206:
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB6_211
	jmp	.LBB6_187
.LBB6_277:
	subq	$8, %rsp
.Lcfi91:
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	80(%rsp), %r14          # 8-byte Reload
	pushq	%r14
.Lcfi92:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rbx          # 8-byte Reload
	pushq	%rbx
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	callq	Atracking
	addq	$32, %rsp
.Lcfi95:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB6_278
.LBB6_207:
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB6_209
# BB#208:
	movl	$1, %esi
	cmpq	$1, %rdx
	jne	.LBB6_211
	jmp	.LBB6_187
.LBB6_209:
	movl	%r13d, %esi
	negl	%esi
	movq	(%r9,%r8,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movl	$1, %esi
	movaps	%xmm1, %xmm0
	cmpq	$1, %rdx
	je	.LBB6_187
.LBB6_211:                              # %.lr.ph51.i.new
	movl	%r13d, %edi
	negl	%edi
	.p2align	4, 0x90
.LBB6_212:                              # =>This Inner Loop Header: Depth=1
	movss	(%r12,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB6_214
# BB#213:                               #   in Loop: Header=BB6_212 Depth=1
	leal	(%rdi,%rsi), %ebp
	movq	(%r9,%r8,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB6_214:                              #   in Loop: Header=BB6_212 Depth=1
	movss	4(%r12,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB6_216
# BB#215:                               #   in Loop: Header=BB6_212 Depth=1
	leal	1(%rdi,%rsi), %ebp
	movq	(%r9,%r8,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB6_216:                              #   in Loop: Header=BB6_212 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB6_212
.LBB6_187:                              # %.preheader8.i
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	js	.LBB6_193
# BB#188:                               # %.lr.ph48.preheader.i
	movq	8(%rsp), %rsi           # 8-byte Reload
	incq	%rsi
	movl	%esi, %ebp
	leaq	-1(%rbp), %rcx
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB6_190
	.p2align	4, 0x90
.LBB6_189:                              # %.lr.ph48.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB6_189
.LBB6_190:                              # %.lr.ph48.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB6_193
# BB#191:                               # %.lr.ph48.preheader.i.new
	negq	%rbp
	leaq	4(%rdx,%rbp), %r8
	leaq	56(%r9,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_192:                              # %.lr.ph48.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %ebp
	leal	-3(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-48(%rcx,%rsi,8), %rdi
	leal	-2(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-40(%rcx,%rsi,8), %rdi
	leal	-1(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-32(%rcx,%rsi,8), %rdi
	movl	%ebp, (%rdi)
	movq	-24(%rcx,%rsi,8), %rdi
	leal	1(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-16(%rcx,%rsi,8), %rdi
	leal	2(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-8(%rcx,%rsi,8), %rdi
	leal	3(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	(%rcx,%rsi,8), %rdi
	leal	4(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	leaq	8(%r8,%rsi), %rdi
	addq	$8, %rsi
	cmpq	$4, %rdi
	jne	.LBB6_192
.LBB6_193:                              # %.preheader7.i
	movq	8(%rsp), %rsi           # 8-byte Reload
	leal	(%r13,%rsi), %ecx
	movl	%ecx, 80(%rsp)          # 4-byte Spill
	testl	%r13d, %r13d
	js	.LBB6_221
# BB#194:                               # %.lr.ph45.i
	movq	(%r9), %rcx
	movq	%r13, %rsi
	incq	%rsi
	movl	%esi, %ebp
	cmpq	$7, %rbp
	jbe	.LBB6_195
# BB#217:                               # %min.iters.checked650
	andl	$7, %esi
	movq	%rbp, %rbx
	subq	%rsi, %rbx
	je	.LBB6_195
# BB#218:                               # %vector.body646.preheader
	leaq	16(%rcx), %rdi
	movdqa	.LCPI6_6(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI6_7(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI6_8(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB6_219:                              # %vector.body646
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdi)
	movdqu	%xmm4, (%rdi)
	paddd	%xmm3, %xmm0
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB6_219
# BB#220:                               # %middle.block647
	testq	%rsi, %rsi
	movq	8(%rsp), %rsi           # 8-byte Reload
	jne	.LBB6_196
	jmp	.LBB6_221
.LBB6_195:
	xorl	%ebx, %ebx
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB6_196:                              # %scalar.ph648.preheader
	movl	%ebx, %edx
	notl	%edx
	leaq	(%rcx,%rbx,4), %rcx
	subq	%rbx, %rbp
	.p2align	4, 0x90
.LBB6_197:                              # %scalar.ph648
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rcx)
	decl	%edx
	addq	$4, %rcx
	decq	%rbp
	jne	.LBB6_197
.LBB6_221:                              # %._crit_edge46.i
	movslq	%esi, %rdx
	leaq	(%r14,%rdx), %r12
	movslq	%r13d, %rcx
	movb	$0, (%rcx,%r12)
	addq	%rcx, %r12
	addq	%rax, %rdx
	leaq	(%rdx,%rcx), %r15
	movb	$0, (%rcx,%rdx)
	movq	352(%rsp), %rcx
	movl	$0, (%rcx)
	cmpl	$0, 80(%rsp)            # 4-byte Folded Reload
	movq	%r14, 120(%rsp)         # 8-byte Spill
	movq	%rax, 112(%rsp)         # 8-byte Spill
	js	.LBB6_270
# BB#222:                               # %.lr.ph37.preheader.i
	xorl	%r10d, %r10d
	movq	impmtx(%rip), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movdqa	.LCPI6_10(%rip), %xmm0  # xmm0 = [45,0,45,0,45,0,45,0,45,0,45,0,45,0,45,0]
	packuswb	%xmm0, %xmm0
	movdqa	.LCPI6_9(%rip), %xmm1   # xmm1 = [111,0,111,0,111,0,111,0,111,0,111,0,111,0,111,0]
	packuswb	%xmm1, %xmm1
	movq	%r13, %rcx
	movl	%esi, %r13d
	movq	%rcx, %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	%ecx, %r14d
	.p2align	4, 0x90
.LBB6_223:                              # %.lr.ph37.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_239 Depth 2
                                        #     Child Loop BB6_243 Depth 2
                                        #     Child Loop BB6_246 Depth 2
                                        #     Child Loop BB6_255 Depth 2
                                        #     Child Loop BB6_259 Depth 2
                                        #     Child Loop BB6_262 Depth 2
	movq	%rsi, %r11
	movslq	%r13d, %rcx
	movq	(%r9,%rcx,8), %rax
	movslq	%r14d, %rdx
	movl	(%rax,%rdx,4), %esi
	testl	%esi, %esi
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	js	.LBB6_224
# BB#225:                               #   in Loop: Header=BB6_223 Depth=1
	je	.LBB6_227
# BB#226:                               #   in Loop: Header=BB6_223 Depth=1
	movl	%r13d, %edx
	subl	%esi, %edx
	jmp	.LBB6_228
	.p2align	4, 0x90
.LBB6_224:                              #   in Loop: Header=BB6_223 Depth=1
	leal	-1(%r13), %edx
	jmp	.LBB6_229
	.p2align	4, 0x90
.LBB6_227:                              #   in Loop: Header=BB6_223 Depth=1
	leal	-1(%r13), %edx
.LBB6_228:                              #   in Loop: Header=BB6_223 Depth=1
	movl	$-1, %esi
.LBB6_229:                              #   in Loop: Header=BB6_223 Depth=1
	movl	%r13d, %eax
	subl	%edx, %eax
	decl	%eax
	movl	%esi, 32(%rsp)          # 4-byte Spill
	je	.LBB6_248
# BB#230:                               # %.lr.ph18.preheader.i
                                        #   in Loop: Header=BB6_223 Depth=1
	leal	-2(%r13), %ecx
	subl	%edx, %ecx
	movq	%rcx, %r8
	negq	%r8
	leaq	1(%rcx), %rbp
	cmpq	$16, %rbp
	jae	.LBB6_232
# BB#231:                               #   in Loop: Header=BB6_223 Depth=1
	movq	%r15, %rcx
	movq	%r12, %rdi
	jmp	.LBB6_241
	.p2align	4, 0x90
.LBB6_232:                              # %min.iters.checked704
                                        #   in Loop: Header=BB6_223 Depth=1
	movl	%ebp, %r9d
	andl	$15, %r9d
	subq	%r9, %rbp
	je	.LBB6_233
# BB#235:                               # %vector.memcheck717
                                        #   in Loop: Header=BB6_223 Depth=1
	leaq	-1(%r12,%r8), %rsi
	cmpq	%r15, %rsi
	jae	.LBB6_238
# BB#236:                               # %vector.memcheck717
                                        #   in Loop: Header=BB6_223 Depth=1
	leaq	-1(%r15,%r8), %rsi
	cmpq	%r12, %rsi
	jae	.LBB6_238
# BB#237:                               #   in Loop: Header=BB6_223 Depth=1
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	8(%rsp), %r11           # 8-byte Reload
	jmp	.LBB6_234
.LBB6_233:                              #   in Loop: Header=BB6_223 Depth=1
	movq	%r15, %rcx
	movq	%r12, %rdi
.LBB6_234:                              # %.lr.ph18.i.preheader
                                        #   in Loop: Header=BB6_223 Depth=1
	movl	32(%rsp), %esi          # 4-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	jmp	.LBB6_241
.LBB6_238:                              # %vector.body700.preheader
                                        #   in Loop: Header=BB6_223 Depth=1
	subl	%ebp, %eax
	leaq	-1(%r9), %rdi
	subq	%rcx, %rdi
	leaq	(%r15,%rdi), %rcx
	addq	%r12, %rdi
	leaq	-8(%r12), %rsi
	leaq	-8(%r15), %rbx
	.p2align	4, 0x90
.LBB6_239:                              # %vector.body700
                                        #   Parent Loop BB6_223 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm1, (%rsi)
	movq	%xmm1, -8(%rsi)
	movq	%xmm0, (%rbx)
	movq	%xmm0, -8(%rbx)
	addq	$-16, %rsi
	addq	$-16, %rbx
	addq	$-16, %rbp
	jne	.LBB6_239
# BB#240:                               # %middle.block701
                                        #   in Loop: Header=BB6_223 Depth=1
	testq	%r9, %r9
	movq	8(%rsp), %r11           # 8-byte Reload
	movl	32(%rsp), %esi          # 4-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	je	.LBB6_247
	.p2align	4, 0x90
.LBB6_241:                              # %.lr.ph18.i.preheader
                                        #   in Loop: Header=BB6_223 Depth=1
	leal	-1(%rax), %ebp
	movl	%eax, %ebx
	andl	$7, %ebx
	je	.LBB6_244
# BB#242:                               # %.lr.ph18.i.prol.preheader
                                        #   in Loop: Header=BB6_223 Depth=1
	negl	%ebx
	.p2align	4, 0x90
.LBB6_243:                              # %.lr.ph18.i.prol
                                        #   Parent Loop BB6_223 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, -1(%rdi)
	decq	%rdi
	movb	$45, -1(%rcx)
	decq	%rcx
	decl	%eax
	incl	%ebx
	jne	.LBB6_243
.LBB6_244:                              # %.lr.ph18.i.prol.loopexit
                                        #   in Loop: Header=BB6_223 Depth=1
	cmpl	$7, %ebp
	jb	.LBB6_247
# BB#245:                               # %.lr.ph18.i.preheader.new
                                        #   in Loop: Header=BB6_223 Depth=1
	decq	%rcx
	decq	%rdi
	.p2align	4, 0x90
.LBB6_246:                              # %.lr.ph18.i
                                        #   Parent Loop BB6_223 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, (%rdi)
	movb	$45, (%rcx)
	movb	$111, -1(%rdi)
	movb	$45, -1(%rcx)
	movb	$111, -2(%rdi)
	movb	$45, -2(%rcx)
	movb	$111, -3(%rdi)
	movb	$45, -3(%rcx)
	movb	$111, -4(%rdi)
	movb	$45, -4(%rcx)
	movb	$111, -5(%rdi)
	movb	$45, -5(%rcx)
	movb	$111, -6(%rdi)
	movb	$45, -6(%rcx)
	movb	$111, -7(%rdi)
	movb	$45, -7(%rcx)
	addq	$-8, %rcx
	addq	$-8, %rdi
	addl	$-8, %eax
	jne	.LBB6_246
.LBB6_247:                              # %._crit_edge19.loopexit.i
                                        #   in Loop: Header=BB6_223 Depth=1
	leaq	-1(%r12,%r8), %r12
	leaq	-1(%r15,%r8), %r15
	leal	-1(%r10,%r13), %r10d
	subl	%edx, %r10d
.LBB6_248:                              # %._crit_edge19.i
                                        #   in Loop: Header=BB6_223 Depth=1
	cmpl	$-1, %esi
	je	.LBB6_249
# BB#250:                               # %.lr.ph26.preheader.i
                                        #   in Loop: Header=BB6_223 Depth=1
	movl	%r14d, 88(%rsp)         # 4-byte Spill
	movl	%esi, %r14d
	notl	%r14d
	movl	$-2, %r8d
	subl	%esi, %r8d
	movq	%r8, %r9
	negq	%r9
	leaq	1(%r8), %rcx
	cmpq	$16, %rcx
	movl	%r14d, %edi
	movq	%r15, %rbx
	movq	%r12, %rbp
	movq	%r11, %rsi
	jb	.LBB6_257
# BB#251:                               # %min.iters.checked668
                                        #   in Loop: Header=BB6_223 Depth=1
	movl	%ecx, %r11d
	andl	$15, %r11d
	subq	%r11, %rcx
	movl	%r14d, %edi
	movq	%r15, %rbx
	movq	%r12, %rbp
	je	.LBB6_257
# BB#252:                               # %vector.memcheck681
                                        #   in Loop: Header=BB6_223 Depth=1
	leaq	-1(%r12,%r9), %rax
	cmpq	%r15, %rax
	jae	.LBB6_254
# BB#253:                               # %vector.memcheck681
                                        #   in Loop: Header=BB6_223 Depth=1
	leaq	-1(%r15,%r9), %rax
	cmpq	%r12, %rax
	movl	%r14d, %edi
	movq	%r15, %rbx
	movq	%r12, %rbp
	jb	.LBB6_257
.LBB6_254:                              # %vector.body664.preheader
                                        #   in Loop: Header=BB6_223 Depth=1
	movl	%r14d, %edi
	subl	%ecx, %edi
	leaq	-1(%r11), %rbp
	subq	%r8, %rbp
	leaq	(%r15,%rbp), %rbx
	addq	%r12, %rbp
	leaq	-8(%r12), %rsi
	leaq	-8(%r15), %rax
	.p2align	4, 0x90
.LBB6_255:                              # %vector.body664
                                        #   Parent Loop BB6_223 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm0, (%rsi)
	movq	%xmm0, -8(%rsi)
	movq	%xmm1, (%rax)
	movq	%xmm1, -8(%rax)
	addq	$-16, %rsi
	addq	$-16, %rax
	addq	$-16, %rcx
	jne	.LBB6_255
# BB#256:                               # %middle.block665
                                        #   in Loop: Header=BB6_223 Depth=1
	testq	%r11, %r11
	movq	8(%rsp), %rsi           # 8-byte Reload
	je	.LBB6_263
	.p2align	4, 0x90
.LBB6_257:                              # %.lr.ph26.i.preheader
                                        #   in Loop: Header=BB6_223 Depth=1
	leal	-1(%rdi), %r8d
	movl	%edi, %ecx
	andl	$7, %ecx
	je	.LBB6_260
# BB#258:                               # %.lr.ph26.i.prol.preheader
                                        #   in Loop: Header=BB6_223 Depth=1
	negl	%ecx
	.p2align	4, 0x90
.LBB6_259:                              # %.lr.ph26.i.prol
                                        #   Parent Loop BB6_223 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, -1(%rbp)
	decq	%rbp
	movb	$111, -1(%rbx)
	decq	%rbx
	decl	%edi
	incl	%ecx
	jne	.LBB6_259
.LBB6_260:                              # %.lr.ph26.i.prol.loopexit
                                        #   in Loop: Header=BB6_223 Depth=1
	cmpl	$7, %r8d
	jb	.LBB6_263
# BB#261:                               # %.lr.ph26.i.preheader.new
                                        #   in Loop: Header=BB6_223 Depth=1
	decq	%rbx
	decq	%rbp
	.p2align	4, 0x90
.LBB6_262:                              # %.lr.ph26.i
                                        #   Parent Loop BB6_223 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, (%rbp)
	movb	$111, (%rbx)
	movb	$45, -1(%rbp)
	movb	$111, -1(%rbx)
	movb	$45, -2(%rbp)
	movb	$111, -2(%rbx)
	movb	$45, -3(%rbp)
	movb	$111, -3(%rbx)
	movb	$45, -4(%rbp)
	movb	$111, -4(%rbx)
	movb	$45, -5(%rbp)
	movb	$111, -5(%rbx)
	movb	$45, -6(%rbp)
	movb	$111, -6(%rbx)
	movb	$45, -7(%rbp)
	movb	$111, -7(%rbx)
	addq	$-8, %rbx
	addq	$-8, %rbp
	addl	$-8, %edi
	jne	.LBB6_262
.LBB6_263:                              # %._crit_edge27.loopexit.i
                                        #   in Loop: Header=BB6_223 Depth=1
	leaq	-1(%r12,%r9), %r12
	leaq	-1(%r15,%r9), %r15
	addl	%r14d, %r10d
	movq	48(%rsp), %r9           # 8-byte Reload
	movl	88(%rsp), %r14d         # 4-byte Reload
	cmpl	%esi, %r13d
	jne	.LBB6_265
	jmp	.LBB6_267
	.p2align	4, 0x90
.LBB6_249:                              #   in Loop: Header=BB6_223 Depth=1
	movq	%r11, %rsi
	cmpl	%esi, %r13d
	je	.LBB6_267
.LBB6_265:                              # %._crit_edge27.i
                                        #   in Loop: Header=BB6_223 Depth=1
	cmpl	144(%rsp), %r14d        # 4-byte Folded Reload
	je	.LBB6_267
# BB#266:                               #   in Loop: Header=BB6_223 Depth=1
	movq	360(%rsp), %rax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movslq	(%rax,%rcx,4), %rax
	movq	136(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	368(%rsp), %rcx
	movq	64(%rsp), %rdi          # 8-byte Reload
	movslq	(%rcx,%rdi,4), %rcx
	movss	(%rax,%rcx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movq	352(%rsp), %rax
	addss	(%rax), %xmm2
	movss	%xmm2, (%rax)
.LBB6_267:                              #   in Loop: Header=BB6_223 Depth=1
	testl	%r13d, %r13d
	movl	32(%rsp), %eax          # 4-byte Reload
	jle	.LBB6_270
# BB#268:                               #   in Loop: Header=BB6_223 Depth=1
	testl	%r14d, %r14d
	jle	.LBB6_270
# BB#269:                               #   in Loop: Header=BB6_223 Depth=1
	addl	%eax, %r14d
	movb	$45, -1(%r12)
	decq	%r12
	movb	$45, -1(%r15)
	decq	%r15
	addl	$2, %r10d
	cmpl	80(%rsp), %r10d         # 4-byte Folded Reload
	movl	%edx, %r13d
	jle	.LBB6_223
.LBB6_270:                              # %._crit_edge38.i
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movq	104(%rsp), %rbx         # 8-byte Reload
	jle	.LBB6_273
# BB#271:                               # %.lr.ph13.preheader.i
	movl	24(%rsp), %r14d         # 4-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_272:                              # %.lr.ph13.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rbp), %rsi
	movq	%r12, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %rbx
	decq	%r14
	jne	.LBB6_272
.LBB6_273:                              # %.preheader.i
	movq	72(%rsp), %r14          # 8-byte Reload
	testl	%r14d, %r14d
	movq	96(%rsp), %r13          # 8-byte Reload
	movq	128(%rsp), %rbx         # 8-byte Reload
	jle	.LBB6_276
# BB#274:                               # %.lr.ph.preheader.i
	movl	%r14d, %r12d
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB6_275:                              # %.lr.ph.i336
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rbp), %rsi
	movq	%r15, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %rbx
	decq	%r12
	jne	.LBB6_275
.LBB6_276:                              # %Atracking_localhom_gapmap.exit
	movq	120(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB6_278:
	movq	A__align_gapmap.mseq1(%rip), %rax
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, %rcx
	movl	336(%rsp), %edx
	cmpl	%edx, %ecx
	jg	.LBB6_280
# BB#279:
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB6_280
# BB#281:                               # %.preheader349
	testl	%ebx, %ebx
	jle	.LBB6_284
.LBB6_282:                              # %.lr.ph363.preheader
	movl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_283:                              # %.lr.ph363
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbp,8), %rdi
	movq	A__align_gapmap.mseq1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB6_283
.LBB6_284:                              # %.preheader
	testl	%r14d, %r14d
	jle	.LBB6_287
# BB#285:                               # %.lr.ph.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_286:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbp,8), %rdi
	movq	A__align_gapmap.mseq2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB6_286
.LBB6_287:                              # %._crit_edge
	movss	(%rsp), %xmm0           # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_280:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.1, %edi
	callq	ErrorExit
	testl	%ebx, %ebx
	jg	.LBB6_282
	jmp	.LBB6_284
.LBB6_128:
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB6_122
.LBB6_93:
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB6_86
.LBB6_109:
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB6_102
.Lfunc_end6:
	.size	A__align_gapmap, .Lfunc_end6-A__align_gapmap
	.cfi_endproc

	.globl	translate_and_Calign
	.p2align	4, 0x90
	.type	translate_and_Calign,@function
translate_and_Calign:                   # @translate_and_Calign
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi102:
	.cfi_def_cfa_offset 64
.Lcfi103:
	.cfi_offset %rbx, -56
.Lcfi104:
	.cfi_offset %r12, -48
.Lcfi105:
	.cfi_offset %r13, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movq	%rsi, %r13
	movl	64(%rsp), %r15d
	cmpl	$1, %r8d
	jne	.LBB7_3
# BB#1:
	cmpl	$1, %ebp
	je	.LBB7_3
# BB#2:
	movq	(%rdi), %r14
	incl	%ebp
	jmp	.LBB7_7
.LBB7_3:
	cmpl	$1, %r8d
	je	.LBB7_6
# BB#4:
	cmpl	$1, %ebp
	jne	.LBB7_6
# BB#5:
	movq	(%r13), %r14
	incl	%r8d
	movq	%rdi, %r13
	movq	%rdx, %rcx
	movl	%r8d, %ebp
	jmp	.LBB7_7
.LBB7_6:
	movl	$.L.str.2, %edi
	callq	ErrorExit
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	xorl	%ebp, %ebp
.LBB7_7:
	leal	-2(%rbp), %r8d
	leaq	4(%rsp), %rdi
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	movq	%r13, %rsi
	movq	%r14, %rdx
	callq	Calignm1
	movq	%rax, %r12
	movq	(%r12), %rdi
	callq	strlen
	movq	%rax, %rcx
	cmpl	%r15d, %ecx
	jg	.LBB7_9
# BB#8:
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB7_9
.LBB7_10:                               # %.preheader
	leal	-1(%rbp), %r15d
	cmpl	$2, %ebp
	jl	.LBB7_13
# BB#11:                                # %.lr.ph.preheader
	movl	%r15d, %ebp
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB7_12:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	movq	(%rbx), %rsi
	callq	strcpy
	addq	$8, %r13
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB7_12
.LBB7_13:                               # %._crit_edge
	movslq	%r15d, %rax
	movq	(%r12,%rax,8), %rsi
	movq	%r14, %rdi
	callq	strcpy
	xorps	%xmm0, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_9:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
	movl	%r15d, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.1, %edi
	callq	ErrorExit
	jmp	.LBB7_10
.Lfunc_end7:
	.size	translate_and_Calign, .Lfunc_end7-translate_and_Calign
	.cfi_endproc

	.type	impmtx,@object          # @impmtx
	.local	impmtx
	.comm	impmtx,8,8
	.type	imp_match_init_strict.nocount1,@object # @imp_match_init_strict.nocount1
	.local	imp_match_init_strict.nocount1
	.comm	imp_match_init_strict.nocount1,8,8
	.type	imp_match_init_strict.nocount2,@object # @imp_match_init_strict.nocount2
	.local	imp_match_init_strict.nocount2
	.comm	imp_match_init_strict.nocount2,8,8
	.type	impalloclen,@object     # @impalloclen
	.local	impalloclen
	.comm	impalloclen,4,4
	.type	A__align.mi,@object     # @A__align.mi
	.local	A__align.mi
	.comm	A__align.mi,4,4
	.type	A__align.m,@object      # @A__align.m
	.local	A__align.m
	.comm	A__align.m,8,8
	.type	A__align.ijp,@object    # @A__align.ijp
	.local	A__align.ijp
	.comm	A__align.ijp,8,8
	.type	A__align.mpi,@object    # @A__align.mpi
	.local	A__align.mpi
	.comm	A__align.mpi,4,4
	.type	A__align.mp,@object     # @A__align.mp
	.local	A__align.mp
	.comm	A__align.mp,8,8
	.type	A__align.w1,@object     # @A__align.w1
	.local	A__align.w1
	.comm	A__align.w1,8,8
	.type	A__align.w2,@object     # @A__align.w2
	.local	A__align.w2
	.comm	A__align.w2,8,8
	.type	A__align.match,@object  # @A__align.match
	.local	A__align.match
	.comm	A__align.match,8,8
	.type	A__align.initverticalw,@object # @A__align.initverticalw
	.local	A__align.initverticalw
	.comm	A__align.initverticalw,8,8
	.type	A__align.lastverticalw,@object # @A__align.lastverticalw
	.local	A__align.lastverticalw
	.comm	A__align.lastverticalw,8,8
	.type	A__align.mseq1,@object  # @A__align.mseq1
	.local	A__align.mseq1
	.comm	A__align.mseq1,8,8
	.type	A__align.mseq2,@object  # @A__align.mseq2
	.local	A__align.mseq2
	.comm	A__align.mseq2,8,8
	.type	A__align.mseq,@object   # @A__align.mseq
	.local	A__align.mseq
	.comm	A__align.mseq,8,8
	.type	A__align.ogcp1,@object  # @A__align.ogcp1
	.local	A__align.ogcp1
	.comm	A__align.ogcp1,8,8
	.type	A__align.ogcp2,@object  # @A__align.ogcp2
	.local	A__align.ogcp2
	.comm	A__align.ogcp2,8,8
	.type	A__align.fgcp1,@object  # @A__align.fgcp1
	.local	A__align.fgcp1
	.comm	A__align.fgcp1,8,8
	.type	A__align.fgcp2,@object  # @A__align.fgcp2
	.local	A__align.fgcp2
	.comm	A__align.fgcp2,8,8
	.type	A__align.cpmx1,@object  # @A__align.cpmx1
	.local	A__align.cpmx1
	.comm	A__align.cpmx1,8,8
	.type	A__align.cpmx2,@object  # @A__align.cpmx2
	.local	A__align.cpmx2
	.comm	A__align.cpmx2,8,8
	.type	A__align.intwork,@object # @A__align.intwork
	.local	A__align.intwork
	.comm	A__align.intwork,8,8
	.type	A__align.floatwork,@object # @A__align.floatwork
	.local	A__align.floatwork
	.comm	A__align.floatwork,8,8
	.type	A__align.orlgth1,@object # @A__align.orlgth1
	.local	A__align.orlgth1
	.comm	A__align.orlgth1,4,4
	.type	A__align.orlgth2,@object # @A__align.orlgth2
	.local	A__align.orlgth2
	.comm	A__align.orlgth2,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"alloclen=%d, resultlen=%d, N=%d\n"
	.size	.L.str, 33

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"LENGTH OVER!\n"
	.size	.L.str.1, 14

	.type	A__align_gapmap.mi,@object # @A__align_gapmap.mi
	.local	A__align_gapmap.mi
	.comm	A__align_gapmap.mi,4,4
	.type	A__align_gapmap.m,@object # @A__align_gapmap.m
	.local	A__align_gapmap.m
	.comm	A__align_gapmap.m,8,8
	.type	A__align_gapmap.ijp,@object # @A__align_gapmap.ijp
	.local	A__align_gapmap.ijp
	.comm	A__align_gapmap.ijp,8,8
	.type	A__align_gapmap.mpi,@object # @A__align_gapmap.mpi
	.local	A__align_gapmap.mpi
	.comm	A__align_gapmap.mpi,4,4
	.type	A__align_gapmap.mp,@object # @A__align_gapmap.mp
	.local	A__align_gapmap.mp
	.comm	A__align_gapmap.mp,8,8
	.type	A__align_gapmap.w1,@object # @A__align_gapmap.w1
	.local	A__align_gapmap.w1
	.comm	A__align_gapmap.w1,8,8
	.type	A__align_gapmap.w2,@object # @A__align_gapmap.w2
	.local	A__align_gapmap.w2
	.comm	A__align_gapmap.w2,8,8
	.type	A__align_gapmap.match,@object # @A__align_gapmap.match
	.local	A__align_gapmap.match
	.comm	A__align_gapmap.match,8,8
	.type	A__align_gapmap.initverticalw,@object # @A__align_gapmap.initverticalw
	.local	A__align_gapmap.initverticalw
	.comm	A__align_gapmap.initverticalw,8,8
	.type	A__align_gapmap.lastverticalw,@object # @A__align_gapmap.lastverticalw
	.local	A__align_gapmap.lastverticalw
	.comm	A__align_gapmap.lastverticalw,8,8
	.type	A__align_gapmap.mseq1,@object # @A__align_gapmap.mseq1
	.local	A__align_gapmap.mseq1
	.comm	A__align_gapmap.mseq1,8,8
	.type	A__align_gapmap.mseq2,@object # @A__align_gapmap.mseq2
	.local	A__align_gapmap.mseq2
	.comm	A__align_gapmap.mseq2,8,8
	.type	A__align_gapmap.mseq,@object # @A__align_gapmap.mseq
	.local	A__align_gapmap.mseq
	.comm	A__align_gapmap.mseq,8,8
	.type	A__align_gapmap.ogcp1,@object # @A__align_gapmap.ogcp1
	.local	A__align_gapmap.ogcp1
	.comm	A__align_gapmap.ogcp1,8,8
	.type	A__align_gapmap.ogcp2,@object # @A__align_gapmap.ogcp2
	.local	A__align_gapmap.ogcp2
	.comm	A__align_gapmap.ogcp2,8,8
	.type	A__align_gapmap.fgcp1,@object # @A__align_gapmap.fgcp1
	.local	A__align_gapmap.fgcp1
	.comm	A__align_gapmap.fgcp1,8,8
	.type	A__align_gapmap.fgcp2,@object # @A__align_gapmap.fgcp2
	.local	A__align_gapmap.fgcp2
	.comm	A__align_gapmap.fgcp2,8,8
	.type	A__align_gapmap.cpmx1,@object # @A__align_gapmap.cpmx1
	.local	A__align_gapmap.cpmx1
	.comm	A__align_gapmap.cpmx1,8,8
	.type	A__align_gapmap.cpmx2,@object # @A__align_gapmap.cpmx2
	.local	A__align_gapmap.cpmx2
	.comm	A__align_gapmap.cpmx2,8,8
	.type	A__align_gapmap.intwork,@object # @A__align_gapmap.intwork
	.local	A__align_gapmap.intwork
	.comm	A__align_gapmap.intwork,8,8
	.type	A__align_gapmap.floatwork,@object # @A__align_gapmap.floatwork
	.local	A__align_gapmap.floatwork
	.comm	A__align_gapmap.floatwork,8,8
	.type	A__align_gapmap.orlgth1,@object # @A__align_gapmap.orlgth1
	.local	A__align_gapmap.orlgth1
	.comm	A__align_gapmap.orlgth1,4,4
	.type	A__align_gapmap.orlgth2,@object # @A__align_gapmap.orlgth2
	.local	A__align_gapmap.orlgth2
	.comm	A__align_gapmap.orlgth2,4,4
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"ERROR in translate_and_Calign"
	.size	.L.str.2, 30


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
