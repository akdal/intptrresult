	.text
	.file	"macroblock.bc"
	.globl	set_MB_parameters
	.p2align	4, 0x90
	.type	set_MB_parameters,@function
set_MB_parameters:                      # @set_MB_parameters
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movl	%edi, %ebx
	movq	img(%rip), %rdx
	movl	%ebx, 12(%rdx)
	leaq	160(%rdx), %rsi
	addq	$164, %rdx
	callq	*get_mb_block_pos(%rip)
	movq	img(%rip), %rax
	movl	160(%rax), %ecx
	leal	(,%rcx,4), %edx
	movl	%edx, 168(%rax)
	movl	164(%rax), %esi
	leal	(,%rsi,4), %edx
	movl	%edx, 172(%rax)
	shll	$4, %ecx
	movl	%ecx, 176(%rax)
	movl	%esi, %edx
	shll	$4, %edx
	movl	%edx, 180(%rax)
	movl	%ecx, 192(%rax)
	cmpl	$0, 15268(%rax)
	je	.LBB0_4
# BB#1:
	movq	14224(%rax), %r8
	movslq	%ebx, %r9
	imulq	$536, %r9, %rdi         # imm = 0x218
	cmpl	$0, 424(%r8,%rdi)
	je	.LBB0_3
# BB#2:
	andl	$1, %ebx
	movl	$imgY_org_bot, %r10d
	movl	$imgY_org_top, %r14d
	movl	$imgUV_org_bot, %r11d
	movl	$imgUV_org_top, %r15d
	cmovneq	%r11, %r15
	testb	%bl, %bl
	cmovneq	%r10, %r14
	movq	(%r14), %rdi
	movq	%rdi, imgY_org(%rip)
	movq	(%r15), %rdi
	movq	%rdi, imgUV_org(%rip)
	shll	$3, %esi
	andl	$-16, %esi
	movl	%esi, 196(%rax)
	leal	2(%rbx,%rbx), %ebx
	jmp	.LBB0_6
.LBB0_4:
	movl	%edx, 196(%rax)
	movq	14224(%rax), %r8
	movslq	%ebx, %r9
	jmp	.LBB0_5
.LBB0_3:
	movq	imgY_org_frm(%rip), %rsi
	movq	%rsi, imgY_org(%rip)
	movq	imgUV_org_frm(%rip), %rsi
	movq	%rsi, imgUV_org(%rip)
	movl	%edx, 196(%rax)
.LBB0_5:
	xorl	%ebx, %ebx
	movl	%edx, %esi
.LBB0_6:
	imulq	$536, %r9, %rdi         # imm = 0x218
	movl	%ebx, 432(%r8,%rdi)
	cmpl	$0, 15536(%rax)
	je	.LBB0_8
# BB#7:
	imull	15544(%rax), %ecx
	sarl	$4, %ecx
	movl	%ecx, 184(%rax)
	movl	15548(%rax), %edi
	imull	%edi, %edx
	sarl	$4, %edx
	movl	%edx, 188(%rax)
	movl	%ecx, 200(%rax)
	imull	%edi, %esi
	sarl	$4, %esi
	movl	%esi, 204(%rax)
.LBB0_8:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	set_MB_parameters, .Lfunc_end0-set_MB_parameters
	.cfi_endproc

	.globl	proceed2nextMacroblock
	.p2align	4, 0x90
	.type	proceed2nextMacroblock,@function
proceed2nextMacroblock:                 # @proceed2nextMacroblock
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	img(%rip), %rax
	movq	14224(%rax), %r14
	movslq	12(%rax), %rcx
	imulq	$536, %rcx, %r15        # imm = 0x218
	movl	28(%r14,%r15), %esi
	movl	15464(%rax), %edx
	cmpl	%edx, %esi
	jle	.LBB1_2
# BB#1:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movq	img(%rip), %rax
.LBB1_2:
	movslq	32(%r14,%r15), %rdx
	movq	stats(%rip), %rcx
	movslq	20(%rax), %rsi
	addq	%rdx, 2000(%rcx,%rsi,8)
	movslq	44(%r14,%r15), %rdx
	addq	%rdx, 2120(%rcx,%rsi,8)
	movslq	40(%r14,%r15), %rdx
	addq	%rdx, 2080(%rcx,%rsi,8)
	movslq	48(%r14,%r15), %rdx
	addq	%rdx, 2160(%rcx,%rsi,8)
	movslq	52(%r14,%r15), %rdx
	addq	%rdx, 2200(%rcx,%rsi,8)
	movl	72(%r14,%r15), %edx
	cmpl	$14, %edx
	ja	.LBB1_7
# BB#3:
	movl	$26112, %esi            # imm = 0x6600
	btl	%edx, %esi
	jae	.LBB1_7
# BB#4:
	leaq	72(%r14,%r15), %rdx
	movslq	416(%r14,%r15), %rsi
	incl	684(%rcx,%rsi,4)
	testb	$15, 364(%r14,%r15)
	je	.LBB1_6
# BB#5:
	movslq	20(%rax), %rsi
	imulq	$60, %rsi, %rsi
	cmpl	$0, 472(%r14,%r15)
	leaq	384(%rcx,%rsi), %rdi
	leaq	84(%rcx,%rsi), %rsi
	cmoveq	%rdi, %rsi
	movslq	(%rdx), %rdi
	incl	(%rsi,%rdi,4)
.LBB1_6:                                # %thread-pre-split
	movl	(%rdx), %edx
.LBB1_7:
	movslq	20(%rax), %rsi
	movslq	%edx, %r8
	imulq	$120, %rsi, %rbx
	addq	%rcx, %rbx
	incq	760(%rbx,%r8,8)
	movslq	36(%r14,%r15), %rdi
	addq	%rdi, 1360(%rbx,%r8,8)
	cmpq	$2, %rsi
	je	.LBB1_23
# BB#8:
	cmpl	$8, %edx
	jne	.LBB1_20
# BB#9:                                 # %.preheader
	movq	stats(%rip), %rcx
	leaq	472(%r14,%r15), %rdx
	movq	input(%rip), %r8
	leaq	384(%rcx), %r9
	leaq	84(%rcx), %r11
	movq	img(%rip), %r10
	xorl	%edi, %edi
	jmp	.LBB1_10
	.p2align	4, 0x90
.LBB1_19:                               # %._crit_edge
                                        #   in Loop: Header=BB1_10 Depth=1
	incq	%rdi
	movl	20(%rax), %esi
.LBB1_10:                               # =>This Inner Loop Header: Depth=1
	movslq	-96(%rdx,%rdi,4), %rbx
	testq	%rbx, %rbx
	movslq	%esi, %rsi
	jle	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_10 Depth=1
	imulq	$120, %rsi, %rsi
	addq	%rcx, %rsi
	incq	760(%rsi,%rbx,8)
	cmpl	$4, %ebx
	je	.LBB1_14
	jmp	.LBB1_18
	.p2align	4, 0x90
.LBB1_12:                               #   in Loop: Header=BB1_10 Depth=1
	movslq	(%rdx), %rbx
	leaq	(%rcx,%rsi,8), %rsi
	incl	44(%rsi,%rbx,4)
	movl	-96(%rdx,%rdi,4), %ebx
	cmpl	$4, %ebx
	jne	.LBB1_18
.LBB1_14:                               #   in Loop: Header=BB1_10 Depth=1
	cmpl	$0, (%rdx)
	je	.LBB1_16
# BB#15:                                #   in Loop: Header=BB1_10 Depth=1
	testb	$15, -108(%rdx)
	movq	%r11, %rsi
	jne	.LBB1_17
.LBB1_16:                               #   in Loop: Header=BB1_10 Depth=1
	cmpl	$2, 5100(%r8)
	movq	%r9, %rsi
	cmoveq	%r11, %rsi
.LBB1_17:                               #   in Loop: Header=BB1_10 Depth=1
	movslq	20(%r10), %rbx
	imulq	$60, %rbx, %rbx
	incl	16(%rsi,%rbx)
.LBB1_18:                               #   in Loop: Header=BB1_10 Depth=1
	cmpq	$3, %rdi
	jne	.LBB1_19
	jmp	.LBB1_23
.LBB1_20:
	cmpl	$3, %edx
	ja	.LBB1_23
# BB#21:
	testb	$15, 364(%r14,%r15)
	je	.LBB1_23
# BB#22:
	imulq	$60, %rsi, %rax
	cmpl	$0, 472(%r14,%r15)
	leaq	384(%rcx,%rax), %rdx
	leaq	84(%rcx,%rax), %rax
	cmoveq	%rdx, %rax
	incl	(%rax,%r8,4)
.LBB1_23:                               # %.loopexit
	movq	img(%rip), %rax
	movl	20(%rax), %eax
	cmpl	$3, %eax
	je	.LBB1_25
# BB#24:                                # %.loopexit
	testl	%eax, %eax
	jne	.LBB1_26
.LBB1_25:
	movq	stats(%rip), %rax
	incl	(%rax)
	movl	8(%r14,%r15), %ecx
	addl	%ecx, 4(%rax)
.LBB1_26:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	proceed2nextMacroblock, .Lfunc_end1-proceed2nextMacroblock
	.cfi_endproc

	.globl	set_chroma_qp
	.p2align	4, 0x90
	.type	set_chroma_qp,@function
set_chroma_qp:                          # @set_chroma_qp
	.cfi_startproc
# BB#0:
	movq	img(%rip), %r9
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	subl	15456(%r9), %edx
	movl	8(%rdi), %eax
	movl	15576(%r9), %ecx
	addl	%eax, %ecx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	cmpl	$52, %ecx
	movl	$51, %edx
	movl	$51, %esi
	cmovll	%ecx, %esi
	movl	%esi, 12(%rdi)
	testl	%esi, %esi
	js	.LBB2_2
# BB#1:
	movslq	%esi, %rcx
	movzbl	QP_SCALE_CR(%rcx), %ecx
.LBB2_2:
	movl	%ecx, 12(%rdi)
	subl	15456(%r9), %r8d
	addl	15580(%r9), %eax
	cmpl	%r8d, %eax
	cmovll	%r8d, %eax
	cmpl	$52, %eax
	cmovll	%eax, %edx
	movl	%edx, 16(%rdi)
	testl	%edx, %edx
	js	.LBB2_4
# BB#3:
	movslq	%edx, %rax
	movzbl	QP_SCALE_CR(%rax), %eax
.LBB2_4:
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end2:
	.size	set_chroma_qp, .Lfunc_end2-set_chroma_qp
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.zero	16,2
.LCPI3_1:
	.zero	16
	.text
	.globl	start_macroblock
	.p2align	4, 0x90
	.type	start_macroblock,@function
start_macroblock:                       # @start_macroblock
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 64
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movq	input(%rip), %rax
	movl	264(%rax), %ebx
	orl	$1, %ebx
	movq	img(%rip), %rax
	movq	14216(%rax), %r13
	movq	14224(%rax), %r15
	movslq	%ebp, %rax
	imulq	$536, %rax, %r14        # imm = 0x218
	leaq	424(%r15,%r14), %r12
	movl	%esi, 424(%r15,%r14)
	movq	enc_picture(%rip), %rcx
	movq	6480(%rcx), %rcx
	movb	%sil, (%rcx,%rax)
	movq	img(%rip), %rcx
	movb	$1, %al
	cmpl	$0, 15312(%rcx)
	jne	.LBB3_4
# BB#1:
	cmpl	$0, 15268(%rcx)
	je	.LBB3_2
# BB#3:
	cmpl	$0, (%r12)
	setne	%al
	jmp	.LBB3_4
.LBB3_2:
	xorl	%eax, %eax
.LBB3_4:
	movzbl	%al, %eax
	movl	%eax, 428(%r15,%r14)
	movl	%ebp, %edi
	callq	set_MB_parameters
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	callq	FmoGetPreviousMBNr
	movl	%eax, %r10d
	movq	input(%rip), %r8
	cmpl	$3, %ebx
	jne	.LBB3_13
# BB#5:
	testb	$1, %bpl
	je	.LBB3_7
# BB#6:
	movl	4708(%r8), %ecx
	testl	%ecx, %ecx
	jne	.LBB3_13
.LBB3_7:
	movq	img(%rip), %rcx
	cmpl	$0, 144(%rcx)
	jne	.LBB3_13
# BB#8:                                 # %.preheader190
	movl	16(%r13), %esi
	testl	%esi, %esi
	jle	.LBB3_13
# BB#9:                                 # %.lr.ph198
	movq	stats(%rip), %rcx
	xorl	%edx, %edx
	movl	$56, %edi
	.p2align	4, 0x90
.LBB3_10:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r13), %rbp
	movq	-56(%rbp,%rdi), %rbx
	movl	4(%rbx), %eax
	movl	%eax, 16(%rbx)
	movl	(%rbx), %eax
	movl	%eax, 12(%rbx)
	movzbl	8(%rbx), %eax
	movb	%al, 20(%rbx)
	movl	32(%rcx), %eax
	movl	%eax, 36(%rcx)
	cmpl	$1, 4008(%r8)
	jne	.LBB3_12
# BB#11:                                #   in Loop: Header=BB3_10 Depth=1
	movups	-48(%rbp,%rdi), %xmm0
	movups	-32(%rbp,%rdi), %xmm1
	movups	-16(%rbp,%rdi), %xmm2
	movups	%xmm2, 32(%rbp,%rdi)
	movups	%xmm1, 16(%rbp,%rdi)
	movups	%xmm0, (%rbp,%rdi)
	movl	16(%r13), %esi
.LBB3_12:                               #   in Loop: Header=BB3_10 Depth=1
	incq	%rdx
	movslq	%esi, %rax
	addq	$104, %rdi
	cmpq	%rax, %rdx
	jl	.LBB3_10
.LBB3_13:                               # %.loopexit
	movq	img(%rip), %rcx
	movl	16(%rcx), %edx
	movl	%edx, (%r15,%r14)
	movl	40(%rcx), %eax
	movl	%eax, 20(%r15,%r14)
	cmpl	$0, 5116(%r8)
	je	.LBB3_76
# BB#14:
	testl	%r10d, %r10d
	js	.LBB3_21
# BB#15:
	cmpl	$2, 4708(%r8)
	jne	.LBB3_19
# BB#16:
	cmpl	$0, 15412(%rcx)
	jne	.LBB3_19
# BB#17:
	cmpl	$0, (%r12)
	je	.LBB3_19
# BB#18:
	movq	14224(%rcx), %rax
	movslq	%r10d, %rsi
	imulq	$536, %rsi, %rsi        # imm = 0x218
	movl	8(%rax,%rsi), %eax
	movl	%eax, 36(%rcx)
	movl	%eax, 8(%r15,%r14)
.LBB3_19:
	movq	14224(%rcx), %rdi
	movslq	%r10d, %rax
	imulq	$536, %rax, %rbp        # imm = 0x218
	movl	8(%rdi,%rbp), %eax
	movl	%eax, 496(%r15,%r14)
	xorl	%esi, %esi
	cmpl	%edx, (%rdi,%rbp)
	jne	.LBB3_22
# BB#20:
	movl	4(%rdi,%rbp), %esi
	jmp	.LBB3_22
.LBB3_76:
	movq	14216(%rcx), %rax
	testl	%r10d, %r10d
	js	.LBB3_79
# BB#77:
	movq	14224(%rcx), %rbp
	movslq	%r10d, %rsi
	imulq	$536, %rsi, %rbx        # imm = 0x218
	movl	8(%rbp,%rbx), %esi
	movl	%esi, 496(%r15,%r14)
	xorl	%edi, %edi
	cmpl	%edx, (%rbp,%rbx)
	jne	.LBB3_80
# BB#78:
	movl	4(%rbp,%rbx), %edi
	jmp	.LBB3_80
.LBB3_21:
	movl	4(%r13), %eax
	movl	%eax, 496(%r15,%r14)
	xorl	%esi, %esi
.LBB3_22:
	movl	%esi, 500(%r15,%r14)
	movl	5128(%r8), %eax
	cmpl	15352(%rcx), %eax
	je	.LBB3_23
# BB#24:
	movl	20(%rcx), %edx
	leal	-1(%rdx), %eax
	cmpl	$1, %eax
	ja	.LBB3_26
# BB#25:
	cmpl	$1, 5136(%r8)
	jne	.LBB3_23
.LBB3_26:
	movl	(%rcx), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	jne	.LBB3_27
.LBB3_23:
	movl	$0, 4(%r15,%r14)
	movl	36(%rcx), %eax
	movl	%eax, 8(%r15,%r14)
	jmp	.LBB3_81
.LBB3_79:
	movl	4(%rax), %esi
	movl	%esi, 496(%r15,%r14)
	xorl	%edi, %edi
.LBB3_80:
	movl	%edi, 500(%r15,%r14)
	movl	4(%rax), %eax
	movl	%eax, 8(%r15,%r14)
	subl	%esi, %eax
	movl	%eax, 4(%r15,%r14)
	movslq	(%r12), %rdx
	movslq	15412(%rcx), %rsi
	shlq	$2, %rsi
	movl	%eax, delta_qp_mbaff(%rsi,%rdx,8)
	movl	8(%r15,%r14), %eax
	movslq	(%r12), %rdx
	movslq	15412(%rcx), %rsi
	shlq	$2, %rsi
	movl	%eax, qp_mbaff(%rsi,%rdx,8)
.LBB3_81:
	movl	15452(%rcx), %eax
	addl	36(%rcx), %eax
	movl	%eax, 44(%rcx)
	xorl	%esi, %esi
	xorl	%edx, %edx
	subl	15456(%rcx), %edx
	movl	8(%r15,%r14), %eax
	movl	15576(%rcx), %edi
	addl	%eax, %edi
	cmpl	%edx, %edi
	cmovll	%edx, %edi
	cmpl	$52, %edi
	movl	$51, %edx
	movl	$51, %ebx
	cmovll	%edi, %ebx
	leaq	12(%r15,%r14), %rbp
	movl	%ebx, 12(%r15,%r14)
	testl	%ebx, %ebx
	js	.LBB3_83
# BB#82:
	movslq	%ebx, %rdi
	movzbl	QP_SCALE_CR(%rdi), %edi
.LBB3_83:
	movl	%edi, (%rbp)
	subl	15456(%rcx), %esi
	addl	15580(%rcx), %eax
	cmpl	%esi, %eax
	cmovll	%esi, %eax
	cmpl	$52, %eax
	cmovll	%eax, %edx
	leaq	16(%r15,%r14), %rsi
	movl	%edx, 16(%r15,%r14)
	testl	%edx, %edx
	js	.LBB3_85
# BB#84:
	movslq	%edx, %rax
	movzbl	QP_SCALE_CR(%rax), %eax
.LBB3_85:                               # %set_chroma_qp.exit189
	movl	%eax, (%rsi)
	movq	active_pps(%rip), %rax
	cmpl	$0, 220(%rax)
	je	.LBB3_86
# BB#87:
	movl	%r10d, %r13d
	movl	14440(%rcx), %eax
	movl	14444(%rcx), %edx
	movl	14448(%rcx), %ecx
	jmp	.LBB3_88
.LBB3_86:
	movl	%r10d, %r13d
	xorl	%eax, %eax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
.LBB3_88:
	movl	%eax, 516(%r15,%r14)
	movl	%edx, 520(%r15,%r14)
	movl	%ecx, 524(%r15,%r14)
	callq	CheckAvailabilityOfNeighbors
	movq	input(%rip), %rax
	cmpl	$1, 4008(%rax)
	jne	.LBB3_90
# BB#89:
	callq	CheckAvailabilityOfNeighborsCABAC
.LBB3_90:                               # %.lr.ph194
	leaq	(%r15,%r14), %r12
	movq	img(%rip), %rbp
	movslq	172(%rbp), %rbx
	decq	%rbx
	.p2align	4, 0x90
.LBB3_91:                               # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax,%rbx,8), %rax
	movslq	168(%rbp), %rcx
	movl	$-1, (%rax,%rcx)
	movq	enc_picture(%rip), %rax
	movq	6512(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	img(%rip), %rcx
	movslq	168(%rcx), %rcx
	movq	(%rax,%rcx,8), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	img(%rip), %rax
	movslq	168(%rax), %rax
	movq	enc_picture(%rip), %rcx
	movq	6496(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	%rax, %rdi
	shlq	$3, %rdi
	addq	8(%rcx,%rbx,8), %rdi
	leal	3(%rax), %ecx
	cmpl	%ecx, %eax
	cmovgel	%eax, %ecx
	subl	%eax, %ecx
	leaq	8(,%rcx,8), %rdx
	movl	$255, %esi
	callq	memset
	movq	img(%rip), %rbp
	movslq	172(%rbp), %rax
	addq	$3, %rax
	incq	%rbx
	cmpq	%rax, %rbx
	jl	.LBB3_91
# BB#92:                                # %.lr.ph194.1
	movslq	172(%rbp), %rbx
	decq	%rbx
	.p2align	4, 0x90
.LBB3_93:                               # %._crit_edge.1
                                        # =>This Inner Loop Header: Depth=1
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	8(%rax,%rbx,8), %rax
	movslq	168(%rbp), %rcx
	movl	$-1, (%rax,%rcx)
	movq	enc_picture(%rip), %rax
	movq	6512(%rax), %rax
	movq	8(%rax), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	img(%rip), %rcx
	movslq	168(%rcx), %rcx
	movq	(%rax,%rcx,8), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	img(%rip), %rax
	movslq	168(%rax), %rax
	movq	enc_picture(%rip), %rcx
	movq	6496(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	%rax, %rdi
	shlq	$3, %rdi
	addq	8(%rcx,%rbx,8), %rdi
	leal	3(%rax), %ecx
	cmpl	%ecx, %eax
	cmovgel	%eax, %ecx
	subl	%eax, %ecx
	leaq	8(,%rcx,8), %rdx
	movl	$255, %esi
	callq	memset
	movq	img(%rip), %rbp
	movslq	172(%rbp), %rax
	addq	$3, %rax
	incq	%rbx
	cmpq	%rax, %rbx
	jl	.LBB3_93
# BB#94:                                # %._crit_edge195.1
	leaq	72(%r15,%r14), %rdi
	movq	$0, 368(%r15,%r14)
	movl	$0, 364(%r15,%r14)
	movq	$0, 408(%r15,%r14)
	movl	$0, 416(%r15,%r14)
	xorl	%esi, %esi
	movl	$260, %edx              # imm = 0x104
	callq	memset
	movaps	.LCPI3_0(%rip), %xmm0   # xmm0 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
	movups	%xmm0, 348(%r15,%r14)
	movups	%xmm0, 332(%r15,%r14)
	movq	input(%rip), %rax
	cmpl	$0, 272(%rax)
	je	.LBB3_96
# BB#95:
	movq	14240(%rbp), %rcx
	movslq	12(%rbp), %rdx
	movl	$1, (%rcx,%rdx,4)
.LBB3_96:
	testl	%r13d, %r13d
	js	.LBB3_98
# BB#97:
	movl	(%r12), %ecx
	movq	14224(%rbp), %rdx
	movslq	%r13d, %rsi
	imulq	$536, %rsi, %rsi        # imm = 0x218
	cmpl	(%rdx,%rsi), %ecx
	jne	.LBB3_99
.LBB3_98:                               # %.sink.split
	movl	$0, 24(%r15,%r14)
.LBB3_99:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r15,%r14)
	movq	$0, 48(%r15,%r14)
	cmpl	$0, 5244(%rax)
	je	.LBB3_101
# BB#100:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_101:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	ResetFastFullIntegerSearch # TAILCALL
.LBB3_27:
	testl	%edx, %edx
	je	.LBB3_29
# BB#28:
	cmpl	$1, 5136(%r8)
	jne	.LBB3_81
.LBB3_29:
	cmpl	$0, 15408(%rcx)
	je	.LBB3_30
# BB#39:
	cmpl	$0, 15412(%rcx)
	je	.LBB3_40
# BB#48:
	movl	$0, 4(%r15,%r14)
	movl	36(%rcx), %eax
	movl	%eax, 8(%r15,%r14)
	xorl	%esi, %esi
	xorl	%edx, %edx
	subl	15456(%rcx), %edx
	movl	15576(%rcx), %edi
	addl	%eax, %edi
	cmpl	%edx, %edi
	cmovll	%edx, %edi
	cmpl	$52, %edi
	movl	$51, %edx
	movl	$51, %ebx
	cmovll	%edi, %ebx
	leaq	12(%r15,%r14), %rbp
	movl	%ebx, 12(%r15,%r14)
	testl	%ebx, %ebx
	js	.LBB3_50
# BB#49:
	movslq	%ebx, %rdi
	movzbl	QP_SCALE_CR(%rdi), %edi
.LBB3_50:
	movl	%edi, (%rbp)
	subl	15456(%rcx), %esi
	addl	15580(%rcx), %eax
	cmpl	%esi, %eax
	cmovll	%esi, %eax
	cmpl	$52, %eax
	cmovll	%eax, %edx
	leaq	16(%r15,%r14), %rsi
	movl	%edx, 16(%r15,%r14)
	testl	%edx, %edx
	js	.LBB3_52
# BB#51:
	movslq	%edx, %rax
	movzbl	QP_SCALE_CR(%rax), %eax
.LBB3_52:                               # %set_chroma_qp.exit
	movl	%eax, (%rsi)
	cmpl	$0, 15408(%rcx)
	jne	.LBB3_81
	jmp	.LBB3_54
.LBB3_30:
	testl	%r10d, %r10d
	js	.LBB3_31
# BB#32:
	cmpl	$0, 4708(%r8)
	je	.LBB3_34
# BB#33:
	cmpl	$0, 15412(%rcx)
	je	.LBB3_34
# BB#38:
	movl	$0, 4(%r15,%r14)
	movl	36(%rcx), %eax
	movl	%eax, 8(%r15,%r14)
	cmpl	$0, 15408(%rcx)
	jne	.LBB3_81
	jmp	.LBB3_54
.LBB3_40:
	cmpl	$0, 15416(%rcx)
	je	.LBB3_43
# BB#41:
	movl	delta_qp_mbaff(%rip), %eax
	movl	%eax, 4(%r15,%r14)
	movl	qp_mbaff(%rip), %eax
	jmp	.LBB3_42
.LBB3_34:
	movq	14224(%rcx), %rsi
	movslq	%r10d, %rax
	imulq	$536, %rax, %rdi        # imm = 0x218
	cmpl	$1, 504(%rsi,%rdi)
	jne	.LBB3_36
# BB#35:
	movl	36(%rcx), %ecx
	leaq	8(%r15,%r14), %rax
	movl	%ecx, 8(%r15,%r14)
	xorl	%edx, %edx
	jmp	.LBB3_37
.LBB3_31:
	movl	$0, 4(%r15,%r14)
	movl	36(%rcx), %eax
	movl	%eax, 8(%r15,%r14)
	movslq	(%r12), %rax
	movslq	15412(%rcx), %rdx
	shlq	$2, %rdx
	movl	$0, delta_qp_mbaff(%rdx,%rax,8)
.LBB3_45:
	movl	8(%r15,%r14), %eax
	jmp	.LBB3_46
.LBB3_43:
	testl	%r10d, %r10d
	js	.LBB3_44
# BB#47:
	movl	delta_qp_mbaff+8(%rip), %eax
	movl	%eax, 4(%r15,%r14)
	movl	qp_mbaff+8(%rip), %eax
.LBB3_42:
	movl	%eax, 8(%r15,%r14)
	movl	%eax, 36(%rcx)
	cmpl	$0, 15408(%rcx)
	jne	.LBB3_81
	jmp	.LBB3_54
.LBB3_36:
	movl	496(%rsi,%rdi), %ebp
	leaq	8(%r15,%r14), %rax
	movl	%ebp, 8(%r15,%r14)
	movl	%ebp, %edx
	subl	8(%rsi,%rdi), %edx
	movl	%ebp, 36(%rcx)
.LBB3_37:
	movl	%edx, 4(%r15,%r14)
	movslq	(%r12), %rsi
	movq	img(%rip), %rcx
	movslq	15412(%rcx), %rdi
	shlq	$2, %rdi
	movl	%edx, delta_qp_mbaff(%rdi,%rsi,8)
	movl	(%rax), %eax
.LBB3_46:
	movslq	(%r12), %rdx
	movslq	15412(%rcx), %rsi
	shlq	$2, %rsi
	movl	%eax, qp_mbaff(%rsi,%rdx,8)
	cmpl	$0, 15408(%rcx)
	jne	.LBB3_81
.LBB3_54:
	movq	input(%rip), %rdi
	movl	4708(%rdi), %esi
	testl	%esi, %esi
	je	.LBB3_56
# BB#55:
	cmpl	$0, 15412(%rcx)
	je	.LBB3_56
# BB#75:
	movl	36(%rcx), %eax
	movl	%eax, 496(%r15,%r14)
	jmp	.LBB3_81
.LBB3_56:
	movl	15388(%rcx), %eax
	testl	%eax, %eax
	jle	.LBB3_64
# BB#57:
	xorl	%edx, %edx
	divl	15404(%rcx)
	testl	%edx, %edx
	jne	.LBB3_64
# BB#58:
	movq	active_sps(%rip), %rax
	cmpl	$0, 1148(%rax)
	je	.LBB3_59
.LBB3_63:                               # %.thread212
	movq	quadratic_RC(%rip), %rdi
	movl	%r10d, %ebx
	callq	updateRCModel
	movq	quadratic_RC(%rip), %rdi
	movq	generic_RC(%rip), %rax
	movl	(%rax), %esi
	callq	*updateQP(%rip)
	movl	%ebx, %r10d
	movq	img(%rip), %rcx
	movl	%eax, 15392(%rcx)
.LBB3_64:                               # %.thread
	cmpl	$0, 12(%rcx)
	je	.LBB3_66
# BB#65:                                # %._crit_edge207
	movl	15392(%rcx), %eax
	jmp	.LBB3_67
.LBB3_66:
	movl	36(%rcx), %eax
	movl	%eax, 15392(%rcx)
.LBB3_67:
	leaq	8(%r15,%r14), %r8
	movl	8(%r15,%r14), %ebx
	movl	15468(%rcx), %r9d
	movl	15472(%rcx), %esi
	movl	%ebx, %edx
	subl	%esi, %edx
	leal	(%r9,%rbx), %edi
	cmpl	%edx, %eax
	cmovgel	%eax, %edx
	cmpl	%edi, %edx
	cmovgl	%edi, %edx
	movl	%edx, 508(%r15,%r14)
	movl	4(%r15,%r14), %ebp
	movl	%ebp, %eax
	subl	%ebx, %eax
	addl	%edx, %eax
	movl	%eax, dq(%rip)
	negl	%esi
	cmpl	%esi, %eax
	jge	.LBB3_69
# BB#68:
	movl	%esi, dq(%rip)
	movl	%esi, %edi
	subl	%ebp, %edi
	movl	%edi, predict_error(%rip)
	movl	36(%rcx), %edx
	addl	%edi, %edx
	movl	%edx, 36(%rcx)
	movl	%esi, -4(%r8)
	jmp	.LBB3_72
.LBB3_69:
	cmpl	%r9d, %eax
	jle	.LBB3_71
# BB#70:
	movl	%r9d, dq(%rip)
	movl	%r9d, %edi
	subl	%ebp, %edi
	movl	%edi, predict_error(%rip)
	movl	36(%rcx), %edx
	addl	%edi, %edx
	movl	%edx, 36(%rcx)
	movl	%r9d, -4(%r8)
	movl	%r9d, %esi
	jmp	.LBB3_72
.LBB3_71:
	movl	%eax, -4(%r8)
	movl	%edx, %edi
	subl	%ebx, %edi
	movl	%edi, predict_error(%rip)
	movl	%edx, 36(%rcx)
	movl	%eax, %esi
.LBB3_72:
	movl	%edx, (%r8)
	movq	input(%rip), %rax
	cmpl	$0, 4708(%rax)
	je	.LBB3_74
# BB#73:
	movslq	(%r12), %rax
	movslq	15412(%rcx), %rdx
	shlq	$2, %rdx
	movl	%esi, delta_qp_mbaff(%rdx,%rax,8)
	movl	(%r8), %eax
	movslq	(%r12), %rdx
	movslq	15412(%rcx), %rsi
	shlq	$2, %rsi
	movl	%eax, qp_mbaff(%rsi,%rdx,8)
.LBB3_74:
	movl	%edi, 512(%r15,%r14)
	jmp	.LBB3_81
.LBB3_44:
	movl	$0, 4(%r15,%r14)
	movl	36(%rcx), %eax
	movl	%eax, 8(%r15,%r14)
	movslq	(%r12), %rax
	movl	$0, delta_qp_mbaff(,%rax,8)
	jmp	.LBB3_45
.LBB3_59:
	cmpl	$0, 4704(%rdi)
	je	.LBB3_62
# BB#60:
	testl	%esi, %esi
	jne	.LBB3_63
# BB#61:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 12(%rax)
	jne	.LBB3_64
	jmp	.LBB3_63
.LBB3_62:
	testl	%esi, %esi
	jne	.LBB3_63
	jmp	.LBB3_64
.Lfunc_end3:
	.size	start_macroblock, .Lfunc_end3-start_macroblock
	.cfi_endproc

	.globl	terminate_macroblock
	.p2align	4, 0x90
	.type	terminate_macroblock,@function
terminate_macroblock:                   # @terminate_macroblock
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 144
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, (%rsp)            # 8-byte Spill
	movq	img(%rip), %rax
	movq	14216(%rax), %r13
	movq	14224(%rax), %r15
	movslq	12(%rax), %r14
	movq	input(%rip), %rax
	movl	264(%rax), %ecx
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	testq	%r14, %r14
	movslq	4016(%rax), %rax
	movq	assignSE2partition(,%rax,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, %ebp
	je	.LBB4_3
# BB#1:
	movl	%r14d, %edi
	callq	FmoGetPreviousMBNr
	testl	%eax, %eax
	js	.LBB4_3
# BB#2:
	movq	img(%rip), %rax
	movq	14224(%rax), %rbx
	movl	12(%rax), %edi
	callq	FmoGetPreviousMBNr
	cltq
	imulq	$536, %rax, %rax        # imm = 0x218
	movl	(%rbx,%rax), %eax
	movq	img(%rip), %rcx
	xorl	%ebp, %ebp
	cmpl	16(%rcx), %eax
	setne	%bpl
.LBB4_3:
	movl	$0, (%r12)
	movq	input(%rip), %rax
	movl	264(%rax), %ecx
	cmpq	$3, %rcx
	ja	.LBB4_55
# BB#4:
	jmpq	*.LJTI4_0(,%rcx,8)
.LBB4_5:
	movq	%r15, 16(%rsp)          # 8-byte Spill
	incl	20(%r13)
	movl	$0, (%r12)
	movl	20(%r13), %ecx
	movq	img(%rip), %rax
	cmpl	15348(%rax), %ecx
	jne	.LBB4_6
# BB#7:
	movq	(%rsp), %r15            # 8-byte Reload
	movl	$1, (%r15)
	movl	$1, %ebp
	movq	img(%rip), %rax
	jmp	.LBB4_8
.LBB4_55:
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	snprintf
	movl	$errortext, %edi
	movl	$600, %esi              # imm = 0x258
	callq	error
	jmp	.LBB4_56
.LBB4_9:
	incl	20(%r13)
	movl	$0, (%r12)
	movq	img(%rip), %rax
	movl	12(%rax), %ebx
	movl	%ebx, %edi
	callq	FmoMB2SliceGroup
	movl	%eax, %edi
	callq	FmoGetLastCodedMBOfSliceGroup
	xorl	%ecx, %ecx
	cmpl	%eax, %ebx
	sete	%cl
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%ecx, (%rsi)
	movl	20(%r13), %eax
	movq	input(%rip), %rdx
	cmpl	268(%rdx), %eax
	setge	%al
	orb	%al, %cl
	movzbl	%cl, %eax
	movl	%eax, (%rsi)
	jmp	.LBB4_56
.LBB4_10:
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movq	img(%rip), %rax
	movl	144(%rax), %eax
	testl	%eax, %eax
	je	.LBB4_11
# BB#12:
	movl	%eax, 44(%rsp)
	movl	$0, 48(%rsp)
	movl	$2, 40(%rsp)
	movq	24(%r13), %r14
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	8(%rax), %rax
	imulq	$104, %rax, %rbx
	leaq	(%r14,%rbx), %rsi
	leaq	40(%rsp), %rdi
	callq	writeSE_UVLC
	movl	52(%rsp), %r10d
	movq	(%r14,%rbx), %rax
	movl	4(%rax), %ecx
	movl	%ecx, 28(%rax)
	movl	(%rax), %ecx
	movl	%ecx, 24(%rax)
	movb	8(%rax), %cl
	movb	%cl, 21(%rax)
	movl	16(%rax), %ecx
	movl	%ecx, 4(%rax)
	movl	12(%rax), %ecx
	movl	%ecx, (%rax)
	movb	20(%rax), %cl
	movb	%cl, 8(%rax)
	movb	$1, terminate_macroblock.skip(%rip)
	testl	%ebp, %ebp
	jne	.LBB4_29
	jmp	.LBB4_14
.LBB4_48:
	testl	%ebp, %ebp
	jne	.LBB4_52
# BB#49:
	movq	img(%rip), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jle	.LBB4_52
# BB#50:
	xorl	%edi, %edi
	callq	*112(%r13)
	testl	%eax, %eax
	je	.LBB4_52
# BB#51:
	movl	$1, (%r12)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$1, (%rax)
.LBB4_52:
	movl	(%r12), %eax
	testl	%eax, %eax
	jne	.LBB4_57
# BB#53:
	movq	img(%rip), %rax
	movl	12(%rax), %ebx
	movl	%ebx, %edi
	callq	FmoMB2SliceGroup
	movl	%eax, %edi
	callq	FmoGetLastCodedMBOfSliceGroup
	cmpl	%eax, %ebx
	jne	.LBB4_56
# BB#54:
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$1, (%rax)
	jmp	.LBB4_56
.LBB4_6:                                # %._crit_edge
	movq	(%rsp), %r15            # 8-byte Reload
	movl	(%r15), %ebp
.LBB4_8:
	movl	12(%rax), %ebx
	movl	%ebx, %edi
	callq	FmoMB2SliceGroup
	movl	%eax, %edi
	callq	FmoGetLastCodedMBOfSliceGroup
	xorl	%ecx, %ecx
	cmpl	%eax, %ebx
	sete	%cl
	orl	%ebp, %ecx
	movl	%ecx, (%r15)
	movq	16(%rsp), %r15          # 8-byte Reload
	jmp	.LBB4_56
.LBB4_11:
	xorl	%r10d, %r10d
	testl	%ebp, %ebp
	jne	.LBB4_29
.LBB4_14:
	movq	img(%rip), %r8
	movq	14216(%r8), %rax
	movq	input(%rip), %rsi
	movl	4008(%rsi), %ecx
	cmpl	$1, %ecx
	je	.LBB4_20
# BB#15:
	testl	%ecx, %ecx
	jne	.LBB4_27
# BB#16:                                # %.preheader34.i
	movslq	16(%rax), %rcx
	testq	%rcx, %rcx
	jle	.LBB4_27
# BB#17:                                # %.lr.ph41.i
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	24(%rax), %rdx
	movl	268(%rsi), %r9d
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_19:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rbx
	movl	4(%rbx), %eax
	xorl	%esi, %esi
	cmpl	$8, %eax
	setl	%sil
	addl	(%rbx), %esi
	xorl	%ebx, %ebx
	cmpl	%r10d, %eax
	setl	%bl
	addl	%esi, %ebx
	cmpl	%r9d, %ebx
	jg	.LBB4_24
# BB#18:                                #   in Loop: Header=BB4_19 Depth=1
	incq	%rdi
	addq	$104, %rdx
	cmpq	%rcx, %rdi
	jl	.LBB4_19
	jmp	.LBB4_26
.LBB4_20:                               # %.preheader.i
	cmpl	$0, 16(%rax)
	jle	.LBB4_27
# BB#21:                                # %.lr.ph.i.preheader
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	%r15, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movl	$8, %r15d
	.p2align	4, 0x90
.LBB4_23:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rax), %rdi
	addq	%r15, %rdi
	movq	%r12, %rbx
	movl	%r10d, %r12d
	movq	%rax, %rbp
	callq	arienco_bits_written
	movl	%r12d, %r10d
	movq	%rbx, %r12
	movq	input(%rip), %rcx
	movl	268(%rcx), %ecx
	shll	$3, %ecx
	cmpl	%ecx, %eax
	jg	.LBB4_24
# BB#22:                                #   in Loop: Header=BB4_23 Depth=1
	incq	%r14
	movslq	16(%rbp), %rax
	addq	$104, %r15
	cmpq	%rax, %r14
	movq	%rbp, %rax
	jl	.LBB4_23
# BB#25:                                # %.loopexit121.loopexit
	movq	img(%rip), %r8
.LBB4_26:                               # %.loopexit121
	movq	16(%rsp), %r15          # 8-byte Reload
	movl	12(%rsp), %ebp          # 4-byte Reload
.LBB4_27:                               # %.loopexit121
	cmpl	$0, 144(%r8)
	jne	.LBB4_29
# BB#28:
	movb	$0, terminate_macroblock.skip(%rip)
	cmpl	$0, (%r12)
	jne	.LBB4_33
	jmp	.LBB4_30
.LBB4_24:                               # %slice_too_big.exit
	movl	$1, (%r12)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$1, (%rax)
	movq	16(%rsp), %r15          # 8-byte Reload
	movl	12(%rsp), %ebp          # 4-byte Reload
.LBB4_29:
	cmpl	$0, (%r12)
	jne	.LBB4_33
.LBB4_30:
	movq	img(%rip), %rax
	movl	12(%rax), %ebx
	movl	%ebx, %edi
	movl	%r10d, %r14d
	callq	FmoMB2SliceGroup
	movl	%eax, %edi
	callq	FmoGetLastCodedMBOfSliceGroup
	movl	%r14d, %r10d
	cmpl	%eax, %ebx
	jne	.LBB4_33
# BB#31:
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$1, (%rax)
	movq	img(%rip), %rax
	cmpl	$0, 144(%rax)
	jne	.LBB4_33
# BB#32:
	movb	$0, terminate_macroblock.skip(%rip)
.LBB4_33:
	testl	%ebp, %ebp
	je	.LBB4_46
# BB#34:
	movq	img(%rip), %rax
	movq	14216(%rax), %r14
	movq	input(%rip), %rdx
	movl	4008(%rdx), %eax
	cmpl	$1, %eax
	je	.LBB4_40
# BB#35:
	testl	%eax, %eax
	jne	.LBB4_46
# BB#36:                                # %.preheader34.i105
	movslq	16(%r14), %rax
	testq	%rax, %rax
	jle	.LBB4_46
# BB#37:                                # %.lr.ph41.i106
	movq	24(%r14), %rcx
	movl	268(%rdx), %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_39:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rdi
	movl	4(%rdi), %ebp
	xorl	%ebx, %ebx
	cmpl	$8, %ebp
	setl	%bl
	addl	(%rdi), %ebx
	xorl	%edi, %edi
	cmpl	%r10d, %ebp
	setl	%dil
	addl	%ebx, %edi
	cmpl	%edx, %edi
	jg	.LBB4_44
# BB#38:                                #   in Loop: Header=BB4_39 Depth=1
	incq	%rsi
	addq	$104, %rcx
	cmpq	%rax, %rsi
	jl	.LBB4_39
	jmp	.LBB4_46
.LBB4_40:                               # %.preheader.i112
	cmpl	$0, 16(%r14)
	jle	.LBB4_46
# BB#41:                                # %.lr.ph.i115.preheader
	xorl	%ebp, %ebp
	movl	$8, %ebx
	.p2align	4, 0x90
.LBB4_43:                               # %.lr.ph.i115
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%r14), %rdi
	addq	%rbx, %rdi
	callq	arienco_bits_written
	movq	input(%rip), %rcx
	movl	268(%rcx), %ecx
	shll	$3, %ecx
	cmpl	%ecx, %eax
	jg	.LBB4_44
# BB#42:                                #   in Loop: Header=BB4_43 Depth=1
	incq	%rbp
	movslq	16(%r14), %rax
	addq	$104, %rbx
	cmpq	%rax, %rbp
	jl	.LBB4_43
	jmp	.LBB4_46
.LBB4_44:                               # %slice_too_big.exit117
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$1, (%rax)
	movq	img(%rip), %rax
	cmpl	$0, 144(%rax)
	jne	.LBB4_46
# BB#45:
	movb	$0, terminate_macroblock.skip(%rip)
.LBB4_46:                               # %slice_too_big.exit117.thread
	movl	(%r12), %eax
	testl	%eax, %eax
	movq	80(%rsp), %r14          # 8-byte Reload
	jne	.LBB4_57
# BB#47:
	incl	20(%r13)
.LBB4_56:                               # %thread-pre-split
	movl	(%r12), %eax
.LBB4_57:
	cmpl	$1, %eax
	jne	.LBB4_63
# BB#58:                                # %.preheader
	movl	16(%r13), %esi
	testl	%esi, %esi
	jle	.LBB4_63
# BB#59:                                # %.lr.ph
	movq	stats(%rip), %rax
	movq	input(%rip), %r8
	xorl	%edx, %edx
	movl	$56, %edi
	.p2align	4, 0x90
.LBB4_60:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r13), %rbp
	movq	-56(%rbp,%rdi), %rbx
	movl	16(%rbx), %ecx
	movl	%ecx, 4(%rbx)
	movl	12(%rbx), %ecx
	movl	%ecx, (%rbx)
	movzbl	20(%rbx), %ecx
	movb	%cl, 8(%rbx)
	movl	36(%rax), %ecx
	movl	%ecx, 32(%rax)
	cmpl	$1, 4008(%r8)
	jne	.LBB4_62
# BB#61:                                #   in Loop: Header=BB4_60 Depth=1
	movups	(%rbp,%rdi), %xmm0
	movups	16(%rbp,%rdi), %xmm1
	movups	32(%rbp,%rdi), %xmm2
	movups	%xmm2, -16(%rbp,%rdi)
	movups	%xmm1, -32(%rbp,%rdi)
	movups	%xmm0, -48(%rbp,%rdi)
	movl	16(%r13), %esi
.LBB4_62:                               #   in Loop: Header=BB4_60 Depth=1
	incq	%rdx
	movslq	%esi, %rcx
	addq	$104, %rdi
	cmpq	%rcx, %rdx
	jl	.LBB4_60
.LBB4_63:                               # %.loopexit
	movq	input(%rip), %rax
	cmpl	$0, 4008(%rax)
	jne	.LBB4_76
# BB#64:
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	8(%rax), %rax
	imulq	$104, %rax, %rbx
	addq	24(%r13), %rbx
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	(%rbp), %eax
	cmpl	$1, %eax
	jne	.LBB4_72
# BB#65:
	testb	$1, terminate_macroblock.skip(%rip)
	je	.LBB4_72
# BB#66:
	movq	img(%rip), %rax
	movl	144(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB4_70
# BB#67:
	cmpl	$1, (%r12)
	jne	.LBB4_70
# BB#68:
	decl	%ecx
	movl	%ecx, 144(%rax)
	je	.LBB4_71
# BB#69:
	movl	%ecx, 44(%rsp)
	movl	$0, 48(%rsp)
	movl	$2, 40(%rsp)
	leaq	40(%rsp), %rdi
	movq	%rbx, %rsi
	callq	writeSE_UVLC
	movl	52(%rsp), %eax
	imulq	$536, %r14, %rcx        # imm = 0x218
	addl	%eax, 32(%r15,%rcx)
	movq	img(%rip), %rax
	movl	$0, 144(%rax)
	jmp	.LBB4_71
.LBB4_70:
	movq	(%rbx), %rcx
	movl	28(%rcx), %edx
	movl	%edx, 4(%rcx)
	movl	24(%rcx), %edx
	movl	%edx, (%rcx)
	movb	21(%rcx), %dl
	movb	%dl, 8(%rcx)
	movl	$0, 144(%rax)
	movb	$0, terminate_macroblock.skip(%rip)
.LBB4_71:                               # %thread-pre-split118
	movl	(%rbp), %eax
.LBB4_72:
	cmpl	$1, %eax
	jne	.LBB4_76
# BB#73:
	movl	28(%rsp), %eax          # 4-byte Reload
	orl	$1, %eax
	cmpl	$3, %eax
	je	.LBB4_76
# BB#74:
	movq	img(%rip), %rax
	movl	144(%rax), %eax
	testl	%eax, %eax
	je	.LBB4_76
# BB#75:
	movl	%eax, 44(%rsp)
	movl	$0, 48(%rsp)
	movl	$2, 40(%rsp)
	leaq	40(%rsp), %rdi
	movq	%rbx, %rsi
	callq	writeSE_UVLC
	movl	52(%rsp), %eax
	imulq	$536, %r14, %rcx        # imm = 0x218
	addl	%eax, 32(%r15,%rcx)
	movq	img(%rip), %rax
	movl	$0, 144(%rax)
.LBB4_76:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	terminate_macroblock, .Lfunc_end4-terminate_macroblock
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_5
	.quad	.LBB4_9
	.quad	.LBB4_10
	.quad	.LBB4_48

	.text
	.globl	OneComponentLumaPrediction4x4
	.p2align	4, 0x90
	.type	OneComponentLumaPrediction4x4,@function
OneComponentLumaPrediction4x4:          # @OneComponentLumaPrediction4x4
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 16
.Lcfi39:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movslq	%r8d, %rax
	movq	(%r9,%rax,8), %rax
	movl	6408(%rax), %edi
	movl	%edi, width_pad(%rip)
	movl	6412(%rax), %edi
	movl	%edi, height_pad(%rip)
	movq	6448(%rax), %rdi
	movswl	2(%rcx), %eax
	addl	%edx, %eax
	movswl	(%rcx), %edx
	addl	%esi, %edx
	movl	%eax, %esi
	callq	UMVLine4X
	movq	(%rax), %rcx
	movq	%rcx, (%rbx)
	movslq	img_padded_size_x(%rip), %rcx
	leaq	(%rax,%rcx,2), %rdx
	movq	(%rax,%rcx,2), %rax
	movq	%rax, 8(%rbx)
	leaq	(%rdx,%rcx,2), %rax
	movq	(%rdx,%rcx,2), %rdx
	movq	%rdx, 16(%rbx)
	movq	(%rax,%rcx,2), %rax
	movq	%rax, 24(%rbx)
	popq	%rbx
	retq
.Lfunc_end5:
	.size	OneComponentLumaPrediction4x4, .Lfunc_end5-OneComponentLumaPrediction4x4
	.cfi_endproc

	.globl	LumaPrediction4x4
	.p2align	4, 0x90
	.type	LumaPrediction4x4,@function
LumaPrediction4x4:                      # @LumaPrediction4x4
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 112
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movl	%r8d, 16(%rsp)          # 4-byte Spill
	movl	%edx, %r12d
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	img(%rip), %rax
	movl	192(%rax), %r10d
	addl	%edi, %r10d
	movl	196(%rax), %r11d
	addl	%esi, %r11d
	movl	%edi, %edx
	sarl	$2, %edx
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movl	%esi, %ebp
	sarl	$2, %ebp
	movq	14224(%rax), %r15
	movslq	12(%rax), %r8
	movq	active_pps(%rip), %rbx
	cmpl	$0, 192(%rbx)
	movl	%r9d, 8(%rsp)           # 4-byte Spill
	je	.LBB6_3
# BB#1:
	movl	20(%rax), %r9d
	movb	$1, %sil
	testl	%r9d, %r9d
	je	.LBB6_6
# BB#2:
	cmpl	$3, %r9d
	movl	8(%rsp), %r9d           # 4-byte Reload
	je	.LBB6_7
.LBB6_3:
	cmpl	$0, 196(%rbx)
	je	.LBB6_5
# BB#4:
	cmpl	$1, 20(%rax)
	sete	%sil
	jmp	.LBB6_7
.LBB6_5:
	xorl	%esi, %esi
	jmp	.LBB6_7
.LBB6_6:
	movl	8(%rsp), %r9d           # 4-byte Reload
.LBB6_7:
	movl	%esi, 44(%rsp)          # 4-byte Spill
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	leal	80(,%r10,4), %r10d
	leal	80(,%r11,4), %r11d
	movq	14384(%rax), %rdi
	movslq	%ebp, %rbx
	movq	(%rdi,%rbx,8), %rdi
	movslq	%edx, %rsi
	movq	(%rdi,%rsi,8), %r14
	imulq	$536, %r8, %r13         # imm = 0x218
	testw	%r9w, %r9w
	jne	.LBB6_14
# BB#8:
	movzwl	480(%r15,%r13), %edi
	testw	%di, %di
	je	.LBB6_14
# BB#9:
	cmpl	$1, 16(%rsp)            # 4-byte Folded Reload
	jne	.LBB6_14
# BB#10:
	cmpl	$1, %ecx
	jne	.LBB6_14
# BB#11:
	cmpl	$2, %r12d
	jne	.LBB6_14
# BB#12:
	cmpw	$0, 112(%rsp)
	jne	.LBB6_14
# BB#13:                                # %.thread
	movl	%r12d, 28(%rsp)         # 4-byte Spill
	movzwl	%di, %edi
	leaq	14400(%rax), %rbp
	addq	$14392, %rax            # imm = 0x3838
	cmpl	$1, %edi
	cmovneq	%rbp, %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax,%rsi,8), %r14
	jmp	.LBB6_20
.LBB6_14:
	cmpl	$2, %r12d
	je	.LBB6_19
# BB#15:
	cmpl	$1, %r12d
	je	.LBB6_18
# BB#16:
	testl	%r12d, %r12d
	movq	32(%rsp), %rbp          # 8-byte Reload
	jne	.LBB6_22
# BB#17:
	movq	(%r14), %rax
	movswq	%r9w, %rdx
	movq	(%rax,%rdx,8), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	432(%r15,%r13), %rcx
	movq	listX(,%rcx,8), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movl	6408(%rcx), %edx
	movl	%edx, width_pad(%rip)
	movl	6412(%rcx), %edx
	movl	%edx, height_pad(%rip)
	movq	6448(%rcx), %rdi
	movswl	2(%rax), %esi
	addl	%r11d, %esi
	movswl	(%rax), %edx
	addl	%r10d, %edx
	callq	UMVLine4X
	movl	8(%rsp), %r9d           # 4-byte Reload
	movq	(%rax), %rcx
	movq	%rcx, LumaPrediction4x4.l0_pred(%rip)
	movslq	img_padded_size_x(%rip), %rcx
	leaq	(%rax,%rcx,2), %rdx
	movq	(%rax,%rcx,2), %rax
	movq	%rax, LumaPrediction4x4.l0_pred+8(%rip)
	leaq	(%rdx,%rcx,2), %rax
	movq	(%rdx,%rcx,2), %rdx
	movq	%rdx, LumaPrediction4x4.l0_pred+16(%rip)
	leaq	(%rax,%rcx,2), %rax
	movl	$LumaPrediction4x4.l0_pred+24, %ecx
	jmp	.LBB6_21
.LBB6_18:
	movq	8(%r14), %rax
	movswq	112(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	16(%rsp), %rdx          # 4-byte Folded Reload
	movq	(%rax,%rdx,8), %rax
	movslq	432(%r15,%r13), %rdx
	movq	listX+8(,%rdx,8), %rdx
	movq	(%rdx,%rcx,8), %rcx
	movl	6408(%rcx), %edx
	movl	%edx, width_pad(%rip)
	movl	6412(%rcx), %edx
	movl	%edx, height_pad(%rip)
	movq	6448(%rcx), %rdi
	movswl	2(%rax), %esi
	addl	%r11d, %esi
	movswl	(%rax), %edx
	addl	%r10d, %edx
	callq	UMVLine4X
	movl	8(%rsp), %r9d           # 4-byte Reload
	movq	(%rax), %rcx
	movq	%rcx, LumaPrediction4x4.l1_pred(%rip)
	movslq	img_padded_size_x(%rip), %rcx
	leaq	(%rax,%rcx,2), %rdx
	movq	(%rax,%rcx,2), %rax
	movq	%rax, LumaPrediction4x4.l1_pred+8(%rip)
	leaq	(%rdx,%rcx,2), %rax
	movq	(%rdx,%rcx,2), %rdx
	movq	%rdx, LumaPrediction4x4.l1_pred+16(%rip)
	leaq	(%rax,%rcx,2), %rax
	movl	$LumaPrediction4x4.l1_pred+24, %ecx
	movq	32(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB6_21
.LBB6_19:
	movl	%r12d, 28(%rsp)         # 4-byte Spill
.LBB6_20:
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	(%r14), %rax
	movswq	%r9w, %rdx
	movq	(%rax,%rdx,8), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	432(%r15,%r13), %rcx
	movq	listX(,%rcx,8), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movl	6408(%rcx), %edx
	movl	%edx, width_pad(%rip)
	movl	6412(%rcx), %edx
	movl	%edx, height_pad(%rip)
	movq	6448(%rcx), %rdi
	movswl	2(%rax), %esi
	addl	%r11d, %esi
	movswl	(%rax), %edx
	addl	%r10d, %edx
	movl	%r10d, %ebx
	movl	%r11d, %r12d
	callq	UMVLine4X
	movq	(%rax), %rcx
	movq	%rcx, LumaPrediction4x4.l0_pred(%rip)
	movslq	img_padded_size_x(%rip), %rcx
	leaq	(%rax,%rcx,2), %rdx
	movq	(%rax,%rcx,2), %rax
	movq	%rax, LumaPrediction4x4.l0_pred+8(%rip)
	leaq	(%rdx,%rcx,2), %rax
	movq	(%rdx,%rcx,2), %rdx
	movq	%rdx, LumaPrediction4x4.l0_pred+16(%rip)
	movq	(%rax,%rcx,2), %rax
	movq	%rax, LumaPrediction4x4.l0_pred+24(%rip)
	movq	8(%r14), %rax
	movswq	112(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	16(%rsp), %rdx          # 4-byte Folded Reload
	movq	(%rax,%rdx,8), %rax
	movslq	432(%r15,%r13), %rdx
	movq	listX+8(,%rdx,8), %rdx
	movq	(%rdx,%rcx,8), %rcx
	movl	6408(%rcx), %edx
	movl	%edx, width_pad(%rip)
	movl	6412(%rcx), %edx
	movl	%edx, height_pad(%rip)
	movq	6448(%rcx), %rdi
	movswl	2(%rax), %esi
	addl	%r12d, %esi
	movswl	(%rax), %edx
	addl	%ebx, %edx
	callq	UMVLine4X
	movl	8(%rsp), %r9d           # 4-byte Reload
	movq	(%rax), %rcx
	movq	%rcx, LumaPrediction4x4.l1_pred(%rip)
	movslq	img_padded_size_x(%rip), %rcx
	leaq	(%rax,%rcx,2), %rdx
	movq	(%rax,%rcx,2), %rax
	movq	%rax, LumaPrediction4x4.l1_pred+8(%rip)
	leaq	(%rdx,%rcx,2), %rax
	movq	(%rdx,%rcx,2), %rdx
	movq	%rdx, LumaPrediction4x4.l1_pred+16(%rip)
	leaq	(%rax,%rcx,2), %rax
	movl	$LumaPrediction4x4.l1_pred+24, %ecx
	movl	28(%rsp), %r12d         # 4-byte Reload
.LBB6_21:                               # %.sink.split
	movq	(%rax), %rax
	movq	%rax, (%rcx)
.LBB6_22:
	movq	48(%rsp), %rbx          # 8-byte Reload
	leal	4(%rbx), %r8d
	leal	4(%rbp), %eax
	cmpb	$0, 44(%rsp)            # 1-byte Folded Reload
	je	.LBB6_29
# BB#23:
	cmpl	$2, %r12d
	jne	.LBB6_35
# BB#24:
	movq	wbp_weight(%rip), %rcx
	movswq	%r9w, %rdx
	movq	(%rcx), %rsi
	movq	8(%rcx), %rcx
	movq	(%rsi,%rdx,8), %rsi
	movswq	112(%rsp), %rdi
	movq	(%rsi,%rdi,8), %rsi
	movl	(%rsi), %r15d
	movq	(%rcx,%rdx,8), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movl	(%rcx), %r10d
	movq	wp_offset(%rip), %rcx
	movq	(%rcx), %rsi
	movq	8(%rcx), %rcx
	movq	(%rsi,%rdx,8), %rdx
	movl	(%rdx), %edx
	movq	(%rcx,%rdi,8), %rcx
	movl	(%rcx), %ecx
	leal	1(%rdx,%rcx), %r12d
	sarl	%r12d
	movl	wp_luma_round(%rip), %r13d
	addl	%r13d, %r13d
	movl	luma_log_weight_denom(%rip), %ecx
	incl	%ecx
	movq	img(%rip), %r9
	movslq	%ebx, %rdx
	movslq	%r8d, %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movslq	%ebp, %rbx
	movl	%r10d, %ebp
	movslq	%eax, %r11
	movq	%rbx, %rax
	shlq	$5, %rax
	leaq	(%rax,%rdx,2), %rax
	incq	%rdx
	leaq	12630(%r9,%rax), %rax
	movl	$LumaPrediction4x4.l1_pred, %r14d
	movl	$LumaPrediction4x4.l0_pred, %esi
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_25:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	15520(%r9), %edi
	movzwl	(%rsi), %r8d
	imull	%r15d, %r8d
	movzwl	(%r14), %r10d
	imull	%ebp, %r10d
	addl	%r13d, %r8d
	addl	%r10d, %r8d
	sarl	%cl, %r8d
	addl	%r12d, %r8d
	movl	%ebp, %r10d
	movl	$0, %ebp
	cmovsl	%ebp, %r8d
	cmpl	%edi, %r8d
	cmovlw	%r8w, %di
	cmpq	8(%rsp), %rdx           # 8-byte Folded Reload
	movw	%di, -6(%rax)
	jge	.LBB6_27
# BB#26:                                #   in Loop: Header=BB6_25 Depth=1
	movl	15520(%r9), %edi
	xorl	%r8d, %r8d
	movzwl	2(%rsi), %ebp
	imull	%r15d, %ebp
	movzwl	2(%r14), %edx
	imull	%r10d, %edx
	addl	%r13d, %ebp
	addl	%edx, %ebp
	sarl	%cl, %ebp
	addl	%r12d, %ebp
	cmovsl	%r8d, %ebp
	cmpl	%edi, %ebp
	cmovlw	%bp, %di
	movw	%di, -4(%rax)
	movl	15520(%r9), %edx
	movzwl	4(%rsi), %edi
	imull	%r15d, %edi
	movzwl	4(%r14), %ebp
	imull	%r10d, %ebp
	addl	%r13d, %edi
	addl	%ebp, %edi
	sarl	%cl, %edi
	addl	%r12d, %edi
	cmovsl	%r8d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, -2(%rax)
	movl	15520(%r9), %edx
	movzwl	6(%rsi), %edi
	addq	$8, %rsi
	imull	%r15d, %edi
	movzwl	6(%r14), %ebp
	addq	$8, %r14
	imull	%r10d, %ebp
	addl	%r13d, %edi
	addl	%ebp, %edi
	sarl	%cl, %edi
	addl	%r12d, %edi
	cmovsl	%r8d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, (%rax)
	movq	16(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB6_28
	.p2align	4, 0x90
.LBB6_27:                               #   in Loop: Header=BB6_25 Depth=1
	addq	$2, %rsi
	addq	$2, %r14
.LBB6_28:                               #   in Loop: Header=BB6_25 Depth=1
	movl	%r10d, %ebp
	incq	%rbx
	addq	$32, %rax
	cmpq	%r11, %rbx
	jl	.LBB6_25
	jmp	.LBB6_52
.LBB6_29:
	cmpl	$2, %r12d
	jne	.LBB6_41
# BB#30:                                # %.preheader208
	movq	img(%rip), %rdi
	movslq	%ebx, %rcx
	movslq	%r8d, %r8
	movslq	%ebp, %rdx
	movslq	%eax, %r9
	movq	%rdx, %rsi
	shlq	$5, %rsi
	leaq	(%rsi,%rcx,2), %rsi
	incq	%rcx
	leaq	12630(%rdi,%rsi), %rdi
	movl	$LumaPrediction4x4.l1_pred, %ebp
	movl	$LumaPrediction4x4.l0_pred, %ebx
	.p2align	4, 0x90
.LBB6_31:                               # %.preheader207
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx), %esi
	movzwl	(%rbp), %eax
	leal	1(%rsi,%rax), %eax
	shrl	%eax
	cmpq	%r8, %rcx
	movw	%ax, -6(%rdi)
	jge	.LBB6_33
# BB#32:                                #   in Loop: Header=BB6_31 Depth=1
	movzwl	2(%rbx), %eax
	movzwl	2(%rbp), %esi
	leal	1(%rax,%rsi), %eax
	shrl	%eax
	movw	%ax, -4(%rdi)
	movzwl	4(%rbx), %eax
	movzwl	4(%rbp), %esi
	leal	1(%rax,%rsi), %eax
	shrl	%eax
	movw	%ax, -2(%rdi)
	movzwl	6(%rbx), %eax
	addq	$8, %rbx
	movzwl	6(%rbp), %esi
	addq	$8, %rbp
	leal	1(%rax,%rsi), %eax
	shrl	%eax
	movw	%ax, (%rdi)
	jmp	.LBB6_34
	.p2align	4, 0x90
.LBB6_33:                               #   in Loop: Header=BB6_31 Depth=1
	addq	$2, %rbx
	addq	$2, %rbp
.LBB6_34:                               #   in Loop: Header=BB6_31 Depth=1
	incq	%rdx
	addq	$32, %rdi
	cmpq	%r9, %rdx
	jl	.LBB6_31
	jmp	.LBB6_52
.LBB6_35:
	movq	wp_weight(%rip), %rcx
	testl	%r12d, %r12d
	je	.LBB6_44
# BB#36:
	movq	8(%rcx), %rcx
	movswq	112(%rsp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movl	(%rcx), %r11d
	movq	wp_offset(%rip), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movl	(%rcx), %r15d
	movq	img(%rip), %rsi
	movl	wp_luma_round(%rip), %r12d
	movl	luma_log_weight_denom(%rip), %ecx
	movslq	%ebx, %r10
	movslq	%r8d, %r8
	movslq	%ebp, %rbx
	movslq	%eax, %r9
	movq	%rbx, %rax
	shlq	$5, %rax
	leaq	(%rax,%r10,2), %rax
	incq	%r10
	leaq	12630(%rsi,%rax), %rbp
	movl	$LumaPrediction4x4.l1_pred, %eax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB6_37:                               # %.preheader205
                                        # =>This Inner Loop Header: Depth=1
	movl	15520(%rsi), %edx
	movzwl	(%rax), %edi
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	cmpq	%r8, %r10
	movw	%dx, -6(%rbp)
	jge	.LBB6_39
# BB#38:                                #   in Loop: Header=BB6_37 Depth=1
	movl	15520(%rsi), %edx
	movzwl	2(%rax), %edi
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, -4(%rbp)
	movl	15520(%rsi), %edx
	movzwl	4(%rax), %edi
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, -2(%rbp)
	movl	15520(%rsi), %edx
	movzwl	6(%rax), %edi
	addq	$8, %rax
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, (%rbp)
	jmp	.LBB6_40
	.p2align	4, 0x90
.LBB6_39:                               #   in Loop: Header=BB6_37 Depth=1
	addq	$2, %rax
.LBB6_40:                               #   in Loop: Header=BB6_37 Depth=1
	incq	%rbx
	addq	$32, %rbp
	cmpq	%r9, %rbx
	jl	.LBB6_37
	jmp	.LBB6_52
.LBB6_41:
	movq	img(%rip), %rdx
	movslq	%ebp, %rsi
	movq	%rsi, %rdi
	shlq	$5, %rdi
	leaq	(%rdx,%rdi), %rbp
	testl	%r12d, %r12d
	movslq	%ebx, %rcx
	cltq
	leaq	12624(%rbp,%rcx,2), %rbp
	je	.LBB6_49
# BB#42:                                # %.preheader212.preheader
	movq	LumaPrediction4x4.l1_pred(%rip), %rbx
	movq	%rbx, (%rbp)
	incq	%rsi
	cmpq	%rax, %rsi
	jge	.LBB6_52
# BB#43:                                # %.preheader212.1
	leaq	12624(%rdx,%rdi), %rax
	shlq	$5, %rsi
	leaq	12624(%rdx,%rsi), %rdx
	movq	LumaPrediction4x4.l1_pred+8(%rip), %rsi
	movq	%rsi, (%rdx,%rcx,2)
	movq	LumaPrediction4x4.l1_pred+16(%rip), %rdx
	movq	%rdx, 64(%rax,%rcx,2)
	movq	LumaPrediction4x4.l1_pred+24(%rip), %rdx
	jmp	.LBB6_51
.LBB6_44:
	movq	(%rcx), %rcx
	movswq	%r9w, %rdx
	movq	(%rcx,%rdx,8), %rcx
	movl	(%rcx), %r11d
	movq	wp_offset(%rip), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movl	(%rcx), %r15d
	movq	img(%rip), %rsi
	movl	wp_luma_round(%rip), %r12d
	movl	luma_log_weight_denom(%rip), %ecx
	movslq	%ebx, %r10
	movslq	%r8d, %r8
	movslq	%ebp, %rbx
	movslq	%eax, %r9
	movq	%rbx, %rax
	shlq	$5, %rax
	leaq	(%rax,%r10,2), %rax
	incq	%r10
	leaq	12630(%rsi,%rax), %rbp
	movl	$LumaPrediction4x4.l0_pred, %eax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB6_45:                               # %.preheader203
                                        # =>This Inner Loop Header: Depth=1
	movl	15520(%rsi), %edx
	movzwl	(%rax), %edi
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	cmpq	%r8, %r10
	movw	%dx, -6(%rbp)
	jge	.LBB6_47
# BB#46:                                #   in Loop: Header=BB6_45 Depth=1
	movl	15520(%rsi), %edx
	movzwl	2(%rax), %edi
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, -4(%rbp)
	movl	15520(%rsi), %edx
	movzwl	4(%rax), %edi
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, -2(%rbp)
	movl	15520(%rsi), %edx
	movzwl	6(%rax), %edi
	addq	$8, %rax
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, (%rbp)
	jmp	.LBB6_48
	.p2align	4, 0x90
.LBB6_47:                               #   in Loop: Header=BB6_45 Depth=1
	addq	$2, %rax
.LBB6_48:                               #   in Loop: Header=BB6_45 Depth=1
	incq	%rbx
	addq	$32, %rbp
	cmpq	%r9, %rbx
	jl	.LBB6_45
	jmp	.LBB6_52
.LBB6_49:                               # %.preheader210.preheader
	movq	LumaPrediction4x4.l0_pred(%rip), %rbx
	movq	%rbx, (%rbp)
	incq	%rsi
	cmpq	%rax, %rsi
	jge	.LBB6_52
# BB#50:                                # %.preheader210.1
	leaq	12624(%rdx,%rdi), %rax
	shlq	$5, %rsi
	leaq	12624(%rdx,%rsi), %rdx
	movq	LumaPrediction4x4.l0_pred+8(%rip), %rsi
	movq	%rsi, (%rdx,%rcx,2)
	movq	LumaPrediction4x4.l0_pred+16(%rip), %rdx
	movq	%rdx, 64(%rax,%rcx,2)
	movq	LumaPrediction4x4.l0_pred+24(%rip), %rdx
.LBB6_51:                               # %.loopexit
	movq	%rdx, 96(%rax,%rcx,2)
.LBB6_52:                               # %.loopexit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	LumaPrediction4x4, .Lfunc_end6-LumaPrediction4x4
	.cfi_endproc

	.globl	LumaPrediction4x4Bi
	.p2align	4, 0x90
	.type	LumaPrediction4x4Bi,@function
LumaPrediction4x4Bi:                    # @LumaPrediction4x4Bi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi59:
	.cfi_def_cfa_offset 112
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movl	%r9d, 8(%rsp)           # 4-byte Spill
	movl	%ecx, 12(%rsp)          # 4-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	%edi, %r14d
	movl	112(%rsp), %r13d
	movq	img(%rip), %rax
	movl	192(%rax), %ecx
	addl	%r14d, %ecx
	leal	80(,%rcx,4), %r11d
	movl	196(%rax), %ecx
	addl	%esi, %ecx
	leal	80(,%rcx,4), %r9d
	sarl	$2, %edi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%esi, %ebp
	sarl	$2, %ebp
	movq	14224(%rax), %r12
	movslq	12(%rax), %r10
	movq	active_pps(%rip), %rcx
	cmpl	$0, 192(%rcx)
	je	.LBB7_3
# BB#1:
	movl	20(%rax), %ebx
	movb	$1, %sil
	testl	%ebx, %ebx
	je	.LBB7_6
# BB#2:
	cmpl	$3, %ebx
	je	.LBB7_6
.LBB7_3:
	cmpl	$0, 196(%rcx)
	je	.LBB7_4
# BB#5:
	cmpl	$1, 20(%rax)
	sete	%sil
	jmp	.LBB7_6
.LBB7_4:
	xorl	%esi, %esi
.LBB7_6:
	movl	%esi, 44(%rsp)          # 4-byte Spill
	leal	4(%r14), %ecx
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	4(%rcx), %ecx
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	leaq	14400(%rax), %rcx
	addq	$14392, %rax            # imm = 0x3838
	testl	%r13d, %r13d
	cmoveq	%rcx, %rax
	movq	(%rax), %rax
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rbp
	movq	(%rbp), %rax
	movswq	%r8w, %r13
	movq	(%rax,%r13,8), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rax
	imulq	$536, %r10, %r15        # imm = 0x218
	movslq	432(%r12,%r15), %rcx
	movq	listX(,%rcx,8), %rcx
	movq	(%rcx,%r13,8), %rcx
	movl	6408(%rcx), %edx
	movl	%edx, width_pad(%rip)
	movl	6412(%rcx), %edx
	movl	%edx, height_pad(%rip)
	movq	6448(%rcx), %rdi
	movswl	2(%rax), %esi
	movq	%r12, %rbx
	movl	%r9d, %r12d
	addl	%r12d, %esi
	movswl	(%rax), %edx
	addl	%r11d, %edx
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movl	%r11d, %r14d
	callq	UMVLine4X
	movq	(%rax), %rcx
	movq	%rcx, LumaPrediction4x4Bi.l0_pred(%rip)
	movslq	img_padded_size_x(%rip), %rcx
	leaq	(%rax,%rcx,2), %rdx
	movq	(%rax,%rcx,2), %rax
	movq	%rax, LumaPrediction4x4Bi.l0_pred+8(%rip)
	leaq	(%rdx,%rcx,2), %rax
	movq	(%rdx,%rcx,2), %rdx
	movq	%rdx, LumaPrediction4x4Bi.l0_pred+16(%rip)
	movq	(%rax,%rcx,2), %rax
	movq	%rax, LumaPrediction4x4Bi.l0_pred+24(%rip)
	movq	8(%rbp), %rax
	movswq	8(%rsp), %rbp           # 2-byte Folded Reload
	movq	(%rax,%rbp,8), %rax
	movslq	12(%rsp), %rcx          # 4-byte Folded Reload
	movq	(%rax,%rcx,8), %rax
	movslq	432(%rbx,%r15), %rcx
	movq	listX+8(,%rcx,8), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	6408(%rcx), %edx
	movl	%edx, width_pad(%rip)
	movl	6412(%rcx), %edx
	movl	%edx, height_pad(%rip)
	movq	6448(%rcx), %rdi
	movswl	2(%rax), %esi
	addl	%r12d, %esi
	movswl	(%rax), %edx
	addl	%r14d, %edx
	callq	UMVLine4X
	movq	(%rax), %rdi
	movq	%rdi, LumaPrediction4x4Bi.l1_pred(%rip)
	movslq	img_padded_size_x(%rip), %rcx
	leaq	(%rax,%rcx,2), %rdx
	movq	(%rax,%rcx,2), %rax
	movq	%rax, LumaPrediction4x4Bi.l1_pred+8(%rip)
	leaq	(%rdx,%rcx,2), %rax
	movq	(%rdx,%rcx,2), %rdx
	movq	%rdx, LumaPrediction4x4Bi.l1_pred+16(%rip)
	movq	(%rax,%rcx,2), %rax
	movq	%rax, LumaPrediction4x4Bi.l1_pred+24(%rip)
	cmpb	$0, 44(%rsp)            # 1-byte Folded Reload
	je	.LBB7_7
# BB#12:
	movq	wbp_weight(%rip), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	(%rcx), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	(%rax,%r13,8), %rax
	movq	(%rax,%rbp,8), %rax
	movl	(%rax), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	wp_offset(%rip), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	(%rcx,%r13,8), %rcx
	movl	(%rcx), %ecx
	movq	(%rax,%rbp,8), %rax
	movl	(%rax), %eax
	leal	1(%rcx,%rax), %ebp
	sarl	%ebp
	movq	img(%rip), %r13
	movl	wp_luma_round(%rip), %r9d
	addl	%r9d, %r9d
	movl	luma_log_weight_denom(%rip), %ecx
	incl	%ecx
	movslq	48(%rsp), %r11          # 4-byte Folded Reload
	movslq	32(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movslq	16(%rsp), %r14          # 4-byte Folded Reload
	movslq	28(%rsp), %rbx          # 4-byte Folded Reload
	movq	%r14, %rax
	shlq	$5, %rax
	leaq	(%rax,%r11,2), %rax
	incq	%r11
	leaq	12630(%r13,%rax), %rax
	movl	$LumaPrediction4x4Bi.l1_pred, %edx
	movl	$LumaPrediction4x4Bi.l0_pred, %esi
	incq	%r14
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	12(%rsp), %r15d         # 4-byte Reload
	movl	8(%rsp), %r12d          # 4-byte Reload
	jmp	.LBB7_13
	.p2align	4, 0x90
.LBB7_16:                               # %..preheader_crit_edge
                                        #   in Loop: Header=BB7_13 Depth=1
	movw	(%rdx), %di
	addq	$32, %rax
	incq	%r14
.LBB7_13:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	15520(%r13), %r8d
	movzwl	(%rsi), %r10d
	imull	%r15d, %r10d
	movzwl	%di, %edi
	imull	%r12d, %edi
	addl	%r10d, %edi
	addl	%r9d, %edi
	sarl	%cl, %edi
	addl	%ebp, %edi
	movl	%ebp, %r10d
	movl	$0, %ebp
	cmovsl	%ebp, %edi
	cmpl	%r8d, %edi
	cmovlw	%di, %r8w
	cmpq	32(%rsp), %r11          # 8-byte Folded Reload
	movw	%r8w, -6(%rax)
	jge	.LBB7_14
# BB#18:                                #   in Loop: Header=BB7_13 Depth=1
	movl	15520(%r13), %edi
	movq	%r11, %r8
	movzwl	2(%rsi), %ebp
	imull	%r15d, %ebp
	xorl	%r11d, %r11d
	movzwl	2(%rdx), %ebx
	imull	%r12d, %ebx
	addl	%ebp, %ebx
	addl	%r9d, %ebx
	sarl	%cl, %ebx
	addl	%r10d, %ebx
	cmovsl	%r11d, %ebx
	xorl	%r11d, %r11d
	cmpl	%edi, %ebx
	cmovlw	%bx, %di
	movw	%di, -4(%rax)
	movl	15520(%r13), %edi
	movzwl	4(%rsi), %ebp
	imull	%r15d, %ebp
	movzwl	4(%rdx), %ebx
	imull	%r12d, %ebx
	addl	%ebp, %ebx
	addl	%r9d, %ebx
	sarl	%cl, %ebx
	addl	%r10d, %ebx
	cmovsl	%r11d, %ebx
	cmpl	%edi, %ebx
	cmovlw	%bx, %di
	movw	%di, -2(%rax)
	movl	15520(%r13), %edi
	movzwl	6(%rsi), %ebp
	addq	$8, %rsi
	imull	%r15d, %ebp
	movzwl	6(%rdx), %ebx
	addq	$8, %rdx
	imull	%r12d, %ebx
	addl	%ebp, %ebx
	movq	%r8, %r11
	addl	%r9d, %ebx
	sarl	%cl, %ebx
	addl	%r10d, %ebx
	movl	$0, %ebp
	cmovsl	%ebp, %ebx
	cmpl	%edi, %ebx
	cmovlw	%bx, %di
	movq	16(%rsp), %rbx          # 8-byte Reload
	movw	%di, (%rax)
	jmp	.LBB7_15
	.p2align	4, 0x90
.LBB7_14:                               #   in Loop: Header=BB7_13 Depth=1
	addq	$2, %rsi
	addq	$2, %rdx
.LBB7_15:                               #   in Loop: Header=BB7_13 Depth=1
	movl	%r10d, %ebp
	cmpq	%rbx, %r14
	jl	.LBB7_16
	jmp	.LBB7_17
.LBB7_7:                                # %.preheader102
	movq	img(%rip), %rcx
	movslq	48(%rsp), %rax          # 4-byte Folded Reload
	movslq	32(%rsp), %r8           # 4-byte Folded Reload
	movslq	16(%rsp), %rsi          # 4-byte Folded Reload
	movslq	28(%rsp), %r9           # 4-byte Folded Reload
	movq	%rsi, %rdx
	shlq	$5, %rdx
	leaq	(%rdx,%rax,2), %rdx
	incq	%rax
	leaq	12630(%rcx,%rdx), %rbp
	movl	$LumaPrediction4x4Bi.l1_pred, %ebx
	movl	$LumaPrediction4x4Bi.l0_pred, %ecx
	jmp	.LBB7_8
	.p2align	4, 0x90
.LBB7_11:                               # %..preheader101_crit_edge
                                        #   in Loop: Header=BB7_8 Depth=1
	movw	(%rbx), %di
	addq	$32, %rbp
.LBB7_8:                                # %.preheader101
                                        # =>This Inner Loop Header: Depth=1
	incq	%rsi
	movzwl	(%rcx), %edx
	movzwl	%di, %edi
	leal	1(%rdx,%rdi), %edx
	shrl	%edx
	cmpq	%r8, %rax
	movw	%dx, -6(%rbp)
	jge	.LBB7_9
# BB#19:                                #   in Loop: Header=BB7_8 Depth=1
	movzwl	2(%rcx), %edx
	movzwl	2(%rbx), %edi
	leal	1(%rdx,%rdi), %edx
	shrl	%edx
	movw	%dx, -4(%rbp)
	movzwl	4(%rcx), %edx
	movzwl	4(%rbx), %edi
	leal	1(%rdx,%rdi), %edx
	shrl	%edx
	movw	%dx, -2(%rbp)
	movzwl	6(%rcx), %edx
	addq	$8, %rcx
	movzwl	6(%rbx), %edi
	addq	$8, %rbx
	leal	1(%rdx,%rdi), %edx
	shrl	%edx
	movw	%dx, (%rbp)
	cmpq	%r9, %rsi
	jl	.LBB7_11
	jmp	.LBB7_17
	.p2align	4, 0x90
.LBB7_9:                                #   in Loop: Header=BB7_8 Depth=1
	addq	$2, %rcx
	addq	$2, %rbx
	cmpq	%r9, %rsi
	jl	.LBB7_11
.LBB7_17:                               # %.loopexit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	LumaPrediction4x4Bi, .Lfunc_end7-LumaPrediction4x4Bi
	.cfi_endproc

	.globl	LumaResidualCoding8x8
	.p2align	4, 0x90
	.type	LumaResidualCoding8x8,@function
LumaResidualCoding8x8:                  # @LumaResidualCoding8x8
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi72:
	.cfi_def_cfa_offset 224
.Lcfi73:
	.cfi_offset %rbx, -56
.Lcfi74:
	.cfi_offset %r12, -48
.Lcfi75:
	.cfi_offset %r13, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movl	%r9d, %r10d
	movl	%r8d, %r11d
	movl	%ecx, %r8d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	movl	$0, 20(%rsp)
	leal	(,%rdx,4), %eax
	movl	%eax, 68(%rsp)          # 4-byte Spill
	movl	%eax, %edi
	andl	$-8, %edi
	movl	%edx, %ebx
	andl	$1, %ebx
	movl	$1, %eax
	movl	%edx, %ecx
	shll	%cl, %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movq	img(%rip), %rcx
	movl	%r10d, %eax
	orl	%r11d, %eax
	je	.LBB8_2
# BB#1:
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
	jmp	.LBB8_3
.LBB8_2:
	cmpl	$1, 20(%rcx)
	setne	%al
	movl	%eax, 44(%rsp)          # 4-byte Spill
.LBB8_3:                                # %._crit_edge
	leal	(,%rbx,8), %eax
	movslq	12(%rcx), %rsi
	imulq	$536, %rsi, %r9         # imm = 0x218
	movq	input(%rip), %rsi
	cmpl	$0, 5772(%rsi)
	movq	14224(%rcx), %rsi
	movl	$OneComponentChromaPrediction4x4_retrieve, %r14d
	movl	$OneComponentChromaPrediction4x4_regenerate, %ebp
	cmovneq	%r14, %rbp
	leal	8(%rdi), %r14d
	cmpl	$0, 472(%rsi,%r9)
	movq	%rbp, OneComponentChromaPrediction4x4(%rip)
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	movl	%r14d, 112(%rsp)        # 4-byte Spill
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	movl	%r10d, 52(%rsp)         # 4-byte Spill
	movl	%r11d, 40(%rsp)         # 4-byte Spill
	je	.LBB8_25
# BB#4:                                 # %.preheader181
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movswl	%r8w, %edx
	movl	%eax, %esi
	movslq	%edi, %rbp
	movslq	%r14d, %rbx
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	movl	%eax, %ebx
	orl	$4, %ebx
	movl	%ebx, 116(%rsp)         # 4-byte Spill
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	shlq	$5, %rbp
	leaq	(%rbp,%rsi,2), %rbx
	movl	%edi, %esi
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	cltq
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movswl	232(%rsp), %edi
	movswl	224(%rsp), %r9d
	xorl	%r12d, %r12d
	movl	%edi, 24(%rsp)          # 4-byte Spill
	movl	%r9d, 56(%rsp)          # 4-byte Spill
	movl	%edx, 36(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB8_5:                                # =>This Inner Loop Header: Depth=1
	movq	160(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12), %rsi
	movl	196(%rcx), %eax
	addl	%esi, %eax
	movslq	%eax, %r13
	movslq	192(%rcx), %rbp
	movq	152(%rsp), %r14         # 8-byte Reload
	addq	%r14, %rbp
	movl	%edi, (%rsp)
	movl	%r14d, %edi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r11d, %ecx
	movl	%r10d, %r8d
	callq	LumaPrediction4x4
	movq	imgY_org(%rip), %r8
	movq	img(%rip), %rcx
	movq	(%r8,%r13,8), %rsi
	movzwl	(%rsi,%rbp,2), %edi
	leaq	12624(%rcx,%r15), %rax
	movzwl	(%rbx,%rax), %edx
	subl	%edx, %edi
	movl	%edi, 13136(%rcx,%r15,2)
	movzwl	2(%rsi,%rbp,2), %edx
	movzwl	2(%rbx,%rax), %edi
	subl	%edi, %edx
	movl	%edx, 13140(%rcx,%r15,2)
	movzwl	4(%rsi,%rbp,2), %edx
	movzwl	4(%rbx,%rax), %edi
	subl	%edi, %edx
	movl	%edx, 13144(%rcx,%r15,2)
	movzwl	6(%rsi,%rbp,2), %edx
	movzwl	6(%rbx,%rax), %esi
	subl	%esi, %edx
	movl	%edx, 13148(%rcx,%r15,2)
	movq	8(%r8,%r13,8), %rdx
	movzwl	(%rdx,%rbp,2), %esi
	movzwl	32(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13200(%rcx,%r15,2)
	movzwl	2(%rdx,%rbp,2), %esi
	movzwl	34(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13204(%rcx,%r15,2)
	movzwl	4(%rdx,%rbp,2), %esi
	movzwl	36(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13208(%rcx,%r15,2)
	movzwl	6(%rdx,%rbp,2), %edx
	movzwl	38(%rbx,%rax), %esi
	subl	%esi, %edx
	movl	%edx, 13212(%rcx,%r15,2)
	movq	16(%r8,%r13,8), %rdx
	movzwl	(%rdx,%rbp,2), %esi
	movzwl	64(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13264(%rcx,%r15,2)
	movzwl	2(%rdx,%rbp,2), %esi
	movzwl	66(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13268(%rcx,%r15,2)
	movzwl	4(%rdx,%rbp,2), %esi
	movzwl	68(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13272(%rcx,%r15,2)
	movzwl	6(%rdx,%rbp,2), %edx
	movzwl	70(%rbx,%rax), %esi
	subl	%esi, %edx
	movl	%edx, 13276(%rcx,%r15,2)
	movq	24(%r8,%r13,8), %rdx
	movzwl	(%rdx,%rbp,2), %esi
	movzwl	96(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13328(%rcx,%r15,2)
	movzwl	2(%rdx,%rbp,2), %esi
	movzwl	98(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13332(%rcx,%r15,2)
	movzwl	4(%rdx,%rbp,2), %esi
	movzwl	100(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13336(%rcx,%r15,2)
	movzwl	6(%rdx,%rbp,2), %edx
	movzwl	102(%rbx,%rax), %eax
	subl	%eax, %edx
	movl	116(%rsp), %eax         # 4-byte Reload
	cmpl	%eax, %r14d
	movl	%edx, 13340(%rcx,%r15,2)
	jae	.LBB8_6
# BB#54:                                # %.loopexit231.loopexit237
                                        #   in Loop: Header=BB8_5 Depth=1
	movslq	192(%rcx), %rbp
	movslq	%eax, %rdi
	addq	%rdi, %rbp
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	104(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	40(%rsp), %ecx          # 4-byte Reload
	movl	52(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, %r8d
	movl	56(%rsp), %r9d          # 4-byte Reload
	callq	LumaPrediction4x4
	movq	imgY_org(%rip), %r8
	movq	img(%rip), %rcx
	movq	(%r8,%r13,8), %rsi
	movzwl	(%rsi,%rbp,2), %edi
	leaq	12632(%rcx,%r15), %rax
	movzwl	(%rbx,%rax), %edx
	subl	%edx, %edi
	movl	%edi, 13152(%rcx,%r15,2)
	movzwl	2(%rsi,%rbp,2), %edx
	movzwl	2(%rbx,%rax), %edi
	subl	%edi, %edx
	movl	%edx, 13156(%rcx,%r15,2)
	movzwl	4(%rsi,%rbp,2), %edx
	movzwl	4(%rbx,%rax), %edi
	subl	%edi, %edx
	movl	%edx, 13160(%rcx,%r15,2)
	movzwl	6(%rsi,%rbp,2), %edx
	movzwl	6(%rbx,%rax), %esi
	subl	%esi, %edx
	movl	%edx, 13164(%rcx,%r15,2)
	movq	8(%r8,%r13,8), %rdx
	movzwl	(%rdx,%rbp,2), %esi
	movzwl	32(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13216(%rcx,%r15,2)
	movzwl	2(%rdx,%rbp,2), %esi
	movzwl	34(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13220(%rcx,%r15,2)
	movzwl	4(%rdx,%rbp,2), %esi
	movzwl	36(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13224(%rcx,%r15,2)
	movzwl	6(%rdx,%rbp,2), %edx
	movzwl	38(%rbx,%rax), %esi
	subl	%esi, %edx
	movl	%edx, 13228(%rcx,%r15,2)
	movq	16(%r8,%r13,8), %rdx
	movzwl	(%rdx,%rbp,2), %esi
	movzwl	64(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13280(%rcx,%r15,2)
	movzwl	2(%rdx,%rbp,2), %esi
	movzwl	66(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13284(%rcx,%r15,2)
	movzwl	4(%rdx,%rbp,2), %esi
	movzwl	68(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13288(%rcx,%r15,2)
	movzwl	6(%rdx,%rbp,2), %edx
	movzwl	70(%rbx,%rax), %esi
	subl	%esi, %edx
	movl	%edx, 13292(%rcx,%r15,2)
	movq	24(%r8,%r13,8), %rdx
	movzwl	(%rdx,%rbp,2), %esi
	movzwl	96(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13344(%rcx,%r15,2)
	movzwl	2(%rdx,%rbp,2), %esi
	movzwl	98(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13348(%rcx,%r15,2)
	movzwl	4(%rdx,%rbp,2), %esi
	movzwl	100(%rbx,%rax), %edi
	subl	%edi, %esi
	movl	%esi, 13352(%rcx,%r15,2)
	movzwl	6(%rdx,%rbp,2), %edx
	movzwl	102(%rbx,%rax), %eax
	subl	%eax, %edx
	movl	%edx, 13356(%rcx,%r15,2)
	movl	%r14d, %r10d
	jmp	.LBB8_7
	.p2align	4, 0x90
.LBB8_6:                                #   in Loop: Header=BB8_5 Depth=1
	movl	52(%rsp), %r10d         # 4-byte Reload
.LBB8_7:                                # %.loopexit231
                                        #   in Loop: Header=BB8_5 Depth=1
	movl	40(%rsp), %r11d         # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	24(%rsp), %edi          # 4-byte Reload
	movl	56(%rsp), %r9d          # 4-byte Reload
	subq	$-128, %r15
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	4(%rax,%r12), %rax
	addq	$4, %r12
	cmpq	88(%rsp), %rax          # 8-byte Folded Reload
	jl	.LBB8_5
# BB#8:
	cmpl	$1, 15256(%rcx)
	sete	%al
	orb	44(%rsp), %al           # 1-byte Folded Reload
	jne	.LBB8_12
# BB#9:
	cmpl	$3, 20(%rcx)
	je	.LBB8_12
# BB#10:
	leaq	20(%rsp), %rsi
	xorl	%edx, %edx
	movq	128(%rsp), %rdi         # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	dct_luma8x8
	testl	%eax, %eax
	je	.LBB8_12
# BB#11:
	movq	120(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rax), %eax
	movl	68(%rsp), %ecx          # 4-byte Reload
	subl	%eax, %ecx
	movl	$51, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	cltq
	movq	80(%rsp), %rcx          # 8-byte Reload
	orq	%rax, (%rcx)
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	48(%rsp), %ecx          # 4-byte Reload
	orl	%ecx, (%rax)
	jmp	.LBB8_12
.LBB8_25:                               # %.preheader180
	leal	8(%rax), %esi
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movswl	%r8w, %esi
	movl	%esi, 36(%rsp)          # 4-byte Spill
	movl	%eax, %esi
	movslq	%edi, %rax
	movslq	%r14d, %rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	%rax, %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	shlq	$4, %rax
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	leaq	6363(%rax,%rsi), %rbx
	jmp	.LBB8_26
	.p2align	4, 0x90
.LBB8_51:                               # %.us-lcssa.us._crit_edge
                                        #   in Loop: Header=BB8_26 Depth=1
	movq	img(%rip), %rcx
	movq	104(%rsp), %rbx         # 8-byte Reload
	addq	$64, %rbx
.LBB8_26:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_27 Depth 2
                                        #     Child Loop BB8_42 Depth 2
	movslq	196(%rcx), %rax
	movslq	24(%rsp), %r12          # 4-byte Folded Reload
	addq	%rax, %r12
	cmpb	$0, 44(%rsp)            # 1-byte Folded Reload
	movq	%rbx, %r14
	movq	96(%rsp), %r15          # 8-byte Reload
	movq	%r15, %r13
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	jne	.LBB8_42
	jmp	.LBB8_27
	.p2align	4, 0x90
.LBB8_41:                               # %..split.us_crit_edge
                                        #   in Loop: Header=BB8_42 Depth=2
	movq	img(%rip), %rcx
	addq	$4, %r14
.LBB8_42:                               #   Parent Loop BB8_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	192(%rcx), %ebx
	addl	%r13d, %ebx
	movswl	232(%rsp), %eax
	movl	%eax, (%rsp)
	movswl	224(%rsp), %r9d
	movl	%r13d, %edi
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	%r11d, %ecx
	movl	%r10d, %r8d
	callq	LumaPrediction4x4
	movq	imgY_org(%rip), %rdx
	movq	img(%rip), %rax
	movslq	%ebx, %rcx
	movq	(%rdx,%r12,8), %rsi
	movzwl	(%rsi,%rcx,2), %edi
	movzwl	-102(%rax,%r14,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13136(%rax)
	movzwl	2(%rsi,%rcx,2), %edi
	movzwl	-100(%rax,%r14,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13140(%rax)
	movzwl	4(%rsi,%rcx,2), %edi
	movzwl	-98(%rax,%r14,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13144(%rax)
	movzwl	6(%rsi,%rcx,2), %esi
	movzwl	-96(%rax,%r14,2), %edi
	subl	%edi, %esi
	movl	%esi, 13148(%rax)
	movq	8(%rdx,%r12,8), %rsi
	movzwl	(%rsi,%rcx,2), %edi
	movzwl	-70(%rax,%r14,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13200(%rax)
	movzwl	2(%rsi,%rcx,2), %edi
	movzwl	-68(%rax,%r14,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13204(%rax)
	movzwl	4(%rsi,%rcx,2), %edi
	movzwl	-66(%rax,%r14,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13208(%rax)
	movzwl	6(%rsi,%rcx,2), %esi
	movzwl	-64(%rax,%r14,2), %edi
	subl	%edi, %esi
	movl	%esi, 13212(%rax)
	movq	16(%rdx,%r12,8), %rsi
	movzwl	(%rsi,%rcx,2), %edi
	movzwl	-38(%rax,%r14,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13264(%rax)
	movzwl	2(%rsi,%rcx,2), %edi
	movzwl	-36(%rax,%r14,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13268(%rax)
	movzwl	4(%rsi,%rcx,2), %edi
	movzwl	-34(%rax,%r14,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13272(%rax)
	movzwl	6(%rsi,%rcx,2), %esi
	movzwl	-32(%rax,%r14,2), %edi
	subl	%edi, %esi
	movl	%esi, 13276(%rax)
	movq	24(%rdx,%r12,8), %rdx
	movzwl	(%rdx,%rcx,2), %esi
	movzwl	-6(%rax,%r14,2), %edi
	subl	%edi, %esi
	movl	%esi, 13328(%rax)
	movzwl	2(%rdx,%rcx,2), %esi
	movzwl	-4(%rax,%r14,2), %edi
	subl	%edi, %esi
	movl	%esi, 13332(%rax)
	movzwl	4(%rdx,%rcx,2), %esi
	movzwl	-2(%rax,%r14,2), %edi
	subl	%edi, %esi
	movl	%esi, 13336(%rax)
	movzwl	6(%rdx,%rcx,2), %ecx
	movzwl	(%rax,%r14,2), %edx
	subl	%edx, %ecx
	movl	%ecx, 13340(%rax)
	cmpl	$0, 44(%rax)
	jne	.LBB8_40
# BB#32:                                #   in Loop: Header=BB8_42 Depth=2
	cmpl	$1, 15540(%rax)
	jne	.LBB8_40
# BB#33:                                #   in Loop: Header=BB8_42 Depth=2
	cmpl	$3, 20(%rax)
	jne	.LBB8_34
# BB#35:                                #   in Loop: Header=BB8_42 Depth=2
	movl	sp2_frame_indicator(%rip), %eax
	orl	si_frame_indicator(%rip), %eax
	je	.LBB8_37
# BB#36:                                #   in Loop: Header=BB8_42 Depth=2
	movl	%r13d, %edi
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	leaq	20(%rsp), %rdx
	callq	dct_luma_sp2
	testl	%eax, %eax
	jne	.LBB8_39
	jmp	.LBB8_40
.LBB8_34:                               #   in Loop: Header=BB8_42 Depth=2
	xorl	%ecx, %ecx
	movl	%r13d, %edi
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	leaq	20(%rsp), %rdx
	callq	dct_luma
	testl	%eax, %eax
	jne	.LBB8_39
	jmp	.LBB8_40
.LBB8_37:                               #   in Loop: Header=BB8_42 Depth=2
	movl	%r13d, %edi
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	leaq	20(%rsp), %rdx
	callq	dct_luma_sp
	testl	%eax, %eax
	je	.LBB8_40
.LBB8_39:                               #   in Loop: Header=BB8_42 Depth=2
	movl	%r13d, %ecx
	sarl	$2, %ecx
	addl	24(%rsp), %ecx          # 4-byte Folded Reload
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rax
	movq	80(%rsp), %rcx          # 8-byte Reload
	orq	%rax, (%rcx)
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	48(%rsp), %ecx          # 4-byte Reload
	orl	%ecx, (%rax)
	.p2align	4, 0x90
.LBB8_40:                               #   in Loop: Header=BB8_42 Depth=2
	addq	$4, %r13
	cmpq	56(%rsp), %r13          # 8-byte Folded Reload
	movl	52(%rsp), %r10d         # 4-byte Reload
	movl	40(%rsp), %r11d         # 4-byte Reload
	jl	.LBB8_41
	jmp	.LBB8_50
	.p2align	4, 0x90
.LBB8_49:                               # %..split_crit_edge
                                        #   in Loop: Header=BB8_27 Depth=2
	movq	img(%rip), %rcx
	addq	$4, %rbx
.LBB8_27:                               #   Parent Loop BB8_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	192(%rcx), %ebp
	addl	%r15d, %ebp
	movswl	232(%rsp), %eax
	movl	%eax, (%rsp)
	movswl	224(%rsp), %r9d
	movl	%r15d, %edi
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	%r11d, %ecx
	movl	%r10d, %r8d
	callq	LumaPrediction4x4
	movq	imgY_org(%rip), %rdx
	movq	img(%rip), %rax
	movslq	%ebp, %rcx
	movq	(%rdx,%r12,8), %rsi
	movzwl	(%rsi,%rcx,2), %edi
	movzwl	-102(%rax,%rbx,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13136(%rax)
	movzwl	2(%rsi,%rcx,2), %edi
	movzwl	-100(%rax,%rbx,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13140(%rax)
	movzwl	4(%rsi,%rcx,2), %edi
	movzwl	-98(%rax,%rbx,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13144(%rax)
	movzwl	6(%rsi,%rcx,2), %esi
	movzwl	-96(%rax,%rbx,2), %edi
	subl	%edi, %esi
	movl	%esi, 13148(%rax)
	movq	8(%rdx,%r12,8), %rsi
	movzwl	(%rsi,%rcx,2), %edi
	movzwl	-70(%rax,%rbx,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13200(%rax)
	movzwl	2(%rsi,%rcx,2), %edi
	movzwl	-68(%rax,%rbx,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13204(%rax)
	movzwl	4(%rsi,%rcx,2), %edi
	movzwl	-66(%rax,%rbx,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13208(%rax)
	movzwl	6(%rsi,%rcx,2), %esi
	movzwl	-64(%rax,%rbx,2), %edi
	subl	%edi, %esi
	movl	%esi, 13212(%rax)
	movq	16(%rdx,%r12,8), %rsi
	movzwl	(%rsi,%rcx,2), %edi
	movzwl	-38(%rax,%rbx,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13264(%rax)
	movzwl	2(%rsi,%rcx,2), %edi
	movzwl	-36(%rax,%rbx,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13268(%rax)
	movzwl	4(%rsi,%rcx,2), %edi
	movzwl	-34(%rax,%rbx,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 13272(%rax)
	movzwl	6(%rsi,%rcx,2), %esi
	movzwl	-32(%rax,%rbx,2), %edi
	subl	%edi, %esi
	movl	%esi, 13276(%rax)
	movq	24(%rdx,%r12,8), %rdx
	movzwl	(%rdx,%rcx,2), %esi
	movzwl	-6(%rax,%rbx,2), %edi
	subl	%edi, %esi
	movl	%esi, 13328(%rax)
	movzwl	2(%rdx,%rcx,2), %esi
	movzwl	-4(%rax,%rbx,2), %edi
	subl	%edi, %esi
	movl	%esi, 13332(%rax)
	movzwl	4(%rdx,%rcx,2), %esi
	movzwl	-2(%rax,%rbx,2), %edi
	subl	%edi, %esi
	movl	%esi, 13336(%rax)
	movzwl	6(%rdx,%rcx,2), %ecx
	movzwl	(%rax,%rbx,2), %edx
	subl	%edx, %ecx
	movl	%ecx, 13340(%rax)
	cmpl	$1, 15256(%rax)
	jne	.LBB8_30
# BB#28:                                #   in Loop: Header=BB8_27 Depth=2
	cmpl	$0, 44(%rax)
	jne	.LBB8_48
# BB#29:                                #   in Loop: Header=BB8_27 Depth=2
	cmpl	$1, 15540(%rax)
	jne	.LBB8_48
	.p2align	4, 0x90
.LBB8_30:                               #   in Loop: Header=BB8_27 Depth=2
	cmpl	$3, 20(%rax)
	jne	.LBB8_31
# BB#43:                                #   in Loop: Header=BB8_27 Depth=2
	movl	sp2_frame_indicator(%rip), %eax
	orl	si_frame_indicator(%rip), %eax
	je	.LBB8_44
# BB#45:                                #   in Loop: Header=BB8_27 Depth=2
	movl	%r15d, %edi
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	leaq	20(%rsp), %rdx
	callq	dct_luma_sp2
	testl	%eax, %eax
	jne	.LBB8_47
	jmp	.LBB8_48
	.p2align	4, 0x90
.LBB8_31:                               #   in Loop: Header=BB8_27 Depth=2
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	leaq	20(%rsp), %rdx
	callq	dct_luma
	testl	%eax, %eax
	jne	.LBB8_47
	jmp	.LBB8_48
.LBB8_44:                               #   in Loop: Header=BB8_27 Depth=2
	movl	%r15d, %edi
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	leaq	20(%rsp), %rdx
	callq	dct_luma_sp
	testl	%eax, %eax
	je	.LBB8_48
	.p2align	4, 0x90
.LBB8_47:                               #   in Loop: Header=BB8_27 Depth=2
	movl	%r15d, %ecx
	sarl	$2, %ecx
	addl	24(%rsp), %ecx          # 4-byte Folded Reload
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rax
	movq	80(%rsp), %rcx          # 8-byte Reload
	orq	%rax, (%rcx)
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	48(%rsp), %ecx          # 4-byte Reload
	orl	%ecx, (%rax)
.LBB8_48:                               #   in Loop: Header=BB8_27 Depth=2
	addq	$4, %r15
	cmpq	56(%rsp), %r15          # 8-byte Folded Reload
	movl	52(%rsp), %r10d         # 4-byte Reload
	movl	40(%rsp), %r11d         # 4-byte Reload
	jl	.LBB8_49
.LBB8_50:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB8_26 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	addq	$4, %rcx
	movq	%rcx, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpq	88(%rsp), %rcx          # 8-byte Folded Reload
	jl	.LBB8_51
.LBB8_12:                               # %.critedge
	movq	img(%rip), %rsi
	cmpl	$1, 15256(%rsi)
	setne	%al
	cmpl	$4, 20(%rsp)
	movq	144(%rsp), %r14         # 8-byte Reload
	movl	112(%rsp), %r15d        # 4-byte Reload
	jg	.LBB8_24
# BB#13:                                # %.critedge
	movl	44(%rsp), %ecx          # 4-byte Reload
	notb	%cl
	andb	%al, %cl
	je	.LBB8_24
# BB#14:
	cmpl	$0, 44(%rsi)
	je	.LBB8_15
.LBB8_16:
	cmpl	$3, 20(%rsi)
	jne	.LBB8_19
# BB#17:
	cmpl	$1, si_frame_indicator(%rip)
	je	.LBB8_24
# BB#18:
	cmpl	$1, sp2_frame_indicator(%rip)
	je	.LBB8_24
.LBB8_19:
	movl	$0, 20(%rsp)
	movl	$63, %eax
	subl	48(%rsp), %eax          # 4-byte Folded Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	andl	%eax, (%rcx)
	movq	120(%rsp), %rax         # 8-byte Reload
	addl	%eax, %eax
	movl	68(%rsp), %ecx          # 4-byte Reload
	subl	%eax, %ecx
	movl	$51, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	notl	%eax
	cltq
	movq	80(%rsp), %rcx          # 8-byte Reload
	andq	%rax, (%rcx)
	movq	136(%rsp), %rax         # 8-byte Reload
	movl	%eax, %r8d
	movslq	%r14d, %r9
	movslq	%r15d, %rdi
	movq	enc_picture(%rip), %rdx
	movq	6440(%rdx), %rdx
	movslq	180(%rsi), %rbp
	addq	%r9, %rbp
	movq	(%rdx,%rbp,8), %rbp
	movslq	176(%rsi), %rbx
	movq	%rax, %r10
	movslq	%eax, %rdx
	addq	%rdx, %rbx
	movq	%r9, %rcx
	shlq	$5, %rcx
	addq	%rsi, %rcx
	movups	12624(%rcx,%r8,2), %xmm0
	movups	%xmm0, (%rbp,%rbx,2)
	movq	%r9, %rax
	orq	$1, %rax
	cmpq	%rdi, %rax
	jge	.LBB8_20
# BB#53:
	movq	enc_picture(%rip), %rcx
	movq	6440(%rcx), %rcx
	movq	img(%rip), %rdi
	movslq	180(%rdi), %rbp
	addq	%rax, %rbp
	movq	(%rcx,%rbp,8), %rcx
	movslq	176(%rdi), %rbp
	addq	%rdx, %rbp
	movq	%rax, %rsi
	shlq	$5, %rsi
	addq	%rdi, %rsi
	movups	12624(%rsi,%r8,2), %xmm0
	movups	%xmm0, (%rcx,%rbp,2)
	incq	%rax
	movq	enc_picture(%rip), %rcx
	movq	6440(%rcx), %rcx
	movq	img(%rip), %rsi
	movslq	180(%rsi), %rdi
	movslq	%eax, %rbp
	addq	%rdi, %rbp
	movq	(%rcx,%rbp,8), %rcx
	movslq	176(%rsi), %rdi
	addq	%rdx, %rdi
	shlq	$5, %rax
	addq	%rsi, %rax
	movups	12624(%rax,%r8,2), %xmm0
	movups	%xmm0, (%rcx,%rdi,2)
	movq	%r9, %rbp
	orq	$3, %rbp
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rax
	movq	img(%rip), %rcx
	movslq	180(%rcx), %rdi
	addq	%rbp, %rdi
	movq	(%rax,%rdi,8), %rax
	movslq	176(%rcx), %rdi
	addq	%rdx, %rdi
	movq	%rbp, %rsi
	shlq	$5, %rsi
	addq	%rcx, %rsi
	movups	12624(%rsi,%r8,2), %xmm0
	movups	%xmm0, (%rax,%rdi,2)
	leaq	1(%rbp), %rax
	movq	enc_picture(%rip), %rcx
	movq	6440(%rcx), %rcx
	movq	img(%rip), %rsi
	movslq	180(%rsi), %rdi
	movslq	%eax, %rbx
	addq	%rdi, %rbx
	movq	(%rcx,%rbx,8), %rcx
	movslq	176(%rsi), %rdi
	addq	%rdx, %rdi
	shlq	$5, %rax
	addq	%rsi, %rax
	movups	12624(%rax,%r8,2), %xmm0
	movups	%xmm0, (%rcx,%rdi,2)
	leaq	2(%rbp), %rax
	movq	enc_picture(%rip), %rcx
	movq	6440(%rcx), %rcx
	movq	img(%rip), %rsi
	movslq	180(%rsi), %rdi
	movslq	%eax, %rbx
	addq	%rdi, %rbx
	movq	(%rcx,%rbx,8), %rcx
	movslq	176(%rsi), %rdi
	addq	%rdx, %rdi
	shlq	$5, %rax
	addq	%rsi, %rax
	movups	12624(%rax,%r8,2), %xmm0
	movups	%xmm0, (%rcx,%rdi,2)
	addq	$3, %rbp
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rax
	movq	img(%rip), %rcx
	movslq	180(%rcx), %rsi
	movslq	%ebp, %rdi
	addq	%rsi, %rdi
	movq	(%rax,%rdi,8), %rax
	movslq	176(%rcx), %rsi
	addq	%rdx, %rsi
	shlq	$5, %rbp
	addq	%rcx, %rbp
	movups	12624(%rbp,%r8,2), %xmm0
	movups	%xmm0, (%rax,%rsi,2)
	orq	$7, %r9
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rax
	movq	img(%rip), %rcx
	movslq	180(%rcx), %rsi
	addq	%r9, %rsi
	movq	(%rax,%rsi,8), %rax
	movslq	176(%rcx), %rsi
	addq	%rdx, %rsi
	shlq	$5, %r9
	addq	%rcx, %r9
	movups	12624(%r9,%r8,2), %xmm0
	movups	%xmm0, (%rax,%rsi,2)
.LBB8_20:
	movq	img(%rip), %rax
	cmpl	$3, 20(%rax)
	jne	.LBB8_24
# BB#21:                                # %.preheader179.preheader
	movl	%r14d, %ebx
	movq	%r10, %rbp
	.p2align	4, 0x90
.LBB8_22:                               # %.preheader179
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edi
	movl	%ebx, %esi
	callq	copyblock_sp
	addl	$4, %ebx
	cmpl	%r15d, %ebx
	jl	.LBB8_22
# BB#23:
	movl	%ebp, %ebx
	orl	$4, %ebx
	cmpl	%ebx, %ebp
	jae	.LBB8_24
	.p2align	4, 0x90
.LBB8_52:                               # %.preheader.1
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %edi
	movl	%r14d, %esi
	callq	copyblock_sp
	addl	$4, %r14d
	cmpl	%r15d, %r14d
	jl	.LBB8_52
	jmp	.LBB8_24
.LBB8_15:
	cmpl	$0, 15540(%rsi)
	je	.LBB8_16
.LBB8_24:                               # %.loopexit
	movl	20(%rsp), %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	LumaResidualCoding8x8, .Lfunc_end8-LumaResidualCoding8x8
	.cfi_endproc

	.p2align	4, 0x90
	.type	OneComponentChromaPrediction4x4_retrieve,@function
OneComponentChromaPrediction4x4_retrieve: # @OneComponentChromaPrediction4x4_retrieve
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi82:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi83:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi85:
	.cfi_def_cfa_offset 160
.Lcfi86:
	.cfi_offset %rbx, -56
.Lcfi87:
	.cfi_offset %r12, -48
.Lcfi88:
	.cfi_offset %r13, -40
.Lcfi89:
	.cfi_offset %r14, -32
.Lcfi90:
	.cfi_offset %r15, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movl	%edx, %r15d
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	movl	160(%rsp), %r10d
	movq	img(%rip), %rdx
	movq	14224(%rdx), %rax
	movslq	12(%rdx), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	movslq	432(%rax,%rcx), %r14
	movl	chroma_shift_x(%rip), %ebx
	movl	$4, 24(%rsp)            # 4-byte Folded Spill
	movl	$4, %ecx
	subl	%ebx, %ecx
	leal	2(%rsi), %r11d
	movl	200(%rdx), %eax
	leal	(%rax,%rsi), %ebp
	leal	2(%rax,%rsi), %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	sarl	%cl, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r11d
	movl	%ebx, %ecx
	shll	%cl, %ebp
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	shll	%cl, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movslq	%r8d, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	addq	%rax, %r14
	movq	listX(,%r14,8), %rax
	movslq	%r9d, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %rcx
	movq	6464(%rcx), %rax
	movslq	168(%rsp), %rbp
	movq	(%rax,%rbp,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	active_sps(%rip), %rax
	cmpl	$1, 32(%rax)
	movl	chroma_shift_y(%rip), %eax
	movl	$80, 12(%rsp)           # 4-byte Folded Spill
	jne	.LBB9_2
# BB#1:
	movl	6424(%rcx), %ebp
	addl	$80, %ebp
	movl	%ebp, 12(%rsp)          # 4-byte Spill
.LBB9_2:
	subl	%eax, 24(%rsp)          # 4-byte Folded Spill
	addl	$80, 20(%rsp)           # 4-byte Folded Spill
	addl	$80, 16(%rsp)           # 4-byte Folded Spill
	movl	6416(%rcx), %ebp
	movl	%ebp, width_pad_cr(%rip)
	movl	6420(%rcx), %ecx
	movl	%ecx, height_pad_cr(%rip)
	movslq	%esi, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movslq	%r10d, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movslq	%r11d, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leal	3(%r15), %ecx
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	72(%rsp), %r14          # 8-byte Reload
	jmp	.LBB9_3
	.p2align	4, 0x90
.LBB9_4:                                # %._crit_edge
                                        #   in Loop: Header=BB9_3 Depth=1
	incl	%r15d
	movq	img(%rip), %rdx
	movl	chroma_shift_y(%rip), %eax
	addq	$8, %r12
.LBB9_3:                                # =>This Inner Loop Header: Depth=1
	movq	%r15, 96(%rsp)          # 8-byte Spill
	movl	%r15d, %esi
	movl	24(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %esi
	movl	204(%rdx), %r13d
	addl	%r15d, %r13d
	movl	%eax, %ecx
	shll	%cl, %r13d
	addl	12(%rsp), %r13d         # 4-byte Folded Reload
	movslq	%esi, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	(%rbp,%rax,8), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%r14,8), %rax
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	(%rax,%r15,8), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movswl	(%rax), %edx
	addl	20(%rsp), %edx          # 4-byte Folded Reload
	movswl	2(%rax), %esi
	addl	%r13d, %esi
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	UMVLine8X_chroma
	movzwl	(%rax), %ecx
	movw	%cx, (%r12)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%r12)
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	(%rbp,%rax,8), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	96(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movswl	(%rax), %edx
	addl	16(%rsp), %edx          # 4-byte Folded Reload
	movswl	2(%rax), %esi
	addl	%r13d, %esi
	movq	%rbx, %rdi
	callq	UMVLine8X_chroma
	movzwl	(%rax), %ecx
	movw	%cx, 4(%r12)
	movzwl	2(%rax), %eax
	movw	%ax, 6(%r12)
	cmpl	28(%rsp), %r15d         # 4-byte Folded Reload
	jl	.LBB9_4
# BB#5:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	OneComponentChromaPrediction4x4_retrieve, .Lfunc_end9-OneComponentChromaPrediction4x4_retrieve
	.cfi_endproc

	.p2align	4, 0x90
	.type	OneComponentChromaPrediction4x4_regenerate,@function
OneComponentChromaPrediction4x4_regenerate: # @OneComponentChromaPrediction4x4_regenerate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi95:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi96:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 56
	subq	$32, %rsp
.Lcfi98:
	.cfi_def_cfa_offset 88
.Lcfi99:
	.cfi_offset %rbx, -56
.Lcfi100:
	.cfi_offset %r12, -48
.Lcfi101:
	.cfi_offset %r13, -40
.Lcfi102:
	.cfi_offset %r14, -32
.Lcfi103:
	.cfi_offset %r15, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%edx, %r8d
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	img(%rip), %rbx
	movl	15544(%rbx), %r12d
	movl	$64, %eax
	xorl	%edx, %edx
	idivl	%r12d
	movl	%eax, %r15d
	movl	15548(%rbx), %r11d
	movl	$64, %eax
	xorl	%edx, %edx
	idivl	%r11d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	%eax, %edx
	imull	%r15d, %edx
	movq	14224(%rbx), %r10
	movslq	12(%rbx), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	movl	432(%r10,%rcx), %r10d
	movl	%edx, -116(%rsp)        # 4-byte Spill
	sarl	%edx
	movl	%edx, -120(%rsp)        # 4-byte Spill
	testl	%r10d, %r10d
	setne	%cl
	addl	%ebp, %r10d
	movslq	%r10d, %rdx
	movq	listX(,%rdx,8), %r10
	movslq	%r9d, %rdx
	movq	%rdx, -24(%rsp)         # 8-byte Spill
	movq	(%r10,%rdx,8), %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	6472(%rdx), %r9
	movslq	96(%rsp), %rdx
	movq	(%r9,%rdx,8), %rdx
	movl	80(%rbx), %r10d
	sarl	%cl, %r10d
	decl	%r10d
	movl	64(%rbx), %r13d
	decl	%r13d
	sarl	$2, %r11d
	movl	%r11d, -80(%rsp)        # 4-byte Spill
	sarl	$2, %r12d
	movslq	%ebp, %r9
	movq	%rax, %r14
	movl	204(%rbx), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	200(%rbx), %eax
	movq	active_sps(%rip), %rcx
	movl	32(%rcx), %ecx
	movl	%ecx, -84(%rsp)         # 4-byte Spill
	leal	(%rax,%rsi), %ebx
	imull	%r15d, %ebx
	leal	1(%rax,%rsi), %eax
	imull	%r15d, %eax
	decl	%eax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	leal	-1(%r15), %r11d
	leal	-1(%r14), %eax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	movq	%rsi, -72(%rsp)         # 8-byte Spill
	leal	4(%rsi), %ecx
	movq	%r15, %rsi
	movq	%rbx, %r15
	movq	%rdx, %rbx
	movslq	88(%rsp), %rbp
	leal	3(%r8), %eax
	movl	%eax, -88(%rsp)         # 4-byte Spill
	movl	%r12d, -112(%rsp)       # 4-byte Spill
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movq	%r9, -32(%rsp)          # 8-byte Spill
	movl	%ecx, -124(%rsp)        # 4-byte Spill
	movq	%r15, -40(%rsp)         # 8-byte Spill
	movq	%rbp, -48(%rsp)         # 8-byte Spill
	movl	%r11d, -128(%rsp)       # 4-byte Spill
	movq	%rbx, -64(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB10_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_3 Depth 2
                                        #     Child Loop BB10_5 Depth 2
	movl	%r8d, %eax
	cltd
	idivl	-80(%rsp)               # 4-byte Folded Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	leal	(%rcx,%r8), %edx
	imull	%r14d, %edx
	cmpl	$1, -84(%rsp)           # 4-byte Folded Reload
	cltq
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movl	%edx, -108(%rsp)        # 4-byte Spill
	jne	.LBB10_2
# BB#4:                                 # %.split.us.preheader
                                        #   in Loop: Header=BB10_1 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	6424(%rax), %eax
	movl	%eax, -76(%rsp)         # 4-byte Spill
	xorl	%r15d, %r15d
	movq	-72(%rsp), %rax         # 8-byte Reload
	movl	%eax, %r8d
	movq	%rdi, %rdx
	.p2align	4, 0x90
.LBB10_5:                               # %.split.us
                                        #   Parent Loop BB10_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, -96(%rsp)         # 8-byte Spill
	movl	%r8d, %eax
	cltd
	idivl	%r12d
	cltq
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	(%rax,%r9,8), %rax
	movq	-24(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movswl	(%rax), %ecx
	movq	-40(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r15), %r9d
	addl	%ecx, %r9d
	movswl	2(%rax), %ebx
	addl	-108(%rsp), %ebx        # 4-byte Folded Reload
	addl	-76(%rsp), %ebx         # 4-byte Folded Reload
	movl	%r9d, %eax
	cltd
	idivl	%esi
	movl	%eax, %r12d
	testl	%r12d, %r12d
	movl	$0, %r11d
	cmovsl	%r11d, %r12d
	cmpl	%r13d, %r12d
	cmovgl	%r13d, %r12d
	movl	%ebx, %eax
	cltd
	idivl	%r14d
	movl	%eax, %ebp
	testl	%ebp, %ebp
	cmovsl	%r11d, %ebp
	cmpl	%r10d, %ebp
	cmovgl	%r10d, %ebp
	movq	-56(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r15), %eax
	addl	%ecx, %eax
	cltd
	idivl	%esi
	movl	%eax, %ecx
	testl	%ecx, %ecx
	cmovsl	%r11d, %ecx
	cmpl	%r13d, %ecx
	cmovgl	%r13d, %ecx
	movq	-104(%rsp), %rdi        # 8-byte Reload
	leal	(%rbx,%rdi), %eax
	cltd
	idivl	%r14d
	testl	%eax, %eax
	cmovsl	%r11d, %eax
	cmpl	%r10d, %eax
	cmovgl	%r10d, %eax
	andl	-128(%rsp), %r9d        # 4-byte Folded Reload
	movl	%esi, %edx
	subl	%r9d, %edx
	movslq	%ebp, %rsi
	movq	-64(%rsp), %r11         # 8-byte Reload
	movq	(%r11,%rsi,8), %rsi
	movslq	%r12d, %r12
	movzwl	(%rsi,%r12,2), %ebp
	imull	%edx, %ebp
	cltq
	movq	(%r11,%rax,8), %r11
	movzwl	(%r11,%r12,2), %r12d
	imull	%edx, %r12d
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %eax
	movq	-16(%rsp), %rsi         # 8-byte Reload
	movzwl	(%r11,%rcx,2), %ecx
	imull	%r9d, %eax
	imull	%r9d, %ecx
	movq	-32(%rsp), %r9          # 8-byte Reload
	andl	%edi, %ebx
	addl	%r12d, %ecx
	movl	-112(%rsp), %r12d       # 4-byte Reload
	movl	%r14d, %edx
	subl	%ebx, %edx
	imull	%ebx, %ecx
	addl	%ebp, %eax
	imull	%edx, %eax
	addl	-120(%rsp), %eax        # 4-byte Folded Reload
	addl	%ecx, %eax
	movl	-124(%rsp), %ecx        # 4-byte Reload
	cltd
	idivl	-116(%rsp)              # 4-byte Folded Reload
	movq	-96(%rsp), %rdx         # 8-byte Reload
	movw	%ax, (%rdx)
	addq	$2, %rdx
	incl	%r8d
	addl	%esi, %r15d
	cmpl	%ecx, %r8d
	jl	.LBB10_5
	jmp	.LBB10_6
	.p2align	4, 0x90
.LBB10_2:                               # %.split.preheader
                                        #   in Loop: Header=BB10_1 Depth=1
	movq	%rdi, %rcx
	xorl	%edi, %edi
	movq	-72(%rsp), %rax         # 8-byte Reload
	movl	%eax, %r8d
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB10_3:                               # %.split
                                        #   Parent Loop BB10_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, -96(%rsp)         # 8-byte Spill
	movl	%r8d, %eax
	cltd
	idivl	%r12d
	cltq
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	(%rax,%r9,8), %rax
	movq	-24(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movswl	(%rax), %ebp
	movq	-40(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%rdi), %r12d
	addl	%ebp, %r12d
	movswl	2(%rax), %r9d
	addl	-108(%rsp), %r9d        # 4-byte Folded Reload
	movl	%r12d, %eax
	cltd
	idivl	%esi
	movl	%eax, %ecx
	testl	%ecx, %ecx
	movl	$0, %r11d
	cmovsl	%r11d, %ecx
	cmpl	%r13d, %ecx
	cmovgl	%r13d, %ecx
	movl	%r9d, %eax
	cltd
	idivl	%r14d
	movl	%eax, %ebx
	testl	%ebx, %ebx
	cmovsl	%r11d, %ebx
	cmpl	%r10d, %ebx
	cmovgl	%r10d, %ebx
	movq	-56(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rdi), %eax
	addl	%ebp, %eax
	cltd
	idivl	%esi
	movl	%eax, %ebp
	testl	%ebp, %ebp
	cmovsl	%r11d, %ebp
	cmpl	%r13d, %ebp
	cmovgl	%r13d, %ebp
	movq	-104(%rsp), %rax        # 8-byte Reload
	leal	(%r9,%rax), %eax
	cltd
	idivl	%r14d
	testl	%eax, %eax
	cmovsl	%r11d, %eax
	cmpl	%r10d, %eax
	cmovgl	%r10d, %eax
	andl	-128(%rsp), %r12d       # 4-byte Folded Reload
	movl	%esi, %edx
	subl	%r12d, %edx
	movslq	%ebx, %rbx
	movl	%r13d, %r11d
	movl	%r10d, %r13d
	movq	%r14, %r10
	movq	-64(%rsp), %r14         # 8-byte Reload
	movq	(%r14,%rbx,8), %r15
	movslq	%ecx, %rcx
	movzwl	(%r15,%rcx,2), %esi
	imull	%edx, %esi
	cltq
	movq	(%r14,%rax,8), %rbx
	movq	%r10, %r14
	movl	%r13d, %r10d
	movl	%r11d, %r13d
	movzwl	(%rbx,%rcx,2), %ecx
	imull	%edx, %ecx
	movslq	%ebp, %rdx
	movzwl	(%r15,%rdx,2), %eax
	movzwl	(%rbx,%rdx,2), %edx
	imull	%r12d, %eax
	imull	%r12d, %edx
	movl	-112(%rsp), %r12d       # 4-byte Reload
	andl	-104(%rsp), %r9d        # 4-byte Folded Reload
	addl	%ecx, %edx
	movl	%r14d, %ecx
	subl	%r9d, %ecx
	imull	%r9d, %edx
	movq	-32(%rsp), %r9          # 8-byte Reload
	addl	%esi, %eax
	movq	-16(%rsp), %rsi         # 8-byte Reload
	imull	%ecx, %eax
	movl	-124(%rsp), %ecx        # 4-byte Reload
	addl	-120(%rsp), %eax        # 4-byte Folded Reload
	addl	%edx, %eax
	cltd
	idivl	-116(%rsp)              # 4-byte Folded Reload
	movq	-96(%rsp), %rdx         # 8-byte Reload
	movw	%ax, (%rdx)
	addq	$2, %rdx
	incl	%r8d
	addl	%esi, %edi
	cmpl	%ecx, %r8d
	jl	.LBB10_3
.LBB10_6:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB10_1 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	-88(%rsp), %eax         # 4-byte Folded Reload
	leal	1(%rax), %eax
	movl	%eax, %r8d
	movq	%rdx, %rdi
	jl	.LBB10_1
# BB#7:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	OneComponentChromaPrediction4x4_regenerate, .Lfunc_end10-OneComponentChromaPrediction4x4_regenerate
	.cfi_endproc

	.globl	SetModesAndRefframe
	.p2align	4, 0x90
	.type	SetModesAndRefframe,@function
SetModesAndRefframe:                    # @SetModesAndRefframe
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 32
.Lcfi108:
	.cfi_offset %rbx, -32
.Lcfi109:
	.cfi_offset %r14, -24
.Lcfi110:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	img(%rip), %rax
	movq	14224(%rax), %r14
	movslq	12(%rax), %rbp
	leal	(%rdi,%rdi), %r11d
	movslq	%edi, %r10
	movl	%edi, %ebx
	andl	$-2, %ebx
	andl	$2, %r11d
	movw	$-1, (%r9)
	movw	$-1, (%r8)
	movl	$-1, (%rcx)
	movl	$-1, (%rdx)
	imulq	$536, %rbp, %rdi        # imm = 0x218
	addq	%r14, %rdi
	movl	392(%rdi,%r10,4), %ebp
	movw	%bp, (%rsi)
	cmpl	$1, 20(%rax)
	jne	.LBB11_1
# BB#2:
	cmpl	$1, %ebp
	je	.LBB11_6
# BB#3:
	testl	%ebp, %ebp
	je	.LBB11_1
# BB#4:
	cmpl	$-1, %ebp
	jne	.LBB11_7
# BB#5:
	movw	$-1, (%r8)
	movw	$-1, (%r9)
	movl	$0, (%rdx)
	xorl	%eax, %eax
	jmp	.LBB11_9
.LBB11_1:
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %rsi
	movq	(%rsi), %rsi
	movslq	172(%rax), %rbp
	movslq	%ebx, %rbx
	addq	%rbp, %rbx
	movq	(%rsi,%rbx,8), %rsi
	movslq	168(%rax), %rax
	movslq	%r11d, %rbp
	addq	%rax, %rbp
	movsbl	(%rsi,%rbp), %eax
	movw	%ax, (%r8)
	movw	$0, (%r9)
	movl	376(%rdi,%r10,4), %eax
	movl	%eax, (%rdx)
	xorl	%eax, %eax
	jmp	.LBB11_9
.LBB11_6:
	movw	$0, (%r8)
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %rsi
	movq	8(%rsi), %rsi
	movslq	172(%rax), %rbp
	movslq	%ebx, %rbx
	addq	%rbp, %rbx
	movq	(%rsi,%rbx,8), %rsi
	movslq	168(%rax), %rax
	movslq	%r11d, %rbp
	addq	%rax, %rbp
	movsbl	(%rsi,%rbp), %eax
	movw	%ax, (%r9)
	movl	$0, (%rdx)
	jmp	.LBB11_8
.LBB11_7:
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %rsi
	movq	(%rsi), %r14
	movslq	172(%rax), %rbp
	movslq	%ebx, %rbx
	addq	%rbp, %rbx
	movq	(%r14,%rbx,8), %r14
	movslq	168(%rax), %rax
	movslq	%r11d, %rbp
	addq	%rax, %rbp
	movsbl	(%r14,%rbp), %eax
	movw	%ax, (%r8)
	movq	8(%rsi), %rax
	movq	(%rax,%rbx,8), %rax
	movsbl	(%rax,%rbp), %eax
	movw	%ax, (%r9)
	movl	376(%rdi,%r10,4), %eax
	movl	%eax, (%rdx)
.LBB11_8:
	movl	376(%rdi,%r10,4), %eax
.LBB11_9:
	movl	%eax, (%rcx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end11:
	.size	SetModesAndRefframe, .Lfunc_end11-SetModesAndRefframe
	.cfi_endproc

	.globl	LumaResidualCoding
	.p2align	4, 0x90
	.type	LumaResidualCoding,@function
LumaResidualCoding:                     # @LumaResidualCoding
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi112:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi114:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi115:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi117:
	.cfi_def_cfa_offset 80
.Lcfi118:
	.cfi_offset %rbx, -56
.Lcfi119:
	.cfi_offset %r12, -48
.Lcfi120:
	.cfi_offset %r13, -40
.Lcfi121:
	.cfi_offset %r14, -32
.Lcfi122:
	.cfi_offset %r15, -24
.Lcfi123:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movq	14224(%rax), %rcx
	movslq	12(%rax), %rax
	imulq	$536, %rax, %rax        # imm = 0x218
	leaq	364(%rcx,%rax), %r14
	movl	$0, 364(%rcx,%rax)
	leaq	368(%rcx,%rax), %r12
	movq	$0, 368(%rcx,%rax)
	leaq	14(%rsp), %r15
	leaq	20(%rsp), %r13
	leaq	16(%rsp), %rcx
	leaq	12(%rsp), %r8
	leaq	10(%rsp), %r9
	movl	$0, %edi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	SetModesAndRefframe
	movl	20(%rsp), %r8d
	movl	16(%rsp), %r9d
	movswl	14(%rsp), %ecx
	movswl	12(%rsp), %eax
	movswl	10(%rsp), %ebp
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	pushq	%rbp
.Lcfi124:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi125:
	.cfi_adjust_cfa_offset 8
	callq	LumaResidualCoding8x8
	addq	$16, %rsp
.Lcfi126:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebx
	movl	$1, %edi
	movq	%r15, %rsi
	movq	%r13, %rdx
	leaq	16(%rsp), %rcx
	movq	%rcx, %r15
	leaq	12(%rsp), %r8
	leaq	10(%rsp), %r9
	callq	SetModesAndRefframe
	movl	20(%rsp), %r8d
	movl	16(%rsp), %r9d
	movswl	14(%rsp), %ecx
	movswl	12(%rsp), %eax
	movswl	10(%rsp), %ebp
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	pushq	%rbp
.Lcfi127:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi128:
	.cfi_adjust_cfa_offset 8
	callq	LumaResidualCoding8x8
	addq	$16, %rsp
.Lcfi129:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movl	$2, %edi
	leaq	14(%rsp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rcx
	leaq	12(%rsp), %r8
	leaq	10(%rsp), %r9
	callq	SetModesAndRefframe
	movl	20(%rsp), %r8d
	movl	16(%rsp), %r9d
	movswl	14(%rsp), %ecx
	movswl	12(%rsp), %eax
	movswl	10(%rsp), %ebx
	movl	$2, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	pushq	%rbx
.Lcfi130:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi131:
	.cfi_adjust_cfa_offset 8
	callq	LumaResidualCoding8x8
	addq	$16, %rsp
.Lcfi132:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %r15d
	addl	%ebp, %r15d
	movl	$3, %edi
	leaq	14(%rsp), %rsi
	movq	%r13, %rdx
	leaq	16(%rsp), %rcx
	leaq	12(%rsp), %r8
	leaq	10(%rsp), %r9
	callq	SetModesAndRefframe
	movl	20(%rsp), %r8d
	movl	16(%rsp), %r9d
	movswl	14(%rsp), %ecx
	movswl	12(%rsp), %eax
	movswl	10(%rsp), %ebp
	movl	$3, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	pushq	%rbp
.Lcfi133:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi134:
	.cfi_adjust_cfa_offset 8
	callq	LumaResidualCoding8x8
	addq	$16, %rsp
.Lcfi135:
	.cfi_adjust_cfa_offset -16
	addl	%r15d, %eax
	cmpl	$5, %eax
	jg	.LBB12_10
# BB#1:
	movq	img(%rip), %rax
	cmpl	$0, 44(%rax)
	je	.LBB12_2
.LBB12_3:
	cmpl	$3, 20(%rax)
	jne	.LBB12_6
# BB#4:
	cmpl	$1, si_frame_indicator(%rip)
	je	.LBB12_10
# BB#5:
	cmpl	$1, sp2_frame_indicator(%rip)
	je	.LBB12_10
.LBB12_6:
	andl	$16777200, (%r14)       # imm = 0xFFFFF0
	andq	$16711680, (%r12)       # imm = 0xFF0000
	xorl	%ecx, %ecx
	movl	$12656, %edx            # imm = 0x3170
	.p2align	4, 0x90
.LBB12_7:                               # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	enc_picture(%rip), %rsi
	movq	6440(%rsi), %rsi
	movl	180(%rax), %edi
	addl	%ecx, %edi
	movslq	%edi, %rdi
	movq	(%rsi,%rdi,8), %rsi
	movslq	176(%rax), %rdi
	movups	-32(%rax,%rdx), %xmm0
	movups	-16(%rax,%rdx), %xmm1
	movups	%xmm1, 16(%rsi,%rdi,2)
	movups	%xmm0, (%rsi,%rdi,2)
	movq	img(%rip), %rax
	movq	enc_picture(%rip), %rsi
	movq	6440(%rsi), %rsi
	movl	180(%rax), %edi
	leal	1(%rcx,%rdi), %edi
	movslq	%edi, %rdi
	movq	(%rsi,%rdi,8), %rsi
	movslq	176(%rax), %rdi
	movups	(%rax,%rdx), %xmm0
	movups	16(%rax,%rdx), %xmm1
	movups	%xmm1, 16(%rsi,%rdi,2)
	movups	%xmm0, (%rsi,%rdi,2)
	addq	$2, %rcx
	movq	img(%rip), %rax
	addq	$64, %rdx
	cmpq	$16, %rcx
	jne	.LBB12_7
# BB#8:
	cmpl	$3, 20(%rax)
	jne	.LBB12_10
# BB#9:                                 # %.loopexit.loopexit57
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	copyblock_sp
	xorl	%edi, %edi
	movl	$4, %esi
	callq	copyblock_sp
	movl	$4, %edi
	xorl	%esi, %esi
	callq	copyblock_sp
	movl	$4, %edi
	movl	$4, %esi
	callq	copyblock_sp
	movl	$8, %edi
	xorl	%esi, %esi
	callq	copyblock_sp
	movl	$8, %edi
	movl	$4, %esi
	callq	copyblock_sp
	movl	$12, %edi
	xorl	%esi, %esi
	callq	copyblock_sp
	movl	$12, %edi
	movl	$4, %esi
	callq	copyblock_sp
	xorl	%edi, %edi
	movl	$8, %esi
	callq	copyblock_sp
	xorl	%edi, %edi
	movl	$12, %esi
	callq	copyblock_sp
	movl	$4, %edi
	movl	$8, %esi
	callq	copyblock_sp
	movl	$4, %edi
	movl	$12, %esi
	callq	copyblock_sp
	movl	$8, %edi
	movl	$8, %esi
	callq	copyblock_sp
	movl	$8, %edi
	movl	$12, %esi
	callq	copyblock_sp
	movl	$12, %edi
	movl	$8, %esi
	callq	copyblock_sp
	movl	$12, %edi
	movl	$12, %esi
	callq	copyblock_sp
	jmp	.LBB12_10
.LBB12_2:
	cmpl	$0, 15540(%rax)
	je	.LBB12_3
.LBB12_10:                              # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	LumaResidualCoding, .Lfunc_end12-LumaResidualCoding
	.cfi_endproc

	.globl	TransformDecision
	.p2align	4, 0x90
	.type	TransformDecision,@function
TransformDecision:                      # @TransformDecision
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi136:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi137:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi138:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi139:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi140:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi141:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi142:
	.cfi_def_cfa_offset 208
.Lcfi143:
	.cfi_offset %rbx, -56
.Lcfi144:
	.cfi_offset %r12, -48
.Lcfi145:
	.cfi_offset %r13, -40
.Lcfi146:
	.cfi_offset %r14, -32
.Lcfi147:
	.cfi_offset %r15, -24
.Lcfi148:
	.cfi_offset %rbp, -16
	movq	%rsi, 120(%rsp)         # 8-byte Spill
	movl	%edi, %r15d
	leal	1(%r15), %eax
	xorl	%edx, %edx
	cmpl	$-1, %r15d
	cmovel	%edx, %r15d
	movl	$4, %ecx
	cmovnel	%eax, %ecx
	movl	%ecx, 60(%rsp)          # 4-byte Spill
	cmpl	%ecx, %r15d
	movl	$0, %r14d
	jge	.LBB13_7
# BB#1:                                 # %.lr.ph.preheader
	leal	(,%r15,8), %ecx
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB13_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_3 Depth 2
	movl	%edx, 68(%rsp)          # 4-byte Spill
	leal	(,%r15,4), %ebp
	andl	$-8, %ebp
	movq	%r14, %r13
	movslq	%ebp, %r14
	movq	%r14, %rax
	shlq	$5, %rax
	movl	%ecx, %edx
	shrl	$3, %edx
	andl	$1, %edx
	shlq	$4, %rdx
	orq	%rax, %rdx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	movl	%ecx, %r12d
	andl	$8, %r12d
	movl	%r15d, %edi
	leaq	38(%rsp), %rsi
	leaq	92(%rsp), %rdx
	leaq	88(%rsp), %rcx
	leaq	36(%rsp), %r8
	leaq	34(%rsp), %r9
	callq	SetModesAndRefframe
	movq	%r15, 128(%rsp)         # 8-byte Spill
	movl	%r15d, %eax
	andl	$1, %eax
	movswl	38(%rsp), %edx
	movl	92(%rsp), %ecx
	movl	88(%rsp), %r8d
	movzwl	36(%rsp), %esi
	movw	%si, 32(%rsp)           # 2-byte Spill
	movzwl	34(%rsp), %esi
	movw	%si, 30(%rsp)           # 2-byte Spill
	movq	%r12, 144(%rsp)         # 8-byte Spill
	leal	4(%r12), %esi
	movl	%esi, 72(%rsp)          # 4-byte Spill
	leal	4(,%rax,8), %eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
	leaq	-4(%r14), %rsi
	leal	4(%r14), %eax
	movq	%r13, %r14
	cltq
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movabsq	$34359738368, %r15      # imm = 0x800000000
	xorl	%r13d, %r13d
	movl	%edx, 20(%rsp)          # 4-byte Spill
	movl	%r8d, 44(%rsp)          # 4-byte Spill
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB13_3:                               # %.preheader
                                        #   Parent Loop BB13_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movl	%r14d, %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movq	img(%rip), %rax
	movl	196(%rax), %esi
	addl	%ebp, %esi
	movslq	%esi, %r12
	movslq	192(%rax), %rax
	movq	144(%rsp), %rbx         # 8-byte Reload
	movslq	%ebx, %r14
	addq	%rax, %r14
	movswl	30(%rsp), %eax          # 2-byte Folded Reload
	movl	%eax, 80(%rsp)          # 4-byte Spill
	movl	%eax, (%rsp)
	movswl	32(%rsp), %r9d          # 2-byte Folded Reload
	movl	%ebp, %esi
	movl	%ebx, %edi
	movl	%esi, 24(%rsp)          # 4-byte Spill
	movl	%r9d, 84(%rsp)          # 4-byte Spill
	callq	LumaPrediction4x4
	movq	imgY_org(%rip), %rcx
	movq	(%rcx,%r12,8), %rax
	movq	(%rax,%r14,2), %xmm0    # xmm0 = mem[0],zero
	pxor	%xmm2, %xmm2
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	img(%rip), %rax
	addq	%r13, %rax
	movq	96(%rsp), %rbp          # 8-byte Reload
	movq	12624(%rbp,%rax), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	leaq	diff64(%r13), %rdi
	movdqa	%xmm0, diff64(%r13)
	movq	8(%rcx,%r12,8), %rdx
	movq	(%rdx,%r14,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	12656(%rbp,%rax), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+16(%r13)
	movq	16(%rcx,%r12,8), %rdx
	movq	%r15, 104(%rsp)         # 8-byte Spill
	sarq	$30, %r15
	movq	(%rdx,%r14,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	12688(%rbp,%rax), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqu	%xmm0, diff64(%r15)
	movq	24(%rcx,%r12,8), %rcx
	orq	$16, %r15
	movq	(%rcx,%r14,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	12720(%rbp,%rax), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqu	%xmm0, diff64(%r15)
	callq	distortion4x4
	movl	%eax, %r14d
	addl	48(%rsp), %r14d         # 4-byte Folded Reload
	cmpl	76(%rsp), %ebx          # 4-byte Folded Reload
	jae	.LBB13_4
# BB#11:                                # %.preheader.1
                                        #   in Loop: Header=BB13_3 Depth=2
	movq	img(%rip), %rax
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movq	96(%rsp), %r14          # 8-byte Reload
	movslq	192(%rax), %rbx
	movslq	72(%rsp), %rdi          # 4-byte Folded Reload
	addq	%rdi, %rbx
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	24(%rsp), %esi          # 4-byte Reload
	movl	20(%rsp), %edx          # 4-byte Reload
	movl	40(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %ecx
	movl	44(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %r8d
	movl	84(%rsp), %r9d          # 4-byte Reload
	callq	LumaPrediction4x4
	movq	imgY_org(%rip), %rax
	movq	(%rax,%r12,8), %rcx
	movq	(%rcx,%rbx,2), %xmm0    # xmm0 = mem[0],zero
	pxor	%xmm2, %xmm2
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	img(%rip), %rcx
	addq	%r13, %rcx
	movq	12632(%r14,%rcx), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	leaq	diff64+64(%r13), %rdi
	movdqa	%xmm0, diff64+64(%r13)
	movq	8(%rax,%r12,8), %rdx
	movq	(%rdx,%rbx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	12664(%r14,%rcx), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+80(%r13)
	movq	16(%rax,%r12,8), %rdx
	movq	(%rdx,%rbx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	12696(%r14,%rcx), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqu	%xmm0, diff64+96(%r13)
	movq	24(%rax,%r12,8), %rax
	movq	(%rax,%rbx,2), %xmm0    # xmm0 = mem[0],zero
	movq	%r14, %rax
	movq	48(%rsp), %r14          # 8-byte Reload
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	12728(%rax,%rcx), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff64+112(%r13)
	callq	distortion4x4
	addl	%eax, %r14d
	movl	20(%rsp), %edx          # 4-byte Reload
	movl	%r15d, %ecx
	movl	%ebp, %r8d
	jmp	.LBB13_5
	.p2align	4, 0x90
.LBB13_4:                               #   in Loop: Header=BB13_3 Depth=2
	movl	20(%rsp), %edx          # 4-byte Reload
	movl	40(%rsp), %ecx          # 4-byte Reload
	movl	44(%rsp), %r8d          # 4-byte Reload
.LBB13_5:                               #   in Loop: Header=BB13_3 Depth=2
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	104(%rsp), %r15         # 8-byte Reload
	movl	24(%rsp), %ebp          # 4-byte Reload
	subq	$-128, %r13
	movabsq	$137438953472, %rax     # imm = 0x2000000000
	addq	%rax, %r15
	addq	$4, %rsi
	addl	$4, %ebp
	cmpq	136(%rsp), %rsi         # 8-byte Folded Reload
	jl	.LBB13_3
# BB#6:                                 #   in Loop: Header=BB13_2 Depth=1
	movl	$diff64, %edi
	callq	distortion8x8
	movl	68(%rsp), %edx          # 4-byte Reload
	addl	%eax, %edx
	movq	128(%rsp), %r15         # 8-byte Reload
	incl	%r15d
	movl	64(%rsp), %ecx          # 4-byte Reload
	addl	$8, %ecx
	cmpl	60(%rsp), %r15d         # 4-byte Folded Reload
	jne	.LBB13_2
.LBB13_7:                               # %._crit_edge
	subl	%edx, %r14d
	movl	$1, %eax
	jg	.LBB13_10
# BB#8:                                 # %._crit_edge
	movq	input(%rip), %rcx
	cmpl	$2, 5100(%rcx)
	je	.LBB13_10
# BB#9:
	movq	120(%rsp), %rax         # 8-byte Reload
	addl	%r14d, (%rax)
	xorl	%eax, %eax
.LBB13_10:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	TransformDecision, .Lfunc_end13-TransformDecision
	.cfi_endproc

	.globl	IntraChromaPrediction4x4
	.p2align	4, 0x90
	.type	IntraChromaPrediction4x4,@function
IntraChromaPrediction4x4:               # @IntraChromaPrediction4x4
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movq	14224(%rax), %r8
	movslq	12(%rax), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	movslq	%esi, %rsi
	movslq	%edi, %rdi
	movslq	416(%r8,%rcx), %rcx
	movslq	%edx, %rdx
	shlq	$5, %rdx
	leaq	12624(%rax,%rdx), %r8
	shlq	$11, %rdi
	addq	%rax, %rdi
	shlq	$9, %rcx
	addq	%rdi, %rcx
	leaq	8528(%rdx,%rcx), %rax
	movq	(%rax,%rsi,2), %rcx
	movq	%rcx, (%r8,%rsi,2)
	movq	32(%rax,%rsi,2), %rcx
	movq	%rcx, 32(%r8,%rsi,2)
	movq	64(%rax,%rsi,2), %rcx
	movq	%rcx, 64(%r8,%rsi,2)
	movq	96(%rax,%rsi,2), %rax
	movq	%rax, 96(%r8,%rsi,2)
	retq
.Lfunc_end14:
	.size	IntraChromaPrediction4x4, .Lfunc_end14-IntraChromaPrediction4x4
	.cfi_endproc

	.globl	ChromaPrediction4x4
	.p2align	4, 0x90
	.type	ChromaPrediction4x4,@function
ChromaPrediction4x4:                    # @ChromaPrediction4x4
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi150:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi151:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi152:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi153:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi154:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi155:
	.cfi_def_cfa_offset 80
.Lcfi156:
	.cfi_offset %rbx, -56
.Lcfi157:
	.cfi_offset %r12, -48
.Lcfi158:
	.cfi_offset %r13, -40
.Lcfi159:
	.cfi_offset %r14, -32
.Lcfi160:
	.cfi_offset %r15, -24
.Lcfi161:
	.cfi_offset %rbp, -16
	movl	%r9d, %r11d
	movl	%r8d, %r10d
	movl	%ecx, %r14d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rsi, (%rsp)            # 8-byte Spill
	movl	%edi, %r15d
	movzwl	80(%rsp), %ebx
	movq	img(%rip), %rbp
	movq	14224(%rbp), %r8
	movq	14384(%rbp), %rcx
	movslq	12(%rbp), %rsi
	movq	active_pps(%rip), %rdi
	cmpl	$0, 192(%rdi)
	je	.LBB15_3
# BB#1:
	movl	20(%rbp), %eax
	movb	$1, %r13b
	testl	%eax, %eax
	je	.LBB15_6
# BB#2:
	cmpl	$3, %eax
	je	.LBB15_6
.LBB15_3:
	cmpl	$0, 196(%rdi)
	je	.LBB15_5
# BB#4:
	cmpl	$1, 20(%rbp)
	sete	%r13b
	jmp	.LBB15_6
.LBB15_5:
	xorl	%r13d, %r13d
.LBB15_6:
	movzwl	88(%rsp), %r12d
	imulq	$536, %rsi, %rsi        # imm = 0x218
	testw	%bx, %bx
	jne	.LBB15_13
# BB#7:
	movzwl	480(%r8,%rsi), %edi
	testw	%di, %di
	je	.LBB15_13
# BB#8:
	cmpl	$1, %r11d
	jne	.LBB15_13
# BB#9:
	cmpl	$1, %r10d
	jne	.LBB15_13
# BB#10:
	cmpl	$2, %r14d
	jne	.LBB15_13
# BB#11:
	testw	%r12w, %r12w
	jne	.LBB15_13
# BB#12:                                # %.thread184
	movzwl	%di, %eax
	leaq	14400(%rbp), %rsi
	addq	$14392, %rbp            # imm = 0x3838
	cmpl	$1, %eax
	cmovneq	%rsi, %rbp
	movq	(%rbp), %rcx
	movb	$1, 16(%rsp)            # 1-byte Folded Spill
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	jmp	.LBB15_15
.LBB15_13:
	cmpl	$-1, %r14d
	je	.LBB15_31
# BB#14:
	testl	%r14d, %r14d
	sete	%al
	movl	%eax, 12(%rsp)          # 4-byte Spill
	cmpl	$2, %r14d
	sete	16(%rsp)                # 1-byte Folded Spill
	movl	%r14d, %eax
	orl	$2, %eax
	cmpl	$2, %eax
	jne	.LBB15_16
.LBB15_15:
	movswl	80(%rsp), %r9d
	movl	$ChromaPrediction4x4.l0_pred, %edi
	movl	$0, %r8d
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	pushq	%r15
.Lcfi162:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi163:
	.cfi_adjust_cfa_offset 8
	movq	%r15, %r12
	movq	%rdx, %r15
	movq	%rcx, %rbx
	movl	%r13d, %ebp
	movq	%r11, %r13
	callq	*OneComponentChromaPrediction4x4(%rip)
	movq	%r13, %r11
	movl	%ebp, %r13d
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %r15
	movzwl	104(%rsp), %r12d
	movzwl	96(%rsp), %ebx
	addq	$16, %rsp
.Lcfi164:
	.cfi_adjust_cfa_offset -16
.LBB15_16:
	decl	%r14d
	cmpl	$1, %r14d
	ja	.LBB15_18
# BB#17:
	movswl	%r12w, %r9d
	movl	$ChromaPrediction4x4.l1_pred, %edi
	movl	$1, %r8d
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	pushq	%r15
.Lcfi165:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi166:
	.cfi_adjust_cfa_offset 8
	movq	%rdx, %rbp
	callq	*OneComponentChromaPrediction4x4(%rip)
	movq	%rbp, %rdx
	addq	$16, %rsp
.Lcfi167:
	.cfi_adjust_cfa_offset -16
.LBB15_18:
	movq	(%rsp), %rax            # 8-byte Reload
	leal	4(%rax), %r8d
	leal	4(%rdx), %r14d
	testb	%r13b, %r13b
	je	.LBB15_25
# BB#19:
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB15_32
# BB#20:
	movq	wbp_weight(%rip), %rax
	movswq	%bx, %rcx
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	(%rsi,%rcx,8), %rdi
	movswq	%r12w, %rsi
	movq	(%rdi,%rsi,8), %rbp
	movslq	%r15d, %rdi
	movq	%rdx, %rbx
	movl	4(%rbp,%rdi,4), %r15d
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%rsi,8), %rax
	movl	4(%rax,%rdi,4), %ebp
	movq	wp_offset(%rip), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	(%rdx,%rcx,8), %rcx
	movl	4(%rcx,%rdi,4), %ecx
	movq	(%rax,%rsi,8), %rax
	movl	4(%rax,%rdi,4), %eax
	leal	1(%rcx,%rax), %r12d
	sarl	%r12d
	movl	wp_chroma_round(%rip), %r13d
	addl	%r13d, %r13d
	movl	luma_log_weight_denom(%rip), %ecx
	incl	%ecx
	movq	img(%rip), %r9
	movslq	(%rsp), %rdx            # 4-byte Folded Reload
	movslq	%r8d, %r11
	movslq	%ebx, %rbx
	movslq	%r14d, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rbx, %rax
	shlq	$5, %rax
	leaq	(%rax,%rdx,2), %rax
	incq	%rdx
	leaq	12630(%r9,%rax), %rax
	movl	$ChromaPrediction4x4.l1_pred, %r14d
	movl	$ChromaPrediction4x4.l0_pred, %esi
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB15_21:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	15524(%r9), %edi
	movzwl	(%rsi), %r8d
	imull	%r15d, %r8d
	movzwl	(%r14), %r10d
	imull	%ebp, %r10d
	addl	%r13d, %r8d
	addl	%r10d, %r8d
	sarl	%cl, %r8d
	addl	%r12d, %r8d
	movl	%ebp, %r10d
	movl	$0, %ebp
	cmovsl	%ebp, %r8d
	cmpl	%edi, %r8d
	cmovlw	%r8w, %di
	cmpq	%r11, %rdx
	movw	%di, -6(%rax)
	jge	.LBB15_23
# BB#22:                                #   in Loop: Header=BB15_21 Depth=1
	movl	15524(%r9), %edi
	xorl	%r8d, %r8d
	movzwl	2(%rsi), %ebp
	imull	%r15d, %ebp
	movzwl	2(%r14), %edx
	imull	%r10d, %edx
	addl	%r13d, %ebp
	addl	%edx, %ebp
	sarl	%cl, %ebp
	addl	%r12d, %ebp
	cmovsl	%r8d, %ebp
	cmpl	%edi, %ebp
	cmovlw	%bp, %di
	movw	%di, -4(%rax)
	movl	15524(%r9), %edx
	movzwl	4(%rsi), %edi
	imull	%r15d, %edi
	movzwl	4(%r14), %ebp
	imull	%r10d, %ebp
	addl	%r13d, %edi
	addl	%ebp, %edi
	sarl	%cl, %edi
	addl	%r12d, %edi
	cmovsl	%r8d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, -2(%rax)
	movl	15524(%r9), %edx
	movzwl	6(%rsi), %edi
	addq	$8, %rsi
	imull	%r15d, %edi
	movzwl	6(%r14), %ebp
	addq	$8, %r14
	imull	%r10d, %ebp
	addl	%r13d, %edi
	addl	%ebp, %edi
	sarl	%cl, %edi
	addl	%r12d, %edi
	cmovsl	%r8d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, (%rax)
	movq	16(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB15_24
	.p2align	4, 0x90
.LBB15_23:                              #   in Loop: Header=BB15_21 Depth=1
	addq	$2, %rsi
	addq	$2, %r14
.LBB15_24:                              #   in Loop: Header=BB15_21 Depth=1
	movl	%r10d, %ebp
	incq	%rbx
	addq	$32, %rax
	cmpq	(%rsp), %rbx            # 8-byte Folded Reload
	jl	.LBB15_21
	jmp	.LBB15_49
.LBB15_25:
	movq	img(%rip), %rdi
	movslq	(%rsp), %r9             # 4-byte Folded Reload
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB15_38
# BB#26:                                # %.preheader191.preheader
	movslq	%r8d, %r8
	movslq	%edx, %rax
	movslq	%r14d, %rsi
	movq	%rax, %rdx
	shlq	$5, %rdx
	leaq	(%rdx,%r9,2), %rdx
	incq	%r9
	leaq	12630(%rdi,%rdx), %rdi
	movl	$ChromaPrediction4x4.l1_pred, %ebp
	movl	$ChromaPrediction4x4.l0_pred, %ebx
	.p2align	4, 0x90
.LBB15_27:                              # %.preheader191
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx), %edx
	movzwl	(%rbp), %ecx
	leal	1(%rdx,%rcx), %ecx
	shrl	%ecx
	cmpq	%r8, %r9
	movw	%cx, -6(%rdi)
	jge	.LBB15_29
# BB#28:                                #   in Loop: Header=BB15_27 Depth=1
	movzwl	2(%rbx), %ecx
	movzwl	2(%rbp), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, -4(%rdi)
	movzwl	4(%rbx), %ecx
	movzwl	4(%rbp), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, -2(%rdi)
	movzwl	6(%rbx), %ecx
	addq	$8, %rbx
	movzwl	6(%rbp), %edx
	addq	$8, %rbp
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, (%rdi)
	jmp	.LBB15_30
	.p2align	4, 0x90
.LBB15_29:                              #   in Loop: Header=BB15_27 Depth=1
	addq	$2, %rbx
	addq	$2, %rbp
.LBB15_30:                              #   in Loop: Header=BB15_27 Depth=1
	incq	%rax
	addq	$32, %rdi
	cmpq	%rsi, %rax
	jl	.LBB15_27
	jmp	.LBB15_49
.LBB15_31:
	movslq	(%rsp), %rax            # 4-byte Folded Reload
	movslq	%r15d, %rdi
	movslq	416(%r8,%rsi), %rbx
	movslq	%edx, %rsi
	shlq	$5, %rsi
	leaq	12624(%rbp,%rsi), %r8
	shlq	$11, %rdi
	addq	%rbp, %rdi
	shlq	$9, %rbx
	addq	%rdi, %rbx
	leaq	8528(%rsi,%rbx), %rcx
	movq	(%rcx,%rax,2), %rdx
	movq	%rdx, (%r8,%rax,2)
	movq	32(%rcx,%rax,2), %rdx
	movq	%rdx, 32(%r8,%rax,2)
	movq	64(%rcx,%rax,2), %rdx
	movq	%rdx, 64(%r8,%rax,2)
	movq	96(%rcx,%rax,2), %rcx
	movq	%rcx, 96(%r8,%rax,2)
	jmp	.LBB15_49
.LBB15_32:
	movq	%rdx, %rdi
	movq	wp_weight(%rip), %rax
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	je	.LBB15_41
# BB#33:
	movq	(%rax), %rax
	movswq	%bx, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	%r15d, %rdx
	movl	4(%rax,%rdx,4), %r11d
	movq	wp_offset(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax,%rcx,8), %rax
	movl	4(%rax,%rdx,4), %r15d
	movq	img(%rip), %rsi
	movl	wp_chroma_round(%rip), %r12d
	movl	chroma_log_weight_denom(%rip), %ecx
	movslq	(%rsp), %r10            # 4-byte Folded Reload
	movslq	%r8d, %r8
	movslq	%edi, %rbx
	movslq	%r14d, %r9
	movq	%rbx, %rax
	shlq	$5, %rax
	leaq	(%rax,%r10,2), %rax
	incq	%r10
	leaq	12630(%rsi,%rax), %rbp
	movl	$ChromaPrediction4x4.l0_pred, %eax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB15_34:                              # %.preheader187
                                        # =>This Inner Loop Header: Depth=1
	movl	15524(%rsi), %edx
	movzwl	(%rax), %edi
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	cmpq	%r8, %r10
	movw	%dx, -6(%rbp)
	jge	.LBB15_36
# BB#35:                                #   in Loop: Header=BB15_34 Depth=1
	movl	15524(%rsi), %edx
	movzwl	2(%rax), %edi
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, -4(%rbp)
	movl	15524(%rsi), %edx
	movzwl	4(%rax), %edi
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, -2(%rbp)
	movl	15524(%rsi), %edx
	movzwl	6(%rax), %edi
	addq	$8, %rax
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, (%rbp)
	jmp	.LBB15_37
	.p2align	4, 0x90
.LBB15_36:                              #   in Loop: Header=BB15_34 Depth=1
	addq	$2, %rax
.LBB15_37:                              #   in Loop: Header=BB15_34 Depth=1
	incq	%rbx
	addq	$32, %rbp
	cmpq	%r9, %rbx
	jl	.LBB15_34
	jmp	.LBB15_49
.LBB15_38:
	movslq	%edx, %rcx
	movslq	%r14d, %rsi
	movq	%rcx, %rdx
	shlq	$5, %rdx
	leaq	(%rdi,%rdx), %rax
	leaq	12624(%rax,%r9,2), %rbp
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	je	.LBB15_46
# BB#39:                                # %.preheader194.preheader
	movq	ChromaPrediction4x4.l0_pred(%rip), %rax
	movq	%rax, (%rbp)
	incq	%rcx
	cmpq	%rsi, %rcx
	jge	.LBB15_49
# BB#40:                                # %.preheader194.1
	leaq	12624(%rdi,%rdx), %rax
	shlq	$5, %rcx
	leaq	12624(%rdi,%rcx), %rcx
	movq	ChromaPrediction4x4.l0_pred+8(%rip), %rdx
	movq	%rdx, (%rcx,%r9,2)
	movq	ChromaPrediction4x4.l0_pred+16(%rip), %rcx
	movq	%rcx, 64(%rax,%r9,2)
	movq	ChromaPrediction4x4.l0_pred+24(%rip), %rcx
	jmp	.LBB15_48
.LBB15_41:
	movq	8(%rax), %rax
	movswq	%r12w, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	%r15d, %rdx
	movl	4(%rax,%rdx,4), %r11d
	movq	wp_offset(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rcx,8), %rax
	movl	4(%rax,%rdx,4), %r15d
	movq	img(%rip), %rsi
	movl	wp_chroma_round(%rip), %r12d
	movl	chroma_log_weight_denom(%rip), %ecx
	movslq	(%rsp), %r10            # 4-byte Folded Reload
	movslq	%r8d, %r8
	movslq	%edi, %rbx
	movslq	%r14d, %r9
	movq	%rbx, %rax
	shlq	$5, %rax
	leaq	(%rax,%r10,2), %rax
	incq	%r10
	leaq	12630(%rsi,%rax), %rbp
	movl	$ChromaPrediction4x4.l1_pred, %eax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB15_42:                              # %.preheader189
                                        # =>This Inner Loop Header: Depth=1
	movl	15524(%rsi), %edx
	movzwl	(%rax), %edi
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	cmpq	%r8, %r10
	movw	%dx, -6(%rbp)
	jge	.LBB15_44
# BB#43:                                #   in Loop: Header=BB15_42 Depth=1
	movl	15524(%rsi), %edx
	movzwl	2(%rax), %edi
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, -4(%rbp)
	movl	15524(%rsi), %edx
	movzwl	4(%rax), %edi
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, -2(%rbp)
	movl	15524(%rsi), %edx
	movzwl	6(%rax), %edi
	addq	$8, %rax
	imull	%r11d, %edi
	addl	%r12d, %edi
	sarl	%cl, %edi
	addl	%r15d, %edi
	cmovsl	%r14d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, (%rbp)
	jmp	.LBB15_45
	.p2align	4, 0x90
.LBB15_44:                              #   in Loop: Header=BB15_42 Depth=1
	addq	$2, %rax
.LBB15_45:                              #   in Loop: Header=BB15_42 Depth=1
	incq	%rbx
	addq	$32, %rbp
	cmpq	%r9, %rbx
	jl	.LBB15_42
	jmp	.LBB15_49
.LBB15_46:                              # %.preheader196.preheader
	movq	ChromaPrediction4x4.l1_pred(%rip), %rax
	movq	%rax, (%rbp)
	incq	%rcx
	cmpq	%rsi, %rcx
	jge	.LBB15_49
# BB#47:                                # %.preheader196.1
	leaq	12624(%rdi,%rdx), %rax
	shlq	$5, %rcx
	leaq	12624(%rdi,%rcx), %rcx
	movq	ChromaPrediction4x4.l1_pred+8(%rip), %rdx
	movq	%rdx, (%rcx,%r9,2)
	movq	ChromaPrediction4x4.l1_pred+16(%rip), %rcx
	movq	%rcx, 64(%rax,%r9,2)
	movq	ChromaPrediction4x4.l1_pred+24(%rip), %rcx
.LBB15_48:                              # %.loopexit
	movq	%rcx, 96(%rax,%r9,2)
.LBB15_49:                              # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	ChromaPrediction4x4, .Lfunc_end15-ChromaPrediction4x4
	.cfi_endproc

	.globl	ChromaResidualCoding
	.p2align	4, 0x90
	.type	ChromaResidualCoding,@function
ChromaResidualCoding:                   # @ChromaResidualCoding
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi168:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi169:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi170:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi171:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi172:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi173:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi174:
	.cfi_def_cfa_offset 112
.Lcfi175:
	.cfi_offset %rbx, -56
.Lcfi176:
	.cfi_offset %r12, -48
.Lcfi177:
	.cfi_offset %r13, -40
.Lcfi178:
	.cfi_offset %r14, -32
.Lcfi179:
	.cfi_offset %r15, -24
.Lcfi180:
	.cfi_offset %rbp, -16
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	img(%rip), %rsi
	movq	14224(%rsi), %rax
	movslq	12(%rsi), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	cmpl	$0, 72(%rax,%rcx)
	je	.LBB16_2
# BB#1:
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	jmp	.LBB16_3
.LBB16_2:
	movl	20(%rsi), %eax
	testl	%eax, %eax
	sete	%cl
	cmpl	$3, %eax
	sete	%al
	orb	%cl, %al
	movl	%eax, 4(%rsp)           # 4-byte Spill
.LBB16_3:
	movslq	15536(%rsi), %rdx
	decq	%rdx
	movq	input(%rip), %rax
	cmpl	$0, 5772(%rax)
	movl	$OneComponentChromaPrediction4x4_retrieve, %eax
	movl	$OneComponentChromaPrediction4x4_regenerate, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, OneComponentChromaPrediction4x4(%rip)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax)
	xorl	%r14d, %r14d
	shlq	$6, %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	leaq	2(%rsp), %rbx
	jmp	.LBB16_4
	.p2align	4, 0x90
.LBB16_39:                              # %.thread77..preheader85_crit_edge
                                        #   in Loop: Header=BB16_4 Depth=1
	movq	img(%rip), %rsi
.LBB16_4:                               # %.preheader85
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_6 Depth 2
                                        #       Child Loop BB16_8 Depth 3
                                        #     Child Loop BB16_13 Depth 2
                                        #     Child Loop BB16_28 Depth 2
                                        #       Child Loop BB16_30 Depth 3
                                        #     Child Loop BB16_26 Depth 2
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movl	15548(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.LBB16_10
# BB#5:                                 # %.preheader79.preheader
                                        #   in Loop: Header=BB16_4 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB16_6:                               # %.preheader79
                                        #   Parent Loop BB16_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB16_8 Depth 3
	movl	%eax, 8(%rsp)           # 4-byte Spill
	cmpl	$0, 15544(%rsi)
	leaq	36(%rsp), %r14
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	32(%rsp), %rbp
	leaq	14(%rsp), %r15
	leaq	12(%rsp), %r13
	jle	.LBB16_9
# BB#7:                                 # %.lr.ph
                                        #   in Loop: Header=BB16_6 Depth=2
	movl	8(%rsp), %eax           # 4-byte Reload
	sarl	$2, %eax
	cltq
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB16_8:                               #   Parent Loop BB16_4 Depth=1
                                        #     Parent Loop BB16_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r12d, %eax
	sarl	$2, %eax
	cltq
	movq	48(%rsp), %rsi          # 8-byte Reload
	shlq	$4, %rsi
	addq	%rdx, %rsi
	movl	ChromaResidualCoding.block8x8_idx(%rsi,%rax,4), %edi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%rbp, %rcx
	movq	%r15, %r8
	movq	%r13, %r9
	callq	SetModesAndRefframe
	movswl	2(%rsp), %ecx
	movl	36(%rsp), %r8d
	movl	32(%rsp), %r9d
	movswl	14(%rsp), %eax
	movswl	12(%rsp), %ebx
	movq	24(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%r12d, %esi
	movl	8(%rsp), %edx           # 4-byte Reload
	pushq	%rbx
	leaq	10(%rsp), %rbx
.Lcfi181:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi182:
	.cfi_adjust_cfa_offset 8
	callq	ChromaPrediction4x4
	movq	56(%rsp), %rdx          # 8-byte Reload
	addq	$16, %rsp
.Lcfi183:
	.cfi_adjust_cfa_offset -16
	addl	$4, %r12d
	movq	img(%rip), %rsi
	cmpl	15544(%rsi), %r12d
	jl	.LBB16_8
.LBB16_9:                               # %._crit_edge
                                        #   in Loop: Header=BB16_6 Depth=2
	movl	8(%rsp), %eax           # 4-byte Reload
	addl	$4, %eax
	movl	15548(%rsi), %ecx
	cmpl	%ecx, %eax
	jl	.LBB16_6
.LBB16_10:                              # %._crit_edge88
                                        #   in Loop: Header=BB16_4 Depth=1
	cmpl	$0, 15256(%rsi)
	je	.LBB16_14
# BB#11:                                # %.preheader83
                                        #   in Loop: Header=BB16_4 Depth=1
	testl	%ecx, %ecx
	movq	24(%rsp), %r14          # 8-byte Reload
	jle	.LBB16_32
# BB#12:                                # %.lr.ph90.preheader
                                        #   in Loop: Header=BB16_4 Depth=1
	movl	$12624, %ebp            # imm = 0x3150
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_13:                              # %.lr.ph90
                                        #   Parent Loop BB16_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	enc_picture(%rip), %rax
	movq	6472(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movl	188(%rsi), %ecx
	addl	%ebx, %ecx
	movslq	%ecx, %rcx
	movslq	184(%rsi), %rdi
	addq	%rdi, %rdi
	addq	(%rax,%rcx,8), %rdi
	movslq	15544(%rsi), %rdx
	addq	%rbp, %rsi
	addq	%rdx, %rdx
	callq	memcpy
	incq	%rbx
	movq	img(%rip), %rsi
	movslq	15548(%rsi), %rax
	addq	$32, %rbp
	cmpq	%rax, %rbx
	jl	.LBB16_13
	jmp	.LBB16_32
	.p2align	4, 0x90
.LBB16_14:                              #   in Loop: Header=BB16_4 Depth=1
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	je	.LBB16_15
# BB#21:                                #   in Loop: Header=BB16_4 Depth=1
	movq	img(%rip), %rax
	cmpl	$3, 20(%rsi)
	jne	.LBB16_24
# BB#22:                                # %.preheader80.preheader
                                        #   in Loop: Header=BB16_4 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, 13152(%rax)
	movups	%xmm0, 13136(%rax)
	movups	%xmm0, 13216(%rax)
	movups	%xmm0, 13200(%rax)
	movups	%xmm0, 13280(%rax)
	movups	%xmm0, 13264(%rax)
	movups	%xmm0, 13344(%rax)
	movups	%xmm0, 13328(%rax)
	movups	%xmm0, 13408(%rax)
	movups	%xmm0, 13392(%rax)
	movups	%xmm0, 13472(%rax)
	movups	%xmm0, 13456(%rax)
	movups	%xmm0, 13536(%rax)
	movups	%xmm0, 13520(%rax)
	movups	%xmm0, 13600(%rax)
	movups	%xmm0, 13584(%rax)
	jmp	.LBB16_23
.LBB16_15:                              # %.critedge.preheader
                                        #   in Loop: Header=BB16_4 Depth=1
	movq	img(%rip), %rax
	testl	%ecx, %ecx
	jle	.LBB16_16
# BB#27:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB16_4 Depth=1
	movq	imgUV_org(%rip), %r8
	movl	15544(%rax), %edi
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movq	24(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB16_28:                              # %.preheader
                                        #   Parent Loop BB16_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB16_30 Depth 3
	testl	%edi, %edi
	jle	.LBB16_31
# BB#29:                                # %.lr.ph92
                                        #   in Loop: Header=BB16_28 Depth=2
	movq	(%r8,%r14,8), %rbp
	movq	%r9, %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB16_30:                              #   Parent Loop BB16_4 Depth=1
                                        #     Parent Loop BB16_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	204(%rax), %rdi
	movslq	%esi, %rbx
	addq	%rdi, %rbx
	movq	(%rbp,%rbx,8), %rdi
	movl	200(%rax), %ebx
	addl	%ecx, %ebx
	movslq	%ebx, %rbx
	movzwl	(%rdi,%rbx,2), %edi
	movzwl	12624(%rax,%rdx,2), %ebx
	subl	%ebx, %edi
	movl	%edi, 13136(%rax,%rdx,4)
	incq	%rcx
	movslq	15544(%rax), %rdi
	incq	%rdx
	cmpq	%rdi, %rcx
	jl	.LBB16_30
.LBB16_31:                              # %.critedge
                                        #   in Loop: Header=BB16_28 Depth=2
	incq	%rsi
	movslq	15548(%rax), %rcx
	addq	$16, %r9
	cmpq	%rcx, %rsi
	jl	.LBB16_28
	jmp	.LBB16_32
.LBB16_24:                              # %.thread.preheader
                                        #   in Loop: Header=BB16_4 Depth=1
	cmpl	$0, 15548(%rax)
	jle	.LBB16_23
# BB#25:                                # %.thread.preheader130
                                        #   in Loop: Header=BB16_4 Depth=1
	movl	$12624, %ebp            # imm = 0x3150
	xorl	%ebx, %ebx
	movq	24(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB16_26:                              # %.thread
                                        #   Parent Loop BB16_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	enc_picture(%rip), %rcx
	movq	6472(%rcx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movl	188(%rax), %edx
	addl	%ebx, %edx
	movslq	%edx, %rdx
	movslq	184(%rax), %rdi
	addq	%rdi, %rdi
	addq	(%rcx,%rdx,8), %rdi
	movslq	15544(%rax), %rdx
	addq	%rbp, %rax
	addq	%rdx, %rdx
	movq	%rax, %rsi
	callq	memcpy
	incq	%rbx
	movq	img(%rip), %rax
	movslq	15548(%rax), %rcx
	addq	$32, %rbp
	cmpq	%rcx, %rbx
	jl	.LBB16_26
	.p2align	4, 0x90
.LBB16_32:                              # %.loopexit
                                        #   in Loop: Header=BB16_4 Depth=1
	movq	img(%rip), %rax
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	leaq	2(%rsp), %rbx
	jne	.LBB16_33
# BB#17:                                # %.loopexit.thread117
                                        #   in Loop: Header=BB16_4 Depth=1
	cmpl	$0, 15256(%rax)
	jne	.LBB16_38
.LBB16_18:                              #   in Loop: Header=BB16_4 Depth=1
	cmpl	$3, 20(%rax)
	jne	.LBB16_20
# BB#19:                                #   in Loop: Header=BB16_4 Depth=1
	movq	14224(%rax), %rcx
	movslq	12(%rax), %rax
	imulq	$536, %rax, %rax        # imm = 0x218
	cmpl	$10, 72(%rcx,%rax)
	jne	.LBB16_34
.LBB16_20:                              #   in Loop: Header=BB16_4 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %esi
	movl	%r14d, %edi
	callq	dct_chroma
	jmp	.LBB16_37
.LBB16_23:                              # %.loopexit.thread
                                        #   in Loop: Header=BB16_4 Depth=1
	movq	img(%rip), %rax
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB16_33:                              #   in Loop: Header=BB16_4 Depth=1
	cmpl	$3, 20(%rax)
	jne	.LBB16_38
.LBB16_34:                              #   in Loop: Header=BB16_4 Depth=1
	movl	sp2_frame_indicator(%rip), %eax
	orl	si_frame_indicator(%rip), %eax
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %esi
	movl	%r14d, %edi
	je	.LBB16_36
# BB#35:                                #   in Loop: Header=BB16_4 Depth=1
	callq	dct_chroma_sp2
	jmp	.LBB16_37
.LBB16_36:                              #   in Loop: Header=BB16_4 Depth=1
	callq	dct_chroma_sp
.LBB16_37:                              # %.sink.split
                                        #   in Loop: Header=BB16_4 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
.LBB16_38:                              # %.thread77
                                        #   in Loop: Header=BB16_4 Depth=1
	incq	%r14
	cmpq	$2, %r14
	jne	.LBB16_39
	jmp	.LBB16_40
.LBB16_16:                              #   in Loop: Header=BB16_4 Depth=1
	movq	24(%rsp), %r14          # 8-byte Reload
	cmpl	$0, 15256(%rax)
	jne	.LBB16_38
	jmp	.LBB16_18
.LBB16_40:
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	shll	$4, %eax
	movq	img(%rip), %rcx
	movq	14224(%rcx), %rdx
	movslq	12(%rcx), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	addl	%eax, 364(%rdx,%rcx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	ChromaResidualCoding, .Lfunc_end16-ChromaResidualCoding
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_0:
	.quad	2                       # 0x2
	.quad	3                       # 0x3
.LCPI17_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI17_2:
	.quad	4                       # 0x4
	.quad	4                       # 0x4
	.text
	.globl	IntraChromaPrediction
	.p2align	4, 0x90
	.type	IntraChromaPrediction,@function
IntraChromaPrediction:                  # @IntraChromaPrediction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi184:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi185:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi186:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi187:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi188:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi189:
	.cfi_def_cfa_offset 56
	subq	$920, %rsp              # imm = 0x398
.Lcfi190:
	.cfi_def_cfa_offset 976
.Lcfi191:
	.cfi_offset %rbx, -56
.Lcfi192:
	.cfi_offset %r12, -48
.Lcfi193:
	.cfi_offset %r13, -40
.Lcfi194:
	.cfi_offset %r14, -32
.Lcfi195:
	.cfi_offset %r15, -24
.Lcfi196:
	.cfi_offset %rbp, -16
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	movq	img(%rip), %rax
	movq	14224(%rax), %rcx
	movq	%rcx, 304(%rsp)         # 8-byte Spill
	movslq	12(%rax), %r12
	movslq	15544(%rax), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movslq	15548(%rax), %rcx
	movslq	15536(%rax), %r15
	testq	%rcx, %rcx
	movl	%ecx, %r13d
	movq	getNeighbour(%rip), %rax
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	js	.LBB17_3
# BB#1:                                 # %.lr.ph552.preheader
	leal	1(%rcx), %r14d
	leaq	512(%rsp), %rbp
	movl	$-1, %ebx
	.p2align	4, 0x90
.LBB17_2:                               # %.lr.ph552
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, %esi
	movl	$1, %ecx
	movl	%r12d, %edi
	movl	%ebx, %edx
	movq	%rbp, %r8
	callq	*%rax
	movq	getNeighbour(%rip), %rax
	addq	$24, %rbp
	incl	%ebx
	decq	%r14
	jne	.LBB17_2
.LBB17_3:                               # %._crit_edge553
	xorl	%r14d, %r14d
	leaq	216(%rsp), %r8
	xorl	%esi, %esi
	movl	$-1, %edx
	movl	$1, %ecx
	movl	%r12d, %edi
	callq	*%rax
	movl	216(%rsp), %r11d
	movl	512(%rsp), %r8d
	movq	input(%rip), %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	cmpl	$0, 272(%rax)
	je	.LBB17_9
# BB#4:
	testl	%r11d, %r11d
	je	.LBB17_6
# BB#5:
	movq	img(%rip), %rcx
	movq	14240(%rcx), %rcx
	movslq	220(%rsp), %rdx
	movl	(%rcx,%rdx,4), %r14d
.LBB17_6:
	movq	80(%rsp), %r9           # 8-byte Reload
	movl	%r9d, %r10d
	sarl	%r10d
	movl	$1, %r11d
	testl	%r10d, %r10d
	movl	$1, %edx
	jle	.LBB17_19
# BB#7:                                 # %.lr.ph547
	movq	img(%rip), %rcx
	movl	%r10d, %esi
	xorl	%edx, %edx
	testb	$1, %sil
	jne	.LBB17_10
# BB#8:
	movl	$1, %ebp
	xorl	%edi, %edi
	cmpl	$1, %r10d
	jne	.LBB17_13
	jmp	.LBB17_19
.LBB17_9:
	movl	536(%rsp), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	80(%rsp), %r9           # 8-byte Reload
	jmp	.LBB17_35
.LBB17_10:                              # %._crit_edge.prol
	cmpl	$0, 536(%rsp)
	je	.LBB17_12
# BB#11:
	movq	14240(%rcx), %rdi
	movslq	540(%rsp), %rbp
	movl	(%rdi,%rbp,4), %edx
.LBB17_12:
	andl	$1, %edx
	movl	$1, %edi
	movl	%edx, %ebp
	cmpl	$1, %r10d
	je	.LBB17_19
.LBB17_13:                              # %.lr.ph547.new
	subq	%rdi, %rsi
	leaq	(%rdi,%rdi,2), %rdi
	leaq	564(%rsp,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB17_14:                              # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	cmpl	$0, -28(%rdi)
	movl	$0, %ebx
	je	.LBB17_16
# BB#15:                                #   in Loop: Header=BB17_14 Depth=1
	movq	14240(%rcx), %rbx
	movslq	-24(%rdi), %rax
	movl	(%rbx,%rax,4), %ebx
.LBB17_16:                              # %._crit_edge.1756
                                        #   in Loop: Header=BB17_14 Depth=1
	andl	%ebp, %ebx
	cmpl	$0, -4(%rdi)
	je	.LBB17_18
# BB#17:                                #   in Loop: Header=BB17_14 Depth=1
	movq	14240(%rcx), %rax
	movslq	(%rdi), %rbp
	movl	(%rax,%rbp,4), %edx
.LBB17_18:                              #   in Loop: Header=BB17_14 Depth=1
	andl	%ebx, %edx
	addq	$48, %rdi
	addq	$-2, %rsi
	movl	%edx, %ebp
	jne	.LBB17_14
.LBB17_19:                              # %.preheader452
	cmpl	%r13d, %r10d
	jge	.LBB17_31
# BB#20:                                # %.lr.ph541
	movq	img(%rip), %rbx
	movslq	%r10d, %rsi
	movl	%r9d, %edi
	subl	%r10d, %edi
	leaq	-1(%r9), %rcx
	testb	$1, %dil
	jne	.LBB17_22
# BB#21:
	movl	$1, %edi
                                        # implicit-def: %R11D
	movq	%rsi, %rbp
	cmpq	%rsi, %rcx
	jne	.LBB17_25
	jmp	.LBB17_31
.LBB17_22:
	leaq	1(%rsi), %rbp
	leaq	(%rbp,%rbp,2), %rdi
	cmpl	$0, 512(%rsp,%rdi,8)
	je	.LBB17_24
# BB#23:
	movq	14240(%rbx), %rax
	movslq	516(%rsp,%rdi,8), %rdi
	movl	(%rax,%rdi,4), %r11d
	andl	$1, %r11d
	movl	%r11d, %edi
	cmpq	%rsi, %rcx
	jne	.LBB17_25
	jmp	.LBB17_31
.LBB17_24:
	xorl	%r11d, %r11d
	xorl	%edi, %edi
	cmpq	%rsi, %rcx
	je	.LBB17_31
.LBB17_25:                              # %.lr.ph541.new
	movq	%r9, %rcx
	subq	%rbp, %rcx
	leaq	(%rbp,%rbp,2), %rsi
	leaq	564(%rsp,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB17_26:                              # =>This Inner Loop Header: Depth=1
	xorl	%r11d, %r11d
	cmpl	$0, -28(%rsi)
	movl	$0, %ebp
	je	.LBB17_28
# BB#27:                                #   in Loop: Header=BB17_26 Depth=1
	movq	14240(%rbx), %rax
	movslq	-24(%rsi), %rbp
	movl	(%rax,%rbp,4), %ebp
.LBB17_28:                              #   in Loop: Header=BB17_26 Depth=1
	andl	%edi, %ebp
	cmpl	$0, -4(%rsi)
	je	.LBB17_30
# BB#29:                                #   in Loop: Header=BB17_26 Depth=1
	movq	14240(%rbx), %rax
	movslq	(%rsi), %rdi
	movl	(%rax,%rdi,4), %r11d
.LBB17_30:                              #   in Loop: Header=BB17_26 Depth=1
	andl	%ebp, %r11d
	addq	$48, %rsi
	addq	$-2, %rcx
	movl	%r11d, %edi
	jne	.LBB17_26
.LBB17_31:                              # %._crit_edge542
	testl	%r8d, %r8d
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%r11d, 48(%rsp)         # 4-byte Spill
	je	.LBB17_33
# BB#32:
	movq	img(%rip), %rax
	movq	14240(%rax), %rax
	movslq	516(%rsp), %rcx
	movl	(%rax,%rcx,4), %r8d
	jmp	.LBB17_34
.LBB17_33:
	xorl	%r8d, %r8d
.LBB17_34:
	movl	%r14d, %r11d
.LBB17_35:
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB17_37
# BB#36:
	movl	%r11d, (%rax)
.LBB17_37:
	testq	%rsi, %rsi
	je	.LBB17_39
# BB#38:
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	setne	%al
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	setne	%cl
	andb	%al, %cl
	movzbl	%cl, %eax
	movl	%eax, (%rsi)
.LBB17_39:
	movq	%r12, 184(%rsp)         # 8-byte Spill
	decq	%r15
	testq	%rdx, %rdx
	je	.LBB17_41
# BB#40:
	movl	%r8d, (%rdx)
.LBB17_41:                              # %.preheader451
	testl	%r11d, %r11d
	setne	%al
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	setne	%cl
	sete	%dl
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	setne	%dil
	sete	%bl
	andb	%al, %cl
	andb	%dil, %al
	movb	%al, 4(%rsp)            # 1-byte Spill
	testl	%r13d, %r13d
	setg	%r10b
	orb	%dl, %bl
	movb	%bl, 6(%rsp)            # 1-byte Spill
	testl	%r8d, %r8d
	setne	%bl
	movb	%cl, 60(%rsp)           # 1-byte Spill
	andb	%cl, %bl
	movq	96(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %r12d
	sarl	%r12d
	leal	-1(%r12), %r14d
	movl	%r9d, %esi
	sarl	%esi
	xorl	%eax, %eax
	cmpl	$8, %edx
	movl	$17, %r8d
	movl	$5, %ebp
	movl	$5, %ecx
	cmovel	%r8d, %ecx
	movl	%ecx, 116(%rsp)         # 4-byte Spill
	setne	%al
	addl	$5, %eax
	movl	%eax, 124(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	cmpl	$8, %r13d
	cmovel	%r8d, %ebp
	movl	%ebp, 120(%rsp)         # 4-byte Spill
	setne	%al
	addl	$5, %eax
	movl	%eax, 112(%rsp)         # 4-byte Spill
	testl	%edx, %edx
	setg	%al
	movl	$1, %ecx
	movl	$1, %ebp
	subl	%r12d, %ebp
	movq	%rbp, 192(%rsp)         # 8-byte Spill
	subl	%esi, %ecx
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	movb	%bl, 3(%rsp)            # 1-byte Spill
	andb	%bl, %dil
	movb	%dil, 7(%rsp)           # 1-byte Spill
	movl	%r13d, %ecx
	andl	$7, %ecx
	movq	%rcx, 424(%rsp)         # 8-byte Spill
	andb	%r10b, %al
	movb	%al, 5(%rsp)            # 1-byte Spill
	movl	%r14d, %eax
	andl	$3, %eax
	movq	%r14, 136(%rsp)         # 8-byte Spill
	movq	%rax, 360(%rsp)         # 8-byte Spill
	subq	%rax, %r14
	movq	%r14, 376(%rsp)         # 8-byte Spill
	leal	-1(%rsi), %eax
	movl	%eax, %ecx
	andl	$3, %ecx
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	%rcx, 352(%rsp)         # 8-byte Spill
	subq	%rcx, %rax
	movq	%rax, 368(%rsp)         # 8-byte Spill
	movl	%edx, %eax
	andl	$1, %eax
	movq	%rax, 472(%rsp)         # 8-byte Spill
	movl	%r13d, %eax
	andl	$1, %eax
	movq	%rax, 416(%rsp)         # 8-byte Spill
	movl	%r13d, %eax
	andl	$3, %eax
	movq	%rax, 464(%rsp)         # 8-byte Spill
	movq	%r15, %rax
	shlq	$5, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	shlq	$6, %r15
	leaq	IntraChromaPrediction.block_pos(%r15), %rax
	movq	%rax, 448(%rsp)         # 8-byte Spill
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqa	%xmm0, 496(%rsp)        # 16-byte Spill
	leaq	(%rdx,%rdx), %rax
	movq	%rax, 456(%rsp)         # 8-byte Spill
	leal	(%rdx,%rdx), %eax
	movl	%eax, 108(%rsp)         # 4-byte Spill
	leal	(%r9,%r9), %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movq	%r12, 440(%rsp)         # 8-byte Spill
	movslq	%r12d, %rax
	movq	%rsi, 432(%rsp)         # 8-byte Spill
	movslq	%esi, %rcx
	movq	img(%rip), %r15
	movl	%edx, %edx
	movq	%rdx, 208(%rsp)         # 8-byte Spill
	movq	%r13, 88(%rsp)          # 8-byte Spill
	leaq	-1(%r13), %rdx
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	leaq	9552(%r15), %rdx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	leaq	9776(%r15), %rdx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	leaq	9040(%r15), %rdx
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	leaq	272(%rsp,%rax,2), %rdx
	movq	%rdx, 344(%rsp)         # 8-byte Spill
	leaq	262(%rsp,%rax,2), %rdx
	movq	%rdx, 336(%rsp)         # 8-byte Spill
	movq	%rax, 408(%rsp)         # 8-byte Spill
	leaq	268(%rsp,%rax,2), %rax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	leaq	240(%rsp,%rcx,2), %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	leaq	230(%rsp,%rcx,2), %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movq	%rcx, 400(%rsp)         # 8-byte Spill
	leaq	236(%rsp,%rcx,2), %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	leaq	10066(%r15), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movl	%r11d, 28(%rsp)         # 4-byte Spill
	movq	%r15, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB17_42:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_44 Depth 2
                                        #       Child Loop BB17_45 Depth 3
                                        #     Child Loop BB17_89 Depth 2
                                        #     Child Loop BB17_93 Depth 2
                                        #     Child Loop BB17_100 Depth 2
                                        #     Child Loop BB17_103 Depth 2
                                        #       Child Loop BB17_105 Depth 3
                                        #       Child Loop BB17_109 Depth 3
                                        #     Child Loop BB17_118 Depth 2
                                        #     Child Loop BB17_122 Depth 2
                                        #     Child Loop BB17_128 Depth 2
                                        #     Child Loop BB17_132 Depth 2
                                        #     Child Loop BB17_136 Depth 2
                                        #       Child Loop BB17_141 Depth 3
	movq	enc_picture(%rip), %rax
	movq	6472(%rax), %rax
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %r12
	movl	15528(%r15), %eax
	sarl	%eax
	testl	%eax, %eax
	jle	.LBB17_85
# BB#43:                                # %.preheader445.lr.ph
                                        #   in Loop: Header=BB17_42 Depth=1
	movl	15516(%r15), %r14d
	movslq	236(%rsp), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movslq	232(%rsp), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	cltq
	movq	%rax, 480(%rsp)         # 8-byte Spill
	movq	448(%rsp), %rax         # 8-byte Reload
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB17_44:                              # %.preheader445
                                        #   Parent Loop BB17_42 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_45 Depth 3
	movq	$-4, %rsi
	movq	%rax, 488(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB17_45:                              #   Parent Loop BB17_42 Depth=1
                                        #     Parent Loop BB17_44 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi), %rax
	movzbl	subblk_offset_y+4(%rax,%r13,4), %r9d
	movzbl	subblk_offset_x+4(%rax,%r13,4), %ebp
	movl	(%rdi), %eax
	cmpq	$3, %rax
	movl	%r14d, %edx
	ja	.LBB17_76
# BB#46:                                #   in Loop: Header=BB17_45 Depth=3
	jmpq	*.LJTI17_0(,%rax,8)
.LBB17_47:                              #   in Loop: Header=BB17_45 Depth=3
	xorl	%r8d, %r8d
	testl	%r11d, %r11d
	movl	$0, %edx
	je	.LBB17_50
# BB#48:                                # %.preheader431
                                        #   in Loop: Header=BB17_45 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%r12,%rax,8), %rcx
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp), %rbx
	movzwl	(%rcx,%rbx,2), %edx
	leaq	3(%rbp), %rax
	cmpq	%rax, %rbp
	jae	.LBB17_50
# BB#49:                                #   in Loop: Header=BB17_45 Depth=3
	movzwl	2(%rcx,%rbx,2), %eax
	addl	%edx, %eax
	movzwl	4(%rcx,%rbx,2), %r10d
	addl	%eax, %r10d
	movzwl	6(%rcx,%rbx,2), %edx
	addl	%r10d, %edx
.LBB17_50:                              # %.loopexit
                                        #   in Loop: Header=BB17_45 Depth=3
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB17_54
# BB#51:                                # %.preheader430
                                        #   in Loop: Header=BB17_45 Depth=3
	leaq	1(%r9), %rax
	leaq	(%rax,%rax,2), %rcx
	movslq	532(%rsp,%rcx,8), %rbx
	movq	(%r12,%rbx,8), %rbx
	movslq	528(%rsp,%rcx,8), %rcx
	movzwl	(%rbx,%rcx,2), %r8d
	leaq	4(%r9), %rcx
	cmpq	%rcx, %rax
	jae	.LBB17_53
# BB#52:                                #   in Loop: Header=BB17_45 Depth=3
	leaq	2(%r9), %rax
	leaq	(%rax,%rax,2), %rax
	movslq	532(%rsp,%rax,8), %rcx
	movq	(%r12,%rcx,8), %rcx
	movslq	528(%rsp,%rax,8), %rax
	movzwl	(%rcx,%rax,2), %eax
	addl	%r8d, %eax
	leaq	(%r9,%r9,2), %rcx
	movslq	604(%rsp,%rcx,8), %rbx
	movq	(%r12,%rbx,8), %r8
	movslq	600(%rsp,%rcx,8), %rbx
	movzwl	(%r8,%rbx,2), %ebx
	addl	%eax, %ebx
	movslq	628(%rsp,%rcx,8), %rax
	movq	(%r12,%rax,8), %rax
	movslq	624(%rsp,%rcx,8), %rcx
	movzwl	(%rax,%rcx,2), %r8d
	addl	%ebx, %r8d
.LBB17_53:                              #   in Loop: Header=BB17_45 Depth=3
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	jne	.LBB17_68
.LBB17_54:                              # %.thread
                                        #   in Loop: Header=BB17_45 Depth=3
	testl	%r11d, %r11d
	jne	.LBB17_75
# BB#55:                                #   in Loop: Header=BB17_45 Depth=3
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movl	%r14d, %edx
	jne	.LBB17_71
	jmp	.LBB17_76
	.p2align	4, 0x90
.LBB17_56:                              #   in Loop: Header=BB17_45 Depth=3
	testl	%r11d, %r11d
	jne	.LBB17_73
# BB#57:                                #   in Loop: Header=BB17_45 Depth=3
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movl	%r14d, %edx
	jne	.LBB17_59
	jmp	.LBB17_76
	.p2align	4, 0x90
.LBB17_58:                              #   in Loop: Header=BB17_45 Depth=3
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	je	.LBB17_72
.LBB17_59:                              # %.preheader434
                                        #   in Loop: Header=BB17_45 Depth=3
	leaq	1(%r9), %rax
	leaq	(%rax,%rax,2), %rcx
	movslq	532(%rsp,%rcx,8), %rdx
	movq	(%r12,%rdx,8), %rdx
	movslq	528(%rsp,%rcx,8), %rcx
	movzwl	(%rdx,%rcx,2), %edx
	leaq	4(%r9), %rcx
	cmpq	%rcx, %rax
	jae	.LBB17_75
# BB#60:                                #   in Loop: Header=BB17_45 Depth=3
	leaq	2(%r9), %rax
	leaq	(%rax,%rax,2), %rax
	movslq	532(%rsp,%rax,8), %rcx
	movq	(%r12,%rcx,8), %rcx
	movslq	528(%rsp,%rax,8), %rax
	movzwl	(%rcx,%rax,2), %eax
	addl	%edx, %eax
	leaq	(%r9,%r9,2), %rcx
	movslq	604(%rsp,%rcx,8), %rdx
	movq	(%r12,%rdx,8), %rdx
	movslq	600(%rsp,%rcx,8), %rbx
	movzwl	(%rdx,%rbx,2), %ebx
	addl	%eax, %ebx
	movslq	628(%rsp,%rcx,8), %rax
	movq	(%r12,%rax,8), %rax
	movslq	624(%rsp,%rcx,8), %rcx
	movzwl	(%rax,%rcx,2), %edx
	addl	%ebx, %edx
	jmp	.LBB17_75
	.p2align	4, 0x90
.LBB17_61:                              #   in Loop: Header=BB17_45 Depth=3
	xorl	%r8d, %r8d
	testl	%r11d, %r11d
	movl	$0, %edx
	je	.LBB17_64
# BB#62:                                # %.preheader441
                                        #   in Loop: Header=BB17_45 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%r12,%rax,8), %rcx
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp), %rdx
	movzwl	(%rcx,%rdx,2), %edx
	leaq	3(%rbp), %rbx
	cmpq	%rbx, %rbp
	jae	.LBB17_64
# BB#63:                                #   in Loop: Header=BB17_45 Depth=3
	leaq	(%rax,%rbp), %r10
	movzwl	2(%rcx,%r10,2), %eax
	addl	%edx, %eax
	movzwl	4(%rcx,%r10,2), %ebx
	addl	%eax, %ebx
	movzwl	6(%rcx,%r10,2), %edx
	addl	%ebx, %edx
.LBB17_64:                              # %.loopexit442
                                        #   in Loop: Header=BB17_45 Depth=3
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	je	.LBB17_69
# BB#65:                                # %.preheader440
                                        #   in Loop: Header=BB17_45 Depth=3
	leaq	1(%r9), %rcx
	leaq	(%rcx,%rcx,2), %rax
	movslq	532(%rsp,%rax,8), %rbx
	movq	(%r12,%rbx,8), %rbx
	movslq	528(%rsp,%rax,8), %rax
	movzwl	(%rbx,%rax,2), %r8d
	leaq	4(%r9), %rbx
	cmpq	%rbx, %rcx
	jae	.LBB17_67
# BB#66:                                #   in Loop: Header=BB17_45 Depth=3
	leaq	2(%r9), %rax
	leaq	(%rax,%rax,2), %rax
	movslq	532(%rsp,%rax,8), %rcx
	movq	(%r12,%rcx,8), %rcx
	movslq	528(%rsp,%rax,8), %rax
	movzwl	(%rcx,%rax,2), %eax
	addl	%r8d, %eax
	leaq	(%r9,%r9,2), %rcx
	movslq	604(%rsp,%rcx,8), %rbx
	movq	(%r12,%rbx,8), %r8
	movslq	600(%rsp,%rcx,8), %rbx
	movzwl	(%r8,%rbx,2), %ebx
	addl	%eax, %ebx
	movslq	628(%rsp,%rcx,8), %rax
	movq	(%r12,%rax,8), %rax
	movslq	624(%rsp,%rcx,8), %rcx
	movzwl	(%rax,%rcx,2), %r8d
	addl	%ebx, %r8d
.LBB17_67:                              #   in Loop: Header=BB17_45 Depth=3
	cmpb	$0, 60(%rsp)            # 1-byte Folded Reload
	je	.LBB17_69
.LBB17_68:                              #   in Loop: Header=BB17_45 Depth=3
	leal	4(%rdx,%r8), %edx
	sarl	$3, %edx
	jmp	.LBB17_76
.LBB17_69:                              # %.thread418
                                        #   in Loop: Header=BB17_45 Depth=3
	testl	%r11d, %r11d
	jne	.LBB17_75
# BB#70:                                #   in Loop: Header=BB17_45 Depth=3
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	movl	%r14d, %edx
	je	.LBB17_76
.LBB17_71:                              #   in Loop: Header=BB17_45 Depth=3
	addl	$2, %r8d
	sarl	$2, %r8d
	movl	%r8d, %edx
	jmp	.LBB17_76
.LBB17_72:                              #   in Loop: Header=BB17_45 Depth=3
	testl	%r11d, %r11d
	movl	%r14d, %edx
	je	.LBB17_76
.LBB17_73:                              # %.preheader432
                                        #   in Loop: Header=BB17_45 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%r12,%rax,8), %rax
	movq	64(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rbp), %rcx
	movzwl	(%rax,%rcx,2), %edx
	leaq	3(%rbp), %rcx
	cmpq	%rcx, %rbp
	jae	.LBB17_75
# BB#74:                                #   in Loop: Header=BB17_45 Depth=3
	leaq	(%rbx,%rbp), %r8
	movzwl	2(%rax,%r8,2), %ebx
	addl	%edx, %ebx
	movzwl	4(%rax,%r8,2), %ecx
	addl	%ebx, %ecx
	movzwl	6(%rax,%r8,2), %edx
	addl	%ecx, %edx
	.p2align	4, 0x90
.LBB17_75:                              #   in Loop: Header=BB17_45 Depth=3
	addl	$2, %edx
	sarl	$2, %edx
.LBB17_76:                              # %.thread414
                                        #   in Loop: Header=BB17_45 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	shlq	$11, %rax
	addq	%r15, %rax
	movq	%r9, %rcx
	shlq	$5, %rcx
	leaq	(%rax,%rcx), %r15
	movw	%dx, 8528(%r15,%rbp,2)
	leaq	1(%rbp), %r8
	leaq	3(%rbp), %r10
	cmpq	%r10, %rbp
	jae	.LBB17_78
# BB#77:                                #   in Loop: Header=BB17_45 Depth=3
	movw	%dx, 8528(%r15,%r8,2)
	movw	%dx, 8532(%r15,%rbp,2)
	movw	%dx, 8534(%r15,%rbp,2)
.LBB17_78:                              #   in Loop: Header=BB17_45 Depth=3
	leaq	3(%r9), %rbx
	cmpq	%rbx, %r9
	jae	.LBB17_83
# BB#79:                                # %.preheader429.1
                                        #   in Loop: Header=BB17_45 Depth=3
	leaq	1(%r9), %rbx
	shlq	$5, %rbx
	leaq	(%rax,%rbx), %r11
	cmpq	%r10, %rbp
	movw	%dx, 8528(%r11,%rbp,2)
	jae	.LBB17_81
# BB#80:                                #   in Loop: Header=BB17_45 Depth=3
	leaq	8528(%rcx,%rax), %rcx
	leaq	8528(%rbx,%rax), %rbx
	movw	%dx, (%rbx,%r8,2)
	movw	%dx, 4(%rbx,%rbp,2)
	movw	%dx, 6(%rbx,%rbp,2)
	movw	%dx, 64(%rcx,%rbp,2)
	movw	%dx, 64(%rcx,%r8,2)
	movw	%dx, 68(%rcx,%rbp,2)
	movw	%dx, 70(%rcx,%rbp,2)
	addq	$3, %r9
	movq	%r9, %rcx
	shlq	$5, %rcx
	leaq	8528(%rcx,%rax), %rcx
	movw	%dx, (%rcx,%rbp,2)
	movw	%dx, (%rcx,%r8,2)
	movw	%dx, 4(%rcx,%rbp,2)
	leaq	3(%rbp), %rbp
	jmp	.LBB17_82
	.p2align	4, 0x90
.LBB17_81:                              # %.preheader429.3625.thread690
                                        #   in Loop: Header=BB17_45 Depth=3
	movw	%dx, 8592(%r15,%rbp,2)
	addq	$3, %r9
.LBB17_82:                              # %.sink.split
                                        #   in Loop: Header=BB17_45 Depth=3
	movl	28(%rsp), %r11d         # 4-byte Reload
	shlq	$5, %r9
	addq	%r9, %rax
	movw	%dx, 8528(%rax,%rbp,2)
.LBB17_83:                              #   in Loop: Header=BB17_45 Depth=3
	addq	$4, %rdi
	incq	%rsi
	movq	40(%rsp), %r15          # 8-byte Reload
	jne	.LBB17_45
# BB#84:                                #   in Loop: Header=BB17_44 Depth=2
	incq	%r13
	movq	488(%rsp), %rax         # 8-byte Reload
	addq	$16, %rax
	cmpq	480(%rsp), %r13         # 8-byte Folded Reload
	jl	.LBB17_44
.LBB17_85:                              # %._crit_edge512
                                        #   in Loop: Header=BB17_42 Depth=1
	testl	%r11d, %r11d
	movq	456(%rsp), %r13         # 8-byte Reload
	leaq	272(%rsp), %rdi
	je	.LBB17_94
# BB#86:                                #   in Loop: Header=BB17_42 Depth=1
	movslq	236(%rsp), %rax
	movslq	232(%rsp), %rsi
	addq	%rsi, %rsi
	addq	(%r12,%rax,8), %rsi
	movq	%r13, %rdx
	callq	memcpy
	cmpl	$0, 80(%rsp)            # 4-byte Folded Reload
	jle	.LBB17_94
# BB#87:                                # %.lr.ph515.preheader
                                        #   in Loop: Header=BB17_42 Depth=1
	movq	424(%rsp), %r14         # 8-byte Reload
	testq	%r14, %r14
	je	.LBB17_90
# BB#88:                                # %.lr.ph515.prol.preheader
                                        #   in Loop: Header=BB17_42 Depth=1
	movq	160(%rsp), %rbx         # 8-byte Reload
	xorl	%ebp, %ebp
	leaq	272(%rsp), %r15
	.p2align	4, 0x90
.LBB17_89:                              # %.lr.ph515.prol
                                        #   Parent Loop BB17_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	memcpy
	incq	%rbp
	addq	$32, %rbx
	cmpq	%rbp, %r14
	jne	.LBB17_89
	jmp	.LBB17_91
.LBB17_90:                              #   in Loop: Header=BB17_42 Depth=1
	xorl	%ebp, %ebp
	leaq	272(%rsp), %r15
.LBB17_91:                              # %.lr.ph515.prol.loopexit
                                        #   in Loop: Header=BB17_42 Depth=1
	cmpq	$7, 176(%rsp)           # 8-byte Folded Reload
	jb	.LBB17_94
# BB#92:                                # %.lr.ph515.preheader.new
                                        #   in Loop: Header=BB17_42 Depth=1
	movq	88(%rsp), %rbx          # 8-byte Reload
	subq	%rbp, %rbx
	shlq	$5, %rbp
	movq	152(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB17_93:                              # %.lr.ph515
                                        #   Parent Loop BB17_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r14,%rbp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	-224(%r14,%rbp), %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	memcpy
	leaq	-192(%r14,%rbp), %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	memcpy
	leaq	-160(%r14,%rbp), %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	memcpy
	leaq	-128(%r14,%rbp), %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	memcpy
	leaq	-96(%r14,%rbp), %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	memcpy
	leaq	-64(%r14,%rbp), %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	memcpy
	leaq	-32(%r14,%rbp), %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	memcpy
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	memcpy
	addq	$256, %r14              # imm = 0x100
	addq	$-8, %rbx
	jne	.LBB17_93
	.p2align	4, 0x90
.LBB17_94:                              # %.loopexit450
                                        #   in Loop: Header=BB17_42 Depth=1
	cmpb	$0, 6(%rsp)             # 1-byte Folded Reload
	movq	464(%rsp), %rbx         # 8-byte Reload
	pxor	%xmm8, %xmm8
	movdqa	.LCPI17_1(%rip), %xmm7  # xmm7 = [1,1,1,1]
	movdqa	.LCPI17_2(%rip), %xmm6  # xmm6 = [4,4]
	movq	208(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	leaq	242(%rsp), %r10
	movq	88(%rsp), %r11          # 8-byte Reload
	jne	.LBB17_111
# BB#95:                                # %.preheader449
                                        #   in Loop: Header=BB17_42 Depth=1
	testl	%r11d, %r11d
	jle	.LBB17_101
# BB#96:                                # %.lr.ph517.preheader
                                        #   in Loop: Header=BB17_42 Depth=1
	cmpq	$0, 416(%rsp)           # 8-byte Folded Reload
	jne	.LBB17_98
# BB#97:                                #   in Loop: Header=BB17_42 Depth=1
	xorl	%edx, %edx
	testq	%r9, %r9
	jne	.LBB17_99
	jmp	.LBB17_101
.LBB17_98:                              # %.lr.ph517.prol
                                        #   in Loop: Header=BB17_42 Depth=1
	movslq	556(%rsp), %rax
	movq	(%r12,%rax,8), %rax
	movslq	552(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, 240(%rsp)
	movl	$1, %edx
	testq	%r9, %r9
	je	.LBB17_101
.LBB17_99:                              # %.lr.ph517.preheader.new
                                        #   in Loop: Header=BB17_42 Depth=1
	movq	%r11, %rax
	subq	%rdx, %rax
	leaq	(%rdx,%rdx,2), %rcx
	leaq	580(%rsp), %rsi
	leaq	(%rsi,%rcx,8), %rcx
	leaq	(%r10,%rdx,2), %rdx
	.p2align	4, 0x90
.LBB17_100:                             # %.lr.ph517
                                        #   Parent Loop BB17_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	-24(%rcx), %rsi
	movq	(%r12,%rsi,8), %rsi
	movslq	-28(%rcx), %rdi
	movzwl	(%rsi,%rdi,2), %esi
	movw	%si, -2(%rdx)
	movslq	(%rcx), %rsi
	movq	(%r12,%rsi,8), %rsi
	movslq	-4(%rcx), %rdi
	movzwl	(%rsi,%rdi,2), %esi
	movw	%si, (%rdx)
	addq	$48, %rcx
	addq	$4, %rdx
	addq	$-2, %rax
	jne	.LBB17_100
.LBB17_101:                             # %.preheader447
                                        #   in Loop: Header=BB17_42 Depth=1
	cmpb	$0, 5(%rsp)             # 1-byte Folded Reload
	je	.LBB17_111
# BB#102:                               # %.preheader444.us.preheader
                                        #   in Loop: Header=BB17_42 Depth=1
	movq	168(%rsp), %rax         # 8-byte Reload
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB17_103:                             # %.preheader444.us
                                        #   Parent Loop BB17_42 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_105 Depth 3
                                        #       Child Loop BB17_109 Depth 3
	testq	%rbx, %rbx
	je	.LBB17_106
# BB#104:                               # %.prol.preheader
                                        #   in Loop: Header=BB17_103 Depth=2
	movq	%rax, %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB17_105:                             #   Parent Loop BB17_42 Depth=1
                                        #     Parent Loop BB17_103 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	240(%rsp,%rdx,2), %edi
	movw	%di, (%rsi)
	incq	%rdx
	addq	$32, %rsi
	cmpq	%rdx, %rbx
	jne	.LBB17_105
	jmp	.LBB17_107
	.p2align	4, 0x90
.LBB17_106:                             #   in Loop: Header=BB17_103 Depth=2
	xorl	%edx, %edx
.LBB17_107:                             # %.prol.loopexit
                                        #   in Loop: Header=BB17_103 Depth=2
	cmpq	$3, %r9
	jb	.LBB17_110
# BB#108:                               # %.preheader444.us.new
                                        #   in Loop: Header=BB17_103 Depth=2
	movq	%r11, %rsi
	subq	%rdx, %rsi
	leaq	4(%r10,%rdx,2), %rdi
	shlq	$5, %rdx
	addq	%rax, %rdx
	.p2align	4, 0x90
.LBB17_109:                             #   Parent Loop BB17_42 Depth=1
                                        #     Parent Loop BB17_103 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	-6(%rdi), %ebp
	movw	%bp, (%rdx)
	movzwl	-4(%rdi), %ebp
	movw	%bp, 32(%rdx)
	movzwl	-2(%rdi), %ebp
	movw	%bp, 64(%rdx)
	movzwl	(%rdi), %ebp
	movw	%bp, 96(%rdx)
	subq	$-128, %rdx
	addq	$8, %rdi
	addq	$-4, %rsi
	jne	.LBB17_109
.LBB17_110:                             # %._crit_edge520.us
                                        #   in Loop: Header=BB17_103 Depth=2
	incq	%rcx
	addq	$2, %rax
	cmpq	%r8, %rcx
	jne	.LBB17_103
.LBB17_111:                             # %.loopexit448
                                        #   in Loop: Header=BB17_42 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	je	.LBB17_143
# BB#112:                               #   in Loop: Header=BB17_42 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	movzwl	270(%rsp,%rax,2), %r14d
	movslq	532(%rsp), %rax
	movq	(%r12,%rax,8), %rax
	movslq	528(%rsp), %rcx
	movzwl	(%rax,%rcx,2), %ecx
	movl	%r14d, %r10d
	subl	%ecx, %r10d
	movq	440(%rsp), %rax         # 8-byte Reload
	imull	%eax, %r10d
	cmpl	$2, %eax
	jl	.LBB17_115
# BB#113:                               # %.lr.ph525.preheader
                                        #   in Loop: Header=BB17_42 Depth=1
	cmpl	$4, 136(%rsp)           # 4-byte Folded Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	jb	.LBB17_120
# BB#116:                               # %min.iters.checked712
                                        #   in Loop: Header=BB17_42 Depth=1
	movq	376(%rsp), %rdx         # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB17_120
# BB#117:                               # %vector.ph716
                                        #   in Loop: Header=BB17_42 Depth=1
	movd	%r10d, %xmm0
	movq	%rdx, %rax
	movq	336(%rsp), %rsi         # 8-byte Reload
	movq	344(%rsp), %rdi         # 8-byte Reload
	movaps	496(%rsp), %xmm1        # 16-byte Reload
	movaps	.LCPI17_0(%rip), %xmm2  # xmm2 = [2,3]
	.p2align	4, 0x90
.LBB17_118:                             # %vector.body708
                                        #   Parent Loop BB17_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %xmm3           # xmm3 = mem[0],zero
	punpcklwd	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3]
	movq	(%rsi), %xmm4           # xmm4 = mem[0],zero
	pshuflw	$27, %xmm4, %xmm4       # xmm4 = xmm4[3,2,1,0,4,5,6,7]
	punpcklwd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3]
	psubd	%xmm4, %xmm3
	movaps	%xmm1, %xmm4
	shufps	$136, %xmm2, %xmm4      # xmm4 = xmm4[0,2],xmm2[0,2]
	paddd	%xmm7, %xmm4
	pshufd	$245, %xmm3, %xmm5      # xmm5 = xmm3[1,1,3,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm5, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	paddd	%xmm3, %xmm0
	paddq	%xmm6, %xmm1
	paddq	%xmm6, %xmm2
	addq	$8, %rdi
	addq	$-8, %rsi
	addq	$-4, %rax
	jne	.LBB17_118
# BB#119:                               # %middle.block709
                                        #   in Loop: Header=BB17_42 Depth=1
	cmpl	$0, 360(%rsp)           # 4-byte Folded Reload
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %r10d
	movq	%rdx, %rax
	jne	.LBB17_121
	jmp	.LBB17_123
	.p2align	4, 0x90
.LBB17_115:                             #   in Loop: Header=BB17_42 Depth=1
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	jmp	.LBB17_123
.LBB17_120:                             #   in Loop: Header=BB17_42 Depth=1
	xorl	%eax, %eax
.LBB17_121:                             # %.lr.ph525.preheader733
                                        #   in Loop: Header=BB17_42 Depth=1
	leaq	(%rax,%rax), %rdi
	movq	392(%rsp), %rsi         # 8-byte Reload
	subq	%rdi, %rsi
	movq	408(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax), %rdi
	leaq	272(%rsp,%rdi,2), %rdi
	movq	136(%rsp), %rbp         # 8-byte Reload
	subq	%rax, %rbp
	incl	%eax
	.p2align	4, 0x90
.LBB17_122:                             # %.lr.ph525
                                        #   Parent Loop BB17_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rdi), %ebx
	movzwl	(%rsi), %edx
	subl	%edx, %ebx
	imull	%eax, %ebx
	addl	%ebx, %r10d
	addq	$-2, %rsi
	addq	$2, %rdi
	incl	%eax
	decq	%rbp
	jne	.LBB17_122
.LBB17_123:                             # %._crit_edge526
                                        #   in Loop: Header=BB17_42 Depth=1
	movzwl	238(%rsp,%r8,2), %edi
	movl	%edi, %r11d
	subl	%ecx, %r11d
	movq	432(%rsp), %rax         # 8-byte Reload
	imull	%eax, %r11d
	cmpl	$2, %eax
	jl	.LBB17_133
# BB#124:                               # %.lr.ph531.preheader
                                        #   in Loop: Header=BB17_42 Depth=1
	cmpl	$4, 128(%rsp)           # 4-byte Folded Reload
	jb	.LBB17_130
# BB#126:                               # %min.iters.checked
                                        #   in Loop: Header=BB17_42 Depth=1
	movq	368(%rsp), %rdx         # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB17_130
# BB#127:                               # %vector.ph
                                        #   in Loop: Header=BB17_42 Depth=1
	movd	%r11d, %xmm0
	movl	$1, %eax
	movd	%rax, %xmm1
	pslldq	$8, %xmm1               # xmm1 = zero,zero,zero,zero,zero,zero,zero,zero,xmm1[0,1,2,3,4,5,6,7]
	movq	%rdx, %rax
	movq	320(%rsp), %rcx         # 8-byte Reload
	movq	328(%rsp), %rsi         # 8-byte Reload
	movaps	.LCPI17_0(%rip), %xmm2  # xmm2 = [2,3]
	.p2align	4, 0x90
.LBB17_128:                             # %vector.body
                                        #   Parent Loop BB17_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %xmm3           # xmm3 = mem[0],zero
	punpcklwd	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0],xmm3[1],xmm8[1],xmm3[2],xmm8[2],xmm3[3],xmm8[3]
	movq	(%rcx), %xmm4           # xmm4 = mem[0],zero
	pshuflw	$27, %xmm4, %xmm4       # xmm4 = xmm4[3,2,1,0,4,5,6,7]
	punpcklwd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3]
	psubd	%xmm4, %xmm3
	movdqa	%xmm1, %xmm4
	shufps	$136, %xmm2, %xmm4      # xmm4 = xmm4[0,2],xmm2[0,2]
	paddd	%xmm7, %xmm4
	pshufd	$245, %xmm3, %xmm5      # xmm5 = xmm3[1,1,3,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm5, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	paddd	%xmm3, %xmm0
	paddq	%xmm6, %xmm1
	paddq	%xmm6, %xmm2
	addq	$8, %rsi
	addq	$-8, %rcx
	addq	$-4, %rax
	jne	.LBB17_128
# BB#129:                               # %middle.block
                                        #   in Loop: Header=BB17_42 Depth=1
	cmpl	$0, 352(%rsp)           # 4-byte Folded Reload
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %r11d
	movq	%rdx, %rax
	jne	.LBB17_131
	jmp	.LBB17_133
.LBB17_130:                             #   in Loop: Header=BB17_42 Depth=1
	xorl	%eax, %eax
.LBB17_131:                             # %.lr.ph531.preheader732
                                        #   in Loop: Header=BB17_42 Depth=1
	leaq	(%rax,%rax), %rdx
	movq	384(%rsp), %rcx         # 8-byte Reload
	subq	%rdx, %rcx
	movq	400(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax), %rdx
	leaq	240(%rsp,%rdx,2), %rsi
	movq	128(%rsp), %rbp         # 8-byte Reload
	subq	%rax, %rbp
	incl	%eax
	.p2align	4, 0x90
.LBB17_132:                             # %.lr.ph531
                                        #   Parent Loop BB17_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rsi), %edx
	movzwl	(%rcx), %ebx
	subl	%ebx, %edx
	imull	%eax, %edx
	addl	%edx, %r11d
	addq	$-2, %rcx
	addq	$2, %rsi
	incl	%eax
	decq	%rbp
	jne	.LBB17_132
.LBB17_133:                             # %._crit_edge532
                                        #   in Loop: Header=BB17_42 Depth=1
	imull	116(%rsp), %r10d        # 4-byte Folded Reload
	addl	108(%rsp), %r10d        # 4-byte Folded Reload
	movl	124(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r10d
	imull	120(%rsp), %r11d        # 4-byte Folded Reload
	addl	104(%rsp), %r11d        # 4-byte Folded Reload
	movl	112(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r11d
	testl	%r9d, %r9d
	jle	.LBB17_143
# BB#134:                               # %.preheader443.lr.ph
                                        #   in Loop: Header=BB17_42 Depth=1
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	jle	.LBB17_143
# BB#135:                               # %.preheader443.us.preheader
                                        #   in Loop: Header=BB17_42 Depth=1
	leal	(%rdi,%r14), %eax
	shll	$4, %eax
	addl	$16, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	192(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r10d, %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movq	200(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r11d, %eax
	shll	$4, %r14d
	addl	%eax, %r14d
	shll	$4, %edi
	leal	16(%rdi,%r14), %r12d
	leal	(%r10,%r10), %r13d
	movq	144(%rsp), %r14         # 8-byte Reload
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB17_136:                             # %.preheader443.us
                                        #   Parent Loop BB17_42 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_141 Depth 3
	cmpq	$0, 472(%rsp)           # 8-byte Folded Reload
	movl	$0, %esi
	jne	.LBB17_138
# BB#137:                               #   in Loop: Header=BB17_136 Depth=2
	xorl	%edx, %edx
	jmp	.LBB17_139
	.p2align	4, 0x90
.LBB17_138:                             #   in Loop: Header=BB17_136 Depth=2
	movq	200(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r15), %eax
	imull	%r11d, %eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	15524(%rdx), %ecx
	addl	64(%rsp), %eax          # 4-byte Folded Reload
	sarl	$5, %eax
	cmovsl	%esi, %eax
	cmpl	%ecx, %eax
	cmovlw	%ax, %cx
	movq	32(%rsp), %rax          # 8-byte Reload
	shlq	$11, %rax
	addq	%rdx, %rax
	movq	%r15, %rdx
	shlq	$5, %rdx
	movw	%cx, 10064(%rdx,%rax)
	movl	$1, %edx
.LBB17_139:                             # %.prol.loopexit747
                                        #   in Loop: Header=BB17_136 Depth=2
	cmpl	$1, 96(%rsp)            # 4-byte Folded Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	$0, %r9d
	je	.LBB17_142
# BB#140:                               # %.preheader443.us.new
                                        #   in Loop: Header=BB17_136 Depth=2
	movq	208(%rsp), %rcx         # 8-byte Reload
	subq	%rdx, %rcx
	movq	192(%rsp), %rsi         # 8-byte Reload
	leal	1(%rsi,%rdx), %eax
	imull	%r10d, %eax
	addl	%r12d, %eax
	leal	(%rsi,%rdx), %esi
	imull	%r10d, %esi
	addl	%r12d, %esi
	leaq	(%r14,%rdx,2), %rbx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB17_141:                             #   Parent Loop BB17_42 Depth=1
                                        #     Parent Loop BB17_136 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	15524(%r8), %ebp
	leal	(%rsi,%rdx), %edi
	sarl	$5, %edi
	cmovsl	%r9d, %edi
	cmpl	%ebp, %edi
	cmovlw	%di, %bp
	movw	%bp, -2(%rbx)
	movl	15524(%r8), %edi
	leal	(%rax,%rdx), %ebp
	sarl	$5, %ebp
	cmovsl	%r9d, %ebp
	cmpl	%edi, %ebp
	cmovlw	%bp, %di
	movw	%di, (%rbx)
	addl	%r13d, %edx
	addq	$4, %rbx
	addq	$-2, %rcx
	jne	.LBB17_141
.LBB17_142:                             # %._crit_edge536.us
                                        #   in Loop: Header=BB17_136 Depth=2
	incq	%r15
	addl	%r11d, %r12d
	addq	$32, %r14
	cmpq	88(%rsp), %r15          # 8-byte Folded Reload
	jne	.LBB17_136
	.p2align	4, 0x90
.LBB17_143:                             # %.loopexit446
                                        #   in Loop: Header=BB17_42 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	addq	$2048, 160(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x800
	addq	$2048, 152(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x800
	addq	$2048, 168(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x800
	addq	$2048, 144(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x800
	cmpq	$2, %rcx
	movl	28(%rsp), %r11d         # 4-byte Reload
	movq	40(%rsp), %r15          # 8-byte Reload
	jne	.LBB17_42
# BB#144:
	movq	312(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 4168(%rax)
	jne	.LBB17_176
# BB#145:                               # %.preheader428
	movq	88(%rsp), %r15          # 8-byte Reload
	testl	%r15d, %r15d
	movq	184(%rsp), %rbx         # 8-byte Reload
	jle	.LBB17_148
# BB#146:                               # %.lr.ph.preheader
	leaq	512(%rsp), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_147:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	movl	$1, %ecx
	movl	%ebx, %edi
	movl	%ebp, %edx
	movq	%r14, %r8
	callq	*getNeighbour(%rip)
	incq	%rbp
	addq	$24, %r14
	cmpq	%rbp, %r15
	jne	.LBB17_147
.LBB17_148:                             # %.preheader427
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	sete	%cl
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	sete	%al
	orb	%al, %cl
	movb	%cl, 4(%rsp)            # 1-byte Spill
	movb	3(%rsp), %cl            # 1-byte Reload
	xorb	$1, %cl
	orb	%al, %cl
	movb	%cl, 3(%rsp)            # 1-byte Spill
	xorl	%ebp, %ebp
	movl	$2147483647, 60(%rsp)   # 4-byte Folded Spill
                                        # imm = 0x7FFFFFFF
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	movq	80(%rsp), %rsi          # 8-byte Reload
	movl	28(%rsp), %edi          # 4-byte Reload
	.p2align	4, 0x90
.LBB17_149:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_161 Depth 2
                                        #       Child Loop BB17_163 Depth 3
                                        #         Child Loop BB17_164 Depth 4
                                        #           Child Loop BB17_165 Depth 5
	movq	img(%rip), %rax
	movq	input(%rip), %rcx
	cmpl	$2, 20(%rax)
	jne	.LBB17_153
# BB#150:                               #   in Loop: Header=BB17_149 Depth=1
	cmpl	$0, 4048(%rcx)
	je	.LBB17_153
# BB#151:                               #   in Loop: Header=BB17_149 Depth=1
	testl	%edi, %edi
	jne	.LBB17_155
# BB#152:                               #   in Loop: Header=BB17_149 Depth=1
	cmpq	$2, %rbp
	je	.LBB17_174
	jmp	.LBB17_155
	.p2align	4, 0x90
.LBB17_153:                             # %._crit_edge680
                                        #   in Loop: Header=BB17_149 Depth=1
	testl	%edi, %edi
	setne	%al
	testq	%rbp, %rbp
	sete	%dl
	cmpl	$1, 4072(%rcx)
	setne	%cl
	orb	%dl, %cl
	cmpq	$2, %rbp
	setne	%dl
	cmpb	$1, %cl
	jne	.LBB17_174
# BB#154:                               # %._crit_edge680
                                        #   in Loop: Header=BB17_149 Depth=1
	orb	%dl, %al
	je	.LBB17_174
.LBB17_155:                             #   in Loop: Header=BB17_149 Depth=1
	cmpl	$3, %ebp
	je	.LBB17_158
# BB#156:                               #   in Loop: Header=BB17_149 Depth=1
	cmpl	$1, %ebp
	jne	.LBB17_159
# BB#157:                               #   in Loop: Header=BB17_149 Depth=1
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	je	.LBB17_159
	jmp	.LBB17_174
.LBB17_158:                             #   in Loop: Header=BB17_149 Depth=1
	cmpb	$0, 3(%rsp)             # 1-byte Folded Reload
	jne	.LBB17_174
.LBB17_159:                             # %.preheader426
                                        #   in Loop: Header=BB17_149 Depth=1
	testl	%esi, %esi
	jle	.LBB17_172
# BB#160:                               # %.preheader426.split.us.preheader
                                        #   in Loop: Header=BB17_149 Depth=1
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%edx, %edx
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB17_161:                             # %.preheader426.split.us
                                        #   Parent Loop BB17_149 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_163 Depth 3
                                        #         Child Loop BB17_164 Depth 4
                                        #           Child Loop BB17_165 Depth 5
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	jle	.LBB17_171
# BB#162:                               # %.preheader425.us.us.preheader
                                        #   in Loop: Header=BB17_161 Depth=2
	movq	imgUV_org(%rip), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rax,%rcx,8), %r12
	movq	$-1, 32(%rsp)           # 8-byte Folded Spill
	leaq	532(%rsp), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB17_163:                             # %.preheader425.us.us
                                        #   Parent Loop BB17_149 Depth=1
                                        #     Parent Loop BB17_161 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB17_164 Depth 4
                                        #           Child Loop BB17_165 Depth 5
	movq	%rcx, %r15
	orq	$3, %r15
	movl	$3, %r13d
	xorl	%r14d, %r14d
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB17_164:                             # %.preheader424.us.us
                                        #   Parent Loop BB17_149 Depth=1
                                        #     Parent Loop BB17_161 Depth=2
                                        #       Parent Loop BB17_163 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB17_165 Depth 5
	movl	%edx, 40(%rsp)          # 4-byte Spill
	movq	%r14, %r9
	orq	$1, %r9
	movq	%r14, %r10
	orq	$3, %r10
	leaq	1(%r9), %r8
	movq	%rbp, %rax
	shlq	$9, %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	shlq	$11, %rdx
	addq	img(%rip), %rdx
	addq	%rax, %rdx
	movq	%rcx, %rax
	shlq	$5, %rax
	addq	%rdx, %rax
	leaq	8528(%rax,%r13,2), %rsi
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB17_165:                             # %.preheader.us.us
                                        #   Parent Loop BB17_149 Depth=1
                                        #     Parent Loop BB17_161 Depth=2
                                        #       Parent Loop BB17_163 Depth=3
                                        #         Parent Loop BB17_164 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movslq	(%rdi), %rax
	movq	(%r12,%rax,8), %r11
	movslq	-4(%rdi), %rbp
	movslq	%edx, %rdx
	leaq	(%rbp,%r14), %rcx
	movzwl	(%r11,%rcx,2), %ecx
	movzwl	-6(%rsi), %eax
	subl	%eax, %ecx
	cmpq	%r10, %r14
	movl	%ecx, diff(,%rdx,4)
	jge	.LBB17_167
# BB#166:                               #   in Loop: Header=BB17_165 Depth=5
	leaq	(%rbp,%r9), %rax
	movzwl	(%r11,%rax,2), %eax
	movzwl	-4(%rsi), %ecx
	subl	%ecx, %eax
	movl	%eax, diff+4(,%rdx,4)
	leaq	(%rbp,%r8), %rax
	movzwl	(%r11,%rax,2), %eax
	movzwl	-2(%rsi), %ecx
	subl	%ecx, %eax
	movl	%eax, diff+8(,%rdx,4)
	addq	%r10, %rbp
	movzwl	(%r11,%rbp,2), %eax
	movzwl	(%rsi), %ecx
	subl	%ecx, %eax
	movl	%eax, diff+12(,%rdx,4)
	addq	$4, %rdx
	jmp	.LBB17_168
	.p2align	4, 0x90
.LBB17_167:                             #   in Loop: Header=BB17_165 Depth=5
	incq	%rdx
.LBB17_168:                             #   in Loop: Header=BB17_165 Depth=5
	incq	%rbx
	addq	$24, %rdi
	addq	$32, %rsi
	cmpq	%r15, %rbx
	jl	.LBB17_165
# BB#169:                               #   in Loop: Header=BB17_164 Depth=4
	movl	$diff, %edi
	callq	distortion4x4
	movl	40(%rsp), %edx          # 4-byte Reload
	addl	%eax, %edx
	addq	$4, %r14
	addq	$4, %r13
	cmpq	96(%rsp), %r14          # 8-byte Folded Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	jl	.LBB17_164
# BB#170:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB17_163 Depth=3
	addq	$4, %rcx
	addq	$4, 32(%rsp)            # 8-byte Folded Spill
	addq	$96, 72(%rsp)           # 8-byte Folded Spill
	movq	80(%rsp), %rsi          # 8-byte Reload
	cmpq	%rsi, %rcx
	jl	.LBB17_163
.LBB17_171:                             # %._crit_edge463.us
                                        #   in Loop: Header=BB17_161 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	$2, %rcx
	movq	184(%rsp), %rbx         # 8-byte Reload
	movl	28(%rsp), %edi          # 4-byte Reload
	jne	.LBB17_161
	jmp	.LBB17_173
.LBB17_172:                             #   in Loop: Header=BB17_149 Depth=1
	xorl	%edx, %edx
.LBB17_173:                             # %.us-lcssa.us
                                        #   in Loop: Header=BB17_149 Depth=1
	movl	60(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, %edx
	movl	16(%rsp), %ecx          # 4-byte Reload
	cmovll	%ebp, %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	cmovlel	%edx, %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
.LBB17_174:                             #   in Loop: Header=BB17_149 Depth=1
	incq	%rbp
	cmpq	$4, %rbp
	jne	.LBB17_149
# BB#175:
	imulq	$536, %rbx, %rax        # imm = 0x218
	movq	304(%rsp), %rcx         # 8-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	%edx, 416(%rcx,%rax)
.LBB17_176:
	addq	$920, %rsp              # imm = 0x398
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	IntraChromaPrediction, .Lfunc_end17-IntraChromaPrediction
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI17_0:
	.quad	.LBB17_47
	.quad	.LBB17_56
	.quad	.LBB17_58
	.quad	.LBB17_61

	.text
	.globl	IntraChromaRDDecision
	.p2align	4, 0x90
	.type	IntraChromaRDDecision,@function
IntraChromaRDDecision:                  # @IntraChromaRDDecision
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi197:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi198:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi199:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi200:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi201:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi202:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi203:
	.cfi_def_cfa_offset 592
.Lcfi204:
	.cfi_offset %rbx, -56
.Lcfi205:
	.cfi_offset %r12, -48
.Lcfi206:
	.cfi_offset %r13, -40
.Lcfi207:
	.cfi_offset %r14, -32
.Lcfi208:
	.cfi_offset %r15, -24
.Lcfi209:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movq	14224(%rax), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movslq	12(%rax), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movslq	15544(%rax), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movslq	15548(%rax), %rcx
	testq	%rcx, %rcx
	movl	%ecx, %r15d
	movq	getNeighbour(%rip), %rax
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	js	.LBB18_1
# BB#2:                                 # %.lr.ph182.preheader
	leal	1(%rcx), %ebx
	leaq	128(%rsp), %r12
	movl	$-1, %r13d
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB18_3:                               # %.lr.ph182
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, %esi
	movl	$1, %ecx
	movl	%ebp, %edi
	movl	%r13d, %edx
	movq	%r12, %r8
	callq	*%rax
	movq	getNeighbour(%rip), %rax
	addq	$24, %r12
	incl	%r13d
	decq	%rbx
	jne	.LBB18_3
	jmp	.LBB18_4
.LBB18_1:
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB18_4:                               # %._crit_edge183
	xorl	%r12d, %r12d
	leaq	104(%rsp), %r8
	xorl	%esi, %esi
	movl	$-1, %edx
	movl	$1, %ecx
	movl	%ebp, %edi
	callq	*%rax
	movl	104(%rsp), %ecx
	movl	128(%rsp), %r9d
	movq	input(%rip), %rax
	cmpl	$0, 272(%rax)
	je	.LBB18_5
# BB#6:
	testl	%ecx, %ecx
	je	.LBB18_8
# BB#7:
	movq	img(%rip), %rax
	movq	14240(%rax), %rax
	movslq	108(%rsp), %rcx
	movl	(%rax,%rcx,4), %r12d
.LBB18_8:
	movl	%r15d, %r10d
	sarl	%r10d
	movl	$1, %ecx
	testl	%r10d, %r10d
	movl	$1, %r14d
	movq	24(%rsp), %r8           # 8-byte Reload
	jle	.LBB18_21
# BB#9:                                 # %.lr.ph177
	movq	img(%rip), %rax
	movl	%r10d, %edx
	xorl	%r14d, %r14d
	testb	$1, %dl
	jne	.LBB18_11
# BB#10:
	movl	$1, %edi
	xorl	%esi, %esi
	cmpl	$1, %r10d
	jne	.LBB18_15
	jmp	.LBB18_21
.LBB18_5:
	movl	%r9d, %ebp
	movl	152(%rsp), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%eax, %r14d
	movq	24(%rsp), %r8           # 8-byte Reload
	jmp	.LBB18_38
.LBB18_11:                              # %._crit_edge.prol
	cmpl	$0, 152(%rsp)
	je	.LBB18_13
# BB#12:
	movq	14240(%rax), %rsi
	movslq	156(%rsp), %rdi
	movl	(%rsi,%rdi,4), %r14d
.LBB18_13:
	andl	$1, %r14d
	movl	$1, %esi
	movl	%r14d, %edi
	cmpl	$1, %r10d
	je	.LBB18_21
.LBB18_15:                              # %.lr.ph177.new
	subq	%rsi, %rdx
	leaq	(%rsi,%rsi,2), %rsi
	leaq	180(%rsp,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB18_16:                              # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	xorl	%r14d, %r14d
	cmpl	$0, -28(%rsi)
	movl	$0, %ebp
	je	.LBB18_18
# BB#17:                                #   in Loop: Header=BB18_16 Depth=1
	movq	14240(%rax), %rbp
	movslq	-24(%rsi), %rbx
	movl	(%rbp,%rbx,4), %ebp
.LBB18_18:                              # %._crit_edge.1249
                                        #   in Loop: Header=BB18_16 Depth=1
	andl	%edi, %ebp
	cmpl	$0, -4(%rsi)
	je	.LBB18_20
# BB#19:                                #   in Loop: Header=BB18_16 Depth=1
	movq	14240(%rax), %rdi
	movslq	(%rsi), %rbx
	movl	(%rdi,%rbx,4), %r14d
.LBB18_20:                              #   in Loop: Header=BB18_16 Depth=1
	andl	%ebp, %r14d
	addq	$48, %rsi
	addq	$-2, %rdx
	movl	%r14d, %edi
	jne	.LBB18_16
.LBB18_21:                              # %.preheader143
	cmpl	%r15d, %r10d
	jge	.LBB18_34
# BB#22:                                # %.lr.ph171
	movq	img(%rip), %rbx
	movslq	%r10d, %rdx
	movl	%r8d, %esi
	subl	%r10d, %esi
	leaq	-1(%r8), %rax
	testb	$1, %sil
	jne	.LBB18_24
# BB#23:
	movl	$1, %esi
                                        # implicit-def: %ECX
	movq	%rdx, %rdi
	cmpq	%rdx, %rax
	jne	.LBB18_28
	jmp	.LBB18_34
.LBB18_24:
	leaq	1(%rdx), %rdi
	leaq	(%rdi,%rdi,2), %rsi
	cmpl	$0, 128(%rsp,%rsi,8)
	je	.LBB18_25
# BB#26:
	movq	14240(%rbx), %rbp
	movslq	132(%rsp,%rsi,8), %rsi
	movl	(%rbp,%rsi,4), %ecx
	andl	$1, %ecx
	movl	%ecx, %esi
	cmpq	%rdx, %rax
	jne	.LBB18_28
	jmp	.LBB18_34
.LBB18_25:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	cmpq	%rdx, %rax
	je	.LBB18_34
.LBB18_28:                              # %.lr.ph171.new
	movq	%r8, %rax
	subq	%rdi, %rax
	leaq	(%rdi,%rdi,2), %rdx
	leaq	180(%rsp,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB18_29:                              # =>This Inner Loop Header: Depth=1
	xorl	%ecx, %ecx
	cmpl	$0, -28(%rdx)
	movl	$0, %edi
	je	.LBB18_31
# BB#30:                                #   in Loop: Header=BB18_29 Depth=1
	movq	14240(%rbx), %rdi
	movslq	-24(%rdx), %rbp
	movl	(%rdi,%rbp,4), %edi
.LBB18_31:                              #   in Loop: Header=BB18_29 Depth=1
	andl	%esi, %edi
	cmpl	$0, -4(%rdx)
	je	.LBB18_33
# BB#32:                                #   in Loop: Header=BB18_29 Depth=1
	movq	14240(%rbx), %rsi
	movslq	(%rdx), %rbp
	movl	(%rsi,%rbp,4), %ecx
.LBB18_33:                              #   in Loop: Header=BB18_29 Depth=1
	andl	%edi, %ecx
	addq	$48, %rdx
	addq	$-2, %rax
	movl	%ecx, %esi
	jne	.LBB18_29
.LBB18_34:                              # %._crit_edge172
	testl	%r9d, %r9d
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	je	.LBB18_35
# BB#36:
	movq	img(%rip), %rax
	movq	14240(%rax), %rax
	movslq	132(%rsp), %rcx
	movl	(%rax,%rcx,4), %ebp
	jmp	.LBB18_37
.LBB18_35:
	xorl	%ebp, %ebp
.LBB18_37:
	movl	%r12d, %ecx
.LBB18_38:
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	testl	%r8d, %r8d
	movq	16(%rsp), %rbx          # 8-byte Reload
	jle	.LBB18_41
# BB#39:                                # %.lr.ph167.preheader
	leaq	128(%rsp), %r12
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB18_40:                              # %.lr.ph167
                                        # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	movl	$1, %ecx
	movl	%ebx, %edi
	movl	%r13d, %edx
	movq	%r12, %r8
	callq	*getNeighbour(%rip)
	incq	%r13
	addq	$24, %r12
	cmpq	%r13, %r15
	jne	.LBB18_40
.LBB18_41:                              # %._crit_edge168
	movq	img(%rip), %rax
	cmpl	$0, 15268(%rax)
	movl	%ebp, %edi
	movl	8(%rsp), %ebp           # 4-byte Reload
	je	.LBB18_51
# BB#42:
	cmpl	$0, 14464(%rax)
	je	.LBB18_51
# BB#43:
	testl	%r15d, %r15d
	jle	.LBB18_51
# BB#44:                                # %.lr.ph.preheader
	leaq	-1(%r15), %rax
	movq	%r15, %rdx
	andq	$3, %rdx
	je	.LBB18_45
# BB#46:                                # %.lr.ph.prol.preheader
	leaq	148(%rsp), %rsi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB18_47:                              # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	sarl	(%rsi)
	incq	%rcx
	addq	$24, %rsi
	cmpq	%rcx, %rdx
	jne	.LBB18_47
	jmp	.LBB18_48
.LBB18_45:
	xorl	%ecx, %ecx
.LBB18_48:                              # %.lr.ph.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB18_51
# BB#49:                                # %.lr.ph.preheader.new
	movq	%r15, %rax
	subq	%rcx, %rax
	leaq	(%rcx,%rcx,2), %rcx
	leaq	220(%rsp,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB18_50:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	sarl	-72(%rcx)
	sarl	-48(%rcx)
	sarl	-24(%rcx)
	sarl	(%rcx)
	addq	$96, %rcx
	addq	$-4, %rax
	jne	.LBB18_50
.LBB18_51:                              # %.preheader141
	movl	36(%rsp), %edx          # 4-byte Reload
	testl	%edx, %edx
	sete	%al
	testl	%r14d, %r14d
	sete	%cl
	testl	%ebp, %ebp
	sete	%bl
	orb	%bl, %al
	orb	%cl, %bl
	movb	%bl, 3(%rsp)            # 1-byte Spill
	testl	%edi, %edi
	sete	%bl
	orb	%cl, %bl
	movl	%edx, %ecx
	orb	%al, %bl
	movb	%bl, 2(%rsp)            # 1-byte Spill
	xorl	%esi, %esi
	movl	$2147483647, 4(%rsp)    # 4-byte Folded Spill
                                        # imm = 0x7FFFFFFF
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r15, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB18_52:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_59 Depth 2
                                        #       Child Loop BB18_73 Depth 3
                                        #       Child Loop BB18_62 Depth 3
                                        #         Child Loop BB18_68 Depth 4
                                        #           Child Loop BB18_65 Depth 5
	testl	%ecx, %ecx
	jne	.LBB18_54
# BB#53:                                #   in Loop: Header=BB18_52 Depth=1
	cmpq	$2, %rsi
	je	.LBB18_78
.LBB18_54:                              #   in Loop: Header=BB18_52 Depth=1
	cmpl	$3, %esi
	je	.LBB18_57
# BB#55:                                #   in Loop: Header=BB18_52 Depth=1
	cmpl	$1, %esi
	jne	.LBB18_58
# BB#56:                                #   in Loop: Header=BB18_52 Depth=1
	cmpb	$0, 3(%rsp)             # 1-byte Folded Reload
	je	.LBB18_58
	jmp	.LBB18_78
.LBB18_57:                              #   in Loop: Header=BB18_52 Depth=1
	cmpb	$0, 2(%rsp)             # 1-byte Folded Reload
	jne	.LBB18_78
.LBB18_58:                              # %.preheader140.preheader
                                        #   in Loop: Header=BB18_52 Depth=1
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB18_59:                              # %.preheader140
                                        #   Parent Loop BB18_52 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB18_73 Depth 3
                                        #       Child Loop BB18_62 Depth 3
                                        #         Child Loop BB18_68 Depth 4
                                        #           Child Loop BB18_65 Depth 5
	testl	%r15d, %r15d
	jle	.LBB18_75
# BB#60:                                # %.preheader139.lr.ph
                                        #   in Loop: Header=BB18_59 Depth=2
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jle	.LBB18_72
# BB#61:                                # %.preheader139.us.preheader
                                        #   in Loop: Header=BB18_59 Depth=2
	movq	imgUV_org(%rip), %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	(%rcx,%rdx,8), %r15
	movq	$-1, 64(%rsp)           # 8-byte Folded Spill
	leaq	148(%rsp), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB18_62:                              # %.preheader139.us
                                        #   Parent Loop BB18_52 Depth=1
                                        #     Parent Loop BB18_59 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB18_68 Depth 4
                                        #           Child Loop BB18_65 Depth 5
	movq	%rdi, %r12
	orq	$3, %r12
	movl	$3, %r13d
	xorl	%r14d, %r14d
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB18_68:                              # %.preheader138.us
                                        #   Parent Loop BB18_52 Depth=1
                                        #     Parent Loop BB18_59 Depth=2
                                        #       Parent Loop BB18_62 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB18_65 Depth 5
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	%r14, %r9
	orq	$1, %r9
	movq	%r14, %r10
	orq	$3, %r10
	leaq	1(%r9), %r8
	movq	%rsi, %rax
	shlq	$9, %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	shlq	$11, %rcx
	addq	img(%rip), %rcx
	addq	%rax, %rcx
	movq	%rdi, %rax
	shlq	$5, %rax
	addq	%rcx, %rax
	leaq	8528(%rax,%r13,2), %rsi
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB18_65:                              # %.preheader.us
                                        #   Parent Loop BB18_52 Depth=1
                                        #     Parent Loop BB18_59 Depth=2
                                        #       Parent Loop BB18_62 Depth=3
                                        #         Parent Loop BB18_68 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movslq	(%rdi), %rcx
	movq	(%r15,%rcx,8), %r11
	movslq	-4(%rdi), %rcx
	cltq
	leaq	(%rcx,%r14), %rbp
	movzwl	(%r11,%rbp,2), %ebp
	movzwl	-6(%rsi), %ebx
	subl	%ebx, %ebp
	cmpq	%r10, %r14
	movl	%ebp, diff(,%rax,4)
	jge	.LBB18_66
# BB#63:                                #   in Loop: Header=BB18_65 Depth=5
	leaq	(%rcx,%r9), %rbx
	movzwl	(%r11,%rbx,2), %ebx
	movzwl	-4(%rsi), %ebp
	subl	%ebp, %ebx
	movl	%ebx, diff+4(,%rax,4)
	leaq	(%rcx,%r8), %rbx
	movzwl	(%r11,%rbx,2), %ebx
	movzwl	-2(%rsi), %ebp
	subl	%ebp, %ebx
	movl	%ebx, diff+8(,%rax,4)
	addq	%r10, %rcx
	movzwl	(%r11,%rcx,2), %ecx
	movzwl	(%rsi), %ebp
	subl	%ebp, %ecx
	movl	%ecx, diff+12(,%rax,4)
	addq	$4, %rax
	jmp	.LBB18_64
	.p2align	4, 0x90
.LBB18_66:                              #   in Loop: Header=BB18_65 Depth=5
	incq	%rax
.LBB18_64:                              #   in Loop: Header=BB18_65 Depth=5
	incq	%rdx
	addq	$32, %rsi
	addq	$24, %rdi
	cmpq	%r12, %rdx
	jl	.LBB18_65
# BB#67:                                #   in Loop: Header=BB18_68 Depth=4
	movl	$diff, %edi
	callq	distortion4x4
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	addq	$4, %r14
	addq	$4, %r13
	cmpq	48(%rsp), %r14          # 8-byte Folded Reload
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	96(%rsp), %rdi          # 8-byte Reload
	jl	.LBB18_68
# BB#69:                                # %._crit_edge.us
                                        #   in Loop: Header=BB18_62 Depth=3
	cmpl	4(%rsp), %eax           # 4-byte Folded Reload
	jg	.LBB18_70
# BB#71:                                # %._crit_edge.us
                                        #   in Loop: Header=BB18_62 Depth=3
	addq	$4, %rdi
	addq	$4, 64(%rsp)            # 8-byte Folded Spill
	addq	$96, 56(%rsp)           # 8-byte Folded Spill
	movq	24(%rsp), %rdx          # 8-byte Reload
	cmpq	%rdx, %rdi
	jl	.LBB18_62
	jmp	.LBB18_75
	.p2align	4, 0x90
.LBB18_72:                              # %.preheader139.preheader
                                        #   in Loop: Header=BB18_59 Depth=2
	movl	$4, %ecx
	.p2align	4, 0x90
.LBB18_73:                              # %.preheader139
                                        #   Parent Loop BB18_52 Depth=1
                                        #     Parent Loop BB18_59 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	4(%rsp), %eax           # 4-byte Folded Reload
	jg	.LBB18_75
# BB#74:                                # %.preheader139
                                        #   in Loop: Header=BB18_73 Depth=3
	cmpl	%edx, %ecx
	leal	4(%rcx), %ecx
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	jl	.LBB18_73
	jmp	.LBB18_75
.LBB18_70:                              #   in Loop: Header=BB18_59 Depth=2
	movq	24(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB18_75:                              # %._crit_edge154
                                        #   in Loop: Header=BB18_59 Depth=2
	cmpl	4(%rsp), %eax           # 4-byte Folded Reload
	movq	80(%rsp), %r15          # 8-byte Reload
	jg	.LBB18_77
# BB#76:                                # %._crit_edge154
                                        #   in Loop: Header=BB18_59 Depth=2
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rdi
	incq	%rdi
	movq	%rdi, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmpq	$2, %rdi
	jl	.LBB18_59
.LBB18_77:                              #   in Loop: Header=BB18_52 Depth=1
	movq	mvbits(%rip), %rcx
	cvtsi2sdl	(%rcx,%rsi,4), %xmm0
	leaq	592(%rsp), %rcx
	mulsd	24(%rcx), %xmm0
	cvttsd2si	%xmm0, %ecx
	addl	%eax, %ecx
	movl	4(%rsp), %eax           # 4-byte Reload
	cmpl	%eax, %ecx
	movl	12(%rsp), %edi          # 4-byte Reload
	cmovll	%esi, %edi
	movl	%edi, 12(%rsp)          # 4-byte Spill
	cmovlel	%ecx, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	36(%rsp), %ecx          # 4-byte Reload
.LBB18_78:                              #   in Loop: Header=BB18_52 Depth=1
	incq	%rsi
	cmpq	$4, %rsi
	jne	.LBB18_52
# BB#79:
	imulq	$536, %rax, %rax        # imm = 0x218
	movq	72(%rsp), %rcx          # 8-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	%edx, 416(%rcx,%rax)
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	IntraChromaRDDecision, .Lfunc_end18-IntraChromaRDDecision
	.cfi_endproc

	.globl	ZeroRef
	.p2align	4, 0x90
	.type	ZeroRef,@function
ZeroRef:                                # @ZeroRef
	.cfi_startproc
# BB#0:                                 # %.lr.ph16
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rax
	movslq	168(%rcx), %rdx
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	(%rcx), %r9
	leaq	3(%rdx), %rsi
	decq	%rdx
	leaq	3(%rax), %r8
.LBB19_1:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_3 Depth 2
	movq	(%r9,%rax,8), %rdi
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB19_3:                               #   Parent Loop BB19_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, 1(%rdi,%rcx)
	jne	.LBB19_4
# BB#2:                                 #   in Loop: Header=BB19_3 Depth=2
	incq	%rcx
	cmpq	%rsi, %rcx
	jl	.LBB19_3
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB19_1 Depth=1
	cmpq	%r8, %rax
	leaq	1(%rax), %rax
	jl	.LBB19_1
# BB#6:
	movl	$1, %eax
	retq
.LBB19_4:
	xorl	%eax, %eax
	retq
.Lfunc_end19:
	.size	ZeroRef, .Lfunc_end19-ZeroRef
	.cfi_endproc

	.globl	MBType2Value
	.p2align	4, 0x90
	.type	MBType2Value,@function
MBType2Value:                           # @MBType2Value
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rcx
	movl	20(%rcx), %esi
	cmpl	$1, %esi
	movl	72(%rdi), %edx
	jne	.LBB20_1
# BB#14:
	testl	%edx, %edx
	je	.LBB20_15
# BB#16:
	movl	%edx, %esi
	orl	$4, %esi
	movl	$23, %eax
	cmpl	$13, %esi
	je	.LBB20_25
# BB#17:
	movslq	392(%rdi), %r8
	movslq	404(%rdi), %rsi
	leal	-1(%rdx), %edi
	cmpl	$13, %edi
	ja	.LBB20_22
# BB#18:
	movl	$48, %eax
	jmpq	*.LJTI20_1(,%rdi,8)
.LBB20_21:
	movl	MBType2Value.dir1offset(,%r8,4), %eax
	retq
.LBB20_1:
	leal	-8(%rdx), %edi
	movl	%edx, %eax
	cmpl	$6, %edi
	ja	.LBB20_25
# BB#2:
	jmpq	*.LJTI20_0(,%rdi,8)
.LBB20_3:
	xorl	%ecx, %ecx
	cmpl	$2, %esi
	movl	$6, %eax
	cmovel	%ecx, %eax
	retq
.LBB20_15:
	xorl	%eax, %eax
	retq
.LBB20_6:
	movq	input(%rip), %rax
	cmpl	$0, 4008(%rax)
	je	.LBB20_8
# BB#7:
	movl	$4, %eax
	retq
.LBB20_4:
	xorl	%edx, %edx
	cmpl	$2, %esi
	movl	$6, %eax
	cmovel	%edx, %eax
	addl	15244(%rcx), %eax
	retq
.LBB20_5:
	cmpl	$2, %esi
	movl	$25, %ecx
	movl	$31, %eax
	cmovel	%ecx, %eax
	retq
.LBB20_22:
	leaq	(%r8,%r8,2), %rax
	shlq	$2, %rsi
	movl	MBType2Value.dir2offset(%rsi,%rax,4), %eax
	cmpl	$2, %edx
	jne	.LBB20_24
# BB#23:
	addl	$4, %eax
	retq
.LBB20_20:
	movl	$22, %eax
	retq
.LBB20_19:
	movl	15244(%rcx), %eax
	addl	$23, %eax
	retq
.LBB20_8:
	movslq	172(%rcx), %rdx
	movslq	168(%rcx), %rsi
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	(%rax), %r9
	leaq	3(%rsi), %rdi
	decq	%rsi
	leaq	3(%rdx), %r8
.LBB20_9:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_11 Depth 2
	movq	(%r9,%rdx,8), %rax
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB20_11:                              #   Parent Loop BB20_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, 1(%rax,%rcx)
	jne	.LBB20_12
# BB#10:                                #   in Loop: Header=BB20_11 Depth=2
	incq	%rcx
	cmpq	%rdi, %rcx
	jl	.LBB20_11
# BB#13:                                # %._crit_edge.i
                                        #   in Loop: Header=BB20_9 Depth=1
	movl	$5, %eax
	cmpq	%r8, %rdx
	leaq	1(%rdx), %rdx
	jl	.LBB20_9
	jmp	.LBB20_25
.LBB20_12:
	movl	$4, %eax
	retq
.LBB20_24:
	addl	$5, %eax
.LBB20_25:                              # %ZeroRef.exit
	retq
.Lfunc_end20:
	.size	MBType2Value, .Lfunc_end20-MBType2Value
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI20_0:
	.quad	.LBB20_6
	.quad	.LBB20_3
	.quad	.LBB20_4
	.quad	.LBB20_25
	.quad	.LBB20_25
	.quad	.LBB20_3
	.quad	.LBB20_5
.LJTI20_1:
	.quad	.LBB20_21
	.quad	.LBB20_22
	.quad	.LBB20_22
	.quad	.LBB20_22
	.quad	.LBB20_22
	.quad	.LBB20_22
	.quad	.LBB20_22
	.quad	.LBB20_20
	.quad	.LBB20_22
	.quad	.LBB20_19
	.quad	.LBB20_22
	.quad	.LBB20_22
	.quad	.LBB20_22
	.quad	.LBB20_25

	.text
	.globl	writeIntra4x4Modes
	.p2align	4, 0x90
	.type	writeIntra4x4Modes,@function
writeIntra4x4Modes:                     # @writeIntra4x4Modes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi210:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi211:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi212:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi213:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi214:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi215:
	.cfi_def_cfa_offset 96
.Lcfi216:
	.cfi_offset %rbx, -48
.Lcfi217:
	.cfi_offset %r12, -40
.Lcfi218:
	.cfi_offset %r14, -32
.Lcfi219:
	.cfi_offset %r15, -24
.Lcfi220:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movq	14224(%rax), %rcx
	movslq	12(%rax), %rdx
	movq	14216(%rax), %rax
	movq	input(%rip), %rsi
	movslq	4016(%rsi), %rsi
	movq	assignSE2partition(,%rsi,8), %rsi
	movslq	16(%rsi), %rsi
	imulq	$104, %rsi, %r15
	addq	24(%rax), %r15
	imulq	$536, %rdx, %rax        # imm = 0x218
	movl	$1, 420(%rcx,%rax)
	leaq	44(%rcx,%rax), %r12
	xorl	%ebx, %ebx
	leaq	8(%rsp), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB21_1:                               # =>This Inner Loop Header: Depth=1
	movl	%ebx, 32(%rsp)
	movsbl	288(%r12,%rbx), %eax
	movl	%eax, 12(%rsp)
	movl	$0, 16(%rsp)
	movl	$4, 8(%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	*writeIntraPredMode(%rip)
	movl	20(%rsp), %eax
	addl	%eax, (%r12)
	addl	%eax, %ebp
	incq	%rbx
	cmpq	$16, %rbx
	jne	.LBB21_1
# BB#2:
	movl	%ebp, %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	writeIntra4x4Modes, .Lfunc_end21-writeIntra4x4Modes
	.cfi_endproc

	.globl	writeIntra8x8Modes
	.p2align	4, 0x90
	.type	writeIntra8x8Modes,@function
writeIntra8x8Modes:                     # @writeIntra8x8Modes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi221:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi222:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi223:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi224:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi225:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi226:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi227:
	.cfi_def_cfa_offset 96
.Lcfi228:
	.cfi_offset %rbx, -56
.Lcfi229:
	.cfi_offset %r12, -48
.Lcfi230:
	.cfi_offset %r13, -40
.Lcfi231:
	.cfi_offset %r14, -32
.Lcfi232:
	.cfi_offset %r15, -24
.Lcfi233:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movq	14224(%rax), %r12
	movslq	12(%rax), %rcx
	movq	14216(%rax), %rax
	movq	input(%rip), %rdx
	movslq	4016(%rdx), %rdx
	movq	assignSE2partition(,%rdx,8), %rdx
	movslq	16(%rdx), %rdx
	imulq	$104, %rdx, %r15
	addq	24(%rax), %r15
	imulq	$536, %rcx, %rbx        # imm = 0x218
	movl	$1, 420(%r12,%rbx)
	movl	$0, 24(%rsp)
	movsbl	348(%r12,%rbx), %eax
	movl	%eax, 4(%rsp)
	movl	$0, 8(%rsp)
	movl	$4, (%rsp)
	movq	%rsp, %r14
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	*writeIntraPredMode(%rip)
	movl	12(%rsp), %r13d
	addl	%r13d, 44(%r12,%rbx)
	movl	$4, 24(%rsp)
	movsbl	352(%r12,%rbx), %eax
	movl	%eax, 4(%rsp)
	movl	$0, 8(%rsp)
	movl	$4, (%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	*writeIntraPredMode(%rip)
	movl	12(%rsp), %ebp
	addl	%ebp, 44(%r12,%rbx)
	addl	%r13d, %ebp
	movl	$8, 24(%rsp)
	movsbl	356(%r12,%rbx), %eax
	movl	%eax, 4(%rsp)
	movl	$0, 8(%rsp)
	movl	$4, (%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	*writeIntraPredMode(%rip)
	movl	12(%rsp), %eax
	addl	%eax, 44(%r12,%rbx)
	addl	%eax, %ebp
	movl	$12, 24(%rsp)
	movsbl	360(%r12,%rbx), %eax
	movl	%eax, 4(%rsp)
	movl	$0, 8(%rsp)
	movl	$4, (%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	*writeIntraPredMode(%rip)
	movl	12(%rsp), %eax
	addl	%eax, 44(%r12,%rbx)
	addl	%eax, %ebp
	movl	%ebp, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	writeIntra8x8Modes, .Lfunc_end22-writeIntra8x8Modes
	.cfi_endproc

	.globl	writeIntraModes
	.p2align	4, 0x90
	.type	writeIntraModes,@function
writeIntraModes:                        # @writeIntraModes
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rdx
	movq	14224(%rdx), %rax
	movslq	12(%rdx), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	movl	72(%rax,%rcx), %esi
	cmpl	$13, %esi
	je	.LBB23_5
# BB#1:
	pushq	%rbp
.Lcfi234:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi235:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi236:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi237:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi238:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi239:
	.cfi_def_cfa_offset 96
.Lcfi240:
	.cfi_offset %rbx, -48
.Lcfi241:
	.cfi_offset %r12, -40
.Lcfi242:
	.cfi_offset %r14, -32
.Lcfi243:
	.cfi_offset %r15, -24
.Lcfi244:
	.cfi_offset %rbp, -16
	xorl	%ebp, %ebp
	cmpl	$9, %esi
	jne	.LBB23_4
# BB#2:
	movq	14216(%rdx), %rdx
	movq	input(%rip), %rsi
	movslq	4016(%rsi), %rsi
	movq	assignSE2partition(,%rsi,8), %rsi
	movslq	16(%rsi), %rsi
	imulq	$104, %rsi, %r14
	addq	24(%rdx), %r14
	movl	$1, 420(%rax,%rcx)
	leaq	44(%rax,%rcx), %r12
	xorl	%ebx, %ebx
	leaq	8(%rsp), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB23_3:                               # =>This Inner Loop Header: Depth=1
	movl	%ebx, 32(%rsp)
	movsbl	288(%r12,%rbx), %eax
	movl	%eax, 12(%rsp)
	movl	$0, 16(%rsp)
	movl	$4, 8(%rsp)
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	*writeIntraPredMode(%rip)
	movl	20(%rsp), %eax
	addl	%eax, (%r12)
	addl	%eax, %ebp
	incq	%rbx
	cmpq	$16, %rbx
	jne	.LBB23_3
.LBB23_4:
	movl	%ebp, %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_5:
	jmp	writeIntra8x8Modes      # TAILCALL
.Lfunc_end23:
	.size	writeIntraModes, .Lfunc_end23-writeIntraModes
	.cfi_endproc

	.globl	B8Mode2Value
	.p2align	4, 0x90
	.type	B8Mode2Value,@function
B8Mode2Value:                           # @B8Mode2Value
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	jne	.LBB24_1
# BB#2:
	movslq	%edi, %rax
	imull	B8Mode2Value.b8inc(,%rax,4), %esi
	addl	B8Mode2Value.b8start(,%rax,4), %esi
	movl	%esi, %edi
	movl	%edi, %eax
	retq
.LBB24_1:
	addl	$-4, %edi
	movl	%edi, %eax
	retq
.Lfunc_end24:
	.size	B8Mode2Value, .Lfunc_end24-B8Mode2Value
	.cfi_endproc

	.globl	writeMBLayer
	.p2align	4, 0x90
	.type	writeMBLayer,@function
writeMBLayer:                           # @writeMBLayer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi245:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi246:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi247:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi248:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi249:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi250:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi251:
	.cfi_def_cfa_offset 240
.Lcfi252:
	.cfi_offset %rbx, -56
.Lcfi253:
	.cfi_offset %r12, -48
.Lcfi254:
	.cfi_offset %r13, -40
.Lcfi255:
	.cfi_offset %r14, -32
.Lcfi256:
	.cfi_offset %r15, -24
.Lcfi257:
	.cfi_offset %rbp, -16
	movq	%rsi, 176(%rsp)         # 8-byte Spill
	movl	%edi, %ebp
	movq	img(%rip), %rax
	movslq	12(%rax), %rbx
	movl	%ebx, %edi
	callq	FmoGetPreviousMBNr
	movq	img(%rip), %rdx
	movq	14224(%rdx), %r13
	imulq	$536, %rbx, %r14        # imm = 0x218
	cltq
	imulq	$536, %rax, %rcx        # imm = 0x218
	leaq	(%r13,%rcx), %rdi
	testq	%rbx, %rbx
	cmoveq	%rbx, %rdi
	movq	14216(%rdx), %r11
	movq	input(%rip), %r9
	movslq	4016(%r9), %rax
	movq	assignSE2partition(,%rax,8), %rsi
	movl	72(%r13,%r14), %r15d
	xorl	%r8d, %r8d
	testl	%r15d, %r15d
	movl	$0, %eax
	jne	.LBB25_3
# BB#1:
	movl	$1, %eax
	cmpl	$1, 20(%rdx)
	jne	.LBB25_3
# BB#2:
	xorl	%eax, %eax
	cmpl	$0, 364(%r13,%r14)
	sete	%al
.LBB25_3:
	movl	15268(%rdx), %r10d
	testl	%r10d, %r10d
	movl	%ebp, 152(%rsp)         # 4-byte Spill
	je	.LBB25_10
# BB#4:
	movl	$1, %ebp
	testb	$1, %bl
	je	.LBB25_11
# BB#5:
	xorl	%ebp, %ebp
	cmpl	$0, 72(%rdi)
	jne	.LBB25_9
# BB#6:
	cmpl	$1, 20(%rdx)
	jne	.LBB25_8
# BB#7:
	cmpl	$0, 364(%rdi)
	jne	.LBB25_9
.LBB25_8:
	movl	$1, %ebp
.LBB25_9:
	movl	528(%r13,%rcx), %r8d
	jmp	.LBB25_11
.LBB25_10:
	xorl	%ebp, %ebp
.LBB25_11:
	movl	%eax, 40(%rsp)          # 4-byte Spill
	leaq	(%r13,%r14), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	72(%r13,%r14), %rdi
	leal	-9(%r15), %ecx
	cmpl	$6, %ecx
	sbbb	%bl, %bl
	movb	$51, %al
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %al
	andb	%bl, %al
	andb	$1, %al
	movzbl	%al, %eax
	movl	%eax, 420(%r13,%r14)
	movq	24(%r11), %rbx
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	movslq	8(%rsi), %rax
	imulq	$104, %rax, %rax
	leaq	(%rbx,%rax), %r12
	movl	20(%rdx), %ecx
	cmpl	$2, %ecx
	movq	%r11, 48(%rsp)          # 8-byte Spill
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	movq	%rax, 168(%rsp)         # 8-byte Spill
	jne	.LBB25_14
# BB#12:
	movq	%rdi, %r15
	testl	%ebp, %ebp
	je	.LBB25_28
# BB#13:
	movl	424(%r13,%r14), %eax
	movl	%eax, 60(%rsp)
	movl	$0, 64(%rsp)
	movl	$2, 56(%rsp)
	leaq	56(%rsp), %rdi
	movq	%r12, %rsi
	callq	*writeFieldModeInfo(%rip)
	movl	68(%rsp), %ebp
	leaq	32(%r13,%r14), %rbx
	addl	%ebp, 32(%r13,%r14)
	jmp	.LBB25_29
.LBB25_14:
	cmpl	$1, 4008(%r9)
	jne	.LBB25_23
# BB#15:
	movq	%rdi, %r15
	testl	%r10d, %r10d
	je	.LBB25_19
# BB#16:
	testl	%r8d, %r8d
	jne	.LBB25_18
# BB#17:
	movl	12(%rdx), %eax
	andl	$1, %eax
	jne	.LBB25_19
.LBB25_18:
	movl	424(%r13,%r14), %ebx
	callq	field_flag_inference
	movl	%eax, 424(%r13,%r14)
	callq	CheckAvailabilityOfNeighborsCABAC
	movl	%ebx, 424(%r13,%r14)
.LBB25_19:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	MBType2Value
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	%eax, 60(%rsp)
	movl	364(%r13,%r14), %eax
	movl	%eax, 64(%rsp)
	movl	$2, 56(%rsp)
	leaq	56(%rsp), %rdi
	movq	%r12, %rsi
	callq	writeMB_skip_flagInfo_CABAC
	movl	68(%rsp), %eax
	leaq	32(%r13,%r14), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	%eax, %ebx
	addl	%eax, 32(%r13,%r14)
	callq	CheckAvailabilityOfNeighborsCABAC
	testl	%ebp, %ebp
	je	.LBB25_41
# BB#20:
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	movq	%r15, %rdi
	jne	.LBB25_43
# BB#21:
	movq	img(%rip), %rax
	movl	15268(%rax), %eax
	testl	%eax, %eax
	je	.LBB25_43
# BB#22:
	movl	424(%r13,%r14), %eax
	movl	%eax, 60(%rsp)
	movl	$0, 64(%rsp)
	movl	$2, 56(%rsp)
	leaq	56(%rsp), %rdi
	movq	%r12, %rsi
	callq	*writeFieldModeInfo(%rip)
	movq	%r15, %rdi
	movl	68(%rsp), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	addl	%eax, (%rcx)
	movl	%ebx, %ebp
	addl	%eax, %ebp
	cmpl	$0, (%rdi)
	jne	.LBB25_46
	jmp	.LBB25_44
.LBB25_23:
	testl	%r15d, %r15d
	je	.LBB25_30
.LBB25_24:
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movl	144(%rdx), %eax
	movl	%eax, 60(%rsp)
	movl	$0, 64(%rsp)
	movl	$2, 56(%rsp)
	leaq	56(%rsp), %rdi
	movq	%r12, %rsi
	callq	writeSE_UVLC
	movl	68(%rsp), %eax
	movl	%eax, %ecx
	addl	%eax, 32(%r13,%r14)
	movq	%r14, 32(%rsp)          # 8-byte Spill
	leaq	32(%r13,%r14), %r14
	movq	img(%rip), %rbx
	movl	$0, 144(%rbx)
	testl	%ebp, %ebp
	je	.LBB25_37
# BB#25:
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jne	.LBB25_37
# BB#26:
	movl	15268(%rbx), %eax
	testl	%eax, %eax
	movl	%ecx, %ebp
	je	.LBB25_38
# BB#27:
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	424(%r13,%rax), %eax
	movl	%eax, 60(%rsp)
	movl	$2, 56(%rsp)
	leaq	56(%rsp), %rdi
	movq	%r12, %rsi
	callq	writeSE_Flag
	movl	68(%rsp), %eax
	addl	%eax, (%r14)
	addl	%eax, %ebp
	movq	img(%rip), %rbx
	jmp	.LBB25_38
.LBB25_28:                              # %._crit_edge284
	leaq	32(%r13,%r14), %rbx
	xorl	%ebp, %ebp
.LBB25_29:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	MBType2Value
	movl	%eax, 60(%rsp)
	movl	$0, 64(%rsp)
	movl	$2, 56(%rsp)
	leaq	56(%rsp), %rdi
	movq	%r12, %rsi
	callq	*writeMB_typeInfo(%rip)
	movl	68(%rsp), %eax
	addl	%eax, (%rbx)
	addl	%eax, %ebp
	movq	%r15, %rdi
	jmp	.LBB25_47
.LBB25_30:
	cmpl	$1, %ecx
	jne	.LBB25_32
# BB#31:
	cmpl	$0, 364(%r13,%r14)
	jne	.LBB25_24
.LBB25_32:
	movq	%rdi, %rbx
	incl	144(%rdx)
	movl	$1, 528(%r13,%r14)
	cmpl	$-4, 15528(%rdx)
	jle	.LBB25_35
# BB#33:                                # %.preheader233.lr.ph
	movq	152(%rdx), %rax
	movq	$-1, %rcx
	.p2align	4, 0x90
.LBB25_34:                              # %.preheader233
                                        # =>This Inner Loop Header: Depth=1
	movslq	12(%rdx), %rsi
	movq	(%rax,%rsi,8), %rsi
	movq	(%rsi), %rsi
	movl	$0, 4(%rsi,%rcx,4)
	movslq	12(%rdx), %rsi
	movq	(%rax,%rsi,8), %rsi
	movq	8(%rsi), %rsi
	movl	$0, 4(%rsi,%rcx,4)
	movslq	12(%rdx), %rsi
	movq	(%rax,%rsi,8), %rsi
	movq	16(%rsi), %rsi
	movl	$0, 4(%rsi,%rcx,4)
	movslq	12(%rdx), %rsi
	movq	(%rax,%rsi,8), %rsi
	movq	24(%rsi), %rsi
	movl	$0, 4(%rsi,%rcx,4)
	movslq	15528(%rdx), %rsi
	addq	$3, %rsi
	incq	%rcx
	cmpq	%rsi, %rcx
	jl	.LBB25_34
.LBB25_35:                              # %._crit_edge259
	movl	12(%rdx), %edi
	callq	FmoGetNextMBNr
	xorl	%ebp, %ebp
	cmpl	$-1, %eax
	je	.LBB25_185
# BB#36:
	movq	%rbx, %rdi
	jmp	.LBB25_47
.LBB25_37:
	movl	%ecx, %ebp
.LBB25_38:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	MBType2Value
	movl	%eax, 60(%rsp)
	cmpl	$1, 20(%rbx)
	je	.LBB25_40
# BB#39:
	decl	%eax
	movl	%eax, 60(%rsp)
.LBB25_40:
	movl	$2, 56(%rsp)
	movl	$0, 64(%rsp)
	leaq	56(%rsp), %rdi
	movq	%r12, %rsi
	callq	*writeMB_typeInfo(%rip)
	movl	68(%rsp), %eax
	addl	%eax, (%r14)
	addl	%eax, %ebp
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	jmp	.LBB25_47
.LBB25_41:
	movl	%ebx, %ebp
	movq	%r15, %rdi
	cmpl	$0, (%rdi)
	jne	.LBB25_46
	jmp	.LBB25_44
.LBB25_43:
	movl	%ebx, %ebp
	cmpl	$0, (%rdi)
	jne	.LBB25_46
.LBB25_44:
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	jne	.LBB25_47
# BB#45:
	leaq	364(%r13,%r14), %rax
	cmpl	$0, (%rax)
	je	.LBB25_47
.LBB25_46:
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	%eax, 60(%rsp)
	movl	$0, 64(%rsp)
	movl	$2, 56(%rsp)
	leaq	56(%rsp), %rdi
	movq	%r12, %rsi
	callq	*writeMB_typeInfo(%rip)
	movq	%r15, %rdi
	movl	68(%rsp), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	addl	%eax, (%rcx)
	addl	%eax, %ebp
.LBB25_47:
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.LBB25_54
# BB#48:
	movl	$1, 476(%r13,%r14)
	cmpl	$8, %eax
	je	.LBB25_56
# BB#49:
	cmpl	$14, %eax
	jne	.LBB25_65
# BB#50:
	movq	input(%rip), %rax
	cmpl	$1, 4008(%rax)
	jne	.LBB25_52
# BB#51:
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
	leaq	8(%rax,%rcx), %rbx
	movq	%rbx, %rdi
	callq	arienco_bits_written
	movl	%eax, %r15d
	movq	%rbx, %rdi
	callq	arienco_done_encoding
	movq	%rbx, %rdi
	callq	arienco_bits_written
	subl	%r15d, %ebp
	addl	%eax, %ebp
	movq	(%r12), %rdx
	movq	32(%rdx), %rsi
	movq	%rbx, %rdi
	callq	arienco_start_encoding
	callq	reset_pic_bin_count
.LBB25_52:
	movq	(%r12), %rax
	movl	4(%rax), %eax
	cmpl	$8, %eax
	movq	%r13, 160(%rsp)         # 8-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	jge	.LBB25_137
# BB#53:
	movl	$2, 56(%rsp)
	movl	%eax, 68(%rsp)
	addl	%eax, %ebp
	movq	%r14, %rcx
	leaq	44(%r13,%rcx), %r14
	addl	%eax, 44(%r13,%rcx)
	movl	$0, 76(%rsp)
	leaq	56(%rsp), %rdi
	movq	%r12, %rsi
	callq	writeSE_Fix
	jmp	.LBB25_138
.LBB25_54:
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	jne	.LBB25_69
# BB#55:
	movq	active_sps(%rip), %rax
	cmpl	$0, 1156(%rax)
	sete	%al
	jmp	.LBB25_70
.LBB25_56:
	movq	%r14, %rax
	movl	%ebp, %r14d
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	476(%r13,%rax), %rbx
	movq	96(%rsp), %rax          # 8-byte Reload
	movslq	8(%rax), %rax
	imulq	$104, %rax, %r12
	movq	48(%rsp), %rax          # 8-byte Reload
	addq	24(%rax), %r12
	movq	$-4, %rbp
	leaq	56(%rsp), %r15
	.p2align	4, 0x90
.LBB25_57:                              # =>This Inner Loop Header: Depth=1
	movslq	-84(%rbx,%rbp,4), %rax
	movq	img(%rip), %rcx
	cmpl	$1, 20(%rcx)
	jne	.LBB25_59
# BB#58:                                #   in Loop: Header=BB25_57 Depth=1
	movl	B8Mode2Value.b8inc(,%rax,4), %ecx
	imull	-68(%rbx,%rbp,4), %ecx
	addl	B8Mode2Value.b8start(,%rax,4), %ecx
	movl	%ecx, %eax
	jmp	.LBB25_60
	.p2align	4, 0x90
.LBB25_59:                              #   in Loop: Header=BB25_57 Depth=1
	addl	$-4, %eax
.LBB25_60:                              # %B8Mode2Value.exit
                                        #   in Loop: Header=BB25_57 Depth=1
	movl	%eax, 60(%rsp)
	movl	$0, 64(%rsp)
	movl	$2, 56(%rsp)
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	*writeB8_typeInfo(%rip)
	movl	68(%rsp), %eax
	addl	%eax, -444(%rbx)
	movl	-84(%rbx,%rbp,4), %ecx
	testl	%ecx, %ecx
	je	.LBB25_62
.LBB25_61:                              #   in Loop: Header=BB25_57 Depth=1
	cmpl	$4, %ecx
	sete	%dl
	jmp	.LBB25_63
	.p2align	4, 0x90
.LBB25_62:                              #   in Loop: Header=BB25_57 Depth=1
	movq	active_sps(%rip), %rsi
	movb	$1, %dl
	cmpl	$0, 1156(%rsi)
	je	.LBB25_61
.LBB25_63:                              #   in Loop: Header=BB25_57 Depth=1
	addl	%eax, %r14d
	movzbl	%dl, %eax
	andl	%eax, (%rbx)
	incq	%rbp
	jne	.LBB25_57
# BB#64:
	callq	writeMotionInfo2NAL
	movl	%r14d, %ebp
	addl	%eax, %ebp
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	(%rdi), %eax
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB25_65:
	orl	$4, %eax
	cmpl	$13, %eax
	jne	.LBB25_68
# BB#66:
	movq	input(%rip), %rax
	cmpl	$0, 5100(%rax)
	je	.LBB25_68
# BB#67:
	movl	472(%r13,%r14), %eax
	movl	%eax, 60(%rsp)
	movl	$2, 56(%rsp)
	movq	%rdi, %rbx
	leaq	56(%rsp), %rdi
	movq	%r12, %rsi
	callq	*writeMB_transform_size(%rip)
	movq	%rbx, %rdi
	movl	68(%rsp), %eax
	addl	%eax, 32(%r13,%r14)
	addl	%eax, %ebp
.LBB25_68:
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	jmp	.LBB25_71
.LBB25_69:
	xorl	%eax, %eax
.LBB25_70:                              # %.thread308
	xorb	$1, %al
	movzbl	%al, %eax
	movl	%eax, 476(%r13,%r14)
.LBB25_71:
	leaq	420(%r13,%r14), %r15
	movq	img(%rip), %rdx
	movq	14224(%rdx), %rax
	movslq	12(%rdx), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	movl	72(%rax,%rcx), %esi
	cmpl	$13, %esi
	je	.LBB25_76
# BB#72:
	xorl	%ebx, %ebx
	cmpl	$9, %esi
	jne	.LBB25_77
# BB#73:
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	%r13, 160(%rsp)         # 8-byte Spill
	movq	14216(%rdx), %rdx
	movq	input(%rip), %rsi
	movslq	4016(%rsi), %rsi
	movq	assignSE2partition(,%rsi,8), %rsi
	movslq	16(%rsi), %rsi
	imulq	$104, %rsi, %r12
	addq	24(%rdx), %r12
	movl	$1, 420(%rax,%rcx)
	leaq	44(%rax,%rcx), %r13
	xorl	%ebx, %ebx
	leaq	104(%rsp), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB25_74:                              # =>This Inner Loop Header: Depth=1
	movl	%ebx, 128(%rsp)
	movsbl	288(%r13,%rbx), %eax
	movl	%eax, 108(%rsp)
	movl	$0, 112(%rsp)
	movl	$4, 104(%rsp)
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	*writeIntraPredMode(%rip)
	movl	116(%rsp), %eax
	addl	%eax, (%r13)
	addl	%eax, %ebp
	incq	%rbx
	cmpq	$16, %rbx
	jne	.LBB25_74
# BB#75:                                # %writeIntra4x4Modes.exit.i
	movl	%ebp, %ebx
	movq	160(%rsp), %r13         # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB25_77
.LBB25_76:
	movq	%rdi, %rbx
	callq	writeIntra8x8Modes
	movq	%rbx, %rdi
	movl	%eax, %ebx
.LBB25_77:                              # %writeIntraModes.exit
	addl	8(%rsp), %ebx           # 4-byte Folded Reload
	cmpl	$0, (%r15)
	je	.LBB25_80
# BB#78:
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB25_80
# BB#79:
	movq	%r14, %r15
	movq	14224(%rax), %r14
	movslq	12(%rax), %rcx
	movq	14216(%rax), %rax
	movq	input(%rip), %rdx
	movslq	4016(%rdx), %rdx
	movq	assignSE2partition(,%rdx,8), %rdx
	movq	%rdi, %r12
	imulq	$536, %rcx, %rbp        # imm = 0x218
	movl	416(%r14,%rbp), %ecx
	movl	%ecx, 108(%rsp)
	movl	$0, 112(%rsp)
	movl	$4, 104(%rsp)
	movslq	16(%rdx), %rcx
	imulq	$104, %rcx, %rsi
	addq	24(%rax), %rsi
	leaq	104(%rsp), %rdi
	callq	*writeCIPredMode(%rip)
	movl	116(%rsp), %eax
	addl	%eax, 48(%r14,%rbp)
	movq	%r12, %rdi
	movq	%r15, %r14
	addl	%eax, %ebx
	jmp	.LBB25_82
.LBB25_80:
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	jne	.LBB25_82
# BB#81:
	movl	$0, 416(%r13,%r14)
.LBB25_82:
	movl	(%rdi), %eax
	movl	%eax, %ecx
	orl	$8, %ecx
	cmpl	$8, %ecx
	je	.LBB25_84
# BB#83:
	movq	%rdi, %rbp
	callq	writeMotionInfo2NAL
	addl	%eax, %ebx
	movl	(%rbp), %eax
.LBB25_84:
	movq	img(%rip), %rcx
	testl	%eax, %eax
	movl	%ebx, 20(%rsp)          # 4-byte Spill
	je	.LBB25_101
.LBB25_85:                              # %._crit_edge280
	movq	14224(%rcx), %rbx
	movslq	12(%rcx), %rax
	movq	14168(%rcx), %rdx
	movq	14216(%rcx), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	input(%rip), %rcx
	movslq	4016(%rcx), %rcx
	movq	assignSE2partition(,%rcx,8), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	imulq	$536, %rax, %rbp        # imm = 0x218
	movl	364(%rbx,%rbp), %r15d
	movq	(%rdx), %rax
	movq	(%rax), %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	8(%rax), %r13
	leaq	72(%rbx,%rbp), %r14
	xorl	%r12d, %r12d
	cmpl	$10, 72(%rbx,%rbp)
	movl	%r15d, 40(%rsp)         # 4-byte Spill
	je	.LBB25_93
# BB#86:
	movl	%r15d, 108(%rsp)
	movl	$6, 104(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	24(%rax), %rax
	imulq	$104, %rax, %r15
	movq	32(%rsp), %rax          # 8-byte Reload
	addq	24(%rax), %r15
	leaq	104(%rsp), %rdi
	movq	%r15, %rsi
	callq	*writeCBP(%rip)
	movl	116(%rsp), %r12d
	addl	%r12d, 40(%rbx,%rbp)
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movl	(%r14), %eax
	leal	-1(%rax), %ecx
	cmpl	$3, %ecx
	jb	.LBB25_91
# BB#87:
	testl	%eax, %eax
	jne	.LBB25_90
# BB#88:
	movq	img(%rip), %rcx
	cmpl	$1, 20(%rcx)
	jne	.LBB25_90
# BB#89:
	movq	active_sps(%rip), %rcx
	cmpl	$0, 1156(%rcx)
	jne	.LBB25_158
.LBB25_90:
	cmpl	$0, 476(%rbx,%rbp)
	je	.LBB25_92
.LBB25_91:                              # %thread-pre-split.i
	orl	$4, %eax
	cmpl	$13, %eax
	jne	.LBB25_158
.LBB25_92:                              # %.critedge.i
	movq	24(%rsp), %r14          # 8-byte Reload
	movl	40(%rsp), %r15d         # 4-byte Reload
.LBB25_93:                              # %.critedge.i
	testl	%r15d, %r15d
	jne	.LBB25_95
# BB#94:
	cmpl	$10, (%r14)
	jne	.LBB25_96
.LBB25_95:
	movl	4(%rbx,%rbp), %eax
	movl	%eax, 108(%rsp)
	movl	$15, 104(%rsp)
	movq	img(%rip), %rax
	movq	14216(%rax), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movslq	60(%rcx), %rcx
	imulq	$104, %rcx, %rsi
	addq	24(%rax), %rsi
	leaq	104(%rsp), %rdi
	callq	*writeDquant(%rip)
	movl	116(%rsp), %eax
	addl	%eax, 52(%rbx,%rbp)
	addl	%eax, %r12d
.LBB25_96:
	movq	img(%rip), %rax
	movq	152(%rax), %rcx
	movslq	12(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	(%rcx), %rdi
	movslq	15528(%rax), %rax
	leaq	16(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movq	img(%rip), %rax
	movq	152(%rax), %rcx
	movslq	12(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	8(%rcx), %rdi
	movslq	15528(%rax), %rax
	leaq	16(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movq	img(%rip), %rax
	movq	152(%rax), %rcx
	movslq	12(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	16(%rcx), %rdi
	movslq	15528(%rax), %rax
	leaq	16(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movq	img(%rip), %rax
	movq	152(%rax), %rcx
	movslq	12(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	24(%rcx), %rdi
	movslq	15528(%rax), %rax
	leaq	16(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	cmpl	$10, (%r14)
	jne	.LBB25_103
# BB#97:
	movq	input(%rip), %rax
	cmpl	$0, 4008(%rax)
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	je	.LBB25_111
# BB#98:
	movq	img(%rip), %rax
	movl	$1, 104(%rax)
	leaq	44(%rbx,%rbp), %rbx
	movl	$1, %ebp
	leaq	104(%rsp), %r14
	.p2align	4, 0x90
.LBB25_99:                              # =>This Inner Loop Header: Depth=1
	movq	152(%rsp), %rax         # 8-byte Reload
	movl	-4(%rax,%rbp,4), %r15d
	movl	%r15d, 108(%rsp)
	movl	-4(%r13,%rbp,4), %eax
	movl	%eax, 112(%rsp)
	movl	$0, 128(%rsp)
	movl	$7, 104(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	28(%rax), %rax
	imulq	$104, %rax, %rsi
	movq	32(%rsp), %rax          # 8-byte Reload
	addq	24(%rax), %rsi
	movq	%r14, %rdi
	callq	writeRunLevel_CABAC
	movl	116(%rsp), %eax
	addl	%eax, (%rbx)
	addl	%eax, %r12d
	cmpq	$16, %rbp
	jg	.LBB25_112
# BB#100:                               #   in Loop: Header=BB25_99 Depth=1
	incq	%rbp
	testl	%r15d, %r15d
	jne	.LBB25_99
	jmp	.LBB25_112
.LBB25_101:
	cmpl	$1, 20(%rcx)
	jne	.LBB25_184
# BB#102:
	cmpl	$0, 364(%r13,%r14)
	jne	.LBB25_85
	jmp	.LBB25_184
.LBB25_103:                             # %.preheader147.i222
	leaq	472(%rbx,%rbp), %r14
	testb	$1, %r15b
	je	.LBB25_105
# BB#104:
	movl	376(%rbx,%rbp), %esi
	movl	(%r14), %edx
	xorl	%edi, %edi
	callq	writeLumaCoeff8x8
	addl	%eax, %r12d
.LBB25_105:
	testb	$2, %r15b
	je	.LBB25_107
# BB#106:
	movl	380(%rbx,%rbp), %esi
	movl	(%r14), %edx
	movl	$1, %edi
	callq	writeLumaCoeff8x8
	addl	%eax, %r12d
.LBB25_107:
	testb	$4, %r15b
	je	.LBB25_109
# BB#108:
	movl	384(%rbx,%rbp), %esi
	movl	(%r14), %edx
	movl	$2, %edi
	callq	writeLumaCoeff8x8
	addl	%eax, %r12d
.LBB25_109:
	testb	$8, %r15b
	je	.LBB25_130
# BB#110:
	movl	388(%rbx,%rbp), %esi
	movl	(%r14), %edx
	movl	$3, %edi
	callq	writeLumaCoeff8x8
	addl	%eax, %r12d
	jmp	.LBB25_130
.LBB25_111:
	movl	$1, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	writeCoeff4x4_CAVLC
	addl	%eax, %r12d
.LBB25_112:                             # %.loopexit146.i224
	testb	$15, 40(%rsp)           # 1-byte Folded Reload
	je	.LBB25_130
# BB#113:                               # %.preheader144.i225
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	44(%rax,%rcx), %r15
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB25_114:                             # %.preheader143.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_115 Depth 2
                                        #       Child Loop BB25_116 Depth 3
                                        #         Child Loop BB25_118 Depth 4
                                        #         Child Loop BB25_124 Depth 4
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB25_115:                             # %.preheader142.i
                                        #   Parent Loop BB25_114 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB25_116 Depth 3
                                        #         Child Loop BB25_118 Depth 4
                                        #         Child Loop BB25_124 Depth 4
	movl	%eax, %ecx
	sarl	%ecx
	movl	%ecx, 96(%rsp)          # 4-byte Spill
	movl	%eax, 144(%rsp)         # 4-byte Spill
	orl	$1, %eax
	movl	%eax, 168(%rsp)         # 4-byte Spill
	movq	48(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB25_116:                             # %.preheader.i226
                                        #   Parent Loop BB25_114 Depth=1
                                        #     Parent Loop BB25_115 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB25_118 Depth 4
                                        #         Child Loop BB25_124 Depth 4
	movl	%ebx, %ebp
	andl	$-2, %ebp
	leaq	(%rbx,%rbx), %r14
	andl	$2, %r14d
	addl	96(%rsp), %ebp          # 4-byte Folded Reload
	movq	input(%rip), %rax
	cmpl	$0, 4008(%rax)
	movq	%rbx, 152(%rsp)         # 8-byte Spill
	je	.LBB25_121
# BB#117:                               #   in Loop: Header=BB25_116 Depth=3
	movq	img(%rip), %rax
	movq	14160(%rax), %rcx
	movl	%ebp, 24(%rsp)          # 4-byte Spill
	movslq	%ebp, %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movq	(%rcx,%r14,8), %rcx
	movq	(%rcx), %r13
	movq	8(%rcx), %rbp
	movl	%ebx, 100(%rax)
	movl	144(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, 96(%rax)
	movl	$1, 104(%rax)
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB25_118:                             #   Parent Loop BB25_114 Depth=1
                                        #     Parent Loop BB25_115 Depth=2
                                        #       Parent Loop BB25_116 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	-4(%r13,%rbx,4), %r14d
	movl	%r14d, 108(%rsp)
	movl	-4(%rbp,%rbx,4), %eax
	movl	%eax, 112(%rsp)
	movl	$1, 128(%rsp)
	movl	$9, 104(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	36(%rax), %rax
	imulq	$104, %rax, %rsi
	movq	32(%rsp), %rax          # 8-byte Reload
	addq	24(%rax), %rsi
	leaq	104(%rsp), %rdi
	callq	writeRunLevel_CABAC
	movl	116(%rsp), %eax
	addl	%eax, (%r15)
	addl	%eax, %r12d
	cmpq	$15, %rbx
	jg	.LBB25_120
# BB#119:                               #   in Loop: Header=BB25_118 Depth=4
	incq	%rbx
	testl	%r14d, %r14d
	jne	.LBB25_118
.LBB25_120:                             # %.loopexit.i229.loopexit
                                        #   in Loop: Header=BB25_116 Depth=3
	movq	152(%rsp), %rbx         # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	24(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB25_122
	.p2align	4, 0x90
.LBB25_121:                             #   in Loop: Header=BB25_116 Depth=3
	movl	$2, %edi
	xorl	%ecx, %ecx
	movl	%ebp, %esi
	movl	%r14d, %edx
	callq	writeCoeff4x4_CAVLC
	addl	%eax, %r12d
.LBB25_122:                             # %.loopexit.i229
                                        #   in Loop: Header=BB25_116 Depth=3
	orl	$1, %r14d
	movq	input(%rip), %rax
	cmpl	$0, 4008(%rax)
	je	.LBB25_126
# BB#123:                               #   in Loop: Header=BB25_116 Depth=3
	movq	img(%rip), %rax
	movq	14160(%rax), %rcx
	movslq	%ebp, %rdx
	movq	(%rcx,%rdx,8), %rcx
	movl	%r14d, %edx
	movq	(%rcx,%rdx,8), %rcx
	movq	(%rcx), %r13
	movq	8(%rcx), %r14
	movl	%ebx, 100(%rax)
	movl	168(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, 96(%rax)
	movl	$1, 104(%rax)
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB25_124:                             #   Parent Loop BB25_114 Depth=1
                                        #     Parent Loop BB25_115 Depth=2
                                        #       Parent Loop BB25_116 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	-4(%r13,%rbp,4), %ebx
	movl	%ebx, 108(%rsp)
	movl	-4(%r14,%rbp,4), %eax
	movl	%eax, 112(%rsp)
	movl	$1, 128(%rsp)
	movl	$9, 104(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	36(%rax), %rax
	imulq	$104, %rax, %rsi
	movq	32(%rsp), %rax          # 8-byte Reload
	addq	24(%rax), %rsi
	leaq	104(%rsp), %rdi
	callq	writeRunLevel_CABAC
	movl	116(%rsp), %eax
	addl	%eax, (%r15)
	addl	%eax, %r12d
	cmpq	$15, %rbp
	jg	.LBB25_127
# BB#125:                               #   in Loop: Header=BB25_124 Depth=4
	incq	%rbp
	testl	%ebx, %ebx
	jne	.LBB25_124
	jmp	.LBB25_127
	.p2align	4, 0x90
.LBB25_126:                             #   in Loop: Header=BB25_116 Depth=3
	movl	$2, %edi
	xorl	%ecx, %ecx
	movl	%ebp, %esi
	movl	%r14d, %edx
	callq	writeCoeff4x4_CAVLC
	addl	%eax, %r12d
.LBB25_127:                             # %.loopexit.loopexit.1.i
                                        #   in Loop: Header=BB25_116 Depth=3
	movq	152(%rsp), %rbx         # 8-byte Reload
	cmpq	48(%rsp), %rbx          # 8-byte Folded Reload
	leaq	1(%rbx), %rbx
	jle	.LBB25_116
# BB#128:                               #   in Loop: Header=BB25_115 Depth=2
	movl	144(%rsp), %eax         # 4-byte Reload
	addl	$2, %eax
	cmpl	$4, %eax
	jl	.LBB25_115
# BB#129:                               #   in Loop: Header=BB25_114 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	addq	$2, %rcx
	movq	%rcx, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	cmpq	$4, %rcx
	jl	.LBB25_114
.LBB25_130:                             # %writeCBPandLumaCoeff.exit
	movq	176(%rsp), %rax         # 8-byte Reload
	movl	%r12d, (%rax)
	movq	img(%rip), %rax
	movslq	15536(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB25_183
# BB#131:
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	14224(%rax), %rdi
	movslq	12(%rax), %rdx
	movq	14216(%rax), %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	input(%rip), %rcx
	movslq	4016(%rcx), %rsi
	movq	assignSE2partition(,%rsi,8), %rsi
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	imulq	$536, %rdx, %rdx        # imm = 0x218
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	movl	364(%rdi,%rdx), %edx
	xorl	%r12d, %r12d
	movl	%edx, 24(%rsp)          # 4-byte Spill
	cmpl	$16, %edx
	jl	.LBB25_169
# BB#132:                               # %.preheader147.i
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	144(%rsp), %rsi         # 8-byte Reload
	leaq	72(%rdx,%rsi), %r15
	cmpl	$0, 4008(%rcx)
	je	.LBB25_161
# BB#133:
	cmpl	$0, 15532(%rax)
	js	.LBB25_162
# BB#134:                               # %.lr.ph.preheader.i
	movq	14168(%rax), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	8(%rcx), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	writeChromaCoeff.chroma_dc_context-4(,%rcx,4), %r13d
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB25_135:                             # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	(%rcx,%r14,4), %ebp
	movl	%ebp, 108(%rsp)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%r14,4), %ecx
	movl	%ecx, 112(%rsp)
	movl	%r13d, 128(%rsp)
	movl	(%r15), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	setb	%dl
	movb	$51, %bl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %bl
	xorl	%ecx, %ecx
	andb	%dl, %bl
	sete	%cl
	leal	8(,%rcx,4), %ecx
	movl	%ecx, 104(%rsp)
	movzbl	%bl, %edx
	movl	%edx, 104(%rax)
	movl	$0, 108(%rax)
	movq	40(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%rcx,4), %rax
	imulq	$104, %rax, %rsi
	movq	152(%rsp), %rax         # 8-byte Reload
	addq	24(%rax), %rsi
	leaq	104(%rsp), %rdi
	callq	writeRunLevel_CABAC
	movl	116(%rsp), %eax
	addl	%eax, -24(%r15)
	addl	%eax, %r12d
	testl	%ebp, %ebp
	je	.LBB25_163
# BB#136:                               # %.lr.ph.i
                                        #   in Loop: Header=BB25_135 Depth=1
	movq	img(%rip), %rax
	movslq	15532(%rax), %rcx
	cmpq	%rcx, %r14
	leaq	1(%r14), %r14
	jl	.LBB25_135
	jmp	.LBB25_163
.LBB25_137:                             # %..preheader232_crit_edge
	leaq	44(%r13,%r14), %r14
.LBB25_138:                             # %.preheader232
	xorl	%r15d, %r15d
	movq	img(%rip), %rax
	leaq	56(%rsp), %r13
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB25_139:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_141 Depth 2
	movslq	180(%rax), %rcx
	movslq	%r15d, %rbp
	addq	%rcx, %rbp
	xorl	%ebx, %ebx
	jmp	.LBB25_141
	.p2align	4, 0x90
.LBB25_140:                             # %._crit_edge278
                                        #   in Loop: Header=BB25_141 Depth=2
	incl	%ebx
	movq	img(%rip), %rax
.LBB25_141:                             #   Parent Loop BB25_139 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	15444(%rax), %ecx
	movl	%ecx, 68(%rsp)
	movl	$2, 56(%rsp)
	addl	%ecx, 20(%rsp)          # 4-byte Folded Spill
	movq	enc_picture(%rip), %rdx
	movq	6440(%rdx), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movl	176(%rax), %eax
	addl	%ebx, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%eax, 76(%rsp)
	movl	%eax, 60(%rsp)
	addl	%ecx, (%r14)
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	writeSE_Fix
	cmpl	$15, %ebx
	jne	.LBB25_140
# BB#142:                               #   in Loop: Header=BB25_139 Depth=1
	incl	%r15d
	movq	img(%rip), %rax
	cmpl	$16, %r15d
	jne	.LBB25_139
# BB#143:
	cmpl	$0, 15536(%rax)
	je	.LBB25_184
# BB#144:                               # %.preheader231
	cmpl	$0, 15548(%rax)
	jle	.LBB25_184
# BB#145:                               # %.lr.ph246.preheader
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	48(%rcx,%rdx), %r13
	xorl	%edx, %edx
	leaq	56(%rsp), %r14
	.p2align	4, 0x90
.LBB25_146:                             # %.lr.ph246
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_148 Depth 2
	cmpl	$0, 15544(%rax)
	movl	%edx, 8(%rsp)           # 4-byte Spill
	jle	.LBB25_149
# BB#147:                               # %.lr.ph
                                        #   in Loop: Header=BB25_146 Depth=1
	movslq	188(%rax), %rcx
	movslq	%edx, %rbx
	addq	%rcx, %rbx
	xorl	%ebp, %ebp
	movl	20(%rsp), %r15d         # 4-byte Reload
	.p2align	4, 0x90
.LBB25_148:                             #   Parent Loop BB25_146 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	15448(%rax), %ecx
	movl	%ecx, 68(%rsp)
	movl	$2, 56(%rsp)
	addl	%ecx, %r15d
	movq	enc_picture(%rip), %rdx
	movq	6472(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movl	184(%rax), %eax
	addl	%ebp, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%eax, 76(%rsp)
	movl	%eax, 60(%rsp)
	addl	%ecx, (%r13)
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	writeSE_Fix
	incl	%ebp
	movq	img(%rip), %rax
	cmpl	15544(%rax), %ebp
	jl	.LBB25_148
	jmp	.LBB25_150
	.p2align	4, 0x90
.LBB25_149:                             #   in Loop: Header=BB25_146 Depth=1
	movl	20(%rsp), %r15d         # 4-byte Reload
.LBB25_150:                             # %._crit_edge
                                        #   in Loop: Header=BB25_146 Depth=1
	movl	%r15d, 20(%rsp)         # 4-byte Spill
	movl	8(%rsp), %edx           # 4-byte Reload
	incl	%edx
	movl	15548(%rax), %ecx
	cmpl	%ecx, %edx
	jl	.LBB25_146
# BB#151:                               # %._crit_edge247
	testl	%ecx, %ecx
	jle	.LBB25_184
# BB#152:                               # %.lr.ph246.1.preheader
	xorl	%edx, %edx
	leaq	56(%rsp), %r14
	.p2align	4, 0x90
.LBB25_153:                             # %.lr.ph246.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_155 Depth 2
	cmpl	$0, 15544(%rax)
	movl	%edx, 8(%rsp)           # 4-byte Spill
	jle	.LBB25_156
# BB#154:                               # %.lr.ph.1
                                        #   in Loop: Header=BB25_153 Depth=1
	movslq	188(%rax), %rcx
	movslq	%edx, %rbx
	addq	%rcx, %rbx
	xorl	%ebp, %ebp
	movl	20(%rsp), %r15d         # 4-byte Reload
	.p2align	4, 0x90
.LBB25_155:                             #   Parent Loop BB25_153 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	15448(%rax), %ecx
	movl	%ecx, 68(%rsp)
	movl	$2, 56(%rsp)
	addl	%ecx, %r15d
	movq	enc_picture(%rip), %rdx
	movq	6472(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movl	184(%rax), %eax
	addl	%ebp, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%eax, 76(%rsp)
	movl	%eax, 60(%rsp)
	addl	%ecx, (%r13)
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	writeSE_Fix
	incl	%ebp
	movq	img(%rip), %rax
	cmpl	15544(%rax), %ebp
	jl	.LBB25_155
	jmp	.LBB25_157
	.p2align	4, 0x90
.LBB25_156:                             #   in Loop: Header=BB25_153 Depth=1
	movl	20(%rsp), %r15d         # 4-byte Reload
.LBB25_157:                             # %._crit_edge.1
                                        #   in Loop: Header=BB25_153 Depth=1
	movl	%r15d, 20(%rsp)         # 4-byte Spill
	movl	8(%rsp), %edx           # 4-byte Reload
	incl	%edx
	cmpl	15548(%rax), %edx
	jl	.LBB25_153
	jmp	.LBB25_184
.LBB25_158:                             # %thread-pre-split.thread.i
	leaq	364(%rbx,%rbp), %rax
	testb	$15, (%rax)
	je	.LBB25_92
# BB#159:
	movq	input(%rip), %rax
	cmpl	$0, 5100(%rax)
	je	.LBB25_92
# BB#160:
	movl	472(%rbx,%rbp), %eax
	movl	%eax, 108(%rsp)
	movl	$2, 104(%rsp)
	leaq	104(%rsp), %rdi
	movq	%r15, %rsi
	callq	*writeMB_transform_size(%rip)
	movl	116(%rsp), %eax
	addl	%eax, 32(%rbx,%rbp)
	addl	%eax, %r12d
	jmp	.LBB25_92
.LBB25_161:
	movl	$6, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	writeCoeff4x4_CAVLC
	movl	%eax, %r12d
	jmp	.LBB25_163
.LBB25_162:
	xorl	%r12d, %r12d
.LBB25_163:                             # %.loopexit146.i
	movq	input(%rip), %rax
	cmpl	$0, 4008(%rax)
	je	.LBB25_168
# BB#164:
	movq	img(%rip), %rax
	cmpl	$0, 15532(%rax)
	js	.LBB25_169
# BB#165:                               # %.lr.ph.preheader.1.i
	movq	14168(%rax), %rcx
	movq	16(%rcx), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	8(%rcx), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	writeChromaCoeff.chroma_dc_context-4(,%rcx,4), %ebp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB25_166:                             # %.lr.ph.1.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	(%rcx,%r14,4), %r13d
	movl	%r13d, 108(%rsp)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%r14,4), %ecx
	movl	%ecx, 112(%rsp)
	movl	%ebp, 128(%rsp)
	movl	(%r15), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	setb	%dl
	movb	$51, %bl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %bl
	xorl	%ecx, %ecx
	andb	%dl, %bl
	sete	%cl
	leal	8(,%rcx,4), %ecx
	movl	%ecx, 104(%rsp)
	movzbl	%bl, %edx
	movl	%edx, 104(%rax)
	movl	$1, 108(%rax)
	movq	40(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%rcx,4), %rax
	imulq	$104, %rax, %rsi
	movq	152(%rsp), %rax         # 8-byte Reload
	addq	24(%rax), %rsi
	leaq	104(%rsp), %rdi
	callq	writeRunLevel_CABAC
	movl	116(%rsp), %eax
	addl	%eax, -24(%r15)
	addl	%eax, %r12d
	testl	%r13d, %r13d
	je	.LBB25_169
# BB#167:                               # %.lr.ph.1.i
                                        #   in Loop: Header=BB25_166 Depth=1
	movq	img(%rip), %rax
	movslq	15532(%rax), %rcx
	cmpq	%rcx, %r14
	leaq	1(%r14), %r14
	jl	.LBB25_166
	jmp	.LBB25_169
.LBB25_168:
	movl	$6, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$1, %ecx
	callq	writeCoeff4x4_CAVLC
	addl	%eax, %r12d
.LBB25_169:                             # %.loopexit148.i
	movl	24(%rsp), %eax          # 4-byte Reload
	andl	$-16, %eax
	cmpl	$32, %eax
	jne	.LBB25_182
# BB#170:                               # %.preheader144.i
	movq	img(%rip), %rax
	cmpl	$0, 15528(%rax)
	jle	.LBB25_182
# BB#171:                               # %.preheader.lr.ph.i
	movq	96(%rsp), %rax          # 8-byte Reload
	decl	%eax
	cltq
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	144(%rsp), %rcx         # 8-byte Reload
	leaq	72(%rax,%rcx), %r15
	movl	$-1, 8(%rsp)            # 4-byte Folded Spill
	movl	$4, %ebx
	.p2align	4, 0x90
.LBB25_172:                             # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_173 Depth 2
                                        #       Child Loop BB25_176 Depth 3
	leaq	-4(%rbx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB25_173:                             #   Parent Loop BB25_172 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB25_176 Depth 3
	movq	input(%rip), %rax
	cmpl	$0, 4008(%rax)
	je	.LBB25_179
# BB#174:                               #   in Loop: Header=BB25_173 Depth=2
	movq	img(%rip), %rax
	movq	14160(%rax), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	8(%rcx), %r13
	incl	8(%rsp)                 # 4-byte Folded Spill
	movq	96(%rsp), %rcx          # 8-byte Reload
	shlq	$5, %rcx
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	(%rcx,%rdx,4), %rcx
	movzbl	subblk_offset_y(%rbp,%rcx), %edx
	shrl	$2, %edx
	movl	%edx, 100(%rax)
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movzbl	subblk_offset_x(%rbp,%rcx), %ecx
	shrl	$2, %ecx
	movl	%ecx, 96(%rax)
	movl	$1, %ebp
	jmp	.LBB25_176
	.p2align	4, 0x90
.LBB25_175:                             # %._crit_edge.i
                                        #   in Loop: Header=BB25_176 Depth=3
	movq	img(%rip), %rax
	incq	%rbp
.LBB25_176:                             #   Parent Loop BB25_172 Depth=1
                                        #     Parent Loop BB25_173 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	-4(%rcx,%rbp,4), %r14d
	movl	%r14d, 108(%rsp)
	movl	-4(%r13,%rbp,4), %ecx
	movl	%ecx, 112(%rsp)
	movl	$7, 128(%rsp)
	movl	(%r15), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	setb	%dl
	movb	$51, %bl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %bl
	xorl	%ecx, %ecx
	andb	%dl, %bl
	sete	%cl
	leal	10(,%rcx,4), %ecx
	movl	%ecx, 104(%rsp)
	movzbl	%bl, %edx
	movl	%edx, 104(%rax)
	movl	15528(%rax), %edx
	addl	%edx, %edx
	xorl	%esi, %esi
	cmpl	%edx, 8(%rsp)           # 4-byte Folded Reload
	setge	%sil
	movl	%esi, 108(%rax)
	movq	40(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%rcx,4), %rax
	imulq	$104, %rax, %rsi
	movq	152(%rsp), %rax         # 8-byte Reload
	addq	24(%rax), %rsi
	leaq	104(%rsp), %rdi
	callq	writeRunLevel_CABAC
	movl	116(%rsp), %eax
	addl	%eax, -24(%r15)
	addl	%eax, %r12d
	cmpq	$15, %rbp
	jg	.LBB25_178
# BB#177:                               #   in Loop: Header=BB25_176 Depth=3
	testl	%r14d, %r14d
	jne	.LBB25_175
.LBB25_178:                             #   in Loop: Header=BB25_173 Depth=2
	movq	144(%rsp), %rbx         # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB25_180
	.p2align	4, 0x90
.LBB25_179:                             #   in Loop: Header=BB25_173 Depth=2
	movq	96(%rsp), %rax          # 8-byte Reload
	shlq	$5, %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,4), %rax
	movzbl	writeChromaCoeff.chroma_ac_param(%rbp,%rax), %ecx
	movl	$7, %edi
	movl	%ebx, %esi
	movl	%ebp, %edx
	callq	writeCoeff4x4_CAVLC
	addl	%eax, %r12d
.LBB25_180:                             # %.loopexit.i
                                        #   in Loop: Header=BB25_173 Depth=2
	incq	%rbp
	cmpq	$4, %rbp
	jne	.LBB25_173
# BB#181:                               #   in Loop: Header=BB25_172 Depth=1
	movq	img(%rip), %rax
	movslq	15528(%rax), %rax
	addq	$3, %rax
	cmpq	%rax, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB25_172
.LBB25_182:                             # %writeChromaCoeff.exit
	movq	176(%rsp), %rax         # 8-byte Reload
	addl	(%rax), %r12d
	movl	%r12d, (%rax)
.LBB25_183:
	addl	%r12d, 20(%rsp)         # 4-byte Folded Spill
.LBB25_184:                             # %.loopexit
	movl	20(%rsp), %eax          # 4-byte Reload
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_185:
	movq	img(%rip), %rax
	movl	144(%rax), %eax
	testl	%eax, %eax
	movq	%rbx, %rdi
	jle	.LBB25_47
# BB#186:
	movl	%eax, 60(%rsp)
	movl	$0, 64(%rsp)
	movl	$2, 56(%rsp)
	leaq	56(%rsp), %rdi
	movq	%r12, %rsi
	callq	writeSE_UVLC
	movq	%rbx, %rdi
	movl	68(%rsp), %ebp
	addl	%ebp, 32(%r13,%r14)
	movq	img(%rip), %rax
	movl	$0, 144(%rax)
	jmp	.LBB25_47
.Lfunc_end25:
	.size	writeMBLayer, .Lfunc_end25-writeMBLayer
	.cfi_endproc

	.p2align	4, 0x90
	.type	writeMotionInfo2NAL,@function
writeMotionInfo2NAL:                    # @writeMotionInfo2NAL
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi258:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi259:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi260:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi261:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi262:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi263:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi264:
	.cfi_def_cfa_offset 160
.Lcfi265:
	.cfi_offset %rbx, -56
.Lcfi266:
	.cfi_offset %r12, -48
.Lcfi267:
	.cfi_offset %r13, -40
.Lcfi268:
	.cfi_offset %r14, -32
.Lcfi269:
	.cfi_offset %r15, -24
.Lcfi270:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movq	14224(%rax), %r15
	movslq	12(%rax), %rdx
	movl	20(%rax), %r14d
	movq	input(%rip), %r11
	imulq	$536, %rdx, %rcx        # imm = 0x218
	leaq	72(%r15,%rcx), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movslq	72(%r15,%rcx), %rdx
	cmpq	$8, %rdx
	movl	$4, %esi
	cmovneq	%rdx, %rsi
	movl	72(%r11,%rsi,8), %edi
	sarl	$2, %edi
	movl	76(%r11,%rsi,8), %esi
	sarl	$2, %esi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	cmpq	$13, %rdx
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	ja	.LBB26_10
# BB#1:
	xorl	%r10d, %r10d
	movl	$9729, %esi             # imm = 0x2601
	btl	%edx, %esi
	jb	.LBB26_26
# BB#2:
	cmpl	$8, %edx
	jne	.LBB26_10
# BB#3:
	movslq	172(%rax), %rdx
	movslq	168(%rax), %r10
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %rsi
	movq	(%rsi), %r9
	leaq	3(%r10), %rbp
	decq	%r10
	leaq	3(%rdx), %r8
.LBB26_4:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_6 Depth 2
	movq	(%r9,%rdx,8), %rbx
	movq	%r10, %rsi
	.p2align	4, 0x90
.LBB26_6:                               #   Parent Loop BB26_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, 1(%rbx,%rsi)
	jne	.LBB26_10
# BB#5:                                 #   in Loop: Header=BB26_6 Depth=2
	incq	%rsi
	cmpq	%rbp, %rsi
	jl	.LBB26_6
# BB#7:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB26_4 Depth=1
	cmpq	%r8, %rdx
	leaq	1(%rdx), %rdx
	jl	.LBB26_4
# BB#8:                                 # %ZeroRef.exit
	cmpl	$1, %r14d
	je	.LBB26_10
# BB#9:                                 # %ZeroRef.exit
	xorl	%r10d, %r10d
	cmpl	$1, 4008(%r11)
	jne	.LBB26_28
.LBB26_10:                              # %ZeroRef.exit.thread.preheader
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	xorl	%ebp, %ebp
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	(%r15,%rcx), %rbx
	xorl	%r10d, %r10d
	movq	%r15, 40(%rsp)          # 8-byte Spill
	jmp	.LBB26_11
	.p2align	4, 0x90
.LBB26_16:                              # %ZeroRef.exit.thread
                                        #   in Loop: Header=BB26_11 Depth=1
	addl	16(%rsp), %ebp          # 4-byte Folded Reload
	cmpl	$3, %ebp
	jg	.LBB26_18
# BB#17:                                # %ZeroRef.exit.thread._crit_edge
                                        #   in Loop: Header=BB26_11 Depth=1
	movq	img(%rip), %rax
.LBB26_11:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_12 Depth 2
	movslq	172(%rax), %rax
	movslq	%ebp, %rcx
	addq	%rax, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB26_12:                              #   Parent Loop BB26_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r14d, %eax
	sarl	%eax
	addl	%ebp, %eax
	cltq
	movl	392(%rbx,%rax,4), %ecx
	orl	$2, %ecx
	cmpl	$2, %ecx
	jne	.LBB26_15
# BB#13:                                #   in Loop: Header=BB26_12 Depth=2
	cmpl	$0, 376(%rbx,%rax,4)
	je	.LBB26_15
# BB#14:                                #   in Loop: Header=BB26_12 Depth=2
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	(%rax), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	img(%rip), %rcx
	movl	168(%rcx), %edx
	addl	%r14d, %edx
	movslq	%edx, %rdx
	movsbl	(%rax,%rdx), %eax
	movq	14224(%rcx), %r15
	movslq	12(%rcx), %rdx
	movl	%r10d, %r13d
	movq	14216(%rcx), %rdi
	movq	input(%rip), %rsi
	movslq	4016(%rsi), %rsi
	movq	assignSE2partition(,%rsi,8), %rsi
	movslq	12(%rsi), %rsi
	imulq	$104, %rsi, %rsi
	addq	24(%rdi), %rsi
	imulq	$536, %rdx, %r12        # imm = 0x218
	movslq	432(%r15,%r12), %rdx
	movl	%eax, 68(%rsp)
	movl	$3, 64(%rsp)
	movl	$0, 72(%rsp)
	movl	%r14d, 96(%rcx)
	movl	%ebp, 100(%rcx)
	leaq	64(%rsp), %rdi
	callq	*writeRefFrame(,%rdx,8)
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%r13d, %r10d
	movl	76(%rsp), %eax
	addl	%eax, 36(%r15,%r12)
	addl	%eax, %r10d
.LBB26_15:                              #   in Loop: Header=BB26_12 Depth=2
	addl	%edi, %r14d
	cmpl	$4, %r14d
	jl	.LBB26_12
	jmp	.LBB26_16
.LBB26_18:                              # %.preheader151
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB26_19:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_20 Depth 2
	movq	img(%rip), %rax
	movslq	172(%rax), %rax
	movslq	%r15d, %rcx
	addq	%rax, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB26_20:                              #   Parent Loop BB26_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r14d, %eax
	sarl	%eax
	addl	%r15d, %eax
	cltq
	movl	392(%rbx,%rax,4), %ecx
	decl	%ecx
	cmpl	$1, %ecx
	ja	.LBB26_23
# BB#21:                                #   in Loop: Header=BB26_20 Depth=2
	cmpl	$0, 376(%rbx,%rax,4)
	je	.LBB26_23
# BB#22:                                #   in Loop: Header=BB26_20 Depth=2
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	8(%rax), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	img(%rip), %rcx
	movl	168(%rcx), %edx
	addl	%r14d, %edx
	movslq	%edx, %rdx
	movsbl	(%rax,%rdx), %eax
	movq	14224(%rcx), %rbp
	movslq	12(%rcx), %rdx
	movl	%r10d, %r13d
	movq	14216(%rcx), %rdi
	movq	input(%rip), %rsi
	movslq	4016(%rsi), %rsi
	movq	assignSE2partition(,%rsi,8), %rsi
	movslq	12(%rsi), %rsi
	imulq	$104, %rsi, %rsi
	addq	24(%rdi), %rsi
	imulq	$536, %rdx, %r12        # imm = 0x218
	movslq	432(%rbp,%r12), %rdx
	movl	%eax, 68(%rsp)
	movl	$3, 64(%rsp)
	movl	$1, 72(%rsp)
	movl	%r14d, 96(%rcx)
	movl	%r15d, 100(%rcx)
	leaq	64(%rsp), %rdi
	callq	*writeRefFrame+8(,%rdx,8)
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%r13d, %r10d
	movl	76(%rsp), %eax
	addl	%eax, 36(%rbp,%r12)
	addl	%eax, %r10d
.LBB26_23:                              #   in Loop: Header=BB26_20 Depth=2
	addl	%edi, %r14d
	cmpl	$4, %r14d
	jl	.LBB26_20
# BB#24:                                #   in Loop: Header=BB26_19 Depth=1
	addl	16(%rsp), %r15d         # 4-byte Folded Reload
	cmpl	$4, %r15d
	jl	.LBB26_19
# BB#25:                                # %.loopexit152.loopexit
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edx
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	12(%rsp), %r14d         # 4-byte Reload
.LBB26_26:                              # %.loopexit152
	cmpl	$13, %edx
	ja	.LBB26_28
# BB#27:                                # %.loopexit152
	movl	$9729, %eax             # imm = 0x2601
	btl	%edx, %eax
	jb	.LBB26_36
.LBB26_28:                              # %.preheader150.preheader
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	xorl	%r13d, %r13d
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r15, 40(%rsp)          # 8-byte Spill
	leaq	(%r15,%rcx), %r12
	.p2align	4, 0x90
.LBB26_29:                              # %.preheader150
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_30 Depth 2
	movq	img(%rip), %rax
	movslq	172(%rax), %rax
	movslq	%r13d, %r15
	addq	%rax, %r15
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	(%r13,%rax), %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB26_30:                              #   Parent Loop BB26_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %eax
	sarl	%eax
	addl	%r13d, %eax
	cltq
	movl	392(%r12,%rax,4), %ecx
	orl	$2, %ecx
	cmpl	$2, %ecx
	jne	.LBB26_33
# BB#31:                                #   in Loop: Header=BB26_30 Depth=2
	movl	376(%r12,%rax,4), %eax
	testl	%eax, %eax
	je	.LBB26_33
# BB#32:                                #   in Loop: Header=BB26_30 Depth=2
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	img(%rip), %rdx
	movl	168(%rdx), %edx
	addl	%ebp, %edx
	movslq	%edx, %rdx
	movsbl	(%rcx,%rdx), %r8d
	leal	(%rdi,%rbp), %edx
	movl	%eax, (%rsp)
	xorl	%r9d, %r9d
	movl	%r10d, %ebx
	movl	%ebp, %edi
	movl	%r13d, %esi
	movl	%r14d, %ecx
	callq	writeMotionVector8x8
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %r10d
	addl	%eax, %r10d
.LBB26_33:                              #   in Loop: Header=BB26_30 Depth=2
	addl	%edi, %ebp
	cmpl	$4, %ebp
	jl	.LBB26_30
# BB#34:                                #   in Loop: Header=BB26_29 Depth=1
	cmpl	$4, %r14d
	movl	%r14d, %r13d
	jl	.LBB26_29
# BB#35:                                # %thread-pre-split
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	12(%rsp), %r14d         # 4-byte Reload
	movq	40(%rsp), %r15          # 8-byte Reload
.LBB26_36:
	cmpl	$13, %edx
	ja	.LBB26_38
# BB#37:
	movl	$9728, %eax             # imm = 0x2600
	btl	%edx, %eax
	jae	.LBB26_38
.LBB26_47:                              # %.loopexit
	movl	%r10d, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB26_38:
	cmpl	$1, %r14d
	jne	.LBB26_47
# BB#39:
	testl	%edx, %edx
	je	.LBB26_47
# BB#40:                                # %.preheader.preheader
	xorl	%r13d, %r13d
	addq	%rcx, %r15
	.p2align	4, 0x90
.LBB26_41:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_42 Depth 2
	movq	img(%rip), %rax
	movslq	172(%rax), %rax
	movslq	%r13d, %r12
	addq	%rax, %r12
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	(%r13,%rax), %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB26_42:                              #   Parent Loop BB26_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %eax
	sarl	%eax
	addl	%r13d, %eax
	cltq
	movl	392(%r15,%rax,4), %ecx
	decl	%ecx
	cmpl	$1, %ecx
	ja	.LBB26_45
# BB#43:                                #   in Loop: Header=BB26_42 Depth=2
	movl	376(%r15,%rax,4), %eax
	testl	%eax, %eax
	je	.LBB26_45
# BB#44:                                #   in Loop: Header=BB26_42 Depth=2
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	img(%rip), %rdx
	movl	168(%rdx), %edx
	addl	%ebp, %edx
	movslq	%edx, %rdx
	movsbl	(%rcx,%rdx), %r8d
	leal	(%rdi,%rbp), %edx
	movl	%eax, (%rsp)
	movl	$1, %r9d
	movl	%r10d, %ebx
	movl	%ebp, %edi
	movl	%r13d, %esi
	movl	%r14d, %ecx
	callq	writeMotionVector8x8
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %r10d
	addl	%eax, %r10d
.LBB26_45:                              #   in Loop: Header=BB26_42 Depth=2
	addl	%edi, %ebp
	cmpl	$4, %ebp
	jl	.LBB26_42
# BB#46:                                #   in Loop: Header=BB26_41 Depth=1
	cmpl	$4, %r14d
	movl	%r14d, %r13d
	jl	.LBB26_41
	jmp	.LBB26_47
.Lfunc_end26:
	.size	writeMotionInfo2NAL, .Lfunc_end26-writeMotionInfo2NAL
	.cfi_endproc

	.globl	write_terminating_bit
	.p2align	4, 0x90
	.type	write_terminating_bit,@function
write_terminating_bit:                  # @write_terminating_bit
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	movq	input(%rip), %rcx
	movslq	4016(%rcx), %rcx
	movq	assignSE2partition(,%rcx,8), %rcx
	movq	img(%rip), %rdx
	movq	14216(%rdx), %rdx
	movq	24(%rdx), %rdx
	movslq	8(%rcx), %rcx
	imulq	$104, %rcx, %rcx
	movq	(%rdx,%rcx), %rsi
	movl	$1, 40(%rsi)
	leaq	8(%rdx,%rcx), %rdi
	movl	%eax, %esi
	jmp	biari_encode_symbol_final # TAILCALL
.Lfunc_end27:
	.size	write_terminating_bit, .Lfunc_end27-write_terminating_bit
	.cfi_endproc

	.globl	set_last_dquant
	.p2align	4, 0x90
	.type	set_last_dquant,@function
set_last_dquant:                        # @set_last_dquant
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rsi
	movq	14224(%rsi), %rax
	movslq	12(%rsi), %rcx
	imulq	$536, %rcx, %rdx        # imm = 0x218
	cmpl	$0, 72(%rax,%rdx)
	je	.LBB28_1
.LBB28_3:
	movl	4(%rax,%rdx), %ecx
	movl	%ecx, last_dquant(%rip)
	retq
.LBB28_1:
	xorl	%ecx, %ecx
	cmpl	$1, 20(%rsi)
	jne	.LBB28_4
# BB#2:
	cmpl	$0, 364(%rax,%rdx)
	jne	.LBB28_3
.LBB28_4:
	movl	%ecx, last_dquant(%rip)
	retq
.Lfunc_end28:
	.size	set_last_dquant, .Lfunc_end28-set_last_dquant
	.cfi_endproc

	.globl	write_one_macroblock
	.p2align	4, 0x90
	.type	write_one_macroblock,@function
write_one_macroblock:                   # @write_one_macroblock
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi271:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi272:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi273:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi274:
	.cfi_def_cfa_offset 48
.Lcfi275:
	.cfi_offset %rbx, -32
.Lcfi276:
	.cfi_offset %r14, -24
.Lcfi277:
	.cfi_offset %r15, -16
	movq	img(%rip), %rax
	movq	14224(%rax), %r14
	movl	12(%rax), %ecx
	movslq	%ecx, %rsi
	imulq	$536, %rsi, %rbx        # imm = 0x218
	movl	8(%r14,%rbx), %edx
	addl	%edx, 15608(%rax)
	movq	input(%rip), %r9
	cmpl	$0, 272(%r9)
	je	.LBB29_3
# BB#1:
	cmpl	$1, 20(%rax)
	ja	.LBB29_3
# BB#2:
	movl	72(%r14,%rbx), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	sbbb	%r8b, %r8b
	movb	$51, %dl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %dl
	andb	%r8b, %dl
	andb	$1, %dl
	movzbl	%dl, %ecx
	movq	14240(%rax), %rdx
	movl	%ecx, (%rdx,%rsi,4)
	movl	12(%rax), %ecx
.LBB29_3:
	testl	%ecx, %ecx
	jne	.LBB29_5
# BB#4:
	movl	$0, intras(%rip)
.LBB29_5:
	movl	72(%r14,%rbx), %esi
	cmpl	$14, %esi
	ja	.LBB29_8
# BB#6:
	movl	$26112, %edx            # imm = 0x6600
	btl	%esi, %edx
	jae	.LBB29_8
# BB#7:
	incl	intras(%rip)
.LBB29_8:
	leaq	72(%r14,%rbx), %r15
	cmpl	$1, 4008(%r9)
	jne	.LBB29_12
# BB#9:
	testl	%edi, %edi
	je	.LBB29_12
# BB#10:
	movq	14216(%rax), %rax
	cmpl	12(%rax), %ecx
	je	.LBB29_12
# BB#11:
	movslq	4016(%r9), %rcx
	movq	assignSE2partition(,%rcx,8), %rcx
	movq	24(%rax), %rax
	movslq	8(%rcx), %rcx
	imulq	$104, %rcx, %rcx
	movq	(%rax,%rcx), %rdx
	movl	$1, 40(%rdx)
	leaq	8(%rax,%rcx), %rdi
	xorl	%esi, %esi
	callq	biari_encode_symbol_final
.LBB29_12:
	movl	$1, cabac_encoding(%rip)
	leaq	12(%rsp), %rsi
	xorl	%edi, %edi
	callq	writeMBLayer
	cmpl	$0, (%r15)
	jne	.LBB29_16
# BB#13:
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	jne	.LBB29_15
# BB#14:
	cmpl	$0, 364(%r14,%rbx)
	jne	.LBB29_16
.LBB29_15:                              # %.preheader
	movq	152(%rax), %rcx
	movslq	12(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	(%rcx), %rdi
	movslq	15528(%rax), %rax
	leaq	16(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movq	img(%rip), %rax
	movq	152(%rax), %rcx
	movslq	12(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	8(%rcx), %rdi
	movslq	15528(%rax), %rax
	leaq	16(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movq	img(%rip), %rax
	movq	152(%rax), %rcx
	movslq	12(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	16(%rcx), %rdi
	movslq	15528(%rax), %rax
	leaq	16(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movq	img(%rip), %rax
	movq	152(%rax), %rcx
	movslq	12(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	24(%rcx), %rdi
	movslq	15528(%rax), %rax
	leaq	16(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movl	$4, 12(%rsp)
.LBB29_16:
	movq	img(%rip), %rax
	movq	14224(%rax), %rcx
	movslq	12(%rax), %rdx
	imulq	$536, %rdx, %rsi        # imm = 0x218
	cmpl	$0, 72(%rcx,%rsi)
	je	.LBB29_17
.LBB29_19:
	movl	4(%rcx,%rsi), %edx
	jmp	.LBB29_20
.LBB29_17:
	xorl	%edx, %edx
	cmpl	$1, 20(%rax)
	jne	.LBB29_20
# BB#18:
	cmpl	$0, 364(%rcx,%rsi)
	jne	.LBB29_19
.LBB29_20:                              # %set_last_dquant.exit
	movl	%edx, last_dquant(%rip)
	movl	32(%r14,%rbx), %r9d
	movl	36(%r14,%rbx), %ecx
	movl	44(%r14,%rbx), %esi
	addl	%r9d, %esi
	addl	%ecx, %esi
	movl	40(%r14,%rbx), %edi
	addl	%edi, %esi
	movl	52(%r14,%rbx), %edx
	addl	%edx, %esi
	addl	48(%r14,%rbx), %esi
	leaq	28(%r14,%rbx), %r8
	movl	%esi, 28(%r14,%rbx)
	movq	input(%rip), %rsi
	cmpl	$0, 5116(%rsi)
	je	.LBB29_23
# BB#21:
	leaq	44(%r14,%rbx), %rsi
	addl	%r9d, %ecx
	addl	%edi, %ecx
	addl	%edx, %ecx
	movl	%ecx, 15400(%rax)
	movl	4(%rsi), %edx
	addl	(%rsi), %edx
	movl	%edx, 15396(%rax)
	movq	generic_RC(%rip), %rsi
	addl	%edx, 20(%rsi)
	addl	%ecx, 16(%rsi)
	movl	15404(%rax), %edi
	cmpl	15352(%rax), %edi
	jae	.LBB29_23
# BB#22:
	addl	%ecx, 24(%rsi)
	addl	%edx, 28(%rsi)
.LBB29_23:
	incl	15388(%rax)
	movl	(%r8), %eax
	movq	stats(%rip), %rcx
	addl	%eax, 32(%rcx)
	movl	$0, cabac_encoding(%rip)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end29:
	.size	write_one_macroblock, .Lfunc_end29-write_one_macroblock
	.cfi_endproc

	.globl	writeReferenceFrame
	.p2align	4, 0x90
	.type	writeReferenceFrame,@function
writeReferenceFrame:                    # @writeReferenceFrame
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi278:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi279:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi280:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi281:
	.cfi_def_cfa_offset 80
.Lcfi282:
	.cfi_offset %rbx, -32
.Lcfi283:
	.cfi_offset %r14, -24
.Lcfi284:
	.cfi_offset %r15, -16
	movq	img(%rip), %rdi
	movq	14224(%rdi), %r14
	movslq	12(%rdi), %r9
	movq	14216(%rdi), %rbx
	movq	input(%rip), %rax
	movslq	4016(%rax), %rax
	movq	assignSE2partition(,%rax,8), %rax
	movslq	12(%rax), %rax
	imulq	$104, %rax, %rax
	addq	24(%rbx), %rax
	imulq	$536, %r9, %r15         # imm = 0x218
	movslq	432(%r14,%r15), %r9
	xorl	%ebx, %ebx
	testl	%ecx, %ecx
	sete	%bl
	leaq	(%rbx,%r9), %rcx
	movl	%r8d, 12(%rsp)
	movl	$3, 8(%rsp)
	movl	%ebx, 16(%rsp)
	movl	%esi, 96(%rdi)
	movl	%edx, 100(%rdi)
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	callq	*writeRefFrame(,%rcx,8)
	movl	20(%rsp), %eax
	addl	%eax, 36(%r14,%r15)
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end30:
	.size	writeReferenceFrame, .Lfunc_end30-writeReferenceFrame
	.cfi_endproc

	.globl	writeMotionVector8x8
	.p2align	4, 0x90
	.type	writeMotionVector8x8,@function
writeMotionVector8x8:                   # @writeMotionVector8x8
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi285:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi286:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi287:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi288:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi289:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi290:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi291:
	.cfi_def_cfa_offset 320
.Lcfi292:
	.cfi_offset %rbx, -56
.Lcfi293:
	.cfi_offset %r12, -48
.Lcfi294:
	.cfi_offset %r13, -40
.Lcfi295:
	.cfi_offset %r14, -32
.Lcfi296:
	.cfi_offset %r15, -24
.Lcfi297:
	.cfi_offset %rbp, -16
	movl	%edx, 32(%rsp)          # 4-byte Spill
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movl	320(%rsp), %r10d
	movq	input(%rip), %rdx
	movslq	%r10d, %rax
	movslq	136(%rdx,%rax,8), %rdi
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movslq	140(%rdx,%rax,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	img(%rip), %rax
	movq	14224(%rax), %r15
	movslq	12(%rax), %rbx
	movslq	4016(%rdx), %rdx
	movq	assignSE2partition(,%rdx,8), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	imulq	$536, %rbx, %r11        # imm = 0x218
	movzwl	480(%r15,%r11), %ebx
	testw	%bx, %bx
	movq	14216(%rax), %rbp
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	14376(%rax), %rdi
	movq	14384(%rax), %rbp
	je	.LBB31_5
# BB#1:
	testl	%r8d, %r8d
	jne	.LBB31_5
# BB#2:
	cmpl	$1, %r10d
	jne	.LBB31_5
# BB#3:
	cmpl	$2, 392(%r15,%r11)
	jne	.LBB31_5
# BB#4:
	movzwl	%bx, %ebp
	leaq	14400(%rax), %rbx
	addq	$14392, %rax            # imm = 0x3838
	cmpl	$1, %ebp
	cmovneq	%rbx, %rax
	movq	(%rax), %rbp
.LBB31_5:
	movq	%rbp, 192(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movq	%rdx, (%rsp)            # 8-byte Spill
	cmpl	%ecx, %esi
	movq	8(%rsp), %rbp           # 8-byte Reload
	jge	.LBB31_13
# BB#6:                                 # %.preheader102.lr.ph
	movl	40(%rsp), %r10d         # 4-byte Reload
	movq	72(%rsp), %r13          # 8-byte Reload
	movl	%r13d, %r12d
	movslq	%r9d, %rbx
	movslq	%r8d, %rdx
	movq	%rbx, %r8
	leaq	36(%r15,%r11), %rbx
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	addq	%r11, %r15
	movslq	28(%rsp), %rbx          # 4-byte Folded Reload
	movslq	32(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movslq	%esi, %r9
	movslq	%ecx, %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movl	%r10d, %r14d
	andl	$3, %r14d
	movq	%r8, %rcx
	shlq	$7, %rcx
	addq	%r15, %rcx
	movq	%r9, %rax
	shlq	$5, %rax
	leaq	(%rcx,%rax), %rsi
	movq	%rbp, %r15
	leaq	76(%rsi,%rbx,8), %r11
	leaq	76(%rax,%rcx), %rbp
	movq	%rbp, 152(%rsp)         # 8-byte Spill
	leaq	80(%rax,%rcx), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	%r11, %rcx
	movq	%r15, %r11
	shlq	$5, %r13
	movq	%r13, 216(%rsp)         # 8-byte Spill
	leaq	80(%rsi,%rbx,8), %rsi
	leal	2(%r8), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	leaq	-1(%r10), %r15
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(,%rax,8), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movq	%rbx, 208(%rsp)         # 8-byte Spill
	leaq	2(%rbx), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	movq	%r10, 184(%rsp)         # 8-byte Spill
	movq	%r8, 160(%rsp)          # 8-byte Spill
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB31_7:                               # %.preheader102
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_9 Depth 2
                                        #       Child Loop BB31_16 Depth 3
                                        #         Child Loop BB31_23 Depth 4
                                        #         Child Loop BB31_26 Depth 4
                                        #       Child Loop BB31_20 Depth 3
                                        #         Child Loop BB31_29 Depth 4
                                        #         Child Loop BB31_32 Depth 4
	movl	28(%rsp), %eax          # 4-byte Reload
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	movq	%rsi, 232(%rsp)         # 8-byte Spill
	jge	.LBB31_12
# BB#8:                                 # %.preheader101.lr.ph
                                        #   in Loop: Header=BB31_7 Depth=1
	movq	%rsi, 176(%rsp)         # 8-byte Spill
	movq	200(%rsp), %r13         # 8-byte Reload
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	movq	208(%rsp), %rbx         # 8-byte Reload
	movq	%r9, 96(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB31_9:                               # %.preheader101
                                        #   Parent Loop BB31_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB31_16 Depth 3
                                        #         Child Loop BB31_23 Depth 4
                                        #         Child Loop BB31_26 Depth 4
                                        #       Child Loop BB31_20 Depth 3
                                        #         Child Loop BB31_29 Depth 4
                                        #         Child Loop BB31_32 Depth 4
	movq	192(%rsp), %rbp         # 8-byte Reload
	movq	(%rbp,%r9,8), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax,%r8,8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%r11,8), %rax
	movswl	(%rax), %eax
	movq	(%rdi,%r9,8), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movq	(%rcx,%r11,8), %rcx
	movswl	(%rcx), %ecx
	subl	%ecx, %eax
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	jle	.LBB31_10
# BB#14:                                # %.preheader101.split.us.preheader
                                        #   in Loop: Header=BB31_9 Depth=2
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB31_18
# BB#15:                                # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB31_9 Depth=2
	movq	152(%rsp), %rcx         # 8-byte Reload
	movq	168(%rsp), %rbx         # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB31_16:                              # %.preheader.us.us
                                        #   Parent Loop BB31_7 Depth=1
                                        #     Parent Loop BB31_9 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB31_23 Depth 4
                                        #         Child Loop BB31_26 Depth 4
	testq	%r14, %r14
	je	.LBB31_17
# BB#22:                                # %.prol.preheader
                                        #   in Loop: Header=BB31_16 Depth=3
	movq	%rbx, %rdx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB31_23:                              #   Parent Loop BB31_7 Depth=1
                                        #     Parent Loop BB31_9 Depth=2
                                        #       Parent Loop BB31_16 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%eax, (%rdx)
	incq	%rdi
	addq	$8, %rdx
	cmpq	%rdi, %r14
	jne	.LBB31_23
	jmp	.LBB31_24
	.p2align	4, 0x90
.LBB31_17:                              #   in Loop: Header=BB31_16 Depth=3
	xorl	%edi, %edi
.LBB31_24:                              # %.prol.loopexit
                                        #   in Loop: Header=BB31_16 Depth=3
	cmpq	$3, %r15
	jb	.LBB31_27
# BB#25:                                # %.preheader.us.us.new
                                        #   in Loop: Header=BB31_16 Depth=3
	movq	%r10, %rbp
	subq	%rdi, %rbp
	addq	%r13, %rdi
	leaq	(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB31_26:                              #   Parent Loop BB31_7 Depth=1
                                        #     Parent Loop BB31_9 Depth=2
                                        #       Parent Loop BB31_16 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%eax, -16(%rdi)
	movl	%eax, -8(%rdi)
	movl	%eax, (%rdi)
	movl	%eax, 8(%rdi)
	addq	$32, %rdi
	addq	$-4, %rbp
	jne	.LBB31_26
.LBB31_27:                              # %._crit_edge.us.us
                                        #   in Loop: Header=BB31_16 Depth=3
	incq	%rsi
	addq	$32, %rbx
	addq	$32, %rcx
	cmpq	%r12, %rsi
	jne	.LBB31_16
.LBB31_18:                              # %._crit_edge105.us
                                        #   in Loop: Header=BB31_9 Depth=2
	movq	64(%rsp), %rcx          # 8-byte Reload
	movslq	20(%rcx), %rcx
	imulq	$104, %rcx, %rsi
	movq	56(%rsp), %rcx          # 8-byte Reload
	addq	24(%rcx), %rsi
	movq	img(%rip), %rcx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, 96(%rcx)
	movl	%r9d, 100(%rcx)
	movl	%eax, 108(%rsp)
	movl	%r8d, 112(%rsp)
	movl	$5, 104(%rsp)
	leaq	104(%rsp), %rdi
	movq	%r8, %rbx
	callq	*writeMVD(%rip)
	movq	96(%rsp), %r9           # 8-byte Reload
	movq	184(%rsp), %r8          # 8-byte Reload
	movl	116(%rsp), %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	addl	%eax, (%rcx)
	movq	(%rsp), %rcx            # 8-byte Reload
	addl	%eax, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r9,8), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%rbx,8), %rax
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movswl	2(%rax), %eax
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r9,8), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movswl	2(%rcx), %ecx
	subl	%ecx, %eax
	testl	%r8d, %r8d
	jle	.LBB31_34
# BB#19:                                # %.preheader.us.us.1.preheader
                                        #   in Loop: Header=BB31_9 Depth=2
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	176(%rsp), %rbx         # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB31_20:                              # %.preheader.us.us.1
                                        #   Parent Loop BB31_7 Depth=1
                                        #     Parent Loop BB31_9 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB31_29 Depth 4
                                        #         Child Loop BB31_32 Depth 4
	testq	%r14, %r14
	je	.LBB31_21
# BB#28:                                # %.prol.preheader141
                                        #   in Loop: Header=BB31_20 Depth=3
	movq	%rbx, %rdx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB31_29:                              #   Parent Loop BB31_7 Depth=1
                                        #     Parent Loop BB31_9 Depth=2
                                        #       Parent Loop BB31_20 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%eax, (%rdx)
	incq	%rdi
	addq	$8, %rdx
	cmpq	%rdi, %r14
	jne	.LBB31_29
	jmp	.LBB31_30
	.p2align	4, 0x90
.LBB31_21:                              #   in Loop: Header=BB31_20 Depth=3
	xorl	%edi, %edi
.LBB31_30:                              # %.prol.loopexit142
                                        #   in Loop: Header=BB31_20 Depth=3
	cmpq	$3, %r15
	jb	.LBB31_33
# BB#31:                                # %.preheader.us.us.1.new
                                        #   in Loop: Header=BB31_20 Depth=3
	movq	%r8, %rbp
	subq	%rdi, %rbp
	addq	%r13, %rdi
	leaq	(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB31_32:                              #   Parent Loop BB31_7 Depth=1
                                        #     Parent Loop BB31_9 Depth=2
                                        #       Parent Loop BB31_20 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%eax, -16(%rdi)
	movl	%eax, -8(%rdi)
	movl	%eax, (%rdi)
	movl	%eax, 8(%rdi)
	addq	$32, %rdi
	addq	$-4, %rbp
	jne	.LBB31_32
.LBB31_33:                              # %._crit_edge.us.us.1
                                        #   in Loop: Header=BB31_20 Depth=3
	incq	%rsi
	addq	$32, %rbx
	addq	$32, %rcx
	cmpq	%r12, %rsi
	jne	.LBB31_20
.LBB31_34:                              # %._crit_edge105.us.1
                                        #   in Loop: Header=BB31_9 Depth=2
	movq	64(%rsp), %rcx          # 8-byte Reload
	movslq	20(%rcx), %rcx
	imulq	$104, %rcx, %rsi
	movq	56(%rsp), %rcx          # 8-byte Reload
	addq	24(%rcx), %rsi
	movq	img(%rip), %rcx
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, 96(%rcx)
	movl	%r9d, 100(%rcx)
	movl	%eax, 108(%rsp)
	movl	36(%rsp), %eax          # 4-byte Reload
	movl	%eax, 112(%rsp)
	movl	$5, 104(%rsp)
	leaq	104(%rsp), %rdi
	callq	*writeMVD(%rip)
	movl	116(%rsp), %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	addl	%eax, (%rcx)
	movq	(%rsp), %rcx            # 8-byte Reload
	addl	%eax, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	8(%rsp), %r11           # 8-byte Reload
	jmp	.LBB31_11
	.p2align	4, 0x90
.LBB31_10:                              # %.preheader101.split.preheader
                                        #   in Loop: Header=BB31_9 Depth=2
	movq	64(%rsp), %rcx          # 8-byte Reload
	movslq	20(%rcx), %rcx
	imulq	$104, %rcx, %rsi
	movq	56(%rsp), %rcx          # 8-byte Reload
	addq	24(%rcx), %rsi
	movq	img(%rip), %rcx
	movl	%ebx, 96(%rcx)
	movl	%r9d, 100(%rcx)
	movl	%eax, 108(%rsp)
	movl	%r8d, 112(%rsp)
	movl	$5, 104(%rsp)
	leaq	104(%rsp), %rax
	movq	%rax, %rdi
	callq	*writeMVD(%rip)
	movl	116(%rsp), %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	addl	%eax, (%rcx)
	movq	(%rsp), %rbx            # 8-byte Reload
	addl	%eax, %ebx
	movq	96(%rsp), %rdx          # 8-byte Reload
	movq	(%rbp,%rdx,8), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	160(%rsp), %rbp         # 8-byte Reload
	movq	(%rax,%rbp,8), %rax
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movswl	2(%rax), %eax
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movq	%rdx, %rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movswl	2(%rcx), %ecx
	subl	%ecx, %eax
	movq	64(%rsp), %rcx          # 8-byte Reload
	movslq	20(%rcx), %rcx
	imulq	$104, %rcx, %rsi
	movq	56(%rsp), %rcx          # 8-byte Reload
	addq	24(%rcx), %rsi
	movq	img(%rip), %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%edx, 96(%rcx)
	movl	%edi, 100(%rcx)
	movl	%eax, 108(%rsp)
	movl	36(%rsp), %eax          # 4-byte Reload
	movl	%eax, 112(%rsp)
	movl	$5, 104(%rsp)
	leaq	104(%rsp), %rdi
	callq	*writeMVD(%rip)
	movq	8(%rsp), %r11           # 8-byte Reload
	movl	116(%rsp), %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	addl	%eax, (%rcx)
	addl	%eax, %ebx
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB31_11:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB31_9 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	%rax, %rbx
	movq	248(%rsp), %rcx         # 8-byte Reload
	addq	%rcx, 168(%rsp)         # 8-byte Folded Spill
	addq	%rax, %r13
	addq	%rcx, 176(%rsp)         # 8-byte Folded Spill
	cmpq	256(%rsp), %rbx         # 8-byte Folded Reload
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	184(%rsp), %r10         # 8-byte Reload
	movq	160(%rsp), %r8          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	96(%rsp), %r9           # 8-byte Reload
	jl	.LBB31_9
.LBB31_12:                              # %._crit_edge118
                                        #   in Loop: Header=BB31_7 Depth=1
	addq	72(%rsp), %r9           # 8-byte Folded Reload
	movq	240(%rsp), %rcx         # 8-byte Reload
	movq	216(%rsp), %rax         # 8-byte Reload
	addq	%rax, %rcx
	addq	%rax, 152(%rsp)         # 8-byte Folded Spill
	movq	232(%rsp), %rsi         # 8-byte Reload
	addq	%rax, %rsi
	addq	%rax, 144(%rsp)         # 8-byte Folded Spill
	cmpq	224(%rsp), %r9          # 8-byte Folded Reload
	jl	.LBB31_7
.LBB31_13:                              # %._crit_edge122
	movq	(%rsp), %rax            # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	writeMotionVector8x8, .Lfunc_end31-writeMotionVector8x8
	.cfi_endproc

	.globl	writeLumaCoeff4x4_CABAC
	.p2align	4, 0x90
	.type	writeLumaCoeff4x4_CABAC,@function
writeLumaCoeff4x4_CABAC:                # @writeLumaCoeff4x4_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi298:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi299:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi300:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi301:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi302:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi303:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi304:
	.cfi_def_cfa_offset 144
.Lcfi305:
	.cfi_offset %rbx, -56
.Lcfi306:
	.cfi_offset %r12, -48
.Lcfi307:
	.cfi_offset %r13, -40
.Lcfi308:
	.cfi_offset %r14, -32
.Lcfi309:
	.cfi_offset %r15, -24
.Lcfi310:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movq	14224(%rax), %r8
	movslq	12(%rax), %r9
	movq	14160(%rax), %rbp
	movq	14216(%rax), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	input(%rip), %rbx
	movslq	4016(%rbx), %rbx
	movq	assignSE2partition(,%rbx,8), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movslq	%edi, %rcx
	movq	(%rbp,%rcx,8), %rbp
	movslq	%esi, %rbx
	movq	(%rbp,%rbx,8), %rbp
	movq	(%rbp), %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	8(%rbp), %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%ebx, %esi
	andl	$1, %esi
	movl	%ecx, %edi
	andl	$1, %edi
	leal	(%rsi,%rdi,2), %esi
	movl	%esi, 96(%rax)
	xorl	%esi, %esi
	cmpl	$1, %ebx
	setg	%sil
	leal	2(%rsi), %edi
	cmpl	$2, %ecx
	cmovll	%esi, %edi
	movl	%edi, 100(%rax)
	xorl	%ecx, %ecx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	testl	%edx, %edx
	sete	%cl
	leal	7(,%rcx,4), %r15d
	leal	9(,%rcx,4), %r12d
	imulq	$536, %r9, %rcx         # imm = 0x218
	leaq	44(%r8,%rcx), %r13
	xorl	%ebp, %ebp
	movl	$1, %ebx
	jmp	.LBB32_1
	.p2align	4, 0x90
.LBB32_3:                               # %._crit_edge
                                        #   in Loop: Header=BB32_1 Depth=1
	movq	img(%rip), %rax
	incq	%rbx
.LBB32_1:                               # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	-4(%rcx,%rbx,4), %r14d
	movl	%r14d, 52(%rsp)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	-4(%rcx,%rbx,4), %ecx
	movl	%ecx, 56(%rsp)
	movl	$5, 72(%rsp)
	cmpq	$1, %rbx
	movl	%r12d, %ecx
	cmovel	%r15d, %ecx
	movl	%ecx, 48(%rsp)
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	%edx, 104(%rax)
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%rcx,4), %rax
	imulq	$104, %rax, %rsi
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	24(%rax), %rsi
	leaq	48(%rsp), %rdi
	callq	writeRunLevel_CABAC
	movl	60(%rsp), %eax
	addl	%eax, (%r13)
	addl	%eax, %ebp
	cmpq	$16, %rbx
	jg	.LBB32_4
# BB#2:                                 #   in Loop: Header=BB32_1 Depth=1
	testl	%r14d, %r14d
	jne	.LBB32_3
.LBB32_4:
	movl	%ebp, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end32:
	.size	writeLumaCoeff4x4_CABAC, .Lfunc_end32-writeLumaCoeff4x4_CABAC
	.cfi_endproc

	.globl	writeLumaCoeff8x8_CABAC
	.p2align	4, 0x90
	.type	writeLumaCoeff8x8_CABAC,@function
writeLumaCoeff8x8_CABAC:                # @writeLumaCoeff8x8_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi311:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi312:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi313:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi314:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi315:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi316:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi317:
	.cfi_def_cfa_offset 144
.Lcfi318:
	.cfi_offset %rbx, -56
.Lcfi319:
	.cfi_offset %r12, -48
.Lcfi320:
	.cfi_offset %r13, -40
.Lcfi321:
	.cfi_offset %r14, -32
.Lcfi322:
	.cfi_offset %r15, -24
.Lcfi323:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movq	14224(%rax), %rcx
	movslq	12(%rax), %rdx
	movq	14160(%rax), %rbx
	movq	14216(%rax), %rbp
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	input(%rip), %rbp
	movslq	4016(%rbp), %rbp
	movq	assignSE2partition(,%rbp,8), %rbp
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movslq	%edi, %rdi
	movq	(%rbx,%rdi,8), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp), %rbx
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	8(%rbp), %rbp
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	leal	(%rdi,%rdi), %ebp
	andl	$2, %ebp
	movl	%ebp, 96(%rax)
	xorl	%ebp, %ebp
	cmpl	$1, %edi
	setg	%bpl
	addl	%ebp, %ebp
	movl	%ebp, 100(%rax)
	xorl	%edi, %edi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	testl	%esi, %esi
	sete	%dil
	leal	7(,%rdi,4), %r15d
	leal	9(,%rdi,4), %r12d
	imulq	$536, %rdx, %rdx        # imm = 0x218
	leaq	44(%rcx,%rdx), %r13
	xorl	%ebp, %ebp
	movl	$1, %ebx
	jmp	.LBB33_1
	.p2align	4, 0x90
.LBB33_3:                               # %._crit_edge
                                        #   in Loop: Header=BB33_1 Depth=1
	movq	img(%rip), %rax
	incq	%rbx
.LBB33_1:                               # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	-4(%rcx,%rbx,4), %r14d
	movl	%r14d, 52(%rsp)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	-4(%rcx,%rbx,4), %ecx
	movl	%ecx, 56(%rsp)
	movl	$2, 72(%rsp)
	cmpq	$1, %rbx
	movl	%r12d, %ecx
	cmovel	%r15d, %ecx
	movl	%ecx, 48(%rsp)
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	%edx, 104(%rax)
	cmpl	$1, 20(%rax)
	movl	$16, %eax
	cmoveq	%rax, %rcx
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%rcx,4), %rax
	imulq	$104, %rax, %rsi
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	24(%rax), %rsi
	leaq	48(%rsp), %rdi
	callq	writeRunLevel_CABAC
	movl	60(%rsp), %eax
	addl	%eax, (%r13)
	addl	%eax, %ebp
	cmpq	$64, %rbx
	jg	.LBB33_4
# BB#2:                                 #   in Loop: Header=BB33_1 Depth=1
	testl	%r14d, %r14d
	jne	.LBB33_3
.LBB33_4:
	movl	%ebp, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end33:
	.size	writeLumaCoeff8x8_CABAC, .Lfunc_end33-writeLumaCoeff8x8_CABAC
	.cfi_endproc

	.globl	writeLumaCoeff8x8
	.p2align	4, 0x90
	.type	writeLumaCoeff8x8,@function
writeLumaCoeff8x8:                      # @writeLumaCoeff8x8
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi324:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi325:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi326:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi327:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi328:
	.cfi_def_cfa_offset 48
.Lcfi329:
	.cfi_offset %rbx, -48
.Lcfi330:
	.cfi_offset %r12, -40
.Lcfi331:
	.cfi_offset %r14, -32
.Lcfi332:
	.cfi_offset %r15, -24
.Lcfi333:
	.cfi_offset %rbp, -16
	movl	%edi, %r12d
	xorl	%ebp, %ebp
	cmpl	$11, %esi
	sete	%bpl
	movq	input(%rip), %rax
	movl	4008(%rax), %eax
	testl	%edx, %edx
	je	.LBB34_3
# BB#1:
	testl	%eax, %eax
	je	.LBB34_2
# BB#6:
	xorl	%eax, %eax
	cmpl	$13, %esi
	sete	%al
	movl	%r12d, %edi
	movl	%eax, %esi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	writeLumaCoeff8x8_CABAC # TAILCALL
.LBB34_3:
	testl	%eax, %eax
	je	.LBB34_2
# BB#4:                                 # %.preheader28.preheader
	xorl	%esi, %esi
	movl	%r12d, %edi
	movl	%ebp, %edx
	callq	writeLumaCoeff4x4_CABAC
	movl	%eax, %r14d
	movl	$1, %esi
	movl	%r12d, %edi
	movl	%ebp, %edx
	callq	writeLumaCoeff4x4_CABAC
	movl	%eax, %r15d
	addl	%r14d, %r15d
	movl	$2, %esi
	movl	%r12d, %edi
	movl	%ebp, %edx
	callq	writeLumaCoeff4x4_CABAC
	movl	%eax, %r14d
	addl	%r15d, %r14d
	movl	$3, %esi
	movl	%r12d, %edi
	movl	%ebp, %edx
	callq	writeLumaCoeff4x4_CABAC
	jmp	.LBB34_5
.LBB34_2:                               # %.preheader
	xorl	%eax, %eax
	cmpl	$13, %esi
	sete	%al
	testl	%edx, %edx
	cmovnel	%eax, %ebp
	xorl	%edi, %edi
	xorl	%edx, %edx
	movl	%r12d, %esi
	movl	%ebp, %ecx
	callq	writeCoeff4x4_CAVLC
	movl	%eax, %r14d
	xorl	%edi, %edi
	movl	$1, %edx
	movl	%r12d, %esi
	movl	%ebp, %ecx
	callq	writeCoeff4x4_CAVLC
	movl	%eax, %ebx
	addl	%r14d, %ebx
	xorl	%edi, %edi
	movl	$2, %edx
	movl	%r12d, %esi
	movl	%ebp, %ecx
	callq	writeCoeff4x4_CAVLC
	movl	%eax, %r14d
	addl	%ebx, %r14d
	xorl	%edi, %edi
	movl	$3, %edx
	movl	%r12d, %esi
	movl	%ebp, %ecx
	callq	writeCoeff4x4_CAVLC
.LBB34_5:                               # %.loopexit
	addl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end34:
	.size	writeLumaCoeff8x8, .Lfunc_end34-writeLumaCoeff8x8
	.cfi_endproc

	.globl	writeCoeff4x4_CAVLC
	.p2align	4, 0x90
	.type	writeCoeff4x4_CAVLC,@function
writeCoeff4x4_CAVLC:                    # @writeCoeff4x4_CAVLC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi334:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi335:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi336:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi337:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi338:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi339:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi340:
	.cfi_def_cfa_offset 192
.Lcfi341:
	.cfi_offset %rbx, -56
.Lcfi342:
	.cfi_offset %r12, -48
.Lcfi343:
	.cfi_offset %r13, -40
.Lcfi344:
	.cfi_offset %r14, -32
.Lcfi345:
	.cfi_offset %r15, -24
.Lcfi346:
	.cfi_offset %rbp, -16
	movl	%ecx, %r13d
	movq	img(%rip), %rcx
	movq	14224(%rcx), %rbp
	movslq	12(%rcx), %rbx
	movq	14216(%rcx), %r14
	movq	input(%rip), %rax
	movslq	4016(%rax), %rax
	movq	assignSE2partition(,%rax,8), %r12
	cmpl	$7, %edi
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movl	%esi, 132(%rsp)         # 4-byte Spill
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	movl	%edx, 128(%rsp)         # 4-byte Spill
	ja	.LBB35_9
# BB#1:
	movl	%edi, %eax
	jmpq	*.LJTI35_0(,%rax,8)
.LBB35_2:
	movq	14160(%rcx), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
	movdqu	(%rax), %xmm0
	movdqa	%xmm0, 80(%rsp)         # 16-byte Spill
	imulq	$536, %rbx, %rax        # imm = 0x218
	movl	72(%rbp,%rax), %eax
	movl	$16, (%rsp)             # 4-byte Folded Spill
	cmpl	$14, %eax
	ja	.LBB35_78
# BB#3:
	movl	$26112, %edx            # imm = 0x6600
	btl	%eax, %edx
	jae	.LBB35_78
# BB#4:
	xorl	%r15d, %r15d
	jmp	.LBB35_7
.LBB35_5:
	movq	14168(%rcx), %rax
	movq	(%rax), %rax
	movdqu	(%rax), %xmm0
	movdqa	%xmm0, 80(%rsp)         # 16-byte Spill
	xorl	%r15d, %r15d
	movl	$16, (%rsp)             # 4-byte Folded Spill
	movl	$7, %r10d
	jmp	.LBB35_8
.LBB35_6:
	movq	14160(%rcx), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
	movdqu	(%rax), %xmm0
	movdqa	%xmm0, 80(%rsp)         # 16-byte Spill
	xorl	%r15d, %r15d
	movl	$15, (%rsp)             # 4-byte Folded Spill
.LBB35_7:
	movl	$9, %r10d
.LBB35_8:
	movl	$5, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB35_10
.LBB35_9:
	movl	$.L.str.8, %edi
	movl	$600, %esi              # imm = 0x258
	callq	error
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 80(%rsp)         # 16-byte Spill
	xorl	%r10d, %r10d
	movq	img(%rip), %rcx
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$0, (%rsp)              # 4-byte Folded Spill
	xorl	%r15d, %r15d
.LBB35_10:
	xorl	%r8d, %r8d
	jmp	.LBB35_19
.LBB35_11:
	movl	15532(%rcx), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movq	14168(%rcx), %rax
	movslq	%r13d, %rdx
	movq	8(%rax,%rdx,8), %rax
	movdqu	(%rax), %xmm0
	movdqa	%xmm0, 80(%rsp)         # 16-byte Spill
	imulq	$536, %rbx, %rax        # imm = 0x218
	movl	72(%rbp,%rax), %eax
	xorl	%r8d, %r8d
	movb	$1, %r15b
	cmpl	$14, %eax
	ja	.LBB35_79
# BB#12:
	movl	$26112, %edx            # imm = 0x6600
	btl	%eax, %edx
	jae	.LBB35_79
# BB#13:
	movl	$8, %r10d
	jmp	.LBB35_18
.LBB35_14:
	movq	14160(%rcx), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
	movdqu	(%rax), %xmm0
	movdqa	%xmm0, 80(%rsp)         # 16-byte Spill
	imulq	$536, %rbx, %rax        # imm = 0x218
	movl	72(%rbp,%rax), %eax
	movl	$1, %r8d
	movl	$15, (%rsp)             # 4-byte Folded Spill
	cmpl	$14, %eax
	ja	.LBB35_80
# BB#15:
	movl	$26112, %edx            # imm = 0x6600
	btl	%eax, %edx
	jae	.LBB35_80
# BB#16:
	movl	$10, %r10d
.LBB35_17:
	xorl	%r15d, %r15d
.LBB35_18:
	movl	$6, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB35_19:
	movl	%r10d, %eax
	movslq	(%r12,%rax,4), %rax
	imulq	$104, %rax, %rax
	addq	24(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testb	%r15b, %r15b
	je	.LBB35_28
# BB#20:                                # %.split.us.preheader
	movslq	15532(%rcx), %rcx
	testq	%rcx, %rcx
	js	.LBB35_35
# BB#21:                                # %.lr.ph335.preheader
	movdqa	80(%rsp), %xmm0         # 16-byte Reload
	movd	%xmm0, %rdx
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r11
	xorl	%esi, %esi
	movl	$3, %r9d
	xorl	%r12d, %r12d
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB35_22:                              # %.lr.ph335
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx,%rbp,4), %edi
	testl	%edi, %edi
	je	.LBB35_26
# BB#23:                                #   in Loop: Header=BB35_22 Depth=1
	addl	(%r11,%rbp,4), %esi
	movl	%edi, %ebx
	negl	%ebx
	cmovll	%edi, %ebx
	xorl	%eax, %eax
	cmpl	$1, %ebx
	jne	.LBB35_25
# BB#24:                                #   in Loop: Header=BB35_22 Depth=1
	leal	1(%r12), %eax
	cmpl	$2, %r12d
	cmovgl	%r9d, %eax
.LBB35_25:                              #   in Loop: Header=BB35_22 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	incl	%ebx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	%ebp, %ebx
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movl	%eax, %r12d
.LBB35_26:                              # %.split.us
                                        #   in Loop: Header=BB35_22 Depth=1
	testl	%edi, %edi
	je	.LBB35_36
# BB#27:                                # %.split.us
                                        #   in Loop: Header=BB35_22 Depth=1
	incq	%rbp
	cmpq	%rcx, %rbp
	jle	.LBB35_22
	jmp	.LBB35_36
.LBB35_28:                              # %.split.preheader.preheader
	movdqa	80(%rsp), %xmm0         # 16-byte Reload
	movd	%xmm0, %rcx
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %rdx
	xorl	%esi, %esi
	movl	$3, %r9d
	xorl	%r12d, %r12d
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB35_29:                              # %.split.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rdi,4), %ebx
	testl	%ebx, %ebx
	je	.LBB35_33
# BB#30:                                #   in Loop: Header=BB35_29 Depth=1
	addl	(%rdx,%rdi,4), %esi
	movl	%ebx, %ebp
	negl	%ebp
	cmovll	%ebx, %ebp
	xorl	%eax, %eax
	cmpl	$1, %ebp
	jne	.LBB35_32
# BB#31:                                #   in Loop: Header=BB35_29 Depth=1
	leal	1(%r12), %eax
	cmpl	$2, %r12d
	cmovgl	%r9d, %eax
.LBB35_32:                              #   in Loop: Header=BB35_29 Depth=1
	movq	8(%rsp), %rbp           # 8-byte Reload
	incl	%ebp
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movl	%edi, %ebp
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	movl	%eax, %r12d
.LBB35_33:                              # %.split
                                        #   in Loop: Header=BB35_29 Depth=1
	testl	%ebx, %ebx
	je	.LBB35_36
# BB#34:                                # %.split
                                        #   in Loop: Header=BB35_29 Depth=1
	incq	%rdi
	cmpq	$17, %rdi
	jl	.LBB35_29
	jmp	.LBB35_36
.LBB35_35:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	xorl	%r12d, %r12d
	xorl	%esi, %esi
.LBB35_36:                              # %.us-lcssa.us
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	testb	%r15b, %r15b
	movq	%r10, 32(%rsp)          # 8-byte Spill
	je	.LBB35_38
# BB#37:
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB35_44
.LBB35_38:
	testl	%r8d, %r8d
	je	.LBB35_40
# BB#39:
	movl	%r13d, %ebx
	sarl	$4, %ebx
	andl	$15, %r13d
	movl	%ebx, %edi
	movl	%r13d, %esi
	callq	predict_nnz_chroma
	jmp	.LBB35_41
.LBB35_40:
	movl	128(%rsp), %esi         # 4-byte Reload
	movl	%esi, %eax
	andl	$1, %eax
	movl	132(%rsp), %edx         # 4-byte Reload
	movl	%edx, %ecx
	andl	$1, %ecx
	leal	(%rax,%rcx,2), %ebx
	xorl	%eax, %eax
	cmpl	$1, %esi
	setg	%al
	leal	2(%rax), %r13d
	cmpl	$2, %edx
	cmovll	%eax, %r13d
	movl	%ebx, %edi
	movl	%r13d, %esi
	callq	predict_nnz
.LBB35_41:
	movq	img(%rip), %rcx
	movq	152(%rcx), %rdx
	movslq	12(%rcx), %rcx
	movq	(%rdx,%rcx,8), %rcx
	movslq	%ebx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	%r13d, %rdx
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	%ebx, (%rcx,%rdx,4)
	cmpl	$2, %eax
	movq	32(%rsp), %r10          # 8-byte Reload
	jl	.LBB35_44
# BB#42:
	movl	$1, %ebp
	cmpl	$4, %eax
	jl	.LBB35_44
# BB#43:
	xorl	%ebp, %ebp
	cmpl	$7, %eax
	setg	%bpl
	orl	$2, %ebp
.LBB35_44:
	movl	%r10d, 40(%rsp)
	movl	%ebx, 44(%rsp)
	movl	%r12d, 48(%rsp)
	movl	%ebp, 52(%rsp)
	leaq	40(%rsp), %rdi
	testb	%r15b, %r15b
	je	.LBB35_46
# BB#45:
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	writeSyntaxElement_NumCoeffTrailingOnesChromaDC
	jmp	.LBB35_47
.LBB35_46:
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	writeSyntaxElement_NumCoeffTrailingOnes
.LBB35_47:
	movl	52(%rsp), %r14d
	imulq	$536, 104(%rsp), %rax   # 8-byte Folded Reload
                                        # imm = 0x218
	movq	120(%rsp), %r13         # 8-byte Reload
	addq	%rax, %r13
	movq	16(%rsp), %r9           # 8-byte Reload
	addl	%r14d, 24(%r13,%r9,4)
	testl	%ebx, %ebx
	je	.LBB35_77
# BB#48:                                # %.preheader279
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	%r15d, 120(%rsp)        # 4-byte Spill
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, %r15d
	subl	%r12d, %r15d
	testl	%r12d, %r12d
	jle	.LBB35_52
# BB#49:                                # %.lr.ph297.preheader
	movslq	%eax, %rcx
	movslq	%r15d, %rdx
	movdqa	80(%rsp), %xmm0         # 16-byte Reload
	movd	%xmm0, %rsi
	xorl	%eax, %eax
	movq	32(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB35_50:                              # %.lr.ph297
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rcx,4), %ebx
	movl	%ebx, %edi
	negl	%edi
	cmovll	%ebx, %edi
	cmpl	$2, %edi
	jge	.LBB35_81
# BB#51:                                #   in Loop: Header=BB35_50 Depth=1
	shldl	$1, %ebx, %eax
	decq	%rcx
	cmpq	%rdx, %rcx
	jg	.LBB35_50
	jmp	.LBB35_53
.LBB35_52:
	xorl	%eax, %eax
	movq	32(%rsp), %r8           # 8-byte Reload
.LBB35_53:                              # %._crit_edge298
	leaq	24(%r13,%r9,4), %rbp
	testl	%r12d, %r12d
	je	.LBB35_55
# BB#54:
	movl	%r8d, 40(%rsp)
	movl	%r12d, 48(%rsp)
	movl	%eax, 44(%rsp)
	leaq	40(%rsp), %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	writeSyntaxElement_VLC
	movl	52(%rsp), %eax
	addl	%eax, (%rbp)
	addl	%eax, %r14d
.LBB35_55:
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%r15d, %r15d
	js	.LBB35_63
# BB#56:                                # %.lr.ph293.preheader
	cmpl	$10, %esi
	setg	%al
	cmpl	$3, %r12d
	setl	%cl
	sete	%dl
	andb	%al, %cl
	movzbl	%cl, %ebx
	movq	%rsi, %rax
	movslq	%r15d, %r12
	cmpl	$3, %eax
	setg	%al
	andb	%dl, %al
	movdqa	80(%rsp), %xmm0         # 16-byte Reload
	movd	%xmm0, %rcx
	leaq	(%rcx,%r12,4), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	incq	%r12
	xorl	%r15d, %r15d
	movq	32(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB35_57:                              # %.lr.ph293
                                        # =>This Inner Loop Header: Depth=1
	movq	104(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx,%r15,4), %r13d
	movl	%r13d, 44(%rsp)
	movl	%ecx, 40(%rsp)
	testb	$1, %al
	jne	.LBB35_59
# BB#58:                                #   in Loop: Header=BB35_57 Depth=1
	testl	%r13d, %r13d
	movl	$1, %eax
	movl	$-1, %ecx
	cmovgl	%ecx, %eax
	addl	%r13d, %eax
	movl	%eax, 44(%rsp)
.LBB35_59:                              #   in Loop: Header=BB35_57 Depth=1
	movq	active_sps(%rip), %rax
	movl	4(%rax), %ecx
	leaq	40(%rsp), %rdi
	testl	%ebx, %ebx
	je	.LBB35_61
# BB#60:                                #   in Loop: Header=BB35_57 Depth=1
	movl	%ebx, %esi
	movq	24(%rsp), %rdx          # 8-byte Reload
	callq	writeSyntaxElement_Level_VLCN
	jmp	.LBB35_62
	.p2align	4, 0x90
.LBB35_61:                              #   in Loop: Header=BB35_57 Depth=1
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%ecx, %edx
	callq	writeSyntaxElement_Level_VLC1
.LBB35_62:                              #   in Loop: Header=BB35_57 Depth=1
	movl	%r13d, %eax
	negl	%eax
	cmovll	%r13d, %eax
	movslq	%ebx, %rcx
	xorl	%edx, %edx
	cmpl	writeCoeff4x4_CAVLC.incVlc(,%rcx,4), %eax
	setg	%dl
	addl	%ebx, %edx
	cmpl	$3, %eax
	movl	%edx, %ebx
	movl	$2, %eax
	cmovgl	%eax, %ebx
	testl	%r15d, %r15d
	cmovnel	%edx, %ebx
	movl	52(%rsp), %eax
	addl	%eax, (%rbp)
	addl	%eax, %r14d
	leaq	-1(%r12,%r15), %rcx
	decq	%r15
	movb	$1, %al
	testq	%rcx, %rcx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	jg	.LBB35_57
	jmp	.LBB35_64
.LBB35_63:
	movq	32(%rsp), %rcx          # 8-byte Reload
.LBB35_64:                              # %._crit_edge
	movq	8(%rsp), %r12           # 8-byte Reload
	cmpl	(%rsp), %r12d           # 4-byte Folded Reload
	jge	.LBB35_69
# BB#65:
	movl	%ecx, 40(%rsp)
	movl	%edx, 44(%rsp)
	leal	-1(%r12), %eax
	movl	%eax, 52(%rsp)
	cmpb	$0, 120(%rsp)           # 1-byte Folded Reload
	je	.LBB35_67
# BB#66:
	leaq	40(%rsp), %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	writeSyntaxElement_TotalZerosChromaDC
	jmp	.LBB35_68
.LBB35_67:
	leaq	40(%rsp), %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	writeSyntaxElement_TotalZeros
.LBB35_68:
	movl	52(%rsp), %eax
	addl	%eax, (%rbp)
	addl	%eax, %r14d
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
.LBB35_69:                              # %.preheader
	movl	4(%rsp), %eax           # 4-byte Reload
	testl	%eax, %eax
	js	.LBB35_77
# BB#70:                                # %.lr.ph
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movslq	%eax, %r13
	cmpl	$2, %r12d
	jl	.LBB35_76
# BB#71:                                # %.lr.ph.split.us.preheader
	pshufd	$78, 80(%rsp), %xmm0    # 16-byte Folded Reload
                                        # xmm0 = mem[2,3,0,1]
	movd	%xmm0, %r15
	incq	%r13
	.p2align	4, 0x90
.LBB35_72:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r15,%r13,4), %ebx
	movl	%ebx, 44(%rsp)
	movl	%ecx, 40(%rsp)
	testl	%edx, %edx
	je	.LBB35_77
# BB#73:                                #   in Loop: Header=BB35_72 Depth=1
	cmpl	$2, %r12d
	jl	.LBB35_75
# BB#74:                                #   in Loop: Header=BB35_72 Depth=1
	leal	-1(%rdx), %eax
	cmpl	$7, %eax
	movl	$6, %ecx
	cmovgel	%ecx, %eax
	movl	%eax, 52(%rsp)
	leaq	40(%rsp), %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rbp
	movq	%rdx, %r12
	callq	writeSyntaxElement_Run
	movq	%r12, %rdx
	movq	%rbp, %r12
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	52(%rsp), %eax
	movq	104(%rsp), %rsi         # 8-byte Reload
	addl	%eax, (%rsi)
	addl	%eax, %r14d
	subl	%ebx, %edx
	decl	%r12d
.LBB35_75:                              #   in Loop: Header=BB35_72 Depth=1
	decq	%r13
	jg	.LBB35_72
	jmp	.LBB35_77
.LBB35_76:                              # %.lr.ph.split
	pshufd	$78, 80(%rsp), %xmm0    # 16-byte Folded Reload
                                        # xmm0 = mem[2,3,0,1]
	movd	%xmm0, %rax
	movl	(%rax,%r13,4), %eax
	movl	%eax, 44(%rsp)
	movl	%ecx, 40(%rsp)
.LBB35_77:                              # %.loopexit
	movl	%r14d, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB35_78:
	xorl	%r15d, %r15d
	movl	$13, %r10d
	jmp	.LBB35_8
.LBB35_79:
	movl	$12, %r10d
	jmp	.LBB35_18
.LBB35_80:
	movl	$14, %r10d
	jmp	.LBB35_17
.LBB35_81:
	movl	$.Lstr, %edi
	callq	puts
	movl	$-1, %edi
	callq	exit
.Lfunc_end35:
	.size	writeCoeff4x4_CAVLC, .Lfunc_end35-writeCoeff4x4_CAVLC
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI35_0:
	.quad	.LBB35_2
	.quad	.LBB35_5
	.quad	.LBB35_6
	.quad	.LBB35_9
	.quad	.LBB35_9
	.quad	.LBB35_9
	.quad	.LBB35_11
	.quad	.LBB35_14

	.text
	.globl	predict_nnz
	.p2align	4, 0x90
	.type	predict_nnz,@function
predict_nnz:                            # @predict_nnz
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi347:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi348:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi349:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi350:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi351:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi352:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi353:
	.cfi_def_cfa_offset 96
.Lcfi354:
	.cfi_offset %rbx, -56
.Lcfi355:
	.cfi_offset %r12, -48
.Lcfi356:
	.cfi_offset %r13, -40
.Lcfi357:
	.cfi_offset %r14, -32
.Lcfi358:
	.cfi_offset %r15, -24
.Lcfi359:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	img(%rip), %rax
	movslq	12(%rax), %r13
	movq	14224(%rax), %rbx
	leal	(,%rdi,4), %r15d
	leal	-1(,%rdi,4), %esi
	shll	$2, %ebp
	leaq	8(%rsp), %r14
	movl	%r13d, %edi
	movl	%ebp, %edx
	movq	%r14, %rcx
	callq	getLuma4x4Neighbour
	imulq	$536, %r13, %rax        # imm = 0x218
	leaq	72(%rbx,%rax), %rdx
	movl	72(%rbx,%rax), %eax
	cmpl	$14, %eax
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	ja	.LBB36_2
# BB#1:
	movl	$26112, %ecx            # imm = 0x6600
	btl	%eax, %ecx
	jae	.LBB36_2
# BB#3:
	movl	8(%rsp), %eax
	testl	%eax, %eax
	je	.LBB36_4
# BB#5:
	leaq	8(%rsp), %r14
	movq	active_pps(%rip), %rcx
	xorl	%r12d, %r12d
	cmpl	$0, 224(%rcx)
	je	.LBB36_6
# BB#8:
	movq	input(%rip), %rcx
	cmpl	$0, 4016(%rcx)
	je	.LBB36_6
# BB#9:
	movq	img(%rip), %rcx
	movq	14208(%rcx), %rdx
	cmpl	$0, 4(%rdx)
	movl	%r15d, %esi
	jne	.LBB36_7
# BB#10:
	leaq	8(%rsp), %r14
	movq	14240(%rcx), %rcx
	movslq	12(%rsp), %rdx
	xorl	%r12d, %r12d
	andl	(%rcx,%rdx,4), %eax
	movl	%eax, 8(%rsp)
	sete	%r12b
	testl	%eax, %eax
	jne	.LBB36_7
	jmp	.LBB36_12
.LBB36_6:
	movl	%r15d, %esi
	jmp	.LBB36_7
.LBB36_4:
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	movl	%r15d, %esi
	jmp	.LBB36_13
.LBB36_2:                               # %._crit_edge
	leaq	8(%rsp), %r14
	xorl	%r12d, %r12d
	movl	8(%rsp), %eax
	movl	%r15d, %esi
	testl	%eax, %eax
	je	.LBB36_12
.LBB36_7:                               # %.thread34
	movq	img(%rip), %rax
	movq	152(%rax), %rax
	movslq	12(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	16(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	20(%rsp), %rcx
	movl	(%rax,%rcx,4), %ebx
	incl	%r12d
.LBB36_13:                              # %.thread38
	decl	%ebp
	leaq	8(%rsp), %rcx
	movl	%r13d, %edi
	movl	%ebp, %edx
	callq	getLuma4x4Neighbour
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpl	$14, %eax
	ja	.LBB36_15
# BB#14:                                # %.thread38
	movl	$26112, %ecx            # imm = 0x6600
	btl	%eax, %ecx
	jae	.LBB36_15
# BB#16:
	movl	(%r14), %eax
	testl	%eax, %eax
	je	.LBB36_23
# BB#17:
	movq	active_pps(%rip), %rcx
	cmpl	$0, 224(%rcx)
	je	.LBB36_22
# BB#18:
	movq	input(%rip), %rcx
	cmpl	$0, 4016(%rcx)
	je	.LBB36_22
# BB#19:
	movq	img(%rip), %rcx
	movq	14208(%rcx), %rdx
	cmpl	$0, 4(%rdx)
	jne	.LBB36_22
# BB#20:
	movq	14240(%rcx), %rcx
	movslq	12(%rsp), %rdx
	andl	(%rcx,%rdx,4), %eax
	movl	%eax, (%r14)
	cmpl	$1, %eax
	adcl	$0, %r12d
	testl	%eax, %eax
	jne	.LBB36_22
	jmp	.LBB36_23
.LBB36_15:                              # %.thread-pre-split_crit_edge
	movl	(%r14), %eax
	testl	%eax, %eax
	je	.LBB36_23
.LBB36_22:                              # %thread-pre-split.thread
	movq	img(%rip), %rax
	movq	152(%rax), %rax
	movslq	12(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	16(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	20(%rsp), %rcx
	addl	(%rax,%rcx,4), %ebx
	incl	%r12d
.LBB36_23:                              # %.thread
	leal	1(%rbx), %eax
	sarl	%eax
	cmpl	$2, %r12d
	cmovnel	%ebx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB36_12:
	xorl	%ebx, %ebx
	jmp	.LBB36_13
.Lfunc_end36:
	.size	predict_nnz, .Lfunc_end36-predict_nnz
	.cfi_endproc

	.globl	predict_nnz_chroma
	.p2align	4, 0x90
	.type	predict_nnz_chroma,@function
predict_nnz_chroma:                     # @predict_nnz_chroma
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi360:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi361:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi362:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi363:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi364:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi365:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi366:
	.cfi_def_cfa_offset 96
.Lcfi367:
	.cfi_offset %rbx, -56
.Lcfi368:
	.cfi_offset %r12, -48
.Lcfi369:
	.cfi_offset %r13, -40
.Lcfi370:
	.cfi_offset %r14, -32
.Lcfi371:
	.cfi_offset %r15, -24
.Lcfi372:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movl	%edi, %r15d
	movq	img(%rip), %rax
	movq	14224(%rax), %rbp
	movslq	12(%rax), %r13
	leal	(,%r15,4), %r14d
	cmpl	$3, 15536(%rax)
	jne	.LBB37_8
# BB#1:
	movslq	%ebx, %rax
	movslq	predict_nnz_chroma.j_off_tab(,%rax,4), %r12
	leal	-1(%r14), %esi
	subl	%r12d, %ebx
	shll	$2, %ebx
	leaq	8(%rsp), %r15
	movl	%r13d, %edi
	movl	%ebx, %edx
	movq	%r15, %rcx
	callq	getChroma4x4Neighbour
	imulq	$536, %r13, %rax        # imm = 0x218
	leaq	72(%rbp,%rax), %rsi
	movl	72(%rbp,%rax), %eax
	cmpl	$14, %eax
	movq	%r12, 32(%rsp)          # 8-byte Spill
	ja	.LBB37_18
# BB#2:
	movl	$26112, %ecx            # imm = 0x6600
	btl	%eax, %ecx
	jae	.LBB37_18
# BB#3:
	movl	8(%rsp), %eax
	testl	%eax, %eax
	je	.LBB37_16
# BB#4:
	leaq	8(%rsp), %rdi
	movq	active_pps(%rip), %rcx
	xorl	%ebp, %ebp
	cmpl	$0, 224(%rcx)
	je	.LBB37_19
# BB#5:
	movq	input(%rip), %rcx
	cmpl	$0, 4016(%rcx)
	je	.LBB37_19
# BB#6:
	movq	img(%rip), %rcx
	movq	14208(%rcx), %rdx
	cmpl	$0, 4(%rdx)
	jne	.LBB37_19
# BB#7:
	leaq	8(%rsp), %rdi
	movq	14240(%rcx), %rcx
	movslq	12(%rsp), %rdx
	andl	(%rcx,%rdx,4), %eax
	movl	%eax, 8(%rsp)
	movl	$-1, %ebp
	testl	%eax, %eax
	jne	.LBB37_19
	jmp	.LBB37_42
.LBB37_8:
	andl	$4, %r14d
	leal	-1(%r14), %esi
	leal	(,%rbx,4), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	leal	-16(,%rbx,4), %edx
	leaq	8(%rsp), %rbx
	movl	%r13d, %edi
	movq	%rbx, %rcx
	callq	getChroma4x4Neighbour
	imulq	$536, %r13, %rax        # imm = 0x218
	leaq	72(%rbp,%rax), %rdx
	movl	72(%rbp,%rax), %eax
	cmpl	$14, %eax
	movq	%rdx, (%rsp)            # 8-byte Spill
	ja	.LBB37_28
# BB#9:
	movl	$26112, %ecx            # imm = 0x6600
	btl	%eax, %ecx
	jae	.LBB37_28
# BB#10:
	movl	8(%rsp), %eax
	testl	%eax, %eax
	je	.LBB37_17
# BB#11:
	leaq	8(%rsp), %rbx
	movq	active_pps(%rip), %rcx
	xorl	%ebp, %ebp
	cmpl	$0, 224(%rcx)
	je	.LBB37_15
# BB#12:
	movq	input(%rip), %rcx
	cmpl	$0, 4016(%rcx)
	je	.LBB37_15
# BB#13:
	movq	img(%rip), %rcx
	movq	14208(%rcx), %rdx
	cmpl	$0, 4(%rdx)
	movl	32(%rsp), %edx          # 4-byte Reload
	jne	.LBB37_29
# BB#14:
	leaq	8(%rsp), %rbx
	movq	14240(%rcx), %rcx
	movslq	12(%rsp), %rsi
	xorl	%ebp, %ebp
	andl	(%rcx,%rsi,4), %eax
	movl	%eax, 8(%rsp)
	sete	%bpl
	testl	%eax, %eax
	jne	.LBB37_29
	jmp	.LBB37_43
.LBB37_15:
	movl	32(%rsp), %edx          # 4-byte Reload
	jmp	.LBB37_29
.LBB37_16:
	movq	%r15, %rbp
	movq	%rsi, %r15
	movl	$0, (%rsp)              # 4-byte Folded Spill
	xorl	%r12d, %r12d
	jmp	.LBB37_20
.LBB37_17:
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	movl	32(%rsp), %edx          # 4-byte Reload
	jmp	.LBB37_30
.LBB37_18:                              # %._crit_edge64
	leaq	8(%rsp), %rdi
	xorl	%ebp, %ebp
	movl	8(%rsp), %eax
	testl	%eax, %eax
	je	.LBB37_42
.LBB37_19:                              # %.thread76
	movl	%ebp, %edx
	movq	%rdi, %rbp
	movq	%rsi, %r15
	movq	img(%rip), %rax
	movq	152(%rax), %rax
	movslq	12(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	16(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	20(%rsp), %rcx
	addq	%r12, %rcx
	movl	(%rax,%rcx,4), %r12d
	incl	%edx
	movl	%edx, (%rsp)            # 4-byte Spill
.LBB37_20:                              # %.thread80
	decl	%ebx
	leaq	8(%rsp), %rcx
	movl	%r13d, %edi
	movl	%r14d, %esi
	movl	%ebx, %edx
	callq	getChroma4x4Neighbour
	movl	(%r15), %eax
	cmpl	$14, %eax
	ja	.LBB37_37
# BB#21:                                # %.thread80
	movl	$26112, %ecx            # imm = 0x6600
	btl	%eax, %ecx
	jae	.LBB37_37
# BB#22:
	movl	(%rbp), %eax
	testl	%eax, %eax
	je	.LBB37_27
# BB#23:
	movq	active_pps(%rip), %rcx
	cmpl	$0, 224(%rcx)
	je	.LBB37_41
# BB#24:
	movq	input(%rip), %rcx
	cmpl	$0, 4016(%rcx)
	je	.LBB37_41
# BB#25:
	movq	img(%rip), %rcx
	movq	14208(%rcx), %rdx
	cmpl	$0, 4(%rdx)
	je	.LBB37_44
# BB#26:
	movl	(%rsp), %ebp            # 4-byte Reload
	jmp	.LBB37_45
.LBB37_27:
	movl	(%rsp), %ebp            # 4-byte Reload
	jmp	.LBB37_47
.LBB37_41:
	movl	(%rsp), %ebp            # 4-byte Reload
	jmp	.LBB37_45
.LBB37_28:                              # %._crit_edge
	leaq	8(%rsp), %rbx
	xorl	%ebp, %ebp
	movl	8(%rsp), %eax
	movl	32(%rsp), %edx          # 4-byte Reload
	testl	%eax, %eax
	je	.LBB37_43
.LBB37_29:                              # %.thread69
	movq	img(%rip), %rax
	movq	152(%rax), %rax
	movslq	12(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movl	%r15d, %ecx
	andl	$-2, %ecx
	movslq	16(%rsp), %rsi
	movslq	%ecx, %rcx
	addq	%rsi, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	20(%rsp), %rcx
	movl	16(%rax,%rcx,4), %r12d
	incl	%ebp
.LBB37_30:                              # %.thread73
	addl	$-17, %edx
	leaq	8(%rsp), %rcx
	movl	%r13d, %edi
	movl	%r14d, %esi
	callq	getChroma4x4Neighbour
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %eax
	cmpl	$14, %eax
	ja	.LBB37_38
# BB#31:                                # %.thread73
	movl	$26112, %ecx            # imm = 0x6600
	btl	%eax, %ecx
	jae	.LBB37_38
# BB#32:
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.LBB37_47
# BB#33:
	movq	active_pps(%rip), %rcx
	cmpl	$0, 224(%rcx)
	je	.LBB37_39
# BB#34:
	movq	input(%rip), %rcx
	cmpl	$0, 4016(%rcx)
	je	.LBB37_39
# BB#35:
	movq	img(%rip), %rcx
	movq	14208(%rcx), %rdx
	cmpl	$0, 4(%rdx)
	jne	.LBB37_39
# BB#36:
	movq	14240(%rcx), %rcx
	movslq	12(%rsp), %rdx
	andl	(%rcx,%rdx,4), %eax
	movl	%eax, (%rbx)
	cmpl	$1, %eax
	adcl	$0, %ebp
	testl	%eax, %eax
	jne	.LBB37_39
	jmp	.LBB37_47
.LBB37_37:                              # %.thread-pre-split60_crit_edge
	movl	(%rbp), %eax
	movl	(%rsp), %ebp            # 4-byte Reload
	testl	%eax, %eax
	jne	.LBB37_45
	jmp	.LBB37_47
.LBB37_38:                              # %.thread-pre-split_crit_edge
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.LBB37_47
.LBB37_39:                              # %thread-pre-split.thread
	movq	img(%rip), %rax
	movq	152(%rax), %rax
	movslq	12(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	andl	$-2, %r15d
	movslq	16(%rsp), %rcx
	movslq	%r15d, %rdx
	addq	%rcx, %rdx
	movq	(%rax,%rdx,8), %rax
	movslq	20(%rsp), %rcx
	addl	16(%rax,%rcx,4), %r12d
	jmp	.LBB37_46
.LBB37_42:
	movl	%ebp, (%rsp)            # 4-byte Spill
	movq	%rdi, %rbp
	movq	%rsi, %r15
	xorl	%r12d, %r12d
	jmp	.LBB37_20
.LBB37_43:
	xorl	%r12d, %r12d
	jmp	.LBB37_30
.LBB37_44:
	movq	14240(%rcx), %rcx
	movslq	12(%rsp), %rdx
	andl	(%rcx,%rdx,4), %eax
	movl	%eax, (%rbp)
	movl	(%rsp), %ebp            # 4-byte Reload
	decl	%ebp
	testl	%eax, %eax
	je	.LBB37_47
.LBB37_45:                              # %thread-pre-split60.thread
	movq	img(%rip), %rax
	movq	152(%rax), %rax
	movslq	12(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	16(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	20(%rsp), %rcx
	addq	32(%rsp), %rcx          # 8-byte Folded Reload
	addl	(%rax,%rcx,4), %r12d
.LBB37_46:                              # %.thread
	incl	%ebp
.LBB37_47:                              # %.thread
	leal	1(%r12), %eax
	sarl	%eax
	cmpl	$2, %ebp
	cmovnel	%r12d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end37:
	.size	predict_nnz_chroma, .Lfunc_end37-predict_nnz_chroma
	.cfi_endproc

	.globl	find_sad_16x16
	.p2align	4, 0x90
	.type	find_sad_16x16,@function
find_sad_16x16:                         # @find_sad_16x16
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi373:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi374:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi375:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi376:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi377:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi378:
	.cfi_def_cfa_offset 56
	subq	$1688, %rsp             # imm = 0x698
.Lcfi379:
	.cfi_def_cfa_offset 1744
.Lcfi380:
	.cfi_offset %rbx, -56
.Lcfi381:
	.cfi_offset %r12, -48
.Lcfi382:
	.cfi_offset %r13, -40
.Lcfi383:
	.cfi_offset %r14, -32
.Lcfi384:
	.cfi_offset %r15, -24
.Lcfi385:
	.cfi_offset %rbp, -16
	movq	%rdi, 160(%rsp)         # 8-byte Spill
	movq	img(%rip), %rax
	movl	12(%rax), %r14d
	leaq	240(%rsp), %rbx
	xorl	%ebp, %ebp
	movq	getNeighbour(%rip), %rax
	.p2align	4, 0x90
.LBB38_1:                               # =>This Inner Loop Header: Depth=1
	leal	-1(%rbp), %edx
	movl	$-1, %esi
	xorl	%ecx, %ecx
	movl	%r14d, %edi
	movq	%rbx, %r8
	callq	*%rax
	incq	%rbp
	movq	getNeighbour(%rip), %rax
	addq	$24, %rbx
	cmpq	$17, %rbp
	jne	.LBB38_1
# BB#2:
	xorl	%ebx, %ebx
	leaq	216(%rsp), %r8
	xorl	%esi, %esi
	movl	$-1, %edx
	xorl	%ecx, %ecx
	movl	%r14d, %edi
	callq	*%rax
	movq	input(%rip), %rax
	movl	216(%rsp), %r11d
	cmpl	$0, 272(%rax)
	je	.LBB38_13
# BB#3:
	testl	%r11d, %r11d
	movq	img(%rip), %rax
	je	.LBB38_5
# BB#4:
	movq	14240(%rax), %rcx
	movslq	220(%rsp), %rdx
	movl	(%rcx,%rdx,4), %ebx
.LBB38_5:                               # %._crit_edge
	movl	$1, %r10d
	movl	$52, %ecx
	.p2align	4, 0x90
.LBB38_6:                               # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	cmpl	$0, 212(%rsp,%rcx)
	movl	$0, %esi
	je	.LBB38_8
# BB#7:                                 #   in Loop: Header=BB38_6 Depth=1
	movq	14240(%rax), %rsi
	movslq	216(%rsp,%rcx), %rdi
	movl	(%rsi,%rdi,4), %esi
.LBB38_8:                               #   in Loop: Header=BB38_6 Depth=1
	andl	%r10d, %esi
	cmpl	$0, 236(%rsp,%rcx)
	je	.LBB38_10
# BB#9:                                 #   in Loop: Header=BB38_6 Depth=1
	movq	14240(%rax), %rdx
	movslq	240(%rsp,%rcx), %rdi
	movl	(%rdx,%rdi,4), %edx
.LBB38_10:                              #   in Loop: Header=BB38_6 Depth=1
	movl	%edx, %r10d
	andl	%esi, %r10d
	addq	$48, %rcx
	cmpq	$436, %rcx              # imm = 0x1B4
	jne	.LBB38_6
# BB#11:
	cmpl	$0, 240(%rsp)
	je	.LBB38_14
# BB#12:
	movq	14240(%rax), %rax
	movslq	244(%rsp), %rcx
	movl	(%rax,%rcx,4), %eax
	jmp	.LBB38_15
.LBB38_13:
	movl	240(%rsp), %eax
	movl	264(%rsp), %r10d
	jmp	.LBB38_16
.LBB38_14:
	xorl	%eax, %eax
.LBB38_15:
	movl	%ebx, %r11d
.LBB38_16:
	movq	160(%rsp), %rcx         # 8-byte Reload
	movl	$2, (%rcx)
	movq	input(%rip), %rdi
	testl	%r11d, %r11d
	setne	%cl
	testl	%r10d, %r10d
	setne	%dl
	testl	%eax, %eax
	setne	%al
	andb	%cl, %al
	andb	%dl, %al
	movb	%al, 11(%rsp)           # 1-byte Spill
	movq	imgY_org(%rip), %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movq	img(%rip), %r14
	movl	108(%rsp), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	124(%rsp), %r9d
	movl	140(%rsp), %r12d
	movl	96(%rsp), %eax
	movl	%eax, 68(%rsp)          # 4-byte Spill
	movl	100(%rsp), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	128(%rsp), %eax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movl	112(%rsp), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movl	144(%rsp), %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	132(%rsp), %r8d
	movl	116(%rsp), %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movl	148(%rsp), %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	104(%rsp), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	movl	136(%rsp), %r15d
	movl	120(%rsp), %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movl	152(%rsp), %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	156(%rsp), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	4818(%r14), %rdx
	movl	$999999, %ecx           # imm = 0xF423F
	xorl	%r13d, %r13d
	movl	%r10d, 48(%rsp)         # 4-byte Spill
	movl	%r11d, 44(%rsp)         # 4-byte Spill
	movq	%rdi, 176(%rsp)         # 8-byte Spill
	movq	%r14, 168(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB38_17:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB38_28 Depth 2
                                        #       Child Loop BB38_29 Depth 3
                                        #     Child Loop BB38_32 Depth 2
                                        #       Child Loop BB38_33 Depth 3
                                        #         Child Loop BB38_35 Depth 4
	cmpl	$0, 4048(%rdi)
	je	.LBB38_19
# BB#18:                                #   in Loop: Header=BB38_17 Depth=1
	cmpl	$2, 20(%r14)
	je	.LBB38_24
.LBB38_19:                              #   in Loop: Header=BB38_17 Depth=1
	movl	%r13d, %eax
	orl	$1, %eax
	cmpl	$1, %eax
	jne	.LBB38_22
# BB#20:                                #   in Loop: Header=BB38_17 Depth=1
	movl	4064(%rdi), %eax
	testl	%eax, %eax
	je	.LBB38_22
# BB#21:                                #   in Loop: Header=BB38_17 Depth=1
	movl	%ecx, %ebp
	jmp	.LBB38_46
	.p2align	4, 0x90
.LBB38_22:                              #   in Loop: Header=BB38_17 Depth=1
	cmpq	$3, %r13
	jne	.LBB38_24
# BB#23:                                #   in Loop: Header=BB38_17 Depth=1
	movl	4068(%rdi), %eax
	testl	%eax, %eax
	jne	.LBB38_47
.LBB38_24:                              #   in Loop: Header=BB38_17 Depth=1
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	testl	%r10d, %r10d
	setne	%bl
	cmpq	$1, %r13
	setne	%al
	cmpq	$3, %r13
	setne	%cl
	movl	%r13d, %esi
	orl	%r11d, %esi
	je	.LBB38_45
# BB#25:                                #   in Loop: Header=BB38_17 Depth=1
	orb	%al, %bl
	je	.LBB38_45
# BB#26:                                #   in Loop: Header=BB38_17 Depth=1
	orb	11(%rsp), %cl           # 1-byte Folded Reload
	je	.LBB38_45
# BB#27:                                # %.preheader270
                                        #   in Loop: Header=BB38_17 Depth=1
	movslq	192(%r14), %r8
	movslq	196(%r14), %r9
	movq	%rdx, 200(%rsp)         # 8-byte Spill
	movq	%rdx, %r10
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB38_28:                              # %.preheader265
                                        #   Parent Loop BB38_17 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB38_29 Depth 3
	leaq	(%r9,%r11), %rcx
	movq	208(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	movl	%r11d, %r14d
	andl	$3, %r14d
	movl	%r11d, %edx
	sarl	$2, %edx
	movslq	%edx, %r15
	leaq	2(%rcx,%r8,2), %r12
	movq	%r10, %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB38_29:                              #   Parent Loop BB38_17 Depth=1
                                        #     Parent Loop BB38_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	-2(%r12,%rcx,2), %eax
	movzwl	-2(%rdx), %esi
	subl	%esi, %eax
	movl	%ecx, %esi
	andl	$2, %esi
	movl	%ecx, %edi
	sarl	$2, %edi
	movslq	%edi, %rdi
	shlq	$6, %rdi
	shlq	$8, %rsi
	leaq	656(%rsp,%rsi), %rsi
	addq	%rdi, %rsi
	movq	%r14, %rbp
	shlq	$4, %rbp
	addq	%rbp, %rsi
	movl	%eax, (%rsi,%r15,4)
	leal	1(%rcx), %eax
	movzwl	(%r12,%rcx,2), %esi
	movzwl	(%rdx), %ebx
	subl	%ebx, %esi
	andl	$3, %eax
	shlq	$8, %rax
	leaq	656(%rsp,%rax), %rax
	addq	%rdi, %rax
	addq	%rbp, %rax
	movl	%esi, (%rax,%r15,4)
	addq	$2, %rcx
	addq	$4, %rdx
	cmpq	$16, %rcx
	jne	.LBB38_29
# BB#30:                                #   in Loop: Header=BB38_28 Depth=2
	incq	%r11
	addq	$32, %r10
	cmpq	$16, %r11
	jne	.LBB38_28
# BB#31:                                # %.preheader264.preheader
                                        #   in Loop: Header=BB38_17 Depth=1
	movq	%r13, 192(%rsp)         # 8-byte Spill
	xorl	%r14d, %r14d
	leaq	960(%rsp), %rax
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB38_32:                              # %.preheader264
                                        #   Parent Loop BB38_17 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB38_33 Depth 3
                                        #         Child Loop BB38_35 Depth 4
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB38_33:                              # %.preheader262
                                        #   Parent Loop BB38_17 Depth=1
                                        #     Parent Loop BB38_32 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB38_35 Depth 4
	movq	%r11, %rax
	shlq	$6, %rax
	leaq	656(%rsp,%rax), %rbx
	movl	(%rbx,%r14,4), %ecx
	movl	768(%rbx,%r14,4), %eax
	leal	(%rax,%rcx), %edx
	movl	256(%rbx,%r14,4), %esi
	movl	512(%rbx,%r14,4), %edi
	leal	(%rdi,%rsi), %ebp
	subl	%edi, %esi
	subl	%eax, %ecx
	leal	(%rbp,%rdx), %eax
	movl	%eax, (%rbx,%r14,4)
	subl	%ebp, %edx
	movl	%edx, 512(%rbx,%r14,4)
	leal	(%rsi,%rcx), %edx
	movl	%edx, 256(%rbx,%r14,4)
	subl	%esi, %ecx
	movl	%ecx, 768(%rbx,%r14,4)
	movl	16(%rbx,%r14,4), %ecx
	movl	784(%rbx,%r14,4), %r15d
	leal	(%r15,%rcx), %edi
	movl	272(%rbx,%r14,4), %ebp
	movl	528(%rbx,%r14,4), %esi
	leal	(%rsi,%rbp), %edx
	subl	%esi, %ebp
	subl	%r15d, %ecx
	leal	(%rdx,%rdi), %esi
	movl	%esi, 16(%rbx,%r14,4)
	subl	%edx, %edi
	movl	%edi, 528(%rbx,%r14,4)
	leal	(%rbp,%rcx), %edx
	movl	%edx, 272(%rbx,%r14,4)
	subl	%ebp, %ecx
	movl	%ecx, 784(%rbx,%r14,4)
	movl	32(%rbx,%r14,4), %ecx
	movl	800(%rbx,%r14,4), %r15d
	leal	(%r15,%rcx), %edi
	movl	288(%rbx,%r14,4), %ebp
	movl	544(%rbx,%r14,4), %edx
	leal	(%rdx,%rbp), %r12d
	subl	%edx, %ebp
	subl	%r15d, %ecx
	leal	(%r12,%rdi), %r10d
	movl	%r10d, 32(%rbx,%r14,4)
	subl	%r12d, %edi
	movl	%edi, 544(%rbx,%r14,4)
	leal	(%rbp,%rcx), %edi
	movl	%edi, 288(%rbx,%r14,4)
	subl	%ebp, %ecx
	movl	%ecx, 800(%rbx,%r14,4)
	movl	48(%rbx,%r14,4), %ecx
	movl	816(%rbx,%r14,4), %r12d
	leal	(%r12,%rcx), %r15d
	movq	%r9, %rdx
	movl	304(%rbx,%r14,4), %r9d
	movl	560(%rbx,%r14,4), %ebp
	leal	(%rbp,%r9), %r13d
	subl	%ebp, %r9d
	subl	%r12d, %ecx
	leal	(%r13,%r15), %ebp
	movl	%ebp, 48(%rbx,%r14,4)
	subl	%r13d, %r15d
	movl	%r15d, 560(%rbx,%r14,4)
	leal	(%r9,%rcx), %edi
	movl	%edi, 304(%rbx,%r14,4)
	subl	%r9d, %ecx
	movq	%rdx, %r9
	movl	%ecx, 816(%rbx,%r14,4)
	xorl	%ebx, %ebx
	movq	%r9, %rdi
	jmp	.LBB38_35
	.p2align	4, 0x90
.LBB38_34:                              # %._crit_edge349
                                        #   in Loop: Header=BB38_35 Depth=4
	movl	-48(%rdi), %eax
	movl	-32(%rdi), %esi
	movl	(%rdi), %ebp
	movl	-16(%rdi), %r10d
	addq	$256, %rdi              # imm = 0x100
	decq	%rbx
.LBB38_35:                              #   Parent Loop BB38_17 Depth=1
                                        #     Parent Loop BB38_32 Depth=2
                                        #       Parent Loop BB38_33 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%rbp,%rax), %ecx
	leal	(%r10,%rsi), %edx
	subl	%r10d, %esi
	subl	%ebp, %eax
	leal	(%rdx,%rcx), %ebp
	movl	%ebp, -304(%rdi)
	subl	%edx, %ecx
	movl	%ecx, -272(%rdi)
	leal	(%rsi,%rax), %edx
	movl	%edx, -288(%rdi)
	subl	%esi, %eax
	movl	%eax, -256(%rdi)
	testq	%rbx, %rbx
	je	.LBB38_37
# BB#36:                                #   in Loop: Header=BB38_35 Depth=4
	movl	%ebp, %esi
	negl	%esi
	cmovll	%ebp, %esi
	addl	%esi, %r8d
.LBB38_37:                              #   in Loop: Header=BB38_35 Depth=4
	movl	%edx, %esi
	negl	%esi
	cmovll	%edx, %esi
	addl	%r8d, %esi
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	addl	%esi, %edx
	movl	%eax, %r8d
	negl	%r8d
	cmovll	%eax, %r8d
	addl	%edx, %r8d
	cmpq	$-3, %rbx
	jne	.LBB38_34
# BB#38:                                #   in Loop: Header=BB38_33 Depth=3
	incq	%r11
	addq	$64, %r9
	cmpq	$4, %r11
	jne	.LBB38_33
# BB#39:                                #   in Loop: Header=BB38_32 Depth=2
	incq	%r14
	movq	16(%rsp), %rax          # 8-byte Reload
	addq	$4, %rax
	cmpq	$4, %r14
	jne	.LBB38_32
# BB#40:                                # %.preheader268
                                        #   in Loop: Header=BB38_17 Depth=1
	movl	656(%rsp), %ecx
	movl	660(%rsp), %eax
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	addl	%ecx, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	720(%rsp), %edx
	movl	%edx, %r14d
	sarl	$31, %r14d
	shrl	$30, %r14d
	addl	%edx, %r14d
	movl	784(%rsp), %edx
	movl	%edx, %ebp
	sarl	$31, %ebp
	shrl	$30, %ebp
	addl	%edx, %ebp
	movl	848(%rsp), %edx
	movl	%edx, %ebx
	sarl	$31, %ebx
	shrl	$30, %ebx
	addl	%edx, %ebx
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%eax, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	724(%rsp), %eax
	movl	%eax, %r11d
	sarl	$31, %r11d
	shrl	$30, %r11d
	addl	%eax, %r11d
	movl	788(%rsp), %eax
	movl	%eax, %r12d
	sarl	$31, %r12d
	shrl	$30, %r12d
	addl	%eax, %r12d
	movl	852(%rsp), %eax
	movl	%eax, %r13d
	sarl	$31, %r13d
	shrl	$30, %r13d
	addl	%eax, %r13d
	movl	664(%rsp), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%eax, %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	728(%rsp), %eax
	movl	%eax, %esi
	sarl	$31, %esi
	shrl	$30, %esi
	addl	%eax, %esi
	movl	792(%rsp), %eax
	movl	%eax, %r10d
	sarl	$31, %r10d
	shrl	$30, %r10d
	addl	%eax, %r10d
	movl	856(%rsp), %eax
	movl	%eax, %r15d
	sarl	$31, %r15d
	shrl	$30, %r15d
	addl	%eax, %r15d
	movl	668(%rsp), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%eax, %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	732(%rsp), %eax
	movl	%eax, %r9d
	sarl	$31, %r9d
	shrl	$30, %r9d
	addl	%eax, %r9d
	movl	796(%rsp), %eax
	movl	%eax, %edi
	sarl	$31, %edi
	shrl	$30, %edi
	addl	%eax, %edi
	movl	860(%rsp), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	addl	%ecx, %edx
	sarl	$2, %r14d
	sarl	$2, %ebp
	leal	(%rbp,%r14), %ecx
	subl	%ebp, %r14d
	movq	16(%rsp), %rbp          # 8-byte Reload
	sarl	$2, %ebp
	sarl	$2, %ebx
	leal	(%rbx,%rbp), %eax
	subl	%ebx, %ebp
	movq	%rbp, %rbx
	leal	(%rcx,%rax), %ebp
	subl	%ecx, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leal	(%r14,%rbx), %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	subl	%r14d, %ebx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	sarl	$2, %r11d
	sarl	$2, %r12d
	leal	(%r12,%r11), %ebx
	subl	%r12d, %r11d
	movq	24(%rsp), %rcx          # 8-byte Reload
	sarl	$2, %ecx
	sarl	$2, %r13d
	leal	(%r13,%rcx), %r12d
	subl	%r13d, %ecx
	movq	%rcx, %r13
	leal	(%rbx,%r12), %ecx
	subl	%ebx, %r12d
	leal	(%r11,%r13), %r14d
	subl	%r11d, %r13d
	sarl	$2, %esi
	sarl	$2, %r10d
	leal	(%r10,%rsi), %r11d
	subl	%r10d, %esi
	movq	32(%rsp), %rbx          # 8-byte Reload
	sarl	$2, %ebx
	sarl	$2, %r15d
	leal	(%r15,%rbx), %r10d
	subl	%r15d, %ebx
	movq	%rbx, %rax
	leal	(%r11,%r10), %r15d
	subl	%r11d, %r10d
	leal	(%rsi,%rax), %ebx
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	subl	%esi, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	sarl	$2, %r9d
	sarl	$2, %edi
	leal	(%rdi,%r9), %esi
	subl	%edi, %r9d
	movq	72(%rsp), %rdi          # 8-byte Reload
	sarl	$2, %edi
	sarl	$2, %edx
	leal	(%rdx,%rdi), %r11d
	subl	%edx, %edi
	leal	(%rsi,%r11), %eax
	subl	%esi, %r11d
	leal	(%r9,%rdi), %edx
	subl	%r9d, %edi
	movq	%rdi, %r9
	leal	(%r15,%rcx), %edi
	subl	%r15d, %ecx
	leal	(%rax,%rbp), %ebx
	subl	%eax, %ebp
	leal	(%rdi,%rbx), %esi
	movl	%esi, %eax
	negl	%eax
	movl	%esi, 68(%rsp)          # 4-byte Spill
	cmovll	%esi, %eax
	subl	%edi, %ebx
	movl	%ebx, %edi
	negl	%edi
	movq	%rbx, 184(%rsp)         # 8-byte Spill
	cmovll	%ebx, %edi
	leal	(%rcx,%rbp), %esi
	movl	%esi, %ebx
	negl	%ebx
	movl	%esi, 64(%rsp)          # 4-byte Spill
	cmovll	%esi, %ebx
	subl	%ecx, %ebp
	movl	%ebp, %ecx
	negl	%ecx
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	cmovll	%ebp, %ecx
	addl	%r8d, %eax
	addl	%ebx, %eax
	addl	%edi, %eax
	addl	%ecx, %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%r14), %edi
	subl	%ecx, %r14d
	movq	80(%rsp), %rcx          # 8-byte Reload
	leal	(%rdx,%rcx), %r8d
	subl	%edx, %ecx
	movq	%rcx, %rdx
	leal	(%rdi,%r8), %esi
	movl	%esi, %ecx
	negl	%ecx
	movl	%esi, 24(%rsp)          # 4-byte Spill
	cmovll	%esi, %ecx
	subl	%edi, %r8d
	movl	%r8d, %esi
	negl	%esi
	cmovll	%r8d, %esi
	leal	(%r14,%rdx), %ebp
	movl	%ebp, %edi
	negl	%edi
	movl	%ebp, 60(%rsp)          # 4-byte Spill
	cmovll	%ebp, %edi
	subl	%r14d, %edx
	movl	%edx, %ebx
	negl	%ebx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	cmovll	%edx, %ebx
	addl	%eax, %ecx
	addl	%edi, %ecx
	addl	%esi, %ecx
	addl	%ebx, %ecx
	leal	(%r10,%r12), %eax
	subl	%r10d, %r12d
	movq	88(%rsp), %rsi          # 8-byte Reload
	leal	(%r11,%rsi), %r15d
	subl	%r11d, %esi
	movq	%rsi, %rbp
	leal	(%rax,%r15), %edx
	movl	%edx, %esi
	negl	%esi
	movl	%edx, 56(%rsp)          # 4-byte Spill
	cmovll	%edx, %esi
	subl	%eax, %r15d
	movl	%r15d, %eax
	negl	%eax
	cmovll	%r15d, %eax
	leal	(%r12,%rbp), %edi
	movl	%edi, %edx
	negl	%edx
	movl	%edi, 52(%rsp)          # 4-byte Spill
	cmovll	%edi, %edx
	movq	%rbp, %rdi
	subl	%r12d, %edi
	movl	%edi, %ebp
	negl	%ebp
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	cmovll	%edi, %ebp
	addl	%ecx, %esi
	addl	%edx, %esi
	addl	%eax, %esi
	addl	%ebp, %esi
	movq	32(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%r13), %eax
	subl	%ecx, %r13d
	movq	16(%rsp), %rdx          # 8-byte Reload
	leal	(%r9,%rdx), %r12d
	subl	%r9d, %edx
	leal	(%rax,%r12), %ecx
	movl	%ecx, %ebp
	negl	%ebp
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	cmovll	%ecx, %ebp
	subl	%eax, %r12d
	movl	%r12d, %eax
	negl	%eax
	cmovll	%r12d, %eax
	leal	(%r13,%rdx), %r9d
	movl	%r9d, %ecx
	negl	%ecx
	cmovll	%r9d, %ecx
	subl	%r13d, %edx
	movl	%edx, %ebx
	negl	%ebx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	cmovll	%edx, %ebx
	addl	%esi, %ebp
	addl	%ecx, %ebp
	addl	%eax, %ebp
	addl	%ebx, %ebp
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, %ebp
	jge	.LBB38_43
# BB#41:                                #   in Loop: Header=BB38_17 Depth=1
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	192(%rsp), %r13         # 8-byte Reload
	movl	%r13d, (%rax)
	movl	48(%rsp), %r10d         # 4-byte Reload
	movl	44(%rsp), %r11d         # 4-byte Reload
	movq	176(%rsp), %rdi         # 8-byte Reload
	movq	168(%rsp), %r14         # 8-byte Reload
	movq	200(%rsp), %rdx         # 8-byte Reload
	jmp	.LBB38_46
	.p2align	4, 0x90
.LBB38_45:                              #   in Loop: Header=BB38_17 Depth=1
	movl	12(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB38_46
.LBB38_43:                              #   in Loop: Header=BB38_17 Depth=1
	movl	%eax, %ebp
	movl	48(%rsp), %r10d         # 4-byte Reload
	movl	44(%rsp), %r11d         # 4-byte Reload
	movq	176(%rsp), %rdi         # 8-byte Reload
	movq	168(%rsp), %r14         # 8-byte Reload
	movq	200(%rsp), %rdx         # 8-byte Reload
	movq	192(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB38_46:                              #   in Loop: Header=BB38_17 Depth=1
	incq	%r13
	addq	$512, %rdx              # imm = 0x200
	cmpq	$4, %r13
	movl	%ebp, %ecx
	jne	.LBB38_17
	jmp	.LBB38_48
.LBB38_47:
	movl	%ecx, %ebp
.LBB38_48:                              # %.thread
	movl	32(%rsp), %eax          # 4-byte Reload
	movl	%eax, 108(%rsp)
	movl	%r9d, 124(%rsp)
	movl	%r12d, 140(%rsp)
	movl	68(%rsp), %eax          # 4-byte Reload
	movl	%eax, 96(%rsp)
	movq	184(%rsp), %rax         # 8-byte Reload
	movl	%eax, 128(%rsp)
	movl	64(%rsp), %eax          # 4-byte Reload
	movl	%eax, 112(%rsp)
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	%eax, 144(%rsp)
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	%eax, 100(%rsp)
	movl	%r8d, 132(%rsp)
	movl	60(%rsp), %eax          # 4-byte Reload
	movl	%eax, 116(%rsp)
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	%eax, 148(%rsp)
	movl	56(%rsp), %eax          # 4-byte Reload
	movl	%eax, 104(%rsp)
	movl	%r15d, 136(%rsp)
	movl	52(%rsp), %eax          # 4-byte Reload
	movl	%eax, 120(%rsp)
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	%eax, 152(%rsp)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, 156(%rsp)
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	sarl	%eax
	addq	$1688, %rsp             # imm = 0x698
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end38:
	.size	find_sad_16x16, .Lfunc_end38-find_sad_16x16
	.cfi_endproc

	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Warning!!! Number of bits (%d) of macroblock_layer() data seems to exceed defined limit (%d).\n"
	.size	.L.str, 95

	.type	delta_qp_mbaff,@object  # @delta_qp_mbaff
	.comm	delta_qp_mbaff,16,16
	.type	qp_mbaff,@object        # @qp_mbaff
	.comm	qp_mbaff,16,16
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	quadratic_RC,@object    # @quadratic_RC
	.comm	quadratic_RC,8,8
	.type	updateQP,@object        # @updateQP
	.comm	updateQP,8,8
	.type	generic_RC,@object      # @generic_RC
	.comm	generic_RC,8,8
	.type	dq,@object              # @dq
	.comm	dq,4,4
	.type	predict_error,@object   # @predict_error
	.comm	predict_error,4,4
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	terminate_macroblock.skip,@object # @terminate_macroblock.skip
	.local	terminate_macroblock.skip
	.comm	terminate_macroblock.skip,1,4
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Slice Mode %d not supported"
	.size	.L.str.1, 28

	.type	width_pad,@object       # @width_pad
	.comm	width_pad,4,4
	.type	height_pad,@object      # @height_pad
	.comm	height_pad,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	LumaPrediction4x4.l0_pred,@object # @LumaPrediction4x4.l0_pred
	.local	LumaPrediction4x4.l0_pred
	.comm	LumaPrediction4x4.l0_pred,32,16
	.type	LumaPrediction4x4.l1_pred,@object # @LumaPrediction4x4.l1_pred
	.local	LumaPrediction4x4.l1_pred
	.comm	LumaPrediction4x4.l1_pred,32,16
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	LumaPrediction4x4Bi.l0_pred,@object # @LumaPrediction4x4Bi.l0_pred
	.local	LumaPrediction4x4Bi.l0_pred
	.comm	LumaPrediction4x4Bi.l0_pred,32,16
	.type	LumaPrediction4x4Bi.l1_pred,@object # @LumaPrediction4x4Bi.l1_pred
	.local	LumaPrediction4x4Bi.l1_pred
	.comm	LumaPrediction4x4Bi.l1_pred,32,16
	.type	OneComponentChromaPrediction4x4,@object # @OneComponentChromaPrediction4x4
	.local	OneComponentChromaPrediction4x4
	.comm	OneComponentChromaPrediction4x4,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	diff64,@object          # @diff64
	.local	diff64
	.comm	diff64,256,16
	.type	ChromaPrediction4x4.l0_pred,@object # @ChromaPrediction4x4.l0_pred
	.local	ChromaPrediction4x4.l0_pred
	.comm	ChromaPrediction4x4.l0_pred,32,16
	.type	ChromaPrediction4x4.l1_pred,@object # @ChromaPrediction4x4.l1_pred
	.local	ChromaPrediction4x4.l1_pred
	.comm	ChromaPrediction4x4.l1_pred,32,16
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	ChromaResidualCoding.block8x8_idx,@object # @ChromaResidualCoding.block8x8_idx
	.section	.rodata,"a",@progbits
	.p2align	4
ChromaResidualCoding.block8x8_idx:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.zero	16
	.zero	16
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.size	ChromaResidualCoding.block8x8_idx, 192

	.type	IntraChromaPrediction.block_pos,@object # @IntraChromaPrediction.block_pos
	.p2align	4
IntraChromaPrediction.block_pos:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.zero	16
	.zero	16
	.zero	16
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.zero	16
	.zero	16
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.size	IntraChromaPrediction.block_pos, 192

	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	subblk_offset_y,@object # @subblk_offset_y
	.p2align	4
subblk_offset_y:
	.ascii	"\000\000\004\004"
	.ascii	"\000\000\004\004"
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\000\000\004\004"
	.ascii	"\b\b\f\f"
	.ascii	"\000\000\004\004"
	.ascii	"\b\b\f\f"
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\000\000\004\004"
	.ascii	"\000\000\004\004"
	.ascii	"\b\b\f\f"
	.ascii	"\b\b\f\f"
	.ascii	"\000\000\004\004"
	.ascii	"\000\000\004\004"
	.ascii	"\b\b\f\f"
	.ascii	"\b\b\f\f"
	.size	subblk_offset_y, 96

	.type	subblk_offset_x,@object # @subblk_offset_x
	.p2align	4
subblk_offset_x:
	.ascii	"\000\004\000\004"
	.ascii	"\000\004\000\004"
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\000\004\000\004"
	.ascii	"\000\004\000\004"
	.ascii	"\000\004\000\004"
	.ascii	"\000\004\000\004"
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\000\004\000\004"
	.ascii	"\b\f\b\f"
	.ascii	"\000\004\000\004"
	.ascii	"\b\f\b\f"
	.ascii	"\000\004\000\004"
	.ascii	"\b\f\b\f"
	.ascii	"\000\004\000\004"
	.ascii	"\b\f\b\f"
	.size	subblk_offset_x, 96

	.type	diff,@object            # @diff
	.local	diff
	.comm	diff,64,16
	.type	MBType2Value.dir1offset,@object # @MBType2Value.dir1offset
	.p2align	2
MBType2Value.dir1offset:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.size	MBType2Value.dir1offset, 12

	.type	MBType2Value.dir2offset,@object # @MBType2Value.dir2offset
	.p2align	4
MBType2Value.dir2offset:
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	6                       # 0x6
	.long	2                       # 0x2
	.long	10                      # 0xa
	.long	12                      # 0xc
	.long	14                      # 0xe
	.long	16                      # 0x10
	.size	MBType2Value.dir2offset, 36

	.type	B8Mode2Value.b8start,@object # @B8Mode2Value.b8start
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
B8Mode2Value.b8start:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	10                      # 0xa
	.size	B8Mode2Value.b8start, 32

	.type	B8Mode2Value.b8inc,@object # @B8Mode2Value.b8inc
	.p2align	4
B8Mode2Value.b8inc:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.size	B8Mode2Value.b8inc, 32

	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	predict_nnz_chroma.j_off_tab,@object # @predict_nnz_chroma.j_off_tab
	.section	.rodata,"a",@progbits
	.p2align	4
predict_nnz_chroma.j_off_tab:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.size	predict_nnz_chroma.j_off_tab, 48

	.type	writeCoeff4x4_CAVLC.incVlc,@object # @writeCoeff4x4_CAVLC.incVlc
	.p2align	4
writeCoeff4x4_CAVLC.incVlc:
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	12                      # 0xc
	.long	24                      # 0x18
	.long	48                      # 0x30
	.long	32768                   # 0x8000
	.size	writeCoeff4x4_CAVLC.incVlc, 28

	.type	.L.str.8,@object        # @.str.8
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.8:
	.asciz	"writeCoeff4x4_CAVLC: Invalid block type"
	.size	.L.str.8, 40

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	height_pad_cr,@object   # @height_pad_cr
	.comm	height_pad_cr,4,4
	.type	width_pad_cr,@object    # @width_pad_cr
	.comm	width_pad_cr,4,4
	.type	diffy,@object           # @diffy
	.comm	diffy,1024,16
	.type	generic_RC_init,@object # @generic_RC_init
	.comm	generic_RC_init,8,8
	.type	generic_RC_best,@object # @generic_RC_best
	.comm	generic_RC_best,8,8
	.type	quadratic_RC_init,@object # @quadratic_RC_init
	.comm	quadratic_RC_init,8,8
	.type	quadratic_RC_best,@object # @quadratic_RC_best
	.comm	quadratic_RC_best,8,8
	.type	writeChromaCoeff.chroma_dc_context,@object # @writeChromaCoeff.chroma_dc_context
	.section	.rodata,"a",@progbits
	.p2align	2
writeChromaCoeff.chroma_dc_context:
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	9                       # 0x9
	.size	writeChromaCoeff.chroma_dc_context, 12

	.type	writeChromaCoeff.chroma_ac_param,@object # @writeChromaCoeff.chroma_ac_param
	.p2align	4
writeChromaCoeff.chroma_ac_param:
	.ascii	"\004\024\005\025"
	.ascii	"$4%5"
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\004\024\005\025"
	.ascii	"\006\026\007\027"
	.ascii	"$4%5"
	.ascii	"&6'7"
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\004\024\005\025"
	.ascii	"$4%5"
	.ascii	"\006\026\007\027"
	.ascii	"&6'7"
	.ascii	"\b\030\t\031"
	.ascii	"(8)9"
	.ascii	"\n\032\013\033"
	.ascii	"*:+;"
	.size	writeChromaCoeff.chroma_ac_param, 96

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"ERROR: level > 1"
	.size	.Lstr, 17


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
