	.text
	.file	"UpdateCallback.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	_ZN22CArchiveUpdateCallbackC2Ev
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallbackC2Ev,@function
_ZN22CArchiveUpdateCallbackC2Ev:        # @_ZN22CArchiveUpdateCallbackC2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$0, 32(%rbx)
	movl	$_ZTV22CArchiveUpdateCallback+160, %eax
	movd	%rax, %xmm0
	movl	$_ZTV22CArchiveUpdateCallback+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTV22CArchiveUpdateCallback+288, %eax
	movd	%rax, %xmm0
	movl	$_ZTV22CArchiveUpdateCallback+224, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 48(%rbx)
	movq	$8, 64(%rbx)
	movq	$_ZTV13CRecordVectorIyE+16, 40(%rbx)
	movdqu	%xmm0, 72(%rbx)
.Ltmp0:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp1:
# BB#1:
	movq	%r14, 72(%rbx)
	movl	$0, (%r14)
	movl	$4, 84(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 88(%rbx)
.Ltmp3:
	movl	$16, %edi
	callq	_Znam
.Ltmp4:
# BB#2:
	movq	%rax, 88(%rbx)
	movl	$0, (%rax)
	movl	$4, 100(%rbx)
	movq	$0, 104(%rbx)
	movw	$0, 112(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 136(%rbx)
	movdqu	%xmm0, 120(%rbx)
	movq	$0, 152(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB0_4:
.Ltmp5:
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	_ZdaPv
	jmp	.LBB0_5
.LBB0_3:
.Ltmp2:
	movq	%rax, %r15
.LBB0_5:                                # %_ZN11CStringBaseIwED2Ev.exit
	addq	$40, %rbx
.Ltmp6:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp7:
# BB#6:
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB0_7:
.Ltmp8:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN22CArchiveUpdateCallbackC2Ev, .Lfunc_end0-_ZN22CArchiveUpdateCallbackC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN22CArchiveUpdateCallback8SetTotalEy
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallback8SetTotalEy,@function
_ZN22CArchiveUpdateCallback8SetTotalEy: # @_ZN22CArchiveUpdateCallback8SetTotalEy
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movq	104(%rdi), %rdi
	movq	(%rdi), %rax
.Ltmp9:
	callq	*(%rax)
.Ltmp10:
.LBB2_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_1:
.Ltmp11:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB2_2
# BB#4:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB2_5
.LBB2_2:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp12:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp13:
# BB#6:
.LBB2_3:
.Ltmp14:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN22CArchiveUpdateCallback8SetTotalEy, .Lfunc_end2-_ZN22CArchiveUpdateCallback8SetTotalEy
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	3                       #   On action: 2
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp12-.Ltmp10         #   Call between .Ltmp10 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp13     #   Call between .Ltmp13 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN22CArchiveUpdateCallback12SetCompletedEPKy
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallback12SetCompletedEPKy,@function
_ZN22CArchiveUpdateCallback12SetCompletedEPKy: # @_ZN22CArchiveUpdateCallback12SetCompletedEPKy
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movq	104(%rdi), %rdi
	movq	(%rdi), %rax
.Ltmp15:
	callq	*8(%rax)
.Ltmp16:
.LBB3_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB3_1:
.Ltmp17:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB3_2
# BB#4:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB3_5
.LBB3_2:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp18:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp19:
# BB#6:
.LBB3_3:
.Ltmp20:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN22CArchiveUpdateCallback12SetCompletedEPKy, .Lfunc_end3-_ZN22CArchiveUpdateCallback12SetCompletedEPKy
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	3                       #   On action: 2
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp18-.Ltmp16         #   Call between .Ltmp16 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp19     #   Call between .Ltmp19 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN22CArchiveUpdateCallback12SetRatioInfoEPKyS1_
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallback12SetRatioInfoEPKyS1_,@function
_ZN22CArchiveUpdateCallback12SetRatioInfoEPKyS1_: # @_ZN22CArchiveUpdateCallback12SetRatioInfoEPKyS1_
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %r14, -16
	movq	104(%rdi), %rdi
	movq	(%rdi), %rax
.Ltmp21:
	callq	*16(%rax)
.Ltmp22:
.LBB4_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_1:
.Ltmp23:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB4_2
# BB#4:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB4_5
.LBB4_2:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp24:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp25:
# BB#6:
.LBB4_3:
.Ltmp26:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN22CArchiveUpdateCallback12SetRatioInfoEPKyS1_, .Lfunc_end4-_ZN22CArchiveUpdateCallback12SetRatioInfoEPKyS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp21-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin3   #     jumps to .Ltmp23
	.byte	3                       #   On action: 2
	.long	.Ltmp22-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp24-.Ltmp22         #   Call between .Ltmp22 and .Ltmp24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin3   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end4-.Ltmp25     #   Call between .Ltmp25 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn24_N22CArchiveUpdateCallback12SetRatioInfoEPKyS1_
	.p2align	4, 0x90
	.type	_ZThn24_N22CArchiveUpdateCallback12SetRatioInfoEPKyS1_,@function
_ZThn24_N22CArchiveUpdateCallback12SetRatioInfoEPKyS1_: # @_ZThn24_N22CArchiveUpdateCallback12SetRatioInfoEPKyS1_
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %r14, -16
	movq	80(%rdi), %rdi
	movq	(%rdi), %rax
.Ltmp27:
	callq	*16(%rax)
.Ltmp28:
.LBB5_5:                                # %_ZN22CArchiveUpdateCallback12SetRatioInfoEPKyS1_.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB5_1:
.Ltmp29:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB5_2
# BB#4:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB5_5
.LBB5_2:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp30:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp31:
# BB#3:
.LBB5_6:
.Ltmp32:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZThn24_N22CArchiveUpdateCallback12SetRatioInfoEPKyS1_, .Lfunc_end5-_ZThn24_N22CArchiveUpdateCallback12SetRatioInfoEPKyS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp27-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin4   #     jumps to .Ltmp29
	.byte	3                       #   On action: 2
	.long	.Ltmp28-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp30-.Ltmp28         #   Call between .Ltmp28 and .Ltmp30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin4   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp31     #   Call between .Ltmp31 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN22CArchiveUpdateCallback17GetUpdateItemInfoEjPiS0_Pj
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallback17GetUpdateItemInfoEjPiS0_Pj,@function
_ZN22CArchiveUpdateCallback17GetUpdateItemInfoEjPiS0_Pj: # @_ZN22CArchiveUpdateCallback17GetUpdateItemInfoEjPiS0_Pj
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -48
.Lcfi32:
	.cfi_offset %r12, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %r12
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	104(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp33:
	callq	*24(%rax)
	movl	%eax, %ecx
.Ltmp34:
# BB#1:
	testl	%ecx, %ecx
	je	.LBB6_2
.LBB6_15:
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_2:
	movq	136(%rbx), %rax
	movq	16(%rax), %rax
	movslq	%ebp, %rdx
	testq	%r12, %r12
	je	.LBB6_4
# BB#3:
	movq	%rdx, %rcx
	shlq	$4, %rcx
	movzbl	(%rax,%rcx), %ecx
	movl	%ecx, (%r12)
.LBB6_4:
	testq	%r15, %r15
	je	.LBB6_6
# BB#5:
	movq	%rdx, %rcx
	shlq	$4, %rcx
	movzbl	1(%rax,%rcx), %ecx
	movl	%ecx, (%r15)
.LBB6_6:
	xorl	%ecx, %ecx
	testq	%r14, %r14
	je	.LBB6_15
# BB#7:
	movl	$-1, (%r14)
	shlq	$4, %rdx
	movslq	8(%rax,%rdx), %rax
	cmpq	$-1, %rax
	je	.LBB6_15
# BB#8:
	movq	128(%rbx), %rdx
	testq	%rdx, %rdx
	je	.LBB6_10
# BB#9:
	movq	16(%rdx), %rdx
	movq	(%rdx,%rax,8), %rax
	movl	36(%rax), %eax
.LBB6_10:
	movl	%eax, (%r14)
	jmp	.LBB6_15
.LBB6_11:
.Ltmp35:
	movq	%rdx, %rbp
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %ebp
	je	.LBB6_12
# BB#14:
	callq	__cxa_end_catch
	movl	$-2147024882, %ecx      # imm = 0x8007000E
	jmp	.LBB6_15
.LBB6_12:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp36:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp37:
# BB#16:
.LBB6_13:
.Ltmp38:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN22CArchiveUpdateCallback17GetUpdateItemInfoEjPiS0_Pj, .Lfunc_end6-_ZN22CArchiveUpdateCallback17GetUpdateItemInfoEjPiS0_Pj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp33-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin5   #     jumps to .Ltmp35
	.byte	3                       #   On action: 2
	.long	.Ltmp34-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp36-.Ltmp34         #   Call between .Ltmp34 and .Ltmp36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin5   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp37     #   Call between .Ltmp37 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN22CArchiveUpdateCallback11GetPropertyEjjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallback11GetPropertyEjjP14tagPROPVARIANT,@function
_ZN22CArchiveUpdateCallback11GetPropertyEjjP14tagPROPVARIANT: # @_ZN22CArchiveUpdateCallback11GetPropertyEjjP14tagPROPVARIANT
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -24
.Lcfi40:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movl	%edx, %eax
	movq	136(%rdi), %rcx
	movq	16(%rcx), %rcx
	movslq	%esi, %rsi
	movl	$0, 8(%rsp)
	shlq	$4, %rsi
	movb	2(%rcx,%rsi), %dl
	cmpl	$21, %eax
	jne	.LBB7_5
# BB#1:
.Ltmp72:
	movzbl	%dl, %esi
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp73:
# BB#2:
	xorl	%ebx, %ebx
.Ltmp74:
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp75:
	jmp	.LBB7_39
.LBB7_5:
	testb	%dl, %dl
	je	.LBB7_12
# BB#6:
	cmpl	$3, %eax
	je	.LBB7_12
# BB#7:
	cmpl	$6, %eax
	je	.LBB7_12
# BB#8:
	cmpl	$7, %eax
	jne	.LBB7_11
# BB#9:
.Ltmp39:
	xorl	%ebx, %ebx
	leaq	8(%rsp), %rdi
	xorl	%esi, %esi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp40:
# BB#10:
.Ltmp41:
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp42:
	jmp	.LBB7_39
.LBB7_12:
	movslq	4(%rcx,%rsi), %rdx
	cmpq	$-1, %rdx
	je	.LBB7_28
# BB#13:
	addl	$-3, %eax
	cmpl	$9, %eax
	ja	.LBB7_38
# BB#14:
	movq	120(%rdi), %rcx
	movq	112(%rcx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	jmpq	*.LJTI7_0(,%rax,8)
.LBB7_15:
.Ltmp55:
	leaq	24(%rsp), %rdi
	movq	%rcx, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	_ZNK9CDirItems10GetLogPathEi
.Ltmp56:
# BB#16:
	movq	24(%rsp), %rsi
.Ltmp58:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp59:
# BB#17:
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_38
# BB#18:
	callq	_ZdaPv
	jmp	.LBB7_38
.LBB7_28:
	cmpl	$3, %eax
	jne	.LBB7_32
# BB#29:
	movslq	12(%rcx,%rsi), %rdx
	testq	%rdx, %rdx
	js	.LBB7_32
# BB#30:
	movq	144(%rdi), %rax
	movq	16(%rax), %rax
	movq	(%rax,%rdx,8), %rax
	movq	(%rax), %rsi
.Ltmp66:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp67:
# BB#31:
	xorl	%ebx, %ebx
.Ltmp68:
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp69:
	jmp	.LBB7_39
.LBB7_32:
	movslq	8(%rcx,%rsi), %rsi
	cmpq	$-1, %rsi
	je	.LBB7_38
# BB#33:
	movq	152(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB7_38
# BB#34:
	movq	128(%rdi), %rdx
	testq	%rdx, %rdx
	je	.LBB7_36
# BB#35:
	movq	16(%rdx), %rdx
	movq	(%rdx,%rsi,8), %rdx
	movl	36(%rdx), %esi
.LBB7_36:
	movq	(%rcx), %rbx
.Ltmp61:
	movq	%rcx, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%eax, %edx
	movq	%r14, %rcx
	callq	*64(%rbx)
	movl	%eax, %ebx
.Ltmp62:
	jmp	.LBB7_39
.LBB7_11:
	xorl	%ebx, %ebx
.Ltmp70:
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp71:
	jmp	.LBB7_39
.LBB7_26:
	addq	$16, %rsi
.Ltmp45:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp46:
	jmp	.LBB7_38
.LBB7_37:
	movl	48(%rsi), %esi
	andl	$16, %esi
	shrl	$4, %esi
.Ltmp53:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp54:
	jmp	.LBB7_38
.LBB7_27:
	addq	$24, %rsi
.Ltmp43:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp44:
	jmp	.LBB7_38
.LBB7_23:
	movq	(%rsi), %rsi
.Ltmp51:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp52:
	jmp	.LBB7_38
.LBB7_24:
	movl	48(%rsi), %esi
.Ltmp49:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp50:
	jmp	.LBB7_38
.LBB7_25:
	addq	$8, %rsi
.Ltmp47:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp48:
.LBB7_38:                               # %_ZN11CStringBaseIwED2Ev.exit52
	xorl	%ebx, %ebx
.Ltmp64:
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp65:
.LBB7_39:
.Ltmp80:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp81:
.LBB7_45:
	movl	%ebx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB7_20:
.Ltmp60:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_22
# BB#21:
	callq	_ZdaPv
	jmp	.LBB7_22
.LBB7_48:
.Ltmp63:
	jmp	.LBB7_4
.LBB7_19:
.Ltmp57:
	jmp	.LBB7_4
.LBB7_40:
.Ltmp82:
	movq	%rdx, %r14
	movq	%rax, %rbx
	jmp	.LBB7_41
.LBB7_3:
.Ltmp76:
.LBB7_4:                                # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rdx, %r14
	movq	%rax, %rbx
.LBB7_22:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp77:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp78:
.LBB7_41:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB7_42
# BB#44:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebx      # imm = 0x8007000E
	jmp	.LBB7_45
.LBB7_42:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp83:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp84:
# BB#47:
.LBB7_43:
.Ltmp85:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB7_46:
.Ltmp79:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN22CArchiveUpdateCallback11GetPropertyEjjP14tagPROPVARIANT, .Lfunc_end7-_ZN22CArchiveUpdateCallback11GetPropertyEjjP14tagPROPVARIANT
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_15
	.quad	.LBB7_38
	.quad	.LBB7_38
	.quad	.LBB7_37
	.quad	.LBB7_23
	.quad	.LBB7_38
	.quad	.LBB7_24
	.quad	.LBB7_25
	.quad	.LBB7_26
	.quad	.LBB7_27
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\270\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\251\001"              # Call site table length
	.long	.Ltmp72-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp42-.Ltmp72         #   Call between .Ltmp72 and .Ltmp42
	.long	.Ltmp76-.Lfunc_begin6   #     jumps to .Ltmp76
	.byte	3                       #   On action: 2
	.long	.Ltmp55-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin6   #     jumps to .Ltmp57
	.byte	3                       #   On action: 2
	.long	.Ltmp58-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin6   #     jumps to .Ltmp60
	.byte	3                       #   On action: 2
	.long	.Ltmp66-.Lfunc_begin6   # >> Call Site 4 <<
	.long	.Ltmp69-.Ltmp66         #   Call between .Ltmp66 and .Ltmp69
	.long	.Ltmp76-.Lfunc_begin6   #     jumps to .Ltmp76
	.byte	3                       #   On action: 2
	.long	.Ltmp61-.Lfunc_begin6   # >> Call Site 5 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin6   #     jumps to .Ltmp63
	.byte	3                       #   On action: 2
	.long	.Ltmp70-.Lfunc_begin6   # >> Call Site 6 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp76-.Lfunc_begin6   #     jumps to .Ltmp76
	.byte	3                       #   On action: 2
	.long	.Ltmp45-.Lfunc_begin6   # >> Call Site 7 <<
	.long	.Ltmp48-.Ltmp45         #   Call between .Ltmp45 and .Ltmp48
	.long	.Ltmp57-.Lfunc_begin6   #     jumps to .Ltmp57
	.byte	3                       #   On action: 2
	.long	.Ltmp64-.Lfunc_begin6   # >> Call Site 8 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp76-.Lfunc_begin6   #     jumps to .Ltmp76
	.byte	3                       #   On action: 2
	.long	.Ltmp80-.Lfunc_begin6   # >> Call Site 9 <<
	.long	.Ltmp81-.Ltmp80         #   Call between .Ltmp80 and .Ltmp81
	.long	.Ltmp82-.Lfunc_begin6   #     jumps to .Ltmp82
	.byte	3                       #   On action: 2
	.long	.Ltmp77-.Lfunc_begin6   # >> Call Site 10 <<
	.long	.Ltmp78-.Ltmp77         #   Call between .Ltmp77 and .Ltmp78
	.long	.Ltmp79-.Lfunc_begin6   #     jumps to .Ltmp79
	.byte	1                       #   On action: 1
	.long	.Ltmp78-.Lfunc_begin6   # >> Call Site 11 <<
	.long	.Ltmp83-.Ltmp78         #   Call between .Ltmp78 and .Ltmp83
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp83-.Lfunc_begin6   # >> Call Site 12 <<
	.long	.Ltmp84-.Ltmp83         #   Call between .Ltmp83 and .Ltmp84
	.long	.Ltmp85-.Lfunc_begin6   #     jumps to .Ltmp85
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin6   # >> Call Site 13 <<
	.long	.Lfunc_end7-.Ltmp84     #   Call between .Ltmp84 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN22CArchiveUpdateCallback9GetStreamEjPP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallback9GetStreamEjPP19ISequentialInStream,@function
_ZN22CArchiveUpdateCallback9GetStreamEjPP19ISequentialInStream: # @_ZN22CArchiveUpdateCallback9GetStreamEjPP19ISequentialInStream
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi47:
	.cfi_def_cfa_offset 96
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %r13
	movq	136(%r13), %rax
	movq	16(%rax), %r12
	movslq	%esi, %rbx
	shlq	$4, %rbx
	cmpb	$0, (%r12,%rbx)
	je	.LBB8_1
# BB#2:
	movq	104(%r13), %rdi
	movq	(%rdi), %rax
.Ltmp86:
	callq	*24(%rax)
	movl	%eax, %ebp
.Ltmp87:
# BB#3:
	testl	%ebp, %ebp
	jne	.LBB8_50
# BB#4:
	movq	104(%r13), %rdi
	movq	(%rdi), %rax
.Ltmp88:
	callq	*32(%rax)
	movl	%eax, %ebp
.Ltmp89:
# BB#5:
	testl	%ebp, %ebp
	jne	.LBB8_50
# BB#6:
	cmpb	$0, 2(%r12,%rbx)
	je	.LBB8_10
# BB#7:
	movq	104(%r13), %rdi
	movq	128(%r13), %rax
	movq	(%rdi), %rcx
	movq	16(%rax), %rax
	movslq	8(%r12,%rbx), %rdx
	movq	(%rax,%rdx,8), %rax
	movq	16(%rax), %rsi
.Ltmp90:
	movl	$1, %edx
	callq	*48(%rcx)
	movl	%eax, %ebp
.Ltmp91:
	jmp	.LBB8_50
.LBB8_1:
	movl	$-2147467259, %ebp      # imm = 0x80004005
.LBB8_50:
	movl	%ebp, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_10:
	movq	104(%r13), %rbp
	movq	120(%r13), %rsi
	movslq	4(%r12,%rbx), %rdx
	movq	112(%rsi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	(%rbp), %rax
	movq	48(%rax), %r15
.Ltmp93:
	leaq	24(%rsp), %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	_ZNK9CDirItems10GetLogPathEi
.Ltmp94:
# BB#11:
	movq	24(%rsp), %rsi
.Ltmp95:
	xorl	%edx, %edx
	movq	%rbp, %rdi
	callq	*%r15
	movl	%eax, %ebp
.Ltmp96:
# BB#12:
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_14
# BB#13:
	callq	_ZdaPv
.LBB8_14:                               # %_ZN11CStringBaseIwED2Ev.exit74
	testl	%ebp, %ebp
	jne	.LBB8_50
# BB#15:
	xorl	%ebp, %ebp
	movq	16(%rsp), %rax          # 8-byte Reload
	testb	$16, 48(%rax)
	jne	.LBB8_50
# BB#16:
	cmpb	$0, 113(%r13)
	je	.LBB8_25
# BB#17:
.Ltmp98:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp99:
# BB#18:
	movl	$0, 8(%rbx)
	movq	$_ZTV16CStdInFileStream+16, (%rbx)
.Ltmp100:
	movq	%rbx, %rdi
	callq	*_ZTV16CStdInFileStream+24(%rip)
.Ltmp101:
# BB#19:
	movq	%rbx, (%r14)
.LBB8_20:
	xorl	%ebp, %ebp
	jmp	.LBB8_50
.LBB8_25:
.Ltmp103:
	movl	$1112, %edi             # imm = 0x458
	callq	_Znwm
	movq	%rax, %r15
.Ltmp104:
# BB#26:
	movl	$0, 16(%r15)
	movl	$_ZTV13CInFileStream+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTV13CInFileStream+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r15)
	movq	$_ZTVN8NWindows5NFile3NIO9CFileBaseE+16, 24(%r15)
	movl	$-1, 32(%r15)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 40(%r15)
.Ltmp106:
	movl	$4, %edi
	callq	_Znam
.Ltmp107:
# BB#27:
	movq	%rax, 40(%r15)
	movb	$0, (%rax)
	movl	$4, 52(%r15)
	movq	$_ZTVN8NWindows5NFile3NIO7CInFileE+16, 24(%r15)
	movb	$0, 20(%r15)
.Ltmp109:
	movq	%r15, %rdi
	callq	*_ZTV13CInFileStream+24(%rip)
.Ltmp110:
# BB#28:                                # %_ZN9CMyComPtrI19ISequentialInStreamEC2EPS0_.exit78
	leaq	4(%r12,%rbx), %rax
	movq	120(%r13), %rsi
	movl	(%rax), %edx
.Ltmp112:
	movq	%rsp, %rdi
	callq	_ZNK9CDirItems10GetPhyPathEi
.Ltmp113:
# BB#29:
	movq	(%rsp), %rsi
	movzbl	112(%r13), %edx
.Ltmp115:
	movq	%r15, %rdi
	callq	_ZN13CInFileStream10OpenSharedEPKwb
.Ltmp116:
# BB#30:
	testb	%al, %al
	je	.LBB8_31
# BB#36:
	movq	%r15, (%r14)
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	jmp	.LBB8_37
.LBB8_31:
	movq	104(%r13), %rbx
	movq	(%rbx), %rax
	movq	56(%rax), %r14
	movq	(%rsp), %rbp
	callq	__errno_location
	movl	(%rax), %edx
.Ltmp117:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	*%r14
	movl	%eax, %ebp
.Ltmp118:
# BB#32:
	movl	$1, %ebx
.LBB8_37:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_39
# BB#38:
	callq	_ZdaPv
.LBB8_39:                               # %_ZN11CStringBaseIwED2Ev.exit
	testq	%r15, %r15
	je	.LBB8_41
# BB#40:
	movq	(%r15), %rax
.Ltmp123:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp124:
.LBB8_41:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit72
	testl	%ebx, %ebx
	jne	.LBB8_50
	jmp	.LBB8_20
.LBB8_42:
.Ltmp125:
	jmp	.LBB8_9
.LBB8_43:
.Ltmp114:
	movq	%rdx, %rbx
	movq	%rax, %rbp
	jmp	.LBB8_44
.LBB8_33:
.Ltmp111:
	jmp	.LBB8_9
.LBB8_45:
.Ltmp108:
	movq	%rdx, %rbx
	movq	%rax, %rbp
	movq	%r15, %rdi
	callq	_ZdlPv
	jmp	.LBB8_46
.LBB8_34:
.Ltmp119:
	movq	%rdx, %rbx
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_44
# BB#35:
	callq	_ZdaPv
.LBB8_44:
	movq	(%r15), %rax
.Ltmp120:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp121:
	jmp	.LBB8_46
.LBB8_51:
.Ltmp122:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_24:
.Ltmp102:
	jmp	.LBB8_9
.LBB8_22:
.Ltmp97:
	movq	%rdx, %rbx
	movq	%rax, %rbp
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_46
# BB#23:
	callq	_ZdaPv
	jmp	.LBB8_46
.LBB8_21:
.Ltmp105:
	jmp	.LBB8_9
.LBB8_8:
.Ltmp92:
.LBB8_9:                                # %_ZN11CStringBaseIwED2Ev.exit75
	movq	%rdx, %rbx
	movq	%rax, %rbp
.LBB8_46:                               # %_ZN11CStringBaseIwED2Ev.exit75
	movq	%rbp, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbp
	cmpl	$2, %ebx
	je	.LBB8_47
# BB#49:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB8_50
.LBB8_47:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbp, (%rax)
.Ltmp126:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp127:
# BB#52:
.LBB8_48:
.Ltmp128:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN22CArchiveUpdateCallback9GetStreamEjPP19ISequentialInStream, .Lfunc_end8-_ZN22CArchiveUpdateCallback9GetStreamEjPP19ISequentialInStream
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\322\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\303\001"              # Call site table length
	.long	.Ltmp86-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp91-.Ltmp86         #   Call between .Ltmp86 and .Ltmp91
	.long	.Ltmp92-.Lfunc_begin7   #     jumps to .Ltmp92
	.byte	3                       #   On action: 2
	.long	.Ltmp93-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Ltmp94-.Ltmp93         #   Call between .Ltmp93 and .Ltmp94
	.long	.Ltmp105-.Lfunc_begin7  #     jumps to .Ltmp105
	.byte	3                       #   On action: 2
	.long	.Ltmp95-.Lfunc_begin7   # >> Call Site 3 <<
	.long	.Ltmp96-.Ltmp95         #   Call between .Ltmp95 and .Ltmp96
	.long	.Ltmp97-.Lfunc_begin7   #     jumps to .Ltmp97
	.byte	3                       #   On action: 2
	.long	.Ltmp98-.Lfunc_begin7   # >> Call Site 4 <<
	.long	.Ltmp99-.Ltmp98         #   Call between .Ltmp98 and .Ltmp99
	.long	.Ltmp105-.Lfunc_begin7  #     jumps to .Ltmp105
	.byte	3                       #   On action: 2
	.long	.Ltmp100-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp101-.Ltmp100       #   Call between .Ltmp100 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin7  #     jumps to .Ltmp102
	.byte	3                       #   On action: 2
	.long	.Ltmp103-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin7  #     jumps to .Ltmp105
	.byte	3                       #   On action: 2
	.long	.Ltmp106-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin7  #     jumps to .Ltmp108
	.byte	3                       #   On action: 2
	.long	.Ltmp109-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Ltmp110-.Ltmp109       #   Call between .Ltmp109 and .Ltmp110
	.long	.Ltmp111-.Lfunc_begin7  #     jumps to .Ltmp111
	.byte	3                       #   On action: 2
	.long	.Ltmp112-.Lfunc_begin7  # >> Call Site 9 <<
	.long	.Ltmp113-.Ltmp112       #   Call between .Ltmp112 and .Ltmp113
	.long	.Ltmp114-.Lfunc_begin7  #     jumps to .Ltmp114
	.byte	3                       #   On action: 2
	.long	.Ltmp115-.Lfunc_begin7  # >> Call Site 10 <<
	.long	.Ltmp118-.Ltmp115       #   Call between .Ltmp115 and .Ltmp118
	.long	.Ltmp119-.Lfunc_begin7  #     jumps to .Ltmp119
	.byte	3                       #   On action: 2
	.long	.Ltmp123-.Lfunc_begin7  # >> Call Site 11 <<
	.long	.Ltmp124-.Ltmp123       #   Call between .Ltmp123 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin7  #     jumps to .Ltmp125
	.byte	3                       #   On action: 2
	.long	.Ltmp120-.Lfunc_begin7  # >> Call Site 12 <<
	.long	.Ltmp121-.Ltmp120       #   Call between .Ltmp120 and .Ltmp121
	.long	.Ltmp122-.Lfunc_begin7  #     jumps to .Ltmp122
	.byte	1                       #   On action: 1
	.long	.Ltmp121-.Lfunc_begin7  # >> Call Site 13 <<
	.long	.Ltmp126-.Ltmp121       #   Call between .Ltmp121 and .Ltmp126
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin7  # >> Call Site 14 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp128-.Lfunc_begin7  #     jumps to .Ltmp128
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin7  # >> Call Site 15 <<
	.long	.Lfunc_end8-.Ltmp127    #   Call between .Ltmp127 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN22CArchiveUpdateCallback18SetOperationResultEi
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallback18SetOperationResultEi,@function
_ZN22CArchiveUpdateCallback18SetOperationResultEi: # @_ZN22CArchiveUpdateCallback18SetOperationResultEi
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -24
.Lcfi58:
	.cfi_offset %r14, -16
	movq	104(%rdi), %rdi
	movq	(%rdi), %rax
.Ltmp129:
	callq	*64(%rax)
.Ltmp130:
.LBB9_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB9_1:
.Ltmp131:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB9_2
# BB#4:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB9_5
.LBB9_2:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp132:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp133:
# BB#6:
.LBB9_3:
.Ltmp134:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end9:
	.size	_ZN22CArchiveUpdateCallback18SetOperationResultEi, .Lfunc_end9-_ZN22CArchiveUpdateCallback18SetOperationResultEi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp129-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp130-.Ltmp129       #   Call between .Ltmp129 and .Ltmp130
	.long	.Ltmp131-.Lfunc_begin8  #     jumps to .Ltmp131
	.byte	3                       #   On action: 2
	.long	.Ltmp130-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp132-.Ltmp130       #   Call between .Ltmp130 and .Ltmp132
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp132-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp133-.Ltmp132       #   Call between .Ltmp132 and .Ltmp133
	.long	.Ltmp134-.Lfunc_begin8  #     jumps to .Ltmp134
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Lfunc_end9-.Ltmp133    #   Call between .Ltmp133 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN22CArchiveUpdateCallback13GetVolumeSizeEjPy
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallback13GetVolumeSizeEjPy,@function
_ZN22CArchiveUpdateCallback13GetVolumeSizeEjPy: # @_ZN22CArchiveUpdateCallback13GetVolumeSizeEjPy
	.cfi_startproc
# BB#0:
	movl	52(%rdi), %eax
	testl	%eax, %eax
	je	.LBB10_1
# BB#2:
	leal	-1(%rax), %ecx
	cmpl	%esi, %eax
	cmoval	%esi, %ecx
	movq	56(%rdi), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	retq
.LBB10_1:
	movl	$1, %eax
	retq
.Lfunc_end10:
	.size	_ZN22CArchiveUpdateCallback13GetVolumeSizeEjPy, .Lfunc_end10-_ZN22CArchiveUpdateCallback13GetVolumeSizeEjPy
	.cfi_endproc

	.globl	_ZN22CArchiveUpdateCallback15GetVolumeStreamEjPP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallback15GetVolumeStreamEjPP20ISequentialOutStream,@function
_ZN22CArchiveUpdateCallback15GetVolumeStreamEjPP20ISequentialOutStream: # @_ZN22CArchiveUpdateCallback15GetVolumeStreamEjPP20ISequentialOutStream
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi65:
	.cfi_def_cfa_offset 192
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movq	%rdx, 56(%rsp)          # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leal	1(%rsi), %edi
.Ltmp135:
	leaq	64(%rsp), %rbx
	movq	%rbx, %rsi
	callq	_Z21ConvertUInt32ToStringjPw
.Ltmp136:
# BB#1:                                 # %.preheader.preheader
	movabsq	$-4294967296, %rbp      # imm = 0xFFFFFFFF00000000
	movl	$-1, %r14d
	movabsq	$4294967296, %rax       # imm = 0x100000000
	.p2align	4, 0x90
.LBB11_2:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	addq	%rax, %rbp
	incl	%r14d
	cmpl	$0, (%rbx)
	leaq	4(%rbx), %rbx
	jne	.LBB11_2
# BB#3:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%r14), %ebx
	movslq	%ebx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp137:
	callq	_Znam
	movq	%rax, %r15
.Ltmp138:
# BB#4:                                 # %.noexc
	movl	$0, (%r15)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	64(%rsp,%rax), %ecx
	movl	%ecx, (%r15,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB11_5
# BB#6:                                 # %_ZN11CStringBaseIwEC2EPKw.exit.preheader
	cmpl	$1, %r14d
	jg	.LBB11_7
# BB#8:                                 # %.lr.ph.preheader
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	sarq	$32, %rbp
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB11_9:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_12 Depth 2
                                        #     Child Loop BB11_17 Depth 2
                                        #     Child Loop BB11_25 Depth 2
.Ltmp140:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp141:
# BB#10:                                # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB11_9 Depth=1
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	$48, (%r14)
.Ltmp143:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp144:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB11_9 Depth=1
	movl	$48, (%rbx)
	movl	$4, %eax
	.p2align	4, 0x90
.LBB11_12:                              # %._crit_edge
                                        #   Parent Loop BB11_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r14,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB11_12
# BB#13:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
                                        #   in Loop: Header=BB11_9 Depth=1
	testq	%rbp, %rbp
	movl	8(%rsp), %r13d          # 4-byte Reload
	jle	.LBB11_16
# BB#14:                                #   in Loop: Header=BB11_9 Depth=1
.Ltmp146:
	movl	$28, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp147:
# BB#15:                                # %.lr.ph.i.i98
                                        #   in Loop: Header=BB11_9 Depth=1
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	movq	%rbx, %rdi
	callq	_ZdaPv
	movl	$0, 4(%r12)
	movq	%r12, %rbx
.LBB11_16:                              # %.noexc.i
                                        #   in Loop: Header=BB11_9 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_17:                              #   Parent Loop BB11_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r15,%rcx), %eax
	movl	%eax, 4(%rbx,%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB11_17
# BB#18:                                #   in Loop: Header=BB11_9 Depth=1
	movl	$0, (%r15)
	leaq	2(%rbp), %r12
	cmpl	%r13d, %r12d
	jne	.LBB11_20
# BB#19:                                #   in Loop: Header=BB11_9 Depth=1
	movl	%r13d, %r12d
	movq	16(%rsp), %r13          # 8-byte Reload
	jmp	.LBB11_24
	.p2align	4, 0x90
.LBB11_20:                              #   in Loop: Header=BB11_9 Depth=1
	movq	%r12, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp149:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp150:
# BB#21:                                # %.noexc30
                                        #   in Loop: Header=BB11_9 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB11_23
# BB#22:                                # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB11_9 Depth=1
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB11_23:                              # %._crit_edge16.i.i
                                        #   in Loop: Header=BB11_9 Depth=1
	movl	$0, (%r13)
	movq	%r13, %r15
.LBB11_24:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i27
                                        #   in Loop: Header=BB11_9 Depth=1
	incq	%rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_25:                              #   Parent Loop BB11_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rax), %ecx
	movl	%ecx, (%r15,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB11_25
# BB#26:                                # %_ZN11CStringBaseIwEaSERKS0_.exit
                                        #   in Loop: Header=BB11_9 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	_ZdaPv
	cmpq	$2, %rbp
	movq	%r13, %rbx
	movl	%r12d, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	jl	.LBB11_9
# BB#27:                                # %_ZN11CStringBaseIwEC2EPKw.exit._crit_edge.loopexit
	movl	%ebp, %r14d
	jmp	.LBB11_28
.LBB11_7:
	movq	%r15, %r13
.LBB11_28:                              # %_ZN11CStringBaseIwEC2EPKw.exit._crit_edge
	movq	32(%rsp), %rcx          # 8-byte Reload
	movslq	80(%rcx), %rbp
	leaq	1(%rbp), %r9
	testl	%r9d, %r9d
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%r13, 16(%rsp)          # 8-byte Spill
	je	.LBB11_29
# BB#37:                                # %._crit_edge16.i.i34
	movl	$4, %ecx
	movq	%r9, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp152:
	movq	%r9, %rbp
	callq	_Znam
	movq	%rax, %rdi
.Ltmp153:
# BB#38:                                # %.noexc38
	movl	$0, (%rdi)
	movl	%ebp, %ebx
	movq	%rdi, %r13
	movq	%rbp, %r9
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB11_39
.LBB11_29:
	xorl	%ebx, %ebx
	xorl	%edi, %edi
	xorl	%r13d, %r13d
.LBB11_39:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i35
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	72(%rax), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_40:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rcx), %edx
	movl	%edx, (%r13,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB11_40
# BB#41:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB11_42
# BB#43:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB11_45
# BB#44:                                # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB11_45:                              # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rbx,%rsi), %r12d
	cmpl	%ebx, %r12d
	jne	.LBB11_46
.LBB11_42:
	movl	%ebx, %r12d
	movq	%rdi, %rax
	jmp	.LBB11_53
.LBB11_46:
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%rdi, (%rsp)            # 8-byte Spill
	movslq	%r12d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp155:
	callq	_Znam
.Ltmp156:
# BB#47:                                # %.noexc46
	testl	%ebx, %ebx
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	jle	.LBB11_52
# BB#48:                                # %.preheader.i.i
	testl	%ebp, %ebp
	jle	.LBB11_50
# BB#49:                                # %.lr.ph.i.i
	leaq	(,%rbp,4), %rdx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	movq	%rax, %rbx
	callq	memcpy
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rbx, %rax
	jmp	.LBB11_51
.LBB11_50:                              # %._crit_edge.i.i
	testq	%r13, %r13
	je	.LBB11_52
.LBB11_51:                              # %._crit_edge.thread.i.i43
	movq	%rcx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	%rbx, %rax
.LBB11_52:                              # %._crit_edge16.i.i44
	movl	$0, (%rax,%rbp,4)
	movq	%rax, %r13
.LBB11_53:
	movl	$46, (%r13,%rbp,4)
	movslq	%r9d, %r8
	movl	$0, (%r13,%r8,4)
	movl	%r12d, %edi
	subl	%r9d, %edi
	cmpl	%r14d, %edi
	jg	.LBB11_54
# BB#55:
	decl	%edi
	cmpl	$8, %r12d
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %r12d
	jl	.LBB11_57
# BB#56:                                # %select.true.sink561
	movl	%r12d, %ecx
	shrl	$31, %ecx
	addl	%r12d, %ecx
	sarl	%ecx
.LBB11_57:                              # %select.end560
	leal	(%rcx,%rdi), %edx
	movl	%r14d, %esi
	subl	%edi, %esi
	cmpl	%r14d, %edx
	cmovgel	%ecx, %esi
	leal	1(%r12,%rsi), %ecx
	cmpl	%r12d, %ecx
	jne	.LBB11_58
.LBB11_54:
                                        # kill: %R12D<def> %R12D<kill> %R12<kill> %R12<def>
	movq	%rax, %rbx
	jmp	.LBB11_83
.LBB11_58:
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movslq	%ecx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp158:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp159:
# BB#59:                                # %.noexc63
	testl	%r12d, %r12d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	jle	.LBB11_82
# BB#60:                                # %.preheader.i.i53
	testl	%eax, %eax
	js	.LBB11_80
# BB#61:                                # %.lr.ph.i.i54.preheader
	cmpl	$7, %r9d
	jbe	.LBB11_62
# BB#69:                                # %min.iters.checked
	movq	%r8, %rax
	andq	$-8, %rax
	je	.LBB11_62
# BB#70:                                # %vector.memcheck
	leaq	(%r13,%r8,4), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB11_72
# BB#71:                                # %vector.memcheck
	leaq	(%rbx,%r8,4), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB11_72
.LBB11_62:
	xorl	%eax, %eax
.LBB11_63:                              # %.lr.ph.i.i54.preheader313
	movl	%r9d, %edx
	subl	%eax, %edx
	leaq	-1(%r8), %rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB11_66
# BB#64:                                # %.lr.ph.i.i54.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB11_65:                              # %.lr.ph.i.i54.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rax,4), %esi
	movl	%esi, (%rbx,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB11_65
.LBB11_66:                              # %.lr.ph.i.i54.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB11_81
# BB#67:                                # %.lr.ph.i.i54.preheader313.new
	movq	%r8, %rcx
	subq	%rax, %rcx
	leaq	28(%rbx,%rax,4), %rdx
	leaq	28(%r13,%rax,4), %rax
	.p2align	4, 0x90
.LBB11_68:                              # %.lr.ph.i.i54
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rax), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rcx
	jne	.LBB11_68
	jmp	.LBB11_81
.LBB11_80:                              # %._crit_edge.i.i55
	testq	%r13, %r13
	je	.LBB11_82
.LBB11_81:                              # %._crit_edge.thread.i.i60
	callq	_ZdaPv
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
.LBB11_82:                              # %._crit_edge16.i.i61
	movl	$0, (%rbx,%r8,4)
	movq	%rbx, %r13
.LBB11_83:                              # %.noexc49
	leaq	(%r13,%r8,4), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_84:                              # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB11_84
# BB#85:
	addl	%r9d, %r14d
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	96(%rax), %eax
	movl	%r12d, %ecx
	subl	%r14d, %ecx
	cmpl	%eax, %ecx
	jle	.LBB11_87
# BB#86:
	movq	%rbx, %rax
	jmp	.LBB11_116
.LBB11_87:
	decl	%ecx
	cmpl	$8, %r12d
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %r12d
	jl	.LBB11_89
# BB#88:                                # %select.true.sink581
	movl	%r12d, %edx
	shrl	$31, %edx
	addl	%r12d, %edx
	sarl	%edx
.LBB11_89:                              # %select.end580
	leal	(%rdx,%rcx), %esi
	movl	%eax, %edi
	subl	%ecx, %edi
	cmpl	%eax, %esi
	cmovgel	%edx, %edi
	leal	1(%r12,%rdi), %eax
	cmpl	%r12d, %eax
	jne	.LBB11_91
# BB#90:
	movq	%rbx, %rax
	jmp	.LBB11_116
.LBB11_91:
	movq	%rbx, (%rsp)            # 8-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp160:
	callq	_Znam
.Ltmp161:
# BB#92:                                # %.noexc82
	testl	%r12d, %r12d
	movq	(%rsp), %rdi            # 8-byte Reload
	jle	.LBB11_115
# BB#93:                                # %.preheader.i.i72
	testl	%r14d, %r14d
	jle	.LBB11_113
# BB#94:                                # %.lr.ph.i.i73
	movslq	%r14d, %rbp
	cmpl	$7, %r14d
	jbe	.LBB11_95
# BB#102:                               # %min.iters.checked288
	movq	%rbp, %rcx
	andq	$-8, %rcx
	je	.LBB11_95
# BB#103:                               # %vector.memcheck301
	leaq	(%r13,%rbp,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB11_105
# BB#104:                               # %vector.memcheck301
	leaq	(%rax,%rbp,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB11_105
.LBB11_95:
	xorl	%ecx, %ecx
.LBB11_96:                              # %scalar.ph286.preheader
	movl	%r14d, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB11_99
# BB#97:                                # %scalar.ph286.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB11_98:                              # %scalar.ph286.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rcx,4), %ebx
	movl	%ebx, (%rax,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB11_98
.LBB11_99:                              # %scalar.ph286.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB11_114
# BB#100:                               # %scalar.ph286.preheader.new
	subq	%rcx, %rbp
	leaq	28(%rax,%rcx,4), %rdx
	leaq	28(%r13,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB11_101:                             # %scalar.ph286
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB11_101
	jmp	.LBB11_114
.LBB11_113:                             # %._crit_edge.i.i74
	testq	%r13, %r13
	je	.LBB11_115
.LBB11_114:                             # %._crit_edge.thread.i.i79
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
.LBB11_115:                             # %._crit_edge16.i.i80
	movslq	%r14d, %rcx
	movl	$0, (%rax,%rcx,4)
	movq	%rax, %r13
.LBB11_116:                             # %.noexc67
	movq	%rax, (%rsp)            # 8-byte Spill
	movslq	%r14d, %rax
	leaq	(%r13,%rax,4), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	88(%rcx), %rcx
	.p2align	4, 0x90
.LBB11_117:                             # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB11_117
# BB#118:
.Ltmp163:
	movl	$1112, %edi             # imm = 0x458
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp164:
# BB#119:
	movl	$0, 8(%rbx)
	movq	$_ZTV14COutFileStream+16, (%rbx)
	movq	$_ZTVN8NWindows5NFile3NIO9CFileBaseE+16, 16(%rbx)
	movl	$-1, 24(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
.Ltmp166:
	movl	$4, %edi
	callq	_Znam
.Ltmp167:
# BB#120:
	movq	%rax, 32(%rbx)
	movb	$0, (%rax)
	movl	$4, 44(%rbx)
	movq	$_ZTVN8NWindows5NFile3NIO8COutFileE+16, 16(%rbx)
.Ltmp169:
	movq	%rbx, %rdi
	callq	*_ZTV14COutFileStream+24(%rip)
.Ltmp170:
# BB#121:                               # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit
	movq	%rbx, %rdi
	addq	$16, %rdi
	movq	$0, 1104(%rbx)
.Ltmp172:
	xorl	%edx, %edx
	movq	%r13, %rsi
	callq	_ZN8NWindows5NFile3NIO8COutFile6CreateEPKwb
.Ltmp173:
# BB#122:                               # %_ZN14COutFileStream6CreateEPKwb.exit
	testb	%al, %al
	je	.LBB11_130
# BB#123:                               # %.thread
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rbx, (%rax)
	xorl	%ebp, %ebp
	testq	%r13, %r13
	jne	.LBB11_132
	jmp	.LBB11_133
.LBB11_130:
	callq	__errno_location
	movl	(%rax), %ebp
	movq	(%rbx), %rax
.Ltmp178:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp179:
# BB#131:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit89
	testq	%r13, %r13
	je	.LBB11_133
.LBB11_132:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZdaPv
.LBB11_133:                             # %_ZN11CStringBaseIwED2Ev.exit90
	testq	%r15, %r15
	je	.LBB11_146
# BB#134:
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB11_146
.LBB11_72:                              # %vector.body.preheader
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB11_73
# BB#74:                                # %vector.body.prol.preheader
	negq	%rdx
	xorl	%esi, %esi
.LBB11_75:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r13,%rsi,4), %xmm0
	movups	16(%r13,%rsi,4), %xmm1
	movups	%xmm0, (%rbx,%rsi,4)
	movups	%xmm1, 16(%rbx,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB11_75
	jmp	.LBB11_76
.LBB11_105:                             # %vector.body284.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB11_106
# BB#107:                               # %vector.body284.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
.LBB11_108:                             # %vector.body284.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r13,%rbx,4), %xmm0
	movups	16(%r13,%rbx,4), %xmm1
	movups	%xmm0, (%rax,%rbx,4)
	movups	%xmm1, 16(%rax,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB11_108
	jmp	.LBB11_109
.LBB11_73:
	xorl	%esi, %esi
.LBB11_76:                              # %vector.body.prol.loopexit
	cmpq	$24, %rcx
	jb	.LBB11_79
# BB#77:                                # %vector.body.preheader.new
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%rbx,%rsi,4), %rdx
	leaq	112(%r13,%rsi,4), %rsi
.LBB11_78:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB11_78
.LBB11_79:                              # %middle.block
	cmpq	%rax, %r8
	jne	.LBB11_63
	jmp	.LBB11_81
.LBB11_106:
	xorl	%ebx, %ebx
.LBB11_109:                             # %vector.body284.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB11_112
# BB#110:                               # %vector.body284.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%rax,%rbx,4), %rsi
	leaq	112(%r13,%rbx,4), %rbx
.LBB11_111:                             # %vector.body284
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB11_111
.LBB11_112:                             # %middle.block285
	cmpq	%rcx, %rbp
	jne	.LBB11_96
	jmp	.LBB11_114
.LBB11_124:
.Ltmp162:
	jmp	.LBB11_136
.LBB11_125:
.Ltmp157:
	jmp	.LBB11_136
.LBB11_135:
.Ltmp180:
	jmp	.LBB11_136
.LBB11_31:
.Ltmp154:
	movq	%rdx, %rbp
	movq	%rax, %r12
	jmp	.LBB11_139
.LBB11_129:
.Ltmp174:
	movq	%rdx, %rbp
	movq	%rax, %r12
	movq	(%rbx), %rax
.Ltmp175:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp176:
	jmp	.LBB11_137
.LBB11_147:
.Ltmp177:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB11_128:
.Ltmp171:
	jmp	.LBB11_136
.LBB11_127:
.Ltmp168:
	movq	%rdx, %rbp
	movq	%rax, %r12
	movq	%rbx, %rdi
	callq	_ZdlPv
	testq	%r13, %r13
	jne	.LBB11_138
	jmp	.LBB11_139
.LBB11_126:
.Ltmp165:
.LBB11_136:                             # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	%rdx, %rbp
	movq	%rax, %r12
.LBB11_137:                             # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	testq	%r13, %r13
	je	.LBB11_139
.LBB11_138:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZdaPv
.LBB11_139:                             # %_ZN11CStringBaseIwED2Ev.exit92
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%r15, %r15
	jne	.LBB11_141
	jmp	.LBB11_142
.LBB11_30:
.Ltmp139:
	movq	%rdx, %rbp
	movq	%rax, %r12
	jmp	.LBB11_142
.LBB11_34:
.Ltmp151:
	jmp	.LBB11_35
.LBB11_149:                             # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp148:
.LBB11_35:                              # %_ZN11CStringBaseIwED2Ev.exit33
	movq	%rdx, %rbp
	movq	%rax, %r12
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB11_36
.LBB11_33:
.Ltmp145:
	movq	%rdx, %rbp
	movq	%rax, %r12
.LBB11_36:                              # %_ZN11CStringBaseIwED2Ev.exit33
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%r14, %rdi
	callq	_ZdaPv
	testq	%r15, %r15
	jne	.LBB11_141
	jmp	.LBB11_142
.LBB11_32:
.Ltmp142:
	movq	%rdx, %rbp
	movq	%rax, %r12
	testq	%r15, %r15
	je	.LBB11_142
.LBB11_141:
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB11_142:                             # %_ZN11CStringBaseIwED2Ev.exit93
	movq	%r12, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %ebp
	je	.LBB11_143
# BB#145:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
.LBB11_146:
	movl	%ebp, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_143:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp181:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp182:
# BB#148:
.LBB11_144:
.Ltmp183:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZN22CArchiveUpdateCallback15GetVolumeStreamEjPP20ISequentialOutStream, .Lfunc_end11-_ZN22CArchiveUpdateCallback15GetVolumeStreamEjPP20ISequentialOutStream
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\206\202\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\367\001"              # Call site table length
	.long	.Ltmp135-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp138-.Ltmp135       #   Call between .Ltmp135 and .Ltmp138
	.long	.Ltmp139-.Lfunc_begin9  #     jumps to .Ltmp139
	.byte	3                       #   On action: 2
	.long	.Ltmp140-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp141-.Ltmp140       #   Call between .Ltmp140 and .Ltmp141
	.long	.Ltmp142-.Lfunc_begin9  #     jumps to .Ltmp142
	.byte	3                       #   On action: 2
	.long	.Ltmp143-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp144-.Ltmp143       #   Call between .Ltmp143 and .Ltmp144
	.long	.Ltmp145-.Lfunc_begin9  #     jumps to .Ltmp145
	.byte	3                       #   On action: 2
	.long	.Ltmp146-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Ltmp147-.Ltmp146       #   Call between .Ltmp146 and .Ltmp147
	.long	.Ltmp148-.Lfunc_begin9  #     jumps to .Ltmp148
	.byte	3                       #   On action: 2
	.long	.Ltmp149-.Lfunc_begin9  # >> Call Site 5 <<
	.long	.Ltmp150-.Ltmp149       #   Call between .Ltmp149 and .Ltmp150
	.long	.Ltmp151-.Lfunc_begin9  #     jumps to .Ltmp151
	.byte	3                       #   On action: 2
	.long	.Ltmp152-.Lfunc_begin9  # >> Call Site 6 <<
	.long	.Ltmp153-.Ltmp152       #   Call between .Ltmp152 and .Ltmp153
	.long	.Ltmp154-.Lfunc_begin9  #     jumps to .Ltmp154
	.byte	3                       #   On action: 2
	.long	.Ltmp155-.Lfunc_begin9  # >> Call Site 7 <<
	.long	.Ltmp156-.Ltmp155       #   Call between .Ltmp155 and .Ltmp156
	.long	.Ltmp157-.Lfunc_begin9  #     jumps to .Ltmp157
	.byte	3                       #   On action: 2
	.long	.Ltmp156-.Lfunc_begin9  # >> Call Site 8 <<
	.long	.Ltmp158-.Ltmp156       #   Call between .Ltmp156 and .Ltmp158
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp158-.Lfunc_begin9  # >> Call Site 9 <<
	.long	.Ltmp159-.Ltmp158       #   Call between .Ltmp158 and .Ltmp159
	.long	.Ltmp165-.Lfunc_begin9  #     jumps to .Ltmp165
	.byte	3                       #   On action: 2
	.long	.Ltmp160-.Lfunc_begin9  # >> Call Site 10 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp162-.Lfunc_begin9  #     jumps to .Ltmp162
	.byte	3                       #   On action: 2
	.long	.Ltmp163-.Lfunc_begin9  # >> Call Site 11 <<
	.long	.Ltmp164-.Ltmp163       #   Call between .Ltmp163 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin9  #     jumps to .Ltmp165
	.byte	3                       #   On action: 2
	.long	.Ltmp166-.Lfunc_begin9  # >> Call Site 12 <<
	.long	.Ltmp167-.Ltmp166       #   Call between .Ltmp166 and .Ltmp167
	.long	.Ltmp168-.Lfunc_begin9  #     jumps to .Ltmp168
	.byte	3                       #   On action: 2
	.long	.Ltmp169-.Lfunc_begin9  # >> Call Site 13 <<
	.long	.Ltmp170-.Ltmp169       #   Call between .Ltmp169 and .Ltmp170
	.long	.Ltmp171-.Lfunc_begin9  #     jumps to .Ltmp171
	.byte	3                       #   On action: 2
	.long	.Ltmp172-.Lfunc_begin9  # >> Call Site 14 <<
	.long	.Ltmp173-.Ltmp172       #   Call between .Ltmp172 and .Ltmp173
	.long	.Ltmp174-.Lfunc_begin9  #     jumps to .Ltmp174
	.byte	3                       #   On action: 2
	.long	.Ltmp178-.Lfunc_begin9  # >> Call Site 15 <<
	.long	.Ltmp179-.Ltmp178       #   Call between .Ltmp178 and .Ltmp179
	.long	.Ltmp180-.Lfunc_begin9  #     jumps to .Ltmp180
	.byte	3                       #   On action: 2
	.long	.Ltmp175-.Lfunc_begin9  # >> Call Site 16 <<
	.long	.Ltmp176-.Ltmp175       #   Call between .Ltmp175 and .Ltmp176
	.long	.Ltmp177-.Lfunc_begin9  #     jumps to .Ltmp177
	.byte	1                       #   On action: 1
	.long	.Ltmp176-.Lfunc_begin9  # >> Call Site 17 <<
	.long	.Ltmp181-.Ltmp176       #   Call between .Ltmp176 and .Ltmp181
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp181-.Lfunc_begin9  # >> Call Site 18 <<
	.long	.Ltmp182-.Ltmp181       #   Call between .Ltmp181 and .Ltmp182
	.long	.Ltmp183-.Lfunc_begin9  #     jumps to .Ltmp183
	.byte	0                       #   On action: cleanup
	.long	.Ltmp182-.Lfunc_begin9  # >> Call Site 19 <<
	.long	.Lfunc_end11-.Ltmp182   #   Call between .Ltmp182 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN22CArchiveUpdateCallback22CryptoGetTextPassword2EPiPPw
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallback22CryptoGetTextPassword2EPiPPw,@function
_ZN22CArchiveUpdateCallback22CryptoGetTextPassword2EPiPPw: # @_ZN22CArchiveUpdateCallback22CryptoGetTextPassword2EPiPPw
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi74:
	.cfi_def_cfa_offset 32
.Lcfi75:
	.cfi_offset %rbx, -24
.Lcfi76:
	.cfi_offset %r14, -16
	movq	104(%rdi), %rdi
	movq	(%rdi), %rax
.Ltmp184:
	callq	*72(%rax)
.Ltmp185:
.LBB12_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB12_1:
.Ltmp186:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB12_2
# BB#4:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB12_5
.LBB12_2:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp187:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp188:
# BB#6:
.LBB12_3:
.Ltmp189:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN22CArchiveUpdateCallback22CryptoGetTextPassword2EPiPPw, .Lfunc_end12-_ZN22CArchiveUpdateCallback22CryptoGetTextPassword2EPiPPw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp184-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp185-.Ltmp184       #   Call between .Ltmp184 and .Ltmp185
	.long	.Ltmp186-.Lfunc_begin10 #     jumps to .Ltmp186
	.byte	3                       #   On action: 2
	.long	.Ltmp185-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp187-.Ltmp185       #   Call between .Ltmp185 and .Ltmp187
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp187-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp188-.Ltmp187       #   Call between .Ltmp187 and .Ltmp188
	.long	.Ltmp189-.Lfunc_begin10 #     jumps to .Ltmp189
	.byte	0                       #   On action: cleanup
	.long	.Ltmp188-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Lfunc_end12-.Ltmp188   #   Call between .Ltmp188 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn8_N22CArchiveUpdateCallback22CryptoGetTextPassword2EPiPPw
	.p2align	4, 0x90
	.type	_ZThn8_N22CArchiveUpdateCallback22CryptoGetTextPassword2EPiPPw,@function
_ZThn8_N22CArchiveUpdateCallback22CryptoGetTextPassword2EPiPPw: # @_ZThn8_N22CArchiveUpdateCallback22CryptoGetTextPassword2EPiPPw
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi79:
	.cfi_def_cfa_offset 32
.Lcfi80:
	.cfi_offset %rbx, -24
.Lcfi81:
	.cfi_offset %r14, -16
	movq	96(%rdi), %rdi
	movq	(%rdi), %rax
.Ltmp190:
	callq	*72(%rax)
.Ltmp191:
.LBB13_5:                               # %_ZN22CArchiveUpdateCallback22CryptoGetTextPassword2EPiPPw.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB13_1:
.Ltmp192:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB13_2
# BB#4:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB13_5
.LBB13_2:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp193:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp194:
# BB#3:
.LBB13_6:
.Ltmp195:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZThn8_N22CArchiveUpdateCallback22CryptoGetTextPassword2EPiPPw, .Lfunc_end13-_ZThn8_N22CArchiveUpdateCallback22CryptoGetTextPassword2EPiPPw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp190-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp191-.Ltmp190       #   Call between .Ltmp190 and .Ltmp191
	.long	.Ltmp192-.Lfunc_begin11 #     jumps to .Ltmp192
	.byte	3                       #   On action: 2
	.long	.Ltmp191-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp193-.Ltmp191       #   Call between .Ltmp191 and .Ltmp193
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp193-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp194-.Ltmp193       #   Call between .Ltmp193 and .Ltmp194
	.long	.Ltmp195-.Lfunc_begin11 #     jumps to .Ltmp195
	.byte	0                       #   On action: cleanup
	.long	.Ltmp194-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Lfunc_end13-.Ltmp194   #   Call between .Ltmp194 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN22CArchiveUpdateCallback21CryptoGetTextPasswordEPPw
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallback21CryptoGetTextPasswordEPPw,@function
_ZN22CArchiveUpdateCallback21CryptoGetTextPasswordEPPw: # @_ZN22CArchiveUpdateCallback21CryptoGetTextPasswordEPPw
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 32
.Lcfi85:
	.cfi_offset %rbx, -24
.Lcfi86:
	.cfi_offset %r14, -16
	movq	104(%rdi), %rdi
	movq	(%rdi), %rax
.Ltmp196:
	callq	*80(%rax)
.Ltmp197:
.LBB14_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB14_1:
.Ltmp198:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB14_2
# BB#4:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB14_5
.LBB14_2:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp199:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp200:
# BB#6:
.LBB14_3:
.Ltmp201:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN22CArchiveUpdateCallback21CryptoGetTextPasswordEPPw, .Lfunc_end14-_ZN22CArchiveUpdateCallback21CryptoGetTextPasswordEPPw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp196-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp197-.Ltmp196       #   Call between .Ltmp196 and .Ltmp197
	.long	.Ltmp198-.Lfunc_begin12 #     jumps to .Ltmp198
	.byte	3                       #   On action: 2
	.long	.Ltmp197-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp199-.Ltmp197       #   Call between .Ltmp197 and .Ltmp199
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp199-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp200-.Ltmp199       #   Call between .Ltmp199 and .Ltmp200
	.long	.Ltmp201-.Lfunc_begin12 #     jumps to .Ltmp201
	.byte	0                       #   On action: cleanup
	.long	.Ltmp200-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Lfunc_end14-.Ltmp200   #   Call between .Ltmp200 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn16_N22CArchiveUpdateCallback21CryptoGetTextPasswordEPPw
	.p2align	4, 0x90
	.type	_ZThn16_N22CArchiveUpdateCallback21CryptoGetTextPasswordEPPw,@function
_ZThn16_N22CArchiveUpdateCallback21CryptoGetTextPasswordEPPw: # @_ZThn16_N22CArchiveUpdateCallback21CryptoGetTextPasswordEPPw
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 32
.Lcfi90:
	.cfi_offset %rbx, -24
.Lcfi91:
	.cfi_offset %r14, -16
	movq	88(%rdi), %rdi
	movq	(%rdi), %rax
.Ltmp202:
	callq	*80(%rax)
.Ltmp203:
.LBB15_5:                               # %_ZN22CArchiveUpdateCallback21CryptoGetTextPasswordEPPw.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB15_1:
.Ltmp204:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB15_2
# BB#4:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB15_5
.LBB15_2:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp205:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp206:
# BB#3:
.LBB15_6:
.Ltmp207:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end15:
	.size	_ZThn16_N22CArchiveUpdateCallback21CryptoGetTextPasswordEPPw, .Lfunc_end15-_ZThn16_N22CArchiveUpdateCallback21CryptoGetTextPasswordEPPw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp202-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp203-.Ltmp202       #   Call between .Ltmp202 and .Ltmp203
	.long	.Ltmp204-.Lfunc_begin13 #     jumps to .Ltmp204
	.byte	3                       #   On action: 2
	.long	.Ltmp203-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp205-.Ltmp203       #   Call between .Ltmp203 and .Ltmp205
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp205-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp206-.Ltmp205       #   Call between .Ltmp205 and .Ltmp206
	.long	.Ltmp207-.Lfunc_begin13 #     jumps to .Ltmp207
	.byte	0                       #   On action: cleanup
	.long	.Ltmp206-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Lfunc_end15-.Ltmp206   #   Call between .Ltmp206 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv,@function
_ZN22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv: # @_ZN22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi92:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB16_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB16_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB16_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB16_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB16_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB16_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB16_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB16_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB16_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB16_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB16_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB16_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB16_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB16_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB16_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB16_16
.LBB16_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_IArchiveUpdateCallback2(%rip), %cl
	jne	.LBB16_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_IArchiveUpdateCallback2+1(%rip), %al
	jne	.LBB16_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_IArchiveUpdateCallback2+2(%rip), %al
	jne	.LBB16_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_IArchiveUpdateCallback2+3(%rip), %al
	jne	.LBB16_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_IArchiveUpdateCallback2+4(%rip), %al
	jne	.LBB16_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_IArchiveUpdateCallback2+5(%rip), %al
	jne	.LBB16_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_IArchiveUpdateCallback2+6(%rip), %al
	jne	.LBB16_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_IArchiveUpdateCallback2+7(%rip), %al
	jne	.LBB16_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_IArchiveUpdateCallback2+8(%rip), %al
	jne	.LBB16_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_IArchiveUpdateCallback2+9(%rip), %al
	jne	.LBB16_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_IArchiveUpdateCallback2+10(%rip), %al
	jne	.LBB16_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_IArchiveUpdateCallback2+11(%rip), %al
	jne	.LBB16_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_IArchiveUpdateCallback2+12(%rip), %al
	jne	.LBB16_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_IArchiveUpdateCallback2+13(%rip), %al
	jne	.LBB16_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_IArchiveUpdateCallback2+14(%rip), %al
	jne	.LBB16_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit12
	movb	15(%rsi), %al
	cmpb	IID_IArchiveUpdateCallback2+15(%rip), %al
	jne	.LBB16_33
.LBB16_16:
	movq	%rdi, (%rdx)
	jmp	.LBB16_85
.LBB16_33:                              # %_ZeqRK4GUIDS1_.exit12.thread
	cmpb	IID_ICryptoGetTextPassword2(%rip), %cl
	jne	.LBB16_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword2+1(%rip), %al
	jne	.LBB16_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword2+2(%rip), %al
	jne	.LBB16_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword2+3(%rip), %al
	jne	.LBB16_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword2+4(%rip), %al
	jne	.LBB16_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword2+5(%rip), %al
	jne	.LBB16_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword2+6(%rip), %al
	jne	.LBB16_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword2+7(%rip), %al
	jne	.LBB16_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword2+8(%rip), %al
	jne	.LBB16_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword2+9(%rip), %al
	jne	.LBB16_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword2+10(%rip), %al
	jne	.LBB16_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword2+11(%rip), %al
	jne	.LBB16_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword2+12(%rip), %al
	jne	.LBB16_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword2+13(%rip), %al
	jne	.LBB16_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword2+14(%rip), %al
	jne	.LBB16_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit16
	movb	15(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword2+15(%rip), %al
	jne	.LBB16_50
# BB#49:
	leaq	8(%rdi), %rax
	jmp	.LBB16_84
.LBB16_50:                              # %_ZeqRK4GUIDS1_.exit16.thread
	cmpb	IID_ICryptoGetTextPassword(%rip), %cl
	jne	.LBB16_67
# BB#51:
	movb	1(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+1(%rip), %al
	jne	.LBB16_67
# BB#52:
	movb	2(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+2(%rip), %al
	jne	.LBB16_67
# BB#53:
	movb	3(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+3(%rip), %al
	jne	.LBB16_67
# BB#54:
	movb	4(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+4(%rip), %al
	jne	.LBB16_67
# BB#55:
	movb	5(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+5(%rip), %al
	jne	.LBB16_67
# BB#56:
	movb	6(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+6(%rip), %al
	jne	.LBB16_67
# BB#57:
	movb	7(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+7(%rip), %al
	jne	.LBB16_67
# BB#58:
	movb	8(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+8(%rip), %al
	jne	.LBB16_67
# BB#59:
	movb	9(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+9(%rip), %al
	jne	.LBB16_67
# BB#60:
	movb	10(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+10(%rip), %al
	jne	.LBB16_67
# BB#61:
	movb	11(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+11(%rip), %al
	jne	.LBB16_67
# BB#62:
	movb	12(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+12(%rip), %al
	jne	.LBB16_67
# BB#63:
	movb	13(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+13(%rip), %al
	jne	.LBB16_67
# BB#64:
	movb	14(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+14(%rip), %al
	jne	.LBB16_67
# BB#65:                                # %_ZeqRK4GUIDS1_.exit18
	movb	15(%rsi), %al
	cmpb	IID_ICryptoGetTextPassword+15(%rip), %al
	jne	.LBB16_67
# BB#66:
	leaq	16(%rdi), %rax
	jmp	.LBB16_84
.LBB16_67:                              # %_ZeqRK4GUIDS1_.exit18.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressProgressInfo(%rip), %cl
	jne	.LBB16_86
# BB#68:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+1(%rip), %cl
	jne	.LBB16_86
# BB#69:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+2(%rip), %cl
	jne	.LBB16_86
# BB#70:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+3(%rip), %cl
	jne	.LBB16_86
# BB#71:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+4(%rip), %cl
	jne	.LBB16_86
# BB#72:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+5(%rip), %cl
	jne	.LBB16_86
# BB#73:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+6(%rip), %cl
	jne	.LBB16_86
# BB#74:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+7(%rip), %cl
	jne	.LBB16_86
# BB#75:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+8(%rip), %cl
	jne	.LBB16_86
# BB#76:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+9(%rip), %cl
	jne	.LBB16_86
# BB#77:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+10(%rip), %cl
	jne	.LBB16_86
# BB#78:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+11(%rip), %cl
	jne	.LBB16_86
# BB#79:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+12(%rip), %cl
	jne	.LBB16_86
# BB#80:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+13(%rip), %cl
	jne	.LBB16_86
# BB#81:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+14(%rip), %cl
	jne	.LBB16_86
# BB#82:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %cl
	cmpb	IID_ICompressProgressInfo+15(%rip), %cl
	jne	.LBB16_86
# BB#83:
	leaq	24(%rdi), %rax
.LBB16_84:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movq	%rax, (%rdx)
.LBB16_85:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB16_86:                              # %_ZeqRK4GUIDS1_.exit14.thread
	popq	%rcx
	retq
.Lfunc_end16:
	.size	_ZN22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv, .Lfunc_end16-_ZN22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN22CArchiveUpdateCallback6AddRefEv,"axG",@progbits,_ZN22CArchiveUpdateCallback6AddRefEv,comdat
	.weak	_ZN22CArchiveUpdateCallback6AddRefEv
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallback6AddRefEv,@function
_ZN22CArchiveUpdateCallback6AddRefEv:   # @_ZN22CArchiveUpdateCallback6AddRefEv
	.cfi_startproc
# BB#0:
	movl	32(%rdi), %eax
	incl	%eax
	movl	%eax, 32(%rdi)
	retq
.Lfunc_end17:
	.size	_ZN22CArchiveUpdateCallback6AddRefEv, .Lfunc_end17-_ZN22CArchiveUpdateCallback6AddRefEv
	.cfi_endproc

	.section	.text._ZN22CArchiveUpdateCallback7ReleaseEv,"axG",@progbits,_ZN22CArchiveUpdateCallback7ReleaseEv,comdat
	.weak	_ZN22CArchiveUpdateCallback7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallback7ReleaseEv,@function
_ZN22CArchiveUpdateCallback7ReleaseEv:  # @_ZN22CArchiveUpdateCallback7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 16
	movl	32(%rdi), %eax
	decl	%eax
	movl	%eax, 32(%rdi)
	jne	.LBB18_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB18_2:
	popq	%rcx
	retq
.Lfunc_end18:
	.size	_ZN22CArchiveUpdateCallback7ReleaseEv, .Lfunc_end18-_ZN22CArchiveUpdateCallback7ReleaseEv
	.cfi_endproc

	.section	.text._ZN22CArchiveUpdateCallbackD2Ev,"axG",@progbits,_ZN22CArchiveUpdateCallbackD2Ev,comdat
	.weak	_ZN22CArchiveUpdateCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallbackD2Ev,@function
_ZN22CArchiveUpdateCallbackD2Ev:        # @_ZN22CArchiveUpdateCallbackD2Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi96:
	.cfi_def_cfa_offset 32
.Lcfi97:
	.cfi_offset %rbx, -24
.Lcfi98:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTV22CArchiveUpdateCallback+160, %eax
	movd	%rax, %xmm0
	movl	$_ZTV22CArchiveUpdateCallback+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTV22CArchiveUpdateCallback+288, %eax
	movd	%rax, %xmm0
	movl	$_ZTV22CArchiveUpdateCallback+224, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB19_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp208:
	callq	*16(%rax)
.Ltmp209:
.LBB19_2:                               # %_ZN9CMyComPtrI10IInArchiveED2Ev.exit
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB19_4
# BB#3:
	callq	_ZdaPv
.LBB19_4:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB19_6
# BB#5:
	callq	_ZdaPv
.LBB19_6:                               # %_ZN11CStringBaseIwED2Ev.exit6
	addq	$40, %rbx
.Ltmp214:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp215:
# BB#7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB19_8:
.Ltmp210:
	movq	%rax, %r14
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB19_10
# BB#9:
	callq	_ZdaPv
.LBB19_10:                              # %_ZN11CStringBaseIwED2Ev.exit8
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB19_12
# BB#11:
	callq	_ZdaPv
.LBB19_12:                              # %_ZN11CStringBaseIwED2Ev.exit7
	addq	$40, %rbx
.Ltmp211:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp212:
	jmp	.LBB19_14
.LBB19_15:
.Ltmp213:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB19_13:
.Ltmp216:
	movq	%rax, %r14
.LBB19_14:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end19:
	.size	_ZN22CArchiveUpdateCallbackD2Ev, .Lfunc_end19-_ZN22CArchiveUpdateCallbackD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp208-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp209-.Ltmp208       #   Call between .Ltmp208 and .Ltmp209
	.long	.Ltmp210-.Lfunc_begin14 #     jumps to .Ltmp210
	.byte	0                       #   On action: cleanup
	.long	.Ltmp214-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp215-.Ltmp214       #   Call between .Ltmp214 and .Ltmp215
	.long	.Ltmp216-.Lfunc_begin14 #     jumps to .Ltmp216
	.byte	0                       #   On action: cleanup
	.long	.Ltmp211-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp212-.Ltmp211       #   Call between .Ltmp211 and .Ltmp212
	.long	.Ltmp213-.Lfunc_begin14 #     jumps to .Ltmp213
	.byte	1                       #   On action: 1
	.long	.Ltmp212-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end19-.Ltmp212   #   Call between .Ltmp212 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN22CArchiveUpdateCallbackD0Ev,"axG",@progbits,_ZN22CArchiveUpdateCallbackD0Ev,comdat
	.weak	_ZN22CArchiveUpdateCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN22CArchiveUpdateCallbackD0Ev,@function
_ZN22CArchiveUpdateCallbackD0Ev:        # @_ZN22CArchiveUpdateCallbackD0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 32
.Lcfi102:
	.cfi_offset %rbx, -24
.Lcfi103:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp217:
	callq	_ZN22CArchiveUpdateCallbackD2Ev
.Ltmp218:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB20_2:
.Ltmp219:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end20:
	.size	_ZN22CArchiveUpdateCallbackD0Ev, .Lfunc_end20-_ZN22CArchiveUpdateCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp217-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp218-.Ltmp217       #   Call between .Ltmp217 and .Ltmp218
	.long	.Ltmp219-.Lfunc_begin15 #     jumps to .Ltmp219
	.byte	0                       #   On action: cleanup
	.long	.Ltmp218-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Lfunc_end20-.Ltmp218   #   Call between .Ltmp218 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end21:
	.size	_ZThn8_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv, .Lfunc_end21-_ZThn8_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N22CArchiveUpdateCallback6AddRefEv,"axG",@progbits,_ZThn8_N22CArchiveUpdateCallback6AddRefEv,comdat
	.weak	_ZThn8_N22CArchiveUpdateCallback6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N22CArchiveUpdateCallback6AddRefEv,@function
_ZThn8_N22CArchiveUpdateCallback6AddRefEv: # @_ZThn8_N22CArchiveUpdateCallback6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end22:
	.size	_ZThn8_N22CArchiveUpdateCallback6AddRefEv, .Lfunc_end22-_ZThn8_N22CArchiveUpdateCallback6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N22CArchiveUpdateCallback7ReleaseEv,"axG",@progbits,_ZThn8_N22CArchiveUpdateCallback7ReleaseEv,comdat
	.weak	_ZThn8_N22CArchiveUpdateCallback7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N22CArchiveUpdateCallback7ReleaseEv,@function
_ZThn8_N22CArchiveUpdateCallback7ReleaseEv: # @_ZThn8_N22CArchiveUpdateCallback7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi104:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB23_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB23_2:                               # %_ZN22CArchiveUpdateCallback7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end23:
	.size	_ZThn8_N22CArchiveUpdateCallback7ReleaseEv, .Lfunc_end23-_ZThn8_N22CArchiveUpdateCallback7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N22CArchiveUpdateCallbackD1Ev,"axG",@progbits,_ZThn8_N22CArchiveUpdateCallbackD1Ev,comdat
	.weak	_ZThn8_N22CArchiveUpdateCallbackD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N22CArchiveUpdateCallbackD1Ev,@function
_ZThn8_N22CArchiveUpdateCallbackD1Ev:   # @_ZThn8_N22CArchiveUpdateCallbackD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN22CArchiveUpdateCallbackD2Ev # TAILCALL
.Lfunc_end24:
	.size	_ZThn8_N22CArchiveUpdateCallbackD1Ev, .Lfunc_end24-_ZThn8_N22CArchiveUpdateCallbackD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N22CArchiveUpdateCallbackD0Ev,"axG",@progbits,_ZThn8_N22CArchiveUpdateCallbackD0Ev,comdat
	.weak	_ZThn8_N22CArchiveUpdateCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N22CArchiveUpdateCallbackD0Ev,@function
_ZThn8_N22CArchiveUpdateCallbackD0Ev:   # @_ZThn8_N22CArchiveUpdateCallbackD0Ev
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi107:
	.cfi_def_cfa_offset 32
.Lcfi108:
	.cfi_offset %rbx, -24
.Lcfi109:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp220:
	movq	%rbx, %rdi
	callq	_ZN22CArchiveUpdateCallbackD2Ev
.Ltmp221:
# BB#1:                                 # %_ZN22CArchiveUpdateCallbackD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB25_2:
.Ltmp222:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end25:
	.size	_ZThn8_N22CArchiveUpdateCallbackD0Ev, .Lfunc_end25-_ZThn8_N22CArchiveUpdateCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp220-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp221-.Ltmp220       #   Call between .Ltmp220 and .Ltmp221
	.long	.Ltmp222-.Lfunc_begin16 #     jumps to .Ltmp222
	.byte	0                       #   On action: cleanup
	.long	.Ltmp221-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Lfunc_end25-.Ltmp221   #   Call between .Ltmp221 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn16_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end26:
	.size	_ZThn16_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv, .Lfunc_end26-_ZThn16_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N22CArchiveUpdateCallback6AddRefEv,"axG",@progbits,_ZThn16_N22CArchiveUpdateCallback6AddRefEv,comdat
	.weak	_ZThn16_N22CArchiveUpdateCallback6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N22CArchiveUpdateCallback6AddRefEv,@function
_ZThn16_N22CArchiveUpdateCallback6AddRefEv: # @_ZThn16_N22CArchiveUpdateCallback6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end27:
	.size	_ZThn16_N22CArchiveUpdateCallback6AddRefEv, .Lfunc_end27-_ZThn16_N22CArchiveUpdateCallback6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N22CArchiveUpdateCallback7ReleaseEv,"axG",@progbits,_ZThn16_N22CArchiveUpdateCallback7ReleaseEv,comdat
	.weak	_ZThn16_N22CArchiveUpdateCallback7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N22CArchiveUpdateCallback7ReleaseEv,@function
_ZThn16_N22CArchiveUpdateCallback7ReleaseEv: # @_ZThn16_N22CArchiveUpdateCallback7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi110:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB28_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB28_2:                               # %_ZN22CArchiveUpdateCallback7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end28:
	.size	_ZThn16_N22CArchiveUpdateCallback7ReleaseEv, .Lfunc_end28-_ZThn16_N22CArchiveUpdateCallback7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N22CArchiveUpdateCallbackD1Ev,"axG",@progbits,_ZThn16_N22CArchiveUpdateCallbackD1Ev,comdat
	.weak	_ZThn16_N22CArchiveUpdateCallbackD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N22CArchiveUpdateCallbackD1Ev,@function
_ZThn16_N22CArchiveUpdateCallbackD1Ev:  # @_ZThn16_N22CArchiveUpdateCallbackD1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN22CArchiveUpdateCallbackD2Ev # TAILCALL
.Lfunc_end29:
	.size	_ZThn16_N22CArchiveUpdateCallbackD1Ev, .Lfunc_end29-_ZThn16_N22CArchiveUpdateCallbackD1Ev
	.cfi_endproc

	.section	.text._ZThn16_N22CArchiveUpdateCallbackD0Ev,"axG",@progbits,_ZThn16_N22CArchiveUpdateCallbackD0Ev,comdat
	.weak	_ZThn16_N22CArchiveUpdateCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N22CArchiveUpdateCallbackD0Ev,@function
_ZThn16_N22CArchiveUpdateCallbackD0Ev:  # @_ZThn16_N22CArchiveUpdateCallbackD0Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi113:
	.cfi_def_cfa_offset 32
.Lcfi114:
	.cfi_offset %rbx, -24
.Lcfi115:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp223:
	movq	%rbx, %rdi
	callq	_ZN22CArchiveUpdateCallbackD2Ev
.Ltmp224:
# BB#1:                                 # %_ZN22CArchiveUpdateCallbackD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB30_2:
.Ltmp225:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end30:
	.size	_ZThn16_N22CArchiveUpdateCallbackD0Ev, .Lfunc_end30-_ZThn16_N22CArchiveUpdateCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp223-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp224-.Ltmp223       #   Call between .Ltmp223 and .Ltmp224
	.long	.Ltmp225-.Lfunc_begin17 #     jumps to .Ltmp225
	.byte	0                       #   On action: cleanup
	.long	.Ltmp224-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Lfunc_end30-.Ltmp224   #   Call between .Ltmp224 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn24_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn24_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn24_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn24_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv,@function
_ZThn24_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv: # @_ZThn24_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end31:
	.size	_ZThn24_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv, .Lfunc_end31-_ZThn24_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn24_N22CArchiveUpdateCallback6AddRefEv,"axG",@progbits,_ZThn24_N22CArchiveUpdateCallback6AddRefEv,comdat
	.weak	_ZThn24_N22CArchiveUpdateCallback6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn24_N22CArchiveUpdateCallback6AddRefEv,@function
_ZThn24_N22CArchiveUpdateCallback6AddRefEv: # @_ZThn24_N22CArchiveUpdateCallback6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end32:
	.size	_ZThn24_N22CArchiveUpdateCallback6AddRefEv, .Lfunc_end32-_ZThn24_N22CArchiveUpdateCallback6AddRefEv
	.cfi_endproc

	.section	.text._ZThn24_N22CArchiveUpdateCallback7ReleaseEv,"axG",@progbits,_ZThn24_N22CArchiveUpdateCallback7ReleaseEv,comdat
	.weak	_ZThn24_N22CArchiveUpdateCallback7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn24_N22CArchiveUpdateCallback7ReleaseEv,@function
_ZThn24_N22CArchiveUpdateCallback7ReleaseEv: # @_ZThn24_N22CArchiveUpdateCallback7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi116:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB33_2
# BB#1:
	addq	$-24, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB33_2:                               # %_ZN22CArchiveUpdateCallback7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end33:
	.size	_ZThn24_N22CArchiveUpdateCallback7ReleaseEv, .Lfunc_end33-_ZThn24_N22CArchiveUpdateCallback7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn24_N22CArchiveUpdateCallbackD1Ev,"axG",@progbits,_ZThn24_N22CArchiveUpdateCallbackD1Ev,comdat
	.weak	_ZThn24_N22CArchiveUpdateCallbackD1Ev
	.p2align	4, 0x90
	.type	_ZThn24_N22CArchiveUpdateCallbackD1Ev,@function
_ZThn24_N22CArchiveUpdateCallbackD1Ev:  # @_ZThn24_N22CArchiveUpdateCallbackD1Ev
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN22CArchiveUpdateCallbackD2Ev # TAILCALL
.Lfunc_end34:
	.size	_ZThn24_N22CArchiveUpdateCallbackD1Ev, .Lfunc_end34-_ZThn24_N22CArchiveUpdateCallbackD1Ev
	.cfi_endproc

	.section	.text._ZThn24_N22CArchiveUpdateCallbackD0Ev,"axG",@progbits,_ZThn24_N22CArchiveUpdateCallbackD0Ev,comdat
	.weak	_ZThn24_N22CArchiveUpdateCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZThn24_N22CArchiveUpdateCallbackD0Ev,@function
_ZThn24_N22CArchiveUpdateCallbackD0Ev:  # @_ZThn24_N22CArchiveUpdateCallbackD0Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi119:
	.cfi_def_cfa_offset 32
.Lcfi120:
	.cfi_offset %rbx, -24
.Lcfi121:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-24, %rbx
.Ltmp226:
	movq	%rbx, %rdi
	callq	_ZN22CArchiveUpdateCallbackD2Ev
.Ltmp227:
# BB#1:                                 # %_ZN22CArchiveUpdateCallbackD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB35_2:
.Ltmp228:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end35:
	.size	_ZThn24_N22CArchiveUpdateCallbackD0Ev, .Lfunc_end35-_ZThn24_N22CArchiveUpdateCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp226-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp227-.Ltmp226       #   Call between .Ltmp226 and .Ltmp227
	.long	.Ltmp228-.Lfunc_begin18 #     jumps to .Ltmp228
	.byte	0                       #   On action: cleanup
	.long	.Ltmp227-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Lfunc_end35-.Ltmp227   #   Call between .Ltmp227 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NWindows5NFile3NIO7CInFileD0Ev,"axG",@progbits,_ZN8NWindows5NFile3NIO7CInFileD0Ev,comdat
	.weak	_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO7CInFileD0Ev,@function
_ZN8NWindows5NFile3NIO7CInFileD0Ev:     # @_ZN8NWindows5NFile3NIO7CInFileD0Ev
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi124:
	.cfi_def_cfa_offset 32
.Lcfi125:
	.cfi_offset %rbx, -24
.Lcfi126:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp229:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp230:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB36_2:
.Ltmp231:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end36:
	.size	_ZN8NWindows5NFile3NIO7CInFileD0Ev, .Lfunc_end36-_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table36:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp229-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp230-.Ltmp229       #   Call between .Ltmp229 and .Ltmp230
	.long	.Ltmp231-.Lfunc_begin19 #     jumps to .Ltmp231
	.byte	0                       #   On action: cleanup
	.long	.Ltmp230-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Lfunc_end36-.Ltmp230   #   Call between .Ltmp230 and .Lfunc_end36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NWindows5NFile3NIO8COutFileD0Ev,"axG",@progbits,_ZN8NWindows5NFile3NIO8COutFileD0Ev,comdat
	.weak	_ZN8NWindows5NFile3NIO8COutFileD0Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO8COutFileD0Ev,@function
_ZN8NWindows5NFile3NIO8COutFileD0Ev:    # @_ZN8NWindows5NFile3NIO8COutFileD0Ev
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi129:
	.cfi_def_cfa_offset 32
.Lcfi130:
	.cfi_offset %rbx, -24
.Lcfi131:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp232:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp233:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB37_2:
.Ltmp234:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end37:
	.size	_ZN8NWindows5NFile3NIO8COutFileD0Ev, .Lfunc_end37-_ZN8NWindows5NFile3NIO8COutFileD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table37:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp232-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp233-.Ltmp232       #   Call between .Ltmp232 and .Ltmp233
	.long	.Ltmp234-.Lfunc_begin20 #     jumps to .Ltmp234
	.byte	0                       #   On action: cleanup
	.long	.Ltmp233-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Lfunc_end37-.Ltmp233   #   Call between .Ltmp233 and .Lfunc_end37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi134:
	.cfi_def_cfa_offset 32
.Lcfi135:
	.cfi_offset %rbx, -24
.Lcfi136:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp235:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp236:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB38_2:
.Ltmp237:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end38:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end38-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table38:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp235-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp236-.Ltmp235       #   Call between .Ltmp235 and .Ltmp236
	.long	.Ltmp237-.Lfunc_begin21 #     jumps to .Ltmp237
	.byte	0                       #   On action: cleanup
	.long	.Ltmp236-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Lfunc_end38-.Ltmp236   #   Call between .Ltmp236 and .Lfunc_end38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV22CArchiveUpdateCallback,@object # @_ZTV22CArchiveUpdateCallback
	.section	.rodata,"a",@progbits
	.globl	_ZTV22CArchiveUpdateCallback
	.p2align	3
_ZTV22CArchiveUpdateCallback:
	.quad	0
	.quad	_ZTI22CArchiveUpdateCallback
	.quad	_ZN22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.quad	_ZN22CArchiveUpdateCallback6AddRefEv
	.quad	_ZN22CArchiveUpdateCallback7ReleaseEv
	.quad	_ZN22CArchiveUpdateCallbackD2Ev
	.quad	_ZN22CArchiveUpdateCallbackD0Ev
	.quad	_ZN22CArchiveUpdateCallback8SetTotalEy
	.quad	_ZN22CArchiveUpdateCallback12SetCompletedEPKy
	.quad	_ZN22CArchiveUpdateCallback17GetUpdateItemInfoEjPiS0_Pj
	.quad	_ZN22CArchiveUpdateCallback11GetPropertyEjjP14tagPROPVARIANT
	.quad	_ZN22CArchiveUpdateCallback9GetStreamEjPP19ISequentialInStream
	.quad	_ZN22CArchiveUpdateCallback18SetOperationResultEi
	.quad	_ZN22CArchiveUpdateCallback13GetVolumeSizeEjPy
	.quad	_ZN22CArchiveUpdateCallback15GetVolumeStreamEjPP20ISequentialOutStream
	.quad	_ZN22CArchiveUpdateCallback12SetRatioInfoEPKyS1_
	.quad	_ZN22CArchiveUpdateCallback22CryptoGetTextPassword2EPiPPw
	.quad	_ZN22CArchiveUpdateCallback21CryptoGetTextPasswordEPPw
	.quad	-8
	.quad	_ZTI22CArchiveUpdateCallback
	.quad	_ZThn8_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N22CArchiveUpdateCallback6AddRefEv
	.quad	_ZThn8_N22CArchiveUpdateCallback7ReleaseEv
	.quad	_ZThn8_N22CArchiveUpdateCallbackD1Ev
	.quad	_ZThn8_N22CArchiveUpdateCallbackD0Ev
	.quad	_ZThn8_N22CArchiveUpdateCallback22CryptoGetTextPassword2EPiPPw
	.quad	-16
	.quad	_ZTI22CArchiveUpdateCallback
	.quad	_ZThn16_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N22CArchiveUpdateCallback6AddRefEv
	.quad	_ZThn16_N22CArchiveUpdateCallback7ReleaseEv
	.quad	_ZThn16_N22CArchiveUpdateCallbackD1Ev
	.quad	_ZThn16_N22CArchiveUpdateCallbackD0Ev
	.quad	_ZThn16_N22CArchiveUpdateCallback21CryptoGetTextPasswordEPPw
	.quad	-24
	.quad	_ZTI22CArchiveUpdateCallback
	.quad	_ZThn24_N22CArchiveUpdateCallback14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn24_N22CArchiveUpdateCallback6AddRefEv
	.quad	_ZThn24_N22CArchiveUpdateCallback7ReleaseEv
	.quad	_ZThn24_N22CArchiveUpdateCallbackD1Ev
	.quad	_ZThn24_N22CArchiveUpdateCallbackD0Ev
	.quad	_ZThn24_N22CArchiveUpdateCallback12SetRatioInfoEPKyS1_
	.size	_ZTV22CArchiveUpdateCallback, 336

	.type	_ZTS22CArchiveUpdateCallback,@object # @_ZTS22CArchiveUpdateCallback
	.globl	_ZTS22CArchiveUpdateCallback
	.p2align	4
_ZTS22CArchiveUpdateCallback:
	.asciz	"22CArchiveUpdateCallback"
	.size	_ZTS22CArchiveUpdateCallback, 25

	.type	_ZTS23IArchiveUpdateCallback2,@object # @_ZTS23IArchiveUpdateCallback2
	.section	.rodata._ZTS23IArchiveUpdateCallback2,"aG",@progbits,_ZTS23IArchiveUpdateCallback2,comdat
	.weak	_ZTS23IArchiveUpdateCallback2
	.p2align	4
_ZTS23IArchiveUpdateCallback2:
	.asciz	"23IArchiveUpdateCallback2"
	.size	_ZTS23IArchiveUpdateCallback2, 26

	.type	_ZTS22IArchiveUpdateCallback,@object # @_ZTS22IArchiveUpdateCallback
	.section	.rodata._ZTS22IArchiveUpdateCallback,"aG",@progbits,_ZTS22IArchiveUpdateCallback,comdat
	.weak	_ZTS22IArchiveUpdateCallback
	.p2align	4
_ZTS22IArchiveUpdateCallback:
	.asciz	"22IArchiveUpdateCallback"
	.size	_ZTS22IArchiveUpdateCallback, 25

	.type	_ZTS9IProgress,@object  # @_ZTS9IProgress
	.section	.rodata._ZTS9IProgress,"aG",@progbits,_ZTS9IProgress,comdat
	.weak	_ZTS9IProgress
_ZTS9IProgress:
	.asciz	"9IProgress"
	.size	_ZTS9IProgress, 11

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI9IProgress,@object  # @_ZTI9IProgress
	.section	.rodata._ZTI9IProgress,"aG",@progbits,_ZTI9IProgress,comdat
	.weak	_ZTI9IProgress
	.p2align	4
_ZTI9IProgress:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS9IProgress
	.quad	_ZTI8IUnknown
	.size	_ZTI9IProgress, 24

	.type	_ZTI22IArchiveUpdateCallback,@object # @_ZTI22IArchiveUpdateCallback
	.section	.rodata._ZTI22IArchiveUpdateCallback,"aG",@progbits,_ZTI22IArchiveUpdateCallback,comdat
	.weak	_ZTI22IArchiveUpdateCallback
	.p2align	4
_ZTI22IArchiveUpdateCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22IArchiveUpdateCallback
	.quad	_ZTI9IProgress
	.size	_ZTI22IArchiveUpdateCallback, 24

	.type	_ZTI23IArchiveUpdateCallback2,@object # @_ZTI23IArchiveUpdateCallback2
	.section	.rodata._ZTI23IArchiveUpdateCallback2,"aG",@progbits,_ZTI23IArchiveUpdateCallback2,comdat
	.weak	_ZTI23IArchiveUpdateCallback2
	.p2align	4
_ZTI23IArchiveUpdateCallback2:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS23IArchiveUpdateCallback2
	.quad	_ZTI22IArchiveUpdateCallback
	.size	_ZTI23IArchiveUpdateCallback2, 24

	.type	_ZTS23ICryptoGetTextPassword2,@object # @_ZTS23ICryptoGetTextPassword2
	.section	.rodata._ZTS23ICryptoGetTextPassword2,"aG",@progbits,_ZTS23ICryptoGetTextPassword2,comdat
	.weak	_ZTS23ICryptoGetTextPassword2
	.p2align	4
_ZTS23ICryptoGetTextPassword2:
	.asciz	"23ICryptoGetTextPassword2"
	.size	_ZTS23ICryptoGetTextPassword2, 26

	.type	_ZTI23ICryptoGetTextPassword2,@object # @_ZTI23ICryptoGetTextPassword2
	.section	.rodata._ZTI23ICryptoGetTextPassword2,"aG",@progbits,_ZTI23ICryptoGetTextPassword2,comdat
	.weak	_ZTI23ICryptoGetTextPassword2
	.p2align	4
_ZTI23ICryptoGetTextPassword2:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS23ICryptoGetTextPassword2
	.quad	_ZTI8IUnknown
	.size	_ZTI23ICryptoGetTextPassword2, 24

	.type	_ZTS22ICryptoGetTextPassword,@object # @_ZTS22ICryptoGetTextPassword
	.section	.rodata._ZTS22ICryptoGetTextPassword,"aG",@progbits,_ZTS22ICryptoGetTextPassword,comdat
	.weak	_ZTS22ICryptoGetTextPassword
	.p2align	4
_ZTS22ICryptoGetTextPassword:
	.asciz	"22ICryptoGetTextPassword"
	.size	_ZTS22ICryptoGetTextPassword, 25

	.type	_ZTI22ICryptoGetTextPassword,@object # @_ZTI22ICryptoGetTextPassword
	.section	.rodata._ZTI22ICryptoGetTextPassword,"aG",@progbits,_ZTI22ICryptoGetTextPassword,comdat
	.weak	_ZTI22ICryptoGetTextPassword
	.p2align	4
_ZTI22ICryptoGetTextPassword:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22ICryptoGetTextPassword
	.quad	_ZTI8IUnknown
	.size	_ZTI22ICryptoGetTextPassword, 24

	.type	_ZTS21ICompressProgressInfo,@object # @_ZTS21ICompressProgressInfo
	.section	.rodata._ZTS21ICompressProgressInfo,"aG",@progbits,_ZTS21ICompressProgressInfo,comdat
	.weak	_ZTS21ICompressProgressInfo
	.p2align	4
_ZTS21ICompressProgressInfo:
	.asciz	"21ICompressProgressInfo"
	.size	_ZTS21ICompressProgressInfo, 24

	.type	_ZTI21ICompressProgressInfo,@object # @_ZTI21ICompressProgressInfo
	.section	.rodata._ZTI21ICompressProgressInfo,"aG",@progbits,_ZTI21ICompressProgressInfo,comdat
	.weak	_ZTI21ICompressProgressInfo
	.p2align	4
_ZTI21ICompressProgressInfo:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21ICompressProgressInfo
	.quad	_ZTI8IUnknown
	.size	_ZTI21ICompressProgressInfo, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI22CArchiveUpdateCallback,@object # @_ZTI22CArchiveUpdateCallback
	.section	.rodata,"a",@progbits
	.globl	_ZTI22CArchiveUpdateCallback
	.p2align	4
_ZTI22CArchiveUpdateCallback:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS22CArchiveUpdateCallback
	.long	1                       # 0x1
	.long	5                       # 0x5
	.quad	_ZTI23IArchiveUpdateCallback2
	.quad	2                       # 0x2
	.quad	_ZTI23ICryptoGetTextPassword2
	.quad	2050                    # 0x802
	.quad	_ZTI22ICryptoGetTextPassword
	.quad	4098                    # 0x1002
	.quad	_ZTI21ICompressProgressInfo
	.quad	6146                    # 0x1802
	.quad	_ZTI13CMyUnknownImp
	.quad	8194                    # 0x2002
	.size	_ZTI22CArchiveUpdateCallback, 104

	.type	_ZTVN8NWindows5NFile3NIO7CInFileE,@object # @_ZTVN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTVN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTVN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTVN8NWindows5NFile3NIO7CInFileE
	.p2align	3
_ZTVN8NWindows5NFile3NIO7CInFileE:
	.quad	0
	.quad	_ZTIN8NWindows5NFile3NIO7CInFileE
	.quad	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.quad	_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.quad	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.size	_ZTVN8NWindows5NFile3NIO7CInFileE, 40

	.type	_ZTSN8NWindows5NFile3NIO7CInFileE,@object # @_ZTSN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTSN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTSN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTSN8NWindows5NFile3NIO7CInFileE
	.p2align	4
_ZTSN8NWindows5NFile3NIO7CInFileE:
	.asciz	"N8NWindows5NFile3NIO7CInFileE"
	.size	_ZTSN8NWindows5NFile3NIO7CInFileE, 30

	.type	_ZTIN8NWindows5NFile3NIO7CInFileE,@object # @_ZTIN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTIN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTIN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTIN8NWindows5NFile3NIO7CInFileE
	.p2align	4
_ZTIN8NWindows5NFile3NIO7CInFileE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows5NFile3NIO7CInFileE
	.quad	_ZTIN8NWindows5NFile3NIO9CFileBaseE
	.size	_ZTIN8NWindows5NFile3NIO7CInFileE, 24

	.type	_ZTVN8NWindows5NFile3NIO8COutFileE,@object # @_ZTVN8NWindows5NFile3NIO8COutFileE
	.section	.rodata._ZTVN8NWindows5NFile3NIO8COutFileE,"aG",@progbits,_ZTVN8NWindows5NFile3NIO8COutFileE,comdat
	.weak	_ZTVN8NWindows5NFile3NIO8COutFileE
	.p2align	3
_ZTVN8NWindows5NFile3NIO8COutFileE:
	.quad	0
	.quad	_ZTIN8NWindows5NFile3NIO8COutFileE
	.quad	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.quad	_ZN8NWindows5NFile3NIO8COutFileD0Ev
	.quad	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.size	_ZTVN8NWindows5NFile3NIO8COutFileE, 40

	.type	_ZTSN8NWindows5NFile3NIO8COutFileE,@object # @_ZTSN8NWindows5NFile3NIO8COutFileE
	.section	.rodata._ZTSN8NWindows5NFile3NIO8COutFileE,"aG",@progbits,_ZTSN8NWindows5NFile3NIO8COutFileE,comdat
	.weak	_ZTSN8NWindows5NFile3NIO8COutFileE
	.p2align	4
_ZTSN8NWindows5NFile3NIO8COutFileE:
	.asciz	"N8NWindows5NFile3NIO8COutFileE"
	.size	_ZTSN8NWindows5NFile3NIO8COutFileE, 31

	.type	_ZTIN8NWindows5NFile3NIO8COutFileE,@object # @_ZTIN8NWindows5NFile3NIO8COutFileE
	.section	.rodata._ZTIN8NWindows5NFile3NIO8COutFileE,"aG",@progbits,_ZTIN8NWindows5NFile3NIO8COutFileE,comdat
	.weak	_ZTIN8NWindows5NFile3NIO8COutFileE
	.p2align	4
_ZTIN8NWindows5NFile3NIO8COutFileE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows5NFile3NIO8COutFileE
	.quad	_ZTIN8NWindows5NFile3NIO9CFileBaseE
	.size	_ZTIN8NWindows5NFile3NIO8COutFileE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24


	.globl	_ZN22CArchiveUpdateCallbackC1Ev
	.type	_ZN22CArchiveUpdateCallbackC1Ev,@function
_ZN22CArchiveUpdateCallbackC1Ev = _ZN22CArchiveUpdateCallbackC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
