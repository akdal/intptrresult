	.text
	.file	"blas.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	isamax
	.p2align	4, 0x90
	.type	isamax,@function
isamax:                                 # @isamax
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$2, %edi
	jge	.LBB0_1
.LBB0_16:                               # %.loopexit
	retq
.LBB0_1:
	cmpl	$1, %edx
	jne	.LBB0_2
# BB#10:                                # %.lr.ph.preheader
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movaps	.LCPI0_0(%rip), %xmm3   # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm3
	xorps	%xmm1, %xmm1
	xorps	%xmm0, %xmm0
	cmpltss	%xmm2, %xmm0
	andps	%xmm0, %xmm2
	andnps	%xmm3, %xmm0
	orps	%xmm2, %xmm0
	testb	$1, %dil
	jne	.LBB0_11
# BB#12:                                # %.lr.ph.prol
	movss	4(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	addq	$4, %rsi
	movaps	.LCPI0_0(%rip), %xmm3   # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm3
	cmpltss	%xmm2, %xmm1
	andps	%xmm1, %xmm2
	andnps	%xmm3, %xmm1
	orps	%xmm2, %xmm1
	xorl	%eax, %eax
	ucomiss	%xmm0, %xmm1
	seta	%al
	maxss	%xmm0, %xmm1
	movl	$2, %ecx
	movaps	%xmm1, %xmm0
	cmpl	$2, %edi
	je	.LBB0_16
	jmp	.LBB0_14
.LBB0_2:
	testl	%edx, %edx
	jns	.LBB0_4
# BB#3:
	movl	$1, %eax
	subl	%edi, %eax
	imull	%edx, %eax
	cltq
	leaq	4(%rsi,%rax,4), %rsi
.LBB0_4:                                # %.lr.ph63.preheader
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movslq	%edx, %r9
	movaps	.LCPI0_0(%rip), %xmm3   # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm3
	xorps	%xmm1, %xmm1
	xorps	%xmm0, %xmm0
	cmpltss	%xmm2, %xmm0
	andps	%xmm0, %xmm2
	andnps	%xmm3, %xmm0
	orps	%xmm2, %xmm0
	testb	$1, %dil
	jne	.LBB0_5
# BB#6:                                 # %.lr.ph63.prol
	movss	(%rsi,%r9,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	leaq	(%rsi,%r9,4), %rsi
	movaps	.LCPI0_0(%rip), %xmm3   # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm3
	cmpltss	%xmm2, %xmm1
	andps	%xmm1, %xmm2
	andnps	%xmm3, %xmm1
	orps	%xmm2, %xmm1
	xorl	%eax, %eax
	ucomiss	%xmm0, %xmm1
	seta	%al
	maxss	%xmm0, %xmm1
	movl	$2, %edx
	movaps	%xmm1, %xmm0
	cmpl	$2, %edi
	je	.LBB0_16
	jmp	.LBB0_8
.LBB0_11:
	movl	$1, %ecx
	xorl	%eax, %eax
	cmpl	$2, %edi
	je	.LBB0_16
.LBB0_14:                               # %.lr.ph.preheader.new
	addq	$8, %rsi
	movaps	.LCPI0_0(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm4, %xmm4
	cmpltss	%xmm2, %xmm4
	movaps	%xmm4, %xmm5
	andps	%xmm2, %xmm4
	xorps	%xmm1, %xmm2
	andnps	%xmm2, %xmm5
	orps	%xmm5, %xmm4
	ucomiss	%xmm0, %xmm4
	maxss	%xmm0, %xmm4
	cmoval	%ecx, %eax
	xorps	%xmm0, %xmm0
	cmpltss	%xmm3, %xmm0
	movaps	%xmm0, %xmm2
	andps	%xmm3, %xmm0
	xorps	%xmm1, %xmm3
	andnps	%xmm3, %xmm2
	orps	%xmm2, %xmm0
	leal	1(%rcx), %edx
	ucomiss	%xmm4, %xmm0
	maxss	%xmm4, %xmm0
	cmoval	%edx, %eax
	addq	$8, %rsi
	addl	$2, %ecx
	cmpl	%edi, %ecx
	jne	.LBB0_15
	jmp	.LBB0_16
.LBB0_5:
	movl	$1, %edx
	xorl	%eax, %eax
	cmpl	$2, %edi
	je	.LBB0_16
.LBB0_8:                                # %.lr.ph63.preheader.new
	leaq	(,%r9,8), %r8
	movaps	.LCPI0_0(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph63
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi,%r9,4), %xmm3     # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	xorps	%xmm1, %xmm4
	xorps	%xmm2, %xmm2
	cmpltss	%xmm3, %xmm2
	andps	%xmm2, %xmm3
	andnps	%xmm4, %xmm2
	orps	%xmm3, %xmm2
	ucomiss	%xmm0, %xmm2
	maxss	%xmm0, %xmm2
	cmoval	%edx, %eax
	movss	(%rsi,%r9,8), %xmm3     # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	xorps	%xmm1, %xmm4
	xorps	%xmm0, %xmm0
	cmpltss	%xmm3, %xmm0
	andps	%xmm0, %xmm3
	andnps	%xmm4, %xmm0
	orps	%xmm3, %xmm0
	leal	1(%rdx), %ecx
	ucomiss	%xmm2, %xmm0
	maxss	%xmm2, %xmm0
	cmoval	%ecx, %eax
	addq	%r8, %rsi
	addl	$2, %edx
	cmpl	%edi, %edx
	jne	.LBB0_9
	jmp	.LBB0_16
.Lfunc_end0:
	.size	isamax, .Lfunc_end0-isamax
	.cfi_endproc

	.globl	saxpy
	.p2align	4, 0x90
	.type	saxpy,@function
saxpy:                                  # @saxpy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB1_45
# BB#1:
	cvtsd2ss	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB1_2
	jnp	.LBB1_45
.LBB1_2:
	cmpl	%r8d, %edx
	jne	.LBB1_33
# BB#3:
	cmpl	$1, %edx
	jne	.LBB1_23
# BB#4:                                 # %.preheader
	testl	%edi, %edi
	jle	.LBB1_45
# BB#5:                                 # %.lr.ph.preheader
	leal	-1(%rdi), %r10d
	leaq	1(%r10), %r9
	xorl	%edx, %edx
	cmpq	$8, %r9
	jb	.LBB1_17
# BB#6:                                 # %min.iters.checked
	movabsq	$8589934584, %r8        # imm = 0x1FFFFFFF8
	andq	%r9, %r8
	je	.LBB1_17
# BB#7:                                 # %vector.memcheck
	leaq	4(%rsi,%r10,4), %rax
	cmpq	%rcx, %rax
	jbe	.LBB1_9
# BB#8:                                 # %vector.memcheck
	leaq	4(%rcx,%r10,4), %rax
	cmpq	%rsi, %rax
	ja	.LBB1_17
.LBB1_9:                                # %vector.ph
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	leaq	-8(%r8), %rdx
	movq	%rdx, %rax
	shrq	$3, %rax
	btl	$3, %edx
	jb	.LBB1_10
# BB#11:                                # %vector.body.prol
	movups	(%rsi), %xmm2
	movups	16(%rsi), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	(%rcx), %xmm4
	movups	16(%rcx), %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, (%rcx)
	movups	%xmm5, 16(%rcx)
	movl	$8, %edx
	testq	%rax, %rax
	jne	.LBB1_13
	jmp	.LBB1_15
.LBB1_23:
	testl	%edx, %edx
	jle	.LBB1_33
# BB#24:                                # %.preheader49
	testl	%edi, %edi
	jle	.LBB1_45
# BB#25:                                # %.lr.ph59
	movslq	%edx, %r11
	leal	-1(%rdi), %r8d
	movl	%edi, %r9d
	andl	$3, %r9d
	je	.LBB1_26
# BB#27:                                # %.prol.preheader
	leaq	(,%r11,4), %r10
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_28:                               # =>This Inner Loop Header: Depth=1
	movss	(%rsi,%rbx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx,%rbx), %xmm1
	movss	%xmm1, (%rcx,%rbx)
	incl	%edx
	addq	%r10, %rbx
	cmpl	%edx, %r9d
	jne	.LBB1_28
# BB#29:                                # %.prol.loopexit.unr-lcssa
	addq	%rbx, %rsi
	addq	%rbx, %rcx
	cmpl	$3, %r8d
	jae	.LBB1_31
	jmp	.LBB1_45
.LBB1_33:
	testl	%edx, %edx
	jns	.LBB1_35
# BB#34:
	movl	$1, %eax
	subl	%edi, %eax
	imull	%edx, %eax
	cltq
	leaq	4(%rsi,%rax,4), %rsi
.LBB1_35:
	testl	%r8d, %r8d
	jns	.LBB1_37
# BB#36:
	movl	$1, %eax
	subl	%edi, %eax
	imull	%r8d, %eax
	cltq
	leaq	4(%rcx,%rax,4), %rcx
.LBB1_37:                               # %.preheader51
	testl	%edi, %edi
	jle	.LBB1_45
# BB#38:                                # %.lr.ph63
	movslq	%edx, %r9
	movslq	%r8d, %rdx
	leal	-1(%rdi), %r8d
	movl	%edi, %r10d
	andl	$3, %r10d
	je	.LBB1_39
# BB#40:                                # %.prol.preheader96
	leaq	(,%r9,4), %r11
	leaq	(,%rdx,4), %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_41:                               # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx), %xmm1
	movss	%xmm1, (%rcx)
	incl	%eax
	addq	%r11, %rsi
	addq	%rbx, %rcx
	cmpl	%eax, %r10d
	jne	.LBB1_41
	jmp	.LBB1_42
.LBB1_39:
	xorl	%eax, %eax
.LBB1_42:                               # %.prol.loopexit97
	cmpl	$3, %r8d
	jb	.LBB1_45
# BB#43:                                # %.lr.ph63.new
	subl	%eax, %edi
	shlq	$2, %r9
	shlq	$2, %rdx
	.p2align	4, 0x90
.LBB1_44:                               # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx), %xmm1
	movss	%xmm1, (%rcx)
	movss	(%rsi,%r9), %xmm1       # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addq	%r9, %rsi
	addss	(%rcx,%rdx), %xmm1
	movss	%xmm1, (%rcx,%rdx)
	leaq	(%rcx,%rdx), %rax
	movss	(%r9,%rsi), %xmm1       # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addq	%r9, %rsi
	addss	(%rdx,%rax), %xmm1
	movss	%xmm1, (%rdx,%rax)
	leaq	(%rax,%rdx), %rax
	movss	(%r9,%rsi), %xmm1       # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addq	%r9, %rsi
	addss	(%rdx,%rax), %xmm1
	movss	%xmm1, (%rdx,%rax)
	leaq	(%rax,%rdx), %rcx
	addq	%r9, %rsi
	addq	%rdx, %rcx
	addl	$-4, %edi
	jne	.LBB1_44
	jmp	.LBB1_45
.LBB1_26:
	xorl	%edx, %edx
	cmpl	$3, %r8d
	jb	.LBB1_45
.LBB1_31:                               # %.lr.ph59.new
	subl	%edx, %edi
	movq	%r11, %r8
	shlq	$4, %r8
	leaq	(,%r11,4), %r9
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_32:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rsi,%rdx), %rbx
	leaq	(%rcx,%rdx), %rax
	movss	(%rsi,%rdx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx,%rdx), %xmm1
	movss	%xmm1, (%rcx,%rdx)
	movss	(%rbx,%r11,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rax,%r11,4), %xmm1
	movss	%xmm1, (%rax,%r11,4)
	movss	(%rbx,%r11,8), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	leaq	(%rbx,%r9), %rbx
	addss	(%rax,%r11,8), %xmm1
	movss	%xmm1, (%rax,%r11,8)
	leaq	(%rax,%r9), %rax
	movss	(%rbx,%r11,8), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rax,%r11,8), %xmm1
	movss	%xmm1, (%rax,%r11,8)
	addq	%r8, %rdx
	addl	$-4, %edi
	jne	.LBB1_32
	jmp	.LBB1_45
.LBB1_10:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.LBB1_15
.LBB1_13:                               # %vector.ph.new
	movq	%r8, %r11
	subq	%rdx, %r11
	leaq	48(%rcx,%rdx,4), %rax
	leaq	48(%rsi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB1_14:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rdx), %xmm2
	movups	-32(%rdx), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	-48(%rax), %xmm4
	movups	-32(%rax), %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, -48(%rax)
	movups	%xmm5, -32(%rax)
	movups	-16(%rdx), %xmm2
	movups	(%rdx), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	-16(%rax), %xmm4
	movups	(%rax), %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, -16(%rax)
	movups	%xmm5, (%rax)
	addq	$64, %rax
	addq	$64, %rdx
	addq	$-16, %r11
	jne	.LBB1_14
.LBB1_15:                               # %middle.block
	cmpq	%r8, %r9
	je	.LBB1_45
# BB#16:
	leaq	(%rsi,%r8,4), %rsi
	leaq	(%rcx,%r8,4), %rcx
	movl	%r8d, %edx
.LBB1_17:                               # %.lr.ph.preheader88
	movl	%edi, %eax
	subl	%edx, %eax
	subl	%edx, %r10d
	andl	$3, %eax
	je	.LBB1_20
# BB#18:                                # %.lr.ph.prol.preheader
	negl	%eax
	.p2align	4, 0x90
.LBB1_19:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx), %xmm1
	movss	%xmm1, (%rcx)
	incl	%edx
	addq	$4, %rcx
	addq	$4, %rsi
	incl	%eax
	jne	.LBB1_19
.LBB1_20:                               # %.lr.ph.prol.loopexit
	cmpl	$3, %r10d
	jb	.LBB1_45
# BB#21:                                # %.lr.ph.preheader88.new
	subl	%edx, %edi
	.p2align	4, 0x90
.LBB1_22:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx), %xmm1
	movss	%xmm1, (%rcx)
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	4(%rcx), %xmm1
	movss	%xmm1, 4(%rcx)
	movss	8(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	8(%rcx), %xmm1
	movss	%xmm1, 8(%rcx)
	movss	12(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	12(%rcx), %xmm1
	movss	%xmm1, 12(%rcx)
	addq	$16, %rsi
	addq	$16, %rcx
	addl	$-4, %edi
	jne	.LBB1_22
.LBB1_45:                               # %.loopexit
	popq	%rbx
	retq
.Lfunc_end1:
	.size	saxpy, .Lfunc_end1-saxpy
	.cfi_endproc

	.globl	saxpyx
	.p2align	4, 0x90
	.type	saxpyx,@function
saxpyx:                                 # @saxpyx
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB2_45
# BB#1:
	cvtsd2ss	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB2_2
	jnp	.LBB2_45
.LBB2_2:
	cmpl	%r8d, %edx
	jne	.LBB2_33
# BB#3:
	cmpl	$1, %edx
	jne	.LBB2_23
# BB#4:                                 # %.preheader
	testl	%edi, %edi
	jle	.LBB2_45
# BB#5:                                 # %.lr.ph.preheader
	leal	-1(%rdi), %r10d
	leaq	1(%r10), %r9
	xorl	%edx, %edx
	cmpq	$8, %r9
	jb	.LBB2_17
# BB#6:                                 # %min.iters.checked
	movabsq	$8589934584, %r8        # imm = 0x1FFFFFFF8
	andq	%r9, %r8
	je	.LBB2_17
# BB#7:                                 # %vector.memcheck
	leaq	4(%rcx,%r10,4), %rax
	cmpq	%rsi, %rax
	jbe	.LBB2_9
# BB#8:                                 # %vector.memcheck
	leaq	4(%rsi,%r10,4), %rax
	cmpq	%rcx, %rax
	ja	.LBB2_17
.LBB2_9:                                # %vector.ph
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	leaq	-8(%r8), %rdx
	movq	%rdx, %rax
	shrq	$3, %rax
	btl	$3, %edx
	jb	.LBB2_10
# BB#11:                                # %vector.body.prol
	movups	(%rcx), %xmm2
	movups	16(%rcx), %xmm3
	movups	(%rsi), %xmm4
	movups	16(%rsi), %xmm5
	mulps	%xmm1, %xmm4
	mulps	%xmm1, %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, (%rsi)
	movups	%xmm5, 16(%rsi)
	movl	$8, %edx
	testq	%rax, %rax
	jne	.LBB2_13
	jmp	.LBB2_15
.LBB2_23:
	testl	%edx, %edx
	jle	.LBB2_33
# BB#24:                                # %.preheader52
	testl	%edi, %edi
	jle	.LBB2_45
# BB#25:                                # %.lr.ph62
	movslq	%edx, %r11
	leal	-1(%rdi), %r8d
	movl	%edi, %r9d
	andl	$3, %r9d
	je	.LBB2_26
# BB#27:                                # %.prol.preheader
	leaq	(,%r11,4), %r10
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_28:                               # =>This Inner Loop Header: Depth=1
	movss	(%rsi,%rbx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx,%rbx), %xmm1
	movss	%xmm1, (%rsi,%rbx)
	incl	%edx
	addq	%r10, %rbx
	cmpl	%edx, %r9d
	jne	.LBB2_28
# BB#29:                                # %.prol.loopexit.unr-lcssa
	addq	%rbx, %rsi
	addq	%rbx, %rcx
	cmpl	$3, %r8d
	jae	.LBB2_31
	jmp	.LBB2_45
.LBB2_33:
	testl	%edx, %edx
	jns	.LBB2_35
# BB#34:
	movl	$1, %eax
	subl	%edi, %eax
	imull	%edx, %eax
	cltq
	leaq	4(%rsi,%rax,4), %rsi
.LBB2_35:
	testl	%r8d, %r8d
	jns	.LBB2_37
# BB#36:
	movl	$1, %eax
	subl	%edi, %eax
	imull	%r8d, %eax
	cltq
	leaq	4(%rcx,%rax,4), %rcx
.LBB2_37:                               # %.preheader54
	testl	%edi, %edi
	jle	.LBB2_45
# BB#38:                                # %.lr.ph66
	movslq	%edx, %rax
	movslq	%r8d, %r9
	leal	-1(%rdi), %r8d
	movl	%edi, %r10d
	andl	$3, %r10d
	je	.LBB2_39
# BB#40:                                # %.prol.preheader99
	leaq	(,%rax,4), %r11
	leaq	(,%r9,4), %rbx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_41:                               # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx), %xmm1
	movss	%xmm1, (%rsi)
	incl	%edx
	addq	%r11, %rsi
	addq	%rbx, %rcx
	cmpl	%edx, %r10d
	jne	.LBB2_41
	jmp	.LBB2_42
.LBB2_39:
	xorl	%edx, %edx
.LBB2_42:                               # %.prol.loopexit100
	cmpl	$3, %r8d
	jb	.LBB2_45
# BB#43:                                # %.lr.ph66.new
	subl	%edx, %edi
	shlq	$2, %r9
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB2_44:                               # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx), %xmm1
	movss	%xmm1, (%rsi)
	movss	(%rsi,%rax), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx,%r9), %xmm1
	addq	%r9, %rcx
	movss	%xmm1, (%rsi,%rax)
	leaq	(%rsi,%rax), %rdx
	movss	(%rax,%rdx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%r9,%rcx), %xmm1
	addq	%r9, %rcx
	movss	%xmm1, (%rax,%rdx)
	leaq	(%rdx,%rax), %rdx
	movss	(%rax,%rdx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%r9,%rcx), %xmm1
	addq	%r9, %rcx
	movss	%xmm1, (%rax,%rdx)
	leaq	(%rdx,%rax), %rsi
	addq	%r9, %rcx
	addq	%rax, %rsi
	addl	$-4, %edi
	jne	.LBB2_44
	jmp	.LBB2_45
.LBB2_26:
	xorl	%edx, %edx
	cmpl	$3, %r8d
	jb	.LBB2_45
.LBB2_31:                               # %.lr.ph62.new
	subl	%edx, %edi
	movq	%r11, %r8
	shlq	$4, %r8
	leaq	(,%r11,4), %r9
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_32:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rsi,%rdx), %rbx
	leaq	(%rcx,%rdx), %rax
	movss	(%rsi,%rdx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx,%rdx), %xmm1
	movss	%xmm1, (%rsi,%rdx)
	movss	(%rbx,%r11,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rax,%r11,4), %xmm1
	movss	%xmm1, (%rbx,%r11,4)
	movss	(%rbx,%r11,8), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rax,%r11,8), %xmm1
	leaq	(%rax,%r9), %rax
	movss	%xmm1, (%rbx,%r11,8)
	leaq	(%rbx,%r9), %rbx
	movss	(%rbx,%r11,8), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rax,%r11,8), %xmm1
	movss	%xmm1, (%rbx,%r11,8)
	addq	%r8, %rdx
	addl	$-4, %edi
	jne	.LBB2_32
	jmp	.LBB2_45
.LBB2_10:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.LBB2_15
.LBB2_13:                               # %vector.ph.new
	movq	%r8, %r11
	subq	%rdx, %r11
	leaq	48(%rcx,%rdx,4), %rax
	leaq	48(%rsi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB2_14:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rax), %xmm2
	movups	-32(%rax), %xmm3
	movups	-48(%rdx), %xmm4
	movups	-32(%rdx), %xmm5
	mulps	%xmm1, %xmm4
	mulps	%xmm1, %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, -48(%rdx)
	movups	%xmm5, -32(%rdx)
	movups	-16(%rax), %xmm2
	movups	(%rax), %xmm3
	movups	-16(%rdx), %xmm4
	movups	(%rdx), %xmm5
	mulps	%xmm1, %xmm4
	mulps	%xmm1, %xmm5
	addps	%xmm2, %xmm4
	addps	%xmm3, %xmm5
	movups	%xmm4, -16(%rdx)
	movups	%xmm5, (%rdx)
	addq	$64, %rax
	addq	$64, %rdx
	addq	$-16, %r11
	jne	.LBB2_14
.LBB2_15:                               # %middle.block
	cmpq	%r8, %r9
	je	.LBB2_45
# BB#16:
	leaq	(%rsi,%r8,4), %rsi
	leaq	(%rcx,%r8,4), %rcx
	movl	%r8d, %edx
.LBB2_17:                               # %.lr.ph.preheader91
	movl	%edi, %eax
	subl	%edx, %eax
	subl	%edx, %r10d
	andl	$3, %eax
	je	.LBB2_20
# BB#18:                                # %.lr.ph.prol.preheader
	negl	%eax
	.p2align	4, 0x90
.LBB2_19:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx), %xmm1
	movss	%xmm1, (%rsi)
	incl	%edx
	addq	$4, %rsi
	addq	$4, %rcx
	incl	%eax
	jne	.LBB2_19
.LBB2_20:                               # %.lr.ph.prol.loopexit
	cmpl	$3, %r10d
	jb	.LBB2_45
# BB#21:                                # %.lr.ph.preheader91.new
	subl	%edx, %edi
	.p2align	4, 0x90
.LBB2_22:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	(%rcx), %xmm1
	movss	%xmm1, (%rsi)
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	4(%rcx), %xmm1
	movss	%xmm1, 4(%rsi)
	movss	8(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	8(%rcx), %xmm1
	movss	%xmm1, 8(%rsi)
	movss	12(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	12(%rcx), %xmm1
	movss	%xmm1, 12(%rsi)
	addq	$16, %rcx
	addq	$16, %rsi
	addl	$-4, %edi
	jne	.LBB2_22
.LBB2_45:                               # %.loopexit
	popq	%rbx
	retq
.Lfunc_end2:
	.size	saxpyx, .Lfunc_end2-saxpyx
	.cfi_endproc

	.globl	scopy
	.p2align	4, 0x90
	.type	scopy,@function
scopy:                                  # @scopy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi7:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 56
.Lcfi10:
	.cfi_offset %rbx, -56
.Lcfi11:
	.cfi_offset %r12, -48
.Lcfi12:
	.cfi_offset %r13, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB3_42
# BB#1:
	cmpl	%r8d, %edx
	jne	.LBB3_31
# BB#2:
	cmpl	$1, %edx
	jne	.LBB3_22
# BB#3:                                 # %.lr.ph.preheader
	leal	-1(%rdi), %r10d
	leaq	1(%r10), %r9
	xorl	%edx, %edx
	cmpq	$8, %r9
	jb	.LBB3_16
# BB#4:                                 # %min.iters.checked
	movabsq	$8589934584, %r8        # imm = 0x1FFFFFFF8
	andq	%r9, %r8
	je	.LBB3_16
# BB#5:                                 # %vector.memcheck
	leaq	4(%rsi,%r10,4), %rax
	cmpq	%rcx, %rax
	jbe	.LBB3_7
# BB#6:                                 # %vector.memcheck
	leaq	4(%rcx,%r10,4), %rax
	cmpq	%rsi, %rax
	ja	.LBB3_16
.LBB3_7:                                # %vector.body.preheader
	leaq	-8(%r8), %r11
	movl	%r11d, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB3_8
# BB#9:                                 # %vector.body.prol.preheader
	negq	%rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_10:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rsi,%rax,4), %xmm0
	movups	16(%rsi,%rax,4), %xmm1
	movups	%xmm0, (%rcx,%rax,4)
	movups	%xmm1, 16(%rcx,%rax,4)
	addq	$8, %rax
	incq	%rdx
	jne	.LBB3_10
	jmp	.LBB3_11
.LBB3_22:
	testl	%edx, %edx
	jle	.LBB3_31
# BB#23:                                # %.lr.ph53
	movslq	%edx, %r9
	leal	-1(%rdi), %r8d
	movl	%edi, %r11d
	andl	$7, %r11d
	je	.LBB3_24
# BB#25:                                # %.prol.preheader
	leaq	(,%r9,4), %r10
	xorl	%eax, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_26:                               # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rax), %ebx
	movl	%ebx, (%rcx,%rax)
	incl	%edx
	addq	%r10, %rax
	cmpl	%edx, %r11d
	jne	.LBB3_26
# BB#27:                                # %.prol.loopexit.unr-lcssa
	addq	%rax, %rsi
	addq	%rax, %rcx
	cmpl	$7, %r8d
	jae	.LBB3_29
	jmp	.LBB3_42
.LBB3_31:
	testl	%edx, %edx
	jns	.LBB3_33
# BB#32:
	movl	$1, %eax
	subl	%edi, %eax
	imull	%edx, %eax
	cltq
	leaq	4(%rsi,%rax,4), %rsi
.LBB3_33:
	testl	%r8d, %r8d
	jns	.LBB3_35
# BB#34:
	movl	$1, %eax
	subl	%edi, %eax
	imull	%r8d, %eax
	cltq
	leaq	4(%rcx,%rax,4), %rcx
.LBB3_35:                               # %.lr.ph57
	movslq	%edx, %r9
	movslq	%r8d, %rdx
	leal	-1(%rdi), %r8d
	movl	%edi, %r10d
	andl	$7, %r10d
	je	.LBB3_36
# BB#37:                                # %.prol.preheader87
	leaq	(,%r9,4), %r11
	leaq	(,%rdx,4), %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_38:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %ebp
	movl	%ebp, (%rsi)
	incl	%eax
	addq	%r11, %rsi
	addq	%rbx, %rcx
	cmpl	%eax, %r10d
	jne	.LBB3_38
	jmp	.LBB3_39
.LBB3_36:
	xorl	%eax, %eax
.LBB3_39:                               # %.prol.loopexit88
	cmpl	$7, %r8d
	jb	.LBB3_42
# BB#40:                                # %.lr.ph57.new
	subl	%eax, %edi
	shlq	$2, %rdx
	shlq	$2, %r9
	.p2align	4, 0x90
.LBB3_41:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %eax
	movl	%eax, (%rsi)
	movl	(%rcx,%rdx), %eax
	addq	%rdx, %rcx
	movl	%eax, (%rsi,%r9)
	addq	%r9, %rsi
	movl	(%rdx,%rcx), %eax
	addq	%rdx, %rcx
	movl	%eax, (%r9,%rsi)
	addq	%r9, %rsi
	movl	(%rdx,%rcx), %eax
	addq	%rdx, %rcx
	movl	%eax, (%r9,%rsi)
	addq	%r9, %rsi
	movl	(%rdx,%rcx), %eax
	addq	%rdx, %rcx
	movl	%eax, (%r9,%rsi)
	addq	%r9, %rsi
	movl	(%rdx,%rcx), %eax
	addq	%rdx, %rcx
	movl	%eax, (%r9,%rsi)
	addq	%r9, %rsi
	movl	(%rdx,%rcx), %eax
	addq	%rdx, %rcx
	movl	%eax, (%r9,%rsi)
	addq	%r9, %rsi
	movl	(%rdx,%rcx), %eax
	addq	%rdx, %rcx
	movl	%eax, (%r9,%rsi)
	addq	%r9, %rsi
	addq	%rdx, %rcx
	addq	%r9, %rsi
	addl	$-8, %edi
	jne	.LBB3_41
	jmp	.LBB3_42
.LBB3_24:
	xorl	%edx, %edx
	cmpl	$7, %r8d
	jb	.LBB3_42
.LBB3_29:                               # %.lr.ph53.new
	subl	%edx, %edi
	movq	%r9, %r8
	shlq	$5, %r8
	leaq	(,%r9,4), %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_30:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rsi,%rax), %rbx
	movl	(%rsi,%rax), %r10d
	leaq	(%rcx,%rax), %r11
	movl	%r10d, (%rcx,%rax)
	leaq	(%rbx,%rdx), %r10
	movl	(%rbx,%r9,4), %r14d
	leaq	(%r11,%rdx), %r13
	movl	%r14d, (%r11,%r9,4)
	leaq	(%r10,%rdx), %r14
	movl	(%rbx,%r9,8), %r15d
	leaq	(%r13,%rdx), %r12
	movl	%r15d, (%r11,%r9,8)
	leaq	(%r14,%rdx), %r11
	movl	(%r10,%r9,8), %r10d
	leaq	(%r12,%rdx), %r15
	movl	%r10d, (%r13,%r9,8)
	leaq	(%r11,%rdx), %r13
	movl	(%r14,%r9,8), %r10d
	leaq	(%r15,%rdx), %rbp
	movl	%r10d, (%r12,%r9,8)
	movl	(%r11,%r9,8), %ebx
	movl	%ebx, (%r15,%r9,8)
	movl	(%r13,%r9,8), %ebx
	addq	%rdx, %r13
	movl	%ebx, (%rbp,%r9,8)
	addq	%rdx, %rbp
	movl	(%r13,%r9,8), %ebx
	movl	%ebx, (%rbp,%r9,8)
	addq	%r8, %rax
	addl	$-8, %edi
	jne	.LBB3_30
	jmp	.LBB3_42
.LBB3_8:
	xorl	%eax, %eax
.LBB3_11:                               # %vector.body.prol.loopexit
	cmpq	$24, %r11
	jb	.LBB3_14
# BB#12:                                # %vector.body.preheader.new
	movq	%r8, %r11
	subq	%rax, %r11
	leaq	112(%rcx,%rax,4), %rdx
	leaq	112(%rsi,%rax,4), %rax
	.p2align	4, 0x90
.LBB3_13:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rax), %xmm0
	movups	-96(%rax), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rax), %xmm0
	movups	-64(%rax), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rax), %xmm0
	movups	-32(%rax), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rax), %xmm0
	movups	(%rax), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rax
	addq	$-32, %r11
	jne	.LBB3_13
.LBB3_14:                               # %middle.block
	cmpq	%r8, %r9
	je	.LBB3_42
# BB#15:
	leaq	(%rsi,%r8,4), %rsi
	leaq	(%rcx,%r8,4), %rcx
	movl	%r8d, %edx
.LBB3_16:                               # %.lr.ph.preheader78
	movl	%edi, %eax
	subl	%edx, %eax
	subl	%edx, %r10d
	andl	$7, %eax
	je	.LBB3_19
# BB#17:                                # %.lr.ph.prol.preheader
	negl	%eax
	.p2align	4, 0x90
.LBB3_18:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %ebx
	addq	$4, %rsi
	movl	%ebx, (%rcx)
	addq	$4, %rcx
	incl	%edx
	incl	%eax
	jne	.LBB3_18
.LBB3_19:                               # %.lr.ph.prol.loopexit
	cmpl	$7, %r10d
	jb	.LBB3_42
# BB#20:                                # %.lr.ph.preheader78.new
	subl	%edx, %edi
	.p2align	4, 0x90
.LBB3_21:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	movl	%eax, (%rcx)
	movl	4(%rsi), %eax
	movl	%eax, 4(%rcx)
	movl	8(%rsi), %eax
	movl	%eax, 8(%rcx)
	movl	12(%rsi), %eax
	movl	%eax, 12(%rcx)
	movl	16(%rsi), %eax
	movl	%eax, 16(%rcx)
	movl	20(%rsi), %eax
	movl	%eax, 20(%rcx)
	movl	24(%rsi), %eax
	movl	%eax, 24(%rcx)
	movl	28(%rsi), %eax
	movl	%eax, 28(%rcx)
	addq	$32, %rsi
	addq	$32, %rcx
	addl	$-8, %edi
	jne	.LBB3_21
.LBB3_42:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	scopy, .Lfunc_end3-scopy
	.cfi_endproc

	.globl	sdot
	.p2align	4, 0x90
	.type	sdot,@function
sdot:                                   # @sdot
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB4_1
# BB#2:
	cmpl	%r8d, %edx
	jne	.LBB4_18
# BB#3:
	cmpl	$1, %edx
	jne	.LBB4_9
# BB#4:                                 # %.lr.ph.preheader
	leal	-1(%rdi), %r8d
	movl	%edi, %eax
	xorl	%edx, %edx
	xorps	%xmm0, %xmm0
	andl	$3, %eax
	je	.LBB4_6
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addss	%xmm1, %xmm0
	incl	%edx
	addq	$4, %rsi
	addq	$4, %rcx
	cmpl	%edx, %eax
	jne	.LBB4_5
.LBB4_6:                                # %.lr.ph.prol.loopexit
	cmpl	$3, %r8d
	jb	.LBB4_29
# BB#7:                                 # %.lr.ph.preheader.new
	subl	%edx, %edi
	.p2align	4, 0x90
.LBB4_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addss	%xmm0, %xmm1
	mulss	4(%rcx), %xmm2
	addss	%xmm1, %xmm2
	movss	8(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	8(%rcx), %xmm1
	addss	%xmm2, %xmm1
	movss	12(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	12(%rcx), %xmm0
	addss	%xmm1, %xmm0
	addq	$16, %rsi
	addq	$16, %rcx
	addl	$-4, %edi
	jne	.LBB4_8
	jmp	.LBB4_29
.LBB4_1:
	xorps	%xmm0, %xmm0
	popq	%rbx
	retq
.LBB4_9:
	testl	%edx, %edx
	jle	.LBB4_18
# BB#10:                                # %.lr.ph69
	movslq	%edx, %r11
	leal	-1(%rdi), %r8d
	movl	%edi, %r9d
	andl	$3, %r9d
	je	.LBB4_11
# BB#12:                                # %.prol.preheader
	leaq	(,%r11,4), %r10
	xorl	%ebx, %ebx
	xorps	%xmm0, %xmm0
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_13:                               # =>This Inner Loop Header: Depth=1
	movss	(%rsi,%rbx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx,%rbx), %xmm1
	addss	%xmm1, %xmm0
	incl	%edx
	addq	%r10, %rbx
	cmpl	%edx, %r9d
	jne	.LBB4_13
# BB#14:                                # %.prol.loopexit.unr-lcssa
	addq	%rbx, %rcx
	addq	%rbx, %rsi
	cmpl	$3, %r8d
	jb	.LBB4_29
	jmp	.LBB4_16
.LBB4_18:
	testl	%edx, %edx
	jns	.LBB4_20
# BB#19:
	movl	$1, %eax
	subl	%edi, %eax
	imull	%edx, %eax
	cltq
	leaq	4(%rsi,%rax,4), %rsi
.LBB4_20:
	testl	%r8d, %r8d
	jns	.LBB4_22
# BB#21:
	movl	$1, %eax
	subl	%edi, %eax
	imull	%r8d, %eax
	cltq
	leaq	4(%rcx,%rax,4), %rcx
.LBB4_22:                               # %.lr.ph76
	movslq	%edx, %r9
	movslq	%r8d, %rdx
	leal	-1(%rdi), %r8d
	movl	%edi, %r10d
	andl	$3, %r10d
	je	.LBB4_23
# BB#24:                                # %.prol.preheader94
	leaq	(,%rdx,4), %r11
	leaq	(,%r9,4), %rbx
	xorl	%eax, %eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_25:                               # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addss	%xmm1, %xmm0
	incl	%eax
	addq	%r11, %rcx
	addq	%rbx, %rsi
	cmpl	%eax, %r10d
	jne	.LBB4_25
	jmp	.LBB4_26
.LBB4_23:
	xorl	%eax, %eax
	xorps	%xmm0, %xmm0
.LBB4_26:                               # %.prol.loopexit95
	cmpl	$3, %r8d
	jb	.LBB4_29
# BB#27:                                # %.lr.ph76.new
	subl	%eax, %edi
	shlq	$2, %r9
	shlq	$2, %rdx
	.p2align	4, 0x90
.LBB4_28:                               # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addss	%xmm0, %xmm1
	movss	(%rsi,%r9), %xmm0       # xmm0 = mem[0],zero,zero,zero
	addq	%r9, %rsi
	mulss	(%rcx,%rdx), %xmm0
	addq	%rdx, %rcx
	addss	%xmm1, %xmm0
	movss	(%r9,%rsi), %xmm1       # xmm1 = mem[0],zero,zero,zero
	addq	%r9, %rsi
	mulss	(%rdx,%rcx), %xmm1
	addq	%rdx, %rcx
	addss	%xmm0, %xmm1
	movss	(%r9,%rsi), %xmm0       # xmm0 = mem[0],zero,zero,zero
	addq	%r9, %rsi
	mulss	(%rdx,%rcx), %xmm0
	addq	%rdx, %rcx
	addss	%xmm1, %xmm0
	addq	%r9, %rsi
	addq	%rdx, %rcx
	addl	$-4, %edi
	jne	.LBB4_28
.LBB4_29:                               # %._crit_edge77
	cvtss2sd	%xmm0, %xmm0
	popq	%rbx
	retq
.LBB4_11:
	xorl	%edx, %edx
	xorps	%xmm0, %xmm0
	cmpl	$3, %r8d
	jb	.LBB4_29
.LBB4_16:                               # %.lr.ph69.new
	subl	%edx, %edi
	movq	%r11, %r8
	shlq	$4, %r8
	leaq	(,%r11,4), %r9
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_17:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rcx,%rdx), %rbx
	leaq	(%rsi,%rdx), %rax
	movss	(%rsi,%rdx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx,%rdx), %xmm1
	addss	%xmm0, %xmm1
	movss	(%rax,%r11,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	mulss	(%rbx,%r11,4), %xmm0
	addss	%xmm1, %xmm0
	movss	(%rax,%r11,8), %xmm1    # xmm1 = mem[0],zero,zero,zero
	leaq	(%rax,%r9), %rax
	mulss	(%rbx,%r11,8), %xmm1
	leaq	(%rbx,%r9), %rbx
	addss	%xmm0, %xmm1
	movss	(%rax,%r11,8), %xmm0    # xmm0 = mem[0],zero,zero,zero
	mulss	(%rbx,%r11,8), %xmm0
	addss	%xmm1, %xmm0
	addq	%r8, %rdx
	addl	$-4, %edi
	jne	.LBB4_17
	jmp	.LBB4_29
.Lfunc_end4:
	.size	sdot, .Lfunc_end4-sdot
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4891288408196988160     # double 1.0E+19
.LCPI5_2:
	.quad	4322057388060732610     # double 9.1589344358391385E-20
.LCPI5_3:
	.quad	4607182418800017408     # double 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	snrm2
	.p2align	4, 0x90
	.type	snrm2,@function
snrm2:                                  # @snrm2
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 16
	testl	%edi, %edi
	xorpd	%xmm3, %xmm3
	jle	.LBB5_37
# BB#1:
	testl	%edx, %edx
	jle	.LBB5_37
# BB#2:
	testl	%edi, %edi
	jle	.LBB5_37
# BB#3:                                 # %.lr.ph203
	cvtsi2sdl	%edi, %xmm0
	movsd	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movslq	%edx, %r8
	leaq	(,%r8,4), %rcx
	movl	$1, %edx
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB5_4:                                # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	jne	.LBB5_6
	jp	.LBB5_6
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=1
	addq	%rcx, %rsi
	cmpl	%edi, %edx
	leal	1(%rdx), %eax
	movl	%eax, %edx
	jl	.LBB5_4
	jmp	.LBB5_37
.LBB5_6:                                # %.critedge.thread.preheader
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm0
	xorps	%xmm6, %xmm6
	cmpltss	%xmm2, %xmm6
	movaps	%xmm6, %xmm3
	andps	%xmm2, %xmm3
	andnps	%xmm0, %xmm6
	orps	%xmm3, %xmm6
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm6, %xmm0
	ucomisd	.LCPI5_2(%rip), %xmm0
	jbe	.LBB5_8
# BB#7:
	decl	%edx
	xorpd	%xmm0, %xmm0
	cmpl	%edi, %edx
	jge	.LBB5_28
.LBB5_17:                               # %.lr.ph168.preheader
	movaps	.LCPI5_1(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm4
	xorps	%xmm3, %xmm3
	cmpltss	%xmm2, %xmm3
	movaps	%xmm3, %xmm5
	andps	%xmm2, %xmm5
	andnps	%xmm4, %xmm3
	orps	%xmm5, %xmm3
	xorps	%xmm4, %xmm4
	cvtss2sd	%xmm3, %xmm4
	ucomisd	%xmm1, %xmm4
	ja	.LBB5_22
# BB#18:                                # %.lr.ph332.preheader
	movaps	.LCPI5_1(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB5_19:                               # %.lr.ph332
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, %xmm3
	mulss	%xmm2, %xmm2
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	incl	%edx
	cmpl	%edi, %edx
	jge	.LBB5_28
# BB#20:                                # %._crit_edge
                                        #   in Loop: Header=BB5_19 Depth=1
	leaq	(%rcx,%rsi), %rax
	movss	(%rcx,%rsi), %xmm2      # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	xorps	%xmm4, %xmm5
	xorpd	%xmm3, %xmm3
	cmpltss	%xmm2, %xmm3
	movaps	%xmm3, %xmm6
	andps	%xmm2, %xmm6
	andnps	%xmm5, %xmm3
	orps	%xmm6, %xmm3
	xorps	%xmm5, %xmm5
	cvtss2sd	%xmm3, %xmm5
	ucomisd	%xmm1, %xmm5
	movq	%rax, %rsi
	jbe	.LBB5_19
# BB#21:
	movq	%rax, %rsi
.LBB5_22:                               # %.lr.ph168._crit_edge
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm1
	divsd	%xmm1, %xmm0
	divsd	%xmm1, %xmm0
	addsd	.LCPI5_3(%rip), %xmm0
	incl	%edx
	cmpl	%edi, %edx
	jge	.LBB5_30
# BB#23:                                # %.lr.ph.preheader
	movaps	.LCPI5_1(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movsd	.LCPI5_3(%rip), %xmm2   # xmm2 = mem[0],zero
.LBB5_24:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_25 Depth 2
	leaq	(%rsi,%r8,4), %rsi
	.p2align	4, 0x90
.LBB5_25:                               #   Parent Loop BB5_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rsi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm6
	xorps	%xmm1, %xmm6
	xorps	%xmm4, %xmm4
	cmpltss	%xmm5, %xmm4
	movaps	%xmm4, %xmm7
	andps	%xmm5, %xmm7
	andnps	%xmm6, %xmm4
	orps	%xmm7, %xmm4
	ucomiss	%xmm3, %xmm4
	ja	.LBB5_27
# BB#26:                                #   in Loop: Header=BB5_25 Depth=2
	divss	%xmm3, %xmm5
	mulss	%xmm5, %xmm5
	xorps	%xmm4, %xmm4
	cvtss2sd	%xmm5, %xmm4
	addsd	%xmm4, %xmm0
	incl	%edx
	addq	%rcx, %rsi
	cmpl	%edi, %edx
	jl	.LBB5_25
	jmp	.LBB5_30
.LBB5_27:                               # %.outer
                                        #   in Loop: Header=BB5_24 Depth=1
	divss	%xmm5, %xmm3
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm3, %xmm0
	mulsd	%xmm3, %xmm0
	addsd	%xmm2, %xmm0
	incl	%edx
	cmpl	%edi, %edx
	movaps	%xmm4, %xmm3
	jl	.LBB5_24
	jmp	.LBB5_31
.LBB5_8:                                # %.lr.ph196
	cmpl	%edi, %edx
	jge	.LBB5_15
# BB#9:                                 # %.lr.ph177.preheader
	movsd	.LCPI5_3(%rip), %xmm8   # xmm8 = mem[0],zero
	movaps	.LCPI5_1(%rip), %xmm9   # xmm9 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movsd	.LCPI5_2(%rip), %xmm5   # xmm5 = mem[0],zero
	movaps	%xmm6, %xmm7
	movapd	%xmm8, %xmm0
.LBB5_10:                               # %.lr.ph177
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_11 Depth 2
	leaq	(%rsi,%r8,4), %rsi
	.p2align	4, 0x90
.LBB5_11:                               #   Parent Loop BB5_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	xorps	%xmm9, %xmm3
	xorps	%xmm6, %xmm6
	cmpltss	%xmm2, %xmm6
	movaps	%xmm6, %xmm4
	andps	%xmm2, %xmm4
	andnps	%xmm3, %xmm6
	orps	%xmm4, %xmm6
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm6, %xmm3
	ucomisd	%xmm5, %xmm3
	ja	.LBB5_16
# BB#12:                                #   in Loop: Header=BB5_11 Depth=2
	ucomiss	%xmm7, %xmm6
	ja	.LBB5_14
# BB#13:                                #   in Loop: Header=BB5_11 Depth=2
	divss	%xmm7, %xmm2
	mulss	%xmm2, %xmm2
	cvtss2sd	%xmm2, %xmm2
	addsd	%xmm2, %xmm0
	incl	%edx
	addq	%rcx, %rsi
	cmpl	%edi, %edx
	jl	.LBB5_11
	jmp	.LBB5_32
.LBB5_14:                               # %.outer123
                                        #   in Loop: Header=BB5_10 Depth=1
	divss	%xmm2, %xmm7
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm7, %xmm2
	mulsd	%xmm2, %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm8, %xmm0
	incl	%edx
	cmpl	%edi, %edx
	movaps	%xmm6, %xmm7
	jl	.LBB5_10
	jmp	.LBB5_33
.LBB5_15:
	movsd	.LCPI5_3(%rip), %xmm0   # xmm0 = mem[0],zero
	jmp	.LBB5_33
.LBB5_30:
	movaps	%xmm3, %xmm4
.LBB5_31:                               # %.outer._crit_edge
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm4, %xmm3
	jmp	.LBB5_34
.LBB5_16:                               # %.preheader.loopexit
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm7, %xmm3
	mulsd	%xmm3, %xmm0
	mulsd	%xmm3, %xmm0
	cmpl	%edi, %edx
	jl	.LBB5_17
.LBB5_28:                               # %._crit_edge169
	xorps	%xmm3, %xmm3
	sqrtsd	%xmm0, %xmm3
	ucomisd	%xmm3, %xmm3
	jnp	.LBB5_37
# BB#29:                                # %call.sqrt
	popq	%rax
	jmp	sqrt                    # TAILCALL
.LBB5_32:
	movaps	%xmm7, %xmm6
.LBB5_33:                               # %.outer123._crit_edge
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm6, %xmm3
.LBB5_34:                               # %.outer._crit_edge
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB5_36
# BB#35:                                # %call.sqrt427
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	callq	sqrt
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB5_36:                               # %.outer._crit_edge.split
	mulsd	%xmm1, %xmm3
.LBB5_37:                               # %.critedge.thread122
	movapd	%xmm3, %xmm0
	popq	%rax
	retq
.Lfunc_end5:
	.size	snrm2, .Lfunc_end5-snrm2
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
	.text
	.globl	r1mach
	.p2align	4, 0x90
	.type	r1mach,@function
r1mach:                                 # @r1mach
	.cfi_startproc
# BB#0:
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	retq
.Lfunc_end6:
	.size	r1mach, .Lfunc_end6-r1mach
	.cfi_endproc

	.globl	min0
	.p2align	4, 0x90
	.type	min0,@function
min0:                                   # @min0
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	-1(%rdi), %r10d
	movl	$-1, %eax
	cmpl	$14, %r10d
	ja	.LBB7_17
# BB#1:
	cmpl	$1, %edi
	je	.LBB7_16
# BB#2:
	cmpl	%edx, %esi
	cmovgl	%edx, %esi
	cmpl	$2, %edi
	je	.LBB7_16
# BB#3:
	cmpl	%ecx, %esi
	cmovgl	%ecx, %esi
	cmpl	$3, %edi
	je	.LBB7_16
# BB#4:
	cmpl	%r8d, %esi
	cmovgl	%r8d, %esi
	cmpl	$4, %edi
	je	.LBB7_16
# BB#5:
	cmpl	%r9d, %esi
	cmovgl	%r9d, %esi
	cmpl	$5, %edi
	je	.LBB7_16
# BB#6:
	movl	8(%rsp), %eax
	cmpl	%eax, %esi
	cmovgl	%eax, %esi
	cmpl	$6, %edi
	je	.LBB7_16
# BB#7:
	movl	16(%rsp), %eax
	cmpl	%eax, %esi
	cmovgl	%eax, %esi
	cmpl	$7, %edi
	je	.LBB7_16
# BB#8:
	movl	24(%rsp), %eax
	cmpl	%eax, %esi
	cmovgl	%eax, %esi
	cmpl	$8, %edi
	je	.LBB7_16
# BB#9:
	movl	32(%rsp), %eax
	cmpl	%eax, %esi
	cmovgl	%eax, %esi
	cmpl	$9, %edi
	je	.LBB7_16
# BB#10:
	movl	40(%rsp), %eax
	cmpl	%eax, %esi
	cmovgl	%eax, %esi
	cmpl	$10, %edi
	je	.LBB7_16
# BB#11:
	movl	48(%rsp), %eax
	cmpl	%eax, %esi
	cmovgl	%eax, %esi
	cmpl	$11, %edi
	je	.LBB7_16
# BB#12:
	movl	56(%rsp), %eax
	cmpl	%eax, %esi
	cmovgl	%eax, %esi
	cmpl	$12, %edi
	je	.LBB7_16
# BB#13:
	movl	64(%rsp), %eax
	cmpl	%eax, %esi
	cmovgl	%eax, %esi
	cmpl	$13, %edi
	je	.LBB7_16
# BB#14:
	movl	72(%rsp), %eax
	cmpl	%eax, %esi
	cmovgl	%eax, %esi
	cmpl	$14, %edi
	je	.LBB7_16
# BB#15:
	movl	80(%rsp), %eax
	cmpl	%eax, %esi
	cmovgl	%eax, %esi
.LBB7_16:
	movl	%esi, %eax
.LBB7_17:
	retq
.Lfunc_end7:
	.size	min0, .Lfunc_end7-min0
	.cfi_endproc

	.globl	sscal
	.p2align	4, 0x90
	.type	sscal,@function
sscal:                                  # @sscal
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB8_1
# BB#2:
	cvtsd2ss	%xmm0, %xmm0
	cmpl	$1, %edx
	jne	.LBB8_16
# BB#3:                                 # %.lr.ph.preheader
	leal	-1(%rdi), %ecx
	incq	%rcx
	xorl	%eax, %eax
	cmpq	$7, %rcx
	jbe	.LBB8_4
# BB#7:                                 # %min.iters.checked
	movabsq	$8589934584, %r8        # imm = 0x1FFFFFFF8
	andq	%rcx, %r8
	je	.LBB8_4
# BB#8:                                 # %vector.ph
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	leaq	-8(%r8), %rdx
	movq	%rdx, %rax
	shrq	$3, %rax
	btl	$3, %edx
	jb	.LBB8_9
# BB#10:                                # %vector.body.prol
	movups	(%rsi), %xmm2
	movups	16(%rsi), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	%xmm2, (%rsi)
	movups	%xmm3, 16(%rsi)
	movl	$8, %edx
	testq	%rax, %rax
	jne	.LBB8_12
	jmp	.LBB8_14
.LBB8_1:
	movl	$1, %eax
	retq
.LBB8_16:
	testl	%edx, %edx
	jns	.LBB8_18
# BB#17:
	movl	$1, %eax
	subl	%edi, %eax
	imull	%edx, %eax
	cltq
	leaq	(%rsi,%rax,4), %rsi
.LBB8_18:                               # %.lr.ph29
	movslq	%edx, %rcx
	leal	-1(%rdi), %r8d
	xorl	%eax, %eax
	movl	%edi, %r9d
	andl	$3, %r9d
	je	.LBB8_19
# BB#20:                                # %.prol.preheader
	leaq	(,%rcx,4), %r10
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB8_21:                               # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsi)
	incl	%edx
	addq	%r10, %rsi
	cmpl	%edx, %r9d
	jne	.LBB8_21
	jmp	.LBB8_22
.LBB8_19:
	xorl	%edx, %edx
.LBB8_22:                               # %.prol.loopexit
	cmpl	$3, %r8d
	jb	.LBB8_25
# BB#23:                                # %.lr.ph29.new
	subl	%edx, %edi
	shlq	$2, %rcx
	.p2align	4, 0x90
.LBB8_24:                               # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsi)
	movss	(%rsi,%rcx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsi,%rcx)
	leaq	(%rsi,%rcx), %rdx
	movss	(%rcx,%rdx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rcx,%rdx)
	leaq	(%rdx,%rcx), %rdx
	movss	(%rcx,%rdx), %xmm1      # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rcx,%rdx)
	leaq	(%rdx,%rcx), %rsi
	addq	%rcx, %rsi
	addl	$-4, %edi
	jne	.LBB8_24
	jmp	.LBB8_25
.LBB8_9:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.LBB8_14
.LBB8_12:                               # %vector.ph.new
	movq	%r8, %rax
	subq	%rdx, %rax
	leaq	48(%rsi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB8_13:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rdx), %xmm2
	movups	-32(%rdx), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	%xmm2, -48(%rdx)
	movups	%xmm3, -32(%rdx)
	movups	-16(%rdx), %xmm2
	movups	(%rdx), %xmm3
	mulps	%xmm1, %xmm2
	mulps	%xmm1, %xmm3
	movups	%xmm2, -16(%rdx)
	movups	%xmm3, (%rdx)
	addq	$64, %rdx
	addq	$-16, %rax
	jne	.LBB8_13
.LBB8_14:                               # %middle.block
	xorl	%eax, %eax
	cmpq	%r8, %rcx
	jne	.LBB8_15
.LBB8_25:                               # %.loopexit
	retq
.LBB8_15:
	leaq	(%rsi,%r8,4), %rsi
	movl	%r8d, %eax
.LBB8_4:                                # %.lr.ph.preheader41
	subl	%eax, %edi
	.p2align	4, 0x90
.LBB8_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsi)
	addq	$4, %rsi
	decl	%edi
	jne	.LBB8_5
# BB#6:
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	sscal, .Lfunc_end8-sscal
	.cfi_endproc

	.globl	vexopy
	.p2align	4, 0x90
	.type	vexopy,@function
vexopy:                                 # @vexopy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB9_38
# BB#1:
	movabsq	$8589934584, %r9        # imm = 0x1FFFFFFF8
	leal	-1(%rdi), %r11d
	leaq	1(%r11), %r10
	cmpl	$1, %r8d
	jne	.LBB9_2
# BB#20:                                # %.lr.ph.preheader
	xorl	%r8d, %r8d
	cmpq	$8, %r10
	jb	.LBB9_32
# BB#21:                                # %min.iters.checked68
	andq	%r10, %r9
	je	.LBB9_32
# BB#22:                                # %vector.memcheck87
	leaq	4(%rsi,%r11,4), %rbp
	leaq	4(%rdx,%r11,4), %rax
	leaq	4(%rcx,%r11,4), %r14
	cmpq	%rax, %rsi
	sbbb	%al, %al
	cmpq	%rbp, %rdx
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%r14, %rsi
	sbbb	%al, %al
	cmpq	%rbp, %rcx
	sbbb	%bpl, %bpl
	testb	$1, %bl
	jne	.LBB9_32
# BB#23:                                # %vector.memcheck87
	andb	%bpl, %al
	andb	$1, %al
	jne	.LBB9_32
# BB#24:                                # %vector.body64.preheader
	leaq	-8(%r9), %rax
	movq	%rax, %rbx
	shrq	$3, %rbx
	btl	$3, %eax
	jb	.LBB9_25
# BB#26:                                # %vector.body64.prol
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movups	(%rcx), %xmm2
	movups	16(%rcx), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, (%rsi)
	movups	%xmm3, 16(%rsi)
	movl	$8, %eax
	testq	%rbx, %rbx
	jne	.LBB9_28
	jmp	.LBB9_30
.LBB9_2:                                # %.lr.ph36.preheader
	xorl	%r8d, %r8d
	cmpq	$7, %r10
	jbe	.LBB9_3
# BB#9:                                 # %min.iters.checked
	andq	%r10, %r9
	je	.LBB9_3
# BB#10:                                # %vector.memcheck
	leaq	4(%rsi,%r11,4), %rbp
	leaq	4(%rdx,%r11,4), %rax
	leaq	4(%rcx,%r11,4), %r14
	cmpq	%rax, %rsi
	sbbb	%al, %al
	cmpq	%rbp, %rdx
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%r14, %rsi
	sbbb	%al, %al
	cmpq	%rbp, %rcx
	sbbb	%bpl, %bpl
	testb	$1, %bl
	jne	.LBB9_3
# BB#11:                                # %vector.memcheck
	andb	%bpl, %al
	andb	$1, %al
	jne	.LBB9_3
# BB#12:                                # %vector.body.preheader
	leaq	-8(%r9), %rax
	movq	%rax, %rbx
	shrq	$3, %rbx
	btl	$3, %eax
	jb	.LBB9_13
# BB#14:                                # %vector.body.prol
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movups	(%rcx), %xmm2
	movups	16(%rcx), %xmm3
	subps	%xmm2, %xmm0
	subps	%xmm3, %xmm1
	movups	%xmm0, (%rsi)
	movups	%xmm1, 16(%rsi)
	movl	$8, %eax
	testq	%rbx, %rbx
	jne	.LBB9_16
	jmp	.LBB9_18
.LBB9_25:
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB9_30
.LBB9_28:                               # %vector.body64.preheader.new
	movq	%r9, %r8
	subq	%rax, %r8
	leaq	48(%rdx,%rax,4), %r14
	leaq	48(%rcx,%rax,4), %rbx
	leaq	48(%rsi,%rax,4), %rax
	.p2align	4, 0x90
.LBB9_29:                               # %vector.body64
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%r14), %xmm0
	movups	-32(%r14), %xmm1
	movups	-48(%rbx), %xmm2
	movups	-32(%rbx), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, -48(%rax)
	movups	%xmm3, -32(%rax)
	movups	-16(%r14), %xmm0
	movups	(%r14), %xmm1
	movups	-16(%rbx), %xmm2
	movups	(%rbx), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, -16(%rax)
	movups	%xmm3, (%rax)
	addq	$64, %r14
	addq	$64, %rbx
	addq	$64, %rax
	addq	$-16, %r8
	jne	.LBB9_29
.LBB9_30:                               # %middle.block65
	cmpq	%r9, %r10
	je	.LBB9_38
# BB#31:
	leaq	(%rsi,%r9,4), %rsi
	leaq	(%rcx,%r9,4), %rcx
	leaq	(%rdx,%r9,4), %rdx
	movl	%r9d, %r8d
.LBB9_32:                               # %.lr.ph.preheader116
	movl	%edi, %eax
	subl	%r8d, %eax
	subl	%r8d, %r11d
	andl	$3, %eax
	je	.LBB9_35
# BB#33:                                # %.lr.ph.prol.preheader
	negl	%eax
	.p2align	4, 0x90
.LBB9_34:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rcx), %xmm0
	movss	%xmm0, (%rsi)
	incl	%r8d
	addq	$4, %rdx
	addq	$4, %rcx
	addq	$4, %rsi
	incl	%eax
	jne	.LBB9_34
.LBB9_35:                               # %.lr.ph.prol.loopexit
	cmpl	$3, %r11d
	jb	.LBB9_38
# BB#36:                                # %.lr.ph.preheader116.new
	subl	%r8d, %edi
	.p2align	4, 0x90
.LBB9_37:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rcx), %xmm0
	movss	%xmm0, (%rsi)
	movss	4(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	4(%rcx), %xmm0
	movss	%xmm0, 4(%rsi)
	movss	8(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	8(%rcx), %xmm0
	movss	%xmm0, 8(%rsi)
	movss	12(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	12(%rcx), %xmm0
	movss	%xmm0, 12(%rsi)
	addq	$16, %rdx
	addq	$16, %rcx
	addq	$16, %rsi
	addl	$-4, %edi
	jne	.LBB9_37
	jmp	.LBB9_38
.LBB9_13:
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB9_18
.LBB9_16:                               # %vector.body.preheader.new
	movq	%r9, %r8
	subq	%rax, %r8
	leaq	48(%rdx,%rax,4), %r14
	leaq	48(%rcx,%rax,4), %rbx
	leaq	48(%rsi,%rax,4), %rax
	.p2align	4, 0x90
.LBB9_17:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%r14), %xmm0
	movups	-32(%r14), %xmm1
	movups	-48(%rbx), %xmm2
	movups	-32(%rbx), %xmm3
	subps	%xmm2, %xmm0
	subps	%xmm3, %xmm1
	movups	%xmm0, -48(%rax)
	movups	%xmm1, -32(%rax)
	movups	-16(%r14), %xmm0
	movups	(%r14), %xmm1
	movups	-16(%rbx), %xmm2
	movups	(%rbx), %xmm3
	subps	%xmm2, %xmm0
	subps	%xmm3, %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$64, %r14
	addq	$64, %rbx
	addq	$64, %rax
	addq	$-16, %r8
	jne	.LBB9_17
.LBB9_18:                               # %middle.block
	cmpq	%r9, %r10
	je	.LBB9_38
# BB#19:
	leaq	(%rsi,%r9,4), %rsi
	leaq	(%rcx,%r9,4), %rcx
	leaq	(%rdx,%r9,4), %rdx
	movl	%r9d, %r8d
.LBB9_3:                                # %.lr.ph36.preheader117
	movl	%edi, %eax
	subl	%r8d, %eax
	subl	%r8d, %r11d
	andl	$3, %eax
	je	.LBB9_6
# BB#4:                                 # %.lr.ph36.prol.preheader
	negl	%eax
	.p2align	4, 0x90
.LBB9_5:                                # %.lr.ph36.prol
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	subss	(%rcx), %xmm0
	movss	%xmm0, (%rsi)
	incl	%r8d
	addq	$4, %rdx
	addq	$4, %rcx
	addq	$4, %rsi
	incl	%eax
	jne	.LBB9_5
.LBB9_6:                                # %.lr.ph36.prol.loopexit
	cmpl	$3, %r11d
	jb	.LBB9_38
# BB#7:                                 # %.lr.ph36.preheader117.new
	subl	%r8d, %edi
	.p2align	4, 0x90
.LBB9_8:                                # %.lr.ph36
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	subss	(%rcx), %xmm0
	movss	%xmm0, (%rsi)
	movss	4(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	4(%rcx), %xmm0
	movss	%xmm0, 4(%rsi)
	movss	8(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	8(%rcx), %xmm0
	movss	%xmm0, 8(%rsi)
	movss	12(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	12(%rcx), %xmm0
	movss	%xmm0, 12(%rsi)
	addq	$16, %rdx
	addq	$16, %rcx
	addq	$16, %rsi
	addl	$-4, %edi
	jne	.LBB9_8
.LBB9_38:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end9:
	.size	vexopy, .Lfunc_end9-vexopy
	.cfi_endproc

	.globl	vfill
	.p2align	4, 0x90
	.type	vfill,@function
vfill:                                  # @vfill
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB10_14
# BB#1:                                 # %.lr.ph.preheader
	cvtsd2ss	%xmm0, %xmm0
	leal	-1(%rdi), %eax
	incq	%rax
	xorl	%ecx, %ecx
	cmpq	$8, %rax
	jb	.LBB10_12
# BB#2:                                 # %min.iters.checked
	movabsq	$8589934584, %r8        # imm = 0x1FFFFFFF8
	andq	%rax, %r8
	je	.LBB10_12
# BB#3:                                 # %vector.ph
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	leaq	-8(%r8), %r9
	movl	%r9d, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$7, %rdx
	je	.LBB10_4
# BB#5:                                 # %vector.body.prol.preheader
	negq	%rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_6:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm1, (%rsi,%rcx,4)
	movups	%xmm1, 16(%rsi,%rcx,4)
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB10_6
	jmp	.LBB10_7
.LBB10_4:
	xorl	%ecx, %ecx
.LBB10_7:                               # %vector.body.prol.loopexit
	cmpq	$56, %r9
	jb	.LBB10_10
# BB#8:                                 # %vector.ph.new
	movq	%r8, %rdx
	subq	%rcx, %rdx
	leaq	240(%rsi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB10_9:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm1, -240(%rcx)
	movups	%xmm1, -224(%rcx)
	movups	%xmm1, -208(%rcx)
	movups	%xmm1, -192(%rcx)
	movups	%xmm1, -176(%rcx)
	movups	%xmm1, -160(%rcx)
	movups	%xmm1, -144(%rcx)
	movups	%xmm1, -128(%rcx)
	movups	%xmm1, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	%xmm1, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	%xmm1, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	%xmm1, -16(%rcx)
	movups	%xmm1, (%rcx)
	addq	$256, %rcx              # imm = 0x100
	addq	$-64, %rdx
	jne	.LBB10_9
.LBB10_10:                              # %middle.block
	cmpq	%r8, %rax
	je	.LBB10_14
# BB#11:
	leaq	(%rsi,%r8,4), %rsi
	movl	%r8d, %ecx
.LBB10_12:                              # %.lr.ph.preheader16
	subl	%ecx, %edi
	.p2align	4, 0x90
.LBB10_13:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	%xmm0, (%rsi)
	addq	$4, %rsi
	decl	%edi
	jne	.LBB10_13
.LBB10_14:                              # %.loopexit
	retq
.Lfunc_end10:
	.size	vfill, .Lfunc_end10-vfill
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
