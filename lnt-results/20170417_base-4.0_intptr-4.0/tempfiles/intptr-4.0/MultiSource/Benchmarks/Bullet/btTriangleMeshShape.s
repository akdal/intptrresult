	.text
	.file	"btTriangleMeshShape.bc"
	.globl	_ZN19btTriangleMeshShapeC2EP23btStridingMeshInterface
	.p2align	4, 0x90
	.type	_ZN19btTriangleMeshShapeC2EP23btStridingMeshInterface,@function
_ZN19btTriangleMeshShapeC2EP23btStridingMeshInterface: # @_ZN19btTriangleMeshShapeC2EP23btStridingMeshInterface
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 48
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN14btConcaveShapeC2Ev
	movq	$_ZTV19btTriangleMeshShape+16, (%rbx)
	movq	%r14, 64(%rbx)
	movl	$21, 8(%rbx)
	movq	(%r14), %rax
.Ltmp0:
	movq	%r14, %rdi
	callq	*80(%rax)
.Ltmp1:
# BB#1:
	testb	%al, %al
	je	.LBB0_3
# BB#2:
	leaq	28(%rbx), %rsi
	leaq	44(%rbx), %rdx
	movq	(%r14), %rax
.Ltmp14:
	movq	%r14, %rdi
	callq	*96(%rax)
.Ltmp15:
	jmp	.LBB0_10
.LBB0_3:
	movl	$0, 12(%rsp)
	movq	$0, 4(%rsp)
	movl	$1065353216, (%rsp)     # imm = 0x3F800000
	movq	(%rbx), %rax
.Ltmp2:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	*104(%rax)
.Ltmp3:
# BB#4:                                 # %.noexc
	addss	24(%rbx), %xmm0
	movss	%xmm0, 44(%rbx)
	movl	$-1082130432, (%rsp)    # imm = 0xBF800000
	movq	(%rbx), %rax
.Ltmp4:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	*104(%rax)
.Ltmp5:
# BB#5:                                 # %.noexc5
	subss	24(%rbx), %xmm0
	movss	%xmm0, 28(%rbx)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$1065353216, 4(%rsp)    # imm = 0x3F800000
	movq	(%rbx), %rax
.Ltmp6:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	*104(%rax)
.Ltmp7:
# BB#6:                                 # %.noexc6
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	addss	24(%rbx), %xmm0
	movss	%xmm0, 48(%rbx)
	movl	$-1082130432, 4(%rsp)   # imm = 0xBF800000
	movq	(%rbx), %rax
.Ltmp8:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	*104(%rax)
.Ltmp9:
# BB#7:                                 # %.noexc7
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	24(%rbx), %xmm0
	movss	%xmm0, 32(%rbx)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$1065353216, 8(%rsp)    # imm = 0x3F800000
	movq	(%rbx), %rax
.Ltmp10:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	*104(%rax)
.Ltmp11:
# BB#8:                                 # %.noexc8
	addss	24(%rbx), %xmm1
	movss	%xmm1, 52(%rbx)
	movl	$-1082130432, 8(%rsp)   # imm = 0xBF800000
	movq	(%rbx), %rax
.Ltmp12:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	*104(%rax)
.Ltmp13:
# BB#9:                                 # %_ZN19btTriangleMeshShape15recalcLocalAabbEv.exit
	subss	24(%rbx), %xmm1
	movss	%xmm1, 36(%rbx)
.LBB0_10:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_11:
.Ltmp16:
	movq	%rax, %r14
.Ltmp17:
	movq	%rbx, %rdi
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp18:
# BB#12:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_13:
.Ltmp19:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN19btTriangleMeshShapeC2EP23btStridingMeshInterface, .Lfunc_end0-_ZN19btTriangleMeshShapeC2EP23btStridingMeshInterface
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp0          #   Call between .Ltmp0 and .Ltmp13
	.long	.Ltmp16-.Lfunc_begin0   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp18     #   Call between .Ltmp18 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.zero	16
	.text
	.globl	_ZN19btTriangleMeshShape15recalcLocalAabbEv
	.p2align	4, 0x90
	.type	_ZN19btTriangleMeshShape15recalcLocalAabbEv,@function
_ZN19btTriangleMeshShape15recalcLocalAabbEv: # @_ZN19btTriangleMeshShape15recalcLocalAabbEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 48
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$0, 12(%rsp)
	movq	$0, 4(%rsp)
	movl	$1065353216, (%rsp)     # imm = 0x3F800000
	movq	(%rbx), %rax
	movq	%rsp, %r14
	movq	%r14, %rsi
	callq	*104(%rax)
	addss	24(%rbx), %xmm0
	movss	%xmm0, 44(%rbx)
	movl	$-1082130432, (%rsp)    # imm = 0xBF800000
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*104(%rax)
	subss	24(%rbx), %xmm0
	movss	%xmm0, 28(%rbx)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$1065353216, 4(%rsp)    # imm = 0x3F800000
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*104(%rax)
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	addss	24(%rbx), %xmm0
	movss	%xmm0, 48(%rbx)
	movl	$-1082130432, 4(%rsp)   # imm = 0xBF800000
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*104(%rax)
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	24(%rbx), %xmm0
	movss	%xmm0, 32(%rbx)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$1065353216, 8(%rsp)    # imm = 0x3F800000
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*104(%rax)
	addss	24(%rbx), %xmm1
	movss	%xmm1, 52(%rbx)
	movl	$-1082130432, 8(%rsp)   # imm = 0xBF800000
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*104(%rax)
	subss	24(%rbx), %xmm1
	movss	%xmm1, 36(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	_ZN19btTriangleMeshShape15recalcLocalAabbEv, .Lfunc_end1-_ZN19btTriangleMeshShape15recalcLocalAabbEv
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN19btTriangleMeshShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN19btTriangleMeshShapeD2Ev,@function
_ZN19btTriangleMeshShapeD2Ev:           # @_ZN19btTriangleMeshShapeD2Ev
	.cfi_startproc
# BB#0:
	jmp	_ZN14btConcaveShapeD2Ev # TAILCALL
.Lfunc_end3:
	.size	_ZN19btTriangleMeshShapeD2Ev, .Lfunc_end3-_ZN19btTriangleMeshShapeD2Ev
	.cfi_endproc

	.globl	_ZN19btTriangleMeshShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN19btTriangleMeshShapeD0Ev,@function
_ZN19btTriangleMeshShapeD0Ev:           # @_ZN19btTriangleMeshShapeD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp20:
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp21:
# BB#1:                                 # %_ZN19btTriangleMeshShapeD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB4_2:
.Ltmp22:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN19btTriangleMeshShapeD0Ev, .Lfunc_end4-_ZN19btTriangleMeshShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin1   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end4-.Ltmp21     #   Call between .Ltmp21 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1056964608              # float 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZNK19btTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK19btTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK19btTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK19btTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 96
.Lcfi20:
	.cfi_offset %rbx, -40
.Lcfi21:
	.cfi_offset %r12, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movss	44(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	28(%rbx), %xmm1
	movss	48(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	32(%rbx), %xmm2
	movss	52(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	36(%rbx), %xmm3
	movss	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	mulss	%xmm0, %xmm2
	movss	%xmm2, 4(%rsp)          # 4-byte Spill
	mulss	%xmm0, %xmm3
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movq	(%rbx), %rax
	callq	*88(%rax)
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	32(%rsp), %xmm13        # 16-byte Reload
	addss	8(%rsp), %xmm13         # 4-byte Folded Reload
	movaps	16(%rsp), %xmm14        # 16-byte Reload
	addss	4(%rsp), %xmm14         # 4-byte Folded Reload
	addss	12(%rsp), %xmm0         # 4-byte Folded Reload
	movss	44(%rbx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	48(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	28(%rbx), %xmm11
	addss	32(%rbx), %xmm3
	movss	52(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	addss	36(%rbx), %xmm12
	movss	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm11
	mulss	%xmm1, %xmm3
	mulss	%xmm1, %xmm12
	movss	16(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	(%r12), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0],xmm6[1],xmm2[1]
	movaps	.LCPI5_1(%rip), %xmm2   # xmm2 = [nan,nan,nan,nan]
	movaps	%xmm11, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm6, %xmm7
	movaps	%xmm6, %xmm9
	andps	%xmm2, %xmm9
	movss	20(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm5    # xmm5 = xmm5[0],xmm6[0],xmm5[1],xmm6[1]
	movaps	%xmm3, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm5, %xmm6
	andps	%xmm2, %xmm5
	movss	24(%r12), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm4          # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1]
	movaps	%xmm12, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	movaps	%xmm4, %xmm8
	andps	%xmm2, %xmm8
	addps	%xmm7, %xmm6
	movss	32(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm11
	andps	%xmm2, %xmm7
	addps	%xmm6, %xmm1
	movsd	48(%r12), %xmm10        # xmm10 = mem[0],zero
	addps	%xmm1, %xmm10
	movss	36(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	andps	%xmm2, %xmm6
	addss	%xmm11, %xmm3
	movss	40(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm2
	mulss	%xmm1, %xmm12
	addss	%xmm3, %xmm12
	movaps	%xmm13, %xmm11
	movaps	%xmm11, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm9, %xmm1
	movaps	%xmm14, %xmm4
	movaps	%xmm4, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm1, %xmm3
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm8, %xmm0
	addps	%xmm3, %xmm0
	mulss	%xmm11, %xmm7
	mulss	%xmm4, %xmm6
	addss	56(%r12), %xmm12
	addss	%xmm7, %xmm6
	addss	%xmm6, %xmm2
	movaps	%xmm12, %xmm1
	subss	%xmm2, %xmm1
	xorps	%xmm3, %xmm3
	movss	%xmm1, %xmm3            # xmm3 = xmm1[0],xmm3[1,2,3]
	movaps	%xmm10, %xmm1
	subps	%xmm0, %xmm1
	movlps	%xmm1, (%r15)
	movlps	%xmm3, 8(%r15)
	addps	%xmm10, %xmm0
	addss	%xmm12, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm0, (%r14)
	movlps	%xmm1, 8(%r14)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	_ZNK19btTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end5-_ZNK19btTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.zero	16
	.text
	.globl	_ZN19btTriangleMeshShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN19btTriangleMeshShape15setLocalScalingERK9btVector3,@function
_ZN19btTriangleMeshShape15setLocalScalingERK9btVector3: # @_ZN19btTriangleMeshShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -24
.Lcfi28:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	64(%rbx), %rax
	movups	(%rsi), %xmm0
	movups	%xmm0, 8(%rax)
	movl	$0, 12(%rsp)
	movq	$0, 4(%rsp)
	movl	$1065353216, (%rsp)     # imm = 0x3F800000
	movq	(%rbx), %rax
	movq	%rsp, %r14
	movq	%r14, %rsi
	callq	*104(%rax)
	addss	24(%rbx), %xmm0
	movss	%xmm0, 44(%rbx)
	movl	$-1082130432, (%rsp)    # imm = 0xBF800000
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*104(%rax)
	subss	24(%rbx), %xmm0
	movss	%xmm0, 28(%rbx)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$1065353216, 4(%rsp)    # imm = 0x3F800000
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*104(%rax)
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	addss	24(%rbx), %xmm0
	movss	%xmm0, 48(%rbx)
	movl	$-1082130432, 4(%rsp)   # imm = 0xBF800000
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*104(%rax)
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	24(%rbx), %xmm0
	movss	%xmm0, 32(%rbx)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$1065353216, 8(%rsp)    # imm = 0x3F800000
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*104(%rax)
	addss	24(%rbx), %xmm1
	movss	%xmm1, 52(%rbx)
	movl	$-1082130432, 8(%rsp)   # imm = 0xBF800000
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*104(%rax)
	subss	24(%rbx), %xmm1
	movss	%xmm1, 36(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	_ZN19btTriangleMeshShape15setLocalScalingERK9btVector3, .Lfunc_end6-_ZN19btTriangleMeshShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.globl	_ZNK19btTriangleMeshShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK19btTriangleMeshShape15getLocalScalingEv,@function
_ZNK19btTriangleMeshShape15getLocalScalingEv: # @_ZNK19btTriangleMeshShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	movq	64(%rdi), %rax
	addq	$8, %rax
	retq
.Lfunc_end7:
	.size	_ZNK19btTriangleMeshShape15getLocalScalingEv, .Lfunc_end7-_ZNK19btTriangleMeshShape15getLocalScalingEv
	.cfi_endproc

	.globl	_ZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_,@function
_ZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_: # @_ZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 16
	subq	$48, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 64
.Lcfi31:
	.cfi_offset %rbx, -16
	movq	$_ZTVZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback+16, (%rsp)
	movq	%rsi, 8(%rsp)
	movups	(%rdx), %xmm0
	movups	%xmm0, 16(%rsp)
	movups	(%rcx), %xmm0
	movups	%xmm0, 32(%rsp)
	movq	64(%rdi), %rdi
	movq	(%rdi), %rax
.Ltmp23:
	movq	%rsp, %rsi
	callq	*16(%rax)
.Ltmp24:
# BB#1:
	movq	%rsp, %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
	addq	$48, %rsp
	popq	%rbx
	retq
.LBB8_2:
.Ltmp25:
	movq	%rax, %rbx
.Ltmp26:
	movq	%rsp, %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp27:
# BB#3:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB8_4:
.Ltmp28:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_, .Lfunc_end8-_ZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp23-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin2   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp26-.Ltmp24         #   Call between .Ltmp24 and .Ltmp26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin2   #     jumps to .Ltmp28
	.byte	1                       #   On action: 1
	.long	.Ltmp27-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp27     #   Call between .Ltmp27 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZNK19btTriangleMeshShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK19btTriangleMeshShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK19btTriangleMeshShape21calculateLocalInertiaEfR9btVector3: # @_ZNK19btTriangleMeshShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rsi)
	retq
.Lfunc_end9:
	.size	_ZNK19btTriangleMeshShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end9-_ZNK19btTriangleMeshShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.long	3713928043              # float -9.99999984E+17
	.long	3713928043              # float -9.99999984E+17
	.zero	4
	.zero	4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_1:
	.long	3713928043              # float -9.99999984E+17
	.text
	.globl	_ZNK19btTriangleMeshShape24localGetSupportingVertexERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK19btTriangleMeshShape24localGetSupportingVertexERK9btVector3,@function
_ZNK19btTriangleMeshShape24localGetSupportingVertexERK9btVector3: # @_ZNK19btTriangleMeshShape24localGetSupportingVertexERK9btVector3
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 16
	subq	$208, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 224
.Lcfi34:
	.cfi_offset %rbx, -16
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 144(%rsp)
	movl	$0, 160(%rsp)
	movq	$_ZTV21SupportVertexCallback+16, 32(%rsp)
	movups	%xmm0, 40(%rsp)
	movl	$1065353216, 56(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 60(%rsp)
	movl	$1065353216, 76(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 80(%rsp)
	movq	$1065353216, 96(%rsp)   # imm = 0x3F800000
	movups	148(%rsp), %xmm1
	movups	%xmm1, 104(%rsp)
	movl	$-581039253, 120(%rsp)  # imm = 0xDD5E0B6B
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	mulss	%xmm2, %xmm3
	xorps	%xmm4, %xmm4
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	pshufd	$229, %xmm1, %xmm5      # xmm5 = xmm1[1,1,2,3]
	movss	60(%rsp), %xmm6         # xmm6 = mem[0],zero,zero,zero
	shufps	$0, %xmm5, %xmm6        # xmm6 = xmm6[0,0],xmm5[0,0]
	shufps	$226, %xmm5, %xmm6      # xmm6 = xmm6[2,0],xmm5[2,3]
	mulps	%xmm4, %xmm6
	movss	64(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addps	%xmm6, %xmm1
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	addps	%xmm1, %xmm3
	addss	%xmm6, %xmm4
	addss	%xmm2, %xmm4
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm3, 124(%rsp)
	movlps	%xmm0, 132(%rsp)
	movabsq	$6727827449093950315, %rax # imm = 0x5D5E0B6B5D5E0B6B
	movq	%rax, 16(%rsp)
	movq	$1566444395, 24(%rsp)   # imm = 0x5D5E0B6B
	movq	(%rdi), %rax
	movq	96(%rax), %rax
	movq	.LCPI10_0(%rip), %rcx
	movq	%rcx, (%rsp)
	movss	.LCPI10_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movlps	%xmm0, 8(%rsp)
.Ltmp29:
	leaq	32(%rsp), %rsi
	movq	%rsp, %rdx
	leaq	16(%rsp), %rcx
	callq	*%rax
.Ltmp30:
# BB#1:
	movsd	40(%rsp), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	movsd	48(%rsp), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 176(%rsp)        # 16-byte Spill
	leaq	32(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
	movaps	192(%rsp), %xmm0        # 16-byte Reload
	movaps	176(%rsp), %xmm1        # 16-byte Reload
	addq	$208, %rsp
	popq	%rbx
	retq
.LBB10_2:
.Ltmp31:
	movq	%rax, %rbx
.Ltmp32:
	leaq	32(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp33:
# BB#3:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB10_4:
.Ltmp34:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end10:
	.size	_ZNK19btTriangleMeshShape24localGetSupportingVertexERK9btVector3, .Lfunc_end10-_ZNK19btTriangleMeshShape24localGetSupportingVertexERK9btVector3
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp29-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin3   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp32-.Ltmp30         #   Call between .Ltmp30 and .Ltmp32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin3   #     jumps to .Ltmp34
	.byte	1                       #   On action: 1
	.long	.Ltmp33-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end10-.Ltmp33    #   Call between .Ltmp33 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK19btTriangleMeshShape7getNameEv,"axG",@progbits,_ZNK19btTriangleMeshShape7getNameEv,comdat
	.weak	_ZNK19btTriangleMeshShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK19btTriangleMeshShape7getNameEv,@function
_ZNK19btTriangleMeshShape7getNameEv:    # @_ZNK19btTriangleMeshShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end11:
	.size	_ZNK19btTriangleMeshShape7getNameEv, .Lfunc_end11-_ZNK19btTriangleMeshShape7getNameEv
	.cfi_endproc

	.section	.text._ZN14btConcaveShape9setMarginEf,"axG",@progbits,_ZN14btConcaveShape9setMarginEf,comdat
	.weak	_ZN14btConcaveShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN14btConcaveShape9setMarginEf,@function
_ZN14btConcaveShape9setMarginEf:        # @_ZN14btConcaveShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 24(%rdi)
	retq
.Lfunc_end12:
	.size	_ZN14btConcaveShape9setMarginEf, .Lfunc_end12-_ZN14btConcaveShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK14btConcaveShape9getMarginEv,"axG",@progbits,_ZNK14btConcaveShape9getMarginEv,comdat
	.weak	_ZNK14btConcaveShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK14btConcaveShape9getMarginEv,@function
_ZNK14btConcaveShape9getMarginEv:       # @_ZNK14btConcaveShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	24(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end13:
	.size	_ZNK14btConcaveShape9getMarginEv, .Lfunc_end13-_ZNK14btConcaveShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3,"axG",@progbits,_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3,comdat
	.weak	_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	104(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end14:
	.size	_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end14-_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN16FilteredCallbackD0Ev,@function
_ZZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN16FilteredCallbackD0Ev: # @_ZZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN16FilteredCallbackD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp35:
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp36:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB15_2:
.Ltmp37:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end15:
	.size	_ZZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN16FilteredCallbackD0Ev, .Lfunc_end15-_ZZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN16FilteredCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp35-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin4   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end15-.Ltmp36    #   Call between .Ltmp36 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN16FilteredCallback28internalProcessTriangleIndexEPS2_ii,@function
_ZZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN16FilteredCallback28internalProcessTriangleIndexEPS2_ii: # @_ZZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN16FilteredCallback28internalProcessTriangleIndexEPS2_ii
	.cfi_startproc
# BB#0:
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	16(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	minss	%xmm2, %xmm3
	movss	32(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	minss	%xmm1, %xmm3
	ucomiss	32(%rdi), %xmm3
	ja	.LBB16_6
# BB#1:
	leaq	16(%rsi), %rax
	leaq	32(%rsi), %r8
	ucomiss	%xmm2, %xmm0
	cmovaq	%rsi, %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	cmovbeq	%r8, %rax
	movss	16(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%rax), %xmm0
	ja	.LBB16_6
# BB#2:
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	24(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	minss	%xmm2, %xmm3
	movss	40(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	minss	%xmm1, %xmm3
	ucomiss	40(%rdi), %xmm3
	ja	.LBB16_6
# BB#3:
	leaq	8(%rsi), %r8
	leaq	24(%rsi), %rax
	leaq	40(%rsi), %r9
	ucomiss	%xmm2, %xmm0
	cmovaq	%r8, %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	cmovbeq	%r9, %rax
	movss	24(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%rax), %xmm0
	ja	.LBB16_6
# BB#4:
	movss	4(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	minss	%xmm2, %xmm3
	movss	36(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	minss	%xmm1, %xmm3
	ucomiss	36(%rdi), %xmm3
	ja	.LBB16_6
# BB#5:                                 # %_Z24TestTriangleAgainstAabb2PK9btVector3RS0_S2_.exit
	leaq	4(%rsi), %r8
	leaq	20(%rsi), %rax
	leaq	36(%rsi), %r9
	ucomiss	%xmm2, %xmm0
	cmovaq	%r8, %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	cmovbeq	%r9, %rax
	movss	20(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%rax), %xmm0
	ja	.LBB16_6
# BB#7:
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.LBB16_6:                               # %_Z24TestTriangleAgainstAabb2PK9btVector3RS0_S2_.exit.thread
	retq
.Lfunc_end16:
	.size	_ZZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN16FilteredCallback28internalProcessTriangleIndexEPS2_ii, .Lfunc_end16-_ZZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN16FilteredCallback28internalProcessTriangleIndexEPS2_ii
	.cfi_endproc

	.section	.text._ZN21SupportVertexCallbackD0Ev,"axG",@progbits,_ZN21SupportVertexCallbackD0Ev,comdat
	.weak	_ZN21SupportVertexCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN21SupportVertexCallbackD0Ev,@function
_ZN21SupportVertexCallbackD0Ev:         # @_ZN21SupportVertexCallbackD0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -24
.Lcfi44:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp38:
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp39:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB17_2:
.Ltmp40:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end17:
	.size	_ZN21SupportVertexCallbackD0Ev, .Lfunc_end17-_ZN21SupportVertexCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp38-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin5   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end17-.Ltmp39    #   Call between .Ltmp39 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN21SupportVertexCallback15processTriangleEP9btVector3ii,"axG",@progbits,_ZN21SupportVertexCallback15processTriangleEP9btVector3ii,comdat
	.weak	_ZN21SupportVertexCallback15processTriangleEP9btVector3ii
	.p2align	4, 0x90
	.type	_ZN21SupportVertexCallback15processTriangleEP9btVector3ii,@function
_ZN21SupportVertexCallback15processTriangleEP9btVector3ii: # @_ZN21SupportVertexCallback15processTriangleEP9btVector3ii
	.cfi_startproc
# BB#0:
	leaq	8(%rdi), %rax
	movss	88(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	92(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movss	96(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm0, %xmm5
	movss	100(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	ucomiss	%xmm3, %xmm4
	jbe	.LBB18_2
# BB#1:
	movss	%xmm4, 88(%rdi)
	movups	(%rsi), %xmm3
	movups	%xmm3, (%rax)
	movaps	%xmm4, %xmm3
.LBB18_2:
	movss	16(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	movss	20(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm4, %xmm5
	movss	24(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	ucomiss	%xmm3, %xmm4
	jbe	.LBB18_4
# BB#3:
	leaq	16(%rsi), %rcx
	movss	%xmm4, 88(%rdi)
	movups	(%rcx), %xmm3
	movups	%xmm3, (%rax)
	movaps	%xmm4, %xmm3
.LBB18_4:
	mulss	32(%rsi), %xmm1
	mulss	36(%rsi), %xmm2
	addss	%xmm1, %xmm2
	mulss	40(%rsi), %xmm0
	addss	%xmm2, %xmm0
	ucomiss	%xmm3, %xmm0
	jbe	.LBB18_6
# BB#5:
	addq	$32, %rsi
	movss	%xmm0, 88(%rdi)
	movups	(%rsi), %xmm0
	movups	%xmm0, (%rax)
.LBB18_6:
	retq
.Lfunc_end18:
	.size	_ZN21SupportVertexCallback15processTriangleEP9btVector3ii, .Lfunc_end18-_ZN21SupportVertexCallback15processTriangleEP9btVector3ii
	.cfi_endproc

	.type	_ZTV19btTriangleMeshShape,@object # @_ZTV19btTriangleMeshShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV19btTriangleMeshShape
	.p2align	3
_ZTV19btTriangleMeshShape:
	.quad	0
	.quad	_ZTI19btTriangleMeshShape
	.quad	_ZN19btTriangleMeshShapeD2Ev
	.quad	_ZN19btTriangleMeshShapeD0Ev
	.quad	_ZNK19btTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN19btTriangleMeshShape15setLocalScalingERK9btVector3
	.quad	_ZNK19btTriangleMeshShape15getLocalScalingEv
	.quad	_ZNK19btTriangleMeshShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK19btTriangleMeshShape7getNameEv
	.quad	_ZN14btConcaveShape9setMarginEf
	.quad	_ZNK14btConcaveShape9getMarginEv
	.quad	_ZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.quad	_ZNK19btTriangleMeshShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.size	_ZTV19btTriangleMeshShape, 136

	.type	_ZTS19btTriangleMeshShape,@object # @_ZTS19btTriangleMeshShape
	.globl	_ZTS19btTriangleMeshShape
	.p2align	4
_ZTS19btTriangleMeshShape:
	.asciz	"19btTriangleMeshShape"
	.size	_ZTS19btTriangleMeshShape, 22

	.type	_ZTI19btTriangleMeshShape,@object # @_ZTI19btTriangleMeshShape
	.globl	_ZTI19btTriangleMeshShape
	.p2align	4
_ZTI19btTriangleMeshShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19btTriangleMeshShape
	.quad	_ZTI14btConcaveShape
	.size	_ZTI19btTriangleMeshShape, 24

	.type	_ZTVZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback,@object # @_ZTVZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback
	.p2align	3
_ZTVZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback:
	.quad	0
	.quad	_ZTIZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback
	.quad	_ZN31btInternalTriangleIndexCallbackD2Ev
	.quad	_ZZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN16FilteredCallbackD0Ev
	.quad	_ZZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN16FilteredCallback28internalProcessTriangleIndexEPS2_ii
	.size	_ZTVZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback, 40

	.type	_ZTSZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback,@object # @_ZTSZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback
	.p2align	4
_ZTSZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback:
	.asciz	"ZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback"
	.size	_ZTSZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback, 102

	.type	_ZTIZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback,@object # @_ZTIZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback
	.p2align	4
_ZTIZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback
	.quad	_ZTI31btInternalTriangleIndexCallback
	.size	_ZTIZNK19btTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E16FilteredCallback, 24

	.type	_ZTV21SupportVertexCallback,@object # @_ZTV21SupportVertexCallback
	.section	.rodata._ZTV21SupportVertexCallback,"aG",@progbits,_ZTV21SupportVertexCallback,comdat
	.weak	_ZTV21SupportVertexCallback
	.p2align	3
_ZTV21SupportVertexCallback:
	.quad	0
	.quad	_ZTI21SupportVertexCallback
	.quad	_ZN18btTriangleCallbackD2Ev
	.quad	_ZN21SupportVertexCallbackD0Ev
	.quad	_ZN21SupportVertexCallback15processTriangleEP9btVector3ii
	.size	_ZTV21SupportVertexCallback, 40

	.type	_ZTS21SupportVertexCallback,@object # @_ZTS21SupportVertexCallback
	.section	.rodata._ZTS21SupportVertexCallback,"aG",@progbits,_ZTS21SupportVertexCallback,comdat
	.weak	_ZTS21SupportVertexCallback
	.p2align	4
_ZTS21SupportVertexCallback:
	.asciz	"21SupportVertexCallback"
	.size	_ZTS21SupportVertexCallback, 24

	.type	_ZTI21SupportVertexCallback,@object # @_ZTI21SupportVertexCallback
	.section	.rodata._ZTI21SupportVertexCallback,"aG",@progbits,_ZTI21SupportVertexCallback,comdat
	.weak	_ZTI21SupportVertexCallback
	.p2align	4
_ZTI21SupportVertexCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21SupportVertexCallback
	.quad	_ZTI18btTriangleCallback
	.size	_ZTI21SupportVertexCallback, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"TRIANGLEMESH"
	.size	.L.str, 13


	.globl	_ZN19btTriangleMeshShapeC1EP23btStridingMeshInterface
	.type	_ZN19btTriangleMeshShapeC1EP23btStridingMeshInterface,@function
_ZN19btTriangleMeshShapeC1EP23btStridingMeshInterface = _ZN19btTriangleMeshShapeC2EP23btStridingMeshInterface
	.globl	_ZN19btTriangleMeshShapeD1Ev
	.type	_ZN19btTriangleMeshShapeD1Ev,@function
_ZN19btTriangleMeshShapeD1Ev = _ZN19btTriangleMeshShapeD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
