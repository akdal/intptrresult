	.text
	.file	"main.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movb	$0, compile_only(%rip)
	movb	$0, use_math(%rip)
	movb	$0, warn_not_std(%rip)
	movb	$0, std_only(%rip)
	xorl	%r14d, %r14d
	xorl	%edi, %edi
	callq	isatty
	testl	%eax, %eax
	je	.LBB0_2
# BB#1:
	movl	$1, %edi
	callq	isatty
	testl	%eax, %eax
	setne	%r14b
.LBB0_2:
	movb	%r14b, interactive(%rip)
	jmp	.LBB0_3
.LBB0_15:                               #   in Loop: Header=BB0_3 Depth=1
	movl	$.L.str.2, %edi
	callq	puts
	.p2align	4, 0x90
.LBB0_3:                                # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str, %edx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	getopt
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-99(%rax), %ecx
	cmpl	$20, %ecx
	ja	.LBB0_4
# BB#9:                                 # %.backedge
                                        #   in Loop: Header=BB0_3 Depth=1
	jmpq	*.LJTI0_0(,%rcx,8)
.LBB0_10:                               #   in Loop: Header=BB0_3 Depth=1
	movb	$1, compile_only(%rip)
	jmp	.LBB0_3
.LBB0_12:                               #   in Loop: Header=BB0_3 Depth=1
	movb	$1, interactive(%rip)
	jmp	.LBB0_3
.LBB0_11:                               #   in Loop: Header=BB0_3 Depth=1
	movb	$1, use_math(%rip)
	jmp	.LBB0_3
.LBB0_14:                               #   in Loop: Header=BB0_3 Depth=1
	movb	$1, std_only(%rip)
	jmp	.LBB0_3
.LBB0_13:                               #   in Loop: Header=BB0_3 Depth=1
	movb	$1, warn_not_std(%rip)
	jmp	.LBB0_3
.LBB0_4:                                # %.backedge
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	$-1, %eax
	jne	.LBB0_3
# BB#5:
	callq	init_storage
	callq	init_load
	cmpb	$0, interactive(%rip)
	je	.LBB0_7
# BB#6:
	movl	$2, %edi
	movl	$use_quit, %esi
	callq	signal
.LBB0_7:
	callq	init_tree
	callq	init_gen
	movq	$0, g_argv(%rip)
	movl	$0, g_argc(%rip)
	movb	$0, is_std_in(%rip)
	movb	$1, first_file(%rip)
	callq	open_new_file
	testl	%eax, %eax
	jne	.LBB0_16
# BB#8:
	movl	$1, %edi
	callq	exit
.LBB0_16:
	callq	yyparse
	cmpb	$0, compile_only(%rip)
	je	.LBB0_18
# BB#17:
	movl	$10, %edi
	callq	putchar
.LBB0_18:
	xorl	%edi, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_10
	.quad	.LBB0_3
	.quad	.LBB0_3
	.quad	.LBB0_3
	.quad	.LBB0_3
	.quad	.LBB0_3
	.quad	.LBB0_12
	.quad	.LBB0_3
	.quad	.LBB0_3
	.quad	.LBB0_11
	.quad	.LBB0_3
	.quad	.LBB0_3
	.quad	.LBB0_3
	.quad	.LBB0_3
	.quad	.LBB0_3
	.quad	.LBB0_3
	.quad	.LBB0_14
	.quad	.LBB0_3
	.quad	.LBB0_3
	.quad	.LBB0_15
	.quad	.LBB0_13

	.text
	.globl	use_quit
	.p2align	4, 0x90
	.type	use_quit,@function
use_quit:                               # @use_quit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 16
	movl	$.Lstr, %edi
	callq	puts
	movl	$2, %edi
	movl	$use_quit, %esi
	popq	%rax
	jmp	signal                  # TAILCALL
.Lfunc_end1:
	.size	use_quit, .Lfunc_end1-use_quit
	.cfi_endproc

	.globl	open_new_file
	.p2align	4, 0x90
	.type	open_new_file,@function
open_new_file:                          # @open_new_file
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
.Lcfi8:
	.cfi_offset %rbx, -16
	movl	$1, line_no(%rip)
	xorl	%eax, %eax
	cmpb	$0, is_std_in(%rip)
	jne	.LBB2_14
# BB#1:
	cmpb	$0, use_math(%rip)
	je	.LBB2_4
# BB#2:
	movb	first_file(%rip), %al
	testb	%al, %al
	je	.LBB2_4
# BB#3:
	movl	$.L.str.4, %edi
	movl	$2, %esi
	callq	lookup
	movl	$.L.str.5, %edi
	movl	$2, %esi
	callq	lookup
	movl	$.L.str.6, %edi
	movl	$2, %esi
	callq	lookup
	movl	$.L.str.7, %edi
	movl	$2, %esi
	callq	lookup
	movl	$.L.str.8, %edi
	movl	$2, %esi
	callq	lookup
	movl	$.L.str.9, %edi
	movl	$2, %esi
	callq	lookup
	movl	$libmath, %edi
	callq	load_code
.LBB2_4:
	movslq	optind(%rip), %rax
	cmpl	g_argc(%rip), %eax
	jge	.LBB2_10
# BB#5:
	movq	g_argv(%rip), %rcx
	movq	(%rcx,%rax,8), %rdi
	movl	$.L.str.10, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_9
# BB#6:
	cmpb	$0, first_file(%rip)
	jne	.LBB2_8
# BB#7:
	movq	yyin(%rip), %rdi
	callq	fclose
.LBB2_8:                                # %new_yy_file.exit
	movq	%rbx, yyin(%rip)
	movb	$0, first_file(%rip)
	incl	optind(%rip)
	jmp	.LBB2_13
.LBB2_10:
	movq	stdin(%rip), %rbx
	cmpb	$0, first_file(%rip)
	jne	.LBB2_12
# BB#11:
	movq	yyin(%rip), %rdi
	callq	fclose
.LBB2_12:                               # %new_yy_file.exit4
	movq	%rbx, yyin(%rip)
	movb	$0, first_file(%rip)
	movb	$1, is_std_in(%rip)
.LBB2_13:
	movl	$1, %eax
.LBB2_14:
	popq	%rbx
	retq
.LBB2_9:
	movq	stderr(%rip), %rdi
	movq	g_argv(%rip), %rax
	movslq	optind(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, optind(%rip)
	movq	(%rax,%rcx,8), %rdx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	open_new_file, .Lfunc_end2-open_new_file
	.cfi_endproc

	.globl	new_yy_file
	.p2align	4, 0x90
	.type	new_yy_file,@function
new_yy_file:                            # @new_yy_file
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpb	$0, first_file(%rip)
	jne	.LBB3_2
# BB#1:
	movq	yyin(%rip), %rdi
	callq	fclose
.LBB3_2:
	movq	%rbx, yyin(%rip)
	movb	$0, first_file(%rip)
	popq	%rbx
	retq
.Lfunc_end3:
	.size	new_yy_file, .Lfunc_end3-new_yy_file
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"lcisvw"
	.size	.L.str, 7

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"bc 1.02 (Mar 3, 92) Copyright (C) 1991, 1992 Free Software Foundation, Inc."
	.size	.L.str.2, 76

	.type	first_file,@object      # @first_file
	.comm	first_file,1,1
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"e"
	.size	.L.str.4, 2

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"l"
	.size	.L.str.5, 2

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"s"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"a"
	.size	.L.str.7, 2

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"c"
	.size	.L.str.8, 2

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"j"
	.size	.L.str.9, 2

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"r"
	.size	.L.str.10, 2

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"File %s is unavailable.\n"
	.size	.L.str.11, 25

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"\n(interrupt) use quit to exit."
	.size	.Lstr, 31


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
