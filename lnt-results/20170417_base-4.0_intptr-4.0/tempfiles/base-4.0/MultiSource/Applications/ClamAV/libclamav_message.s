	.text
	.file	"libclamav_message.bc"
	.globl	messageCreate
	.p2align	4, 0x90
	.type	messageCreate,@function
messageCreate:                          # @messageCreate
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$1, %edi
	movl	$136, %esi
	callq	cli_calloc
	testq	%rax, %rax
	je	.LBB0_2
# BB#1:
	movl	$0, (%rax)
.LBB0_2:
	popq	%rcx
	retq
.Lfunc_end0:
	.size	messageCreate, .Lfunc_end0-messageCreate
	.cfi_endproc

	.globl	messageDestroy
	.p2align	4, 0x90
	.type	messageDestroy,@function
messageDestroy:                         # @messageDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 16
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	messageReset
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	messageDestroy, .Lfunc_end1-messageDestroy
	.cfi_endproc

	.globl	messageReset
	.p2align	4, 0x90
	.type	messageReset,@function
messageReset:                           # @messageReset
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -24
.Lcfi7:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	callq	free
.LBB2_2:
	movq	48(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#3:
	callq	free
.LBB2_4:
	movq	40(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_9
# BB#5:                                 # %.preheader
	cmpl	$0, 32(%r14)
	jle	.LBB2_8
# BB#6:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rdi
	callq	free
	incq	%rbx
	movslq	32(%r14), %rax
	movq	40(%r14), %rdi
	cmpq	%rax, %rbx
	jl	.LBB2_7
.LBB2_8:                                # %._crit_edge
	callq	free
.LBB2_9:
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_11
# BB#10:
	callq	textDestroy
.LBB2_11:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_13
# BB#12:
	callq	free
.LBB2_13:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 112(%r14)
	movups	%xmm0, 96(%r14)
	movups	%xmm0, 80(%r14)
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 48(%r14)
	movups	%xmm0, 32(%r14)
	movups	%xmm0, 16(%r14)
	movups	%xmm0, (%r14)
	movq	$0, 128(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	messageReset, .Lfunc_end2-messageReset
	.cfi_endproc

	.globl	messageSetMimeType
	.p2align	4, 0x90
	.type	messageSetMimeType,@function
messageSetMimeType:                     # @messageSetMimeType
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 96
.Lcfi15:
	.cfi_offset %rbx, -56
.Lcfi16:
	.cfi_offset %r12, -48
.Lcfi17:
	.cfi_offset %r13, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB3_1
# BB#2:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	callq	__ctype_b_loc
	movq	(%rax), %rax
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movsbq	(%rbx), %rcx
	testb	$4, 1(%rax,%rcx,2)
	jne	.LBB3_6
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB3_3
# BB#5:
	xorl	%ebp, %ebp
	jmp	.LBB3_22
.LBB3_1:
	xorl	%ebp, %ebp
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB3_22
.LBB3_6:
	movq	messageSetMimeType.mime_table(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_7
.LBB3_11:                               # %.loopexit
	movq	%rbx, %rsi
	callq	tableFind
	cmpl	$-1, %eax
	je	.LBB3_13
# BB#12:
	movl	%eax, (%r14)
.LBB3_21:                               # %.loopexit56
	movl	$1, %ebp
	jmp	.LBB3_22
.LBB3_7:
	callq	tableCreate
	movq	%rax, messageSetMimeType.mime_table(%rip)
	xorl	%ebp, %ebp
	testq	%rax, %rax
	je	.LBB3_22
# BB#8:                                 # %.preheader55.preheader
	movl	$.L.str.51, %esi
	movl	$6, %edx
	movq	%rax, %rdi
	callq	tableInsert
	testl	%eax, %eax
	je	.LBB3_10
# BB#9:                                 # %.preheader55.170
	movq	messageSetMimeType.mime_table(%rip), %rdi
	movl	$.L.str.52, %esi
	movl	$5, %edx
	callq	tableInsert
	testl	%eax, %eax
	je	.LBB3_10
# BB#23:                                # %.preheader55.271
	movq	messageSetMimeType.mime_table(%rip), %rdi
	movl	$.L.str.13, %esi
	movl	$1, %edx
	callq	tableInsert
	testl	%eax, %eax
	je	.LBB3_10
# BB#24:                                # %.preheader55.372
	movq	messageSetMimeType.mime_table(%rip), %rdi
	movl	$.L.str.53, %esi
	movl	$2, %edx
	callq	tableInsert
	testl	%eax, %eax
	je	.LBB3_10
# BB#25:                                # %.preheader55.473
	movq	messageSetMimeType.mime_table(%rip), %rdi
	movl	$.L.str.54, %esi
	movl	$3, %edx
	callq	tableInsert
	testl	%eax, %eax
	je	.LBB3_10
# BB#26:                                # %.preheader55.574
	movq	messageSetMimeType.mime_table(%rip), %rdi
	movl	$.L.str.55, %esi
	movl	$4, %edx
	callq	tableInsert
	testl	%eax, %eax
	je	.LBB3_10
# BB#27:                                # %.preheader55.675
	movq	messageSetMimeType.mime_table(%rip), %rdi
	movl	$.L.str.56, %esi
	movl	$7, %edx
	callq	tableInsert
	testl	%eax, %eax
	je	.LBB3_10
# BB#28:                                # %.loopexit.loopexit76
	movq	messageSetMimeType.mime_table(%rip), %rdi
	jmp	.LBB3_11
.LBB3_13:
	xorl	%ebp, %ebp
	cmpl	$0, (%r14)
	jne	.LBB3_22
# BB#14:
	movl	$.L.str.2, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB3_15
# BB#16:
	movl	$.L.str.3, %esi
	movq	%rbx, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB3_29
# BB#17:                                # %.preheader.preheader
	movl	$.L.str.51, %edi
	movq	%rbx, %rsi
	callq	simil
	xorl	%r12d, %r12d
	testl	%eax, %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	cmovnsl	%eax, %r12d
	movl	$.L.str.52, %edi
	movq	%rbx, %rsi
	callq	simil
	cmpl	%r12d, %eax
	movl	%r12d, 36(%rsp)         # 4-byte Spill
	movl	%eax, 24(%rsp)          # 4-byte Spill
	cmovgel	%eax, %r12d
	movl	$.L.str.13, %edi
	movq	%rbx, %rsi
	callq	simil
	cmpl	%r12d, %eax
	movl	%r12d, 32(%rsp)         # 4-byte Spill
	movl	%eax, 20(%rsp)          # 4-byte Spill
	cmovgel	%eax, %r12d
	movl	$.L.str.53, %edi
	movq	%rbx, %rsi
	callq	simil
	cmpl	%r12d, %eax
	movl	%r12d, %r15d
	movl	%eax, 16(%rsp)          # 4-byte Spill
	cmovgel	%eax, %r15d
	movl	$.L.str.54, %edi
	movq	%rbx, %rsi
	callq	simil
	cmpl	%r15d, %eax
	movl	%r15d, %ebp
	movl	%eax, 12(%rsp)          # 4-byte Spill
	cmovgel	%eax, %ebp
	movl	$.L.str.55, %edi
	movq	%rbx, %rsi
	callq	simil
	cmpl	%ebp, %eax
	movl	%ebp, %r13d
	movl	%eax, 8(%rsp)           # 4-byte Spill
	cmovgel	%eax, %r13d
	movl	$.L.str.56, %edi
	movq	%rbx, %rsi
	callq	simil
	cmpl	%r13d, %eax
	movl	%r13d, %ecx
	cmovgel	%eax, %ecx
	cmpl	$49, %ecx
	jle	.LBB3_19
# BB#18:
	xorl	%edx, %edx
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	movl	$.L.str.51, %esi
	cmovleq	%rdx, %rsi
	movl	$6, %edx
	movl	$-1, %edi
	cmovgl	%edx, %edi
	movl	24(%rsp), %edx          # 4-byte Reload
	cmpl	36(%rsp), %edx          # 4-byte Folded Reload
	movl	$.L.str.52, %edx
	cmovleq	%rsi, %rdx
	movl	$5, %esi
	cmovlel	%edi, %esi
	movl	20(%rsp), %edi          # 4-byte Reload
	cmpl	32(%rsp), %edi          # 4-byte Folded Reload
	movl	$.L.str.13, %edi
	cmovleq	%rdx, %rdi
	movl	$1, %edx
	cmovlel	%esi, %edx
	cmpl	%r12d, 16(%rsp)         # 4-byte Folded Reload
	movl	$.L.str.53, %esi
	cmovleq	%rdi, %rsi
	movl	$2, %edi
	cmovlel	%edx, %edi
	cmpl	%r15d, 12(%rsp)         # 4-byte Folded Reload
	movl	$.L.str.54, %edx
	cmovleq	%rsi, %rdx
	movl	$3, %esi
	cmovlel	%edi, %esi
	cmpl	%ebp, 8(%rsp)           # 4-byte Folded Reload
	movl	$.L.str.55, %ebp
	cmovleq	%rdx, %rbp
	movl	$4, %edi
	cmovlel	%esi, %edi
	cmpl	%r13d, %eax
	movl	$.L.str.56, %edx
	cmovleq	%rbp, %rdx
	movl	$7, %ebp
	cmovlel	%edi, %ebp
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	jmp	.LBB3_20
.LBB3_10:
	movq	messageSetMimeType.mime_table(%rip), %rdi
	callq	tableDestroy
	movq	$0, messageSetMimeType.mime_table(%rip)
.LBB3_22:                               # %.loopexit56
	movl	%ebp, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_15:
	movl	$8, %ebp
	jmp	.LBB3_20
.LBB3_29:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$6, %ebp
	jmp	.LBB3_20
.LBB3_19:
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movl	$1, %ebp
.LBB3_20:
	movl	%ebp, (%r14)
	jmp	.LBB3_21
.Lfunc_end3:
	.size	messageSetMimeType, .Lfunc_end3-messageSetMimeType
	.cfi_endproc

	.p2align	4, 0x90
	.type	simil,@function
simil:                                  # @simil
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 272
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_6
# BB#1:
	movq	%rbp, %rdi
	callq	cli_strdup
	movq	%rax, %r15
	movl	$-2, %ebp
	testq	%r15, %r15
	je	.LBB4_63
# BB#2:
	movq	%rbx, %rdi
	callq	cli_strdup
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB4_7
# BB#3:
	movq	%r15, %rdi
	callq	strstrip
	movq	%rax, %rbx
	cmpq	$49, %rbx
	ja	.LBB4_5
# BB#4:
	movq	%r14, %rdi
	callq	strstrip
	cmpq	$50, %rax
	jb	.LBB4_8
.LBB4_5:
	movq	%r15, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movl	$-5, %ebp
	jmp	.LBB4_63
.LBB4_6:
	movl	$100, %ebp
	jmp	.LBB4_63
.LBB4_7:
	movq	%r15, %rdi
	jmp	.LBB4_62
.LBB4_8:
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB4_61
# BB#9:
	movq	%r15, %rdi
	callq	cli_strdup
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.LBB4_61
# BB#10:
	movq	$0, 8(%r12)
	movl	$16, %edi
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB4_61
# BB#11:
	movq	%rax, %r13
	movq	%r14, %rdi
	callq	cli_strdup
	movq	%rax, (%r13)
	testq	%rax, %rax
	je	.LBB4_61
# BB#12:                                # %compare.exit.thread.outer.outer.sink.split.preheader
	addq	%rbx, 64(%rsp)          # 8-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
.LBB4_13:                               # %compare.exit.thread.outer.outer.sink.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_14 Depth 2
                                        #       Child Loop BB4_20 Depth 3
                                        #         Child Loop BB4_26 Depth 4
                                        #           Child Loop BB4_29 Depth 5
                                        #             Child Loop BB4_30 Depth 6
                                        #               Child Loop BB4_31 Depth 7
                                        #               Child Loop BB4_34 Depth 7
	movq	%r12, 8(%r13)
.LBB4_14:                               # %compare.exit.thread.outer.outer
                                        #   Parent Loop BB4_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_20 Depth 3
                                        #         Child Loop BB4_26 Depth 4
                                        #           Child Loop BB4_29 Depth 5
                                        #             Child Loop BB4_30 Depth 6
                                        #               Child Loop BB4_31 Depth 7
                                        #               Child Loop BB4_34 Depth 7
	movl	%ebx, %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	leaq	160(%rsp), %r12
	leaq	96(%rsp), %rbp
	testq	%r13, %r13
	jne	.LBB4_20
	jmp	.LBB4_64
.LBB4_18:                               # %compare.exit.thread.outer
                                        #   in Loop: Header=BB4_20 Depth=3
	leaq	96(%rsp), %rbp
	testq	%r13, %r13
	jne	.LBB4_20
	jmp	.LBB4_64
.LBB4_15:                               # %._crit_edge.i
                                        #   in Loop: Header=BB4_20 Depth=3
	testb	$1, 56(%rsp)            # 1-byte Folded Reload
	je	.LBB4_17
# BB#16:                                #   in Loop: Header=BB4_20 Depth=3
	movb	$0, (%r14)
	movb	$0, (%r13)
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB4_17:                               # %compare.exit
                                        #   in Loop: Header=BB4_20 Depth=3
	testl	%ebx, %ebx
	movl	$-2, %ebp
	movq	80(%rsp), %r14          # 8-byte Reload
	movq	72(%rsp), %r15          # 8-byte Reload
	leaq	160(%rsp), %r12
	movq	8(%rsp), %r13           # 8-byte Reload
	je	.LBB4_18
	jmp	.LBB4_44
	.p2align	4, 0x90
.LBB4_19:                               # %compare.exit.thread
                                        #   in Loop: Header=BB4_20 Depth=3
	testq	%r13, %r13
	je	.LBB4_64
.LBB4_20:                               #   Parent Loop BB4_13 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_26 Depth 4
                                        #           Child Loop BB4_29 Depth 5
                                        #             Child Loop BB4_30 Depth 6
                                        #               Child Loop BB4_31 Depth 7
                                        #               Child Loop BB4_34 Depth 7
	movq	(%r13), %rsi
	movq	%rbp, %rdi
	callq	strcpy
	movq	(%r13), %rdi
	movq	8(%r13), %rbx
	callq	free
	movq	%r13, %rdi
	callq	free
	testq	%rbx, %rbx
	je	.LBB4_22
# BB#21:                                #   in Loop: Header=BB4_20 Depth=3
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	callq	strcpy
	movq	(%rbx), %rdi
	movq	8(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	callq	free
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB4_23
.LBB4_22:                               #   in Loop: Header=BB4_20 Depth=3
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB4_23:                               # %pop.exit59
                                        #   in Loop: Header=BB4_20 Depth=3
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	movq	8(%rsp), %r13           # 8-byte Reload
	jle	.LBB4_19
# BB#24:                                # %.preheader116.preheader.i
                                        #   in Loop: Header=BB4_20 Depth=3
	movq	%r15, 72(%rsp)          # 8-byte Spill
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	callq	strlen
	movq	%r12, %rdx
	leaq	96(%rsp,%rax), %r12
	leaq	160(%rsp,%rbx), %rsi
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%rdx, %r15
	movq	%rsi, %rdx
	jmp	.LBB4_26
.LBB4_25:                               #   in Loop: Header=BB4_26 Depth=4
	leaq	96(%rsp), %rbp
	jmp	.LBB4_43
	.p2align	4, 0x90
.LBB4_26:                               # %.preheader116.i
                                        #   Parent Loop BB4_13 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_20 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB4_29 Depth 5
                                        #             Child Loop BB4_30 Depth 6
                                        #               Child Loop BB4_31 Depth 7
                                        #               Child Loop BB4_34 Depth 7
	cmpq	%rbp, %r12
	jbe	.LBB4_43
# BB#27:                                # %.preheader116.i
                                        #   in Loop: Header=BB4_26 Depth=4
	cmpq	%rdx, %r15
	jae	.LBB4_43
# BB#28:                                # %.lr.ph.lr.ph.lr.ph.i
                                        #   in Loop: Header=BB4_26 Depth=4
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r15, 88(%rsp)          # 8-byte Spill
	movq	%rbp, %r15
	movq	%rdx, %rbp
	callq	__ctype_tolower_loc
	movq	%rbp, %rdx
	movq	%r15, %rbp
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	(%rax), %rsi
	movl	%ebx, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	%r15, %rcx
	movq	%rbp, %rax
	movq	%r14, %r9
	movq	%r13, %r10
.LBB4_29:                               # %.lr.ph.lr.ph.i
                                        #   Parent Loop BB4_13 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_20 Depth=3
                                        #         Parent Loop BB4_26 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB4_30 Depth 6
                                        #               Child Loop BB4_31 Depth 7
                                        #               Child Loop BB4_34 Depth 7
	movq	%r12, %r11
	negq	%r11
	movq	%rdx, %r8
	negq	%rdx
	movq	%rcx, %r14
.LBB4_30:                               # %.lr.ph.split.us.preheader.i
                                        #   Parent Loop BB4_13 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_20 Depth=3
                                        #         Parent Loop BB4_26 Depth=4
                                        #           Parent Loop BB4_29 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB4_31 Depth 7
                                        #               Child Loop BB4_34 Depth 7
	movsbq	(%r14), %rcx
	movl	(%rsi,%rcx,4), %ecx
	movq	%rax, %r13
	.p2align	4, 0x90
.LBB4_31:                               # %.lr.ph.split.us.i
                                        #   Parent Loop BB4_13 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_20 Depth=3
                                        #         Parent Loop BB4_26 Depth=4
                                        #           Parent Loop BB4_29 Depth=5
                                        #             Parent Loop BB4_30 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movsbq	(%r13), %rax
	cmpl	(%rsi,%rax,4), %ecx
	je	.LBB4_33
# BB#32:                                #   in Loop: Header=BB4_31 Depth=7
	incq	%r13
	cmpq	%r12, %r13
	jb	.LBB4_31
	jmp	.LBB4_42
.LBB4_33:                               # %.preheader.i.preheader
                                        #   in Loop: Header=BB4_30 Depth=6
	leaq	(%r13,%r11), %rbp
	leaq	(%r14,%rdx), %rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_34:                               # %.preheader.i
                                        #   Parent Loop BB4_13 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        #       Parent Loop BB4_20 Depth=3
                                        #         Parent Loop BB4_26 Depth=4
                                        #           Parent Loop BB4_29 Depth=5
                                        #             Parent Loop BB4_30 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movq	%rbp, %rax
	addq	%rbx, %rax
	je	.LBB4_37
# BB#35:                                # %.preheader.i
                                        #   in Loop: Header=BB4_34 Depth=7
	movq	%rcx, %rax
	addq	%rbx, %rax
	je	.LBB4_37
# BB#36:                                #   in Loop: Header=BB4_34 Depth=7
	movsbq	1(%r14,%rbx), %rax
	movl	(%rsi,%rax,4), %eax
	movsbq	1(%r13,%rbx), %rdi
	incq	%rbx
	cmpl	(%rsi,%rdi,4), %eax
	je	.LBB4_34
.LBB4_37:                               # %.preheader.i._crit_edge
                                        #   in Loop: Header=BB4_30 Depth=6
	leaq	(%r13,%rbx), %rax
	leaq	(%r14,%rbx), %rcx
	movl	%ebx, %ebp
	subl	4(%rsp), %ebp           # 4-byte Folded Reload
	ja	.LBB4_40
# BB#38:                                # %.outer117.i
                                        #   in Loop: Header=BB4_30 Depth=6
	movb	$1, %dil
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	cmpq	%r12, %rax
	leaq	96(%rsp), %rbp
	jae	.LBB4_42
# BB#39:                                # %.outer117.i
                                        #   in Loop: Header=BB4_30 Depth=6
	movl	%ebx, %edi
	subq	%rdi, %rcx
	cmpq	%r8, %rcx
	movq	%rcx, %r14
	jb	.LBB4_30
	jmp	.LBB4_42
.LBB4_40:                               # %.outer.i
                                        #   in Loop: Header=BB4_29 Depth=5
	movq	%r8, %rdx
	subq	%rbp, %rdx
	subq	%rbp, %r12
	movb	$1, %dil
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	cmpq	%r12, %rax
	jae	.LBB4_25
# BB#41:                                # %.outer.i
                                        #   in Loop: Header=BB4_29 Depth=5
	cmpq	%rdx, %rcx
	movl	%ebx, %edi
	movl	%edi, 4(%rsp)           # 4-byte Spill
	movq	%r14, %r9
	movq	%r13, %r10
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	96(%rsp), %rbp
	jb	.LBB4_29
	jmp	.LBB4_43
.LBB4_42:                               #   in Loop: Header=BB4_26 Depth=4
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%r10, %r13
	movq	%r9, %r14
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	%edx, %ebx
	movq	%r8, %rdx
.LBB4_43:                               # %.critedge.i
                                        #   in Loop: Header=BB4_26 Depth=4
	incq	%r15
	cmpq	%rdx, %r15
	jb	.LBB4_26
	jmp	.LBB4_15
.LBB4_44:                               #   in Loop: Header=BB4_14 Depth=2
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %r13
	leaq	96(%rsp), %rdi
	callq	strlen
	cmpq	$2, %r13
	jb	.LBB4_46
# BB#45:                                #   in Loop: Header=BB4_14 Depth=2
	testq	%rax, %rax
	jne	.LBB4_48
.LBB4_46:                               #   in Loop: Header=BB4_14 Depth=2
	testq	%r13, %r13
	je	.LBB4_53
# BB#47:                                #   in Loop: Header=BB4_14 Depth=2
	cmpq	$2, %rax
	jb	.LBB4_53
.LBB4_48:                               #   in Loop: Header=BB4_14 Depth=2
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB4_61
# BB#49:                                #   in Loop: Header=BB4_14 Depth=2
	movq	%r12, %rdi
	callq	cli_strdup
	movq	%rax, (%r13)
	testq	%rax, %rax
	je	.LBB4_61
# BB#50:                                #   in Loop: Header=BB4_14 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 8(%r13)
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	testq	%rax, %rax
	je	.LBB4_61
# BB#51:                                #   in Loop: Header=BB4_14 Depth=2
	leaq	96(%rsp), %rdi
	callq	cli_strdup
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, (%rcx)
	testq	%rax, %rax
	je	.LBB4_61
# BB#52:                                # %push.exit64
                                        #   in Loop: Header=BB4_14 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%r13, 8(%rax)
.LBB4_53:                               #   in Loop: Header=BB4_14 Depth=2
	addl	20(%rsp), %ebx          # 4-byte Folded Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	strlen
	movq	%rax, %r12
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	strlen
	cmpq	$2, %r12
	movq	8(%rsp), %r13           # 8-byte Reload
	jb	.LBB4_55
# BB#54:                                #   in Loop: Header=BB4_14 Depth=2
	testq	%rax, %rax
	jne	.LBB4_57
.LBB4_55:                               #   in Loop: Header=BB4_14 Depth=2
	testq	%r12, %r12
	je	.LBB4_14
# BB#56:                                #   in Loop: Header=BB4_14 Depth=2
	cmpq	$1, %rax
	jbe	.LBB4_14
.LBB4_57:                               #   in Loop: Header=BB4_13 Depth=1
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB4_61
# BB#58:                                #   in Loop: Header=BB4_13 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	cli_strdup
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.LBB4_61
# BB#59:                                #   in Loop: Header=BB4_13 Depth=1
	movq	%rcx, 8(%r12)
	movl	$16, %edi
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB4_61
# BB#60:                                #   in Loop: Header=BB4_13 Depth=1
	movq	%rax, %r13
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	cli_strdup
	movq	%rax, (%r13)
	testq	%rax, %rax
	jne	.LBB4_13
.LBB4_61:                               # %push.exit.thread
	movq	%r15, %rdi
	callq	free
	movq	%r14, %rdi
.LBB4_62:
	callq	free
.LBB4_63:
	movl	%ebp, %eax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_64:                               # %pop.exit
	movq	%r15, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	64(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB4_66
# BB#65:
	imull	$200, 20(%rsp), %eax    # 4-byte Folded Reload
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rbp
	jmp	.LBB4_63
.LBB4_66:
	xorl	%ebp, %ebp
	jmp	.LBB4_63
.Lfunc_end4:
	.size	simil, .Lfunc_end4-simil
	.cfi_endproc

	.globl	messageGetMimeType
	.p2align	4, 0x90
	.type	messageGetMimeType,@function
messageGetMimeType:                     # @messageGetMimeType
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	retq
.Lfunc_end5:
	.size	messageGetMimeType, .Lfunc_end5-messageGetMimeType
	.cfi_endproc

	.globl	messageSetMimeSubtype
	.p2align	4, 0x90
	.type	messageSetMimeSubtype,@function
messageSetMimeSubtype:                  # @messageSetMimeSubtype
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 32
.Lcfi37:
	.cfi_offset %rbx, -24
.Lcfi38:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	jne	.LBB6_2
# BB#1:
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.8, %ebx
.LBB6_2:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB6_4
# BB#3:
	callq	free
.LBB6_4:
	movq	%rbx, %rdi
	callq	cli_strdup
	movq	%rax, 24(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	messageSetMimeSubtype, .Lfunc_end6-messageSetMimeSubtype
	.cfi_endproc

	.globl	messageGetMimeSubtype
	.p2align	4, 0x90
	.type	messageGetMimeSubtype,@function
messageGetMimeSubtype:                  # @messageGetMimeSubtype
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rcx
	testq	%rcx, %rcx
	movl	$.L.str.8, %eax
	cmovneq	%rcx, %rax
	retq
.Lfunc_end7:
	.size	messageGetMimeSubtype, .Lfunc_end7-messageGetMimeSubtype
	.cfi_endproc

	.globl	messageSetDispositionType
	.p2align	4, 0x90
	.type	messageSetDispositionType,@function
messageSetDispositionType:              # @messageSetDispositionType
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 32
.Lcfi42:
	.cfi_offset %rbx, -32
.Lcfi43:
	.cfi_offset %r14, -24
.Lcfi44:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	48(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB8_2
# BB#1:
	callq	free
.LBB8_2:
	testq	%r15, %r15
	je	.LBB8_7
# BB#3:                                 # %.preheader
	movb	(%r15), %bl
	testb	%bl, %bl
	je	.LBB8_7
# BB#4:                                 # %.lr.ph
	callq	__ctype_b_loc
	movq	(%rax), %rax
	.p2align	4, 0x90
.LBB8_5:                                # =>This Inner Loop Header: Depth=1
	movsbq	%bl, %rcx
	testb	$32, 1(%rax,%rcx,2)
	je	.LBB8_9
# BB#6:                                 #   in Loop: Header=BB8_5 Depth=1
	movzbl	1(%r15), %ebx
	incq	%r15
	testb	%bl, %bl
	jne	.LBB8_5
.LBB8_7:                                # %.critedge.thread
	movq	$0, 48(%r14)
.LBB8_8:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB8_9:                                # %.critedge
	movq	%r15, %rdi
	callq	cli_strdup
	movq	%rax, 48(%r14)
	testq	%rax, %rax
	je	.LBB8_8
# BB#10:
	movq	%rax, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	strstrip                # TAILCALL
.Lfunc_end8:
	.size	messageSetDispositionType, .Lfunc_end8-messageSetDispositionType
	.cfi_endproc

	.globl	messageGetDispositionType
	.p2align	4, 0x90
	.type	messageGetDispositionType,@function
messageGetDispositionType:              # @messageGetDispositionType
	.cfi_startproc
# BB#0:
	movq	48(%rdi), %rcx
	testq	%rcx, %rcx
	movl	$.L.str.8, %eax
	cmovneq	%rcx, %rax
	retq
.Lfunc_end9:
	.size	messageGetDispositionType, .Lfunc_end9-messageGetDispositionType
	.cfi_endproc

	.globl	messageAddArgument
	.p2align	4, 0x90
	.type	messageAddArgument,@function
messageAddArgument:                     # @messageAddArgument
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 80
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB10_32
# BB#1:                                 # %.preheader35
	callq	__ctype_b_loc
	movq	%rax, %r13
	movq	(%r13), %rax
	leaq	-1(%rbx), %rbp
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movsbq	1(%rbp), %rcx
	incq	%rbp
	incq	%rbx
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB10_2
# BB#3:
	testb	%cl, %cl
	je	.LBB10_32
# BB#4:
	xorl	%r12d, %r12d
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	usefulArg
	testl	%eax, %eax
	je	.LBB10_32
# BB#5:                                 # %.preheader
	movslq	32(%r14), %r15
	testq	%r15, %r15
	jle	.LBB10_10
# BB#6:                                 # %.lr.ph
	movq	40(%r14), %rbx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB10_7:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%r12,8), %rsi
	testq	%rsi, %rsi
	je	.LBB10_10
# BB#8:                                 #   in Loop: Header=BB10_7 Depth=1
	movq	%rbp, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB10_32
# BB#9:                                 #   in Loop: Header=BB10_7 Depth=1
	incq	%r12
	cmpq	%r15, %r12
	jl	.LBB10_7
.LBB10_10:                              # %._crit_edge
	cmpl	%r15d, %r12d
	jne	.LBB10_13
# BB#11:
	leaq	1(%r15), %rax
	movl	%eax, 32(%r14)
	movq	40(%r14), %rdi
	leaq	8(,%r15,8), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB10_24
# BB#12:
	movq	%rax, 40(%r14)
.LBB10_13:
	movl	$.L.str.108, %esi
	movq	%rbp, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB10_22
# BB#14:
	movl	$.L.str.109, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB10_15:
	movq	%rbp, %rdi
.LBB10_16:                              # %rfc2231.exit
	callq	cli_strdup
	movq	%rax, %rbx
.LBB10_17:                              # %rfc2231.exit
	movq	40(%r14), %rax
	movslq	%r12d, %rcx
	movq	%rbx, (%rax,%rcx,8)
	testq	%rbx, %rbx
	je	.LBB10_32
# BB#18:
	movl	$.L.str.10, %esi
	movl	$9, %edx
	movq	%rbx, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB10_20
# BB#19:
	movl	$.L.str.11, %esi
	movl	$5, %edx
	movq	%rbx, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	jne	.LBB10_32
.LBB10_20:
	cmpl	$0, (%r14)
	jne	.LBB10_32
# BB#21:
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.13, %esi
	movq	%r14, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	messageSetMimeType      # TAILCALL
.LBB10_22:
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movl	$.L.str.110, %esi
	movq	%rbp, %rdi
	callq	strstr
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB10_25
# BB#23:
	movl	$2, %r15d
	jmp	.LBB10_27
.LBB10_24:                              # %.critedge
	decl	32(%r14)
	jmp	.LBB10_32
.LBB10_25:
	movl	$.L.str.111, %esi
	movq	%rbp, %rdi
	callq	strstr
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB10_15
# BB#26:
	xorl	%r15d, %r15d
.LBB10_27:                              # %.thread.i
	movl	$.L.str.112, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB10_31
# BB#28:                                # %.preheader67.i
	movq	%r13, %rax
	subq	%rbp, %rax
	je	.LBB10_33
# BB#29:                                # %.lr.ph.preheader.i
	cmpq	$32, %rax
	jae	.LBB10_34
# BB#30:
	movq	%rbx, %rdx
	jmp	.LBB10_47
.LBB10_31:                              # %rfc2231.exit.thread
	movq	40(%r14), %rax
	movslq	%r12d, %rcx
	movq	$0, (%rax,%rcx,8)
.LBB10_32:                              # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_33:
	movq	%rbx, %rdx
	jmp	.LBB10_49
.LBB10_34:                              # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB10_38
# BB#35:                                # %vector.memcheck
	cmpq	%r13, %rbx
	jae	.LBB10_39
# BB#36:                                # %vector.memcheck
	leaq	(%rbx,%rax), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB10_39
# BB#37:
	movq	%rbx, %rdx
	jmp	.LBB10_47
.LBB10_38:
	movq	%rbx, %rdx
	jmp	.LBB10_47
.LBB10_39:                              # %vector.body.preheader
	leaq	-32(%rcx), %rsi
	movl	%esi, %edi
	shrl	$5, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB10_42
# BB#40:                                # %vector.body.prol.preheader
	negq	%rdi
	xorl	%edx, %edx
.LBB10_41:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rdx), %xmm0
	movups	16(%rbp,%rdx), %xmm1
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm1, 16(%rbx,%rdx)
	addq	$32, %rdx
	incq	%rdi
	jne	.LBB10_41
	jmp	.LBB10_43
.LBB10_42:
	xorl	%edx, %edx
.LBB10_43:                              # %vector.body.prol.loopexit
	cmpq	$96, %rsi
	jb	.LBB10_45
.LBB10_44:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rdx), %xmm0
	movups	16(%rbp,%rdx), %xmm1
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm1, 16(%rbx,%rdx)
	movups	32(%rbp,%rdx), %xmm0
	movups	48(%rbp,%rdx), %xmm1
	movups	%xmm0, 32(%rbx,%rdx)
	movups	%xmm1, 48(%rbx,%rdx)
	movups	64(%rbp,%rdx), %xmm0
	movups	80(%rbp,%rdx), %xmm1
	movups	%xmm0, 64(%rbx,%rdx)
	movups	%xmm1, 80(%rbx,%rdx)
	movups	96(%rbp,%rdx), %xmm0
	movups	112(%rbp,%rdx), %xmm1
	movups	%xmm0, 96(%rbx,%rdx)
	movups	%xmm1, 112(%rbx,%rdx)
	subq	$-128, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB10_44
.LBB10_45:                              # %middle.block
	cmpq	%rcx, %rax
	je	.LBB10_48
# BB#46:
	leaq	(%rbx,%rcx), %rdx
	addq	%rcx, %rbp
	.p2align	4, 0x90
.LBB10_47:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %ecx
	incq	%rbp
	movb	%cl, (%rdx)
	incq	%rdx
	cmpq	%rbp, %r13
	jne	.LBB10_47
.LBB10_48:                              # %._crit_edge.loopexit.i
	movq	%rbx, %rdx
	addq	%rax, %rdx
	movq	%r13, %rbp
.LBB10_49:                              # %._crit_edge.i
	movb	$61, (%rdx)
	.p2align	4, 0x90
.LBB10_50:                              # =>This Inner Loop Header: Depth=1
	cmpb	$61, (%r13)
	leaq	1(%r13), %r13
	jne	.LBB10_50
# BB#51:                                # %.preheader.preheader.i
	incq	%rdx
	.p2align	4, 0x90
.LBB10_52:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r13), %eax
	testb	%al, %al
	je	.LBB10_84
# BB#53:                                #   in Loop: Header=BB10_52 Depth=1
	cmpl	$2, %r15d
	je	.LBB10_57
# BB#54:                                #   in Loop: Header=BB10_52 Depth=1
	cmpl	$1, %r15d
	je	.LBB10_61
# BB#55:                                #   in Loop: Header=BB10_52 Depth=1
	testl	%r15d, %r15d
	jne	.LBB10_83
# BB#56:                                #   in Loop: Header=BB10_52 Depth=1
	xorl	%r15d, %r15d
	cmpb	$39, %al
	sete	%r15b
	jmp	.LBB10_83
.LBB10_57:                              #   in Loop: Header=BB10_52 Depth=1
	cmpb	$37, %al
	jne	.LBB10_62
# BB#58:                                #   in Loop: Header=BB10_52 Depth=1
	leaq	1(%r13), %rax
	movsbl	1(%r13), %esi
	testl	%esi, %esi
	movl	$2, %r15d
	je	.LBB10_63
# BB#59:                                #   in Loop: Header=BB10_52 Depth=1
	cmpb	$10, %sil
	jne	.LBB10_64
# BB#60:                                #   in Loop: Header=BB10_52 Depth=1
	movq	%rax, %r13
	jmp	.LBB10_83
.LBB10_61:                              #   in Loop: Header=BB10_52 Depth=1
	xorl	%r15d, %r15d
	cmpb	$39, %al
	sete	%r15b
	incl	%r15d
	jmp	.LBB10_83
.LBB10_62:                              #   in Loop: Header=BB10_52 Depth=1
	movb	%al, (%rdx)
	incq	%rdx
	movl	$2, %r15d
	jmp	.LBB10_83
.LBB10_63:                              #   in Loop: Header=BB10_52 Depth=1
	movq	%rax, %r13
	jmp	.LBB10_83
.LBB10_64:                              #   in Loop: Header=BB10_52 Depth=1
	movslq	%esi, %rdi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	testb	$8, 1(%rax,%rdi,2)
	jne	.LBB10_67
# BB#65:                                #   in Loop: Header=BB10_52 Depth=1
	movl	%esi, %eax
	addb	$-65, %al
	cmpb	$5, %al
	ja	.LBB10_68
# BB#66:                                #   in Loop: Header=BB10_52 Depth=1
	addb	$-55, %sil
	movb	%sil, %dil
	jmp	.LBB10_71
.LBB10_67:                              #   in Loop: Header=BB10_52 Depth=1
	addb	$-48, %dil
	jmp	.LBB10_71
.LBB10_68:                              #   in Loop: Header=BB10_52 Depth=1
	movl	%edi, %eax
	addb	$-97, %al
	cmpb	$5, %al
	ja	.LBB10_70
# BB#69:                                #   in Loop: Header=BB10_52 Depth=1
	addb	$-87, %dil
	jmp	.LBB10_71
.LBB10_70:                              #   in Loop: Header=BB10_52 Depth=1
	movl	$.L.str.97, %edi
	xorl	%eax, %eax
	movq	%rdx, (%rsp)            # 8-byte Spill
	callq	cli_dbgmsg
	movq	(%rsp), %rdx            # 8-byte Reload
	movb	$61, %dil
.LBB10_71:                              # %hex.exit.i
                                        #   in Loop: Header=BB10_52 Depth=1
	movsbl	2(%r13), %esi
	addq	$2, %r13
	cmpl	$10, %esi
	je	.LBB10_73
# BB#72:                                # %hex.exit.i
                                        #   in Loop: Header=BB10_52 Depth=1
	testb	%sil, %sil
	jne	.LBB10_74
.LBB10_73:                              #   in Loop: Header=BB10_52 Depth=1
	movb	%dil, (%rdx)
	jmp	.LBB10_82
.LBB10_74:                              #   in Loop: Header=BB10_52 Depth=1
	movslq	%esi, %rax
	shlb	$4, %dil
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx), %rcx
	testb	$8, 1(%rcx,%rax,2)
	jne	.LBB10_77
# BB#75:                                #   in Loop: Header=BB10_52 Depth=1
	movl	%esi, %ecx
	addb	$-65, %cl
	cmpb	$5, %cl
	ja	.LBB10_78
# BB#76:                                #   in Loop: Header=BB10_52 Depth=1
	addb	$-55, %sil
	movb	%sil, %al
	jmp	.LBB10_81
.LBB10_77:                              #   in Loop: Header=BB10_52 Depth=1
	addb	$-48, %al
	jmp	.LBB10_81
.LBB10_78:                              #   in Loop: Header=BB10_52 Depth=1
	movl	%eax, %ecx
	addb	$-97, %cl
	cmpb	$5, %cl
	ja	.LBB10_80
# BB#79:                                #   in Loop: Header=BB10_52 Depth=1
	addb	$-87, %al
	jmp	.LBB10_81
.LBB10_80:                              #   in Loop: Header=BB10_52 Depth=1
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	$.L.str.97, %edi
	xorl	%eax, %eax
	movq	%rdx, (%rsp)            # 8-byte Spill
	callq	cli_dbgmsg
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rsp), %rdx            # 8-byte Reload
	movb	$61, %al
.LBB10_81:                              # %hex.exit64.i
                                        #   in Loop: Header=BB10_52 Depth=1
	addb	%dil, %al
	movb	%al, (%rdx)
.LBB10_82:                              #   in Loop: Header=BB10_52 Depth=1
	incq	%rdx
	.p2align	4, 0x90
.LBB10_83:                              #   in Loop: Header=BB10_52 Depth=1
	cmpb	$0, (%r13)
	leaq	1(%r13), %r13
	jne	.LBB10_52
.LBB10_84:
	cmpl	$2, %r15d
	jne	.LBB10_86
# BB#85:
	movb	$0, (%rdx)
	movl	$.L.str.114, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	jmp	.LBB10_17
.LBB10_86:
	movq	%rbx, %rdi
	callq	free
	movl	$.L.str.113, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_warnmsg
	movl	$.L.str.8, %edi
	jmp	.LBB10_16
.Lfunc_end10:
	.size	messageAddArgument, .Lfunc_end10-messageAddArgument
	.cfi_endproc

	.p2align	4, 0x90
	.type	usefulArg,@function
usefulArg:                              # @usefulArg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 32
.Lcfi61:
	.cfi_offset %rbx, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$.L.str.22, %esi
	movl	$4, %edx
	callq	strncasecmp
	movl	$1, %ebp
	testl	%eax, %eax
	je	.LBB11_9
# BB#1:
	movl	$.L.str.21, %esi
	movl	$8, %edx
	movq	%rbx, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB11_9
# BB#2:
	movl	$.L.str.98, %esi
	movl	$8, %edx
	movq	%rbx, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB11_9
# BB#3:
	movl	$.L.str.99, %esi
	movl	$8, %edx
	movq	%rbx, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB11_9
# BB#4:
	movl	$.L.str.100, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB11_9
# BB#5:
	movl	$.L.str.101, %esi
	movl	$6, %edx
	movq	%rbx, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB11_9
# BB#6:
	movl	$.L.str.102, %esi
	movl	$5, %edx
	movq	%rbx, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB11_9
# BB#7:
	movl	$.L.str.103, %esi
	movl	$4, %edx
	movq	%rbx, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB11_9
# BB#8:
	xorl	%ebp, %ebp
	movl	$.L.str.104, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
.LBB11_9:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end11:
	.size	usefulArg, .Lfunc_end11-usefulArg
	.cfi_endproc

	.globl	messageAddArguments
	.p2align	4, 0x90
	.type	messageAddArguments,@function
messageAddArguments:                    # @messageAddArguments
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi66:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi69:
	.cfi_def_cfa_offset 64
.Lcfi70:
	.cfi_offset %rbx, -56
.Lcfi71:
	.cfi_offset %r12, -48
.Lcfi72:
	.cfi_offset %r13, -40
.Lcfi73:
	.cfi_offset %r14, -32
.Lcfi74:
	.cfi_offset %r15, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, (%rsp)            # 8-byte Spill
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	(%r15), %r14b
	testb	%r14b, %r14b
	je	.LBB12_39
# BB#1:                                 # %.lr.ph.lr.ph
	callq	__ctype_b_loc
	movq	%rax, %r12
	movq	%r15, %r13
	jmp	.LBB12_2
.LBB12_31:                              # %.thread99.thread110
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	%rbp, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB12_38
	.p2align	4, 0x90
.LBB12_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_10 Depth 2
                                        #     Child Loop BB12_17 Depth 2
	cmpb	$59, %r14b
	je	.LBB12_4
# BB#3:                                 #   in Loop: Header=BB12_2 Depth=1
	movq	(%r12), %rbp
	movsbq	%r14b, %rax
	movzwl	(%rbp,%rax,2), %eax
	andl	$8192, %eax             # imm = 0x2000
	testw	%ax, %ax
	jne	.LBB12_4
# BB#5:                                 #   in Loop: Header=BB12_2 Depth=1
	movl	$61, %esi
	movq	%r13, %rdi
	callq	strchr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB12_9
# BB#6:                                 #   in Loop: Header=BB12_2 Depth=1
	movl	$58, %esi
	movq	%r13, %rdi
	callq	strchr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB12_7
.LBB12_9:                               # %.thread
                                        #   in Loop: Header=BB12_2 Depth=1
	incq	%rbx
	.p2align	4, 0x90
.LBB12_10:                              #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%rbx), %rax
	incq	%rbx
	testq	%rax, %rax
	je	.LBB12_12
# BB#11:                                #   in Loop: Header=BB12_10 Depth=2
	movzwl	(%rbp,%rax,2), %ecx
	andl	$8192, %ecx             # imm = 0x2000
	testw	%cx, %cx
	jne	.LBB12_10
.LBB12_12:                              #   in Loop: Header=BB12_2 Depth=1
	testb	%r14b, %r14b
	je	.LBB12_37
# BB#13:                                #   in Loop: Header=BB12_2 Depth=1
	cmpb	$34, %al
	jne	.LBB12_14
# BB#19:                                #   in Loop: Header=BB12_2 Depth=1
	movq	%r13, %rdi
	callq	cli_strdup
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB12_39
# BB#20:                                #   in Loop: Header=BB12_2 Depth=1
	movl	$61, %esi
	movq	%rbp, %rdi
	callq	strchr
	testq	%rax, %rax
	jne	.LBB12_22
# BB#21:                                #   in Loop: Header=BB12_2 Depth=1
	movl	$58, %esi
	movq	%rbp, %rdi
	callq	strchr
.LBB12_22:                              #   in Loop: Header=BB12_2 Depth=1
	movb	$0, (%rax)
	movl	$34, %esi
	movq	%rbx, %rdi
	callq	strchr
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB12_23
# BB#24:                                #   in Loop: Header=BB12_2 Depth=1
	incq	%r13
	jmp	.LBB12_25
	.p2align	4, 0x90
.LBB12_4:                               #   in Loop: Header=BB12_2 Depth=1
	incq	%r13
	jmp	.LBB12_38
.LBB12_23:                              #   in Loop: Header=BB12_2 Depth=1
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movl	$.L.str.8, %r13d
.LBB12_25:                              #   in Loop: Header=BB12_2 Depth=1
	movq	%rbp, %rdi
	callq	usefulArg
	testl	%eax, %eax
	jne	.LBB12_26
# BB#40:                                #   in Loop: Header=BB12_2 Depth=1
	movq	%rbp, %rdi
	callq	free
	jmp	.LBB12_38
.LBB12_14:                              #   in Loop: Header=BB12_2 Depth=1
	testb	%al, %al
	je	.LBB12_34
# BB#15:                                # %.preheader
                                        #   in Loop: Header=BB12_2 Depth=1
	movb	(%rbx), %al
	testb	%al, %al
	jne	.LBB12_17
	jmp	.LBB12_32
	.p2align	4, 0x90
.LBB12_18:                              #   in Loop: Header=BB12_17 Depth=2
	movzbl	1(%rbx), %eax
	incq	%rbx
	testb	%al, %al
	je	.LBB12_32
.LBB12_17:                              # %.lr.ph129
                                        #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%al, %rax
	testb	$32, 1(%rbp,%rax,2)
	je	.LBB12_18
.LBB12_32:                              # %.critedge
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	%rbx, %rbp
	subq	%r13, %rbp
	leaq	1(%rbp), %rdi
	callq	cli_malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB12_37
# BB#33:                                # %.thread99.thread107
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movb	$0, (%r14,%rbp)
	jmp	.LBB12_36
.LBB12_26:                              #   in Loop: Header=BB12_2 Depth=1
	movq	%rbx, %rdi
	callq	cli_strdup
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB12_41
# BB#27:                                #   in Loop: Header=BB12_2 Depth=1
	movl	$34, %esi
	movq	%rbx, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB12_28
# BB#30:                                #   in Loop: Header=BB12_2 Depth=1
	movb	$0, (%rax)
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	strlen
	leaq	2(%r14,%rax), %rsi
	movq	%rbp, %rdi
	callq	cli_realloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB12_31
# BB#35:                                # %.thread99
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	%r14, %rdi
	callq	strlen
	movw	$61, (%r14,%rax)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	strcat
	movq	%rbx, %rdi
	callq	free
	movq	%r13, %rbx
.LBB12_36:                              #   in Loop: Header=BB12_2 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r14, %rsi
	callq	messageAddArgument
	movq	%r14, %rdi
	callq	free
.LBB12_37:                              # %.thread102.outer.backedge
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	%rbx, %r13
	.p2align	4, 0x90
.LBB12_38:                              # %.thread102.outer.backedge
                                        #   in Loop: Header=BB12_2 Depth=1
	movb	(%r13), %r14b
	testb	%r14b, %r14b
	jne	.LBB12_2
.LBB12_39:                              # %.thread105
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_7:
	movl	$.L.str.15, %edi
	jmp	.LBB12_8
.LBB12_34:
	movl	$.L.str.19, %edi
.LBB12_8:
	xorl	%eax, %eax
	movq	%r15, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	cli_dbgmsg              # TAILCALL
.LBB12_41:
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	jmp	.LBB12_29
.LBB12_28:
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	free
.LBB12_29:
	movq	%rbp, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end12:
	.size	messageAddArguments, .Lfunc_end12-messageAddArguments
	.cfi_endproc

	.globl	messageFindArgument
	.p2align	4, 0x90
	.type	messageFindArgument,@function
messageFindArgument:                    # @messageFindArgument
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi79:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi80:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi82:
	.cfi_def_cfa_offset 64
.Lcfi83:
	.cfi_offset %rbx, -56
.Lcfi84:
	.cfi_offset %r12, -48
.Lcfi85:
	.cfi_offset %r13, -40
.Lcfi86:
	.cfi_offset %r14, -32
.Lcfi87:
	.cfi_offset %r15, -24
.Lcfi88:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rsi, %rdi
	callq	strlen
	movq	%rax, %r12
	movslq	32(%rbx), %r13
	testq	%r13, %r13
	jle	.LBB13_15
# BB#1:                                 # %.lr.ph
	movq	40(%rbx), %rbp
	movl	$.L.str.8, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%r15,8), %rbx
	testq	%rbx, %rbx
	cmoveq	%r14, %rbx
	cmpb	$0, (%rbx)
	je	.LBB13_14
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=1
	movq	%rbx, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%r12, %rdx
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB13_4
.LBB13_14:                              #   in Loop: Header=BB13_2 Depth=1
	incq	%r15
	cmpq	%r13, %r15
	jl	.LBB13_2
.LBB13_15:
	xorl	%ebp, %ebp
.LBB13_16:                              # %.thread
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_4:
	addq	%rbx, %r12
	callq	__ctype_b_loc
	movq	(%rax), %rax
	.p2align	4, 0x90
.LBB13_5:                               # =>This Inner Loop Header: Depth=1
	movsbq	(%r12), %rcx
	incq	%r12
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB13_5
# BB#6:
	cmpb	$61, %cl
	jne	.LBB13_7
# BB#8:
	cmpb	$34, (%r12)
	jne	.LBB13_13
# BB#9:
	leaq	1(%r12), %rbx
	movl	$34, %esi
	movq	%rbx, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB13_13
# BB#10:
	movq	%rbx, %rdi
	callq	cli_strdup
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB13_15
# BB#11:
	movl	$34, %esi
	movq	%rbp, %rdi
	callq	strchr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB13_16
# BB#12:
	movq	%rbp, %rdi
	callq	strlen
	movb	$0, -1(%rbp,%rax)
	movb	$0, (%rbx)
	jmp	.LBB13_16
.LBB13_7:
	xorl	%ebp, %ebp
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rbx, %rdx
	callq	cli_warnmsg
	jmp	.LBB13_16
.LBB13_13:
	movq	%r12, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	cli_strdup              # TAILCALL
.Lfunc_end13:
	.size	messageFindArgument, .Lfunc_end13-messageFindArgument
	.cfi_endproc

	.globl	messageGetFilename
	.p2align	4, 0x90
	.type	messageGetFilename,@function
messageGetFilename:                     # @messageGetFilename
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 16
.Lcfi90:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.L.str.21, %esi
	callq	messageFindArgument
	testq	%rax, %rax
	je	.LBB14_2
# BB#1:
	popq	%rbx
	retq
.LBB14_2:
	movl	$.L.str.22, %esi
	movq	%rbx, %rdi
	popq	%rbx
	jmp	messageFindArgument     # TAILCALL
.Lfunc_end14:
	.size	messageGetFilename, .Lfunc_end14-messageGetFilename
	.cfi_endproc

	.globl	messageHasFilename
	.p2align	4, 0x90
	.type	messageHasFilename,@function
messageHasFilename:                     # @messageHasFilename
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 16
.Lcfi92:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.L.str.21, %esi
	callq	messageHasArgument
	movb	$1, %cl
	testl	%eax, %eax
	jne	.LBB15_2
# BB#1:
	movl	$.L.str.23, %esi
	movq	%rbx, %rdi
	callq	messageHasArgument
	testl	%eax, %eax
	setne	%cl
.LBB15_2:
	movzbl	%cl, %eax
	popq	%rbx
	retq
.Lfunc_end15:
	.size	messageHasFilename, .Lfunc_end15-messageHasFilename
	.cfi_endproc

	.p2align	4, 0x90
	.type	messageHasArgument,@function
messageHasArgument:                     # @messageHasArgument
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi96:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi97:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 64
.Lcfi100:
	.cfi_offset %rbx, -56
.Lcfi101:
	.cfi_offset %r12, -48
.Lcfi102:
	.cfi_offset %r13, -40
.Lcfi103:
	.cfi_offset %r14, -32
.Lcfi104:
	.cfi_offset %r15, -24
.Lcfi105:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rsi, %rdi
	callq	strlen
	movq	%rax, %r15
	movslq	32(%rbx), %r12
	testq	%r12, %r12
	jle	.LBB16_9
# BB#1:                                 # %.lr.ph
	movq	40(%rbx), %r13
	xorl	%ebp, %ebp
	movl	$.L.str.8, %r14d
	.p2align	4, 0x90
.LBB16_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbp,8), %rbx
	testq	%rbx, %rbx
	cmoveq	%r14, %rbx
	cmpb	$0, (%rbx)
	je	.LBB16_8
# BB#3:                                 #   in Loop: Header=BB16_2 Depth=1
	movq	%rbx, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%r15, %rdx
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB16_4
.LBB16_8:                               #   in Loop: Header=BB16_2 Depth=1
	incq	%rbp
	cmpq	%r12, %rbp
	jl	.LBB16_2
.LBB16_9:
	xorl	%ebp, %ebp
.LBB16_10:                              # %.thread
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_4:
	addq	%rbx, %r15
	callq	__ctype_b_loc
	movq	(%rax), %rax
	.p2align	4, 0x90
.LBB16_5:                               # =>This Inner Loop Header: Depth=1
	movsbq	(%r15), %rcx
	incq	%r15
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB16_5
# BB#6:
	movl	$1, %ebp
	cmpb	$61, %cl
	je	.LBB16_10
# BB#7:
	xorl	%ebp, %ebp
	movl	$.L.str.57, %edi
	xorl	%eax, %eax
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rbx, %rdx
	callq	cli_warnmsg
	jmp	.LBB16_10
.Lfunc_end16:
	.size	messageHasArgument, .Lfunc_end16-messageHasArgument
	.cfi_endproc

	.globl	messageSetEncoding
	.p2align	4, 0x90
	.type	messageSetEncoding,@function
messageSetEncoding:                     # @messageSetEncoding
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi109:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi110:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi112:
	.cfi_def_cfa_offset 80
.Lcfi113:
	.cfi_offset %rbx, -56
.Lcfi114:
	.cfi_offset %r12, -48
.Lcfi115:
	.cfi_offset %r13, -40
.Lcfi116:
	.cfi_offset %r14, -32
.Lcfi117:
	.cfi_offset %r15, -24
.Lcfi118:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	callq	__ctype_b_loc
	movq	(%rax), %rax
	decq	%r15
	.p2align	4, 0x90
.LBB17_1:                               # =>This Inner Loop Header: Depth=1
	movsbq	1(%r15), %rcx
	incq	%r15
	testb	$1, (%rax,%rcx,2)
	jne	.LBB17_1
# BB#2:
	movq	%rbx, (%rsp)            # 8-byte Spill
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movl	$.L.str.25, %esi
	movq	%r15, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	jne	.LBB17_4
# BB#3:
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.27, %r15d
.LBB17_4:
	xorl	%esi, %esi
	movl	$.L.str.28, %edx
	movq	%r15, %rdi
	callq	cli_strtok
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB17_29
# BB#5:                                 # %.preheader115.lr.ph
	movl	$1, %ebp
	callq	__ctype_tolower_loc
	movq	%rax, %r12
	jmp	.LBB17_6
	.p2align	4, 0x90
.LBB17_11:                              # %.preheader
                                        #   in Loop: Header=BB17_6 Depth=1
	movq	(%rsp), %r13            # 8-byte Reload
	movslq	16(%r13), %rax
	testq	%rax, %rax
	jle	.LBB17_12
# BB#13:                                # %.lr.ph
                                        #   in Loop: Header=BB17_6 Depth=1
	movq	8(%r13), %rdx
	movl	8(%rbp), %esi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB17_14:                              #   Parent Loop BB17_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%esi, (%rdx,%rcx,4)
	je	.LBB17_16
# BB#15:                                #   in Loop: Header=BB17_14 Depth=2
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB17_14
	jmp	.LBB17_16
.LBB17_12:                              #   in Loop: Header=BB17_6 Depth=1
	xorl	%ecx, %ecx
.LBB17_16:                              # %._crit_edge
                                        #   in Loop: Header=BB17_6 Depth=1
	cmpl	%eax, %ecx
	jge	.LBB17_18
# BB#17:                                #   in Loop: Header=BB17_6 Depth=1
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	jmp	.LBB17_20
.LBB17_18:                              #   in Loop: Header=BB17_6 Depth=1
	movq	8(%r13), %rdi
	leaq	4(,%rax,4), %rsi
	callq	cli_realloc
	testq	%rax, %rax
	je	.LBB17_20
# BB#19:                                #   in Loop: Header=BB17_6 Depth=1
	movq	%rax, 8(%r13)
	movl	8(%rbp), %ecx
	movslq	16(%r13), %rdx
	leal	1(%rdx), %esi
	movl	%esi, 16(%r13)
	movl	%ecx, (%rax,%rdx,4)
	movl	16(%r13), %esi
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	cli_dbgmsg
.LBB17_20:                              #   in Loop: Header=BB17_6 Depth=1
	cmpq	$0, (%rbp)
	jne	.LBB17_28
	jmp	.LBB17_24
	.p2align	4, 0x90
.LBB17_6:                               # %.preheader115
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_7 Depth 2
                                        #     Child Loop BB17_14 Depth 2
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	$.L.str.58, %r13d
	movl	$encoding_map, %ebp
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB17_7:                               #   Parent Loop BB17_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %rcx
	movsbq	(%rbx), %rax
	movsbl	(%rcx,%rax,4), %eax
	movsbq	(%r13), %rdx
	cmpl	(%rcx,%rdx,4), %eax
	je	.LBB17_9
# BB#8:                                 #   in Loop: Header=BB17_7 Depth=2
	cmpl	$120, %eax
	jne	.LBB17_23
.LBB17_9:                               #   in Loop: Header=BB17_7 Depth=2
	movl	$.L.str.29, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB17_23
# BB#10:                                #   in Loop: Header=BB17_7 Depth=2
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	simil
	cmpl	$100, %eax
	je	.LBB17_11
# BB#21:                                #   in Loop: Header=BB17_7 Depth=2
	cmpl	%r14d, %eax
	jle	.LBB17_23
# BB#22:                                #   in Loop: Header=BB17_7 Depth=2
	movq	(%rbp), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	%eax, %r14d
	.p2align	4, 0x90
.LBB17_23:                              #   in Loop: Header=BB17_7 Depth=2
	movq	16(%rbp), %r13
	addq	$16, %rbp
	testq	%r13, %r13
	jne	.LBB17_7
.LBB17_24:                              # %.thread
                                        #   in Loop: Header=BB17_6 Depth=1
	cmpl	$50, %r14d
	jl	.LBB17_26
# BB#25:                                #   in Loop: Header=BB17_6 Depth=1
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdx
	movl	%r14d, %ecx
	callq	cli_dbgmsg
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbp, %rsi
	jmp	.LBB17_27
	.p2align	4, 0x90
.LBB17_26:                              #   in Loop: Header=BB17_6 Depth=1
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movl	$.L.str.34, %esi
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%rbp, %rdi
	callq	messageSetEncoding
	movl	$.L.str.35, %esi
	movq	%rbp, %rdi
.LBB17_27:                              #   in Loop: Header=BB17_6 Depth=1
	callq	messageSetEncoding
.LBB17_28:                              #   in Loop: Header=BB17_6 Depth=1
	movq	%rbx, %rdi
	callq	free
	movq	16(%rsp), %rsi          # 8-byte Reload
	leal	1(%rsi), %ebp
	movl	$.L.str.28, %edx
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_strtok
	movq	%rax, %rbx
	testq	%rbx, %rbx
                                        # kill: %EBP<def> %EBP<kill> %RBP<def>
	jne	.LBB17_6
.LBB17_29:                              # %._crit_edge130
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	messageSetEncoding, .Lfunc_end17-messageSetEncoding
	.cfi_endproc

	.globl	messageGetEncoding
	.p2align	4, 0x90
	.type	messageGetEncoding,@function
messageGetEncoding:                     # @messageGetEncoding
	.cfi_startproc
# BB#0:
	cmpl	$0, 16(%rdi)
	je	.LBB18_1
# BB#2:
	movq	8(%rdi), %rax
	movl	(%rax), %eax
	retq
.LBB18_1:
	xorl	%eax, %eax
	retq
.Lfunc_end18:
	.size	messageGetEncoding, .Lfunc_end18-messageGetEncoding
	.cfi_endproc

	.globl	messageAddLine
	.p2align	4, 0x90
	.type	messageAddLine,@function
messageAddLine:                         # @messageAddLine
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi121:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi123:
	.cfi_def_cfa_offset 48
.Lcfi124:
	.cfi_offset %rbx, -40
.Lcfi125:
	.cfi_offset %r12, -32
.Lcfi126:
	.cfi_offset %r14, -24
.Lcfi127:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	56(%r12), %rbx
	movl	$16, %edi
	callq	cli_malloc
	cmpq	$0, %rbx
	je	.LBB19_1
# BB#2:
	leaq	64(%r12), %r15
	movq	64(%r12), %rcx
	movq	%rax, 8(%rcx)
	jmp	.LBB19_3
.LBB19_1:
	movq	%rax, 56(%r12)
	leaq	64(%r12), %r15
.LBB19_3:
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.LBB19_4
# BB#5:
	movq	$0, 8(%rax)
	testq	%r14, %r14
	je	.LBB19_8
# BB#6:
	movq	%r14, %rdi
	callq	lineGetData
	testq	%rax, %rax
	je	.LBB19_7
# BB#11:
	movq	%r14, %rdi
	callq	lineLink
	movq	64(%r12), %rcx
	movq	%rax, (%rcx)
	movq	%r12, %rdi
	callq	messageIsEncoding
	jmp	.LBB19_9
.LBB19_4:
	movl	$-1, %eax
	jmp	.LBB19_10
.LBB19_7:                               # %._crit_edge
	movq	(%r15), %rax
.LBB19_8:
	movq	$0, (%rax)
.LBB19_9:
	movl	$1, %eax
.LBB19_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end19:
	.size	messageAddLine, .Lfunc_end19-messageAddLine
	.cfi_endproc

	.p2align	4, 0x90
	.type	messageIsEncoding,@function
messageIsEncoding:                      # @messageIsEncoding
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi130:
	.cfi_def_cfa_offset 32
.Lcfi131:
	.cfi_offset %rbx, -24
.Lcfi132:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	callq	lineGetData
	movq	%rax, %r14
	cmpq	$0, 120(%rbx)
	jne	.LBB20_4
# BB#1:
	movl	$messageIsEncoding.encoding, %esi
	movl	$25, %edx
	movq	%r14, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	jne	.LBB20_4
# BB#2:
	movl	$.L.str.58, %esi
	movq	%r14, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB20_3
.LBB20_4:
	cmpq	$0, 96(%rbx)
	jne	.LBB20_8
# BB#5:
	movl	$.L.str.66, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	jne	.LBB20_8
# BB#6:
	movq	%r14, %rdi
	callq	strlen
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	cli_filetype
	cmpl	$529, %eax              # imm = 0x211
	jne	.LBB20_8
# BB#7:
	movq	64(%rbx), %rax
	movq	%rax, 96(%rbx)
	jmp	.LBB20_15
.LBB20_8:
	cmpq	$0, 104(%rbx)
	je	.LBB20_9
.LBB20_12:
	cmpq	$0, 112(%rbx)
	jne	.LBB20_15
# BB#13:
	movl	$.L.str.68, %esi
	movl	$13, %edx
	movq	%r14, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB20_15
# BB#14:
	movq	64(%rbx), %rax
	movq	%rax, 112(%rbx)
	jmp	.LBB20_15
.LBB20_9:
	movl	$.L.str.67, %esi
	movq	%r14, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB20_12
# BB#10:
	movl	$messageIsEncoding.binhex, %esi
	movq	%r14, %rdi
	callq	simil
	cmpl	$91, %eax
	jl	.LBB20_12
# BB#11:
	movq	64(%rbx), %rax
	movq	%rax, 104(%rbx)
	jmp	.LBB20_15
.LBB20_3:
	movq	64(%rbx), %rax
	movq	%rax, 120(%rbx)
.LBB20_15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end20:
	.size	messageIsEncoding, .Lfunc_end20-messageIsEncoding
	.cfi_endproc

	.globl	messageAddStr
	.p2align	4, 0x90
	.type	messageAddStr,@function
messageAddStr:                          # @messageAddStr
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi134:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi135:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi136:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 48
.Lcfi138:
	.cfi_offset %rbx, -48
.Lcfi139:
	.cfi_offset %r12, -40
.Lcfi140:
	.cfi_offset %r14, -32
.Lcfi141:
	.cfi_offset %r15, -24
.Lcfi142:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	testq	%r12, %r12
	je	.LBB21_7
# BB#1:
	movb	(%r12), %bpl
	testb	%bpl, %bpl
	je	.LBB21_8
# BB#2:                                 # %.lr.ph.preheader
	leaq	1(%r12), %rbx
	.p2align	4, 0x90
.LBB21_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testb	%bpl, %bpl
	js	.LBB21_9
# BB#4:                                 #   in Loop: Header=BB21_3 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	%bpl, %rcx
	testb	$32, 1(%rax,%rcx,2)
	je	.LBB21_9
# BB#5:                                 #   in Loop: Header=BB21_3 Depth=1
	movzbl	(%rbx), %ebp
	incq	%rbx
	testb	%bpl, %bpl
	jne	.LBB21_3
# BB#6:
	movl	$.L.str.36, %r12d
	cmpq	$0, 56(%r14)
	jne	.LBB21_10
	jmp	.LBB21_19
.LBB21_7:
	xorl	%r12d, %r12d
	cmpq	$0, 56(%r14)
	jne	.LBB21_10
	jmp	.LBB21_19
.LBB21_8:
	xorl	%r12d, %r12d
.LBB21_9:                               # %.thread
	cmpq	$0, 56(%r14)
	je	.LBB21_19
.LBB21_10:
	leaq	64(%r14), %r15
	testq	%r12, %r12
	jne	.LBB21_13
# BB#11:
	movq	(%r15), %rax
	cmpq	$0, (%rax)
	jne	.LBB21_13
# BB#12:
	movl	$1, %eax
	cmpl	$6, (%r14)
	jne	.LBB21_35
.LBB21_13:                              # %._crit_edge65
	movl	$16, %edi
	callq	cli_malloc
	movq	(%r15), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB21_15
# BB#14:
	movq	%r14, %rdi
	callq	messageDedup
	movl	$16, %edi
	callq	cli_malloc
	movq	64(%r14), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.LBB21_34
.LBB21_15:
	testq	%r12, %r12
	je	.LBB21_21
# BB#16:
	movq	(%rcx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_21
# BB#17:
	callq	lineGetData
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	strcmp
	movq	(%r15), %rcx
	testl	%eax, %eax
	je	.LBB21_22
# BB#18:
	xorl	%edi, %edi
	jmp	.LBB21_23
.LBB21_19:
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, 56(%r14)
	leaq	64(%r14), %r15
	xorl	%edi, %edi
	jmp	.LBB21_24
.LBB21_21:
	xorl	%edi, %edi
.LBB21_23:                              # %._crit_edge
	movq	8(%rcx), %rax
.LBB21_24:
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.LBB21_34
# BB#25:
	movq	$0, 8(%rax)
	testq	%r12, %r12
	je	.LBB21_29
# BB#26:
	cmpb	$0, (%r12)
	je	.LBB21_29
# BB#27:
	testq	%rdi, %rdi
	je	.LBB21_31
# BB#28:
	callq	lineLink
	movq	(%r15), %rcx
	movq	%rax, (%rcx)
	jmp	.LBB21_30
.LBB21_29:
	movq	$0, (%rax)
.LBB21_30:
	movl	$1, %eax
	jmp	.LBB21_35
.LBB21_31:
	movq	%r12, %rdi
	callq	lineCreate
	movq	(%r15), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB21_33
# BB#32:
	movq	%r14, %rdi
	callq	messageDedup
	movq	%r12, %rdi
	callq	lineCreate
	movq	64(%r14), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	je	.LBB21_34
.LBB21_33:
	movq	%r14, %rdi
	callq	messageIsEncoding
	jmp	.LBB21_30
.LBB21_34:
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-1, %eax
.LBB21_35:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_22:
	movq	(%rcx), %rdi
	jmp	.LBB21_23
.Lfunc_end21:
	.size	messageAddStr, .Lfunc_end21-messageAddStr
	.cfi_endproc

	.p2align	4, 0x90
	.type	messageDedup,@function
messageDedup:                           # @messageDedup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi143:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi144:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi145:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi146:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi147:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi148:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi149:
	.cfi_def_cfa_offset 80
.Lcfi150:
	.cfi_offset %rbx, -56
.Lcfi151:
	.cfi_offset %r12, -48
.Lcfi152:
	.cfi_offset %r13, -40
.Lcfi153:
	.cfi_offset %r14, -32
.Lcfi154:
	.cfi_offset %r15, -24
.Lcfi155:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	movl	$.L.str.105, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	56(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB22_1
# BB#2:                                 # %.lr.ph92
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB22_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_12 Depth 2
	movq	(%rbp), %r13
	testq	%r13, %r13
	je	.LBB22_23
# BB#4:                                 #   in Loop: Header=BB22_3 Depth=1
	movq	%r13, %rdi
	callq	lineGetData
	movq	%rax, %r12
	movq	%r12, %rdi
	callq	strlen
	cmpq	$8, %rax
	jb	.LBB22_23
# BB#5:                                 #   in Loop: Header=BB22_3 Depth=1
	movzbl	(%r13), %eax
	cmpl	$255, %eax
	je	.LBB22_23
# BB#6:                                 #   in Loop: Header=BB22_3 Depth=1
	cmpq	120(%r14), %rbp
	je	.LBB22_23
# BB#7:                                 #   in Loop: Header=BB22_3 Depth=1
	cmpq	96(%r14), %rbp
	je	.LBB22_23
# BB#8:                                 #   in Loop: Header=BB22_3 Depth=1
	cmpq	104(%r14), %rbp
	je	.LBB22_23
# BB#9:                                 #   in Loop: Header=BB22_3 Depth=1
	cmpq	112(%r14), %rbp
	je	.LBB22_23
# BB#10:                                # %.preheader
                                        #   in Loop: Header=BB22_3 Depth=1
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	8(%rbp), %r15
	testq	%r15, %r15
	jne	.LBB22_12
	jmp	.LBB22_23
	.p2align	4, 0x90
.LBB22_13:                              #   in Loop: Header=BB22_12 Depth=2
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	8(%r15), %r15
	testq	%r15, %r15
	je	.LBB22_23
.LBB22_12:                              # %.lr.ph
                                        #   Parent Loop BB22_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	(%r15), %rbx
	testq	%rbx, %rbx
	je	.LBB22_13
# BB#14:                                #   in Loop: Header=BB22_12 Depth=2
	movq	%rbx, %rdi
	callq	lineGetData
	cmpq	%rax, %r12
	je	.LBB22_15
# BB#16:                                #   in Loop: Header=BB22_12 Depth=2
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB22_18
# BB#17:                                #   in Loop: Header=BB22_12 Depth=2
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	8(%r15), %r15
	testq	%r15, %r15
	jne	.LBB22_12
	jmp	.LBB22_23
	.p2align	4, 0x90
.LBB22_15:                              #   in Loop: Header=BB22_12 Depth=2
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	8(%r15), %r15
	testq	%r15, %r15
	jne	.LBB22_12
	jmp	.LBB22_23
.LBB22_18:                              #   in Loop: Header=BB22_12 Depth=2
	movq	%rbx, %rdi
	callq	lineUnlink
	testq	%rax, %rax
	jne	.LBB22_20
# BB#19:                                #   in Loop: Header=BB22_12 Depth=2
	movq	%r12, %rdi
	callq	strlen
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	1(%rcx,%rax), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
.LBB22_20:                              #   in Loop: Header=BB22_12 Depth=2
	movq	%r13, %rdi
	callq	lineLink
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.LBB22_26
# BB#21:                                #   in Loop: Header=BB22_12 Depth=2
	movl	20(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	cmpl	$255, %eax
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB22_23
# BB#22:                                # %.backedge
                                        #   in Loop: Header=BB22_12 Depth=2
	movq	8(%r15), %r15
	testq	%r15, %r15
	jne	.LBB22_12
	.p2align	4, 0x90
.LBB22_23:                              # %.thread
                                        #   in Loop: Header=BB22_3 Depth=1
	movq	8(%rbp), %rbp
	cmpq	$99999, %rbx            # imm = 0x1869F
	ja	.LBB22_25
# BB#24:                                # %.thread
                                        #   in Loop: Header=BB22_3 Depth=1
	testq	%rbp, %rbp
	jne	.LBB22_3
	jmp	.LBB22_25
.LBB22_1:
	xorl	%ebp, %ebp
.LBB22_25:                              # %.loopexit
	movl	$.L.str.107, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movq	%rbp, 128(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB22_26:                              # %.loopexit.loopexit
	movl	$.L.str.106, %edi
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	cli_errmsg              # TAILCALL
.Lfunc_end22:
	.size	messageDedup, .Lfunc_end22-messageDedup
	.cfi_endproc

	.globl	messageAddStrAtTop
	.p2align	4, 0x90
	.type	messageAddStrAtTop,@function
messageAddStrAtTop:                     # @messageAddStrAtTop
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi156:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi157:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi158:
	.cfi_def_cfa_offset 32
.Lcfi159:
	.cfi_offset %rbx, -32
.Lcfi160:
	.cfi_offset %r14, -24
.Lcfi161:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	56(%r15), %rbx
	testq	%rbx, %rbx
	je	.LBB23_4
# BB#1:
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, 56(%r15)
	testq	%rax, %rax
	je	.LBB23_6
# BB#2:
	movq	%rbx, 8(%rax)
	testq	%r14, %r14
	movl	$.L.str.8, %edi
	cmovneq	%r14, %rdi
	callq	lineCreate
	movq	%rax, %rcx
	movq	56(%r15), %rax
	movq	%rcx, (%rax)
	movl	$1, %eax
	testq	%rcx, %rcx
	jne	.LBB23_16
# BB#3:
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB23_12
.LBB23_4:
	movq	%r14, %rdi
	callq	lineCreate
	movq	%rax, %r14
	movq	56(%r15), %rbx
	movl	$16, %edi
	callq	cli_malloc
	cmpq	$0, %rbx
	je	.LBB23_7
# BB#5:
	leaq	64(%r15), %rbx
	movq	64(%r15), %rcx
	movq	%rax, 8(%rcx)
	jmp	.LBB23_8
.LBB23_6:
	movq	%rbx, 56(%r15)
	jmp	.LBB23_12
.LBB23_7:
	movq	%rax, 56(%r15)
	leaq	64(%r15), %rbx
.LBB23_8:
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB23_12
# BB#9:
	movq	$0, 8(%rax)
	testq	%r14, %r14
	je	.LBB23_14
# BB#10:
	movq	%r14, %rdi
	callq	lineGetData
	testq	%rax, %rax
	je	.LBB23_13
# BB#11:
	movq	%r14, %rdi
	callq	lineLink
	movq	64(%r15), %rcx
	movq	%rax, (%rcx)
	movq	%r15, %rdi
	callq	messageIsEncoding
	jmp	.LBB23_15
.LBB23_12:                              # %messageAddLine.exit
	movl	$-1, %eax
	jmp	.LBB23_16
.LBB23_13:                              # %._crit_edge.i
	movq	(%rbx), %rax
.LBB23_14:
	movq	$0, (%rax)
.LBB23_15:                              # %messageAddLine.exit
	movl	$1, %eax
.LBB23_16:                              # %messageAddLine.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end23:
	.size	messageAddStrAtTop, .Lfunc_end23-messageAddStrAtTop
	.cfi_endproc

	.globl	messageMoveText
	.p2align	4, 0x90
	.type	messageMoveText,@function
messageMoveText:                        # @messageMoveText
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi162:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi163:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi164:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi165:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi166:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi167:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi168:
	.cfi_def_cfa_offset 64
.Lcfi169:
	.cfi_offset %rbx, -56
.Lcfi170:
	.cfi_offset %r12, -48
.Lcfi171:
	.cfi_offset %r13, -40
.Lcfi172:
	.cfi_offset %r14, -32
.Lcfi173:
	.cfi_offset %r15, -24
.Lcfi174:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r13
	cmpq	$0, 56(%r13)
	je	.LBB24_1
# BB#16:
	leaq	64(%r13), %rbp
	movq	64(%r13), %rdi
	movq	%r15, %rsi
	callq	textMove
	movq	%rax, 64(%r13)
	testq	%rax, %rax
	je	.LBB24_18
# BB#17:
	xorl	%r14d, %r14d
	jmp	.LBB24_19
.LBB24_1:
	testq	%r14, %r14
	je	.LBB24_15
# BB#2:
	movq	%r15, 56(%r13)
	leaq	56(%r14), %r12
	movq	56(%r14), %rbp
	.p2align	4, 0x90
.LBB24_3:                               # %.thread
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r15, %rbp
	je	.LBB24_9
# BB#4:                                 #   in Loop: Header=BB24_3 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB24_6
# BB#5:                                 #   in Loop: Header=BB24_3 Depth=1
	callq	lineUnlink
.LBB24_6:                               #   in Loop: Header=BB24_3 Depth=1
	movq	8(%rbp), %rbx
	movq	%rbp, %rdi
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rbp
	jne	.LBB24_3
# BB#7:
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-1, %r14d
	jmp	.LBB24_8
.LBB24_18:
	movq	56(%r13), %rax
	movq	%rax, 64(%r13)
	movl	$-1, %r14d
	jmp	.LBB24_19
.LBB24_15:
	xorl	%edi, %edi
	movq	%r15, %rsi
	callq	textMove
	movq	%rax, 56(%r13)
	leaq	64(%r13), %rbp
	movq	%rax, 64(%r13)
	cmpq	$1, %rax
	sbbl	%r14d, %r14d
	jmp	.LBB24_19
.LBB24_9:
	movq	64(%r14), %rax
	movq	%rax, 64(%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	cmpq	$0, 96(%r14)
	jne	.LBB24_14
# BB#10:
	cmpq	$0, 120(%r14)
	jne	.LBB24_14
# BB#11:
	cmpq	$0, 104(%r14)
	jne	.LBB24_14
# BB#12:
	cmpq	$0, 112(%r14)
	je	.LBB24_13
.LBB24_14:
	leaq	64(%r13), %rbp
	movq	56(%r13), %rax
	movq	%rax, 64(%r13)
	xorl	%r14d, %r14d
	jmp	.LBB24_19
	.p2align	4, 0x90
.LBB24_21:                              #   in Loop: Header=BB24_19 Depth=1
	movq	%r13, %rdi
	callq	messageIsEncoding
	movq	(%rbp), %rax
.LBB24_19:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.LBB24_8
# BB#20:                                # %.lr.ph
                                        #   in Loop: Header=BB24_19 Depth=1
	movq	%rax, (%rbp)
	cmpq	$0, (%rax)
	jne	.LBB24_21
	jmp	.LBB24_19
.LBB24_13:
	xorl	%r14d, %r14d
.LBB24_8:                               # %.thread53
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	messageMoveText, .Lfunc_end24-messageMoveText
	.cfi_endproc

	.globl	messageGetBody
	.p2align	4, 0x90
	.type	messageGetBody,@function
messageGetBody:                         # @messageGetBody
	.cfi_startproc
# BB#0:
	movq	56(%rdi), %rax
	retq
.Lfunc_end25:
	.size	messageGetBody, .Lfunc_end25-messageGetBody
	.cfi_endproc

	.globl	base64Flush
	.p2align	4, 0x90
	.type	base64Flush,@function
base64Flush:                            # @base64Flush
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi175:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi176:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi177:
	.cfi_def_cfa_offset 32
.Lcfi178:
	.cfi_offset %rbx, -24
.Lcfi179:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	84(%rbx), %esi
	movl	$.L.str.40, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$0, 84(%rbx)
	je	.LBB26_2
# BB#1:
	xorl	%esi, %esi
	movl	$base64, %ecx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	decode
	movl	$0, 84(%rbx)
	jmp	.LBB26_3
.LBB26_2:
	xorl	%eax, %eax
.LBB26_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end26:
	.size	base64Flush, .Lfunc_end26-base64Flush
	.cfi_endproc

	.p2align	4, 0x90
	.type	decode,@function
decode:                                 # @decode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi180:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi181:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi182:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi183:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi184:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi185:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi186:
	.cfi_def_cfa_offset 80
.Lcfi187:
	.cfi_offset %rbx, -56
.Lcfi188:
	.cfi_offset %r12, -48
.Lcfi189:
	.cfi_offset %r13, -40
.Lcfi190:
	.cfi_offset %r14, -32
.Lcfi191:
	.cfi_offset %r15, -24
.Lcfi192:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movl	84(%rbx), %ebp
	xorl	%eax, %eax
	cmpl	$1, %ebp
	je	.LBB27_1
# BB#2:
	cmpl	$2, %ebp
	je	.LBB27_5
# BB#3:
	cmpl	$3, %ebp
	jne	.LBB27_12
# BB#4:
	movb	82(%rbx), %al
.LBB27_5:
                                        # kill: %AL<def> %AL<kill> %EAX<kill> %EAX<def>
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movb	81(%rbx), %al
	jmp	.LBB27_6
.LBB27_1:
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
.LBB27_6:                               # %.thread
	movb	80(%rbx), %dl
	movl	%edx, (%rsp)            # 4-byte Spill
	testq	%r12, %r12
	je	.LBB27_17
.LBB27_8:                               # %.preheader146
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movb	(%r12), %al
	testb	%al, %al
	je	.LBB27_47
# BB#9:                                 # %.lr.ph173.preheader
                                        # implicit-def: %DL
                                        # implicit-def: %R14B
	testl	%ebp, %ebp
	jne	.LBB27_11
	jmp	.LBB27_28
	.p2align	4, 0x90
.LBB27_49:                              # %..lr.ph173_crit_edge
	movl	84(%rbx), %ebp
	testl	%ebp, %ebp
	je	.LBB27_28
.LBB27_11:
	decl	%ebp
	movl	%ebp, 84(%rbx)
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, %r13d
	jmp	.LBB27_29
	.p2align	4, 0x90
.LBB27_28:
	incq	%r12
	movsbl	%al, %edi
	movl	%edx, %ebp
	callq	*8(%rsp)                # 8-byte Folded Reload
	movl	%ebp, %edx
	movl	%eax, %r13d
.LBB27_29:
	movb	(%r12), %al
	testb	%al, %al
	je	.LBB27_30
# BB#31:
	movl	84(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB27_33
# BB#32:
	decl	%ecx
	movl	%ecx, 84(%rbx)
	movl	20(%rsp), %ebp          # 4-byte Reload
                                        # kill: %BPL<def> %BPL<kill> %EBP<kill> %EBP<def>
	jmp	.LBB27_34
	.p2align	4, 0x90
.LBB27_30:
	movl	$1, %eax
	xorl	%ebp, %ebp
	jmp	.LBB27_42
	.p2align	4, 0x90
.LBB27_33:
	incq	%r12
	movsbl	%al, %edi
	movl	%edx, %ebp
	callq	*8(%rsp)                # 8-byte Folded Reload
	movl	%ebp, %edx
	movb	%al, %bpl
.LBB27_34:
	movb	(%r12), %al
	testb	%al, %al
	je	.LBB27_35
# BB#36:
	movl	84(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB27_38
# BB#37:
	decl	%ecx
	movl	%ecx, 84(%rbx)
	movl	4(%rsp), %r14d          # 4-byte Reload
                                        # kill: %R14B<def> %R14B<kill> %R14D<kill> %R14D<def>
	jmp	.LBB27_39
	.p2align	4, 0x90
.LBB27_35:
	movl	$2, %eax
	xorl	%r14d, %r14d
	jmp	.LBB27_42
	.p2align	4, 0x90
.LBB27_38:
	incq	%r12
	movsbl	%al, %edi
	callq	*8(%rsp)                # 8-byte Folded Reload
	movb	%al, %r14b
.LBB27_39:
	movb	(%r12), %al
	testb	%al, %al
	je	.LBB27_40
# BB#41:
	incq	%r12
	movsbl	%al, %edi
	callq	*8(%rsp)                # 8-byte Folded Reload
	movb	%al, %dl
	movl	$4, %eax
	jmp	.LBB27_42
	.p2align	4, 0x90
.LBB27_40:
	movl	$3, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB27_42:
	movl	%eax, %ecx
	andb	$7, %cl
	decb	%cl
	cmpb	$3, %cl
	ja	.LBB27_47
# BB#43:
	movzbl	%cl, %ecx
	jmpq	*.LJTI27_0(,%rcx,8)
.LBB27_48:
	shlb	$2, %r13b
	movl	%ebp, %eax
	shrb	$4, %al
	andb	$3, %al
	orb	%r13b, %al
	movb	%al, (%r15)
	shlb	$4, %bpl
	movl	%r14d, %eax
	shrb	$2, %al
	andb	$15, %al
	orb	%bpl, %al
	movb	%al, 1(%r15)
	movl	%r14d, %eax
	shlb	$6, %al
	movl	%edx, %ecx
	andb	$63, %cl
	orb	%al, %cl
	movb	%cl, 2(%r15)
	movb	(%r12), %al
	addq	$3, %r15
	testb	%al, %al
	jne	.LBB27_49
	jmp	.LBB27_47
.LBB27_12:
	testb	%r8b, %r8b
	je	.LBB27_13
# BB#14:                                # %.preheader
	movb	(%r12), %al
	testb	%al, %al
	je	.LBB27_47
# BB#15:                                # %.lr.ph.preheader
	addq	$4, %r12
	movq	%rcx, %r13
	.p2align	4, 0x90
.LBB27_16:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	%al, %edi
	callq	*%r13
	movl	%eax, %ebp
	movsbl	-3(%r12), %edi
	callq	*%r13
	movl	%eax, %ebx
	movsbl	-2(%r12), %edi
	callq	*%r13
	movl	%eax, %r14d
	shlb	$2, %bpl
	movl	%ebx, %eax
	shrb	$4, %al
	andb	$3, %al
	orb	%bpl, %al
	movb	%al, (%r15)
	movsbl	-1(%r12), %edi
	callq	*%r13
	shlb	$4, %bl
	movl	%r14d, %ecx
	shrb	$2, %cl
	andb	$15, %cl
	orb	%bl, %cl
	movb	%cl, 1(%r15)
	shlb	$6, %r14b
	andb	$63, %al
	orb	%r14b, %al
	movb	%al, 2(%r15)
	addq	$3, %r15
	movzbl	(%r12), %eax
	addq	$4, %r12
	testb	%al, %al
	jne	.LBB27_16
	jmp	.LBB27_47
.LBB27_13:
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	xorl	%eax, %eax
	movl	$0, (%rsp)              # 4-byte Folded Spill
	testq	%r12, %r12
	jne	.LBB27_8
.LBB27_17:
	testl	%ebp, %ebp
	je	.LBB27_47
# BB#18:
	movl	%eax, %r12d
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	(%rsp), %edx            # 1-byte Folded Reload
	movzwl	(%rax,%rdx,2), %ecx
	testb	$8, %cl
	movl	$64, %r8d
	cmovel	%r8d, %edx
	movzbl	%r12b, %r14d
	movzwl	(%rax,%r14,2), %ecx
	testb	$8, %cl
	movl	$64, %ecx
	cmovnel	%r14d, %ecx
	movzbl	4(%rsp), %esi           # 1-byte Folded Reload
	movzwl	(%rax,%rsi,2), %eax
	testb	$8, %al
	cmovnel	%esi, %r8d
	movl	$.L.str.96, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_dbgmsg
	movl	84(%rbx), %eax
	movl	%eax, %ecx
	decl	%ecx
	movl	%ecx, 84(%rbx)
	je	.LBB27_27
# BB#19:
	movl	%eax, %ecx
	addl	$-2, %ecx
	movl	%ecx, 84(%rbx)
	je	.LBB27_26
# BB#20:                                # %.thread141
	addl	$-3, %eax
	movl	%eax, 84(%rbx)
	movl	(%rsp), %ecx            # 4-byte Reload
	shlb	$2, %cl
	movl	%r12d, %eax
	shrb	$4, %al
	andb	$3, %al
	orb	%cl, %al
	movb	%al, (%r15)
	shlb	$4, %r12b
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, %eax
	shrb	$2, %al
	andb	$15, %al
	orb	%r12b, %al
	movb	%al, 1(%r15)
	testb	%cl, %cl
	je	.LBB27_21
# BB#22:
	shlb	$6, %cl
	movb	%cl, 2(%r15)
	addq	$3, %r15
	jmp	.LBB27_47
.LBB27_26:
	movl	%r12d, %eax
	testb	%al, %al
	je	.LBB27_27
# BB#23:
	movl	(%rsp), %ecx            # 4-byte Reload
	shlb	$2, %cl
	shrb	$4, %al
	andb	$3, %al
	orb	%cl, %al
	movb	%al, (%r15)
	shll	$4, %r14d
	testb	$-16, %r14b
	je	.LBB27_24
# BB#25:
	movb	%r14b, 1(%r15)
	addq	$2, %r15
	jmp	.LBB27_47
.LBB27_27:                              # %.thread142
	movl	(%rsp), %eax            # 4-byte Reload
	shlb	$2, %al
	movb	%al, (%r15)
	incq	%r15
	jmp	.LBB27_47
.LBB27_44:
	movb	%r14b, 82(%rbx)
.LBB27_45:                              # %.loopexit
	movb	%bpl, 81(%rbx)
.LBB27_46:                              # %.loopexit148
	movb	%r13b, 80(%rbx)
	movl	%eax, 84(%rbx)
.LBB27_47:                              # %.thread143
	movq	%r15, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB27_21:
	addq	$2, %r15
	jmp	.LBB27_47
.LBB27_24:
	incq	%r15
	jmp	.LBB27_47
.Lfunc_end27:
	.size	decode, .Lfunc_end27-decode
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI27_0:
	.quad	.LBB27_46
	.quad	.LBB27_45
	.quad	.LBB27_44
	.quad	.LBB27_48

	.text
	.p2align	4, 0x90
	.type	base64,@function
base64:                                 # @base64
	.cfi_startproc
# BB#0:
	movzbl	%dil, %eax
	movb	base64Table(%rax), %cl
	cmpb	$-1, %cl
	movb	$63, %al
	je	.LBB28_2
# BB#1:
	movl	%ecx, %eax
.LBB28_2:
	retq
.Lfunc_end28:
	.size	base64, .Lfunc_end28-base64
	.cfi_endproc

	.globl	messageToFileblob
	.p2align	4, 0x90
	.type	messageToFileblob,@function
messageToFileblob:                      # @messageToFileblob
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi193:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi194:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi195:
	.cfi_def_cfa_offset 32
.Lcfi196:
	.cfi_offset %rbx, -32
.Lcfi197:
	.cfi_offset %r14, -24
.Lcfi198:
	.cfi_offset %r15, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$.L.str.41, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	subq	$8, %rsp
.Lcfi199:
	.cfi_adjust_cfa_offset 8
	movl	$fileblobCreate, %edx
	movl	$fileblobDestroy, %ecx
	movl	$fileblobSetFilename, %r8d
	movl	$fileblobAddData, %r9d
	movq	%rbx, %rdi
	movq	%r15, %rsi
	pushq	%r14
.Lcfi200:
	.cfi_adjust_cfa_offset 8
	pushq	$fileblobSetCTX
.Lcfi201:
	.cfi_adjust_cfa_offset 8
	pushq	$textToFileblob
.Lcfi202:
	.cfi_adjust_cfa_offset 8
	callq	messageExport
	addq	$32, %rsp
.Lcfi203:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r15
	testl	%r14d, %r14d
	je	.LBB29_3
# BB#1:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB29_3
# BB#2:
	addq	$56, %rbx
	callq	textDestroy
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
.LBB29_3:
	movq	%r15, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end29:
	.size	messageToFileblob, .Lfunc_end29-messageToFileblob
	.cfi_endproc

	.p2align	4, 0x90
	.type	messageExport,@function
messageExport:                          # @messageExport
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi204:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi205:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi206:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi207:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi208:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi209:
	.cfi_def_cfa_offset 56
	subq	$1112, %rsp             # imm = 0x458
.Lcfi210:
	.cfi_def_cfa_offset 1168
.Lcfi211:
	.cfi_offset %rbx, -56
.Lcfi212:
	.cfi_offset %r12, -48
.Lcfi213:
	.cfi_offset %r13, -40
.Lcfi214:
	.cfi_offset %r14, -32
.Lcfi215:
	.cfi_offset %r15, -24
.Lcfi216:
	.cfi_offset %rbp, -16
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	cmpq	$0, 56(%r14)
	je	.LBB30_107
# BB#1:
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	callq	*%rdx
	movq	%rax, %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	testq	%rax, %rax
	je	.LBB30_107
# BB#2:
	movq	1176(%rsp), %rbp
	movl	16(%r14), %esi
	movl	$.L.str.69, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	104(%r14), %rbx
	testq	%rbx, %rbx
	movq	%r14, 56(%rsp)          # 8-byte Spill
	je	.LBB30_6
# BB#3:
	movl	$.L.str.70, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	.p2align	4, 0x90
.LBB30_4:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB30_78
# BB#5:                                 #   in Loop: Header=BB30_4 Depth=1
	cmpq	$0, (%rbx)
	je	.LBB30_4
	jmp	.LBB30_79
.LBB30_6:
	movl	16(%r14), %eax
	testl	%eax, %eax
	jne	.LBB30_12
# BB#7:
	movl	$.L.str.84, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.21, %esi
	movq	%r14, %rdi
	callq	messageFindArgument
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB30_10
# BB#8:
	movl	$.L.str.22, %esi
	movq	%r14, %rdi
	callq	messageFindArgument
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB30_138
# BB#9:
	movl	$.L.str.34, %esi
	movq	%r14, %rdi
	callq	messageSetEncoding
.LBB30_10:
	cmpb	$0, (%rbx)
	movl	$.L.str.87, %edx
	cmovneq	%rbx, %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	*40(%rsp)               # 8-byte Folded Reload
	movq	%rbx, %rdi
	callq	free
.LBB30_11:
	movl	16(%r14), %eax
	testl	%eax, %eax
	je	.LBB30_110
.LBB30_12:                              # %.thread435
	testq	%rbp, %rbp
	je	.LBB30_15
# BB#13:
	movq	72(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB30_15
# BB#14:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	*%rbp
	movl	16(%r14), %eax
.LBB30_15:                              # %.preheader470
	testl	%eax, %eax
	jle	.LBB30_109
# BB#16:                                # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB30_17:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_56 Depth 2
                                        #     Child Loop BB30_39 Depth 2
	movq	8(%r14), %rax
	movl	(%rax,%rbx,4), %r15d
	testq	%rbx, %rbx
	jle	.LBB30_20
# BB#18:                                #   in Loop: Header=BB30_17 Depth=1
	movq	%rbx, %rbp
	callq	*72(%rsp)               # 8-byte Folded Reload
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB30_111
# BB#19:                                # %.thread440
                                        #   in Loop: Header=BB30_17 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	*16(%rsp)               # 8-byte Folded Reload
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	%rbp, %rbx
.LBB30_20:                              #   in Loop: Header=BB30_17 Depth=1
	movl	$.L.str.89, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movl	%r15d, %ebp
	movl	%ebp, %edx
	callq	cli_dbgmsg
	movl	%ebp, %eax
	testq	%rbx, %rbx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	je	.LBB30_22
# BB#21:                                #   in Loop: Header=BB30_17 Depth=1
	cmpl	$6, %eax
	jne	.LBB30_26
.LBB30_22:                              #   in Loop: Header=BB30_17 Depth=1
	movq	112(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB30_26
# BB#23:                                #   in Loop: Header=BB30_17 Depth=1
	movq	(%rbp), %rdi
	callq	lineGetData
	movl	$.L.str.90, %esi
	movq	%rax, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB30_33
# BB#24:                                #   in Loop: Header=BB30_17 Depth=1
	addq	$6, %rax
	movq	%rax, %rdi
	callq	cli_strdup
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB30_33
# BB#25:                                #   in Loop: Header=BB30_17 Depth=1
	movq	%rbx, %rdi
	callq	cli_chomp
	movq	%rbx, %rdi
	callq	strstrip
	movl	$.L.str.91, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	cmpb	$0, (%rbx)
	movl	$.L.str.87, %edx
	cmovneq	%rbx, %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	*40(%rsp)               # 8-byte Folded Reload
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB30_34
	.p2align	4, 0x90
.LBB30_26:                              #   in Loop: Header=BB30_17 Depth=1
	cmpl	$5, %eax
	jne	.LBB30_28
# BB#27:                                #   in Loop: Header=BB30_17 Depth=1
	xorl	%r15d, %r15d
	movl	$.L.str.92, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%r14), %rax
	movl	$0, (%rax,%rbx,4)
.LBB30_28:                              #   in Loop: Header=BB30_17 Depth=1
	movl	$.L.str.21, %esi
	movq	%r14, %rdi
	callq	messageFindArgument
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB30_30
# BB#29:                                # %messageGetFilename.exit
                                        #   in Loop: Header=BB30_17 Depth=1
	movl	$.L.str.22, %esi
	movq	%r14, %rdi
	callq	messageFindArgument
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB30_35
.LBB30_30:                              # %messageGetFilename.exit.thread
                                        #   in Loop: Header=BB30_17 Depth=1
	testl	%r15d, %r15d
	jne	.LBB30_32
# BB#31:                                #   in Loop: Header=BB30_17 Depth=1
	movl	$.L.str.34, %esi
	movq	%r14, %rdi
	callq	messageSetEncoding
.LBB30_32:                              #   in Loop: Header=BB30_17 Depth=1
	cmpb	$0, (%rbp)
	movl	$.L.str.87, %edx
	cmovneq	%rbp, %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	*40(%rsp)               # 8-byte Folded Reload
	movq	56(%r14), %rbx
	movq	%rbp, %rdi
	callq	free
	testq	%rbx, %rbx
	jne	.LBB30_36
	jmp	.LBB30_104
.LBB30_33:                              # %.critedge425
                                        #   in Loop: Header=BB30_17 Depth=1
	movl	$.L.str.87, %edx
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	*40(%rsp)               # 8-byte Folded Reload
.LBB30_34:                              # %.thread450
                                        #   in Loop: Header=BB30_17 Depth=1
	movq	8(%rbp), %rbx
	movq	$0, 112(%r14)
	movl	$6, %r15d
	testq	%rbx, %rbx
	jne	.LBB30_36
	jmp	.LBB30_104
.LBB30_35:                              # %.thread456
                                        #   in Loop: Header=BB30_17 Depth=1
	movl	$.L.str.93, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.86, %esi
	movq	%r14, %rdi
	callq	messageAddArgument
	movl	$.L.str.87, %edx
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	*40(%rsp)               # 8-byte Folded Reload
	movq	56(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB30_104
	.p2align	4, 0x90
.LBB30_36:                              #   in Loop: Header=BB30_17 Depth=1
	testl	%r15d, %r15d
	je	.LBB30_53
# BB#37:                                #   in Loop: Header=BB30_17 Depth=1
	cmpl	$6, %r15d
	movl	%r15d, 24(%rsp)         # 4-byte Spill
	jne	.LBB30_55
# BB#38:                                # %.preheader.split.us.preheader
                                        #   in Loop: Header=BB30_17 Depth=1
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB30_39:                              # %.preheader.split.us
                                        #   Parent Loop BB30_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rdi
	callq	lineGetData
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB30_52
# BB#40:                                #   in Loop: Header=BB30_39 Depth=2
	movl	$.L.str.49, %esi
	movl	$6, %edx
	movq	%r13, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB30_73
# BB#41:                                # %.thread457.us
                                        #   in Loop: Header=BB30_39 Depth=2
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rbp
	addq	$2, %rbp
	cmpq	$1024, %rbp             # imm = 0x400
	jb	.LBB30_43
# BB#42:                                #   in Loop: Header=BB30_39 Depth=2
	movq	%rbp, %rdi
	callq	cli_malloc
	movq	%rax, %r15
	movq	%r15, %r12
	jmp	.LBB30_44
	.p2align	4, 0x90
.LBB30_43:                              #   in Loop: Header=BB30_39 Depth=2
	leaq	80(%rsp), %r15
	xorl	%r12d, %r12d
	movl	$1024, %ebp             # imm = 0x400
.LBB30_44:                              # %.thread458.us
                                        #   in Loop: Header=BB30_39 Depth=2
	movl	$6, %esi
	movq	%r14, %rdi
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%rbp, %r8
	callq	decodeLine
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB30_71
# BB#45:                                #   in Loop: Header=BB30_39 Depth=2
	subq	%r15, %rbp
	je	.LBB30_47
# BB#46:                                #   in Loop: Header=BB30_39 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	*64(%rsp)               # 8-byte Folded Reload
	addq	%rbp, 48(%rsp)          # 8-byte Folded Spill
.LBB30_47:                              #   in Loop: Header=BB30_39 Depth=2
	cmpq	%r12, %r15
	jne	.LBB30_49
# BB#48:                                #   in Loop: Header=BB30_39 Depth=2
	movq	%r15, %rdi
	callq	free
.LBB30_49:                              #   in Loop: Header=BB30_39 Depth=2
	cmpl	$0, 1184(%rsp)
	je	.LBB30_52
# BB#50:                                #   in Loop: Header=BB30_39 Depth=2
	movl	16(%r14), %eax
	decl	%eax
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	jne	.LBB30_52
# BB#51:                                #   in Loop: Header=BB30_39 Depth=2
	movq	(%rbx), %rdi
	callq	lineUnlink
	movq	$0, (%rbx)
	.p2align	4, 0x90
.LBB30_52:                              #   in Loop: Header=BB30_39 Depth=2
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB30_39
	jmp	.LBB30_73
	.p2align	4, 0x90
.LBB30_53:                              #   in Loop: Header=BB30_17 Depth=1
	movl	16(%r14), %eax
	decl	%eax
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	je	.LBB30_137
# BB#54:                                #   in Loop: Header=BB30_17 Depth=1
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	*1168(%rsp)
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB30_77
	.p2align	4, 0x90
.LBB30_55:                              # %.preheader.split.preheader
                                        #   in Loop: Header=BB30_17 Depth=1
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB30_56:                              # %.preheader.split
                                        #   Parent Loop BB30_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rdi
	callq	lineGetData
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB30_59
# BB#57:                                # %.thread457
                                        #   in Loop: Header=BB30_56 Depth=2
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %r13
	addq	$2, %r13
	movb	$1, %r14b
	cmpq	$1024, %r13             # imm = 0x400
	jb	.LBB30_60
# BB#58:                                #   in Loop: Header=BB30_56 Depth=2
	movq	%r13, %rdi
	callq	cli_malloc
	movq	%rax, %r15
	movq	%r15, %r12
	jmp	.LBB30_61
	.p2align	4, 0x90
.LBB30_59:                              #   in Loop: Header=BB30_56 Depth=2
	xorl	%r14d, %r14d
.LBB30_60:                              #   in Loop: Header=BB30_56 Depth=2
	leaq	80(%rsp), %r15
	xorl	%r12d, %r12d
	movl	$1024, %r13d            # imm = 0x400
.LBB30_61:                              # %.thread458
                                        #   in Loop: Header=BB30_56 Depth=2
	movl	24(%rsp), %esi          # 4-byte Reload
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rdx
	movq	%r15, %rcx
	movq	%r13, %r8
	callq	decodeLine
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB30_70
# BB#62:                                #   in Loop: Header=BB30_56 Depth=2
	subq	%r15, %rbp
	je	.LBB30_64
# BB#63:                                #   in Loop: Header=BB30_56 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	*64(%rsp)               # 8-byte Folded Reload
	addq	%rbp, 48(%rsp)          # 8-byte Folded Spill
.LBB30_64:                              #   in Loop: Header=BB30_56 Depth=2
	cmpq	%r12, %r15
	jne	.LBB30_66
# BB#65:                                #   in Loop: Header=BB30_56 Depth=2
	movq	%r15, %rdi
	callq	free
.LBB30_66:                              #   in Loop: Header=BB30_56 Depth=2
	cmpl	$0, 1184(%rsp)
	setne	%al
	andb	%r14b, %al
	cmpb	$1, %al
	movq	56(%rsp), %r14          # 8-byte Reload
	jne	.LBB30_69
# BB#67:                                #   in Loop: Header=BB30_56 Depth=2
	movl	16(%r14), %eax
	decl	%eax
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	jne	.LBB30_69
# BB#68:                                #   in Loop: Header=BB30_56 Depth=2
	movq	(%rbx), %rdi
	callq	lineUnlink
	movq	$0, (%rbx)
.LBB30_69:                              #   in Loop: Header=BB30_56 Depth=2
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB30_56
	jmp	.LBB30_73
.LBB30_70:                              #   in Loop: Header=BB30_17 Depth=1
	movq	56(%rsp), %r14          # 8-byte Reload
.LBB30_71:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB30_17 Depth=1
	cmpq	%r12, %r15
	jne	.LBB30_73
# BB#72:                                #   in Loop: Header=BB30_17 Depth=1
	movq	%r15, %rdi
	callq	free
	.p2align	4, 0x90
.LBB30_73:                              # %.loopexit469
                                        #   in Loop: Header=BB30_17 Depth=1
	movl	$.L.str.95, %edi
	xorl	%eax, %eax
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	24(%rsp), %edx          # 4-byte Reload
	callq	cli_dbgmsg
	movl	84(%r14), %esi
	testl	%esi, %esi
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB30_77
# BB#74:                                #   in Loop: Header=BB30_17 Depth=1
	movl	$.L.str.40, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$0, 84(%r14)
	je	.LBB30_77
# BB#75:                                # %base64Flush.exit
                                        #   in Loop: Header=BB30_17 Depth=1
	xorl	%esi, %esi
	movl	$base64, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	leaq	80(%rsp), %rdx
	callq	decode
	movl	$0, 84(%r14)
	testq	%rax, %rax
	je	.LBB30_77
# BB#76:                                #   in Loop: Header=BB30_17 Depth=1
	leaq	80(%rsp), %rsi
	subq	%rsi, %rax
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rax, %rdx
	callq	*64(%rsp)               # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB30_77:                              #   in Loop: Header=BB30_17 Depth=1
	incq	%rbx
	movslq	16(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB30_17
	jmp	.LBB30_109
.LBB30_78:
	xorl	%ebx, %ebx
.LBB30_79:                              # %.critedge
	xorl	%edx, %edx
	cmpl	$1, 16(%r14)
	jne	.LBB30_81
# BB#80:
	movq	8(%r14), %rax
	xorl	%edx, %edx
	cmpl	$8, (%rax)
	cmovel	1184(%rsp), %edx
.LBB30_81:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	textToBlob
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB30_100
# BB#82:
	movq	%rbx, %rdi
	callq	blobGetData
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB30_101
# BB#83:
	movq	%rbx, %rdi
	callq	blobGetDataSize
	movq	%rax, %rbp
	cmpb	$58, (%r13)
	jne	.LBB30_103
# BB#84:
	movl	$.L.str.73, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB30_102
# BB#85:
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	xorl	%ecx, %ecx
	cmpq	$2, %rbp
	jb	.LBB30_112
# BB#86:                                # %.lr.ph502.preheader
	movl	$1, %eax
	xorl	%r12d, %r12d
.LBB30_87:                              # %.lr.ph502
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r15,%rax), %esi
	cmpq	$10, %rsi
	je	.LBB30_99
# BB#88:                                # %.lr.ph502
                                        #   in Loop: Header=BB30_87 Depth=1
	cmpb	$13, %sil
	je	.LBB30_99
# BB#89:                                # %.lr.ph502
                                        #   in Loop: Header=BB30_87 Depth=1
	cmpb	$58, %sil
	je	.LBB30_113
# BB#90:                                #   in Loop: Header=BB30_87 Depth=1
	cmpb	$32, %sil
	jb	.LBB30_149
# BB#91:                                #   in Loop: Header=BB30_87 Depth=1
	testb	%sil, %sil
	js	.LBB30_149
# BB#92:                                #   in Loop: Header=BB30_87 Depth=1
	movzbl	messageExport.hqxtbl(%rsi), %edx
	cmpb	$-1, %dl
	je	.LBB30_149
# BB#93:                                #   in Loop: Header=BB30_87 Depth=1
	cmpl	$3, %ecx
	ja	.LBB30_99
# BB#94:                                #   in Loop: Header=BB30_87 Depth=1
	movl	%ecx, %ecx
	jmpq	*.LJTI30_0(,%rcx,8)
.LBB30_95:                              #   in Loop: Header=BB30_87 Depth=1
	shlb	$2, %dl
	movb	%dl, (%r13,%r12)
	movl	$1, %ecx
	jmp	.LBB30_99
.LBB30_96:                              #   in Loop: Header=BB30_87 Depth=1
	movl	%edx, %ecx
	shrb	$4, %cl
	andb	$3, %cl
	orb	%cl, (%r13,%r12)
	shlb	$4, %dl
	movb	%dl, 1(%r13,%r12)
	incq	%r12
	movl	$2, %ecx
	jmp	.LBB30_99
.LBB30_97:                              #   in Loop: Header=BB30_87 Depth=1
	movl	%edx, %ecx
	shrb	$2, %cl
	andb	$15, %cl
	orb	%cl, (%r13,%r12)
	shlb	$6, %dl
	movb	%dl, 1(%r13,%r12)
	incq	%r12
	movl	$3, %ecx
	jmp	.LBB30_99
.LBB30_98:                              #   in Loop: Header=BB30_87 Depth=1
	andb	$63, %dl
	orb	%dl, (%r13,%r12)
	incq	%r12
	xorl	%ecx, %ecx
.LBB30_99:                              #   in Loop: Header=BB30_87 Depth=1
	incq	%rax
	cmpq	%rbp, %rax
	jb	.LBB30_87
	jmp	.LBB30_113
.LBB30_100:
	movl	$.L.str.71, %edi
	jmp	.LBB30_105
.LBB30_101:
	movl	$.L.str.72, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB30_102:
	movq	%rbx, %rdi
	callq	blobDestroy
	jmp	.LBB30_106
.LBB30_103:
	movl	$.L.str.76, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB30_114
.LBB30_104:
	movl	$.L.str.94, %edi
.LBB30_105:                             # %.thread433
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB30_106:                             # %.thread433
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	*16(%rsp)               # 8-byte Folded Reload
.LBB30_107:
	xorl	%eax, %eax
.LBB30_108:                             # %.thread433
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB30_109:                             # %.thread433
	movq	(%rsp), %rax            # 8-byte Reload
	addq	$1112, %rsp             # imm = 0x458
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB30_110:
	movq	56(%r14), %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	1184(%rsp), %edx
	callq	*1168(%rsp)
	jmp	.LBB30_108
.LBB30_111:
	movl	$.L.str.88, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB30_109
.LBB30_112:
	xorl	%r12d, %r12d
.LBB30_113:                             # %.thread
	movl	$.L.str.75, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	free
	movq	%r12, %rbp
.LBB30_114:
	movl	$144, %esi
	movq	%r13, %rdi
	movq	%rbp, %rdx
	callq	memchr
	testq	%rax, %rax
	je	.LBB30_127
# BB#115:
	callq	blobCreate
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB30_136
# BB#116:                               # %.preheader472
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	testq	%rbp, %rbp
	je	.LBB30_126
# BB#117:                               # %.lr.ph498
	leaq	-1(%rbp), %r12
	xorl	%r14d, %r14d
	leaq	80(%rsp), %rbx
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB30_118:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_122 Depth 2
	movb	(%r13,%r14), %al
	movb	%al, 80(%rsp)
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	blobAddData
	cmpq	%r12, %r14
	jae	.LBB30_125
# BB#119:                               #   in Loop: Header=BB30_118 Depth=1
	cmpb	$-112, 1(%r13,%r14)
	jne	.LBB30_125
# BB#120:                               #   in Loop: Header=BB30_118 Depth=1
	movzbl	2(%r13,%r14), %ebp
	addq	$2, %r14
	testq	%rbp, %rbp
	je	.LBB30_123
# BB#121:                               #   in Loop: Header=BB30_118 Depth=1
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	blobGrow
	cmpb	$1, %bpl
	je	.LBB30_124
	.p2align	4, 0x90
.LBB30_122:                             # %.lr.ph496
                                        #   Parent Loop BB30_118 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%ebp
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	blobAddData
	cmpl	$1, %ebp
	jg	.LBB30_122
	jmp	.LBB30_124
.LBB30_123:                             #   in Loop: Header=BB30_118 Depth=1
	movb	$-112, 80(%rsp)
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	blobAddData
.LBB30_124:                             #   in Loop: Header=BB30_118 Depth=1
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB30_125:                             # %.loopexit471
                                        #   in Loop: Header=BB30_118 Depth=1
	incq	%r14
	cmpq	%rbp, %r14
	jb	.LBB30_118
.LBB30_126:                             # %.thread431
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	blobDestroy
	movq	%r15, %rdi
	callq	blobGetData
	movq	%rax, %r13
	movq	%r15, %rdi
	callq	blobGetDataSize
	movq	%rax, %rbx
	movl	$.L.str.77, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	cli_dbgmsg
	movq	%rbx, %rbp
	movq	56(%rsp), %r14          # 8-byte Reload
	testq	%rbp, %rbp
	jne	.LBB30_128
	jmp	.LBB30_134
.LBB30_127:
	movl	$.L.str.78, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	%rbx, %r15
	testq	%rbp, %rbp
	je	.LBB30_134
.LBB30_128:
	movzbl	(%r13), %r12d
	cmpq	%rbp, %r12
	jae	.LBB30_135
# BB#129:
	leaq	1(%r12), %rdi
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB30_135
# BB#130:
	leaq	1(%r13), %rsi
	movq	%rbp, %rdi
	movq	%r12, %rdx
	callq	memcpy
	movb	$0, (%rbp,%r12)
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	callq	*40(%rsp)               # 8-byte Folded Reload
	leaq	6(%r12), %rdi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB30_132
# BB#131:
	movl	$.L.str.80, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	sprintf
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	messageAddArgument
	movq	%rbx, %rdi
	callq	free
.LBB30_132:
	movl	%r12d, %eax
	addb	$12, %al
	movzbl	%al, %eax
	movzbl	(%r13,%rax), %ecx
	shlq	$24, %rcx
	movzbl	1(%r13,%rax), %edx
	shlq	$16, %rdx
	orq	%rcx, %rdx
	movzbl	2(%r13,%rax), %ecx
	shlq	$8, %rcx
	orq	%rdx, %rcx
	movq	%rbp, %rbx
	movzbl	3(%r13,%rax), %ebp
	orq	%rcx, %rbp
	movzbl	4(%r13,%rax), %ecx
	shlq	$24, %rcx
	movzbl	5(%r13,%rax), %edx
	shlq	$16, %rdx
	orq	%rcx, %rdx
	movzbl	6(%r13,%rax), %esi
	shlq	$8, %rsi
	orq	%rdx, %rsi
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movzbl	7(%r13,%rax), %ecx
	orq	%rsi, %rcx
	movl	$.L.str.81, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	free
	addb	$22, %r12b
	movq	%r15, %rdi
	callq	blobGetDataSize
	movq	%rax, %rbx
	movzbl	%r12b, %r14d
	subq	%r14, %rbx
	cmpq	%rbp, %rbx
	jae	.LBB30_139
# BB#133:
	movl	$.L.str.82, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	cli_warnmsg
	jmp	.LBB30_140
.LBB30_134:
	movl	$.L.str.79, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB30_135:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	*16(%rsp)               # 8-byte Folded Reload
	movq	%r15, %rdi
	callq	blobDestroy
	jmp	.LBB30_107
.LBB30_136:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	*16(%rsp)               # 8-byte Folded Reload
	movq	%rbx, %rdi
	callq	blobDestroy
	jmp	.LBB30_107
.LBB30_137:
	movq	%rbx, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	1184(%rsp), %edx
	callq	*1168(%rsp)
	jmp	.LBB30_109
.LBB30_138:                             # %.critedge424
	movl	$.L.str.85, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.86, %esi
	movq	%r14, %rdi
	callq	messageAddArgument
	movl	$.L.str.87, %edx
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	*40(%rsp)               # 8-byte Folded Reload
	jmp	.LBB30_11
.LBB30_139:
	movq	%rbp, %rbx
.LBB30_140:
	movq	1176(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB30_143
# BB#141:
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	72(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB30_143
# BB#142:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	*%rbp
.LBB30_143:
	movq	8(%rsp), %rsi           # 8-byte Reload
	addq	%r14, %rsi
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbx, %rdx
	callq	*64(%rsp)               # 8-byte Folded Reload
	movq	%r15, %rdi
	callq	blobDestroy
	cmpl	$0, 1184(%rsp)
	movq	56(%rsp), %r14          # 8-byte Reload
	je	.LBB30_145
# BB#144:
	movq	$0, 104(%r14)
.LBB30_145:
	movl	16(%r14), %eax
	testl	%eax, %eax
	je	.LBB30_148
# BB#146:
	cmpl	$1, %eax
	jne	.LBB30_12
# BB#147:
	movq	8(%r14), %rcx
	movl	$1, %eax
	cmpl	$8, (%rcx)
	jne	.LBB30_12
.LBB30_148:
	movl	$.L.str.83, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB30_109
.LBB30_149:
	movl	$.L.str.74, %edi
	xorl	%eax, %eax
	movl	%esi, %edx
	callq	cli_warnmsg
	jmp	.LBB30_113
.Lfunc_end30:
	.size	messageExport, .Lfunc_end30-messageExport
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI30_0:
	.quad	.LBB30_95
	.quad	.LBB30_96
	.quad	.LBB30_97
	.quad	.LBB30_98

	.text
	.globl	messageToBlob
	.p2align	4, 0x90
	.type	messageToBlob,@function
messageToBlob:                          # @messageToBlob
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi217:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi218:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi219:
	.cfi_def_cfa_offset 32
.Lcfi220:
	.cfi_offset %rbx, -32
.Lcfi221:
	.cfi_offset %r14, -24
.Lcfi222:
	.cfi_offset %r15, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	subq	$8, %rsp
.Lcfi223:
	.cfi_adjust_cfa_offset 8
	movl	$0, %esi
	movl	$blobCreate, %edx
	movl	$blobDestroy, %ecx
	movl	$blobSetFilename, %r8d
	movl	$blobAddData, %r9d
	movq	%rbx, %rdi
	pushq	%r14
.Lcfi224:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi225:
	.cfi_adjust_cfa_offset 8
	pushq	$textToBlob
.Lcfi226:
	.cfi_adjust_cfa_offset 8
	callq	messageExport
	addq	$32, %rsp
.Lcfi227:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r15
	testl	%r14d, %r14d
	je	.LBB31_3
# BB#1:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB31_3
# BB#2:
	addq	$56, %rbx
	callq	textDestroy
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
.LBB31_3:
	movq	%r15, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end31:
	.size	messageToBlob, .Lfunc_end31-messageToBlob
	.cfi_endproc

	.globl	messageToText
	.p2align	4, 0x90
	.type	messageToText,@function
messageToText:                          # @messageToText
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi228:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi229:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi230:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi231:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi232:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi233:
	.cfi_def_cfa_offset 56
	subq	$1064, %rsp             # imm = 0x428
.Lcfi234:
	.cfi_def_cfa_offset 1120
.Lcfi235:
	.cfi_offset %rbx, -56
.Lcfi236:
	.cfi_offset %r12, -48
.Lcfi237:
	.cfi_offset %r13, -40
.Lcfi238:
	.cfi_offset %r14, -32
.Lcfi239:
	.cfi_offset %r15, -24
.Lcfi240:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	16(%r14), %eax
	testl	%eax, %eax
	je	.LBB32_63
# BB#1:                                 # %.preheader
	testl	%eax, %eax
	jle	.LBB32_82
# BB#2:                                 # %.lr.ph179
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	movq	%r14, 24(%rsp)          # 8-byte Spill
.LBB32_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_7 Depth 2
                                        #     Child Loop BB32_35 Depth 2
                                        #     Child Loop BB32_20 Depth 2
	movq	8(%r14), %rax
	movl	(%rax,%r12,4), %ebp
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	movl	%ebp, %edx
	callq	cli_dbgmsg
	cmpq	$6, %rbp
	ja	.LBB32_15
# BB#4:                                 #   in Loop: Header=BB32_3 Depth=1
	jmpq	*.LJTI32_0(,%rbp,8)
.LBB32_5:                               #   in Loop: Header=BB32_3 Depth=1
	movq	56(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB32_60
# BB#6:                                 # %.lr.ph167.preheader
                                        #   in Loop: Header=BB32_3 Depth=1
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB32_7:                               # %.lr.ph167
                                        #   Parent Loop BB32_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %r13
	testq	%r15, %r15
	je	.LBB32_9
# BB#8:                                 #   in Loop: Header=BB32_7 Depth=2
	movq	%r13, 8(%rbx)
	testq	%r13, %r13
	jne	.LBB32_10
	jmp	.LBB32_73
	.p2align	4, 0x90
.LBB32_9:                               #   in Loop: Header=BB32_7 Depth=2
	movq	%r13, %r15
	testq	%r13, %r13
	je	.LBB32_73
.LBB32_10:                              #   in Loop: Header=BB32_7 Depth=2
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB32_12
# BB#11:                                #   in Loop: Header=BB32_7 Depth=2
	callq	lineLink
	jmp	.LBB32_13
	.p2align	4, 0x90
.LBB32_12:                              #   in Loop: Header=BB32_7 Depth=2
	xorl	%eax, %eax
.LBB32_13:                              #   in Loop: Header=BB32_7 Depth=2
	movq	%rax, (%r13)
	movq	8(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%r13, %rbx
	jne	.LBB32_7
	jmp	.LBB32_60
.LBB32_14:                              #   in Loop: Header=BB32_3 Depth=1
	cmpq	$0, 112(%r14)
	je	.LBB32_85
.LBB32_15:                              #   in Loop: Header=BB32_3 Depth=1
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	%r12, 16(%rsp)          # 8-byte Spill
	testq	%r12, %r12
	jne	.LBB32_18
# BB#16:                                #   in Loop: Header=BB32_3 Depth=1
	cmpq	$0, 104(%r14)
	je	.LBB32_18
# BB#17:                                #   in Loop: Header=BB32_3 Depth=1
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB32_18:                              #   in Loop: Header=BB32_3 Depth=1
	movq	56(%r14), %r12
	testq	%r12, %r12
	je	.LBB32_48
# BB#19:                                # %.lr.ph173
                                        #   in Loop: Header=BB32_3 Depth=1
	cmpl	$2, %ebp
	jne	.LBB32_33
	.p2align	4, 0x90
.LBB32_20:                              # %.lr.ph173.split.us
                                        #   Parent Loop BB32_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %rdi
	callq	lineGetData
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB32_32
# BB#21:                                #   in Loop: Header=BB32_20 Depth=2
	movl	$2, %esi
	movl	$1024, %r8d             # imm = 0x400
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rdx
	leaq	32(%rsp), %rbx
	movq	%rbx, %rcx
	callq	decodeLine
	testq	%rax, %rax
	je	.LBB32_46
# BB#22:                                #   in Loop: Header=BB32_20 Depth=2
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %r15
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB32_24
# BB#23:                                #   in Loop: Header=BB32_20 Depth=2
	movq	%r15, 8(%r13)
	testq	%r15, %r15
	jne	.LBB32_25
	jmp	.LBB32_47
	.p2align	4, 0x90
.LBB32_24:                              #   in Loop: Header=BB32_20 Depth=2
	movq	%r15, 8(%rsp)           # 8-byte Spill
	testq	%r15, %r15
	je	.LBB32_47
.LBB32_25:                              #   in Loop: Header=BB32_20 Depth=2
	movzbl	32(%rsp), %eax
	testb	%al, %al
	je	.LBB32_29
# BB#26:                                #   in Loop: Header=BB32_20 Depth=2
	cmpb	$10, %al
	movl	$0, %eax
	je	.LBB32_31
# BB#27:                                #   in Loop: Header=BB32_20 Depth=2
	movq	%rbp, %rdi
	callq	strlen
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB32_30
# BB#28:                                #   in Loop: Header=BB32_20 Depth=2
	movq	%rbx, %rdi
	callq	lineCreate
	jmp	.LBB32_31
.LBB32_29:                              #   in Loop: Header=BB32_20 Depth=2
	xorl	%eax, %eax
	jmp	.LBB32_31
.LBB32_30:                              #   in Loop: Header=BB32_20 Depth=2
	movq	(%r12), %rdi
	callq	lineLink
	.p2align	4, 0x90
.LBB32_31:                              #   in Loop: Header=BB32_20 Depth=2
	movq	%rax, (%r15)
	movl	$61, %esi
	movq	%rbp, %rdi
	callq	strchr
	testq	%rax, %rax
	movq	%r15, %r13
	jne	.LBB32_47
.LBB32_32:                              #   in Loop: Header=BB32_20 Depth=2
	movq	8(%r12), %r12
	testq	%r12, %r12
	jne	.LBB32_20
	jmp	.LBB32_48
.LBB32_33:                              #   in Loop: Header=BB32_3 Depth=1
	movq	%r13, %r15
	jmp	.LBB32_35
.LBB32_34:                              #   in Loop: Header=BB32_35 Depth=2
	movq	(%r12), %rdi
	callq	lineLink
	jmp	.LBB32_45
	.p2align	4, 0x90
.LBB32_35:                              # %.lr.ph173.split
                                        #   Parent Loop BB32_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %rdi
	callq	lineGetData
	movq	%rax, %r14
	movl	$1024, %r8d             # imm = 0x400
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	movq	%r14, %rdx
	leaq	32(%rsp), %rcx
	callq	decodeLine
	testq	%rax, %rax
	je	.LBB32_47
# BB#36:                                #   in Loop: Header=BB32_35 Depth=2
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %r13
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB32_38
# BB#37:                                #   in Loop: Header=BB32_35 Depth=2
	movq	%r13, 8(%r15)
	testq	%r13, %r13
	jne	.LBB32_39
	jmp	.LBB32_46
	.p2align	4, 0x90
.LBB32_38:                              #   in Loop: Header=BB32_35 Depth=2
	movq	%r13, 8(%rsp)           # 8-byte Spill
	testq	%r13, %r13
	je	.LBB32_46
.LBB32_39:                              #   in Loop: Header=BB32_35 Depth=2
	movzbl	32(%rsp), %eax
	testb	%al, %al
	je	.LBB32_44
# BB#40:                                #   in Loop: Header=BB32_35 Depth=2
	cmpb	$10, %al
	movl	$0, %eax
	je	.LBB32_45
# BB#41:                                #   in Loop: Header=BB32_35 Depth=2
	testq	%r14, %r14
	je	.LBB32_43
# BB#42:                                #   in Loop: Header=BB32_35 Depth=2
	movq	%r14, %rdi
	callq	strlen
	leaq	32(%rsp), %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB32_34
.LBB32_43:                              #   in Loop: Header=BB32_35 Depth=2
	leaq	32(%rsp), %rdi
	callq	lineCreate
	jmp	.LBB32_45
	.p2align	4, 0x90
.LBB32_44:                              #   in Loop: Header=BB32_35 Depth=2
	xorl	%eax, %eax
.LBB32_45:                              #   in Loop: Header=BB32_35 Depth=2
	movq	%rax, (%r13)
	movq	8(%r12), %r12
	testq	%r12, %r12
	movq	%r13, %r15
	jne	.LBB32_35
	jmp	.LBB32_48
.LBB32_46:                              #   in Loop: Header=BB32_3 Depth=1
	movq	%r13, %r15
.LBB32_47:                              # %.thread
                                        #   in Loop: Header=BB32_3 Depth=1
	movq	%r15, %r13
.LBB32_48:                              # %.loopexit
                                        #   in Loop: Header=BB32_3 Depth=1
	movq	24(%rsp), %r14          # 8-byte Reload
	cmpl	$0, 84(%r14)
	je	.LBB32_53
# BB#49:                                #   in Loop: Header=BB32_3 Depth=1
	movl	$0, 32(%rsp)
	xorl	%esi, %esi
	movl	$base64, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	leaq	32(%rsp), %rdx
	callq	decode
	testq	%rax, %rax
	je	.LBB32_54
# BB#50:                                #   in Loop: Header=BB32_3 Depth=1
	movb	32(%rsp), %al
	testb	%al, %al
	je	.LBB32_54
# BB#51:                                #   in Loop: Header=BB32_3 Depth=1
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %rbp
	movq	8(%rsp), %r15           # 8-byte Reload
	testq	%r15, %r15
	je	.LBB32_55
# BB#52:                                #   in Loop: Header=BB32_3 Depth=1
	movq	%rbp, 8(%r13)
	jmp	.LBB32_56
.LBB32_53:                              #   in Loop: Header=BB32_3 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB32_60
.LBB32_54:                              #   in Loop: Header=BB32_3 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB32_59
.LBB32_55:                              #   in Loop: Header=BB32_3 Depth=1
	movq	%rbp, %r15
.LBB32_56:                              #   in Loop: Header=BB32_3 Depth=1
	testq	%rbp, %rbp
	movq	16(%rsp), %r12          # 8-byte Reload
	je	.LBB32_58
# BB#57:                                #   in Loop: Header=BB32_3 Depth=1
	leaq	32(%rsp), %rdi
	callq	lineCreate
	movq	%rax, (%rbp)
.LBB32_58:                              #   in Loop: Header=BB32_3 Depth=1
	movq	%rbp, %r13
.LBB32_59:                              #   in Loop: Header=BB32_3 Depth=1
	movl	$0, 84(%r14)
.LBB32_60:                              # %.loopexit147
                                        #   in Loop: Header=BB32_3 Depth=1
	incq	%r12
	movslq	16(%r14), %rax
	cmpq	%rax, %r12
	jl	.LBB32_3
# BB#61:                                # %._crit_edge180
	testq	%r13, %r13
	je	.LBB32_83
# BB#62:
	movq	$0, 8(%r13)
	jmp	.LBB32_83
.LBB32_63:
	movq	56(%r14), %rbp
	xorl	%r15d, %r15d
	testq	%rbp, %rbp
	je	.LBB32_83
# BB#64:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB32_65:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %r14
	testq	%r15, %r15
	je	.LBB32_67
# BB#66:                                #   in Loop: Header=BB32_65 Depth=1
	movq	%r14, 8(%rbx)
	testq	%r14, %r14
	jne	.LBB32_68
	jmp	.LBB32_76
	.p2align	4, 0x90
.LBB32_67:                              #   in Loop: Header=BB32_65 Depth=1
	movq	%r14, %r15
	testq	%r14, %r14
	je	.LBB32_76
.LBB32_68:                              #   in Loop: Header=BB32_65 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB32_70
# BB#69:                                #   in Loop: Header=BB32_65 Depth=1
	callq	lineLink
	jmp	.LBB32_71
	.p2align	4, 0x90
.LBB32_70:                              #   in Loop: Header=BB32_65 Depth=1
	xorl	%eax, %eax
.LBB32_71:                              #   in Loop: Header=BB32_65 Depth=1
	movq	%rax, (%r14)
	movq	8(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%r14, %rbx
	jne	.LBB32_65
# BB#72:                                # %._crit_edge
	movq	$0, 8(%r14)
	jmp	.LBB32_83
.LBB32_73:
	testq	%r15, %r15
	je	.LBB32_82
# BB#74:
	movq	$0, 8
	jmp	.LBB32_81
.LBB32_76:
	testq	%r15, %r15
	jne	.LBB32_81
	jmp	.LBB32_82
.LBB32_79:
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	testq	%r15, %r15
	je	.LBB32_82
.LBB32_80:
	movq	$0, 8(%r13)
.LBB32_81:
	movq	%r15, %rdi
	callq	textDestroy
.LBB32_82:                              # %.thread135
	xorl	%r15d, %r15d
.LBB32_83:                              # %.thread135
	movq	%r15, %rax
	addq	$1064, %rsp             # imm = 0x428
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB32_85:
	testq	%r15, %r15
	jne	.LBB32_80
	jmp	.LBB32_82
.Lfunc_end32:
	.size	messageToText, .Lfunc_end32-messageToText
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI32_0:
	.quad	.LBB32_5
	.quad	.LBB32_15
	.quad	.LBB32_15
	.quad	.LBB32_5
	.quad	.LBB32_5
	.quad	.LBB32_79
	.quad	.LBB32_14

	.text
	.globl	yEncBegin
	.p2align	4, 0x90
	.type	yEncBegin,@function
yEncBegin:                              # @yEncBegin
	.cfi_startproc
# BB#0:
	movq	112(%rdi), %rax
	retq
.Lfunc_end33:
	.size	yEncBegin, .Lfunc_end33-yEncBegin
	.cfi_endproc

	.globl	binhexBegin
	.p2align	4, 0x90
	.type	binhexBegin,@function
binhexBegin:                            # @binhexBegin
	.cfi_startproc
# BB#0:
	movq	104(%rdi), %rax
	retq
.Lfunc_end34:
	.size	binhexBegin, .Lfunc_end34-binhexBegin
	.cfi_endproc

	.globl	decodeLine
	.p2align	4, 0x90
	.type	decodeLine,@function
decodeLine:                             # @decodeLine
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi241:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi242:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi243:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi244:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi245:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi246:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi247:
	.cfi_def_cfa_offset 160
.Lcfi248:
	.cfi_offset %rbx, -56
.Lcfi249:
	.cfi_offset %r12, -48
.Lcfi250:
	.cfi_offset %r13, -40
.Lcfi251:
	.cfi_offset %r14, -32
.Lcfi252:
	.cfi_offset %r15, -24
.Lcfi253:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movq	%rcx, %r12
	movq	%rdx, %r14
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r15
	decl	%esi
	cmpl	$5, %esi
	ja	.LBB35_2
# BB#1:
	jmpq	*.LJTI35_0(,%rsi,8)
.LBB35_5:
	testq	%r14, %r14
	je	.LBB35_31
# BB#6:                                 # %.preheader154
	testq	%r13, %r13
	je	.LBB35_31
	.p2align	4, 0x90
.LBB35_7:                               # %.lr.ph205
                                        # =>This Inner Loop Header: Depth=1
	movb	(%r14), %al
	cmpb	$61, %al
	je	.LBB35_9
# BB#8:                                 # %.lr.ph205
                                        #   in Loop: Header=BB35_7 Depth=1
	testb	%al, %al
	jne	.LBB35_30
	jmp	.LBB35_31
	.p2align	4, 0x90
.LBB35_9:                               #   in Loop: Header=BB35_7 Depth=1
	movsbl	1(%r14), %ebp
	testl	%ebp, %ebp
	je	.LBB35_32
# BB#10:                                #   in Loop: Header=BB35_7 Depth=1
	cmpb	$10, %bpl
	je	.LBB35_32
# BB#11:                                #   in Loop: Header=BB35_7 Depth=1
	movslq	%ebp, %r15
	callq	__ctype_b_loc
	movq	%rax, %rbx
	movq	(%rbx), %rax
	testb	$8, 1(%rax,%r15,2)
	jne	.LBB35_12
# BB#13:                                #   in Loop: Header=BB35_7 Depth=1
	movl	%ebp, %eax
	addb	$-65, %al
	cmpb	$5, %al
	ja	.LBB35_15
# BB#14:                                #   in Loop: Header=BB35_7 Depth=1
	addb	$-55, %bpl
	movb	%bpl, %r15b
	jmp	.LBB35_18
	.p2align	4, 0x90
.LBB35_12:                              #   in Loop: Header=BB35_7 Depth=1
	addb	$-48, %r15b
	jmp	.LBB35_18
.LBB35_15:                              #   in Loop: Header=BB35_7 Depth=1
	movl	%r15d, %eax
	addb	$-97, %al
	cmpb	$5, %al
	ja	.LBB35_17
# BB#16:                                #   in Loop: Header=BB35_7 Depth=1
	addb	$-87, %r15b
	jmp	.LBB35_18
.LBB35_17:                              #   in Loop: Header=BB35_7 Depth=1
	movl	$.L.str.97, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	movb	$61, %r15b
	.p2align	4, 0x90
.LBB35_18:                              # %hex.exit
                                        #   in Loop: Header=BB35_7 Depth=1
	movsbl	2(%r14), %esi
	cmpl	$10, %esi
	je	.LBB35_20
# BB#19:                                # %hex.exit
                                        #   in Loop: Header=BB35_7 Depth=1
	testb	%sil, %sil
	je	.LBB35_20
# BB#21:                                #   in Loop: Header=BB35_7 Depth=1
	movb	$61, %al
	cmpb	$61, %r15b
	je	.LBB35_30
# BB#22:                                #   in Loop: Header=BB35_7 Depth=1
	movslq	%esi, %rax
	shlb	$4, %r15b
	movq	(%rbx), %rcx
	testb	$8, 1(%rcx,%rax,2)
	jne	.LBB35_23
# BB#24:                                #   in Loop: Header=BB35_7 Depth=1
	movl	%esi, %ecx
	addb	$-65, %cl
	cmpb	$5, %cl
	ja	.LBB35_26
# BB#25:                                #   in Loop: Header=BB35_7 Depth=1
	addb	$-55, %sil
	movb	%sil, %al
	jmp	.LBB35_29
.LBB35_23:                              #   in Loop: Header=BB35_7 Depth=1
	addb	$-48, %al
	jmp	.LBB35_29
.LBB35_26:                              #   in Loop: Header=BB35_7 Depth=1
	movl	%eax, %ecx
	addb	$-97, %cl
	cmpb	$5, %cl
	ja	.LBB35_28
# BB#27:                                #   in Loop: Header=BB35_7 Depth=1
	addb	$-87, %al
	jmp	.LBB35_29
.LBB35_28:                              #   in Loop: Header=BB35_7 Depth=1
	movl	$.L.str.97, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$61, %al
.LBB35_29:                              # %hex.exit103
                                        #   in Loop: Header=BB35_7 Depth=1
	addq	$2, %r14
	addb	%r15b, %al
.LBB35_30:                              #   in Loop: Header=BB35_7 Depth=1
	movb	%al, (%r12)
	incq	%r12
	incq	%r14
	decq	%r13
	jne	.LBB35_7
	jmp	.LBB35_31
.LBB35_34:
	testq	%r14, %r14
	je	.LBB35_32
# BB#35:
	movq	%r14, %rdi
	callq	strlen
	cmpq	$76, %rax
	ja	.LBB35_37
# BB#36:
	leaq	16(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	strcpy
	jmp	.LBB35_38
.LBB35_2:
	testq	%r14, %r14
	je	.LBB35_4
# BB#3:
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	cli_strrcpy
	movq	%rax, %r12
.LBB35_4:
	movl	$.L.str.46, %esi
	movq	%r12, %rdi
	callq	cli_strrcpy
	movq	%rax, %r12
	jmp	.LBB35_33
.LBB35_114:
	testq	%r14, %r14
	je	.LBB35_32
# BB#115:
	movb	(%r14), %bl
	testb	%bl, %bl
	je	.LBB35_32
# BB#116:
	movl	$.L.str.47, %esi
	movq	%r14, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB35_32
# BB#117:
	cmpb	$98, %bl
	je	.LBB35_32
# BB#118:
	movl	%ebx, %eax
	andb	$63, %al
	cmpb	$32, %al
	je	.LBB35_32
# BB#119:
	movl	%ebx, %eax
	addb	$-33, %al
	cmpb	$61, %al
	ja	.LBB35_32
# BB#120:
	leaq	1(%r14), %rbp
	movq	%rbp, %rdi
	callq	strlen
	cmpq	%r13, %rax
	ja	.LBB35_122
# BB#121:
	addb	$-32, %bl
	movzbl	%bl, %r8d
	cmpq	%rax, %r8
	ja	.LBB35_122
# BB#123:
	leaq	84(%r15), %r13
	movl	84(%r15), %esi
	xorl	%r10d, %r10d
	cmpl	$1, %esi
	je	.LBB35_124
# BB#125:
	cmpl	$2, %esi
	je	.LBB35_128
# BB#126:
	cmpl	$3, %esi
	jne	.LBB35_134
# BB#127:
	movb	82(%r15), %r10b
.LBB35_128:
	movb	%r10b, %r9b
	movb	81(%r15), %r10b
	jmp	.LBB35_129
.LBB35_161:
	testq	%r14, %r14
	je	.LBB35_32
# BB#162:
	movb	(%r14), %bl
	testb	%bl, %bl
	je	.LBB35_32
# BB#163:
	movl	$.L.str.49, %esi
	movl	$6, %edx
	movq	%r14, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB35_32
	.p2align	4, 0x90
.LBB35_166:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%r14), %rax
	cmpb	$61, %bl
	jne	.LBB35_164
# BB#167:                               #   in Loop: Header=BB35_166 Depth=1
	movzbl	(%rax), %ebx
	testb	%bl, %bl
	je	.LBB35_32
# BB#168:                               #   in Loop: Header=BB35_166 Depth=1
	addq	$2, %r14
	addb	$-64, %bl
	movq	%r14, %rax
	jmp	.LBB35_165
	.p2align	4, 0x90
.LBB35_164:                             #   in Loop: Header=BB35_166 Depth=1
	addb	$-42, %bl
.LBB35_165:                             # %.sink.split
                                        #   in Loop: Header=BB35_166 Depth=1
	movb	%bl, (%r12)
	incq	%r12
	movzbl	(%rax), %ebx
	testb	%bl, %bl
	movq	%rax, %r14
	jne	.LBB35_166
	jmp	.LBB35_32
.LBB35_37:
	movq	%r14, %rdi
	callq	cli_strdup
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB35_32
.LBB35_38:
	movl	$61, %esi
	movq	%rbp, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB35_40
# BB#39:
	movb	$0, (%rax)
.LBB35_40:
	movb	(%rbp), %dl
	testb	%dl, %dl
	je	.LBB35_47
# BB#41:                                # %.lr.ph14.i.preheader
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB35_42:                              # %.lr.ph14.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_44 Depth 2
	movzbl	%dl, %edx
	cmpb	$-1, base64Table(%rdx)
	jne	.LBB35_46
# BB#43:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB35_42 Depth=1
	leaq	1(%rcx), %rdx
	.p2align	4, 0x90
.LBB35_44:                              # %.lr.ph.i
                                        #   Parent Loop BB35_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdx), %ebx
	movb	%bl, -1(%rdx)
	incq	%rdx
	testb	%bl, %bl
	jne	.LBB35_44
# BB#45:                                # %._crit_edge.i
                                        #   in Loop: Header=BB35_42 Depth=1
	decq	%rcx
.LBB35_46:                              #   in Loop: Header=BB35_42 Depth=1
	movb	1(%rcx), %dl
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB35_42
.LBB35_47:                              # %sanitiseBase64.exit
	testq	%rax, %rax
	je	.LBB35_49
# BB#48:
	xorl	%eax, %eax
	jmp	.LBB35_50
.LBB35_49:
	movq	%rbp, %rdi
	callq	strlen
	testb	$3, %al
	sete	%al
.LBB35_50:
	movl	84(%r15), %ebx
	xorl	%r14d, %r14d
	cmpl	$1, %ebx
	je	.LBB35_51
# BB#52:
	cmpl	$2, %ebx
	je	.LBB35_55
# BB#53:
	cmpl	$3, %ebx
	jne	.LBB35_62
# BB#54:
	movb	82(%r15), %r14b
.LBB35_55:
	movb	%r14b, %r9b
	movb	81(%r15), %r14b
	jmp	.LBB35_56
.LBB35_51:
	xorl	%r9d, %r9d
.LBB35_56:                              # %.thread.i
	movb	80(%r15), %r13b
	testq	%rbp, %rbp
	je	.LBB35_71
.LBB35_58:                              # %.preheader146.i
	movb	(%rbp), %cl
	testb	%cl, %cl
	je	.LBB35_110
# BB#59:                                # %.lr.ph173.i.preheader
                                        # implicit-def: %SIL
	movq	%rbp, %rax
                                        # implicit-def: %BPL
	movq	%rax, %r8
	testl	%ebx, %ebx
	jne	.LBB35_61
	jmp	.LBB35_82
	.p2align	4, 0x90
.LBB35_113:                             # %..lr.ph173_crit_edge.i
	movl	84(%r15), %ebx
	testl	%ebx, %ebx
	je	.LBB35_82
.LBB35_61:
	decl	%ebx
	movl	%ebx, 84(%r15)
	movl	%r13d, %ecx
	jmp	.LBB35_85
	.p2align	4, 0x90
.LBB35_82:
	movzbl	%cl, %ecx
	movb	base64Table(%rcx), %dl
	cmpb	$-1, %dl
	movb	$63, %cl
	je	.LBB35_84
# BB#83:
	movl	%edx, %ecx
.LBB35_84:
	incq	%rax
	xorl	%ebx, %ebx
.LBB35_85:
	movzbl	(%rax), %edi
	testq	%rdi, %rdi
	je	.LBB35_86
# BB#87:
	testl	%ebx, %ebx
	je	.LBB35_89
# BB#88:
	decl	%ebx
	movl	%ebx, 84(%r15)
	movb	%r14b, %dil
	jmp	.LBB35_92
	.p2align	4, 0x90
.LBB35_86:
	xorl	%edi, %edi
	movl	$1, %ebx
	jmp	.LBB35_104
.LBB35_89:
	movb	base64Table(%rdi), %dl
	cmpb	$-1, %dl
	movb	$63, %dil
	je	.LBB35_91
# BB#90:
	movb	%dl, %dil
.LBB35_91:
	incq	%rax
	xorl	%ebx, %ebx
.LBB35_92:
	movzbl	(%rax), %edx
	testq	%rdx, %rdx
	je	.LBB35_93
# BB#94:
	testl	%ebx, %ebx
	je	.LBB35_96
# BB#95:
	decl	%ebx
	movl	%ebx, 84(%r15)
	movb	%r9b, %bpl
	jmp	.LBB35_99
.LBB35_93:
	xorl	%ebp, %ebp
	movl	$2, %ebx
	jmp	.LBB35_104
.LBB35_96:
	movb	base64Table(%rdx), %dl
	cmpb	$-1, %dl
	movb	$63, %bpl
	je	.LBB35_98
# BB#97:
	movb	%dl, %bpl
.LBB35_98:
	incq	%rax
.LBB35_99:
	movzbl	(%rax), %edx
	testq	%rdx, %rdx
	je	.LBB35_100
# BB#101:
	movb	base64Table(%rdx), %dl
	cmpb	$-1, %dl
	movb	$63, %sil
	je	.LBB35_103
# BB#102:
	movb	%dl, %sil
.LBB35_103:
	incq	%rax
	movl	$4, %ebx
	jmp	.LBB35_104
.LBB35_100:
	xorl	%esi, %esi
	movl	$3, %ebx
	.p2align	4, 0x90
.LBB35_104:
	movl	%ebx, %edx
	andb	$7, %dl
	decb	%dl
	cmpb	$3, %dl
	ja	.LBB35_109
# BB#105:
	movzbl	%dl, %edx
	jmpq	*.LJTI35_1(,%rdx,8)
.LBB35_112:
	shlb	$2, %cl
	movl	%edi, %edx
	shrb	$4, %dl
	andb	$3, %dl
	orb	%cl, %dl
	movb	%dl, (%r12)
	shlb	$4, %dil
	movl	%ebp, %ecx
	shrb	$2, %cl
	andb	$15, %cl
	orb	%dil, %cl
	movb	%cl, 1(%r12)
	movl	%ebp, %ecx
	shlb	$6, %cl
	movl	%esi, %edx
	andb	$63, %dl
	orb	%cl, %dl
	movb	%dl, 2(%r12)
	movb	(%rax), %cl
	addq	$3, %r12
	testb	%cl, %cl
	jne	.LBB35_113
	jmp	.LBB35_109
.LBB35_62:
	testb	%al, %al
	je	.LBB35_63
# BB#64:                                # %.preheader.i
	movb	(%rbp), %cl
	testb	%cl, %cl
	je	.LBB35_110
# BB#65:                                # %.lr.ph.i104.preheader
	leaq	4(%rbp), %rax
	.p2align	4, 0x90
.LBB35_66:                              # %.lr.ph.i104
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%cl, %esi
	movzbl	-3(%rax), %ecx
	movzbl	base64Table(%rcx), %ebx
	cmpb	$-1, %bl
	movb	$63, %dil
	movb	$63, %dl
	je	.LBB35_68
# BB#67:                                # %.lr.ph.i104
                                        #   in Loop: Header=BB35_66 Depth=1
	movl	%ebx, %edx
.LBB35_68:                              # %.lr.ph.i104
                                        #   in Loop: Header=BB35_66 Depth=1
	movzbl	base64Table(%rsi), %ebx
	movzbl	-2(%rax), %ecx
	movzbl	base64Table(%rcx), %ecx
	cmpb	$-1, %cl
	je	.LBB35_70
# BB#69:                                # %.lr.ph.i104
                                        #   in Loop: Header=BB35_66 Depth=1
	movl	%ecx, %edi
.LBB35_70:                              # %.lr.ph.i104
                                        #   in Loop: Header=BB35_66 Depth=1
	shlb	$2, %bl
	movl	%edx, %ecx
	shrb	$4, %cl
	andb	$3, %cl
	orb	%bl, %cl
	movb	%cl, (%r12)
	movzbl	-1(%rax), %ecx
	movzbl	base64Table(%rcx), %ecx
	shlb	$4, %dl
	movl	%edi, %ebx
	shrb	$2, %bl
	andb	$15, %bl
	orb	%dl, %bl
	movb	%bl, 1(%r12)
	shlb	$6, %dil
	andb	$63, %cl
	orb	%dil, %cl
	movb	%cl, 2(%r12)
	addq	$3, %r12
	movzbl	(%rax), %ecx
	addq	$4, %rax
	testb	%cl, %cl
	jne	.LBB35_66
	jmp	.LBB35_110
.LBB35_20:
	movb	%r15b, (%r12)
	incq	%r12
.LBB35_31:
	movb	$10, (%r12)
	incq	%r12
	jmp	.LBB35_32
.LBB35_63:
	xorl	%r9d, %r9d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	testq	%rbp, %rbp
	jne	.LBB35_58
.LBB35_71:
	testl	%ebx, %ebx
	je	.LBB35_110
# BB#72:
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	%r13b, %edx
	movzwl	(%rax,%rdx,2), %ecx
	testb	$8, %cl
	movl	$64, %r8d
	cmovel	%r8d, %edx
	movzbl	%r14b, %esi
	movzwl	(%rax,%rsi,2), %ecx
	testb	$8, %cl
	movl	$64, %ecx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	cmovnel	%esi, %ecx
	movzbl	4(%rsp), %esi           # 1-byte Folded Reload
	movzwl	(%rax,%rsi,2), %eax
	testb	$8, %al
	cmovnel	%esi, %r8d
	movl	$.L.str.96, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_dbgmsg
	movl	84(%r15), %eax
	movl	%eax, %ecx
	decl	%ecx
	movl	%ecx, 84(%r15)
	je	.LBB35_81
# BB#73:
	movl	%eax, %ecx
	addl	$-2, %ecx
	movl	%ecx, 84(%r15)
	je	.LBB35_80
# BB#74:                                # %.thread141.i
	addl	$-3, %eax
	movl	%eax, 84(%r15)
	shlb	$2, %r13b
	movl	%r14d, %eax
	shrb	$4, %al
	andb	$3, %al
	orb	%r13b, %al
	movb	%al, (%r12)
	shlb	$4, %r14b
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, %eax
	shrb	$2, %al
	andb	$15, %al
	orb	%r14b, %al
	movb	%al, 1(%r12)
	testb	%cl, %cl
	je	.LBB35_75
# BB#76:
	shlb	$6, %cl
	movb	%cl, 2(%r12)
	addq	$3, %r12
	jmp	.LBB35_110
.LBB35_122:
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	addq	$84, %r15
	movl	$0, (%r15)
	jmp	.LBB35_32
.LBB35_80:
	testb	%r14b, %r14b
	je	.LBB35_81
# BB#77:
	shlb	$2, %r13b
	shrb	$4, %r14b
	andb	$3, %r14b
	orb	%r13b, %r14b
	movb	%r14b, (%r12)
	movq	8(%rsp), %rax           # 8-byte Reload
	shll	$4, %eax
	testb	$-16, %al
	je	.LBB35_78
# BB#79:
	movb	%al, 1(%r12)
	addq	$2, %r12
	jmp	.LBB35_110
.LBB35_81:                              # %.thread142.i
	shlb	$2, %r13b
	movb	%r13b, (%r12)
	incq	%r12
	jmp	.LBB35_110
.LBB35_106:
	movb	%bpl, 82(%r15)
.LBB35_107:                             # %.loopexit.i
	movb	%dil, 81(%r15)
.LBB35_108:                             # %.loopexit148.i
	movb	%cl, 80(%r15)
	movl	%ebx, 84(%r15)
.LBB35_109:                             # %.decode.exit.loopexit344_crit_edge
	movq	%r8, %rbp
.LBB35_110:                             # %decode.exit
	leaq	16(%rsp), %rax
	cmpq	%rax, %rbp
	je	.LBB35_32
# BB#111:
	movq	%rbp, %rdi
	callq	free
.LBB35_32:                              # %.critedge.thread
	movb	$0, (%r12)
.LBB35_33:
	movq	%r12, %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB35_75:
	addq	$2, %r12
	jmp	.LBB35_110
.LBB35_124:
	xorl	%r9d, %r9d
.LBB35_129:                             # %.thread.i113
	movb	80(%r15), %r11b
.LBB35_130:
	movb	(%rbp), %bl
	testb	%bl, %bl
	je	.LBB35_158
# BB#131:                               # %.lr.ph173.i126.preheader
                                        # implicit-def: %R14B
                                        # implicit-def: %AL
	movq	%r12, %rdx
	testl	%esi, %esi
	jne	.LBB35_133
	jmp	.LBB35_139
.LBB35_160:                             # %..lr.ph173_crit_edge.i142
	movl	(%r13), %esi
	addq	$3, %rdx
	testl	%esi, %esi
	je	.LBB35_139
.LBB35_133:
	decl	%esi
	movl	%esi, (%r13)
	movl	%r11d, %ebx
	jmp	.LBB35_140
.LBB35_139:
	incq	%rbp
	addb	$-32, %bl
	xorl	%esi, %esi
.LBB35_140:
	movb	(%rbp), %dil
	testb	%dil, %dil
	je	.LBB35_141
# BB#142:
	testl	%esi, %esi
	je	.LBB35_144
# BB#143:
	decl	%esi
	movl	%esi, (%r13)
	movb	%r10b, %dil
	jmp	.LBB35_145
.LBB35_141:
	movl	$1, %esi
	xorl	%edi, %edi
	jmp	.LBB35_153
.LBB35_144:
	incq	%rbp
	addb	$-32, %dil
	xorl	%esi, %esi
.LBB35_145:
	movb	(%rbp), %al
	testb	%al, %al
	je	.LBB35_146
# BB#147:
	testl	%esi, %esi
	je	.LBB35_149
# BB#148:
	decl	%esi
	movl	%esi, (%r13)
	movb	%r9b, %al
	jmp	.LBB35_150
.LBB35_146:
	movl	$2, %esi
	xorl	%eax, %eax
	jmp	.LBB35_153
.LBB35_149:
	incq	%rbp
	addb	$-32, %al
.LBB35_150:
	movb	(%rbp), %r14b
	testb	%r14b, %r14b
	je	.LBB35_151
# BB#152:
	incq	%rbp
	addb	$-32, %r14b
	movl	$4, %esi
	jmp	.LBB35_153
.LBB35_151:
	movl	$3, %esi
	xorl	%r14d, %r14d
.LBB35_153:
	movl	%esi, %ecx
	andb	$7, %cl
	decb	%cl
	cmpb	$3, %cl
	ja	.LBB35_158
# BB#154:
	movzbl	%cl, %ecx
	jmpq	*.LJTI35_2(,%rcx,8)
.LBB35_159:
	shlb	$2, %bl
	movl	%edi, %ecx
	shrb	$4, %cl
	andb	$3, %cl
	orb	%bl, %cl
	movb	%cl, (%rdx)
	shlb	$4, %dil
	movl	%eax, %ecx
	shrb	$2, %cl
	andb	$15, %cl
	orb	%dil, %cl
	movb	%cl, 1(%rdx)
	movl	%eax, %ecx
	shlb	$6, %cl
	movl	%r14d, %ebx
	andb	$63, %bl
	orb	%cl, %bl
	movb	%bl, 2(%rdx)
	movb	(%rbp), %bl
	testb	%bl, %bl
	jne	.LBB35_160
	jmp	.LBB35_158
.LBB35_134:
	testb	$3, %al
	je	.LBB35_136
# BB#135:
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	jmp	.LBB35_130
.LBB35_78:
	incq	%r12
	jmp	.LBB35_110
.LBB35_136:                             # %.preheader.i114
	movb	(%rbp), %cl
	testb	%cl, %cl
	je	.LBB35_158
# BB#137:                               # %.lr.ph.i117.preheader
	addq	$5, %r14
	movq	%r12, %rsi
.LBB35_138:                             # %.lr.ph.i117
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%r14), %eax
	movzbl	-2(%r14), %ebx
	addb	$-32, %al
	addb	$-32, %bl
	shlb	$2, %cl
	movl	%eax, %edx
	shrb	$4, %dl
	andb	$3, %dl
	orb	%cl, %dl
	xorb	$-128, %dl
	movb	%dl, (%rsi)
	movzbl	-1(%r14), %ecx
	addb	$32, %cl
	shlb	$4, %al
	movl	%ebx, %edx
	shrb	$2, %dl
	andb	$15, %dl
	orb	%al, %dl
	movb	%dl, 1(%rsi)
	shlb	$6, %bl
	andb	$63, %cl
	orb	%bl, %cl
	movb	%cl, 2(%rsi)
	movzbl	(%r14), %ecx
	addq	$4, %r14
	addq	$3, %rsi
	testb	%cl, %cl
	jne	.LBB35_138
	jmp	.LBB35_158
.LBB35_155:
	movb	%al, 82(%r15)
.LBB35_156:                             # %.loopexit.i139
	movb	%dil, 81(%r15)
.LBB35_157:                             # %.loopexit148.i140
	movb	%bl, 80(%r15)
	movl	%esi, 84(%r15)
.LBB35_158:                             # %decode.exit144
	addq	%r8, %r12
	movl	$0, (%r13)
	jmp	.LBB35_32
.Lfunc_end35:
	.size	decodeLine, .Lfunc_end35-decodeLine
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI35_0:
	.quad	.LBB35_5
	.quad	.LBB35_34
	.quad	.LBB35_2
	.quad	.LBB35_2
	.quad	.LBB35_114
	.quad	.LBB35_161
.LJTI35_1:
	.quad	.LBB35_108
	.quad	.LBB35_107
	.quad	.LBB35_106
	.quad	.LBB35_112
.LJTI35_2:
	.quad	.LBB35_157
	.quad	.LBB35_156
	.quad	.LBB35_155
	.quad	.LBB35_159

	.text
	.globl	bounceBegin
	.p2align	4, 0x90
	.type	bounceBegin,@function
bounceBegin:                            # @bounceBegin
	.cfi_startproc
# BB#0:
	movq	96(%rdi), %rax
	retq
.Lfunc_end36:
	.size	bounceBegin, .Lfunc_end36-bounceBegin
	.cfi_endproc

	.globl	encodingLine
	.p2align	4, 0x90
	.type	encodingLine,@function
encodingLine:                           # @encodingLine
	.cfi_startproc
# BB#0:
	movq	120(%rdi), %rax
	retq
.Lfunc_end37:
	.size	encodingLine, .Lfunc_end37-encodingLine
	.cfi_endproc

	.globl	messageClearMarkers
	.p2align	4, 0x90
	.type	messageClearMarkers,@function
messageClearMarkers:                    # @messageClearMarkers
	.cfi_startproc
# BB#0:
	movq	$0, 120(%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 96(%rdi)
	retq
.Lfunc_end38:
	.size	messageClearMarkers, .Lfunc_end38-messageClearMarkers
	.cfi_endproc

	.globl	isuuencodebegin
	.p2align	4, 0x90
	.type	isuuencodebegin,@function
isuuencodebegin:                        # @isuuencodebegin
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi254:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi255:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi256:
	.cfi_def_cfa_offset 32
.Lcfi257:
	.cfi_offset %rbx, -24
.Lcfi258:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%ebp, %ebp
	cmpb	$98, (%rbx)
	jne	.LBB39_12
# BB#1:
	movq	%rbx, %rdi
	callq	strlen
	cmpq	$10, %rax
	jb	.LBB39_12
# BB#2:
	movl	$.L.str.50, %esi
	movl	$6, %edx
	movq	%rbx, %rdi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB39_4
# BB#3:
	xorl	%eax, %eax
	jmp	.LBB39_11
.LBB39_4:
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	6(%rbx), %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB39_6
# BB#5:
	xorl	%eax, %eax
	jmp	.LBB39_11
.LBB39_6:
	movsbq	7(%rbx), %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB39_8
# BB#7:
	xorl	%eax, %eax
	jmp	.LBB39_11
.LBB39_8:
	movsbq	8(%rbx), %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB39_10
# BB#9:
	xorl	%eax, %eax
	jmp	.LBB39_11
.LBB39_10:
	cmpb	$32, 9(%rbx)
	sete	%al
.LBB39_11:
	movzbl	%al, %ebp
.LBB39_12:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end39:
	.size	isuuencodebegin, .Lfunc_end39-isuuencodebegin
	.cfi_endproc

	.globl	messageSetCTX
	.p2align	4, 0x90
	.type	messageSetCTX,@function
messageSetCTX:                          # @messageSetCTX
	.cfi_startproc
# BB#0:
	movq	%rsi, 72(%rdi)
	retq
.Lfunc_end40:
	.size	messageSetCTX, .Lfunc_end40-messageSetCTX
	.cfi_endproc

	.globl	messageContainsVirus
	.p2align	4, 0x90
	.type	messageContainsVirus,@function
messageContainsVirus:                   # @messageContainsVirus
	.cfi_startproc
# BB#0:
	movzbl	88(%rdi), %eax
	andl	$1, %eax
	retq
.Lfunc_end41:
	.size	messageContainsVirus, .Lfunc_end41-messageContainsVirus
	.cfi_endproc

	.type	messageSetMimeType.mime_table,@object # @messageSetMimeType.mime_table
	.local	messageSetMimeType.mime_table
	.comm	messageSetMimeType.mime_table,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Empty content-type field\n"
	.size	.L.str, 26

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"messageSetMimeType: '%s'\n"
	.size	.L.str.1, 26

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"x-"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"plain"
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Incorrect MIME type: `plain', set to Text\n"
	.size	.L.str.4, 43

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Unknown MIME type \"%s\" - guessing as %s (%u%% certainty)\n"
	.size	.L.str.5, 58

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Unknown MIME type: `%s', set to Application - if you believe this file contains a virus, submit it to www.clamav.net\n"
	.size	.L.str.6, 118

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Empty content subtype\n"
	.size	.L.str.7, 23

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.zero	1
	.size	.L.str.8, 1

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"messageAddArgument, arg='%s'\n"
	.size	.L.str.9, 30

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"filename="
	.size	.L.str.10, 10

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"name="
	.size	.L.str.11, 6

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Force mime encoding to application\n"
	.size	.L.str.12, 36

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"application"
	.size	.L.str.13, 12

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Add arguments '%s'\n"
	.size	.L.str.14, 20

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Can't parse header \"%s\"\n"
	.size	.L.str.15, 25

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Unbalanced quote character in \"%s\"\n"
	.size	.L.str.16, 36

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Can't parse header \"%s\" - if you believe this file contains a virus, submit it to www.clamav.net\n"
	.size	.L.str.17, 98

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Ignoring empty field in \"%s\"\n"
	.size	.L.str.19, 30

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"messageFindArgument: no '=' sign found in MIME header '%s' (%s)\n"
	.size	.L.str.20, 65

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"filename"
	.size	.L.str.21, 9

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"name"
	.size	.L.str.22, 5

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"file"
	.size	.L.str.23, 5

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"messageSetEncoding: '%s'\n"
	.size	.L.str.24, 26

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"8 bit"
	.size	.L.str.25, 6

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Broken content-transfer-encoding: '8 bit' changed to '8bit'\n"
	.size	.L.str.26, 61

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"8bit"
	.size	.L.str.27, 5

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	" \t"
	.size	.L.str.28, 3

	.type	encoding_map,@object    # @encoding_map
	.section	.rodata,"a",@progbits
	.p2align	4
encoding_map:
	.quad	.L.str.58
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.59
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.35
	.long	1                       # 0x1
	.zero	4
	.quad	.L.str.34
	.long	2                       # 0x2
	.zero	4
	.quad	.L.str.27
	.long	3                       # 0x3
	.zero	4
	.quad	.L.str.60
	.long	4                       # 0x4
	.zero	4
	.quad	.L.str.61
	.long	5                       # 0x5
	.zero	4
	.quad	.L.str.62
	.long	6                       # 0x6
	.zero	4
	.quad	.L.str.63
	.long	8                       # 0x8
	.zero	4
	.quad	.L.str.64
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.65
	.long	5                       # 0x5
	.zero	4
	.quad	.L.str.29
	.long	5                       # 0x5
	.zero	4
	.zero	16
	.size	encoding_map, 208

	.type	.L.str.29,@object       # @.str.29
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.29:
	.asciz	"uuencode"
	.size	.L.str.29, 9

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Ignoring duplicate encoding mechanism '%s'\n"
	.size	.L.str.30, 44

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Encoding type %d is \"%s\"\n"
	.size	.L.str.31, 26

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Unknown encoding type \"%s\" - guessing as %s (%u%% certainty)\n"
	.size	.L.str.32, 62

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"Unknown encoding type \"%s\" - if you believe this file contains a virus, submit it to www.clamav.net\n"
	.size	.L.str.33, 101

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"base64"
	.size	.L.str.34, 7

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"quoted-printable"
	.size	.L.str.35, 17

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	" "
	.size	.L.str.36, 2

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"messageAddStr: out of memory\n"
	.size	.L.str.37, 30

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"messageAddStrAtTop: out of memory\n"
	.size	.L.str.38, 35

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"messageMoveText sanity check: t not within old_message\n"
	.size	.L.str.39, 56

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"%d trailing bytes to export\n"
	.size	.L.str.40, 29

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"messageToFileblob\n"
	.size	.L.str.41, 19

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"messageToBlob\n"
	.size	.L.str.42, 15

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"messageToText: export transfer method %d = %d\n"
	.size	.L.str.43, 47

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"messageToText: Unexpected attempt to handle uuencoded file - report to http://bugs.clamav.net\n"
	.size	.L.str.44, 95

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"Binhex messages not supported yet.\n"
	.size	.L.str.45, 36

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"\n"
	.size	.L.str.46, 2

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"end"
	.size	.L.str.47, 4

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"uudecode: buffer overflow stopped, attempting to ignore but decoding may fail\n"
	.size	.L.str.48, 79

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"=yend "
	.size	.L.str.49, 7

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"begin "
	.size	.L.str.50, 7

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"text"
	.size	.L.str.51, 5

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"multipart"
	.size	.L.str.52, 10

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"audio"
	.size	.L.str.53, 6

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"image"
	.size	.L.str.54, 6

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"message"
	.size	.L.str.55, 8

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"video"
	.size	.L.str.56, 6

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"messageArgumentExists: no '=' sign found in MIME header '%s' (%s)\n"
	.size	.L.str.57, 67

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"7bit"
	.size	.L.str.58, 5

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"text/plain"
	.size	.L.str.59, 11

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"binary"
	.size	.L.str.60, 7

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"x-uuencode"
	.size	.L.str.61, 11

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"x-yencode"
	.size	.L.str.62, 10

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"x-binhex"
	.size	.L.str.63, 9

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"us-ascii"
	.size	.L.str.64, 9

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"x-uue"
	.size	.L.str.65, 6

	.type	messageIsEncoding.encoding,@object # @messageIsEncoding.encoding
	.section	.rodata,"a",@progbits
	.p2align	4
messageIsEncoding.encoding:
	.asciz	"Content-Transfer-Encoding"
	.size	messageIsEncoding.encoding, 26

	.type	messageIsEncoding.binhex,@object # @messageIsEncoding.binhex
	.p2align	4
messageIsEncoding.binhex:
	.asciz	"(This file must be converted with BinHex 4.0)"
	.size	messageIsEncoding.binhex, 46

	.type	.L.str.66,@object       # @.str.66
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.66:
	.asciz	"Received: "
	.size	.L.str.66, 11

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"BinHex"
	.size	.L.str.67, 7

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"=ybegin line="
	.size	.L.str.68, 14

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"messageExport: numberOfEncTypes == %d\n"
	.size	.L.str.69, 39

	.type	messageExport.hqxtbl,@object # @messageExport.hqxtbl
	.section	.rodata,"a",@progbits
	.p2align	4
messageExport.hqxtbl:
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\000\001\002\003\004\005\006\007\b\t\n\013\f\377\377\r\016\017\020\021\022\023\377\024\025\377\377\377\377\377\377\026\027\030\031\032\033\034\035\036\037 !\"#$\377%&'()*+\377,-./\377\377\377\3770123456\377789:;<\377\377=>?\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.size	messageExport.hqxtbl, 128

	.type	.L.str.70,@object       # @.str.70
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.70:
	.asciz	"messageExport: decode binhex\n"
	.size	.L.str.70, 30

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"Couldn't start binhex parser\n"
	.size	.L.str.71, 30

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"Couldn't locate the binhex message that was claimed to be there\n"
	.size	.L.str.72, 65

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"decode HQX7 message (%lu bytes)\n"
	.size	.L.str.73, 33

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"Invalid HQX7 character '%c' (0x%02x)\n"
	.size	.L.str.74, 38

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"decoded HQX7 message (now %lu bytes)\n"
	.size	.L.str.75, 38

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"HQX8 messages not yet supported, extraction may fail - if you believe this file contains a virus, submit it to www.clamav.net\n"
	.size	.L.str.76, 127

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"Uncompressed %lu bytes to %lu\n"
	.size	.L.str.77, 31

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"HQX7 message (%lu bytes) is not compressed\n"
	.size	.L.str.78, 44

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"Discarding empty binHex attachment\n"
	.size	.L.str.79, 36

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"name=%s"
	.size	.L.str.80, 8

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"Filename = '%s', data fork length = %lu, resource fork length = %lu bytes\n"
	.size	.L.str.81, 75

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"Corrupt BinHex file, claims it is %lu bytes long in a message of %lu bytes\n"
	.size	.L.str.82, 76

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"Finished exporting binhex file\n"
	.size	.L.str.83, 32

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"messageExport: Entering fast copy mode\n"
	.size	.L.str.84, 40

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"Unencoded attachment sent with no filename\n"
	.size	.L.str.85, 44

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"name=attachment"
	.size	.L.str.86, 16

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"attachment"
	.size	.L.str.87, 11

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"Not all decoding algorithms were run\n"
	.size	.L.str.88, 38

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"messageExport: enctype %d is %d\n"
	.size	.L.str.89, 33

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	" name="
	.size	.L.str.90, 7

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"Set yEnc filename to \"%s\"\n"
	.size	.L.str.91, 27

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"messageExport: treat uuencode as text/plain\n"
	.size	.L.str.92, 45

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"Attachment sent with no filename\n"
	.size	.L.str.93, 34

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"Empty attachment not saved\n"
	.size	.L.str.94, 28

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"Exported %lu bytes using enctype %d\n"
	.size	.L.str.95, 37

	.type	base64Table,@object     # @base64Table
	.section	.rodata,"a",@progbits
	.p2align	4
base64Table:
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377>\377\377\377?456789:;<=\377\377\377\000\377\377\377\000\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020\021\022\023\024\025\026\027\030\031\377\377\377\377\377\377\032\033\034\035\036\037 !\"#$%&'()*+,-./0123\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.size	base64Table, 256

	.type	.L.str.96,@object       # @.str.96
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.96:
	.asciz	"base64chars = %d (%c %c %c)\n"
	.size	.L.str.96, 29

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"Illegal hex character '%c'\n"
	.size	.L.str.97, 28

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"boundary"
	.size	.L.str.98, 9

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"protocol"
	.size	.L.str.99, 9

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"id"
	.size	.L.str.100, 3

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"number"
	.size	.L.str.101, 7

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"total"
	.size	.L.str.102, 6

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"type"
	.size	.L.str.103, 5

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"Discarding unwanted argument '%s'\n"
	.size	.L.str.104, 35

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"messageDedup\n"
	.size	.L.str.105, 14

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"messageDedup: out of memory\n"
	.size	.L.str.106, 29

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"messageDedup reclaimed %lu bytes\n"
	.size	.L.str.107, 34

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"*0*="
	.size	.L.str.108, 5

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"RFC2231 parameter continuations are not yet handled\n"
	.size	.L.str.109, 53

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"*0="
	.size	.L.str.110, 4

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"*="
	.size	.L.str.111, 3

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"rfc2231 '%s'\n"
	.size	.L.str.112, 14

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"Invalid RFC2231 header: '%s'\n"
	.size	.L.str.113, 30

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"rfc2231 returns '%s'\n"
	.size	.L.str.114, 22


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
