	.text
	.file	"z51.bc"
	.globl	Plain_PrintInitialize
	.p2align	4, 0x90
	.type	Plain_PrintInitialize,@function
Plain_PrintInitialize:                  # @Plain_PrintInitialize
	.cfi_startproc
# BB#0:
	movq	%rdi, out_fp(%rip)
	movb	$0, prologue_done(%rip)
	retq
.Lfunc_end0:
	.size	Plain_PrintInitialize, .Lfunc_end0-Plain_PrintInitialize
	.cfi_endproc

	.globl	Plain_CoordTranslate
	.p2align	4, 0x90
	.type	Plain_CoordTranslate,@function
Plain_CoordTranslate:                   # @Plain_CoordTranslate
	.cfi_startproc
# BB#0:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	jmp	Error                   # TAILCALL
.Lfunc_end1:
	.size	Plain_CoordTranslate, .Lfunc_end1-Plain_CoordTranslate
	.cfi_endproc

	.globl	Plain_CoordScale
	.p2align	4, 0x90
	.type	Plain_CoordScale,@function
Plain_CoordScale:                       # @Plain_CoordScale
	.cfi_startproc
# BB#0:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	jmp	Error                   # TAILCALL
.Lfunc_end2:
	.size	Plain_CoordScale, .Lfunc_end2-Plain_CoordScale
	.cfi_endproc

	.globl	Plain_SaveGraphicState
	.p2align	4, 0x90
	.type	Plain_SaveGraphicState,@function
Plain_SaveGraphicState:                 # @Plain_SaveGraphicState
	.cfi_startproc
# BB#0:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	jmp	Error                   # TAILCALL
.Lfunc_end3:
	.size	Plain_SaveGraphicState, .Lfunc_end3-Plain_SaveGraphicState
	.cfi_endproc

	.globl	Plain_RestoreGraphicState
	.p2align	4, 0x90
	.type	Plain_RestoreGraphicState,@function
Plain_RestoreGraphicState:              # @Plain_RestoreGraphicState
	.cfi_startproc
# BB#0:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	jmp	Error                   # TAILCALL
.Lfunc_end4:
	.size	Plain_RestoreGraphicState, .Lfunc_end4-Plain_RestoreGraphicState
	.cfi_endproc

	.globl	Plain_PrintGraphicObject
	.p2align	4, 0x90
	.type	Plain_PrintGraphicObject,@function
Plain_PrintGraphicObject:               # @Plain_PrintGraphicObject
	.cfi_startproc
# BB#0:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	jmp	Error                   # TAILCALL
.Lfunc_end5:
	.size	Plain_PrintGraphicObject, .Lfunc_end5-Plain_PrintGraphicObject
	.cfi_endproc

	.globl	Plain_DefineGraphicNames
	.p2align	4, 0x90
	.type	Plain_DefineGraphicNames,@function
Plain_DefineGraphicNames:               # @Plain_DefineGraphicNames
	.cfi_startproc
# BB#0:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	jmp	Error                   # TAILCALL
.Lfunc_end6:
	.size	Plain_DefineGraphicNames, .Lfunc_end6-Plain_DefineGraphicNames
	.cfi_endproc

	.globl	Plain_SaveTranslateDefineSave
	.p2align	4, 0x90
	.type	Plain_SaveTranslateDefineSave,@function
Plain_SaveTranslateDefineSave:          # @Plain_SaveTranslateDefineSave
	.cfi_startproc
# BB#0:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	jmp	Error                   # TAILCALL
.Lfunc_end7:
	.size	Plain_SaveTranslateDefineSave, .Lfunc_end7-Plain_SaveTranslateDefineSave
	.cfi_endproc

	.globl	Plain_PrintGraphicInclude
	.p2align	4, 0x90
	.type	Plain_PrintGraphicInclude,@function
Plain_PrintGraphicInclude:              # @Plain_PrintGraphicInclude
	.cfi_startproc
# BB#0:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	jmp	Error                   # TAILCALL
.Lfunc_end8:
	.size	Plain_PrintGraphicInclude, .Lfunc_end8-Plain_PrintGraphicInclude
	.cfi_endproc

	.p2align	4, 0x90
	.type	Plain_PrintLength,@function
Plain_PrintLength:                      # @Plain_PrintLength
	.cfi_startproc
# BB#0:
	cvtsi2ssl	%esi, %xmm0
	testl	%edx, %edx
	je	.LBB9_1
# BB#2:
	movl	PlainCharHeight(%rip), %eax
	cvtsi2ssl	%eax, %xmm1
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.11, %esi
	movb	$1, %al
	jmp	sprintf                 # TAILCALL
.LBB9_1:
	movl	PlainCharWidth(%rip), %eax
	cvtsi2ssl	%eax, %xmm1
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.10, %esi
	movb	$1, %al
	jmp	sprintf                 # TAILCALL
.Lfunc_end9:
	.size	Plain_PrintLength, .Lfunc_end9-Plain_PrintLength
	.cfi_endproc

	.p2align	4, 0x90
	.type	Plain_PrintPageSetupForFont,@function
Plain_PrintPageSetupForFont:            # @Plain_PrintPageSetupForFont
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end10:
	.size	Plain_PrintPageSetupForFont, .Lfunc_end10-Plain_PrintPageSetupForFont
	.cfi_endproc

	.p2align	4, 0x90
	.type	Plain_PrintPageResourceForFont,@function
Plain_PrintPageResourceForFont:         # @Plain_PrintPageResourceForFont
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	Plain_PrintPageResourceForFont, .Lfunc_end11-Plain_PrintPageResourceForFont
	.cfi_endproc

	.p2align	4, 0x90
	.type	Plain_PrintMapping,@function
Plain_PrintMapping:                     # @Plain_PrintMapping
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end12:
	.size	Plain_PrintMapping, .Lfunc_end12-Plain_PrintMapping
	.cfi_endproc

	.p2align	4, 0x90
	.type	Plain_PrintBeforeFirstPage,@function
Plain_PrintBeforeFirstPage:             # @Plain_PrintBeforeFirstPage
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	-1(%rdi), %eax
	cltd
	idivl	PlainCharWidth(%rip)
	movl	%eax, %r15d
	leal	1(%r15), %r12d
	movl	%r12d, hsize(%rip)
	leal	-1(%rsi), %eax
	cltd
	idivl	PlainCharHeight(%rip)
	movl	%eax, %ebx
	leal	1(%rbx), %ebp
	movl	%ebp, vsize(%rip)
	movl	%ebp, %eax
	imull	%r12d, %eax
	movslq	%eax, %rdi
	callq	malloc
	movq	%rax, %r14
	movq	%r14, page(%rip)
	orl	%r15d, %ebx
	js	.LBB13_3
# BB#1:                                 # %.preheader.us.preheader
	movl	%r15d, %r15d
	incq	%r15
	movslq	%ebp, %r13
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_2:                               # %.preheader.us
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rbx
	leaq	(%r14,%rbx), %rdi
	movl	$32, %esi
	movq	%r15, %rdx
	callq	memset
	incq	%rbp
	addl	%r12d, %ebx
	cmpq	%r13, %rbp
	jl	.LBB13_2
.LBB13_3:                               # %._crit_edge15
	movb	$1, prologue_done(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	Plain_PrintBeforeFirstPage, .Lfunc_end13-Plain_PrintBeforeFirstPage
	.cfi_endproc

	.p2align	4, 0x90
	.type	Plain_PrintBetweenPages,@function
Plain_PrintBetweenPages:                # @Plain_PrintBetweenPages
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movl	%edi, (%rsp)            # 4-byte Spill
	movl	vsize(%rip), %r13d
	testl	%r13d, %r13d
	jle	.LBB14_10
# BB#1:                                 # %.lr.ph46.preheader
	leal	-1(%r13), %r15d
	.p2align	4, 0x90
.LBB14_2:                               # %.lr.ph46
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_3 Depth 2
                                        #     Child Loop BB14_8 Depth 2
	movl	%r13d, %r12d
	leal	-1(%r12), %r13d
	movslq	hsize(%rip), %rax
	movq	page(%rip), %rcx
	movl	%eax, %edx
	imull	%r13d, %edx
	movslq	%edx, %rdx
	leal	-1(%rax), %esi
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB14_3:                               #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, %r14
	movl	%esi, %ebx
	testq	%r14, %r14
	jle	.LBB14_5
# BB#4:                                 #   in Loop: Header=BB14_3 Depth=2
	leaq	-1(%r14), %rdi
	leaq	(%rdx,%rdi), %rbp
	leal	-1(%rbx), %esi
	cmpb	$32, (%rcx,%rbp)
	je	.LBB14_3
.LBB14_5:                               # %.critedge.preheader
                                        #   in Loop: Header=BB14_2 Depth=1
	testl	%r14d, %r14d
	jle	.LBB14_9
# BB#6:                                 # %.critedge.preheader54
                                        #   in Loop: Header=BB14_2 Depth=1
	imull	%r13d, %eax
	cltq
	movzbl	(%rcx,%rax), %edi
	movq	out_fp(%rip), %rsi
	callq	_IO_putc
	cmpl	$1, %r14d
	je	.LBB14_9
# BB#7:                                 # %.critedge..critedge_crit_edge.preheader
                                        #   in Loop: Header=BB14_2 Depth=1
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB14_8:                               # %.critedge..critedge_crit_edge
                                        #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	page(%rip), %rax
	movl	hsize(%rip), %ecx
	imull	%r15d, %ecx
	addl	%ebp, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %edi
	movq	out_fp(%rip), %rsi
	callq	_IO_putc
	incl	%ebp
	decl	%ebx
	jne	.LBB14_8
.LBB14_9:                               # %.critedge._crit_edge
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	out_fp(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	decl	%r15d
	cmpl	$1, %r12d
	jg	.LBB14_2
.LBB14_10:                              # %._crit_edge47
	cmpl	$0, PlainFormFeed(%rip)
	je	.LBB14_12
# BB#11:
	movq	out_fp(%rip), %rsi
	movl	$12, %edi
	callq	_IO_putc
.LBB14_12:
	movl	(%rsp), %eax            # 4-byte Reload
	decl	%eax
	cltd
	idivl	PlainCharWidth(%rip)
	movl	%eax, %r15d
	leal	1(%r15), %r12d
	movl	4(%rsp), %eax           # 4-byte Reload
	decl	%eax
	cltd
	idivl	PlainCharHeight(%rip)
	movl	%eax, %ebx
	leal	1(%rbx), %r14d
	cmpl	hsize(%rip), %r12d
	jne	.LBB14_14
# BB#13:
	cmpl	vsize(%rip), %r14d
	je	.LBB14_15
.LBB14_14:
	movq	page(%rip), %rdi
	callq	free
	movl	%r12d, hsize(%rip)
	movl	%r14d, vsize(%rip)
	movl	%r14d, %eax
	imull	%r12d, %eax
	movslq	%eax, %rdi
	callq	malloc
	movq	%rax, page(%rip)
.LBB14_15:                              # %.preheader36
	testl	%ebx, %ebx
	js	.LBB14_19
# BB#16:                                # %.preheader.lr.ph
	testl	%r15d, %r15d
	js	.LBB14_19
# BB#17:                                # %.preheader.us.preheader
	movq	page(%rip), %r13
	movl	%r15d, %r15d
	incq	%r15
	movslq	%r14d, %r14
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_18:                              # %.preheader.us
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rbx
	leaq	(%r13,%rbx), %rdi
	movl	$32, %esi
	movq	%r15, %rdx
	callq	memset
	incq	%rbp
	addl	%r12d, %ebx
	cmpq	%r14, %rbp
	jl	.LBB14_18
.LBB14_19:                              # %._crit_edge40
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	Plain_PrintBetweenPages, .Lfunc_end14-Plain_PrintBetweenPages
	.cfi_endproc

	.p2align	4, 0x90
	.type	Plain_PrintAfterLastPage,@function
Plain_PrintAfterLastPage:               # @Plain_PrintAfterLastPage
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -48
.Lcfi32:
	.cfi_offset %r12, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	cmpb	$1, prologue_done(%rip)
	jne	.LBB15_11
# BB#1:
	movl	vsize(%rip), %r15d
	testl	%r15d, %r15d
	jle	.LBB15_11
# BB#2:                                 # %.lr.ph21.preheader
	leal	-1(%r15), %r12d
	.p2align	4, 0x90
.LBB15_3:                               # %.lr.ph21
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_4 Depth 2
                                        #     Child Loop BB15_9 Depth 2
	movl	%r15d, %r14d
	leal	-1(%r14), %r15d
	movslq	hsize(%rip), %r8
	movq	page(%rip), %rcx
	movl	%r8d, %edx
	imull	%r15d, %edx
	movslq	%edx, %rdx
	leal	-1(%r8), %esi
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB15_4:                               #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, %rbp
	movl	%esi, %ebx
	testq	%rbp, %rbp
	jle	.LBB15_6
# BB#5:                                 #   in Loop: Header=BB15_4 Depth=2
	leaq	-1(%rbp), %rdi
	leaq	(%rdx,%rdi), %rax
	leal	-1(%rbx), %esi
	cmpb	$32, (%rcx,%rax)
	je	.LBB15_4
.LBB15_6:                               # %.critedge.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	testl	%ebp, %ebp
	jle	.LBB15_10
# BB#7:                                 # %.critedge.preheader25
                                        #   in Loop: Header=BB15_3 Depth=1
	imull	%r15d, %r8d
	movslq	%r8d, %rax
	movzbl	(%rcx,%rax), %edi
	movq	out_fp(%rip), %rsi
	callq	_IO_putc
	cmpl	$1, %ebp
	je	.LBB15_10
# BB#8:                                 # %.critedge..critedge_crit_edge.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB15_9:                               # %.critedge..critedge_crit_edge
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	page(%rip), %rax
	movl	hsize(%rip), %ecx
	imull	%r12d, %ecx
	addl	%ebp, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %edi
	movq	out_fp(%rip), %rsi
	callq	_IO_putc
	incl	%ebp
	decl	%ebx
	jne	.LBB15_9
.LBB15_10:                              # %.critedge._crit_edge
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	out_fp(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	decl	%r12d
	cmpl	$1, %r14d
	jg	.LBB15_3
.LBB15_11:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	Plain_PrintAfterLastPage, .Lfunc_end15-Plain_PrintAfterLastPage
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI16_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.p2align	4, 0x90
	.type	Plain_PrintWord,@function
Plain_PrintWord:                        # @Plain_PrintWord
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 48
.Lcfi41:
	.cfi_offset %rbx, -40
.Lcfi42:
	.cfi_offset %r12, -32
.Lcfi43:
	.cfi_offset %r14, -24
.Lcfi44:
	.cfi_offset %r15, -16
	incl	TotalWordCount(%rip)
	movq	%rdi, %rbx
	cvtsi2ssl	%esi, %xmm0
	cvtsi2ssl	PlainCharWidth(%rip), %xmm1
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	addsd	.LCPI16_0(%rip), %xmm0
	cvttsd2si	%xmm0, %r15d
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	PlainCharHeight(%rip), %xmm1
	divss	%xmm1, %xmm0
	cvttss2si	%xmm0, %r12d
	leaq	64(%rbx), %r14
	testl	%r15d, %r15d
	js	.LBB16_1
# BB#2:
	movq	%r14, %rdi
	callq	strlen
	movl	vsize(%rip), %ecx
	cmpl	%ecx, %r12d
	jge	.LBB16_1
# BB#3:
	testl	%r12d, %r12d
	js	.LBB16_1
# BB#4:
	movslq	%r15d, %rdx
	addq	%rdx, %rax
	movl	hsize(%rip), %edx
	movslq	%edx, %rsi
	cmpq	%rsi, %rax
	jae	.LBB16_1
# BB#5:
	cmpl	%edx, %r15d
	jl	.LBB16_7
# BB#6:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	vsize(%rip), %ecx
.LBB16_7:
	cmpl	%ecx, %r12d
	jl	.LBB16_9
# BB#8:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB16_9:
	movb	(%r14), %al
	testb	%al, %al
	je	.LBB16_12
# BB#10:                                # %.lr.ph.preheader
	imull	hsize(%rip), %r12d
	addl	%r15d, %r12d
	movslq	%r12d, %rcx
	addq	page(%rip), %rcx
	addq	$65, %rbx
	.p2align	4, 0x90
.LBB16_11:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movb	%al, (%rcx)
	incq	%rcx
	movzbl	(%rbx), %eax
	incq	%rbx
	testb	%al, %al
	jne	.LBB16_11
.LBB16_12:                              # %.loopexit
	addq	$8, %rsp
	jmp	.LBB16_13
.LBB16_1:
	addq	$32, %rbx
	movl	$51, %edi
	movl	$1, %esi
	movl	$.L.str.16, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	%rbx, %r8
	movq	%r14, %r9
	pushq	%r12
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$24, %rsp
.Lcfi47:
	.cfi_adjust_cfa_offset -16
.LBB16_13:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	Plain_PrintWord, .Lfunc_end16-Plain_PrintWord
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_0:
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
	.text
	.p2align	4, 0x90
	.type	Plain_PrintPlainGraphic,@function
Plain_PrintPlainGraphic:                # @Plain_PrintPlainGraphic
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi54:
	.cfi_def_cfa_offset 112
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movl	%edx, %r12d
	movl	%esi, %r13d
	movq	%rdi, %r14
	leaq	32(%r14), %r15
	movb	32(%r14), %al
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB17_1
# BB#3:
	addq	$64, %r14
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB17_4
# BB#5:
	movq	%r15, 16(%rsp)          # 8-byte Spill
	cvtsi2ssl	%r13d, %xmm0
	cvtsi2ssl	PlainCharHeight(%rip), %xmm1
	movss	%xmm1, 24(%rsp)         # 4-byte Spill
	movl	56(%rbp), %eax
	movl	60(%rbp), %r13d
	addl	48(%rbp), %eax
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	addss	%xmm0, %xmm1
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movd	PlainCharWidth(%rip), %xmm1 # xmm1 = mem[0],zero,zero,zero
	pshufd	$80, %xmm1, %xmm1       # xmm1 = xmm1[0,0,1,1]
	cvtdq2ps	%xmm1, %xmm1
	divps	%xmm1, %xmm0
	cvtps2pd	%xmm0, %xmm0
	addpd	.LCPI17_0(%rip), %xmm0
	cvttpd2dq	%xmm0, %xmm0
	movapd	%xmm0, 32(%rsp)         # 16-byte Spill
	movd	%xmm0, %r15d
	addl	52(%rbp), %r13d
	xorl	%edi, %edi
	callq	SetLengthDim
	movl	$1, %edi
	callq	SetLengthDim
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movl	%r15d, %edi
	testl	%edi, %edi
	js	.LBB17_19
# BB#6:
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r12d, %xmm0
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	cvttss2si	%xmm1, %ecx
	testl	%ecx, %ecx
	js	.LBB17_19
# BB#7:
	pshufd	$212, 32(%rsp), %xmm1   # 16-byte Folded Reload
                                        # xmm1 = mem[0,1,1,3]
	movl	hsize(%rip), %edx
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	movd	%xmm1, %r10d
	cmpl	%edx, %r10d
	jge	.LBB17_19
# BB#8:
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%r13d, %xmm1
	subss	%xmm1, %xmm0
	divss	%xmm2, %xmm0
	cvttss2si	%xmm0, %esi
	cmpl	vsize(%rip), %esi
	jge	.LBB17_19
# BB#9:                                 # %.preheader55
	cmpl	%esi, %ecx
	jle	.LBB17_20
# BB#10:                                # %.preheader.lr.ph
	cmpl	%r10d, %edi
	jge	.LBB17_20
# BB#11:                                # %.preheader.us.preheader
	movslq	%edx, %r13
	movq	page(%rip), %rax
	movslq	%edi, %r8
	movslq	%ecx, %r11
	movslq	%esi, %r15
	movslq	%r10d, %rcx
	subl	%edi, %r10d
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	-1(%rcx), %r12
	andl	$1, %r10d
	leaq	1(%r8), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	-1(%r11), %rcx
	imulq	%r13, %rcx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	1(%rax,%rcx), %rcx
	movq	%r13, 16(%rsp)          # 8-byte Spill
	negq	%r13
	xorl	%edx, %edx
	xorl	%ebp, %ebp
	movq	24(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB17_12:                              # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_17 Depth 2
	decq	%r11
	testq	%r10, %r10
	jne	.LBB17_14
# BB#13:                                #   in Loop: Header=BB17_12 Depth=1
	movq	%r8, %rsi
	cmpq	%r8, %r12
	jne	.LBB17_16
	jmp	.LBB17_18
	.p2align	4, 0x90
.LBB17_14:                              #   in Loop: Header=BB17_12 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	imulq	%r11, %rsi
	cmpl	%ebx, %ebp
	cmovel	%edx, %ebp
	movslq	%ebp, %rax
	incl	%ebp
	movb	(%r14,%rax), %al
	addq	%r8, %rsi
	movb	%al, (%rdi,%rsi)
	movq	48(%rsp), %rsi          # 8-byte Reload
	cmpq	%r8, %r12
	je	.LBB17_18
.LBB17_16:                              # %.preheader.us.new
                                        #   in Loop: Header=BB17_12 Depth=1
	movq	32(%rsp), %r9           # 8-byte Reload
	subq	%rsi, %r9
	addq	%rcx, %rsi
	.p2align	4, 0x90
.LBB17_17:                              #   Parent Loop BB17_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebx, %ebp
	cmovel	%edx, %ebp
	movslq	%ebp, %rax
	incl	%ebp
	movzbl	(%r14,%rax), %eax
	movb	%al, -1(%rsi)
	cmpl	%ebx, %ebp
	cmovel	%edx, %ebp
	movslq	%ebp, %rax
	incl	%ebp
	movzbl	(%r14,%rax), %eax
	movb	%al, (%rsi)
	addq	$2, %rsi
	addq	$-2, %r9
	jne	.LBB17_17
.LBB17_18:                              # %..loopexit_crit_edge.us
                                        #   in Loop: Header=BB17_12 Depth=1
	addq	%r13, %rcx
	cmpq	%r15, %r11
	jg	.LBB17_12
	jmp	.LBB17_20
.LBB17_1:
	movl	$51, %edi
	movl	$2, %esi
	movl	$.L.str.17, %edx
	jmp	.LBB17_2
.LBB17_19:
	movl	$51, %edi
	movl	$4, %esi
	movl	$.L.str.20, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%r14, %r9
	callq	Error
.LBB17_20:                              # %.loopexit56
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_4:
	movl	$51, %edi
	movl	$3, %esi
	movl	$.L.str.19, %edx
.LBB17_2:
	movl	$2, %ecx
	movl	$.L.str.18, %r9d
	xorl	%eax, %eax
	movq	%r15, %r8
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	Error                   # TAILCALL
.Lfunc_end17:
	.size	Plain_PrintPlainGraphic, .Lfunc_end17-Plain_PrintPlainGraphic
	.cfi_endproc

	.p2align	4, 0x90
	.type	Plain_PrintUnderline,@function
Plain_PrintUnderline:                   # @Plain_PrintUnderline
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end18:
	.size	Plain_PrintUnderline, .Lfunc_end18-Plain_PrintUnderline
	.cfi_endproc

	.p2align	4, 0x90
	.type	Plain_CoordRotate,@function
Plain_CoordRotate:                      # @Plain_CoordRotate
	.cfi_startproc
# BB#0:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.21, %r9d
	xorl	%eax, %eax
	jmp	Error                   # TAILCALL
.Lfunc_end19:
	.size	Plain_CoordRotate, .Lfunc_end19-Plain_CoordRotate
	.cfi_endproc

	.p2align	4, 0x90
	.type	Plain_LinkSource,@function
Plain_LinkSource:                       # @Plain_LinkSource
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end20:
	.size	Plain_LinkSource, .Lfunc_end20-Plain_LinkSource
	.cfi_endproc

	.p2align	4, 0x90
	.type	Plain_LinkDest,@function
Plain_LinkDest:                         # @Plain_LinkDest
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end21:
	.size	Plain_LinkDest, .Lfunc_end21-Plain_LinkDest
	.cfi_endproc

	.p2align	4, 0x90
	.type	Plain_LinkCheck,@function
Plain_LinkCheck:                        # @Plain_LinkCheck
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end22:
	.size	Plain_LinkCheck, .Lfunc_end22-Plain_LinkCheck
	.cfi_endproc

	.type	out_fp,@object          # @out_fp
	.local	out_fp
	.comm	out_fp,8,8
	.type	prologue_done,@object   # @prologue_done
	.local	prologue_done
	.comm	prologue_done,1,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Plain_CoordTranslate: should never be called!"
	.size	.L.str.1, 46

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Plain_CoordScale: should never be called!"
	.size	.L.str.2, 42

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Plain_SaveGraphicState: should never be called!"
	.size	.L.str.3, 48

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Plain_RestoreGraphicState: should never be called!"
	.size	.L.str.4, 51

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Plain_PrintGraphicObject: should never be called!"
	.size	.L.str.5, 50

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Plain_DefineGraphicNames: should never be called!"
	.size	.L.str.6, 50

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Plain_SaveTranslateDefineSave: should never be called!"
	.size	.L.str.7, 55

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Plain_PrintGraphicInclude: should never be called!"
	.size	.L.str.8, 51

	.type	plain_back,@object      # @plain_back
	.data
	.p2align	3
plain_back:
	.long	2                       # 0x2
	.zero	4
	.quad	.L.str.9
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	Plain_PrintInitialize
	.quad	Plain_PrintLength
	.quad	Plain_PrintPageSetupForFont
	.quad	Plain_PrintPageResourceForFont
	.quad	Plain_PrintMapping
	.quad	Plain_PrintBeforeFirstPage
	.quad	Plain_PrintBetweenPages
	.quad	Plain_PrintAfterLastPage
	.quad	Plain_PrintWord
	.quad	Plain_PrintPlainGraphic
	.quad	Plain_PrintUnderline
	.quad	Plain_CoordTranslate
	.quad	Plain_CoordRotate
	.quad	Plain_CoordScale
	.quad	Plain_SaveGraphicState
	.quad	Plain_RestoreGraphicState
	.quad	Plain_PrintGraphicObject
	.quad	Plain_DefineGraphicNames
	.quad	Plain_SaveTranslateDefineSave
	.quad	Plain_PrintGraphicInclude
	.quad	Plain_LinkSource
	.quad	Plain_LinkDest
	.quad	Plain_LinkCheck
	.size	plain_back, 232

	.type	Plain_BackEnd,@object   # @Plain_BackEnd
	.globl	Plain_BackEnd
	.p2align	3
Plain_BackEnd:
	.quad	plain_back
	.size	Plain_BackEnd, 8

	.type	PlainCharWidth,@object  # @PlainCharWidth
	.comm	PlainCharWidth,4,4
	.type	PlainCharHeight,@object # @PlainCharHeight
	.comm	PlainCharHeight,4,4
	.type	PlainFormFeed,@object   # @PlainFormFeed
	.comm	PlainFormFeed,4,4
	.type	.L.str.9,@object        # @.str.9
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.9:
	.asciz	"PlainText"
	.size	.L.str.9, 10

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%.2fs"
	.size	.L.str.10, 6

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%.2ff"
	.size	.L.str.11, 6

	.type	hsize,@object           # @hsize
	.local	hsize
	.comm	hsize,4,4
	.type	vsize,@object           # @vsize
	.local	vsize
	.comm	vsize,4,4
	.type	page,@object            # @page
	.local	page
	.comm	page,8,8
	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"PrintWord:  h >= hsize!"
	.size	.L.str.13, 24

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"PrintWord:  v >= vsize!"
	.size	.L.str.15, 24

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"word %s deleted (internal error, off page at %d,%d)"
	.size	.L.str.16, 52

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"left parameter of %s must be a simple word"
	.size	.L.str.17, 43

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"@PlainGraphic"
	.size	.L.str.18, 14

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"left parameter of %s must be a non-empty word"
	.size	.L.str.19, 46

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"fill %s deleted (internal error, off page at %d,%d)"
	.size	.L.str.20, 52

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Plain_CoordRotate: should never be called!"
	.size	.L.str.21, 43


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
