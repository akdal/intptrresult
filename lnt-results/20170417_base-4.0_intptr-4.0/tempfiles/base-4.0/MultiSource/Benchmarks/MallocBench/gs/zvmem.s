	.text
	.file	"zvmem.bc"
	.globl	zsave
	.p2align	4, 0x90
	.type	zsave,@function
zsave:                                  # @zsave
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	xorl	%eax, %eax
	callq	zgsave
	movl	%eax, %ebx
	movl	$1, %edi
	movl	$24, %esi
	movl	$.L.str, %edx
	callq	alloc
	movq	%rax, %r15
	callq	alloc_save_state
	testl	%ebx, %ebx
	js	.LBB0_6
# BB#1:
	testq	%r15, %r15
	movl	$-25, %ebx
	je	.LBB0_6
# BB#2:
	testq	%rax, %rax
	je	.LBB0_6
# BB#3:
	movq	%rax, (%r15)
	movq	istate(%rip), %rax
	movq	%rax, 8(%r15)
	movq	$0, istate(%rip)
	movq	igs(%rip), %rdi
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	callq	gs_state_swap_saved
	movq	%rax, 16(%r15)
	leaq	16(%r14), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB0_5
# BB#4:
	movq	%r14, osp(%rip)
	movl	$-16, %ebx
	jmp	.LBB0_6
.LBB0_5:
	movq	%r15, 16(%r14)
	movw	$48, 24(%r14)
.LBB0_6:
	movl	%ebx, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	zsave, .Lfunc_end0-zsave
	.cfi_endproc

	.globl	zrestore
	.p2align	4, 0x90
	.type	zrestore,@function
zrestore:                               # @zrestore
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 64
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movzwl	8(%r15), %eax
	andl	$252, %eax
	movl	$-20, %ebp
	cmpl	$48, %eax
	jne	.LBB1_27
# BB#1:
	movq	(%r15), %r14
	movq	(%r14), %rbx
	movq	osbot(%rip), %rbp
	cmpq	%r15, %rbp
	jae	.LBB1_7
# BB#2:                                 # %.lr.ph.i.preheader
	movl	$62613, %r12d           # imm = 0xF495
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	8(%rbp), %eax
	shrl	$2, %eax
	movl	%eax, %ecx
	andb	$63, %cl
	cmpb	$15, %cl
	ja	.LBB1_6
# BB#4:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB1_3 Depth=1
	andl	$63, %eax
	btq	%rax, %r12
	jae	.LBB1_6
# BB#5:                                 #   in Loop: Header=BB1_3 Depth=1
	movq	(%rbp), %rdi
	movq	%rbx, %rsi
	callq	alloc_is_since_save
	testl	%eax, %eax
	jne	.LBB1_23
.LBB1_6:                                # %.thread.i
                                        #   in Loop: Header=BB1_3 Depth=1
	addq	$16, %rbp
	cmpq	%r15, %rbp
	jb	.LBB1_3
.LBB1_7:                                # %.loopexit51
	movq	esp(%rip), %r12
	leaq	16(%r12), %rax
	movl	$estack, %ecx
	cmpq	%rcx, %rax
	jbe	.LBB1_13
# BB#8:                                 # %.lr.ph.i29.preheader
	movl	$estack-16, %ebp
	movl	$62613, %r13d           # imm = 0xF495
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph.i29
                                        # =>This Inner Loop Header: Depth=1
	movzwl	24(%rbp), %eax
	shrl	$2, %eax
	movl	%eax, %ecx
	andb	$63, %cl
	cmpb	$15, %cl
	ja	.LBB1_12
# BB#10:                                # %.lr.ph.i29
                                        #   in Loop: Header=BB1_9 Depth=1
	andl	$63, %eax
	btq	%rax, %r13
	jae	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_9 Depth=1
	movq	16(%rbp), %rdi
	movq	%rbx, %rsi
	callq	alloc_is_since_save
	testl	%eax, %eax
	jne	.LBB1_23
.LBB1_12:                               # %.thread.i32
                                        #   in Loop: Header=BB1_9 Depth=1
	addq	$16, %rbp
	cmpq	%r12, %rbp
	jb	.LBB1_9
.LBB1_13:                               # %.loopexit49
	movq	dsp(%rip), %r12
	leaq	16(%r12), %rax
	movl	$dstack, %ecx
	cmpq	%rcx, %rax
	jbe	.LBB1_19
# BB#14:                                # %.lr.ph.i37.preheader
	movl	$dstack-16, %ebp
	movl	$62613, %r13d           # imm = 0xF495
	.p2align	4, 0x90
.LBB1_15:                               # %.lr.ph.i37
                                        # =>This Inner Loop Header: Depth=1
	movzwl	24(%rbp), %eax
	shrl	$2, %eax
	movl	%eax, %ecx
	andb	$63, %cl
	cmpb	$15, %cl
	ja	.LBB1_18
# BB#16:                                # %.lr.ph.i37
                                        #   in Loop: Header=BB1_15 Depth=1
	andl	$63, %eax
	btq	%rax, %r13
	jae	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_15 Depth=1
	movq	16(%rbp), %rdi
	movq	%rbx, %rsi
	callq	alloc_is_since_save
	testl	%eax, %eax
	jne	.LBB1_23
.LBB1_18:                               # %.thread.i40
                                        #   in Loop: Header=BB1_15 Depth=1
	addq	$16, %rbp
	cmpq	%r12, %rbp
	jb	.LBB1_15
.LBB1_19:                               # %.loopexit
	movq	%rbx, %rdi
	callq	alloc_restore_state_check
	testl	%eax, %eax
	js	.LBB1_23
# BB#20:
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	zgrestoreall
	movq	%rbx, %rdi
	callq	alloc_restore_state
	movq	8(%r14), %rax
	movq	%rax, istate(%rip)
	movq	igs(%rip), %rdi
	movq	16(%r14), %rsi
	callq	gs_state_swap_saved
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	zgrestore
	testl	%eax, %eax
	js	.LBB1_26
# BB#21:
	movl	$1, %esi
	movl	$24, %edx
	movl	$.L.str.1, %ecx
	movq	%r14, %rdi
	callq	alloc_free
	addq	$-16, osp(%rip)
	jmp	.LBB1_27
.LBB1_23:
	movl	$-11, %ebp
.LBB1_27:                               # %restore_check_stack.exit.thread46
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_26:
	movl	%eax, %ebp
	jmp	.LBB1_27
.Lfunc_end1:
	.size	zrestore, .Lfunc_end1-zrestore
	.cfi_endproc

	.globl	restore_check_stack
	.p2align	4, 0x90
	.type	restore_check_stack,@function
restore_check_stack:                    # @restore_check_stack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 48
.Lcfi24:
	.cfi_offset %rbx, -48
.Lcfi25:
	.cfi_offset %r12, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	cmpq	%rbp, %rbx
	jae	.LBB2_7
# BB#1:                                 # %.lr.ph.preheader
	movl	$62613, %r12d           # imm = 0xF495
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	8(%rbx), %eax
	shrl	$2, %eax
	movl	%eax, %ecx
	andb	$63, %cl
	cmpb	$15, %cl
	ja	.LBB2_6
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB2_2 Depth=1
	andl	$63, %eax
	btq	%rax, %r12
	jae	.LBB2_6
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	callq	alloc_is_since_save
	testl	%eax, %eax
	jne	.LBB2_5
.LBB2_6:                                # %.thread
                                        #   in Loop: Header=BB2_2 Depth=1
	addq	$16, %rbx
	cmpq	%rbp, %rbx
	jb	.LBB2_2
	jmp	.LBB2_7
.LBB2_5:
	movl	$-11, %r14d
.LBB2_7:                                # %._crit_edge
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	restore_check_stack, .Lfunc_end2-restore_check_stack
	.cfi_endproc

	.globl	zvmstatus
	.p2align	4, 0x90
	.type	zvmstatus,@function
zvmstatus:                              # @zvmstatus
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	8(%rsp), %rdi
	movq	%rsp, %rsi
	callq	alloc_status
	leaq	48(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB3_2
# BB#1:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB3_3
.LBB3_2:
	callq	alloc_save_level
	cltq
	movq	%rax, 16(%rbx)
	movw	$20, 24(%rbx)
	movq	8(%rsp), %rax
	movq	%rax, 32(%rbx)
	movw	$20, 40(%rbx)
	movq	(%rsp), %rax
	movq	%rax, 48(%rbx)
	movw	$20, 56(%rbx)
	xorl	%eax, %eax
.LBB3_3:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end3:
	.size	zvmstatus, .Lfunc_end3-zvmstatus
	.cfi_endproc

	.globl	zvmem_op_init
	.p2align	4, 0x90
	.type	zvmem_op_init,@function
zvmem_op_init:                          # @zvmem_op_init
	.cfi_startproc
# BB#0:
	movl	$zvmem_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end4:
	.size	zvmem_op_init, .Lfunc_end4-zvmem_op_init
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"zsave"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"zrestore"
	.size	.L.str.1, 9

	.type	zvmem_op_init.my_defs,@object # @zvmem_op_init.my_defs
	.data
	.p2align	4
zvmem_op_init.my_defs:
	.quad	.L.str.2
	.quad	zrestore
	.quad	.L.str.3
	.quad	zsave
	.quad	.L.str.4
	.quad	zvmstatus
	.zero	16
	.size	zvmem_op_init.my_defs, 64

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"1restore"
	.size	.L.str.2, 9

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"0save"
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"0vmstatus"
	.size	.L.str.4, 10


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
