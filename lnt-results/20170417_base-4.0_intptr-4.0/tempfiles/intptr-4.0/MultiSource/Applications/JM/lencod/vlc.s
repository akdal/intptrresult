	.text
	.file	"vlc.bc"
	.globl	ue_v
	.p2align	4, 0x90
	.type	ue_v,@function
ue_v:                                   # @ue_v
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	leal	1(%rsi), %r8d
	addl	$2, %esi
	xorl	%ecx, %ecx
	cmpl	$3, %esi
	jb	.LBB0_4
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%ecx, %ecx
	movl	%r8d, %eax
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	$15, %ecx
	jg	.LBB0_4
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%eax, %esi
	shrl	$31, %esi
	addl	%eax, %esi
	sarl	%esi
	leal	1(%rsi), %eax
	cmpl	$2, %eax
	movl	%esi, %eax
	ja	.LBB0_2
.LBB0_4:                                # %ue_linfo.exit
	leal	1(%rcx,%rcx), %eax
	movl	$1, %r10d
	movl	$1, %edi
	shll	%cl, %edi
	movl	%eax, %esi
	shrl	$31, %esi
	leal	1(%rsi,%rcx,2), %ecx
	shrl	%ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	testl	%eax, %eax
	jle	.LBB0_9
# BB#5:                                 # %.lr.ph.i11
	subl	%edi, %r8d
	leal	-1(%rsi), %r9d
	andl	%r8d, %r9d
	orl	%esi, %r9d
	leal	-1(%rax), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r10d
	movb	8(%rdx), %cl
	movl	%eax, %esi
	.p2align	4, 0x90
.LBB0_6:                                # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%r9d, %r10d
	setne	%dil
	orb	%cl, %dil
	movb	%dil, 8(%rdx)
	decl	4(%rdx)
	jne	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=1
	movl	$8, 4(%rdx)
	movq	32(%rdx), %r8
	movslq	(%rdx), %r11
	leal	1(%r11), %ecx
	movl	%ecx, (%rdx)
	movb	%dil, (%r8,%r11)
	movb	$0, 8(%rdx)
	xorl	%edi, %edi
.LBB0_8:                                #   in Loop: Header=BB0_6 Depth=1
	shrl	%r10d
	decl	%esi
	movl	%edi, %ecx
	jne	.LBB0_6
.LBB0_9:                                # %writeUVLC2buffer.exit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end0:
	.size	ue_v, .Lfunc_end0-ue_v
	.cfi_endproc

	.globl	ue_linfo
	.p2align	4, 0x90
	.type	ue_linfo,@function
ue_linfo:                               # @ue_linfo
	.cfi_startproc
# BB#0:
	movq	%rcx, %r8
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	1(%rdi), %esi
	addl	$2, %edi
	xorl	%ecx, %ecx
	cmpl	$3, %edi
	jb	.LBB1_4
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	movl	%esi, %edi
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	$15, %ecx
	jg	.LBB1_4
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	sarl	%eax
	leal	1(%rax), %edi
	cmpl	$2, %edi
	movl	%eax, %edi
	ja	.LBB1_2
.LBB1_4:                                # %._crit_edge
	leal	1(%rcx,%rcx), %eax
	movl	%eax, (%rdx)
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	subl	%eax, %esi
	movl	%esi, (%r8)
	retq
.Lfunc_end1:
	.size	ue_linfo, .Lfunc_end1-ue_linfo
	.cfi_endproc

	.globl	symbol2uvlc
	.p2align	4, 0x90
	.type	symbol2uvlc,@function
symbol2uvlc:                            # @symbol2uvlc
	.cfi_startproc
# BB#0:
	movl	12(%rdi), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	shrl	%ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	leal	-1(%rax), %ecx
	andl	16(%rdi), %ecx
	orl	%eax, %ecx
	movl	%ecx, 20(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	symbol2uvlc, .Lfunc_end2-symbol2uvlc
	.cfi_endproc

	.globl	writeUVLC2buffer
	.p2align	4, 0x90
	.type	writeUVLC2buffer,@function
writeUVLC2buffer:                       # @writeUVLC2buffer
	.cfi_startproc
# BB#0:
	movl	12(%rdi), %r8d
	testl	%r8d, %r8d
	jle	.LBB3_7
# BB#1:                                 # %.lr.ph
	leal	-1(%r8), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movb	8(%rsi), %r10b
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	addb	%r10b, %r10b
	movb	%r10b, 8(%rsi)
	testl	20(%rdi), %eax
	je	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	orb	$1, %r10b
	movb	%r10b, 8(%rsi)
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	decl	4(%rsi)
	jne	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	$8, 4(%rsi)
	movq	32(%rsi), %r8
	movslq	(%rsi), %r9
	leal	1(%r9), %edx
	movl	%edx, (%rsi)
	movb	%r10b, (%r8,%r9)
	movb	$0, 8(%rsi)
	movl	12(%rdi), %r8d
	xorl	%r10d, %r10d
.LBB3_6:                                #   in Loop: Header=BB3_2 Depth=1
	shrl	%eax
	incl	%ecx
	cmpl	%r8d, %ecx
	jl	.LBB3_2
.LBB3_7:                                # %._crit_edge
	retq
.Lfunc_end3:
	.size	writeUVLC2buffer, .Lfunc_end3-writeUVLC2buffer
	.cfi_endproc

	.globl	se_v
	.p2align	4, 0x90
	.type	se_v,@function
se_v:                                   # @se_v
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	negl	%eax
	cmovll	%esi, %eax
	xorl	%r8d, %r8d
	testl	%esi, %esi
	setle	%r9b
	leal	(%rax,%rax), %r11d
	leal	1(%rax,%rax), %eax
	xorl	%ecx, %ecx
	cmpl	$3, %eax
	jb	.LBB4_4
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%ecx, %ecx
	movl	%r11d, %eax
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	$15, %ecx
	jg	.LBB4_4
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	%eax, %esi
	shrl	$31, %esi
	addl	%eax, %esi
	sarl	%esi
	leal	1(%rsi), %eax
	cmpl	$2, %eax
	movl	%esi, %eax
	ja	.LBB4_2
.LBB4_4:                                # %se_linfo.exit
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	leal	1(%rcx,%rcx), %eax
	movl	$-1, %r10d
	shll	%cl, %r10d
	movl	%eax, %esi
	shrl	$31, %esi
	leal	1(%rsi,%rcx,2), %ecx
	shrl	%ecx
	movl	$1, %ebx
	movl	$1, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	testl	%eax, %eax
	jle	.LBB4_9
# BB#5:                                 # %.lr.ph.i11
	movb	%r9b, %r8b
	orl	%r8d, %r11d
	addl	%r11d, %r10d
	leal	-1(%rdi), %r8d
	andl	%r10d, %r8d
	orl	%edi, %r8d
	leal	-1(%rax), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	movb	8(%rdx), %cl
	movl	%eax, %esi
	.p2align	4, 0x90
.LBB4_6:                                # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%r8d, %ebx
	setne	%dil
	orb	%cl, %dil
	movb	%dil, 8(%rdx)
	decl	4(%rdx)
	jne	.LBB4_8
# BB#7:                                 #   in Loop: Header=BB4_6 Depth=1
	movl	$8, 4(%rdx)
	movq	32(%rdx), %r9
	movslq	(%rdx), %r10
	leal	1(%r10), %ecx
	movl	%ecx, (%rdx)
	movb	%dil, (%r9,%r10)
	movb	$0, 8(%rdx)
	xorl	%edi, %edi
.LBB4_8:                                #   in Loop: Header=BB4_6 Depth=1
	shrl	%ebx
	decl	%esi
	movl	%edi, %ecx
	jne	.LBB4_6
.LBB4_9:                                # %writeUVLC2buffer.exit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	retq
.Lfunc_end4:
	.size	se_v, .Lfunc_end4-se_v
	.cfi_endproc

	.globl	se_linfo
	.p2align	4, 0x90
	.type	se_linfo,@function
se_linfo:                               # @se_linfo
	.cfi_startproc
# BB#0:
	movq	%rcx, %r8
	movl	%edi, %ecx
	negl	%ecx
	cmovll	%edi, %ecx
	xorl	%r9d, %r9d
	testl	%edi, %edi
	setle	%r9b
	leal	(%rcx,%rcx), %eax
	leal	1(%rcx,%rcx), %edi
	xorl	%ecx, %ecx
	cmpl	$3, %edi
	jb	.LBB5_4
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	movl	%eax, %edi
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	$15, %ecx
	jg	.LBB5_4
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	%edi, %esi
	shrl	$31, %esi
	addl	%edi, %esi
	sarl	%esi
	leal	1(%rsi), %edi
	cmpl	$2, %edi
	movl	%esi, %edi
	ja	.LBB5_2
.LBB5_4:                                # %._crit_edge
	leal	1(%rcx,%rcx), %esi
	movl	%esi, (%rdx)
	movl	$-1, %edx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edx
	orl	%r9d, %eax
	addl	%edx, %eax
	movl	%eax, (%r8)
	retq
.Lfunc_end5:
	.size	se_linfo, .Lfunc_end5-se_linfo
	.cfi_endproc

	.globl	u_1
	.p2align	4, 0x90
	.type	u_1,@function
u_1:                                    # @u_1
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	movb	8(%rdx), %al
	addb	%al, %al
	andb	$1, %sil
	orb	%al, %sil
	movb	%sil, 8(%rdx)
	decl	4(%rdx)
	jne	.LBB6_2
# BB#1:
	movl	$8, 4(%rdx)
	movq	32(%rdx), %rax
	movslq	(%rdx), %rcx
	leal	1(%rcx), %edi
	movl	%edi, (%rdx)
	movb	%sil, (%rax,%rcx)
	movb	$0, 8(%rdx)
.LBB6_2:                                # %writeUVLC2buffer.exit19
	movl	$1, %eax
	retq
.Lfunc_end6:
	.size	u_1, .Lfunc_end6-u_1
	.cfi_endproc

	.globl	u_v
	.p2align	4, 0x90
	.type	u_v,@function
u_v:                                    # @u_v
	.cfi_startproc
# BB#0:
	movq	%rcx, %r8
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB7_5
# BB#1:                                 # %.lr.ph.i
	leal	-1(%rdi), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movb	8(%r8), %cl
	movl	%edi, %r9d
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%edx, %esi
	setne	%al
	orb	%cl, %al
	movb	%al, 8(%r8)
	decl	4(%r8)
	jne	.LBB7_4
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	$8, 4(%r8)
	movq	32(%r8), %r10
	movslq	(%r8), %r11
	leal	1(%r11), %ecx
	movl	%ecx, (%r8)
	movb	%al, (%r10,%r11)
	movb	$0, 8(%r8)
	xorl	%eax, %eax
.LBB7_4:                                #   in Loop: Header=BB7_2 Depth=1
	shrl	%esi
	decl	%r9d
	movl	%eax, %ecx
	jne	.LBB7_2
.LBB7_5:                                # %writeUVLC2buffer.exit
	movl	%edi, %eax
	retq
.Lfunc_end7:
	.size	u_v, .Lfunc_end7-u_v
	.cfi_endproc

	.globl	cbp_linfo_intra
	.p2align	4, 0x90
	.type	cbp_linfo_intra,@function
cbp_linfo_intra:                        # @cbp_linfo_intra
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	xorl	%esi, %esi
	cmpl	$0, 15536(%rax)
	movq	%rcx, %r8
	setne	%sil
	movslq	%edi, %rcx
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$5, %rsi
	movzbl	NCBP(%rsi,%rcx,2), %ecx
	leal	1(%rcx), %esi
	testl	%ecx, %ecx
	je	.LBB8_1
# BB#2:                                 # %.lr.ph.i.preheader
	xorl	%ecx, %ecx
	movl	%esi, %edi
	.p2align	4, 0x90
.LBB8_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	$15, %ecx
	jg	.LBB8_5
# BB#4:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB8_3 Depth=1
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	sarl	%eax
	leal	1(%rax), %edi
	cmpl	$2, %edi
	movl	%eax, %edi
	ja	.LBB8_3
	jmp	.LBB8_5
.LBB8_1:
	xorl	%ecx, %ecx
.LBB8_5:                                # %ue_linfo.exit
	leal	1(%rcx,%rcx), %eax
	movl	%eax, (%rdx)
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	subl	%eax, %esi
	movl	%esi, (%r8)
	retq
.Lfunc_end8:
	.size	cbp_linfo_intra, .Lfunc_end8-cbp_linfo_intra
	.cfi_endproc

	.globl	cbp_linfo_inter
	.p2align	4, 0x90
	.type	cbp_linfo_inter,@function
cbp_linfo_inter:                        # @cbp_linfo_inter
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	xorl	%esi, %esi
	cmpl	$0, 15536(%rax)
	movq	%rcx, %r8
	setne	%sil
	movslq	%edi, %rcx
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$5, %rsi
	movzbl	NCBP+1(%rsi,%rcx,2), %ecx
	leal	1(%rcx), %esi
	testl	%ecx, %ecx
	je	.LBB9_1
# BB#2:                                 # %.lr.ph.i.preheader
	xorl	%ecx, %ecx
	movl	%esi, %edi
	.p2align	4, 0x90
.LBB9_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	$15, %ecx
	jg	.LBB9_5
# BB#4:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB9_3 Depth=1
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	sarl	%eax
	leal	1(%rax), %edi
	cmpl	$2, %edi
	movl	%eax, %edi
	ja	.LBB9_3
	jmp	.LBB9_5
.LBB9_1:
	xorl	%ecx, %ecx
.LBB9_5:                                # %ue_linfo.exit
	leal	1(%rcx,%rcx), %eax
	movl	%eax, (%rdx)
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	subl	%eax, %esi
	movl	%esi, (%r8)
	retq
.Lfunc_end9:
	.size	cbp_linfo_inter, .Lfunc_end9-cbp_linfo_inter
	.cfi_endproc

	.globl	levrun_linfo_c2x2
	.p2align	4, 0x90
	.type	levrun_linfo_c2x2,@function
levrun_linfo_c2x2:                      # @levrun_linfo_c2x2
	.cfi_startproc
# BB#0:
	movq	%rcx, %r8
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%edi, %edi
	je	.LBB10_1
# BB#2:
	movl	%edi, %eax
	negl	%eax
	cmovll	%edi, %eax
	xorl	%r9d, %r9d
	testl	%edi, %edi
	setle	%r10b
	movslq	%esi, %rdi
	movl	%eax, %ecx
	subl	levrun_linfo_c2x2.LEVRUN(,%rdi,4), %ecx
	jle	.LBB10_3
# BB#4:
	addl	%esi, %esi
	leal	(%rsi,%rcx,8), %esi
	jmp	.LBB10_5
.LBB10_1:
	movl	$1, (%rdx)
	retq
.LBB10_3:
	cltq
	shlq	$2, %rdi
	movl	levrun_linfo_c2x2.NTAB-8(%rdi,%rax,8), %esi
	incl	%esi
.LBB10_5:
	movb	%r10b, %r9b
	leal	1(%rsi), %eax
	xorl	%ecx, %ecx
	cmpl	$3, %eax
	jb	.LBB10_9
# BB#6:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	movl	%esi, %edi
	.p2align	4, 0x90
.LBB10_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	$15, %ecx
	jg	.LBB10_9
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB10_7 Depth=1
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	sarl	%eax
	leal	1(%rax), %edi
	cmpl	$2, %edi
	movl	%eax, %edi
	ja	.LBB10_7
.LBB10_9:                               # %._crit_edge
	leal	1(%rcx,%rcx), %eax
	movl	%eax, (%rdx)
	movl	$-1, %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	addl	%r9d, %esi
	addl	%eax, %esi
	movl	%esi, (%r8)
	retq
.Lfunc_end10:
	.size	levrun_linfo_c2x2, .Lfunc_end10-levrun_linfo_c2x2
	.cfi_endproc

	.globl	levrun_linfo_inter
	.p2align	4, 0x90
	.type	levrun_linfo_inter,@function
levrun_linfo_inter:                     # @levrun_linfo_inter
	.cfi_startproc
# BB#0:
	movq	%rcx, %r8
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%edi, %edi
	je	.LBB11_1
# BB#2:
	movl	%edi, %ecx
	negl	%ecx
	cmovll	%edi, %ecx
	xorl	%r10d, %r10d
	testl	%edi, %edi
	setle	%r9b
	movslq	%esi, %r11
	movzbl	levrun_linfo_inter.LEVRUN(%r11), %edi
	movl	%ecx, %eax
	subl	%edi, %eax
	jle	.LBB11_3
# BB#4:
	shll	$5, %eax
	leal	(%rax,%rsi,2), %esi
	jmp	.LBB11_5
.LBB11_1:
	movl	$1, (%rdx)
	retq
.LBB11_3:
	decl	%ecx
	movslq	%ecx, %rax
	leaq	(%rax,%rax,4), %rax
	movzbl	levrun_linfo_inter.NTAB(%r11,%rax,2), %esi
	incl	%esi
.LBB11_5:
	movb	%r9b, %r10b
	leal	1(%rsi), %eax
	xorl	%ecx, %ecx
	cmpl	$3, %eax
	jb	.LBB11_9
# BB#6:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	movl	%esi, %edi
	.p2align	4, 0x90
.LBB11_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	$15, %ecx
	jg	.LBB11_9
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB11_7 Depth=1
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	sarl	%eax
	leal	1(%rax), %edi
	cmpl	$2, %edi
	movl	%eax, %edi
	ja	.LBB11_7
.LBB11_9:                               # %._crit_edge
	leal	1(%rcx,%rcx), %eax
	movl	%eax, (%rdx)
	movl	$-1, %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	addl	%r10d, %esi
	addl	%eax, %esi
	movl	%esi, (%r8)
	retq
.Lfunc_end11:
	.size	levrun_linfo_inter, .Lfunc_end11-levrun_linfo_inter
	.cfi_endproc

	.globl	writeSE_UVLC
	.p2align	4, 0x90
	.type	writeSE_UVLC,@function
writeSE_UVLC:                           # @writeSE_UVLC
	.cfi_startproc
# BB#0:
	movl	4(%rdi), %eax
	leal	1(%rax), %r10d
	addl	$2, %eax
	xorl	%ecx, %ecx
	cmpl	$3, %eax
	jb	.LBB12_4
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%ecx, %ecx
	movl	%r10d, %eax
	.p2align	4, 0x90
.LBB12_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	$15, %ecx
	jg	.LBB12_4
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB12_2 Depth=1
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%eax, %edx
	sarl	%edx
	leal	1(%rdx), %eax
	cmpl	$2, %eax
	movl	%edx, %eax
	ja	.LBB12_2
.LBB12_4:                               # %ue_linfo.exit
	leal	1(%rcx,%rcx), %r8d
	movl	%r8d, 12(%rdi)
	movl	$1, %r9d
	movl	$1, %eax
	shll	%cl, %eax
	subl	%eax, %r10d
	movl	%r10d, 16(%rdi)
	movl	%r8d, %eax
	shrl	$31, %eax
	leal	1(%rax,%rcx,2), %ecx
	shrl	%ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	leal	-1(%rdx), %eax
	andl	%r10d, %eax
	orl	%edx, %eax
	movl	%eax, 20(%rdi)
	testl	%r8d, %r8d
	jle	.LBB12_12
# BB#5:                                 # %.lr.ph.i9
	movq	(%rsi), %rdx
	leal	-1(%r8), %ecx
	movl	$1, %r10d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r10d
	movb	8(%rdx), %cl
	jmp	.LBB12_6
	.p2align	4, 0x90
.LBB12_11:                              # %._crit_edge
                                        #   in Loop: Header=BB12_6 Depth=1
	shrl	%r10d
	movl	20(%rdi), %eax
	incl	%r9d
.LBB12_6:                               # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%r10d, %eax
	je	.LBB12_8
# BB#7:                                 #   in Loop: Header=BB12_6 Depth=1
	orb	$1, %cl
.LBB12_8:                               #   in Loop: Header=BB12_6 Depth=1
	movb	%cl, 8(%rdx)
	decl	4(%rdx)
	jne	.LBB12_10
# BB#9:                                 #   in Loop: Header=BB12_6 Depth=1
	movl	$8, 4(%rdx)
	movq	32(%rdx), %r8
	movslq	(%rdx), %r11
	leal	1(%r11), %eax
	movl	%eax, (%rdx)
	movb	%cl, (%r8,%r11)
	movb	$0, 8(%rdx)
	movl	12(%rdi), %r8d
	xorl	%ecx, %ecx
.LBB12_10:                              #   in Loop: Header=BB12_6 Depth=1
	cmpl	%r8d, %r9d
	jl	.LBB12_11
.LBB12_12:                              # %writeUVLC2buffer.exit
	cmpl	$0, (%rdi)
	je	.LBB12_14
# BB#13:
	movq	(%rsi), %rax
	movl	$1, 40(%rax)
.LBB12_14:
	retq
.Lfunc_end12:
	.size	writeSE_UVLC, .Lfunc_end12-writeSE_UVLC
	.cfi_endproc

	.globl	writeSE_SVLC
	.p2align	4, 0x90
	.type	writeSE_SVLC,@function
writeSE_SVLC:                           # @writeSE_SVLC
	.cfi_startproc
# BB#0:
	movl	4(%rdi), %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	xorl	%r9d, %r9d
	testl	%eax, %eax
	setle	%r9b
	leal	(%rcx,%rcx), %r10d
	leal	1(%rcx,%rcx), %eax
	xorl	%ecx, %ecx
	cmpl	$3, %eax
	jb	.LBB13_4
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%ecx, %ecx
	movl	%r10d, %eax
	.p2align	4, 0x90
.LBB13_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	$15, %ecx
	jg	.LBB13_4
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB13_2 Depth=1
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%eax, %edx
	sarl	%edx
	leal	1(%rdx), %eax
	cmpl	$2, %eax
	movl	%edx, %eax
	ja	.LBB13_2
.LBB13_4:                               # %se_linfo.exit
	leal	1(%rcx,%rcx), %r8d
	movl	%r8d, 12(%rdi)
	movl	$-1, %eax
	shll	%cl, %eax
	orl	%r9d, %r10d
	addl	%eax, %r10d
	movl	%r10d, 16(%rdi)
	movl	%r8d, %eax
	shrl	$31, %eax
	leal	1(%rax,%rcx,2), %ecx
	shrl	%ecx
	movl	$1, %r9d
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	leal	-1(%rax), %edx
	andl	%r10d, %edx
	orl	%eax, %edx
	movl	%edx, 20(%rdi)
	testl	%r8d, %r8d
	jle	.LBB13_12
# BB#5:                                 # %.lr.ph.i9
	movq	(%rsi), %rax
	leal	-1(%r8), %ecx
	movl	$1, %r10d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r10d
	movb	8(%rax), %cl
	jmp	.LBB13_6
	.p2align	4, 0x90
.LBB13_11:                              # %._crit_edge
                                        #   in Loop: Header=BB13_6 Depth=1
	shrl	%r10d
	movl	20(%rdi), %edx
	incl	%r9d
.LBB13_6:                               # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%r10d, %edx
	je	.LBB13_8
# BB#7:                                 #   in Loop: Header=BB13_6 Depth=1
	orb	$1, %cl
.LBB13_8:                               #   in Loop: Header=BB13_6 Depth=1
	movb	%cl, 8(%rax)
	decl	4(%rax)
	jne	.LBB13_10
# BB#9:                                 #   in Loop: Header=BB13_6 Depth=1
	movl	$8, 4(%rax)
	movq	32(%rax), %r8
	movslq	(%rax), %r11
	leal	1(%r11), %edx
	movl	%edx, (%rax)
	movb	%cl, (%r8,%r11)
	movb	$0, 8(%rax)
	movl	12(%rdi), %r8d
	xorl	%ecx, %ecx
.LBB13_10:                              #   in Loop: Header=BB13_6 Depth=1
	cmpl	%r8d, %r9d
	jl	.LBB13_11
.LBB13_12:                              # %writeUVLC2buffer.exit
	cmpl	$0, (%rdi)
	je	.LBB13_14
# BB#13:
	movq	(%rsi), %rax
	movl	$1, 40(%rax)
.LBB13_14:
	retq
.Lfunc_end13:
	.size	writeSE_SVLC, .Lfunc_end13-writeSE_SVLC
	.cfi_endproc

	.globl	writeCBP_VLC
	.p2align	4, 0x90
	.type	writeCBP_VLC,@function
writeCBP_VLC:                           # @writeCBP_VLC
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movq	14224(%rax), %rcx
	movslq	12(%rax), %rdx
	imulq	$536, %rdx, %rdx        # imm = 0x218
	movl	72(%rcx,%rdx), %ecx
	cmpl	$13, %ecx
	ja	.LBB14_6
# BB#1:
	movl	$12800, %edx            # imm = 0x3200
	btl	%ecx, %edx
	jae	.LBB14_6
# BB#2:
	leaq	12(%rdi), %r9
	xorl	%ecx, %ecx
	cmpl	$0, 15536(%rax)
	leaq	16(%rdi), %r8
	setne	%cl
	movslq	4(%rdi), %rax
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movzbl	NCBP(%rcx,%rax,2), %eax
	leal	1(%rax), %r10d
	xorl	%ecx, %ecx
	testl	%eax, %eax
	je	.LBB14_10
# BB#3:                                 # %.lr.ph.i.i.preheader
	movl	%r10d, %eax
	.p2align	4, 0x90
.LBB14_4:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	$15, %ecx
	jg	.LBB14_10
# BB#5:                                 # %.lr.ph.i.i
                                        #   in Loop: Header=BB14_4 Depth=1
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%eax, %edx
	sarl	%edx
	leal	1(%rdx), %eax
	cmpl	$2, %eax
	movl	%edx, %eax
	ja	.LBB14_4
	jmp	.LBB14_10
.LBB14_6:
	leaq	12(%rdi), %r9
	xorl	%ecx, %ecx
	cmpl	$0, 15536(%rax)
	leaq	16(%rdi), %r8
	setne	%cl
	movslq	4(%rdi), %rax
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movzbl	NCBP+1(%rcx,%rax,2), %eax
	leal	1(%rax), %r10d
	xorl	%ecx, %ecx
	testl	%eax, %eax
	je	.LBB14_10
# BB#7:                                 # %.lr.ph.i.i21.preheader
	movl	%r10d, %eax
	.p2align	4, 0x90
.LBB14_8:                               # %.lr.ph.i.i21
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	$15, %ecx
	jg	.LBB14_10
# BB#9:                                 # %.lr.ph.i.i21
                                        #   in Loop: Header=BB14_8 Depth=1
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%eax, %edx
	sarl	%edx
	leal	1(%rdx), %eax
	cmpl	$2, %eax
	movl	%edx, %eax
	ja	.LBB14_8
.LBB14_10:                              # %cbp_linfo_inter.exit
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %rbp, -16
	leal	1(%rcx,%rcx), %r11d
	movl	%r11d, (%r9)
	movl	$1, %r14d
	movl	$1, %eax
	shll	%cl, %eax
	subl	%eax, %r10d
	movl	%r10d, (%r8)
	movl	%r11d, %eax
	shrl	$31, %eax
	leal	1(%rax,%rcx,2), %ecx
	shrl	%ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	leal	-1(%rax), %ebx
	andl	%r10d, %ebx
	orl	%eax, %ebx
	movl	%ebx, 20(%rdi)
	testl	%r11d, %r11d
	jle	.LBB14_18
# BB#11:                                # %.lr.ph.i
	movq	(%rsi), %rdx
	leal	-1(%r11), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movb	8(%rdx), %cl
	jmp	.LBB14_12
	.p2align	4, 0x90
.LBB14_17:                              # %._crit_edge
                                        #   in Loop: Header=BB14_12 Depth=1
	shrl	%eax
	movl	20(%rdi), %ebx
	incl	%r14d
.LBB14_12:                              # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%eax, %ebx
	je	.LBB14_14
# BB#13:                                #   in Loop: Header=BB14_12 Depth=1
	orb	$1, %cl
.LBB14_14:                              #   in Loop: Header=BB14_12 Depth=1
	movb	%cl, 8(%rdx)
	decl	4(%rdx)
	jne	.LBB14_16
# BB#15:                                #   in Loop: Header=BB14_12 Depth=1
	movl	$8, 4(%rdx)
	movq	32(%rdx), %r8
	movslq	(%rdx), %rbp
	leal	1(%rbp), %ebx
	movl	%ebx, (%rdx)
	movb	%cl, (%r8,%rbp)
	movb	$0, 8(%rdx)
	movl	(%r9), %r11d
	xorl	%ecx, %ecx
.LBB14_16:                              #   in Loop: Header=BB14_12 Depth=1
	cmpl	%r11d, %r14d
	jl	.LBB14_17
.LBB14_18:                              # %writeUVLC2buffer.exit
	cmpl	$0, (%rdi)
	popq	%rbx
	popq	%r14
	popq	%rbp
	je	.LBB14_20
# BB#19:
	movq	(%rsi), %rax
	movl	$1, 40(%rax)
.LBB14_20:
	retq
.Lfunc_end14:
	.size	writeCBP_VLC, .Lfunc_end14-writeCBP_VLC
	.cfi_endproc

	.globl	writeIntraPredMode_CAVLC
	.p2align	4, 0x90
	.type	writeIntraPredMode_CAVLC,@function
writeIntraPredMode_CAVLC:               # @writeIntraPredMode_CAVLC
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	movl	4(%rdi), %r10d
	xorl	%eax, %eax
	cmpl	$-1, %r10d
	setne	%al
	leal	(%rax,%rax,2), %ecx
	leal	1(%rax,%rax,2), %r9d
	movl	$1, %r8d
	cmovel	%r8d, %r10d
	movl	%r9d, 12(%rdi)
	movl	%r10d, 16(%rdi)
	movl	%r10d, 20(%rdi)
	movq	(%rsi), %rdx
	movl	$1, %r11d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r11d
	movb	8(%rdx), %cl
	jmp	.LBB15_1
	.p2align	4, 0x90
.LBB15_6:                               # %._crit_edge
                                        #   in Loop: Header=BB15_1 Depth=1
	shrl	%r11d
	movl	20(%rdi), %r10d
	incl	%r8d
.LBB15_1:                               # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%r11d, %r10d
	je	.LBB15_3
# BB#2:                                 #   in Loop: Header=BB15_1 Depth=1
	orb	$1, %cl
.LBB15_3:                               #   in Loop: Header=BB15_1 Depth=1
	movb	%cl, 8(%rdx)
	decl	4(%rdx)
	jne	.LBB15_5
# BB#4:                                 #   in Loop: Header=BB15_1 Depth=1
	movl	$8, 4(%rdx)
	movq	32(%rdx), %r9
	movslq	(%rdx), %r10
	leal	1(%r10), %eax
	movl	%eax, (%rdx)
	movb	%cl, (%r9,%r10)
	movb	$0, 8(%rdx)
	movl	12(%rdi), %r9d
	xorl	%ecx, %ecx
.LBB15_5:                               #   in Loop: Header=BB15_1 Depth=1
	cmpl	%r9d, %r8d
	jl	.LBB15_6
# BB#7:                                 # %writeUVLC2buffer.exit
	cmpl	$0, (%rdi)
	je	.LBB15_9
# BB#8:
	movq	(%rsi), %rax
	movl	$1, 40(%rax)
.LBB15_9:
	retq
.Lfunc_end15:
	.size	writeIntraPredMode_CAVLC, .Lfunc_end15-writeIntraPredMode_CAVLC
	.cfi_endproc

	.globl	writeSyntaxElement2Buf_UVLC
	.p2align	4, 0x90
	.type	writeSyntaxElement2Buf_UVLC,@function
writeSyntaxElement2Buf_UVLC:            # @writeSyntaxElement2Buf_UVLC
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -32
.Lcfi12:
	.cfi_offset %r14, -24
.Lcfi13:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	4(%r14), %edi
	movl	8(%r14), %esi
	leaq	12(%r14), %r15
	leaq	16(%r14), %rcx
	movq	%r15, %rdx
	callq	*32(%r14)
	movl	12(%r14), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	shrl	%ecx
	movl	$1, %edx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	leal	-1(%rsi), %edi
	andl	16(%r14), %edi
	orl	%esi, %edi
	movl	%edi, 20(%r14)
	testl	%eax, %eax
	jle	.LBB16_8
# BB#1:                                 # %.lr.ph.i
	leal	-1(%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movb	8(%rbx), %cl
	jmp	.LBB16_2
	.p2align	4, 0x90
.LBB16_7:                               # %._crit_edge
                                        #   in Loop: Header=BB16_2 Depth=1
	shrl	%esi
	movl	20(%r14), %edi
	incl	%edx
.LBB16_2:                               # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%esi, %edi
	je	.LBB16_4
# BB#3:                                 #   in Loop: Header=BB16_2 Depth=1
	orb	$1, %cl
.LBB16_4:                               #   in Loop: Header=BB16_2 Depth=1
	movb	%cl, 8(%rbx)
	decl	4(%rbx)
	jne	.LBB16_6
# BB#5:                                 #   in Loop: Header=BB16_2 Depth=1
	movl	$8, 4(%rbx)
	movq	32(%rbx), %r8
	movslq	(%rbx), %rdi
	leal	1(%rdi), %eax
	movl	%eax, (%rbx)
	movb	%cl, (%r8,%rdi)
	movb	$0, 8(%rbx)
	movl	(%r15), %eax
	xorl	%ecx, %ecx
.LBB16_6:                               #   in Loop: Header=BB16_2 Depth=1
	cmpl	%eax, %edx
	jl	.LBB16_7
.LBB16_8:                               # %writeUVLC2buffer.exit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	writeSyntaxElement2Buf_UVLC, .Lfunc_end16-writeSyntaxElement2Buf_UVLC
	.cfi_endproc

	.globl	writeSyntaxElement2Buf_Fixed
	.p2align	4, 0x90
	.type	writeSyntaxElement2Buf_Fixed,@function
writeSyntaxElement2Buf_Fixed:           # @writeSyntaxElement2Buf_Fixed
	.cfi_startproc
# BB#0:
	movl	12(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB17_7
# BB#1:                                 # %.lr.ph.i
	leal	-1(%rax), %ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	movb	8(%rsi), %r8b
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB17_2:                               # =>This Inner Loop Header: Depth=1
	addb	%r8b, %r8b
	movb	%r8b, 8(%rsi)
	testl	20(%rdi), %edx
	je	.LBB17_4
# BB#3:                                 #   in Loop: Header=BB17_2 Depth=1
	orb	$1, %r8b
	movb	%r8b, 8(%rsi)
.LBB17_4:                               #   in Loop: Header=BB17_2 Depth=1
	decl	4(%rsi)
	jne	.LBB17_6
# BB#5:                                 #   in Loop: Header=BB17_2 Depth=1
	movl	$8, 4(%rsi)
	movq	32(%rsi), %r9
	movslq	(%rsi), %r10
	leal	1(%r10), %eax
	movl	%eax, (%rsi)
	movb	%r8b, (%r9,%r10)
	movb	$0, 8(%rsi)
	movl	12(%rdi), %eax
	xorl	%r8d, %r8d
.LBB17_6:                               #   in Loop: Header=BB17_2 Depth=1
	shrl	%edx
	incl	%ecx
	cmpl	%eax, %ecx
	jl	.LBB17_2
.LBB17_7:                               # %writeUVLC2buffer.exit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end17:
	.size	writeSyntaxElement2Buf_Fixed, .Lfunc_end17-writeSyntaxElement2Buf_Fixed
	.cfi_endproc

	.globl	writeSE_Flag
	.p2align	4, 0x90
	.type	writeSE_Flag,@function
writeSE_Flag:                           # @writeSE_Flag
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	movl	$1, 12(%rdi)
	movl	4(%rdi), %ecx
	andl	$1, %ecx
	movl	%ecx, 20(%rdi)
	movq	(%rsi), %rax
	movb	8(%rax), %dl
	movl	$1, %r9d
	movl	$1, %r8d
	movl	$1, %r10d
	jmp	.LBB18_1
	.p2align	4, 0x90
.LBB18_6:                               # %._crit_edge
                                        #   in Loop: Header=BB18_1 Depth=1
	shrl	%r10d
	movl	20(%rdi), %ecx
	incl	%r9d
.LBB18_1:                               # =>This Inner Loop Header: Depth=1
	addb	%dl, %dl
	testl	%r10d, %ecx
	je	.LBB18_3
# BB#2:                                 #   in Loop: Header=BB18_1 Depth=1
	orb	$1, %dl
.LBB18_3:                               #   in Loop: Header=BB18_1 Depth=1
	movb	%dl, 8(%rax)
	decl	4(%rax)
	jne	.LBB18_5
# BB#4:                                 #   in Loop: Header=BB18_1 Depth=1
	movl	$8, 4(%rax)
	movq	32(%rax), %r8
	movslq	(%rax), %rcx
	leal	1(%rcx), %esi
	movl	%esi, (%rax)
	movb	%dl, (%r8,%rcx)
	movb	$0, 8(%rax)
	movl	12(%rdi), %r8d
	xorl	%edx, %edx
.LBB18_5:                               #   in Loop: Header=BB18_1 Depth=1
	cmpl	%r8d, %r9d
	jl	.LBB18_6
# BB#7:                                 # %writeUVLC2buffer.exit
	retq
.Lfunc_end18:
	.size	writeSE_Flag, .Lfunc_end18-writeSE_Flag
	.cfi_endproc

	.globl	writeSE_invFlag
	.p2align	4, 0x90
	.type	writeSE_invFlag,@function
writeSE_invFlag:                        # @writeSE_invFlag
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	movl	$1, 12(%rdi)
	movl	4(%rdi), %ecx
	notl	%ecx
	andl	$1, %ecx
	movl	%ecx, 20(%rdi)
	movq	(%rsi), %rax
	movb	8(%rax), %dl
	movl	$1, %r9d
	movl	$1, %r8d
	movl	$1, %r10d
	jmp	.LBB19_1
	.p2align	4, 0x90
.LBB19_6:                               # %._crit_edge
                                        #   in Loop: Header=BB19_1 Depth=1
	shrl	%r10d
	movl	20(%rdi), %ecx
	incl	%r9d
.LBB19_1:                               # =>This Inner Loop Header: Depth=1
	addb	%dl, %dl
	testl	%r10d, %ecx
	je	.LBB19_3
# BB#2:                                 #   in Loop: Header=BB19_1 Depth=1
	orb	$1, %dl
.LBB19_3:                               #   in Loop: Header=BB19_1 Depth=1
	movb	%dl, 8(%rax)
	decl	4(%rax)
	jne	.LBB19_5
# BB#4:                                 #   in Loop: Header=BB19_1 Depth=1
	movl	$8, 4(%rax)
	movq	32(%rax), %r8
	movslq	(%rax), %rcx
	leal	1(%rcx), %esi
	movl	%esi, (%rax)
	movb	%dl, (%r8,%rcx)
	movb	$0, 8(%rax)
	movl	12(%rdi), %r8d
	xorl	%edx, %edx
.LBB19_5:                               #   in Loop: Header=BB19_1 Depth=1
	cmpl	%r8d, %r9d
	jl	.LBB19_6
# BB#7:                                 # %writeUVLC2buffer.exit
	retq
.Lfunc_end19:
	.size	writeSE_invFlag, .Lfunc_end19-writeSE_invFlag
	.cfi_endproc

	.globl	writeSE_Dummy
	.p2align	4, 0x90
	.type	writeSE_Dummy,@function
writeSE_Dummy:                          # @writeSE_Dummy
	.cfi_startproc
# BB#0:
	movl	$0, 12(%rdi)
	retq
.Lfunc_end20:
	.size	writeSE_Dummy, .Lfunc_end20-writeSE_Dummy
	.cfi_endproc

	.globl	writeSE_Fix
	.p2align	4, 0x90
	.type	writeSE_Fix,@function
writeSE_Fix:                            # @writeSE_Fix
	.cfi_startproc
# BB#0:
	movl	12(%rdi), %r8d
	testl	%r8d, %r8d
	jle	.LBB21_7
# BB#1:                                 # %.lr.ph.i
	movq	(%rsi), %r10
	leal	-1(%r8), %ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	movb	8(%r10), %sil
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB21_2:                               # =>This Inner Loop Header: Depth=1
	addb	%sil, %sil
	movb	%sil, 8(%r10)
	testl	20(%rdi), %edx
	je	.LBB21_4
# BB#3:                                 #   in Loop: Header=BB21_2 Depth=1
	orb	$1, %sil
	movb	%sil, 8(%r10)
.LBB21_4:                               #   in Loop: Header=BB21_2 Depth=1
	decl	4(%r10)
	jne	.LBB21_6
# BB#5:                                 #   in Loop: Header=BB21_2 Depth=1
	movl	$8, 4(%r10)
	movq	32(%r10), %r8
	movslq	(%r10), %r9
	leal	1(%r9), %eax
	movl	%eax, (%r10)
	movb	%sil, (%r8,%r9)
	movb	$0, 8(%r10)
	movl	12(%rdi), %r8d
	xorl	%esi, %esi
.LBB21_6:                               #   in Loop: Header=BB21_2 Depth=1
	shrl	%edx
	incl	%ecx
	cmpl	%r8d, %ecx
	jl	.LBB21_2
.LBB21_7:                               # %writeUVLC2buffer.exit
	retq
.Lfunc_end21:
	.size	writeSE_Fix, .Lfunc_end21-writeSE_Fix
	.cfi_endproc

	.globl	symbol2vlc
	.p2align	4, 0x90
	.type	symbol2vlc,@function
symbol2vlc:                             # @symbol2vlc
	.cfi_startproc
# BB#0:
	movl	12(%rdi), %eax
	movl	$0, 20(%rdi)
	testl	%eax, %eax
	jle	.LBB22_4
# BB#1:                                 # %.lr.ph
	movl	16(%rdi), %r8d
	incl	%eax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB22_2:                               # =>This Inner Loop Header: Depth=1
	leal	-2(%rax), %ecx
	movl	%r8d, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	andl	$1, %edx
	leal	(%rdx,%rsi,2), %esi
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB22_2
# BB#3:                                 # %._crit_edge
	movl	%esi, 20(%rdi)
.LBB22_4:
	xorl	%eax, %eax
	retq
.Lfunc_end22:
	.size	symbol2vlc, .Lfunc_end22-symbol2vlc
	.cfi_endproc

	.globl	writeSyntaxElement_VLC
	.p2align	4, 0x90
	.type	writeSyntaxElement_VLC,@function
writeSyntaxElement_VLC:                 # @writeSyntaxElement_VLC
	.cfi_startproc
# BB#0:
	movl	4(%rdi), %r8d
	movl	%r8d, 16(%rdi)
	movl	8(%rdi), %eax
	movl	%eax, 12(%rdi)
	movl	$0, 20(%rdi)
	testl	%eax, %eax
	jle	.LBB23_11
# BB#1:                                 # %.lr.ph.i.preheader
	leal	1(%rax), %r9d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB23_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%r9), %ecx
	movl	%r8d, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	andl	$1, %edx
	leal	(%rdx,%r11,2), %r11d
	decl	%r9d
	cmpl	$1, %r9d
	jg	.LBB23_2
# BB#3:                                 # %symbol2vlc.exit
	movl	%r11d, 20(%rdi)
	testl	%eax, %eax
	jle	.LBB23_11
# BB#4:                                 # %.lr.ph.i10
	movq	(%rsi), %r10
	leal	-1(%rax), %ecx
	movl	$1, %r8d
	movl	$1, %r9d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r9d
	movb	8(%r10), %cl
	jmp	.LBB23_5
	.p2align	4, 0x90
.LBB23_10:                              # %._crit_edge
                                        #   in Loop: Header=BB23_5 Depth=1
	shrl	%r9d
	movl	20(%rdi), %r11d
	incl	%r8d
.LBB23_5:                               # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%r9d, %r11d
	je	.LBB23_7
# BB#6:                                 #   in Loop: Header=BB23_5 Depth=1
	orb	$1, %cl
.LBB23_7:                               #   in Loop: Header=BB23_5 Depth=1
	movb	%cl, 8(%r10)
	decl	4(%r10)
	jne	.LBB23_9
# BB#8:                                 #   in Loop: Header=BB23_5 Depth=1
	movl	$8, 4(%r10)
	movq	32(%r10), %r11
	movslq	(%r10), %rdx
	leal	1(%rdx), %eax
	movl	%eax, (%r10)
	movb	%cl, (%r11,%rdx)
	movb	$0, 8(%r10)
	movl	12(%rdi), %eax
	xorl	%ecx, %ecx
.LBB23_9:                               #   in Loop: Header=BB23_5 Depth=1
	cmpl	%eax, %r8d
	jl	.LBB23_10
.LBB23_11:                              # %writeUVLC2buffer.exit
	cmpl	$0, (%rdi)
	je	.LBB23_13
# BB#12:
	movq	(%rsi), %rcx
	movl	$1, 40(%rcx)
.LBB23_13:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end23:
	.size	writeSyntaxElement_VLC, .Lfunc_end23-writeSyntaxElement_VLC
	.cfi_endproc

	.globl	writeSyntaxElement_NumCoeffTrailingOnes
	.p2align	4, 0x90
	.type	writeSyntaxElement_NumCoeffTrailingOnes,@function
writeSyntaxElement_NumCoeffTrailingOnes: # @writeSyntaxElement_NumCoeffTrailingOnes
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movslq	12(%rdi), %r8
	cmpq	$3, %r8
	jne	.LBB24_5
# BB#1:
	movl	$6, 12(%rdi)
	movl	4(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB24_2
# BB#3:
	leal	-4(,%rax,4), %r9d
	orl	8(%rdi), %r9d
	jmp	.LBB24_4
.LBB24_5:
	movslq	8(%rdi), %rax
	movslq	4(%rdi), %rdx
	imulq	$272, %r8, %rcx         # imm = 0x110
	imulq	$68, %rax, %rbx
	addq	%rcx, %rbx
	movl	writeSyntaxElement_NumCoeffTrailingOnes.lentab(%rbx,%rdx,4), %eax
	movl	%eax, 12(%rdi)
	movl	writeSyntaxElement_NumCoeffTrailingOnes.codtab(%rbx,%rdx,4), %r9d
	movl	%r9d, 16(%rdi)
	testl	%eax, %eax
	je	.LBB24_20
# BB#6:
	movl	$0, 20(%rdi)
	testl	%eax, %eax
	jle	.LBB24_17
# BB#7:
	leaq	20(%rdi), %r8
	jmp	.LBB24_8
.LBB24_2:
	movl	$3, %r9d
.LBB24_4:                               # %.thread32
	movl	%r9d, 16(%rdi)
	leaq	20(%rdi), %r8
	movl	$0, 20(%rdi)
	movl	$6, %eax
.LBB24_8:                               # %.lr.ph.i
	leal	1(%rax), %edx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB24_9:                               # =>This Inner Loop Header: Depth=1
	leal	-2(%rdx), %ecx
	movl	%r9d, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebx
	andl	$1, %ebx
	leal	(%rbx,%r14,2), %r14d
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB24_9
# BB#10:                                # %symbol2vlc.exit
	movl	%r14d, (%r8)
	movq	(%rsi), %r11
	leal	-1(%rax), %ecx
	movl	$1, %r9d
	movl	$1, %r10d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r10d
	movb	8(%r11), %cl
	jmp	.LBB24_11
	.p2align	4, 0x90
.LBB24_16:                              # %._crit_edge
                                        #   in Loop: Header=BB24_11 Depth=1
	shrl	%r10d
	movl	(%r8), %r14d
	incl	%r9d
.LBB24_11:                              # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%r10d, %r14d
	je	.LBB24_13
# BB#12:                                #   in Loop: Header=BB24_11 Depth=1
	orb	$1, %cl
.LBB24_13:                              #   in Loop: Header=BB24_11 Depth=1
	movb	%cl, 8(%r11)
	decl	4(%r11)
	jne	.LBB24_15
# BB#14:                                #   in Loop: Header=BB24_11 Depth=1
	movl	$8, 4(%r11)
	movq	32(%r11), %rax
	movslq	(%r11), %rdx
	leal	1(%rdx), %ebx
	movl	%ebx, (%r11)
	movb	%cl, (%rax,%rdx)
	movb	$0, 8(%r11)
	movl	12(%rdi), %eax
	xorl	%ecx, %ecx
.LBB24_15:                              #   in Loop: Header=BB24_11 Depth=1
	cmpl	%eax, %r9d
	jl	.LBB24_16
.LBB24_17:                              # %writeUVLC2buffer.exit
	cmpl	$0, (%rdi)
	je	.LBB24_19
# BB#18:
	movq	(%rsi), %rcx
	movl	$1, 40(%rcx)
.LBB24_19:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB24_20:
	movl	8(%rdi), %ecx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r8d, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end24:
	.size	writeSyntaxElement_NumCoeffTrailingOnes, .Lfunc_end24-writeSyntaxElement_NumCoeffTrailingOnes
	.cfi_endproc

	.globl	writeSyntaxElement_NumCoeffTrailingOnesChromaDC
	.p2align	4, 0x90
	.type	writeSyntaxElement_NumCoeffTrailingOnesChromaDC,@function
writeSyntaxElement_NumCoeffTrailingOnesChromaDC: # @writeSyntaxElement_NumCoeffTrailingOnesChromaDC
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movslq	15536(%rax), %rax
	imulq	$272, %rax, %rax        # imm = 0x110
	movslq	8(%rdi), %rdx
	movslq	4(%rdi), %r9
	imulq	$68, %rdx, %rcx
	addq	%rax, %rcx
	movl	writeSyntaxElement_NumCoeffTrailingOnesChromaDC.lentab-272(%rcx,%r9,4), %eax
	movl	%eax, 12(%rdi)
	movl	writeSyntaxElement_NumCoeffTrailingOnesChromaDC.codtab-272(%rcx,%r9,4), %r8d
	movl	%r8d, 16(%rdi)
	testl	%eax, %eax
	je	.LBB25_15
# BB#1:
	movl	$0, 20(%rdi)
	jle	.LBB25_12
# BB#2:                                 # %.lr.ph.i.preheader
	leal	1(%rax), %r9d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB25_3:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%r9), %ecx
	movl	%r8d, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	andl	$1, %edx
	leal	(%rdx,%r11,2), %r11d
	decl	%r9d
	cmpl	$1, %r9d
	jg	.LBB25_3
# BB#4:                                 # %symbol2vlc.exit
	movl	%r11d, 20(%rdi)
	testl	%eax, %eax
	jle	.LBB25_12
# BB#5:                                 # %.lr.ph.i16
	movq	(%rsi), %r10
	leal	-1(%rax), %ecx
	movl	$1, %r8d
	movl	$1, %r9d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r9d
	movb	8(%r10), %cl
	jmp	.LBB25_6
	.p2align	4, 0x90
.LBB25_11:                              # %._crit_edge
                                        #   in Loop: Header=BB25_6 Depth=1
	shrl	%r9d
	movl	20(%rdi), %r11d
	incl	%r8d
.LBB25_6:                               # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%r9d, %r11d
	je	.LBB25_8
# BB#7:                                 #   in Loop: Header=BB25_6 Depth=1
	orb	$1, %cl
.LBB25_8:                               #   in Loop: Header=BB25_6 Depth=1
	movb	%cl, 8(%r10)
	decl	4(%r10)
	jne	.LBB25_10
# BB#9:                                 #   in Loop: Header=BB25_6 Depth=1
	movl	$8, 4(%r10)
	movq	32(%r10), %r11
	movslq	(%r10), %rdx
	leal	1(%rdx), %eax
	movl	%eax, (%r10)
	movb	%cl, (%r11,%rdx)
	movb	$0, 8(%r10)
	movl	12(%rdi), %eax
	xorl	%ecx, %ecx
.LBB25_10:                              #   in Loop: Header=BB25_6 Depth=1
	cmpl	%eax, %r8d
	jl	.LBB25_11
.LBB25_12:                              # %writeUVLC2buffer.exit
	cmpl	$0, (%rdi)
	je	.LBB25_14
# BB#13:
	movq	(%rsi), %rcx
	movl	$1, 40(%rcx)
.LBB25_14:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB25_15:
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 16
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%r9d, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end25:
	.size	writeSyntaxElement_NumCoeffTrailingOnesChromaDC, .Lfunc_end25-writeSyntaxElement_NumCoeffTrailingOnesChromaDC
	.cfi_endproc

	.globl	writeSyntaxElement_TotalZeros
	.p2align	4, 0x90
	.type	writeSyntaxElement_TotalZeros,@function
writeSyntaxElement_TotalZeros:          # @writeSyntaxElement_TotalZeros
	.cfi_startproc
# BB#0:
	movslq	12(%rdi), %rdx
	movslq	4(%rdi), %rcx
	shlq	$6, %rdx
	movl	writeSyntaxElement_TotalZeros.lentab(%rdx,%rcx,4), %eax
	movl	%eax, 12(%rdi)
	movl	writeSyntaxElement_TotalZeros.codtab(%rdx,%rcx,4), %r8d
	movl	%r8d, 16(%rdi)
	testl	%eax, %eax
	je	.LBB26_15
# BB#1:
	movl	$0, 20(%rdi)
	jle	.LBB26_12
# BB#2:                                 # %.lr.ph.i.preheader
	leal	1(%rax), %r9d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB26_3:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%r9), %ecx
	movl	%r8d, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	andl	$1, %edx
	leal	(%rdx,%r11,2), %r11d
	decl	%r9d
	cmpl	$1, %r9d
	jg	.LBB26_3
# BB#4:                                 # %symbol2vlc.exit
	movl	%r11d, 20(%rdi)
	testl	%eax, %eax
	jle	.LBB26_12
# BB#5:                                 # %.lr.ph.i14
	movq	(%rsi), %r10
	leal	-1(%rax), %ecx
	movl	$1, %r8d
	movl	$1, %r9d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r9d
	movb	8(%r10), %cl
	jmp	.LBB26_6
	.p2align	4, 0x90
.LBB26_11:                              # %._crit_edge
                                        #   in Loop: Header=BB26_6 Depth=1
	shrl	%r9d
	movl	20(%rdi), %r11d
	incl	%r8d
.LBB26_6:                               # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%r9d, %r11d
	je	.LBB26_8
# BB#7:                                 #   in Loop: Header=BB26_6 Depth=1
	orb	$1, %cl
.LBB26_8:                               #   in Loop: Header=BB26_6 Depth=1
	movb	%cl, 8(%r10)
	decl	4(%r10)
	jne	.LBB26_10
# BB#9:                                 #   in Loop: Header=BB26_6 Depth=1
	movl	$8, 4(%r10)
	movq	32(%r10), %r11
	movslq	(%r10), %rdx
	leal	1(%rdx), %eax
	movl	%eax, (%r10)
	movb	%cl, (%r11,%rdx)
	movb	$0, 8(%r10)
	movl	12(%rdi), %eax
	xorl	%ecx, %ecx
.LBB26_10:                              #   in Loop: Header=BB26_6 Depth=1
	cmpl	%eax, %r8d
	jl	.LBB26_11
.LBB26_12:                              # %writeUVLC2buffer.exit
	cmpl	$0, (%rdi)
	je	.LBB26_14
# BB#13:
	movq	(%rsi), %rcx
	movl	$1, 40(%rcx)
.LBB26_14:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB26_15:
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 16
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end26:
	.size	writeSyntaxElement_TotalZeros, .Lfunc_end26-writeSyntaxElement_TotalZeros
	.cfi_endproc

	.globl	writeSyntaxElement_TotalZerosChromaDC
	.p2align	4, 0x90
	.type	writeSyntaxElement_TotalZerosChromaDC,@function
writeSyntaxElement_TotalZerosChromaDC:  # @writeSyntaxElement_TotalZerosChromaDC
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movslq	15536(%rax), %rax
	imulq	$960, %rax, %rax        # imm = 0x3C0
	movslq	12(%rdi), %rdx
	movslq	4(%rdi), %rcx
	shlq	$6, %rdx
	addq	%rax, %rdx
	movl	writeSyntaxElement_TotalZerosChromaDC.lentab-960(%rdx,%rcx,4), %eax
	movl	%eax, 12(%rdi)
	movl	writeSyntaxElement_TotalZerosChromaDC.codtab-960(%rdx,%rcx,4), %r8d
	movl	%r8d, 16(%rdi)
	testl	%eax, %eax
	je	.LBB27_15
# BB#1:
	movl	$0, 20(%rdi)
	jle	.LBB27_12
# BB#2:                                 # %.lr.ph.i.preheader
	leal	1(%rax), %r9d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB27_3:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%r9), %ecx
	movl	%r8d, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	andl	$1, %edx
	leal	(%rdx,%r11,2), %r11d
	decl	%r9d
	cmpl	$1, %r9d
	jg	.LBB27_3
# BB#4:                                 # %symbol2vlc.exit
	movl	%r11d, 20(%rdi)
	testl	%eax, %eax
	jle	.LBB27_12
# BB#5:                                 # %.lr.ph.i18
	movq	(%rsi), %r10
	leal	-1(%rax), %ecx
	movl	$1, %r8d
	movl	$1, %r9d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r9d
	movb	8(%r10), %cl
	jmp	.LBB27_6
	.p2align	4, 0x90
.LBB27_11:                              # %._crit_edge
                                        #   in Loop: Header=BB27_6 Depth=1
	shrl	%r9d
	movl	20(%rdi), %r11d
	incl	%r8d
.LBB27_6:                               # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%r9d, %r11d
	je	.LBB27_8
# BB#7:                                 #   in Loop: Header=BB27_6 Depth=1
	orb	$1, %cl
.LBB27_8:                               #   in Loop: Header=BB27_6 Depth=1
	movb	%cl, 8(%r10)
	decl	4(%r10)
	jne	.LBB27_10
# BB#9:                                 #   in Loop: Header=BB27_6 Depth=1
	movl	$8, 4(%r10)
	movq	32(%r10), %r11
	movslq	(%r10), %rdx
	leal	1(%rdx), %eax
	movl	%eax, (%r10)
	movb	%cl, (%r11,%rdx)
	movb	$0, 8(%r10)
	movl	12(%rdi), %eax
	xorl	%ecx, %ecx
.LBB27_10:                              #   in Loop: Header=BB27_6 Depth=1
	cmpl	%eax, %r8d
	jl	.LBB27_11
.LBB27_12:                              # %writeUVLC2buffer.exit
	cmpl	$0, (%rdi)
	je	.LBB27_14
# BB#13:
	movq	(%rsi), %rcx
	movl	$1, 40(%rcx)
.LBB27_14:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB27_15:
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 16
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end27:
	.size	writeSyntaxElement_TotalZerosChromaDC, .Lfunc_end27-writeSyntaxElement_TotalZerosChromaDC
	.cfi_endproc

	.globl	writeSyntaxElement_Run
	.p2align	4, 0x90
	.type	writeSyntaxElement_Run,@function
writeSyntaxElement_Run:                 # @writeSyntaxElement_Run
	.cfi_startproc
# BB#0:
	movslq	12(%rdi), %rdx
	movslq	4(%rdi), %rcx
	shlq	$6, %rdx
	movl	writeSyntaxElement_Run.lentab(%rdx,%rcx,4), %eax
	movl	%eax, 12(%rdi)
	movl	writeSyntaxElement_Run.codtab(%rdx,%rcx,4), %r8d
	movl	%r8d, 16(%rdi)
	testl	%eax, %eax
	je	.LBB28_15
# BB#1:
	movl	$0, 20(%rdi)
	jle	.LBB28_12
# BB#2:                                 # %.lr.ph.i.preheader
	leal	1(%rax), %r9d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB28_3:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%r9), %ecx
	movl	%r8d, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	andl	$1, %edx
	leal	(%rdx,%r11,2), %r11d
	decl	%r9d
	cmpl	$1, %r9d
	jg	.LBB28_3
# BB#4:                                 # %symbol2vlc.exit
	movl	%r11d, 20(%rdi)
	testl	%eax, %eax
	jle	.LBB28_12
# BB#5:                                 # %.lr.ph.i14
	movq	(%rsi), %r10
	leal	-1(%rax), %ecx
	movl	$1, %r8d
	movl	$1, %r9d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r9d
	movb	8(%r10), %cl
	jmp	.LBB28_6
	.p2align	4, 0x90
.LBB28_11:                              # %._crit_edge
                                        #   in Loop: Header=BB28_6 Depth=1
	shrl	%r9d
	movl	20(%rdi), %r11d
	incl	%r8d
.LBB28_6:                               # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%r9d, %r11d
	je	.LBB28_8
# BB#7:                                 #   in Loop: Header=BB28_6 Depth=1
	orb	$1, %cl
.LBB28_8:                               #   in Loop: Header=BB28_6 Depth=1
	movb	%cl, 8(%r10)
	decl	4(%r10)
	jne	.LBB28_10
# BB#9:                                 #   in Loop: Header=BB28_6 Depth=1
	movl	$8, 4(%r10)
	movq	32(%r10), %r11
	movslq	(%r10), %rdx
	leal	1(%rdx), %eax
	movl	%eax, (%r10)
	movb	%cl, (%r11,%rdx)
	movb	$0, 8(%r10)
	movl	12(%rdi), %eax
	xorl	%ecx, %ecx
.LBB28_10:                              #   in Loop: Header=BB28_6 Depth=1
	cmpl	%eax, %r8d
	jl	.LBB28_11
.LBB28_12:                              # %writeUVLC2buffer.exit
	cmpl	$0, (%rdi)
	je	.LBB28_14
# BB#13:
	movq	(%rsi), %rcx
	movl	$1, 40(%rcx)
.LBB28_14:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB28_15:
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 16
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end28:
	.size	writeSyntaxElement_Run, .Lfunc_end28-writeSyntaxElement_Run
	.cfi_endproc

	.globl	writeSyntaxElement_Level_VLC1
	.p2align	4, 0x90
	.type	writeSyntaxElement_Level_VLC1,@function
writeSyntaxElement_Level_VLC1:          # @writeSyntaxElement_Level_VLC1
	.cfi_startproc
# BB#0:
	movl	4(%rdi), %r8d
	movl	%r8d, %r10d
	negl	%r10d
	cmovll	%r8d, %r10d
	shrl	$31, %r8d
	cmpl	$7, %r10d
	jg	.LBB29_2
# BB#1:
	leal	-1(%r8,%r10,2), %r10d
	leaq	12(%rdi), %r9
	movl	%r10d, 12(%rdi)
	movl	$1, %r8d
	movl	%r8d, 16(%rdi)
	jmp	.LBB29_10
.LBB29_2:
	cmpl	$15, %r10d
	jg	.LBB29_5
# BB#3:
	leaq	12(%rdi), %r9
	movl	$19, 12(%rdi)
	leal	-16(%r8,%r10,2), %r8d
	orl	$16, %r8d
	movl	$19, %r10d
	movl	%r8d, 16(%rdi)
	jmp	.LBB29_10
.LBB29_5:
	leal	-16(%r10), %r9d
	movl	$15, %r11d
	cmpl	$2049, %r9d             # imm = 0x801
	jl	.LBB29_8
# BB#6:                                 # %.preheader.preheader
	movl	$15, %r11d
	.p2align	4, 0x90
.LBB29_7:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%r11), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	addl	$-4080, %eax            # imm = 0xF010
	incl	%r11d
	cmpl	%eax, %r10d
	jg	.LBB29_7
.LBB29_8:                               # %.loopexit
	leal	-3(%r11), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	leal	(%r8,%r9,2), %r8d
	orl	%eax, %r8d
	cmpl	$15, %r11d
	setg	%al
	cmpl	$100, %edx
	setl	%cl
	leal	-2(%r11,%r11), %r10d
	andb	%al, %cl
	movl	$65535, %eax            # imm = 0xFFFF
	movl	%r10d, %edx
	cmovnel	%eax, %edx
	movl	%edx, 12(%rdi)
	movl	%r8d, 16(%rdi)
	testb	%cl, %cl
	jne	.LBB29_24
# BB#9:
	leaq	12(%rdi), %r9
.LBB29_10:
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 16
.Lcfi24:
	.cfi_offset %rbx, -16
	movl	$0, 20(%rdi)
	testl	%r10d, %r10d
	jle	.LBB29_21
# BB#11:                                # %.lr.ph.i68.preheader
	leal	1(%r10), %eax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB29_12:                              # %.lr.ph.i68
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%rax), %ecx
	movl	%r8d, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	andl	$1, %edx
	leal	(%rdx,%rbx,2), %ebx
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB29_12
# BB#13:                                # %symbol2vlc.exit
	movl	%ebx, 20(%rdi)
	testl	%r10d, %r10d
	jle	.LBB29_21
# BB#14:                                # %.lr.ph.i
	movq	(%rsi), %rax
	leal	-1(%r10), %ecx
	movl	$1, %r8d
	movl	$1, %r11d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r11d
	movb	8(%rax), %cl
	jmp	.LBB29_15
	.p2align	4, 0x90
.LBB29_20:                              # %._crit_edge
                                        #   in Loop: Header=BB29_15 Depth=1
	shrl	%r11d
	movl	20(%rdi), %ebx
	incl	%r8d
.LBB29_15:                              # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%r11d, %ebx
	je	.LBB29_17
# BB#16:                                #   in Loop: Header=BB29_15 Depth=1
	orb	$1, %cl
.LBB29_17:                              #   in Loop: Header=BB29_15 Depth=1
	movb	%cl, 8(%rax)
	decl	4(%rax)
	jne	.LBB29_19
# BB#18:                                #   in Loop: Header=BB29_15 Depth=1
	movl	$8, 4(%rax)
	movq	32(%rax), %r10
	movslq	(%rax), %rbx
	leal	1(%rbx), %edx
	movl	%edx, (%rax)
	movb	%cl, (%r10,%rbx)
	movb	$0, 8(%rax)
	movl	(%r9), %r10d
	xorl	%ecx, %ecx
.LBB29_19:                              #   in Loop: Header=BB29_15 Depth=1
	cmpl	%r10d, %r8d
	jl	.LBB29_20
.LBB29_21:                              # %writeUVLC2buffer.exit
	cmpl	$0, (%rdi)
	popq	%rbx
	je	.LBB29_23
# BB#22:
	movq	(%rsi), %rax
	movl	$1, 40(%rax)
.LBB29_23:
	movl	%r10d, %eax
.LBB29_24:
	retq
.Lfunc_end29:
	.size	writeSyntaxElement_Level_VLC1, .Lfunc_end29-writeSyntaxElement_Level_VLC1
	.cfi_endproc

	.globl	writeSyntaxElement_Level_VLCN
	.p2align	4, 0x90
	.type	writeSyntaxElement_Level_VLCN,@function
writeSyntaxElement_Level_VLCN:          # @writeSyntaxElement_Level_VLCN
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 32
.Lcfi28:
	.cfi_offset %rbx, -32
.Lcfi29:
	.cfi_offset %r14, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movl	%ecx, %r8d
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	4(%rdi), %r9d
	movl	%r9d, %r10d
	negl	%r10d
	cmovll	%r9d, %r10d
	shrl	$31, %r9d
	leal	-1(%rsi), %ecx
	movl	$15, %ebx
	movl	$15, %eax
	shll	%cl, %eax
	cmpl	%eax, %r10d
	jle	.LBB30_1
# BB#13:
	notl	%eax
	addl	%eax, %r10d
	cmpl	$2049, %r10d            # imm = 0x801
	jl	.LBB30_16
# BB#14:                                # %.preheader.preheader
	movl	$15, %ebx
	.p2align	4, 0x90
.LBB30_15:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%rbx), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	addl	$-4096, %eax            # imm = 0xF000
	incl	%ebx
	cmpl	%eax, %r10d
	jg	.LBB30_15
.LBB30_16:                              # %.loopexit
	leal	-15(%rbx), %r11d
	leal	-2(%rbx,%rbx), %eax
	leal	-3(%rbx), %ecx
	movl	$1, %r14d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r14d
	movl	$-2048, %esi            # imm = 0xF800
	movl	%r11d, %ecx
	shll	%cl, %esi
	addl	%esi, %r10d
	leal	4096(%r10,%r10), %ecx
	orl	%r9d, %r14d
	orl	%ecx, %r14d
	cmpl	$99, %r8d
	jg	.LBB30_2
# BB#17:                                # %.loopexit
	cmpl	$16, %ebx
	jl	.LBB30_2
# BB#18:                                # %.critedge
	movl	$65535, 12(%rdi)        # imm = 0xFFFF
	movl	%r14d, 16(%rdi)
	movl	$65535, %eax            # imm = 0xFFFF
	jmp	.LBB30_21
.LBB30_1:
	decl	%r10d
	movl	$-1, %ebp
	shll	%cl, %ebp
	notl	%ebp
	andl	%r10d, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r10d
	leal	1(%rsi,%r10), %eax
	movl	$1, %ebx
	movl	%esi, %ecx
	shll	%cl, %ebx
	addl	%ebp, %ebp
	orl	%ebx, %r9d
	orl	%ebp, %r9d
	movl	%r9d, %r14d
.LBB30_2:
	movl	%eax, 12(%rdi)
	movl	%r14d, 16(%rdi)
	movl	$0, 20(%rdi)
	testl	%eax, %eax
	jle	.LBB30_19
# BB#3:                                 # %.lr.ph.i80.preheader
	leal	1(%rax), %esi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB30_4:                               # %.lr.ph.i80
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%rsi), %ecx
	movl	%r14d, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	andl	$1, %ebp
	leal	(%rbp,%rbx,2), %ebx
	decl	%esi
	cmpl	$1, %esi
	jg	.LBB30_4
# BB#5:                                 # %symbol2vlc.exit
	movl	%ebx, 20(%rdi)
	testl	%eax, %eax
	jle	.LBB30_19
# BB#6:                                 # %.lr.ph.i
	movq	(%rdx), %rsi
	leal	-1(%rax), %ecx
	movl	$1, %r8d
	movl	$1, %r9d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r9d
	movb	8(%rsi), %cl
	jmp	.LBB30_7
	.p2align	4, 0x90
.LBB30_12:                              # %._crit_edge
                                        #   in Loop: Header=BB30_7 Depth=1
	shrl	%r9d
	movl	20(%rdi), %ebx
	incl	%r8d
.LBB30_7:                               # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	testl	%r9d, %ebx
	je	.LBB30_9
# BB#8:                                 #   in Loop: Header=BB30_7 Depth=1
	orb	$1, %cl
.LBB30_9:                               #   in Loop: Header=BB30_7 Depth=1
	movb	%cl, 8(%rsi)
	decl	4(%rsi)
	jne	.LBB30_11
# BB#10:                                #   in Loop: Header=BB30_7 Depth=1
	movl	$8, 4(%rsi)
	movq	32(%rsi), %rax
	movslq	(%rsi), %rbp
	leal	1(%rbp), %ebx
	movl	%ebx, (%rsi)
	movb	%cl, (%rax,%rbp)
	movb	$0, 8(%rsi)
	movl	12(%rdi), %eax
	xorl	%ecx, %ecx
.LBB30_11:                              #   in Loop: Header=BB30_7 Depth=1
	cmpl	%eax, %r8d
	jl	.LBB30_12
.LBB30_19:                              # %writeUVLC2buffer.exit
	cmpl	$0, (%rdi)
	je	.LBB30_21
# BB#20:
	movq	(%rdx), %rcx
	movl	$1, 40(%rcx)
.LBB30_21:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end30:
	.size	writeSyntaxElement_Level_VLCN, .Lfunc_end30-writeSyntaxElement_Level_VLCN
	.cfi_endproc

	.globl	writeVlcByteAlign
	.p2align	4, 0x90
	.type	writeVlcByteAlign,@function
writeVlcByteAlign:                      # @writeVlcByteAlign
	.cfi_startproc
# BB#0:
	movslq	4(%rdi), %rax
	cmpq	$7, %rax
	jg	.LBB31_2
# BB#1:
	movzbl	8(%rdi), %edx
	movl	%eax, %ecx
	shll	%cl, %edx
	movl	$8, %ecx
	subl	%eax, %ecx
	movl	$255, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	orl	%edx, %esi
	movb	%sil, 8(%rdi)
	movq	stats(%rip), %rcx
	movq	img(%rip), %rdx
	movslq	20(%rdx), %rdx
	addq	%rax, 1960(%rcx,%rdx,8)
	movq	32(%rdi), %rax
	movslq	(%rdi), %rcx
	leal	1(%rcx), %edx
	movl	%edx, (%rdi)
	movb	%sil, (%rax,%rcx)
	movl	$8, 4(%rdi)
.LBB31_2:
	retq
.Lfunc_end31:
	.size	writeVlcByteAlign, .Lfunc_end31-writeVlcByteAlign
	.cfi_endproc

	.type	NCBP,@object            # @NCBP
	.section	.rodata,"a",@progbits
	.p2align	4
NCBP:
	.asciz	"\001"
	.ascii	"\n\001"
	.ascii	"\013\002"
	.ascii	"\006\005"
	.ascii	"\f\003"
	.ascii	"\007\006"
	.zero	2,14
	.ascii	"\002\n"
	.ascii	"\r\004"
	.zero	2,15
	.ascii	"\b\007"
	.ascii	"\003\013"
	.ascii	"\t\b"
	.ascii	"\004\f"
	.ascii	"\005\r"
	.ascii	"\000\t"
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.asciz	"\003"
	.ascii	"\035\002"
	.ascii	"\036\003"
	.ascii	"\021\007"
	.ascii	"\037\004"
	.ascii	"\022\b"
	.ascii	"%\021"
	.ascii	"\b\r"
	.ascii	" \005"
	.ascii	"&\022"
	.ascii	"\023\t"
	.ascii	"\t\016"
	.ascii	"\024\n"
	.ascii	"\n\017"
	.ascii	"\013\020"
	.ascii	"\002\013"
	.ascii	"\020\001"
	.ascii	"! "
	.ascii	"\"!"
	.ascii	"\025$"
	.ascii	"#\""
	.ascii	"\026%"
	.ascii	"',"
	.ascii	"\004("
	.ascii	"$#"
	.ascii	"(-"
	.ascii	"\027&"
	.ascii	"\005)"
	.ascii	"\030'"
	.ascii	"\006*"
	.ascii	"\007+"
	.ascii	"\001\023"
	.ascii	")\006"
	.ascii	"*\030"
	.ascii	"+\031"
	.ascii	"\031\024"
	.ascii	",\032"
	.ascii	"\032\025"
	.zero	2,46
	.ascii	"\f\034"
	.ascii	"-\033"
	.zero	2,47
	.ascii	"\033\026"
	.ascii	"\r\035"
	.ascii	"\034\027"
	.ascii	"\016\036"
	.ascii	"\017\037"
	.ascii	"\000\f"
	.size	NCBP, 192

	.type	levrun_linfo_c2x2.NTAB,@object # @levrun_linfo_c2x2.NTAB
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
levrun_linfo_c2x2.NTAB:
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	0                       # 0x0
	.size	levrun_linfo_c2x2.NTAB, 16

	.type	levrun_linfo_c2x2.LEVRUN,@object # @levrun_linfo_c2x2.LEVRUN
	.p2align	4
levrun_linfo_c2x2.LEVRUN:
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	levrun_linfo_c2x2.LEVRUN, 16

	.type	levrun_linfo_inter.LEVRUN,@object # @levrun_linfo_inter.LEVRUN
	.p2align	4
levrun_linfo_inter.LEVRUN:
	.asciz	"\004\002\002\001\001\001\001\001\001\001\000\000\000\000\000"
	.size	levrun_linfo_inter.LEVRUN, 16

	.type	levrun_linfo_inter.NTAB,@object # @levrun_linfo_inter.NTAB
	.section	.rodata,"a",@progbits
	.p2align	4
levrun_linfo_inter.NTAB:
	.ascii	"\001\003\005\t\013\r\025\027\031\033"
	.asciz	"\007\021\023\000\000\000\000\000\000"
	.asciz	"\017\000\000\000\000\000\000\000\000"
	.asciz	"\035\000\000\000\000\000\000\000\000"
	.size	levrun_linfo_inter.NTAB, 40

	.type	writeSyntaxElement_NumCoeffTrailingOnes.lentab,@object # @writeSyntaxElement_NumCoeffTrailingOnes.lentab
	.p2align	4
writeSyntaxElement_NumCoeffTrailingOnes.lentab:
	.long	1                       # 0x1
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	2                       # 0x2
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	10                      # 0xa
	.size	writeSyntaxElement_NumCoeffTrailingOnes.lentab, 816

	.type	writeSyntaxElement_NumCoeffTrailingOnes.codtab,@object # @writeSyntaxElement_NumCoeffTrailingOnes.codtab
	.p2align	4
writeSyntaxElement_NumCoeffTrailingOnes.codtab:
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	8                       # 0x8
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	7                       # 0x7
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	1                       # 0x1
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	3                       # 0x3
	.long	11                      # 0xb
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	4                       # 0x4
	.long	7                       # 0x7
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	8                       # 0x8
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	7                       # 0x7
	.long	9                       # 0x9
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	7                       # 0x7
	.long	10                      # 0xa
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	8                       # 0x8
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	9                       # 0x9
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	6                       # 0x6
	.long	10                      # 0xa
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	1                       # 0x1
	.long	4                       # 0x4
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	8                       # 0x8
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	8                       # 0x8
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	8                       # 0x8
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	5                       # 0x5
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	12                      # 0xc
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	7                       # 0x7
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	10                      # 0xa
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	7                       # 0x7
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	12                      # 0xc
	.long	11                      # 0xb
	.long	10                      # 0xa
	.long	9                       # 0x9
	.long	8                       # 0x8
	.long	13                      # 0xd
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	10                      # 0xa
	.long	6                       # 0x6
	.long	2                       # 0x2
	.size	writeSyntaxElement_NumCoeffTrailingOnes.codtab, 816

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ERROR: (numcoeff,trailingones) not valid: vlc=%d (%d, %d)\n"
	.size	.L.str, 59

	.type	writeSyntaxElement_NumCoeffTrailingOnesChromaDC.lentab,@object # @writeSyntaxElement_NumCoeffTrailingOnesChromaDC.lentab
	.section	.rodata,"a",@progbits
	.p2align	4
writeSyntaxElement_NumCoeffTrailingOnesChromaDC.lentab:
	.long	2                       # 0x2
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	16                      # 0x10
	.size	writeSyntaxElement_NumCoeffTrailingOnesChromaDC.lentab, 816

	.type	writeSyntaxElement_NumCoeffTrailingOnesChromaDC.codtab,@object # @writeSyntaxElement_NumCoeffTrailingOnesChromaDC.codtab
	.p2align	4
writeSyntaxElement_NumCoeffTrailingOnesChromaDC.codtab:
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	6                       # 0x6
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	15                      # 0xf
	.long	14                      # 0xe
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	13                      # 0xd
	.long	12                      # 0xc
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	11                      # 0xb
	.long	10                      # 0xa
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	9                       # 0x9
	.long	8                       # 0x8
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	8                       # 0x8
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	7                       # 0x7
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	1                       # 0x1
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	12                      # 0xc
	.long	8                       # 0x8
	.size	writeSyntaxElement_NumCoeffTrailingOnesChromaDC.codtab, 816

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"ERROR: (numcoeff,trailingones) not valid: (%d, %d)\n"
	.size	.L.str.1, 52

	.type	writeSyntaxElement_TotalZeros.lentab,@object # @writeSyntaxElement_TotalZeros.lentab
	.section	.rodata,"a",@progbits
	.p2align	4
writeSyntaxElement_TotalZeros.lentab:
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	writeSyntaxElement_TotalZeros.lentab, 960

	.type	writeSyntaxElement_TotalZeros.codtab,@object # @writeSyntaxElement_TotalZeros.codtab
	.p2align	4
writeSyntaxElement_TotalZeros.codtab:
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	writeSyntaxElement_TotalZeros.codtab, 960

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"ERROR: (TotalZeros) not valid: (%d)\n"
	.size	.L.str.2, 37

	.type	writeSyntaxElement_TotalZerosChromaDC.lentab,@object # @writeSyntaxElement_TotalZerosChromaDC.lentab
	.section	.rodata,"a",@progbits
	.p2align	4
writeSyntaxElement_TotalZerosChromaDC.lentab:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	writeSyntaxElement_TotalZerosChromaDC.lentab, 2880

	.type	writeSyntaxElement_TotalZerosChromaDC.codtab,@object # @writeSyntaxElement_TotalZerosChromaDC.codtab
	.p2align	4
writeSyntaxElement_TotalZerosChromaDC.codtab:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	writeSyntaxElement_TotalZerosChromaDC.codtab, 2880

	.type	writeSyntaxElement_Run.lentab,@object # @writeSyntaxElement_Run.lentab
	.p2align	4
writeSyntaxElement_Run.lentab:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	0                       # 0x0
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.size	writeSyntaxElement_Run.lentab, 960

	.type	writeSyntaxElement_Run.codtab,@object # @writeSyntaxElement_Run.codtab
	.p2align	4
writeSyntaxElement_Run.codtab:
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.zero	64
	.size	writeSyntaxElement_Run.codtab, 960

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"ERROR: (run) not valid: (%d)\n"
	.size	.L.str.3, 30

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
