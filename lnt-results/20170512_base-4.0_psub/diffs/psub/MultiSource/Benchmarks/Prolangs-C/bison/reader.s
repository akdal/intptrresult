	.text
	.file	"reader.bc"
	.globl	reader
	.p2align	4, 0x90
	.type	reader,@function
reader:                                 # @reader
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movl	$0, start_flag(%rip)
	movq	$0, startval(%rip)
	movl	$0, translations(%rip)
	movl	$1, nsyms(%rip)
	movl	$0, nvars(%rip)
	movl	$0, nrules(%rip)
	movl	$0, nitems(%rip)
	movl	$10, rline_allocated(%rip)
	movl	$20, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, rline(%rip)
	movb	$0, typed(%rip)
	movl	$0, lastprec(%rip)
	movl	$0, gensym_count(%rip)
	movl	$0, semantic_parser(%rip)
	movl	$0, pure_parser(%rip)
	movb	$0, yylsp_needed(%rip)
	movq	$0, grammar(%rip)
	callq	init_lex
	movl	$1, lineno(%rip)
	callq	tabinit
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	getsym
	movq	%rax, errtoken(%rip)
	movb	$1, 40(%rax)
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	getsym
	movb	$1, 40(%rax)
	movq	ftable(%rip), %r14
	movq	infile(%rip), %rbx
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rdx
	cmoveq	%rbx, %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	callq	read_declarations
	callq	output_ltype
	callq	output_headers
	callq	readgram
	callq	output_trailers
	cmpb	$1, yylsp_needed(%rip)
	jne	.LBB0_2
# BB#1:
	movq	ftable(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$22, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_2:
	callq	packsymbols
	callq	packgram
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free_symtab             # TAILCALL
.Lfunc_end0:
	.size	reader, .Lfunc_end0-reader
	.cfi_endproc

	.globl	read_declarations
	.p2align	4, 0x90
	.type	read_declarations,@function
read_declarations:                      # @read_declarations
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 48
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbx
	jmp	.LBB1_1
.LBB1_23:                               # %parse_expect_decl.exit
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	finput(%rip), %rsi
	movl	%eax, %edi
	callq	ungetc
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movl	%eax, expected_conflicts(%rip)
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_31:                               #   in Loop: Header=BB1_1 Depth=1
	subq	$8, %rsp
.Lcfi10:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.6, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%eax, %esi
	pushq	$0
.Lcfi11:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi12:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi14:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB1_1
.LBB1_7:                                #   in Loop: Header=BB1_1 Depth=1
	movl	$1, %edi
	movl	$2, %esi
	callq	parse_token_decl
	jmp	.LBB1_1
.LBB1_8:                                #   in Loop: Header=BB1_1 Depth=1
	movl	$2, %edi
	movl	$1, %esi
	callq	parse_token_decl
	jmp	.LBB1_1
.LBB1_29:                               #   in Loop: Header=BB1_1 Depth=1
	movl	$.L.str.4, %edi
	callq	fatal
	jmp	.LBB1_1
.LBB1_9:                                #   in Loop: Header=BB1_1 Depth=1
	callq	parse_type_decl
	jmp	.LBB1_1
.LBB1_15:                               #   in Loop: Header=BB1_1 Depth=1
	callq	parse_union_decl
	jmp	.LBB1_1
.LBB1_10:                               #   in Loop: Header=BB1_1 Depth=1
	cmpl	$0, start_flag(%rip)
	je	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_1 Depth=1
	movl	$.L.str.14, %edi
	callq	fatal
.LBB1_12:                               #   in Loop: Header=BB1_1 Depth=1
	movl	$1, start_flag(%rip)
	callq	lex
	cmpl	$1, %eax
	je	.LBB1_14
# BB#13:                                #   in Loop: Header=BB1_1 Depth=1
	movl	$.L.str.15, %edi
	callq	fatal
.LBB1_14:                               # %parse_start_decl.exit
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	symval(%rip), %rax
	movq	%rax, startval(%rip)
	jmp	.LBB1_1
.LBB1_24:                               #   in Loop: Header=BB1_1 Depth=1
	movl	$2, %edi
	callq	parse_assoc_decl
	jmp	.LBB1_1
.LBB1_25:                               #   in Loop: Header=BB1_1 Depth=1
	movl	$1, %edi
	callq	parse_assoc_decl
	jmp	.LBB1_1
.LBB1_26:                               #   in Loop: Header=BB1_1 Depth=1
	movl	$3, %edi
	callq	parse_assoc_decl
	jmp	.LBB1_1
.LBB1_27:                               #   in Loop: Header=BB1_1 Depth=1
	movl	$1, semantic_parser(%rip)
	callq	open_extra_files
	jmp	.LBB1_1
.LBB1_28:                               #   in Loop: Header=BB1_1 Depth=1
	movl	$1, pure_parser(%rip)
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_30:                               #   in Loop: Header=BB1_1 Depth=1
	movl	$.L.str.5, %edi
	callq	fatal
.LBB1_1:                                # %.backedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_16 Depth 2
                                        #     Child Loop BB1_20 Depth 2
	callq	skip_white_space
	cmpl	$-1, %eax
	je	.LBB1_30
# BB#2:                                 # %.backedge
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpl	$37, %eax
	jne	.LBB1_31
# BB#3:                                 #   in Loop: Header=BB1_1 Depth=1
	callq	parse_percent_token
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	addl	$-7, %eax
	cmpl	$16, %eax
	ja	.LBB1_29
# BB#4:                                 #   in Loop: Header=BB1_1 Depth=1
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_6:                                #   in Loop: Header=BB1_1 Depth=1
	callq	copy_definition
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_16:                               # %.backedge.i
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$9, %eax
	je	.LBB1_16
# BB#17:                                # %.backedge.i
                                        #   in Loop: Header=BB1_16 Depth=2
	cmpl	$32, %eax
	je	.LBB1_16
# BB#18:                                # %.preheader.i
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	%eax, %ecx
	addl	$-48, %ecx
	cmpl	$9, %ecx
	ja	.LBB1_23
# BB#19:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB1_1 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_20:                               # %.lr.ph.i
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$19, %ebp
	jg	.LBB1_22
# BB#21:                                #   in Loop: Header=BB1_20 Depth=2
	movslq	%ebp, %rcx
	incl	%ebp
	movb	%al, (%rsp,%rcx)
.LBB1_22:                               #   in Loop: Header=BB1_20 Depth=2
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ecx
	addl	$-48, %ecx
	cmpl	$10, %ecx
	jb	.LBB1_20
	jmp	.LBB1_23
.LBB1_5:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	read_declarations, .Lfunc_end1-read_declarations
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_5
	.quad	.LBB1_6
	.quad	.LBB1_7
	.quad	.LBB1_8
	.quad	.LBB1_29
	.quad	.LBB1_9
	.quad	.LBB1_15
	.quad	.LBB1_10
	.quad	.LBB1_24
	.quad	.LBB1_25
	.quad	.LBB1_26
	.quad	.LBB1_29
	.quad	.LBB1_27
	.quad	.LBB1_28
	.quad	.LBB1_29
	.quad	.LBB1_29
	.quad	.LBB1_16

	.text
	.globl	output_ltype
	.p2align	4, 0x90
	.type	output_ltype,@function
output_ltype:                           # @output_ltype
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 16
	movq	fattrs(%rip), %rcx
	movl	$.L.str.26, %edi
	movl	$188, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fdefines(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB2_2
# BB#1:
	movl	$.L.str.26, %edi
	movl	$188, %esi
	movl	$1, %edx
	callq	fwrite
.LBB2_2:
	movq	fattrs(%rip), %rcx
	movl	$.L.str.27, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fattrs(%rip), %rcx
	movl	$.L.str.28, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fattrs(%rip), %rcx
	movl	$.L.str.29, %edi
	movl	$30, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fattrs(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$49, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fattrs(%rip), %rcx
	movl	$.L.str.31, %edi
	movl	$51, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fattrs(%rip), %rcx
	movl	$.L.str.32, %edi
	movl	$51, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fattrs(%rip), %rcx
	movl	$.L.str.33, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fdefines(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB2_3
# BB#4:
	movl	$.L.str.27, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fdefines(%rip), %rcx
	movl	$.L.str.28, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fdefines(%rip), %rcx
	movl	$.L.str.29, %edi
	movl	$30, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fdefines(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$49, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fdefines(%rip), %rcx
	movl	$.L.str.31, %edi
	movl	$51, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fdefines(%rip), %rcx
	movl	$.L.str.32, %edi
	movl	$51, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fdefines(%rip), %rcx
	movl	$.L.str.33, %edi
	movl	$50, %esi
	movl	$1, %edx
	popq	%rax
	jmp	fwrite                  # TAILCALL
.LBB2_3:
	popq	%rax
	retq
.Lfunc_end2:
	.size	output_ltype, .Lfunc_end2-output_ltype
	.cfi_endproc

	.globl	readgram
	.p2align	4, 0x90
	.type	readgram,@function
readgram:                               # @readgram
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 112
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	callq	lex
	movl	%eax, %ebx
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
                                        # implicit-def: %R15
	testl	%ebx, %ebx
	jne	.LBB3_2
	jmp	.LBB3_73
.LBB3_66:                               # %parse_expect_decl.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	finput(%rip), %rsi
	movl	%eax, %edi
	callq	ungetc
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	32(%rsp), %rdi
	callq	strtol
	movl	%eax, expected_conflicts(%rip)
	jmp	.LBB3_51
.LBB3_18:                               #   in Loop: Header=BB3_2 Depth=1
	movl	16(%r15), %esi
	subq	$8, %rsp
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.52, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset -32
.LBB3_19:                               # %.preheader158.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%r15, 16(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	(%rsp), %r12            # 8-byte Reload
	jmp	.LBB3_20
	.p2align	4, 0x90
.LBB3_32:                               #   in Loop: Header=BB3_20 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r13d, %esi
	callq	copy_action
	incl	%r14d
	movl	$1, %ebp
	incl	%r13d
.LBB3_20:                               # %.preheader158
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	lex
	movl	%eax, %ebx
	cmpl	$6, %ebx
	je	.LBB3_32
# BB#21:                                # %.preheader158
                                        #   in Loop: Header=BB3_20 Depth=2
	cmpl	$1, %ebx
	jne	.LBB3_33
# BB#22:                                #   in Loop: Header=BB3_20 Depth=2
	movq	symval(%rip), %r15
	callq	lex
	movl	%eax, %ebx
	movl	%ebx, %edi
	callq	unlex
	movq	%r15, symval(%rip)
	cmpl	$3, %ebx
	je	.LBB3_23
# BB#26:                                #   in Loop: Header=BB3_20 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	cmoveq	%r15, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	je	.LBB3_31
# BB#27:                                #   in Loop: Header=BB3_20 Depth=2
	movl	gensym_count(%rip), %edx
	incl	%edx
	movl	%edx, gensym_count(%rip)
	movl	$token_buffer, %edi
	movl	$.L.str.49, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$token_buffer, %edi
	xorl	%eax, %eax
	callq	getsym
	movq	%rax, %rbx
	movb	$2, 40(%rbx)
	movl	nvars(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, nvars(%rip)
	movw	%ax, 32(%rbx)
	movslq	nrules(%rip), %rsi
	incq	%rsi
	movl	%esi, nrules(%rip)
	incl	nitems(%rip)
	cmpl	rline_allocated(%rip), %esi
	jl	.LBB3_30
# BB#28:                                #   in Loop: Header=BB3_20 Depth=2
	leal	(%rsi,%rsi), %eax
	movl	%eax, rline_allocated(%rip)
	movq	rline(%rip), %rdi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, rline(%rip)
	testq	%rax, %rax
	je	.LBB3_29
.LBB3_30:                               # %record_rule_line.exit151
                                        #   in Loop: Header=BB3_20 Depth=2
	movzwl	lineno(%rip), %eax
	movq	rline(%rip), %rcx
	movslq	nrules(%rip), %rdx
	movw	%ax, (%rcx,%rdx,2)
	movl	$24, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %rbp
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	testq	%rcx, %rcx
	movl	$grammar, %eax
	cmoveq	%rax, %rcx
	movq	%rbp, (%rcx)
	movq	%rbx, 8(%rbp)
	movl	$24, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, (%rbp)
	movq	%rax, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%rax)
	incl	nitems(%rip)
	movl	$24, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rbx, 8(%rax)
	movq	%rax, (%r12)
	movq	%rax, %r12
.LBB3_31:                               # %.thread
                                        #   in Loop: Header=BB3_20 Depth=2
	incl	nitems(%rip)
	xorl	%ebp, %ebp
	movl	$24, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	symval(%rip), %rcx
	movq	%rcx, 8(%rax)
	movq	%rax, (%r12)
	incl	%r13d
	movq	%rax, %r12
	jmp	.LBB3_20
.LBB3_29:                               #   in Loop: Header=BB3_20 Depth=2
	movq	stderr(%rip), %rcx
	movl	$.L.str.60, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	done
	jmp	.LBB3_30
	.p2align	4, 0x90
.LBB3_33:                               #   in Loop: Header=BB3_2 Depth=1
	movl	$24, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rax, (%r12)
	cmpl	$18, %ebx
	jne	.LBB3_35
# BB#34:                                #   in Loop: Header=BB3_2 Depth=1
	callq	lex
	movq	symval(%rip), %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rax, 16(%rcx)
	callq	lex
	movl	%eax, %ebx
.LBB3_35:                               #   in Loop: Header=BB3_2 Depth=1
	cmpl	$6, %ebx
	je	.LBB3_41
# BB#36:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	$11, %ebx
	jne	.LBB3_24
# BB#37:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	$0, semantic_parser(%rip)
	jne	.LBB3_39
# BB#38:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$.L.str.53, %edi
	callq	fatal
.LBB3_39:                               #   in Loop: Header=BB3_2 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r13d, %esi
	callq	copy_guard
	jmp	.LBB3_40
.LBB3_23:                               # %.thread155
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$24, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rax, (%r12)
	movl	$1, %ebx
.LBB3_24:                               #   in Loop: Header=BB3_2 Depth=1
	testl	%r14d, %r14d
	jne	.LBB3_25
# BB#44:                                #   in Loop: Header=BB3_2 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	movq	16(%rsp), %r15          # 8-byte Reload
	je	.LBB3_50
# BB#45:                                #   in Loop: Header=BB3_2 Depth=1
	movq	24(%r15), %rbp
	movq	24(%rax), %r12
	cmpq	%r12, %rbp
	je	.LBB3_50
# BB#46:                                #   in Loop: Header=BB3_2 Depth=1
	testq	%rbp, %rbp
	je	.LBB3_49
# BB#47:                                #   in Loop: Header=BB3_2 Depth=1
	testq	%r12, %r12
	je	.LBB3_49
# BB#48:                                #   in Loop: Header=BB3_2 Depth=1
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_50
.LBB3_49:                               #   in Loop: Header=BB3_2 Depth=1
	testq	%rbp, %rbp
	movl	$.L.str.56, %eax
	cmoveq	%rax, %rbp
	testq	%r12, %r12
	movq	stderr(%rip), %rdi
	movq	infile(%rip), %rdx
	movl	lineno(%rip), %ecx
	cmoveq	%rax, %r12
	movl	$.L.str.55, %esi
	xorl	%eax, %eax
	movq	%rbp, %r8
	movq	%r12, %r9
	callq	fprintf
	cmpl	$4, %ebx
	jne	.LBB3_1
	jmp	.LBB3_51
.LBB3_41:                               #   in Loop: Header=BB3_2 Depth=1
	testl	%ebp, %ebp
	je	.LBB3_43
# BB#42:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$.L.str.54, %edi
	callq	fatal
.LBB3_43:                               #   in Loop: Header=BB3_2 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r13d, %esi
	callq	copy_action
.LBB3_40:                               #   in Loop: Header=BB3_2 Depth=1
	callq	lex
	movl	%eax, %ebx
	movq	16(%rsp), %r15          # 8-byte Reload
	cmpl	$4, %ebx
	jne	.LBB3_1
	jmp	.LBB3_51
.LBB3_56:                               #   in Loop: Header=BB3_2 Depth=1
	movl	$2, %edi
	movl	$1, %esi
	callq	parse_token_decl
	jmp	.LBB3_51
.LBB3_72:                               #   in Loop: Header=BB3_2 Depth=1
	movl	$.L.str.57, %edi
	callq	fatal
	testl	%ebx, %ebx
	jne	.LBB3_2
	jmp	.LBB3_73
.LBB3_57:                               #   in Loop: Header=BB3_2 Depth=1
	callq	get_type
	movl	%eax, %ebx
	testl	%ebx, %ebx
	jne	.LBB3_2
	jmp	.LBB3_73
.LBB3_58:                               #   in Loop: Header=BB3_2 Depth=1
	callq	parse_union_decl
	jmp	.LBB3_51
.LBB3_67:                               #   in Loop: Header=BB3_2 Depth=1
	cmpl	$0, start_flag(%rip)
	je	.LBB3_69
# BB#68:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$.L.str.14, %edi
	callq	fatal
.LBB3_69:                               #   in Loop: Header=BB3_2 Depth=1
	movl	$1, start_flag(%rip)
	callq	lex
	cmpl	$1, %eax
	je	.LBB3_71
# BB#70:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$.L.str.15, %edi
	callq	fatal
.LBB3_71:                               # %parse_start_decl.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	symval(%rip), %rax
	movq	%rax, startval(%rip)
	jmp	.LBB3_51
.LBB3_25:                               #   in Loop: Header=BB3_2 Depth=1
	movq	16(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_50:                               #   in Loop: Header=BB3_2 Depth=1
	cmpl	$4, %ebx
	jne	.LBB3_1
	jmp	.LBB3_51
.LBB3_14:                               #   in Loop: Header=BB3_2 Depth=1
	movq	stderr(%rip), %rcx
	movl	$.L.str.60, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	done
	jmp	.LBB3_15
	.p2align	4, 0x90
.LBB3_51:                               #   in Loop: Header=BB3_2 Depth=1
	callq	lex
	movl	%eax, %ebx
.LBB3_1:                                # %.backedge
                                        #   in Loop: Header=BB3_2 Depth=1
	testl	%ebx, %ebx
	je	.LBB3_73
.LBB3_2:                                # %.backedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_59 Depth 2
                                        #     Child Loop BB3_63 Depth 2
                                        #     Child Loop BB3_20 Depth 2
	cmpl	$7, %ebx
	je	.LBB3_73
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	%ebx, %eax
	orl	$4, %eax
	cmpl	$5, %eax
	jne	.LBB3_52
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=1
	cmpl	$1, %ebx
	jne	.LBB3_7
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	symval(%rip), %r15
	callq	lex
	movl	%eax, %ebp
	movl	$3, %ebx
	cmpl	$3, %ebp
	je	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	$.L.str.50, %edi
	callq	fatal
	movl	%ebp, %ebx
.LBB3_7:                                #   in Loop: Header=BB3_2 Depth=1
	cmpl	$0, nrules(%rip)
	jne	.LBB3_12
# BB#8:                                 #   in Loop: Header=BB3_2 Depth=1
	cmpl	$5, %ebx
	jne	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	$.L.str.51, %edi
	callq	fatal
.LBB3_10:                               #   in Loop: Header=BB3_2 Depth=1
	cmpl	$0, start_flag(%rip)
	jne	.LBB3_12
# BB#11:                                #   in Loop: Header=BB3_2 Depth=1
	movq	%r15, startval(%rip)
	.p2align	4, 0x90
.LBB3_12:                               #   in Loop: Header=BB3_2 Depth=1
	movslq	nrules(%rip), %rsi
	incq	%rsi
	movl	%esi, nrules(%rip)
	incl	nitems(%rip)
	cmpl	rline_allocated(%rip), %esi
	jl	.LBB3_15
# BB#13:                                #   in Loop: Header=BB3_2 Depth=1
	leal	(%rsi,%rsi), %eax
	movl	%eax, rline_allocated(%rip)
	movq	rline(%rip), %rdi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, rline(%rip)
	testq	%rax, %rax
	je	.LBB3_14
.LBB3_15:                               # %record_rule_line.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movzwl	lineno(%rip), %eax
	movq	rline(%rip), %rcx
	movslq	nrules(%rip), %rdx
	movw	%ax, (%rcx,%rdx,2)
	movl	$24, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %rdx
	movq	%r15, 8(%rdx)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	testq	%rcx, %rcx
	movl	$grammar, %eax
	cmovneq	%rcx, %rax
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rdx, (%rax)
	movb	40(%r15), %al
	cmpb	$1, %al
	je	.LBB3_18
# BB#16:                                # %record_rule_line.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	testb	%al, %al
	jne	.LBB3_19
# BB#17:                                #   in Loop: Header=BB3_2 Depth=1
	movb	$2, 40(%r15)
	movl	nvars(%rip), %eax
	movw	%ax, 32(%r15)
	incl	%eax
	movl	%eax, nvars(%rip)
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_52:                               #   in Loop: Header=BB3_2 Depth=1
	leal	-9(%rbx), %eax
	cmpl	$14, %eax
	ja	.LBB3_72
# BB#53:                                #   in Loop: Header=BB3_2 Depth=1
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_54:                               #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %edi
	movl	$2, %esi
	callq	parse_token_decl
	jmp	.LBB3_51
	.p2align	4, 0x90
.LBB3_59:                               # %.backedge.i
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$9, %eax
	je	.LBB3_59
# BB#60:                                # %.backedge.i
                                        #   in Loop: Header=BB3_59 Depth=2
	cmpl	$32, %eax
	je	.LBB3_59
# BB#61:                                # %.preheader.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	%eax, %ecx
	addl	$-48, %ecx
	cmpl	$9, %ecx
	ja	.LBB3_66
# BB#62:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_63:                               # %.lr.ph.i
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$19, %ebx
	jg	.LBB3_65
# BB#64:                                #   in Loop: Header=BB3_63 Depth=2
	movslq	%ebx, %rcx
	incl	%ebx
	movb	%al, 32(%rsp,%rcx)
.LBB3_65:                               #   in Loop: Header=BB3_63 Depth=2
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ecx
	addl	$-48, %ecx
	cmpl	$10, %ecx
	jb	.LBB3_63
	jmp	.LBB3_66
.LBB3_73:
	cmpl	$0, nrules(%rip)
	jne	.LBB3_75
# BB#74:
	movl	$.L.str.5, %edi
	callq	fatal
.LBB3_75:
	movb	typed(%rip), %al
	testb	%al, %al
	jne	.LBB3_78
# BB#76:
	movq	fattrs(%rip), %rcx
	movl	$.L.str.58, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fdefines(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB3_78
# BB#77:
	movl	$.L.str.58, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
.LBB3_78:                               # %.preheader
	movq	firstsymbol(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_80
	jmp	.LBB3_83
	.p2align	4, 0x90
.LBB3_82:                               #   in Loop: Header=BB3_80 Depth=1
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB3_83
.LBB3_80:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, 40(%rbx)
	jne	.LBB3_82
# BB#81:                                #   in Loop: Header=BB3_80 Depth=1
	movq	stderr(%rip), %rdi
	movq	16(%rbx), %rdx
	movl	$.L.str.59, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, failure(%rip)
	movb	$2, 40(%rbx)
	movl	nvars(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, nvars(%rip)
	movw	%ax, 32(%rbx)
	jmp	.LBB3_82
.LBB3_83:                               # %._crit_edge
	movl	nsyms(%rip), %eax
	subl	nvars(%rip), %eax
	movl	%eax, ntokens(%rip)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	readgram, .Lfunc_end3-readgram
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_54
	.quad	.LBB3_56
	.quad	.LBB3_72
	.quad	.LBB3_57
	.quad	.LBB3_58
	.quad	.LBB3_67
	.quad	.LBB3_72
	.quad	.LBB3_72
	.quad	.LBB3_72
	.quad	.LBB3_72
	.quad	.LBB3_72
	.quad	.LBB3_72
	.quad	.LBB3_72
	.quad	.LBB3_72
	.quad	.LBB3_59

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.text
	.globl	packsymbols
	.p2align	4, 0x90
	.type	packsymbols,@function
packsymbols:                            # @packsymbols
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 16
.Lcfi35:
	.cfi_offset %rbx, -16
	movl	nsyms(%rip), %eax
	leal	8(,%rax,8), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, tags(%rip)
	movq	$.L.str.61, (%rax)
	movl	nsyms(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, sprec(%rip)
	movl	nsyms(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, sassoc(%rip)
	movl	$255, max_user_token_number(%rip)
	movq	firstsymbol(%rip), %rax
	testq	%rax, %rax
	je	.LBB4_1
# BB#2:                                 # %.lr.ph61
	movl	ntokens(%rip), %r8d
	cmpl	$0, translations(%rip)
	je	.LBB4_6
# BB#3:                                 # %.lr.ph61.split.preheader
	movl	$1, %esi
	movl	$255, %edx
	movl	$255, %r9d
	.p2align	4, 0x90
.LBB4_4:                                # %.lr.ph61.split
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$2, 40(%rax)
	jne	.LBB4_13
# BB#5:                                 #   in Loop: Header=BB4_4 Depth=1
	movl	32(%rax), %ebx
	addl	%r8d, %ebx
	movw	%bx, 32(%rax)
	jmp	.LBB4_18
	.p2align	4, 0x90
.LBB4_13:                               #   in Loop: Header=BB4_4 Depth=1
	movzwl	38(%rax), %ebx
	testw	%bx, %bx
	jne	.LBB4_15
# BB#14:                                #   in Loop: Header=BB4_4 Depth=1
	incl	%r9d
	movw	%r9w, 38(%rax)
	movl	%r9d, %ebx
.LBB4_15:                               #   in Loop: Header=BB4_4 Depth=1
	movswl	%bx, %ebx
	cmpl	%edx, %ebx
	jle	.LBB4_17
# BB#16:                                #   in Loop: Header=BB4_4 Depth=1
	movl	%ebx, max_user_token_number(%rip)
	movl	%ebx, %edx
.LBB4_17:                               #   in Loop: Header=BB4_4 Depth=1
	movw	%si, 32(%rax)
	movw	%si, %bx
	leal	1(%rsi), %ecx
	movl	%ecx, %esi
.LBB4_18:                               #   in Loop: Header=BB4_4 Depth=1
	movq	16(%rax), %rcx
	movq	tags(%rip), %rdi
	movswq	%bx, %rbx
	movq	%rcx, (%rdi,%rbx,8)
	movzwl	34(%rax), %ecx
	movq	sprec(%rip), %rdi
	movw	%cx, (%rdi,%rbx,2)
	movzwl	36(%rax), %ecx
	movq	sassoc(%rip), %rdi
	movswq	32(%rax), %rbx
	movw	%cx, (%rdi,%rbx,2)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.LBB4_4
	jmp	.LBB4_19
.LBB4_1:
	movl	$255, %edx
	cmpl	$0, translations(%rip)
	jne	.LBB4_31
	jmp	.LBB4_20
.LBB4_6:                                # %.lr.ph61.split.us.preheader
	movl	$1, %esi
	movl	$255, %edx
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph61.split.us
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$2, 40(%rax)
	jne	.LBB4_8
# BB#11:                                #   in Loop: Header=BB4_7 Depth=1
	movl	32(%rax), %edi
	addl	%r8d, %edi
	movw	%di, 32(%rax)
	jmp	.LBB4_12
	.p2align	4, 0x90
.LBB4_8:                                #   in Loop: Header=BB4_7 Depth=1
	movswl	38(%rax), %edi
	cmpl	%edx, %edi
	jle	.LBB4_10
# BB#9:                                 #   in Loop: Header=BB4_7 Depth=1
	movl	%edi, max_user_token_number(%rip)
	movl	%edi, %edx
.LBB4_10:                               #   in Loop: Header=BB4_7 Depth=1
	movw	%si, 32(%rax)
	movw	%si, %di
	leal	1(%rsi), %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
.LBB4_12:                               #   in Loop: Header=BB4_7 Depth=1
	movq	16(%rax), %rbx
	movq	tags(%rip), %rcx
	movswq	%di, %rdi
	movq	%rbx, (%rcx,%rdi,8)
	movzwl	34(%rax), %ecx
	movq	sprec(%rip), %rbx
	movw	%cx, (%rbx,%rdi,2)
	movzwl	36(%rax), %ecx
	movq	sassoc(%rip), %rdi
	movswq	32(%rax), %rbx
	movw	%cx, (%rdi,%rbx,2)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.LBB4_7
.LBB4_19:                               # %._crit_edge62
	cmpl	$0, translations(%rip)
	je	.LBB4_20
.LBB4_31:
	leal	2(%rdx,%rdx), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, token_translations(%rip)
	movslq	max_user_token_number(%rip), %rcx
	testq	%rcx, %rcx
	js	.LBB4_20
# BB#32:                                # %.lr.ph55
	leaq	1(%rcx), %rsi
	cmpq	$16, %rsi
	jae	.LBB4_34
# BB#33:
	xorl	%edx, %edx
	jmp	.LBB4_44
.LBB4_34:                               # %min.iters.checked
	movq	%rsi, %rdx
	andq	$-16, %rdx
	je	.LBB4_35
# BB#36:                                # %vector.body.preheader
	leaq	-16(%rdx), %r8
	movl	%r8d, %edi
	shrl	$4, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB4_37
# BB#38:                                # %vector.body.prol.preheader
	negq	%rdi
	xorl	%ebx, %ebx
	movaps	.LCPI4_0(%rip), %xmm0   # xmm0 = [2,2,2,2,2,2,2,2]
	.p2align	4, 0x90
.LBB4_39:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, (%rax,%rbx,2)
	movups	%xmm0, 16(%rax,%rbx,2)
	addq	$16, %rbx
	incq	%rdi
	jne	.LBB4_39
	jmp	.LBB4_40
.LBB4_35:
	xorl	%edx, %edx
	jmp	.LBB4_44
.LBB4_37:
	xorl	%ebx, %ebx
.LBB4_40:                               # %vector.body.prol.loopexit
	cmpq	$112, %r8
	jb	.LBB4_43
# BB#41:                                # %vector.body.preheader.new
	movq	%rdx, %rdi
	subq	%rbx, %rdi
	leaq	240(%rax,%rbx,2), %rbx
	movaps	.LCPI4_0(%rip), %xmm0   # xmm0 = [2,2,2,2,2,2,2,2]
	.p2align	4, 0x90
.LBB4_42:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rbx)
	movups	%xmm0, -224(%rbx)
	movups	%xmm0, -208(%rbx)
	movups	%xmm0, -192(%rbx)
	movups	%xmm0, -176(%rbx)
	movups	%xmm0, -160(%rbx)
	movups	%xmm0, -144(%rbx)
	movups	%xmm0, -128(%rbx)
	movups	%xmm0, -112(%rbx)
	movups	%xmm0, -96(%rbx)
	movups	%xmm0, -80(%rbx)
	movups	%xmm0, -64(%rbx)
	movups	%xmm0, -48(%rbx)
	movups	%xmm0, -32(%rbx)
	movups	%xmm0, -16(%rbx)
	movups	%xmm0, (%rbx)
	addq	$256, %rbx              # imm = 0x100
	addq	$-128, %rdi
	jne	.LBB4_42
.LBB4_43:                               # %middle.block
	cmpq	%rdx, %rsi
	je	.LBB4_20
.LBB4_44:                               # %scalar.ph.preheader
	decq	%rdx
	.p2align	4, 0x90
.LBB4_45:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movw	$2, 2(%rax,%rdx,2)
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB4_45
.LBB4_20:                               # %.preheader
	movq	firstsymbol(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_22
	jmp	.LBB4_28
	.p2align	4, 0x90
.LBB4_27:                               #   in Loop: Header=BB4_22 Depth=1
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB4_28
.LBB4_22:                               # %.lr.ph52
                                        # =>This Inner Loop Header: Depth=1
	movswl	32(%rbx), %eax
	cmpl	ntokens(%rip), %eax
	jge	.LBB4_27
# BB#23:                                # %.lr.ph52
                                        #   in Loop: Header=BB4_22 Depth=1
	movl	translations(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB4_27
# BB#24:                                #   in Loop: Header=BB4_22 Depth=1
	movq	token_translations(%rip), %rdx
	movswq	38(%rbx), %rcx
	movswq	(%rdx,%rcx,2), %rsi
	cmpq	$2, %rsi
	je	.LBB4_26
# BB#25:                                #   in Loop: Header=BB4_22 Depth=1
	movq	tags(%rip), %rax
	movl	(%rax,%rsi,8), %esi
	movl	16(%rbx), %edx
	subq	$8, %rsp
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.62, %edi
	movl	$0, %r8d
	movl	$0, %r9d
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	pushq	$0
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi40:
	.cfi_adjust_cfa_offset -32
	movw	32(%rbx), %ax
	movw	38(%rbx), %cx
	movq	token_translations(%rip), %rdx
.LBB4_26:                               #   in Loop: Header=BB4_22 Depth=1
	movswq	%cx, %rcx
	movw	%ax, (%rdx,%rcx,2)
	jmp	.LBB4_27
.LBB4_28:                               # %._crit_edge
	movq	errtoken(%rip), %rax
	movswl	32(%rax), %eax
	movl	%eax, error_token_number(%rip)
	movq	ftable(%rip), %rdi
	callq	output_token_defines
	movq	startval(%rip), %rax
	movb	40(%rax), %cl
	cmpb	$1, %cl
	je	.LBB4_46
# BB#29:                                # %._crit_edge
	testb	%cl, %cl
	jne	.LBB4_47
# BB#30:
	movl	16(%rax), %esi
	subq	$8, %rsp
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.63, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi45:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB4_47
.LBB4_46:
	movl	16(%rax), %esi
	subq	$8, %rsp
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.64, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi50:
	.cfi_adjust_cfa_offset -32
.LBB4_47:
	movq	startval(%rip), %rax
	movswl	32(%rax), %eax
	movl	%eax, start_symbol(%rip)
	cmpl	$0, definesflag(%rip)
	je	.LBB4_54
# BB#48:
	movq	fdefines(%rip), %rdi
	callq	output_token_defines
	cmpl	$0, semantic_parser(%rip)
	je	.LBB4_53
# BB#49:
	movslq	ntokens(%rip), %rbx
	movl	nsyms(%rip), %eax
	cmpl	%eax, %ebx
	jge	.LBB4_53
	.p2align	4, 0x90
.LBB4_50:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	tags(%rip), %rcx
	movq	(%rcx,%rbx,8), %rdx
	cmpb	$64, (%rdx)
	je	.LBB4_52
# BB#51:                                #   in Loop: Header=BB4_50 Depth=1
	movq	fdefines(%rip), %rdi
	movl	$.L.str.65, %esi
	xorl	%eax, %eax
	movl	%ebx, %ecx
	callq	fprintf
	movl	nsyms(%rip), %eax
.LBB4_52:                               #   in Loop: Header=BB4_50 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB4_50
.LBB4_53:                               # %.loopexit
	movq	fdefines(%rip), %rdi
	callq	fclose
	movq	$0, fdefines(%rip)
.LBB4_54:
	popq	%rbx
	retq
.Lfunc_end4:
	.size	packsymbols, .Lfunc_end4-packsymbols
	.cfi_endproc

	.globl	packgram
	.p2align	4, 0x90
	.type	packgram,@function
packgram:                               # @packgram
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 32
.Lcfi54:
	.cfi_offset %rbx, -32
.Lcfi55:
	.cfi_offset %r14, -24
.Lcfi56:
	.cfi_offset %r15, -16
	movl	nitems(%rip), %eax
	leal	2(%rax,%rax), %edi
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, ritem(%rip)
	movl	nrules(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	addq	$-2, %rax
	movq	%rax, rlhs(%rip)
	movl	nrules(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	addq	$-2, %rax
	movq	%rax, rrhs(%rip)
	movl	nrules(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	addq	$-2, %rax
	movq	%rax, rprec(%rip)
	movl	nrules(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	addq	$-2, %rax
	movq	%rax, rassoc(%rip)
	movq	rlhs(%rip), %r8
	movq	rrhs(%rip), %r9
	movq	ritem(%rip), %r15
	movl	$grammar, %esi
	movl	$1, %edi
	movq	rprec(%rip), %r14
.LBB5_1:                                # %.sink.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_2 Depth 2
                                        #       Child Loop BB5_6 Depth 3
	movq	(%rsi), %rdx
	movslq	%edi, %rdi
	.p2align	4, 0x90
.LBB5_2:                                #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_6 Depth 3
	testq	%rdx, %rdx
	je	.LBB5_13
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=2
	movq	8(%rdx), %rsi
	movzwl	32(%rsi), %esi
	movw	%si, (%r8,%rdi,2)
	movw	%bx, (%r9,%rdi,2)
	movq	(%rdx), %rsi
	movq	16(%rdx), %r10
	testq	%rsi, %rsi
	je	.LBB5_4
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB5_2 Depth=2
	movslq	%ebx, %rbx
	.p2align	4, 0x90
.LBB5_6:                                #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	je	.LBB5_9
# BB#7:                                 #   in Loop: Header=BB5_6 Depth=3
	movzwl	32(%rdx), %ecx
	movw	%cx, (%r15,%rbx,2)
	cmpb	$1, 40(%rdx)
	jne	.LBB5_8
# BB#14:                                #   in Loop: Header=BB5_6 Depth=3
	movzwl	34(%rdx), %ecx
	movw	%cx, (%r14,%rdi,2)
	movzwl	36(%rdx), %ecx
	movw	%cx, (%rax,%rdi,2)
.LBB5_8:                                # %.backedge
                                        #   in Loop: Header=BB5_6 Depth=3
	incq	%rbx
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB5_6
.LBB5_4:                                #   in Loop: Header=BB5_2 Depth=2
	xorl	%esi, %esi
	xorl	%r11d, %r11d
	testq	%r10, %r10
	jne	.LBB5_11
	jmp	.LBB5_12
	.p2align	4, 0x90
.LBB5_9:                                # %..critedge.loopexit_crit_edge
                                        #   in Loop: Header=BB5_2 Depth=2
	movb	$1, %r11b
	testq	%r10, %r10
	je	.LBB5_12
.LBB5_11:                               #   in Loop: Header=BB5_2 Depth=2
	movzwl	34(%r10), %ecx
	movw	%cx, (%r14,%rdi,2)
	movzwl	36(%r10), %ecx
	movw	%cx, (%rax,%rdi,2)
.LBB5_12:                               #   in Loop: Header=BB5_2 Depth=2
	movl	%edi, %ecx
	negl	%ecx
	movslq	%ebx, %rdx
	incl	%ebx
	movw	%cx, (%r15,%rdx,2)
	incq	%rdi
	testb	%r11b, %r11b
	movl	$0, %edx
	je	.LBB5_2
	jmp	.LBB5_1
.LBB5_13:
	movslq	%ebx, %rax
	movw	$0, (%r15,%rax,2)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	packgram, .Lfunc_end5-packgram
	.cfi_endproc

	.globl	copy_definition
	.p2align	4, 0x90
	.type	copy_definition,@function
copy_definition:                        # @copy_definition
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 32
.Lcfi60:
	.cfi_offset %rbx, -32
.Lcfi61:
	.cfi_offset %r14, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	cmpl	$0, nolinesflag(%rip)
	jne	.LBB6_2
# BB#1:
	movq	fattrs(%rip), %r14
	movl	lineno(%rip), %ebp
	movq	infile(%rip), %rbx
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rcx
	cmoveq	%rbx, %rcx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	fprintf
	jmp	.LBB6_2
	.p2align	4, 0x90
.LBB6_34:                               # %._crit_edge
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	fattrs(%rip), %rsi
	movl	%ebx, %edi
	callq	_IO_putc
.LBB6_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_3 Depth 2
                                        #     Child Loop BB6_20 Depth 2
                                        #       Child Loop BB6_30 Depth 3
                                        #     Child Loop BB6_7 Depth 2
                                        #       Child Loop BB6_8 Depth 3
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	jmp	.LBB6_3
.LBB6_36:                               #   in Loop: Header=BB6_3 Depth=2
	movq	fattrs(%rip), %rsi
	movl	$37, %edi
	callq	_IO_putc
	.p2align	4, 0x90
.LBB6_3:                                # %.backedge43
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1(%rbx), %eax
	cmpl	$48, %eax
	ja	.LBB6_34
# BB#4:                                 # %.backedge43
                                        #   in Loop: Header=BB6_3 Depth=2
	jmpq	*.LJTI6_0(,%rax,8)
.LBB6_35:                               #   in Loop: Header=BB6_3 Depth=2
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$125, %ebx
	jne	.LBB6_36
	jmp	.LBB6_37
.LBB6_18:                               #   in Loop: Header=BB6_3 Depth=2
	movq	fattrs(%rip), %rsi
	movl	$47, %edi
	callq	_IO_putc
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$42, %ebx
	jne	.LBB6_3
# BB#19:                                #   in Loop: Header=BB6_2 Depth=1
	movq	fattrs(%rip), %rsi
	movl	$42, %edi
	jmp	.LBB6_20
	.p2align	4, 0x90
.LBB6_6:                                #   in Loop: Header=BB6_2 Depth=1
	movq	fattrs(%rip), %rsi
	movl	%ebx, %edi
	jmp	.LBB6_7
	.p2align	4, 0x90
.LBB6_11:                               # %.thread
                                        #   in Loop: Header=BB6_7 Depth=2
	movl	$.L.str.8, %edi
	callq	fatal
	movq	fattrs(%rip), %rsi
	movl	%ebp, %edi
.LBB6_7:                                #   Parent Loop BB6_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_8 Depth 3
	callq	_IO_putc
	jmp	.LBB6_8
.LBB6_17:                               #   in Loop: Header=BB6_8 Depth=3
	incl	lineno(%rip)
	.p2align	4, 0x90
.LBB6_8:                                #   Parent Loop BB6_2 Depth=1
                                        #     Parent Loop BB6_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	%ebx, %ebp
	je	.LBB6_34
# BB#9:                                 # %.lr.ph54
                                        #   in Loop: Header=BB6_8 Depth=3
	cmpl	$10, %ebp
	je	.LBB6_11
# BB#10:                                # %.lr.ph54
                                        #   in Loop: Header=BB6_8 Depth=3
	cmpl	$-1, %ebp
	je	.LBB6_11
# BB#12:                                #   in Loop: Header=BB6_8 Depth=3
	movq	fattrs(%rip), %rsi
	movl	%ebp, %edi
	callq	_IO_putc
	cmpl	$92, %ebp
	jne	.LBB6_8
# BB#13:                                #   in Loop: Header=BB6_8 Depth=3
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$10, %ebp
	je	.LBB6_15
# BB#14:                                #   in Loop: Header=BB6_8 Depth=3
	cmpl	$-1, %ebp
	jne	.LBB6_16
.LBB6_15:                               #   in Loop: Header=BB6_8 Depth=3
	movl	$.L.str.8, %edi
	callq	fatal
.LBB6_16:                               #   in Loop: Header=BB6_8 Depth=3
	movq	fattrs(%rip), %rsi
	movl	%ebp, %edi
	callq	_IO_putc
	cmpl	$10, %ebp
	jne	.LBB6_8
	jmp	.LBB6_17
.LBB6_33:                               #   in Loop: Header=BB6_2 Depth=1
	movl	$.L.str.10, %edi
	callq	fatal
	jmp	.LBB6_34
.LBB6_5:                                #   in Loop: Header=BB6_2 Depth=1
	movq	fattrs(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	incl	lineno(%rip)
	jmp	.LBB6_2
.LBB6_32:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB6_20 Depth=2
	movq	fattrs(%rip), %rsi
	movl	%eax, %edi
	.p2align	4, 0x90
.LBB6_20:                               # %.lr.ph.split.us
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_30 Depth 3
	callq	_IO_putc
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$42, %eax
	je	.LBB6_25
	jmp	.LBB6_22
.LBB6_31:                               # %.us-lcssa51.us
                                        #   in Loop: Header=BB6_20 Depth=2
	movl	$.L.str.9, %edi
	callq	fatal
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB6_21:                               # %.lr.ph.split.us
                                        #   in Loop: Header=BB6_20 Depth=2
	cmpl	$42, %eax
	jne	.LBB6_22
.LBB6_25:                               # %.preheader.us.preheader
                                        #   in Loop: Header=BB6_20 Depth=2
	movl	$42, %eax
	cmpl	$42, %eax
	jne	.LBB6_27
	.p2align	4, 0x90
.LBB6_30:                               #   Parent Loop BB6_2 Depth=1
                                        #     Parent Loop BB6_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	fattrs(%rip), %rsi
	movl	$42, %edi
	callq	_IO_putc
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$42, %eax
	je	.LBB6_30
.LBB6_27:                               # %.preheader.us
                                        #   in Loop: Header=BB6_20 Depth=2
	cmpl	$47, %eax
	jne	.LBB6_21
	jmp	.LBB6_28
.LBB6_22:                               # %.lr.ph.split.us
                                        #   in Loop: Header=BB6_20 Depth=2
	cmpl	$-1, %eax
	je	.LBB6_31
# BB#23:                                # %.lr.ph.split.us
                                        #   in Loop: Header=BB6_20 Depth=2
	cmpl	$10, %eax
	jne	.LBB6_32
# BB#24:                                # %.us-lcssa50.us
                                        #   in Loop: Header=BB6_20 Depth=2
	incl	lineno(%rip)
	movq	fattrs(%rip), %rsi
	movl	$10, %edi
	jmp	.LBB6_20
.LBB6_28:                               # %.thread42.loopexit56
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	fattrs(%rip), %rsi
	movl	$47, %edi
	callq	_IO_putc
	jmp	.LBB6_2
.LBB6_37:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end6:
	.size	copy_definition, .Lfunc_end6-copy_definition
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_33
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_5
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_6
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_35
	.quad	.LBB6_34
	.quad	.LBB6_6
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_34
	.quad	.LBB6_18

	.text
	.globl	parse_token_decl
	.p2align	4, 0x90
	.type	parse_token_decl,@function
parse_token_decl:                       # @parse_token_decl
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi66:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi69:
	.cfi_def_cfa_offset 64
.Lcfi70:
	.cfi_offset %rbx, -56
.Lcfi71:
	.cfi_offset %r12, -48
.Lcfi72:
	.cfi_offset %r13, -40
.Lcfi73:
	.cfi_offset %r14, -32
.Lcfi74:
	.cfi_offset %r15, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movl	%edi, %r15d
	callq	skip_white_space
	movq	finput(%rip), %rsi
	movl	%eax, %edi
	callq	ungetc
	cmpl	$37, %eax
	je	.LBB7_38
# BB#1:                                 # %.lr.ph.lr.ph
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
.LBB7_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_18 Depth 2
                                        #     Child Loop BB7_3 Depth 2
	cmpl	$2, %r15d
	jne	.LBB7_18
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph.split.us
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	lex
	movl	%eax, %ebp
	cmpl	$1, %ebp
	je	.LBB7_9
# BB#4:                                 # %.lr.ph.split.us
                                        #   in Loop: Header=BB7_3 Depth=2
	movl	$2, %r13d
	cmpl	$2, %ebp
	je	.LBB7_17
# BB#5:                                 # %.lr.ph.split.us
                                        #   in Loop: Header=BB7_3 Depth=2
	cmpl	$21, %ebp
	je	.LBB7_33
# BB#6:                                 #   in Loop: Header=BB7_3 Depth=2
	cmpl	$1, %ebx
	jne	.LBB7_14
# BB#7:                                 #   in Loop: Header=BB7_3 Depth=2
	cmpl	$22, %ebp
	jne	.LBB7_14
# BB#8:                                 #   in Loop: Header=BB7_3 Depth=2
	movzwl	numval(%rip), %eax
	movq	symval(%rip), %rcx
	movw	%ax, 38(%rcx)
	movl	$1, translations(%rip)
	movl	$22, %r13d
	jmp	.LBB7_17
	.p2align	4, 0x90
.LBB7_9:                                #   in Loop: Header=BB7_3 Depth=2
	movq	symval(%rip), %rax
	movsbl	40(%rax), %ecx
	cmpl	%r14d, %ecx
	jne	.LBB7_11
# BB#10:                                #   in Loop: Header=BB7_3 Depth=2
	movl	16(%rax), %esi
	subq	$8, %rsp
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.11, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi78:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi80:
	.cfi_adjust_cfa_offset -32
	movq	symval(%rip), %rax
.LBB7_11:                               #   in Loop: Header=BB7_3 Depth=2
	movb	%r15b, 40(%rax)
	movl	nvars(%rip), %ecx
	leal	1(%rcx), %edx
	testq	%r12, %r12
	movl	%edx, nvars(%rip)
	movw	%cx, 32(%rax)
	je	.LBB7_36
# BB#12:                                #   in Loop: Header=BB7_3 Depth=2
	cmpq	$0, 24(%rax)
	je	.LBB7_15
# BB#13:                                #   in Loop: Header=BB7_3 Depth=2
	movl	16(%rax), %esi
	subq	$8, %rsp
.Lcfi81:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.12, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
.Lcfi82:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi83:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi84:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi85:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB7_16
.LBB7_14:                               #   in Loop: Header=BB7_3 Depth=2
	movl	$.L.str.13, %edi
	callq	fatal
	movl	%ebp, %r13d
	jmp	.LBB7_17
.LBB7_15:                               #   in Loop: Header=BB7_3 Depth=2
	movq	%r12, 24(%rax)
.LBB7_16:                               # %.backedge.us
                                        #   in Loop: Header=BB7_3 Depth=2
	movl	$1, %r13d
.LBB7_17:                               # %.backedge.us
                                        #   in Loop: Header=BB7_3 Depth=2
	callq	skip_white_space
	movq	finput(%rip), %rsi
	movl	%eax, %edi
	callq	ungetc
	cmpl	$37, %eax
	movl	%r13d, %ebx
	jne	.LBB7_3
	jmp	.LBB7_38
	.p2align	4, 0x90
.LBB7_18:                               # %.lr.ph.split
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	lex
	movl	%eax, %ebp
	cmpl	$1, %ebp
	je	.LBB7_24
# BB#19:                                # %.lr.ph.split
                                        #   in Loop: Header=BB7_18 Depth=2
	movl	$2, %r13d
	cmpl	$2, %ebp
	je	.LBB7_32
# BB#20:                                # %.lr.ph.split
                                        #   in Loop: Header=BB7_18 Depth=2
	cmpl	$21, %ebp
	je	.LBB7_33
# BB#21:                                #   in Loop: Header=BB7_18 Depth=2
	cmpl	$1, %ebx
	jne	.LBB7_29
# BB#22:                                #   in Loop: Header=BB7_18 Depth=2
	cmpl	$22, %ebp
	jne	.LBB7_29
# BB#23:                                #   in Loop: Header=BB7_18 Depth=2
	movzwl	numval(%rip), %eax
	movq	symval(%rip), %rcx
	movw	%ax, 38(%rcx)
	movl	$1, translations(%rip)
	movl	$22, %r13d
	jmp	.LBB7_32
	.p2align	4, 0x90
.LBB7_24:                               #   in Loop: Header=BB7_18 Depth=2
	movq	symval(%rip), %rax
	movsbl	40(%rax), %ecx
	cmpl	%r14d, %ecx
	jne	.LBB7_26
# BB#25:                                #   in Loop: Header=BB7_18 Depth=2
	movl	16(%rax), %esi
	subq	$8, %rsp
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.11, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
.Lcfi87:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi89:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi90:
	.cfi_adjust_cfa_offset -32
	movq	symval(%rip), %rax
.LBB7_26:                               #   in Loop: Header=BB7_18 Depth=2
	testq	%r12, %r12
	movb	%r15b, 40(%rax)
	je	.LBB7_36
# BB#27:                                #   in Loop: Header=BB7_18 Depth=2
	cmpq	$0, 24(%rax)
	je	.LBB7_30
# BB#28:                                #   in Loop: Header=BB7_18 Depth=2
	movl	16(%rax), %esi
	subq	$8, %rsp
.Lcfi91:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.12, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
.Lcfi92:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi95:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB7_31
.LBB7_29:                               #   in Loop: Header=BB7_18 Depth=2
	movl	$.L.str.13, %edi
	callq	fatal
	movl	%ebp, %r13d
	jmp	.LBB7_32
.LBB7_30:                               #   in Loop: Header=BB7_18 Depth=2
	movq	%r12, 24(%rax)
.LBB7_31:                               # %.backedge
                                        #   in Loop: Header=BB7_18 Depth=2
	movl	$1, %r13d
.LBB7_32:                               # %.backedge
                                        #   in Loop: Header=BB7_18 Depth=2
	callq	skip_white_space
	movq	finput(%rip), %rsi
	movl	%eax, %edi
	callq	ungetc
	cmpl	$37, %eax
	movl	%r13d, %ebx
	jne	.LBB7_18
	jmp	.LBB7_38
	.p2align	4, 0x90
.LBB7_33:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	$token_buffer, %edi
	callq	strlen
	movq	%rax, %rbp
	testq	%r12, %r12
	je	.LBB7_35
# BB#34:                                #   in Loop: Header=BB7_2 Depth=1
	movq	%r12, %rdi
	callq	free
.LBB7_35:                               #   in Loop: Header=BB7_2 Depth=1
	incl	%ebp
	xorl	%eax, %eax
	movl	%ebp, %edi
	callq	mallocate
	movq	%rax, %r12
	movl	$token_buffer, %esi
	movq	%r12, %rdi
	callq	strcpy
	movl	$21, %ebx
	jmp	.LBB7_37
.LBB7_36:                               #   in Loop: Header=BB7_2 Depth=1
	movl	$1, %ebx
	xorl	%r12d, %r12d
.LBB7_37:                               # %.outer.backedge
                                        #   in Loop: Header=BB7_2 Depth=1
	callq	skip_white_space
	movq	finput(%rip), %rsi
	movl	%eax, %edi
	callq	ungetc
	cmpl	$37, %eax
	jne	.LBB7_2
.LBB7_38:                               # %.outer._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	parse_token_decl, .Lfunc_end7-parse_token_decl
	.cfi_endproc

	.globl	parse_type_decl
	.p2align	4, 0x90
	.type	parse_type_decl,@function
parse_type_decl:                        # @parse_type_decl
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 16
.Lcfi97:
	.cfi_offset %rbx, -16
	callq	lex
	cmpl	$21, %eax
	je	.LBB8_2
# BB#1:
	movl	$.L.str.16, %edi
	callq	fatal
.LBB8_2:
	movl	$token_buffer, %edi
	callq	strlen
	leal	1(%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %rbx
	movl	$token_buffer, %esi
	movq	%rbx, %rdi
	callq	strcpy
	jmp	.LBB8_3
	.p2align	4, 0x90
.LBB8_6:                                #   in Loop: Header=BB8_3 Depth=1
	movl	$.L.str.17, %edi
	callq	fatal
.LBB8_3:                                # =>This Inner Loop Header: Depth=1
	callq	skip_white_space
	movq	finput(%rip), %rsi
	movl	%eax, %edi
	callq	ungetc
	cmpl	$37, %eax
	je	.LBB8_7
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB8_3 Depth=1
	callq	lex
	cmpl	$1, %eax
	je	.LBB8_8
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB8_3 Depth=1
	cmpl	$2, %eax
	je	.LBB8_3
	jmp	.LBB8_6
	.p2align	4, 0x90
.LBB8_8:                                #   in Loop: Header=BB8_3 Depth=1
	movq	symval(%rip), %rax
	cmpq	$0, 24(%rax)
	je	.LBB8_9
# BB#10:                                #   in Loop: Header=BB8_3 Depth=1
	movl	16(%rax), %esi
	subq	$8, %rsp
.Lcfi98:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.12, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi100:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi101:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi102:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB8_3
.LBB8_9:                                #   in Loop: Header=BB8_3 Depth=1
	movq	%rbx, 24(%rax)
	jmp	.LBB8_3
.LBB8_7:                                # %.critedge
	popq	%rbx
	retq
.Lfunc_end8:
	.size	parse_type_decl, .Lfunc_end8-parse_type_decl
	.cfi_endproc

	.globl	parse_start_decl
	.p2align	4, 0x90
	.type	parse_start_decl,@function
parse_start_decl:                       # @parse_start_decl
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi103:
	.cfi_def_cfa_offset 16
	cmpl	$0, start_flag(%rip)
	je	.LBB9_2
# BB#1:
	movl	$.L.str.14, %edi
	callq	fatal
.LBB9_2:
	movl	$1, start_flag(%rip)
	callq	lex
	cmpl	$1, %eax
	je	.LBB9_4
# BB#3:
	movl	$.L.str.15, %edi
	callq	fatal
.LBB9_4:
	movq	symval(%rip), %rax
	movq	%rax, startval(%rip)
	popq	%rax
	retq
.Lfunc_end9:
	.size	parse_start_decl, .Lfunc_end9-parse_start_decl
	.cfi_endproc

	.globl	parse_union_decl
	.p2align	4, 0x90
	.type	parse_union_decl,@function
parse_union_decl:                       # @parse_union_decl
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -32
.Lcfi108:
	.cfi_offset %r14, -24
.Lcfi109:
	.cfi_offset %rbp, -16
	cmpb	$1, typed(%rip)
	jne	.LBB10_2
# BB#1:
	movl	$.L.str.20, %edi
	callq	fatal
.LBB10_2:
	movb	$1, typed(%rip)
	movq	fattrs(%rip), %rbx
	cmpl	$0, nolinesflag(%rip)
	je	.LBB10_3
# BB#4:
	movl	$10, %edi
	movq	%rbx, %rsi
	callq	fputc
	jmp	.LBB10_5
.LBB10_3:
	movl	lineno(%rip), %r14d
	movq	infile(%rip), %rbp
	movl	$47, %esi
	movq	%rbp, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rcx
	cmoveq	%rbp, %rcx
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	fprintf
.LBB10_5:
	movq	fattrs(%rip), %rcx
	movl	$.L.str.23, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fdefines(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB10_7
# BB#6:
	movl	$.L.str.23, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
.LBB10_7:                               # %.preheader
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB10_25
# BB#8:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	jmp	.LBB10_9
.LBB10_37:                              #   in Loop: Header=BB10_9 Depth=1
	incl	%ebp
	jmp	.LBB10_24
	.p2align	4, 0x90
.LBB10_9:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_29 Depth 2
	movq	fattrs(%rip), %rsi
	movl	%ebx, %edi
	callq	_IO_putc
	movq	fdefines(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB10_11
# BB#10:                                #   in Loop: Header=BB10_9 Depth=1
	movl	%ebx, %edi
	callq	_IO_putc
.LBB10_11:                              #   in Loop: Header=BB10_9 Depth=1
	cmpl	$46, %ebx
	jle	.LBB10_12
# BB#14:                                #   in Loop: Header=BB10_9 Depth=1
	cmpl	$47, %ebx
	jne	.LBB10_15
# BB#22:                                #   in Loop: Header=BB10_9 Depth=1
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$42, %eax
	jne	.LBB10_23
# BB#26:                                #   in Loop: Header=BB10_9 Depth=1
	movq	fattrs(%rip), %rsi
	movl	$42, %edi
	callq	_IO_putc
	movq	fdefines(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB10_28
# BB#27:                                #   in Loop: Header=BB10_9 Depth=1
	movl	$42, %edi
	callq	_IO_putc
.LBB10_28:                              # %.outer..outer.split_crit_edge
                                        #   in Loop: Header=BB10_9 Depth=1
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	.p2align	4, 0x90
.LBB10_29:                              #   Parent Loop BB10_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %ebx
	jne	.LBB10_31
# BB#30:                                #   in Loop: Header=BB10_29 Depth=2
	movl	$.L.str.24, %edi
	callq	fatal
.LBB10_31:                              #   in Loop: Header=BB10_29 Depth=2
	movq	fattrs(%rip), %rsi
	movl	%ebx, %edi
	callq	_IO_putc
	movq	fdefines(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB10_33
# BB#32:                                #   in Loop: Header=BB10_29 Depth=2
	movl	%ebx, %edi
	callq	_IO_putc
.LBB10_33:                              #   in Loop: Header=BB10_29 Depth=2
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$42, %ebx
	movl	%eax, %ebx
	jne	.LBB10_29
# BB#34:                                #   in Loop: Header=BB10_29 Depth=2
	cmpl	$47, %eax
	movl	%eax, %ebx
	jne	.LBB10_29
# BB#35:                                #   in Loop: Header=BB10_9 Depth=1
	movq	fattrs(%rip), %rsi
	movl	$47, %edi
	callq	_IO_putc
	movq	fdefines(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB10_24
# BB#36:                                #   in Loop: Header=BB10_9 Depth=1
	movl	$47, %edi
	callq	_IO_putc
	jmp	.LBB10_24
	.p2align	4, 0x90
.LBB10_12:                              #   in Loop: Header=BB10_9 Depth=1
	cmpl	$10, %ebx
	jne	.LBB10_24
# BB#13:                                #   in Loop: Header=BB10_9 Depth=1
	incl	lineno(%rip)
	jmp	.LBB10_24
.LBB10_15:                              #   in Loop: Header=BB10_9 Depth=1
	cmpl	$123, %ebx
	je	.LBB10_37
# BB#16:                                #   in Loop: Header=BB10_9 Depth=1
	cmpl	$125, %ebx
	jne	.LBB10_24
# BB#17:                                #   in Loop: Header=BB10_9 Depth=1
	decl	%ebp
	jne	.LBB10_24
	jmp	.LBB10_18
.LBB10_23:                              #   in Loop: Header=BB10_9 Depth=1
	movq	finput(%rip), %rsi
	movl	%eax, %edi
	callq	ungetc
	.p2align	4, 0x90
.LBB10_24:                              # %.backedge
                                        #   in Loop: Header=BB10_9 Depth=1
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	jne	.LBB10_9
.LBB10_25:                              # %.loopexit26
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB10_18:
	movq	fattrs(%rip), %rcx
	movl	$.L.str.25, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movq	fdefines(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB10_20
# BB#19:
	movl	$.L.str.25, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
.LBB10_20:
	callq	skip_white_space
	cmpl	$59, %eax
	je	.LBB10_25
# BB#21:
	movq	finput(%rip), %rsi
	movl	%eax, %edi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	ungetc                  # TAILCALL
.Lfunc_end10:
	.size	parse_union_decl, .Lfunc_end10-parse_union_decl
	.cfi_endproc

	.globl	parse_expect_decl
	.p2align	4, 0x90
	.type	parse_expect_decl,@function
parse_expect_decl:                      # @parse_expect_decl
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi111:
	.cfi_def_cfa_offset 48
.Lcfi112:
	.cfi_offset %rbx, -16
	.p2align	4, 0x90
.LBB11_1:                               # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$9, %eax
	je	.LBB11_1
# BB#2:                                 # %.backedge
                                        #   in Loop: Header=BB11_1 Depth=1
	cmpl	$32, %eax
	je	.LBB11_1
# BB#3:                                 # %.preheader
	movl	%eax, %ecx
	addl	$-48, %ecx
	cmpl	$9, %ecx
	ja	.LBB11_8
# BB#4:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$19, %ebx
	jg	.LBB11_7
# BB#6:                                 #   in Loop: Header=BB11_5 Depth=1
	movslq	%ebx, %rcx
	incl	%ebx
	movb	%al, (%rsp,%rcx)
.LBB11_7:                               #   in Loop: Header=BB11_5 Depth=1
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ecx
	addl	$-48, %ecx
	cmpl	$10, %ecx
	jb	.LBB11_5
.LBB11_8:                               # %._crit_edge
	movq	finput(%rip), %rsi
	movl	%eax, %edi
	callq	ungetc
	movq	%rsp, %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, expected_conflicts(%rip)
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end11:
	.size	parse_expect_decl, .Lfunc_end11-parse_expect_decl
	.cfi_endproc

	.globl	parse_assoc_decl
	.p2align	4, 0x90
	.type	parse_assoc_decl,@function
parse_assoc_decl:                       # @parse_assoc_decl
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi116:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 48
.Lcfi118:
	.cfi_offset %rbx, -48
.Lcfi119:
	.cfi_offset %r12, -40
.Lcfi120:
	.cfi_offset %r14, -32
.Lcfi121:
	.cfi_offset %r15, -24
.Lcfi122:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	incl	lastprec(%rip)
	callq	skip_white_space
	movq	finput(%rip), %rsi
	movl	%eax, %edi
	callq	ungetc
	cmpl	$37, %eax
	je	.LBB12_6
# BB#1:                                 # %.lr.ph
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_2:                               # =>This Inner Loop Header: Depth=1
	callq	lex
	movl	%eax, %ebp
	leal	-1(%rbp), %eax
	cmpl	$21, %eax
	ja	.LBB12_17
# BB#3:                                 #   in Loop: Header=BB12_2 Depth=1
	movl	$2, %r12d
	jmpq	*.LJTI12_0(,%rax,8)
.LBB12_7:                               #   in Loop: Header=BB12_2 Depth=1
	movzwl	lastprec(%rip), %ecx
	movq	symval(%rip), %rax
	movw	%cx, 34(%rax)
	movw	%r14w, 36(%rax)
	cmpb	$2, 40(%rax)
	jne	.LBB12_9
# BB#8:                                 #   in Loop: Header=BB12_2 Depth=1
	movl	16(%rax), %esi
	subq	$8, %rsp
.Lcfi123:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.11, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
.Lcfi124:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi125:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi126:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi127:
	.cfi_adjust_cfa_offset -32
	movq	symval(%rip), %rax
.LBB12_9:                               #   in Loop: Header=BB12_2 Depth=1
	movb	$1, 40(%rax)
	movl	$1, %r12d
	testq	%r15, %r15
	je	.LBB12_10
# BB#11:                                #   in Loop: Header=BB12_2 Depth=1
	cmpq	$0, 24(%rax)
	je	.LBB12_12
# BB#13:                                #   in Loop: Header=BB12_2 Depth=1
	movl	16(%rax), %esi
	subq	$8, %rsp
.Lcfi128:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.12, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
.Lcfi129:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi130:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi131:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi132:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB12_5
.LBB12_17:                              #   in Loop: Header=BB12_2 Depth=1
	movl	$.L.str.19, %edi
	callq	fatal
	movl	%ebp, %r12d
	jmp	.LBB12_5
.LBB12_4:                               #   in Loop: Header=BB12_2 Depth=1
	movl	$token_buffer, %edi
	callq	strlen
	leal	1(%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %r15
	movl	$token_buffer, %esi
	movq	%r15, %rdi
	callq	strcpy
	movl	$21, %r12d
	jmp	.LBB12_5
.LBB12_14:                              #   in Loop: Header=BB12_2 Depth=1
	cmpl	$1, %ebx
	jne	.LBB12_16
# BB#15:                                #   in Loop: Header=BB12_2 Depth=1
	movzwl	numval(%rip), %eax
	movq	symval(%rip), %rcx
	movw	%ax, 38(%rcx)
	movl	$1, translations(%rip)
	movl	$22, %r12d
	jmp	.LBB12_5
.LBB12_16:                              #   in Loop: Header=BB12_2 Depth=1
	movl	$.L.str.18, %edi
	callq	fatal
	movl	$22, %r12d
	jmp	.LBB12_5
.LBB12_10:                              #   in Loop: Header=BB12_2 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB12_5
.LBB12_12:                              #   in Loop: Header=BB12_2 Depth=1
	movq	%r15, 24(%rax)
	.p2align	4, 0x90
.LBB12_5:                               # %.backedge
                                        #   in Loop: Header=BB12_2 Depth=1
	callq	skip_white_space
	movq	finput(%rip), %rsi
	movl	%eax, %edi
	callq	ungetc
	cmpl	$37, %eax
	movl	%r12d, %ebx
	jne	.LBB12_2
.LBB12_6:                               # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	parse_assoc_decl, .Lfunc_end12-parse_assoc_decl
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI12_0:
	.quad	.LBB12_7
	.quad	.LBB12_5
	.quad	.LBB12_17
	.quad	.LBB12_6
	.quad	.LBB12_17
	.quad	.LBB12_17
	.quad	.LBB12_17
	.quad	.LBB12_17
	.quad	.LBB12_17
	.quad	.LBB12_17
	.quad	.LBB12_17
	.quad	.LBB12_17
	.quad	.LBB12_17
	.quad	.LBB12_17
	.quad	.LBB12_17
	.quad	.LBB12_17
	.quad	.LBB12_17
	.quad	.LBB12_17
	.quad	.LBB12_17
	.quad	.LBB12_17
	.quad	.LBB12_4
	.quad	.LBB12_14

	.text
	.globl	get_type_name
	.p2align	4, 0x90
	.type	get_type_name,@function
get_type_name:                          # @get_type_name
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi135:
	.cfi_def_cfa_offset 32
.Lcfi136:
	.cfi_offset %rbx, -24
.Lcfi137:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	testl	%ebp, %ebp
	js	.LBB13_1
# BB#2:                                 # %.preheader
	je	.LBB13_7
	.p2align	4, 0x90
.LBB13_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB13_5
# BB#4:                                 #   in Loop: Header=BB13_3 Depth=1
	cmpq	$0, 8(%rbx)
	jne	.LBB13_6
.LBB13_5:                               #   in Loop: Header=BB13_3 Depth=1
	movl	$.L.str.34, %edi
	callq	fatal
.LBB13_6:                               #   in Loop: Header=BB13_3 Depth=1
	decl	%ebp
	jne	.LBB13_3
	jmp	.LBB13_7
.LBB13_1:                               # %.preheader.thread
	movl	$.L.str.34, %edi
	callq	fatal
.LBB13_7:                               # %._crit_edge
	movq	8(%rbx), %rax
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end13:
	.size	get_type_name, .Lfunc_end13-get_type_name
	.cfi_endproc

	.globl	copy_guard
	.p2align	4, 0x90
	.type	copy_guard,@function
copy_guard:                             # @copy_guard
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi138:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi139:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi140:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi141:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi142:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi144:
	.cfi_def_cfa_offset 80
.Lcfi145:
	.cfi_offset %rbx, -56
.Lcfi146:
	.cfi_offset %r12, -48
.Lcfi147:
	.cfi_offset %r13, -40
.Lcfi148:
	.cfi_offset %r14, -32
.Lcfi149:
	.cfi_offset %r15, -24
.Lcfi150:
	.cfi_offset %rbp, -16
	xorl	%eax, %eax
	cmpl	$0, semantic_parser(%rip)
	movl	%esi, %r13d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	cmovnel	%eax, %r13d
	movq	fguard(%rip), %rdi
	movl	nrules(%rip), %edx
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	callq	fprintf
	cmpl	$0, nolinesflag(%rip)
	jne	.LBB14_2
# BB#1:
	movq	fguard(%rip), %r14
	movl	lineno(%rip), %ebp
	movq	infile(%rip), %rbx
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rcx
	cmoveq	%rbx, %rcx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	fprintf
.LBB14_2:
	movq	fguard(%rip), %rsi
	movl	$123, %edi
	callq	_IO_putc
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
                                        # implicit-def: %R14D
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	movl	%r13d, 4(%rsp)          # 4-byte Spill
	testl	%r15d, %r15d
	jne	.LBB14_12
	jmp	.LBB14_24
	.p2align	4, 0x90
.LBB14_10:                              # %.thread134
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movl	%ebx, %r12d
.LBB14_11:                              # %.outer141
	testl	%r15d, %r15d
	je	.LBB14_24
.LBB14_12:                              # %.outer141.split.us
	testl	%r12d, %r12d
	jle	.LBB14_97
	.p2align	4, 0x90
.LBB14_13:                              # %.outer141.split.us.split
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rbp), %eax
	cmpl	$65, %eax
	ja	.LBB14_16
# BB#14:                                # %.outer141.split.us.split
                                        #   in Loop: Header=BB14_13 Depth=1
	jmpq	*.LJTI14_0(,%rax,8)
.LBB14_15:                              #   in Loop: Header=BB14_13 Depth=1
	movq	fguard(%rip), %rsi
	movl	$47, %edi
	callq	_IO_putc
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$42, %ebp
	jne	.LBB14_13
	jmp	.LBB14_27
.LBB14_3:                               # %get_type_name.exit
	movq	8(%r13), %rax
	movq	24(%rax), %rbx
.LBB14_4:
	movq	fguard(%rip), %rdi
	movl	%r14d, %edx
	movl	4(%rsp), %r13d          # 4-byte Reload
	subl	%r13d, %edx
	movl	$.L.str.40, %esi
	xorl	%eax, %eax
	callq	fprintf
	testq	%rbx, %rbx
	je	.LBB14_6
# BB#5:                                 # %.thread131
	movq	fguard(%rip), %rdi
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	testl	%r15d, %r15d
	jne	.LBB14_12
	jmp	.LBB14_24
.LBB14_6:
	cmpb	$1, typed(%rip)
	jne	.LBB14_11
# BB#7:
	movq	stderr(%rip), %rdi
	movq	infile(%rip), %rdx
	movl	lineno(%rip), %ecx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %rax
	movq	16(%rax), %r9
	movl	$.L.str.41, %esi
	xorl	%eax, %eax
	movl	%r14d, %r8d
	callq	fprintf
	testl	%r15d, %r15d
	jne	.LBB14_12
	.p2align	4, 0x90
.LBB14_24:                              # %.outer141.split
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rbp), %eax
	cmpl	$65, %eax
	ja	.LBB14_16
# BB#25:                                # %.outer141.split
                                        #   in Loop: Header=BB14_24 Depth=1
	jmpq	*.LJTI14_1(,%rax,8)
.LBB14_26:                              #   in Loop: Header=BB14_24 Depth=1
	movq	fguard(%rip), %rsi
	movl	$47, %edi
	callq	_IO_putc
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$42, %ebp
	jne	.LBB14_24
.LBB14_27:                              # %.us-lcssa191.us
	movq	fguard(%rip), %rsi
	movl	$42, %edi
	jmp	.LBB14_64
.LBB14_16:                              # %.outer141.split.us.split
	cmpl	$123, %ebp
	je	.LBB14_20
# BB#17:                                # %.outer141.split.us.split
	cmpl	$125, %ebp
	jne	.LBB14_41
# BB#18:                                # %.us-lcssa186.us
	movq	fguard(%rip), %rsi
	movl	$125, %edi
	callq	_IO_putc
	testl	%r12d, %r12d
	jle	.LBB14_21
# BB#19:
	decl	%r12d
	jmp	.LBB14_22
.LBB14_28:                              # %.us-lcssa187.us
	movq	fguard(%rip), %rsi
	movl	%ebp, %edi
	jmp	.LBB14_30
	.p2align	4, 0x90
.LBB14_29:                              # %.thread
                                        #   in Loop: Header=BB14_30 Depth=1
	movl	$.L.str.8, %edi
	callq	fatal
	movq	fguard(%rip), %rsi
	movl	%ebx, %edi
.LBB14_30:                              # %.us-lcssa187.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_32 Depth 2
	callq	_IO_putc
	jmp	.LBB14_32
.LBB14_31:                              #   in Loop: Header=BB14_32 Depth=2
	incl	lineno(%rip)
	.p2align	4, 0x90
.LBB14_32:                              # %.us-lcssa187.us
                                        #   Parent Loop BB14_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	%ebp, %ebx
	je	.LBB14_41
# BB#33:                                # %.lr.ph203
                                        #   in Loop: Header=BB14_32 Depth=2
	cmpl	$10, %ebx
	je	.LBB14_29
# BB#34:                                # %.lr.ph203
                                        #   in Loop: Header=BB14_32 Depth=2
	cmpl	$-1, %ebx
	je	.LBB14_29
# BB#35:                                #   in Loop: Header=BB14_32 Depth=2
	movq	fguard(%rip), %rsi
	movl	%ebx, %edi
	callq	_IO_putc
	cmpl	$92, %ebx
	jne	.LBB14_32
# BB#36:                                #   in Loop: Header=BB14_32 Depth=2
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$10, %ebx
	je	.LBB14_38
# BB#37:                                #   in Loop: Header=BB14_32 Depth=2
	cmpl	$-1, %ebx
	jne	.LBB14_39
.LBB14_38:                              #   in Loop: Header=BB14_32 Depth=2
	movl	$.L.str.8, %edi
	callq	fatal
.LBB14_39:                              #   in Loop: Header=BB14_32 Depth=2
	movq	fguard(%rip), %rsi
	movl	%ebx, %edi
	callq	_IO_putc
	cmpl	$10, %ebx
	jne	.LBB14_32
	jmp	.LBB14_31
.LBB14_40:                              # %.us-lcssa190.us
	movl	$.L.str.45, %edi
	callq	fatal
	movl	$-1, %ebp
.LBB14_41:                              # %._crit_edge204
	movq	fguard(%rip), %rsi
	movl	%ebp, %edi
	callq	_IO_putc
.LBB14_23:                              # %.loopexit138
	movl	%r12d, %ebx
	cmpl	$125, %ebp
	jne	.LBB14_10
.LBB14_42:                              # %.loopexit138
	xorl	%r12d, %r12d
	movl	$125, %ebp
	testl	%ebx, %ebx
	je	.LBB14_11
	jmp	.LBB14_10
.LBB14_43:                              # %.us-lcssa184.us
	movq	fguard(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	incl	lineno(%rip)
	jmp	.LBB14_9
.LBB14_44:                              # %.us-lcssa188.us
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$60, %ebp
	jne	.LBB14_50
# BB#45:                                # %.preheader139
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$62, %eax
	je	.LBB14_74
# BB#46:                                # %.preheader139
	testl	%eax, %eax
	movl	$token_buffer, %ebx
	jle	.LBB14_75
# BB#47:                                # %.lr.ph.preheader
	movl	$token_buffer, %ebx
	.p2align	4, 0x90
.LBB14_48:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movb	%al, (%rbx)
	incq	%rbx
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$62, %eax
	je	.LBB14_75
# BB#49:                                # %.lr.ph
                                        #   in Loop: Header=BB14_48 Depth=1
	testl	%eax, %eax
	jg	.LBB14_48
	jmp	.LBB14_75
.LBB14_20:                              # %.us-lcssa185.us
	movq	fguard(%rip), %rsi
	movl	$123, %edi
	callq	_IO_putc
	incl	%r12d
	jmp	.LBB14_9
.LBB14_50:
	xorl	%ebx, %ebx
	cmpl	$36, %ebp
	je	.LBB14_76
	jmp	.LBB14_79
.LBB14_51:                              # %.us-lcssa189.us.loopexit
	xorl	%r15d, %r15d
.LBB14_52:                              # %.us-lcssa189.us
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	callq	__ctype_b_loc
	movq	%rax, %rbx
	cmpl	$45, %ebp
	je	.LBB14_55
# BB#53:                                # %.us-lcssa189.us
	movq	(%rbx), %rax
	movslq	%ebp, %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$2048, %eax             # imm = 0x800
	testw	%ax, %ax
	jne	.LBB14_55
# BB#54:
	subq	$8, %rsp
.Lcfi151:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.43, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%ebp, %esi
	pushq	$0
.Lcfi152:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi153:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi154:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi155:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB14_62
.LBB14_55:
	movq	finput(%rip), %rsi
	movl	%ebp, %edi
	callq	ungetc
	movq	finput(%rip), %rbp
	movq	%rbp, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$45, %eax
	jne	.LBB14_57
# BB#56:
	movq	%rbp, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	$-1, %r13d
	jmp	.LBB14_58
.LBB14_21:
	movl	$.L.str.36, %edi
	callq	fatal
.LBB14_22:                              # %.loopexit138
	movl	$1, %r15d
	movl	$125, %ebp
	jmp	.LBB14_23
.LBB14_57:
	movl	$1, %r13d
.LBB14_58:
	movq	(%rbx), %rcx
	movslq	%eax, %rdx
	xorl	%r14d, %r14d
	testb	$8, 1(%rcx,%rdx,2)
	je	.LBB14_61
# BB#59:                                # %.lr.ph.i125.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB14_60:                              # %.lr.ph.i125
                                        # =>This Inner Loop Header: Depth=1
	leal	(%r14,%r14,4), %ecx
	leal	-48(%rax,%rcx,2), %r14d
	movq	%rbp, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	(%rbx), %rcx
	movslq	%eax, %rdx
	testb	$8, 1(%rcx,%rdx,2)
	jne	.LBB14_60
.LBB14_61:                              # %read_signed_integer.exit128
	movl	%eax, %edi
	movq	%rbp, %rsi
	callq	ungetc
	imull	%r13d, %r14d
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movl	4(%rsp), %r13d          # 4-byte Reload
.LBB14_62:
	movq	fguard(%rip), %rdi
	movl	%r14d, %edx
	subl	%r13d, %edx
	movl	$.L.str.44, %esi
	xorl	%eax, %eax
	callq	fprintf
	movb	$1, yylsp_needed(%rip)
	testl	%r15d, %r15d
	jne	.LBB14_12
	jmp	.LBB14_24
.LBB14_63:                              # %.us-lcssa197.us
                                        #   in Loop: Header=BB14_64 Depth=1
	movq	fguard(%rip), %rsi
	movl	%eax, %edi
	.p2align	4, 0x90
.LBB14_64:                              # %.lr.ph194.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_67 Depth 2
                                        #       Child Loop BB14_68 Depth 3
                                        #     Child Loop BB14_70 Depth 2
	callq	_IO_putc
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$42, %eax
	je	.LBB14_67
	jmp	.LBB14_70
.LBB14_65:                              # %.us-lcssa199.us
                                        #   in Loop: Header=BB14_70 Depth=2
	movl	$.L.str.24, %edi
	callq	fatal
	movl	$-1, %eax
	cmpl	$42, %eax
	je	.LBB14_67
.LBB14_70:                              # %.lr.ph194.split.us
                                        #   Parent Loop BB14_64 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %eax
	jne	.LBB14_71
	jmp	.LBB14_65
	.p2align	4, 0x90
.LBB14_66:                              # %.lr.ph194.split.us
                                        #   in Loop: Header=BB14_67 Depth=2
	cmpl	$42, %eax
	jne	.LBB14_70
.LBB14_67:                              # %.preheader.us.preheader
                                        #   Parent Loop BB14_64 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_68 Depth 3
	movl	$42, %eax
	cmpl	$42, %eax
	jne	.LBB14_69
	.p2align	4, 0x90
.LBB14_68:                              #   Parent Loop BB14_64 Depth=1
                                        #     Parent Loop BB14_67 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	fguard(%rip), %rsi
	movl	$42, %edi
	callq	_IO_putc
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$42, %eax
	je	.LBB14_68
.LBB14_69:                              # %.preheader.us
                                        #   in Loop: Header=BB14_67 Depth=2
	cmpl	$47, %eax
	jne	.LBB14_66
	jmp	.LBB14_73
.LBB14_71:                              # %.lr.ph194.split.us
                                        #   in Loop: Header=BB14_64 Depth=1
	cmpl	$10, %eax
	jne	.LBB14_63
# BB#72:                                # %.us-lcssa198.us
                                        #   in Loop: Header=BB14_64 Depth=1
	incl	lineno(%rip)
	movq	fguard(%rip), %rsi
	movl	$10, %edi
	jmp	.LBB14_64
	.p2align	4, 0x90
.LBB14_73:                              # %.loopexit138.loopexit207
	movq	fguard(%rip), %rsi
	movl	$47, %edi
	callq	_IO_putc
.LBB14_9:                               # %.thread134
	movl	%r12d, %ebx
	jmp	.LBB14_10
.LBB14_74:
	movl	$token_buffer, %ebx
.LBB14_75:                              # %._crit_edge
	movb	$0, (%rbx)
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movl	$token_buffer, %ebx
	cmpl	$36, %ebp
	jne	.LBB14_79
.LBB14_76:
	movq	fguard(%rip), %rcx
	movl	$.L.str.37, %edi
	movl	$5, %esi
	movl	$1, %edx
	callq	fwrite
	testq	%rbx, %rbx
	jne	.LBB14_78
# BB#77:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %rax
	movq	24(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB14_82
.LBB14_78:                              # %.thread262
	movq	fguard(%rip), %rdi
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	jmp	.LBB14_9
.LBB14_79:
	callq	__ctype_b_loc
	movq	%rax, %r13
	cmpl	$45, %ebp
	je	.LBB14_84
# BB#80:
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$2048, %eax             # imm = 0x800
	testw	%ax, %ax
	jne	.LBB14_84
# BB#81:
	subq	$8, %rsp
.Lcfi156:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.42, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%ebp, %esi
	pushq	$0
.Lcfi157:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi158:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi159:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi160:
	.cfi_adjust_cfa_offset -32
	movl	%r12d, %ebx
	movl	4(%rsp), %r13d          # 4-byte Reload
	cmpl	$125, %ebp
	je	.LBB14_42
	jmp	.LBB14_10
.LBB14_82:
	cmpb	$1, typed(%rip)
	jne	.LBB14_9
# BB#8:
	movq	stderr(%rip), %rdi
	movq	infile(%rip), %rdx
	movl	lineno(%rip), %ecx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %rax
	movq	16(%rax), %r8
	movl	$.L.str.39, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB14_9
.LBB14_84:
	movq	finput(%rip), %rsi
	movl	%ebp, %edi
	callq	ungetc
	movq	finput(%rip), %rbp
	movq	%rbp, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$45, %eax
	jne	.LBB14_86
# BB#85:
	movq	%rbp, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	$-1, 20(%rsp)           # 4-byte Folded Spill
	jmp	.LBB14_87
.LBB14_86:
	movl	$1, 20(%rsp)            # 4-byte Folded Spill
.LBB14_87:
	movq	(%r13), %rcx
	movslq	%eax, %rdx
	xorl	%r14d, %r14d
	testb	$8, 1(%rcx,%rdx,2)
	je	.LBB14_90
# BB#88:                                # %.lr.ph.i.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB14_89:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leal	(%r14,%r14,4), %ecx
	leal	-48(%rax,%rcx,2), %r14d
	movq	%rbp, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	(%r13), %rcx
	movslq	%eax, %rdx
	testb	$8, 1(%rcx,%rdx,2)
	jne	.LBB14_89
.LBB14_90:                              # %read_signed_integer.exit
	movl	%eax, %edi
	movq	%rbp, %rsi
	callq	ungetc
	imull	20(%rsp), %r14d         # 4-byte Folded Reload
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	testq	%rbx, %rbx
	jne	.LBB14_4
# BB#91:                                # %read_signed_integer.exit
	testl	%r14d, %r14d
	jle	.LBB14_4
# BB#92:                                # %.lr.ph.i119.preheader
	movl	%r14d, %ebx
	movq	8(%rsp), %r13           # 8-byte Reload
	.p2align	4, 0x90
.LBB14_93:                              # %.lr.ph.i119
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %r13
	testq	%r13, %r13
	je	.LBB14_95
# BB#94:                                #   in Loop: Header=BB14_93 Depth=1
	cmpq	$0, 8(%r13)
	jne	.LBB14_96
.LBB14_95:                              #   in Loop: Header=BB14_93 Depth=1
	movl	$.L.str.34, %edi
	callq	fatal
.LBB14_96:                              #   in Loop: Header=BB14_93 Depth=1
	decl	%ebx
	jne	.LBB14_93
	jmp	.LBB14_3
.LBB14_97:                              # %.us-lcssa.us.split
	callq	skip_white_space
	movl	%eax, %ebx
	movq	fguard(%rip), %rcx
	movl	$.L.str.46, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	cmpl	$61, %ebx
	je	.LBB14_100
# BB#98:                                # %.us-lcssa.us.split
	cmpl	$123, %ebx
	je	.LBB14_101
# BB#99:
	movq	finput(%rip), %rsi
	movl	%ebx, %edi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	ungetc                  # TAILCALL
.LBB14_100:
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$123, %eax
	jne	.LBB14_102
.LBB14_101:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%r13d, %esi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	copy_action             # TAILCALL
.LBB14_102:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	copy_guard, .Lfunc_end14-copy_guard
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI14_0:
	.quad	.LBB14_40
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_43
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_28
	.quad	.LBB14_41
	.quad	.LBB14_44
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_28
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_15
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_52
.LJTI14_1:
	.quad	.LBB14_40
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_43
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_28
	.quad	.LBB14_41
	.quad	.LBB14_44
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_28
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_26
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_97
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_41
	.quad	.LBB14_51

	.text
	.globl	read_signed_integer
	.p2align	4, 0x90
	.type	read_signed_integer,@function
read_signed_integer:                    # @read_signed_integer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi163:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi164:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 48
.Lcfi166:
	.cfi_offset %rbx, -48
.Lcfi167:
	.cfi_offset %r12, -40
.Lcfi168:
	.cfi_offset %r14, -32
.Lcfi169:
	.cfi_offset %r15, -24
.Lcfi170:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	_IO_getc
	movl	%eax, %r15d
	cmpl	$45, %r15d
	jne	.LBB15_1
# BB#2:
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %r15d
	movl	$-1, %ebp
	jmp	.LBB15_3
.LBB15_1:
	movl	$1, %ebp
.LBB15_3:
	callq	__ctype_b_loc
	movq	%rax, %r12
	movq	(%r12), %rax
	movslq	%r15d, %rcx
	xorl	%ebx, %ebx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB15_6
# BB#4:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB15_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbx,%rbx,4), %eax
	leal	-48(%r15,%rax,2), %ebx
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %r15d
	movq	(%r12), %rax
	movslq	%r15d, %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB15_5
.LBB15_6:                               # %._crit_edge
	movl	%r15d, %edi
	movq	%r14, %rsi
	callq	ungetc
	imull	%ebp, %ebx
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	read_signed_integer, .Lfunc_end15-read_signed_integer
	.cfi_endproc

	.globl	copy_action
	.p2align	4, 0x90
	.type	copy_action,@function
copy_action:                            # @copy_action
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi171:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi172:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi173:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi174:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi175:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi176:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi177:
	.cfi_def_cfa_offset 80
.Lcfi178:
	.cfi_offset %rbx, -56
.Lcfi179:
	.cfi_offset %r12, -48
.Lcfi180:
	.cfi_offset %r13, -40
.Lcfi181:
	.cfi_offset %r14, -32
.Lcfi182:
	.cfi_offset %r15, -24
.Lcfi183:
	.cfi_offset %rbp, -16
	xorl	%eax, %eax
	cmpl	$0, semantic_parser(%rip)
	movl	%esi, %r15d
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	cmovnel	%eax, %r15d
	movq	faction(%rip), %rdi
	movl	nrules(%rip), %edx
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	callq	fprintf
	cmpl	$0, nolinesflag(%rip)
	jne	.LBB16_2
# BB#1:
	movq	faction(%rip), %r12
	movl	lineno(%rip), %ebp
	movq	infile(%rip), %rbx
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rcx
	cmoveq	%rbx, %rcx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%ebp, %edx
	callq	fprintf
.LBB16_2:
	movq	faction(%rip), %rsi
	movl	$123, %edi
	callq	_IO_putc
	movl	$1, %r14d
                                        # implicit-def: %R13D
	jmp	.LBB16_3
	.p2align	4, 0x90
.LBB16_10:                              #   in Loop: Header=BB16_3 Depth=1
	movq	faction(%rip), %rsi
	movl	$125, %edi
	callq	_IO_putc
.LBB16_3:                               # %.sink.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_4 Depth 2
                                        #       Child Loop BB16_6 Depth 3
                                        #         Child Loop BB16_83 Depth 4
                                        #         Child Loop BB16_26 Depth 4
                                        #           Child Loop BB16_36 Depth 5
                                        #         Child Loop BB16_45 Depth 4
                                        #         Child Loop BB16_61 Depth 4
                                        #         Child Loop BB16_65 Depth 4
                                        #         Child Loop BB16_14 Depth 4
                                        #           Child Loop BB16_15 Depth 5
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movl	%r14d, %eax
	.p2align	4, 0x90
.LBB16_4:                               #   Parent Loop BB16_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB16_6 Depth 3
                                        #         Child Loop BB16_83 Depth 4
                                        #         Child Loop BB16_26 Depth 4
                                        #           Child Loop BB16_36 Depth 5
                                        #         Child Loop BB16_45 Depth 4
                                        #         Child Loop BB16_61 Depth 4
                                        #         Child Loop BB16_65 Depth 4
                                        #         Child Loop BB16_14 Depth 4
                                        #           Child Loop BB16_15 Depth 5
	testl	%eax, %eax
	jle	.LBB16_89
# BB#5:                                 # %.outer118.preheader
                                        #   in Loop: Header=BB16_4 Depth=2
	movl	%eax, %r14d
	jmp	.LBB16_6
.LBB16_69:                              # %get_type_name.exit
                                        #   in Loop: Header=BB16_6 Depth=3
	movq	8(%r12), %rax
	movq	24(%rax), %r12
.LBB16_70:                              #   in Loop: Header=BB16_6 Depth=3
	movq	faction(%rip), %rdi
	movl	%r13d, %edx
	subl	%r15d, %edx
	movl	$.L.str.40, %esi
	xorl	%eax, %eax
	callq	fprintf
	testq	%r12, %r12
	je	.LBB16_72
# BB#71:                                # %.thread112
                                        #   in Loop: Header=BB16_6 Depth=3
	movq	faction(%rip), %rdi
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	fprintf
	jmp	.LBB16_6
.LBB16_72:                              #   in Loop: Header=BB16_6 Depth=3
	cmpb	$1, typed(%rip)
	jne	.LBB16_6
# BB#73:                                #   in Loop: Header=BB16_6 Depth=3
	movq	stderr(%rip), %rdi
	movq	infile(%rip), %rdx
	movl	lineno(%rip), %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	movq	16(%rax), %r9
	movl	$.L.str.41, %esi
	xorl	%eax, %eax
	movl	%r13d, %r8d
	callq	fprintf
	jmp	.LBB16_6
.LBB16_75:                              # %.loopexit115
                                        #   in Loop: Header=BB16_6 Depth=3
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	.p2align	4, 0x90
.LBB16_6:                               #   Parent Loop BB16_3 Depth=1
                                        #     Parent Loop BB16_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB16_83 Depth 4
                                        #         Child Loop BB16_26 Depth 4
                                        #           Child Loop BB16_36 Depth 5
                                        #         Child Loop BB16_45 Depth 4
                                        #         Child Loop BB16_61 Depth 4
                                        #         Child Loop BB16_65 Depth 4
                                        #         Child Loop BB16_14 Depth 4
                                        #           Child Loop BB16_15 Depth 5
	leal	1(%rbp), %eax
	cmpl	$65, %eax
	ja	.LBB16_7
# BB#90:                                #   in Loop: Header=BB16_6 Depth=3
	jmpq	*.LJTI16_0(,%rax,8)
.LBB16_24:                              #   in Loop: Header=BB16_6 Depth=3
	movq	faction(%rip), %rsi
	movl	$47, %edi
	callq	_IO_putc
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$42, %ebp
	jne	.LBB16_6
# BB#25:                                #   in Loop: Header=BB16_6 Depth=3
	movq	faction(%rip), %rsi
	movl	$42, %edi
	jmp	.LBB16_26
.LBB16_7:                               #   in Loop: Header=BB16_6 Depth=3
	cmpl	$123, %ebp
	je	.LBB16_12
# BB#8:                                 #   in Loop: Header=BB16_6 Depth=3
	cmpl	$125, %ebp
	je	.LBB16_9
	jmp	.LBB16_88
.LBB16_13:                              #   in Loop: Header=BB16_6 Depth=3
	movq	faction(%rip), %rsi
	movl	%ebp, %edi
	jmp	.LBB16_14
	.p2align	4, 0x90
.LBB16_18:                              # %.thread
                                        #   in Loop: Header=BB16_14 Depth=4
	movl	$.L.str.8, %edi
	callq	fatal
	movq	faction(%rip), %rsi
	movl	%ebx, %edi
.LBB16_14:                              #   Parent Loop BB16_3 Depth=1
                                        #     Parent Loop BB16_4 Depth=2
                                        #       Parent Loop BB16_6 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB16_15 Depth 5
	callq	_IO_putc
	jmp	.LBB16_15
.LBB16_23:                              #   in Loop: Header=BB16_15 Depth=5
	incl	lineno(%rip)
	.p2align	4, 0x90
.LBB16_15:                              #   Parent Loop BB16_3 Depth=1
                                        #     Parent Loop BB16_4 Depth=2
                                        #       Parent Loop BB16_6 Depth=3
                                        #         Parent Loop BB16_14 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	%ebp, %ebx
	je	.LBB16_88
# BB#16:                                # %.lr.ph156
                                        #   in Loop: Header=BB16_15 Depth=5
	cmpl	$10, %ebx
	je	.LBB16_18
# BB#17:                                # %.lr.ph156
                                        #   in Loop: Header=BB16_15 Depth=5
	cmpl	$-1, %ebx
	je	.LBB16_18
# BB#19:                                #   in Loop: Header=BB16_15 Depth=5
	movq	faction(%rip), %rsi
	movl	%ebx, %edi
	callq	_IO_putc
	cmpl	$92, %ebx
	jne	.LBB16_15
# BB#20:                                #   in Loop: Header=BB16_15 Depth=5
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB16_21
# BB#22:                                #   in Loop: Header=BB16_15 Depth=5
	movq	faction(%rip), %rsi
	movl	%ebx, %edi
	callq	_IO_putc
	cmpl	$10, %ebx
	jne	.LBB16_15
	jmp	.LBB16_23
.LBB16_21:                              # %.thread109
                                        #   in Loop: Header=BB16_14 Depth=4
	movl	$.L.str.8, %edi
	callq	fatal
	movq	faction(%rip), %rsi
	movl	$-1, %edi
	jmp	.LBB16_14
.LBB16_87:                              #   in Loop: Header=BB16_6 Depth=3
	movl	$.L.str.48, %edi
	callq	fatal
	movl	$-1, %ebp
.LBB16_88:                              # %._crit_edge157
                                        #   in Loop: Header=BB16_6 Depth=3
	movq	faction(%rip), %rsi
	movl	%ebp, %edi
	callq	_IO_putc
	jmp	.LBB16_75
.LBB16_11:                              #   in Loop: Header=BB16_6 Depth=3
	movq	faction(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	incl	lineno(%rip)
	jmp	.LBB16_75
.LBB16_39:                              #   in Loop: Header=BB16_6 Depth=3
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$60, %ebx
	jne	.LBB16_40
# BB#41:                                # %.preheader116
                                        #   in Loop: Header=BB16_6 Depth=3
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$62, %eax
	je	.LBB16_42
# BB#43:                                # %.preheader116
                                        #   in Loop: Header=BB16_6 Depth=3
	testl	%eax, %eax
	movl	$token_buffer, %ebx
	jle	.LBB16_47
# BB#44:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB16_6 Depth=3
	movl	$token_buffer, %ebx
	.p2align	4, 0x90
.LBB16_45:                              # %.lr.ph
                                        #   Parent Loop BB16_3 Depth=1
                                        #     Parent Loop BB16_4 Depth=2
                                        #       Parent Loop BB16_6 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movb	%al, (%rbx)
	incq	%rbx
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$62, %eax
	je	.LBB16_47
# BB#46:                                # %.lr.ph
                                        #   in Loop: Header=BB16_45 Depth=4
	testl	%eax, %eax
	jg	.LBB16_45
	jmp	.LBB16_47
.LBB16_76:                              #   in Loop: Header=BB16_6 Depth=3
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	callq	__ctype_b_loc
	movq	%rax, %rbx
	cmpl	$45, %ebp
	je	.LBB16_78
# BB#77:                                #   in Loop: Header=BB16_6 Depth=3
	movq	(%rbx), %rax
	movslq	%ebp, %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$2048, %eax             # imm = 0x800
	testw	%ax, %ax
	jne	.LBB16_78
# BB#85:                                #   in Loop: Header=BB16_6 Depth=3
	movl	$.L.str.47, %edi
	callq	fatal
	jmp	.LBB16_86
.LBB16_12:                              #   in Loop: Header=BB16_6 Depth=3
	movq	faction(%rip), %rsi
	movl	$123, %edi
	callq	_IO_putc
	incl	%r14d
	jmp	.LBB16_75
.LBB16_40:                              #   in Loop: Header=BB16_6 Depth=3
	xorl	%r12d, %r12d
	cmpl	$36, %ebx
	je	.LBB16_49
	jmp	.LBB16_54
.LBB16_78:                              #   in Loop: Header=BB16_6 Depth=3
	movq	finput(%rip), %rsi
	movl	%ebp, %edi
	callq	ungetc
	movq	finput(%rip), %rbp
	movq	%rbp, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$45, %eax
	jne	.LBB16_79
# BB#80:                                #   in Loop: Header=BB16_6 Depth=3
	movq	%rbp, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	$-1, %r12d
	jmp	.LBB16_81
.LBB16_79:                              #   in Loop: Header=BB16_6 Depth=3
	movl	$1, %r12d
.LBB16_81:                              #   in Loop: Header=BB16_6 Depth=3
	movq	(%rbx), %rcx
	movslq	%eax, %rdx
	xorl	%r13d, %r13d
	testb	$8, 1(%rcx,%rdx,2)
	je	.LBB16_84
# BB#82:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB16_6 Depth=3
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB16_83:                              # %.lr.ph.i
                                        #   Parent Loop BB16_3 Depth=1
                                        #     Parent Loop BB16_4 Depth=2
                                        #       Parent Loop BB16_6 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%r13,%r13,4), %ecx
	leal	-48(%rax,%rcx,2), %r13d
	movq	%rbp, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	(%rbx), %rcx
	movslq	%eax, %rdx
	testb	$8, 1(%rcx,%rdx,2)
	jne	.LBB16_83
.LBB16_84:                              # %read_signed_integer.exit
                                        #   in Loop: Header=BB16_6 Depth=3
	movl	%eax, %edi
	movq	%rbp, %rsi
	callq	ungetc
	imull	%r12d, %r13d
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
.LBB16_86:                              #   in Loop: Header=BB16_6 Depth=3
	movq	faction(%rip), %rdi
	movl	%r13d, %edx
	subl	%r15d, %edx
	movl	$.L.str.44, %esi
	xorl	%eax, %eax
	callq	fprintf
	movb	$1, yylsp_needed(%rip)
	jmp	.LBB16_6
.LBB16_38:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB16_26 Depth=4
	movq	faction(%rip), %rsi
	movl	%eax, %edi
	.p2align	4, 0x90
.LBB16_26:                              # %.lr.ph150.split.us
                                        #   Parent Loop BB16_3 Depth=1
                                        #     Parent Loop BB16_4 Depth=2
                                        #       Parent Loop BB16_6 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB16_36 Depth 5
	callq	_IO_putc
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$42, %eax
	je	.LBB16_31
	jmp	.LBB16_28
.LBB16_37:                              # %.us-lcssa152.us
                                        #   in Loop: Header=BB16_26 Depth=4
	movl	$.L.str.24, %edi
	callq	fatal
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB16_27:                              # %.lr.ph150.split.us
                                        #   in Loop: Header=BB16_26 Depth=4
	cmpl	$42, %eax
	jne	.LBB16_28
.LBB16_31:                              # %.preheader.us.preheader
                                        #   in Loop: Header=BB16_26 Depth=4
	movl	$42, %eax
	cmpl	$42, %eax
	jne	.LBB16_33
	.p2align	4, 0x90
.LBB16_36:                              #   Parent Loop BB16_3 Depth=1
                                        #     Parent Loop BB16_4 Depth=2
                                        #       Parent Loop BB16_6 Depth=3
                                        #         Parent Loop BB16_26 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	faction(%rip), %rsi
	movl	$42, %edi
	callq	_IO_putc
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$42, %eax
	je	.LBB16_36
.LBB16_33:                              # %.preheader.us
                                        #   in Loop: Header=BB16_26 Depth=4
	cmpl	$47, %eax
	jne	.LBB16_27
	jmp	.LBB16_34
.LBB16_28:                              # %.lr.ph150.split.us
                                        #   in Loop: Header=BB16_26 Depth=4
	cmpl	$-1, %eax
	je	.LBB16_37
# BB#29:                                # %.lr.ph150.split.us
                                        #   in Loop: Header=BB16_26 Depth=4
	cmpl	$10, %eax
	jne	.LBB16_38
# BB#30:                                # %.us-lcssa151.us
                                        #   in Loop: Header=BB16_26 Depth=4
	incl	lineno(%rip)
	movq	faction(%rip), %rsi
	movl	$10, %edi
	jmp	.LBB16_26
.LBB16_34:                              # %.loopexit115.loopexit160
                                        #   in Loop: Header=BB16_6 Depth=3
	movq	faction(%rip), %rsi
	movl	$47, %edi
	callq	_IO_putc
	jmp	.LBB16_75
.LBB16_42:                              #   in Loop: Header=BB16_6 Depth=3
	movl	$token_buffer, %ebx
.LBB16_47:                              # %._crit_edge
                                        #   in Loop: Header=BB16_6 Depth=3
	movb	$0, (%rbx)
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movl	$token_buffer, %r12d
	cmpl	$36, %ebx
	jne	.LBB16_54
.LBB16_49:                              #   in Loop: Header=BB16_6 Depth=3
	movq	faction(%rip), %rcx
	movl	$.L.str.37, %edi
	movl	$5, %esi
	movl	$1, %edx
	callq	fwrite
	testq	%r12, %r12
	jne	.LBB16_51
# BB#50:                                #   in Loop: Header=BB16_6 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	movq	24(%rax), %r12
	testq	%r12, %r12
	je	.LBB16_52
.LBB16_51:                              # %.thread187
                                        #   in Loop: Header=BB16_6 Depth=3
	movq	faction(%rip), %rdi
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	fprintf
	jmp	.LBB16_75
.LBB16_54:                              #   in Loop: Header=BB16_6 Depth=3
	callq	__ctype_b_loc
	movq	%rax, %rbp
	cmpl	$45, %ebx
	je	.LBB16_56
# BB#55:                                #   in Loop: Header=BB16_6 Depth=3
	movq	(%rbp), %rax
	movslq	%ebx, %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$2048, %eax             # imm = 0x800
	testw	%ax, %ax
	jne	.LBB16_56
# BB#74:                                #   in Loop: Header=BB16_6 Depth=3
	subq	$8, %rsp
.Lcfi184:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.42, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%ebx, %esi
	pushq	$0
.Lcfi185:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi186:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi187:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi188:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB16_75
.LBB16_52:                              #   in Loop: Header=BB16_6 Depth=3
	cmpb	$1, typed(%rip)
	jne	.LBB16_75
# BB#53:                                #   in Loop: Header=BB16_6 Depth=3
	movq	stderr(%rip), %rdi
	movq	infile(%rip), %rdx
	movl	lineno(%rip), %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	movq	16(%rax), %r8
	movl	$.L.str.39, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB16_75
.LBB16_56:                              #   in Loop: Header=BB16_6 Depth=3
	movq	finput(%rip), %rsi
	movl	%ebx, %edi
	callq	ungetc
	movq	finput(%rip), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$45, %eax
	jne	.LBB16_57
# BB#58:                                #   in Loop: Header=BB16_6 Depth=3
	movq	%rbx, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	$-1, 12(%rsp)           # 4-byte Folded Spill
	jmp	.LBB16_59
.LBB16_57:                              #   in Loop: Header=BB16_6 Depth=3
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
.LBB16_59:                              #   in Loop: Header=BB16_6 Depth=3
	movq	(%rbp), %rcx
	movslq	%eax, %rdx
	xorl	%r13d, %r13d
	testb	$8, 1(%rcx,%rdx,2)
	je	.LBB16_62
# BB#60:                                # %.lr.ph.i103.preheader
                                        #   in Loop: Header=BB16_6 Depth=3
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB16_61:                              # %.lr.ph.i103
                                        #   Parent Loop BB16_3 Depth=1
                                        #     Parent Loop BB16_4 Depth=2
                                        #       Parent Loop BB16_6 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%r13,%r13,4), %ecx
	leal	-48(%rax,%rcx,2), %r13d
	movq	%rbx, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	(%rbp), %rcx
	movslq	%eax, %rdx
	testb	$8, 1(%rcx,%rdx,2)
	jne	.LBB16_61
.LBB16_62:                              # %read_signed_integer.exit106
                                        #   in Loop: Header=BB16_6 Depth=3
	movl	%eax, %edi
	movq	%rbx, %rsi
	callq	ungetc
	imull	12(%rsp), %r13d         # 4-byte Folded Reload
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	testq	%r12, %r12
	jne	.LBB16_70
# BB#63:                                # %read_signed_integer.exit106
                                        #   in Loop: Header=BB16_6 Depth=3
	testl	%r13d, %r13d
	jle	.LBB16_70
# BB#64:                                # %.lr.ph.i107.preheader
                                        #   in Loop: Header=BB16_6 Depth=3
	movl	%r13d, %ebx
	movq	16(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB16_65:                              # %.lr.ph.i107
                                        #   Parent Loop BB16_3 Depth=1
                                        #     Parent Loop BB16_4 Depth=2
                                        #       Parent Loop BB16_6 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.LBB16_67
# BB#66:                                #   in Loop: Header=BB16_65 Depth=4
	cmpq	$0, 8(%r12)
	jne	.LBB16_68
.LBB16_67:                              #   in Loop: Header=BB16_65 Depth=4
	movl	$.L.str.34, %edi
	callq	fatal
.LBB16_68:                              #   in Loop: Header=BB16_65 Depth=4
	decl	%ebx
	jne	.LBB16_65
	jmp	.LBB16_69
	.p2align	4, 0x90
.LBB16_9:                               #   in Loop: Header=BB16_4 Depth=2
	xorl	%eax, %eax
	decl	%r14d
	movl	$125, %ebp
	je	.LBB16_4
	jmp	.LBB16_10
.LBB16_89:
	movq	faction(%rip), %rcx
	movl	$.L.str.46, %edi
	movl	$13, %esi
	movl	$1, %edx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end16:
	.size	copy_action, .Lfunc_end16-copy_action
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI16_0:
	.quad	.LBB16_87
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_11
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_13
	.quad	.LBB16_88
	.quad	.LBB16_39
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_13
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_24
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_88
	.quad	.LBB16_76

	.text
	.globl	gensym
	.p2align	4, 0x90
	.type	gensym,@function
gensym:                                 # @gensym
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi189:
	.cfi_def_cfa_offset 16
	movl	gensym_count(%rip), %edx
	incl	%edx
	movl	%edx, gensym_count(%rip)
	movl	$token_buffer, %edi
	movl	$.L.str.49, %esi
	xorl	%eax, %eax
	callq	sprintf
	movl	$token_buffer, %edi
	xorl	%eax, %eax
	callq	getsym
	movb	$2, 40(%rax)
	movl	nvars(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, nvars(%rip)
	movw	%cx, 32(%rax)
	popq	%rcx
	retq
.Lfunc_end17:
	.size	gensym, .Lfunc_end17-gensym
	.cfi_endproc

	.globl	record_rule_line
	.p2align	4, 0x90
	.type	record_rule_line,@function
record_rule_line:                       # @record_rule_line
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi190:
	.cfi_def_cfa_offset 16
	movslq	nrules(%rip), %rsi
	cmpl	rline_allocated(%rip), %esi
	jl	.LBB18_3
# BB#1:
	leal	(%rsi,%rsi), %eax
	movl	%eax, rline_allocated(%rip)
	movq	rline(%rip), %rdi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, rline(%rip)
	testq	%rax, %rax
	je	.LBB18_2
.LBB18_3:
	movzwl	lineno(%rip), %eax
	movq	rline(%rip), %rcx
	movslq	nrules(%rip), %rdx
	movw	%ax, (%rcx,%rdx,2)
	popq	%rax
	retq
.LBB18_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str.60, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	done
	jmp	.LBB18_3
.Lfunc_end18:
	.size	record_rule_line, .Lfunc_end18-record_rule_line
	.cfi_endproc

	.globl	get_type
	.p2align	4, 0x90
	.type	get_type,@function
get_type:                               # @get_type
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi191:
	.cfi_def_cfa_offset 16
.Lcfi192:
	.cfi_offset %rbx, -16
	callq	lex
	cmpl	$21, %eax
	je	.LBB19_2
# BB#1:
	movl	$.L.str.16, %edi
	callq	fatal
.LBB19_2:
	movl	$token_buffer, %edi
	callq	strlen
	leal	1(%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %rbx
	movl	$token_buffer, %esi
	movq	%rbx, %rdi
	callq	strcpy
	jmp	.LBB19_3
	.p2align	4, 0x90
.LBB19_9:                               #   in Loop: Header=BB19_3 Depth=1
	movl	16(%rax), %esi
	subq	$8, %rsp
.Lcfi193:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.12, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
.Lcfi194:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi195:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi196:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi197:
	.cfi_adjust_cfa_offset -32
.LBB19_3:                               # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	callq	lex
	cmpl	$1, %eax
	je	.LBB19_7
# BB#4:                                 # %.backedge
                                        #   in Loop: Header=BB19_3 Depth=1
	cmpl	$2, %eax
	je	.LBB19_3
	jmp	.LBB19_5
	.p2align	4, 0x90
.LBB19_7:                               #   in Loop: Header=BB19_3 Depth=1
	movq	symval(%rip), %rax
	cmpq	$0, 24(%rax)
	jne	.LBB19_9
# BB#8:                                 #   in Loop: Header=BB19_3 Depth=1
	movq	%rbx, 24(%rax)
	jmp	.LBB19_3
.LBB19_5:                               # %.backedge
	cmpl	$4, %eax
	jne	.LBB19_10
# BB#6:
	popq	%rbx
	jmp	lex                     # TAILCALL
.LBB19_10:                              # %.loopexit
	popq	%rbx
	retq
.Lfunc_end19:
	.size	get_type, .Lfunc_end19-get_type
	.cfi_endproc

	.globl	output_token_defines
	.p2align	4, 0x90
	.type	output_token_defines,@function
output_token_defines:                   # @output_token_defines
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi198:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi199:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi200:
	.cfi_def_cfa_offset 32
.Lcfi201:
	.cfi_offset %rbx, -24
.Lcfi202:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	firstsymbol(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB20_2
	jmp	.LBB20_14
.LBB20_9:                               # %.preheader._crit_edge
                                        #   in Loop: Header=BB20_2 Depth=1
	cmpl	$0, translations(%rip)
	je	.LBB20_11
# BB#10:                                #   in Loop: Header=BB20_2 Depth=1
	movw	38(%rbx), %ax
.LBB20_11:                              #   in Loop: Header=BB20_2 Depth=1
	movswl	%ax, %ecx
	movl	$.L.str.66, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	cmpl	$0, semantic_parser(%rip)
	je	.LBB20_13
# BB#12:                                #   in Loop: Header=BB20_2 Depth=1
	movq	tags(%rip), %rax
	movswl	32(%rbx), %ecx
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rdx
	movl	$.L.str.67, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	.p2align	4, 0x90
.LBB20_13:                              # %.loopexit
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB20_14
.LBB20_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_7 Depth 2
	movswl	32(%rbx), %eax
	cmpl	ntokens(%rip), %eax
	jge	.LBB20_13
# BB#3:                                 #   in Loop: Header=BB20_2 Depth=1
	movslq	%eax, %rcx
	movq	tags(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movb	(%rdx), %cl
	cmpb	$39, %cl
	je	.LBB20_13
# BB#4:                                 #   in Loop: Header=BB20_2 Depth=1
	cmpq	errtoken(%rip), %rbx
	je	.LBB20_13
# BB#5:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB20_2 Depth=1
	testb	%cl, %cl
	je	.LBB20_9
# BB#6:                                 # %.lr.ph25.preheader
                                        #   in Loop: Header=BB20_2 Depth=1
	leaq	1(%rdx), %rsi
	.p2align	4, 0x90
.LBB20_7:                               # %.lr.ph25
                                        #   Parent Loop BB20_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$46, %cl
	je	.LBB20_13
# BB#8:                                 # %..preheader_crit_edge
                                        #   in Loop: Header=BB20_7 Depth=2
	movzbl	(%rsi), %ecx
	incq	%rsi
	testb	%cl, %cl
	jne	.LBB20_7
	jmp	.LBB20_9
.LBB20_14:                              # %._crit_edge
	movl	$10, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_IO_putc                # TAILCALL
.Lfunc_end20:
	.size	output_token_defines, .Lfunc_end20-output_token_defines
	.cfi_endproc

	.type	start_flag,@object      # @start_flag
	.comm	start_flag,4,4
	.type	startval,@object        # @startval
	.comm	startval,8,8
	.type	rline_allocated,@object # @rline_allocated
	.comm	rline_allocated,4,4
	.type	typed,@object           # @typed
	.local	typed
	.comm	typed,1,4
	.type	lastprec,@object        # @lastprec
	.local	lastprec
	.comm	lastprec,4,4
	.type	gensym_count,@object    # @gensym_count
	.local	gensym_count
	.comm	gensym_count,4,4
	.type	yylsp_needed,@object    # @yylsp_needed
	.local	yylsp_needed
	.comm	yylsp_needed,1,4
	.type	grammar,@object         # @grammar
	.comm	grammar,8,8
	.type	lineno,@object          # @lineno
	.comm	lineno,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"error"
	.size	.L.str, 6

	.type	errtoken,@object        # @errtoken
	.local	errtoken
	.comm	errtoken,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"$illegal."
	.size	.L.str.1, 10

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n/*  A Bison parser, made from %s  */\n\n"
	.size	.L.str.2, 40

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"#define YYLSP_NEEDED\n\n"
	.size	.L.str.3, 23

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"junk after % in definition section"
	.size	.L.str.4, 35

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"no input grammar"
	.size	.L.str.5, 17

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Unrecognized char '%c' in declaration section"
	.size	.L.str.6, 46

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"#line %d \"%s\"\n"
	.size	.L.str.7, 15

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"unterminated string"
	.size	.L.str.8, 20

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"unterminated comment in %{ definition"
	.size	.L.str.9, 38

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"unterminated %{ definition"
	.size	.L.str.10, 27

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"symbol %s redefined"
	.size	.L.str.11, 20

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"type redeclaration for %s"
	.size	.L.str.12, 26

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"invalid text in %token or %nterm declaration"
	.size	.L.str.13, 45

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"multiple %start declarations"
	.size	.L.str.14, 29

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"invalid %start declaration"
	.size	.L.str.15, 27

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"ill-formed %type declaration"
	.size	.L.str.16, 29

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"invalid %type declaration"
	.size	.L.str.17, 26

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"invalid text in association declaration"
	.size	.L.str.18, 40

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"malformatted association declaration"
	.size	.L.str.19, 37

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"multiple %union declarations"
	.size	.L.str.20, 29

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"\n#line %d \"%s\"\n"
	.size	.L.str.21, 16

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"typedef union"
	.size	.L.str.23, 14

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"unterminated comment"
	.size	.L.str.24, 21

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	" YYSTYPE;\n"
	.size	.L.str.25, 11

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"\n#ifndef YYLTYPE\ntypedef\n  struct yyltype\n    {\n      int first_line;\n      int first_column;\n      int last_line;\n      int last_column;\n    }\n  yyltype;\n\n#define YYLTYPE yyltype\n#endif\n\n"
	.size	.L.str.26, 189

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"#define\tYYACCEPT\tgoto yyaccept\n"
	.size	.L.str.27, 32

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"#define\tYYABORT\tgoto yyabort\n"
	.size	.L.str.28, 30

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"#define\tYYERROR\tgoto yyerrlab\n"
	.size	.L.str.29, 31

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"#define\tyytext\t(*_yytext)\nextern char **_yytext;\n"
	.size	.L.str.30, 50

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"#define\tyylval\t(*_yylval)\nextern YYSTYPE *_yylval;\n"
	.size	.L.str.31, 52

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"#define\tyylloc\t(*_yylloc)\nextern YYLTYPE *_yylloc;\n"
	.size	.L.str.32, 52

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"#define\tyyppval\t(*_yyppval)\nextern int *_yyppval;\n"
	.size	.L.str.33, 51

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"invalid $ value"
	.size	.L.str.34, 16

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"\ncase %d:\n"
	.size	.L.str.35, 11

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"unmatched right brace ('}')"
	.size	.L.str.36, 28

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"yyval"
	.size	.L.str.37, 6

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	".%s"
	.size	.L.str.38, 4

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"%s:%d:  warning:  $$ of '%s' has no declared type.\n"
	.size	.L.str.39, 52

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"yyp->yyvsp[%d]"
	.size	.L.str.40, 15

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"%s:%d:  warning:  $%d of '%s' has no declared type.\n"
	.size	.L.str.41, 53

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"$%c is invalid"
	.size	.L.str.42, 15

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"@%c is invalid"
	.size	.L.str.43, 15

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"yyp->yylsp[%d]"
	.size	.L.str.44, 15

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"unterminated %guard clause"
	.size	.L.str.45, 27

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	";\n    break;}"
	.size	.L.str.46, 14

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"invalid @-construct"
	.size	.L.str.47, 20

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"unmatched '{'"
	.size	.L.str.48, 14

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"@%d"
	.size	.L.str.49, 4

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"ill-formed rule"
	.size	.L.str.50, 16

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"grammar starts with vertical bar"
	.size	.L.str.51, 33

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"rule given for %s, which is a token"
	.size	.L.str.52, 36

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"%guard present but %semantic_parser not specified"
	.size	.L.str.53, 50

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"two actions at end of one rule"
	.size	.L.str.54, 31

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"%s:%d:  warning:  type clash ('%s' '%s') on default action\n"
	.size	.L.str.55, 60

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.zero	1
	.size	.L.str.56, 1

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"invalid input"
	.size	.L.str.57, 14

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"#ifndef YYSTYPE\n#define YYSTYPE int\n#endif\n"
	.size	.L.str.58, 44

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"symbol %s used, not defined as token, and no rules for it\n"
	.size	.L.str.59, 59

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"bison: memory exhausted\n"
	.size	.L.str.60, 25

	.type	tags,@object            # @tags
	.comm	tags,8,8
	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"$"
	.size	.L.str.61, 2

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"tokens %s and %s both assigned number %d"
	.size	.L.str.62, 41

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"the start symbol %s is undefined"
	.size	.L.str.63, 33

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"the start symbol %s is a token"
	.size	.L.str.64, 31

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"#define\tNT%s\t%d\n"
	.size	.L.str.65, 17

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"#define\t%s\t%d\n"
	.size	.L.str.66, 15

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"#define\tT%s\t%d\n"
	.size	.L.str.67, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
