	.text
	.file	"idctref.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	3
.LCPI0_0:
	.quad	4602678819172646912     # double 0.5
	.quad	4600040671590431693     # double 0.35355339059327379
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_1:
	.quad	4600745857669934360     # double 0.39269908169872414
.LCPI0_2:
	.quad	4602678819172646912     # double 0.5
.LCPI0_3:
	.quad	4609434218613702656     # double 1.5
.LCPI0_4:
	.quad	4612811918334230528     # double 2.5
.LCPI0_5:
	.quad	4615063718147915776     # double 3.5
.LCPI0_6:
	.quad	4616752568008179712     # double 4.5
.LCPI0_7:
	.quad	4617878467915022336     # double 5.5
.LCPI0_8:
	.quad	4619004367821864960     # double 6.5
.LCPI0_9:
	.quad	4620130267728707584     # double 7.5
	.text
	.globl	Initialize_Reference_IDCT
	.p2align	4, 0x90
	.type	Initialize_Reference_IDCT,@function
Initialize_Reference_IDCT:              # @Initialize_Reference_IDCT
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 48
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	xorl	%r14d, %r14d
	movl	$c+56, %ebx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	testq	%r14, %r14
	sete	%al
	movsd	.LCPI0_0(,%rax,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	mulsd	.LCPI0_1(%rip), %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	mulsd	.LCPI0_2(%rip), %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, -56(%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI0_3(%rip), %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, -48(%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI0_4(%rip), %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, -40(%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI0_5(%rip), %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, -32(%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI0_6(%rip), %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, -24(%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI0_7(%rip), %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, -16(%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI0_8(%rip), %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, -8(%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI0_9(%rip), %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, (%rbx)
	incq	%r14
	addq	$64, %rbx
	cmpq	$8, %r14
	jne	.LBB0_1
# BB#2:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	Initialize_Reference_IDCT, .Lfunc_end0-Initialize_Reference_IDCT
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	Reference_IDCT
	.p2align	4, 0x90
	.type	Reference_IDCT,@function
Reference_IDCT:                         # @Reference_IDCT
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 48
	subq	$576, %rsp              # imm = 0x240
.Lcfi10:
	.cfi_def_cfa_offset 624
.Lcfi11:
	.cfi_offset %rbx, -48
.Lcfi12:
	.cfi_offset %r12, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	leaq	64(%rsp), %rax
	xorl	%ecx, %ecx
	xorpd	%xmm8, %xmm8
	.p2align	4, 0x90
.LBB1_1:                                # %.preheader50
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_2 Depth 2
	movq	%rcx, %rdx
	shlq	$4, %rdx
	movswl	(%r14,%rdx), %esi
	xorps	%xmm9, %xmm9
	cvtsi2sdl	%esi, %xmm9
	movswl	2(%r14,%rdx), %esi
	xorps	%xmm10, %xmm10
	cvtsi2sdl	%esi, %xmm10
	movswl	4(%r14,%rdx), %esi
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%esi, %xmm3
	movswl	6(%r14,%rdx), %esi
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%esi, %xmm4
	movswl	8(%r14,%rdx), %esi
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%esi, %xmm5
	movswl	10(%r14,%rdx), %esi
	xorps	%xmm6, %xmm6
	cvtsi2sdl	%esi, %xmm6
	movswl	12(%r14,%rdx), %esi
	xorps	%xmm7, %xmm7
	cvtsi2sdl	%esi, %xmm7
	movswl	14(%r14,%rdx), %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movq	$-64, %rdx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader49
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	c+64(%rdx), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm9, %xmm1
	addsd	%xmm8, %xmm1
	movsd	c+128(%rdx), %xmm2      # xmm2 = mem[0],zero
	mulsd	%xmm10, %xmm2
	addsd	%xmm1, %xmm2
	movsd	c+192(%rdx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm3, %xmm1
	addsd	%xmm2, %xmm1
	movsd	c+256(%rdx), %xmm2      # xmm2 = mem[0],zero
	mulsd	%xmm4, %xmm2
	addsd	%xmm1, %xmm2
	movsd	c+320(%rdx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm5, %xmm1
	addsd	%xmm2, %xmm1
	movsd	c+384(%rdx), %xmm2      # xmm2 = mem[0],zero
	mulsd	%xmm6, %xmm2
	addsd	%xmm1, %xmm2
	movsd	c+448(%rdx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm7, %xmm1
	addsd	%xmm2, %xmm1
	movsd	c+512(%rdx), %xmm2      # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, (%rsi)
	addq	$8, %rsi
	addq	$8, %rdx
	jne	.LBB1_2
# BB#3:                                 #   in Loop: Header=BB1_1 Depth=1
	incq	%rcx
	addq	$64, %rax
	cmpq	$8, %rcx
	jne	.LBB1_1
# BB#4:                                 # %.preheader47.preheader
	xorl	%r15d, %r15d
	xorpd	%xmm1, %xmm1
	movsd	.LCPI1_0(%rip), %xmm3   # xmm3 = mem[0],zero
	movw	$-256, %r12w
	.p2align	4, 0x90
.LBB1_5:                                # %.preheader47
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_6 Depth 2
	movsd	64(%rsp,%r15,8), %xmm0  # xmm0 = mem[0],zero
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movsd	128(%rsp,%r15,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	movsd	192(%rsp,%r15,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	movsd	256(%rsp,%r15,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	movsd	320(%rsp,%r15,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movsd	384(%rsp,%r15,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	448(%rsp,%r15,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	512(%rsp,%r15,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	$-64, %rbx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	c+64(%rbx), %xmm0       # xmm0 = mem[0],zero
	mulsd	56(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	%xmm1, %xmm0
	movsd	c+128(%rbx), %xmm1      # xmm1 = mem[0],zero
	mulsd	48(%rsp), %xmm1         # 8-byte Folded Reload
	addsd	%xmm0, %xmm1
	movsd	c+192(%rbx), %xmm0      # xmm0 = mem[0],zero
	mulsd	40(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	%xmm1, %xmm0
	movsd	c+256(%rbx), %xmm1      # xmm1 = mem[0],zero
	mulsd	32(%rsp), %xmm1         # 8-byte Folded Reload
	addsd	%xmm0, %xmm1
	movsd	c+320(%rbx), %xmm0      # xmm0 = mem[0],zero
	mulsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	%xmm1, %xmm0
	movsd	c+384(%rbx), %xmm1      # xmm1 = mem[0],zero
	mulsd	16(%rsp), %xmm1         # 8-byte Folded Reload
	addsd	%xmm0, %xmm1
	movsd	c+448(%rbx), %xmm2      # xmm2 = mem[0],zero
	mulsd	8(%rsp), %xmm2          # 8-byte Folded Reload
	addsd	%xmm1, %xmm2
	movsd	c+512(%rbx), %xmm0      # xmm0 = mem[0],zero
	mulsd	(%rsp), %xmm0           # 8-byte Folded Reload
	addsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	callq	floor
	movsd	.LCPI1_0(%rip), %xmm3   # xmm3 = mem[0],zero
	xorpd	%xmm1, %xmm1
	cvttsd2si	%xmm0, %eax
	cmpl	$255, %eax
	movw	$255, %cx
	cmovlw	%ax, %cx
	cmpl	$-256, %eax
	cmovlw	%r12w, %cx
	movw	%cx, (%rbp)
	addq	$16, %rbp
	addq	$8, %rbx
	jne	.LBB1_6
# BB#7:                                 #   in Loop: Header=BB1_5 Depth=1
	incq	%r15
	addq	$2, %r14
	cmpq	$8, %r15
	jne	.LBB1_5
# BB#8:
	addq	$576, %rsp              # imm = 0x240
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	Reference_IDCT, .Lfunc_end1-Reference_IDCT
	.cfi_endproc

	.type	c,@object               # @c
	.local	c
	.comm	c,512,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
