	.text
	.file	"jcparam.bc"
	.globl	jpeg_add_quant_table
	.p2align	4, 0x90
	.type	jpeg_add_quant_table,@function
jpeg_add_quant_table:                   # @jpeg_add_quant_table
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movl	%ecx, %r15d
	movq	%rdx, %rbx
	movq	%rdi, %rbp
	movslq	%esi, %r12
	movl	28(%rbp), %eax
	cmpl	$100, %eax
	je	.LBB0_2
# BB#1:
	movq	(%rbp), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbp, %rdi
	callq	*(%rcx)
.LBB0_2:
	movq	88(%rbp,%r12,8), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_4
# BB#3:
	movq	%rbp, %rdi
	callq	jpeg_alloc_quant_table
	movq	%rax, %rcx
	movq	%rcx, 88(%rbp,%r12,8)
.LBB0_4:                                # %.preheader
	movslq	%r15d, %rsi
	xorl	%edi, %edi
	movabsq	$-6640827866535438581, %r8 # imm = 0xA3D70A3D70A3D70B
	movl	$1, %r9d
	testl	%r14d, %r14d
	je	.LBB0_7
# BB#5:                                 # %.preheader.split.us.preheader
	movw	$255, %r10w
	.p2align	4, 0x90
.LBB0_6:                                # %.preheader.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rdi,4), %ebp
	imulq	%rsi, %rbp
	leaq	50(%rbp), %rax
	imulq	%r8
	leaq	50(%rdx,%rbp), %rax
	movq	%rax, %rdx
	shrq	$63, %rdx
	sarq	$6, %rax
	addq	%rdx, %rax
	testq	%rax, %rax
	cmovleq	%r9, %rax
	cmpq	$255, %rax
	cmovgew	%r10w, %ax
	movw	%ax, (%rcx,%rdi,2)
	movl	4(%rbx,%rdi,4), %ebp
	imulq	%rsi, %rbp
	leaq	50(%rbp), %rax
	imulq	%r8
	leaq	50(%rdx,%rbp), %rax
	movq	%rax, %rdx
	shrq	$63, %rdx
	sarq	$6, %rax
	addq	%rdx, %rax
	testq	%rax, %rax
	cmovleq	%r9, %rax
	cmpq	$255, %rax
	cmovgew	%r10w, %ax
	movw	%ax, 2(%rcx,%rdi,2)
	addq	$2, %rdi
	cmpq	$64, %rdi
	jne	.LBB0_6
	jmp	.LBB0_9
.LBB0_7:                                # %.preheader.split.preheader
	movw	$32767, %r10w           # imm = 0x7FFF
	.p2align	4, 0x90
.LBB0_8:                                # %.preheader.split
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rdi,4), %ebp
	imulq	%rsi, %rbp
	leaq	50(%rbp), %rax
	imulq	%r8
	leaq	50(%rdx,%rbp), %rax
	movq	%rax, %rdx
	shrq	$63, %rdx
	sarq	$6, %rax
	addq	%rdx, %rax
	testq	%rax, %rax
	cmovleq	%r9, %rax
	cmpq	$32767, %rax            # imm = 0x7FFF
	cmovgew	%r10w, %ax
	movw	%ax, (%rcx,%rdi,2)
	movl	4(%rbx,%rdi,4), %ebp
	imulq	%rsi, %rbp
	leaq	50(%rbp), %rax
	imulq	%r8
	leaq	50(%rdx,%rbp), %rax
	movq	%rax, %rdx
	shrq	$63, %rdx
	sarq	$6, %rax
	addq	%rdx, %rax
	testq	%rax, %rax
	cmovleq	%r9, %rax
	cmpq	$32767, %rax            # imm = 0x7FFF
	cmovgew	%r10w, %ax
	movw	%ax, 2(%rcx,%rdi,2)
	addq	$2, %rdi
	cmpq	$64, %rdi
	jne	.LBB0_8
.LBB0_9:                                # %.us-lcssa.us
	movl	$0, 128(%rcx)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jpeg_add_quant_table, .Lfunc_end0-jpeg_add_quant_table
	.cfi_endproc

	.globl	jpeg_set_linear_quality
	.p2align	4, 0x90
	.type	jpeg_set_linear_quality,@function
jpeg_set_linear_quality:                # @jpeg_set_linear_quality
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movq	%rdi, %r15
	movl	28(%r15), %eax
	cmpl	$100, %eax
	je	.LBB1_2
# BB#1:
	movq	(%r15), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%r15, %rdi
	callq	*(%rcx)
.LBB1_2:
	movq	88(%r15), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_4
# BB#3:
	movq	%r15, %rdi
	callq	jpeg_alloc_quant_table
	movq	%rax, %rcx
	movq	%rcx, 88(%r15)
.LBB1_4:                                # %.preheader.i
	movslq	%ebp, %rbp
	xorl	%esi, %esi
	movabsq	$-6640827866535438581, %r8 # imm = 0xA3D70A3D70A3D70B
	movl	$1, %r9d
	testl	%r14d, %r14d
	je	.LBB1_7
# BB#5:                                 # %.preheader.split.us.i.preheader
	movw	$255, %di
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movl	jpeg_set_linear_quality.std_luminance_quant_tbl(,%rsi,4), %ebx
	imulq	%rbp, %rbx
	leaq	50(%rbx), %rax
	imulq	%r8
	leaq	50(%rdx,%rbx), %rax
	movq	%rax, %rdx
	shrq	$63, %rdx
	sarq	$6, %rax
	addq	%rdx, %rax
	testq	%rax, %rax
	cmovleq	%r9, %rax
	cmpq	$255, %rax
	cmovgew	%di, %ax
	movw	%ax, (%rcx,%rsi,2)
	movl	jpeg_set_linear_quality.std_luminance_quant_tbl+4(,%rsi,4), %ebx
	imulq	%rbp, %rbx
	leaq	50(%rbx), %rax
	imulq	%r8
	leaq	50(%rdx,%rbx), %rax
	movq	%rax, %rdx
	shrq	$63, %rdx
	sarq	$6, %rax
	addq	%rdx, %rax
	testq	%rax, %rax
	cmovleq	%r9, %rax
	cmpq	$255, %rax
	cmovgew	%di, %ax
	movw	%ax, 2(%rcx,%rsi,2)
	addq	$2, %rsi
	cmpq	$64, %rsi
	jne	.LBB1_6
	jmp	.LBB1_9
.LBB1_7:                                # %.preheader.split.i.preheader
	movw	$32767, %di             # imm = 0x7FFF
	.p2align	4, 0x90
.LBB1_8:                                # %.preheader.split.i
                                        # =>This Inner Loop Header: Depth=1
	movl	jpeg_set_linear_quality.std_luminance_quant_tbl(,%rsi,4), %ebx
	imulq	%rbp, %rbx
	leaq	50(%rbx), %rax
	imulq	%r8
	leaq	50(%rdx,%rbx), %rax
	movq	%rax, %rdx
	shrq	$63, %rdx
	sarq	$6, %rax
	addq	%rdx, %rax
	testq	%rax, %rax
	cmovleq	%r9, %rax
	cmpq	$32767, %rax            # imm = 0x7FFF
	cmovgew	%di, %ax
	movw	%ax, (%rcx,%rsi,2)
	movl	jpeg_set_linear_quality.std_luminance_quant_tbl+4(,%rsi,4), %ebx
	imulq	%rbp, %rbx
	leaq	50(%rbx), %rax
	imulq	%r8
	leaq	50(%rdx,%rbx), %rax
	movq	%rax, %rdx
	shrq	$63, %rdx
	sarq	$6, %rax
	addq	%rdx, %rax
	testq	%rax, %rax
	cmovleq	%r9, %rax
	cmpq	$32767, %rax            # imm = 0x7FFF
	cmovgew	%di, %ax
	movw	%ax, 2(%rcx,%rsi,2)
	addq	$2, %rsi
	cmpq	$64, %rsi
	jne	.LBB1_8
.LBB1_9:                                # %jpeg_add_quant_table.exit
	movl	$0, 128(%rcx)
	movl	28(%r15), %eax
	cmpl	$100, %eax
	je	.LBB1_11
# BB#10:
	movq	(%r15), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%r15, %rdi
	callq	*(%rcx)
.LBB1_11:
	movq	96(%r15), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_13
# BB#12:
	movq	%r15, %rdi
	callq	jpeg_alloc_quant_table
	movq	%rax, %rcx
	movq	%rcx, 96(%r15)
.LBB1_13:                               # %.preheader.i5
	xorl	%esi, %esi
	movabsq	$-6640827866535438581, %r8 # imm = 0xA3D70A3D70A3D70B
	movl	$1, %r9d
	testl	%r14d, %r14d
	je	.LBB1_16
# BB#14:                                # %.preheader.split.us.i10.preheader
	movw	$255, %di
	.p2align	4, 0x90
.LBB1_15:                               # %.preheader.split.us.i10
                                        # =>This Inner Loop Header: Depth=1
	movl	jpeg_set_linear_quality.std_chrominance_quant_tbl(,%rsi,4), %ebx
	imulq	%rbp, %rbx
	leaq	50(%rbx), %rax
	imulq	%r8
	leaq	50(%rdx,%rbx), %rax
	movq	%rax, %rdx
	shrq	$63, %rdx
	sarq	$6, %rax
	addq	%rdx, %rax
	testq	%rax, %rax
	cmovleq	%r9, %rax
	cmpq	$255, %rax
	cmovgew	%di, %ax
	movw	%ax, (%rcx,%rsi,2)
	movl	jpeg_set_linear_quality.std_chrominance_quant_tbl+4(,%rsi,4), %ebx
	imulq	%rbp, %rbx
	leaq	50(%rbx), %rax
	imulq	%r8
	leaq	50(%rdx,%rbx), %rax
	movq	%rax, %rdx
	shrq	$63, %rdx
	sarq	$6, %rax
	addq	%rdx, %rax
	testq	%rax, %rax
	cmovleq	%r9, %rax
	cmpq	$255, %rax
	cmovgew	%di, %ax
	movw	%ax, 2(%rcx,%rsi,2)
	addq	$2, %rsi
	cmpq	$64, %rsi
	jne	.LBB1_15
	jmp	.LBB1_18
.LBB1_16:                               # %.preheader.split.i16.preheader
	movw	$32767, %di             # imm = 0x7FFF
	.p2align	4, 0x90
.LBB1_17:                               # %.preheader.split.i16
                                        # =>This Inner Loop Header: Depth=1
	movl	jpeg_set_linear_quality.std_chrominance_quant_tbl(,%rsi,4), %ebx
	imulq	%rbp, %rbx
	leaq	50(%rbx), %rax
	imulq	%r8
	leaq	50(%rdx,%rbx), %rax
	movq	%rax, %rdx
	shrq	$63, %rdx
	sarq	$6, %rax
	addq	%rdx, %rax
	testq	%rax, %rax
	cmovleq	%r9, %rax
	cmpq	$32767, %rax            # imm = 0x7FFF
	cmovgew	%di, %ax
	movw	%ax, (%rcx,%rsi,2)
	movl	jpeg_set_linear_quality.std_chrominance_quant_tbl+4(,%rsi,4), %ebx
	imulq	%rbp, %rbx
	leaq	50(%rbx), %rax
	imulq	%r8
	leaq	50(%rdx,%rbx), %rax
	movq	%rax, %rdx
	shrq	$63, %rdx
	sarq	$6, %rax
	addq	%rdx, %rax
	testq	%rax, %rax
	cmovleq	%r9, %rax
	cmpq	$32767, %rax            # imm = 0x7FFF
	cmovgew	%di, %ax
	movw	%ax, 2(%rcx,%rsi,2)
	addq	$2, %rsi
	cmpq	$64, %rsi
	jne	.LBB1_17
.LBB1_18:                               # %jpeg_add_quant_table.exit17
	movl	$0, 128(%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	jpeg_set_linear_quality, .Lfunc_end1-jpeg_set_linear_quality
	.cfi_endproc

	.globl	jpeg_quality_scaling
	.p2align	4, 0x90
	.type	jpeg_quality_scaling,@function
jpeg_quality_scaling:                   # @jpeg_quality_scaling
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	movl	$1, %eax
	cmovgl	%edi, %eax
	cmpl	$101, %eax
	movl	$100, %ecx
	cmovll	%eax, %ecx
	cmpl	$49, %ecx
	jg	.LBB2_2
# BB#1:
	movl	$5000, %eax             # imm = 0x1388
	xorl	%edx, %edx
	divl	%ecx
	retq
.LBB2_2:
	addl	%ecx, %ecx
	movl	$200, %eax
	subl	%ecx, %eax
	retq
.Lfunc_end2:
	.size	jpeg_quality_scaling, .Lfunc_end2-jpeg_quality_scaling
	.cfi_endproc

	.globl	jpeg_set_quality
	.p2align	4, 0x90
	.type	jpeg_set_quality,@function
jpeg_set_quality:                       # @jpeg_set_quality
	.cfi_startproc
# BB#0:
	movl	%edx, %r8d
	testl	%esi, %esi
	movl	$1, %eax
	cmovgl	%esi, %eax
	cmpl	$101, %eax
	movl	$100, %ecx
	cmovll	%eax, %ecx
	cmpl	$49, %ecx
	jg	.LBB3_2
# BB#1:
	movl	$5000, %eax             # imm = 0x1388
	xorl	%edx, %edx
	divl	%ecx
	movl	%eax, %esi
	movl	%r8d, %edx
	jmp	jpeg_set_linear_quality # TAILCALL
.LBB3_2:
	addl	%ecx, %ecx
	movl	$200, %esi
	subl	%ecx, %esi
	movl	%r8d, %edx
	jmp	jpeg_set_linear_quality # TAILCALL
.Lfunc_end3:
	.size	jpeg_set_quality, .Lfunc_end3-jpeg_set_quality
	.cfi_endproc

	.globl	jpeg_set_defaults
	.p2align	4, 0x90
	.type	jpeg_set_defaults,@function
jpeg_set_defaults:                      # @jpeg_set_defaults
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 16
.Lcfi20:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	cmpl	$100, %eax
	je	.LBB4_2
# BB#1:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB4_2:
	cmpq	$0, 80(%rbx)
	jne	.LBB4_4
# BB#3:
	movq	8(%rbx), %rax
	xorl	%esi, %esi
	movl	$960, %edx              # imm = 0x3C0
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, 80(%rbx)
.LBB4_4:
	movl	$8, 64(%rbx)
	movl	$50, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	jpeg_set_linear_quality
	movq	120(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB4_6
# BB#5:
	movq	%rbx, %rdi
	callq	jpeg_alloc_huff_table
	movq	%rax, 120(%rbx)
.LBB4_6:                                # %add_huff_table.exit.i
	movaps	std_huff_tables.bits_dc_luminance(%rip), %xmm0
	movups	%xmm0, (%rax)
	movb	$0, 16(%rax)
	movq	120(%rbx), %rdi
	addq	$17, %rdi
	movl	$std_huff_tables.val_dc_luminance, %esi
	movl	$256, %edx              # imm = 0x100
	callq	memcpy
	movq	120(%rbx), %rax
	movl	$0, 276(%rax)
	movq	152(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB4_8
# BB#7:
	movq	%rbx, %rdi
	callq	jpeg_alloc_huff_table
	movq	%rax, 152(%rbx)
.LBB4_8:                                # %add_huff_table.exit8.i
	movaps	std_huff_tables.bits_ac_luminance(%rip), %xmm0
	movups	%xmm0, (%rax)
	movb	$125, 16(%rax)
	movq	152(%rbx), %rdi
	addq	$17, %rdi
	movl	$std_huff_tables.val_ac_luminance, %esi
	movl	$256, %edx              # imm = 0x100
	callq	memcpy
	movq	152(%rbx), %rax
	movl	$0, 276(%rax)
	movq	128(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB4_10
# BB#9:
	movq	%rbx, %rdi
	callq	jpeg_alloc_huff_table
	movq	%rax, 128(%rbx)
.LBB4_10:                               # %add_huff_table.exit9.i
	movaps	std_huff_tables.bits_dc_chrominance(%rip), %xmm0
	movups	%xmm0, (%rax)
	movb	$0, 16(%rax)
	movq	128(%rbx), %rdi
	addq	$17, %rdi
	movl	$std_huff_tables.val_dc_chrominance, %esi
	movl	$256, %edx              # imm = 0x100
	callq	memcpy
	movq	128(%rbx), %rax
	movl	$0, 276(%rax)
	movq	160(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB4_12
# BB#11:
	movq	%rbx, %rdi
	callq	jpeg_alloc_huff_table
	movq	%rax, 160(%rbx)
.LBB4_12:                               # %std_huff_tables.exit
	movaps	std_huff_tables.bits_ac_chrominance(%rip), %xmm0
	movups	%xmm0, (%rax)
	movb	$119, 16(%rax)
	movq	160(%rbx), %rdi
	addq	$17, %rdi
	movl	$std_huff_tables.val_ac_chrominance, %esi
	movl	$256, %edx              # imm = 0x100
	callq	memcpy
	movq	160(%rbx), %rax
	movl	$0, 276(%rax)
	movb	$0, 184(%rbx)
	movb	$1, 200(%rbx)
	movb	$5, 216(%rbx)
	movb	$0, 185(%rbx)
	movb	$1, 201(%rbx)
	movb	$5, 217(%rbx)
	movb	$0, 186(%rbx)
	movb	$1, 202(%rbx)
	movb	$5, 218(%rbx)
	movb	$0, 187(%rbx)
	movb	$1, 203(%rbx)
	movb	$5, 219(%rbx)
	movb	$0, 188(%rbx)
	movb	$1, 204(%rbx)
	movb	$5, 220(%rbx)
	movb	$0, 189(%rbx)
	movb	$1, 205(%rbx)
	movb	$5, 221(%rbx)
	movb	$0, 190(%rbx)
	movb	$1, 206(%rbx)
	movb	$5, 222(%rbx)
	movb	$0, 191(%rbx)
	movb	$1, 207(%rbx)
	movb	$5, 223(%rbx)
	movb	$0, 192(%rbx)
	movb	$1, 208(%rbx)
	movb	$5, 224(%rbx)
	movb	$0, 193(%rbx)
	movb	$1, 209(%rbx)
	movb	$5, 225(%rbx)
	movb	$0, 194(%rbx)
	movb	$1, 210(%rbx)
	movb	$5, 226(%rbx)
	movb	$0, 195(%rbx)
	movb	$1, 211(%rbx)
	movb	$5, 227(%rbx)
	movb	$0, 196(%rbx)
	movb	$1, 212(%rbx)
	movb	$5, 228(%rbx)
	movb	$0, 197(%rbx)
	movb	$1, 213(%rbx)
	movb	$5, 229(%rbx)
	movb	$0, 198(%rbx)
	movb	$1, 214(%rbx)
	movb	$5, 230(%rbx)
	movb	$0, 199(%rbx)
	movb	$1, 215(%rbx)
	movb	$5, 231(%rbx)
	movl	$0, 232(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 240(%rbx)
	xorl	%eax, %eax
	cmpl	$8, 64(%rbx)
	setg	%al
	movl	%eax, 256(%rbx)
	movb	$0, 284(%rbx)
	movups	%xmm0, 260(%rbx)
	movl	$0, 276(%rbx)
	movl	$65537, 286(%rbx)       # imm = 0x10001
	movq	%rbx, %rdi
	popq	%rbx
	jmp	jpeg_default_colorspace # TAILCALL
.Lfunc_end4:
	.size	jpeg_set_defaults, .Lfunc_end4-jpeg_set_defaults
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI5_1:
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI5_2:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	jpeg_default_colorspace
	.p2align	4, 0x90
	.type	jpeg_default_colorspace,@function
jpeg_default_colorspace:                # @jpeg_default_colorspace
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 16
.Lcfi22:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	52(%rbx), %eax
	cmpq	$5, %rax
	ja	.LBB5_22
# BB#1:
	jmpq	*.LJTI5_0(,%rax,8)
.LBB5_5:
	movl	28(%rbx), %eax
	cmpl	$100, %eax
	je	.LBB5_7
# BB#6:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB5_7:                                # %jpeg_set_colorspace.exit10
	movl	$0, 292(%rbx)
	movl	$1, 280(%rbx)
	movabsq	$12884901891, %rax      # imm = 0x300000003
	movq	%rax, 68(%rbx)
	movq	80(%rbx), %rax
	movl	$1, (%rax)
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [2,2,0,0]
	movups	%xmm0, 8(%rax)
	movl	$0, 24(%rax)
	movl	$2, 96(%rax)
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [1,1,1,1]
	movups	%xmm0, 104(%rax)
	movl	$1, 120(%rax)
	movl	$3, 192(%rax)
	movl	$1, 200(%rax)
	movl	$1, 204(%rax)
	movl	$1, 208(%rax)
	movl	$1, 212(%rax)
	movl	$1, 216(%rax)
	popq	%rbx
	retq
.LBB5_15:
	movl	28(%rbx), %eax
	cmpl	$100, %eax
	je	.LBB5_17
# BB#16:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB5_17:
	movl	$0, 72(%rbx)
	movl	$0, 280(%rbx)
	movl	$0, 292(%rbx)
	movl	48(%rbx), %ecx
	movl	%ecx, 68(%rbx)
	leal	-1(%rcx), %eax
	cmpl	$10, %eax
	jb	.LBB5_19
# BB#18:                                # %.preheader.i
	movq	(%rbx), %rax
	movl	$24, 40(%rax)
	movl	%ecx, 44(%rax)
	movl	$10, 48(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
	movl	68(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.LBB5_21
.LBB5_19:                               # %.lr.ph.i
	movq	80(%rbx), %rax
	movslq	%ecx, %rcx
	xorl	%edx, %edx
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [1,1,0,0]
	.p2align	4, 0x90
.LBB5_20:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rax)
	movups	%xmm0, 8(%rax)
	movl	$0, 24(%rax)
	incq	%rdx
	addq	$96, %rax
	cmpq	%rcx, %rdx
	jl	.LBB5_20
.LBB5_21:                               # %jpeg_set_colorspace.exit14
	popq	%rbx
	retq
.LBB5_2:
	movl	28(%rbx), %eax
	cmpl	$100, %eax
	je	.LBB5_4
# BB#3:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB5_4:                                # %jpeg_set_colorspace.exit
	movl	$0, 292(%rbx)
	movl	$1, 280(%rbx)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 68(%rbx)
	movq	80(%rbx), %rax
	movl	$1, (%rax)
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [1,1,0,0]
	movups	%xmm0, 8(%rax)
	movl	$0, 24(%rax)
	popq	%rbx
	retq
.LBB5_8:
	movl	28(%rbx), %eax
	cmpl	$100, %eax
	je	.LBB5_10
# BB#9:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB5_10:                               # %jpeg_set_colorspace.exit12
	movl	$0, 280(%rbx)
	movl	$1, 292(%rbx)
	movabsq	$17179869188, %rax      # imm = 0x400000004
	movq	%rax, 68(%rbx)
	movq	80(%rbx), %rax
	movl	$67, (%rax)
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [1,1,0,0]
	movups	%xmm0, 8(%rax)
	movl	$0, 24(%rax)
	movl	$77, 96(%rax)
	movups	%xmm0, 104(%rax)
	movl	$0, 120(%rax)
	movl	$89, 192(%rax)
	movl	$1, 200(%rax)
	movl	$1, 204(%rax)
	movl	$0, 208(%rax)
	movl	$0, 212(%rax)
	movl	$0, 216(%rax)
	movl	$75, 288(%rax)
	jmp	.LBB5_11
.LBB5_12:
	movl	28(%rbx), %eax
	cmpl	$100, %eax
	je	.LBB5_14
# BB#13:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB5_14:                               # %jpeg_set_colorspace.exit13
	movl	$0, 280(%rbx)
	movl	$1, 292(%rbx)
	movabsq	$21474836484, %rax      # imm = 0x500000004
	movq	%rax, 68(%rbx)
	movq	80(%rbx), %rax
	movl	$1, (%rax)
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [2,2,0,0]
	movups	%xmm0, 8(%rax)
	movl	$0, 24(%rax)
	movl	$2, 96(%rax)
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [1,1,1,1]
	movups	%xmm1, 104(%rax)
	movl	$1, 120(%rax)
	movl	$3, 192(%rax)
	movl	$1, 200(%rax)
	movl	$1, 204(%rax)
	movl	$1, 208(%rax)
	movl	$1, 212(%rax)
	movl	$1, 216(%rax)
	movl	$4, 288(%rax)
.LBB5_11:                               # %jpeg_set_colorspace.exit14
	movups	%xmm0, 296(%rax)
	movl	$0, 312(%rax)
	popq	%rbx
	retq
.LBB5_22:
	movq	(%rbx), %rax
	movl	$7, 40(%rax)
	movq	%rbx, %rdi
	popq	%rbx
	jmpq	*(%rax)                 # TAILCALL
.Lfunc_end5:
	.size	jpeg_default_colorspace, .Lfunc_end5-jpeg_default_colorspace
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_15
	.quad	.LBB5_2
	.quad	.LBB5_5
	.quad	.LBB5_5
	.quad	.LBB5_8
	.quad	.LBB5_12

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI6_1:
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI6_2:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	jpeg_set_colorspace
	.p2align	4, 0x90
	.type	jpeg_set_colorspace,@function
jpeg_set_colorspace:                    # @jpeg_set_colorspace
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	cmpl	$100, %eax
	je	.LBB6_2
# BB#1:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB6_2:
	movl	%ebp, 72(%rbx)
	movl	$0, 280(%rbx)
	movl	$0, 292(%rbx)
	cmpl	$5, %ebp
	ja	.LBB6_13
# BB#3:
	movl	%ebp, %eax
	jmpq	*.LJTI6_0(,%rax,8)
.LBB6_9:
	movl	48(%rbx), %ecx
	movl	%ecx, 68(%rbx)
	leal	-1(%rcx), %eax
	cmpl	$10, %eax
	jb	.LBB6_11
# BB#10:                                # %.preheader
	movq	(%rbx), %rax
	movl	$24, 40(%rax)
	movl	%ecx, 44(%rax)
	movl	$10, 48(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
	movl	68(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.LBB6_15
.LBB6_11:                               # %.lr.ph
	movq	80(%rbx), %rax
	movslq	%ecx, %rcx
	xorl	%edx, %edx
	movaps	.LCPI6_0(%rip), %xmm0   # xmm0 = [1,1,0,0]
	.p2align	4, 0x90
.LBB6_12:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rax)
	movups	%xmm0, 8(%rax)
	movl	$0, 24(%rax)
	incq	%rdx
	addq	$96, %rax
	cmpq	%rcx, %rdx
	jl	.LBB6_12
	jmp	.LBB6_15
.LBB6_14:
	movl	$1, 280(%rbx)
	movl	$1, 68(%rbx)
	movq	80(%rbx), %rax
	movl	$1, (%rax)
	movaps	.LCPI6_0(%rip), %xmm0   # xmm0 = [1,1,0,0]
	movups	%xmm0, 8(%rax)
	movl	$0, 24(%rax)
	jmp	.LBB6_15
.LBB6_4:
	movl	$1, 292(%rbx)
	movl	$3, 68(%rbx)
	movq	80(%rbx), %rax
	movl	$82, (%rax)
	movaps	.LCPI6_0(%rip), %xmm0   # xmm0 = [1,1,0,0]
	movups	%xmm0, 8(%rax)
	movl	$0, 24(%rax)
	movl	$71, 96(%rax)
	movups	%xmm0, 104(%rax)
	movl	$0, 120(%rax)
	movl	$66, 192(%rax)
	movl	$1, 200(%rax)
	movl	$1, 204(%rax)
	movl	$0, 208(%rax)
	movl	$0, 212(%rax)
	movl	$0, 216(%rax)
	jmp	.LBB6_15
.LBB6_5:
	movl	$1, 280(%rbx)
	movl	$3, 68(%rbx)
	movq	80(%rbx), %rax
	movl	$1, (%rax)
	movaps	.LCPI6_1(%rip), %xmm0   # xmm0 = [2,2,0,0]
	movups	%xmm0, 8(%rax)
	movl	$0, 24(%rax)
	movl	$2, 96(%rax)
	movaps	.LCPI6_2(%rip), %xmm0   # xmm0 = [1,1,1,1]
	movups	%xmm0, 104(%rax)
	movl	$1, 120(%rax)
	movl	$3, 192(%rax)
	movl	$1, 200(%rax)
	movl	$1, 204(%rax)
	movl	$1, 208(%rax)
	movl	$1, 212(%rax)
	movl	$1, 216(%rax)
	jmp	.LBB6_15
.LBB6_6:
	movl	$1, 292(%rbx)
	movl	$4, 68(%rbx)
	movq	80(%rbx), %rax
	movl	$67, (%rax)
	movaps	.LCPI6_0(%rip), %xmm0   # xmm0 = [1,1,0,0]
	movups	%xmm0, 8(%rax)
	movl	$0, 24(%rax)
	movl	$77, 96(%rax)
	movups	%xmm0, 104(%rax)
	movl	$0, 120(%rax)
	movl	$89, 192(%rax)
	movl	$1, 200(%rax)
	movl	$1, 204(%rax)
	movl	$0, 208(%rax)
	movl	$0, 212(%rax)
	movl	$0, 216(%rax)
	movl	$75, 288(%rax)
	jmp	.LBB6_7
.LBB6_13:
	movq	(%rbx), %rax
	movl	$8, 40(%rax)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmpq	*(%rax)                 # TAILCALL
.LBB6_8:
	movl	$1, 292(%rbx)
	movl	$4, 68(%rbx)
	movq	80(%rbx), %rax
	movl	$1, (%rax)
	movaps	.LCPI6_1(%rip), %xmm0   # xmm0 = [2,2,0,0]
	movups	%xmm0, 8(%rax)
	movl	$0, 24(%rax)
	movl	$2, 96(%rax)
	movaps	.LCPI6_2(%rip), %xmm1   # xmm1 = [1,1,1,1]
	movups	%xmm1, 104(%rax)
	movl	$1, 120(%rax)
	movl	$3, 192(%rax)
	movl	$1, 200(%rax)
	movl	$1, 204(%rax)
	movl	$1, 208(%rax)
	movl	$1, 212(%rax)
	movl	$1, 216(%rax)
	movl	$4, 288(%rax)
.LBB6_7:                                # %.loopexit
	movups	%xmm0, 296(%rax)
	movl	$0, 312(%rax)
.LBB6_15:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end6:
	.size	jpeg_set_colorspace, .Lfunc_end6-jpeg_set_colorspace
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_9
	.quad	.LBB6_14
	.quad	.LBB6_4
	.quad	.LBB6_5
	.quad	.LBB6_6
	.quad	.LBB6_8

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
.LCPI7_1:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI7_2:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI7_3:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI7_4:
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	2                       # 0x2
.LCPI7_5:
	.long	6                       # 0x6
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	2                       # 0x2
.LCPI7_6:
	.long	1                       # 0x1
	.long	63                      # 0x3f
	.long	2                       # 0x2
	.long	1                       # 0x1
.LCPI7_7:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
.LCPI7_8:
	.long	1                       # 0x1
	.long	63                      # 0x3f
	.long	1                       # 0x1
	.long	0                       # 0x0
.LCPI7_9:
	.long	1                       # 0x1
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	1                       # 0x1
	.text
	.globl	jpeg_simple_progression
	.p2align	4, 0x90
	.type	jpeg_simple_progression,@function
jpeg_simple_progression:                # @jpeg_simple_progression
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -32
.Lcfi32:
	.cfi_offset %r14, -24
.Lcfi33:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	movl	68(%rbx), %r14d
	cmpl	$100, %eax
	je	.LBB7_2
# BB#1:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB7_2:
	cmpl	$3, %r14d
	jne	.LBB7_5
# BB#3:
	movl	$10, %eax
	cmpl	$3, 72(%rbx)
	jne	.LBB7_4
	jmp	.LBB7_7
.LBB7_5:
	cmpl	$5, %r14d
	jl	.LBB7_4
# BB#6:
	leal	(%r14,%r14), %eax
	leal	(%rax,%rax,2), %eax
	jmp	.LBB7_7
.LBB7_4:                                # %.thread
	leal	2(,%r14,4), %eax
.LBB7_7:
	movq	8(%rbx), %rcx
	movslq	%eax, %r15
	leaq	(,%r15,4), %rax
	leaq	(%rax,%rax,8), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	*(%rcx)
	movq	%rax, 240(%rbx)
	movl	%r15d, 232(%rbx)
	cmpl	$3, %r14d
	jne	.LBB7_10
# BB#8:
	movl	72(%rbx), %ecx
	movl	$3, (%rax)
	cmpl	$3, %ecx
	jne	.LBB7_12
# BB#9:                                 # %.lr.ph.i119
	movl	$0, 4(%rax)
	movl	$1, 8(%rax)
	movl	$2, 12(%rax)
	movaps	.LCPI7_0(%rip), %xmm0   # xmm0 = [0,0,0,1]
	movups	%xmm0, 20(%rax)
	movl	$1, 36(%rax)
	movl	$0, 40(%rax)
	movaps	.LCPI7_4(%rip), %xmm0   # xmm0 = [1,5,0,2]
	movups	%xmm0, 56(%rax)
	movl	$1, 72(%rax)
	movl	$2, 76(%rax)
	movaps	.LCPI7_9(%rip), %xmm0   # xmm0 = [1,63,0,1]
	movups	%xmm0, 92(%rax)
	movl	$1, 108(%rax)
	movl	$1, 112(%rax)
	movups	%xmm0, 128(%rax)
	movl	$1, 144(%rax)
	movl	$0, 148(%rax)
	movaps	.LCPI7_5(%rip), %xmm0   # xmm0 = [6,63,0,2]
	movups	%xmm0, 164(%rax)
	movl	$1, 180(%rax)
	movl	$0, 184(%rax)
	movl	$1, 200(%rax)
	movl	$63, 204(%rax)
	movl	$2, 208(%rax)
	movl	$1, 212(%rax)
	movl	$3, 216(%rax)
	movl	$0, 220(%rax)
	movl	$1, 224(%rax)
	movl	$2, 228(%rax)
	movaps	.LCPI7_7(%rip), %xmm0   # xmm0 = [0,0,1,0]
	movups	%xmm0, 236(%rax)
	movl	$1, 252(%rax)
	movl	$2, 256(%rax)
	movdqa	.LCPI7_8(%rip), %xmm0   # xmm0 = [1,63,1,0]
	movdqu	%xmm0, 272(%rax)
	movl	$1, 288(%rax)
	movl	$1, 292(%rax)
	movdqu	%xmm0, 308(%rax)
	movl	$1, 324(%rax)
	movl	$0, 328(%rax)
	movdqu	%xmm0, 344(%rax)
	jmp	.LBB7_78
.LBB7_10:
	cmpl	$4, %r14d
	jg	.LBB7_21
# BB#11:
	movl	%r14d, (%rax)
	testl	%r14d, %r14d
	jle	.LBB7_20
.LBB7_12:                               # %.lr.ph.preheader.i99
	cmpl	$8, %r14d
	jae	.LBB7_14
# BB#13:
	xorl	%ecx, %ecx
	jmp	.LBB7_19
.LBB7_21:                               # %.lr.ph.preheader.i.i105
	leal	-1(%r14), %ecx
	movl	%r14d, %edi
	xorl	%edx, %edx
	andl	$3, %edi
	je	.LBB7_22
# BB#23:                                # %.lr.ph.i.i109.prol.preheader
	movdqa	.LCPI7_0(%rip), %xmm0   # xmm0 = [0,0,0,1]
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB7_24:                               # %.lr.ph.i.i109.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, (%rsi)
	movl	%edx, 4(%rsi)
	movdqu	%xmm0, 20(%rsi)
	addq	$36, %rsi
	incl	%edx
	cmpl	%edx, %edi
	jne	.LBB7_24
	jmp	.LBB7_25
.LBB7_14:                               # %min.iters.checked
	movl	%r14d, %edx
	andl	$7, %edx
	movq	%r14, %rcx
	subq	%rdx, %rcx
	je	.LBB7_15
# BB#16:                                # %vector.body.preheader
	leaq	20(%rax), %rsi
	movdqa	.LCPI7_1(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI7_2(%rip), %xmm1   # xmm1 = [4,4,4,4]
	movdqa	.LCPI7_3(%rip), %xmm2   # xmm2 = [8,8,8,8]
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB7_17:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm3
	paddd	%xmm1, %xmm3
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	paddd	%xmm2, %xmm0
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB7_17
# BB#18:                                # %middle.block
	testl	%edx, %edx
	jne	.LBB7_19
	jmp	.LBB7_20
.LBB7_22:
	movq	%rax, %rsi
.LBB7_25:                               # %.lr.ph.i.i109.prol.loopexit
	cmpl	$3, %ecx
	jb	.LBB7_28
# BB#26:                                # %.lr.ph.preheader.i.i105.new
	movdqa	.LCPI7_0(%rip), %xmm0   # xmm0 = [0,0,0,1]
	.p2align	4, 0x90
.LBB7_27:                               # %.lr.ph.i.i109
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, (%rsi)
	movl	%edx, 4(%rsi)
	movdqu	%xmm0, 20(%rsi)
	leal	1(%rdx), %edi
	movl	$1, 36(%rsi)
	movl	%edi, 40(%rsi)
	movdqu	%xmm0, 56(%rsi)
	movl	$1, 72(%rsi)
	leal	2(%rdx), %edi
	movl	%edi, 76(%rsi)
	movdqu	%xmm0, 92(%rsi)
	movl	$1, 108(%rsi)
	leal	3(%rdx), %edi
	movl	%edi, 112(%rsi)
	movdqu	%xmm0, 128(%rsi)
	addl	$4, %edx
	addq	$144, %rsi
	cmpl	%r14d, %edx
	jne	.LBB7_27
.LBB7_28:                               # %fill_scans.exit.loopexit.i110
	movl	%ecx, %ecx
	incq	%rcx
	xorl	%r8d, %r8d
	jmp	.LBB7_29
.LBB7_15:
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_19:                               # %.lr.ph.i103
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, 4(%rax,%rcx,4)
	incq	%rcx
	cmpq	%rcx, %r14
	jne	.LBB7_19
.LBB7_20:                               # %._crit_edge.i104
	movdqa	.LCPI7_0(%rip), %xmm0   # xmm0 = [0,0,0,1]
	movdqu	%xmm0, 20(%rax)
	movb	$1, %r8b
	movl	$1, %ecx
.LBB7_29:                               # %fill_dc_scans.exit113
	leaq	(%rcx,%rcx,8), %rcx
	leaq	(%rax,%rcx,4), %rax
	testl	%r14d, %r14d
	jle	.LBB7_52
# BB#30:                                # %.lr.ph.preheader.i88
	leal	-1(%r14), %edx
	movl	%r14d, %ebx
	xorl	%esi, %esi
	andl	$3, %ebx
	je	.LBB7_31
# BB#32:                                # %.lr.ph.i92.prol.preheader
	movdqa	.LCPI7_4(%rip), %xmm0   # xmm0 = [1,5,0,2]
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB7_33:                               # %.lr.ph.i92.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, (%rdi)
	movl	%esi, 4(%rdi)
	movdqu	%xmm0, 20(%rdi)
	addq	$36, %rdi
	incl	%esi
	cmpl	%esi, %ebx
	jne	.LBB7_33
	jmp	.LBB7_34
.LBB7_31:
	movq	%rax, %rdi
.LBB7_34:                               # %.lr.ph.i92.prol.loopexit
	cmpl	$3, %edx
	jb	.LBB7_37
# BB#35:                                # %.lr.ph.preheader.i88.new
	movdqa	.LCPI7_4(%rip), %xmm0   # xmm0 = [1,5,0,2]
	.p2align	4, 0x90
.LBB7_36:                               # %.lr.ph.i92
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, (%rdi)
	movl	%esi, 4(%rdi)
	movdqu	%xmm0, 20(%rdi)
	leal	1(%rsi), %ecx
	movl	$1, 36(%rdi)
	movl	%ecx, 40(%rdi)
	movdqu	%xmm0, 56(%rdi)
	movl	$1, 72(%rdi)
	leal	2(%rsi), %ecx
	movl	%ecx, 76(%rdi)
	movdqu	%xmm0, 92(%rdi)
	movl	$1, 108(%rdi)
	leal	3(%rsi), %ecx
	movl	%ecx, 112(%rdi)
	movdqu	%xmm0, 128(%rdi)
	addl	$4, %esi
	addq	$144, %rdi
	cmpl	%r14d, %esi
	jne	.LBB7_36
.LBB7_37:                               # %.lr.ph.preheader.i78
	leaq	1(%rdx), %rcx
	leaq	(%rcx,%rcx,8), %rsi
	leaq	(%rax,%rsi,4), %rbx
	movl	%r14d, %ecx
	xorl	%edi, %edi
	andl	$3, %ecx
	je	.LBB7_38
# BB#39:                                # %.lr.ph.i82.prol.preheader
	movdqa	.LCPI7_5(%rip), %xmm0   # xmm0 = [6,63,0,2]
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB7_40:                               # %.lr.ph.i82.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, (%rax)
	movl	%edi, 4(%rax)
	movdqu	%xmm0, 20(%rax)
	addq	$36, %rax
	incl	%edi
	cmpl	%edi, %ecx
	jne	.LBB7_40
	jmp	.LBB7_41
.LBB7_38:
	movq	%rbx, %rax
.LBB7_41:                               # %.lr.ph.i82.prol.loopexit
	cmpl	$3, %edx
	jb	.LBB7_44
# BB#42:                                # %.lr.ph.preheader.i78.new
	movdqa	.LCPI7_5(%rip), %xmm0   # xmm0 = [6,63,0,2]
	.p2align	4, 0x90
.LBB7_43:                               # %.lr.ph.i82
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, (%rax)
	movl	%edi, 4(%rax)
	movdqu	%xmm0, 20(%rax)
	leal	1(%rdi), %ecx
	movl	$1, 36(%rax)
	movl	%ecx, 40(%rax)
	movdqu	%xmm0, 56(%rax)
	movl	$1, 72(%rax)
	leal	2(%rdi), %ecx
	movl	%ecx, 76(%rax)
	movdqu	%xmm0, 92(%rax)
	movl	$1, 108(%rax)
	leal	3(%rdi), %ecx
	movl	%ecx, 112(%rax)
	movdqu	%xmm0, 128(%rax)
	addl	$4, %edi
	addq	$144, %rax
	cmpl	%r14d, %edi
	jne	.LBB7_43
.LBB7_44:                               # %.lr.ph.preheader.i68
	leaq	(%rbx,%rsi,4), %rbx
	movl	%r14d, %ecx
	xorl	%edi, %edi
	andl	$3, %ecx
	je	.LBB7_45
# BB#46:                                # %.lr.ph.i72.prol.preheader
	movdqa	.LCPI7_6(%rip), %xmm0   # xmm0 = [1,63,2,1]
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB7_47:                               # %.lr.ph.i72.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, (%rax)
	movl	%edi, 4(%rax)
	movdqu	%xmm0, 20(%rax)
	addq	$36, %rax
	incl	%edi
	cmpl	%edi, %ecx
	jne	.LBB7_47
	jmp	.LBB7_48
.LBB7_45:
	movq	%rbx, %rax
.LBB7_48:                               # %.lr.ph.i72.prol.loopexit
	cmpl	$3, %edx
	jb	.LBB7_51
# BB#49:                                # %.lr.ph.preheader.i68.new
	movdqa	.LCPI7_6(%rip), %xmm0   # xmm0 = [1,63,2,1]
	.p2align	4, 0x90
.LBB7_50:                               # %.lr.ph.i72
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, (%rax)
	movl	%edi, 4(%rax)
	movdqu	%xmm0, 20(%rax)
	leal	1(%rdi), %ecx
	movl	$1, 36(%rax)
	movl	%ecx, 40(%rax)
	movdqu	%xmm0, 56(%rax)
	movl	$1, 72(%rax)
	leal	2(%rdi), %ecx
	movl	%ecx, 76(%rax)
	movdqu	%xmm0, 92(%rax)
	movl	$1, 108(%rax)
	leal	3(%rdi), %ecx
	movl	%ecx, 112(%rax)
	movdqu	%xmm0, 128(%rax)
	addl	$4, %edi
	addq	$144, %rax
	cmpl	%r14d, %edi
	jne	.LBB7_50
.LBB7_51:                               # %._crit_edge.loopexit.i74
	leaq	(%rbx,%rsi,4), %rax
.LBB7_52:                               # %fill_scans.exit77
	testb	%r8b, %r8b
	je	.LBB7_63
# BB#53:
	movl	%r14d, (%rax)
	testl	%r14d, %r14d
	jle	.LBB7_62
# BB#54:                                # %.lr.ph.preheader.i53
	cmpl	$8, %r14d
	jae	.LBB7_56
# BB#55:
	xorl	%ecx, %ecx
	jmp	.LBB7_61
.LBB7_63:                               # %.lr.ph.preheader.i.i59
	leal	-1(%r14), %ecx
	movl	%r14d, %edi
	xorl	%edx, %edx
	andl	$3, %edi
	je	.LBB7_64
# BB#65:                                # %.lr.ph.i.i63.prol.preheader
	movdqa	.LCPI7_7(%rip), %xmm0   # xmm0 = [0,0,1,0]
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB7_66:                               # %.lr.ph.i.i63.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, (%rsi)
	movl	%edx, 4(%rsi)
	movdqu	%xmm0, 20(%rsi)
	addq	$36, %rsi
	incl	%edx
	cmpl	%edx, %edi
	jne	.LBB7_66
	jmp	.LBB7_67
.LBB7_64:
	movq	%rax, %rsi
.LBB7_67:                               # %.lr.ph.i.i63.prol.loopexit
	cmpl	$3, %ecx
	jb	.LBB7_70
# BB#68:                                # %.lr.ph.preheader.i.i59.new
	movdqa	.LCPI7_7(%rip), %xmm0   # xmm0 = [0,0,1,0]
	.p2align	4, 0x90
.LBB7_69:                               # %.lr.ph.i.i63
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, (%rsi)
	movl	%edx, 4(%rsi)
	movdqu	%xmm0, 20(%rsi)
	leal	1(%rdx), %edi
	movl	$1, 36(%rsi)
	movl	%edi, 40(%rsi)
	movdqu	%xmm0, 56(%rsi)
	movl	$1, 72(%rsi)
	leal	2(%rdx), %edi
	movl	%edi, 76(%rsi)
	movdqu	%xmm0, 92(%rsi)
	movl	$1, 108(%rsi)
	leal	3(%rdx), %edi
	movl	%edi, 112(%rsi)
	movdqu	%xmm0, 128(%rsi)
	addl	$4, %edx
	addq	$144, %rsi
	cmpl	%r14d, %edx
	jne	.LBB7_69
.LBB7_70:                               # %fill_scans.exit.loopexit.i64
	movl	%ecx, %ecx
	incq	%rcx
	testl	%r14d, %r14d
	jg	.LBB7_72
	jmp	.LBB7_78
.LBB7_56:                               # %min.iters.checked141
	movl	%r14d, %edx
	andl	$7, %edx
	movq	%r14, %rcx
	subq	%rdx, %rcx
	je	.LBB7_57
# BB#58:                                # %vector.body137.preheader
	leaq	20(%rax), %rsi
	movdqa	.LCPI7_1(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI7_2(%rip), %xmm1   # xmm1 = [4,4,4,4]
	movdqa	.LCPI7_3(%rip), %xmm2   # xmm2 = [8,8,8,8]
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB7_59:                               # %vector.body137
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm3
	paddd	%xmm1, %xmm3
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	paddd	%xmm2, %xmm0
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB7_59
# BB#60:                                # %middle.block138
	testl	%edx, %edx
	jne	.LBB7_61
	jmp	.LBB7_62
.LBB7_57:
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_61:                               # %.lr.ph.i57
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, 4(%rax,%rcx,4)
	incq	%rcx
	cmpq	%rcx, %r14
	jne	.LBB7_61
.LBB7_62:                               # %._crit_edge.i58
	movdqa	.LCPI7_7(%rip), %xmm0   # xmm0 = [0,0,1,0]
	movdqu	%xmm0, 20(%rax)
	movl	$1, %ecx
	testl	%r14d, %r14d
	jle	.LBB7_78
.LBB7_72:                               # %.lr.ph.preheader.i48
	leaq	(%rcx,%rcx,8), %rcx
	leaq	(%rax,%rcx,4), %rax
	leal	-1(%r14), %edx
	movl	%r14d, %esi
	xorl	%ecx, %ecx
	andl	$3, %esi
	je	.LBB7_75
# BB#73:                                # %.lr.ph.i50.prol.preheader
	movdqa	.LCPI7_8(%rip), %xmm0   # xmm0 = [1,63,1,0]
	.p2align	4, 0x90
.LBB7_74:                               # %.lr.ph.i50.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, (%rax)
	movl	%ecx, 4(%rax)
	movdqu	%xmm0, 20(%rax)
	addq	$36, %rax
	incl	%ecx
	cmpl	%ecx, %esi
	jne	.LBB7_74
.LBB7_75:                               # %.lr.ph.i50.prol.loopexit
	cmpl	$3, %edx
	jb	.LBB7_78
# BB#76:                                # %.lr.ph.preheader.i48.new
	movdqa	.LCPI7_8(%rip), %xmm0   # xmm0 = [1,63,1,0]
	.p2align	4, 0x90
.LBB7_77:                               # %.lr.ph.i50
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, (%rax)
	movl	%ecx, 4(%rax)
	movdqu	%xmm0, 20(%rax)
	leal	1(%rcx), %edx
	movl	$1, 36(%rax)
	movl	%edx, 40(%rax)
	movdqu	%xmm0, 56(%rax)
	movl	$1, 72(%rax)
	leal	2(%rcx), %edx
	movl	%edx, 76(%rax)
	movdqu	%xmm0, 92(%rax)
	movl	$1, 108(%rax)
	leal	3(%rcx), %edx
	movl	%edx, 112(%rax)
	movdqu	%xmm0, 128(%rax)
	addl	$4, %ecx
	addq	$144, %rax
	cmpl	%r14d, %ecx
	jne	.LBB7_77
.LBB7_78:                               # %fill_scans.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	jpeg_simple_progression, .Lfunc_end7-jpeg_simple_progression
	.cfi_endproc

	.type	jpeg_set_linear_quality.std_luminance_quant_tbl,@object # @jpeg_set_linear_quality.std_luminance_quant_tbl
	.section	.rodata,"a",@progbits
	.p2align	4
jpeg_set_linear_quality.std_luminance_quant_tbl:
	.long	16                      # 0x10
	.long	11                      # 0xb
	.long	10                      # 0xa
	.long	16                      # 0x10
	.long	24                      # 0x18
	.long	40                      # 0x28
	.long	51                      # 0x33
	.long	61                      # 0x3d
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	14                      # 0xe
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	58                      # 0x3a
	.long	60                      # 0x3c
	.long	55                      # 0x37
	.long	14                      # 0xe
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	24                      # 0x18
	.long	40                      # 0x28
	.long	57                      # 0x39
	.long	69                      # 0x45
	.long	56                      # 0x38
	.long	14                      # 0xe
	.long	17                      # 0x11
	.long	22                      # 0x16
	.long	29                      # 0x1d
	.long	51                      # 0x33
	.long	87                      # 0x57
	.long	80                      # 0x50
	.long	62                      # 0x3e
	.long	18                      # 0x12
	.long	22                      # 0x16
	.long	37                      # 0x25
	.long	56                      # 0x38
	.long	68                      # 0x44
	.long	109                     # 0x6d
	.long	103                     # 0x67
	.long	77                      # 0x4d
	.long	24                      # 0x18
	.long	35                      # 0x23
	.long	55                      # 0x37
	.long	64                      # 0x40
	.long	81                      # 0x51
	.long	104                     # 0x68
	.long	113                     # 0x71
	.long	92                      # 0x5c
	.long	49                      # 0x31
	.long	64                      # 0x40
	.long	78                      # 0x4e
	.long	87                      # 0x57
	.long	103                     # 0x67
	.long	121                     # 0x79
	.long	120                     # 0x78
	.long	101                     # 0x65
	.long	72                      # 0x48
	.long	92                      # 0x5c
	.long	95                      # 0x5f
	.long	98                      # 0x62
	.long	112                     # 0x70
	.long	100                     # 0x64
	.long	103                     # 0x67
	.long	99                      # 0x63
	.size	jpeg_set_linear_quality.std_luminance_quant_tbl, 256

	.type	jpeg_set_linear_quality.std_chrominance_quant_tbl,@object # @jpeg_set_linear_quality.std_chrominance_quant_tbl
	.p2align	4
jpeg_set_linear_quality.std_chrominance_quant_tbl:
	.long	17                      # 0x11
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	47                      # 0x2f
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	18                      # 0x12
	.long	21                      # 0x15
	.long	26                      # 0x1a
	.long	66                      # 0x42
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	24                      # 0x18
	.long	26                      # 0x1a
	.long	56                      # 0x38
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	47                      # 0x2f
	.long	66                      # 0x42
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.long	99                      # 0x63
	.size	jpeg_set_linear_quality.std_chrominance_quant_tbl, 256

	.type	std_huff_tables.bits_dc_luminance,@object # @std_huff_tables.bits_dc_luminance
	.p2align	4
std_huff_tables.bits_dc_luminance:
	.asciz	"\000\000\001\005\001\001\001\001\001\001\000\000\000\000\000\000"
	.size	std_huff_tables.bits_dc_luminance, 17

	.type	std_huff_tables.val_dc_luminance,@object # @std_huff_tables.val_dc_luminance
std_huff_tables.val_dc_luminance:
	.ascii	"\000\001\002\003\004\005\006\007\b\t\n\013"
	.size	std_huff_tables.val_dc_luminance, 12

	.type	std_huff_tables.bits_dc_chrominance,@object # @std_huff_tables.bits_dc_chrominance
	.p2align	4
std_huff_tables.bits_dc_chrominance:
	.asciz	"\000\000\003\001\001\001\001\001\001\001\001\001\000\000\000\000"
	.size	std_huff_tables.bits_dc_chrominance, 17

	.type	std_huff_tables.val_dc_chrominance,@object # @std_huff_tables.val_dc_chrominance
std_huff_tables.val_dc_chrominance:
	.ascii	"\000\001\002\003\004\005\006\007\b\t\n\013"
	.size	std_huff_tables.val_dc_chrominance, 12

	.type	std_huff_tables.bits_ac_luminance,@object # @std_huff_tables.bits_ac_luminance
	.p2align	4
std_huff_tables.bits_ac_luminance:
	.ascii	"\000\000\002\001\003\003\002\004\003\005\005\004\004\000\000\001}"
	.size	std_huff_tables.bits_ac_luminance, 17

	.type	std_huff_tables.val_ac_luminance,@object # @std_huff_tables.val_ac_luminance
	.p2align	4
std_huff_tables.val_ac_luminance:
	.ascii	"\001\002\003\000\004\021\005\022!1A\006\023Qa\007\"q\0242\201\221\241\b#B\261\301\025R\321\360$3br\202\t\n\026\027\030\031\032%&'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz\203\204\205\206\207\210\211\212\222\223\224\225\226\227\230\231\232\242\243\244\245\246\247\250\251\252\262\263\264\265\266\267\270\271\272\302\303\304\305\306\307\310\311\312\322\323\324\325\326\327\330\331\332\341\342\343\344\345\346\347\350\351\352\361\362\363\364\365\366\367\370\371\372"
	.size	std_huff_tables.val_ac_luminance, 162

	.type	std_huff_tables.bits_ac_chrominance,@object # @std_huff_tables.bits_ac_chrominance
	.p2align	4
std_huff_tables.bits_ac_chrominance:
	.ascii	"\000\000\002\001\002\004\004\003\004\007\005\004\004\000\001\002w"
	.size	std_huff_tables.bits_ac_chrominance, 17

	.type	std_huff_tables.val_ac_chrominance,@object # @std_huff_tables.val_ac_chrominance
	.p2align	4
std_huff_tables.val_ac_chrominance:
	.ascii	"\000\001\002\003\021\004\005!1\006\022AQ\007aq\023\"2\201\b\024B\221\241\261\301\t#3R\360\025br\321\n\026$4\341%\361\027\030\031\032&'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz\202\203\204\205\206\207\210\211\212\222\223\224\225\226\227\230\231\232\242\243\244\245\246\247\250\251\252\262\263\264\265\266\267\270\271\272\302\303\304\305\306\307\310\311\312\322\323\324\325\326\327\330\331\332\342\343\344\345\346\347\350\351\352\362\363\364\365\366\367\370\371\372"
	.size	std_huff_tables.val_ac_chrominance, 162


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
