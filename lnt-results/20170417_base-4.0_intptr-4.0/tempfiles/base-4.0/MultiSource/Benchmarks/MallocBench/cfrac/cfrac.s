	.text
	.file	"cfrac.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	-4616189618054758400    # double -1
.LCPI0_2:
	.long	1059760811              # float 0.666666686
	.long	1068149419              # float 1.33333337
.LCPI0_3:
	.quad	4602678819172646912     # double 0.5
.LCPI0_4:
	.quad	4604180019048437077     # double 0.66666666666666663
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_1:
	.long	1051372203              # float 0.333333343
	.text
	.globl	pfKnuthEx28
	.p2align	4, 0x90
	.type	pfKnuthEx28,@function
pfKnuthEx28:                            # @pfKnuthEx28
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %r14d
	movq	$0, 8(%rsp)
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:
	incw	(%rbx)
.LBB0_2:
	cmpl	$2, %r14d
	jne	.LBB0_9
# BB#3:
	movq	%rbx, %rdi
	callq	podd
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	phalf
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	8(%rsp), %rdi
	callq	podd
	testl	%ebp, %ebp
	je	.LBB0_4
# BB#7:
	testl	%eax, %eax
	jne	.LBB0_5
# BB#8:
	movq	8(%rsp), %rdi
	callq	phalf
	movq	%rax, %rdi
	callq	podd
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	movss	.LCPI0_2(,%rcx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB0_12
.LBB0_9:
	movl	%r14d, %edi
	callq	utop
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	8(%rsp), %rdi
	movq	pone(%rip), %rsi
	callq	psub
	movq	%rax, %rdi
	callq	phalf
	movq	8(%rsp), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	ppowmod
	movq	pone(%rip), %rsi
	movq	%rax, %rdi
	callq	pcmp
	xorps	%xmm0, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jne	.LBB0_13
# BB#10:
	leal	(%r14,%r14), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	%r14d, %eax
	cvtsi2ssq	%rax, %xmm1
	mulss	%xmm1, %xmm1
	cvtss2sd	%xmm1, %xmm1
	addsd	.LCPI0_0(%rip), %xmm1
	divsd	%xmm1, %xmm0
	jmp	.LBB0_11
.LBB0_4:
	testl	%eax, %eax
	je	.LBB0_6
.LBB0_5:
	movss	.LCPI0_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB0_12
.LBB0_6:
	movq	8(%rsp), %rsi
	movl	$2, %edi
	callq	pfKnuthEx28
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI0_3(%rip), %xmm0
	addsd	.LCPI0_4(%rip), %xmm0
.LBB0_11:
	cvtsd2ss	%xmm0, %xmm0
.LBB0_12:
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
.LBB0_13:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_16
# BB#14:
	decw	(%rdi)
	jne	.LBB0_16
# BB#15:
	xorl	%eax, %eax
	callq	pfree
.LBB0_16:
	testq	%rbx, %rbx
	je	.LBB0_19
# BB#17:
	decw	(%rbx)
	jne	.LBB0_19
# BB#18:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pfree
.LBB0_19:
	cmpl	$2, debug(%rip)
	jl	.LBB0_21
# BB#20:
	movq	stdout(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	movq	stdout(%rip), %rdi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.1, %esi
	movb	$1, %al
	callq	fprintf
.LBB0_21:
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	pfKnuthEx28, .Lfunc_end0-pfKnuthEx28
	.cfi_endproc

	.globl	logf_
	.p2align	4, 0x90
	.type	logf_,@function
logf_:                                  # @logf_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	testq	%rbx, %rbx
	je	.LBB1_2
# BB#1:
	incw	(%rbx)
.LBB1_2:
	movl	%edx, %edi
	callq	itop
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	pmul
	movl	%ebp, %edi
	movq	%rax, %rsi
	callq	pfKnuthEx28
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movl	%ebp, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	log
	mulsd	(%rsp), %xmm0           # 8-byte Folded Reload
	cvtsd2ss	%xmm0, %xmm0
	testq	%rbx, %rbx
	je	.LBB1_5
# BB#3:
	decw	(%rbx)
	jne	.LBB1_5
# BB#4:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movss	%xmm0, (%rsp)           # 4-byte Spill
	callq	pfree
	movss	(%rsp), %xmm0           # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
.LBB1_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	logf_, .Lfunc_end1-logf_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	3630391209              # float -9.99999986E+14
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_1:
	.quad	-4620693217682128896    # double -0.5
.LCPI2_2:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	findk
	.p2align	4, 0x90
	.type	findk,@function
findk:                                  # @findk
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 128
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movl	%edx, 40(%rsp)          # 4-byte Spill
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	incw	(%rdi)
.LBB2_2:                                # %.preheader
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	cmpl	$2, 28(%rsp)            # 4-byte Folded Reload
	movl	$0, %r12d
	jb	.LBB2_22
# BB#3:                                 # %.lr.ph69.preheader
	movss	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movl	$1, %ebp
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph69
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_10 Depth 2
                                        #     Child Loop BB2_12 Depth 2
	movl	%ebx, 44(%rsp)          # 4-byte Spill
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
	cmpl	$0, debug(%rip)
	movq	32(%rsp), %r15          # 8-byte Reload
	je	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_4 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$5, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %r14
	movl	%ebp, %edi
	callq	utop
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	pmul
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	fputp
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
.LBB2_6:                                #   in Loop: Header=BB2_4 Depth=1
	movl	%r12d, 52(%rsp)         # 4-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	movl	%eax, 24(%rsp)
	movq	%r15, %rdi
	movl	%ebp, %ebx
	movl	%ebp, %esi
	leaq	24(%rsp), %rdx
	movl	40(%rsp), %ecx          # 4-byte Reload
	callq	pfactorbase
	testq	%rax, %rax
	je	.LBB2_26
# BB#7:                                 #   in Loop: Header=BB2_4 Depth=1
	movl	24(%rsp), %ecx
	decl	%ecx
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	(%rax,%rcx,4), %r13d
	movzwl	primes(%rip), %r12d
	movzwl	%r12w, %r14d
	xorps	%xmm1, %xmm1
	cmpl	%r13d, %r14d
	ja	.LBB2_15
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB2_4 Depth=1
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB2_9
# BB#11:                                # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB2_4 Depth=1
	xorps	%xmm1, %xmm1
	movl	$primes+2, %r15d
	.p2align	4, 0x90
.LBB2_12:                               # %.lr.ph.split.us
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movq	32(%rsp), %rbp          # 8-byte Reload
	incw	(%rbp)
	movl	%ebx, %edi
	callq	itop
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	pmul
	movl	%r14d, %edi
	movq	%rax, %rsi
	callq	pfKnuthEx28
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movzwl	%r12w, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	callq	log
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	cvtsd2ss	%xmm0, %xmm0
	decw	(%rbp)
	jne	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_12 Depth=2
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	callq	pfree
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
.LBB2_14:                               # %logf_.exit.us
                                        #   in Loop: Header=BB2_12 Depth=2
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movzwl	(%r15), %r12d
	movzwl	%r12w, %r14d
	addq	$2, %r15
	cmpl	%r13d, %r14d
	jbe	.LBB2_12
	jmp	.LBB2_15
.LBB2_9:                                # %.lr.ph.split.preheader
                                        #   in Loop: Header=BB2_4 Depth=1
	xorps	%xmm1, %xmm1
	movl	$primes+2, %ebp
	.p2align	4, 0x90
.LBB2_10:                               # %.lr.ph.split
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movl	%ebx, %edi
	callq	itop
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	pmul
	movl	%r14d, %edi
	movq	%rax, %rsi
	callq	pfKnuthEx28
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movzwl	%r12w, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	callq	log
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	cvtsd2ss	%xmm0, %xmm0
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movzwl	(%rbp), %r12d
	movzwl	%r12w, %r14d
	addq	$2, %rbp
	cmpl	%r13d, %r14d
	jbe	.LBB2_10
	.p2align	4, 0x90
.LBB2_15:                               # %._crit_edge
                                        #   in Loop: Header=BB2_4 Depth=1
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movl	%ebx, %ebp
	movl	%ebp, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	callq	log
	mulsd	.LCPI2_1(%rip), %xmm0
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	cmpl	$3, verbose(%rip)
	jl	.LBB2_17
# BB#16:                                #   in Loop: Header=BB2_4 Depth=1
	movq	stdout(%rip), %rdi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.4, %esi
	movb	$1, %al
	movl	%ebp, %edx
	callq	fprintf
.LBB2_17:                               #   in Loop: Header=BB2_4 Depth=1
	cmpl	$0, debug(%rip)
	movl	52(%rsp), %r12d         # 4-byte Reload
	movl	44(%rsp), %ebx          # 4-byte Reload
	je	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_4 Depth=1
	movq	stdout(%rip), %r14
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	log
	mulsd	.LCPI2_2(%rip), %xmm0
	movl	$.L.str.5, %esi
	movb	$1, %al
	movq	%r14, %rdi
	callq	fprintf
.LBB2_19:                               #   in Loop: Header=BB2_4 Depth=1
	cmpl	$3, verbose(%rip)
	jl	.LBB2_21
# BB#20:                                #   in Loop: Header=BB2_4 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB2_21:                               #   in Loop: Header=BB2_4 Depth=1
	movss	48(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	cmoval	24(%rsp), %ebx
	maxss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	cmoval	%ebp, %r12d
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	free
	incl	%ebp
	cmpl	28(%rsp), %ebp          # 4-byte Folded Reload
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	jb	.LBB2_4
.LBB2_22:                               # %._crit_edge70
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	%ebx, (%rax)
	movq	32(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB2_25
# BB#23:
	decw	(%rdi)
	jne	.LBB2_25
# BB#24:
	xorl	%eax, %eax
	callq	pfree
.LBB2_25:
	movl	%r12d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_26:
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$38, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	findk, .Lfunc_end2-findk
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 96
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movl	%edi, %ebx
	movl	$0, 20(%rsp)
	movq	$0, 8(%rsp)
	movq	$0, 24(%rsp)
	movq	(%r13), %rax
	movq	%rax, progName(%rip)
	movl	$3, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1073741824, %r15d      # imm = 0x40000000
	xorl	%r12d, %r12d
	jmp	.LBB3_2
	.p2align	4, 0x90
.LBB3_1:                                # %.backedge
                                        #   in Loop: Header=BB3_2 Depth=1
	incl	(%rax)
	jmp	.LBB3_2
.LBB3_5:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$debug, %eax
	jmp	.LBB3_1
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movl	$.L.str.7, %edx
	xorl	%eax, %eax
	movl	%ebx, %edi
	movq	%r13, %rsi
	callq	getopt
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-97(%rax), %ecx
	cmpl	$21, %ecx
	ja	.LBB3_8
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	$verbose, %eax
	jmpq	*.LJTI3_0(,%rcx,8)
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB3_2
.LBB3_6:                                #   in Loop: Header=BB3_2 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r15
	jmp	.LBB3_2
.LBB3_7:                                #   in Loop: Header=BB3_2 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r12
	jmp	.LBB3_2
.LBB3_8:
	cmpl	$-1, %eax
	jne	.LBB3_13
# BB#9:
	movslq	optind(%rip), %r14
	subl	%r14d, %ebx
	leal	-1(%rbx), %ebp
	cmpl	$2, %ebp
	ja	.LBB3_13
# BB#10:
	movq	(%r13,%r14,8), %rdi
	callq	atop
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	testl	%ebp, %ebp
	je	.LBB3_14
# BB#11:
	movq	8(%r13,%r14,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbp
	movl	%ebp, 20(%rsp)
	cmpl	$2, %ebx
	je	.LBB3_12
# BB#15:
	movq	16(%r13,%r14,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbx
	testl	%ebx, %ebx
	movq	32(%rsp), %r14          # 8-byte Reload
	jne	.LBB3_21
# BB#16:                                # %.thread45
	testl	%r12d, %r12d
	jne	.LBB3_19
	jmp	.LBB3_17
.LBB3_13:                               # %.loopexit
	movq	stderr(%rip), %rdi
	movq	progName(%rip), %rdx
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %ebx
	jmp	.LBB3_30
.LBB3_14:
	xorl	%ebp, %ebp
.LBB3_12:
	movq	32(%rsp), %r14          # 8-byte Reload
	testl	%r12d, %r12d
	jne	.LBB3_19
.LBB3_17:
	shrl	%ebp
	movl	%ebp, %r12d
	addl	$5, %r12d
	cmpl	$0, verbose(%rip)
	je	.LBB3_19
# BB#18:
	movq	stdout(%rip), %rdi
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movl	%r12d, %edx
	callq	fprintf
.LBB3_19:
	movq	8(%rsp), %rdi
	leaq	20(%rsp), %rsi
	movl	%r14d, %edx
	movl	%r12d, %ecx
	callq	findk
	movl	%eax, %ebx
	cmpl	$0, verbose(%rip)
	je	.LBB3_21
# BB#20:
	movq	stdout(%rip), %rdi
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
.LBB3_21:
	movl	%r15d, 4(%rsp)
	movl	20(%rsp), %edi
	movl	%ebx, %esi
	movl	%r14d, %edx
	callq	pcfracInit
	movq	8(%rsp), %rdi
	leaq	4(%rsp), %rsi
	callq	pcfrac
	leaq	24(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	subl	4(%rsp), %r15d
	movl	%r15d, 4(%rsp)
	cmpl	$0, verbose(%rip)
	je	.LBB3_23
# BB#22:
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rdi
	movl	4(%rsp), %edx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	movl	cfracNabort(%rip), %edx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	movl	cfracTsolns(%rip), %edx
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	movl	cfracT2solns(%rip), %edx
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	movl	cfracPsolns(%rip), %edx
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rdi
	movl	cfracFsolns(%rip), %edx
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB3_23:
	cmpq	$0, 24(%rsp)
	je	.LBB3_27
# BB#24:
	movq	stdout(%rip), %rdi
	movq	8(%rsp), %rsi
	callq	fputp
	movq	stdout(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	movq	24(%rsp), %rsi
	callq	fputp
	movq	stdout(%rip), %rcx
	movl	$.L.str.18, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%rsp), %rdi
	movq	24(%rsp), %rsi
	leaq	8(%rsp), %rdx
	xorl	%ecx, %ecx
	callq	pdivmod
	movq	stdout(%rip), %rdi
	movq	8(%rsp), %rsi
	callq	fputp
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_27
# BB#25:
	decw	(%rdi)
	jne	.LBB3_27
# BB#26:
	xorl	%eax, %eax
	callq	pfree
.LBB3_27:                               # %.thread46
	movq	8(%rsp), %rdi
	xorl	%ebx, %ebx
	testq	%rdi, %rdi
	je	.LBB3_30
# BB#28:
	decw	(%rdi)
	jne	.LBB3_30
# BB#29:
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	callq	pfree
.LBB3_30:
	movl	%ebx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_4
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_5
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_6
	.quad	.LBB3_13
	.quad	.LBB3_7
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_13
	.quad	.LBB3_1

	.type	verbose,@object         # @verbose
	.bss
	.globl	verbose
	.p2align	2
verbose:
	.long	0                       # 0x0
	.size	verbose, 4

	.type	debug,@object           # @debug
	.globl	debug
	.p2align	2
debug:
	.long	0                       # 0x0
	.size	debug, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"f(%u,"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"d) = %9.7f\n"
	.size	.L.str.1, 12

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"kN = "
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"couldn't compute factor base in findk\n"
	.size	.L.str.3, 39

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%u: %5.2f"
	.size	.L.str.4, 10

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" log(k)/2=%5.2f"
	.size	.L.str.5, 16

	.type	progName,@object        # @progName
	.comm	progName,8,8
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"a:k:i:dv"
	.size	.L.str.7, 9

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"usage: %s [-dv] [-a aborts ] [-k maxk ] [-i maxCount ] n [[ m ] k ]\n"
	.size	.L.str.8, 69

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"maxk = %u\n"
	.size	.L.str.9, 11

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"k = %u\n"
	.size	.L.str.10, 8

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Iterations     : %u\n"
	.size	.L.str.11, 21

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Early Aborts   : %u\n"
	.size	.L.str.12, 21

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Total Partials : %u\n"
	.size	.L.str.13, 21

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Used  Partials : %u\n"
	.size	.L.str.14, 21

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Full Solutions : %u\n"
	.size	.L.str.15, 21

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Factor Attempts: %u\n"
	.size	.L.str.16, 21

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	" = "
	.size	.L.str.17, 4

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	" * "
	.size	.L.str.18, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
