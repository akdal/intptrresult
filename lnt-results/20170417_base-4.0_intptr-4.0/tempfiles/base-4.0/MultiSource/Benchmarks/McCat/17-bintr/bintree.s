	.text
	.file	"bintree.bc"
	.globl	createBinaryTree
	.p2align	4, 0x90
	.type	createBinaryTree,@function
createBinaryTree:                       # @createBinaryTree
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movl	$24, %edi
	callq	malloc
	movl	%ebx, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	createBinaryTree, .Lfunc_end0-createBinaryTree
	.cfi_endproc

	.globl	printBinaryTree
	.p2align	4, 0x90
	.type	printBinaryTree,@function
printBinaryTree:                        # @printBinaryTree
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB1_1
# BB#2:
	movl	(%rbx), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movq	8(%rbx), %rdi
	callq	printBinaryTree
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movq	16(%rbx), %rdi
	callq	printBinaryTree
	movl	$.Lstr, %edi
	popq	%rbx
	jmp	puts                    # TAILCALL
.LBB1_1:
	movl	$88, %edi
	popq	%rbx
	jmp	putchar                 # TAILCALL
.Lfunc_end1:
	.size	printBinaryTree, .Lfunc_end1-printBinaryTree
	.cfi_endproc

	.globl	printSortedBinaryTree
	.p2align	4, 0x90
	.type	printSortedBinaryTree,@function
printSortedBinaryTree:                  # @printSortedBinaryTree
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB2_3
	.p2align	4, 0x90
.LBB2_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	printSortedBinaryTree
	movl	(%rbx), %esi
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_1
.LBB2_3:                                # %tailrecurse._crit_edge
	popq	%rbx
	retq
.Lfunc_end2:
	.size	printSortedBinaryTree, .Lfunc_end2-printSortedBinaryTree
	.cfi_endproc

	.globl	insertSortedBinaryTree
	.p2align	4, 0x90
	.type	insertSortedBinaryTree,@function
insertSortedBinaryTree:                 # @insertSortedBinaryTree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$24, %edi
	callq	malloc
	movl	%ebp, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB3_7
	.p2align	4, 0x90
.LBB3_1:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rbx
	cmpl	%ebp, (%rbx)
	jle	.LBB3_4
# BB#2:                                 #   in Loop: Header=BB3_1 Depth=1
	movq	8(%rbx), %rcx
	testq	%rcx, %rcx
	jne	.LBB3_1
	jmp	.LBB3_3
	.p2align	4, 0x90
.LBB3_4:                                #   in Loop: Header=BB3_1 Depth=1
	jge	.LBB3_8
# BB#5:                                 #   in Loop: Header=BB3_1 Depth=1
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	jne	.LBB3_1
# BB#6:
	addq	$16, %rbx
	jmp	.LBB3_7
.LBB3_3:
	addq	$8, %rbx
.LBB3_7:                                # %.thread.sink.split
	movq	%rax, (%rbx)
.LBB3_8:                                # %.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end3:
	.size	insertSortedBinaryTree, .Lfunc_end3-insertSortedBinaryTree
	.cfi_endproc

	.globl	getArithmeticMeanBinaryTree
	.p2align	4, 0x90
	.type	getArithmeticMeanBinaryTree,@function
getArithmeticMeanBinaryTree:            # @getArithmeticMeanBinaryTree
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	getSumBinaryTree
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	getSizeBinaryTree
	cvtsi2sdl	%eax, %xmm1
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end4:
	.size	getArithmeticMeanBinaryTree, .Lfunc_end4-getArithmeticMeanBinaryTree
	.cfi_endproc

	.globl	getSumBinaryTree
	.p2align	4, 0x90
	.type	getSumBinaryTree,@function
getSumBinaryTree:                       # @getSumBinaryTree
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB5_2
# BB#1:
	cvtsi2sdl	(%rbx), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	8(%rbx), %rdi
	callq	getSumBinaryTree
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	16(%rbx), %rdi
	callq	getSumBinaryTree
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	jmp	.LBB5_3
.LBB5_2:
	xorpd	%xmm0, %xmm0
.LBB5_3:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end5:
	.size	getSumBinaryTree, .Lfunc_end5-getSumBinaryTree
	.cfi_endproc

	.globl	getSizeBinaryTree
	.p2align	4, 0x90
	.type	getSizeBinaryTree,@function
getSizeBinaryTree:                      # @getSizeBinaryTree
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -24
.Lcfi21:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	.LBB6_3
	.p2align	4, 0x90
.LBB6_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	getSizeBinaryTree
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	16(%rbx), %rbx
	leal	1(%r14,%rax), %r14d
	testq	%rbx, %rbx
	jne	.LBB6_1
.LBB6_3:                                # %tailrecurse._crit_edge
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	getSizeBinaryTree, .Lfunc_end6-getSizeBinaryTree
	.cfi_endproc

	.globl	getArithmeticMeanOptimized
	.p2align	4, 0x90
	.type	getArithmeticMeanOptimized,@function
getArithmeticMeanOptimized:             # @getArithmeticMeanOptimized
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 32
	movq	$0, 16(%rsp)
	movq	$0, 8(%rsp)
	leaq	16(%rsp), %rsi
	leaq	8(%rsp), %rdx
	callq	getArithmeticMeanOptimizedRecurs
	movsd	16(%rsp), %xmm0         # xmm0 = mem[0],zero
	divsd	8(%rsp), %xmm0
	addq	$24, %rsp
	retq
.Lfunc_end7:
	.size	getArithmeticMeanOptimized, .Lfunc_end7-getArithmeticMeanOptimized
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	getArithmeticMeanOptimizedRecurs
	.p2align	4, 0x90
	.type	getArithmeticMeanOptimizedRecurs,@function
getArithmeticMeanOptimizedRecurs:       # @getArithmeticMeanOptimizedRecurs
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB8_3
	.p2align	4, 0x90
.LBB8_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	(%rbx), %xmm0
	addsd	(%r15), %xmm0
	movsd	%xmm0, (%r15)
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	addsd	.LCPI8_0(%rip), %xmm0
	movsd	%xmm0, (%r14)
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	getArithmeticMeanOptimizedRecurs
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_1
.LBB8_3:                                # %tailrecurse._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	getArithmeticMeanOptimizedRecurs, .Lfunc_end8-getArithmeticMeanOptimizedRecurs
	.cfi_endproc

	.globl	memberOfBinaryTree
	.p2align	4, 0x90
	.type	memberOfBinaryTree,@function
memberOfBinaryTree:                     # @memberOfBinaryTree
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 16
	movl	$0, 4(%rsp)
	leaq	4(%rsp), %rdx
	callq	memberOfBinaryTreeRecurs
	movl	4(%rsp), %eax
	popq	%rcx
	retq
.Lfunc_end9:
	.size	memberOfBinaryTree, .Lfunc_end9-memberOfBinaryTree
	.cfi_endproc

	.globl	memberOfBinaryTreeRecurs
	.p2align	4, 0x90
	.type	memberOfBinaryTreeRecurs,@function
memberOfBinaryTreeRecurs:               # @memberOfBinaryTreeRecurs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -32
.Lcfi34:
	.cfi_offset %r14, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB10_2
	jmp	.LBB10_4
	.p2align	4, 0x90
.LBB10_6:                               # %tailrecurse
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB10_4
.LBB10_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%ebp, (%rbx)
	je	.LBB10_3
# BB#5:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	8(%rbx), %rdi
	movl	%ebp, %esi
	movq	%r14, %rdx
	callq	memberOfBinaryTreeRecurs
	cmpl	$0, (%r14)
	je	.LBB10_6
	jmp	.LBB10_4
.LBB10_3:
	movl	$1, (%r14)
.LBB10_4:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end10:
	.size	memberOfBinaryTreeRecurs, .Lfunc_end10-memberOfBinaryTreeRecurs
	.cfi_endproc

	.globl	memberOfSortedBinaryTree
	.p2align	4, 0x90
	.type	memberOfSortedBinaryTree,@function
memberOfSortedBinaryTree:               # @memberOfSortedBinaryTree
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	jne	.LBB11_2
	jmp	.LBB11_5
	.p2align	4, 0x90
.LBB11_6:                               # %.us-lcssa15.us
                                        #   in Loop: Header=BB11_2 Depth=1
	addq	$8, %rdi
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB11_5
.LBB11_2:                               # %.outer.split.us.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%esi, (%rdi)
	jg	.LBB11_6
# BB#3:                                 #   in Loop: Header=BB11_2 Depth=1
	jge	.LBB11_4
# BB#8:                                 # %.us-lcssa16.us
                                        #   in Loop: Header=BB11_2 Depth=1
	addq	$16, %rdi
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB11_2
	jmp	.LBB11_5
.LBB11_4:
	movl	$1, %eax
.LBB11_5:                               # %.us-lcssa.us
	retq
.Lfunc_end11:
	.size	memberOfSortedBinaryTree, .Lfunc_end11-memberOfSortedBinaryTree
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"(%d <L "
	.size	.L.str.1, 8

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" L> <R "
	.size	.L.str.2, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%d "
	.size	.L.str.4, 4

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	" R>)"
	.size	.Lstr, 5


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
