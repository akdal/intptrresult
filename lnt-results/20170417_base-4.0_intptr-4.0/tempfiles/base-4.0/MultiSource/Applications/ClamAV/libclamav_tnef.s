	.text
	.file	"libclamav_tnef.bc"
	.globl	cli_tnef
	.p2align	4, 0x90
	.type	cli_tnef,@function
cli_tnef:                               # @cli_tnef
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$8408, %rsp             # imm = 0x20D8
.Lcfi6:
	.cfi_def_cfa_offset 8464
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %r12d
	movq	%rdi, %rbp
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r12d, %edi
	callq	lseek
	leaq	64(%rsp), %rdx
	movl	$1, %edi
	movl	%r12d, %esi
	callq	__fxstat
	testl	%eax, %eax
	js	.LBB0_52
# BB#1:
	movq	112(%rsp), %r14
	movl	%r12d, %edi
	callq	dup
	movl	%eax, %ebx
	movl	$.L.str.1, %esi
	movl	%ebx, %edi
	callq	fdopen
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_54
# BB#2:
	leaq	48(%rsp), %rdi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB0_53
# BB#3:
	cmpl	$574529400, 48(%rsp)    # imm = 0x223E9F78
	jne	.LBB0_55
# BB#4:
	leaq	52(%rsp), %rdi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB0_53
# BB#5:                                 # %.preheader
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movb	$0, 7(%rsp)
	leaq	7(%rsp), %rdi
	movl	$1, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB0_56
# BB#6:                                 # %.preheader
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movb	7(%rsp), %bl
	testb	%bl, %bl
	movl	$0, %r15d
	je	.LBB0_57
# BB#7:                                 # %.lr.ph183
	leaq	208(%rsp), %rbp
	xorl	%r15d, %r15d
.LBB0_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_39 Depth 2
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbp, %rdi
	movq	%r13, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB0_59
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=1
	movl	208(%rsp), %r15d
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbp, %rdi
	movq	%r13, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB0_62
# BB#10:                                #   in Loop: Header=BB0_8 Depth=1
	movq	%r13, %r14
	movl	%r15d, %ebp
	shrl	$16, %ebp
	movl	208(%rsp), %r13d
	movzwl	%r15w, %esi
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movl	%ebp, %edx
	movl	%r13d, %ecx
	callq	cli_dbgmsg
	movq	%r13, %rsi
	testq	%rsi, %rsi
	je	.LBB0_27
# BB#11:                                #   in Loop: Header=BB0_8 Depth=1
	testl	%esi, %esi
	movq	%r14, %r13
	js	.LBB0_66
# BB#12:                                #   in Loop: Header=BB0_8 Depth=1
	testb	%bl, %bl
	je	.LBB0_28
# BB#13:                                #   in Loop: Header=BB0_8 Depth=1
	movslq	%esi, %r14
	cmpb	$2, %bl
	je	.LBB0_29
# BB#14:                                #   in Loop: Header=BB0_8 Depth=1
	cmpb	$1, %bl
	jne	.LBB0_67
# BB#15:                                #   in Loop: Header=BB0_8 Depth=1
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_8 Depth=1
	callq	fileblobDestroy
.LBB0_17:                               #   in Loop: Header=BB0_8 Depth=1
	callq	fileblobCreate
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	%ebp, %edx
	movl	%ebx, %ecx
	callq	cli_dbgmsg
	movq	%r13, %rdi
	callq	ftell
	movq	%rax, %r14
	movzwl	%r15w, %eax
	cmpl	$32780, %eax            # imm = 0x800C
	jne	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_8 Depth=1
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB0_19:                               #   in Loop: Header=BB0_8 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	jle	.LBB0_63
# BB#20:                                #   in Loop: Header=BB0_8 Depth=1
	testl	%ebx, %ebx
	js	.LBB0_63
# BB#21:                                #   in Loop: Header=BB0_8 Depth=1
	cmpq	%rax, 24(%rsp)          # 8-byte Folded Reload
	jg	.LBB0_63
# BB#22:                                #   in Loop: Header=BB0_8 Depth=1
	testq	%r14, %r14
	js	.LBB0_63
# BB#23:                                #   in Loop: Header=BB0_8 Depth=1
	addq	24(%rsp), %r14          # 8-byte Folded Reload
	cmpq	%rax, %r14
	jg	.LBB0_63
# BB#24:                                #   in Loop: Header=BB0_8 Depth=1
	testq	%r14, %r14
	js	.LBB0_63
# BB#25:                                #   in Loop: Header=BB0_8 Depth=1
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	fseek
	testl	%eax, %eax
	movq	8(%rsp), %r15           # 8-byte Reload
	js	.LBB0_64
# BB#26:                                # %tnef_message.exit
                                        #   in Loop: Header=BB0_8 Depth=1
	movl	$2, %esi
	movl	$1, %edx
	leaq	208(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%r13, %rcx
	callq	fread
	cmpq	$1, %rax
	je	.LBB0_50
	jmp	.LBB0_64
.LBB0_27:                               #   in Loop: Header=BB0_8 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	leaq	208(%rsp), %rbp
	movq	%r14, %r13
	jmp	.LBB0_50
.LBB0_28:                               #   in Loop: Header=BB0_8 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	leaq	208(%rsp), %rbp
	jmp	.LBB0_50
.LBB0_29:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movq	%rsi, %rbx
	callq	cli_dbgmsg
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	callq	cli_dbgmsg
	movq	%r13, %rdi
	callq	ftell
	movq	%rax, %rbx
	movzwl	%r15w, %eax
	cmpl	$32783, %eax            # imm = 0x800F
	je	.LBB0_36
# BB#30:                                #   in Loop: Header=BB0_8 Depth=1
	cmpl	$32784, %eax            # imm = 0x8010
	movq	8(%rsp), %r15           # 8-byte Reload
	jne	.LBB0_42
# BB#31:                                #   in Loop: Header=BB0_8 Depth=1
	movq	%r14, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	1(%r14), %rdi
	callq	cli_malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_76
# BB#32:                                #   in Loop: Header=BB0_8 Depth=1
	movq	%rbx, %rbp
	movl	$1, %esi
	movq	%r14, %rdi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r13, %rcx
	callq	fread
	cmpq	%rbx, %rax
	jne	.LBB0_84
# BB#33:                                #   in Loop: Header=BB0_8 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movb	$0, (%r14,%rax)
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	testq	%r15, %r15
	movq	%rbp, %rbx
	jne	.LBB0_35
# BB#34:                                #   in Loop: Header=BB0_8 Depth=1
	callq	fileblobCreate
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_85
.LBB0_35:                               #   in Loop: Header=BB0_8 Depth=1
	movq	%r15, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	callq	fileblobSetFilename
	movq	%r14, %rdi
	callq	free
	leaq	208(%rsp), %rbp
	movq	24(%rsp), %r14          # 8-byte Reload
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	jg	.LBB0_43
	jmp	.LBB0_65
.LBB0_36:                               #   in Loop: Header=BB0_8 Depth=1
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r15           # 8-byte Reload
	testq	%r15, %r15
	jne	.LBB0_38
# BB#37:                                #   in Loop: Header=BB0_8 Depth=1
	callq	fileblobCreate
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_86
.LBB0_38:                               # %.lr.ph.i
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ebx
	leaq	208(%rsp), %rbp
.LBB0_39:                               #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	fgetc
	movl	%eax, 208(%rsp)
	cmpl	$-1, %eax
	je	.LBB0_41
# BB#40:                                # %.critedge.i
                                        #   in Loop: Header=BB0_39 Depth=2
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	fileblobAddData
	decl	%ebx
	jne	.LBB0_39
.LBB0_41:                               #   in Loop: Header=BB0_8 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	jg	.LBB0_43
	jmp	.LBB0_65
.LBB0_42:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	%ebp, %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	cli_dbgmsg
	leaq	208(%rsp), %rbp
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	jle	.LBB0_65
.LBB0_43:                               #   in Loop: Header=BB0_8 Depth=1
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	js	.LBB0_65
# BB#44:                                #   in Loop: Header=BB0_8 Depth=1
	cmpq	40(%rsp), %r14          # 8-byte Folded Reload
	jg	.LBB0_65
# BB#45:                                #   in Loop: Header=BB0_8 Depth=1
	testq	%rbx, %rbx
	js	.LBB0_65
# BB#46:                                #   in Loop: Header=BB0_8 Depth=1
	addq	%r14, %rbx
	cmpq	40(%rsp), %rbx          # 8-byte Folded Reload
	jg	.LBB0_65
# BB#47:                                #   in Loop: Header=BB0_8 Depth=1
	testq	%rbx, %rbx
	js	.LBB0_65
# BB#48:                                #   in Loop: Header=BB0_8 Depth=1
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	fseek
	testl	%eax, %eax
	js	.LBB0_76
# BB#49:                                # %tnef_attachment.exit
                                        #   in Loop: Header=BB0_8 Depth=1
	movl	$2, %esi
	movl	$1, %edx
	leaq	54(%rsp), %rdi
	movq	%r13, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB0_76
.LBB0_50:                               #   in Loop: Header=BB0_8 Depth=1
	movb	$0, 7(%rsp)
	movl	$1, %esi
	movl	$1, %edx
	leaq	7(%rsp), %rdi
	movq	%r13, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB0_57
# BB#51:                                #   in Loop: Header=BB0_8 Depth=1
	movb	7(%rsp), %bl
	testb	%bl, %bl
	jne	.LBB0_8
	jmp	.LBB0_57
.LBB0_52:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	cli_errmsg
	movl	$-123, %ebp
	jmp	.LBB0_83
.LBB0_53:
	movq	%r13, %rdi
	callq	fclose
	movl	$-123, %ebp
	jmp	.LBB0_83
.LBB0_54:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	cli_errmsg
	movl	%ebx, %edi
	callq	close
	movl	$-115, %ebp
	jmp	.LBB0_83
.LBB0_55:
	movq	%r13, %rdi
	callq	fclose
	movl	$-124, %ebp
	jmp	.LBB0_83
.LBB0_56:
	xorl	%r15d, %r15d
.LBB0_57:                               # %.loopexit
	movq	%r13, %rdi
	callq	ferror
	testl	%eax, %eax
	jne	.LBB0_75
# BB#58:
	xorl	%ebp, %ebp
	jmp	.LBB0_78
.LBB0_59:
	cmpb	$10, %bl
	jne	.LBB0_62
# BB#60:
	movq	%r13, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB0_62
# BB#61:
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB0_57
.LBB0_62:                               # %.loopexit108
	movl	$-123, %ebp
	jmp	.LBB0_74
.LBB0_63:
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	8(%rsp), %r15           # 8-byte Reload
.LBB0_64:                               # %.loopexit110
	movl	$.L.str.6, %edi
	jmp	.LBB0_77
.LBB0_65:
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB0_76:                               # %.loopexit109
	movl	$.L.str.8, %edi
.LBB0_77:                               # %.thread101
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-124, %ebp
	jmp	.LBB0_78
.LBB0_66:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_warnmsg
	movl	$-124, %ebp
	jmp	.LBB0_74
.LBB0_67:
	movzbl	%bl, %esi
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movl	16(%rsp), %edx          # 4-byte Reload
	callq	cli_warnmsg
	movl	$-124, %ebp
	cmpb	$0, cli_debug_flag(%rip)
	je	.LBB0_74
# BB#68:
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r14
	movl	$705, %esi              # imm = 0x2C1
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	open
	movl	%eax, %r15d
	testl	%r15d, %r15d
	js	.LBB0_73
# BB#69:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_warnmsg
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r12d, %edi
	callq	lseek
	leaq	208(%rsp), %rsi
	movl	$8192, %edx             # imm = 0x2000
	movl	%r12d, %edi
	callq	cli_readn
	testl	%eax, %eax
	jle	.LBB0_72
# BB#70:                                # %.lr.ph.preheader
	leaq	208(%rsp), %rbx
.LBB0_71:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%r15d, %edi
	movq	%rbx, %rsi
	movl	%eax, %edx
	callq	cli_writen
	movl	$8192, %edx             # imm = 0x2000
	movl	%r12d, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	testl	%eax, %eax
	jg	.LBB0_71
.LBB0_72:                               # %._crit_edge
	movl	%r15d, %edi
	callq	close
.LBB0_73:
	movq	%r14, %rdi
	callq	free
.LBB0_74:                               # %.thread101
	movq	8(%rsp), %r15           # 8-byte Reload
.LBB0_78:                               # %.thread101
	movq	%r13, %rdi
	callq	fclose
	testq	%r15, %r15
	je	.LBB0_82
# BB#79:
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	fileblobGetFilename
	testq	%rax, %rax
	jne	.LBB0_81
# BB#80:
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.13, %edx
	movq	%r15, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	fileblobSetFilename
.LBB0_81:
	movq	%r15, %rdi
	callq	fileblobDestroy
.LBB0_82:
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
.LBB0_83:
	movl	%ebp, %eax
	addq	$8408, %rsp             # imm = 0x20D8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_75:
	movl	$.L.str.3, %edi
	callq	perror
	movl	$-123, %ebp
	jmp	.LBB0_78
.LBB0_84:
	movq	%r14, %rdi
	callq	free
	jmp	.LBB0_76
.LBB0_85:
	movq	%r14, %rdi
	callq	free
.LBB0_86:
	xorl	%r15d, %r15d
	jmp	.LBB0_76
.Lfunc_end0:
	.size	cli_tnef, .Lfunc_end0-cli_tnef
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Can't fstat descriptor %d\n"
	.size	.L.str, 27

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"rb"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Can't open descriptor %d\n"
	.size	.L.str.2, 26

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"read"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Corrupt TNEF header detected - length %d\n"
	.size	.L.str.4, 42

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"TNEF - found message\n"
	.size	.L.str.5, 22

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Error reading TNEF message\n"
	.size	.L.str.6, 28

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"TNEF - found attachment\n"
	.size	.L.str.7, 25

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Error reading TNEF attachment\n"
	.size	.L.str.8, 31

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"TNEF - unknown level %d tag 0x%x\n"
	.size	.L.str.9, 34

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Saving dump to %s:  refer to http://www.clamav.net/bugs\n"
	.size	.L.str.10, 57

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"cli_tnef: flushing final data\n"
	.size	.L.str.11, 31

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Saving TNEF portion with an unknown name\n"
	.size	.L.str.12, 42

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"tnef"
	.size	.L.str.13, 5

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"cli_tnef: returning %d\n"
	.size	.L.str.14, 24

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"message tag 0x%x, type 0x%x, length %d\n"
	.size	.L.str.15, 40

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"TNEF body not being scanned - if you believe this file contains a virus, submit it to www.clamav.net\n"
	.size	.L.str.16, 102

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"TNEF: Incorrect length field in tnef_message\n"
	.size	.L.str.17, 46

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"attachment tag 0x%x, type 0x%x, length %d\n"
	.size	.L.str.18, 43

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"TNEF filename %s\n"
	.size	.L.str.19, 18

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"TNEF - unsupported attachment tag 0x%x type 0x%d length %d\n"
	.size	.L.str.20, 60

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"TNEF: Incorrect length field in tnef_attachment\n"
	.size	.L.str.21, 49

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"tnef_header: ignoring trailing newline\n"
	.size	.L.str.22, 40


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
