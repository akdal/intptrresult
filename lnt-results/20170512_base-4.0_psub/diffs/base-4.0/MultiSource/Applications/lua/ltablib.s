	.text
	.file	"ltablib.bc"
	.globl	luaopen_table
	.p2align	4, 0x90
	.type	luaopen_table,@function
luaopen_table:                          # @luaopen_table
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$.L.str, %esi
	movl	$tab_funcs, %edx
	callq	luaL_register
	movl	$1, %eax
	popq	%rcx
	retq
.Lfunc_end0:
	.size	luaopen_table, .Lfunc_end0-luaopen_table
	.cfi_endproc

	.p2align	4, 0x90
	.type	tconcat,@function
tconcat:                                # @tconcat
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 48
	subq	$8224, %rsp             # imm = 0x2020
.Lcfi6:
	.cfi_def_cfa_offset 8272
.Lcfi7:
	.cfi_offset %rbx, -48
.Lcfi8:
	.cfi_offset %r12, -40
.Lcfi9:
	.cfi_offset %r13, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movq	%rsp, %rcx
	movl	$2, %esi
	movl	$.L.str.10, %edx
	callq	luaL_optlstring
	movq	%rax, %r15
	movl	$1, %esi
	movl	$5, %edx
	movq	%r12, %rdi
	callq	luaL_checktype
	movl	$3, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	luaL_optinteger
	movq	%rax, %rbx
	movl	$4, %esi
	movq	%r12, %rdi
	callq	lua_type
	testl	%eax, %eax
	jle	.LBB1_1
# BB#2:
	movl	$4, %esi
	movq	%r12, %rdi
	callq	luaL_checkinteger
	jmp	.LBB1_3
.LBB1_1:
	movl	$1, %esi
	movq	%r12, %rdi
	callq	lua_objlen
.LBB1_3:
	movq	%rax, %r14
	leaq	8(%rsp), %rsi
	movq	%r12, %rdi
	callq	luaL_buffinit
	cmpl	%r14d, %ebx
	jge	.LBB1_8
# BB#4:                                 # %.lr.ph.preheader
	leaq	8(%rsp), %r13
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%ebx, %edx
	callq	lua_rawgeti
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	lua_isstring
	testl	%eax, %eax
	jne	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_5 Depth=1
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	lua_type
	movq	%r12, %rdi
	movl	%eax, %esi
	callq	lua_typename
	movq	%rax, %rcx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %rdx
	movl	%ebx, %ecx
	callq	luaL_error
.LBB1_7:                                # %addfield.exit
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%r13, %rdi
	callq	luaL_addvalue
	movq	(%rsp), %rdx
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	luaL_addlstring
	incl	%ebx
	cmpl	%ebx, %r14d
	jne	.LBB1_5
	jmp	.LBB1_9
.LBB1_8:                                # %._crit_edge
	jne	.LBB1_12
.LBB1_9:                                # %._crit_edge.thread
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%r14d, %edx
	callq	lua_rawgeti
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	lua_isstring
	testl	%eax, %eax
	jne	.LBB1_11
# BB#10:
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	lua_type
	movq	%r12, %rdi
	movl	%eax, %esi
	callq	lua_typename
	movq	%rax, %rcx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %rdx
	movl	%r14d, %ecx
	callq	luaL_error
.LBB1_11:                               # %addfield.exit20
	leaq	8(%rsp), %rdi
	callq	luaL_addvalue
.LBB1_12:
	leaq	8(%rsp), %rdi
	callq	luaL_pushresult
	movl	$1, %eax
	addq	$8224, %rsp             # imm = 0x2020
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	tconcat, .Lfunc_end1-tconcat
	.cfi_endproc

	.p2align	4, 0x90
	.type	foreach,@function
foreach:                                # @foreach
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movl	$2, %esi
	movl	$6, %edx
	movq	%rbx, %rdi
	callq	luaL_checktype
	movq	%rbx, %rdi
	callq	lua_pushnil
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_next
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jne	.LBB2_2
	jmp	.LBB2_4
	.p2align	4, 0x90
.LBB2_5:                                #   in Loop: Header=BB2_2 Depth=1
	movl	$-3, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_next
	testl	%eax, %eax
	je	.LBB2_4
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-3, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-3, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_call
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	testl	%eax, %eax
	je	.LBB2_5
# BB#3:
	movl	$1, %ebp
.LBB2_4:                                # %._crit_edge
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end2:
	.size	foreach, .Lfunc_end2-foreach
	.cfi_endproc

	.p2align	4, 0x90
	.type	foreachi,@function
foreachi:                               # @foreachi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 48
.Lcfi22:
	.cfi_offset %rbx, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$1, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movl	$1, %esi
	movq	%r14, %rdi
	callq	lua_objlen
	movq	%rax, %rbx
	movl	$2, %esi
	movl	$6, %edx
	movq	%r14, %rdi
	callq	luaL_checktype
	testl	%ebx, %ebx
	jle	.LBB3_4
# BB#1:                                 # %.lr.ph.preheader
	movslq	%ebx, %r15
	movl	$1, %ebx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$2, %esi
	movq	%r14, %rdi
	callq	lua_pushvalue
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	lua_pushinteger
	movl	$1, %esi
	movq	%r14, %rdi
	movl	%ebx, %edx
	callq	lua_rawgeti
	movl	$2, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	lua_call
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	lua_type
	testl	%eax, %eax
	jne	.LBB3_5
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	$-2, %esi
	movq	%r14, %rdi
	callq	lua_settop
	cmpq	%r15, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB3_2
.LBB3_4:
	xorl	%ebp, %ebp
.LBB3_5:                                # %._crit_edge
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	foreachi, .Lfunc_end3-foreachi
	.cfi_endproc

	.p2align	4, 0x90
	.type	getn,@function
getn:                                   # @getn
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 16
.Lcfi27:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_objlen
	movslq	%eax, %rsi
	movq	%rbx, %rdi
	callq	lua_pushinteger
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	getn, .Lfunc_end4-getn
	.cfi_endproc

	.p2align	4, 0x90
	.type	maxn,@function
maxn:                                   # @maxn
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movq	%rbx, %rdi
	callq	lua_pushnil
	xorpd	%xmm0, %xmm0
	jmp	.LBB5_1
	.p2align	4, 0x90
.LBB5_4:                                #   in Loop: Header=BB5_1 Depth=1
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_tonumber
	maxsd	8(%rsp), %xmm0          # 8-byte Folded Reload
.LBB5_1:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_2 Depth 2
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_2:                                #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_next
	testl	%eax, %eax
	je	.LBB5_5
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=2
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	cmpl	$3, %eax
	jne	.LBB5_2
	jmp	.LBB5_4
.LBB5_5:
	movq	%rbx, %rdi
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	lua_pushnumber
	movl	$1, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end5:
	.size	maxn, .Lfunc_end5-maxn
	.cfi_endproc

	.p2align	4, 0x90
	.type	tinsert,@function
tinsert:                                # @tinsert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 48
.Lcfi36:
	.cfi_offset %rbx, -48
.Lcfi37:
	.cfi_offset %r12, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$1, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movl	$1, %esi
	movq	%r14, %rdi
	callq	lua_objlen
	movq	%rax, %r12
	leal	1(%r12), %ebx
	movq	%r14, %rdi
	callq	lua_gettop
	cmpl	$2, %eax
	je	.LBB6_6
# BB#1:
	cmpl	$3, %eax
	jne	.LBB6_7
# BB#2:
	movl	$2, %esi
	movq	%r14, %rdi
	callq	luaL_checkinteger
	movq	%rax, %r15
	cmpl	%r12d, %r15d
	jg	.LBB6_5
# BB#3:                                 # %.lr.ph.preheader
	cmpl	%ebx, %r15d
	cmovgel	%r15d, %ebx
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rbx), %ebp
	movl	$1, %esi
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	lua_rawgeti
	movl	$1, %esi
	movq	%r14, %rdi
	movl	%ebx, %edx
	callq	lua_rawseti
	cmpl	%r15d, %ebp
	movl	%ebp, %ebx
	jg	.LBB6_4
.LBB6_5:
	movl	%r15d, %ebx
.LBB6_6:
	movl	$1, %esi
	movq	%r14, %rdi
	movl	%ebx, %edx
	callq	lua_rawseti
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_7:
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	luaL_error              # TAILCALL
.Lfunc_end6:
	.size	tinsert, .Lfunc_end6-tinsert
	.cfi_endproc

	.p2align	4, 0x90
	.type	tremove,@function
tremove:                                # @tremove
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 48
.Lcfi46:
	.cfi_offset %rbx, -48
.Lcfi47:
	.cfi_offset %r12, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movl	$1, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movl	$1, %esi
	movq	%r12, %rdi
	callq	lua_objlen
	movq	%rax, %r14
	movslq	%r14d, %rdx
	movl	$2, %esi
	movq	%r12, %rdi
	callq	luaL_optinteger
	movq	%rax, %rbp
	xorl	%r15d, %r15d
	testl	%ebp, %ebp
	jle	.LBB7_5
# BB#1:
	cmpl	%r14d, %ebp
	jg	.LBB7_5
# BB#2:
	movl	$1, %r15d
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%ebp, %edx
	callq	lua_rawgeti
	cmpl	%r14d, %ebp
	jge	.LBB7_4
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rbp), %ebx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%ebx, %edx
	callq	lua_rawgeti
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%ebp, %edx
	callq	lua_rawseti
	cmpl	%ebx, %r14d
	movl	%ebx, %ebp
	jne	.LBB7_3
.LBB7_4:                                # %._crit_edge
	movq	%r12, %rdi
	callq	lua_pushnil
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%r14d, %edx
	callq	lua_rawseti
.LBB7_5:
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	tremove, .Lfunc_end7-tremove
	.cfi_endproc

	.p2align	4, 0x90
	.type	setn,@function
setn:                                   # @setn
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 16
.Lcfi52:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end8:
	.size	setn, .Lfunc_end8-setn
	.cfi_endproc

	.p2align	4, 0x90
	.type	sort,@function
sort:                                   # @sort
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 32
.Lcfi56:
	.cfi_offset %rbx, -24
.Lcfi57:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_objlen
	movq	%rax, %r14
	movl	$40, %esi
	movl	$.L.str.10, %edx
	movq	%rbx, %rdi
	callq	luaL_checkstack
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_type
	testl	%eax, %eax
	jle	.LBB9_2
# BB#1:
	movl	$2, %esi
	movl	$6, %edx
	movq	%rbx, %rdi
	callq	luaL_checktype
.LBB9_2:
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	auxsort
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	sort, .Lfunc_end9-sort
	.cfi_endproc

	.p2align	4, 0x90
	.type	auxsort,@function
auxsort:                                # @auxsort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 64
.Lcfi65:
	.cfi_offset %rbx, -56
.Lcfi66:
	.cfi_offset %r12, -48
.Lcfi67:
	.cfi_offset %r13, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	cmpl	%r15d, %r14d
	jle	.LBB10_25
	.p2align	4, 0x90
.LBB10_1:                               # %.lr.ph101
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_15 Depth 2
                                        #       Child Loop BB10_16 Depth 3
                                        #       Child Loop BB10_21 Depth 3
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r15d, %edx
	callq	lua_rawgeti
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	lua_rawgeti
	movl	$-1, %esi
	movl	$-2, %edx
	movq	%rbx, %rdi
	callq	sort_comp
	testl	%eax, %eax
	je	.LBB10_3
# BB#2:                                 #   in Loop: Header=BB10_1 Depth=1
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r15d, %edx
	callq	lua_rawseti
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	lua_rawseti
	jmp	.LBB10_4
	.p2align	4, 0x90
.LBB10_3:                               #   in Loop: Header=BB10_1 Depth=1
	movl	$-3, %esi
	movq	%rbx, %rdi
	callq	lua_settop
.LBB10_4:                               #   in Loop: Header=BB10_1 Depth=1
	movl	%r14d, %ebp
	subl	%r15d, %ebp
	cmpl	$1, %ebp
	je	.LBB10_25
# BB#5:                                 #   in Loop: Header=BB10_1 Depth=1
	leal	(%r14,%r15), %eax
	movl	%eax, %r12d
	shrl	$31, %r12d
	addl	%eax, %r12d
	sarl	%r12d
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r12d, %edx
	callq	lua_rawgeti
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r15d, %edx
	callq	lua_rawgeti
	movl	$-2, %esi
	movl	$-1, %edx
	movq	%rbx, %rdi
	callq	sort_comp
	testl	%eax, %eax
	je	.LBB10_8
# BB#6:                                 #   in Loop: Header=BB10_1 Depth=1
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r12d, %edx
	callq	lua_rawseti
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r15d, %edx
	jmp	.LBB10_7
	.p2align	4, 0x90
.LBB10_8:                               #   in Loop: Header=BB10_1 Depth=1
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	lua_rawgeti
	movl	$-1, %esi
	movl	$-2, %edx
	movq	%rbx, %rdi
	callq	sort_comp
	testl	%eax, %eax
	je	.LBB10_10
# BB#9:                                 #   in Loop: Header=BB10_1 Depth=1
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r12d, %edx
	callq	lua_rawseti
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r14d, %edx
.LBB10_7:                               #   in Loop: Header=BB10_1 Depth=1
	callq	lua_rawseti
	cmpl	$2, %ebp
	je	.LBB10_25
.LBB10_11:                              #   in Loop: Header=BB10_1 Depth=1
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r12d, %edx
	callq	lua_rawgeti
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	leal	-1(%r14), %ebp
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	lua_rawgeti
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r12d, %edx
	callq	lua_rawseti
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	lua_rawseti
	movl	%r15d, %eax
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	jmp	.LBB10_15
	.p2align	4, 0x90
.LBB10_12:                              # %._crit_edge.loopexit
                                        #   in Loop: Header=BB10_15 Depth=2
	leal	-1(%r13), %ebp
.LBB10_13:                              # %._crit_edge
                                        #   in Loop: Header=BB10_15 Depth=2
	movl	(%rsp), %eax            # 4-byte Reload
	cmpl	%eax, %r13d
	jle	.LBB10_24
# BB#14:                                #   in Loop: Header=BB10_15 Depth=2
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	(%rsp), %edx            # 4-byte Reload
	callq	lua_rawseti
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	lua_rawseti
	movl	(%rsp), %eax            # 4-byte Reload
.LBB10_15:                              #   Parent Loop BB10_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_16 Depth 3
                                        #       Child Loop BB10_21 Depth 3
	movl	%ebp, %r13d
	movl	%eax, %r12d
	leal	1(%r12), %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%edx, (%rsp)            # 4-byte Spill
	callq	lua_rawgeti
	movl	$-1, %esi
	movl	$-2, %edx
	movq	%rbx, %rdi
	callq	sort_comp
	testl	%eax, %eax
	je	.LBB10_20
	.p2align	4, 0x90
.LBB10_16:                              # %.lr.ph
                                        #   Parent Loop BB10_1 Depth=1
                                        #     Parent Loop BB10_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%r12d, %r14d
	jg	.LBB10_18
# BB#17:                                #   in Loop: Header=BB10_16 Depth=3
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB10_18:                              #   in Loop: Header=BB10_16 Depth=3
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	leal	2(%r12), %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_rawgeti
	movl	$-1, %esi
	movl	$-2, %edx
	movq	%rbx, %rdi
	callq	sort_comp
	incl	%r12d
	testl	%eax, %eax
	jne	.LBB10_16
# BB#19:                                # %.preheader.loopexit
                                        #   in Loop: Header=BB10_15 Depth=2
	leal	1(%r12), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
.LBB10_20:                              # %.preheader
                                        #   in Loop: Header=BB10_15 Depth=2
	leal	-1(%r13), %ebp
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	lua_rawgeti
	movl	$-3, %esi
	movl	$-1, %edx
	movq	%rbx, %rdi
	callq	sort_comp
	testl	%eax, %eax
	je	.LBB10_13
	.p2align	4, 0x90
.LBB10_21:                              # %.lr.ph95
                                        #   Parent Loop BB10_1 Depth=1
                                        #     Parent Loop BB10_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%r15d, %r13d
	jg	.LBB10_23
# BB#22:                                #   in Loop: Header=BB10_21 Depth=3
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB10_23:                              #   in Loop: Header=BB10_21 Depth=3
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	leal	-2(%r13), %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_rawgeti
	movl	$-3, %esi
	movl	$-1, %edx
	movq	%rbx, %rdi
	callq	sort_comp
	decl	%r13d
	testl	%eax, %eax
	jne	.LBB10_21
	jmp	.LBB10_12
	.p2align	4, 0x90
.LBB10_24:                              #   in Loop: Header=BB10_1 Depth=1
	movl	$-4, %esi
	movq	%rbx, %rdi
	movl	%eax, %ebp
	callq	lua_settop
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	4(%rsp), %r13d          # 4-byte Reload
	movl	%r13d, %edx
	callq	lua_rawgeti
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	lua_rawgeti
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r13d, %edx
	callq	lua_rawseti
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	lua_rawseti
	movl	%r14d, %eax
	subl	%ebp, %eax
	subl	%r15d, %ebp
	leal	2(%r12), %esi
	cmpl	%eax, %ebp
	movl	%r15d, %r13d
	cmovll	%esi, %r13d
	movl	%r12d, %ebp
	cmovll	%r14d, %ebp
	cmovll	%r12d, %r14d
	cmovll	%r15d, %esi
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	auxsort
	cmpl	%r13d, %ebp
	movl	%ebp, %r14d
	movl	%r13d, %r15d
	jg	.LBB10_1
	jmp	.LBB10_25
.LBB10_10:                              #   in Loop: Header=BB10_1 Depth=1
	movl	$-3, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	cmpl	$2, %ebp
	jne	.LBB10_11
.LBB10_25:                              # %.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	auxsort, .Lfunc_end10-auxsort
	.cfi_endproc

	.p2align	4, 0x90
	.type	sort_comp,@function
sort_comp:                              # @sort_comp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 32
.Lcfi74:
	.cfi_offset %rbx, -32
.Lcfi75:
	.cfi_offset %r14, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$2, %esi
	callq	lua_type
	testl	%eax, %eax
	je	.LBB11_2
# BB#1:
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	decl	%ebp
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	lua_pushvalue
	addl	$-2, %r14d
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	lua_pushvalue
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_call
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_toboolean
	movl	%eax, %ebp
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB11_2:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%r14d, %edx
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	lua_lessthan            # TAILCALL
.Lfunc_end11:
	.size	sort_comp, .Lfunc_end11-sort_comp
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"table"
	.size	.L.str, 6

	.type	tab_funcs,@object       # @tab_funcs
	.section	.rodata,"a",@progbits
	.p2align	4
tab_funcs:
	.quad	.L.str.1
	.quad	tconcat
	.quad	.L.str.2
	.quad	foreach
	.quad	.L.str.3
	.quad	foreachi
	.quad	.L.str.4
	.quad	getn
	.quad	.L.str.5
	.quad	maxn
	.quad	.L.str.6
	.quad	tinsert
	.quad	.L.str.7
	.quad	tremove
	.quad	.L.str.8
	.quad	setn
	.quad	.L.str.9
	.quad	sort
	.zero	16
	.size	tab_funcs, 160

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"concat"
	.size	.L.str.1, 7

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"foreach"
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"foreachi"
	.size	.L.str.3, 9

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"getn"
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"maxn"
	.size	.L.str.5, 5

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"insert"
	.size	.L.str.6, 7

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"remove"
	.size	.L.str.7, 7

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"setn"
	.size	.L.str.8, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"sort"
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.zero	1
	.size	.L.str.10, 1

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"invalid value (%s) at index %d in table for 'concat'"
	.size	.L.str.11, 53

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"wrong number of arguments to 'insert'"
	.size	.L.str.12, 38

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"'setn' is obsolete"
	.size	.L.str.13, 19

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"invalid order function for sorting"
	.size	.L.str.14, 35


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
