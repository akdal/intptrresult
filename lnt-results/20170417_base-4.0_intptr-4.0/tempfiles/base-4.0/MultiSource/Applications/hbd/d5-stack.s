	.text
	.file	"d5-stack.bc"
	.globl	_Z5dopopP9Classfile
	.p2align	4, 0x90
	.type	_Z5dopopP9Classfile,@function
_Z5dopopP9Classfile:                    # @_Z5dopopP9Classfile
	.cfi_startproc
# BB#0:
	movq	stkptr(%rip), %rax
	movl	$stack, %ecx
	cmpq	%rcx, %rax
	je	.LBB0_2
# BB#1:
	leaq	-8(%rax), %rcx
	movq	%rcx, stkptr(%rip)
	movq	-8(%rax), %rax
	movq	donestkptr(%rip), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, donestkptr(%rip)
	movq	%rax, (%rcx)
.LBB0_2:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	_Z5dopopP9Classfile, .Lfunc_end0-_Z5dopopP9Classfile
	.cfi_endproc

	.globl	_Z5dodupP9Classfile
	.p2align	4, 0x90
	.type	_Z5dodupP9Classfile,@function
_Z5dodupP9Classfile:                    # @_Z5dodupP9Classfile
	.cfi_startproc
# BB#0:
	movq	stkptr(%rip), %rax
	movq	-8(%rax), %rcx
	movq	(%rcx), %rdx
	cmpl	$18, 12(%rdx)
	je	.LBB1_2
# BB#1:
	movq	%rcx, (%rax)
	movq	stkptr(%rip), %rax
	movq	(%rax), %rcx
	incl	8(%rcx)
	addq	$8, %rax
	movq	%rax, stkptr(%rip)
.LBB1_2:
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	_Z5dodupP9Classfile, .Lfunc_end1-_Z5dodupP9Classfile
	.cfi_endproc

	.globl	_Z8dodup_x1P9Classfile
	.p2align	4, 0x90
	.type	_Z8dodup_x1P9Classfile,@function
_Z8dodup_x1P9Classfile:                 # @_Z8dodup_x1P9Classfile
	.cfi_startproc
# BB#0:
	movq	stkptr(%rip), %rax
	movq	-8(%rax), %rcx
	movq	%rcx, (%rax)
	movq	stkptr(%rip), %rax
	movq	-16(%rax), %rcx
	movq	%rcx, -8(%rax)
	movq	stkptr(%rip), %rax
	movq	(%rax), %rcx
	movq	%rcx, -16(%rax)
	movq	stkptr(%rip), %rax
	movq	(%rax), %rcx
	incl	8(%rcx)
	addq	$8, %rax
	movq	%rax, stkptr(%rip)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	_Z8dodup_x1P9Classfile, .Lfunc_end2-_Z8dodup_x1P9Classfile
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
