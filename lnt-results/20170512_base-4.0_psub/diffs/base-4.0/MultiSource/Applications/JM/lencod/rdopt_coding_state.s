	.text
	.file	"rdopt_coding_state.bc"
	.globl	delete_coding_state
	.p2align	4, 0x90
	.type	delete_coding_state,@function
delete_coding_state:                    # @delete_coding_state
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB0_6
# BB#1:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_3
# BB#2:
	callq	free
.LBB0_3:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_5
# BB#4:
	callq	free
.LBB0_5:
	movq	32(%rbx), %rdi
	callq	delete_contexts_MotionInfo
	movq	40(%rbx), %rdi
	callq	delete_contexts_TextureInfo
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB0_6:
	popq	%rbx
	retq
.Lfunc_end0:
	.size	delete_coding_state, .Lfunc_end0-delete_coding_state
	.cfi_endproc

	.globl	create_coding_state
	.p2align	4, 0x90
	.type	create_coding_state,@function
create_coding_state:                    # @create_coding_state
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movl	$1, %edi
	movl	$344, %esi              # imm = 0x158
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB1_2:
	movq	input(%rip), %rax
	xorl	%ecx, %ecx
	cmpl	$0, 4016(%rax)
	setne	%cl
	leal	1(%rcx,%rcx), %ebp
	movl	%ebp, (%rbx)
	cmpl	$1, 4008(%rax)
	jne	.LBB1_5
# BB#3:
	movl	%ebp, %edi
	movl	$48, %esi
	callq	calloc
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	jne	.LBB1_5
# BB#4:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB1_5:
	movl	%ebp, %edi
	movl	$48, %esi
	callq	calloc
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	jne	.LBB1_7
# BB#6:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
.LBB1_7:
	movq	input(%rip), %rax
	movl	4008(%rax), %eax
	movl	%eax, 24(%rbx)
	cmpl	$1, %eax
	jne	.LBB1_8
# BB#9:
	callq	create_contexts_MotionInfo
	movq	%rax, 32(%rbx)
	callq	create_contexts_TextureInfo
	jmp	.LBB1_10
.LBB1_8:
	xorl	%eax, %eax
.LBB1_10:
	movq	%rax, 40(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	create_coding_state, .Lfunc_end1-create_coding_state
	.cfi_endproc

	.globl	store_coding_state
	.p2align	4, 0x90
	.type	store_coding_state,@function
store_coding_state:                     # @store_coding_state
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 48
.Lcfi12:
	.cfi_offset %rbx, -40
.Lcfi13:
	.cfi_offset %r12, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movq	img(%rip), %rax
	movq	14208(%rax), %rdx
	movl	$1, %ecx
	cmpl	$0, 4(%rdx)
	jne	.LBB2_2
# BB#1:
	movl	(%r12), %ecx
.LBB2_2:
	movq	input(%rip), %rdx
	cmpl	$0, 4168(%rdx)
	je	.LBB2_20
# BB#3:
	movq	14216(%rax), %rbx
	movq	14224(%rax), %r14
	movslq	12(%rax), %r15
	cmpl	$1, 24(%r12)
	jne	.LBB2_4
# BB#11:                                # %.preheader
	testl	%ecx, %ecx
	jle	.LBB2_18
# BB#12:                                # %.lr.ph
	movl	%ecx, %eax
	testb	$1, %al
	jne	.LBB2_14
# BB#13:
	xorl	%edx, %edx
	cmpl	$1, %ecx
	jne	.LBB2_16
	jmp	.LBB2_18
.LBB2_4:                                # %.preheader35
	testl	%ecx, %ecx
	jle	.LBB2_19
# BB#5:                                 # %.lr.ph38
	movl	%ecx, %eax
	testb	$1, %al
	jne	.LBB2_7
# BB#6:
	xorl	%edx, %edx
	cmpl	$1, %ecx
	jne	.LBB2_9
	jmp	.LBB2_19
.LBB2_14:
	movq	8(%r12), %rdx
	movq	24(%rbx), %rsi
	movups	8(%rsi), %xmm0
	movups	24(%rsi), %xmm1
	movups	40(%rsi), %xmm2
	movups	%xmm2, 32(%rdx)
	movups	%xmm1, 16(%rdx)
	movups	%xmm0, (%rdx)
	movq	16(%r12), %rdx
	movq	24(%rbx), %rsi
	movq	(%rsi), %rsi
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movups	32(%rsi), %xmm2
	movups	%xmm2, 32(%rdx)
	movups	%xmm1, 16(%rdx)
	movups	%xmm0, (%rdx)
	movl	$1, %edx
	cmpl	$1, %ecx
	je	.LBB2_18
.LBB2_16:                               # %.lr.ph.new
	subq	%rdx, %rax
	imulq	$104, %rdx, %rcx
	shlq	$4, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	addq	$112, %rcx
	.p2align	4, 0x90
.LBB2_17:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rsi
	movq	24(%rbx), %rdi
	movups	-104(%rdi,%rcx), %xmm0
	movups	-88(%rdi,%rcx), %xmm1
	movups	-72(%rdi,%rcx), %xmm2
	movups	%xmm2, 32(%rsi,%rdx)
	movups	%xmm1, 16(%rsi,%rdx)
	movups	%xmm0, (%rsi,%rdx)
	movq	16(%r12), %rsi
	movq	24(%rbx), %rdi
	movq	-112(%rdi,%rcx), %rdi
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	32(%rdi), %xmm2
	movups	%xmm2, 32(%rsi,%rdx)
	movups	%xmm1, 16(%rsi,%rdx)
	movups	%xmm0, (%rsi,%rdx)
	movq	8(%r12), %rsi
	movq	24(%rbx), %rdi
	movups	(%rdi,%rcx), %xmm0
	movups	16(%rdi,%rcx), %xmm1
	movups	32(%rdi,%rcx), %xmm2
	movups	%xmm2, 80(%rsi,%rdx)
	movups	%xmm1, 64(%rsi,%rdx)
	movups	%xmm0, 48(%rsi,%rdx)
	movq	16(%r12), %rsi
	movq	24(%rbx), %rdi
	movq	-8(%rdi,%rcx), %rdi
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	32(%rdi), %xmm2
	movups	%xmm2, 80(%rsi,%rdx)
	movups	%xmm1, 64(%rsi,%rdx)
	movups	%xmm0, 48(%rsi,%rdx)
	addq	$96, %rdx
	addq	$208, %rcx
	addq	$-2, %rax
	jne	.LBB2_17
.LBB2_18:                               # %._crit_edge
	movq	32(%r12), %rdi
	movq	32(%rbx), %rsi
	movl	$1504, %edx             # imm = 0x5E0
	callq	memcpy
	movq	40(%r12), %rdi
	movq	40(%rbx), %rsi
	movl	$12128, %edx            # imm = 0x2F60
	callq	memcpy
	jmp	.LBB2_19
.LBB2_7:
	movq	16(%r12), %rdx
	movq	24(%rbx), %rsi
	movq	(%rsi), %rsi
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movups	32(%rsi), %xmm2
	movups	%xmm2, 32(%rdx)
	movups	%xmm1, 16(%rdx)
	movups	%xmm0, (%rdx)
	movl	$1, %edx
	cmpl	$1, %ecx
	je	.LBB2_19
.LBB2_9:                                # %.lr.ph38.new
	subq	%rdx, %rax
	imulq	$104, %rdx, %rcx
	shlq	$4, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	.p2align	4, 0x90
.LBB2_10:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rsi
	movq	24(%rbx), %rdi
	movq	(%rdi,%rcx), %rdi
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	32(%rdi), %xmm2
	movups	%xmm2, 32(%rsi,%rdx)
	movups	%xmm1, 16(%rsi,%rdx)
	movups	%xmm0, (%rsi,%rdx)
	movq	16(%r12), %rsi
	movq	24(%rbx), %rdi
	movq	104(%rdi,%rcx), %rdi
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	32(%rdi), %xmm2
	movups	%xmm2, 80(%rsi,%rdx)
	movups	%xmm1, 64(%rsi,%rdx)
	movups	%xmm0, 48(%rsi,%rdx)
	addq	$96, %rdx
	addq	$208, %rcx
	addq	$-2, %rax
	jne	.LBB2_10
.LBB2_19:                               # %.loopexit
	imulq	$536, %r15, %rbx        # imm = 0x218
	movups	24(%r14,%rbx), %xmm0
	movups	40(%r14,%rbx), %xmm1
	movups	%xmm1, 64(%r12)
	movups	%xmm0, 48(%r12)
	leaq	80(%r12), %rdi
	leaq	76(%r14,%rbx), %rsi
	movl	$256, %edx              # imm = 0x100
	callq	memcpy
	movq	408(%r14,%rbx), %rax
	movq	%rax, 336(%r12)
.LBB2_20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	store_coding_state, .Lfunc_end2-store_coding_state
	.cfi_endproc

	.globl	reset_coding_state
	.p2align	4, 0x90
	.type	reset_coding_state,@function
reset_coding_state:                     # @reset_coding_state
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 48
.Lcfi21:
	.cfi_offset %rbx, -40
.Lcfi22:
	.cfi_offset %r12, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movq	img(%rip), %rax
	movq	14208(%rax), %rdx
	movl	$1, %ecx
	cmpl	$0, 4(%rdx)
	jne	.LBB3_2
# BB#1:
	movl	(%r12), %ecx
.LBB3_2:
	movq	input(%rip), %rdx
	cmpl	$0, 4168(%rdx)
	je	.LBB3_20
# BB#3:
	movq	14216(%rax), %rbx
	movq	14224(%rax), %r14
	movslq	12(%rax), %r15
	cmpl	$1, 24(%r12)
	jne	.LBB3_4
# BB#11:                                # %.preheader
	testl	%ecx, %ecx
	jle	.LBB3_18
# BB#12:                                # %.lr.ph
	movl	%ecx, %eax
	testb	$1, %al
	jne	.LBB3_14
# BB#13:
	xorl	%edx, %edx
	cmpl	$1, %ecx
	jne	.LBB3_16
	jmp	.LBB3_18
.LBB3_4:                                # %.preheader35
	testl	%ecx, %ecx
	jle	.LBB3_19
# BB#5:                                 # %.lr.ph38
	movl	%ecx, %eax
	testb	$1, %al
	jne	.LBB3_7
# BB#6:
	xorl	%edx, %edx
	cmpl	$1, %ecx
	jne	.LBB3_9
	jmp	.LBB3_19
.LBB3_14:
	movq	24(%rbx), %rdx
	movq	8(%r12), %rsi
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movups	32(%rsi), %xmm2
	movups	%xmm2, 40(%rdx)
	movups	%xmm1, 24(%rdx)
	movups	%xmm0, 8(%rdx)
	movq	24(%rbx), %rdx
	movq	(%rdx), %rdx
	movq	16(%r12), %rsi
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movups	32(%rsi), %xmm2
	movups	%xmm2, 32(%rdx)
	movups	%xmm1, 16(%rdx)
	movups	%xmm0, (%rdx)
	movl	$1, %edx
	cmpl	$1, %ecx
	je	.LBB3_18
.LBB3_16:                               # %.lr.ph.new
	subq	%rdx, %rax
	imulq	$104, %rdx, %rcx
	shlq	$4, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	addq	$112, %rcx
	.p2align	4, 0x90
.LBB3_17:                               # =>This Inner Loop Header: Depth=1
	movq	24(%rbx), %rsi
	movq	8(%r12), %rdi
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	32(%rdi,%rdx), %xmm2
	movups	%xmm2, -72(%rsi,%rcx)
	movups	%xmm1, -88(%rsi,%rcx)
	movups	%xmm0, -104(%rsi,%rcx)
	movq	24(%rbx), %rsi
	movq	-112(%rsi,%rcx), %rsi
	movq	16(%r12), %rdi
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	32(%rdi,%rdx), %xmm2
	movups	%xmm2, 32(%rsi)
	movups	%xmm1, 16(%rsi)
	movups	%xmm0, (%rsi)
	movq	24(%rbx), %rsi
	movq	8(%r12), %rdi
	movups	48(%rdi,%rdx), %xmm0
	movups	64(%rdi,%rdx), %xmm1
	movups	80(%rdi,%rdx), %xmm2
	movups	%xmm2, 32(%rsi,%rcx)
	movups	%xmm1, 16(%rsi,%rcx)
	movups	%xmm0, (%rsi,%rcx)
	movq	24(%rbx), %rsi
	movq	-8(%rsi,%rcx), %rsi
	movq	16(%r12), %rdi
	movups	48(%rdi,%rdx), %xmm0
	movups	64(%rdi,%rdx), %xmm1
	movups	80(%rdi,%rdx), %xmm2
	movups	%xmm2, 32(%rsi)
	movups	%xmm1, 16(%rsi)
	movups	%xmm0, (%rsi)
	addq	$96, %rdx
	addq	$208, %rcx
	addq	$-2, %rax
	jne	.LBB3_17
.LBB3_18:                               # %._crit_edge
	movq	32(%rbx), %rdi
	movq	32(%r12), %rsi
	movl	$1504, %edx             # imm = 0x5E0
	callq	memcpy
	movq	40(%rbx), %rdi
	movq	40(%r12), %rsi
	movl	$12128, %edx            # imm = 0x2F60
	callq	memcpy
	jmp	.LBB3_19
.LBB3_7:
	movq	24(%rbx), %rdx
	movq	(%rdx), %rdx
	movq	16(%r12), %rsi
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movups	32(%rsi), %xmm2
	movups	%xmm2, 32(%rdx)
	movups	%xmm1, 16(%rdx)
	movups	%xmm0, (%rdx)
	movl	$1, %edx
	cmpl	$1, %ecx
	je	.LBB3_19
.LBB3_9:                                # %.lr.ph38.new
	subq	%rdx, %rax
	imulq	$104, %rdx, %rcx
	shlq	$4, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	.p2align	4, 0x90
.LBB3_10:                               # =>This Inner Loop Header: Depth=1
	movq	24(%rbx), %rsi
	movq	(%rsi,%rcx), %rsi
	movq	16(%r12), %rdi
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	32(%rdi,%rdx), %xmm2
	movups	%xmm2, 32(%rsi)
	movups	%xmm1, 16(%rsi)
	movups	%xmm0, (%rsi)
	movq	24(%rbx), %rsi
	movq	104(%rsi,%rcx), %rsi
	movq	16(%r12), %rdi
	movups	48(%rdi,%rdx), %xmm0
	movups	64(%rdi,%rdx), %xmm1
	movups	80(%rdi,%rdx), %xmm2
	movups	%xmm2, 32(%rsi)
	movups	%xmm1, 16(%rsi)
	movups	%xmm0, (%rsi)
	addq	$96, %rdx
	addq	$208, %rcx
	addq	$-2, %rax
	jne	.LBB3_10
.LBB3_19:                               # %.loopexit
	imulq	$536, %r15, %rbx        # imm = 0x218
	movups	48(%r12), %xmm0
	movups	64(%r12), %xmm1
	movups	%xmm1, 40(%r14,%rbx)
	movups	%xmm0, 24(%r14,%rbx)
	leaq	76(%r14,%rbx), %rdi
	leaq	80(%r12), %rsi
	movl	$256, %edx              # imm = 0x100
	callq	memcpy
	movq	336(%r12), %rax
	movq	%rax, 408(%r14,%rbx)
.LBB3_20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	reset_coding_state, .Lfunc_end3-reset_coding_state
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"init_coding_state: cs"
	.size	.L.str, 22

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"init_coding_state: cs->encenv"
	.size	.L.str.1, 30

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"init_coding_state: cs->bitstream"
	.size	.L.str.2, 33

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
