	.text
	.file	"libclamav_autoit.bc"
	.globl	cli_scanautoit
	.p2align	4, 0x90
	.type	cli_scanautoit,@function
cli_scanautoit:                         # @cli_scanautoit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1768, %rsp             # imm = 0x6E8
.Lcfi6:
	.cfi_def_cfa_offset 1824
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %rax
	movq	%rsi, %r15
	movl	%edi, %r14d
	xorl	%edx, %edx
	movq	%rax, %rsi
	callq	lseek
	leaq	87(%rsp), %rsi
	movl	$1, %edx
	movl	%r14d, %edi
	callq	cli_readn
	movl	$-123, %r13d
	cmpl	$1, %eax
	jne	.LBB0_16
# BB#1:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r12
	movl	$-118, %r13d
	testq	%r12, %r12
	je	.LBB0_16
# BB#2:
	movl	$448, %esi              # imm = 0x1C0
	movq	%r12, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB0_4
# BB#3:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_dbgmsg
	jmp	.LBB0_15
.LBB0_4:
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_6
# BB#5:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_dbgmsg
.LBB0_6:
	movb	87(%rsp), %al
	cmpb	$54, %al
	movq	%r15, 96(%rsp)          # 8-byte Spill
	je	.LBB0_10
# BB#7:
	cmpb	$53, %al
	jne	.LBB0_11
# BB#8:
	leaq	128(%rsp), %rsi
	movl	$16, %edx
	movl	%r14d, %edi
	callq	cli_readn
	xorl	%r13d, %r13d
	cmpl	$16, %eax
	jne	.LBB0_13
# BB#9:                                 # %.preheader423.preheader.i
	movzbl	128(%rsp), %ecx
	movzbl	129(%rsp), %eax
	movzbl	130(%rsp), %edx
	movzbl	131(%rsp), %esi
	movzbl	132(%rsp), %edi
	addl	%ecx, %eax
	movzbl	133(%rsp), %ecx
	addl	%edx, %eax
	movzbl	134(%rsp), %edx
	addl	%esi, %eax
	movzbl	135(%rsp), %esi
	addl	%edi, %eax
	movzbl	136(%rsp), %edi
	addl	%ecx, %eax
	movzbl	137(%rsp), %ecx
	addl	%edx, %eax
	movzbl	138(%rsp), %edx
	addl	%esi, %eax
	movzbl	139(%rsp), %esi
	addl	%edi, %eax
	movzbl	140(%rsp), %edi
	addl	%ecx, %eax
	movzbl	141(%rsp), %ecx
	addl	%edx, %eax
	movzbl	142(%rsp), %edx
	addl	%esi, %eax
	movzbl	143(%rsp), %esi
	addl	%edi, %eax
	addl	%ecx, %eax
	addl	%edx, %eax
	leal	8879(%rsi,%rax), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	leaq	128(%rsp), %rbx
	movl	$0, 68(%rsp)            # 4-byte Folded Spill
	movl	%r14d, 44(%rsp)         # 4-byte Spill
	jmp	.LBB0_18
.LBB0_10:
	movl	$16, %esi
	movl	$1, %edx
	movl	%r14d, %edi
	callq	lseek
	leaq	128(%rsp), %rbx
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_319
.LBB0_11:
	xorl	%r13d, %r13d
	movl	$.L.str.3, %edi
.LBB0_12:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_13:
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_15
.LBB0_14:
	movq	%r12, %rdi
	callq	cli_rmdirs
.LBB0_15:
	movq	%r12, %rdi
	callq	free
.LBB0_16:
	movl	%r13d, %eax
	addq	$1768, %rsp             # imm = 0x6E8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_17:                               # %.backedge.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	%rbp, %rdi
	callq	free
	jmp	.LBB0_18
.LBB0_51:                               #   in Loop: Header=BB0_18 Depth=1
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_17
.LBB0_18:                               # %.backedge.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_93 Depth 2
                                        #       Child Loop BB0_103 Depth 3
                                        #       Child Loop BB0_156 Depth 3
                                        #       Child Loop BB0_159 Depth 3
                                        #     Child Loop BB0_206 Depth 2
                                        #     Child Loop BB0_217 Depth 2
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_18 Depth=1
	movl	4(%rax), %esi
	leal	-1(%rsi), %eax
	cmpl	68(%rsp), %eax          # 4-byte Folded Reload
	jb	.LBB0_539
.LBB0_20:                               # %.critedge.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$8, %edx
	movl	%r14d, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	xorl	%r13d, %r13d
	cmpl	$8, %eax
	jne	.LBB0_13
# BB#21:                                #   in Loop: Header=BB0_18 Depth=1
	cmpl	$-827298305, 128(%rsp)  # imm = 0xCEB06DFF
	jne	.LBB0_226
# BB#22:                                #   in Loop: Header=BB0_18 Depth=1
	movl	132(%rsp), %ebx
	testl	%ebx, %ebx
	js	.LBB0_13
# BB#23:                                #   in Loop: Header=BB0_18 Depth=1
	xorl	$10684, %ebx            # imm = 0x29BC
	movl	%ebx, %ebp
	cmpl	$299, %ebx              # imm = 0x12B
	ja	.LBB0_27
# BB#24:                                #   in Loop: Header=BB0_18 Depth=1
	movb	cli_debug_flag(%rip), %al
	testb	%al, %al
	je	.LBB0_27
# BB#25:                                #   in Loop: Header=BB0_18 Depth=1
	movl	%r14d, %r13d
	movl	%r14d, %edi
	leaq	128(%rsp), %r14
	movq	%r14, %rsi
	movl	%ebx, %edx
	callq	cli_readn
	cmpl	%ebx, %eax
	jne	.LBB0_542
# BB#26:                                #   in Loop: Header=BB0_18 Depth=1
	movb	$0, 128(%rsp,%rbp)
	leal	41566(%rbx), %edx
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	MT_decrypt
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movq	%r14, %rsi
	movl	%r13d, %r14d
	xorl	%r13d, %r13d
	jmp	.LBB0_28
.LBB0_27:                               # %._crit_edge589.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$1, %edx
	movl	%r14d, %edi
	movq	%rbp, %rsi
	callq	lseek
	leaq	128(%rsp), %rsi
.LBB0_28:                               #   in Loop: Header=BB0_18 Depth=1
	movl	$4, %edx
	movl	%r14d, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB0_13
# BB#29:                                #   in Loop: Header=BB0_18 Depth=1
	movl	128(%rsp), %ebp
	testl	%ebp, %ebp
	js	.LBB0_13
# BB#30:                                #   in Loop: Header=BB0_18 Depth=1
	xorl	$10668, %ebp            # imm = 0x29AC
	movl	%ebp, %r14d
	cmpl	$299, %ebp              # imm = 0x12B
	ja	.LBB0_34
# BB#31:                                #   in Loop: Header=BB0_18 Depth=1
	movb	cli_debug_flag(%rip), %al
	testb	%al, %al
	je	.LBB0_34
# BB#32:                                #   in Loop: Header=BB0_18 Depth=1
	movl	44(%rsp), %edi          # 4-byte Reload
	leaq	128(%rsp), %rbx
	movq	%rbx, %rsi
	movl	%ebp, %edx
	callq	cli_readn
	cmpl	%ebp, %eax
	jne	.LBB0_13
# BB#33:                                #   in Loop: Header=BB0_18 Depth=1
	leal	62046(%rbp), %edx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	MT_decrypt
	movb	$0, 128(%rsp,%r14)
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movl	44(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB0_35
.LBB0_34:                               # %._crit_edge588.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$1, %edx
	movl	44(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %edi
	movq	%r14, %rsi
	callq	lseek
	leaq	128(%rsp), %rbx
.LBB0_35:                               #   in Loop: Header=BB0_18 Depth=1
	movl	$13, %edx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	cmpl	$13, %eax
	movl	%ebp, %eax
	jne	.LBB0_13
# BB#36:                                #   in Loop: Header=BB0_18 Depth=1
	movl	129(%rsp), %ebp
	testl	%ebp, %ebp
	js	.LBB0_536
# BB#37:                                #   in Loop: Header=BB0_18 Depth=1
	movl	%eax, %edi
	movb	128(%rsp), %r14b
	xorl	$17834, %ebp            # imm = 0x45AA
	movl	$16, %esi
	movl	$1, %edx
	callq	lseek
	testl	%ebp, %ebp
	je	.LBB0_50
# BB#38:                                #   in Loop: Header=BB0_18 Depth=1
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	movl	133(%rsp), %esi
	xorl	$17834, %esi            # imm = 0x45AA
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	137(%rsp), %esi
	xorl	$50130, %esi            # imm = 0xC3D2
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB0_42
# BB#39:                                #   in Loop: Header=BB0_18 Depth=1
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB0_42
# BB#40:                                #   in Loop: Header=BB0_18 Depth=1
	movl	%ebp, %ebx
	cmpq	%rdx, %rbx
	jbe	.LBB0_42
# BB#41:                                #   in Loop: Header=BB0_18 Depth=1
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	movl	$1, %edx
	movl	44(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, %edi
	movq	%rbx, %rsi
	callq	lseek
	leaq	128(%rsp), %rbx
	jmp	.LBB0_18
.LBB0_42:                               #   in Loop: Header=BB0_18 Depth=1
	movl	%ebp, %edi
	callq	cli_malloc
	movl	%ebp, %ebx
	movq	%rax, %rbp
	movl	$-114, %r13d
	testq	%rbp, %rbp
	je	.LBB0_13
# BB#43:                                #   in Loop: Header=BB0_18 Depth=1
	movl	44(%rsp), %edi          # 4-byte Reload
	movq	%rbp, %rsi
	movl	%ebx, %edx
	callq	cli_readn
	cmpl	%ebx, %eax
	jne	.LBB0_541
# BB#44:                                #   in Loop: Header=BB0_18 Depth=1
	movq	%rbp, %rdi
	movl	%ebx, 28(%rsp)          # 4-byte Spill
	movl	%ebx, %esi
	movl	32(%rsp), %edx          # 4-byte Reload
	callq	MT_decrypt
	cmpb	$1, %r14b
	jne	.LBB0_52
# BB#45:                                #   in Loop: Header=BB0_18 Depth=1
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$892354885, (%rbp)      # imm = 0x35304145
	leaq	128(%rsp), %rbx
	movl	44(%rsp), %r14d         # 4-byte Reload
	jne	.LBB0_51
# BB#46:                                #   in Loop: Header=BB0_18 Depth=1
	movl	4(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, %ecx
	bswapl	%ecx
	cmovel	28(%rsp), %ecx          # 4-byte Folded Reload
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB0_53
# BB#47:                                #   in Loop: Header=BB0_18 Depth=1
	movq	24(%rax), %rdx
	movl	%ecx, %edi
	testq	%rdx, %rdx
	je	.LBB0_54
# BB#48:                                #   in Loop: Header=BB0_18 Depth=1
	cmpq	%rdx, %rdi
	jbe	.LBB0_54
# BB#49:                                #   in Loop: Header=BB0_18 Depth=1
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	movl	28(%rsp), %esi          # 4-byte Reload
	callq	cli_dbgmsg
	jmp	.LBB0_17
.LBB0_50:                               #   in Loop: Header=BB0_18 Depth=1
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	44(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB0_18
.LBB0_52:                               #   in Loop: Header=BB0_18 Depth=1
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbp, %r15
	movl	28(%rsp), %ebp          # 4-byte Reload
	movl	44(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB0_191
.LBB0_53:                               # %._crit_edge587.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	%ecx, %edi
.LBB0_54:                               # %.loopexit621.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	%rbp, (%rsp)            # 8-byte Spill
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	movq	%r12, 72(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_556
# BB#55:                                #   in Loop: Header=BB0_18 Depth=1
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movl	48(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %esi
	callq	cli_dbgmsg
	xorl	%edi, %edi
	movl	%ebx, %r11d
	addq	%r15, %rbp
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movl	$8, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	jmp	.LBB0_93
.LBB0_56:                               # %.lr.ph.i172.1570.thread.i
                                        #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	orl	%ecx, %r13d
	movzbl	(%rdx,%rax), %edx
	orl	%r13d, %edx
	andl	$32768, %r13d           # imm = 0x8000
	addl	%edx, %edx
	shrl	$15, %r13d
	movl	$15, %ecx
.LBB0_57:                               # %.lr.ph.i172.2571.i.thread
                                        #   in Loop: Header=BB0_93 Depth=2
	addl	$2, %r9d
	shll	$16, %r13d
	movzwl	%dx, %edi
	orl	%r13d, %edi
	leal	(%rdx,%rdx), %esi
	shrl	$15, %edi
.LBB0_58:                               # %.lr.ph.i172.3572.i
                                        #   in Loop: Header=BB0_93 Depth=2
	shll	$16, %edi
	movzwl	%si, %esi
	orl	%edi, %esi
	shll	$2, %edx
	addl	$-2, %ecx
	jne	.LBB0_60
# BB#59:                                #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%ecx, %eax
	orl	%edx, %eax
	movl	$16, %ecx
	movw	%ax, %dx
.LBB0_60:                               # %.lr.ph.i172.4573.i
                                        #   in Loop: Header=BB0_93 Depth=2
	andl	$67076096, %esi         # imm = 0x3FF8000
	movzwl	%dx, %eax
	leal	(%rax,%rsi,2), %esi
	leal	(%rdx,%rdx), %edi
	decl	%ecx
	jne	.LBB0_62
# BB#61:                                #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdx,%rax), %eax
	orl	%ecx, %eax
	orl	%edi, %eax
	movl	$16, %ecx
	movw	%ax, %di
.LBB0_62:                               # %.lr.ph.i172.5574.i
                                        #   in Loop: Header=BB0_93 Depth=2
	andl	$134184960, %esi        # imm = 0x7FF8000
	movzwl	%di, %eax
	leal	(%rax,%rsi,2), %edx
	leal	(%rdi,%rdi), %esi
	decl	%ecx
	jne	.LBB0_64
# BB#63:                                #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%ecx, %eax
	orl	%esi, %eax
	movl	$16, %ecx
	movw	%ax, %si
.LBB0_64:                               # %.lr.ph.i172.6575.i
                                        #   in Loop: Header=BB0_93 Depth=2
	andl	$268402688, %edx        # imm = 0xFFF8000
	movzwl	%si, %eax
	leal	(%rax,%rdx,2), %edx
	leal	(%rsi,%rsi), %esi
	decl	%ecx
	jne	.LBB0_66
# BB#65:                                #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%ecx, %eax
	orl	%esi, %eax
	movl	$16, %ecx
	movw	%ax, %si
.LBB0_66:                               # %.lr.ph.i172.7576.i
                                        #   in Loop: Header=BB0_93 Depth=2
	andl	$536838144, %edx        # imm = 0x1FFF8000
	movzwl	%si, %eax
	leal	(%rax,%rdx,2), %edx
	leal	(%rsi,%rsi), %esi
	decl	%ecx
	jne	.LBB0_68
# BB#67:                                #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%ecx, %eax
	orl	%esi, %eax
	movl	$16, %ecx
	movw	%ax, %si
.LBB0_68:                               # %getbits.exit177577.i
                                        #   in Loop: Header=BB0_93 Depth=2
	andl	$1073709056, %edx       # imm = 0x3FFF8000
	movzwl	%si, %eax
	leal	(%rax,%rdx,2), %ebx
	leal	(%rsi,%rsi), %r13d
	shrl	$15, %ebx
	movl	%ecx, %ebp
	decl	%ebp
	movl	$41, %r10d
	cmpl	$255, %ebx
	jne	.LBB0_90
# BB#69:                                # %.preheader418.i.preheader
                                        #   in Loop: Header=BB0_93 Depth=2
	movl	$296, %r10d             # imm = 0x128
	cmpl	$7, %ebp
	ja	.LBB0_73
.LBB0_71:                               #   in Loop: Header=BB0_93 Depth=2
	movl	$7, %eax
	subl	%ebp, %eax
	shrl	$4, %eax
	leal	2(%rax,%rax), %eax
	movl	28(%rsp), %ecx          # 4-byte Reload
	subl	%r9d, %ecx
	cmpl	%ecx, %eax
	ja	.LBB0_92
# BB#72:                                # %.lr.ph.i179.preheader.i
                                        #   in Loop: Header=BB0_93 Depth=2
	testl	%ebp, %ebp
	jne	.LBB0_73
# BB#77:                                # %.lr.ph.i179.1578.thread.i
                                        #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	orl	%ecx, %r13d
	movzbl	(%rdx,%rax), %ecx
	orl	%r13d, %ecx
	andl	$32768, %r13d           # imm = 0x8000
	addl	%ecx, %ecx
	shrl	$15, %r13d
	movl	$15, %ebp
	movl	%r13d, %esi
	jmp	.LBB0_78
.LBB0_70:                               #   in Loop: Header=BB0_93 Depth=2
	addl	$255, %r10d
	cmpl	$7, %ebp
	jbe	.LBB0_71
.LBB0_73:                               # %.lr.ph.i179.1578.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movzwl	%r13w, %esi
	leal	(%r13,%r13), %ecx
	shrl	$15, %esi
	decl	%ebp
	je	.LBB0_76
# BB#74:                                # %.lr.ph.i179.2579.i
                                        #   in Loop: Header=BB0_93 Depth=2
	shll	$16, %esi
	movzwl	%cx, %edx
	orl	%esi, %edx
	shll	$2, %r13d
	shrl	$15, %edx
	cmpl	$1, %ebp
	jne	.LBB0_79
# BB#75:                                # %.lr.ph.i179.3580.thread.i
                                        #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rcx), %esi
	shll	$8, %esi
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %ecx
	orl	%r13d, %ecx
	orl	%esi, %ecx
	shll	$16, %edx
	movzwl	%cx, %esi
	orl	%edx, %esi
	addl	%ecx, %ecx
	movl	$15, %ebp
	jmp	.LBB0_81
.LBB0_76:                               #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %edx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rdx), %edx
	shll	$8, %edx
	movzbl	(%rdi,%rax), %eax
	orl	%ecx, %eax
	orl	%edx, %eax
	movl	$16, %ebp
	movw	%ax, %cx
.LBB0_78:                               # %.lr.ph.i179.2579.i.thread
                                        #   in Loop: Header=BB0_93 Depth=2
	addl	$2, %r9d
	shll	$16, %esi
	movzwl	%cx, %edx
	orl	%esi, %edx
	leal	(%rcx,%rcx), %r13d
	shrl	$15, %edx
.LBB0_79:                               # %.lr.ph.i179.3580.i
                                        #   in Loop: Header=BB0_93 Depth=2
	shll	$16, %edx
	movzwl	%r13w, %esi
	orl	%edx, %esi
	shll	$2, %ecx
	addl	$-2, %ebp
	jne	.LBB0_81
# BB#80:                                #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %edx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rdx), %edx
	shll	$8, %edx
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%ecx, %eax
	orl	%edx, %eax
	movl	$16, %ebp
	movw	%ax, %cx
.LBB0_81:                               # %.lr.ph.i179.4581.i
                                        #   in Loop: Header=BB0_93 Depth=2
	andl	$67076096, %esi         # imm = 0x3FF8000
	movzwl	%cx, %eax
	leal	(%rax,%rsi,2), %edx
	leal	(%rcx,%rcx), %esi
	decl	%ebp
	jne	.LBB0_83
# BB#82:                                #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%ecx, %eax
	orl	%esi, %eax
	movl	$16, %ebp
	movw	%ax, %si
.LBB0_83:                               # %.lr.ph.i179.5582.i
                                        #   in Loop: Header=BB0_93 Depth=2
	andl	$134184960, %edx        # imm = 0x7FF8000
	movzwl	%si, %eax
	leal	(%rax,%rdx,2), %ecx
	leal	(%rsi,%rsi), %edx
	decl	%ebp
	jne	.LBB0_85
# BB#84:                                #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rsi), %esi
	shll	$8, %esi
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%esi, %eax
	orl	%edx, %eax
	movl	$16, %ebp
	movw	%ax, %dx
.LBB0_85:                               # %.lr.ph.i179.6583.i
                                        #   in Loop: Header=BB0_93 Depth=2
	andl	$268402688, %ecx        # imm = 0xFFF8000
	movzwl	%dx, %eax
	leal	(%rax,%rcx,2), %ecx
	leal	(%rdx,%rdx), %edx
	decl	%ebp
	jne	.LBB0_87
# BB#86:                                #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rsi), %esi
	shll	$8, %esi
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%esi, %eax
	orl	%edx, %eax
	movl	$16, %ebp
	movw	%ax, %dx
.LBB0_87:                               # %.lr.ph.i179.7584.i
                                        #   in Loop: Header=BB0_93 Depth=2
	andl	$536838144, %ecx        # imm = 0x1FFF8000
	movzwl	%dx, %eax
	leal	(%rax,%rcx,2), %ecx
	leal	(%rdx,%rdx), %edx
	decl	%ebp
	jne	.LBB0_89
# BB#88:                                #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rsi), %esi
	shll	$8, %esi
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%esi, %eax
	orl	%edx, %eax
	movl	$16, %ebp
	movw	%ax, %dx
.LBB0_89:                               # %getbits.exit184585.i
                                        #   in Loop: Header=BB0_93 Depth=2
	andl	$1073709056, %ecx       # imm = 0x3FFF8000
	movzwl	%dx, %eax
	leal	(%rax,%rcx,2), %ebx
	leal	(%rdx,%rdx), %r13d
	shrl	$15, %ebx
	decl	%ebp
	cmpl	$255, %ebx
	je	.LBB0_70
.LBB0_90:                               #   in Loop: Header=BB0_93 Depth=2
	movq	%r9, 8(%rsp)            # 8-byte Spill
.LBB0_91:                               # %.loopexit.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movl	44(%rsp), %r14d         # 4-byte Reload
	movl	48(%rsp), %r11d         # 4-byte Reload
	jmp	.LBB0_148
.LBB0_92:                               # %getbits.exit184.thread.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movq	%r9, 8(%rsp)            # 8-byte Spill
	xorl	%ebx, %ebx
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movq	%r10, %r14
	callq	cli_dbgmsg
	movq	%r14, %r10
	movl	$1, %r8d
	jmp	.LBB0_91
.LBB0_93:                               #   Parent Loop BB0_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_103 Depth 3
                                        #       Child Loop BB0_156 Depth 3
                                        #       Child Loop BB0_159 Depth 3
	cmpl	%r11d, %edi
	movq	72(%rsp), %r12          # 8-byte Reload
	jae	.LBB0_187
# BB#94:                                #   in Loop: Header=BB0_93 Depth=2
	testl	%ebp, %ebp
	movl	%edi, 16(%rsp)          # 4-byte Spill
	je	.LBB0_96
# BB#95:                                #   in Loop: Header=BB0_93 Depth=2
	movl	%ebp, %eax
	movq	8(%rsp), %r9            # 8-byte Reload
	jmp	.LBB0_98
.LBB0_96:                               #   in Loop: Header=BB0_93 Depth=2
	movl	28(%rsp), %eax          # 4-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
	subl	%r9d, %eax
	cmpl	$1, %eax
	jbe	.LBB0_166
# BB#97:                                #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdx,%rax), %edx
	orl	%r13d, %edx
	orl	%ecx, %edx
	movl	$16, %eax
	movw	%dx, %r13w
.LBB0_98:                               # %getbits.exit559.i
                                        #   in Loop: Header=BB0_93 Depth=2
                                        # kill: %R13W<def> %R13W<kill> %R13<kill> %R13<def>
	testw	%r13w, %r13w
	leal	(%r13,%r13), %r13d
	leal	-1(%rax), %ebp
	jns	.LBB0_136
# BB#99:                                #   in Loop: Header=BB0_93 Depth=2
	cmpl	$14, %ebp
	ja	.LBB0_102
# BB#100:                               #   in Loop: Header=BB0_93 Depth=2
	movl	$15, %ecx
	subl	%eax, %ecx
	shrl	$4, %ecx
	leal	2(%rcx,%rcx), %eax
	movl	28(%rsp), %ecx          # 4-byte Reload
	subl	%r9d, %ecx
	cmpl	%ecx, %eax
	jbe	.LBB0_102
# BB#101:                               #   in Loop: Header=BB0_93 Depth=2
	xorl	%r12d, %r12d
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movq	%r9, %rbx
	callq	cli_dbgmsg
	movq	%rbx, %r9
	movl	48(%rsp), %r11d         # 4-byte Reload
	movl	$1, %r8d
	cmpl	$1, %ebp
	jbe	.LBB0_107
	jmp	.LBB0_109
.LBB0_102:                              # %.lr.ph.i144.i.preheader
                                        #   in Loop: Header=BB0_93 Depth=2
	xorl	%r12d, %r12d
	movl	$-15, %eax
.LBB0_103:                              # %.lr.ph.i144.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_93 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%ebp, %ebp
	jne	.LBB0_105
# BB#104:                               #   in Loop: Header=BB0_103 Depth=3
	leal	1(%r9), %ecx
	movl	%r9d, %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movzbl	(%rsi,%rdx), %edx
	shll	$8, %edx
	addl	$2, %r9d
	movzbl	(%rsi,%rcx), %ecx
	orl	%r13d, %ecx
	orl	%edx, %ecx
	movl	$16, %ebp
	movw	%cx, %r13w
.LBB0_105:                              #   in Loop: Header=BB0_103 Depth=3
	shll	$16, %r12d
	movzwl	%r13w, %ecx
	orl	%r12d, %ecx
	leal	(%r13,%r13), %r13d
	shrl	$15, %ecx
	movzwl	%cx, %r12d
	decl	%ebp
	incl	%eax
	jne	.LBB0_103
# BB#106:                               #   in Loop: Header=BB0_93 Depth=2
	xorl	%r8d, %r8d
	cmpl	$1, %ebp
	ja	.LBB0_109
.LBB0_107:                              #   in Loop: Header=BB0_93 Depth=2
	movl	$1, %eax
	subl	%ebp, %eax
	shrl	$4, %eax
	leal	2(%rax,%rax), %eax
	movl	28(%rsp), %ecx          # 4-byte Reload
	subl	%r9d, %ecx
	cmpl	%ecx, %eax
	jbe	.LBB0_111
# BB#108:                               # %getbits.exit156.thread.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movq	%r9, 8(%rsp)            # 8-byte Spill
	xorl	%ebx, %ebx
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	48(%rsp), %r11d         # 4-byte Reload
	movl	$1, %r8d
	xorl	%r10d, %r10d
	jmp	.LBB0_148
.LBB0_109:                              # %.lr.ph.i151.1560.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movzwl	%r13w, %esi
	leal	(%r13,%r13), %edx
	shrl	$15, %esi
	decl	%ebp
	je	.LBB0_113
# BB#110:                               #   in Loop: Header=BB0_93 Depth=2
	movl	%ebp, %ecx
	jmp	.LBB0_115
.LBB0_111:                              # %.lr.ph.i151.preheader.i
                                        #   in Loop: Header=BB0_93 Depth=2
	testl	%ebp, %ebp
	je	.LBB0_114
# BB#112:                               # %.lr.ph.i151.1560.i.thread
                                        #   in Loop: Header=BB0_93 Depth=2
	movzwl	%r13w, %esi
	leal	(%r13,%r13), %edx
	shrl	$15, %esi
.LBB0_113:                              #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%edx, %eax
	orl	%ecx, %eax
	movl	$16, %ecx
	movw	%ax, %dx
	jmp	.LBB0_115
.LBB0_114:                              # %.lr.ph.i151.1560.thread.i
                                        #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	orl	%ecx, %r13d
	addl	$2, %r9d
	movzbl	(%rdx,%rax), %edx
	orl	%r13d, %edx
	andl	$32768, %r13d           # imm = 0x8000
	addl	%edx, %edx
	shrl	$15, %r13d
	movl	$15, %ecx
	movl	%r13d, %esi
.LBB0_115:                              # %getbits.exit156561.i
                                        #   in Loop: Header=BB0_93 Depth=2
	shll	$16, %esi
	movzwl	%dx, %ebx
	orl	%esi, %ebx
	leal	(%rdx,%rdx), %r13d
	shrl	$15, %ebx
	leal	-1(%rcx), %ebp
	xorl	%r10d, %r10d
	cmpl	$3, %ebx
	movl	16(%rsp), %eax          # 4-byte Reload
	jne	.LBB0_119
# BB#116:                               #   in Loop: Header=BB0_93 Depth=2
	cmpl	$2, %ebp
	ja	.LBB0_121
# BB#117:                               #   in Loop: Header=BB0_93 Depth=2
	movl	$3, %eax
	subl	%ecx, %eax
	shrl	$4, %eax
	leal	2(%rax,%rax), %eax
	movl	28(%rsp), %esi          # 4-byte Reload
	subl	%r9d, %esi
	cmpl	%esi, %eax
	jbe	.LBB0_120
# BB#118:                               # %getbits.exit163.thread.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movq	%r9, 8(%rsp)            # 8-byte Spill
	xorl	%ebx, %ebx
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	48(%rsp), %r11d         # 4-byte Reload
	movl	$3, %r10d
	movl	$1, %r8d
	jmp	.LBB0_149
.LBB0_119:                              #   in Loop: Header=BB0_93 Depth=2
	movq	%r9, 8(%rsp)            # 8-byte Spill
	jmp	.LBB0_149
.LBB0_120:                              # %.lr.ph.i158.preheader.i
                                        #   in Loop: Header=BB0_93 Depth=2
	testl	%ebp, %ebp
	je	.LBB0_124
.LBB0_121:                              # %.lr.ph.i158.1562.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movzwl	%r13w, %r13d
	shll	$2, %edx
	shrl	$15, %r13d
	addl	$-2, %ecx
	je	.LBB0_123
# BB#122:                               #   in Loop: Header=BB0_93 Depth=2
	movq	(%rsp), %rbp            # 8-byte Reload
	jmp	.LBB0_125
.LBB0_123:                              #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rbp            # 8-byte Reload
	movzbl	(%rbp,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rbp,%rax), %eax
	orl	%edx, %eax
	orl	%ecx, %eax
	movl	$16, %ecx
	movw	%ax, %dx
	jmp	.LBB0_125
.LBB0_124:                              # %.lr.ph.i158.1562.thread.i
                                        #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rbp            # 8-byte Reload
	movzbl	(%rbp,%rcx), %ecx
	shll	$8, %ecx
	orl	%ecx, %r13d
	addl	$2, %r9d
	movzbl	(%rbp,%rax), %edx
	orl	%r13d, %edx
	andl	$32768, %r13d           # imm = 0x8000
	addl	%edx, %edx
	shrl	$15, %r13d
	movl	$15, %ecx
.LBB0_125:                              # %.lr.ph.i158.2563.i
                                        #   in Loop: Header=BB0_93 Depth=2
	shll	$16, %r13d
	movzwl	%dx, %edi
	orl	%r13d, %edi
	leal	(%rdx,%rdx), %esi
	shrl	$15, %edi
	decl	%ecx
	jne	.LBB0_127
# BB#126:                               #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movzbl	(%rbp,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rbp,%rax), %eax
	orl	%esi, %eax
	orl	%ecx, %eax
	movl	$16, %ecx
	movw	%ax, %si
.LBB0_127:                              # %getbits.exit163564.i
                                        #   in Loop: Header=BB0_93 Depth=2
	shll	$16, %edi
	movzwl	%si, %eax
	orl	%edi, %eax
	leal	(%rsi,%rsi), %r13d
	shrl	$15, %eax
	leal	-1(%rcx), %ebp
	movzwl	%ax, %ebx
	movl	$3, %r10d
	cmpl	$7, %ebx
	jne	.LBB0_147
# BB#128:                               #   in Loop: Header=BB0_93 Depth=2
	cmpl	$4, %ebp
	ja	.LBB0_132
# BB#129:                               #   in Loop: Header=BB0_93 Depth=2
	movl	$5, %eax
	subl	%ecx, %eax
	shrl	$4, %eax
	leal	2(%rax,%rax), %eax
	movl	28(%rsp), %edx          # 4-byte Reload
	subl	%r9d, %edx
	cmpl	%edx, %eax
	jbe	.LBB0_131
# BB#130:                               # %getbits.exit170.thread.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movq	%r9, 8(%rsp)            # 8-byte Spill
	xorl	%ebx, %ebx
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	48(%rsp), %r11d         # 4-byte Reload
	movl	$10, %r10d
	movl	$1, %r8d
	jmp	.LBB0_148
.LBB0_131:                              # %.lr.ph.i165.preheader.i
                                        #   in Loop: Header=BB0_93 Depth=2
	testl	%ebp, %ebp
	je	.LBB0_137
.LBB0_132:                              # %.lr.ph.i165.1565.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movzwl	%r13w, %r13d
	leal	(,%rsi,4), %edx
	shrl	$15, %r13d
	addl	$-2, %ecx
	je	.LBB0_135
# BB#133:                               # %.lr.ph.i165.2566.i
                                        #   in Loop: Header=BB0_93 Depth=2
	shll	$16, %r13d
	movzwl	%dx, %edi
	orl	%r13d, %edi
	shll	$3, %esi
	shrl	$15, %edi
	cmpl	$1, %ecx
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB0_139
# BB#134:                               # %.lr.ph.i165.3567.thread.i
                                        #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movzbl	(%rbp,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rbp,%rax), %edx
	orl	%esi, %edx
	orl	%ecx, %edx
	shll	$16, %edi
	movzwl	%dx, %esi
	orl	%edi, %esi
	addl	%edx, %edx
	movl	$15, %ecx
	jmp	.LBB0_141
.LBB0_135:                              #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rbp            # 8-byte Reload
	movzbl	(%rbp,%rcx), %ecx
	shll	$8, %ecx
	movzbl	(%rbp,%rax), %eax
	orl	%edx, %eax
	orl	%ecx, %eax
	movl	$16, %ecx
	movw	%ax, %dx
	jmp	.LBB0_138
.LBB0_136:                              #   in Loop: Header=BB0_93 Depth=2
	xorl	%eax, %eax
	cmpl	$8, %ebp
	jb	.LBB0_167
	jmp	.LBB0_169
.LBB0_137:                              # %.lr.ph.i165.1565.thread.i
                                        #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rbp            # 8-byte Reload
	movzbl	(%rbp,%rcx), %ecx
	shll	$8, %ecx
	orl	%ecx, %r13d
	movzbl	(%rbp,%rax), %edx
	orl	%r13d, %edx
	andl	$32768, %r13d           # imm = 0x8000
	addl	%edx, %edx
	shrl	$15, %r13d
	movl	$15, %ecx
.LBB0_138:                              # %.lr.ph.i165.2566.i.thread
                                        #   in Loop: Header=BB0_93 Depth=2
	addl	$2, %r9d
	shll	$16, %r13d
	movzwl	%dx, %edi
	orl	%r13d, %edi
	leal	(%rdx,%rdx), %esi
	shrl	$15, %edi
.LBB0_139:                              # %.lr.ph.i165.3567.i
                                        #   in Loop: Header=BB0_93 Depth=2
	shll	$16, %edi
	movzwl	%si, %esi
	orl	%edi, %esi
	shll	$2, %edx
	addl	$-2, %ecx
	jne	.LBB0_141
# BB#140:                               #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movzbl	(%rbp,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rbp,%rax), %eax
	orl	%edx, %eax
	orl	%ecx, %eax
	movl	$16, %ecx
	movw	%ax, %dx
.LBB0_141:                              # %.lr.ph.i165.4568.i
                                        #   in Loop: Header=BB0_93 Depth=2
	andl	$536838144, %esi        # imm = 0x1FFF8000
	movzwl	%dx, %eax
	leal	(%rax,%rsi,2), %edi
	leal	(%rdx,%rdx), %esi
	decl	%ecx
	jne	.LBB0_143
# BB#142:                               #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movzbl	(%rbp,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rbp,%rax), %eax
	orl	%ecx, %eax
	orl	%esi, %eax
	movl	$16, %ecx
	movw	%ax, %si
.LBB0_143:                              # %getbits.exit170569.i
                                        #   in Loop: Header=BB0_93 Depth=2
	andl	$1073709056, %edi       # imm = 0x3FFF8000
	movzwl	%si, %eax
	leal	(%rax,%rdi,2), %ebx
	leal	(%rsi,%rsi), %r13d
	shrl	$15, %ebx
	leal	-1(%rcx), %ebp
	movl	$10, %r10d
	cmpl	$31, %ebx
	jne	.LBB0_147
# BB#144:                               #   in Loop: Header=BB0_93 Depth=2
	cmpl	$7, %ebp
	ja	.LBB0_162
# BB#145:                               #   in Loop: Header=BB0_93 Depth=2
	movl	$8, %eax
	subl	%ecx, %eax
	shrl	$4, %eax
	leal	2(%rax,%rax), %eax
	movl	28(%rsp), %edx          # 4-byte Reload
	subl	%r9d, %edx
	cmpl	%edx, %eax
	jbe	.LBB0_161
# BB#146:                               # %getbits.exit177.thread.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movq	%r9, 8(%rsp)            # 8-byte Spill
	xorl	%ebx, %ebx
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	48(%rsp), %r11d         # 4-byte Reload
	movl	$41, %r10d
	movl	$1, %r8d
	jmp	.LBB0_148
.LBB0_147:                              #   in Loop: Header=BB0_93 Depth=2
	movq	%r9, 8(%rsp)            # 8-byte Spill
.LBB0_148:                              # %.loopexit.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movl	16(%rsp), %eax          # 4-byte Reload
.LBB0_149:                              # %.loopexit.i
                                        #   in Loop: Header=BB0_93 Depth=2
	leal	2(%rbx,%r10), %r9d
	cmpl	%r11d, %r9d
	jae	.LBB0_189
# BB#150:                               #   in Loop: Header=BB0_93 Depth=2
	leal	3(%rbx,%r10), %r10d
	movl	%eax, %edx
	addq	%r15, %rdx
	movl	%r10d, %ecx
	addq	%rcx, %rdx
	cmpq	56(%rsp), %rdx          # 8-byte Folded Reload
	ja	.LBB0_189
# BB#151:                               #   in Loop: Header=BB0_93 Depth=2
	cmpq	%r15, %rdx
	jbe	.LBB0_189
# BB#152:                               #   in Loop: Header=BB0_93 Depth=2
	movl	%eax, %edx
	subl	%r12d, %edx
	addq	%r15, %rdx
	addq	%rcx, %rdx
	cmpq	56(%rsp), %rdx          # 8-byte Folded Reload
	ja	.LBB0_189
# BB#153:                               #   in Loop: Header=BB0_93 Depth=2
	cmpq	%r15, %rdx
	jbe	.LBB0_189
# BB#154:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_93 Depth=2
	movl	%r10d, %ecx
	andl	$3, %ecx
	movl	%r10d, %edx
	movl	%eax, %esi
	je	.LBB0_157
# BB#155:                               # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB0_93 Depth=2
	movl	%r12d, %edi
	negl	%edi
	negl	%ecx
	movl	%r10d, %edx
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	%eax, %esi
.LBB0_156:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_93 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	decl	%edx
	leal	(%rdi,%rsi), %eax
	movzbl	(%r15,%rax), %eax
	movl	%esi, %ebx
	movb	%al, (%r15,%rbx)
	incl	%esi
	incl	%ecx
	jne	.LBB0_156
.LBB0_157:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB0_93 Depth=2
	cmpl	$3, %r9d
	jb	.LBB0_160
# BB#158:                               # %.lr.ph.i.preheader.new
                                        #   in Loop: Header=BB0_93 Depth=2
	movl	%esi, %edi
	subl	%r12d, %edi
	xorl	%ebx, %ebx
.LBB0_159:                              # %.lr.ph.i
                                        #   Parent Loop BB0_18 Depth=1
                                        #     Parent Loop BB0_93 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rsi,%rbx), %eax
	leal	(%rdi,%rbx), %ecx
	movzbl	(%r15,%rcx), %ecx
	movb	%cl, (%r15,%rax)
	leal	1(%rsi,%rbx), %eax
	leal	1(%rdi,%rbx), %ecx
	movzbl	(%r15,%rcx), %ecx
	movb	%cl, (%r15,%rax)
	leal	2(%rsi,%rbx), %eax
	leal	2(%rdi,%rbx), %ecx
	movzbl	(%r15,%rcx), %ecx
	movb	%cl, (%r15,%rax)
	leal	3(%rsi,%rbx), %eax
	leal	3(%rdi,%rbx), %ecx
	movzbl	(%r15,%rcx), %ecx
	movb	%cl, (%r15,%rax)
	addl	$4, %ebx
	cmpl	%ebx, %edx
	jne	.LBB0_159
.LBB0_160:                              # %._crit_edge.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movl	16(%rsp), %edi          # 4-byte Reload
	addl	%r10d, %edi
	testl	%r8d, %r8d
	je	.LBB0_93
	jmp	.LBB0_189
.LBB0_161:                              # %.lr.ph.i172.preheader.i
                                        #   in Loop: Header=BB0_93 Depth=2
	testl	%ebp, %ebp
	je	.LBB0_56
.LBB0_162:                              # %.lr.ph.i172.1570.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movzwl	%r13w, %r13d
	leal	(,%rsi,4), %edx
	shrl	$15, %r13d
	addl	$-2, %ecx
	je	.LBB0_165
# BB#163:                               # %.lr.ph.i172.2571.i
                                        #   in Loop: Header=BB0_93 Depth=2
	shll	$16, %r13d
	movzwl	%dx, %edi
	orl	%r13d, %edi
	shll	$3, %esi
	shrl	$15, %edi
	cmpl	$1, %ecx
	jne	.LBB0_58
# BB#164:                               # %.lr.ph.i172.3572.thread.i
                                        #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdx,%rax), %edx
	orl	%ecx, %edx
	orl	%esi, %edx
	shll	$16, %edi
	movzwl	%dx, %esi
	orl	%edi, %esi
	addl	%edx, %edx
	movl	$15, %ecx
	jmp	.LBB0_60
.LBB0_165:                              #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rsi            # 8-byte Reload
	movzbl	(%rsi,%rcx), %ecx
	shll	$8, %ecx
	movzbl	(%rsi,%rax), %eax
	orl	%ecx, %eax
	orl	%edx, %eax
	movl	$16, %ecx
	movw	%ax, %dx
	jmp	.LBB0_57
.LBB0_166:                              # %.thread406.i
                                        #   in Loop: Header=BB0_93 Depth=2
	xorl	%ebp, %ebp
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%rsp), %r9            # 8-byte Reload
	movl	$1, %eax
.LBB0_167:                              #   in Loop: Header=BB0_93 Depth=2
	movl	$7, %ecx
	subl	%ebp, %ecx
	shrl	$4, %ecx
	leal	2(%rcx,%rcx), %ecx
	movl	28(%rsp), %edx          # 4-byte Reload
	subl	%r9d, %edx
	cmpl	%edx, %ecx
	ja	.LBB0_188
# BB#168:                               # %.lr.ph.i186.i
                                        #   in Loop: Header=BB0_93 Depth=2
	testl	%ebp, %ebp
	je	.LBB0_173
.LBB0_169:                              # %.lr.ph.i186.thread.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movzwl	%r13w, %esi
	leal	(%r13,%r13), %ecx
	shrl	$15, %esi
	decl	%ebp
	je	.LBB0_172
# BB#170:                               #   in Loop: Header=BB0_93 Depth=2
	shll	$16, %esi
	movzwl	%cx, %edx
	orl	%esi, %edx
	shll	$2, %r13d
	shrl	$15, %edx
	cmpl	$1, %ebp
	jne	.LBB0_175
# BB#171:                               # %.thread615.i
                                        #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %ecx
	movl	%r9d, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rsi), %esi
	shll	$8, %esi
	addl	$2, %r9d
	movzbl	(%rdi,%rcx), %ecx
	orl	%r13d, %ecx
	orl	%esi, %ecx
	shll	$16, %edx
	movzwl	%cx, %esi
	orl	%edx, %esi
	addl	%ecx, %ecx
	movl	$15, %ebp
	movl	48(%rsp), %r11d         # 4-byte Reload
	jmp	.LBB0_177
.LBB0_172:                              #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %edx
	movl	%r9d, %edi
	movq	(%rsp), %rbp            # 8-byte Reload
	movzbl	(%rbp,%rdi), %edi
	shll	$8, %edi
	movzbl	(%rbp,%rdx), %edx
	orl	%ecx, %edx
	orl	%edi, %edx
	movl	$16, %ebp
	movw	%dx, %cx
	jmp	.LBB0_174
.LBB0_173:                              # %.thread.i
                                        #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %ecx
	movl	%r9d, %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movzbl	(%rsi,%rdx), %edx
	shll	$8, %edx
	orl	%edx, %r13d
	movzbl	(%rsi,%rcx), %ecx
	orl	%r13d, %ecx
	andl	$32768, %r13d           # imm = 0x8000
	addl	%ecx, %ecx
	shrl	$15, %r13d
	movl	$15, %ebp
	movl	%r13d, %esi
.LBB0_174:                              # %.thread
                                        #   in Loop: Header=BB0_93 Depth=2
	addl	$2, %r9d
	shll	$16, %esi
	movzwl	%cx, %edx
	orl	%esi, %edx
	leal	(%rcx,%rcx), %r13d
	shrl	$15, %edx
.LBB0_175:                              #   in Loop: Header=BB0_93 Depth=2
	movl	48(%rsp), %r11d         # 4-byte Reload
	shll	$16, %edx
	movzwl	%r13w, %esi
	orl	%edx, %esi
	shll	$2, %ecx
	addl	$-2, %ebp
	jne	.LBB0_177
# BB#176:                               #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %edx
	movl	%r9d, %edi
	movq	(%rsp), %rbp            # 8-byte Reload
	movzbl	(%rbp,%rdi), %edi
	shll	$8, %edi
	addl	$2, %r9d
	movzbl	(%rbp,%rdx), %edx
	orl	%ecx, %edx
	orl	%edi, %edx
	movl	$16, %ebp
	movw	%dx, %cx
.LBB0_177:                              #   in Loop: Header=BB0_93 Depth=2
	andl	$229376, %esi           # imm = 0x38000
	movzwl	%cx, %edx
	leal	(%rdx,%rsi,2), %edx
	leal	(%rcx,%rcx), %esi
	decl	%ebp
	jne	.LBB0_179
# BB#178:                               #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %ecx
	movl	%r9d, %edi
	movq	(%rsp), %rbp            # 8-byte Reload
	movzbl	(%rbp,%rdi), %edi
	shll	$8, %edi
	addl	$2, %r9d
	movzbl	(%rbp,%rcx), %ecx
	orl	%edi, %ecx
	orl	%esi, %ecx
	movl	$16, %ebp
	movw	%cx, %si
.LBB0_179:                              #   in Loop: Header=BB0_93 Depth=2
	andl	$491520, %edx           # imm = 0x78000
	movzwl	%si, %ecx
	leal	(%rcx,%rdx,2), %ecx
	leal	(%rsi,%rsi), %edx
	decl	%ebp
	jne	.LBB0_181
# BB#180:                               #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %esi
	movl	%r9d, %edi
	movq	(%rsp), %rbp            # 8-byte Reload
	movzbl	(%rbp,%rdi), %edi
	shll	$8, %edi
	addl	$2, %r9d
	movzbl	(%rbp,%rsi), %esi
	orl	%edi, %esi
	orl	%edx, %esi
	movl	$16, %ebp
	movw	%si, %dx
.LBB0_181:                              #   in Loop: Header=BB0_93 Depth=2
	andl	$1015808, %ecx          # imm = 0xF8000
	movzwl	%dx, %esi
	leal	(%rsi,%rcx,2), %ecx
	leal	(%rdx,%rdx), %edx
	decl	%ebp
	jne	.LBB0_183
# BB#182:                               #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %esi
	movl	%r9d, %edi
	movq	(%rsp), %rbp            # 8-byte Reload
	movzbl	(%rbp,%rdi), %edi
	shll	$8, %edi
	addl	$2, %r9d
	movzbl	(%rbp,%rsi), %esi
	orl	%edi, %esi
	orl	%edx, %esi
	movl	$16, %ebp
	movw	%si, %dx
.LBB0_183:                              #   in Loop: Header=BB0_93 Depth=2
	andl	$2064384, %ecx          # imm = 0x1F8000
	movzwl	%dx, %esi
	leal	(%rsi,%rcx,2), %ecx
	leal	(%rdx,%rdx), %edx
	decl	%ebp
	je	.LBB0_185
# BB#184:                               #   in Loop: Header=BB0_93 Depth=2
	movq	%r9, 8(%rsp)            # 8-byte Spill
	jmp	.LBB0_186
.LBB0_185:                              #   in Loop: Header=BB0_93 Depth=2
	leal	1(%r9), %esi
	movl	%r9d, %edi
	movq	(%rsp), %rbp            # 8-byte Reload
	movzbl	(%rbp,%rdi), %edi
	shll	$8, %edi
	addl	$2, %r9d
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movzbl	(%rbp,%rsi), %esi
	orl	%edi, %esi
	orl	%edx, %esi
	movl	$16, %ebp
	movw	%si, %dx
.LBB0_186:                              # %getbits.exit191.i
                                        #   in Loop: Header=BB0_93 Depth=2
	movl	16(%rsp), %edi          # 4-byte Reload
	andl	$4161536, %ecx          # imm = 0x3F8000
	movzwl	%dx, %esi
	leal	(%rsi,%rcx,2), %ecx
	leal	(%rdx,%rdx), %r13d
	shrl	$15, %ecx
	decl	%ebp
	movl	%edi, %edx
	movb	%cl, (%r15,%rdx)
	incl	%edi
	testl	%eax, %eax
	je	.LBB0_93
	jmp	.LBB0_189
.LBB0_187:                              # %.critedge2.thread415.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	jmp	.LBB0_190
.LBB0_188:                              # %getbits.exit191.thread.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	16(%rsp), %eax          # 4-byte Reload
	movb	$0, (%r15,%rax)
.LBB0_189:                              # %.critedge2.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	movq	72(%rsp), %r12          # 8-byte Reload
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_190:                              #   in Loop: Header=BB0_18 Depth=1
	movl	48(%rsp), %ebp          # 4-byte Reload
.LBB0_191:                              #   in Loop: Header=BB0_18 Depth=1
	incl	68(%rsp)                # 4-byte Folded Spill
	cmpl	$2, %ebp
	movq	%r12, 72(%rsp)          # 8-byte Spill
	jae	.LBB0_193
# BB#192:                               #   in Loop: Header=BB0_18 Depth=1
	movl	%ebp, %r12d
	jmp	.LBB0_218
.LBB0_193:                              #   in Loop: Header=BB0_18 Depth=1
	cmpl	$5, %ebp
	jb	.LBB0_198
# BB#194:                               #   in Loop: Header=BB0_18 Depth=1
	cmpb	$-1, (%r15)
	jne	.LBB0_198
# BB#195:                               #   in Loop: Header=BB0_18 Depth=1
	cmpb	$-2, 1(%r15)
	jne	.LBB0_198
# BB#196:                               #   in Loop: Header=BB0_18 Depth=1
	cmpb	$0, 2(%r15)
	je	.LBB0_198
# BB#197:                               # %.thread.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	leaq	2(%r15), %rax
	addl	$-2, %ebp
	movl	%ebp, %r12d
	shrl	%r12d
	jmp	.LBB0_216
.LBB0_198:                              #   in Loop: Header=BB0_18 Depth=1
	movl	%ebp, %eax
	andl	$-2, %eax
	cmpl	$20, %ebp
	movl	$20, %ecx
	cmoval	%ecx, %eax
	testl	%eax, %eax
	je	.LBB0_202
# BB#199:                               # %.lr.ph49.preheader.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	%eax, %ecx
	leaq	-1(%rcx), %rdx
	movq	%rdx, %rdi
	shrq	%rdi
	btl	$1, %edx
	jb	.LBB0_203
# BB#200:                               # %.lr.ph49.i.i.prol
                                        #   in Loop: Header=BB0_18 Depth=1
	cmpb	$0, (%r15)
	je	.LBB0_204
# BB#201:                               #   in Loop: Header=BB0_18 Depth=1
	cmpb	$0, 1(%r15)
	sete	%dl
	jmp	.LBB0_205
.LBB0_202:                              #   in Loop: Header=BB0_18 Depth=1
	xorl	%esi, %esi
	cmpl	%eax, %esi
	jb	.LBB0_214
	jmp	.LBB0_215
.LBB0_203:                              #   in Loop: Header=BB0_18 Depth=1
	xorl	%edx, %edx
	xorl	%esi, %esi
	testq	%rdi, %rdi
	jne	.LBB0_206
	jmp	.LBB0_213
.LBB0_204:                              #   in Loop: Header=BB0_18 Depth=1
	xorl	%edx, %edx
.LBB0_205:                              #   in Loop: Header=BB0_18 Depth=1
	movzbl	%dl, %esi
	movl	$2, %edx
	testq	%rdi, %rdi
	je	.LBB0_213
.LBB0_206:                              # %.lr.ph49.i.i
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, (%r15,%rdx)
	je	.LBB0_208
# BB#207:                               #   in Loop: Header=BB0_206 Depth=2
	cmpb	$0, 1(%r15,%rdx)
	sete	%dil
	jmp	.LBB0_209
.LBB0_208:                              #   in Loop: Header=BB0_206 Depth=2
	xorl	%edi, %edi
.LBB0_209:                              # %.lr.ph49.i.i.1734
                                        #   in Loop: Header=BB0_206 Depth=2
	movzbl	%dil, %edi
	addl	%esi, %edi
	cmpb	$0, 2(%r15,%rdx)
	je	.LBB0_211
# BB#210:                               #   in Loop: Header=BB0_206 Depth=2
	cmpb	$0, 3(%r15,%rdx)
	sete	%sil
	jmp	.LBB0_212
.LBB0_211:                              #   in Loop: Header=BB0_206 Depth=2
	xorl	%esi, %esi
.LBB0_212:                              #   in Loop: Header=BB0_206 Depth=2
	movzbl	%sil, %esi
	addl	%edi, %esi
	addq	$4, %rdx
	cmpq	%rcx, %rdx
	jb	.LBB0_206
.LBB0_213:                              # %._crit_edge.loopexit.i193.i
                                        #   in Loop: Header=BB0_18 Depth=1
	shll	$2, %esi
	cmpl	%eax, %esi
	jae	.LBB0_215
.LBB0_214:                              #   in Loop: Header=BB0_18 Depth=1
	movl	%ebp, %r12d
	jmp	.LBB0_218
.LBB0_215:                              #   in Loop: Header=BB0_18 Depth=1
	movl	%ebp, %r12d
	shrl	%r12d
	testl	%ebp, %ebp
	movq	%r15, %rax
	je	.LBB0_218
.LBB0_216:                              # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB0_18 Depth=1
	xorl	%ecx, %ecx
	movq	%r15, %rdx
.LBB0_217:                              # %.lr.ph.i194.i
                                        #   Parent Loop BB0_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %esi
	movzbl	(%rax,%rsi), %ebx
	movb	%bl, (%rdx)
	incq	%rdx
	addl	$2, %ecx
	cmpl	%ebp, %ecx
	jb	.LBB0_217
.LBB0_218:                              # %u2a.exit.i
                                        #   in Loop: Header=BB0_18 Depth=1
	movl	$1023, %esi             # imm = 0x3FF
	movl	$.L.str.19, %edx
	xorl	%eax, %eax
	leaq	736(%rsp), %rbx
	movq	%rbx, %rdi
	movq	72(%rsp), %rcx          # 8-byte Reload
	movl	68(%rsp), %r8d          # 4-byte Reload
	callq	snprintf
	movb	$0, 1759(%rsp)
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movl	%eax, %ebx
	testl	%ebx, %ebx
	js	.LBB0_549
# BB#219:                               #   in Loop: Header=BB0_18 Depth=1
	movl	%ebx, %edi
	movq	%r15, %rsi
	movl	%r12d, %edx
	callq	cli_writen
	cmpl	%r12d, %eax
	jne	.LBB0_550
# BB#220:                               #   in Loop: Header=BB0_18 Depth=1
	movq	%r15, %rdi
	callq	free
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_222
# BB#221:                               #   in Loop: Header=BB0_18 Depth=1
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	leaq	736(%rsp), %rsi
	callq	cli_dbgmsg
	jmp	.LBB0_223
.LBB0_222:                              #   in Loop: Header=BB0_18 Depth=1
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_223:                              #   in Loop: Header=BB0_18 Depth=1
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	96(%rsp), %r15          # 8-byte Reload
	movl	%ebx, %edi
	callq	fsync
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebx, %edi
	callq	lseek
	movl	%ebx, %edi
	movq	%r15, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %ebp
	movl	%ebx, %edi
	callq	close
	movb	cli_leavetemps_flag(%rip), %al
	cmpl	$1, %ebp
	je	.LBB0_552
# BB#224:                               #   in Loop: Header=BB0_18 Depth=1
	testb	%al, %al
	leaq	128(%rsp), %rbx
	jne	.LBB0_18
# BB#225:                               #   in Loop: Header=BB0_18 Depth=1
	leaq	736(%rsp), %rdi
	callq	unlink
	jmp	.LBB0_18
.LBB0_226:
	xorl	%r13d, %r13d
	movl	$.L.str.4, %edi
	jmp	.LBB0_12
.LBB0_539:
	movl	$.L.str.24, %edi
	jmp	.LBB0_540
.LBB0_542:
	xorl	%r13d, %r13d
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_15
	jmp	.LBB0_14
.LBB0_299:                              #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.63, %edi
	xorl	%eax, %eax
	movq	%r9, %rbx
	movl	%r13d, %esi
	callq	cli_dbgmsg
	jmp	.LBB0_303
.LBB0_300:                              #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.60, %edi
.LBB0_301:                              # %.critedge9.i
                                        #   in Loop: Header=BB0_319 Depth=1
	xorl	%eax, %eax
	movq	%r9, %rbx
	callq	cli_dbgmsg
	jmp	.LBB0_303
.LBB0_302:                              #   in Loop: Header=BB0_319 Depth=1
	movl	%edx, %ecx
	subl	%edi, %ecx
	movq	%r8, %rsi
	movl	$.L.str.61, %edi
	xorl	%eax, %eax
	movq	%r9, %rbx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_dbgmsg
.LBB0_303:                              # %.critedge9.i
                                        #   in Loop: Header=BB0_319 Depth=1
	movq	%rbx, %r9
.LBB0_304:                              # %.critedge9.i
                                        #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.64, %edi
	xorl	%eax, %eax
	movq	%r9, %rbx
	callq	cli_dbgmsg
	movq	%rbx, %r9
	movq	96(%rsp), %r15          # 8-byte Reload
.LBB0_305:                              # %.critedge9.thread.i
                                        #   in Loop: Header=BB0_319 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_306
.LBB0_315:                              #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.54, %edi
	jmp	.LBB0_301
.LBB0_316:                              #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.56, %edi
	jmp	.LBB0_301
.LBB0_317:                              #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.58, %edi
	jmp	.LBB0_301
.LBB0_297:                              #   in Loop: Header=BB0_319 Depth=1
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jmp	.LBB0_307
.LBB0_298:                              #   in Loop: Header=BB0_319 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	xorl	%r9d, %r9d
.LBB0_306:                              # %.critedge9.thread.i
                                        #   in Loop: Header=BB0_319 Depth=1
	movq	%r9, 56(%rsp)           # 8-byte Spill
	callq	free
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	72(%rsp), %r12          # 8-byte Reload
.LBB0_307:                              #   in Loop: Header=BB0_319 Depth=1
	movl	$1023, %esi             # imm = 0x3FF
	movl	$.L.str.19, %edx
	xorl	%eax, %eax
	leaq	736(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r12, %rcx
	movl	28(%rsp), %r8d          # 4-byte Reload
	callq	snprintf
	movb	$0, 1759(%rsp)
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movl	%eax, %r13d
	testl	%r13d, %r13d
	js	.LBB0_544
# BB#308:                               #   in Loop: Header=BB0_319 Depth=1
	movl	%r13d, %edi
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rsi
	movq	56(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %edx
	callq	cli_writen
	cmpl	%ebx, %eax
	jne	.LBB0_545
# BB#309:                               #   in Loop: Header=BB0_319 Depth=1
	movq	%rbp, %rdi
	callq	free
	cmpb	$0, 68(%rsp)            # 1-byte Folded Reload
	movl	$.L.str.67, %esi
	movl	$.L.str.66, %eax
	cmovneq	%rax, %rsi
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_311
# BB#310:                               #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.65, %edi
	xorl	%eax, %eax
	leaq	736(%rsp), %rdx
	callq	cli_dbgmsg
	jmp	.LBB0_312
.LBB0_311:                              #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.68, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_312:                              #   in Loop: Header=BB0_319 Depth=1
	movl	%r13d, %edi
	callq	fsync
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r13d, %edi
	callq	lseek
	movl	%r13d, %edi
	movq	%r15, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %ebp
	movl	%r13d, %edi
	callq	close
	movb	cli_leavetemps_flag(%rip), %al
	cmpl	$1, %ebp
	je	.LBB0_547
# BB#313:                               #   in Loop: Header=BB0_319 Depth=1
	testb	%al, %al
	leaq	128(%rsp), %rbx
	jne	.LBB0_319
# BB#314:                               #   in Loop: Header=BB0_319 Depth=1
	leaq	736(%rsp), %rdi
	callq	unlink
.LBB0_319:                              # %.backedge654.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_342 Depth 2
                                        #     Child Loop BB0_366 Depth 2
                                        #     Child Loop BB0_389 Depth 2
                                        #       Child Loop BB0_399 Depth 3
                                        #       Child Loop BB0_495 Depth 3
                                        #       Child Loop BB0_499 Depth 3
                                        #     Child Loop BB0_526 Depth 2
                                        #       Child Loop BB0_243 Depth 3
                                        #       Child Loop BB0_280 Depth 3
                                        #       Child Loop BB0_290 Depth 3
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB0_321
# BB#320:                               #   in Loop: Header=BB0_319 Depth=1
	movl	4(%rax), %esi
	leal	-1(%rsi), %eax
	cmpl	28(%rsp), %eax          # 4-byte Folded Reload
	jb	.LBB0_538
.LBB0_321:                              # %.critedge.i21
                                        #   in Loop: Header=BB0_319 Depth=1
	movl	$8, %edx
	movl	%r14d, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	xorl	%r13d, %r13d
	cmpl	$8, %eax
	jne	.LBB0_13
# BB#322:                               #   in Loop: Header=BB0_319 Depth=1
	cmpl	$1388987243, 128(%rsp)  # imm = 0x52CA436B
	jne	.LBB0_537
# BB#323:                               #   in Loop: Header=BB0_319 Depth=1
	movl	132(%rsp), %r15d
	movl	$44476, %eax            # imm = 0xADBC
	xorl	%eax, %r15d
	movl	%r15d, %ebp
	addl	%ebp, %ebp
	js	.LBB0_13
# BB#324:                               #   in Loop: Header=BB0_319 Depth=1
	cmpl	$299, %r15d             # imm = 0x12B
	ja	.LBB0_330
# BB#325:                               #   in Loop: Header=BB0_319 Depth=1
	movl	%r14d, %edi
	movq	%rbx, %rsi
	movl	%ebp, %edx
	callq	cli_readn
	cmpl	%ebp, %eax
	jne	.LBB0_13
# BB#326:                               #   in Loop: Header=BB0_319 Depth=1
	leal	45887(%r15), %eax
	movzwl	%ax, %edx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	LAME_decrypt
	cmpl	$2, %ebp
	jb	.LBB0_343
# BB#327:                               #   in Loop: Header=BB0_319 Depth=1
	cmpl	$5, %ebp
	setb	%al
	movb	128(%rsp), %dl
	cmpb	$-1, %dl
	setne	%cl
	cmpb	$-2, 129(%rsp)
	setne	%sil
	cmpb	$0, 130(%rsp)
	je	.LBB0_331
# BB#328:                               #   in Loop: Header=BB0_319 Depth=1
	orb	%sil, %cl
	orb	%cl, %al
	jne	.LBB0_331
# BB#329:                               # %.thread.i.i22
                                        #   in Loop: Header=BB0_319 Depth=1
	addl	$-2, %ebp
	movl	$2, %eax
	jmp	.LBB0_341
.LBB0_330:                              #   in Loop: Header=BB0_319 Depth=1
	movl	$0, 68(%rsp)            # 4-byte Folded Spill
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebp, %esi
	movl	$1, %edx
	movl	%r14d, %edi
	callq	lseek
	jmp	.LBB0_345
.LBB0_331:                              #   in Loop: Header=BB0_319 Depth=1
	cmpl	$20, %ebp
	movl	$20, %eax
	cmovbl	%ebp, %eax
	testl	%eax, %eax
	je	.LBB0_338
# BB#332:                               # %.lr.ph49.preheader.i.i23
                                        #   in Loop: Header=BB0_319 Depth=1
	movl	%eax, %esi
	xorl	%ecx, %ecx
	movl	$2, %edi
	testb	%dl, %dl
	jne	.LBB0_334
	jmp	.LBB0_335
	.p2align	4, 0x90
.LBB0_333:                              # %..lr.ph49.i_crit_edge.i
                                        #   in Loop: Header=BB0_319 Depth=1
	movb	128(%rsp,%rdi), %dl
	addq	$2, %rdi
	testb	%dl, %dl
	je	.LBB0_335
.LBB0_334:                              #   in Loop: Header=BB0_319 Depth=1
	cmpb	$0, 127(%rsp,%rdi)
	sete	%dl
	jmp	.LBB0_336
.LBB0_335:                              #   in Loop: Header=BB0_319 Depth=1
	xorl	%edx, %edx
.LBB0_336:                              #   in Loop: Header=BB0_319 Depth=1
	movzbl	%dl, %edx
	addl	%edx, %ecx
	cmpq	%rsi, %rdi
	jb	.LBB0_333
# BB#337:                               # %._crit_edge.loopexit.i.i
                                        #   in Loop: Header=BB0_319 Depth=1
	shll	$2, %ecx
	testl	%ebp, %ebp
	jne	.LBB0_339
	jmp	.LBB0_343
.LBB0_338:                              #   in Loop: Header=BB0_319 Depth=1
	xorl	%ecx, %ecx
	testl	%ebp, %ebp
	je	.LBB0_343
.LBB0_339:                              # %._crit_edge.i.i31
                                        #   in Loop: Header=BB0_319 Depth=1
	cmpl	%eax, %ecx
	jb	.LBB0_343
# BB#340:                               #   in Loop: Header=BB0_319 Depth=1
	xorl	%eax, %eax
.LBB0_341:                              # %.lr.ph.preheader.i.i33
                                        #   in Loop: Header=BB0_319 Depth=1
	leaq	128(%rsp,%rax), %rax
	xorl	%ecx, %ecx
	leaq	128(%rsp), %rdx
	.p2align	4, 0x90
.LBB0_342:                              # %.lr.ph.i.i
                                        #   Parent Loop BB0_319 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %esi
	movzbl	(%rax,%rsi), %ebx
	movb	%bl, (%rdx)
	incq	%rdx
	addl	$2, %ecx
	cmpl	%ebp, %ecx
	jb	.LBB0_342
.LBB0_343:                              # %u2a.exit.i36
                                        #   in Loop: Header=BB0_319 Depth=1
	movl	$0, 68(%rsp)            # 4-byte Folded Spill
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	leaq	128(%rsp), %rbx
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	cmpl	$19, %r15d
	jne	.LBB0_345
# BB#344:                               #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.50, %edi
	movl	$19, %edx
	movq	%rbx, %rsi
	callq	memcmp
	testl	%eax, %eax
	sete	%al
	movl	%eax, 68(%rsp)          # 4-byte Spill
.LBB0_345:                              #   in Loop: Header=BB0_319 Depth=1
	movl	$4, %edx
	movl	%r14d, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB0_13
# BB#346:                               #   in Loop: Header=BB0_319 Depth=1
	movl	128(%rsp), %ebp
	movl	$63520, %eax            # imm = 0xF820
	xorl	%eax, %ebp
	movl	%ebp, %r15d
	addl	%r15d, %r15d
	js	.LBB0_13
# BB#347:                               #   in Loop: Header=BB0_319 Depth=1
	cmpl	$299, %ebp              # imm = 0x12B
	ja	.LBB0_354
# BB#348:                               #   in Loop: Header=BB0_319 Depth=1
	movb	cli_debug_flag(%rip), %al
	testb	%al, %al
	je	.LBB0_354
# BB#349:                               #   in Loop: Header=BB0_319 Depth=1
	movl	%r14d, %edi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	callq	cli_readn
	cmpl	%r15d, %eax
	jne	.LBB0_13
# BB#350:                               #   in Loop: Header=BB0_319 Depth=1
	addl	$62585, %ebp            # imm = 0xF479
	movzwl	%bp, %edx
	movq	%rbx, %rdi
	movl	%r15d, %esi
	callq	LAME_decrypt
	movl	%r15d, %eax
	movb	$0, 128(%rsp,%rax)
	movl	%r15d, %eax
	orl	$1, %eax
	movb	$0, 128(%rsp,%rax)
	cmpl	$2, %r15d
	jb	.LBB0_367
# BB#351:                               #   in Loop: Header=BB0_319 Depth=1
	cmpl	$5, %r15d
	setb	%al
	movb	128(%rsp), %dl
	cmpb	$-1, %dl
	setne	%cl
	cmpb	$-2, 129(%rsp)
	setne	%sil
	cmpb	$0, 130(%rsp)
	je	.LBB0_355
# BB#352:                               #   in Loop: Header=BB0_319 Depth=1
	orb	%sil, %cl
	orb	%cl, %al
	jne	.LBB0_355
# BB#353:                               # %.thread.i274.i
                                        #   in Loop: Header=BB0_319 Depth=1
	addl	$-2, %r15d
	movl	$2, %eax
	jmp	.LBB0_365
.LBB0_354:                              #   in Loop: Header=BB0_319 Depth=1
	movl	%r15d, %esi
	movl	$1, %edx
	movl	%r14d, %edi
	callq	lseek
	jmp	.LBB0_368
.LBB0_355:                              #   in Loop: Header=BB0_319 Depth=1
	cmpl	$20, %r15d
	movl	$20, %eax
	cmovbl	%r15d, %eax
	testl	%eax, %eax
	je	.LBB0_362
# BB#356:                               # %.lr.ph49.preheader.i275.i
                                        #   in Loop: Header=BB0_319 Depth=1
	movl	%eax, %esi
	xorl	%ecx, %ecx
	movl	$2, %edi
	testb	%dl, %dl
	jne	.LBB0_358
	jmp	.LBB0_359
	.p2align	4, 0x90
.LBB0_357:                              # %..lr.ph49.i278_crit_edge.i
                                        #   in Loop: Header=BB0_319 Depth=1
	movb	128(%rsp,%rdi), %dl
	addq	$2, %rdi
	testb	%dl, %dl
	je	.LBB0_359
.LBB0_358:                              #   in Loop: Header=BB0_319 Depth=1
	cmpb	$0, 127(%rsp,%rdi)
	sete	%dl
	jmp	.LBB0_360
.LBB0_359:                              #   in Loop: Header=BB0_319 Depth=1
	xorl	%edx, %edx
.LBB0_360:                              #   in Loop: Header=BB0_319 Depth=1
	movzbl	%dl, %edx
	addl	%edx, %ecx
	cmpq	%rsi, %rdi
	jb	.LBB0_357
# BB#361:                               # %._crit_edge.loopexit.i281.i
                                        #   in Loop: Header=BB0_319 Depth=1
	shll	$2, %ecx
	testl	%r15d, %r15d
	jne	.LBB0_363
	jmp	.LBB0_367
.LBB0_362:                              #   in Loop: Header=BB0_319 Depth=1
	xorl	%ecx, %ecx
	testl	%r15d, %r15d
	je	.LBB0_367
.LBB0_363:                              # %._crit_edge.i284.i
                                        #   in Loop: Header=BB0_319 Depth=1
	cmpl	%eax, %ecx
	jb	.LBB0_367
# BB#364:                               #   in Loop: Header=BB0_319 Depth=1
	xorl	%eax, %eax
.LBB0_365:                              # %.lr.ph.preheader.i287.i
                                        #   in Loop: Header=BB0_319 Depth=1
	leaq	128(%rsp,%rax), %rax
	xorl	%ecx, %ecx
	leaq	128(%rsp), %rdx
	.p2align	4, 0x90
.LBB0_366:                              # %.lr.ph.i290.i
                                        #   Parent Loop BB0_319 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %esi
	movzbl	(%rax,%rsi), %ebx
	movb	%bl, (%rdx)
	incq	%rdx
	addl	$2, %ecx
	cmpl	%r15d, %ecx
	jb	.LBB0_366
.LBB0_367:                              # %u2a.exit292.i
                                        #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	leaq	128(%rsp), %rbx
	movq	%rbx, %rsi
	callq	cli_dbgmsg
.LBB0_368:                              #   in Loop: Header=BB0_319 Depth=1
	movl	$13, %edx
	movl	%r14d, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	cmpl	$13, %eax
	movq	96(%rsp), %r15          # 8-byte Reload
	jne	.LBB0_13
# BB#369:                               #   in Loop: Header=BB0_319 Depth=1
	movl	129(%rsp), %ebp
	testl	%ebp, %ebp
	js	.LBB0_536
# BB#370:                               #   in Loop: Header=BB0_319 Depth=1
	movl	%r14d, %edi
	movb	128(%rsp), %r14b
	xorl	$34748, %ebp            # imm = 0x87BC
	movl	$16, %esi
	movl	$1, %edx
	movl	%edi, %r13d
	callq	lseek
	testl	%ebp, %ebp
	je	.LBB0_381
# BB#371:                               #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	movl	133(%rsp), %esi
	xorl	$34748, %esi            # imm = 0x87BC
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	137(%rsp), %esi
	xorl	$42629, %esi            # imm = 0xA685
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB0_374
# BB#372:                               #   in Loop: Header=BB0_319 Depth=1
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB0_374
# BB#373:                               #   in Loop: Header=BB0_319 Depth=1
	movl	%ebp, %ebx
	cmpq	%rdx, %rbx
	ja	.LBB0_318
.LBB0_374:                              #   in Loop: Header=BB0_319 Depth=1
	movl	%ebp, %edi
	callq	cli_malloc
	movl	%ebp, %ebx
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_543
# BB#375:                               #   in Loop: Header=BB0_319 Depth=1
	movl	%r13d, %edi
	movq	%rbp, %rsi
	movl	%ebx, %edx
	callq	cli_readn
	cmpl	%ebx, %eax
	jne	.LBB0_541
# BB#376:                               #   in Loop: Header=BB0_319 Depth=1
	incl	28(%rsp)                # 4-byte Folded Spill
	movl	$9335, %edx             # imm = 0x2477
	movq	%rbp, %rdi
	movl	%ebx, %esi
	callq	LAME_decrypt
	cmpb	$1, %r14b
	jne	.LBB0_382
# BB#377:                               #   in Loop: Header=BB0_319 Depth=1
	movl	%ebx, 56(%rsp)          # 4-byte Spill
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$909132101, (%rbp)      # imm = 0x36304145
	leaq	128(%rsp), %rbx
	movl	%r13d, %r14d
	jne	.LBB0_383
# BB#378:                               #   in Loop: Header=BB0_319 Depth=1
	movl	4(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, %ecx
	bswapl	%ecx
	cmovel	56(%rsp), %ecx          # 4-byte Folded Reload
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB0_384
# BB#379:                               #   in Loop: Header=BB0_319 Depth=1
	movq	24(%rax), %rax
	movl	%ecx, %edi
	testq	%rax, %rax
	je	.LBB0_385
# BB#380:                               #   in Loop: Header=BB0_319 Depth=1
	cmpq	%rax, %rdi
	ja	.LBB0_520
	jmp	.LBB0_385
.LBB0_381:                              #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r13d, %r14d
	jmp	.LBB0_319
.LBB0_382:                              #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebx, %eax
	leaq	128(%rsp), %rbx
	movl	%r13d, %r14d
	cmpl	$3, %eax
	jbe	.LBB0_518
	jmp	.LBB0_521
.LBB0_383:                              #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.15, %edi
	jmp	.LBB0_519
.LBB0_384:                              # %._crit_edge840.i
                                        #   in Loop: Header=BB0_319 Depth=1
	movl	%ecx, %edi
.LBB0_385:                              #   in Loop: Header=BB0_319 Depth=1
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r12, 72(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	callq	cli_malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_555
# BB#386:                               #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %r15          # 8-byte Reload
	movl	%r15d, %esi
	callq	cli_dbgmsg
	xorl	%esi, %esi
	movq	%r15, %r10
	addq	%r13, %rbp
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movl	$8, %r9d
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	movq	%r13, %r11
	movq	%r11, 32(%rsp)          # 8-byte Spill
	cmpl	%r10d, %esi
	jb	.LBB0_389
	jmp	.LBB0_515
.LBB0_387:                              # %getbits.exit.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$4161536, %ecx          # imm = 0x3F8000
	movzwl	%dx, %esi
	leal	(%rsi,%rcx,2), %ecx
	leal	(%rdx,%rdx), %r15d
	shrl	$15, %ecx
	decl	%eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%esi, %edx
	movb	%cl, (%r11,%rdx)
	incl	%esi
	movl	%eax, %r12d
.LBB0_388:                              #   in Loop: Header=BB0_389 Depth=2
	cmpl	%r10d, %esi
	jae	.LBB0_515
.LBB0_389:                              #   Parent Loop BB0_319 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_399 Depth 3
                                        #       Child Loop BB0_495 Depth 3
                                        #       Child Loop BB0_499 Depth 3
	testl	%r12d, %r12d
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	je	.LBB0_391
# BB#390:                               #   in Loop: Header=BB0_389 Depth=2
	movw	%r15w, %dx
	movl	%r12d, %eax
	jmp	.LBB0_394
.LBB0_391:                              #   in Loop: Header=BB0_389 Depth=2
	movl	56(%rsp), %eax          # 4-byte Reload
	subl	%r9d, %eax
	cmpl	$1, %eax
	ja	.LBB0_393
# BB#392:                               # %.thread.i39
                                        #   in Loop: Header=BB0_389 Depth=2
	xorl	%r12d, %r12d
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movq	%r9, %rbp
	callq	cli_dbgmsg
	movq	%rbp, %r9
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	movl	$1, %r8d
	jmp	.LBB0_396
.LBB0_393:                              #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdx,%rax), %edx
	orl	%r15d, %edx
	orl	%ecx, %edx
	movl	$16, %eax
.LBB0_394:                              # %getbits.exit308803.i
                                        #   in Loop: Header=BB0_389 Depth=2
	leal	(%rdx,%rdx), %r15d
	leal	-1(%rax), %r12d
	testw	%dx, %dx
	js	.LBB0_430
# BB#395:                               #   in Loop: Header=BB0_389 Depth=2
	xorl	%r8d, %r8d
	cmpl	$15, %r12d
	jae	.LBB0_398
.LBB0_396:                              #   in Loop: Header=BB0_389 Depth=2
	movl	$14, %eax
	subl	%r12d, %eax
	shrl	$4, %eax
	leal	2(%rax,%rax), %eax
	movl	56(%rsp), %ecx          # 4-byte Reload
	subl	%r9d, %ecx
	cmpl	%ecx, %eax
	jbe	.LBB0_398
# BB#397:                               #   in Loop: Header=BB0_389 Depth=2
	xorl	%ebp, %ebp
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movq	%r9, %r13
	callq	cli_dbgmsg
	movq	%r13, %r9
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	movl	$1, %r8d
	cmpl	$1, %r12d
	jbe	.LBB0_403
	jmp	.LBB0_405
.LBB0_398:                              # %.lr.ph.i310.i
                                        #   in Loop: Header=BB0_389 Depth=2
	xorl	%ebp, %ebp
	movl	$-15, %ecx
.LBB0_399:                              #   Parent Loop BB0_319 Depth=1
                                        #     Parent Loop BB0_389 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%r12d, %r12d
	jne	.LBB0_401
# BB#400:                               #   in Loop: Header=BB0_399 Depth=3
	leal	1(%r9), %eax
	movl	%r9d, %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movzbl	(%rsi,%rdx), %edx
	shll	$8, %edx
	addl	$2, %r9d
	movzbl	(%rsi,%rax), %eax
	orl	%r15d, %eax
	orl	%edx, %eax
	movl	$16, %r12d
	movw	%ax, %r15w
.LBB0_401:                              #   in Loop: Header=BB0_399 Depth=3
	shll	$16, %ebp
	movzwl	%r15w, %eax
	orl	%ebp, %eax
	leal	(%r15,%r15), %r15d
	shrl	$15, %eax
	movzwl	%ax, %ebp
	decl	%r12d
	incl	%ecx
	jne	.LBB0_399
# BB#402:                               # %getbits.exit316.i
                                        #   in Loop: Header=BB0_389 Depth=2
	cmpl	$1, %r12d
	ja	.LBB0_405
.LBB0_403:                              #   in Loop: Header=BB0_389 Depth=2
	movl	$1, %eax
	subl	%r12d, %eax
	shrl	$4, %eax
	leal	2(%rax,%rax), %eax
	movl	56(%rsp), %ecx          # 4-byte Reload
	subl	%r9d, %ecx
	cmpl	%ecx, %eax
	jbe	.LBB0_407
# BB#404:                               # %getbits.exit324.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%edi, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	movl	$1, %r8d
	xorl	%r13d, %r13d
	jmp	.LBB0_488
.LBB0_405:                              # %.lr.ph.i318.1804.i
                                        #   in Loop: Header=BB0_389 Depth=2
	movzwl	%r15w, %esi
	leal	(%r15,%r15), %edx
	shrl	$15, %esi
	decl	%r12d
	je	.LBB0_409
# BB#406:                               #   in Loop: Header=BB0_389 Depth=2
	movl	%r12d, %ecx
	jmp	.LBB0_411
.LBB0_407:                              # %.lr.ph.i318.preheader.i
                                        #   in Loop: Header=BB0_389 Depth=2
	testl	%r12d, %r12d
	je	.LBB0_410
# BB#408:                               # %.lr.ph.i318.1804.i.thread
                                        #   in Loop: Header=BB0_389 Depth=2
	movzwl	%r15w, %esi
	leal	(%r15,%r15), %edx
	shrl	$15, %esi
.LBB0_409:                              #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%edx, %eax
	orl	%ecx, %eax
	movl	$16, %ecx
	movw	%ax, %dx
	jmp	.LBB0_411
.LBB0_410:                              # %.lr.ph.i318.1804.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	orl	%ecx, %r15d
	addl	$2, %r9d
	movzbl	(%rdx,%rax), %edx
	orl	%r15d, %edx
	andl	$32768, %r15d           # imm = 0x8000
	addl	%edx, %edx
	shrl	$15, %r15d
	movl	$15, %ecx
	movl	%r15d, %esi
.LBB0_411:                              # %getbits.exit324805.i
                                        #   in Loop: Header=BB0_389 Depth=2
	shll	$16, %esi
	movzwl	%dx, %r13d
	orl	%esi, %r13d
	leal	(%rdx,%rdx), %r15d
	shrl	$15, %r13d
	leal	-1(%rcx), %r12d
	xorl	%edi, %edi
	cmpl	$3, %r13d
	jne	.LBB0_486
# BB#412:                               #   in Loop: Header=BB0_389 Depth=2
	cmpl	$2, %r12d
	ja	.LBB0_416
# BB#413:                               #   in Loop: Header=BB0_389 Depth=2
	movl	$3, %eax
	subl	%ecx, %eax
	shrl	$4, %eax
	leal	2(%rax,%rax), %eax
	movl	56(%rsp), %esi          # 4-byte Reload
	subl	%r9d, %esi
	cmpl	%esi, %eax
	jbe	.LBB0_415
# BB#414:                               # %getbits.exit332.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	movq	%r9, 88(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	movl	$3, %edi
	jmp	.LBB0_502
.LBB0_415:                              # %.lr.ph.i326.preheader.i
                                        #   in Loop: Header=BB0_389 Depth=2
	testl	%r12d, %r12d
	je	.LBB0_418
.LBB0_416:                              # %.lr.ph.i326.1806.i
                                        #   in Loop: Header=BB0_389 Depth=2
	movzwl	%r15w, %r15d
	shll	$2, %edx
	shrl	$15, %r15d
	addl	$-2, %ecx
	jne	.LBB0_419
# BB#417:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rsi            # 8-byte Reload
	movzbl	(%rsi,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rsi,%rax), %eax
	orl	%edx, %eax
	orl	%ecx, %eax
	movl	$16, %ecx
	movw	%ax, %dx
	jmp	.LBB0_419
.LBB0_418:                              # %.lr.ph.i326.1806.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	orl	%ecx, %r15d
	addl	$2, %r9d
	movzbl	(%rdx,%rax), %edx
	orl	%r15d, %edx
	andl	$32768, %r15d           # imm = 0x8000
	addl	%edx, %edx
	shrl	$15, %r15d
	movl	$15, %ecx
.LBB0_419:                              # %.lr.ph.i326.2807.i
                                        #   in Loop: Header=BB0_389 Depth=2
	shll	$16, %r15d
	movzwl	%dx, %edi
	orl	%r15d, %edi
	leal	(%rdx,%rdx), %esi
	shrl	$15, %edi
	decl	%ecx
	jne	.LBB0_421
# BB#420:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdx,%rax), %eax
	orl	%esi, %eax
	orl	%ecx, %eax
	movl	$16, %ecx
	movw	%ax, %si
.LBB0_421:                              # %getbits.exit332808.i
                                        #   in Loop: Header=BB0_389 Depth=2
	shll	$16, %edi
	movzwl	%si, %eax
	orl	%edi, %eax
	leal	(%rsi,%rsi), %r15d
	shrl	$15, %eax
	leal	-1(%rcx), %r12d
	movzwl	%ax, %r13d
	movl	$3, %edi
	cmpl	$7, %r13d
	jne	.LBB0_486
# BB#422:                               #   in Loop: Header=BB0_389 Depth=2
	cmpl	$4, %r12d
	ja	.LBB0_426
# BB#423:                               #   in Loop: Header=BB0_389 Depth=2
	movl	$5, %eax
	subl	%ecx, %eax
	shrl	$4, %eax
	leal	2(%rax,%rax), %eax
	movl	56(%rsp), %edx          # 4-byte Reload
	subl	%r9d, %edx
	cmpl	%edx, %eax
	jbe	.LBB0_425
# BB#424:                               # %getbits.exit340.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	movq	%r9, 88(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	movl	$10, %edi
	jmp	.LBB0_502
.LBB0_425:                              # %.lr.ph.i334.preheader.i
                                        #   in Loop: Header=BB0_389 Depth=2
	testl	%r12d, %r12d
	je	.LBB0_436
.LBB0_426:                              # %.lr.ph.i334.1809.i
                                        #   in Loop: Header=BB0_389 Depth=2
	movzwl	%r15w, %r15d
	leal	(,%rsi,4), %edx
	shrl	$15, %r15d
	addl	$-2, %ecx
	je	.LBB0_429
# BB#427:                               # %.lr.ph.i334.2810.i
                                        #   in Loop: Header=BB0_389 Depth=2
	shll	$16, %r15d
	movzwl	%dx, %edi
	orl	%r15d, %edi
	shll	$3, %esi
	shrl	$15, %edi
	cmpl	$1, %ecx
	jne	.LBB0_438
# BB#428:                               # %.lr.ph.i334.3811.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdx,%rax), %edx
	orl	%esi, %edx
	orl	%ecx, %edx
	shll	$16, %edi
	movzwl	%dx, %esi
	orl	%edi, %esi
	addl	%edx, %edx
	movl	$15, %ecx
	jmp	.LBB0_440
.LBB0_429:                              #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rsi            # 8-byte Reload
	movzbl	(%rsi,%rcx), %ecx
	shll	$8, %ecx
	movzbl	(%rsi,%rax), %eax
	orl	%edx, %eax
	orl	%ecx, %eax
	movl	$16, %ecx
	movw	%ax, %dx
	jmp	.LBB0_437
.LBB0_430:                              #   in Loop: Header=BB0_389 Depth=2
	cmpl	$7, %r12d
	ja	.LBB0_433
# BB#431:                               #   in Loop: Header=BB0_389 Depth=2
	movl	$8, %ecx
	subl	%eax, %ecx
	shrl	$4, %ecx
	leal	2(%rcx,%rcx), %ecx
	movl	56(%rsp), %esi          # 4-byte Reload
	subl	%r9d, %esi
	cmpl	%esi, %ecx
	ja	.LBB0_516
# BB#432:                               # %.lr.ph.i270.preheader.i
                                        #   in Loop: Header=BB0_389 Depth=2
	testl	%r12d, %r12d
	je	.LBB0_503
.LBB0_433:                              # %.lr.ph.i270.1830.i
                                        #   in Loop: Header=BB0_389 Depth=2
	movzwl	%r15w, %r15d
	leal	(,%rdx,4), %ecx
	shrl	$15, %r15d
	addl	$-2, %eax
	je	.LBB0_451
# BB#434:                               # %.lr.ph.i270.2831.i
                                        #   in Loop: Header=BB0_389 Depth=2
	shll	$16, %r15d
	movzwl	%cx, %esi
	orl	%r15d, %esi
	shll	$3, %edx
	shrl	$15, %esi
	cmpl	$1, %eax
	jne	.LBB0_505
# BB#435:                               # %.lr.ph.i270.3832.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rbp            # 8-byte Reload
	movzbl	(%rbp,%rcx), %edi
	shll	$8, %edi
	addl	$2, %r9d
	movzbl	(%rbp,%rax), %ecx
	orl	%edx, %ecx
	orl	%edi, %ecx
	shll	$16, %esi
	movzwl	%cx, %edx
	orl	%esi, %edx
	addl	%ecx, %ecx
	movl	$15, %eax
	jmp	.LBB0_507
.LBB0_436:                              # %.lr.ph.i334.1809.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	orl	%ecx, %r15d
	movzbl	(%rdx,%rax), %edx
	orl	%r15d, %edx
	andl	$32768, %r15d           # imm = 0x8000
	addl	%edx, %edx
	shrl	$15, %r15d
	movl	$15, %ecx
.LBB0_437:                              # %.lr.ph.i334.2810.i.thread
                                        #   in Loop: Header=BB0_389 Depth=2
	addl	$2, %r9d
	shll	$16, %r15d
	movzwl	%dx, %edi
	orl	%r15d, %edi
	leal	(%rdx,%rdx), %esi
	shrl	$15, %edi
.LBB0_438:                              # %.lr.ph.i334.3811.i
                                        #   in Loop: Header=BB0_389 Depth=2
	shll	$16, %edi
	movzwl	%si, %esi
	orl	%edi, %esi
	shll	$2, %edx
	addl	$-2, %ecx
	jne	.LBB0_440
# BB#439:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%edx, %eax
	orl	%ecx, %eax
	movl	$16, %ecx
	movw	%ax, %dx
.LBB0_440:                              # %.lr.ph.i334.4812.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$536838144, %esi        # imm = 0x1FFF8000
	movzwl	%dx, %eax
	leal	(%rax,%rsi,2), %edi
	leal	(%rdx,%rdx), %esi
	decl	%ecx
	jne	.LBB0_442
# BB#441:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdx,%rax), %eax
	orl	%ecx, %eax
	orl	%esi, %eax
	movl	$16, %ecx
	movw	%ax, %si
.LBB0_442:                              # %getbits.exit340813.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$1073709056, %edi       # imm = 0x3FFF8000
	movzwl	%si, %eax
	leal	(%rax,%rdi,2), %r13d
	leal	(%rsi,%rsi), %r15d
	shrl	$15, %r13d
	leal	-1(%rcx), %r12d
	movl	$10, %edi
	cmpl	$31, %r13d
	jne	.LBB0_486
# BB#443:                               #   in Loop: Header=BB0_389 Depth=2
	cmpl	$7, %r12d
	ja	.LBB0_447
# BB#444:                               #   in Loop: Header=BB0_389 Depth=2
	movl	$8, %eax
	subl	%ecx, %eax
	shrl	$4, %eax
	leal	2(%rax,%rax), %eax
	movl	56(%rsp), %edx          # 4-byte Reload
	subl	%r9d, %edx
	cmpl	%edx, %eax
	jbe	.LBB0_446
# BB#445:                               # %getbits.exit348.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	movq	%r9, 88(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	movl	$41, %edi
	jmp	.LBB0_502
.LBB0_446:                              # %.lr.ph.i342.preheader.i
                                        #   in Loop: Header=BB0_389 Depth=2
	testl	%r12d, %r12d
	je	.LBB0_452
.LBB0_447:                              # %.lr.ph.i342.1814.i
                                        #   in Loop: Header=BB0_389 Depth=2
	movzwl	%r15w, %r15d
	leal	(,%rsi,4), %edx
	shrl	$15, %r15d
	addl	$-2, %ecx
	je	.LBB0_450
# BB#448:                               # %.lr.ph.i342.2815.i
                                        #   in Loop: Header=BB0_389 Depth=2
	shll	$16, %r15d
	movzwl	%dx, %edi
	orl	%r15d, %edi
	shll	$3, %esi
	shrl	$15, %edi
	cmpl	$1, %ecx
	jne	.LBB0_454
# BB#449:                               # %.lr.ph.i342.3816.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdx,%rax), %edx
	orl	%ecx, %edx
	orl	%esi, %edx
	shll	$16, %edi
	movzwl	%dx, %esi
	orl	%edi, %esi
	addl	%edx, %edx
	movl	$15, %ecx
	jmp	.LBB0_456
.LBB0_450:                              #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rsi            # 8-byte Reload
	movzbl	(%rsi,%rcx), %ecx
	shll	$8, %ecx
	movzbl	(%rsi,%rax), %eax
	orl	%ecx, %eax
	orl	%edx, %eax
	movl	$16, %ecx
	movw	%ax, %dx
	jmp	.LBB0_453
.LBB0_451:                              #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	movzbl	(%rsi,%rdx), %edx
	shll	$8, %edx
	movzbl	(%rsi,%rax), %esi
	orl	%ecx, %esi
	orl	%edx, %esi
	movl	$16, %eax
	movw	%si, %cx
	jmp	.LBB0_504
.LBB0_452:                              # %.lr.ph.i342.1814.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	orl	%ecx, %r15d
	movzbl	(%rdx,%rax), %edx
	orl	%r15d, %edx
	andl	$32768, %r15d           # imm = 0x8000
	addl	%edx, %edx
	shrl	$15, %r15d
	movl	$15, %ecx
.LBB0_453:                              # %.lr.ph.i342.2815.i.thread
                                        #   in Loop: Header=BB0_389 Depth=2
	addl	$2, %r9d
	shll	$16, %r15d
	movzwl	%dx, %edi
	orl	%r15d, %edi
	leal	(%rdx,%rdx), %esi
	shrl	$15, %edi
.LBB0_454:                              # %.lr.ph.i342.3816.i
                                        #   in Loop: Header=BB0_389 Depth=2
	shll	$16, %edi
	movzwl	%si, %esi
	orl	%edi, %esi
	shll	$2, %edx
	addl	$-2, %ecx
	jne	.LBB0_456
# BB#455:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%ecx, %eax
	orl	%edx, %eax
	movl	$16, %ecx
	movw	%ax, %dx
.LBB0_456:                              # %.lr.ph.i342.4817.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$67076096, %esi         # imm = 0x3FF8000
	movzwl	%dx, %eax
	leal	(%rax,%rsi,2), %esi
	leal	(%rdx,%rdx), %edi
	decl	%ecx
	jne	.LBB0_458
# BB#457:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdx,%rax), %eax
	orl	%ecx, %eax
	orl	%edi, %eax
	movl	$16, %ecx
	movw	%ax, %di
.LBB0_458:                              # %.lr.ph.i342.5818.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$134184960, %esi        # imm = 0x7FF8000
	movzwl	%di, %eax
	leal	(%rax,%rsi,2), %edx
	leal	(%rdi,%rdi), %esi
	decl	%ecx
	jne	.LBB0_460
# BB#459:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%ecx, %eax
	orl	%esi, %eax
	movl	$16, %ecx
	movw	%ax, %si
.LBB0_460:                              # %.lr.ph.i342.6819.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$268402688, %edx        # imm = 0xFFF8000
	movzwl	%si, %eax
	leal	(%rax,%rdx,2), %edx
	leal	(%rsi,%rsi), %esi
	decl	%ecx
	jne	.LBB0_462
# BB#461:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%ecx, %eax
	orl	%esi, %eax
	movl	$16, %ecx
	movw	%ax, %si
.LBB0_462:                              # %.lr.ph.i342.7820.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$536838144, %edx        # imm = 0x1FFF8000
	movzwl	%si, %eax
	leal	(%rax,%rdx,2), %edx
	leal	(%rsi,%rsi), %esi
	decl	%ecx
	jne	.LBB0_464
# BB#463:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %eax
	orl	%ecx, %eax
	orl	%esi, %eax
	movl	$16, %ecx
	movw	%ax, %si
.LBB0_464:                              # %getbits.exit348821.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$1073709056, %edx       # imm = 0x3FFF8000
	movzwl	%si, %eax
	leal	(%rax,%rdx,2), %r13d
	leal	(%rsi,%rsi), %r15d
	shrl	$15, %r13d
	movl	%ecx, %r12d
	decl	%r12d
	movl	$41, %edi
	cmpl	$255, %r13d
	jne	.LBB0_486
# BB#465:                               # %.preheader649.i.preheader
                                        #   in Loop: Header=BB0_389 Depth=2
	movl	$296, %edi              # imm = 0x128
	cmpl	$7, %r12d
	ja	.LBB0_469
.LBB0_467:                              #   in Loop: Header=BB0_389 Depth=2
	movl	$7, %eax
	subl	%r12d, %eax
	shrl	$4, %eax
	leal	2(%rax,%rax), %eax
	movl	56(%rsp), %ecx          # 4-byte Reload
	subl	%r9d, %ecx
	cmpl	%ecx, %eax
	ja	.LBB0_501
# BB#468:                               # %.lr.ph.i294.preheader.i
                                        #   in Loop: Header=BB0_389 Depth=2
	testl	%r12d, %r12d
	jne	.LBB0_469
# BB#473:                               # %.lr.ph.i294.1822.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	orl	%ecx, %r15d
	movzbl	(%rdx,%rax), %ecx
	orl	%r15d, %ecx
	andl	$32768, %r15d           # imm = 0x8000
	addl	%ecx, %ecx
	shrl	$15, %r15d
	movl	$15, %r12d
	movl	%r15d, %esi
	jmp	.LBB0_474
.LBB0_466:                              #   in Loop: Header=BB0_389 Depth=2
	addl	$255, %edi
	cmpl	$7, %r12d
	jbe	.LBB0_467
.LBB0_469:                              # %.lr.ph.i294.1822.i
                                        #   in Loop: Header=BB0_389 Depth=2
	movzwl	%r15w, %esi
	leal	(%r15,%r15), %ecx
	shrl	$15, %esi
	decl	%r12d
	je	.LBB0_472
# BB#470:                               # %.lr.ph.i294.2823.i
                                        #   in Loop: Header=BB0_389 Depth=2
	shll	$16, %esi
	movzwl	%cx, %edx
	orl	%esi, %edx
	shll	$2, %r15d
	shrl	$15, %edx
	cmpl	$1, %r12d
	jne	.LBB0_475
# BB#471:                               # %.lr.ph.i294.3824.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %r12d
	movl	%r9d, %ecx
	movq	(%rsp), %rax            # 8-byte Reload
	movzbl	(%rax,%rcx), %esi
	shll	$8, %esi
	addl	$2, %r9d
	movzbl	(%rax,%r12), %ecx
	orl	%r15d, %ecx
	orl	%esi, %ecx
	shll	$16, %edx
	movzwl	%cx, %esi
	orl	%edx, %esi
	addl	%ecx, %ecx
	movl	$15, %r12d
	jmp	.LBB0_477
.LBB0_472:                              #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %r15d
	movl	%r9d, %edx
	movq	(%rsp), %rax            # 8-byte Reload
	movzbl	(%rax,%rdx), %edx
	shll	$8, %edx
	movzbl	(%rax,%r15), %eax
	orl	%ecx, %eax
	orl	%edx, %eax
	movl	$16, %r12d
	movw	%ax, %cx
.LBB0_474:                              # %.lr.ph.i294.2823.i.thread
                                        #   in Loop: Header=BB0_389 Depth=2
	addl	$2, %r9d
	shll	$16, %esi
	movzwl	%cx, %edx
	orl	%esi, %edx
	leal	(%rcx,%rcx), %r15d
	shrl	$15, %edx
.LBB0_475:                              # %.lr.ph.i294.3824.i
                                        #   in Loop: Header=BB0_389 Depth=2
	shll	$16, %edx
	movzwl	%r15w, %esi
	orl	%edx, %esi
	shll	$2, %ecx
	addl	$-2, %r12d
	jne	.LBB0_477
# BB#476:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %r15d
	movl	%r9d, %edx
	movq	(%rsp), %rax            # 8-byte Reload
	movzbl	(%rax,%rdx), %edx
	shll	$8, %edx
	addl	$2, %r9d
	movzbl	(%rax,%r15), %eax
	orl	%ecx, %eax
	orl	%edx, %eax
	movl	$16, %r12d
	movw	%ax, %cx
.LBB0_477:                              # %.lr.ph.i294.4825.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$67076096, %esi         # imm = 0x3FF8000
	movzwl	%cx, %eax
	leal	(%rax,%rsi,2), %edx
	leal	(%rcx,%rcx), %esi
	decl	%r12d
	jne	.LBB0_479
# BB#478:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %r15d
	movl	%r9d, %ecx
	movq	(%rsp), %rax            # 8-byte Reload
	movzbl	(%rax,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rax,%r15), %eax
	orl	%ecx, %eax
	orl	%esi, %eax
	movl	$16, %r12d
	movw	%ax, %si
.LBB0_479:                              # %.lr.ph.i294.5826.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$134184960, %edx        # imm = 0x7FF8000
	movzwl	%si, %eax
	leal	(%rax,%rdx,2), %ecx
	leal	(%rsi,%rsi), %edx
	decl	%r12d
	jne	.LBB0_481
# BB#480:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %r15d
	movl	%r9d, %esi
	movq	(%rsp), %rax            # 8-byte Reload
	movzbl	(%rax,%rsi), %esi
	shll	$8, %esi
	addl	$2, %r9d
	movzbl	(%rax,%r15), %eax
	orl	%esi, %eax
	orl	%edx, %eax
	movl	$16, %r12d
	movw	%ax, %dx
.LBB0_481:                              # %.lr.ph.i294.6827.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$268402688, %ecx        # imm = 0xFFF8000
	movzwl	%dx, %eax
	leal	(%rax,%rcx,2), %ecx
	leal	(%rdx,%rdx), %edx
	decl	%r12d
	jne	.LBB0_483
# BB#482:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %r15d
	movl	%r9d, %esi
	movq	(%rsp), %rax            # 8-byte Reload
	movzbl	(%rax,%rsi), %esi
	shll	$8, %esi
	addl	$2, %r9d
	movzbl	(%rax,%r15), %eax
	orl	%esi, %eax
	orl	%edx, %eax
	movl	$16, %r12d
	movw	%ax, %dx
.LBB0_483:                              # %.lr.ph.i294.7828.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$536838144, %ecx        # imm = 0x1FFF8000
	movzwl	%dx, %eax
	leal	(%rax,%rcx,2), %ecx
	leal	(%rdx,%rdx), %edx
	decl	%r12d
	jne	.LBB0_485
# BB#484:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %r15d
	movl	%r9d, %esi
	movq	(%rsp), %rax            # 8-byte Reload
	movzbl	(%rax,%rsi), %esi
	shll	$8, %esi
	addl	$2, %r9d
	movzbl	(%rax,%r15), %eax
	orl	%esi, %eax
	orl	%edx, %eax
	movl	$16, %r12d
	movw	%ax, %dx
.LBB0_485:                              # %getbits.exit300829.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$1073709056, %ecx       # imm = 0x3FFF8000
	movzwl	%dx, %eax
	leal	(%rax,%rcx,2), %r13d
	leal	(%rdx,%rdx), %r15d
	shrl	$15, %r13d
	decl	%r12d
	cmpl	$255, %r13d
	je	.LBB0_466
.LBB0_486:                              #   in Loop: Header=BB0_389 Depth=2
	movq	%r9, 88(%rsp)           # 8-byte Spill
.LBB0_487:                              # %.loopexit.i40
                                        #   in Loop: Header=BB0_389 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB0_488:                              # %.loopexit.i40
                                        #   in Loop: Header=BB0_389 Depth=2
	leal	2(%rdi,%r13), %r9d
	cmpl	%r10d, %r9d
	jae	.LBB0_517
# BB#489:                               #   in Loop: Header=BB0_389 Depth=2
	leal	3(%rdi,%r13), %ecx
	movl	%esi, %edx
	addq	%r11, %rdx
	movl	%ecx, %eax
	addq	%rax, %rdx
	cmpq	48(%rsp), %rdx          # 8-byte Folded Reload
	ja	.LBB0_517
# BB#490:                               #   in Loop: Header=BB0_389 Depth=2
	cmpq	%r11, %rdx
	jbe	.LBB0_517
# BB#491:                               #   in Loop: Header=BB0_389 Depth=2
	movl	%esi, %edx
	subl	%ebp, %edx
	addq	%r11, %rdx
	addq	%rax, %rdx
	cmpq	48(%rsp), %rdx          # 8-byte Folded Reload
	ja	.LBB0_517
# BB#492:                               #   in Loop: Header=BB0_389 Depth=2
	cmpq	%r11, %rdx
	jbe	.LBB0_517
# BB#493:                               # %.lr.ph.i41.preheader
                                        #   in Loop: Header=BB0_389 Depth=2
	movq	%rdi, %r10
	movl	%ecx, %edi
	andl	$3, %edi
	je	.LBB0_496
# BB#494:                               # %.lr.ph.i41.prol.preheader
                                        #   in Loop: Header=BB0_389 Depth=2
	movl	%ebp, %eax
	negl	%eax
	negl	%edi
	movl	%esi, %edx
.LBB0_495:                              # %.lr.ph.i41.prol
                                        #   Parent Loop BB0_319 Depth=1
                                        #     Parent Loop BB0_389 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	decl	%ecx
	leal	(%rax,%rdx), %esi
	movzbl	(%r11,%rsi), %ebx
	movl	%edx, %esi
	movb	%bl, (%r11,%rsi)
	incl	%edx
	incl	%edi
	jne	.LBB0_495
	jmp	.LBB0_497
.LBB0_496:                              #   in Loop: Header=BB0_389 Depth=2
	movl	%esi, %edx
.LBB0_497:                              # %.lr.ph.i41.prol.loopexit
                                        #   in Loop: Header=BB0_389 Depth=2
	cmpl	$3, %r9d
	jb	.LBB0_500
# BB#498:                               # %.lr.ph.i41.preheader.new
                                        #   in Loop: Header=BB0_389 Depth=2
	movl	%edx, %esi
	subl	%ebp, %esi
	xorl	%edi, %edi
.LBB0_499:                              # %.lr.ph.i41
                                        #   Parent Loop BB0_319 Depth=1
                                        #     Parent Loop BB0_389 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rdx,%rdi), %eax
	leal	(%rsi,%rdi), %ebp
	movzbl	(%r11,%rbp), %ebx
	movb	%bl, (%r11,%rax)
	leal	1(%rdx,%rdi), %eax
	leal	1(%rsi,%rdi), %ebp
	movzbl	(%r11,%rbp), %ebx
	movb	%bl, (%r11,%rax)
	leal	2(%rdx,%rdi), %eax
	leal	2(%rsi,%rdi), %ebp
	movzbl	(%r11,%rbp), %ebx
	movb	%bl, (%r11,%rax)
	leal	3(%rdx,%rdi), %eax
	leal	3(%rsi,%rdi), %ebp
	movzbl	(%r11,%rbp), %ebx
	movb	%bl, (%r11,%rax)
	addl	$4, %edi
	cmpl	%edi, %ecx
	jne	.LBB0_499
.LBB0_500:                              # %._crit_edge.i42
                                        #   in Loop: Header=BB0_389 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	%r10d, %esi
	leal	3(%r13,%rsi), %esi
	testl	%r8d, %r8d
	leaq	128(%rsp), %rbx
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	je	.LBB0_388
	jmp	.LBB0_517
.LBB0_501:                              # %getbits.exit300.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	movq	%r9, 88(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
.LBB0_502:                              # %.loopexit.i40
                                        #   in Loop: Header=BB0_389 Depth=2
	movl	$1, %r8d
	jmp	.LBB0_487
.LBB0_503:                              # %.lr.ph.i270.1830.thread.i
                                        #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	orl	%ecx, %r15d
	movzbl	(%rdx,%rax), %ecx
	orl	%r15d, %ecx
	andl	$32768, %r15d           # imm = 0x8000
	addl	%ecx, %ecx
	shrl	$15, %r15d
	movl	$15, %eax
.LBB0_504:                              # %.lr.ph.i270.2831.i.thread
                                        #   in Loop: Header=BB0_389 Depth=2
	addl	$2, %r9d
	shll	$16, %r15d
	movzwl	%cx, %esi
	orl	%r15d, %esi
	leal	(%rcx,%rcx), %edx
	shrl	$15, %esi
.LBB0_505:                              # %.lr.ph.i270.3832.i
                                        #   in Loop: Header=BB0_389 Depth=2
	shll	$16, %esi
	movzwl	%dx, %edx
	orl	%esi, %edx
	shll	$2, %ecx
	addl	$-2, %eax
	jne	.LBB0_507
# BB#506:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rsi), %esi
	shll	$8, %esi
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %edi
	orl	%ecx, %edi
	orl	%esi, %edi
	movl	$16, %eax
	movw	%di, %cx
.LBB0_507:                              # %.lr.ph.i270.4833.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$229376, %edx           # imm = 0x38000
	movzwl	%cx, %esi
	leal	(%rsi,%rdx,2), %edx
	leal	(%rcx,%rcx), %esi
	decl	%eax
	jne	.LBB0_509
# BB#508:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %ecx
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rcx), %ecx
	shll	$8, %ecx
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %edi
	orl	%ecx, %edi
	orl	%esi, %edi
	movl	$16, %eax
	movw	%di, %si
.LBB0_509:                              # %.lr.ph.i270.5834.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$491520, %edx           # imm = 0x78000
	movzwl	%si, %ecx
	leal	(%rcx,%rdx,2), %ecx
	leal	(%rsi,%rsi), %edx
	decl	%eax
	jne	.LBB0_511
# BB#510:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rsi), %esi
	shll	$8, %esi
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %edi
	orl	%esi, %edi
	orl	%edx, %edi
	movl	$16, %eax
	movw	%di, %dx
.LBB0_511:                              # %.lr.ph.i270.6835.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$1015808, %ecx          # imm = 0xF8000
	movzwl	%dx, %esi
	leal	(%rsi,%rcx,2), %ecx
	leal	(%rdx,%rdx), %edx
	decl	%eax
	jne	.LBB0_513
# BB#512:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rsi), %esi
	shll	$8, %esi
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %edi
	orl	%esi, %edi
	orl	%edx, %edi
	movl	$16, %eax
	movw	%di, %dx
.LBB0_513:                              # %.lr.ph.i270.7836.i
                                        #   in Loop: Header=BB0_389 Depth=2
	andl	$2064384, %ecx          # imm = 0x1F8000
	movzwl	%dx, %esi
	leal	(%rsi,%rcx,2), %ecx
	leal	(%rdx,%rdx), %edx
	decl	%eax
	jne	.LBB0_387
# BB#514:                               #   in Loop: Header=BB0_389 Depth=2
	leal	1(%r9), %eax
	movl	%r9d, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movzbl	(%rdi,%rsi), %esi
	shll	$8, %esi
	addl	$2, %r9d
	movzbl	(%rdi,%rax), %edi
	orl	%esi, %edi
	orl	%edx, %edi
	movl	$16, %eax
	movw	%di, %dx
	jmp	.LBB0_387
.LBB0_515:                              # %.critedge4.thread629.i
                                        #   in Loop: Header=BB0_319 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	96(%rsp), %r15          # 8-byte Reload
	cmpl	$3, %eax
	jbe	.LBB0_518
	jmp	.LBB0_521
.LBB0_516:                              # %.critedge4.thread.loopexit746.i
                                        #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	8(%rsp), %eax           # 4-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movb	$0, (%rcx,%rax)
.LBB0_517:                              # %.critedge4.i
                                        #   in Loop: Header=BB0_319 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	96(%rsp), %r15          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	cmpl	$3, %eax
	ja	.LBB0_521
.LBB0_518:                              #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.52, %edi
.LBB0_519:                              # %.outer.i20
                                        #   in Loop: Header=BB0_319 Depth=1
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_520:                              #   in Loop: Header=BB0_319 Depth=1
	movq	%rbp, %rdi
	callq	free
	jmp	.LBB0_319
.LBB0_521:                              #   in Loop: Header=BB0_319 Depth=1
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	cmpb	$0, 68(%rsp)            # 1-byte Folded Reload
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB0_297
# BB#522:                               #   in Loop: Header=BB0_319 Depth=1
	movl	%eax, %edi
	movq	%rax, %rbx
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_554
# BB#523:                               #   in Loop: Header=BB0_319 Depth=1
	movq	%r12, 72(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r12d
	movl	$.L.str.53, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	cli_dbgmsg
	cmpl	$5, %ebx
	movq	%rbx, %rdx
	jb	.LBB0_298
# BB#524:                               #   in Loop: Header=BB0_319 Depth=1
	testl	%r12d, %r12d
	je	.LBB0_298
# BB#525:                               # %.lr.ph734.i
                                        #   in Loop: Header=BB0_319 Depth=1
	movl	%r12d, (%rsp)           # 4-byte Spill
	leal	-4(%rdx), %eax
	movl	%eax, 112(%rsp)         # 4-byte Spill
	leal	-8(%rdx), %eax
	movl	%eax, 108(%rsp)         # 4-byte Spill
	movq	32(%rsp), %rdi          # 8-byte Reload
	leaq	3(%rdi), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	$4, %r12d
	xorl	%r9d, %r9d
	movl	%edx, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB0_526
.LBB0_318:                              #   in Loop: Header=BB0_319 Depth=1
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	movl	$1, %edx
	movl	%r13d, %r14d
	movl	%r14d, %edi
	movq	%rbx, %rsi
	callq	lseek
	leaq	128(%rsp), %rbx
	jmp	.LBB0_319
.LBB0_535:                              # %.backedge.i44
                                        #   in Loop: Header=BB0_526 Depth=2
	movl	%edi, (%rsp)            # 4-byte Spill
	cmpl	%edx, %r12d
	movq	32(%rsp), %rdi          # 8-byte Reload
	jb	.LBB0_526
	jmp	.LBB0_306
.LBB0_227:                              #   in Loop: Header=BB0_526 Depth=2
	cmpl	112(%rsp), %r15d        # 4-byte Folded Reload
	jae	.LBB0_300
# BB#228:                               #   in Loop: Header=BB0_526 Depth=2
	movl	%r15d, %eax
	movl	(%rdi,%rax), %r15d
	leal	(%r15,%r15), %r8d
	leal	5(%r12), %edi
	movl	%edx, %eax
	subl	%r8d, %eax
	jb	.LBB0_302
# BB#229:                               #   in Loop: Header=BB0_526 Depth=2
	cmpl	%eax, %edi
	jae	.LBB0_302
# BB#230:                               #   in Loop: Header=BB0_526 Depth=2
	leal	3(%r9,%r15), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	%ecx, %eax
	movq	%rcx, %rsi
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movl	%edi, 88(%rsp)          # 4-byte Spill
	jae	.LBB0_232
# BB#231:                               #   in Loop: Header=BB0_526 Depth=2
	movq	%rbp, %rax
	jmp	.LBB0_233
.LBB0_232:                              #   in Loop: Header=BB0_526 Depth=2
	leal	512(%rsi,%r15), %esi
	movq	%rbp, %rdi
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	callq	cli_realloc
	movl	88(%rsp), %edi          # 4-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	testq	%rax, %rax
	je	.LBB0_304
.LBB0_233:                              # %.thread633.i
                                        #   in Loop: Header=BB0_526 Depth=2
	leaq	-48(%r13), %rdx
	movl	$236, %ecx
	btq	%rdx, %rcx
	jae	.LBB0_235
# BB#234:                               #   in Loop: Header=BB0_526 Depth=2
	movb	ea06.prefixes(%rdx), %dl
	movl	%r9d, %ecx
	incl	%r9d
	movb	%dl, (%rax,%rcx)
.LBB0_235:                              #   in Loop: Header=BB0_526 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	testl	%r15d, %r15d
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	%rax, 56(%rsp)          # 8-byte Spill
	je	.LBB0_239
# BB#236:                               # %.preheader650.i
                                        #   in Loop: Header=BB0_526 Depth=2
	movq	%r9, %r10
	testl	%r8d, %r8d
	je	.LBB0_240
# BB#237:                               # %.lr.ph722.i
                                        #   in Loop: Header=BB0_526 Depth=2
	movl	%r15d, %r9d
	shrl	$8, %r9d
	movl	%r8d, %r8d
	leaq	-1(%r8), %rdx
	movq	%rdx, %rsi
	shrq	%rsi
	btl	$1, %edx
	jb	.LBB0_241
# BB#238:                               #   in Loop: Header=BB0_526 Depth=2
	leal	5(%r12), %edx
	movzbl	(%r11,%rdx), %edi
	xorl	%r15d, %edi
	movb	%dil, (%r11,%rdx)
	addl	$6, %r12d
	movzbl	(%r11,%r12), %edx
	xorl	%r9d, %edx
	movb	%dl, (%r11,%r12)
	movl	$2, %edx
	testq	%rsi, %rsi
	jne	.LBB0_242
	jmp	.LBB0_244
.LBB0_239:                              #   in Loop: Header=BB0_526 Depth=2
	movl	%edi, %r12d
	jmp	.LBB0_292
.LBB0_240:                              # %._crit_edge723.thread.i
                                        #   in Loop: Header=BB0_526 Depth=2
	movl	%edi, %esi
	addq	%r11, %rsi
	jmp	.LBB0_291
.LBB0_241:                              #   in Loop: Header=BB0_526 Depth=2
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.LBB0_244
.LBB0_242:                              # %.lr.ph722.i.new
                                        #   in Loop: Header=BB0_526 Depth=2
	leaq	(%rdx,%rbx), %rsi
	leaq	7(%rdx,%rbx), %rdi
	xorl	%ebx, %ebx
.LBB0_243:                              #   Parent Loop BB0_319 Depth=1
                                        #     Parent Loop BB0_526 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	5(%rsi,%rbx), %ebp
	movzbl	(%r11,%rbp), %ecx
	xorl	%r15d, %ecx
	movb	%cl, (%r11,%rbp)
	leal	6(%rsi,%rbx), %ecx
	movzbl	(%r11,%rcx), %ebp
	xorl	%r9d, %ebp
	movb	%bpl, (%r11,%rcx)
	leal	(%rdi,%rbx), %ecx
	movzbl	(%r11,%rcx), %ebp
	xorl	%r15d, %ebp
	movb	%bpl, (%r11,%rcx)
	leal	8(%rsi,%rbx), %ecx
	movzbl	(%r11,%rcx), %ebp
	xorl	%r9d, %ebp
	movb	%bpl, (%r11,%rcx)
	leaq	4(%rdx,%rbx), %rcx
	addq	$4, %rbx
	cmpq	%r8, %rcx
	jb	.LBB0_243
.LBB0_244:                              # %._crit_edge723.i
                                        #   in Loop: Header=BB0_526 Depth=2
	movl	88(%rsp), %ebp          # 4-byte Reload
	leaq	(%r11,%rbp), %rsi
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmpl	$2, %ecx
	jb	.LBB0_291
# BB#245:                               #   in Loop: Header=BB0_526 Depth=2
	cmpl	$5, %ecx
	jb	.LBB0_250
# BB#246:                               #   in Loop: Header=BB0_526 Depth=2
	cmpb	$-1, (%rsi)
	jne	.LBB0_250
# BB#247:                               #   in Loop: Header=BB0_526 Depth=2
	cmpb	$-2, 1(%rsi)
	jne	.LBB0_250
# BB#248:                               #   in Loop: Header=BB0_526 Depth=2
	cmpb	$0, 2(%rsi)
	je	.LBB0_250
# BB#249:                               # %.thread.i251.i
                                        #   in Loop: Header=BB0_526 Depth=2
	leaq	2(%rsi), %r8
	leal	-2(%rcx), %ecx
	jmp	.LBB0_289
.LBB0_250:                              #   in Loop: Header=BB0_526 Depth=2
	cmpl	$20, %ecx
	movl	$20, %r9d
	cmovbl	%ecx, %r9d
	testl	%r9d, %r9d
	je	.LBB0_267
# BB#251:                               # %.lr.ph49.preheader.i252.i
                                        #   in Loop: Header=BB0_526 Depth=2
	movl	%r9d, %edx
	leaq	-1(%rdx), %rcx
	movq	%rcx, %r8
	shrq	%r8
	btl	$1, %ecx
	jb	.LBB0_276
# BB#252:                               # %.lr.ph49.i255.i.prol
                                        #   in Loop: Header=BB0_526 Depth=2
	cmpb	$0, (%rsi)
	je	.LBB0_277
# BB#253:                               #   in Loop: Header=BB0_526 Depth=2
	cmpb	$0, 1(%rsi)
	sete	%cl
	jmp	.LBB0_278
.LBB0_254:                              #   in Loop: Header=BB0_526 Depth=2
	cmpl	112(%rsp), %r15d        # 4-byte Folded Reload
	jae	.LBB0_315
# BB#255:                               #   in Loop: Header=BB0_526 Depth=2
	leal	12(%r9), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	%ecx, %eax
	movq	%rcx, %rsi
	jae	.LBB0_270
# BB#256:                               #   in Loop: Header=BB0_526 Depth=2
	movq	%rbp, %rbx
	jmp	.LBB0_271
.LBB0_257:                              #   in Loop: Header=BB0_526 Depth=2
	cmpl	$8, %edx
	jb	.LBB0_316
# BB#258:                               #   in Loop: Header=BB0_526 Depth=2
	cmpl	108(%rsp), %r15d        # 4-byte Folded Reload
	jae	.LBB0_316
# BB#259:                               #   in Loop: Header=BB0_526 Depth=2
	leal	20(%r9), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	%ecx, %eax
	movq	%rcx, %rsi
	jae	.LBB0_272
# BB#260:                               #   in Loop: Header=BB0_526 Depth=2
	movq	%rbp, %rbx
	jmp	.LBB0_273
.LBB0_261:                              #   in Loop: Header=BB0_526 Depth=2
	cmpl	$8, %edx
	jb	.LBB0_317
# BB#262:                               #   in Loop: Header=BB0_526 Depth=2
	cmpl	108(%rsp), %r15d        # 4-byte Folded Reload
	jae	.LBB0_317
# BB#263:                               #   in Loop: Header=BB0_526 Depth=2
	leal	40(%r9), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	%ecx, %eax
	movq	%rcx, %rsi
	jae	.LBB0_274
# BB#264:                               #   in Loop: Header=BB0_526 Depth=2
	movq	%rbp, %r13
	jmp	.LBB0_275
.LBB0_265:                              #   in Loop: Header=BB0_526 Depth=2
	leal	1(%r9), %ebx
	movq	8(%rsp), %rsi           # 8-byte Reload
	cmpl	%esi, %ebx
	jae	.LBB0_268
# BB#266:                               #   in Loop: Header=BB0_526 Depth=2
	movq	%rbp, %rax
	jmp	.LBB0_269
.LBB0_267:                              #   in Loop: Header=BB0_526 Depth=2
	xorl	%ebx, %ebx
	jmp	.LBB0_288
.LBB0_268:                              #   in Loop: Header=BB0_526 Depth=2
	addl	$512, %esi              # imm = 0x200
	movq	%rbp, %rdi
	movq	%r9, %r12
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	callq	cli_realloc
	movq	%r12, %r9
	testq	%rax, %rax
	je	.LBB0_304
.LBB0_269:                              # %.thread635.i
                                        #   in Loop: Header=BB0_526 Depth=2
	movl	(%rsp), %edi            # 4-byte Reload
	decl	%edi
	movl	%r9d, %ecx
	movb	$10, (%rax,%rcx)
	movl	%r15d, %r12d
	movl	%ebx, %r9d
	movq	%rax, %rbp
	movq	96(%rsp), %r15          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	testl	%edi, %edi
	jne	.LBB0_535
	jmp	.LBB0_305
.LBB0_270:                              #   in Loop: Header=BB0_526 Depth=2
	addl	$512, %esi              # imm = 0x200
	movq	%rbp, %rdi
	movq	%r9, %rbx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	callq	cli_realloc
	movq	%rbx, %r9
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_304
.LBB0_271:                              # %.thread630.i
                                        #   in Loop: Header=BB0_526 Depth=2
	movl	%r9d, %edi
	addq	%rbx, %rdi
	movl	%r15d, %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%rax), %ecx
	movq	%r9, %rbp
	movl	$12, %esi
	movl	$.L.str.55, %edx
	xorl	%eax, %eax
	callq	snprintf
	movq	%rbp, %r9
	addl	$11, %r9d
	addl	$5, %r12d
	jmp	.LBB0_532
.LBB0_272:                              #   in Loop: Header=BB0_526 Depth=2
	addl	$512, %esi              # imm = 0x200
	movq	%rbp, %rdi
	movq	%r9, %rbx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	callq	cli_realloc
	movq	%rbx, %r9
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_304
.LBB0_273:                              # %.thread631.i
                                        #   in Loop: Header=BB0_526 Depth=2
	leal	5(%r12), %eax
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx,%rax), %eax
	shlq	$32, %rax
	movl	%r15d, %ecx
	movslq	(%rdx,%rcx), %rcx
	addq	%rax, %rcx
	movl	%r9d, %edi
	addq	%rbx, %rdi
	movq	%r9, %rbp
	movl	$20, %esi
	movl	$.L.str.57, %edx
	xorl	%eax, %eax
	callq	snprintf
	movq	%rbp, %r9
	addl	$19, %r9d
	addl	$9, %r12d
	jmp	.LBB0_532
.LBB0_274:                              #   in Loop: Header=BB0_526 Depth=2
	addl	$512, %esi              # imm = 0x200
	movq	%rbp, %rdi
	movq	%r9, %rbx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	callq	cli_realloc
	movq	%rbx, %r9
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_304
.LBB0_275:                              # %.thread632.i
                                        #   in Loop: Header=BB0_526 Depth=2
	movl	%r9d, %ebx
	addq	%r13, %rbx
	movl	%r15d, %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rax), %xmm0      # xmm0 = mem[0],zero
	movq	%r9, %r15
	movl	$39, %esi
	movl	$.L.str.59, %edx
	movb	$1, %al
	movq	%rbx, %rdi
	callq	snprintf
	leal	38(%r15), %eax
	movb	$32, (%r13,%rax)
	leal	39(%r15), %eax
	movb	$0, (%r13,%rax)
	movq	%rbx, %rdi
	callq	strlen
	addl	%r15d, %eax
	addl	$9, %r12d
	movl	%eax, %r9d
	movq	%r13, %rbp
	jmp	.LBB0_533
.LBB0_276:                              #   in Loop: Header=BB0_526 Depth=2
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	testq	%r8, %r8
	jne	.LBB0_279
	jmp	.LBB0_287
.LBB0_277:                              #   in Loop: Header=BB0_526 Depth=2
	xorl	%ecx, %ecx
.LBB0_278:                              #   in Loop: Header=BB0_526 Depth=2
	movzbl	%cl, %ebx
	movl	$2, %edi
	testq	%r8, %r8
	je	.LBB0_287
.LBB0_279:                              # %.lr.ph49.preheader.i252.i.new
                                        #   in Loop: Header=BB0_526 Depth=2
	addq	120(%rsp), %rbp         # 8-byte Folded Reload
.LBB0_280:                              # %.lr.ph49.i255.i
                                        #   Parent Loop BB0_319 Depth=1
                                        #     Parent Loop BB0_526 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$0, -3(%rbp,%rdi)
	je	.LBB0_282
# BB#281:                               #   in Loop: Header=BB0_280 Depth=3
	cmpb	$0, -2(%rbp,%rdi)
	sete	%cl
	jmp	.LBB0_283
.LBB0_282:                              #   in Loop: Header=BB0_280 Depth=3
	xorl	%ecx, %ecx
.LBB0_283:                              # %.lr.ph49.i255.i.1742
                                        #   in Loop: Header=BB0_280 Depth=3
	movzbl	%cl, %ecx
	addl	%ebx, %ecx
	cmpb	$0, -1(%rbp,%rdi)
	je	.LBB0_285
# BB#284:                               #   in Loop: Header=BB0_280 Depth=3
	cmpb	$0, (%rbp,%rdi)
	sete	%bl
	jmp	.LBB0_286
.LBB0_285:                              #   in Loop: Header=BB0_280 Depth=3
	xorl	%ebx, %ebx
.LBB0_286:                              #   in Loop: Header=BB0_280 Depth=3
	movzbl	%bl, %ebx
	addl	%ecx, %ebx
	addq	$4, %rdi
	cmpq	%rdx, %rdi
	jb	.LBB0_280
.LBB0_287:                              # %._crit_edge.loopexit.i258.i
                                        #   in Loop: Header=BB0_526 Depth=2
	shll	$2, %ebx
	movq	48(%rsp), %rcx          # 8-byte Reload
.LBB0_288:                              # %._crit_edge.i261.i
                                        #   in Loop: Header=BB0_526 Depth=2
	cmpl	%r9d, %ebx
	movq	%rsi, %r8
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	jb	.LBB0_291
.LBB0_289:                              # %.lr.ph.preheader.i264.i
                                        #   in Loop: Header=BB0_526 Depth=2
	xorl	%edx, %edx
	movq	%rsi, %rdi
.LBB0_290:                              # %.lr.ph.i267.i
                                        #   Parent Loop BB0_319 Depth=1
                                        #     Parent Loop BB0_526 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edx, %ebp
	movzbl	(%r8,%rbp), %ebx
	movb	%bl, (%rdi)
	incq	%rdi
	addl	$2, %edx
	cmpl	%ecx, %edx
	jb	.LBB0_290
.LBB0_291:                              # %u2a.exit269.i
                                        #   in Loop: Header=BB0_526 Depth=2
	movq	%r10, %rbx
	movl	%ebx, %edi
	addq	%rax, %rdi
	movq	%r15, %rdx
	callq	memcpy
	movq	%rbx, %r9
	leal	(%r15,%r9), %r9d
	movq	48(%rsp), %rax          # 8-byte Reload
	addl	88(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, %r12d
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB0_292:                              #   in Loop: Header=BB0_526 Depth=2
	cmpb	$52, %r13b
	movq	96(%rsp), %r15          # 8-byte Reload
	je	.LBB0_296
# BB#293:                               #   in Loop: Header=BB0_526 Depth=2
	cmpb	$54, %r13b
	movq	56(%rsp), %rbp          # 8-byte Reload
	jne	.LBB0_295
# BB#294:                               #   in Loop: Header=BB0_526 Depth=2
	movl	%r9d, %eax
	incl	%r9d
	movb	$34, (%rbp,%rax)
.LBB0_295:                              #   in Loop: Header=BB0_526 Depth=2
	movl	(%rsp), %edi            # 4-byte Reload
	movl	%r9d, %eax
	incl	%r9d
	movb	$32, (%rbp,%rax)
	testl	%edi, %edi
	jne	.LBB0_535
	jmp	.LBB0_305
.LBB0_296:                              #   in Loop: Header=BB0_526 Depth=2
	movq	56(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_534
.LBB0_526:                              #   Parent Loop BB0_319 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_243 Depth 3
                                        #       Child Loop BB0_280 Depth 3
                                        #       Child Loop BB0_290 Depth 3
	movl	%r12d, %ebx
	movzbl	(%rdi,%rbx), %r13d
	movl	%r13d, %eax
	addb	$-5, %al
	cmpb	$122, %al
	ja	.LBB0_299
# BB#527:                               #   in Loop: Header=BB0_526 Depth=2
	leal	1(%r12), %r15d
	movzbl	%al, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_528:                              #   in Loop: Header=BB0_526 Depth=2
	leal	4(%r9), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	%ecx, %eax
	movq	%rcx, %rsi
	jae	.LBB0_530
# BB#529:                               #   in Loop: Header=BB0_526 Depth=2
	movq	%rbp, %rbx
	jmp	.LBB0_531
.LBB0_530:                              #   in Loop: Header=BB0_526 Depth=2
	addl	$512, %esi              # imm = 0x200
	movq	%rbp, %rdi
	movq	%r9, %rbx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	callq	cli_realloc
	movq	%rbx, %r9
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_304
.LBB0_531:                              # %.thread634.i
                                        #   in Loop: Header=BB0_526 Depth=2
	movl	%r9d, %edi
	addq	%rbx, %rdi
	movq	.Lea06.opers-512(,%r13,8), %rcx
	movq	%r9, %rbp
	movl	$4, %esi
	movl	$.L.str.62, %edx
	xorl	%eax, %eax
	callq	snprintf
	addl	%ebp, %eax
	movl	%r15d, %r12d
	movl	%eax, %r9d
.LBB0_532:                              # %.backedge.i44
                                        #   in Loop: Header=BB0_526 Depth=2
	movq	%rbx, %rbp
.LBB0_533:                              # %.backedge.i44
                                        #   in Loop: Header=BB0_526 Depth=2
	movq	96(%rsp), %r15          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB0_534:                              # %.backedge.i44
                                        #   in Loop: Header=BB0_526 Depth=2
	movl	(%rsp), %edi            # 4-byte Reload
	testl	%edi, %edi
	jne	.LBB0_535
	jmp	.LBB0_305
.LBB0_536:
	xorl	%r13d, %r13d
	movl	$.L.str.7, %edi
	jmp	.LBB0_12
.LBB0_537:
	xorl	%r13d, %r13d
	movl	$.L.str.49, %edi
	jmp	.LBB0_12
.LBB0_538:
	movl	$.L.str.69, %edi
.LBB0_540:
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movl	$-102, %r13d
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_15
	jmp	.LBB0_14
.LBB0_541:
	xorl	%r13d, %r13d
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	free
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_15
	jmp	.LBB0_14
.LBB0_544:
	leaq	736(%rsp), %rsi
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_546
.LBB0_545:
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movl	%r13d, %edi
	callq	close
	movq	%rbp, %rdi
.LBB0_546:                              # %ea06.exit
	callq	free
	movl	$-123, %r13d
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_15
	jmp	.LBB0_14
.LBB0_547:
	movl	$1, %r13d
	testb	%al, %al
	jne	.LBB0_13
# BB#548:
	leaq	736(%rsp), %rdi
	callq	unlink
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_15
	jmp	.LBB0_14
.LBB0_549:
	leaq	736(%rsp), %rsi
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_551
.LBB0_550:
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	cli_dbgmsg
	movl	%ebx, %edi
	callq	close
.LBB0_551:                              # %ea05.exit
	movq	%r15, %rdi
	callq	free
	movl	$-123, %r13d
	jmp	.LBB0_557
.LBB0_552:
	testb	%al, %al
	jne	.LBB0_553
# BB#558:
	leaq	736(%rsp), %rdi
	callq	unlink
.LBB0_553:
	movl	$1, %r13d
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_15
	jmp	.LBB0_14
.LBB0_554:
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB0_543
.LBB0_555:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	movq	72(%rsp), %r12          # 8-byte Reload
.LBB0_543:
	movl	$-114, %r13d
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_15
	jmp	.LBB0_14
.LBB0_556:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
.LBB0_557:
	movq	72(%rsp), %r12          # 8-byte Reload
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_15
	jmp	.LBB0_14
.Lfunc_end0:
	.size	cli_scanautoit, .Lfunc_end0-cli_scanautoit
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_254
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_257
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_261
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_227
	.quad	.LBB0_227
	.quad	.LBB0_227
	.quad	.LBB0_227
	.quad	.LBB0_227
	.quad	.LBB0_227
	.quad	.LBB0_227
	.quad	.LBB0_227
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_528
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_299
	.quad	.LBB0_265

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	2147483646              # 0x7ffffffe
	.long	2147483646              # 0x7ffffffe
	.long	2147483646              # 0x7ffffffe
	.long	2147483646              # 0x7ffffffe
.LCPI1_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI1_2:
	.long	2567483615              # 0x9908b0df
	.long	2567483615              # 0x9908b0df
	.long	2567483615              # 0x9908b0df
	.long	2567483615              # 0x9908b0df
	.text
	.p2align	4, 0x90
	.type	MT_decrypt,@function
MT_decrypt:                             # @MT_decrypt
	.cfi_startproc
# BB#0:
	subq	$2384, %rsp             # imm = 0x950
.Lcfi13:
	.cfi_def_cfa_offset 2392
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%edx, -128(%rsp)
	movl	$1, %r8d
	xorl	%r9d, %r9d
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_13:                               #   in Loop: Header=BB1_1 Depth=1
	leaq	3(%r8), %rdx
	movl	%eax, %ecx
	shrl	$30, %ecx
	xorl	%eax, %ecx
	imull	$1812433253, %ecx, %eax # imm = 0x6C078965
	addl	%eax, %edx
	leal	4(%r9,%rax), %eax
	movl	%eax, -112(%rsp,%r9,4)
	addq	$4, %r8
	addq	$4, %r9
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movl	%edx, %eax
	shrl	$30, %eax
	xorl	%edx, %eax
	imull	$1812433253, %eax, %eax # imm = 0x6C078965
	leal	1(%r9,%rax), %eax
	movl	%eax, -124(%rsp,%r9,4)
	movl	%eax, %edx
	shrl	$30, %edx
	xorl	%eax, %edx
	imull	$1812433253, %edx, %eax # imm = 0x6C078965
	leal	2(%r9,%rax), %eax
	movl	%eax, -120(%rsp,%r9,4)
	movl	%eax, %edx
	shrl	$30, %edx
	xorl	%eax, %edx
	imull	$1812433253, %edx, %eax # imm = 0x6C078965
	leaq	3(%r9,%rax), %rax
	movl	%eax, -116(%rsp,%r9,4)
	cmpq	$620, %r9               # imm = 0x26C
	jne	.LBB1_13
# BB#2:
	movl	$1, 2368(%rsp)
	testl	%esi, %esi
	je	.LBB1_12
# BB#3:                                 # %.lr.ph
	movl	$1, %ecx
	leaq	-128(%rsp), %r8
	movaps	.LCPI1_0(%rip), %xmm8   # xmm8 = [2147483646,2147483646,2147483646,2147483646]
	movaps	.LCPI1_1(%rip), %xmm9   # xmm9 = [1,1,1,1]
	movdqa	.LCPI1_2(%rip), %xmm2   # xmm2 = [2567483615,2567483615,2567483615,2567483615]
	jmp	.LBB1_4
	.p2align	4, 0x90
.LBB1_11:                               # %MT_getnext.exit._crit_edge
                                        #   in Loop: Header=BB1_4 Depth=1
	incq	%rdi
	movl	2368(%rsp), %ecx
.LBB1_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_6 Depth 2
                                        #     Child Loop BB1_8 Depth 2
	decl	%esi
	decl	%ecx
	movl	%ecx, 2368(%rsp)
	jne	.LBB1_10
# BB#5:                                 # %min.iters.checked24
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	$624, 2368(%rsp)        # imm = 0x270
	movq	%r8, 2376(%rsp)
	movd	-128(%rsp), %xmm3       # xmm3 = mem[0],zero,zero,zero
	pshufd	$36, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,0]
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_6:                                # %vector.body21
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-124(%rsp,%rcx,4), %xmm5
	shufps	$3, %xmm5, %xmm3        # xmm3 = xmm3[3,0],xmm5[0,0]
	shufps	$152, %xmm5, %xmm3      # xmm3 = xmm3[0,2],xmm5[1,2]
	movaps	%xmm5, %xmm4
	xorps	%xmm3, %xmm4
	andps	%xmm8, %xmm4
	xorps	%xmm3, %xmm4
	movups	-108(%rsp,%rcx,4), %xmm3
	movaps	%xmm5, %xmm6
	shufps	$3, %xmm3, %xmm6        # xmm6 = xmm6[3,0],xmm3[0,0]
	shufps	$152, %xmm3, %xmm6      # xmm6 = xmm6[0,2],xmm3[1,2]
	movaps	%xmm3, %xmm7
	xorps	%xmm6, %xmm7
	andps	%xmm8, %xmm7
	xorps	%xmm6, %xmm7
	psrld	$1, %xmm4
	psrld	$1, %xmm7
	andps	%xmm9, %xmm5
	movaps	%xmm3, %xmm6
	andps	%xmm9, %xmm6
	pxor	%xmm0, %xmm0
	psubd	%xmm5, %xmm0
	pxor	%xmm5, %xmm5
	psubd	%xmm6, %xmm5
	pand	%xmm2, %xmm0
	pand	%xmm2, %xmm5
	movdqu	1460(%rsp,%rcx,4), %xmm6
	movdqu	1476(%rsp,%rcx,4), %xmm1
	pxor	%xmm0, %xmm6
	pxor	%xmm5, %xmm1
	pxor	%xmm4, %xmm6
	pxor	%xmm7, %xmm1
	movdqu	%xmm6, -128(%rsp,%rcx,4)
	movdqu	%xmm1, -112(%rsp,%rcx,4)
	addq	$8, %rcx
	cmpq	$224, %rcx
	jne	.LBB1_6
# BB#7:                                 # %.lr.ph.preheader.i44
                                        #   in Loop: Header=BB1_4 Depth=1
	pshufd	$231, %xmm3, %xmm0      # xmm0 = xmm3[3,1,2,3]
	movd	%xmm0, %ecx
	movl	772(%rsp), %eax
	movl	%eax, %edx
	xorl	%ecx, %edx
	andl	$2147483646, %edx       # imm = 0x7FFFFFFE
	xorl	%ecx, %edx
	shrl	%edx
	movl	%eax, %ecx
	andl	$1, %ecx
	negl	%ecx
	andl	$-1727483681, %ecx      # imm = 0x9908B0DF
	xorl	2356(%rsp), %ecx
	xorl	%edx, %ecx
	movl	%ecx, 768(%rsp)
	movl	776(%rsp), %ecx
	movl	%ecx, %edx
	xorl	%eax, %edx
	andl	$2147483646, %edx       # imm = 0x7FFFFFFE
	xorl	%eax, %edx
	shrl	%edx
	movl	%ecx, %eax
	andl	$1, %eax
	negl	%eax
	andl	$-1727483681, %eax      # imm = 0x9908B0DF
	xorl	2360(%rsp), %eax
	xorl	%edx, %eax
	movl	%eax, 772(%rsp)
	movl	780(%rsp), %eax
	movl	%eax, %edx
	xorl	%ecx, %edx
	andl	$2147483646, %edx       # imm = 0x7FFFFFFE
	xorl	%ecx, %edx
	shrl	%edx
	andl	$1, %eax
	negl	%eax
	andl	$-1727483681, %eax      # imm = 0x9908B0DF
	xorl	2364(%rsp), %eax
	xorl	%edx, %eax
	movl	%eax, 776(%rsp)
	movd	780(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	pshufd	$36, %xmm0, %xmm3       # xmm3 = xmm0[0,1,2,0]
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_8:                                # %vector.body
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	784(%rsp,%rcx,4), %xmm0
	shufps	$3, %xmm0, %xmm3        # xmm3 = xmm3[3,0],xmm0[0,0]
	shufps	$152, %xmm0, %xmm3      # xmm3 = xmm3[0,2],xmm0[1,2]
	movaps	%xmm0, %xmm1
	xorps	%xmm3, %xmm1
	andps	%xmm8, %xmm1
	xorps	%xmm3, %xmm1
	psrld	$1, %xmm1
	movaps	%xmm0, %xmm3
	andps	%xmm9, %xmm0
	pxor	%xmm4, %xmm4
	psubd	%xmm0, %xmm4
	pand	%xmm2, %xmm4
	movdqu	-128(%rsp,%rcx,4), %xmm0
	pxor	%xmm4, %xmm0
	pxor	%xmm1, %xmm0
	movdqu	%xmm0, 780(%rsp,%rcx,4)
	addq	$4, %rcx
	cmpq	$396, %rcx              # imm = 0x18C
	jne	.LBB1_8
# BB#9:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	2364(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	%ecx, %edx
	xorl	%eax, %edx
	andl	$2147483646, %edx       # imm = 0x7FFFFFFE
	xorl	%eax, %edx
	shrl	%edx
	andl	$1, %ecx
	negl	%ecx
	andl	$-1727483681, %ecx      # imm = 0x9908B0DF
	xorl	1456(%rsp), %ecx
	xorl	%edx, %ecx
	movl	%ecx, 2364(%rsp)
.LBB1_10:                               # %MT_getnext.exit
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	2376(%rsp), %rax
	leaq	4(%rax), %rcx
	movq	%rcx, 2376(%rsp)
	movl	(%rax), %eax
	movl	%eax, %ecx
	shrl	$11, %ecx
	xorl	%eax, %ecx
	movl	%ecx, %eax
	shll	$7, %eax
	andl	$-1658038656, %eax      # imm = 0x9D2C5680
	xorl	%ecx, %eax
	movl	%eax, %ecx
	shll	$15, %ecx
	andl	$130023424, %ecx        # imm = 0x7C00000
	xorl	%eax, %ecx
	shrl	$18, %ecx
	xorl	%eax, %ecx
	shrl	%ecx
	xorb	%cl, (%rdi)
	testl	%esi, %esi
	jne	.LBB1_11
.LBB1_12:                               # %._crit_edge
	addq	$2384, %rsp             # imm = 0x950
	retq
.Lfunc_end1:
	.size	MT_decrypt, .Lfunc_end1-MT_decrypt
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	-4616189618054758400    # double -1
.LCPI2_1:
	.quad	4643211215818981376     # double 256
	.text
	.p2align	4, 0x90
	.type	LAME_decrypt,@function
LAME_decrypt:                           # @LAME_decrypt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%esi, -84(%rsp)         # 4-byte Spill
	imull	$-1403630843, %edx, %ecx # imm = 0xAC564B05
	incl	%ecx
	imull	$-1403630843, %ecx, %eax # imm = 0xAC564B05
	incl	%eax
	movl	%eax, -68(%rsp)
	imull	$-1403630843, %eax, %eax # imm = 0xAC564B05
	incl	%eax
	movl	%eax, -64(%rsp)
	imull	$-1403630843, %eax, %eax # imm = 0xAC564B05
	incl	%eax
	movl	%eax, -60(%rsp)
	imull	$-1403630843, %eax, %r13d # imm = 0xAC564B05
	incl	%r13d
	movl	%r13d, -56(%rsp)
	imull	$-1403630843, %r13d, %r9d # imm = 0xAC564B05
	incl	%r9d
	movl	%r9d, -52(%rsp)
	imull	$-1403630843, %r9d, %r10d # imm = 0xAC564B05
	incl	%r10d
	movl	%r10d, -48(%rsp)
	imull	$-1403630843, %r10d, %r14d # imm = 0xAC564B05
	incl	%r14d
	movl	%r14d, -44(%rsp)
	imull	$-1403630843, %r14d, %r15d # imm = 0xAC564B05
	incl	%r15d
	movl	%r15d, -40(%rsp)
	imull	$-1403630843, %r15d, %esi # imm = 0xAC564B05
	incl	%esi
	imull	$-1403630843, %esi, %r12d # imm = 0xAC564B05
	incl	%r12d
	imull	$-1403630843, %r12d, %r11d # imm = 0xAC564B05
	incl	%r11d
	imull	$-1403630843, %r11d, %ebx # imm = 0xAC564B05
	incl	%ebx
	imull	$-1403630843, %ebx, %ebp # imm = 0xAC564B05
	incl	%ebp
	imull	$-1403630843, %ebp, %edx # imm = 0xAC564B05
	incl	%edx
	roll	$9, %ecx
	movl	%r12d, %eax
	roll	$13, %eax
	addl	%ecx, %eax
	imull	$-1403630843, %edx, %ecx # imm = 0xAC564B05
	incl	%ecx
	movl	%eax, -72(%rsp)
	imull	$-1403630843, %ecx, %eax # imm = 0xAC564B05
	incl	%eax
	roll	$9, %eax
	movl	%esi, %r8d
	roll	$13, %r8d
	addl	%eax, %r8d
	movl	%r8d, -8(%rsp)
	movd	%ebx, %xmm0
	movd	%edx, %xmm1
	movd	%ebp, %xmm2
	movd	%ecx, %xmm3
	movd	%r9d, %xmm4
	movd	%r14d, %xmm5
	movd	%r10d, %xmm6
	movd	%r15d, %xmm7
	roll	$9, %r11d
	roll	$13, %r13d
	addl	%r11d, %r13d
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	punpckldq	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	movdqa	%xmm0, %xmm1
	pslld	$9, %xmm1
	psrld	$23, %xmm0
	por	%xmm1, %xmm0
	punpckldq	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	punpckldq	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1]
	punpckldq	%xmm6, %xmm4    # xmm4 = xmm4[0],xmm6[0],xmm4[1],xmm6[1]
	movdqa	%xmm4, %xmm1
	pslld	$13, %xmm1
	psrld	$19, %xmm4
	por	%xmm1, %xmm4
	paddd	%xmm0, %xmm4
	movdqu	%xmm4, -24(%rsp)
	movl	%r13d, -28(%rsp)
	roll	$9, %r12d
	movl	-60(%rsp), %eax
	roll	$13, %eax
	addl	%r12d, %eax
	movl	%eax, -32(%rsp)
	roll	$9, %esi
	movl	-64(%rsp), %eax
	roll	$13, %eax
	addl	%esi, %eax
	movl	-84(%rsp), %ecx         # 4-byte Reload
	movl	%eax, -36(%rsp)
	movl	$1, -76(%rsp)
	movl	$8, -80(%rsp)
	testl	%ecx, %ecx
	je	.LBB2_6
# BB#1:                                 # %.lr.ph.preheader
	movl	$1, %edx
	movl	$1, %eax
	subl	%ecx, %eax
	movl	$8, %esi
	movl	$16, %r8d
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero
	jmp	.LBB2_2
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph..lr.ph_crit_edge
                                        #   in Loop: Header=BB2_2 Depth=1
	incq	%rdi
	movl	-80(%rsp), %esi
	movl	-76(%rsp), %edx
	incl	%eax
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, %ebp
	movl	-72(%rsp,%rbp,4), %ebx
	roll	$9, %ebx
	movl	%edx, %ecx
	movl	-72(%rsp,%rcx,4), %ecx
	roll	$13, %ecx
	addl	%ebx, %ecx
	movl	%ecx, -72(%rsp,%rbp,4)
	leal	-1(%rsi), %ecx
	testl	%esi, %esi
	cmovel	%r8d, %ecx
	leal	-1(%rdx), %esi
	testl	%edx, %edx
	cmovel	%r8d, %esi
	movl	-72(%rsp,%rcx,4), %edx
	roll	$9, %edx
	movl	-72(%rsp,%rsi,4), %ebp
	roll	$13, %ebp
	addl	%edx, %ebp
	movl	%ebp, -72(%rsp,%rcx,4)
	leal	-1(%rcx), %edx
	testl	%ecx, %ecx
	cmovel	%r8d, %edx
	movl	%edx, -80(%rsp)
	leal	-1(%rsi), %ecx
	testl	%esi, %esi
	cmovel	%r8d, %ecx
	movl	%ecx, -76(%rsp)
	movl	%ebp, %ecx
	shll	$20, %ecx
	shrl	$12, %ebp
	orl	$1072693248, %ebp       # imm = 0x3FF00000
	shlq	$32, %rbp
	orq	%rcx, %rbp
	movd	%rbp, %xmm2
	addsd	%xmm0, %xmm2
	mulsd	%xmm1, %xmm2
	cvttsd2si	%xmm2, %edx
	cmpl	$256, %edx              # imm = 0x100
	jl	.LBB2_4
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB2_2 Depth=1
	movb	$-1, %dl
.LBB2_4:                                # %.lr.ph
                                        #   in Loop: Header=BB2_2 Depth=1
	xorb	%dl, (%rdi)
	testl	%eax, %eax
	jne	.LBB2_5
.LBB2_6:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	LAME_decrypt, .Lfunc_end2-LAME_decrypt
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in scanautoit()\n"
	.size	.L.str, 17

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"autoit: Can't create temporary directory %s\n"
	.size	.L.str.1, 45

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"autoit: Extracting files to %s\n"
	.size	.L.str.2, 32

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"autoit: unknown method\n"
	.size	.L.str.3, 24

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"autoit: no FILE magic found, extraction complete\n"
	.size	.L.str.4, 50

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"autoit: magic string '%s'\n"
	.size	.L.str.5, 27

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"autoit: original filename '%s'\n"
	.size	.L.str.6, 32

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"autoit: bad file size - giving up\n"
	.size	.L.str.7, 35

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"autoit: skipping empty file\n"
	.size	.L.str.8, 29

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"autoit: compressed size: %x\n"
	.size	.L.str.9, 29

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"autoit: advertised uncompressed size %x\n"
	.size	.L.str.10, 41

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"autoit: ref chksum: %x\n"
	.size	.L.str.11, 24

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"autoit: skipping file due to size limit (%u, max: %lu)\n"
	.size	.L.str.12, 56

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"autoit: failed to read compressed stream. broken/truncated file?\n"
	.size	.L.str.13, 66

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"autoit: file is compressed\n"
	.size	.L.str.14, 28

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"autoit: bad magic or unsupported version\n"
	.size	.L.str.15, 42

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"autoit: uncompressed size again: %x\n"
	.size	.L.str.16, 37

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"autoit: decompression error - partial file may exist\n"
	.size	.L.str.17, 54

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"autoit: file is not compressed\n"
	.size	.L.str.18, 32

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"%s/autoit.%.3u"
	.size	.L.str.19, 15

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"autoit: Can't create file %s\n"
	.size	.L.str.20, 30

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"autoit: cannot write %d bytes\n"
	.size	.L.str.21, 31

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"autoit: file extracted to %s\n"
	.size	.L.str.22, 30

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"autoit: file successfully extracted\n"
	.size	.L.str.23, 37

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"autoit: files limit reached (max: %u)\n"
	.size	.L.str.24, 39

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"autoit: getbits() - not enough bits available\n"
	.size	.L.str.25, 47

	.type	ea06.prefixes,@object   # @ea06.prefixes
	.section	.rodata.cst8,"aM",@progbits,8
ea06.prefixes:
	.ascii	"\000\000@$\000.\"#"
	.size	ea06.prefixes, 8

	.type	.L.str.26,@object       # @.str.26
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.26:
	.asciz	","
	.size	.L.str.26, 2

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"="
	.size	.L.str.27, 2

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	">"
	.size	.L.str.28, 2

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"<"
	.size	.L.str.29, 2

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"<>"
	.size	.L.str.30, 3

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	">="
	.size	.L.str.31, 3

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"<="
	.size	.L.str.32, 3

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"("
	.size	.L.str.33, 2

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	")"
	.size	.L.str.34, 2

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"+"
	.size	.L.str.35, 2

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"-"
	.size	.L.str.36, 2

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"/"
	.size	.L.str.37, 2

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"*"
	.size	.L.str.38, 2

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"&"
	.size	.L.str.39, 2

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"["
	.size	.L.str.40, 2

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"]"
	.size	.L.str.41, 2

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"=="
	.size	.L.str.42, 3

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"^"
	.size	.L.str.43, 2

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"+="
	.size	.L.str.44, 3

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"-="
	.size	.L.str.45, 3

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"/="
	.size	.L.str.46, 3

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"*="
	.size	.L.str.47, 3

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"&="
	.size	.L.str.48, 3

	.type	.Lea06.opers,@object    # @ea06.opers
	.section	.rodata,"a",@progbits
	.p2align	4
.Lea06.opers:
	.quad	.L.str.26
	.quad	.L.str.27
	.quad	.L.str.28
	.quad	.L.str.29
	.quad	.L.str.30
	.quad	.L.str.31
	.quad	.L.str.32
	.quad	.L.str.33
	.quad	.L.str.34
	.quad	.L.str.35
	.quad	.L.str.36
	.quad	.L.str.37
	.quad	.L.str.38
	.quad	.L.str.39
	.quad	.L.str.40
	.quad	.L.str.41
	.quad	.L.str.42
	.quad	.L.str.43
	.quad	.L.str.44
	.quad	.L.str.45
	.quad	.L.str.46
	.quad	.L.str.47
	.quad	.L.str.48
	.size	.Lea06.opers, 184

	.type	.L.str.49,@object       # @.str.49
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.49:
	.asciz	"autoit: no FILE magic found, giving up\n"
	.size	.L.str.49, 40

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	">>>AUTOIT SCRIPT<<<"
	.size	.L.str.50, 20

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"autoit: magic string too long to print\n"
	.size	.L.str.51, 40

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"autoit: file is too short\n"
	.size	.L.str.52, 27

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"autoit: script has got %u lines\n"
	.size	.L.str.53, 33

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"autoit: not enough space for an int\n"
	.size	.L.str.54, 37

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"0x%08x "
	.size	.L.str.55, 8

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"autoit: not enough space for an int64\n"
	.size	.L.str.56, 39

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"0x%016lx "
	.size	.L.str.57, 10

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"autoit: not enough space for a double\n"
	.size	.L.str.58, 39

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"%g "
	.size	.L.str.59, 4

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"autoit: not enough space for size\n"
	.size	.L.str.60, 35

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"autoit: size too big - needed %d, total %d, avail %d\n"
	.size	.L.str.61, 54

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"%s "
	.size	.L.str.62, 4

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"autoit: found unknown op (%x)\n"
	.size	.L.str.63, 31

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"autoit: decompilation aborted - partial script may exist\n"
	.size	.L.str.64, 58

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"autoit: %s extracted to %s\n"
	.size	.L.str.65, 28

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"script"
	.size	.L.str.66, 7

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"file"
	.size	.L.str.67, 5

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"autoit: %s successfully extracted\n"
	.size	.L.str.68, 35

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"autoit: Files limit reached (max: %u)\n"
	.size	.L.str.69, 39


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
