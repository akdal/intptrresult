	.text
	.file	"term.bc"
	.globl	term_Init
	.p2align	4, 0x90
	.type	term_Init,@function
term_Init:                              # @term_Init
	.cfi_startproc
# BB#0:
	movl	$1, term_MARK(%rip)
	movl	$0, term_STAMP(%rip)
	movl	$0, term_STAMPBLOCKED(%rip)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, term_STAMPOVERFLOW+64(%rip)
	movaps	%xmm0, term_STAMPOVERFLOW+48(%rip)
	movaps	%xmm0, term_STAMPOVERFLOW+32(%rip)
	movaps	%xmm0, term_STAMPOVERFLOW+16(%rip)
	movaps	%xmm0, term_STAMPOVERFLOW(%rip)
	movl	$0, term_STAMPUSERS(%rip)
	retq
.Lfunc_end0:
	.size	term_Init, .Lfunc_end0-term_Init
	.cfi_endproc

	.globl	term_Create
	.p2align	4, 0x90
	.type	term_Create,@function
term_Create:                            # @term_Create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$32, %edi
	callq	memory_Malloc
	movl	%ebp, (%rax)
	movq	%rbx, 16(%rax)
	movq	$0, 8(%rax)
	movq	$0, 24(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	term_Create, .Lfunc_end1-term_Create
	.cfi_endproc

	.globl	term_CreateAddFather
	.p2align	4, 0x90
	.type	term_CreateAddFather,@function
term_CreateAddFather:                   # @term_CreateAddFather
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$32, %edi
	callq	memory_Malloc
	movl	%ebp, (%rax)
	movq	%rbx, 16(%rax)
	movq	$0, 8(%rax)
	movq	$0, 24(%rax)
	testq	%rbx, %rbx
	je	.LBB2_3
	.p2align	4, 0x90
.LBB2_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rcx
	movq	%rax, 8(%rcx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_1
.LBB2_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end2:
	.size	term_CreateAddFather, .Lfunc_end2-term_CreateAddFather
	.cfi_endproc

	.globl	term_CreateStandardVariable
	.p2align	4, 0x90
	.type	term_CreateStandardVariable,@function
term_CreateStandardVariable:            # @term_CreateStandardVariable
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	movl	symbol_STANDARDVARCOUNTER(%rip), %ebx
	incl	%ebx
	movl	%ebx, symbol_STANDARDVARCOUNTER(%rip)
	movl	$32, %edi
	callq	memory_Malloc
	movl	%ebx, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	movq	$0, 24(%rax)
	popq	%rbx
	retq
.Lfunc_end3:
	.size	term_CreateStandardVariable, .Lfunc_end3-term_CreateStandardVariable
	.cfi_endproc

	.globl	term_Delete
	.p2align	4, 0x90
	.type	term_Delete,@function
term_Delete:                            # @term_Delete
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -24
.Lcfi16:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB4_4
	.p2align	4, 0x90
.LBB4_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Delete
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_1
# BB#2:                                 # %._crit_edge
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.LBB4_4
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB4_3
.LBB4_4:                                # %list_Delete.exit
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%r14, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	term_Delete, .Lfunc_end4-term_Delete
	.cfi_endproc

	.globl	term_DeleteIterative
	.p2align	4, 0x90
	.type	term_DeleteIterative,@function
term_DeleteIterative:                   # @term_DeleteIterative
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	16(%r14), %r15
	testq	%r15, %r15
	je	.LBB5_8
# BB#1:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	testq	%r15, %r15
	jne	.LBB5_3
	jmp	.LBB5_4
	.p2align	4, 0x90
.LBB5_6:                                #   in Loop: Header=BB5_5 Depth=2
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB5_5
	jmp	.LBB5_9
	.p2align	4, 0x90
.LBB5_7:                                # %.critedge.thread
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	(%rax), %rcx
	movq	8(%rax), %r14
	movq	%rcx, 8(%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	16(%r14), %r15
	testq	%r15, %r15
	je	.LBB5_4
.LBB5_3:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, %rbx
.LBB5_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_5 Depth 2
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%r14, (%rax)
	testq	%rbx, %rbx
	je	.LBB5_9
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph
                                        #   Parent Loop BB5_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.LBB5_6
	jmp	.LBB5_7
.LBB5_8:
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%r14, (%rax)
.LBB5_9:                                # %.critedge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	term_DeleteIterative, .Lfunc_end5-term_DeleteIterative
	.cfi_endproc

	.globl	term_Equal
	.p2align	4, 0x90
	.type	term_Equal,@function
term_Equal:                             # @term_Equal
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.LBB6_10
# BB#1:
	movl	(%rdi), %ecx
	cmpl	(%rsi), %ecx
	jne	.LBB6_9
# BB#2:
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB6_10
# BB#3:
	movq	16(%rsi), %r14
	testq	%r14, %r14
	je	.LBB6_9
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	8(%r14), %rsi
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB6_9
# BB#5:                                 #   in Loop: Header=BB6_4 Depth=1
	movq	(%rbx), %rbx
	movq	(%r14), %r14
	testq	%rbx, %rbx
	je	.LBB6_7
# BB#6:                                 #   in Loop: Header=BB6_4 Depth=1
	testq	%r14, %r14
	jne	.LBB6_4
.LBB6_7:                                # %.critedge
	testq	%rbx, %rbx
	movl	$0, %eax
	jne	.LBB6_10
# BB#8:
	xorl	%eax, %eax
	testq	%r14, %r14
	sete	%al
	jmp	.LBB6_10
.LBB6_9:
	xorl	%eax, %eax
.LBB6_10:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	term_Equal, .Lfunc_end6-term_Equal
	.cfi_endproc

	.globl	term_EqualIterative
	.p2align	4, 0x90
	.type	term_EqualIterative,@function
term_EqualIterative:                    # @term_EqualIterative
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 64
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	jmp	.LBB7_1
	.p2align	4, 0x90
.LBB7_9:                                #   in Loop: Header=BB7_8 Depth=2
	testq	%r13, %r13
	je	.LBB7_10
# BB#18:                                #   in Loop: Header=BB7_8 Depth=2
	cmpq	$0, 8(%r13)
	jne	.LBB7_19
# BB#20:                                #   in Loop: Header=BB7_8 Depth=2
	movq	(%rbx), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	movq	(%r13), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%r13)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%r13, (%rsi)
	testq	%rcx, %rcx
	movq	%rdx, %r13
	movq	%rcx, %rbx
	jne	.LBB7_8
	jmp	.LBB7_26
	.p2align	4, 0x90
.LBB7_21:                               #   in Loop: Header=BB7_1 Depth=1
	cmpq	$0, 8(%r13)
	je	.LBB7_23
# BB#22:                                # %.critedge.thread
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	(%rcx), %rax
	movq	8(%rcx), %r14
	movq	%rax, 8(%rbx)
	movq	8(%r13), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %r12
	movq	%rcx, 8(%r13)
.LBB7_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_8 Depth 2
	movl	(%r14), %ecx
	movl	(%r12), %edx
	cmpl	%edx, %ecx
	jne	.LBB7_5
# BB#2:                                 #   in Loop: Header=BB7_1 Depth=1
	movq	16(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB7_6
# BB#3:                                 #   in Loop: Header=BB7_1 Depth=1
	cmpq	$0, 16(%r12)
	je	.LBB7_6
# BB#4:                                 #   in Loop: Header=BB7_1 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbp, 8(%r15)
	movq	%rbx, (%r15)
	movq	16(%r12), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movl	(%r14), %ecx
	movl	(%r12), %edx
	movq	%r15, %rbx
	movq	%rax, %r13
.LBB7_5:                                #   in Loop: Header=BB7_1 Depth=1
	cmpl	%edx, %ecx
	jne	.LBB7_14
.LBB7_6:                                # %.thread
                                        #   in Loop: Header=BB7_1 Depth=1
	cmpq	$0, 16(%r14)
	setne	%al
	cmpq	$0, 16(%r12)
	setne	%cl
	xorb	%al, %cl
	jne	.LBB7_14
# BB#7:                                 # %.preheader
                                        #   in Loop: Header=BB7_1 Depth=1
	movl	$1, %eax
	testq	%rbx, %rbx
	je	.LBB7_26
	.p2align	4, 0x90
.LBB7_8:                                # %.lr.ph
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB7_9
	jmp	.LBB7_21
	.p2align	4, 0x90
.LBB7_23:                               # %.lr.ph.i62
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB7_23
# BB#24:                                # %list_StackFree.exit63
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.LBB7_26
.LBB7_25:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r13)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r13, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r13
	jne	.LBB7_25
	jmp	.LBB7_26
.LBB7_10:
	movb	$1, %cl
	xorl	%r13d, %r13d
	jmp	.LBB7_11
.LBB7_19:
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_11:                               # %.lr.ph.i72
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB7_11
# BB#12:                                # %list_StackFree.exit73
	xorl	%eax, %eax
	testb	%cl, %cl
	jne	.LBB7_26
	.p2align	4, 0x90
.LBB7_13:                               # %.lr.ph.i67
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r13)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r13, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r13
	jne	.LBB7_13
.LBB7_26:                               # %list_StackFree.exit80
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_14:
	testq	%rbx, %rbx
	je	.LBB7_16
	.p2align	4, 0x90
.LBB7_15:                               # %.lr.ph.i84
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB7_15
.LBB7_16:                               # %list_StackFree.exit85
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.LBB7_26
	.p2align	4, 0x90
.LBB7_17:                               # %.lr.ph.i79
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r13)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r13, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r13
	jne	.LBB7_17
	jmp	.LBB7_26
.Lfunc_end7:
	.size	term_EqualIterative, .Lfunc_end7-term_EqualIterative
	.cfi_endproc

	.globl	term_VariableEqual
	.p2align	4, 0x90
	.type	term_VariableEqual,@function
term_VariableEqual:                     # @term_VariableEqual
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	(%rsi), %ecx
	sete	%al
	retq
.Lfunc_end8:
	.size	term_VariableEqual, .Lfunc_end8-term_VariableEqual
	.cfi_endproc

	.globl	term_IsGround
	.p2align	4, 0x90
	.type	term_IsGround,@function
term_IsGround:                          # @term_IsGround
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 32
.Lcfi44:
	.cfi_offset %rbx, -24
.Lcfi45:
	.cfi_offset %r14, -16
	movq	16(%rdi), %r14
	testq	%r14, %r14
	je	.LBB9_11
# BB#1:                                 # %.preheader31.preheader
	xorl	%ebx, %ebx
	testq	%r14, %r14
	jne	.LBB9_3
	jmp	.LBB9_4
	.p2align	4, 0x90
.LBB9_7:                                #   in Loop: Header=BB9_6 Depth=1
	movq	(%rbx), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB9_6
	jmp	.LBB9_12
	.p2align	4, 0x90
.LBB9_10:                               # %.critedge.thread
	movq	(%rcx), %rax
	movq	8(%rcx), %rdi
	movq	%rax, 8(%rbx)
	movq	16(%rdi), %r14
	testq	%r14, %r14
	je	.LBB9_4
.LBB9_3:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, %rbx
	jmp	.LBB9_5
	.p2align	4, 0x90
.LBB9_4:
	cmpl	$0, (%rdi)
	jg	.LBB9_8
.LBB9_5:                                # %.preheader
	movl	$1, %eax
	testq	%rbx, %rbx
	je	.LBB9_12
	.p2align	4, 0x90
.LBB9_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB9_7
	jmp	.LBB9_10
.LBB9_11:
	xorl	%eax, %eax
	cmpl	$0, (%rdi)
	setle	%al
.LBB9_12:                               # %list_StackFree.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB9_8:
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB9_12
	.p2align	4, 0x90
.LBB9_9:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB9_9
	jmp	.LBB9_12
.Lfunc_end9:
	.size	term_IsGround, .Lfunc_end9-term_IsGround
	.cfi_endproc

	.globl	term_IsTerm
	.p2align	4, 0x90
	.type	term_IsTerm,@function
term_IsTerm:                            # @term_IsTerm
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB10_1
# BB#2:
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 16
	movl	(%rdi), %edi
	callq	symbol_IsSymbol
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	retq
.LBB10_1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.Lfunc_end10:
	.size	term_IsTerm, .Lfunc_end10-term_IsTerm
	.cfi_endproc

	.globl	term_IsTermList
	.p2align	4, 0x90
	.type	term_IsTermList,@function
term_IsTermList:                        # @term_IsTermList
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 32
.Lcfi50:
	.cfi_offset %rbx, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB11_5
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.LBB11_6
# BB#3:                                 # %term_IsTerm.exit
                                        #   in Loop: Header=BB11_2 Depth=1
	movl	(%rax), %edi
	callq	symbol_IsSymbol
	testl	%eax, %eax
	je	.LBB11_6
# BB#4:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB11_2
.LBB11_5:
	movl	$1, %ebp
.LBB11_6:                               # %term_IsTerm.exit.thread
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end11:
	.size	term_IsTermList, .Lfunc_end11-term_IsTermList
	.cfi_endproc

	.globl	term_AllArgsAreVar
	.p2align	4, 0x90
	.type	term_AllArgsAreVar,@function
term_AllArgsAreVar:                     # @term_AllArgsAreVar
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rcx
	movl	$1, %eax
	testq	%rcx, %rcx
	jne	.LBB12_3
	jmp	.LBB12_5
	.p2align	4, 0x90
.LBB12_1:                               #   in Loop: Header=BB12_3 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB12_5
.LBB12_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdx
	cmpl	$0, (%rdx)
	jg	.LBB12_1
# BB#4:
	xorl	%eax, %eax
.LBB12_5:                               # %._crit_edge
	retq
.Lfunc_end12:
	.size	term_AllArgsAreVar, .Lfunc_end12-term_AllArgsAreVar
	.cfi_endproc

	.globl	term_Copy
	.p2align	4, 0x90
	.type	term_Copy,@function
term_Copy:                              # @term_Copy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 32
.Lcfi55:
	.cfi_offset %rbx, -32
.Lcfi56:
	.cfi_offset %r14, -24
.Lcfi57:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB13_5
# BB#1:
	callq	list_Copy
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB13_5
# BB#2:                                 # %.lr.ph.preheader
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB13_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Copy
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB13_3
	jmp	.LBB13_6
.LBB13_5:
	xorl	%r14d, %r14d
.LBB13_6:                               # %.loopexit
	movl	(%r15), %ebx
	movl	$32, %edi
	callq	memory_Malloc
	movl	%ebx, (%rax)
	movq	%r14, 16(%rax)
	movq	$0, 8(%rax)
	movq	$0, 24(%rax)
	movl	24(%r15), %ecx
	movl	%ecx, 24(%rax)
	movl	28(%r15), %ecx
	movl	%ecx, 28(%rax)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	term_Copy, .Lfunc_end13-term_Copy
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_0:
	.zero	16
	.text
	.globl	term_CopyIterative
	.p2align	4, 0x90
	.type	term_CopyIterative,@function
term_CopyIterative:                     # @term_CopyIterative
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 64
.Lcfi65:
	.cfi_offset %rbx, -56
.Lcfi66:
	.cfi_offset %r12, -48
.Lcfi67:
	.cfi_offset %r13, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	cmpq	$0, 16(%r13)
	movl	(%r13), %ebp
	je	.LBB14_8
# BB#1:
	movslq	%ebp, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%rbx, 8(%r12)
	movq	$0, (%r12)
	movq	16(%r13), %rdi
	callq	list_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	movq	$0, (%r15)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbx, 8(%r14)
	movq	$0, (%r14)
	testq	%r12, %r12
	jne	.LBB14_2
	jmp	.LBB14_9
	.p2align	4, 0x90
.LBB14_5:                               # %.outer.backedge.thread95
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	8(%rdx), %rsi
	movq	%r13, 8(%rsi)
	movq	8(%rdx), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, 8(%rdx)
	movq	%rdx, %r14
	movq	%rcx, %r15
	movq	%rax, %r12
.LBB14_2:                               # %.lr.ph.split.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_6 Depth 2
	movq	8(%r14), %rbp
	testq	%rbp, %rbp
	jne	.LBB14_6
	jmp	.LBB14_4
	.p2align	4, 0x90
.LBB14_7:                               # %.lr.ph.split
                                        #   in Loop: Header=BB14_6 Depth=2
	movl	$32, %edi
	callq	memory_Malloc
	movl	%ebx, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	movq	$0, 24(%rax)
	movq	%rax, 8(%rbp)
	movq	8(%r14), %rax
	movq	(%rax), %rbp
	movq	%rbp, 8(%r14)
	testq	%rbp, %rbp
	je	.LBB14_4
.LBB14_6:                               # %.lr.ph
                                        #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %r13
	movl	(%r13), %ebx
	cmpq	$0, 16(%r13)
	je	.LBB14_7
# BB#10:                                # %.outer.backedge
                                        #   in Loop: Header=BB14_2 Depth=1
	movslq	%ebx, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	%r12, (%rbx)
	movq	16(%r13), %rdi
	callq	list_Copy
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r12, 8(%rbp)
	movq	%r15, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r14, (%rax)
	testq	%rbx, %rbx
	movq	%rax, %r14
	movq	%rbp, %r15
	movq	%rbx, %r12
	jne	.LBB14_2
	jmp	.LBB14_9
	.p2align	4, 0x90
.LBB14_4:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB14_2 Depth=1
	movl	8(%r12), %ebx
	movq	8(%r15), %rbp
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movl	%ebx, (%r13)
	movq	%rbp, 16(%r13)
	movq	$0, 8(%r13)
	movq	$0, 24(%r13)
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	movq	(%r15), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r15)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r15, (%rdx)
	movq	(%r14), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%r14)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%r14, (%rsi)
	testq	%rax, %rax
	jne	.LBB14_5
	jmp	.LBB14_9
.LBB14_8:
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movl	%ebp, (%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r13)
	movq	$0, 24(%r13)
.LBB14_9:                               # %.loopexit
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	term_CopyIterative, .Lfunc_end14-term_CopyIterative
	.cfi_endproc

	.globl	term_CopyWithEmptyArgListNode
	.p2align	4, 0x90
	.type	term_CopyWithEmptyArgListNode,@function
term_CopyWithEmptyArgListNode:          # @term_CopyWithEmptyArgListNode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi74:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi77:
	.cfi_def_cfa_offset 64
.Lcfi78:
	.cfi_offset %rbx, -56
.Lcfi79:
	.cfi_offset %r12, -48
.Lcfi80:
	.cfi_offset %r13, -40
.Lcfi81:
	.cfi_offset %r14, -32
.Lcfi82:
	.cfi_offset %r15, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB15_8
# BB#1:
	movq	%rbx, %rdi
	callq	list_Copy
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB15_7
# BB#2:                                 # %.lr.ph.preheader
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB15_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r13, %rbx
	je	.LBB15_5
# BB#4:                                 #   in Loop: Header=BB15_3 Depth=1
	movq	8(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	term_CopyWithEmptyArgListNode
	movq	%rax, 8(%rbp)
	jmp	.LBB15_6
	.p2align	4, 0x90
.LBB15_5:                               #   in Loop: Header=BB15_3 Depth=1
	movq	$0, 8(%rbp)
	movq	%rbp, (%r15)
.LBB15_6:                               #   in Loop: Header=BB15_3 Depth=1
	movq	(%rbp), %rbp
	movq	(%rbx), %rbx
	testq	%rbp, %rbp
	jne	.LBB15_3
.LBB15_7:                               # %._crit_edge
	movl	$32, %edi
	callq	memory_Malloc
	movl	(%r14), %ecx
	movl	%ecx, (%rax)
	movq	%r12, 16(%rax)
	movq	$0, 8(%rax)
	jmp	.LBB15_9
.LBB15_8:
	movl	(%r14), %ebx
	movl	$32, %edi
	callq	memory_Malloc
	movl	%ebx, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	movq	$0, 24(%rax)
.LBB15_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	term_CopyWithEmptyArgListNode, .Lfunc_end15-term_CopyWithEmptyArgListNode
	.cfi_endproc

	.globl	term_PrintWithEmptyArgListNode
	.p2align	4, 0x90
	.type	term_PrintWithEmptyArgListNode,@function
term_PrintWithEmptyArgListNode:         # @term_PrintWithEmptyArgListNode
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 16
.Lcfi85:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB16_6
# BB#1:
	cmpq	$0, 16(%rbx)
	je	.LBB16_3
# BB#2:
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	movl	(%rbx), %edi
	callq	symbol_Print
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	16(%rbx), %rsi
	movl	$term_PrintWithEmptyArgListNode, %edi
	callq	list_Apply
.LBB16_5:
	movq	stdout(%rip), %rsi
	movl	$41, %edi
	popq	%rbx
	jmp	_IO_putc                # TAILCALL
.LBB16_6:
	movq	stdout(%rip), %rcx
	movl	$.L.str, %edi
	movl	$6, %esi
	movl	$1, %edx
	popq	%rbx
	jmp	fwrite                  # TAILCALL
.LBB16_3:
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.LBB16_4
# BB#7:
	popq	%rbx
	jmp	symbol_Print            # TAILCALL
.LBB16_4:
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	movl	(%rbx), %edi
	callq	symbol_Print
	jmp	.LBB16_5
.Lfunc_end16:
	.size	term_PrintWithEmptyArgListNode, .Lfunc_end16-term_PrintWithEmptyArgListNode
	.cfi_endproc

	.globl	term_ReplaceSubtermBy
	.p2align	4, 0x90
	.type	term_ReplaceSubtermBy,@function
term_ReplaceSubtermBy:                  # @term_ReplaceSubtermBy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi89:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi90:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi92:
	.cfi_def_cfa_offset 64
.Lcfi93:
	.cfi_offset %rbx, -56
.Lcfi94:
	.cfi_offset %r12, -48
.Lcfi95:
	.cfi_offset %r13, -40
.Lcfi96:
	.cfi_offset %r14, -32
.Lcfi97:
	.cfi_offset %r15, -24
.Lcfi98:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rdi, %rbp
	movq	%rsi, %rdi
	callq	term_Copy
	movq	%rax, %r12
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	term_Equal
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jne	.LBB17_11
# BB#1:
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	16(%rbp), %rax
	testq	%rax, %rax
	je	.LBB17_11
# BB#2:                                 # %.lr.ph32.preheader
	movl	stack_POINTER(%rip), %ebx
	leal	1(%rbx), %ebp
	movl	%ebp, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rbx,8)
	xorl	%r15d, %r15d
.LBB17_4:                               # %.lr.ph32
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_9 Depth 2
	leal	-1(%rbp), %eax
	movq	stack_STACK(,%rax,8), %r14
	movq	(%r14), %rcx
	movq	8(%r14), %r13
	movq	%rcx, stack_STACK(,%rax,8)
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB17_6
# BB#5:                                 #   in Loop: Header=BB17_4 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	term_Copy
	movq	%rax, 8(%r14)
	movq	%r13, %rdi
	callq	term_Delete
	movl	$1, %r15d
	movl	stack_POINTER(%rip), %ebp
	cmpl	%ebx, %ebp
	jne	.LBB17_9
	jmp	.LBB17_11
.LBB17_6:                               #   in Loop: Header=BB17_4 Depth=1
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.LBB17_8
# BB#7:                                 #   in Loop: Header=BB17_4 Depth=1
	movl	%ebp, %ecx
	leal	1(%rbp), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rcx,8)
	movl	%edx, %ebp
.LBB17_8:                               # %.preheader
                                        #   in Loop: Header=BB17_4 Depth=1
	cmpl	%ebx, %ebp
	je	.LBB17_11
	.p2align	4, 0x90
.LBB17_9:                               # %.lr.ph
                                        #   Parent Loop BB17_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rbp), %eax
	cmpq	$0, stack_STACK(,%rax,8)
	jne	.LBB17_3
# BB#10:                                #   in Loop: Header=BB17_9 Depth=2
	movl	%eax, stack_POINTER(%rip)
	cmpl	%eax, %ebx
	movl	%eax, %ebp
	jne	.LBB17_9
	jmp	.LBB17_11
	.p2align	4, 0x90
.LBB17_3:                               # %.critedge.loopexit
                                        #   in Loop: Header=BB17_4 Depth=1
	cmpl	%ebp, %ebx
	jne	.LBB17_4
.LBB17_11:                              # %.loopexit
	movq	%r12, %rdi
	callq	term_Delete
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	term_ReplaceSubtermBy, .Lfunc_end17-term_ReplaceSubtermBy
	.cfi_endproc

	.globl	term_ReplaceVariable
	.p2align	4, 0x90
	.type	term_ReplaceVariable,@function
term_ReplaceVariable:                   # @term_ReplaceVariable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi103:
	.cfi_def_cfa_offset 48
.Lcfi104:
	.cfi_offset %rbx, -40
.Lcfi105:
	.cfi_offset %r14, -32
.Lcfi106:
	.cfi_offset %r15, -24
.Lcfi107:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %ebp
	movq	%rdi, %r14
	cmpl	%ebp, (%r14)
	jne	.LBB18_2
# BB#1:
	movl	(%r15), %eax
	movl	%eax, (%r14)
	movq	16(%r15), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 16(%r14)
	jmp	.LBB18_5
.LBB18_2:
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB18_5
	.p2align	4, 0x90
.LBB18_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	term_ReplaceVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB18_3
.LBB18_5:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	term_ReplaceVariable, .Lfunc_end18-term_ReplaceVariable
	.cfi_endproc

	.globl	term_ExchangeVariable
	.p2align	4, 0x90
	.type	term_ExchangeVariable,@function
term_ExchangeVariable:                  # @term_ExchangeVariable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 32
.Lcfi111:
	.cfi_offset %rbx, -32
.Lcfi112:
	.cfi_offset %r14, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	cmpl	%ebp, (%rdi)
	jne	.LBB19_2
# BB#1:
	movl	%r14d, (%rdi)
	jmp	.LBB19_5
.LBB19_2:
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB19_5
	.p2align	4, 0x90
.LBB19_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	%ebp, %esi
	movl	%r14d, %edx
	callq	term_ExchangeVariable
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB19_3
.LBB19_5:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end19:
	.size	term_ExchangeVariable, .Lfunc_end19-term_ExchangeVariable
	.cfi_endproc

	.globl	term_SubstituteVariable
	.p2align	4, 0x90
	.type	term_SubstituteVariable,@function
term_SubstituteVariable:                # @term_SubstituteVariable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi114:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi115:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi116:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi117:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 48
.Lcfi119:
	.cfi_offset %rbx, -48
.Lcfi120:
	.cfi_offset %r12, -40
.Lcfi121:
	.cfi_offset %r14, -32
.Lcfi122:
	.cfi_offset %r15, -24
.Lcfi123:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movl	%edi, %r15d
	movq	(%rbx), %rax
	cmpl	%r15d, (%rax)
	jne	.LBB20_2
# BB#1:
	movq	%r14, %rdi
	callq	term_Copy
	movl	(%r14), %ecx
	movq	(%rbx), %rdx
	movl	%ecx, (%rdx)
	movq	16(%rax), %rcx
	movq	(%rbx), %rdx
	movq	%rcx, 16(%rdx)
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
	movl	$1, %ebx
	jmp	.LBB20_5
.LBB20_2:
	movq	16(%rax), %rbp
	xorl	%ebx, %ebx
	testq	%rbp, %rbp
	je	.LBB20_5
# BB#3:                                 # %.lr.ph.preheader
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB20_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	8(%rbp), %rdx
	movl	%r15d, %edi
	movq	%r14, %rsi
	callq	term_SubstituteVariable
	testl	%eax, %eax
	cmovnel	%r12d, %ebx
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB20_4
.LBB20_5:                               # %.loopexit
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	term_SubstituteVariable, .Lfunc_end20-term_SubstituteVariable
	.cfi_endproc

	.globl	term_ListOfConstants
	.p2align	4, 0x90
	.type	term_ListOfConstants,@function
term_ListOfConstants:                   # @term_ListOfConstants
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi124:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi125:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi126:
	.cfi_def_cfa_offset 32
.Lcfi127:
	.cfi_offset %rbx, -24
.Lcfi128:
	.cfi_offset %r14, -16
	movslq	(%rdi), %rbx
	testq	%rbx, %rbx
	jns	.LBB21_3
# BB#1:                                 # %symbol_IsConstant.exit
	movl	%ebx, %eax
	negl	%eax
	testl	symbol_TYPEMASK(%rip), %eax
	je	.LBB21_2
.LBB21_3:                               # %symbol_IsConstant.exit.thread
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB21_4
# BB#5:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB21_6:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_10 Depth 2
	movq	8(%rbx), %rdi
	callq	term_ListOfConstants
	testq	%rax, %rax
	je	.LBB21_7
# BB#8:                                 #   in Loop: Header=BB21_6 Depth=1
	testq	%r14, %r14
	je	.LBB21_12
# BB#9:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB21_6 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB21_10:                              # %.preheader.i
                                        #   Parent Loop BB21_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB21_10
# BB#11:                                #   in Loop: Header=BB21_6 Depth=1
	movq	%r14, (%rcx)
	jmp	.LBB21_12
	.p2align	4, 0x90
.LBB21_7:                               #   in Loop: Header=BB21_6 Depth=1
	movq	%r14, %rax
.LBB21_12:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB21_6 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r14
	jne	.LBB21_6
	jmp	.LBB21_13
.LBB21_4:
	xorl	%eax, %eax
	jmp	.LBB21_13
.LBB21_2:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
.LBB21_13:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end21:
	.size	term_ListOfConstants, .Lfunc_end21-term_ListOfConstants
	.cfi_endproc

	.globl	term_ListOfFunctions
	.p2align	4, 0x90
	.type	term_ListOfFunctions,@function
term_ListOfFunctions:                   # @term_ListOfFunctions
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi129:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi130:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 32
.Lcfi132:
	.cfi_offset %rbx, -32
.Lcfi133:
	.cfi_offset %r14, -24
.Lcfi134:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movslq	(%r14), %rbx
	testq	%rbx, %rbx
	jns	.LBB22_4
# BB#2:                                 # %symbol_IsFunction.exit
	movl	%ebx, %eax
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	orl	$1, %eax
	cmpl	$1, %eax
	jne	.LBB22_4
# BB#3:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	movq	$0, (%r15)
	jmp	.LBB22_5
.LBB22_4:
	xorl	%r15d, %r15d
.LBB22_5:                               # %symbol_IsFunction.exit.thread
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB22_13
	.p2align	4, 0x90
.LBB22_6:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_9 Depth 2
	movq	8(%rbx), %rdi
	callq	term_ListOfFunctions
	testq	%rax, %rax
	je	.LBB22_11
# BB#7:                                 #   in Loop: Header=BB22_6 Depth=1
	testq	%r15, %r15
	je	.LBB22_12
# BB#8:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB22_6 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB22_9:                               # %.preheader.i
                                        #   Parent Loop BB22_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB22_9
# BB#10:                                #   in Loop: Header=BB22_6 Depth=1
	movq	%r15, (%rcx)
	jmp	.LBB22_12
	.p2align	4, 0x90
.LBB22_11:                              #   in Loop: Header=BB22_6 Depth=1
	movq	%r15, %rax
.LBB22_12:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB22_6 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r15
	jne	.LBB22_6
	jmp	.LBB22_14
.LBB22_13:
	movq	%r15, %rax
.LBB22_14:                              # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end22:
	.size	term_ListOfFunctions, .Lfunc_end22-term_ListOfFunctions
	.cfi_endproc

	.globl	term_CountSymbols
	.p2align	4, 0x90
	.type	term_CountSymbols,@function
term_CountSymbols:                      # @term_CountSymbols
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi135:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi136:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi137:
	.cfi_def_cfa_offset 32
.Lcfi138:
	.cfi_offset %rbx, -24
.Lcfi139:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	(%rbx), %ebp
	testl	%ebp, %ebp
	jns	.LBB23_3
# BB#1:                                 # %symbol_IsFunction.exit
	movl	%ebp, %eax
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	orl	$1, %eax
	cmpl	$1, %eax
	jne	.LBB23_3
# BB#2:
	movl	%ebp, %edi
	callq	symbol_GetCount
	leaq	1(%rax), %rsi
	movl	%ebp, %edi
	callq	symbol_SetCount
.LBB23_3:                               # %symbol_IsFunction.exit.thread
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB23_6
	.p2align	4, 0x90
.LBB23_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_CountSymbols
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB23_4
.LBB23_6:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end23:
	.size	term_CountSymbols, .Lfunc_end23-term_CountSymbols
	.cfi_endproc

	.globl	term_CompareBySymbolOccurences
	.p2align	4, 0x90
	.type	term_CompareBySymbolOccurences,@function
term_CompareBySymbolOccurences:         # @term_CompareBySymbolOccurences
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi140:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi141:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi143:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 48
.Lcfi145:
	.cfi_offset %rbx, -48
.Lcfi146:
	.cfi_offset %r12, -40
.Lcfi147:
	.cfi_offset %r14, -32
.Lcfi148:
	.cfi_offset %r15, -24
.Lcfi149:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	(%r14), %edi
	movl	(%rbx), %r15d
	testl	%edi, %edi
	js	.LBB24_2
# BB#1:
	xorl	%eax, %eax
	testl	%r15d, %r15d
	jns	.LBB24_4
	jmp	.LBB24_5
.LBB24_2:
	movl	%edi, %eax
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	orl	$1, %eax
	cmpl	$1, %eax
	sete	%al
	testl	%r15d, %r15d
	js	.LBB24_5
.LBB24_4:
	xorl	%ecx, %ecx
	testb	%al, %al
	jne	.LBB24_7
	jmp	.LBB24_15
.LBB24_5:
	movl	%r15d, %ecx
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	orl	$1, %ecx
	cmpl	$1, %ecx
	sete	%cl
	testb	%al, %al
	je	.LBB24_15
.LBB24_7:
	movl	$-1, %ebp
	testb	%cl, %cl
	je	.LBB24_17
# BB#8:
	callq	symbol_GetCount
	movq	%rax, %r12
	movl	%r15d, %edi
	callq	symbol_GetCount
	cmpq	%rax, %r12
	ja	.LBB24_17
# BB#9:
	movl	$1, %ebp
	jb	.LBB24_17
# BB#10:
	movq	16(%r14), %rbp
	xorl	%r14d, %r14d
	testq	%rbp, %rbp
	je	.LBB24_16
# BB#11:                                # %.lr.ph.preheader
	addq	$16, %rbx
	.p2align	4, 0x90
.LBB24_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movq	8(%rbp), %rdi
	movq	8(%rbx), %rsi
	callq	term_CompareBySymbolOccurences
	testl	%eax, %eax
	jne	.LBB24_13
# BB#14:                                #   in Loop: Header=BB24_12 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB24_12
	jmp	.LBB24_16
.LBB24_15:
	xorl	%r14d, %r14d
	movl	$1, %ebp
	testb	%cl, %cl
	jne	.LBB24_17
.LBB24_16:                              # %.loopexit
	movl	%r14d, %ebp
.LBB24_17:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_13:
	movl	%eax, %r14d
	jmp	.LBB24_16
.Lfunc_end24:
	.size	term_CompareBySymbolOccurences, .Lfunc_end24-term_CompareBySymbolOccurences
	.cfi_endproc

	.globl	term_CompareAbstract
	.p2align	4, 0x90
	.type	term_CompareAbstract,@function
term_CompareAbstract:                   # @term_CompareAbstract
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi150:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi151:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi152:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi154:
	.cfi_def_cfa_offset 48
.Lcfi155:
	.cfi_offset %rbx, -40
.Lcfi156:
	.cfi_offset %r12, -32
.Lcfi157:
	.cfi_offset %r14, -24
.Lcfi158:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	callq	term_CompareByArity
	testl	%eax, %eax
	jne	.LBB25_16
# BB#1:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	term_CompareBySymbolOccurences
	testl	%eax, %eax
	jne	.LBB25_16
# BB#2:
	movq	%r15, %rdi
	callq	term_ListOfConstants
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	term_ListOfConstants
	movq	%rax, %r12
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	list_CompareMultisetsByElementDistribution
	testq	%rbx, %rbx
	je	.LBB25_4
	.p2align	4, 0x90
.LBB25_3:                               # %.lr.ph.i20
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB25_3
.LBB25_4:                               # %list_Delete.exit21
	testq	%r12, %r12
	je	.LBB25_6
	.p2align	4, 0x90
.LBB25_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r12)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r12, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r12
	jne	.LBB25_5
.LBB25_6:                               # %list_Delete.exit
	testl	%eax, %eax
	jne	.LBB25_16
# BB#7:
	movq	%r15, %rdi
	callq	term_ListOfVariables
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	term_ListOfVariables
	movq	%rax, %r12
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	list_CompareMultisetsByElementDistribution
	testq	%rbx, %rbx
	je	.LBB25_9
	.p2align	4, 0x90
.LBB25_8:                               # %.lr.ph.i30
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB25_8
.LBB25_9:                               # %list_Delete.exit31
	testq	%r12, %r12
	je	.LBB25_11
	.p2align	4, 0x90
.LBB25_10:                              # %.lr.ph.i25
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r12)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r12, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r12
	jne	.LBB25_10
.LBB25_11:                              # %list_Delete.exit26
	testl	%eax, %eax
	jne	.LBB25_16
# BB#12:
	movq	%r15, %rdi
	callq	term_ListOfFunctions
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	term_ListOfFunctions
	movq	%rax, %r14
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	list_CompareMultisetsByElementDistribution
	testq	%rbx, %rbx
	je	.LBB25_14
	.p2align	4, 0x90
.LBB25_13:                              # %.lr.ph.i40
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB25_13
.LBB25_14:                              # %list_Delete.exit41
	testq	%r14, %r14
	je	.LBB25_16
	.p2align	4, 0x90
.LBB25_15:                              # %.lr.ph.i35
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r14)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r14, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r14
	jne	.LBB25_15
.LBB25_16:                              # %list_Delete.exit36
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end25:
	.size	term_CompareAbstract, .Lfunc_end25-term_CompareAbstract
	.cfi_endproc

	.p2align	4, 0x90
	.type	term_CompareByArity,@function
term_CompareByArity:                    # @term_CompareByArity
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi161:
	.cfi_def_cfa_offset 32
.Lcfi162:
	.cfi_offset %rbx, -32
.Lcfi163:
	.cfi_offset %r14, -24
.Lcfi164:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	(%rdi), %edx
	movl	(%rbx), %eax
	testl	%edx, %edx
	jle	.LBB26_2
# BB#1:
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setle	%r14b
	jmp	.LBB26_10
.LBB26_2:
	movl	$-1, %r14d
	testl	%eax, %eax
	jg	.LBB26_10
# BB#3:
	negl	%edx
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rsi
	movslq	%edx, %rdx
	movq	(%rsi,%rdx,8), %rdx
	negl	%eax
	sarl	%cl, %eax
	cltq
	movq	(%rsi,%rax,8), %rax
	movl	16(%rax), %eax
	cmpl	%eax, 16(%rdx)
	ja	.LBB26_10
# BB#4:
	movl	$1, %r14d
	jb	.LBB26_10
# BB#5:
	movq	16(%rdi), %rbp
	xorl	%r14d, %r14d
	testq	%rbp, %rbp
	je	.LBB26_10
# BB#6:                                 # %.lr.ph.preheader
	addq	$16, %rbx
	.p2align	4, 0x90
.LBB26_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movq	8(%rbp), %rdi
	movq	8(%rbx), %rsi
	callq	term_CompareByArity
	testl	%eax, %eax
	jne	.LBB26_8
# BB#9:                                 #   in Loop: Header=BB26_7 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB26_7
	jmp	.LBB26_10
.LBB26_8:
	movl	%eax, %r14d
.LBB26_10:                              # %.loopexit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end26:
	.size	term_CompareByArity, .Lfunc_end26-term_CompareByArity
	.cfi_endproc

	.globl	term_CompareAbstractLEQ
	.p2align	4, 0x90
	.type	term_CompareAbstractLEQ,@function
term_CompareAbstractLEQ:                # @term_CompareAbstractLEQ
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi165:
	.cfi_def_cfa_offset 16
	callq	term_CompareAbstract
	shrl	$31, %eax
	xorl	$1, %eax
	popq	%rcx
	retq
.Lfunc_end27:
	.size	term_CompareAbstractLEQ, .Lfunc_end27-term_CompareAbstractLEQ
	.cfi_endproc

	.globl	term_ComputeSize
	.p2align	4, 0x90
	.type	term_ComputeSize,@function
term_ComputeSize:                       # @term_ComputeSize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi166:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi167:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi168:
	.cfi_def_cfa_offset 32
.Lcfi169:
	.cfi_offset %rbx, -24
.Lcfi170:
	.cfi_offset %rbp, -16
	movq	16(%rdi), %rbx
	movl	$1, %ebp
	testq	%rbx, %rbx
	je	.LBB28_3
	.p2align	4, 0x90
.LBB28_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_ComputeSize
	addl	%eax, %ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB28_1
.LBB28_3:                               # %._crit_edge
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end28:
	.size	term_ComputeSize, .Lfunc_end28-term_ComputeSize
	.cfi_endproc

	.globl	term_RootDistance
	.p2align	4, 0x90
	.type	term_RootDistance,@function
term_RootDistance:                      # @term_RootDistance
	.cfi_startproc
# BB#0:
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB29_1:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rdi), %rdi
	incl	%eax
	testq	%rdi, %rdi
	jne	.LBB29_1
# BB#2:
	retq
.Lfunc_end29:
	.size	term_RootDistance, .Lfunc_end29-term_RootDistance
	.cfi_endproc

	.globl	term_RootDistanceSmaller
	.p2align	4, 0x90
	.type	term_RootDistanceSmaller,@function
term_RootDistanceSmaller:               # @term_RootDistanceSmaller
	.cfi_startproc
# BB#0:
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB30_1:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rdi), %rdi
	incl	%eax
	testq	%rdi, %rdi
	jne	.LBB30_1
# BB#2:                                 # %term_RootDistance.exit.preheader
	movl	$-1, %ecx
	.p2align	4, 0x90
.LBB30_3:                               # %term_RootDistance.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsi), %rsi
	incl	%ecx
	testq	%rsi, %rsi
	jne	.LBB30_3
# BB#4:                                 # %term_RootDistance.exit4
	cmpl	%ecx, %eax
	sbbl	%eax, %eax
	andl	$1, %eax
	retq
.Lfunc_end30:
	.size	term_RootDistanceSmaller, .Lfunc_end30-term_RootDistanceSmaller
	.cfi_endproc

	.globl	term_InstallSize
	.p2align	4, 0x90
	.type	term_InstallSize,@function
term_InstallSize:                       # @term_InstallSize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi171:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi172:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi173:
	.cfi_def_cfa_offset 32
.Lcfi174:
	.cfi_offset %rbx, -32
.Lcfi175:
	.cfi_offset %r14, -24
.Lcfi176:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	16(%r14), %rbx
	movl	$1, %ebp
	testq	%rbx, %rbx
	je	.LBB31_3
	.p2align	4, 0x90
.LBB31_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_InstallSize
	movq	8(%rbx), %rax
	movq	(%rbx), %rbx
	addl	28(%rax), %ebp
	testq	%rbx, %rbx
	jne	.LBB31_1
.LBB31_3:                               # %._crit_edge
	movl	%ebp, 28(%r14)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end31:
	.size	term_InstallSize, .Lfunc_end31-term_InstallSize
	.cfi_endproc

	.globl	term_Depth
	.p2align	4, 0x90
	.type	term_Depth,@function
term_Depth:                             # @term_Depth
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi177:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi178:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi179:
	.cfi_def_cfa_offset 32
.Lcfi180:
	.cfi_offset %rbx, -24
.Lcfi181:
	.cfi_offset %rbp, -16
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB32_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB32_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Depth
	cmpl	%ebp, %eax
	cmoval	%eax, %ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB32_3
# BB#4:                                 # %._crit_edge.loopexit
	incl	%ebp
	jmp	.LBB32_5
.LBB32_1:
	movl	$1, %ebp
.LBB32_5:                               # %._crit_edge
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end32:
	.size	term_Depth, .Lfunc_end32-term_Depth
	.cfi_endproc

	.globl	term_ContainsSymbol
	.p2align	4, 0x90
	.type	term_ContainsSymbol,@function
term_ContainsSymbol:                    # @term_ContainsSymbol
	.cfi_startproc
# BB#0:
	movl	stack_POINTER(%rip), %ecx
	cmpl	%esi, (%rdi)
	jne	.LBB33_1
.LBB33_9:                               # %._crit_edge
	movl	%ecx, stack_POINTER(%rip)
	movl	$1, %eax
	retq
.LBB33_1:
	movl	%ecx, %edx
.LBB33_2:                               # %.lr.ph25
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_5 Depth 2
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB33_4
# BB#3:                                 #   in Loop: Header=BB33_2 Depth=1
	movl	%edx, %edi
	leal	1(%rdx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rdi,8)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
.LBB33_4:                               # %.preheader
                                        #   in Loop: Header=BB33_2 Depth=1
	xorl	%eax, %eax
	cmpl	%ecx, %edx
	je	.LBB33_10
	.p2align	4, 0x90
.LBB33_5:                               # %.lr.ph
                                        #   Parent Loop BB33_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rdx), %edi
	cmpq	$0, stack_STACK(,%rdi,8)
	jne	.LBB33_7
# BB#6:                                 #   in Loop: Header=BB33_5 Depth=2
	movl	%edi, stack_POINTER(%rip)
	cmpl	%edi, %ecx
	movl	%edi, %edx
	jne	.LBB33_5
	jmp	.LBB33_10
	.p2align	4, 0x90
.LBB33_7:                               # %.critedge
                                        #   in Loop: Header=BB33_2 Depth=1
	cmpl	%edx, %ecx
	je	.LBB33_10
# BB#8:                                 #   in Loop: Header=BB33_2 Depth=1
	leal	-1(%rdx), %eax
	movq	stack_STACK(,%rax,8), %rdi
	movq	(%rdi), %r8
	movq	8(%rdi), %rdi
	movq	%r8, stack_STACK(,%rax,8)
	cmpl	%esi, (%rdi)
	jne	.LBB33_2
	jmp	.LBB33_9
.LBB33_10:                              # %.loopexit
	retq
.Lfunc_end33:
	.size	term_ContainsSymbol, .Lfunc_end33-term_ContainsSymbol
	.cfi_endproc

	.globl	term_FindSubterm
	.p2align	4, 0x90
	.type	term_FindSubterm,@function
term_FindSubterm:                       # @term_FindSubterm
	.cfi_startproc
# BB#0:
	movl	stack_POINTER(%rip), %eax
	cmpl	%esi, (%rdi)
	jne	.LBB34_1
.LBB34_12:                              # %._crit_edge
	movl	%eax, stack_POINTER(%rip)
	movq	%rdi, %rax
	retq
.LBB34_1:
	movl	%eax, %ecx
.LBB34_2:                               # %.lr.ph32
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB34_6 Depth 2
	movq	16(%rdi), %rdx
	testq	%rdx, %rdx
	je	.LBB34_4
# BB#3:                                 #   in Loop: Header=BB34_2 Depth=1
	movl	%ecx, %edi
	leal	1(%rcx), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movq	%rdx, stack_STACK(,%rdi,8)
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
.LBB34_4:                               # %.preheader
                                        #   in Loop: Header=BB34_2 Depth=1
	cmpl	%eax, %ecx
	je	.LBB34_5
	.p2align	4, 0x90
.LBB34_6:                               # %.lr.ph
                                        #   Parent Loop BB34_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rcx), %edx
	cmpq	$0, stack_STACK(,%rdx,8)
	jne	.LBB34_9
# BB#7:                                 #   in Loop: Header=BB34_6 Depth=2
	movl	%edx, stack_POINTER(%rip)
	cmpl	%edx, %eax
	movl	%edx, %ecx
	jne	.LBB34_6
	jmp	.LBB34_8
	.p2align	4, 0x90
.LBB34_9:                               # %.critedge
                                        #   in Loop: Header=BB34_2 Depth=1
	cmpl	%ecx, %eax
	je	.LBB34_10
# BB#11:                                #   in Loop: Header=BB34_2 Depth=1
	leal	-1(%rcx), %edx
	movq	stack_STACK(,%rdx,8), %rdi
	movq	(%rdi), %r8
	movq	8(%rdi), %rdi
	movq	%r8, stack_STACK(,%rdx,8)
	cmpl	%esi, (%rdi)
	jne	.LBB34_2
	jmp	.LBB34_12
.LBB34_8:
	xorl	%edi, %edi
	movq	%rdi, %rax
	retq
.LBB34_5:
	xorl	%edi, %edi
	movq	%rdi, %rax
	retq
.LBB34_10:
	xorl	%edi, %edi
	movq	%rdi, %rax
	retq
.Lfunc_end34:
	.size	term_FindSubterm, .Lfunc_end34-term_FindSubterm
	.cfi_endproc

	.globl	term_Sharing
	.p2align	4, 0x90
	.type	term_Sharing,@function
term_Sharing:                           # @term_Sharing
	.cfi_startproc
# BB#0:                                 # %.lr.ph25
	pushq	%rbp
.Lcfi182:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi183:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi184:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi185:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi186:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi187:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi188:
	.cfi_def_cfa_offset 64
.Lcfi189:
	.cfi_offset %rbx, -56
.Lcfi190:
	.cfi_offset %r12, -48
.Lcfi191:
	.cfi_offset %r13, -40
.Lcfi192:
	.cfi_offset %r14, -32
.Lcfi193:
	.cfi_offset %r15, -24
.Lcfi194:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movl	stack_POINTER(%rip), %r15d
	leal	1(%r15), %ebx
	movl	%ebx, stack_POINTER(%rip)
	movq	%r12, stack_STACK(,%r15,8)
	movl	$1, %r14d
.LBB35_1:                               # %.backedge._crit_edge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_7 Depth 2
	leal	-1(%rbx), %r13d
	movq	stack_STACK(,%r13,8), %rbp
	movl	%r13d, stack_POINTER(%rip)
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	term_SharingTerm
	cmpl	$1, %eax
	jg	.LBB35_10
# BB#2:                                 #   in Loop: Header=BB35_1 Depth=1
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB35_3
# BB#5:                                 #   in Loop: Header=BB35_1 Depth=1
	movq	16(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB35_3
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB35_1 Depth=1
	movq	%rbp, %rsi
	callq	term_SharingList
	cmpl	$1, %eax
	jg	.LBB35_10
	.p2align	4, 0x90
.LBB35_7:                               # %.lr.ph33
                                        #   Parent Loop BB35_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rbx), %eax
	movq	8(%rbp), %rcx
	movl	%ebx, stack_POINTER(%rip)
	movq	%rcx, stack_STACK(,%rax,8)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB35_8
# BB#9:                                 # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB35_7 Depth=2
	movq	16(%r12), %rdi
	movq	%rbp, %rsi
	callq	term_SharingList
	incl	%ebx
	cmpl	$2, %eax
	jl	.LBB35_7
	jmp	.LBB35_10
.LBB35_8:                               #   in Loop: Header=BB35_1 Depth=1
	movl	%ebx, %r13d
.LBB35_3:                               # %.backedge
                                        #   in Loop: Header=BB35_1 Depth=1
	cmpl	%r15d, %r13d
	movl	%r13d, %ebx
	jne	.LBB35_1
# BB#4:
	xorl	%r14d, %r14d
.LBB35_10:                              # %.loopexit21
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end35:
	.size	term_Sharing, .Lfunc_end35-term_Sharing
	.cfi_endproc

	.p2align	4, 0x90
	.type	term_SharingTerm,@function
term_SharingTerm:                       # @term_SharingTerm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi195:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi196:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi197:
	.cfi_def_cfa_offset 32
.Lcfi198:
	.cfi_offset %rbx, -32
.Lcfi199:
	.cfi_offset %r14, -24
.Lcfi200:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	xorl	%ebp, %ebp
	cmpq	%r14, %rdi
	sete	%bpl
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB36_3
	.p2align	4, 0x90
.LBB36_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	callq	term_SharingTerm
	addl	%eax, %ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB36_1
.LBB36_3:                               # %._crit_edge
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end36:
	.size	term_SharingTerm, .Lfunc_end36-term_SharingTerm
	.cfi_endproc

	.p2align	4, 0x90
	.type	term_SharingList,@function
term_SharingList:                       # @term_SharingList
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi201:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi202:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi203:
	.cfi_def_cfa_offset 32
.Lcfi204:
	.cfi_offset %rbx, -32
.Lcfi205:
	.cfi_offset %r14, -24
.Lcfi206:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB37_3
	.p2align	4, 0x90
.LBB37_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%ebp, %ebp
	cmpq	%r14, %rbx
	sete	%bpl
	addl	%eax, %ebp
	movq	8(%rbx), %rax
	movq	16(%rax), %rdi
	movq	%r14, %rsi
	callq	term_SharingList
	addl	%ebp, %eax
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB37_1
.LBB37_3:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end37:
	.size	term_SharingList, .Lfunc_end37-term_SharingList
	.cfi_endproc

	.globl	term_AddFatherLinks
	.p2align	4, 0x90
	.type	term_AddFatherLinks,@function
term_AddFatherLinks:                    # @term_AddFatherLinks
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi207:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi208:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi209:
	.cfi_def_cfa_offset 32
.Lcfi210:
	.cfi_offset %rbx, -32
.Lcfi211:
	.cfi_offset %r14, -24
.Lcfi212:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	$0, 8(%r14)
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB38_3
	.p2align	4, 0x90
.LBB38_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r15
	movq	%r15, %rdi
	callq	term_AddFatherLinks
	movq	%r14, 8(%r15)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB38_1
.LBB38_3:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end38:
	.size	term_AddFatherLinks, .Lfunc_end38-term_AddFatherLinks
	.cfi_endproc

	.globl	term_FatherLinksEstablished
	.p2align	4, 0x90
	.type	term_FatherLinksEstablished,@function
term_FatherLinksEstablished:            # @term_FatherLinksEstablished
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi213:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi214:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi215:
	.cfi_def_cfa_offset 32
.Lcfi216:
	.cfi_offset %rbx, -32
.Lcfi217:
	.cfi_offset %r14, -24
.Lcfi218:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB39_6
# BB#1:
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB39_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	cmpq	%r14, 8(%rdi)
	jne	.LBB39_7
# BB#3:                                 #   in Loop: Header=BB39_2 Depth=1
	callq	term_FatherLinksEstablished
	testl	%eax, %eax
	je	.LBB39_7
# BB#4:                                 #   in Loop: Header=BB39_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB39_2
.LBB39_6:
	movl	$1, %ebp
.LBB39_7:                               # %._crit_edge
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end39:
	.size	term_FatherLinksEstablished, .Lfunc_end39-term_FatherLinksEstablished
	.cfi_endproc

	.globl	term_TopLevelTerm
	.p2align	4, 0x90
	.type	term_TopLevelTerm,@function
term_TopLevelTerm:                      # @term_TopLevelTerm
	.cfi_startproc
# BB#0:
	.p2align	4, 0x90
.LBB40_1:                               # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	jne	.LBB40_1
# BB#2:
	retq
.Lfunc_end40:
	.size	term_TopLevelTerm, .Lfunc_end40-term_TopLevelTerm
	.cfi_endproc

	.globl	term_HasPointerSubterm
	.p2align	4, 0x90
	.type	term_HasPointerSubterm,@function
term_HasPointerSubterm:                 # @term_HasPointerSubterm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi219:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi220:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi221:
	.cfi_def_cfa_offset 32
.Lcfi222:
	.cfi_offset %rbx, -32
.Lcfi223:
	.cfi_offset %r14, -24
.Lcfi224:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	$1, %r14d
	cmpq	%rbx, %rdi
	je	.LBB41_6
# BB#1:
	movq	16(%rdi), %rbp
	testq	%rbp, %rbp
	je	.LBB41_5
	.p2align	4, 0x90
.LBB41_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movq	%rbx, %rsi
	callq	term_HasPointerSubterm
	testl	%eax, %eax
	jne	.LBB41_6
# BB#3:                                 #   in Loop: Header=BB41_2 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB41_2
.LBB41_5:
	xorl	%r14d, %r14d
.LBB41_6:                               # %.loopexit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end41:
	.size	term_HasPointerSubterm, .Lfunc_end41-term_HasPointerSubterm
	.cfi_endproc

	.globl	term_HasSubterm
	.p2align	4, 0x90
	.type	term_HasSubterm,@function
term_HasSubterm:                        # @term_HasSubterm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi225:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi226:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi227:
	.cfi_def_cfa_offset 32
.Lcfi228:
	.cfi_offset %rbx, -32
.Lcfi229:
	.cfi_offset %r14, -24
.Lcfi230:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	callq	term_Equal
	movl	$1, %r14d
	testl	%eax, %eax
	jne	.LBB42_1
# BB#2:
	movq	16(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB42_6
	.p2align	4, 0x90
.LBB42_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movq	%rbx, %rsi
	callq	term_HasSubterm
	testl	%eax, %eax
	jne	.LBB42_1
# BB#4:                                 #   in Loop: Header=BB42_3 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB42_3
.LBB42_6:
	xorl	%r14d, %r14d
.LBB42_1:                               # %.loopexit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end42:
	.size	term_HasSubterm, .Lfunc_end42-term_HasSubterm
	.cfi_endproc

	.globl	term_HasProperSuperterm
	.p2align	4, 0x90
	.type	term_HasProperSuperterm,@function
term_HasProperSuperterm:                # @term_HasProperSuperterm
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	jne	.LBB43_1
	jmp	.LBB43_4
	.p2align	4, 0x90
.LBB43_5:                               #   in Loop: Header=BB43_1 Depth=1
	movq	8(%rdi), %rdi
.LBB43_1:                               # =>This Inner Loop Header: Depth=1
	testq	%rdi, %rdi
	je	.LBB43_4
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB43_1 Depth=1
	cmpq	%rsi, %rdi
	jne	.LBB43_5
# BB#3:
	movl	$1, %eax
.LBB43_4:                               # %.loopexit
	retq
.Lfunc_end43:
	.size	term_HasProperSuperterm, .Lfunc_end43-term_HasProperSuperterm
	.cfi_endproc

	.globl	term_Print
	.p2align	4, 0x90
	.type	term_Print,@function
term_Print:                             # @term_Print
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi231:
	.cfi_def_cfa_offset 16
.Lcfi232:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpq	$0, 16(%rbx)
	je	.LBB44_5
# BB#1:
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	movl	(%rbx), %edi
	callq	symbol_Print
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB44_3
	jmp	.LBB44_7
	.p2align	4, 0x90
.LBB44_4:                               # %.backedge
                                        #   in Loop: Header=BB44_3 Depth=1
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB44_7
.LBB44_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Print
	movq	stdout(%rip), %rdi
	callq	fflush
	cmpq	$0, (%rbx)
	jne	.LBB44_4
	jmp	.LBB44_7
.LBB44_5:
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.LBB44_6
# BB#8:
	popq	%rbx
	jmp	symbol_Print            # TAILCALL
.LBB44_6:
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	movl	(%rbx), %edi
	callq	symbol_Print
.LBB44_7:                               # %term_TermListPrint.exit
	movq	stdout(%rip), %rsi
	movl	$41, %edi
	popq	%rbx
	jmp	_IO_putc                # TAILCALL
.Lfunc_end44:
	.size	term_Print, .Lfunc_end44-term_Print
	.cfi_endproc

	.globl	term_TermListPrint
	.p2align	4, 0x90
	.type	term_TermListPrint,@function
term_TermListPrint:                     # @term_TermListPrint
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi233:
	.cfi_def_cfa_offset 16
.Lcfi234:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB45_2
	jmp	.LBB45_4
	.p2align	4, 0x90
.LBB45_3:                               #   in Loop: Header=BB45_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB45_4
.LBB45_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Print
	movq	stdout(%rip), %rdi
	callq	fflush
	cmpq	$0, (%rbx)
	jne	.LBB45_3
.LBB45_4:                               # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end45:
	.size	term_TermListPrint, .Lfunc_end45-term_TermListPrint
	.cfi_endproc

	.globl	term_PrettyPrint
	.p2align	4, 0x90
	.type	term_PrettyPrint,@function
term_PrettyPrint:                       # @term_PrettyPrint
	.cfi_startproc
# BB#0:
	xorl	%esi, %esi
	jmp	term_PrettyPrintIntern  # TAILCALL
.Lfunc_end46:
	.size	term_PrettyPrint, .Lfunc_end46-term_PrettyPrint
	.cfi_endproc

	.p2align	4, 0x90
	.type	term_PrettyPrintIntern,@function
term_PrettyPrintIntern:                 # @term_PrettyPrintIntern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi235:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi236:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi237:
	.cfi_def_cfa_offset 32
.Lcfi238:
	.cfi_offset %rbx, -32
.Lcfi239:
	.cfi_offset %r14, -24
.Lcfi240:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	testl	%r14d, %r14d
	jle	.LBB47_3
# BB#1:                                 # %.lr.ph30.preheader
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB47_2:                               # %.lr.ph30
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	decl	%ebp
	jne	.LBB47_2
.LBB47_3:                               # %._crit_edge31
	movl	(%rbx), %edi
	testl	%edi, %edi
	jns	.LBB47_12
# BB#4:                                 # %symbol_IsJunctor.exit
	movl	%edi, %eax
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$3, %eax
	jne	.LBB47_12
# BB#5:
	cmpq	$0, 16(%rbx)
	je	.LBB47_11
# BB#6:
	callq	symbol_Print
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB47_10
# BB#7:                                 # %.lr.ph
	incl	%r14d
	.p2align	4, 0x90
.LBB47_8:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movl	%r14d, %esi
	callq	term_PrettyPrintIntern
	cmpq	$0, (%rbx)
	je	.LBB47_10
# BB#9:                                 # %.backedge
                                        #   in Loop: Header=BB47_8 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.10, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB47_8
	jmp	.LBB47_10
.LBB47_12:                              # %symbol_IsJunctor.exit.thread
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	term_PrintPrefix        # TAILCALL
.LBB47_11:
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	movl	(%rbx), %edi
	callq	symbol_Print
.LBB47_10:                              # %._crit_edge
	movq	stdout(%rip), %rsi
	movl	$41, %edi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.Lfunc_end47:
	.size	term_PrettyPrintIntern, .Lfunc_end47-term_PrettyPrintIntern
	.cfi_endproc

	.globl	term_FPrint
	.p2align	4, 0x90
	.type	term_FPrint,@function
term_FPrint:                            # @term_FPrint
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi241:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi242:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi243:
	.cfi_def_cfa_offset 32
.Lcfi244:
	.cfi_offset %rbx, -32
.Lcfi245:
	.cfi_offset %r14, -24
.Lcfi246:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpq	$0, 16(%r14)
	je	.LBB48_5
# BB#1:
	movl	$40, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movl	(%r14), %esi
	movq	%r15, %rdi
	callq	symbol_FPrint
	movl	$32, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB48_3
	jmp	.LBB48_7
	.p2align	4, 0x90
.LBB48_4:                               # %.backedge
                                        #   in Loop: Header=BB48_3 Depth=1
	movl	$32, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB48_7
.LBB48_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	term_FPrint
	cmpq	$0, (%rbx)
	jne	.LBB48_4
	jmp	.LBB48_7
.LBB48_5:
	movl	(%r14), %esi
	testl	%esi, %esi
	jle	.LBB48_6
# BB#8:
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	symbol_FPrint           # TAILCALL
.LBB48_6:
	movl	$40, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movl	(%r14), %esi
	movq	%r15, %rdi
	callq	symbol_FPrint
.LBB48_7:                               # %term_TermListFPrint.exit
	movl	$41, %edi
	movq	%r15, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_IO_putc                # TAILCALL
.Lfunc_end48:
	.size	term_FPrint, .Lfunc_end48-term_FPrint
	.cfi_endproc

	.globl	term_TermListFPrint
	.p2align	4, 0x90
	.type	term_TermListFPrint,@function
term_TermListFPrint:                    # @term_TermListFPrint
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi247:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi248:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi249:
	.cfi_def_cfa_offset 32
.Lcfi250:
	.cfi_offset %rbx, -24
.Lcfi251:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	jne	.LBB49_2
	jmp	.LBB49_4
	.p2align	4, 0x90
.LBB49_3:                               #   in Loop: Header=BB49_2 Depth=1
	movl	$32, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB49_4
.LBB49_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	term_FPrint
	cmpq	$0, (%rbx)
	jne	.LBB49_3
.LBB49_4:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end49:
	.size	term_TermListFPrint, .Lfunc_end49-term_TermListFPrint
	.cfi_endproc

	.globl	term_PrintPrefix
	.p2align	4, 0x90
	.type	term_PrintPrefix,@function
term_PrintPrefix:                       # @term_PrintPrefix
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi252:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi253:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi254:
	.cfi_def_cfa_offset 32
.Lcfi255:
	.cfi_offset %rbx, -24
.Lcfi256:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %r14
	movl	(%rbx), %edi
	callq	symbol_Print
	cmpq	$0, %r14
	je	.LBB50_6
# BB#1:
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB50_3
	jmp	.LBB50_5
	.p2align	4, 0x90
.LBB50_4:                               # %.backedge
                                        #   in Loop: Header=BB50_3 Depth=1
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB50_5
.LBB50_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_PrintPrefix
	cmpq	$0, (%rbx)
	jne	.LBB50_4
.LBB50_5:                               # %term_TermListPrintPrefix.exit
	movq	stdout(%rip), %rsi
	movl	$41, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_IO_putc                # TAILCALL
.LBB50_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end50:
	.size	term_PrintPrefix, .Lfunc_end50-term_PrintPrefix
	.cfi_endproc

	.globl	term_TermListPrintPrefix
	.p2align	4, 0x90
	.type	term_TermListPrintPrefix,@function
term_TermListPrintPrefix:               # @term_TermListPrintPrefix
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi257:
	.cfi_def_cfa_offset 16
.Lcfi258:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB51_2
	jmp	.LBB51_4
	.p2align	4, 0x90
.LBB51_3:                               #   in Loop: Header=BB51_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB51_4
.LBB51_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_PrintPrefix
	cmpq	$0, (%rbx)
	jne	.LBB51_3
.LBB51_4:                               # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end51:
	.size	term_TermListPrintPrefix, .Lfunc_end51-term_TermListPrintPrefix
	.cfi_endproc

	.globl	term_FPrintPrefix
	.p2align	4, 0x90
	.type	term_FPrintPrefix,@function
term_FPrintPrefix:                      # @term_FPrintPrefix
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi259:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi260:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi261:
	.cfi_def_cfa_offset 32
.Lcfi262:
	.cfi_offset %rbx, -32
.Lcfi263:
	.cfi_offset %r14, -24
.Lcfi264:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	16(%r14), %rbx
	movl	(%r14), %esi
	callq	symbol_FPrint
	cmpq	$0, %rbx
	je	.LBB52_6
# BB#1:
	movl	$40, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB52_3
	jmp	.LBB52_5
	.p2align	4, 0x90
.LBB52_4:                               # %.backedge
                                        #   in Loop: Header=BB52_3 Depth=1
	movl	$44, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB52_5
.LBB52_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	term_FPrintPrefix
	cmpq	$0, (%rbx)
	jne	.LBB52_4
.LBB52_5:                               # %term_TermListFPrintPrefix.exit
	movl	$41, %edi
	movq	%r15, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_IO_putc                # TAILCALL
.LBB52_6:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end52:
	.size	term_FPrintPrefix, .Lfunc_end52-term_FPrintPrefix
	.cfi_endproc

	.globl	term_TermListFPrintPrefix
	.p2align	4, 0x90
	.type	term_TermListFPrintPrefix,@function
term_TermListFPrintPrefix:              # @term_TermListFPrintPrefix
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi265:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi266:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi267:
	.cfi_def_cfa_offset 32
.Lcfi268:
	.cfi_offset %rbx, -24
.Lcfi269:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	jne	.LBB53_2
	jmp	.LBB53_4
	.p2align	4, 0x90
.LBB53_3:                               #   in Loop: Header=BB53_2 Depth=1
	movl	$44, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB53_4
.LBB53_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	term_FPrintPrefix
	cmpq	$0, (%rbx)
	jne	.LBB53_3
.LBB53_4:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end53:
	.size	term_TermListFPrintPrefix, .Lfunc_end53-term_TermListFPrintPrefix
	.cfi_endproc

	.globl	term_FPrintOtterPrefix
	.p2align	4, 0x90
	.type	term_FPrintOtterPrefix,@function
term_FPrintOtterPrefix:                 # @term_FPrintOtterPrefix
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi270:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi271:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi272:
	.cfi_def_cfa_offset 32
.Lcfi273:
	.cfi_offset %rbx, -32
.Lcfi274:
	.cfi_offset %r14, -24
.Lcfi275:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	16(%r14), %rbx
	movl	(%r14), %esi
	callq	symbol_FPrintOtter
	cmpq	$0, %rbx
	je	.LBB54_6
# BB#1:
	movl	$40, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB54_3
	jmp	.LBB54_5
	.p2align	4, 0x90
.LBB54_4:                               # %.backedge
                                        #   in Loop: Header=BB54_3 Depth=1
	movl	$44, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB54_5
.LBB54_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	term_FPrintOtterPrefix
	cmpq	$0, (%rbx)
	jne	.LBB54_4
.LBB54_5:                               # %term_TermListFPrintOtterPrefix.exit
	movl	$41, %edi
	movq	%r15, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_IO_putc                # TAILCALL
.LBB54_6:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end54:
	.size	term_FPrintOtterPrefix, .Lfunc_end54-term_FPrintOtterPrefix
	.cfi_endproc

	.globl	term_TermListFPrintOtterPrefix
	.p2align	4, 0x90
	.type	term_TermListFPrintOtterPrefix,@function
term_TermListFPrintOtterPrefix:         # @term_TermListFPrintOtterPrefix
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi276:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi277:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi278:
	.cfi_def_cfa_offset 32
.Lcfi279:
	.cfi_offset %rbx, -24
.Lcfi280:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	jne	.LBB55_2
	jmp	.LBB55_4
	.p2align	4, 0x90
.LBB55_3:                               #   in Loop: Header=BB55_2 Depth=1
	movl	$44, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB55_4
.LBB55_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	term_FPrintOtterPrefix
	cmpq	$0, (%rbx)
	jne	.LBB55_3
.LBB55_4:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end55:
	.size	term_TermListFPrintOtterPrefix, .Lfunc_end55-term_TermListFPrintOtterPrefix
	.cfi_endproc

	.globl	term_FPrintPosition
	.p2align	4, 0x90
	.type	term_FPrintPosition,@function
term_FPrintPosition:                    # @term_FPrintPosition
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi281:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi282:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi283:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi284:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi285:
	.cfi_def_cfa_offset 48
.Lcfi286:
	.cfi_offset %rbx, -40
.Lcfi287:
	.cfi_offset %r14, -32
.Lcfi288:
	.cfi_offset %r15, -24
.Lcfi289:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rdi, %r14
	cmpq	%r15, %rsi
	jne	.LBB56_2
	jmp	.LBB56_7
	.p2align	4, 0x90
.LBB56_8:                               #   in Loop: Header=BB56_4 Depth=2
	incl	%ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB56_4
	jmp	.LBB56_9
	.p2align	4, 0x90
.LBB56_5:                               #   in Loop: Header=BB56_2 Depth=1
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	fprintf
	cmpq	%r15, 8(%rbx)
	je	.LBB56_7
# BB#6:                                 # %tailrecurse
                                        #   in Loop: Header=BB56_2 Depth=1
	movl	$46, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	8(%rbx), %rsi
	cmpq	%r15, %rsi
	je	.LBB56_7
.LBB56_2:                               # %.lr.ph34
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB56_4 Depth 2
	movq	16(%rsi), %rbx
	testq	%rbx, %rbx
	je	.LBB56_9
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB56_2 Depth=1
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB56_4:                               # %.lr.ph
                                        #   Parent Loop BB56_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	callq	term_HasPointerSubterm
	testl	%eax, %eax
	je	.LBB56_8
	jmp	.LBB56_5
.LBB56_7:                               # %._crit_edge35
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB56_9:                               # %._crit_edge
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	movl	$.L.str.3, %edx
	movl	$1807, %ecx             # imm = 0x70F
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end56:
	.size	term_FPrintPosition, .Lfunc_end56-term_FPrintPosition
	.cfi_endproc

	.globl	term_Bytes
	.p2align	4, 0x90
	.type	term_Bytes,@function
term_Bytes:                             # @term_Bytes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi290:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi291:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi292:
	.cfi_def_cfa_offset 32
.Lcfi293:
	.cfi_offset %rbx, -32
.Lcfi294:
	.cfi_offset %r14, -24
.Lcfi295:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	16(%r14), %rdi
	callq	list_Bytes
	movl	%eax, %ebp
	addl	$32, %ebp
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB57_3
	.p2align	4, 0x90
.LBB57_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	term_Bytes
	addl	%eax, %ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB57_1
.LBB57_3:                               # %._crit_edge
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end57:
	.size	term_Bytes, .Lfunc_end57-term_Bytes
	.cfi_endproc

	.globl	term_ListOfVariables
	.p2align	4, 0x90
	.type	term_ListOfVariables,@function
term_ListOfVariables:                   # @term_ListOfVariables
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi296:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi297:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi298:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi299:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi300:
	.cfi_def_cfa_offset 48
.Lcfi301:
	.cfi_offset %rbx, -40
.Lcfi302:
	.cfi_offset %r12, -32
.Lcfi303:
	.cfi_offset %r14, -24
.Lcfi304:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	jmp	.LBB58_1
	.p2align	4, 0x90
.LBB58_10:                              # %.critedge.thread
                                        #   in Loop: Header=BB58_1 Depth=1
	movq	(%rax), %rcx
	movq	8(%rax), %r15
	movq	%rcx, 8(%rbx)
.LBB58_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB58_7 Depth 2
	movq	16(%r15), %r12
	testq	%r12, %r12
	je	.LBB58_3
# BB#2:                                 #   in Loop: Header=BB58_1 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%rbx, %rcx
	movq	%rax, %rbx
	jmp	.LBB58_5
	.p2align	4, 0x90
.LBB58_3:                               #   in Loop: Header=BB58_1 Depth=1
	cmpl	$0, (%r15)
	jle	.LBB58_6
# BB#4:                                 #   in Loop: Header=BB58_1 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r14, %rcx
	movq	%rax, %r14
.LBB58_5:                               # %.preheader.sink.split
                                        #   in Loop: Header=BB58_1 Depth=1
	movq	%rcx, (%rax)
.LBB58_6:                               # %.preheader
                                        #   in Loop: Header=BB58_1 Depth=1
	testq	%rbx, %rbx
	je	.LBB58_9
	.p2align	4, 0x90
.LBB58_7:                               # %.lr.ph
                                        #   Parent Loop BB58_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB58_10
# BB#8:                                 #   in Loop: Header=BB58_7 Depth=2
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB58_7
.LBB58_9:                               # %.critedge
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end58:
	.size	term_ListOfVariables, .Lfunc_end58-term_ListOfVariables
	.cfi_endproc

	.globl	term_MarkVariables
	.p2align	4, 0x90
	.type	term_MarkVariables,@function
term_MarkVariables:                     # @term_MarkVariables
	.cfi_startproc
# BB#0:
	movl	stack_POINTER(%rip), %eax
	movl	%esi, %r8d
	movl	%eax, %edx
	jmp	.LBB59_1
	.p2align	4, 0x90
.LBB59_10:                              #   in Loop: Header=BB59_1 Depth=1
	leal	-1(%rdx), %esi
	movq	stack_STACK(,%rsi,8), %rdi
	movq	(%rdi), %rcx
	movq	8(%rdi), %rdi
	movq	%rcx, stack_STACK(,%rsi,8)
.LBB59_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB59_6 Depth 2
	movq	16(%rdi), %rsi
	testq	%rsi, %rsi
	je	.LBB59_3
# BB#2:                                 #   in Loop: Header=BB59_1 Depth=1
	movl	%edx, %edi
	leal	1(%rdx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rsi, stack_STACK(,%rdi,8)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	cmpl	%eax, %edx
	jne	.LBB59_6
	jmp	.LBB59_8
	.p2align	4, 0x90
.LBB59_3:                               #   in Loop: Header=BB59_1 Depth=1
	movslq	(%rdi), %rsi
	testq	%rsi, %rsi
	jle	.LBB59_5
# BB#4:                                 #   in Loop: Header=BB59_1 Depth=1
	shlq	$4, %rsi
	movq	%r8, term_BIND(%rsi)
.LBB59_5:                               # %.preheader
                                        #   in Loop: Header=BB59_1 Depth=1
	cmpl	%eax, %edx
	je	.LBB59_8
	.p2align	4, 0x90
.LBB59_6:                               # %.lr.ph
                                        #   Parent Loop BB59_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rdx), %esi
	cmpq	$0, stack_STACK(,%rsi,8)
	jne	.LBB59_9
# BB#7:                                 #   in Loop: Header=BB59_6 Depth=2
	movl	%esi, stack_POINTER(%rip)
	cmpl	%esi, %eax
	movl	%esi, %edx
	jne	.LBB59_6
	jmp	.LBB59_8
	.p2align	4, 0x90
.LBB59_9:                               # %.critedge
                                        #   in Loop: Header=BB59_1 Depth=1
	cmpl	%edx, %eax
	jne	.LBB59_10
.LBB59_8:                               # %.critedge.thread
	retq
.Lfunc_end59:
	.size	term_MarkVariables, .Lfunc_end59-term_MarkVariables
	.cfi_endproc

	.globl	term_VariableSymbols
	.p2align	4, 0x90
	.type	term_VariableSymbols,@function
term_VariableSymbols:                   # @term_VariableSymbols
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi305:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi306:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi307:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi308:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi309:
	.cfi_def_cfa_offset 48
.Lcfi310:
	.cfi_offset %rbx, -48
.Lcfi311:
	.cfi_offset %r12, -40
.Lcfi312:
	.cfi_offset %r14, -32
.Lcfi313:
	.cfi_offset %r15, -24
.Lcfi314:
	.cfi_offset %rbp, -16
	movl	stack_POINTER(%rip), %ebp
	movl	term_MARK(%rip), %eax
	cmpl	$-1, %eax
	jne	.LBB60_4
# BB#1:                                 # %.preheader.i.preheader
	movq	$-48000, %rax           # imm = 0xFFFF4480
	jmp	.LBB60_2
	.p2align	4, 0x90
.LBB60_16:                              # %.preheader.i.1
                                        #   in Loop: Header=BB60_2 Depth=1
	movq	$0, term_BIND+48016(%rax)
	movq	$0, term_BIND+48032(%rax)
	movq	$0, term_BIND+48048(%rax)
	movq	$0, term_BIND+48064(%rax)
	movq	$0, term_BIND+48080(%rax)
	movq	$0, term_BIND+48096(%rax)
	movq	$0, term_BIND+48112(%rax)
	subq	$-128, %rax
.LBB60_2:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, term_BIND+48000(%rax)
	testq	%rax, %rax
	jne	.LBB60_16
# BB#3:
	movl	$1, term_MARK(%rip)
	movl	$1, %eax
.LBB60_4:                               # %term_ActMark.exit
	leal	1(%rax), %ecx
	movl	%ecx, term_MARK(%rip)
	movl	%eax, %r14d
	xorl	%r15d, %r15d
	movl	%ebp, %ecx
	jmp	.LBB60_5
	.p2align	4, 0x90
.LBB60_12:                              #   in Loop: Header=BB60_11 Depth=2
	movl	%eax, stack_POINTER(%rip)
	cmpl	%eax, %ebp
	movl	%eax, %ecx
	jne	.LBB60_11
	jmp	.LBB60_13
	.p2align	4, 0x90
.LBB60_14:                              # %.critedge
                                        #   in Loop: Header=BB60_5 Depth=1
	cmpl	%ecx, %ebp
	jne	.LBB60_15
	jmp	.LBB60_13
	.p2align	4, 0x90
.LBB60_7:                               #   in Loop: Header=BB60_5 Depth=1
	movslq	(%rdi), %rbx
	testq	%rbx, %rbx
	jle	.LBB60_10
# BB#8:                                 #   in Loop: Header=BB60_5 Depth=1
	movq	%rbx, %rax
	shlq	$4, %rax
	cmpl	%r14d, term_BIND(%rax)
	jae	.LBB60_10
# BB#9:                                 #   in Loop: Header=BB60_5 Depth=1
	leaq	term_BIND(%rax), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movq	%r14, (%r12)
	movl	stack_POINTER(%rip), %ecx
	movq	%rax, %r15
	cmpl	%ebp, %ecx
	jne	.LBB60_11
	jmp	.LBB60_13
	.p2align	4, 0x90
.LBB60_15:                              #   in Loop: Header=BB60_5 Depth=1
	leal	-1(%rcx), %eax
	movq	stack_STACK(,%rax,8), %rdx
	movq	(%rdx), %rsi
	movq	8(%rdx), %rdi
	movq	%rsi, stack_STACK(,%rax,8)
.LBB60_5:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB60_11 Depth 2
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB60_7
# BB#6:                                 #   in Loop: Header=BB60_5 Depth=1
	movl	%ecx, %edx
	leal	1(%rcx), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rdx,8)
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
.LBB60_10:                              # %.preheader
                                        #   in Loop: Header=BB60_5 Depth=1
	cmpl	%ebp, %ecx
	je	.LBB60_13
	.p2align	4, 0x90
.LBB60_11:                              # %.lr.ph
                                        #   Parent Loop BB60_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rcx), %eax
	cmpq	$0, stack_STACK(,%rax,8)
	je	.LBB60_12
	jmp	.LBB60_14
.LBB60_13:                              # %.critedge.thread
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end60:
	.size	term_VariableSymbols, .Lfunc_end60-term_VariableSymbols
	.cfi_endproc

	.globl	term_NumberOfVarOccs
	.p2align	4, 0x90
	.type	term_NumberOfVarOccs,@function
term_NumberOfVarOccs:                   # @term_NumberOfVarOccs
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	movl	stack_POINTER(%rip), %ecx
	movl	%ecx, %edx
	jmp	.LBB61_1
	.p2align	4, 0x90
.LBB61_9:                               #   in Loop: Header=BB61_1 Depth=1
	leal	-1(%rdx), %esi
	movq	stack_STACK(,%rsi,8), %rdi
	movq	(%rdi), %r8
	movq	8(%rdi), %rdi
	movq	%r8, stack_STACK(,%rsi,8)
.LBB61_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB61_5 Depth 2
	movq	16(%rdi), %rsi
	testq	%rsi, %rsi
	je	.LBB61_3
# BB#2:                                 #   in Loop: Header=BB61_1 Depth=1
	movl	%edx, %edi
	leal	1(%rdx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rsi, stack_STACK(,%rdi,8)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	cmpl	%ecx, %edx
	jne	.LBB61_5
	jmp	.LBB61_7
	.p2align	4, 0x90
.LBB61_3:                               #   in Loop: Header=BB61_1 Depth=1
	xorl	%esi, %esi
	cmpl	$0, (%rdi)
	setg	%sil
	addl	%esi, %eax
	cmpl	%ecx, %edx
	je	.LBB61_7
	.p2align	4, 0x90
.LBB61_5:                               # %.lr.ph
                                        #   Parent Loop BB61_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rdx), %esi
	cmpq	$0, stack_STACK(,%rsi,8)
	jne	.LBB61_8
# BB#6:                                 #   in Loop: Header=BB61_5 Depth=2
	movl	%esi, stack_POINTER(%rip)
	cmpl	%esi, %ecx
	movl	%esi, %edx
	jne	.LBB61_5
	jmp	.LBB61_7
	.p2align	4, 0x90
.LBB61_8:                               # %.critedge
                                        #   in Loop: Header=BB61_1 Depth=1
	cmpl	%edx, %ecx
	jne	.LBB61_9
.LBB61_7:                               # %.critedge.thread
	retq
.Lfunc_end61:
	.size	term_NumberOfVarOccs, .Lfunc_end61-term_NumberOfVarOccs
	.cfi_endproc

	.globl	term_NumberOfSymbolOccurrences
	.p2align	4, 0x90
	.type	term_NumberOfSymbolOccurrences,@function
term_NumberOfSymbolOccurrences:         # @term_NumberOfSymbolOccurrences
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi315:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi316:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi317:
	.cfi_def_cfa_offset 32
.Lcfi318:
	.cfi_offset %rbx, -32
.Lcfi319:
	.cfi_offset %r14, -24
.Lcfi320:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	xorl	%ebx, %ebx
	cmpl	%r14d, (%rdi)
	sete	%bl
	movq	16(%rdi), %rbp
	testq	%rbp, %rbp
	je	.LBB62_3
	.p2align	4, 0x90
.LBB62_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movl	%r14d, %esi
	callq	term_NumberOfSymbolOccurrences
	addl	%eax, %ebx
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB62_1
.LBB62_3:                               # %._crit_edge
	movl	%ebx, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end62:
	.size	term_NumberOfSymbolOccurrences, .Lfunc_end62-term_NumberOfSymbolOccurrences
	.cfi_endproc

	.globl	term_ContainsFunctions
	.p2align	4, 0x90
	.type	term_ContainsFunctions,@function
term_ContainsFunctions:                 # @term_ContainsFunctions
	.cfi_startproc
# BB#0:
	movl	stack_POINTER(%rip), %ecx
	movl	symbol_TYPEMASK(%rip), %r8d
	movl	%ecx, %esi
	jmp	.LBB63_1
	.p2align	4, 0x90
.LBB63_10:                              #   in Loop: Header=BB63_9 Depth=2
	movl	%edi, stack_POINTER(%rip)
	cmpl	%edi, %ecx
	movl	%edi, %esi
	jne	.LBB63_9
	jmp	.LBB63_6
	.p2align	4, 0x90
.LBB63_11:                              # %.critedge
                                        #   in Loop: Header=BB63_1 Depth=1
	cmpl	%esi, %ecx
	je	.LBB63_6
# BB#12:                                #   in Loop: Header=BB63_1 Depth=1
	leal	-1(%rsi), %eax
	movq	stack_STACK(,%rax,8), %rdx
	movq	(%rdx), %r9
	movq	8(%rdx), %rdi
	movq	%r9, stack_STACK(,%rax,8)
.LBB63_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB63_9 Depth 2
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB63_8
# BB#2:                                 #   in Loop: Header=BB63_1 Depth=1
	movl	(%rdi), %edx
	testl	%edx, %edx
	jns	.LBB63_7
# BB#3:                                 # %symbol_IsFunction.exit
                                        #   in Loop: Header=BB63_1 Depth=1
	negl	%edx
	andl	%r8d, %edx
	movl	%edx, %edi
	orl	$1, %edi
	cmpl	$1, %edi
	jne	.LBB63_7
# BB#4:                                 # %symbol_IsFunction.exit
                                        #   in Loop: Header=BB63_1 Depth=1
	testl	%edx, %edx
	jne	.LBB63_5
	.p2align	4, 0x90
.LBB63_7:                               # %symbol_IsFunction.exit.thread
                                        #   in Loop: Header=BB63_1 Depth=1
	movl	%esi, %edx
	leal	1(%rsi), %esi
	movl	%esi, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rdx,8)
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
.LBB63_8:                               # %.preheader
                                        #   in Loop: Header=BB63_1 Depth=1
	xorl	%eax, %eax
	cmpl	%ecx, %esi
	je	.LBB63_6
	.p2align	4, 0x90
.LBB63_9:                               # %.lr.ph
                                        #   Parent Loop BB63_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rsi), %edi
	cmpq	$0, stack_STACK(,%rdi,8)
	je	.LBB63_10
	jmp	.LBB63_11
.LBB63_5:                               # %symbol_IsConstant.exit.thread
	movl	%ecx, stack_POINTER(%rip)
	movl	$1, %eax
.LBB63_6:                               # %.loopexit
	retq
.Lfunc_end63:
	.size	term_ContainsFunctions, .Lfunc_end63-term_ContainsFunctions
	.cfi_endproc

	.globl	term_ContainsVariable
	.p2align	4, 0x90
	.type	term_ContainsVariable,@function
term_ContainsVariable:                  # @term_ContainsVariable
	.cfi_startproc
# BB#0:
	movl	stack_POINTER(%rip), %ecx
	movl	%ecx, %edx
	jmp	.LBB64_1
	.p2align	4, 0x90
.LBB64_6:                               #   in Loop: Header=BB64_5 Depth=2
	movl	%edi, stack_POINTER(%rip)
	cmpl	%edi, %ecx
	movl	%edi, %edx
	jne	.LBB64_5
	jmp	.LBB64_10
	.p2align	4, 0x90
.LBB64_7:                               # %.critedge
                                        #   in Loop: Header=BB64_1 Depth=1
	cmpl	%edx, %ecx
	je	.LBB64_10
# BB#8:                                 #   in Loop: Header=BB64_1 Depth=1
	leal	-1(%rdx), %eax
	movq	stack_STACK(,%rax,8), %rdi
	movq	(%rdi), %r8
	movq	8(%rdi), %rdi
	movq	%r8, stack_STACK(,%rax,8)
.LBB64_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB64_5 Depth 2
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB64_3
# BB#2:                                 #   in Loop: Header=BB64_1 Depth=1
	movl	%edx, %edi
	leal	1(%rdx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rdi,8)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	jmp	.LBB64_4
	.p2align	4, 0x90
.LBB64_3:                               #   in Loop: Header=BB64_1 Depth=1
	cmpl	%esi, (%rdi)
	je	.LBB64_9
.LBB64_4:                               # %.preheader
                                        #   in Loop: Header=BB64_1 Depth=1
	xorl	%eax, %eax
	cmpl	%ecx, %edx
	je	.LBB64_10
	.p2align	4, 0x90
.LBB64_5:                               # %.lr.ph
                                        #   Parent Loop BB64_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rdx), %edi
	cmpq	$0, stack_STACK(,%rdi,8)
	je	.LBB64_6
	jmp	.LBB64_7
.LBB64_9:
	movl	%ecx, stack_POINTER(%rip)
	movl	$1, %eax
.LBB64_10:                              # %.loopexit
	retq
.Lfunc_end64:
	.size	term_ContainsVariable, .Lfunc_end64-term_ContainsVariable
	.cfi_endproc

	.globl	term_MaxVar
	.p2align	4, 0x90
	.type	term_MaxVar,@function
term_MaxVar:                            # @term_MaxVar
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	leal	-1(%rax), %ecx
	cmpl	$2000, %ecx             # imm = 0x7D0
	jb	.LBB65_12
# BB#1:
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB65_2
# BB#3:                                 # %.critedge.preheader
	movl	stack_POINTER(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rcx,8)
	xorl	%eax, %eax
.LBB65_5:                               # %.lr.ph36
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB65_10 Depth 2
	leal	-1(%rdx), %edi
	movq	stack_STACK(,%rdi,8), %rsi
	movq	(%rsi), %r8
	movq	8(%rsi), %r9
	movq	%r8, stack_STACK(,%rdi,8)
	movl	(%r9), %edi
	leal	-1(%rdi), %esi
	cmpl	$1999, %esi             # imm = 0x7CF
	ja	.LBB65_7
# BB#6:                                 #   in Loop: Header=BB65_5 Depth=1
	cmpl	%eax, %edi
	cmovgel	%edi, %eax
	cmpl	%ecx, %edx
	jne	.LBB65_10
	jmp	.LBB65_12
	.p2align	4, 0x90
.LBB65_7:                               #   in Loop: Header=BB65_5 Depth=1
	movq	16(%r9), %rsi
	testq	%rsi, %rsi
	je	.LBB65_9
# BB#8:                                 #   in Loop: Header=BB65_5 Depth=1
	movl	%edx, %edi
	leal	1(%rdx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rsi, stack_STACK(,%rdi,8)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
.LBB65_9:                               # %.preheader
                                        #   in Loop: Header=BB65_5 Depth=1
	cmpl	%ecx, %edx
	je	.LBB65_12
	.p2align	4, 0x90
.LBB65_10:                              # %.lr.ph
                                        #   Parent Loop BB65_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rdx), %esi
	cmpq	$0, stack_STACK(,%rsi,8)
	jne	.LBB65_4
# BB#11:                                #   in Loop: Header=BB65_10 Depth=2
	movl	%esi, stack_POINTER(%rip)
	cmpl	%esi, %ecx
	movl	%esi, %edx
	jne	.LBB65_10
	jmp	.LBB65_12
	.p2align	4, 0x90
.LBB65_4:                               # %.critedge.loopexit
                                        #   in Loop: Header=BB65_5 Depth=1
	cmpl	%edx, %ecx
	jne	.LBB65_5
.LBB65_12:                              # %.critedge._crit_edge
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB65_2:
	xorl	%eax, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end65:
	.size	term_MaxVar, .Lfunc_end65-term_MaxVar
	.cfi_endproc

	.globl	term_StartMinRenaming
	.p2align	4, 0x90
	.type	term_StartMinRenaming,@function
term_StartMinRenaming:                  # @term_StartMinRenaming
	.cfi_startproc
# BB#0:
	movl	$0, symbol_STANDARDVARCOUNTER(%rip)
	movl	term_MARK(%rip), %eax
	cmpl	$-1, %eax
	jne	.LBB66_4
# BB#1:                                 # %.preheader.i.preheader
	movq	$-48000, %rax           # imm = 0xFFFF4480
	jmp	.LBB66_2
	.p2align	4, 0x90
.LBB66_5:                               # %.preheader.i.1
                                        #   in Loop: Header=BB66_2 Depth=1
	movq	$0, term_BIND+48016(%rax)
	movq	$0, term_BIND+48032(%rax)
	movq	$0, term_BIND+48048(%rax)
	movq	$0, term_BIND+48064(%rax)
	movq	$0, term_BIND+48080(%rax)
	movq	$0, term_BIND+48096(%rax)
	movq	$0, term_BIND+48112(%rax)
	subq	$-128, %rax
.LBB66_2:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, term_BIND+48000(%rax)
	testq	%rax, %rax
	jne	.LBB66_5
# BB#3:
	movl	$1, term_MARK(%rip)
	movl	$1, %eax
.LBB66_4:                               # %term_NewMark.exit
	incl	%eax
	movl	%eax, term_MARK(%rip)
	retq
.Lfunc_end66:
	.size	term_StartMinRenaming, .Lfunc_end66-term_StartMinRenaming
	.cfi_endproc

	.globl	term_StartMaxRenaming
	.p2align	4, 0x90
	.type	term_StartMaxRenaming,@function
term_StartMaxRenaming:                  # @term_StartMaxRenaming
	.cfi_startproc
# BB#0:
	movl	%edi, symbol_STANDARDVARCOUNTER(%rip)
	movl	term_MARK(%rip), %eax
	cmpl	$-1, %eax
	jne	.LBB67_4
# BB#1:                                 # %.preheader.i.preheader
	movq	$-48000, %rax           # imm = 0xFFFF4480
	jmp	.LBB67_2
	.p2align	4, 0x90
.LBB67_5:                               # %.preheader.i.1
                                        #   in Loop: Header=BB67_2 Depth=1
	movq	$0, term_BIND+48016(%rax)
	movq	$0, term_BIND+48032(%rax)
	movq	$0, term_BIND+48048(%rax)
	movq	$0, term_BIND+48064(%rax)
	movq	$0, term_BIND+48080(%rax)
	movq	$0, term_BIND+48096(%rax)
	movq	$0, term_BIND+48112(%rax)
	subq	$-128, %rax
.LBB67_2:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, term_BIND+48000(%rax)
	testq	%rax, %rax
	jne	.LBB67_5
# BB#3:
	movl	$1, term_MARK(%rip)
	movl	$1, %eax
.LBB67_4:                               # %term_NewMark.exit
	incl	%eax
	movl	%eax, term_MARK(%rip)
	retq
.Lfunc_end67:
	.size	term_StartMaxRenaming, .Lfunc_end67-term_StartMaxRenaming
	.cfi_endproc

	.globl	term_Rename
	.p2align	4, 0x90
	.type	term_Rename,@function
term_Rename:                            # @term_Rename
	.cfi_startproc
# BB#0:
	movl	stack_POINTER(%rip), %eax
	movl	term_MARK(%rip), %r8d
	decl	%r8d
	movl	%eax, %edx
	movq	%rdi, %rsi
	jmp	.LBB68_1
	.p2align	4, 0x90
.LBB68_10:                              #   in Loop: Header=BB68_9 Depth=2
	movl	%ecx, stack_POINTER(%rip)
	cmpl	%ecx, %eax
	movl	%ecx, %edx
	jne	.LBB68_9
	jmp	.LBB68_11
	.p2align	4, 0x90
.LBB68_12:                              # %.critedge
                                        #   in Loop: Header=BB68_1 Depth=1
	cmpl	%edx, %eax
	je	.LBB68_11
# BB#13:                                #   in Loop: Header=BB68_1 Depth=1
	leal	-1(%rdx), %ecx
	movq	stack_STACK(,%rcx,8), %rsi
	movq	(%rsi), %r9
	movq	8(%rsi), %rsi
	movq	%r9, stack_STACK(,%rcx,8)
.LBB68_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB68_9 Depth 2
	movq	16(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB68_3
# BB#2:                                 #   in Loop: Header=BB68_1 Depth=1
	movl	%edx, %esi
	leal	1(%rdx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rcx, stack_STACK(,%rsi,8)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	cmpl	%eax, %edx
	jne	.LBB68_9
	jmp	.LBB68_11
	.p2align	4, 0x90
.LBB68_3:                               #   in Loop: Header=BB68_1 Depth=1
	movslq	(%rsi), %rcx
	testq	%rcx, %rcx
	jle	.LBB68_8
# BB#4:                                 #   in Loop: Header=BB68_1 Depth=1
	shlq	$4, %rcx
	cmpl	%r8d, term_BIND(%rcx)
	jae	.LBB68_5
# BB#6:                                 #   in Loop: Header=BB68_1 Depth=1
	leaq	term_BIND(%rcx), %r9
	movslq	symbol_STANDARDVARCOUNTER(%rip), %rdx
	incq	%rdx
	movl	%edx, symbol_STANDARDVARCOUNTER(%rip)
	movq	%r8, (%r9)
	movq	%rdx, term_BIND+8(%rcx)
	jmp	.LBB68_7
.LBB68_5:                               # %._crit_edge
                                        #   in Loop: Header=BB68_1 Depth=1
	movq	term_BIND+8(%rcx), %rdx
.LBB68_7:                               #   in Loop: Header=BB68_1 Depth=1
	movl	%edx, (%rsi)
	movl	stack_POINTER(%rip), %edx
.LBB68_8:                               # %.preheader
                                        #   in Loop: Header=BB68_1 Depth=1
	cmpl	%eax, %edx
	je	.LBB68_11
	.p2align	4, 0x90
.LBB68_9:                               # %.lr.ph
                                        #   Parent Loop BB68_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rdx), %ecx
	cmpq	$0, stack_STACK(,%rcx,8)
	je	.LBB68_10
	jmp	.LBB68_12
.LBB68_11:                              # %.critedge.thread
	movq	%rdi, %rax
	retq
.Lfunc_end68:
	.size	term_Rename, .Lfunc_end68-term_Rename
	.cfi_endproc

	.globl	term_GetRenamedVarSymbol
	.p2align	4, 0x90
	.type	term_GetRenamedVarSymbol,@function
term_GetRenamedVarSymbol:               # @term_GetRenamedVarSymbol
	.cfi_startproc
# BB#0:
	movl	term_MARK(%rip), %ecx
	decl	%ecx
	movslq	%edi, %rax
	shlq	$4, %rax
	cmpl	%ecx, term_BIND(%rax)
	jb	.LBB69_2
# BB#1:
	movl	term_BIND+8(%rax), %edi
.LBB69_2:
	movl	%edi, %eax
	retq
.Lfunc_end69:
	.size	term_GetRenamedVarSymbol, .Lfunc_end69-term_GetRenamedVarSymbol
	.cfi_endproc

	.globl	term_RenamePseudoLinear
	.p2align	4, 0x90
	.type	term_RenamePseudoLinear,@function
term_RenamePseudoLinear:                # @term_RenamePseudoLinear
	.cfi_startproc
# BB#0:
	movl	%esi, symbol_STANDARDVARCOUNTER(%rip)
	movl	term_MARK(%rip), %edx
	cmpl	$-1, %edx
	je	.LBB70_1
# BB#4:                                 # %term_NewMark.exit
	incl	%edx
	movl	%edx, term_MARK(%rip)
	cmpl	$-1, %edx
	jne	.LBB70_9
# BB#5:                                 # %.preheader.i.preheader
	movq	$-48000, %rax           # imm = 0xFFFF4480
	jmp	.LBB70_6
	.p2align	4, 0x90
.LBB70_11:                              # %.preheader.i.1
                                        #   in Loop: Header=BB70_6 Depth=1
	movq	$0, term_BIND+48016(%rax)
	movq	$0, term_BIND+48032(%rax)
	movq	$0, term_BIND+48048(%rax)
	movq	$0, term_BIND+48064(%rax)
	movq	$0, term_BIND+48080(%rax)
	movq	$0, term_BIND+48096(%rax)
	movq	$0, term_BIND+48112(%rax)
	subq	$-128, %rax
.LBB70_6:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, term_BIND+48000(%rax)
	testq	%rax, %rax
	jne	.LBB70_11
# BB#7:
	movl	$1, %edx
	jmp	.LBB70_8
.LBB70_1:                               # %.preheader.i9.preheader
	movq	$-48000, %rax           # imm = 0xFFFF4480
	jmp	.LBB70_2
	.p2align	4, 0x90
.LBB70_10:                              # %.preheader.i9.1
                                        #   in Loop: Header=BB70_2 Depth=1
	movq	$0, term_BIND+48016(%rax)
	movq	$0, term_BIND+48032(%rax)
	movq	$0, term_BIND+48048(%rax)
	movq	$0, term_BIND+48064(%rax)
	movq	$0, term_BIND+48080(%rax)
	movq	$0, term_BIND+48096(%rax)
	movq	$0, term_BIND+48112(%rax)
	subq	$-128, %rax
.LBB70_2:                               # %.preheader.i9
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, term_BIND+48000(%rax)
	testq	%rax, %rax
	jne	.LBB70_10
# BB#3:                                 # %term_NewMark.exit.thread
	movl	$1, term_MARK(%rip)
	movl	$2, %edx
.LBB70_8:                               # %term_ActMark.exit.sink.split
	movl	%edx, term_MARK(%rip)
.LBB70_9:                               # %term_ActMark.exit
	leal	1(%rdx), %eax
	movl	%eax, term_MARK(%rip)
	xorl	%esi, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	jmp	term_MakePseudoLinear   # TAILCALL
.Lfunc_end70:
	.size	term_RenamePseudoLinear, .Lfunc_end70-term_RenamePseudoLinear
	.cfi_endproc

	.p2align	4, 0x90
	.type	term_MakePseudoLinear,@function
term_MakePseudoLinear:                  # @term_MakePseudoLinear
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi321:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi322:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi323:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi324:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi325:
	.cfi_def_cfa_offset 48
.Lcfi326:
	.cfi_offset %rbx, -40
.Lcfi327:
	.cfi_offset %r14, -32
.Lcfi328:
	.cfi_offset %r15, -24
.Lcfi329:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB71_9
# BB#1:                                 # %.lr.ph
	incl	%r15d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB71_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB71_6 Depth 2
	movq	8(%rbx), %rdi
	movl	%r15d, %esi
	movl	%r14d, %edx
	callq	term_MakePseudoLinear
	testq	%rax, %rax
	je	.LBB71_3
# BB#4:                                 #   in Loop: Header=BB71_2 Depth=1
	testq	%rbp, %rbp
	je	.LBB71_8
# BB#5:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB71_2 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB71_6:                               # %.preheader.i
                                        #   Parent Loop BB71_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB71_6
# BB#7:                                 #   in Loop: Header=BB71_2 Depth=1
	movq	%rbp, (%rcx)
	jmp	.LBB71_8
	.p2align	4, 0x90
.LBB71_3:                               #   in Loop: Header=BB71_2 Depth=1
	movq	%rbp, %rax
.LBB71_8:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB71_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %rbp
	jne	.LBB71_2
	jmp	.LBB71_16
.LBB71_9:
	movslq	(%rdi), %rbp
	testq	%rbp, %rbp
	jle	.LBB71_15
# BB#10:
	movq	%rbp, %rax
	shlq	$4, %rax
	cmpl	%r14d, term_BIND(%rax)
	jae	.LBB71_11
# BB#14:
	leaq	term_BIND(%rax), %rcx
	movl	%r15d, %edx
	movl	%r14d, %esi
	movq	%rsi, (%rcx)
	movq	%rdx, term_BIND+8(%rax)
.LBB71_15:                              # %.loopexit
	xorl	%eax, %eax
	jmp	.LBB71_16
.LBB71_11:
	cmpl	%r15d, term_BIND+8(%rax)
	movl	%ebp, %eax
	je	.LBB71_13
# BB#12:
	movl	symbol_STANDARDVARCOUNTER(%rip), %eax
	incl	%eax
	movl	%eax, symbol_STANDARDVARCOUNTER(%rip)
	movl	%eax, (%rdi)
.LBB71_13:
	movslq	%eax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	%r14, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
.LBB71_16:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end71:
	.size	term_MakePseudoLinear, .Lfunc_end71-term_MakePseudoLinear
	.cfi_endproc

	.globl	term_GetStampID
	.p2align	4, 0x90
	.type	term_GetStampID,@function
term_GetStampID:                        # @term_GetStampID
	.cfi_startproc
# BB#0:
	movl	term_STAMPUSERS(%rip), %eax
	cmpl	$20, %eax
	jae	.LBB72_2
# BB#1:
	leal	1(%rax), %ecx
	movl	%ecx, term_STAMPUSERS(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB72_2:
	pushq	%rax
.Lcfi330:
	.cfi_def_cfa_offset 16
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end72:
	.size	term_GetStampID, .Lfunc_end72-term_GetStampID
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI73_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	term_StampOverflow
	.p2align	4, 0x90
	.type	term_StampOverflow,@function
term_StampOverflow:                     # @term_StampOverflow
	.cfi_startproc
# BB#0:
	cmpl	$-1, term_STAMP(%rip)
	je	.LBB73_1
# BB#2:
	movl	%edi, %eax
	cmpl	$0, term_STAMPOVERFLOW(,%rax,4)
	jne	.LBB73_3
# BB#5:
	xorl	%eax, %eax
	retq
.LBB73_1:
	movl	$0, term_STAMP(%rip)
	movaps	.LCPI73_0(%rip), %xmm0  # xmm0 = [1,1,1,1]
	movaps	%xmm0, term_STAMPOVERFLOW(%rip)
	movaps	%xmm0, term_STAMPOVERFLOW+16(%rip)
	movaps	%xmm0, term_STAMPOVERFLOW+32(%rip)
	movaps	%xmm0, term_STAMPOVERFLOW+48(%rip)
	movaps	%xmm0, term_STAMPOVERFLOW+64(%rip)
	movl	%edi, %eax
.LBB73_3:
	leaq	term_STAMPOVERFLOW(,%rax,4), %rax
	movl	$0, (%rax)
	movl	$1, %eax
	retq
.Lfunc_end73:
	.size	term_StampOverflow, .Lfunc_end73-term_StampOverflow
	.cfi_endproc

	.globl	term_SetTermSubtermStamp
	.p2align	4, 0x90
	.type	term_SetTermSubtermStamp,@function
term_SetTermSubtermStamp:               # @term_SetTermSubtermStamp
	.cfi_startproc
# BB#0:
	movl	term_STAMP(%rip), %eax
	movl	%eax, 24(%rdi)
	movq	16(%rdi), %rsi
	movl	$term_SetTermSubtermStamp, %edi
	jmp	list_Apply              # TAILCALL
.Lfunc_end74:
	.size	term_SetTermSubtermStamp, .Lfunc_end74-term_SetTermSubtermStamp
	.cfi_endproc

	.globl	term_ListOfAtoms
	.p2align	4, 0x90
	.type	term_ListOfAtoms,@function
term_ListOfAtoms:                       # @term_ListOfAtoms
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi331:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi332:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi333:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi334:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi335:
	.cfi_def_cfa_offset 48
.Lcfi336:
	.cfi_offset %rbx, -40
.Lcfi337:
	.cfi_offset %r14, -32
.Lcfi338:
	.cfi_offset %r15, -24
.Lcfi339:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	cmpl	%r15d, (%r14)
	jne	.LBB75_3
# BB#1:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r14, 8(%rbx)
	movq	$0, (%rbx)
	jmp	.LBB75_2
.LBB75_3:
	movq	16(%r14), %rbp
	xorl	%ebx, %ebx
	testq	%rbp, %rbp
	jne	.LBB75_5
	jmp	.LBB75_2
	.p2align	4, 0x90
.LBB75_11:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB75_5 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB75_2
.LBB75_5:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB75_9 Depth 2
	movq	8(%rbp), %rdi
	movl	%r15d, %esi
	callq	term_ListOfAtoms
	testq	%rbx, %rbx
	je	.LBB75_6
# BB#7:                                 #   in Loop: Header=BB75_5 Depth=1
	testq	%rax, %rax
	je	.LBB75_11
# BB#8:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB75_5 Depth=1
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB75_9:                               # %.preheader.i
                                        #   Parent Loop BB75_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB75_9
# BB#10:                                #   in Loop: Header=BB75_5 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB75_11
	.p2align	4, 0x90
.LBB75_6:                               #   in Loop: Header=BB75_5 Depth=1
	movq	%rax, %rbx
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB75_5
.LBB75_2:                               # %.loopexit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end75:
	.size	term_ListOfAtoms, .Lfunc_end75-term_ListOfAtoms
	.cfi_endproc

	.globl	term_FindAllAtoms
	.p2align	4, 0x90
	.type	term_FindAllAtoms,@function
term_FindAllAtoms:                      # @term_FindAllAtoms
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi340:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi341:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi342:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi343:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi344:
	.cfi_def_cfa_offset 48
.Lcfi345:
	.cfi_offset %rbx, -40
.Lcfi346:
	.cfi_offset %r14, -32
.Lcfi347:
	.cfi_offset %r15, -24
.Lcfi348:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbp
	movl	stack_POINTER(%rip), %ebx
	xorl	%r15d, %r15d
	movl	%ebx, %ecx
	cmpl	%r14d, (%rbp)
	jne	.LBB76_3
	jmp	.LBB76_2
	.p2align	4, 0x90
.LBB76_10:
	leal	-1(%rcx), %eax
	movq	stack_STACK(,%rax,8), %rdx
	movq	(%rdx), %rsi
	movq	8(%rdx), %rbp
	movq	%rsi, stack_STACK(,%rax,8)
	cmpl	%r14d, (%rbp)
	jne	.LBB76_3
.LBB76_2:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r15, (%rax)
	movl	stack_POINTER(%rip), %ecx
	movq	%rax, %r15
	cmpl	%ebx, %ecx
	jne	.LBB76_6
	jmp	.LBB76_8
	.p2align	4, 0x90
.LBB76_3:
	movq	16(%rbp), %rax
	testq	%rax, %rax
	je	.LBB76_5
# BB#4:
	movl	%ecx, %edx
	leal	1(%rcx), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rdx,8)
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
.LBB76_5:                               # %.preheader
	cmpl	%ebx, %ecx
	je	.LBB76_8
	.p2align	4, 0x90
.LBB76_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rcx), %eax
	cmpq	$0, stack_STACK(,%rax,8)
	jne	.LBB76_9
# BB#7:                                 #   in Loop: Header=BB76_6 Depth=1
	movl	%eax, stack_POINTER(%rip)
	cmpl	%eax, %ebx
	movl	%eax, %ecx
	jne	.LBB76_6
	jmp	.LBB76_8
	.p2align	4, 0x90
.LBB76_9:                               # %.critedge
	cmpl	%ecx, %ebx
	jne	.LBB76_10
.LBB76_8:                               # %.critedge.thread
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end76:
	.size	term_FindAllAtoms, .Lfunc_end76-term_FindAllAtoms
	.cfi_endproc

	.globl	term_CheckTermIntern
	.p2align	4, 0x90
	.type	term_CheckTermIntern,@function
term_CheckTermIntern:                   # @term_CheckTermIntern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi349:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi350:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi351:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi352:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi353:
	.cfi_def_cfa_offset 48
.Lcfi354:
	.cfi_offset %rbx, -40
.Lcfi355:
	.cfi_offset %r14, -32
.Lcfi356:
	.cfi_offset %r15, -24
.Lcfi357:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB77_23
# BB#1:                                 # %term_IsTerm.exit
	movl	(%r15), %edi
	callq	symbol_IsSymbol
	testl	%eax, %eax
	je	.LBB77_23
# BB#2:
	movl	(%r15), %eax
	testl	%eax, %eax
	js	.LBB77_8
# BB#3:
	je	.LBB77_10
# BB#4:
	xorl	%ebp, %ebp
	cmpq	$0, 16(%r15)
	jne	.LBB77_24
# BB#5:
	xorl	%ebx, %ebx
	testl	%r14d, %r14d
	jne	.LBB77_11
	jmp	.LBB77_17
.LBB77_8:
	negl	%eax
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movl	16(%rax), %ebx
	cmpl	$-1, %ebx
	je	.LBB77_10
# BB#9:
	movq	16(%r15), %rdi
	callq	list_Length
	cmpl	%eax, %ebx
	jne	.LBB77_23
.LBB77_10:                              # %._crit_edge
	movq	16(%r15), %rbx
	testl	%r14d, %r14d
	je	.LBB77_17
.LBB77_11:                              # %.preheader34
	movl	$1, %ebp
	testq	%rbx, %rbx
	je	.LBB77_24
	.p2align	4, 0x90
.LBB77_13:                              # %.lr.ph43
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	cmpq	%r15, 8(%rdi)
	jne	.LBB77_23
# BB#14:                                #   in Loop: Header=BB77_13 Depth=1
	movl	%r14d, %esi
	callq	term_CheckTermIntern
	testl	%eax, %eax
	je	.LBB77_23
# BB#12:                                #   in Loop: Header=BB77_13 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB77_13
	jmp	.LBB77_24
	.p2align	4, 0x90
.LBB77_16:                              #   in Loop: Header=BB77_17 Depth=1
	movq	(%rbx), %rbx
.LBB77_17:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	je	.LBB77_20
# BB#18:                                # %.lr.ph
                                        #   in Loop: Header=BB77_17 Depth=1
	movq	8(%rbx), %rdi
	cmpq	$0, 8(%rdi)
	jne	.LBB77_23
# BB#19:                                #   in Loop: Header=BB77_17 Depth=1
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	callq	term_CheckTermIntern
	testl	%eax, %eax
	jne	.LBB77_16
	jmp	.LBB77_24
.LBB77_23:
	xorl	%ebp, %ebp
.LBB77_24:                              # %term_IsTerm.exit.thread
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB77_20:
	movl	$1, %ebp
	jmp	.LBB77_24
.Lfunc_end77:
	.size	term_CheckTermIntern, .Lfunc_end77-term_CheckTermIntern
	.cfi_endproc

	.globl	term_CheckTerm
	.p2align	4, 0x90
	.type	term_CheckTerm,@function
term_CheckTerm:                         # @term_CheckTerm
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB78_1
# BB#2:
	movq	8(%rax), %rax
	xorl	%esi, %esi
	cmpq	$0, 8(%rax)
	setne	%sil
	jmp	term_CheckTermIntern    # TAILCALL
.LBB78_1:
	xorl	%esi, %esi
	jmp	term_CheckTermIntern    # TAILCALL
.Lfunc_end78:
	.size	term_CheckTerm, .Lfunc_end78-term_CheckTerm
	.cfi_endproc

	.type	term_MARK,@object       # @term_MARK
	.comm	term_MARK,4,4
	.type	term_STAMP,@object      # @term_STAMP
	.comm	term_STAMP,4,4
	.type	term_STAMPBLOCKED,@object # @term_STAMPBLOCKED
	.comm	term_STAMPBLOCKED,4,4
	.type	term_STAMPOVERFLOW,@object # @term_STAMPOVERFLOW
	.local	term_STAMPOVERFLOW
	.comm	term_STAMPOVERFLOW,80,16
	.type	term_STAMPUSERS,@object # @term_STAMPUSERS
	.local	term_STAMPUSERS
	.comm	term_STAMPUSERS,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"(NULL)"
	.size	.L.str, 7

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%u"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str.2, 31

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/SPASS/term.c"
	.size	.L.str.3, 76

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n In term_FPrintPosition: Term isn't subterm of the other one."
	.size	.L.str.4, 63

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.5, 133

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n In term_GetStampID: no more free stamp IDs."
	.size	.L.str.6, 46

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\n You have to increase the constant term_MAXSTAMPUSERS."
	.size	.L.str.7, 56

	.type	term_BIND,@object       # @term_BIND
	.comm	term_BIND,48016,16
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"  "
	.size	.L.str.8, 3

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	",\n"
	.size	.L.str.10, 3

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\n\n"
	.size	.L.str.11, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
