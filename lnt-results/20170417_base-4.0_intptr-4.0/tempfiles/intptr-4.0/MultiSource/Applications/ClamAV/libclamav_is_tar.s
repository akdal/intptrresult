	.text
	.file	"libclamav_is_tar.bc"
	.globl	is_tar
	.p2align	4, 0x90
	.type	is_tar,@function
is_tar:                                 # @is_tar
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpl	$512, %esi              # imm = 0x200
	jb	.LBB0_28
# BB#1:
	callq	__ctype_b_loc
	movq	(%rax), %r9
	movzbl	148(%rbx), %r8d
	testb	$32, 1(%r9,%r8,2)
	jne	.LBB0_3
# BB#2:
	leaq	148(%rbx), %rsi
	movl	$8, %edi
	movb	%r8b, %dl
	jmp	.LBB0_5
.LBB0_3:
	movzbl	149(%rbx), %edx
	testb	$32, 1(%r9,%rdx,2)
	jne	.LBB0_16
# BB#4:
	leaq	149(%rbx), %rsi
	movl	$7, %edi
	jmp	.LBB0_5
.LBB0_16:
	movzbl	150(%rbx), %edx
	testb	$32, 1(%r9,%rdx,2)
	jne	.LBB0_18
# BB#17:
	leaq	150(%rbx), %rsi
	movl	$6, %edi
	jmp	.LBB0_5
.LBB0_18:
	movzbl	151(%rbx), %edx
	testb	$32, 1(%r9,%rdx,2)
	jne	.LBB0_20
# BB#19:
	leaq	151(%rbx), %rsi
	movl	$5, %edi
	jmp	.LBB0_5
.LBB0_20:
	movzbl	152(%rbx), %edx
	testb	$32, 1(%r9,%rdx,2)
	jne	.LBB0_22
# BB#21:
	leaq	152(%rbx), %rsi
	movl	$4, %edi
	jmp	.LBB0_5
.LBB0_22:
	movzbl	153(%rbx), %edx
	testb	$32, 1(%r9,%rdx,2)
	jne	.LBB0_24
# BB#23:
	leaq	153(%rbx), %rsi
	movl	$3, %edi
	jmp	.LBB0_5
.LBB0_24:
	movzbl	154(%rbx), %edx
	testb	$32, 1(%r9,%rdx,2)
	jne	.LBB0_26
# BB#25:
	leaq	154(%rbx), %rsi
	movl	$2, %edi
	jmp	.LBB0_5
.LBB0_26:
	movzbl	155(%rbx), %edx
	movl	$-1, %esi
	testb	$32, 1(%r9,%rdx,2)
	jne	.LBB0_12
# BB#27:
	leaq	155(%rbx), %rsi
	movl	$1, %edi
.LBB0_5:                                # %.lr.ph.i.preheader
	movl	%edx, %ecx
	andb	$-8, %cl
	xorl	%eax, %eax
	cmpb	$48, %cl
	jne	.LBB0_9
# BB#6:                                 # %.lr.ph.preheader
	incq	%rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ecx
	movsbl	%dl, %eax
	shll	$3, %ecx
	addl	$-48, %eax
	orl	%ecx, %eax
	cmpl	$2, %edi
	jl	.LBB0_11
# BB#8:                                 # %..lr.ph_crit_edge.i
                                        #   in Loop: Header=BB0_7 Depth=1
	decl	%edi
	movb	(%rsi), %dl
	movl	%edx, %ecx
	andb	$-8, %cl
	incq	%rsi
	cmpb	$48, %cl
	je	.LBB0_7
.LBB0_9:                                # %.critedge.i
	testb	%dl, %dl
	je	.LBB0_11
# BB#10:
	movzbl	%dl, %ecx
	movl	$-1, %esi
	testb	$32, 1(%r9,%rcx,2)
	je	.LBB0_12
.LBB0_11:                               # %.critedge1.i
	movl	%eax, %esi
.LBB0_12:                               # %vector.ph
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	pxor	%xmm2, %xmm2
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB0_13:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	(%rbx,%rax), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movd	4(%rbx,%rax), %xmm4     # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	punpcklbw	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3],xmm4[4],xmm0[4],xmm4[5],xmm0[5],xmm4[6],xmm0[6],xmm4[7],xmm0[7]
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	paddd	%xmm2, %xmm3
	paddd	%xmm1, %xmm4
	movd	8(%rbx,%rax), %xmm2     # xmm2 = mem[0],zero,zero,zero
	movd	12(%rbx,%rax), %xmm1    # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	paddd	%xmm3, %xmm2
	paddd	%xmm4, %xmm1
	addq	$16, %rax
	cmpq	$512, %rax              # imm = 0x200
	jne	.LBB0_13
# BB#14:                                # %middle.block
	paddd	%xmm2, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %eax
	movzbl	155(%rbx), %ecx
	subl	%ecx, %eax
	movzbl	154(%rbx), %ecx
	subl	%ecx, %eax
	movzbl	153(%rbx), %ecx
	subl	%ecx, %eax
	movzbl	152(%rbx), %ecx
	subl	%ecx, %eax
	movzbl	151(%rbx), %ecx
	subl	%ecx, %eax
	movzbl	150(%rbx), %ecx
	subl	%ecx, %eax
	movzbl	149(%rbx), %ecx
	subl	%ecx, %eax
	subl	%r8d, %eax
	addl	$256, %eax              # imm = 0x100
	cmpl	%esi, %eax
	jne	.LBB0_28
# BB#15:
	addq	$257, %rbx              # imm = 0x101
	movl	$.L.str, %esi
	movq	%rbx, %rdi
	callq	strcmp
	cmpl	$1, %eax
	movl	$1, %eax
	adcl	$0, %eax
	popq	%rbx
	retq
.LBB0_28:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	is_tar, .Lfunc_end0-is_tar
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ustar  "
	.size	.L.str, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
