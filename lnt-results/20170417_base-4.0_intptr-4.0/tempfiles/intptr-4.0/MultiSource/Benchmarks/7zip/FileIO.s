	.text
	.file	"FileIO.bc"
	.globl	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev,@function
_ZN8NWindows5NFile3NIO9CFileBaseD2Ev:   # @_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NWindows5NFile3NIO9CFileBaseE+16, (%rbx)
.Ltmp0:
	callq	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
.Ltmp1:
# BB#1:
	movq	16(%rbx), %rdi
	addq	$8, %rsp
	testq	%rdi, %rdi
	je	.LBB0_2
# BB#6:
	popq	%rbx
	popq	%r14
	jmp	_ZdaPv                  # TAILCALL
.LBB0_2:                                # %_ZN11CStringBaseIcED2Ev.exit
	popq	%rbx
	popq	%r14
	retq
.LBB0_3:
.Ltmp2:
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_5
# BB#4:
	callq	_ZdaPv
.LBB0_5:                                # %_ZN11CStringBaseIcED2Ev.exit2
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev, .Lfunc_end0-_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end0-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile3NIO9CFileBaseD0Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO9CFileBaseD0Ev,@function
_ZN8NWindows5NFile3NIO9CFileBaseD0Ev:   # @_ZN8NWindows5NFile3NIO9CFileBaseD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NWindows5NFile3NIO9CFileBaseE+16, (%rbx)
.Ltmp3:
	callq	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
.Ltmp4:
# BB#1:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_3
# BB#2:
	callq	_ZdaPv
.LBB1_3:                                # %_ZN8NWindows5NFile3NIO9CFileBaseD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB1_4:
.Ltmp5:
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_6
# BB#5:
	callq	_ZdaPv
.LBB1_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZN8NWindows5NFile3NIO9CFileBaseD0Ev, .Lfunc_end1-_ZN8NWindows5NFile3NIO9CFileBaseD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Lfunc_end1-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKcjjjjb
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKcjjjjb,@function
_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKcjjjjb: # @_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKcjjjjb
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 112
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movl	%edx, %r13d
	movq	%rsi, %r14
	movq	(%rdi), %rax
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	callq	*16(%rax)
	cmpb	$99, (%r14)
	jne	.LBB2_2
# BB#1:
	leaq	2(%r14), %rax
	cmpb	$58, 1(%r14)
	cmoveq	%rax, %r14
.LBB2_2:                                # %_ZL16nameWindowToUnixPKc.exit
	xorl	%edi, %edi
	callq	umask
	movl	%eax, %r12d
	movl	%r12d, %edi
	callq	umask
	movl	%r13d, %ebx
	andl	$1073741824, %ebx       # imm = 0x40000000
	movl	%ebx, %r15d
	shrl	$30, %r15d
	cmpl	$4, %ebp
	je	.LBB2_6
# BB#3:                                 # %_ZL16nameWindowToUnixPKc.exit
	cmpl	$2, %ebp
	je	.LBB2_6
# BB#4:                                 # %_ZL16nameWindowToUnixPKc.exit
	cmpl	$1, %ebp
	jne	.LBB2_7
# BB#5:
	orl	$192, %r15d
	jmp	.LBB2_7
.LBB2_6:
	orl	$64, %r15d
.LBB2_7:
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	$-1, 8(%rbp)
	cmpl	$0, global_use_lstat(%rip)
	je	.LBB2_16
# BB#8:
	movb	112(%rsp), %al
	testb	%al, %al
	jne	.LBB2_16
# BB#9:
	leaq	52(%rbp), %rsi
	movl	$1024, %edx             # imm = 0x400
	movq	%r14, %rdi
	callq	readlink
	movl	%eax, 48(%rbp)
	testl	%eax, %eax
	jle	.LBB2_15
# BB#10:
	testl	%r13d, %r13d
	js	.LBB2_14
# BB#11:
	testl	%ebx, %ebx
	je	.LBB2_15
# BB#12:
	movq	%r14, %rdi
	callq	unlink
	testl	%eax, %eax
	jne	.LBB2_15
# BB#13:
	xorl	%eax, %eax
	jmp	.LBB2_63
.LBB2_14:
	movl	$-2, 8(%rbp)
	movl	$0, 1080(%rbp)
	cltq
	movb	$0, 52(%rbp,%rax)
.LBB2_15:
	cmpl	$-1, 8(%rbp)
	jne	.LBB2_40
.LBB2_16:
	andl	$54, %r12d
	xorl	$438, %r12d             # imm = 0x1B6
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%r15d, %esi
	movl	%r12d, %edx
	callq	open64
	movl	%eax, 8(%rbp)
	cmpl	$-1, %eax
	jne	.LBB2_39
# BB#17:
	movl	global_use_utf16_conversion(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB2_39
# BB#18:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$-1, %ebx
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB2_19:                               # =>This Inner Loop Header: Depth=1
	incl	%ebx
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB2_19
# BB#20:                                # %_Z11MyStringLenIcEiPKT_.exit.i62
	leal	1(%rbx), %eax
	movslq	%eax, %rbp
	cmpl	$-1, %ebx
	movq	$-1, %rdi
	cmovgeq	%rbp, %rdi
	callq	_Znam
	movq	%rax, 16(%rsp)
	movb	$0, (%rax)
	movl	%ebp, 28(%rsp)
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB2_21:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i65
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB2_21
# BB#22:                                # %_ZN11CStringBaseIcEC2EPKc.exit
	movl	%ebx, 24(%rsp)
.Ltmp6:
	leaq	40(%rsp), %rdi
	leaq	16(%rsp), %rsi
	xorl	%edx, %edx
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp7:
# BB#23:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB2_25
# BB#24:
	callq	_ZdaPv
.LBB2_25:                               # %_ZN11CStringBaseIcED2Ev.exit
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
.Ltmp9:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %rcx
.Ltmp10:
# BB#26:                                # %_ZN11CStringBaseIcEC2Ev.exit
	movq	%rcx, 16(%rsp)
	movb	$0, (%rcx)
	movl	$4, 28(%rsp)
	cmpl	$0, 48(%rsp)
	jle	.LBB2_32
# BB#27:                                # %.lr.ph
	xorl	%ebx, %ebx
	leaq	16(%rsp), %rbp
	.p2align	4, 0x90
.LBB2_28:                               # =>This Inner Loop Header: Depth=1
	movq	40(%rsp), %rax
	movl	(%rax,%rbx,4), %eax
	cmpl	$255, %eax
	jg	.LBB2_34
# BB#29:                                #   in Loop: Header=BB2_28 Depth=1
.Ltmp12:
	movsbl	%al, %esi
	movq	%rbp, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp13:
# BB#30:                                #   in Loop: Header=BB2_28 Depth=1
	incq	%rbx
	movslq	48(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB2_28
# BB#31:                                # %.critedge.loopexit
	movq	16(%rsp), %rcx
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB2_32:                               # %.critedge
.Ltmp15:
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r15d, %esi
	movl	%r12d, %edx
	callq	open64
.Ltmp16:
# BB#33:
	movl	%eax, 8(%rbx)
.LBB2_34:                               # %.loopexit
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_36
# BB#35:
	callq	_ZdaPv
.LBB2_36:                               # %_ZN11CStringBaseIcED2Ev.exit70
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB2_38
# BB#37:
	callq	_ZdaPv
.LBB2_38:                               # %_ZN11CStringBaseIwED2Ev.exit69
	movl	8(%rbp), %eax
.LBB2_39:
	cmpl	$-1, %eax
	je	.LBB2_57
.LBB2_40:                               # %.thread
	movl	$0, 24(%rbp)
	movq	16(%rbp), %rax
	movb	$0, (%rax)
	xorl	%ebx, %ebx
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB2_41:                               # =>This Inner Loop Header: Depth=1
	incl	%ebx
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB2_41
# BB#42:                                # %_Z11MyStringLenIcEiPKT_.exit.i
	leal	-1(%rbx), %r15d
	movl	28(%rbp), %r12d
	cmpl	%ebx, %r12d
	jne	.LBB2_44
# BB#43:                                # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	16(%rbp), %rbp
	jmp	.LBB2_61
.LBB2_44:
	movslq	%ebx, %rax
	cmpl	$-1, %r15d
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
	testl	%r12d, %r12d
	jle	.LBB2_60
# BB#45:                                # %.preheader.i.i
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	24(%rax), %r9
	testq	%r9, %r9
	movq	16(%rax), %rdi
	jle	.LBB2_58
# BB#46:                                # %.lr.ph.preheader.i.i
	cmpl	$31, %r9d
	jbe	.LBB2_50
# BB#47:                                # %min.iters.checked
	movq	%r9, %rcx
	andq	$-32, %rcx
	je	.LBB2_50
# BB#48:                                # %vector.memcheck
	leaq	(%rdi,%r9), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB2_64
# BB#49:                                # %vector.memcheck
	leaq	(%rbp,%r9), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB2_64
.LBB2_50:
	xorl	%ecx, %ecx
.LBB2_51:                               # %.lr.ph.i.i.preheader
	movl	%r9d, %esi
	subl	%ecx, %esi
	leaq	-1(%r9), %r8
	subq	%rcx, %r8
	andq	$7, %rsi
	je	.LBB2_54
# BB#52:                                # %.lr.ph.i.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB2_53:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB2_53
.LBB2_54:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB2_59
# BB#55:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %r9
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB2_56:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r9
	jne	.LBB2_56
	jmp	.LBB2_59
.LBB2_57:
	xorl	%eax, %eax
	jmp	.LBB2_63
.LBB2_58:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB2_60
.LBB2_59:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
.LBB2_60:                               # %._crit_edge17.i.i
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rbp, 16(%rcx)
	movslq	24(%rcx), %rax
	movb	$0, (%rbp,%rax)
	movl	%ebx, 28(%rcx)
	.p2align	4, 0x90
.LBB2_61:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14), %eax
	incq	%r14
	movb	%al, (%rbp)
	incq	%rbp
	testb	%al, %al
	jne	.LBB2_61
# BB#62:                                # %_ZN11CStringBaseIcEaSEPKc.exit
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%r15d, 24(%rax)
	movb	$1, %al
.LBB2_63:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_64:                               # %vector.body.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_67
# BB#65:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_66:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbp,%rdx)
	movups	%xmm1, 16(%rbp,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB2_66
	jmp	.LBB2_68
.LBB2_67:
	xorl	%edx, %edx
.LBB2_68:                               # %vector.body.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB2_71
# BB#69:                                # %vector.body.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbp,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB2_70:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB2_70
.LBB2_71:                               # %middle.block
	cmpq	%rcx, %r9
	jne	.LBB2_51
	jmp	.LBB2_59
.LBB2_72:
.Ltmp17:
	jmp	.LBB2_76
.LBB2_73:
.Ltmp11:
	movq	%rax, %rbx
	jmp	.LBB2_78
.LBB2_74:
.Ltmp8:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB2_79
	jmp	.LBB2_80
.LBB2_75:
.Ltmp14:
.LBB2_76:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_78
# BB#77:
	callq	_ZdaPv
.LBB2_78:
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_80
.LBB2_79:
	callq	_ZdaPv
.LBB2_80:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKcjjjjb, .Lfunc_end2-_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKcjjjjb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\320"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp6-.Lfunc_begin2    #   Call between .Lfunc_begin2 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin2    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin2    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin2    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin2   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin2   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Lfunc_end2-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIcEpLEc,"axG",@progbits,_ZN11CStringBaseIcEpLEc,comdat
	.weak	_ZN11CStringBaseIcEpLEc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEc,@function
_ZN11CStringBaseIcEpLEc:                # @_ZN11CStringBaseIcEpLEc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 64
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB3_28
# BB#1:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB3_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB3_3:                                # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rsi,%rbx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB3_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB3_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB3_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB3_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB3_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB3_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB3_17
.LBB3_7:
	xorl	%ecx, %ecx
.LBB3_8:                                # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB3_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB3_10:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB3_10
.LBB3_11:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB3_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB3_13:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB3_13
	jmp	.LBB3_26
.LBB3_25:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB3_27
.LBB3_26:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB3_27:
	movq	%r12, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%r13)
.LBB3_28:                               # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	movb	%r14b, (%rax,%rcx)
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movb	$0, 1(%rax,%rcx)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_17:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB3_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r12,%rbx)
	movups	%xmm1, 16(%r12,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB3_20
	jmp	.LBB3_21
.LBB3_18:
	xorl	%ebx, %ebx
.LBB3_21:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB3_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB3_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB3_23
.LBB3_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB3_8
	jmp	.LBB3_26
.Lfunc_end3:
	.size	_ZN11CStringBaseIcEpLEc, .Lfunc_end3-_ZN11CStringBaseIcEpLEc
	.cfi_endproc

	.text
	.globl	_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKwjjjjb
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKwjjjjb,@function
_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKwjjjjb: # @_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKwjjjjb
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 112
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movl	%edx, %r15d
	movq	%rsi, %rbp
	movq	%rdi, %r12
	movq	(%r12), %rax
	callq	*16(%rax)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$-1, %r13d
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	incl	%r13d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB4_1
# BB#2:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%r13), %eax
	movslq	%eax, %rbx
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, 16(%rsp)
	movl	$0, (%rax)
	movl	%ebx, 28(%rsp)
	.p2align	4, 0x90
.LBB4_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %ecx
	addq	$4, %rbp
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB4_3
# BB#4:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%r13d, 24(%rsp)
.Ltmp18:
	leaq	40(%rsp), %rdi
	leaq	16(%rsp), %rsi
	xorl	%edx, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp19:
# BB#5:
	movq	40(%rsp), %rsi
.Ltmp21:
	movzbl	112(%rsp), %eax
	movl	%eax, (%rsp)
	movq	%r12, %rdi
	movl	%r15d, %edx
	movl	%r14d, %r8d
	callq	_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKcjjjjb
	movl	%eax, %ebx
.Ltmp22:
# BB#6:
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_8
# BB#7:
	callq	_ZdaPv
.LBB4_8:                                # %_ZN11CStringBaseIcED2Ev.exit
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_10
# BB#9:
	callq	_ZdaPv
.LBB4_10:                               # %_ZN11CStringBaseIwED2Ev.exit9
	movl	%ebx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_12:
.Ltmp23:
	movq	%rax, %rbx
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_14
# BB#13:
	callq	_ZdaPv
	jmp	.LBB4_14
.LBB4_11:
.Ltmp20:
	movq	%rax, %rbx
.LBB4_14:                               # %_ZN11CStringBaseIcED2Ev.exit10
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_16
# BB#15:
	callq	_ZdaPv
.LBB4_16:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKwjjjjb, .Lfunc_end4-_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKwjjjjb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp18-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin3   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin3   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end4-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv,@function
_ZN8NWindows5NFile3NIO9CFileBase5CloseEv: # @_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 40
	subq	$168, %rsp
.Lcfi53:
	.cfi_def_cfa_offset 208
.Lcfi54:
	.cfi_offset %rbx, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rbp
	movq	%rbp, 8(%rsp)
	movq	40(%rbx), %r15
	movq	%r15, 16(%rsp)
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 32(%rbx)
	movl	8(%rbx), %edi
	movb	$1, %r14b
	cmpl	$-1, %edi
	je	.LBB5_16
# BB#1:
	cmpl	$-2, %edi
	jne	.LBB5_3
# BB#2:
	movl	$-1, 8(%rbx)
	jmp	.LBB5_16
.LBB5_3:
	callq	close
	testl	%eax, %eax
	je	.LBB5_5
# BB#4:
	xorl	%r14d, %r14d
	jmp	.LBB5_16
.LBB5_5:
	movl	$-1, 8(%rbx)
	movq	%r15, %rax
	andq	%rbp, %rax
	cmpq	$-1, %rax
	je	.LBB5_16
# BB#6:
	movq	16(%rbx), %rsi
	leaq	24(%rsp), %rdx
	movl	$1, %edi
	callq	__xstat64
	testl	%eax, %eax
	je	.LBB5_7
# BB#11:
	xorl	%edi, %edi
	callq	time
	cmpq	$-1, %rbp
	jne	.LBB5_13
# BB#12:
	movq	%rax, 8(%rsp)
.LBB5_13:
	cmpq	$-1, %r15
	jne	.LBB5_15
	jmp	.LBB5_14
.LBB5_7:
	cmpq	$-1, %rbp
	jne	.LBB5_9
# BB#8:
	movq	96(%rsp), %rax
	movq	%rax, 8(%rsp)
.LBB5_9:
	cmpq	$-1, %r15
	jne	.LBB5_15
# BB#10:
	movq	112(%rsp), %rax
.LBB5_14:
	movq	%rax, 16(%rsp)
.LBB5_15:
	movq	16(%rbx), %rdi
	leaq	8(%rsp), %rsi
	callq	utime
.LBB5_16:
	movl	%r14d, %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv, .Lfunc_end5-_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.cfi_endproc

	.globl	_ZNK8NWindows5NFile3NIO9CFileBase9GetLengthERy
	.p2align	4, 0x90
	.type	_ZNK8NWindows5NFile3NIO9CFileBase9GetLengthERy,@function
_ZNK8NWindows5NFile3NIO9CFileBase9GetLengthERy: # @_ZNK8NWindows5NFile3NIO9CFileBase9GetLengthERy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 48
.Lcfi63:
	.cfi_offset %rbx, -48
.Lcfi64:
	.cfi_offset %r12, -40
.Lcfi65:
	.cfi_offset %r13, -32
.Lcfi66:
	.cfi_offset %r14, -24
.Lcfi67:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	8(%rbx), %edi
	cmpl	$-2, %edi
	je	.LBB6_3
# BB#1:
	cmpl	$-1, %edi
	jne	.LBB6_4
# BB#2:
	callq	__errno_location
	movl	$9, (%rax)
	xorl	%r15d, %r15d
	jmp	.LBB6_9
.LBB6_3:
	movslq	48(%rbx), %rax
	movq	%rax, (%r14)
	jmp	.LBB6_8
.LBB6_4:
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movl	$1, %edx
	callq	lseek64
	movq	%rax, %r12
	cmpq	$-1, %r12
	je	.LBB6_9
# BB#5:
	movl	8(%rbx), %edi
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movl	$2, %edx
	callq	lseek64
	movq	%rax, %r13
	cmpq	$-1, %r13
	je	.LBB6_9
# BB#6:
	movl	8(%rbx), %edi
	xorl	%r15d, %r15d
	xorl	%edx, %edx
	movq	%r12, %rsi
	callq	lseek64
	cmpq	$-1, %rax
	je	.LBB6_9
# BB#7:
	movq	%r13, (%r14)
.LBB6_8:
	movb	$1, %r15b
.LBB6_9:
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	_ZNK8NWindows5NFile3NIO9CFileBase9GetLengthERy, .Lfunc_end6-_ZNK8NWindows5NFile3NIO9CFileBase9GetLengthERy
	.cfi_endproc

	.globl	_ZN8NWindows5NFile3NIO9CFileBase4SeekExjRy
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO9CFileBase4SeekExjRy,@function
_ZN8NWindows5NFile3NIO9CFileBase4SeekExjRy: # @_ZN8NWindows5NFile3NIO9CFileBase4SeekExjRy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 16
.Lcfi69:
	.cfi_offset %rbx, -16
	movq	%rcx, %rbx
	movl	8(%rdi), %eax
	cmpl	$-2, %eax
	je	.LBB7_3
# BB#1:
	cmpl	$-1, %eax
	jne	.LBB7_7
# BB#2:
	callq	__errno_location
	movl	$9, (%rax)
	jmp	.LBB7_15
.LBB7_3:
	testl	%edx, %edx
	je	.LBB7_11
# BB#4:
	cmpl	$2, %edx
	je	.LBB7_9
# BB#5:
	cmpl	$1, %edx
	jne	.LBB7_14
# BB#6:
	leaq	1080(%rdi), %rax
	jmp	.LBB7_10
.LBB7_7:
	movl	%eax, %edi
	callq	lseek64
	cmpq	$-1, %rax
	jne	.LBB7_13
	jmp	.LBB7_15
.LBB7_9:
	leaq	48(%rdi), %rax
.LBB7_10:                               # %.sink.split
	movslq	(%rax), %rax
	addq	%rax, %rsi
.LBB7_11:
	testq	%rsi, %rsi
	js	.LBB7_14
# BB#12:
	movslq	48(%rdi), %rax
	cmpq	%rax, %rsi
	cmovgq	%rax, %rsi
	movl	%esi, 1080(%rdi)
	movslq	%esi, %rax
.LBB7_13:
	movq	%rax, (%rbx)
	movb	$1, %al
	jmp	.LBB7_16
.LBB7_14:                               # %.thread
	callq	__errno_location
	movl	$22, (%rax)
.LBB7_15:
	xorl	%eax, %eax
.LBB7_16:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end7:
	.size	_ZN8NWindows5NFile3NIO9CFileBase4SeekExjRy, .Lfunc_end7-_ZN8NWindows5NFile3NIO9CFileBase4SeekExjRy
	.cfi_endproc

	.globl	_ZN8NWindows5NFile3NIO9CFileBase4SeekEyRy
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO9CFileBase4SeekEyRy,@function
_ZN8NWindows5NFile3NIO9CFileBase4SeekEyRy: # @_ZN8NWindows5NFile3NIO9CFileBase4SeekEyRy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi72:
	.cfi_def_cfa_offset 32
.Lcfi73:
	.cfi_offset %rbx, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	8(%rdi), %eax
	cmpl	$-2, %eax
	je	.LBB8_3
# BB#1:
	cmpl	$-1, %eax
	jne	.LBB8_5
# BB#2:
	callq	__errno_location
	movl	$9, (%rax)
	jmp	.LBB8_8
.LBB8_3:
	testq	%rsi, %rsi
	js	.LBB8_7
# BB#4:
	movslq	48(%rdi), %rax
	cmpq	%rsi, %rax
	cmovleq	%rax, %rsi
	movl	%esi, 1080(%rdi)
	movslq	%esi, %rax
	jmp	.LBB8_6
.LBB8_5:
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	movl	%eax, %edi
	callq	lseek64
	cmpq	$-1, %rax
	je	.LBB8_9
.LBB8_6:
	movq	%rax, (%rbx)
	movb	$1, %bpl
	jmp	.LBB8_9
.LBB8_7:                                # %.thread.i
	callq	__errno_location
	movl	$22, (%rax)
.LBB8_8:                                # %_ZN8NWindows5NFile3NIO9CFileBase4SeekExjRy.exit
	xorl	%ebp, %ebp
.LBB8_9:                                # %_ZN8NWindows5NFile3NIO9CFileBase4SeekExjRy.exit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN8NWindows5NFile3NIO9CFileBase4SeekEyRy, .Lfunc_end8-_ZN8NWindows5NFile3NIO9CFileBase4SeekEyRy
	.cfi_endproc

	.globl	_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwjjj
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwjjj,@function
_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwjjj: # @_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwjjj
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi75:
	.cfi_def_cfa_offset 16
	movl	$0, (%rsp)
	movl	$-2147483648, %edx      # imm = 0x80000000
	movl	%ecx, %r8d
	callq	_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKwjjjjb
	popq	%rcx
	retq
.Lfunc_end9:
	.size	_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwjjj, .Lfunc_end9-_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwjjj
	.cfi_endproc

	.globl	_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwb
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwb,@function
_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwb: # @_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwb
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi76:
	.cfi_def_cfa_offset 16
	movzbl	%dl, %eax
	movl	%eax, (%rsp)
	movl	$-2147483648, %edx      # imm = 0x80000000
	movl	$3, %r8d
	callq	_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKwjjjjb
	popq	%rcx
	retq
.Lfunc_end10:
	.size	_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwb, .Lfunc_end10-_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwb
	.cfi_endproc

	.globl	_ZN8NWindows5NFile3NIO7CInFile8ReadPartEPvjRj
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO7CInFile8ReadPartEPvjRj,@function
_ZN8NWindows5NFile3NIO7CInFile8ReadPartEPvjRj: # @_ZN8NWindows5NFile3NIO7CInFile8ReadPartEPvjRj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi83:
	.cfi_def_cfa_offset 64
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r13
	movl	8(%r13), %edi
	cmpl	$-1, %edi
	je	.LBB11_1
# BB#2:
	testl	%edx, %edx
	je	.LBB11_3
# BB#4:
	cmpl	$-2, %edi
	jne	.LBB11_5
# BB#10:
	movslq	1080(%r13), %rax
	movl	48(%r13), %ecx
	cmpl	%eax, %ecx
	jle	.LBB11_3
# BB#11:
	subl	%eax, %ecx
	cmpl	%edx, %ecx
	cmoval	%edx, %ecx
	leaq	52(%r13,%rax), %rsi
	movslq	%ecx, %rbx
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	movl	%ebx, (%r14)
	addl	%ebx, 1080(%r13)
	jmp	.LBB11_12
.LBB11_1:
	callq	__errno_location
	movl	$9, (%rax)
	xorl	%eax, %eax
	jmp	.LBB11_13
.LBB11_3:
	movl	$0, (%r14)
.LBB11_12:                              # %_ZN8NWindows5NFile3NIO7CInFile4ReadEPvjRj.exit
	movb	$1, %al
	jmp	.LBB11_13
.LBB11_5:                               # %.preheader.i
	movl	%edx, %r12d
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	read
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jns	.LBB11_9
# BB#6:                                 # %.lr.ph
	callq	__errno_location
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB11_7:                               # =>This Inner Loop Header: Depth=1
	cmpl	$4, (%rbp)
	jne	.LBB11_9
# BB#8:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB11_7 Depth=1
	movl	8(%r13), %edi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	read
	movq	%rax, %rbx
	testq	%rbx, %rbx
	js	.LBB11_7
.LBB11_9:                               # %.critedge.i
	xorl	%eax, %eax
	cmpq	$-1, %rbx
	cmovel	%eax, %ebx
	setne	%al
	movl	%ebx, (%r14)
.LBB11_13:                              # %_ZN8NWindows5NFile3NIO7CInFile4ReadEPvjRj.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN8NWindows5NFile3NIO7CInFile8ReadPartEPvjRj, .Lfunc_end11-_ZN8NWindows5NFile3NIO7CInFile8ReadPartEPvjRj
	.cfi_endproc

	.globl	_ZN8NWindows5NFile3NIO7CInFile4ReadEPvjRj
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO7CInFile4ReadEPvjRj,@function
_ZN8NWindows5NFile3NIO7CInFile4ReadEPvjRj: # @_ZN8NWindows5NFile3NIO7CInFile4ReadEPvjRj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi93:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi94:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi96:
	.cfi_def_cfa_offset 64
.Lcfi97:
	.cfi_offset %rbx, -56
.Lcfi98:
	.cfi_offset %r12, -48
.Lcfi99:
	.cfi_offset %r13, -40
.Lcfi100:
	.cfi_offset %r14, -32
.Lcfi101:
	.cfi_offset %r15, -24
.Lcfi102:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r13
	movl	8(%r13), %edi
	cmpl	$-1, %edi
	je	.LBB12_1
# BB#2:
	testl	%edx, %edx
	je	.LBB12_3
# BB#4:
	cmpl	$-2, %edi
	jne	.LBB12_5
# BB#10:
	movslq	1080(%r13), %rax
	movl	48(%r13), %ecx
	cmpl	%eax, %ecx
	jle	.LBB12_3
# BB#11:
	subl	%eax, %ecx
	cmpl	%edx, %ecx
	cmoval	%edx, %ecx
	leaq	52(%r13,%rax), %rsi
	movslq	%ecx, %rbx
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	movl	%ebx, (%r14)
	addl	%ebx, 1080(%r13)
	jmp	.LBB12_12
.LBB12_1:
	callq	__errno_location
	movl	$9, (%rax)
	xorl	%eax, %eax
	jmp	.LBB12_13
.LBB12_3:
	movl	$0, (%r14)
.LBB12_12:
	movb	$1, %al
	jmp	.LBB12_13
.LBB12_5:                               # %.preheader
	movl	%edx, %r12d
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	read
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jns	.LBB12_9
# BB#6:                                 # %.lr.ph.preheader
	callq	__errno_location
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB12_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$4, (%rbp)
	jne	.LBB12_9
# BB#8:                                 # %._crit_edge
                                        #   in Loop: Header=BB12_7 Depth=1
	movl	8(%r13), %edi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	read
	movq	%rax, %rbx
	testq	%rbx, %rbx
	js	.LBB12_7
.LBB12_9:                               # %.critedge
	xorl	%eax, %eax
	cmpq	$-1, %rbx
	cmovel	%eax, %ebx
	setne	%al
	movl	%ebx, (%r14)
.LBB12_13:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN8NWindows5NFile3NIO7CInFile4ReadEPvjRj, .Lfunc_end12-_ZN8NWindows5NFile3NIO7CInFile4ReadEPvjRj
	.cfi_endproc

	.globl	_ZN8NWindows5NFile3NIO8COutFile4OpenEPKwjjj
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO8COutFile4OpenEPKwjjj,@function
_ZN8NWindows5NFile3NIO8COutFile4OpenEPKwjjj: # @_ZN8NWindows5NFile3NIO8COutFile4OpenEPKwjjj
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi103:
	.cfi_def_cfa_offset 16
	movl	$0, (%rsp)
	movl	$1073741824, %edx       # imm = 0x40000000
	movl	%ecx, %r8d
	callq	_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKwjjjjb
	popq	%rcx
	retq
.Lfunc_end13:
	.size	_ZN8NWindows5NFile3NIO8COutFile4OpenEPKwjjj, .Lfunc_end13-_ZN8NWindows5NFile3NIO8COutFile4OpenEPKwjjj
	.cfi_endproc

	.globl	_ZN8NWindows5NFile3NIO8COutFile4OpenEPKwj
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO8COutFile4OpenEPKwj,@function
_ZN8NWindows5NFile3NIO8COutFile4OpenEPKwj: # @_ZN8NWindows5NFile3NIO8COutFile4OpenEPKwj
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi104:
	.cfi_def_cfa_offset 16
	movl	%edx, %eax
	movl	$0, (%rsp)
	movl	$1073741824, %edx       # imm = 0x40000000
	movl	%eax, %r8d
	callq	_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKwjjjjb
	popq	%rcx
	retq
.Lfunc_end14:
	.size	_ZN8NWindows5NFile3NIO8COutFile4OpenEPKwj, .Lfunc_end14-_ZN8NWindows5NFile3NIO8COutFile4OpenEPKwj
	.cfi_endproc

	.globl	_ZN8NWindows5NFile3NIO8COutFile6CreateEPKwb
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO8COutFile6CreateEPKwb,@function
_ZN8NWindows5NFile3NIO8COutFile6CreateEPKwb: # @_ZN8NWindows5NFile3NIO8COutFile6CreateEPKwb
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi105:
	.cfi_def_cfa_offset 16
	movzbl	%dl, %r8d
	incl	%r8d
	movl	$0, (%rsp)
	movl	$1073741824, %edx       # imm = 0x40000000
	callq	_ZN8NWindows5NFile3NIO9CFileBase6CreateEPKwjjjjb
	popq	%rcx
	retq
.Lfunc_end15:
	.size	_ZN8NWindows5NFile3NIO8COutFile6CreateEPKwb, .Lfunc_end15-_ZN8NWindows5NFile3NIO8COutFile6CreateEPKwb
	.cfi_endproc

	.globl	_ZN8NWindows5NFile3NIO8COutFile7SetTimeEPK9_FILETIMES5_S5_
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO8COutFile7SetTimeEPK9_FILETIMES5_S5_,@function
_ZN8NWindows5NFile3NIO8COutFile7SetTimeEPK9_FILETIMES5_S5_: # @_ZN8NWindows5NFile3NIO8COutFile7SetTimeEPK9_FILETIMES5_S5_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi109:
	.cfi_def_cfa_offset 48
.Lcfi110:
	.cfi_offset %rbx, -32
.Lcfi111:
	.cfi_offset %r14, -24
.Lcfi112:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdi, %r14
	cmpl	$-1, 8(%r14)
	je	.LBB16_1
# BB#2:
	testq	%rdx, %rdx
	je	.LBB16_4
# BB#3:
	movl	(%rdx), %eax
	movl	4(%rdx), %ecx
	shlq	$32, %rcx
	orq	%rcx, %rax
	movq	%rax, 8(%rsp)
	leaq	8(%rsp), %rdi
	leaq	4(%rsp), %rsi
	callq	_Z25RtlTimeToSecondsSince1970PK13LARGE_INTEGERPj
	movl	4(%rsp), %eax
	movq	%rax, 32(%r14)
.LBB16_4:
	movb	$1, %bpl
	testq	%rbx, %rbx
	je	.LBB16_6
# BB#5:
	movl	(%rbx), %eax
	movl	4(%rbx), %ecx
	shlq	$32, %rcx
	orq	%rcx, %rax
	movq	%rax, 8(%rsp)
	leaq	8(%rsp), %rdi
	leaq	4(%rsp), %rsi
	callq	_Z25RtlTimeToSecondsSince1970PK13LARGE_INTEGERPj
	movl	4(%rsp), %eax
	movq	%rax, 40(%r14)
	jmp	.LBB16_6
.LBB16_1:
	callq	__errno_location
	movl	$9, (%rax)
	xorl	%ebp, %ebp
.LBB16_6:
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end16:
	.size	_ZN8NWindows5NFile3NIO8COutFile7SetTimeEPK9_FILETIMES5_S5_, .Lfunc_end16-_ZN8NWindows5NFile3NIO8COutFile7SetTimeEPK9_FILETIMES5_S5_
	.cfi_endproc

	.globl	_ZN8NWindows5NFile3NIO8COutFile8SetMTimeEPK9_FILETIME
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO8COutFile8SetMTimeEPK9_FILETIME,@function
_ZN8NWindows5NFile3NIO8COutFile8SetMTimeEPK9_FILETIME: # @_ZN8NWindows5NFile3NIO8COutFile8SetMTimeEPK9_FILETIME
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi115:
	.cfi_def_cfa_offset 48
.Lcfi116:
	.cfi_offset %rbx, -24
.Lcfi117:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cmpl	$-1, 8(%rbx)
	je	.LBB17_1
# BB#2:
	movb	$1, %bpl
	testq	%rsi, %rsi
	je	.LBB17_4
# BB#3:
	movl	(%rsi), %eax
	movl	4(%rsi), %ecx
	shlq	$32, %rcx
	orq	%rcx, %rax
	movq	%rax, 16(%rsp)
	leaq	16(%rsp), %rdi
	leaq	12(%rsp), %rsi
	callq	_Z25RtlTimeToSecondsSince1970PK13LARGE_INTEGERPj
	movl	12(%rsp), %eax
	movq	%rax, 40(%rbx)
	jmp	.LBB17_4
.LBB17_1:
	callq	__errno_location
	movl	$9, (%rax)
	xorl	%ebp, %ebp
.LBB17_4:                               # %_ZN8NWindows5NFile3NIO8COutFile7SetTimeEPK9_FILETIMES5_S5_.exit
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end17:
	.size	_ZN8NWindows5NFile3NIO8COutFile8SetMTimeEPK9_FILETIME, .Lfunc_end17-_ZN8NWindows5NFile3NIO8COutFile8SetMTimeEPK9_FILETIME
	.cfi_endproc

	.globl	_ZN8NWindows5NFile3NIO8COutFile9WritePartEPKvjRj
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO8COutFile9WritePartEPKvjRj,@function
_ZN8NWindows5NFile3NIO8COutFile9WritePartEPKvjRj: # @_ZN8NWindows5NFile3NIO8COutFile9WritePartEPKvjRj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi118:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi119:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi120:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi121:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi122:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi124:
	.cfi_def_cfa_offset 64
.Lcfi125:
	.cfi_offset %rbx, -56
.Lcfi126:
	.cfi_offset %r12, -48
.Lcfi127:
	.cfi_offset %r13, -40
.Lcfi128:
	.cfi_offset %r14, -32
.Lcfi129:
	.cfi_offset %r15, -24
.Lcfi130:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r13
	movl	8(%r13), %edi
	cmpl	$-1, %edi
	je	.LBB18_7
# BB#1:                                 # %.preheader.i
	movl	%edx, %r12d
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	write
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jns	.LBB18_5
# BB#2:                                 # %.lr.ph
	callq	__errno_location
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB18_3:                               # =>This Inner Loop Header: Depth=1
	cmpl	$4, (%rbp)
	jne	.LBB18_5
# BB#4:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB18_3 Depth=1
	movl	8(%r13), %edi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	write
	movq	%rax, %rbx
	testq	%rbx, %rbx
	js	.LBB18_3
.LBB18_5:                               # %.critedge.i
	xorl	%eax, %eax
	cmpq	$-1, %rbx
	cmovel	%eax, %ebx
	setne	%al
	movl	%ebx, (%r14)
	jmp	.LBB18_6
.LBB18_7:
	callq	__errno_location
	movl	$9, (%rax)
	xorl	%eax, %eax
.LBB18_6:                               # %_ZN8NWindows5NFile3NIO8COutFile5WriteEPKvjRj.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	_ZN8NWindows5NFile3NIO8COutFile9WritePartEPKvjRj, .Lfunc_end18-_ZN8NWindows5NFile3NIO8COutFile9WritePartEPKvjRj
	.cfi_endproc

	.globl	_ZN8NWindows5NFile3NIO8COutFile5WriteEPKvjRj
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO8COutFile5WriteEPKvjRj,@function
_ZN8NWindows5NFile3NIO8COutFile5WriteEPKvjRj: # @_ZN8NWindows5NFile3NIO8COutFile5WriteEPKvjRj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi131:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi132:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi133:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi134:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi135:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi136:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi137:
	.cfi_def_cfa_offset 64
.Lcfi138:
	.cfi_offset %rbx, -56
.Lcfi139:
	.cfi_offset %r12, -48
.Lcfi140:
	.cfi_offset %r13, -40
.Lcfi141:
	.cfi_offset %r14, -32
.Lcfi142:
	.cfi_offset %r15, -24
.Lcfi143:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r13
	movl	8(%r13), %edi
	cmpl	$-1, %edi
	je	.LBB19_7
# BB#1:                                 # %.preheader
	movl	%edx, %r12d
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	write
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jns	.LBB19_5
# BB#2:                                 # %.lr.ph.preheader
	callq	__errno_location
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB19_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$4, (%rbp)
	jne	.LBB19_5
# BB#4:                                 # %._crit_edge
                                        #   in Loop: Header=BB19_3 Depth=1
	movl	8(%r13), %edi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	write
	movq	%rax, %rbx
	testq	%rbx, %rbx
	js	.LBB19_3
.LBB19_5:                               # %.critedge
	xorl	%eax, %eax
	cmpq	$-1, %rbx
	cmovel	%eax, %ebx
	setne	%al
	movl	%ebx, (%r14)
	jmp	.LBB19_6
.LBB19_7:
	callq	__errno_location
	movl	$9, (%rax)
	xorl	%eax, %eax
.LBB19_6:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	_ZN8NWindows5NFile3NIO8COutFile5WriteEPKvjRj, .Lfunc_end19-_ZN8NWindows5NFile3NIO8COutFile5WriteEPKvjRj
	.cfi_endproc

	.globl	_ZN8NWindows5NFile3NIO8COutFile12SetEndOfFileEv
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO8COutFile12SetEndOfFileEv,@function
_ZN8NWindows5NFile3NIO8COutFile12SetEndOfFileEv: # @_ZN8NWindows5NFile3NIO8COutFile12SetEndOfFileEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi144:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi145:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi146:
	.cfi_def_cfa_offset 32
.Lcfi147:
	.cfi_offset %rbx, -24
.Lcfi148:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	8(%rbx), %edi
	cmpl	$-1, %edi
	je	.LBB20_1
# BB#2:
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movl	$1, %edx
	callq	lseek64
	cmpq	$-1, %rax
	je	.LBB20_4
# BB#3:
	movl	8(%rbx), %edi
	movq	%rax, %rsi
	callq	ftruncate64
	testl	%eax, %eax
	sete	%r14b
	jmp	.LBB20_4
.LBB20_1:
	callq	__errno_location
	movl	$9, (%rax)
	xorl	%r14d, %r14d
.LBB20_4:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end20:
	.size	_ZN8NWindows5NFile3NIO8COutFile12SetEndOfFileEv, .Lfunc_end20-_ZN8NWindows5NFile3NIO8COutFile12SetEndOfFileEv
	.cfi_endproc

	.globl	_ZN8NWindows5NFile3NIO8COutFile9SetLengthEy
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO8COutFile9SetLengthEy,@function
_ZN8NWindows5NFile3NIO8COutFile9SetLengthEy: # @_ZN8NWindows5NFile3NIO8COutFile9SetLengthEy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi150:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 32
.Lcfi152:
	.cfi_offset %rbx, -32
.Lcfi153:
	.cfi_offset %r14, -24
.Lcfi154:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movl	8(%r15), %edi
	cmpl	$-2, %edi
	je	.LBB21_3
# BB#1:
	cmpl	$-1, %edi
	jne	.LBB21_5
.LBB21_2:
	callq	__errno_location
	movl	$9, (%rax)
	jmp	.LBB21_12
.LBB21_3:
	testq	%rbx, %rbx
	js	.LBB21_11
# BB#4:
	movslq	48(%r15), %rax
	cmpq	%rbx, %rax
	cmovgq	%rbx, %rax
	movl	%eax, 1080(%r15)
	cltq
	cmpq	%rbx, %rax
	je	.LBB21_7
	jmp	.LBB21_12
.LBB21_5:
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	movq	%rbx, %rsi
	callq	lseek64
	cmpq	$-1, %rax
	je	.LBB21_13
# BB#6:                                 # %_ZN8NWindows5NFile3NIO9CFileBase4SeekEyRy.exit
	cmpq	%rbx, %rax
	jne	.LBB21_12
.LBB21_7:
	movl	8(%r15), %edi
	cmpl	$-1, %edi
	je	.LBB21_2
# BB#8:
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movl	$1, %edx
	callq	lseek64
	cmpq	$-1, %rax
	je	.LBB21_13
# BB#9:
	movl	8(%r15), %edi
	movq	%rax, %rsi
	callq	ftruncate64
	testl	%eax, %eax
	sete	%r14b
	jmp	.LBB21_13
.LBB21_11:                              # %.thread.i.i
	callq	__errno_location
	movl	$22, (%rax)
.LBB21_12:                              # %_ZN8NWindows5NFile3NIO8COutFile12SetEndOfFileEv.exit
	xorl	%r14d, %r14d
.LBB21_13:                              # %_ZN8NWindows5NFile3NIO8COutFile12SetEndOfFileEv.exit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	_ZN8NWindows5NFile3NIO8COutFile9SetLengthEy, .Lfunc_end21-_ZN8NWindows5NFile3NIO8COutFile9SetLengthEy
	.cfi_endproc

	.type	_ZTVN8NWindows5NFile3NIO9CFileBaseE,@object # @_ZTVN8NWindows5NFile3NIO9CFileBaseE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NWindows5NFile3NIO9CFileBaseE
	.p2align	3
_ZTVN8NWindows5NFile3NIO9CFileBaseE:
	.quad	0
	.quad	_ZTIN8NWindows5NFile3NIO9CFileBaseE
	.quad	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.quad	_ZN8NWindows5NFile3NIO9CFileBaseD0Ev
	.quad	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.size	_ZTVN8NWindows5NFile3NIO9CFileBaseE, 40

	.type	_ZTSN8NWindows5NFile3NIO9CFileBaseE,@object # @_ZTSN8NWindows5NFile3NIO9CFileBaseE
	.globl	_ZTSN8NWindows5NFile3NIO9CFileBaseE
	.p2align	4
_ZTSN8NWindows5NFile3NIO9CFileBaseE:
	.asciz	"N8NWindows5NFile3NIO9CFileBaseE"
	.size	_ZTSN8NWindows5NFile3NIO9CFileBaseE, 32

	.type	_ZTIN8NWindows5NFile3NIO9CFileBaseE,@object # @_ZTIN8NWindows5NFile3NIO9CFileBaseE
	.globl	_ZTIN8NWindows5NFile3NIO9CFileBaseE
	.p2align	3
_ZTIN8NWindows5NFile3NIO9CFileBaseE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN8NWindows5NFile3NIO9CFileBaseE
	.size	_ZTIN8NWindows5NFile3NIO9CFileBaseE, 16


	.globl	_ZN8NWindows5NFile3NIO9CFileBaseD1Ev
	.type	_ZN8NWindows5NFile3NIO9CFileBaseD1Ev,@function
_ZN8NWindows5NFile3NIO9CFileBaseD1Ev = _ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
