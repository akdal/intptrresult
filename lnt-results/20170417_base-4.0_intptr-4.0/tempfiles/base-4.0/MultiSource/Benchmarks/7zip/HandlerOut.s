	.text
	.file	"HandlerOut.bc"
	.globl	_ZNK8NArchive14COneMethodInfo6IsLzmaEv
	.p2align	4, 0x90
	.type	_ZNK8NArchive14COneMethodInfo6IsLzmaEv,@function
_ZNK8NArchive14COneMethodInfo6IsLzmaEv: # @_ZNK8NArchive14COneMethodInfo6IsLzmaEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	movl	$.L.str.11, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB0_1
# BB#2:
	movq	32(%rbx), %rdi
	movl	$.L.str.12, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	sete	%al
	popq	%rbx
	retq
.LBB0_1:
	movb	$1, %al
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZNK8NArchive14COneMethodInfo6IsLzmaEv, .Lfunc_end0-_ZNK8NArchive14COneMethodInfo6IsLzmaEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	76                      # 0x4c
	.long	90                      # 0x5a
	.long	77                      # 0x4d
	.long	65                      # 0x41
	.text
	.globl	_ZN8NArchive11COutHandler21SetCompressionMethod2ERNS_14COneMethodInfoEj
	.p2align	4, 0x90
	.type	_ZN8NArchive11COutHandler21SetCompressionMethod2ERNS_14COneMethodInfoEj,@function
_ZN8NArchive11COutHandler21SetCompressionMethod2ERNS_14COneMethodInfoEj: # @_ZN8NArchive11COutHandler21SetCompressionMethod2ERNS_14COneMethodInfoEj
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi8:
	.cfi_def_cfa_offset 592
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r12
	movl	76(%rdi), %r13d
	cmpl	$0, 40(%r12)
	je	.LBB1_2
# BB#1:                                 # %._crit_edge
	leaq	32(%r12), %rbx
	movq	32(%r12), %rbp
	jmp	.LBB1_8
.LBB1_2:                                # %_Z11MyStringLenIwEiPKT_.exit.i
	movl	$0, 40(%r12)
	movq	32(%r12), %rbp
	movl	$0, (%rbp)
	movl	44(%r12), %ebx
	cmpl	$5, %ebx
	je	.LBB1_7
# BB#3:
	movl	$20, %edi
	callq	_Znam
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB1_6
# BB#4:
	testl	%ebx, %ebx
	jle	.LBB1_6
# BB#5:                                 # %._crit_edge.thread.i.i
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	40(%r12), %rax
.LBB1_6:                                # %._crit_edge16.i.i
	movq	%r15, 32(%r12)
	movl	$0, (%r15,%rax,4)
	movl	$5, 44(%r12)
	movq	%r15, %rbp
.LBB1_7:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.preheader
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [76,90,77,65]
	movups	%xmm0, (%rbp)
	movl	$0, 16(%rbp)
	movl	$4, 40(%r12)
	leaq	32(%r12), %rbx
.LBB1_8:
	movl	$.L.str.11, %esi
	movq	%rbp, %rdi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB1_10
# BB#9:                                 # %_ZNK8NArchive14COneMethodInfo6IsLzmaEv.exit
	movq	(%rbx), %rdi
	movl	$.L.str.12, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB1_10
# BB#85:
	movq	32(%r12), %rdi
	movl	$.L.str.15, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB1_87
# BB#86:                                # %_ZN8NArchiveL15IsDeflateMethodERK11CStringBaseIwE.exit
	movq	32(%r12), %rdi
	movl	$.L.str.16, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB1_87
# BB#130:
	movq	32(%r12), %rdi
	movl	$.L.str.17, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB1_131
# BB#174:
	movq	32(%r12), %rdi
	movl	$.L.str.18, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	jne	.LBB1_198
# BB#175:
	cmpl	$8, %r13d
	jbe	.LBB1_177
# BB#176:
	movl	$32, %r15d
	movl	$201326592, %eax        # imm = 0xC000000
	jmp	.LBB1_180
.LBB1_10:                               # %_ZNK8NArchive14COneMethodInfo6IsLzmaEv.exit.thread
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	movl	$67108864, %eax         # imm = 0x4000000
	cmpl	$8, %r13d
	ja	.LBB1_14
# BB#11:
	movl	$33554432, %eax         # imm = 0x2000000
	cmpl	$6, %r13d
	ja	.LBB1_14
# BB#12:
	movl	$16777216, %eax         # imm = 0x1000000
	cmpl	$4, %r13d
	ja	.LBB1_14
# BB#13:
	cmpl	$2, %r13d
	movl	$1048576, %ecx          # imm = 0x100000
	movl	$65536, %eax            # imm = 0x10000
	cmoval	%ecx, %eax
.LBB1_14:
	cmpl	$6, %r13d
	movl	$64, %ecx
	movl	$32, %r14d
	cmoval	%ecx, %r14d
	cmpl	$4, %r13d
	movl	$.L.str.13, %ecx
	movl	$.L.str.14, %r15d
	cmovaq	%rcx, %r15
	movw	$19, 192(%rsp)
	movw	$0, 194(%rsp)
	movl	%eax, 200(%rsp)
	movslq	12(%r12), %rax
	testq	%rax, %rax
	jle	.LBB1_20
# BB#15:                                # %.lr.ph.i
	movq	16(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_17:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$1, (%rsi)
	je	.LBB1_18
# BB#16:                                #   in Loop: Header=BB1_17 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB1_17
.LBB1_20:                               # %.critedge.i
	leaq	232(%rsp), %rbx
	movw	$0, 232(%rsp)
	movw	$0, 234(%rsp)
	movl	$1, 224(%rsp)
.Ltmp144:
	leaq	192(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp145:
# BB#21:
.Ltmp146:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp147:
# BB#22:                                # %.noexc97
	movl	224(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp148:
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp149:
# BB#23:                                # %_ZN5CPropC2ERKS_.exit.i
.Ltmp151:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp152:
# BB#24:
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
.Ltmp157:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp158:
.LBB1_18:                               # %_ZN8NArchiveL13SetMethodPropERNS_14COneMethodInfoEjRKN8NWindows4NCOM12CPropVariantE.exit
	xorl	%ebx, %ebx
	cmpl	$4, %r13d
	seta	%bl
	leaq	192(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movw	$19, 176(%rsp)
	movw	$0, 178(%rsp)
	movl	%ebx, 184(%rsp)
	movslq	12(%r12), %rax
	testq	%rax, %rax
	jle	.LBB1_32
# BB#19:                                # %.lr.ph.i101
	movq	16(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_29:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$12, (%rsi)
	je	.LBB1_30
# BB#28:                                #   in Loop: Header=BB1_29 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB1_29
.LBB1_32:                               # %.critedge.i104
	leaq	256(%rsp), %rbx
	movw	$0, 256(%rsp)
	movw	$0, 258(%rsp)
	movl	$12, 248(%rsp)
.Ltmp162:
	leaq	176(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp163:
# BB#33:
.Ltmp164:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp165:
# BB#34:                                # %.noexc111
	movl	248(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp166:
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp167:
# BB#35:                                # %_ZN5CPropC2ERKS_.exit.i110
.Ltmp169:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp170:
# BB#36:
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
.Ltmp175:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp176:
.LBB1_30:                               # %_ZN8NArchiveL13SetMethodPropERNS_14COneMethodInfoEjRKN8NWindows4NCOM12CPropVariantE.exit109
	leaq	176(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movw	$19, 160(%rsp)
	movw	$0, 162(%rsp)
	movl	%r14d, 168(%rsp)
	movslq	12(%r12), %rax
	testq	%rax, %rax
	movl	12(%rsp), %r14d         # 4-byte Reload
	jle	.LBB1_44
# BB#31:                                # %.lr.ph.i116
	movq	16(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_41:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$8, (%rsi)
	je	.LBB1_42
# BB#40:                                #   in Loop: Header=BB1_41 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB1_41
.LBB1_44:                               # %.critedge.i119
	leaq	280(%rsp), %rbx
	movw	$0, 280(%rsp)
	movw	$0, 282(%rsp)
	movl	$8, 272(%rsp)
.Ltmp180:
	leaq	160(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp181:
# BB#45:
.Ltmp182:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp183:
# BB#46:                                # %.noexc126
	movl	272(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp184:
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp185:
# BB#47:                                # %_ZN5CPropC2ERKS_.exit.i125
.Ltmp187:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp188:
# BB#48:
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
.Ltmp193:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp194:
.LBB1_42:                               # %_ZN8NArchiveL13SetMethodPropERNS_14COneMethodInfoEjRKN8NWindows4NCOM12CPropVariantE.exit124
	leaq	160(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	leaq	208(%rsp), %rdi
	movq	%r15, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1EPKw
	movslq	12(%r12), %rax
	testq	%rax, %rax
	jle	.LBB1_56
# BB#43:                                # %.lr.ph.i131
	movq	16(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_53:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$9, (%rsi)
	je	.LBB1_54
# BB#52:                                #   in Loop: Header=BB1_53 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB1_53
.LBB1_56:                               # %.critedge.i134
	leaq	304(%rsp), %r15
	movw	$0, 304(%rsp)
	movw	$0, 306(%rsp)
	movl	$9, 296(%rsp)
.Ltmp198:
	leaq	208(%rsp), %rsi
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp199:
# BB#57:
.Ltmp200:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp201:
# BB#58:                                # %.noexc141
	movl	296(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp202:
	movq	%r15, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp203:
# BB#59:                                # %_ZN5CPropC2ERKS_.exit.i140
.Ltmp205:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp206:
# BB#60:
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
.Ltmp211:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp212:
.LBB1_54:                               # %_ZN8NArchiveL13SetMethodPropERNS_14COneMethodInfoEjRKN8NWindows4NCOM12CPropVariantE.exit139
	leaq	208(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movw	$19, 144(%rsp)
	movw	$0, 146(%rsp)
	movl	%r14d, 152(%rsp)
	movslq	12(%r12), %rax
	testq	%rax, %rax
	jle	.LBB1_67
# BB#55:                                # %.lr.ph.i146
	movq	16(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_65:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$13, (%rsi)
	je	.LBB1_66
# BB#64:                                #   in Loop: Header=BB1_65 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB1_65
.LBB1_67:                               # %.critedge.i149
	leaq	328(%rsp), %r14
	movw	$0, 328(%rsp)
	movw	$0, 330(%rsp)
	movl	$13, 320(%rsp)
.Ltmp216:
	leaq	144(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp217:
# BB#68:
.Ltmp218:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp219:
# BB#69:                                # %.noexc156
	movl	320(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp220:
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp221:
# BB#70:                                # %_ZN5CPropC2ERKS_.exit.i155
.Ltmp223:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp224:
# BB#71:
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
.Ltmp229:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp230:
.LBB1_66:                               # %_ZN8NArchiveL13SetMethodPropERNS_14COneMethodInfoEjRKN8NWindows4NCOM12CPropVariantE.exit154
	leaq	144(%rsp), %rdi
	jmp	.LBB1_197
.LBB1_87:                               # %_ZN8NArchiveL15IsDeflateMethodERK11CStringBaseIwE.exit.thread
	xorl	%eax, %eax
	cmpl	$6, %r13d
	seta	%al
	movl	$64, %ecx
	movl	$32, %edx
	cmoval	%ecx, %edx
	cmpl	$8, %r13d
	movl	$128, %ebx
	cmovbel	%edx, %ebx
	leal	1(%rax,%rax), %eax
	movl	$10, %r15d
	cmovbel	%eax, %r15d
	xorl	%eax, %eax
	cmpl	$4, %r13d
	seta	%al
	movw	$19, 128(%rsp)
	movw	$0, 130(%rsp)
	movl	%eax, 136(%rsp)
	movslq	12(%r12), %rax
	testq	%rax, %rax
	jle	.LBB1_93
# BB#88:                                # %.lr.ph.i171
	movq	16(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_90:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$12, (%rsi)
	je	.LBB1_91
# BB#89:                                #   in Loop: Header=BB1_90 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB1_90
.LBB1_93:                               # %.critedge.i174
	leaq	352(%rsp), %r14
	movw	$0, 352(%rsp)
	movw	$0, 354(%rsp)
	movl	$12, 344(%rsp)
.Ltmp90:
	leaq	128(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp91:
# BB#94:
.Ltmp92:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp93:
# BB#95:                                # %.noexc181
	movl	344(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp94:
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp95:
# BB#96:                                # %_ZN5CPropC2ERKS_.exit.i180
.Ltmp97:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp98:
# BB#97:
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
.Ltmp103:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp104:
.LBB1_91:                               # %_ZN8NArchiveL13SetMethodPropERNS_14COneMethodInfoEjRKN8NWindows4NCOM12CPropVariantE.exit179
	leaq	128(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movw	$19, 112(%rsp)
	movw	$0, 114(%rsp)
	movl	%ebx, 120(%rsp)
	movslq	12(%r12), %rax
	testq	%rax, %rax
	jle	.LBB1_105
# BB#92:                                # %.lr.ph.i186
	movq	16(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_102:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$8, (%rsi)
	je	.LBB1_103
# BB#101:                               #   in Loop: Header=BB1_102 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB1_102
.LBB1_105:                              # %.critedge.i189
	leaq	376(%rsp), %r14
	movw	$0, 376(%rsp)
	movw	$0, 378(%rsp)
	movl	$8, 368(%rsp)
.Ltmp108:
	leaq	112(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp109:
# BB#106:
.Ltmp110:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp111:
# BB#107:                               # %.noexc196
	movl	368(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp112:
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp113:
# BB#108:                               # %_ZN5CPropC2ERKS_.exit.i195
.Ltmp115:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp116:
# BB#109:
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
.Ltmp121:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp122:
.LBB1_103:                              # %_ZN8NArchiveL13SetMethodPropERNS_14COneMethodInfoEjRKN8NWindows4NCOM12CPropVariantE.exit194
	leaq	112(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movw	$19, 96(%rsp)
	movw	$0, 98(%rsp)
	movl	%r15d, 104(%rsp)
	movslq	12(%r12), %rax
	testq	%rax, %rax
	jle	.LBB1_116
# BB#104:                               # %.lr.ph.i201
	movq	16(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_114:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$11, (%rsi)
	je	.LBB1_115
# BB#113:                               #   in Loop: Header=BB1_114 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB1_114
.LBB1_116:                              # %.critedge.i204
	leaq	400(%rsp), %r14
	movw	$0, 400(%rsp)
	movw	$0, 402(%rsp)
	movl	$11, 392(%rsp)
.Ltmp126:
	leaq	96(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp127:
# BB#117:
.Ltmp128:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp129:
# BB#118:                               # %.noexc211
	movl	392(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp130:
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp131:
# BB#119:                               # %_ZN5CPropC2ERKS_.exit.i210
.Ltmp133:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp134:
# BB#120:
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
.Ltmp139:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp140:
.LBB1_115:                              # %_ZN8NArchiveL13SetMethodPropERNS_14COneMethodInfoEjRKN8NWindows4NCOM12CPropVariantE.exit209
	leaq	96(%rsp), %rdi
.LBB1_197:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.LBB1_198:
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_131:
	movl	%r14d, %ebx
	xorl	%eax, %eax
	cmpl	$6, %r13d
	seta	%al
	incl	%eax
	cmpl	$8, %r13d
	movl	$7, %ecx
	cmovbel	%eax, %ecx
	cmpl	$2, %r13d
	movl	$500000, %eax           # imm = 0x7A120
	movl	$100000, %edx           # imm = 0x186A0
	cmoval	%eax, %edx
	cmpl	$4, %r13d
	movl	$900000, %r14d          # imm = 0xDBBA0
	cmovbel	%edx, %r14d
	movw	$19, 80(%rsp)
	movw	$0, 82(%rsp)
	movl	%ecx, 88(%rsp)
	movslq	12(%r12), %rax
	testq	%rax, %rax
	jle	.LBB1_137
# BB#132:                               # %.lr.ph.i222
	movq	16(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_134:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$11, (%rsi)
	je	.LBB1_135
# BB#133:                               #   in Loop: Header=BB1_134 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB1_134
.LBB1_137:                              # %.critedge.i225
	leaq	424(%rsp), %r15
	movw	$0, 424(%rsp)
	movw	$0, 426(%rsp)
	movl	$11, 416(%rsp)
.Ltmp36:
	leaq	80(%rsp), %rsi
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp37:
# BB#138:
.Ltmp38:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp39:
# BB#139:                               # %.noexc232
	movl	416(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp40:
	movq	%r15, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp41:
# BB#140:                               # %_ZN5CPropC2ERKS_.exit.i231
.Ltmp43:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp44:
# BB#141:
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
.Ltmp49:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp50:
.LBB1_135:                              # %_ZN8NArchiveL13SetMethodPropERNS_14COneMethodInfoEjRKN8NWindows4NCOM12CPropVariantE.exit230
	leaq	80(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movw	$19, 64(%rsp)
	movw	$0, 66(%rsp)
	movl	%r14d, 72(%rsp)
	movslq	12(%r12), %rax
	testq	%rax, %rax
	jle	.LBB1_149
# BB#136:                               # %.lr.ph.i237
	movq	16(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_146:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$1, (%rsi)
	je	.LBB1_147
# BB#145:                               #   in Loop: Header=BB1_146 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB1_146
.LBB1_149:                              # %.critedge.i240
	leaq	448(%rsp), %r15
	movw	$0, 448(%rsp)
	movw	$0, 450(%rsp)
	movl	$1, 440(%rsp)
.Ltmp54:
	leaq	64(%rsp), %rsi
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp55:
# BB#150:
.Ltmp56:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp57:
# BB#151:                               # %.noexc247
	movl	440(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp58:
	movq	%r15, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp59:
# BB#152:                               # %_ZN5CPropC2ERKS_.exit.i246
.Ltmp61:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp62:
# BB#153:
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
.Ltmp67:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp68:
.LBB1_147:                              # %_ZN8NArchiveL13SetMethodPropERNS_14COneMethodInfoEjRKN8NWindows4NCOM12CPropVariantE.exit245
	leaq	64(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movw	$19, 48(%rsp)
	movw	$0, 50(%rsp)
	movl	%ebx, 56(%rsp)
	movslq	12(%r12), %rax
	testq	%rax, %rax
	jle	.LBB1_160
# BB#148:                               # %.lr.ph.i252
	movq	16(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_158:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$13, (%rsi)
	je	.LBB1_159
# BB#157:                               #   in Loop: Header=BB1_158 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB1_158
.LBB1_160:                              # %.critedge.i255
	leaq	472(%rsp), %r14
	movw	$0, 472(%rsp)
	movw	$0, 474(%rsp)
	movl	$13, 464(%rsp)
.Ltmp72:
	leaq	48(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp73:
# BB#161:
.Ltmp74:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp75:
# BB#162:                               # %.noexc262
	movl	464(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp76:
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp77:
# BB#163:                               # %_ZN5CPropC2ERKS_.exit.i261
.Ltmp79:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp80:
# BB#164:
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
.Ltmp85:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp86:
.LBB1_159:                              # %_ZN8NArchiveL13SetMethodPropERNS_14COneMethodInfoEjRKN8NWindows4NCOM12CPropVariantE.exit260
	leaq	48(%rsp), %rdi
	jmp	.LBB1_197
.LBB1_177:
	cmpl	$6, %r13d
	jbe	.LBB1_179
# BB#178:
	movl	$16, %r15d
	movl	$67108864, %eax         # imm = 0x4000000
	jmp	.LBB1_180
.LBB1_179:
	xorl	%ecx, %ecx
	cmpl	$4, %r13d
	seta	%cl
	movl	$16777216, %edx         # imm = 0x1000000
	movl	$4194304, %eax          # imm = 0x400000
	cmoval	%edx, %eax
	leal	4(%rcx,%rcx), %r15d
.LBB1_180:                              # %.thread
	movw	$19, 32(%rsp)
	movw	$0, 34(%rsp)
	movl	%eax, 40(%rsp)
	movslq	12(%r12), %rax
	testq	%rax, %rax
	jle	.LBB1_186
# BB#181:                               # %.lr.ph.i273
	movq	16(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_183:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$2, (%rsi)
	je	.LBB1_184
# BB#182:                               #   in Loop: Header=BB1_183 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB1_183
.LBB1_186:                              # %.critedge.i276
	leaq	496(%rsp), %r14
	movw	$0, 496(%rsp)
	movw	$0, 498(%rsp)
	movl	$2, 488(%rsp)
.Ltmp0:
	leaq	32(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp1:
# BB#187:
.Ltmp2:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp3:
# BB#188:                               # %.noexc283
	movl	488(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp4:
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp5:
# BB#189:                               # %_ZN5CPropC2ERKS_.exit.i282
.Ltmp7:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp8:
# BB#190:
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
.Ltmp13:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp14:
.LBB1_184:                              # %_ZN8NArchiveL13SetMethodPropERNS_14COneMethodInfoEjRKN8NWindows4NCOM12CPropVariantE.exit281
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movw	$19, 16(%rsp)
	movw	$0, 18(%rsp)
	movl	%r15d, 24(%rsp)
	movslq	12(%r12), %rax
	testq	%rax, %rax
	jle	.LBB1_199
# BB#185:                               # %.lr.ph.i288
	movq	16(%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_195:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$3, (%rsi)
	je	.LBB1_196
# BB#194:                               #   in Loop: Header=BB1_195 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB1_195
.LBB1_199:                              # %.critedge.i291
	leaq	520(%rsp), %r14
	movw	$0, 520(%rsp)
	movw	$0, 522(%rsp)
	movl	$3, 512(%rsp)
.Ltmp18:
	leaq	16(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERKS1_
.Ltmp19:
# BB#200:
.Ltmp20:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp21:
# BB#201:                               # %.noexc298
	movl	512(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp22:
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp23:
# BB#202:                               # %_ZN5CPropC2ERKS_.exit.i297
.Ltmp25:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp26:
# BB#203:
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
.Ltmp31:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp32:
.LBB1_196:                              # %_ZN8NArchiveL13SetMethodPropERNS_14COneMethodInfoEjRKN8NWindows4NCOM12CPropVariantE.exit296
	leaq	16(%rsp), %rdi
	jmp	.LBB1_197
.LBB1_209:
.Ltmp33:
	movq	%rax, %r12
	jmp	.LBB1_210
.LBB1_225:
.Ltmp24:
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB1_205
.LBB1_207:
.Ltmp15:
	movq	%rax, %r12
	jmp	.LBB1_208
.LBB1_224:
.Ltmp6:
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB1_192
.LBB1_172:
.Ltmp87:
	movq	%rax, %r12
	jmp	.LBB1_173
.LBB1_223:
.Ltmp78:
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB1_166
.LBB1_170:
.Ltmp69:
	movq	%rax, %r12
	jmp	.LBB1_171
.LBB1_222:
.Ltmp60:
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB1_155
.LBB1_168:
.Ltmp51:
	movq	%rax, %r12
	jmp	.LBB1_169
.LBB1_221:
.Ltmp42:
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB1_143
.LBB1_204:
.Ltmp27:
	movq	%rax, %r12
.LBB1_205:                              # %.body300
.Ltmp28:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp29:
.LBB1_210:                              # %.body294
.Ltmp34:
	leaq	16(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp35:
	jmp	.LBB1_211
.LBB1_206:
.Ltmp30:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_191:
.Ltmp9:
	movq	%rax, %r12
.LBB1_192:                              # %.body285
.Ltmp10:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp11:
.LBB1_208:                              # %.body279
.Ltmp16:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp17:
	jmp	.LBB1_211
.LBB1_193:
.Ltmp12:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_165:
.Ltmp81:
	movq	%rax, %r12
.LBB1_166:                              # %.body264
.Ltmp82:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp83:
.LBB1_173:                              # %.body258
.Ltmp88:
	leaq	48(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp89:
	jmp	.LBB1_211
.LBB1_167:
.Ltmp84:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_154:
.Ltmp63:
	movq	%rax, %r12
.LBB1_155:                              # %.body249
.Ltmp64:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp65:
.LBB1_171:                              # %.body243
.Ltmp70:
	leaq	64(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp71:
	jmp	.LBB1_211
.LBB1_156:
.Ltmp66:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_142:
.Ltmp45:
	movq	%rax, %r12
.LBB1_143:                              # %.body234
.Ltmp46:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp47:
.LBB1_169:                              # %.body228
.Ltmp52:
	leaq	80(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp53:
	jmp	.LBB1_211
.LBB1_144:
.Ltmp48:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_128:
.Ltmp141:
	movq	%rax, %r12
	jmp	.LBB1_129
.LBB1_220:
.Ltmp132:
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB1_122
.LBB1_126:
.Ltmp123:
	movq	%rax, %r12
	jmp	.LBB1_127
.LBB1_219:
.Ltmp114:
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB1_111
.LBB1_124:
.Ltmp105:
	movq	%rax, %r12
	jmp	.LBB1_125
.LBB1_218:
.Ltmp96:
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB1_99
.LBB1_83:
.Ltmp231:
	movq	%rax, %r12
	jmp	.LBB1_84
.LBB1_217:
.Ltmp222:
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB1_73
.LBB1_81:
.Ltmp213:
	movq	%rax, %r12
	jmp	.LBB1_82
.LBB1_216:
.Ltmp204:
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB1_62
.LBB1_79:
.Ltmp195:
	movq	%rax, %r12
	jmp	.LBB1_80
.LBB1_215:
.Ltmp186:
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB1_50
.LBB1_77:
.Ltmp177:
	movq	%rax, %r12
	jmp	.LBB1_78
.LBB1_214:
.Ltmp168:
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB1_38
.LBB1_75:
.Ltmp159:
	movq	%rax, %r12
	jmp	.LBB1_76
.LBB1_213:
.Ltmp150:
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB1_26
.LBB1_121:
.Ltmp135:
	movq	%rax, %r12
.LBB1_122:                              # %.body213
.Ltmp136:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp137:
.LBB1_129:                              # %.body207
.Ltmp142:
	leaq	96(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp143:
	jmp	.LBB1_211
.LBB1_123:
.Ltmp138:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_110:
.Ltmp117:
	movq	%rax, %r12
.LBB1_111:                              # %.body198
.Ltmp118:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp119:
.LBB1_127:                              # %.body192
.Ltmp124:
	leaq	112(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp125:
	jmp	.LBB1_211
.LBB1_112:
.Ltmp120:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_98:
.Ltmp99:
	movq	%rax, %r12
.LBB1_99:                               # %.body183
.Ltmp100:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp101:
.LBB1_125:                              # %.body177
.Ltmp106:
	leaq	128(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp107:
	jmp	.LBB1_211
.LBB1_100:
.Ltmp102:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_72:
.Ltmp225:
	movq	%rax, %r12
.LBB1_73:                               # %.body158
.Ltmp226:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp227:
.LBB1_84:                               # %.body152
.Ltmp232:
	leaq	144(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp233:
	jmp	.LBB1_211
.LBB1_74:
.Ltmp228:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_61:
.Ltmp207:
	movq	%rax, %r12
.LBB1_62:                               # %.body143
.Ltmp208:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp209:
.LBB1_82:                               # %.body137
.Ltmp214:
	leaq	208(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp215:
	jmp	.LBB1_211
.LBB1_63:
.Ltmp210:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_49:
.Ltmp189:
	movq	%rax, %r12
.LBB1_50:                               # %.body128
.Ltmp190:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp191:
.LBB1_80:                               # %.body122
.Ltmp196:
	leaq	160(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp197:
	jmp	.LBB1_211
.LBB1_51:
.Ltmp192:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_37:
.Ltmp171:
	movq	%rax, %r12
.LBB1_38:                               # %.body113
.Ltmp172:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp173:
.LBB1_78:                               # %.body107
.Ltmp178:
	leaq	176(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp179:
	jmp	.LBB1_211
.LBB1_39:
.Ltmp174:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_25:
.Ltmp153:
	movq	%rax, %r12
.LBB1_26:                               # %.body99
.Ltmp154:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp155:
.LBB1_76:                               # %.body
.Ltmp160:
	leaq	192(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp161:
.LBB1_211:
	movq	%r12, %rdi
	callq	_Unwind_Resume
.LBB1_27:
.Ltmp156:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_212:
.Ltmp234:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end1:
	.size	_ZN8NArchive11COutHandler21SetCompressionMethod2ERNS_14COneMethodInfoEj, .Lfunc_end1-_ZN8NArchive11COutHandler21SetCompressionMethod2ERNS_14COneMethodInfoEj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\233\211"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\222\t"                # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp144-.Lfunc_begin0  #   Call between .Lfunc_begin0 and .Ltmp144
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp144-.Lfunc_begin0  # >> Call Site 2 <<
	.long	.Ltmp147-.Ltmp144       #   Call between .Ltmp144 and .Ltmp147
	.long	.Ltmp153-.Lfunc_begin0  #     jumps to .Ltmp153
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin0  # >> Call Site 3 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin0  #     jumps to .Ltmp150
	.byte	0                       #   On action: cleanup
	.long	.Ltmp151-.Lfunc_begin0  # >> Call Site 4 <<
	.long	.Ltmp152-.Ltmp151       #   Call between .Ltmp151 and .Ltmp152
	.long	.Ltmp153-.Lfunc_begin0  #     jumps to .Ltmp153
	.byte	0                       #   On action: cleanup
	.long	.Ltmp157-.Lfunc_begin0  # >> Call Site 5 <<
	.long	.Ltmp158-.Ltmp157       #   Call between .Ltmp157 and .Ltmp158
	.long	.Ltmp159-.Lfunc_begin0  #     jumps to .Ltmp159
	.byte	0                       #   On action: cleanup
	.long	.Ltmp158-.Lfunc_begin0  # >> Call Site 6 <<
	.long	.Ltmp162-.Ltmp158       #   Call between .Ltmp158 and .Ltmp162
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp162-.Lfunc_begin0  # >> Call Site 7 <<
	.long	.Ltmp165-.Ltmp162       #   Call between .Ltmp162 and .Ltmp165
	.long	.Ltmp171-.Lfunc_begin0  #     jumps to .Ltmp171
	.byte	0                       #   On action: cleanup
	.long	.Ltmp166-.Lfunc_begin0  # >> Call Site 8 <<
	.long	.Ltmp167-.Ltmp166       #   Call between .Ltmp166 and .Ltmp167
	.long	.Ltmp168-.Lfunc_begin0  #     jumps to .Ltmp168
	.byte	0                       #   On action: cleanup
	.long	.Ltmp169-.Lfunc_begin0  # >> Call Site 9 <<
	.long	.Ltmp170-.Ltmp169       #   Call between .Ltmp169 and .Ltmp170
	.long	.Ltmp171-.Lfunc_begin0  #     jumps to .Ltmp171
	.byte	0                       #   On action: cleanup
	.long	.Ltmp175-.Lfunc_begin0  # >> Call Site 10 <<
	.long	.Ltmp176-.Ltmp175       #   Call between .Ltmp175 and .Ltmp176
	.long	.Ltmp177-.Lfunc_begin0  #     jumps to .Ltmp177
	.byte	0                       #   On action: cleanup
	.long	.Ltmp176-.Lfunc_begin0  # >> Call Site 11 <<
	.long	.Ltmp180-.Ltmp176       #   Call between .Ltmp176 and .Ltmp180
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp180-.Lfunc_begin0  # >> Call Site 12 <<
	.long	.Ltmp183-.Ltmp180       #   Call between .Ltmp180 and .Ltmp183
	.long	.Ltmp189-.Lfunc_begin0  #     jumps to .Ltmp189
	.byte	0                       #   On action: cleanup
	.long	.Ltmp184-.Lfunc_begin0  # >> Call Site 13 <<
	.long	.Ltmp185-.Ltmp184       #   Call between .Ltmp184 and .Ltmp185
	.long	.Ltmp186-.Lfunc_begin0  #     jumps to .Ltmp186
	.byte	0                       #   On action: cleanup
	.long	.Ltmp187-.Lfunc_begin0  # >> Call Site 14 <<
	.long	.Ltmp188-.Ltmp187       #   Call between .Ltmp187 and .Ltmp188
	.long	.Ltmp189-.Lfunc_begin0  #     jumps to .Ltmp189
	.byte	0                       #   On action: cleanup
	.long	.Ltmp193-.Lfunc_begin0  # >> Call Site 15 <<
	.long	.Ltmp194-.Ltmp193       #   Call between .Ltmp193 and .Ltmp194
	.long	.Ltmp195-.Lfunc_begin0  #     jumps to .Ltmp195
	.byte	0                       #   On action: cleanup
	.long	.Ltmp194-.Lfunc_begin0  # >> Call Site 16 <<
	.long	.Ltmp198-.Ltmp194       #   Call between .Ltmp194 and .Ltmp198
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp198-.Lfunc_begin0  # >> Call Site 17 <<
	.long	.Ltmp201-.Ltmp198       #   Call between .Ltmp198 and .Ltmp201
	.long	.Ltmp207-.Lfunc_begin0  #     jumps to .Ltmp207
	.byte	0                       #   On action: cleanup
	.long	.Ltmp202-.Lfunc_begin0  # >> Call Site 18 <<
	.long	.Ltmp203-.Ltmp202       #   Call between .Ltmp202 and .Ltmp203
	.long	.Ltmp204-.Lfunc_begin0  #     jumps to .Ltmp204
	.byte	0                       #   On action: cleanup
	.long	.Ltmp205-.Lfunc_begin0  # >> Call Site 19 <<
	.long	.Ltmp206-.Ltmp205       #   Call between .Ltmp205 and .Ltmp206
	.long	.Ltmp207-.Lfunc_begin0  #     jumps to .Ltmp207
	.byte	0                       #   On action: cleanup
	.long	.Ltmp211-.Lfunc_begin0  # >> Call Site 20 <<
	.long	.Ltmp212-.Ltmp211       #   Call between .Ltmp211 and .Ltmp212
	.long	.Ltmp213-.Lfunc_begin0  #     jumps to .Ltmp213
	.byte	0                       #   On action: cleanup
	.long	.Ltmp212-.Lfunc_begin0  # >> Call Site 21 <<
	.long	.Ltmp216-.Ltmp212       #   Call between .Ltmp212 and .Ltmp216
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp216-.Lfunc_begin0  # >> Call Site 22 <<
	.long	.Ltmp219-.Ltmp216       #   Call between .Ltmp216 and .Ltmp219
	.long	.Ltmp225-.Lfunc_begin0  #     jumps to .Ltmp225
	.byte	0                       #   On action: cleanup
	.long	.Ltmp220-.Lfunc_begin0  # >> Call Site 23 <<
	.long	.Ltmp221-.Ltmp220       #   Call between .Ltmp220 and .Ltmp221
	.long	.Ltmp222-.Lfunc_begin0  #     jumps to .Ltmp222
	.byte	0                       #   On action: cleanup
	.long	.Ltmp223-.Lfunc_begin0  # >> Call Site 24 <<
	.long	.Ltmp224-.Ltmp223       #   Call between .Ltmp223 and .Ltmp224
	.long	.Ltmp225-.Lfunc_begin0  #     jumps to .Ltmp225
	.byte	0                       #   On action: cleanup
	.long	.Ltmp229-.Lfunc_begin0  # >> Call Site 25 <<
	.long	.Ltmp230-.Ltmp229       #   Call between .Ltmp229 and .Ltmp230
	.long	.Ltmp231-.Lfunc_begin0  #     jumps to .Ltmp231
	.byte	0                       #   On action: cleanup
	.long	.Ltmp90-.Lfunc_begin0   # >> Call Site 26 <<
	.long	.Ltmp93-.Ltmp90         #   Call between .Ltmp90 and .Ltmp93
	.long	.Ltmp99-.Lfunc_begin0   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin0   # >> Call Site 27 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin0   #     jumps to .Ltmp96
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin0   # >> Call Site 28 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin0   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp103-.Lfunc_begin0  # >> Call Site 29 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin0  #     jumps to .Ltmp105
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin0  # >> Call Site 30 <<
	.long	.Ltmp108-.Ltmp104       #   Call between .Ltmp104 and .Ltmp108
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp108-.Lfunc_begin0  # >> Call Site 31 <<
	.long	.Ltmp111-.Ltmp108       #   Call between .Ltmp108 and .Ltmp111
	.long	.Ltmp117-.Lfunc_begin0  #     jumps to .Ltmp117
	.byte	0                       #   On action: cleanup
	.long	.Ltmp112-.Lfunc_begin0  # >> Call Site 32 <<
	.long	.Ltmp113-.Ltmp112       #   Call between .Ltmp112 and .Ltmp113
	.long	.Ltmp114-.Lfunc_begin0  #     jumps to .Ltmp114
	.byte	0                       #   On action: cleanup
	.long	.Ltmp115-.Lfunc_begin0  # >> Call Site 33 <<
	.long	.Ltmp116-.Ltmp115       #   Call between .Ltmp115 and .Ltmp116
	.long	.Ltmp117-.Lfunc_begin0  #     jumps to .Ltmp117
	.byte	0                       #   On action: cleanup
	.long	.Ltmp121-.Lfunc_begin0  # >> Call Site 34 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp123-.Lfunc_begin0  #     jumps to .Ltmp123
	.byte	0                       #   On action: cleanup
	.long	.Ltmp122-.Lfunc_begin0  # >> Call Site 35 <<
	.long	.Ltmp126-.Ltmp122       #   Call between .Ltmp122 and .Ltmp126
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin0  # >> Call Site 36 <<
	.long	.Ltmp129-.Ltmp126       #   Call between .Ltmp126 and .Ltmp129
	.long	.Ltmp135-.Lfunc_begin0  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp130-.Lfunc_begin0  # >> Call Site 37 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin0  #     jumps to .Ltmp132
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin0  # >> Call Site 38 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp135-.Lfunc_begin0  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp139-.Lfunc_begin0  # >> Call Site 39 <<
	.long	.Ltmp140-.Ltmp139       #   Call between .Ltmp139 and .Ltmp140
	.long	.Ltmp141-.Lfunc_begin0  #     jumps to .Ltmp141
	.byte	0                       #   On action: cleanup
	.long	.Ltmp140-.Lfunc_begin0  # >> Call Site 40 <<
	.long	.Ltmp36-.Ltmp140        #   Call between .Ltmp140 and .Ltmp36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin0   # >> Call Site 41 <<
	.long	.Ltmp39-.Ltmp36         #   Call between .Ltmp36 and .Ltmp39
	.long	.Ltmp45-.Lfunc_begin0   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin0   # >> Call Site 42 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin0   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin0   # >> Call Site 43 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin0   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin0   # >> Call Site 44 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin0   #     jumps to .Ltmp51
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin0   # >> Call Site 45 <<
	.long	.Ltmp54-.Ltmp50         #   Call between .Ltmp50 and .Ltmp54
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin0   # >> Call Site 46 <<
	.long	.Ltmp57-.Ltmp54         #   Call between .Ltmp54 and .Ltmp57
	.long	.Ltmp63-.Lfunc_begin0   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin0   # >> Call Site 47 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin0   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin0   # >> Call Site 48 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin0   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin0   # >> Call Site 49 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin0   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin0   # >> Call Site 50 <<
	.long	.Ltmp72-.Ltmp68         #   Call between .Ltmp68 and .Ltmp72
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp72-.Lfunc_begin0   # >> Call Site 51 <<
	.long	.Ltmp75-.Ltmp72         #   Call between .Ltmp72 and .Ltmp75
	.long	.Ltmp81-.Lfunc_begin0   #     jumps to .Ltmp81
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin0   # >> Call Site 52 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin0   #     jumps to .Ltmp78
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin0   # >> Call Site 53 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin0   #     jumps to .Ltmp81
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin0   # >> Call Site 54 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin0   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 55 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 56 <<
	.long	.Ltmp5-.Ltmp4           #   Call between .Ltmp4 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 57 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 58 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 59 <<
	.long	.Ltmp18-.Ltmp14         #   Call between .Ltmp14 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 60 <<
	.long	.Ltmp21-.Ltmp18         #   Call between .Ltmp18 and .Ltmp21
	.long	.Ltmp27-.Lfunc_begin0   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 61 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin0   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin0   # >> Call Site 62 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin0   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin0   # >> Call Site 63 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin0   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin0   # >> Call Site 64 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin0   #     jumps to .Ltmp30
	.byte	1                       #   On action: 1
	.long	.Ltmp34-.Lfunc_begin0   # >> Call Site 65 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp234-.Lfunc_begin0  #     jumps to .Ltmp234
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 66 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 67 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp234-.Lfunc_begin0  #     jumps to .Ltmp234
	.byte	1                       #   On action: 1
	.long	.Ltmp82-.Lfunc_begin0   # >> Call Site 68 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin0   #     jumps to .Ltmp84
	.byte	1                       #   On action: 1
	.long	.Ltmp88-.Lfunc_begin0   # >> Call Site 69 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp234-.Lfunc_begin0  #     jumps to .Ltmp234
	.byte	1                       #   On action: 1
	.long	.Ltmp64-.Lfunc_begin0   # >> Call Site 70 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin0   #     jumps to .Ltmp66
	.byte	1                       #   On action: 1
	.long	.Ltmp70-.Lfunc_begin0   # >> Call Site 71 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp234-.Lfunc_begin0  #     jumps to .Ltmp234
	.byte	1                       #   On action: 1
	.long	.Ltmp46-.Lfunc_begin0   # >> Call Site 72 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin0   #     jumps to .Ltmp48
	.byte	1                       #   On action: 1
	.long	.Ltmp52-.Lfunc_begin0   # >> Call Site 73 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp234-.Lfunc_begin0  #     jumps to .Ltmp234
	.byte	1                       #   On action: 1
	.long	.Ltmp136-.Lfunc_begin0  # >> Call Site 74 <<
	.long	.Ltmp137-.Ltmp136       #   Call between .Ltmp136 and .Ltmp137
	.long	.Ltmp138-.Lfunc_begin0  #     jumps to .Ltmp138
	.byte	1                       #   On action: 1
	.long	.Ltmp142-.Lfunc_begin0  # >> Call Site 75 <<
	.long	.Ltmp143-.Ltmp142       #   Call between .Ltmp142 and .Ltmp143
	.long	.Ltmp234-.Lfunc_begin0  #     jumps to .Ltmp234
	.byte	1                       #   On action: 1
	.long	.Ltmp118-.Lfunc_begin0  # >> Call Site 76 <<
	.long	.Ltmp119-.Ltmp118       #   Call between .Ltmp118 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin0  #     jumps to .Ltmp120
	.byte	1                       #   On action: 1
	.long	.Ltmp124-.Lfunc_begin0  # >> Call Site 77 <<
	.long	.Ltmp125-.Ltmp124       #   Call between .Ltmp124 and .Ltmp125
	.long	.Ltmp234-.Lfunc_begin0  #     jumps to .Ltmp234
	.byte	1                       #   On action: 1
	.long	.Ltmp100-.Lfunc_begin0  # >> Call Site 78 <<
	.long	.Ltmp101-.Ltmp100       #   Call between .Ltmp100 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin0  #     jumps to .Ltmp102
	.byte	1                       #   On action: 1
	.long	.Ltmp106-.Lfunc_begin0  # >> Call Site 79 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp234-.Lfunc_begin0  #     jumps to .Ltmp234
	.byte	1                       #   On action: 1
	.long	.Ltmp226-.Lfunc_begin0  # >> Call Site 80 <<
	.long	.Ltmp227-.Ltmp226       #   Call between .Ltmp226 and .Ltmp227
	.long	.Ltmp228-.Lfunc_begin0  #     jumps to .Ltmp228
	.byte	1                       #   On action: 1
	.long	.Ltmp232-.Lfunc_begin0  # >> Call Site 81 <<
	.long	.Ltmp233-.Ltmp232       #   Call between .Ltmp232 and .Ltmp233
	.long	.Ltmp234-.Lfunc_begin0  #     jumps to .Ltmp234
	.byte	1                       #   On action: 1
	.long	.Ltmp208-.Lfunc_begin0  # >> Call Site 82 <<
	.long	.Ltmp209-.Ltmp208       #   Call between .Ltmp208 and .Ltmp209
	.long	.Ltmp210-.Lfunc_begin0  #     jumps to .Ltmp210
	.byte	1                       #   On action: 1
	.long	.Ltmp214-.Lfunc_begin0  # >> Call Site 83 <<
	.long	.Ltmp215-.Ltmp214       #   Call between .Ltmp214 and .Ltmp215
	.long	.Ltmp234-.Lfunc_begin0  #     jumps to .Ltmp234
	.byte	1                       #   On action: 1
	.long	.Ltmp190-.Lfunc_begin0  # >> Call Site 84 <<
	.long	.Ltmp191-.Ltmp190       #   Call between .Ltmp190 and .Ltmp191
	.long	.Ltmp192-.Lfunc_begin0  #     jumps to .Ltmp192
	.byte	1                       #   On action: 1
	.long	.Ltmp196-.Lfunc_begin0  # >> Call Site 85 <<
	.long	.Ltmp197-.Ltmp196       #   Call between .Ltmp196 and .Ltmp197
	.long	.Ltmp234-.Lfunc_begin0  #     jumps to .Ltmp234
	.byte	1                       #   On action: 1
	.long	.Ltmp172-.Lfunc_begin0  # >> Call Site 86 <<
	.long	.Ltmp173-.Ltmp172       #   Call between .Ltmp172 and .Ltmp173
	.long	.Ltmp174-.Lfunc_begin0  #     jumps to .Ltmp174
	.byte	1                       #   On action: 1
	.long	.Ltmp178-.Lfunc_begin0  # >> Call Site 87 <<
	.long	.Ltmp179-.Ltmp178       #   Call between .Ltmp178 and .Ltmp179
	.long	.Ltmp234-.Lfunc_begin0  #     jumps to .Ltmp234
	.byte	1                       #   On action: 1
	.long	.Ltmp154-.Lfunc_begin0  # >> Call Site 88 <<
	.long	.Ltmp155-.Ltmp154       #   Call between .Ltmp154 and .Ltmp155
	.long	.Ltmp156-.Lfunc_begin0  #     jumps to .Ltmp156
	.byte	1                       #   On action: 1
	.long	.Ltmp160-.Lfunc_begin0  # >> Call Site 89 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp234-.Lfunc_begin0  #     jumps to .Ltmp234
	.byte	1                       #   On action: 1
	.long	.Ltmp161-.Lfunc_begin0  # >> Call Site 90 <<
	.long	.Lfunc_end1-.Ltmp161    #   Call between .Ltmp161 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN8NArchive11COutHandler8SetParamERNS_14COneMethodInfoERK11CStringBaseIwES6_
	.p2align	4, 0x90
	.type	_ZN8NArchive11COutHandler8SetParamERNS_14COneMethodInfoERK11CStringBaseIwES6_,@function
_ZN8NArchive11COutHandler8SetParamERNS_14COneMethodInfoERK11CStringBaseIwES6_: # @_ZN8NArchive11COutHandler8SetParamERNS_14COneMethodInfoERK11CStringBaseIwES6_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 40
	subq	$72, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 112
.Lcfi20:
	.cfi_offset %rbx, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rsi, %r14
	movl	$0, 56(%rsp)
.Ltmp235:
	movq	%rdx, %rdi
	callq	_ZN8NArchiveL15FindPropIdExactERK11CStringBaseIwE
.Ltmp236:
# BB#1:
	movl	$-2147024809, %r15d     # imm = 0x80070057
	testl	%eax, %eax
	js	.LBB3_36
# BB#2:
	movslq	%eax, %rbp
	shlq	$4, %rbp
	movl	_ZN8NArchiveL14g_NameToPropIDE(%rbp), %ecx
	movl	%ecx, 48(%rsp)
	cmpl	$2, %eax
	ja	.LBB3_6
# BB#3:
.Ltmp266:
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_Z24ParsePropDictionaryValueRK11CStringBaseIwERj
	movl	%eax, %r15d
.Ltmp267:
# BB#4:
	testl	%r15d, %r15d
	jne	.LBB3_36
# BB#5:
	movl	16(%rsp), %esi
	leaq	56(%rsp), %rdi
.Ltmp268:
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp269:
	jmp	.LBB3_32
.LBB3_6:
	movl	$0, 32(%rsp)
	cmpl	$7, %eax
	je	.LBB3_10
# BB#7:
	cmpl	$12, %eax
	jne	.LBB3_13
# BB#8:
	movq	(%rbx), %rsi
.Ltmp243:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp244:
	jmp	.LBB3_18
.LBB3_10:
.Ltmp238:
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_Z12StringToBoolRK11CStringBaseIwERb
.Ltmp239:
# BB#11:
	testb	%al, %al
	je	.LBB3_29
# BB#12:
	movzbl	16(%rsp), %esi
.Ltmp240:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp241:
	jmp	.LBB3_18
.LBB3_13:
.Ltmp245:
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_Z19ParseStringToUInt32RK11CStringBaseIwERj
.Ltmp246:
# BB#14:
	cmpl	8(%rbx), %eax
	jne	.LBB3_17
# BB#15:
	movl	16(%rsp), %esi
.Ltmp249:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp250:
	jmp	.LBB3_18
.LBB3_17:
	movq	(%rbx), %rsi
.Ltmp247:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp248:
.LBB3_18:
	leaq	_ZN8NArchiveL14g_NameToPropIDE+4(%rbp), %rdx
	movq	32(%rsp), %rcx
	movq	40(%rsp), %rax
	movzwl	(%rdx), %edx
	leaq	56(%rsp), %rbx
	movq	%rcx, 16(%rsp)
	movq	%rax, 24(%rsp)
	cmpw	%dx, %cx
	jne	.LBB3_20
# BB#19:
.Ltmp258:
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK14tagPROPVARIANT
.Ltmp259:
.LBB3_28:                               # %.thread58
	xorl	%ebx, %ebx
	jmp	.LBB3_30
.LBB3_20:
	movzwl	%dx, %edx
	cmpl	$11, %edx
	je	.LBB3_25
# BB#21:
	cmpl	$17, %edx
	jne	.LBB3_29
# BB#22:
	movzwl	%cx, %ecx
	cmpl	$19, %ecx
	jne	.LBB3_29
# BB#23:
	cmpl	$255, %eax
	ja	.LBB3_29
# BB#24:
.Ltmp256:
	movzbl	%al, %esi
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEh
.Ltmp257:
	jmp	.LBB3_28
.LBB3_25:
.Ltmp252:
	leaq	15(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
.Ltmp253:
# BB#26:                                # %.noexc50
	testl	%eax, %eax
	je	.LBB3_27
.LBB3_29:
	movl	$1, %ebx
.LBB3_30:
.Ltmp263:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp264:
# BB#31:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	testl	%ebx, %ebx
	jne	.LBB3_36
.LBB3_32:
.Ltmp271:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp272:
# BB#33:                                # %.noexc
	movl	48(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
	leaq	56(%rsp), %rsi
.Ltmp273:
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp274:
# BB#34:                                # %_ZN5CPropC2ERKS_.exit.i
.Ltmp276:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp277:
# BB#35:                                # %_ZN13CObjectVectorI5CPropE3AddERKS0_.exit
	movq	16(%r14), %rax
	movslq	12(%r14), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r14)
	xorl	%r15d, %r15d
.LBB3_36:
	leaq	56(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	movl	%r15d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_27:
	movzbl	15(%rsp), %esi
.Ltmp254:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp255:
	jmp	.LBB3_28
.LBB3_47:
.Ltmp242:
	jmp	.LBB3_39
.LBB3_16:
.Ltmp251:
	jmp	.LBB3_39
.LBB3_40:
.Ltmp275:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB3_43
.LBB3_37:
.Ltmp265:
	jmp	.LBB3_42
.LBB3_38:
.Ltmp260:
.LBB3_39:
	movq	%rax, %rbx
.Ltmp261:
	leaq	32(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp262:
	jmp	.LBB3_43
.LBB3_46:
.Ltmp270:
	jmp	.LBB3_42
.LBB3_9:
.Ltmp278:
	jmp	.LBB3_42
.LBB3_41:
.Ltmp237:
.LBB3_42:
	movq	%rax, %rbx
.LBB3_43:
	leaq	56(%rsp), %rdi
.Ltmp279:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp280:
# BB#44:                                # %_ZN5CPropD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB3_45:
.Ltmp281:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN8NArchive11COutHandler8SetParamERNS_14COneMethodInfoERK11CStringBaseIwES6_, .Lfunc_end3-_ZN8NArchive11COutHandler8SetParamERNS_14COneMethodInfoERK11CStringBaseIwES6_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\277\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\266\001"              # Call site table length
	.long	.Ltmp235-.Lfunc_begin1  # >> Call Site 1 <<
	.long	.Ltmp236-.Ltmp235       #   Call between .Ltmp235 and .Ltmp236
	.long	.Ltmp237-.Lfunc_begin1  #     jumps to .Ltmp237
	.byte	0                       #   On action: cleanup
	.long	.Ltmp266-.Lfunc_begin1  # >> Call Site 2 <<
	.long	.Ltmp269-.Ltmp266       #   Call between .Ltmp266 and .Ltmp269
	.long	.Ltmp270-.Lfunc_begin1  #     jumps to .Ltmp270
	.byte	0                       #   On action: cleanup
	.long	.Ltmp243-.Lfunc_begin1  # >> Call Site 3 <<
	.long	.Ltmp244-.Ltmp243       #   Call between .Ltmp243 and .Ltmp244
	.long	.Ltmp260-.Lfunc_begin1  #     jumps to .Ltmp260
	.byte	0                       #   On action: cleanup
	.long	.Ltmp238-.Lfunc_begin1  # >> Call Site 4 <<
	.long	.Ltmp241-.Ltmp238       #   Call between .Ltmp238 and .Ltmp241
	.long	.Ltmp242-.Lfunc_begin1  #     jumps to .Ltmp242
	.byte	0                       #   On action: cleanup
	.long	.Ltmp245-.Lfunc_begin1  # >> Call Site 5 <<
	.long	.Ltmp248-.Ltmp245       #   Call between .Ltmp245 and .Ltmp248
	.long	.Ltmp251-.Lfunc_begin1  #     jumps to .Ltmp251
	.byte	0                       #   On action: cleanup
	.long	.Ltmp258-.Lfunc_begin1  # >> Call Site 6 <<
	.long	.Ltmp253-.Ltmp258       #   Call between .Ltmp258 and .Ltmp253
	.long	.Ltmp260-.Lfunc_begin1  #     jumps to .Ltmp260
	.byte	0                       #   On action: cleanup
	.long	.Ltmp263-.Lfunc_begin1  # >> Call Site 7 <<
	.long	.Ltmp264-.Ltmp263       #   Call between .Ltmp263 and .Ltmp264
	.long	.Ltmp265-.Lfunc_begin1  #     jumps to .Ltmp265
	.byte	0                       #   On action: cleanup
	.long	.Ltmp271-.Lfunc_begin1  # >> Call Site 8 <<
	.long	.Ltmp272-.Ltmp271       #   Call between .Ltmp271 and .Ltmp272
	.long	.Ltmp278-.Lfunc_begin1  #     jumps to .Ltmp278
	.byte	0                       #   On action: cleanup
	.long	.Ltmp273-.Lfunc_begin1  # >> Call Site 9 <<
	.long	.Ltmp274-.Ltmp273       #   Call between .Ltmp273 and .Ltmp274
	.long	.Ltmp275-.Lfunc_begin1  #     jumps to .Ltmp275
	.byte	0                       #   On action: cleanup
	.long	.Ltmp276-.Lfunc_begin1  # >> Call Site 10 <<
	.long	.Ltmp277-.Ltmp276       #   Call between .Ltmp276 and .Ltmp277
	.long	.Ltmp278-.Lfunc_begin1  #     jumps to .Ltmp278
	.byte	0                       #   On action: cleanup
	.long	.Ltmp277-.Lfunc_begin1  # >> Call Site 11 <<
	.long	.Ltmp254-.Ltmp277       #   Call between .Ltmp277 and .Ltmp254
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp254-.Lfunc_begin1  # >> Call Site 12 <<
	.long	.Ltmp255-.Ltmp254       #   Call between .Ltmp254 and .Ltmp255
	.long	.Ltmp260-.Lfunc_begin1  #     jumps to .Ltmp260
	.byte	0                       #   On action: cleanup
	.long	.Ltmp261-.Lfunc_begin1  # >> Call Site 13 <<
	.long	.Ltmp280-.Ltmp261       #   Call between .Ltmp261 and .Ltmp280
	.long	.Ltmp281-.Lfunc_begin1  #     jumps to .Ltmp281
	.byte	1                       #   On action: 1
	.long	.Ltmp280-.Lfunc_begin1  # >> Call Site 14 <<
	.long	.Lfunc_end3-.Ltmp280    #   Call between .Ltmp280 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchiveL15FindPropIdExactERK11CStringBaseIwE,@function
_ZN8NArchiveL15FindPropIdExactERK11CStringBaseIwE: # @_ZN8NArchiveL15FindPropIdExactERK11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
.Lcfi25:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	movl	$.L.str.19, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB4_1
# BB#2:
	movq	(%rbx), %rdi
	movl	$.L.str.20, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB4_3
# BB#5:
	movq	(%rbx), %rdi
	movl	$.L.str.21, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB4_6
# BB#7:
	movq	(%rbx), %rdi
	movl	$.L.str.22, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB4_8
# BB#9:
	movq	(%rbx), %rdi
	movl	$.L.str.23, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB4_10
# BB#11:
	movq	(%rbx), %rdi
	movl	$.L.str.24, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB4_12
# BB#13:
	movq	(%rbx), %rdi
	movl	$.L.str.25, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB4_14
# BB#15:
	movq	(%rbx), %rdi
	movl	$.L.str.26, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB4_16
# BB#17:
	movq	(%rbx), %rdi
	movl	$.L.str.27, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB4_18
# BB#19:
	movq	(%rbx), %rdi
	movl	$.L.str.28, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB4_20
# BB#21:
	movq	(%rbx), %rdi
	movl	$.L.str.29, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB4_22
# BB#23:
	movq	(%rbx), %rdi
	movl	$.L.str.30, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB4_24
# BB#25:
	movq	(%rbx), %rdi
	movl	$.L.str.31, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB4_26
# BB#27:
	movq	(%rbx), %rdi
	movl	$.L.str.32, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB4_28
# BB#29:
	movq	(%rbx), %rdi
	movl	$.L.str.33, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	cmpl	$1, %eax
	sbbl	%eax, %eax
	notl	%eax
	orl	$14, %eax
	popq	%rbx
	retq
.LBB4_1:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB4_3:
	movl	$1, %eax
	popq	%rbx
	retq
.LBB4_6:
	movl	$2, %eax
	popq	%rbx
	retq
.LBB4_8:
	movl	$3, %eax
	popq	%rbx
	retq
.LBB4_10:
	movl	$4, %eax
	popq	%rbx
	retq
.LBB4_12:
	movl	$5, %eax
	popq	%rbx
	retq
.LBB4_14:
	movl	$6, %eax
	popq	%rbx
	retq
.LBB4_16:
	movl	$7, %eax
	popq	%rbx
	retq
.LBB4_18:
	movl	$8, %eax
	popq	%rbx
	retq
.LBB4_20:
	movl	$9, %eax
	popq	%rbx
	retq
.LBB4_22:
	movl	$10, %eax
	popq	%rbx
	retq
.LBB4_24:
	movl	$11, %eax
	popq	%rbx
	retq
.LBB4_26:
	movl	$12, %eax
	popq	%rbx
	retq
.LBB4_28:
	movl	$13, %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZN8NArchiveL15FindPropIdExactERK11CStringBaseIwE, .Lfunc_end4-_ZN8NArchiveL15FindPropIdExactERK11CStringBaseIwE
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.zero	16
	.text
	.globl	_ZN8NArchive11COutHandler9SetParamsERNS_14COneMethodInfoERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NArchive11COutHandler9SetParamsERNS_14COneMethodInfoERK11CStringBaseIwE,@function
_ZN8NArchive11COutHandler9SetParamsERNS_14COneMethodInfoERK11CStringBaseIwE: # @_ZN8NArchive11COutHandler9SetParamsERNS_14COneMethodInfoERK11CStringBaseIwE
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 240
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%rsp)
	movq	$8, 72(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 48(%rsp)
.Ltmp282:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp283:
# BB#1:                                 # %.noexc
.Ltmp284:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp285:
# BB#2:                                 # %.noexc34
	movl	$0, (%rbp)
	movq	96(%rsp), %rax          # 8-byte Reload
	movslq	8(%rax), %r8
	testq	%r8, %r8
	je	.LBB5_12
# BB#3:                                 # %.preheader.i
	testl	%r8d, %r8d
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	jle	.LBB5_13
# BB#4:                                 # %.lr.ph.i
	movl	$4, %edi
	xorl	%r12d, %r12d
	movq	%rbp, %r13
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movq	%r8, 112(%rsp)          # 8-byte Spill
	jmp	.LBB5_5
.LBB5_12:                               # %.noexc34..thread.i_crit_edge
	leaq	60(%rsp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	40(%rsp), %r12          # 8-byte Reload
	jmp	.LBB5_52
.LBB5_13:
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movq	%rbp, %r15
	xorl	%ebx, %ebx
	jmp	.LBB5_46
.LBB5_14:                               # %vector.body.preheader
                                        #   in Loop: Header=BB5_5 Depth=1
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_17
# BB#15:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB5_5 Depth=1
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_16:                               # %vector.body.prol
                                        #   Parent Loop BB5_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r13,%rdi,4), %xmm0
	movups	16(%r13,%rdi,4), %xmm1
	movups	%xmm0, (%rax,%rdi,4)
	movups	%xmm1, 16(%rax,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB5_16
	jmp	.LBB5_18
.LBB5_17:                               #   in Loop: Header=BB5_5 Depth=1
	xorl	%edi, %edi
.LBB5_18:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB5_5 Depth=1
	cmpq	$24, %rdx
	jb	.LBB5_21
# BB#19:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%rax,%rdi,4), %rsi
	leaq	112(%r13,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB5_20:                               # %vector.body
                                        #   Parent Loop BB5_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB5_20
.LBB5_21:                               # %middle.block
                                        #   in Loop: Header=BB5_5 Depth=1
	cmpq	%rcx, %rbp
	jne	.LBB5_35
	jmp	.LBB5_42
	.p2align	4, 0x90
.LBB5_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_16 Depth 2
                                        #     Child Loop BB5_20 Depth 2
                                        #     Child Loop BB5_37 Depth 2
                                        #     Child Loop BB5_40 Depth 2
                                        #     Child Loop BB5_9 Depth 2
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movl	(%rax,%r12,4), %ebp
	cmpl	$58, %ebp
	jne	.LBB5_22
# BB#6:                                 #   in Loop: Header=BB5_5 Depth=1
.Ltmp288:
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %r12
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp289:
# BB#7:                                 # %.noexc30.i
                                        #   in Loop: Header=BB5_5 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movq	%rbx, %r15
	movq	%rbx, %rbp
	leal	1(%rbp), %ebp
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp290:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp291:
# BB#8:                                 # %.noexc.i26.i
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%ebp, 12(%r14)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_9:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i27.i
                                        #   Parent Loop BB5_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB5_9
# BB#10:                                #   in Loop: Header=BB5_5 Depth=1
	movl	%r15d, 8(%r14)
.Ltmp293:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp294:
# BB#11:                                #   in Loop: Header=BB5_5 Depth=1
	movq	64(%rsp), %rax
	movslq	60(%rsp), %rcx
	movq	%r14, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 60(%rsp)
	xorl	%ebx, %ebx
	movq	%r13, %r15
	movq	112(%rsp), %r8          # 8-byte Reload
	movq	%r12, %rdi
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB5_45
	.p2align	4, 0x90
.LBB5_22:                               #   in Loop: Header=BB5_5 Depth=1
	movl	%edi, %eax
	subl	%ebx, %eax
	cmpl	$1, %eax
	jg	.LBB5_26
# BB#23:                                #   in Loop: Header=BB5_5 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$8, %edi
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %edi
	jl	.LBB5_25
# BB#24:                                # %select.true.sink
                                        #   in Loop: Header=BB5_5 Depth=1
	movl	%edi, %edx
	shrl	$31, %edx
	addl	%edi, %edx
	sarl	%edx
.LBB5_25:                               # %select.end
                                        #   in Loop: Header=BB5_5 Depth=1
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rdi,%rsi), %r14d
	cmpl	%edi, %r14d
	jne	.LBB5_27
.LBB5_26:                               #   in Loop: Header=BB5_5 Depth=1
	movl	%edi, %r14d
	movq	80(%rsp), %rax          # 8-byte Reload
	jmp	.LBB5_44
.LBB5_27:                               #   in Loop: Header=BB5_5 Depth=1
	movq	%r12, %r15
	movq	%rdi, %r12
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movl	%ebp, 32(%rsp)          # 4-byte Spill
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp286:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp287:
# BB#28:                                # %.noexc36.i
                                        #   in Loop: Header=BB5_5 Depth=1
	testl	%r12d, %r12d
	movl	32(%rsp), %ebp          # 4-byte Reload
	movq	104(%rsp), %rbx         # 8-byte Reload
	movq	%r15, %r12
	jle	.LBB5_43
# BB#29:                                # %.preheader.i.i.i
                                        #   in Loop: Header=BB5_5 Depth=1
	testl	%ebx, %ebx
	jle	.LBB5_41
# BB#30:                                # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB5_5 Depth=1
	movslq	%ebx, %rbp
	cmpl	$7, %ebx
	jbe	.LBB5_34
# BB#31:                                # %min.iters.checked
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	%rbp, %rcx
	andq	$-8, %rcx
	je	.LBB5_34
# BB#32:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_5 Depth=1
	leaq	(%r13,%rbp,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB5_14
# BB#33:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_5 Depth=1
	leaq	(%rax,%rbp,4), %rdx
	cmpq	%rdx, 88(%rsp)          # 8-byte Folded Reload
	jae	.LBB5_14
.LBB5_34:                               #   in Loop: Header=BB5_5 Depth=1
	xorl	%ecx, %ecx
.LBB5_35:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB5_5 Depth=1
	movl	%ebx, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB5_38
# BB#36:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB5_5 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB5_37:                               # %scalar.ph.prol
                                        #   Parent Loop BB5_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13,%rcx,4), %edi
	movl	%edi, (%rax,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB5_37
.LBB5_38:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB5_5 Depth=1
	cmpq	$7, %rdx
	jb	.LBB5_42
# BB#39:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB5_5 Depth=1
	subq	%rcx, %rbp
	leaq	28(%rax,%rcx,4), %rdx
	leaq	28(%r13,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB5_40:                               # %scalar.ph
                                        #   Parent Loop BB5_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB5_40
	jmp	.LBB5_42
.LBB5_41:                               # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB5_5 Depth=1
	testq	%r13, %r13
	je	.LBB5_43
.LBB5_42:                               # %._crit_edge.thread.i.i.i
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movq	104(%rsp), %rbx         # 8-byte Reload
	movl	32(%rsp), %ebp          # 4-byte Reload
.LBB5_43:                               # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB5_5 Depth=1
	movslq	%ebx, %rcx
	movl	$0, (%rax,%rcx,4)
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rax, %r13
	movq	112(%rsp), %r8          # 8-byte Reload
.LBB5_44:                               # %_ZN11CStringBaseIwEpLEw.exit.i
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	%r13, %r15
	movslq	%ebx, %rcx
	movl	%ebp, (%r15,%rcx,4)
	incl	%ebx
	leaq	4(%r15,%rcx,4), %r13
	movl	%r14d, %edi
	movq	%rax, 80(%rsp)          # 8-byte Spill
.LBB5_45:                               #   in Loop: Header=BB5_5 Depth=1
	movl	$0, (%r13)
	incq	%r12
	cmpq	%r8, %r12
	movq	%r15, %r13
	jl	.LBB5_5
.LBB5_46:                               # %._crit_edge.i
.Ltmp296:
	movq	%rbx, %r13
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp297:
	movq	40(%rsp), %r12          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
# BB#47:                                # %.noexc.i
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	leal	1(%r13), %r14d
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp298:
	callq	_Znam
.Ltmp299:
# BB#48:                                # %.noexc.i.i
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%r14d, 12(%rbx)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_49:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB5_49
# BB#50:
	movl	%r13d, 8(%rbx)
.Ltmp301:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp302:
# BB#51:
	movq	64(%rsp), %rax
	leaq	60(%rsp), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movslq	60(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %ecx
	movl	%ecx, 60(%rsp)
	testq	%r15, %r15
	je	.LBB5_53
.LBB5_52:                               # %.thread.i
	movq	%rbp, %rdi
	callq	_ZdaPv
	movl	60(%rsp), %ecx
.LBB5_53:                               # %_ZN8NArchiveL11SplitParamsERK11CStringBaseIwER13CObjectVectorIS1_E.exit
	testl	%ecx, %ecx
	jle	.LBB5_66
# BB#54:
	movq	64(%rsp), %rax
	movq	(%rax), %r14
	leaq	32(%r12), %rax
	cmpq	%rax, %r14
	je	.LBB5_66
# BB#55:
	movl	$0, 40(%r12)
	movq	32(%r12), %rbx
	movl	$0, (%rbx)
	movslq	8(%r14), %rbp
	incq	%rbp
	movl	44(%r12), %r12d
	cmpl	%r12d, %ebp
	jne	.LBB5_57
# BB#56:
	movq	40(%rsp), %r12          # 8-byte Reload
	jmp	.LBB5_63
.LBB5_57:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp304:
	callq	_Znam
	movq	%rax, %r15
.Ltmp305:
# BB#58:                                # %.noexc36
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB5_61
# BB#59:                                # %.noexc36
	testl	%r12d, %r12d
	movq	40(%rsp), %r12          # 8-byte Reload
	jle	.LBB5_62
# BB#60:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	40(%r12), %rax
	jmp	.LBB5_62
.LBB5_61:
	movq	40(%rsp), %r12          # 8-byte Reload
.LBB5_62:                               # %._crit_edge16.i.i
	movq	%r15, 32(%r12)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 44(%r12)
	movq	%r15, %rbx
.LBB5_63:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB5_64:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB5_64
# BB#65:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%r14), %eax
	movl	%eax, 40(%r12)
.LBB5_66:                               # %_ZN11CStringBaseIwEaSERKS0_.exit.preheader
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpl	$2, (%rax)
	jl	.LBB5_144
# BB#67:                                # %.lr.ph
	movl	$1, %ecx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB5_68:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_72 Depth 2
                                        #     Child Loop BB5_85 Depth 2
                                        #     Child Loop BB5_98 Depth 2
                                        #     Child Loop BB5_120 Depth 2
                                        #     Child Loop BB5_132 Depth 2
                                        #     Child Loop BB5_103 Depth 2
                                        #     Child Loop BB5_115 Depth 2
	movq	64(%rsp), %rax
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %r13
	movaps	%xmm0, (%rsp)
.Ltmp307:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp308:
# BB#69:                                #   in Loop: Header=BB5_68 Depth=1
	movq	%rbx, (%rsp)
	movl	$0, (%rbx)
	movl	$4, 12(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
.Ltmp310:
	movl	$16, %edi
	callq	_Znam
.Ltmp311:
# BB#70:                                #   in Loop: Header=BB5_68 Depth=1
	movq	%rax, 16(%rsp)
	movl	$0, (%rax)
	movl	$4, 28(%rsp)
	movq	(%r13), %rax
	movl	(%rax), %ecx
	cmpl	$61, %ecx
	movq	%rax, %r15
	je	.LBB5_74
# BB#71:                                # %.lr.ph.i.i.i42.preheader
                                        #   in Loop: Header=BB5_68 Depth=1
	movl	%ecx, %edx
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB5_72:                               # %.lr.ph.i.i.i42
                                        #   Parent Loop BB5_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%edx, %edx
	je	.LBB5_81
# BB#73:                                #   in Loop: Header=BB5_72 Depth=2
	movl	4(%r15), %edx
	addq	$4, %r15
	cmpl	$61, %edx
	jne	.LBB5_72
.LBB5_74:                               # %_ZNK11CStringBaseIwE4FindEw.exit.i
                                        #   in Loop: Header=BB5_68 Depth=1
	subq	%rax, %r15
	shrq	$2, %r15
	testl	%r15d, %r15d
	js	.LBB5_81
# BB#75:                                #   in Loop: Header=BB5_68 Depth=1
.Ltmp325:
	xorl	%edx, %edx
	leaq	168(%rsp), %rdi
	movq	%r13, %rsi
	movl	%r15d, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp326:
# BB#76:                                # %.noexc51
                                        #   in Loop: Header=BB5_68 Depth=1
	movl	$0, 8(%rsp)
	movq	(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	176(%rsp), %rbp
	incq	%rbp
	movl	12(%rsp), %r14d
	cmpl	%r14d, %ebp
	je	.LBB5_102
# BB#77:                                #   in Loop: Header=BB5_68 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp327:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp328:
# BB#78:                                # %.noexc.i44
                                        #   in Loop: Header=BB5_68 Depth=1
	testq	%rbx, %rbx
	je	.LBB5_100
# BB#79:                                # %.noexc.i44
                                        #   in Loop: Header=BB5_68 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	jle	.LBB5_101
# BB#80:                                # %._crit_edge.thread.i.i44.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%rsp), %rax
	jmp	.LBB5_101
	.p2align	4, 0x90
.LBB5_81:                               # %_ZNK11CStringBaseIwE4FindEw.exit.thread.preheader.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movslq	8(%r13), %rdx
	testq	%rdx, %rdx
	jle	.LBB5_94
# BB#82:                                # %.lr.ph.i43
                                        #   in Loop: Header=BB5_68 Depth=1
	addl	$-48, %ecx
	cmpl	$10, %ecx
	jae	.LBB5_84
# BB#83:                                #   in Loop: Header=BB5_68 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB5_88
.LBB5_84:                               # %_ZNK11CStringBaseIwE4FindEw.exit.thread.i.preheader
                                        #   in Loop: Header=BB5_68 Depth=1
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB5_85:                               # %_ZNK11CStringBaseIwE4FindEw.exit.thread.i
                                        #   Parent Loop BB5_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdx, %rbp
	jge	.LBB5_94
# BB#86:                                # %_ZNK11CStringBaseIwE4FindEw.exit.thread._crit_edge98.i
                                        #   in Loop: Header=BB5_85 Depth=2
	movl	(%rax,%rbp,4), %ecx
	addl	$-48, %ecx
	incq	%rbp
	cmpl	$10, %ecx
	jae	.LBB5_85
# BB#87:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB5_68 Depth=1
	decq	%rbp
.LBB5_88:                               # %._crit_edge
                                        #   in Loop: Header=BB5_68 Depth=1
.Ltmp315:
	xorl	%edx, %edx
	leaq	136(%rsp), %rdi
	movq	%r13, %rsi
	movl	%ebp, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp316:
# BB#89:                                # %.noexc53
                                        #   in Loop: Header=BB5_68 Depth=1
	movl	$0, 8(%rsp)
	movq	(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	144(%rsp), %r15
	incq	%r15
	movl	12(%rsp), %r14d
	cmpl	%r14d, %r15d
	je	.LBB5_119
# BB#90:                                #   in Loop: Header=BB5_68 Depth=1
	movq	%r15, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp317:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp318:
# BB#91:                                # %.noexc74.i
                                        #   in Loop: Header=BB5_68 Depth=1
	testq	%rbx, %rbx
	je	.LBB5_117
# BB#92:                                # %.noexc74.i
                                        #   in Loop: Header=BB5_68 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	jle	.LBB5_118
# BB#93:                                # %._crit_edge.thread.i.i68.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%rsp), %rax
	jmp	.LBB5_118
	.p2align	4, 0x90
.LBB5_94:                               # %_ZNK11CStringBaseIwE4FindEw.exit.thread._crit_edge.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	%rsp, %rcx
	cmpq	%rcx, %r13
	je	.LBB5_136
# BB#95:                                #   in Loop: Header=BB5_68 Depth=1
	movl	$0, 8(%rsp)
	movl	$0, (%rbx)
	movslq	8(%r13), %rbp
	incq	%rbp
	cmpl	$4, %ebp
	je	.LBB5_98
# BB#96:                                #   in Loop: Header=BB5_68 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp313:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp314:
# BB#97:                                # %._crit_edge16.i.i.i50
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%rsp), %rax
	movq	%r15, (%rsp)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 12(%rsp)
	movq	(%r13), %rax
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB5_98:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   Parent Loop BB5_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB5_98
# BB#99:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movl	8(%r13), %eax
	movl	%eax, 8(%rsp)
	jmp	.LBB5_136
.LBB5_100:                              #   in Loop: Header=BB5_68 Depth=1
	xorl	%eax, %eax
.LBB5_101:                              # %._crit_edge16.i.i45.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	%r12, (%rsp)
	movl	$0, (%r12,%rax,4)
	movl	%ebp, 12(%rsp)
	movq	%r12, %rbx
	movq	40(%rsp), %r12          # 8-byte Reload
.LBB5_102:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i46.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	168(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_103:                              #   Parent Loop BB5_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB5_103
# BB#104:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i49.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movl	176(%rsp), %eax
	movl	%eax, 8(%rsp)
	testq	%rdi, %rdi
	je	.LBB5_106
# BB#105:                               #   in Loop: Header=BB5_68 Depth=1
	callq	_ZdaPv
.LBB5_106:                              # %_ZN11CStringBaseIwED2Ev.exit.i45
                                        #   in Loop: Header=BB5_68 Depth=1
	incl	%r15d
	movl	8(%r13), %ecx
	subl	%r15d, %ecx
.Ltmp330:
	leaq	152(%rsp), %rdi
	movq	%r13, %rsi
	movl	%r15d, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp331:
# BB#107:                               # %.noexc52
                                        #   in Loop: Header=BB5_68 Depth=1
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	160(%rsp), %rbp
	incq	%rbp
	movl	28(%rsp), %r14d
	cmpl	%r14d, %ebp
	je	.LBB5_114
# BB#108:                               #   in Loop: Header=BB5_68 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp332:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp333:
# BB#109:                               # %.noexc60.i
                                        #   in Loop: Header=BB5_68 Depth=1
	testq	%rbx, %rbx
	je	.LBB5_112
# BB#110:                               # %.noexc60.i
                                        #   in Loop: Header=BB5_68 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	jle	.LBB5_113
# BB#111:                               # %._crit_edge.thread.i.i54.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	24(%rsp), %rax
	jmp	.LBB5_113
.LBB5_112:                              #   in Loop: Header=BB5_68 Depth=1
	xorl	%eax, %eax
.LBB5_113:                              # %._crit_edge16.i.i55.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	%r15, 16(%rsp)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 28(%rsp)
	movq	%r15, %rbx
.LBB5_114:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i56.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	152(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_115:                              #   Parent Loop BB5_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB5_115
# BB#116:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i59.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movl	160(%rsp), %eax
	jmp	.LBB5_134
.LBB5_117:                              #   in Loop: Header=BB5_68 Depth=1
	xorl	%eax, %eax
.LBB5_118:                              # %._crit_edge16.i.i69.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	%r12, (%rsp)
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%rsp)
	movq	%r12, %rbx
	movq	40(%rsp), %r12          # 8-byte Reload
.LBB5_119:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i70.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	136(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_120:                              #   Parent Loop BB5_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB5_120
# BB#121:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i73.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movl	144(%rsp), %eax
	movl	%eax, 8(%rsp)
	testq	%rdi, %rdi
	je	.LBB5_123
# BB#122:                               #   in Loop: Header=BB5_68 Depth=1
	callq	_ZdaPv
.LBB5_123:                              # %_ZN11CStringBaseIwED2Ev.exit76.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movl	8(%r13), %ecx
	subl	%ebp, %ecx
.Ltmp320:
	leaq	120(%rsp), %rdi
	movq	%r13, %rsi
	movl	%ebp, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp321:
# BB#124:                               # %.noexc54
                                        #   in Loop: Header=BB5_68 Depth=1
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	128(%rsp), %rbp
	incq	%rbp
	movl	28(%rsp), %r14d
	cmpl	%r14d, %ebp
	je	.LBB5_131
# BB#125:                               #   in Loop: Header=BB5_68 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp322:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp323:
# BB#126:                               # %.noexc86.i
                                        #   in Loop: Header=BB5_68 Depth=1
	testq	%rbx, %rbx
	je	.LBB5_129
# BB#127:                               # %.noexc86.i
                                        #   in Loop: Header=BB5_68 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	jle	.LBB5_130
# BB#128:                               # %._crit_edge.thread.i.i80.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	24(%rsp), %rax
	jmp	.LBB5_130
.LBB5_129:                              #   in Loop: Header=BB5_68 Depth=1
	xorl	%eax, %eax
.LBB5_130:                              # %._crit_edge16.i.i81.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	%r15, 16(%rsp)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 28(%rsp)
	movq	%r15, %rbx
.LBB5_131:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i82.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	120(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_132:                              #   Parent Loop BB5_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB5_132
# BB#133:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i85.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movl	128(%rsp), %eax
.LBB5_134:                              # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i85.i
                                        #   in Loop: Header=BB5_68 Depth=1
	movl	%eax, 24(%rsp)
	testq	%rdi, %rdi
	je	.LBB5_136
# BB#135:                               #   in Loop: Header=BB5_68 Depth=1
	callq	_ZdaPv
.LBB5_136:                              # %_ZN8NArchiveL10SplitParamERK11CStringBaseIwERS1_S4_.exit
                                        #   in Loop: Header=BB5_68 Depth=1
.Ltmp335:
	movq	%r12, %rsi
	movq	%rsp, %rdx
	leaq	16(%rsp), %rcx
	callq	_ZN8NArchive11COutHandler8SetParamERNS_14COneMethodInfoERK11CStringBaseIwES6_
	movl	%eax, %ebx
.Ltmp336:
# BB#137:                               #   in Loop: Header=BB5_68 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_139
# BB#138:                               #   in Loop: Header=BB5_68 Depth=1
	callq	_ZdaPv
.LBB5_139:                              # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_141
# BB#140:                               #   in Loop: Header=BB5_68 Depth=1
	callq	_ZdaPv
.LBB5_141:                              # %_ZN11CStringBaseIwED2Ev.exit58
                                        #   in Loop: Header=BB5_68 Depth=1
	testl	%ebx, %ebx
	jne	.LBB5_145
# BB#142:                               # %_ZN11CStringBaseIwEaSERKS0_.exit
                                        #   in Loop: Header=BB5_68 Depth=1
	movq	96(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	(%rax), %rax
	cmpq	%rax, %rcx
	xorps	%xmm0, %xmm0
	jl	.LBB5_68
# BB#143:
	movl	$2, %ebp
	jmp	.LBB5_146
.LBB5_144:
                                        # implicit-def: %EBX
	movl	$2, %ebp
	jmp	.LBB5_146
.LBB5_145:
	movl	$1, %ebp
.LBB5_146:                              # %_ZN11CStringBaseIwEaSERKS0_.exit._crit_edge
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 48(%rsp)
.Ltmp347:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp348:
# BB#147:                               # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	xorl	%eax, %eax
	cmpl	$2, %ebp
	cmovel	%eax, %ebx
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	movl	%ebx, %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_148:
.Ltmp300:
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB5_156
.LBB5_149:
.Ltmp349:
	movq	%rax, %rbp
.Ltmp350:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp351:
	jmp	.LBB5_176
.LBB5_150:
.Ltmp352:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_151:
.Ltmp324:
	movq	%rax, %rbp
	movq	120(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_169
# BB#152:
	callq	_ZdaPv
	jmp	.LBB5_169
.LBB5_153:
.Ltmp319:
	movq	%rax, %rbp
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_169
# BB#154:
	callq	_ZdaPv
	jmp	.LBB5_169
.LBB5_155:
.Ltmp303:
	movq	%rax, %rbp
.LBB5_156:                              # %.body32.i
	movq	%r15, %r13
	testq	%r13, %r13
	jne	.LBB5_167
	jmp	.LBB5_174
.LBB5_157:
.Ltmp334:
	movq	%rax, %rbp
	movq	152(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_169
# BB#158:
	callq	_ZdaPv
	jmp	.LBB5_169
.LBB5_159:
.Ltmp329:
	movq	%rax, %rbp
	movq	168(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_169
# BB#160:
	callq	_ZdaPv
	jmp	.LBB5_169
.LBB5_161:
.Ltmp306:
	jmp	.LBB5_165
.LBB5_162:
.Ltmp292:
	movq	%rax, %rbp
	movq	%r14, %rdi
	callq	_ZdlPv
	testq	%r13, %r13
	jne	.LBB5_167
	jmp	.LBB5_174
.LBB5_163:                              # %.thread
.Ltmp312:
	movq	%rax, %rbp
	jmp	.LBB5_172
.LBB5_164:
.Ltmp309:
.LBB5_165:
	movq	%rax, %rbp
	jmp	.LBB5_174
.LBB5_166:
.Ltmp295:
	movq	%rax, %rbp
	testq	%r13, %r13
	je	.LBB5_174
.LBB5_167:
	movq	80(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB5_173
.LBB5_168:
.Ltmp337:
	movq	%rax, %rbp
.LBB5_169:                              # %.body56
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_171
# BB#170:
	callq	_ZdaPv
.LBB5_171:
	movq	(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB5_174
.LBB5_172:
	movq	%rbx, %rdi
.LBB5_173:
	callq	_ZdaPv
.LBB5_174:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 48(%rsp)
.Ltmp338:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp339:
# BB#175:
.Ltmp344:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp345:
.LBB5_176:                              # %unwind_resume
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB5_177:
.Ltmp346:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB5_178:
.Ltmp340:
	movq	%rax, %rbx
.Ltmp341:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp342:
# BB#179:                               # %.body62
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB5_180:
.Ltmp343:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN8NArchive11COutHandler9SetParamsERNS_14COneMethodInfoERK11CStringBaseIwE, .Lfunc_end5-_ZN8NArchive11COutHandler9SetParamsERNS_14COneMethodInfoERK11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\333\202"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\322\002"              # Call site table length
	.long	.Ltmp282-.Lfunc_begin2  # >> Call Site 1 <<
	.long	.Ltmp285-.Ltmp282       #   Call between .Ltmp282 and .Ltmp285
	.long	.Ltmp306-.Lfunc_begin2  #     jumps to .Ltmp306
	.byte	0                       #   On action: cleanup
	.long	.Ltmp288-.Lfunc_begin2  # >> Call Site 2 <<
	.long	.Ltmp289-.Ltmp288       #   Call between .Ltmp288 and .Ltmp289
	.long	.Ltmp295-.Lfunc_begin2  #     jumps to .Ltmp295
	.byte	0                       #   On action: cleanup
	.long	.Ltmp290-.Lfunc_begin2  # >> Call Site 3 <<
	.long	.Ltmp291-.Ltmp290       #   Call between .Ltmp290 and .Ltmp291
	.long	.Ltmp292-.Lfunc_begin2  #     jumps to .Ltmp292
	.byte	0                       #   On action: cleanup
	.long	.Ltmp293-.Lfunc_begin2  # >> Call Site 4 <<
	.long	.Ltmp287-.Ltmp293       #   Call between .Ltmp293 and .Ltmp287
	.long	.Ltmp295-.Lfunc_begin2  #     jumps to .Ltmp295
	.byte	0                       #   On action: cleanup
	.long	.Ltmp296-.Lfunc_begin2  # >> Call Site 5 <<
	.long	.Ltmp297-.Ltmp296       #   Call between .Ltmp296 and .Ltmp297
	.long	.Ltmp303-.Lfunc_begin2  #     jumps to .Ltmp303
	.byte	0                       #   On action: cleanup
	.long	.Ltmp298-.Lfunc_begin2  # >> Call Site 6 <<
	.long	.Ltmp299-.Ltmp298       #   Call between .Ltmp298 and .Ltmp299
	.long	.Ltmp300-.Lfunc_begin2  #     jumps to .Ltmp300
	.byte	0                       #   On action: cleanup
	.long	.Ltmp301-.Lfunc_begin2  # >> Call Site 7 <<
	.long	.Ltmp302-.Ltmp301       #   Call between .Ltmp301 and .Ltmp302
	.long	.Ltmp303-.Lfunc_begin2  #     jumps to .Ltmp303
	.byte	0                       #   On action: cleanup
	.long	.Ltmp304-.Lfunc_begin2  # >> Call Site 8 <<
	.long	.Ltmp305-.Ltmp304       #   Call between .Ltmp304 and .Ltmp305
	.long	.Ltmp306-.Lfunc_begin2  #     jumps to .Ltmp306
	.byte	0                       #   On action: cleanup
	.long	.Ltmp307-.Lfunc_begin2  # >> Call Site 9 <<
	.long	.Ltmp308-.Ltmp307       #   Call between .Ltmp307 and .Ltmp308
	.long	.Ltmp309-.Lfunc_begin2  #     jumps to .Ltmp309
	.byte	0                       #   On action: cleanup
	.long	.Ltmp310-.Lfunc_begin2  # >> Call Site 10 <<
	.long	.Ltmp311-.Ltmp310       #   Call between .Ltmp310 and .Ltmp311
	.long	.Ltmp312-.Lfunc_begin2  #     jumps to .Ltmp312
	.byte	0                       #   On action: cleanup
	.long	.Ltmp325-.Lfunc_begin2  # >> Call Site 11 <<
	.long	.Ltmp326-.Ltmp325       #   Call between .Ltmp325 and .Ltmp326
	.long	.Ltmp337-.Lfunc_begin2  #     jumps to .Ltmp337
	.byte	0                       #   On action: cleanup
	.long	.Ltmp327-.Lfunc_begin2  # >> Call Site 12 <<
	.long	.Ltmp328-.Ltmp327       #   Call between .Ltmp327 and .Ltmp328
	.long	.Ltmp329-.Lfunc_begin2  #     jumps to .Ltmp329
	.byte	0                       #   On action: cleanup
	.long	.Ltmp315-.Lfunc_begin2  # >> Call Site 13 <<
	.long	.Ltmp316-.Ltmp315       #   Call between .Ltmp315 and .Ltmp316
	.long	.Ltmp337-.Lfunc_begin2  #     jumps to .Ltmp337
	.byte	0                       #   On action: cleanup
	.long	.Ltmp317-.Lfunc_begin2  # >> Call Site 14 <<
	.long	.Ltmp318-.Ltmp317       #   Call between .Ltmp317 and .Ltmp318
	.long	.Ltmp319-.Lfunc_begin2  #     jumps to .Ltmp319
	.byte	0                       #   On action: cleanup
	.long	.Ltmp313-.Lfunc_begin2  # >> Call Site 15 <<
	.long	.Ltmp331-.Ltmp313       #   Call between .Ltmp313 and .Ltmp331
	.long	.Ltmp337-.Lfunc_begin2  #     jumps to .Ltmp337
	.byte	0                       #   On action: cleanup
	.long	.Ltmp332-.Lfunc_begin2  # >> Call Site 16 <<
	.long	.Ltmp333-.Ltmp332       #   Call between .Ltmp332 and .Ltmp333
	.long	.Ltmp334-.Lfunc_begin2  #     jumps to .Ltmp334
	.byte	0                       #   On action: cleanup
	.long	.Ltmp320-.Lfunc_begin2  # >> Call Site 17 <<
	.long	.Ltmp321-.Ltmp320       #   Call between .Ltmp320 and .Ltmp321
	.long	.Ltmp337-.Lfunc_begin2  #     jumps to .Ltmp337
	.byte	0                       #   On action: cleanup
	.long	.Ltmp322-.Lfunc_begin2  # >> Call Site 18 <<
	.long	.Ltmp323-.Ltmp322       #   Call between .Ltmp322 and .Ltmp323
	.long	.Ltmp324-.Lfunc_begin2  #     jumps to .Ltmp324
	.byte	0                       #   On action: cleanup
	.long	.Ltmp335-.Lfunc_begin2  # >> Call Site 19 <<
	.long	.Ltmp336-.Ltmp335       #   Call between .Ltmp335 and .Ltmp336
	.long	.Ltmp337-.Lfunc_begin2  #     jumps to .Ltmp337
	.byte	0                       #   On action: cleanup
	.long	.Ltmp347-.Lfunc_begin2  # >> Call Site 20 <<
	.long	.Ltmp348-.Ltmp347       #   Call between .Ltmp347 and .Ltmp348
	.long	.Ltmp349-.Lfunc_begin2  #     jumps to .Ltmp349
	.byte	0                       #   On action: cleanup
	.long	.Ltmp348-.Lfunc_begin2  # >> Call Site 21 <<
	.long	.Ltmp350-.Ltmp348       #   Call between .Ltmp348 and .Ltmp350
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp350-.Lfunc_begin2  # >> Call Site 22 <<
	.long	.Ltmp351-.Ltmp350       #   Call between .Ltmp350 and .Ltmp351
	.long	.Ltmp352-.Lfunc_begin2  #     jumps to .Ltmp352
	.byte	1                       #   On action: 1
	.long	.Ltmp338-.Lfunc_begin2  # >> Call Site 23 <<
	.long	.Ltmp339-.Ltmp338       #   Call between .Ltmp338 and .Ltmp339
	.long	.Ltmp340-.Lfunc_begin2  #     jumps to .Ltmp340
	.byte	1                       #   On action: 1
	.long	.Ltmp344-.Lfunc_begin2  # >> Call Site 24 <<
	.long	.Ltmp345-.Ltmp344       #   Call between .Ltmp344 and .Ltmp345
	.long	.Ltmp346-.Lfunc_begin2  #     jumps to .Ltmp346
	.byte	1                       #   On action: 1
	.long	.Ltmp345-.Lfunc_begin2  # >> Call Site 25 <<
	.long	.Ltmp341-.Ltmp345       #   Call between .Ltmp345 and .Ltmp341
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp341-.Lfunc_begin2  # >> Call Site 26 <<
	.long	.Ltmp342-.Ltmp341       #   Call between .Ltmp341 and .Ltmp342
	.long	.Ltmp343-.Lfunc_begin2  #     jumps to .Ltmp343
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED2Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED2Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 32
.Lcfi42:
	.cfi_offset %rbx, -24
.Lcfi43:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp353:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp354:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB6_2:
.Ltmp355:
	movq	%rax, %r14
.Ltmp356:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp357:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_4:
.Ltmp358:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED2Ev, .Lfunc_end6-_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp353-.Lfunc_begin3  # >> Call Site 1 <<
	.long	.Ltmp354-.Ltmp353       #   Call between .Ltmp353 and .Ltmp354
	.long	.Ltmp355-.Lfunc_begin3  #     jumps to .Ltmp355
	.byte	0                       #   On action: cleanup
	.long	.Ltmp354-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp356-.Ltmp354       #   Call between .Ltmp354 and .Ltmp356
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp356-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Ltmp357-.Ltmp356       #   Call between .Ltmp356 and .Ltmp357
	.long	.Ltmp358-.Lfunc_begin3  #     jumps to .Ltmp358
	.byte	1                       #   On action: 1
	.long	.Ltmp357-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp357    #   Call between .Ltmp357 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive11COutHandler16SetSolidSettingsERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NArchive11COutHandler16SetSolidSettingsERK11CStringBaseIwE,@function
_ZN8NArchive11COutHandler16SetSolidSettingsERK11CStringBaseIwE: # @_ZN8NArchive11COutHandler16SetSolidSettingsERK11CStringBaseIwE
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 64
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movslq	8(%r15), %r12
	leaq	1(%r12), %rax
	testl	%eax, %eax
	je	.LBB7_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	movl	$0, (%rbx)
	jmp	.LBB7_3
.LBB7_1:
	xorl	%ebx, %ebx
.LBB7_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_4:                                # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rcx), %edx
	movl	%edx, (%rbx,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB7_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
.Ltmp359:
	movq	%rbx, %rdi
	callq	_Z13MyStringUpperPw
.Ltmp360:
# BB#6:                                 # %_ZN11CStringBaseIwE9MakeUpperEv.exit.preheader
	testl	%r12d, %r12d
	jle	.LBB7_7
# BB#8:                                 # %.lr.ph
	xorl	%r13d, %r13d
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB7_9:                                # =>This Inner Loop Header: Depth=1
	movslq	%r13d, %rax
	leaq	(%rbx,%rax,4), %rbp
.Ltmp362:
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	_Z21ConvertStringToUInt64PKwPS0_
.Ltmp363:
# BB#10:                                #   in Loop: Header=BB7_9 Depth=1
	movq	(%rsp), %rcx
	cmpq	%rcx, %rbp
	je	.LBB7_11
# BB#13:                                #   in Loop: Header=BB7_9 Depth=1
	subq	%rbp, %rcx
	shrq	$2, %rcx
	addl	%r13d, %ecx
	cmpl	%r12d, %ecx
	je	.LBB7_23
# BB#14:                                #   in Loop: Header=BB7_9 Depth=1
	movslq	%ecx, %rdx
	movl	(%rbx,%rdx,4), %edx
	addl	$-66, %edx
	cmpl	$11, %edx
	ja	.LBB7_23
# BB#15:                                #   in Loop: Header=BB7_9 Depth=1
	incl	%ecx
	jmpq	*.LJTI7_0(,%rdx,8)
.LBB7_16:                               #   in Loop: Header=BB7_9 Depth=1
	testq	%rax, %rax
	movl	$1, %edx
	cmoveq	%rdx, %rax
	movq	%rax, 48(%r14)
	jmp	.LBB7_21
	.p2align	4, 0x90
.LBB7_11:                               #   in Loop: Header=BB7_9 Depth=1
	cmpl	$69, (%rbp)
	jne	.LBB7_23
# BB#12:                                #   in Loop: Header=BB7_9 Depth=1
	incl	%r13d
	movb	$1, 65(%r14)
	movl	%r13d, %ecx
	jmp	.LBB7_21
.LBB7_19:                               #   in Loop: Header=BB7_9 Depth=1
	shlq	$30, %rax
	jmp	.LBB7_20
.LBB7_17:                               #   in Loop: Header=BB7_9 Depth=1
	shlq	$10, %rax
	jmp	.LBB7_20
.LBB7_18:                               #   in Loop: Header=BB7_9 Depth=1
	shlq	$20, %rax
.LBB7_20:                               # %.thread
                                        #   in Loop: Header=BB7_9 Depth=1
	movq	%rax, 56(%r14)
	movb	$1, 64(%r14)
.LBB7_21:                               # %.thread
                                        #   in Loop: Header=BB7_9 Depth=1
	cmpl	%r12d, %ecx
	movl	%ecx, %r13d
	jl	.LBB7_9
# BB#22:
	xorl	%ebp, %ebp
	jmp	.LBB7_25
.LBB7_23:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	testq	%rbx, %rbx
	jne	.LBB7_25
	jmp	.LBB7_26
.LBB7_7:
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	je	.LBB7_26
.LBB7_25:                               # %.loopexit.thread
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB7_26:                               # %_ZN11CStringBaseIwED2Ev.exit40
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_27:
.Ltmp361:
	jmp	.LBB7_28
.LBB7_31:
.Ltmp364:
.LBB7_28:
	movq	%rax, %rbp
	testq	%rbx, %rbx
	je	.LBB7_30
# BB#29:
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB7_30:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN8NArchive11COutHandler16SetSolidSettingsERK11CStringBaseIwE, .Lfunc_end7-_ZN8NArchive11COutHandler16SetSolidSettingsERK11CStringBaseIwE
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_20
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_16
	.quad	.LBB7_19
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_17
	.quad	.LBB7_23
	.quad	.LBB7_18
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp359-.Lfunc_begin4  #   Call between .Lfunc_begin4 and .Ltmp359
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp359-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp360-.Ltmp359       #   Call between .Ltmp359 and .Ltmp360
	.long	.Ltmp361-.Lfunc_begin4  #     jumps to .Ltmp361
	.byte	0                       #   On action: cleanup
	.long	.Ltmp362-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp363-.Ltmp362       #   Call between .Ltmp362 and .Ltmp363
	.long	.Ltmp364-.Lfunc_begin4  #     jumps to .Ltmp364
	.byte	0                       #   On action: cleanup
	.long	.Ltmp363-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Lfunc_end7-.Ltmp363    #   Call between .Ltmp363 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NArchive11COutHandler16SetSolidSettingsERK14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive11COutHandler16SetSolidSettingsERK14tagPROPVARIANT,@function
_ZN8NArchive11COutHandler16SetSolidSettingsERK14tagPROPVARIANT: # @_ZN8NArchive11COutHandler16SetSolidSettingsERK14tagPROPVARIANT
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi62:
	.cfi_def_cfa_offset 80
.Lcfi63:
	.cfi_offset %rbx, -48
.Lcfi64:
	.cfi_offset %r12, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	movzwl	(%r12), %eax
	cmpl	$11, %eax
	je	.LBB8_4
# BB#1:
	movzwl	%ax, %ecx
	cmpl	$8, %ecx
	je	.LBB8_5
# BB#2:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	testw	%ax, %ax
	jne	.LBB8_25
# BB#3:                                 # %.thread
	movb	$1, 15(%rsp)
	jmp	.LBB8_22
.LBB8_5:
	movq	8(%r12), %rbp
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 16(%rsp)
	movl	$-1, %ebx
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB8_6:                                # =>This Inner Loop Header: Depth=1
	incl	%ebx
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB8_6
# BB#7:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%rbx), %eax
	movslq	%eax, %r15
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, 16(%rsp)
	movl	$0, (%rax)
	movl	%r15d, 28(%rsp)
	.p2align	4, 0x90
.LBB8_8:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %ecx
	addq	$4, %rbp
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_8
# BB#9:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%ebx, 24(%rsp)
.Ltmp365:
	leaq	16(%rsp), %rdi
	leaq	15(%rsp), %rsi
	callq	_Z12StringToBoolRK11CStringBaseIwERb
	movl	%eax, %ebp
.Ltmp366:
# BB#10:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_12
# BB#11:
	callq	_ZdaPv
.LBB8_12:                               # %_ZN11CStringBaseIwED2Ev.exit
	testb	%bpl, %bpl
	je	.LBB8_13
# BB#20:                                # %thread-pre-split
	movb	15(%rsp), %al
	testb	%al, %al
	jne	.LBB8_22
	jmp	.LBB8_23
.LBB8_4:
	cmpw	$0, 8(%r12)
	setne	%al
	setne	15(%rsp)
	testb	%al, %al
	je	.LBB8_23
.LBB8_22:
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 48(%r14)
	movw	$0, 64(%r14)
	jmp	.LBB8_24
.LBB8_23:
	movq	$1, 48(%r14)
.LBB8_24:
	xorl	%ebp, %ebp
.LBB8_25:
	movl	%ebp, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_13:
	movq	8(%r12), %r12
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 16(%rsp)
	movl	$-1, %ebp
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB8_14:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB8_14
# BB#15:                                # %_Z11MyStringLenIwEiPKT_.exit.i12
	leal	1(%rbp), %eax
	movslq	%eax, %r15
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, 16(%rsp)
	movl	$0, (%rbx)
	movl	%r15d, 28(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_16:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i15
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_16
# BB#17:                                # %_ZN11CStringBaseIwEC2EPKw.exit16
	movl	%ebp, 24(%rsp)
.Ltmp368:
	leaq	16(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZN8NArchive11COutHandler16SetSolidSettingsERK11CStringBaseIwE
	movl	%eax, %ebp
.Ltmp369:
# BB#18:                                # %_ZN11CStringBaseIwED2Ev.exit17
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB8_25
.LBB8_26:                               # %_ZN11CStringBaseIwED2Ev.exit18
.Ltmp370:
	movq	%rax, %r14
	movq	%rbx, %rdi
	jmp	.LBB8_27
.LBB8_19:
.Ltmp367:
	movq	%rax, %r14
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_28
.LBB8_27:
	callq	_ZdaPv
.LBB8_28:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN8NArchive11COutHandler16SetSolidSettingsERK14tagPROPVARIANT, .Lfunc_end8-_ZN8NArchive11COutHandler16SetSolidSettingsERK14tagPROPVARIANT
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp365-.Lfunc_begin5  #   Call between .Lfunc_begin5 and .Ltmp365
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp365-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp366-.Ltmp365       #   Call between .Ltmp365 and .Ltmp366
	.long	.Ltmp367-.Lfunc_begin5  #     jumps to .Ltmp367
	.byte	0                       #   On action: cleanup
	.long	.Ltmp366-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp368-.Ltmp366       #   Call between .Ltmp366 and .Ltmp368
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp368-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp369-.Ltmp368       #   Call between .Ltmp368 and .Ltmp369
	.long	.Ltmp370-.Lfunc_begin5  #     jumps to .Ltmp370
	.byte	0                       #   On action: cleanup
	.long	.Ltmp369-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Lfunc_end8-.Ltmp369    #   Call between .Ltmp369 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NArchive11COutHandler4InitEv
	.p2align	4, 0x90
	.type	_ZN8NArchive11COutHandler4InitEv,@function
_ZN8NArchive11COutHandler4InitEv:       # @_ZN8NArchive11COutHandler4InitEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 16
.Lcfi69:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	$0, 40(%rbx)
	movb	$1, 66(%rbx)
	movl	$0, 67(%rbx)
	movb	$1, 71(%rbx)
	callq	_ZN8NWindows7NSystem21GetNumberOfProcessorsEv
	movl	%eax, (%rbx)
	movl	$5, 76(%rbx)
	movb	$1, 72(%rbx)
	movb	$0, 80(%rbx)
	movl	$4, 4(%rbx)
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 48(%rbx)
	movw	$0, 64(%rbx)
	popq	%rbx
	retq
.Lfunc_end9:
	.size	_ZN8NArchive11COutHandler4InitEv, .Lfunc_end9-_ZN8NArchive11COutHandler4InitEv
	.cfi_endproc

	.globl	_ZN8NArchive11COutHandler17BeforeSetPropertyEv
	.p2align	4, 0x90
	.type	_ZN8NArchive11COutHandler17BeforeSetPropertyEv,@function
_ZN8NArchive11COutHandler17BeforeSetPropertyEv: # @_ZN8NArchive11COutHandler17BeforeSetPropertyEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 16
.Lcfi71:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	$0, 40(%rbx)
	movb	$1, 66(%rbx)
	movl	$0, 67(%rbx)
	movb	$1, 71(%rbx)
	callq	_ZN8NWindows7NSystem21GetNumberOfProcessorsEv
	movl	%eax, (%rbx)
	movl	$5, 76(%rbx)
	movb	$1, 72(%rbx)
	movb	$0, 80(%rbx)
	movl	$4, 4(%rbx)
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 48(%rbx)
	movw	$0, 64(%rbx)
	callq	_ZN8NWindows7NSystem21GetNumberOfProcessorsEv
	movl	%eax, 88(%rbx)
	movl	$-1, 92(%rbx)
	movl	$-1, 96(%rbx)
	movl	$0, 84(%rbx)
	movl	$4, 4(%rbx)
	popq	%rbx
	retq
.Lfunc_end10:
	.size	_ZN8NArchive11COutHandler17BeforeSetPropertyEv, .Lfunc_end10-_ZN8NArchive11COutHandler17BeforeSetPropertyEv
	.cfi_endproc

	.globl	_ZN8NArchive11COutHandler11SetPropertyEPKwRK14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive11COutHandler11SetPropertyEPKwRK14tagPROPVARIANT,@function
_ZN8NArchive11COutHandler11SetPropertyEPKwRK14tagPROPVARIANT: # @_ZN8NArchive11COutHandler11SetPropertyEPKwRK14tagPROPVARIANT
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi78:
	.cfi_def_cfa_offset 240
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$-1, %ebp
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB11_1:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB11_1
# BB#2:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%rbp), %eax
	movslq	%eax, %r14
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, 16(%rsp)
	movl	$0, (%rax)
	movl	%r14d, 28(%rsp)
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB11_3:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %edx
	addq	$4, %rbx
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB11_3
# BB#4:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%ebp, 24(%rsp)
.Ltmp371:
	movq	%rax, %rdi
	callq	_Z13MyStringUpperPw
.Ltmp372:
# BB#5:                                 # %_ZN11CStringBaseIwE9MakeUpperEv.exit
	movl	24(%rsp), %ebx
	movl	$-2147024809, %r14d     # imm = 0x80070057
	testl	%ebx, %ebx
	je	.LBB11_125
# BB#6:
	movq	16(%rsp), %rdi
	movl	(%rdi), %eax
	cmpl	$83, %eax
	je	.LBB11_11
# BB#7:
	cmpl	$88, %eax
	jne	.LBB11_15
# BB#8:
	cmpl	$2, %ebx
	movl	$1, %eax
	cmovll	%ebx, %eax
	testl	%ebx, %ebx
	jle	.LBB11_10
# BB#9:
	movslq	%eax, %rbp
	leaq	(%rdi,%rbp,4), %rsi
	leal	1(%rbx), %eax
	subl	%ebp, %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	callq	memmove
	subl	%ebp, %ebx
	movl	%ebx, 24(%rsp)
.LBB11_10:                              # %_ZN11CStringBaseIwE6DeleteEii.exit
	movl	$9, 76(%r12)
	addq	$76, %r12
.Ltmp377:
	leaq	16(%rsp), %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %r14d
.Ltmp378:
	jmp	.LBB11_125
.LBB11_11:
	cmpl	$2, %ebx
	movl	$1, %eax
	cmovll	%ebx, %eax
	testl	%ebx, %ebx
	jle	.LBB11_13
# BB#12:                                # %_ZN11CStringBaseIwE6DeleteEii.exit140
	movslq	%eax, %rbp
	leaq	(%rdi,%rbp,4), %rsi
	leal	1(%rbx), %eax
	subl	%ebp, %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	callq	memmove
	subl	%ebp, %ebx
	movl	%ebx, 24(%rsp)
	je	.LBB11_50
.LBB11_13:                              # %_ZN11CStringBaseIwE6DeleteEii.exit140.thread
	cmpw	$0, (%r15)
	jne	.LBB11_125
# BB#14:
.Ltmp373:
	leaq	16(%rsp), %rsi
	movq	%r12, %rdi
	callq	_ZN8NArchive11COutHandler16SetSolidSettingsERK11CStringBaseIwE
	movl	%eax, %r14d
.Ltmp374:
	jmp	.LBB11_125
.LBB11_15:
.Ltmp379:
	movl	$.L.str, %esi
	callq	_Z15MyStringComparePKwS0_
.Ltmp380:
# BB#16:
	testl	%eax, %eax
	je	.LBB11_47
# BB#17:
.Ltmp381:
	leaq	16(%rsp), %rdi
	leaq	44(%rsp), %rsi
	callq	_Z19ParseStringToUInt32RK11CStringBaseIwERj
	movl	%eax, %ebx
.Ltmp382:
# BB#18:
	movl	24(%rsp), %ecx
	subl	%ebx, %ecx
.Ltmp384:
	leaq	96(%rsp), %rdi
	leaq	16(%rsp), %rsi
	movl	%ebx, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp385:
# BB#19:                                # %_ZNK11CStringBaseIwE3MidEi.exit
	testl	%ebx, %ebx
	je	.LBB11_51
# BB#20:
	movl	44(%rsp), %eax
	movl	$-2147467259, %r14d     # imm = 0x80004005
	cmpl	$10000, %eax            # imm = 0x2710
	ja	.LBB11_123
.LBB11_21:
	movl	$-2147024809, %r14d     # imm = 0x80070057
	subl	84(%r12), %eax
	jb	.LBB11_123
# BB#22:
	movq	%r15, 152(%rsp)         # 8-byte Spill
	movl	%eax, 44(%rsp)
	movl	20(%r12), %ebx
	cmpl	%eax, %ebx
	jg	.LBB11_33
# BB#23:                                # %.lr.ph
	leaq	8(%r12), %r15
	leaq	56(%rsp), %r13
	decl	%ebx
	leaq	48(%rsp), %r14
	.p2align	4, 0x90
.LBB11_24:                              # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	movq	$8, 72(%rsp)
	movq	$_ZTV13CObjectVectorI5CPropE+16, 48(%rsp)
	movups	%xmm0, 24(%r13)
.Ltmp410:
	movl	$16, %edi
	callq	_Znam
.Ltmp411:
# BB#25:                                #   in Loop: Header=BB11_24 Depth=1
	movq	%rax, 80(%rsp)
	movl	$0, (%rax)
	movl	$4, 92(%rsp)
.Ltmp422:
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp423:
# BB#26:                                # %.noexc162
                                        #   in Loop: Header=BB11_24 Depth=1
.Ltmp424:
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	_ZN8NArchive14COneMethodInfoC2ERKS0_
.Ltmp425:
# BB#27:                                #   in Loop: Header=BB11_24 Depth=1
.Ltmp427:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp428:
# BB#28:                                #   in Loop: Header=BB11_24 Depth=1
	movq	24(%r12), %rax
	movslq	20(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 20(%r12)
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_30
# BB#29:                                #   in Loop: Header=BB11_24 Depth=1
	callq	_ZdaPv
.LBB11_30:                              # %_ZN11CStringBaseIwED2Ev.exit.i
                                        #   in Loop: Header=BB11_24 Depth=1
	movq	$_ZTV13CObjectVectorI5CPropE+16, 48(%rsp)
.Ltmp438:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp439:
# BB#31:                                # %_ZN13CObjectVectorI5CPropED2Ev.exit.i166
                                        #   in Loop: Header=BB11_24 Depth=1
.Ltmp444:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp445:
# BB#32:                                # %_ZN8NArchive14COneMethodInfoD2Ev.exit
                                        #   in Loop: Header=BB11_24 Depth=1
	movl	44(%rsp), %eax
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB11_24
.LBB11_33:                              # %._crit_edge
	movq	24(%r12), %rcx
	cltq
	movq	(%rcx,%rax,8), %r13
	cmpl	$0, 104(%rsp)
	je	.LBB11_80
# BB#34:
	movq	%r13, 160(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	movabsq	$4294967296, %r14       # imm = 0x100000000
	.p2align	4, 0x90
.LBB11_35:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_36 Depth 2
                                        #     Child Loop BB11_39 Depth 2
	movq	%r12, %r13
	movq	%rbp, %rax
	shlq	$4, %rax
	movq	_ZN8NArchiveL14g_NameToPropIDE+8(%rax), %r12
	movl	$-1, %ebx
	movq	%r12, %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_36:                              #   Parent Loop BB11_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%r14, %rax
	incl	%ebx
	cmpl	$0, (%rcx)
	leaq	4(%rcx), %rcx
	jne	.LBB11_36
# BB#37:                                # %_Z11MyStringLenIwEiPKT_.exit.i.i
                                        #   in Loop: Header=BB11_35 Depth=1
	sarq	$32, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp447:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp448:
# BB#38:                                # %.noexc187
                                        #   in Loop: Header=BB11_35 Depth=1
	movl	$0, (%r15)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_39:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   Parent Loop BB11_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r12,%rax), %ecx
	movl	%ecx, (%r15,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB11_39
# BB#40:                                # %_ZN11CStringBaseIwEC2EPKw.exit.i
                                        #   in Loop: Header=BB11_35 Depth=1
.Ltmp450:
	xorl	%edx, %edx
	leaq	48(%rsp), %rdi
	leaq	96(%rsp), %rsi
	movl	%ebx, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp451:
# BB#41:                                # %_ZNK11CStringBaseIwE4LeftEi.exit.i
                                        #   in Loop: Header=BB11_35 Depth=1
	movq	48(%rsp), %rsi
.Ltmp453:
	movq	%r15, %rdi
	callq	_Z21MyStringCompareNoCasePKwS0_
	movl	%eax, %ebx
.Ltmp454:
	movq	%r13, %r12
# BB#42:                                # %_ZNK11CStringBaseIwE13CompareNoCaseERKS0_.exit.i
                                        #   in Loop: Header=BB11_35 Depth=1
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_44
# BB#43:                                #   in Loop: Header=BB11_35 Depth=1
	callq	_ZdaPv
.LBB11_44:                              # %_ZN11CStringBaseIwED2Ev.exit17.i
                                        #   in Loop: Header=BB11_35 Depth=1
	movq	%r15, %rdi
	callq	_ZdaPv
	testl	%ebx, %ebx
	je	.LBB11_89
# BB#45:                                #   in Loop: Header=BB11_35 Depth=1
	incq	%rbp
	cmpq	$14, %rbp
	jbe	.LBB11_35
# BB#46:
	movl	$-2147024809, %r14d     # imm = 0x80070057
	jmp	.LBB11_123
.LBB11_47:
	movl	$4, 4(%r12)
	addq	$4, %r12
	movl	24(%rsp), %ebx
	cmpl	$4, %ebx
	movl	$3, %eax
	cmovll	%ebx, %eax
	testl	%ebx, %ebx
	jle	.LBB11_49
# BB#48:
	movq	16(%rsp), %rdi
	movslq	%eax, %rbp
	leaq	(%rdi,%rbp,4), %rsi
	leal	1(%rbx), %eax
	subl	%ebp, %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	callq	memmove
	subl	%ebp, %ebx
	movl	%ebx, 24(%rsp)
.LBB11_49:                              # %_ZN11CStringBaseIwE6DeleteEii.exit143
.Ltmp522:
	leaq	16(%rsp), %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %r14d
.Ltmp523:
	jmp	.LBB11_125
.LBB11_50:
.Ltmp375:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_ZN8NArchive11COutHandler16SetSolidSettingsERK14tagPROPVARIANT
	movl	%eax, %r14d
.Ltmp376:
	jmp	.LBB11_125
.LBB11_51:
.Ltmp387:
	leaq	168(%rsp), %rdi
	leaq	16(%rsp), %rsi
	xorl	%edx, %edx
	movl	$2, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp388:
# BB#52:                                # %_ZNK11CStringBaseIwE4LeftEi.exit
	movq	168(%rsp), %rdi
.Ltmp389:
	movl	$.L.str.1, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	movl	%eax, %ebx
.Ltmp390:
# BB#53:                                # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit
	movq	168(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_55
# BB#54:
	callq	_ZdaPv
.LBB11_55:                              # %_ZN11CStringBaseIwED2Ev.exit145
	testl	%ebx, %ebx
	je	.LBB11_75
# BB#56:
	movq	16(%rsp), %rdi
.Ltmp392:
	movl	$.L.str.2, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp393:
# BB#57:                                # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit150
	testl	%eax, %eax
	je	.LBB11_79
# BB#58:
	movq	16(%rsp), %rdi
.Ltmp394:
	movl	$.L.str.3, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp395:
# BB#59:                                # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit151
	testl	%eax, %eax
	je	.LBB11_88
# BB#60:
	movq	16(%rsp), %rdi
.Ltmp396:
	movl	$.L.str.4, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp397:
# BB#61:                                # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit152
	testl	%eax, %eax
	je	.LBB11_99
# BB#62:
	movq	16(%rsp), %rdi
.Ltmp398:
	movl	$.L.str.5, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp399:
# BB#63:                                # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit153
	testl	%eax, %eax
	je	.LBB11_100
# BB#64:
	movq	16(%rsp), %rdi
.Ltmp400:
	movl	$.L.str.6, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp401:
# BB#65:                                # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit154
	testl	%eax, %eax
	je	.LBB11_108
# BB#66:
	movq	16(%rsp), %rdi
.Ltmp402:
	movl	$.L.str.7, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp403:
# BB#67:                                # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit155
	testl	%eax, %eax
	je	.LBB11_111
# BB#68:
	movq	16(%rsp), %rdi
.Ltmp404:
	movl	$.L.str.8, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp405:
# BB#69:                                # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit156
	testl	%eax, %eax
	je	.LBB11_128
# BB#70:
	movq	16(%rsp), %rdi
.Ltmp406:
	movl	$.L.str.9, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp407:
# BB#71:                                # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit157
	testl	%eax, %eax
	je	.LBB11_134
# BB#72:
	movq	16(%rsp), %rdi
.Ltmp408:
	movl	$.L.str.10, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp409:
# BB#73:                                # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit158
	testl	%eax, %eax
	je	.LBB11_135
# BB#74:                                # %.thread
	movl	$0, 44(%rsp)
	xorl	%eax, %eax
	jmp	.LBB11_21
.LBB11_75:
	movl	24(%rsp), %ecx
	addl	$-2, %ecx
.Ltmp516:
	leaq	48(%rsp), %rdi
	leaq	16(%rsp), %rsi
	movl	$2, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp517:
# BB#76:                                # %_ZNK11CStringBaseIwE3MidEi.exit146
	movl	88(%r12), %edx
.Ltmp519:
	leaq	48(%rsp), %rdi
	movq	%r15, %rsi
	movq	%r12, %rcx
	callq	_Z11ParseMtPropRK11CStringBaseIwERK14tagPROPVARIANTjRj
	movl	%eax, %r14d
.Ltmp520:
# BB#77:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_123
# BB#78:
	callq	_ZdaPv
	jmp	.LBB11_123
.LBB11_79:
	addq	$40, %r12
.Ltmp513:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
	movl	%eax, %r14d
.Ltmp514:
	jmp	.LBB11_123
.LBB11_80:
	movq	152(%rsp), %rcx         # 8-byte Reload
	movzwl	(%rcx), %eax
	cmpl	$8, %eax
	movl	$-2147024809, %r14d     # imm = 0x80070057
	jne	.LBB11_123
# BB#81:
	movq	8(%rcx), %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movl	$-1, %ebp
	movq	%r14, %rax
.LBB11_82:                              # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB11_82
# BB#83:                                # %_Z11MyStringLenIwEiPKT_.exit.i178
	leal	1(%rbp), %r15d
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp490:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp491:
# BB#84:                                # %.noexc182
	movq	%rbx, 48(%rsp)
	movl	$0, (%rbx)
	movl	%r15d, 60(%rsp)
	xorl	%eax, %eax
.LBB11_85:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i181
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB11_85
# BB#86:
	movl	%ebp, 56(%rsp)
.Ltmp493:
	leaq	48(%rsp), %rdx
	movq	%r13, %rsi
	callq	_ZN8NArchive11COutHandler9SetParamsERNS_14COneMethodInfoERK11CStringBaseIwE
	movl	%eax, %r14d
.Ltmp494:
# BB#87:                                # %_ZN11CStringBaseIwED2Ev.exit184
	movq	%rbx, %rdi
	callq	_ZdaPv
	testl	%r14d, %r14d
	jne	.LBB11_123
	jmp	.LBB11_122
.LBB11_88:
	addq	$72, %r12
.Ltmp511:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
	movl	%eax, %r14d
.Ltmp512:
	jmp	.LBB11_123
.LBB11_89:                              # %_ZN8NArchiveL15FindPropIdStartERK11CStringBaseIwE.exit
	testl	%ebp, %ebp
	movq	152(%rsp), %rbx         # 8-byte Reload
	movl	$-2147024809, %r14d     # imm = 0x80070057
	movq	160(%rsp), %r15         # 8-byte Reload
	js	.LBB11_123
# BB#90:
	movslq	%ebp, %rax
	movw	$0, 136(%rsp)
	movw	$0, 138(%rsp)
	shlq	$4, %rax
	movl	_ZN8NArchiveL14g_NameToPropIDE(%rax), %ecx
	movl	%ecx, 128(%rsp)
	cmpl	$2, %ebp
	ja	.LBB11_103
# BB#91:
	movq	_ZN8NArchiveL14g_NameToPropIDE+8(%rax), %rax
	movl	$-1, %edx
.LBB11_92:                              # =>This Inner Loop Header: Depth=1
	incl	%edx
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB11_92
# BB#93:                                # %_Z11MyStringLenIwEiPKT_.exit
	movl	104(%rsp), %ecx
	subl	%edx, %ecx
.Ltmp467:
	leaq	48(%rsp), %rdi
	leaq	96(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp468:
# BB#94:                                # %_ZNK11CStringBaseIwE3MidEi.exit193
.Ltmp470:
	leaq	48(%rsp), %rdi
	leaq	112(%rsp), %rdx
	movq	%rbx, %rsi
	callq	_Z24ParsePropDictionaryValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %r14d
.Ltmp471:
# BB#95:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_97
# BB#96:
	callq	_ZdaPv
.LBB11_97:                              # %_ZN11CStringBaseIwED2Ev.exit194
	testl	%r14d, %r14d
	je	.LBB11_112
# BB#98:
	movl	$1, %ebp
	jmp	.LBB11_120
.LBB11_99:
	addq	$66, %r12
.Ltmp509:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
	movl	%eax, %r14d
.Ltmp510:
	jmp	.LBB11_123
.LBB11_100:
	movb	$1, 48(%rsp)
.Ltmp506:
	leaq	48(%rsp), %rdi
	movq	%r15, %rsi
	callq	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
	movl	%eax, %r14d
.Ltmp507:
# BB#101:
	testl	%r14d, %r14d
	jne	.LBB11_123
# BB#102:
	xorl	%eax, %eax
	cmpb	$0, 48(%rsp)
	movl	$-2147024809, %r14d     # imm = 0x80070057
	cmovnel	%eax, %r14d
	jmp	.LBB11_123
.LBB11_103:
.Ltmp456:
	leaq	96(%rsp), %rdi
	callq	_ZN8NArchiveL15FindPropIdExactERK11CStringBaseIwE
.Ltmp457:
# BB#104:
	movl	$-2147024809, %r14d     # imm = 0x80070057
	movl	$1, %ebp
	testl	%eax, %eax
	js	.LBB11_120
# BB#105:
	movslq	%eax, %rdx
	shlq	$4, %rdx
	movl	_ZN8NArchiveL14g_NameToPropIDE(%rdx), %eax
	movl	%eax, 128(%rsp)
	movq	(%rbx), %rcx
	movq	8(%rbx), %rax
	movzwl	_ZN8NArchiveL14g_NameToPropIDE+4(%rdx), %edx
	leaq	136(%rsp), %rbx
	movq	%rcx, 112(%rsp)
	movq	%rax, 120(%rsp)
	cmpw	%dx, %cx
	jne	.LBB11_129
# BB#106:
.Ltmp464:
	leaq	112(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK14tagPROPVARIANT
.Ltmp465:
.LBB11_107:                             # %.thread205
	movl	$-2147024809, %r14d     # imm = 0x80070057
	jmp	.LBB11_116
.LBB11_108:
	leaq	68(%r12), %rdi
.Ltmp504:
	movq	%r15, %rsi
	callq	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
	movl	%eax, %r14d
.Ltmp505:
# BB#109:
	testl	%r14d, %r14d
	jne	.LBB11_123
# BB#110:
	movb	$1, 67(%r12)
	jmp	.LBB11_122
.LBB11_111:
	addq	$69, %r12
.Ltmp502:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
	movl	%eax, %r14d
.Ltmp503:
	jmp	.LBB11_123
.LBB11_112:
	movl	112(%rsp), %esi
	leaq	136(%rsp), %rdi
.Ltmp473:
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp474:
# BB#113:
	movl	44(%rsp), %eax
	cmpl	96(%r12), %eax
	ja	.LBB11_115
# BB#114:
	movl	112(%rsp), %eax
	movl	%eax, 92(%r12)
.LBB11_115:                             # %.thread202
	xorl	%r14d, %r14d
.LBB11_116:
.Ltmp476:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp477:
# BB#117:                               # %.noexc
	movl	128(%rsp), %eax
	movl	%eax, (%rbx)
	movq	%rbx, %rdi
	addq	$8, %rdi
	leaq	136(%rsp), %rsi
.Ltmp478:
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp479:
# BB#118:                               # %_ZN5CPropC2ERKS_.exit.i
.Ltmp481:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp482:
# BB#119:                               # %_ZN13CObjectVectorI5CPropE3AddERKS0_.exit
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	xorl	%ebp, %ebp
.LBB11_120:                             # %.thread210
	leaq	136(%rsp), %rdi
.Ltmp487:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp488:
# BB#121:
	testl	%ebp, %ebp
	jne	.LBB11_123
.LBB11_122:
	xorl	%r14d, %r14d
.LBB11_123:                             # %.thread213
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_125
# BB#124:
	callq	_ZdaPv
.LBB11_125:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_127
# BB#126:
	callq	_ZdaPv
.LBB11_127:                             # %_ZN11CStringBaseIwED2Ev.exit137
	movl	%r14d, %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_128:
	addq	$70, %r12
.Ltmp500:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
	movl	%eax, %r14d
.Ltmp501:
	jmp	.LBB11_123
.LBB11_129:
	movzwl	%dx, %edx
	cmpl	$11, %edx
	je	.LBB11_136
# BB#130:
	cmpl	$17, %edx
	jne	.LBB11_120
# BB#131:
	movzwl	%cx, %ecx
	cmpl	$19, %ecx
	jne	.LBB11_120
# BB#132:
	cmpl	$255, %eax
	ja	.LBB11_120
# BB#133:
.Ltmp462:
	movzbl	%al, %esi
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEh
.Ltmp463:
	jmp	.LBB11_107
.LBB11_134:
	addq	$71, %r12
.Ltmp498:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
	movl	%eax, %r14d
.Ltmp499:
	jmp	.LBB11_123
.LBB11_135:
	addq	$80, %r12
.Ltmp496:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
	movl	%eax, %r14d
.Ltmp497:
	jmp	.LBB11_123
.LBB11_136:
.Ltmp458:
	leaq	15(%rsp), %rdi
	leaq	112(%rsp), %rsi
	callq	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
.Ltmp459:
# BB#137:                               # %.noexc199
	testl	%eax, %eax
	jne	.LBB11_120
# BB#138:
	movzbl	15(%rsp), %esi
.Ltmp460:
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp461:
	jmp	.LBB11_107
.LBB11_139:
.Ltmp475:
	jmp	.LBB11_150
.LBB11_140:
.Ltmp466:
	jmp	.LBB11_150
.LBB11_141:
.Ltmp480:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB11_151
.LBB11_142:
.Ltmp472:
	movq	%rax, %r14
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_151
# BB#143:
	callq	_ZdaPv
	jmp	.LBB11_151
.LBB11_144:
.Ltmp469:
	jmp	.LBB11_150
.LBB11_145:
.Ltmp508:
	jmp	.LBB11_175
.LBB11_146:
.Ltmp495:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB11_181
.LBB11_147:
.Ltmp492:
	jmp	.LBB11_175
.LBB11_148:
.Ltmp489:
	jmp	.LBB11_175
.LBB11_149:
.Ltmp483:
.LBB11_150:
	movq	%rax, %r14
.LBB11_151:
	leaq	136(%rsp), %rdi
.Ltmp484:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp485:
	jmp	.LBB11_181
.LBB11_152:
.Ltmp521:
	movq	%rax, %r14
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_181
# BB#153:
	callq	_ZdaPv
	jmp	.LBB11_181
.LBB11_154:
.Ltmp518:
	jmp	.LBB11_175
.LBB11_155:
.Ltmp391:
	movq	%rax, %r14
	movq	168(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_181
# BB#156:
	callq	_ZdaPv
	jmp	.LBB11_181
.LBB11_157:
.Ltmp386:
	jmp	.LBB11_188
.LBB11_158:
.Ltmp383:
	jmp	.LBB11_188
.LBB11_159:
.Ltmp515:
	jmp	.LBB11_175
.LBB11_160:
.Ltmp452:
	movq	%rax, %r14
	jmp	.LBB11_163
.LBB11_161:
.Ltmp455:
	movq	%rax, %r14
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_163
# BB#162:
	callq	_ZdaPv
.LBB11_163:                             # %_ZN11CStringBaseIwED2Ev.exit.i186
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB11_181
.LBB11_164:
.Ltmp449:
	jmp	.LBB11_175
.LBB11_165:
.Ltmp412:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorI5CPropE+16, 48(%rsp)
.Ltmp413:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp414:
# BB#166:
.Ltmp419:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp420:
	jmp	.LBB11_181
.LBB11_167:
.Ltmp421:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB11_168:
.Ltmp415:
	movq	%rax, %rbx
.Ltmp416:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp417:
# BB#169:                               # %.body.i
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB11_170:
.Ltmp418:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB11_171:
.Ltmp426:
	movq	%rax, %r14
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB11_177
.LBB11_172:
.Ltmp440:
	movq	%rax, %r14
.Ltmp441:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp442:
	jmp	.LBB11_181
.LBB11_173:
.Ltmp443:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB11_174:
.Ltmp446:
.LBB11_175:                             # %_ZN11CStringBaseIwED2Ev.exit148
	movq	%rax, %r14
	jmp	.LBB11_181
.LBB11_176:
.Ltmp429:
	movq	%rax, %r14
.LBB11_177:                             # %.body164
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_179
# BB#178:
	callq	_ZdaPv
.LBB11_179:                             # %_ZN11CStringBaseIwED2Ev.exit.i170
	movq	$_ZTV13CObjectVectorI5CPropE+16, 48(%rsp)
.Ltmp430:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp431:
# BB#180:                               # %_ZN13CObjectVectorI5CPropED2Ev.exit.i171
.Ltmp436:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp437:
.LBB11_181:                             # %_ZN11CStringBaseIwED2Ev.exit148
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_189
# BB#182:
	callq	_ZdaPv
	jmp	.LBB11_189
.LBB11_183:
.Ltmp432:
	movq	%rax, %rbx
.Ltmp433:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp434:
	jmp	.LBB11_186
.LBB11_184:
.Ltmp435:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB11_185:
.Ltmp486:
	movq	%rax, %rbx
.LBB11_186:                             # %.body172
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB11_187:
.Ltmp524:
.LBB11_188:
	movq	%rax, %r14
.LBB11_189:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_191
# BB#190:
	callq	_ZdaPv
.LBB11_191:                             # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZN8NArchive11COutHandler11SetPropertyEPKwRK14tagPROPVARIANT, .Lfunc_end11-_ZN8NArchive11COutHandler11SetPropertyEPKwRK14tagPROPVARIANT
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\272\205\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\261\005"              # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp371-.Lfunc_begin6  #   Call between .Lfunc_begin6 and .Ltmp371
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp371-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp372-.Ltmp371       #   Call between .Ltmp371 and .Ltmp372
	.long	.Ltmp524-.Lfunc_begin6  #     jumps to .Ltmp524
	.byte	0                       #   On action: cleanup
	.long	.Ltmp372-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp377-.Ltmp372       #   Call between .Ltmp372 and .Ltmp377
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp377-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp378-.Ltmp377       #   Call between .Ltmp377 and .Ltmp378
	.long	.Ltmp524-.Lfunc_begin6  #     jumps to .Ltmp524
	.byte	0                       #   On action: cleanup
	.long	.Ltmp378-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp373-.Ltmp378       #   Call between .Ltmp378 and .Ltmp373
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp373-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp380-.Ltmp373       #   Call between .Ltmp373 and .Ltmp380
	.long	.Ltmp524-.Lfunc_begin6  #     jumps to .Ltmp524
	.byte	0                       #   On action: cleanup
	.long	.Ltmp381-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp382-.Ltmp381       #   Call between .Ltmp381 and .Ltmp382
	.long	.Ltmp383-.Lfunc_begin6  #     jumps to .Ltmp383
	.byte	0                       #   On action: cleanup
	.long	.Ltmp384-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp385-.Ltmp384       #   Call between .Ltmp384 and .Ltmp385
	.long	.Ltmp386-.Lfunc_begin6  #     jumps to .Ltmp386
	.byte	0                       #   On action: cleanup
	.long	.Ltmp410-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp411-.Ltmp410       #   Call between .Ltmp410 and .Ltmp411
	.long	.Ltmp412-.Lfunc_begin6  #     jumps to .Ltmp412
	.byte	0                       #   On action: cleanup
	.long	.Ltmp422-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp423-.Ltmp422       #   Call between .Ltmp422 and .Ltmp423
	.long	.Ltmp429-.Lfunc_begin6  #     jumps to .Ltmp429
	.byte	0                       #   On action: cleanup
	.long	.Ltmp424-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp425-.Ltmp424       #   Call between .Ltmp424 and .Ltmp425
	.long	.Ltmp426-.Lfunc_begin6  #     jumps to .Ltmp426
	.byte	0                       #   On action: cleanup
	.long	.Ltmp427-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Ltmp428-.Ltmp427       #   Call between .Ltmp427 and .Ltmp428
	.long	.Ltmp429-.Lfunc_begin6  #     jumps to .Ltmp429
	.byte	0                       #   On action: cleanup
	.long	.Ltmp438-.Lfunc_begin6  # >> Call Site 13 <<
	.long	.Ltmp439-.Ltmp438       #   Call between .Ltmp438 and .Ltmp439
	.long	.Ltmp440-.Lfunc_begin6  #     jumps to .Ltmp440
	.byte	0                       #   On action: cleanup
	.long	.Ltmp444-.Lfunc_begin6  # >> Call Site 14 <<
	.long	.Ltmp445-.Ltmp444       #   Call between .Ltmp444 and .Ltmp445
	.long	.Ltmp446-.Lfunc_begin6  #     jumps to .Ltmp446
	.byte	0                       #   On action: cleanup
	.long	.Ltmp447-.Lfunc_begin6  # >> Call Site 15 <<
	.long	.Ltmp448-.Ltmp447       #   Call between .Ltmp447 and .Ltmp448
	.long	.Ltmp449-.Lfunc_begin6  #     jumps to .Ltmp449
	.byte	0                       #   On action: cleanup
	.long	.Ltmp450-.Lfunc_begin6  # >> Call Site 16 <<
	.long	.Ltmp451-.Ltmp450       #   Call between .Ltmp450 and .Ltmp451
	.long	.Ltmp452-.Lfunc_begin6  #     jumps to .Ltmp452
	.byte	0                       #   On action: cleanup
	.long	.Ltmp453-.Lfunc_begin6  # >> Call Site 17 <<
	.long	.Ltmp454-.Ltmp453       #   Call between .Ltmp453 and .Ltmp454
	.long	.Ltmp455-.Lfunc_begin6  #     jumps to .Ltmp455
	.byte	0                       #   On action: cleanup
	.long	.Ltmp454-.Lfunc_begin6  # >> Call Site 18 <<
	.long	.Ltmp522-.Ltmp454       #   Call between .Ltmp454 and .Ltmp522
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp522-.Lfunc_begin6  # >> Call Site 19 <<
	.long	.Ltmp376-.Ltmp522       #   Call between .Ltmp522 and .Ltmp376
	.long	.Ltmp524-.Lfunc_begin6  #     jumps to .Ltmp524
	.byte	0                       #   On action: cleanup
	.long	.Ltmp387-.Lfunc_begin6  # >> Call Site 20 <<
	.long	.Ltmp388-.Ltmp387       #   Call between .Ltmp387 and .Ltmp388
	.long	.Ltmp515-.Lfunc_begin6  #     jumps to .Ltmp515
	.byte	0                       #   On action: cleanup
	.long	.Ltmp389-.Lfunc_begin6  # >> Call Site 21 <<
	.long	.Ltmp390-.Ltmp389       #   Call between .Ltmp389 and .Ltmp390
	.long	.Ltmp391-.Lfunc_begin6  #     jumps to .Ltmp391
	.byte	0                       #   On action: cleanup
	.long	.Ltmp392-.Lfunc_begin6  # >> Call Site 22 <<
	.long	.Ltmp409-.Ltmp392       #   Call between .Ltmp392 and .Ltmp409
	.long	.Ltmp515-.Lfunc_begin6  #     jumps to .Ltmp515
	.byte	0                       #   On action: cleanup
	.long	.Ltmp516-.Lfunc_begin6  # >> Call Site 23 <<
	.long	.Ltmp517-.Ltmp516       #   Call between .Ltmp516 and .Ltmp517
	.long	.Ltmp518-.Lfunc_begin6  #     jumps to .Ltmp518
	.byte	0                       #   On action: cleanup
	.long	.Ltmp519-.Lfunc_begin6  # >> Call Site 24 <<
	.long	.Ltmp520-.Ltmp519       #   Call between .Ltmp519 and .Ltmp520
	.long	.Ltmp521-.Lfunc_begin6  #     jumps to .Ltmp521
	.byte	0                       #   On action: cleanup
	.long	.Ltmp513-.Lfunc_begin6  # >> Call Site 25 <<
	.long	.Ltmp514-.Ltmp513       #   Call between .Ltmp513 and .Ltmp514
	.long	.Ltmp515-.Lfunc_begin6  #     jumps to .Ltmp515
	.byte	0                       #   On action: cleanup
	.long	.Ltmp490-.Lfunc_begin6  # >> Call Site 26 <<
	.long	.Ltmp491-.Ltmp490       #   Call between .Ltmp490 and .Ltmp491
	.long	.Ltmp492-.Lfunc_begin6  #     jumps to .Ltmp492
	.byte	0                       #   On action: cleanup
	.long	.Ltmp493-.Lfunc_begin6  # >> Call Site 27 <<
	.long	.Ltmp494-.Ltmp493       #   Call between .Ltmp493 and .Ltmp494
	.long	.Ltmp495-.Lfunc_begin6  #     jumps to .Ltmp495
	.byte	0                       #   On action: cleanup
	.long	.Ltmp511-.Lfunc_begin6  # >> Call Site 28 <<
	.long	.Ltmp512-.Ltmp511       #   Call between .Ltmp511 and .Ltmp512
	.long	.Ltmp515-.Lfunc_begin6  #     jumps to .Ltmp515
	.byte	0                       #   On action: cleanup
	.long	.Ltmp467-.Lfunc_begin6  # >> Call Site 29 <<
	.long	.Ltmp468-.Ltmp467       #   Call between .Ltmp467 and .Ltmp468
	.long	.Ltmp469-.Lfunc_begin6  #     jumps to .Ltmp469
	.byte	0                       #   On action: cleanup
	.long	.Ltmp470-.Lfunc_begin6  # >> Call Site 30 <<
	.long	.Ltmp471-.Ltmp470       #   Call between .Ltmp470 and .Ltmp471
	.long	.Ltmp472-.Lfunc_begin6  #     jumps to .Ltmp472
	.byte	0                       #   On action: cleanup
	.long	.Ltmp509-.Lfunc_begin6  # >> Call Site 31 <<
	.long	.Ltmp510-.Ltmp509       #   Call between .Ltmp509 and .Ltmp510
	.long	.Ltmp515-.Lfunc_begin6  #     jumps to .Ltmp515
	.byte	0                       #   On action: cleanup
	.long	.Ltmp506-.Lfunc_begin6  # >> Call Site 32 <<
	.long	.Ltmp507-.Ltmp506       #   Call between .Ltmp506 and .Ltmp507
	.long	.Ltmp508-.Lfunc_begin6  #     jumps to .Ltmp508
	.byte	0                       #   On action: cleanup
	.long	.Ltmp456-.Lfunc_begin6  # >> Call Site 33 <<
	.long	.Ltmp457-.Ltmp456       #   Call between .Ltmp456 and .Ltmp457
	.long	.Ltmp483-.Lfunc_begin6  #     jumps to .Ltmp483
	.byte	0                       #   On action: cleanup
	.long	.Ltmp464-.Lfunc_begin6  # >> Call Site 34 <<
	.long	.Ltmp465-.Ltmp464       #   Call between .Ltmp464 and .Ltmp465
	.long	.Ltmp466-.Lfunc_begin6  #     jumps to .Ltmp466
	.byte	0                       #   On action: cleanup
	.long	.Ltmp504-.Lfunc_begin6  # >> Call Site 35 <<
	.long	.Ltmp503-.Ltmp504       #   Call between .Ltmp504 and .Ltmp503
	.long	.Ltmp515-.Lfunc_begin6  #     jumps to .Ltmp515
	.byte	0                       #   On action: cleanup
	.long	.Ltmp473-.Lfunc_begin6  # >> Call Site 36 <<
	.long	.Ltmp474-.Ltmp473       #   Call between .Ltmp473 and .Ltmp474
	.long	.Ltmp475-.Lfunc_begin6  #     jumps to .Ltmp475
	.byte	0                       #   On action: cleanup
	.long	.Ltmp476-.Lfunc_begin6  # >> Call Site 37 <<
	.long	.Ltmp477-.Ltmp476       #   Call between .Ltmp476 and .Ltmp477
	.long	.Ltmp483-.Lfunc_begin6  #     jumps to .Ltmp483
	.byte	0                       #   On action: cleanup
	.long	.Ltmp478-.Lfunc_begin6  # >> Call Site 38 <<
	.long	.Ltmp479-.Ltmp478       #   Call between .Ltmp478 and .Ltmp479
	.long	.Ltmp480-.Lfunc_begin6  #     jumps to .Ltmp480
	.byte	0                       #   On action: cleanup
	.long	.Ltmp481-.Lfunc_begin6  # >> Call Site 39 <<
	.long	.Ltmp482-.Ltmp481       #   Call between .Ltmp481 and .Ltmp482
	.long	.Ltmp483-.Lfunc_begin6  #     jumps to .Ltmp483
	.byte	0                       #   On action: cleanup
	.long	.Ltmp487-.Lfunc_begin6  # >> Call Site 40 <<
	.long	.Ltmp488-.Ltmp487       #   Call between .Ltmp487 and .Ltmp488
	.long	.Ltmp489-.Lfunc_begin6  #     jumps to .Ltmp489
	.byte	0                       #   On action: cleanup
	.long	.Ltmp500-.Lfunc_begin6  # >> Call Site 41 <<
	.long	.Ltmp501-.Ltmp500       #   Call between .Ltmp500 and .Ltmp501
	.long	.Ltmp515-.Lfunc_begin6  #     jumps to .Ltmp515
	.byte	0                       #   On action: cleanup
	.long	.Ltmp462-.Lfunc_begin6  # >> Call Site 42 <<
	.long	.Ltmp463-.Ltmp462       #   Call between .Ltmp462 and .Ltmp463
	.long	.Ltmp466-.Lfunc_begin6  #     jumps to .Ltmp466
	.byte	0                       #   On action: cleanup
	.long	.Ltmp498-.Lfunc_begin6  # >> Call Site 43 <<
	.long	.Ltmp497-.Ltmp498       #   Call between .Ltmp498 and .Ltmp497
	.long	.Ltmp515-.Lfunc_begin6  #     jumps to .Ltmp515
	.byte	0                       #   On action: cleanup
	.long	.Ltmp458-.Lfunc_begin6  # >> Call Site 44 <<
	.long	.Ltmp461-.Ltmp458       #   Call between .Ltmp458 and .Ltmp461
	.long	.Ltmp466-.Lfunc_begin6  #     jumps to .Ltmp466
	.byte	0                       #   On action: cleanup
	.long	.Ltmp484-.Lfunc_begin6  # >> Call Site 45 <<
	.long	.Ltmp485-.Ltmp484       #   Call between .Ltmp484 and .Ltmp485
	.long	.Ltmp486-.Lfunc_begin6  #     jumps to .Ltmp486
	.byte	1                       #   On action: 1
	.long	.Ltmp413-.Lfunc_begin6  # >> Call Site 46 <<
	.long	.Ltmp414-.Ltmp413       #   Call between .Ltmp413 and .Ltmp414
	.long	.Ltmp415-.Lfunc_begin6  #     jumps to .Ltmp415
	.byte	1                       #   On action: 1
	.long	.Ltmp419-.Lfunc_begin6  # >> Call Site 47 <<
	.long	.Ltmp420-.Ltmp419       #   Call between .Ltmp419 and .Ltmp420
	.long	.Ltmp421-.Lfunc_begin6  #     jumps to .Ltmp421
	.byte	1                       #   On action: 1
	.long	.Ltmp416-.Lfunc_begin6  # >> Call Site 48 <<
	.long	.Ltmp417-.Ltmp416       #   Call between .Ltmp416 and .Ltmp417
	.long	.Ltmp418-.Lfunc_begin6  #     jumps to .Ltmp418
	.byte	1                       #   On action: 1
	.long	.Ltmp441-.Lfunc_begin6  # >> Call Site 49 <<
	.long	.Ltmp442-.Ltmp441       #   Call between .Ltmp441 and .Ltmp442
	.long	.Ltmp443-.Lfunc_begin6  #     jumps to .Ltmp443
	.byte	1                       #   On action: 1
	.long	.Ltmp430-.Lfunc_begin6  # >> Call Site 50 <<
	.long	.Ltmp431-.Ltmp430       #   Call between .Ltmp430 and .Ltmp431
	.long	.Ltmp432-.Lfunc_begin6  #     jumps to .Ltmp432
	.byte	1                       #   On action: 1
	.long	.Ltmp436-.Lfunc_begin6  # >> Call Site 51 <<
	.long	.Ltmp437-.Ltmp436       #   Call between .Ltmp436 and .Ltmp437
	.long	.Ltmp486-.Lfunc_begin6  #     jumps to .Ltmp486
	.byte	1                       #   On action: 1
	.long	.Ltmp433-.Lfunc_begin6  # >> Call Site 52 <<
	.long	.Ltmp434-.Ltmp433       #   Call between .Ltmp433 and .Ltmp434
	.long	.Ltmp435-.Lfunc_begin6  #     jumps to .Ltmp435
	.byte	1                       #   On action: 1
	.long	.Ltmp434-.Lfunc_begin6  # >> Call Site 53 <<
	.long	.Lfunc_end11-.Ltmp434   #   Call between .Ltmp434 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropED2Ev,"axG",@progbits,_ZN13CObjectVectorI5CPropED2Ev,comdat
	.weak	_ZN13CObjectVectorI5CPropED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropED2Ev,@function
_ZN13CObjectVectorI5CPropED2Ev:         # @_ZN13CObjectVectorI5CPropED2Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi87:
	.cfi_def_cfa_offset 32
.Lcfi88:
	.cfi_offset %rbx, -24
.Lcfi89:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp525:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp526:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB12_2:
.Ltmp527:
	movq	%rax, %r14
.Ltmp528:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp529:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_4:
.Ltmp530:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN13CObjectVectorI5CPropED2Ev, .Lfunc_end12-_ZN13CObjectVectorI5CPropED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp525-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp526-.Ltmp525       #   Call between .Ltmp525 and .Ltmp526
	.long	.Ltmp527-.Lfunc_begin7  #     jumps to .Ltmp527
	.byte	0                       #   On action: cleanup
	.long	.Ltmp526-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp528-.Ltmp526       #   Call between .Ltmp526 and .Ltmp528
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp528-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp529-.Ltmp528       #   Call between .Ltmp528 and .Ltmp529
	.long	.Ltmp530-.Lfunc_begin7  #     jumps to .Ltmp530
	.byte	1                       #   On action: 1
	.long	.Ltmp529-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Lfunc_end12-.Ltmp529   #   Call between .Ltmp529 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropED0Ev,"axG",@progbits,_ZN13CObjectVectorI5CPropED0Ev,comdat
	.weak	_ZN13CObjectVectorI5CPropED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropED0Ev,@function
_ZN13CObjectVectorI5CPropED0Ev:         # @_ZN13CObjectVectorI5CPropED0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi92:
	.cfi_def_cfa_offset 32
.Lcfi93:
	.cfi_offset %rbx, -24
.Lcfi94:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp531:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp532:
# BB#1:
.Ltmp537:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp538:
# BB#2:                                 # %_ZN13CObjectVectorI5CPropED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB13_5:
.Ltmp539:
	movq	%rax, %r14
	jmp	.LBB13_6
.LBB13_3:
.Ltmp533:
	movq	%rax, %r14
.Ltmp534:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp535:
.LBB13_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB13_4:
.Ltmp536:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZN13CObjectVectorI5CPropED0Ev, .Lfunc_end13-_ZN13CObjectVectorI5CPropED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp531-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp532-.Ltmp531       #   Call between .Ltmp531 and .Ltmp532
	.long	.Ltmp533-.Lfunc_begin8  #     jumps to .Ltmp533
	.byte	0                       #   On action: cleanup
	.long	.Ltmp537-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp538-.Ltmp537       #   Call between .Ltmp537 and .Ltmp538
	.long	.Ltmp539-.Lfunc_begin8  #     jumps to .Ltmp539
	.byte	0                       #   On action: cleanup
	.long	.Ltmp534-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp535-.Ltmp534       #   Call between .Ltmp534 and .Ltmp535
	.long	.Ltmp536-.Lfunc_begin8  #     jumps to .Ltmp536
	.byte	1                       #   On action: 1
	.long	.Ltmp535-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Lfunc_end13-.Ltmp535   #   Call between .Ltmp535 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI5CPropE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI5CPropE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropE6DeleteEii,@function
_ZN13CObjectVectorI5CPropE6DeleteEii:   # @_ZN13CObjectVectorI5CPropE6DeleteEii
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi99:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 64
.Lcfi102:
	.cfi_offset %rbx, -56
.Lcfi103:
	.cfi_offset %r12, -48
.Lcfi104:
	.cfi_offset %r13, -40
.Lcfi105:
	.cfi_offset %r14, -32
.Lcfi106:
	.cfi_offset %r15, -24
.Lcfi107:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB14_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB14_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB14_5
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	leaq	8(%rbp), %rdi
.Ltmp540:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp541:
# BB#4:                                 # %_ZN5CPropD2Ev.exit
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB14_5:                               #   in Loop: Header=BB14_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB14_2
.LBB14_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB14_7:
.Ltmp542:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN13CObjectVectorI5CPropE6DeleteEii, .Lfunc_end14-_ZN13CObjectVectorI5CPropE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp540-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp541-.Ltmp540       #   Call between .Ltmp540 and .Ltmp541
	.long	.Ltmp542-.Lfunc_begin9  #     jumps to .Ltmp542
	.byte	0                       #   On action: cleanup
	.long	.Ltmp541-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Lfunc_end14-.Ltmp541   #   Call between .Ltmp541 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%rbp
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi111:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi112:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi114:
	.cfi_def_cfa_offset 64
.Lcfi115:
	.cfi_offset %rbx, -56
.Lcfi116:
	.cfi_offset %r12, -48
.Lcfi117:
	.cfi_offset %r13, -40
.Lcfi118:
	.cfi_offset %r14, -32
.Lcfi119:
	.cfi_offset %r15, -24
.Lcfi120:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB15_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB15_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB15_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB15_5
.LBB15_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB15_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp543:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp544:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB15_35
# BB#12:
	movq	%rbx, %r13
.LBB15_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB15_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB15_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB15_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB15_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB15_15
.LBB15_14:
	xorl	%esi, %esi
.LBB15_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB15_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB15_16
.LBB15_29:
	movq	%r13, %rbx
.LBB15_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp545:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp546:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB15_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB15_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB15_8
.LBB15_3:
	xorl	%eax, %eax
.LBB15_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB15_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB15_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB15_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB15_30
.LBB15_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB15_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB15_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB15_24
	jmp	.LBB15_25
.LBB15_22:
	xorl	%ecx, %ecx
.LBB15_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB15_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB15_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB15_27
.LBB15_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB15_15
	jmp	.LBB15_29
.LBB15_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp547:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end15:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end15-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin10-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp543-.Lfunc_begin10 #   Call between .Lfunc_begin10 and .Ltmp543
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp543-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp546-.Ltmp543       #   Call between .Ltmp543 and .Ltmp546
	.long	.Ltmp547-.Lfunc_begin10 #     jumps to .Ltmp547
	.byte	0                       #   On action: cleanup
	.long	.Ltmp546-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Lfunc_end15-.Ltmp546   #   Call between .Ltmp546 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED0Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED0Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED0Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi123:
	.cfi_def_cfa_offset 32
.Lcfi124:
	.cfi_offset %rbx, -24
.Lcfi125:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp548:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp549:
# BB#1:
.Ltmp554:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp555:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB16_5:
.Ltmp556:
	movq	%rax, %r14
	jmp	.LBB16_6
.LBB16_3:
.Ltmp550:
	movq	%rax, %r14
.Ltmp551:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp552:
.LBB16_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB16_4:
.Ltmp553:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED0Ev, .Lfunc_end16-_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp548-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp549-.Ltmp548       #   Call between .Ltmp548 and .Ltmp549
	.long	.Ltmp550-.Lfunc_begin11 #     jumps to .Ltmp550
	.byte	0                       #   On action: cleanup
	.long	.Ltmp554-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp555-.Ltmp554       #   Call between .Ltmp554 and .Ltmp555
	.long	.Ltmp556-.Lfunc_begin11 #     jumps to .Ltmp556
	.byte	0                       #   On action: cleanup
	.long	.Ltmp551-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp552-.Ltmp551       #   Call between .Ltmp551 and .Ltmp552
	.long	.Ltmp553-.Lfunc_begin11 #     jumps to .Ltmp553
	.byte	1                       #   On action: 1
	.long	.Ltmp552-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Lfunc_end16-.Ltmp552   #   Call between .Ltmp552 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,@function
_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii: # @_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi126:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi127:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi128:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi129:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi130:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi132:
	.cfi_def_cfa_offset 64
.Lcfi133:
	.cfi_offset %rbx, -56
.Lcfi134:
	.cfi_offset %r12, -48
.Lcfi135:
	.cfi_offset %r13, -40
.Lcfi136:
	.cfi_offset %r14, -32
.Lcfi137:
	.cfi_offset %r15, -24
.Lcfi138:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB17_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB17_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB17_6
# BB#3:                                 #   in Loop: Header=BB17_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_5
# BB#4:                                 #   in Loop: Header=BB17_2 Depth=1
	callq	_ZdaPv
.LBB17_5:                               # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB17_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB17_6:                               #   in Loop: Header=BB17_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB17_2
.LBB17_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end17:
	.size	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii, .Lfunc_end17-_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN8NArchive14COneMethodInfoC2ERKS0_,"axG",@progbits,_ZN8NArchive14COneMethodInfoC2ERKS0_,comdat
	.weak	_ZN8NArchive14COneMethodInfoC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN8NArchive14COneMethodInfoC2ERKS0_,@function
_ZN8NArchive14COneMethodInfoC2ERKS0_:   # @_ZN8NArchive14COneMethodInfoC2ERKS0_
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r15
.Lcfi139:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi140:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi141:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi143:
	.cfi_def_cfa_offset 48
.Lcfi144:
	.cfi_offset %rbx, -40
.Lcfi145:
	.cfi_offset %r12, -32
.Lcfi146:
	.cfi_offset %r14, -24
.Lcfi147:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movq	$8, 24(%rbx)
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp557:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp558:
# BB#1:                                 # %.noexc.i
.Ltmp559:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN13CObjectVectorI5CPropEpLERKS1_
.Ltmp560:
# BB#2:                                 # %_ZN13CObjectVectorI5CPropEC2ERKS1_.exit
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movslq	40(%r14), %r12
	leaq	1(%r12), %r15
	testl	%r15d, %r15d
	je	.LBB18_3
# BB#7:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp565:
	callq	_Znam
.Ltmp566:
# BB#8:                                 # %.noexc
	movq	%rax, 32(%rbx)
	movl	$0, (%rax)
	movl	%r15d, 44(%rbx)
	jmp	.LBB18_9
.LBB18_3:
	xorl	%eax, %eax
.LBB18_9:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	32(%r14), %rcx
	.p2align	4, 0x90
.LBB18_10:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB18_10
# BB#11:
	movl	%r12d, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB18_12:
.Ltmp567:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp568:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp569:
# BB#13:
.Ltmp574:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp575:
	jmp	.LBB18_5
.LBB18_16:
.Ltmp576:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB18_14:
.Ltmp570:
	movq	%rax, %r14
.Ltmp571:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp572:
# BB#17:                                # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB18_15:
.Ltmp573:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_4:
.Ltmp561:
	movq	%rax, %r14
.Ltmp562:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp563:
.LBB18_5:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB18_6:
.Ltmp564:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN8NArchive14COneMethodInfoC2ERKS0_, .Lfunc_end18-_ZN8NArchive14COneMethodInfoC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp557-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp560-.Ltmp557       #   Call between .Ltmp557 and .Ltmp560
	.long	.Ltmp561-.Lfunc_begin12 #     jumps to .Ltmp561
	.byte	0                       #   On action: cleanup
	.long	.Ltmp565-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp566-.Ltmp565       #   Call between .Ltmp565 and .Ltmp566
	.long	.Ltmp567-.Lfunc_begin12 #     jumps to .Ltmp567
	.byte	0                       #   On action: cleanup
	.long	.Ltmp568-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp569-.Ltmp568       #   Call between .Ltmp568 and .Ltmp569
	.long	.Ltmp570-.Lfunc_begin12 #     jumps to .Ltmp570
	.byte	1                       #   On action: 1
	.long	.Ltmp574-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Ltmp575-.Ltmp574       #   Call between .Ltmp574 and .Ltmp575
	.long	.Ltmp576-.Lfunc_begin12 #     jumps to .Ltmp576
	.byte	1                       #   On action: 1
	.long	.Ltmp571-.Lfunc_begin12 # >> Call Site 5 <<
	.long	.Ltmp572-.Ltmp571       #   Call between .Ltmp571 and .Ltmp572
	.long	.Ltmp573-.Lfunc_begin12 #     jumps to .Ltmp573
	.byte	1                       #   On action: 1
	.long	.Ltmp562-.Lfunc_begin12 # >> Call Site 6 <<
	.long	.Ltmp563-.Ltmp562       #   Call between .Ltmp562 and .Ltmp563
	.long	.Ltmp564-.Lfunc_begin12 #     jumps to .Ltmp564
	.byte	1                       #   On action: 1
	.long	.Ltmp563-.Lfunc_begin12 # >> Call Site 7 <<
	.long	.Lfunc_end18-.Ltmp563   #   Call between .Ltmp563 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropEpLERKS1_,"axG",@progbits,_ZN13CObjectVectorI5CPropEpLERKS1_,comdat
	.weak	_ZN13CObjectVectorI5CPropEpLERKS1_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropEpLERKS1_,@function
_ZN13CObjectVectorI5CPropEpLERKS1_:     # @_ZN13CObjectVectorI5CPropEpLERKS1_
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%rbp
.Lcfi148:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi149:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi150:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi151:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi152:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi154:
	.cfi_def_cfa_offset 64
.Lcfi155:
	.cfi_offset %rbx, -56
.Lcfi156:
	.cfi_offset %r12, -48
.Lcfi157:
	.cfi_offset %r13, -40
.Lcfi158:
	.cfi_offset %r14, -32
.Lcfi159:
	.cfi_offset %r15, -24
.Lcfi160:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	12(%r14), %r13
	movl	12(%r15), %esi
	addl	%r13d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testq	%r13, %r13
	jle	.LBB19_4
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB19_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbp,8), %rbx
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r12
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	leaq	8(%r12), %rdi
	addq	$8, %rbx
.Ltmp577:
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp578:
# BB#3:                                 # %_ZN13CObjectVectorI5CPropE3AddERKS0_.exit
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%r12, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB19_2
.LBB19_4:                               # %._crit_edge
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB19_5:
.Ltmp579:
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end19:
	.size	_ZN13CObjectVectorI5CPropEpLERKS1_, .Lfunc_end19-_ZN13CObjectVectorI5CPropEpLERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin13-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp577-.Lfunc_begin13 #   Call between .Lfunc_begin13 and .Ltmp577
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp577-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp578-.Ltmp577       #   Call between .Ltmp577 and .Ltmp578
	.long	.Ltmp579-.Lfunc_begin13 #     jumps to .Ltmp579
	.byte	0                       #   On action: cleanup
	.long	.Ltmp578-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Lfunc_end19-.Ltmp578   #   Call between .Ltmp578 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZN8NArchiveL14g_NameToPropIDE,@object # @_ZN8NArchiveL14g_NameToPropIDE
	.section	.rodata,"a",@progbits
	.p2align	4
_ZN8NArchiveL14g_NameToPropIDE:
	.long	4                       # 0x4
	.short	19                      # 0x13
	.zero	2
	.quad	.L.str.19
	.long	1                       # 0x1
	.short	19                      # 0x13
	.zero	2
	.quad	.L.str.20
	.long	2                       # 0x2
	.short	19                      # 0x13
	.zero	2
	.quad	.L.str.21
	.long	3                       # 0x3
	.short	19                      # 0x13
	.zero	2
	.quad	.L.str.22
	.long	5                       # 0x5
	.short	19                      # 0x13
	.zero	2
	.quad	.L.str.23
	.long	6                       # 0x6
	.short	19                      # 0x13
	.zero	2
	.quad	.L.str.24
	.long	7                       # 0x7
	.short	19                      # 0x13
	.zero	2
	.quad	.L.str.25
	.long	14                      # 0xe
	.short	11                      # 0xb
	.zero	2
	.quad	.L.str.26
	.long	11                      # 0xb
	.short	19                      # 0x13
	.zero	2
	.quad	.L.str.27
	.long	8                       # 0x8
	.short	19                      # 0x13
	.zero	2
	.quad	.L.str.28
	.long	10                      # 0xa
	.short	19                      # 0x13
	.zero	2
	.quad	.L.str.29
	.long	12                      # 0xc
	.short	19                      # 0x13
	.zero	2
	.quad	.L.str.30
	.long	9                       # 0x9
	.short	8                       # 0x8
	.zero	2
	.quad	.L.str.31
	.long	13                      # 0xd
	.short	19                      # 0x13
	.zero	2
	.quad	.L.str.32
	.long	0                       # 0x0
	.short	19                      # 0x13
	.zero	2
	.quad	.L.str.33
	.size	_ZN8NArchiveL14g_NameToPropIDE, 240

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	67                      # 0x43
	.long	82                      # 0x52
	.long	67                      # 0x43
	.long	0                       # 0x0
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
	.p2align	2
.L.str.1:
	.long	77                      # 0x4d
	.long	84                      # 0x54
	.long	0                       # 0x0
	.size	.L.str.1, 12

	.type	.L.str.2,@object        # @.str.2
	.p2align	2
.L.str.2:
	.long	82                      # 0x52
	.long	83                      # 0x53
	.long	70                      # 0x46
	.long	88                      # 0x58
	.long	0                       # 0x0
	.size	.L.str.2, 20

	.type	.L.str.3,@object        # @.str.3
	.p2align	2
.L.str.3:
	.long	70                      # 0x46
	.long	0                       # 0x0
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
	.p2align	2
.L.str.4:
	.long	72                      # 0x48
	.long	67                      # 0x43
	.long	0                       # 0x0
	.size	.L.str.4, 12

	.type	.L.str.5,@object        # @.str.5
	.p2align	2
.L.str.5:
	.long	72                      # 0x48
	.long	67                      # 0x43
	.long	70                      # 0x46
	.long	0                       # 0x0
	.size	.L.str.5, 16

	.type	.L.str.6,@object        # @.str.6
	.p2align	2
.L.str.6:
	.long	72                      # 0x48
	.long	69                      # 0x45
	.long	0                       # 0x0
	.size	.L.str.6, 12

	.type	.L.str.7,@object        # @.str.7
	.p2align	2
.L.str.7:
	.long	84                      # 0x54
	.long	67                      # 0x43
	.long	0                       # 0x0
	.size	.L.str.7, 12

	.type	.L.str.8,@object        # @.str.8
	.p2align	2
.L.str.8:
	.long	84                      # 0x54
	.long	65                      # 0x41
	.long	0                       # 0x0
	.size	.L.str.8, 12

	.type	.L.str.9,@object        # @.str.9
	.p2align	2
.L.str.9:
	.long	84                      # 0x54
	.long	77                      # 0x4d
	.long	0                       # 0x0
	.size	.L.str.9, 12

	.type	.L.str.10,@object       # @.str.10
	.p2align	2
.L.str.10:
	.long	86                      # 0x56
	.long	0                       # 0x0
	.size	.L.str.10, 8

	.type	.L.str.11,@object       # @.str.11
	.p2align	2
.L.str.11:
	.long	76                      # 0x4c
	.long	90                      # 0x5a
	.long	77                      # 0x4d
	.long	65                      # 0x41
	.long	0                       # 0x0
	.size	.L.str.11, 20

	.type	.L.str.12,@object       # @.str.12
	.p2align	2
.L.str.12:
	.long	76                      # 0x4c
	.long	90                      # 0x5a
	.long	77                      # 0x4d
	.long	65                      # 0x41
	.long	50                      # 0x32
	.long	0                       # 0x0
	.size	.L.str.12, 24

	.type	.L.str.13,@object       # @.str.13
	.p2align	2
.L.str.13:
	.long	66                      # 0x42
	.long	84                      # 0x54
	.long	52                      # 0x34
	.long	0                       # 0x0
	.size	.L.str.13, 16

	.type	.L.str.14,@object       # @.str.14
	.p2align	2
.L.str.14:
	.long	72                      # 0x48
	.long	67                      # 0x43
	.long	52                      # 0x34
	.long	0                       # 0x0
	.size	.L.str.14, 16

	.type	.L.str.15,@object       # @.str.15
	.p2align	2
.L.str.15:
	.long	68                      # 0x44
	.long	101                     # 0x65
	.long	102                     # 0x66
	.long	108                     # 0x6c
	.long	97                      # 0x61
	.long	116                     # 0x74
	.long	101                     # 0x65
	.long	0                       # 0x0
	.size	.L.str.15, 32

	.type	.L.str.16,@object       # @.str.16
	.p2align	2
.L.str.16:
	.long	68                      # 0x44
	.long	101                     # 0x65
	.long	102                     # 0x66
	.long	108                     # 0x6c
	.long	97                      # 0x61
	.long	116                     # 0x74
	.long	101                     # 0x65
	.long	54                      # 0x36
	.long	52                      # 0x34
	.long	0                       # 0x0
	.size	.L.str.16, 40

	.type	.L.str.17,@object       # @.str.17
	.p2align	2
.L.str.17:
	.long	66                      # 0x42
	.long	90                      # 0x5a
	.long	105                     # 0x69
	.long	112                     # 0x70
	.long	50                      # 0x32
	.long	0                       # 0x0
	.size	.L.str.17, 24

	.type	.L.str.18,@object       # @.str.18
	.p2align	2
.L.str.18:
	.long	80                      # 0x50
	.long	80                      # 0x50
	.long	77                      # 0x4d
	.long	100                     # 0x64
	.long	0                       # 0x0
	.size	.L.str.18, 20

	.type	.L.str.19,@object       # @.str.19
	.p2align	2
.L.str.19:
	.long	67                      # 0x43
	.long	0                       # 0x0
	.size	.L.str.19, 8

	.type	.L.str.20,@object       # @.str.20
	.p2align	2
.L.str.20:
	.long	68                      # 0x44
	.long	0                       # 0x0
	.size	.L.str.20, 8

	.type	.L.str.21,@object       # @.str.21
	.p2align	2
.L.str.21:
	.long	77                      # 0x4d
	.long	69                      # 0x45
	.long	77                      # 0x4d
	.long	0                       # 0x0
	.size	.L.str.21, 16

	.type	.L.str.22,@object       # @.str.22
	.p2align	2
.L.str.22:
	.long	79                      # 0x4f
	.long	0                       # 0x0
	.size	.L.str.22, 8

	.type	.L.str.23,@object       # @.str.23
	.p2align	2
.L.str.23:
	.long	80                      # 0x50
	.long	66                      # 0x42
	.long	0                       # 0x0
	.size	.L.str.23, 12

	.type	.L.str.24,@object       # @.str.24
	.p2align	2
.L.str.24:
	.long	76                      # 0x4c
	.long	67                      # 0x43
	.long	0                       # 0x0
	.size	.L.str.24, 12

	.type	.L.str.25,@object       # @.str.25
	.p2align	2
.L.str.25:
	.long	76                      # 0x4c
	.long	80                      # 0x50
	.long	0                       # 0x0
	.size	.L.str.25, 12

	.type	.L.str.26,@object       # @.str.26
	.p2align	2
.L.str.26:
	.long	101                     # 0x65
	.long	111                     # 0x6f
	.long	115                     # 0x73
	.long	0                       # 0x0
	.size	.L.str.26, 16

	.type	.L.str.27,@object       # @.str.27
	.p2align	2
.L.str.27:
	.long	80                      # 0x50
	.long	97                      # 0x61
	.long	115                     # 0x73
	.long	115                     # 0x73
	.long	0                       # 0x0
	.size	.L.str.27, 20

	.type	.L.str.28,@object       # @.str.28
	.p2align	2
.L.str.28:
	.long	102                     # 0x66
	.long	98                      # 0x62
	.long	0                       # 0x0
	.size	.L.str.28, 12

	.type	.L.str.29,@object       # @.str.29
	.p2align	2
.L.str.29:
	.long	109                     # 0x6d
	.long	99                      # 0x63
	.long	0                       # 0x0
	.size	.L.str.29, 12

	.type	.L.str.30,@object       # @.str.30
	.p2align	2
.L.str.30:
	.long	97                      # 0x61
	.long	0                       # 0x0
	.size	.L.str.30, 8

	.type	.L.str.31,@object       # @.str.31
	.p2align	2
.L.str.31:
	.long	109                     # 0x6d
	.long	102                     # 0x66
	.long	0                       # 0x0
	.size	.L.str.31, 12

	.type	.L.str.32,@object       # @.str.32
	.p2align	2
.L.str.32:
	.long	109                     # 0x6d
	.long	116                     # 0x74
	.long	0                       # 0x0
	.size	.L.str.32, 12

	.type	.L.str.33,@object       # @.str.33
	.p2align	2
.L.str.33:
	.zero	4
	.size	.L.str.33, 4

	.type	_ZTV13CObjectVectorI5CPropE,@object # @_ZTV13CObjectVectorI5CPropE
	.section	.rodata._ZTV13CObjectVectorI5CPropE,"aG",@progbits,_ZTV13CObjectVectorI5CPropE,comdat
	.weak	_ZTV13CObjectVectorI5CPropE
	.p2align	3
_ZTV13CObjectVectorI5CPropE:
	.quad	0
	.quad	_ZTI13CObjectVectorI5CPropE
	.quad	_ZN13CObjectVectorI5CPropED2Ev
	.quad	_ZN13CObjectVectorI5CPropED0Ev
	.quad	_ZN13CObjectVectorI5CPropE6DeleteEii
	.size	_ZTV13CObjectVectorI5CPropE, 40

	.type	_ZTS13CObjectVectorI5CPropE,@object # @_ZTS13CObjectVectorI5CPropE
	.section	.rodata._ZTS13CObjectVectorI5CPropE,"aG",@progbits,_ZTS13CObjectVectorI5CPropE,comdat
	.weak	_ZTS13CObjectVectorI5CPropE
	.p2align	4
_ZTS13CObjectVectorI5CPropE:
	.asciz	"13CObjectVectorI5CPropE"
	.size	_ZTS13CObjectVectorI5CPropE, 24

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI5CPropE,@object # @_ZTI13CObjectVectorI5CPropE
	.section	.rodata._ZTI13CObjectVectorI5CPropE,"aG",@progbits,_ZTI13CObjectVectorI5CPropE,comdat
	.weak	_ZTI13CObjectVectorI5CPropE
	.p2align	4
_ZTI13CObjectVectorI5CPropE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI5CPropE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI5CPropE, 24

	.type	_ZTV13CObjectVectorI11CStringBaseIwEE,@object # @_ZTV13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTV13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTV13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTV13CObjectVectorI11CStringBaseIwEE
	.p2align	3
_ZTV13CObjectVectorI11CStringBaseIwEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CStringBaseIwEE
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.size	_ZTV13CObjectVectorI11CStringBaseIwEE, 40

	.type	_ZTS13CObjectVectorI11CStringBaseIwEE,@object # @_ZTS13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTS13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTS13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTS13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTS13CObjectVectorI11CStringBaseIwEE:
	.asciz	"13CObjectVectorI11CStringBaseIwEE"
	.size	_ZTS13CObjectVectorI11CStringBaseIwEE, 34

	.type	_ZTI13CObjectVectorI11CStringBaseIwEE,@object # @_ZTI13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTI13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTI13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTI13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTI13CObjectVectorI11CStringBaseIwEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CStringBaseIwEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CStringBaseIwEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
