	.text
	.file	"align.bc"
	.globl	align_path
	.p2align	4, 0x90
	.type	align_path,@function
align_path:                             # @align_path
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$376, %rsp              # imm = 0x178
.Lcfi6:
	.cfi_def_cfa_offset 432
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r13d
	movq	448(%rsp), %r15
	movq	440(%rsp), %r12
	movq	$0, (%r15)
	movq	$0, (%r12)
	movl	%r8d, %ebx
	subl	%r13d, %ebx
	jne	.LBB0_3
# BB#1:
	subl	%ecx, %r9d
	jne	.LBB0_5
# BB#2:
	movq	$0, (%r12)
	jmp	.LBB0_7
.LBB0_3:
	movl	%r9d, %eax
	subl	%ecx, %eax
	jne	.LBB0_8
# BB#4:
	movl	$16, %edi
	callq	xmalloc
	movq	%rax, 8(%rsp)
	movb	$1, 12(%rax)
	jmp	.LBB0_6
.LBB0_5:
	movl	$16, %edi
	movq	%r9, %rbx
	callq	xmalloc
	movq	%rax, 8(%rsp)
	movb	$2, 12(%rax)
.LBB0_6:
	movl	%ebx, 8(%rax)
	movq	$0, (%rax)
	movq	8(%rsp), %rax
	movq	%rax, (%r15)
	movq	%rax, (%r12)
.LBB0_7:
	addq	$376, %rsp              # imm = 0x178
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_8:
	movl	432(%rsp), %ebp
	cmpl	$1, %ebp
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	jg	.LBB0_11
# BB#9:
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%r13d, %ecx
	movl	%r9d, %edx
	subl	%r8d, %edx
	cmpl	%ecx, %edx
	jne	.LBB0_64
# BB#10:
	movl	$16, %edi
	callq	xmalloc
	movq	%rax, 8(%rsp)
	movb	$3, 12(%rax)
	jmp	.LBB0_6
.LBB0_11:
	movl	%ebp, %edx
	shrl	%edx
	movl	%ebp, %eax
	subl	%edx, %eax
	movl	%ecx, %edi
	subl	%r13d, %edi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%r8d, %ecx
	movl	%edi, %esi
	subl	%edx, %esi
	cmpl	%esi, %ecx
	movl	%ecx, %r12d
	movl	%esi, 112(%rsp)         # 4-byte Spill
	cmovll	%esi, %r12d
	movl	%r9d, %esi
	subl	%r13d, %esi
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	movq	%rdi, 160(%rsp)         # 8-byte Spill
	leal	(%rdx,%rdi), %r14d
	cmpl	%r14d, %esi
	cmovlel	%esi, %r14d
	movq	%r9, 80(%rsp)           # 8-byte Spill
                                        # kill: %R9D<def> %R9D<kill> %R9<kill> %R9<def>
	subl	%r8d, %r9d
	movl	%r9d, %edx
	subl	%eax, %edx
	cmpl	%edx, %ecx
	movl	%ecx, 180(%rsp)         # 4-byte Spill
	movl	%edx, 324(%rsp)         # 4-byte Spill
	cmovll	%edx, %ecx
	movl	%ecx, 108(%rsp)         # 4-byte Spill
	movq	%rax, 336(%rsp)         # 8-byte Spill
	movq	%r9, 344(%rsp)          # 8-byte Spill
	leal	(%rax,%r9), %eax
	cmpl	%eax, %esi
	cmovgl	%eax, %esi
	movl	%esi, 36(%rsp)          # 4-byte Spill
	movl	$1, %eax
	subl	%r12d, %eax
	addl	%r14d, %eax
	movslq	%eax, %rbx
	shlq	$2, %rbx
	movq	%rbx, %rdi
	callq	xmalloc
	movslq	%r12d, %r15
	leaq	(,%r15,4), %rbp
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	xmalloc
	movl	%r14d, %ecx
	subl	%r15d, %ecx
	jl	.LBB0_13
# BB#12:                                # %.lr.ph485.preheader
	leaq	4(,%rcx,4), %rdx
	movl	$255, %esi
	movq	%rbx, %rdi
	movq	%rax, 40(%rsp)          # 8-byte Spill
	callq	memset
	movq	40(%rsp), %rax          # 8-byte Reload
.LBB0_13:                               # %._crit_edge486
	movq	%rbx, %r8
	subq	%rbp, %rbx
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%rax, %rbx
	subq	%rbp, %rbx
	testl	%r13d, %r13d
	movq	%r13, 120(%rsp)         # 8-byte Spill
	movq	80(%rsp), %r9           # 8-byte Reload
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	movq	%rbp, %r11
	movq	%r15, 328(%rsp)         # 8-byte Spill
	movl	%r12d, 176(%rsp)        # 4-byte Spill
	movl	%r14d, 172(%rsp)        # 4-byte Spill
	js	.LBB0_21
# BB#14:
	cmpl	%r13d, 16(%rsp)         # 4-byte Folded Reload
	jle	.LBB0_21
# BB#15:
	cmpl	24(%rsp), %r9d          # 4-byte Folded Reload
	jle	.LBB0_21
# BB#16:                                # %.lr.ph.preheader.i420
	movslq	24(%rsp), %rcx          # 4-byte Folded Reload
	movslq	%r9d, %rdx
	movslq	%r13d, %rbp
	movslq	16(%rsp), %rsi          # 4-byte Folded Reload
	incq	%rcx
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph.i424
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	movzbl	(%rdi,%rbp), %ebx
	movq	56(%rsp), %rdi          # 8-byte Reload
	cmpb	-1(%rdi,%rcx), %bl
	jne	.LBB0_20
# BB#18:                                #   in Loop: Header=BB0_17 Depth=1
	incq	%rbp
	cmpq	%rsi, %rbp
	jge	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_17 Depth=1
	cmpq	%rdx, %rcx
	leaq	1(%rcx), %rcx
	jl	.LBB0_17
.LBB0_20:                               # %snake.exit429
	movslq	160(%rsp), %rcx         # 4-byte Folded Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	%ebp, (%rdx,%rcx,4)
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	movq	88(%rsp), %rbx          # 8-byte Reload
	jne	.LBB0_22
	jmp	.LBB0_58
.LBB0_21:                               # %snake.exit429.thread
	movslq	160(%rsp), %rdx         # 4-byte Folded Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%r13d, (%rcx,%rdx,4)
.LBB0_22:                               # %.lr.ph481
	movslq	%r9d, %r12
	movslq	16(%rsp), %rcx          # 4-byte Folded Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	leal	-1(%rsi), %r10d
	movq	120(%rsp), %rdx         # 8-byte Reload
	subl	%edx, %r10d
	leal	-2(%rdx), %ebp
	subl	%esi, %ebp
	leal	-1(%rdx), %edx
	movl	%edx, %edi
	subl	%esi, %edi
	subl	152(%rsp), %edi         # 4-byte Folded Reload
	movl	%edi, 192(%rsp)         # 4-byte Spill
	subl	%r9d, %edx
	movl	%edx, 200(%rsp)         # 4-byte Spill
	movl	$4, %edx
	subq	%r11, %rdx
	leaq	(%r8,%rdx), %rsi
	movq	%rsi, 240(%rsp)         # 8-byte Spill
	addq	%rax, %rdx
	movq	%rdx, 248(%rsp)         # 8-byte Spill
	movl	$16, %edx
	subq	%r11, %rdx
	leaq	(%r8,%rdx), %rsi
	movq	%rsi, 224(%rsp)         # 8-byte Spill
	addq	%rax, %rdx
	movq	%rdx, 232(%rsp)         # 8-byte Spill
	movl	$112, %edx
	subq	%r11, %rdx
	addq	%rdx, %r8
	movq	%r8, 64(%rsp)           # 8-byte Spill
	addq	%rax, %rdx
	movq	%rdx, 216(%rsp)         # 8-byte Spill
	movl	$1, %r13d
	xorl	%edi, %edi
	movl	%ebp, 128(%rsp)         # 4-byte Spill
	movl	%r10d, 304(%rsp)        # 4-byte Spill
	movl	%r10d, %esi
	.p2align	4, 0x90
.LBB0_23:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_25 Depth 2
                                        #       Child Loop BB0_37 Depth 3
                                        #     Child Loop BB0_49 Depth 2
                                        #     Child Loop BB0_52 Depth 2
                                        #     Child Loop BB0_56 Depth 2
	movl	112(%rsp), %r14d        # 4-byte Reload
	cmpl	%r14d, %esi
	movl	%r14d, %edx
	movl	%esi, 116(%rsp)         # 4-byte Spill
	cmovgel	%esi, %edx
	movl	180(%rsp), %r11d        # 4-byte Reload
	cmpl	%r11d, %edx
	cmovll	%r11d, %edx
	movslq	%edx, %r10
	movl	192(%rsp), %esi         # 4-byte Reload
	cmpl	%esi, %ebp
	movl	%esi, %edx
	movl	%ebp, 32(%rsp)          # 4-byte Spill
	cmovgel	%ebp, %edx
	movl	200(%rsp), %ebp         # 4-byte Reload
	cmpl	%ebp, %edx
	cmovll	%ebp, %edx
	notl	%edx
	movslq	%edx, %r8
	cmpq	%r8, %r10
	cmovgeq	%r10, %r8
	movl	304(%rsp), %edx         # 4-byte Reload
	subl	%edi, %edx
	cmpl	%r14d, %edx
	cmovll	%r14d, %edx
	cmpl	%r11d, %edx
	cmovll	%r11d, %edx
	movslq	%edx, %r11
	movl	128(%rsp), %edx         # 4-byte Reload
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	subl	%edi, %edx
	cmpl	%esi, %edx
	movl	%esi, %edi
	cmovgel	%edx, %edi
	cmpl	%ebp, %edi
	cmovll	%ebp, %edi
	notl	%edi
	movslq	%edi, %rax
	cmpq	%rax, %r11
	cmovgeq	%r11, %rax
	cmpl	%esi, %edx
	cmovll	%esi, %edx
	cmpl	%ebp, %edx
	cmovll	%ebp, %edx
	notl	%edx
	movslq	%edx, %rdi
	cmpq	%rdi, %r11
	cmovgeq	%r11, %rdi
	movq	160(%rsp), %rbp         # 8-byte Reload
	movl	%ebp, %r15d
	subl	%r13d, %r15d
	movl	176(%rsp), %edx         # 4-byte Reload
	cmpl	%r15d, %edx
	cmovgel	%edx, %r15d
	movq	%r13, 312(%rsp)         # 8-byte Spill
	leal	(%r13,%rbp), %r13d
	movl	172(%rsp), %edx         # 4-byte Reload
	cmpl	%r13d, %edx
	cmovlel	%edx, %r13d
	cmpl	%r13d, %r15d
	movq	40(%rsp), %rbp          # 8-byte Reload
	jg	.LBB0_57
# BB#24:                                # %.lr.ph476.preheader
                                        #   in Loop: Header=BB0_23 Depth=1
	movq	64(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r10,4), %rdx
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	movq	216(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r10,4), %rdx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	leaq	1(%r8), %rdx
	subq	%r10, %rdx
	andq	$-8, %rdx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	movq	224(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r10,4), %rdx
	movq	%rdx, 208(%rsp)         # 8-byte Spill
	movq	232(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r10,4), %rdx
	movq	%rdx, 360(%rsp)         # 8-byte Spill
	addl	$-7, %r8d
	subl	%r10d, %r8d
	shrl	$3, %r8d
	incl	%r8d
	andl	$3, %r8d
	negq	%r8
	incq	%rax
	subq	%r11, %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 264(%rsp)         # 8-byte Spill
	shrq	$3, %rdx
	incq	%rdx
	movq	%rdx, 256(%rsp)         # 8-byte Spill
	leaq	(%rbp,%r11,4), %rdx
	movq	%rdx, 296(%rsp)         # 8-byte Spill
	movq	240(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,4), %rdx
	movq	%rdx, 280(%rsp)         # 8-byte Spill
	leaq	(%rbx,%r11,4), %rdx
	movq	%rdx, 272(%rsp)         # 8-byte Spill
	movq	248(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,4), %rdx
	movq	%rdx, 288(%rsp)         # 8-byte Spill
	movslq	%r13d, %r11
	movq	%r10, %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_25:                               # %.lr.ph476
                                        #   Parent Loop BB0_23 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_37 Depth 3
	cmpl	%r15d, %esi
	jne	.LBB0_27
# BB#26:                                #   in Loop: Header=BB0_25 Depth=2
	movl	4(%rbp,%rsi,4), %r14d
	incl	%r14d
	testl	%r14d, %r14d
	jns	.LBB0_34
	jmp	.LBB0_41
	.p2align	4, 0x90
.LBB0_27:                               #   in Loop: Header=BB0_25 Depth=2
	cmpl	%r13d, %esi
	jne	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_25 Depth=2
	movl	-4(%rbp,%rsi,4), %r14d
	testl	%r14d, %r14d
	jns	.LBB0_34
	jmp	.LBB0_41
	.p2align	4, 0x90
.LBB0_29:                               #   in Loop: Header=BB0_25 Depth=2
	movl	(%rbp,%rsi,4), %edx
	movl	4(%rbp,%rsi,4), %ebx
	cmpl	%ebx, %edx
	jge	.LBB0_31
# BB#30:                                # %._crit_edge534
                                        #   in Loop: Header=BB0_25 Depth=2
	movl	-4(%rbp,%rsi,4), %ebp
	jmp	.LBB0_32
.LBB0_31:                               #   in Loop: Header=BB0_25 Depth=2
	leal	1(%rdx), %r14d
	movl	-4(%rbp,%rsi,4), %ebp
	cmpl	%ebp, %r14d
	jge	.LBB0_33
.LBB0_32:                               #   in Loop: Header=BB0_25 Depth=2
	leal	1(%rbx), %edi
	cmpl	%edx, %ebx
	movl	%edi, %r14d
	cmovll	%ebp, %r14d
	cmpl	%ebp, %edi
	movq	16(%rsp), %rdi          # 8-byte Reload
	cmovll	%ebp, %r14d
	.p2align	4, 0x90
.LBB0_33:                               #   in Loop: Header=BB0_25 Depth=2
	testl	%r14d, %r14d
	js	.LBB0_41
.LBB0_34:                               #   in Loop: Header=BB0_25 Depth=2
	cmpl	%edi, %r14d
	jge	.LBB0_41
# BB#35:                                #   in Loop: Header=BB0_25 Depth=2
	leal	(%r14,%rsi), %edx
	cmpl	%r9d, %edx
	jge	.LBB0_41
# BB#36:                                # %.lr.ph.preheader.i434
                                        #   in Loop: Header=BB0_25 Depth=2
	movslq	%r14d, %r14
	movslq	%edx, %r9
	incq	%r9
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_37:                               # %.lr.ph.i438
                                        #   Parent Loop BB0_23 Depth=1
                                        #     Parent Loop BB0_25 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbp,%r14), %edx
	cmpb	-1(%rbx,%r9), %dl
	jne	.LBB0_40
# BB#38:                                #   in Loop: Header=BB0_37 Depth=3
	incq	%r14
	cmpq	%rcx, %r14
	jge	.LBB0_40
# BB#39:                                #   in Loop: Header=BB0_37 Depth=3
	cmpq	%r12, %r9
	leaq	1(%r9), %r9
	jl	.LBB0_37
.LBB0_40:                               # %.lr.ph.i438.snake.exit443.loopexit_crit_edge
                                        #   in Loop: Header=BB0_25 Depth=2
	movq	80(%rsp), %r9           # 8-byte Reload
.LBB0_41:                               # %snake.exit443
                                        #   in Loop: Header=BB0_25 Depth=2
	movq	88(%rsp), %rbx          # 8-byte Reload
	movl	%r14d, (%rbx,%rsi,4)
	cmpq	%r11, %rsi
	leaq	1(%rsi), %rsi
	movq	40(%rsp), %rbp          # 8-byte Reload
	jl	.LBB0_25
# BB#42:                                # %.preheader458
                                        #   in Loop: Header=BB0_23 Depth=1
	cmpl	%r13d, %r15d
	jg	.LBB0_57
# BB#43:                                # %.lr.ph478.preheader
                                        #   in Loop: Header=BB0_23 Depth=1
	cmpq	$7, %rax
	jbe	.LBB0_55
# BB#44:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_23 Depth=1
	movq	%rax, %r14
	andq	$-8, %r14
	je	.LBB0_55
# BB#45:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_23 Depth=1
	movq	288(%rsp), %rdx         # 8-byte Reload
	cmpq	%rdx, 296(%rsp)         # 8-byte Folded Reload
	jae	.LBB0_47
# BB#46:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_23 Depth=1
	movq	272(%rsp), %rdx         # 8-byte Reload
	cmpq	280(%rsp), %rdx         # 8-byte Folded Reload
	jb	.LBB0_55
.LBB0_47:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_23 Depth=1
	testb	$3, 256(%rsp)           # 1-byte Folded Reload
	je	.LBB0_50
# BB#48:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_23 Depth=1
	xorl	%edi, %edi
	movq	184(%rsp), %rdx         # 8-byte Reload
	movq	208(%rsp), %r15         # 8-byte Reload
	movq	360(%rsp), %rsi         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_49:                               # %vector.body.prol
                                        #   Parent Loop BB0_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rsi,%rdi,4), %xmm0
	movups	(%rsi,%rdi,4), %xmm1
	movdqu	%xmm0, -16(%r15,%rdi,4)
	movups	%xmm1, (%r15,%rdi,4)
	addq	$8, %rdi
	incq	%r8
	jne	.LBB0_49
	jmp	.LBB0_51
.LBB0_50:                               #   in Loop: Header=BB0_23 Depth=1
	xorl	%edi, %edi
	movq	184(%rsp), %rdx         # 8-byte Reload
.LBB0_51:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_23 Depth=1
	cmpq	$24, 264(%rsp)          # 8-byte Folded Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	144(%rsp), %r8          # 8-byte Reload
	jb	.LBB0_53
	.p2align	4, 0x90
.LBB0_52:                               # %vector.body
                                        #   Parent Loop BB0_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi,%rdi,4), %xmm0
	movups	-96(%rsi,%rdi,4), %xmm1
	movups	%xmm0, -112(%rdx,%rdi,4)
	movups	%xmm1, -96(%rdx,%rdi,4)
	movups	-80(%rsi,%rdi,4), %xmm0
	movups	-64(%rsi,%rdi,4), %xmm1
	movups	%xmm0, -80(%rdx,%rdi,4)
	movups	%xmm1, -64(%rdx,%rdi,4)
	movups	-48(%rsi,%rdi,4), %xmm0
	movups	-32(%rsi,%rdi,4), %xmm1
	movups	%xmm0, -48(%rdx,%rdi,4)
	movups	%xmm1, -32(%rdx,%rdi,4)
	movdqu	-16(%rsi,%rdi,4), %xmm0
	movups	(%rsi,%rdi,4), %xmm1
	movdqu	%xmm0, -16(%rdx,%rdi,4)
	movups	%xmm1, (%rdx,%rdi,4)
	addq	$32, %rdi
	cmpq	%rdi, %r8
	jne	.LBB0_52
.LBB0_53:                               # %middle.block
                                        #   in Loop: Header=BB0_23 Depth=1
	cmpq	%r14, %rax
	je	.LBB0_57
# BB#54:                                #   in Loop: Header=BB0_23 Depth=1
	addq	%r14, %r10
	.p2align	4, 0x90
.LBB0_55:                               # %.lr.ph478.preheader656
                                        #   in Loop: Header=BB0_23 Depth=1
	decq	%r10
	.p2align	4, 0x90
.LBB0_56:                               # %.lr.ph478
                                        #   Parent Loop BB0_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rbx,%r10,4), %edx
	movl	%edx, 4(%rbp,%r10,4)
	incq	%r10
	cmpq	%r11, %r10
	jl	.LBB0_56
.LBB0_57:                               # %._crit_edge479
                                        #   in Loop: Header=BB0_23 Depth=1
	movl	116(%rsp), %esi         # 4-byte Reload
	decl	%esi
	movq	136(%rsp), %rdi         # 8-byte Reload
	incl	%edi
	movl	32(%rsp), %ebp          # 4-byte Reload
	decl	%ebp
	movq	312(%rsp), %rdx         # 8-byte Reload
	cmpl	152(%rsp), %edx         # 4-byte Folded Reload
	leal	1(%rdx), %edx
	movl	%edx, %r13d
	jne	.LBB0_23
.LBB0_58:                               # %._crit_edge482
	movl	$1, %eax
	movl	108(%rsp), %ebp         # 4-byte Reload
	subl	%ebp, %eax
	movl	36(%rsp), %r14d         # 4-byte Reload
	addl	%r14d, %eax
	movslq	%eax, %rbx
	shlq	$2, %rbx
	movq	%rbx, %rdi
	callq	xmalloc
	movslq	%ebp, %r15
	leaq	(,%r15,4), %r12
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rax, %rbp
	subq	%r12, %rbp
	movq	%rbx, %rdi
	callq	xmalloc
	movq	%rbp, %r8
	movq	%rax, %r13
	cmpl	%r14d, %r15d
	movq	16(%rsp), %rbp          # 8-byte Reload
	jg	.LBB0_94
# BB#59:                                # %.lr.ph472
	leal	1(%rbp), %eax
	movslq	36(%rsp), %rcx          # 4-byte Folded Reload
	cmpq	%rcx, %r15
	movq	%rcx, %rdx
	cmovgeq	%r15, %rdx
	incq	%rdx
	subq	%r15, %rdx
	cmpq	$8, %rdx
	movq	%r15, %rsi
	jb	.LBB0_92
# BB#60:                                # %min.iters.checked590
	movq	%rdx, %r9
	andq	$-8, %r9
	movq	%rdx, %r10
	andq	$-8, %r10
	movq	%r15, %rsi
	je	.LBB0_92
# BB#61:                                # %vector.ph594
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	-8(%r10), %rdi
	movl	%edi, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_86
# BB#62:                                # %vector.body585.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
	movq	64(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_63:                               # %vector.body585.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rbx,%rbp,4)
	movdqu	%xmm0, 16(%rbx,%rbp,4)
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB0_63
	jmp	.LBB0_87
.LBB0_64:
	leal	1(%rbx), %ecx
	cmpl	%ecx, %eax
	jne	.LBB0_74
# BB#65:
	testl	%r13d, %r13d
	js	.LBB0_84
# BB#66:
	cmpl	%r13d, %r8d
	jle	.LBB0_167
# BB#67:
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	%eax, %r9d
	jle	.LBB0_167
# BB#68:                                # %.lr.ph.preheader.i
	cltq
	movslq	%r9d, %rcx
	movslq	%r13d, %rbp
	movslq	%r8d, %rdx
	incq	%rax
.LBB0_69:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%rsp), %rsi          # 8-byte Reload
	movzbl	(%rsi,%rbp), %ebx
	movq	56(%rsp), %rsi          # 8-byte Reload
	cmpb	-1(%rsi,%rax), %bl
	jne	.LBB0_72
# BB#70:                                #   in Loop: Header=BB0_69 Depth=1
	incq	%rbp
	cmpq	%rdx, %rbp
	jge	.LBB0_72
# BB#71:                                #   in Loop: Header=BB0_69 Depth=1
	cmpq	%rcx, %rax
	leaq	1(%rax), %rax
	jl	.LBB0_69
.LBB0_72:                               # %snake.exit
	movl	%ebp, %r14d
	subl	%r13d, %r14d
	jle	.LBB0_168
# BB#73:
	movl	$16, %edi
	callq	xmalloc
	movq	%rax, %rbx
	movq	%rbx, 8(%rsp)
	movb	$3, 12(%rbx)
	movl	%r14d, 8(%rbx)
	movq	%rbx, (%r12)
	jmp	.LBB0_169
.LBB0_74:
	incl	%eax
	cmpl	%ebx, %eax
	jne	.LBB0_181
# BB#75:
	testl	%r13d, %r13d
	js	.LBB0_85
# BB#76:
	cmpl	%r13d, %r8d
	jle	.LBB0_173
# BB#77:
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	%eax, %r9d
	jle	.LBB0_173
# BB#78:                                # %.lr.ph.preheader.i409
	cltq
	movslq	%r9d, %rcx
	movslq	%r13d, %rbp
	movslq	%r8d, %rdx
	incq	%rax
.LBB0_79:                               # %.lr.ph.i413
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%rsp), %rsi          # 8-byte Reload
	movzbl	(%rsi,%rbp), %ebx
	movq	56(%rsp), %rsi          # 8-byte Reload
	cmpb	-1(%rsi,%rax), %bl
	jne	.LBB0_82
# BB#80:                                #   in Loop: Header=BB0_79 Depth=1
	incq	%rbp
	cmpq	%rdx, %rbp
	jge	.LBB0_82
# BB#81:                                #   in Loop: Header=BB0_79 Depth=1
	cmpq	%rcx, %rax
	leaq	1(%rax), %rax
	jl	.LBB0_79
.LBB0_82:                               # %snake.exit418
	movl	%ebp, %r14d
	subl	%r13d, %r14d
	jle	.LBB0_174
# BB#83:
	movl	$16, %edi
	callq	xmalloc
	movq	%rax, %rbx
	movq	%rbx, 8(%rsp)
	movb	$3, 12(%rbx)
	movl	%r14d, 8(%rbx)
	movq	%rbx, (%r12)
	jmp	.LBB0_175
.LBB0_84:
                                        # implicit-def: %RBX
	jmp	.LBB0_170
.LBB0_85:
                                        # implicit-def: %RBX
	jmp	.LBB0_176
.LBB0_167:
                                        # implicit-def: %RBX
	jmp	.LBB0_170
.LBB0_173:
                                        # implicit-def: %RBX
	jmp	.LBB0_176
.LBB0_86:
	xorl	%ebp, %ebp
	movq	64(%rsp), %rbx          # 8-byte Reload
.LBB0_87:                               # %vector.body585.prol.loopexit
	cmpq	$24, %rdi
	jb	.LBB0_90
# BB#88:                                # %vector.ph594.new
	movq	%r10, %rsi
	subq	%rbp, %rsi
	leaq	112(%rbx,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB0_89:                               # %vector.body585
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -112(%rbp)
	movdqu	%xmm0, -96(%rbp)
	movdqu	%xmm0, -80(%rbp)
	movdqu	%xmm0, -64(%rbp)
	movdqu	%xmm0, -48(%rbp)
	movdqu	%xmm0, -32(%rbp)
	movdqu	%xmm0, -16(%rbp)
	movdqu	%xmm0, (%rbp)
	subq	$-128, %rbp
	addq	$-32, %rsi
	jne	.LBB0_89
.LBB0_90:                               # %middle.block586
	cmpq	%r10, %rdx
	movq	16(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_94
# BB#91:
	addq	%r15, %r9
	movq	%r9, %rsi
.LBB0_92:                               # %scalar.ph587.preheader
	decq	%rsi
	.p2align	4, 0x90
.LBB0_93:                               # %scalar.ph587
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, 4(%r8,%rsi,4)
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB0_93
.LBB0_94:                               # %._crit_edge473
	movl	464(%rsp), %r11d
	movl	456(%rsp), %r9d
	cmpl	%r9d, %ebp
	movl	%ebp, %eax
	movq	120(%rsp), %rbx         # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%r8, 72(%rsp)           # 8-byte Spill
	jg	.LBB0_101
# BB#95:
	movq	24(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	orl	%ebx, %eax
	js	.LBB0_179
# BB#96:
	cmpl	%r11d, %ecx
	jg	.LBB0_180
.LBB0_97:                               # %.preheader.preheader.i444
	movslq	16(%rsp), %rdx          # 4-byte Folded Reload
	movslq	%ebx, %rdi
	movslq	%ecx, %rsi
	movslq	24(%rsp), %r10          # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB0_98:                               # %.preheader.i448
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %eax
	cmpq	%r10, %rsi
	jle	.LBB0_101
# BB#99:                                # %.preheader.i448
                                        #   in Loop: Header=BB0_98 Depth=1
	cmpq	%rdi, %rdx
	jle	.LBB0_101
# BB#100:                               #   in Loop: Header=BB0_98 Depth=1
	movq	48(%rsp), %rbp          # 8-byte Reload
	movzbl	-1(%rbp,%rdx), %ebx
	decq	%rdx
	movq	56(%rsp), %rbp          # 8-byte Reload
	cmpb	-1(%rbp,%rsi), %bl
	leaq	-1(%rsi), %rsi
	je	.LBB0_98
.LBB0_101:                              # %rsnake.exit453
	movq	%r15, 240(%rsp)         # 8-byte Spill
	movq	%r13, %rdi
	subq	%r12, %rdi
	movq	344(%rsp), %rbx         # 8-byte Reload
	movslq	%ebx, %rdx
	movl	%eax, (%r8,%rdx,4)
	cmpl	$0, 336(%rsp)           # 4-byte Folded Reload
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	jle	.LBB0_153
# BB#102:                               # %.lr.ph468
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %esi
	movq	120(%rsp), %rax         # 8-byte Reload
	orl	%eax, %esi
	movl	%esi, 256(%rsp)         # 4-byte Spill
	movslq	%eax, %r14
	movslq	%edx, %r15
	leal	-1(%rcx), %ebp
	movq	16(%rsp), %rsi          # 8-byte Reload
	subl	%esi, %ebp
	movl	432(%rsp), %edx
	movq	%r12, %r10
	leal	1(%rdx), %r12d
	movq	152(%rsp), %rdx         # 8-byte Reload
	subl	%edx, %r12d
	movl	%r12d, 288(%rsp)        # 4-byte Spill
	movq	%r13, %r12
	leal	-2(%rsi), %r13d
	subl	%ecx, %r13d
	leal	-1(%rdx,%rsi), %edx
	movl	432(%rsp), %esi
	subl	%esi, %edx
	subl	%ecx, %edx
	movl	%edx, 272(%rsp)         # 4-byte Spill
	leal	-1(%rax), %eax
	subl	%ecx, %eax
	movl	%eax, 184(%rsp)         # 4-byte Spill
	movl	$4, %ecx
	subq	%r10, %rcx
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx), %rdx
	movq	%rdx, 232(%rsp)         # 8-byte Spill
	addq	%r12, %rcx
	movq	%rcx, 248(%rsp)         # 8-byte Spill
	movl	$16, %ecx
	subq	%r10, %rcx
	leaq	(%rax,%rcx), %rdx
	movq	%rdx, 216(%rsp)         # 8-byte Spill
	addq	%r12, %rcx
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	movl	$112, %ecx
	subq	%r10, %rcx
	addq	%rcx, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	addq	%r12, %rcx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movl	%ebp, %edx
	movl	$1, %r10d
	movl	%r13d, 280(%rsp)        # 4-byte Spill
	movl	%r13d, %ebp
	xorl	%r12d, %r12d
	movl	%edx, 296(%rsp)         # 4-byte Spill
	movl	%edx, %esi
	.p2align	4, 0x90
.LBB0_103:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_122 Depth 2
                                        #       Child Loop BB0_132 Depth 3
                                        #     Child Loop BB0_112 Depth 2
                                        #       Child Loop BB0_108 Depth 3
                                        #     Child Loop BB0_143 Depth 2
                                        #     Child Loop BB0_147 Depth 2
                                        #     Child Loop BB0_151 Depth 2
	movl	324(%rsp), %edx         # 4-byte Reload
	cmpl	%edx, %esi
	movl	%edx, %eax
	movl	%esi, 112(%rsp)         # 4-byte Spill
	cmovgel	%esi, %eax
	movl	180(%rsp), %ecx         # 4-byte Reload
	cmpl	%ecx, %eax
	cmovll	%ecx, %eax
	movslq	%eax, %r13
	movl	272(%rsp), %esi         # 4-byte Reload
	cmpl	%esi, %ebp
	movl	%esi, %eax
	movl	%ebp, 116(%rsp)         # 4-byte Spill
	cmovgel	%ebp, %eax
	movl	184(%rsp), %ebp         # 4-byte Reload
	cmpl	%ebp, %eax
	cmovll	%ebp, %eax
	notl	%eax
	cltq
	cmpq	%rax, %r13
	cmovgeq	%r13, %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movl	296(%rsp), %eax         # 4-byte Reload
	subl	%r12d, %eax
	cmpl	%edx, %eax
	cmovll	%edx, %eax
	cmpl	%ecx, %eax
	cmovll	%ecx, %eax
	movslq	%eax, %rdx
	movl	280(%rsp), %eax         # 4-byte Reload
	movl	%r12d, 160(%rsp)        # 4-byte Spill
	subl	%r12d, %eax
	cmpl	%esi, %eax
	movl	%esi, %ecx
	cmovgel	%eax, %ecx
	cmpl	%ebp, %ecx
	cmovll	%ebp, %ecx
	notl	%ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rdx
	cmovgeq	%rdx, %rcx
	cmpl	%esi, %eax
	cmovll	%esi, %eax
	cmpl	%ebp, %eax
	cmovll	%ebp, %eax
	notl	%eax
	movslq	%eax, %rsi
	cmpq	%rsi, %rdx
	movq	%rdx, 304(%rsp)         # 8-byte Spill
	cmovgeq	%rdx, %rsi
	movl	%ebx, %edx
	subl	%r10d, %edx
	movl	108(%rsp), %eax         # 4-byte Reload
	cmpl	%edx, %eax
	cmovgel	%eax, %edx
	movq	%r10, 312(%rsp)         # 8-byte Spill
	leal	(%r10,%rbx), %ebp
	movl	36(%rsp), %eax          # 4-byte Reload
	cmpl	%ebp, %eax
	cmovlel	%eax, %ebp
	cmpl	%ebp, %edx
	jg	.LBB0_152
# BB#104:                               # %.lr.ph464
                                        #   in Loop: Header=BB0_103 Depth=1
	movq	%rsi, 264(%rsp)         # 8-byte Spill
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	cmpl	$0, 256(%rsp)           # 4-byte Folded Reload
	movl	%ebp, 32(%rsp)          # 4-byte Spill
	movslq	%ebp, %r10
	movq	%r13, %rbx
	movl	%edx, 136(%rsp)         # 4-byte Spill
	movq	%r10, 200(%rsp)         # 8-byte Spill
	jns	.LBB0_122
# BB#105:                               # %.lr.ph464.split.us.preheader
                                        #   in Loop: Header=BB0_103 Depth=1
	movq	%r13, %r12
	jmp	.LBB0_112
.LBB0_106:                              #   in Loop: Header=BB0_112 Depth=2
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	120(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	464(%rsp), %r11d
	leal	(%rbp,%r12), %ebx
	cmpl	%r11d, %ebx
	jg	.LBB0_111
.LBB0_107:                              # %.preheader.preheader.i.us
                                        #   in Loop: Header=BB0_112 Depth=2
	movslq	%ebp, %rax
	movslq	%ebx, %rcx
	movl	456(%rsp), %r9d
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	200(%rsp), %r10         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_108:                              # %.preheader.i.us
                                        #   Parent Loop BB0_103 Depth=1
                                        #     Parent Loop BB0_112 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, %ebp
	cmpq	%r15, %rcx
	jle	.LBB0_120
# BB#109:                               # %.preheader.i.us
                                        #   in Loop: Header=BB0_108 Depth=3
	cmpq	%r14, %rax
	jle	.LBB0_120
# BB#110:                               #   in Loop: Header=BB0_108 Depth=3
	movq	48(%rsp), %rdx          # 8-byte Reload
	movzbl	-1(%rdx,%rax), %edx
	decq	%rax
	movq	56(%rsp), %rsi          # 8-byte Reload
	cmpb	-1(%rsi,%rcx), %dl
	leaq	-1(%rcx), %rcx
	je	.LBB0_108
	jmp	.LBB0_120
.LBB0_111:                              #   in Loop: Header=BB0_112 Depth=2
	movq	stderr(%rip), %rdi
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movl	%ebx, %ecx
	callq	fprintf
	movl	464(%rsp), %r11d
	jmp	.LBB0_107
	.p2align	4, 0x90
.LBB0_112:                              # %.lr.ph464.split.us
                                        #   Parent Loop BB0_103 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_108 Depth 3
	cmpl	%edx, %r12d
	jne	.LBB0_114
# BB#113:                               #   in Loop: Header=BB0_112 Depth=2
	movl	4(%r8,%r12,4), %ebp
	cmpl	%r9d, %ebp
	jg	.LBB0_120
	jmp	.LBB0_106
	.p2align	4, 0x90
.LBB0_114:                              #   in Loop: Header=BB0_112 Depth=2
	cmpl	32(%rsp), %r12d         # 4-byte Folded Reload
	jne	.LBB0_116
# BB#115:                               #   in Loop: Header=BB0_112 Depth=2
	movl	-4(%r8,%r12,4), %ebp
	decl	%ebp
	cmpl	%r9d, %ebp
	jg	.LBB0_120
	jmp	.LBB0_106
	.p2align	4, 0x90
.LBB0_116:                              #   in Loop: Header=BB0_112 Depth=2
	movl	(%r8,%r12,4), %ecx
	leal	-1(%rcx), %ebp
	movl	4(%r8,%r12,4), %eax
	cmpl	%eax, %ebp
	movl	-4(%r8,%r12,4), %edx
	jg	.LBB0_118
# BB#117:                               #   in Loop: Header=BB0_112 Depth=2
	cmpl	%edx, %ecx
	jle	.LBB0_119
.LBB0_118:                              # %._crit_edge536
                                        #   in Loop: Header=BB0_112 Depth=2
	leal	-1(%rdx), %esi
	cmpl	%ecx, %edx
	movl	%esi, %ebp
	cmovgl	%eax, %ebp
	cmpl	%eax, %esi
	cmovgl	%eax, %ebp
	.p2align	4, 0x90
.LBB0_119:                              #   in Loop: Header=BB0_112 Depth=2
	cmpl	%r9d, %ebp
	jle	.LBB0_106
.LBB0_120:                              # %rsnake.exit.us
                                        #   in Loop: Header=BB0_112 Depth=2
	movl	%ebp, (%rdi,%r12,4)
	cmpq	%r10, %r12
	leaq	1(%r12), %r12
	movl	136(%rsp), %edx         # 4-byte Reload
	jl	.LBB0_112
	jmp	.LBB0_136
.LBB0_121:                              #   in Loop: Header=BB0_122 Depth=2
	movq	stderr(%rip), %rdi
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movl	%r12d, %ecx
	callq	fprintf
	movq	200(%rsp), %r10         # 8-byte Reload
	movq	144(%rsp), %rdi         # 8-byte Reload
	movl	464(%rsp), %r11d
	movq	72(%rsp), %r8           # 8-byte Reload
	movl	456(%rsp), %r9d
	jmp	.LBB0_131
	.p2align	4, 0x90
.LBB0_122:                              # %.lr.ph464.split
                                        #   Parent Loop BB0_103 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_132 Depth 3
	cmpl	%edx, %ebx
	jne	.LBB0_124
# BB#123:                               #   in Loop: Header=BB0_122 Depth=2
	movl	4(%r8,%rbx,4), %ebp
	cmpl	%r9d, %ebp
	jle	.LBB0_130
	jmp	.LBB0_135
	.p2align	4, 0x90
.LBB0_124:                              #   in Loop: Header=BB0_122 Depth=2
	cmpl	32(%rsp), %ebx          # 4-byte Folded Reload
	jne	.LBB0_126
# BB#125:                               #   in Loop: Header=BB0_122 Depth=2
	movl	-4(%r8,%rbx,4), %ebp
	decl	%ebp
	cmpl	%r9d, %ebp
	jle	.LBB0_130
	jmp	.LBB0_135
	.p2align	4, 0x90
.LBB0_126:                              #   in Loop: Header=BB0_122 Depth=2
	movl	(%r8,%rbx,4), %ecx
	leal	-1(%rcx), %ebp
	movl	4(%r8,%rbx,4), %eax
	cmpl	%eax, %ebp
	movl	-4(%r8,%rbx,4), %edx
	jg	.LBB0_128
# BB#127:                               #   in Loop: Header=BB0_122 Depth=2
	cmpl	%edx, %ecx
	jle	.LBB0_129
.LBB0_128:                              # %._crit_edge535
                                        #   in Loop: Header=BB0_122 Depth=2
	leal	-1(%rdx), %esi
	cmpl	%ecx, %edx
	movl	%esi, %ebp
	cmovgl	%eax, %ebp
	cmpl	%eax, %esi
	cmovgl	%eax, %ebp
	.p2align	4, 0x90
.LBB0_129:                              #   in Loop: Header=BB0_122 Depth=2
	cmpl	%r9d, %ebp
	jg	.LBB0_135
.LBB0_130:                              #   in Loop: Header=BB0_122 Depth=2
	leal	(%rbp,%rbx), %r12d
	cmpl	%r11d, %r12d
	jg	.LBB0_121
.LBB0_131:                              # %.preheader.preheader.i
                                        #   in Loop: Header=BB0_122 Depth=2
	movslq	%ebp, %rax
	movslq	%r12d, %rcx
	.p2align	4, 0x90
.LBB0_132:                              # %.preheader.i
                                        #   Parent Loop BB0_103 Depth=1
                                        #     Parent Loop BB0_122 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, %ebp
	cmpq	%r15, %rcx
	jle	.LBB0_135
# BB#133:                               # %.preheader.i
                                        #   in Loop: Header=BB0_132 Depth=3
	cmpq	%r14, %rax
	jle	.LBB0_135
# BB#134:                               #   in Loop: Header=BB0_132 Depth=3
	movq	48(%rsp), %rdx          # 8-byte Reload
	movzbl	-1(%rdx,%rax), %edx
	decq	%rax
	movq	56(%rsp), %rsi          # 8-byte Reload
	cmpb	-1(%rsi,%rcx), %dl
	leaq	-1(%rcx), %rcx
	je	.LBB0_132
.LBB0_135:                              # %rsnake.exit
                                        #   in Loop: Header=BB0_122 Depth=2
	movl	%ebp, (%rdi,%rbx,4)
	cmpq	%r10, %rbx
	leaq	1(%rbx), %rbx
	movl	136(%rsp), %edx         # 4-byte Reload
	jl	.LBB0_122
.LBB0_136:                              # %.preheader
                                        #   in Loop: Header=BB0_103 Depth=1
	cmpl	32(%rsp), %edx          # 4-byte Folded Reload
	movq	344(%rsp), %rbx         # 8-byte Reload
	movq	128(%rsp), %rax         # 8-byte Reload
	jg	.LBB0_152
# BB#137:                               # %.lr.ph466.preheader
                                        #   in Loop: Header=BB0_103 Depth=1
	incq	%rax
	movq	304(%rsp), %rcx         # 8-byte Reload
	subq	%rcx, %rax
	cmpq	$7, %rax
	jbe	.LBB0_150
# BB#138:                               # %min.iters.checked618
                                        #   in Loop: Header=BB0_103 Depth=1
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	%rax, %r12
	andq	$-8, %r12
	je	.LBB0_150
# BB#139:                               # %vector.memcheck640
                                        #   in Loop: Header=BB0_103 Depth=1
	movq	%rcx, %rax
	leaq	(%r8,%rax,4), %rcx
	movq	248(%rsp), %rdx         # 8-byte Reload
	movq	264(%rsp), %rsi         # 8-byte Reload
	leaq	(%rdx,%rsi,4), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB0_141
# BB#140:                               # %vector.memcheck640
                                        #   in Loop: Header=BB0_103 Depth=1
	movq	232(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rcx
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rcx, %rdx
	jb	.LBB0_150
.LBB0_141:                              # %vector.body607.preheader
                                        #   in Loop: Header=BB0_103 Depth=1
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	-8(%rax), %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	shrq	$3, %rcx
	incq	%rcx
	testb	$3, %cl
	je	.LBB0_144
# BB#142:                               # %vector.body607.prol.preheader
                                        #   in Loop: Header=BB0_103 Depth=1
	movq	192(%rsp), %rax         # 8-byte Reload
	leal	-7(%rax), %esi
	subl	%r13d, %esi
	shrl	$3, %esi
	incl	%esi
	andl	$3, %esi
	movq	216(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r13,4), %rdx
	movq	224(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r13,4), %rbp
	negq	%rsi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_143:                              # %vector.body607.prol
                                        #   Parent Loop BB0_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rbp,%rcx,4), %xmm0
	movups	(%rbp,%rcx,4), %xmm1
	movdqu	%xmm0, -16(%rdx,%rcx,4)
	movups	%xmm1, (%rdx,%rcx,4)
	addq	$8, %rcx
	incq	%rsi
	jne	.LBB0_143
	jmp	.LBB0_145
.LBB0_144:                              #   in Loop: Header=BB0_103 Depth=1
	xorl	%ecx, %ecx
	movq	192(%rsp), %rax         # 8-byte Reload
.LBB0_145:                              # %vector.body607.prol.loopexit
                                        #   in Loop: Header=BB0_103 Depth=1
	cmpq	$24, 136(%rsp)          # 8-byte Folded Reload
	jb	.LBB0_148
# BB#146:                               # %vector.body607.preheader.new
                                        #   in Loop: Header=BB0_103 Depth=1
	incq	%rax
	subq	%r13, %rax
	movq	64(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r13,4), %rdx
	movq	208(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r13,4), %rsi
	andq	$-8, %rax
	.p2align	4, 0x90
.LBB0_147:                              # %vector.body607
                                        #   Parent Loop BB0_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi,%rcx,4), %xmm0
	movups	-96(%rsi,%rcx,4), %xmm1
	movups	%xmm0, -112(%rdx,%rcx,4)
	movups	%xmm1, -96(%rdx,%rcx,4)
	movups	-80(%rsi,%rcx,4), %xmm0
	movups	-64(%rsi,%rcx,4), %xmm1
	movups	%xmm0, -80(%rdx,%rcx,4)
	movups	%xmm1, -64(%rdx,%rcx,4)
	movups	-48(%rsi,%rcx,4), %xmm0
	movups	-32(%rsi,%rcx,4), %xmm1
	movups	%xmm0, -48(%rdx,%rcx,4)
	movups	%xmm1, -32(%rdx,%rcx,4)
	movdqu	-16(%rsi,%rcx,4), %xmm0
	movups	(%rsi,%rcx,4), %xmm1
	movdqu	%xmm0, -16(%rdx,%rcx,4)
	movups	%xmm1, (%rdx,%rcx,4)
	addq	$32, %rcx
	cmpq	%rcx, %rax
	jne	.LBB0_147
.LBB0_148:                              # %middle.block608
                                        #   in Loop: Header=BB0_103 Depth=1
	cmpq	%r12, 128(%rsp)         # 8-byte Folded Reload
	je	.LBB0_152
# BB#149:                               #   in Loop: Header=BB0_103 Depth=1
	addq	%r12, %r13
	.p2align	4, 0x90
.LBB0_150:                              # %.lr.ph466.preheader654
                                        #   in Loop: Header=BB0_103 Depth=1
	decq	%r13
	.p2align	4, 0x90
.LBB0_151:                              # %.lr.ph466
                                        #   Parent Loop BB0_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rdi,%r13,4), %eax
	movl	%eax, 4(%r8,%r13,4)
	incq	%r13
	cmpq	%r10, %r13
	jl	.LBB0_151
.LBB0_152:                              # %._crit_edge
                                        #   in Loop: Header=BB0_103 Depth=1
	movq	312(%rsp), %r10         # 8-byte Reload
	incl	%r10d
	movl	112(%rsp), %esi         # 4-byte Reload
	decl	%esi
	movl	160(%rsp), %r12d        # 4-byte Reload
	incl	%r12d
	movl	116(%rsp), %ebp         # 4-byte Reload
	decl	%ebp
	cmpl	288(%rsp), %r10d        # 4-byte Folded Reload
	jne	.LBB0_103
.LBB0_153:                              # %._crit_edge469
	movl	176(%rsp), %eax         # 4-byte Reload
	movl	108(%rsp), %ecx         # 4-byte Reload
	cmpl	%ecx, %eax
	cmovll	%ecx, %eax
	movl	172(%rsp), %ecx         # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	cmpl	%edx, %ecx
	cmovgl	%edx, %ecx
	cmpl	%ecx, %eax
	jle	.LBB0_155
# BB#154:
	xorl	%r14d, %r14d
	movq	120(%rsp), %r13         # 8-byte Reload
	movl	%r13d, %ebx
	movq	24(%rsp), %rbp          # 8-byte Reload
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	movq	328(%rsp), %r15         # 8-byte Reload
	jmp	.LBB0_160
.LBB0_155:                              # %.lr.ph.preheader
	movslq	%eax, %rbp
	movslq	%ecx, %rax
	movq	120(%rsp), %r13         # 8-byte Reload
	movq	328(%rsp), %r15         # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_156:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx,%rbp,4), %ebx
	movl	(%r8,%rbp,4), %ecx
	cmpl	%ecx, %ebx
	jge	.LBB0_159
# BB#157:                               #   in Loop: Header=BB0_156 Depth=1
	cmpq	%rax, %rbp
	leaq	1(%rbp), %rbp
	jl	.LBB0_156
# BB#158:
	xorl	%r14d, %r14d
	movl	%r13d, %ebx
	movq	24(%rsp), %rbp          # 8-byte Reload
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	jmp	.LBB0_160
.LBB0_159:
	movl	%ebx, %eax
	subl	%r13d, %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	subl	%ecx, %edx
	cmpl	%edx, %eax
	cmovll	%ecx, %ebx
	addl	%ebx, %ebp
	movb	$1, %r14b
.LBB0_160:                              # %.loopexit
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r15,4), %rdi
	callq	free
	movq	240(%rsp), %r12         # 8-byte Reload
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r12,4), %rdi
	callq	free
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r15,4), %rdi
	callq	free
	movq	144(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12,4), %rdi
	callq	free
	testb	%r14b, %r14b
	je	.LBB0_165
# BB#161:
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	leaq	360(%rsp), %r11
	leaq	16(%rsp), %r10
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdi
	movq	64(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rsi
	movl	%r13d, %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%ebx, %r8d
	movl	%ebp, %r9d
	movl	472(%rsp), %eax
	movq	%rax, %r12
	pushq	%r12
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	movl	472(%rsp), %eax
	movq	%rax, %r13
	pushq	%r13
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	192(%rsp)               # 8-byte Folded Reload
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	callq	align_path
	addq	$48, %rsp
.Lcfi19:
	.cfi_adjust_cfa_offset -48
	subq	$8, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	leaq	376(%rsp), %rax
	leaq	104(%rsp), %r10
	movq	%r15, %rdi
	movq	%r14, %rsi
	movl	%ebx, %edx
	movl	%ebp, %ecx
	movq	24(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	88(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%r12
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	376(%rsp)               # 8-byte Folded Reload
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	callq	align_path
	addq	$48, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset -48
	cmpq	$0, 8(%rsp)
	movq	96(%rsp), %rax
	je	.LBB0_163
# BB#162:
	movq	352(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	%rax, %rcx
	movq	8(%rsp), %rax
	jmp	.LBB0_164
.LBB0_163:
	movq	%rax, 8(%rsp)
	movq	%rax, %rcx
.LBB0_164:
	movq	448(%rsp), %rdx
	movq	440(%rsp), %rsi
	movq	%rax, (%rsi)
	testq	%rcx, %rcx
	leaq	352(%rsp), %rax
	leaq	368(%rsp), %rcx
	cmoveq	%rax, %rcx
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
	jmp	.LBB0_7
.LBB0_165:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$48, %esi
	movl	$1, %edx
	callq	fwrite
	movq	$0, 8(%rsp)
	xorl	%eax, %eax
                                        # implicit-def: %RCX
	jmp	.LBB0_164
.LBB0_168:
                                        # implicit-def: %RBX
.LBB0_169:                              # %snake.exit.thread
	movl	%ebp, %r13d
.LBB0_170:                              # %snake.exit.thread
	movl	$16, %edi
	callq	xmalloc
	movq	%rax, 96(%rsp)
	movb	$2, 12(%rax)
	movl	$1, 8(%rax)
	cmpq	$0, (%r12)
	cmoveq	%r12, %rbx
	movq	%rax, (%rbx)
	movq	%rax, (%r15)
	movq	$0, (%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	subl	%r13d, %ecx
	je	.LBB0_7
# BB#171:
	movq	96(%rsp), %rax
	movq	%rax, 8(%rsp)
	movq	%rcx, %rbx
	movl	$16, %edi
	callq	xmalloc
	movq	%rax, 96(%rsp)
	movq	%rax, (%r15)
	movb	$3, 12(%rax)
	jmp	.LBB0_178
.LBB0_174:
                                        # implicit-def: %RBX
.LBB0_175:                              # %snake.exit418.thread
	movl	%ebp, %r13d
.LBB0_176:                              # %snake.exit418.thread
	movl	$16, %edi
	callq	xmalloc
	movq	%rax, 96(%rsp)
	movb	$1, 12(%rax)
	movl	$1, 8(%rax)
	cmpq	$0, (%r12)
	cmoveq	%r12, %rbx
	movq	%rax, (%rbx)
	movq	%rax, (%r15)
	movq	$0, (%rax)
	leal	1(%r13), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	%ecx, %eax
	jge	.LBB0_7
# BB#177:
	movq	96(%rsp), %rax
	movq	%rax, 8(%rsp)
	movq	%rcx, %rbx
	movl	$16, %edi
	callq	xmalloc
	movq	%rax, 96(%rsp)
	movq	%rax, (%r15)
	movb	$3, 12(%rax)
	decl	%ebx
	subl	%r13d, %ebx
.LBB0_178:
	movl	%ebx, 8(%rax)
	movq	$0, (%rax)
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	jmp	.LBB0_7
.LBB0_179:
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	464(%rsp), %r11d
	movq	72(%rsp), %r8           # 8-byte Reload
	movl	456(%rsp), %r9d
	movq	80(%rsp), %rcx          # 8-byte Reload
	cmpl	%r11d, %ecx
	jle	.LBB0_97
.LBB0_180:
	movq	stderr(%rip), %rdi
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	464(%rsp), %r11d
	movq	72(%rsp), %r8           # 8-byte Reload
	movl	456(%rsp), %r9d
	movq	80(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB0_97
.LBB0_181:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$48, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB0_7
.Lfunc_end0:
	.size	align_path, .Lfunc_end0-align_path
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.text
	.globl	align_get_dist
	.p2align	4, 0x90
	.type	align_get_dist,@function
align_get_dist:                         # @align_get_dist
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi33:
	.cfi_def_cfa_offset 336
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, %r12
	movq	%rdi, %rbp
	movl	336(%rsp), %eax
	movl	%ecx, %ebx
	subl	%edx, %ebx
	movl	%ecx, %esi
	subl	%r8d, %esi
	movl	%ebx, %edi
	subl	%eax, %edi
	cmpl	%edi, %esi
	movl	%esi, 84(%rsp)          # 4-byte Spill
	movl	%edi, 80(%rsp)          # 4-byte Spill
	cmovll	%edi, %esi
	movl	%r9d, %edi
	subl	%edx, %edi
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	leal	(%rbx,%rax), %eax
	cmpl	%eax, %edi
	cmovgl	%eax, %edi
	movl	%r9d, %eax
	movl	%r8d, 8(%rsp)           # 4-byte Spill
	subl	%r8d, %eax
	movl	%edi, 12(%rsp)          # 4-byte Spill
	cmpl	%edi, %eax
	jg	.LBB1_64
# BB#1:
	cmpl	%esi, %eax
	jl	.LBB1_64
# BB#2:
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movl	$1, %eax
	subl	%esi, %eax
	addl	12(%rsp), %eax          # 4-byte Folded Reload
	movl	%r9d, 28(%rsp)          # 4-byte Spill
	movl	%esi, %r15d
	movslq	%eax, %r13
	shlq	$2, %r13
	movq	%r13, %rdi
	callq	xmalloc
	movq	%rax, %rbx
	movslq	%r15d, %r14
	leaq	(,%r14,4), %rcx
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	subq	%rcx, %rbx
	movq	%r13, %rdi
	movl	28(%rsp), %r13d         # 4-byte Reload
	callq	xmalloc
	movl	%r15d, 76(%rsp)         # 4-byte Spill
	cmpl	12(%rsp), %r15d         # 4-byte Folded Reload
	movq	%rbx, %r15
	jg	.LBB1_16
# BB#3:                                 # %.lr.ph166.preheader
	movslq	12(%rsp), %rcx          # 4-byte Folded Reload
	cmpq	%rcx, %r14
	movq	%rcx, %r11
	cmovgeq	%r14, %r11
	incq	%r11
	subq	%r14, %r11
	cmpq	$8, %r11
	movq	%r14, %rsi
	jb	.LBB1_14
# BB#4:                                 # %min.iters.checked
	movq	%r11, %r9
	andq	$-8, %r9
	movq	%r11, %r10
	andq	$-8, %r10
	movq	%r14, %rsi
	je	.LBB1_14
# BB#5:                                 # %vector.body.preheader
	leaq	-8(%r10), %r8
	movl	%r8d, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB1_6
# BB#7:                                 # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [2147483648,2147483648,2147483648,2147483648]
	movq	32(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_8:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, (%rdx,%rdi,4)
	movups	%xmm0, 16(%rdx,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB1_8
	jmp	.LBB1_9
.LBB1_6:
	xorl	%edi, %edi
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB1_9:                                # %vector.body.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB1_12
# BB#10:                                # %vector.body.preheader.new
	movq	%r10, %rsi
	subq	%rdi, %rsi
	leaq	112(%rdx,%rdi,4), %rdi
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [2147483648,2147483648,2147483648,2147483648]
	.p2align	4, 0x90
.LBB1_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -112(%rdi)
	movups	%xmm0, -96(%rdi)
	movups	%xmm0, -80(%rdi)
	movups	%xmm0, -64(%rdi)
	movups	%xmm0, -48(%rdi)
	movups	%xmm0, -32(%rdi)
	movups	%xmm0, -16(%rdi)
	movups	%xmm0, (%rdi)
	subq	$-128, %rdi
	addq	$-32, %rsi
	jne	.LBB1_11
.LBB1_12:                               # %middle.block
	cmpq	%r10, %r11
	je	.LBB1_16
# BB#13:
	addq	%r14, %r9
	movq	%r9, %rsi
.LBB1_14:                               # %.lr.ph166.preheader227
	decq	%rsi
	.p2align	4, 0x90
.LBB1_15:                               # %.lr.ph166
                                        # =>This Inner Loop Header: Depth=1
	movl	$-2147483648, 4(%r15,%rsi,4) # imm = 0x80000000
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB1_15
.LBB1_16:                               # %._crit_edge167
	movq	56(%rsp), %r10          # 8-byte Reload
	testl	%r10d, %r10d
	movl	%r10d, %esi
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	48(%rsp), %r11          # 8-byte Reload
	js	.LBB1_23
# BB#17:
	cmpl	8(%rsp), %r10d          # 4-byte Folded Reload
	movl	%r10d, %esi
	jge	.LBB1_23
# BB#18:
	cmpl	%r13d, %r11d
	movl	%r10d, %esi
	jge	.LBB1_23
# BB#19:                                # %.lr.ph.preheader.i
	movslq	%r11d, %rdx
	movslq	%r13d, %r8
	movslq	%r10d, %rsi
	movslq	8(%rsp), %rdi           # 4-byte Folded Reload
	incq	%rdx
	.p2align	4, 0x90
.LBB1_20:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp,%rsi), %ecx
	cmpb	-1(%r12,%rdx), %cl
	jne	.LBB1_23
# BB#21:                                #   in Loop: Header=BB1_20 Depth=1
	incq	%rsi
	cmpq	%rdi, %rsi
	jge	.LBB1_23
# BB#22:                                #   in Loop: Header=BB1_20 Depth=1
	cmpq	%r8, %rdx
	leaq	1(%rdx), %rdx
	jl	.LBB1_20
.LBB1_23:                               # %snake.exit
	movq	%rax, %rbx
	subq	40(%rsp), %rbx          # 8-byte Folded Reload
	movslq	128(%rsp), %rcx         # 4-byte Folded Reload
	movl	%esi, (%r15,%rcx,4)
	movslq	16(%rsp), %rdx          # 4-byte Folded Reload
	movl	8(%rsp), %ecx           # 4-byte Reload
	movq	%rdx, 264(%rsp)         # 8-byte Spill
	cmpl	%ecx, (%r15,%rdx,4)
	jge	.LBB1_66
# BB#24:                                # %.preheader157
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	336(%rsp), %ecx
	movq	%rcx, %r8
	testl	%r8d, %r8d
	jle	.LBB1_64
# BB#25:                                # %.lr.ph163
	movq	%r14, 144(%rsp)         # 8-byte Spill
	movslq	%r13d, %rdx
	movslq	8(%rsp), %rsi           # 4-byte Folded Reload
	movq	%r11, %rcx
	leal	-1(%rcx), %r11d
	subl	%r10d, %r11d
	leal	-2(%r10), %r14d
	subl	%ecx, %r14d
	decl	%r10d
	movl	%r10d, %edi
	subl	%r8d, %edi
	subl	%ecx, %edi
	movl	%edi, 64(%rsp)          # 4-byte Spill
	movq	%r10, %rcx
	subl	%r13d, %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	$4, %edi
	movq	40(%rsp), %rbx          # 8-byte Reload
	subq	%rbx, %rdi
	leaq	(%r9,%rdi), %rcx
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	addq	%rax, %rdi
	movq	%rdi, 240(%rsp)         # 8-byte Spill
	movl	$16, %ecx
	subq	%rbx, %rcx
	leaq	(%r9,%rcx), %rdi
	movq	%rdi, 216(%rsp)         # 8-byte Spill
	addq	%rax, %rcx
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	movl	$112, %ecx
	subq	%rbx, %rcx
	addq	%rcx, %r9
	movq	%r9, 32(%rsp)           # 8-byte Spill
	addq	%rax, %rcx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movl	$1, %r13d
	movl	%r14d, 68(%rsp)         # 4-byte Spill
	movl	%r14d, %edi
	movl	%r11d, 72(%rsp)         # 4-byte Spill
	movl	%r11d, %ebx
	xorl	%ecx, %ecx
	movq	%r15, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_26:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_28 Depth 2
                                        #       Child Loop BB1_58 Depth 3
                                        #     Child Loop BB1_42 Depth 2
                                        #     Child Loop BB1_44 Depth 2
                                        #     Child Loop BB1_33 Depth 2
	movl	80(%rsp), %r9d          # 4-byte Reload
	cmpl	%r9d, %ebx
	movl	%r9d, %eax
	movl	%ebx, 88(%rsp)          # 4-byte Spill
	cmovgel	%ebx, %eax
	movl	84(%rsp), %r11d         # 4-byte Reload
	cmpl	%r11d, %eax
	cmovll	%r11d, %eax
	movslq	%eax, %r8
	movl	64(%rsp), %r14d         # 4-byte Reload
	cmpl	%r14d, %edi
	movl	%r14d, %eax
	movl	%edi, 92(%rsp)          # 4-byte Spill
	cmovgel	%edi, %eax
	movq	56(%rsp), %rbx          # 8-byte Reload
	cmpl	%ebx, %eax
	cmovll	%ebx, %eax
	notl	%eax
	movslq	%eax, %rdi
	cmpq	%rdi, %r8
	cmovgeq	%r8, %rdi
	movl	72(%rsp), %eax          # 4-byte Reload
	subl	%ecx, %eax
	cmpl	%r9d, %eax
	cmovll	%r9d, %eax
	cmpl	%r11d, %eax
	cmovll	%r11d, %eax
	movslq	%eax, %r11
	movl	68(%rsp), %eax          # 4-byte Reload
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	subl	%ecx, %eax
	cmpl	%r14d, %eax
	movl	%r14d, %ecx
	cmovgel	%eax, %ecx
	cmpl	%ebx, %ecx
	cmovll	%ebx, %ecx
	notl	%ecx
	movslq	%ecx, %r10
	cmpq	%r10, %r11
	cmovgeq	%r11, %r10
	cmpl	%r14d, %eax
	cmovll	%r14d, %eax
	cmpl	%ebx, %eax
	cmovll	%ebx, %eax
	notl	%eax
	movslq	%eax, %rbx
	cmpq	%rbx, %r11
	cmovgeq	%r11, %rbx
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	%eax, %r14d
	subl	%r13d, %r14d
	movl	76(%rsp), %ecx          # 4-byte Reload
	cmpl	%r14d, %ecx
	cmovgel	%ecx, %r14d
	movq	%r13, 48(%rsp)          # 8-byte Spill
	leal	(%r13,%rax), %r9d
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	%r9d, %eax
	cmovlel	%eax, %r9d
	cmpl	%r9d, %r14d
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	28(%rsp), %r13d         # 4-byte Reload
	jg	.LBB1_34
# BB#27:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_26 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r8,4), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	208(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r8,4), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	1(%rdi), %rax
	subq	%r8, %rax
	andq	$-8, %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	216(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r8,4), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	224(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r8,4), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	addl	$-7, %edi
	subl	%r8d, %edi
	shrl	$3, %edi
	incl	%edi
	andl	$3, %edi
	negq	%rdi
	movq	%rdi, 168(%rsp)         # 8-byte Spill
	incq	%r10
	subq	%r11, %r10
	movq	%r10, 120(%rsp)         # 8-byte Spill
	leaq	-8(%r10), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	shrq	$3, %rax
	incq	%rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	leaq	(%r15,%r11,4), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movq	232(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,4), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	leaq	(%rcx,%r11,4), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	240(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,4), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movslq	%r9d, %rbx
	movq	%r8, %r10
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB1_28:                               # %.lr.ph
                                        #   Parent Loop BB1_26 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_58 Depth 3
	cmpl	%r14d, %r10d
	jne	.LBB1_47
# BB#29:                                #   in Loop: Header=BB1_28 Depth=2
	movl	4(%r15,%r10,4), %edi
	incl	%edi
	testl	%edi, %edi
	jns	.LBB1_55
	jmp	.LBB1_54
	.p2align	4, 0x90
.LBB1_47:                               #   in Loop: Header=BB1_28 Depth=2
	cmpl	%r9d, %r10d
	jne	.LBB1_49
# BB#48:                                #   in Loop: Header=BB1_28 Depth=2
	movl	-4(%r15,%r10,4), %edi
	testl	%edi, %edi
	jns	.LBB1_55
	jmp	.LBB1_54
	.p2align	4, 0x90
.LBB1_49:                               #   in Loop: Header=BB1_28 Depth=2
	movl	(%r15,%r10,4), %ecx
	movl	4(%r15,%r10,4), %r11d
	cmpl	%r11d, %ecx
	jge	.LBB1_51
# BB#50:                                # %._crit_edge181
                                        #   in Loop: Header=BB1_28 Depth=2
	movl	-4(%r15,%r10,4), %r15d
	jmp	.LBB1_52
.LBB1_51:                               #   in Loop: Header=BB1_28 Depth=2
	leal	1(%rcx), %edi
	movl	-4(%r15,%r10,4), %r15d
	cmpl	%r15d, %edi
	jge	.LBB1_53
.LBB1_52:                               #   in Loop: Header=BB1_28 Depth=2
	leal	1(%r11), %r13d
	cmpl	%ecx, %r11d
	movl	%r13d, %edi
	cmovll	%r15d, %edi
	cmpl	%r15d, %r13d
	movl	28(%rsp), %r13d         # 4-byte Reload
	cmovll	%r15d, %edi
	.p2align	4, 0x90
.LBB1_53:                               #   in Loop: Header=BB1_28 Depth=2
	testl	%edi, %edi
	js	.LBB1_54
.LBB1_55:                               #   in Loop: Header=BB1_28 Depth=2
	cmpl	8(%rsp), %edi           # 4-byte Folded Reload
	movq	40(%rsp), %r15          # 8-byte Reload
	jge	.LBB1_62
# BB#56:                                #   in Loop: Header=BB1_28 Depth=2
	leal	(%rdi,%r10), %ecx
	cmpl	%r13d, %ecx
	jge	.LBB1_62
# BB#57:                                # %.lr.ph.preheader.i147
                                        #   in Loop: Header=BB1_28 Depth=2
	movslq	%edi, %rdi
	movslq	%ecx, %rcx
	incq	%rcx
	.p2align	4, 0x90
.LBB1_58:                               # %.lr.ph.i151
                                        #   Parent Loop BB1_26 Depth=1
                                        #     Parent Loop BB1_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbp,%rdi), %eax
	cmpb	-1(%r12,%rcx), %al
	jne	.LBB1_61
# BB#59:                                #   in Loop: Header=BB1_58 Depth=3
	incq	%rdi
	cmpq	%rsi, %rdi
	jge	.LBB1_61
# BB#60:                                #   in Loop: Header=BB1_58 Depth=3
	cmpq	%rdx, %rcx
	leaq	1(%rcx), %rcx
	jl	.LBB1_58
.LBB1_61:                               # %.lr.ph.i151.snake.exit156.loopexit_crit_edge
                                        #   in Loop: Header=BB1_28 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB1_62
	.p2align	4, 0x90
.LBB1_54:                               #   in Loop: Header=BB1_28 Depth=2
	movq	40(%rsp), %r15          # 8-byte Reload
.LBB1_62:                               # %snake.exit156
                                        #   in Loop: Header=BB1_28 Depth=2
	movl	%edi, (%rax,%r10,4)
	cmpq	%rbx, %r10
	leaq	1(%r10), %r10
	jl	.LBB1_28
# BB#30:                                # %.preheader
                                        #   in Loop: Header=BB1_26 Depth=1
	cmpl	%r9d, %r14d
	movq	120(%rsp), %rax         # 8-byte Reload
	jg	.LBB1_34
# BB#31:                                # %.lr.ph161.preheader
                                        #   in Loop: Header=BB1_26 Depth=1
	cmpq	$7, %rax
	movq	%rax, %rdi
	jbe	.LBB1_32
# BB#36:                                # %min.iters.checked197
                                        #   in Loop: Header=BB1_26 Depth=1
	movq	%rdi, %rcx
	andq	$-8, %rcx
	je	.LBB1_32
# BB#37:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_26 Depth=1
	movq	248(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, 256(%rsp)         # 8-byte Folded Reload
	jae	.LBB1_39
# BB#38:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_26 Depth=1
	movq	192(%rsp), %rax         # 8-byte Reload
	cmpq	200(%rsp), %rax         # 8-byte Folded Reload
	jb	.LBB1_32
.LBB1_39:                               # %vector.body188.preheader
                                        #   in Loop: Header=BB1_26 Depth=1
	testb	$3, 176(%rsp)           # 1-byte Folded Reload
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	je	.LBB1_40
# BB#41:                                # %vector.body188.prol.preheader
                                        #   in Loop: Header=BB1_26 Depth=1
	xorl	%ecx, %ecx
	movq	168(%rsp), %rdi         # 8-byte Reload
	movq	112(%rsp), %r11         # 8-byte Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	96(%rsp), %r14          # 8-byte Reload
	movq	160(%rsp), %r9          # 8-byte Reload
	movq	152(%rsp), %r10         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_42:                               # %vector.body188.prol
                                        #   Parent Loop BB1_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%r10,%rcx,4), %xmm0
	movups	(%r10,%rcx,4), %xmm1
	movups	%xmm0, -16(%r9,%rcx,4)
	movups	%xmm1, (%r9,%rcx,4)
	addq	$8, %rcx
	incq	%rdi
	jne	.LBB1_42
	jmp	.LBB1_43
.LBB1_40:                               #   in Loop: Header=BB1_26 Depth=1
	xorl	%ecx, %ecx
	movq	112(%rsp), %r11         # 8-byte Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	96(%rsp), %r14          # 8-byte Reload
.LBB1_43:                               # %vector.body188.prol.loopexit
                                        #   in Loop: Header=BB1_26 Depth=1
	cmpq	$24, 184(%rsp)          # 8-byte Folded Reload
	jb	.LBB1_45
	.p2align	4, 0x90
.LBB1_44:                               # %vector.body188
                                        #   Parent Loop BB1_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rax,%rcx,4), %xmm0
	movups	-96(%rax,%rcx,4), %xmm1
	movups	%xmm0, -112(%r11,%rcx,4)
	movups	%xmm1, -96(%r11,%rcx,4)
	movups	-80(%rax,%rcx,4), %xmm0
	movups	-64(%rax,%rcx,4), %xmm1
	movups	%xmm0, -80(%r11,%rcx,4)
	movups	%xmm1, -64(%r11,%rcx,4)
	movups	-48(%rax,%rcx,4), %xmm0
	movups	-32(%rax,%rcx,4), %xmm1
	movups	%xmm0, -48(%r11,%rcx,4)
	movups	%xmm1, -32(%r11,%rcx,4)
	movups	-16(%rax,%rcx,4), %xmm0
	movups	(%rax,%rcx,4), %xmm1
	movups	%xmm0, -16(%r11,%rcx,4)
	movups	%xmm1, (%r11,%rcx,4)
	addq	$32, %rcx
	cmpq	%rcx, %r14
	jne	.LBB1_44
.LBB1_45:                               # %middle.block189
                                        #   in Loop: Header=BB1_26 Depth=1
	movq	136(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, 120(%rsp)         # 8-byte Folded Reload
	je	.LBB1_34
# BB#46:                                #   in Loop: Header=BB1_26 Depth=1
	addq	%rax, %r8
	.p2align	4, 0x90
.LBB1_32:                               # %.lr.ph161.preheader226
                                        #   in Loop: Header=BB1_26 Depth=1
	decq	%r8
	movq	16(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_33:                               # %.lr.ph161
                                        #   Parent Loop BB1_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rcx,%r8,4), %eax
	movl	%eax, 4(%r15,%r8,4)
	incq	%r8
	cmpq	%rbx, %r8
	jl	.LBB1_33
.LBB1_34:                               # %._crit_edge
                                        #   in Loop: Header=BB1_26 Depth=1
	movl	8(%rsp), %eax           # 4-byte Reload
	movq	264(%rsp), %rcx         # 8-byte Reload
	cmpl	%eax, (%r15,%rcx,4)
	jge	.LBB1_35
# BB#63:                                #   in Loop: Header=BB1_26 Depth=1
	movl	88(%rsp), %ebx          # 4-byte Reload
	decl	%ebx
	movq	272(%rsp), %rcx         # 8-byte Reload
	incl	%ecx
	movl	92(%rsp), %edi          # 4-byte Reload
	decl	%edi
	movl	336(%rsp), %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	cmpl	%eax, %r8d
	leal	1(%r8), %eax
	movl	%eax, %r13d
	jl	.LBB1_26
.LBB1_64:
	movl	$-1, %eax
	jmp	.LBB1_65
.LBB1_66:
	leaq	(%r15,%r14,4), %rdi
	callq	free
	leaq	(%rbx,%r14,4), %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB1_65
.LBB1_35:
	movq	144(%rsp), %rbx         # 8-byte Reload
	leaq	(%r15,%rbx,4), %rdi
	callq	free
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,4), %rdi
	callq	free
	movq	48(%rsp), %rax          # 8-byte Reload
.LBB1_65:                               # %.loopexit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	align_get_dist, .Lfunc_end1-align_get_dist
	.cfi_endproc

	.globl	Condense_both_Ends
	.p2align	4, 0x90
	.type	Condense_both_Ends,@function
Condense_both_Ends:                     # @Condense_both_Ends
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -32
.Lcfi44:
	.cfi_offset %r14, -24
.Lcfi45:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	(%rdi), %rbx
	movq	$0, (%r15)
	testq	%rbx, %rbx
	jne	.LBB2_2
	jmp	.LBB2_7
.LBB2_6:                                # %.critedge
                                        #   in Loop: Header=BB2_2 Depth=1
	testq	%rdi, %rdi
	movq	%r14, %rax
	cmovneq	%r15, %rax
	movq	%rbx, (%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_2
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=1
	movl	8(%rdi), %eax
	addl	%eax, 8(%rbx)
	movq	(%rdi), %rax
	movq	%rax, (%rbx)
	callq	free
.LBB2_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_5
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movzbl	12(%rbx), %eax
	cmpb	12(%rdi), %al
	je	.LBB2_4
	jmp	.LBB2_6
.LBB2_5:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%edi, %edi
	jmp	.LBB2_6
.LBB2_7:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	Condense_both_Ends, .Lfunc_end2-Condense_both_Ends
	.cfi_endproc

	.globl	S2A
	.p2align	4, 0x90
	.type	S2A,@function
S2A:                                    # @S2A
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	movq	%rsi, %rax
	je	.LBB3_14
# BB#1:                                 # %.lr.ph27
	movq	%rsi, %rax
	testl	%edx, %edx
	je	.LBB3_2
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph27.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_11 Depth 2
	movb	12(%rdi), %cl
	cmpb	$3, %cl
	jne	.LBB3_7
# BB#9:                                 # %.preheader.us
                                        #   in Loop: Header=BB3_6 Depth=1
	cmpl	$0, 8(%rdi)
	jle	.LBB3_8
# BB#10:                                # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB3_6 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_11:                               # %.lr.ph.us
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, (%rax)
	addq	$4, %rax
	incl	%ecx
	cmpl	8(%rdi), %ecx
	jl	.LBB3_11
	jmp	.LBB3_8
	.p2align	4, 0x90
.LBB3_7:                                # %.sink.split.us
                                        #   in Loop: Header=BB3_6 Depth=1
	movl	8(%rdi), %r8d
	movl	%r8d, %edx
	negl	%edx
	cmpb	$2, %cl
	cmovnel	%r8d, %edx
	movl	%edx, (%rax)
	addq	$4, %rax
.LBB3_8:                                # %.loopexit.us
                                        #   in Loop: Header=BB3_6 Depth=1
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB3_6
	jmp	.LBB3_14
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph27.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_5 Depth 2
	movb	12(%rdi), %cl
	cmpb	$3, %cl
	jne	.LBB3_12
# BB#3:                                 # %.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpl	$0, 8(%rdi)
	jle	.LBB3_13
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, (%rax)
	addq	$4, %rax
	incl	%ecx
	cmpl	8(%rdi), %ecx
	jl	.LBB3_5
	jmp	.LBB3_13
	.p2align	4, 0x90
.LBB3_12:                               # %.sink.split
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	8(%rdi), %r8d
	movl	%r8d, %edx
	negl	%edx
	cmpb	$2, %cl
	cmovel	%r8d, %edx
	movl	%edx, (%rax)
	addq	$4, %rax
.LBB3_13:                               # %.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB3_2
.LBB3_14:                               # %._crit_edge
	subq	%rsi, %rax
	shrq	$2, %rax
	movl	%eax, -4(%rsi)
	retq
.Lfunc_end3:
	.size	S2A, .Lfunc_end3-S2A
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
	.text
	.globl	IDISPLAY
	.p2align	4, 0x90
	.type	IDISPLAY,@function
IDISPLAY:                               # @IDISPLAY
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 176
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movl	%ecx, %r10d
	movl	%edx, %r11d
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	184(%rsp), %rax
	movq	(%rax), %rdx
	movl	8(%rax), %ecx
	leal	-1(%rcx), %eax
	movq	(%rdx,%rax,8), %rsi
	movl	options+68(%rip), %edi
	addl	8(%rsi), %edi
	movl	$1, %esi
	cmpl	$10, %edi
	jb	.LBB4_3
# BB#1:                                 # %.lr.ph.i.preheader
	movl	$1, %esi
	movl	$3435973837, %ebp       # imm = 0xCCCCCCCD
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%edi, %ebx
	imulq	%rbp, %rbx
	shrq	$35, %rbx
	incl	%esi
	cmpl	$99, %edi
	movl	%ebx, %edi
	ja	.LBB4_2
.LBB4_3:                                # %get_pos_width.exit
	movl	176(%rsp), %ebp
	cmpl	$7, %esi
	movl	$7, %edi
	cmoval	%esi, %edi
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	testl	%ecx, %ecx
	je	.LBB4_4
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph194
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%r13,8), %rsi
	cmpl	%r9d, (%rsi)
	jne	.LBB4_7
# BB#6:                                 #   in Loop: Header=BB4_5 Depth=1
	cmpl	%ebp, 4(%rsi)
	je	.LBB4_8
.LBB4_7:                                # %.thread
                                        #   in Loop: Header=BB4_5 Depth=1
	incq	%r13
	cmpl	%ecx, %r13d
	jb	.LBB4_5
	jmp	.LBB4_8
.LBB4_4:
                                        # implicit-def: %RSI
.LBB4_8:                                # %.thread189
	cmpl	%ecx, %r13d
	jae	.LBB4_55
# BB#9:
	movl	$-1, 20(%rsp)           # 4-byte Folded Spill
	cmpl	%eax, %r13d
	movq	48(%rsp), %rdi          # 8-byte Reload
	jae	.LBB4_11
# BB#10:
	movl	8(%rsi), %eax
	incl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
.LBB4_11:                               # %.preheader
	cmpl	$0, 192(%rsp)
	movb	$62, 11(%rsp)           # 1-byte Folded Spill
	jg	.LBB4_13
# BB#12:                                # %.preheader
	movb	$60, 11(%rsp)           # 1-byte Folded Spill
.LBB4_13:                               # %.preheader
	movl	$BLINE, %eax
	movd	%rax, %xmm0
	movl	$ALINE, %eax
	movd	%rax, %xmm2
	punpcklqdq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0]
	movdqa	.LCPI4_0(%rip), %xmm3   # xmm3 = [1,1]
	movl	$0, %ebx
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movl	$0, %r14d
	movl	$0, %r15d
	movl	$0, 36(%rsp)            # 4-byte Folded Spill
	movl	%r9d, %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movb	$42, %bpl
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movl	%r10d, 44(%rsp)         # 4-byte Spill
	movl	%r11d, 40(%rsp)         # 4-byte Spill
	movdqa	%xmm2, 96(%rsp)         # 16-byte Spill
	jmp	.LBB4_14
	.p2align	4, 0x90
.LBB4_53:                               #   in Loop: Header=BB4_14 Depth=1
	incl	36(%rsp)                # 4-byte Folded Spill
	movl	32(%rsp), %edx          # 4-byte Reload
	addl	options+68(%rip), %edx
	movl	$.L.str.6, %edi
	movl	$ALINE, %ecx
	movl	$.L.str.7, %r9d
	movl	$0, %eax
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%esi, %r8d
	pushq	$BLINE
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	pushq	64(%rsp)                # 8-byte Folded Reload
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	pushq	%rsi
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	$CLINE
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$32, %rsp
.Lcfi63:
	.cfi_adjust_cfa_offset -32
	movq	72(%rsp), %r9           # 8-byte Reload
	leal	(%r14,%r9), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	176(%rsp), %eax
	leal	(%r15,%rax), %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	88(%rsp), %r8           # 8-byte Reload
	movl	44(%rsp), %r10d         # 4-byte Reload
	movl	40(%rsp), %r11d         # 4-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	movdqa	96(%rsp), %xmm2         # 16-byte Reload
	movb	19(%rsp), %bpl          # 1-byte Reload
	movdqa	.LCPI4_0(%rip), %xmm3   # xmm3 = [1,1]
.LBB4_14:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_15 Depth 2
                                        #     Child Loop BB4_50 Depth 2
	movl	$CLINE, %eax
	movdqa	%xmm2, %xmm0
	.p2align	4, 0x90
.LBB4_15:                               #   Parent Loop BB4_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%r11d, %r14d
	jb	.LBB4_17
# BB#16:                                #   in Loop: Header=BB4_15 Depth=2
	cmpl	%r10d, %r15d
	jae	.LBB4_54
.LBB4_17:                               # %.critedge
                                        #   in Loop: Header=BB4_15 Depth=2
	testl	%ebx, %ebx
	jne	.LBB4_22
# BB#18:                                #   in Loop: Header=BB4_15 Depth=2
	movl	(%r8), %ebx
	addq	$4, %r8
	testl	%ebx, %ebx
	je	.LBB4_19
.LBB4_22:                               # %.critedge186
                                        #   in Loop: Header=BB4_15 Depth=2
	testl	%ebx, %ebx
	jle	.LBB4_24
# BB#23:                                #   in Loop: Header=BB4_15 Depth=2
	movd	%xmm0, %rcx
	movb	$32, (%rcx)
	incl	%r15d
	movzbl	(%rdi,%r15), %ecx
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm3, %xmm0
	movd	%xmm1, %rdx
	movb	%cl, (%rdx)
	movb	$45, (%rax)
	incq	%rax
	decl	%ebx
	jmp	.LBB4_45
	.p2align	4, 0x90
.LBB4_24:                               #   in Loop: Header=BB4_15 Depth=2
	leal	(%r14,%r9), %ecx
	cmpl	20(%rsp), %ecx          # 4-byte Folded Reload
	jne	.LBB4_35
# BB#25:                                #   in Loop: Header=BB4_15 Depth=2
	movq	24(%rsi), %rcx
	sarq	$56, %rcx
	testl	%ecx, %ecx
	movb	$61, %bpl
	movb	$61, %cl
	js	.LBB4_27
# BB#26:                                #   in Loop: Header=BB4_15 Depth=2
	movzbl	11(%rsp), %ecx          # 1-byte Folded Reload
.LBB4_27:                               #   in Loop: Header=BB4_15 Depth=2
	cmpl	$0, 192(%rsp)
	je	.LBB4_29
# BB#28:                                #   in Loop: Header=BB4_15 Depth=2
	movl	%ecx, %ebp
.LBB4_29:                               #   in Loop: Header=BB4_15 Depth=2
	incl	%r13d
	movq	184(%rsp), %rdx
	movl	8(%rdx), %ecx
	cmpl	%ecx, %r13d
	jae	.LBB4_30
# BB#31:                                #   in Loop: Header=BB4_15 Depth=2
	movq	(%rdx), %rdx
	movl	%r13d, %esi
	movq	(%rdx,%rsi,8), %rsi
	jmp	.LBB4_32
.LBB4_35:                               #   in Loop: Header=BB4_15 Depth=2
	movq	%rdi, %r12
	movl	12(%rsp), %edi          # 4-byte Reload
	cmpl	$8, %edi
	ja	.LBB4_44
# BB#36:                                #   in Loop: Header=BB4_15 Depth=2
	movl	%edi, %ecx
	jmpq	*.LJTI4_0(,%rcx,8)
.LBB4_41:                               #   in Loop: Header=BB4_15 Depth=2
	incl	%r14d
	movq	24(%rsp), %rcx          # 8-byte Reload
	movzbl	(%rcx,%r14), %ecx
	movd	%xmm0, %rdx
	movb	%cl, (%rdx)
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm3, %xmm0
	movd	%xmm1, %rcx
	movb	$32, (%rcx)
	movb	%bpl, (%rax)
	incq	%rax
	jmp	.LBB4_39
.LBB4_19:                               #   in Loop: Header=BB4_15 Depth=2
	movl	%ebp, %r12d
	movq	%rsi, %rbp
	incl	%r14d
	movq	24(%rsp), %rcx          # 8-byte Reload
	movzbl	(%rcx,%r14), %ecx
	movd	%xmm0, %rdx
	movb	%cl, (%rdx)
	incl	%r15d
	movzbl	(%rdi,%r15), %edx
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	movd	%xmm1, %rsi
	movb	%dl, (%rsi)
	cmpb	%dl, %cl
	movb	$124, %cl
	je	.LBB4_21
# BB#20:                                #   in Loop: Header=BB4_15 Depth=2
	movb	$32, %cl
.LBB4_21:                               #   in Loop: Header=BB4_15 Depth=2
	paddq	%xmm3, %xmm0
	movb	%cl, (%rax)
	incq	%rax
	xorl	%ebx, %ebx
	movq	%rbp, %rsi
	movl	%r12d, %ebp
	jmp	.LBB4_45
.LBB4_30:                               #   in Loop: Header=BB4_15 Depth=2
	xorl	%esi, %esi
.LBB4_32:                               #   in Loop: Header=BB4_15 Depth=2
	decl	%ecx
	movl	$-1, 20(%rsp)           # 4-byte Folded Spill
	cmpl	%ecx, %r13d
	jae	.LBB4_34
# BB#33:                                #   in Loop: Header=BB4_15 Depth=2
	movl	8(%rsi), %ecx
	incl	%ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
.LBB4_34:                               #   in Loop: Header=BB4_15 Depth=2
	movb	%bpl, (%rax)
	incq	%rax
	incl	%r14d
	movq	24(%rsp), %rcx          # 8-byte Reload
	movzbl	(%rcx,%r14), %ecx
	movd	%xmm0, %rdx
	movb	%cl, (%rdx)
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm3, %xmm0
	movd	%xmm1, %rcx
	movb	$32, (%rcx)
	incl	%ebx
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
	jmp	.LBB4_45
.LBB4_38:                               #   in Loop: Header=BB4_15 Depth=2
	movd	%xmm0, %rcx
	movb	$46, (%rcx)
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm3, %xmm0
	movd	%xmm1, %rcx
	movb	$32, (%rcx)
	movb	$46, (%rax)
	incq	%rax
	incl	%r14d
.LBB4_39:                               #   in Loop: Header=BB4_15 Depth=2
	incl	%ebx
	incl	%edi
	movl	%edi, 12(%rsp)          # 4-byte Spill
	jmp	.LBB4_44
.LBB4_37:                               #   in Loop: Header=BB4_15 Depth=2
	movb	$45, (%rax)
	incq	%rax
	incl	%r14d
	movq	24(%rsp), %rcx          # 8-byte Reload
	movzbl	(%rcx,%r14), %ecx
	movd	%xmm0, %rdx
	movb	%cl, (%rdx)
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm3, %xmm0
	movd	%xmm1, %rcx
	movb	$32, (%rcx)
	jmp	.LBB4_43
.LBB4_40:                               #   in Loop: Header=BB4_15 Depth=2
	movd	%xmm0, %rcx
	movb	$46, (%rcx)
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm3, %xmm0
	movd	%xmm1, %rcx
	movb	$32, (%rcx)
	movb	$46, (%rax)
	incq	%rax
	addl	$-3, %r14d
	subl	%ebx, %r14d
	movl	$6, 12(%rsp)            # 4-byte Folded Spill
	movl	$-3, %ebx
	jmp	.LBB4_44
.LBB4_42:                               #   in Loop: Header=BB4_15 Depth=2
	incl	%r14d
	movq	24(%rsp), %rcx          # 8-byte Reload
	movzbl	(%rcx,%r14), %ecx
	movd	%xmm0, %rdx
	movb	%cl, (%rdx)
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm3, %xmm0
	movd	%xmm1, %rcx
	movb	$32, (%rcx)
	movb	%bpl, (%rax)
	incq	%rax
.LBB4_43:                               #   in Loop: Header=BB4_15 Depth=2
	incl	%ebx
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
.LBB4_44:                               #   in Loop: Header=BB4_15 Depth=2
	movq	%r12, %rdi
	.p2align	4, 0x90
.LBB4_45:                               #   in Loop: Header=BB4_15 Depth=2
	movd	%xmm0, %r12
	movl	$ALINE+50, %ecx
	cmpq	%rcx, %r12
	jae	.LBB4_48
# BB#46:                                #   in Loop: Header=BB4_15 Depth=2
	cmpl	%r11d, %r14d
	jb	.LBB4_15
# BB#47:                                #   in Loop: Header=BB4_15 Depth=2
	cmpl	%r10d, %r15d
	jb	.LBB4_15
.LBB4_48:                               #   in Loop: Header=BB4_14 Depth=1
	movb	%bpl, 19(%rsp)          # 1-byte Spill
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movb	$0, (%rax)
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %rax
	movb	$0, (%rax)
	movb	$0, (%r12)
	imull	$50, 36(%rsp), %edx     # 4-byte Folded Reload
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movq	64(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movl	$ALINE+10, %eax
	cmpq	%rax, %r12
	movl	$ALINE+10, %ebp
	jb	.LBB4_51
# BB#49:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_14 Depth=1
	movl	$ALINE+10, %ebp
	.p2align	4, 0x90
.LBB4_50:                               # %.lr.ph
                                        #   Parent Loop BB4_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	addq	$10, %rbp
	cmpq	%r12, %rbp
	jbe	.LBB4_50
.LBB4_51:                               # %._crit_edge
                                        #   in Loop: Header=BB4_14 Depth=1
	addq	$5, %r12
	cmpq	%r12, %rbp
	ja	.LBB4_53
# BB#52:                                #   in Loop: Header=BB4_14 Depth=1
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB4_53
.LBB4_54:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_55:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	fatal
.Lfunc_end4:
	.size	IDISPLAY, .Lfunc_end4-IDISPLAY
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_37
	.quad	.LBB4_41
	.quad	.LBB4_41
	.quad	.LBB4_38
	.quad	.LBB4_38
	.quad	.LBB4_40
	.quad	.LBB4_41
	.quad	.LBB4_41
	.quad	.LBB4_42

	.text
	.globl	Free_script
	.p2align	4, 0x90
	.type	Free_script,@function
Free_script:                            # @Free_script
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 16
.Lcfi65:
	.cfi_offset %rbx, -16
	testq	%rdi, %rdi
	je	.LBB5_2
	.p2align	4, 0x90
.LBB5_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB5_1
.LBB5_2:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end5:
	.size	Free_script, .Lfunc_end5-Free_script
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"align.c: warning: something wrong when aligning."
	.size	.L.str, 49

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"align.c: warning: something wrong when dividing\n"
	.size	.L.str.1, 49

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"align.c: Alignment fragment not found.\n"
	.size	.L.str.2, 40

	.type	ALINE,@object           # @ALINE
	.local	ALINE
	.comm	ALINE,51,16
	.type	BLINE,@object           # @BLINE
	.local	BLINE
	.comm	BLINE,51,16
	.type	CLINE,@object           # @CLINE
	.local	CLINE
	.comm	CLINE,51,16
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n%*u "
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"    .    :"
	.size	.L.str.4, 11

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"    ."
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n%*u %s\n%*s %s\n%*u %s\n"
	.size	.L.str.6, 23

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	" "
	.size	.L.str.7, 2

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"TROUBLE!!! startx:  %5d,  starty:  %5d\n"
	.size	.L.str.8, 40

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"TROUBLE!!! x:  %5d,  y:  %5d\n"
	.size	.L.str.9, 30


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
