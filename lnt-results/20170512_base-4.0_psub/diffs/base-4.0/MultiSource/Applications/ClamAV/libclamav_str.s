	.text
	.file	"libclamav_str.bc"
	.globl	cli_hex2ui
	.p2align	4, 0x90
	.type	cli_hex2ui,@function
cli_hex2ui:                             # @cli_hex2ui
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	callq	strlen
	movq	%rax, %r15
	testb	$1, %r15b
	jne	.LBB0_42
# BB#1:
	movl	%r15d, %edi
	shrl	%edi
	incl	%edi
	movl	$2, %esi
	callq	cli_calloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_49
# BB#2:                                 # %.preheader
	testl	%r15d, %r15d
	je	.LBB0_50
# BB#3:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	movq	%r14, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	movsbl	(%r12,%rax), %ebx
	leal	1(%rbp), %eax
	cmpl	$63, %ebx
	jne	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	movw	$256, %r13w             # imm = 0x100
	cmpb	$63, (%r12,%rax)
	je	.LBB0_41
.LBB0_6:                                # %thread-pre-split
                                        #   in Loop: Header=BB0_4 Depth=1
	movslq	%ebx, %r14
	movsbl	(%r12,%rax), %r13d
	cmpl	$63, %r13d
	jne	.LBB0_13
# BB#7:                                 #   in Loop: Header=BB0_4 Depth=1
	testb	%r14b, %r14b
	js	.LBB0_45
# BB#8:                                 #   in Loop: Header=BB0_4 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$8, 1(%rax,%r14,2)
	jne	.LBB0_21
# BB#9:                                 #   in Loop: Header=BB0_4 Depth=1
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_11
# BB#10:                                #   in Loop: Header=BB0_4 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movl	(%rax,%r14,4), %ebx
.LBB0_11:                               # %tolower.exit.i
                                        #   in Loop: Header=BB0_4 Depth=1
	leal	-97(%rbx), %eax
	cmpl	$5, %eax
	ja	.LBB0_44
# BB#12:                                # %cli_hex2int.exit.thread69
                                        #   in Loop: Header=BB0_4 Depth=1
	addl	$-87, %ebx
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_13:                               #   in Loop: Header=BB0_4 Depth=1
	cmpb	$40, %bl
	je	.LBB0_23
# BB#14:                                #   in Loop: Header=BB0_4 Depth=1
	movslq	%r13d, %rax
	cmpb	$63, %bl
	jne	.LBB0_24
# BB#15:                                #   in Loop: Header=BB0_4 Depth=1
	testb	%al, %al
	movq	8(%rsp), %r14           # 8-byte Reload
	js	.LBB0_47
# BB#16:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, %rbx
	callq	__ctype_b_loc
	movq	%rbx, %rcx
	movq	(%rax), %rax
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB0_30
# BB#17:                                #   in Loop: Header=BB0_4 Depth=1
	movl	%r13d, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, %rbx
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movl	(%rax,%rbx,4), %r13d
.LBB0_19:                               # %tolower.exit.i56
                                        #   in Loop: Header=BB0_4 Depth=1
	leal	-97(%r13), %eax
	cmpl	$5, %eax
	ja	.LBB0_46
# BB#20:                                # %cli_hex2int.exit58.thread71
                                        #   in Loop: Header=BB0_4 Depth=1
	addl	$-87, %r13d
	jmp	.LBB0_31
.LBB0_21:                               # %cli_hex2int.exit
                                        #   in Loop: Header=BB0_4 Depth=1
	addl	$-48, %ebx
	js	.LBB0_45
.LBB0_22:                               #   in Loop: Header=BB0_4 Depth=1
	orl	$48, %ebx
	shll	$4, %ebx
	movw	%bx, %r13w
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB0_41
.LBB0_23:                               #   in Loop: Header=BB0_4 Depth=1
	movw	$512, %r13w             # imm = 0x200
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB0_41
.LBB0_24:                               #   in Loop: Header=BB0_4 Depth=1
	testb	%r14b, %r14b
	js	.LBB0_45
# BB#25:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rax, 16(%rsp)          # 8-byte Spill
	callq	__ctype_b_loc
	movq	(%rax), %rdx
	testb	$8, 1(%rdx,%r14,2)
	jne	.LBB0_32
# BB#26:                                #   in Loop: Header=BB0_4 Depth=1
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_28
# BB#27:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rdx, %rbx
	callq	__ctype_tolower_loc
	movq	%rbx, %rdx
	movq	(%rax), %rax
	movl	(%rax,%r14,4), %ebx
.LBB0_28:                               # %tolower.exit.i61
                                        #   in Loop: Header=BB0_4 Depth=1
	leal	-97(%rbx), %eax
	cmpl	$5, %eax
	ja	.LBB0_44
# BB#29:                                # %cli_hex2int.exit63.thread73
                                        #   in Loop: Header=BB0_4 Depth=1
	addl	$-87, %ebx
	movq	16(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB0_33
.LBB0_30:                               # %cli_hex2int.exit58
                                        #   in Loop: Header=BB0_4 Depth=1
	addl	$-48, %r13d
	js	.LBB0_47
.LBB0_31:                               #   in Loop: Header=BB0_4 Depth=1
	orl	$1024, %r13d            # imm = 0x400
	jmp	.LBB0_41
.LBB0_32:                               # %cli_hex2int.exit63
                                        #   in Loop: Header=BB0_4 Depth=1
	addl	$-48, %ebx
	movq	16(%rsp), %rcx          # 8-byte Reload
	js	.LBB0_45
.LBB0_33:                               #   in Loop: Header=BB0_4 Depth=1
	testb	%r13b, %r13b
	movq	8(%rsp), %r14           # 8-byte Reload
	js	.LBB0_47
# BB#34:                                #   in Loop: Header=BB0_4 Depth=1
	testb	$8, 1(%rdx,%rcx,2)
	jne	.LBB0_39
# BB#35:                                #   in Loop: Header=BB0_4 Depth=1
	movl	%r13d, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_37
# BB#36:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rcx, %r13
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movl	(%rax,%r13,4), %r13d
.LBB0_37:                               # %tolower.exit.i66
                                        #   in Loop: Header=BB0_4 Depth=1
	leal	-97(%r13), %eax
	cmpl	$5, %eax
	ja	.LBB0_46
# BB#38:                                # %cli_hex2int.exit68.thread75
                                        #   in Loop: Header=BB0_4 Depth=1
	addl	$-87, %r13d
	jmp	.LBB0_40
.LBB0_39:                               # %cli_hex2int.exit68
                                        #   in Loop: Header=BB0_4 Depth=1
	addl	$-48, %r13d
	js	.LBB0_47
.LBB0_40:                               #   in Loop: Header=BB0_4 Depth=1
	shll	$4, %ebx
	addl	%r13d, %ebx
	movw	%bx, %r13w
	.p2align	4, 0x90
.LBB0_41:                               #   in Loop: Header=BB0_4 Depth=1
	movw	%r13w, (%r14,%rbp)
	addq	$2, %rbp
	cmpl	%r15d, %ebp
	jb	.LBB0_4
	jmp	.LBB0_50
.LBB0_42:
	xorl	%r14d, %r14d
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	movl	%r15d, %edx
	callq	cli_errmsg
	jmp	.LBB0_50
.LBB0_44:
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
.LBB0_45:                               # %cli_hex2int.exit.thread
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB0_48
.LBB0_46:
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_errmsg
.LBB0_47:                               # %cli_hex2int.exit58.thread
	movq	%r14, %rdi
.LBB0_48:                               # %.loopexit
	callq	free
.LBB0_49:                               # %.loopexit
	xorl	%r14d, %r14d
.LBB0_50:                               # %.loopexit
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cli_hex2ui, .Lfunc_end0-cli_hex2ui
	.cfi_endproc

	.globl	cli_hex2str
	.p2align	4, 0x90
	.type	cli_hex2str,@function
cli_hex2str:                            # @cli_hex2str
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	callq	strlen
	movq	%rax, %rbx
	testb	$1, %bl
	jne	.LBB1_26
# BB#1:
	movl	%ebx, %eax
	shrl	$31, %eax
	addl	%ebx, %eax
	sarl	%eax
	incl	%eax
	movslq	%eax, %rdi
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB1_28
# BB#2:                                 # %.preheader
	testl	%ebx, %ebx
	jle	.LBB1_29
# BB#3:                                 # %.lr.ph.preheader
	movslq	%ebx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r15, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%r15,%r13), %ebx
	testl	%ebx, %ebx
	movl	$-1, %r12d
	js	.LBB1_13
# BB#5:                                 #   in Loop: Header=BB1_4 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%ebx, %r14
	testb	$8, 1(%rax,%r14,2)
	jne	.LBB1_10
# BB#6:                                 #   in Loop: Header=BB1_4 Depth=1
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB1_8
# BB#7:                                 #   in Loop: Header=BB1_4 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movl	(%rax,%r14,4), %ebx
.LBB1_8:                                # %tolower.exit.i
                                        #   in Loop: Header=BB1_4 Depth=1
	leal	-97(%rbx), %eax
	cmpl	$5, %eax
	ja	.LBB1_12
# BB#9:                                 #   in Loop: Header=BB1_4 Depth=1
	addl	$-87, %ebx
	jmp	.LBB1_11
	.p2align	4, 0x90
.LBB1_10:                               #   in Loop: Header=BB1_4 Depth=1
	addl	$-48, %ebx
.LBB1_11:                               #   in Loop: Header=BB1_4 Depth=1
	movl	%ebx, %r12d
	testb	%r12b, %r12b
	jns	.LBB1_14
	jmp	.LBB1_27
.LBB1_12:                               #   in Loop: Header=BB1_4 Depth=1
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
	.p2align	4, 0x90
.LBB1_13:                               # %cli_hex2int.exit
                                        #   in Loop: Header=BB1_4 Depth=1
	testb	%r12b, %r12b
	js	.LBB1_27
.LBB1_14:                               #   in Loop: Header=BB1_4 Depth=1
	movsbl	1(%r15,%r13), %ebx
	testl	%ebx, %ebx
	movl	$-1, %r14d
	js	.LBB1_23
# BB#15:                                #   in Loop: Header=BB1_4 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%ebx, %r15
	testb	$8, 1(%rax,%r15,2)
	jne	.LBB1_20
# BB#16:                                #   in Loop: Header=BB1_4 Depth=1
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_4 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movl	(%rax,%r15,4), %ebx
.LBB1_18:                               # %tolower.exit.i34
                                        #   in Loop: Header=BB1_4 Depth=1
	leal	-97(%rbx), %eax
	cmpl	$5, %eax
	ja	.LBB1_21
# BB#19:                                #   in Loop: Header=BB1_4 Depth=1
	addl	$-87, %ebx
	movl	%ebx, %r14d
	jmp	.LBB1_22
	.p2align	4, 0x90
.LBB1_20:                               #   in Loop: Header=BB1_4 Depth=1
	addl	$-48, %ebx
	movl	%ebx, %r14d
	jmp	.LBB1_22
.LBB1_21:                               #   in Loop: Header=BB1_4 Depth=1
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
.LBB1_22:                               # %cli_hex2int.exit36
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
.LBB1_23:                               # %cli_hex2int.exit36
                                        #   in Loop: Header=BB1_4 Depth=1
	testb	%r14b, %r14b
	js	.LBB1_27
# BB#24:                                #   in Loop: Header=BB1_4 Depth=1
	shll	$4, %r12d
	addl	%r12d, %r14d
	movb	%r14b, (%rbp)
	incq	%rbp
	addq	$2, %r13
	cmpq	16(%rsp), %r13          # 8-byte Folded Reload
	jl	.LBB1_4
# BB#25:
	movq	(%rsp), %rbp            # 8-byte Reload
	jmp	.LBB1_29
.LBB1_26:
	xorl	%ebp, %ebp
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movl	%ebx, %edx
	callq	cli_errmsg
	jmp	.LBB1_29
.LBB1_27:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
.LBB1_28:                               # %.loopexit
	xorl	%ebp, %ebp
.LBB1_29:                               # %.loopexit
	movq	%rbp, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	cli_hex2str, .Lfunc_end1-cli_hex2str
	.cfi_endproc

	.globl	cli_hex2num
	.p2align	4, 0x90
	.type	cli_hex2num,@function
cli_hex2num:                            # @cli_hex2num
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	strlen
	movq	%rax, %rcx
	testb	$1, %cl
	jne	.LBB2_3
# BB#1:                                 # %.preheader
	testl	%ecx, %ecx
	jle	.LBB2_2
# BB#4:                                 # %.lr.ph.preheader
	movslq	%ecx, %r15
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%r14,%r12), %ebx
	testl	%ebx, %ebx
	js	.LBB2_15
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	movslq	%ebx, %r13
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$8, 1(%rax,%r13,2)
	jne	.LBB2_12
# BB#7:                                 #   in Loop: Header=BB2_5 Depth=1
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB2_9
# BB#8:                                 #   in Loop: Header=BB2_5 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movl	(%rax,%r13,4), %ebx
.LBB2_9:                                # %tolower.exit.i
                                        #   in Loop: Header=BB2_5 Depth=1
	leal	-97(%rbx), %eax
	cmpl	$5, %eax
	ja	.LBB2_11
# BB#10:                                # %cli_hex2int.exit.thread21
                                        #   in Loop: Header=BB2_5 Depth=1
	addl	$-87, %ebx
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_12:                               # %cli_hex2int.exit
                                        #   in Loop: Header=BB2_5 Depth=1
	cmpb	$48, %bl
	jl	.LBB2_15
# BB#13:                                #   in Loop: Header=BB2_5 Depth=1
	addl	$-48, %ebx
.LBB2_14:                               #   in Loop: Header=BB2_5 Depth=1
	shll	$4, %ebp
	orl	%ebx, %ebp
	incq	%r12
	cmpq	%r15, %r12
	jl	.LBB2_5
	jmp	.LBB2_15
.LBB2_3:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	movl	%ecx, %edx
	callq	cli_errmsg
	movl	$-1, %ebp
	jmp	.LBB2_15
.LBB2_2:
	xorl	%ebp, %ebp
	jmp	.LBB2_15
.LBB2_11:
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
.LBB2_15:                               # %cli_hex2int.exit.thread
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	cli_hex2num, .Lfunc_end2-cli_hex2num
	.cfi_endproc

	.globl	cli_str2hex
	.p2align	4, 0x90
	.type	cli_str2hex,@function
cli_str2hex:                            # @cli_str2hex
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 32
.Lcfi42:
	.cfi_offset %rbx, -24
.Lcfi43:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	leal	1(%rbx,%rbx), %edi
	movl	$1, %esi
	callq	cli_calloc
	testq	%rax, %rax
	je	.LBB3_1
# BB#2:                                 # %.preheader
	testl	%ebx, %ebx
	je	.LBB3_9
# BB#3:                                 # %.lr.ph.preheader
	movl	%ebx, %ecx
	testb	$1, %cl
	jne	.LBB3_5
# BB#4:
	xorl	%esi, %esi
	xorl	%edx, %edx
	cmpl	$1, %ebx
	jne	.LBB3_7
	jmp	.LBB3_9
.LBB3_1:
	xorl	%eax, %eax
	jmp	.LBB3_9
.LBB3_5:                                # %.lr.ph.prol
	movzbl	(%r14), %edx
	shrq	$4, %rdx
	movb	.Lcli_str2hex.HEX(%rdx), %dl
	movb	%dl, (%rax)
	movzbl	(%r14), %edx
	andl	$15, %edx
	movb	.Lcli_str2hex.HEX(%rdx), %dl
	movb	%dl, 1(%rax)
	movl	$2, %edx
	movl	$1, %esi
	cmpl	$1, %ebx
	je	.LBB3_9
.LBB3_7:                                # %.lr.ph.preheader.new
	subq	%rsi, %rcx
	leaq	1(%r14,%rsi), %rsi
	.p2align	4, 0x90
.LBB3_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rsi), %edi
	shrq	$4, %rdi
	movzbl	.Lcli_str2hex.HEX(%rdi), %ebx
	movl	%edx, %edi
	movb	%bl, (%rax,%rdi)
	movzbl	-1(%rsi), %edi
	andl	$15, %edi
	movzbl	.Lcli_str2hex.HEX(%rdi), %ebx
	leal	1(%rdx), %edi
	movb	%bl, (%rax,%rdi)
	leal	2(%rdx), %edi
	movzbl	(%rsi), %ebx
	shrq	$4, %rbx
	movzbl	.Lcli_str2hex.HEX(%rbx), %ebx
	movb	%bl, (%rax,%rdi)
	movzbl	(%rsi), %edi
	andl	$15, %edi
	movzbl	.Lcli_str2hex.HEX(%rdi), %ebx
	leal	3(%rdx), %edi
	movb	%bl, (%rax,%rdi)
	addl	$4, %edx
	addq	$2, %rsi
	addq	$-2, %rcx
	jne	.LBB3_8
.LBB3_9:                                # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	cli_str2hex, .Lfunc_end3-cli_str2hex
	.cfi_endproc

	.globl	cli_utf16toascii
	.p2align	4, 0x90
	.type	cli_utf16toascii,@function
cli_utf16toascii:                       # @cli_utf16toascii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	cmpl	$1, %r14d
	ja	.LBB4_2
# BB#1:
	xorl	%ebp, %ebp
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB4_7
.LBB4_2:
	movl	%r14d, %eax
	andl	$1, %eax
	subl	%eax, %r14d
	movl	%r14d, %edi
	shrl	%edi
	incl	%edi
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB4_3
# BB#4:                                 # %.preheader
	testl	%r14d, %r14d
	je	.LBB4_7
# BB#5:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rcx), %edx
	movzbl	(%rbx,%rdx), %edx
	shlb	$4, %dl
	movl	%eax, %esi
	movb	%dl, (%rbp,%rsi)
	movl	%ecx, %edi
	addb	(%rbx,%rdi), %dl
	movb	%dl, (%rbp,%rsi)
	addl	$2, %ecx
	incl	%eax
	cmpl	%r14d, %ecx
	jb	.LBB4_6
	jmp	.LBB4_7
.LBB4_3:
	xorl	%ebp, %ebp
.LBB4_7:                                # %.loopexit
	movq	%rbp, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end4:
	.size	cli_utf16toascii, .Lfunc_end4-cli_utf16toascii
	.cfi_endproc

	.globl	cli_strbcasestr
	.p2align	4, 0x90
	.type	cli_strbcasestr,@function
cli_strbcasestr:                        # @cli_strbcasestr
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 32
.Lcfi53:
	.cfi_offset %rbx, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	callq	strlen
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	strlen
	xorl	%ecx, %ecx
	cmpl	%eax, %ebx
	jl	.LBB5_2
# BB#1:
	subl	%eax, %ebx
	movslq	%ebx, %rax
	addq	%rax, %r15
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	strcasecmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
.LBB5_2:
	movl	%ecx, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	cli_strbcasestr, .Lfunc_end5-cli_strbcasestr
	.cfi_endproc

	.globl	cli_chomp
	.p2align	4, 0x90
	.type	cli_chomp,@function
cli_chomp:                              # @cli_chomp
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 16
.Lcfi57:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB6_1
# BB#2:
	movq	%rbx, %rdi
	callq	strlen
	testl	%eax, %eax
	je	.LBB6_3
# BB#4:                                 # %.preheader
	jle	.LBB6_9
# BB#5:                                 # %.lr.ph.preheader
	cltq
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rbx,%rax), %ecx
	cmpb	$13, %cl
	je	.LBB6_8
# BB#7:                                 # %.lr.ph
                                        #   in Loop: Header=BB6_6 Depth=1
	cmpb	$10, %cl
	jne	.LBB6_9
.LBB6_8:                                # %.critedge1
                                        #   in Loop: Header=BB6_6 Depth=1
	movb	$0, -1(%rbx,%rax)
	decq	%rax
	leaq	1(%rax), %rcx
	cmpq	$1, %rcx
	jg	.LBB6_6
	jmp	.LBB6_9
.LBB6_1:
	movl	$-1, %eax
	jmp	.LBB6_9
.LBB6_3:
	xorl	%eax, %eax
.LBB6_9:                                # %.critedge
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	retq
.Lfunc_end6:
	.size	cli_chomp, .Lfunc_end6-cli_chomp
	.cfi_endproc

	.globl	cli_strtok
	.p2align	4, 0x90
	.type	cli_strtok,@function
cli_strtok:                             # @cli_strtok
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi64:
	.cfi_def_cfa_offset 80
.Lcfi65:
	.cfi_offset %rbx, -56
.Lcfi66:
	.cfi_offset %r12, -48
.Lcfi67:
	.cfi_offset %r13, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rdi, %rbp
	movb	(%rbp), %al
	testb	%al, %al
	sete	%cl
	xorl	%r14d, %r14d
	movl	%esi, 12(%rsp)          # 4-byte Spill
	testl	%esi, %esi
	je	.LBB7_11
# BB#1:
	testb	%al, %al
	je	.LBB7_11
# BB#2:                                 # %.lr.ph58.preheader
	leaq	1(%rbp), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph58
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_5 Depth 2
	movsbl	%al, %esi
	movq	%r12, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB7_8
# BB#4:                                 #   in Loop: Header=BB7_3 Depth=1
	movq	%rbp, %r15
	incl	%r13d
	movslq	%r14d, %rbx
	addq	16(%rsp), %rbx          # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB7_5:                                #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r14d, %ebp
	movsbl	(%rbx), %esi
	testl	%esi, %esi
	je	.LBB7_7
# BB#6:                                 #   in Loop: Header=BB7_5 Depth=2
	movq	%r12, %rdi
	callq	strchr
	leal	1(%rbp), %r14d
	incq	%rbx
	testq	%rax, %rax
	jne	.LBB7_5
.LBB7_7:                                #   in Loop: Header=BB7_3 Depth=1
	movl	%ebp, %r14d
	movq	%r15, %rbp
.LBB7_8:                                # %.critedge1
                                        #   in Loop: Header=BB7_3 Depth=1
	movslq	%r14d, %rax
	leaq	1(%rax), %r14
	leaq	1(%rbp,%rax), %r15
	movb	1(%rbp,%rax), %al
	testb	%al, %al
	sete	%cl
	cmpl	12(%rsp), %r13d         # 4-byte Folded Reload
	je	.LBB7_12
# BB#9:                                 # %.critedge1
                                        #   in Loop: Header=BB7_3 Depth=1
	testb	%al, %al
	jne	.LBB7_3
	jmp	.LBB7_12
.LBB7_11:
	movq	%rbp, %r15
.LBB7_12:                               # %.critedge
	xorl	%ebx, %ebx
	testb	%cl, %cl
	jne	.LBB7_22
# BB#13:                                # %.critedge
	testb	%al, %al
	je	.LBB7_22
# BB#14:                                # %.lr.ph.preheader
	movslq	%r14d, %r13
	.p2align	4, 0x90
.LBB7_15:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	%al, %esi
	movq	%r12, %rdi
	callq	strchr
	testq	%rax, %rax
	jne	.LBB7_17
# BB#16:                                #   in Loop: Header=BB7_15 Depth=1
	movzbl	1(%rbp,%r13), %eax
	incq	%r13
	testb	%al, %al
	jne	.LBB7_15
.LBB7_17:                               # %._crit_edge
	subl	%r14d, %r13d
	je	.LBB7_21
# BB#19:
	leal	1(%r13), %eax
	movslq	%eax, %rdi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB7_21
# BB#20:
	movslq	%r13d, %rbp
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	strncpy
	movb	$0, (%rbx,%rbp)
	jmp	.LBB7_22
.LBB7_21:
	xorl	%ebx, %ebx
.LBB7_22:                               # %._crit_edge.thread
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	cli_strtok, .Lfunc_end7-cli_strtok
	.cfi_endproc

	.globl	cli_strtokbuf
	.p2align	4, 0x90
	.type	cli_strtokbuf,@function
cli_strtokbuf:                          # @cli_strtokbuf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi74:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi77:
	.cfi_def_cfa_offset 80
.Lcfi78:
	.cfi_offset %rbx, -56
.Lcfi79:
	.cfi_offset %r12, -48
.Lcfi80:
	.cfi_offset %r13, -40
.Lcfi81:
	.cfi_offset %r14, -32
.Lcfi82:
	.cfi_offset %r15, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %r12
	movq	%rdi, %rbx
	movb	(%rbx), %cl
	testb	%cl, %cl
	sete	%dl
	xorl	%r15d, %r15d
	movl	%esi, 4(%rsp)           # 4-byte Spill
	testl	%esi, %esi
	je	.LBB8_11
# BB#1:
	testb	%cl, %cl
	je	.LBB8_11
# BB#2:                                 # %.lr.ph53.preheader
	leaq	1(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB8_3:                                # %.lr.ph53
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_5 Depth 2
	movsbl	%cl, %esi
	movq	%r12, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB8_8
# BB#4:                                 #   in Loop: Header=BB8_3 Depth=1
	movq	%rbx, %r13
	incl	%r14d
	movslq	%r15d, %rbx
	addq	16(%rsp), %rbx          # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB8_5:                                #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r15d, %ebp
	movsbl	(%rbx), %esi
	testl	%esi, %esi
	je	.LBB8_7
# BB#6:                                 #   in Loop: Header=BB8_5 Depth=2
	movq	%r12, %rdi
	callq	strchr
	leal	1(%rbp), %r15d
	incq	%rbx
	testq	%rax, %rax
	jne	.LBB8_5
.LBB8_7:                                #   in Loop: Header=BB8_3 Depth=1
	movl	%ebp, %r15d
	movq	%r13, %rbx
.LBB8_8:                                # %.critedge1
                                        #   in Loop: Header=BB8_3 Depth=1
	movslq	%r15d, %rax
	leaq	1(%rax), %r15
	leaq	1(%rbx,%rax), %r13
	movb	1(%rbx,%rax), %cl
	testb	%cl, %cl
	sete	%dl
	cmpl	4(%rsp), %r14d          # 4-byte Folded Reload
	je	.LBB8_12
# BB#9:                                 # %.critedge1
                                        #   in Loop: Header=BB8_3 Depth=1
	testb	%cl, %cl
	jne	.LBB8_3
	jmp	.LBB8_12
.LBB8_11:
	movq	%rbx, %r13
.LBB8_12:                               # %.critedge
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.LBB8_20
# BB#13:                                # %.critedge
	testb	%cl, %cl
	je	.LBB8_20
# BB#14:                                # %.lr.ph.preheader
	movslq	%r15d, %r14
	.p2align	4, 0x90
.LBB8_15:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	%cl, %esi
	movq	%r12, %rdi
	callq	strchr
	testq	%rax, %rax
	jne	.LBB8_17
# BB#16:                                #   in Loop: Header=BB8_15 Depth=1
	movzbl	1(%rbx,%r14), %ecx
	incq	%r14
	testb	%cl, %cl
	jne	.LBB8_15
.LBB8_17:                               # %._crit_edge
	subl	%r15d, %r14d
	jne	.LBB8_19
# BB#18:
	xorl	%eax, %eax
	jmp	.LBB8_20
.LBB8_19:
	movslq	%r14d, %rbx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	strncpy
	movb	$0, (%rbp,%rbx)
	movq	%rbp, %rax
.LBB8_20:                               # %._crit_edge.thread
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	cli_strtokbuf, .Lfunc_end8-cli_strtokbuf
	.cfi_endproc

	.globl	cli_memstr
	.p2align	4, 0x90
	.type	cli_memstr,@function
cli_memstr:                             # @cli_memstr
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi90:
	.cfi_def_cfa_offset 64
.Lcfi91:
	.cfi_offset %rbx, -56
.Lcfi92:
	.cfi_offset %r12, -48
.Lcfi93:
	.cfi_offset %r13, -40
.Lcfi94:
	.cfi_offset %r14, -32
.Lcfi95:
	.cfi_offset %r15, -24
.Lcfi96:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %rbx
	cmpl	%r15d, %ebp
	jge	.LBB9_1
.LBB9_9:
	xorl	%ebx, %ebx
	jmp	.LBB9_10
.LBB9_1:
	cmpq	%r14, %rbx
	je	.LBB9_10
# BB#2:
	movslq	%r15d, %r12
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB9_10
# BB#3:                                 # %.preheader
	movsbl	(%r14), %esi
	movslq	%ebp, %rdx
	movq	%rbx, %rdi
	movl	%esi, 4(%rsp)           # 4-byte Spill
	jmp	.LBB9_4
	.p2align	4, 0x90
.LBB9_8:                                # %.backedge
                                        #   in Loop: Header=BB9_4 Depth=1
	cmpq	%r13, %rbx
	leaq	1(%rbx), %rbx
	cmovneq	%r13, %rbx
	movl	$0, %eax
	movl	$-1, %ecx
	cmovel	%ecx, %eax
	addl	%eax, %ebp
	movslq	%ebp, %rdx
	movq	%rbx, %rdi
	movl	4(%rsp), %esi           # 4-byte Reload
.LBB9_4:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	callq	memchr
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB9_9
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB9_4 Depth=1
	movl	%r13d, %eax
	subl	%ebx, %eax
	subl	%eax, %ebp
	cmpl	%r15d, %ebp
	jl	.LBB9_9
# BB#6:                                 #   in Loop: Header=BB9_4 Depth=1
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB9_8
# BB#7:
	movq	%r13, %rbx
.LBB9_10:                               # %.loopexit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	cli_memstr, .Lfunc_end9-cli_memstr
	.cfi_endproc

	.globl	cli_strrcpy
	.p2align	4, 0x90
	.type	cli_strrcpy,@function
cli_strrcpy:                            # @cli_strrcpy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 16
.Lcfi98:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB10_5
# BB#1:
	testq	%rsi, %rsi
	je	.LBB10_5
# BB#2:                                 # %.preheader.preheader
	decq	%rbx
	.p2align	4, 0x90
.LBB10_3:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %eax
	incq	%rsi
	movb	%al, 1(%rbx)
	incq	%rbx
	testb	%al, %al
	jne	.LBB10_3
	jmp	.LBB10_4
.LBB10_5:
	xorl	%ebx, %ebx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB10_4:                               # %.loopexit
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end10:
	.size	cli_strrcpy, .Lfunc_end10-cli_strrcpy
	.cfi_endproc

	.globl	cli_strtokenize
	.p2align	4, 0x90
	.type	cli_strtokenize,@function
cli_strtokenize:                        # @cli_strtokenize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi102:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 48
.Lcfi104:
	.cfi_offset %rbx, -48
.Lcfi105:
	.cfi_offset %r12, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	testq	%r14, %r14
	je	.LBB11_6
# BB#1:                                 # %.lr.ph18
	movsbl	%sil, %r12d
	leaq	-8(,%r14,8), %r15
	addq	$8, %rbx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	movq	%rdi, -8(%rbx)
	movl	%r12d, %esi
	callq	strchr
	testq	%rax, %rax
	je	.LBB11_3
# BB#5:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	%rax, %rdi
	incq	%rdi
	movb	$0, (%rax)
	addq	$-8, %r15
	addq	$8, %rbx
	cmpq	%r14, %rbp
	leaq	1(%rbp), %rbp
	jb	.LBB11_2
	jmp	.LBB11_6
.LBB11_3:                               # %.preheader
	cmpq	%r14, %rbp
	jae	.LBB11_6
# BB#4:                                 # %.lr.ph.preheader
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	memset
.LBB11_6:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	cli_strtokenize, .Lfunc_end11-cli_strtokenize
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"cli_hex2si(): Malformed hexstring: %s (length: %u)\n"
	.size	.L.str, 52

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"cli_hex2str(): Malformed hexstring: %s (length: %d)\n"
	.size	.L.str.1, 53

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"cli_hex2num(): Malformed hexstring: %s (length: %d)\n"
	.size	.L.str.2, 53

	.type	.Lcli_str2hex.HEX,@object # @cli_str2hex.HEX
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.Lcli_str2hex.HEX:
	.ascii	"0123456789abcdef"
	.size	.Lcli_str2hex.HEX, 16

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"cli_utf16toascii: length < 2\n"
	.size	.L.str.3, 30

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"cli_strrcpy: NULL argument\n"
	.size	.L.str.4, 28

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"hex2int() translation problem (%d)\n"
	.size	.L.str.5, 36


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
