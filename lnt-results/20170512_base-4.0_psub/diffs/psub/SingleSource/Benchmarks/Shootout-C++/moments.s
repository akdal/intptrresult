	.text
	.file	"moments.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$88, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 128
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	cmpl	$2, %edi
	jne	.LBB0_1
# BB#5:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbx
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, (%rsp)
	movq	$0, 16(%rsp)
	testl	%ebx, %ebx
	jne	.LBB0_2
# BB#6:                                 # %.._crit_edge_crit_edge
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.LBB0_12
.LBB0_1:                                # %.thread
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, (%rsp)
	movq	$0, 16(%rsp)
	movl	$5000000, %ebx          # imm = 0x4C4B40
.LBB0_2:                                # %.lr.ph
	decl	%ebx
	xorl	%esi, %esi
	movq	%rsp, %r14
	leaq	32(%rsp), %r15
	xorl	%ecx, %ecx
	xorl	%ebp, %ebp
	jmp	.LBB0_3
	.p2align	4, 0x90
.LBB0_9:                                # %_ZNSt6vectorIdSaIdEE9push_backERKd.exit._crit_edge
                                        #   in Loop: Header=BB0_3 Depth=1
	incl	%ebp
	movq	8(%rsp), %rcx
	movq	16(%rsp), %rsi
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movsd	%xmm0, 32(%rsp)
	cmpq	%rsi, %rcx
	je	.LBB0_7
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	movsd	%xmm0, (%rcx)
	addq	$8, %rcx
	movq	%rcx, 8(%rsp)
	cmpl	%ebp, %ebx
	jne	.LBB0_9
	jmp	.LBB0_11
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_3 Depth=1
.Ltmp0:
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	_ZNSt6vectorIdSaIdEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPdS1_EERKd
.Ltmp1:
# BB#8:                                 # %_ZNSt6vectorIdSaIdEE9push_backERKd.exit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	%ebp, %ebx
	jne	.LBB0_9
.LBB0_11:                               # %._crit_edge.loopexit
	movq	(%rsp), %rsi
	movq	8(%rsp), %rdx
.LBB0_12:                               # %._crit_edge
.Ltmp3:
	leaq	32(%rsp), %rdi
	callq	_ZN7momentsIdEC2IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEET_S9_
.Ltmp4:
# BB#13:
	movq	8(%rsp), %rsi
	subq	(%rsp), %rsi
	sarq	$3, %rsi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	32(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str.1, %edi
	movb	$1, %al
	callq	printf
	movsd	40(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str.2, %edi
	movb	$1, %al
	callq	printf
	movsd	48(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str.3, %edi
	movb	$1, %al
	callq	printf
	movsd	56(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str.4, %edi
	movb	$1, %al
	callq	printf
	movsd	64(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str.5, %edi
	movb	$1, %al
	callq	printf
	movsd	72(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str.6, %edi
	movb	$1, %al
	callq	printf
	movsd	80(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str.7, %edi
	movb	$1, %al
	callq	printf
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_15
# BB#14:
	callq	_ZdlPv
.LBB0_15:                               # %_ZNSt6vectorIdSaIdEED2Ev.exit21
	xorl	%eax, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_16:
.Ltmp5:
	jmp	.LBB0_17
.LBB0_10:
.Ltmp2:
.LBB0_17:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_19
# BB#18:
	callq	_ZdlPv
.LBB0_19:                               # %_ZNSt6vectorIdSaIdEED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI1_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
.LCPI1_2:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_3:
	.quad	-4609434218613702656    # double -3
.LCPI1_4:
	.quad	4602678819172646912     # double 0.5
	.section	.text._ZN7momentsIdEC2IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEET_S9_,"axG",@progbits,_ZN7momentsIdEC2IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEET_S9_,comdat
	.weak	_ZN7momentsIdEC2IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEET_S9_
	.p2align	4, 0x90
	.type	_ZN7momentsIdEC2IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEET_S9_,@function
_ZN7momentsIdEC2IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEET_S9_: # @_ZN7momentsIdEC2IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEET_S9_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 80
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cmpq	%r15, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r14)
	movups	%xmm0, 16(%r14)
	movups	%xmm0, (%r14)
	movq	$0, 48(%r14)
	je	.LBB1_29
# BB#1:                                 # %.lr.ph.i.preheader
	leaq	-8(%r15), %rcx
	subq	%rbx, %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$7, %rdx
	je	.LBB1_2
# BB#3:                                 # %.lr.ph.i.prol.preheader
	negq	%rdx
	xorpd	%xmm1, %xmm1
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	addsd	(%rax), %xmm1
	addq	$8, %rax
	incq	%rdx
	jne	.LBB1_4
	jmp	.LBB1_5
.LBB1_29:                               # %_ZSt10accumulateIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdET0_T_S8_S7_.exit.thread
	movq	%r15, %r13
	subq	%rbx, %r13
	sarq	$3, %r13
	movd	%r13, %xmm0
	punpckldq	.LCPI1_0(%rip), %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	.LCPI1_1(%rip), %xmm0
	pshufd	$78, %xmm0, %xmm8       # xmm8 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm8
	xorpd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	divsd	%xmm8, %xmm1
	movsd	%xmm1, 8(%r14)
	jmp	.LBB1_9
.LBB1_2:
	xorpd	%xmm1, %xmm1
	movq	%rbx, %rax
.LBB1_5:                                # %.lr.ph.i.prol.loopexit
	cmpq	$56, %rcx
	jb	.LBB1_7
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	addsd	(%rax), %xmm1
	addsd	8(%rax), %xmm1
	addsd	16(%rax), %xmm1
	addsd	24(%rax), %xmm1
	addsd	32(%rax), %xmm1
	addsd	40(%rax), %xmm1
	addsd	48(%rax), %xmm1
	addsd	56(%rax), %xmm1
	addq	$64, %rax
	cmpq	%r15, %rax
	jne	.LBB1_6
.LBB1_7:                                # %_ZSt10accumulateIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEdET0_T_S8_S7_.exit
	movq	%r15, %r13
	subq	%rbx, %r13
	sarq	$3, %r13
	cmpq	%r15, %rbx
	movd	%r13, %xmm0
	punpckldq	.LCPI1_0(%rip), %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	.LCPI1_1(%rip), %xmm0
	pshufd	$78, %xmm0, %xmm8       # xmm8 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm8
	divsd	%xmm8, %xmm1
	movsd	%xmm1, 8(%r14)
	je	.LBB1_8
# BB#25:                                # %.lr.ph.preheader
	xorpd	%xmm0, %xmm0
	xorpd	%xmm3, %xmm3
	movapd	.LCPI1_2(%rip), %xmm4   # xmm4 = [nan,nan]
	xorpd	%xmm2, %xmm2
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB1_26:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rax), %xmm5           # xmm5 = mem[0],zero
	subsd	%xmm1, %xmm5
	movapd	%xmm5, %xmm6
	mulsd	%xmm6, %xmm6
	movapd	%xmm5, %xmm7
	mulsd	%xmm6, %xmm7
	unpcklpd	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0]
	mulsd	%xmm5, %xmm7
	andpd	%xmm4, %xmm5
	addsd	%xmm5, %xmm2
	movsd	%xmm2, 16(%r14)
	addpd	%xmm6, %xmm0
	movupd	%xmm0, 32(%r14)
	addsd	%xmm7, %xmm3
	movsd	%xmm3, 48(%r14)
	addq	$8, %rax
	cmpq	%rax, %r15
	jne	.LBB1_26
	jmp	.LBB1_10
.LBB1_8:
	xorpd	%xmm0, %xmm0
.LBB1_9:                                # %._crit_edge
	xorpd	%xmm2, %xmm2
.LBB1_10:                               # %._crit_edge
	divsd	%xmm8, %xmm2
	movsd	%xmm2, 16(%r14)
	leaq	-1(%r13), %rax
	movd	%rax, %xmm1
	punpckldq	.LCPI1_0(%rip), %xmm1 # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	subpd	.LCPI1_1(%rip), %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm2
	divsd	%xmm2, %xmm0
	movsd	%xmm0, 32(%r14)
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB1_12
# BB#11:                                # %call.sqrt
	movapd	%xmm8, (%rsp)           # 16-byte Spill
	callq	sqrt
	movapd	(%rsp), %xmm8           # 16-byte Reload
	movapd	%xmm0, %xmm1
.LBB1_12:                               # %._crit_edge.split
	movsd	%xmm1, 24(%r14)
	movsd	32(%r14), %xmm0         # xmm0 = mem[0],zero
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm0
	jne	.LBB1_13
	jnp	.LBB1_14
.LBB1_13:
	mulsd	%xmm0, %xmm8
	mulsd	%xmm8, %xmm1
	movsd	40(%r14), %xmm2         # xmm2 = mem[0],zero
	divsd	%xmm1, %xmm2
	movsd	%xmm2, 40(%r14)
	movsd	48(%r14), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm8
	divsd	%xmm8, %xmm1
	addsd	.LCPI1_3(%rip), %xmm1
	movsd	%xmm1, 48(%r14)
.LBB1_14:
	movq	%r13, %rbp
	shrq	%rbp
	cmpq	%r15, %rbx
	leaq	(%rbx,%rbp,8), %r12
	je	.LBB1_17
# BB#15:
	cmpq	%r15, %r12
	je	.LBB1_17
# BB#16:
	bsrq	%r13, %rcx
	xorq	$63, %rcx
	addq	%rcx, %rcx
	xorq	$126, %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	_ZSt13__introselectIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_S9_T0_T1_
.LBB1_17:                               # %_ZSt11nth_elementIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEvT_S7_S7_.exit
	testb	$1, %r13b
	jne	.LBB1_27
# BB#18:
	cmpq	$4, %r13
	jb	.LBB1_24
# BB#19:                                # %.lr.ph.i.i.preheader
	leaq	8(%rbx), %rax
	leaq	-16(,%rbp,8), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB1_22
# BB#20:                                # %.lr.ph.i.i.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB1_21:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	ucomisd	(%rbx), %xmm0
	cmovaq	%rax, %rbx
	addq	$8, %rax
	incq	%rdx
	jne	.LBB1_21
.LBB1_22:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$24, %rcx
	jb	.LBB1_24
	.p2align	4, 0x90
.LBB1_23:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	ucomisd	(%rbx), %xmm0
	cmovaq	%rax, %rbx
	leaq	8(%rax), %rcx
	ucomisd	(%rbx), %xmm1
	cmovbeq	%rbx, %rcx
	leaq	16(%rax), %rdx
	movsd	16(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rcx), %xmm0
	cmovbeq	%rcx, %rdx
	leaq	24(%rax), %rbx
	movsd	24(%rax), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rdx), %xmm0
	cmovbeq	%rdx, %rbx
	addq	$32, %rax
	cmpq	%r12, %rax
	jne	.LBB1_23
.LBB1_24:                               # %_ZSt11max_elementIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEET_S7_S7_.exit
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	addsd	(%rbx), %xmm0
	mulsd	.LCPI1_4(%rip), %xmm0
	movsd	%xmm0, (%r14)
	jmp	.LBB1_28
.LBB1_27:
	movq	(%r12), %rax
	movq	%rax, (%r14)
.LBB1_28:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN7momentsIdEC2IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEET_S9_, .Lfunc_end1-_ZN7momentsIdEC2IN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEET_S9_
	.cfi_endproc

	.section	.text._ZNSt6vectorIdSaIdEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPdS1_EERKd,"axG",@progbits,_ZNSt6vectorIdSaIdEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPdS1_EERKd,comdat
	.weak	_ZNSt6vectorIdSaIdEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPdS1_EERKd
	.p2align	4, 0x90
	.type	_ZNSt6vectorIdSaIdEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPdS1_EERKd,@function
_ZNSt6vectorIdSaIdEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPdS1_EERKd: # @_ZNSt6vectorIdSaIdEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPdS1_EERKd
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 64
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	cmpq	16(%rbx), %rdi
	je	.LBB2_4
# BB#1:
	leaq	-8(%rdi), %rdx
	movq	-8(%rdi), %rax
	movq	%rax, (%rdi)
	leaq	8(%rdi), %rax
	movq	%rax, 8(%rbx)
	movq	(%r15), %rbx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	je	.LBB2_3
# BB#2:
	shlq	$3, %rax
	subq	%rax, %rdi
	movq	%r14, %rsi
	callq	memmove
.LBB2_3:                                # %_ZSt13copy_backwardIPdS0_ET0_T_S2_S1_.exit
	movq	%rbx, (%r14)
	jmp	.LBB2_15
.LBB2_4:                                # %_ZNKSt6vectorIdSaIdEE12_M_check_lenEmPKc.exit
	movq	(%rbx), %rbp
	subq	%rbp, %rdi
	sarq	$3, %rdi
	movl	$1, %ecx
	cmovneq	%rdi, %rcx
	leaq	(%rcx,%rdi), %r13
	movq	%r13, %rax
	shrq	$61, %rax
	movabsq	$2305843009213693951, %rax # imm = 0x1FFFFFFFFFFFFFFF
	cmovneq	%rax, %r13
	addq	%rdi, %rcx
	cmovbq	%rax, %r13
	testq	%r13, %r13
	je	.LBB2_5
# BB#6:
	cmpq	%rax, %r13
	ja	.LBB2_16
# BB#7:                                 # %_ZN9__gnu_cxx14__alloc_traitsISaIdEE8allocateERS1_m.exit.i
	leaq	(,%r13,8), %rdi
	callq	_Znwm
	movq	%rax, %r12
	movq	(%rbx), %rsi
	jmp	.LBB2_8
.LBB2_5:
	xorl	%r12d, %r12d
	movq	%rbp, %rsi
.LBB2_8:
	movq	%r14, %rax
	subq	%rbp, %rax
	sarq	$3, %rax
	movq	(%r15), %rcx
	movq	%rcx, (%r12,%rax,8)
	movq	%r14, %rdx
	subq	%rsi, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB2_10
# BB#9:
	movq	%r12, %rdi
	movq	%rsi, %r15
	callq	memmove
	movq	%r15, %rsi
.LBB2_10:
	leaq	8(%r12,%rbp,8), %r15
	movq	8(%rbx), %rdx
	subq	%r14, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB2_12
# BB#11:
	movq	%r15, %rdi
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%r14, %rsi
	callq	memmove
	movq	(%rsp), %rsi            # 8-byte Reload
.LBB2_12:
	leaq	(%r15,%rbp,8), %rbp
	testq	%rsi, %rsi
	je	.LBB2_14
# BB#13:
	movq	%rsi, %rdi
	callq	_ZdlPv
.LBB2_14:                               # %_ZNSt12_Vector_baseIdSaIdEE13_M_deallocateEPdm.exit37
	movq	%r12, (%rbx)
	movq	%rbp, 8(%rbx)
	leaq	(%r12,%r13,8), %rax
	movq	%rax, 16(%rbx)
.LBB2_15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_16:
	callq	_ZSt17__throw_bad_allocv
.Lfunc_end2:
	.size	_ZNSt6vectorIdSaIdEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPdS1_EERKd, .Lfunc_end2-_ZNSt6vectorIdSaIdEE13_M_insert_auxEN9__gnu_cxx17__normal_iteratorIPdS1_EERKd
	.cfi_endproc

	.section	.text._ZSt13__introselectIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_S9_T0_T1_,"axG",@progbits,_ZSt13__introselectIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_S9_T0_T1_,comdat
	.weak	_ZSt13__introselectIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_S9_T0_T1_
	.p2align	4, 0x90
	.type	_ZSt13__introselectIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_S9_T0_T1_,@function
_ZSt13__introselectIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_S9_T0_T1_: # @_ZSt13__introselectIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_S9_T0_T1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 64
.Lcfi40:
	.cfi_offset %rbx, -40
.Lcfi41:
	.cfi_offset %r12, -32
.Lcfi42:
	.cfi_offset %r14, -24
.Lcfi43:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	%r14, 8(%rsp)
	movq	%rbx, (%rsp)
	movq	%rbx, %rdi
	subq	%r14, %rdi
	cmpq	$25, %rdi
	jl	.LBB3_17
# BB#1:                                 # %.lr.ph.preheader
	leaq	8(%rsp), %r8
	movq	%rsp, %r9
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_11 Depth 2
                                        #       Child Loop BB3_12 Depth 3
                                        #       Child Loop BB3_14 Depth 3
	testq	%rcx, %rcx
	je	.LBB3_27
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	shrq	$4, %rdi
	leaq	(%r14,%rdi,8), %rdx
	leaq	8(%r14), %rsi
	leaq	-8(%rbx), %rax
	movsd	8(%r14), %xmm1          # xmm1 = mem[0],zero
	movsd	(%r14,%rdi,8), %xmm2    # xmm2 = mem[0],zero
	movsd	-8(%rbx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	jbe	.LBB3_6
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=1
	ucomisd	%xmm2, %xmm0
	movq	%rdx, %rdi
	movapd	%xmm2, %xmm3
	ja	.LBB3_9
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	ucomisd	%xmm1, %xmm0
	cmovbeq	%rsi, %rax
	maxsd	%xmm1, %xmm0
	movq	%rax, %rdi
	jmp	.LBB3_8
	.p2align	4, 0x90
.LBB3_6:                                #   in Loop: Header=BB3_2 Depth=1
	ucomisd	%xmm1, %xmm0
	movq	%rsi, %rdi
	movapd	%xmm1, %xmm3
	ja	.LBB3_9
# BB#7:                                 #   in Loop: Header=BB3_2 Depth=1
	ucomisd	%xmm2, %xmm0
	cmovaq	%rax, %rdx
	maxsd	%xmm2, %xmm0
	movq	%rdx, %rdi
.LBB3_8:                                # %_ZSt22__move_median_to_firstIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_S9_S9_T0_.exit.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movapd	%xmm0, %xmm3
.LBB3_9:                                # %_ZSt22__move_median_to_firstIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_S9_S9_T0_.exit.i
                                        #   in Loop: Header=BB3_2 Depth=1
	decq	%rcx
	movq	(%r14), %rax
	movsd	%xmm3, (%r14)
	movq	%rax, (%rdi)
	jmp	.LBB3_11
	.p2align	4, 0x90
.LBB3_10:                               #   in Loop: Header=BB3_11 Depth=2
	movsd	%xmm2, (%rax)
	movsd	%xmm0, (%rbx)
.LBB3_11:                               #   Parent Loop BB3_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_12 Depth 3
                                        #       Child Loop BB3_14 Depth 3
	movsd	(%r14), %xmm1           # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB3_12:                               #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rsi
	ucomisd	%xmm0, %xmm1
	ja	.LBB3_12
# BB#13:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB3_11 Depth=2
	leaq	-8(%rsi), %rax
	.p2align	4, 0x90
.LBB3_14:                               # %.preheader.i.i
                                        #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rbx), %xmm2         # xmm2 = mem[0],zero
	addq	$-8, %rbx
	ucomisd	%xmm1, %xmm2
	ja	.LBB3_14
# BB#15:                                #   in Loop: Header=BB3_11 Depth=2
	cmpq	%rbx, %rax
	jb	.LBB3_10
# BB#16:                                # %_ZSt27__unguarded_partition_pivotIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEET_S9_S9_T0_.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpq	%r15, %rax
	movq	%r9, %rdx
	cmovbeq	%r8, %rdx
	movq	%rax, (%rdx)
	movq	(%rsp), %rbx
	movq	8(%rsp), %r14
	movq	%rbx, %rdi
	subq	%r14, %rdi
	cmpq	$24, %rdi
	jg	.LBB3_2
.LBB3_17:                               # %._crit_edge
	cmpq	%rbx, %r14
	je	.LBB3_45
# BB#18:                                # %.preheader.i
	leaq	8(%r14), %rax
	cmpq	%rbx, %rax
	je	.LBB3_45
# BB#19:                                # %.lr.ph.i
	movq	%r14, %r15
	.p2align	4, 0x90
.LBB3_20:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_25 Depth 2
	movq	%r15, %rdi
	movq	%rax, %r15
	movsd	(%r15), %xmm1           # xmm1 = mem[0],zero
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB3_23
# BB#21:                                #   in Loop: Header=BB3_20 Depth=1
	movq	%r15, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rcx
	sarq	$3, %rcx
	movq	%r14, %rax
	je	.LBB3_26
# BB#22:                                #   in Loop: Header=BB3_20 Depth=1
	shlq	$3, %rcx
	subq	%rcx, %rdi
	addq	$16, %rdi
	movq	%r14, %rsi
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	callq	memmove
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%r14, %rax
	jmp	.LBB3_26
	.p2align	4, 0x90
.LBB3_23:                               #   in Loop: Header=BB3_20 Depth=1
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	movq	%r15, %rax
	jbe	.LBB3_26
# BB#24:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB3_20 Depth=1
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB3_25:                               # %.lr.ph.i.i
                                        #   Parent Loop BB3_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rax), %rcx
	movq	%rcx, (%rax)
	movsd	-16(%rax), %xmm0        # xmm0 = mem[0],zero
	leaq	-8(%rax), %rax
	ucomisd	%xmm1, %xmm0
	ja	.LBB3_25
.LBB3_26:                               # %_ZSt25__unguarded_linear_insertIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops14_Val_less_iterEEvT_T0_.exit.i
                                        #   in Loop: Header=BB3_20 Depth=1
	movsd	%xmm1, (%rax)
	leaq	8(%r15), %rax
	cmpq	%rbx, %rax
	jne	.LBB3_20
	jmp	.LBB3_45
.LBB3_27:
	leaq	8(%r15), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_
	cmpq	%rbx, %r12
	jae	.LBB3_44
# BB#28:                                # %.lr.ph.i11.preheader
	movq	%r12, %r10
	subq	%r14, %r10
	sarq	$3, %r10
	leaq	-1(%r10), %r9
	movq	%r9, %rcx
	shrq	$63, %rcx
	leaq	-1(%r10,%rcx), %rdx
	sarq	%rdx
	leaq	-2(%r10), %rcx
	shrq	$63, %rcx
	leaq	-2(%r10,%rcx), %r8
	sarq	%r8
	.p2align	4, 0x90
.LBB3_29:                               # %.lr.ph.i11
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_32 Depth 2
                                        #     Child Loop BB3_38 Depth 2
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	movsd	(%r14), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB3_43
# BB#30:                                #   in Loop: Header=BB3_29 Depth=1
	movsd	%xmm1, (%r12)
	cmpq	$2, %r9
	jl	.LBB3_33
# BB#31:                                # %.lr.ph.i.i13.preheader
                                        #   in Loop: Header=BB3_29 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_32:                               # %.lr.ph.i.i13
                                        #   Parent Loop BB3_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rsi,%rsi), %rcx
	leaq	2(%rsi,%rsi), %rax
	leaq	1(%rsi,%rsi), %rdi
	movsd	8(%r14,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%r14,%rcx,8), %xmm1
	cmovbeq	%rax, %rdi
	movq	(%r14,%rdi,8), %rax
	movq	%rax, (%r14,%rsi,8)
	cmpq	%rdx, %rdi
	movq	%rdi, %rsi
	jl	.LBB3_32
	jmp	.LBB3_34
	.p2align	4, 0x90
.LBB3_33:                               #   in Loop: Header=BB3_29 Depth=1
	xorl	%edi, %edi
.LBB3_34:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB3_29 Depth=1
	testb	$1, %r10b
	jne	.LBB3_37
# BB#35:                                #   in Loop: Header=BB3_29 Depth=1
	cmpq	%r8, %rdi
	jne	.LBB3_37
# BB#36:                                #   in Loop: Header=BB3_29 Depth=1
	leaq	(%rdi,%rdi), %rax
	movq	8(%r14,%rax,8), %rax
	movq	%rax, (%r14,%rdi,8)
	leaq	1(%rdi,%rdi), %rdi
.LBB3_37:                               #   in Loop: Header=BB3_29 Depth=1
	testq	%rdi, %rdi
	jle	.LBB3_41
	.p2align	4, 0x90
.LBB3_38:                               # %.lr.ph.i.i.i
                                        #   Parent Loop BB3_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdi), %rcx
	movq	%rcx, %rax
	shrq	$63, %rax
	leaq	-1(%rdi,%rax), %rsi
	sarq	%rsi
	movsd	(%r14,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB3_41
# BB#39:                                #   in Loop: Header=BB3_38 Depth=2
	movsd	%xmm1, (%r14,%rdi,8)
	cmpq	$1, %rcx
	movq	%rsi, %rdi
	jg	.LBB3_38
	jmp	.LBB3_42
	.p2align	4, 0x90
.LBB3_41:                               #   in Loop: Header=BB3_29 Depth=1
	movq	%rdi, %rsi
.LBB3_42:                               # %_ZSt10__pop_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_S9_T0_.exit
                                        #   in Loop: Header=BB3_29 Depth=1
	movsd	%xmm0, (%r14,%rsi,8)
.LBB3_43:                               #   in Loop: Header=BB3_29 Depth=1
	addq	$8, %r12
	cmpq	%rbx, %r12
	jb	.LBB3_29
.LBB3_44:                               # %_ZSt13__heap_selectIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_S9_T0_.exit
	movq	(%r14), %rax
	movq	(%r15), %rcx
	movq	%rcx, (%r14)
	movq	%rax, (%r15)
.LBB3_45:                               # %_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZSt13__introselectIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_S9_T0_T1_, .Lfunc_end3-_ZSt13__introselectIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_S9_T0_T1_
	.cfi_endproc

	.section	.text._ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_,"axG",@progbits,_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_,comdat
	.weak	_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_
	.p2align	4, 0x90
	.type	_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_,@function
_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_: # @_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_
	.cfi_startproc
# BB#0:
	subq	%rdi, %rsi
	cmpq	$16, %rsi
	jl	.LBB4_23
# BB#1:
	sarq	$3, %rsi
	leaq	-2(%rsi), %rax
	shrq	$63, %rax
	leaq	-2(%rsi,%rax), %r9
	sarq	%r9
	leaq	-1(%rsi), %rax
	shrq	$63, %rax
	leaq	-1(%rsi,%rax), %r11
	sarq	%r11
	testb	$1, %sil
	jne	.LBB4_14
# BB#2:                                 # %.split.us.preheader
	leaq	1(%r9,%r9), %r8
	movq	%r9, %r10
	.p2align	4, 0x90
.LBB4_3:                                # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_5 Depth 2
                                        #     Child Loop BB4_9 Depth 2
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	cmpq	%r10, %r11
	movq	%r10, %rdx
	jle	.LBB4_6
# BB#4:                                 # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph.i.us
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rax), %rsi
	leaq	2(%rax,%rax), %rcx
	leaq	1(%rax,%rax), %rdx
	movsd	8(%rdi,%rsi,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%rdi,%rsi,8), %xmm1
	cmovbeq	%rcx, %rdx
	movq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	cmpq	%r11, %rdx
	movq	%rdx, %rax
	jl	.LBB4_5
.LBB4_6:                                # %._crit_edge.i.us
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpq	%r9, %rdx
	jne	.LBB4_8
# BB#7:                                 #   in Loop: Header=BB4_3 Depth=1
	movq	(%rdi,%r8,8), %rax
	movq	%rax, (%rdi,%r9,8)
	movq	%r8, %rdx
.LBB4_8:                                #   in Loop: Header=BB4_3 Depth=1
	cmpq	%r10, %rdx
	jle	.LBB4_12
	.p2align	4, 0x90
.LBB4_9:                                # %.lr.ph.i.i.us
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rax
	sarq	%rax
	movsd	(%rdi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB4_12
# BB#10:                                #   in Loop: Header=BB4_9 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r10, %rax
	movq	%rax, %rdx
	jg	.LBB4_9
	jmp	.LBB4_13
	.p2align	4, 0x90
.LBB4_12:                               #   in Loop: Header=BB4_3 Depth=1
	movq	%rdx, %rax
.LBB4_13:                               # %_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEldNS0_5__ops15_Iter_less_iterEEvT_T0_SA_T1_T2_.exit.us
                                        #   in Loop: Header=BB4_3 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	testq	%r10, %r10
	leaq	-1(%r10), %r10
	jne	.LBB4_3
	jmp	.LBB4_23
	.p2align	4, 0x90
.LBB4_14:                               # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_16 Depth 2
                                        #     Child Loop BB4_18 Depth 2
	movsd	(%rdi,%r9,8), %xmm0     # xmm0 = mem[0],zero
	cmpq	%r9, %r11
	movq	%r9, %rsi
	jle	.LBB4_22
# BB#15:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB4_14 Depth=1
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB4_16:                               # %.lr.ph.i
                                        #   Parent Loop BB4_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rax
	leaq	(%rax,%rax), %rcx
	leaq	2(%rax,%rax), %rsi
	leaq	1(%rax,%rax), %rdx
	movsd	8(%rdi,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%rdi,%rcx,8), %xmm1
	cmovbeq	%rsi, %rdx
	movq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	cmpq	%r11, %rdx
	jl	.LBB4_16
# BB#17:                                # %._crit_edge.i
                                        #   in Loop: Header=BB4_14 Depth=1
	cmpq	%r9, %rdx
	jle	.LBB4_21
	.p2align	4, 0x90
.LBB4_18:                               # %.lr.ph.i.i
                                        #   Parent Loop BB4_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rsi
	sarq	%rsi
	movsd	(%rdi,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB4_21
# BB#19:                                #   in Loop: Header=BB4_18 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r9, %rsi
	movq	%rsi, %rdx
	jg	.LBB4_18
	jmp	.LBB4_22
	.p2align	4, 0x90
.LBB4_21:                               #   in Loop: Header=BB4_14 Depth=1
	movq	%rdx, %rsi
.LBB4_22:                               # %_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEldNS0_5__ops15_Iter_less_iterEEvT_T0_SA_T1_T2_.exit
                                        #   in Loop: Header=BB4_14 Depth=1
	movsd	%xmm0, (%rdi,%rsi,8)
	testq	%r9, %r9
	leaq	-1(%r9), %r9
	jne	.LBB4_14
.LBB4_23:                               # %.loopexit
	retq
.Lfunc_end4:
	.size	_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_, .Lfunc_end4-_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"n:                  %d\n"
	.size	.L.str, 24

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"median:             %f\n"
	.size	.L.str.1, 24

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"mean:               %f\n"
	.size	.L.str.2, 24

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"average_deviation:  %f\n"
	.size	.L.str.3, 24

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"standard_deviation: %f\n"
	.size	.L.str.4, 24

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"variance:           %f\n"
	.size	.L.str.5, 24

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"skew:               %f\n"
	.size	.L.str.6, 24

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"kurtosis:           %f\n"
	.size	.L.str.7, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
