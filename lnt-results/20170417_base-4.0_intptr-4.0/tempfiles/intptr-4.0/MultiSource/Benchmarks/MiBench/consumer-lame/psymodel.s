	.text
	.file	"psymodel.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4652007308841189376     # double 1000
.LCPI0_1:
	.quad	4652218415073722368     # double 1024
.LCPI0_2:
	.quad	4711138879413944320     # double 9084621.09375
.LCPI0_4:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI0_5:
	.quad	4622945017495814144     # double 12
.LCPI0_6:
	.quad	4607182418800017408     # double 1
.LCPI0_7:
	.quad	4608308318706860032     # double 1.25
.LCPI0_8:
	.quad	-4610560118520545280    # double -2.5
.LCPI0_9:
	.quad	4621819117588971520     # double 10
.LCPI0_10:
	.quad	4626604192193052672     # double 21
.LCPI0_11:
	.quad	4598196632236683340     # double 0.25118864315095801
.LCPI0_12:
	.quad	4597463957383972899     # double 0.23025850929940458
.LCPI0_18:
	.quad	4600877379321698714     # double 0.40000000000000002
.LCPI0_19:
	.quad	4589210947481545209     # double 0.063095734448019317
.LCPI0_20:
	.quad	4587187278520977485     # double 0.048755843010000001
.LCPI0_21:
	.quad	4602659010228396791     # double 0.49890038269999998
.LCPI0_22:
	.quad	4608029698597914083     # double 1.1881339079849276
.LCPI0_23:
	.quad	4605616675118089831     # double 0.82616753136626364
.LCPI0_24:
	.quad	4625196817309499392     # double 16
.LCPI0_26:
	.quad	4658815484840378368     # double 3000
.LCPI0_29:
	.quad	4609794506583892296     # double 1.5800000000000001
.LCPI0_30:
	.quad	4613937818241073152     # double 3
.LCPI0_31:
	.quad	4604480259023595110     # double 0.69999999999999996
.LCPI0_32:
	.quad	4602678819172646912     # double 0.5
.LCPI0_34:
	.quad	0                       # double 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_3:
	.long	1053609165              # float 0.400000006
	.long	1053609165              # float 0.400000006
	.long	1053609165              # float 0.400000006
	.long	1053609165              # float 0.400000006
.LCPI0_13:
	.long	1060439283              # float 0.707106769
	.long	1060439283              # float 0.707106769
	.long	1060439283              # float 0.707106769
	.long	1060439283              # float 0.707106769
.LCPI0_15:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
.LCPI0_17:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI0_25:
	.quad	4625196817309499392     # double 16
	.quad	4611686018427387904     # double 2
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_14:
	.long	1056964608              # float 0.5
.LCPI0_16:
	.long	1065353216              # float 1
.LCPI0_27:
	.long	1106247680              # float 30
.LCPI0_28:
	.long	1092616192              # float 10
.LCPI0_33:
	.long	0                       # float 0
	.text
	.globl	L3psycho_anal
	.p2align	4, 0x90
	.type	L3psycho_anal,@function
L3psycho_anal:                          # @L3psycho_anal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$680, %rsp              # imm = 0x2A8
.Lcfi6:
	.cfi_def_cfa_offset 736
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movq	%r8, 152(%rsp)          # 8-byte Spill
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movl	%edx, %ebx
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	movq	%rdi, %r14
	testl	%ebx, %ebx
	movq	%r14, 64(%rsp)          # 8-byte Spill
	jne	.LBB0_122
# BB#1:
	movq	168(%r14), %rax
	testq	%rax, %rax
	jne	.LBB0_122
# BB#2:
	movabsq	$12884901891, %rax      # imm = 0x300000003
	movq	%rax, L3psycho_anal.blocktype_old(%rip)
	movl	16(%r14), %edx
	cmpl	$31999, %edx            # imm = 0x7CFF
	jg	.LBB0_6
# BB#3:
	cmpl	$16000, %edx            # imm = 0x3E80
	je	.LBB0_9
# BB#4:
	cmpl	$22050, %edx            # imm = 0x5622
	je	.LBB0_9
# BB#5:
	cmpl	$24000, %edx            # imm = 0x5DC0
	je	.LBB0_9
	jmp	.LBB0_334
.LBB0_6:
	cmpl	$32000, %edx            # imm = 0x7D00
	je	.LBB0_9
# BB#7:
	cmpl	$44100, %edx            # imm = 0xAC44
	je	.LBB0_9
# BB#8:
	cmpl	$48000, %edx            # imm = 0xBB80
	jne	.LBB0_334
.LBB0_9:                                # %min.iters.checked
	movl	$L3psycho_anal.rx_sav, %edi
	xorl	%esi, %esi
	movl	$16416, %edx            # imm = 0x4020
	callq	memset
	movl	$L3psycho_anal.ax_sav, %edi
	xorl	%esi, %esi
	movl	$16416, %edx            # imm = 0x4020
	callq	memset
	movl	$L3psycho_anal.bx_sav, %edi
	xorl	%esi, %esi
	movl	$16416, %edx            # imm = 0x4020
	callq	memset
	movl	$L3psycho_anal.en, %edi
	xorl	%esi, %esi
	movl	$1952, %edx             # imm = 0x7A0
	callq	memset
	movl	$L3psycho_anal.thm, %edi
	xorl	%esi, %esi
	movl	$1952, %edx             # imm = 0x7A0
	callq	memset
	movb	$1, L3psycho_anal.cw_lower_index(%rip)
	movss	152(%r14), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	ja	.LBB0_11
# BB#10:                                # %min.iters.checked
	movsd	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero
	jmp	.LBB0_12
.LBB0_11:
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	mulsd	.LCPI0_1(%rip), %xmm0
.LBB0_12:                               # %min.iters.checked
	xorps	%xmm1, %xmm1
	cvtsi2sdl	16(%r14), %xmm1
	divsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	$510, %eax              # imm = 0x1FE
	movl	$509, %ecx              # imm = 0x1FD
	cmovll	%eax, %ecx
	cmpl	$5, %ecx
	movl	$6, %eax
	cmovgl	%ecx, %eax
	movl	%eax, L3psycho_anal.cw_upper_index(%rip)
	movq	$-2048, %rax            # imm = 0xF800
	movapd	.LCPI0_3(%rip), %xmm0   # xmm0 = [4.000000e-01,4.000000e-01,4.000000e-01,4.000000e-01]
	.p2align	4, 0x90
.LBB0_13:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, L3psycho_anal.cw+2048(%rax)
	movapd	%xmm0, L3psycho_anal.cw+2064(%rax)
	movapd	%xmm0, L3psycho_anal.cw+2080(%rax)
	movapd	%xmm0, L3psycho_anal.cw+2096(%rax)
	movapd	%xmm0, L3psycho_anal.cw+2112(%rax)
	movapd	%xmm0, L3psycho_anal.cw+2128(%rax)
	movapd	%xmm0, L3psycho_anal.cw+2144(%rax)
	movapd	%xmm0, L3psycho_anal.cw+2160(%rax)
	movapd	%xmm0, L3psycho_anal.cw+2176(%rax)
	movapd	%xmm0, L3psycho_anal.cw+2192(%rax)
	movapd	%xmm0, L3psycho_anal.cw+2208(%rax)
	movapd	%xmm0, L3psycho_anal.cw+2224(%rax)
	movapd	%xmm0, L3psycho_anal.cw+2240(%rax)
	movapd	%xmm0, L3psycho_anal.cw+2256(%rax)
	movapd	%xmm0, L3psycho_anal.cw+2272(%rax)
	movapd	%xmm0, L3psycho_anal.cw+2288(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB0_13
# BB#14:                                # %scalar.ph
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movl	$1053609165, L3psycho_anal.cw+2048(%rip) # imm = 0x3ECCCCCD
	xorl	%ebx, %ebx
	movsd	.LCPI0_4(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB0_15:                               # %.preheader1096
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	mulsd	%xmm1, %xmm0
	movsd	.LCPI0_5(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm1, %xmm0
	callq	cos
	movsd	.LCPI0_6(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	.LCPI0_7(%rip), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	.LCPI0_8(%rip), %xmm0   # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	.LCPI0_9(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	pow
	movsd	%xmm0, L3psycho_anal.mld_s(,%rbx,8)
	leal	1(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI0_4(%rip), %xmm0
	divsd	.LCPI0_5(%rip), %xmm0
	callq	cos
	movsd	.LCPI0_6(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	mulsd	.LCPI0_7(%rip), %xmm1
	addsd	.LCPI0_8(%rip), %xmm1
	movsd	.LCPI0_9(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	pow
	movsd	.LCPI0_4(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	%xmm0, L3psycho_anal.mld_s+8(,%rbx,8)
	addq	$2, %rbx
	cmpq	$12, %rbx
	jne	.LBB0_15
# BB#16:                                # %.preheader1095.preheader
	xorl	%ebp, %ebp
	jmp	.LBB0_18
	.p2align	4, 0x90
.LBB0_17:                               # %.preheader1095.1
                                        #   in Loop: Header=BB0_18 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	.LCPI0_4(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	divsd	.LCPI0_10(%rip), %xmm0
	callq	cos
	movsd	.LCPI0_6(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	mulsd	.LCPI0_7(%rip), %xmm1
	addsd	.LCPI0_8(%rip), %xmm1
	movsd	.LCPI0_9(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	pow
	movsd	%xmm0, L3psycho_anal.mld_l+8(,%rbp,8)
	incq	%rbx
	movq	%rbx, %rbp
	movsd	.LCPI0_4(%rip), %xmm1   # xmm1 = mem[0],zero
.LBB0_18:                               # %.preheader1095
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	mulsd	%xmm1, %xmm0
	divsd	.LCPI0_10(%rip), %xmm0
	callq	cos
	movsd	.LCPI0_6(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	mulsd	.LCPI0_7(%rip), %xmm1
	addsd	.LCPI0_8(%rip), %xmm1
	movsd	.LCPI0_9(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	pow
	movsd	%xmm0, L3psycho_anal.mld_l(,%rbp,8)
	leaq	1(%rbp), %rbx
	cmpq	$21, %rbx
	jne	.LBB0_17
# BB#19:                                # %.preheader1094.preheader
	movl	$L3psycho_anal.partition_l, %edi
	movl	$255, %esi
	movl	$2052, %edx             # imm = 0x804
	callq	memset
	movl	16(%r14), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	leaq	184(%rsp), %rax
	movl	$L3psycho_anal.numlines_l, %edi
	movl	$L3psycho_anal.numlines_s, %esi
	movl	$L3psycho_anal.partition_l, %edx
	movl	$L3psycho_anal.minval, %ecx
	movl	$L3psycho_anal.qthr_l, %r8d
	movl	$L3psycho_anal.s3_l, %r9d
	pushq	$L3psycho_anal.w2_s
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	$L3psycho_anal.w1_s
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	$L3psycho_anal.bo_s
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	$L3psycho_anal.bu_s
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	$L3psycho_anal.w2_l
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	$L3psycho_anal.w1_l
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	$L3psycho_anal.bo_l
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	$L3psycho_anal.bu_l
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	$L3psycho_anal.qthr_s
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	$L3psycho_anal.s3_s
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	callq	L3para_read
	addq	$96, %rsp
.Lcfi25:
	.cfi_adjust_cfa_offset -96
	movl	$0, L3psycho_anal.npart_l_orig(%rip)
	movl	$0, L3psycho_anal.npart_s_orig(%rip)
	xorl	%esi, %esi
	movq	$-2052, %rax            # imm = 0xF7FC
	.p2align	4, 0x90
.LBB0_20:                               # =>This Inner Loop Header: Depth=1
	movl	L3psycho_anal.partition_l+2052(%rax), %ecx
	cmpl	%esi, %ecx
	jle	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_20 Depth=1
	movl	%ecx, L3psycho_anal.npart_l_orig(%rip)
	movl	%ecx, %esi
.LBB0_22:                               #   in Loop: Header=BB0_20 Depth=1
	movl	L3psycho_anal.partition_l+2056(%rax), %ecx
	cmpl	%esi, %ecx
	jle	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_20 Depth=1
	movl	%ecx, L3psycho_anal.npart_l_orig(%rip)
	movl	%ecx, %esi
.LBB0_24:                               #   in Loop: Header=BB0_20 Depth=1
	movl	L3psycho_anal.partition_l+2060(%rax), %ecx
	cmpl	%esi, %ecx
	jle	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_20 Depth=1
	movl	%ecx, L3psycho_anal.npart_l_orig(%rip)
	movl	%ecx, %esi
.LBB0_26:                               #   in Loop: Header=BB0_20 Depth=1
	addq	$12, %rax
	jne	.LBB0_20
# BB#27:
	leal	1(%rsi), %r11d
	movl	%r11d, L3psycho_anal.npart_l_orig(%rip)
	movl	$L3psycho_anal.numlines_s, %eax
	movl	$-2, %r12d
	movabsq	$-4294967296, %r13      # imm = 0xFFFFFFFF00000000
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	.p2align	4, 0x90
.LBB0_28:                               # =>This Inner Loop Header: Depth=1
	incl	%r12d
	addq	%rcx, %r13
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jns	.LBB0_28
# BB#29:
	leal	1(%r12), %r9d
	movl	%r9d, L3psycho_anal.npart_s_orig(%rip)
	movl	L3psycho_anal.bo_l+80(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, L3psycho_anal.npart_l(%rip)
	movl	L3psycho_anal.bo_s+44(%rip), %eax
	leal	1(%rax), %r15d
	movl	%r15d, L3psycho_anal.npart_s(%rip)
	cmpl	%esi, %ecx
	jle	.LBB0_31
# BB#30:
	movl	%r11d, L3psycho_anal.npart_l(%rip)
	movl	%esi, L3psycho_anal.bo_l+80(%rip)
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	movq	%rcx, L3psycho_anal.w2_l+160(%rip)
	movl	%r11d, %edx
.LBB0_31:
	cmpl	%r9d, %eax
	jl	.LBB0_33
# BB#32:
	movl	%r9d, L3psycho_anal.npart_s(%rip)
	movl	%r12d, L3psycho_anal.bo_s+44(%rip)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, L3psycho_anal.w2_s+88(%rip)
	movl	%r9d, %r15d
.LBB0_33:                               # %.preheader1093
	movl	%edx, 16(%rsp)          # 4-byte Spill
	testl	%edx, %edx
	jle	.LBB0_57
# BB#34:                                # %.preheader1092.lr.ph
	testl	%esi, %esi
	movslq	%r11d, %rdx
	movl	16(%rsp), %ebp          # 4-byte Reload
	movslq	%ebp, %r10
	js	.LBB0_43
# BB#35:                                # %.preheader1092.us.preheader
	decl	%r11d
	movl	$L3psycho_anal.s3_l, %r8d
	leaq	L3psycho_anal.s3_l-8(,%rdx,8), %rsi
	xorl	%ebx, %ebx
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_36:                               # %.preheader1092.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_37 Depth 2
                                        #     Child Loop BB0_40 Depth 2
	movq	%r8, %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_37:                               #   Parent Loop BB0_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB0_39
	jp	.LBB0_39
# BB#38:                                #   in Loop: Header=BB0_37 Depth=2
	incq	%rax
	addq	$8, %rcx
	cmpq	%rdx, %rax
	jl	.LBB0_37
.LBB0_39:                               # %._crit_edge1259.us
                                        #   in Loop: Header=BB0_36 Depth=1
	movl	%eax, L3psycho_anal.s3ind(,%rbx,8)
	movq	%rsi, %rax
	movl	%r11d, %edi
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB0_40:                               #   Parent Loop BB0_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %ebp
	cmpq	$2, %rcx
	jl	.LBB0_42
# BB#41:                                #   in Loop: Header=BB0_40 Depth=2
	decq	%rcx
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	leal	-1(%rbp), %edi
	addq	$-8, %rax
	ucomisd	%xmm0, %xmm1
	jne	.LBB0_42
	jnp	.LBB0_40
.LBB0_42:                               #   in Loop: Header=BB0_36 Depth=1
	movl	%ebp, L3psycho_anal.s3ind+4(,%rbx,8)
	incq	%rbx
	addq	$512, %r8               # imm = 0x200
	addq	$512, %rsi              # imm = 0x200
	cmpq	%r10, %rbx
	jl	.LBB0_36
	jmp	.LBB0_57
.LBB0_43:                               # %.preheader1092.preheader
	testb	$1, %r10b
	jne	.LBB0_45
# BB#44:
	xorl	%edi, %edi
	cmpl	$1, %ebp
	jne	.LBB0_49
	jmp	.LBB0_57
.LBB0_45:                               # %.preheader1092.prol
	movl	$0, L3psycho_anal.s3ind(%rip)
	leal	-1(%r11), %esi
	xorpd	%xmm0, %xmm0
	movq	%rdx, %rax
	.p2align	4, 0x90
.LBB0_46:                               # =>This Inner Loop Header: Depth=1
	movl	%esi, %ecx
	cmpq	$2, %rax
	jl	.LBB0_48
# BB#47:                                #   in Loop: Header=BB0_46 Depth=1
	movsd	L3psycho_anal.s3_l-8(,%rax,8), %xmm1 # xmm1 = mem[0],zero
	decq	%rax
	leal	-1(%rcx), %esi
	ucomisd	%xmm0, %xmm1
	jne	.LBB0_48
	jnp	.LBB0_46
.LBB0_48:
	movl	%ecx, L3psycho_anal.s3ind+4(%rip)
	movl	$1, %edi
	cmpl	$1, %ebp
	je	.LBB0_57
.LBB0_49:                               # %.preheader1092.preheader.new
	decl	%r11d
	movq	%rdi, %rax
	shlq	$9, %rax
	leaq	L3psycho_anal.s3_l-8(%rax,%rdx,8), %r8
	leaq	L3psycho_anal.s3_l+504(%rax,%rdx,8), %rbx
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_50:                               # %.preheader1092
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_51 Depth 2
                                        #     Child Loop BB0_54 Depth 2
	movl	$0, L3psycho_anal.s3ind(,%rdi,8)
	movq	%r8, %rax
	movl	%r11d, %r14d
	movq	%rdx, %rbp
	.p2align	4, 0x90
.LBB0_51:                               #   Parent Loop BB0_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r14d, %ecx
	cmpq	$2, %rbp
	jl	.LBB0_53
# BB#52:                                #   in Loop: Header=BB0_51 Depth=2
	decq	%rbp
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	leal	-1(%rcx), %r14d
	addq	$-8, %rax
	ucomisd	%xmm0, %xmm1
	jne	.LBB0_53
	jnp	.LBB0_51
.LBB0_53:                               # %.preheader1092.11765
                                        #   in Loop: Header=BB0_50 Depth=1
	movl	%ecx, L3psycho_anal.s3ind+4(,%rdi,8)
	movl	$0, L3psycho_anal.s3ind+8(,%rdi,8)
	movq	%rbx, %rax
	movl	%r11d, %esi
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB0_54:                               #   Parent Loop BB0_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %ebp
	cmpq	$2, %rcx
	jl	.LBB0_56
# BB#55:                                #   in Loop: Header=BB0_54 Depth=2
	decq	%rcx
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	leal	-1(%rbp), %esi
	addq	$-8, %rax
	ucomisd	%xmm0, %xmm1
	jne	.LBB0_56
	jnp	.LBB0_54
.LBB0_56:                               #   in Loop: Header=BB0_50 Depth=1
	movl	%ebp, L3psycho_anal.s3ind+12(,%rdi,8)
	addq	$2, %rdi
	addq	$1024, %r8              # imm = 0x400
	addq	$1024, %rbx             # imm = 0x400
	cmpq	%r10, %rdi
	jl	.LBB0_50
.LBB0_57:                               # %.preheader1091
	testl	%r15d, %r15d
	movl	16(%rsp), %r8d          # 4-byte Reload
	jle	.LBB0_81
# BB#58:                                # %.preheader1090.lr.ph
	sarq	$32, %r13
	testl	%r9d, %r9d
	movslq	%r15d, %r10
	jle	.LBB0_67
# BB#59:                                # %.preheader1090.us.preheader
	movslq	%r9d, %rax
	movl	$L3psycho_anal.s3_s, %edi
	leaq	L3psycho_anal.s3_s-8(,%rax,8), %rbp
	xorl	%ebx, %ebx
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_60:                               # %.preheader1090.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_61 Depth 2
                                        #     Child Loop BB0_64 Depth 2
	movq	%rdi, %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_61:                               #   Parent Loop BB0_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB0_63
	jp	.LBB0_63
# BB#62:                                #   in Loop: Header=BB0_61 Depth=2
	incq	%rax
	addq	$8, %rcx
	cmpq	%r13, %rax
	jl	.LBB0_61
.LBB0_63:                               # %._crit_edge1252.us
                                        #   in Loop: Header=BB0_60 Depth=1
	movl	%eax, L3psycho_anal.s3ind_s(,%rbx,8)
	movq	%rbp, %rax
	movl	%r12d, %esi
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB0_64:                               #   Parent Loop BB0_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %edx
	cmpq	$2, %rcx
	jl	.LBB0_66
# BB#65:                                #   in Loop: Header=BB0_64 Depth=2
	decq	%rcx
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	leal	-1(%rdx), %esi
	addq	$-8, %rax
	ucomisd	%xmm0, %xmm1
	jne	.LBB0_66
	jnp	.LBB0_64
.LBB0_66:                               #   in Loop: Header=BB0_60 Depth=1
	movl	%edx, L3psycho_anal.s3ind_s+4(,%rbx,8)
	incq	%rbx
	addq	$512, %rdi              # imm = 0x200
	addq	$512, %rbp              # imm = 0x200
	cmpq	%r10, %rbx
	jl	.LBB0_60
	jmp	.LBB0_81
.LBB0_67:                               # %.preheader1090.preheader
	testb	$1, %r10b
	jne	.LBB0_69
# BB#68:
	xorl	%edi, %edi
	cmpl	$1, %r15d
	jne	.LBB0_73
	jmp	.LBB0_81
.LBB0_69:                               # %.preheader1090.prol
	movl	$0, L3psycho_anal.s3ind_s(%rip)
	xorpd	%xmm0, %xmm0
	movl	%r12d, %edx
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB0_70:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	cmpq	$2, %rax
	jl	.LBB0_72
# BB#71:                                #   in Loop: Header=BB0_70 Depth=1
	movsd	L3psycho_anal.s3_s-8(,%rax,8), %xmm1 # xmm1 = mem[0],zero
	decq	%rax
	leal	-1(%rcx), %edx
	ucomisd	%xmm0, %xmm1
	jne	.LBB0_72
	jnp	.LBB0_70
.LBB0_72:
	movl	%ecx, L3psycho_anal.s3ind_s+4(%rip)
	movl	$1, %edi
	cmpl	$1, %r15d
	je	.LBB0_81
.LBB0_73:                               # %.preheader1090.preheader.new
	movq	%rdi, %rax
	shlq	$9, %rax
	movslq	%r9d, %rcx
	leaq	L3psycho_anal.s3_s-8(%rax,%rcx,8), %rbp
	leaq	L3psycho_anal.s3_s+504(%rax,%rcx,8), %rbx
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_74:                               # %.preheader1090
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_75 Depth 2
                                        #     Child Loop BB0_78 Depth 2
	movl	$0, L3psycho_anal.s3ind_s(,%rdi,8)
	movq	%rbp, %rax
	movl	%r12d, %esi
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB0_75:                               #   Parent Loop BB0_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %edx
	cmpq	$2, %rcx
	jl	.LBB0_77
# BB#76:                                #   in Loop: Header=BB0_75 Depth=2
	decq	%rcx
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	leal	-1(%rdx), %esi
	addq	$-8, %rax
	ucomisd	%xmm0, %xmm1
	jne	.LBB0_77
	jnp	.LBB0_75
.LBB0_77:                               # %.preheader1090.11762
                                        #   in Loop: Header=BB0_74 Depth=1
	movl	%edx, L3psycho_anal.s3ind_s+4(,%rdi,8)
	movl	$0, L3psycho_anal.s3ind_s+8(,%rdi,8)
	movq	%rbx, %rax
	movl	%r12d, %esi
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB0_78:                               #   Parent Loop BB0_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %edx
	cmpq	$2, %rcx
	jl	.LBB0_80
# BB#79:                                #   in Loop: Header=BB0_78 Depth=2
	decq	%rcx
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	leal	-1(%rdx), %esi
	addq	$-8, %rax
	ucomisd	%xmm0, %xmm1
	jne	.LBB0_80
	jnp	.LBB0_78
.LBB0_80:                               #   in Loop: Header=BB0_74 Depth=1
	movl	%edx, L3psycho_anal.s3ind_s+12(,%rdi,8)
	addq	$2, %rdi
	addq	$1024, %rbp             # imm = 0x400
	addq	$1024, %rbx             # imm = 0x400
	cmpq	%r10, %rdi
	jl	.LBB0_74
.LBB0_81:                               # %.preheader1089
	testl	%r8d, %r8d
	jle	.LBB0_99
# BB#82:                                # %.lr.ph1248
	movl	$L3psycho_anal.s3_l, %ecx
	movslq	%r8d, %r8
	xorl	%esi, %esi
	movl	$L3psycho_anal.s3_l+32, %edi
	movsd	.LCPI0_11(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB0_83:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_85 Depth 2
                                        #     Child Loop BB0_93 Depth 2
                                        #     Child Loop BB0_97 Depth 2
	movslq	L3psycho_anal.s3ind(,%rsi,8), %rbp
	movslq	L3psycho_anal.s3ind+4(,%rsi,8), %rbx
	cmpl	%ebx, %ebp
	jg	.LBB0_98
# BB#84:                                # %.lr.ph1239
                                        #   in Loop: Header=BB0_83 Depth=1
	leaq	-1(%rbp), %rax
	leaq	(%rcx,%rbp,8), %rdx
	xorpd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB0_85:                               #   Parent Loop BB0_83 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rdx), %xmm2
	incq	%rax
	addq	$8, %rdx
	cmpq	%rbx, %rax
	jl	.LBB0_85
# BB#86:                                # %._crit_edge1240
                                        #   in Loop: Header=BB0_83 Depth=1
	cmpl	%ebx, %ebp
	jg	.LBB0_98
# BB#87:                                # %.lr.ph1245
                                        #   in Loop: Header=BB0_83 Depth=1
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	cmpq	%rbx, %rbp
	movq	%rbx, %r11
	cmovgeq	%rbp, %r11
	incq	%r11
	subq	%rbp, %r11
	cmpq	$4, %r11
	jb	.LBB0_96
# BB#88:                                # %min.iters.checked1535
                                        #   in Loop: Header=BB0_83 Depth=1
	movq	%r11, %r9
	andq	$-4, %r9
	movq	%r11, %r10
	andq	$-4, %r10
	je	.LBB0_96
# BB#89:                                # %vector.ph1536
                                        #   in Loop: Header=BB0_83 Depth=1
	movapd	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	leaq	-4(%r10), %rdx
	movq	%rdx, %rax
	shrq	$2, %rax
	btl	$2, %edx
	jb	.LBB0_91
# BB#90:                                # %vector.body1532.prol
                                        #   in Loop: Header=BB0_83 Depth=1
	movq	%rsi, %rdx
	shlq	$9, %rdx
	movupd	L3psycho_anal.s3_l(%rdx,%rbp,8), %xmm3
	movupd	L3psycho_anal.s3_l+16(%rdx,%rbp,8), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	%xmm3, L3psycho_anal.s3_l(%rdx,%rbp,8)
	movupd	%xmm4, L3psycho_anal.s3_l+16(%rdx,%rbp,8)
	movl	$4, %edx
	testq	%rax, %rax
	jne	.LBB0_92
	jmp	.LBB0_94
.LBB0_91:                               #   in Loop: Header=BB0_83 Depth=1
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.LBB0_94
.LBB0_92:                               # %vector.ph1536.new
                                        #   in Loop: Header=BB0_83 Depth=1
	movq	%r10, %rax
	subq	%rdx, %rax
	addq	%rbp, %rdx
	leaq	(%rdi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB0_93:                               # %vector.body1532
                                        #   Parent Loop BB0_83 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-32(%rdx), %xmm3
	movupd	-16(%rdx), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	%xmm3, -32(%rdx)
	movupd	%xmm4, -16(%rdx)
	movupd	(%rdx), %xmm3
	movupd	16(%rdx), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	%xmm3, (%rdx)
	movupd	%xmm4, 16(%rdx)
	addq	$64, %rdx
	addq	$-8, %rax
	jne	.LBB0_93
.LBB0_94:                               # %middle.block1533
                                        #   in Loop: Header=BB0_83 Depth=1
	cmpq	%r10, %r11
	je	.LBB0_98
# BB#95:                                #   in Loop: Header=BB0_83 Depth=1
	addq	%r9, %rbp
	.p2align	4, 0x90
.LBB0_96:                               # %scalar.ph1534.preheader
                                        #   in Loop: Header=BB0_83 Depth=1
	leaq	(%rcx,%rbp,8), %rax
	decq	%rbp
	.p2align	4, 0x90
.LBB0_97:                               # %scalar.ph1534
                                        #   Parent Loop BB0_83 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, (%rax)
	incq	%rbp
	addq	$8, %rax
	cmpq	%rbx, %rbp
	jl	.LBB0_97
.LBB0_98:                               # %._crit_edge1246
                                        #   in Loop: Header=BB0_83 Depth=1
	incq	%rsi
	addq	$512, %rcx              # imm = 0x200
	addq	$512, %rdi              # imm = 0x200
	cmpq	%r8, %rsi
	jl	.LBB0_83
.LBB0_99:                               # %._crit_edge1249
	movq	64(%rsp), %rax          # 8-byte Reload
	cmpl	$1, 192(%rax)
	jne	.LBB0_103
# BB#100:                               # %._crit_edge1249
	testl	%r15d, %r15d
	jle	.LBB0_103
# BB#101:                               # %.lr.ph1235.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_102:                              # %.lr.ph1235
                                        # =>This Inner Loop Header: Depth=1
	movsd	176(%rsp,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI0_12(%rip), %xmm0
	callq	exp
	movsd	%xmm0, 176(%rsp,%rbx,8)
	incq	%rbx
	movslq	L3psycho_anal.npart_s(%rip), %r15
	cmpq	%r15, %rbx
	jl	.LBB0_102
.LBB0_103:                              # %.preheader1087
	testl	%r15d, %r15d
	jle	.LBB0_121
# BB#104:                               # %.lr.ph1232
	movl	$L3psycho_anal.s3_s, %ecx
	movslq	%r15d, %r9
	xorl	%edx, %edx
	movl	$L3psycho_anal.s3_s+32, %esi
	.p2align	4, 0x90
.LBB0_105:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_107 Depth 2
                                        #     Child Loop BB0_115 Depth 2
                                        #     Child Loop BB0_119 Depth 2
	movslq	L3psycho_anal.s3ind_s(,%rdx,8), %rdi
	movslq	L3psycho_anal.s3ind_s+4(,%rdx,8), %rbp
	cmpl	%ebp, %edi
	jg	.LBB0_120
# BB#106:                               # %.lr.ph1223
                                        #   in Loop: Header=BB0_105 Depth=1
	leaq	-1(%rdi), %rax
	leaq	(%rcx,%rdi,8), %rbx
	xorpd	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB0_107:                              #   Parent Loop BB0_105 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rbx), %xmm1
	incq	%rax
	addq	$8, %rbx
	cmpq	%rbp, %rax
	jl	.LBB0_107
# BB#108:                               # %._crit_edge1224
                                        #   in Loop: Header=BB0_105 Depth=1
	cmpl	%ebp, %edi
	jg	.LBB0_120
# BB#109:                               # %.lr.ph1229
                                        #   in Loop: Header=BB0_105 Depth=1
	movsd	176(%rsp,%rdx,8), %xmm0 # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	cmpq	%rbp, %rdi
	movq	%rbp, %r11
	cmovgeq	%rdi, %r11
	incq	%r11
	subq	%rdi, %r11
	cmpq	$4, %r11
	jb	.LBB0_118
# BB#110:                               # %min.iters.checked1553
                                        #   in Loop: Header=BB0_105 Depth=1
	movq	%r11, %r8
	andq	$-4, %r8
	movq	%r11, %r10
	andq	$-4, %r10
	je	.LBB0_118
# BB#111:                               # %vector.ph1557
                                        #   in Loop: Header=BB0_105 Depth=1
	movapd	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	-4(%r10), %rbx
	movq	%rbx, %rax
	shrq	$2, %rax
	btl	$2, %ebx
	jb	.LBB0_113
# BB#112:                               # %vector.body1548.prol
                                        #   in Loop: Header=BB0_105 Depth=1
	movq	%rdx, %rbx
	shlq	$9, %rbx
	movupd	L3psycho_anal.s3_s(%rbx,%rdi,8), %xmm2
	movupd	L3psycho_anal.s3_s+16(%rbx,%rdi,8), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	%xmm2, L3psycho_anal.s3_s(%rbx,%rdi,8)
	movupd	%xmm3, L3psycho_anal.s3_s+16(%rbx,%rdi,8)
	movl	$4, %ebx
	testq	%rax, %rax
	jne	.LBB0_114
	jmp	.LBB0_116
.LBB0_113:                              #   in Loop: Header=BB0_105 Depth=1
	xorl	%ebx, %ebx
	testq	%rax, %rax
	je	.LBB0_116
.LBB0_114:                              # %vector.ph1557.new
                                        #   in Loop: Header=BB0_105 Depth=1
	movq	%r10, %rax
	subq	%rbx, %rax
	addq	%rdi, %rbx
	leaq	(%rsi,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB0_115:                              # %vector.body1548
                                        #   Parent Loop BB0_105 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-32(%rbx), %xmm2
	movupd	-16(%rbx), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	%xmm2, -32(%rbx)
	movupd	%xmm3, -16(%rbx)
	movupd	(%rbx), %xmm2
	movupd	16(%rbx), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	%xmm2, (%rbx)
	movupd	%xmm3, 16(%rbx)
	addq	$64, %rbx
	addq	$-8, %rax
	jne	.LBB0_115
.LBB0_116:                              # %middle.block1549
                                        #   in Loop: Header=BB0_105 Depth=1
	cmpq	%r10, %r11
	je	.LBB0_120
# BB#117:                               #   in Loop: Header=BB0_105 Depth=1
	addq	%r8, %rdi
	.p2align	4, 0x90
.LBB0_118:                              # %scalar.ph1550.preheader
                                        #   in Loop: Header=BB0_105 Depth=1
	leaq	(%rcx,%rdi,8), %rax
	decq	%rdi
	.p2align	4, 0x90
.LBB0_119:                              # %scalar.ph1550
                                        #   Parent Loop BB0_105 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax)
	incq	%rdi
	addq	$8, %rax
	cmpq	%rbp, %rdi
	jl	.LBB0_119
.LBB0_120:                              # %._crit_edge1230
                                        #   in Loop: Header=BB0_105 Depth=1
	incq	%rdx
	addq	$512, %rcx              # imm = 0x200
	addq	$512, %rsi              # imm = 0x200
	cmpq	%r9, %rdx
	jl	.LBB0_105
.LBB0_121:                              # %._crit_edge1233
	callq	init_fft
	movq	64(%rsp), %r14          # 8-byte Reload
	movl	8(%rsp), %ebx           # 4-byte Reload
.LBB0_122:
	cmpl	$1, 36(%r14)
	movl	$4, %ecx
	cmovnel	204(%r14), %ecx
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	jle	.LBB0_290
# BB#123:                               # %.lr.ph1218
	movslq	%ebx, %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	%ecx, 84(%rsp)          # 4-byte Spill
	movl	%ecx, %eax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_124:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_128 Depth 2
                                        #     Child Loop BB0_130 Depth 2
                                        #     Child Loop BB0_132 Depth 2
                                        #     Child Loop BB0_134 Depth 2
                                        #     Child Loop BB0_136 Depth 2
                                        #     Child Loop BB0_138 Depth 2
                                        #     Child Loop BB0_140 Depth 2
                                        #     Child Loop BB0_142 Depth 2
                                        #     Child Loop BB0_145 Depth 2
                                        #     Child Loop BB0_158 Depth 2
                                        #     Child Loop BB0_175 Depth 2
                                        #       Child Loop BB0_181 Depth 3
                                        #     Child Loop BB0_187 Depth 2
                                        #       Child Loop BB0_190 Depth 3
                                        #       Child Loop BB0_194 Depth 3
                                        #     Child Loop BB0_199 Depth 2
                                        #       Child Loop BB0_201 Depth 3
                                        #     Child Loop BB0_219 Depth 2
                                        #     Child Loop BB0_226 Depth 2
                                        #       Child Loop BB0_229 Depth 3
                                        #       Child Loop BB0_231 Depth 3
                                        #     Child Loop BB0_256 Depth 2
                                        #       Child Loop BB0_258 Depth 3
                                        #         Child Loop BB0_260 Depth 4
                                        #       Child Loop BB0_263 Depth 3
                                        #         Child Loop BB0_266 Depth 4
                                        #         Child Loop BB0_268 Depth 4
                                        #     Child Loop BB0_235 Depth 2
                                        #       Child Loop BB0_236 Depth 3
                                        #         Child Loop BB0_238 Depth 4
                                        #       Child Loop BB0_242 Depth 3
                                        #         Child Loop BB0_244 Depth 4
                                        #       Child Loop BB0_247 Depth 3
                                        #         Child Loop BB0_250 Depth 4
                                        #         Child Loop BB0_252 Depth 4
	movl	%r15d, %ebp
	andl	$1, %ebp
	movq	%rbp, %rax
	shlq	$12, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	L3psycho_anal.wsamp_L(%rax), %r14
	movq	%rbp, %rax
	shlq	$8, %rax
	leaq	(%rax,%rax,2), %r13
	cmpq	$1, %r15
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jg	.LBB0_126
# BB#125:                               #   in Loop: Header=BB0_124 Depth=1
	movq	%r14, %rdi
	movl	%r15d, %esi
	movq	160(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdx
	callq	fft_long
	leaq	(%rbp,%rbp,2), %rax
	shlq	$10, %rax
	leaq	L3psycho_anal.wsamp_S(%rax), %rdi
	movl	%r15d, %esi
	movq	%rbx, %rdx
	callq	fft_short
	movq	L3psycho_anal.pe(,%r15,8), %rax
	movq	752(%rsp), %rcx
	movq	%rax, (%rcx,%r15,8)
	imulq	$1952, 136(%rsp), %rbx  # 8-byte Folded Reload
                                        # imm = 0x7A0
	addq	736(%rsp), %rbx
	imulq	$976, %r15, %rbp        # imm = 0x3D0
	leaq	(%rbx,%rbp), %rdi
	imulq	$488, %r15, %r12        # imm = 0x1E8
	leaq	L3psycho_anal.thm(%r12), %rsi
	movl	$488, %edx              # imm = 0x1E8
	callq	memcpy
	leaq	488(%rbp,%rbx), %rdi
	leaq	L3psycho_anal.en(%r12), %rsi
	movl	$488, %edx              # imm = 0x1E8
	callq	memcpy
	movss	.LCPI0_14(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	jmp	.LBB0_135
	.p2align	4, 0x90
.LBB0_126:                              #   in Loop: Header=BB0_124 Depth=1
	movq	L3psycho_anal.pe(,%r15,8), %rax
	leaq	-2(%r15), %rcx
	movq	760(%rsp), %rdx
	movq	%rax, -16(%rdx,%r15,8)
	imulq	$1952, 136(%rsp), %rax  # 8-byte Folded Reload
                                        # imm = 0x7A0
	addq	744(%rsp), %rax
	imulq	$976, %rcx, %rcx        # imm = 0x3D0
	leaq	(%rax,%rcx), %rbp
	leaq	488(%rcx,%rax), %rdi
	imulq	$488, %r15, %rbx        # imm = 0x1E8
	leaq	L3psycho_anal.en(%rbx), %rsi
	movl	$488, %edx              # imm = 0x1E8
	callq	memcpy
	leaq	L3psycho_anal.thm(%rbx), %rsi
	movl	$488, %edx              # imm = 0x1E8
	movq	%rbp, %rdi
	callq	memcpy
	cmpq	$2, %r15
	movss	.LCPI0_14(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movaps	.LCPI0_13(%rip), %xmm3  # xmm3 = [7.071068e-01,7.071068e-01,7.071068e-01,7.071068e-01]
	jne	.LBB0_135
# BB#127:                               # %vector.body1688.preheader
                                        #   in Loop: Header=BB0_124 Depth=1
	movl	$4096, %eax             # imm = 0x1000
	.p2align	4, 0x90
.LBB0_128:                              # %vector.body1688
                                        #   Parent Loop BB0_124 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	L3psycho_anal.wsamp_L-16(%rax), %xmm0
	movups	L3psycho_anal.wsamp_L+4080(%rax), %xmm1
	movaps	%xmm0, %xmm2
	addps	%xmm1, %xmm2
	mulps	%xmm3, %xmm2
	movups	%xmm2, L3psycho_anal.wsamp_L-16(%rax)
	subps	%xmm1, %xmm0
	mulps	%xmm3, %xmm0
	movups	%xmm0, L3psycho_anal.wsamp_L+4080(%rax)
	movups	L3psycho_anal.wsamp_L-32(%rax), %xmm0
	movups	L3psycho_anal.wsamp_L+4064(%rax), %xmm1
	movaps	%xmm0, %xmm2
	addps	%xmm1, %xmm2
	mulps	%xmm3, %xmm2
	movups	%xmm2, L3psycho_anal.wsamp_L-32(%rax)
	subps	%xmm1, %xmm0
	mulps	%xmm3, %xmm0
	movups	%xmm0, L3psycho_anal.wsamp_L+4064(%rax)
	addq	$-32, %rax
	jne	.LBB0_128
# BB#129:                               # %vector.body1669.preheader
                                        #   in Loop: Header=BB0_124 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_130:                              # %vector.body1669
                                        #   Parent Loop BB0_124 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	L3psycho_anal.wsamp_S+3056(%rax), %xmm0
	movups	L3psycho_anal.wsamp_S+6128(%rax), %xmm1
	movaps	%xmm0, %xmm2
	addps	%xmm1, %xmm2
	mulps	%xmm3, %xmm2
	movups	%xmm2, L3psycho_anal.wsamp_S+3056(%rax)
	subps	%xmm1, %xmm0
	mulps	%xmm3, %xmm0
	movups	%xmm0, L3psycho_anal.wsamp_S+6128(%rax)
	movups	L3psycho_anal.wsamp_S+3040(%rax), %xmm0
	movups	L3psycho_anal.wsamp_S+6112(%rax), %xmm1
	movaps	%xmm0, %xmm2
	addps	%xmm1, %xmm2
	mulps	%xmm3, %xmm2
	movups	%xmm2, L3psycho_anal.wsamp_S+3040(%rax)
	subps	%xmm1, %xmm0
	mulps	%xmm3, %xmm0
	movups	%xmm0, L3psycho_anal.wsamp_S+6112(%rax)
	addq	$-32, %rax
	cmpq	$-1024, %rax            # imm = 0xFC00
	jne	.LBB0_130
# BB#131:                               # %vector.body1650.preheader
                                        #   in Loop: Header=BB0_124 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_132:                              # %vector.body1650
                                        #   Parent Loop BB0_124 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	L3psycho_anal.wsamp_S+2032(%rax), %xmm0
	movups	L3psycho_anal.wsamp_S+5104(%rax), %xmm1
	movaps	%xmm0, %xmm2
	addps	%xmm1, %xmm2
	mulps	%xmm3, %xmm2
	movups	%xmm2, L3psycho_anal.wsamp_S+2032(%rax)
	subps	%xmm1, %xmm0
	mulps	%xmm3, %xmm0
	movups	%xmm0, L3psycho_anal.wsamp_S+5104(%rax)
	movups	L3psycho_anal.wsamp_S+2016(%rax), %xmm0
	movups	L3psycho_anal.wsamp_S+5088(%rax), %xmm1
	movaps	%xmm0, %xmm2
	addps	%xmm1, %xmm2
	mulps	%xmm3, %xmm2
	movups	%xmm2, L3psycho_anal.wsamp_S+2016(%rax)
	subps	%xmm1, %xmm0
	mulps	%xmm3, %xmm0
	movups	%xmm0, L3psycho_anal.wsamp_S+5088(%rax)
	addq	$-32, %rax
	cmpq	$-1024, %rax            # imm = 0xFC00
	jne	.LBB0_132
# BB#133:                               # %vector.body1631.preheader
                                        #   in Loop: Header=BB0_124 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_134:                              # %vector.body1631
                                        #   Parent Loop BB0_124 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	L3psycho_anal.wsamp_S+1008(%rax), %xmm0
	movups	L3psycho_anal.wsamp_S+4080(%rax), %xmm1
	movaps	%xmm0, %xmm2
	addps	%xmm1, %xmm2
	mulps	%xmm3, %xmm2
	movups	%xmm2, L3psycho_anal.wsamp_S+1008(%rax)
	subps	%xmm1, %xmm0
	mulps	%xmm3, %xmm0
	movups	%xmm0, L3psycho_anal.wsamp_S+4080(%rax)
	movups	L3psycho_anal.wsamp_S+992(%rax), %xmm0
	movups	L3psycho_anal.wsamp_S+4064(%rax), %xmm1
	movaps	%xmm0, %xmm2
	addps	%xmm1, %xmm2
	mulps	%xmm3, %xmm2
	movups	%xmm2, L3psycho_anal.wsamp_S+992(%rax)
	subps	%xmm1, %xmm0
	mulps	%xmm3, %xmm0
	movups	%xmm0, L3psycho_anal.wsamp_S+4064(%rax)
	addq	$-32, %rax
	cmpq	$-1024, %rax            # imm = 0xFC00
	jne	.LBB0_134
.LBB0_135:                              # %.loopexit1085
                                        #   in Loop: Header=BB0_124 Depth=1
	leaq	L3psycho_anal.wsamp_S+1008(,%r13,4), %rax
	leaq	L3psycho_anal.wsamp_S+2032(,%r13,4), %rcx
	leaq	L3psycho_anal.wsamp_S+3056(,%r13,4), %rdx
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	movss	%xmm0, L3psycho_anal.energy(%rip)
	movss	%xmm0, 176(%rsp,%r15,4)
	movl	$8, %esi
	movl	$513, %edi              # imm = 0x201
	movaps	.LCPI0_15(%rip), %xmm5  # xmm5 = [5.000000e-01,5.000000e-01,5.000000e-01,5.000000e-01]
	.p2align	4, 0x90
.LBB0_136:                              #   Parent Loop BB0_124 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-4(%r14,%rsi), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movss	2040(%r14,%rdi,4), %xmm2 # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm4, %xmm2
	movss	%xmm2, L3psycho_anal.energy-4(%rsi)
	addss	%xmm0, %xmm2
	movss	(%r14,%rsi), %xmm1      # xmm1 = mem[0],zero,zero,zero
	movss	2036(%r14,%rdi,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	mulss	%xmm4, %xmm0
	movss	%xmm0, L3psycho_anal.energy(%rsi)
	addss	%xmm2, %xmm0
	addq	$-2, %rdi
	addq	$8, %rsi
	cmpq	$1, %rdi
	jg	.LBB0_136
# BB#137:                               # %.preheader1083
                                        #   in Loop: Header=BB0_124 Depth=1
	movss	%xmm0, 176(%rsp,%r15,4)
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%rsi,%rsi,2), %r12
	shlq	$10, %r12
	movss	L3psycho_anal.wsamp_S+2048(%r12), %xmm0 # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	movss	%xmm0, L3psycho_anal.energy_s+1032(%rip)
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_138:                              # %vector.body1611
                                        #   Parent Loop BB0_124 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	L3psycho_anal.wsamp_S+2052(%rsi,%r13,4), %xmm0
	movups	L3psycho_anal.wsamp_S+2068(%rsi,%r13,4), %xmm1
	movups	-16(%rdx), %xmm2
	movups	(%rdx), %xmm3
	mulps	%xmm0, %xmm0
	mulps	%xmm1, %xmm1
	mulps	%xmm3, %xmm3
	shufps	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0]
	mulps	%xmm2, %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	addps	%xmm0, %xmm3
	addps	%xmm1, %xmm2
	mulps	%xmm5, %xmm3
	mulps	%xmm5, %xmm2
	movups	%xmm3, L3psycho_anal.energy_s+1036(%rsi)
	movups	%xmm2, L3psycho_anal.energy_s+1052(%rsi)
	addq	$32, %rsi
	addq	$-32, %rdx
	cmpq	$512, %rsi              # imm = 0x200
	jne	.LBB0_138
# BB#139:                               # %middle.block1612
                                        #   in Loop: Header=BB0_124 Depth=1
	movss	L3psycho_anal.wsamp_S+1024(%r12), %xmm0 # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	movss	%xmm0, L3psycho_anal.energy_s+516(%rip)
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_140:                              # %vector.body1591
                                        #   Parent Loop BB0_124 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	L3psycho_anal.wsamp_S+1028(%rdx,%r13,4), %xmm0
	movups	L3psycho_anal.wsamp_S+1044(%rdx,%r13,4), %xmm1
	movups	-16(%rcx), %xmm2
	movups	(%rcx), %xmm3
	mulps	%xmm0, %xmm0
	mulps	%xmm1, %xmm1
	mulps	%xmm3, %xmm3
	shufps	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0]
	mulps	%xmm2, %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	addps	%xmm0, %xmm3
	addps	%xmm1, %xmm2
	mulps	%xmm5, %xmm3
	mulps	%xmm5, %xmm2
	movups	%xmm3, L3psycho_anal.energy_s+520(%rdx)
	movups	%xmm2, L3psycho_anal.energy_s+536(%rdx)
	addq	$32, %rdx
	addq	$-32, %rcx
	cmpq	$512, %rdx              # imm = 0x200
	jne	.LBB0_140
# BB#141:                               # %middle.block1592
                                        #   in Loop: Header=BB0_124 Depth=1
	movss	L3psycho_anal.wsamp_S(%r12), %xmm0 # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	movss	%xmm0, L3psycho_anal.energy_s(%rip)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_142:                              # %vector.body1572
                                        #   Parent Loop BB0_124 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	L3psycho_anal.wsamp_S+4(%rcx,%r13,4), %xmm0
	movups	L3psycho_anal.wsamp_S+20(%rcx,%r13,4), %xmm1
	movups	-16(%rax), %xmm2
	movups	(%rax), %xmm3
	mulps	%xmm0, %xmm0
	mulps	%xmm1, %xmm1
	mulps	%xmm3, %xmm3
	shufps	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0]
	mulps	%xmm2, %xmm2
	shufps	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0]
	addps	%xmm0, %xmm3
	addps	%xmm1, %xmm2
	mulps	%xmm5, %xmm3
	mulps	%xmm5, %xmm2
	movups	%xmm3, L3psycho_anal.energy_s+4(%rcx)
	movups	%xmm2, L3psycho_anal.energy_s+20(%rcx)
	addq	$32, %rcx
	addq	$-32, %rax
	cmpq	$512, %rcx              # imm = 0x200
	jne	.LBB0_142
# BB#143:                               # %.preheader10821338
                                        #   in Loop: Header=BB0_124 Depth=1
	xorl	%eax, %eax
	cmpb	$1, L3psycho_anal.cw_lower_index(%rip)
	jne	.LBB0_156
# BB#144:                               # %.lr.ph1126.preheader
                                        #   in Loop: Header=BB0_124 Depth=1
	movl	$1024, %r13d            # imm = 0x400
	movq	72(%rsp), %rbx          # 8-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_145:                              # %.lr.ph1126
                                        #   Parent Loop BB0_124 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	L3psycho_anal.ax_sav+2052(%rbx), %xmm10 # xmm10 = mem[0],zero,zero,zero
	movss	L3psycho_anal.bx_sav+2052(%rbx), %xmm11 # xmm11 = mem[0],zero,zero,zero
	movss	L3psycho_anal.rx_sav+2052(%rbx), %xmm9 # xmm9 = mem[0],zero,zero,zero
	movss	L3psycho_anal.ax_sav(%rbx), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, L3psycho_anal.ax_sav+2052(%rbx)
	movss	L3psycho_anal.bx_sav(%rbx), %xmm2 # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, L3psycho_anal.bx_sav+2052(%rbx)
	movss	L3psycho_anal.rx_sav(%rbx), %xmm7 # xmm7 = mem[0],zero,zero,zero
	movss	%xmm7, L3psycho_anal.rx_sav+2052(%rbx)
	movss	(%r14,%rbp,4), %xmm6    # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, L3psycho_anal.ax_sav(%rbx)
	testq	%rbp, %rbp
	movq	%r13, %rax
	cmoveq	%rbp, %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movss	L3psycho_anal.wsamp_L(%rcx,%rax,4), %xmm8 # xmm8 = mem[0],zero,zero,zero
	movss	%xmm8, L3psycho_anal.bx_sav(%rbx)
	movss	L3psycho_anal.energy(,%rbp,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm5, %xmm5
	sqrtss	%xmm0, %xmm5
	ucomiss	%xmm5, %xmm5
	jnp	.LBB0_147
# BB#146:                               # %call.sqrt
                                        #   in Loop: Header=BB0_145 Depth=2
	movss	%xmm6, 8(%rsp)          # 4-byte Spill
	movaps	%xmm7, 48(%rsp)         # 16-byte Spill
	movss	%xmm8, 112(%rsp)        # 4-byte Spill
	movss	%xmm9, 44(%rsp)         # 4-byte Spill
	movss	%xmm10, 100(%rsp)       # 4-byte Spill
	movss	%xmm11, 96(%rsp)        # 4-byte Spill
	movss	%xmm3, 92(%rsp)         # 4-byte Spill
	movss	%xmm2, 88(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	88(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	92(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movss	100(%rsp), %xmm10       # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	44(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movss	112(%rsp), %xmm8        # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm7         # 16-byte Reload
	movss	8(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	.LCPI0_14(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm5
.LBB0_147:                              # %.lr.ph1126.split
                                        #   in Loop: Header=BB0_145 Depth=2
	movss	%xmm5, L3psycho_anal.rx_sav(%rbx)
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm7
	movss	.LCPI0_16(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm12
	jne	.LBB0_148
	jnp	.LBB0_149
.LBB0_148:                              #   in Loop: Header=BB0_145 Depth=2
	movaps	%xmm3, %xmm1
	mulss	%xmm2, %xmm1
	mulss	%xmm3, %xmm3
	mulss	%xmm2, %xmm2
	subss	%xmm2, %xmm3
	mulss	%xmm4, %xmm3
	movaps	%xmm7, %xmm12
	mulss	%xmm12, %xmm12
	movaps	%xmm3, %xmm0
.LBB0_149:                              #   in Loop: Header=BB0_145 Depth=2
	ucomiss	.LCPI0_33, %xmm9
	jne	.LBB0_150
	jnp	.LBB0_151
.LBB0_150:                              #   in Loop: Header=BB0_145 Depth=2
	movaps	%xmm10, %xmm3
	addss	%xmm11, %xmm3
	mulss	%xmm0, %xmm11
	addss	%xmm1, %xmm0
	mulss	%xmm0, %xmm3
	mulss	%xmm4, %xmm3
	mulss	%xmm1, %xmm10
	movaps	%xmm3, %xmm0
	subss	%xmm10, %xmm0
	subss	%xmm11, %xmm3
	mulss	%xmm9, %xmm12
	movaps	%xmm3, %xmm1
.LBB0_151:                              #   in Loop: Header=BB0_145 Depth=2
	addss	%xmm7, %xmm7
	subss	%xmm9, %xmm7
	movaps	%xmm7, %xmm3
	andps	.LCPI0_17(%rip), %xmm3
	addss	%xmm3, %xmm5
	ucomiss	.LCPI0_33, %xmm5
	jne	.LBB0_152
	jnp	.LBB0_155
.LBB0_152:                              #   in Loop: Header=BB0_145 Depth=2
	divss	%xmm12, %xmm7
	mulss	%xmm7, %xmm0
	mulss	%xmm1, %xmm7
	movaps	%xmm6, %xmm1
	addss	%xmm8, %xmm1
	mulss	%xmm4, %xmm1
	subss	%xmm7, %xmm1
	subss	%xmm8, %xmm6
	mulss	%xmm4, %xmm6
	subss	%xmm0, %xmm6
	mulss	%xmm1, %xmm1
	mulss	%xmm6, %xmm6
	addss	%xmm1, %xmm6
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm6, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_154
# BB#153:                               # %call.sqrt2596
                                        #   in Loop: Header=BB0_145 Depth=2
	movapd	%xmm1, %xmm0
	movss	%xmm5, 8(%rsp)          # 4-byte Spill
	callq	sqrt
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	.LCPI0_14(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
.LBB0_154:                              # %.split
                                        #   in Loop: Header=BB0_145 Depth=2
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm5, %xmm1
	divsd	%xmm1, %xmm0
	xorps	%xmm5, %xmm5
	cvtsd2ss	%xmm0, %xmm5
.LBB0_155:                              #   in Loop: Header=BB0_145 Depth=2
	movss	%xmm5, L3psycho_anal.cw(,%rbp,4)
	incq	%rbp
	movzbl	L3psycho_anal.cw_lower_index(%rip), %eax
	testb	%al, %al
	movl	$0, %eax
	movl	$6, %ecx
	cmovnel	%ecx, %eax
	addq	$4, %rbx
	decq	%r13
	cmpq	%rax, %rbp
	jl	.LBB0_145
.LBB0_156:                              # %.preheader1081
                                        #   in Loop: Header=BB0_124 Depth=1
	movl	L3psycho_anal.cw_upper_index(%rip), %ecx
	cmpl	%ecx, %eax
	movabsq	$17179869184, %r13      # imm = 0x400000000
	jge	.LBB0_173
# BB#157:                               # %.lr.ph1129.preheader
                                        #   in Loop: Header=BB0_124 Depth=1
	movl	%eax, %ebp
	movq	%rbp, %r14
	shlq	$32, %r14
	.p2align	4, 0x90
.LBB0_158:                              # %.lr.ph1129
                                        #   Parent Loop BB0_124 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	2(%rbp), %eax
	sarl	$31, %eax
	shrl	$30, %eax
	leal	2(%rbp,%rax), %eax
	sarl	$2, %eax
	movslq	%eax, %rbx
	movss	L3psycho_anal.energy_s(,%rbx,4), %xmm8 # xmm8 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm8
	jne	.LBB0_159
	jnp	.LBB0_161
.LBB0_159:                              #   in Loop: Header=BB0_158 Depth=2
	movss	L3psycho_anal.wsamp_S(%r12,%rbx,4), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movl	$256, %eax              # imm = 0x100
	subl	%ebx, %eax
	cltq
	movss	L3psycho_anal.wsamp_S(%r12,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm6
	mulss	%xmm0, %xmm6
	mulss	%xmm3, %xmm3
	mulss	%xmm0, %xmm0
	subss	%xmm0, %xmm3
	mulss	%xmm4, %xmm3
	xorps	%xmm5, %xmm5
	sqrtss	%xmm8, %xmm5
	ucomiss	%xmm5, %xmm5
	jnp	.LBB0_162
# BB#160:                               # %call.sqrt2598
                                        #   in Loop: Header=BB0_158 Depth=2
	movaps	%xmm8, %xmm0
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movss	%xmm8, 8(%rsp)          # 4-byte Spill
	movss	%xmm6, 48(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	48(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI0_14(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm5
	jmp	.LBB0_162
	.p2align	4, 0x90
.LBB0_161:                              #   in Loop: Header=BB0_158 Depth=2
	movaps	%xmm8, %xmm5
	movss	.LCPI0_16(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm6
.LBB0_162:                              #   in Loop: Header=BB0_158 Depth=2
	movss	L3psycho_anal.energy_s+1032(,%rbx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI0_33, %xmm0
	jne	.LBB0_163
	jnp	.LBB0_166
.LBB0_163:                              #   in Loop: Header=BB0_158 Depth=2
	movss	L3psycho_anal.wsamp_S+2048(%r12,%rbx,4), %xmm7 # xmm7 = mem[0],zero,zero,zero
	movl	$256, %eax              # imm = 0x100
	subl	%ebx, %eax
	cltq
	movss	L3psycho_anal.wsamp_S+2048(%r12,%rax,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm2
	addss	%xmm3, %xmm2
	mulss	%xmm7, %xmm6
	addss	%xmm1, %xmm7
	mulss	%xmm2, %xmm7
	mulss	%xmm4, %xmm7
	mulss	%xmm1, %xmm3
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB0_165
# BB#164:                               # %call.sqrt2600
                                        #   in Loop: Header=BB0_158 Depth=2
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movss	%xmm8, 8(%rsp)          # 4-byte Spill
	movss	%xmm6, 48(%rsp)         # 4-byte Spill
	movaps	%xmm5, 112(%rsp)        # 16-byte Spill
	movss	%xmm7, 44(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	44(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	112(%rsp), %xmm5        # 16-byte Reload
	movss	48(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI0_14(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB0_165:                              # %.split2599
                                        #   in Loop: Header=BB0_158 Depth=2
	movaps	%xmm7, %xmm2
	subss	%xmm6, %xmm2
	subss	%xmm3, %xmm7
	mulss	%xmm1, %xmm8
	movaps	%xmm1, %xmm0
	movaps	%xmm7, %xmm6
	movaps	%xmm2, %xmm3
.LBB0_166:                              #   in Loop: Header=BB0_158 Depth=2
	addss	%xmm5, %xmm5
	subss	%xmm0, %xmm5
	movss	L3psycho_anal.energy_s+516(,%rbx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB0_168
# BB#167:                               # %call.sqrt2602
                                        #   in Loop: Header=BB0_158 Depth=2
	movaps	%xmm1, %xmm0
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movss	%xmm8, 8(%rsp)          # 4-byte Spill
	movss	%xmm6, 48(%rsp)         # 4-byte Spill
	movaps	%xmm5, 112(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	112(%rsp), %xmm5        # 16-byte Reload
	movss	48(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI0_14(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
.LBB0_168:                              # %.split2601
                                        #   in Loop: Header=BB0_158 Depth=2
	movaps	%xmm5, %xmm7
	andps	.LCPI0_17(%rip), %xmm7
	addss	%xmm0, %xmm7
	ucomiss	.LCPI0_33, %xmm7
	jne	.LBB0_169
	jnp	.LBB0_172
.LBB0_169:                              #   in Loop: Header=BB0_158 Depth=2
	divss	%xmm8, %xmm5
	mulss	%xmm5, %xmm3
	mulss	%xmm5, %xmm6
	movss	L3psycho_anal.wsamp_S+1024(%r12,%rbx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movl	$256, %eax              # imm = 0x100
	subl	%ebx, %eax
	cltq
	movss	L3psycho_anal.wsamp_S+1024(%r12,%rax,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm4, %xmm2
	subss	%xmm6, %xmm2
	subss	%xmm1, %xmm0
	mulss	%xmm4, %xmm0
	subss	%xmm3, %xmm0
	mulss	%xmm2, %xmm2
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_171
# BB#170:                               # %call.sqrt2604
                                        #   in Loop: Header=BB0_158 Depth=2
	movapd	%xmm1, %xmm0
	movaps	%xmm7, 16(%rsp)         # 16-byte Spill
	callq	sqrt
	movaps	16(%rsp), %xmm7         # 16-byte Reload
	movss	.LCPI0_14(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
.LBB0_171:                              # %.split2603
                                        #   in Loop: Header=BB0_158 Depth=2
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm7, %xmm1
	divsd	%xmm1, %xmm0
	xorps	%xmm7, %xmm7
	cvtsd2ss	%xmm0, %xmm7
.LBB0_172:                              #   in Loop: Header=BB0_158 Depth=2
	movss	%xmm7, L3psycho_anal.cw(,%rbp,4)
	movss	%xmm7, L3psycho_anal.cw+12(,%rbp,4)
	movss	%xmm7, L3psycho_anal.cw+8(,%rbp,4)
	movq	%r14, %rax
	sarq	$30, %rax
	orq	$4, %rax
	movss	%xmm7, L3psycho_anal.cw(%rax)
	addq	$4, %rbp
	movslq	L3psycho_anal.cw_upper_index(%rip), %rcx
	addq	%r13, %r14
	cmpq	%rcx, %rbp
	jl	.LBB0_158
.LBB0_173:                              # %.preheader1080
                                        #   in Loop: Header=BB0_124 Depth=1
	testl	%ecx, %ecx
	jle	.LBB0_184
# BB#174:                               # %.lr.ph1143.preheader
                                        #   in Loop: Header=BB0_124 Depth=1
	xorl	%edx, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_175:                              # %.lr.ph1143
                                        #   Parent Loop BB0_124 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_181 Depth 3
	movslq	%eax, %rdi
	movss	L3psycho_anal.energy(,%rdi,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	L3psycho_anal.cw(,%rdi,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	cvtps2pd	%xmm0, %xmm0
	movl	L3psycho_anal.numlines_l(,%rdx,4), %esi
	incl	%edi
	cmpl	$2, %esi
	jl	.LBB0_178
# BB#176:                               # %.lr.ph1136.preheader
                                        #   in Loop: Header=BB0_175 Depth=2
	movslq	%edi, %rdi
	testb	$1, %sil
	jne	.LBB0_179
# BB#177:                               # %.lr.ph1136.prol
                                        #   in Loop: Header=BB0_175 Depth=2
	leal	-1(%rsi), %ebp
	movss	L3psycho_anal.energy(,%rdi,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movss	L3psycho_anal.cw(,%rdi,4), %xmm2 # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	cvtps2pd	%xmm1, %xmm1
	addpd	%xmm1, %xmm0
	incq	%rdi
	cmpl	$2, %esi
	jne	.LBB0_180
	jmp	.LBB0_182
	.p2align	4, 0x90
.LBB0_178:                              #   in Loop: Header=BB0_175 Depth=2
	movl	%edi, %eax
	jmp	.LBB0_183
	.p2align	4, 0x90
.LBB0_179:                              #   in Loop: Header=BB0_175 Depth=2
	movl	%esi, %ebp
	cmpl	$2, %esi
	je	.LBB0_182
.LBB0_180:                              # %.lr.ph1136.preheader.new
                                        #   in Loop: Header=BB0_175 Depth=2
	shlq	$2, %rdi
	.p2align	4, 0x90
.LBB0_181:                              # %.lr.ph1136
                                        #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_175 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	L3psycho_anal.energy(%rdi), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movss	L3psycho_anal.cw(%rdi), %xmm2 # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	cvtps2pd	%xmm1, %xmm1
	addpd	%xmm0, %xmm1
	addl	$-2, %ebp
	movss	L3psycho_anal.energy+4(%rdi), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	L3psycho_anal.cw+4(%rdi), %xmm2 # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	unpcklps	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	cvtps2pd	%xmm0, %xmm0
	addpd	%xmm1, %xmm0
	addq	$8, %rdi
	cmpl	$1, %ebp
	jg	.LBB0_181
.LBB0_182:                              # %._crit_edge1137.loopexit
                                        #   in Loop: Header=BB0_175 Depth=2
	addl	%esi, %eax
.LBB0_183:                              # %._crit_edge1137
                                        #   in Loop: Header=BB0_175 Depth=2
	movlpd	%xmm0, L3psycho_anal.eb(,%rdx,8)
	movhpd	%xmm0, L3psycho_anal.cb(,%rdx,8)
	incq	%rdx
	cmpl	%ecx, %eax
	jl	.LBB0_175
	jmp	.LBB0_185
	.p2align	4, 0x90
.LBB0_184:                              #   in Loop: Header=BB0_124 Depth=1
	xorl	%eax, %eax
	xorl	%edx, %edx
.LBB0_185:                              # %.preheader1079
                                        #   in Loop: Header=BB0_124 Depth=1
	movslq	L3psycho_anal.npart_l_orig(%rip), %r8
	cmpl	%r8d, %edx
	jge	.LBB0_197
# BB#186:                               # %.lr.ph1157
                                        #   in Loop: Header=BB0_124 Depth=1
	movslq	%edx, %rdx
	.p2align	4, 0x90
.LBB0_187:                              #   Parent Loop BB0_124 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_190 Depth 3
                                        #       Child Loop BB0_194 Depth 3
	movslq	%eax, %rcx
	movss	L3psycho_anal.energy(,%rcx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	L3psycho_anal.numlines_l(,%rdx,4), %esi
	incl	%ecx
	cmpl	$2, %esi
	jl	.LBB0_196
# BB#188:                               # %.lr.ph1151.preheader
                                        #   in Loop: Header=BB0_187 Depth=2
	movslq	%ecx, %rbp
	leal	3(%rsi), %ecx
	leal	-2(%rsi), %ebx
	andl	$3, %ecx
	je	.LBB0_191
# BB#189:                               # %.lr.ph1151.prol.preheader
                                        #   in Loop: Header=BB0_187 Depth=2
	negl	%ecx
	movl	%esi, %edi
	.p2align	4, 0x90
.LBB0_190:                              # %.lr.ph1151.prol
                                        #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_187 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	decl	%edi
	movss	L3psycho_anal.energy(,%rbp,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm1, %xmm0
	incq	%rbp
	incl	%ecx
	jne	.LBB0_190
	jmp	.LBB0_192
.LBB0_191:                              #   in Loop: Header=BB0_187 Depth=2
	movl	%esi, %edi
.LBB0_192:                              # %.lr.ph1151.prol.loopexit
                                        #   in Loop: Header=BB0_187 Depth=2
	cmpl	$3, %ebx
	jb	.LBB0_195
# BB#193:                               # %.lr.ph1151.preheader.new
                                        #   in Loop: Header=BB0_187 Depth=2
	leaq	L3psycho_anal.energy+12(,%rbp,4), %rcx
	.p2align	4, 0x90
.LBB0_194:                              # %.lr.ph1151
                                        #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_187 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	-12(%rcx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	-8(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	movss	-4(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	addl	$-4, %edi
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	addq	$16, %rcx
	cmpl	$1, %edi
	jg	.LBB0_194
.LBB0_195:                              # %._crit_edge1152.loopexit
                                        #   in Loop: Header=BB0_187 Depth=2
	addl	%eax, %esi
	movl	%esi, %ecx
.LBB0_196:                              # %._crit_edge1152
                                        #   in Loop: Header=BB0_187 Depth=2
	movsd	%xmm0, L3psycho_anal.eb(,%rdx,8)
	mulsd	.LCPI0_18(%rip), %xmm0
	movsd	%xmm0, L3psycho_anal.cb(,%rdx,8)
	incq	%rdx
	cmpq	%r8, %rdx
	movl	%ecx, %eax
	jl	.LBB0_187
.LBB0_197:                              # %._crit_edge1158
                                        #   in Loop: Header=BB0_124 Depth=1
	movq	$0, L3psycho_anal.pe(,%r15,8)
	cmpl	$0, L3psycho_anal.npart_l(%rip)
	jle	.LBB0_212
# BB#198:                               # %.lr.ph1169
                                        #   in Loop: Header=BB0_124 Depth=1
	imulq	$504, %r15, %r13        # imm = 0x1F8
	leaq	L3psycho_anal.nb_1(%r13), %r14
	leaq	L3psycho_anal.nb_2(%r13), %r12
	movl	$L3psycho_anal.s3_l, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_199:                              #   Parent Loop BB0_124 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_201 Depth 3
	movslq	L3psycho_anal.s3ind(,%rbx,8), %rax
	movslq	L3psycho_anal.s3ind+4(,%rbx,8), %rcx
	xorps	%xmm2, %xmm2
	cmpl	%ecx, %eax
	xorps	%xmm0, %xmm0
	jg	.LBB0_209
# BB#200:                               # %.lr.ph1163
                                        #   in Loop: Header=BB0_199 Depth=2
	leaq	(%rbp,%rax,8), %rdx
	decq	%rax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_201:                              #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_199 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movsd	L3psycho_anal.cb+8(,%rax,8), %xmm2 # xmm2 = mem[0],zero
	movhpd	L3psycho_anal.eb+8(,%rax,8), %xmm2 # xmm2 = xmm2[0],mem[0]
	mulpd	%xmm1, %xmm2
	addpd	%xmm2, %xmm0
	incq	%rax
	addq	$8, %rdx
	cmpq	%rcx, %rax
	jl	.LBB0_201
# BB#202:                               # %._crit_edge1164
                                        #   in Loop: Header=BB0_199 Depth=2
	movapd	%xmm0, %xmm2
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	ucomisd	.LCPI0_34, %xmm2
	jne	.LBB0_203
	jnp	.LBB0_206
.LBB0_203:                              #   in Loop: Header=BB0_199 Depth=2
	divsd	%xmm2, %xmm0
	movsd	.LCPI0_20(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jae	.LBB0_207
# BB#204:                               #   in Loop: Header=BB0_199 Depth=2
	ucomisd	.LCPI0_21(%rip), %xmm0
	jbe	.LBB0_208
# BB#205:                               #   in Loop: Header=BB0_199 Depth=2
	movsd	.LCPI0_6(%rip), %xmm0   # xmm0 = mem[0],zero
	jmp	.LBB0_209
.LBB0_206:                              #   in Loop: Header=BB0_199 Depth=2
	movaps	%xmm2, %xmm0
	jmp	.LBB0_209
.LBB0_207:                              #   in Loop: Header=BB0_199 Depth=2
	movsd	.LCPI0_19(%rip), %xmm0  # xmm0 = mem[0],zero
	jmp	.LBB0_209
.LBB0_208:                              #   in Loop: Header=BB0_199 Depth=2
	movaps	%xmm2, 16(%rsp)         # 16-byte Spill
	callq	log
	mulsd	.LCPI0_22(%rip), %xmm0
	addsd	.LCPI0_23(%rip), %xmm0
	callq	exp
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	.p2align	4, 0x90
.LBB0_209:                              # %._crit_edge1164.thread
                                        #   in Loop: Header=BB0_199 Depth=2
	movsd	L3psycho_anal.minval(,%rbx,8), %xmm1 # xmm1 = mem[0],zero
	minsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm1
	movsd	L3psycho_anal.nb_1(%r13,%rbx,8), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm0
	addsd	%xmm0, %xmm0
	movsd	L3psycho_anal.nb_2(%r13,%rbx,8), %xmm3 # xmm3 = mem[0],zero
	mulsd	.LCPI0_24(%rip), %xmm3
	xorl	%eax, %eax
	ucomisd	%xmm0, %xmm3
	seta	%al
	movq	%r12, %rcx
	cmovaq	%r14, %rcx
	movsd	(%rcx,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	.LCPI0_25(,%rax,8), %xmm0
	movsd	%xmm1, L3psycho_anal.nb_1(%r13,%rbx,8)
	minsd	%xmm0, %xmm1
	movsd	L3psycho_anal.qthr_l(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	maxsd	%xmm1, %xmm0
	movsd	%xmm0, L3psycho_anal.thr(,%rbx,8)
	movsd	%xmm2, L3psycho_anal.nb_2(%r13,%rbx,8)
	movsd	L3psycho_anal.eb(,%rbx,8), %xmm1 # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB0_211
# BB#210:                               #   in Loop: Header=BB0_199 Depth=2
	movl	L3psycho_anal.numlines_l(,%rbx,4), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movsd	%xmm2, 16(%rsp)         # 8-byte Spill
	divsd	%xmm1, %xmm0
	callq	log
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	L3psycho_anal.pe(,%r15,8), %xmm1 # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, L3psycho_anal.pe(,%r15,8)
.LBB0_211:                              #   in Loop: Header=BB0_199 Depth=2
	incq	%rbx
	movslq	L3psycho_anal.npart_l(%rip), %rax
	addq	$512, %rbp              # imm = 0x200
	cmpq	%rax, %rbx
	jl	.LBB0_199
.LBB0_212:                              # %._crit_edge1170
                                        #   in Loop: Header=BB0_124 Depth=1
	cmpq	$2, %r15
	jge	.LBB0_225
# BB#213:                               #   in Loop: Header=BB0_124 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 160(%rax)
	je	.LBB0_215
# BB#214:                               #   in Loop: Header=BB0_124 Depth=1
	movl	$1, 36(%rsp,%r15,4)
	jmp	.LBB0_225
.LBB0_215:                              #   in Loop: Header=BB0_124 Depth=1
	movsd	L3psycho_anal.pe(,%r15,8), %xmm0 # xmm0 = mem[0],zero
	ucomisd	.LCPI0_26(%rip), %xmm0
	ja	.LBB0_224
# BB#217:                               # %.preheader1078.preheader
                                        #   in Loop: Header=BB0_124 Depth=1
	xorps	%xmm2, %xmm2
	movq	$-256, %rax
	xorps	%xmm3, %xmm3
	xorps	%xmm1, %xmm1
	jmp	.LBB0_219
	.p2align	4, 0x90
.LBB0_218:                              # %.preheader1078.1
                                        #   in Loop: Header=BB0_219 Depth=2
	addss	L3psycho_anal.energy_s+516(%rax), %xmm1
	addss	L3psycho_anal.energy_s+1032(%rax), %xmm3
	addss	L3psycho_anal.energy_s+1548(%rax), %xmm2
	addq	$8, %rax
.LBB0_219:                              # %.preheader1078
                                        #   Parent Loop BB0_124 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addss	L3psycho_anal.energy_s+512(%rax), %xmm1
	addss	L3psycho_anal.energy_s+1028(%rax), %xmm3
	addss	L3psycho_anal.energy_s+1544(%rax), %xmm2
	testq	%rax, %rax
	jne	.LBB0_218
# BB#220:                               #   in Loop: Header=BB0_124 Depth=1
	movaps	%xmm1, %xmm4
	minss	%xmm3, %xmm4
	minss	%xmm2, %xmm4
	maxss	%xmm3, %xmm1
	maxss	%xmm2, %xmm1
	movl	$1, 36(%rsp,%r15,4)
	movaps	%xmm4, %xmm2
	mulss	.LCPI0_27(%rip), %xmm2
	ucomiss	%xmm2, %xmm1
	ja	.LBB0_224
# BB#222:                               #   in Loop: Header=BB0_124 Depth=1
	mulss	.LCPI0_28(%rip), %xmm4
	ucomiss	%xmm4, %xmm1
	jbe	.LBB0_225
# BB#223:                               #   in Loop: Header=BB0_124 Depth=1
	ucomisd	.LCPI0_0(%rip), %xmm0
	jbe	.LBB0_225
.LBB0_224:                              #   in Loop: Header=BB0_124 Depth=1
	movl	$0, 36(%rsp,%r15,4)
	.p2align	4, 0x90
.LBB0_225:                              # %.preheader1077.preheader
                                        #   in Loop: Header=BB0_124 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_226:                              # %.preheader1077
                                        #   Parent Loop BB0_124 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_229 Depth 3
                                        #       Child Loop BB0_231 Depth 3
	movslq	L3psycho_anal.bu_l(,%rcx,4), %rax
	movslq	L3psycho_anal.bo_l(,%rcx,4), %rdx
	movsd	L3psycho_anal.w1_l(,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movsd	L3psycho_anal.eb(,%rax,8), %xmm1 # xmm1 = mem[0],zero
	movhpd	L3psycho_anal.thr(,%rax,8), %xmm1 # xmm1 = xmm1[0],mem[0]
	mulpd	%xmm0, %xmm1
	movsd	L3psycho_anal.w2_l(,%rcx,8), %xmm2 # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movsd	L3psycho_anal.eb(,%rdx,8), %xmm0 # xmm0 = mem[0],zero
	movhpd	L3psycho_anal.thr(,%rdx,8), %xmm0 # xmm0 = xmm0[0],mem[0]
	mulpd	%xmm2, %xmm0
	addpd	%xmm1, %xmm0
	incq	%rax
	cmpl	%edx, %eax
	jge	.LBB0_232
# BB#227:                               # %.lr.ph1180
                                        #   in Loop: Header=BB0_226 Depth=2
	movl	%edx, %edi
	subl	%eax, %edi
	leaq	-1(%rdx), %rsi
	subq	%rax, %rsi
	andq	$3, %rdi
	je	.LBB0_230
# BB#228:                               # %.prol.preheader
                                        #   in Loop: Header=BB0_226 Depth=2
	negq	%rdi
	.p2align	4, 0x90
.LBB0_229:                              #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_226 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	L3psycho_anal.eb(,%rax,8), %xmm1 # xmm1 = mem[0],zero
	movhpd	L3psycho_anal.thr(,%rax,8), %xmm1 # xmm1 = xmm1[0],mem[0]
	addpd	%xmm1, %xmm0
	incq	%rax
	incq	%rdi
	jne	.LBB0_229
.LBB0_230:                              # %.prol.loopexit
                                        #   in Loop: Header=BB0_226 Depth=2
	cmpq	$3, %rsi
	jb	.LBB0_232
	.p2align	4, 0x90
.LBB0_231:                              #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_226 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	L3psycho_anal.eb(,%rax,8), %xmm1 # xmm1 = mem[0],zero
	movhpd	L3psycho_anal.thr(,%rax,8), %xmm1 # xmm1 = xmm1[0],mem[0]
	addpd	%xmm0, %xmm1
	movsd	L3psycho_anal.eb+8(,%rax,8), %xmm0 # xmm0 = mem[0],zero
	movhpd	L3psycho_anal.thr+8(,%rax,8), %xmm0 # xmm0 = xmm0[0],mem[0]
	addpd	%xmm1, %xmm0
	movsd	L3psycho_anal.eb+16(,%rax,8), %xmm1 # xmm1 = mem[0],zero
	movhpd	L3psycho_anal.thr+16(,%rax,8), %xmm1 # xmm1 = xmm1[0],mem[0]
	addpd	%xmm0, %xmm1
	movsd	L3psycho_anal.eb+24(,%rax,8), %xmm0 # xmm0 = mem[0],zero
	movhpd	L3psycho_anal.thr+24(,%rax,8), %xmm0 # xmm0 = xmm0[0],mem[0]
	addpd	%xmm1, %xmm0
	addq	$4, %rax
	cmpq	%rdx, %rax
	jl	.LBB0_231
.LBB0_232:                              # %._crit_edge1181
                                        #   in Loop: Header=BB0_226 Depth=2
	imulq	$488, %r15, %rax        # imm = 0x1E8
	movlpd	%xmm0, L3psycho_anal.en(%rax,%rcx,8)
	movhpd	%xmm0, L3psycho_anal.thm(%rax,%rcx,8)
	incq	%rcx
	cmpq	$21, %rcx
	jne	.LBB0_226
# BB#233:                               # %.preheader1076
                                        #   in Loop: Header=BB0_124 Depth=1
	movl	L3psycho_anal.npart_s_orig(%rip), %r10d
	testl	%r10d, %r10d
	movl	L3psycho_anal.npart_s(%rip), %r8d
	jle	.LBB0_255
# BB#234:                               # %.preheader1074.us.preheader
                                        #   in Loop: Header=BB0_124 Depth=1
	movl	$L3psycho_anal.energy_s, %r9d
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_235:                              # %.preheader1074.us
                                        #   Parent Loop BB0_124 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_236 Depth 3
                                        #         Child Loop BB0_238 Depth 4
                                        #       Child Loop BB0_242 Depth 3
                                        #         Child Loop BB0_244 Depth 4
                                        #       Child Loop BB0_247 Depth 3
                                        #         Child Loop BB0_250 Depth 4
                                        #         Child Loop BB0_252 Depth 4
	xorl	%ecx, %ecx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_236:                              #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_235 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_238 Depth 4
	movslq	%ebp, %rbp
	imulq	$516, %rdi, %rdx        # imm = 0x204
	movss	L3psycho_anal.energy_s(%rdx,%rbp,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movl	L3psycho_anal.numlines_s(,%rcx,4), %esi
	incl	%ebp
	testl	%esi, %esi
	jle	.LBB0_239
# BB#237:                               # %.lr.ph1190.us.preheader
                                        #   in Loop: Header=BB0_236 Depth=3
	movslq	%ebp, %rdx
	leaq	(%r9,%rdx,4), %rdx
	incl	%esi
	.p2align	4, 0x90
.LBB0_238:                              # %.lr.ph1190.us
                                        #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_235 Depth=2
                                        #       Parent Loop BB0_236 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	addss	(%rdx), %xmm0
	addq	$4, %rdx
	incl	%ebp
	decl	%esi
	cmpl	$1, %esi
	jg	.LBB0_238
.LBB0_239:                              # %._crit_edge1191.us
                                        #   in Loop: Header=BB0_236 Depth=3
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, L3psycho_anal.eb(,%rcx,8)
	incq	%rcx
	cmpq	%r10, %rcx
	jne	.LBB0_236
# BB#240:                               # %..preheader1073_crit_edge.us
                                        #   in Loop: Header=BB0_235 Depth=2
	testl	%r8d, %r8d
	jle	.LBB0_246
# BB#241:                               # %.lr.ph1204.us.preheader
                                        #   in Loop: Header=BB0_235 Depth=2
	movl	$L3psycho_anal.s3_s, %ecx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_242:                              # %.lr.ph1204.us
                                        #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_235 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_244 Depth 4
	movslq	L3psycho_anal.s3ind_s(,%rbp,8), %rsi
	movslq	L3psycho_anal.s3ind_s+4(,%rbp,8), %rdx
	xorpd	%xmm0, %xmm0
	cmpl	%edx, %esi
	jg	.LBB0_245
# BB#243:                               # %.lr.ph1200.us
                                        #   in Loop: Header=BB0_242 Depth=3
	leaq	(%rcx,%rsi,8), %rbx
	decq	%rsi
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_244:                              #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_235 Depth=2
                                        #       Parent Loop BB0_242 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	mulsd	L3psycho_anal.eb+8(,%rsi,8), %xmm1
	addsd	%xmm1, %xmm0
	incq	%rsi
	addq	$8, %rbx
	cmpq	%rdx, %rsi
	jl	.LBB0_244
.LBB0_245:                              # %._crit_edge1201.us
                                        #   in Loop: Header=BB0_242 Depth=3
	movsd	L3psycho_anal.qthr_s(,%rbp,8), %xmm1 # xmm1 = mem[0],zero
	maxsd	%xmm0, %xmm1
	movsd	%xmm1, L3psycho_anal.thr(,%rbp,8)
	incq	%rbp
	addq	$512, %rcx              # imm = 0x200
	cmpq	%r8, %rbp
	jne	.LBB0_242
.LBB0_246:                              # %.preheader1072.us.preheader
                                        #   in Loop: Header=BB0_235 Depth=2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_247:                              # %.preheader1072.us
                                        #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_235 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_250 Depth 4
                                        #         Child Loop BB0_252 Depth 4
	movslq	L3psycho_anal.bu_s(,%rbp,4), %rbx
	movslq	L3psycho_anal.bo_s(,%rbp,4), %rdx
	movsd	L3psycho_anal.w1_s(,%rbp,8), %xmm0 # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movsd	L3psycho_anal.eb(,%rbx,8), %xmm1 # xmm1 = mem[0],zero
	movhpd	L3psycho_anal.thr(,%rbx,8), %xmm1 # xmm1 = xmm1[0],mem[0]
	mulpd	%xmm0, %xmm1
	movsd	L3psycho_anal.w2_s(,%rbp,8), %xmm2 # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movsd	L3psycho_anal.eb(,%rdx,8), %xmm0 # xmm0 = mem[0],zero
	movhpd	L3psycho_anal.thr(,%rdx,8), %xmm0 # xmm0 = xmm0[0],mem[0]
	mulpd	%xmm2, %xmm0
	addpd	%xmm1, %xmm0
	incq	%rbx
	cmpl	%edx, %ebx
	jge	.LBB0_253
# BB#248:                               # %.lr.ph1210.us
                                        #   in Loop: Header=BB0_247 Depth=3
	movl	%edx, %ecx
	subl	%ebx, %ecx
	leaq	-1(%rdx), %rsi
	subq	%rbx, %rsi
	andq	$3, %rcx
	je	.LBB0_251
# BB#249:                               # %.prol.preheader1750
                                        #   in Loop: Header=BB0_247 Depth=3
	negq	%rcx
	.p2align	4, 0x90
.LBB0_250:                              #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_235 Depth=2
                                        #       Parent Loop BB0_247 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	L3psycho_anal.eb(,%rbx,8), %xmm1 # xmm1 = mem[0],zero
	movhpd	L3psycho_anal.thr(,%rbx,8), %xmm1 # xmm1 = xmm1[0],mem[0]
	addpd	%xmm1, %xmm0
	incq	%rbx
	incq	%rcx
	jne	.LBB0_250
.LBB0_251:                              # %.prol.loopexit1751
                                        #   in Loop: Header=BB0_247 Depth=3
	cmpq	$3, %rsi
	jb	.LBB0_253
	.p2align	4, 0x90
.LBB0_252:                              #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_235 Depth=2
                                        #       Parent Loop BB0_247 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	L3psycho_anal.eb(,%rbx,8), %xmm1 # xmm1 = mem[0],zero
	movhpd	L3psycho_anal.thr(,%rbx,8), %xmm1 # xmm1 = xmm1[0],mem[0]
	addpd	%xmm0, %xmm1
	movsd	L3psycho_anal.eb+8(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	movhpd	L3psycho_anal.thr+8(,%rbx,8), %xmm0 # xmm0 = xmm0[0],mem[0]
	addpd	%xmm1, %xmm0
	movsd	L3psycho_anal.eb+16(,%rbx,8), %xmm1 # xmm1 = mem[0],zero
	movhpd	L3psycho_anal.thr+16(,%rbx,8), %xmm1 # xmm1 = xmm1[0],mem[0]
	addpd	%xmm0, %xmm1
	movsd	L3psycho_anal.eb+24(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	movhpd	L3psycho_anal.thr+24(,%rbx,8), %xmm0 # xmm0 = xmm0[0],mem[0]
	addpd	%xmm1, %xmm0
	addq	$4, %rbx
	cmpq	%rdx, %rbx
	jl	.LBB0_252
.LBB0_253:                              # %._crit_edge1211.us
                                        #   in Loop: Header=BB0_247 Depth=3
	leaq	(%rbp,%rbp,2), %rcx
	leaq	(%rax,%rcx,8), %rcx
	movlpd	%xmm0, L3psycho_anal.en+176(%rcx,%rdi,8)
	movhpd	%xmm0, L3psycho_anal.thm+176(%rcx,%rdi,8)
	incq	%rbp
	cmpq	$12, %rbp
	jne	.LBB0_247
# BB#254:                               #   in Loop: Header=BB0_235 Depth=2
	incq	%rdi
	addq	$516, %r9               # imm = 0x204
	cmpq	$3, %rdi
	jne	.LBB0_235
	jmp	.LBB0_271
	.p2align	4, 0x90
.LBB0_255:                              # %.preheader1074.preheader
                                        #   in Loop: Header=BB0_124 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_256:                              # %.preheader1074
                                        #   Parent Loop BB0_124 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_258 Depth 3
                                        #         Child Loop BB0_260 Depth 4
                                        #       Child Loop BB0_263 Depth 3
                                        #         Child Loop BB0_266 Depth 4
                                        #         Child Loop BB0_268 Depth 4
	testl	%r8d, %r8d
	jle	.LBB0_262
# BB#257:                               # %.lr.ph1204.preheader
                                        #   in Loop: Header=BB0_256 Depth=2
	movl	$L3psycho_anal.s3_s, %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_258:                              # %.lr.ph1204
                                        #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_256 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_260 Depth 4
	movslq	L3psycho_anal.s3ind_s(,%rsi,8), %rdi
	movslq	L3psycho_anal.s3ind_s+4(,%rsi,8), %rbp
	xorpd	%xmm0, %xmm0
	cmpl	%ebp, %edi
	jg	.LBB0_261
# BB#259:                               # %.lr.ph1200
                                        #   in Loop: Header=BB0_258 Depth=3
	leaq	(%rcx,%rdi,8), %rbx
	decq	%rdi
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_260:                              #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_256 Depth=2
                                        #       Parent Loop BB0_258 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	mulsd	L3psycho_anal.eb+8(,%rdi,8), %xmm1
	addsd	%xmm1, %xmm0
	incq	%rdi
	addq	$8, %rbx
	cmpq	%rbp, %rdi
	jl	.LBB0_260
.LBB0_261:                              # %._crit_edge1201
                                        #   in Loop: Header=BB0_258 Depth=3
	movsd	L3psycho_anal.qthr_s(,%rsi,8), %xmm1 # xmm1 = mem[0],zero
	maxsd	%xmm0, %xmm1
	movsd	%xmm1, L3psycho_anal.thr(,%rsi,8)
	incq	%rsi
	addq	$512, %rcx              # imm = 0x200
	cmpq	%r8, %rsi
	jne	.LBB0_258
.LBB0_262:                              # %.preheader1072.preheader
                                        #   in Loop: Header=BB0_256 Depth=2
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_263:                              # %.preheader1072
                                        #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_256 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_266 Depth 4
                                        #         Child Loop BB0_268 Depth 4
	movslq	L3psycho_anal.bu_s(,%rsi,4), %rdi
	movslq	L3psycho_anal.bo_s(,%rsi,4), %rbx
	movsd	L3psycho_anal.w1_s(,%rsi,8), %xmm0 # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movsd	L3psycho_anal.eb(,%rdi,8), %xmm1 # xmm1 = mem[0],zero
	movhpd	L3psycho_anal.thr(,%rdi,8), %xmm1 # xmm1 = xmm1[0],mem[0]
	mulpd	%xmm0, %xmm1
	movsd	L3psycho_anal.w2_s(,%rsi,8), %xmm2 # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movsd	L3psycho_anal.eb(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	movhpd	L3psycho_anal.thr(,%rbx,8), %xmm0 # xmm0 = xmm0[0],mem[0]
	mulpd	%xmm2, %xmm0
	addpd	%xmm1, %xmm0
	incq	%rdi
	cmpl	%ebx, %edi
	jge	.LBB0_269
# BB#264:                               # %.lr.ph1210
                                        #   in Loop: Header=BB0_263 Depth=3
	movl	%ebx, %ebp
	subl	%edi, %ebp
	leaq	-1(%rbx), %rcx
	subq	%rdi, %rcx
	andq	$3, %rbp
	je	.LBB0_267
# BB#265:                               # %.prol.preheader1744
                                        #   in Loop: Header=BB0_263 Depth=3
	negq	%rbp
	.p2align	4, 0x90
.LBB0_266:                              #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_256 Depth=2
                                        #       Parent Loop BB0_263 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	L3psycho_anal.eb(,%rdi,8), %xmm1 # xmm1 = mem[0],zero
	movhpd	L3psycho_anal.thr(,%rdi,8), %xmm1 # xmm1 = xmm1[0],mem[0]
	addpd	%xmm1, %xmm0
	incq	%rdi
	incq	%rbp
	jne	.LBB0_266
.LBB0_267:                              # %.prol.loopexit1745
                                        #   in Loop: Header=BB0_263 Depth=3
	cmpq	$3, %rcx
	jb	.LBB0_269
	.p2align	4, 0x90
.LBB0_268:                              #   Parent Loop BB0_124 Depth=1
                                        #     Parent Loop BB0_256 Depth=2
                                        #       Parent Loop BB0_263 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	L3psycho_anal.eb(,%rdi,8), %xmm1 # xmm1 = mem[0],zero
	movhpd	L3psycho_anal.thr(,%rdi,8), %xmm1 # xmm1 = xmm1[0],mem[0]
	addpd	%xmm0, %xmm1
	movsd	L3psycho_anal.eb+8(,%rdi,8), %xmm0 # xmm0 = mem[0],zero
	movhpd	L3psycho_anal.thr+8(,%rdi,8), %xmm0 # xmm0 = xmm0[0],mem[0]
	addpd	%xmm1, %xmm0
	movsd	L3psycho_anal.eb+16(,%rdi,8), %xmm1 # xmm1 = mem[0],zero
	movhpd	L3psycho_anal.thr+16(,%rdi,8), %xmm1 # xmm1 = xmm1[0],mem[0]
	addpd	%xmm0, %xmm1
	movsd	L3psycho_anal.eb+24(,%rdi,8), %xmm0 # xmm0 = mem[0],zero
	movhpd	L3psycho_anal.thr+24(,%rdi,8), %xmm0 # xmm0 = xmm0[0],mem[0]
	addpd	%xmm1, %xmm0
	addq	$4, %rdi
	cmpq	%rbx, %rdi
	jl	.LBB0_268
.LBB0_269:                              # %._crit_edge1211
                                        #   in Loop: Header=BB0_263 Depth=3
	leaq	(%rsi,%rsi,2), %rcx
	leaq	(%rax,%rcx,8), %rcx
	movlpd	%xmm0, L3psycho_anal.en+176(%rcx,%rdx,8)
	movhpd	%xmm0, L3psycho_anal.thm+176(%rcx,%rdx,8)
	incq	%rsi
	cmpq	$12, %rsi
	jne	.LBB0_263
# BB#270:                               #   in Loop: Header=BB0_256 Depth=2
	incq	%rdx
	cmpq	$3, %rdx
	jne	.LBB0_256
.LBB0_271:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB0_124 Depth=1
	incq	%r15
	addq	$4104, 72(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x1008
	cmpq	168(%rsp), %r15         # 8-byte Folded Reload
	jne	.LBB0_124
# BB#272:                               # %._crit_edge1219
	cmpl	$4, 84(%rsp)            # 4-byte Folded Reload
	sete	%al
	movq	%rax, 72(%rsp)          # 8-byte Spill
	jne	.LBB0_289
# BB#273:                               # %.preheader1071.preheader
	movq	$-168, %rax
	movsd	.LCPI0_29(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB0_274:                              # %.preheader1071
                                        # =>This Inner Loop Header: Depth=1
	movsd	L3psycho_anal.thm+168(%rax), %xmm1 # xmm1 = mem[0],zero
	movsd	L3psycho_anal.thm+656(%rax), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	mulsd	%xmm0, %xmm3
	ucomisd	%xmm2, %xmm3
	jb	.LBB0_277
# BB#275:                               # %.preheader1071
                                        #   in Loop: Header=BB0_274 Depth=1
	mulsd	%xmm0, %xmm2
	ucomisd	%xmm1, %xmm2
	jb	.LBB0_277
# BB#276:                               #   in Loop: Header=BB0_274 Depth=1
	movsd	L3psycho_anal.mld_l+168(%rax), %xmm1 # xmm1 = mem[0],zero
	movsd	L3psycho_anal.en+1632(%rax), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	L3psycho_anal.thm+1144(%rax), %xmm3 # xmm3 = mem[0],zero
	movsd	L3psycho_anal.thm+1632(%rax), %xmm4 # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm5
	minsd	%xmm2, %xmm5
	movapd	%xmm3, %xmm2
	maxsd	%xmm5, %xmm2
	mulsd	L3psycho_anal.en+1144(%rax), %xmm1
	minsd	%xmm1, %xmm3
	maxsd	%xmm3, %xmm4
	movsd	%xmm2, L3psycho_anal.thm+1144(%rax)
	movsd	%xmm4, L3psycho_anal.thm+1632(%rax)
.LBB0_277:                              #   in Loop: Header=BB0_274 Depth=1
	addq	$8, %rax
	jne	.LBB0_274
# BB#278:                               # %.preheader1069.preheader
	movq	$-96, %rax
	movq	64(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_279:                              # %.preheader1069
                                        # =>This Inner Loop Header: Depth=1
	movsd	L3psycho_anal.thm+464(%rax,%rax,2), %xmm1 # xmm1 = mem[0],zero
	movsd	L3psycho_anal.thm+952(%rax,%rax,2), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	mulsd	%xmm0, %xmm3
	ucomisd	%xmm2, %xmm3
	jb	.LBB0_282
# BB#280:                               # %.preheader1069
                                        #   in Loop: Header=BB0_279 Depth=1
	mulsd	%xmm0, %xmm2
	ucomisd	%xmm1, %xmm2
	jb	.LBB0_282
# BB#281:                               #   in Loop: Header=BB0_279 Depth=1
	movsd	L3psycho_anal.mld_s+96(%rax), %xmm1 # xmm1 = mem[0],zero
	movsd	L3psycho_anal.en+1928(%rax,%rax,2), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	L3psycho_anal.thm+1440(%rax,%rax,2), %xmm3 # xmm3 = mem[0],zero
	movsd	L3psycho_anal.thm+1928(%rax,%rax,2), %xmm4 # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm5
	minsd	%xmm2, %xmm5
	movapd	%xmm3, %xmm2
	maxsd	%xmm5, %xmm2
	mulsd	L3psycho_anal.en+1440(%rax,%rax,2), %xmm1
	minsd	%xmm1, %xmm3
	maxsd	%xmm3, %xmm4
	movsd	%xmm2, L3psycho_anal.thm+1440(%rax,%rax,2)
	movsd	%xmm4, L3psycho_anal.thm+1928(%rax,%rax,2)
.LBB0_282:                              #   in Loop: Header=BB0_279 Depth=1
	movsd	L3psycho_anal.thm+472(%rax,%rax,2), %xmm1 # xmm1 = mem[0],zero
	movsd	L3psycho_anal.thm+960(%rax,%rax,2), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	mulsd	%xmm0, %xmm3
	ucomisd	%xmm2, %xmm3
	jb	.LBB0_285
# BB#283:                               #   in Loop: Header=BB0_279 Depth=1
	mulsd	%xmm0, %xmm2
	ucomisd	%xmm1, %xmm2
	jb	.LBB0_285
# BB#284:                               #   in Loop: Header=BB0_279 Depth=1
	movsd	L3psycho_anal.mld_s+96(%rax), %xmm1 # xmm1 = mem[0],zero
	movsd	L3psycho_anal.en+1936(%rax,%rax,2), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	L3psycho_anal.thm+1448(%rax,%rax,2), %xmm3 # xmm3 = mem[0],zero
	movsd	L3psycho_anal.thm+1936(%rax,%rax,2), %xmm4 # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm5
	minsd	%xmm2, %xmm5
	movapd	%xmm3, %xmm2
	maxsd	%xmm5, %xmm2
	mulsd	L3psycho_anal.en+1448(%rax,%rax,2), %xmm1
	minsd	%xmm1, %xmm3
	maxsd	%xmm3, %xmm4
	movsd	%xmm2, L3psycho_anal.thm+1448(%rax,%rax,2)
	movsd	%xmm4, L3psycho_anal.thm+1936(%rax,%rax,2)
.LBB0_285:                              #   in Loop: Header=BB0_279 Depth=1
	movsd	L3psycho_anal.thm+480(%rax,%rax,2), %xmm1 # xmm1 = mem[0],zero
	movsd	L3psycho_anal.thm+968(%rax,%rax,2), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	mulsd	%xmm0, %xmm3
	ucomisd	%xmm2, %xmm3
	jb	.LBB0_288
# BB#286:                               #   in Loop: Header=BB0_279 Depth=1
	mulsd	%xmm0, %xmm2
	ucomisd	%xmm1, %xmm2
	jb	.LBB0_288
# BB#287:                               #   in Loop: Header=BB0_279 Depth=1
	movsd	L3psycho_anal.mld_s+96(%rax), %xmm1 # xmm1 = mem[0],zero
	movsd	L3psycho_anal.en+1944(%rax,%rax,2), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	L3psycho_anal.thm+1456(%rax,%rax,2), %xmm3 # xmm3 = mem[0],zero
	movsd	L3psycho_anal.thm+1944(%rax,%rax,2), %xmm4 # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm5
	minsd	%xmm2, %xmm5
	movapd	%xmm3, %xmm2
	maxsd	%xmm5, %xmm2
	mulsd	L3psycho_anal.en+1456(%rax,%rax,2), %xmm1
	minsd	%xmm1, %xmm3
	maxsd	%xmm3, %xmm4
	movsd	%xmm2, L3psycho_anal.thm+1456(%rax,%rax,2)
	movsd	%xmm4, L3psycho_anal.thm+1944(%rax,%rax,2)
.LBB0_288:                              #   in Loop: Header=BB0_279 Depth=1
	addq	$8, %rax
	jne	.LBB0_279
	jmp	.LBB0_290
.LBB0_289:
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	64(%rsp), %r14          # 8-byte Reload
.LBB0_290:                              # %.loopexit
	xorps	%xmm3, %xmm3
	cmpl	$1, 36(%r14)
	xorpd	%xmm1, %xmm1
	jne	.LBB0_308
# BB#291:                               # %.preheader1068.preheader
	xorps	%xmm3, %xmm3
	xorl	%ebx, %ebx
	movl	$L3psycho_anal.thm, %ebp
	movsd	.LCPI0_0(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI0_30(%rip), %xmm6  # xmm6 = mem[0],zero
	movsd	.LCPI0_6(%rip), %xmm5   # xmm5 = mem[0],zero
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_292:                              # %.preheader1068
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	movsd	L3psycho_anal.thm+40(%rbx), %xmm0 # xmm0 = mem[0],zero
	movsd	L3psycho_anal.thm+528(%rbx), %xmm1 # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movl	$L3psycho_anal.thm+488, %eax
	cmovaq	%rbp, %rax
	movsd	40(%rax,%rbx), %xmm2    # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	movl	$L3psycho_anal.thm+488, %eax
	cmovaq	%rbp, %rax
	movsd	40(%rax,%rbx), %xmm0    # xmm0 = mem[0],zero
	movapd	%xmm2, %xmm1
	mulsd	%xmm4, %xmm1
	ucomisd	%xmm1, %xmm0
	movapd	%xmm6, %xmm1
	jae	.LBB0_294
# BB#293:                               #   in Loop: Header=BB0_292 Depth=1
	divsd	%xmm2, %xmm0
	movsd	%xmm3, 16(%rsp)         # 8-byte Spill
	callq	log10
	movsd	.LCPI0_30(%rip), %xmm6  # xmm6 = mem[0],zero
	movsd	16(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	.LCPI0_6(%rip), %xmm5   # xmm5 = mem[0],zero
	movsd	.LCPI0_0(%rip), %xmm4   # xmm4 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB0_294:                              #   in Loop: Header=BB0_292 Depth=1
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	addsd	%xmm5, %xmm3
	addq	$8, %rbx
	cmpq	$128, %rbx
	jne	.LBB0_292
# BB#295:                               # %.preheader1067
	divsd	%xmm3, %xmm0
	mulsd	.LCPI0_31(%rip), %xmm0
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	xorpd	%xmm6, %xmm6
	movq	$-216, %rbx
	movsd	.LCPI0_30(%rip), %xmm7  # xmm7 = mem[0],zero
	xorpd	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB0_296:                              # =>This Inner Loop Header: Depth=1
	leaq	L3psycho_anal.thm+464(%rbx), %rax
	movsd	L3psycho_anal.thm+464(%rbx), %xmm0 # xmm0 = mem[0],zero
	leaq	L3psycho_anal.thm+952(%rbx), %rcx
	movsd	L3psycho_anal.thm+952(%rbx), %xmm1 # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movq	%rcx, %rdx
	cmovaq	%rax, %rdx
	movsd	(%rdx), %xmm2           # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	cmovaq	%rax, %rcx
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	movapd	%xmm2, %xmm1
	mulsd	%xmm4, %xmm1
	ucomisd	%xmm1, %xmm0
	movapd	%xmm7, %xmm1
	jae	.LBB0_298
# BB#297:                               #   in Loop: Header=BB0_296 Depth=1
	divsd	%xmm2, %xmm0
	movsd	%xmm3, 16(%rsp)         # 8-byte Spill
	movsd	%xmm6, 8(%rsp)          # 8-byte Spill
	callq	log10
	movsd	.LCPI0_30(%rip), %xmm7  # xmm7 = mem[0],zero
	movsd	8(%rsp), %xmm6          # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movsd	.LCPI0_6(%rip), %xmm5   # xmm5 = mem[0],zero
	movsd	.LCPI0_0(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	16(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB0_298:                              #   in Loop: Header=BB0_296 Depth=1
	addsd	%xmm1, %xmm3
	addsd	%xmm5, %xmm6
	addq	$24, %rbx
	jne	.LBB0_296
# BB#299:                               # %.preheader1067.11309.preheader
	movq	$-216, %rbx
	movsd	.LCPI0_30(%rip), %xmm7  # xmm7 = mem[0],zero
	.p2align	4, 0x90
.LBB0_300:                              # %.preheader1067.11309
                                        # =>This Inner Loop Header: Depth=1
	leaq	L3psycho_anal.thm+464(%rbx), %rax
	movsd	L3psycho_anal.thm+472(%rbx), %xmm0 # xmm0 = mem[0],zero
	leaq	L3psycho_anal.thm+952(%rbx), %rcx
	movsd	L3psycho_anal.thm+960(%rbx), %xmm1 # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movq	%rcx, %rdx
	cmovaq	%rax, %rdx
	movsd	8(%rdx), %xmm2          # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	cmovaq	%rax, %rcx
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	movapd	%xmm2, %xmm1
	mulsd	%xmm4, %xmm1
	ucomisd	%xmm1, %xmm0
	movapd	%xmm7, %xmm1
	jae	.LBB0_302
# BB#301:                               #   in Loop: Header=BB0_300 Depth=1
	divsd	%xmm2, %xmm0
	movsd	%xmm3, 16(%rsp)         # 8-byte Spill
	movsd	%xmm6, 8(%rsp)          # 8-byte Spill
	callq	log10
	movsd	.LCPI0_30(%rip), %xmm7  # xmm7 = mem[0],zero
	movsd	8(%rsp), %xmm6          # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movsd	.LCPI0_6(%rip), %xmm5   # xmm5 = mem[0],zero
	movsd	.LCPI0_0(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	16(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB0_302:                              #   in Loop: Header=BB0_300 Depth=1
	addsd	%xmm1, %xmm3
	addsd	%xmm5, %xmm6
	addq	$24, %rbx
	jne	.LBB0_300
# BB#303:                               # %.preheader1067.21310.preheader
	movq	$-216, %rbx
	movsd	.LCPI0_30(%rip), %xmm7  # xmm7 = mem[0],zero
	.p2align	4, 0x90
.LBB0_304:                              # %.preheader1067.21310
                                        # =>This Inner Loop Header: Depth=1
	leaq	L3psycho_anal.thm+464(%rbx), %rax
	movsd	L3psycho_anal.thm+480(%rbx), %xmm0 # xmm0 = mem[0],zero
	leaq	L3psycho_anal.thm+952(%rbx), %rcx
	movsd	L3psycho_anal.thm+968(%rbx), %xmm1 # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movq	%rcx, %rdx
	cmovaq	%rax, %rdx
	movsd	16(%rdx), %xmm2         # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	cmovaq	%rax, %rcx
	movsd	16(%rcx), %xmm0         # xmm0 = mem[0],zero
	movapd	%xmm2, %xmm1
	mulsd	%xmm4, %xmm1
	ucomisd	%xmm1, %xmm0
	movapd	%xmm7, %xmm1
	jae	.LBB0_306
# BB#305:                               #   in Loop: Header=BB0_304 Depth=1
	divsd	%xmm2, %xmm0
	movsd	%xmm3, 16(%rsp)         # 8-byte Spill
	movsd	%xmm6, 8(%rsp)          # 8-byte Spill
	callq	log10
	movsd	.LCPI0_30(%rip), %xmm7  # xmm7 = mem[0],zero
	movsd	8(%rsp), %xmm6          # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movsd	.LCPI0_6(%rip), %xmm5   # xmm5 = mem[0],zero
	movsd	.LCPI0_0(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	16(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB0_306:                              #   in Loop: Header=BB0_304 Depth=1
	addsd	%xmm1, %xmm3
	addsd	%xmm5, %xmm6
	addq	$24, %rbx
	jne	.LBB0_304
# BB#307:
	movsd	.LCPI0_32(%rip), %xmm0  # xmm0 = mem[0],zero
	movsd	48(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	minsd	%xmm0, %xmm1
	divsd	%xmm6, %xmm3
	mulsd	.LCPI0_31(%rip), %xmm3
	minsd	%xmm0, %xmm3
.LBB0_308:
	movq	768(%rsp), %rbx
	movl	204(%r14), %ebp
	testl	%ebp, %ebp
	jle	.LBB0_328
# BB#309:                               # %._crit_edge1104
	leal	-1(%rbp), %eax
	leaq	4(,%rax,4), %rdx
	leaq	104(%rsp), %rdi
	xorl	%esi, %esi
	movsd	%xmm1, 48(%rsp)         # 8-byte Spill
	movsd	%xmm3, 16(%rsp)         # 8-byte Spill
	callq	memset
	movsd	16(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	48(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	cmpl	$2, %ebp
	jne	.LBB0_316
# BB#310:
	cmpl	$0, 156(%r14)
	je	.LBB0_312
# BB#311:
	cmpl	$1, 36(%r14)
	jne	.LBB0_315
.LBB0_312:
	cmpl	$0, 36(%rsp)
	je	.LBB0_314
# BB#313:
	movl	40(%rsp), %eax
	testl	%eax, %eax
	jne	.LBB0_315
.LBB0_314:
	movq	$0, 36(%rsp)
.LBB0_315:                              # %.preheader
	testl	%ebp, %ebp
	jle	.LBB0_328
.LBB0_316:                              # %.lr.ph.preheader
	movl	$L3psycho_anal.blocktype_old, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_317:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 36(%rsp,%rcx,4)
	je	.LBB0_322
# BB#318:                               #   in Loop: Header=BB0_317 Depth=1
	movl	(%rax), %edx
	cmpq	$3, %rdx
	ja	.LBB0_325
# BB#319:                               #   in Loop: Header=BB0_317 Depth=1
	xorl	%esi, %esi
	jmpq	*.LJTI0_0(,%rdx,8)
.LBB0_320:                              #   in Loop: Header=BB0_317 Depth=1
	movl	$3, %esi
.LBB0_321:                              # %.sink.split
                                        #   in Loop: Header=BB0_317 Depth=1
	movl	%esi, 104(%rsp,%rcx,4)
	movq	%rax, %rdi
	jmp	.LBB0_327
	.p2align	4, 0x90
.LBB0_322:                              #   in Loop: Header=BB0_317 Depth=1
	movl	$2, 104(%rsp,%rcx,4)
	leaq	L3psycho_anal.blocktype_old(,%rcx,4), %rdi
	movl	(%rax), %edx
	cmpl	$3, %edx
	je	.LBB0_326
# BB#323:                               #   in Loop: Header=BB0_317 Depth=1
	movl	$2, %esi
	testl	%edx, %edx
	jne	.LBB0_327
# BB#324:                               # %.thread
                                        #   in Loop: Header=BB0_317 Depth=1
	movl	$1, (%rax)
	movl	$1, %edx
	jmp	.LBB0_327
.LBB0_325:                              # %._crit_edge1452
                                        #   in Loop: Header=BB0_317 Depth=1
	movl	104(%rsp,%rcx,4), %esi
	movq	%rax, %rdi
	jmp	.LBB0_327
.LBB0_326:                              #   in Loop: Header=BB0_317 Depth=1
	movl	$2, (%rax)
	movl	$2, %esi
	movl	$2, %edx
	.p2align	4, 0x90
.LBB0_327:                              #   in Loop: Header=BB0_317 Depth=1
	movl	%edx, (%rbx,%rcx,4)
	movl	%esi, (%rdi)
	incq	%rcx
	movslq	204(%r14), %rdx
	addq	$4, %rax
	cmpq	%rdx, %rcx
	jl	.LBB0_317
.LBB0_328:                              # %._crit_edge
	cmpl	$2, (%rbx)
	movl	$L3psycho_anal.ms_ratio_s_old, %eax
	movl	$L3psycho_anal.ms_ratio_l_old, %ecx
	cmoveq	%rax, %rcx
	movq	(%rcx), %rax
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	%rax, (%rcx)
	movsd	%xmm3, L3psycho_anal.ms_ratio_s_old(%rip)
	movsd	%xmm1, L3psycho_anal.ms_ratio_l_old(%rip)
	movq	152(%rsp), %rax         # 8-byte Reload
	movsd	%xmm1, (%rax)
	cmpb	$0, 72(%rsp)            # 1-byte Folded Reload
	je	.LBB0_331
# BB#329:
	movss	188(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	184(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movq	L3psycho_anal.ms_ener_ratio_old(%rip), %rax
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	%rax, (%rcx)
	movq	$0, L3psycho_anal.ms_ener_ratio_old(%rip)
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm1
	jbe	.LBB0_332
# BB#330:
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, L3psycho_anal.ms_ener_ratio_old(%rip)
	jmp	.LBB0_332
.LBB0_331:
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	$0, (%rax)
.LBB0_332:
	addq	$680, %rsp              # imm = 0x2A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_333:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	callq	abort
.LBB0_334:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$-1, %edi
	callq	exit
.Lfunc_end0:
	.size	L3psycho_anal, .Lfunc_end0-L3psycho_anal
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_321
	.quad	.LBB0_333
	.quad	.LBB0_320
	.quad	.LBB0_321

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	-4604930618986332160    # double -6
.LCPI1_1:
	.quad	-4625908079470802909    # double -0.23025850929940458
.LCPI1_3:
	.quad	4602678819172646912     # double 0.5
.LCPI1_4:
	.quad	4612811918334230528     # double 2.5
.LCPI1_5:
	.quad	-4620693217682128896    # double -0.5
.LCPI1_6:
	.quad	-4611686018427387904    # double -2
.LCPI1_7:
	.quad	4620693217682128896     # double 8
.LCPI1_8:
	.quad	4602210444811400380     # double 0.47399999999999998
.LCPI1_9:
	.quad	4620130267728707584     # double 7.5
.LCPI1_10:
	.quad	4625090638755834645     # double 15.811389
.LCPI1_11:
	.quad	4607182418800017408     # double 1
.LCPI1_12:
	.quad	-4597753007080210432    # double -17.5
.LCPI1_13:
	.quad	-4589730970243956736    # double -60
.LCPI1_14:
	.quad	4597463957383972899     # double 0.23025850929940458
.LCPI1_16:
	.quad	4576918229304087675     # double 0.01
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_2:
	.quad	4613937818241073152     # double 3
	.quad	4609434218613702656     # double 1.5
.LCPI1_15:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	L3para_read
	.p2align	4, 0x90
	.type	L3para_read,@function
L3para_read:                            # @L3para_read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$1080, %rsp             # imm = 0x438
.Lcfi32:
	.cfi_def_cfa_offset 1136
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	movl	$psy_data, %r14d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_21 Depth 2
                                        #       Child Loop BB1_24 Depth 3
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	cvttsd2si	8(%r14), %eax
	addq	$16, %r14
	leal	1(%rax), %esi
	ucomisd	32(%rsp), %xmm0         # 8-byte Folded Reload
	jne	.LBB1_31
	jp	.LBB1_31
# BB#2:                                 # %.preheader296
                                        #   in Loop: Header=BB1_1 Depth=1
	testl	%eax, %eax
	js	.LBB1_3
# BB#20:                                # %.lr.ph350.preheader
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movslq	%eax, %r13
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_21:                               # %.lr.ph350
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_24 Depth 3
	cvttsd2si	(%r14), %ebx
	cvttsd2si	8(%r14), %eax
	movl	%eax, (%rbp,%r15,4)
	movsd	16(%r14), %xmm0         # xmm0 = mem[0],zero
	addsd	.LCPI1_0(%rip), %xmm0
	mulsd	.LCPI1_1(%rip), %xmm0
	callq	exp
	movq	8(%rsp), %rax           # 8-byte Reload
	movsd	%xmm0, (%rax,%r15,8)
	movq	24(%r14), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx,%r15,8)
	movq	40(%r14), %rax
	movq	%rax, 576(%rsp,%r15,8)
	cmpq	%r15, %rbx
	jne	.LBB1_28
# BB#22:                                # %.preheader295
                                        #   in Loop: Header=BB1_21 Depth=2
	cmpl	$0, (%rbp,%r15,4)
	jle	.LBB1_26
# BB#23:                                # %.lr.ph344.preheader
                                        #   in Loop: Header=BB1_21 Depth=2
	movslq	%r12d, %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_24:                               # %.lr.ph344
                                        #   Parent Loop BB1_1 Depth=1
                                        #     Parent Loop BB1_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r15d, (%rcx,%rax,4)
	incq	%rax
	cmpl	(%rbp,%r15,4), %eax
	jl	.LBB1_24
# BB#25:                                # %._crit_edge345.loopexit
                                        #   in Loop: Header=BB1_21 Depth=2
	addl	%eax, %r12d
.LBB1_26:                               # %._crit_edge345
                                        #   in Loop: Header=BB1_21 Depth=2
	addq	$48, %r14
	cmpq	%r13, %r15
	leaq	1(%r15), %r15
	jl	.LBB1_21
# BB#27:                                #   in Loop: Header=BB1_1 Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	movq	48(%rsp), %r15          # 8-byte Reload
	movl	24(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_32
	.p2align	4, 0x90
.LBB1_31:                               #   in Loop: Header=BB1_1 Depth=1
	addl	%esi, %esi
	leal	(%rsi,%rsi,2), %eax
	cltq
	leaq	(%r14,%rax,8), %r14
	jmp	.LBB1_32
	.p2align	4, 0x90
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	movl	%esi, %ecx
.LBB1_32:                               # %.loopexit297
                                        #   in Loop: Header=BB1_1 Depth=1
	incl	%edx
	cmpl	$6, %edx
	jl	.LBB1_1
# BB#4:                                 # %.preheader294
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	movq	40(%rsp), %rax          # 8-byte Reload
	jle	.LBB1_5
# BB#9:                                 # %.preheader293.us.preheader
	movl	24(%rsp), %r12d         # 4-byte Reload
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB1_10:                               # %.preheader293.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_11 Depth 2
	movsd	576(%rsp,%r13,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rax, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_11:                               #   Parent Loop BB1_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%eax, %eax
	cmpq	%rbx, %r13
	setl	%al
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	subsd	576(%rsp,%rbx,8), %xmm0
	mulsd	.LCPI1_2(,%rax,8), %xmm0
	xorpd	%xmm2, %xmm2
	ucomisd	.LCPI1_3(%rip), %xmm0
	jb	.LBB1_14
# BB#12:                                #   in Loop: Header=BB1_11 Depth=2
	movsd	.LCPI1_4(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jb	.LBB1_14
# BB#13:                                #   in Loop: Header=BB1_11 Depth=2
	movapd	%xmm0, %xmm2
	addsd	.LCPI1_5(%rip), %xmm2
	movapd	%xmm2, %xmm1
	mulsd	%xmm1, %xmm1
	mulsd	.LCPI1_6(%rip), %xmm2
	addsd	%xmm1, %xmm2
	mulsd	.LCPI1_7(%rip), %xmm2
.LBB1_14:                               #   in Loop: Header=BB1_11 Depth=2
	addsd	.LCPI1_8(%rip), %xmm0
	movapd	%xmm0, %xmm3
	mulsd	.LCPI1_9(%rip), %xmm3
	addsd	.LCPI1_10(%rip), %xmm3
	mulsd	%xmm0, %xmm0
	addsd	.LCPI1_11(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB1_16
# BB#15:                                # %call.sqrt
                                        #   in Loop: Header=BB1_11 Depth=2
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	callq	sqrt
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB1_16:                               # %.split
                                        #   in Loop: Header=BB1_11 Depth=2
	mulsd	.LCPI1_12(%rip), %xmm1
	addsd	%xmm1, %xmm3
	xorpd	%xmm0, %xmm0
	movsd	.LCPI1_13(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm3, %xmm1
	jae	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_11 Depth=2
	addsd	%xmm3, %xmm2
	mulsd	.LCPI1_14(%rip), %xmm2
	movapd	%xmm2, %xmm0
	callq	exp
.LBB1_18:                               #   in Loop: Header=BB1_11 Depth=2
	movsd	%xmm0, (%rbp)
	incq	%rbx
	addq	$8, %rbp
	cmpq	%rbx, %r12
	jne	.LBB1_11
# BB#19:                                # %._crit_edge340.us
                                        #   in Loop: Header=BB1_10 Depth=1
	incq	%r13
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	$512, %rax              # imm = 0x200
	cmpq	%rbx, %r13
	jne	.LBB1_10
.LBB1_5:                                # %.preheader292.preheader
	xorl	%r9d, %r9d
	movq	1136(%rsp), %r13
	movq	24(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader292
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_52 Depth 2
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	cvttsd2si	8(%r14), %edx
	addq	$16, %r14
	leal	1(%rdx), %r8d
	ucomisd	32(%rsp), %xmm0         # 8-byte Folded Reload
	jne	.LBB1_57
	jp	.LBB1_57
# BB#7:                                 # %.preheader291
                                        #   in Loop: Header=BB1_6 Depth=1
	testl	%edx, %edx
	js	.LBB1_8
# BB#51:                                # %.lr.ph331.preheader
                                        #   in Loop: Header=BB1_6 Depth=1
	movslq	%edx, %rsi
	movq	$-1, %rdi
	movq	1152(%rsp), %rax
	movq	%rax, %r10
	movq	1144(%rsp), %rax
	.p2align	4, 0x90
.LBB1_52:                               # %.lr.ph331
                                        #   Parent Loop BB1_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%rdi), %rdx
	cvttsd2si	(%r14), %ebx
	cvttsd2si	8(%r14), %ebp
	movl	%ebp, 4(%r15,%rdi,4)
	movq	16(%r14), %rcx
	movq	%rcx, 8(%rax,%rdi,8)
	movq	32(%r14), %rcx
	movq	%rcx, 8(%r10,%rdi,8)
	movq	40(%r14), %rcx
	movq	%rcx, 72(%rsp,%rdi,8)
	cmpq	%rbx, %rdx
	jne	.LBB1_53
# BB#54:                                #   in Loop: Header=BB1_52 Depth=2
	addq	$48, %r14
	decl	%ebp
	movl	%ebp, 4(%r15,%rdi,4)
	cmpq	%rsi, %rdx
	movq	%rdx, %rdi
	jl	.LBB1_52
# BB#55:                                # %._crit_edge332.loopexit
                                        #   in Loop: Header=BB1_6 Depth=1
	incq	%rdx
	jmp	.LBB1_56
	.p2align	4, 0x90
.LBB1_57:                               #   in Loop: Header=BB1_6 Depth=1
	addl	%r8d, %r8d
	leal	(%r8,%r8,2), %ecx
	movslq	%ecx, %rcx
	leaq	(%r14,%rcx,8), %r14
	jmp	.LBB1_58
	.p2align	4, 0x90
.LBB1_8:                                #   in Loop: Header=BB1_6 Depth=1
	xorl	%edx, %edx
.LBB1_56:                               # %._crit_edge332
                                        #   in Loop: Header=BB1_6 Depth=1
	movslq	%edx, %rcx
	movl	$-1, (%r15,%rcx,4)
	movl	%r8d, %eax
.LBB1_58:                               #   in Loop: Header=BB1_6 Depth=1
	incl	%r9d
	cmpl	$6, %r9d
	jl	.LBB1_6
# BB#33:                                # %.preheader290
	testl	%eax, %eax
	jle	.LBB1_34
# BB#40:                                # %.preheader289.us.preheader
	movl	%eax, %r15d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_41:                               # %.preheader289.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_42 Depth 2
	movsd	64(%rsp,%r12,8), %xmm0  # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movq	%r13, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_42:                               #   Parent Loop BB1_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%eax, %eax
	cmpq	%rbx, %r12
	setl	%al
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	subsd	64(%rsp,%rbx,8), %xmm0
	mulsd	.LCPI1_2(,%rax,8), %xmm0
	xorpd	%xmm2, %xmm2
	ucomisd	.LCPI1_3(%rip), %xmm0
	jb	.LBB1_45
# BB#43:                                #   in Loop: Header=BB1_42 Depth=2
	movsd	.LCPI1_4(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jb	.LBB1_45
# BB#44:                                #   in Loop: Header=BB1_42 Depth=2
	movapd	%xmm0, %xmm2
	addsd	.LCPI1_5(%rip), %xmm2
	movapd	%xmm2, %xmm1
	mulsd	%xmm1, %xmm1
	mulsd	.LCPI1_6(%rip), %xmm2
	addsd	%xmm1, %xmm2
	mulsd	.LCPI1_7(%rip), %xmm2
.LBB1_45:                               #   in Loop: Header=BB1_42 Depth=2
	addsd	.LCPI1_8(%rip), %xmm0
	movapd	%xmm0, %xmm3
	mulsd	.LCPI1_9(%rip), %xmm3
	addsd	.LCPI1_10(%rip), %xmm3
	mulsd	%xmm0, %xmm0
	addsd	.LCPI1_11(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB1_47
# BB#46:                                # %call.sqrt537
                                        #   in Loop: Header=BB1_42 Depth=2
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	callq	sqrt
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB1_47:                               # %.split536
                                        #   in Loop: Header=BB1_42 Depth=2
	mulsd	.LCPI1_12(%rip), %xmm1
	addsd	%xmm1, %xmm3
	xorpd	%xmm0, %xmm0
	movsd	.LCPI1_13(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm3, %xmm1
	jae	.LBB1_49
# BB#48:                                #   in Loop: Header=BB1_42 Depth=2
	addsd	%xmm3, %xmm2
	mulsd	.LCPI1_14(%rip), %xmm2
	movapd	%xmm2, %xmm0
	callq	exp
.LBB1_49:                               #   in Loop: Header=BB1_42 Depth=2
	movsd	%xmm0, (%rbp)
	incq	%rbx
	addq	$8, %rbp
	cmpq	%rbx, %r15
	jne	.LBB1_42
# BB#50:                                # %._crit_edge.us
                                        #   in Loop: Header=BB1_41 Depth=1
	incq	%r12
	addq	$512, %r13              # imm = 0x200
	cmpq	%rbx, %r12
	jne	.LBB1_41
.LBB1_34:                               # %.preheader288.preheader
	xorl	%r10d, %r10d
	movsd	.LCPI1_11(%rip), %xmm0  # xmm0 = mem[0],zero
	movapd	.LCPI1_15(%rip), %xmm1  # xmm1 = [nan,nan]
	movsd	.LCPI1_16(%rip), %xmm2  # xmm2 = mem[0],zero
	movq	1216(%rsp), %r15
	movq	1208(%rsp), %r12
	movq	1200(%rsp), %r8
	movq	1192(%rsp), %r9
	movq	1184(%rsp), %r13
	.p2align	4, 0x90
.LBB1_35:                               # %.preheader288
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_38 Depth 2
	movsd	(%r14), %xmm3           # xmm3 = mem[0],zero
	cvttsd2si	8(%r14), %ecx
	addq	$16, %r14
	ucomisd	32(%rsp), %xmm3         # 8-byte Folded Reload
	jne	.LBB1_64
	jp	.LBB1_64
# BB#36:                                # %.preheader286
                                        #   in Loop: Header=BB1_35 Depth=1
	testl	%ecx, %ecx
	js	.LBB1_65
# BB#37:                                # %.lr.ph322.preheader
                                        #   in Loop: Header=BB1_35 Depth=1
	movslq	%ecx, %rcx
	movq	$-1, %rbx
	.p2align	4, 0x90
.LBB1_38:                               # %.lr.ph322
                                        #   Parent Loop BB1_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdx
	leaq	1(%rbx), %rsi
	cvttsd2si	(%rdx), %edi
	cvttsd2si	16(%rdx), %ebp
	movq	1160(%rsp), %rax
	movl	%ebp, 4(%rax,%rbx,4)
	cvttsd2si	24(%rdx), %ebp
	movq	1168(%rsp), %rax
	movl	%ebp, 4(%rax,%rbx,4)
	movq	32(%rdx), %rbp
	movq	1176(%rsp), %rax
	movq	%rbp, 8(%rax,%rbx,8)
	movq	40(%rdx), %rbp
	movq	%rbp, 8(%r13,%rbx,8)
	cmpq	%rdi, %rsi
	jne	.LBB1_39
# BB#59:                                #   in Loop: Header=BB1_38 Depth=2
	cmpq	$-1, %rbx
	je	.LBB1_62
# BB#60:                                #   in Loop: Header=BB1_38 Depth=2
	movapd	%xmm0, %xmm3
	movq	1176(%rsp), %rax
	subsd	8(%rax,%rbx,8), %xmm3
	subsd	(%r13,%rbx,8), %xmm3
	andpd	%xmm1, %xmm3
	ucomisd	%xmm2, %xmm3
	ja	.LBB1_61
.LBB1_62:                               #   in Loop: Header=BB1_38 Depth=2
	leaq	48(%rdx), %r14
	cmpq	%rcx, %rsi
	movq	%rsi, %rbx
	jl	.LBB1_38
# BB#63:                                #   in Loop: Header=BB1_35 Depth=1
	addq	$48, %rdx
	movq	%rdx, %r14
	jmp	.LBB1_65
	.p2align	4, 0x90
.LBB1_64:                               #   in Loop: Header=BB1_35 Depth=1
	leal	(%rcx,%rcx,2), %ecx
	leal	6(%rcx,%rcx), %ecx
	movslq	%ecx, %rcx
	leaq	(%r14,%rcx,8), %r14
.LBB1_65:                               # %.loopexit287
                                        #   in Loop: Header=BB1_35 Depth=1
	incl	%r10d
	cmpl	$6, %r10d
	jl	.LBB1_35
# BB#66:                                # %.preheader285.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_67:                               # %.preheader285
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_70 Depth 2
	movsd	(%r14), %xmm3           # xmm3 = mem[0],zero
	cvttsd2si	8(%r14), %ecx
	addq	$16, %r14
	ucomisd	32(%rsp), %xmm3         # 8-byte Folded Reload
	jne	.LBB1_77
	jp	.LBB1_77
# BB#68:                                # %.preheader
                                        #   in Loop: Header=BB1_67 Depth=1
	testl	%ecx, %ecx
	js	.LBB1_78
# BB#69:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_67 Depth=1
	movslq	%ecx, %rcx
	movq	$-1, %rbx
	.p2align	4, 0x90
.LBB1_70:                               # %.lr.ph
                                        #   Parent Loop BB1_67 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdx
	leaq	1(%rbx), %rsi
	cvttsd2si	(%rdx), %edi
	cvttsd2si	16(%rdx), %ebp
	movl	%ebp, 4(%r9,%rbx,4)
	cvttsd2si	24(%rdx), %ebp
	movl	%ebp, 4(%r8,%rbx,4)
	movq	32(%rdx), %rbp
	movq	%rbp, 8(%r12,%rbx,8)
	movq	40(%rdx), %rbp
	movq	%rbp, 8(%r15,%rbx,8)
	cmpq	%rdi, %rsi
	jne	.LBB1_39
# BB#71:                                #   in Loop: Header=BB1_70 Depth=2
	cmpq	$-1, %rbx
	je	.LBB1_75
# BB#72:                                #   in Loop: Header=BB1_70 Depth=2
	movapd	%xmm0, %xmm3
	subsd	8(%r12,%rbx,8), %xmm3
	subsd	(%r15,%rbx,8), %xmm3
	andpd	%xmm1, %xmm3
	ucomisd	%xmm2, %xmm3
	ja	.LBB1_73
.LBB1_75:                               #   in Loop: Header=BB1_70 Depth=2
	leaq	48(%rdx), %r14
	cmpq	%rcx, %rsi
	movq	%rsi, %rbx
	jl	.LBB1_70
# BB#76:                                #   in Loop: Header=BB1_67 Depth=1
	addq	$48, %rdx
	movq	%rdx, %r14
	jmp	.LBB1_78
	.p2align	4, 0x90
.LBB1_77:                               #   in Loop: Header=BB1_67 Depth=1
	leal	(%rcx,%rcx,2), %ecx
	leal	6(%rcx,%rcx), %ecx
	movslq	%ecx, %rcx
	leaq	(%r14,%rcx,8), %r14
.LBB1_78:                               # %.loopexit
                                        #   in Loop: Header=BB1_67 Depth=1
	incl	%eax
	cmpl	$6, %eax
	jl	.LBB1_67
# BB#79:
	addq	$1080, %rsp             # imm = 0x438
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_39:
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$27, %esi
	jmp	.LBB1_30
.LBB1_28:
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	jmp	.LBB1_29
.LBB1_53:
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
.LBB1_29:
	movl	$26, %esi
.LBB1_30:
	movl	$1, %edx
	callq	fwrite
	movl	$-1, %edi
	callq	exit
.LBB1_61:
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$30, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	movq	1176(%rsp), %rax
	movsd	8(%rax,%rbx,8), %xmm0   # xmm0 = mem[0],zero
	movsd	(%r13,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	jmp	.LBB1_74
.LBB1_73:
	movq	stderr(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$30, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	movsd	8(%r12,%rbx,8), %xmm0   # xmm0 = mem[0],zero
	movsd	(%r15,%rbx,8), %xmm1    # xmm1 = mem[0],zero
.LBB1_74:
	movl	$.L.str.6, %esi
	movb	$2, %al
	callq	fprintf
	movl	$-1, %edi
	callq	exit
.Lfunc_end1:
	.size	L3para_read, .Lfunc_end1-L3para_read
	.cfi_endproc

	.type	L3psycho_anal.minval,@object # @L3psycho_anal.minval
	.local	L3psycho_anal.minval
	.comm	L3psycho_anal.minval,504,16
	.type	L3psycho_anal.qthr_l,@object # @L3psycho_anal.qthr_l
	.local	L3psycho_anal.qthr_l
	.comm	L3psycho_anal.qthr_l,504,16
	.type	L3psycho_anal.qthr_s,@object # @L3psycho_anal.qthr_s
	.local	L3psycho_anal.qthr_s
	.comm	L3psycho_anal.qthr_s,504,16
	.type	L3psycho_anal.nb_1,@object # @L3psycho_anal.nb_1
	.local	L3psycho_anal.nb_1
	.comm	L3psycho_anal.nb_1,2016,16
	.type	L3psycho_anal.nb_2,@object # @L3psycho_anal.nb_2
	.local	L3psycho_anal.nb_2
	.comm	L3psycho_anal.nb_2,2016,16
	.type	L3psycho_anal.s3_s,@object # @L3psycho_anal.s3_s
	.local	L3psycho_anal.s3_s
	.comm	L3psycho_anal.s3_s,32768,16
	.type	L3psycho_anal.s3_l,@object # @L3psycho_anal.s3_l
	.local	L3psycho_anal.s3_l
	.comm	L3psycho_anal.s3_l,32768,16
	.type	L3psycho_anal.thm,@object # @L3psycho_anal.thm
	.local	L3psycho_anal.thm
	.comm	L3psycho_anal.thm,1952,16
	.type	L3psycho_anal.en,@object # @L3psycho_anal.en
	.local	L3psycho_anal.en
	.comm	L3psycho_anal.en,1952,16
	.type	L3psycho_anal.cw_upper_index,@object # @L3psycho_anal.cw_upper_index
	.local	L3psycho_anal.cw_upper_index
	.comm	L3psycho_anal.cw_upper_index,4,4
	.type	L3psycho_anal.cw_lower_index,@object # @L3psycho_anal.cw_lower_index
	.local	L3psycho_anal.cw_lower_index
	.comm	L3psycho_anal.cw_lower_index,1,4
	.type	L3psycho_anal.ax_sav,@object # @L3psycho_anal.ax_sav
	.local	L3psycho_anal.ax_sav
	.comm	L3psycho_anal.ax_sav,16416,16
	.type	L3psycho_anal.bx_sav,@object # @L3psycho_anal.bx_sav
	.local	L3psycho_anal.bx_sav
	.comm	L3psycho_anal.bx_sav,16416,16
	.type	L3psycho_anal.rx_sav,@object # @L3psycho_anal.rx_sav
	.local	L3psycho_anal.rx_sav
	.comm	L3psycho_anal.rx_sav,16416,16
	.type	L3psycho_anal.cw,@object # @L3psycho_anal.cw
	.local	L3psycho_anal.cw
	.comm	L3psycho_anal.cw,2052,16
	.type	L3psycho_anal.wsamp_L,@object # @L3psycho_anal.wsamp_L
	.local	L3psycho_anal.wsamp_L
	.comm	L3psycho_anal.wsamp_L,8192,16
	.type	L3psycho_anal.energy,@object # @L3psycho_anal.energy
	.local	L3psycho_anal.energy
	.comm	L3psycho_anal.energy,2052,16
	.type	L3psycho_anal.wsamp_S,@object # @L3psycho_anal.wsamp_S
	.local	L3psycho_anal.wsamp_S
	.comm	L3psycho_anal.wsamp_S,6144,16
	.type	L3psycho_anal.energy_s,@object # @L3psycho_anal.energy_s
	.local	L3psycho_anal.energy_s
	.comm	L3psycho_anal.energy_s,1548,16
	.type	L3psycho_anal.eb,@object # @L3psycho_anal.eb
	.local	L3psycho_anal.eb
	.comm	L3psycho_anal.eb,504,16
	.type	L3psycho_anal.cb,@object # @L3psycho_anal.cb
	.local	L3psycho_anal.cb
	.comm	L3psycho_anal.cb,504,16
	.type	L3psycho_anal.thr,@object # @L3psycho_anal.thr
	.local	L3psycho_anal.thr
	.comm	L3psycho_anal.thr,504,16
	.type	L3psycho_anal.w1_l,@object # @L3psycho_anal.w1_l
	.local	L3psycho_anal.w1_l
	.comm	L3psycho_anal.w1_l,168,16
	.type	L3psycho_anal.w2_l,@object # @L3psycho_anal.w2_l
	.local	L3psycho_anal.w2_l
	.comm	L3psycho_anal.w2_l,168,16
	.type	L3psycho_anal.w1_s,@object # @L3psycho_anal.w1_s
	.local	L3psycho_anal.w1_s
	.comm	L3psycho_anal.w1_s,96,16
	.type	L3psycho_anal.w2_s,@object # @L3psycho_anal.w2_s
	.local	L3psycho_anal.w2_s
	.comm	L3psycho_anal.w2_s,96,16
	.type	L3psycho_anal.mld_l,@object # @L3psycho_anal.mld_l
	.local	L3psycho_anal.mld_l
	.comm	L3psycho_anal.mld_l,168,16
	.type	L3psycho_anal.mld_s,@object # @L3psycho_anal.mld_s
	.local	L3psycho_anal.mld_s
	.comm	L3psycho_anal.mld_s,96,16
	.type	L3psycho_anal.bu_l,@object # @L3psycho_anal.bu_l
	.local	L3psycho_anal.bu_l
	.comm	L3psycho_anal.bu_l,84,16
	.type	L3psycho_anal.bo_l,@object # @L3psycho_anal.bo_l
	.local	L3psycho_anal.bo_l
	.comm	L3psycho_anal.bo_l,84,16
	.type	L3psycho_anal.bu_s,@object # @L3psycho_anal.bu_s
	.local	L3psycho_anal.bu_s
	.comm	L3psycho_anal.bu_s,48,16
	.type	L3psycho_anal.bo_s,@object # @L3psycho_anal.bo_s
	.local	L3psycho_anal.bo_s
	.comm	L3psycho_anal.bo_s,48,16
	.type	L3psycho_anal.npart_l,@object # @L3psycho_anal.npart_l
	.local	L3psycho_anal.npart_l
	.comm	L3psycho_anal.npart_l,4,4
	.type	L3psycho_anal.npart_s,@object # @L3psycho_anal.npart_s
	.local	L3psycho_anal.npart_s
	.comm	L3psycho_anal.npart_s,4,4
	.type	L3psycho_anal.npart_l_orig,@object # @L3psycho_anal.npart_l_orig
	.local	L3psycho_anal.npart_l_orig
	.comm	L3psycho_anal.npart_l_orig,4,4
	.type	L3psycho_anal.npart_s_orig,@object # @L3psycho_anal.npart_s_orig
	.local	L3psycho_anal.npart_s_orig
	.comm	L3psycho_anal.npart_s_orig,4,4
	.type	L3psycho_anal.s3ind,@object # @L3psycho_anal.s3ind
	.local	L3psycho_anal.s3ind
	.comm	L3psycho_anal.s3ind,504,16
	.type	L3psycho_anal.s3ind_s,@object # @L3psycho_anal.s3ind_s
	.local	L3psycho_anal.s3ind_s
	.comm	L3psycho_anal.s3ind_s,504,16
	.type	L3psycho_anal.numlines_s,@object # @L3psycho_anal.numlines_s
	.local	L3psycho_anal.numlines_s
	.comm	L3psycho_anal.numlines_s,252,16
	.type	L3psycho_anal.numlines_l,@object # @L3psycho_anal.numlines_l
	.local	L3psycho_anal.numlines_l
	.comm	L3psycho_anal.numlines_l,252,16
	.type	L3psycho_anal.partition_l,@object # @L3psycho_anal.partition_l
	.local	L3psycho_anal.partition_l
	.comm	L3psycho_anal.partition_l,2052,16
	.type	L3psycho_anal.pe,@object # @L3psycho_anal.pe
	.local	L3psycho_anal.pe
	.comm	L3psycho_anal.pe,32,16
	.type	L3psycho_anal.ms_ratio_s_old,@object # @L3psycho_anal.ms_ratio_s_old
	.local	L3psycho_anal.ms_ratio_s_old
	.comm	L3psycho_anal.ms_ratio_s_old,8,8
	.type	L3psycho_anal.ms_ratio_l_old,@object # @L3psycho_anal.ms_ratio_l_old
	.local	L3psycho_anal.ms_ratio_l_old
	.comm	L3psycho_anal.ms_ratio_l_old,8,8
	.type	L3psycho_anal.ms_ener_ratio_old,@object # @L3psycho_anal.ms_ener_ratio_old
	.data
	.p2align	3
L3psycho_anal.ms_ener_ratio_old:
	.quad	4598175219545276416     # double 0.25
	.size	L3psycho_anal.ms_ener_ratio_old, 8

	.type	L3psycho_anal.blocktype_old,@object # @L3psycho_anal.blocktype_old
	.local	L3psycho_anal.blocktype_old
	.comm	L3psycho_anal.blocktype_old,8,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"error, invalid sampling frequency: %d Hz\n"
	.size	.L.str, 42

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Error in block selecting\n"
	.size	.L.str.1, 26

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"1. please check \"psy_data\""
	.size	.L.str.2, 27

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"3. please check \"psy_data\""
	.size	.L.str.3, 27

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"30:please check \"psy_data\"\n"
	.size	.L.str.4, 28

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"31l: please check \"psy_data.\"\n"
	.size	.L.str.5, 31

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"w1,w2: %f %f \n"
	.size	.L.str.6, 15

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"31s: please check \"psy_data.\"\n"
	.size	.L.str.7, 31


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
