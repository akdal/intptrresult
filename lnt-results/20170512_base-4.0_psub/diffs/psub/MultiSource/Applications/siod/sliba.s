	.text
	.file	"sliba.bc"
	.globl	init_storage_a1
	.p2align	4, 0x90
	.type	init_storage_a1,@function
init_storage_a1:                        # @init_storage_a1
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	8(%rsp), %r9
	movl	$array_gc_relocate, %esi
	movl	$array_gc_mark, %edx
	movl	$array_gc_scan, %ecx
	movl	$array_gc_free, %r8d
	callq	set_gc_hooks
	movl	$array_prin1, %esi
	movq	%rbx, %rdi
	callq	set_print_hooks
	movq	%rbx, %rdi
	callq	get_user_type_hooks
	movq	$array_fast_print, 56(%rax)
	movq	$array_fast_read, 64(%rax)
	movq	$array_equal, 72(%rax)
	movq	$array_sxhash, 48(%rax)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end0:
	.size	init_storage_a1, .Lfunc_end0-init_storage_a1
	.cfi_endproc

	.globl	array_gc_relocate
	.p2align	4, 0x90
	.type	array_gc_relocate,@function
array_gc_relocate:                      # @array_gc_relocate
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -24
.Lcfi7:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB1_2
# BB#1:
	callq	gc_fatal_error
.LBB1_2:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	movq	16(%r14), %rax
	movq	%rax, 16(%rbx)
	movups	(%r14), %xmm0
	movups	%xmm0, (%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	array_gc_relocate, .Lfunc_end1-array_gc_relocate
	.cfi_endproc

	.globl	array_gc_mark
	.p2align	4, 0x90
	.type	array_gc_mark,@function
array_gc_mark:                          # @array_gc_mark
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB2_5
# BB#1:
	movzwl	2(%r14), %eax
	cmpl	$16, %eax
	jne	.LBB2_5
# BB#2:                                 # %.preheader
	cmpq	$0, 8(%r14)
	jle	.LBB2_5
# BB#3:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	gc_mark
	incq	%rbx
	cmpq	8(%r14), %rbx
	jl	.LBB2_4
.LBB2_5:                                # %.critedge
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	array_gc_mark, .Lfunc_end2-array_gc_mark
	.cfi_endproc

	.globl	array_gc_scan
	.p2align	4, 0x90
	.type	array_gc_scan,@function
array_gc_scan:                          # @array_gc_scan
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB3_5
# BB#1:
	movzwl	2(%r14), %eax
	cmpl	$16, %eax
	jne	.LBB3_5
# BB#2:                                 # %.preheader
	cmpq	$0, 8(%r14)
	jle	.LBB3_5
# BB#3:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_4:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	gc_relocate
	movq	16(%r14), %rcx
	movq	%rax, (%rcx,%rbx,8)
	incq	%rbx
	cmpq	8(%r14), %rbx
	jl	.LBB3_4
.LBB3_5:                                # %.critedge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	array_gc_scan, .Lfunc_end3-array_gc_scan
	.cfi_endproc

	.globl	array_gc_free
	.p2align	4, 0x90
	.type	array_gc_free,@function
array_gc_free:                          # @array_gc_free
	.cfi_startproc
# BB#0:
	movswl	2(%rdi), %eax
	addl	$-13, %eax
	cmpl	$5, %eax
	ja	.LBB4_3
# BB#1:
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_2:
	movq	16(%rdi), %rdi
	jmp	free                    # TAILCALL
.LBB4_3:
	retq
.Lfunc_end4:
	.size	array_gc_free, .Lfunc_end4-array_gc_free
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_2
	.quad	.LBB4_2
	.quad	.LBB4_2
	.quad	.LBB4_2
	.quad	.LBB4_3
	.quad	.LBB4_2

	.text
	.globl	array_prin1
	.p2align	4, 0x90
	.type	array_prin1,@function
array_prin1:                            # @array_prin1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 64
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movswl	2(%rbx), %eax
	addl	$-13, %eax
	cmpl	$5, %eax
	ja	.LBB5_42
# BB#1:
	jmpq	*.LJTI5_0(,%rax,8)
.LBB5_2:
	movl	$.L.str, %esi
	movq	%r14, %rdi
	callq	gput_st
	movq	16(%rbx), %r12
	movl	$.L.str.1, %esi
	movq	%r12, %rdi
	callq	strcspn
	movq	%rax, %r15
	movq	%r12, %rdi
	callq	strlen
	cmpq	%rax, %r15
	jne	.LBB5_27
# BB#3:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	gput_st
	jmp	.LBB5_41
.LBB5_4:
	movl	$.L.str.5, %esi
	movq	%r14, %rdi
	callq	gput_st
	cmpq	$0, 8(%rbx)
	jle	.LBB5_20
# BB#5:                                 # %.lr.ph85
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_6:                                # =>This Inner Loop Header: Depth=1
	movq	tkbuffer(%rip), %rdi
	movq	16(%rbx), %rax
	movsd	(%rax,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.6, %esi
	movb	$1, %al
	callq	sprintf
	movq	tkbuffer(%rip), %rsi
	movq	%r14, %rdi
	callq	gput_st
	incq	%rbp
	movq	8(%rbx), %rax
	cmpq	%rax, %rbp
	jge	.LBB5_8
# BB#7:                                 #   in Loop: Header=BB5_6 Depth=1
	movl	$.L.str.7, %esi
	movq	%r14, %rdi
	callq	gput_st
	movq	8(%rbx), %rax
.LBB5_8:                                # %.backedge
                                        #   in Loop: Header=BB5_6 Depth=1
	cmpq	%rax, %rbp
	jl	.LBB5_6
	jmp	.LBB5_20
.LBB5_9:
	movl	$.L.str.5, %esi
	movq	%r14, %rdi
	callq	gput_st
	leaq	8(%rbx), %r15
	cmpq	$0, 8(%rbx)
	jle	.LBB5_14
# BB#10:                                # %.lr.ph93
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_11:                               # =>This Inner Loop Header: Depth=1
	movq	tkbuffer(%rip), %rdi
	movq	16(%rbx), %rax
	movq	(%rax,%rbp,8), %rdx
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	callq	sprintf
	movq	tkbuffer(%rip), %rsi
	movq	%r14, %rdi
	callq	gput_st
	incq	%rbp
	movq	8(%rbx), %rax
	cmpq	%rax, %rbp
	jge	.LBB5_13
# BB#12:                                #   in Loop: Header=BB5_11 Depth=1
	movl	$.L.str.7, %esi
	movq	%r14, %rdi
	callq	gput_st
	movq	(%r15), %rax
.LBB5_13:                               # %.backedge80
                                        #   in Loop: Header=BB5_11 Depth=1
	cmpq	%rax, %rbp
	jl	.LBB5_11
.LBB5_14:                               # %._crit_edge94
	movl	$.L.str.8, %esi
	movq	%r14, %rdi
	callq	gput_st
	jmp	.LBB5_22
.LBB5_15:
	movl	$.L.str.5, %esi
	movq	%r14, %rdi
	callq	gput_st
	cmpq	$0, 8(%rbx)
	jle	.LBB5_20
# BB#16:                                # %.lr.ph97
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_17:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	%r14, %rsi
	callq	lprin1g
	incq	%rbp
	movq	8(%rbx), %rax
	cmpq	%rax, %rbp
	jge	.LBB5_19
# BB#18:                                #   in Loop: Header=BB5_17 Depth=1
	movl	$.L.str.7, %esi
	movq	%r14, %rdi
	callq	gput_st
	movq	8(%rbx), %rax
.LBB5_19:                               # %.backedge81
                                        #   in Loop: Header=BB5_17 Depth=1
	cmpq	%rax, %rbp
	jl	.LBB5_17
.LBB5_20:                               # %._crit_edge86
	movl	$.L.str.8, %esi
	jmp	.LBB5_26
.LBB5_21:                               # %._crit_edge111
	leaq	8(%rbx), %r15
.LBB5_22:
	movq	tkbuffer(%rip), %rdi
	movq	(%r15), %rdx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	callq	sprintf
	movq	tkbuffer(%rip), %rsi
	movq	%r14, %rdi
	callq	gput_st
	cmpq	$0, (%r15)
	jle	.LBB5_25
# BB#23:                                # %.lr.ph89
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_24:                               # =>This Inner Loop Header: Depth=1
	movq	tkbuffer(%rip), %rdi
	movq	16(%rbx), %rax
	movzbl	(%rax,%rbp), %edx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	callq	sprintf
	movq	tkbuffer(%rip), %rsi
	movq	%r14, %rdi
	callq	gput_st
	incq	%rbp
	cmpq	8(%rbx), %rbp
	jl	.LBB5_24
.LBB5_25:                               # %._crit_edge90
	movl	$.L.str, %esi
.LBB5_26:                               # %._crit_edge86
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	gput_st                 # TAILCALL
.LBB5_27:
	testl	%eax, %eax
	jle	.LBB5_41
# BB#28:                                # %.lr.ph
	movl	%eax, %r13d
	decq	%r13
	xorl	%ebp, %ebp
	leaq	5(%rsp), %r15
	jmp	.LBB5_30
	.p2align	4, 0x90
.LBB5_29:                               # %._crit_edge109
                                        #   in Loop: Header=BB5_30 Depth=1
	incq	%rbp
	movq	16(%rbx), %r12
.LBB5_30:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%r12,%rbp), %eax
	movsbl	%al, %ecx
	leal	-9(%rcx), %edx
	cmpl	$25, %edx
	ja	.LBB5_33
# BB#31:                                #   in Loop: Header=BB5_30 Depth=1
	jmpq	*.LJTI5_1(,%rdx,8)
.LBB5_32:                               #   in Loop: Header=BB5_30 Depth=1
	movl	$.L.str.4, %esi
	jmp	.LBB5_39
	.p2align	4, 0x90
.LBB5_33:                               #   in Loop: Header=BB5_30 Depth=1
	cmpl	$92, %ecx
	jne	.LBB5_35
.LBB5_34:                               #   in Loop: Header=BB5_30 Depth=1
	movb	$92, 5(%rsp)
	movb	%al, 6(%rsp)
	movb	$0, 7(%rsp)
	jmp	.LBB5_36
.LBB5_35:                               #   in Loop: Header=BB5_30 Depth=1
	movb	%al, 5(%rsp)
	movb	$0, 6(%rsp)
.LBB5_36:                               #   in Loop: Header=BB5_30 Depth=1
	movq	%r14, %rdi
	movq	%r15, %rsi
	jmp	.LBB5_40
.LBB5_37:                               #   in Loop: Header=BB5_30 Depth=1
	movl	$.L.str.2, %esi
	jmp	.LBB5_39
.LBB5_38:                               #   in Loop: Header=BB5_30 Depth=1
	movl	$.L.str.3, %esi
.LBB5_39:                               #   in Loop: Header=BB5_30 Depth=1
	movq	%r14, %rdi
.LBB5_40:                               #   in Loop: Header=BB5_30 Depth=1
	callq	gput_st
	cmpq	%rbp, %r13
	jne	.LBB5_29
.LBB5_41:
	movl	$.L.str, %esi
	movq	%r14, %rdi
	callq	gput_st
.LBB5_42:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	array_prin1, .Lfunc_end5-array_prin1
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_2
	.quad	.LBB5_4
	.quad	.LBB5_9
	.quad	.LBB5_15
	.quad	.LBB5_42
	.quad	.LBB5_21
.LJTI5_1:
	.quad	.LBB5_32
	.quad	.LBB5_37
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_38
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_34

	.text
	.globl	array_fast_print
	.p2align	4, 0x90
	.type	array_fast_print,@function
array_fast_print:                       # @array_fast_print
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 64
.Lcfi37:
	.cfi_offset %rbx, -48
.Lcfi38:
	.cfi_offset %r12, -40
.Lcfi39:
	.cfi_offset %r13, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	%r12, %rdi
	callq	car
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	get_c_file
	movq	%rax, %r14
	movswl	2(%r15), %edi
	leal	-13(%rdi), %eax
	cmpl	$5, %eax
	ja	.LBB6_11
# BB#1:
	jmpq	*.LJTI6_0(,%rax,8)
.LBB6_2:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r14, %rsi
	callq	_IO_putc
	movslq	8(%r15), %rbx
	jmp	.LBB6_3
.LBB6_4:
	movl	$14, %edi
	jmp	.LBB6_5
.LBB6_6:
	movl	$15, %edi
.LBB6_5:                                # %.loopexit
	movq	%r14, %rsi
	callq	_IO_putc
	movl	8(%r15), %eax
	shll	$3, %eax
	movslq	%eax, %rbx
.LBB6_3:                                # %.loopexit
	movq	%rbx, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	16(%r15), %rdi
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r14, %rcx
	callq	fwrite
.LBB6_10:                               # %.loopexit
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB6_7:
	movl	$16, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movl	8(%r15), %r13d
	movslq	%r13d, %rbx
	movq	%rbx, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	testl	%ebx, %ebx
	jle	.LBB6_10
# BB#8:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_9:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	%r12, %rsi
	callq	fast_print
	incq	%rbx
	cmpq	%rbx, %r13
	jne	.LBB6_9
	jmp	.LBB6_10
.LBB6_11:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	errswitch               # TAILCALL
.Lfunc_end6:
	.size	array_fast_print, .Lfunc_end6-array_fast_print
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_2
	.quad	.LBB6_4
	.quad	.LBB6_6
	.quad	.LBB6_7
	.quad	.LBB6_11
	.quad	.LBB6_2

	.text
	.globl	array_fast_read
	.p2align	4, 0x90
	.type	array_fast_read,@function
array_fast_read:                        # @array_fast_read
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi47:
	.cfi_def_cfa_offset 64
.Lcfi48:
	.cfi_offset %rbx, -48
.Lcfi49:
	.cfi_offset %r12, -40
.Lcfi50:
	.cfi_offset %r13, -32
.Lcfi51:
	.cfi_offset %r14, -24
.Lcfi52:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movl	%edi, %ebx
	movq	%r12, %rdi
	callq	car
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	get_c_file
	movq	%rax, %r14
	addl	$-13, %ebx
	cmpl	$5, %ebx
	ja	.LBB7_14
# BB#1:
	jmpq	*.LJTI7_0(,%rbx,8)
.LBB7_2:
	leaq	8(%rsp), %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fread
	movq	8(%rsp), %r13
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r12
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r15
	movw	$13, 2(%r15)
	cmpq	$-1, %r13
	movq	%r13, %rbx
	jne	.LBB7_4
# BB#3:
	xorl	%edi, %edi
	callq	strlen
	movq	%rax, %rbx
.LBB7_4:                                # %strcons.exit
	leaq	1(%rbx), %rdi
	callq	must_malloc
	movq	%rax, 16(%r15)
	movq	%rbx, 8(%r15)
	movb	$0, (%rax,%rbx)
	movq	%r12, %rdi
	callq	no_interrupt
	movq	16(%r15), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r14, %rcx
	callq	fread
	movq	16(%r15), %rax
	movb	$0, (%rax,%r13)
	jmp	.LBB7_13
.LBB7_6:
	leaq	8(%rsp), %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fread
	movq	8(%rsp), %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r12
	movl	$14, %edi
	jmp	.LBB7_7
.LBB7_9:
	leaq	8(%rsp), %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fread
	movq	8(%rsp), %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r12
	movl	$15, %edi
.LBB7_7:                                # %.loopexit
	callq	newcell
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	leaq	(,%rbx,8), %rdi
	callq	must_malloc
	movq	%rax, 16(%r15)
	movl	$8, %esi
	movq	%rax, %rdi
	movq	%rbx, %rdx
	jmp	.LBB7_8
.LBB7_10:
	leaq	8(%rsp), %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fread
	movq	8(%rsp), %r14
	cvtsi2sdq	%r14, %xmm0
	movq	bashnum(%rip), %rdi
	movsd	%xmm0, 8(%rdi)
	xorl	%esi, %esi
	callq	cons_array
	movq	%rax, %r15
	testq	%r14, %r14
	jle	.LBB7_13
# BB#11:                                # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_12:                               # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	fast_read
	movq	16(%r15), %rcx
	movq	%rax, (%rcx,%rbx,8)
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB7_12
	jmp	.LBB7_13
.LBB7_14:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	errswitch               # TAILCALL
.LBB7_5:
	leaq	8(%rsp), %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fread
	movq	8(%rsp), %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r12
	movl	$18, %edi
	callq	newcell
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	movq	%rbx, %rdi
	callq	must_malloc
	movq	%rax, 16(%r15)
	movl	$1, %edx
	movq	%rax, %rdi
	movq	%rbx, %rsi
.LBB7_8:                                # %.loopexit
	movq	%r14, %rcx
	callq	fread
	movq	%r12, %rdi
	callq	no_interrupt
.LBB7_13:                               # %.loopexit
	movq	%r15, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	array_fast_read, .Lfunc_end7-array_fast_read
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_2
	.quad	.LBB7_6
	.quad	.LBB7_9
	.quad	.LBB7_10
	.quad	.LBB7_14
	.quad	.LBB7_5

	.text
	.globl	array_equal
	.p2align	4, 0x90
	.type	array_equal,@function
array_equal:                            # @array_equal
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 48
.Lcfi58:
	.cfi_offset %rbx, -48
.Lcfi59:
	.cfi_offset %r12, -40
.Lcfi60:
	.cfi_offset %r13, -32
.Lcfi61:
	.cfi_offset %r14, -24
.Lcfi62:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r13
	testq	%r13, %r13
	je	.LBB8_5
# BB#1:
	movswl	2(%r13), %eax
	addl	$-13, %eax
	cmpl	$5, %eax
	ja	.LBB8_5
# BB#2:
	jmpq	*.LJTI8_0(,%rax,8)
.LBB8_3:
	movq	8(%r13), %rdx
	cmpq	8(%r15), %rdx
	jne	.LBB8_23
# BB#4:
	movq	16(%r13), %rdi
	movq	16(%r15), %rsi
	callq	memcmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	cmoveq	sym_t(%rip), %rcx
	movq	%rcx, %rax
	jmp	.LBB8_25
.LBB8_5:                                # %.thread
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	errswitch               # TAILCALL
.LBB8_6:
	movq	8(%r13), %rax
	cmpq	8(%r15), %rax
	jne	.LBB8_23
# BB#7:                                 # %.preheader
	testq	%rax, %rax
	jle	.LBB8_11
# BB#8:                                 # %.lr.ph
	movq	16(%r13), %rcx
	movq	16(%r15), %rdx
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB8_9:                                # =>This Inner Loop Header: Depth=1
	movsd	(%rcx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	ucomisd	(%rdx,%rsi,8), %xmm0
	jne	.LBB8_24
	jp	.LBB8_24
# BB#10:                                #   in Loop: Header=BB8_9 Depth=1
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB8_9
.LBB8_11:                               # %._crit_edge
	movq	sym_t(%rip), %r14
	jmp	.LBB8_24
.LBB8_12:
	movq	8(%r13), %rdx
	cmpq	8(%r15), %rdx
	jne	.LBB8_23
# BB#13:
	movq	16(%r13), %rdi
	movq	16(%r15), %rsi
	shlq	$3, %rdx
	callq	memcmp
	xorl	%r14d, %r14d
	testl	%eax, %eax
	cmoveq	sym_t(%rip), %r14
	jmp	.LBB8_24
.LBB8_14:
	movq	8(%r13), %r12
	cmpq	8(%r15), %r12
	jne	.LBB8_23
# BB#15:                                # %.preheader39
	testq	%r12, %r12
	jle	.LBB8_19
# BB#16:                                # %.lr.ph43
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_17:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r13), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	16(%r15), %rax
	movq	(%rax,%rbx,8), %rsi
	callq	equal
	testq	%rax, %rax
	je	.LBB8_24
# BB#18:                                #   in Loop: Header=BB8_17 Depth=1
	incq	%rbx
	cmpq	%r12, %rbx
	jl	.LBB8_17
.LBB8_19:                               # %._crit_edge44
	movq	sym_t(%rip), %r14
	jmp	.LBB8_24
.LBB8_23:
	xorl	%r14d, %r14d
.LBB8_24:                               # %.loopexit
	movq	%r14, %rax
.LBB8_25:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	array_equal, .Lfunc_end8-array_equal
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI8_0:
	.quad	.LBB8_3
	.quad	.LBB8_6
	.quad	.LBB8_12
	.quad	.LBB8_14
	.quad	.LBB8_5
	.quad	.LBB8_3

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI9_0:
	.quad	4890909195324358656     # double 9.2233720368547758E+18
	.text
	.globl	array_sxhash
	.p2align	4, 0x90
	.type	array_sxhash,@function
array_sxhash:                           # @array_sxhash
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi65:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 48
.Lcfi68:
	.cfi_offset %rbx, -48
.Lcfi69:
	.cfi_offset %r12, -40
.Lcfi70:
	.cfi_offset %r13, -32
.Lcfi71:
	.cfi_offset %r14, -24
.Lcfi72:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB9_23
# BB#1:
	movswl	2(%r12), %eax
	addl	$-13, %eax
	cmpl	$5, %eax
	ja	.LBB9_23
# BB#2:
	jmpq	*.LJTI9_0(,%rax,8)
.LBB9_3:
	movq	8(%r12), %rcx
	testq	%rcx, %rcx
	jle	.LBB9_24
# BB#4:                                 # %.lr.ph.preheader
	movq	16(%r12), %rsi
	testb	$1, %cl
	jne	.LBB9_6
# BB#5:
	xorl	%edx, %edx
	xorl	%eax, %eax
	cmpq	$1, %rcx
	je	.LBB9_25
	jmp	.LBB9_8
.LBB9_23:                               # %.thread
	callq	errswitch
.LBB9_24:                               # %.loopexit
	xorl	%edx, %edx
.LBB9_25:                               # %.loopexit
	movq	%rdx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB9_17:
	movq	8(%r12), %rsi
	testq	%rsi, %rsi
	jle	.LBB9_24
# BB#18:                                # %.lr.ph73.preheader
	movq	16(%r12), %rdi
	xorl	%edx, %edx
	movsd	.LCPI9_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movabsq	$-9223372036854775808, %rbx # imm = 0x8000000000000000
	.p2align	4, 0x90
.LBB9_19:                               # %.lr.ph73
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rax
	shlq	$4, %rax
	leaq	1(%rdx,%rax), %rcx
	movsd	(%rdi), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	subsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %rdx
	xorq	%rbx, %rdx
	cvttsd2si	%xmm1, %rax
	ucomisd	%xmm1, %xmm0
	cmovbeq	%rdx, %rax
	xorl	%edx, %edx
	divq	%r14
	xorq	%rdx, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r14
	addq	$8, %rdi
	decq	%rsi
	jne	.LBB9_19
	jmp	.LBB9_25
.LBB9_10:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	jle	.LBB9_24
# BB#11:                                # %.lr.ph68.preheader
	movq	16(%r12), %rbx
	testb	$1, %dil
	jne	.LBB9_13
# BB#12:
	xorl	%edx, %edx
	xorl	%eax, %eax
	cmpq	$1, %rdi
	je	.LBB9_25
	jmp	.LBB9_15
.LBB9_20:
	movq	8(%r12), %r15
	testq	%r15, %r15
	jle	.LBB9_24
# BB#21:                                # %.lr.ph77
	xorl	%edx, %edx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_22:                               # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rax
	shlq	$4, %rax
	leaq	1(%rdx,%rax), %r13
	movq	16(%r12), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	%r14, %rsi
	callq	c_sxhash
	xorq	%r13, %rax
	cqto
	idivq	%r14
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB9_22
	jmp	.LBB9_25
.LBB9_6:                                # %.lr.ph.prol
	movzbl	(%rsi), %eax
	xorq	$1, %rax
	xorl	%edx, %edx
	idivq	%r14
	incq	%rsi
	movl	$1, %eax
	cmpq	$1, %rcx
	je	.LBB9_25
.LBB9_8:                                # %.lr.ph.preheader.new
	subq	%rax, %rcx
	.p2align	4, 0x90
.LBB9_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rax
	shlq	$4, %rax
	leaq	1(%rdx,%rax), %rdx
	movzbl	(%rsi), %eax
	xorq	%rdx, %rax
	cqto
	idivq	%r14
	movq	%rdx, %rax
	shlq	$4, %rax
	leaq	1(%rdx,%rax), %rdx
	movzbl	1(%rsi), %eax
	xorq	%rdx, %rax
	cqto
	idivq	%r14
	addq	$2, %rsi
	addq	$-2, %rcx
	jne	.LBB9_9
	jmp	.LBB9_25
.LBB9_13:                               # %.lr.ph68.prol
	movq	(%rbx), %rax
	xorl	%edx, %edx
	divq	%r14
	movq	%rdx, %rax
	xorq	$1, %rax
	xorl	%edx, %edx
	divq	%r14
	addq	$8, %rbx
	movl	$1, %eax
	cmpq	$1, %rdi
	je	.LBB9_25
.LBB9_15:                               # %.lr.ph68.preheader.new
	subq	%rax, %rdi
	.p2align	4, 0x90
.LBB9_16:                               # %.lr.ph68
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rax
	shlq	$4, %rax
	leaq	1(%rdx,%rax), %rcx
	movq	(%rbx), %rax
	movq	8(%rbx), %rsi
	xorl	%edx, %edx
	divq	%r14
	xorq	%rdx, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r14
	movq	%rdx, %rax
	shlq	$4, %rax
	leaq	1(%rdx,%rax), %rcx
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r14
	xorq	%rdx, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r14
	addq	$16, %rbx
	addq	$-2, %rdi
	jne	.LBB9_16
	jmp	.LBB9_25
.Lfunc_end9:
	.size	array_sxhash, .Lfunc_end9-array_sxhash
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI9_0:
	.quad	.LBB9_3
	.quad	.LBB9_17
	.quad	.LBB9_10
	.quad	.LBB9_20
	.quad	.LBB9_23
	.quad	.LBB9_3

	.text
	.globl	init_storage_a
	.p2align	4, 0x90
	.type	init_storage_a,@function
init_storage_a:                         # @init_storage_a
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 16
	movl	$bashnum, %edi
	callq	gc_protect
	movl	$2, %edi
	callq	newcell
	movq	%rax, bashnum(%rip)
	movq	%rsp, %r9
	movl	$13, %edi
	movl	$array_gc_relocate, %esi
	movl	$array_gc_mark, %edx
	movl	$array_gc_scan, %ecx
	movl	$array_gc_free, %r8d
	callq	set_gc_hooks
	movl	$13, %edi
	movl	$array_prin1, %esi
	callq	set_print_hooks
	movl	$13, %edi
	callq	get_user_type_hooks
	movq	$array_fast_print, 56(%rax)
	movq	$array_fast_read, 64(%rax)
	movq	$array_equal, 72(%rax)
	movq	$array_sxhash, 48(%rax)
	movq	%rsp, %r9
	movl	$14, %edi
	movl	$array_gc_relocate, %esi
	movl	$array_gc_mark, %edx
	movl	$array_gc_scan, %ecx
	movl	$array_gc_free, %r8d
	callq	set_gc_hooks
	movl	$14, %edi
	movl	$array_prin1, %esi
	callq	set_print_hooks
	movl	$14, %edi
	callq	get_user_type_hooks
	movq	$array_fast_print, 56(%rax)
	movq	$array_fast_read, 64(%rax)
	movq	$array_equal, 72(%rax)
	movq	$array_sxhash, 48(%rax)
	movq	%rsp, %r9
	movl	$15, %edi
	movl	$array_gc_relocate, %esi
	movl	$array_gc_mark, %edx
	movl	$array_gc_scan, %ecx
	movl	$array_gc_free, %r8d
	callq	set_gc_hooks
	movl	$15, %edi
	movl	$array_prin1, %esi
	callq	set_print_hooks
	movl	$15, %edi
	callq	get_user_type_hooks
	movq	$array_fast_print, 56(%rax)
	movq	$array_fast_read, 64(%rax)
	movq	$array_equal, 72(%rax)
	movq	$array_sxhash, 48(%rax)
	movq	%rsp, %r9
	movl	$16, %edi
	movl	$array_gc_relocate, %esi
	movl	$array_gc_mark, %edx
	movl	$array_gc_scan, %ecx
	movl	$array_gc_free, %r8d
	callq	set_gc_hooks
	movl	$16, %edi
	movl	$array_prin1, %esi
	callq	set_print_hooks
	movl	$16, %edi
	callq	get_user_type_hooks
	movq	$array_fast_print, 56(%rax)
	movq	$array_fast_read, 64(%rax)
	movq	$array_equal, 72(%rax)
	movq	$array_sxhash, 48(%rax)
	movq	%rsp, %r9
	movl	$18, %edi
	movl	$array_gc_relocate, %esi
	movl	$array_gc_mark, %edx
	movl	$array_gc_scan, %ecx
	movl	$array_gc_free, %r8d
	callq	set_gc_hooks
	movl	$18, %edi
	movl	$array_prin1, %esi
	callq	set_print_hooks
	movl	$18, %edi
	callq	get_user_type_hooks
	movq	$array_fast_print, 56(%rax)
	movq	$array_fast_read, 64(%rax)
	movq	$array_equal, 72(%rax)
	movq	$array_sxhash, 48(%rax)
	popq	%rax
	retq
.Lfunc_end10:
	.size	init_storage_a, .Lfunc_end10-init_storage_a
	.cfi_endproc

	.globl	strcons
	.p2align	4, 0x90
	.type	strcons,@function
strcons:                                # @strcons
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 48
.Lcfi79:
	.cfi_offset %rbx, -40
.Lcfi80:
	.cfi_offset %r12, -32
.Lcfi81:
	.cfi_offset %r14, -24
.Lcfi82:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r15
	movw	$13, 2(%r15)
	cmpq	$-1, %rbx
	jne	.LBB11_2
# BB#1:
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rbx
.LBB11_2:
	leaq	1(%rbx), %rdi
	callq	must_malloc
	movq	%rax, 16(%r15)
	movq	%rbx, 8(%r15)
	testq	%r12, %r12
	je	.LBB11_4
# BB#3:
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	16(%r15), %rax
.LBB11_4:
	movb	$0, (%rax,%rbx)
	movq	%r14, %rdi
	callq	no_interrupt
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	strcons, .Lfunc_end11-strcons
	.cfi_endproc

	.globl	rfs_getc
	.p2align	4, 0x90
	.type	rfs_getc,@function
rfs_getc:                               # @rfs_getc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rcx
	movzbl	(%rcx), %eax
	testl	%eax, %eax
	je	.LBB12_1
# BB#2:
	incq	%rcx
	movq	%rcx, (%rdi)
	retq
.LBB12_1:
	movl	$-1, %eax
	retq
.Lfunc_end12:
	.size	rfs_getc, .Lfunc_end12-rfs_getc
	.cfi_endproc

	.globl	rfs_ungetc
	.p2align	4, 0x90
	.type	rfs_ungetc,@function
rfs_ungetc:                             # @rfs_ungetc
	.cfi_startproc
# BB#0:
	decq	(%rsi)
	retq
.Lfunc_end13:
	.size	rfs_ungetc, .Lfunc_end13-rfs_ungetc
	.cfi_endproc

	.globl	read_from_string
	.p2align	4, 0x90
	.type	read_from_string,@function
read_from_string:                       # @read_from_string
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi83:
	.cfi_def_cfa_offset 48
	callq	get_c_string
	movq	%rax, 8(%rsp)
	movq	$rfs_getc, 16(%rsp)
	movq	$rfs_ungetc, 24(%rsp)
	leaq	8(%rsp), %rax
	movq	%rax, 32(%rsp)
	leaq	16(%rsp), %rdi
	callq	readtl
	addq	$40, %rsp
	retq
.Lfunc_end14:
	.size	read_from_string, .Lfunc_end14-read_from_string
	.cfi_endproc

	.globl	pts_puts
	.p2align	4, 0x90
	.type	pts_puts,@function
pts_puts:                               # @pts_puts
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi90:
	.cfi_def_cfa_offset 64
.Lcfi91:
	.cfi_offset %rbx, -56
.Lcfi92:
	.cfi_offset %r12, -48
.Lcfi93:
	.cfi_offset %r13, -40
.Lcfi94:
	.cfi_offset %r14, -32
.Lcfi95:
	.cfi_offset %r15, -24
.Lcfi96:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, (%rsp)            # 8-byte Spill
	callq	strlen
	movq	%rax, %r12
	movq	16(%r15), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r13
	movq	8(%r15), %r14
	subq	%r13, %r14
	cmpq	%r14, %r12
	movq	%r14, %rbp
	cmovbq	%r12, %rbp
	addq	%r13, %rbx
	movq	%rbx, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rbp, %rdx
	callq	memcpy
	addq	%r13, %rbp
	cmpq	%r14, %r12
	movq	16(%r15), %rax
	movb	$0, (%rax,%rbp)
	jbe	.LBB15_2
# BB#1:
	movl	$.L.str.12, %edi
	xorl	%esi, %esi
	callq	err
.LBB15_2:
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	pts_puts, .Lfunc_end15-pts_puts
	.cfi_endproc

	.globl	err_wta_str
	.p2align	4, 0x90
	.type	err_wta_str,@function
err_wta_str:                            # @err_wta_str
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movl	$.L.str.13, %edi
	movq	%rax, %rsi
	jmp	err                     # TAILCALL
.Lfunc_end16:
	.size	err_wta_str, .Lfunc_end16-err_wta_str
	.cfi_endproc

	.globl	print_to_string
	.p2align	4, 0x90
	.type	print_to_string,@function
print_to_string:                        # @print_to_string
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi100:
	.cfi_def_cfa_offset 64
.Lcfi101:
	.cfi_offset %rbx, -32
.Lcfi102:
	.cfi_offset %r14, -24
.Lcfi103:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB17_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$13, %eax
	je	.LBB17_3
.LBB17_2:                               # %.critedge
	movl	$.L.str.13, %edi
	movq	%rbx, %rsi
	callq	err
.LBB17_3:
	movq	$0, 8(%rsp)
	movq	$pts_puts, 16(%rsp)
	movq	%rbx, 24(%rsp)
	testq	%r15, %r15
	jne	.LBB17_5
# BB#4:
	movq	16(%rbx), %rax
	movb	$0, (%rax)
.LBB17_5:
	leaq	8(%rsp), %rsi
	movq	%r14, %rdi
	callq	lprin1g
	movq	%rbx, %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	print_to_string, .Lfunc_end17-print_to_string
	.cfi_endproc

	.globl	aref1
	.p2align	4, 0x90
	.type	aref1,@function
aref1:                                  # @aref1
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -32
.Lcfi108:
	.cfi_offset %r14, -24
.Lcfi109:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB18_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB18_3
.LBB18_2:                               # %.critedge
	movl	$.L.str.14, %edi
	movq	%rbx, %rsi
	callq	err
.LBB18_3:
	cvttsd2si	8(%rbx), %r15
	testq	%r15, %r15
	jns	.LBB18_5
# BB#4:
	movl	$.L.str.15, %edi
	movq	%rbx, %rsi
	callq	err
.LBB18_5:
	testq	%r14, %r14
	je	.LBB18_25
# BB#6:
	movswl	2(%r14), %eax
	addl	$-13, %eax
	cmpl	$5, %eax
	ja	.LBB18_25
# BB#7:
	jmpq	*.LJTI18_0(,%rax,8)
.LBB18_8:
	cmpq	8(%r14), %r15
	jl	.LBB18_10
# BB#9:
	movl	$.L.str.16, %edi
	movq	%rbx, %rsi
	callq	err
.LBB18_10:
	movq	16(%r14), %rax
	movzbl	(%rax,%r15), %eax
	jmp	.LBB18_11
.LBB18_25:                              # %.thread
	movl	$.L.str.17, %edi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	err                     # TAILCALL
.LBB18_16:
	cmpq	8(%r14), %r15
	jl	.LBB18_18
# BB#17:
	movl	$.L.str.16, %edi
	movq	%rbx, %rsi
	callq	err
.LBB18_18:
	movq	16(%r14), %rax
	movsd	(%rax,%r15,8), %xmm0    # xmm0 = mem[0],zero
	jmp	.LBB18_12
.LBB18_19:
	cmpq	8(%r14), %r15
	jl	.LBB18_21
# BB#20:
	movl	$.L.str.16, %edi
	movq	%rbx, %rsi
	callq	err
.LBB18_21:
	movq	16(%r14), %rax
	cvtsi2sdq	(%rax,%r15,8), %xmm0
	jmp	.LBB18_12
.LBB18_22:
	cmpq	8(%r14), %r15
	jl	.LBB18_24
# BB#23:
	movl	$.L.str.16, %edi
	movq	%rbx, %rsi
	callq	err
.LBB18_24:
	movq	16(%r14), %rax
	movq	(%rax,%r15,8), %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB18_13:
	cmpq	8(%r14), %r15
	jl	.LBB18_15
# BB#14:
	movl	$.L.str.16, %edi
	movq	%rbx, %rsi
	callq	err
.LBB18_15:
	movq	16(%r14), %rax
	movsbl	(%rax,%r15), %eax
.LBB18_11:
	cvtsi2sdl	%eax, %xmm0
.LBB18_12:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	flocons                 # TAILCALL
.Lfunc_end18:
	.size	aref1, .Lfunc_end18-aref1
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI18_0:
	.quad	.LBB18_8
	.quad	.LBB18_16
	.quad	.LBB18_19
	.quad	.LBB18_22
	.quad	.LBB18_25
	.quad	.LBB18_13

	.text
	.globl	err1_aset1
	.p2align	4, 0x90
	.type	err1_aset1,@function
err1_aset1:                             # @err1_aset1
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movl	$.L.str.18, %edi
	movq	%rax, %rsi
	jmp	err                     # TAILCALL
.Lfunc_end19:
	.size	err1_aset1, .Lfunc_end19-err1_aset1
	.cfi_endproc

	.globl	err2_aset1
	.p2align	4, 0x90
	.type	err2_aset1,@function
err2_aset1:                             # @err2_aset1
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movl	$.L.str.19, %edi
	movq	%rax, %rsi
	jmp	err                     # TAILCALL
.Lfunc_end20:
	.size	err2_aset1, .Lfunc_end20-err2_aset1
	.cfi_endproc

	.globl	aset1
	.p2align	4, 0x90
	.type	aset1,@function
aset1:                                  # @aset1
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi114:
	.cfi_def_cfa_offset 48
.Lcfi115:
	.cfi_offset %rbx, -40
.Lcfi116:
	.cfi_offset %r12, -32
.Lcfi117:
	.cfi_offset %r14, -24
.Lcfi118:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%rbx, %rbx
	je	.LBB21_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB21_3
.LBB21_2:                               # %.thread
	movl	$.L.str.20, %edi
	movq	%rbx, %rsi
	callq	err
.LBB21_3:
	cvttsd2si	8(%rbx), %r12
	testq	%r12, %r12
	jns	.LBB21_5
# BB#4:
	movl	$.L.str.21, %edi
	movq	%rbx, %rsi
	callq	err
.LBB21_5:
	testq	%r15, %r15
	je	.LBB21_31
# BB#6:
	movswl	2(%r15), %eax
	addl	$-13, %eax
	cmpl	$5, %eax
	ja	.LBB21_31
# BB#7:
	jmpq	*.LJTI21_0(,%rax,8)
.LBB21_8:
	testq	%r14, %r14
	je	.LBB21_10
# BB#9:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB21_11
.LBB21_10:                              # %.critedge
	movl	$.L.str.19, %edi
	movq	%r14, %rsi
	callq	err
.LBB21_11:
	cmpq	8(%r15), %r12
	jl	.LBB21_13
# BB#12:
	movl	$.L.str.18, %edi
	movq	%rbx, %rsi
	callq	err
.LBB21_13:
	cvttsd2si	8(%r14), %eax
	movq	16(%r15), %rcx
	movb	%al, (%rcx,%r12)
	jmp	.LBB21_30
.LBB21_31:                              # %.thread51
	movl	$.L.str.22, %edi
	movq	%r15, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	err                     # TAILCALL
.LBB21_14:
	testq	%r14, %r14
	je	.LBB21_16
# BB#15:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB21_17
.LBB21_16:                              # %.critedge49
	movl	$.L.str.19, %edi
	movq	%r14, %rsi
	callq	err
.LBB21_17:
	cmpq	8(%r15), %r12
	jl	.LBB21_19
# BB#18:
	movl	$.L.str.18, %edi
	movq	%rbx, %rsi
	callq	err
.LBB21_19:
	movq	8(%r14), %rax
	jmp	.LBB21_20
.LBB21_21:
	testq	%r14, %r14
	je	.LBB21_23
# BB#22:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB21_24
.LBB21_23:                              # %.critedge50
	movl	$.L.str.19, %edi
	movq	%r14, %rsi
	callq	err
.LBB21_24:
	cmpq	8(%r15), %r12
	jl	.LBB21_26
# BB#25:
	movl	$.L.str.18, %edi
	movq	%rbx, %rsi
	callq	err
.LBB21_26:
	cvttsd2si	8(%r14), %rax
.LBB21_20:
	movq	16(%r15), %rcx
	movq	%rax, (%rcx,%r12,8)
	jmp	.LBB21_30
.LBB21_27:
	cmpq	8(%r15), %r12
	jl	.LBB21_29
# BB#28:
	movl	$.L.str.18, %edi
	movq	%rbx, %rsi
	callq	err
.LBB21_29:
	movq	16(%r15), %rax
	movq	%r14, (%rax,%r12,8)
.LBB21_30:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	aset1, .Lfunc_end21-aset1
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI21_0:
	.quad	.LBB21_8
	.quad	.LBB21_14
	.quad	.LBB21_21
	.quad	.LBB21_27
	.quad	.LBB21_31
	.quad	.LBB21_8

	.text
	.globl	arcons
	.p2align	4, 0x90
	.type	arcons,@function
arcons:                                 # @arcons
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi122:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi125:
	.cfi_def_cfa_offset 64
.Lcfi126:
	.cfi_offset %rbx, -56
.Lcfi127:
	.cfi_offset %r12, -48
.Lcfi128:
	.cfi_offset %r13, -40
.Lcfi129:
	.cfi_offset %r14, -32
.Lcfi130:
	.cfi_offset %r15, -24
.Lcfi131:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r12
	movq	%rdi, %r15
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %rbx
	movq	%rbx, %r13
	leaq	-13(%r15), %rax
	cmpq	$5, %rax
	ja	.LBB22_32
# BB#1:
	jmpq	*.LJTI22_0(,%rax,8)
.LBB22_3:
	movq	%r12, 8(%r13)
	leaq	(,%r12,8), %rbx
	movq	%rbx, %rdi
	callq	must_malloc
	movq	%rax, 16(%r13)
	testq	%rbp, %rbp
	je	.LBB22_33
# BB#4:
	testq	%r12, %r12
	jle	.LBB22_33
# BB#5:                                 # %.lr.ph
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rbx, %rdx
	callq	memset
	jmp	.LBB22_33
.LBB22_6:
	movq	%r12, 8(%r13)
	leaq	1(%r12), %rdi
	callq	must_malloc
	leaq	8(%rbx), %rcx
	addq	$16, %rbx
	movq	%rax, 16(%r13)
	movb	$0, (%rax,%r12)
	testq	%rbp, %rbp
	je	.LBB22_12
# BB#7:
	testq	%r12, %r12
	jle	.LBB22_12
# BB#8:                                 # %.lr.ph73.preheader
	leaq	-1(%r12), %rdx
	movq	%r12, %rsi
	xorl	%eax, %eax
	andq	$7, %rsi
	je	.LBB22_10
	.p2align	4, 0x90
.LBB22_9:                               # %.lr.ph73.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movb	$32, (%rdi,%rax)
	incq	%rax
	cmpq	%rax, %rsi
	jne	.LBB22_9
.LBB22_10:                              # %.lr.ph73.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB22_12
	.p2align	4, 0x90
.LBB22_11:                              # %.lr.ph73
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movb	$32, (%rdx,%rax)
	movq	(%rbx), %rdx
	movb	$32, 1(%rdx,%rax)
	movq	(%rbx), %rdx
	movb	$32, 2(%rdx,%rax)
	movq	(%rbx), %rdx
	movb	$32, 3(%rdx,%rax)
	movq	(%rbx), %rdx
	movb	$32, 4(%rdx,%rax)
	movq	(%rbx), %rdx
	movb	$32, 5(%rdx,%rax)
	movq	(%rbx), %rdx
	movb	$32, 6(%rdx,%rax)
	movq	(%rbx), %rdx
	movb	$32, 7(%rdx,%rax)
	addq	$8, %rax
	cmpq	%rax, %r12
	jne	.LBB22_11
	jmp	.LBB22_12
.LBB22_22:
	movq	%r12, 8(%r13)
	leaq	(,%r12,8), %rdi
	callq	must_malloc
	movq	%rax, 16(%r13)
	testq	%r12, %r12
	jle	.LBB22_33
# BB#23:                                # %.lr.ph75.preheader
	movq	$0, (%rax)
	cmpq	$1, %r12
	je	.LBB22_33
# BB#24:                                # %.lr.ph75..lr.ph75_crit_edge.preheader
	leal	7(%r12), %edx
	leaq	-2(%r12), %rcx
	andq	$7, %rdx
	je	.LBB22_25
# BB#26:                                # %.lr.ph75..lr.ph75_crit_edge.prol.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB22_27:                              # %.lr.ph75..lr.ph75_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r13), %rsi
	movq	$0, 8(%rsi,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB22_27
# BB#28:                                # %.lr.ph75..lr.ph75_crit_edge.prol.loopexit.unr-lcssa
	incq	%rax
	cmpq	$7, %rcx
	jae	.LBB22_30
	jmp	.LBB22_33
.LBB22_32:
	callq	errswitch
	jmp	.LBB22_33
.LBB22_2:                               # %..loopexit65_crit_edge
	leaq	8(%rbx), %rcx
	addq	$16, %rbx
.LBB22_12:                              # %.loopexit65
	movq	%r12, (%rcx)
	movq	%r12, %rdi
	callq	must_malloc
	movq	%rax, (%rbx)
	testq	%rbp, %rbp
	je	.LBB22_33
# BB#13:                                # %.loopexit65
	testq	%r12, %r12
	jle	.LBB22_33
# BB#14:                                # %.lr.ph71.preheader
	movb	$0, (%rax)
	cmpq	$1, %r12
	je	.LBB22_33
# BB#15:                                # %.lr.ph71..lr.ph71_crit_edge.preheader
	leal	7(%r12), %edx
	leaq	-2(%r12), %rcx
	andq	$7, %rdx
	je	.LBB22_16
# BB#17:                                # %.lr.ph71..lr.ph71_crit_edge.prol.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB22_18:                              # %.lr.ph71..lr.ph71_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movb	$0, 1(%rsi,%rax)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB22_18
# BB#19:                                # %.lr.ph71..lr.ph71_crit_edge.prol.loopexit.unr-lcssa
	incq	%rax
	cmpq	$7, %rcx
	jae	.LBB22_21
	jmp	.LBB22_33
.LBB22_16:
	movl	$1, %eax
	cmpq	$7, %rcx
	jb	.LBB22_33
	.p2align	4, 0x90
.LBB22_21:                              # %.lr.ph71..lr.ph71_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movb	$0, (%rcx,%rax)
	movq	(%rbx), %rcx
	movb	$0, 1(%rcx,%rax)
	movq	(%rbx), %rcx
	movb	$0, 2(%rcx,%rax)
	movq	(%rbx), %rcx
	movb	$0, 3(%rcx,%rax)
	movq	(%rbx), %rcx
	movb	$0, 4(%rcx,%rax)
	movq	(%rbx), %rcx
	movb	$0, 5(%rcx,%rax)
	movq	(%rbx), %rcx
	movb	$0, 6(%rcx,%rax)
	movq	(%rbx), %rcx
	movb	$0, 7(%rcx,%rax)
	addq	$8, %rax
	cmpq	%rax, %r12
	jne	.LBB22_21
	jmp	.LBB22_33
.LBB22_25:
	movl	$1, %eax
	cmpq	$7, %rcx
	jb	.LBB22_33
.LBB22_30:                              # %.lr.ph75..lr.ph75_crit_edge.preheader.new
	subq	%rax, %r12
	shlq	$3, %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB22_31:                              # %.lr.ph75..lr.ph75_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r13), %rdx
	addq	%rax, %rdx
	movq	$0, (%rdx,%rcx,8)
	movq	16(%r13), %rdx
	addq	%rax, %rdx
	movq	$0, 8(%rdx,%rcx,8)
	movq	16(%r13), %rdx
	addq	%rax, %rdx
	movq	$0, 16(%rdx,%rcx,8)
	movq	16(%r13), %rdx
	addq	%rax, %rdx
	movq	$0, 24(%rdx,%rcx,8)
	movq	16(%r13), %rdx
	addq	%rax, %rdx
	movq	$0, 32(%rdx,%rcx,8)
	movq	16(%r13), %rdx
	addq	%rax, %rdx
	movq	$0, 40(%rdx,%rcx,8)
	movq	16(%r13), %rdx
	addq	%rax, %rdx
	movq	$0, 48(%rdx,%rcx,8)
	movq	16(%r13), %rdx
	addq	%rax, %rdx
	movq	$0, 56(%rdx,%rcx,8)
	addq	$8, %rcx
	cmpq	%rcx, %r12
	jne	.LBB22_31
.LBB22_33:                              # %.loopexit
	movw	%r15w, 2(%r13)
	movq	%r14, %rdi
	callq	no_interrupt
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	arcons, .Lfunc_end22-arcons
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI22_0:
	.quad	.LBB22_6
	.quad	.LBB22_3
	.quad	.LBB22_3
	.quad	.LBB22_22
	.quad	.LBB22_32
	.quad	.LBB22_2

	.text
	.globl	mallocl
	.p2align	4, 0x90
	.type	mallocl,@function
mallocl:                                # @mallocl
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi136:
	.cfi_def_cfa_offset 48
.Lcfi137:
	.cfi_offset %rbx, -40
.Lcfi138:
	.cfi_offset %r12, -32
.Lcfi139:
	.cfi_offset %r14, -24
.Lcfi140:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	%rsi, %r12
	shrq	$3, %r12
	andl	$7, %esi
	cmpq	$1, %rsi
	sbbq	$-1, %r12
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r15
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %rbx
	movq	%r12, 8(%rbx)
	leaq	(,%r12,8), %rdi
	callq	must_malloc
	movq	%rax, 16(%rbx)
	movw	$15, 2(%rbx)
	movq	%r15, %rdi
	callq	no_interrupt
	movq	16(%rbx), %rax
	movq	%rax, (%r14)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end23:
	.size	mallocl, .Lfunc_end23-mallocl
	.cfi_endproc

	.globl	cons_array
	.p2align	4, 0x90
	.type	cons_array,@function
cons_array:                             # @cons_array
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi143:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi145:
	.cfi_def_cfa_offset 48
.Lcfi146:
	.cfi_offset %rbx, -40
.Lcfi147:
	.cfi_offset %r12, -32
.Lcfi148:
	.cfi_offset %r14, -24
.Lcfi149:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB24_39
# BB#1:
	movzwl	2(%rax), %ecx
	cmpl	$2, %ecx
	jne	.LBB24_39
# BB#2:
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	xorps	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jbe	.LBB24_3
.LBB24_39:                              # %.critedge
	movl	$.L.str.23, %edi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	err                     # TAILCALL
.LBB24_3:
	cvttsd2si	%xmm0, %r15
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r12
	movl	$.L.str.24, %edi
	callq	cintern
	cmpq	%rbx, %rax
	je	.LBB24_4
# BB#7:
	movl	$.L.str.25, %edi
	callq	cintern
	cmpq	%rbx, %rax
	je	.LBB24_8
# BB#9:
	movl	$.L.str.26, %edi
	callq	cintern
	cmpq	%rbx, %rax
	je	.LBB24_10
# BB#15:
	movl	$.L.str.27, %edi
	callq	cintern
	cmpq	%rbx, %rax
	je	.LBB24_16
# BB#25:
	movl	$.L.str.28, %edi
	callq	cintern
	testq	%rbx, %rbx
	je	.LBB24_27
# BB#26:
	cmpq	%rbx, %rax
	je	.LBB24_27
# BB#37:
	movl	$.L.str.29, %edi
	movq	%rbx, %rsi
	callq	err
	jmp	.LBB24_38
.LBB24_4:
	movw	$14, 2(%r12)
	jmp	.LBB24_5
.LBB24_8:
	movw	$15, 2(%r12)
.LBB24_5:
	movq	%r15, 8(%r12)
	leaq	(,%r15,8), %rbx
	movq	%rbx, %rdi
	callq	must_malloc
	movq	%rax, 16(%r12)
	testq	%r15, %r15
	jle	.LBB24_38
# BB#6:                                 # %.lr.ph
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rbx, %rdx
	callq	memset
.LBB24_38:
	movq	%r14, %rdi
	callq	no_interrupt
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB24_10:
	movw	$13, 2(%r12)
	movq	%r15, 8(%r12)
	leaq	1(%r15), %rdi
	callq	must_malloc
	movq	%rax, 16(%r12)
	movb	$0, (%rax,%r15)
	testq	%r15, %r15
	jle	.LBB24_38
# BB#11:                                # %.lr.ph83.preheader
	leaq	-1(%r15), %rcx
	movq	%r15, %rdx
	xorl	%eax, %eax
	andq	$7, %rdx
	je	.LBB24_13
	.p2align	4, 0x90
.LBB24_12:                              # %.lr.ph83.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rsi
	movb	$32, (%rsi,%rax)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB24_12
.LBB24_13:                              # %.lr.ph83.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB24_38
	.p2align	4, 0x90
.LBB24_14:                              # %.lr.ph83
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rcx
	movb	$32, (%rcx,%rax)
	movq	16(%r12), %rcx
	movb	$32, 1(%rcx,%rax)
	movq	16(%r12), %rcx
	movb	$32, 2(%rcx,%rax)
	movq	16(%r12), %rcx
	movb	$32, 3(%rcx,%rax)
	movq	16(%r12), %rcx
	movb	$32, 4(%rcx,%rax)
	movq	16(%r12), %rcx
	movb	$32, 5(%rcx,%rax)
	movq	16(%r12), %rcx
	movb	$32, 6(%rcx,%rax)
	movq	16(%r12), %rcx
	movb	$32, 7(%rcx,%rax)
	addq	$8, %rax
	cmpq	%rax, %r15
	jne	.LBB24_14
	jmp	.LBB24_38
.LBB24_16:
	movw	$18, 2(%r12)
	movq	%r15, 8(%r12)
	movq	%r15, %rdi
	callq	must_malloc
	movq	%rax, 16(%r12)
	testq	%r15, %r15
	jle	.LBB24_38
# BB#17:                                # %.lr.ph85.preheader
	movb	$0, (%rax)
	cmpq	$1, %r15
	je	.LBB24_38
# BB#18:                                # %.lr.ph85..lr.ph85_crit_edge.preheader
	leal	7(%r15), %edx
	leaq	-2(%r15), %rcx
	andq	$7, %rdx
	je	.LBB24_19
# BB#20:                                # %.lr.ph85..lr.ph85_crit_edge.prol.preheader
	xorl	%eax, %eax
.LBB24_21:                              # %.lr.ph85..lr.ph85_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rsi
	movb	$0, 1(%rsi,%rax)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB24_21
# BB#22:                                # %.lr.ph85..lr.ph85_crit_edge.prol.loopexit.unr-lcssa
	incq	%rax
	cmpq	$7, %rcx
	jae	.LBB24_24
	jmp	.LBB24_38
.LBB24_27:
	movw	$16, 2(%r12)
	movq	%r15, 8(%r12)
	leaq	(,%r15,8), %rdi
	callq	must_malloc
	movq	%rax, 16(%r12)
	testq	%r15, %r15
	jle	.LBB24_38
# BB#28:                                # %.lr.ph87.preheader
	movq	$0, (%rax)
	cmpq	$1, %r15
	je	.LBB24_38
# BB#29:                                # %.lr.ph87..lr.ph87_crit_edge.preheader
	leal	7(%r15), %edx
	leaq	-2(%r15), %rcx
	andq	$7, %rdx
	je	.LBB24_30
# BB#31:                                # %.lr.ph87..lr.ph87_crit_edge.prol.preheader
	xorl	%eax, %eax
.LBB24_32:                              # %.lr.ph87..lr.ph87_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rsi
	movq	$0, 8(%rsi,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB24_32
# BB#33:                                # %.lr.ph87..lr.ph87_crit_edge.prol.loopexit.unr-lcssa
	incq	%rax
	cmpq	$7, %rcx
	jb	.LBB24_38
	jmp	.LBB24_35
.LBB24_19:
	movl	$1, %eax
	cmpq	$7, %rcx
	jb	.LBB24_38
.LBB24_24:                              # %.lr.ph85..lr.ph85_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rcx
	movb	$0, (%rcx,%rax)
	movq	16(%r12), %rcx
	movb	$0, 1(%rcx,%rax)
	movq	16(%r12), %rcx
	movb	$0, 2(%rcx,%rax)
	movq	16(%r12), %rcx
	movb	$0, 3(%rcx,%rax)
	movq	16(%r12), %rcx
	movb	$0, 4(%rcx,%rax)
	movq	16(%r12), %rcx
	movb	$0, 5(%rcx,%rax)
	movq	16(%r12), %rcx
	movb	$0, 6(%rcx,%rax)
	movq	16(%r12), %rcx
	movb	$0, 7(%rcx,%rax)
	addq	$8, %rax
	cmpq	%rax, %r15
	jne	.LBB24_24
	jmp	.LBB24_38
.LBB24_30:
	movl	$1, %eax
	cmpq	$7, %rcx
	jb	.LBB24_38
.LBB24_35:                              # %.lr.ph87..lr.ph87_crit_edge.preheader.new
	subq	%rax, %r15
	shlq	$3, %rax
	xorl	%ecx, %ecx
.LBB24_36:                              # %.lr.ph87..lr.ph87_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rdx
	addq	%rax, %rdx
	movq	$0, (%rdx,%rcx,8)
	movq	16(%r12), %rdx
	addq	%rax, %rdx
	movq	$0, 8(%rdx,%rcx,8)
	movq	16(%r12), %rdx
	addq	%rax, %rdx
	movq	$0, 16(%rdx,%rcx,8)
	movq	16(%r12), %rdx
	addq	%rax, %rdx
	movq	$0, 24(%rdx,%rcx,8)
	movq	16(%r12), %rdx
	addq	%rax, %rdx
	movq	$0, 32(%rdx,%rcx,8)
	movq	16(%r12), %rdx
	addq	%rax, %rdx
	movq	$0, 40(%rdx,%rcx,8)
	movq	16(%r12), %rdx
	addq	%rax, %rdx
	movq	$0, 48(%rdx,%rcx,8)
	movq	16(%r12), %rdx
	addq	%rax, %rdx
	movq	$0, 56(%rdx,%rcx,8)
	addq	$8, %rcx
	cmpq	%rcx, %r15
	jne	.LBB24_36
	jmp	.LBB24_38
.Lfunc_end24:
	.size	cons_array, .Lfunc_end24-cons_array
	.cfi_endproc

	.globl	string_append
	.p2align	4, 0x90
	.type	string_append,@function
string_append:                          # @string_append
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi150:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi151:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi152:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi153:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi154:
	.cfi_def_cfa_offset 48
.Lcfi155:
	.cfi_offset %rbx, -48
.Lcfi156:
	.cfi_offset %r12, -40
.Lcfi157:
	.cfi_offset %r13, -32
.Lcfi158:
	.cfi_offset %r14, -24
.Lcfi159:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	xorl	%r13d, %r13d
	testq	%r14, %r14
	je	.LBB25_3
# BB#1:                                 # %.lr.ph25.preheader
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB25_2:                               # %.lr.ph25
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	car
	movq	%rax, %rdi
	callq	get_c_string
	movq	%rax, %rdi
	callq	strlen
	addq	%rax, %r13
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB25_2
.LBB25_3:                               # %._crit_edge26
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r12
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r15
	movw	$13, 2(%r15)
	cmpq	$-1, %r13
	jne	.LBB25_5
# BB#4:
	xorl	%edi, %edi
	callq	strlen
	movq	%rax, %r13
.LBB25_5:                               # %strcons.exit
	leaq	1(%r13), %rdi
	callq	must_malloc
	movq	%rax, 16(%r15)
	movq	%r13, 8(%r15)
	movb	$0, (%rax,%r13)
	movq	%r12, %rdi
	callq	no_interrupt
	testq	%r14, %r14
	movq	16(%r15), %rbx
	movb	$0, (%rbx)
	je	.LBB25_7
	.p2align	4, 0x90
.LBB25_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	car
	movq	%rax, %rdi
	callq	get_c_string
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcat
	movq	%r14, %rdi
	callq	cdr
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB25_6
.LBB25_7:                               # %._crit_edge
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end25:
	.size	string_append, .Lfunc_end25-string_append
	.cfi_endproc

	.globl	bytes_append
	.p2align	4, 0x90
	.type	bytes_append,@function
bytes_append:                           # @bytes_append
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi162:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi163:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi164:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi165:
	.cfi_def_cfa_offset 64
.Lcfi166:
	.cfi_offset %rbx, -48
.Lcfi167:
	.cfi_offset %r12, -40
.Lcfi168:
	.cfi_offset %r13, -32
.Lcfi169:
	.cfi_offset %r14, -24
.Lcfi170:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	xorl	%r12d, %r12d
	testq	%r15, %r15
	je	.LBB26_3
# BB#1:                                 # %.lr.ph33.preheader
	leaq	8(%rsp), %r14
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB26_2:                               # %.lr.ph33
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	car
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	get_c_string_dim
	addq	8(%rsp), %r12
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB26_2
.LBB26_3:                               # %._crit_edge34
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %rbx
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r14
	movq	%r12, 8(%r14)
	movq	%r12, %rdi
	callq	must_malloc
	movq	%rax, 16(%r14)
	movw	$18, 2(%r14)
	movq	%rbx, %rdi
	callq	no_interrupt
	testq	%r15, %r15
	je	.LBB26_6
# BB#4:                                 # %.lr.ph.preheader
	movq	16(%r14), %r13
	xorl	%ebx, %ebx
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB26_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	car
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	get_c_string_dim
	leaq	(%r13,%rbx), %rdi
	movq	8(%rsp), %rdx
	movq	%rax, %rsi
	callq	memcpy
	addq	8(%rsp), %rbx
	movq	%r15, %rdi
	callq	cdr
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB26_5
.LBB26_6:                               # %._crit_edge
	movq	%r14, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end26:
	.size	bytes_append, .Lfunc_end26-bytes_append
	.cfi_endproc

	.globl	substring
	.p2align	4, 0x90
	.type	substring,@function
substring:                              # @substring
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi171:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi172:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi173:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi174:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi175:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi176:
	.cfi_def_cfa_offset 64
.Lcfi177:
	.cfi_offset %rbx, -48
.Lcfi178:
	.cfi_offset %r12, -40
.Lcfi179:
	.cfi_offset %r13, -32
.Lcfi180:
	.cfi_offset %r14, -24
.Lcfi181:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	leaq	8(%rsp), %rsi
	callq	get_c_string_dim
	movq	%rax, %r14
	testq	%r12, %r12
	je	.LBB27_2
# BB#1:
	movzwl	2(%r12), %eax
	cmpl	$2, %eax
	je	.LBB27_3
.LBB27_2:                               # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%r12, %rsi
	callq	err
.LBB27_3:                               # %get_c_long.exit
	cvttsd2si	8(%r12), %r13
	testq	%r15, %r15
	je	.LBB27_4
# BB#5:
	movzwl	2(%r15), %eax
	cmpl	$2, %eax
	je	.LBB27_7
# BB#6:                                 # %.critedge.i22
	movl	$.L.str.44, %edi
	movq	%r15, %rsi
	callq	err
.LBB27_7:                               # %get_c_long.exit23
	cvttsd2si	8(%r15), %rbx
	testq	%r13, %r13
	jns	.LBB27_9
	jmp	.LBB27_10
.LBB27_4:
	movq	8(%rsp), %rbx
	testq	%r13, %r13
	js	.LBB27_10
.LBB27_9:
	cmpq	%r13, %rbx
	jge	.LBB27_11
.LBB27_10:
	movl	$.L.str.30, %edi
	movq	%r12, %rsi
	callq	err
.LBB27_11:
	testq	%rbx, %rbx
	js	.LBB27_13
# BB#12:
	cmpq	8(%rsp), %rbx
	jle	.LBB27_14
.LBB27_13:
	movl	$.L.str.31, %edi
	movq	%r15, %rsi
	callq	err
.LBB27_14:
	subq	%r13, %rbx
	addq	%r13, %r14
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r15
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r12
	movw	$13, 2(%r12)
	cmpq	$-1, %rbx
	jne	.LBB27_16
# BB#15:
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
.LBB27_16:
	leaq	1(%rbx), %rdi
	callq	must_malloc
	movq	%rax, 16(%r12)
	movq	%rbx, 8(%r12)
	testq	%r14, %r14
	je	.LBB27_18
# BB#17:
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	16(%r12), %rax
.LBB27_18:                              # %strcons.exit
	movb	$0, (%rax,%rbx)
	movq	%r15, %rdi
	callq	no_interrupt
	movq	%r12, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end27:
	.size	substring, .Lfunc_end27-substring
	.cfi_endproc

	.globl	get_c_long
	.p2align	4, 0x90
	.type	get_c_long,@function
get_c_long:                             # @get_c_long
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi182:
	.cfi_def_cfa_offset 16
.Lcfi183:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB28_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB28_3
.LBB28_2:                               # %.critedge
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB28_3:
	cvttsd2si	8(%rbx), %rax
	popq	%rbx
	retq
.Lfunc_end28:
	.size	get_c_long, .Lfunc_end28-get_c_long
	.cfi_endproc

	.globl	string_search
	.p2align	4, 0x90
	.type	string_search,@function
string_search:                          # @string_search
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi184:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi185:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi186:
	.cfi_def_cfa_offset 32
.Lcfi187:
	.cfi_offset %rbx, -24
.Lcfi188:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	callq	get_c_string
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	strstr
	testq	%rax, %rax
	je	.LBB29_1
# BB#2:
	subq	%r14, %rax
	cvtsi2sdq	%rax, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	flocons                 # TAILCALL
.LBB29_1:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end29:
	.size	string_search, .Lfunc_end29-string_search
	.cfi_endproc

	.globl	string_trim
	.p2align	4, 0x90
	.type	string_trim,@function
string_trim:                            # @string_trim
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi189:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi190:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi191:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi192:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi193:
	.cfi_def_cfa_offset 48
.Lcfi194:
	.cfi_offset %rbx, -40
.Lcfi195:
	.cfi_offset %r12, -32
.Lcfi196:
	.cfi_offset %r14, -24
.Lcfi197:
	.cfi_offset %r15, -16
	movabsq	$4294977025, %r15       # imm = 0x100002601
	callq	get_c_string
	movq	%rax, %r14
	movb	(%r14), %al
	testb	%al, %al
	jne	.LBB30_2
	jmp	.LBB30_5
	.p2align	4, 0x90
.LBB30_4:                               #   in Loop: Header=BB30_2 Depth=1
	movzbl	1(%r14), %eax
	incq	%r14
	testb	%al, %al
	je	.LBB30_5
.LBB30_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbq	%al, %rax
	movl	%eax, %ecx
	movl	$1, %eax
	shlq	%cl, %rax
	cmpq	$63, %rcx
	ja	.LBB30_5
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB30_2 Depth=1
	andq	%r15, %rax
	jne	.LBB30_4
.LBB30_5:                               # %.critedge
	movq	%r14, %rdi
	callq	strlen
	addq	%r14, %rax
	.p2align	4, 0x90
.LBB30_6:                               # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	subq	%r14, %rbx
	jbe	.LBB30_9
# BB#7:                                 #   in Loop: Header=BB30_6 Depth=1
	movsbq	-1(%rax), %rcx
	movl	%ecx, %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	cmpq	$63, %rcx
	ja	.LBB30_9
# BB#8:                                 #   in Loop: Header=BB30_6 Depth=1
	decq	%rax
	andq	%r15, %rdx
	jne	.LBB30_6
.LBB30_9:                               # %.critedge20
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r15
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r12
	movw	$13, 2(%r12)
	cmpq	$-1, %rbx
	jne	.LBB30_11
# BB#10:
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
.LBB30_11:
	leaq	1(%rbx), %rdi
	callq	must_malloc
	movq	%rax, 16(%r12)
	movq	%rbx, 8(%r12)
	testq	%r14, %r14
	je	.LBB30_13
# BB#12:
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	16(%r12), %rax
.LBB30_13:                              # %strcons.exit
	movb	$0, (%rax,%rbx)
	movq	%r15, %rdi
	callq	no_interrupt
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end30:
	.size	string_trim, .Lfunc_end30-string_trim
	.cfi_endproc

	.globl	string_trim_left
	.p2align	4, 0x90
	.type	string_trim_left,@function
string_trim_left:                       # @string_trim_left
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi198:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi199:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi200:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi201:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi202:
	.cfi_def_cfa_offset 48
.Lcfi203:
	.cfi_offset %rbx, -40
.Lcfi204:
	.cfi_offset %r12, -32
.Lcfi205:
	.cfi_offset %r14, -24
.Lcfi206:
	.cfi_offset %r15, -16
	callq	get_c_string
	movq	%rax, %rbx
	movb	(%rbx), %cl
	testb	%cl, %cl
	je	.LBB31_5
# BB#1:                                 # %.lr.ph.preheader
	movabsq	$4294977025, %rax       # imm = 0x100002601
	.p2align	4, 0x90
.LBB31_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbq	%cl, %rcx
	movl	%ecx, %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	cmpq	$63, %rcx
	ja	.LBB31_5
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB31_2 Depth=1
	andq	%rax, %rdx
	je	.LBB31_5
# BB#4:                                 #   in Loop: Header=BB31_2 Depth=1
	movzbl	1(%rbx), %ecx
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB31_2
.LBB31_5:                               # %.critedge
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r14
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r15
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r12
	movw	$13, 2(%r12)
	cmpq	$-1, %r14
	jne	.LBB31_7
# BB#6:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r14
.LBB31_7:
	leaq	1(%r14), %rdi
	callq	must_malloc
	movq	%rax, 16(%r12)
	movq	%r14, 8(%r12)
	testq	%rbx, %rbx
	je	.LBB31_9
# BB#8:
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	memcpy
	movq	16(%r12), %rax
.LBB31_9:                               # %strcons.exit
	movb	$0, (%rax,%r14)
	movq	%r15, %rdi
	callq	no_interrupt
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end31:
	.size	string_trim_left, .Lfunc_end31-string_trim_left
	.cfi_endproc

	.globl	string_trim_right
	.p2align	4, 0x90
	.type	string_trim_right,@function
string_trim_right:                      # @string_trim_right
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi207:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi208:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi209:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi210:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi211:
	.cfi_def_cfa_offset 48
.Lcfi212:
	.cfi_offset %rbx, -40
.Lcfi213:
	.cfi_offset %r12, -32
.Lcfi214:
	.cfi_offset %r14, -24
.Lcfi215:
	.cfi_offset %r15, -16
	callq	get_c_string
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	strlen
	addq	%r14, %rax
	movabsq	$4294977025, %rdx       # imm = 0x100002601
	.p2align	4, 0x90
.LBB32_1:                               # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	subq	%r14, %rbx
	jbe	.LBB32_4
# BB#2:                                 #   in Loop: Header=BB32_1 Depth=1
	movsbq	-1(%rax), %rcx
	movl	%ecx, %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	cmpq	$63, %rcx
	ja	.LBB32_4
# BB#3:                                 #   in Loop: Header=BB32_1 Depth=1
	decq	%rax
	andq	%rdx, %rsi
	jne	.LBB32_1
.LBB32_4:                               # %.critedge
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r15
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r12
	movw	$13, 2(%r12)
	cmpq	$-1, %rbx
	jne	.LBB32_6
# BB#5:
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
.LBB32_6:
	leaq	1(%rbx), %rdi
	callq	must_malloc
	movq	%rax, 16(%r12)
	movq	%rbx, 8(%r12)
	testq	%r14, %r14
	je	.LBB32_8
# BB#7:
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	16(%r12), %rax
.LBB32_8:                               # %strcons.exit
	movb	$0, (%rax,%rbx)
	movq	%r15, %rdi
	callq	no_interrupt
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end32:
	.size	string_trim_right, .Lfunc_end32-string_trim_right
	.cfi_endproc

	.globl	string_upcase
	.p2align	4, 0x90
	.type	string_upcase,@function
string_upcase:                          # @string_upcase
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi216:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi217:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi218:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi219:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi220:
	.cfi_def_cfa_offset 48
.Lcfi221:
	.cfi_offset %rbx, -48
.Lcfi222:
	.cfi_offset %r12, -40
.Lcfi223:
	.cfi_offset %r13, -32
.Lcfi224:
	.cfi_offset %r14, -24
.Lcfi225:
	.cfi_offset %r15, -16
	callq	get_c_string
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r12
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r14
	movw	$13, 2(%r14)
	cmpq	$-1, %rbx
	movq	%rbx, %r13
	jne	.LBB33_2
# BB#1:
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r13
.LBB33_2:
	leaq	1(%r13), %rdi
	callq	must_malloc
	movq	%rax, 16(%r14)
	movq	%r13, 8(%r14)
	testq	%r15, %r15
	je	.LBB33_4
# BB#3:
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	memcpy
	movq	16(%r14), %rax
.LBB33_4:                               # %strcons.exit
	movb	$0, (%rax,%r13)
	movq	%r12, %rdi
	callq	no_interrupt
	movq	%r14, %rdi
	callq	get_c_string
	movq	%rax, %r15
	testq	%rbx, %rbx
	jle	.LBB33_10
# BB#5:                                 # %.lr.ph
	callq	__ctype_toupper_loc
	leaq	-1(%rbx), %r8
	movq	%rbx, %rsi
	xorl	%edx, %edx
	andq	$3, %rsi
	je	.LBB33_7
	.p2align	4, 0x90
.LBB33_6:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	movsbq	(%r15,%rdx), %rcx
	movzbl	(%rdi,%rcx,4), %ecx
	movb	%cl, (%r15,%rdx)
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB33_6
.LBB33_7:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB33_10
# BB#8:                                 # %.lr.ph.new
	subq	%rdx, %rbx
	leaq	3(%r15,%rdx), %rcx
	.p2align	4, 0x90
.LBB33_9:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdx
	movsbq	-3(%rcx), %rsi
	movzbl	(%rdx,%rsi,4), %edx
	movb	%dl, -3(%rcx)
	movq	(%rax), %rdx
	movsbq	-2(%rcx), %rsi
	movzbl	(%rdx,%rsi,4), %edx
	movb	%dl, -2(%rcx)
	movq	(%rax), %rdx
	movsbq	-1(%rcx), %rsi
	movzbl	(%rdx,%rsi,4), %edx
	movb	%dl, -1(%rcx)
	movq	(%rax), %rdx
	movsbq	(%rcx), %rsi
	movzbl	(%rdx,%rsi,4), %edx
	movb	%dl, (%rcx)
	addq	$4, %rcx
	addq	$-4, %rbx
	jne	.LBB33_9
.LBB33_10:                              # %._crit_edge
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end33:
	.size	string_upcase, .Lfunc_end33-string_upcase
	.cfi_endproc

	.globl	string_downcase
	.p2align	4, 0x90
	.type	string_downcase,@function
string_downcase:                        # @string_downcase
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi226:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi227:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi228:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi229:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi230:
	.cfi_def_cfa_offset 48
.Lcfi231:
	.cfi_offset %rbx, -48
.Lcfi232:
	.cfi_offset %r12, -40
.Lcfi233:
	.cfi_offset %r13, -32
.Lcfi234:
	.cfi_offset %r14, -24
.Lcfi235:
	.cfi_offset %r15, -16
	callq	get_c_string
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r12
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r14
	movw	$13, 2(%r14)
	cmpq	$-1, %rbx
	movq	%rbx, %r13
	jne	.LBB34_2
# BB#1:
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r13
.LBB34_2:
	leaq	1(%r13), %rdi
	callq	must_malloc
	movq	%rax, 16(%r14)
	movq	%r13, 8(%r14)
	testq	%r15, %r15
	je	.LBB34_4
# BB#3:
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	memcpy
	movq	16(%r14), %rax
.LBB34_4:                               # %strcons.exit
	movb	$0, (%rax,%r13)
	movq	%r12, %rdi
	callq	no_interrupt
	movq	%r14, %rdi
	callq	get_c_string
	movq	%rax, %r15
	testq	%rbx, %rbx
	jle	.LBB34_10
# BB#5:                                 # %.lr.ph
	callq	__ctype_tolower_loc
	leaq	-1(%rbx), %r8
	movq	%rbx, %rsi
	xorl	%edx, %edx
	andq	$3, %rsi
	je	.LBB34_7
	.p2align	4, 0x90
.LBB34_6:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	movsbq	(%r15,%rdx), %rcx
	movzbl	(%rdi,%rcx,4), %ecx
	movb	%cl, (%r15,%rdx)
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB34_6
.LBB34_7:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB34_10
# BB#8:                                 # %.lr.ph.new
	subq	%rdx, %rbx
	leaq	3(%r15,%rdx), %rcx
	.p2align	4, 0x90
.LBB34_9:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdx
	movsbq	-3(%rcx), %rsi
	movzbl	(%rdx,%rsi,4), %edx
	movb	%dl, -3(%rcx)
	movq	(%rax), %rdx
	movsbq	-2(%rcx), %rsi
	movzbl	(%rdx,%rsi,4), %edx
	movb	%dl, -2(%rcx)
	movq	(%rax), %rdx
	movsbq	-1(%rcx), %rsi
	movzbl	(%rdx,%rsi,4), %edx
	movb	%dl, -1(%rcx)
	movq	(%rax), %rdx
	movsbq	(%rcx), %rsi
	movzbl	(%rdx,%rsi,4), %edx
	movb	%dl, (%rcx)
	addq	$4, %rcx
	addq	$-4, %rbx
	jne	.LBB34_9
.LBB34_10:                              # %._crit_edge
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end34:
	.size	string_downcase, .Lfunc_end34-string_downcase
	.cfi_endproc

	.globl	lreadstring
	.p2align	4, 0x90
	.type	lreadstring,@function
lreadstring:                            # @lreadstring
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi236:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi237:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi238:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi239:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi240:
	.cfi_def_cfa_offset 48
.Lcfi241:
	.cfi_offset %rbx, -48
.Lcfi242:
	.cfi_offset %r12, -40
.Lcfi243:
	.cfi_offset %r13, -32
.Lcfi244:
	.cfi_offset %r14, -24
.Lcfi245:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	xorl	%r13d, %r13d
	movq	tkbuffer(%rip), %r12
	jmp	.LBB35_1
	.p2align	4, 0x90
.LBB35_24:                              #   in Loop: Header=BB35_1 Depth=1
	incl	%r13d
	movb	%r15b, (%r12)
	incq	%r12
.LBB35_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_17 Depth 2
	movq	16(%r14), %rdi
	callq	*(%r14)
	movl	%eax, %r15d
	cmpl	$92, %r15d
	je	.LBB35_6
# BB#2:                                 #   in Loop: Header=BB35_1 Depth=1
	cmpl	$-1, %r15d
	je	.LBB35_4
# BB#3:                                 #   in Loop: Header=BB35_1 Depth=1
	cmpl	$34, %r15d
	je	.LBB35_4
.LBB35_22:                              #   in Loop: Header=BB35_1 Depth=1
	cmpl	$5119, %r13d            # imm = 0x13FF
	jl	.LBB35_24
.LBB35_23:                              #   in Loop: Header=BB35_1 Depth=1
	movl	$.L.str.35, %edi
	xorl	%esi, %esi
	callq	err
	jmp	.LBB35_24
	.p2align	4, 0x90
.LBB35_6:                               #   in Loop: Header=BB35_1 Depth=1
	movq	16(%r14), %rdi
	callq	*(%r14)
	movl	%eax, %r15d
	leal	-48(%r15), %eax
	cmpl	$68, %eax
	ja	.LBB35_7
# BB#9:                                 #   in Loop: Header=BB35_1 Depth=1
	jmpq	*.LJTI35_0(,%rax,8)
.LBB35_16:                              # %.preheader.preheader
                                        #   in Loop: Header=BB35_1 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB35_17
	.p2align	4, 0x90
.LBB35_20:                              #   in Loop: Header=BB35_17 Depth=2
	leal	-48(%rbx,%r15,8), %r15d
.LBB35_17:                              # %.preheader
                                        #   Parent Loop BB35_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r14), %rdi
	callq	*(%r14)
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	jne	.LBB35_19
# BB#18:                                #   in Loop: Header=BB35_17 Depth=2
	movl	$.L.str.34, %edi
	xorl	%esi, %esi
	callq	err
.LBB35_19:                              #   in Loop: Header=BB35_17 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB35_20
# BB#21:                                #   in Loop: Header=BB35_1 Depth=1
	movq	16(%r14), %rsi
	movl	%ebx, %edi
	callq	*8(%r14)
	cmpl	$5119, %r13d            # imm = 0x13FF
	jl	.LBB35_24
	jmp	.LBB35_23
.LBB35_7:                               #   in Loop: Header=BB35_1 Depth=1
	cmpl	$-1, %r15d
	jne	.LBB35_22
# BB#8:                                 #   in Loop: Header=BB35_1 Depth=1
	movl	$.L.str.33, %edi
	xorl	%esi, %esi
	callq	err
	movl	$-1, %r15d
	cmpl	$5119, %r13d            # imm = 0x13FF
	jl	.LBB35_24
	jmp	.LBB35_23
.LBB35_14:                              #   in Loop: Header=BB35_1 Depth=1
	xorl	%r15d, %r15d
	cmpl	$5119, %r13d            # imm = 0x13FF
	jl	.LBB35_24
	jmp	.LBB35_23
.LBB35_13:                              #   in Loop: Header=BB35_1 Depth=1
	movl	$4, %r15d
	cmpl	$5119, %r13d            # imm = 0x13FF
	jl	.LBB35_24
	jmp	.LBB35_23
.LBB35_10:                              #   in Loop: Header=BB35_1 Depth=1
	movl	$10, %r15d
	cmpl	$5119, %r13d            # imm = 0x13FF
	jl	.LBB35_24
	jmp	.LBB35_23
.LBB35_12:                              #   in Loop: Header=BB35_1 Depth=1
	movl	$13, %r15d
	cmpl	$5119, %r13d            # imm = 0x13FF
	jl	.LBB35_24
	jmp	.LBB35_23
.LBB35_15:                              #   in Loop: Header=BB35_1 Depth=1
	movl	$32, %r15d
	cmpl	$5119, %r13d            # imm = 0x13FF
	jl	.LBB35_24
	jmp	.LBB35_23
.LBB35_11:                              #   in Loop: Header=BB35_1 Depth=1
	movl	$9, %r15d
	cmpl	$5119, %r13d            # imm = 0x13FF
	jl	.LBB35_24
	jmp	.LBB35_23
.LBB35_4:
	movb	$0, (%r12)
	movq	tkbuffer(%rip), %r15
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r12
	movw	$13, 2(%r12)
	cmpl	$-1, %r13d
	je	.LBB35_25
# BB#5:
	movslq	%r13d, %rbx
	jmp	.LBB35_26
.LBB35_25:
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
.LBB35_26:
	leaq	1(%rbx), %rdi
	callq	must_malloc
	movq	%rax, 16(%r12)
	movq	%rbx, 8(%r12)
	testq	%r15, %r15
	je	.LBB35_28
# BB#27:
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	16(%r12), %rax
.LBB35_28:                              # %strcons.exit
	movb	$0, (%rax,%rbx)
	movq	%r14, %rdi
	callq	no_interrupt
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end35:
	.size	lreadstring, .Lfunc_end35-lreadstring
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI35_0:
	.quad	.LBB35_16
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_14
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_13
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_10
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_22
	.quad	.LBB35_12
	.quad	.LBB35_15
	.quad	.LBB35_11

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI36_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	lreadsharp
	.p2align	4, 0x90
	.type	lreadsharp,@function
lreadsharp:                             # @lreadsharp
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi246:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi247:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi248:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi249:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi250:
	.cfi_def_cfa_offset 48
.Lcfi251:
	.cfi_offset %rbx, -48
.Lcfi252:
	.cfi_offset %r12, -40
.Lcfi253:
	.cfi_offset %r13, -32
.Lcfi254:
	.cfi_offset %r14, -24
.Lcfi255:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	callq	*(%rbx)
	cmpl	$101, %eax
	jg	.LBB36_4
# BB#1:
	cmpl	$40, %eax
	je	.LBB36_6
# BB#2:
	cmpl	$46, %eax
	jne	.LBB36_35
# BB#3:
	movq	%rbx, %rdi
	callq	lreadr
	xorl	%esi, %esi
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	leval                   # TAILCALL
.LBB36_4:
	cmpl	$102, %eax
	je	.LBB36_5
# BB#34:
	cmpl	$116, %eax
	jne	.LBB36_35
# BB#36:
	movsd	.LCPI36_0(%rip), %xmm0  # xmm0 = mem[0],zero
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	flocons                 # TAILCALL
.LBB36_6:
	movq	16(%rbx), %rsi
	movl	$40, %edi
	callq	*8(%rbx)
	movq	%rbx, %rdi
	callq	lreadr
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB36_17
# BB#7:
	movswl	2(%r15), %eax
	cmpl	$18, %eax
	ja	.LBB36_16
# BB#8:
	xorl	%r14d, %r14d
	jmpq	*.LJTI36_0(,%rax,8)
.LBB36_15:
	movq	8(%r15), %r14
	jmp	.LBB36_18
.LBB36_5:
	xorl	%r13d, %r13d
	jmp	.LBB36_20
.LBB36_35:
	movl	$.L.str.36, %edi
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	err                     # TAILCALL
.LBB36_9:                               # %.lr.ph.i.preheader
	movzwl	%ax, %eax
	xorl	%r14d, %r14d
	cmpl	$1, %eax
	jne	.LBB36_13
# BB#10:                                # %.lr.ph40.preheader
	movq	%r15, %rax
.LBB36_11:                              # %.lr.ph40
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	incq	%r14
	testq	%rax, %rax
	je	.LBB36_18
# BB#12:                                # %..lr.ph_crit_edge.i
                                        #   in Loop: Header=BB36_11 Depth=1
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	je	.LBB36_11
.LBB36_13:                              # %.lr.ph.i._crit_edge
	movl	$.L.str.46, %edi
	movq	%r15, %rsi
	callq	err
	jmp	.LBB36_18
.LBB36_16:
	movl	$.L.str.47, %edi
	movq	%r15, %rsi
	callq	err
.LBB36_17:                              # %nlength.exit
	xorl	%r14d, %r14d
.LBB36_18:                              # %nlength.exit
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r12
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r13
	movq	%r14, 8(%r13)
	leaq	(,%r14,8), %rdi
	callq	must_malloc
	movq	%rax, 16(%r13)
	testq	%r14, %r14
	jle	.LBB36_19
# BB#21:                                # %.lr.ph75.preheader.i
	movq	$0, (%rax)
	cmpq	$1, %r14
	jne	.LBB36_22
# BB#31:                                # %arcons.exit.thread
	movw	$16, 2(%r13)
	movq	%r12, %rdi
	callq	no_interrupt
	jmp	.LBB36_32
.LBB36_19:                              # %arcons.exit.thread42
	movw	$16, 2(%r13)
	movq	%r12, %rdi
	callq	no_interrupt
	jmp	.LBB36_20
.LBB36_22:                              # %.lr.ph75..lr.ph75_crit_edge.i.preheader
	leal	7(%r14), %edx
	leaq	-2(%r14), %rcx
	andq	$7, %rdx
	je	.LBB36_23
# BB#24:                                # %.lr.ph75..lr.ph75_crit_edge.i.prol.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB36_25:                              # %.lr.ph75..lr.ph75_crit_edge.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r13), %rsi
	movq	$0, 8(%rsi,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB36_25
# BB#26:                                # %.lr.ph75..lr.ph75_crit_edge.i.prol.loopexit.unr-lcssa
	incq	%rax
	cmpq	$7, %rcx
	jae	.LBB36_28
	jmp	.LBB36_30
.LBB36_23:
	movl	$1, %eax
	cmpq	$7, %rcx
	jb	.LBB36_30
.LBB36_28:                              # %.lr.ph75..lr.ph75_crit_edge.i.preheader.new
	movq	%r14, %rcx
	subq	%rax, %rcx
	shlq	$3, %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB36_29:                              # %.lr.ph75..lr.ph75_crit_edge.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r13), %rsi
	addq	%rax, %rsi
	movq	$0, (%rsi,%rdx,8)
	movq	16(%r13), %rsi
	addq	%rax, %rsi
	movq	$0, 8(%rsi,%rdx,8)
	movq	16(%r13), %rsi
	addq	%rax, %rsi
	movq	$0, 16(%rsi,%rdx,8)
	movq	16(%r13), %rsi
	addq	%rax, %rsi
	movq	$0, 24(%rsi,%rdx,8)
	movq	16(%r13), %rsi
	addq	%rax, %rsi
	movq	$0, 32(%rsi,%rdx,8)
	movq	16(%r13), %rsi
	addq	%rax, %rsi
	movq	$0, 40(%rsi,%rdx,8)
	movq	16(%r13), %rsi
	addq	%rax, %rsi
	movq	$0, 48(%rsi,%rdx,8)
	movq	16(%r13), %rsi
	addq	%rax, %rsi
	movq	$0, 56(%rsi,%rdx,8)
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB36_29
.LBB36_30:                              # %arcons.exit
	movw	$16, 2(%r13)
	movq	%r12, %rdi
	callq	no_interrupt
	testq	%r14, %r14
	jle	.LBB36_20
.LBB36_32:                              # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB36_33:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	car
	movq	16(%r13), %rcx
	movq	%rax, (%rcx,%rbx,8)
	movq	%r15, %rdi
	callq	cdr
	movq	%rax, %r15
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB36_33
.LBB36_20:                              # %.loopexit
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB36_14:
	movq	16(%r15), %rdi
	callq	strlen
	movq	%rax, %r14
	jmp	.LBB36_18
.Lfunc_end36:
	.size	lreadsharp, .Lfunc_end36-lreadsharp
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI36_0:
	.quad	.LBB36_18
	.quad	.LBB36_9
	.quad	.LBB36_16
	.quad	.LBB36_16
	.quad	.LBB36_16
	.quad	.LBB36_16
	.quad	.LBB36_16
	.quad	.LBB36_16
	.quad	.LBB36_16
	.quad	.LBB36_16
	.quad	.LBB36_16
	.quad	.LBB36_16
	.quad	.LBB36_16
	.quad	.LBB36_14
	.quad	.LBB36_15
	.quad	.LBB36_15
	.quad	.LBB36_15
	.quad	.LBB36_16
	.quad	.LBB36_15

	.text
	.globl	nlength
	.p2align	4, 0x90
	.type	nlength,@function
nlength:                                # @nlength
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi256:
	.cfi_def_cfa_offset 16
.Lcfi257:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB37_11
# BB#1:
	movswl	2(%rax), %ecx
	cmpl	$18, %ecx
	ja	.LBB37_10
# BB#2:
	xorl	%ebx, %ebx
	jmpq	*.LJTI37_0(,%rcx,8)
.LBB37_9:
	movq	8(%rax), %rbx
	jmp	.LBB37_12
.LBB37_10:
	movl	$.L.str.47, %edi
	movq	%rax, %rsi
	callq	err
.LBB37_11:                              # %.thread
	xorl	%ebx, %ebx
.LBB37_12:                              # %.thread
	movq	%rbx, %rax
	popq	%rbx
	retq
.LBB37_3:                               # %.lr.ph.preheader
	movzwl	%cx, %ecx
	xorl	%ebx, %ebx
	cmpl	$1, %ecx
	jne	.LBB37_7
# BB#4:                                 # %.lr.ph30.preheader
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB37_5:                               # %.lr.ph30
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rcx), %rcx
	incq	%rbx
	testq	%rcx, %rcx
	je	.LBB37_12
# BB#6:                                 # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB37_5 Depth=1
	movzwl	2(%rcx), %edx
	cmpl	$1, %edx
	je	.LBB37_5
.LBB37_7:                               # %.lr.ph._crit_edge
	movl	$.L.str.46, %edi
	movq	%rax, %rsi
	callq	err
	jmp	.LBB37_12
.LBB37_8:
	movq	16(%rax), %rdi
	popq	%rbx
	jmp	strlen                  # TAILCALL
.Lfunc_end37:
	.size	nlength, .Lfunc_end37-nlength
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI37_0:
	.quad	.LBB37_12
	.quad	.LBB37_3
	.quad	.LBB37_10
	.quad	.LBB37_10
	.quad	.LBB37_10
	.quad	.LBB37_10
	.quad	.LBB37_10
	.quad	.LBB37_10
	.quad	.LBB37_10
	.quad	.LBB37_10
	.quad	.LBB37_10
	.quad	.LBB37_10
	.quad	.LBB37_10
	.quad	.LBB37_8
	.quad	.LBB37_9
	.quad	.LBB37_9
	.quad	.LBB37_9
	.quad	.LBB37_10
	.quad	.LBB37_9

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI38_0:
	.quad	4890909195324358656     # double 9.2233720368547758E+18
	.text
	.globl	c_sxhash
	.p2align	4, 0x90
	.type	c_sxhash,@function
c_sxhash:                               # @c_sxhash
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi258:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi259:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi260:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi261:
	.cfi_def_cfa_offset 48
.Lcfi262:
	.cfi_offset %rbx, -32
.Lcfi263:
	.cfi_offset %r14, -24
.Lcfi264:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, 8(%rsp)
	leaq	8(%rsp), %rax
	cmpq	stack_limit_ptr(%rip), %rax
	jae	.LBB38_2
# BB#1:
	leaq	8(%rsp), %rdi
	callq	err_stack
	movq	8(%rsp), %rdi
.LBB38_2:
	testq	%rdi, %rdi
	je	.LBB38_23
# BB#3:
	movswl	2(%rdi), %eax
	cmpl	$20, %eax
	ja	.LBB38_18
# BB#4:
	xorl	%edx, %edx
	jmpq	*.LJTI38_0(,%rax,8)
.LBB38_5:
	movq	8(%rdi), %rcx
	movb	(%rcx), %al
	testb	%al, %al
	je	.LBB38_23
# BB#6:                                 # %.lr.ph52.preheader
	incq	%rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB38_7:                               # %.lr.ph52
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rsi
	shlq	$4, %rsi
	leaq	1(%rdx,%rsi), %rdx
	movzbl	%al, %eax
	xorq	%rdx, %rax
	cqto
	idivq	%r14
	movzbl	(%rcx), %eax
	incq	%rcx
	testb	%al, %al
	jne	.LBB38_7
	jmp	.LBB38_24
.LBB38_10:
	movq	8(%rdi), %rdi
	movq	%r14, %rsi
	callq	c_sxhash
	movq	%rax, %rdx
	movq	8(%rsp), %rax
	movq	16(%rax), %rbx
	jmp	.LBB38_12
	.p2align	4, 0x90
.LBB38_11:                              #   in Loop: Header=BB38_12 Depth=1
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	callq	c_sxhash
	xorq	%r15, %rax
	cqto
	idivq	%r14
	movq	16(%rbx), %rbx
.LBB38_12:                              # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rax
	shlq	$4, %rax
	leaq	1(%rdx,%rax), %r15
	testq	%rbx, %rbx
	je	.LBB38_20
# BB#13:                                # %.lr.ph
                                        #   in Loop: Header=BB38_12 Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB38_11
	jmp	.LBB38_21
.LBB38_14:
	movsd	8(%rdi), %xmm0          # xmm0 = mem[0],zero
	movsd	.LCPI38_0(%rip), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	cvttsd2si	%xmm2, %rax
	movabsq	$-9223372036854775808, %rcx # imm = 0x8000000000000000
	xorq	%rax, %rcx
	cvttsd2si	%xmm0, %rax
	ucomisd	%xmm0, %xmm1
	cmovbeq	%rcx, %rax
	xorl	%edx, %edx
	divq	%r14
	jmp	.LBB38_24
.LBB38_15:
	movq	8(%rdi), %rcx
	movb	(%rcx), %al
	testb	%al, %al
	je	.LBB38_23
# BB#16:                                # %.lr.ph48.preheader
	incq	%rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB38_17:                              # %.lr.ph48
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rsi
	shlq	$4, %rsi
	leaq	1(%rdx,%rsi), %rdx
	movzbl	%al, %eax
	xorq	%rdx, %rax
	cqto
	idivq	%r14
	movzbl	(%rcx), %eax
	incq	%rcx
	testb	%al, %al
	jne	.LBB38_17
	jmp	.LBB38_24
.LBB38_18:
	movslq	%eax, %rdi
	callq	get_user_type_hooks
	movq	48(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB38_23
# BB#19:
	movq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	*%rcx
	movq	%rax, %rdx
	jmp	.LBB38_24
.LBB38_23:
	xorl	%edx, %edx
.LBB38_24:                              # %.thread
	movq	%rdx, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB38_20:                              # %.thread36
	xorl	%ebx, %ebx
.LBB38_21:                              # %.loopexit
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	c_sxhash
	xorq	%r15, %rax
	cqto
	idivq	%r14
	jmp	.LBB38_24
.Lfunc_end38:
	.size	c_sxhash, .Lfunc_end38-c_sxhash
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI38_0:
	.quad	.LBB38_24
	.quad	.LBB38_10
	.quad	.LBB38_14
	.quad	.LBB38_15
	.quad	.LBB38_5
	.quad	.LBB38_5
	.quad	.LBB38_5
	.quad	.LBB38_5
	.quad	.LBB38_5
	.quad	.LBB38_5
	.quad	.LBB38_5
	.quad	.LBB38_18
	.quad	.LBB38_18
	.quad	.LBB38_18
	.quad	.LBB38_18
	.quad	.LBB38_18
	.quad	.LBB38_18
	.quad	.LBB38_18
	.quad	.LBB38_18
	.quad	.LBB38_5
	.quad	.LBB38_5

	.text
	.globl	sxhash
	.p2align	4, 0x90
	.type	sxhash,@function
sxhash:                                 # @sxhash
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB39_4
# BB#1:
	movzwl	2(%rsi), %eax
	cmpl	$2, %eax
	jne	.LBB39_4
# BB#2:
	cvttsd2si	8(%rsi), %rsi
	jmp	.LBB39_5
.LBB39_4:
	movl	$10000, %esi            # imm = 0x2710
.LBB39_5:                               # %.critedge
	pushq	%rax
.Lcfi265:
	.cfi_def_cfa_offset 16
	callq	c_sxhash
	cvtsi2sdq	%rax, %xmm0
	popq	%rax
	jmp	flocons                 # TAILCALL
.Lfunc_end39:
	.size	sxhash, .Lfunc_end39-sxhash
	.cfi_endproc

	.globl	equal
	.p2align	4, 0x90
	.type	equal,@function
equal:                                  # @equal
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi266:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi267:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi268:
	.cfi_def_cfa_offset 32
.Lcfi269:
	.cfi_offset %rbx, -24
.Lcfi270:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, (%rsp)
	movq	%rsp, %rax
	cmpq	stack_limit_ptr(%rip), %rax
	jae	.LBB40_3
# BB#1:
	movq	%rsp, %rdi
	callq	err_stack
.LBB40_2:                               # %thread-pre-split
	movq	(%rsp), %rdi
.LBB40_3:
	cmpq	%rbx, %rdi
	je	.LBB40_6
# BB#4:
	testq	%rdi, %rdi
	je	.LBB40_7
# BB#5:
	movswq	2(%rdi), %rax
	testq	%rbx, %rbx
	jne	.LBB40_8
	jmp	.LBB40_9
.LBB40_6:
	movq	sym_t(%rip), %rax
	jmp	.LBB40_16
.LBB40_7:
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB40_9
.LBB40_8:
	movswq	2(%rbx), %rcx
	cmpq	%rcx, %rax
	jne	.LBB40_15
	jmp	.LBB40_10
.LBB40_9:
	xorl	%ecx, %ecx
	cmpq	%rcx, %rax
	jne	.LBB40_15
.LBB40_10:
	cmpq	$3, %rax
	je	.LBB40_15
# BB#11:
	cmpq	$2, %rax
	je	.LBB40_18
# BB#12:
	cmpq	$1, %rax
	jne	.LBB40_19
# BB#13:
	callq	car
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	car
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	equal
	testq	%rax, %rax
	je	.LBB40_15
# BB#14:
	movq	(%rsp), %rdi
	callq	cdr
	movq	%rax, (%rsp)
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbx
	jmp	.LBB40_2
.LBB40_18:
	movsd	8(%rdi), %xmm0          # xmm0 = mem[0],zero
	xorl	%ecx, %ecx
	ucomisd	8(%rbx), %xmm0
	movq	sym_t(%rip), %rax
	cmovneq	%rcx, %rax
	cmovpq	%rcx, %rax
	jmp	.LBB40_16
.LBB40_19:
	movq	%rax, %rdi
	callq	get_user_type_hooks
	movq	72(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB40_15
# BB#20:
	movq	(%rsp), %rdi
	movq	%rbx, %rsi
	callq	*%rcx
	jmp	.LBB40_16
.LBB40_15:
	xorl	%eax, %eax
.LBB40_16:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end40:
	.size	equal, .Lfunc_end40-equal
	.cfi_endproc

	.globl	href_index
	.p2align	4, 0x90
	.type	href_index,@function
href_index:                             # @href_index
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi271:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi272:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi273:
	.cfi_def_cfa_offset 32
.Lcfi274:
	.cfi_offset %rbx, -24
.Lcfi275:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB41_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$16, %eax
	je	.LBB41_3
.LBB41_2:                               # %.critedge
	movl	$.L.str.37, %edi
	movq	%rbx, %rsi
	callq	err
.LBB41_3:
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	c_sxhash
	testq	%rax, %rax
	js	.LBB41_5
# BB#4:
	cmpq	8(%rbx), %rax
	jl	.LBB41_6
.LBB41_5:
	movl	$.L.str.38, %edi
	movq	%rbx, %rsi
	callq	err
	xorl	%eax, %eax
.LBB41_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end41:
	.size	href_index, .Lfunc_end41-href_index
	.cfi_endproc

	.globl	href
	.p2align	4, 0x90
	.type	href,@function
href:                                   # @href
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi276:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi277:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi278:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi279:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi280:
	.cfi_def_cfa_offset 48
.Lcfi281:
	.cfi_offset %rbx, -40
.Lcfi282:
	.cfi_offset %r12, -32
.Lcfi283:
	.cfi_offset %r14, -24
.Lcfi284:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	16(%rbx), %r15
	testq	%rbx, %rbx
	je	.LBB42_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$16, %eax
	je	.LBB42_3
.LBB42_2:                               # %.critedge.i
	movl	$.L.str.37, %edi
	movq	%rbx, %rsi
	callq	err
.LBB42_3:
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	c_sxhash
	testq	%rax, %rax
	js	.LBB42_5
# BB#4:
	cmpq	8(%rbx), %rax
	jl	.LBB42_6
.LBB42_5:
	movl	$.L.str.38, %edi
	movq	%rbx, %rsi
	callq	err
	xorl	%eax, %eax
.LBB42_6:                               # %href_index.exit
	movq	(%r15,%rax,8), %r15
	testq	%r15, %r15
	je	.LBB42_14
# BB#7:                                 # %.lr.ph.i.preheader
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB42_8:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB42_15
# BB#9:                                 #   in Loop: Header=BB42_8 Depth=1
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.LBB42_12
# BB#10:                                #   in Loop: Header=BB42_8 Depth=1
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	jne	.LBB42_12
# BB#11:                                #   in Loop: Header=BB42_8 Depth=1
	movq	8(%r12), %rdi
	movq	%r14, %rsi
	callq	equal
	testq	%rax, %rax
	jne	.LBB42_16
.LBB42_12:                              # %.thread16.i
                                        #   in Loop: Header=BB42_8 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB42_8
.LBB42_14:
	xorl	%r12d, %r12d
	jmp	.LBB42_16
.LBB42_15:
	movl	$.L.str.39, %edi
	movq	%r15, %rsi
	callq	err
	movq	%rax, %r12
.LBB42_16:                              # %assoc.exit
	movq	%r12, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	cdr                     # TAILCALL
.Lfunc_end42:
	.size	href, .Lfunc_end42-href
	.cfi_endproc

	.globl	assoc
	.p2align	4, 0x90
	.type	assoc,@function
assoc:                                  # @assoc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi285:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi286:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi287:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi288:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi289:
	.cfi_def_cfa_offset 48
.Lcfi290:
	.cfi_offset %rbx, -40
.Lcfi291:
	.cfi_offset %r12, -32
.Lcfi292:
	.cfi_offset %r14, -24
.Lcfi293:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB43_7
# BB#1:                                 # %.lr.ph.preheader
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB43_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB43_9
# BB#3:                                 #   in Loop: Header=BB43_2 Depth=1
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.LBB43_6
# BB#4:                                 #   in Loop: Header=BB43_2 Depth=1
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	jne	.LBB43_6
# BB#5:                                 #   in Loop: Header=BB43_2 Depth=1
	movq	8(%r12), %rdi
	movq	%r15, %rsi
	callq	equal
	testq	%rax, %rax
	jne	.LBB43_8
.LBB43_6:                               # %.thread16
                                        #   in Loop: Header=BB43_2 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB43_2
.LBB43_7:
	xorl	%r12d, %r12d
.LBB43_8:                               # %.thread17
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB43_9:
	movl	$.L.str.39, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	err                     # TAILCALL
.Lfunc_end43:
	.size	assoc, .Lfunc_end43-assoc
	.cfi_endproc

	.globl	hset
	.p2align	4, 0x90
	.type	hset,@function
hset:                                   # @hset
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi294:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi295:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi296:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi297:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi298:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi299:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi300:
	.cfi_def_cfa_offset 64
.Lcfi301:
	.cfi_offset %rbx, -56
.Lcfi302:
	.cfi_offset %r12, -48
.Lcfi303:
	.cfi_offset %r13, -40
.Lcfi304:
	.cfi_offset %r14, -32
.Lcfi305:
	.cfi_offset %r15, -24
.Lcfi306:
	.cfi_offset %rbp, -16
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB44_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$16, %eax
	je	.LBB44_3
.LBB44_2:                               # %.critedge.i
	movl	$.L.str.37, %edi
	movq	%rbx, %rsi
	callq	err
.LBB44_3:
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	c_sxhash
	movq	%rax, %r12
	testq	%r12, %r12
	js	.LBB44_5
# BB#4:
	cmpq	8(%rbx), %r12
	jl	.LBB44_6
.LBB44_5:
	movl	$.L.str.38, %edi
	movq	%rbx, %rsi
	callq	err
	xorl	%r12d, %r12d
.LBB44_6:                               # %href_index.exit
	movq	16(%rbx), %rax
	movq	(%rax,%r12,8), %r14
	testq	%r14, %r14
	je	.LBB44_14
# BB#7:                                 # %.lr.ph.i.preheader
	movq	%r14, %r13
	.p2align	4, 0x90
.LBB44_8:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%r13), %eax
	cmpl	$1, %eax
	jne	.LBB44_13
# BB#9:                                 #   in Loop: Header=BB44_8 Depth=1
	movq	8(%r13), %rbp
	testq	%rbp, %rbp
	je	.LBB44_12
# BB#10:                                #   in Loop: Header=BB44_8 Depth=1
	movzwl	2(%rbp), %eax
	cmpl	$1, %eax
	jne	.LBB44_12
# BB#11:                                #   in Loop: Header=BB44_8 Depth=1
	movq	8(%rbp), %rdi
	movq	%r15, %rsi
	callq	equal
	testq	%rax, %rax
	jne	.LBB44_15
.LBB44_12:                              # %.thread16.i
                                        #   in Loop: Header=BB44_8 Depth=1
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.LBB44_8
	jmp	.LBB44_14
.LBB44_13:                              # %assoc.exit
	movl	$.L.str.39, %edi
	movq	%r14, %rsi
	callq	err
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB44_14
.LBB44_15:                              # %assoc.exit.thread21
	movq	%rbp, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	setcdr                  # TAILCALL
.LBB44_14:
	movq	%r15, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%rbp, %rsi
	callq	cons
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	cons
	movq	16(%rbx), %rcx
	movq	%rax, (%rcx,%r12,8)
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end44:
	.size	hset, .Lfunc_end44-hset
	.cfi_endproc

	.globl	assv
	.p2align	4, 0x90
	.type	assv,@function
assv:                                   # @assv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi307:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi308:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi309:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi310:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi311:
	.cfi_def_cfa_offset 48
.Lcfi312:
	.cfi_offset %rbx, -40
.Lcfi313:
	.cfi_offset %r12, -32
.Lcfi314:
	.cfi_offset %r14, -24
.Lcfi315:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB45_7
# BB#1:                                 # %.lr.ph.preheader
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB45_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB45_9
# BB#3:                                 #   in Loop: Header=BB45_2 Depth=1
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.LBB45_6
# BB#4:                                 #   in Loop: Header=BB45_2 Depth=1
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	jne	.LBB45_6
# BB#5:                                 #   in Loop: Header=BB45_2 Depth=1
	movq	8(%r12), %rdi
	movq	%r15, %rsi
	callq	eql
	testq	%rax, %rax
	jne	.LBB45_8
.LBB45_6:                               # %.thread16
                                        #   in Loop: Header=BB45_2 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB45_2
.LBB45_7:
	xorl	%r12d, %r12d
.LBB45_8:                               # %.thread17
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB45_9:
	movl	$.L.str.40, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	err                     # TAILCALL
.Lfunc_end45:
	.size	assv, .Lfunc_end45-assv
	.cfi_endproc

	.globl	put_long
	.p2align	4, 0x90
	.type	put_long,@function
put_long:                               # @put_long
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi316:
	.cfi_def_cfa_offset 16
	movq	%rsi, %rax
	movq	%rdi, (%rsp)
	movq	%rsp, %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	popq	%rax
	retq
.Lfunc_end46:
	.size	put_long, .Lfunc_end46-put_long
	.cfi_endproc

	.globl	get_long
	.p2align	4, 0x90
	.type	get_long,@function
get_long:                               # @get_long
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi317:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rax
	movq	%rsp, %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fread
	movq	(%rsp), %rax
	popq	%rcx
	retq
.Lfunc_end47:
	.size	get_long, .Lfunc_end47-get_long
	.cfi_endproc

	.globl	fast_print_table
	.p2align	4, 0x90
	.type	fast_print_table,@function
fast_print_table:                       # @fast_print_table
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi318:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi319:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi320:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi321:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi322:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi323:
	.cfi_def_cfa_offset 64
.Lcfi324:
	.cfi_offset %rbx, -48
.Lcfi325:
	.cfi_offset %r12, -40
.Lcfi326:
	.cfi_offset %r13, -32
.Lcfi327:
	.cfi_offset %r14, -24
.Lcfi328:
	.cfi_offset %r15, -16
	movq	%rsi, %r13
	movq	%rdi, %r15
	movq	%r13, %rdi
	callq	car
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	get_c_file
	movq	%rax, %r14
	movq	%r13, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	car
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB48_10
# BB#1:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	href
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB48_6
# BB#2:
	movl	$127, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movzwl	2(%r12), %eax
	cmpl	$2, %eax
	je	.LBB48_4
# BB#3:                                 # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%r12, %rsi
	callq	err
.LBB48_4:                               # %get_c_long.exit
	cvttsd2si	8(%r12), %rax
	movq	%rax, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	xorl	%ebx, %ebx
	jmp	.LBB48_11
.LBB48_6:
	movq	%r13, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	car
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB48_10
# BB#7:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	hset
	movq	bashnum(%rip), %rax
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	movq	%rcx, 8(%rax)
	movq	%r13, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rbx
	movq	bashnum(%rip), %rsi
	movq	%r12, %rdi
	callq	plus
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setcar
	movl	$126, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movzwl	2(%r12), %eax
	cmpl	$2, %eax
	je	.LBB48_9
# BB#8:                                 # %.critedge.i23
	movl	$.L.str.44, %edi
	movq	%r12, %rsi
	callq	err
.LBB48_9:                               # %get_c_long.exit24
	cvttsd2si	8(%r12), %rax
	movq	%rax, 8(%rsp)
	movl	$1, %ebx
	leaq	8(%rsp), %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	jmp	.LBB48_11
.LBB48_10:
	movl	$1, %ebx
.LBB48_11:
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end48:
	.size	fast_print_table, .Lfunc_end48-fast_print_table
	.cfi_endproc

	.globl	fast_print
	.p2align	4, 0x90
	.type	fast_print,@function
fast_print:                             # @fast_print
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi329:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi330:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi331:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi332:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi333:
	.cfi_def_cfa_offset 64
.Lcfi334:
	.cfi_offset %rbx, -40
.Lcfi335:
	.cfi_offset %r12, -32
.Lcfi336:
	.cfi_offset %r14, -24
.Lcfi337:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, 8(%rsp)
	leaq	8(%rsp), %rax
	cmpq	stack_limit_ptr(%rip), %rax
	jae	.LBB49_2
# BB#1:
	leaq	8(%rsp), %rdi
	callq	err_stack
.LBB49_2:
	movq	%r15, %rdi
	callq	car
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	get_c_file
	movq	%rax, %r14
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB49_5
# BB#3:
	movswl	2(%rdi), %eax
	cmpl	$3, %eax
	ja	.LBB49_6
# BB#4:
	jmpq	*.LJTI49_0(,%rax,8)
.LBB49_5:                               # %.thread
	xorl	%r12d, %r12d
	xorl	%edi, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	jmp	.LBB49_35
.LBB49_6:
	movslq	%eax, %rdi
	callq	get_user_type_hooks
	movq	56(%rax), %rcx
	testq	%rcx, %rcx
	movq	8(%rsp), %rax
	je	.LBB49_19
# BB#7:
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	*%rcx
	jmp	.LBB49_20
.LBB49_8:                               # %.lr.ph61.preheader
	movzwl	%ax, %eax
	cmpl	$1, %eax
	jne	.LBB49_17
# BB#9:                                 # %.lr.ph69.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB49_10:                              # %.lr.ph69
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB49_22
# BB#11:                                # %..lr.ph61_crit_edge
                                        #   in Loop: Header=BB49_10 Depth=1
	movzwl	2(%rdi), %ecx
	leaq	1(%rbx), %rax
	cmpl	$1, %ecx
	je	.LBB49_10
	jmp	.LBB49_18
.LBB49_12:
	movl	$2, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	8(%rsp), %rdi
	addq	$8, %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	jmp	.LBB49_34
.LBB49_13:
	movq	%r15, %rsi
	callq	fast_print_table
	testq	%rax, %rax
	je	.LBB49_34
# BB#14:
	movl	$3, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	8(%rsp), %r15
	movq	8(%r15), %rdi
	callq	strlen
	movq	%rax, %rbx
	cmpq	$5120, %rbx             # imm = 0x1400
	jl	.LBB49_16
# BB#15:
	movl	$.L.str.41, %edi
	movq	%r15, %rsi
	callq	err
.LBB49_16:
	movq	%rbx, 16(%rsp)
	leaq	16(%rsp), %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	8(%rsp), %rax
	movq	8(%rax), %rdi
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r14, %rcx
	callq	fwrite
	movq	sym_t(%rip), %r12
	jmp	.LBB49_35
.LBB49_17:
	xorl	%ebx, %ebx
.LBB49_18:
	xorl	%eax, %eax
	cmpq	$1, %rbx
	je	.LBB49_23
	jmp	.LBB49_24
.LBB49_19:
	movl	$.L.str.42, %edi
	movq	%rax, %rsi
	callq	err
.LBB49_20:                              # %.thread48
	movq	%rax, %r12
	jmp	.LBB49_35
.LBB49_22:
	movb	$1, %al
	cmpq	$1, %rbx
	jne	.LBB49_24
.LBB49_23:
	movl	$1, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	8(%rsp), %rdi
	callq	car
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	fast_print
	movq	8(%rsp), %rdi
	callq	cdr
	movq	%rax, %rdi
	jmp	.LBB49_33
.LBB49_24:
	testb	%al, %al
	je	.LBB49_28
# BB#25:
	movl	$125, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	%rbx, 16(%rsp)
	leaq	16(%rsp), %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	8(%rsp), %rbx
	xorl	%r12d, %r12d
	testq	%rbx, %rbx
	je	.LBB49_35
	.p2align	4, 0x90
.LBB49_27:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB49_35
# BB#26:                                #   in Loop: Header=BB49_27 Depth=1
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	callq	fast_print
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB49_27
	jmp	.LBB49_35
.LBB49_28:
	movl	$124, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	%rbx, 16(%rsp)
	leaq	16(%rsp), %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB49_31
	.p2align	4, 0x90
.LBB49_30:                              # %.lr.ph55
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB49_32
# BB#29:                                #   in Loop: Header=BB49_30 Depth=1
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	callq	fast_print
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB49_30
.LBB49_31:
	xorl	%ebx, %ebx
.LBB49_32:                              # %.thread49
	movq	%rbx, %rdi
.LBB49_33:                              # %.thread48
	movq	%r15, %rsi
	callq	fast_print
.LBB49_34:                              # %.thread48
	xorl	%r12d, %r12d
.LBB49_35:                              # %.thread48
	movq	%r12, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end49:
	.size	fast_print, .Lfunc_end49-fast_print
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI49_0:
	.quad	.LBB49_5
	.quad	.LBB49_8
	.quad	.LBB49_12
	.quad	.LBB49_13

	.text
	.globl	fast_read
	.p2align	4, 0x90
	.type	fast_read,@function
fast_read:                              # @fast_read
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi338:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi339:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi340:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi341:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi342:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi343:
	.cfi_def_cfa_offset 64
.Lcfi344:
	.cfi_offset %rbx, -48
.Lcfi345:
	.cfi_offset %r12, -40
.Lcfi346:
	.cfi_offset %r13, -32
.Lcfi347:
	.cfi_offset %r14, -24
.Lcfi348:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	callq	car
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	get_c_file
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %r14d
	cmpl	$123, %r14d
	jg	.LBB50_8
# BB#1:
	leal	1(%r14), %eax
	cmpl	$36, %eax
	ja	.LBB50_24
# BB#2:
	jmpq	*.LJTI50_0(,%rax,8)
.LBB50_3:
	xorl	%r15d, %r15d
	jmp	.LBB50_33
	.p2align	4, 0x90
.LBB50_4:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB50_33
# BB#5:                                 # %.preheader
                                        #   in Loop: Header=BB50_4 Depth=1
	testl	%eax, %eax
	je	.LBB50_11
# BB#6:                                 # %.preheader
                                        #   in Loop: Header=BB50_4 Depth=1
	cmpl	$10, %eax
	jne	.LBB50_4
# BB#7:
	movq	%r15, %rdi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	fast_read               # TAILCALL
.LBB50_8:
	leal	-124(%r14), %eax
	cmpl	$2, %eax
	jb	.LBB50_12
# BB#9:
	cmpl	$126, %r14d
	je	.LBB50_23
# BB#10:
	cmpl	$127, %r14d
	jne	.LBB50_24
.LBB50_11:                              # %.loopexit
	movq	%rsp, %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cvtsi2sdq	(%rsp), %xmm0
	movq	bashnum(%rip), %rax
	movsd	%xmm0, 8(%rax)
	movq	%r15, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	car
	movq	bashnum(%rip), %rsi
	movq	%rax, %rdi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	href                    # TAILCALL
.LBB50_12:
	movq	%rsp, %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	movq	(%rsp), %r13
	cvtsi2sdq	%r13, %xmm0
	movq	bashnum(%rip), %r12
	movsd	%xmm0, 8(%r12)
	testq	%r12, %r12
	je	.LBB50_14
# BB#13:
	movzwl	2(%r12), %eax
	cmpl	$2, %eax
	je	.LBB50_15
.LBB50_14:                              # %.critedge.i.i
	movl	$.L.str.44, %edi
	movq	%r12, %rsi
	callq	err
	movsd	8(%r12), %xmm0          # xmm0 = mem[0],zero
.LBB50_15:                              # %get_c_long.exit.i
	cvttsd2si	%xmm0, %rbx
	testq	%rbx, %rbx
	jle	.LBB50_26
# BB#16:                                # %.lr.ph.i.preheader
	incq	%rbx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB50_17:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edi, %edi
	movq	%r12, %rsi
	callq	cons
	movq	%rax, %r12
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB50_17
	jmp	.LBB50_27
.LBB50_18:
	movq	%r15, %rdi
	callq	fast_read
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	fast_read
	movq	%rbx, %rdi
	movq	%rax, %rsi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	cons                    # TAILCALL
.LBB50_19:
	movl	$2, %edi
	callq	newcell
	movq	%rax, %r15
	leaq	8(%r15), %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	jmp	.LBB50_33
.LBB50_20:
	movq	%rsp, %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	movq	(%rsp), %r14
	cmpq	$5120, %r14             # imm = 0x1400
	jl	.LBB50_22
# BB#21:
	movl	$.L.str.41, %edi
	xorl	%esi, %esi
	callq	err
.LBB50_22:
	movq	tkbuffer(%rip), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%rbx, %rcx
	callq	fread
	movq	tkbuffer(%rip), %rax
	movb	$0, (%rax,%r14)
	movq	tkbuffer(%rip), %rdi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	rintern                 # TAILCALL
.LBB50_23:
	movq	%rsp, %rdi
	movl	$8, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cvtsi2sdq	(%rsp), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	%r15, %rdi
	callq	fast_read
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	car
	movq	%rax, %rbx
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	flocons
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%r14, %rdx
	callq	hset
	movq	%r14, %r15
	jmp	.LBB50_33
.LBB50_24:
	movslq	%r14d, %rdi
	callq	get_user_type_hooks
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.LBB50_34
# BB#25:
	movl	%r14d, %edi
	movq	%r15, %rsi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.LBB50_26:
	xorl	%r12d, %r12d
.LBB50_27:                              # %make_list.exit
	movq	%r15, %rdi
	callq	fast_read
	movq	%rax, 8(%r12)
	cmpq	$2, %r13
	movq	%r12, %rbx
	jl	.LBB50_30
# BB#28:                                # %.lr.ph.preheader
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB50_29:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	decq	%r13
	movq	%r15, %rdi
	callq	fast_read
	movq	%rax, 8(%rbx)
	cmpq	$1, %r13
	jg	.LBB50_29
.LBB50_30:                              # %._crit_edge
	cmpl	$124, %r14d
	jne	.LBB50_32
# BB#31:
	movq	%r15, %rdi
	callq	fast_read
	movq	%rax, 16(%rbx)
.LBB50_32:
	movq	%r12, %r15
.LBB50_33:                              # %.loopexit60
	movq	%r15, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB50_34:
	cvtsi2sdl	%r14d, %xmm0
	callq	flocons
	movl	$.L.str.43, %edi
	movq	%rax, %rsi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	err                     # TAILCALL
.Lfunc_end50:
	.size	fast_read, .Lfunc_end50-fast_read
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI50_0:
	.quad	.LBB50_33
	.quad	.LBB50_3
	.quad	.LBB50_18
	.quad	.LBB50_19
	.quad	.LBB50_20
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_24
	.quad	.LBB50_4

	.text
	.globl	make_list
	.p2align	4, 0x90
	.type	make_list,@function
make_list:                              # @make_list
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi349:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi350:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi351:
	.cfi_def_cfa_offset 32
.Lcfi352:
	.cfi_offset %rbx, -32
.Lcfi353:
	.cfi_offset %r14, -24
.Lcfi354:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB51_2
# BB#1:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB51_3
.LBB51_2:                               # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%r14, %rsi
	callq	err
.LBB51_3:                               # %get_c_long.exit
	cvttsd2si	8(%r14), %rbx
	testq	%rbx, %rbx
	jle	.LBB51_4
# BB#5:                                 # %.lr.ph.preheader
	incq	%rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB51_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	cons
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB51_6
	jmp	.LBB51_7
.LBB51_4:
	xorl	%eax, %eax
.LBB51_7:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end51:
	.size	make_list, .Lfunc_end51-make_list
	.cfi_endproc

	.globl	get_c_double
	.p2align	4, 0x90
	.type	get_c_double,@function
get_c_double:                           # @get_c_double
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi355:
	.cfi_def_cfa_offset 16
.Lcfi356:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB52_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB52_3
.LBB52_2:                               # %.critedge
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB52_3:
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	popq	%rbx
	retq
.Lfunc_end52:
	.size	get_c_double, .Lfunc_end52-get_c_double
	.cfi_endproc

	.globl	lfread
	.p2align	4, 0x90
	.type	lfread,@function
lfread:                                 # @lfread
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi357:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi358:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi359:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi360:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi361:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi362:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi363:
	.cfi_def_cfa_offset 64
.Lcfi364:
	.cfi_offset %rbx, -56
.Lcfi365:
	.cfi_offset %r12, -48
.Lcfi366:
	.cfi_offset %r13, -40
.Lcfi367:
	.cfi_offset %r14, -32
.Lcfi368:
	.cfi_offset %r15, -24
.Lcfi369:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	stdin(%rip), %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	get_c_file
	movq	%rax, %r12
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB53_5
# BB#1:
	movzwl	2(%rbx), %eax
	movswl	%ax, %ecx
	cmpl	$18, %ecx
	je	.LBB53_3
# BB#2:
	cmpl	$13, %ecx
	jne	.LBB53_4
.LBB53_3:
	movq	8(%rbx), %rbp
	movq	16(%rbx), %r15
	xorl	%ebx, %ebx
	jmp	.LBB53_7
.LBB53_4:
	movzwl	%ax, %eax
	cmpl	$2, %eax
	je	.LBB53_6
.LBB53_5:                               # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB53_6:                               # %get_c_long.exit
	cvttsd2si	8(%rbx), %rbp
	leaq	1(%rbp), %rdi
	callq	must_malloc
	movq	%rax, %r15
	movb	$0, (%r15,%rbp)
	movb	$1, %bl
.LBB53_7:
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rbp, %rdx
	movq	%r12, %rcx
	callq	fread
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB53_8
# BB#11:
	testb	%bl, %bl
	je	.LBB53_19
# BB#12:
	cmpq	%rbp, %r13
	jne	.LBB53_14
# BB#13:
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r12
	movw	$13, 2(%r12)
	movq	%r15, 16(%r12)
	movq	%rbp, 8(%r12)
	jmp	.LBB53_17
.LBB53_8:
	testb	%bl, %bl
	je	.LBB53_10
# BB#9:
	movq	%r15, %rdi
	callq	free
.LBB53_10:
	movq	%r14, %rdi
	callq	no_interrupt
	xorl	%r12d, %r12d
	jmp	.LBB53_18
.LBB53_19:
	movq	%r14, %rdi
	callq	no_interrupt
	cvtsi2sdq	%r13, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	flocons                 # TAILCALL
.LBB53_14:
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %rbx
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r12
	movw	$13, 2(%r12)
	cmpq	$-1, %r13
	movq	%r13, %rbp
	jne	.LBB53_16
# BB#15:
	xorl	%edi, %edi
	callq	strlen
	movq	%rax, %rbp
.LBB53_16:                              # %strcons.exit
	leaq	1(%rbp), %rdi
	callq	must_malloc
	movq	%rax, 16(%r12)
	movq	%rbp, 8(%r12)
	movb	$0, (%rax,%rbp)
	movq	%rbx, %rdi
	callq	no_interrupt
	movq	16(%r12), %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	memcpy
	movq	%r15, %rdi
	callq	free
.LBB53_17:
	movq	%r14, %rdi
	callq	no_interrupt
.LBB53_18:
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end53:
	.size	lfread, .Lfunc_end53-lfread
	.cfi_endproc

	.globl	lfwrite
	.p2align	4, 0x90
	.type	lfwrite,@function
lfwrite:                                # @lfwrite
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi370:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi371:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi372:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi373:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi374:
	.cfi_def_cfa_offset 48
.Lcfi375:
	.cfi_offset %rbx, -40
.Lcfi376:
	.cfi_offset %r12, -32
.Lcfi377:
	.cfi_offset %r14, -24
.Lcfi378:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movq	stdout(%rip), %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	get_c_file
	movq	%rax, %r14
	testq	%r12, %r12
	je	.LBB54_8
# BB#1:
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	movq	%r12, %rdi
	jne	.LBB54_3
# BB#2:
	movq	%r12, %rdi
	callq	car
	movq	%rax, %rdi
.LBB54_3:
	movq	%rsp, %rsi
	callq	get_c_string_dim
	movq	%rax, %r15
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	jne	.LBB54_9
# BB#4:
	movq	%r12, %rdi
	callq	cadr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB54_6
# BB#5:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB54_7
.LBB54_6:                               # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB54_7:                               # %get_c_long.exit
	cvttsd2si	8(%rbx), %rbx
	testq	%rbx, %rbx
	jg	.LBB54_11
	jmp	.LBB54_14
.LBB54_8:                               # %.critedge
	movq	%rsp, %rsi
	xorl	%edi, %edi
	callq	get_c_string_dim
	movq	%rax, %r15
.LBB54_9:                               # %.critedge22
	movq	(%rsp), %rbx
	testq	%rbx, %rbx
	jle	.LBB54_14
.LBB54_11:
	cmpq	(%rsp), %rbx
	jle	.LBB54_13
# BB#12:
	movl	$.L.str.45, %edi
	movq	%r12, %rsi
	callq	err
.LBB54_13:
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r12
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	callq	fwrite
	movq	%r12, %rdi
	callq	no_interrupt
.LBB54_14:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end54:
	.size	lfwrite, .Lfunc_end54-lfwrite
	.cfi_endproc

	.globl	lfflush
	.p2align	4, 0x90
	.type	lfflush,@function
lfflush:                                # @lfflush
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi379:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi380:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi381:
	.cfi_def_cfa_offset 32
.Lcfi382:
	.cfi_offset %rbx, -24
.Lcfi383:
	.cfi_offset %r14, -16
	movq	stdout(%rip), %rsi
	callq	get_c_file
	movq	%rax, %r14
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	fflush
	movq	%rbx, %rdi
	callq	no_interrupt
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end55:
	.size	lfflush, .Lfunc_end55-lfflush
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI56_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI56_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.text
	.globl	string_length
	.p2align	4, 0x90
	.type	string_length,@function
string_length:                          # @string_length
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi384:
	.cfi_def_cfa_offset 16
.Lcfi385:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB56_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$13, %eax
	je	.LBB56_3
.LBB56_2:                               # %.critedge
	movl	$.L.str.13, %edi
	movq	%rbx, %rsi
	callq	err
.LBB56_3:
	movq	16(%rbx), %rdi
	callq	strlen
	movd	%rax, %xmm1
	punpckldq	.LCPI56_0(%rip), %xmm1 # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	subpd	.LCPI56_1(%rip), %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	popq	%rbx
	jmp	flocons                 # TAILCALL
.Lfunc_end56:
	.size	string_length, .Lfunc_end56-string_length
	.cfi_endproc

	.globl	string_dim
	.p2align	4, 0x90
	.type	string_dim,@function
string_dim:                             # @string_dim
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi386:
	.cfi_def_cfa_offset 16
.Lcfi387:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB57_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$13, %eax
	je	.LBB57_3
.LBB57_2:                               # %.critedge
	movl	$.L.str.13, %edi
	movq	%rbx, %rsi
	callq	err
.LBB57_3:
	cvtsi2sdq	8(%rbx), %xmm0
	popq	%rbx
	jmp	flocons                 # TAILCALL
.Lfunc_end57:
	.size	string_dim, .Lfunc_end57-string_dim
	.cfi_endproc

	.globl	llength
	.p2align	4, 0x90
	.type	llength,@function
llength:                                # @llength
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi388:
	.cfi_def_cfa_offset 16
.Lcfi389:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB58_11
# BB#1:
	movswl	2(%rax), %ecx
	cmpl	$18, %ecx
	ja	.LBB58_10
# BB#2:
	xorl	%ebx, %ebx
	jmpq	*.LJTI58_0(,%rcx,8)
.LBB58_9:
	movq	8(%rax), %rbx
	jmp	.LBB58_12
.LBB58_10:
	movl	$.L.str.47, %edi
	movq	%rax, %rsi
	callq	err
.LBB58_11:                              # %nlength.exit
	xorl	%ebx, %ebx
.LBB58_12:                              # %nlength.exit
	cvtsi2sdq	%rbx, %xmm0
	popq	%rbx
	jmp	flocons                 # TAILCALL
.LBB58_3:                               # %.lr.ph.i.preheader
	movzwl	%cx, %ecx
	xorl	%ebx, %ebx
	cmpl	$1, %ecx
	jne	.LBB58_7
# BB#4:                                 # %.lr.ph.preheader
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB58_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rcx), %rcx
	incq	%rbx
	testq	%rcx, %rcx
	je	.LBB58_12
# BB#6:                                 # %..lr.ph_crit_edge.i
                                        #   in Loop: Header=BB58_5 Depth=1
	movzwl	2(%rcx), %edx
	cmpl	$1, %edx
	je	.LBB58_5
.LBB58_7:                               # %.lr.ph.i._crit_edge
	movl	$.L.str.46, %edi
	movq	%rax, %rsi
	callq	err
	jmp	.LBB58_12
.LBB58_8:
	movq	16(%rax), %rdi
	callq	strlen
	movq	%rax, %rbx
	jmp	.LBB58_12
.Lfunc_end58:
	.size	llength, .Lfunc_end58-llength
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI58_0:
	.quad	.LBB58_12
	.quad	.LBB58_3
	.quad	.LBB58_10
	.quad	.LBB58_10
	.quad	.LBB58_10
	.quad	.LBB58_10
	.quad	.LBB58_10
	.quad	.LBB58_10
	.quad	.LBB58_10
	.quad	.LBB58_10
	.quad	.LBB58_10
	.quad	.LBB58_10
	.quad	.LBB58_10
	.quad	.LBB58_8
	.quad	.LBB58_9
	.quad	.LBB58_9
	.quad	.LBB58_9
	.quad	.LBB58_10
	.quad	.LBB58_9

	.text
	.globl	number2string
	.p2align	4, 0x90
	.type	number2string,@function
number2string:                          # @number2string
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi390:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi391:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi392:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi393:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi394:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi395:
	.cfi_def_cfa_offset 56
	subq	$1016, %rsp             # imm = 0x3F8
.Lcfi396:
	.cfi_def_cfa_offset 1072
.Lcfi397:
	.cfi_offset %rbx, -56
.Lcfi398:
	.cfi_offset %r12, -48
.Lcfi399:
	.cfi_offset %r13, -40
.Lcfi400:
	.cfi_offset %r14, -32
.Lcfi401:
	.cfi_offset %r15, -24
.Lcfi402:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %rbp
	movq	%rsi, %r12
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB59_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB59_3
.LBB59_2:                               # %.thread
	movl	$.L.str.48, %edi
	movq	%rbx, %rsi
	callq	err
.LBB59_3:
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	$-1, %r15
	testq	%rbp, %rbp
	movq	$-1, %r14
	je	.LBB59_8
# BB#4:
	movzwl	2(%rbp), %eax
	cmpl	$2, %eax
	je	.LBB59_6
# BB#5:                                 # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbp, %rsi
	callq	err
.LBB59_6:
	cvttsd2si	8(%rbp), %r14
	cmpq	$101, %r14
	jl	.LBB59_8
# BB#7:
	movl	$.L.str.49, %edi
	movq	%rbp, %rsi
	callq	err
.LBB59_8:                               # %.thread65
	testq	%r13, %r13
	je	.LBB59_13
# BB#9:
	movzwl	2(%r13), %eax
	cmpl	$2, %eax
	je	.LBB59_11
# BB#10:                                # %.critedge.i60
	movl	$.L.str.44, %edi
	movq	%r13, %rsi
	callq	err
.LBB59_11:
	cvttsd2si	8(%r13), %r15
	cmpq	$101, %r15
	jl	.LBB59_13
# BB#12:
	movl	$.L.str.50, %edi
	movq	%r13, %rsi
	callq	err
.LBB59_13:                              # %.thread66
	testq	%r12, %r12
	movq	sym_e(%rip), %rax
	je	.LBB59_16
# BB#14:                                # %.thread66
	cmpq	%r12, %rax
	je	.LBB59_16
# BB#15:                                # %.thread66
	cmpq	%r12, sym_f(%rip)
	je	.LBB59_16
# BB#24:
	movzwl	2(%r12), %eax
	cmpl	$2, %eax
	je	.LBB59_26
# BB#25:                                # %.critedge.i63
	movl	$.L.str.44, %edi
	movq	%r12, %rsi
	callq	err
.LBB59_26:                              # %get_c_long.exit64
	cvttsd2si	8(%r12), %rax
	cmpq	$16, %rax
	ja	.LBB59_31
# BB#27:                                # %get_c_long.exit64
	movl	$66816, %ecx            # imm = 0x10500
	btq	%rax, %rcx
	jae	.LBB59_31
# BB#28:
	testq	%r14, %r14
	js	.LBB59_30
# BB#29:
	cmpq	$8, %rax
	movl	$.L.str.63, %ecx
	movl	$.L.str.64, %edx
	cmoveq	%rcx, %rdx
	cmpq	$10, %rax
	movl	$.L.str.62, %esi
	cmovneq	%rdx, %rsi
	cvttsd2si	8(%rsp), %rcx   # 8-byte Folded Reload
	leaq	16(%rsp), %rdi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	sprintf
	jmp	.LBB59_32
.LBB59_16:
	movq	%r15, %rcx
	orq	%r14, %rcx
	js	.LBB59_18
# BB#17:
	cmpq	%r12, %rax
	movl	$.L.str.52, %eax
	movl	$.L.str.53, %ecx
	cmoveq	%rax, %rcx
	testq	%r12, %r12
	movl	$.L.str.51, %esi
	cmovneq	%rcx, %rsi
	leaq	16(%rsp), %rdi
	movb	$1, %al
	movq	%r14, %rdx
	movq	%r15, %rcx
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sprintf
	jmp	.LBB59_32
.LBB59_18:
	testq	%r14, %r14
	js	.LBB59_21
# BB#19:
	cmpq	%r12, %rax
	movl	$.L.str.55, %eax
	movl	$.L.str.56, %ecx
	cmoveq	%rax, %rcx
	testq	%r12, %r12
	movl	$.L.str.54, %esi
	cmovneq	%rcx, %rsi
	leaq	16(%rsp), %rdi
	movb	$1, %al
	movq	%r14, %rdx
	jmp	.LBB59_20
.LBB59_30:
	cmpq	$8, %rax
	movl	$.L.str.65, %ecx
	movl	$.L.str.66, %edx
	cmoveq	%rcx, %rdx
	cmpq	$10, %rax
	movl	$.L.str.9, %esi
	cmovneq	%rdx, %rsi
	cvttsd2si	8(%rsp), %rdx   # 8-byte Folded Reload
	leaq	16(%rsp), %rdi
	xorl	%eax, %eax
	callq	sprintf
	jmp	.LBB59_32
.LBB59_31:
	movl	$.L.str.67, %edi
	movq	%r12, %rsi
	callq	err
	jmp	.LBB59_32
.LBB59_21:
	testq	%r15, %r15
	js	.LBB59_23
# BB#22:
	cmpq	%r12, %rax
	movl	$.L.str.58, %eax
	movl	$.L.str.59, %ecx
	cmoveq	%rax, %rcx
	testq	%r12, %r12
	movl	$.L.str.57, %esi
	cmovneq	%rcx, %rsi
	leaq	16(%rsp), %rdi
	movb	$1, %al
	movq	%r15, %rdx
.LBB59_20:
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sprintf
.LBB59_32:
	leaq	16(%rsp), %rdi
	callq	strlen
	movq	%rax, %r15
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %rbx
	movw	$13, 2(%rbx)
	cmpq	$-1, %r15
	jne	.LBB59_34
# BB#33:
	leaq	16(%rsp), %rdi
	callq	strlen
	movq	%rax, %r15
.LBB59_34:                              # %strcons.exit
	leaq	1(%r15), %rdi
	callq	must_malloc
	movq	%rax, 16(%rbx)
	movq	%r15, 8(%rbx)
	leaq	16(%rsp), %rsi
	movq	%rax, %rdi
	movq	%r15, %rdx
	callq	memcpy
	movq	16(%rbx), %rax
	movb	$0, (%rax,%r15)
	movq	%r14, %rdi
	callq	no_interrupt
	movq	%rbx, %rax
	addq	$1016, %rsp             # imm = 0x3F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB59_23:
	cmpq	%r12, %rax
	movl	$.L.str.60, %eax
	movl	$.L.str.61, %ecx
	cmoveq	%rax, %rcx
	testq	%r12, %r12
	movl	$.L.str.6, %esi
	cmovneq	%rcx, %rsi
	leaq	16(%rsp), %rdi
	movb	$1, %al
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sprintf
	jmp	.LBB59_32
.Lfunc_end59:
	.size	number2string, .Lfunc_end59-number2string
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI60_0:
	.quad	-4591419820104220672    # double -48
.LCPI60_1:
	.quad	-4589097651546357760    # double -65
.LCPI60_2:
	.quad	4621819117588971520     # double 10
	.text
	.globl	string2number
	.p2align	4, 0x90
	.type	string2number,@function
string2number:                          # @string2number
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi403:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi404:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi405:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi406:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi407:
	.cfi_def_cfa_offset 64
.Lcfi408:
	.cfi_offset %rbx, -40
.Lcfi409:
	.cfi_offset %r12, -32
.Lcfi410:
	.cfi_offset %r14, -24
.Lcfi411:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	$0, (%rsp)
	callq	get_c_string
	movq	%rax, %r15
	testq	%r14, %r14
	je	.LBB60_1
# BB#2:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB60_4
# BB#3:                                 # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%r14, %rsi
	callq	err
.LBB60_4:                               # %get_c_long.exit
	cvttsd2si	8(%r14), %r12
	cmpq	$8, %r12
	je	.LBB60_9
# BB#5:                                 # %get_c_long.exit
	cmpq	$16, %r12
	je	.LBB60_10
# BB#6:                                 # %get_c_long.exit
	cmpq	$10, %r12
	jne	.LBB60_11
# BB#7:
	movq	%rsp, %rdx
	movl	$.L.str.9, %esi
	jmp	.LBB60_8
.LBB60_1:
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	strtod
	jmp	.LBB60_20
.LBB60_9:
	movq	%rsp, %rdx
	movl	$.L.str.65, %esi
	jmp	.LBB60_8
.LBB60_10:
	movq	%rsp, %rdx
	movl	$.L.str.68, %esi
.LBB60_8:                               # %.loopexit
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sscanf
	cvtsi2sdq	(%rsp), %xmm0
.LBB60_20:                              # %.loopexit
	callq	flocons
.LBB60_21:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB60_11:
	leaq	-1(%r12), %rax
	cmpq	$15, %rax
	ja	.LBB60_22
# BB#12:                                # %.preheader
	movb	(%r15), %bl
	testb	%bl, %bl
	je	.LBB60_13
# BB#14:                                # %.lr.ph
	callq	__ctype_b_loc
	movq	(%rax), %r14
	cvtsi2sdq	%r12, %xmm2
	incq	%r15
	xorpd	%xmm0, %xmm0
	movsd	.LCPI60_0(%rip), %xmm3  # xmm3 = mem[0],zero
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB60_15:                              # =>This Inner Loop Header: Depth=1
	movsbq	%bl, %rbx
	movzwl	(%r14,%rbx,2), %eax
	testb	$8, %ah
	jne	.LBB60_16
# BB#17:                                #   in Loop: Header=BB60_15 Depth=1
	testb	$16, %ah
	je	.LBB60_19
# BB#18:                                #   in Loop: Header=BB60_15 Depth=1
	mulsd	%xmm2, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	callq	__ctype_toupper_loc
	movsd	.LCPI60_0(%rip), %xmm3  # xmm3 = mem[0],zero
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movq	(%rax), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	(%rax,%rbx,4), %xmm0
	addsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	addsd	.LCPI60_1(%rip), %xmm0
	addsd	.LCPI60_2(%rip), %xmm0
	jmp	.LBB60_19
	.p2align	4, 0x90
.LBB60_16:                              #   in Loop: Header=BB60_15 Depth=1
	mulsd	%xmm2, %xmm0
	cvtsi2sdl	%ebx, %xmm1
	addsd	%xmm0, %xmm1
	addsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm0
.LBB60_19:                              #   in Loop: Header=BB60_15 Depth=1
	movzbl	(%r15), %ebx
	incq	%r15
	testb	%bl, %bl
	jne	.LBB60_15
	jmp	.LBB60_20
.LBB60_22:
	movl	$.L.str.67, %edi
	movq	%r14, %rsi
	callq	err
	jmp	.LBB60_21
.LBB60_13:
	xorpd	%xmm0, %xmm0
	jmp	.LBB60_20
.Lfunc_end60:
	.size	string2number, .Lfunc_end60-string2number
	.cfi_endproc

	.globl	lstrcmp
	.p2align	4, 0x90
	.type	lstrcmp,@function
lstrcmp:                                # @lstrcmp
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi412:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi413:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi414:
	.cfi_def_cfa_offset 32
.Lcfi415:
	.cfi_offset %rbx, -24
.Lcfi416:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	callq	get_c_string
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	get_c_string
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcmp
	cvtsi2sdl	%eax, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	flocons                 # TAILCALL
.Lfunc_end61:
	.size	lstrcmp, .Lfunc_end61-lstrcmp
	.cfi_endproc

	.globl	chk_string
	.p2align	4, 0x90
	.type	chk_string,@function
chk_string:                             # @chk_string
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB62_3
# BB#1:
	movzwl	2(%rax), %ecx
	cmpl	$13, %ecx
	jne	.LBB62_3
# BB#2:
	movq	16(%rax), %rcx
	movq	%rcx, (%rsi)
	movq	8(%rax), %rax
	movq	%rax, (%rdx)
	retq
.LBB62_3:                               # %.critedge
	movl	$.L.str.13, %edi
	movq	%rax, %rsi
	jmp	err                     # TAILCALL
.Lfunc_end62:
	.size	chk_string, .Lfunc_end62-chk_string
	.cfi_endproc

	.globl	lstrcpy
	.p2align	4, 0x90
	.type	lstrcpy,@function
lstrcpy:                                # @lstrcpy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi417:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi418:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi419:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi420:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi421:
	.cfi_def_cfa_offset 48
.Lcfi422:
	.cfi_offset %rbx, -48
.Lcfi423:
	.cfi_offset %r12, -40
.Lcfi424:
	.cfi_offset %r13, -32
.Lcfi425:
	.cfi_offset %r14, -24
.Lcfi426:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB63_3
# BB#1:
	movzwl	2(%rax), %ecx
	cmpl	$13, %ecx
	jne	.LBB63_3
# BB#2:
	movq	8(%rax), %r13
	movq	16(%rax), %r15
	jmp	.LBB63_4
.LBB63_3:                               # %.critedge.i
	movl	$.L.str.13, %edi
	movq	%rax, %rsi
	callq	err
                                        # implicit-def: %R13
                                        # implicit-def: %R15
.LBB63_4:                               # %chk_string.exit
	movq	%r14, %rdi
	callq	get_c_string
	movq	%rax, %r12
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rbx
	cmpq	%r13, %rbx
	jle	.LBB63_6
# BB#5:
	movl	$.L.str.69, %edi
	movq	%r14, %rsi
	callq	err
.LBB63_6:
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movb	$0, (%r15,%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end63:
	.size	lstrcpy, .Lfunc_end63-lstrcpy
	.cfi_endproc

	.globl	lstrcat
	.p2align	4, 0x90
	.type	lstrcat,@function
lstrcat:                                # @lstrcat
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi427:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi428:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi429:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi430:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi431:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi432:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi433:
	.cfi_def_cfa_offset 64
.Lcfi434:
	.cfi_offset %rbx, -56
.Lcfi435:
	.cfi_offset %r12, -48
.Lcfi436:
	.cfi_offset %r13, -40
.Lcfi437:
	.cfi_offset %r14, -32
.Lcfi438:
	.cfi_offset %r15, -24
.Lcfi439:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB64_3
# BB#1:
	movzwl	2(%rax), %ecx
	cmpl	$13, %ecx
	jne	.LBB64_3
# BB#2:
	movq	8(%rax), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	16(%rax), %r15
	jmp	.LBB64_4
.LBB64_3:                               # %.critedge.i
	movl	$.L.str.13, %edi
	movq	%rax, %rsi
	callq	err
                                        # implicit-def: %RAX
	movq	%rax, (%rsp)            # 8-byte Spill
                                        # implicit-def: %R15
.LBB64_4:                               # %chk_string.exit
	movq	%r14, %rdi
	callq	get_c_string
	movq	%rax, %r12
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%rbp, %r13
	addq	%rbx, %r13
	cmpq	(%rsp), %r13            # 8-byte Folded Reload
	jle	.LBB64_6
# BB#5:
	movl	$.L.str.69, %edi
	movq	%r14, %rsi
	callq	err
.LBB64_6:
	addq	%r15, %rbx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movb	$0, (%r15,%r13)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end64:
	.size	lstrcat, .Lfunc_end64-lstrcat
	.cfi_endproc

	.globl	lstrbreakup
	.p2align	4, 0x90
	.type	lstrbreakup,@function
lstrbreakup:                            # @lstrbreakup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi440:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi441:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi442:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi443:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi444:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi445:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi446:
	.cfi_def_cfa_offset 80
.Lcfi447:
	.cfi_offset %rbx, -56
.Lcfi448:
	.cfi_offset %r12, -48
.Lcfi449:
	.cfi_offset %r13, -40
.Lcfi450:
	.cfi_offset %r14, -32
.Lcfi451:
	.cfi_offset %r15, -24
.Lcfi452:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	callq	get_c_string
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	get_c_string
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	callq	strlen
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpb	$0, (%rbx)
	je	.LBB65_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB65_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	strstr
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB65_5
# BB#4:                                 #   in Loop: Header=BB65_3 Depth=1
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r13
	addq	%rbx, %r13
.LBB65_5:                               #   in Loop: Header=BB65_3 Depth=1
	movq	%r13, %r14
	subq	%rbx, %r14
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %rbp
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r15
	movw	$13, 2(%r15)
	cmpq	$-1, %r14
	jne	.LBB65_7
# BB#6:                                 #   in Loop: Header=BB65_3 Depth=1
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r14
.LBB65_7:                               #   in Loop: Header=BB65_3 Depth=1
	leaq	1(%r14), %rdi
	callq	must_malloc
	movq	%rax, 16(%r15)
	movq	%r14, 8(%r15)
	testq	%rbx, %rbx
	je	.LBB65_9
# BB#8:                                 #   in Loop: Header=BB65_3 Depth=1
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	memcpy
	movq	16(%r15), %rax
.LBB65_9:                               # %strcons.exit
                                        #   in Loop: Header=BB65_3 Depth=1
	movb	$0, (%rax,%r14)
	movq	%rbp, %rdi
	callq	no_interrupt
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	cons
	movq	%rax, %r12
	cmpb	$0, (%r13)
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%r13,%rax), %rbx
	cmoveq	%r13, %rbx
	cmpb	$0, (%rbx)
	jne	.LBB65_3
	jmp	.LBB65_10
.LBB65_1:
	xorl	%r12d, %r12d
.LBB65_10:                              # %._crit_edge
	movq	%r12, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	nreverse                # TAILCALL
.Lfunc_end65:
	.size	lstrbreakup, .Lfunc_end65-lstrbreakup
	.cfi_endproc

	.globl	lstrunbreakup
	.p2align	4, 0x90
	.type	lstrunbreakup,@function
lstrunbreakup:                          # @lstrunbreakup
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi453:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi454:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi455:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi456:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi457:
	.cfi_def_cfa_offset 48
.Lcfi458:
	.cfi_offset %rbx, -48
.Lcfi459:
	.cfi_offset %r12, -40
.Lcfi460:
	.cfi_offset %r13, -32
.Lcfi461:
	.cfi_offset %r14, -24
.Lcfi462:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorl	%r12d, %r12d
	testq	%r15, %r15
	je	.LBB66_5
# BB#1:                                 # %.lr.ph.preheader
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB66_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	car
	movq	%rax, %r13
	cmpq	%r15, %rbx
	je	.LBB66_4
# BB#3:                                 #   in Loop: Header=BB66_2 Depth=1
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	cons
	movq	%rax, %r12
.LBB66_4:                               #   in Loop: Header=BB66_2 Depth=1
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	cons
	movq	%rax, %r12
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB66_2
.LBB66_5:                               # %._crit_edge
	movq	%r12, %rdi
	callq	nreverse
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	string_append           # TAILCALL
.Lfunc_end66:
	.size	lstrunbreakup, .Lfunc_end66-lstrunbreakup
	.cfi_endproc

	.globl	stringp
	.p2align	4, 0x90
	.type	stringp,@function
stringp:                                # @stringp
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB67_2
# BB#1:
	movzwl	2(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	$13, %ecx
	cmoveq	sym_t(%rip), %rax
	retq
.LBB67_2:                               # %.thread
	xorl	%eax, %eax
	retq
.Lfunc_end67:
	.size	stringp, .Lfunc_end67-stringp
	.cfi_endproc

	.globl	base64encode
	.p2align	4, 0x90
	.type	base64encode,@function
base64encode:                           # @base64encode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi463:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi464:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi465:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi466:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi467:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi468:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi469:
	.cfi_def_cfa_offset 80
.Lcfi470:
	.cfi_offset %rbx, -56
.Lcfi471:
	.cfi_offset %r12, -48
.Lcfi472:
	.cfi_offset %r13, -40
.Lcfi473:
	.cfi_offset %r14, -32
.Lcfi474:
	.cfi_offset %r15, -24
.Lcfi475:
	.cfi_offset %rbp, -16
	leaq	16(%rsp), %rsi
	callq	get_c_string_dim
	movq	%rax, %rbx
	movq	16(%rsp), %r15
	movabsq	$6148914691236517206, %rcx # imm = 0x5555555555555556
	movq	%r15, %rax
	imulq	%rcx
	movq	%rdx, %rbp
	shrq	$63, %rbp
	addq	%rdx, %rbp
	leaq	(%rbp,%rbp,2), %rax
	xorl	%r14d, %r14d
	movq	%r15, %rcx
	subq	%rax, %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	setne	%r14b
	addq	%rbp, %r14
	leaq	(,%r14,4), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r12
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r13
	movw	$13, 2(%r13)
	leaq	1(,%r14,4), %rdi
	callq	must_malloc
	movq	%rax, 16(%r13)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 8(%r13)
	movb	$0, (%rax,%r14,4)
	movq	%r12, %rdi
	callq	no_interrupt
	movq	%r13, %rdi
	callq	get_c_string
	cmpq	$3, %r15
	jl	.LBB68_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB68_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %edx
	shrq	$2, %rdx
	movzbl	.L.str.232(%rdx), %edx
	movb	%dl, (%rax)
	movzbl	(%rbx), %edx
	shlb	$4, %dl
	andb	$48, %dl
	movzbl	%dl, %edx
	movzbl	1(%rbx), %esi
	shrl	$4, %esi
	orl	%edx, %esi
	movzbl	.L.str.232(%rsi), %edx
	movb	%dl, 1(%rax)
	movzbl	1(%rbx), %edx
	shlb	$2, %dl
	andb	$60, %dl
	movzbl	%dl, %edx
	movzbl	2(%rbx), %esi
	shrl	$6, %esi
	orl	%edx, %esi
	movzbl	.L.str.232(%rsi), %edx
	movb	%dl, 2(%rax)
	movzbl	2(%rbx), %edx
	andl	$63, %edx
	movzbl	.L.str.232(%rdx), %edx
	movb	%dl, 3(%rax)
	addq	$4, %rax
	incq	%rcx
	addq	$3, %rbx
	cmpq	%rbp, %rcx
	jl	.LBB68_2
.LBB68_3:
	movq	(%rsp), %rcx            # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB68_9
# BB#4:                                 # %._crit_edge
	cmpq	$2, %rcx
	je	.LBB68_7
# BB#5:                                 # %._crit_edge
	cmpq	$1, %rcx
	jne	.LBB68_8
# BB#6:
	movzbl	(%rbx), %ecx
	shrq	$2, %rcx
	movb	.L.str.232(%rcx), %cl
	movb	%cl, (%rax)
	movb	(%rbx), %cl
	shlb	$4, %cl
	andb	$48, %cl
	movzbl	%cl, %ecx
	movb	.L.str.232(%rcx), %cl
	movb	%cl, 1(%rax)
	movb	$61, 2(%rax)
	movb	$61, 3(%rax)
	jmp	.LBB68_9
.LBB68_7:
	movzbl	(%rbx), %ecx
	shrq	$2, %rcx
	movb	.L.str.232(%rcx), %cl
	movb	%cl, (%rax)
	movb	(%rbx), %cl
	shlb	$4, %cl
	andb	$48, %cl
	movzbl	%cl, %ecx
	movzbl	1(%rbx), %edx
	shrl	$4, %edx
	orl	%ecx, %edx
	movb	.L.str.232(%rdx), %cl
	movb	%cl, 1(%rax)
	movb	1(%rbx), %cl
	shlb	$2, %cl
	andb	$60, %cl
	movzbl	%cl, %ecx
	movb	.L.str.232(%rcx), %cl
	movb	%cl, 2(%rax)
	movb	$61, 3(%rax)
	jmp	.LBB68_9
.LBB68_8:
	callq	errswitch
.LBB68_9:
	movq	%r13, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end68:
	.size	base64encode, .Lfunc_end68-base64encode
	.cfi_endproc

	.globl	base64decode
	.p2align	4, 0x90
	.type	base64decode,@function
base64decode:                           # @base64decode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi476:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi477:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi478:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi479:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi480:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi481:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi482:
	.cfi_def_cfa_offset 64
.Lcfi483:
	.cfi_offset %rbx, -56
.Lcfi484:
	.cfi_offset %r12, -48
.Lcfi485:
	.cfi_offset %r13, -40
.Lcfi486:
	.cfi_offset %r14, -32
.Lcfi487:
	.cfi_offset %r15, -24
.Lcfi488:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	base64_decode_table(%rip), %rbx
	callq	get_c_string
	movq	%rax, %r13
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB69_5
# BB#1:
	testb	$3, %r14b
	je	.LBB69_3
# BB#2:
	movl	$.L.str.70, %edi
	movq	%r15, %rsi
	callq	err
.LBB69_3:
	cmpb	$61, -1(%r13,%r14)
	jne	.LBB69_7
# BB#4:
	xorl	%eax, %eax
	cmpb	$61, -2(%r13,%r14)
	setne	%al
	incq	%rax
	jmp	.LBB69_8
.LBB69_5:
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %rbx
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r14
	movw	$13, 2(%r14)
	movl	$1, %edi
	callq	must_malloc
	movq	%rax, 16(%r14)
	movq	$0, 8(%r14)
	movb	$0, (%rax)
	movq	%rbx, %rdi
	callq	no_interrupt
	jmp	.LBB69_6
.LBB69_7:
	xorl	%eax, %eax
.LBB69_8:
	movq	%r14, %r15
	sarq	$63, %r15
	shrq	$62, %r15
	addq	%r14, %r15
	sarq	$2, %r15
	cmpq	$1, %rax
	adcq	$-1, %r15
	leaq	(%r15,%r15,2), %r12
	movq	%rax, (%rsp)            # 8-byte Spill
	addq	%rax, %r12
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %rbp
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r14
	movw	$13, 2(%r14)
	cmpq	$-1, %r12
	jne	.LBB69_10
# BB#9:
	xorl	%edi, %edi
	callq	strlen
	movq	%rax, %r12
.LBB69_10:                              # %strcons.exit
	leaq	1(%r12), %rdi
	callq	must_malloc
	movq	%rax, 16(%r14)
	movq	%r12, 8(%r14)
	movb	$0, (%rax,%r12)
	movq	%rbp, %rdi
	callq	no_interrupt
	movq	%r14, %rdi
	callq	get_c_string
	testq	%r15, %r15
	jle	.LBB69_17
# BB#11:                                # %.lr.ph.preheader
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	.p2align	4, 0x90
.LBB69_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r13), %edx
	movsbq	(%r8,%rdx), %rdi
	cmpq	$63, %rdi
	ja	.LBB69_23
# BB#13:                                #   in Loop: Header=BB69_12 Depth=1
	movzbl	1(%r13), %edx
	movsbq	(%r8,%rdx), %rdx
	cmpq	$63, %rdx
	ja	.LBB69_23
# BB#14:                                #   in Loop: Header=BB69_12 Depth=1
	movzbl	2(%r13), %esi
	movsbq	(%r8,%rsi), %rsi
	cmpq	$63, %rsi
	ja	.LBB69_23
# BB#15:                                #   in Loop: Header=BB69_12 Depth=1
	movzbl	3(%r13), %ebx
	movzbl	(%r8,%rbx), %ebx
	cmpb	$63, %bl
	ja	.LBB69_27
# BB#16:                                #   in Loop: Header=BB69_12 Depth=1
	shll	$2, %edi
	movl	%edx, %ebp
	shrl	$4, %ebp
	orl	%edi, %ebp
	movb	%bpl, (%rax)
	shll	$4, %edx
	movl	%esi, %edi
	shrl	$2, %edi
	orl	%edx, %edi
	movb	%dil, 1(%rax)
	shlb	$6, %sil
	orb	%sil, %bl
	movb	%bl, 2(%rax)
	addq	$3, %rax
	incq	%rcx
	addq	$4, %r13
	cmpq	%r15, %rcx
	jl	.LBB69_12
	jmp	.LBB69_18
.LBB69_17:
	movq	%rbx, %r8
.LBB69_18:                              # %._crit_edge
	movq	(%rsp), %rcx            # 8-byte Reload
	andb	$3, %cl
	je	.LBB69_6
# BB#19:                                # %._crit_edge
	cmpb	$2, %cl
	je	.LBB69_24
# BB#20:                                # %._crit_edge
	cmpb	$1, %cl
	jne	.LBB69_26
# BB#21:
	movzbl	(%r13), %ecx
	movsbq	(%r8,%rcx), %rcx
	cmpq	$63, %rcx
	jbe	.LBB69_28
.LBB69_23:
	xorl	%r14d, %r14d
	jmp	.LBB69_6
.LBB69_24:
	movzbl	(%r13), %ecx
	movsbq	(%r8,%rcx), %rdx
	cmpq	$63, %rdx
	jbe	.LBB69_30
# BB#25:
	xorl	%r14d, %r14d
	jmp	.LBB69_6
.LBB69_26:
	callq	errswitch
	jmp	.LBB69_6
.LBB69_27:
	xorl	%r14d, %r14d
.LBB69_6:                               # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB69_28:
	movzbl	1(%r13), %edx
	movsbq	(%r8,%rdx), %rdx
	cmpq	$63, %rdx
	jbe	.LBB69_32
# BB#29:
	xorl	%r14d, %r14d
	jmp	.LBB69_6
.LBB69_30:
	movzbl	1(%r13), %ecx
	movsbq	(%r8,%rcx), %rcx
	cmpq	$63, %rcx
	jbe	.LBB69_33
# BB#31:
	xorl	%r14d, %r14d
	jmp	.LBB69_6
.LBB69_32:
	shll	$2, %ecx
	shrl	$4, %edx
	orl	%ecx, %edx
	movb	%dl, (%rax)
	jmp	.LBB69_6
.LBB69_33:
	movzbl	2(%r13), %esi
	movsbq	(%r8,%rsi), %rsi
	cmpq	$63, %rsi
	jbe	.LBB69_35
# BB#34:
	xorl	%r14d, %r14d
	jmp	.LBB69_6
.LBB69_35:
	shll	$2, %edx
	movl	%ecx, %edi
	shrl	$4, %edi
	orl	%edx, %edi
	movb	%dil, (%rax)
	shll	$4, %ecx
	shrl	$2, %esi
	orl	%ecx, %esi
	movb	%sil, 1(%rax)
	jmp	.LBB69_6
.Lfunc_end69:
	.size	base64decode, .Lfunc_end69-base64decode
	.cfi_endproc

	.globl	memq
	.p2align	4, 0x90
	.type	memq,@function
memq:                                   # @memq
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB70_5
# BB#1:                                 # %.lr.ph.preheader
	movq	%rsi, %rax
	.p2align	4, 0x90
.LBB70_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB70_7
# BB#3:                                 #   in Loop: Header=BB70_2 Depth=1
	cmpq	%rdi, 8(%rax)
	je	.LBB70_6
# BB#4:                                 #   in Loop: Header=BB70_2 Depth=1
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.LBB70_2
.LBB70_5:
	xorl	%eax, %eax
.LBB70_6:                               # %.thread13
	retq
.LBB70_7:
	movl	$.L.str.71, %edi
	jmp	err                     # TAILCALL
.Lfunc_end70:
	.size	memq, .Lfunc_end70-memq
	.cfi_endproc

	.globl	member
	.p2align	4, 0x90
	.type	member,@function
member:                                 # @member
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi489:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi490:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi491:
	.cfi_def_cfa_offset 32
.Lcfi492:
	.cfi_offset %rbx, -32
.Lcfi493:
	.cfi_offset %r14, -24
.Lcfi494:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB71_5
# BB#1:                                 # %.lr.ph.preheader
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB71_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB71_7
# BB#3:                                 #   in Loop: Header=BB71_2 Depth=1
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	equal
	testq	%rax, %rax
	jne	.LBB71_6
# BB#4:                                 #   in Loop: Header=BB71_2 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB71_2
.LBB71_5:
	xorl	%ebx, %ebx
.LBB71_6:                               # %.thread13
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB71_7:
	movl	$.L.str.72, %edi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	err                     # TAILCALL
.Lfunc_end71:
	.size	member, .Lfunc_end71-member
	.cfi_endproc

	.globl	memv
	.p2align	4, 0x90
	.type	memv,@function
memv:                                   # @memv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi495:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi496:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi497:
	.cfi_def_cfa_offset 32
.Lcfi498:
	.cfi_offset %rbx, -32
.Lcfi499:
	.cfi_offset %r14, -24
.Lcfi500:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB72_5
# BB#1:                                 # %.lr.ph.preheader
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB72_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB72_7
# BB#3:                                 #   in Loop: Header=BB72_2 Depth=1
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	eql
	testq	%rax, %rax
	jne	.LBB72_6
# BB#4:                                 #   in Loop: Header=BB72_2 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB72_2
.LBB72_5:
	xorl	%ebx, %ebx
.LBB72_6:                               # %.thread13
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB72_7:
	movl	$.L.str.73, %edi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	err                     # TAILCALL
.Lfunc_end72:
	.size	memv, .Lfunc_end72-memv
	.cfi_endproc

	.globl	nth
	.p2align	4, 0x90
	.type	nth,@function
nth:                                    # @nth
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi501:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi502:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi503:
	.cfi_def_cfa_offset 32
.Lcfi504:
	.cfi_offset %rbx, -24
.Lcfi505:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB73_2
# BB#1:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB73_3
.LBB73_2:                               # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%r14, %rsi
	callq	err
.LBB73_3:                               # %get_c_long.exit
	cvttsd2si	8(%r14), %rax
	testq	%rbx, %rbx
	sete	%dl
	testq	%rax, %rax
	jle	.LBB73_8
# BB#4:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB73_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testb	$1, %dl
	jne	.LBB73_11
# BB#6:                                 #   in Loop: Header=BB73_5 Depth=1
	movzwl	2(%rbx), %edx
	cmpl	$1, %edx
	jne	.LBB73_11
# BB#7:                                 #   in Loop: Header=BB73_5 Depth=1
	movq	16(%rbx), %rbx
	incq	%rcx
	testq	%rbx, %rbx
	sete	%dl
	cmpq	%rax, %rcx
	jl	.LBB73_5
.LBB73_8:                               # %.critedge
	testb	%dl, %dl
	jne	.LBB73_11
# BB#9:                                 # %.critedge.thread19
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB73_11
# BB#10:
	movq	8(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB73_11:                              # %.critedge18
	movl	$.L.str.74, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	err                     # TAILCALL
.Lfunc_end73:
	.size	nth, .Lfunc_end73-nth
	.cfi_endproc

	.globl	lref_default
	.p2align	4, 0x90
	.type	lref_default,@function
lref_default:                           # @lref_default
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi506:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi507:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi508:
	.cfi_def_cfa_offset 32
.Lcfi509:
	.cfi_offset %rbx, -32
.Lcfi510:
	.cfi_offset %r14, -24
.Lcfi511:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	testq	%r15, %r15
	je	.LBB74_2
# BB#1:
	movzwl	2(%r15), %eax
	cmpl	$2, %eax
	je	.LBB74_3
.LBB74_2:                               # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%r15, %rsi
	callq	err
.LBB74_3:                               # %get_c_long.exit
	cvttsd2si	8(%r15), %rax
	testq	%rbx, %rbx
	sete	%dl
	testq	%rax, %rax
	jle	.LBB74_8
# BB#4:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB74_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testb	$1, %dl
	jne	.LBB74_11
# BB#6:                                 #   in Loop: Header=BB74_5 Depth=1
	movzwl	2(%rbx), %edx
	cmpl	$1, %edx
	jne	.LBB74_11
# BB#7:                                 #   in Loop: Header=BB74_5 Depth=1
	movq	16(%rbx), %rbx
	incq	%rcx
	testq	%rbx, %rbx
	sete	%dl
	cmpq	%rax, %rcx
	jl	.LBB74_5
.LBB74_8:                               # %.critedge
	testb	%dl, %dl
	jne	.LBB74_11
# BB#9:                                 # %.critedge.thread20
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB74_11
# BB#10:
	movq	8(%rbx), %rax
	jmp	.LBB74_13
.LBB74_11:                              # %.critedge19
	testq	%r14, %r14
	je	.LBB74_12
# BB#14:
	xorl	%esi, %esi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	lapply                  # TAILCALL
.LBB74_12:
	xorl	%eax, %eax
.LBB74_13:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end74:
	.size	lref_default, .Lfunc_end74-lref_default
	.cfi_endproc

	.globl	larg_default
	.p2align	4, 0x90
	.type	larg_default,@function
larg_default:                           # @larg_default
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi512:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi513:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi514:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi515:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi516:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi517:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi518:
	.cfi_def_cfa_offset 64
.Lcfi519:
	.cfi_offset %rbx, -56
.Lcfi520:
	.cfi_offset %r12, -48
.Lcfi521:
	.cfi_offset %r13, -40
.Lcfi522:
	.cfi_offset %r14, -32
.Lcfi523:
	.cfi_offset %r15, -24
.Lcfi524:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%rbx, %rbx
	je	.LBB75_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB75_3
.LBB75_2:                               # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB75_3:                               # %get_c_long.exit
	testq	%r15, %r15
	je	.LBB75_14
# BB#4:                                 # %.lr.ph.preheader
	cvttsd2si	8(%rbx), %r12
	xorl	%r13d, %r13d
	movabsq	$288265560523800577, %rbp # imm = 0x400200000000001
	.p2align	4, 0x90
.LBB75_5:                               # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	car
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB75_6
# BB#9:                                 #   in Loop: Header=BB75_5 Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$13, %eax
	jne	.LBB75_7
# BB#10:                                #   in Loop: Header=BB75_5 Depth=1
	movq	%rbx, %rdi
	callq	get_c_string
	movsbq	(%rax), %rax
	movl	%eax, %ecx
	movl	$1, %eax
	shlq	%cl, %rax
	cmpq	$63, %rcx
	ja	.LBB75_7
# BB#11:                                #   in Loop: Header=BB75_5 Depth=1
	andq	%rbp, %rax
	je	.LBB75_7
# BB#12:                                #   in Loop: Header=BB75_5 Depth=1
	movq	%r15, %rdi
	callq	cdr
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB75_5
	jmp	.LBB75_14
.LBB75_6:                               #   in Loop: Header=BB75_5 Depth=1
	xorl	%ebx, %ebx
.LBB75_7:                               # %.thread
                                        #   in Loop: Header=BB75_5 Depth=1
	cmpq	%r12, %r13
	je	.LBB75_15
# BB#8:                                 # %.outer
                                        #   in Loop: Header=BB75_5 Depth=1
	movq	%r15, %rdi
	callq	cdr
	movq	%rax, %r15
	incq	%r13
	testq	%r15, %r15
	jne	.LBB75_5
.LBB75_14:
	movq	%r14, %rbx
.LBB75_15:                              # %.loopexit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end75:
	.size	larg_default, .Lfunc_end75-larg_default
	.cfi_endproc

	.globl	lkey_default
	.p2align	4, 0x90
	.type	lkey_default,@function
lkey_default:                           # @lkey_default
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi525:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi526:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi527:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi528:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi529:
	.cfi_def_cfa_offset 48
.Lcfi530:
	.cfi_offset %rbx, -48
.Lcfi531:
	.cfi_offset %r12, -40
.Lcfi532:
	.cfi_offset %r13, -32
.Lcfi533:
	.cfi_offset %r14, -24
.Lcfi534:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	callq	get_c_string
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r12
	testq	%rbx, %rbx
	jne	.LBB76_2
	jmp	.LBB76_12
	.p2align	4, 0x90
.LBB76_13:                              # %.thread
                                        #   in Loop: Header=BB76_2 Depth=1
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB76_12
.LBB76_2:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	car
	testq	%rax, %rax
	je	.LBB76_13
# BB#3:                                 #   in Loop: Header=BB76_2 Depth=1
	movzwl	2(%rax), %ecx
	cmpl	$13, %ecx
	jne	.LBB76_13
# BB#4:                                 #   in Loop: Header=BB76_2 Depth=1
	movq	%rax, %rdi
	callq	get_c_string
	movq	%rax, %r13
	cmpb	$58, (%r13)
	jne	.LBB76_13
# BB#5:                                 #   in Loop: Header=BB76_2 Depth=1
	leaq	1(%r13), %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB76_13
# BB#6:                                 #   in Loop: Header=BB76_2 Depth=1
	cmpb	$61, 1(%r13,%r12)
	jne	.LBB76_13
# BB#7:
	leaq	2(%r13,%r12), %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r12
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r14
	movw	$13, 2(%r14)
	cmpq	$-1, %rbx
	jne	.LBB76_9
# BB#8:
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
.LBB76_9:
	leaq	1(%rbx), %rdi
	callq	must_malloc
	movq	%rax, 16(%r14)
	movq	%rbx, 8(%r14)
	testq	%r15, %r15
	je	.LBB76_11
# BB#10:
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	16(%r14), %rax
.LBB76_11:                              # %strcons.exit
	movb	$0, (%rax,%rbx)
	movq	%r12, %rdi
	callq	no_interrupt
.LBB76_12:                              # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end76:
	.size	lkey_default, .Lfunc_end76-lkey_default
	.cfi_endproc

	.globl	llist
	.p2align	4, 0x90
	.type	llist,@function
llist:                                  # @llist
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end77:
	.size	llist, .Lfunc_end77-llist
	.cfi_endproc

	.globl	writes1
	.p2align	4, 0x90
	.type	writes1,@function
writes1:                                # @writes1
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi535:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi536:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi537:
	.cfi_def_cfa_offset 32
.Lcfi538:
	.cfi_offset %rbx, -24
.Lcfi539:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rsp, %rax
	cmpq	stack_limit_ptr(%rip), %rax
	jae	.LBB78_2
# BB#1:
	movq	%rsp, %rdi
	callq	err_stack
	jmp	.LBB78_2
	.p2align	4, 0x90
.LBB78_4:                               #   in Loop: Header=BB78_2 Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	writes1
	movq	(%rsp), %rax
	movq	16(%rax), %rbx
.LBB78_2:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, (%rsp)
	testq	%rbx, %rbx
	je	.LBB78_10
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB78_2 Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB78_4
# BB#5:                                 # %.loopexit
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB78_10
# BB#6:                                 # %.loopexit
	cmpl	$13, %eax
	je	.LBB78_8
# BB#7:                                 # %.loopexit
	cmpl	$3, %eax
	jne	.LBB78_9
.LBB78_8:
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	fput_st
	jmp	.LBB78_10
.LBB78_9:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	lprin1f
.LBB78_10:                              # %.thread4
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end78:
	.size	writes1, .Lfunc_end78-writes1
	.cfi_endproc

	.globl	writes
	.p2align	4, 0x90
	.type	writes,@function
writes:                                 # @writes
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi540:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi541:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi542:
	.cfi_def_cfa_offset 32
.Lcfi543:
	.cfi_offset %rbx, -24
.Lcfi544:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	callq	car
	movq	stdout(%rip), %rsi
	movq	%rax, %rdi
	callq	get_c_file
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	cdr
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	writes1
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end79:
	.size	writes, .Lfunc_end79-writes
	.cfi_endproc

	.globl	last
	.p2align	4, 0x90
	.type	last,@function
last:                                   # @last
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi545:
	.cfi_def_cfa_offset 16
.Lcfi546:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB80_3
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB80_3
# BB#2:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB80_6
	jmp	.LBB80_5
.LBB80_3:                               # %.critedge
	movl	$.L.str.76, %edi
	movq	%rbx, %rsi
	callq	err
	testq	%rax, %rax
	je	.LBB80_5
	.p2align	4, 0x90
.LBB80_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rcx
	movq	%rax, %rbx
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB80_8
# BB#7:                                 #   in Loop: Header=BB80_6 Depth=1
	movq	16(%rbx), %rax
	testq	%rax, %rax
	movq	%rbx, %rcx
	jne	.LBB80_6
	jmp	.LBB80_8
.LBB80_5:
	movq	%rbx, %rcx
.LBB80_8:                               # %.thread
	movq	%rcx, %rax
	popq	%rbx
	retq
.Lfunc_end80:
	.size	last, .Lfunc_end80-last
	.cfi_endproc

	.globl	butlast
	.p2align	4, 0x90
	.type	butlast,@function
butlast:                                # @butlast
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi547:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi548:
	.cfi_def_cfa_offset 32
.Lcfi549:
	.cfi_offset %rbx, -16
	movq	%rdi, 8(%rsp)
	leaq	8(%rsp), %rax
	cmpq	stack_limit_ptr(%rip), %rax
	jae	.LBB81_2
# BB#1:
	leaq	8(%rsp), %rdi
	callq	err_stack
	movq	8(%rsp), %rdi
.LBB81_2:
	testq	%rdi, %rdi
	jne	.LBB81_4
# BB#3:
	xorl	%ebx, %ebx
	movl	$.L.str.77, %edi
	xorl	%esi, %esi
	callq	err
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB81_6
.LBB81_4:                               # %.thread1
	movzwl	2(%rdi), %eax
	cmpl	$1, %eax
	jne	.LBB81_5
# BB#7:
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB81_8
# BB#9:
	movq	8(%rdi), %rbx
	movq	%rax, %rdi
	callq	butlast
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	cons
	jmp	.LBB81_10
.LBB81_5:
	movq	%rdi, %rbx
.LBB81_6:                               # %.thread
	movl	$.L.str.78, %edi
	movq	%rbx, %rsi
	callq	err
	jmp	.LBB81_10
.LBB81_8:
	xorl	%eax, %eax
.LBB81_10:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end81:
	.size	butlast, .Lfunc_end81-butlast
	.cfi_endproc

	.globl	nconc
	.p2align	4, 0x90
	.type	nconc,@function
nconc:                                  # @nconc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi550:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi551:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi552:
	.cfi_def_cfa_offset 32
.Lcfi553:
	.cfi_offset %rbx, -24
.Lcfi554:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB82_1
# BB#2:
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB82_4
# BB#3:
	movq	16(%rbx), %rax
	jmp	.LBB82_5
.LBB82_1:
	movq	%r14, %rbx
	jmp	.LBB82_10
.LBB82_4:                               # %.critedge.i
	movl	$.L.str.76, %edi
	movq	%rbx, %rsi
	callq	err
.LBB82_5:                               # %.preheader.i
	testq	%rax, %rax
	movq	%rbx, %rdi
	je	.LBB82_9
# BB#6:                                 # %.lr.ph.i.preheader
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB82_7:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdi
	movq	%rax, %rcx
	movzwl	2(%rcx), %eax
	cmpl	$1, %eax
	jne	.LBB82_9
# BB#8:                                 #   in Loop: Header=BB82_7 Depth=1
	movq	16(%rcx), %rax
	testq	%rax, %rax
	movq	%rcx, %rdi
	jne	.LBB82_7
.LBB82_9:                               # %last.exit
	movq	%r14, %rsi
	callq	setcdr
.LBB82_10:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end82:
	.size	nconc, .Lfunc_end82-nconc
	.cfi_endproc

	.globl	funcall1
	.p2align	4, 0x90
	.type	funcall1,@function
funcall1:                               # @funcall1
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi555:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi556:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi557:
	.cfi_def_cfa_offset 32
.Lcfi558:
	.cfi_offset %rbx, -24
.Lcfi559:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%rbx, (%rsp)
	testq	%rbx, %rbx
	je	.LBB83_12
# BB#1:
	movswl	2(%rbx), %eax
	cmpl	$11, %eax
	je	.LBB83_6
# BB#2:
	cmpl	$5, %eax
	jne	.LBB83_12
# BB#3:
	movq	%rsp, %rax
	cmpq	stack_limit_ptr(%rip), %rax
	jae	.LBB83_5
# BB#4:
	movq	%rsp, %rdi
	callq	err_stack
	movq	(%rsp), %rbx
.LBB83_5:
	movq	%r14, %rdi
	callq	*16(%rbx)
	jmp	.LBB83_11
.LBB83_6:
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB83_12
# BB#7:
	movzwl	2(%rcx), %eax
	cmpl	$6, %eax
	jne	.LBB83_12
# BB#8:
	movq	%rsp, %rax
	cmpq	stack_limit_ptr(%rip), %rax
	jae	.LBB83_10
# BB#9:
	movq	%rsp, %rdi
	callq	err_stack
	movq	(%rsp), %rbx
	movq	16(%rbx), %rcx
.LBB83_10:
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	callq	*16(%rcx)
.LBB83_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB83_12:                              # %.critedge
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	cons
	movq	%rbx, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	lapply                  # TAILCALL
.Lfunc_end83:
	.size	funcall1, .Lfunc_end83-funcall1
	.cfi_endproc

	.globl	funcall2
	.p2align	4, 0x90
	.type	funcall2,@function
funcall2:                               # @funcall2
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi560:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi561:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi562:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi563:
	.cfi_def_cfa_offset 48
.Lcfi564:
	.cfi_offset %rbx, -32
.Lcfi565:
	.cfi_offset %r14, -24
.Lcfi566:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%rbx, 8(%rsp)
	testq	%rbx, %rbx
	je	.LBB84_6
# BB#1:
	movswl	2(%rbx), %eax
	cmpl	$21, %eax
	je	.LBB84_3
# BB#2:
	cmpl	$6, %eax
	jne	.LBB84_6
.LBB84_3:
	leaq	8(%rsp), %rax
	cmpq	stack_limit_ptr(%rip), %rax
	jae	.LBB84_5
# BB#4:
	leaq	8(%rsp), %rdi
	callq	err_stack
	movq	8(%rsp), %rbx
.LBB84_5:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	*16(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB84_6:                               # %.thread
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	cons
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	cons
	movq	%rbx, %rdi
	movq	%rax, %rsi
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	lapply                  # TAILCALL
.Lfunc_end84:
	.size	funcall2, .Lfunc_end84-funcall2
	.cfi_endproc

	.globl	lqsort
	.p2align	4, 0x90
	.type	lqsort,@function
lqsort:                                 # @lqsort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi567:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi568:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi569:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi570:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi571:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi572:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi573:
	.cfi_def_cfa_offset 96
.Lcfi574:
	.cfi_offset %rbx, -56
.Lcfi575:
	.cfi_offset %r12, -48
.Lcfi576:
	.cfi_offset %r13, -40
.Lcfi577:
	.cfi_offset %r14, -32
.Lcfi578:
	.cfi_offset %r15, -24
.Lcfi579:
	.cfi_offset %rbp, -16
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %rbx
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	je	.LBB85_51
# BB#1:                                 # %.lr.ph76.preheader
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB85_2:                               # %.lr.ph76
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB85_12
# BB#3:                                 #   in Loop: Header=BB85_2 Depth=1
	movq	16(%rax), %rax
	incl	%ebp
	testq	%rax, %rax
	jne	.LBB85_2
	jmp	.LBB85_4
.LBB85_12:                              # %.thread58
	movl	$.L.str.79, %edi
	movq	%rbx, %rsi
	callq	err
	testl	%ebp, %ebp
	je	.LBB85_13
.LBB85_4:                               # %.thread58.thread81
	callq	rand
	cltd
	idivl	%ebp
	movl	%edx, %r12d
	testl	%r12d, %r12d
	movq	%rbx, %rax
	jle	.LBB85_10
# BB#5:                                 # %.lr.ph70.preheader
	leal	-1(%r12), %ecx
	movl	%r12d, %esi
	xorl	%edx, %edx
	movq	%rbx, %rax
	andl	$7, %esi
	je	.LBB85_7
	.p2align	4, 0x90
.LBB85_6:                               # %.lr.ph70.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	incl	%edx
	cmpl	%edx, %esi
	jne	.LBB85_6
.LBB85_7:                               # %.lr.ph70.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB85_10
# BB#8:                                 # %.lr.ph70.preheader.new
	movl	%r12d, %ecx
	subl	%edx, %ecx
	.p2align	4, 0x90
.LBB85_9:                               # %.lr.ph70
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	addl	$-8, %ecx
	jne	.LBB85_9
.LBB85_10:                              # %._crit_edge71
	testq	%rbx, %rbx
	movq	8(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	je	.LBB85_11
# BB#14:                                # %.lr.ph
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB85_23
# BB#15:                                # %.lr.ph.split.preheader
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB85_16:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	testl	%r12d, %r12d
	je	.LBB85_40
# BB#17:                                # %.thread59
                                        #   in Loop: Header=BB85_16 Depth=1
	movq	8(%rbx), %rsi
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	funcall1
	movq	%rax, %r13
	movq	%rbp, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	funcall1
	movq	%rax, %rbp
	testq	%r15, %r15
	movq	%r15, 16(%rsp)
	je	.LBB85_36
# BB#18:                                #   in Loop: Header=BB85_16 Depth=1
	movswl	2(%r15), %eax
	cmpl	$21, %eax
	je	.LBB85_20
# BB#19:                                #   in Loop: Header=BB85_16 Depth=1
	cmpl	$6, %eax
	jne	.LBB85_36
.LBB85_20:                              #   in Loop: Header=BB85_16 Depth=1
	leaq	16(%rsp), %rax
	cmpq	stack_limit_ptr(%rip), %rax
	movq	%r15, %rcx
	jae	.LBB85_22
# BB#21:                                #   in Loop: Header=BB85_16 Depth=1
	leaq	16(%rsp), %rdi
	callq	err_stack
	movq	16(%rsp), %rcx
.LBB85_22:                              #   in Loop: Header=BB85_16 Depth=1
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	*16(%rcx)
	jmp	.LBB85_37
	.p2align	4, 0x90
.LBB85_36:                              # %.thread.i
                                        #   in Loop: Header=BB85_16 Depth=1
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	cons
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	cons
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	lapply
.LBB85_37:                              # %funcall2.exit
                                        #   in Loop: Header=BB85_16 Depth=1
	testq	%rax, %rax
	movq	8(%rbx), %rdi
	je	.LBB85_39
# BB#38:                                #   in Loop: Header=BB85_16 Depth=1
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
	jmp	.LBB85_40
	.p2align	4, 0x90
.LBB85_39:                              #   in Loop: Header=BB85_16 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	cons
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB85_40:                              #   in Loop: Header=BB85_16 Depth=1
	movq	16(%rbx), %rbx
	decl	%r12d
	testq	%rbx, %rbx
	jne	.LBB85_16
	jmp	.LBB85_41
.LBB85_11:
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB85_41
.LBB85_13:
	xorl	%ebp, %ebp
	jmp	.LBB85_51
.LBB85_23:                              # %.lr.ph.split.us.preheader
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	16(%rsp), %r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB85_24:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	testl	%r12d, %r12d
	je	.LBB85_35
# BB#25:                                # %.thread59.us
                                        #   in Loop: Header=BB85_24 Depth=1
	testq	%r15, %r15
	movq	8(%rbx), %rbp
	movq	%r15, 16(%rsp)
	je	.LBB85_31
# BB#26:                                #   in Loop: Header=BB85_24 Depth=1
	movswl	2(%r15), %eax
	cmpl	$21, %eax
	je	.LBB85_28
# BB#27:                                #   in Loop: Header=BB85_24 Depth=1
	cmpl	$6, %eax
	jne	.LBB85_31
.LBB85_28:                              #   in Loop: Header=BB85_24 Depth=1
	cmpq	stack_limit_ptr(%rip), %r13
	movq	%r15, %rcx
	jae	.LBB85_30
# BB#29:                                #   in Loop: Header=BB85_24 Depth=1
	movq	%r13, %rdi
	callq	err_stack
	movq	16(%rsp), %rcx
.LBB85_30:                              #   in Loop: Header=BB85_24 Depth=1
	movq	%rbp, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	*16(%rcx)
	jmp	.LBB85_32
	.p2align	4, 0x90
.LBB85_31:                              # %.thread.i.us
                                        #   in Loop: Header=BB85_24 Depth=1
	xorl	%esi, %esi
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	cons
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	cons
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	lapply
.LBB85_32:                              # %funcall2.exit.us
                                        #   in Loop: Header=BB85_24 Depth=1
	testq	%rax, %rax
	movq	8(%rbx), %rdi
	je	.LBB85_34
# BB#33:                                #   in Loop: Header=BB85_24 Depth=1
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
	jmp	.LBB85_35
	.p2align	4, 0x90
.LBB85_34:                              #   in Loop: Header=BB85_24 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	cons
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB85_35:                              #   in Loop: Header=BB85_24 Depth=1
	movq	16(%rbx), %rbx
	decl	%r12d
	testq	%rbx, %rbx
	jne	.LBB85_24
.LBB85_41:                              # %._crit_edge
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	callq	lqsort
	movq	%rax, %rbp
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	lqsort
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	cons
	movq	%rax, %rbx
	testq	%rbp, %rbp
	je	.LBB85_42
# BB#43:
	movzwl	2(%rbp), %eax
	cmpl	$1, %eax
	jne	.LBB85_45
# BB#44:
	movq	16(%rbp), %rax
	jmp	.LBB85_46
.LBB85_42:
	movq	%rbx, %rbp
	jmp	.LBB85_51
.LBB85_45:                              # %.critedge.i.i
	movl	$.L.str.76, %edi
	movq	%rbp, %rsi
	callq	err
.LBB85_46:                              # %.preheader.i.i
	testq	%rax, %rax
	movq	%rbp, %rdi
	je	.LBB85_50
# BB#47:                                # %.lr.ph.i.i.preheader
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB85_48:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdi
	movq	%rax, %rcx
	movzwl	2(%rcx), %eax
	cmpl	$1, %eax
	jne	.LBB85_50
# BB#49:                                #   in Loop: Header=BB85_48 Depth=1
	movq	16(%rcx), %rax
	testq	%rax, %rax
	movq	%rcx, %rdi
	jne	.LBB85_48
.LBB85_50:                              # %last.exit.i
	movq	%rbx, %rsi
	callq	setcdr
.LBB85_51:                              # %nconc.exit
	movq	%rbp, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end85:
	.size	lqsort, .Lfunc_end85-lqsort
	.cfi_endproc

	.globl	string_lessp
	.p2align	4, 0x90
	.type	string_lessp,@function
string_lessp:                           # @string_lessp
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi580:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi581:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi582:
	.cfi_def_cfa_offset 32
.Lcfi583:
	.cfi_offset %rbx, -24
.Lcfi584:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	callq	get_c_string
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	get_c_string
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	cmovsq	sym_t(%rip), %rcx
	movq	%rcx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end86:
	.size	string_lessp, .Lfunc_end86-string_lessp
	.cfi_endproc

	.globl	benchmark_funcall1
	.p2align	4, 0x90
	.type	benchmark_funcall1,@function
benchmark_funcall1:                     # @benchmark_funcall1
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi585:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi586:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi587:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi588:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi589:
	.cfi_def_cfa_offset 48
.Lcfi590:
	.cfi_offset %rbx, -40
.Lcfi591:
	.cfi_offset %r12, -32
.Lcfi592:
	.cfi_offset %r14, -24
.Lcfi593:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB87_2
# BB#1:
	movzwl	2(%r15), %eax
	cmpl	$2, %eax
	je	.LBB87_3
.LBB87_2:                               # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%r15, %rsi
	callq	err
.LBB87_3:                               # %get_c_long.exit
	cvttsd2si	8(%r15), %rbx
	testq	%rbx, %rbx
	jle	.LBB87_4
	.p2align	4, 0x90
.LBB87_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	funcall1
	decq	%rbx
	jne	.LBB87_5
	jmp	.LBB87_6
.LBB87_4:
	xorl	%eax, %eax
.LBB87_6:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end87:
	.size	benchmark_funcall1, .Lfunc_end87-benchmark_funcall1
	.cfi_endproc

	.globl	benchmark_funcall2
	.p2align	4, 0x90
	.type	benchmark_funcall2,@function
benchmark_funcall2:                     # @benchmark_funcall2
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi594:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi595:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi596:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi597:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi598:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi599:
	.cfi_def_cfa_offset 64
.Lcfi600:
	.cfi_offset %rbx, -48
.Lcfi601:
	.cfi_offset %r12, -40
.Lcfi602:
	.cfi_offset %r13, -32
.Lcfi603:
	.cfi_offset %r14, -24
.Lcfi604:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	callq	car
	movq	%rax, %r12
	movq	%r15, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	car
	movq	%rax, %r13
	movq	%r15, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	car
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	car
	movq	%rax, %r15
	testq	%r12, %r12
	je	.LBB88_2
# BB#1:
	movzwl	2(%r12), %eax
	cmpl	$2, %eax
	je	.LBB88_3
.LBB88_2:                               # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%r12, %rsi
	callq	err
.LBB88_3:                               # %get_c_long.exit
	cvttsd2si	8(%r12), %rbx
	testq	%rbx, %rbx
	jle	.LBB88_4
# BB#5:                                 # %.lr.ph
	testq	%r13, %r13
	je	.LBB88_12
# BB#6:
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB88_7:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	%r13, 8(%rsp)
	movswl	2(%r13), %eax
	cmpl	$21, %eax
	je	.LBB88_9
# BB#8:                                 # %.lr.ph.split
                                        #   in Loop: Header=BB88_7 Depth=1
	cmpl	$6, %eax
	jne	.LBB88_13
.LBB88_9:                               #   in Loop: Header=BB88_7 Depth=1
	cmpq	stack_limit_ptr(%rip), %r12
	movq	%r13, %rcx
	jae	.LBB88_11
# BB#10:                                #   in Loop: Header=BB88_7 Depth=1
	movq	%r12, %rdi
	callq	err_stack
	movq	8(%rsp), %rcx
.LBB88_11:                              #   in Loop: Header=BB88_7 Depth=1
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	*16(%rcx)
	decq	%rbx
	jne	.LBB88_7
	jmp	.LBB88_15
	.p2align	4, 0x90
.LBB88_13:                              # %.thread.i
                                        #   in Loop: Header=BB88_7 Depth=1
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	cons
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	cons
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	lapply
	decq	%rbx
	jne	.LBB88_7
	jmp	.LBB88_15
	.p2align	4, 0x90
.LBB88_12:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, 8(%rsp)
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	cons
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	cons
	xorl	%edi, %edi
	movq	%rax, %rsi
	callq	lapply
	decq	%rbx
	jne	.LBB88_12
	jmp	.LBB88_15
.LBB88_4:
	xorl	%eax, %eax
.LBB88_15:                              # %._crit_edge
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end88:
	.size	benchmark_funcall2, .Lfunc_end88-benchmark_funcall2
	.cfi_endproc

	.globl	benchmark_eval
	.p2align	4, 0x90
	.type	benchmark_eval,@function
benchmark_eval:                         # @benchmark_eval
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi605:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi606:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi607:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi608:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi609:
	.cfi_def_cfa_offset 48
.Lcfi610:
	.cfi_offset %rbx, -40
.Lcfi611:
	.cfi_offset %r12, -32
.Lcfi612:
	.cfi_offset %r14, -24
.Lcfi613:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB89_2
# BB#1:
	movzwl	2(%r15), %eax
	cmpl	$2, %eax
	je	.LBB89_3
.LBB89_2:                               # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%r15, %rsi
	callq	err
.LBB89_3:                               # %get_c_long.exit
	cvttsd2si	8(%r15), %rbx
	testq	%rbx, %rbx
	jle	.LBB89_4
	.p2align	4, 0x90
.LBB89_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	leval
	decq	%rbx
	jne	.LBB89_5
	jmp	.LBB89_6
.LBB89_4:
	xorl	%eax, %eax
.LBB89_6:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end89:
	.size	benchmark_eval, .Lfunc_end89-benchmark_eval
	.cfi_endproc

	.globl	mapcar1
	.p2align	4, 0x90
	.type	mapcar1,@function
mapcar1:                                # @mapcar1
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi614:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi615:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi616:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi617:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi618:
	.cfi_def_cfa_offset 48
.Lcfi619:
	.cfi_offset %rbx, -40
.Lcfi620:
	.cfi_offset %r12, -32
.Lcfi621:
	.cfi_offset %r14, -24
.Lcfi622:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%rbx, %rbx
	je	.LBB90_1
# BB#2:
	movq	%rbx, %rdi
	callq	car
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	funcall1
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	cons
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB90_6
# BB#3:                                 # %.lr.ph.preheader
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB90_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB90_6
# BB#5:                                 #   in Loop: Header=BB90_4 Depth=1
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	funcall1
	movq	16(%r12), %rsi
	movq	%rax, %rdi
	callq	cons
	movq	%rax, 16(%r12)
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r12
	jne	.LBB90_4
	jmp	.LBB90_6
.LBB90_1:
	xorl	%r14d, %r14d
.LBB90_6:                               # %.thread
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end90:
	.size	mapcar1, .Lfunc_end90-mapcar1
	.cfi_endproc

	.globl	mapcar2
	.p2align	4, 0x90
	.type	mapcar2,@function
mapcar2:                                # @mapcar2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi623:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi624:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi625:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi626:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi627:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi628:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi629:
	.cfi_def_cfa_offset 80
.Lcfi630:
	.cfi_offset %rbx, -56
.Lcfi631:
	.cfi_offset %r12, -48
.Lcfi632:
	.cfi_offset %r13, -40
.Lcfi633:
	.cfi_offset %r14, -32
.Lcfi634:
	.cfi_offset %r15, -24
.Lcfi635:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rdi, %r15
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB91_28
# BB#1:
	testq	%r13, %r13
	je	.LBB91_28
# BB#2:
	movq	%rbx, %rdi
	callq	car
	movq	%rax, %r14
	movq	%r13, %rdi
	callq	car
	movq	%rax, %r12
	movq	%r15, 16(%rsp)
	testq	%r15, %r15
	je	.LBB91_8
# BB#3:
	movswl	2(%r15), %eax
	cmpl	$21, %eax
	je	.LBB91_5
# BB#4:
	cmpl	$6, %eax
	jne	.LBB91_8
.LBB91_5:
	leaq	16(%rsp), %rax
	cmpq	stack_limit_ptr(%rip), %rax
	movq	%r15, %rcx
	jae	.LBB91_7
# BB#6:
	leaq	16(%rsp), %rdi
	callq	err_stack
	movq	16(%rsp), %rcx
.LBB91_7:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	*16(%rcx)
	jmp	.LBB91_9
.LBB91_8:                               # %.thread.i
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	cons
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	cons
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	lapply
.LBB91_9:                               # %funcall2.exit
	movq	%rax, %rdi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %r12
	movq	%r13, %rdi
	callq	cdr
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.LBB91_27
# BB#10:                                # %.lr.ph
	testq	%r15, %r15
	je	.LBB91_20
# BB#11:
	movq	8(%rsp), %r14           # 8-byte Reload
	.p2align	4, 0x90
.LBB91_12:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	je	.LBB91_27
# BB#13:                                # %.lr.ph.split
                                        #   in Loop: Header=BB91_12 Depth=1
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	jne	.LBB91_27
# BB#14:                                #   in Loop: Header=BB91_12 Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB91_27
# BB#15:                                #   in Loop: Header=BB91_12 Depth=1
	movq	8(%r12), %rbp
	movq	8(%rbx), %r13
	movq	%r15, 16(%rsp)
	movswl	2(%r15), %eax
	cmpl	$21, %eax
	je	.LBB91_17
# BB#16:                                #   in Loop: Header=BB91_12 Depth=1
	cmpl	$6, %eax
	jne	.LBB91_25
.LBB91_17:                              #   in Loop: Header=BB91_12 Depth=1
	leaq	16(%rsp), %rax
	cmpq	stack_limit_ptr(%rip), %rax
	movq	%r15, %rcx
	jae	.LBB91_19
# BB#18:                                #   in Loop: Header=BB91_12 Depth=1
	leaq	16(%rsp), %rdi
	callq	err_stack
	movq	16(%rsp), %rcx
.LBB91_19:                              #   in Loop: Header=BB91_12 Depth=1
	movq	%rbp, %rdi
	movq	%r13, %rsi
	callq	*16(%rcx)
	jmp	.LBB91_26
	.p2align	4, 0x90
.LBB91_25:                              # %.thread.i32
                                        #   in Loop: Header=BB91_12 Depth=1
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	cons
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	cons
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	lapply
.LBB91_26:                              # %funcall2.exit34
                                        #   in Loop: Header=BB91_12 Depth=1
	movq	%rax, %rdi
	movq	16(%r14), %rsi
	callq	cons
	movq	%rax, 16(%r14)
	movq	16(%r12), %r12
	movq	16(%rbx), %rbx
	testq	%r12, %r12
	movq	%rax, %r14
	jne	.LBB91_12
	jmp	.LBB91_27
.LBB91_20:                              # %.lr.ph.split.us.preheader
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB91_21:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	je	.LBB91_27
# BB#22:                                # %.lr.ph.split.us
                                        #   in Loop: Header=BB91_21 Depth=1
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	jne	.LBB91_27
# BB#23:                                #   in Loop: Header=BB91_21 Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB91_27
# BB#24:                                # %.thread.i32.us
                                        #   in Loop: Header=BB91_21 Depth=1
	movq	8(%r12), %r14
	movq	8(%rbx), %rdi
	movq	$0, 16(%rsp)
	xorl	%esi, %esi
	callq	cons
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	cons
	xorl	%edi, %edi
	movq	%rax, %rsi
	callq	lapply
	movq	16(%rbp), %rsi
	movq	%rax, %rdi
	callq	cons
	movq	%rax, 16(%rbp)
	movq	16(%r12), %r12
	movq	16(%rbx), %rbx
	testq	%r12, %r12
	movq	%rax, %rbp
	jne	.LBB91_21
.LBB91_27:
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB91_28:                              # %.critedge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end91:
	.size	mapcar2, .Lfunc_end91-mapcar2
	.cfi_endproc

	.globl	mapcar
	.p2align	4, 0x90
	.type	mapcar,@function
mapcar:                                 # @mapcar
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi636:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi637:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi638:
	.cfi_def_cfa_offset 32
.Lcfi639:
	.cfi_offset %rbx, -32
.Lcfi640:
	.cfi_offset %r14, -24
.Lcfi641:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	callq	car
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	llength
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB92_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB92_3
.LBB92_2:                               # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB92_3:                               # %get_c_long.exit
	cvttsd2si	8(%rbx), %rax
	cmpq	$3, %rax
	je	.LBB92_6
# BB#4:                                 # %get_c_long.exit
	cmpq	$2, %rax
	jne	.LBB92_7
# BB#5:
	movq	%r15, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	car
	movq	%r14, %rdi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	mapcar1                 # TAILCALL
.LBB92_6:
	movq	%r15, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	car
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	car
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	mapcar2                 # TAILCALL
.LBB92_7:
	movl	$.L.str.80, %edi
	movq	%r15, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	err                     # TAILCALL
.Lfunc_end92:
	.size	mapcar, .Lfunc_end92-mapcar
	.cfi_endproc

	.globl	lfmod
	.p2align	4, 0x90
	.type	lfmod,@function
lfmod:                                  # @lfmod
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi642:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi643:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi644:
	.cfi_def_cfa_offset 32
.Lcfi645:
	.cfi_offset %rbx, -24
.Lcfi646:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB93_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB93_3
.LBB93_2:                               # %.critedge
	movl	$.L.str.81, %edi
	movq	%rbx, %rsi
	callq	err
.LBB93_3:
	testq	%r14, %r14
	je	.LBB93_5
# BB#4:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB93_6
.LBB93_5:                               # %.critedge9
	movl	$.L.str.82, %edi
	movq	%r14, %rsi
	callq	err
.LBB93_6:
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	movsd	8(%r14), %xmm1          # xmm1 = mem[0],zero
	callq	fmod
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	flocons                 # TAILCALL
.Lfunc_end93:
	.size	lfmod, .Lfunc_end93-lfmod
	.cfi_endproc

	.globl	lsubset
	.p2align	4, 0x90
	.type	lsubset,@function
lsubset:                                # @lsubset
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi647:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi648:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi649:
	.cfi_def_cfa_offset 32
.Lcfi650:
	.cfi_offset %rbx, -32
.Lcfi651:
	.cfi_offset %r14, -24
.Lcfi652:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	jne	.LBB94_2
	jmp	.LBB94_6
	.p2align	4, 0x90
.LBB94_5:                               #   in Loop: Header=BB94_2 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB94_6
.LBB94_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB94_6
# BB#3:                                 #   in Loop: Header=BB94_2 Depth=1
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	funcall1
	testq	%rax, %rax
	je	.LBB94_5
# BB#4:                                 #   in Loop: Header=BB94_2 Depth=1
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
	jmp	.LBB94_5
.LBB94_6:                               # %.thread
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	nreverse                # TAILCALL
.Lfunc_end94:
	.size	lsubset, .Lfunc_end94-lsubset
	.cfi_endproc

	.globl	ass
	.p2align	4, 0x90
	.type	ass,@function
ass:                                    # @ass
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi653:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi654:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi655:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi656:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi657:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi658:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi659:
	.cfi_def_cfa_offset 64
.Lcfi660:
	.cfi_offset %rbx, -56
.Lcfi661:
	.cfi_offset %r12, -48
.Lcfi662:
	.cfi_offset %r13, -40
.Lcfi663:
	.cfi_offset %r14, -32
.Lcfi664:
	.cfi_offset %r15, -24
.Lcfi665:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %r15
	testq	%r13, %r13
	je	.LBB95_14
# BB#1:                                 # %.lr.ph
	testq	%r12, %r12
	je	.LBB95_15
# BB#2:
	movq	%r13, %r14
	.p2align	4, 0x90
.LBB95_3:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%r14), %eax
	cmpl	$1, %eax
	jne	.LBB95_22
# BB#4:                                 #   in Loop: Header=BB95_3 Depth=1
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB95_12
# BB#5:                                 #   in Loop: Header=BB95_3 Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB95_12
# BB#6:                                 #   in Loop: Header=BB95_3 Depth=1
	movq	8(%rbx), %rbp
	movq	%r12, (%rsp)
	movswl	2(%r12), %eax
	cmpl	$21, %eax
	je	.LBB95_8
# BB#7:                                 #   in Loop: Header=BB95_3 Depth=1
	cmpl	$6, %eax
	jne	.LBB95_11
.LBB95_8:                               #   in Loop: Header=BB95_3 Depth=1
	movq	%rsp, %rax
	cmpq	stack_limit_ptr(%rip), %rax
	movq	%r12, %rcx
	jae	.LBB95_10
# BB#9:                                 #   in Loop: Header=BB95_3 Depth=1
	movq	%rsp, %rdi
	callq	err_stack
	movq	(%rsp), %rcx
.LBB95_10:                              #   in Loop: Header=BB95_3 Depth=1
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	*16(%rcx)
	testq	%rax, %rax
	je	.LBB95_12
	jmp	.LBB95_23
.LBB95_11:                              # %.thread.i
                                        #   in Loop: Header=BB95_3 Depth=1
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	cons
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	cons
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	lapply
	testq	%rax, %rax
	jne	.LBB95_23
	.p2align	4, 0x90
.LBB95_12:                              # %.thread17
                                        #   in Loop: Header=BB95_3 Depth=1
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.LBB95_3
.LBB95_14:
	xorl	%ebx, %ebx
	jmp	.LBB95_23
.LBB95_15:                              # %.lr.ph.split.us.preheader
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB95_16:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rbp), %eax
	cmpl	$1, %eax
	jne	.LBB95_22
# BB#17:                                #   in Loop: Header=BB95_16 Depth=1
	movq	8(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB95_20
# BB#18:                                #   in Loop: Header=BB95_16 Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB95_20
# BB#19:                                # %.thread.i.us
                                        #   in Loop: Header=BB95_16 Depth=1
	movq	8(%rbx), %r14
	movq	$0, (%rsp)
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	cons
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	cons
	xorl	%edi, %edi
	movq	%rax, %rsi
	callq	lapply
	testq	%rax, %rax
	jne	.LBB95_23
.LBB95_20:                              # %.thread17.us
                                        #   in Loop: Header=BB95_16 Depth=1
	movq	16(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB95_16
# BB#21:
	xorl	%ebx, %ebx
	jmp	.LBB95_23
.LBB95_22:                              # %.us-lcssa.us
	movl	$.L.str.83, %edi
	movq	%r13, %rsi
	callq	err
	movq	%rax, %rbx
.LBB95_23:                              # %.thread18
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end95:
	.size	ass, .Lfunc_end95-ass
	.cfi_endproc

	.globl	append2
	.p2align	4, 0x90
	.type	append2,@function
append2:                                # @append2
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi666:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi667:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi668:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi669:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi670:
	.cfi_def_cfa_offset 48
.Lcfi671:
	.cfi_offset %rbx, -40
.Lcfi672:
	.cfi_offset %r12, -32
.Lcfi673:
	.cfi_offset %r14, -24
.Lcfi674:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB96_11
# BB#1:
	movswl	2(%r12), %eax
	cmpl	$18, %eax
	ja	.LBB96_10
# BB#2:
	xorl	%r15d, %r15d
	jmpq	*.LJTI96_0(,%rax,8)
.LBB96_9:
	movq	8(%r12), %r15
	testq	%r14, %r14
	jne	.LBB96_13
	jmp	.LBB96_23
.LBB96_10:
	movl	$.L.str.47, %edi
	movq	%r12, %rsi
	callq	err
.LBB96_11:                              # %nlength.exit
	xorl	%r15d, %r15d
.LBB96_12:                              # %nlength.exit
	testq	%r14, %r14
	je	.LBB96_23
.LBB96_13:
	movswl	2(%r14), %eax
	cmpl	$18, %eax
	ja	.LBB96_22
# BB#14:
	xorl	%ebx, %ebx
	jmpq	*.LJTI96_1(,%rax,8)
.LBB96_21:
	movq	8(%r14), %rbx
	jmp	.LBB96_24
.LBB96_22:
	movl	$.L.str.47, %edi
	movq	%r14, %rsi
	callq	err
	jmp	.LBB96_23
.LBB96_3:                               # %.lr.ph.i.preheader
	movzwl	%ax, %eax
	xorl	%r15d, %r15d
	cmpl	$1, %eax
	jne	.LBB96_7
# BB#4:                                 # %.lr.ph55.preheader
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB96_5:                               # %.lr.ph55
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	incq	%r15
	testq	%rax, %rax
	je	.LBB96_12
# BB#6:                                 # %..lr.ph_crit_edge.i
                                        #   in Loop: Header=BB96_5 Depth=1
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	je	.LBB96_5
.LBB96_7:                               # %.lr.ph.i._crit_edge
	movl	$.L.str.46, %edi
	movq	%r12, %rsi
	callq	err
	testq	%r14, %r14
	jne	.LBB96_13
	jmp	.LBB96_23
.LBB96_8:
	movq	16(%r12), %rdi
	callq	strlen
	movq	%rax, %r15
	testq	%r14, %r14
	jne	.LBB96_13
.LBB96_23:                              # %nlength.exit35
	xorl	%ebx, %ebx
.LBB96_24:                              # %nlength.exit35
	movq	%rbx, %rax
	addq	%r15, %rax
	jle	.LBB96_25
# BB#29:                                # %.lr.ph47.preheader
	leaq	1(%rbx,%r15), %rbx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB96_30:                              # %.lr.ph47
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edi, %edi
	movq	%r15, %rsi
	callq	cons
	movq	%rax, %r15
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB96_30
	jmp	.LBB96_26
.LBB96_25:
	xorl	%r15d, %r15d
.LBB96_26:                              # %.preheader36
	testq	%r12, %r12
	movq	%r15, %rbx
	je	.LBB96_32
# BB#27:
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB96_28:                              # %.lr.ph44
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	car
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setcar
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	cdr
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB96_28
	jmp	.LBB96_32
	.p2align	4, 0x90
.LBB96_31:                              # %.lr.ph
                                        #   in Loop: Header=BB96_32 Depth=1
	movq	%r14, %rdi
	callq	car
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setcar
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	cdr
	movq	%rax, %r14
.LBB96_32:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testq	%r14, %r14
	jne	.LBB96_31
# BB#33:                                # %._crit_edge
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB96_15:                              # %.lr.ph.i30.preheader
	movzwl	%ax, %eax
	xorl	%ebx, %ebx
	cmpl	$1, %eax
	jne	.LBB96_19
# BB#16:                                # %.lr.ph51.preheader
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB96_17:                              # %.lr.ph51
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	incq	%rbx
	testq	%rax, %rax
	je	.LBB96_24
# BB#18:                                # %..lr.ph_crit_edge.i33
                                        #   in Loop: Header=BB96_17 Depth=1
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	je	.LBB96_17
.LBB96_19:                              # %.lr.ph.i30._crit_edge
	movl	$.L.str.46, %edi
	movq	%r14, %rsi
	callq	err
	jmp	.LBB96_24
.LBB96_20:
	movq	16(%r14), %rdi
	callq	strlen
	movq	%rax, %rbx
	jmp	.LBB96_24
.Lfunc_end96:
	.size	append2, .Lfunc_end96-append2
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI96_0:
	.quad	.LBB96_12
	.quad	.LBB96_3
	.quad	.LBB96_10
	.quad	.LBB96_10
	.quad	.LBB96_10
	.quad	.LBB96_10
	.quad	.LBB96_10
	.quad	.LBB96_10
	.quad	.LBB96_10
	.quad	.LBB96_10
	.quad	.LBB96_10
	.quad	.LBB96_10
	.quad	.LBB96_10
	.quad	.LBB96_8
	.quad	.LBB96_9
	.quad	.LBB96_9
	.quad	.LBB96_9
	.quad	.LBB96_10
	.quad	.LBB96_9
.LJTI96_1:
	.quad	.LBB96_24
	.quad	.LBB96_15
	.quad	.LBB96_22
	.quad	.LBB96_22
	.quad	.LBB96_22
	.quad	.LBB96_22
	.quad	.LBB96_22
	.quad	.LBB96_22
	.quad	.LBB96_22
	.quad	.LBB96_22
	.quad	.LBB96_22
	.quad	.LBB96_22
	.quad	.LBB96_22
	.quad	.LBB96_20
	.quad	.LBB96_21
	.quad	.LBB96_21
	.quad	.LBB96_21
	.quad	.LBB96_22
	.quad	.LBB96_21

	.text
	.globl	append
	.p2align	4, 0x90
	.type	append,@function
append:                                 # @append
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi675:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi676:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi677:
	.cfi_def_cfa_offset 32
.Lcfi678:
	.cfi_offset %rbx, -24
.Lcfi679:
	.cfi_offset %r14, -16
	movq	%rdi, (%rsp)
	movq	%rsp, %rax
	cmpq	stack_limit_ptr(%rip), %rax
	jae	.LBB97_2
# BB#1:
	movq	%rsp, %rdi
	callq	err_stack
	movq	(%rsp), %rdi
.LBB97_2:
	testq	%rdi, %rdi
	je	.LBB97_3
# BB#4:
	callq	cdr
	movq	(%rsp), %rdi
	testq	%rax, %rax
	je	.LBB97_5
# BB#6:
	callq	cddr
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	callq	car
	movq	%rax, %r14
	movq	(%rsp), %rdi
	testq	%rbx, %rbx
	je	.LBB97_7
# BB#8:
	callq	cdr
	movq	%rax, %rdi
	callq	append
	jmp	.LBB97_9
.LBB97_3:
	xorl	%eax, %eax
	jmp	.LBB97_10
.LBB97_5:
	callq	car
	jmp	.LBB97_10
.LBB97_7:
	callq	cadr
.LBB97_9:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	append2
.LBB97_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end97:
	.size	append, .Lfunc_end97-append
	.cfi_endproc

	.globl	listn
	.p2align	4, 0x90
	.type	listn,@function
listn:                                  # @listn
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi680:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi681:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi682:
	.cfi_def_cfa_offset 32
	subq	$208, %rsp
.Lcfi683:
	.cfi_def_cfa_offset 240
.Lcfi684:
	.cfi_offset %rbx, -32
.Lcfi685:
	.cfi_offset %r14, -24
.Lcfi686:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testb	%al, %al
	je	.LBB98_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB98_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	testq	%r14, %r14
	jle	.LBB98_12
# BB#3:                                 # %.lr.ph22.preheader
	xorl	%r15d, %r15d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB98_4:                               # %.lr.ph22
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edi, %edi
	movq	%r15, %rsi
	callq	cons
	movq	%rax, %r15
	decq	%rbx
	jne	.LBB98_4
# BB#5:                                 # %._crit_edge23
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	240(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	testq	%r14, %r14
	jle	.LBB98_11
# BB#6:                                 # %.lr.ph
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB98_7:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rsp), %rcx
	cmpq	$40, %rcx
	ja	.LBB98_9
# BB#8:                                 #   in Loop: Header=BB98_7 Depth=1
	movq	%rcx, %rax
	addq	16(%rsp), %rax
	leal	8(%rcx), %ecx
	movl	%ecx, (%rsp)
	jmp	.LBB98_10
	.p2align	4, 0x90
.LBB98_9:                               #   in Loop: Header=BB98_7 Depth=1
	movq	8(%rsp), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, 8(%rsp)
.LBB98_10:                              #   in Loop: Header=BB98_7 Depth=1
	movq	(%rax), %rsi
	movq	%rbx, %rdi
	callq	setcar
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbx
	decq	%r14
	jne	.LBB98_7
	jmp	.LBB98_11
.LBB98_12:                              # %._crit_edge23.thread
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	240(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	xorl	%r15d, %r15d
.LBB98_11:                              # %._crit_edge
	movq	%r15, %rax
	addq	$208, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end98:
	.size	listn, .Lfunc_end98-listn
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI99_0:
	.quad	4636737291354636288     # double 100
	.text
	.globl	fast_load
	.p2align	4, 0x90
	.type	fast_load,@function
fast_load:                              # @fast_load
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi687:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi688:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi689:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi690:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi691:
	.cfi_def_cfa_offset 48
.Lcfi692:
	.cfi_offset %rbx, -40
.Lcfi693:
	.cfi_offset %r12, -32
.Lcfi694:
	.cfi_offset %r14, -24
.Lcfi695:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	callq	get_c_string
	movq	%rax, %rbx
	cmpq	$3, siod_verbose_level(%rip)
	jl	.LBB99_2
# BB#1:
	movl	$.L.str.84, %edi
	callq	put_st
	movq	%rbx, %rdi
	callq	put_st
	movl	$.L.str.85, %edi
	callq	put_st
.LBB99_2:
	movl	$.L.str.86, %esi
	movq	%rbx, %rdi
	callq	fopen_c
	movq	%rax, %r12
	movsd	.LCPI99_0(%rip), %xmm0  # xmm0 = mem[0],zero
	callq	flocons
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	cons_array
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	callq	flocons
	movq	%rax, %rcx
	movl	$3, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	listn
	movq	%rax, %r12
	movq	%r12, %rdi
	callq	fast_read
	movq	%rax, %rbx
	cmpq	%rbx, %r12
	je	.LBB99_10
# BB#3:                                 # %.lr.ph.lr.ph
	xorl	%r14d, %r14d
	testq	%r15, %r15
	je	.LBB99_7
	.p2align	4, 0x90
.LBB99_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$5, siod_verbose_level(%rip)
	jl	.LBB99_6
# BB#5:                                 #   in Loop: Header=BB99_4 Depth=1
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	lprint
.LBB99_6:                               # %.outer
                                        #   in Loop: Header=BB99_4 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
	movq	%r12, %rdi
	callq	fast_read
	movq	%rax, %rbx
	cmpq	%rbx, %r12
	jne	.LBB99_4
	jmp	.LBB99_10
	.p2align	4, 0x90
.LBB99_7:                               # %.lr.ph.split.us.us
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$5, siod_verbose_level(%rip)
	jl	.LBB99_9
# BB#8:                                 #   in Loop: Header=BB99_7 Depth=1
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	lprint
.LBB99_9:                               #   in Loop: Header=BB99_7 Depth=1
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	leval
	movq	%r12, %rdi
	callq	fast_read
	movq	%rax, %rbx
	cmpq	%rbx, %r12
	jne	.LBB99_7
.LBB99_10:                              # %.outer._crit_edge
	movq	%r12, %rdi
	callq	car
	movq	%rax, %rdi
	callq	fclose_l
	cmpq	$3, siod_verbose_level(%rip)
	jl	.LBB99_12
# BB#11:
	movl	$.L.str.87, %edi
	callq	put_st
.LBB99_12:
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	nreverse                # TAILCALL
.Lfunc_end99:
	.size	fast_load, .Lfunc_end99-fast_load
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI100_0:
	.quad	4636737291354636288     # double 100
	.text
	.globl	fast_save
	.p2align	4, 0x90
	.type	fast_save,@function
fast_save:                              # @fast_save
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi696:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi697:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi698:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi699:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi700:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi701:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi702:
	.cfi_def_cfa_offset 224
.Lcfi703:
	.cfi_offset %rbx, -56
.Lcfi704:
	.cfi_offset %r12, -48
.Lcfi705:
	.cfi_offset %r13, -40
.Lcfi706:
	.cfi_offset %r14, -32
.Lcfi707:
	.cfi_offset %r15, -24
.Lcfi708:
	.cfi_offset %rbp, -16
	movq	%r8, %rbp
	movq	%rcx, %r12
	movq	%rdx, %r14
	movq	%rsi, %rbx
	callq	get_c_string
	movq	%rax, %r15
	cmpq	$3, siod_verbose_level(%rip)
	jl	.LBB100_2
# BB#1:
	movl	$.L.str.88, %edi
	callq	put_st
	movq	%r15, %rdi
	callq	put_st
	movl	$.L.str.85, %edi
	callq	put_st
.LBB100_2:
	testq	%rbp, %rbp
	je	.LBB100_3
# BB#4:
	movq	%rbp, %rdi
	callq	get_c_string
	movq	%rax, %rsi
	jmp	.LBB100_5
.LBB100_3:
	movl	$.L.str.89, %esi
.LBB100_5:
	movq	%r15, %rdi
	callq	fopen_c
	movq	%rax, %r15
	testq	%r14, %r14
	je	.LBB100_7
# BB#6:
	xorl	%ebp, %ebp
	jmp	.LBB100_8
.LBB100_7:
	movsd	.LCPI100_0(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	cons_array
	movq	%rax, %rbp
.LBB100_8:
	xorps	%xmm0, %xmm0
	callq	flocons
	movq	%rax, %rcx
	movl	$3, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	listn
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	car
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	get_c_file
	movq	%rax, %r15
	testq	%r12, %r12
	je	.LBB100_10
# BB#9:
	movq	%r12, %rdi
	callq	get_c_string
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	fput_st
.LBB100_10:
	movups	.L.str.90+16(%rip), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	.L.str.90(%rip), %xmm0
	movaps	%xmm0, 64(%rsp)
	leaq	64(%rsp), %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	fput_st
	movl	$.L.str.91, %esi
	movl	$8, %edx
	movl	$8, %ecx
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sprintf
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	fput_st
	movq	%rsp, %r13
	movl	$.L.str.233, %esi
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sprintf
	leaq	2(%rsp), %rdi
	movl	$.L.str.233, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	sprintf
	leaq	4(%rsp), %rdi
	movl	$.L.str.233, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	sprintf
	leaq	6(%rsp), %rdi
	movl	$.L.str.233, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	sprintf
	leaq	8(%rsp), %rdi
	movl	$.L.str.233, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	sprintf
	leaq	10(%rsp), %rbp
	movl	$.L.str.233, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sprintf
	leaq	12(%rsp), %rdi
	movl	$.L.str.233, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	sprintf
	leaq	14(%rsp), %rdi
	movl	$.L.str.233, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	sprintf
	movl	$.L.str.92, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r13, %rdx
	callq	sprintf
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	fput_st
	movl	$.L.str.233, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sprintf
	movl	$.L.str.233, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	leaq	2(%rsp), %rdi
	callq	sprintf
	movl	$.L.str.233, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	leaq	4(%rsp), %rdi
	callq	sprintf
	movl	$.L.str.233, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	leaq	6(%rsp), %rdi
	callq	sprintf
	movl	$.L.str.233, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	leaq	8(%rsp), %rdi
	callq	sprintf
	movl	$.L.str.233, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sprintf
	movl	$.L.str.233, %esi
	movl	$240, %edx
	xorl	%eax, %eax
	leaq	12(%rsp), %rdi
	callq	sprintf
	movl	$.L.str.233, %esi
	movl	$63, %edx
	xorl	%eax, %eax
	leaq	14(%rsp), %rdi
	callq	sprintf
	movl	$.L.str.93, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r13, %rdx
	callq	sprintf
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	fput_st
	testq	%rbx, %rbx
	je	.LBB100_13
	.p2align	4, 0x90
.LBB100_11:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	car
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	fast_print
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB100_11
.LBB100_13:                             # %._crit_edge
	movq	%r14, %rdi
	callq	car
	movq	%rax, %rdi
	callq	fclose_l
	cmpq	$3, siod_verbose_level(%rip)
	jl	.LBB100_15
# BB#14:
	movl	$.L.str.87, %edi
	callq	put_st
.LBB100_15:
	xorl	%eax, %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end100:
	.size	fast_save, .Lfunc_end100-fast_save
	.cfi_endproc

	.globl	swrite1
	.p2align	4, 0x90
	.type	swrite1,@function
swrite1:                                # @swrite1
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi709:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi710:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi711:
	.cfi_def_cfa_offset 32
.Lcfi712:
	.cfi_offset %rbx, -24
.Lcfi713:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	stdout(%rip), %rsi
	callq	get_c_file
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB101_4
# BB#1:
	movswl	2(%rbx), %eax
	cmpl	$13, %eax
	je	.LBB101_3
# BB#2:
	cmpl	$3, %eax
	jne	.LBB101_4
.LBB101_3:
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%r14, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	fput_st                 # TAILCALL
.LBB101_4:                              # %.thread
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	lprin1f                 # TAILCALL
.Lfunc_end101:
	.size	swrite1, .Lfunc_end101-swrite1
	.cfi_endproc

	.globl	swrite
	.p2align	4, 0x90
	.type	swrite,@function
swrite:                                 # @swrite
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi714:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi715:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi716:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi717:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi718:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi719:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi720:
	.cfi_def_cfa_offset 64
.Lcfi721:
	.cfi_offset %rbx, -56
.Lcfi722:
	.cfi_offset %r12, -48
.Lcfi723:
	.cfi_offset %r13, -40
.Lcfi724:
	.cfi_offset %r14, -32
.Lcfi725:
	.cfi_offset %r15, -24
.Lcfi726:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB102_1
# BB#3:
	movzwl	2(%r14), %eax
	movswl	%ax, %ecx
	cmpl	$1, %ecx
	je	.LBB102_46
# BB#4:
	cmpl	$16, %ecx
	je	.LBB102_22
# BB#5:
	cmpl	$3, %ecx
	jne	.LBB102_12
# BB#6:
	movzwl	%ax, %eax
	cmpl	$3, %eax
	movq	%r14, %r12
	jne	.LBB102_9
# BB#7:
	movq	8(%r14), %rdi
	cmpb	$46, (%rdi)
	movq	%r14, %r12
	jne	.LBB102_9
# BB#8:
	incq	%rdi
	callq	rintern
	movq	%rax, %r12
.LBB102_9:                              # %.critedge.i
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	href
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB102_12
# BB#10:
	movzwl	2(%rbp), %eax
	cmpl	$1, %eax
	jne	.LBB102_11
# BB#16:
	movq	16(%rbp), %rdx
	testq	%rdx, %rdx
	je	.LBB102_20
# BB#17:
	cmpq	%r14, %r12
	jne	.LBB102_20
# BB#18:
	movzwl	2(%rdx), %eax
	cmpl	$1, %eax
	jne	.LBB102_20
# BB#19:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	hset
.LBB102_20:                             # %swrite2.exit
	movq	8(%rbp), %r14
	movq	stdout(%rip), %rsi
	movq	%r15, %rdi
	callq	get_c_file
	movq	%rax, %rbx
	testq	%r14, %r14
	jne	.LBB102_13
# BB#21:
	xorl	%r14d, %r14d
	jmp	.LBB102_2
.LBB102_1:                              # %.thread46
	movq	stdout(%rip), %rsi
	movq	%r15, %rdi
	callq	get_c_file
	movq	%rax, %rbx
	jmp	.LBB102_2
.LBB102_22:
	movq	8(%r14), %r13
	testq	%r13, %r13
	jg	.LBB102_24
# BB#23:
	movl	$.L.str.94, %edi
	movq	%r14, %rsi
	callq	err
.LBB102_24:
	movq	16(%r14), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.LBB102_25
# BB#26:
	movzwl	2(%r12), %eax
	cmpl	$3, %eax
	movq	%r12, %rax
	jne	.LBB102_29
# BB#27:
	movq	8(%r12), %rdi
	cmpb	$46, (%rdi)
	movq	%r12, %rax
	jne	.LBB102_29
# BB#28:
	incq	%rdi
	callq	rintern
	jmp	.LBB102_29
.LBB102_25:
	xorl	%eax, %eax
.LBB102_29:                             # %.critedge.i31
	movq	%rbx, %rdi
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rax, %rsi
	callq	href
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB102_36
# BB#30:
	movzwl	2(%rbp), %eax
	cmpl	$1, %eax
	jne	.LBB102_36
# BB#31:
	movq	16(%rbp), %rdx
	testq	%rdx, %rdx
	je	.LBB102_35
# BB#32:
	cmpq	%r12, (%rsp)            # 8-byte Folded Reload
	jne	.LBB102_35
# BB#33:
	movzwl	2(%rdx), %eax
	cmpl	$1, %eax
	jne	.LBB102_35
# BB#34:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	hset
.LBB102_35:                             # %.critedge27.i35
	movq	8(%rbp), %r12
	testq	%r12, %r12
	jne	.LBB102_38
	jmp	.LBB102_39
.LBB102_36:                             # %.critedge26.i37
	testq	%rbp, %rbp
	cmovneq	%rbp, %r12
	testq	%r12, %r12
	je	.LBB102_39
.LBB102_38:
	movzwl	2(%r12), %eax
	cmpl	$2, %eax
	je	.LBB102_40
.LBB102_39:                             # %.critedge.i41
	movl	$.L.str.44, %edi
	movq	%r12, %rsi
	callq	err
.LBB102_40:                             # %get_c_long.exit
	cvttsd2si	8(%r12), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	testq	%rax, %rax
	jle	.LBB102_46
# BB#41:                                # %get_c_long.exit
	cmpq	$2, %r13
	jl	.LBB102_46
# BB#42:                                # %.preheader.us.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB102_43:                             # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB102_44 Depth 2
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB102_44:                             #   Parent Loop BB102_43 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r14), %rax
	movq	(%rax,%r12,8), %rdx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	swrite
	incq	%r12
	cmpq	%r12, %r13
	jne	.LBB102_44
# BB#45:                                # %._crit_edge.us
                                        #   in Loop: Header=BB102_43 Depth=1
	incq	%rbp
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	jne	.LBB102_43
	jmp	.LBB102_46
.LBB102_11:
	movq	%rbp, %r14
.LBB102_12:                             # %swrite2.exit.thread
	movq	stdout(%rip), %rsi
	movq	%r15, %rdi
	callq	get_c_file
	movq	%rax, %rbx
.LBB102_13:
	movswl	2(%r14), %eax
	cmpl	$13, %eax
	je	.LBB102_15
# BB#14:
	cmpl	$3, %eax
	jne	.LBB102_2
.LBB102_15:
	movq	%r14, %rdi
	callq	get_c_string
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	fput_st
	jmp	.LBB102_46
.LBB102_2:                              # %.thread.i42
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	lprin1f
.LBB102_46:                             # %swrite1.exit
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end102:
	.size	swrite, .Lfunc_end102-swrite
	.cfi_endproc

	.globl	lpow
	.p2align	4, 0x90
	.type	lpow,@function
lpow:                                   # @lpow
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi727:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi728:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi729:
	.cfi_def_cfa_offset 32
.Lcfi730:
	.cfi_offset %rbx, -24
.Lcfi731:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB103_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB103_3
.LBB103_2:                              # %.critedge
	movl	$.L.str.95, %edi
	movq	%rbx, %rsi
	callq	err
.LBB103_3:
	testq	%r14, %r14
	je	.LBB103_5
# BB#4:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB103_6
.LBB103_5:                              # %.critedge9
	movl	$.L.str.96, %edi
	movq	%r14, %rsi
	callq	err
.LBB103_6:
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	movsd	8(%r14), %xmm1          # xmm1 = mem[0],zero
	callq	pow
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	flocons                 # TAILCALL
.Lfunc_end103:
	.size	lpow, .Lfunc_end103-lpow
	.cfi_endproc

	.globl	lexp
	.p2align	4, 0x90
	.type	lexp,@function
lexp:                                   # @lexp
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi732:
	.cfi_def_cfa_offset 16
.Lcfi733:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB104_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB104_3
.LBB104_2:                              # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB104_3:                              # %get_c_double.exit
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	callq	exp
	popq	%rbx
	jmp	flocons                 # TAILCALL
.Lfunc_end104:
	.size	lexp, .Lfunc_end104-lexp
	.cfi_endproc

	.globl	llog
	.p2align	4, 0x90
	.type	llog,@function
llog:                                   # @llog
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi734:
	.cfi_def_cfa_offset 16
.Lcfi735:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB105_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB105_3
.LBB105_2:                              # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB105_3:                              # %get_c_double.exit
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	callq	log
	popq	%rbx
	jmp	flocons                 # TAILCALL
.Lfunc_end105:
	.size	llog, .Lfunc_end105-llog
	.cfi_endproc

	.globl	lsin
	.p2align	4, 0x90
	.type	lsin,@function
lsin:                                   # @lsin
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi736:
	.cfi_def_cfa_offset 16
.Lcfi737:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB106_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB106_3
.LBB106_2:                              # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB106_3:                              # %get_c_double.exit
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	callq	sin
	popq	%rbx
	jmp	flocons                 # TAILCALL
.Lfunc_end106:
	.size	lsin, .Lfunc_end106-lsin
	.cfi_endproc

	.globl	lcos
	.p2align	4, 0x90
	.type	lcos,@function
lcos:                                   # @lcos
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi738:
	.cfi_def_cfa_offset 16
.Lcfi739:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB107_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB107_3
.LBB107_2:                              # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB107_3:                              # %get_c_double.exit
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	callq	cos
	popq	%rbx
	jmp	flocons                 # TAILCALL
.Lfunc_end107:
	.size	lcos, .Lfunc_end107-lcos
	.cfi_endproc

	.globl	ltan
	.p2align	4, 0x90
	.type	ltan,@function
ltan:                                   # @ltan
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi740:
	.cfi_def_cfa_offset 16
.Lcfi741:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB108_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB108_3
.LBB108_2:                              # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB108_3:                              # %get_c_double.exit
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	callq	tan
	popq	%rbx
	jmp	flocons                 # TAILCALL
.Lfunc_end108:
	.size	ltan, .Lfunc_end108-ltan
	.cfi_endproc

	.globl	lasin
	.p2align	4, 0x90
	.type	lasin,@function
lasin:                                  # @lasin
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi742:
	.cfi_def_cfa_offset 16
.Lcfi743:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB109_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB109_3
.LBB109_2:                              # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB109_3:                              # %get_c_double.exit
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	callq	asin
	popq	%rbx
	jmp	flocons                 # TAILCALL
.Lfunc_end109:
	.size	lasin, .Lfunc_end109-lasin
	.cfi_endproc

	.globl	lacos
	.p2align	4, 0x90
	.type	lacos,@function
lacos:                                  # @lacos
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi744:
	.cfi_def_cfa_offset 16
.Lcfi745:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB110_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB110_3
.LBB110_2:                              # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB110_3:                              # %get_c_double.exit
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	callq	acos
	popq	%rbx
	jmp	flocons                 # TAILCALL
.Lfunc_end110:
	.size	lacos, .Lfunc_end110-lacos
	.cfi_endproc

	.globl	latan
	.p2align	4, 0x90
	.type	latan,@function
latan:                                  # @latan
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi746:
	.cfi_def_cfa_offset 16
.Lcfi747:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB111_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB111_3
.LBB111_2:                              # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB111_3:                              # %get_c_double.exit
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	callq	atan
	popq	%rbx
	jmp	flocons                 # TAILCALL
.Lfunc_end111:
	.size	latan, .Lfunc_end111-latan
	.cfi_endproc

	.globl	latan2
	.p2align	4, 0x90
	.type	latan2,@function
latan2:                                 # @latan2
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi748:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi749:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi750:
	.cfi_def_cfa_offset 32
.Lcfi751:
	.cfi_offset %rbx, -24
.Lcfi752:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB112_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB112_3
.LBB112_2:                              # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB112_3:                              # %get_c_double.exit
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	testq	%r14, %r14
	je	.LBB112_5
# BB#4:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB112_6
.LBB112_5:                              # %.critedge.i3
	movl	$.L.str.44, %edi
	movq	%r14, %rsi
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	callq	err
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB112_6:                              # %get_c_double.exit4
	movsd	8(%r14), %xmm1          # xmm1 = mem[0],zero
	callq	atan2
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	flocons                 # TAILCALL
.Lfunc_end112:
	.size	latan2, .Lfunc_end112-latan2
	.cfi_endproc

	.globl	hexstr
	.p2align	4, 0x90
	.type	hexstr,@function
hexstr:                                 # @hexstr
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi753:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi754:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi755:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi756:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi757:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi758:
	.cfi_def_cfa_offset 64
.Lcfi759:
	.cfi_offset %rbx, -48
.Lcfi760:
	.cfi_offset %r12, -40
.Lcfi761:
	.cfi_offset %r13, -32
.Lcfi762:
	.cfi_offset %r14, -24
.Lcfi763:
	.cfi_offset %r15, -16
	leaq	8(%rsp), %rsi
	callq	get_c_string_dim
	movq	%rax, %r15
	movq	8(%rsp), %rbx
	leaq	(%rbx,%rbx), %r13
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r12
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r14
	movw	$13, 2(%r14)
	leaq	1(%rbx,%rbx), %rdi
	callq	must_malloc
	movq	%rax, 16(%r14)
	movq	%r13, 8(%r14)
	movb	$0, (%rax,%rbx,2)
	movq	%r12, %rdi
	callq	no_interrupt
	movq	%r14, %rdi
	callq	get_c_string
	movq	%rax, %r12
	cmpq	$0, 8(%rsp)
	jle	.LBB113_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB113_2:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r15,%rbx), %edx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sprintf
	incq	%rbx
	addq	$2, %r12
	cmpq	8(%rsp), %rbx
	jl	.LBB113_2
.LBB113_3:                              # %._crit_edge
	movq	%r14, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end113:
	.size	hexstr, .Lfunc_end113-hexstr
	.cfi_endproc

	.globl	hexstr2bytes
	.p2align	4, 0x90
	.type	hexstr2bytes,@function
hexstr2bytes:                           # @hexstr2bytes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi764:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi765:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi766:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi767:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi768:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi769:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi770:
	.cfi_def_cfa_offset 80
.Lcfi771:
	.cfi_offset %rbx, -56
.Lcfi772:
	.cfi_offset %r12, -48
.Lcfi773:
	.cfi_offset %r13, -40
.Lcfi774:
	.cfi_offset %r14, -32
.Lcfi775:
	.cfi_offset %r15, -24
.Lcfi776:
	.cfi_offset %rbp, -16
	callq	get_c_string
	movq	%rax, %r13
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rbx
	shrq	%rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r12
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	%rbx, %rdi
	callq	must_malloc
	movq	%rax, 16(%rbp)
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movw	$18, 2(%rbp)
	movq	%r12, %rdi
	callq	no_interrupt
	testq	%rbx, %rbx
	je	.LBB114_15
# BB#1:                                 # %.lr.ph
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	16(%rax), %r14
	callq	__ctype_b_loc
	movq	%rbx, %rdx
	movq	%rax, %rsi
	incq	%r13
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB114_2:                              # =>This Inner Loop Header: Depth=1
	movsbq	-1(%r13), %r12
	movq	(%rsi), %r15
	movzwl	(%r15,%r12,2), %eax
	testb	$8, %ah
	jne	.LBB114_3
# BB#4:                                 #   in Loop: Header=BB114_2 Depth=1
	xorl	%ebp, %ebp
	testb	$16, %ah
	je	.LBB114_8
# BB#5:                                 #   in Loop: Header=BB114_2 Depth=1
	movl	%r12d, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB114_7
# BB#6:                                 #   in Loop: Header=BB114_2 Depth=1
	movq	%rdx, %rbx
	callq	__ctype_toupper_loc
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	movq	(%rax), %rax
	movl	(%rax,%r12,4), %r12d
.LBB114_7:                              # %toupper.exit.i
                                        #   in Loop: Header=BB114_2 Depth=1
	addl	$-55, %r12d
	movl	%r12d, %ebp
	jmp	.LBB114_8
	.p2align	4, 0x90
.LBB114_3:                              #   in Loop: Header=BB114_2 Depth=1
	leal	-48(%r12), %ebp
.LBB114_8:                              # %xdigitvalue.exit
                                        #   in Loop: Header=BB114_2 Depth=1
	shll	$4, %ebp
	movsbq	(%r13), %rbx
	movzwl	(%r15,%rbx,2), %ecx
	testb	$8, %ch
	jne	.LBB114_9
# BB#10:                                #   in Loop: Header=BB114_2 Depth=1
	xorl	%eax, %eax
	testb	$16, %ch
	je	.LBB114_14
# BB#11:                                #   in Loop: Header=BB114_2 Depth=1
	movl	%ebx, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB114_13
# BB#12:                                #   in Loop: Header=BB114_2 Depth=1
	movq	%rdx, %r15
	callq	__ctype_toupper_loc
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	(%rax), %rax
	movl	(%rax,%rbx,4), %ebx
.LBB114_13:                             # %toupper.exit.i23
                                        #   in Loop: Header=BB114_2 Depth=1
	addl	$-55, %ebx
	movl	%ebx, %eax
	jmp	.LBB114_14
	.p2align	4, 0x90
.LBB114_9:                              #   in Loop: Header=BB114_2 Depth=1
	leal	-48(%rbx), %eax
.LBB114_14:                             # %xdigitvalue.exit25
                                        #   in Loop: Header=BB114_2 Depth=1
	addl	%ebp, %eax
	movb	%al, (%r14)
	incq	%r14
	addq	$2, %r13
	decq	%rdx
	jne	.LBB114_2
.LBB114_15:                             # %._crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end114:
	.size	hexstr2bytes, .Lfunc_end114-hexstr2bytes
	.cfi_endproc

	.globl	getprop
	.p2align	4, 0x90
	.type	getprop,@function
getprop:                                # @getprop
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi777:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi778:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi779:
	.cfi_def_cfa_offset 32
.Lcfi780:
	.cfi_offset %rbx, -24
.Lcfi781:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	callq	cdr
	jmp	.LBB115_1
	.p2align	4, 0x90
.LBB115_3:                              #   in Loop: Header=BB115_1 Depth=1
	callq	cddr
.LBB115_1:                              # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB115_4
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB115_1 Depth=1
	movq	%rbx, %rdi
	callq	car
	movq	%rbx, %rdi
	cmpq	%r14, %rax
	jne	.LBB115_3
# BB#5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	cadr                    # TAILCALL
.LBB115_4:                              # %.loopexit
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end115:
	.size	getprop, .Lfunc_end115-getprop
	.cfi_endproc

	.globl	setprop
	.p2align	4, 0x90
	.type	setprop,@function
setprop:                                # @setprop
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi782:
	.cfi_def_cfa_offset 16
	movl	$.L.str.97, %edi
	xorl	%esi, %esi
	callq	err
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end116:
	.size	setprop, .Lfunc_end116-setprop
	.cfi_endproc

	.globl	putprop
	.p2align	4, 0x90
	.type	putprop,@function
putprop:                                # @putprop
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi783:
	.cfi_def_cfa_offset 16
	movl	$.L.str.97, %edi
	xorl	%esi, %esi
	callq	err
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end117:
	.size	putprop, .Lfunc_end117-putprop
	.cfi_endproc

	.globl	ltypeof
	.p2align	4, 0x90
	.type	ltypeof,@function
ltypeof:                                # @ltypeof
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB118_3
# BB#1:
	movzwl	2(%rdi), %eax
	movswq	%ax, %rcx
	cmpq	$21, %rcx
	ja	.LBB118_25
# BB#2:
	jmpq	*.LJTI118_0(,%rcx,8)
.LBB118_3:                              # %.thread
	movl	$.L.str.98, %edi
	jmp	cintern                 # TAILCALL
.LBB118_25:
	cwtl
	cvtsi2sdl	%eax, %xmm0
	jmp	flocons                 # TAILCALL
.LBB118_4:
	movl	$.L.str.99, %edi
	jmp	cintern                 # TAILCALL
.LBB118_5:
	movl	$.L.str.100, %edi
	jmp	cintern                 # TAILCALL
.LBB118_6:
	movl	$.L.str.101, %edi
	jmp	cintern                 # TAILCALL
.LBB118_7:
	movl	$.L.str.102, %edi
	jmp	cintern                 # TAILCALL
.LBB118_8:
	movl	$.L.str.103, %edi
	jmp	cintern                 # TAILCALL
.LBB118_9:
	movl	$.L.str.104, %edi
	jmp	cintern                 # TAILCALL
.LBB118_11:
	movl	$.L.str.106, %edi
	jmp	cintern                 # TAILCALL
.LBB118_14:
	movl	$.L.str.109, %edi
	jmp	cintern                 # TAILCALL
.LBB118_15:
	movl	$.L.str.110, %edi
	jmp	cintern                 # TAILCALL
.LBB118_16:
	movl	$.L.str.111, %edi
	jmp	cintern                 # TAILCALL
.LBB118_17:
	movl	$.L.str.112, %edi
	jmp	cintern                 # TAILCALL
.LBB118_18:
	movl	$.L.str.113, %edi
	jmp	cintern                 # TAILCALL
.LBB118_19:
	movl	$.L.str.114, %edi
	jmp	cintern                 # TAILCALL
.LBB118_21:
	movl	$.L.str.116, %edi
	jmp	cintern                 # TAILCALL
.LBB118_22:
	movl	$.L.str.117, %edi
	jmp	cintern                 # TAILCALL
.LBB118_23:
	movl	$.L.str.118, %edi
	jmp	cintern                 # TAILCALL
.LBB118_24:
	movl	$.L.str.119, %edi
	jmp	cintern                 # TAILCALL
.LBB118_20:
	movl	$.L.str.115, %edi
	jmp	cintern                 # TAILCALL
.LBB118_13:
	movl	$.L.str.108, %edi
	jmp	cintern                 # TAILCALL
.LBB118_10:
	movl	$.L.str.105, %edi
	jmp	cintern                 # TAILCALL
.LBB118_12:
	movl	$.L.str.107, %edi
	jmp	cintern                 # TAILCALL
.Lfunc_end118:
	.size	ltypeof, .Lfunc_end118-ltypeof
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI118_0:
	.quad	.LBB118_3
	.quad	.LBB118_4
	.quad	.LBB118_5
	.quad	.LBB118_6
	.quad	.LBB118_7
	.quad	.LBB118_8
	.quad	.LBB118_9
	.quad	.LBB118_11
	.quad	.LBB118_14
	.quad	.LBB118_15
	.quad	.LBB118_16
	.quad	.LBB118_17
	.quad	.LBB118_18
	.quad	.LBB118_19
	.quad	.LBB118_21
	.quad	.LBB118_22
	.quad	.LBB118_23
	.quad	.LBB118_24
	.quad	.LBB118_20
	.quad	.LBB118_12
	.quad	.LBB118_13
	.quad	.LBB118_10

	.text
	.globl	caaar
	.p2align	4, 0x90
	.type	caaar,@function
caaar:                                  # @caaar
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi784:
	.cfi_def_cfa_offset 16
	callq	car
	movq	%rax, %rdi
	callq	car
	movq	%rax, %rdi
	popq	%rax
	jmp	car                     # TAILCALL
.Lfunc_end119:
	.size	caaar, .Lfunc_end119-caaar
	.cfi_endproc

	.globl	caadr
	.p2align	4, 0x90
	.type	caadr,@function
caadr:                                  # @caadr
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi785:
	.cfi_def_cfa_offset 16
	callq	cdr
	movq	%rax, %rdi
	callq	car
	movq	%rax, %rdi
	popq	%rax
	jmp	car                     # TAILCALL
.Lfunc_end120:
	.size	caadr, .Lfunc_end120-caadr
	.cfi_endproc

	.globl	cadar
	.p2align	4, 0x90
	.type	cadar,@function
cadar:                                  # @cadar
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi786:
	.cfi_def_cfa_offset 16
	callq	car
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	popq	%rax
	jmp	car                     # TAILCALL
.Lfunc_end121:
	.size	cadar, .Lfunc_end121-cadar
	.cfi_endproc

	.globl	caddr
	.p2align	4, 0x90
	.type	caddr,@function
caddr:                                  # @caddr
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi787:
	.cfi_def_cfa_offset 16
	callq	cdr
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	popq	%rax
	jmp	car                     # TAILCALL
.Lfunc_end122:
	.size	caddr, .Lfunc_end122-caddr
	.cfi_endproc

	.globl	cdaar
	.p2align	4, 0x90
	.type	cdaar,@function
cdaar:                                  # @cdaar
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi788:
	.cfi_def_cfa_offset 16
	callq	car
	movq	%rax, %rdi
	callq	car
	movq	%rax, %rdi
	popq	%rax
	jmp	cdr                     # TAILCALL
.Lfunc_end123:
	.size	cdaar, .Lfunc_end123-cdaar
	.cfi_endproc

	.globl	cdadr
	.p2align	4, 0x90
	.type	cdadr,@function
cdadr:                                  # @cdadr
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi789:
	.cfi_def_cfa_offset 16
	callq	cdr
	movq	%rax, %rdi
	callq	car
	movq	%rax, %rdi
	popq	%rax
	jmp	cdr                     # TAILCALL
.Lfunc_end124:
	.size	cdadr, .Lfunc_end124-cdadr
	.cfi_endproc

	.globl	cddar
	.p2align	4, 0x90
	.type	cddar,@function
cddar:                                  # @cddar
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi790:
	.cfi_def_cfa_offset 16
	callq	car
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	popq	%rax
	jmp	cdr                     # TAILCALL
.Lfunc_end125:
	.size	cddar, .Lfunc_end125-cddar
	.cfi_endproc

	.globl	cdddr
	.p2align	4, 0x90
	.type	cdddr,@function
cdddr:                                  # @cdddr
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi791:
	.cfi_def_cfa_offset 16
	callq	cdr
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	popq	%rax
	jmp	cdr                     # TAILCALL
.Lfunc_end126:
	.size	cdddr, .Lfunc_end126-cdddr
	.cfi_endproc

	.globl	ash
	.p2align	4, 0x90
	.type	ash,@function
ash:                                    # @ash
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi792:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi793:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi794:
	.cfi_def_cfa_offset 32
.Lcfi795:
	.cfi_offset %rbx, -24
.Lcfi796:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB127_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB127_3
.LBB127_2:                              # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB127_3:                              # %get_c_long.exit
	cvttsd2si	8(%rbx), %rbx
	testq	%r14, %r14
	je	.LBB127_5
# BB#4:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB127_6
.LBB127_5:                              # %.critedge.i9
	movl	$.L.str.44, %edi
	movq	%r14, %rsi
	callq	err
.LBB127_6:                              # %get_c_long.exit10
	cvttsd2si	8(%r14), %rax
	movq	%rbx, %rdx
	movl	%eax, %ecx
	shlq	%cl, %rdx
	movl	%eax, %ecx
	negl	%ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarq	%cl, %rbx
	testq	%rax, %rax
	cmovgq	%rdx, %rbx
	cvtsi2sdq	%rbx, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	flocons                 # TAILCALL
.Lfunc_end127:
	.size	ash, .Lfunc_end127-ash
	.cfi_endproc

	.globl	bitand
	.p2align	4, 0x90
	.type	bitand,@function
bitand:                                 # @bitand
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi797:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi798:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi799:
	.cfi_def_cfa_offset 32
.Lcfi800:
	.cfi_offset %rbx, -24
.Lcfi801:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB128_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB128_3
.LBB128_2:                              # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB128_3:                              # %get_c_long.exit
	cvttsd2si	8(%rbx), %rbx
	testq	%r14, %r14
	je	.LBB128_5
# BB#4:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB128_6
.LBB128_5:                              # %.critedge.i3
	movl	$.L.str.44, %edi
	movq	%r14, %rsi
	callq	err
.LBB128_6:                              # %get_c_long.exit4
	cvttsd2si	8(%r14), %rax
	andq	%rbx, %rax
	cvtsi2sdq	%rax, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	flocons                 # TAILCALL
.Lfunc_end128:
	.size	bitand, .Lfunc_end128-bitand
	.cfi_endproc

	.globl	bitor
	.p2align	4, 0x90
	.type	bitor,@function
bitor:                                  # @bitor
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi802:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi803:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi804:
	.cfi_def_cfa_offset 32
.Lcfi805:
	.cfi_offset %rbx, -24
.Lcfi806:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB129_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB129_3
.LBB129_2:                              # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB129_3:                              # %get_c_long.exit
	cvttsd2si	8(%rbx), %rbx
	testq	%r14, %r14
	je	.LBB129_5
# BB#4:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB129_6
.LBB129_5:                              # %.critedge.i3
	movl	$.L.str.44, %edi
	movq	%r14, %rsi
	callq	err
.LBB129_6:                              # %get_c_long.exit4
	cvttsd2si	8(%r14), %rax
	orq	%rbx, %rax
	cvtsi2sdq	%rax, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	flocons                 # TAILCALL
.Lfunc_end129:
	.size	bitor, .Lfunc_end129-bitor
	.cfi_endproc

	.globl	bitxor
	.p2align	4, 0x90
	.type	bitxor,@function
bitxor:                                 # @bitxor
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi807:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi808:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi809:
	.cfi_def_cfa_offset 32
.Lcfi810:
	.cfi_offset %rbx, -24
.Lcfi811:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB130_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB130_3
.LBB130_2:                              # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB130_3:                              # %get_c_long.exit
	cvttsd2si	8(%rbx), %rbx
	testq	%r14, %r14
	je	.LBB130_5
# BB#4:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB130_6
.LBB130_5:                              # %.critedge.i3
	movl	$.L.str.44, %edi
	movq	%r14, %rsi
	callq	err
.LBB130_6:                              # %get_c_long.exit4
	cvttsd2si	8(%r14), %rax
	xorq	%rbx, %rax
	cvtsi2sdq	%rax, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	flocons                 # TAILCALL
.Lfunc_end130:
	.size	bitxor, .Lfunc_end130-bitxor
	.cfi_endproc

	.globl	bitnot
	.p2align	4, 0x90
	.type	bitnot,@function
bitnot:                                 # @bitnot
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi812:
	.cfi_def_cfa_offset 16
.Lcfi813:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB131_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB131_3
.LBB131_2:                              # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB131_3:                              # %get_c_long.exit
	cvttsd2si	8(%rbx), %rax
	notq	%rax
	cvtsi2sdq	%rax, %xmm0
	popq	%rbx
	jmp	flocons                 # TAILCALL
.Lfunc_end131:
	.size	bitnot, .Lfunc_end131-bitnot
	.cfi_endproc

	.globl	leval_prog1
	.p2align	4, 0x90
	.type	leval_prog1,@function
leval_prog1:                            # @leval_prog1
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi814:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi815:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi816:
	.cfi_def_cfa_offset 32
.Lcfi817:
	.cfi_offset %rbx, -32
.Lcfi818:
	.cfi_offset %r14, -24
.Lcfi819:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	callq	car
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	leval
	movq	%rax, %r14
	jmp	.LBB132_2
	.p2align	4, 0x90
.LBB132_1:                              # %.lr.ph
                                        #   in Loop: Header=BB132_2 Depth=1
	movq	%rbx, %rdi
	callq	car
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	leval
.LBB132_2:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB132_1
# BB#3:                                 # %._crit_edge
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end132:
	.size	leval_prog1, .Lfunc_end132-leval_prog1
	.cfi_endproc

	.globl	leval_cond
	.p2align	4, 0x90
	.type	leval_cond,@function
leval_cond:                             # @leval_cond
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi820:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi821:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi822:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi823:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi824:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi825:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi826:
	.cfi_def_cfa_offset 64
.Lcfi827:
	.cfi_offset %rbx, -56
.Lcfi828:
	.cfi_offset %r12, -48
.Lcfi829:
	.cfi_offset %r13, -40
.Lcfi830:
	.cfi_offset %r14, -32
.Lcfi831:
	.cfi_offset %r15, -24
.Lcfi832:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movq	(%r14), %rdi
	callq	cdr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB133_12
# BB#1:
	movq	(%rbp), %r15
	.p2align	4, 0x90
.LBB133_2:                              # %.preheader52
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %r12
	movq	%rbx, %rdi
	callq	car
	movq	%rax, %r13
	movq	%r13, %rdi
	testq	%r12, %r12
	je	.LBB133_7
# BB#3:                                 #   in Loop: Header=BB133_2 Depth=1
	callq	car
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	leval
	movq	%rax, %rbp
	testq	%rbp, %rbp
	movq	%r12, %rbx
	je	.LBB133_2
# BB#4:
	movq	%r13, %rdi
	callq	cdr
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB133_13
# BB#5:                                 # %.preheader51
	movq	%r12, %rdi
	callq	cdr
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	car
	testq	%rbx, %rbx
	je	.LBB133_11
	.p2align	4, 0x90
.LBB133_6:                              # %.lr.ph57
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	leval
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	car
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB133_6
	jmp	.LBB133_11
.LBB133_7:
	callq	cdr
	movq	%rax, %r12
	movq	%r13, %rdi
	callq	car
	testq	%r12, %r12
	je	.LBB133_11
# BB#8:
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	leval
	testq	%rax, %rax
	je	.LBB133_12
# BB#9:                                 # %.preheader
	movq	%r12, %rdi
	callq	cdr
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	car
	testq	%rbx, %rbx
	je	.LBB133_11
	.p2align	4, 0x90
.LBB133_10:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	leval
	movq	%rbx, %rdi
	callq	cdr
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	car
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB133_10
.LBB133_11:                             # %._crit_edge
	movq	%rax, (%r14)
	movq	sym_t(%rip), %rax
	jmp	.LBB133_15
.LBB133_12:
	movq	$0, (%r14)
	jmp	.LBB133_14
.LBB133_13:
	movq	%rbp, (%r14)
.LBB133_14:
	xorl	%eax, %eax
.LBB133_15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end133:
	.size	leval_cond, .Lfunc_end133-leval_cond
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI134_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI134_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.text
	.globl	lstrspn
	.p2align	4, 0x90
	.type	lstrspn,@function
lstrspn:                                # @lstrspn
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi833:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi834:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi835:
	.cfi_def_cfa_offset 32
.Lcfi836:
	.cfi_offset %rbx, -24
.Lcfi837:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	callq	get_c_string
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	get_c_string
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strspn
	movd	%rax, %xmm1
	punpckldq	.LCPI134_0(%rip), %xmm1 # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	subpd	.LCPI134_1(%rip), %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	flocons                 # TAILCALL
.Lfunc_end134:
	.size	lstrspn, .Lfunc_end134-lstrspn
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI135_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI135_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.text
	.globl	lstrcspn
	.p2align	4, 0x90
	.type	lstrcspn,@function
lstrcspn:                               # @lstrcspn
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi838:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi839:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi840:
	.cfi_def_cfa_offset 32
.Lcfi841:
	.cfi_offset %rbx, -24
.Lcfi842:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	callq	get_c_string
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	get_c_string
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcspn
	movd	%rax, %xmm1
	punpckldq	.LCPI135_0(%rip), %xmm1 # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	subpd	.LCPI135_1(%rip), %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	flocons                 # TAILCALL
.Lfunc_end135:
	.size	lstrcspn, .Lfunc_end135-lstrcspn
	.cfi_endproc

	.globl	substring_equal
	.p2align	4, 0x90
	.type	substring_equal,@function
substring_equal:                        # @substring_equal
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi843:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi844:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi845:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi846:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi847:
	.cfi_def_cfa_offset 64
.Lcfi848:
	.cfi_offset %rbx, -40
.Lcfi849:
	.cfi_offset %r12, -32
.Lcfi850:
	.cfi_offset %r14, -24
.Lcfi851:
	.cfi_offset %r15, -16
	movq	%rcx, %r12
	movq	%rdx, %rbx
	movq	%rsi, %r15
	leaq	8(%rsp), %rsi
	callq	get_c_string_dim
	movq	%rax, %r14
	leaq	16(%rsp), %rsi
	movq	%r15, %rdi
	callq	get_c_string_dim
	movq	%rax, %r15
	testq	%rbx, %rbx
	je	.LBB136_4
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB136_3
# BB#2:                                 # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB136_3:                              # %get_c_long.exit
	cvttsd2si	8(%rbx), %rbx
	testq	%r12, %r12
	jne	.LBB136_5
	jmp	.LBB136_8
.LBB136_4:
	xorl	%ebx, %ebx
	testq	%r12, %r12
	je	.LBB136_8
.LBB136_5:
	movzwl	2(%r12), %eax
	cmpl	$2, %eax
	je	.LBB136_7
# BB#6:                                 # %.critedge.i29
	movl	$.L.str.44, %edi
	movq	%r12, %rsi
	callq	err
.LBB136_7:                              # %get_c_long.exit30
	cvttsd2si	8(%r12), %rcx
	jmp	.LBB136_9
.LBB136_8:
	movq	8(%rsp), %rcx
.LBB136_9:
	xorl	%eax, %eax
	testq	%rbx, %rbx
	js	.LBB136_18
# BB#10:
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	jl	.LBB136_18
# BB#11:
	testq	%rcx, %rcx
	js	.LBB136_18
# BB#12:
	cmpq	16(%rsp), %rcx
	jg	.LBB136_18
# BB#13:
	cmpq	8(%rsp), %rdx
	jne	.LBB136_16
# BB#14:
	addq	%rbx, %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB136_17
.LBB136_16:
	xorl	%eax, %eax
	jmp	.LBB136_18
.LBB136_17:
	callq	a_true_value
.LBB136_18:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end136:
	.size	substring_equal, .Lfunc_end136-substring_equal
	.cfi_endproc

	.globl	substring_equalcase
	.p2align	4, 0x90
	.type	substring_equalcase,@function
substring_equalcase:                    # @substring_equalcase
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi852:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi853:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi854:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi855:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi856:
	.cfi_def_cfa_offset 64
.Lcfi857:
	.cfi_offset %rbx, -40
.Lcfi858:
	.cfi_offset %r12, -32
.Lcfi859:
	.cfi_offset %r14, -24
.Lcfi860:
	.cfi_offset %r15, -16
	movq	%rcx, %r12
	movq	%rdx, %rbx
	movq	%rsi, %r15
	leaq	8(%rsp), %rsi
	callq	get_c_string_dim
	movq	%rax, %r14
	leaq	16(%rsp), %rsi
	movq	%r15, %rdi
	callq	get_c_string_dim
	movq	%rax, %r15
	testq	%rbx, %rbx
	je	.LBB137_4
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB137_3
# BB#2:                                 # %.critedge.i
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB137_3:                              # %get_c_long.exit
	cvttsd2si	8(%rbx), %rbx
	testq	%r12, %r12
	jne	.LBB137_5
	jmp	.LBB137_8
.LBB137_4:
	xorl	%ebx, %ebx
	testq	%r12, %r12
	je	.LBB137_8
.LBB137_5:
	movzwl	2(%r12), %eax
	cmpl	$2, %eax
	je	.LBB137_7
# BB#6:                                 # %.critedge.i29
	movl	$.L.str.44, %edi
	movq	%r12, %rsi
	callq	err
.LBB137_7:                              # %get_c_long.exit30
	cvttsd2si	8(%r12), %rcx
	jmp	.LBB137_9
.LBB137_8:
	movq	8(%rsp), %rcx
.LBB137_9:
	xorl	%eax, %eax
	testq	%rbx, %rbx
	js	.LBB137_18
# BB#10:
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	jl	.LBB137_18
# BB#11:
	testq	%rcx, %rcx
	js	.LBB137_18
# BB#12:
	cmpq	16(%rsp), %rcx
	jg	.LBB137_18
# BB#13:
	cmpq	8(%rsp), %rdx
	jne	.LBB137_16
# BB#14:
	addq	%rbx, %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB137_17
.LBB137_16:
	xorl	%eax, %eax
	jmp	.LBB137_18
.LBB137_17:
	callq	a_true_value
.LBB137_18:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end137:
	.size	substring_equalcase, .Lfunc_end137-substring_equalcase
	.cfi_endproc

	.globl	set_eval_history
	.p2align	4, 0x90
	.type	set_eval_history,@function
set_eval_history:                       # @set_eval_history
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi861:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi862:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi863:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi864:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi865:
	.cfi_def_cfa_offset 48
.Lcfi866:
	.cfi_offset %rbx, -40
.Lcfi867:
	.cfi_offset %r12, -32
.Lcfi868:
	.cfi_offset %r14, -24
.Lcfi869:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB138_11
# BB#1:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB138_3
# BB#2:                                 # %.critedge.i.i
	movl	$.L.str.44, %edi
	movq	%r14, %rsi
	callq	err
.LBB138_3:                              # %get_c_long.exit.i
	cvttsd2si	8(%r14), %rbx
	testq	%rbx, %rbx
	jle	.LBB138_11
# BB#4:                                 # %.lr.ph.i.preheader
	incq	%rbx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB138_5:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edi, %edi
	movq	%r12, %rsi
	callq	cons
	movq	%rax, %r12
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB138_5
# BB#6:                                 # %make_list.exit
	testq	%r15, %r15
	je	.LBB138_18
# BB#7:                                 # %make_list.exit
	testq	%r12, %r12
	je	.LBB138_18
# BB#8:
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	jne	.LBB138_12
# BB#9:
	movq	16(%r12), %rax
	jmp	.LBB138_13
.LBB138_11:
	xorl	%r12d, %r12d
	jmp	.LBB138_18
.LBB138_12:                             # %.critedge.i.i10
	movl	$.L.str.76, %edi
	movq	%r12, %rsi
	callq	err
.LBB138_13:                             # %.preheader.i.i
	testq	%rax, %rax
	movq	%r12, %rdi
	je	.LBB138_17
# BB#14:                                # %.lr.ph.i.i.preheader
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB138_15:                             # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdi
	movq	%rax, %rcx
	movzwl	2(%rcx), %eax
	cmpl	$1, %eax
	jne	.LBB138_17
# BB#16:                                #   in Loop: Header=BB138_15 Depth=1
	movq	16(%rcx), %rax
	testq	%rax, %rax
	movq	%rcx, %rdi
	jne	.LBB138_15
.LBB138_17:                             # %last.exit.i
	movq	%r12, %rsi
	callq	setcdr
.LBB138_18:                             # %nconc.exit
	movl	$.L.str.120, %edi
	callq	cintern
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	setvar
	movl	$.L.str.121, %edi
	callq	cintern
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	setvar
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end138:
	.size	set_eval_history, .Lfunc_end138-set_eval_history
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI139_0:
	.quad	4607182418800017408     # double 1
.LCPI139_1:
	.quad	4614256656552045848     # double 3.1415926535897931
	.text
	.globl	init_subrs_a
	.p2align	4, 0x90
	.type	init_subrs_a,@function
init_subrs_a:                           # @init_subrs_a
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi870:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi871:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi872:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi873:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi874:
	.cfi_def_cfa_offset 48
.Lcfi875:
	.cfi_offset %rbx, -40
.Lcfi876:
	.cfi_offset %r12, -32
.Lcfi877:
	.cfi_offset %r14, -24
.Lcfi878:
	.cfi_offset %r15, -16
	movl	$.L.str.122, %edi
	movl	$aref1, %esi
	callq	init_subr_2
	movl	$.L.str.123, %edi
	movl	$aset1, %esi
	callq	init_subr_3
	movl	$.L.str.124, %edi
	movl	$string_append, %esi
	callq	init_lsubr
	movl	$.L.str.125, %edi
	movl	$bytes_append, %esi
	callq	init_lsubr
	movl	$.L.str.126, %edi
	movl	$string_length, %esi
	callq	init_subr_1
	movl	$.L.str.127, %edi
	movl	$string_dim, %esi
	callq	init_subr_1
	movl	$.L.str.128, %edi
	movl	$read_from_string, %esi
	callq	init_subr_1
	movl	$.L.str.129, %edi
	movl	$print_to_string, %esi
	callq	init_subr_3
	movl	$.L.str.130, %edi
	movl	$cons_array, %esi
	callq	init_subr_2
	movl	$.L.str.131, %edi
	movl	$sxhash, %esi
	callq	init_subr_2
	movl	$.L.str.132, %edi
	movl	$equal, %esi
	callq	init_subr_2
	movl	$.L.str.133, %edi
	movl	$href, %esi
	callq	init_subr_2
	movl	$.L.str.134, %edi
	movl	$hset, %esi
	callq	init_subr_3
	movl	$.L.str.135, %edi
	movl	$assoc, %esi
	callq	init_subr_2
	movl	$.L.str.136, %edi
	movl	$assv, %esi
	callq	init_subr_2
	movl	$.L.str.137, %edi
	movl	$fast_read, %esi
	callq	init_subr_1
	movl	$.L.str.138, %edi
	movl	$fast_print, %esi
	callq	init_subr_2
	movl	$.L.str.139, %edi
	movl	$make_list, %esi
	callq	init_subr_2
	movl	$.L.str.140, %edi
	movl	$lfread, %esi
	callq	init_subr_2
	movl	$.L.str.141, %edi
	movl	$lfwrite, %esi
	callq	init_subr_2
	movl	$.L.str.142, %edi
	movl	$lfflush, %esi
	callq	init_subr_1
	movl	$.L.str.143, %edi
	movl	$llength, %esi
	callq	init_subr_1
	movl	$.L.str.144, %edi
	movl	$number2string, %esi
	callq	init_subr_4
	movl	$.L.str.145, %edi
	movl	$string2number, %esi
	callq	init_subr_2
	movl	$.L.str.146, %edi
	movl	$substring, %esi
	callq	init_subr_3
	movl	$.L.str.147, %edi
	movl	$string_search, %esi
	callq	init_subr_2
	movl	$.L.str.148, %edi
	movl	$string_trim, %esi
	callq	init_subr_1
	movl	$.L.str.149, %edi
	movl	$string_trim_left, %esi
	callq	init_subr_1
	movl	$.L.str.150, %edi
	movl	$string_trim_right, %esi
	callq	init_subr_1
	movl	$.L.str.151, %edi
	movl	$string_upcase, %esi
	callq	init_subr_1
	movl	$.L.str.152, %edi
	movl	$string_downcase, %esi
	callq	init_subr_1
	movl	$.L.str.153, %edi
	movl	$lstrcmp, %esi
	callq	init_subr_2
	movl	$.L.str.154, %edi
	movl	$lstrcat, %esi
	callq	init_subr_2
	movl	$.L.str.155, %edi
	movl	$lstrcpy, %esi
	callq	init_subr_2
	movl	$.L.str.156, %edi
	movl	$lstrbreakup, %esi
	callq	init_subr_2
	movl	$.L.str.157, %edi
	movl	$lstrunbreakup, %esi
	callq	init_subr_2
	movl	$.L.str.158, %edi
	movl	$stringp, %esi
	callq	init_subr_1
	movl	$sym_e, %edi
	movl	$.L.str.159, %esi
	callq	gc_protect_sym
	movl	$sym_f, %edi
	movl	$.L.str.160, %esi
	callq	gc_protect_sym
	movl	$sym_plists, %edi
	movl	$.L.str.161, %esi
	callq	gc_protect_sym
	movq	sym_plists(%rip), %r14
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r15
	xorl	%ebx, %ebx
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r12
	movq	$100, 8(%r12)
	movl	$800, %edi              # imm = 0x320
	callq	must_malloc
	movq	%rax, 16(%r12)
	movq	$0, (%rax)
	.p2align	4, 0x90
.LBB139_1:                              # %.lr.ph75..lr.ph75_crit_edge.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	movq	$0, 8(%rax,%rbx,8)
	movq	16(%r12), %rax
	movq	$0, 16(%rax,%rbx,8)
	movq	16(%r12), %rax
	movq	$0, 24(%rax,%rbx,8)
	addq	$3, %rbx
	cmpq	$99, %rbx
	jne	.LBB139_1
# BB#2:                                 # %arcons.exit
	movw	$16, 2(%r12)
	movq	%r15, %rdi
	callq	no_interrupt
	xorl	%r15d, %r15d
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	setvar
	movl	$.L.str.162, %edi
	movl	$lref_default, %esi
	callq	init_subr_3
	movl	$.L.str.163, %edi
	movl	$larg_default, %esi
	callq	init_subr_3
	movl	$.L.str.164, %edi
	movl	$lkey_default, %esi
	callq	init_subr_3
	movl	$.L.str.165, %edi
	movl	$llist, %esi
	callq	init_lsubr
	movl	$.L.str.166, %edi
	movl	$writes, %esi
	callq	init_lsubr
	movl	$.L.str.167, %edi
	movl	$lqsort, %esi
	callq	init_subr_3
	movl	$.L.str.168, %edi
	movl	$string_lessp, %esi
	callq	init_subr_2
	movl	$.L.str.169, %edi
	movl	$mapcar, %esi
	callq	init_lsubr
	movl	$.L.str.170, %edi
	movl	$mapcar2, %esi
	callq	init_subr_3
	movl	$.L.str.171, %edi
	movl	$mapcar1, %esi
	callq	init_subr_2
	movl	$.L.str.172, %edi
	movl	$benchmark_funcall1, %esi
	callq	init_subr_3
	movl	$.L.str.173, %edi
	movl	$benchmark_funcall2, %esi
	callq	init_lsubr
	movl	$.L.str.174, %edi
	movl	$benchmark_eval, %esi
	callq	init_subr_3
	movl	$.L.str.175, %edi
	movl	$lfmod, %esi
	callq	init_subr_2
	movl	$.L.str.176, %edi
	movl	$lsubset, %esi
	callq	init_subr_2
	movl	$.L.str.177, %edi
	movl	$base64encode, %esi
	callq	init_subr_1
	movl	$.L.str.178, %edi
	movl	$base64decode, %esi
	callq	init_subr_1
	movl	$.L.str.179, %edi
	movl	$ass, %esi
	callq	init_subr_3
	movl	$.L.str.180, %edi
	movl	$append2, %esi
	callq	init_subr_2
	movl	$.L.str.181, %edi
	movl	$append, %esi
	callq	init_lsubr
	movl	$.L.str.182, %edi
	movl	$fast_save, %esi
	callq	init_subr_5
	movl	$.L.str.183, %edi
	movl	$fast_load, %esi
	callq	init_subr_2
	movl	$.L.str.184, %edi
	movl	$swrite, %esi
	callq	init_subr_3
	movl	$.L.str.185, %edi
	movl	$lpow, %esi
	callq	init_subr_2
	movl	$.L.str.186, %edi
	movl	$lexp, %esi
	callq	init_subr_1
	movl	$.L.str.187, %edi
	movl	$llog, %esi
	callq	init_subr_1
	movl	$.L.str.188, %edi
	movl	$lsin, %esi
	callq	init_subr_1
	movl	$.L.str.189, %edi
	movl	$lcos, %esi
	callq	init_subr_1
	movl	$.L.str.190, %edi
	movl	$ltan, %esi
	callq	init_subr_1
	movl	$.L.str.191, %edi
	movl	$lasin, %esi
	callq	init_subr_1
	movl	$.L.str.192, %edi
	movl	$lacos, %esi
	callq	init_subr_1
	movl	$.L.str.193, %edi
	movl	$latan, %esi
	callq	init_subr_1
	movl	$.L.str.194, %edi
	movl	$latan2, %esi
	callq	init_subr_2
	movl	$.L.str.195, %edi
	movl	$ltypeof, %esi
	callq	init_subr_1
	movl	$.L.str.196, %edi
	movl	$caaar, %esi
	callq	init_subr_1
	movl	$.L.str.197, %edi
	movl	$caadr, %esi
	callq	init_subr_1
	movl	$.L.str.198, %edi
	movl	$cadar, %esi
	callq	init_subr_1
	movl	$.L.str.199, %edi
	movl	$caddr, %esi
	callq	init_subr_1
	movl	$.L.str.200, %edi
	movl	$cdaar, %esi
	callq	init_subr_1
	movl	$.L.str.201, %edi
	movl	$cdadr, %esi
	callq	init_subr_1
	movl	$.L.str.202, %edi
	movl	$cddar, %esi
	callq	init_subr_1
	movl	$.L.str.203, %edi
	movl	$cdddr, %esi
	callq	init_subr_1
	movl	$.L.str.204, %edi
	callq	cintern
	movq	%rax, %rbx
	movsd	.LCPI139_0(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	atan
	movq	.LCPI139_1(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$256, %edi              # imm = 0x100
	callq	malloc
	movq	%rax, base64_decode_table(%rip)
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 240(%rax)
	movdqu	%xmm0, 224(%rax)
	movdqu	%xmm0, 208(%rax)
	movdqu	%xmm0, 192(%rax)
	movdqu	%xmm0, 176(%rax)
	movdqu	%xmm0, 160(%rax)
	movdqu	%xmm0, 144(%rax)
	movdqu	%xmm0, 128(%rax)
	movdqu	%xmm0, 112(%rax)
	movdqu	%xmm0, 96(%rax)
	movdqu	%xmm0, 80(%rax)
	movdqu	%xmm0, 64(%rax)
	movdqu	%xmm0, 48(%rax)
	movdqu	%xmm0, 32(%rax)
	movdqu	%xmm0, 16(%rax)
	movdqu	%xmm0, (%rax)
	.p2align	4, 0x90
.LBB139_3:                              # =>This Inner Loop Header: Depth=1
	movsbq	.L.str.232(%r15), %rcx
	movb	%r15b, (%rax,%rcx)
	leal	1(%r15), %ecx
	movsbq	.L.str.232+1(%r15), %rdx
	movb	%cl, (%rax,%rdx)
	leal	2(%r15), %ecx
	movsbq	.L.str.232+2(%r15), %rdx
	movb	%cl, (%rax,%rdx)
	leal	3(%r15), %ecx
	movsbq	.L.str.232+3(%r15), %rdx
	movb	%cl, (%rax,%rdx)
	leal	4(%r15), %ecx
	movsbq	.L.str.232+4(%r15), %rdx
	movb	%cl, (%rax,%rdx)
	addq	$5, %r15
	cmpq	$65, %r15
	jne	.LBB139_3
# BB#4:                                 # %init_base64_table.exit
	movl	$.L.str.205, %edi
	movl	$hexstr, %esi
	callq	init_subr_1
	movl	$.L.str.206, %edi
	movl	$hexstr2bytes, %esi
	callq	init_subr_1
	movl	$.L.str.179, %edi
	movl	$ass, %esi
	callq	init_subr_3
	movl	$.L.str.207, %edi
	movl	$bitand, %esi
	callq	init_subr_2
	movl	$.L.str.208, %edi
	movl	$bitor, %esi
	callq	init_subr_2
	movl	$.L.str.209, %edi
	movl	$bitxor, %esi
	callq	init_subr_2
	movl	$.L.str.210, %edi
	movl	$bitnot, %esi
	callq	init_subr_1
	movl	$.L.str.211, %edi
	movl	$leval_cond, %esi
	callq	init_msubr
	movl	$.L.str.212, %edi
	movl	$leval_prog1, %esi
	callq	init_fsubr
	movl	$.L.str.213, %edi
	movl	$lstrspn, %esi
	callq	init_subr_2
	movl	$.L.str.214, %edi
	movl	$lstrcspn, %esi
	callq	init_subr_2
	movl	$.L.str.215, %edi
	movl	$substring_equal, %esi
	callq	init_subr_4
	movl	$.L.str.216, %edi
	movl	$substring_equalcase, %esi
	callq	init_subr_4
	movl	$.L.str.217, %edi
	movl	$butlast, %esi
	callq	init_subr_1
	movl	$.L.str.218, %edi
	movl	$ash, %esi
	callq	init_subr_2
	movl	$.L.str.219, %edi
	movl	$getprop, %esi
	callq	init_subr_2
	movl	$.L.str.220, %edi
	movl	$setprop, %esi
	callq	init_subr_3
	movl	$.L.str.221, %edi
	movl	$putprop, %esi
	callq	init_subr_3
	movl	$.L.str.222, %edi
	movl	$last, %esi
	callq	init_subr_1
	movl	$.L.str.223, %edi
	movl	$memq, %esi
	callq	init_subr_2
	movl	$.L.str.224, %edi
	movl	$memv, %esi
	callq	init_subr_2
	movl	$.L.str.225, %edi
	movl	$member, %esi
	callq	init_subr_2
	movl	$.L.str.226, %edi
	movl	$nth, %esi
	callq	init_subr_2
	movl	$.L.str.227, %edi
	movl	$nconc, %esi
	callq	init_subr_2
	movl	$.L.str.228, %edi
	movl	$set_eval_history, %esi
	callq	init_subr_2
	movl	$.L.str.229, %edi
	movl	$parser_fasl, %esi
	callq	init_subr_1
	movl	$.L.str.230, %edi
	callq	cintern
	movq	%rax, %rbx
	callq	a_true_value
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.231, %edi
	movl	$parser_fasl_hook, %esi
	callq	init_subr_2
	movl	$.L.str.234, %edi
	callq	cintern
	movq	%rax, %rbx
	movl	$.L.str.235, %edi
	callq	cintern
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	setvar                  # TAILCALL
.Lfunc_end139:
	.size	init_subrs_a, .Lfunc_end139-init_subrs_a
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI140_0:
	.quad	4636737291354636288     # double 100
	.text
	.p2align	4, 0x90
	.type	parser_fasl,@function
parser_fasl:                            # @parser_fasl
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi879:
	.cfi_def_cfa_offset 16
.Lcfi880:
	.cfi_offset %rbx, -16
	movsd	.LCPI140_0(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	cons_array
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	callq	flocons
	movq	%rax, %rcx
	movl	$3, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	listn
	movq	%rax, %rbx
	movl	$.L.str.231, %edi
	callq	cintern
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	leval
	movq	%rbx, %rdi
	movq	%rax, %rsi
	popq	%rbx
	jmp	closure                 # TAILCALL
.Lfunc_end140:
	.size	parser_fasl, .Lfunc_end140-parser_fasl
	.cfi_endproc

	.p2align	4, 0x90
	.type	parser_fasl_hook,@function
parser_fasl_hook:                       # @parser_fasl_hook
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi881:
	.cfi_def_cfa_offset 16
.Lcfi882:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	setcar
	movq	%rbx, %rdi
	callq	fast_read
	cmpq	%rbx, %rax
	je	.LBB141_2
# BB#1:
	popq	%rbx
	retq
.LBB141_2:
	popq	%rbx
	jmp	get_eof_val             # TAILCALL
.Lfunc_end141:
	.size	parser_fasl_hook, .Lfunc_end141-parser_fasl_hook
	.cfi_endproc

	.type	bashnum,@object         # @bashnum
	.local	bashnum
	.comm	bashnum,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\""
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\"\\\n\r\t"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\\n"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\\r"
	.size	.L.str.3, 3

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\\t"
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"#("
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%g"
	.size	.L.str.6, 3

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	" "
	.size	.L.str.7, 2

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	")"
	.size	.L.str.8, 2

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%ld"
	.size	.L.str.9, 4

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"#%ld\""
	.size	.L.str.10, 6

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%02x"
	.size	.L.str.11, 5

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"print to string overflow"
	.size	.L.str.12, 25

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"not a string"
	.size	.L.str.13, 13

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"bad index to aref"
	.size	.L.str.14, 18

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"negative index to aref"
	.size	.L.str.15, 23

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"index too large"
	.size	.L.str.16, 16

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"invalid argument to aref"
	.size	.L.str.17, 25

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"index to aset too large"
	.size	.L.str.18, 24

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"bad value to store in array"
	.size	.L.str.19, 28

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"bad index to aset"
	.size	.L.str.20, 18

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"negative index to aset"
	.size	.L.str.21, 23

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"invalid argument to aset"
	.size	.L.str.22, 25

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"bad dimension to cons-array"
	.size	.L.str.23, 28

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"double"
	.size	.L.str.24, 7

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"long"
	.size	.L.str.25, 5

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"string"
	.size	.L.str.26, 7

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"byte"
	.size	.L.str.27, 5

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"lisp"
	.size	.L.str.28, 5

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"bad type of array"
	.size	.L.str.29, 18

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"bad start index"
	.size	.L.str.30, 16

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"bad end index"
	.size	.L.str.31, 14

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"eof after \\"
	.size	.L.str.33, 12

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"eof after \\0"
	.size	.L.str.34, 13

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"read string overflow"
	.size	.L.str.35, 21

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"readsharp syntax not handled"
	.size	.L.str.36, 29

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"not a hash table"
	.size	.L.str.37, 17

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"sxhash inconsistency"
	.size	.L.str.38, 21

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"improper list to assoc"
	.size	.L.str.39, 23

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"improper list to assv"
	.size	.L.str.40, 22

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"symbol name too long"
	.size	.L.str.41, 21

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"cannot fast-print"
	.size	.L.str.42, 18

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"unknown fast-read opcode"
	.size	.L.str.43, 25

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"not a number"
	.size	.L.str.44, 13

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"write length too long"
	.size	.L.str.45, 22

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"improper list to length"
	.size	.L.str.46, 24

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"wta to length"
	.size	.L.str.47, 14

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"wta"
	.size	.L.str.48, 4

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"width too long"
	.size	.L.str.49, 15

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"precision too large"
	.size	.L.str.50, 20

	.type	sym_e,@object           # @sym_e
	.local	sym_e
	.comm	sym_e,8,8
	.type	sym_f,@object           # @sym_f
	.local	sym_f
	.comm	sym_f,8,8
	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"% *.*g"
	.size	.L.str.51, 7

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"% *.*e"
	.size	.L.str.52, 7

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"% *.*f"
	.size	.L.str.53, 7

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"% *g"
	.size	.L.str.54, 5

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"% *e"
	.size	.L.str.55, 5

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"% *f"
	.size	.L.str.56, 5

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"%.*g"
	.size	.L.str.57, 5

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"%.*e"
	.size	.L.str.58, 5

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"%.*f"
	.size	.L.str.59, 5

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"%e"
	.size	.L.str.60, 3

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"%f"
	.size	.L.str.61, 3

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"%0*ld"
	.size	.L.str.62, 6

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"%0*lo"
	.size	.L.str.63, 6

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"%0*lX"
	.size	.L.str.64, 6

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"%lo"
	.size	.L.str.65, 4

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"%lX"
	.size	.L.str.66, 4

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"number base not handled"
	.size	.L.str.67, 24

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"%lx"
	.size	.L.str.68, 4

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"string too long"
	.size	.L.str.69, 16

	.type	base64_decode_table,@object # @base64_decode_table
	.local	base64_decode_table
	.comm	base64_decode_table,8,8
	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"illegal base64 data length"
	.size	.L.str.70, 27

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"improper list to memq"
	.size	.L.str.71, 22

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"improper list to member"
	.size	.L.str.72, 24

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"improper list to memv"
	.size	.L.str.73, 22

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"bad arg to nth"
	.size	.L.str.74, 15

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"bad arg to last"
	.size	.L.str.76, 16

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"list is empty"
	.size	.L.str.77, 14

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"not a list"
	.size	.L.str.78, 11

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"bad list to qsort"
	.size	.L.str.79, 18

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"mapcar case not handled"
	.size	.L.str.80, 24

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"wta(1st) to fmod"
	.size	.L.str.81, 17

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"wta(2nd) to fmod"
	.size	.L.str.82, 17

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"improper list to ass"
	.size	.L.str.83, 21

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"fast loading "
	.size	.L.str.84, 14

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"\n"
	.size	.L.str.85, 2

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"rb"
	.size	.L.str.86, 3

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"done.\n"
	.size	.L.str.87, 7

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"fast saving forms to "
	.size	.L.str.88, 22

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"wb"
	.size	.L.str.89, 3

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"# Siod Binary Object Save File\n"
	.size	.L.str.90, 32

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"# sizeof(long) = %d\n# sizeof(double) = %d\n"
	.size	.L.str.91, 43

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"# 1 = %s\n"
	.size	.L.str.92, 10

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"# 1.0 = %s\n"
	.size	.L.str.93, 12

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"no object repeat count"
	.size	.L.str.94, 23

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"wta(1st) to pow"
	.size	.L.str.95, 16

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"wta(2nd) to pow"
	.size	.L.str.96, 16

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"not implemented"
	.size	.L.str.97, 16

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"tc_nil"
	.size	.L.str.98, 7

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"tc_cons"
	.size	.L.str.99, 8

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"tc_flonum"
	.size	.L.str.100, 10

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"tc_symbol"
	.size	.L.str.101, 10

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"tc_subr_0"
	.size	.L.str.102, 10

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"tc_subr_1"
	.size	.L.str.103, 10

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"tc_subr_2"
	.size	.L.str.104, 10

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"tc_subr_2n"
	.size	.L.str.105, 11

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"tc_subr_3"
	.size	.L.str.106, 10

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"tc_subr_4"
	.size	.L.str.107, 10

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"tc_subr_5"
	.size	.L.str.108, 10

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"tc_lsubr"
	.size	.L.str.109, 9

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"tc_fsubr"
	.size	.L.str.110, 9

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"tc_msubr"
	.size	.L.str.111, 9

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"tc_closure"
	.size	.L.str.112, 11

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"tc_free_cell"
	.size	.L.str.113, 13

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"tc_string"
	.size	.L.str.114, 10

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"tc_byte_array"
	.size	.L.str.115, 14

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"tc_double_array"
	.size	.L.str.116, 16

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"tc_long_array"
	.size	.L.str.117, 14

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"tc_lisp_array"
	.size	.L.str.118, 14

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"tc_c_file"
	.size	.L.str.119, 10

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"*eval-history-ptr*"
	.size	.L.str.120, 19

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"*eval-history*"
	.size	.L.str.121, 15

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"aref"
	.size	.L.str.122, 5

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"aset"
	.size	.L.str.123, 5

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"string-append"
	.size	.L.str.124, 14

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"bytes-append"
	.size	.L.str.125, 13

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"string-length"
	.size	.L.str.126, 14

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"string-dimension"
	.size	.L.str.127, 17

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"read-from-string"
	.size	.L.str.128, 17

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"print-to-string"
	.size	.L.str.129, 16

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"cons-array"
	.size	.L.str.130, 11

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"sxhash"
	.size	.L.str.131, 7

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"equal?"
	.size	.L.str.132, 7

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"href"
	.size	.L.str.133, 5

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"hset"
	.size	.L.str.134, 5

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"assoc"
	.size	.L.str.135, 6

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"assv"
	.size	.L.str.136, 5

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"fast-read"
	.size	.L.str.137, 10

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"fast-print"
	.size	.L.str.138, 11

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"make-list"
	.size	.L.str.139, 10

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"fread"
	.size	.L.str.140, 6

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"fwrite"
	.size	.L.str.141, 7

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"fflush"
	.size	.L.str.142, 7

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"length"
	.size	.L.str.143, 7

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"number->string"
	.size	.L.str.144, 15

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"string->number"
	.size	.L.str.145, 15

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"substring"
	.size	.L.str.146, 10

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"string-search"
	.size	.L.str.147, 14

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"string-trim"
	.size	.L.str.148, 12

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"string-trim-left"
	.size	.L.str.149, 17

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"string-trim-right"
	.size	.L.str.150, 18

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"string-upcase"
	.size	.L.str.151, 14

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"string-downcase"
	.size	.L.str.152, 16

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"strcmp"
	.size	.L.str.153, 7

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"strcat"
	.size	.L.str.154, 7

	.type	.L.str.155,@object      # @.str.155
.L.str.155:
	.asciz	"strcpy"
	.size	.L.str.155, 7

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	"strbreakup"
	.size	.L.str.156, 11

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	"unbreakupstr"
	.size	.L.str.157, 13

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	"string?"
	.size	.L.str.158, 8

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	"e"
	.size	.L.str.159, 2

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	"f"
	.size	.L.str.160, 2

	.type	sym_plists,@object      # @sym_plists
	.local	sym_plists
	.comm	sym_plists,8,8
	.type	.L.str.161,@object      # @.str.161
.L.str.161:
	.asciz	"*plists*"
	.size	.L.str.161, 9

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.asciz	"lref-default"
	.size	.L.str.162, 13

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	"larg-default"
	.size	.L.str.163, 13

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	"lkey-default"
	.size	.L.str.164, 13

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	"list"
	.size	.L.str.165, 5

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	"writes"
	.size	.L.str.166, 7

	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	"qsort"
	.size	.L.str.167, 6

	.type	.L.str.168,@object      # @.str.168
.L.str.168:
	.asciz	"string-lessp"
	.size	.L.str.168, 13

	.type	.L.str.169,@object      # @.str.169
.L.str.169:
	.asciz	"mapcar"
	.size	.L.str.169, 7

	.type	.L.str.170,@object      # @.str.170
.L.str.170:
	.asciz	"mapcar2"
	.size	.L.str.170, 8

	.type	.L.str.171,@object      # @.str.171
.L.str.171:
	.asciz	"mapcar1"
	.size	.L.str.171, 8

	.type	.L.str.172,@object      # @.str.172
.L.str.172:
	.asciz	"benchmark-funcall1"
	.size	.L.str.172, 19

	.type	.L.str.173,@object      # @.str.173
.L.str.173:
	.asciz	"benchmark-funcall2"
	.size	.L.str.173, 19

	.type	.L.str.174,@object      # @.str.174
.L.str.174:
	.asciz	"benchmark-eval"
	.size	.L.str.174, 15

	.type	.L.str.175,@object      # @.str.175
.L.str.175:
	.asciz	"fmod"
	.size	.L.str.175, 5

	.type	.L.str.176,@object      # @.str.176
.L.str.176:
	.asciz	"subset"
	.size	.L.str.176, 7

	.type	.L.str.177,@object      # @.str.177
.L.str.177:
	.asciz	"base64encode"
	.size	.L.str.177, 13

	.type	.L.str.178,@object      # @.str.178
.L.str.178:
	.asciz	"base64decode"
	.size	.L.str.178, 13

	.type	.L.str.179,@object      # @.str.179
.L.str.179:
	.asciz	"ass"
	.size	.L.str.179, 4

	.type	.L.str.180,@object      # @.str.180
.L.str.180:
	.asciz	"append2"
	.size	.L.str.180, 8

	.type	.L.str.181,@object      # @.str.181
.L.str.181:
	.asciz	"append"
	.size	.L.str.181, 7

	.type	.L.str.182,@object      # @.str.182
.L.str.182:
	.asciz	"fast-save"
	.size	.L.str.182, 10

	.type	.L.str.183,@object      # @.str.183
.L.str.183:
	.asciz	"fast-load"
	.size	.L.str.183, 10

	.type	.L.str.184,@object      # @.str.184
.L.str.184:
	.asciz	"swrite"
	.size	.L.str.184, 7

	.type	.L.str.185,@object      # @.str.185
.L.str.185:
	.asciz	"pow"
	.size	.L.str.185, 4

	.type	.L.str.186,@object      # @.str.186
.L.str.186:
	.asciz	"exp"
	.size	.L.str.186, 4

	.type	.L.str.187,@object      # @.str.187
.L.str.187:
	.asciz	"log"
	.size	.L.str.187, 4

	.type	.L.str.188,@object      # @.str.188
.L.str.188:
	.asciz	"sin"
	.size	.L.str.188, 4

	.type	.L.str.189,@object      # @.str.189
.L.str.189:
	.asciz	"cos"
	.size	.L.str.189, 4

	.type	.L.str.190,@object      # @.str.190
.L.str.190:
	.asciz	"tan"
	.size	.L.str.190, 4

	.type	.L.str.191,@object      # @.str.191
.L.str.191:
	.asciz	"asin"
	.size	.L.str.191, 5

	.type	.L.str.192,@object      # @.str.192
.L.str.192:
	.asciz	"acos"
	.size	.L.str.192, 5

	.type	.L.str.193,@object      # @.str.193
.L.str.193:
	.asciz	"atan"
	.size	.L.str.193, 5

	.type	.L.str.194,@object      # @.str.194
.L.str.194:
	.asciz	"atan2"
	.size	.L.str.194, 6

	.type	.L.str.195,@object      # @.str.195
.L.str.195:
	.asciz	"typeof"
	.size	.L.str.195, 7

	.type	.L.str.196,@object      # @.str.196
.L.str.196:
	.asciz	"caaar"
	.size	.L.str.196, 6

	.type	.L.str.197,@object      # @.str.197
.L.str.197:
	.asciz	"caadr"
	.size	.L.str.197, 6

	.type	.L.str.198,@object      # @.str.198
.L.str.198:
	.asciz	"cadar"
	.size	.L.str.198, 6

	.type	.L.str.199,@object      # @.str.199
.L.str.199:
	.asciz	"caddr"
	.size	.L.str.199, 6

	.type	.L.str.200,@object      # @.str.200
.L.str.200:
	.asciz	"cdaar"
	.size	.L.str.200, 6

	.type	.L.str.201,@object      # @.str.201
.L.str.201:
	.asciz	"cdadr"
	.size	.L.str.201, 6

	.type	.L.str.202,@object      # @.str.202
.L.str.202:
	.asciz	"cddar"
	.size	.L.str.202, 6

	.type	.L.str.203,@object      # @.str.203
.L.str.203:
	.asciz	"cdddr"
	.size	.L.str.203, 6

	.type	.L.str.204,@object      # @.str.204
.L.str.204:
	.asciz	"*pi*"
	.size	.L.str.204, 5

	.type	.L.str.205,@object      # @.str.205
.L.str.205:
	.asciz	"array->hexstr"
	.size	.L.str.205, 14

	.type	.L.str.206,@object      # @.str.206
.L.str.206:
	.asciz	"hexstr->bytes"
	.size	.L.str.206, 14

	.type	.L.str.207,@object      # @.str.207
.L.str.207:
	.asciz	"bit-and"
	.size	.L.str.207, 8

	.type	.L.str.208,@object      # @.str.208
.L.str.208:
	.asciz	"bit-or"
	.size	.L.str.208, 7

	.type	.L.str.209,@object      # @.str.209
.L.str.209:
	.asciz	"bit-xor"
	.size	.L.str.209, 8

	.type	.L.str.210,@object      # @.str.210
.L.str.210:
	.asciz	"bit-not"
	.size	.L.str.210, 8

	.type	.L.str.211,@object      # @.str.211
.L.str.211:
	.asciz	"cond"
	.size	.L.str.211, 5

	.type	.L.str.212,@object      # @.str.212
.L.str.212:
	.asciz	"prog1"
	.size	.L.str.212, 6

	.type	.L.str.213,@object      # @.str.213
.L.str.213:
	.asciz	"strspn"
	.size	.L.str.213, 7

	.type	.L.str.214,@object      # @.str.214
.L.str.214:
	.asciz	"strcspn"
	.size	.L.str.214, 8

	.type	.L.str.215,@object      # @.str.215
.L.str.215:
	.asciz	"substring-equal?"
	.size	.L.str.215, 17

	.type	.L.str.216,@object      # @.str.216
.L.str.216:
	.asciz	"substring-equalcase?"
	.size	.L.str.216, 21

	.type	.L.str.217,@object      # @.str.217
.L.str.217:
	.asciz	"butlast"
	.size	.L.str.217, 8

	.type	.L.str.218,@object      # @.str.218
.L.str.218:
	.asciz	"ash"
	.size	.L.str.218, 4

	.type	.L.str.219,@object      # @.str.219
.L.str.219:
	.asciz	"get"
	.size	.L.str.219, 4

	.type	.L.str.220,@object      # @.str.220
.L.str.220:
	.asciz	"setprop"
	.size	.L.str.220, 8

	.type	.L.str.221,@object      # @.str.221
.L.str.221:
	.asciz	"putprop"
	.size	.L.str.221, 8

	.type	.L.str.222,@object      # @.str.222
.L.str.222:
	.asciz	"last"
	.size	.L.str.222, 5

	.type	.L.str.223,@object      # @.str.223
.L.str.223:
	.asciz	"memq"
	.size	.L.str.223, 5

	.type	.L.str.224,@object      # @.str.224
.L.str.224:
	.asciz	"memv"
	.size	.L.str.224, 5

	.type	.L.str.225,@object      # @.str.225
.L.str.225:
	.asciz	"member"
	.size	.L.str.225, 7

	.type	.L.str.226,@object      # @.str.226
.L.str.226:
	.asciz	"nth"
	.size	.L.str.226, 4

	.type	.L.str.227,@object      # @.str.227
.L.str.227:
	.asciz	"nconc"
	.size	.L.str.227, 6

	.type	.L.str.228,@object      # @.str.228
.L.str.228:
	.asciz	"set-eval-history"
	.size	.L.str.228, 17

	.type	.L.str.229,@object      # @.str.229
.L.str.229:
	.asciz	"parser_fasl"
	.size	.L.str.229, 12

	.type	.L.str.230,@object      # @.str.230
.L.str.230:
	.asciz	"*parser_fasl.scm-loaded*"
	.size	.L.str.230, 25

	.type	.L.str.231,@object      # @.str.231
.L.str.231:
	.asciz	"parser_fasl_hook"
	.size	.L.str.231, 17

	.type	.L.str.232,@object      # @.str.232
.L.str.232:
	.asciz	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
	.size	.L.str.232, 66

	.type	.L.str.233,@object      # @.str.233
.L.str.233:
	.asciz	"%02X"
	.size	.L.str.233, 5

	.type	.L.str.234,@object      # @.str.234
.L.str.234:
	.asciz	"*sliba-version*"
	.size	.L.str.234, 16

	.type	.L.str.235,@object      # @.str.235
.L.str.235:
	.asciz	"$Id: sliba.c 9206 2003-10-17 18:48:45Z gaeke $"
	.size	.L.str.235, 47


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
