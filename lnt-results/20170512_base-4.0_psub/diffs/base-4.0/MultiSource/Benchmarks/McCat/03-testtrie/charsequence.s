	.text
	.file	"charsequence.bc"
	.globl	charsequence_reset
	.p2align	4, 0x90
	.type	charsequence_reset,@function
charsequence_reset:                     # @charsequence_reset
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_2
# BB#1:
	callq	free
.LBB0_2:
	movl	$16, %edi
	callq	malloc
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB0_3
.LBB0_4:
	movl	$16, %eax
	movd	%rax, %xmm0
	movdqu	%xmm0, 8(%rbx)
	popq	%rbx
	retq
.LBB0_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB0_4
.Lfunc_end0:
	.size	charsequence_reset, .Lfunc_end0-charsequence_reset
	.cfi_endproc

	.globl	charsequence_push
	.p2align	4, 0x90
	.type	charsequence_push,@function
charsequence_push:                      # @charsequence_push
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB1_1
# BB#6:
	leaq	16(%rbx), %rbp
	cmpq	%rsi, 16(%rbx)
	jne	.LBB1_9
# BB#7:
	addq	%rsi, %rsi
	movq	%rsi, 8(%rbx)
	movq	(%rbx), %rdi
	callq	realloc
	movq	%rax, (%rbx)
	testq	%rax, %rax
	jne	.LBB1_9
# BB#8:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB1_9
.LBB1_1:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_3
# BB#2:
	callq	free
.LBB1_3:
	movl	$16, %edi
	callq	malloc
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB1_4
.LBB1_5:                                # %.thread
	leaq	16(%rbx), %rbp
	movl	$16, %eax
	movd	%rax, %xmm0
	movdqu	%xmm0, 8(%rbx)
.LBB1_9:
	movq	(%rbx), %rax
	movq	(%rbp), %rcx
	movb	%r14b, (%rax,%rcx)
	incq	(%rbp)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB1_4:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB1_5
.Lfunc_end1:
	.size	charsequence_push, .Lfunc_end1-charsequence_push
	.cfi_endproc

	.globl	charsequence_pop
	.p2align	4, 0x90
	.type	charsequence_pop,@function
charsequence_pop:                       # @charsequence_pop
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB2_1
.LBB2_2:
	movq	(%rbx), %rcx
	leaq	-1(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movb	(%rcx,%rax), %al
	popq	%rbx
	retq
.LBB2_1:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	16(%rbx), %rax
	jmp	.LBB2_2
.Lfunc_end2:
	.size	charsequence_pop, .Lfunc_end2-charsequence_pop
	.cfi_endproc

	.globl	charsequence_val
	.p2align	4, 0x90
	.type	charsequence_val,@function
charsequence_val:                       # @charsequence_val
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	16(%r14), %r15
	leaq	1(%r15), %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_1
.LBB3_2:
	movq	(%r14), %rsi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	strncpy
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB3_1:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movq	16(%r14), %r15
	jmp	.LBB3_2
.Lfunc_end3:
	.size	charsequence_val, .Lfunc_end3-charsequence_val
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"OUT OF MEMORY"
	.size	.L.str, 14

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"ATTEMPTED POP ON EMPTY SEQUENCE"
	.size	.L.str.1, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
