	.text
	.file	"utop.bc"
	.globl	utop
	.p2align	4, 0x90
	.type	utop,@function
utop:                                   # @utop
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movl	$2, %edi
	xorl	%eax, %eax
	callq	palloc
	testq	%rax, %rax
	je	.LBB0_4
# BB#1:
	movb	$0, 6(%rax)
	movq	%rax, %rdx
	addq	$8, %rdx
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movw	%bx, (%rcx)
	addq	$2, %rcx
	shrl	$16, %ebx
	jne	.LBB0_2
# BB#3:
	subl	%edx, %ecx
	shrl	%ecx
	movw	%cx, 4(%rax)
	movq	%rax, %rdi
	popq	%rbx
	jmp	presult                 # TAILCALL
.LBB0_4:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	utop, .Lfunc_end0-utop
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
