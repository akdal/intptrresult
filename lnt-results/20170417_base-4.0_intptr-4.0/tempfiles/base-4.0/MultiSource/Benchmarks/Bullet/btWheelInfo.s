	.text
	.file	"btWheelInfo.bc"
	.globl	_ZNK11btWheelInfo23getSuspensionRestLengthEv
	.p2align	4, 0x90
	.type	_ZNK11btWheelInfo23getSuspensionRestLengthEv,@function
_ZNK11btWheelInfo23getSuspensionRestLengthEv: # @_ZNK11btWheelInfo23getSuspensionRestLengthEv
	.cfi_startproc
# BB#0:
	movss	208(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end0:
	.size	_ZNK11btWheelInfo23getSuspensionRestLengthEv, .Lfunc_end0-_ZNK11btWheelInfo23getSuspensionRestLengthEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1092616192              # float 10
.LCPI1_1:
	.long	3184315597              # float -0.100000001
.LCPI1_2:
	.long	3212836864              # float -1
.LCPI1_4:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_3:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN11btWheelInfo11updateWheelERK11btRigidBodyRNS_11RaycastInfoE
	.p2align	4, 0x90
	.type	_ZN11btWheelInfo11updateWheelERK11btRigidBodyRNS_11RaycastInfoE,@function
_ZN11btWheelInfo11updateWheelERK11btRigidBodyRNS_11RaycastInfoE: # @_ZN11btWheelInfo11updateWheelERK11btRigidBodyRNS_11RaycastInfoE
	.cfi_startproc
# BB#0:
	cmpb	$0, 84(%rdi)
	je	.LBB1_5
# BB#1:
	movss	(%rdi), %xmm10          # xmm10 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	52(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm0
	movss	56(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm0, %xmm4
	movss	8(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	60(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm4, %xmm3
	ucomiss	.LCPI1_1(%rip), %xmm3
	jae	.LBB1_2
# BB#3:
	movss	16(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	56(%rsi), %xmm4
	movss	20(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	60(%rsi), %xmm5
	movss	24(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	64(%rsi), %xmm6
	movss	344(%rsi), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movss	348(%rsi), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movss	352(%rsi), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm2
	mulss	%xmm7, %xmm2
	mulss	%xmm4, %xmm7
	mulss	%xmm9, %xmm4
	mulss	%xmm6, %xmm9
	subss	%xmm2, %xmm9
	addss	328(%rsi), %xmm9
	mulss	%xmm9, %xmm10
	mulss	%xmm8, %xmm6
	subss	%xmm6, %xmm7
	addss	332(%rsi), %xmm7
	mulss	%xmm7, %xmm1
	addss	%xmm10, %xmm1
	mulss	%xmm8, %xmm5
	subss	%xmm4, %xmm5
	addss	336(%rsi), %xmm5
	mulss	%xmm5, %xmm0
	addss	%xmm1, %xmm0
	movss	.LCPI1_2(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm1
	mulss	%xmm1, %xmm0
	movss	%xmm0, 276(%rdi)
	movss	%xmm1, 272(%rdi)
	retq
.LBB1_5:
	movl	208(%rdi), %eax
	movl	%eax, 32(%rdi)
	movl	$0, 276(%rdi)
	movsd	52(%rdi), %xmm0         # xmm0 = mem[0],zero
	movaps	.LCPI1_3(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm0
	movss	60(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm0, (%rdi)
	movlps	%xmm1, 8(%rdi)
	movss	.LCPI1_4(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 272(%rdi)
	retq
.LBB1_2:
	movss	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	movss	%xmm0, 276(%rdi)
	movss	%xmm1, 272(%rdi)
	retq
.Lfunc_end1:
	.size	_ZN11btWheelInfo11updateWheelERK11btRigidBodyRNS_11RaycastInfoE, .Lfunc_end1-_ZN11btWheelInfo11updateWheelERK11btRigidBodyRNS_11RaycastInfoE
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
