	.text
	.file	"fbench.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	4665101392816373760     # double 7621
	.quad	4664275610105890734     # double 6869.9549999999999
.LCPI0_1:
	.quad	4663937907204047241     # double 6562.8159999999998
	.quad	4663204673685809005     # double 5895.9440000000004
.LCPI0_2:
	.quad	4662515953895821279     # double 5269.5569999999998
	.quad	4662067118955711955     # double 4861.3440000000001
.LCPI0_3:
	.quad	4661494419632687153     # double 4340.4769999999999
	.quad	4660945225669240947     # double 3968.4940000000001
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_4:
	.quad	4652007308841189376     # double 1000
.LCPI0_5:
	.quad	4602678819172646912     # double 0.5
.LCPI0_6:
	.quad	4607182418800017408     # double 1
.LCPI0_7:
	.quad	4546461098394361986     # double 9.2600000000000001E-5
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:                                 # %.preheader56
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [7.621000e+03,6.869955e+03]
	movups	%xmm0, spectral_line+8(%rip)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [6.562816e+03,5.895944e+03]
	movups	%xmm0, spectral_line+24(%rip)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [5.269557e+03,4.861344e+03]
	movups	%xmm0, spectral_line+40(%rip)
	movaps	.LCPI0_3(%rip), %xmm0   # xmm0 = [4.340477e+03,3.968494e+03]
	movups	%xmm0, spectral_line+56(%rip)
	movl	$1000000, niter(%rip)   # imm = 0xF4240
	movabsq	$4616189618054758400, %rax # imm = 0x4010000000000000
	movq	%rax, clear_aperture(%rip)
	movb	$1, current_surfaces(%rip)
	movaps	testcase+16(%rip), %xmm0
	movaps	%xmm0, s+64(%rip)
	movaps	testcase(%rip), %xmm0
	movaps	%xmm0, s+48(%rip)
	movaps	testcase+48(%rip), %xmm0
	movups	%xmm0, s+104(%rip)
	movaps	testcase+32(%rip), %xmm0
	movups	%xmm0, s+88(%rip)
	movaps	testcase+80(%rip), %xmm0
	movaps	%xmm0, s+144(%rip)
	movaps	testcase+64(%rip), %xmm0
	movaps	%xmm0, s+128(%rip)
	movaps	testcase+112(%rip), %xmm0
	movups	%xmm0, s+184(%rip)
	movaps	testcase+96(%rip), %xmm0
	movups	%xmm0, s+168(%rip)
	movl	$.Lstr, %edi
	callq	puts
	movl	niter(%rip), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	niter(%rip), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	.LCPI0_4(%rip), %xmm0
	movl	$.L.str.2, %edi
	movb	$1, %al
	callq	printf
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$0, itercount(%rip)
	cmpl	$0, niter(%rip)
	jle	.LBB0_5
# BB#1:                                 # %.preheader.preheader
	movabsq	$4567911030049346683, %rbx # imm = 0x3F647AE147AE147B
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_3 Depth 2
	movw	$0, paraxial(%rip)
	.p2align	4, 0x90
.LBB0_3:                                #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	clear_aperture(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI0_5(%rip), %xmm0
	movl	$4, %edi
	callq	trace_line
	movq	object_distance(%rip), %rax
	movswq	paraxial(%rip), %rcx
	leal	1(%rcx), %edx
	shlq	$4, %rcx
	movq	%rax, od_sa(%rcx)
	movq	axis_slope_angle(%rip), %rax
	movq	%rax, od_sa+8(%rcx)
	movw	%dx, paraxial(%rip)
	movswl	%dx, %eax
	cmpl	$2, %eax
	jl	.LBB0_3
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	movw	$0, paraxial(%rip)
	movsd	clear_aperture(%rip), %xmm0 # xmm0 = mem[0],zero
	movsd	.LCPI0_5(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movl	$3, %edi
	callq	trace_line
	movsd	object_distance(%rip), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	clear_aperture(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	.LCPI0_5(%rip), %xmm0
	movl	$6, %edi
	callq	trace_line
	movsd	object_distance(%rip), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	od_sa+16(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm0
	subsd	od_sa(%rip), %xmm0
	movsd	%xmm0, aberr_lspher(%rip)
	mulsd	od_sa+24(%rip), %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	od_sa+8(%rip), %xmm0    # xmm0 = mem[0],zero
	callq	sin
	mulsd	od_sa(%rip), %xmm0
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	.LCPI0_6(%rip), %xmm0   # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	movsd	%xmm0, aberr_osc(%rip)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	subsd	(%rsp), %xmm0           # 8-byte Folded Reload
	movsd	%xmm0, aberr_lchrom(%rip)
	movsd	od_sa+8(%rip), %xmm0    # xmm0 = mem[0],zero
	callq	sin
	mulsd	%xmm0, %xmm0
	movsd	.LCPI0_7(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, max_lspher(%rip)
	movq	%rbx, max_osc(%rip)
	movsd	%xmm1, max_lchrom(%rip)
	movl	itercount(%rip), %eax
	incl	%eax
	movl	%eax, itercount(%rip)
	cmpl	niter(%rip), %eax
	jl	.LBB0_2
.LBB0_5:                                # %._crit_edge61
	movsd	od_sa(%rip), %xmm0      # xmm0 = mem[0],zero
	movsd	od_sa+8(%rip), %xmm1    # xmm1 = mem[0],zero
	movl	$outarr, %ebx
	movl	$outarr, %edi
	movl	$.L.str.5, %esi
	movl	$.L.str.6, %edx
	movb	$2, %al
	callq	sprintf
	movsd	od_sa+16(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	od_sa+24(%rip), %xmm1   # xmm1 = mem[0],zero
	movl	$outarr+80, %edi
	movl	$.L.str.5, %esi
	movl	$.L.str.7, %edx
	movb	$2, %al
	callq	sprintf
	movsd	aberr_lspher(%rip), %xmm0 # xmm0 = mem[0],zero
	movl	$outarr+160, %edi
	movl	$.L.str.8, %esi
	movb	$1, %al
	callq	sprintf
	movsd	max_lspher(%rip), %xmm0 # xmm0 = mem[0],zero
	movl	$outarr+240, %edi
	movl	$.L.str.9, %esi
	movb	$1, %al
	callq	sprintf
	movsd	aberr_osc(%rip), %xmm0  # xmm0 = mem[0],zero
	movl	$outarr+320, %edi
	movl	$.L.str.10, %esi
	movb	$1, %al
	callq	sprintf
	movsd	max_osc(%rip), %xmm0    # xmm0 = mem[0],zero
	movl	$outarr+400, %edi
	movl	$.L.str.9, %esi
	movb	$1, %al
	callq	sprintf
	movsd	aberr_lchrom(%rip), %xmm0 # xmm0 = mem[0],zero
	movl	$outarr+480, %edi
	movl	$.L.str.11, %esi
	movb	$1, %al
	callq	sprintf
	movsd	max_lchrom(%rip), %xmm0 # xmm0 = mem[0],zero
	movl	$outarr+560, %edi
	movl	$.L.str.9, %esi
	movb	$1, %al
	callq	sprintf
	movl	$32, %r13d
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_10 Depth 2
	leaq	(%rbp,%rbp,4), %rax
	shlq	$4, %rax
	leaq	outarr(%rax), %r15
	movq	refarr(,%rbp,8), %r14
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	strcmp
	incq	%rbp
	testl	%eax, %eax
	je	.LBB0_12
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=1
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	printf
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	printf
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	printf
	movq	%r14, %rdi
	callq	strlen
	testl	%eax, %eax
	movq	%rbp, (%rsp)            # 8-byte Spill
	jle	.LBB0_8
# BB#9:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_6 Depth=1
	movl	%eax, %ebp
	movq	%rbx, %r15
	.p2align	4, 0x90
.LBB0_10:                               # %.lr.ph
                                        #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r14), %eax
	cmpb	(%rbx), %al
	movl	$94, %edi
	cmovel	%r13d, %edi
	callq	putchar
	movzbl	(%r14), %eax
	xorl	%ecx, %ecx
	cmpb	(%rbx), %al
	setne	%cl
	addl	%ecx, %r12d
	incq	%r14
	incq	%rbx
	decq	%rbp
	jne	.LBB0_10
	jmp	.LBB0_11
	.p2align	4, 0x90
.LBB0_8:                                #   in Loop: Header=BB0_6 Depth=1
	movq	%rbx, %r15
.LBB0_11:                               # %._crit_edge
                                        #   in Loop: Header=BB0_6 Depth=1
	movl	$10, %edi
	callq	putchar
	movq	%r15, %rbx
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB0_12:                               # %._crit_edge77
                                        #   in Loop: Header=BB0_6 Depth=1
	addq	$80, %rbx
	cmpq	$8, %rbp
	jne	.LBB0_6
# BB#13:
	testl	%r12d, %r12d
	jle	.LBB0_15
# BB#14:
	cmpl	$1, %r12d
	movl	$.L.str.19, %eax
	movl	$.L.str.20, %edx
	cmovneq	%rax, %rdx
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	printf
	jmp	.LBB0_16
.LBB0_15:
	movl	$.Lstr.3, %edi
	callq	puts
.LBB0_16:
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4607182418800017408     # double 1
.LCPI1_1:
	.quad	-4616189618054758400    # double -1
.LCPI1_2:
	.quad	4602678819172646912     # double 0.5
	.text
	.p2align	4, 0x90
	.type	trace_line,@function
trace_line:                             # @trace_line
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 64
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	$0, object_distance(%rip)
	movsd	%xmm0, ray_height(%rip)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, from_index(%rip)
	cmpb	$1, current_surfaces(%rip)
	jne	.LBB1_22
# BB#1:                                 # %.lr.ph
	movslq	%edi, %r14
	xorps	%xmm3, %xmm3
	movsd	.LCPI1_0(%rip), %xmm4   # xmm4 = mem[0],zero
	movl	$1, %r15d
	movl	$s+72, %ebx
	movapd	%xmm4, %xmm6
	xorpd	%xmm5, %xmm5
	jmp	.LBB1_2
	.p2align	4, 0x90
.LBB1_21:                               # %.thread
                                        #   in Loop: Header=BB1_2 Depth=1
	movd	%rax, %xmm6
	subsd	(%rbx), %xmm5
	movsd	%xmm5, object_distance(%rip)
	incq	%r15
	addq	$40, %rbx
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movq	-24(%rbx), %rax
	movq	%rax, radius_of_curvature(%rip)
	movsd	-16(%rbx), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, to_index(%rip)
	ucomisd	%xmm4, %xmm0
	jbe	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movsd	spectral_line+32(%rip), %xmm1 # xmm1 = mem[0],zero
	subsd	spectral_line(,%r14,8), %xmm1
	movsd	spectral_line+24(%rip), %xmm2 # xmm2 = mem[0],zero
	subsd	spectral_line+48(%rip), %xmm2
	divsd	%xmm2, %xmm1
	movapd	%xmm0, %xmm2
	addsd	.LCPI1_1(%rip), %xmm2
	divsd	-8(%rbx), %xmm2
	mulsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm0
	movsd	%xmm0, to_index(%rip)
.LBB1_4:                                #   in Loop: Header=BB1_2 Depth=1
	movd	%rax, %xmm2
	cmpw	$0, paraxial(%rip)
	je	.LBB1_14
# BB#5:                                 #   in Loop: Header=BB1_2 Depth=1
	ucomisd	%xmm3, %xmm2
	jne	.LBB1_6
	jnp	.LBB1_13
.LBB1_6:                                #   in Loop: Header=BB1_2 Depth=1
	ucomisd	%xmm3, %xmm5
	jne	.LBB1_8
	jp	.LBB1_8
# BB#7:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	$0, axis_slope_angle(%rip)
	movsd	ray_height(%rip), %xmm1 # xmm1 = mem[0],zero
	divsd	%xmm2, %xmm1
	xorpd	%xmm2, %xmm2
	jmp	.LBB1_9
	.p2align	4, 0x90
.LBB1_14:                               #   in Loop: Header=BB1_2 Depth=1
	ucomisd	%xmm3, %xmm2
	jne	.LBB1_15
	jnp	.LBB1_19
.LBB1_15:                               #   in Loop: Header=BB1_2 Depth=1
	ucomisd	%xmm3, %xmm5
	jne	.LBB1_17
	jp	.LBB1_17
# BB#16:                                #   in Loop: Header=BB1_2 Depth=1
	movq	$0, axis_slope_angle(%rip)
	movsd	ray_height(%rip), %xmm0 # xmm0 = mem[0],zero
	divsd	%xmm2, %xmm0
	jmp	.LBB1_18
	.p2align	4, 0x90
.LBB1_13:                               #   in Loop: Header=BB1_2 Depth=1
	movapd	%xmm0, %xmm1
	divsd	%xmm6, %xmm1
	mulsd	%xmm1, %xmm5
	movsd	%xmm5, object_distance(%rip)
	divsd	%xmm0, %xmm6
	mulsd	axis_slope_angle(%rip), %xmm6
	movsd	%xmm6, axis_slope_angle(%rip)
	jmp	.LBB1_20
.LBB1_8:                                #   in Loop: Header=BB1_2 Depth=1
	movapd	%xmm5, %xmm1
	subsd	%xmm2, %xmm1
	divsd	%xmm2, %xmm1
	movsd	axis_slope_angle(%rip), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
.LBB1_9:                                #   in Loop: Header=BB1_2 Depth=1
	divsd	%xmm0, %xmm6
	mulsd	%xmm1, %xmm6
	addsd	%xmm2, %xmm1
	subsd	%xmm6, %xmm1
	movsd	%xmm1, axis_slope_angle(%rip)
	ucomisd	%xmm3, %xmm5
	jne	.LBB1_11
	jnp	.LBB1_10
.LBB1_11:                               #   in Loop: Header=BB1_2 Depth=1
	mulsd	%xmm2, %xmm5
	movsd	%xmm5, ray_height(%rip)
	jmp	.LBB1_12
.LBB1_10:                               # %._crit_edge.i
                                        #   in Loop: Header=BB1_2 Depth=1
	movsd	ray_height(%rip), %xmm5 # xmm5 = mem[0],zero
.LBB1_12:                               #   in Loop: Header=BB1_2 Depth=1
	divsd	%xmm1, %xmm5
	movsd	%xmm5, object_distance(%rip)
	jmp	.LBB1_20
.LBB1_19:                               #   in Loop: Header=BB1_2 Depth=1
	divsd	%xmm0, %xmm6
	movsd	%xmm6, (%rsp)           # 8-byte Spill
	movsd	axis_slope_angle(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	sin
	mulsd	(%rsp), %xmm0           # 8-byte Folded Reload
	callq	asin
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	object_distance(%rip), %xmm1 # xmm1 = mem[0],zero
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movsd	to_index(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	callq	cos
	mulsd	(%rsp), %xmm0           # 8-byte Folded Reload
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	from_index(%rip), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movsd	axis_slope_angle(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	cos
	movsd	(%rsp), %xmm5           # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movsd	.LCPI1_0(%rip), %xmm4   # xmm4 = mem[0],zero
	xorps	%xmm3, %xmm3
	mulsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	divsd	%xmm0, %xmm5
	mulsd	16(%rsp), %xmm5         # 8-byte Folded Reload
	movsd	%xmm5, object_distance(%rip)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, axis_slope_angle(%rip)
	jmp	.LBB1_20
.LBB1_17:                               #   in Loop: Header=BB1_2 Depth=1
	subsd	%xmm2, %xmm5
	divsd	%xmm2, %xmm5
	movsd	%xmm5, (%rsp)           # 8-byte Spill
	movsd	axis_slope_angle(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	sin
	mulsd	(%rsp), %xmm0           # 8-byte Folded Reload
.LBB1_18:                               #   in Loop: Header=BB1_2 Depth=1
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	callq	asin
	movapd	%xmm0, %xmm1
	movsd	from_index(%rip), %xmm0 # xmm0 = mem[0],zero
	divsd	to_index(%rip), %xmm0
	mulsd	(%rsp), %xmm0           # 8-byte Folded Reload
	addsd	axis_slope_angle(%rip), %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	callq	asin
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm1
	subsd	%xmm0, %xmm1
	movsd	%xmm1, axis_slope_angle(%rip)
	movapd	%xmm2, %xmm0
	mulsd	.LCPI1_2(%rip), %xmm0
	callq	sin
	movsd	radius_of_curvature(%rip), %xmm1 # xmm1 = mem[0],zero
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	addsd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	axis_slope_angle(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	tan
	movsd	.LCPI1_0(%rip), %xmm4   # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm5
	divsd	%xmm0, %xmm5
	mulsd	(%rsp), %xmm5           # 8-byte Folded Reload
	addsd	16(%rsp), %xmm5         # 8-byte Folded Reload
	movsd	%xmm5, object_distance(%rip)
	xorps	%xmm3, %xmm3
.LBB1_20:                               # %transit_surface.exit
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	to_index(%rip), %rax
	movq	%rax, from_index(%rip)
	movzbl	current_surfaces(%rip), %ecx
	shlq	$2, %rcx
	cmpq	%rcx, %r15
	jl	.LBB1_21
.LBB1_22:                               # %._crit_edge
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	trace_line, .Lfunc_end1-trace_line
	.cfi_endproc

	.type	niter,@object           # @niter
	.data
	.globl	niter
	.p2align	2
niter:
	.long	1000                    # 0x3e8
	.size	niter, 4

	.type	spectral_line,@object   # @spectral_line
	.local	spectral_line
	.comm	spectral_line,72,16
	.type	clear_aperture,@object  # @clear_aperture
	.local	clear_aperture
	.comm	clear_aperture,8,8
	.type	current_surfaces,@object # @current_surfaces
	.local	current_surfaces
	.comm	current_surfaces,1,2
	.type	testcase,@object        # @testcase
	.section	.rodata,"a",@progbits
	.p2align	4
testcase:
	.quad	4628307115802152141     # double 27.050000000000001
	.quad	4609495917928597632     # double 1.5137
	.quad	4634147721568898253     # double 63.600000000000001
	.quad	4602858963157741732     # double 0.52000000000000002
	.quad	-4597983816561113170    # double -16.68
	.quad	4607182418800017408     # double 1
	.quad	0                       # double 0
	.quad	4594139994279152452     # double 0.13800000000000001
	.quad	-4597983816561113170    # double -16.68
	.quad	4609958437610328582     # double 1.6164000000000001
	.quad	4630361883132139930     # double 36.700000000000003
	.quad	4600517091351509074     # double 0.38
	.quad	-4588175820997630362    # double -78.099999999999994
	.quad	4607182418800017408     # double 1
	.quad	0                       # double 0
	.quad	0                       # double 0
	.size	testcase, 128

	.type	s,@object               # @s
	.local	s
	.comm	s,400,16
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"and performance benchmark.  %d iterations will be made.\n\n"
	.size	.L.str.1, 58

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\nMeasured run time in seconds should be divided by %.f\n"
	.size	.L.str.2, 56

	.type	itercount,@object       # @itercount
	.comm	itercount,4,4
	.type	paraxial,@object        # @paraxial
	.local	paraxial
	.comm	paraxial,2,2
	.type	object_distance,@object # @object_distance
	.local	object_distance
	.comm	object_distance,8,8
	.type	od_sa,@object           # @od_sa
	.local	od_sa
	.comm	od_sa,32,16
	.type	axis_slope_angle,@object # @axis_slope_angle
	.local	axis_slope_angle
	.comm	axis_slope_angle,8,8
	.type	aberr_lspher,@object    # @aberr_lspher
	.local	aberr_lspher
	.comm	aberr_lspher,8,8
	.type	aberr_osc,@object       # @aberr_osc
	.local	aberr_osc
	.comm	aberr_osc,8,8
	.type	aberr_lchrom,@object    # @aberr_lchrom
	.local	aberr_lchrom
	.comm	aberr_lchrom,8,8
	.type	max_lspher,@object      # @max_lspher
	.local	max_lspher
	.comm	max_lspher,8,8
	.type	max_osc,@object         # @max_osc
	.local	max_osc
	.comm	max_osc,8,8
	.type	max_lchrom,@object      # @max_lchrom
	.local	max_lchrom
	.comm	max_lchrom,8,8
	.type	outarr,@object          # @outarr
	.local	outarr
	.comm	outarr,640,16
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%15s   %21.11f  %14.11f"
	.size	.L.str.5, 24

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Marginal ray"
	.size	.L.str.6, 13

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Paraxial ray"
	.size	.L.str.7, 13

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Longitudinal spherical aberration:      %16.11f"
	.size	.L.str.8, 48

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"    (Maximum permissible):              %16.11f"
	.size	.L.str.9, 48

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Offense against sine condition (coma):  %16.11f"
	.size	.L.str.10, 48

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Axial chromatic aberration:             %16.11f"
	.size	.L.str.11, 48

	.type	refarr,@object          # @refarr
	.section	.rodata,"a",@progbits
	.p2align	4
refarr:
	.quad	.L.str.22
	.quad	.L.str.23
	.quad	.L.str.24
	.quad	.L.str.25
	.quad	.L.str.26
	.quad	.L.str.27
	.quad	.L.str.28
	.quad	.L.str.25
	.size	refarr, 64

	.type	.L.str.12,@object       # @.str.12
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.12:
	.asciz	"\nError in results on line %d...\n"
	.size	.L.str.12, 33

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Expected:  \"%s\"\n"
	.size	.L.str.13, 17

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Received:  \"%s\"\n"
	.size	.L.str.14, 17

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"(Errors)    "
	.size	.L.str.15, 13

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"\n%d error%s in results.  This is VERY SERIOUS.\n"
	.size	.L.str.18, 48

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"s"
	.size	.L.str.19, 2

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.zero	1
	.size	.L.str.20, 1

	.type	ray_height,@object      # @ray_height
	.local	ray_height
	.comm	ray_height,8,8
	.type	from_index,@object      # @from_index
	.local	from_index
	.comm	from_index,8,8
	.type	radius_of_curvature,@object # @radius_of_curvature
	.local	radius_of_curvature
	.comm	radius_of_curvature,8,8
	.type	to_index,@object        # @to_index
	.local	to_index
	.comm	to_index,8,8
	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"   Marginal ray          47.09479120920   0.04178472683"
	.size	.L.str.22, 56

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"   Paraxial ray          47.08372160249   0.04177864821"
	.size	.L.str.23, 56

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Longitudinal spherical aberration:        -0.01106960671"
	.size	.L.str.24, 57

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"    (Maximum permissible):                 0.05306749907"
	.size	.L.str.25, 57

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Offense against sine condition (coma):     0.00008954761"
	.size	.L.str.26, 57

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"    (Maximum permissible):                 0.00250000000"
	.size	.L.str.27, 57

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Axial chromatic aberration:                0.00448229032"
	.size	.L.str.28, 57

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Ready to begin John Walker's floating point accuracy"
	.size	.Lstr, 53

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"to normalise for reporting results.  For archival results,"
	.size	.Lstr.1, 59

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"adjust iteration count so the benchmark runs about five minutes.\n"
	.size	.Lstr.2, 66

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"\nNo errors in results."
	.size	.Lstr.3, 23


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
