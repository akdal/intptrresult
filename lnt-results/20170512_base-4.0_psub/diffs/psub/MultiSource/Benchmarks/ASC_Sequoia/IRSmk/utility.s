	.text
	.file	"utility.bc"
	.globl	readInput
	.p2align	4, 0x90
	.type	readInput,@function
readInput:                              # @readInput
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$.L.str, %esi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_3
# BB#1:
	movl	$.L.str.3, %esi
	movl	$kmin, %edx
	movl	$kmax, %ecx
	movl	$jmin, %r8d
	movl	$jmax, %r9d
	movl	$0, %eax
	movq	%r14, %rdi
	pushq	$jp
.Lcfi5:
	.cfi_adjust_cfa_offset 8
	pushq	$kp
.Lcfi6:
	.cfi_adjust_cfa_offset 8
	pushq	$imax
.Lcfi7:
	.cfi_adjust_cfa_offset 8
	pushq	$imin
.Lcfi8:
	.cfi_adjust_cfa_offset 8
	callq	fscanf
	addq	$32, %rsp
.Lcfi9:
	.cfi_adjust_cfa_offset -32
	movl	$.Lstr, %edi
	callq	puts
	movl	kmin(%rip), %esi
	movl	kmax(%rip), %edx
	movl	jmin(%rip), %ecx
	movl	jmax(%rip), %r8d
	movl	imin(%rip), %r9d
	movl	imax(%rip), %r10d
	movl	kp(%rip), %r11d
	movl	jp(%rip), %ebx
	subq	$8, %rsp
.Lcfi10:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.5, %edi
	movl	$0, %eax
	pushq	%rbx
.Lcfi11:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi12:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$32, %rsp
.Lcfi14:
	.cfi_adjust_cfa_offset -32
	movl	jp(%rip), %eax
	movl	jmin(%rip), %edx
	imull	%eax, %edx
	addl	imin(%rip), %edx
	movl	kp(%rip), %ecx
	movl	kmin(%rip), %esi
	imull	%ecx, %esi
	addl	%edx, %esi
	movl	%esi, i_lb(%rip)
	movl	jmax(%rip), %edx
	decl	%edx
	imull	%eax, %edx
	movl	kmax(%rip), %edi
	decl	%edi
	imull	%ecx, %edi
	addl	imax(%rip), %edx
	addl	%edi, %edx
	movl	%edx, i_ub(%rip)
	subl	%ecx, %esi
	cmpl	%eax, %esi
	jle	.LBB0_4
# BB#2:
	addl	%ecx, %eax
	leal	12(%rdx,%rax,2), %eax
	movl	%eax, x_size(%rip)
	movl	$.Lstr.1, %edi
	callq	puts
	movl	i_lb(%rip), %esi
	movl	i_ub(%rip), %edx
	movl	x_size(%rip), %ecx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	fclose                  # TAILCALL
.LBB0_3:
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	movl	$1, %edi
	callq	exit
.LBB0_4:
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	readInput, .Lfunc_end0-readInput
	.cfi_endproc

	.globl	allocMem
	.p2align	4, 0x90
	.type	allocMem,@function
allocMem:                               # @allocMem
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	i_ub(%rip), %eax
	shll	$3, %eax
	movslq	%eax, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, (%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 8(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 16(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 24(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 32(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 40(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 48(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 56(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 64(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 72(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 80(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 88(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 96(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 104(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 112(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 120(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 128(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 136(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 144(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 152(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 160(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 168(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 176(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 184(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 192(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 200(%r14)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 208(%r14)
	testq	%rax, %rax
	je	.LBB1_2
# BB#1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_2:
	movl	$.Lstr.5, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	puts                    # TAILCALL
.Lfunc_end1:
	.size	allocMem, .Lfunc_end1-allocMem
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	init
	.p2align	4, 0x90
	.type	init,@function
init:                                   # @init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$48, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 104
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movl	imin(%rip), %eax
	movl	%eax, 8(%rdi)
	movl	imax(%rip), %eax
	movl	%eax, 20(%rdi)
	movl	jmin(%rip), %eax
	movl	%eax, 12(%rdi)
	movl	jmax(%rip), %eax
	movl	%eax, 24(%rdi)
	movl	kmin(%rip), %eax
	movl	%eax, 16(%rdi)
	movl	kmax(%rip), %eax
	movl	%eax, 28(%rdi)
	movl	jp(%rip), %eax
	movl	%eax, 68(%rdi)
	movl	kp(%rip), %eax
	movl	%eax, 72(%rdi)
	movl	i_ub(%rip), %edi
	testl	%edi, %edi
	jle	.LBB2_3
# BB#1:                                 # %.lr.ph211
	movq	208(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	200(%rsi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	192(%rsi), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	184(%rsi), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movq	176(%rsi), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movq	168(%rsi), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movq	160(%rsi), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movq	152(%rsi), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movq	144(%rsi), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movq	136(%rsi), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movq	128(%rsi), %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	movq	120(%rsi), %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	movq	112(%rsi), %rax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	movq	104(%rsi), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	movq	96(%rsi), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	movq	88(%rsi), %rax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	movq	80(%rsi), %rax
	movq	%rax, -112(%rsp)        # 8-byte Spill
	movq	72(%rsi), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movq	64(%rsi), %rax
	movq	%rax, -128(%rsp)        # 8-byte Spill
	movq	56(%rsi), %r13
	movq	48(%rsi), %r14
	movq	40(%rsi), %r12
	movq	32(%rsi), %rax
	movq	24(%rsi), %rbp
	movq	16(%rsi), %r8
	movq	(%rsi), %r15
	movq	8(%rsi), %rsi
	xorpd	%xmm9, %xmm9
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	$0, (%rcx,%r9,8)
	movsd	%xmm9, (%r15,%r9,8)
	leaq	1(%r9), %r10
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r10d, %xmm1
	movsd	%xmm1, (%rsi,%r9,8)
	leal	2(%r9), %r11d
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r11d, %xmm1
	movsd	%xmm1, (%r8,%r9,8)
	leal	3(%r9), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	movsd	%xmm1, (%rbp,%r9,8)
	leal	4(%r9), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	movsd	%xmm1, (%rax,%r9,8)
	leal	5(%r9), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	movsd	%xmm1, (%r12,%r9,8)
	leal	6(%r9), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	movsd	%xmm1, (%r14,%r9,8)
	leal	7(%r9), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	movsd	%xmm1, (%r13,%r9,8)
	leal	8(%r9), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	movq	-128(%rsp), %rbx        # 8-byte Reload
	movsd	%xmm1, (%rbx,%r9,8)
	leal	9(%r9), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	movq	-120(%rsp), %rbx        # 8-byte Reload
	movsd	%xmm1, (%rbx,%r9,8)
	leal	10(%r9), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	movq	-112(%rsp), %rbx        # 8-byte Reload
	movsd	%xmm1, (%rbx,%r9,8)
	leal	11(%r9), %ebx
	cvtsi2sdl	%ebx, %xmm0
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	leal	12(%r9), %ebx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	leal	13(%r9), %ebx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	leal	14(%r9), %ebx
	cvtsi2sdl	%ebx, %xmm12
	leal	15(%r9), %ebx
	cvtsi2sdl	%ebx, %xmm13
	leal	16(%r9), %ebx
	cvtsi2sdl	%ebx, %xmm14
	leal	17(%r9), %ebx
	cvtsi2sdl	%ebx, %xmm15
	leal	18(%r9), %ebx
	cvtsi2sdl	%ebx, %xmm11
	leal	19(%r9), %ebx
	cvtsi2sdl	%ebx, %xmm3
	leal	20(%r9), %ebx
	cvtsi2sdl	%ebx, %xmm4
	leal	21(%r9), %ebx
	cvtsi2sdl	%ebx, %xmm5
	leal	22(%r9), %ebx
	cvtsi2sdl	%ebx, %xmm6
	leal	23(%r9), %ebx
	cvtsi2sdl	%ebx, %xmm7
	leal	24(%r9), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	leal	25(%r9), %ebx
	cvtsi2sdl	%ebx, %xmm8
	leal	26(%r9), %ebx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movq	-104(%rsp), %rbx        # 8-byte Reload
	movsd	40(%rsp), %xmm10        # 8-byte Reload
                                        # xmm10 = mem[0],zero
	movsd	%xmm10, (%rbx,%r9,8)
	movq	-96(%rsp), %rbx         # 8-byte Reload
	movsd	32(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	%xmm2, (%rbx,%r9,8)
	movq	-88(%rsp), %rbx         # 8-byte Reload
	movsd	24(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	%xmm2, (%rbx,%r9,8)
	movq	-80(%rsp), %rbx         # 8-byte Reload
	movsd	%xmm12, (%rbx,%r9,8)
	movq	-72(%rsp), %rbx         # 8-byte Reload
	movsd	%xmm13, (%rbx,%r9,8)
	movq	-64(%rsp), %rbx         # 8-byte Reload
	movsd	%xmm14, (%rbx,%r9,8)
	movq	-56(%rsp), %rbx         # 8-byte Reload
	movsd	%xmm15, (%rbx,%r9,8)
	movq	-48(%rsp), %rbx         # 8-byte Reload
	movsd	%xmm11, (%rbx,%r9,8)
	movq	-40(%rsp), %rbx         # 8-byte Reload
	movsd	%xmm3, (%rbx,%r9,8)
	movq	-32(%rsp), %rbx         # 8-byte Reload
	movsd	%xmm4, (%rbx,%r9,8)
	movq	-24(%rsp), %rbx         # 8-byte Reload
	movsd	%xmm5, (%rbx,%r9,8)
	movq	-16(%rsp), %rbx         # 8-byte Reload
	movsd	%xmm6, (%rbx,%r9,8)
	movq	-8(%rsp), %rbx          # 8-byte Reload
	movsd	%xmm7, (%rbx,%r9,8)
	movq	(%rsp), %rbx            # 8-byte Reload
	movsd	%xmm1, (%rbx,%r9,8)
	movq	8(%rsp), %rbx           # 8-byte Reload
	movsd	%xmm8, (%rbx,%r9,8)
	movq	16(%rsp), %rbx          # 8-byte Reload
	movsd	%xmm0, (%rbx,%r9,8)
	addsd	.LCPI2_0(%rip), %xmm9
	cmpl	%edi, %r10d
	movq	%r10, %r9
	jl	.LBB2_2
.LBB2_3:                                # %.preheader
	movl	x_size(%rip), %eax
	testl	%eax, %eax
	jle	.LBB2_9
# BB#4:                                 # %.lr.ph.preheader
	leal	-1(%rax), %esi
	movl	%eax, %edi
	xorl	%ecx, %ecx
	andl	$3, %edi
	je	.LBB2_7
# BB#5:                                 # %.lr.ph.prol.preheader
	xorpd	%xmm0, %xmm0
	movsd	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, %xmm2
	addsd	%xmm2, %xmm2
	movsd	%xmm2, (%rdx)
	addq	$8, %rdx
	addsd	%xmm1, %xmm0
	incl	%ecx
	cmpl	%ecx, %edi
	jne	.LBB2_6
.LBB2_7:                                # %.lr.ph.prol.loopexit
	cmpl	$3, %esi
	jb	.LBB2_9
	.p2align	4, 0x90
.LBB2_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, (%rdx)
	leal	1(%rcx), %esi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rdx)
	leal	2(%rcx), %esi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rdx)
	leal	3(%rcx), %esi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 24(%rdx)
	addl	$4, %ecx
	addq	$32, %rdx
	cmpl	%eax, %ecx
	jl	.LBB2_8
.LBB2_9:                                # %._crit_edge
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	init, .Lfunc_end2-init
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"r"
	.size	.L.str, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"      Cannot open input file: %s\n"
	.size	.L.str.2, 34

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%d %d %d %d %d %d %d %d"
	.size	.L.str.3, 24

	.type	kmin,@object            # @kmin
	.comm	kmin,4,4
	.type	kmax,@object            # @kmax
	.comm	kmax,4,4
	.type	jmin,@object            # @jmin
	.comm	jmin,4,4
	.type	jmax,@object            # @jmax
	.comm	jmax,4,4
	.type	imin,@object            # @imin
	.comm	imin,4,4
	.type	imax,@object            # @imax
	.comm	imax,4,4
	.type	kp,@object              # @kp
	.comm	kp,4,4
	.type	jp,@object              # @jp
	.comm	jp,4,4
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"kmin = %8d     kmax = %8d \njmin = %8d     jmax = %8d \nimin = %8d     imax = %8d \nkp   = %8d     jp   = %8d \n \n \n"
	.size	.L.str.5, 113

	.type	i_lb,@object            # @i_lb
	.comm	i_lb,4,4
	.type	i_ub,@object            # @i_ub
	.comm	i_ub,4,4
	.type	x_size,@object          # @x_size
	.comm	x_size,4,4
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"i_lb = %10d    i_ub = %10d    x_size = %10d \n \n \n"
	.size	.L.str.8, 50

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"***** input  "
	.size	.Lstr, 14

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.1:
	.asciz	"***** array bounds  "
	.size	.Lstr.1, 21

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"      lb of xdbl < 0 "
	.size	.Lstr.3, 22

	.type	.Lstr.4,@object         # @str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.4:
	.asciz	"***** ERROR "
	.size	.Lstr.4, 13

	.type	.Lstr.5,@object         # @str.5
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.5:
	.asciz	"*****ERROR: allocMem out of memory "
	.size	.Lstr.5, 36


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
