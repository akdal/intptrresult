	.text
	.file	"Xz.bc"
	.globl	Xz_WriteVarInt
	.p2align	4, 0x90
	.type	Xz_WriteVarInt,@function
Xz_WriteVarInt:                         # @Xz_WriteVarInt
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	%esi, %ecx
	movl	%ecx, %edx
	orb	$-128, %dl
	movb	%dl, (%rdi,%rax)
	incq	%rax
	shrq	$7, %rsi
	jne	.LBB0_1
# BB#2:
	andb	$127, %cl
	movb	%cl, -1(%rdi,%rax)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end0:
	.size	Xz_WriteVarInt, .Lfunc_end0-Xz_WriteVarInt
	.cfi_endproc

	.globl	Xz_Construct
	.p2align	4, 0x90
	.type	Xz_Construct,@function
Xz_Construct:                           # @Xz_Construct
	.cfi_startproc
# BB#0:
	movw	$0, (%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rdi)
	movq	$0, 24(%rdi)
	retq
.Lfunc_end1:
	.size	Xz_Construct, .Lfunc_end1-Xz_Construct
	.cfi_endproc

	.globl	Xz_Free
	.p2align	4, 0x90
	.type	Xz_Free,@function
Xz_Free:                                # @Xz_Free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	24(%rbx), %rsi
	movq	%rax, %rdi
	callq	*8(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movq	$0, 24(%rbx)
	popq	%rbx
	retq
.Lfunc_end2:
	.size	Xz_Free, .Lfunc_end2-Xz_Free
	.cfi_endproc

	.globl	XzFlags_GetCheckSize
	.p2align	4, 0x90
	.type	XzFlags_GetCheckSize,@function
XzFlags_GetCheckSize:                   # @XzFlags_GetCheckSize
	.cfi_startproc
# BB#0:
	andw	$15, %di
	je	.LBB3_1
# BB#2:
	movzwl	%di, %eax
	decl	%eax
	cltq
	imulq	$1431655766, %rax, %rcx # imm = 0x55555556
	movq	%rcx, %rax
	shrq	$63, %rax
	shrq	$32, %rcx
	addl	%eax, %ecx
	movl	$4, %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	retq
.LBB3_1:
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	XzFlags_GetCheckSize, .Lfunc_end3-XzFlags_GetCheckSize
	.cfi_endproc

	.globl	XzCheck_Init
	.p2align	4, 0x90
	.type	XzCheck_Init,@function
XzCheck_Init:                           # @XzCheck_Init
	.cfi_startproc
# BB#0:
	movl	%esi, (%rdi)
	cmpl	$10, %esi
	je	.LBB4_4
# BB#1:
	cmpl	$4, %esi
	je	.LBB4_5
# BB#2:
	cmpl	$1, %esi
	jne	.LBB4_6
# BB#3:
	movl	$-1, 4(%rdi)
	retq
.LBB4_4:
	addq	$16, %rdi
	jmp	Sha256_Init             # TAILCALL
.LBB4_5:
	movq	$-1, 8(%rdi)
.LBB4_6:
	retq
.Lfunc_end4:
	.size	XzCheck_Init, .Lfunc_end4-XzCheck_Init
	.cfi_endproc

	.globl	XzCheck_Update
	.p2align	4, 0x90
	.type	XzCheck_Update,@function
XzCheck_Update:                         # @XzCheck_Update
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	cmpl	$10, %eax
	je	.LBB5_4
# BB#1:
	cmpl	$4, %eax
	je	.LBB5_5
# BB#2:
	cmpl	$1, %eax
	jne	.LBB5_6
# BB#3:
	movl	4(%rbx), %edi
	callq	CrcUpdate
	movl	%eax, 4(%rbx)
	popq	%rbx
	retq
.LBB5_4:
	addq	$16, %rbx
	movq	%rbx, %rdi
	popq	%rbx
	jmp	Sha256_Update           # TAILCALL
.LBB5_5:
	movq	8(%rbx), %rdi
	callq	Crc64Update
	movq	%rax, 8(%rbx)
.LBB5_6:
	popq	%rbx
	retq
.Lfunc_end5:
	.size	XzCheck_Update, .Lfunc_end5-XzCheck_Update
	.cfi_endproc

	.globl	XzCheck_Final
	.p2align	4, 0x90
	.type	XzCheck_Final,@function
XzCheck_Final:                          # @XzCheck_Final
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	cmpl	$10, %ecx
	je	.LBB6_5
# BB#1:
	cmpl	$4, %ecx
	je	.LBB6_4
# BB#2:
	xorl	%eax, %eax
	cmpl	$1, %ecx
	jne	.LBB6_6
# BB#3:
	movl	4(%rdi), %eax
	notl	%eax
	movl	%eax, (%rsi)
	movl	$1, %eax
	retq
.LBB6_5:
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 16
	addq	$16, %rdi
	callq	Sha256_Final
	movl	$1, %eax
	addq	$8, %rsp
.LBB6_6:                                # %.loopexit
	retq
.LBB6_4:                                # %.loopexit.loopexit19
	movq	8(%rdi), %rax
	notq	%rax
	movb	%al, (%rsi)
	movb	%ah, 1(%rsi)  # NOREX
	movq	%rax, %rcx
	shrq	$16, %rcx
	movb	%cl, 2(%rsi)
	movq	%rax, %rcx
	shrq	$24, %rcx
	movb	%cl, 3(%rsi)
	movq	%rax, %rcx
	shrq	$32, %rcx
	movb	%cl, 4(%rsi)
	movq	%rax, %rcx
	shrq	$40, %rcx
	movb	%cl, 5(%rsi)
	movq	%rax, %rcx
	shrq	$48, %rcx
	movb	%cl, 6(%rsi)
	shrq	$56, %rax
	movb	%al, 7(%rsi)
	movl	$1, %eax
	retq
.Lfunc_end6:
	.size	XzCheck_Final, .Lfunc_end6-XzCheck_Final
	.cfi_endproc

	.type	XZ_SIG,@object          # @XZ_SIG
	.data
	.globl	XZ_SIG
XZ_SIG:
	.asciz	"\3757zXZ"
	.size	XZ_SIG, 6

	.type	XZ_FOOTER_SIG,@object   # @XZ_FOOTER_SIG
	.globl	XZ_FOOTER_SIG
XZ_FOOTER_SIG:
	.ascii	"YZ"
	.size	XZ_FOOTER_SIG, 2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
