	.text
	.file	"btRaycastCallback.bc"
	.globl	_ZN25btTriangleRaycastCallbackC2ERK9btVector3S2_j
	.p2align	4, 0x90
	.type	_ZN25btTriangleRaycastCallbackC2ERK9btVector3S2_j,@function
_ZN25btTriangleRaycastCallbackC2ERK9btVector3S2_j: # @_ZN25btTriangleRaycastCallbackC2ERK9btVector3S2_j
	.cfi_startproc
# BB#0:
	movq	$_ZTV25btTriangleRaycastCallback+16, (%rdi)
	movups	(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movups	(%rdx), %xmm0
	movups	%xmm0, 24(%rdi)
	movl	%ecx, 40(%rdi)
	movl	$1065353216, 44(%rdi)   # imm = 0x3F800000
	retq
.Lfunc_end0:
	.size	_ZN25btTriangleRaycastCallbackC2ERK9btVector3S2_j, .Lfunc_end0-_ZN25btTriangleRaycastCallbackC2ERK9btVector3S2_j
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	3100751639              # float -9.99999974E-5
.LCPI1_1:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN25btTriangleRaycastCallback15processTriangleEP9btVector3ii
	.p2align	4, 0x90
	.type	_ZN25btTriangleRaycastCallback15processTriangleEP9btVector3ii,@function
_ZN25btTriangleRaycastCallback15processTriangleEP9btVector3ii: # @_ZN25btTriangleRaycastCallback15processTriangleEP9btVector3ii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$160, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 192
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %ebp
	movq	%rdi, %rbx
	movss	(%rsi), %xmm14          # xmm14 = mem[0],zero,zero,zero
	movss	16(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 16(%rsp)         # 4-byte Spill
	subss	%xmm14, %xmm1
	movsd	20(%rsi), %xmm12        # xmm12 = mem[0],zero
	movsd	4(%rsi), %xmm11         # xmm11 = mem[0],zero
	movaps	%xmm12, %xmm0
	subps	%xmm11, %xmm0
	movss	32(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	36(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	subss	%xmm11, %xmm2
	movss	40(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, 144(%rsp)        # 16-byte Spill
	movaps	%xmm4, 128(%rsp)        # 16-byte Spill
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	pshufd	$229, %xmm11, %xmm13    # xmm13 = xmm11[1,1,2,3]
	movaps	%xmm14, %xmm4
	shufps	$0, %xmm13, %xmm4       # xmm4 = xmm4[0,0],xmm13[0,0]
	shufps	$226, %xmm13, %xmm4     # xmm4 = xmm4[2,0],xmm13[2,3]
	subps	%xmm4, %xmm3
	movaps	%xmm0, %xmm7
	mulps	%xmm3, %xmm7
	movaps	%xmm1, %xmm4
	mulss	%xmm2, %xmm1
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	mulss	%xmm0, %xmm3
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	shufps	$0, %xmm0, %xmm4        # xmm4 = xmm4[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm4      # xmm4 = xmm4[2,0],xmm0[2,3]
	mulps	%xmm4, %xmm2
	subps	%xmm2, %xmm7
	subss	%xmm3, %xmm1
	xorps	%xmm0, %xmm0
	movaps	%xmm1, %xmm2
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movlps	%xmm7, 48(%rsp)
	movlps	%xmm2, 56(%rsp)
	movss	8(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movaps	%xmm2, %xmm4
	unpcklps	%xmm14, %xmm4   # xmm4 = xmm4[0],xmm14[0],xmm4[1],xmm14[1]
	mulps	%xmm3, %xmm4
	movaps	%xmm11, %xmm3
	shufps	$16, %xmm7, %xmm3       # xmm3 = xmm3[0,0],xmm7[1,0]
	shufps	$226, %xmm7, %xmm3      # xmm3 = xmm3[2,0],xmm7[2,3]
	movaps	%xmm7, %xmm5
	shufps	$1, %xmm6, %xmm5        # xmm5 = xmm5[1,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm5      # xmm5 = xmm5[2,0],xmm6[2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm4, %xmm5
	movss	16(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	unpcklps	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1]
	movaps	%xmm11, %xmm10
	shufps	$1, %xmm3, %xmm10       # xmm10 = xmm10[1,0],xmm3[0,0]
	shufps	$226, %xmm3, %xmm10     # xmm10 = xmm10[2,0],xmm3[2,3]
	mulps	%xmm4, %xmm10
	addps	%xmm5, %xmm10
	movaps	%xmm7, %xmm9
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	movss	24(%rbx), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm7, 32(%rsp)         # 16-byte Spill
	movaps	%xmm7, %xmm5
	mulss	%xmm15, %xmm5
	movss	28(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm4
	mulss	%xmm7, %xmm4
	addss	%xmm5, %xmm4
	movss	32(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm8
	mulss	%xmm5, %xmm8
	addss	%xmm4, %xmm8
	movaps	%xmm10, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	subss	%xmm4, %xmm10
	subss	%xmm4, %xmm8
	movaps	%xmm10, %xmm4
	mulss	%xmm8, %xmm4
	ucomiss	%xmm0, %xmm4
	jae	.LBB1_14
# BB#1:
	ucomiss	%xmm0, %xmm10
	jbe	.LBB1_3
# BB#2:
	movl	40(%rbx), %eax
	andl	$1, %eax
	jne	.LBB1_14
.LBB1_3:
	movaps	%xmm10, %xmm0
	subss	%xmm8, %xmm0
	movaps	%xmm10, %xmm4
	divss	%xmm0, %xmm4
	movss	44(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm4, 80(%rsp)         # 16-byte Spill
	ucomiss	%xmm4, %xmm0
	jbe	.LBB1_14
# BB#4:
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	mulss	%xmm0, %xmm0
	movaps	%xmm9, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm0, %xmm4
	movaps	%xmm1, %xmm8
	mulss	%xmm8, %xmm8
	addss	%xmm4, %xmm8
	movss	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	subss	%xmm0, %xmm4
	mulss	%xmm4, %xmm2
	mulss	%xmm0, %xmm15
	addss	%xmm2, %xmm15
	mulss	%xmm4, %xmm6
	mulss	%xmm0, %xmm7
	addss	%xmm6, %xmm7
	mulss	%xmm4, %xmm3
	mulss	%xmm0, %xmm5
	addss	%xmm3, %xmm5
	subss	%xmm15, %xmm14
	subss	%xmm7, %xmm11
	subss	%xmm5, %xmm13
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	%xmm15, %xmm0
	movaps	%xmm12, %xmm3
	subss	%xmm7, %xmm3
	shufps	$229, %xmm12, %xmm12    # xmm12 = xmm12[1,1,2,3]
	subss	%xmm5, %xmm12
	movaps	%xmm11, %xmm2
	mulss	%xmm12, %xmm2
	movaps	%xmm13, %xmm4
	mulss	%xmm3, %xmm4
	subss	%xmm4, %xmm2
	movaps	%xmm13, %xmm4
	mulss	%xmm0, %xmm4
	movaps	%xmm14, %xmm6
	mulss	%xmm12, %xmm6
	subss	%xmm6, %xmm4
	movaps	%xmm14, %xmm6
	mulss	%xmm3, %xmm6
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	movaps	%xmm11, %xmm1
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm6
	mulss	32(%rsp), %xmm2         # 16-byte Folded Reload
	mulss	%xmm9, %xmm4
	addss	%xmm2, %xmm4
	mulss	64(%rsp), %xmm6         # 16-byte Folded Reload
	addss	%xmm4, %xmm6
	movss	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm8, 112(%rsp)        # 16-byte Spill
	mulss	%xmm8, %xmm0
	ucomiss	%xmm0, %xmm6
	jb	.LBB1_14
# BB#5:
	movaps	128(%rsp), %xmm8        # 16-byte Reload
	subss	%xmm15, %xmm8
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm2
	movaps	144(%rsp), %xmm6        # 16-byte Reload
	subss	%xmm5, %xmm6
	movaps	%xmm3, %xmm1
	mulss	%xmm6, %xmm1
	movaps	%xmm12, %xmm4
	mulss	%xmm2, %xmm4
	subss	%xmm4, %xmm1
	mulss	%xmm8, %xmm12
	movss	16(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm4
	movaps	%xmm6, %xmm5
	mulss	%xmm6, %xmm4
	subss	%xmm4, %xmm12
	movaps	%xmm2, %xmm4
	mulss	%xmm2, %xmm7
	movaps	%xmm8, %xmm2
	mulss	%xmm8, %xmm3
	subss	%xmm3, %xmm7
	mulss	32(%rsp), %xmm1         # 16-byte Folded Reload
	mulss	%xmm9, %xmm12
	addss	%xmm1, %xmm12
	mulss	64(%rsp), %xmm7         # 16-byte Folded Reload
	addss	%xmm12, %xmm7
	ucomiss	%xmm0, %xmm7
	jb	.LBB1_14
# BB#6:
	movaps	%xmm13, %xmm1
	mulss	%xmm4, %xmm1
	movaps	%xmm11, %xmm3
	mulss	%xmm5, %xmm3
	subss	%xmm3, %xmm1
	mulss	%xmm14, %xmm5
	mulss	%xmm2, %xmm13
	subss	%xmm13, %xmm5
	mulss	%xmm2, %xmm11
	mulss	%xmm4, %xmm14
	subss	%xmm14, %xmm11
	mulss	32(%rsp), %xmm1         # 16-byte Folded Reload
	mulss	%xmm5, %xmm9
	addss	%xmm1, %xmm9
	mulss	64(%rsp), %xmm11        # 16-byte Folded Reload
	addss	%xmm9, %xmm11
	ucomiss	%xmm0, %xmm11
	jb	.LBB1_14
# BB#7:
	movaps	%xmm10, 16(%rsp)        # 16-byte Spill
	movaps	112(%rsp), %xmm0        # 16-byte Reload
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB1_9
# BB#8:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB1_9:                                # %.split
	movss	.LCPI1_1(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm3
	movaps	%xmm3, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	32(%rsp), %xmm2         # 16-byte Folded Reload
	movaps	%xmm2, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm2, 48(%rsp)
	movss	%xmm1, 52(%rsp)
	mulss	64(%rsp), %xmm3         # 16-byte Folded Reload
	movss	%xmm3, 56(%rsp)
	xorps	%xmm1, %xmm1
	ucomiss	16(%rsp), %xmm1         # 16-byte Folded Reload
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	jae	.LBB1_11
# BB#10:                                # %.split
	movl	40(%rbx), %ecx
	andl	$2, %ecx
	jne	.LBB1_11
# BB#12:
	leaq	48(%rsp), %rsi
	jmp	.LBB1_13
.LBB1_11:
	movaps	.LCPI1_2(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm2
	xorps	%xmm1, %xmm3
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movlps	%xmm2, 96(%rsp)
	movlps	%xmm1, 104(%rsp)
	leaq	96(%rsp), %rsi
.LBB1_13:
	movq	%rbx, %rdi
	movl	%ebp, %edx
	movl	%r14d, %ecx
	callq	*%rax
	movss	%xmm0, 44(%rbx)
.LBB1_14:
	addq	$160, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN25btTriangleRaycastCallback15processTriangleEP9btVector3ii, .Lfunc_end1-_ZN25btTriangleRaycastCallback15processTriangleEP9btVector3ii
	.cfi_endproc

	.globl	_ZN28btTriangleConvexcastCallbackC2EPK13btConvexShapeRK11btTransformS5_S5_f
	.p2align	4, 0x90
	.type	_ZN28btTriangleConvexcastCallbackC2EPK13btConvexShapeRK11btTransformS5_S5_f,@function
_ZN28btTriangleConvexcastCallbackC2EPK13btConvexShapeRK11btTransformS5_S5_f: # @_ZN28btTriangleConvexcastCallbackC2EPK13btConvexShapeRK11btTransformS5_S5_f
	.cfi_startproc
# BB#0:
	movq	$_ZTV28btTriangleConvexcastCallback+16, (%rdi)
	movq	%rsi, 8(%rdi)
	movups	(%rdx), %xmm1
	movups	%xmm1, 16(%rdi)
	movups	16(%rdx), %xmm1
	movups	%xmm1, 32(%rdi)
	movups	32(%rdx), %xmm1
	movups	%xmm1, 48(%rdi)
	movups	48(%rdx), %xmm1
	movups	%xmm1, 64(%rdi)
	movups	(%rcx), %xmm1
	movups	%xmm1, 80(%rdi)
	movups	16(%rcx), %xmm1
	movups	%xmm1, 96(%rdi)
	movups	32(%rcx), %xmm1
	movups	%xmm1, 112(%rdi)
	movups	48(%rcx), %xmm1
	movups	%xmm1, 128(%rdi)
	movups	(%r8), %xmm1
	movups	%xmm1, 144(%rdi)
	movups	16(%r8), %xmm1
	movups	%xmm1, 160(%rdi)
	movups	32(%r8), %xmm1
	movups	%xmm1, 176(%rdi)
	movups	48(%r8), %xmm1
	movups	%xmm1, 192(%rdi)
	movl	$1065353216, 208(%rdi)  # imm = 0x3F800000
	movss	%xmm0, 212(%rdi)
	retq
.Lfunc_end2:
	.size	_ZN28btTriangleConvexcastCallbackC2EPK13btConvexShapeRK11btTransformS5_S5_f, .Lfunc_end2-_ZN28btTriangleConvexcastCallbackC2EPK13btConvexShapeRK11btTransformS5_S5_f
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	953267991               # float 9.99999974E-5
.LCPI4_1:
	.long	1065353216              # float 1
	.text
	.globl	_ZN28btTriangleConvexcastCallback15processTriangleEP9btVector3ii
	.p2align	4, 0x90
	.type	_ZN28btTriangleConvexcastCallback15processTriangleEP9btVector3ii,@function
_ZN28btTriangleConvexcastCallback15processTriangleEP9btVector3ii: # @_ZN28btTriangleConvexcastCallback15processTriangleEP9btVector3ii
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 48
	subq	$720, %rsp              # imm = 0x2D0
.Lcfi12:
	.cfi_def_cfa_offset 768
.Lcfi13:
	.cfi_offset %rbx, -48
.Lcfi14:
	.cfi_offset %r12, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %r12d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	leaq	248(%rsp), %r15
	movq	%r15, %rdi
	callq	_ZN23btPolyhedralConvexShapeC2Ev
	movq	$_ZTV15btTriangleShape+16, 248(%rsp)
	movl	$1, 256(%rsp)
	movups	(%rbp), %xmm0
	movups	%xmm0, 312(%rsp)
	movups	16(%rbp), %xmm0
	movups	%xmm0, 328(%rsp)
	movups	32(%rbp), %xmm0
	movups	%xmm0, 344(%rsp)
	movl	212(%rbx), %eax
	movl	%eax, 304(%rsp)
	movb	$0, 688(%rsp)
	movq	$_ZTV30btGjkEpaPenetrationDepthSolver+16, 8(%rsp)
	movq	8(%rbx), %rsi
.Ltmp0:
	leaq	16(%rsp), %rdi
	leaq	360(%rsp), %rcx
	leaq	8(%rsp), %r8
	movq	%r15, %rdx
	callq	_ZN27btContinuousConvexCollisionC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
.Ltmp1:
# BB#1:
	movq	$_ZTVN12btConvexCast10CastResultE+16, 56(%rsp)
	movq	$0, 232(%rsp)
	movl	$0, 240(%rsp)
	movl	$1065353216, 224(%rsp)  # imm = 0x3F800000
	leaq	16(%rbx), %rsi
	leaq	80(%rbx), %rdx
	leaq	144(%rbx), %rcx
.Ltmp3:
	leaq	16(%rsp), %rdi
	leaq	56(%rsp), %r9
	movq	%rcx, %r8
	callq	_ZN27btContinuousConvexCollision16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
.Ltmp4:
# BB#2:
	testb	%al, %al
	je	.LBB4_8
# BB#3:
	movss	192(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	196(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	200(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	.LCPI4_0(%rip), %xmm0
	jbe	.LBB4_8
# BB#4:
	movss	208(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	224(%rsp), %xmm1
	jbe	.LBB4_8
# BB#5:
	leaq	192(%rsp), %r15
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB4_7
# BB#6:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB4_7:                                # %.split
	movss	.LCPI4_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	192(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 192(%rsp)
	movss	196(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 196(%rsp)
	mulss	200(%rsp), %xmm0
	movss	%xmm0, 200(%rsp)
	movq	(%rbx), %rax
	leaq	208(%rsp), %rdx
	movss	224(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
.Ltmp5:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movl	%r12d, %ecx
	movl	%r14d, %r8d
	callq	*24(%rax)
.Ltmp6:
.LBB4_8:
.Ltmp10:
	leaq	16(%rsp), %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp11:
# BB#9:
	leaq	248(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
	addq	$720, %rsp              # imm = 0x2D0
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_10:
.Ltmp12:
	movq	%rax, %rbx
	jmp	.LBB4_12
.LBB4_15:
.Ltmp2:
	movq	%rax, %rbx
	jmp	.LBB4_12
.LBB4_11:
.Ltmp7:
	movq	%rax, %rbx
.Ltmp8:
	leaq	16(%rsp), %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp9:
.LBB4_12:
.Ltmp13:
	leaq	248(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp14:
# BB#13:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_14:
.Ltmp15:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN28btTriangleConvexcastCallback15processTriangleEP9btVector3ii, .Lfunc_end4-_ZN28btTriangleConvexcastCallback15processTriangleEP9btVector3ii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp8-.Ltmp11          #   Call between .Ltmp11 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp14-.Ltmp8          #   Call between .Ltmp8 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	1                       #   On action: 1
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Lfunc_end4-.Ltmp14     #   Call between .Ltmp14 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN21btConvexInternalShape9setMarginEf,"axG",@progbits,_ZN21btConvexInternalShape9setMarginEf,comdat
	.weak	_ZN21btConvexInternalShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShape9setMarginEf,@function
_ZN21btConvexInternalShape9setMarginEf: # @_ZN21btConvexInternalShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 56(%rdi)
	retq
.Lfunc_end5:
	.size	_ZN21btConvexInternalShape9setMarginEf, .Lfunc_end5-_ZN21btConvexInternalShape9setMarginEf
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResultD2Ev,"axG",@progbits,_ZN12btConvexCast10CastResultD2Ev,comdat
	.weak	_ZN12btConvexCast10CastResultD2Ev
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResultD2Ev,@function
_ZN12btConvexCast10CastResultD2Ev:      # @_ZN12btConvexCast10CastResultD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end6:
	.size	_ZN12btConvexCast10CastResultD2Ev, .Lfunc_end6-_ZN12btConvexCast10CastResultD2Ev
	.cfi_endproc

	.section	.text._ZN25btTriangleRaycastCallbackD0Ev,"axG",@progbits,_ZN25btTriangleRaycastCallbackD0Ev,comdat
	.weak	_ZN25btTriangleRaycastCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN25btTriangleRaycastCallbackD0Ev,@function
_ZN25btTriangleRaycastCallbackD0Ev:     # @_ZN25btTriangleRaycastCallbackD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp16:
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp17:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB7_2:
.Ltmp18:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN25btTriangleRaycastCallbackD0Ev, .Lfunc_end7-_ZN25btTriangleRaycastCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end7-.Ltmp17     #   Call between .Ltmp17 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN28btTriangleConvexcastCallbackD0Ev,"axG",@progbits,_ZN28btTriangleConvexcastCallbackD0Ev,comdat
	.weak	_ZN28btTriangleConvexcastCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN28btTriangleConvexcastCallbackD0Ev,@function
_ZN28btTriangleConvexcastCallbackD0Ev:  # @_ZN28btTriangleConvexcastCallbackD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp19:
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp20:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB8_2:
.Ltmp21:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN28btTriangleConvexcastCallbackD0Ev, .Lfunc_end8-_ZN28btTriangleConvexcastCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin2   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end8-.Ltmp20     #   Call between .Ltmp20 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN15btTriangleShapeD0Ev,"axG",@progbits,_ZN15btTriangleShapeD0Ev,comdat
	.weak	_ZN15btTriangleShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN15btTriangleShapeD0Ev,@function
_ZN15btTriangleShapeD0Ev:               # @_ZN15btTriangleShapeD0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp22:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp23:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB9_2:
.Ltmp24:
	movq	%rax, %r14
.Ltmp25:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp26:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB9_4:
.Ltmp27:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN15btTriangleShapeD0Ev, .Lfunc_end9-_ZN15btTriangleShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp22-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin3   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp25-.Ltmp23         #   Call between .Ltmp23 and .Ltmp25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin3   #     jumps to .Ltmp27
	.byte	1                       #   On action: 1
	.long	.Ltmp26-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end9-.Ltmp26     #   Call between .Ltmp26 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_,"axG",@progbits,_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_,comdat
	.weak	_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	120(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end10:
	.size	_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end10-_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end11:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end11-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3,"axG",@progbits,_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3,comdat
	.weak	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3: # @_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rsi)
	retq
.Lfunc_end12:
	.size	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end12-_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape7getNameEv,"axG",@progbits,_ZNK15btTriangleShape7getNameEv,comdat
	.weak	_ZNK15btTriangleShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape7getNameEv,@function
_ZNK15btTriangleShape7getNameEv:        # @_ZNK15btTriangleShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end13:
	.size	_ZNK15btTriangleShape7getNameEv, .Lfunc_end13-_ZNK15btTriangleShape7getNameEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape9getMarginEv,"axG",@progbits,_ZNK21btConvexInternalShape9getMarginEv,comdat
	.weak	_ZNK21btConvexInternalShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape9getMarginEv,@function
_ZNK21btConvexInternalShape9getMarginEv: # @_ZNK21btConvexInternalShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end14:
	.size	_ZNK21btConvexInternalShape9getMarginEv, .Lfunc_end14-_ZNK21btConvexInternalShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3,"axG",@progbits,_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3,comdat
	.weak	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	80(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	64(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	68(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	84(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm3, %xmm6
	addps	%xmm5, %xmm6
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	88(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	addps	%xmm6, %xmm5
	mulss	96(%rdi), %xmm2
	mulss	100(%rdi), %xmm1
	addss	%xmm2, %xmm1
	mulss	104(%rdi), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm5, -16(%rsp)
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm1, -12(%rsp)
	movss	%xmm0, -8(%rsp)
	movl	$0, -4(%rsp)
	xorl	%eax, %eax
	ucomiss	%xmm5, %xmm1
	seta	%al
	ucomiss	-16(%rsp,%rax,4), %xmm0
	movl	$2, %ecx
	cmovbeq	%rax, %rcx
	shlq	$4, %rcx
	movsd	64(%rdi,%rcx), %xmm0    # xmm0 = mem[0],zero
	movsd	72(%rdi,%rcx), %xmm1    # xmm1 = mem[0],zero
	retq
.Lfunc_end15:
	.size	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end15-_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,"axG",@progbits,_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,comdat
	.weak	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	jle	.LBB16_3
# BB#1:                                 # %.lr.ph
	movl	%ecx, %eax
	addq	$8, %rsi
	movl	$2, %r8d
	.p2align	4, 0x90
.LBB16_2:                               # =>This Inner Loop Header: Depth=1
	movss	-8(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	-4(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	80(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	64(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	68(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	84(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm3, %xmm6
	addps	%xmm5, %xmm6
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	88(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	addps	%xmm6, %xmm5
	mulss	96(%rdi), %xmm2
	mulss	100(%rdi), %xmm1
	addss	%xmm2, %xmm1
	mulss	104(%rdi), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm5, -16(%rsp)
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm1, -12(%rsp)
	movss	%xmm0, -8(%rsp)
	movl	$0, -4(%rsp)
	xorl	%ecx, %ecx
	ucomiss	%xmm5, %xmm1
	seta	%cl
	ucomiss	-16(%rsp,%rcx,4), %xmm0
	cmovaq	%r8, %rcx
	shlq	$4, %rcx
	movups	64(%rdi,%rcx), %xmm0
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	addq	$16, %rsi
	decq	%rax
	jne	.LBB16_2
.LBB16_3:                               # %._crit_edge
	retq
.Lfunc_end16:
	.size	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end16-_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	movl	$2, %eax
	retq
.Lfunc_end17:
	.size	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end17-_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI18_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 32
.Lcfi36:
	.cfi_offset %rbx, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %ebp
	movss	64(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	80(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	movsd	84(%rdi), %xmm1         # xmm1 = mem[0],zero
	movsd	68(%rdi), %xmm4         # xmm4 = mem[0],zero
	subps	%xmm4, %xmm1
	movss	100(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm2
	movss	96(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	104(%rdi), %xmm6        # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	pshufd	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	shufps	$0, %xmm4, %xmm3        # xmm3 = xmm3[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	subps	%xmm3, %xmm6
	movaps	%xmm1, %xmm3
	mulps	%xmm6, %xmm3
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm0
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm1, %xmm6
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	shufps	$0, %xmm1, %xmm4        # xmm4 = xmm4[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm4      # xmm4 = xmm4[2,0],xmm1[2,3]
	mulps	%xmm4, %xmm2
	subps	%xmm2, %xmm3
	subss	%xmm6, %xmm0
	movaps	%xmm3, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm3, (%rbx)
	movlps	%xmm2, 8(%rbx)
	mulss	%xmm3, %xmm3
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB18_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB18_2:                               # %.split
	movss	.LCPI18_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	%xmm2, 4(%rbx)
	mulss	8(%rbx), %xmm0
	movss	%xmm0, 8(%rbx)
	testl	%ebp, %ebp
	je	.LBB18_4
# BB#3:
	movaps	.LCPI18_1(%rip), %xmm3  # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm3, %xmm1
	movss	%xmm1, (%rbx)
	xorps	%xmm3, %xmm2
	movss	%xmm2, 4(%rbx)
	xorps	%xmm3, %xmm0
	movss	%xmm0, 8(%rbx)
.LBB18_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end18:
	.size	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end18-_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape14getNumVerticesEv,"axG",@progbits,_ZNK15btTriangleShape14getNumVerticesEv,comdat
	.weak	_ZNK15btTriangleShape14getNumVerticesEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape14getNumVerticesEv,@function
_ZNK15btTriangleShape14getNumVerticesEv: # @_ZNK15btTriangleShape14getNumVerticesEv
	.cfi_startproc
# BB#0:
	movl	$3, %eax
	retq
.Lfunc_end19:
	.size	_ZNK15btTriangleShape14getNumVerticesEv, .Lfunc_end19-_ZNK15btTriangleShape14getNumVerticesEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape11getNumEdgesEv,"axG",@progbits,_ZNK15btTriangleShape11getNumEdgesEv,comdat
	.weak	_ZNK15btTriangleShape11getNumEdgesEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape11getNumEdgesEv,@function
_ZNK15btTriangleShape11getNumEdgesEv:   # @_ZNK15btTriangleShape11getNumEdgesEv
	.cfi_startproc
# BB#0:
	movl	$3, %eax
	retq
.Lfunc_end20:
	.size	_ZNK15btTriangleShape11getNumEdgesEv, .Lfunc_end20-_ZNK15btTriangleShape11getNumEdgesEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape7getEdgeEiR9btVector3S1_,"axG",@progbits,_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_,comdat
	.weak	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_,@function
_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_: # @_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 32
.Lcfi41:
	.cfi_offset %rbx, -32
.Lcfi42:
	.cfi_offset %r14, -24
.Lcfi43:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*168(%rax)
	movq	(%rbx), %rax
	movq	168(%rax), %rax
	leal	1(%r15), %ecx
	movslq	%ecx, %rcx
	imulq	$1431655766, %rcx, %rcx # imm = 0x55555556
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$32, %rcx
	addl	%edx, %ecx
	leal	(%rcx,%rcx,2), %ecx
	negl	%ecx
	leal	1(%r15,%rcx), %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.Lfunc_end21:
	.size	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_, .Lfunc_end21-_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape9getVertexEiR9btVector3,"axG",@progbits,_ZNK15btTriangleShape9getVertexEiR9btVector3,comdat
	.weak	_ZNK15btTriangleShape9getVertexEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape9getVertexEiR9btVector3,@function
_ZNK15btTriangleShape9getVertexEiR9btVector3: # @_ZNK15btTriangleShape9getVertexEiR9btVector3
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	shlq	$4, %rax
	movups	64(%rdi,%rax), %xmm0
	movups	%xmm0, (%rdx)
	retq
.Lfunc_end22:
	.size	_ZNK15btTriangleShape9getVertexEiR9btVector3, .Lfunc_end22-_ZNK15btTriangleShape9getVertexEiR9btVector3
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape12getNumPlanesEv,"axG",@progbits,_ZNK15btTriangleShape12getNumPlanesEv,comdat
	.weak	_ZNK15btTriangleShape12getNumPlanesEv
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape12getNumPlanesEv,@function
_ZNK15btTriangleShape12getNumPlanesEv:  # @_ZNK15btTriangleShape12getNumPlanesEv
	.cfi_startproc
# BB#0:
	movl	$1, %eax
	retq
.Lfunc_end23:
	.size	_ZNK15btTriangleShape12getNumPlanesEv, .Lfunc_end23-_ZNK15btTriangleShape12getNumPlanesEv
	.cfi_endproc

	.section	.text._ZNK15btTriangleShape8getPlaneER9btVector3S1_i,"axG",@progbits,_ZNK15btTriangleShape8getPlaneER9btVector3S1_i,comdat
	.weak	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i,@function
_ZNK15btTriangleShape8getPlaneER9btVector3S1_i: # @_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.cfi_startproc
# BB#0:
	movq	%rdx, %r8
	movq	%rsi, %rdx
	movq	(%rdi), %rsi
	movq	200(%rsi), %rax
	movl	%ecx, %esi
	movq	%r8, %rcx
	jmpq	*%rax                   # TAILCALL
.Lfunc_end24:
	.size	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i, .Lfunc_end24-_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI25_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI25_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZNK15btTriangleShape8isInsideERK9btVector3f,"axG",@progbits,_ZNK15btTriangleShape8isInsideERK9btVector3f,comdat
	.weak	_ZNK15btTriangleShape8isInsideERK9btVector3f
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape8isInsideERK9btVector3f,@function
_ZNK15btTriangleShape8isInsideERK9btVector3f: # @_ZNK15btTriangleShape8isInsideERK9btVector3f
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 48
	subq	$144, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 192
.Lcfi50:
	.cfi_offset %rbx, -48
.Lcfi51:
	.cfi_offset %r12, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movaps	%xmm0, %xmm8
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movss	80(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm0
	movss	84(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm3
	movss	88(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm6
	movss	96(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm5
	movss	100(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm7
	movss	104(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movaps	%xmm6, %xmm2
	mulss	%xmm5, %xmm6
	mulss	%xmm3, %xmm5
	mulss	%xmm1, %xmm3
	mulss	%xmm7, %xmm2
	subss	%xmm2, %xmm3
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm6
	mulss	%xmm0, %xmm7
	subss	%xmm5, %xmm7
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm7, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB25_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	movaps	%xmm8, 32(%rsp)         # 16-byte Spill
	movss	%xmm7, 16(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	16(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm8         # 16-byte Reload
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	movaps	64(%rsp), %xmm6         # 16-byte Reload
.LBB25_2:                               # %.split
	movss	.LCPI25_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm5
	divss	%xmm0, %xmm5
	mulss	%xmm5, %xmm3
	mulss	%xmm5, %xmm6
	mulss	%xmm7, %xmm5
	movss	64(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%r14), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	movss	4(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movaps	%xmm4, 48(%rsp)         # 16-byte Spill
	movaps	%xmm4, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	movaps	%xmm6, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm3, %xmm1
	addps	%xmm0, %xmm1
	movaps	%xmm5, 32(%rsp)         # 16-byte Spill
	movaps	%xmm5, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	72(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1]
	mulps	%xmm2, %xmm0
	addps	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	subss	%xmm1, %xmm0
	movaps	.LCPI25_1(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm8, %xmm1
	xorl	%eax, %eax
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	ucomiss	%xmm1, %xmm0
	jb	.LBB25_11
# BB#3:                                 # %.split
	ucomiss	%xmm0, %xmm8
	jb	.LBB25_11
# BB#4:                                 # %.preheader
	xorl	%ebp, %ebp
	movq	%rsp, %r15
	leaq	80(%rsp), %r12
	.p2align	4, 0x90
.LBB25_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	*160(%rax)
	movss	80(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	84(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	(%rsp), %xmm5
	subss	4(%rsp), %xmm0
	movss	88(%rsp), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	8(%rsp), %xmm6
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	movaps	%xmm4, %xmm7
	mulss	%xmm0, %xmm7
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm2, %xmm1
	mulss	%xmm6, %xmm1
	subss	%xmm1, %xmm7
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	mulss	%xmm3, %xmm6
	movaps	%xmm4, %xmm1
	mulss	%xmm5, %xmm1
	subss	%xmm1, %xmm6
	mulss	%xmm2, %xmm5
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm5
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB25_7
# BB#6:                                 # %call.sqrt86
                                        #   in Loop: Header=BB25_5 Depth=1
	movaps	%xmm5, 128(%rsp)        # 16-byte Spill
	movaps	%xmm6, 112(%rsp)        # 16-byte Spill
	movaps	%xmm7, 96(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	96(%rsp), %xmm7         # 16-byte Reload
	movaps	112(%rsp), %xmm6        # 16-byte Reload
	movaps	128(%rsp), %xmm5        # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB25_7:                               # %.split85
                                        #   in Loop: Header=BB25_5 Depth=1
	movss	.LCPI25_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm7
	mulss	%xmm0, %xmm6
	mulss	%xmm0, %xmm5
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	(%r14), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm2, %xmm7
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm3, %xmm6
	addps	%xmm7, %xmm6
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	mulps	%xmm5, %xmm1
	addps	%xmm6, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm0, %xmm1
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	ucomiss	%xmm1, %xmm0
	ja	.LBB25_10
# BB#8:                                 #   in Loop: Header=BB25_5 Depth=1
	incl	%ebp
	cmpl	$2, %ebp
	jle	.LBB25_5
# BB#9:
	movb	$1, %al
	jmp	.LBB25_11
.LBB25_10:
	xorl	%eax, %eax
.LBB25_11:                              # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	_ZNK15btTriangleShape8isInsideERK9btVector3f, .Lfunc_end25-_ZNK15btTriangleShape8isInsideERK9btVector3f
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI26_0:
	.long	1065353216              # float 1
	.section	.text._ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_,"axG",@progbits,_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_,comdat
	.weak	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_,@function
_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_: # @_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 32
.Lcfi58:
	.cfi_offset %rbx, -32
.Lcfi59:
	.cfi_offset %r14, -24
.Lcfi60:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movss	64(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	80(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	movsd	84(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	68(%rbx), %xmm4         # xmm4 = mem[0],zero
	subps	%xmm4, %xmm1
	movss	100(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm2
	movss	96(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	104(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	pshufd	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	shufps	$0, %xmm4, %xmm3        # xmm3 = xmm3[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm3      # xmm3 = xmm3[2,0],xmm4[2,3]
	subps	%xmm3, %xmm6
	movaps	%xmm1, %xmm3
	mulps	%xmm6, %xmm3
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm0
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm1, %xmm6
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	shufps	$0, %xmm1, %xmm4        # xmm4 = xmm4[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm4      # xmm4 = xmm4[2,0],xmm1[2,3]
	mulps	%xmm4, %xmm2
	subps	%xmm2, %xmm3
	subss	%xmm6, %xmm0
	movaps	%xmm3, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm3, (%r15)
	movlps	%xmm2, 8(%r15)
	mulss	%xmm3, %xmm3
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB26_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB26_2:                               # %.split
	movss	.LCPI26_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%r15)
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 4(%r15)
	mulss	8(%r15), %xmm0
	movss	%xmm0, 8(%r15)
	movups	64(%rbx), %xmm0
	movups	%xmm0, (%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end26:
	.size	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_, .Lfunc_end26-_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResult9DebugDrawEf,"axG",@progbits,_ZN12btConvexCast10CastResult9DebugDrawEf,comdat
	.weak	_ZN12btConvexCast10CastResult9DebugDrawEf
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResult9DebugDrawEf,@function
_ZN12btConvexCast10CastResult9DebugDrawEf: # @_ZN12btConvexCast10CastResult9DebugDrawEf
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end27:
	.size	_ZN12btConvexCast10CastResult9DebugDrawEf, .Lfunc_end27-_ZN12btConvexCast10CastResult9DebugDrawEf
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,"axG",@progbits,_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,comdat
	.weak	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,@function
_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform: # @_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end28:
	.size	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform, .Lfunc_end28-_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResultD0Ev,"axG",@progbits,_ZN12btConvexCast10CastResultD0Ev,comdat
	.weak	_ZN12btConvexCast10CastResultD0Ev
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResultD0Ev,@function
_ZN12btConvexCast10CastResultD0Ev:      # @_ZN12btConvexCast10CastResultD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end29:
	.size	_ZN12btConvexCast10CastResultD0Ev, .Lfunc_end29-_ZN12btConvexCast10CastResultD0Ev
	.cfi_endproc

	.type	_ZTV25btTriangleRaycastCallback,@object # @_ZTV25btTriangleRaycastCallback
	.section	.rodata,"a",@progbits
	.globl	_ZTV25btTriangleRaycastCallback
	.p2align	3
_ZTV25btTriangleRaycastCallback:
	.quad	0
	.quad	_ZTI25btTriangleRaycastCallback
	.quad	_ZN18btTriangleCallbackD2Ev
	.quad	_ZN25btTriangleRaycastCallbackD0Ev
	.quad	_ZN25btTriangleRaycastCallback15processTriangleEP9btVector3ii
	.quad	__cxa_pure_virtual
	.size	_ZTV25btTriangleRaycastCallback, 48

	.type	_ZTV28btTriangleConvexcastCallback,@object # @_ZTV28btTriangleConvexcastCallback
	.globl	_ZTV28btTriangleConvexcastCallback
	.p2align	3
_ZTV28btTriangleConvexcastCallback:
	.quad	0
	.quad	_ZTI28btTriangleConvexcastCallback
	.quad	_ZN18btTriangleCallbackD2Ev
	.quad	_ZN28btTriangleConvexcastCallbackD0Ev
	.quad	_ZN28btTriangleConvexcastCallback15processTriangleEP9btVector3ii
	.quad	__cxa_pure_virtual
	.size	_ZTV28btTriangleConvexcastCallback, 48

	.type	_ZTS25btTriangleRaycastCallback,@object # @_ZTS25btTriangleRaycastCallback
	.globl	_ZTS25btTriangleRaycastCallback
	.p2align	4
_ZTS25btTriangleRaycastCallback:
	.asciz	"25btTriangleRaycastCallback"
	.size	_ZTS25btTriangleRaycastCallback, 28

	.type	_ZTI25btTriangleRaycastCallback,@object # @_ZTI25btTriangleRaycastCallback
	.globl	_ZTI25btTriangleRaycastCallback
	.p2align	4
_ZTI25btTriangleRaycastCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS25btTriangleRaycastCallback
	.quad	_ZTI18btTriangleCallback
	.size	_ZTI25btTriangleRaycastCallback, 24

	.type	_ZTS28btTriangleConvexcastCallback,@object # @_ZTS28btTriangleConvexcastCallback
	.globl	_ZTS28btTriangleConvexcastCallback
	.p2align	4
_ZTS28btTriangleConvexcastCallback:
	.asciz	"28btTriangleConvexcastCallback"
	.size	_ZTS28btTriangleConvexcastCallback, 31

	.type	_ZTI28btTriangleConvexcastCallback,@object # @_ZTI28btTriangleConvexcastCallback
	.globl	_ZTI28btTriangleConvexcastCallback
	.p2align	4
_ZTI28btTriangleConvexcastCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS28btTriangleConvexcastCallback
	.quad	_ZTI18btTriangleCallback
	.size	_ZTI28btTriangleConvexcastCallback, 24

	.type	_ZTV15btTriangleShape,@object # @_ZTV15btTriangleShape
	.section	.rodata._ZTV15btTriangleShape,"aG",@progbits,_ZTV15btTriangleShape,comdat
	.weak	_ZTV15btTriangleShape
	.p2align	3
_ZTV15btTriangleShape:
	.quad	0
	.quad	_ZTI15btTriangleShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN15btTriangleShapeD0Ev
	.quad	_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK15btTriangleShape7getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3
	.quad	_ZNK15btTriangleShape14getNumVerticesEv
	.quad	_ZNK15btTriangleShape11getNumEdgesEv
	.quad	_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_
	.quad	_ZNK15btTriangleShape9getVertexEiR9btVector3
	.quad	_ZNK15btTriangleShape12getNumPlanesEv
	.quad	_ZNK15btTriangleShape8getPlaneER9btVector3S1_i
	.quad	_ZNK15btTriangleShape8isInsideERK9btVector3f
	.quad	_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_
	.size	_ZTV15btTriangleShape, 224

	.type	_ZTS15btTriangleShape,@object # @_ZTS15btTriangleShape
	.section	.rodata._ZTS15btTriangleShape,"aG",@progbits,_ZTS15btTriangleShape,comdat
	.weak	_ZTS15btTriangleShape
	.p2align	4
_ZTS15btTriangleShape:
	.asciz	"15btTriangleShape"
	.size	_ZTS15btTriangleShape, 18

	.type	_ZTI15btTriangleShape,@object # @_ZTI15btTriangleShape
	.section	.rodata._ZTI15btTriangleShape,"aG",@progbits,_ZTI15btTriangleShape,comdat
	.weak	_ZTI15btTriangleShape
	.p2align	4
_ZTI15btTriangleShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15btTriangleShape
	.quad	_ZTI23btPolyhedralConvexShape
	.size	_ZTI15btTriangleShape, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Triangle"
	.size	.L.str, 9

	.type	_ZTVN12btConvexCast10CastResultE,@object # @_ZTVN12btConvexCast10CastResultE
	.section	.rodata._ZTVN12btConvexCast10CastResultE,"aG",@progbits,_ZTVN12btConvexCast10CastResultE,comdat
	.weak	_ZTVN12btConvexCast10CastResultE
	.p2align	3
_ZTVN12btConvexCast10CastResultE:
	.quad	0
	.quad	_ZTIN12btConvexCast10CastResultE
	.quad	_ZN12btConvexCast10CastResult9DebugDrawEf
	.quad	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.quad	_ZN12btConvexCast10CastResultD2Ev
	.quad	_ZN12btConvexCast10CastResultD0Ev
	.size	_ZTVN12btConvexCast10CastResultE, 48

	.type	_ZTSN12btConvexCast10CastResultE,@object # @_ZTSN12btConvexCast10CastResultE
	.section	.rodata._ZTSN12btConvexCast10CastResultE,"aG",@progbits,_ZTSN12btConvexCast10CastResultE,comdat
	.weak	_ZTSN12btConvexCast10CastResultE
	.p2align	4
_ZTSN12btConvexCast10CastResultE:
	.asciz	"N12btConvexCast10CastResultE"
	.size	_ZTSN12btConvexCast10CastResultE, 29

	.type	_ZTIN12btConvexCast10CastResultE,@object # @_ZTIN12btConvexCast10CastResultE
	.section	.rodata._ZTIN12btConvexCast10CastResultE,"aG",@progbits,_ZTIN12btConvexCast10CastResultE,comdat
	.weak	_ZTIN12btConvexCast10CastResultE
	.p2align	3
_ZTIN12btConvexCast10CastResultE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN12btConvexCast10CastResultE
	.size	_ZTIN12btConvexCast10CastResultE, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
