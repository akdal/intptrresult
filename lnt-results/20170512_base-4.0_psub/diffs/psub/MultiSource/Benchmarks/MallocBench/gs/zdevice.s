	.text
	.file	"zdevice.bc"
	.globl	zcopypage
	.p2align	4, 0x90
	.type	zcopypage,@function
zcopypage:                              # @zcopypage
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_copypage             # TAILCALL
.Lfunc_end0:
	.size	zcopypage, .Lfunc_end0-zcopypage
	.cfi_endproc

	.globl	zcopyscanlines
	.p2align	4, 0x90
	.type	zcopyscanlines,@function
zcopyscanlines:                         # @zcopyscanlines
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	-24(%rbx), %eax
	andl	$252, %eax
	cmpl	$60, %eax
	jne	.LBB1_9
# BB#1:
	movzwl	-8(%rbx), %eax
	andl	$252, %eax
	cmpl	$20, %eax
	jne	.LBB1_9
# BB#2:
	movq	-16(%rbx), %rsi
	movl	$-15, %eax
	testq	%rsi, %rsi
	js	.LBB1_10
# BB#3:
	movq	-32(%rbx), %rdi
	movslq	28(%rdi), %rcx
	cmpq	%rcx, %rsi
	jg	.LBB1_10
# BB#4:
	movzwl	8(%rbx), %ecx
	movl	%ecx, %eax
	andl	$252, %eax
	cmpl	$52, %eax
	jne	.LBB1_9
# BB#5:
	movl	$-7, %eax
	testb	$1, %ch
	je	.LBB1_10
# BB#6:
	movq	(%rbx), %rdx
	movzwl	10(%rbx), %ecx
	leaq	12(%rsp), %r9
	xorl	%r8d, %r8d
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	gs_copyscanlines
	testl	%eax, %eax
	movl	$-20, %eax
	js	.LBB1_10
# BB#7:
	leaq	-32(%rbx), %rax
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rax)
	movzwl	12(%rsp), %eax
	movw	%ax, -22(%rbx)
	orb	$-128, -23(%rbx)
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
	jmp	.LBB1_10
.LBB1_9:
	movl	$-20, %eax
.LBB1_10:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end1:
	.size	zcopyscanlines, .Lfunc_end1-zcopyscanlines
	.cfi_endproc

	.globl	zcurrentdevice
	.p2align	4, 0x90
	.type	zcurrentdevice,@function
zcurrentdevice:                         # @zcurrentdevice
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 16
.Lcfi4:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	igs(%rip), %rdi
	callq	gs_currentdevice
	leaq	16(%rbx), %rcx
	movq	%rcx, osp(%rip)
	cmpq	ostop(%rip), %rcx
	jbe	.LBB2_2
# BB#1:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	popq	%rbx
	retq
.LBB2_2:
	movq	%rax, 16(%rbx)
	movw	$60, 24(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	zcurrentdevice, .Lfunc_end2-zcurrentdevice
	.cfi_endproc

	.globl	zdevicename
	.p2align	4, 0x90
	.type	zdevicename,@function
zdevicename:                            # @zdevicename
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$60, %ecx
	jne	.LBB3_2
# BB#1:
	movq	(%rbx), %rdi
	callq	gs_devicename
	movq	%rax, %rcx
	movl	$.L.str, %edx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbx, %rsi
	callq	string_to_ref
	movl	%eax, %ecx
	sarl	$31, %eax
	andl	%ecx, %eax
.LBB3_2:
	popq	%rbx
	retq
.Lfunc_end3:
	.size	zdevicename, .Lfunc_end3-zdevicename
	.cfi_endproc

	.globl	zdeviceparams
	.p2align	4, 0x90
	.type	zdeviceparams,@function
zdeviceparams:                          # @zdeviceparams
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
	subq	$112, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 128
.Lcfi9:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%eax, %eax
	callq	write_matrix
	testl	%eax, %eax
	js	.LBB4_5
# BB#1:
	movzwl	-8(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$60, %ecx
	jne	.LBB4_5
# BB#2:
	movq	-16(%rbx), %rdi
	leaq	16(%rsp), %rsi
	leaq	12(%rsp), %rdx
	leaq	8(%rsp), %rcx
	callq	gs_deviceparams
	movq	(%rbx), %rax
	movl	16(%rsp), %ecx
	movl	%ecx, (%rax)
	movw	$44, 8(%rax)
	movl	32(%rsp), %ecx
	movl	%ecx, 16(%rax)
	movw	$44, 24(%rax)
	movl	48(%rsp), %ecx
	movl	%ecx, 32(%rax)
	movw	$44, 40(%rax)
	movl	64(%rsp), %ecx
	movl	%ecx, 48(%rax)
	movw	$44, 56(%rax)
	movl	80(%rsp), %ecx
	movl	%ecx, 64(%rax)
	movw	$44, 72(%rax)
	movl	96(%rsp), %ecx
	movl	%ecx, 80(%rax)
	movw	$44, 88(%rax)
	leaq	32(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB4_4
# BB#3:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB4_5
.LBB4_4:
	movq	$0, -16(%rbx)
	movw	$24, -8(%rbx)
	movslq	12(%rsp), %rax
	movq	%rax, 16(%rbx)
	movw	$20, 24(%rbx)
	movslq	8(%rsp), %rax
	movq	%rax, 32(%rbx)
	movw	$20, 40(%rbx)
	xorl	%eax, %eax
.LBB4_5:
	addq	$112, %rsp
	popq	%rbx
	retq
.Lfunc_end4:
	.size	zdeviceparams, .Lfunc_end4-zdeviceparams
	.cfi_endproc

	.globl	zflushpage
	.p2align	4, 0x90
	.type	zflushpage,@function
zflushpage:                             # @zflushpage
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_flushpage            # TAILCALL
.Lfunc_end5:
	.size	zflushpage, .Lfunc_end5-zflushpage
	.cfi_endproc

	.globl	zgetdevice
	.p2align	4, 0x90
	.type	zgetdevice,@function
zgetdevice:                             # @zgetdevice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %eax
	andl	$252, %eax
	movl	$-20, %ebp
	cmpl	$20, %eax
	jne	.LBB6_4
# BB#1:
	movq	(%rbx), %rdi
	movslq	%edi, %rax
	movl	$-15, %ebp
	cmpq	%rax, %rdi
	jne	.LBB6_4
# BB#2:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	gs_getdevice
	testq	%rax, %rax
	je	.LBB6_4
# BB#3:
	movq	%rax, (%rbx)
	movw	$60, 8(%rbx)
	xorl	%ebp, %ebp
.LBB6_4:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end6:
	.size	zgetdevice, .Lfunc_end6-zgetdevice
	.cfi_endproc

	.globl	zmakedevice
	.p2align	4, 0x90
	.type	zmakedevice,@function
zmakedevice:                            # @zmakedevice
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 16
	subq	$112, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 128
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	-40(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$60, %ecx
	jne	.LBB7_8
# BB#1:
	movzwl	-8(%rbx), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB7_8
# BB#2:
	movzwl	8(%rbx), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB7_8
# BB#3:
	movl	$-15, %eax
	cmpq	$2147483647, -16(%rbx)  # imm = 0x7FFFFFFF
	ja	.LBB7_8
# BB#4:
	cmpq	$2147483647, (%rbx)     # imm = 0x7FFFFFFF
	ja	.LBB7_8
# BB#5:
	leaq	-32(%rbx), %rdi
	leaq	16(%rsp), %rsi
	xorl	%eax, %eax
	callq	read_matrix
	testl	%eax, %eax
	js	.LBB7_8
# BB#6:
	movq	-48(%rbx), %rsi
	movl	-16(%rbx), %ecx
	movl	(%rbx), %r8d
	leaq	8(%rsp), %rdi
	leaq	16(%rsp), %rdx
	movl	$alloc, %r9d
	callq	gs_makedevice
	testl	%eax, %eax
	jne	.LBB7_8
# BB#7:
	movq	8(%rsp), %rax
	movq	%rax, -48(%rbx)
	movw	$60, -40(%rbx)
	addq	$-48, osp(%rip)
	xorl	%eax, %eax
.LBB7_8:
	addq	$112, %rsp
	popq	%rbx
	retq
.Lfunc_end7:
	.size	zmakedevice, .Lfunc_end7-zmakedevice
	.cfi_endproc

	.globl	zmakeimagedevice
	.p2align	4, 0x90
	.type	zmakeimagedevice,@function
zmakeimagedevice:                       # @zmakeimagedevice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 48
	subq	$3184, %rsp             # imm = 0xC70
.Lcfi23:
	.cfi_def_cfa_offset 3232
.Lcfi24:
	.cfi_offset %rbx, -48
.Lcfi25:
	.cfi_offset %r12, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movzwl	-24(%r15), %eax
	andl	$252, %eax
	cmpl	$20, %eax
	jne	.LBB8_18
# BB#1:
	movzwl	-8(%r15), %eax
	andl	$252, %eax
	cmpl	$20, %eax
	jne	.LBB8_18
# BB#2:
	movzwl	8(%r15), %eax
	andl	$252, %eax
	shrl	$2, %eax
	movl	$-24, %r14d
	cmpl	$8, %eax
	je	.LBB8_6
# BB#3:
	andb	$63, %al
	cmpb	$10, %al
	je	.LBB8_5
# BB#4:
	testb	%al, %al
	jne	.LBB8_18
.LBB8_5:
	movzwl	10(%r15), %r14d
.LBB8_6:
	movl	$-15, %eax
	cmpq	$2147483647, -32(%r15)  # imm = 0x7FFFFFFF
	ja	.LBB8_19
# BB#7:
	cmpl	$256, %r14d             # imm = 0x100
	jg	.LBB8_19
# BB#8:
	cmpq	$2147483647, -16(%r15)  # imm = 0x7FFFFFFF
	ja	.LBB8_19
# BB#9:
	leaq	-48(%r15), %rdi
	leaq	16(%rsp), %rsi
	xorl	%eax, %eax
	callq	read_matrix
	testl	%eax, %eax
	js	.LBB8_19
# BB#10:
	testl	%r14d, %r14d
	jle	.LBB8_15
# BB#11:                                # %.lr.ph.preheader
	leaq	112(%rsp), %r12
	movq	(%r15), %rbx
	addq	$8, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_12:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx), %eax
	andl	$252, %eax
	cmpl	$56, %eax
	jne	.LBB8_18
# BB#13:                                #   in Loop: Header=BB8_12 Depth=1
	movq	-8(%rbx), %rdi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	gs_colorrgb
	testl	%eax, %eax
	js	.LBB8_19
# BB#14:                                #   in Loop: Header=BB8_12 Depth=1
	incl	%ebp
	addq	$12, %r12
	addq	$16, %rbx
	cmpl	%r14d, %ebp
	jl	.LBB8_12
.LBB8_15:                               # %._crit_edge
	movl	-32(%r15), %edx
	movl	-16(%r15), %ecx
	movq	$alloc, (%rsp)
	leaq	8(%rsp), %rdi
	leaq	16(%rsp), %rsi
	leaq	112(%rsp), %r8
	movl	%r14d, %r9d
	callq	gs_makeimagedevice
	testl	%eax, %eax
	jne	.LBB8_19
# BB#16:
	movq	8(%rsp), %rax
	movq	%rax, -48(%r15)
	movw	$60, -40(%r15)
	addq	$-48, osp(%rip)
	xorl	%eax, %eax
	jmp	.LBB8_19
.LBB8_18:
	movl	$-20, %eax
.LBB8_19:                               # %.thread
	addq	$3184, %rsp             # imm = 0xC70
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	zmakeimagedevice, .Lfunc_end8-zmakeimagedevice
	.cfi_endproc

	.globl	znulldevice
	.p2align	4, 0x90
	.type	znulldevice,@function
znulldevice:                            # @znulldevice
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 16
	movq	igs(%rip), %rdi
	callq	gs_nulldevice
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end9:
	.size	znulldevice, .Lfunc_end9-znulldevice
	.cfi_endproc

	.globl	zsetdevice
	.p2align	4, 0x90
	.type	zsetdevice,@function
zsetdevice:                             # @zsetdevice
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$60, %ecx
	jne	.LBB10_3
# BB#1:
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 16
	movq	igs(%rip), %rax
	movq	(%rdi), %rsi
	movq	%rax, %rdi
	callq	gs_setdevice
	testl	%eax, %eax
	leaq	8(%rsp), %rsp
	jne	.LBB10_3
# BB#2:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB10_3:
	retq
.Lfunc_end10:
	.size	zsetdevice, .Lfunc_end10-zsetdevice
	.cfi_endproc

	.globl	zdevice_op_init
	.p2align	4, 0x90
	.type	zdevice_op_init,@function
zdevice_op_init:                        # @zdevice_op_init
	.cfi_startproc
# BB#0:
	movl	$zdevice_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end11:
	.size	zdevice_op_init, .Lfunc_end11-zdevice_op_init
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"devicename"
	.size	.L.str, 11

	.type	zdevice_op_init.my_defs,@object # @zdevice_op_init.my_defs
	.data
	.p2align	4
zdevice_op_init.my_defs:
	.quad	.L.str.1
	.quad	zcopypage
	.quad	.L.str.2
	.quad	zcopyscanlines
	.quad	.L.str.3
	.quad	zcurrentdevice
	.quad	.L.str.4
	.quad	zdevicename
	.quad	.L.str.5
	.quad	zdeviceparams
	.quad	.L.str.6
	.quad	zflushpage
	.quad	.L.str.7
	.quad	zgetdevice
	.quad	.L.str.8
	.quad	zmakedevice
	.quad	.L.str.9
	.quad	zmakeimagedevice
	.quad	.L.str.10
	.quad	znulldevice
	.quad	.L.str.11
	.quad	zsetdevice
	.zero	16
	.size	zdevice_op_init.my_defs, 192

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"0copypage"
	.size	.L.str.1, 10

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"3copyscanlines"
	.size	.L.str.2, 15

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"0currentdevice"
	.size	.L.str.3, 15

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"1devicename"
	.size	.L.str.4, 12

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"1deviceparams"
	.size	.L.str.5, 14

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"0flushpage"
	.size	.L.str.6, 11

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"1getdevice"
	.size	.L.str.7, 11

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"4makedevice"
	.size	.L.str.8, 12

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"4makeimagedevice"
	.size	.L.str.9, 17

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"0.nulldevice"
	.size	.L.str.10, 13

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"1.setdevice"
	.size	.L.str.11, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
