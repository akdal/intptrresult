	.text
	.file	"channel.bc"
	.globl	BuildChannel
	.p2align	4, 0x90
	.type	BuildChannel,@function
BuildChannel:                           # @BuildChannel
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	callq	DimensionChannel
	callq	DescribeChannel
	popq	%rax
	jmp	DensityChannel          # TAILCALL
.Lfunc_end0:
	.size	BuildChannel, .Lfunc_end0-BuildChannel
	.cfi_endproc

	.globl	DimensionChannel
	.p2align	4, 0x90
	.type	DimensionChannel,@function
DimensionChannel:                       # @DimensionChannel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 96
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	channelFile(%rip), %rdi
	movl	$.L.str, %esi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_5
# BB#1:                                 # %.preheader
	movl	$1, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	28(%rsp), %rdx
	leaq	24(%rsp), %rcx
	leaq	20(%rsp), %rbx
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB1_2
	.p2align	4, 0x90
.LBB1_4:                                # %.thread
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	28(%rsp), %eax
	movl	24(%rsp), %ecx
	movl	20(%rsp), %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	cmpq	%rsi, %rax
	cmovaq	%rax, %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	cmpq	%rbp, %rcx
	cmovaq	%rcx, %rbp
	cmpq	%rbp, %rdx
	cmovaq	%rdx, %rbp
	incq	32(%rsp)                # 8-byte Folded Spill
	movq	%r12, %rdx
	movq	%r13, %rcx
	movq	%r14, %rbx
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rdx, %r12
	movq	%rcx, %r13
	movq	%rbx, %r14
	movq	%rbx, %r8
	callq	fscanf
	cmpl	$-1, %eax
	je	.LBB1_8
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	cmpl	$3, %eax
	je	.LBB1_4
# BB#7:
	movl	$.Lstr.15, %edi
	callq	puts
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	printf
	movl	$.Lstr.12, %edi
	jmp	.LBB1_6
.LBB1_8:
	movq	%r15, %rdi
	callq	fclose
	cmpl	$-1, %eax
	je	.LBB1_9
# BB#10:
	movq	8(%rsp), %rax           # 8-byte Reload
	testq	%rax, %rax
	je	.LBB1_11
# BB#12:
	movq	%rax, channelColumns(%rip)
	movq	%rbp, channelNets(%rip)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_5:
	movl	$.Lstr.15, %edi
	callq	puts
	movl	$.Lstr.16, %edi
	jmp	.LBB1_6
.LBB1_9:
	movl	$.Lstr.15, %edi
	callq	puts
	movl	$.Lstr.10, %edi
	jmp	.LBB1_6
.LBB1_11:
	movl	$.Lstr.15, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr.2, %edi
.LBB1_6:
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	DimensionChannel, .Lfunc_end1-DimensionChannel
	.cfi_endproc

	.globl	DescribeChannel
	.p2align	4, 0x90
	.type	DescribeChannel,@function
DescribeChannel:                        # @DescribeChannel
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -48
.Lcfi21:
	.cfi_offset %r12, -40
.Lcfi22:
	.cfi_offset %r13, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %r15, -16
	movq	channelColumns(%rip), %r15
	leaq	8(,%r15,8), %r14
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, TOP(%rip)
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, BOT(%rip)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movq	$0, (%rbx,%rcx,8)
	movq	$0, (%rax,%rcx,8)
	incq	%rcx
	cmpq	%r15, %rcx
	jbe	.LBB2_1
# BB#2:
	movq	channelFile(%rip), %rdi
	movl	$.L.str, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_8
# BB#3:                                 # %.preheader
	movl	$1, %r14d
	leaq	12(%rsp), %r15
	leaq	8(%rsp), %r12
	leaq	4(%rsp), %r13
	jmp	.LBB2_4
	.p2align	4, 0x90
.LBB2_10:                               #   in Loop: Header=BB2_4 Depth=1
	movl	8(%rsp), %ecx
	movl	4(%rsp), %edx
	movq	BOT(%rip), %rsi
	movq	%rcx, (%rsi,%rax,8)
	movq	TOP(%rip), %rcx
	movq	%rdx, (%rcx,%rax,8)
	incq	%r14
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r12, %rcx
	movq	%r13, %r8
	callq	fscanf
	cmpl	$-1, %eax
	je	.LBB2_12
# BB#5:                                 #   in Loop: Header=BB2_4 Depth=1
	cmpl	$3, %eax
	jne	.LBB2_11
# BB#6:                                 #   in Loop: Header=BB2_4 Depth=1
	movl	12(%rsp), %eax
	cmpq	channelColumns(%rip), %rax
	jbe	.LBB2_10
# BB#7:
	movl	$.Lstr.15, %edi
	callq	puts
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	printf
	movl	$.Lstr.14, %edi
	jmp	.LBB2_9
.LBB2_12:
	movq	%rbx, %rdi
	callq	fclose
	cmpl	$-1, %eax
	je	.LBB2_13
# BB#14:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB2_11:
	movl	$.Lstr.15, %edi
	callq	puts
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	printf
	movl	$.Lstr.12, %edi
	jmp	.LBB2_9
.LBB2_8:
	movl	$.Lstr.15, %edi
	callq	puts
	movl	$.Lstr.16, %edi
	jmp	.LBB2_9
.LBB2_13:
	movl	$.Lstr.15, %edi
	callq	puts
	movl	$.Lstr.10, %edi
.LBB2_9:
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	DescribeChannel, .Lfunc_end2-DescribeChannel
	.cfi_endproc

	.globl	DensityChannel
	.p2align	4, 0x90
	.type	DensityChannel,@function
DensityChannel:                         # @DensityChannel
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 48
.Lcfi30:
	.cfi_offset %rbx, -40
.Lcfi31:
	.cfi_offset %r12, -32
.Lcfi32:
	.cfi_offset %r14, -24
.Lcfi33:
	.cfi_offset %r15, -16
	movq	channelNets(%rip), %rax
	leaq	8(,%rax,8), %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %r14
	movq	%r14, FIRST(%rip)
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, LAST(%rip)
	movq	channelColumns(%rip), %rax
	leaq	8(,%rax,8), %rdi
	callq	malloc
	movq	%rax, %r15
	movq	%r15, DENSITY(%rip)
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, CROSSING(%rip)
	xorl	%ecx, %ecx
	movq	channelNets(%rip), %rdx
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movq	$0, (%r14,%rcx,8)
	movq	$0, (%rbx,%rcx,8)
	movq	$0, (%rax,%rcx,8)
	incq	%rcx
	cmpq	%rdx, %rcx
	jbe	.LBB3_1
# BB#2:                                 # %.preheader58.preheader
	movq	channelColumns(%rip), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader58
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, (%r15,%rcx,8)
	incq	%rcx
	cmpq	%rax, %rcx
	jbe	.LBB3_3
# BB#4:                                 # %.preheader57
	cmpq	$0, channelNets(%rip)
	je	.LBB3_20
# BB#5:                                 # %.preheader.lr.ph
	movq	FIRST(%rip), %r8
	movq	LAST(%rip), %rdx
	movq	DENSITY(%rip), %rsi
	movq	BOT(%rip), %rdi
	movq	TOP(%rip), %r9
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB3_6:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_8 Depth 2
                                        #     Child Loop BB3_13 Depth 2
                                        #     Child Loop BB3_18 Depth 2
	testq	%rax, %rax
	je	.LBB3_16
# BB#7:                                 # %.lr.ph66.preheader
                                        #   in Loop: Header=BB3_6 Depth=1
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB3_8:                                # %.lr.ph66
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, (%rdi,%rbx,8)
	je	.LBB3_11
# BB#9:                                 #   in Loop: Header=BB3_8 Depth=2
	cmpq	%rcx, (%r9,%rbx,8)
	je	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_8 Depth=2
	incq	%rbx
	cmpq	%rax, %rbx
	jbe	.LBB3_8
	jmp	.LBB3_12
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_6 Depth=1
	movq	%rbx, (%r8,%rcx,8)
	movq	channelColumns(%rip), %rax
.LBB3_12:                               # %.loopexit56
                                        #   in Loop: Header=BB3_6 Depth=1
	testq	%rax, %rax
	je	.LBB3_16
	.p2align	4, 0x90
.LBB3_13:                               # %.lr.ph69
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, (%rdi,%rax,8)
	je	.LBB3_17
# BB#14:                                #   in Loop: Header=BB3_13 Depth=2
	cmpq	%rcx, (%r9,%rax,8)
	je	.LBB3_17
# BB#15:                                #   in Loop: Header=BB3_13 Depth=2
	decq	%rax
	jne	.LBB3_13
	jmp	.LBB3_16
	.p2align	4, 0x90
.LBB3_17:                               #   in Loop: Header=BB3_6 Depth=1
	movq	%rax, (%rdx,%rcx,8)
.LBB3_16:                               # %.loopexit
                                        #   in Loop: Header=BB3_6 Depth=1
	movq	(%r8,%rcx,8), %rax
	cmpq	(%rdx,%rcx,8), %rax
	ja	.LBB3_19
	.p2align	4, 0x90
.LBB3_18:                               # %.lr.ph72
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	(%rsi,%rax,8)
	incq	%rax
	cmpq	(%rdx,%rcx,8), %rax
	jbe	.LBB3_18
.LBB3_19:                               # %._crit_edge73
                                        #   in Loop: Header=BB3_6 Depth=1
	incq	%rcx
	movq	channelColumns(%rip), %rax
	cmpq	channelNets(%rip), %rcx
	jbe	.LBB3_6
.LBB3_20:                               # %._crit_edge75
	testq	%rax, %rax
	je	.LBB3_24
# BB#21:                                # %.lr.ph
	movq	DENSITY(%rip), %rcx
	leaq	-1(%rax), %r8
	movq	%rax, %rbx
	andq	$3, %rbx
	je	.LBB3_25
# BB#22:                                # %.prol.preheader
	negq	%rbx
	xorl	%edx, %edx
                                        # implicit-def: %RSI
	.p2align	4, 0x90
.LBB3_23:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rax,8), %rdi
	cmpq	%rdx, %rdi
	cmovaq	%rdi, %rdx
	cmovaq	%rax, %rsi
	decq	%rax
	incq	%rbx
	jne	.LBB3_23
	jmp	.LBB3_26
.LBB3_24:
	xorl	%edx, %edx
                                        # implicit-def: %RSI
	jmp	.LBB3_28
.LBB3_25:
	xorl	%edx, %edx
                                        # implicit-def: %RSI
.LBB3_26:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB3_28
	.p2align	4, 0x90
.LBB3_27:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rax,8), %rdi
	cmpq	%rdx, %rdi
	cmovaq	%rdi, %rdx
	cmovaq	%rax, %rsi
	movq	-8(%rcx,%rax,8), %rdi
	leaq	-1(%rax), %rbx
	cmpq	%rdx, %rdi
	cmovaq	%rdi, %rdx
	cmovbeq	%rsi, %rbx
	movq	-16(%rcx,%rax,8), %rsi
	leaq	-2(%rax), %rdi
	cmpq	%rdx, %rsi
	cmovaq	%rsi, %rdx
	cmovbeq	%rbx, %rdi
	movq	-24(%rcx,%rax,8), %rbx
	leaq	-3(%rax), %rsi
	cmpq	%rdx, %rbx
	cmovaq	%rbx, %rdx
	cmovbeq	%rdi, %rsi
	addq	$-4, %rax
	jne	.LBB3_27
.LBB3_28:                               # %._crit_edge
	movq	%rdx, channelTracks(%rip)
	movq	%rdx, channelDensity(%rip)
	movq	%rsi, channelDensityColumn(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	DensityChannel, .Lfunc_end3-DensityChannel
	.cfi_endproc

	.type	channelFile,@object     # @channelFile
	.comm	channelFile,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"r"
	.size	.L.str, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%u%u%u"
	.size	.L.str.3, 7

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\tChannel file description invalid at line %d.\n"
	.size	.L.str.4, 47

	.type	channelColumns,@object  # @channelColumns
	.comm	channelColumns,8,8
	.type	channelNets,@object     # @channelNets
	.comm	channelNets,8,8
	.type	TOP,@object             # @TOP
	.comm	TOP,8,8
	.type	BOT,@object             # @BOT
	.comm	BOT,8,8
	.type	FIRST,@object           # @FIRST
	.comm	FIRST,8,8
	.type	LAST,@object            # @LAST
	.comm	LAST,8,8
	.type	DENSITY,@object         # @DENSITY
	.comm	DENSITY,8,8
	.type	CROSSING,@object        # @CROSSING
	.comm	CROSSING,8,8
	.type	channelTracks,@object   # @channelTracks
	.comm	channelTracks,8,8
	.type	channelDensity,@object  # @channelDensity
	.comm	channelDensity,8,8
	.type	channelDensityColumn,@object # @channelDensityColumn
	.comm	channelDensityColumn,8,8
	.type	channelTracksCopy,@object # @channelTracksCopy
	.comm	channelTracksCopy,8,8
	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.1:
	.asciz	"\tChannel description invalid."
	.size	.Lstr.1, 30

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"\tChannel has null dimension."
	.size	.Lstr.2, 29

	.type	.Lstr.10,@object        # @str.10
	.p2align	4
.Lstr.10:
	.asciz	"\tChannel file cannot be closed."
	.size	.Lstr.10, 32

	.type	.Lstr.12,@object        # @str.12
	.p2align	4
.Lstr.12:
	.asciz	"\tIncorrect number of specifiers."
	.size	.Lstr.12, 33

	.type	.Lstr.14,@object        # @str.14
	.p2align	4
.Lstr.14:
	.asciz	"\tColumn number out of range."
	.size	.Lstr.14, 29

	.type	.Lstr.15,@object        # @str.15
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.15:
	.asciz	"Error:"
	.size	.Lstr.15, 7

	.type	.Lstr.16,@object        # @str.16
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.16:
	.asciz	"\tChannel file cannot be opened."
	.size	.Lstr.16, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
