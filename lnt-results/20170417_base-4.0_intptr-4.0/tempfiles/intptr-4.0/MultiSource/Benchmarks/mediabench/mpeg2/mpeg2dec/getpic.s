	.text
	.file	"getpic.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	4294965248              # 0xfffff800
	.long	4294965248              # 0xfffff800
	.long	4294965248              # 0xfffff800
	.long	4294965248              # 0xfffff800
.LCPI0_1:
	.long	63488                   # 0xf800
	.long	63488                   # 0xf800
	.long	63488                   # 0xf800
	.long	63488                   # 0xf800
.LCPI0_2:
	.long	2048                    # 0x800
	.long	2048                    # 0x800
	.long	2048                    # 0x800
	.long	2048                    # 0x800
.LCPI0_3:
	.long	2047                    # 0x7ff
	.long	2047                    # 0x7ff
	.long	2047                    # 0x7ff
	.long	2047                    # 0x7ff
	.text
	.globl	Decode_Picture
	.p2align	4, 0x90
	.type	Decode_Picture,@function
Decode_Picture:                         # @Decode_Picture
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 224
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, 52(%rsp)          # 4-byte Spill
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movl	picture_structure(%rip), %esi
	movl	Second_Field(%rip), %eax
	cmpl	$3, %esi
	jne	.LBB0_3
# BB#1:
	testl	%eax, %eax
	je	.LBB0_3
# BB#2:
	movl	$.Lstr, %edi
	callq	puts
	movl	$0, Second_Field(%rip)
	xorl	%eax, %eax
	movl	picture_structure(%rip), %esi
.LBB0_3:
	movslq	Coded_Picture_Width(%rip), %rdx
	movslq	Chroma_Width(%rip), %rcx
	cmpl	$3, picture_coding_type(%rip)
	jne	.LBB0_6
# BB#4:                                 # %.split.us.i
	cmpl	$2, %esi
	jne	.LBB0_9
# BB#5:                                 # %.split.us.split.us.preheader.i
	addq	auxframe(%rip), %rdx
	movq	%rdx, current_frame(%rip)
	movq	auxframe+8(%rip), %rdx
	addq	%rcx, %rdx
	movq	%rdx, current_frame+8(%rip)
	addq	auxframe+16(%rip), %rcx
	jmp	.LBB0_14
.LBB0_6:                                # %.split.i
	testl	%eax, %eax
	je	.LBB0_10
# BB#7:                                 # %.split.split.preheader.i
	cmpl	$2, %esi
	movq	backward_reference_frame(%rip), %rsi
	jne	.LBB0_12
# BB#8:
	addq	%rdx, %rsi
	movq	%rsi, current_frame(%rip)
	movq	backward_reference_frame+8(%rip), %rdx
	addq	%rcx, %rdx
	movq	%rdx, current_frame+8(%rip)
	addq	backward_reference_frame+16(%rip), %rcx
	jmp	.LBB0_14
.LBB0_9:                                # %.split.us.split.preheader.i
	movq	auxframe+16(%rip), %rcx
	movq	%rcx, current_frame+16(%rip)
	movdqa	auxframe(%rip), %xmm0
	movdqa	%xmm0, current_frame(%rip)
	cmpl	$0, Ersatz_Flag(%rip)
	jne	.LBB0_15
	jmp	.LBB0_16
.LBB0_10:                               # %.split.split.us.preheader.i
	cmpl	$2, %esi
	movq	forward_reference_frame(%rip), %rsi
	movq	backward_reference_frame(%rip), %rdi
	movq	%rdi, forward_reference_frame(%rip)
	movq	%rsi, backward_reference_frame(%rip)
	jne	.LBB0_13
# BB#11:
	addq	%rdx, %rsi
	movq	%rsi, current_frame(%rip)
	movq	forward_reference_frame+8(%rip), %rdx
	movdqu	backward_reference_frame+8(%rip), %xmm0
	movq	%rdx, backward_reference_frame+8(%rip)
	addq	%rcx, %rdx
	movq	%rdx, current_frame+8(%rip)
	movq	forward_reference_frame+16(%rip), %rdx
	movdqu	%xmm0, forward_reference_frame+8(%rip)
	movq	%rdx, backward_reference_frame+16(%rip)
	addq	%rcx, %rdx
	movq	%rdx, current_frame+16(%rip)
	cmpl	$0, Ersatz_Flag(%rip)
	jne	.LBB0_15
	jmp	.LBB0_16
.LBB0_12:                               # %.split.split.232.i
	movq	%rsi, current_frame(%rip)
	movq	backward_reference_frame+8(%rip), %rcx
	movq	%rcx, current_frame+8(%rip)
	movq	backward_reference_frame+16(%rip), %rcx
	jmp	.LBB0_14
.LBB0_13:                               # %.split.split.us.229.i
	movq	%rsi, current_frame(%rip)
	movq	forward_reference_frame+8(%rip), %rcx
	movdqu	backward_reference_frame+8(%rip), %xmm0
	movq	%rcx, backward_reference_frame+8(%rip)
	movq	%rcx, current_frame+8(%rip)
	movq	forward_reference_frame+16(%rip), %rcx
	movdqu	%xmm0, forward_reference_frame+8(%rip)
	movq	%rcx, backward_reference_frame+16(%rip)
.LBB0_14:                               # %.us-lcssa.us.loopexit2733.i
	movq	%rcx, current_frame+16(%rip)
	cmpl	$0, Ersatz_Flag(%rip)
	je	.LBB0_16
.LBB0_15:
	movl	28(%rsp), %edi          # 4-byte Reload
	movl	52(%rsp), %esi          # 4-byte Reload
	callq	Substitute_Frame_Buffer
	movl	Second_Field(%rip), %eax
.LBB0_16:
	cmpl	$0, base+3160(%rip)
	je	.LBB0_19
# BB#17:
	testl	%eax, %eax
	jne	.LBB0_19
# BB#18:
	callq	Spatial_Prediction
.LBB0_19:
	movl	mb_height(%rip), %eax
	imull	mb_width(%rip), %eax
	cmpl	$3, picture_structure(%rip)
	setne	%cl
	sarl	%cl, %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movq	$base, ld(%rip)
	movl	$0, Fault_Flag(%rip)
	callq	next_start_code
	movl	$32, %edi
	callq	Show_Bits
	movl	%eax, %ebx
	leal	-257(%rbx), %eax
	cmpl	$174, %eax
	ja	.LBB0_208
	.p2align	4, 0x90
.LBB0_21:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_29 Depth 2
                                        #       Child Loop BB0_52 Depth 3
                                        #       Child Loop BB0_101 Depth 3
                                        #       Child Loop BB0_91 Depth 3
                                        #       Child Loop BB0_148 Depth 3
                                        #       Child Loop BB0_160 Depth 3
                                        #       Child Loop BB0_174 Depth 3
                                        #         Child Loop BB0_181 Depth 4
                                        #         Child Loop BB0_204 Depth 4
                                        #         Child Loop BB0_202 Depth 4
	callq	Flush_Buffer32
	callq	slice_header
	movl	%eax, %ebp
	cmpl	$1, base+3148(%rip)
	jne	.LBB0_25
# BB#22:                                #   in Loop: Header=BB0_21 Depth=1
	movq	$enhan, ld(%rip)
	callq	next_start_code
	movl	$32, %edi
	callq	Show_Bits
	movl	%eax, %ebx
	leal	-257(%rbx), %eax
	cmpl	$175, %eax
	jae	.LBB0_216
# BB#23:                                #   in Loop: Header=BB0_21 Depth=1
	callq	Flush_Buffer32
	callq	slice_header
	movl	%eax, %ebp
	cmpl	$1, base+3164(%rip)
	je	.LBB0_25
# BB#24:                                #   in Loop: Header=BB0_21 Depth=1
	movq	$base, ld(%rip)
.LBB0_25:                               #   in Loop: Header=BB0_21 Depth=1
	callq	Get_macroblock_address_increment
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$0, Fault_Flag(%rip)
	je	.LBB0_27
# BB#26:                                # %.critedge6.i
                                        #   in Loop: Header=BB0_21 Depth=1
	movl	$.Lstr.3, %edi
	callq	puts
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_27:                               #   in Loop: Header=BB0_21 Depth=1
	shll	$7, %ebp
	movzbl	%bl, %ecx
	leal	-1(%rcx,%rbp), %ecx
	imull	mb_width(%rip), %ecx
	leal	-1(%rax,%rcx), %r12d
	movl	$0, 84(%rsp)
	movq	$0, 76(%rsp)
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 112(%rsp)
	movdqa	%xmm0, 96(%rsp)
	movl	$0, Fault_Flag(%rip)
	cmpl	32(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB0_210
# BB#28:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB0_21 Depth=1
	xorl	%ebx, %ebx
	movl	$1, %eax
	xorl	%ebp, %ebp
	movdqa	.LCPI0_0(%rip), %xmm4   # xmm4 = [4294965248,4294965248,4294965248,4294965248]
	movdqa	.LCPI0_1(%rip), %xmm5   # xmm5 = [63488,63488,63488,63488]
	movdqa	.LCPI0_2(%rip), %xmm6   # xmm6 = [2048,2048,2048,2048]
	movdqa	.LCPI0_3(%rip), %xmm7   # xmm7 = [2047,2047,2047,2047]
.LBB0_29:                               # %.preheader.i.i
                                        #   Parent Loop BB0_21 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_52 Depth 3
                                        #       Child Loop BB0_101 Depth 3
                                        #       Child Loop BB0_91 Depth 3
                                        #       Child Loop BB0_148 Depth 3
                                        #       Child Loop BB0_160 Depth 3
                                        #       Child Loop BB0_174 Depth 3
                                        #         Child Loop BB0_181 Depth 4
                                        #         Child Loop BB0_204 Depth 4
                                        #         Child Loop BB0_202 Depth 4
	movq	$base, ld(%rip)
	testl	%eax, %eax
	jne	.LBB0_40
# BB#30:                                #   in Loop: Header=BB0_29 Depth=2
	cmpl	$1, base+3148(%rip)
	jne	.LBB0_33
# BB#31:                                #   in Loop: Header=BB0_29 Depth=2
	cmpl	$1, base+3164(%rip)
	jne	.LBB0_33
# BB#32:                                #   in Loop: Header=BB0_29 Depth=2
	movq	$enhan, ld(%rip)
.LBB0_33:                               #   in Loop: Header=BB0_29 Depth=2
	movl	$23, %edi
	callq	Show_Bits
	testl	%eax, %eax
	je	.LBB0_207
# BB#34:                                #   in Loop: Header=BB0_29 Depth=2
	movl	Fault_Flag(%rip), %eax
	testl	%eax, %eax
	jne	.LBB0_207
# BB#35:                                #   in Loop: Header=BB0_29 Depth=2
	cmpl	$1, base+3148(%rip)
	jne	.LBB0_38
# BB#36:                                #   in Loop: Header=BB0_29 Depth=2
	cmpl	$1, base+3164(%rip)
	jne	.LBB0_38
# BB#37:                                #   in Loop: Header=BB0_29 Depth=2
	movq	$enhan, ld(%rip)
.LBB0_38:                               #   in Loop: Header=BB0_29 Depth=2
	callq	Get_macroblock_address_increment
	cmpl	$0, Fault_Flag(%rip)
	jne	.LBB0_207
# BB#39:                                #   in Loop: Header=BB0_29 Depth=2
	pxor	%xmm0, %xmm0
	movdqa	.LCPI0_0(%rip), %xmm4   # xmm4 = [4294965248,4294965248,4294965248,4294965248]
	movdqa	.LCPI0_1(%rip), %xmm5   # xmm5 = [63488,63488,63488,63488]
	movdqa	.LCPI0_2(%rip), %xmm6   # xmm6 = [2048,2048,2048,2048]
	movdqa	.LCPI0_3(%rip), %xmm7   # xmm7 = [2047,2047,2047,2047]
.LBB0_40:                               #   in Loop: Header=BB0_29 Depth=2
	cmpl	$1, %eax
	movl	%r12d, 36(%rsp)         # 4-byte Spill
	movl	%eax, 136(%rsp)         # 4-byte Spill
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	jne	.LBB0_48
# BB#41:                                #   in Loop: Header=BB0_29 Depth=2
	movl	%ebx, 40(%rsp)          # 4-byte Spill
	cmpl	$1, base+3148(%rip)
	jne	.LBB0_43
# BB#42:                                #   in Loop: Header=BB0_29 Depth=2
	cmpl	$3, base+3164(%rip)
	movl	$base, %eax
	movl	$enhan, %ecx
	cmovlq	%rcx, %rax
	movq	%rax, ld(%rip)
.LBB0_43:                               #   in Loop: Header=BB0_29 Depth=2
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	leaq	56(%rsp), %rdi
	leaq	68(%rsp), %rsi
	leaq	148(%rsp), %rdx
	leaq	24(%rsp), %rcx
	leaq	32(%rsp), %r8
	leaq	28(%rsp), %r9
	leaq	72(%rsp), %rax
	pushq	%rax
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	leaq	84(%rsp), %rax
	pushq	%rax
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	leaq	96(%rsp), %rax
	pushq	%rax
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	macroblock_modes
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	cmpl	$0, Fault_Flag(%rip)
	pxor	%xmm0, %xmm0
	jne	.LBB0_207
# BB#44:                                #   in Loop: Header=BB0_29 Depth=2
	movl	48(%rsp), %r13d
	testb	$16, %r13b
	je	.LBB0_62
# BB#45:                                #   in Loop: Header=BB0_29 Depth=2
	movl	$5, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	cmpl	$0, 3144(%rcx)
	je	.LBB0_60
# BB#46:                                #   in Loop: Header=BB0_29 Depth=2
	cmpl	$0, 3152(%rcx)
	je	.LBB0_59
# BB#47:                                #   in Loop: Header=BB0_29 Depth=2
	cltq
	movzbl	Non_Linear_quantizer_scale(%rax), %eax
	jmp	.LBB0_60
	.p2align	4, 0x90
.LBB0_48:                               #   in Loop: Header=BB0_29 Depth=2
	cmpl	$1, base+3148(%rip)
	jne	.LBB0_50
# BB#49:                                #   in Loop: Header=BB0_29 Depth=2
	movq	$base, ld(%rip)
.LBB0_50:                               # %.preheader.i4.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	cmpl	$0, block_count(%rip)
	jle	.LBB0_53
# BB#51:                                # %.lr.ph.i8.i.i.preheader
                                        #   in Loop: Header=BB0_29 Depth=2
	movl	$3176, %eax             # imm = 0xC68
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_52:                               # %.lr.ph.i8.i.i
                                        #   Parent Loop BB0_21 Depth=1
                                        #     Parent Loop BB0_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	ld(%rip), %rdx
	movdqu	%xmm0, 112(%rdx,%rax)
	movdqu	%xmm0, 96(%rdx,%rax)
	movdqu	%xmm0, 80(%rdx,%rax)
	movdqu	%xmm0, 64(%rdx,%rax)
	movdqu	%xmm0, 48(%rdx,%rax)
	movdqu	%xmm0, 32(%rdx,%rax)
	movdqu	%xmm0, 16(%rdx,%rax)
	movdqu	%xmm0, (%rdx,%rax)
	incq	%rcx
	movslq	block_count(%rip), %rdx
	subq	$-128, %rax
	cmpq	%rdx, %rcx
	jl	.LBB0_52
.LBB0_53:                               # %._crit_edge.i9.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	movl	$0, 84(%rsp)
	movq	$0, 76(%rsp)
	movl	picture_coding_type(%rip), %eax
	cmpl	$2, %eax
	jne	.LBB0_55
# BB#54:                                #   in Loop: Header=BB0_29 Depth=2
	movl	$0, 116(%rsp)
	movl	$0, 112(%rsp)
	movq	$0, 96(%rsp)
.LBB0_55:                               #   in Loop: Header=BB0_29 Depth=2
	movl	picture_structure(%rip), %ecx
	cmpl	$3, %ecx
	jne	.LBB0_57
# BB#56:                                #   in Loop: Header=BB0_29 Depth=2
	movl	$2, 16(%rsp)
	jmp	.LBB0_58
	.p2align	4, 0x90
.LBB0_57:                               #   in Loop: Header=BB0_29 Depth=2
	movl	$1, 16(%rsp)
	xorl	%edx, %edx
	cmpl	$2, %ecx
	sete	%dl
	movl	%edx, 148(%rsp)
	movl	%edx, 144(%rsp)
.LBB0_58:                               # %skipped_macroblock.exit.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	sete	%cl
	shll	$3, %ecx
	movl	%ecx, 60(%rsp)
	movl	48(%rsp), %r13d
	andl	$-2, %r13d
	movl	%r13d, 48(%rsp)
	cmpl	$0, Two_Streams(%rip)
	jne	.LBB0_125
	jmp	.LBB0_168
.LBB0_59:                               #   in Loop: Header=BB0_29 Depth=2
	addl	%eax, %eax
.LBB0_60:                               #   in Loop: Header=BB0_29 Depth=2
	pxor	%xmm0, %xmm0
	movl	%eax, 3168(%rcx)
	cmpl	$1, base+3148(%rip)
	jne	.LBB0_62
# BB#61:                                #   in Loop: Header=BB0_29 Depth=2
	movl	%eax, base+3168(%rip)
.LBB0_62:                               #   in Loop: Header=BB0_29 Depth=2
	testb	$8, %r13b
	jne	.LBB0_65
# BB#63:                                #   in Loop: Header=BB0_29 Depth=2
	testb	$1, %r13b
	je	.LBB0_69
# BB#64:                                #   in Loop: Header=BB0_29 Depth=2
	movl	concealment_motion_vectors(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_69
.LBB0_65:                               #   in Loop: Header=BB0_29 Depth=2
	movq	ld(%rip), %rax
	cmpl	$0, 3144(%rax)
	je	.LBB0_67
# BB#66:                                #   in Loop: Header=BB0_29 Depth=2
	movl	24(%rsp), %r8d
	movl	20(%rsp), %r9d
	movl	f_code(%rip), %eax
	decl	%eax
	movl	f_code+4(%rip), %ebp
	decl	%ebp
	movl	72(%rsp), %r10d
	movl	68(%rsp), %ebx
	movl	$0, %ecx
	leaq	96(%rsp), %rdi
	leaq	88(%rsp), %rsi
	leaq	144(%rsp), %rdx
	pushq	%rbx
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	callq	motion_vectors
	addq	$32, %rsp
.Lcfi22:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_68
.LBB0_67:                               #   in Loop: Header=BB0_29 Depth=2
	movl	forward_f_code(%rip), %edx
	decl	%edx
	movl	full_pel_forward_vector(%rip), %eax
	subq	$8, %rsp
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	movl	$0, %r8d
	xorl	%r9d, %r9d
	leaq	104(%rsp), %rdi
	leaq	96(%rsp), %rsi
	movl	%edx, %ecx
	pushq	%rax
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	callq	motion_vector
	addq	$16, %rsp
.Lcfi25:
	.cfi_adjust_cfa_offset -16
.LBB0_68:                               #   in Loop: Header=BB0_29 Depth=2
	pxor	%xmm0, %xmm0
.LBB0_69:                               #   in Loop: Header=BB0_29 Depth=2
	cmpl	$0, Fault_Flag(%rip)
	jne	.LBB0_207
# BB#70:                                #   in Loop: Header=BB0_29 Depth=2
	testb	$4, %r13b
	je	.LBB0_74
# BB#71:                                #   in Loop: Header=BB0_29 Depth=2
	movq	ld(%rip), %rax
	cmpl	$0, 3144(%rax)
	je	.LBB0_73
# BB#72:                                #   in Loop: Header=BB0_29 Depth=2
	movl	24(%rsp), %r8d
	movl	20(%rsp), %r9d
	movl	f_code+8(%rip), %eax
	decl	%eax
	movl	f_code+12(%rip), %ebp
	decl	%ebp
	movl	68(%rsp), %ebx
	movl	$1, %ecx
	leaq	96(%rsp), %rdi
	leaq	88(%rsp), %rsi
	leaq	144(%rsp), %rdx
	pushq	%rbx
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	callq	motion_vectors
	pxor	%xmm0, %xmm0
	addq	$32, %rsp
.Lcfi30:
	.cfi_adjust_cfa_offset -32
	cmpl	$0, Fault_Flag(%rip)
	je	.LBB0_75
	jmp	.LBB0_207
.LBB0_73:                               #   in Loop: Header=BB0_29 Depth=2
	movl	backward_f_code(%rip), %edx
	decl	%edx
	movl	full_pel_backward_vector(%rip), %eax
	subq	$8, %rsp
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	movl	$0, %r8d
	xorl	%r9d, %r9d
	leaq	112(%rsp), %rdi
	leaq	96(%rsp), %rsi
	movl	%edx, %ecx
	pushq	%rax
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	callq	motion_vector
	pxor	%xmm0, %xmm0
	addq	$16, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset -16
	.p2align	4, 0x90
.LBB0_74:                               #   in Loop: Header=BB0_29 Depth=2
	cmpl	$0, Fault_Flag(%rip)
	jne	.LBB0_207
.LBB0_75:                               #   in Loop: Header=BB0_29 Depth=2
	movl	%r13d, %r14d
	andl	$1, %r14d
	je	.LBB0_78
# BB#76:                                #   in Loop: Header=BB0_29 Depth=2
	movl	concealment_motion_vectors(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_78
# BB#77:                                #   in Loop: Header=BB0_29 Depth=2
	movl	$1, %edi
	callq	Flush_Buffer
	pxor	%xmm0, %xmm0
.LBB0_78:                               #   in Loop: Header=BB0_29 Depth=2
	cmpl	$1, base+3148(%rip)
	jne	.LBB0_81
# BB#79:                                #   in Loop: Header=BB0_29 Depth=2
	cmpl	$3, base+3164(%rip)
	jne	.LBB0_81
# BB#80:                                #   in Loop: Header=BB0_29 Depth=2
	movq	$enhan, ld(%rip)
.LBB0_81:                               #   in Loop: Header=BB0_29 Depth=2
	testb	$2, %r13b
	jne	.LBB0_84
# BB#82:                                #   in Loop: Header=BB0_29 Depth=2
	xorl	%r15d, %r15d
	testl	%r14d, %r14d
	je	.LBB0_88
# BB#83:                                #   in Loop: Header=BB0_29 Depth=2
	movb	block_count(%rip), %cl
	movl	$1, %r15d
	shll	%cl, %r15d
	decl	%r15d
	cmpl	$0, Fault_Flag(%rip)
	je	.LBB0_89
	jmp	.LBB0_207
	.p2align	4, 0x90
.LBB0_84:                               #   in Loop: Header=BB0_29 Depth=2
	callq	Get_coded_block_pattern
	movl	%eax, %r15d
	movl	chroma_format(%rip), %eax
	cmpl	$3, %eax
	je	.LBB0_87
# BB#85:                                #   in Loop: Header=BB0_29 Depth=2
	cmpl	$2, %eax
	pxor	%xmm0, %xmm0
	jne	.LBB0_88
# BB#86:                                #   in Loop: Header=BB0_29 Depth=2
	shll	$2, %r15d
	movl	$2, %edi
	callq	Get_Bits
	pxor	%xmm0, %xmm0
	orl	%eax, %r15d
	cmpl	$0, Fault_Flag(%rip)
	je	.LBB0_89
	jmp	.LBB0_207
.LBB0_87:                               #   in Loop: Header=BB0_29 Depth=2
	shll	$6, %r15d
	movl	$6, %edi
	callq	Get_Bits
	orl	%eax, %r15d
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_88:                               #   in Loop: Header=BB0_29 Depth=2
	cmpl	$0, Fault_Flag(%rip)
	jne	.LBB0_207
.LBB0_89:                               # %.preheader.i.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	cmpl	$0, block_count(%rip)
	jle	.LBB0_111
# BB#90:                                # %.lr.ph.i.preheader.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	movl	$-1, %r12d
	movl	$3176, %ebx             # imm = 0xC68
	xorl	%ebp, %ebp
	testl	%r14d, %r14d
	jne	.LBB0_101
	.p2align	4, 0x90
.LBB0_91:                               # %.lr.ph.i.us.i.i
                                        #   Parent Loop BB0_21 Depth=1
                                        #     Parent Loop BB0_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$1, base+3148(%rip)
	jne	.LBB0_93
# BB#92:                                #   in Loop: Header=BB0_91 Depth=3
	movq	$base, ld(%rip)
	movl	$base, %eax
	jmp	.LBB0_94
	.p2align	4, 0x90
.LBB0_93:                               # %.lr.ph._crit_edge.i.us.i.i
                                        #   in Loop: Header=BB0_91 Depth=3
	movq	ld(%rip), %rax
.LBB0_94:                               #   in Loop: Header=BB0_91 Depth=3
	movdqu	%xmm0, 112(%rax,%rbx)
	movdqu	%xmm0, 96(%rax,%rbx)
	movdqu	%xmm0, 80(%rax,%rbx)
	movdqu	%xmm0, 64(%rax,%rbx)
	movdqu	%xmm0, 48(%rax,%rbx)
	movdqu	%xmm0, 32(%rax,%rbx)
	movdqu	%xmm0, 16(%rax,%rbx)
	movdqu	%xmm0, (%rax,%rbx)
	movl	block_count(%rip), %eax
	leal	(%r12,%rax), %ecx
	btl	%ecx, %r15d
	jae	.LBB0_100
# BB#95:                                #   in Loop: Header=BB0_91 Depth=3
	movq	ld(%rip), %rax
	cmpl	$0, 3144(%rax)
	je	.LBB0_97
# BB#96:                                #   in Loop: Header=BB0_91 Depth=3
	movl	%ebp, %edi
	callq	Decode_MPEG2_Non_Intra_Block
	jmp	.LBB0_98
	.p2align	4, 0x90
.LBB0_97:                               #   in Loop: Header=BB0_91 Depth=3
	movl	%ebp, %edi
	callq	Decode_MPEG1_Non_Intra_Block
.LBB0_98:                               #   in Loop: Header=BB0_91 Depth=3
	cmpl	$0, Fault_Flag(%rip)
	pxor	%xmm0, %xmm0
	jne	.LBB0_207
# BB#99:                                # %._crit_edge78.i.us.i.i
                                        #   in Loop: Header=BB0_91 Depth=3
	movl	block_count(%rip), %eax
.LBB0_100:                              #   in Loop: Header=BB0_91 Depth=3
	incq	%rbp
	cltq
	subq	$-128, %rbx
	decl	%r12d
	cmpq	%rax, %rbp
	jl	.LBB0_91
	jmp	.LBB0_111
	.p2align	4, 0x90
.LBB0_101:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB0_21 Depth=1
                                        #     Parent Loop BB0_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$1, base+3148(%rip)
	jne	.LBB0_103
# BB#102:                               #   in Loop: Header=BB0_101 Depth=3
	movq	$base, ld(%rip)
	movl	$base, %eax
	jmp	.LBB0_104
	.p2align	4, 0x90
.LBB0_103:                              # %.lr.ph._crit_edge.i.i.i
                                        #   in Loop: Header=BB0_101 Depth=3
	movq	ld(%rip), %rax
.LBB0_104:                              #   in Loop: Header=BB0_101 Depth=3
	movdqu	%xmm0, 112(%rax,%rbx)
	movdqu	%xmm0, 96(%rax,%rbx)
	movdqu	%xmm0, 80(%rax,%rbx)
	movdqu	%xmm0, 64(%rax,%rbx)
	movdqu	%xmm0, 48(%rax,%rbx)
	movdqu	%xmm0, 32(%rax,%rbx)
	movdqu	%xmm0, 16(%rax,%rbx)
	movdqu	%xmm0, (%rax,%rbx)
	movl	block_count(%rip), %eax
	leal	(%r12,%rax), %ecx
	btl	%ecx, %r15d
	jae	.LBB0_110
# BB#105:                               #   in Loop: Header=BB0_101 Depth=3
	movq	ld(%rip), %rax
	cmpl	$0, 3144(%rax)
	je	.LBB0_107
# BB#106:                               #   in Loop: Header=BB0_101 Depth=3
	movl	%ebp, %edi
	leaq	76(%rsp), %rsi
	callq	Decode_MPEG2_Intra_Block
	jmp	.LBB0_108
	.p2align	4, 0x90
.LBB0_107:                              #   in Loop: Header=BB0_101 Depth=3
	movl	%ebp, %edi
	leaq	76(%rsp), %rsi
	callq	Decode_MPEG1_Intra_Block
.LBB0_108:                              #   in Loop: Header=BB0_101 Depth=3
	cmpl	$0, Fault_Flag(%rip)
	pxor	%xmm0, %xmm0
	jne	.LBB0_207
# BB#109:                               # %._crit_edge78.i.i.i
                                        #   in Loop: Header=BB0_101 Depth=3
	movl	block_count(%rip), %eax
.LBB0_110:                              #   in Loop: Header=BB0_101 Depth=3
	incq	%rbp
	cltq
	subq	$-128, %rbx
	decl	%r12d
	cmpq	%rax, %rbp
	jl	.LBB0_101
.LBB0_111:                              # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	cmpl	$4, picture_coding_type(%rip)
	jne	.LBB0_113
# BB#112:                               #   in Loop: Header=BB0_29 Depth=2
	movl	$.L.str.6, %edi
	callq	marker_bit
	pxor	%xmm0, %xmm0
.LBB0_113:                              #   in Loop: Header=BB0_29 Depth=2
	testl	%r14d, %r14d
	movl	40(%rsp), %ebx          # 4-byte Reload
	jne	.LBB0_115
# BB#114:                               # %.thread.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	movl	$0, 84(%rsp)
	movq	$0, 76(%rsp)
	movdqa	.LCPI0_0(%rip), %xmm4   # xmm4 = [4294965248,4294965248,4294965248,4294965248]
	movdqa	.LCPI0_1(%rip), %xmm5   # xmm5 = [63488,63488,63488,63488]
	movdqa	.LCPI0_2(%rip), %xmm6   # xmm6 = [2048,2048,2048,2048]
	movdqa	.LCPI0_3(%rip), %xmm7   # xmm7 = [2047,2047,2047,2047]
	movl	36(%rsp), %r12d         # 4-byte Reload
	movl	12(%rsp), %ebp          # 4-byte Reload
	testb	$9, %r13b
	je	.LBB0_118
	jmp	.LBB0_122
.LBB0_115:                              #   in Loop: Header=BB0_29 Depth=2
	cmpl	$0, concealment_motion_vectors(%rip)
	movdqa	.LCPI0_0(%rip), %xmm4   # xmm4 = [4294965248,4294965248,4294965248,4294965248]
	movdqa	.LCPI0_1(%rip), %xmm5   # xmm5 = [63488,63488,63488,63488]
	movdqa	.LCPI0_2(%rip), %xmm6   # xmm6 = [2048,2048,2048,2048]
	movdqa	.LCPI0_3(%rip), %xmm7   # xmm7 = [2047,2047,2047,2047]
	movl	36(%rsp), %r12d         # 4-byte Reload
	movl	12(%rsp), %ebp          # 4-byte Reload
	jne	.LBB0_117
# BB#116:                               #   in Loop: Header=BB0_29 Depth=2
	movdqa	%xmm0, 112(%rsp)
	movdqa	%xmm0, 96(%rsp)
	.p2align	4, 0x90
.LBB0_117:                              #   in Loop: Header=BB0_29 Depth=2
	testb	$9, %r13b
	jne	.LBB0_122
.LBB0_118:                              #   in Loop: Header=BB0_29 Depth=2
	cmpl	$2, picture_coding_type(%rip)
	jne	.LBB0_122
# BB#119:                               #   in Loop: Header=BB0_29 Depth=2
	movl	$0, 116(%rsp)
	movl	$0, 112(%rsp)
	movq	$0, 96(%rsp)
	movl	picture_structure(%rip), %eax
	cmpl	$3, %eax
	jne	.LBB0_121
# BB#120:                               #   in Loop: Header=BB0_29 Depth=2
	movl	$2, 16(%rsp)
	cmpl	$4, 140(%rsp)
	je	.LBB0_123
	jmp	.LBB0_124
.LBB0_121:                              #   in Loop: Header=BB0_29 Depth=2
	movl	$1, 16(%rsp)
	xorl	%ecx, %ecx
	cmpl	$2, %eax
	sete	%cl
	movl	%ecx, 144(%rsp)
.LBB0_122:                              #   in Loop: Header=BB0_29 Depth=2
	cmpl	$4, 140(%rsp)
	jne	.LBB0_124
.LBB0_123:                              #   in Loop: Header=BB0_29 Depth=2
	movdqa	%xmm0, 112(%rsp)
	movdqa	%xmm0, 96(%rsp)
	.p2align	4, 0x90
.LBB0_124:                              #   in Loop: Header=BB0_29 Depth=2
	cmpl	$0, Two_Streams(%rip)
	je	.LBB0_168
.LBB0_125:                              #   in Loop: Header=BB0_29 Depth=2
	cmpl	$3, enhan+3148(%rip)
	jne	.LBB0_168
# BB#126:                               #   in Loop: Header=BB0_29 Depth=2
	movq	$enhan, ld(%rip)
	testl	%ebp, %ebp
	jne	.LBB0_137
# BB#127:                               #   in Loop: Header=BB0_29 Depth=2
	movl	$23, %edi
	callq	Show_Bits
	testl	%eax, %eax
	je	.LBB0_130
# BB#128:                               #   in Loop: Header=BB0_29 Depth=2
	cmpl	32(%rsp), %ebx          # 4-byte Folded Reload
	jge	.LBB0_133
# BB#129:                               #   in Loop: Header=BB0_29 Depth=2
	callq	Get_macroblock_address_increment
	movl	%eax, %ebp
	jmp	.LBB0_136
.LBB0_130:                              #   in Loop: Header=BB0_29 Depth=2
	callq	next_start_code
	movl	$32, %edi
	callq	Show_Bits
	movl	%eax, %r14d
	leal	-257(%r14), %eax
	cmpl	$175, %eax
	jb	.LBB0_135
# BB#131:                               #   in Loop: Header=BB0_29 Depth=2
	cmpl	$0, Quiet_Flag(%rip)
	je	.LBB0_164
# BB#132:                               #   in Loop: Header=BB0_29 Depth=2
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_166
.LBB0_133:                              #   in Loop: Header=BB0_29 Depth=2
	cmpl	$0, Quiet_Flag(%rip)
	pxor	%xmm0, %xmm0
	je	.LBB0_149
# BB#134:                               #   in Loop: Header=BB0_29 Depth=2
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_167
.LBB0_135:                              #   in Loop: Header=BB0_29 Depth=2
	callq	Flush_Buffer32
	callq	slice_header
	movl	%eax, %ebp
	callq	Get_macroblock_address_increment
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	shll	$7, %ebp
	movzbl	%r14b, %ecx
	leal	-1(%rcx,%rbp), %ecx
	imull	mb_width(%rip), %ecx
	leal	-1(%rax,%rcx), %ebx
	movl	$1, %ebp
.LBB0_136:                              # %.sink.split.i.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	pxor	%xmm0, %xmm0
	movdqa	.LCPI0_0(%rip), %xmm4   # xmm4 = [4294965248,4294965248,4294965248,4294965248]
	movdqa	.LCPI0_1(%rip), %xmm5   # xmm5 = [63488,63488,63488,63488]
	movdqa	.LCPI0_2(%rip), %xmm6   # xmm6 = [2048,2048,2048,2048]
	movdqa	.LCPI0_3(%rip), %xmm7   # xmm7 = [2047,2047,2047,2047]
.LBB0_137:                              # %.sink.split.i.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	cmpl	%r12d, %ebx
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	jne	.LBB0_144
# BB#138:                               #   in Loop: Header=BB0_29 Depth=2
	cmpl	$1, %ebp
	jne	.LBB0_146
# BB#139:                               #   in Loop: Header=BB0_29 Depth=2
	subq	$8, %rsp
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	leaq	32(%rsp), %rdi
	leaq	80(%rsp), %rsi
	movq	%rsi, %rdx
	movq	%rsi, %rcx
	movq	%rsi, %r8
	movq	%rsi, %r9
	leaq	28(%rsp), %rax
	pushq	%rax
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	%rsi
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	pushq	%rsi
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	callq	macroblock_modes
	addq	$32, %rsp
.Lcfi38:
	.cfi_adjust_cfa_offset -32
	movl	24(%rsp), %eax
	movl	%eax, %ebx
	andl	$2, %ebx
	je	.LBB0_141
# BB#140:                               #   in Loop: Header=BB0_29 Depth=2
	movl	20(%rsp), %ecx
	movl	%ecx, 64(%rsp)
.LBB0_141:                              #   in Loop: Header=BB0_29 Depth=2
	testb	$16, %al
	je	.LBB0_152
# BB#142:                               #   in Loop: Header=BB0_29 Depth=2
	movl	$5, %edi
	callq	Get_Bits
	movq	ld(%rip), %rcx
	cmpl	$0, 3152(%rcx)
	je	.LBB0_150
# BB#143:                               #   in Loop: Header=BB0_29 Depth=2
	cltq
	movzbl	Non_Linear_quantizer_scale(%rax), %eax
	jmp	.LBB0_151
.LBB0_144:                              #   in Loop: Header=BB0_29 Depth=2
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB0_168
# BB#145:                               #   in Loop: Header=BB0_29 Depth=2
	movl	$.Lstr.6, %edi
	callq	puts
	movdqa	.LCPI0_3(%rip), %xmm7   # xmm7 = [2047,2047,2047,2047]
	movdqa	.LCPI0_2(%rip), %xmm6   # xmm6 = [2048,2048,2048,2048]
	movdqa	.LCPI0_1(%rip), %xmm5   # xmm5 = [63488,63488,63488,63488]
	movdqa	.LCPI0_0(%rip), %xmm4   # xmm4 = [4294965248,4294965248,4294965248,4294965248]
	pxor	%xmm0, %xmm0
	jmp	.LBB0_168
.LBB0_146:                              # %.preheader.i12.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	cmpl	$0, block_count(%rip)
	jle	.LBB0_163
# BB#147:                               # %.lr.ph44.i.i.i.preheader
                                        #   in Loop: Header=BB0_29 Depth=2
	movl	$3176, %eax             # imm = 0xC68
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_148:                              # %.lr.ph44.i.i.i
                                        #   Parent Loop BB0_21 Depth=1
                                        #     Parent Loop BB0_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	ld(%rip), %rdx
	movdqu	%xmm0, 112(%rdx,%rax)
	movdqu	%xmm0, 96(%rdx,%rax)
	movdqu	%xmm0, 80(%rdx,%rax)
	movdqu	%xmm0, 64(%rdx,%rax)
	movdqu	%xmm0, 48(%rdx,%rax)
	movdqu	%xmm0, 32(%rdx,%rax)
	movdqu	%xmm0, 16(%rdx,%rax)
	movdqu	%xmm0, (%rdx,%rax)
	incq	%rcx
	movslq	block_count(%rip), %rdx
	subq	$-128, %rax
	cmpq	%rdx, %rcx
	jl	.LBB0_148
	jmp	.LBB0_163
.LBB0_149:                              #   in Loop: Header=BB0_29 Depth=2
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movl	$.Lstr.8, %edi
	jmp	.LBB0_165
.LBB0_150:                              #   in Loop: Header=BB0_29 Depth=2
	addl	%eax, %eax
.LBB0_151:                              #   in Loop: Header=BB0_29 Depth=2
	movl	%eax, 3168(%rcx)
.LBB0_152:                              #   in Loop: Header=BB0_29 Depth=2
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	je	.LBB0_158
# BB#153:                               #   in Loop: Header=BB0_29 Depth=2
	callq	Get_coded_block_pattern
	movl	%eax, %r14d
	movl	chroma_format(%rip), %eax
	cmpl	$3, %eax
	je	.LBB0_156
# BB#154:                               #   in Loop: Header=BB0_29 Depth=2
	cmpl	$2, %eax
	jne	.LBB0_158
# BB#155:                               #   in Loop: Header=BB0_29 Depth=2
	shll	$2, %r14d
	movl	$2, %edi
	jmp	.LBB0_157
.LBB0_156:                              #   in Loop: Header=BB0_29 Depth=2
	shll	$6, %r14d
	movl	$6, %edi
.LBB0_157:                              #   in Loop: Header=BB0_29 Depth=2
	callq	Get_Bits
	orl	%eax, %r14d
.LBB0_158:                              #   in Loop: Header=BB0_29 Depth=2
	cmpl	$0, block_count(%rip)
	pxor	%xmm0, %xmm0
	movdqa	.LCPI0_0(%rip), %xmm4   # xmm4 = [4294965248,4294965248,4294965248,4294965248]
	movdqa	.LCPI0_1(%rip), %xmm5   # xmm5 = [63488,63488,63488,63488]
	movdqa	.LCPI0_2(%rip), %xmm6   # xmm6 = [2048,2048,2048,2048]
	movdqa	.LCPI0_3(%rip), %xmm7   # xmm7 = [2047,2047,2047,2047]
	jle	.LBB0_163
# BB#159:                               # %.lr.ph.i16.i.i.preheader
                                        #   in Loop: Header=BB0_29 Depth=2
	movl	$-1, %r15d
	movl	$3176, %ebx             # imm = 0xC68
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_160:                              # %.lr.ph.i16.i.i
                                        #   Parent Loop BB0_21 Depth=1
                                        #     Parent Loop BB0_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	ld(%rip), %rax
	movdqu	%xmm0, 112(%rax,%rbx)
	movdqu	%xmm0, 96(%rax,%rbx)
	movdqu	%xmm0, 80(%rax,%rbx)
	movdqu	%xmm0, 64(%rax,%rbx)
	movdqu	%xmm0, 48(%rax,%rbx)
	movdqu	%xmm0, 32(%rax,%rbx)
	movdqu	%xmm0, 16(%rax,%rbx)
	movdqu	%xmm0, (%rax,%rbx)
	movl	block_count(%rip), %eax
	leal	(%r15,%rax), %ecx
	btl	%ecx, %r14d
	jae	.LBB0_162
# BB#161:                               #   in Loop: Header=BB0_160 Depth=3
	movl	%ebp, %edi
	callq	Decode_MPEG2_Non_Intra_Block
	movdqa	.LCPI0_3(%rip), %xmm7   # xmm7 = [2047,2047,2047,2047]
	movdqa	.LCPI0_2(%rip), %xmm6   # xmm6 = [2048,2048,2048,2048]
	movdqa	.LCPI0_1(%rip), %xmm5   # xmm5 = [63488,63488,63488,63488]
	movdqa	.LCPI0_0(%rip), %xmm4   # xmm4 = [4294965248,4294965248,4294965248,4294965248]
	pxor	%xmm0, %xmm0
	movl	block_count(%rip), %eax
.LBB0_162:                              #   in Loop: Header=BB0_160 Depth=3
	incq	%rbp
	cltq
	subq	$-128, %rbx
	decl	%r15d
	cmpq	%rax, %rbp
	jl	.LBB0_160
.LBB0_163:                              # %.loopexit.i.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	movq	$base, ld(%rip)
	movl	%r12d, %ebx
	jmp	.LBB0_168
.LBB0_164:                              #   in Loop: Header=BB0_29 Depth=2
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movl	$.Lstr.7, %edi
.LBB0_165:                              # %Decode_SNR_Macroblock.exit.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	callq	puts
.LBB0_166:                              # %Decode_SNR_Macroblock.exit.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	pxor	%xmm0, %xmm0
.LBB0_167:                              # %Decode_SNR_Macroblock.exit.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	movdqa	.LCPI0_0(%rip), %xmm4   # xmm4 = [4294965248,4294965248,4294965248,4294965248]
	movdqa	.LCPI0_1(%rip), %xmm5   # xmm5 = [63488,63488,63488,63488]
	movdqa	.LCPI0_2(%rip), %xmm6   # xmm6 = [2048,2048,2048,2048]
	movdqa	.LCPI0_3(%rip), %xmm7   # xmm7 = [2047,2047,2047,2047]
	.p2align	4, 0x90
.LBB0_168:                              #   in Loop: Header=BB0_29 Depth=2
	movl	%ebx, 40(%rsp)          # 4-byte Spill
	movl	64(%rsp), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	movl	%r12d, %eax
	cltd
	idivl	mb_width(%rip)
	movl	%eax, %r12d
	shll	$4, %edx
	movl	%edx, 44(%rsp)          # 4-byte Spill
	shll	$4, %r12d
	movl	%r13d, %r14d
	andl	$1, %r14d
	jne	.LBB0_170
# BB#169:                               #   in Loop: Header=BB0_29 Depth=2
	movl	60(%rsp), %eax
	movl	16(%rsp), %ecx
	movl	44(%rsp), %edi          # 4-byte Reload
	movl	%r12d, %esi
	movl	%r13d, %edx
	leaq	96(%rsp), %r8
	leaq	144(%rsp), %r9
	pushq	%rax
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	leaq	96(%rsp), %rax
	pushq	%rax
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	callq	form_predictions
	movdqa	.LCPI0_3(%rip), %xmm7   # xmm7 = [2047,2047,2047,2047]
	movdqa	.LCPI0_2(%rip), %xmm6   # xmm6 = [2048,2048,2048,2048]
	movdqa	.LCPI0_1(%rip), %xmm5   # xmm5 = [63488,63488,63488,63488]
	movdqa	.LCPI0_0(%rip), %xmm4   # xmm4 = [4294965248,4294965248,4294965248,4294965248]
	pxor	%xmm0, %xmm0
	addq	$16, %rsp
.Lcfi41:
	.cfi_adjust_cfa_offset -16
.LBB0_170:                              #   in Loop: Header=BB0_29 Depth=2
	cmpl	$1, base+3148(%rip)
	jne	.LBB0_172
# BB#171:                               #   in Loop: Header=BB0_29 Depth=2
	movq	$base, ld(%rip)
.LBB0_172:                              # %.preheader.i19.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	cmpl	$0, block_count(%rip)
	jle	.LBB0_206
# BB#173:                               # %.lr.ph.i20.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	movslq	44(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 128(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_174:                              #   Parent Loop BB0_21 Depth=1
                                        #     Parent Loop BB0_29 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_181 Depth 4
                                        #         Child Loop BB0_204 Depth 4
                                        #         Child Loop BB0_202 Depth 4
	movl	Two_Streams(%rip), %ecx
	movl	enhan+3148(%rip), %edx
	testl	%ecx, %ecx
	je	.LBB0_177
# BB#175:                               #   in Loop: Header=BB0_174 Depth=3
	cmpl	$3, %edx
	jne	.LBB0_177
# BB#176:                               # %vector.body31
                                        #   in Loop: Header=BB0_174 Depth=3
	movq	%r15, %rax
	shlq	$7, %rax
	movdqu	enhan+3176(%rax), %xmm0
	movdqu	base+3176(%rax), %xmm1
	paddw	%xmm0, %xmm1
	movdqu	%xmm1, base+3176(%rax)
	movdqu	enhan+3192(%rax), %xmm0
	movdqu	base+3192(%rax), %xmm1
	paddw	%xmm0, %xmm1
	movdqu	%xmm1, base+3192(%rax)
	movdqu	enhan+3208(%rax), %xmm0
	movdqu	base+3208(%rax), %xmm1
	paddw	%xmm0, %xmm1
	movdqu	%xmm1, base+3208(%rax)
	movdqu	enhan+3224(%rax), %xmm0
	movdqu	base+3224(%rax), %xmm1
	paddw	%xmm0, %xmm1
	movdqu	%xmm1, base+3224(%rax)
	movdqu	enhan+3240(%rax), %xmm0
	movdqu	base+3240(%rax), %xmm1
	paddw	%xmm0, %xmm1
	movdqu	%xmm1, base+3240(%rax)
	movdqu	enhan+3256(%rax), %xmm0
	movdqu	base+3256(%rax), %xmm1
	paddw	%xmm0, %xmm1
	movdqu	%xmm1, base+3256(%rax)
	movdqu	enhan+3272(%rax), %xmm0
	movdqu	base+3272(%rax), %xmm1
	paddw	%xmm0, %xmm1
	movdqu	%xmm1, base+3272(%rax)
	movdqu	enhan+3288(%rax), %xmm0
	movdqu	base+3288(%rax), %xmm1
	paddw	%xmm0, %xmm1
	movdqu	%xmm1, base+3288(%rax)
	movl	enhan+3148(%rip), %edx
.LBB0_177:                              # %Sum_Block.exit.i.i.i
                                        #   in Loop: Header=BB0_174 Depth=3
	movq	ld(%rip), %rax
	testl	%ecx, %ecx
	je	.LBB0_179
# BB#178:                               # %Sum_Block.exit.i.i.i
                                        #   in Loop: Header=BB0_174 Depth=3
	cmpl	$3, %edx
	je	.LBB0_180
.LBB0_179:                              #   in Loop: Header=BB0_174 Depth=3
	cmpl	$0, 3144(%rax)
	je	.LBB0_184
.LBB0_180:                              # %vector.body.preheader
                                        #   in Loop: Header=BB0_174 Depth=3
	movq	%r15, %rcx
	shlq	$7, %rcx
	leaq	3184(%rax,%rcx), %rdx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_181:                              # %vector.body
                                        #   Parent Loop BB0_21 Depth=1
                                        #     Parent Loop BB0_29 Depth=2
                                        #       Parent Loop BB0_174 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	-8(%rdx,%rsi,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1],xmm2[2],xmm1[2],xmm2[3],xmm1[3]
	psrad	$16, %xmm2
	movdqa	%xmm2, %xmm3
	pcmpgtd	%xmm4, %xmm3
	movdqa	%xmm6, %xmm1
	pcmpgtd	%xmm2, %xmm1
	pand	%xmm3, %xmm2
	pandn	%xmm5, %xmm3
	por	%xmm2, %xmm3
	pslld	$16, %xmm3
	psrad	$16, %xmm3
	pand	%xmm1, %xmm3
	pandn	%xmm7, %xmm1
	por	%xmm3, %xmm1
	pshuflw	$232, %xmm1, %xmm2      # xmm2 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	movq	%xmm2, -8(%rdx,%rsi,2)
	paddd	%xmm0, %xmm1
	movq	(%rdx,%rsi,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psrad	$16, %xmm2
	movdqa	%xmm2, %xmm3
	pcmpgtd	%xmm4, %xmm3
	movdqa	%xmm6, %xmm0
	pcmpgtd	%xmm2, %xmm0
	pand	%xmm3, %xmm2
	pandn	%xmm5, %xmm3
	por	%xmm2, %xmm3
	pslld	$16, %xmm3
	psrad	$16, %xmm3
	pand	%xmm0, %xmm3
	pandn	%xmm7, %xmm0
	por	%xmm3, %xmm0
	pshuflw	$232, %xmm0, %xmm2      # xmm2 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	movq	%xmm2, (%rdx,%rsi,2)
	paddd	%xmm1, %xmm0
	addq	$8, %rsi
	cmpq	$64, %rsi
	jne	.LBB0_181
# BB#182:                               # %middle.block
                                        #   in Loop: Header=BB0_174 Depth=3
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %edx
	testb	$1, %dl
	jne	.LBB0_184
# BB#183:                               #   in Loop: Header=BB0_174 Depth=3
	xorb	$1, 3302(%rax,%rcx)
.LBB0_184:                              # %Saturate.exit.i.i.i
                                        #   in Loop: Header=BB0_174 Depth=3
	movq	%r15, %r13
	shlq	$7, %r13
	cmpl	$0, Reference_IDCT_Flag(%rip)
	leaq	3176(%rax,%r13), %rdi
	je	.LBB0_186
# BB#185:                               #   in Loop: Header=BB0_174 Depth=3
	callq	Reference_IDCT
	jmp	.LBB0_187
	.p2align	4, 0x90
.LBB0_186:                              #   in Loop: Header=BB0_174 Depth=3
	callq	Fast_IDCT
.LBB0_187:                              #   in Loop: Header=BB0_174 Depth=3
	movl	%r15d, %esi
	andl	$1, %esi
	incl	%esi
	cmpq	$4, %r15
	movl	$0, %eax
	cmovll	%eax, %esi
	cmpq	$3, %r15
	jg	.LBB0_191
# BB#188:                               #   in Loop: Header=BB0_174 Depth=3
	movq	current_frame(%rip), %rax
	movl	Coded_Picture_Width(%rip), %ecx
	cmpl	$3, picture_structure(%rip)
	jne	.LBB0_195
# BB#189:                               #   in Loop: Header=BB0_174 Depth=3
	movl	%r15d, %edx
	andl	$2, %edx
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	pxor	%xmm0, %xmm0
	movdqa	.LCPI0_0(%rip), %xmm4   # xmm4 = [4294965248,4294965248,4294965248,4294965248]
	movdqa	.LCPI0_1(%rip), %xmm5   # xmm5 = [63488,63488,63488,63488]
	movdqa	.LCPI0_2(%rip), %xmm6   # xmm6 = [2048,2048,2048,2048]
	movdqa	.LCPI0_3(%rip), %xmm7   # xmm7 = [2047,2047,2047,2047]
	je	.LBB0_199
# BB#190:                               #   in Loop: Header=BB0_174 Depth=3
	shrl	%edx
	orl	%r12d, %edx
	imull	%ecx, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rax
	addq	128(%rsp), %rax         # 8-byte Folded Reload
	movl	%r15d, %edx
	andl	$1, %edx
	leaq	(%rax,%rdx,8), %rax
	addl	%ecx, %ecx
	jmp	.LBB0_200
	.p2align	4, 0x90
.LBB0_191:                              #   in Loop: Header=BB0_174 Depth=3
	movl	chroma_format(%rip), %edi
	cmpl	$3, %edi
	setne	%cl
	movl	44(%rsp), %eax          # 4-byte Reload
	sarl	%cl, %eax
	cmpl	$1, %edi
	sete	%cl
	movl	%r12d, %edx
	sarl	%cl, %edx
	cmpl	$3, picture_structure(%rip)
	jne	.LBB0_196
# BB#192:                               #   in Loop: Header=BB0_174 Depth=3
	movl	%esi, %ecx
	movq	current_frame(,%rcx,8), %rsi
	movl	Chroma_Width(%rip), %ecx
	movl	%r15d, %ebp
	andl	$2, %ebp
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	pxor	%xmm0, %xmm0
	movdqa	.LCPI0_0(%rip), %xmm4   # xmm4 = [4294965248,4294965248,4294965248,4294965248]
	movdqa	.LCPI0_1(%rip), %xmm5   # xmm5 = [63488,63488,63488,63488]
	movdqa	.LCPI0_2(%rip), %xmm6   # xmm6 = [2048,2048,2048,2048]
	movdqa	.LCPI0_3(%rip), %xmm7   # xmm7 = [2047,2047,2047,2047]
	je	.LBB0_198
# BB#193:                               #   in Loop: Header=BB0_174 Depth=3
	cmpl	$1, %edi
	je	.LBB0_198
# BB#194:                               #   in Loop: Header=BB0_174 Depth=3
	shrl	%ebp
	orl	%ebp, %edx
	imull	%ecx, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rsi
	movslq	%eax, %rdx
	addq	%rsi, %rdx
	movl	%r15d, %eax
	andl	$8, %eax
	addq	%rdx, %rax
	addl	%ecx, %ecx
	jmp	.LBB0_200
	.p2align	4, 0x90
.LBB0_195:                              #   in Loop: Header=BB0_174 Depth=3
	addl	%ecx, %ecx
	leal	(,%r15,4), %edx
	andl	$8, %edx
	orl	%r12d, %edx
	imull	%ecx, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rax
	addq	128(%rsp), %rax         # 8-byte Folded Reload
	movl	%r15d, %edx
	andl	$1, %edx
	leaq	(%rax,%rdx,8), %rax
	jmp	.LBB0_197
	.p2align	4, 0x90
.LBB0_196:                              #   in Loop: Header=BB0_174 Depth=3
	movl	%esi, %esi
	movl	Chroma_Width(%rip), %ecx
	addl	%ecx, %ecx
	movl	%r15d, %edi
	andl	$2, %edi
	leal	(%rdx,%rdi,4), %edx
	imull	%ecx, %edx
	movslq	%edx, %rdx
	addq	current_frame(,%rsi,8), %rdx
	movslq	%eax, %rsi
	addq	%rdx, %rsi
	movl	%r15d, %eax
	andl	$8, %eax
	addq	%rsi, %rax
.LBB0_197:                              #   in Loop: Header=BB0_174 Depth=3
	pxor	%xmm0, %xmm0
	movdqa	.LCPI0_0(%rip), %xmm4   # xmm4 = [4294965248,4294965248,4294965248,4294965248]
	movdqa	.LCPI0_1(%rip), %xmm5   # xmm5 = [63488,63488,63488,63488]
	movdqa	.LCPI0_2(%rip), %xmm6   # xmm6 = [2048,2048,2048,2048]
	movdqa	.LCPI0_3(%rip), %xmm7   # xmm7 = [2047,2047,2047,2047]
	jmp	.LBB0_200
.LBB0_198:                              #   in Loop: Header=BB0_174 Depth=3
	leal	(%rdx,%rbp,4), %edx
	imull	%ecx, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rsi
	movslq	%eax, %rdx
	addq	%rsi, %rdx
	movl	%r15d, %eax
	andl	$8, %eax
	addq	%rdx, %rax
	jmp	.LBB0_200
.LBB0_199:                              #   in Loop: Header=BB0_174 Depth=3
	shll	$2, %edx
	orl	%r12d, %edx
	imull	%ecx, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rax
	addq	128(%rsp), %rax         # 8-byte Folded Reload
	movl	%r15d, %edx
	andl	$1, %edx
	leaq	(%rax,%rdx,8), %rax
	.p2align	4, 0x90
.LBB0_200:                              #   in Loop: Header=BB0_174 Depth=3
	addl	$-8, %ecx
	testl	%r14d, %r14d
	movq	ld(%rip), %rdx
	movslq	%ecx, %rcx
	leaq	3176(%rdx,%r13), %rdx
	jne	.LBB0_203
# BB#201:                               # %.preheader72.i.i.i.i.preheader
                                        #   in Loop: Header=BB0_174 Depth=3
	addq	$7, %rax
	addq	$8, %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_202:                              # %.preheader72.i.i.i.i
                                        #   Parent Loop BB0_21 Depth=1
                                        #     Parent Loop BB0_29 Depth=2
                                        #       Parent Loop BB0_174 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	Clip(%rip), %rdi
	movswq	(%rdx,%rsi), %rbp
	movzbl	-7(%rax), %ebx
	addq	%rbp, %rbx
	movzbl	(%rdi,%rbx), %ebx
	movb	%bl, -7(%rax)
	movq	Clip(%rip), %rdi
	movswq	2(%rdx,%rsi), %rbp
	movzbl	-6(%rax), %ebx
	addq	%rbp, %rbx
	movzbl	(%rdi,%rbx), %ebx
	movb	%bl, -6(%rax)
	movq	Clip(%rip), %rdi
	movswq	4(%rdx,%rsi), %rbp
	movzbl	-5(%rax), %ebx
	addq	%rbp, %rbx
	movzbl	(%rdi,%rbx), %ebx
	movb	%bl, -5(%rax)
	movq	Clip(%rip), %rdi
	movswq	6(%rdx,%rsi), %rbp
	movzbl	-4(%rax), %ebx
	addq	%rbp, %rbx
	movzbl	(%rdi,%rbx), %ebx
	movb	%bl, -4(%rax)
	movq	Clip(%rip), %rdi
	movswq	8(%rdx,%rsi), %rbp
	movzbl	-3(%rax), %ebx
	addq	%rbp, %rbx
	movzbl	(%rdi,%rbx), %ebx
	movb	%bl, -3(%rax)
	movq	Clip(%rip), %rdi
	movswq	10(%rdx,%rsi), %rbp
	movzbl	-2(%rax), %ebx
	addq	%rbp, %rbx
	movzbl	(%rdi,%rbx), %ebx
	movb	%bl, -2(%rax)
	movq	Clip(%rip), %rdi
	movswq	12(%rdx,%rsi), %rbp
	movzbl	-1(%rax), %ebx
	addq	%rbp, %rbx
	movzbl	(%rdi,%rbx), %ebx
	movb	%bl, -1(%rax)
	movq	Clip(%rip), %rdi
	movswq	14(%rdx,%rsi), %rbp
	movzbl	(%rax), %ebx
	addq	%rbp, %rbx
	movzbl	(%rdi,%rbx), %ebx
	movb	%bl, (%rax)
	addq	$16, %rsi
	addq	%rcx, %rax
	cmpl	$128, %esi
	jne	.LBB0_202
	jmp	.LBB0_205
	.p2align	4, 0x90
.LBB0_203:                              # %.preheader.i.i.i.i.preheader
                                        #   in Loop: Header=BB0_174 Depth=3
	addq	$7, %rax
	addq	$8, %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_204:                              # %.preheader.i.i.i.i
                                        #   Parent Loop BB0_21 Depth=1
                                        #     Parent Loop BB0_29 Depth=2
                                        #       Parent Loop BB0_174 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	Clip(%rip), %rdi
	movswq	(%rdx,%rsi), %rbp
	movzbl	128(%rdi,%rbp), %ebx
	movb	%bl, -7(%rax)
	movq	Clip(%rip), %rdi
	movswq	2(%rdx,%rsi), %rbp
	movzbl	128(%rdi,%rbp), %ebx
	movb	%bl, -6(%rax)
	movq	Clip(%rip), %rdi
	movswq	4(%rdx,%rsi), %rbp
	movzbl	128(%rdi,%rbp), %ebx
	movb	%bl, -5(%rax)
	movq	Clip(%rip), %rdi
	movswq	6(%rdx,%rsi), %rbp
	movzbl	128(%rdi,%rbp), %ebx
	movb	%bl, -4(%rax)
	movq	Clip(%rip), %rdi
	movswq	8(%rdx,%rsi), %rbp
	movzbl	128(%rdi,%rbp), %ebx
	movb	%bl, -3(%rax)
	movq	Clip(%rip), %rdi
	movswq	10(%rdx,%rsi), %rbp
	movzbl	128(%rdi,%rbp), %ebx
	movb	%bl, -2(%rax)
	movq	Clip(%rip), %rdi
	movswq	12(%rdx,%rsi), %rbp
	movzbl	128(%rdi,%rbp), %ebx
	movb	%bl, -1(%rax)
	movq	Clip(%rip), %rdi
	movswq	14(%rdx,%rsi), %rbp
	movzbl	128(%rdi,%rbp), %ebx
	movb	%bl, (%rax)
	addq	$16, %rsi
	addq	%rcx, %rax
	cmpl	$128, %esi
	jne	.LBB0_204
.LBB0_205:                              # %Add_Block.exit.i.i.i
                                        #   in Loop: Header=BB0_174 Depth=3
	incq	%r15
	movslq	block_count(%rip), %rax
	cmpq	%rax, %r15
	jl	.LBB0_174
.LBB0_206:                              # %motion_compensation.exit.i.i
                                        #   in Loop: Header=BB0_29 Depth=2
	movl	36(%rsp), %r12d         # 4-byte Reload
	incl	%r12d
	movl	136(%rsp), %eax         # 4-byte Reload
	decl	%eax
	cmpl	$0, Two_Streams(%rip)
	setne	%dl
	cmpl	$3, enhan+3148(%rip)
	sete	%cl
	andb	%dl, %cl
	movzbl	%cl, %ecx
	movl	40(%rsp), %ebx          # 4-byte Reload
	addl	%ecx, %ebx
	movl	12(%rsp), %ebp          # 4-byte Reload
	subl	%ecx, %ebp
	cmpl	32(%rsp), %r12d         # 4-byte Folded Reload
	jl	.LBB0_29
	jmp	.LBB0_210
	.p2align	4, 0x90
.LBB0_207:                              # %.loopexit.i.i
                                        #   in Loop: Header=BB0_21 Depth=1
	movl	$0, Fault_Flag(%rip)
.LBB0_20:                               # %.backedge.i
                                        #   in Loop: Header=BB0_21 Depth=1
	movq	$base, ld(%rip)
	movl	$0, Fault_Flag(%rip)
	callq	next_start_code
	movl	$32, %edi
	callq	Show_Bits
	movl	%eax, %ebx
	addl	$-257, %eax             # imm = 0xFEFF
	cmpl	$174, %eax
	jbe	.LBB0_21
.LBB0_208:                              # %._crit_edge.i
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB0_210
# BB#209:                               # %.critedge4.i
	movl	$.Lstr.5, %edi
	callq	puts
.LBB0_210:                              # %picture_data.exit
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	je	.LBB0_218
.LBB0_211:
	movl	picture_structure(%rip), %eax
	cmpl	$3, %eax
	je	.LBB0_213
# BB#212:
	movl	Second_Field(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB0_221
.LBB0_213:
	cmpl	$3, picture_coding_type(%rip)
	jne	.LBB0_215
# BB#214:
	movl	28(%rsp), %esi          # 4-byte Reload
	decl	%esi
	movl	$auxframe, %edi
	callq	Write_Frame
	jmp	.LBB0_220
.LBB0_215:
	movl	progressive_frame(%rip), %eax
	movl	%eax, frame_reorder.Newref_progressive_frame(%rip)
	movl	frame_reorder.Oldref_progressive_frame(%rip), %eax
	movl	%eax, progressive_frame(%rip)
	movl	28(%rsp), %esi          # 4-byte Reload
	decl	%esi
	movl	$forward_reference_frame, %edi
	callq	Write_Frame
	movl	frame_reorder.Newref_progressive_frame(%rip), %eax
	movl	%eax, progressive_frame(%rip)
	jmp	.LBB0_219
.LBB0_216:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB0_210
# BB#217:                               # %.critedge2.i
	movl	$.Lstr.4, %edi
	callq	puts
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_211
.LBB0_218:
	movl	progressive_frame(%rip), %eax
.LBB0_219:                              # %frame_reorder.exitthread-pre-split
	movl	%eax, frame_reorder.Oldref_progressive_frame(%rip)
.LBB0_220:                              # %frame_reorder.exitthread-pre-split
	movl	picture_structure(%rip), %eax
.LBB0_221:                              # %frame_reorder.exit
	cmpl	$3, %eax
	je	.LBB0_223
# BB#222:
	xorl	%eax, %eax
	cmpl	$0, Second_Field(%rip)
	sete	%al
	movl	%eax, Second_Field(%rip)
.LBB0_223:
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Decode_Picture, .Lfunc_end0-Decode_Picture
	.cfi_endproc

	.globl	Output_Last_Frame_of_Sequence
	.p2align	4, 0x90
	.type	Output_Last_Frame_of_Sequence,@function
Output_Last_Frame_of_Sequence:          # @Output_Last_Frame_of_Sequence
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	cmpl	$0, Second_Field(%rip)
	je	.LBB1_2
# BB#1:
	movl	$.Lstr.1, %edi
	jmp	puts                    # TAILCALL
.LBB1_2:
	decl	%eax
	movl	$backward_reference_frame, %edi
	movl	%eax, %esi
	jmp	Write_Frame             # TAILCALL
.Lfunc_end1:
	.size	Output_Last_Frame_of_Sequence, .Lfunc_end1-Output_Last_Frame_of_Sequence
	.cfi_endproc

	.p2align	4, 0x90
	.type	macroblock_modes,@function
macroblock_modes:                       # @macroblock_modes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 112
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %rbp
	movq	%rcx, %rbx
	movq	%rdx, %r13
	movq	%rsi, %r14
	movq	%rdi, %r12
	callq	Get_macroblock_type
	movl	%eax, %r8d
	cmpl	$0, Fault_Flag(%rip)
	jne	.LBB2_22
# BB#1:
	movq	%r12, (%rsp)            # 8-byte Spill
	testb	$32, %r8b
	jne	.LBB2_2
# BB#5:
	movl	%r8d, %r10d
	shrl	$3, %r10d
	andl	$8, %r10d
	jmp	.LBB2_6
.LBB2_2:
	cmpl	$0, spatial_temporal_weight_code_table_index(%rip)
	je	.LBB2_3
# BB#4:
	movl	$2, %edi
	movl	%r8d, %r12d
	callq	Get_Bits
	movl	%r12d, %r8d
	movslq	spatial_temporal_weight_code_table_index(%rip), %rcx
	cltq
	movzbl	macroblock_modes.stwc_table-4(%rax,%rcx,4), %r10d
	jmp	.LBB2_6
.LBB2_3:
	movl	$4, %r10d
.LBB2_6:
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%r13, %rsi
	movq	%r14, %r13
	testb	$12, %r8b
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	je	.LBB2_11
# BB#7:
	cmpl	$3, picture_structure(%rip)
	jne	.LBB2_9
# BB#8:
	movl	$2, %eax
	cmpl	$0, frame_pred_frame_dct(%rip)
	jne	.LBB2_10
.LBB2_9:
	movq	%r10, %r14
	movl	$2, %edi
	movl	%r8d, %ebp
	movq	%rsi, %rbx
	callq	Get_Bits
	movq	%rbx, %rsi
	movl	%ebp, %r8d
	movq	%r14, %r10
	movq	(%rsp), %rdx            # 8-byte Reload
	jmp	.LBB2_10
.LBB2_11:
	xorl	%eax, %eax
	testb	$1, %r8b
	je	.LBB2_10
# BB#12:
	movl	concealment_motion_vectors(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB2_10
# BB#13:
	movl	picture_structure(%rip), %r12d
	xorl	%eax, %eax
	cmpl	$3, %r12d
	sete	%al
	incl	%eax
	jmp	.LBB2_14
.LBB2_10:                               # %thread-pre-split
	movl	picture_structure(%rip), %r12d
.LBB2_14:
	cmpl	$3, %r12d
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %r14
	jne	.LBB2_16
# BB#15:
	xorl	%edi, %edi
	cmpl	$1, %eax
	sete	%dil
	incl	%edi
	movl	$27, %edx
	btq	%r10, %rdx
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	$1, %r13d
	cmovbl	%edi, %r13d
	xorl	%r15d, %r15d
	cmpl	$2, %eax
	sete	%r15b
	jmp	.LBB2_17
.LBB2_16:
	xorl	%r13d, %r13d
	cmpl	$2, %eax
	sete	%r13b
	incl	%r13d
	xorl	%r15d, %r15d
.LBB2_17:
	movq	128(%rsp), %r9
	movq	120(%rsp), %rdi
	movq	112(%rsp), %r11
	movzbl	macroblock_modes.stwclass_table(%r10), %ebx
	cmpl	$3, %r12d
	sete	%cl
	xorl	%ebp, %ebp
	cmpl	$3, %eax
	sete	%bpl
	testl	%r15d, %r15d
	sete	%dl
	andb	%cl, %dl
	movzbl	%dl, %esi
	xorl	%eax, %eax
	testb	$3, %r8b
	je	.LBB2_21
# BB#18:
	cmpl	$3, %r12d
	jne	.LBB2_21
# BB#19:
	movl	frame_pred_frame_dct(%rip), %ecx
	testl	%ecx, %ecx
	jne	.LBB2_21
# BB#20:
	movq	%r10, %r12
	movl	$1, %edi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	%r8d, %r14d
	callq	Get_Bits
	movq	112(%rsp), %r11
	movq	120(%rsp), %rdi
	movq	128(%rsp), %r9
	movl	%r14d, %r8d
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	%r12, %r10
	movq	(%rsp), %r14            # 8-byte Reload
.LBB2_21:
	movl	%r8d, (%r14)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%r10d, (%rcx)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%ebx, (%rcx)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	%edx, (%rcx)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%r13d, (%rcx)
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	%r15d, (%rcx)
	movl	%ebp, (%r11)
	movl	%esi, (%rdi)
	movl	%eax, (%r9)
.LBB2_22:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	macroblock_modes, .Lfunc_end2-macroblock_modes
	.cfi_endproc

	.type	.L.str.6,@object        # @.str.6
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.6:
	.asciz	"D picture end_of_macroblock bit"
	.size	.L.str.6, 32

	.type	macroblock_modes.stwc_table,@object # @macroblock_modes.stwc_table
	.section	.rodata,"a",@progbits
macroblock_modes.stwc_table:
	.ascii	"\006\003\007\004"
	.ascii	"\002\001\005\004"
	.ascii	"\002\005\007\004"
	.size	macroblock_modes.stwc_table, 12

	.type	macroblock_modes.stwclass_table,@object # @macroblock_modes.stwclass_table
macroblock_modes.stwclass_table:
	.ascii	"\000\001\002\001\001\002\003\003\004"
	.size	macroblock_modes.stwclass_table, 9

	.type	frame_reorder.Oldref_progressive_frame,@object # @frame_reorder.Oldref_progressive_frame
	.local	frame_reorder.Oldref_progressive_frame
	.comm	frame_reorder.Oldref_progressive_frame,4,4
	.type	frame_reorder.Newref_progressive_frame,@object # @frame_reorder.Newref_progressive_frame
	.local	frame_reorder.Newref_progressive_frame
	.comm	frame_reorder.Newref_progressive_frame,4,4
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"odd number of field pictures"
	.size	.Lstr, 29

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"last frame incomplete, not stored"
	.size	.Lstr.1, 34

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"start_of_slice(): MBAinc unsuccessful"
	.size	.Lstr.3, 38

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"DP: Premature end of picture"
	.size	.Lstr.4, 29

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"start_of_slice(): Premature end of picture"
	.size	.Lstr.5, 43

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	"Cant't synchronize streams"
	.size	.Lstr.6, 27

	.type	.Lstr.7,@object         # @str.7
	.p2align	4
.Lstr.7:
	.asciz	"SNR: Premature end of picture"
	.size	.Lstr.7, 30

	.type	.Lstr.8,@object         # @str.8
	.p2align	4
.Lstr.8:
	.asciz	"Too many macroblocks in picture"
	.size	.Lstr.8, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
