	.text
	.file	"z19.bc"
	.globl	DetachGalley
	.p2align	4, 0x90
	.type	DetachGalley,@function
DetachGalley:                           # @DetachGalley
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	cmpb	$8, 32(%r14)
	jne	.LBB0_2
# BB#1:
	movq	24(%r14), %rbx
	cmpq	%r14, %rbx
	jne	.LBB0_3
.LBB0_2:                                # %._crit_edge31
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	24(%r14), %rbx
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB0_3
# BB#4:
	cmpq	%rbx, 24(%rbx)
	jne	.LBB0_6
# BB#5:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_6:
	movzbl	zz_lengths+120(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB0_7
# BB#8:
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_9
.LBB0_7:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB0_9:
	movb	$120, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	movq	$0, 88(%r15)
	movq	24(%r14), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_11
# BB#10:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_11:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB0_14
# BB#12:
	testq	%rax, %rax
	je	.LBB0_14
# BB#13:
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB0_14:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_15
# BB#16:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_17
.LBB0_15:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_17:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	24(%rbx), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB0_20
# BB#18:
	testq	%rax, %rax
	je	.LBB0_20
# BB#19:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_20:
	testq	%r15, %r15
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	je	.LBB0_23
# BB#21:
	testq	%rax, %rax
	je	.LBB0_23
# BB#22:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_23:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	DetachGalley, .Lfunc_end0-DetachGalley
	.cfi_endproc

	.globl	SearchGalley
	.p2align	4, 0x90
	.type	SearchGalley,@function
SearchGalley:                           # @SearchGalley
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 64
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r8d, %r13d
	movl	%ecx, %r15d
	movl	%edx, %ebx
	movq	%rsi, %r12
	movq	%rdi, %rbp
	movb	32(%rbp), %al
	orb	$8, %al
	cmpb	$8, %al
	je	.LBB1_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_2:
	leaq	8(%rbp), %rax
	testl	%ebx, %ebx
	cmoveq	%rbp, %rax
	testl	%r13d, %r13d
	movq	(%rax), %r13
	movb	32(%r13), %al
	movl	%ebx, %edx
	movl	%edx, (%rsp)            # 4-byte Spill
	je	.LBB1_33
# BB#3:                                 # %.split.preheader
	cmpb	$8, %al
	je	.LBB1_58
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph121
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_6 Depth 2
                                        #     Child Loop BB1_13 Depth 2
                                        #       Child Loop BB1_14 Depth 3
	leaq	16(%r13), %rbp
	jmp	.LBB1_6
	.p2align	4, 0x90
.LBB1_5:                                #   in Loop: Header=BB1_6 Depth=2
	addq	$16, %rbp
.LBB1_6:                                #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	cmpb	$119, %al
	jg	.LBB1_8
# BB#7:                                 #   in Loop: Header=BB1_6 Depth=2
	testb	%al, %al
	je	.LBB1_5
	jmp	.LBB1_29
	.p2align	4, 0x90
.LBB1_8:                                #   in Loop: Header=BB1_4 Depth=1
	cmpb	$120, %al
	je	.LBB1_11
# BB#9:                                 #   in Loop: Header=BB1_4 Depth=1
	cmpb	$121, %al
	je	.LBB1_17
# BB#10:                                #   in Loop: Header=BB1_4 Depth=1
	cmpb	$122, %al
	jne	.LBB1_29
.LBB1_11:                               #   in Loop: Header=BB1_4 Depth=1
	testl	%r15d, %r15d
	je	.LBB1_21
# BB#12:                                # %.preheader51
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	8(%rbp), %rbx
	cmpq	%rbp, %rbx
	je	.LBB1_23
	.p2align	4, 0x90
.LBB1_13:                               # %.preheader
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_14 Depth 3
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB1_14:                               #   Parent Loop BB1_4 Depth=1
                                        #     Parent Loop BB1_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB1_14
# BB#15:                                #   in Loop: Header=BB1_13 Depth=2
	movl	$1, %edx
	movl	$1, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	movl	%r14d, %r9d
	callq	SearchGalley
	testq	%rax, %rax
	jne	.LBB1_24
# BB#16:                                #   in Loop: Header=BB1_13 Depth=2
	movq	8(%rbx), %rbx
	cmpq	%rbp, %rbx
	jne	.LBB1_13
	jmp	.LBB1_24
	.p2align	4, 0x90
.LBB1_17:                               #   in Loop: Header=BB1_4 Depth=1
	movq	80(%rbp), %rax
	cmpb	$2, 32(%rax)
	jne	.LBB1_22
# BB#18:                                #   in Loop: Header=BB1_4 Depth=1
	movq	80(%rax), %rdi
	movq	%r12, %rsi
	callq	SearchUses
	testl	%eax, %eax
	movl	$0, %eax
	cmovneq	%rbp, %rax
	jne	.LBB1_30
# BB#19:                                #   in Loop: Header=BB1_4 Depth=1
	testl	%r14d, %r14d
	movl	(%rsp), %edx            # 4-byte Reload
	jne	.LBB1_27
	jmp	.LBB1_31
.LBB1_21:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%eax, %eax
	testl	%r14d, %r14d
	jne	.LBB1_25
	jmp	.LBB1_30
.LBB1_22:                               #   in Loop: Header=BB1_4 Depth=1
	testl	%r14d, %r14d
	jne	.LBB1_28
	jmp	.LBB1_29
.LBB1_23:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%eax, %eax
.LBB1_24:                               # %.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	testl	%r14d, %r14d
	je	.LBB1_30
.LBB1_25:                               # %.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	testq	%rax, %rax
	movl	(%rsp), %edx            # 4-byte Reload
	jne	.LBB1_31
# BB#26:                                #   in Loop: Header=BB1_4 Depth=1
	cmpb	$122, 32(%rbp)
	jne	.LBB1_29
.LBB1_27:                               #   in Loop: Header=BB1_4 Depth=1
	movq	80(%rbp), %rax
.LBB1_28:                               # %._crit_edge75
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	80(%rax), %rax
	cmpq	InputSym(%rip), %rax
	je	.LBB1_59
.LBB1_29:                               # %.loopexit52.thread
                                        #   in Loop: Header=BB1_4 Depth=1
	leaq	8(%r13), %rcx
	testl	%edx, %edx
	cmoveq	%r13, %rcx
	jmp	.LBB1_32
.LBB1_30:                               #   in Loop: Header=BB1_4 Depth=1
	movl	(%rsp), %edx            # 4-byte Reload
.LBB1_31:                               # %.loopexit52
                                        #   in Loop: Header=BB1_4 Depth=1
	leaq	8(%r13), %rcx
	testl	%edx, %edx
	cmoveq	%r13, %rcx
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB1_59
.LBB1_32:                               # %.split.backedge
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	(%rcx), %r13
	cmpb	$8, 32(%r13)
	jne	.LBB1_4
	jmp	.LBB1_58
.LBB1_33:                               # %.split.us.preheader
	cmpb	$8, %al
	je	.LBB1_58
# BB#34:                                # %.lr.ph.preheader
	movl	%r15d, 4(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB1_35:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_37 Depth 2
                                        #     Child Loop BB1_45 Depth 2
                                        #       Child Loop BB1_46 Depth 3
	leaq	16(%r13), %rbx
	jmp	.LBB1_37
	.p2align	4, 0x90
.LBB1_36:                               #   in Loop: Header=BB1_37 Depth=2
	addq	$16, %rbx
.LBB1_37:                               #   Parent Loop BB1_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	cmpb	$119, %al
	jg	.LBB1_39
# BB#38:                                #   in Loop: Header=BB1_37 Depth=2
	testb	%al, %al
	je	.LBB1_36
	jmp	.LBB1_55
	.p2align	4, 0x90
.LBB1_39:                               #   in Loop: Header=BB1_35 Depth=1
	cmpb	$120, %al
	je	.LBB1_43
# BB#40:                                #   in Loop: Header=BB1_35 Depth=1
	cmpb	$122, %al
	je	.LBB1_43
# BB#41:                                #   in Loop: Header=BB1_35 Depth=1
	cmpb	$121, %al
	jne	.LBB1_55
# BB#42:                                #   in Loop: Header=BB1_35 Depth=1
	testl	%r14d, %r14d
	jne	.LBB1_54
	jmp	.LBB1_55
	.p2align	4, 0x90
.LBB1_43:                               #   in Loop: Header=BB1_35 Depth=1
	testl	%r15d, %r15d
	je	.LBB1_49
# BB#44:                                # %.preheader51.us
                                        #   in Loop: Header=BB1_35 Depth=1
	movq	8(%rbx), %r15
	cmpq	%rbx, %r15
	je	.LBB1_50
	.p2align	4, 0x90
.LBB1_45:                               # %.preheader.us
                                        #   Parent Loop BB1_35 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_46 Depth 3
	movq	%r15, %rdi
	.p2align	4, 0x90
.LBB1_46:                               #   Parent Loop BB1_35 Depth=1
                                        #     Parent Loop BB1_45 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB1_46
# BB#47:                                #   in Loop: Header=BB1_45 Depth=2
	movl	$1, %edx
	movl	$1, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	movl	%r14d, %r9d
	callq	SearchGalley
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB1_51
# BB#48:                                #   in Loop: Header=BB1_45 Depth=2
	movq	8(%r15), %r15
	cmpq	%rbx, %r15
	jne	.LBB1_45
	jmp	.LBB1_51
.LBB1_49:                               #   in Loop: Header=BB1_35 Depth=1
	xorl	%ebp, %ebp
	testl	%r14d, %r14d
	jne	.LBB1_52
	jmp	.LBB1_56
.LBB1_50:                               #   in Loop: Header=BB1_35 Depth=1
	xorl	%ebp, %ebp
.LBB1_51:                               # %.loopexit.us
                                        #   in Loop: Header=BB1_35 Depth=1
	testl	%r14d, %r14d
	je	.LBB1_56
.LBB1_52:                               # %.loopexit.us
                                        #   in Loop: Header=BB1_35 Depth=1
	testq	%rbp, %rbp
	jne	.LBB1_56
# BB#53:                                #   in Loop: Header=BB1_35 Depth=1
	cmpb	$122, 32(%rbx)
	movl	4(%rsp), %r15d          # 4-byte Reload
	movl	(%rsp), %edx            # 4-byte Reload
	jne	.LBB1_55
.LBB1_54:                               #   in Loop: Header=BB1_35 Depth=1
	movq	80(%rbx), %rax
	movq	80(%rax), %rax
	cmpq	InputSym(%rip), %rax
	movq	%rbx, %rbp
	je	.LBB1_59
.LBB1_55:                               # %.loopexit52.us.thread
                                        #   in Loop: Header=BB1_35 Depth=1
	leaq	8(%r13), %rax
	testl	%edx, %edx
	cmoveq	%r13, %rax
	jmp	.LBB1_57
	.p2align	4, 0x90
.LBB1_56:                               # %.loopexit52.us
                                        #   in Loop: Header=BB1_35 Depth=1
	leaq	8(%r13), %rax
	movl	(%rsp), %edx            # 4-byte Reload
	testl	%edx, %edx
	cmoveq	%r13, %rax
	testq	%rbp, %rbp
	movl	4(%rsp), %r15d          # 4-byte Reload
	jne	.LBB1_59
.LBB1_57:                               # %.split.us.backedge
                                        #   in Loop: Header=BB1_35 Depth=1
	movq	(%rax), %r13
	cmpb	$8, 32(%r13)
	jne	.LBB1_35
.LBB1_58:
	xorl	%ebp, %ebp
.LBB1_59:                               # %.critedge
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	SearchGalley, .Lfunc_end1-SearchGalley
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	1141751808              # float 567
	.text
	.globl	AttachGalley
	.p2align	4, 0x90
	.type	AttachGalley,@function
AttachGalley:                           # @AttachGalley
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi25:
	.cfi_def_cfa_offset 320
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rdx, 224(%rsp)         # 8-byte Spill
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	movq	24(%r13), %rbp
	cmpq	%r13, %rbp
	jne	.LBB2_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	24(%r13), %rbp
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB2_2
# BB#3:
	cmpb	$120, %al
	je	.LBB2_5
# BB#4:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_5:                                # %.loopexit946
	movq	$0, 56(%rsp)
	movq	$0, 64(%rsp)
	movzwl	42(%r13), %eax
	movl	%eax, %ecx
	andl	$2, %ecx
	movl	%ecx, 164(%rsp)         # 4-byte Spill
	movl	%eax, %r15d
	shrl	$8, %r15d
	andl	$1, %r15d
	movl	%r15d, %ecx
	xorl	$1, %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	48(%r13,%rcx,4), %rcx
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	leaq	32(%r13), %rcx
	movq	%rcx, 248(%rsp)         # 8-byte Spill
                                        # implicit-def: %ECX
	movl	%ecx, 116(%rsp)         # 4-byte Spill
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movq	%r13, 240(%rsp)         # 8-byte Spill
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_6:                                # %.backedge945.backedge
                                        #   in Loop: Header=BB2_7 Depth=1
	movw	42(%r13), %ax
.LBB2_7:                                # %.backedge945
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_53 Depth 2
                                        #     Child Loop BB2_122 Depth 2
                                        #       Child Loop BB2_123 Depth 3
                                        #       Child Loop BB2_126 Depth 3
                                        #       Child Loop BB2_139 Depth 3
                                        #       Child Loop BB2_141 Depth 3
                                        #       Child Loop BB2_150 Depth 3
                                        #       Child Loop BB2_157 Depth 3
                                        #     Child Loop BB2_190 Depth 2
                                        #       Child Loop BB2_191 Depth 3
                                        #       Child Loop BB2_198 Depth 3
                                        #       Child Loop BB2_205 Depth 3
                                        #       Child Loop BB2_215 Depth 3
                                        #         Child Loop BB2_216 Depth 4
                                        #       Child Loop BB2_220 Depth 3
                                        #       Child Loop BB2_226 Depth 3
                                        #     Child Loop BB2_264 Depth 2
                                        #     Child Loop BB2_288 Depth 2
	movq	88(%r13), %rbx
	testb	$2, %al
	jne	.LBB2_10
# BB#8:                                 #   in Loop: Header=BB2_7 Depth=1
	movq	24(%rbp), %rdi
	cmpb	$-123, 40(%r13)
	jne	.LBB2_12
# BB#9:                                 #   in Loop: Header=BB2_7 Depth=1
	movl	$0, 116(%rsp)           # 4-byte Folded Spill
	xorl	%edx, %edx
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_10:                               #   in Loop: Header=BB2_7 Depth=1
	movq	24(%rbp), %rdi
	movl	$1, %edx
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movl	$1, %r9d
	movq	%rbx, %rsi
	callq	SearchGalley
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB2_347
# BB#11:                                #   in Loop: Header=BB2_7 Depth=1
	movq	80(%r14), %rax
	movq	80(%rax), %rax
	cmpq	InputSym(%rip), %rax
	jne	.LBB2_15
	jmp	.LBB2_368
	.p2align	4, 0x90
.LBB2_12:                               #   in Loop: Header=BB2_7 Depth=1
	xorl	%edx, %edx
	movl	$1, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rbx, %rsi
	callq	SearchGalley
	movq	%rax, %r14
	xorl	%eax, %eax
	testq	%r14, %r14
	setne	%al
	movl	%eax, 116(%rsp)         # 4-byte Spill
	jne	.LBB2_15
# BB#13:                                #   in Loop: Header=BB2_7 Depth=1
	movq	24(%rbp), %rdi
	movl	$1, %edx
.LBB2_14:                               #   in Loop: Header=BB2_7 Depth=1
	movl	$1, %ecx
	movl	$1, %r8d
	xorl	%r9d, %r9d
	movq	%rbx, %rsi
	callq	SearchGalley
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB2_384
.LBB2_15:                               # %.thread881
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpb	$121, 32(%r14)
	je	.LBB2_17
# BB#16:                                #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_17:                               #   in Loop: Header=BB2_7 Depth=1
	movq	80(%r14), %rbp
	cmpb	$2, 32(%rbp)
	je	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.9, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_19:                               #   in Loop: Header=BB2_7 Depth=1
	xorl	%edi, %edi
	callq	EnterErrorBlock
	movzbl	zz_lengths+8(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_21
# BB#20:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_22
	.p2align	4, 0x90
.LBB2_21:                               #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_22:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movb	$8, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movzwl	42(%rax), %esi
	movl	%esi, %ecx
	andl	$61439, %ecx            # imm = 0xEFFF
	movw	%cx, 42(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 128(%rax)
	movzwl	42(%rbp), %ecx
	andl	$8, %ecx
	shll	$5, %ecx
	andl	$61183, %esi            # imm = 0xEEFF
	orl	%ecx, %esi
	xorl	$256, %esi              # imm = 0x100
	movw	%si, 42(%rax)
	movzwl	34(%rbp), %ecx
	movw	%cx, 34(%rax)
	movl	36(%rbp), %ecx
	movl	$1048575, %edx          # imm = 0xFFFFF
	andl	%edx, %ecx
	movl	36(%rax), %edx
	movl	$-1048576, %edi         # imm = 0xFFF00000
	andl	%edi, %edx
	orl	%ecx, %edx
	movl	%edx, 36(%rax)
	movl	36(%rbp), %edx
	andl	%edi, %edx
	orl	%ecx, %edx
	movl	%edx, 36(%rax)
	movq	80(%rbp), %rcx
	movq	%rcx, 80(%rax)
	movups	%xmm0, 88(%rax)
	movb	$-127, 40(%rax)
	andl	$61309, %esi            # imm = 0xEF7D
	movw	%si, 42(%rax)
	testw	%r15w, %r15w
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	je	.LBB2_28
# BB#23:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rbp, %rdi
	leaq	8(%rsp), %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	leaq	120(%rsp), %rcx
	callq	Constrained
	movl	8(%rsp), %eax
	cmpl	$8388607, %eax          # imm = 0x7FFFFF
	jne	.LBB2_27
# BB#24:                                #   in Loop: Header=BB2_7 Depth=1
	movl	$8388607, %eax          # imm = 0x7FFFFF
	cmpl	$8388607, 12(%rsp)      # imm = 0x7FFFFF
	jne	.LBB2_27
# BB#25:                                #   in Loop: Header=BB2_7 Depth=1
	cmpl	$8388607, 16(%rsp)      # imm = 0x7FFFFF
	jne	.LBB2_27
# BB#26:                                #   in Loop: Header=BB2_7 Depth=1
	leaq	32(%rbp), %rbx
	movq	80(%rbp), %rdi
	callq	SymName
	movq	%rax, %rbp
	movl	$19, %edi
	movl	$2, %esi
	movl	$.L.str.10, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
	movl	8(%rsp), %eax
.LBB2_27:                               # %thread-pre-split
                                        #   in Loop: Header=BB2_7 Depth=1
	orl	12(%rsp), %eax
	orl	16(%rsp), %eax
	jns	.LBB2_29
	jmp	.LBB2_106
	.p2align	4, 0x90
.LBB2_28:                               #   in Loop: Header=BB2_7 Depth=1
	movabsq	$36028792732385279, %rax # imm = 0x7FFFFF007FFFFF
	movq	%rax, 8(%rsp)
	movl	$8388607, 16(%rsp)      # imm = 0x7FFFFF
.LBB2_29:                               #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %rsi
	movq	104(%rsp), %r12         # 8-byte Reload
	movq	%r12, %rdi
	callq	CopyObject
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_31
# BB#30:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_32
	.p2align	4, 0x90
.LBB2_31:                               #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_32:                               #   in Loop: Header=BB2_7 Depth=1
	movq	80(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rdx, zz_hold(%rip)
	testq	%rdx, %rdx
	je	.LBB2_35
# BB#33:                                #   in Loop: Header=BB2_7 Depth=1
	testq	%rax, %rax
	je	.LBB2_35
# BB#34:                                #   in Loop: Header=BB2_7 Depth=1
	movq	(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_35:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB2_38
# BB#36:                                #   in Loop: Header=BB2_7 Depth=1
	testq	%rax, %rax
	je	.LBB2_38
# BB#37:                                #   in Loop: Header=BB2_7 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_38:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, %rdi
	callq	DetachEnv
	movq	%rax, %rcx
	movzwl	42(%r12), %ebp
	xorl	%ebx, %ebx
	testb	$24, %bpl
	setne	%bl
	shrl	$2, %ebp
	andl	$1, %ebp
	movzwl	42(%r14), %r15d
	movq	%r12, %rax
	movl	%r15d, %r12d
	andl	$1, %r12d
	shrl	$6, %r15d
	andl	$1, %r15d
	leaq	64(%rax), %r10
	movq	88(%r13), %r11
	movq	136(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB2_40
# BB#39:                                #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %rsi
	movq	%rcx, %r14
	movq	%r10, %r13
	movq	%r11, 24(%rsp)          # 8-byte Spill
	callq	CopyObject
	movq	24(%rsp), %r11          # 8-byte Reload
	movq	%r13, %r10
	movq	%r14, %rcx
	jmp	.LBB2_41
	.p2align	4, 0x90
.LBB2_40:                               #   in Loop: Header=BB2_7 Depth=1
	xorl	%eax, %eax
.LBB2_41:                               #   in Loop: Header=BB2_7 Depth=1
	subq	$8, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%rcx, %rsi
	movl	%ebx, %edx
	movl	%ebp, %ecx
	movl	%r12d, %r8d
	movl	%r15d, %r9d
	pushq	%rax
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	leaq	72(%rsp), %rax
	pushq	%rax
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	leaq	200(%rsp), %rax
	pushq	%rax
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	leaq	200(%rsp), %rax
	pushq	%rax
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	leaq	56(%rsp), %rax
	pushq	%rax
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	callq	SizeGalley
	addq	$64, %rsp
.Lcfi40:
	.cfi_adjust_cfa_offset -64
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_43
# BB#42:                                #   in Loop: Header=BB2_7 Depth=1
	callq	ExpandRecursives
.LBB2_43:                               #   in Loop: Header=BB2_7 Depth=1
	movq	168(%rsp), %rax
	movq	80(%rax), %rdx
	movl	40(%rdx), %eax
	testl	$1610612736, %eax       # imm = 0x60000000
	movq	240(%rsp), %r13         # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	jne	.LBB2_45
# BB#44:                                #   in Loop: Header=BB2_7 Depth=1
	andl	$-1610612737, %eax      # imm = 0x9FFFFFFF
	orl	$536870912, %eax        # imm = 0x20000000
	movl	%eax, 40(%rdx)
.LBB2_45:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	testw	%cx, %cx
	je	.LBB2_51
# BB#46:                                #   in Loop: Header=BB2_7 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	8(%rsp), %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	leaq	120(%rsp), %rcx
	callq	Constrained
	movl	8(%rsp), %eax
	cmpl	$8388607, %eax          # imm = 0x7FFFFF
	jne	.LBB2_50
# BB#47:                                #   in Loop: Header=BB2_7 Depth=1
	movl	$8388607, %eax          # imm = 0x7FFFFF
	cmpl	$8388607, 12(%rsp)      # imm = 0x7FFFFF
	jne	.LBB2_50
# BB#48:                                #   in Loop: Header=BB2_7 Depth=1
	cmpl	$8388607, 16(%rsp)      # imm = 0x7FFFFF
	jne	.LBB2_50
# BB#49:                                #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	8(%rsp), %eax
.LBB2_50:                               # %thread-pre-split882
                                        #   in Loop: Header=BB2_7 Depth=1
	orl	12(%rsp), %eax
	orl	16(%rsp), %eax
	js	.LBB2_106
.LBB2_51:                               #   in Loop: Header=BB2_7 Depth=1
	testb	$2, 42(%r13)
	movq	40(%rsp), %r15          # 8-byte Reload
	jne	.LBB2_103
# BB#52:                                #   in Loop: Header=BB2_7 Depth=1
	movl	$1, %edi
	callq	EnterErrorBlock
	movq	$0, 256(%rsp)
	movq	8(%r13), %rax
	addq	$16, %rax
	movq	80(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_53:                               #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB2_53
# BB#54:                                #   in Loop: Header=BB2_7 Depth=1
	callq	DetachEnv
	testw	%r15w, %r15w
	je	.LBB2_56
# BB#55:                                #   in Loop: Header=BB2_7 Depth=1
	movq	24(%rsp), %rdx          # 8-byte Reload
	movzwl	42(%rdx), %ecx
	shrl	$2, %ecx
	andl	$1, %ecx
	jmp	.LBB2_57
.LBB2_56:                               #   in Loop: Header=BB2_7 Depth=1
	xorl	%ecx, %ecx
	movq	24(%rsp), %rdx          # 8-byte Reload
.LBB2_57:                               #   in Loop: Header=BB2_7 Depth=1
	movzwl	42(%rbx), %r8d
	andl	$1, %r8d
	leaq	64(%rdx), %rbp
	subq	$8, %rsp
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	movl	$1, %edx
	movl	$1, %r9d
	movq	%r13, %rdi
	movq	%rax, %rsi
	pushq	$0
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	leaq	80(%rsp), %rax
	pushq	%rax
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	leaq	200(%rsp), %rax
	pushq	%rax
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	leaq	288(%rsp), %rax
	pushq	%rax
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	leaq	56(%rsp), %rax
	pushq	%rax
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	callq	SizeGalley
	addq	$64, %rsp
.Lcfi49:
	.cfi_adjust_cfa_offset -64
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_59
# BB#58:                                #   in Loop: Header=BB2_7 Depth=1
	callq	ExpandRecursives
.LBB2_59:                               #   in Loop: Header=BB2_7 Depth=1
	cmpl	$0, 116(%rsp)           # 4-byte Folded Reload
	je	.LBB2_102
# BB#60:                                #   in Loop: Header=BB2_7 Depth=1
	movzbl	zz_lengths+124(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB2_62
# BB#61:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_63
.LBB2_62:                               #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB2_63:                               #   in Loop: Header=BB2_7 Depth=1
	movb	$124, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzbl	zz_lengths+125(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB2_65
# BB#64:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_66
.LBB2_65:                               #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB2_66:                               #   in Loop: Header=BB2_7 Depth=1
	movb	$125, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	andb	$-33, 42(%rbp)
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movl	$.L.str.12, %esi
	callq	MakeWord
	movq	%rax, %r14
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_68
# BB#67:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_69
.LBB2_68:                               #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_69:                               #   in Loop: Header=BB2_7 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB2_72
# BB#70:                                #   in Loop: Header=BB2_7 Depth=1
	testq	%rax, %rax
	je	.LBB2_72
# BB#71:                                #   in Loop: Header=BB2_7 Depth=1
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_72:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB2_75
# BB#73:                                #   in Loop: Header=BB2_7 Depth=1
	testq	%rax, %rax
	je	.LBB2_75
# BB#74:                                #   in Loop: Header=BB2_7 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_75:                               #   in Loop: Header=BB2_7 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_77
# BB#76:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_78
.LBB2_77:                               #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_78:                               #   in Loop: Header=BB2_7 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB2_81
# BB#79:                                #   in Loop: Header=BB2_7 Depth=1
	testq	%rax, %rax
	je	.LBB2_81
# BB#80:                                #   in Loop: Header=BB2_7 Depth=1
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_81:                               #   in Loop: Header=BB2_7 Depth=1
	testq	%r14, %r14
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	je	.LBB2_84
# BB#82:                                #   in Loop: Header=BB2_7 Depth=1
	testq	%rax, %rax
	je	.LBB2_84
# BB#83:                                #   in Loop: Header=BB2_7 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_84:                               #   in Loop: Header=BB2_7 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_86
# BB#85:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_87
.LBB2_86:                               #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_87:                               #   in Loop: Header=BB2_7 Depth=1
	movq	96(%rsp), %rcx          # 8-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	24(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_90
# BB#88:                                #   in Loop: Header=BB2_7 Depth=1
	testq	%rcx, %rcx
	je	.LBB2_90
# BB#89:                                #   in Loop: Header=BB2_7 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_90:                               #   in Loop: Header=BB2_7 Depth=1
	testq	%rbx, %rbx
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB2_93
# BB#91:                                #   in Loop: Header=BB2_7 Depth=1
	testq	%rax, %rax
	je	.LBB2_93
# BB#92:                                #   in Loop: Header=BB2_7 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_93:                               #   in Loop: Header=BB2_7 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_95
# BB#94:                                #   in Loop: Header=BB2_7 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_96
.LBB2_95:                               #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_96:                               #   in Loop: Header=BB2_7 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r13), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_99
# BB#97:                                #   in Loop: Header=BB2_7 Depth=1
	testq	%rcx, %rcx
	je	.LBB2_99
# BB#98:                                #   in Loop: Header=BB2_7 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_99:                               #   in Loop: Header=BB2_7 Depth=1
	testq	%rbp, %rbp
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB2_102
# BB#100:                               #   in Loop: Header=BB2_7 Depth=1
	testq	%rax, %rax
	je	.LBB2_102
# BB#101:                               #   in Loop: Header=BB2_7 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_102:                              #   in Loop: Header=BB2_7 Depth=1
	movl	$1, %edi
	callq	LeaveErrorBlock
.LBB2_103:                              #   in Loop: Header=BB2_7 Depth=1
	testw	%r15w, %r15w
	je	.LBB2_121
# BB#104:                               #   in Loop: Header=BB2_7 Depth=1
	movq	232(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	cmpl	8(%rsp), %eax
	jle	.LBB2_119
.LBB2_105:                              #   in Loop: Header=BB2_7 Depth=1
	movq	80(%r13), %rdi
	callq	SymName
	movq	%rax, %rbx
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	80(%rax), %rdi
	callq	SymName
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	movl	$19, %edi
	movl	$3, %esi
	movl	$.L.str.13, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	256(%rsp), %r8          # 8-byte Reload
	movq	%rbx, %r9
	pushq	%rbp
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi52:
	.cfi_adjust_cfa_offset -16
.LBB2_106:                              # %.thread888
                                        #   in Loop: Header=BB2_7 Depth=1
	movl	$1, %edi
	callq	LeaveErrorBlock
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_108
# BB#107:                               #   in Loop: Header=BB2_7 Depth=1
	callq	DisposeObject
	movq	$0, 56(%rsp)
.LBB2_108:                              #   in Loop: Header=BB2_7 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	DisposeObject
	cmpb	$-123, 40(%r13)
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	96(%rsp), %rbp          # 8-byte Reload
	movq	40(%rsp), %r15          # 8-byte Reload
	jne	.LBB2_114
# BB#109:                               #   in Loop: Header=BB2_7 Depth=1
	testb	$2, 42(%r13)
	jne	.LBB2_114
# BB#110:                               #   in Loop: Header=BB2_7 Depth=1
	movq	24(%rbp), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_112
# BB#111:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_112:                              #   in Loop: Header=BB2_7 Depth=1
	movq	%rax, zz_res(%rip)
	movq	24(%rsi), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_6
# BB#113:                               #   in Loop: Header=BB2_7 Depth=1
	testq	%rcx, %rcx
	jne	.LBB2_118
	jmp	.LBB2_6
	.p2align	4, 0x90
.LBB2_114:                              #   in Loop: Header=BB2_7 Depth=1
	movq	24(%rbp), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_116
# BB#115:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_116:                              #   in Loop: Header=BB2_7 Depth=1
	movq	%rax, zz_res(%rip)
	movq	24(%rsi), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB2_6
# BB#117:                               #   in Loop: Header=BB2_7 Depth=1
	testq	%rax, %rax
	je	.LBB2_6
.LBB2_118:                              #   in Loop: Header=BB2_7 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movw	42(%r13), %ax
	jmp	.LBB2_7
.LBB2_119:                              #   in Loop: Header=BB2_7 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	56(%r13,%rcx,4), %ecx
	addl	%ecx, %eax
	cmpl	12(%rsp), %eax
	jg	.LBB2_105
# BB#120:                               #   in Loop: Header=BB2_7 Depth=1
	cmpl	16(%rsp), %ecx
	jg	.LBB2_105
.LBB2_121:                              #   in Loop: Header=BB2_7 Depth=1
	movq	8(%r13), %r12
	cmpq	%r13, %r12
	movl	$0, %ebp
	movl	$1610612736, %ebx       # imm = 0x60000000
	movq	24(%rsp), %rsi          # 8-byte Reload
	je	.LBB2_302
	.p2align	4, 0x90
.LBB2_122:                              # %.preheader939
                                        #   Parent Loop BB2_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_123 Depth 3
                                        #       Child Loop BB2_126 Depth 3
                                        #       Child Loop BB2_139 Depth 3
                                        #       Child Loop BB2_141 Depth 3
                                        #       Child Loop BB2_150 Depth 3
                                        #       Child Loop BB2_157 Depth 3
	movq	%r12, %r14
	.p2align	4, 0x90
.LBB2_123:                              #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_122 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r14), %r14
	movzbl	32(%r14), %eax
	testb	%al, %al
	je	.LBB2_123
# BB#124:                               #   in Loop: Header=BB2_122 Depth=2
	cmpb	$9, %al
	jne	.LBB2_127
# BB#125:                               #   in Loop: Header=BB2_122 Depth=2
	leaq	8(%r14), %rax
	testw	%r15w, %r15w
	cmovneq	%r14, %rax
	movq	(%rax), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB2_126:                              #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_122 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %r14
	leaq	16(%r14), %rax
	cmpb	$0, 32(%r14)
	je	.LBB2_126
.LBB2_127:                              # %.loopexit938
                                        #   in Loop: Header=BB2_122 Depth=2
	movb	32(%r14), %al
	testb	%bpl, %bpl
	jne	.LBB2_135
# BB#128:                               # %.loopexit938
                                        #   in Loop: Header=BB2_122 Depth=2
	movl	%eax, %ecx
	addb	$-128, %cl
	movzbl	%cl, %ecx
	jmpq	*.LJTI2_0(,%rcx,8)
.LBB2_129:                              #   in Loop: Header=BB2_122 Depth=2
	movl	40(%rsi), %eax
	andl	%ebx, %eax
	movl	40(%r14), %ecx
	movl	$-1610612737, %edx      # imm = 0x9FFFFFFF
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 40(%r14)
.LBB2_130:                              #   in Loop: Header=BB2_122 Depth=2
	movq	8(%r12), %r12
	cmpq	%r13, %r12
	jne	.LBB2_122
	jmp	.LBB2_302
.LBB2_131:                              #   in Loop: Header=BB2_122 Depth=2
	cmpw	$0, 164(%rsp)           # 2-byte Folded Reload
	je	.LBB2_130
# BB#132:                               #   in Loop: Header=BB2_122 Depth=2
	cmpq	$0, 64(%rsp)
	jne	.LBB2_167
# BB#133:                               #   in Loop: Header=BB2_122 Depth=2
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_165
# BB#134:                               #   in Loop: Header=BB2_122 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_166
.LBB2_135:                              #   in Loop: Header=BB2_122 Depth=2
	movzbl	%al, %edi
	movq	no_fpos(%rip), %r14
	callq	Image
	movq	%rax, %rbx
	subq	$8, %rsp
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.24, %edx
	movl	$0, %ecx
	movl	$.L.str.25, %r9d
	xorl	%eax, %eax
	movq	%r14, %r8
	pushq	%rbx
	movl	$1610612736, %ebx       # imm = 0x60000000
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	callq	Error
	movq	40(%rsp), %rsi          # 8-byte Reload
	addq	$16, %rsp
.Lcfi55:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_130
.LBB2_136:                              #   in Loop: Header=BB2_122 Depth=2
	movl	40(%rsi), %eax
	andl	%ebx, %eax
	movl	40(%r14), %ecx
	movl	$-1610612737, %edx      # imm = 0x9FFFFFFF
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 40(%r14)
	testb	$2, 45(%r14)
	jne	.LBB2_130
# BB#137:                               #   in Loop: Header=BB2_122 Depth=2
	orb	$32, 42(%r13)
	jmp	.LBB2_130
.LBB2_138:                              #   in Loop: Header=BB2_122 Depth=2
	movq	8(%r14), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB2_139:                              #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_122 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rcx
	leaq	16(%rcx), %rax
	cmpb	$0, 32(%rcx)
	je	.LBB2_139
# BB#140:                               #   in Loop: Header=BB2_122 Depth=2
	movq	24(%rcx), %rbx
	cmpq	16(%rcx), %rbx
	je	.LBB2_147
	.p2align	4, 0x90
.LBB2_141:                              # %.preheader936
                                        #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_122 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB2_141
# BB#142:                               # %.preheader936
                                        #   in Loop: Header=BB2_122 Depth=2
	cmpb	$124, %al
	je	.LBB2_144
# BB#143:                               #   in Loop: Header=BB2_122 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.14, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_144:                              # %.loopexit937
                                        #   in Loop: Header=BB2_122 Depth=2
	movq	%rbx, %rdi
	movq	80(%rsp), %rsi          # 8-byte Reload
	callq	CheckComponentOrder
	cmpl	$157, %eax
	je	.LBB2_149
# BB#145:                               # %.loopexit937
                                        #   in Loop: Header=BB2_122 Depth=2
	cmpl	$155, %eax
	je	.LBB2_106
# BB#146:                               # %.loopexit937
                                        #   in Loop: Header=BB2_122 Depth=2
	cmpl	$156, %eax
	movq	40(%rsp), %r15          # 8-byte Reload
	movl	$0, %ebp
	movl	$1610612736, %ebx       # imm = 0x60000000
	movq	24(%rsp), %rsi          # 8-byte Reload
	jne	.LBB2_130
	jmp	.LBB2_387
.LBB2_147:                              #   in Loop: Header=BB2_122 Depth=2
	movq	(%r12), %r12
	movq	8(%r12), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_176
# BB#148:                               #   in Loop: Header=BB2_122 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB2_177
.LBB2_149:                              #   in Loop: Header=BB2_122 Depth=2
	movq	%rbx, xx_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB2_156
	.p2align	4, 0x90
.LBB2_150:                              # %.lr.ph1110
                                        #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_122 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_152
# BB#151:                               #   in Loop: Header=BB2_150 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_152:                              #   in Loop: Header=BB2_150 Depth=3
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_154
# BB#153:                               #   in Loop: Header=BB2_150 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_154:                              #   in Loop: Header=BB2_150 Depth=3
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB2_150
# BB#155:                               # %..preheader935_crit_edge
                                        #   in Loop: Header=BB2_122 Depth=2
	movl	%ecx, zz_size(%rip)
.LBB2_156:                              # %.preheader935
                                        #   in Loop: Header=BB2_122 Depth=2
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	movl	$0, %ebp
	je	.LBB2_163
	.p2align	4, 0x90
.LBB2_157:                              # %.lr.ph1112
                                        #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_122 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_159
# BB#158:                               #   in Loop: Header=BB2_157 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_159:                              #   in Loop: Header=BB2_157 Depth=3
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_161
# BB#160:                               #   in Loop: Header=BB2_157 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_161:                              #   in Loop: Header=BB2_157 Depth=3
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB2_157
# BB#162:                               # %._crit_edge1113
                                        #   in Loop: Header=BB2_122 Depth=2
	movl	%ecx, zz_size(%rip)
.LBB2_163:                              #   in Loop: Header=BB2_122 Depth=2
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	(%r12), %r12
	movq	8(%r12), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_181
# BB#164:                               #   in Loop: Header=BB2_122 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB2_182
.LBB2_165:                              #   in Loop: Header=BB2_122 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rax, zz_hold(%rip)
.LBB2_166:                              #   in Loop: Header=BB2_122 Depth=2
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, 64(%rsp)
.LBB2_167:                              #   in Loop: Header=BB2_122 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_169
# BB#168:                               #   in Loop: Header=BB2_122 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_170
.LBB2_169:                              #   in Loop: Header=BB2_122 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rax, zz_hold(%rip)
.LBB2_170:                              #   in Loop: Header=BB2_122 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	64(%rsp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_173
# BB#171:                               #   in Loop: Header=BB2_122 Depth=2
	testq	%rcx, %rcx
	je	.LBB2_173
# BB#172:                               #   in Loop: Header=BB2_122 Depth=2
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_173:                              #   in Loop: Header=BB2_122 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB2_130
# BB#174:                               #   in Loop: Header=BB2_122 Depth=2
	testq	%rax, %rax
	je	.LBB2_130
# BB#175:                               #   in Loop: Header=BB2_122 Depth=2
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	jmp	.LBB2_130
.LBB2_176:                              #   in Loop: Header=BB2_122 Depth=2
	xorl	%ecx, %ecx
.LBB2_177:                              #   in Loop: Header=BB2_122 Depth=2
	movl	$1610612736, %ebx       # imm = 0x60000000
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_179
# BB#178:                               #   in Loop: Header=BB2_122 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_179:                              #   in Loop: Header=BB2_122 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	je	.LBB2_186
# BB#180:                               #   in Loop: Header=BB2_122 Depth=2
	movq	24(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB2_130
.LBB2_181:                              #   in Loop: Header=BB2_122 Depth=2
	xorl	%ecx, %ecx
.LBB2_182:                              #   in Loop: Header=BB2_122 Depth=2
	movl	$1610612736, %ebx       # imm = 0x60000000
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_184
# BB#183:                               #   in Loop: Header=BB2_122 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_184:                              #   in Loop: Header=BB2_122 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	je	.LBB2_186
# BB#185:                               #   in Loop: Header=BB2_122 Depth=2
	movq	24(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB2_130
.LBB2_186:                              #   in Loop: Header=BB2_122 Depth=2
	callq	DisposeObject
	movq	24(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB2_130
.LBB2_187:                              #   in Loop: Header=BB2_7 Depth=1
	leaq	32(%r14), %rbp
	movl	40(%rsi), %ecx
	andl	%ebx, %ecx
	movl	40(%r14), %edx
	movl	$-1610612737, %esi      # imm = 0x9FFFFFFF
	andl	%esi, %edx
	orl	%ecx, %edx
	movl	%edx, 40(%r14)
	testw	%r15w, %r15w
	je	.LBB2_237
# BB#188:                               # %.preheader941
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	8(%r12), %r15
	cmpq	%r13, %r15
	je	.LBB2_235
# BB#189:                               # %.preheader933.lr.ph
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_190:                              # %.preheader933
                                        #   Parent Loop BB2_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_191 Depth 3
                                        #       Child Loop BB2_198 Depth 3
                                        #       Child Loop BB2_205 Depth 3
                                        #       Child Loop BB2_215 Depth 3
                                        #         Child Loop BB2_216 Depth 4
                                        #       Child Loop BB2_220 Depth 3
                                        #       Child Loop BB2_226 Depth 3
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB2_191:                              #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_190 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	jg	.LBB2_193
# BB#192:                               #   in Loop: Header=BB2_191 Depth=3
	je	.LBB2_191
	jmp	.LBB2_233
	.p2align	4, 0x90
.LBB2_193:                              #   in Loop: Header=BB2_190 Depth=2
	cmpb	$1, %al
	je	.LBB2_210
# BB#194:                               #   in Loop: Header=BB2_190 Depth=2
	cmpb	$122, %al
	je	.LBB2_212
# BB#195:                               #   in Loop: Header=BB2_190 Depth=2
	cmpb	$121, %al
	jne	.LBB2_233
# BB#196:                               #   in Loop: Header=BB2_190 Depth=2
	movzwl	42(%rbp), %eax
	testb	$1, %al
	je	.LBB2_386
# BB#197:                               #   in Loop: Header=BB2_190 Depth=2
	movq	(%r15), %r15
	movq	%rbp, xx_hold(%rip)
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB2_204
	.p2align	4, 0x90
.LBB2_198:                              # %.lr.ph1137
                                        #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_190 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_200
# BB#199:                               #   in Loop: Header=BB2_198 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_200:                              #   in Loop: Header=BB2_198 Depth=3
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_202
# BB#201:                               #   in Loop: Header=BB2_198 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_202:                              #   in Loop: Header=BB2_198 Depth=3
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbp
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_198
# BB#203:                               # %..preheader931_crit_edge
                                        #   in Loop: Header=BB2_190 Depth=2
	movl	%ecx, zz_size(%rip)
.LBB2_204:                              # %.preheader931
                                        #   in Loop: Header=BB2_190 Depth=2
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB2_232
	.p2align	4, 0x90
.LBB2_205:                              # %.lr.ph1139
                                        #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_190 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_207
# BB#206:                               #   in Loop: Header=BB2_205 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_207:                              #   in Loop: Header=BB2_205 Depth=3
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_209
# BB#208:                               #   in Loop: Header=BB2_205 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_209:                              #   in Loop: Header=BB2_205 Depth=3
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbp
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_205
	jmp	.LBB2_231
	.p2align	4, 0x90
.LBB2_210:                              #   in Loop: Header=BB2_190 Depth=2
	testb	$2, 45(%rbp)
	jne	.LBB2_233
# BB#211:                               #   in Loop: Header=BB2_190 Depth=2
	movq	(%r13), %r15
	jmp	.LBB2_233
	.p2align	4, 0x90
.LBB2_212:                              #   in Loop: Header=BB2_190 Depth=2
	movzwl	42(%rbp), %eax
	testb	$1, %al
	je	.LBB2_386
# BB#213:                               #   in Loop: Header=BB2_190 Depth=2
	movq	(%r15), %r15
	cmpq	%rbp, 8(%rbp)
	je	.LBB2_219
	.p2align	4, 0x90
.LBB2_215:                              # %.lr.ph1127
                                        #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_190 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_216 Depth 4
	movq	8(%r14), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB2_216:                              #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_190 Depth=2
                                        #       Parent Loop BB2_215 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rax), %rbx
	leaq	16(%rbx), %rax
	cmpb	$0, 32(%rbx)
	je	.LBB2_216
# BB#217:                               #   in Loop: Header=BB2_215 Depth=3
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_214
# BB#218:                               #   in Loop: Header=BB2_215 Depth=3
	callq	DisposeObject
	movq	$0, 104(%rbx)
.LBB2_214:                              #   in Loop: Header=BB2_215 Depth=3
	movq	%rbx, %rdi
	callq	DetachGalley
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	KillGalley
	cmpq	%rbp, 8(%rbp)
	jne	.LBB2_215
.LBB2_219:                              # %._crit_edge1128
                                        #   in Loop: Header=BB2_190 Depth=2
	movq	%rbp, xx_hold(%rip)
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB2_232
	.p2align	4, 0x90
.LBB2_220:                              # %.lr.ph1130
                                        #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_190 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_222
# BB#221:                               #   in Loop: Header=BB2_220 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_222:                              #   in Loop: Header=BB2_220 Depth=3
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_224
# BB#223:                               #   in Loop: Header=BB2_220 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_224:                              #   in Loop: Header=BB2_220 Depth=3
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rdx
	movq	24(%rdx), %rax
	cmpq	%rdx, %rax
	jne	.LBB2_220
# BB#225:                               # %.preheader932
                                        #   in Loop: Header=BB2_190 Depth=2
	movl	%ecx, zz_size(%rip)
	movq	8(%rdx), %rax
	cmpq	%rdx, %rax
	movq	%rax, %rbp
	je	.LBB2_232
	.p2align	4, 0x90
.LBB2_226:                              # %.lr.ph1132
                                        #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_190 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_228
# BB#227:                               #   in Loop: Header=BB2_226 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_228:                              #   in Loop: Header=BB2_226 Depth=3
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_230
# BB#229:                               #   in Loop: Header=BB2_226 Depth=3
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_230:                              #   in Loop: Header=BB2_226 Depth=3
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbp
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_226
.LBB2_231:                              # %._crit_edge1140
                                        #   in Loop: Header=BB2_190 Depth=2
	movl	%ecx, zz_size(%rip)
.LBB2_232:                              #   in Loop: Header=BB2_190 Depth=2
	movq	%rbp, zz_hold(%rip)
	movzbl	32(%rbp), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbp), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbp)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
.LBB2_233:                              # %.backedge
                                        #   in Loop: Header=BB2_190 Depth=2
	movq	8(%r15), %r15
	cmpq	%r13, %r15
	jne	.LBB2_190
# BB#234:                               # %._crit_edge1146.loopexit
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	88(%rsp), %rbp          # 8-byte Reload
	movb	(%rbp), %al
.LBB2_235:                              # %._crit_edge1146
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpb	$18, %al
	movq	40(%rsp), %r15          # 8-byte Reload
	jne	.LBB2_237
# BB#236:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%r14, %rdi
	callq	VerticalHyphenate
.LBB2_237:                              #   in Loop: Header=BB2_7 Depth=1
	movq	$0, 152(%rsp)
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	8(%rsp), %rsi
	movl	%r15d, %edx
	leaq	152(%rsp), %rcx
	callq	Constrained
	movl	48(%r14,%r15,4), %edi
	cmpl	8(%rsp), %edi
	jle	.LBB2_243
.LBB2_238:                              #   in Loop: Header=BB2_7 Depth=1
	testb	$16, 43(%r13)
	je	.LBB2_246
# BB#239:                               #   in Loop: Header=BB2_7 Depth=1
	movl	56(%r14,%r15,4), %esi
	movl	%esi, %eax
	addl	%edi, %eax
	jle	.LBB2_246
# BB#240:                               #   in Loop: Header=BB2_7 Depth=1
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	leaq	8(%rsp), %rdx
	callq	ScaleToConstraint
	movl	%eax, %ebx
	cmpl	$64, %ebx
	jle	.LBB2_245
# BB#241:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	movl	56(%r14,%r15,4), %eax
	addl	48(%r14,%r15,4), %eax
	cvtsi2ssl	%eax, %xmm0
	movss	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.15, %esi
	movb	$1, %al
	leaq	128(%rsp), %rbp
	movq	%rbp, %rdi
	callq	sprintf
	movl	12(%rsp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	.LCPI2_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.15, %esi
	movb	$1, %al
	leaq	192(%rsp), %rbp
	movq	%rbp, %rdi
	callq	sprintf
	movl	$19, %edi
	testw	%r15w, %r15w
	je	.LBB2_250
# BB#242:                               #   in Loop: Header=BB2_7 Depth=1
	movl	$4, %esi
	movl	$.L.str.16, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	88(%rsp), %r8           # 8-byte Reload
	leaq	128(%rsp), %r9
	pushq	$.L.str.17
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi58:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_251
.LBB2_243:                              #   in Loop: Header=BB2_7 Depth=1
	movl	56(%r14,%r15,4), %eax
	leal	(%rax,%rdi), %ecx
	cmpl	12(%rsp), %ecx
	jg	.LBB2_238
# BB#244:                               #   in Loop: Header=BB2_7 Depth=1
	cmpl	16(%rsp), %eax
	jle	.LBB2_252
	jmp	.LBB2_238
.LBB2_245:                              # %._crit_edge1373
                                        #   in Loop: Header=BB2_7 Depth=1
	movl	48(%r14,%r15,4), %edi
.LBB2_246:                              # %._crit_edge1396
                                        #   in Loop: Header=BB2_7 Depth=1
	addl	56(%r14,%r15,4), %edi
	jle	.LBB2_106
# BB#247:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%rbp, %r14
	cvtsi2ssl	%edi, %xmm0
	movss	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.15, %esi
	movb	$1, %al
	leaq	128(%rsp), %rbx
	movq	%rbx, %rdi
	callq	sprintf
	movl	12(%rsp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	.LCPI2_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.15, %esi
	movb	$1, %al
	leaq	192(%rsp), %rbp
	movq	%rbp, %rdi
	callq	sprintf
	subq	$8, %rsp
	testw	%r15w, %r15w
	je	.LBB2_249
# BB#248:                               #   in Loop: Header=BB2_7 Depth=1
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	movl	$19, %edi
	movl	$12, %esi
	movl	$.L.str.19, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbx, %r9
	pushq	%rbp
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi61:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_106
.LBB2_249:                              #   in Loop: Header=BB2_7 Depth=1
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	movl	$19, %edi
	movl	$13, %esi
	movl	$.L.str.20, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbx, %r9
	pushq	%rbp
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi64:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_106
.LBB2_250:                              #   in Loop: Header=BB2_7 Depth=1
	movl	$5, %esi
	movl	$.L.str.18, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	88(%rsp), %r8           # 8-byte Reload
	leaq	128(%rsp), %r9
	pushq	$.L.str.17
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi67:
	.cfi_adjust_cfa_offset -16
.LBB2_251:                              # %.thread887
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	%r14, %rdi
	movl	%ebx, %esi
	movl	%r15d, %edx
	callq	InterposeScale
	movq	%rax, %r14
.LBB2_252:                              #   in Loop: Header=BB2_7 Depth=1
	testw	%r15w, %r15w
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	48(%r14,%rdx,4), %rax
	movq	%r14, %rcx
	cmovneq	%r13, %rcx
	cmovneq	232(%rsp), %rax         # 8-byte Folded Reload
	movl	(%rax), %ebx
	movl	56(%rcx,%rdx,4), %r15d
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	8(%rsp), %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	leaq	120(%rsp), %rcx
	callq	Constrained
	cmpl	8(%rsp), %ebx
	jle	.LBB2_258
.LBB2_253:                              #   in Loop: Header=BB2_7 Depth=1
	testb	$16, 43(%r13)
	je	.LBB2_106
# BB#254:                               #   in Loop: Header=BB2_7 Depth=1
	movl	%r15d, %ebp
	addl	%ebx, %ebp
	jle	.LBB2_106
# BB#255:                               #   in Loop: Header=BB2_7 Depth=1
	movl	%ebx, %edi
	movl	%r15d, %esi
	leaq	8(%rsp), %rdx
	callq	ScaleToConstraint
	cmpl	$65, %eax
	jl	.LBB2_106
# BB#256:                               #   in Loop: Header=BB2_7 Depth=1
	movl	%eax, 188(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	movss	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.15, %esi
	movb	$1, %al
	leaq	128(%rsp), %rbp
	movq	%rbp, %rdi
	callq	sprintf
	movl	12(%rsp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	.LCPI2_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.15, %esi
	movb	$1, %al
	leaq	192(%rsp), %rbp
	movq	%rbp, %rdi
	callq	sprintf
	leaq	32(%r14), %r8
	cmpw	$0, 40(%rsp)            # 2-byte Folded Reload
	je	.LBB2_261
# BB#257:                               #   in Loop: Header=BB2_7 Depth=1
	movl	$19, %edi
	movl	$7, %esi
	movl	$.L.str.18, %edx
	movl	$2, %ecx
	movl	$0, %eax
	leaq	128(%rsp), %r9
	pushq	$.L.str.17
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi70:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_262
.LBB2_258:                              #   in Loop: Header=BB2_7 Depth=1
	leal	(%r15,%rbx), %eax
	cmpl	12(%rsp), %eax
	jg	.LBB2_253
# BB#259:                               #   in Loop: Header=BB2_7 Depth=1
	cmpl	16(%rsp), %r15d
	jg	.LBB2_253
# BB#260:                               #   in Loop: Header=BB2_7 Depth=1
	movq	%r15, 88(%rsp)          # 8-byte Spill
	jmp	.LBB2_263
.LBB2_261:                              #   in Loop: Header=BB2_7 Depth=1
	movl	$19, %edi
	movl	$6, %esi
	movl	$.L.str.16, %edx
	movl	$2, %ecx
	movl	$0, %eax
	leaq	128(%rsp), %r9
	pushq	$.L.str.17
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi73:
	.cfi_adjust_cfa_offset -16
.LBB2_262:                              #   in Loop: Header=BB2_7 Depth=1
	movq	%r15, 88(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	movl	188(%rsp), %esi         # 4-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	InterposeScale
	movq	%rax, %r14
.LBB2_263:                              #   in Loop: Header=BB2_7 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	movl	48(%r14,%rbp,4), %esi
	movl	56(%r14,%rbp,4), %edx
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdi
	movl	%ebp, %ecx
	callq	AdjustSize
	movq	%r15, %rdi
	movl	%ebx, %esi
	movq	88(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	32(%rsp), %r15          # 8-byte Reload
	movl	%r15d, %ecx
	callq	AdjustSize
	movq	104(%rsp), %rdi         # 8-byte Reload
	leaq	8(%rsp), %rsi
	movl	%ebp, %edx
	leaq	152(%rsp), %rcx
	callq	Constrained
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB2_264:                              #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbx
	movzbl	32(%rbx), %ecx
	leaq	16(%rbx), %rax
	testb	%cl, %cl
	je	.LBB2_264
# BB#265:                               #   in Loop: Header=BB2_7 Depth=1
	addb	$-119, %cl
	cmpb	$19, %cl
	ja	.LBB2_267
# BB#266:                               #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.21, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_267:                              #   in Loop: Header=BB2_7 Depth=1
	movl	48(%rbx,%rbp,4), %edi
	testl	%edi, %edi
	js	.LBB2_269
# BB#268:                               #   in Loop: Header=BB2_7 Depth=1
	cmpl	$0, 56(%rbx,%rbp,4)
	jns	.LBB2_270
.LBB2_269:                              #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.22, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	48(%rbx,%rbp,4), %edi
.LBB2_270:                              #   in Loop: Header=BB2_7 Depth=1
	cmpl	8(%rsp), %edi
	jle	.LBB2_273
.LBB2_271:                              #   in Loop: Header=BB2_7 Depth=1
	testb	$16, 43(%r13)
	jne	.LBB2_275
.LBB2_272:                              # %._crit_edge1395
                                        #   in Loop: Header=BB2_7 Depth=1
	leaq	128(%rsp), %r15
	jmp	.LBB2_280
.LBB2_273:                              #   in Loop: Header=BB2_7 Depth=1
	movl	56(%rbx,%rbp,4), %eax
	leal	(%rax,%rdi), %ecx
	cmpl	12(%rsp), %ecx
	jg	.LBB2_271
# BB#274:                               #   in Loop: Header=BB2_7 Depth=1
	cmpl	16(%rsp), %eax
	jle	.LBB2_287
	jmp	.LBB2_271
.LBB2_275:                              #   in Loop: Header=BB2_7 Depth=1
	movl	56(%rbx,%rbp,4), %esi
	movl	%esi, %eax
	addl	%edi, %eax
	leaq	128(%rsp), %r15
	jle	.LBB2_280
# BB#276:                               #   in Loop: Header=BB2_7 Depth=1
	movq	128(%r13), %rax
	cmpq	152(%rsp), %rax
	je	.LBB2_280
# BB#277:                               #   in Loop: Header=BB2_7 Depth=1
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	leaq	8(%rsp), %rdx
	callq	ScaleToConstraint
	movl	%eax, %r15d
	cmpl	$64, %r15d
	jle	.LBB2_284
# BB#278:                               #   in Loop: Header=BB2_7 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	movl	56(%rbx,%rbp,4), %eax
	addl	48(%rbx,%rbp,4), %eax
	cvtsi2ssl	%eax, %xmm0
	movss	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.15, %esi
	movb	$1, %al
	leaq	128(%rsp), %rcx
	movq	%rcx, %rdi
	callq	sprintf
	movl	12(%rsp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	.LCPI2_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.15, %esi
	movb	$1, %al
	leaq	192(%rsp), %rcx
	movq	%rcx, %rdi
	callq	sprintf
	leaq	32(%r14), %r8
	movl	$19, %edi
	testw	%bp, %bp
	je	.LBB2_285
# BB#279:                               #   in Loop: Header=BB2_7 Depth=1
	movl	$8, %esi
	movl	$.L.str.16, %edx
	movl	$2, %ecx
	movl	$0, %eax
	leaq	128(%rsp), %r9
	pushq	$.L.str.17
.Lcfi74:
	.cfi_adjust_cfa_offset 8
	leaq	200(%rsp), %rbp
	pushq	%rbp
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi76:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_286
.LBB2_280:                              # %._crit_edge1395
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	152(%rsp), %rax
	movq	%rax, 128(%r13)
	addl	56(%rbx,%rbp,4), %edi
	jle	.LBB2_106
# BB#281:                               #   in Loop: Header=BB2_7 Depth=1
	cvtsi2ssl	%edi, %xmm0
	movss	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.15, %esi
	movb	$1, %al
	movq	%r15, %rdi
	callq	sprintf
	movl	12(%rsp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	.LCPI2_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.15, %esi
	movb	$1, %al
	leaq	192(%rsp), %rbx
	movq	%rbx, %rdi
	callq	sprintf
	addq	$32, %r14
	subq	$8, %rsp
	testw	%bp, %bp
	je	.LBB2_283
# BB#282:                               #   in Loop: Header=BB2_7 Depth=1
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	movl	$19, %edi
	movl	$14, %esi
	movl	$.L.str.19, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%r15, %r9
	pushq	%rbx
.Lcfi78:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi79:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_106
.LBB2_283:                              #   in Loop: Header=BB2_7 Depth=1
.Lcfi80:
	.cfi_adjust_cfa_offset 8
	movl	$19, %edi
	movl	$15, %esi
	movl	$.L.str.20, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%r15, %r9
	pushq	%rbx
.Lcfi81:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi82:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_106
.LBB2_284:                              # %._crit_edge1371
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	movl	48(%rbx,%rbp,4), %edi
	jmp	.LBB2_272
.LBB2_285:                              #   in Loop: Header=BB2_7 Depth=1
	movl	$9, %esi
	movl	$.L.str.18, %edx
	movl	$2, %ecx
	movl	$0, %eax
	leaq	128(%rsp), %r9
	pushq	$.L.str.17
.Lcfi83:
	.cfi_adjust_cfa_offset 8
	leaq	200(%rsp), %rbp
	pushq	%rbp
.Lcfi84:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi85:
	.cfi_adjust_cfa_offset -16
.LBB2_286:                              # %.thread892
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, %rdi
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %esi
	callq	InterposeWideOrHigh
	movq	%rax, %rdi
	movl	%r15d, %esi
	movl	%ebx, %edx
	callq	InterposeScale
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB2_287:                              #   in Loop: Header=BB2_7 Depth=1
	movq	152(%rsp), %rax
	movq	%rax, 128(%r13)
	movq	104(%rsp), %rdi         # 8-byte Reload
	leaq	8(%rsp), %rsi
	movl	%r15d, %edx
	leaq	120(%rsp), %rcx
	callq	Constrained
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB2_288:                              #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbx
	movzbl	32(%rbx), %ecx
	leaq	16(%rbx), %rax
	testb	%cl, %cl
	je	.LBB2_288
# BB#289:                               #   in Loop: Header=BB2_7 Depth=1
	addb	$-119, %cl
	cmpb	$19, %cl
	ja	.LBB2_291
# BB#290:                               #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.21, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_291:                              #   in Loop: Header=BB2_7 Depth=1
	movl	48(%rbx,%r15,4), %edi
	testl	%edi, %edi
	js	.LBB2_293
# BB#292:                               #   in Loop: Header=BB2_7 Depth=1
	cmpl	$0, 56(%rbx,%r15,4)
	jns	.LBB2_294
.LBB2_293:                              #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	48(%rbx,%r15,4), %edi
.LBB2_294:                              #   in Loop: Header=BB2_7 Depth=1
	cmpl	8(%rsp), %edi
	jg	.LBB2_297
# BB#295:                               #   in Loop: Header=BB2_7 Depth=1
	movl	56(%rbx,%r15,4), %eax
	leal	(%rax,%rdi), %ecx
	cmpl	12(%rsp), %ecx
	jg	.LBB2_297
# BB#296:                               #   in Loop: Header=BB2_7 Depth=1
	cmpl	16(%rsp), %eax
	jle	.LBB2_409
.LBB2_297:                              #   in Loop: Header=BB2_7 Depth=1
	testb	$16, 43(%r13)
	je	.LBB2_106
# BB#298:                               #   in Loop: Header=BB2_7 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	56(%rbx,%rax,4), %esi
	movl	%esi, %eax
	addl	%edi, %eax
	jle	.LBB2_106
# BB#299:                               #   in Loop: Header=BB2_7 Depth=1
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	leaq	8(%rsp), %rdx
	callq	ScaleToConstraint
	movl	%eax, %r15d
	cmpl	$65, %r15d
	jl	.LBB2_106
# BB#300:
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	56(%rbx,%rcx,4), %eax
	addl	48(%rbx,%rcx,4), %eax
	cvtsi2ssl	%eax, %xmm0
	movss	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	leaq	128(%rsp), %rdi
	movl	$.L.str.15, %esi
	movb	$1, %al
	callq	sprintf
	movl	12(%rsp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	.LCPI2_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	leaq	192(%rsp), %rbp
	movl	$.L.str.15, %esi
	movb	$1, %al
	movq	%rbp, %rdi
	callq	sprintf
	leaq	32(%r14), %r8
	cmpw	$0, 40(%rsp)            # 2-byte Folded Reload
	je	.LBB2_407
# BB#301:
	leaq	128(%rsp), %r9
	movl	$19, %edi
	movl	$11, %esi
	movl	$.L.str.18, %edx
	movl	$2, %ecx
	movl	$0, %eax
	pushq	$.L.str.17
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi87:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi88:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_408
.LBB2_302:                              # %._crit_edge1118
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_304
# BB#303:
	callq	DisposeObject
	movq	$0, 56(%rsp)
.LBB2_304:
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	DisposeObject
	xorl	%edi, %edi
	callq	LeaveErrorBlock
	movq	8(%r13), %rbx
	cmpq	%r13, %rbx
	movq	80(%rsp), %r14          # 8-byte Reload
	je	.LBB2_324
# BB#305:                               # %.lr.ph1072.preheader
	movl	$246, %ebp
	.p2align	4, 0x90
.LBB2_306:                              # %.lr.ph1072
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_308 Depth 2
	leaq	16(%rbx), %rax
	jmp	.LBB2_308
	.p2align	4, 0x90
.LBB2_307:                              #   in Loop: Header=BB2_308 Depth=2
	addq	$16, %rax
.LBB2_308:                              #   Parent Loop BB2_306 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rax
	movzbl	32(%rax), %ecx
	cmpb	$7, %cl
	ja	.LBB2_318
# BB#309:                               #   in Loop: Header=BB2_308 Depth=2
	testb	%cl, %cl
	je	.LBB2_307
# BB#310:                               #   in Loop: Header=BB2_306 Depth=1
	movzbl	%cl, %eax
	btl	%eax, %ebp
	jae	.LBB2_318
# BB#311:                               #   in Loop: Header=BB2_306 Depth=1
	movq	(%rbx), %rbx
	movq	8(%rbx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_313
# BB#312:                               #   in Loop: Header=BB2_306 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB2_314
	.p2align	4, 0x90
.LBB2_313:                              #   in Loop: Header=BB2_306 Depth=1
	xorl	%ecx, %ecx
.LBB2_314:                              #   in Loop: Header=BB2_306 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_316
# BB#315:                               #   in Loop: Header=BB2_306 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_316:                              #   in Loop: Header=BB2_306 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB2_318
# BB#317:                               #   in Loop: Header=BB2_306 Depth=1
	callq	DisposeObject
.LBB2_318:                              # %.loopexit
                                        #   in Loop: Header=BB2_306 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r13, %rbx
	jne	.LBB2_306
# BB#319:                               # %._crit_edge1073
	movq	8(%r13), %rbx
	cmpq	%r13, %rbx
	je	.LBB2_324
# BB#320:
	movq	24(%r14), %rbp
	cmpb	$0, 32(%rbx)
	je	.LBB2_322
# BB#321:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_322:
	movq	%rbx, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%rbx, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB2_324
# BB#323:
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB2_324:                              # %._crit_edge1073.thread
	movq	24(%r13), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_326
# BB#325:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_326:
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB2_329
# BB#327:
	testq	%rax, %rax
	je	.LBB2_329
# BB#328:
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB2_329:
	movq	96(%rsp), %rbx          # 8-byte Reload
	cmpb	$120, 32(%rbx)
	je	.LBB2_331
# BB#330:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.27, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_331:
	movq	%rbx, xx_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB2_338
	.p2align	4, 0x90
.LBB2_332:                              # %.lr.ph1067
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_334
# BB#333:                               #   in Loop: Header=BB2_332 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_334:                              #   in Loop: Header=BB2_332 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_336
# BB#335:                               #   in Loop: Header=BB2_332 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_336:                              #   in Loop: Header=BB2_332 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB2_332
# BB#337:                               # %..preheader_crit_edge
	movl	%ecx, zz_size(%rip)
.LBB2_338:                              # %.preheader
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB2_345
	.p2align	4, 0x90
.LBB2_339:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_341
# BB#340:                               #   in Loop: Header=BB2_339 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_341:                              #   in Loop: Header=BB2_339 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_343
# BB#342:                               #   in Loop: Header=BB2_339 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_343:                              #   in Loop: Header=BB2_339 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB2_339
# BB#344:                               # %._crit_edge
	movl	%ecx, zz_size(%rip)
.LBB2_345:
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	64(%rsp), %rax
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movl	$4, %ebx
.LBB2_346:
	movl	%ebx, %eax
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_386:
	movq	%rbp, %r14
	jmp	.LBB2_388
.LBB2_347:                              # %.preheader929
	movq	%r13, %rax
	testw	%r15w, %r15w
	je	.LBB2_355
	.p2align	4, 0x90
.LBB2_348:                              # %.preheader929.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_350 Depth 2
                                        #     Child Loop BB2_353 Depth 2
	movq	8(%rax), %rax
	cmpq	%r13, %rax
	je	.LBB2_363
# BB#349:                               # %.preheader927.preheader
                                        #   in Loop: Header=BB2_348 Depth=1
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB2_350:                              # %.preheader927
                                        #   Parent Loop BB2_348 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %ecx
	testb	%cl, %cl
	je	.LBB2_350
# BB#351:                               # %.preheader927
                                        #   in Loop: Header=BB2_348 Depth=1
	cmpb	$9, %cl
	jne	.LBB2_354
# BB#352:                               #   in Loop: Header=BB2_348 Depth=1
	movq	(%rbx), %rdx
	addq	$16, %rdx
	.p2align	4, 0x90
.LBB2_353:                              #   Parent Loop BB2_348 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rbx
	movzbl	32(%rbx), %ecx
	leaq	16(%rbx), %rdx
	testb	%cl, %cl
	je	.LBB2_353
.LBB2_354:                              # %.loopexit926
                                        #   in Loop: Header=BB2_348 Depth=1
	addb	$-9, %cl
	cmpb	$90, %cl
	ja	.LBB2_348
	jmp	.LBB2_362
	.p2align	4, 0x90
.LBB2_355:                              # %.preheader929.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_357 Depth 2
                                        #     Child Loop BB2_360 Depth 2
	movq	8(%rax), %rax
	cmpq	%r13, %rax
	je	.LBB2_363
# BB#356:                               # %.preheader927.us.preheader
                                        #   in Loop: Header=BB2_355 Depth=1
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB2_357:                              # %.preheader927.us
                                        #   Parent Loop BB2_355 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %ecx
	testb	%cl, %cl
	je	.LBB2_357
# BB#358:                               # %.preheader927.us
                                        #   in Loop: Header=BB2_355 Depth=1
	cmpb	$9, %cl
	jne	.LBB2_361
# BB#359:                               #   in Loop: Header=BB2_355 Depth=1
	movq	8(%rbx), %rdx
	addq	$16, %rdx
	.p2align	4, 0x90
.LBB2_360:                              #   Parent Loop BB2_355 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rbx
	movzbl	32(%rbx), %ecx
	leaq	16(%rbx), %rdx
	testb	%cl, %cl
	je	.LBB2_360
.LBB2_361:                              # %.loopexit926.us
                                        #   in Loop: Header=BB2_355 Depth=1
	addb	$-9, %cl
	cmpb	$91, %cl
	jae	.LBB2_355
.LBB2_362:                              # %.us-lcssa.us
	addq	$32, %rbx
	movq	80(%r13), %rdi
	callq	SymName
	movq	%rax, %rbp
	movl	$19, %edi
	movl	$1, %esi
	movl	$.L.str.7, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
.LBB2_363:                              # %.thread
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_365
# BB#364:
	callq	DisposeObject
	movq	$0, 64(%rsp)
.LBB2_365:
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_367
# BB#366:
	callq	DisposeObject
	movq	$0, 56(%rsp)
.LBB2_367:
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	KillGalley
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax)
	jmp	.LBB2_346
.LBB2_368:
	movq	%rbp, xx_hold(%rip)
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB2_375
	.p2align	4, 0x90
.LBB2_369:                              # %.lr.ph1107
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_371
# BB#370:                               #   in Loop: Header=BB2_369 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_371:                              #   in Loop: Header=BB2_369 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_373
# BB#372:                               #   in Loop: Header=BB2_369 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_373:                              #   in Loop: Header=BB2_369 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbp
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_369
# BB#374:                               # %..preheader930_crit_edge
	movl	%ecx, zz_size(%rip)
.LBB2_375:                              # %.preheader930
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB2_382
	.p2align	4, 0x90
.LBB2_376:                              # %.lr.ph1102
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_378
# BB#377:                               #   in Loop: Header=BB2_376 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_378:                              #   in Loop: Header=BB2_376 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_380
# BB#379:                               #   in Loop: Header=BB2_376 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_380:                              #   in Loop: Header=BB2_376 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbp
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_376
# BB#381:                               # %._crit_edge1103
	movl	%ecx, zz_size(%rip)
.LBB2_382:
	movq	%rbp, zz_hold(%rip)
	movzbl	32(%rbp), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbp), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbp)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_400
# BB#383:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_401
.LBB2_384:
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax)
	movl	$2, %ebx
	jmp	.LBB2_346
.LBB2_387:                              # %.loopexit942.loopexit1175
	movw	42(%r14), %ax
.LBB2_388:                              # %.loopexit942
	orl	$32, %eax
	movw	%ax, 42(%r14)
	xorl	%edi, %edi
	callq	LeaveErrorBlock
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	je	.LBB2_390
# BB#389:
	callq	DisposeObject
	movq	$0, 56(%rsp)
.LBB2_390:
	movq	%rbp, %rdi
	callq	DisposeObject
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_392
# BB#391:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_392:
	movq	224(%rsp), %rbp         # 8-byte Reload
	movq	%rax, zz_res(%rip)
	movq	24(%rbx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_395
# BB#393:
	testq	%rcx, %rcx
	je	.LBB2_395
# BB#394:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB2_395:
	cmpw	$0, 164(%rsp)           # 2-byte Folded Reload
	movq	64(%rsp), %rdi
	je	.LBB2_399
# BB#396:
	testq	%rdi, %rdi
	je	.LBB2_398
# BB#397:
	callq	DisposeObject
	movq	$0, 64(%rsp)
.LBB2_398:
	xorl	%edi, %edi
.LBB2_399:
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rdi, (%rax)
	movq	%r14, (%rbp)
	movl	$3, %ebx
	jmp	.LBB2_346
.LBB2_400:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_401:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_403
# BB#402:
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_403:
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB2_406
# BB#404:
	testq	%rax, %rax
	je	.LBB2_406
# BB#405:
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_406:
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax)
	movl	$1, %ebx
	jmp	.LBB2_346
.LBB2_407:
	leaq	128(%rsp), %r9
	movl	$19, %edi
	movl	$10, %esi
	movl	$.L.str.16, %edx
	movl	$2, %ecx
	movl	$0, %eax
	pushq	$.L.str.17
.Lcfi89:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi90:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi91:
	.cfi_adjust_cfa_offset -16
.LBB2_408:
	movq	%rbx, %rdi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %esi
	callq	InterposeWideOrHigh
	movq	%rax, %rdi
	movl	%r15d, %esi
	movl	%ebx, %edx
	callq	InterposeScale
.LBB2_409:                              # %.loopexit944
	movq	104(%rsp), %rax         # 8-byte Reload
	movzwl	42(%rax), %eax
	testb	$8, %al
	movq	40(%rsp), %r15          # 8-byte Reload
	jne	.LBB2_416
# BB#410:
	testb	$16, %al
	jne	.LBB2_414
# BB#411:
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rbx
.LBB2_412:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB2_412
# BB#413:
	movl	48(%rbx,%r15,4), %esi
	movl	56(%rbx,%r15,4), %edx
	movq	104(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rdi
	movl	%r15d, %ecx
	callq	AdjustSize
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	48(%rbx,%rcx,4), %esi
	movl	56(%rbx,%rcx,4), %edx
	movq	%rbp, %rdi
	jmp	.LBB2_415
.LBB2_414:
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	48(%rax,%rcx,4), %esi
	movl	56(%rax,%rcx,4), %edx
	movq	104(%rsp), %rdi         # 8-byte Reload
.LBB2_415:
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	AdjustSize
.LBB2_416:
	movl	$1, %edi
	callq	LeaveErrorBlock
	movq	24(%r13), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_418
# BB#417:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_418:
	movq	%rax, zz_res(%rip)
	movq	168(%rsp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_421
# BB#419:
	testq	%rcx, %rcx
	je	.LBB2_421
# BB#420:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB2_421:
	movq	96(%rsp), %rax          # 8-byte Reload
	cmpb	$120, 32(%rax)
	je	.LBB2_423
# BB#422:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.27, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_423:
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, xx_hold(%rip)
	movq	24(%rsi), %rax
	cmpq	%rsi, %rax
	je	.LBB2_430
.LBB2_424:                              # %.lr.ph1100
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_426
# BB#425:                               #   in Loop: Header=BB2_424 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_426:                              #   in Loop: Header=BB2_424 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_428
# BB#427:                               #   in Loop: Header=BB2_424 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_428:                              #   in Loop: Header=BB2_424 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rsi
	movq	24(%rsi), %rax
	cmpq	%rsi, %rax
	jne	.LBB2_424
# BB#429:                               # %..preheader925_crit_edge
	movl	%ecx, zz_size(%rip)
.LBB2_430:                              # %.preheader925
	movq	8(%rsi), %rax
	cmpq	%rsi, %rax
	je	.LBB2_437
.LBB2_431:                              # %.lr.ph1095
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_433
# BB#432:                               #   in Loop: Header=BB2_431 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_433:                              #   in Loop: Header=BB2_431 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_435
# BB#434:                               #   in Loop: Header=BB2_431 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_435:                              #   in Loop: Header=BB2_431 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rsi
	movq	8(%rsi), %rax
	cmpq	%rsi, %rax
	jne	.LBB2_431
# BB#436:                               # %._crit_edge1096
	movl	%ecx, zz_size(%rip)
.LBB2_437:
	movq	%rsi, zz_hold(%rip)
	movzbl	32(%rsi), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rsi), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rsi)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	24(%rsp), %rbx          # 8-byte Reload
	movw	42(%rbx), %ax
	testw	%r15w, %r15w
	je	.LBB2_440
# BB#438:
	testb	$16, %al
	jne	.LBB2_446
# BB#439:
	movl	$19, %esi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	movq	%r14, %rcx
	callq	Interpose
	jmp	.LBB2_446
.LBB2_440:
	testb	$8, %al
	jne	.LBB2_446
# BB#441:
	movl	$17, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	movq	%r14, %rcx
	callq	Interpose
	movq	24(%rbx), %rax
.LBB2_442:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	movq	%rax, 120(%rsp)
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB2_442
# BB#443:
	cmpb	$17, %cl
	je	.LBB2_445
# BB#444:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.28, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	120(%rsp), %rax
.LBB2_445:                              # %.loopexit924
	movq	24(%rsp), %rsi          # 8-byte Reload
	movzwl	64(%rsi), %edx
	andl	$128, %edx
	movzwl	64(%rax), %ecx
	andl	$65407, %ecx            # imm = 0xFF7F
	orl	%edx, %ecx
	movw	%cx, 64(%rax)
	movzwl	64(%rsi), %edx
	andl	$256, %edx              # imm = 0x100
	andl	$-257, %ecx             # imm = 0xFEFF
	orl	%edx, %ecx
	movw	%cx, 64(%rax)
	movzwl	64(%rsi), %edx
	andl	$512, %edx              # imm = 0x200
	andl	$-513, %ecx             # imm = 0xFDFF
	orl	%edx, %ecx
	movw	%cx, 64(%rax)
	movzwl	64(%rsi), %edx
	andl	$7168, %edx             # imm = 0x1C00
	andl	$-7169, %ecx            # imm = 0xE3FF
	orl	%edx, %ecx
	movw	%cx, 64(%rax)
	movzwl	64(%rsi), %edx
	andl	$57344, %edx            # imm = 0xE000
	andl	$8191, %ecx             # imm = 0x1FFF
	orl	%edx, %ecx
	movw	%cx, 64(%rax)
	movzwl	66(%rsi), %ecx
	movw	%cx, 66(%rax)
	movb	68(%rsi), %cl
	andb	$3, %cl
	movb	68(%rax), %dl
	andb	$-4, %dl
	orb	%cl, %dl
	movb	%dl, 68(%rax)
	movb	68(%rsi), %cl
	andb	$12, %cl
	andb	$-13, %dl
	orb	%cl, %dl
	movb	%dl, 68(%rax)
	movb	68(%rsi), %cl
	andb	$112, %cl
	andb	$-113, %dl
	orb	%cl, %dl
	movb	%dl, 68(%rax)
	movb	64(%rsi), %cl
	andb	$8, %cl
	movb	64(%rax), %dl
	andb	$-9, %dl
	orb	%cl, %dl
	movb	%dl, 64(%rax)
	movzwl	68(%rsi), %ecx
	andl	$128, %ecx
	movzwl	68(%rax), %edx
	andl	$65407, %edx            # imm = 0xFF7F
	orl	%ecx, %edx
	movw	%dx, 68(%rax)
	movzwl	68(%rsi), %ecx
	andl	$256, %ecx              # imm = 0x100
	andl	$-257, %edx             # imm = 0xFEFF
	orl	%ecx, %edx
	movw	%dx, 68(%rax)
	movzwl	68(%rsi), %ecx
	andl	$512, %ecx              # imm = 0x200
	movq	120(%rsp), %rax
	movzwl	68(%rax), %edx
	andl	$65023, %edx            # imm = 0xFDFF
	orl	%ecx, %edx
	movw	%dx, 68(%rax)
	movzwl	68(%rsi), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %edx            # imm = 0xE3FF
	orl	%ecx, %edx
	movw	%dx, 68(%rax)
	movzwl	68(%rsi), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%ecx, %edx
	movw	%dx, 68(%rax)
	movzwl	70(%rsi), %ecx
	movw	%cx, 70(%rax)
	movl	$4095, %edx             # imm = 0xFFF
	andl	76(%rsi), %edx
	movl	$-4096, %ecx            # imm = 0xF000
	andl	76(%rax), %ecx
	orl	%edx, %ecx
	movl	%ecx, 76(%rax)
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	76(%rsi), %edx
	andl	$-4190209, %ecx         # imm = 0xFFC00FFF
	orl	%edx, %ecx
	movl	%ecx, 76(%rax)
	movl	$12582912, %edx         # imm = 0xC00000
	andl	76(%rsi), %edx
	andl	$-12582913, %ecx        # imm = 0xFF3FFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%rax)
	movl	$1056964608, %edx       # imm = 0x3F000000
	andl	76(%rsi), %edx
	andl	$-1056964609, %ecx      # imm = 0xC0FFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%rax)
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	76(%rsi), %edx
	andl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%rax)
	movl	$1073741824, %edx       # imm = 0x40000000
	andl	76(%rsi), %edx
	andl	$-1073741825, %ecx      # imm = 0xBFFFFFFF
	orl	%edx, %ecx
	movl	%ecx, 76(%rax)
	movb	64(%rsi), %cl
	andb	$1, %cl
	movb	64(%rax), %dl
	andb	$-2, %dl
	orb	%cl, %dl
	movb	%dl, 64(%rax)
	movb	64(%rsi), %cl
	andb	$2, %cl
	andb	$-3, %dl
	orb	%cl, %dl
	movb	%dl, 64(%rax)
	movb	64(%rsi), %cl
	andb	$4, %cl
	andb	$-5, %dl
	orb	%cl, %dl
	movb	%dl, 64(%rax)
	movb	64(%rsi), %cl
	andb	$112, %cl
	andb	$-113, %dl
	orb	%cl, %dl
	movb	%dl, 64(%rax)
	movzwl	72(%rsi), %ecx
	movw	%cx, 72(%rax)
	movzwl	74(%rsi), %ecx
	movw	%cx, 74(%rax)
	movb	64(%rax), %cl
	shrb	$2, %cl
	movzwl	42(%rax), %edx
	andb	$1, %cl
	movzbl	%cl, %ecx
	shll	$11, %ecx
	andl	$63487, %edx            # imm = 0xF7FF
	orl	%ecx, %edx
	movw	%dx, 42(%rax)
	movq	240(%rsp), %r13         # 8-byte Reload
.LBB2_446:
	movq	8(%r12), %rsi
	movq	168(%rsp), %rdx
	movl	$1, %ecx
	movq	%r13, %rdi
	callq	Promote
	movq	104(%rsp), %rdi         # 8-byte Reload
	testb	$24, 42(%rdi)
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	jne	.LBB2_450
# BB#447:
	movq	(%rbp), %rax
	addq	$16, %rax
.LBB2_448:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdx
	leaq	16(%rdx), %rax
	cmpb	$0, 32(%rdx)
	je	.LBB2_448
# BB#449:
	movl	$19, %esi
	movq	%rdx, %rcx
	callq	Interpose
.LBB2_450:
	movl	$1, %ecx
	movq	%rbp, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	Promote
	movq	%rbp, xx_hold(%rip)
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB2_457
.LBB2_451:                              # %.lr.ph1093
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_453
# BB#452:                               #   in Loop: Header=BB2_451 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_453:                              #   in Loop: Header=BB2_451 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_455
# BB#454:                               #   in Loop: Header=BB2_451 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_455:                              #   in Loop: Header=BB2_451 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbp
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_451
# BB#456:                               # %..preheader923_crit_edge
	movl	%ecx, zz_size(%rip)
.LBB2_457:                              # %.preheader923
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB2_464
.LBB2_458:                              # %.lr.ph1088
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_460
# BB#459:                               #   in Loop: Header=BB2_458 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_460:                              #   in Loop: Header=BB2_458 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_462
# BB#461:                               #   in Loop: Header=BB2_458 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_462:                              #   in Loop: Header=BB2_458 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbp
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_458
# BB#463:                               # %._crit_edge1089
	movl	%ecx, zz_size(%rip)
.LBB2_464:
	movq	%rbp, zz_hold(%rip)
	movzbl	32(%rbp), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbp), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbp)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	cmpq	%rbx, 8(%rbx)
	je	.LBB2_466
# BB#465:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.29, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_466:
	testb	$32, 42(%rbx)
	je	.LBB2_468
# BB#467:
	movq	168(%rsp), %rax
	orw	$32, 42(%rax)
.LBB2_468:
	movq	%rbx, xx_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB2_475
.LBB2_469:                              # %.lr.ph1086
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_471
# BB#470:                               #   in Loop: Header=BB2_469 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_471:                              #   in Loop: Header=BB2_469 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_473
# BB#472:                               #   in Loop: Header=BB2_469 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_473:                              #   in Loop: Header=BB2_469 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB2_469
# BB#474:                               # %..preheader922_crit_edge
	movl	%ecx, zz_size(%rip)
.LBB2_475:                              # %.preheader922
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB2_482
.LBB2_476:                              # %.lr.ph1081
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_478
# BB#477:                               #   in Loop: Header=BB2_476 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_478:                              #   in Loop: Header=BB2_476 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_480
# BB#479:                               #   in Loop: Header=BB2_476 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_480:                              #   in Loop: Header=BB2_476 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB2_476
# BB#481:                               # %._crit_edge1082
	movl	%ecx, zz_size(%rip)
.LBB2_482:
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	56(%rsp), %rbp
	movq	64(%rsp), %rbx
	testq	%rbp, %rbp
	je	.LBB2_503
# BB#483:
	testq	%rbx, %rbx
	je	.LBB2_504
# BB#484:
	movq	8(%rbx), %r14
	cmpq	%rbx, %r14
	je	.LBB2_488
# BB#485:
	cmpb	$0, 32(%r14)
	je	.LBB2_487
# BB#486:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_487:
	movq	%r14, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r14), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%r14, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r14), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	64(%rsp), %rbx
.LBB2_488:                              # %._crit_edge1394
	movq	%rbx, xx_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB2_495
.LBB2_489:                              # %.lr.ph1079
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_491
# BB#490:                               #   in Loop: Header=BB2_489 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_491:                              #   in Loop: Header=BB2_489 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_493
# BB#492:                               #   in Loop: Header=BB2_489 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_493:                              #   in Loop: Header=BB2_489 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB2_489
# BB#494:                               # %..preheader921_crit_edge
	movl	%ecx, zz_size(%rip)
.LBB2_495:                              # %.preheader921
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB2_502
.LBB2_496:                              # %.lr.ph1074
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_498
# BB#497:                               #   in Loop: Header=BB2_496 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_498:                              #   in Loop: Header=BB2_496 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_500
# BB#499:                               #   in Loop: Header=BB2_496 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_500:                              #   in Loop: Header=BB2_496 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB2_496
# BB#501:                               # %._crit_edge1075
	movl	%ecx, zz_size(%rip)
.LBB2_502:
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	56(%rsp), %rax
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	jmp	.LBB2_505
.LBB2_503:
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rbx, (%rax)
	jmp	.LBB2_505
.LBB2_504:
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
.LBB2_505:
	movl	$5, %ebx
	jmp	.LBB2_346
.Lfunc_end2:
	.size	AttachGalley, .Lfunc_end2-AttachGalley
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_130
	.quad	.LBB2_130
	.quad	.LBB2_130
	.quad	.LBB2_130
	.quad	.LBB2_130
	.quad	.LBB2_130
	.quad	.LBB2_130
	.quad	.LBB2_130
	.quad	.LBB2_130
	.quad	.LBB2_130
	.quad	.LBB2_130
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_136
	.quad	.LBB2_129
	.quad	.LBB2_135
	.quad	.LBB2_129
	.quad	.LBB2_129
	.quad	.LBB2_129
	.quad	.LBB2_129
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_129
	.quad	.LBB2_129
	.quad	.LBB2_129
	.quad	.LBB2_129
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_187
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_135
	.quad	.LBB2_131
	.quad	.LBB2_387
	.quad	.LBB2_387
	.quad	.LBB2_135
	.quad	.LBB2_131
	.quad	.LBB2_138
	.quad	.LBB2_135
	.quad	.LBB2_130

	.text
	.p2align	4, 0x90
	.type	InterposeScale,@function
InterposeScale:                         # @InterposeScale
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi96:
	.cfi_def_cfa_offset 48
.Lcfi97:
	.cfi_offset %rbx, -40
.Lcfi98:
	.cfi_offset %r14, -32
.Lcfi99:
	.cfi_offset %r15, -24
.Lcfi100:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %r14
	movzbl	zz_lengths+34(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB3_1
# BB#2:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB3_3
.LBB3_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB3_3:
	movb	$34, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	34(%r14), %eax
	movw	%ax, 34(%rbx)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r14), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbx), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbx)
	andl	36(%r14), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	testl	%r15d, %r15d
	movl	$128, %eax
	movl	$128, %ecx
	cmovel	%ebp, %ecx
	cmovnel	%ebp, %eax
	movl	%ecx, 64(%rbx)
	movl	%eax, 72(%rbx)
	movslq	%r15d, %rax
	movl	48(%r14,%rax,4), %ecx
	imull	%ebp, %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$25, %edx
	addl	%ecx, %edx
	sarl	$7, %edx
	movl	%edx, 48(%rbx,%rax,4)
	imull	56(%r14,%rax,4), %ebp
	movl	%ebp, %ecx
	sarl	$31, %ecx
	shrl	$25, %ecx
	addl	%ebp, %ecx
	sarl	$7, %ecx
	movl	%ecx, 56(%rbx,%rax,4)
	movl	$1, %ecx
	subl	%eax, %ecx
	movslq	%ecx, %rax
	movl	48(%r14,%rax,4), %ecx
	movl	%ecx, 48(%rbx,%rax,4)
	movl	56(%r14,%rax,4), %ecx
	movl	%ecx, 56(%rbx,%rax,4)
	movq	%r14, zz_hold(%rip)
	movq	24(%r14), %rax
	cmpq	%r14, %rax
	je	.LBB3_4
# BB#5:
	movq	16(%r14), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r14), %rcx
	movq	%rax, 24(%rcx)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%bpl
	je	.LBB3_8
# BB#6:
	testq	%rax, %rax
	je	.LBB3_8
# BB#7:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB3_8
.LBB3_4:                                # %.thread
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%bpl
.LBB3_8:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_9
# BB#10:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_11
.LBB3_9:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_11:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	sete	%cl
	orb	%cl, %bpl
	jne	.LBB3_13
# BB#12:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_13:
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB3_15
# BB#14:
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_15:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	InterposeScale, .Lfunc_end3-InterposeScale
	.cfi_endproc

	.p2align	4, 0x90
	.type	InterposeWideOrHigh,@function
InterposeWideOrHigh:                    # @InterposeWideOrHigh
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi105:
	.cfi_def_cfa_offset 48
.Lcfi106:
	.cfi_offset %rbx, -40
.Lcfi107:
	.cfi_offset %r14, -32
.Lcfi108:
	.cfi_offset %r15, -24
.Lcfi109:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	xorl	%ebp, %ebp
	testl	%r15d, %r15d
	setne	%bpl
	orl	$26, %ebp
	movzbl	zz_lengths(%rbp), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB4_1
# BB#2:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB4_3
.LBB4_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB4_3:
	movb	%bpl, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	34(%r14), %eax
	movw	%ax, 34(%rbx)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r14), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbx), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbx)
	andl	36(%r14), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movslq	%r15d, %rax
	movl	48(%r14,%rax,4), %ecx
	movl	%ecx, 48(%rbx,%rax,4)
	movl	56(%r14,%rax,4), %ecx
	movl	%ecx, 56(%rbx,%rax,4)
	movl	$1, %ecx
	subl	%r15d, %ecx
	movslq	%ecx, %rcx
	movl	48(%r14,%rcx,4), %edx
	movl	%edx, 48(%rbx,%rcx,4)
	movl	56(%r14,%rcx,4), %edx
	movl	%edx, 56(%rbx,%rcx,4)
	movl	$8388607, 64(%rbx)      # imm = 0x7FFFFF
	movl	56(%rbx,%rax,4), %ecx
	addl	48(%rbx,%rax,4), %ecx
	movl	%ecx, 68(%rbx)
	movl	$8388607, 72(%rbx)      # imm = 0x7FFFFF
	movq	%r14, zz_hold(%rip)
	movq	24(%r14), %rax
	cmpq	%r14, %rax
	je	.LBB4_4
# BB#5:
	movq	16(%r14), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r14), %rcx
	movq	%rax, 24(%rcx)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%bpl
	je	.LBB4_8
# BB#6:
	testq	%rax, %rax
	je	.LBB4_8
# BB#7:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB4_8
.LBB4_4:                                # %.thread
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%bpl
.LBB4_8:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_9
# BB#10:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_11
.LBB4_9:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB4_11:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	sete	%cl
	orb	%cl, %bpl
	jne	.LBB4_13
# BB#12:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB4_13:
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB4_15
# BB#14:
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB4_15:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	InterposeWideOrHigh, .Lfunc_end4-InterposeWideOrHigh
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"DetachGalley: precondition!"
	.size	.L.str.1, 28

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"DetachGalley: parent!"
	.size	.L.str.2, 22

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"SearchGalley: start!"
	.size	.L.str.4, 21

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"AttachGalley: no index!"
	.size	.L.str.5, 24

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"AttachGalley: not UNATTACHED!"
	.size	.L.str.6, 30

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"galley %s deleted from here (no target)"
	.size	.L.str.7, 40

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"AttachGalley: target_index!"
	.size	.L.str.8, 28

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"AttachGalley: target!"
	.size	.L.str.9, 22

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"receptive symbol %s has unconstrained width"
	.size	.L.str.10, 44

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"AttachGalley: dest unconstrained!"
	.size	.L.str.11, 34

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.zero	1
	.size	.L.str.12, 1

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"too little horizontal space for galley %s at %s"
	.size	.L.str.13, 48

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Attach: PRECEDES!"
	.size	.L.str.14, 18

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"%.1fc"
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"%s object too high for %s space; %s inserted"
	.size	.L.str.16, 45

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"@Scale"
	.size	.L.str.17, 7

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"%s object too wide for %s space; %s inserted"
	.size	.L.str.18, 45

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"%s object too high for %s space; will try elsewhere"
	.size	.L.str.19, 52

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"%s object too wide for %s space; will try elsewhere"
	.size	.L.str.20, 52

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"AttachGalley: is_index(z)!"
	.size	.L.str.21, 27

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"AttachGalley: z size!"
	.size	.L.str.22, 22

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"AttachGalley: z size (perpendicular)!"
	.size	.L.str.23, 38

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"assert failed in %s %s"
	.size	.L.str.24, 23

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"AttachGalley:"
	.size	.L.str.25, 14

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"TransferLinks: start_link!"
	.size	.L.str.26, 27

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"AttachGalley: type(hd_index)!"
	.size	.L.str.27, 30

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"AttachGalley: type(junk) != ACAT!"
	.size	.L.str.28, 34

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"AttachGalley: target_ind"
	.size	.L.str.29, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
