	.text
	.file	"durbin.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4602678819172646912     # double 0.5
.LCPI6_1:
	.quad	4598175219545276416     # double 0.25
.LCPI6_2:
	.quad	4661014508095930368     # double 4000
.LCPI6_5:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_3:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
.LCPI6_4:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 96
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$128000000, %edx        # imm = 0x7A12000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_72
# BB#1:
	movq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_72
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$128000000, %edx        # imm = 0x7A12000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_72
# BB#3:                                 # %polybench_alloc_data.exit
	movq	8(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_72
# BB#4:                                 # %polybench_alloc_data.exit53
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_72
# BB#5:                                 # %polybench_alloc_data.exit53
	movq	8(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_72
# BB#6:                                 # %polybench_alloc_data.exit55
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_72
# BB#7:                                 # %polybench_alloc_data.exit55
	movq	8(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_72
# BB#8:                                 # %polybench_alloc_data.exit57
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_72
# BB#9:                                 # %polybench_alloc_data.exit57
	movq	8(%rsp), %r13
	testq	%r13, %r13
	je	.LBB6_72
# BB#10:                                # %polybench_alloc_data.exit59
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_72
# BB#11:                                # %polybench_alloc_data.exit59
	movq	8(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_72
# BB#12:                                # %polybench_alloc_data.exit61
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_72
# BB#13:                                # %polybench_alloc_data.exit61
	movq	8(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB6_72
# BB#14:                                # %polybench_alloc_data.exit63
	xorl	%ecx, %ecx
	movl	$1, %eax
	movsd	.LCPI6_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI6_2(%rip), %xmm3   # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB6_16:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_17 Depth 2
	movq	%rcx, %rdx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movsd	%xmm0, (%r15,%rdx,8)
	leaq	1(%rdx), %rcx
	movslq	%ecx, %rsi
	imulq	$274877907, %rsi, %rsi  # imm = 0x10624DD3
	movq	%rsi, %rdi
	shrq	$63, %rdi
	sarq	$40, %rsi
	addl	%edi, %esi
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%esi, %xmm4
	movapd	%xmm4, %xmm5
	mulsd	%xmm1, %xmm5
	movsd	%xmm5, (%r12,%rdx,8)
	mulsd	%xmm2, %xmm4
	movsd	%xmm4, (%r13,%rdx,8)
	movq	%rax, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_17:                               #   Parent Loop BB6_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%esi, %xmm4
	mulsd	%xmm0, %xmm4
	divsd	%xmm3, %xmm4
	movsd	%xmm4, -8(%rbx,%rdx,8)
	movsd	%xmm4, -8(%r14,%rdx,8)
	movl	%esi, %edi
	orl	$1, %edi
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%edi, %xmm4
	mulsd	%xmm0, %xmm4
	divsd	%xmm3, %xmm4
	movsd	%xmm4, (%rbx,%rdx,8)
	movsd	%xmm4, (%r14,%rdx,8)
	addq	$2, %rsi
	addq	$2, %rdx
	cmpq	$4000, %rsi             # imm = 0xFA0
	jne	.LBB6_17
# BB#15:                                # %.loopexit.i
                                        #   in Loop: Header=BB6_16 Depth=1
	addq	$4000, %rax             # imm = 0xFA0
	cmpq	$4000, %rcx             # imm = 0xFA0
	jne	.LBB6_16
# BB#18:                                # %init_array.exit
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	(%r13), %rax
	movq	%rax, (%rbx)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, (%r12)
	movq	(%r13), %rax
	movq	%rax, (%r15)
	movd	%rax, %xmm4
	leaq	32000(%r14), %r10
	leaq	-8(%rbx), %r11
	leaq	-8(%r13), %r8
	leaq	-32008(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, %edi
	movapd	.LCPI6_3(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00]
	jmp	.LBB6_19
	.p2align	4, 0x90
.LBB6_37:                               # %._crit_edge6._crit_edge.i
                                        #   in Loop: Header=BB6_19 Depth=1
	movsd	(%r15,%rdi,8), %xmm4    # xmm4 = mem[0],zero
	movq	%rax, %rdi
.LBB6_19:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_24 Depth 2
                                        #     Child Loop BB6_31 Depth 2
	movsd	-8(%r12,%rdi,8), %xmm5  # xmm5 = mem[0],zero
	mulsd	%xmm4, %xmm4
	mulsd	%xmm5, %xmm4
	subsd	%xmm4, %xmm5
	movsd	%xmm5, (%r12,%rdi,8)
	movq	(%r13,%rdi,8), %rax
	movq	%rax, (%r14,%rdi,8)
	movd	%rax, %xmm4
	testb	$1, %dil
	jne	.LBB6_21
# BB#20:                                #   in Loop: Header=BB6_19 Depth=1
	xorl	%r9d, %r9d
	jmp	.LBB6_22
	.p2align	4, 0x90
.LBB6_21:                               #   in Loop: Header=BB6_19 Depth=1
	movsd	-8(%r13,%rdi,8), %xmm5  # xmm5 = mem[0],zero
	mulsd	-8(%rbx,%rdi,8), %xmm5
	addsd	%xmm5, %xmm4
	movsd	%xmm4, 32000(%r14,%rdi,8)
	movl	$1, %r9d
.LBB6_22:                               # %.prol.loopexit141
                                        #   in Loop: Header=BB6_19 Depth=1
	leaq	-1(%rdi), %rax
	cmpq	$1, %rdi
	je	.LBB6_25
# BB#23:                                # %.lr.ph.i.new
                                        #   in Loop: Header=BB6_19 Depth=1
	movq	%rdi, %rsi
	subq	%r9, %rsi
	imulq	$32000, %r9, %rbp       # imm = 0x7D00
	leaq	(%r10,%rbp), %rcx
	addq	%r11, %rbp
	shlq	$3, %r9
	movq	%r8, %rdx
	subq	%r9, %rdx
	.p2align	4, 0x90
.LBB6_24:                               #   Parent Loop BB6_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdx,%rdi,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	(%rbp,%rdi,8), %xmm5
	addsd	%xmm4, %xmm5
	movsd	%xmm5, (%rcx,%rdi,8)
	movsd	-8(%rdx,%rdi,8), %xmm4  # xmm4 = mem[0],zero
	mulsd	32000(%rbp,%rdi,8), %xmm4
	addsd	%xmm5, %xmm4
	movsd	%xmm4, 32000(%rcx,%rdi,8)
	addq	$64000, %rcx            # imm = 0xFA00
	addq	$64000, %rbp            # imm = 0xFA00
	addq	$-16, %rdx
	addq	$-2, %rsi
	jne	.LBB6_24
.LBB6_25:                               # %.lr.ph5.i
                                        #   in Loop: Header=BB6_19 Depth=1
	imulq	$32000, %rdi, %rsi      # imm = 0x7D00
	leaq	(%r14,%rsi), %rcx
	movsd	(%rcx,%rdi,8), %xmm4    # xmm4 = mem[0],zero
	mulsd	(%r12,%rdi,8), %xmm4
	movapd	%xmm4, %xmm5
	xorpd	%xmm0, %xmm5
	movlpd	%xmm5, (%r15,%rdi,8)
	movsd	-8(%rbx,%rdi,8), %xmm5  # xmm5 = mem[0],zero
	imulq	$32000, %rax, %rcx      # imm = 0x7D00
	addq	%rbx, %rcx
	mulsd	(%rcx,%rax,8), %xmm4
	subsd	%xmm4, %xmm5
	movsd	%xmm5, (%rbx,%rdi,8)
	cmpq	$1, %rdi
	jne	.LBB6_26
# BB#36:                                # %._crit_edge6.i.thread
                                        #   in Loop: Header=BB6_19 Depth=1
	movq	(%r15,%rdi,8), %rax
	addq	%rbx, %rsi
	movq	%rax, (%rsi,%rdi,8)
	movl	$2, %eax
	jmp	.LBB6_37
	.p2align	4, 0x90
.LBB6_26:                               # %._crit_edge24.i.preheader
                                        #   in Loop: Header=BB6_19 Depth=1
	testb	$1, %al
	jne	.LBB6_28
# BB#27:                                #   in Loop: Header=BB6_19 Depth=1
	movl	$1, %ecx
	cmpq	$2, %rdi
	jne	.LBB6_30
	jmp	.LBB6_32
	.p2align	4, 0x90
.LBB6_28:                               # %._crit_edge24.i.prol
                                        #   in Loop: Header=BB6_19 Depth=1
	leaq	(%rbx,%rsi), %rcx
	movsd	(%r15,%rdi,8), %xmm4    # xmm4 = mem[0],zero
	mulsd	-64000(%rcx,%rax,8), %xmm4
	addsd	31992(%rbx,%rdi,8), %xmm4
	movsd	%xmm4, 32000(%rbx,%rdi,8)
	movl	$2, %ecx
	cmpq	$2, %rdi
	je	.LBB6_32
.LBB6_30:                               # %._crit_edge24.i.preheader.new
                                        #   in Loop: Header=BB6_19 Depth=1
	movq	%rdi, %rax
	subq	%rcx, %rax
	imulq	$32000, %rcx, %rbp      # imm = 0x7D00
	leaq	(%rbp,%rdi,8), %rcx
	addq	%rbx, %rcx
	imulq	$32008, %rdi, %rdx      # imm = 0x7D08
	subq	%rbp, %rdx
	addq	32(%rsp), %rdx          # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB6_31:                               # %._crit_edge24.i
                                        #   Parent Loop BB6_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r15,%rdi,8), %xmm4    # xmm4 = mem[0],zero
	mulsd	(%rdx), %xmm4
	addsd	-8(%rcx), %xmm4
	movsd	%xmm4, (%rcx)
	movsd	(%r15,%rdi,8), %xmm4    # xmm4 = mem[0],zero
	mulsd	-32000(%rdx), %xmm4
	addsd	31992(%rcx), %xmm4
	movsd	%xmm4, 32000(%rcx)
	addq	$64000, %rcx            # imm = 0xFA00
	addq	$-64000, %rdx           # imm = 0xFFFF0600
	addq	$-2, %rax
	jne	.LBB6_31
.LBB6_32:                               # %._crit_edge6.i
                                        #   in Loop: Header=BB6_19 Depth=1
	movq	(%r15,%rdi,8), %rax
	addq	%rbx, %rsi
	movq	%rax, (%rsi,%rdi,8)
	leaq	1(%rdi), %rax
	cmpq	$4000, %rax             # imm = 0xFA0
	jne	.LBB6_37
# BB#33:                                # %.preheader.i.preheader
	leaq	31992(%rbx), %rax
	movl	$4, %ecx
	movq	16(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_34:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdx
	movq	%rdx, -32(%rsi,%rcx,8)
	movq	32000(%rax), %rdx
	movq	%rdx, -24(%rsi,%rcx,8)
	movq	64000(%rax), %rdx
	movq	%rdx, -16(%rsi,%rcx,8)
	movq	96000(%rax), %rdx
	movq	%rdx, -8(%rsi,%rcx,8)
	movq	128000(%rax), %rdx
	movq	%rdx, (%rsi,%rcx,8)
	addq	$5, %rcx
	addq	$160000, %rax           # imm = 0x27100
	cmpq	$4004, %rcx             # imm = 0xFA4
	jne	.LBB6_34
# BB#35:                                # %kernel_durbin.exit.preheader
	xorl	%esi, %esi
	movl	$1, %eax
	.p2align	4, 0x90
.LBB6_39:                               # %kernel_durbin.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_40 Depth 2
	movq	%rsi, %rcx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%ecx, %xmm4
	movsd	%xmm4, (%r15,%rcx,8)
	leaq	1(%rcx), %rsi
	movslq	%esi, %rdx
	imulq	$274877907, %rdx, %rdx  # imm = 0x10624DD3
	movq	%rdx, %rdi
	shrq	$63, %rdi
	sarq	$40, %rdx
	addl	%edi, %edx
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%edx, %xmm5
	movapd	%xmm5, %xmm6
	mulsd	%xmm1, %xmm6
	movsd	%xmm6, (%r12,%rcx,8)
	mulsd	%xmm2, %xmm5
	movsd	%xmm5, (%r13,%rcx,8)
	movq	%rax, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_40:                               #   Parent Loop BB6_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%edx, %xmm5
	mulsd	%xmm4, %xmm5
	divsd	%xmm3, %xmm5
	movsd	%xmm5, -8(%rbx,%rcx,8)
	movsd	%xmm5, -8(%r14,%rcx,8)
	movl	%edx, %edi
	orl	$1, %edi
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%edi, %xmm5
	mulsd	%xmm4, %xmm5
	divsd	%xmm3, %xmm5
	movsd	%xmm5, (%rbx,%rcx,8)
	movsd	%xmm5, (%r14,%rcx,8)
	addq	$2, %rdx
	addq	$2, %rcx
	cmpq	$4000, %rdx             # imm = 0xFA0
	jne	.LBB6_40
# BB#38:                                # %.loopexit.i68
                                        #   in Loop: Header=BB6_39 Depth=1
	addq	$4000, %rax             # imm = 0xFA0
	cmpq	$4000, %rsi             # imm = 0xFA0
	jne	.LBB6_39
# BB#41:                                # %init_array.exit74
	movq	(%r13), %rax
	movq	%rax, (%rbx)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, (%r12)
	movq	(%r13), %rax
	movq	%rax, (%r15)
	movd	%rax, %xmm1
	movl	$1, %edi
	jmp	.LBB6_42
	.p2align	4, 0x90
.LBB6_63:                               # %._crit_edge6._crit_edge.i92
                                        #   in Loop: Header=BB6_42 Depth=1
	movsd	(%r15,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	movq	%rax, %rdi
.LBB6_42:                               # %.lr.ph.i76
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_47 Depth 2
                                        #     Child Loop BB6_54 Depth 2
	movsd	-8(%r12,%rdi,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm1
	mulsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%r12,%rdi,8)
	movq	(%r13,%rdi,8), %rax
	movq	%rax, (%r14,%rdi,8)
	movd	%rax, %xmm1
	testb	$1, %dil
	jne	.LBB6_44
# BB#43:                                #   in Loop: Header=BB6_42 Depth=1
	xorl	%eax, %eax
	jmp	.LBB6_45
	.p2align	4, 0x90
.LBB6_44:                               #   in Loop: Header=BB6_42 Depth=1
	movsd	-8(%r13,%rdi,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	-8(%rbx,%rdi,8), %xmm2
	addsd	%xmm2, %xmm1
	movsd	%xmm1, 32000(%r14,%rdi,8)
	movl	$1, %eax
.LBB6_45:                               # %.prol.loopexit
                                        #   in Loop: Header=BB6_42 Depth=1
	leaq	-1(%rdi), %r9
	cmpq	$1, %rdi
	je	.LBB6_48
# BB#46:                                # %.lr.ph.i76.new
                                        #   in Loop: Header=BB6_42 Depth=1
	movq	%rdi, %rsi
	subq	%rax, %rsi
	imulq	$32000, %rax, %rbp      # imm = 0x7D00
	leaq	(%r10,%rbp), %rcx
	addq	%r11, %rbp
	shlq	$3, %rax
	movq	%r8, %rdx
	subq	%rax, %rdx
	.p2align	4, 0x90
.LBB6_47:                               #   Parent Loop BB6_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdx,%rdi,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	(%rbp,%rdi,8), %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, (%rcx,%rdi,8)
	movsd	-8(%rdx,%rdi,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	32000(%rbp,%rdi,8), %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, 32000(%rcx,%rdi,8)
	addq	$64000, %rcx            # imm = 0xFA00
	addq	$64000, %rbp            # imm = 0xFA00
	addq	$-16, %rdx
	addq	$-2, %rsi
	jne	.LBB6_47
.LBB6_48:                               # %.lr.ph5.i80
                                        #   in Loop: Header=BB6_42 Depth=1
	imulq	$32000, %rdi, %rsi      # imm = 0x7D00
	leaq	(%r14,%rsi), %rax
	movsd	(%rax,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%r12,%rdi,8), %xmm1
	movapd	%xmm1, %xmm2
	xorpd	%xmm0, %xmm2
	movlpd	%xmm2, (%r15,%rdi,8)
	movsd	-8(%rbx,%rdi,8), %xmm2  # xmm2 = mem[0],zero
	imulq	$32000, %r9, %rax       # imm = 0x7D00
	addq	%rbx, %rax
	mulsd	(%rax,%r9,8), %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%rbx,%rdi,8)
	cmpq	$1, %rdi
	jne	.LBB6_49
# BB#62:                                # %._crit_edge6.i90.thread
                                        #   in Loop: Header=BB6_42 Depth=1
	movq	(%r15,%rdi,8), %rax
	addq	%rbx, %rsi
	movq	%rax, (%rsi,%rdi,8)
	movl	$2, %eax
	jmp	.LBB6_63
	.p2align	4, 0x90
.LBB6_49:                               # %._crit_edge24.i85.preheader
                                        #   in Loop: Header=BB6_42 Depth=1
	testb	$1, %r9b
	jne	.LBB6_51
# BB#50:                                #   in Loop: Header=BB6_42 Depth=1
	movl	$1, %ecx
	cmpq	$2, %rdi
	jne	.LBB6_53
	jmp	.LBB6_55
	.p2align	4, 0x90
.LBB6_51:                               # %._crit_edge24.i85.prol
                                        #   in Loop: Header=BB6_42 Depth=1
	leaq	(%rbx,%rsi), %rax
	movsd	(%r15,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	-64000(%rax,%r9,8), %xmm1
	addsd	31992(%rbx,%rdi,8), %xmm1
	movsd	%xmm1, 32000(%rbx,%rdi,8)
	movl	$2, %ecx
	cmpq	$2, %rdi
	je	.LBB6_55
.LBB6_53:                               # %._crit_edge24.i85.preheader.new
                                        #   in Loop: Header=BB6_42 Depth=1
	movq	%rdi, %rax
	subq	%rcx, %rax
	imulq	$32000, %rcx, %rbp      # imm = 0x7D00
	leaq	(%rbp,%rdi,8), %rcx
	addq	%rbx, %rcx
	imulq	$32008, %rdi, %rdx      # imm = 0x7D08
	subq	%rbp, %rdx
	addq	32(%rsp), %rdx          # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB6_54:                               # %._crit_edge24.i85
                                        #   Parent Loop BB6_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r15,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%rdx), %xmm1
	addsd	-8(%rcx), %xmm1
	movsd	%xmm1, (%rcx)
	movsd	(%r15,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	-32000(%rdx), %xmm1
	addsd	31992(%rcx), %xmm1
	movsd	%xmm1, 32000(%rcx)
	addq	$64000, %rcx            # imm = 0xFA00
	addq	$-64000, %rdx           # imm = 0xFFFF0600
	addq	$-2, %rax
	jne	.LBB6_54
.LBB6_55:                               # %._crit_edge6.i90
                                        #   in Loop: Header=BB6_42 Depth=1
	movq	(%r15,%rdi,8), %rax
	addq	%rbx, %rsi
	movq	%rax, (%rsi,%rdi,8)
	leaq	1(%rdi), %rax
	cmpq	$4000, %rax             # imm = 0xFA0
	jne	.LBB6_63
# BB#56:                                # %.preheader.i96.preheader
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	32(%rsi), %rax
	movl	$19999, %ecx            # imm = 0x4E1F
	.p2align	4, 0x90
.LBB6_57:                               # %.preheader.i96
                                        # =>This Inner Loop Header: Depth=1
	movq	-128000(%rbx,%rcx,8), %rdx
	movq	%rdx, -32(%rax)
	movq	-96000(%rbx,%rcx,8), %rdx
	movq	%rdx, -24(%rax)
	movq	-64000(%rbx,%rcx,8), %rdx
	movq	%rdx, -16(%rax)
	movq	-32000(%rbx,%rcx,8), %rdx
	movq	%rdx, -8(%rax)
	movq	(%rbx,%rcx,8), %rdx
	movq	%rdx, (%rax)
	addq	$20000, %rcx            # imm = 0x4E20
	addq	$40, %rax
	cmpq	$16019999, %rcx         # imm = 0xF4721F
	jne	.LBB6_57
# BB#58:                                # %kernel_durbin_StrictFP.exit.preheader
	movl	$1, %edx
	movapd	.LCPI6_4(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_5(%rip), %xmm3   # xmm3 = mem[0],zero
	movq	16(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_59:                               # %kernel_durbin_StrictFP.exit
                                        # =>This Inner Loop Header: Depth=1
	movsd	-8(%rcx,%rdx,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%rsi,%rdx,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_60
# BB#64:                                # %kernel_durbin_StrictFP.exit.1137
                                        #   in Loop: Header=BB6_59 Depth=1
	movsd	(%rcx,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rsi,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_61
# BB#65:                                #   in Loop: Header=BB6_59 Depth=1
	leaq	2(%rdx), %rax
	incq	%rdx
	cmpq	$4000, %rdx             # imm = 0xFA0
	movq	%rax, %rdx
	jl	.LBB6_59
# BB#66:                                # %check_FP.exit.preheader
	xorl	%ebp, %ebp
	jmp	.LBB6_67
.LBB6_68:                               #   in Loop: Header=BB6_67 Depth=1
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	jmp	.LBB6_69
	.p2align	4, 0x90
.LBB6_67:                               # %check_FP.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	stderr(%rip), %rdi
	movsd	(%rsi,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	movslq	%ebp, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	je	.LBB6_68
.LBB6_69:                               #   in Loop: Header=BB6_67 Depth=1
	incq	%rbp
	cmpq	$4000, %rbp             # imm = 0xFA0
	movq	24(%rsp), %rsi          # 8-byte Reload
	jne	.LBB6_67
# BB#70:                                # %print_array.exit
	movq	%rbx, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_71
.LBB6_60:                               # %check_FP.exit.threadsplit
	decq	%rdx
.LBB6_61:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_5(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %ecx
	callq	fprintf
	movl	$1, %eax
.LBB6_71:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_72:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d] = %lf and B[%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 68

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%0.2lf "
	.size	.L.str.3, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
