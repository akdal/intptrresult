	.text
	.file	"jdsample.bc"
	.globl	jinit_upsampler
	.p2align	4, 0x90
	.type	jinit_upsampler,@function
jinit_upsampler:                        # @jinit_upsampler
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	8(%r12), %rax
	movl	$1, %esi
	movl	$256, %edx              # imm = 0x100
	callq	*(%rax)
	movq	%rax, %r14
	movq	%r14, 592(%r12)
	movq	$start_pass_upsample, (%r14)
	movq	$sep_upsample, 8(%r14)
	movl	$0, 16(%r14)
	cmpl	$0, 384(%r12)
	je	.LBB0_2
# BB#1:
	movq	(%r12), %rax
	movl	$23, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_2:
	cmpl	$0, 92(%r12)
	je	.LBB0_3
# BB#4:
	cmpl	$1, 396(%r12)
	setg	%r15b
	cmpl	$0, 48(%r12)
	jg	.LBB0_6
	jmp	.LBB0_31
.LBB0_3:
	xorl	%r15d, %r15d
	cmpl	$0, 48(%r12)
	jle	.LBB0_31
.LBB0_6:                                # %.lr.ph
	movq	296(%r12), %rbp
	addq	$48, %rbp
	xorl	%ebx, %ebx
	jmp	.LBB0_7
.LBB0_27:                               # %.critedge110
                                        #   in Loop: Header=BB0_7 Depth=1
	movq	$int_upsample, 104(%r14,%rbx,8)
	movl	%edi, %eax
	cltd
	idivl	%ecx
	movb	%al, 232(%r14,%rbx)
	movl	%r8d, %eax
	cltd
	idivl	%esi
	movb	%al, 242(%r14,%rbx)
	jmp	.LBB0_29
	.p2align	4, 0x90
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %esi
	movl	-40(%rbp), %eax
	imull	%esi, %eax
	movl	396(%r12), %edi
	cltd
	idivl	%edi
	movl	%eax, %ecx
	imull	-36(%rbp), %esi
	movl	%esi, %eax
	cltd
	idivl	%edi
	movl	%eax, %esi
	movl	388(%r12), %edi
	movl	392(%r12), %r8d
	movl	%esi, 192(%r14,%rbx,4)
	cmpl	$0, (%rbp)
	je	.LBB0_8
# BB#9:                                 #   in Loop: Header=BB0_7 Depth=1
	cmpl	%edi, %ecx
	jne	.LBB0_12
# BB#10:                                #   in Loop: Header=BB0_7 Depth=1
	cmpl	%r8d, %esi
	jne	.LBB0_12
# BB#11:                                # %.critedge
                                        #   in Loop: Header=BB0_7 Depth=1
	movq	$fullsize_upsample, 104(%r14,%rbx,8)
	jmp	.LBB0_30
	.p2align	4, 0x90
.LBB0_8:                                #   in Loop: Header=BB0_7 Depth=1
	movq	$noop_upsample, 104(%r14,%rbx,8)
	jmp	.LBB0_30
	.p2align	4, 0x90
.LBB0_12:                               #   in Loop: Header=BB0_7 Depth=1
	leal	(%rcx,%rcx), %eax
	cmpl	%edi, %eax
	jne	.LBB0_19
# BB#13:                                #   in Loop: Header=BB0_7 Depth=1
	cmpl	%r8d, %esi
	jne	.LBB0_19
# BB#14:                                #   in Loop: Header=BB0_7 Depth=1
	testb	%r15b, %r15b
	je	.LBB0_17
# BB#15:                                #   in Loop: Header=BB0_7 Depth=1
	cmpl	$2, -8(%rbp)
	jbe	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_7 Depth=1
	movl	$h2v1_fancy_upsample, %eax
	movq	%rax, 104(%r14,%rbx,8)
	jmp	.LBB0_29
.LBB0_19:                               #   in Loop: Header=BB0_7 Depth=1
	cmpl	%edi, %eax
	jne	.LBB0_25
# BB#20:                                #   in Loop: Header=BB0_7 Depth=1
	leal	(%rsi,%rsi), %eax
	cmpl	%r8d, %eax
	jne	.LBB0_25
# BB#21:                                #   in Loop: Header=BB0_7 Depth=1
	testb	%r15b, %r15b
	je	.LBB0_24
# BB#22:                                #   in Loop: Header=BB0_7 Depth=1
	cmpl	$3, -8(%rbp)
	jb	.LBB0_24
# BB#23:                                # %.critedge108
                                        #   in Loop: Header=BB0_7 Depth=1
	movq	$h2v2_fancy_upsample, 104(%r14,%rbx,8)
	movl	$1, 16(%r14)
	jmp	.LBB0_29
.LBB0_17:                               #   in Loop: Header=BB0_7 Depth=1
	movl	$h2v1_upsample, %eax
	movq	%rax, 104(%r14,%rbx,8)
	jmp	.LBB0_29
.LBB0_25:                               #   in Loop: Header=BB0_7 Depth=1
	movl	%edi, %eax
	cltd
	idivl	%ecx
	testl	%edx, %edx
	jne	.LBB0_28
# BB#26:                                #   in Loop: Header=BB0_7 Depth=1
	movl	%r8d, %eax
	cltd
	idivl	%esi
	testl	%edx, %edx
	je	.LBB0_27
.LBB0_28:                               # %.critedge111
                                        #   in Loop: Header=BB0_7 Depth=1
	movq	(%r12), %rax
	movl	$37, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
	jmp	.LBB0_29
.LBB0_24:                               # %.critedge109
                                        #   in Loop: Header=BB0_7 Depth=1
	movq	$h2v2_upsample, 104(%r14,%rbx,8)
	.p2align	4, 0x90
.LBB0_29:                               #   in Loop: Header=BB0_7 Depth=1
	movq	8(%r12), %rax
	movq	16(%rax), %r13
	movl	128(%r12), %edi
	movslq	388(%r12), %rsi
	callq	jround_up
	movl	392(%r12), %ecx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	callq	*%r13
	movq	%rax, 24(%r14,%rbx,8)
.LBB0_30:                               #   in Loop: Header=BB0_7 Depth=1
	incq	%rbx
	movslq	48(%r12), %rax
	addq	$96, %rbp
	cmpq	%rax, %rbx
	jl	.LBB0_7
.LBB0_31:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jinit_upsampler, .Lfunc_end0-jinit_upsampler
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_pass_upsample,@function
start_pass_upsample:                    # @start_pass_upsample
	.cfi_startproc
# BB#0:
	movq	592(%rdi), %rax
	movl	392(%rdi), %ecx
	movl	%ecx, 184(%rax)
	movl	132(%rdi), %ecx
	movl	%ecx, 188(%rax)
	retq
.Lfunc_end1:
	.size	start_pass_upsample, .Lfunc_end1-start_pass_upsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	sep_upsample,@function
sep_upsample:                           # @sep_upsample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movl	80(%rsp), %ebp
	movq	592(%rbx), %r14
	movl	184(%r14), %edx
	movl	392(%rbx), %eax
	cmpl	%eax, %edx
	jl	.LBB2_6
# BB#1:
	cmpl	$0, 48(%rbx)
	jle	.LBB2_5
# BB#2:                                 # %.lr.ph.preheader
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	296(%rbx), %r12
	movq	%r15, (%rsp)            # 8-byte Spill
	leaq	24(%r14), %r15
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%r13,8), %rax
	movl	192(%r14,%r13,4), %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	imull	(%rdx), %ecx
	leaq	(%rax,%rcx,8), %rdx
	movq	%rbx, %rdi
	movq	%rsi, %rbp
	movq	%r12, %rsi
	movq	%r15, %rcx
	callq	*104(%r14,%r13,8)
	movq	%rbp, %rsi
	incq	%r13
	addq	$96, %r12
	movslq	48(%rbx), %rax
	addq	$8, %r15
	cmpq	%rax, %r13
	jl	.LBB2_3
# BB#4:                                 # %._crit_edge.loopexit
	movl	392(%rbx), %eax
	movq	(%rsp), %r15            # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	80(%rsp), %ebp
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB2_5:                                # %._crit_edge
	movl	$0, 184(%r14)
	xorl	%edx, %edx
.LBB2_6:
	subl	%edx, %eax
	movl	188(%r14), %ecx
	cmpl	%ecx, %eax
	cmoval	%ecx, %eax
	movl	(%r12), %ecx
	subl	%ecx, %ebp
	cmpl	%ebp, %eax
	cmovbel	%eax, %ebp
	movq	600(%rbx), %rax
	leaq	24(%r14), %rsi
	leaq	(%r8,%rcx,8), %rcx
	movq	%rbx, %rdi
	movl	%ebp, %r8d
	callq	*8(%rax)
	addl	%ebp, (%r12)
	subl	%ebp, 188(%r14)
	addl	184(%r14), %ebp
	movl	%ebp, 184(%r14)
	cmpl	392(%rbx), %ebp
	jl	.LBB2_8
# BB#7:
	incl	(%r15)
.LBB2_8:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	sep_upsample, .Lfunc_end2-sep_upsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	noop_upsample,@function
noop_upsample:                          # @noop_upsample
	.cfi_startproc
# BB#0:
	movq	$0, (%rcx)
	retq
.Lfunc_end3:
	.size	noop_upsample, .Lfunc_end3-noop_upsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	fullsize_upsample,@function
fullsize_upsample:                      # @fullsize_upsample
	.cfi_startproc
# BB#0:
	movq	%rdx, (%rcx)
	retq
.Lfunc_end4:
	.size	fullsize_upsample, .Lfunc_end4-fullsize_upsample
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	3                       # 0x3
.LCPI5_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI5_2:
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
.LCPI5_3:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.text
	.p2align	4, 0x90
	.type	h2v1_fancy_upsample,@function
h2v1_fancy_upsample:                    # @h2v1_fancy_upsample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rdx, %r8
	movq	%rsi, %r12
	movq	%rdi, %r14
	cmpl	$0, 392(%r14)
	jle	.LBB5_14
# BB#1:                                 # %.lr.ph51
	movq	(%rcx), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	pxor	%xmm12, %xmm12
	movdqa	.LCPI5_0(%rip), %xmm8   # xmm8 = [3,3,3,3,3,3,3,3]
	movdqa	.LCPI5_1(%rip), %xmm9   # xmm9 = [1,1,1,1]
	movdqa	.LCPI5_2(%rip), %xmm10  # xmm10 = [2,2,2,2]
	movdqa	.LCPI5_3(%rip), %xmm11  # xmm11 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	movq	%r14, -16(%rsp)         # 8-byte Spill
	movq	%r12, -24(%rsp)         # 8-byte Spill
	movq	%r8, -32(%rsp)          # 8-byte Spill
	jmp	.LBB5_2
.LBB5_8:                                # %vector.body.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%rsi, %r14
	leaq	(%r10,%r14,2), %r8
	subl	%r14d, %ebp
	leaq	(%rbx,%r14,2), %rcx
	addq	%r14, %rax
	addq	$2, %rbx
	movq	%r14, %rdx
	movq	%r9, %r12
	.p2align	4, 0x90
.LBB5_9:                                # %vector.body
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-2(%r12), %xmm2
	movdqu	-1(%r12), %xmm7
	movdqu	(%r12), %xmm6
	movdqa	%xmm7, %xmm1
	punpckhbw	%xmm12, %xmm1   # xmm1 = xmm1[8],xmm12[8],xmm1[9],xmm12[9],xmm1[10],xmm12[10],xmm1[11],xmm12[11],xmm1[12],xmm12[12],xmm1[13],xmm12[13],xmm1[14],xmm12[14],xmm1[15],xmm12[15]
	pmullw	%xmm8, %xmm1
	movdqa	%xmm1, %xmm13
	punpckhwd	%xmm12, %xmm13  # xmm13 = xmm13[4],xmm12[4],xmm13[5],xmm12[5],xmm13[6],xmm12[6],xmm13[7],xmm12[7]
	punpcklwd	%xmm12, %xmm1   # xmm1 = xmm1[0],xmm12[0],xmm1[1],xmm12[1],xmm1[2],xmm12[2],xmm1[3],xmm12[3]
	punpcklbw	%xmm12, %xmm7   # xmm7 = xmm7[0],xmm12[0],xmm7[1],xmm12[1],xmm7[2],xmm12[2],xmm7[3],xmm12[3],xmm7[4],xmm12[4],xmm7[5],xmm12[5],xmm7[6],xmm12[6],xmm7[7],xmm12[7]
	pmullw	%xmm8, %xmm7
	movdqa	%xmm7, %xmm3
	punpckhwd	%xmm12, %xmm3   # xmm3 = xmm3[4],xmm12[4],xmm3[5],xmm12[5],xmm3[6],xmm12[6],xmm3[7],xmm12[7]
	punpcklwd	%xmm12, %xmm7   # xmm7 = xmm7[0],xmm12[0],xmm7[1],xmm12[1],xmm7[2],xmm12[2],xmm7[3],xmm12[3]
	pshufd	$78, %xmm2, %xmm4       # xmm4 = xmm2[2,3,0,1]
	punpcklbw	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1],xmm4[2],xmm12[2],xmm4[3],xmm12[3],xmm4[4],xmm12[4],xmm4[5],xmm12[5],xmm4[6],xmm12[6],xmm4[7],xmm12[7]
	movdqa	%xmm4, %xmm0
	punpckhwd	%xmm12, %xmm0   # xmm0 = xmm0[4],xmm12[4],xmm0[5],xmm12[5],xmm0[6],xmm12[6],xmm0[7],xmm12[7]
	punpcklwd	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1],xmm4[2],xmm12[2],xmm4[3],xmm12[3]
	punpcklbw	%xmm12, %xmm2   # xmm2 = xmm2[0],xmm12[0],xmm2[1],xmm12[1],xmm2[2],xmm12[2],xmm2[3],xmm12[3],xmm2[4],xmm12[4],xmm2[5],xmm12[5],xmm2[6],xmm12[6],xmm2[7],xmm12[7]
	movdqa	%xmm2, %xmm5
	punpckhwd	%xmm12, %xmm5   # xmm5 = xmm5[4],xmm12[4],xmm5[5],xmm12[5],xmm5[6],xmm12[6],xmm5[7],xmm12[7]
	punpcklwd	%xmm12, %xmm2   # xmm2 = xmm2[0],xmm12[0],xmm2[1],xmm12[1],xmm2[2],xmm12[2],xmm2[3],xmm12[3]
	paddd	%xmm9, %xmm0
	paddd	%xmm13, %xmm0
	paddd	%xmm9, %xmm4
	paddd	%xmm1, %xmm4
	paddd	%xmm9, %xmm5
	paddd	%xmm3, %xmm5
	paddd	%xmm9, %xmm2
	paddd	%xmm7, %xmm2
	psrld	$2, %xmm2
	psrld	$2, %xmm5
	psrld	$2, %xmm4
	psrld	$2, %xmm0
	pand	%xmm11, %xmm0
	pand	%xmm11, %xmm4
	packuswb	%xmm0, %xmm4
	pand	%xmm11, %xmm5
	pand	%xmm11, %xmm2
	packuswb	%xmm5, %xmm2
	packuswb	%xmm4, %xmm2
	pshufd	$78, %xmm6, %xmm4       # xmm4 = xmm6[2,3,0,1]
	punpcklbw	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1],xmm4[2],xmm12[2],xmm4[3],xmm12[3],xmm4[4],xmm12[4],xmm4[5],xmm12[5],xmm4[6],xmm12[6],xmm4[7],xmm12[7]
	movdqa	%xmm4, %xmm0
	punpckhwd	%xmm12, %xmm0   # xmm0 = xmm0[4],xmm12[4],xmm0[5],xmm12[5],xmm0[6],xmm12[6],xmm0[7],xmm12[7]
	punpcklwd	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1],xmm4[2],xmm12[2],xmm4[3],xmm12[3]
	punpcklbw	%xmm12, %xmm6   # xmm6 = xmm6[0],xmm12[0],xmm6[1],xmm12[1],xmm6[2],xmm12[2],xmm6[3],xmm12[3],xmm6[4],xmm12[4],xmm6[5],xmm12[5],xmm6[6],xmm12[6],xmm6[7],xmm12[7]
	movdqa	%xmm6, %xmm5
	punpckhwd	%xmm12, %xmm5   # xmm5 = xmm5[4],xmm12[4],xmm5[5],xmm12[5],xmm5[6],xmm12[6],xmm5[7],xmm12[7]
	punpcklwd	%xmm12, %xmm6   # xmm6 = xmm6[0],xmm12[0],xmm6[1],xmm12[1],xmm6[2],xmm12[2],xmm6[3],xmm12[3]
	paddd	%xmm10, %xmm0
	paddd	%xmm13, %xmm0
	paddd	%xmm10, %xmm4
	paddd	%xmm1, %xmm4
	paddd	%xmm10, %xmm5
	paddd	%xmm3, %xmm5
	paddd	%xmm10, %xmm6
	paddd	%xmm7, %xmm6
	psrld	$2, %xmm6
	psrld	$2, %xmm5
	psrld	$2, %xmm4
	psrld	$2, %xmm0
	pand	%xmm11, %xmm0
	pand	%xmm11, %xmm4
	packuswb	%xmm0, %xmm4
	pand	%xmm11, %xmm5
	pand	%xmm11, %xmm6
	packuswb	%xmm5, %xmm6
	packuswb	%xmm4, %xmm6
	movdqa	%xmm2, %xmm0
	punpcklbw	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0],xmm0[1],xmm6[1],xmm0[2],xmm6[2],xmm0[3],xmm6[3],xmm0[4],xmm6[4],xmm0[5],xmm6[5],xmm0[6],xmm6[6],xmm0[7],xmm6[7]
	punpckhbw	%xmm6, %xmm2    # xmm2 = xmm2[8],xmm6[8],xmm2[9],xmm6[9],xmm2[10],xmm6[10],xmm2[11],xmm6[11],xmm2[12],xmm6[12],xmm2[13],xmm6[13],xmm2[14],xmm6[14],xmm2[15],xmm6[15]
	movdqu	%xmm2, 16(%rbx)
	movdqu	%xmm0, (%rbx)
	addq	$16, %r12
	addq	$32, %rbx
	addq	$-16, %r14
	jne	.LBB5_9
# BB#10:                                # %middle.block
                                        #   in Loop: Header=BB5_2 Depth=1
	cmpq	%rdx, %rdi
	movq	-32(%rsp), %rdi         # 8-byte Reload
	movq	-24(%rsp), %r12         # 8-byte Reload
	movq	-16(%rsp), %r14         # 8-byte Reload
	movq	-40(%rsp), %rsi         # 8-byte Reload
	jne	.LBB5_11
	jmp	.LBB5_12
	.p2align	4, 0x90
.LBB5_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_9 Depth 2
                                        #     Child Loop BB5_11 Depth 2
	movq	(%r8,%r13,8), %rcx
	movq	-8(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %rbx
	leaq	1(%rcx), %rax
	movzbl	(%rcx), %edx
	movb	%dl, (%rbx)
	leal	(%rdx,%rdx,2), %ebp
	movzbl	1(%rcx), %edx
	leal	2(%rdx,%rbp), %edx
	shrl	$2, %edx
	movb	%dl, 1(%rbx)
	movl	40(%r12), %r11d
	leaq	2(%rbx), %r10
	movl	%r11d, %ebp
	addl	$-2, %ebp
	je	.LBB5_13
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	addl	$-3, %r11d
	leaq	(%r11,%r11), %rdx
	movq	%rdx, -40(%rsp)         # 8-byte Spill
	leaq	2(%rcx), %r9
	leaq	4(%rbx), %r15
	leaq	1(%r11), %rdi
	cmpq	$16, %rdi
	jb	.LBB5_4
# BB#5:                                 # %min.iters.checked
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%rdi, %rsi
	movabsq	$8589934576, %rdx       # imm = 0x1FFFFFFF0
	andq	%rdx, %rsi
	je	.LBB5_4
# BB#6:                                 # %vector.memcheck
                                        #   in Loop: Header=BB5_2 Depth=1
	leaq	3(%rcx,%r11), %rdx
	cmpq	%rdx, %r10
	jae	.LBB5_8
# BB#7:                                 # %vector.memcheck
                                        #   in Loop: Header=BB5_2 Depth=1
	leaq	4(%rbx,%r11,2), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB5_8
	.p2align	4, 0x90
.LBB5_4:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%r8, %rdi
	movq	%r10, %r8
	movq	%rbx, %rcx
	movq	-40(%rsp), %rsi         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_11:                               # %.lr.ph
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %edx
	leal	(%rdx,%rdx,2), %ebx
	movzbl	-1(%rax), %edx
	leal	1(%rdx,%rbx), %edx
	shrl	$2, %edx
	movb	%dl, (%r8)
	movzbl	1(%rax), %edx
	incq	%rax
	leal	2(%rdx,%rbx), %edx
	shrl	$2, %edx
	movb	%dl, 3(%rcx)
	movq	%r8, %rcx
	leaq	2(%r8), %r8
	decl	%ebp
	jne	.LBB5_11
.LBB5_12:                               # %._crit_edge.loopexit
                                        #   in Loop: Header=BB5_2 Depth=1
	addq	%rsi, %r10
	addq	%r11, %r9
	addq	%rsi, %r15
	movq	%r9, %rax
	movq	%r10, %rbx
	movq	%r15, %r10
	movq	%rdi, %r8
.LBB5_13:                               # %._crit_edge
                                        #   in Loop: Header=BB5_2 Depth=1
	movzbl	(%rax), %ecx
	leal	(%rcx,%rcx,2), %edx
	movzbl	-1(%rax), %eax
	leal	1(%rax,%rdx), %eax
	shrl	$2, %eax
	movb	%al, (%r10)
	movb	%cl, 3(%rbx)
	incq	%r13
	movslq	392(%r14), %rax
	cmpq	%rax, %r13
	jl	.LBB5_2
.LBB5_14:                               # %._crit_edge52
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	h2v1_fancy_upsample, .Lfunc_end5-h2v1_fancy_upsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	h2v1_upsample,@function
h2v1_upsample:                          # @h2v1_upsample
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 16
.Lcfi39:
	.cfi_offset %rbx, -16
	movl	392(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB6_10
# BB#1:                                 # %.lr.ph31
	movq	(%rcx), %r8
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB6_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_5 Depth 2
                                        #     Child Loop BB6_7 Depth 2
	movl	128(%rdi), %r11d
	testq	%r11, %r11
	je	.LBB6_9
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	(%r8,%r9,8), %rax
	addq	%rax, %r11
	movq	(%rdx,%r9,8), %rcx
	leaq	2(%rax), %r10
	cmpq	%r10, %r11
	cmovaq	%r11, %r10
	subq	%rax, %r10
	decq	%r10
	movl	%r10d, %esi
	shrl	%esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB6_6
# BB#4:                                 # %.lr.ph.prol.preheader
                                        #   in Loop: Header=BB6_2 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph.prol
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %ebx
	incq	%rcx
	movb	%bl, (%rax)
	movb	%bl, 1(%rax)
	addq	$2, %rax
	incq	%rsi
	jne	.LBB6_5
.LBB6_6:                                # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB6_2 Depth=1
	cmpq	$14, %r10
	jb	.LBB6_8
	.p2align	4, 0x90
.LBB6_7:                                # %.lr.ph
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %ebx
	movb	%bl, (%rax)
	movb	%bl, 1(%rax)
	movzbl	1(%rcx), %ebx
	movb	%bl, 2(%rax)
	movb	%bl, 3(%rax)
	movzbl	2(%rcx), %ebx
	movb	%bl, 4(%rax)
	movb	%bl, 5(%rax)
	movzbl	3(%rcx), %ebx
	movb	%bl, 6(%rax)
	movb	%bl, 7(%rax)
	movzbl	4(%rcx), %ebx
	movb	%bl, 8(%rax)
	movb	%bl, 9(%rax)
	movzbl	5(%rcx), %ebx
	movb	%bl, 10(%rax)
	movb	%bl, 11(%rax)
	movzbl	6(%rcx), %ebx
	movb	%bl, 12(%rax)
	movb	%bl, 13(%rax)
	movzbl	7(%rcx), %ebx
	movb	%bl, 14(%rax)
	movb	%bl, 15(%rax)
	addq	$16, %rax
	addq	$8, %rcx
	cmpq	%r11, %rax
	jb	.LBB6_7
.LBB6_8:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	392(%rdi), %eax
.LBB6_9:                                # %._crit_edge
                                        #   in Loop: Header=BB6_2 Depth=1
	incq	%r9
	movslq	%eax, %rcx
	cmpq	%rcx, %r9
	jl	.LBB6_2
.LBB6_10:                               # %._crit_edge32
	popq	%rbx
	retq
.Lfunc_end6:
	.size	h2v1_upsample, .Lfunc_end6-h2v1_upsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	h2v2_fancy_upsample,@function
h2v2_fancy_upsample:                    # @h2v2_fancy_upsample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	cmpl	$0, 392(%rdi)
	jle	.LBB7_13
# BB#1:                                 # %.preheader.lr.ph
	movq	(%rcx), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB7_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_5 Depth 2
                                        #     Child Loop BB7_10 Depth 2
	movslq	%r10d, %r10
	movq	(%rdx,%r9,8), %rcx
	movq	-8(%rdx,%r9,8), %rbx
	movq	-24(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r10,8), %r11
	movzbl	(%rcx), %eax
	leal	(%rax,%rax,2), %eax
	movzbl	(%rbx), %r15d
	addl	%eax, %r15d
	movzbl	1(%rcx), %eax
	leal	(%rax,%rax,2), %edi
	movzbl	1(%rbx), %eax
	addl	%edi, %eax
	leal	8(,%r15,4), %edi
	shrl	$4, %edi
	movb	%dil, (%r11)
	leal	(%r15,%r15,2), %edi
	leal	7(%rdi,%rax), %edi
	shrl	$4, %edi
	movb	%dil, 1(%r11)
	movq	-16(%rsp), %rsi         # 8-byte Reload
	movl	40(%rsi), %edi
	leaq	2(%r11), %r8
	cmpl	$2, %edi
	je	.LBB7_3
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	addq	$2, %rbx
	addq	$2, %rcx
	leal	-3(%rdi), %r12d
	addq	%r12, %r12
	addq	$4, %r11
	movl	$2, %ebp
	subl	%edi, %ebp
	movq	%r8, %r14
	.p2align	4, 0x90
.LBB7_5:                                # %.lr.ph
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %r13d
	movzbl	(%rcx), %eax
	incq	%rcx
	leal	(%rax,%rax,2), %edi
	movzbl	(%rbx), %eax
	incq	%rbx
	addl	%edi, %eax
	leal	(%r13,%r13,2), %edi
	leal	8(%r15,%rdi), %esi
	shrl	$4, %esi
	movb	%sil, (%r14)
	leal	7(%rdi,%rax), %esi
	shrl	$4, %esi
	movb	%sil, 1(%r14)
	addq	$2, %r14
	incl	%ebp
	movl	%r13d, %r15d
	jne	.LBB7_5
# BB#6:                                 # %._crit_edge.loopexit
                                        #   in Loop: Header=BB7_2 Depth=1
	addq	%r12, %r8
	movq	%r11, %rcx
	addq	%r12, %rcx
	movq	%r8, %r11
	movq	%rcx, %r8
	jmp	.LBB7_7
	.p2align	4, 0x90
.LBB7_3:                                #   in Loop: Header=BB7_2 Depth=1
	movl	%r15d, %r13d
.LBB7_7:                                # %._crit_edge
                                        #   in Loop: Header=BB7_2 Depth=1
	leal	(%rax,%rax,2), %ecx
	leal	8(%r13,%rcx), %ecx
	shrl	$4, %ecx
	movb	%cl, (%r8)
	leal	7(,%rax,4), %eax
	shrl	$4, %eax
	movb	%al, 3(%r11)
	movq	(%rdx,%r9,8), %r8
	movq	8(%rdx,%r9,8), %rbx
	movq	-24(%rsp), %rax         # 8-byte Reload
	movq	8(%rax,%r10,8), %r11
	movzbl	(%r8), %eax
	leal	(%rax,%rax,2), %eax
	movzbl	(%rbx), %r15d
	addl	%eax, %r15d
	movzbl	1(%r8), %eax
	leal	(%rax,%rax,2), %esi
	movzbl	1(%rbx), %eax
	addl	%esi, %eax
	leal	8(,%r15,4), %esi
	shrl	$4, %esi
	movb	%sil, (%r11)
	leal	(%r15,%r15,2), %esi
	leal	7(%rsi,%rax), %esi
	shrl	$4, %esi
	movb	%sil, 1(%r11)
	movq	-16(%rsp), %rcx         # 8-byte Reload
	movl	40(%rcx), %edi
	leaq	2(%r11), %r14
	cmpl	$2, %edi
	je	.LBB7_8
# BB#9:                                 # %.lr.ph.preheader.1
                                        #   in Loop: Header=BB7_2 Depth=1
	addq	$2, %rbx
	addq	$2, %r8
	leal	-3(%rdi), %r13d
	addq	%r13, %r13
	addq	$4, %r11
	movl	$2, %ebp
	subl	%edi, %ebp
	movq	%r14, %rdi
	.p2align	4, 0x90
.LBB7_10:                               # %.lr.ph.1
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %r12d
	movzbl	(%r8), %eax
	incq	%r8
	leal	(%rax,%rax,2), %esi
	movzbl	(%rbx), %eax
	incq	%rbx
	addl	%esi, %eax
	leal	(%r12,%r12,2), %esi
	leal	8(%r15,%rsi), %ecx
	shrl	$4, %ecx
	movb	%cl, (%rdi)
	leal	7(%rsi,%rax), %ecx
	shrl	$4, %ecx
	movb	%cl, 1(%rdi)
	addq	$2, %rdi
	incl	%ebp
	movl	%r12d, %r15d
	jne	.LBB7_10
# BB#11:                                # %._crit_edge.loopexit.1
                                        #   in Loop: Header=BB7_2 Depth=1
	addq	%r13, %r14
	movq	%r11, %rcx
	addq	%r13, %rcx
	movq	%r14, %r11
	movq	%rcx, %r14
	jmp	.LBB7_12
	.p2align	4, 0x90
.LBB7_8:                                #   in Loop: Header=BB7_2 Depth=1
	movl	%r15d, %r12d
.LBB7_12:                               # %._crit_edge.1
                                        #   in Loop: Header=BB7_2 Depth=1
	incq	%r9
	leal	(%rax,%rax,2), %ecx
	leal	8(%r12,%rcx), %ecx
	shrl	$4, %ecx
	movb	%cl, (%r14)
	leal	7(,%rax,4), %eax
	shrl	$4, %eax
	movb	%al, 3(%r11)
	addq	$2, %r10
	movq	-8(%rsp), %rax          # 8-byte Reload
	cmpl	392(%rax), %r10d
	jl	.LBB7_2
.LBB7_13:                               # %._crit_edge87
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	h2v2_fancy_upsample, .Lfunc_end7-h2v2_fancy_upsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	h2v2_upsample,@function
h2v2_upsample:                          # @h2v2_upsample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi58:
	.cfi_def_cfa_offset 64
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %r15
	cmpl	$0, 392(%r15)
	jle	.LBB8_10
# BB#1:                                 # %.lr.ph41
	movq	(%rcx), %r12
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_5 Depth 2
                                        #     Child Loop BB8_7 Depth 2
	movl	128(%r15), %eax
	testq	%rax, %rax
	movl	$0, %r9d
	je	.LBB8_9
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	(%r12,%rbp,8), %rcx
	addq	%rcx, %rax
	movq	(%r14,%r13,8), %rdx
	leaq	2(%rcx), %rsi
	cmpq	%rsi, %rax
	cmovaq	%rax, %rsi
	subq	%rcx, %rsi
	decq	%rsi
	movl	%esi, %edi
	shrl	%edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB8_6
# BB#4:                                 # %.lr.ph.prol.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB8_5:                                # %.lr.ph.prol
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdx), %ebx
	incq	%rdx
	movb	%bl, (%rcx)
	movb	%bl, 1(%rcx)
	addq	$2, %rcx
	incq	%rdi
	jne	.LBB8_5
.LBB8_6:                                # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB8_2 Depth=1
	cmpq	$14, %rsi
	jb	.LBB8_8
	.p2align	4, 0x90
.LBB8_7:                                # %.lr.ph
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdx), %ebx
	movb	%bl, (%rcx)
	movb	%bl, 1(%rcx)
	movzbl	1(%rdx), %ebx
	movb	%bl, 2(%rcx)
	movb	%bl, 3(%rcx)
	movzbl	2(%rdx), %ebx
	movb	%bl, 4(%rcx)
	movb	%bl, 5(%rcx)
	movzbl	3(%rdx), %ebx
	movb	%bl, 6(%rcx)
	movb	%bl, 7(%rcx)
	movzbl	4(%rdx), %ebx
	movb	%bl, 8(%rcx)
	movb	%bl, 9(%rcx)
	movzbl	5(%rdx), %ebx
	movb	%bl, 10(%rcx)
	movb	%bl, 11(%rcx)
	movzbl	6(%rdx), %ebx
	movb	%bl, 12(%rcx)
	movb	%bl, 13(%rcx)
	movzbl	7(%rdx), %ebx
	movb	%bl, 14(%rcx)
	movb	%bl, 15(%rcx)
	addq	$16, %rcx
	addq	$8, %rdx
	cmpq	%rax, %rcx
	jb	.LBB8_7
.LBB8_8:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB8_2 Depth=1
	movl	128(%r15), %r9d
.LBB8_9:                                # %._crit_edge
                                        #   in Loop: Header=BB8_2 Depth=1
	movl	%ebp, %ecx
	orl	$1, %ecx
	movl	$1, %r8d
	movq	%r12, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	callq	jcopy_sample_rows
	incq	%r13
	addq	$2, %rbp
	movslq	392(%r15), %rax
	cmpq	%rax, %rbp
	jl	.LBB8_2
.LBB8_10:                               # %._crit_edge42
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	h2v2_upsample, .Lfunc_end8-h2v2_upsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	int_upsample,@function
int_upsample:                           # @int_upsample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi71:
	.cfi_def_cfa_offset 112
.Lcfi72:
	.cfi_offset %rbx, -56
.Lcfi73:
	.cfi_offset %r12, -48
.Lcfi74:
	.cfi_offset %r13, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	movl	392(%r14), %eax
	testl	%eax, %eax
	jle	.LBB9_24
# BB#1:                                 # %.lr.ph62
	movq	592(%r14), %rdx
	movq	(%rcx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movslq	4(%rsi), %rcx
	movzbl	232(%rdx,%rcx), %r12d
	movzbl	242(%rdx,%rcx), %esi
	cmpb	$2, %sil
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%r14, 40(%rsp)          # 8-byte Spill
	jb	.LBB9_12
# BB#2:                                 # %.lr.ph62.split.us.preheader
	movl	%esi, %eax
	decl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	%r12d, %eax
	notl	%eax
	testb	%r12b, %r12b
	movl	$-2, %ecx
	cmovel	%eax, %ecx
	leal	1(%rcx,%r12), %ebp
	incq	%rbp
	leal	1(%r12), %r15d
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB9_3:                                # %.lr.ph62.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_9 Depth 2
                                        #       Child Loop BB9_10 Depth 3
	movl	128(%r14), %r13d
	testq	%r13, %r13
	movl	$0, %r9d
	je	.LBB9_7
# BB#4:                                 # %.lr.ph58.us
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rax,%rcx,8), %r14
	testb	%r12b, %r12b
	je	.LBB9_5
# BB#8:                                 # %.lr.ph.us.us.preheader
                                        #   in Loop: Header=BB9_3 Depth=1
	addq	%r14, %r13
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rbx
	.p2align	4, 0x90
.LBB9_9:                                # %.lr.ph.us.us
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_10 Depth 3
	movzbl	(%rbx), %esi
	movq	%r14, %rdi
	movq	%rbp, %rdx
	callq	memset
	movl	%r15d, %eax
	.p2align	4, 0x90
.LBB9_10:                               #   Parent Loop BB9_3 Depth=1
                                        #     Parent Loop BB9_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incq	%r14
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB9_10
# BB#11:                                # %..loopexit_crit_edge.us.us
                                        #   in Loop: Header=BB9_9 Depth=2
	incq	%rbx
	cmpq	%r13, %r14
	jb	.LBB9_9
# BB#6:                                 # %._crit_edge.us.loopexit
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	128(%r14), %r9d
	movq	48(%rsp), %rsi          # 8-byte Reload
.LBB9_7:                                # %._crit_edge.us
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	(%rsp), %rbx            # 8-byte Reload
	leal	1(%rbx), %ecx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rsi, %r13
	movl	%ebx, %esi
	movq	%rdi, %rdx
	movl	20(%rsp), %r8d          # 4-byte Reload
	callq	jcopy_sample_rows
	movq	%r13, %rsi
	incq	32(%rsp)                # 8-byte Folded Spill
	addq	%rsi, %rbx
	movslq	392(%r14), %rax
	movq	%rbx, %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	cmpq	%rax, %rbx
	jl	.LBB9_3
	jmp	.LBB9_24
	.p2align	4, 0x90
.LBB9_5:                                # %.loopexit.us68
                                        # =>This Inner Loop Header: Depth=1
	jmp	.LBB9_5
.LBB9_12:                               # %.lr.ph62.split
	testb	%r12b, %r12b
	je	.LBB9_13
# BB#16:                                # %.lr.ph62.split.split.us.preheader
	leal	-1(%r12), %r13d
	incq	%r13
	incl	%r12d
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB9_17:                               # %.lr.ph62.split.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_19 Depth 2
                                        #       Child Loop BB9_20 Depth 3
	movl	128(%r14), %ebx
	testq	%rbx, %rbx
	je	.LBB9_22
# BB#18:                                # %.lr.ph.us.us94.preheader
                                        #   in Loop: Header=BB9_17 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r15,8), %rbp
	addq	%rbp, %rbx
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rax,%rcx,8), %r14
	.p2align	4, 0x90
.LBB9_19:                               # %.lr.ph.us.us94
                                        #   Parent Loop BB9_17 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_20 Depth 3
	movzbl	(%r14), %esi
	movq	%rbp, %rdi
	movq	%r13, %rdx
	callq	memset
	movl	%r12d, %eax
	.p2align	4, 0x90
.LBB9_20:                               #   Parent Loop BB9_17 Depth=1
                                        #     Parent Loop BB9_19 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incq	%rbp
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB9_20
# BB#21:                                # %..loopexit_crit_edge.us.us95
                                        #   in Loop: Header=BB9_19 Depth=2
	incq	%r14
	cmpq	%rbx, %rbp
	jb	.LBB9_19
.LBB9_22:                               # %._crit_edge.us-lcssa.us.us100
                                        #   in Loop: Header=BB9_17 Depth=1
	incq	(%rsp)                  # 8-byte Folded Spill
	addq	48(%rsp), %r15          # 8-byte Folded Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	movslq	392(%r14), %rax
	cmpq	%rax, %r15
	jl	.LBB9_17
	jmp	.LBB9_24
.LBB9_13:                               # %.lr.ph62.split.split.preheader
	movl	128(%r14), %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_14:                               # %.lr.ph62.split.split
                                        # =>This Inner Loop Header: Depth=1
	testl	%ecx, %ecx
	jne	.LBB9_15
# BB#23:                                # %._crit_edge
                                        #   in Loop: Header=BB9_14 Depth=1
	addl	%esi, %edx
	cmpl	%eax, %edx
	jl	.LBB9_14
.LBB9_24:                               # %._crit_edge63
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.p2align	4, 0x90
.LBB9_15:                               # %.lr.ph58.split
                                        # =>This Inner Loop Header: Depth=1
	jmp	.LBB9_15
.Lfunc_end9:
	.size	int_upsample, .Lfunc_end9-int_upsample
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
