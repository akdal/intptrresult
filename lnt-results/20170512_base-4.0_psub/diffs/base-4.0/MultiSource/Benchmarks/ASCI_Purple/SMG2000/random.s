	.text
	.file	"random.bc"
	.globl	hypre_SeedRand
	.p2align	4, 0x90
	.type	hypre_SeedRand,@function
hypre_SeedRand:                         # @hypre_SeedRand
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	-1(%rdi,%rdi), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	leal	-1(%rcx,%rdi,2), %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %eax
	movl	%eax, Seed(%rip)
	retq
.Lfunc_end0:
	.size	hypre_SeedRand, .Lfunc_end0-hypre_SeedRand
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4562146422526312448     # double 9.765625E-4
	.text
	.globl	hypre_Rand
	.p2align	4, 0x90
	.type	hypre_Rand,@function
hypre_Rand:                             # @hypre_Rand
	.cfi_startproc
# BB#0:
	imull	$1664525, Seed(%rip), %eax # imm = 0x19660D
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$22, %ecx
	addl	%eax, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	subl	%ecx, %eax
	movl	%eax, Seed(%rip)
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI1_0(%rip), %xmm0
	retq
.Lfunc_end1:
	.size	hypre_Rand, .Lfunc_end1-hypre_Rand
	.cfi_endproc

	.type	Seed,@object            # @Seed
	.data
	.p2align	2
Seed:
	.long	13579                   # 0x350b
	.size	Seed, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
