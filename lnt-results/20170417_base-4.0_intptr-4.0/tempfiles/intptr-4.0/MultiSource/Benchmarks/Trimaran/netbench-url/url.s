	.text
	.file	"url.bc"
	.globl	internet_checksum
	.p2align	4, 0x90
	.type	internet_checksum,@function
internet_checksum:                      # @internet_checksum
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	cmpl	$2, %esi
	jl	.LBB0_1
# BB#2:                                 # %.lr.ph19.preheader
	leal	-2(%rsi), %r9d
	movl	%r9d, %ecx
	shrl	%ecx
	leal	1(%rcx), %r8d
	addl	%ecx, %ecx
	cmpl	$4, %r8d
	jb	.LBB0_3
# BB#4:                                 # %min.iters.checked
	movl	%r8d, %ebx
	movl	%r8d, %r10d
	andl	$3, %r10d
	movq	%rbx, %rax
	subq	%r10, %rax
	subq	%r10, %rbx
	je	.LBB0_3
# BB#5:                                 # %vector.body.preheader
	addl	%eax, %eax
	subl	%eax, %esi
	leaq	(%rdi,%rbx,2), %rdx
	leaq	4(%rdi), %rax
	pxor	%xmm1, %xmm1
	pcmpeqd	%xmm2, %xmm2
	pxor	%xmm3, %xmm3
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_6:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movd	(%rax), %xmm5           # xmm5 = mem[0],zero,zero,zero
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	punpcklwd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1],xmm5[2],xmm1[2],xmm5[3],xmm1[3]
	punpckldq	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	pxor	%xmm2, %xmm4
	pxor	%xmm2, %xmm5
	paddq	%xmm4, %xmm3
	paddq	%xmm5, %xmm0
	addq	$8, %rax
	addq	$-4, %rbx
	jne	.LBB0_6
# BB#7:                                 # %middle.block
	paddq	%xmm3, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm0, %xmm1
	movd	%xmm1, %rbx
	testl	%r10d, %r10d
	jne	.LBB0_8
	jmp	.LBB0_9
.LBB0_1:
	xorl	%ebx, %ebx
	cmpl	$1, %esi
	je	.LBB0_11
	jmp	.LBB0_14
.LBB0_3:
	xorl	%ebx, %ebx
	movq	%rdi, %rdx
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph19
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rdx), %eax
	addq	$2, %rdx
	notq	%rax
	addq	%rax, %rbx
	addl	$-2, %esi
	cmpl	$1, %esi
	jg	.LBB0_8
.LBB0_9:                                # %._crit_edge20.loopexit
	leaq	(%rdi,%r8,2), %rdi
	subl	%ecx, %r9d
	movl	%r9d, %esi
	cmpl	$1, %esi
	jne	.LBB0_14
.LBB0_11:
	movzbl	(%rdi), %eax
	jmp	.LBB0_13
.LBB0_12:                               # %.lr.ph
	movzwl	%bx, %ebx
.LBB0_13:                               # %.lr.ph
	addq	%rax, %rbx
.LBB0_14:                               # %.lr.ph
	movq	%rbx, %rax
	shrq	$16, %rax
	jne	.LBB0_12
# BB#15:                                # %._crit_edge
	movzbl	%bh, %ecx  # NOREX
	shlq	$8, %rbx
	movzwl	%bx, %eax
	orq	%rcx, %rax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	internet_checksum, .Lfunc_end0-internet_checksum
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	cmpl	$3, %edi
	jne	.LBB1_7
# BB#1:
	movq	16(%rbx), %rdi
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
	movq	8(%rbx), %rdi
	callq	db_init
	movq	stdout(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	testl	%r14d, %r14d
	je	.LBB1_6
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
	movl	%r15d, %edi
	callq	get_next_packet
	movq	%rax, %rbx
	movw	$0, 10(%rbx)
	movl	%r15d, %edi
	callq	packet_size
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	find_destination
	movzwl	(%rbx), %eax
	notq	%rax
	movzwl	2(%rbx), %ecx
	notq	%rcx
	addq	%rax, %rcx
	movzwl	4(%rbx), %eax
	notq	%rax
	addq	%rcx, %rax
	movzwl	6(%rbx), %ecx
	notq	%rcx
	addq	%rax, %rcx
	movzwl	8(%rbx), %eax
	notq	%rax
	addq	%rcx, %rax
	movzwl	10(%rbx), %ecx
	notq	%rcx
	addq	%rax, %rcx
	movzwl	12(%rbx), %eax
	notq	%rax
	addq	%rcx, %rax
	movzwl	14(%rbx), %ecx
	notq	%rcx
	addq	%rax, %rcx
	movzwl	16(%rbx), %edx
	notq	%rdx
	addq	%rcx, %rdx
	movzwl	18(%rbx), %eax
	notq	%rax
	addq	%rdx, %rax
	jmp	.LBB1_4
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph.i
                                        #   in Loop: Header=BB1_4 Depth=2
	movzwl	%ax, %eax
	addq	%rcx, %rax
.LBB1_4:                                # %.lr.ph.i
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rcx
	shrq	$16, %rcx
	jne	.LBB1_3
# BB#5:                                 # %internet_checksum.exit
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%rbx, %rdi
	callq	free
	incl	%r15d
	cmpl	%r14d, %r15d
	jne	.LBB1_2
.LBB1_6:                                # %._crit_edge
	movq	stdout(%rip), %rdi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB1_7:
	movl	$.L.str, %edi
	movl	$.L__FUNCTION__.main, %esi
	movl	$102, %edx
	movl	$.L.str.1, %ecx
	xorl	%eax, %eax
	callq	_fatal
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Benchmarks/Trimaran/netbench-url/url.c"
	.size	.L.str, 89

	.type	.L__FUNCTION__.main,@object # @__FUNCTION__.main
.L__FUNCTION__.main:
	.asciz	"main"
	.size	.L__FUNCTION__.main, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Usage: url inputfilename #numberofpackets"
	.size	.L.str.1, 42

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"URL table initialized, reading packets... \n"
	.size	.L.str.2, 44

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"URL finished for %d packets\n"
	.size	.L.str.3, 29

	.type	tree_head,@object       # @tree_head
	.comm	tree_head,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
