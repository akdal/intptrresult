	.text
	.file	"UTFConvert.bc"
	.globl	_Z20ConvertUTF8ToUnicodeRK11CStringBaseIcERS_IwE
	.p2align	4, 0x90
	.type	_Z20ConvertUTF8ToUnicodeRK11CStringBaseIcERS_IwE,@function
_Z20ConvertUTF8ToUnicodeRK11CStringBaseIcERS_IwE: # @_Z20ConvertUTF8ToUnicodeRK11CStringBaseIcERS_IwE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	$0, 8(%rsi)
	movq	(%rsi), %r10
	movl	$0, (%r10)
	movq	(%rdi), %r12
	movslq	8(%rdi), %r13
	xorl	%r14d, %r14d
	testq	%r13, %r13
	je	.LBB0_19
# BB#1:                                 # %.lr.ph.split.us.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_10 Depth 2
	movzbl	(%r12,%rax), %ecx
	incq	%rax
	testb	%cl, %cl
	js	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	incq	%r14
	cmpq	%r13, %rax
	jne	.LBB0_2
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_2 Depth=1
	cmpb	$-64, %cl
	jb	.LBB0_19
# BB#5:                                 # %.preheader.us.preheader.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	$1, %edx
	cmpb	$-32, %cl
	jb	.LBB0_9
# BB#6:                                 # %.preheader.us.191.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	$2, %edx
	cmpb	$-17, %cl
	jbe	.LBB0_9
# BB#7:                                 # %.preheader.us.292.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	$3, %edx
	cmpb	$-8, %cl
	jb	.LBB0_9
# BB#8:                                 # %.preheader.us.393.i
                                        #   in Loop: Header=BB0_2 Depth=1
	xorl	%edx, %edx
	cmpb	$-5, %cl
	seta	%dl
	orl	$4, %edx
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%edx, %rbp
	movzbl	_ZL11kUtf8Limits-1(%rbp), %ebp
	subl	%ebp, %ecx
	.p2align	4, 0x90
.LBB0_10:                               #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r13
	je	.LBB0_14
# BB#11:                                #   in Loop: Header=BB0_10 Depth=2
	movzbl	(%r12,%rax), %ebx
	incq	%rax
	testb	%bl, %bl
	jns	.LBB0_15
# BB#12:                                #   in Loop: Header=BB0_10 Depth=2
	cmpb	$-65, %bl
	ja	.LBB0_15
# BB#13:                                #   in Loop: Header=BB0_10 Depth=2
	movl	%ecx, %ebp
	movzbl	%bl, %ecx
	shll	$6, %ebp
	addl	$-128, %ecx
	orl	%ebp, %ecx
	decl	%edx
	jne	.LBB0_10
	jmp	.LBB0_15
.LBB0_14:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %rax
.LBB0_15:                               # %.thread.us.i
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$65536, %ecx            # imm = 0x10000
	jae	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$1, %ecx
	addq	%rcx, %r14
	cmpq	%r13, %rax
	jne	.LBB0_2
	jmp	.LBB0_19
.LBB0_17:                               #   in Loop: Header=BB0_2 Depth=1
	addl	$-65536, %ecx           # imm = 0xFFFF0000
	cmpl	$1048575, %ecx          # imm = 0xFFFFF
	ja	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$2, %ecx
	addq	%rcx, %r14
	cmpq	%r13, %rax
	jne	.LBB0_2
.LBB0_19:                               # %_ZL13Utf8_To_Utf16PwPmPKcm.exit
	movl	12(%rsi), %r15d
	cmpl	%r14d, %r15d
	jg	.LBB0_26
# BB#20:
	incl	%r14d
	cmpl	%r15d, %r14d
	je	.LBB0_26
# BB#21:
	movq	%r10, %rbp
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	xorl	%eax, %eax
	testl	%r15d, %r15d
	jle	.LBB0_24
# BB#22:
	testq	%rbp, %rbp
	movq	8(%rsp), %rsi           # 8-byte Reload
	je	.LBB0_25
# BB#23:                                # %._crit_edge.thread.i.i
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %r12
	movl	8(%rbp), %r13d
	movslq	8(%rsi), %rax
	jmp	.LBB0_25
.LBB0_24:
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB0_25:                               # %._crit_edge16.i.i
	movq	%rbx, (%rsi)
	movl	$0, (%rbx,%rax,4)
	movl	%r14d, 12(%rsi)
	movq	%rbx, %r10
.LBB0_26:                               # %_ZN11CStringBaseIwE9GetBufferEi.exit
	movl	$1, %eax
	testl	%r13d, %r13d
	je	.LBB0_66
# BB#27:                                # %.lr.ph.i14
	movslq	%r13d, %r9
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	testq	%r10, %r10
	je	.LBB0_47
	.p2align	4, 0x90
.LBB0_28:                               # %.lr.ph.split.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_36 Depth 2
	movzbl	(%r12,%rdx), %ebx
	incq	%rdx
	testb	%bl, %bl
	js	.LBB0_30
# BB#29:                                #   in Loop: Header=BB0_28 Depth=1
	movl	%ebx, (%r10,%r8,4)
	incq	%r8
	jmp	.LBB0_46
	.p2align	4, 0x90
.LBB0_30:                               #   in Loop: Header=BB0_28 Depth=1
	cmpb	$-64, %bl
	jb	.LBB0_70
# BB#31:                                # %.preheader.preheader.i
                                        #   in Loop: Header=BB0_28 Depth=1
	movl	$1, %ebp
	cmpb	$-32, %bl
	jb	.LBB0_35
# BB#32:                                # %.preheader.194.i
                                        #   in Loop: Header=BB0_28 Depth=1
	movl	$2, %ebp
	cmpb	$-17, %bl
	jbe	.LBB0_35
# BB#33:                                # %.preheader.295.i
                                        #   in Loop: Header=BB0_28 Depth=1
	movl	$3, %ebp
	cmpb	$-8, %bl
	jb	.LBB0_35
# BB#34:                                # %.preheader.396.i
                                        #   in Loop: Header=BB0_28 Depth=1
	xorl	%ebp, %ebp
	cmpb	$-5, %bl
	seta	%bpl
	orl	$4, %ebp
	.p2align	4, 0x90
.LBB0_35:                               #   in Loop: Header=BB0_28 Depth=1
	movslq	%ebp, %rcx
	movzbl	_ZL11kUtf8Limits-1(%rcx), %ecx
	subl	%ecx, %ebx
	.p2align	4, 0x90
.LBB0_36:                               #   Parent Loop BB0_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdx, %r9
	je	.LBB0_40
# BB#37:                                #   in Loop: Header=BB0_36 Depth=2
	movzbl	(%r12,%rdx), %ecx
	incq	%rdx
	testb	%cl, %cl
	jns	.LBB0_41
# BB#38:                                #   in Loop: Header=BB0_36 Depth=2
	cmpb	$-65, %cl
	ja	.LBB0_41
# BB#39:                                #   in Loop: Header=BB0_36 Depth=2
	movl	%ebx, %edi
	movzbl	%cl, %ebx
	shll	$6, %edi
	addl	$-128, %ebx
	orl	%edi, %ebx
	decl	%ebp
	jne	.LBB0_36
	jmp	.LBB0_41
.LBB0_40:                               #   in Loop: Header=BB0_28 Depth=1
	movq	%r9, %rdx
.LBB0_41:                               # %.thread.i
                                        #   in Loop: Header=BB0_28 Depth=1
	cmpl	$65536, %ebx            # imm = 0x10000
	jae	.LBB0_43
# BB#42:                                #   in Loop: Header=BB0_28 Depth=1
	movq	%r8, %rcx
	movl	$1, %ebp
	jmp	.LBB0_45
.LBB0_43:                               #   in Loop: Header=BB0_28 Depth=1
	addl	$-65536, %ebx           # imm = 0xFFFF0000
	cmpl	$1048575, %ebx          # imm = 0xFFFFF
	ja	.LBB0_70
# BB#44:                                #   in Loop: Header=BB0_28 Depth=1
	movl	%ebx, %ecx
	shrl	$10, %ecx
	addl	$55296, %ecx            # imm = 0xD800
	movl	%ecx, (%r10,%r8,4)
	andl	$1023, %ebx             # imm = 0x3FF
	orl	$56320, %ebx            # imm = 0xDC00
	leaq	1(%r8), %rcx
	movl	$2, %ebp
.LBB0_45:                               #   in Loop: Header=BB0_28 Depth=1
	movl	%ebx, (%r10,%rcx,4)
	addq	%rbp, %r8
.LBB0_46:                               # %.thread73.backedge.i
                                        #   in Loop: Header=BB0_28 Depth=1
	cmpq	%r9, %rdx
	jne	.LBB0_28
	jmp	.LBB0_71
	.p2align	4, 0x90
.LBB0_47:                               # %.lr.ph.split.us.i17
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_55 Depth 2
	movzbl	(%r12,%rdx), %ebx
	incq	%rdx
	testb	%bl, %bl
	js	.LBB0_49
# BB#48:                                #   in Loop: Header=BB0_47 Depth=1
	incq	%r8
	jmp	.LBB0_65
	.p2align	4, 0x90
.LBB0_49:                               #   in Loop: Header=BB0_47 Depth=1
	cmpb	$-64, %bl
	jb	.LBB0_70
# BB#50:                                # %.preheader.us.preheader.i18
                                        #   in Loop: Header=BB0_47 Depth=1
	movl	$1, %ebp
	cmpb	$-32, %bl
	jb	.LBB0_54
# BB#51:                                # %.preheader.us.191.i19
                                        #   in Loop: Header=BB0_47 Depth=1
	movl	$2, %ebp
	cmpb	$-17, %bl
	jbe	.LBB0_54
# BB#52:                                # %.preheader.us.292.i34
                                        #   in Loop: Header=BB0_47 Depth=1
	movl	$3, %ebp
	cmpb	$-8, %bl
	jb	.LBB0_54
# BB#53:                                # %.preheader.us.393.i36
                                        #   in Loop: Header=BB0_47 Depth=1
	xorl	%ebp, %ebp
	cmpb	$-5, %bl
	seta	%bpl
	orl	$4, %ebp
	.p2align	4, 0x90
.LBB0_54:                               #   in Loop: Header=BB0_47 Depth=1
	movslq	%ebp, %rcx
	movzbl	_ZL11kUtf8Limits-1(%rcx), %ecx
	subl	%ecx, %ebx
	.p2align	4, 0x90
.LBB0_55:                               #   Parent Loop BB0_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdx, %r9
	je	.LBB0_59
# BB#56:                                #   in Loop: Header=BB0_55 Depth=2
	movzbl	(%r12,%rdx), %ecx
	incq	%rdx
	testb	%cl, %cl
	jns	.LBB0_60
# BB#57:                                #   in Loop: Header=BB0_55 Depth=2
	cmpb	$-65, %cl
	ja	.LBB0_60
# BB#58:                                #   in Loop: Header=BB0_55 Depth=2
	movl	%ebx, %edi
	movzbl	%cl, %ebx
	shll	$6, %edi
	addl	$-128, %ebx
	orl	%edi, %ebx
	decl	%ebp
	jne	.LBB0_55
	jmp	.LBB0_60
.LBB0_59:                               #   in Loop: Header=BB0_47 Depth=1
	movq	%r9, %rdx
.LBB0_60:                               # %.thread.us.i27
                                        #   in Loop: Header=BB0_47 Depth=1
	cmpl	$65536, %ebx            # imm = 0x10000
	jae	.LBB0_62
# BB#61:                                #   in Loop: Header=BB0_47 Depth=1
	movl	$1, %ecx
	jmp	.LBB0_64
.LBB0_62:                               #   in Loop: Header=BB0_47 Depth=1
	addl	$-65536, %ebx           # imm = 0xFFFF0000
	cmpl	$1048575, %ebx          # imm = 0xFFFFF
	ja	.LBB0_70
# BB#63:                                #   in Loop: Header=BB0_47 Depth=1
	movl	$2, %ecx
.LBB0_64:                               # %.thread73.backedge.us.i31
                                        #   in Loop: Header=BB0_47 Depth=1
	addq	%rcx, %r8
.LBB0_65:                               # %.thread73.backedge.us.i31
                                        #   in Loop: Header=BB0_47 Depth=1
	cmpq	%r9, %rdx
	jne	.LBB0_47
	jmp	.LBB0_71
.LBB0_66:
	xorl	%r8d, %r8d
	jmp	.LBB0_71
.LBB0_70:
	xorl	%eax, %eax
.LBB0_71:                               # %_ZL13Utf8_To_Utf16PwPmPKcm.exit37
	movl	$0, (%r10,%r8,4)
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	movl	$-1, %ecx
	movabsq	$4294967296, %rbp       # imm = 0x100000000
	movq	%r10, %rdi
	.p2align	4, 0x90
.LBB0_72:                               # =>This Inner Loop Header: Depth=1
	addq	%rbp, %rdx
	incl	%ecx
	cmpl	$0, (%rdi)
	leaq	4(%rdi), %rdi
	jne	.LBB0_72
# BB#73:                                # %_ZN11CStringBaseIwE13ReleaseBufferEv.exit
	sarq	$30, %rdx
	movl	$0, (%r10,%rdx)
	movl	%ecx, 8(%rsi)
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_Z20ConvertUTF8ToUnicodeRK11CStringBaseIcERS_IwE, .Lfunc_end0-_Z20ConvertUTF8ToUnicodeRK11CStringBaseIcERS_IwE
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
.LCPI1_1:
	.long	4294967218              # 0xffffffb2
	.long	4294967212              # 0xffffffac
	.long	4294967206              # 0xffffffa6
	.long	4294967200              # 0xffffffa0
.LCPI1_2:
	.long	4294967242              # 0xffffffca
	.long	4294967236              # 0xffffffc4
	.long	4294967230              # 0xffffffbe
	.long	4294967224              # 0xffffffb8
.LCPI1_3:
	.long	4294967266              # 0xffffffe2
	.long	4294967260              # 0xffffffdc
	.long	4294967254              # 0xffffffd6
	.long	4294967248              # 0xffffffd0
.LCPI1_4:
	.long	4294967290              # 0xfffffffa
	.long	4294967284              # 0xfffffff4
	.long	4294967278              # 0xffffffee
	.long	4294967272              # 0xffffffe8
.LCPI1_5:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
.LCPI1_6:
	.zero	16,63
.LCPI1_7:
	.zero	16,128
	.text
	.globl	_Z20ConvertUnicodeToUTF8RK11CStringBaseIwERS_IcE
	.p2align	4, 0x90
	.type	_Z20ConvertUnicodeToUTF8RK11CStringBaseIwERS_IcE,@function
_Z20ConvertUnicodeToUTF8RK11CStringBaseIwERS_IcE: # @_Z20ConvertUnicodeToUTF8RK11CStringBaseIwERS_IcE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 128
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r9
	movq	%rdi, %r15
	movl	$0, 8(%r9)
	movq	(%r9), %rax
	movb	$0, (%rax)
	movq	(%r15), %r13
	movslq	8(%r15), %rbp
	testq	%rbp, %rbp
	je	.LBB1_18
# BB#1:                                 # %.lr.ph.split.i.preheader
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph.split.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rbx), %rcx
	movl	(%r13,%rbx,4), %edx
	cmpl	$127, %edx
	ja	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	incq	%rax
	jmp	.LBB1_17
	.p2align	4, 0x90
.LBB1_4:                                #   in Loop: Header=BB1_2 Depth=1
	movl	%edx, %edi
	andl	$-2048, %edi            # imm = 0xF800
	cmpl	$55296, %edi            # imm = 0xD800
	jne	.LBB1_9
# BB#5:                                 #   in Loop: Header=BB1_2 Depth=1
	cmpq	%rbp, %rcx
	je	.LBB1_19
# BB#6:                                 #   in Loop: Header=BB1_2 Depth=1
	cmpl	$56319, %edx            # imm = 0xDBFF
	ja	.LBB1_19
# BB#7:                                 #   in Loop: Header=BB1_2 Depth=1
	movl	4(%r13,%rbx,4), %edi
	movl	%edi, %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	cmpl	$56320, %ecx            # imm = 0xDC00
	jne	.LBB1_19
# BB#8:                                 #   in Loop: Header=BB1_2 Depth=1
	addq	$2, %rbx
	shll	$10, %edx
	addl	$-56623104, %edx        # imm = 0xFCA00000
	addl	$-56320, %edi           # imm = 0xFFFF2400
	orl	%edx, %edi
	addl	$65536, %edi            # imm = 0x10000
	movq	%rbx, %rcx
	movl	%edi, %edx
.LBB1_9:                                #   in Loop: Header=BB1_2 Depth=1
	cmpl	$2048, %edx             # imm = 0x800
	jae	.LBB1_11
# BB#10:                                #   in Loop: Header=BB1_2 Depth=1
	xorl	%edi, %edi
	jmp	.LBB1_16
	.p2align	4, 0x90
.LBB1_11:                               #   in Loop: Header=BB1_2 Depth=1
	cmpl	$65535, %edx            # imm = 0xFFFF
	ja	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_2 Depth=1
	movl	$1, %edi
	jmp	.LBB1_16
.LBB1_13:                               #   in Loop: Header=BB1_2 Depth=1
	cmpl	$2097152, %edx          # imm = 0x200000
	jae	.LBB1_15
# BB#14:                                #   in Loop: Header=BB1_2 Depth=1
	movl	$2, %edi
	jmp	.LBB1_16
.LBB1_15:                               #   in Loop: Header=BB1_2 Depth=1
	xorl	%edi, %edi
	cmpl	$67108863, %edx         # imm = 0x3FFFFFF
	seta	%dil
	addq	$3, %rdi
	.p2align	4, 0x90
.LBB1_16:                               # %.thread72.backedge.i
                                        #   in Loop: Header=BB1_2 Depth=1
	leaq	2(%rax,%rdi), %rax
.LBB1_17:                               # %.thread72.backedge.i
                                        #   in Loop: Header=BB1_2 Depth=1
	cmpq	%rbp, %rcx
	movq	%rcx, %rbx
	jne	.LBB1_2
	jmp	.LBB1_19
.LBB1_18:
	xorl	%eax, %eax
.LBB1_19:                               # %_ZL13Utf16_To_Utf8PcPmPKwm.exit
	movl	12(%r9), %ebx
	cmpl	%eax, %ebx
	movq	%r9, (%rsp)             # 8-byte Spill
	jg	.LBB1_38
# BB#20:
	leal	1(%rax), %r14d
	cmpl	%ebx, %r14d
	je	.LBB1_38
# BB#21:
	movslq	%r14d, %rcx
	cmpl	$-1, %eax
	movq	$-1, %rdi
	cmovgeq	%rcx, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB1_34
# BB#22:                                # %.preheader.i.i
	movq	(%rsp), %r9             # 8-byte Reload
	movslq	8(%r9), %rax
	testq	%rax, %rax
	movq	(%r9), %rdi
	jle	.LBB1_35
# BB#23:                                # %.lr.ph.preheader.i.i
	cmpl	$31, %eax
	jbe	.LBB1_27
# BB#24:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB1_27
# BB#25:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB1_91
# BB#26:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB1_91
.LBB1_27:
	xorl	%ecx, %ecx
.LBB1_28:                               # %.lr.ph.i.i.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB1_31
# BB#29:                                # %.lr.ph.i.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB1_30:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB1_30
.LBB1_31:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB1_36
# BB#32:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB1_33:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB1_33
	jmp	.LBB1_36
.LBB1_34:
	movq	(%rsp), %r9             # 8-byte Reload
	jmp	.LBB1_37
.LBB1_35:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB1_37
.LBB1_36:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movq	(%rsp), %r9             # 8-byte Reload
	movq	(%r15), %r13
	movl	8(%r15), %ebp
.LBB1_37:                               # %._crit_edge17.i.i
	movq	%r12, (%r9)
	movslq	8(%r9), %rax
	movb	$0, (%r12,%rax)
	movl	%r14d, 12(%r9)
.LBB1_38:                               # %_ZN11CStringBaseIcE9GetBufferEi.exit
	movq	(%r9), %r8
	testl	%ebp, %ebp
	je	.LBB1_67
# BB#39:                                # %.lr.ph.i14
	movslq	%ebp, %rbp
	testq	%r8, %r8
	je	.LBB1_68
# BB#40:                                # %.lr.ph.split.us.i.preheader
	leaq	17(%r8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	1(%r8), %r11
	xorl	%eax, %eax
	movdqa	.LCPI1_3(%rip), %xmm1   # xmm1 = [4294967266,4294967260,4294967254,4294967248]
	movdqa	.LCPI1_4(%rip), %xmm2   # xmm2 = [4294967290,4294967284,4294967278,4294967272]
	pxor	%xmm12, %xmm12
	movdqa	.LCPI1_5(%rip), %xmm13  # xmm13 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	movdqa	.LCPI1_6(%rip), %xmm8   # xmm8 = [63,63,63,63,63,63,63,63,63,63,63,63,63,63,63,63]
	movdqa	.LCPI1_7(%rip), %xmm15  # xmm15 = [128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128]
	xorl	%r15d, %r15d
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	movdqa	.LCPI1_1(%rip), %xmm11  # xmm11 = [4294967218,4294967212,4294967206,4294967200]
	jmp	.LBB1_41
.LBB1_43:                               #   in Loop: Header=BB1_41 Depth=1
	movdqa	%xmm0, %xmm5
	xorl	%r11d, %r11d
.LBB1_44:                               # %vector.body50.prol.loopexit
                                        #   in Loop: Header=BB1_41 Depth=1
	testq	%rcx, %rcx
	movdqa	.LCPI1_3(%rip), %xmm13  # xmm13 = [4294967266,4294967260,4294967254,4294967248]
	movdqa	%xmm13, %xmm2
	movdqa	.LCPI1_4(%rip), %xmm14  # xmm14 = [4294967290,4294967284,4294967278,4294967272]
	movdqa	%xmm14, %xmm4
	movdqa	.LCPI1_6(%rip), %xmm8   # xmm8 = [63,63,63,63,63,63,63,63,63,63,63,63,63,63,63,63]
	movdqa	.LCPI1_7(%rip), %xmm15  # xmm15 = [128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128]
	movdqa	%xmm6, %xmm13
	je	.LBB1_47
# BB#45:                                # %vector.ph58.new
                                        #   in Loop: Header=BB1_41 Depth=1
	movq	%rdx, %r8
	subq	%r11, %r8
	leaq	(%r15,%r11), %rax
	addq	8(%rsp), %rax           # 8-byte Folded Reload
	movl	%ebx, %r9d
	subl	%r11d, %r9d
	addl	$16, %r11d
	movl	%ebx, %r14d
	subl	%r11d, %r14d
	movdqa	%xmm5, 48(%rsp)         # 16-byte Spill
	.p2align	4, 0x90
.LBB1_46:                               # %vector.body50
                                        #   Parent Loop BB1_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	pshufd	$0, 48(%rsp), %xmm5     # 16-byte Folded Reload
                                        # xmm5 = mem[0,0,0,0]
	movd	%r9d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	.LCPI1_0(%rip), %xmm14  # xmm14 = [6,6,6,6]
	pmuludq	%xmm14, %xmm0
	pshufd	$160, %xmm0, %xmm1      # xmm1 = xmm0[0,0,2,2]
	movdqa	%xmm1, %xmm3
	paddd	%xmm11, %xmm3
	movdqa	%xmm1, %xmm7
	movdqa	.LCPI1_2(%rip), %xmm9   # xmm9 = [4294967242,4294967236,4294967230,4294967224]
	paddd	%xmm9, %xmm7
	movdqa	%xmm1, %xmm0
	paddd	%xmm2, %xmm0
	paddd	%xmm4, %xmm1
	movdqa	%xmm1, %xmm4
	psrldq	$12, %xmm4              # xmm4 = xmm4[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm2, %xmm10
	movdqa	%xmm5, %xmm2
	psrld	%xmm4, %xmm2
	movdqa	%xmm1, %xmm4
	psrlq	$32, %xmm4
	movdqa	%xmm5, %xmm6
	psrld	%xmm4, %xmm6
	movsd	%xmm6, %xmm2            # xmm2 = xmm6[0],xmm2[1]
	pshufd	$237, %xmm2, %xmm2      # xmm2 = xmm2[1,3,2,3]
	movdqa	%xmm1, %xmm4
	punpckhdq	%xmm12, %xmm4   # xmm4 = xmm4[2],xmm12[2],xmm4[3],xmm12[3]
	movdqa	%xmm5, %xmm6
	psrld	%xmm4, %xmm6
	punpckldq	%xmm12, %xmm1   # xmm1 = xmm1[0],xmm12[0],xmm1[1],xmm12[1]
	movdqa	%xmm5, %xmm4
	psrld	%xmm1, %xmm4
	movsd	%xmm4, %xmm6            # xmm6 = xmm4[0],xmm6[1]
	pshufd	$232, %xmm6, %xmm4      # xmm4 = xmm6[0,2,2,3]
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	movdqa	%xmm0, %xmm1
	psrldq	$12, %xmm1              # xmm1 = xmm1[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm5, %xmm2
	psrld	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	psrlq	$32, %xmm1
	movdqa	%xmm5, %xmm6
	psrld	%xmm1, %xmm6
	movsd	%xmm6, %xmm2            # xmm2 = xmm6[0],xmm2[1]
	pshufd	$237, %xmm2, %xmm1      # xmm1 = xmm2[1,3,2,3]
	movdqa	%xmm0, %xmm2
	punpckhdq	%xmm12, %xmm2   # xmm2 = xmm2[2],xmm12[2],xmm2[3],xmm12[3]
	movdqa	%xmm5, %xmm6
	psrld	%xmm2, %xmm6
	punpckldq	%xmm12, %xmm0   # xmm0 = xmm0[0],xmm12[0],xmm0[1],xmm12[1]
	movdqa	%xmm5, %xmm2
	psrld	%xmm0, %xmm2
	movsd	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1]
	pshufd	$232, %xmm6, %xmm0      # xmm0 = xmm6[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movdqa	%xmm7, %xmm1
	psrldq	$12, %xmm1              # xmm1 = xmm1[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm5, %xmm2
	psrld	%xmm1, %xmm2
	movdqa	%xmm7, %xmm1
	psrlq	$32, %xmm1
	movdqa	%xmm5, %xmm6
	psrld	%xmm1, %xmm6
	movsd	%xmm6, %xmm2            # xmm2 = xmm6[0],xmm2[1]
	pshufd	$237, %xmm2, %xmm1      # xmm1 = xmm2[1,3,2,3]
	movdqa	%xmm7, %xmm2
	punpckhdq	%xmm12, %xmm2   # xmm2 = xmm2[2],xmm12[2],xmm2[3],xmm12[3]
	movdqa	%xmm5, %xmm6
	psrld	%xmm2, %xmm6
	punpckldq	%xmm12, %xmm7   # xmm7 = xmm7[0],xmm12[0],xmm7[1],xmm12[1]
	movdqa	%xmm5, %xmm2
	psrld	%xmm7, %xmm2
	movsd	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1]
	pshufd	$232, %xmm6, %xmm7      # xmm7 = xmm6[0,2,2,3]
	punpckldq	%xmm1, %xmm7    # xmm7 = xmm7[0],xmm1[0],xmm7[1],xmm1[1]
	movdqa	%xmm3, %xmm1
	psrldq	$12, %xmm1              # xmm1 = xmm1[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm5, %xmm2
	psrld	%xmm1, %xmm2
	movdqa	%xmm3, %xmm1
	psrlq	$32, %xmm1
	movdqa	%xmm5, %xmm6
	psrld	%xmm1, %xmm6
	movsd	%xmm6, %xmm2            # xmm2 = xmm6[0],xmm2[1]
	pshufd	$237, %xmm2, %xmm1      # xmm1 = xmm2[1,3,2,3]
	movdqa	%xmm3, %xmm2
	punpckhdq	%xmm12, %xmm2   # xmm2 = xmm2[2],xmm12[2],xmm2[3],xmm12[3]
	movdqa	%xmm5, %xmm6
	psrld	%xmm2, %xmm6
	punpckldq	%xmm12, %xmm3   # xmm3 = xmm3[0],xmm12[0],xmm3[1],xmm12[1]
	movdqa	%xmm5, %xmm2
	psrld	%xmm3, %xmm2
	movsd	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1]
	pshufd	$232, %xmm6, %xmm2      # xmm2 = xmm6[0,2,2,3]
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	pand	%xmm13, %xmm2
	pand	%xmm13, %xmm7
	packuswb	%xmm2, %xmm7
	pand	%xmm13, %xmm0
	pand	%xmm13, %xmm4
	packuswb	%xmm0, %xmm4
	packuswb	%xmm7, %xmm4
	pand	%xmm8, %xmm4
	por	%xmm15, %xmm4
	movdqu	%xmm4, -16(%rax)
	movd	%r14d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	pmuludq	%xmm14, %xmm0
	pshufd	$160, %xmm0, %xmm1      # xmm1 = xmm0[0,0,2,2]
	movdqa	%xmm1, %xmm3
	paddd	%xmm11, %xmm3
	movdqa	%xmm1, %xmm7
	paddd	%xmm9, %xmm7
	movdqa	%xmm1, %xmm0
	paddd	%xmm10, %xmm0
	paddd	.LCPI1_4(%rip), %xmm1
	movdqa	%xmm1, %xmm2
	psrldq	$12, %xmm2              # xmm2 = xmm2[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm5, %xmm4
	psrld	%xmm2, %xmm4
	movdqa	%xmm1, %xmm2
	psrlq	$32, %xmm2
	movdqa	%xmm5, %xmm6
	psrld	%xmm2, %xmm6
	movsd	%xmm6, %xmm4            # xmm4 = xmm6[0],xmm4[1]
	pshufd	$237, %xmm4, %xmm2      # xmm2 = xmm4[1,3,2,3]
	movdqa	%xmm1, %xmm4
	punpckhdq	%xmm12, %xmm4   # xmm4 = xmm4[2],xmm12[2],xmm4[3],xmm12[3]
	movdqa	%xmm5, %xmm6
	psrld	%xmm4, %xmm6
	punpckldq	%xmm12, %xmm1   # xmm1 = xmm1[0],xmm12[0],xmm1[1],xmm12[1]
	movdqa	%xmm5, %xmm4
	psrld	%xmm1, %xmm4
	movsd	%xmm4, %xmm6            # xmm6 = xmm4[0],xmm6[1]
	pshufd	$232, %xmm6, %xmm4      # xmm4 = xmm6[0,2,2,3]
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	movdqa	%xmm0, %xmm1
	psrldq	$12, %xmm1              # xmm1 = xmm1[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm5, %xmm2
	psrld	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	psrlq	$32, %xmm1
	movdqa	%xmm5, %xmm6
	psrld	%xmm1, %xmm6
	movsd	%xmm6, %xmm2            # xmm2 = xmm6[0],xmm2[1]
	pshufd	$237, %xmm2, %xmm1      # xmm1 = xmm2[1,3,2,3]
	movdqa	%xmm0, %xmm2
	punpckhdq	%xmm12, %xmm2   # xmm2 = xmm2[2],xmm12[2],xmm2[3],xmm12[3]
	movdqa	%xmm5, %xmm6
	psrld	%xmm2, %xmm6
	punpckldq	%xmm12, %xmm0   # xmm0 = xmm0[0],xmm12[0],xmm0[1],xmm12[1]
	movdqa	%xmm5, %xmm2
	psrld	%xmm0, %xmm2
	movsd	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1]
	pshufd	$232, %xmm6, %xmm0      # xmm0 = xmm6[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movdqa	%xmm7, %xmm1
	psrldq	$12, %xmm1              # xmm1 = xmm1[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm5, %xmm2
	psrld	%xmm1, %xmm2
	movdqa	%xmm7, %xmm1
	psrlq	$32, %xmm1
	movdqa	%xmm5, %xmm6
	psrld	%xmm1, %xmm6
	movsd	%xmm6, %xmm2            # xmm2 = xmm6[0],xmm2[1]
	pshufd	$237, %xmm2, %xmm1      # xmm1 = xmm2[1,3,2,3]
	movdqa	%xmm7, %xmm2
	punpckhdq	%xmm12, %xmm2   # xmm2 = xmm2[2],xmm12[2],xmm2[3],xmm12[3]
	movdqa	%xmm5, %xmm6
	psrld	%xmm2, %xmm6
	punpckldq	%xmm12, %xmm7   # xmm7 = xmm7[0],xmm12[0],xmm7[1],xmm12[1]
	movdqa	%xmm5, %xmm2
	psrld	%xmm7, %xmm2
	movsd	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1]
	pshufd	$232, %xmm6, %xmm7      # xmm7 = xmm6[0,2,2,3]
	punpckldq	%xmm1, %xmm7    # xmm7 = xmm7[0],xmm1[0],xmm7[1],xmm1[1]
	movdqa	%xmm3, %xmm1
	psrldq	$12, %xmm1              # xmm1 = xmm1[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm5, %xmm2
	psrld	%xmm1, %xmm2
	movdqa	%xmm3, %xmm1
	psrlq	$32, %xmm1
	movdqa	%xmm5, %xmm6
	psrld	%xmm1, %xmm6
	movsd	%xmm6, %xmm2            # xmm2 = xmm6[0],xmm2[1]
	movdqa	%xmm3, %xmm1
	punpckhdq	%xmm12, %xmm1   # xmm1 = xmm1[2],xmm12[2],xmm1[3],xmm12[3]
	movdqa	%xmm5, %xmm6
	psrld	%xmm1, %xmm6
	punpckldq	%xmm12, %xmm3   # xmm3 = xmm3[0],xmm12[0],xmm3[1],xmm12[1]
	psrld	%xmm3, %xmm5
	movsd	%xmm5, %xmm6            # xmm6 = xmm5[0],xmm6[1]
	pshufd	$237, %xmm2, %xmm1      # xmm1 = xmm2[1,3,2,3]
	pshufd	$232, %xmm6, %xmm2      # xmm2 = xmm6[0,2,2,3]
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	pand	%xmm13, %xmm2
	pand	%xmm13, %xmm7
	packuswb	%xmm2, %xmm7
	movdqa	%xmm10, %xmm2
	pand	%xmm13, %xmm0
	pand	%xmm13, %xmm4
	packuswb	%xmm0, %xmm4
	packuswb	%xmm7, %xmm4
	pand	%xmm8, %xmm4
	por	%xmm15, %xmm4
	movdqu	%xmm4, (%rax)
	movdqa	.LCPI1_4(%rip), %xmm4   # xmm4 = [4294967290,4294967284,4294967278,4294967272]
	addq	$32, %rax
	addl	$-32, %r9d
	addl	$-32, %r14d
	addq	$-32, %r8
	jne	.LBB1_46
.LBB1_47:                               # %middle.block51
                                        #   in Loop: Header=BB1_41 Depth=1
	cmpq	%rdx, %rbp
	movq	(%rsp), %r9             # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	movdqa	%xmm2, %xmm1
	movdqa	%xmm4, %xmm2
	je	.LBB1_65
# BB#48:                                #   in Loop: Header=BB1_41 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rax
	addq	%r15, %rax
	subl	%ecx, %ebx
	jmp	.LBB1_63
	.p2align	4, 0x90
.LBB1_41:                               # %.lr.ph.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_46 Depth 2
                                        #     Child Loop BB1_64 Depth 2
	leaq	1(%rax), %rdi
	movl	(%r13,%rax,4), %r10d
	cmpl	$128, %r10d
	jae	.LBB1_49
# BB#42:                                #   in Loop: Header=BB1_41 Depth=1
	movb	%r10b, (%r8,%r15)
	incq	%r15
	jmp	.LBB1_66
	.p2align	4, 0x90
.LBB1_49:                               #   in Loop: Header=BB1_41 Depth=1
	movl	%r10d, %ecx
	andl	$-2048, %ecx            # imm = 0xF800
	cmpl	$55296, %ecx            # imm = 0xD800
	jne	.LBB1_54
# BB#50:                                #   in Loop: Header=BB1_41 Depth=1
	cmpq	%rbp, %rdi
	je	.LBB1_90
# BB#51:                                #   in Loop: Header=BB1_41 Depth=1
	cmpl	$56319, %r10d           # imm = 0xDBFF
	ja	.LBB1_90
# BB#52:                                #   in Loop: Header=BB1_41 Depth=1
	movl	4(%r13,%rax,4), %ecx
	movl	%ecx, %edi
	andl	$-1024, %edi            # imm = 0xFC00
	cmpl	$56320, %edi            # imm = 0xDC00
	jne	.LBB1_90
# BB#53:                                #   in Loop: Header=BB1_41 Depth=1
	addq	$2, %rax
	shll	$10, %r10d
	addl	$-56623104, %r10d       # imm = 0xFCA00000
	addl	$-56320, %ecx           # imm = 0xFFFF2400
	orl	%r10d, %ecx
	addl	$65536, %ecx            # imm = 0x10000
	movq	%rax, %rdi
	movl	%ecx, %r10d
.LBB1_54:                               #   in Loop: Header=BB1_41 Depth=1
	movl	$1, %ebx
	cmpl	$2048, %r10d            # imm = 0x800
	jb	.LBB1_58
# BB#55:                                #   in Loop: Header=BB1_41 Depth=1
	movl	$2, %ebx
	cmpl	$65535, %r10d           # imm = 0xFFFF
	jbe	.LBB1_58
# BB#56:                                #   in Loop: Header=BB1_41 Depth=1
	movl	$3, %ebx
	cmpl	$2097152, %r10d         # imm = 0x200000
	jb	.LBB1_58
# BB#57:                                #   in Loop: Header=BB1_41 Depth=1
	xorl	%ebx, %ebx
	cmpl	$67108863, %r10d        # imm = 0x3FFFFFF
	seta	%bl
	orl	$4, %ebx
	.p2align	4, 0x90
.LBB1_58:                               # %.split.us.us.i
                                        #   in Loop: Header=BB1_41 Depth=1
	leal	-1(%rbx), %r12d
	movzbl	_ZL11kUtf8Limits(%r12), %eax
	leal	(%rbx,%rbx), %ecx
	leal	(%rcx,%rcx,2), %ecx
	movl	%r10d, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	addl	%eax, %edx
	movb	%dl, (%r8,%r15)
	leaq	1(%r12), %rcx
	cmpq	$15, %rcx
	jbe	.LBB1_62
# BB#59:                                # %min.iters.checked54
                                        #   in Loop: Header=BB1_41 Depth=1
	movq	%rcx, %rsi
	movabsq	$8589934576, %rax       # imm = 0x1FFFFFFF0
	andq	%rax, %rsi
	movq	%rcx, %rdx
	andq	%rax, %rdx
	je	.LBB1_62
# BB#60:                                # %vector.ph58
                                        #   in Loop: Header=BB1_41 Depth=1
	movdqa	%xmm13, %xmm6
	movq	%rcx, %rbp
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movd	%r10d, %xmm0
	leaq	-16(%rdx), %rax
	movq	%rax, %rcx
	shrq	$4, %rcx
	btl	$4, %eax
	jb	.LBB1_43
# BB#61:                                # %vector.body50.prol
                                        #   in Loop: Header=BB1_41 Depth=1
	movdqa	%xmm0, %xmm5
	pshufd	$0, %xmm0, %xmm9        # xmm9 = xmm0[0,0,0,0]
	movd	%ebx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	.LCPI1_0(%rip), %xmm3   # xmm3 = [6,6,6,6]
	pmuludq	%xmm3, %xmm0
	movdqa	%xmm8, %xmm10
	movdqa	%xmm15, %xmm13
	pshufd	$160, %xmm0, %xmm4      # xmm4 = xmm0[0,0,2,2]
	movdqa	%xmm4, %xmm15
	paddd	.LCPI1_1(%rip), %xmm15
	movdqa	%xmm4, %xmm7
	paddd	.LCPI1_2(%rip), %xmm7
	movdqa	%xmm4, %xmm0
	paddd	%xmm1, %xmm0
	paddd	%xmm2, %xmm4
	movdqa	%xmm4, %xmm3
	psrldq	$12, %xmm3              # xmm3 = xmm3[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm9, %xmm2
	psrld	%xmm3, %xmm2
	movdqa	%xmm4, %xmm3
	psrlq	$32, %xmm3
	movdqa	%xmm9, %xmm8
	psrld	%xmm3, %xmm8
	movsd	%xmm8, %xmm2            # xmm2 = xmm8[0],xmm2[1]
	pshufd	$237, %xmm2, %xmm8      # xmm8 = xmm2[1,3,2,3]
	movdqa	%xmm4, %xmm3
	punpckhdq	%xmm12, %xmm3   # xmm3 = xmm3[2],xmm12[2],xmm3[3],xmm12[3]
	movdqa	%xmm9, %xmm2
	psrld	%xmm3, %xmm2
	punpckldq	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1]
	movdqa	%xmm9, %xmm3
	psrld	%xmm4, %xmm3
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	pshufd	$232, %xmm2, %xmm4      # xmm4 = xmm2[0,2,2,3]
	punpckldq	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1]
	movdqa	%xmm0, %xmm2
	psrldq	$12, %xmm2              # xmm2 = xmm2[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm9, %xmm3
	psrld	%xmm2, %xmm3
	movdqa	%xmm0, %xmm2
	psrlq	$32, %xmm2
	movdqa	%xmm9, %xmm1
	psrld	%xmm2, %xmm1
	movsd	%xmm1, %xmm3            # xmm3 = xmm1[0],xmm3[1]
	pshufd	$237, %xmm3, %xmm1      # xmm1 = xmm3[1,3,2,3]
	movdqa	%xmm0, %xmm2
	punpckhdq	%xmm12, %xmm2   # xmm2 = xmm2[2],xmm12[2],xmm2[3],xmm12[3]
	movdqa	%xmm9, %xmm3
	psrld	%xmm2, %xmm3
	punpckldq	%xmm12, %xmm0   # xmm0 = xmm0[0],xmm12[0],xmm0[1],xmm12[1]
	movdqa	%xmm9, %xmm2
	psrld	%xmm0, %xmm2
	movsd	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1]
	pshufd	$232, %xmm3, %xmm0      # xmm0 = xmm3[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movdqa	%xmm7, %xmm1
	psrldq	$12, %xmm1              # xmm1 = xmm1[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm9, %xmm2
	psrld	%xmm1, %xmm2
	movdqa	%xmm7, %xmm1
	psrlq	$32, %xmm1
	movdqa	%xmm9, %xmm3
	psrld	%xmm1, %xmm3
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	pshufd	$237, %xmm2, %xmm1      # xmm1 = xmm2[1,3,2,3]
	movdqa	%xmm7, %xmm2
	punpckhdq	%xmm12, %xmm2   # xmm2 = xmm2[2],xmm12[2],xmm2[3],xmm12[3]
	movdqa	%xmm9, %xmm3
	psrld	%xmm2, %xmm3
	punpckldq	%xmm12, %xmm7   # xmm7 = xmm7[0],xmm12[0],xmm7[1],xmm12[1]
	movdqa	%xmm9, %xmm2
	psrld	%xmm7, %xmm2
	movsd	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1]
	pshufd	$232, %xmm3, %xmm7      # xmm7 = xmm3[0,2,2,3]
	punpckldq	%xmm1, %xmm7    # xmm7 = xmm7[0],xmm1[0],xmm7[1],xmm1[1]
	movdqa	%xmm15, %xmm1
	psrldq	$12, %xmm1              # xmm1 = xmm1[12,13,14,15],zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero,zero
	movdqa	%xmm9, %xmm2
	psrld	%xmm1, %xmm2
	movdqa	%xmm15, %xmm1
	psrlq	$32, %xmm1
	movdqa	%xmm9, %xmm3
	psrld	%xmm1, %xmm3
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	movdqa	%xmm15, %xmm1
	punpckhdq	%xmm12, %xmm1   # xmm1 = xmm1[2],xmm12[2],xmm1[3],xmm12[3]
	movdqa	%xmm9, %xmm3
	psrld	%xmm1, %xmm3
	punpckldq	%xmm12, %xmm15  # xmm15 = xmm15[0],xmm12[0],xmm15[1],xmm12[1]
	psrld	%xmm15, %xmm9
	movsd	%xmm9, %xmm3            # xmm3 = xmm9[0],xmm3[1]
	pshufd	$237, %xmm2, %xmm1      # xmm1 = xmm2[1,3,2,3]
	pshufd	$232, %xmm3, %xmm2      # xmm2 = xmm3[0,2,2,3]
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movdqa	%xmm6, %xmm1
	pand	%xmm1, %xmm2
	pand	%xmm1, %xmm7
	packuswb	%xmm2, %xmm7
	pand	%xmm1, %xmm0
	pand	%xmm1, %xmm4
	packuswb	%xmm0, %xmm4
	packuswb	%xmm7, %xmm4
	pand	%xmm10, %xmm4
	por	%xmm13, %xmm4
	movdqu	%xmm4, 1(%r8,%r15)
	movl	$16, %r11d
	jmp	.LBB1_44
	.p2align	4, 0x90
.LBB1_62:                               #   in Loop: Header=BB1_41 Depth=1
	movq	%r15, %rax
.LBB1_63:                               # %scalar.ph52.preheader
                                        #   in Loop: Header=BB1_41 Depth=1
	addq	%r11, %rax
	leal	(%rbx,%rbx,2), %ecx
	leal	-6(%rcx,%rcx), %ecx
	.p2align	4, 0x90
.LBB1_64:                               # %scalar.ph52
                                        #   Parent Loop BB1_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r10d, %edx
	shrl	%cl, %edx
	andb	$63, %dl
	orb	$-128, %dl
	addl	$-6, %ecx
	decl	%ebx
	movb	%dl, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB1_64
.LBB1_65:                               # %.thread72.backedge.us.loopexit.i
                                        #   in Loop: Header=BB1_41 Depth=1
	leaq	2(%r15,%r12), %r15
.LBB1_66:                               # %.thread72.backedge.us.i
                                        #   in Loop: Header=BB1_41 Depth=1
	cmpq	%rbp, %rdi
	movq	%rdi, %rax
	jne	.LBB1_41
	jmp	.LBB1_85
.LBB1_67:
	xorl	%r15d, %r15d
	jmp	.LBB1_85
.LBB1_68:                               # %.lr.ph.split.i17.preheader
	xorl	%edx, %edx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_69:                               # %.lr.ph.split.i17
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rdx), %rax
	movl	(%r13,%rdx,4), %ecx
	cmpl	$127, %ecx
	ja	.LBB1_71
# BB#70:                                #   in Loop: Header=BB1_69 Depth=1
	incq	%r15
	jmp	.LBB1_84
	.p2align	4, 0x90
.LBB1_71:                               #   in Loop: Header=BB1_69 Depth=1
	movl	%ecx, %edi
	andl	$-2048, %edi            # imm = 0xF800
	cmpl	$55296, %edi            # imm = 0xD800
	jne	.LBB1_76
# BB#72:                                #   in Loop: Header=BB1_69 Depth=1
	cmpq	%rbp, %rax
	je	.LBB1_90
# BB#73:                                #   in Loop: Header=BB1_69 Depth=1
	cmpl	$56319, %ecx            # imm = 0xDBFF
	ja	.LBB1_90
# BB#74:                                #   in Loop: Header=BB1_69 Depth=1
	movl	4(%r13,%rdx,4), %edi
	movl	%edi, %eax
	andl	$-1024, %eax            # imm = 0xFC00
	cmpl	$56320, %eax            # imm = 0xDC00
	jne	.LBB1_90
# BB#75:                                #   in Loop: Header=BB1_69 Depth=1
	addq	$2, %rdx
	shll	$10, %ecx
	addl	$-56623104, %ecx        # imm = 0xFCA00000
	addl	$-56320, %edi           # imm = 0xFFFF2400
	orl	%ecx, %edi
	addl	$65536, %edi            # imm = 0x10000
	movq	%rdx, %rax
	movl	%edi, %ecx
.LBB1_76:                               #   in Loop: Header=BB1_69 Depth=1
	cmpl	$2048, %ecx             # imm = 0x800
	jae	.LBB1_78
# BB#77:                                #   in Loop: Header=BB1_69 Depth=1
	xorl	%edx, %edx
	jmp	.LBB1_83
	.p2align	4, 0x90
.LBB1_78:                               #   in Loop: Header=BB1_69 Depth=1
	cmpl	$65535, %ecx            # imm = 0xFFFF
	ja	.LBB1_80
# BB#79:                                #   in Loop: Header=BB1_69 Depth=1
	movl	$1, %edx
	jmp	.LBB1_83
.LBB1_80:                               #   in Loop: Header=BB1_69 Depth=1
	cmpl	$2097152, %ecx          # imm = 0x200000
	jae	.LBB1_82
# BB#81:                                #   in Loop: Header=BB1_69 Depth=1
	movl	$2, %edx
	jmp	.LBB1_83
.LBB1_82:                               #   in Loop: Header=BB1_69 Depth=1
	xorl	%edx, %edx
	cmpl	$67108863, %ecx         # imm = 0x3FFFFFF
	seta	%dl
	addq	$3, %rdx
	.p2align	4, 0x90
.LBB1_83:                               # %.thread72.backedge.i20
                                        #   in Loop: Header=BB1_69 Depth=1
	leaq	2(%r15,%rdx), %r15
.LBB1_84:                               # %.thread72.backedge.i20
                                        #   in Loop: Header=BB1_69 Depth=1
	cmpq	%rbp, %rax
	movq	%rax, %rdx
	jne	.LBB1_69
.LBB1_85:                               # %_ZL13Utf16_To_Utf8PcPmPKwm.exit29
	movl	$1, %eax
	jmp	.LBB1_86
.LBB1_90:
	xorl	%eax, %eax
.LBB1_86:                               # %_ZL13Utf16_To_Utf8PcPmPKwm.exit29
	movb	$0, (%r8,%r15)
	movq	(%r9), %rbx
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	movl	$-1, %ecx
	movabsq	$4294967296, %rbp       # imm = 0x100000000
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB1_87:                               # =>This Inner Loop Header: Depth=1
	addq	%rbp, %rdx
	incl	%ecx
	cmpb	$0, (%rdi)
	leaq	1(%rdi), %rdi
	jne	.LBB1_87
# BB#88:                                # %_ZN11CStringBaseIcE13ReleaseBufferEv.exit
	sarq	$32, %rdx
	movb	$0, (%rbx,%rdx)
	movl	%ecx, 8(%r9)
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_91:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB1_94
# BB#92:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_93:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rdi,%rbp), %xmm0
	movdqu	16(%rdi,%rbp), %xmm1
	movdqu	%xmm0, (%r12,%rbp)
	movdqu	%xmm1, 16(%r12,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB1_93
	jmp	.LBB1_95
.LBB1_94:
	xorl	%ebp, %ebp
.LBB1_95:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB1_98
# BB#96:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r12,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
	.p2align	4, 0x90
.LBB1_97:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbp), %xmm0
	movdqu	(%rbp), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB1_97
.LBB1_98:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB1_28
	jmp	.LBB1_36
.Lfunc_end1:
	.size	_Z20ConvertUnicodeToUTF8RK11CStringBaseIwERS_IcE, .Lfunc_end1-_Z20ConvertUnicodeToUTF8RK11CStringBaseIwERS_IcE
	.cfi_endproc

	.type	_ZL11kUtf8Limits,@object # @_ZL11kUtf8Limits
	.section	.rodata,"a",@progbits
_ZL11kUtf8Limits:
	.ascii	"\300\340\360\370\374"
	.size	_ZL11kUtf8Limits, 5


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
