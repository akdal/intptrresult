	.text
	.file	"gstype1.bc"
	.globl	gs_type1_encrypt
	.p2align	4, 0x90
	.type	gs_type1_encrypt,@function
gs_type1_encrypt:                       # @gs_type1_encrypt
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movw	(%rcx), %ax
	testl	%edx, %edx
	je	.LBB0_6
# BB#1:                                 # %.lr.ph.preheader
	testb	$1, %dl
	jne	.LBB0_3
# BB#2:
	movl	%edx, %r8d
	cmpl	$1, %edx
	jne	.LBB0_5
	jmp	.LBB0_6
.LBB0_3:                                # %.lr.ph.prol
	movzwl	%ax, %r9d
	movzbl	(%rsi), %r8d
	movl	%r9d, %eax
	shrl	$8, %eax
	xorl	%r8d, %eax
	movb	%al, (%rdi)
	addl	%r9d, %eax
	imull	$52845, %eax, %eax      # imm = 0xCE6D
	addl	$22719, %eax            # imm = 0x58BF
	incq	%rsi
	incq	%rdi
	leal	-1(%rdx), %r8d
	cmpl	$1, %edx
	je	.LBB0_6
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %r9d
	movzwl	%ax, %edx
	movl	%edx, %eax
	shrl	$8, %eax
	xorl	%r9d, %eax
	movb	%al, (%rdi)
	addl	%edx, %eax
	imull	$52845, %eax, %eax      # imm = 0xCE6D
	addl	$22719, %eax            # imm = 0x58BF
	movzbl	1(%rsi), %r9d
	movzwl	%ax, %edx
	movl	%edx, %eax
	shrl	$8, %eax
	xorl	%r9d, %eax
	movb	%al, 1(%rdi)
	addl	%edx, %eax
	imull	$52845, %eax, %eax      # imm = 0xCE6D
	addl	$22719, %eax            # imm = 0x58BF
	addq	$2, %rsi
	addq	$2, %rdi
	addl	$-2, %r8d
	jne	.LBB0_5
.LBB0_6:                                # %._crit_edge
	movw	%ax, (%rcx)
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	gs_type1_encrypt, .Lfunc_end0-gs_type1_encrypt
	.cfi_endproc

	.globl	gs_type1_decrypt
	.p2align	4, 0x90
	.type	gs_type1_decrypt,@function
gs_type1_decrypt:                       # @gs_type1_decrypt
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movw	(%rcx), %ax
	testl	%edx, %edx
	je	.LBB1_6
# BB#1:                                 # %.lr.ph.preheader
	testb	$1, %dl
	jne	.LBB1_3
# BB#2:
	movl	%edx, %r8d
	cmpl	$1, %edx
	jne	.LBB1_5
	jmp	.LBB1_6
.LBB1_3:                                # %.lr.ph.prol
	movzwl	%ax, %r8d
	movzbl	(%rsi), %r9d
	incq	%rsi
	movl	%r8d, %eax
	shrl	$8, %eax
	xorl	%r9d, %eax
	movb	%al, (%rdi)
	addl	%r8d, %r9d
	imull	$52845, %r9d, %eax      # imm = 0xCE6D
	addl	$22719, %eax            # imm = 0x58BF
	incq	%rdi
	leal	-1(%rdx), %r8d
	cmpl	$1, %edx
	je	.LBB1_6
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %edx
	movzwl	%ax, %r9d
	movl	%r9d, %eax
	shrl	$8, %eax
	xorl	%edx, %eax
	movb	%al, (%rdi)
	addl	%r9d, %edx
	imull	$52845, %edx, %r9d      # imm = 0xCE6D
	leal	22719(%r9), %edx
	movzbl	1(%rsi), %eax
	shrl	$8, %edx
	xorl	%eax, %edx
	movb	%dl, 1(%rdi)
	leal	22719(%rax,%r9), %eax
	imull	$52845, %eax, %eax      # imm = 0xCE6D
	addl	$22719, %eax            # imm = 0x58BF
	addq	$2, %rsi
	addq	$2, %rdi
	addl	$-2, %r8d
	jne	.LBB1_5
.LBB1_6:                                # %._crit_edge
	movw	%ax, (%rcx)
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	gs_type1_decrypt, .Lfunc_end1-gs_type1_decrypt
	.cfi_endproc

	.globl	gs_type1_init_matrix
	.p2align	4, 0x90
	.type	gs_type1_init_matrix,@function
gs_type1_init_matrix:                   # @gs_type1_init_matrix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movabsq	$9223372032559808512, %rsi # imm = 0x7FFFFFFF00000000
	movl	$-10000, 8(%rsp)        # imm = 0xD8F0
	movq	8(%r14), %rax
	movl	24(%rax), %edx
	movl	28(%rax), %r15d
	movl	40(%rax), %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	44(%rax), %ebp
	movl	56(%rax), %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	60(%rax), %r12d
	movl	72(%rax), %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	76(%rax), %r13d
	movl	$0, 64(%r14)
	andq	$2147483647, %r15       # imm = 0x7FFFFFFF
	shlq	$32, %r15
	orq	%rdx, %r15
	movq	%rdx, %rbx
	je	.LBB2_2
# BB#1:
	movd	%edx, %xmm0
	cvtss2sd	%xmm0, %xmm0
	leaq	8(%rsp), %rdi
	callq	frexp
	movabsq	$9223372032559808512, %rsi # imm = 0x7FFFFFFF00000000
	movq	%rbx, %rdx
.LBB2_2:
	shlq	$32, %rbp
	andq	%rsi, %rbp
	orq	16(%rsp), %rbp          # 8-byte Folded Reload
	je	.LBB2_6
# BB#3:
	movd	16(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	leaq	12(%rsp), %rdi
	callq	frexp
	movl	12(%rsp), %eax
	cmpl	8(%rsp), %eax
	jle	.LBB2_5
# BB#4:
	movl	%eax, 8(%rsp)
.LBB2_5:
	movl	$1, 64(%r14)
	movq	%rbx, %rdx
	movabsq	$9223372032559808512, %rsi # imm = 0x7FFFFFFF00000000
.LBB2_6:
	shlq	$32, %r12
	andq	%rsi, %r12
	orq	24(%rsp), %r12          # 8-byte Folded Reload
	je	.LBB2_10
# BB#7:
	movd	24(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	leaq	12(%rsp), %rdi
	callq	frexp
	movl	12(%rsp), %eax
	cmpl	8(%rsp), %eax
	jle	.LBB2_9
# BB#8:
	movl	%eax, 8(%rsp)
.LBB2_9:
	movl	$1, 64(%r14)
	movq	%rbx, %rdx
	movabsq	$9223372032559808512, %rsi # imm = 0x7FFFFFFF00000000
.LBB2_10:
	shlq	$32, %r13
	andq	%rsi, %r13
	movq	32(%rsp), %rax          # 8-byte Reload
	orq	%rax, %r13
	je	.LBB2_13
# BB#11:
	movd	%eax, %xmm0
	cvtss2sd	%xmm0, %xmm0
	leaq	12(%rsp), %rdi
	callq	frexp
	movq	%rbx, %rdx
	movl	12(%rsp), %ecx
	movl	8(%rsp), %eax
	cmpl	%eax, %ecx
	jle	.LBB2_14
# BB#12:
	movl	%ecx, 8(%rsp)
	movl	%ecx, %eax
	jmp	.LBB2_14
.LBB2_13:                               # %._crit_edge
	movl	8(%rsp), %eax
.LBB2_14:
	movl	$20, %ebx
	subl	%eax, %ebx
	testq	%r15, %r15
	movl	%ebx, 8(%rsp)
	je	.LBB2_16
# BB#15:
	movd	%edx, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	%ebx, %edi
	callq	ldexp
	cvttsd2si	%xmm0, %rax
	jmp	.LBB2_17
.LBB2_16:
	xorl	%eax, %eax
.LBB2_17:
	testq	%r13, %r13
	movq	%rax, 32(%r14)
	je	.LBB2_19
# BB#18:
	movd	32(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	movl	%ebx, %edi
	callq	ldexp
	cvttsd2si	%xmm0, %rax
	jmp	.LBB2_20
.LBB2_19:
	xorl	%eax, %eax
.LBB2_20:
	movq	%rax, 56(%r14)
	cmpl	$0, 64(%r14)
	je	.LBB2_23
# BB#21:
	testq	%rbp, %rbp
	je	.LBB2_24
# BB#22:
	movd	16(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	movl	%ebx, %edi
	callq	ldexp
	cvttsd2si	%xmm0, %rax
	jmp	.LBB2_25
.LBB2_23:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%r14)
	jmp	.LBB2_29
.LBB2_24:
	xorl	%eax, %eax
.LBB2_25:
	testq	%r12, %r12
	movq	%rax, 40(%r14)
	je	.LBB2_27
# BB#26:
	movd	24(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	movl	%ebx, %edi
	callq	ldexp
	cvttsd2si	%xmm0, %rax
	jmp	.LBB2_28
.LBB2_27:
	xorl	%eax, %eax
.LBB2_28:
	movq	%rax, 48(%r14)
.LBB2_29:
	leal	-12(%rbx), %eax
	movl	%eax, 68(%r14)
	leal	-13(%rbx), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rax
	xorl	%ecx, %ecx
	cmpl	$12, %ebx
	cmovleq	%rcx, %rax
	movq	%rax, 72(%r14)
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	gs_type1_init_matrix, .Lfunc_end2-gs_type1_init_matrix
	.cfi_endproc

	.globl	gs_type1_init
	.p2align	4, 0x90
	.type	gs_type1_init,@function
gs_type1_init:                          # @gs_type1_init
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	(%rsi), %rbx
	movq	%rsi, (%rdi)
	movq	%rbx, 8(%rdi)
	movq	%r9, 16(%rdi)
	movl	%edx, 24(%rdi)
	movl	%ecx, 28(%rdi)
	movl	$0, 272(%rdi)
	movq	%r8, 280(%rdi)
	movw	$4330, 288(%rdi)        # imm = 0x10EA
	movl	$1, 456(%rdi)
	movl	24(%r9), %eax
	movl	%eax, 460(%rdi)
	movl	$-1, 496(%rdi)
	callq	gs_type1_init_matrix
	movq	256(%rbx), %rax
	movups	120(%rbx), %xmm0
	movups	%xmm0, 120(%rax)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	gs_type1_init, .Lfunc_end3-gs_type1_init
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4553139223271571456     # double 2.44140625E-4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_1:
	.long	1065353216              # float 1
.LCPI4_2:
	.long	1166016512              # float 4096
	.text
	.globl	gs_type1_interpret
	.p2align	4, 0x90
	.type	gs_type1_interpret,@function
gs_type1_interpret:                     # @gs_type1_interpret
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$360, %rsp              # imm = 0x168
.Lcfi21:
	.cfi_def_cfa_offset 416
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movq	8(%rbp), %rax
	movq	16(%rbp), %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	256(%rax), %rdx
	movslq	456(%rbp), %r13
	shlq	$4, %r13
	movl	460(%rbp), %ecx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movq	120(%rax), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	128(%rax), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	120(%rdx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	128(%rdx), %r14
	movq	32(%rbp), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	40(%rbp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	48(%rbp), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movslq	272(%rbp), %rbx
	testq	%rbx, %rbx
	movq	56(%rbp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	64(%rbp), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	68(%rbp), %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	72(%rbp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	je	.LBB4_2
# BB#1:
	leaq	80(%rbp), %rsi
	movq	%rbx, %rdx
	shlq	$3, %rdx
	leaq	160(%rsp), %rdi
	callq	memcpy
	leal	-1(%rbx), %eax
	cltq
	jmp	.LBB4_3
.LBB4_2:
	movq	$-1, %rax
.LBB4_3:
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	leaq	264(%rbp,%r13), %rcx
	testq	%r15, %r15
	movq	136(%rsp), %rdx         # 8-byte Reload
	je	.LBB4_5
# BB#4:
	movq	%r15, (%rcx)
.LBB4_5:                                # %.preheader
	leaq	160(%rsp,%rax,8), %r13
	leaq	152(%rsp), %r11
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r10d
	negl	%r10d
	movq	%r10, 96(%rsp)          # 8-byte Spill
	jmp	.LBB4_7
.LBB4_6:                                #   in Loop: Header=BB4_7 Depth=1
	movq	128(%rsp), %rcx         # 8-byte Reload
	addq	$-16, %rcx
	movq	136(%rsp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_9 Depth 2
                                        #       Child Loop BB4_10 Depth 3
                                        #       Child Loop BB4_13 Depth 3
	movq	(%rcx), %r15
	movw	8(%rcx), %r12w
	movq	%r13, %rax
	jmp	.LBB4_9
.LBB4_8:                                #   in Loop: Header=BB4_9 Depth=2
	addq	$-8, %r13
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	%r15, (%rcx)
	movw	%r12w, 8(%rcx)
	addq	$16, %rcx
	movq	80(%rsp), %r15
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movl	24(%rax), %edx
	movw	$4330, %r12w            # imm = 0x10EA
	movq	%r13, %rax
	movq	96(%rsp), %r10          # 8-byte Reload
	leaq	152(%rsp), %r11
	.p2align	4, 0x90
.LBB4_9:                                #   Parent Loop BB4_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_10 Depth 3
                                        #       Child Loop BB4_13 Depth 3
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	testl	%edx, %edx
	jle	.LBB4_11
	.p2align	4, 0x90
.LBB4_10:                               # %.lr.ph
                                        #   Parent Loop BB4_7 Depth=1
                                        #     Parent Loop BB4_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%r15), %ecx
	addl	%ecx, %r12d
	imull	$-12691, %r12d, %r12d   # imm = 0xCE6D
	addl	$22719, %r12d           # imm = 0x58BF
	incq	%r15
	cmpl	$1, %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, %edx
	jg	.LBB4_10
.LBB4_11:                               # %.thread705.preheader
                                        #   in Loop: Header=BB4_9 Depth=2
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	jmp	.LBB4_13
.LBB4_12:                               #   in Loop: Header=BB4_13 Depth=3
	imulq	32(%rsp), %rdx          # 8-byte Folded Reload
	movq	56(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdx,%rdi), %rax
	movq	64(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %ecx
	sarq	%cl, %rax
	movl	%r10d, %ecx
	shlq	%cl, %rdx
	imulq	40(%rsp), %rsi          # 8-byte Folded Reload
	leaq	(%rsi,%rdi), %rdi
	movl	%ebx, %ecx
	sarq	%cl, %rdi
	movl	%r10d, %ecx
	shlq	%cl, %rsi
	testl	%ebx, %ebx
	cmovnsq	%rax, %rdx
	cmovnsq	%rdi, %rsi
	addq	%rdx, %r14
	addq	%rsi, 16(%rsp)          # 8-byte Folded Spill
	movq	%rbp, %r15
	movq	%r11, %rax
	.p2align	4, 0x90
.LBB4_13:                               # %.thread705
                                        #   Parent Loop BB4_7 Depth=1
                                        #     Parent Loop BB4_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, %r13
	movq	%r15, %rbp
	movzbl	(%rbp), %eax
	movzwl	%r12w, %edx
	movl	%edx, %ecx
	shrl	$8, %ecx
	xorl	%eax, %ecx
	addl	%edx, %eax
	imull	$52845, %eax, %r12d     # imm = 0xCE6D
	addl	$22719, %r12d           # imm = 0x58BF
	leaq	1(%rbp), %r15
	cmpb	$31, %cl
	ja	.LBB4_17
# BB#14:                                # %.thread705
                                        #   in Loop: Header=BB4_13 Depth=3
	movl	$-10, %ebx
	movq	%r11, %rax
	jmpq	*.LJTI4_0(,%rcx,8)
.LBB4_15:                               #   in Loop: Header=BB4_13 Depth=3
	movq	160(%rsp), %rax
	shrq	$12, %rax
	cltq
	movq	%rax, %rdx
	imulq	48(%rsp), %rdx          # 8-byte Folded Reload
	movq	56(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdx,%rdi), %rsi
	movq	64(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %ecx
	sarq	%cl, %rsi
	movl	%r10d, %ecx
	shlq	%cl, %rdx
	testl	%ebp, %ebp
	cmovnsq	%rsi, %rdx
	addq	%rdx, %r14
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_49
# BB#16:                                #   in Loop: Header=BB4_13 Depth=3
	imulq	40(%rsp), %rax          # 8-byte Folded Reload
	leaq	(%rax,%rdi), %rdx
	movl	%ebp, %ecx
	sarq	%cl, %rdx
	movl	%r10d, %ecx
	shlq	%cl, %rax
	testl	%ebp, %ebp
	cmovnsq	%rdx, %rax
	movq	16(%rsp), %rsi          # 8-byte Reload
	addq	%rax, %rsi
	jmp	.LBB4_53
.LBB4_17:                               #   in Loop: Header=BB4_13 Depth=3
	addq	$8, %r13
	cmpl	$246, %ecx
	ja	.LBB4_47
# BB#18:                                #   in Loop: Header=BB4_13 Depth=3
	shlq	$12, %rcx
	addq	$-569344, %rcx          # imm = 0xFFF75000
	movq	%rcx, (%r13)
	movq	%r13, %rax
	jmp	.LBB4_13
.LBB4_19:                               #   in Loop: Header=BB4_13 Depth=3
	movq	160(%rsp), %rax
	movq	168(%rsp), %rdx
	shrq	$12, %rax
	cltq
	movq	%rax, %r13
	imulq	72(%rsp), %r13          # 8-byte Folded Reload
	movq	56(%rsp), %r8           # 8-byte Reload
	leaq	(%r13,%r8), %rsi
	movq	64(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %ecx
	sarq	%cl, %rsi
	movl	%r10d, %ecx
	shlq	%cl, %r13
	shrq	$12, %rdx
	movslq	%edx, %rdx
	movq	%rdx, %rbp
	imulq	48(%rsp), %rbp          # 8-byte Folded Reload
	leaq	(%rbp,%r8), %rdi
	movl	%ebx, %ecx
	sarq	%cl, %rdi
	movl	%r10d, %ecx
	shlq	%cl, %rbp
	testl	%ebx, %ebx
	cmovnsq	%rsi, %r13
	cmovnsq	%rdi, %rbp
	addq	16(%rsp), %r13          # 8-byte Folded Reload
	addq	%r14, %rbp
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_52
# BB#20:                                #   in Loop: Header=BB4_13 Depth=3
	imulq	32(%rsp), %rax          # 8-byte Folded Reload
	leaq	(%rax,%r8), %rsi
	movl	%ebx, %ecx
	sarq	%cl, %rsi
	movl	%r10d, %ecx
	shlq	%cl, %rax
	imulq	40(%rsp), %rdx          # 8-byte Folded Reload
	leaq	(%rdx,%r8), %rdi
	movl	%ebx, %ecx
	sarq	%cl, %rdi
	movl	%r10d, %ecx
	shlq	%cl, %rdx
	testl	%ebx, %ebx
	cmovnsq	%rsi, %rax
	cmovnsq	%rdi, %rdx
	addq	%rax, %rbp
	addq	%rdx, %r13
	jmp	.LBB4_52
.LBB4_21:                               #   in Loop: Header=BB4_13 Depth=3
	movq	160(%rsp), %rax
	shrq	$12, %rax
	movslq	%eax, %rbp
	movq	%rbp, %r13
	imulq	72(%rsp), %r13          # 8-byte Folded Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	leaq	(%r13,%rdx), %rax
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%esi, %ecx
	sarq	%cl, %rax
	movl	%r10d, %ecx
	shlq	%cl, %r13
	testl	%esi, %esi
	cmovnsq	%rax, %r13
	addq	16(%rsp), %r13          # 8-byte Folded Reload
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_50
# BB#22:                                #   in Loop: Header=BB4_13 Depth=3
	imulq	32(%rsp), %rbp          # 8-byte Folded Reload
	leaq	(%rbp,%rdx), %rax
	movl	%esi, %ecx
	sarq	%cl, %rax
	movl	%r10d, %ecx
	shlq	%cl, %rbp
	testl	%esi, %esi
	cmovnsq	%rax, %rbp
	addq	%r14, %rbp
	jmp	.LBB4_52
.LBB4_23:                               #   in Loop: Header=BB4_13 Depth=3
	movq	160(%rsp), %rax
	shrq	$12, %rax
	movslq	%eax, %r13
	movq	%r13, %rbp
	imulq	48(%rsp), %rbp          # 8-byte Folded Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	leaq	(%rbp,%rdx), %rax
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%esi, %ecx
	sarq	%cl, %rax
	movl	%r10d, %ecx
	shlq	%cl, %rbp
	testl	%esi, %esi
	cmovnsq	%rax, %rbp
	addq	%r14, %rbp
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_51
# BB#24:                                #   in Loop: Header=BB4_13 Depth=3
	imulq	40(%rsp), %r13          # 8-byte Folded Reload
	leaq	(%r13,%rdx), %rax
	movl	%esi, %ecx
	sarq	%cl, %rax
	movl	%r10d, %ecx
	shlq	%cl, %r13
	testl	%esi, %esi
	cmovnsq	%rax, %r13
	addq	16(%rsp), %r13          # 8-byte Folded Reload
	jmp	.LBB4_52
.LBB4_25:                               #   in Loop: Header=BB4_13 Depth=3
	movq	160(%rsp), %rax
	movq	168(%rsp), %rdx
	shrq	$12, %rax
	cltq
	movq	%rax, %rsi
	movq	%r10, %r13
	movq	72(%rsp), %r10          # 8-byte Reload
	imulq	%r10, %rsi
	movq	56(%rsp), %r8           # 8-byte Reload
	leaq	(%rsi,%r8), %rbp
	movq	64(%rsp), %r11          # 8-byte Reload
	movl	%r11d, %ecx
	sarq	%cl, %rbp
	movl	%r13d, %ecx
	shlq	%cl, %rsi
	shrq	$12, %rdx
	movslq	%edx, %rdi
	movq	%rdi, %rdx
	imulq	48(%rsp), %rdx          # 8-byte Folded Reload
	leaq	(%rdx,%r8), %rbx
	movl	%r11d, %ecx
	sarq	%cl, %rbx
	movl	%r13d, %ecx
	shlq	%cl, %rdx
	testl	%r11d, %r11d
	cmovnsq	%rbp, %rsi
	cmovnsq	%rbx, %rdx
	addq	16(%rsp), %rsi          # 8-byte Folded Reload
	addq	%r14, %rdx
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_27
# BB#26:                                #   in Loop: Header=BB4_13 Depth=3
	imulq	32(%rsp), %rax          # 8-byte Folded Reload
	leaq	(%rax,%r8), %rbp
	movl	%r11d, %ecx
	sarq	%cl, %rbp
	movl	%r13d, %ecx
	shlq	%cl, %rax
	imulq	40(%rsp), %rdi          # 8-byte Folded Reload
	leaq	(%rdi,%r8), %rbx
	movl	%r11d, %ecx
	sarq	%cl, %rbx
	movl	%r13d, %ecx
	shlq	%cl, %rdi
	testl	%r11d, %r11d
	cmovnsq	%rbp, %rax
	cmovnsq	%rbx, %rdi
	addq	%rax, %rdx
	addq	%rdi, %rsi
.LBB4_27:                               #   in Loop: Header=BB4_13 Depth=3
	movq	%r8, %r14
	movq	176(%rsp), %rax
	movq	184(%rsp), %rbp
	shrq	$12, %rax
	movslq	%eax, %r9
	movq	%r9, %rax
	imulq	%r10, %rax
	leaq	(%rax,%r14), %rbx
	movl	%r11d, %ecx
	sarq	%cl, %rbx
	movl	%r13d, %ecx
	shlq	%cl, %rax
	shrq	$12, %rbp
	movslq	%ebp, %rbp
	movq	%rbp, %r8
	imulq	48(%rsp), %r8           # 8-byte Folded Reload
	leaq	(%r8,%r14), %rdi
	movl	%r11d, %ecx
	sarq	%cl, %rdi
	movl	%r13d, %ecx
	shlq	%cl, %r8
	testl	%r11d, %r11d
	cmovnsq	%rbx, %rax
	cmovnsq	%rdi, %r8
	addq	%rsi, %rax
	addq	%rdx, %r8
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_29
# BB#28:                                #   in Loop: Header=BB4_13 Depth=3
	imulq	32(%rsp), %r9           # 8-byte Folded Reload
	leaq	(%r9,%r14), %rdi
	movl	%r11d, %ecx
	sarq	%cl, %rdi
	movl	%r13d, %ecx
	shlq	%cl, %r9
	imulq	40(%rsp), %rbp          # 8-byte Folded Reload
	leaq	(%rbp,%r14), %rbx
	movl	%r11d, %ecx
	sarq	%cl, %rbx
	movl	%r13d, %ecx
	shlq	%cl, %rbp
	testl	%r11d, %r11d
	cmovnsq	%rdi, %r9
	cmovnsq	%rbx, %rbp
	addq	%r9, %r8
	addq	%rbp, %rax
.LBB4_29:                               #   in Loop: Header=BB4_13 Depth=3
	movq	192(%rsp), %rcx
	movq	200(%rsp), %rbp
	shrq	$12, %rcx
	movslq	%ecx, %r13
	movq	%r13, %r9
	imulq	%r10, %r9
	leaq	(%r9,%r14), %r10
	movl	%r11d, %ecx
	sarq	%cl, %r10
	movq	96(%rsp), %rdi          # 8-byte Reload
	movl	%edi, %ecx
	shlq	%cl, %r9
	shrq	$12, %rbp
	movslq	%ebp, %rbp
	movq	%r14, %rcx
	movq	%rbp, %r14
	imulq	48(%rsp), %r14          # 8-byte Folded Reload
	leaq	(%r14,%rcx), %rbx
	movl	%r11d, %ecx
	sarq	%cl, %rbx
	movl	%edi, %ecx
	shlq	%cl, %r14
	testl	%r11d, %r11d
	cmovnsq	%r10, %r9
	cmovnsq	%rbx, %r14
	addq	%rax, %r9
	addq	%r8, %r14
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_31
# BB#30:                                #   in Loop: Header=BB4_13 Depth=3
	imulq	32(%rsp), %r13          # 8-byte Folded Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	leaq	(%r13,%rbx), %r10
	movl	%r11d, %ecx
	sarq	%cl, %r10
	movq	96(%rsp), %rdi          # 8-byte Reload
	movl	%edi, %ecx
	shlq	%cl, %r13
	imulq	40(%rsp), %rbp          # 8-byte Folded Reload
	leaq	(%rbp,%rbx), %rbx
	movl	%r11d, %ecx
	sarq	%cl, %rbx
	movl	%edi, %ecx
	shlq	%cl, %rbp
	testl	%r11d, %r11d
	cmovnsq	%r10, %r13
	cmovnsq	%rbx, %rbp
	addq	%r13, %r14
	addq	%rbp, %r9
.LBB4_31:                               #   in Loop: Header=BB4_13 Depth=3
	movq	24(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB4_67
.LBB4_32:                               #   in Loop: Header=BB4_13 Depth=3
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	gx_path_close_subpath
	movl	%eax, %ebx
	testl	%ebx, %ebx
	movq	16(%rsp), %rsi          # 8-byte Reload
	jns	.LBB4_54
	jmp	.LBB4_102
.LBB4_33:                               #   in Loop: Header=BB4_13 Depth=3
	movzbl	1(%rbp), %ecx
	movl	%r12d, %eax
	shrl	$8, %eax
	xorl	%ecx, %eax
	cmpb	$33, %al
	ja	.LBB4_102
# BB#34:                                #   in Loop: Header=BB4_13 Depth=3
	addl	%r12d, %ecx
	imull	$52845, %ecx, %r12d     # imm = 0xCE6D
	addl	$22719, %r12d           # imm = 0x58BF
	addq	$2, %rbp
	movzbl	%al, %ecx
	movq	%rbp, %r15
	movq	%r11, %rax
	movq	112(%rsp), %r8          # 8-byte Reload
	movq	120(%rsp), %r9          # 8-byte Reload
	jmpq	*.LJTI4_1(,%rcx,8)
.LBB4_35:                               #   in Loop: Header=BB4_13 Depth=3
	movaps	160(%rsp), %xmm0
	movq	104(%rsp), %rax         # 8-byte Reload
	movups	%xmm0, 464(%rax)
	movaps	176(%rsp), %xmm0
	movups	%xmm0, 480(%rax)
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%r14, %r9
	jmp	.LBB4_37
.LBB4_36:                               #   in Loop: Header=BB4_13 Depth=3
	movq	160(%rsp), %rax
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	%rax, 464(%rcx)
	movq	$0, 472(%rcx)
	movq	168(%rsp), %rax
	movq	%rax, 480(%rcx)
	movq	$0, 488(%rcx)
	movq	$0, 168(%rsp)
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%r14, %r9
	movq	%r15, %rbp
.LBB4_37:                               #   in Loop: Header=BB4_13 Depth=3
	movq	160(%rsp), %rax
	movq	168(%rsp), %rsi
	shrq	$12, %rax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	imulq	72(%rsp), %rax          # 8-byte Folded Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	leaq	(%rax,%rbx), %r15
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	%edi, %ecx
	sarq	%cl, %r15
	movl	%r10d, %ecx
	shlq	%cl, %rax
	shrq	$12, %rsi
	movslq	%esi, %rsi
	movq	%rsi, %r14
	imulq	48(%rsp), %r14          # 8-byte Folded Reload
	leaq	(%r14,%rbx), %rbx
	movl	%edi, %ecx
	sarq	%cl, %rbx
	movl	%r10d, %ecx
	shlq	%cl, %r14
	testl	%edi, %edi
	cmovnsq	%r15, %rax
	cmovnsq	%rbx, %r14
	addq	%r8, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	addq	%r9, %r14
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movq	%rbp, %r15
	movq	%r11, %rax
	je	.LBB4_13
	jmp	.LBB4_12
.LBB4_38:                               #   in Loop: Header=BB4_13 Depth=3
	movq	160(%rsp), %rax
	movq	168(%rsp), %rdx
	shrq	$12, %rax
	cltq
	movq	%rax, %rsi
	imulq	72(%rsp), %rsi          # 8-byte Folded Reload
	movq	56(%rsp), %r8           # 8-byte Reload
	leaq	(%rsi,%r8), %rdi
	movq	64(%rsp), %r9           # 8-byte Reload
	movl	%r9d, %ecx
	sarq	%cl, %rdi
	movl	%r10d, %ecx
	shlq	%cl, %rsi
	shrq	$12, %rdx
	movslq	%edx, %rbp
	movq	%rbp, %rdx
	imulq	48(%rsp), %rdx          # 8-byte Folded Reload
	leaq	(%rdx,%r8), %rbx
	movl	%r9d, %ecx
	sarq	%cl, %rbx
	movl	%r10d, %ecx
	shlq	%cl, %rdx
	testl	%r9d, %r9d
	cmovnsq	%rdi, %rsi
	cmovnsq	%rbx, %rdx
	movq	16(%rsp), %rdi          # 8-byte Reload
	addq	%rsi, %rdi
	addq	%rdx, %r14
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_40
# BB#39:                                #   in Loop: Header=BB4_13 Depth=3
	imulq	32(%rsp), %rax          # 8-byte Folded Reload
	leaq	(%rax,%r8), %rdx
	movl	%r9d, %ecx
	sarq	%cl, %rdx
	movl	%r10d, %ecx
	shlq	%cl, %rax
	imulq	40(%rsp), %rbp          # 8-byte Folded Reload
	leaq	(%rbp,%r8), %rsi
	movl	%r9d, %ecx
	sarq	%cl, %rsi
	movl	%r10d, %ecx
	shlq	%cl, %rbp
	testl	%r9d, %r9d
	cmovnsq	%rdx, %rax
	cmovnsq	%rsi, %rbp
	addq	%rax, %r14
	addq	%rbp, %rdi
.LBB4_40:                               #   in Loop: Header=BB4_13 Depth=3
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rdi, %rsi
	jmp	.LBB4_54
.LBB4_41:                               #   in Loop: Header=BB4_13 Depth=3
	movq	160(%rsp), %rax
	shrq	$12, %rax
	cltq
	movq	%rax, %rdx
	imulq	72(%rsp), %rdx          # 8-byte Folded Reload
	movq	56(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdx,%rdi), %rsi
	movq	64(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %ecx
	sarq	%cl, %rsi
	movl	%r10d, %ecx
	shlq	%cl, %rdx
	testl	%ebp, %ebp
	cmovnsq	%rsi, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	addq	%rdx, %rsi
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_53
# BB#42:                                #   in Loop: Header=BB4_13 Depth=3
	imulq	32(%rsp), %rax          # 8-byte Folded Reload
	leaq	(%rax,%rdi), %rdx
	movl	%ebp, %ecx
	sarq	%cl, %rdx
	movl	%r10d, %ecx
	shlq	%cl, %rax
	testl	%ebp, %ebp
	cmovnsq	%rdx, %rax
	addq	%rax, %r14
.LBB4_53:                               #   in Loop: Header=BB4_13 Depth=3
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB4_54:                               #   in Loop: Header=BB4_13 Depth=3
	movq	%rbp, %rdi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%r14, %rdx
	callq	gx_path_add_point
	jmp	.LBB4_68
.LBB4_43:                               #   in Loop: Header=BB4_13 Depth=3
	movq	160(%rsp), %rax
	shrq	$12, %rax
	movslq	%eax, %rsi
	movq	%rsi, %rdx
	movq	48(%rsp), %rdi          # 8-byte Reload
	imulq	%rdi, %rdx
	movq	56(%rsp), %r11          # 8-byte Reload
	leaq	(%rdx,%r11), %rax
	movq	64(%rsp), %r13          # 8-byte Reload
	movl	%r13d, %ecx
	sarq	%cl, %rax
	movl	%r10d, %ecx
	shlq	%cl, %rdx
	testl	%r13d, %r13d
	cmovnsq	%rax, %rdx
	addq	%r14, %rdx
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_55
# BB#44:                                #   in Loop: Header=BB4_13 Depth=3
	imulq	40(%rsp), %rsi          # 8-byte Folded Reload
	leaq	(%rsi,%r11), %rax
	movl	%r13d, %ecx
	sarq	%cl, %rax
	movl	%r10d, %ecx
	shlq	%cl, %rsi
	testl	%r13d, %r13d
	cmovnsq	%rax, %rsi
	addq	16(%rsp), %rsi          # 8-byte Folded Reload
	jmp	.LBB4_56
.LBB4_45:                               #   in Loop: Header=BB4_13 Depth=3
	movq	160(%rsp), %rax
	shrq	$12, %rax
	movslq	%eax, %rdx
	movq	%rdx, %rsi
	movq	72(%rsp), %rdi          # 8-byte Reload
	imulq	%rdi, %rsi
	movq	56(%rsp), %r11          # 8-byte Reload
	leaq	(%rsi,%r11), %rax
	movq	64(%rsp), %r13          # 8-byte Reload
	movl	%r13d, %ecx
	sarq	%cl, %rax
	movl	%r10d, %ecx
	shlq	%cl, %rsi
	testl	%r13d, %r13d
	cmovnsq	%rax, %rsi
	addq	16(%rsp), %rsi          # 8-byte Folded Reload
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_60
# BB#46:                                #   in Loop: Header=BB4_13 Depth=3
	imulq	32(%rsp), %rdx          # 8-byte Folded Reload
	leaq	(%rdx,%r11), %rax
	movl	%r13d, %ecx
	sarq	%cl, %rax
	movl	%r10d, %ecx
	shlq	%cl, %rdx
	testl	%r13d, %r13d
	cmovnsq	%rax, %rdx
	addq	%r14, %rdx
	jmp	.LBB4_61
.LBB4_47:                               #   in Loop: Header=BB4_13 Depth=3
	movzbl	1(%rbp), %edx
	movzwl	%r12w, %esi
	shrl	$8, %r12d
	movl	%r12d, %eax
	xorl	%edx, %eax
	addl	%edx, %esi
	imull	$52845, %esi, %r12d     # imm = 0xCE6D
	addl	$22719, %r12d           # imm = 0x58BF
	leaq	2(%rbp), %r15
	cmpl	$250, %ecx
	ja	.LBB4_70
# BB#48:                                #   in Loop: Header=BB4_13 Depth=3
	shll	$8, %ecx
	addl	$2304, %ecx             # imm = 0x900
	movzwl	%cx, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	shll	$12, %eax
	addl	$442368, %eax           # imm = 0x6C000
	jmp	.LBB4_73
.LBB4_49:                               #   in Loop: Header=BB4_13 Depth=3
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB4_54
.LBB4_50:                               #   in Loop: Header=BB4_13 Depth=3
	movq	%r14, %rbp
	jmp	.LBB4_52
.LBB4_51:                               #   in Loop: Header=BB4_13 Depth=3
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB4_52:                               #   in Loop: Header=BB4_13 Depth=3
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	gx_path_add_line
	movl	%eax, %ebx
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%rbp, %r14
	jmp	.LBB4_69
.LBB4_55:                               #   in Loop: Header=BB4_13 Depth=3
	movq	16(%rsp), %rsi          # 8-byte Reload
.LBB4_56:                               #   in Loop: Header=BB4_13 Depth=3
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	168(%rsp), %rax
	movq	176(%rsp), %rbp
	shrq	$12, %rax
	movslq	%eax, %r9
	movq	%r9, %rax
	imulq	%r14, %rax
	leaq	(%rax,%r11), %rbx
	movl	%r13d, %ecx
	sarq	%cl, %rbx
	movl	%r10d, %ecx
	shlq	%cl, %rax
	shrq	$12, %rbp
	movslq	%ebp, %rbp
	movq	%rbp, %r8
	imulq	%rdi, %r8
	leaq	(%r8,%r11), %rdi
	movl	%r13d, %ecx
	sarq	%cl, %rdi
	movl	%r10d, %ecx
	shlq	%cl, %r8
	testl	%r13d, %r13d
	cmovnsq	%rbx, %rax
	cmovnsq	%rdi, %r8
	addq	%rsi, %rax
	addq	%rdx, %r8
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_58
# BB#57:                                #   in Loop: Header=BB4_13 Depth=3
	imulq	32(%rsp), %r9           # 8-byte Folded Reload
	leaq	(%r9,%r11), %rdi
	movl	%r13d, %ecx
	sarq	%cl, %rdi
	movl	%r10d, %ecx
	shlq	%cl, %r9
	imulq	40(%rsp), %rbp          # 8-byte Folded Reload
	leaq	(%rbp,%r11), %rbx
	movl	%r13d, %ecx
	sarq	%cl, %rbx
	movl	%r10d, %ecx
	shlq	%cl, %rbp
	testl	%r13d, %r13d
	cmovnsq	%rdi, %r9
	cmovnsq	%rbx, %rbp
	addq	%r9, %r8
	addq	%rbp, %rax
.LBB4_58:                               #   in Loop: Header=BB4_13 Depth=3
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	184(%rsp), %rcx
	shrq	$12, %rcx
	movslq	%ecx, %r14
	movq	%r14, %r9
	imulq	72(%rsp), %r9           # 8-byte Folded Reload
	leaq	(%r9,%r11), %rbp
	movl	%r13d, %ecx
	sarq	%cl, %rbp
	movl	%r10d, %ecx
	shlq	%cl, %r9
	testl	%r13d, %r13d
	cmovnsq	%rbp, %r9
	addq	%rax, %r9
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_65
# BB#59:                                #   in Loop: Header=BB4_13 Depth=3
	imulq	32(%rsp), %r14          # 8-byte Folded Reload
	leaq	(%r14,%r11), %rbp
	movl	%r13d, %ecx
	sarq	%cl, %rbp
	movl	%r10d, %ecx
	shlq	%cl, %r14
	testl	%r13d, %r13d
	cmovnsq	%rbp, %r14
	addq	%r8, %r14
	jmp	.LBB4_67
.LBB4_60:                               #   in Loop: Header=BB4_13 Depth=3
	movq	%r14, %rdx
.LBB4_61:                               #   in Loop: Header=BB4_13 Depth=3
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	168(%rsp), %rax
	movq	176(%rsp), %rbp
	shrq	$12, %rax
	movslq	%eax, %r9
	movq	%r9, %rax
	imulq	%rdi, %rax
	leaq	(%rax,%r11), %rbx
	movl	%r13d, %ecx
	sarq	%cl, %rbx
	movl	%r10d, %ecx
	shlq	%cl, %rax
	shrq	$12, %rbp
	movslq	%ebp, %rbp
	movq	%rbp, %r8
	imulq	%r14, %r8
	leaq	(%r8,%r11), %rdi
	movl	%r13d, %ecx
	sarq	%cl, %rdi
	movl	%r10d, %ecx
	shlq	%cl, %r8
	testl	%r13d, %r13d
	cmovnsq	%rbx, %rax
	cmovnsq	%rdi, %r8
	addq	%rsi, %rax
	addq	%rdx, %r8
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_63
# BB#62:                                #   in Loop: Header=BB4_13 Depth=3
	imulq	32(%rsp), %r9           # 8-byte Folded Reload
	leaq	(%r9,%r11), %rdi
	movl	%r13d, %ecx
	sarq	%cl, %rdi
	movl	%r10d, %ecx
	shlq	%cl, %r9
	imulq	40(%rsp), %rbp          # 8-byte Folded Reload
	leaq	(%rbp,%r11), %rbx
	movl	%r13d, %ecx
	sarq	%cl, %rbx
	movl	%r10d, %ecx
	shlq	%cl, %rbp
	testl	%r13d, %r13d
	cmovnsq	%rdi, %r9
	cmovnsq	%rbx, %rbp
	addq	%r9, %r8
	addq	%rbp, %rax
.LBB4_63:                               #   in Loop: Header=BB4_13 Depth=3
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	184(%rsp), %rcx
	shrq	$12, %rcx
	movslq	%ecx, %r9
	movq	%r9, %r14
	imulq	48(%rsp), %r14          # 8-byte Folded Reload
	leaq	(%r14,%r11), %rbp
	movl	%r13d, %ecx
	sarq	%cl, %rbp
	movl	%r10d, %ecx
	shlq	%cl, %r14
	testl	%r13d, %r13d
	cmovnsq	%rbp, %r14
	addq	%r8, %r14
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_66
# BB#64:                                #   in Loop: Header=BB4_13 Depth=3
	imulq	40(%rsp), %r9           # 8-byte Folded Reload
	leaq	(%r9,%r11), %rbp
	movl	%r13d, %ecx
	sarq	%cl, %rbp
	movl	%r10d, %ecx
	shlq	%cl, %r9
	testl	%r13d, %r13d
	cmovnsq	%rbp, %r9
	addq	%rax, %r9
	jmp	.LBB4_67
.LBB4_65:                               #   in Loop: Header=BB4_13 Depth=3
	movq	%r8, %r14
	jmp	.LBB4_67
.LBB4_66:                               #   in Loop: Header=BB4_13 Depth=3
	movq	%rax, %r9
.LBB4_67:                               #   in Loop: Header=BB4_13 Depth=3
	movq	%r14, (%rsp)
	movq	%rax, %rcx
	movq	%r9, 16(%rsp)           # 8-byte Spill
	callq	gx_path_add_curve
.LBB4_68:                               #   in Loop: Header=BB4_13 Depth=3
	movl	%eax, %ebx
.LBB4_69:                               #   in Loop: Header=BB4_13 Depth=3
	movq	96(%rsp), %r10          # 8-byte Reload
	leaq	152(%rsp), %r11
	testl	%ebx, %ebx
	movq	%r11, %rax
	jns	.LBB4_13
	jmp	.LBB4_102
.LBB4_70:                               #   in Loop: Header=BB4_13 Depth=3
	cmpl	$255, %ecx
	jne	.LBB4_72
# BB#71:                                #   in Loop: Header=BB4_13 Depth=3
	movzbl	2(%rbp), %r8d
	movzwl	%r12w, %ecx
	addl	%r8d, %ecx
	imull	$52845, %ecx, %edi      # imm = 0xCE6D
	leal	22719(%rdi), %ecx
	movzbl	3(%rbp), %r9d
	leal	22719(%rdi,%r9), %edi
	imull	$52845, %edi, %edi      # imm = 0xCE6D
	addl	$22719, %edi            # imm = 0x58BF
	movzbl	4(%rbp), %ebx
	movzwl	%di, %edx
	movl	%edx, %esi
	shrl	$8, %esi
	xorl	%ebx, %esi
	addl	%ebx, %edx
	movl	%r12d, %ebx
	movzbl	%bh, %edi  # NOREX
	imull	$52845, %edx, %edx      # imm = 0xCE6D
	addl	$22719, %edx            # imm = 0x58BF
	addq	$5, %rbp
	shll	$24, %eax
	xorl	%r8d, %edi
	shll	$16, %edi
	shll	$8, %r9d
	andl	$65280, %ecx            # imm = 0xFF00
	xorl	%r9d, %ecx
	orl	%eax, %edi
	orl	%ecx, %edi
	orl	%esi, %edi
	movq	%rdi, %rax
	shlq	$12, %rax
	movq	%rax, (%r13)
	movslq	%edi, %rax
	cmpq	%rax, %rdi
	movw	%dx, %r12w
	movq	%rbp, %r15
	movq	%r13, %rax
	je	.LBB4_13
	jmp	.LBB4_91
.LBB4_72:                               #   in Loop: Header=BB4_13 Depth=3
	shll	$8, %ecx
	addl	$1280, %ecx             # imm = 0x500
	movzwl	%cx, %ecx
	movzbl	%al, %eax
	orl	%ecx, %eax
	movl	$-108, %ecx
	subl	%eax, %ecx
	movslq	%ecx, %rax
	shlq	$12, %rax
.LBB4_73:                               # %.thread705
                                        #   in Loop: Header=BB4_13 Depth=3
	movq	%rax, (%r13)
	movq	%r13, %rax
	jmp	.LBB4_13
.LBB4_74:                               #   in Loop: Header=BB4_13 Depth=3
	leaq	-8(%r13), %rax
	cvtsi2ssq	-8(%r13), %xmm0
	cvtsi2ssq	(%r13), %xmm1
	divss	%xmm1, %xmm0
	mulss	.LCPI4_2(%rip), %xmm0
	cvttss2si	%xmm0, %rcx
	movq	%rcx, -8(%r13)
	movq	%rbp, %r15
	jmp	.LBB4_13
.LBB4_75:                               #   in Loop: Header=BB4_13 Depth=3
	movq	-16(%r13), %rax
	cmpq	-8(%r13), %rax
	jle	.LBB4_77
# BB#76:                                #   in Loop: Header=BB4_13 Depth=3
	movq	(%r13), %rax
	addq	%rax, -24(%r13)
.LBB4_77:                               # %._crit_edge807
                                        #   in Loop: Header=BB4_13 Depth=3
	addq	$-24, %r13
	movq	%rbp, %r15
	movq	%r13, %rax
	jmp	.LBB4_13
.LBB4_78:                               #   in Loop: Header=BB4_13 Depth=3
	addq	$8, %r13
	movq	152(%rsp), %rdi         # 8-byte Reload
	movq	%r13, %rsi
	callq	*8(%rdi)
	leaq	152(%rsp), %r11
	movq	96(%rsp), %r10          # 8-byte Reload
	movl	%eax, %ebx
	testl	%ebx, %ebx
	movq	%rbp, %r15
	movq	%r13, %rax
	jns	.LBB4_13
	jmp	.LBB4_102
.LBB4_79:                               #   in Loop: Header=BB4_9 Depth=2
	movq	(%r13), %rsi
	shrq	$12, %rsi
	movq	152(%rsp), %rdi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	leaq	80(%rsp), %rdx
	callq	*(%rdi)
	movl	%eax, %ebx
	testl	%ebx, %ebx
	jns	.LBB4_8
	jmp	.LBB4_102
.LBB4_80:
	movq	104(%rsp), %rbp         # 8-byte Reload
	movl	496(%rbp), %eax
	testl	%eax, %eax
	js	.LBB4_82
# BB#81:
	movl	$-1, 496(%rbp)
	movq	144(%rsp), %rcx         # 8-byte Reload
	movups	120(%rcx), %xmm0
	movq	24(%rsp), %rcx          # 8-byte Reload
	movups	%xmm0, 120(%rcx)
	leal	1(%rax,%rax), %ebx
	jmp	.LBB4_102
.LBB4_82:
	movq	144(%rsp), %r14         # 8-byte Reload
	movq	120(%r14), %rsi
	movq	128(%r14), %rdx
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdi
	callq	gx_path_add_point
	cmpl	$0, 24(%rbp)
	je	.LBB4_92
# BB#83:
	movq	(%rbp), %rdi
	cvtsi2sdq	480(%rbp), %xmm0
	movsd	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	cvtsi2sdq	488(%rbp), %xmm2
	mulsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	cvtss2sd	%xmm1, %xmm1
	callq	gs_setcharwidth
	movl	%eax, %ebx
	testl	%ebx, %ebx
	js	.LBB4_102
# BB#84:
	movq	(%r14), %rax
	movq	256(%rax), %rsi
	movq	%r15, %rdi
	callq	gx_path_merge
	jmp	.LBB4_101
.LBB4_85:
	movl	184(%rsp), %eax
	shrl	$12, %eax
	movzbl	%al, %eax
	movq	104(%rsp), %rcx         # 8-byte Reload
	movl	%eax, 496(%rcx)
	movq	168(%rsp), %rax
	subq	160(%rsp), %rax
	movq	%rax, 168(%rsp)
	shrq	$12, %rax
	movslq	%eax, %r8
	movq	72(%rsp), %rbp          # 8-byte Reload
	imulq	%r8, %rbp
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rbp,%rax), %rsi
	movq	64(%rsp), %r9           # 8-byte Reload
	movl	%r9d, %ecx
	sarq	%cl, %rsi
	movl	%r10d, %ecx
	shlq	%cl, %rbp
	movq	176(%rsp), %rcx
	shrq	$12, %rcx
	movslq	%ecx, %rdx
	movq	48(%rsp), %rbx          # 8-byte Reload
	imulq	%rdx, %rbx
	leaq	(%rbx,%rax), %rdi
	movl	%r9d, %ecx
	sarq	%cl, %rdi
	movl	%r10d, %ecx
	shlq	%cl, %rbx
	testl	%r9d, %r9d
	cmovnsq	%rsi, %rbp
	cmovnsq	%rdi, %rbx
	addq	112(%rsp), %rbp         # 8-byte Folded Reload
	addq	120(%rsp), %rbx         # 8-byte Folded Reload
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB4_87
# BB#86:
	imulq	32(%rsp), %r8           # 8-byte Folded Reload
	leaq	(%r8,%rax), %rsi
	movl	%r9d, %ecx
	sarq	%cl, %rsi
	movl	%r10d, %ecx
	shlq	%cl, %r8
	imulq	40(%rsp), %rdx          # 8-byte Folded Reload
	addq	%rdx, %rax
	movl	%r9d, %ecx
	sarq	%cl, %rax
	movl	%r10d, %ecx
	shlq	%cl, %rdx
	testl	%r9d, %r9d
	cmovnsq	%rsi, %r8
	cmovnsq	%rax, %rdx
	addq	%r8, %rbx
	addq	%rdx, %rbp
.LBB4_87:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbp, 120(%rax)
	movq	%rbx, 128(%rax)
	movl	192(%rsp), %ebx
	shrl	$11, %ebx
	andl	$510, %ebx              # imm = 0x1FE
	orl	$1, %ebx
	jmp	.LBB4_102
.LBB4_88:
	movq	(%r13), %rbx
	leaq	160(%rsp), %rax
	subq	%rax, %r13
	movq	%r13, %rax
	shrq	$3, %rax
	movq	128(%rsp), %rdx         # 8-byte Reload
	movq	%rbp, (%rdx)
	movw	%r12w, 8(%rdx)
	movq	104(%rsp), %rdi         # 8-byte Reload
	movl	%eax, 272(%rdi)
	leaq	280(%rdi), %rcx
	subq	%rcx, %rdx
	shrq	$4, %rdx
	incl	%edx
	movl	%edx, 456(%rdi)
	movl	$0, 460(%rdi)
	testl	%eax, %eax
	je	.LBB4_90
# BB#89:
	addq	$80, %rdi
	shlq	$29, %r13
	sarq	$29, %r13
	leaq	160(%rsp), %rsi
	movq	%r13, %rdx
	callq	memcpy
.LBB4_90:
	shrq	$11, %rbx
	addl	$2, %ebx
	andl	$-2, %ebx
	jmp	.LBB4_102
.LBB4_91:
	movl	$-15, %ebx
	jmp	.LBB4_102
.LBB4_92:
	leaq	80(%rsp), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	gs_pathbbox
	movl	%eax, %ebx
	movq	%r15, %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	120(%rsp), %rdx         # 8-byte Reload
	callq	gx_path_add_point
	testl	%ebx, %ebx
	jns	.LBB4_94
# BB#93:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 80(%rsp)
.LBB4_94:
	cmpl	$0, 28(%rbp)
	je	.LBB4_96
# BB#95:
	movq	%r14, %rdi
	callq	gs_currentlinewidth
	xorps	%xmm1, %xmm1
	cmpeqss	%xmm0, %xmm1
	movss	.LCPI4_1(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm2
	andnps	%xmm0, %xmm1
	orps	%xmm2, %xmm1
	movss	80(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	movss	%xmm2, 80(%rsp)
	movss	84(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm3
	movss	%xmm3, 84(%rsp)
	movss	88(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm4
	movss	%xmm4, 88(%rsp)
	movss	92(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm5
	movss	%xmm5, 92(%rsp)
	jmp	.LBB4_97
.LBB4_96:                               # %._crit_edge
	movss	80(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	84(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	88(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	92(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
.LBB4_97:
	movq	(%rbp), %rdi
	xorps	%xmm0, %xmm0
	cvtsi2sdq	480(%rbp), %xmm0
	movsd	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	cvtsi2sdq	488(%rbp), %xmm6
	mulsd	%xmm1, %xmm6
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm6, %xmm1
	cvtss2sd	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm2
	cvtss2sd	%xmm3, %xmm3
	cvtss2sd	%xmm4, %xmm4
	cvtss2sd	%xmm5, %xmm5
	callq	gs_setcachedevice
	movl	%eax, %ebx
	testl	%ebx, %ebx
	js	.LBB4_102
# BB#98:
	movq	256(%r14), %rdi
	movq	120(%r14), %rsi
	movq	128(%r14), %rdx
	subq	112(%rsp), %rsi         # 8-byte Folded Reload
	subq	120(%rsp), %rdx         # 8-byte Folded Reload
	callq	gx_path_translate
	cmpl	$0, 28(%rbp)
	je	.LBB4_100
# BB#99:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	gs_stroke
	jmp	.LBB4_101
.LBB4_100:
	movl	$819, %esi              # imm = 0x333
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	gs_fill_trim
.LBB4_101:                              # %.loopexit
	movl	%eax, %ebx
.LBB4_102:                              # %.loopexit
	movl	%ebx, %eax
	addq	$360, %rsp              # imm = 0x168
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	gs_type1_interpret, .Lfunc_end4-gs_type1_interpret
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_102
	.quad	.LBB4_13
	.quad	.LBB4_102
	.quad	.LBB4_13
	.quad	.LBB4_15
	.quad	.LBB4_19
	.quad	.LBB4_21
	.quad	.LBB4_23
	.quad	.LBB4_25
	.quad	.LBB4_32
	.quad	.LBB4_79
	.quad	.LBB4_6
	.quad	.LBB4_33
	.quad	.LBB4_36
	.quad	.LBB4_80
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_38
	.quad	.LBB4_41
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_43
	.quad	.LBB4_45
.LJTI4_1:
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_13
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_85
	.quad	.LBB4_35
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_74
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_75
	.quad	.LBB4_88
	.quad	.LBB4_78
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_102
	.quad	.LBB4_37

	.text
	.globl	gs_type1_pop
	.p2align	4, 0x90
	.type	gs_type1_pop,@function
gs_type1_pop:                           # @gs_type1_pop
	.cfi_startproc
# BB#0:
	movslq	272(%rdi), %rax
	leaq	-1(%rax), %rcx
	movl	%ecx, 272(%rdi)
	movq	72(%rdi,%rax,8), %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	gs_type1_pop, .Lfunc_end5-gs_type1_pop
	.cfi_endproc

	.type	gs_type1_state_sizeof,@object # @gs_type1_state_sizeof
	.data
	.globl	gs_type1_state_sizeof
	.p2align	2
gs_type1_state_sizeof:
	.long	504                     # 0x1f8
	.size	gs_type1_state_sizeof, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
