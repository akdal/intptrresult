	.text
	.file	"cdjpeg.bc"
	.globl	keymatch
	.p2align	4, 0x90
	.type	keymatch,@function
keymatch:                               # @keymatch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movb	(%r13), %al
	testb	%al, %al
	je	.LBB0_9
# BB#1:                                 # %.lr.ph.preheader
	movl	%edx, 4(%rsp)           # 4-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%r12,%rbp), %ebx
	testl	%ebx, %ebx
	je	.LBB0_12
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movsbl	%al, %r14d
	callq	__ctype_b_loc
	movl	%r14d, %ecx
	subl	$-128, %ecx
	cmpl	$383, %ecx              # imm = 0x17F
	ja	.LBB0_6
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	(%rax), %rax
	movslq	%r14d, %r15
	movzwl	(%rax,%r15,2), %eax
	andl	$256, %eax              # imm = 0x100
	testw	%ax, %ax
	je	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movl	(%rax,%r15,4), %r14d
.LBB0_6:                                # %tolower.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	%ebx, %r14d
	jne	.LBB0_12
# BB#7:                                 #   in Loop: Header=BB0_2 Depth=1
	movzbl	1(%r13,%rbp), %eax
	incq	%rbp
	testb	%al, %al
	jne	.LBB0_2
# BB#8:                                 # %._crit_edge.loopexit
	movl	4(%rsp), %edx           # 4-byte Reload
	jmp	.LBB0_10
.LBB0_12:
	xorl	%eax, %eax
	jmp	.LBB0_13
.LBB0_9:
	xorl	%ebp, %ebp
.LBB0_10:                               # %._crit_edge
	xorl	%eax, %eax
	cmpl	%edx, %ebp
	setge	%al
.LBB0_13:                               # %.loopexit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	keymatch, .Lfunc_end0-keymatch
	.cfi_endproc

	.globl	read_stdin
	.p2align	4, 0x90
	.type	read_stdin,@function
read_stdin:                             # @read_stdin
	.cfi_startproc
# BB#0:
	movq	stdin(%rip), %rax
	retq
.Lfunc_end1:
	.size	read_stdin, .Lfunc_end1-read_stdin
	.cfi_endproc

	.globl	write_stdout
	.p2align	4, 0x90
	.type	write_stdout,@function
write_stdout:                           # @write_stdout
	.cfi_startproc
# BB#0:
	movq	stdout(%rip), %rax
	retq
.Lfunc_end2:
	.size	write_stdout, .Lfunc_end2-write_stdout
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
