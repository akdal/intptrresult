	.text
	.file	"jdmerge.bc"
	.globl	jinit_merged_upsampler
	.p2align	4, 0x90
	.type	jinit_merged_upsampler,@function
jinit_merged_upsampler:                 # @jinit_merged_upsampler
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$88, %edx
	callq	*(%rax)
	movq	%rax, %r14
	movq	%r14, 592(%rbx)
	movq	$start_pass_merged_upsample, (%r14)
	movl	$0, 16(%r14)
	movl	136(%rbx), %eax
	imull	128(%rbx), %eax
	movl	%eax, 76(%r14)
	cmpl	$2, 392(%rbx)
	jne	.LBB0_2
# BB#1:
	movq	$merged_2v_upsample, 8(%r14)
	movq	$h2v2_merged_upsample, 24(%r14)
	movq	8(%rbx), %rcx
	movl	%eax, %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	*8(%rcx)
	movq	592(%rbx), %r15
	jmp	.LBB0_3
.LBB0_2:
	movq	$merged_1v_upsample, 8(%r14)
	movq	$h2v1_merged_upsample, 24(%r14)
	xorl	%eax, %eax
	movq	%r14, %r15
.LBB0_3:
	movq	%rax, 64(%r14)
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$1024, %edx             # imm = 0x400
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, 32(%r15)
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$1024, %edx             # imm = 0x400
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, 40(%r15)
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$2048, %edx             # imm = 0x800
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, 48(%r15)
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$2048, %edx             # imm = 0x800
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, 56(%r15)
	movq	32(%r15), %r8
	movq	40(%r15), %r9
	movq	48(%r15), %r10
	movq	$-14831872, %r11        # imm = 0xFF1DAF00
	movq	$-11728000, %rdi        # imm = 0xFF4D0B80
	xorl	%ecx, %ecx
	movl	$2919680, %edx          # imm = 0x2C8D00
	movl	$5990656, %esi          # imm = 0x5B6900
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rbx
	shrq	$16, %rbx
	movl	%ebx, (%r8,%rcx)
	movq	%r11, %rbx
	shrq	$16, %rbx
	movl	%ebx, (%r9,%rcx)
	movq	%rsi, (%r10,%rcx,2)
	movq	%rdx, (%rax,%rcx,2)
	addq	$4, %rcx
	addq	$-22554, %rdx           # imm = 0xA7E6
	addq	$-46802, %rsi           # imm = 0xFFFF492E
	addq	$116130, %r11           # imm = 0x1C5A2
	addq	$91881, %rdi            # imm = 0x166E9
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB0_4
# BB#5:                                 # %build_ycc_rgb_table.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	jinit_merged_upsampler, .Lfunc_end0-jinit_merged_upsampler
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_pass_merged_upsample,@function
start_pass_merged_upsample:             # @start_pass_merged_upsample
	.cfi_startproc
# BB#0:
	movq	592(%rdi), %rax
	movl	$0, 72(%rax)
	movl	132(%rdi), %ecx
	movl	%ecx, 80(%rax)
	retq
.Lfunc_end1:
	.size	start_pass_merged_upsample, .Lfunc_end1-start_pass_merged_upsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	merged_2v_upsample,@function
merged_2v_upsample:                     # @merged_2v_upsample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 64
.Lcfi11:
	.cfi_offset %rbx, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%rdx, %r14
	movq	592(%rdi), %rbx
	cmpl	$0, 72(%rbx)
	je	.LBB2_2
# BB#1:
	leaq	64(%rbx), %rdi
	movl	(%r15), %eax
	leaq	(%r8,%rax,8), %rdx
	movl	76(%rbx), %r9d
	movl	$1, %ebp
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movl	$1, %r8d
	callq	jcopy_sample_rows
	movl	$0, 72(%rbx)
	jmp	.LBB2_6
.LBB2_2:
	movl	64(%rsp), %ebp
	movl	80(%rbx), %eax
	cmpl	$2, %eax
	movl	$2, %ecx
	cmovbl	%eax, %ecx
	movl	(%r15), %eax
	subl	%eax, %ebp
	cmpl	%ebp, %ecx
	cmovbel	%ecx, %ebp
	movq	(%r8,%rax,8), %rcx
	movq	%rcx, (%rsp)
	cmpl	$2, %ebp
	jb	.LBB2_4
# BB#3:
	leal	1(%rax), %eax
	movq	(%r8,%rax,8), %rax
	movq	%rax, 8(%rsp)
	jmp	.LBB2_5
.LBB2_4:
	movq	64(%rbx), %rax
	movq	%rax, 8(%rsp)
	movl	$1, 72(%rbx)
.LBB2_5:
	movl	(%r14), %edx
	movq	%rsp, %rcx
	callq	*24(%rbx)
.LBB2_6:
	addl	%ebp, (%r15)
	subl	%ebp, 80(%rbx)
	cmpl	$0, 72(%rbx)
	jne	.LBB2_8
# BB#7:
	incl	(%r14)
.LBB2_8:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	merged_2v_upsample, .Lfunc_end2-merged_2v_upsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	h2v2_merged_upsample,@function
h2v2_merged_upsample:                   # @h2v2_merged_upsample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	592(%rdi), %rbp
	movq	408(%rdi), %r9
	movq	32(%rbp), %r14
	movq	40(%rbp), %r15
	movq	48(%rbp), %r10
	movq	56(%rbp), %r11
	leal	(%rdx,%rdx), %ebp
	movq	(%rsi), %rbx
	movq	8(%rsi), %r8
	movq	(%rbx,%rbp,8), %r12
	leal	1(%rdx,%rdx), %ebp
	movq	(%rbx,%rbp,8), %rbx
	movl	%edx, %ebp
	movq	(%r8,%rbp,8), %rdx
	movq	16(%rsi), %rsi
	movq	(%rsi,%rbp,8), %rax
	movq	(%rcx), %r8
	movq	8(%rcx), %rsi
	movl	128(%rdi), %ecx
	movl	%ecx, %ebp
	shrl	%ebp
	je	.LBB3_1
# BB#2:                                 # %.lr.ph.preheader
	movq	%rdi, -56(%rsp)         # 8-byte Spill
	leal	-1(%rbp), %ecx
	movq	%rdx, %rdi
	leaq	1(%rdi,%rcx), %rdx
	movq	%rdx, -64(%rsp)         # 8-byte Spill
	leaq	2(%rcx,%rcx), %rdx
	movq	%rdx, -80(%rsp)         # 8-byte Spill
	movq	%rsi, -112(%rsp)        # 8-byte Spill
	leaq	(%rcx,%rcx,2), %rsi
	leaq	6(%rsi,%rsi), %rdx
	movq	%rdx, -72(%rsp)         # 8-byte Spill
	leaq	2(%rbx,%rcx,2), %rdx
	movq	%rdx, -40(%rsp)         # 8-byte Spill
	movq	%rdi, %rdx
	incq	%rcx
	movq	%rcx, -88(%rsp)         # 8-byte Spill
	negl	%ebp
	movq	%rbp, -104(%rsp)        # 8-byte Spill
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movq	%rax, %r13
	movq	%r12, -32(%rsp)         # 8-byte Spill
	movq	-112(%rsp), %rcx        # 8-byte Reload
	movq	%r8, -24(%rsp)          # 8-byte Spill
	movq	%r8, %rsi
	movq	%r14, -96(%rsp)         # 8-byte Spill
	movq	%r10, -16(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, -8(%rsp)          # 8-byte Spill
	movzbl	(%r13), %edi
	movzbl	(%rdx), %ebp
	movq	(%r10,%rdi,8), %rdx
	addq	(%r11,%rbp,8), %rdx
	shrq	$16, %rdx
	movq	%r11, %r10
	movzbl	(%r12), %r11d
	movq	-96(%rsp), %rax         # 8-byte Reload
	movslq	(%rax,%rdi,4), %r14
	leaq	(%r11,%r14), %rdi
	movzbl	(%r9,%rdi), %eax
	movq	%r15, %rdi
	movslq	(%rdi,%rbp,4), %r15
	movb	%al, (%rsi)
	movslq	%edx, %r8
	leaq	(%r11,%r8), %rax
	movzbl	(%r9,%rax), %eax
	movb	%al, 1(%rsi)
	addq	%r15, %r11
	movzbl	(%r9,%r11), %eax
	movq	%r10, %r11
	movq	-16(%rsp), %r10         # 8-byte Reload
	movb	%al, 2(%rsi)
	movzbl	1(%r12), %eax
	leaq	(%rax,%r14), %rdx
	movzbl	(%r9,%rdx), %edx
	movb	%dl, 3(%rsi)
	leaq	(%rax,%r8), %rdx
	movzbl	(%r9,%rdx), %edx
	movb	%dl, 4(%rsi)
	addq	%r15, %rax
	movzbl	(%r9,%rax), %eax
	movb	%al, 5(%rsi)
	movzbl	(%rbx), %eax
	leaq	(%rax,%r14), %rdx
	movzbl	(%r9,%rdx), %edx
	movb	%dl, (%rcx)
	leaq	(%rax,%r8), %rdx
	movzbl	(%r9,%rdx), %edx
	movb	%dl, 1(%rcx)
	addq	%r15, %rax
	movzbl	(%r9,%rax), %eax
	movb	%al, 2(%rcx)
	movzbl	1(%rbx), %eax
	addq	%rax, %r14
	addq	%rax, %r8
	addq	%r15, %rax
	movq	%rdi, %r15
	movzbl	(%r9,%r14), %edx
	movq	-96(%rsp), %r14         # 8-byte Reload
	movb	%dl, 3(%rcx)
	movzbl	(%r9,%r8), %edx
	movb	%dl, 4(%rcx)
	movq	-8(%rsp), %rdx          # 8-byte Reload
	movzbl	(%r9,%rax), %eax
	movb	%al, 5(%rcx)
	incq	%r13
	incq	%rdx
	addq	$2, %r12
	addq	$6, %rsi
	addq	$2, %rbx
	addq	$6, %rcx
	movq	-104(%rsp), %rax        # 8-byte Reload
	incl	%eax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	jne	.LBB3_3
# BB#4:                                 # %._crit_edge.loopexit
	movq	-48(%rsp), %rax         # 8-byte Reload
	addq	-88(%rsp), %rax         # 8-byte Folded Reload
	movq	-32(%rsp), %r12         # 8-byte Reload
	addq	-80(%rsp), %r12         # 8-byte Folded Reload
	movq	-24(%rsp), %r8          # 8-byte Reload
	movq	-72(%rsp), %rcx         # 8-byte Reload
	addq	%rcx, %r8
	movq	-112(%rsp), %rbp        # 8-byte Reload
	addq	%rcx, %rbp
	movq	-56(%rsp), %rcx         # 8-byte Reload
	movl	128(%rcx), %ecx
	movq	-40(%rsp), %r13         # 8-byte Reload
	movq	-64(%rsp), %rdx         # 8-byte Reload
	testb	$1, %cl
	jne	.LBB3_6
	jmp	.LBB3_7
.LBB3_1:
	movq	%rbx, %r13
	movq	%rsi, %rbp
	testb	$1, %cl
	je	.LBB3_7
.LBB3_6:
	movzbl	(%rax), %ebx
	movzbl	(%rdx), %ecx
	movq	(%r10,%rbx,8), %rdx
	addq	(%r11,%rcx,8), %rdx
	shrq	$16, %rdx
	movzbl	(%r12), %esi
	movslq	(%r14,%rbx,4), %rax
	leaq	(%rsi,%rax), %rdi
	movb	(%r9,%rdi), %bl
	movslq	(%r15,%rcx,4), %rcx
	movb	%bl, (%r8)
	movslq	%edx, %rdx
	leaq	(%rsi,%rdx), %rdi
	movb	(%r9,%rdi), %bl
	movb	%bl, 1(%r8)
	addq	%rcx, %rsi
	movb	(%r9,%rsi), %bl
	movb	%bl, 2(%r8)
	movzbl	(%r13), %esi
	addq	%rsi, %rax
	movb	(%r9,%rax), %al
	movb	%al, (%rbp)
	addq	%rsi, %rdx
	movb	(%r9,%rdx), %al
	movb	%al, 1(%rbp)
	addq	%rcx, %rsi
	movb	(%r9,%rsi), %al
	movb	%al, 2(%rbp)
.LBB3_7:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	h2v2_merged_upsample, .Lfunc_end3-h2v2_merged_upsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	merged_1v_upsample,@function
merged_1v_upsample:                     # @merged_1v_upsample
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -24
.Lcfi31:
	.cfi_offset %r14, -16
	movq	%r9, %r14
	movq	%rdx, %rbx
	movq	592(%rdi), %rax
	movl	(%rbx), %edx
	movl	(%r14), %ecx
	leaq	(%r8,%rcx,8), %rcx
	callq	*24(%rax)
	incl	(%r14)
	incl	(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	merged_1v_upsample, .Lfunc_end4-merged_1v_upsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	h2v1_merged_upsample,@function
h2v1_merged_upsample:                   # @h2v1_merged_upsample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	592(%rdi), %rbp
	movq	408(%rdi), %r9
	movq	32(%rbp), %r8
	movq	40(%rbp), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	movq	48(%rbp), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	movq	56(%rbp), %rbx
	movl	%edx, %edx
	movq	(%rsi), %rax
	movq	8(%rsi), %rbp
	movq	(%rax,%rdx,8), %r15
	movq	(%rbp,%rdx,8), %r11
	movq	16(%rsi), %rsi
	movq	(%rsi,%rdx,8), %rdx
	movq	(%rcx), %rbp
	movl	128(%rdi), %ecx
	movl	%ecx, %r10d
	shrl	%r10d
	je	.LBB5_4
# BB#1:                                 # %.lr.ph.preheader
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	leal	-1(%r10), %ecx
	leaq	1(%r11,%rcx), %rsi
	movq	%rsi, -56(%rsp)         # 8-byte Spill
	leaq	2(%rcx,%rcx), %rsi
	movq	%rsi, -64(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %rsi
	leaq	1(%rcx), %rcx
	movq	%rcx, -80(%rsp)         # 8-byte Spill
	leaq	6(%rsi,%rsi), %rcx
	movq	%rcx, -72(%rsp)         # 8-byte Spill
	negl	%r10d
	movq	%rdx, -40(%rsp)         # 8-byte Spill
	movq	%rdx, %rsi
	movq	%r15, -32(%rsp)         # 8-byte Spill
	movq	%rbp, -24(%rsp)         # 8-byte Spill
	movq	%rbp, %rcx
	movq	%r8, -8(%rsp)           # 8-byte Spill
	movq	%rbx, -16(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %ebp
	incq	%rsi
	movzbl	(%r11), %r12d
	incq	%r11
	movq	-96(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbp,8), %r14
	addq	(%rbx,%r12,8), %r14
	shrq	$16, %r14
	movzbl	(%r15), %r13d
	movslq	(%r8,%rbp,4), %rbp
	leaq	(%r13,%rbp), %r8
	movzbl	(%r9,%r8), %eax
	movq	-88(%rsp), %rdx         # 8-byte Reload
	movslq	(%rdx,%r12,4), %rdi
	movb	%al, (%rcx)
	movslq	%r14d, %rax
	leaq	(%r13,%rax), %rdx
	movzbl	(%r9,%rdx), %edx
	movb	%dl, 1(%rcx)
	addq	%rdi, %r13
	movzbl	(%r9,%r13), %edx
	movb	%dl, 2(%rcx)
	movzbl	1(%r15), %edx
	addq	%rdx, %rbp
	movzbl	(%r9,%rbp), %ebx
	movb	%bl, 3(%rcx)
	addq	%rdx, %rax
	movzbl	(%r9,%rax), %eax
	movb	%al, 4(%rcx)
	addq	%rdi, %rdx
	movq	-16(%rsp), %rbx         # 8-byte Reload
	movq	-8(%rsp), %r8           # 8-byte Reload
	movzbl	(%r9,%rdx), %eax
	movb	%al, 5(%rcx)
	addq	$2, %r15
	addq	$6, %rcx
	incl	%r10d
	jne	.LBB5_2
# BB#3:                                 # %._crit_edge.loopexit
	movq	-40(%rsp), %rdx         # 8-byte Reload
	addq	-80(%rsp), %rdx         # 8-byte Folded Reload
	movq	-32(%rsp), %r15         # 8-byte Reload
	addq	-64(%rsp), %r15         # 8-byte Folded Reload
	movq	-24(%rsp), %rbp         # 8-byte Reload
	addq	-72(%rsp), %rbp         # 8-byte Folded Reload
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movl	128(%rcx), %ecx
	movq	-56(%rsp), %r11         # 8-byte Reload
.LBB5_4:                                # %._crit_edge
	testb	$1, %cl
	je	.LBB5_6
# BB#5:
	movzbl	(%rdx), %ecx
	movzbl	(%r11), %r10d
	movq	-96(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%rcx,8), %rdx
	addq	(%rbx,%r10,8), %rdx
	shrq	$16, %rdx
	movzbl	(%r15), %esi
	movslq	(%r8,%rcx,4), %rax
	addq	%rsi, %rax
	movb	(%r9,%rax), %al
	movq	-88(%rsp), %rdi         # 8-byte Reload
	movslq	(%rdi,%r10,4), %rcx
	movb	%al, (%rbp)
	movslq	%edx, %rax
	addq	%rsi, %rax
	movb	(%r9,%rax), %al
	movb	%al, 1(%rbp)
	addq	%rsi, %rcx
	movb	(%r9,%rcx), %al
	movb	%al, 2(%rbp)
.LBB5_6:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	h2v1_merged_upsample, .Lfunc_end5-h2v1_merged_upsample
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
