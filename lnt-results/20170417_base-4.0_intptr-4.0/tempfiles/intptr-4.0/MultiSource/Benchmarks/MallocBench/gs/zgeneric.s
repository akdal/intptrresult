	.text
	.file	"zgeneric.bc"
	.globl	zcopy
	.p2align	4, 0x90
	.type	zcopy,@function
zcopy:                                  # @zcopy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movzwl	8(%r15), %edx
	movl	%edx, %ecx
	shrl	$2, %ecx
	movl	%ecx, %ebx
	andb	$63, %bl
	movl	$-20, %eax
	cmpb	$13, %bl
	ja	.LBB0_17
# BB#1:
	movl	%ecx, %esi
	andl	$63, %esi
	jmpq	*.LJTI0_0(,%rsi,8)
.LBB0_6:
	movzwl	-8(%r15), %ebx
	movl	%edx, %esi
	xorl	%ebx, %esi
	shrl	$2, %esi
	testb	$63, %sil
	jne	.LBB0_17
# BB#7:
	testb	$2, %bh
	movl	$-7, %eax
	je	.LBB0_17
# BB#8:
	andl	$256, %edx              # imm = 0x100
	je	.LBB0_17
# BB#9:
	movzwl	-6(%r15), %edx
	movl	$-15, %eax
	cmpw	10(%r15), %dx
	ja	.LBB0_17
# BB#10:
	leaq	-16(%r15), %r14
	andb	$63, %cl
	je	.LBB0_13
# BB#11:
	cmpb	$13, %cl
	je	.LBB0_15
# BB#12:
	cmpb	$10, %cl
	jne	.LBB0_16
.LBB0_13:
	movq	-16(%r15), %rsi
	movq	(%r15), %rdi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	refcpy
	jmp	.LBB0_16
.LBB0_14:
	xorl	%eax, %eax
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	zcopy_dict              # TAILCALL
.LBB0_2:
	movq	(%r15), %rcx
	movq	%r15, %rdx
	subq	osbot(%rip), %rdx
	sarq	$4, %rdx
	movl	$-15, %eax
	cmpq	%rdx, %rcx
	ja	.LBB0_17
# BB#3:
	movq	%rcx, %r14
	shlq	$32, %r14
	movq	%r14, %rdx
	sarq	$28, %rdx
	leaq	-16(%r15,%rdx), %rsi
	movl	$-16, %eax
	cmpq	ostop(%rip), %rsi
	ja	.LBB0_17
# BB#4:
	movslq	%ecx, %rax
	shlq	$4, %rax
	movq	%r15, %rsi
	subq	%rax, %rsi
	movq	%r15, %rdi
	callq	memcpy
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	addq	%rax, %r14
	movq	%r14, %rcx
	sarq	$28, %rcx
	addq	%r15, %rcx
	movq	%rcx, osp(%rip)
	xorl	%eax, %eax
	cmpq	ostop(%rip), %rcx
	jbe	.LBB0_17
# BB#5:
	sarq	$32, %r14
	shlq	$4, %r14
	subq	%r14, %rcx
	movq	%rcx, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB0_17
.LBB0_15:
	movq	-16(%r15), %rsi
	movq	(%r15), %rdi
	callq	memcpy
.LBB0_16:
	movzwl	-6(%r15), %eax
	movw	%ax, 10(%r15)
	movups	(%r15), %xmm0
	movups	%xmm0, (%r14)
	orb	$-128, -7(%r15)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB0_17:                               # %copy_interval.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	zcopy, .Lfunc_end0-zcopy
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_6
	.quad	.LBB0_17
	.quad	.LBB0_14
	.quad	.LBB0_17
	.quad	.LBB0_17
	.quad	.LBB0_2
	.quad	.LBB0_17
	.quad	.LBB0_17
	.quad	.LBB0_17
	.quad	.LBB0_17
	.quad	.LBB0_6
	.quad	.LBB0_17
	.quad	.LBB0_17
	.quad	.LBB0_6

	.text
	.globl	copy_interval
	.p2align	4, 0x90
	.type	copy_interval,@function
copy_interval:                          # @copy_interval
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movzwl	8(%rdx), %ecx
	movzwl	8(%rdi), %ebx
	movl	%ebx, %eax
	xorl	%ecx, %eax
	shrl	$2, %eax
	movl	$-20, %ebp
	testb	$63, %al
	je	.LBB1_1
.LBB1_9:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB1_1:
	testb	$2, %ch
	movl	$-7, %ebp
	je	.LBB1_9
# BB#2:
	movl	%ebx, %eax
	andl	$256, %eax              # imm = 0x100
	je	.LBB1_9
# BB#3:
	movzwl	10(%rdx), %r8d
	movzwl	10(%rdi), %eax
	subl	%esi, %eax
	movl	$-15, %ebp
	cmpl	%eax, %r8d
	ja	.LBB1_9
# BB#4:
	shrl	$2, %ebx
	andb	$63, %bl
	je	.LBB1_7
# BB#5:
	xorl	%ebp, %ebp
	cmpb	$13, %bl
	je	.LBB1_8
# BB#6:
	cmpb	$10, %bl
	jne	.LBB1_9
.LBB1_7:
	movl	%esi, %ecx
	shlq	$4, %rcx
	addq	(%rdi), %rcx
	movq	(%rdx), %rsi
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r8d, %edx
	callq	refcpy
	jmp	.LBB1_9
.LBB1_8:
	movl	%esi, %eax
	addq	(%rdi), %rax
	movq	(%rdx), %rsi
	movq	%rax, %rdi
	movq	%r8, %rdx
	callq	memcpy
	jmp	.LBB1_9
.Lfunc_end1:
	.size	copy_interval, .Lfunc_end1-copy_interval
	.cfi_endproc

	.globl	zlength
	.p2align	4, 0x90
	.type	zlength,@function
zlength:                                # @zlength
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %eax
	movl	%eax, %ecx
	shrl	$2, %ecx
	movl	%ecx, %edx
	andb	$63, %dl
	movl	$-20, %ebp
	cmpb	$13, %dl
	ja	.LBB2_8
# BB#1:
	andl	$63, %ecx
	jmpq	*.LJTI2_0(,%rcx,8)
.LBB2_2:
	movl	$-7, %ebp
	testb	$2, %ah
	je	.LBB2_8
# BB#3:
	movzwl	10(%rbx), %eax
	jmp	.LBB2_4
.LBB2_5:
	movq	%rbx, %rdi
	callq	dict_access_ref
	movl	$-7, %ebp
	testb	$2, 9(%rax)
	je	.LBB2_8
# BB#6:
	movq	%rbx, %rdi
	callq	dict_length
	movl	%eax, %eax
.LBB2_4:
	movq	%rax, (%rbx)
	movw	$20, 8(%rbx)
	xorl	%ebp, %ebp
	jmp	.LBB2_8
.LBB2_7:
	xorl	%ebp, %ebp
	leaq	8(%rsp), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	name_string_ref
	movzwl	18(%rsp), %eax
	movq	%rax, (%rbx)
	movw	$20, 8(%rbx)
.LBB2_8:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end2:
	.size	zlength, .Lfunc_end2-zlength
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_2
	.quad	.LBB2_8
	.quad	.LBB2_5
	.quad	.LBB2_8
	.quad	.LBB2_8
	.quad	.LBB2_8
	.quad	.LBB2_8
	.quad	.LBB2_7
	.quad	.LBB2_8
	.quad	.LBB2_8
	.quad	.LBB2_2
	.quad	.LBB2_8
	.quad	.LBB2_8
	.quad	.LBB2_2

	.text
	.globl	zget
	.p2align	4, 0x90
	.type	zget,@function
zget:                                   # @zget
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movzwl	-8(%rbx), %eax
	movl	%eax, %edx
	shrl	$2, %edx
	movl	%edx, %ecx
	andb	$63, %cl
	movl	$-20, %esi
	cmpb	$13, %cl
	ja	.LBB3_16
# BB#1:
	leaq	-16(%rbx), %r14
	andl	$63, %edx
	jmpq	*.LJTI3_0(,%rdx,8)
.LBB3_5:
	movzwl	8(%rbx), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB3_16
# BB#6:
	movl	$-7, %esi
	testb	$2, %ah
	je	.LBB3_16
# BB#7:
	movq	(%rbx), %rcx
	movzwl	-6(%rbx), %eax
	movl	$-15, %esi
	cmpq	%rax, %rcx
	jae	.LBB3_16
# BB#8:
	movl	%ecx, %eax
	shlq	$4, %rax
	addq	(%r14), %rax
	movq	%rax, (%rsp)
	jmp	.LBB3_14
.LBB3_2:
	movq	%r14, %rdi
	callq	dict_access_ref
	movq	%rax, %rcx
	movl	$-7, %esi
	testb	$2, 9(%rcx)
	je	.LBB3_16
# BB#3:
	movq	%rsp, %rcx
	movq	%r14, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB3_4
# BB#13:                                # %._crit_edge
	movq	(%rsp), %rax
.LBB3_14:
	movups	(%rax), %xmm0
	movups	%xmm0, (%r14)
.LBB3_15:
	addq	$-16, osp(%rip)
	xorl	%esi, %esi
	jmp	.LBB3_16
.LBB3_9:
	movzwl	8(%rbx), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB3_16
# BB#10:
	movl	$-7, %esi
	testb	$2, %ah
	je	.LBB3_16
# BB#11:
	movq	(%rbx), %rcx
	movzwl	-6(%rbx), %eax
	movl	$-15, %esi
	cmpq	%rax, %rcx
	jae	.LBB3_16
# BB#12:
	movq	-16(%rbx), %rax
	movl	%ecx, %ecx
	movzbl	(%rax,%rcx), %eax
	movq	%rax, -16(%rbx)
	movw	$20, -8(%rbx)
	jmp	.LBB3_15
.LBB3_4:
	movl	$-21, %esi
.LBB3_16:
	movl	%esi, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	zget, .Lfunc_end3-zget
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_5
	.quad	.LBB3_16
	.quad	.LBB3_2
	.quad	.LBB3_16
	.quad	.LBB3_16
	.quad	.LBB3_16
	.quad	.LBB3_16
	.quad	.LBB3_16
	.quad	.LBB3_16
	.quad	.LBB3_16
	.quad	.LBB3_5
	.quad	.LBB3_16
	.quad	.LBB3_16
	.quad	.LBB3_9

	.text
	.globl	zput
	.p2align	4, 0x90
	.type	zput,@function
zput:                                   # @zput
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movzwl	-24(%rbx), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	movl	%edx, %eax
	andb	$63, %al
	cmpb	$13, %al
	ja	.LBB4_18
# BB#1:
	leaq	-16(%rbx), %r15
	leaq	-32(%rbx), %r14
	andl	$63, %edx
	movl	$-7, %eax
	jmpq	*.LJTI4_0(,%rdx,8)
.LBB4_4:
	movzwl	-8(%rbx), %edx
	andl	$252, %edx
	movl	$-20, %eax
	cmpl	$20, %edx
	jne	.LBB4_17
# BB#5:
	testb	$1, %ch
	movl	$-7, %eax
	je	.LBB4_17
# BB#6:
	movq	(%r15), %rcx
	movl	$-15, %eax
	testq	%rcx, %rcx
	js	.LBB4_17
# BB#7:
	movzwl	-22(%rbx), %edx
	cmpq	%rdx, %rcx
	jge	.LBB4_17
# BB#8:
	movq	-32(%rbx), %rax
	movl	%ecx, %ecx
	shlq	$4, %rcx
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rax,%rcx)
	jmp	.LBB4_16
.LBB4_18:
	movl	$-20, %eax
	jmp	.LBB4_17
.LBB4_2:
	movq	%r14, %rdi
	callq	dict_access_ref
	testb	$1, 9(%rax)
	movl	$-7, %eax
	je	.LBB4_17
# BB#3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	dict_put
	testl	%eax, %eax
	jne	.LBB4_17
.LBB4_16:
	addq	$-48, osp(%rip)
	xorl	%eax, %eax
	jmp	.LBB4_17
.LBB4_9:
	movzwl	-8(%rbx), %edx
	andl	$252, %edx
	movl	$-20, %eax
	cmpl	$20, %edx
	jne	.LBB4_17
# BB#10:
	testb	$1, %ch
	movl	$-7, %eax
	je	.LBB4_17
# BB#11:
	movq	(%r15), %rcx
	movl	$-15, %eax
	testq	%rcx, %rcx
	js	.LBB4_17
# BB#12:
	movzwl	-22(%rbx), %edx
	cmpq	%rdx, %rcx
	jge	.LBB4_17
# BB#13:
	movzwl	8(%rbx), %eax
	andl	$252, %eax
	cmpl	$20, %eax
	movl	$-20, %eax
	jne	.LBB4_17
# BB#14:
	movq	(%rbx), %rdx
	cmpq	$255, %rdx
	movl	$-15, %eax
	jbe	.LBB4_15
.LBB4_17:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB4_15:
	movq	(%r14), %rax
	movl	%ecx, %ecx
	movb	%dl, (%rax,%rcx)
	jmp	.LBB4_16
.Lfunc_end4:
	.size	zput, .Lfunc_end4-zput
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_4
	.quad	.LBB4_18
	.quad	.LBB4_2
	.quad	.LBB4_18
	.quad	.LBB4_18
	.quad	.LBB4_18
	.quad	.LBB4_18
	.quad	.LBB4_18
	.quad	.LBB4_18
	.quad	.LBB4_18
	.quad	.LBB4_17
	.quad	.LBB4_18
	.quad	.LBB4_18
	.quad	.LBB4_9

	.text
	.globl	zgetinterval
	.p2align	4, 0x90
	.type	zgetinterval,@function
zgetinterval:                           # @zgetinterval
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 16
.Lcfi28:
	.cfi_offset %rbx, -16
	movzwl	-8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB5_14
# BB#1:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB5_14
# BB#2:
	movzwl	-24(%rdi), %ebx
	movl	%ebx, %edx
	shrl	$2, %edx
	movl	%edx, %ecx
	andb	$63, %cl
	cmpb	$13, %cl
	ja	.LBB5_14
# BB#3:
	movl	%edx, %ecx
	andl	$63, %ecx
	movl	$9217, %esi             # imm = 0x2401
	btq	%rcx, %rsi
	jae	.LBB5_14
# BB#4:
	movl	$-7, %eax
	testb	$2, %bh
	je	.LBB5_14
# BB#5:
	movq	-16(%rdi), %r9
	movzwl	-22(%rdi), %esi
	movl	$-15, %eax
	cmpq	%r9, %rsi
	jb	.LBB5_14
# BB#6:
	movq	(%rdi), %r8
	subl	%r9d, %esi
	cmpq	%rsi, %r8
	ja	.LBB5_14
# BB#7:
	andb	$63, %dl
	je	.LBB5_10
# BB#8:
	cmpb	$13, %dl
	je	.LBB5_11
# BB#9:
	cmpb	$10, %dl
	jne	.LBB5_13
.LBB5_10:
	movl	%r9d, %eax
	shlq	$4, %rax
	jmp	.LBB5_12
.LBB5_11:
	movl	%r9d, %eax
.LBB5_12:
	addq	%rax, -32(%rdi)
.LBB5_13:
	movw	%r8w, -22(%rdi)
	orl	$32768, %ebx            # imm = 0x8000
	movw	%bx, -24(%rdi)
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
.LBB5_14:
	popq	%rbx
	retq
.Lfunc_end5:
	.size	zgetinterval, .Lfunc_end5-zgetinterval
	.cfi_endproc

	.globl	zputinterval
	.p2align	4, 0x90
	.type	zputinterval,@function
zputinterval:                           # @zputinterval
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 16
.Lcfi30:
	.cfi_offset %rbx, -16
	movzwl	-8(%rdi), %eax
	andl	$252, %eax
	movl	$-20, %ebx
	cmpl	$20, %eax
	jne	.LBB6_17
# BB#1:
	movzwl	-24(%rdi), %eax
	movl	%eax, %ecx
	shrb	$2, %cl
	je	.LBB6_5
# BB#2:
	cmpb	$13, %cl
	je	.LBB6_5
# BB#3:
	cmpb	$10, %cl
	jne	.LBB6_17
# BB#4:
	movl	$-7, %ebx
	jmp	.LBB6_17
.LBB6_5:
	movl	$-7, %ebx
	testb	$1, %ah
	je	.LBB6_17
# BB#6:
	movq	-16(%rdi), %r8
	movzwl	-22(%rdi), %esi
	movl	$-15, %ebx
	cmpq	%rsi, %r8
	ja	.LBB6_17
# BB#7:
	movzwl	8(%rdi), %edx
	movl	%eax, %ecx
	xorl	%edx, %ecx
	shrl	$2, %ecx
	xorl	%ebx, %ebx
	testb	$63, %cl
	je	.LBB6_8
.LBB6_17:                               # %copy_interval.exit
	movl	%ebx, %eax
	popq	%rbx
	retq
.LBB6_8:
	testb	$2, %dh
	je	.LBB6_17
# BB#9:
	movl	%eax, %ecx
	andl	$256, %ecx              # imm = 0x100
	je	.LBB6_17
# BB#10:
	movzwl	10(%rdi), %edx
	subl	%r8d, %esi
	cmpl	%esi, %edx
	ja	.LBB6_17
# BB#11:
	shrl	$2, %eax
	andb	$63, %al
	je	.LBB6_14
# BB#12:
	cmpb	$13, %al
	je	.LBB6_15
# BB#13:
	cmpb	$10, %al
	jne	.LBB6_16
.LBB6_14:
	movl	%r8d, %ecx
	shlq	$4, %rcx
	addq	-32(%rdi), %rcx
	movq	(%rdi), %rsi
	xorl	%eax, %eax
	movq	%rcx, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	refcpy
	jmp	.LBB6_16
.LBB6_15:
	movl	%r8d, %eax
	addq	-32(%rdi), %rax
	movq	(%rdi), %rsi
	movq	%rax, %rdi
	callq	memcpy
.LBB6_16:
	addq	$-48, osp(%rip)
	jmp	.LBB6_17
.Lfunc_end6:
	.size	zputinterval, .Lfunc_end6-zputinterval
	.cfi_endproc

	.globl	zforall
	.p2align	4, 0x90
	.type	zforall,@function
zforall:                                # @zforall
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movzwl	-8(%r15), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	movl	%edx, %ebx
	andb	$63, %bl
	movl	$-20, %eax
	cmpb	$13, %bl
	ja	.LBB7_9
# BB#1:
	leaq	-16(%r15), %r14
	andl	$63, %edx
	jmpq	*.LJTI7_0(,%rdx,8)
.LBB7_2:
	movl	$-7, %eax
	testb	$2, %ch
	je	.LBB7_9
# BB#3:
	movl	$array_continue, %ecx
	xorl	%edx, %edx
	jmp	.LBB7_8
.LBB7_4:
	movl	$-7, %eax
	testb	$2, %ch
	je	.LBB7_9
# BB#5:
	movl	$string_continue, %ecx
	xorl	%edx, %edx
	jmp	.LBB7_8
.LBB7_6:
	movq	%r14, %rdi
	callq	dict_access_ref
	movq	%rax, %rcx
	movl	$-7, %eax
	testb	$2, 9(%rcx)
	je	.LBB7_9
# BB#7:
	movq	%r14, %rdi
	callq	dict_first
	movl	$dict_continue, %ecx
	movl	%eax, %edx
.LBB7_8:
	movq	esp(%rip), %rsi
	leaq	96(%rsi), %rdi
	movl	$-5, %eax
	cmpq	estop(%rip), %rdi
	jbe	.LBB7_10
.LBB7_9:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB7_10:
	movw	$2, 16(%rsi)
	movw	$33, 24(%rsi)
	leaq	32(%rsi), %rax
	movq	%rax, esp(%rip)
	movups	(%r14), %xmm0
	movups	%xmm0, 32(%rsi)
	movq	%rdx, 48(%rsi)
	movw	$20, 56(%rsi)
	leaq	64(%rsi), %rax
	movq	%rax, esp(%rip)
	movups	(%r15), %xmm0
	movups	%xmm0, 64(%rsi)
	addq	$-32, osp(%rip)
	addq	$-32, %r15
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rcx                   # TAILCALL
.Lfunc_end7:
	.size	zforall, .Lfunc_end7-zforall
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_2
	.quad	.LBB7_9
	.quad	.LBB7_6
	.quad	.LBB7_9
	.quad	.LBB7_9
	.quad	.LBB7_9
	.quad	.LBB7_9
	.quad	.LBB7_9
	.quad	.LBB7_9
	.quad	.LBB7_9
	.quad	.LBB7_2
	.quad	.LBB7_9
	.quad	.LBB7_9
	.quad	.LBB7_4

	.text
	.globl	array_continue
	.p2align	4, 0x90
	.type	array_continue,@function
array_continue:                         # @array_continue
	.cfi_startproc
# BB#0:
	movq	esp(%rip), %rax
	movzwl	-22(%rax), %ecx
	movl	%ecx, %edx
	decl	%edx
	movw	%dx, -22(%rax)
	testw	%cx, %cx
	je	.LBB8_4
# BB#1:
	leaq	16(%rdi), %rcx
	movq	%rcx, osp(%rip)
	cmpq	ostop(%rip), %rcx
	jbe	.LBB8_3
# BB#2:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB8_4:
	addq	$-64, %rax
	movq	%rax, esp(%rip)
	movl	$1, %eax
	retq
.LBB8_3:
	movq	-32(%rax), %rdx
	movups	(%rdx), %xmm0
	movups	%xmm0, (%rcx)
	addq	$16, -32(%rax)
	movq	esp(%rip), %rcx
	movq	$array_continue, 16(%rcx)
	movw	$37, 24(%rcx)
	movw	$0, 26(%rcx)
	leaq	32(%rcx), %rdx
	movq	%rdx, esp(%rip)
	movups	(%rax), %xmm0
	movups	%xmm0, 32(%rcx)
	movl	$1, %eax
	retq
.Lfunc_end8:
	.size	array_continue, .Lfunc_end8-array_continue
	.cfi_endproc

	.globl	string_continue
	.p2align	4, 0x90
	.type	string_continue,@function
string_continue:                        # @string_continue
	.cfi_startproc
# BB#0:
	movq	esp(%rip), %rax
	movzwl	-22(%rax), %ecx
	movl	%ecx, %edx
	decl	%edx
	movw	%dx, -22(%rax)
	testw	%cx, %cx
	je	.LBB9_4
# BB#1:
	leaq	16(%rdi), %rcx
	movq	%rcx, osp(%rip)
	cmpq	ostop(%rip), %rcx
	jbe	.LBB9_3
# BB#2:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB9_4:
	addq	$-64, %rax
	movq	%rax, esp(%rip)
	movl	$1, %eax
	retq
.LBB9_3:
	movq	-32(%rax), %rcx
	movzbl	(%rcx), %edx
	movq	%rdx, 16(%rdi)
	movw	$20, 24(%rdi)
	incq	%rcx
	movq	%rcx, -32(%rax)
	movq	esp(%rip), %rcx
	movq	$string_continue, 16(%rcx)
	movw	$37, 24(%rcx)
	movw	$0, 26(%rcx)
	leaq	32(%rcx), %rdx
	movq	%rdx, esp(%rip)
	movups	(%rax), %xmm0
	movups	%xmm0, 32(%rcx)
	movl	$1, %eax
	retq
.Lfunc_end9:
	.size	string_continue, .Lfunc_end9-string_continue
	.cfi_endproc

	.globl	dict_continue
	.p2align	4, 0x90
	.type	dict_continue,@function
dict_continue:                          # @dict_continue
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 16
.Lcfi38:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	movq	esp(%rip), %rbx
	movq	-16(%rbx), %rsi
	leaq	32(%rax), %rcx
	movq	%rcx, osp(%rip)
	cmpq	ostop(%rip), %rcx
	jbe	.LBB10_2
# BB#1:
	movq	%rax, osp(%rip)
	movl	$-16, %eax
	popq	%rbx
	retq
.LBB10_2:
	leaq	-32(%rbx), %rdi
	addq	$16, %rax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%rax, %rdx
	callq	dict_next
	testl	%eax, %eax
	js	.LBB10_4
# BB#3:
	cltq
	movq	esp(%rip), %rcx
	movq	%rax, -16(%rcx)
	movq	$dict_continue, 16(%rcx)
	movw	$37, 24(%rcx)
	movw	$0, 26(%rcx)
	leaq	32(%rcx), %rax
	movq	%rax, esp(%rip)
	movups	(%rbx), %xmm0
	movups	%xmm0, 32(%rcx)
	jmp	.LBB10_5
.LBB10_4:
	addq	$-32, osp(%rip)
	addq	$-64, esp(%rip)
.LBB10_5:
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end10:
	.size	dict_continue, .Lfunc_end10-dict_continue
	.cfi_endproc

	.globl	zgeneric_op_init
	.p2align	4, 0x90
	.type	zgeneric_op_init,@function
zgeneric_op_init:                       # @zgeneric_op_init
	.cfi_startproc
# BB#0:
	movl	$zgeneric_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end11:
	.size	zgeneric_op_init, .Lfunc_end11-zgeneric_op_init
	.cfi_endproc

	.type	zgeneric_op_init.my_defs,@object # @zgeneric_op_init.my_defs
	.data
	.p2align	4
zgeneric_op_init.my_defs:
	.quad	.L.str
	.quad	zcopy
	.quad	.L.str.1
	.quad	zforall
	.quad	.L.str.2
	.quad	zget
	.quad	.L.str.3
	.quad	zgetinterval
	.quad	.L.str.4
	.quad	zlength
	.quad	.L.str.5
	.quad	zput
	.quad	.L.str.6
	.quad	zputinterval
	.zero	16
	.size	zgeneric_op_init.my_defs, 128

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"1copy"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"2forall"
	.size	.L.str.1, 8

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"2get"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"3getinterval"
	.size	.L.str.3, 13

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"1length"
	.size	.L.str.4, 8

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"3put"
	.size	.L.str.5, 5

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"3putinterval"
	.size	.L.str.6, 13


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
