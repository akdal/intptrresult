	.text
	.file	"class.bc"
	.globl	_ZN9ClassfileC2EiPPc
	.p2align	4, 0x90
	.type	_ZN9ClassfileC2EiPPc,@function
_ZN9ClassfileC2EiPPc:                   # @_ZN9ClassfileC2EiPPc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %ebx
	movq	%rdi, %r14
	movw	$0, 48(%r14)
	movq	$0, 160(%r14)
	movq	stdout(%rip), %rax
	movq	%rax, 8(%r14)
	movq	stdin(%rip), %rax
	movq	%rax, (%r14)
	movq	(%r15), %r12
	movq	%r12, progname(%rip)
	movl	$0, 24(%r14)
	movq	%r12, %rdi
	callq	strlen
	leaq	-3(%r12,%rax), %rdi
	movl	$.L.str, %esi
	callq	strcmp
	leal	-1(%rbx), %ecx
	testl	%eax, %eax
	je	.LBB0_9
# BB#1:                                 # %.preheader36
	testl	%ecx, %ecx
	je	.LBB0_41
# BB#2:                                 # %.lr.ph46.preheader
	addq	$16, %r15
	jmp	.LBB0_3
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$0, 24(%r14)
	movsbl	1(%rdi), %ebp
	movl	%ebp, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebp, %rcx
	movl	(%rax,%rcx,4), %ebp
.LBB0_6:                                # %toupper.exit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	$68, %ebp
	je	.LBB0_31
# BB#7:                                 # %toupper.exit
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	$79, %ebp
	jne	.LBB0_32
# BB#8:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	$2, 24(%r14)
	jmp	.LBB0_33
	.p2align	4, 0x90
.LBB0_31:                               #   in Loop: Header=BB0_3 Depth=1
	movl	$1, 24(%r14)
.LBB0_33:                               #   in Loop: Header=BB0_3 Depth=1
	decl	%ebx
	addq	$8, %r15
	cmpl	$1, %ebx
	jne	.LBB0_3
	jmp	.LBB0_41
.LBB0_32:                               #   in Loop: Header=BB0_3 Depth=1
	movq	stderr(%rip), %rdi
	movq	(%r15), %rdx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	progname(%rip), %rsi
	movl	$2, %edi
	xorl	%eax, %eax
	callq	_Z10fatalerroriz
	jmp	.LBB0_33
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph46
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%r15), %rdi
	cmpb	$45, (%rdi)
	je	.LBB0_4
# BB#34:                                # %.critedge1
	cmpl	$2, %ebx
	je	.LBB0_38
# BB#35:                                # %.critedge1
	cmpl	$3, %ebx
	jne	.LBB0_41
# BB#36:
	movq	(%r15), %rdi
	movl	$.L.str.5, %esi
	callq	fopen
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.LBB0_43
# BB#37:                                # %._crit_edge
	movq	-8(%r15), %rdi
.LBB0_38:
	movl	$.L.str.3, %esi
	callq	fopen
	movq	%rax, (%r14)
	testq	%rax, %rax
	jne	.LBB0_42
# BB#39:
	movq	stderr(%rip), %rdi
	movq	-8(%r15), %rdx
	jmp	.LBB0_40
.LBB0_9:                                # %.preheader
	testl	%ecx, %ecx
	je	.LBB0_20
# BB#10:                                # %.lr.ph.preheader
	addq	$8, %r15
	movl	$2, %r12d
	subl	%ebx, %r12d
	jmp	.LBB0_11
	.p2align	4, 0x90
.LBB0_12:                               #   in Loop: Header=BB0_11 Depth=1
	movl	$0, 24(%r14)
	movsbl	1(%rbx), %ebp
	movl	%ebp, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_11 Depth=1
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movslq	%ebp, %rcx
	movl	(%rax,%rcx,4), %ebp
.LBB0_14:                               # %toupper.exit35
                                        #   in Loop: Header=BB0_11 Depth=1
	cmpl	$73, %ebp
	je	.LBB0_17
# BB#15:                                # %toupper.exit35
                                        #   in Loop: Header=BB0_11 Depth=1
	cmpl	$68, %ebp
	jne	.LBB0_18
# BB#16:                                #   in Loop: Header=BB0_11 Depth=1
	movl	$1, 24(%r14)
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_17:                               #   in Loop: Header=BB0_11 Depth=1
	addq	$2, %rbx
	movq	%rbx, 160(%r14)
.LBB0_19:                               #   in Loop: Header=BB0_11 Depth=1
	addq	$8, %r15
	incl	%r12d
	cmpl	$1, %r12d
	jne	.LBB0_11
	jmp	.LBB0_20
.LBB0_18:                               #   in Loop: Header=BB0_11 Depth=1
	movq	stderr(%rip), %rdi
	movq	8(%r15), %rdx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	progname(%rip), %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	callq	_Z10fatalerroriz
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rbx
	cmpb	$45, (%rbx)
	je	.LBB0_12
# BB#21:                                # %.critedge
	testl	%r12d, %r12d
	je	.LBB0_22
.LBB0_20:                               # %.loopexit
	movq	progname(%rip), %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	callq	_Z10fatalerroriz
	cmpq	$0, 160(%r14)
	jne	.LBB0_30
.LBB0_29:
	movq	progname(%rip), %rsi
	movl	$3, %edi
	xorl	%eax, %eax
	callq	_Z10fatalerroriz
.LBB0_30:
	movl	$0, 20(%r14)
	jmp	.LBB0_42
.LBB0_22:
	movq	%rbx, %rdi
	callq	strlen
	leaq	5(%rax), %rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	(%r15), %rsi
	movq	%rbx, %rdi
	callq	strcpy
	movq	%rbx, %rdi
	callq	strlen
	movb	$0, 4(%rbx,%rax)
	movl	$1801544238, (%rbx,%rax) # imm = 0x6B61622E
	movq	(%r15), %rdi
	movq	%rbx, %rsi
	callq	rename
	movl	$.L.str.3, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.LBB0_23
# BB#25:
	movq	(%r15), %rdi
	movl	$.L.str.5, %esi
	callq	fopen
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.LBB0_26
# BB#27:
	movq	%rbx, %rdi
	callq	_ZdaPv
	cmpq	$0, 160(%r14)
	jne	.LBB0_30
	jmp	.LBB0_29
.LBB0_43:
	movq	stderr(%rip), %rdi
	movq	(%r15), %rdx
.LBB0_40:                               # %.critedge1.thread
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB0_41:                               # %.critedge1.thread
	movq	progname(%rip), %rsi
	movl	$2, %edi
	xorl	%eax, %eax
	callq	_Z10fatalerroriz
.LBB0_42:
	movl	$0, 16(%r14)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_23:
	movq	stderr(%rip), %rdi
	movq	(%r15), %rdx
	jmp	.LBB0_24
.LBB0_26:
	movq	stderr(%rip), %rdi
	movq	8(%r15), %rdx
.LBB0_24:                               # %.loopexit
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB0_20
.Lfunc_end0:
	.size	_ZN9ClassfileC2EiPPc, .Lfunc_end0-_ZN9ClassfileC2EiPPc
	.cfi_endproc

	.globl	_ZN9Classfile4readEv
	.p2align	4, 0x90
	.type	_ZN9Classfile4readEv,@function
_ZN9Classfile4readEv:                   # @_ZN9Classfile4readEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 160
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movabsq	$4294967296, %r15       # imm = 0x100000000
	movq	(%r13), %r14
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movzwl	%bp, %ebx
	orq	%rax, %rbx
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movzwl	%bp, %ecx
	orq	%rax, %rcx
	shlq	$16, %rbx
	orq	%rcx, %rbx
	leaq	-889275714(%r15), %rax
	cmpq	%rax, %rbx
	je	.LBB1_2
# BB#1:
	movl	$4, %edi
	xorl	%eax, %eax
	callq	_Z10fatalerroriz
.LBB1_2:
	leaq	28(%r13), %rdi
	movq	%r13, %rsi
	callq	_ZN12ClassVersion4readEP9Classfile
	movq	stderr(%rip), %rdi
	movzwl	30(%r13), %edx
	movzwl	28(%r13), %ecx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	callq	fprintf
	leaq	144(%r13), %rdx
	movw	$0, 144(%r13)
	leaq	32(%r13), %rdi
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	%r13, %rsi
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	callq	_ZN9ConstPool4readEP9ClassfilePt
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movw	%ax, 48(%r13)
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movw	%ax, 50(%r13)
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movw	%ax, 72(%r13)
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movw	%ax, 88(%r13)
	movzwl	%ax, %r14d
	leaq	(%r14,%r14), %rdi
	callq	_Znam
	movq	%rax, 96(%r13)
	movq	(%r13), %rbp
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movq	%rbp, %rdi
	callq	_IO_getc
	shll	$8, %ebx
	movzbl	%al, %ebp
	orl	%ebx, %ebp
	testw	%r14w, %r14w
	je	.LBB1_5
# BB#3:                                 # %.lr.ph624.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph624
                                        # =>This Inner Loop Header: Depth=1
	movq	96(%r13), %rax
	movw	%bp, (%rax,%rbx,2)
	incq	%rbx
	movq	(%r13), %r15
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	movq	%r15, %rdi
	callq	_IO_getc
	shll	$8, %r12d
	movzbl	%al, %ebp
	orl	%r12d, %ebp
	cmpw	%bx, %r14w
	jne	.LBB1_4
.LBB1_5:                                # %._crit_edge625
	movw	%bp, 104(%r13)
	movzwl	%bp, %r14d
	leaq	(,%r14,8), %rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, 112(%r13)
	testw	%r14w, %r14w
	movq	%r13, 8(%rsp)           # 8-byte Spill
	je	.LBB1_16
# BB#6:                                 # %.lr.ph619
	movl	%ebp, 48(%rsp)          # 4-byte Spill
	xorl	%ebp, %ebp
	jmp	.LBB1_9
	.p2align	4, 0x90
.LBB1_7:                                # %.loopexit509
                                        #   in Loop: Header=BB1_9 Depth=1
	movl	48(%rsp), %eax          # 4-byte Reload
	decl	%eax
	testw	%ax, %ax
	je	.LBB1_16
# BB#8:                                 # %.loopexit509._crit_edge
                                        #   in Loop: Header=BB1_9 Depth=1
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movq	16(%rsp), %rbp          # 8-byte Reload
	incq	%rbp
	movq	112(%r13), %rbx
.LBB1_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_12 Depth 2
                                        #       Child Loop BB1_11 Depth 3
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r14
	movw	$0, (%r14)
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%r14, (%rbx,%rbp,8)
	movl	$0, 24(%r14)
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movw	%ax, (%r14)
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movq	40(%r13), %rcx
	movzwl	%bp, %edx
	orq	%rax, %rdx
	shlq	$4, %rdx
	movq	8(%rcx,%rdx), %rax
	movq	%rax, 8(%r14)
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movq	40(%r13), %rcx
	movzwl	%bp, %edx
	orq	%rax, %rdx
	shlq	$4, %rdx
	movq	8(%rcx,%rdx), %rax
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	%rax, 16(%r14)
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movzwl	%bp, %r15d
	orl	%eax, %r15d
	jne	.LBB1_12
	jmp	.LBB1_7
.LBB1_10:                               #   in Loop: Header=BB1_12 Depth=2
	movq	stderr(%rip), %rdi
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	movq	%rbp, %rcx
	callq	fprintf
	testq	%rbp, %rbp
	je	.LBB1_15
	.p2align	4, 0x90
.LBB1_11:                               # %.lr.ph614
                                        #   Parent Loop BB1_9 Depth=1
                                        #     Parent Loop BB1_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13), %rdi
	callq	_IO_getc
	decq	%rbp
	jne	.LBB1_11
	jmp	.LBB1_15
	.p2align	4, 0x90
.LBB1_12:                               #   Parent Loop BB1_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_11 Depth 3
	movq	(%r13), %rbp
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	movq	%rbp, %rdi
	callq	_IO_getc
	shll	$8, %r12d
	movzbl	%al, %r14d
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %r13
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r13, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movzwl	%bp, %ebp
	orq	%rax, %rbp
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movq	%r13, %rdi
	movq	8(%rsp), %r13           # 8-byte Reload
	callq	_IO_getc
	shll	$8, %ebx
	movzbl	%al, %eax
	movzwl	%bx, %ecx
	orq	%rax, %rcx
	shlq	$16, %rbp
	orq	%rcx, %rbp
	movq	40(%r13), %rax
	movzwl	%r12w, %ecx
	orq	%r14, %rcx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rbx
	movl	$.L.str.7, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_10
# BB#13:                                #   in Loop: Header=BB1_12 Depth=2
	cmpq	$2, %rbp
	jne	.LBB1_155
# BB#14:                                #   in Loop: Header=BB1_12 Depth=2
	movq	32(%rsp), %r14          # 8-byte Reload
	movl	$1, 24(%r14)
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movw	%ax, 28(%r14)
.LBB1_15:                               # %.backedge510
                                        #   in Loop: Header=BB1_12 Depth=2
	decl	%r15d
	jne	.LBB1_12
	jmp	.LBB1_7
.LBB1_16:                               # %._crit_edge620
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movw	%ax, 120(%r13)
	movzwl	%ax, %ebx
	leaq	(,%rbx,8), %rdi
	callq	_Znam
	movq	%rax, 128(%r13)
	addw	144(%r13), %bx
	movw	%bx, 144(%r13)
	movzwl	%bx, %edi
	shlq	$3, %rdi
	callq	_Znam
	movq	%rax, 152(%r13)
	movq	40(%r13), %rax
	movzwl	50(%r13), %ecx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rcx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rbx
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB1_21
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph606
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbp
	leaq	1(%rbp), %rdi
	movl	$47, %esi
	callq	strchr
	testq	%rax, %rax
	jne	.LBB1_17
# BB#18:                                # %._crit_edge607
	subq	%rbx, %rbp
	movq	%rbp, %rax
	shlq	$32, %rax
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	addq	%rcx, %rax
	sarq	$32, %rax
	cmpl	$-1, %ebp
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, 56(%r13)
	movslq	%ebp, %rbp
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	strncpy
	movq	56(%r13), %rax
	movb	$0, (%rax,%rbp)
	leaq	1(%rbx,%rbp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 64(%r13)
	movq	40(%r13), %rcx
	movzwl	50(%r13), %edx
	shlq	$4, %rdx
	movq	8(%rcx,%rdx), %rdx
	shlq	$4, %rdx
	movq	%rbx, 8(%rcx,%rdx)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	56(%r13), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r15
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB1_22
# BB#19:                                # %.lr.ph604.preheader
	movq	40(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_20:                               # %.lr.ph604
                                        # =>This Inner Loop Header: Depth=1
	movb	$46, (%rax)
	incq	%rax
	movl	$47, %esi
	movq	%rax, %rdi
	callq	strchr
	testq	%rax, %rax
	jne	.LBB1_20
	jmp	.LBB1_23
.LBB1_21:
	movq	$0, 56(%r13)
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 64(%r13)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	xorl	%r15d, %r15d
.LBB1_22:                               # %.loopexit506
	movq	40(%rsp), %rbx          # 8-byte Reload
.LBB1_23:                               # %.loopexit506
	movw	(%rbx), %r12w
	decw	%r12w
	js	.LBB1_80
# BB#24:                                # %.lr.ph581.lr.ph
	movslq	%r15d, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leal	1(%rax), %eax
	cltq
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB1_65
.LBB1_25:                               # %.outer501.loopexit
                                        #   in Loop: Header=BB1_26 Depth=2
	addq	%rax, %rbx
	addq	%rax, %r14
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB1_26:                               #   Parent Loop BB1_65 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_33 Depth 3
                                        #       Child Loop BB1_43 Depth 3
                                        #       Child Loop BB1_47 Depth 3
                                        #       Child Loop BB1_57 Depth 3
	movb	(%rbp), %al
	movb	%al, (%rbx)
	testb	%al, %al
	je	.LBB1_79
# BB#27:                                #   in Loop: Header=BB1_26 Depth=2
	incq	%rbx
	cmpb	$76, (%rbp)
	leaq	1(%rbp), %rbp
	jne	.LBB1_26
# BB#28:                                #   in Loop: Header=BB1_26 Depth=2
	movl	$59, %esi
	movq	%rbp, %rdi
	callq	strchr
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB1_30
# BB#29:                                #   in Loop: Header=BB1_26 Depth=2
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	_Z10fatalerroriz
.LBB1_30:                               #   in Loop: Header=BB1_26 Depth=2
	movl	$.L.str.10, %esi
	movl	$10, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_35
# BB#31:                                # %.preheader496
                                        #   in Loop: Header=BB1_26 Depth=2
	movl	$47, %esi
	movq	%rbp, %rdi
	jmp	.LBB1_33
	.p2align	4, 0x90
.LBB1_32:                               # %.lr.ph586
                                        #   in Loop: Header=BB1_33 Depth=3
	movb	$46, (%rax)
	incq	%rax
	movl	$47, %esi
	movq	%rax, %rdi
.LBB1_33:                               # %.preheader496
                                        #   Parent Loop BB1_65 Depth=1
                                        #     Parent Loop BB1_26 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	callq	strchr
	testq	%rax, %rax
	je	.LBB1_36
# BB#34:                                # %.preheader496
                                        #   in Loop: Header=BB1_33 Depth=3
	cmpq	%r13, %rax
	jb	.LBB1_32
	jmp	.LBB1_36
.LBB1_35:                               #   in Loop: Header=BB1_26 Depth=2
	addq	$10, %rbp
.LBB1_36:                               # %.critedge
                                        #   in Loop: Header=BB1_26 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	56(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB1_38
# BB#37:                                #   in Loop: Header=BB1_26 Depth=2
	movq	%rbp, %rdi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	strncmp
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rbp,%rcx), %rcx
	testl	%eax, %eax
	cmoveq	%rcx, %rbp
.LBB1_38:                               #   in Loop: Header=BB1_26 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpw	(%rax), %cx
	jne	.LBB1_40
# BB#39:                                #   in Loop: Header=BB1_26 Depth=2
	movq	%rbp, %r14
	jmp	.LBB1_56
.LBB1_40:                               #   in Loop: Header=BB1_26 Depth=2
	movl	$46, %esi
	movq	%rbp, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB1_51
# BB#41:                                #   in Loop: Header=BB1_26 Depth=2
	cmpq	%r13, %rax
	jae	.LBB1_51
# BB#42:                                # %.lr.ph590.preheader
                                        #   in Loop: Header=BB1_26 Depth=2
	movq	%r15, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_43:                               # %.lr.ph590
                                        #   Parent Loop BB1_65 Depth=1
                                        #     Parent Loop BB1_26 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, %r14
	incq	%r14
	movl	$46, %esi
	movq	%r14, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB1_45
# BB#44:                                # %.lr.ph590
                                        #   in Loop: Header=BB1_43 Depth=3
	cmpq	%r13, %rax
	jb	.LBB1_43
.LBB1_45:                               # %.critedge1
                                        #   in Loop: Header=BB1_26 Depth=2
	subq	%rbp, %r13
	movq	%r13, %rax
	shlq	$32, %rax
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	addq	%rcx, %rax
	movq	%rax, %rdi
	sarq	$32, %rdi
	testq	%rax, %rax
	movq	$-1, %rax
	cmovsq	%rax, %rdi
	callq	_Znam
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	152(%rcx), %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	leal	1(%rdx), %r15d
	movzwl	%dx, %edx
	movq	%rax, (%rcx,%rdx,8)
	movslq	%r13d, %rcx
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movq	%rcx, %rbp
	movq	%rbp, %rdx
	callq	strncpy
	movq	%rax, %r13
	movb	$0, (%r13,%rbp)
	movzwl	%r15w, %ebp
	movl	%ebp, %eax
	addl	$-2, %eax
	js	.LBB1_52
# BB#46:                                # %.lr.ph593
                                        #   in Loop: Header=BB1_26 Depth=2
	movl	%r15d, 40(%rsp)         # 4-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	152(%rax), %r15
	decq	%rbp
	.p2align	4, 0x90
.LBB1_47:                               #   Parent Loop BB1_65 Depth=1
                                        #     Parent Loop BB1_26 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-8(%r15,%rbp,8), %rdi
	movq	%r13, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_54
# BB#48:                                #   in Loop: Header=BB1_47 Depth=3
	decq	%rbp
	jg	.LBB1_47
# BB#49:                                #   in Loop: Header=BB1_26 Depth=2
	movl	40(%rsp), %eax          # 4-byte Reload
                                        # kill: %AX<def> %AX<kill> %EAX<kill> %RAX<def>
	jmp	.LBB1_53
.LBB1_51:                               #   in Loop: Header=BB1_26 Depth=2
	movq	%rbp, %r14
	jmp	.LBB1_56
.LBB1_52:                               #   in Loop: Header=BB1_26 Depth=2
	movw	%r15w, %ax
.LBB1_53:                               # %.preheader493
                                        #   in Loop: Header=BB1_26 Depth=2
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%rsp), %r15          # 8-byte Reload
	jmp	.LBB1_56
.LBB1_54:                               #   in Loop: Header=BB1_26 Depth=2
	testq	%r13, %r13
	movq	16(%rsp), %r15          # 8-byte Reload
	je	.LBB1_56
# BB#55:                                #   in Loop: Header=BB1_26 Depth=2
	movq	%r13, %rdi
	callq	_ZdaPv
.LBB1_56:                               # %.preheader493
                                        #   in Loop: Header=BB1_26 Depth=2
	xorl	%eax, %eax
	movq	8(%rsp), %r13           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_57:                               #   Parent Loop BB1_65 Depth=1
                                        #     Parent Loop BB1_26 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%r14,%rax), %ecx
	movb	%cl, (%rbx,%rax)
	incq	%rax
	cmpb	$59, %cl
	jne	.LBB1_57
	jmp	.LBB1_25
.LBB1_58:                               #   in Loop: Header=BB1_65 Depth=1
	movq	152(%r13), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	leal	1(%rcx), %edx
	movzwl	%cx, %ecx
	movq	%rbx, (%rax,%rcx,8)
	movq	%r13, %rcx
	movzwl	%dx, %r13d
	movl	%r13d, %eax
	addl	$-2, %eax
	js	.LBB1_64
# BB#59:                                # %.lr.ph596
                                        #   in Loop: Header=BB1_65 Depth=1
	movl	%edx, 40(%rsp)          # 4-byte Spill
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	152(%rcx), %r15
	decq	%r13
	.p2align	4, 0x90
.LBB1_60:                               #   Parent Loop BB1_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%r15,%r13,8), %rdi
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_63
# BB#61:                                #   in Loop: Header=BB1_60 Depth=2
	decq	%r13
	jg	.LBB1_60
# BB#62:                                #   in Loop: Header=BB1_65 Depth=1
	movq	16(%rsp), %r15          # 8-byte Reload
	movl	40(%rsp), %edx          # 4-byte Reload
	jmp	.LBB1_64
.LBB1_63:                               #   in Loop: Header=BB1_65 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movw	%ax, %dx
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB1_64:                               # %._crit_edge597
                                        #   in Loop: Header=BB1_65 Depth=1
	incq	%rbp
	movq	%rbp, (%r14)
	movw	%dx, %ax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB1_79
	.p2align	4, 0x90
.LBB1_65:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_26 Depth 2
                                        #       Child Loop BB1_33 Depth 3
                                        #       Child Loop BB1_43 Depth 3
                                        #       Child Loop BB1_47 Depth 3
                                        #       Child Loop BB1_57 Depth 3
                                        #     Child Loop BB1_71 Depth 2
                                        #     Child Loop BB1_60 Depth 2
	movq	40(%r13), %rax
	movzwl	%r12w, %r12d
	movq	%r12, %rcx
	shlq	$4, %rcx
	movb	(%rax,%rcx), %dl
	cmpb	$7, %dl
	je	.LBB1_69
# BB#66:                                #   in Loop: Header=BB1_65 Depth=1
	cmpb	$12, %dl
	jne	.LBB1_79
# BB#67:                                #   in Loop: Header=BB1_65 Depth=1
	movq	8(%rax,%rcx), %rcx
	movzwl	2(%rcx), %ecx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rbx
	movq	%rbx, %rdi
	callq	strdup
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB1_26
# BB#68:                                #   in Loop: Header=BB1_65 Depth=1
	movl	$1, %edi
	xorl	%eax, %eax
	callq	_Z10fatalerroriz
	xorl	%ebp, %ebp
	jmp	.LBB1_26
	.p2align	4, 0x90
.LBB1_69:                               #   in Loop: Header=BB1_65 Depth=1
	movq	8(%rax,%rcx), %rcx
	shlq	$4, %rcx
	leaq	8(%rax,%rcx), %r14
	movq	8(%rax,%rcx), %rbx
	movl	$.L.str.10, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_73
# BB#70:                                # %.preheader497
                                        #   in Loop: Header=BB1_65 Depth=1
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB1_74
	.p2align	4, 0x90
.LBB1_71:                               # %.lr.ph580
                                        #   Parent Loop BB1_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$46, (%rax)
	incq	%rax
	movl	$47, %esi
	movq	%rax, %rdi
	callq	strchr
	testq	%rax, %rax
	jne	.LBB1_71
# BB#72:                                # %.loopexit498.loopexit
                                        #   in Loop: Header=BB1_65 Depth=1
	movq	(%r14), %rbx
	jmp	.LBB1_74
.LBB1_73:                               #   in Loop: Header=BB1_65 Depth=1
	addq	$10, %rbx
	movq	%rbx, (%r14)
.LBB1_74:                               # %.loopexit498
                                        #   in Loop: Header=BB1_65 Depth=1
	movq	56(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB1_77
# BB#75:                                #   in Loop: Header=BB1_65 Depth=1
	movq	%rbx, %rdi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB1_77
# BB#76:                                #   in Loop: Header=BB1_65 Depth=1
	addq	48(%rsp), %rbx          # 8-byte Folded Reload
	movq	%rbx, (%r14)
.LBB1_77:                               #   in Loop: Header=BB1_65 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpw	(%rax), %cx
	je	.LBB1_79
# BB#78:                                #   in Loop: Header=BB1_65 Depth=1
	movl	$46, %esi
	movq	%rbx, %rdi
	callq	strrchr
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB1_58
.LBB1_79:                               # %.backedge505
                                        #   in Loop: Header=BB1_65 Depth=1
	decw	%r12w
	jns	.LBB1_65
	jmp	.LBB1_81
.LBB1_80:
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB1_81:                               # %.outer504._crit_edge
	movw	120(%r13), %ax
	testw	%ax, %ax
	je	.LBB1_147
# BB#82:                                # %.lr.ph577
	movslq	%r15d, %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	incl	%r15d
	movslq	%r15d, %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_83:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_87 Depth 2
                                        #       Child Loop BB1_89 Depth 3
                                        #         Child Loop BB1_90 Depth 4
                                        #           Child Loop BB1_97 Depth 5
                                        #           Child Loop BB1_105 Depth 5
                                        #           Child Loop BB1_109 Depth 5
                                        #     Child Loop BB1_119 Depth 2
                                        #       Child Loop BB1_115 Depth 3
                                        #       Child Loop BB1_123 Depth 3
                                        #       Child Loop BB1_126 Depth 3
                                        #       Child Loop BB1_131 Depth 3
                                        #         Child Loop BB1_130 Depth 4
                                        #         Child Loop BB1_136 Depth 4
                                        #         Child Loop BB1_143 Depth 4
	decl	%eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
	movl	$144, %edi
	callq	_Znwm
	movq	%rax, %r15
	movw	$0, (%r15)
	movq	128(%r13), %rax
	movq	%r15, (%rax,%rbx,8)
	movq	(%r13), %r14
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movw	%ax, (%r15)
	movq	(%r13), %r14
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movq	40(%r13), %rcx
	movzwl	%bp, %edx
	orq	%rax, %rdx
	shlq	$4, %rdx
	movq	8(%rcx,%rdx), %rax
	movq	%rax, 8(%r15)
	movq	(%r13), %r14
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movq	40(%r13), %rcx
	movzwl	%bp, %edx
	orq	%rax, %rdx
	shlq	$4, %rdx
	movq	8(%rcx,%rdx), %r13
	movq	%r13, 16(%r15)
	movq	%r13, %rdi
	callq	strdup
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB1_85
# BB#84:                                #   in Loop: Header=BB1_83 Depth=1
	movl	$1, %edi
	xorl	%eax, %eax
	callq	_Z10fatalerroriz
	xorl	%r14d, %r14d
.LBB1_85:                               # %.outer.outer.preheader
                                        #   in Loop: Header=BB1_83 Depth=1
	incq	%rbx
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	1(%rax), %ecx
                                        # kill: %AX<def> %AX<kill> %RAX<kill> %EAX<def>
	jmp	.LBB1_87
.LBB1_86:                               # %.outer.outer.backedge
                                        #   in Loop: Header=BB1_87 Depth=2
	movl	40(%rsp), %ecx          # 4-byte Reload
	incl	%ecx
	movl	60(%rsp), %eax          # 4-byte Reload
	.p2align	4, 0x90
.LBB1_87:                               # %.outer.outer
                                        #   Parent Loop BB1_83 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_89 Depth 3
                                        #         Child Loop BB1_90 Depth 4
                                        #           Child Loop BB1_97 Depth 5
                                        #           Child Loop BB1_105 Depth 5
                                        #           Child Loop BB1_109 Depth 5
                                        # kill: %AX<def> %AX<kill> %EAX<kill> %RAX<def>
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movzwl	%cx, %ecx
	decq	%rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leal	1(%rax), %ecx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movzwl	%ax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%ecx, 60(%rsp)          # 4-byte Spill
	movzwl	%cx, %eax
	addl	$-2, %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	jmp	.LBB1_89
.LBB1_88:                               #   in Loop: Header=BB1_89 Depth=3
	movq	%r12, %rdi
	callq	_ZdaPv
	.p2align	4, 0x90
.LBB1_89:                               # %.outer
                                        #   Parent Loop BB1_83 Depth=1
                                        #     Parent Loop BB1_87 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_90 Depth 4
                                        #           Child Loop BB1_97 Depth 5
                                        #           Child Loop BB1_105 Depth 5
                                        #           Child Loop BB1_109 Depth 5
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB1_90:                               #   Parent Loop BB1_83 Depth=1
                                        #     Parent Loop BB1_87 Depth=2
                                        #       Parent Loop BB1_89 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB1_97 Depth 5
                                        #           Child Loop BB1_105 Depth 5
                                        #           Child Loop BB1_109 Depth 5
	movb	(%rbp), %al
	movb	%al, (%r13)
	testb	%al, %al
	je	.LBB1_112
# BB#91:                                #   in Loop: Header=BB1_90 Depth=4
	incq	%r13
	cmpb	$76, (%rbp)
	leaq	1(%rbp), %rbp
	jne	.LBB1_90
# BB#92:                                #   in Loop: Header=BB1_90 Depth=4
	movl	$59, %esi
	movq	%rbp, %rdi
	callq	strchr
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_94
# BB#93:                                #   in Loop: Header=BB1_90 Depth=4
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	_Z10fatalerroriz
.LBB1_94:                               #   in Loop: Header=BB1_90 Depth=4
	movl	$.L.str.10, %esi
	movl	$10, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_99
# BB#95:                                # %.preheader487
                                        #   in Loop: Header=BB1_90 Depth=4
	movl	$47, %esi
	movq	%rbp, %rdi
	jmp	.LBB1_97
	.p2align	4, 0x90
.LBB1_96:                               # %.lr.ph537
                                        #   in Loop: Header=BB1_97 Depth=5
	movb	$46, (%rax)
	incq	%rax
	movl	$47, %esi
	movq	%rax, %rdi
.LBB1_97:                               # %.preheader487
                                        #   Parent Loop BB1_83 Depth=1
                                        #     Parent Loop BB1_87 Depth=2
                                        #       Parent Loop BB1_89 Depth=3
                                        #         Parent Loop BB1_90 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	callq	strchr
	testq	%rax, %rax
	je	.LBB1_100
# BB#98:                                # %.preheader487
                                        #   in Loop: Header=BB1_97 Depth=5
	cmpq	%rbx, %rax
	jb	.LBB1_96
	jmp	.LBB1_100
.LBB1_99:                               #   in Loop: Header=BB1_90 Depth=4
	addq	$10, %rbp
.LBB1_100:                              # %.critedge2
                                        #   in Loop: Header=BB1_90 Depth=4
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	56(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB1_102
# BB#101:                               #   in Loop: Header=BB1_90 Depth=4
	movq	%rbp, %rdi
	movq	88(%rsp), %rdx          # 8-byte Reload
	callq	strncmp
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%rbp,%rcx), %rcx
	testl	%eax, %eax
	cmoveq	%rcx, %rbp
.LBB1_102:                              #   in Loop: Header=BB1_90 Depth=4
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpw	(%rax), %cx
	je	.LBB1_90
# BB#103:                               #   in Loop: Header=BB1_90 Depth=4
	movl	$46, %esi
	movq	%rbp, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB1_90
# BB#104:                               #   in Loop: Header=BB1_90 Depth=4
	cmpq	%rbx, %rax
	jae	.LBB1_90
	.p2align	4, 0x90
.LBB1_105:                              # %.lr.ph540
                                        #   Parent Loop BB1_83 Depth=1
                                        #     Parent Loop BB1_87 Depth=2
                                        #       Parent Loop BB1_89 Depth=3
                                        #         Parent Loop BB1_90 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rax, %r14
	incq	%r14
	movl	$46, %esi
	movq	%r14, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB1_107
# BB#106:                               # %.lr.ph540
                                        #   in Loop: Header=BB1_105 Depth=5
	cmpq	%rbx, %rax
	jb	.LBB1_105
.LBB1_107:                              # %.critedge3
                                        #   in Loop: Header=BB1_90 Depth=4
	subq	%rbp, %rbx
	movq	%rbx, %rax
	shlq	$32, %rax
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	addq	%rcx, %rax
	movq	%rax, %rdi
	sarq	$32, %rdi
	testq	%rax, %rax
	movq	$-1, %rax
	cmovsq	%rax, %rdi
	callq	_Znam
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	152(%rcx), %rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%rax, (%rcx,%rdx,8)
	movslq	%ebx, %rbx
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	strncpy
	movq	%rax, %r12
	movb	$0, (%r12,%rbx)
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	js	.LBB1_86
# BB#108:                               # %.lr.ph543
                                        #   in Loop: Header=BB1_90 Depth=4
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	152(%rax), %rbp
	movq	16(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_109:                              #   Parent Loop BB1_83 Depth=1
                                        #     Parent Loop BB1_87 Depth=2
                                        #       Parent Loop BB1_89 Depth=3
                                        #         Parent Loop BB1_90 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	-8(%rbp,%rbx,8), %rdi
	movq	%r12, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_111
# BB#110:                               #   in Loop: Header=BB1_109 Depth=5
	decq	%rbx
	jg	.LBB1_109
	jmp	.LBB1_86
.LBB1_111:                              #   in Loop: Header=BB1_90 Depth=4
	testq	%r12, %r12
	movq	%r14, %rbp
	je	.LBB1_90
	jmp	.LBB1_88
	.p2align	4, 0x90
.LBB1_112:                              #   in Loop: Header=BB1_83 Depth=1
	movl	$0, 132(%r15)
	movw	$0, 72(%r15)
	movw	$0, 56(%r15)
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	(%r13), %r14
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movzwl	%bp, %ecx
	orl	%eax, %ecx
	jne	.LBB1_119
	jmp	.LBB1_146
.LBB1_113:                              #   in Loop: Header=BB1_119 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	shll	$8, %ecx
	movzbl	%r12b, %eax
	movzwl	%cx, %r14d
	orq	%rax, %r14
	movq	32(%rsp), %rcx          # 8-byte Reload
	shll	$8, %ecx
	movzbl	16(%rsp), %eax          # 1-byte Folded Reload
	movzwl	%cx, %ecx
	orq	%rax, %rcx
	shlq	$16, %r14
	orq	%rcx, %r14
	movq	stderr(%rip), %rdi
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	movq	%r14, %rcx
	callq	fprintf
	testq	%r14, %r14
	movq	8(%rsp), %r13           # 8-byte Reload
	je	.LBB1_118
# BB#114:                               # %.lr.ph551.preheader
                                        #   in Loop: Header=BB1_119 Depth=2
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB1_115:                              # %.lr.ph551
                                        #   Parent Loop BB1_83 Depth=1
                                        #     Parent Loop BB1_119 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13), %rdi
	callq	_IO_getc
	movzbl	%al, %esi
	movl	$32, %edx
	testb	$7, %bl
	jne	.LBB1_117
# BB#116:                               #   in Loop: Header=BB1_115 Depth=3
	movl	%ebx, %eax
	andl	$15, %eax
	cmpl	$1, %eax
	movl	$9, %edx
	sbbl	$-1, %edx
.LBB1_117:                              #   in Loop: Header=BB1_115 Depth=3
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	printf
	movl	%ebx, %eax
	incl	%ebx
	cmpq	%r14, %rax
	jne	.LBB1_115
.LBB1_118:                              # %._crit_edge552
                                        #   in Loop: Header=BB1_119 Depth=2
	movl	$10, %edi
	callq	putchar
	jmp	.LBB1_145
	.p2align	4, 0x90
.LBB1_119:                              #   Parent Loop BB1_83 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_115 Depth 3
                                        #       Child Loop BB1_123 Depth 3
                                        #       Child Loop BB1_126 Depth 3
                                        #       Child Loop BB1_131 Depth 3
                                        #         Child Loop BB1_130 Depth 4
                                        #         Child Loop BB1_136 Depth 4
                                        #         Child Loop BB1_143 Depth 4
	decl	%ecx
	movl	%ecx, 60(%rsp)          # 4-byte Spill
	movq	(%r13), %rbp
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movq	%rbp, %rdi
	callq	_IO_getc
	shll	$8, %ebx
	movzbl	%al, %r14d
	movq	(%r13), %rbp
	movq	%rbp, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	movq	%rbp, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	40(%r13), %rax
	movzwl	%bx, %ecx
	orq	%r14, %rcx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rbp
	movl	$.L.str.11, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_124
# BB#120:                               #   in Loop: Header=BB1_119 Depth=2
	movl	$.L.str.15, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_113
# BB#121:                               #   in Loop: Header=BB1_119 Depth=2
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	(%r13), %rbp
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movq	%rbp, %rdi
	callq	_IO_getc
	shll	$8, %ebx
	movzbl	%al, %r14d
	movl	%r14d, %eax
	orw	%bx, %ax
	movzwl	%ax, %r12d
	movl	%r12d, 132(%r15)
	leaq	(,%r12,4), %rdi
	callq	_Znam
	movq	%rax, %rbp
	orw	%bx, %r14w
	movq	%rbp, 136(%r15)
	je	.LBB1_145
# BB#122:                               # %.lr.ph556.preheader
                                        #   in Loop: Header=BB1_119 Depth=2
	negl	%r12d
	.p2align	4, 0x90
.LBB1_123:                              # %.lr.ph556
                                        #   Parent Loop BB1_83 Depth=1
                                        #     Parent Loop BB1_119 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13), %r14
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movq	%r14, %rdi
	callq	_IO_getc
	shll	$8, %ebx
	movzbl	%al, %eax
	movzwl	%bx, %ecx
	orl	%eax, %ecx
	movl	%ecx, (%rbp)
	addq	$4, %rbp
	incl	%r12d
	jne	.LBB1_123
	jmp	.LBB1_145
	.p2align	4, 0x90
.LBB1_124:                              #   in Loop: Header=BB1_119 Depth=2
	movq	(%r13), %rbp
	movq	%rbp, %rdi
	callq	_IO_getc
	movq	%rbp, %rdi
	callq	_IO_getc
	movb	%al, 24(%r15)
	movq	(%r13), %rbp
	movq	%rbp, %rdi
	callq	_IO_getc
	movq	%rbp, %rdi
	callq	_IO_getc
	movb	%al, 25(%r15)
	movq	(%r13), %r14
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movzwl	%bp, %ebp
	orq	%rax, %rbp
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movq	%r14, %rdi
	callq	_IO_getc
	shll	$8, %ebx
	movzbl	%al, %eax
	movzwl	%bx, %ecx
	orq	%rax, %rcx
	shlq	$16, %rbp
	orq	%rcx, %rbp
	movl	%ebp, 28(%r15)
	movq	%rbp, %rdi
	callq	_Znam
	movq	%rax, 32(%r15)
	addl	%ebp, 16(%r13)
	movq	(%r13), %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	fread
	movq	(%r13), %r14
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %r14d
	orl	%ebp, %r14d
	movw	%r14w, 40(%r15)
	movzwl	%r14w, %ebx
	leaq	(,%rbx,8), %rax
	leaq	(%rax,%rax,2), %rdi
	callq	_Znam
	movq	%rax, 48(%r15)
	testw	%bx, %bx
	je	.LBB1_127
# BB#125:                               # %.lr.ph560.preheader
                                        #   in Loop: Header=BB1_119 Depth=2
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_126:                              # %.lr.ph560
                                        #   Parent Loop BB1_83 Depth=1
                                        #     Parent Loop BB1_119 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$0, (%rax,%r12)
	movq	(%r13), %rbp
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movq	%rbp, %rdi
	callq	_IO_getc
	shll	$8, %ebx
	movzbl	%al, %eax
	orl	%ebx, %eax
	movq	48(%r15), %rcx
	movw	%ax, 4(%rcx,%r12)
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movq	48(%r15), %rcx
	movw	%ax, 6(%rcx,%r12)
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movq	48(%r15), %rcx
	movw	%ax, 8(%rcx,%r12)
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %ecx
	orl	%ebp, %ecx
	decw	%r14w
	movq	48(%r15), %rax
	movw	%cx, 12(%rax,%r12)
	leaq	24(%r12), %r12
	jne	.LBB1_126
.LBB1_127:                              # %._crit_edge561
                                        #   in Loop: Header=BB1_119 Depth=2
	movq	(%r13), %r14
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movzwl	%bp, %ecx
	orl	%eax, %ecx
	jne	.LBB1_131
	jmp	.LBB1_145
.LBB1_128:                              #   in Loop: Header=BB1_131 Depth=3
	movq	16(%rsp), %rcx          # 8-byte Reload
	shll	$8, %ecx
	movzbl	%r14b, %eax
	movzwl	%cx, %ebp
	orq	%rax, %rbp
	movq	48(%rsp), %rcx          # 8-byte Reload
	shll	$8, %ecx
	movzbl	40(%rsp), %eax          # 1-byte Folded Reload
	movzwl	%cx, %ecx
	orq	%rax, %rcx
	shlq	$16, %rbp
	orq	%rcx, %rbp
	movq	stderr(%rip), %rdi
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	movq	%rbp, %rcx
	callq	fprintf
	testl	%ebp, %ebp
	je	.LBB1_144
# BB#129:                               # %.lr.ph564.preheader
                                        #   in Loop: Header=BB1_131 Depth=3
	negl	%ebp
	.p2align	4, 0x90
.LBB1_130:                              # %.lr.ph564
                                        #   Parent Loop BB1_83 Depth=1
                                        #     Parent Loop BB1_119 Depth=2
                                        #       Parent Loop BB1_131 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rdi
	callq	_IO_getc
	incl	%ebp
	jne	.LBB1_130
	jmp	.LBB1_144
	.p2align	4, 0x90
.LBB1_131:                              # %.lr.ph571
                                        #   Parent Loop BB1_83 Depth=1
                                        #     Parent Loop BB1_119 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_130 Depth 4
                                        #         Child Loop BB1_136 Depth 4
                                        #         Child Loop BB1_143 Depth 4
	decl	%ecx
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %r12d
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %r14d
	movq	%rbx, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movq	40(%r13), %rax
	movzwl	%bp, %ecx
	orq	%r12, %rcx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rbx
	movl	$.L.str.12, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_138
# BB#132:                               #   in Loop: Header=BB1_131 Depth=3
	movl	$.L.str.13, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_128
# BB#133:                               #   in Loop: Header=BB1_131 Depth=3
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	(%r14), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movw	%ax, 72(%r15)
	movzwl	%ax, %ebp
	leaq	(%rbp,%rbp), %rax
	leaq	(%rax,%rax,4), %rbx
	movq	%rbx, %rdi
	callq	_Znam
	movq	%rax, 80(%r15)
	leal	(%rbp,%rbp,4), %ecx
	addl	%ecx, %ecx
	addl	%ecx, 16(%r14)
	movq	(%r14), %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	fread
	movzwl	72(%r15), %ebx
	movq	%rbx, %rdi
	shlq	$4, %rdi
	callq	_Znam
	movq	%rax, %r14
	testq	%rbx, %rbx
	movq	%r14, 88(%r15)
	je	.LBB1_139
# BB#134:                               # %.lr.ph566.preheader
                                        #   in Loop: Header=BB1_131 Depth=3
	leaq	(%rbx,%rbx,4), %rax
	leaq	-2(%rax,%rax), %r13
	movl	$1, %r12d
	subl	%ebx, %r12d
	jmp	.LBB1_136
	.p2align	4, 0x90
.LBB1_135:                              # %.lr.ph566..lr.ph566_crit_edge
                                        #   in Loop: Header=BB1_136 Depth=4
	movq	88(%r15), %r14
	addq	$-10, %r13
	incl	%r12d
.LBB1_136:                              # %.lr.ph566
                                        #   Parent Loop BB1_83 Depth=1
                                        #     Parent Loop BB1_119 Depth=2
                                        #       Parent Loop BB1_131 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	80(%r15), %rbp
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	40(%rax), %rax
	movzwl	-4(%rbp,%r13), %ecx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rbx
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movzwl	(%rbp,%r13), %ecx
	movq	%rax, (%r14,%rcx,8)
	movq	80(%r15), %rax
	movq	88(%r15), %rcx
	movzwl	(%rax,%r13), %eax
	movq	(%rcx,%rax,8), %rdi
	movq	%rbx, %rsi
	callq	strcpy
	testl	%r12d, %r12d
	jne	.LBB1_135
# BB#137:                               # %._crit_edge567.loopexit
                                        #   in Loop: Header=BB1_131 Depth=3
	movw	72(%r15), %ax
	jmp	.LBB1_140
	.p2align	4, 0x90
.LBB1_138:                              #   in Loop: Header=BB1_131 Depth=3
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movw	%ax, 56(%r15)
	movzwl	%ax, %ebp
	leaq	(,%rbp,4), %rbx
	movq	%rbx, %rdi
	callq	_Znam
	movq	%rax, 64(%r15)
	shll	$2, %ebp
	addl	%ebp, 16(%r13)
	movq	(%r13), %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	fread
	jmp	.LBB1_144
	.p2align	4, 0x90
.LBB1_139:                              #   in Loop: Header=BB1_131 Depth=3
	xorl	%eax, %eax
.LBB1_140:                              # %._crit_edge567
                                        #   in Loop: Header=BB1_131 Depth=3
	movzwl	%ax, %ebx
	movq	%rbx, %rdi
	shlq	$4, %rdi
	callq	_Znam
	movq	%rax, %r14
	movq	%r14, 96(%r15)
	testw	%bx, %bx
	je	.LBB1_144
# BB#141:                               # %.lr.ph569.preheader
                                        #   in Loop: Header=BB1_131 Depth=3
	leaq	(%rbx,%rbx,4), %rax
	leaq	-2(%rax,%rax), %r13
	movl	$1, %r12d
	subl	%ebx, %r12d
	jmp	.LBB1_143
	.p2align	4, 0x90
.LBB1_142:                              # %.lr.ph569..lr.ph569_crit_edge
                                        #   in Loop: Header=BB1_143 Depth=4
	movq	96(%r15), %r14
	addq	$-10, %r13
	incl	%r12d
.LBB1_143:                              # %.lr.ph569
                                        #   Parent Loop BB1_83 Depth=1
                                        #     Parent Loop BB1_119 Depth=2
                                        #       Parent Loop BB1_131 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	80(%r15), %rbp
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	40(%rax), %rax
	movzwl	-2(%rbp,%r13), %ecx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rbx
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movzwl	(%rbp,%r13), %ecx
	movq	%rax, (%r14,%rcx,8)
	movq	80(%r15), %rax
	movq	96(%r15), %rcx
	movzwl	(%rax,%r13), %eax
	movq	(%rcx,%rax,8), %rdi
	movq	%rbx, %rsi
	callq	strcpy
	testl	%r12d, %r12d
	jne	.LBB1_142
.LBB1_144:                              # %.backedge484
                                        #   in Loop: Header=BB1_131 Depth=3
	movl	32(%rsp), %ecx          # 4-byte Reload
	testl	%ecx, %ecx
	movq	8(%rsp), %r13           # 8-byte Reload
	jne	.LBB1_131
.LBB1_145:                              # %.backedge489
                                        #   in Loop: Header=BB1_119 Depth=2
	movl	60(%rsp), %ecx          # 4-byte Reload
	testl	%ecx, %ecx
	jne	.LBB1_119
.LBB1_146:                              # %.loopexit488
                                        #   in Loop: Header=BB1_83 Depth=1
	movl	76(%rsp), %eax          # 4-byte Reload
	testw	%ax, %ax
	movq	96(%rsp), %rbx          # 8-byte Reload
	jne	.LBB1_83
.LBB1_147:                              # %._crit_edge578
	movq	24(%rsp), %rax          # 8-byte Reload
	movw	%ax, 144(%r13)
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %r12d
	orw	%bp, %r12w
	jne	.LBB1_150
	jmp	.LBB1_154
.LBB1_148:                              #   in Loop: Header=BB1_150 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	callq	fprintf
	testq	%rbx, %rbx
	movq	8(%rsp), %r13           # 8-byte Reload
	je	.LBB1_153
	.p2align	4, 0x90
.LBB1_149:                              # %.lr.ph
                                        #   Parent Loop BB1_150 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rdi
	callq	_IO_getc
	decq	%rbx
	jne	.LBB1_149
	jmp	.LBB1_153
	.p2align	4, 0x90
.LBB1_150:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_149 Depth 2
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %r14d
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %r14d
	movzbl	%al, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	(%r13), %r15
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movq	%r15, %rdi
	callq	_IO_getc
	shll	$8, %ebx
	movzbl	%al, %eax
	movzwl	%bx, %ebx
	orq	%rax, %rbx
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r15, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movzwl	%bp, %ecx
	orq	%rax, %rcx
	shlq	$16, %rbx
	orq	%rcx, %rbx
	movq	40(%r13), %rax
	movzwl	%r14w, %ecx
	addq	32(%rsp), %rcx          # 8-byte Folded Reload
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rbp
	movl	$.L.str.19, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_148
# BB#151:                               #   in Loop: Header=BB1_150 Depth=1
	cmpq	$2, %rbx
	jne	.LBB1_157
# BB#152:                               #   in Loop: Header=BB1_150 Depth=1
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movq	40(%r13), %rcx
	movzwl	%bp, %edx
	orq	%rax, %rdx
	shlq	$4, %rdx
	movq	8(%rcx,%rdx), %rbx
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 136(%r13)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcpy
.LBB1_153:                              # %.backedge
                                        #   in Loop: Header=BB1_150 Depth=1
	decl	%r12d
	testw	%r12w, %r12w
	jne	.LBB1_150
.LBB1_154:                              # %._crit_edge
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_155:
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$51, %esi
.LBB1_156:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB1_157:
	movq	stderr(%rip), %rcx
	movl	$.L.str.20, %edi
	movl	$48, %esi
	jmp	.LBB1_156
.Lfunc_end1:
	.size	_ZN9Classfile4readEv, .Lfunc_end1-_ZN9Classfile4readEv
	.cfi_endproc

	.globl	_ZN9Classfile5printEv
	.p2align	4, 0x90
	.type	_ZN9Classfile5printEv,@function
_ZN9Classfile5printEv:                  # @_ZN9Classfile5printEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 64
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	stderr(%rip), %rdi
	movq	136(%r15), %rdx
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	8(%r15), %rdi
	movq	136(%r15), %rdx
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	56(%r15), %rdx
	testq	%rdx, %rdx
	je	.LBB2_2
# BB#1:
	movq	8(%r15), %rdi
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB2_2:
	movzwl	144(%r15), %ebx
	testl	%ebx, %ebx
	movq	8(%r15), %rcx
	je	.LBB2_5
# BB#3:                                 # %.lr.ph55.preheader
	movq	152(%r15), %rbp
	negl	%ebx
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph55
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdx
	addq	$8, %rbp
	movl	$.L.str.25, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fprintf
	movq	8(%r15), %rcx
	incl	%ebx
	jne	.LBB2_4
.LBB2_5:                                # %.loopexit
	movl	$10, %edi
	movq	%rcx, %rsi
	callq	fputc
	leaq	48(%r15), %r14
	movq	%r14, %rdi
	callq	_ZN11AccessFlags6strlenEv
	movzwl	%ax, %edi
	incq	%rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, (%rsp)
	movq	8(%r15), %r12
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN11AccessFlags8toStringEPc
	movq	%rax, %rdx
	movq	64(%r15), %rcx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	fprintf
	movq	%rbx, %rdi
	callq	_ZdaPv
	movzwl	72(%r15), %eax
	testq	%rax, %rax
	je	.LBB2_9
# BB#6:
	movq	40(%r15), %rcx
	shlq	$4, %rax
	movq	8(%rcx,%rax), %rax
	shlq	$4, %rax
	movq	8(%rcx,%rax), %rbx
	movq	%rbx, (%rsp)
	movl	$.L.str.27, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_7
# BB#8:
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 80(%r15)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	8(%r15), %rdi
	movq	80(%r15), %rdx
	movl	$.L.str.28, %esi
	xorl	%eax, %eax
	callq	fprintf
	cmpw	$0, 88(%r15)
	jne	.LBB2_10
	jmp	.LBB2_14
.LBB2_7:
	movq	$.L.str.27, 80(%r15)
.LBB2_9:
	cmpw	$0, 88(%r15)
	je	.LBB2_14
.LBB2_10:
	movq	8(%r15), %rcx
	movl	$.L.str.29, %edi
	movl	$11, %esi
	movl	$1, %edx
	callq	fwrite
	movzwl	88(%r15), %eax
	movq	96(%r15), %rcx
	movq	8(%r15), %rdi
	movq	40(%r15), %rdx
	movzwl	(%rcx), %ecx
	shlq	$4, %rcx
	movq	8(%rdx,%rcx), %rcx
	shlq	$4, %rcx
	cmpl	$1, %eax
	movq	8(%rdx,%rcx), %rdx
	je	.LBB2_13
# BB#11:                                # %.lr.ph49.preheader
	movl	$1, %ebx
	subl	%eax, %ebx
	.p2align	4, 0x90
.LBB2_12:                               # %.lr.ph49
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	8(%r15), %rdi
	movq	40(%r15), %rax
	movq	96(%r15), %rcx
	movzwl	(%rcx), %ecx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rcx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rdx
	incl	%ebx
	jne	.LBB2_12
.LBB2_13:                               # %._crit_edge50
	movl	$.L.str.31, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB2_14:
	movq	8(%r15), %rsi
	movl	$123, %edi
	callq	fputc
	movzwl	104(%r15), %r13d
	testq	%r13, %r13
	je	.LBB2_30
# BB#15:                                # %.lr.ph44
	movq	%rsp, %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_16:                               # =>This Inner Loop Header: Depth=1
	movq	112(%r15), %rax
	movq	(%rax,%rbp,8), %rbx
	movq	%rbx, %rdi
	callq	_ZN11AccessFlags6strlenEv
	movzwl	%ax, %edi
	incq	%rdi
	callq	_Znam
	movq	%rax, (%rsp)
	movq	8(%r15), %r12
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN11AccessFlags8toStringEPc
	movq	%rax, %rcx
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %rdx
	callq	fprintf
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_18
# BB#17:                                #   in Loop: Header=BB2_16 Depth=1
	callq	_ZdaPv
.LBB2_18:                               #   in Loop: Header=BB2_16 Depth=1
	incq	%rbp
	movq	16(%rbx), %rax
	movq	%rax, (%rsp)
	movq	8(%r15), %rsi
	movq	8(%rbx), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	_Z12printsignameP9ClassfileP8_IO_FILERPcS3_Pv
	cmpl	$0, 24(%rbx)
	je	.LBB2_29
# BB#19:                                #   in Loop: Header=BB2_16 Depth=1
	movq	8(%r15), %rcx
	movl	$.L.str.34, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movq	16(%rbx), %rax
	movsbl	(%rax), %eax
	addl	$-68, %eax
	cmpl	$6, %eax
	ja	.LBB2_28
# BB#20:                                #   in Loop: Header=BB2_16 Depth=1
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_27:                               #   in Loop: Header=BB2_16 Depth=1
	movq	8(%r15), %rdi
	movq	40(%r15), %rax
	movzwl	28(%rbx), %ecx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.39, %esi
	jmp	.LBB2_26
.LBB2_25:                               #   in Loop: Header=BB2_16 Depth=1
	movq	8(%r15), %rdi
	movq	40(%r15), %rax
	movzwl	28(%rbx), %ecx
	shlq	$4, %rcx
	movss	8(%rax,%rcx), %xmm0     # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.38, %esi
.LBB2_26:                               #   in Loop: Header=BB2_16 Depth=1
	movb	$1, %al
	callq	fprintf
	jmp	.LBB2_29
.LBB2_21:                               #   in Loop: Header=BB2_16 Depth=1
	movq	8(%r15), %rdi
	movq	40(%r15), %rax
	movzwl	28(%rbx), %ecx
	shlq	$4, %rcx
	movq	8(%rax,%rcx), %rdx
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB2_29
.LBB2_22:                               #   in Loop: Header=BB2_16 Depth=1
	movzwl	28(%rbx), %eax
	movq	8(%r15), %rdi
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	shlq	$4, %rax
	movq	8(%rcx,%rax), %rdx
	movzwl	%si, %eax
	shlq	$4, %rax
	testq	%rdx, %rdx
	movq	8(%rcx,%rax), %rcx
	je	.LBB2_24
# BB#23:                                #   in Loop: Header=BB2_16 Depth=1
	movl	$.L.str.36, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB2_29
.LBB2_24:                               #   in Loop: Header=BB2_16 Depth=1
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	fprintf
	jmp	.LBB2_29
.LBB2_28:                               #   in Loop: Header=BB2_16 Depth=1
	movq	stderr(%rip), %rcx
	movl	$.L.str.40, %edi
	movl	$22, %esi
	movl	$1, %edx
	callq	fwrite
	.p2align	4, 0x90
.LBB2_29:                               #   in Loop: Header=BB2_16 Depth=1
	movq	8(%r15), %rsi
	movl	$59, %edi
	callq	fputc
	cmpl	%ebp, %r13d
	jne	.LBB2_16
.LBB2_30:                               # %._crit_edge45
	movzwl	120(%r15), %ebx
	testq	%rbx, %rbx
	je	.LBB2_35
# BB#31:                                # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_32:                               # =>This Inner Loop Header: Depth=1
	movq	128(%r15), %rax
	movq	(%rax,%rbp,8), %rsi
	incq	%rbp
	movq	%r15, %rdi
	callq	_Z14decompileblockP9ClassfileP11method_info
	testl	%eax, %eax
	je	.LBB2_34
# BB#33:                                #   in Loop: Header=BB2_32 Depth=1
	movq	8(%r15), %rcx
	movl	$.L.str.42, %edi
	movl	$41, %esi
	movl	$1, %edx
	callq	fwrite
.LBB2_34:                               # %.backedge
                                        #   in Loop: Header=BB2_32 Depth=1
	cmpl	%ebp, %ebx
	jne	.LBB2_32
.LBB2_35:                               # %._crit_edge
	movq	8(%r15), %rcx
	movl	$.L.str.43, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN9Classfile5printEv, .Lfunc_end2-_ZN9Classfile5printEv
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_27
	.quad	.LBB2_28
	.quad	.LBB2_25
	.quad	.LBB2_28
	.quad	.LBB2_28
	.quad	.LBB2_21
	.quad	.LBB2_22

	.type	progname,@object        # @progname
	.bss
	.globl	progname
	.p2align	3
progname:
	.quad	0
	.size	progname, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"hbt"
	.size	.L.str, 4

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Unknown flag: %s\n"
	.size	.L.str.1, 18

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	".bak"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"rb"
	.size	.L.str.3, 3

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Could not open file %s\n"
	.size	.L.str.4, 24

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"wb"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Classfile version %d.%d\n"
	.size	.L.str.6, 25

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"ConstantValue"
	.size	.L.str.7, 14

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Bad size on ConstantValue Attribute - should be 2!\n"
	.size	.L.str.8, 52

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Skipping Unknown Field Attribute: %s (size %ld)\n"
	.size	.L.str.9, 49

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"java/lang/"
	.size	.L.str.10, 11

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Code"
	.size	.L.str.11, 5

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"LineNumberTable"
	.size	.L.str.12, 16

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"LocalVariableTable"
	.size	.L.str.13, 19

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Skipping Unknown Code Attribute: %s (size %ld)\n"
	.size	.L.str.14, 48

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Exceptions"
	.size	.L.str.15, 11

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Skipping Unknown Method Attribute: %s (size %ld)\n"
	.size	.L.str.16, 50

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"%02x%c"
	.size	.L.str.17, 7

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"SourceFile"
	.size	.L.str.19, 11

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Bad size on SourceFile Attribute - should be 2!\n"
	.size	.L.str.20, 49

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Skipping Unknown Attribute: %s (size %ld)\n"
	.size	.L.str.21, 43

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Compiled from %s\n"
	.size	.L.str.22, 18

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"/*\n** Compiled from %s - COPYRIGHT UNKNOWN.\n**\n** Decompiled using the HomeBrew Decompiler\n** Copyright (c) 1994-2003 Widget (aka Pete Ryland).\n** Available under GPL from http://pdr.cx/hbd/\n*/\n\n"
	.size	.L.str.23, 196

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"package %s;\n\n"
	.size	.L.str.24, 14

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"import %s;\n"
	.size	.L.str.25, 12

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"%sclass %s "
	.size	.L.str.26, 12

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Object"
	.size	.L.str.27, 7

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"extends %s "
	.size	.L.str.28, 12

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"implements "
	.size	.L.str.29, 12

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"%s, "
	.size	.L.str.30, 5

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"%s "
	.size	.L.str.31, 4

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"\n  %s"
	.size	.L.str.33, 6

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	" = "
	.size	.L.str.34, 4

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"0x%lX"
	.size	.L.str.35, 6

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"0x%lX%08lXL"
	.size	.L.str.36, 12

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"0x%lXL"
	.size	.L.str.37, 7

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"%#.100Gf"
	.size	.L.str.38, 9

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"%#.100Gd"
	.size	.L.str.39, 9

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"Bad type for constant\n"
	.size	.L.str.40, 23

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"/* Decompilation Error.  Continuing... */"
	.size	.L.str.42, 42

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"\n}"
	.size	.L.str.43, 3


	.globl	_ZN9ClassfileC1EiPPc
	.type	_ZN9ClassfileC1EiPPc,@function
_ZN9ClassfileC1EiPPc = _ZN9ClassfileC2EiPPc
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
