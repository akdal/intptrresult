	.text
	.file	"padd.bc"
	.globl	padd
	.p2align	4, 0x90
	.type	padd,@function
padd:                                   # @padd
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB0_2
# BB#1:
	incw	(%r15)
.LBB0_2:
	testq	%r14, %r14
	je	.LBB0_4
# BB#3:
	incw	(%r14)
.LBB0_4:
	movb	6(%r15), %al
	movb	6(%r14), %cl
	cmpb	%cl, %al
	jne	.LBB0_5
# BB#9:
	movzwl	4(%r15), %ecx
	movzwl	4(%r14), %eax
	cmpw	%ax, %cx
	jae	.LBB0_10
# BB#11:
	movq	%r15, (%rsp)
	movq	%r14, %r12
	jmp	.LBB0_12
.LBB0_5:
	movq	$0, (%rsp)
	testb	%cl, %cl
	je	.LBB0_7
# BB#6:
	leaq	6(%r14), %rbx
	movb	$0, 6(%r14)
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	psub
	movq	%rsp, %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	%r14, %rax
	jmp	.LBB0_8
.LBB0_10:
	movl	%ecx, %eax
	movq	%r15, %r12
	movq	%r14, %r15
.LBB0_12:
	movzwl	%ax, %edi
	incl	%edi
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	callq	palloc
	movq	%rax, (%rsp)
	testq	%rax, %rax
	je	.LBB0_29
# BB#13:
	movq	%r15, %r14
	movb	6(%r12), %cl
	movb	%cl, 6(%rax)
	leaq	8(%r15), %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_14:                               # =>This Inner Loop Header: Depth=1
	movzwl	8(%r12,%rcx), %esi
	movzwl	8(%r15,%rcx), %edi
	addl	%ebx, %esi
	addl	%edi, %esi
	movl	%esi, %ebx
	shrl	$16, %ebx
	movw	%si, 8(%rax,%rcx)
	movzwl	4(%r15), %esi
	leaq	8(%r15,%rsi,2), %rsi
	leaq	2(%rdx,%rcx), %rdi
	addq	$2, %rcx
	cmpq	%rsi, %rdi
	jb	.LBB0_14
# BB#15:                                # %.preheader
	movzwl	4(%r12), %edx
	leaq	8(%r12,%rdx,2), %rdx
	leaq	8(%r12,%rcx), %rsi
	cmpq	%rdx, %rsi
	jae	.LBB0_16
# BB#17:                                # %.lr.ph.preheader
	leaq	8(%rax,%rcx), %r8
	leaq	(%r12,%rcx), %r9
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_18:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	8(%r9,%rdx), %esi
	addl	%ebx, %esi
	movl	%esi, %ebx
	shrl	$16, %ebx
	movw	%si, (%r8,%rdx)
	movzwl	4(%r12), %esi
	leaq	8(%r12,%rsi,2), %rsi
	leaq	10(%r9,%rdx), %rdi
	addq	$2, %rdx
	cmpq	%rsi, %rdi
	jb	.LBB0_18
# BB#19:                                # %._crit_edge.loopexit
	movq	%rax, %rsi
	addq	%rcx, %rsi
	leaq	8(%rdx,%rsi), %rcx
	jmp	.LBB0_20
.LBB0_7:
	leaq	6(%r15), %rbx
	testb	%al, %al
	sete	6(%r15)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	psub
	movq	%rsp, %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	%r15, %rax
.LBB0_8:
	cmpb	$0, (%rbx)
	sete	6(%rax)
	decw	(%r15)
	jne	.LBB0_25
	jmp	.LBB0_24
.LBB0_16:
	leaq	8(%rax,%rcx), %rcx
.LBB0_20:                               # %._crit_edge
	movw	%bx, (%rcx)
	testl	%ebx, %ebx
	jne	.LBB0_22
# BB#21:
	decw	4(%rax)
.LBB0_22:
	movq	%r12, %r15
	decw	(%r15)
	jne	.LBB0_25
.LBB0_24:
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	pfree
.LBB0_25:
	testq	%r14, %r14
	je	.LBB0_28
# BB#26:
	decw	(%r14)
	jne	.LBB0_28
# BB#27:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	pfree
.LBB0_28:
	movq	(%rsp), %rdi
	callq	presult
	movq	%rax, %rbx
.LBB0_29:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	padd, .Lfunc_end0-padd
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
